///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Web;
using System.Web.SessionState;
using System.Security.Principal;
using System.Data;
using System.Configuration;

// Import log4net classes.
using log4net;
using log4net.Config;

using Facade = Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva 
{
	/// <summary>
	/// Summary description for Global.
	/// </summary>
	public class Global : System.Web.HttpApplication
	{
		/// <summary>
		/// Get the current cache
		/// </summary>
		/// <returns></returns>
		public static System.Web.Caching.Cache Cache ()
		{
			return HttpRuntime.Cache;
		}

		/// <summary>
		/// Get the database utility
		/// </summary>
		/// <returns></returns>
		[Obsolete("MOVED TO KanevaGlobals - This is for backward compatibility")]
		public static DatabaseUtility GetDatabaseUtilityKGP()
		{
			if (m_DatabaseUtiltyKGP == null)
			{
				switch (KanevaGlobals.DatabaseType)
				{
					case (int) Constants.eSUPPORTED_DATABASES.SQLSERVER:
					{
						m_DatabaseUtiltyKGP = new SQLServerUtility (KanevaGlobals.ConnectionStringKGP);
						break;
					}
					case (int) Constants.eSUPPORTED_DATABASES.MYSQL:
					{
						m_DatabaseUtiltyKGP = new MySQLUtility (KanevaGlobals.ConnectionStringKGP);
						break;
					}
					default:
					{
						throw new Exception ("This database type is not supported");
					}
				}
			}
			return m_DatabaseUtiltyKGP;
		}

		public Global()
		{
			InitializeComponent();
		}	
		
		/// <summary>
		/// Application_Start
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void Application_Start(Object sender, EventArgs e)
		{
			// Change to log4net to be consistent with other modules
            XmlConfigurator.Configure(new System.IO.FileInfo(Server.MapPath("~") + "\\" + System.Configuration.ConfigurationManager.AppSettings["LogConfigFile"]));
            
			// Set appliation variables
			Application [Constants.CURRENT_USER_COUNT] = 0;

			// Set up Advertisements, get the settings
            AdvertisementSettings Settings = (AdvertisementSettings)ConfigurationManager.GetSection("Advertisement");
			if (Settings != null  && Settings.Mode != AdvertisingMode.Off)
			{
				// Store the settings in application state for quick access on each request
				Application ["Advertisement"] = Settings;
			}

            // Setup application RabbitMQ connection.
            try
            {
                var factory = new RabbitMQ.Client.ConnectionFactory
                {
                    HostName = Facade.Configuration.RabbitMQHost,
                    Port = Facade.Configuration.RabbitMQPort,
                    UserName = Facade.Configuration.RabbitMQUsername,
                    Password = Facade.Configuration.RabbitMQPassword,
                    VirtualHost = Facade.Configuration.RabbitMQVHost,
                    AutomaticRecoveryEnabled = Facade.Configuration.RabbitMQAutoRecoveryEnabled,
                    NetworkRecoveryInterval = Facade.Configuration.RabbitMQReconnectInterval
                };

                rabbitMQConnection = factory.CreateConnection();
            }
            catch (Exception exc)
            {
                m_logger.Error("Unable to establish connection to RabbitMQ.", exc);
            }
		}
 
		protected void Session_Start(Object sender, EventArgs e)
		{
			Application.Lock (); 
			Application [Constants.CURRENT_USER_COUNT] = Convert.ToInt32 (Application [Constants.CURRENT_USER_COUNT]) + 1;
			Application.UnLock ();

            // Aquisition - if this is the first time user is visiting kaneva, get their info on where they are
            // coming from and store in a cookie.  We'll use this on the registration page if they decide to become a Kaneva member.
            try
            {
                if (Request.UrlReferrer != null && Request.Cookies[Constants.ACQUISITION_COOKIE_NAME] == null)
                {
                    if (!KanevaGlobals.SiteName.Contains (Request.UrlReferrer.Host.ToString ()) && Request.UrlReferrer.Host.ToString () != "")
                    {
                        Response.Cookies[Constants.ACQUISITION_COOKIE_NAME].Values[Constants.ACQUISITION_COOKIE_VALUE_HOST] = KanevaGlobals.Encrypt (Request.UrlReferrer.Host.ToString ());
                        Response.Cookies[Constants.ACQUISITION_COOKIE_NAME].Values[Constants.ACQUISITION_COOKIE_VALUE_ABSOLUTEURI] = KanevaGlobals.Encrypt (Request.UrlReferrer.AbsoluteUri.ToString ());
                        Response.Cookies[Constants.ACQUISITION_COOKIE_NAME].Expires = DateTime.Now.AddYears (1);
                    }
                }
            }
            catch (Exception ex)
            {
                m_logger.Error (ex.ToString ());
            }
		}

		protected void Application_BeginRequest(Object sender, EventArgs e)
		{
            String strCurrentPath;
			strCurrentPath = Request.Path;
			strCurrentPath = strCurrentPath.ToLower();

			int communityId = 0;
			string commName = "";

			string siteName = "http://" + KanevaGlobals.SiteName;
			string status301 = "301 Moved Permanently";

			// RSS Feeds
			if (strCurrentPath.IndexOf ("/rss/") > -1)
			{
				string rssFeed = System.IO.Path.GetFileNameWithoutExtension (strCurrentPath);
				int rssFeedId = (int) Constants.eRSS_FEEDS.KANEVA_NEWS;
				string xmlQueryString = "";

				switch (rssFeed)
				{
					
					case "kanevanews":
					{
						rssFeedId = (int) Constants.eRSS_FEEDS.KANEVA_NEWS;
						break;
					}
					case "videonews":
					{
						rssFeedId = (int) Constants.eRSS_FEEDS.VIDEO_NEWS;
						break;
					}
					case "gamenews":
					{
						rssFeedId = (int) Constants.eRSS_FEEDS.GAME_NEWS;
						break;
					}
					case "devnews":
					{
						rssFeedId = (int) Constants.eRSS_FEEDS.DEV_NEWS;
						break;
					}
					case "communitynews":
					{
						rssFeedId = (int) Constants.eRSS_FEEDS.COMMUNITY_NEWS;
						break;
					}
					case "devforum":
					{
						xmlQueryString += "&channelId=" + (int) Constants.eCOMMUNITIES.CREATE;
						rssFeedId = (int) Constants.eRSS_FEEDS.FORUM_POSTS;
						break;
					}
					default:
					{					                            
                        // See if it is a valid community name
						commName = System.IO.Path.GetFileNameWithoutExtension (strCurrentPath);

						if (commName.LastIndexOf (".") > -1)
						{
							commName = System.IO.Path.GetFileNameWithoutExtension (commName);
						}

						//rss feed is a regular channel
                        Facade.CommunityFacade communityFacade = new Facade.CommunityFacade();
                        communityId = communityFacade.GetCommunityIdFromName(commName, false);

						// What type of feed?
						if (strCurrentPath.EndsWith (".forum.xml"))
						{
							rssFeedId = (int) Constants.eRSS_FEEDS.FORUM_POSTS;
							xmlQueryString += "&channelId=" + communityId;
						}
						else if (strCurrentPath.EndsWith (".blog.xml"))
						{
							rssFeedId = (int) Constants.eRSS_FEEDS.COMMUNITY_NEWS;
							xmlQueryString += "&channelId=" + communityId;
						}
						else if (strCurrentPath.EndsWith (".store.xml"))
						{
							rssFeedId = (int) Constants.eRSS_FEEDS.STORE_ITEMS;
							xmlQueryString += "&channelId=" + communityId;
						}
						else if (strCurrentPath.EndsWith (".xml"))
						{
							rssFeedId = (int) Constants.eRSS_FEEDS.WHOLE_CHANNEL;
							xmlQueryString += "&channelId=" + communityId;
						}
						break;
					}
				}

				Context.RewritePath ("~/rssFeed.aspx", Context.Request.PathInfo, "rssFeedId=" + rssFeedId + xmlQueryString);
				return;
			}
            
            
            //
            // Referral & Aquisition Capturing. 
            // Placed after RSS to skip requests for those, and before any redirects.
            //
            // Referrals
            if (Request["ref"] != null && !Request.IsAuthenticated && Request.Cookies["refuser"] == null)
            {
                try
                {
                    int referringUserId = Convert.ToInt32(Request["ref"]);
                    Response.Cookies["refuser"].Value = referringUserId.ToString();
                    Response.Cookies["refuser"].Expires = DateTime.Now.AddYears(1);                    
                    // Store Click
                    if (Request.Cookies["refuser"] != null)
                    {
                        Facade.MarketingFacade marketingFacade = new Facade.MarketingFacade();
                        int addClick = marketingFacade.UpdateShareClick(referringUserId);
                    }
                }
                catch (Exception ex)
                {
                    m_logger.Error(ex.ToString());
                }                
            }

			// Asset deep linking (Does not handle anything that should stay inside a community)
			if (strCurrentPath.IndexOf ("/asset/") > -1)
			{
				// Linking to Asset Details (Not linking to a community)
                if (strCurrentPath.EndsWith(".storeitem") || strCurrentPath.EndsWith(".media"))
                {
                    // Get the asset parameters
                    int assetId = 0;

                    try
                    {
                        assetId = Convert.ToInt32(System.IO.Path.GetFileNameWithoutExtension(strCurrentPath));
                    }
                    catch (Exception) { }

                    string qs = Request.QueryString.ToString();

                    string redirectUrl = siteName + "/asset/assetDetails.aspx?assetId=" + assetId + "&communityId=0";
                    if (qs != null && qs.Length > 0)
                    {
                        redirectUrl += "&" + qs;
                    }

                    Response.Status = status301;
                    Response.AddHeader("Location", redirectUrl);
                    Response.End();

                    return;
                }
                if (strCurrentPath.EndsWith(".vworld"))
                {
                    // Get the asset parameters
                    int itemId = 0;

                    try
                    {
                        itemId = Convert.ToInt32(System.IO.Path.GetFileNameWithoutExtension(strCurrentPath));
                    }
                    catch (Exception) { }

                    string qs = Request.QueryString.ToString();

                    string redirectUrl = siteName + "/asset/wokItemDetails.aspx?itemId=" + itemId;
                    if (qs != null && qs.Length > 0)
                    {
                        redirectUrl += "&" + qs;
                    }

                    Response.Status = status301;
                    Response.AddHeader("Location", redirectUrl);
                    Response.End();

                    return;
                }
            }

			// Community deep linking
			// Handles everything that should stay in a community
            if ((strCurrentPath.IndexOf("/community/") > -1) || (strCurrentPath.IndexOf("/channel/") > -1))
			{
				string tab = "";
				string page = "";
				string qs = "";

                // Added this so we would not do a db call to get community id when this
                // page will never be for a community, only for community and 3dapp search
                if (!strCurrentPath.EndsWith("/channel.kaneva") && !System.IO.Path.GetExtension(strCurrentPath).ToLower().Equals(".aspx") &&
                    !strCurrentPath.EndsWith ("/safety.kaneva") && !strCurrentPath.ToLower ().EndsWith ("/install3d.kaneva") &&
                    !strCurrentPath.ToLower().EndsWith("/careers.kaneva") && !strCurrentPath.ToLower().EndsWith("/sharekaneva.kaneva") &&
                    !strCurrentPath.ToLower().EndsWith("/news.kaneva"))
                {
                    //get the community name from the file path
                    Facade.CommunityFacade cFacade = new Facade.CommunityFacade();
                    commName = System.IO.Path.GetFileNameWithoutExtension(strCurrentPath);

                    // Profiles only uses .people, otherwise it is a community
                    if (strCurrentPath.EndsWith(".people"))
                    {
                        page = "ProfilePage.aspx";
                        communityId = cFacade.GetCommunityIdFromName(commName, true);
                        
                        if (communityId == 0)
                        {
                            Response.Redirect ("~/people/people.kaneva");
                            return;
                        }
                    }
                    else
                    {
                        //regular channel
                        communityId = cFacade.GetCommunityIdFromName(commName, false);

                        if (communityId == 0)
                        {
                            Response.Redirect ("~/community/commDrillDown.aspx");
                            return;
                        }
                    }

                    if (strCurrentPath.EndsWith(".channel"))
                    {
                        page = "CommunityPage.aspx";
                        qs = Request.QueryString.ToString();
                    }
                    else if (strCurrentPath.EndsWith(".forums"))
                    {
                        tab = "forums";
                        page = "commInForum.aspx";
                    }
                    else if (strCurrentPath.EndsWith(".admin"))
                    {
                        tab = "adm";
                        page = "CommunityEdit.aspx";
                    }
                    else if (strCurrentPath.EndsWith(".storeitem") || strCurrentPath.EndsWith(".media"))
                    {
                        // Get the asset parameters
                        int commAssetId = 0;

                        try
                        {
                            commAssetId = Convert.ToInt32(System.IO.Path.GetFileNameWithoutExtension(strCurrentPath));
                        }
                        catch (Exception) { }

                        if (commAssetId > 0)
                        {
                            tab = "sto&assetId=" + commAssetId.ToString();
                            Context.RewritePath("~/asset/assetDetails.aspx", Context.Request.PathInfo, "communityId=0" + qs);
                            return;
                        }
                    }

                    if (communityId > 0)
                    {
                        Context.RewritePath ("~/community/" + page, Context.Request.PathInfo,
                            (qs.IndexOf ("communityId=") < 0 ? "communityId=" + communityId : "") +
                            (qs.Length > 0 ? "&" + qs : ""));
                        return;
                    }
                }
                else if (strCurrentPath.EndsWith("/start3dapp.aspx"))
                {
                    Response.Status = status301;
                    Response.AddHeader("Location", strCurrentPath.Replace("/start3dapp.aspx", "/startworld.aspx"));
                    Response.End();
                }   
			}

			// Linking to blog details
			if (strCurrentPath.EndsWith (".blog"))
			{
                Facade.BlogFacade blogFacade = new Facade.BlogFacade();
				int blogId = 0;

				try 
				{
					blogId = Convert.ToInt32 (System.IO.Path.GetFileNameWithoutExtension (strCurrentPath));
					
					// See if there is a channel id with this blog to pass
                    Blog blog = blogFacade.GetBlog(blogId);

                    Context.RewritePath("~/blog/blogComments.aspx?topicId=" + blogId + "&communityId=" + blog.CommunityId);
					return;
				}
				catch (Exception){}
			}

			// Is it a watch/play/create
			int categoryId = 0;
			if (strCurrentPath.EndsWith (".watch"))
			{
				try 
				{
					categoryId = Convert.ToInt32 (System.IO.Path.GetFileNameWithoutExtension (strCurrentPath));
					Context.RewritePath ("~/watch/media.aspx?cat=" + categoryId.ToString ());
					return;
				}
				catch (Exception){}
			}
            else if (strCurrentPath.EndsWith(".play"))
            {
                try
                {
                    categoryId = Convert.ToInt32(System.IO.Path.GetFileNameWithoutExtension(strCurrentPath));
                    Context.RewritePath("~/play/games.aspx?cat=" + categoryId.ToString());
                    return;
                }
                catch (Exception) { }
            }
            else if (strCurrentPath.EndsWith(".create"))
            {
                try
                {
                    categoryId = Convert.ToInt32(System.IO.Path.GetFileNameWithoutExtension(strCurrentPath));
                    Context.RewritePath("~/create/assets.aspx?cat=" + categoryId.ToString());
                    return;
                }
                catch (Exception) { }
            }

			// Top Tabs
			if (strCurrentPath.EndsWith ("/watch.kaneva"))
			{
				Context.RewritePath ("~/watch/media.aspx");
			}
			else if (strCurrentPath.EndsWith ("/play.kaneva"))
			{
				Context.RewritePath ("~/play/games.aspx");
			}
			else if (strCurrentPath.EndsWith ("/create.kaneva"))
			{
				//Context.Response.Redirect("http://elite.kaneva.com/");
				Response.Status = status301;   
				Response.AddHeader("Location", "http://elite.kaneva.com/");   
				Response.End();
			}
			else if (strCurrentPath.EndsWith ("/home.kaneva"))
			{
				Context.RewritePath ("~/Home.aspx");
			}
            else if (strCurrentPath.EndsWith("/free-virtual-world.kaneva"))
            {
                Context.RewritePath("~/Home.aspx");
            }

			else if (strCurrentPath.EndsWith ("/channel.kaneva"))
			{
				Context.RewritePath ("~/community/commDrillDown.aspx");
                return;
			}
			else if (strCurrentPath.EndsWith ("/mykaneva.kaneva"))
			{
                //Response.Redirect ("~/community/ProfilePage.aspx");
				Response.Status = status301;
                Response.AddHeader("Location", siteName + "/community/ProfilePage.aspx");   
				Response.End();
			}
			else if (strCurrentPath.EndsWith ("/support.kaneva"))
			{
				Context.Response.Redirect("~/default.aspx");
			}	
			else if (strCurrentPath.EndsWith ("/publish.kaneva"))
			{
				Context.RewritePath ("~/asset/publishOverview.aspx");
			}	
			else if (strCurrentPath.EndsWith ("/people.kaneva"))
			{
				Context.RewritePath ("~/people/people.aspx");
			}

            // buy credits link
            else if (strCurrentPath.EndsWith("/buy"))
            {
                Response.Redirect("~/mykaneva/buycredits.aspx");                
            }

            //redirect link for buycredits - temporary until someone has time to change all links in site
            else if (strCurrentPath.EndsWith("/mykaneva/buycredits.aspx"))
            {
                Response.Redirect("~/mykaneva/buyCreditPackages.aspx");
            }

            //redirect link for buyaccess pass - temporary until someone has time to change all links in site
            else if (strCurrentPath.EndsWith("/mykaneva/buyaccess.aspx") || strCurrentPath.EndsWith("/mykaneva/buypasses.aspx"))
            {
                //Response.Redirect("~/mykaneva/buySpecials.aspx");
                Context.RewritePath("~/mykaneva/buySpecials.aspx?pass=true");
            }

            //redirect link for buyaccess pass - temporary until someone has time to change all links in site
            else if (strCurrentPath.EndsWith("/mykaneva/managecredits.aspx"))
            {
                Response.Redirect("~/mykaneva/buyCreditPackages.aspx");
            }
            // for old links to vworld
			else if (strCurrentPath.EndsWith ("/vworld.kaneva"))
			{
                Context.RewritePath("~/3d-virtual-world/vworld.aspx");
                //Context.RewritePath("~/community/CommunityPage.aspx?communityId=1118");
			}

            // vworld content maps
            else if (strCurrentPath.EndsWith("/3d-virtual-world/virtual-life.kaneva"))
            {
                Context.RewritePath("~/3d-virtual-world/vworld.aspx?s=1");
                //Context.RewritePath("~/community/CommunityPage.aspx?communityId=1118");
            }
            else if (strCurrentPath.EndsWith("/3d-virtual-world/news-archive.kaneva"))
            {
                Context.RewritePath("~/3d-virtual-world/vworld.aspx?contentpath=news-archive.html&s=1");
            }
            
            // Homes
            
            else if (strCurrentPath.EndsWith("/3d-virtual-world/3-d-homes.kaneva"))
            {
                Context.RewritePath("~/3d-virtual-world/vworld.aspx?contentpath=member-homes.html&s=3");
            }
           
            // Hangouts
            
            else if (strCurrentPath.EndsWith("/3d-virtual-world/online-avatar-community.kaneva"))
            {
                Context.RewritePath("~/3d-virtual-world/vworld.aspx?contentpath=member-hangouts.html&s=4");
            }

            // Special Features

            else if (strCurrentPath.EndsWith("/3d-virtual-world/special-features.kaneva"))
            {
                Context.RewritePath("~/3d-virtual-world/vworld.aspx?contentpath=special-features/special-features.html&s=6");
            }            
            else if (strCurrentPath.EndsWith("/3d-virtual-world/adult-chat-rooms.kaneva"))
            {
                Context.RewritePath("~/3d-virtual-world/vworld.aspx?contentpath=special-features/access-pass.html&s=6&ss=access-pass");
            }
            else if (strCurrentPath.EndsWith("/3d-virtual-world/virtual-dj.kaneva"))
            {
                Context.RewritePath("~/3d-virtual-world/vworld.aspx?contentpath=special-features/cover-charge.html&s=6&ss=cover-charge");
            }
            else if (strCurrentPath.EndsWith("/3d-virtual-world/virtual-dance-game.kaneva"))
            {
                Context.RewritePath("~/3d-virtual-world/vworld.aspx?contentpath=special-features/dance-party-3d.html&s=6&ss=dance-party");
            }
            else if (strCurrentPath.EndsWith("/3d-virtual-world/animated-avatars.kaneva"))
            {
                Context.RewritePath("~/3d-virtual-world/vworld.aspx?contentpath=special-features/emotes-animations.html&s=6&ss=emotes-animations");
            }
            else if (strCurrentPath.EndsWith("/3d-virtual-world/flash-widgets.kaneva"))
            {
                Context.RewritePath("~/3d-virtual-world/vworld.aspx?contentpath=special-features/flash-widgets.html&s=6&ss=flash-widgets");
            }
            else if (strCurrentPath.EndsWith("/3d-virtual-world/tv-online.kaneva"))
            {
                Context.RewritePath("~/3d-virtual-world/vworld.aspx?contentpath=special-features/tv-channels.html&s=6&ss=tv-channels");
            }
            else if (strCurrentPath.EndsWith("/3d-virtual-world/create-3-d-objects.kaneva"))
            {
                Context.RewritePath("~/3d-virtual-world/vworld.aspx?contentpath=special-features/textrure-patterns.html&s=6&ss=texture-patterns");
            }
            else if (strCurrentPath.EndsWith("/3d-virtual-world/artists.kaneva"))
            {
                Context.RewritePath("~/3d-virtual-world/vworld.aspx?contentpath=special-features/artist.html&s=6&ss=artist");
            }
           
            
            // Contests
            
            else if (strCurrentPath.EndsWith("/3d-virtual-world/kaneva-contest-events.kaneva"))
            {
                Context.RewritePath("~/3d-virtual-world/vworld.aspx?contentpath=contests.html");
            }
            else if (strCurrentPath.EndsWith("/3d-virtual-world/kaneva-contests-events-archive.kaneva"))
            {
                Context.RewritePath("~/3d-virtual-world/vworld.aspx?contentpath=contests-archive.html");
            }
            
            // Places

            else if (strCurrentPath.EndsWith("/3d-virtual-world/world-of-kaneva.kaneva"))
            {
                Context.RewritePath("~/3d-virtual-world/vworld.aspx?contentpath=places-to-go/places-to-go.html&s=5");
            }
            else if (strCurrentPath.EndsWith("/3d-virtual-world/kaneva-help-center.kaneva"))
            {
                Context.RewritePath("~/3d-virtual-world/vworld.aspx?contentpath=places-to-go/help-center.html&s=5&ss=kaneva-help");
            }
            else if (strCurrentPath.EndsWith("/3d-virtual-world/kaneva-city.kaneva"))
            {
                Context.RewritePath("~/3d-virtual-world/vworld.aspx?contentpath=places-to-go/kaneva-city.html&s=5&ss=kaneva-city");
            }
            else if (strCurrentPath.EndsWith("/3d-virtual-world/underground-kaneva-club.kaneva"))
            {
                Context.RewritePath("~/3d-virtual-world/vworld.aspx?contentpath=places-to-go/underground.html&s=5&ss=underground");
            }
            else if (strCurrentPath.EndsWith("/3d-virtual-world/third-dimension-kaneva-dance.kaneva"))
            {
                Context.RewritePath("~/3d-virtual-world/vworld.aspx?contentpath=places-to-go/third-dimension.html&s=5&ss=third-dimension");
            }
            else if (strCurrentPath.EndsWith("/3d-virtual-world/tbs-headquarters.kaneva"))
            {
                Context.RewritePath("~/3d-virtual-world/vworld.aspx?contentpath=places-to-go/tbs-headquarters.html&s=5&ss=tbs-headquarters");
            }
            else if (strCurrentPath.EndsWith("/3d-virtual-world/kaneva-plaza.kaneva"))
            {
                Context.RewritePath("~/3d-virtual-world/vworld.aspx?contentpath=places-to-go/plaza.html&s=5&ss=plaza");
            }
            else if (strCurrentPath.EndsWith("/3d-virtual-world/tnt-backlot-sets.kaneva"))
            {
                Context.RewritePath("~/3d-virtual-world/vworld.aspx?contentpath=places-to-go/tnt-lot.html&s=5&ss=tnt-lot");
            } 


            // fame & fortune
            else if (strCurrentPath.EndsWith ("/3d-virtual-world/world-of-kaneva-fame.kaneva"))
            {
                Context.RewritePath ("~/3d-virtual-world/vworld.aspx?contentpath=special-features/world-of-kaneva-fame.html&s=7&ss=world-of-kaneva-fame");
            }
            else if (strCurrentPath.EndsWith ("/3d-virtual-world/dp3d-dance-fame.kaneva"))
            {
                Context.RewritePath ("~/3d-virtual-world/vworld.aspx?contentpath=special-features/dp3d-dance-fame.html&s=7&ss=dp3d-dance-fame");
            }
            else if (strCurrentPath.EndsWith ("/3d-virtual-world/fame-how-to.kaneva"))
            {
                Context.RewritePath ("~/3d-virtual-world/vworld.aspx?contentpath=special-features/fame-how-to.html&s=7&ss=fame-how-to");
            }
            
            
            // Ka-ching!            
            else if (strCurrentPath.EndsWith("/3d-virtual-world/kaneva-ka-ching.kaneva"))
            {
                Context.RewritePath("~/3d-virtual-world/vworld.aspx?contentpath=ka-ching/kaneva-ka-ching.html&s=6&ss=ka-ching");
            }
            else if (strCurrentPath.EndsWith("/3d-virtual-world/play-ka-ching.kaneva"))
            {
                Context.RewritePath("~/3d-virtual-world/vworld.aspx?contentpath=ka-ching/play-ka-ching.html&s=6&ss=ka-ching");
            }
            else if (strCurrentPath.EndsWith("/3d-virtual-world/ka-ching-leaderboard.kaneva"))
            {
                Context.RewritePath("~/3d-virtual-world/vworld.aspx?contentpath=ka-ching/ka-ching-leaderboard.html&s=6&ss=ka-ching");
            }

            // Kaneva Communities
            else if (strCurrentPath.EndsWith("/sharekaneva.kaneva"))
            {
                Context.RewritePath ("~/share/default.aspx");
            }
            else if (strCurrentPath.EndsWith("/community/install3d.kaneva"))
            {
                Context.RewritePath("~/community/Install3dworld.aspx");
            }
            else if (strCurrentPath.EndsWith("/safety.kaneva"))
            {
                Context.RewritePath("~/community/KanevaCommunity.aspx?communitypath=ParentalResources.htm");
            }
            else if (strCurrentPath.EndsWith("/careers.kaneva"))
            {
                Context.RewritePath("~/community/KanevaCommunity.aspx?communitypath=KanevaCareers.htm");
            }
            else if (strCurrentPath.EndsWith("/news.kaneva"))
            {
                Context.RewritePath("~/community/KanevaCommunity.aspx?communitypath=KanevaMarketing.htm");
            }

            // SEO page changes for redirect to configurable Home page            
            else if (strCurrentPath.EndsWith("/3-d-world.aspx"))
            {
                Context.RewritePath("~/Home.aspx?seo=1");
            }
            else if (strCurrentPath.EndsWith("/virtual-life.aspx"))
            {
                Context.RewritePath("~/Home.aspx?seo=2");
            }
            else if (strCurrentPath.EndsWith("/avatar.aspx"))
            {
                Context.RewritePath("~/Home.aspx?seo=3");
            }
            else if (strCurrentPath.EndsWith("/virtual-games.aspx"))
            {
                Context.RewritePath("~/Home.aspx?seo=4");
            }
            else if (strCurrentPath.EndsWith("/rpg-mmo.aspx"))
            {
                Context.RewritePath("~/Home.aspx?seo=5");
            }
            else if (strCurrentPath.EndsWith("/3d-chat-world.aspx"))
            {
                Context.RewritePath("~/Home.aspx?seo=6");
            }
            else if (strCurrentPath.EndsWith("/online-3d-community.aspx"))
            {
                Context.RewritePath("~/Home.aspx?seo=7");
            }
            else if (strCurrentPath.EndsWith("/artists.aspx"))
            {
                Context.RewritePath("~/Home.aspx?seo=8");
            }
            else if (strCurrentPath.EndsWith("/media-sharing.aspx"))
            {
                Context.RewritePath("~/Home.aspx?seo=9");
            }
            

            else if (strCurrentPath.EndsWith ("/iris"))
            {
                Response.Redirect ("~/default.aspx");
            }
            else if (strCurrentPath.EndsWith ("/sharekaneva"))
            {
                Response.Status = status301;
                Response.AddHeader ("Location", siteName + "/share/default.aspx");
                Response.End ();
            }
            else if (strCurrentPath.EndsWith ("/register.aspx"))
            {
                Response.Status = status301;
                Response.AddHeader ("Location", siteName + System.Configuration.ConfigurationManager.AppSettings["JoinLocation"].Replace ("~", ""));
                Response.End ();
            }
			else if (strCurrentPath.EndsWith("/registerInfo.aspx"))
			{
				Response.Status = status301;
				Response.AddHeader("Location", siteName + System.Configuration.ConfigurationManager.AppSettings["JoinLocation"].Replace("~", ""));
				Response.End();
			}
        }

		/// <summary>
		/// Application_EndRequest
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void Application_EndRequest (Object sender, EventArgs e)
		{
            // Clean up per-request RabbitMQ channel.
            try
            {
                RabbitMQ.Client.IModel channel = RequestRabbitMQChannel(false);
                if (channel != null && channel.IsOpen)
                {
                    channel.Close();
                }
            }
            catch (Exception exc)
            {
                m_logger.Error("Error when attempting to close RabbitMQ channel.", exc);
            }
        }

		/// <summary>
		/// Application_AuthenticateRequest
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void Application_AuthenticateRequest (Object sender, EventArgs e)
		{
			// Get the user information and save it to context for later.
			// This is for speed so we only have to get this data once per request.

            if (Request.IsAuthenticated)
            {
                // Load user data
                Facade.UserFacade userFacade = new Facade.UserFacade();
                User user = userFacade.GetUser (Convert.ToInt32 (User.Identity.Name));
                HttpContext.Current.Items["User"] = user;

                ArrayList ar = UsersUtility.GetUserRoleArray(user.Role);
                string[] roles = (string[])ar.ToArray(Type.GetType("System.String"));

                HttpContext.Current.User = new GenericPrincipal(User.Identity, roles);
            }
            else
            {
                User user = new User();
                HttpContext.Current.Items["User"] = user;
            }
		}

		/// <summary>
		/// Application_Error
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void Application_Error (Object sender, EventArgs e)
		{
			string strURL = Request.Url.ToString ();
            Exception serverError = Server.GetLastError();

            if (serverError is HttpException)
			{
				// Ignore known issue with Cutesoft TextEditor
                if (String.Compare("http://" + KanevaGlobals.SiteName + "/mykaneva/null", strURL, true) != 0)
                {
                    //log the error
                    string urlReferrer = "";
                    if (Request.UrlReferrer != null)
                    {
                        urlReferrer = Request.UrlReferrer.ToString();
                    }
                    m_logger.Warn("HttpException from " + strURL + " [UrlReferrer=" + urlReferrer + "]", serverError);


                    //send to custom page
                    // int errorId = (int)Constants.eERROR_TYPE.WEBPAGE_NOT_FOUND;
                    string errorMessage = "";
                    string strCurrentPath = (Request.Path).ToLower();

                    switch (((HttpException)serverError).GetHttpCode())
                    {
                        case 500:
                            //errorId = (int)Constants.eERROR_TYPE.GENERIC;
                            //errorMessage = ((HttpException)serverError).Message;
                            //redirect to error page with appropriate messaging
          //                  Response.Redirect("~/error.aspx?error=" + errorId + "&message=" + errorMessage);
                            break;
                        case 404:
                            //if error is for a community or profile taylor the message accordingly
                            if (strCurrentPath.EndsWith(".channel"))
                            {
                                //errorId = (int)Constants.eERROR_TYPE.COMMUNITY_NOT_FOUND;
                                errorMessage = System.IO.Path.GetFileNameWithoutExtension(strCurrentPath);
                            }
                            if (strCurrentPath.EndsWith(".people"))
                            {
                                //errorId = (int)Constants.eERROR_TYPE.PROFILE_NOT_FOUND;
                                errorMessage = System.IO.Path.GetFileNameWithoutExtension(strCurrentPath);
                            }
                            //redirect to error page with appropriate messaging
         //                   Response.Redirect("~/error.aspx?error=" + errorId + "&message=" + errorMessage);
                            break;
                        case 401:
                        case 403:
                        default:
                            break;
                    }

                }
			}
			else
			{
				m_logger.Error ("Unhandled application error in " + strURL, Server.GetLastError ());
			}
		}

		/// <summary>
		/// Session_End
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void Session_End (Object sender, EventArgs e)
		{
            Facade.UserFacade userFacade = new Facade.UserFacade();
			Application.Lock (); 
			Application [Constants.CURRENT_USER_COUNT] = Convert.ToInt32 (Application [Constants.CURRENT_USER_COUNT]) - 1;
			Application.UnLock ();

            int loginId = 0;
            if (Session["loginId"] != null)
            {
                loginId = Convert.ToInt32(Session["loginId"]);
            }

			if (Session ["userId"] != null)
			{
                userFacade.UpdateOnlineStatus(Convert.ToInt32(Session["userId"]), 0, Constants.ONLINE_USTATE_OFF, loginId);
			}
		}

		/// <summary>
		/// Application_End
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void Application_End (Object sender, EventArgs e)
		{
            // Cleanup RabbitMQ connection.
            try
            {
                if (rabbitMQConnection != null && rabbitMQConnection.IsOpen)
                {
                    rabbitMQConnection.Close();
                }
            }
            catch (Exception exc)
            {
                m_logger.Warn("Error when attempting to close RabbitMQ connection.", exc);
            }
		}

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// DataBase Utility
		/// </summary>
		private static DatabaseUtility m_DatabaseUtiltyKGP; //this connects to KGP database

		/// <summary>
		/// Logger
		/// </summary>
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

        private static RabbitMQ.Client.IConnection rabbitMQConnection;

        /// <summary>
        /// Returns the RabbitMQ channel for the current request context.
        /// </summary>
        /// <param name="reconnectIfClosed">If true, re-open the request's channel if closed.</param>
        /// <returns>A RabbitMQ channel or null if an error occurs.</returns>
        public static RabbitMQ.Client.IModel RequestRabbitMQChannel(bool reconnectIfClosed = true)
        {
            RabbitMQ.Client.IModel channel = null;

            if (rabbitMQConnection != null && rabbitMQConnection.IsOpen)
            {
                if (HttpContext.Current.Items["rabbitMQChannel"] != null)
                    channel = (RabbitMQ.Client.IModel)HttpContext.Current.Items["rabbitMQChannel"];

                if ((channel == null || channel.IsClosed) && reconnectIfClosed)
                {
                    try
                    {
                        channel = rabbitMQConnection.CreateModel();
                        HttpContext.Current.Items["rabbitMQChannel"] = channel;
                    }
                    catch (Exception) { }
                }
            }

            return channel;
        }
			
		#region Web Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.components = new System.ComponentModel.Container();
		}
		#endregion
	}
}

