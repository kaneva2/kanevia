<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="buyNow.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.buyNow" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:literal id="litJS" runat="server"></asp:literal>

<link href="css/base/base.css" rel="stylesheet" type="text/css" />
<link href="css/facebox.css" rel="stylesheet" type="text/css" />

<div id="BuyNow" runat="server">
	
<asp:updatepanel id="up1" runat="server">
	<contenttemplate>

	<div class="buy" id="divBuyNow" runat="server">
		<div class="imgContainer">
			<img src="~/images/facebox/credits_xlarge_133x115.jpg" runat="server" width="133" height="115" />
		</div>	
		<div class="dataContainer">
			<span>
				<div>This Premium Item costs:</div>					
				<div class="cost" id="divItemCost" runat="server"></div>
				<div>Buy this item?</div>
			</span>
			<div class="buyContainer">				
				<asp:linkbutton class="buttonnarrow" oncommand="btnBuy_Click" id="btnBuy" runat="server" CausesValidation="False"><span class="buyButton" id="spnBuy">Buy Now</span></asp:linkbutton>
			</div>
			<div class="cancelContainer">
				<a class="buttonnarrow" id="btnCancel" href="javascript:parent.$j.facebox.close();"><span class="cancelButton" id="spnCancel">Cancel</span></a>
			</div>							
		</div>
		<div class="disclaimer">*Note:  All Premium Item sales are subject to the Kaneva Terms & Conditions.</div>
	</div>													  
	
	<div class="success" id="divSuccess" runat="server">
		<img id="Img5" src="~/images/facebox/largeSuccess.gif" height="115" width="113" runat="server" />
		<div>Thank you for your purchase!</div>
		<a href="" class="button" id="aClose" onfocus="javascript:amtHasChanged=true;" runat="server" ><span>OK</span></a>

		<script type="text/javascript">
		function IsIE ()
		{					  
			if ((navigator.userAgent.indexOf('MSIE') != -1) && (navigator.userAgent.indexOf('Win') != -1) ||
				(navigator.userAgent.indexOf('Trident') != -1) && (navigator.userAgent.indexOf('Win') != -1))
			{ return true; }
			else
			{ return false;	}
		}
		var amtHasChanged = <asp:literal id="litAmtHasChanged" runat="server" text="false"></asp:literal>;
		var totCredits = '<asp:literal id="litTotCredits" runat="server" text="-1"></asp:literal>';
		var totRewards = '<asp:literal id="litTotRewards" runat="server" text="-1"></asp:literal>';
		function Update (credits, rewards)
		{
			try
			{										
				var $j = jQuery.noConflict();
				if (amtHasChanged)
				{									
					if (IsIE() && totCredits>-1)
					{
						var doc = parent.document;
						var ctrlRewards = $j("#spnRewardsCount_Header",parent.document);
						var ctrlCredits = $j("#spnCreditsCount_Header",parent.document);	
						ctrlRewards.text(rewards);									  
						ctrlCredits.text(credits);		
						
						parent.$j.facebox.close();
					}
					else
					{
						parent.location.replace(parent.location.href);
					}
				}
				else
				{
					parent.$j.facebox.close();
				}
			}
			catch  (err)
			{						   
				// reload parent page
				parent.location.replace(parent.location.href);
			}
		}
		function UpdateAndClose ()
		{												  
			Update (totCredits, totRewards);
		}

		try {document.getElementById('<asp:literal id="litCloseBtnClientId" runat="server"></asp:literal>').focus();}catch(err){}
		</script>
	</div>
	
	<div class="buyCredits" id="divBuyCredits" runat="server">
		<div class="imgContainer">
			<img id="Img1" src="~/images/facebox/credits_xlarge_133x115.jpg" runat="server" width="133" height="115" />
		</div>	
		<div class="dataContainer">
			<span>
				<div>You don't have enough credits for this item.</div>					
			</span>
			<div class="buyContainer">				
				<asp:linkbutton class="buttonFixed" id="btnBuyXtraCredits" onclick="btnBuyXtraCredits_Click" runat="server" CausesValidation="False"><span class="buyButton" id="Span2">Buy More Credits</span></asp:linkbutton>
			</div>
			<a href="javascript:parent.$j.facebox.close();">cancel</a>
		</div>
	</div>
	
	<div class="error" id="divError" runat="server">
		<img id="Img3" src="~/images/facebox/largeAlert.gif" height="115" width="113" runat="server" style="float:left" />
		<p id="errText" runat="server"></p>
		<div class="clearit"></div>
		<div class="cancelContainer">
			<a class="buttonnarrow" id="btnErrClose" href="javascript:parent.$j.facebox.close();"><span class="cancelButton" id="Span1">Cancel</span></a>
		</div>							
	</div>

	</contenttemplate>
</asp:updatepanel>
	
	
<style type="text/css">
body {background-color:#fff;
background-image:none;
margin-top:0;
}
</style>

<script type="text/javascript">	

 var el = document.getElementById('__AjaxCall_Wait');
 if (el != null)
	el.style.display="none";

function SetHeight (ht)
{											
	window.frameElement.style.height = ht; 
}
</script>

</div> 