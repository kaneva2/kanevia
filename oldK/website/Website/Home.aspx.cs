///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
    public partial class Home : System.Web.UI.Page
    {
        #region Declarations
        private int _configuration = 0;
        string _basepath = "";

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.IsAuthenticated)
            {
                Response.Redirect("default.aspx");
            }

            //string _basepath = "http://" + KanevaGlobals.SiteName + "/";//leave in as long as SEO/ search engine indexes are still directing traffic to overview/###.aspx
            _basepath = ResolveUrl("~");

            //sets a custom CSS for this page
            ((homePage)Master).CustomCSS = "<link href=\"" + _basepath + "css/registration_flow/registrationform.css?v8\" rel=\"stylesheet\" type=\"text/css\" />\n" +
                                            "<link href=\"" + _basepath + "css/home/homeBase_a.css?v8\" rel=\"stylesheet\" type=\"text/css\" />\n" +

                                            "<style>#wrapper {background:#000000 url('images/home_signup/image_1920x1280_noElephant.jpg') no-repeat center top fixed; background-size:cover;}</style>\n" +
                                            "<!--[if lte IE 8]><style>.bottom_inner_wrapper { max-width:1920px; } .signupForm input { line-height:24px; }</style><![endif]-->\n";
            ((homePage)Master).CustomJavaScript = "<script type=\"text/javascript\" src=\"" + _basepath + "jscript/selectBox/jquery.selectbox.min.js\"></script>\n" +
                                                    "<script type=\"text/javascript\" src=\"" + _basepath + "jscript/landing_page/register.js?v" + KanevaGlobals.BuildRevision() + "\"></script>\n" +
                                                    "<script type=\"text/javascript\">\n" +
                                                    "    jQuery(document).ready(function () {\n" +
                                                    "        var neededHeight = 778;\n" +
                                                    "        var footerHeight = 234;\n" +
                                                    "        var height = jQuery(window).height();\n" +
                                                    "        var newMinHeight;\n" +
                                                    "        if (height < neededHeight && (neededHeight - height < footerHeight)) {\n" +
                                                    "            newMinHeight = neededHeight + (footerHeight - (neededHeight - height));\n" +
                                                    "            if (newMinHeight > neededHeight) {\n" +
                                                    "                jQuery('#wrapper').css('min-height', newMinHeight + 'px');\n" +
                                                    "            }\n" +
                                                    "        } else {\n" +
                                                    "            jQuery('#wrapper').css('min-height', neededHeight + 'px');\n" +
                                                    "        }\n" +
                                                    "    });\n" +
                                                    "</script>";

            //configure the page for SEO optimization
            GetRequestParams();
            switch (_configuration)
            {
                case 1: //3D Virtual world
                    this.seo1.Attributes.Add("style", "display:block");
                    ((homePage)Master).Title = "Experience Kaneva’s Virtual People Games";
                    ((homePage)Master).MetaDataTitle = "<meta name=\"title\" content=\"Experience Kaneva’s Online 3D World\" />";
                    ((homePage)Master).MetaDataKeywords = "<meta name=\"keywords\" content=\"world, Online virtual world, 3-d virtual world, online worlds\" />";
                    ((homePage)Master).MetaDataDescription = "<meta name=\"description\" content=\"Kaneva’s 3d virtual world lets you customize your 3D avatar, decorate your free 3D home, and it provides an online virtual world experience like no other.\"/>";
                    break;
                case 2: //Virtual Life
                    this.seo2.Attributes.Add("style", "display:block");
                    ((homePage)Master).Title = "Kaneva | A Virtual Life";
                    ((homePage)Master).MetaDataTitle = "<meta name=\"title\" content=\"Kaneva | A Virtual Life\" />";
                    ((homePage)Master).MetaDataKeywords = "<meta name=\"keywords\" content=\"virtual life, create a virtual life, virtual life games, free virtual life games\" />";
                    ((homePage)Master).MetaDataDescription = "<meta name=\"description\" content=\"Create a virtual life. Chat with your friends and meet new people who share your interests.  \"/>";
                    break;
                case 3://Avatar
                    this.seo3.Attributes.Add("style", "display:block");
                    ((homePage)Master).Title = "Create Your Own Free Kaneva Avatar";
                    ((homePage)Master).MetaDataTitle = "<meta name=\"title\" content=\"Create Your Own Free Kaneva Avatar\" />";
                    ((homePage)Master).MetaDataKeywords = "<meta name=\"keywords\" content=\"create an avatar, free avatar creator, avatar worlds, avatar games, cool avatar\" />";
                    ((homePage)Master).MetaDataDescription = "<meta name=\"description\" content=\"Easily create an avatar that’s as distinct and stylish as you are.\"/>";
                    break;
                case 4://Virtual Games
                    this.seo4.Attributes.Add("style", "display:block");
                    ((homePage)Master).Title = "Experience Kaneva’s Virtual People Games";
                    ((homePage)Master).MetaDataTitle = "<meta name=\"title\" content=\"Experience Kaneva’s Virtual People Games\" />";
                    ((homePage)Master).MetaDataKeywords = "<meta name=\"keywords\" content=\"virtual games, virtual reality games, virtual online games, virtual people games\" />";
                    ((homePage)Master).MetaDataDescription = "<meta name=\"description\" content=\"More than a virtual reality game, you can experience an entire virtual world full of exciting people, places, and entertainment.\"/>";
                    break;
                case 5:// Create Your MMO Landing page
                    this.seo5.Attributes.Add("style", "display:block");
                    ((homePage)Master).Title = "Kaneva | Build Your Own MMO";
                    ((homePage)Master).MetaDataTitle = "<meta name=\"title\" content=\"Kaneva | Build Your Own MMO\" />";
                    ((homePage)Master).MetaDataKeywords = "<meta name=\"keywords\" content=\"play rpg games, free mmo, free online rpg, mmog\" />";
                    ((homePage)Master).MetaDataDescription = "<meta name=\"description\" content=\"The ultimate mmog, Kaneva is the hottest new trend in online social entertainment.\"/>";
                    break;
                case 6:// 3D chat Landing
                    this.seo6.Attributes.Add("style", "display:block");
                    ((homePage)Master).Title = "Kaneva’s 3D Animated Chat";
                    ((homePage)Master).MetaDataTitle = "<meta name=\"title\" content=\"Kaneva’s 3D Animated Chat\" />";
                    ((homePage)Master).MetaDataKeywords = "<meta name=\"keywords\" content=\"avatar chat, 3d chat world, chat room, animated chat, emote chat\" />";
                    ((homePage)Master).MetaDataDescription = "<meta name=\"description\" content=\"Dress up your Avatar and Chat with friends in 3D. Free!\"/>";
                    break;
                case 7:// online community
                    this.seo7.Attributes.Add("style", "display:block");
                    ((homePage)Master).Title = "Kaneva  | The Online Virtual Community";
                    ((homePage)Master).MetaDataTitle = "<meta name=\"title\" content=\"Kaneva  | The Online Virtual Community\" />";
                    ((homePage)Master).MetaDataKeywords = "<meta name=\"keywords\" content=\"best online community, online community, online virtual community, 3d community, create online community\" />";
                    ((homePage)Master).MetaDataDescription = "<meta name=\"description\" content=\"\"/>";
                    break;
                case 8:// artists
                    this.seo8.Attributes.Add("style", "display:block");
                    ((homePage)Master).Title = "Artist Network";
                    ((homePage)Master).MetaDataTitle = "<meta name=\"title\" content=\"Artist Network\" />";
                    ((homePage)Master).MetaDataKeywords = "<meta name=\"keywords\" content=\"Artist network, online artist community\" />";
                    ((homePage)Master).MetaDataDescription = "<meta name=\"description\" content=\"Kaneva's growing artist network showcases new and exciting talent. Our online artist community makes it easy for artists to get noticed.\" />";
                    break;
                case 9:// media sharing
                    this.seo9.Attributes.Add("style", "display:block");
                    ((homePage)Master).Title = "Media Sharing Features";
                    ((homePage)Master).MetaDataTitle = "<meta name=\"title\" content=\"Media Sharing Features\" />";
                    ((homePage)Master).MetaDataKeywords = "<meta name=\"keywords\" content=\"Free video sharing, media sharing, photo and video sharing\" />";
                    ((homePage)Master).MetaDataDescription = "<meta name=\"description\" content=\"Our members can take advantage of free video sharing, as well as Flash games and photos. Express yourself with photo and video sharing tools.\" />";
                    break;
                default:
                    this.seo.Attributes.Add("style", "display:block");
                    ((homePage)Master).Title = "Kaneva. Imagine What You Can Do.";
                    ((homePage)Master).MetaDataTitle = "<meta name=\"title\" content=\"Kaneva. A Unique Online Community and 3D Social Network.\" />";
                    ((homePage)Master).MetaDataKeywords = "<meta name=\"keywords\" content=\"free virtual world, virtual world, virtual worlds, 3D world, online avatar community, mmo game, virtual reality, caneva, kaniva\" />";
                    ((homePage)Master).MetaDataDescription = "<meta name=\"description\" content=\"Kaneva is the first to combine social networking and a free 3D virtual world. It's a whole new way to connect with friends. Have fun in a vibrant virtual world full of people, cool places and fun games. Create your avatar and get a free 3D home today! \"/>";
                    break;
            }

            // Analytics
            ((homePage)Master).WebAnalyticsURL = "/home.aspx";

            String strTestDNS;
            strTestDNS = Request.Url.Host + Request.Path;
            strTestDNS = strTestDNS.ToLower();

            int communityId = 0;
            string commName = "";

            if (strTestDNS.EndsWith(".kaneva.com/default.aspx"))
            {
                strTestDNS = strTestDNS.Replace("/default.aspx", "");
            }

            // Make sure they are trying to get to the home page
            // Test for DNS per channel
            if (strTestDNS.EndsWith(".kaneva.com"))
            {
                // Assume Channel Name is the first part before .kaneva.com
                commName = System.IO.Path.GetFileNameWithoutExtension(System.IO.Path.GetFileNameWithoutExtension(strTestDNS));

                // regular channel
                CommunityFacade communityFacade = new CommunityFacade();
                communityId = communityFacade.GetCommunityIdFromName(commName, false);

                if (communityId > 0)
                {
                    Response.Redirect(KanevaGlobals.GetBroadcastChannelUrl(commName));
                    // Issue with RewritePath, need to wait for IIS 7.0. (http://www.codecomments.com/archive289-2005-5-497785.html)
                    //Context.RewritePath ("~/community/CommInInfo.aspx", Context.Request.PathInfo, "ttCat=inCommunity&subtab=inf&communityId=" + communityId);
                    return;
                }
            }

            // Initialize dropdowns
            LoadCountries();
            LoadYears();

            // Set the postback url for the signup form
            string signupUrl = KanevaGlobals.SSLLogin ? "https://" : "http://";
            signupUrl += KanevaGlobals.SiteName + "/register/kaneva/registerInfo.aspx";

            btnRegister.PostBackUrl = btnRegister.PostBackUrl = signupUrl;
        }

        #region Helper Functions

        //get all possible parameters
        private void GetRequestParams()
        {
            //check to see if there is a valid usercontrol id in URL
            try
            {
                _configuration = Convert.ToInt32(Request["seo"].ToString());
            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// Initialize the Country dropbox on the signup form
        /// </summary>
        private void LoadCountries()
        {
            drpCountry.DataTextField = "Name";
            drpCountry.DataValueField = "CountryId";
            drpCountry.DataSource = WebCache.GetCountries();
            drpCountry.DataBind();

            drpCountry.Items.Insert(0, new ListItem("United States", Constants.COUNTRY_CODE_UNITED_STATES));
        }

        /// <summary>
        /// Initialize the years dropdown on the signup form
        /// </summary>
        private void LoadYears()
        {
            int startYear = Convert.ToInt16(DateTime.Now.Year) - 13;

            drpYear.Items.Add(new ListItem(">" + startYear.ToString(), (startYear + 1).ToString()));

            int currentYear = Convert.ToInt16(DateTime.Now.Year) - 13;

            for (int i = currentYear; i > 1940; i--)
            {
                drpYear.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
            drpYear.Items.Add(new ListItem("<1940", "1940"));
            drpYear.SelectedIndex = 0;
        }

        #endregion
    }
}