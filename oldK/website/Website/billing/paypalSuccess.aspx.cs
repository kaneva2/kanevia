///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using System.Net;
using log4net;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for paypalSuccess.
	/// </summary>
	public class paypalSuccess : MainTemplatePage
	{
		public paypalSuccess ()
		{
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			if (IsPostBack)
			{
                Response.Redirect (ResolveUrl ("~/mykaneva/managecredits.aspx"), false);
            }

            try
            {
                // Get the PDT Transaction token
                if (Request["tx"] != null)
                {
                    string transactionToken = Request["tx"].ToString ();

                    // tx, cmd, at
                    // Build the retur Response
                    string formPostData = "cmd=_notify-synch&tx=" + transactionToken + "&at=" + KanevaGlobals.PayPalPDTToken;

                    WebClient client = new WebClient ();
                    client.Headers.Add ("Content-Type", "application/x-www-form-urlencoded");
                    byte[] postByteArray = System.Text.Encoding.ASCII.GetBytes (formPostData);
                    byte[] responseArray = client.UploadData (KanevaGlobals.PayPalURL + "/cgi-bin/webscr", "POST", postByteArray);

                    string response = System.Text.Encoding.ASCII.GetString (responseArray);

                    string itemNumber = "";
                    string itemName = "Unknown";
                    string amount = "Unknown";
                    char[] splitter = { '\n' };
                    char[] splitter2 = { '=' };


                    if (response.StartsWith ("SUCCESS"))
                    {
                        response = response.Substring ("SUCCESS".Length);

                        string[] values = response.Split (splitter);
                        string[] pairs;

                        for (int i = 0; i < values.Length; i++)
                        {
                            pairs = values[i].Split (splitter2);
                            if (pairs.Length == 2)
                            {
                                switch (pairs[0])
                                {
                                    case "item_name":
                                        itemName = System.Web.HttpUtility.UrlDecode (pairs[1]);
                                        break;

                                    case "item_number":
                                        itemNumber = System.Web.HttpUtility.UrlDecode (pairs[1]);
                                        break;

                                    case "mc_gross":
                                        try
                                        {
                                            amount = KanevaGlobals.FormatCurrency (Convert.ToDouble (pairs[1]));
                                        }
                                        catch (Exception exc)
                                        {
                                            m_logger.Error ("Reading paypal amount", exc);
                                        }
                                        break;
                                }
                            }
                        }
                    }

                    lblDescription.Text = itemName;
                    lblAmount.Text = amount;

                    int purchasePointTransactionId = 0;
                    int orderId = 0;

                    // Is it a point bucket purchase?
                    if (itemNumber.StartsWith ("PB"))
                    {
                        // Get the purchase transaction, if they purchased k-points, 
                        try
                        {
                            char[] splitter3 = { '-' };
                            string[] values = itemNumber.Split (splitter3);

                            if (values.Length == 3)
                            {
                                // Get our transactionID
                                purchasePointTransactionId = Convert.ToInt32 (values[1].Substring (3));

                                // Get the user from the transaction id
                                DataRow drPurchasePointTransaction = StoreUtility.GetPurchasePointTransaction (purchasePointTransactionId);
                                if (drPurchasePointTransaction != null)
                                {
                                    orderId = Convert.ToInt32 (drPurchasePointTransaction["order_id"]);
                                }
                            }

                            if (orderId > 0)
                            {
                                // Successfull k-point purchase here
                                MailUtilityWeb.SendKpointPurchaseEmail (KanevaWebGlobals.CurrentUser.UserId, orderId);

                                Response.Redirect (ResolveUrl ("~/checkout/orderComplete.aspx?orderId=" + orderId), false);
                            }
                            else
                            {
                                m_logger.Error ("Error in paypalSuccess : OrderId = " + orderId.ToString () + " and OrderId was expected to be > 0!");
                                Response.Redirect (ResolveUrl ("~/mykaneva/managecredits.aspx"), false);
                            }
                        }
                        catch (Exception exc)
                        {
                            m_logger.Error ("Error in paypalSuccess", exc);
                            Response.Redirect (ResolveUrl ("~/mykaneva/managecredits.aspx"), false);
                        }
                    }
                    else
                    {
                        m_logger.Error ("Return ItemNumber from Paypal did not start with PB.  ItemNumber= " + itemNumber);
                        Response.Redirect (ResolveUrl ("~/mykaneva/managecredits.aspx"), false);
                    }
                }
                else
                {
                    m_logger.Error ("paypalSucess.aspx page was accessed but the transaction code was null");
                    Response.Redirect (ResolveUrl ("~/mykaneva/managecredits.aspx"), false);
                }
            }
            catch (Exception exc)
            {
                m_logger.Error ("Error in paypalSuccess", exc);
                Response.Redirect (ResolveUrl ("~/mykaneva/managecredits.aspx"), false);
            }
		}

		protected Label lblDescription, lblAmount;

		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
