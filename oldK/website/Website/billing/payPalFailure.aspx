<%@ Page language="c#" Codebehind="payPalFailure.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.payPalFailure" %>

<link href="../css/new.css" rel="stylesheet" type="text/css">	

<table cellpadding="0" cellspacing="0" border="0" width="790" align="center">
	<tr>
		<td colspan="3"><img runat="server" src="~/images/spacer.gif" width="1" height="14" id="Img12"/></td>
	</tr>
	<tr>
		<td colspan="3">
			<table  border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="frTopLeft"></td>
					<td class="frTop"></td>
					<td class="frTopRight"></td>
				</tr>
				<tr>
					<td class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" id="Img13" width="1" height="1" /></td>
					<td valign="top" class="frBgIntMembers">
						<table width="100%" border="0" cellpadding="0" cellspacing="0" >
							<tr>
								<td width="770" align="center"  valign="top">
									<br /><br />
									<table cellpadding="0" cellspacing="0" border="0"  width="90%">
										<tr>
											<td valign="top">
												<div><h1>Transaction Cancelled</h1></div>
											</td>
										</tr>
										<tr>
											<td width="790" valign="top">
												
												<table border="0" cellspacing="0" cellpadding="0" width="100%">
													<tr>
														<!-- START WIZARD CONTENT -->
														<td valign="top" align="center">
															
															<table border="0" cellspacing="0" cellpadding="0" width="670" align="center" class="wizform">
																<tr>
																	<td width="670">

																		<br />	
																		<p>Transaction cancelled.  Payment cancelled by the user or there was a problem receiving payment.</p>
																		
																		<div style="text-align:right;"><a href="~/mykaneva/managecredits.aspx" runat="server">Return to Manage Your Credits</a></div>
																		
																	</td>
																</tr>
															</table>
															
														</td>
														
														<!-- END WIZARD CONTENT -->
													</tr>
												</table>
											
											</td>
											
										</tr>
										
									</table>
									<br /><br />		
								</td>
							</tr>
							
						</table>
					</td>
					<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" id="Img15" width="1" height="1" /></td>
				</tr>
				<tr>
					<td class="frBottomLeft"></td>
					<td class="frBottom"></td>
					<td class="frBottomRight"></td>
				</tr>
			</table>				
		</td>
	</tr>
</table>
<br />