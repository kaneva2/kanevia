///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Kaneva.BusinessLayer.Facade;

using System.Net;
using log4net;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for payPalIPN.
	/// </summary>
	public class payPalIPNHandler : System.Web.UI.Page
	{
		private void Page_Load (object sender, System.EventArgs e)
		{
			// Kind of purchase
			bool isPointBucketPurchase = false;
			//bool isPilotLicensePurchase = false;
			bool isCustomKPointPurchase = false;

			string itemNumber = "";
			int purchasePointTransactionId = 0;
			int orderId = 0;
			int pointBucketId = 0;
			int userId = 0;
			//int licenseSubscriptionId = 0;

			string paypalTransactionId = "";
			Double dCustomerPayment = 0.0;

			// REMEMBER, No Session and no user info here!!!!!!!!!!

			// Should never get a post back to this page
			if (IsPostBack)
			{
				return;
			}

			// Step 1a: Modify the POST string.
			string formPostData = "cmd = _notify-validate";
			foreach (String postKey in Request.Form)
			{
				string postValue = Encode (Request.Form [postKey]);
				formPostData += string.Format ("&{0}={1}", postKey, postValue);
			}

			m_logger.Debug ("Incoming message from paypal " + Request.Form.ToString ());

            string response = "Not Set";

			// ***************************************************************************
			// Notification Validation 
			// ***************************************************************************
			// Post the data that was posted to us back to PayPal. 
			// This ensures that the original post indeed came from PayPal, and is not sort of fraud. 
			// In the data that you post back, append the "cmd=_notify-validate" value to the POST string. 
			// Once the data is posted back to PayPal, PayPal will respond with a string of either "VERIFIED" or "INVALID". 
			// "VERIFIED" means that PayPal sent the original post, and that you should continue your automation process 
			// as part of the transaction. "INVALID" means that PayPal did not send the original post, and it should 
			// probably belogged and investigated for possible fraud. 

            try
            {
                WebClient client = new WebClient();
                client.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
                byte[] postByteArray = System.Text.Encoding.ASCII.GetBytes(formPostData);
                byte[] responseArray = client.UploadData(KanevaGlobals.PayPalURL + "/cgi-bin/webscr", "POST", postByteArray);

                response = System.Text.Encoding.ASCII.GetString(responseArray);
            }
            catch (Exception exc)
            {
                try
                {
                    MailUtility.SendEmail("support@kaneva.com", "support@kaneva.com", "PayPal Issue Ignored","This transaction must be verified - " + Request.Form.ToString(), false, false, 1);
                    MailUtility.SendEmail("support@kaneva.com", "jboduch@kaneva.com", "PayPal Issue Ignored", "This transaction must be verified - " + Request.Form.ToString(), false, false, 1);
                }
                catch (Exception) { }

                m_logger.Error("Error in ack to paypal ", exc);

                // ************************************************************************************************
                // For now we are ignoring this error!!!!  Per Animesh, Micheal will manually review these!!!!!!!!
                ///return;
                response = "VERIFIED"; // = Dangerous
                // ************************************************************************************************
            }

            m_logger.Debug("The response after we ack '" + response + "'");

			// Process the response from PayPal.
			switch (response)
			{
				case "VERIFIED":
				{
					if (Request ["item_number"] != null)
					{
						itemNumber = Request ["item_number"].ToString ();
					}

					// ***************************************************************************
					// Get the type of purchase we are processing,
					// along with the variables that go with it
					// ***************************************************************************

					// Is it a point bucket purchase?
					if (itemNumber.StartsWith ("PB"))
					{
						isPointBucketPurchase = true;

						// Get the variables from the item number
						GetPointBucketVariables (ref pointBucketId, ref purchasePointTransactionId, ref userId, ref orderId, itemNumber);
					}
					// Are they purchasing a custom point amount?
					else if (itemNumber.StartsWith ("CA"))
					{
						isCustomKPointPurchase = true;

						// Get the variables from the item number
						GetCustomKPointVariables (ref purchasePointTransactionId, ref userId, ref orderId, itemNumber);
					}
                    //// Are they purchasing a pilot subscription?
                    //else if (itemNumber.StartsWith ("PL"))
                    //{
                    //    isPilotLicensePurchase = true;

                    //    // Get the variables from the item number
                    //    GetPilotLicenseVariables (ref purchasePointTransactionId, ref userId, ref licenseSubscriptionId, itemNumber);
                    //}

					// Get the paypal transaction ID
					if (Request ["txn_id"] != null)
					{
						paypalTransactionId = Request ["txn_id"].ToString ();
					}

					// ***************************************************************************
					// Verify we have the correct variables to proceed
					// ***************************************************************************

					// Make sure we have a point bucket id, transaction id, and user id here!!!!!!!!!
					if (purchasePointTransactionId == 0)
					{
						m_logger.Error ("Paypal transaction "+ paypalTransactionId +" failed because transaction could not be found!!!");
						return;
					}

					// Did we get a valid point bucket?
					if (pointBucketId == 0 && isPointBucketPurchase)
					{
						m_logger.Error ("Paypal transaction "+ paypalTransactionId +" failed because point bucket could not be found!!!");
						StoreUtility.UpdatePointTransaction (purchasePointTransactionId, (int) Constants.eTRANSACTION_STATUS.POINT_BUCKET_NOT_FOUND, "Paypal transaction "+ paypalTransactionId +" failed because point bucket could not be found!!!", paypalTransactionId);
						return;
					}

					// Did we get the user id?
					if (userId == 0)
					{
						m_logger.Error ("Paypal transaction "+ paypalTransactionId +" failed because user could not be looked up!!!");
						StoreUtility.UpdatePointTransaction (purchasePointTransactionId, (int) Constants.eTRANSACTION_STATUS.USER_NOT_VALID, "Paypal transaction "+ paypalTransactionId +" failed because user could not be looked up!!!", paypalTransactionId);
						return;
					}

					// Form variables aer found here?....
					// http://www.sandbox.paypal.com/cgi-bin/webscr?cmd=p/xcl/rec/ipn-techview-outside

					// Payment Status Check 
					// Check that the "payment_status" form field is "Completed". 
					// This ensures that the customer's payment has been processed by PayPal, and it has been added to the seller's account. 
					// payment_status are : Canceled_Reversal, Completed, Denied, Failed, Pending, Refunded, Reversed
					if (Request ["payment_status"] != null)
					{
						if (Request ["payment_status"].Equals ("Completed"))
						{
							// We are OK to continue
							m_logger.Info ("Process the Payment, we received a 'Completed' payment status message");
						}
						else
						{
							int transactionStatus = GetTransactionStatusFromPaypalPaymentStatus (Request ["payment_status"].ToString ());

							string logInfo = "";

							// If pending, pending_reason are
							if (transactionStatus.Equals ((int) Constants.eTRANSACTION_STATUS.PENDING))
							{
								//	address, echeck, intl, multi_currency, unilateral, upgrade, other
								if (Request ["pending_reason"] != null)
								{
									logInfo = " Pending because " + Request ["pending_reason"].ToString ();
								}
							}
							// Reversed or refunded
							else if (transactionStatus.Equals ((int) Constants.eTRANSACTION_STATUS.REFUNDED) || transactionStatus.Equals ((int) Constants.eTRANSACTION_STATUS.PP_REVERSED))
							{
								//	buyer complaint, chargeback, guarante, refund, other
								if (Request ["reason_code"] != null)
								{
									logInfo = " Reversed or refunded because " + Request ["reason_code"].ToString ();
								}
							}

							// Update the transaction status here
							m_logger.Info ("Paypal transaction "+ paypalTransactionId +" was not completed yet. Status is " + Request ["payment_status"].ToString () + logInfo);
							StoreUtility.UpdatePointTransaction (purchasePointTransactionId, transactionStatus, "Paypal transaction "+ paypalTransactionId + " status is " + Request ["payment_status"].ToString () + logInfo, paypalTransactionId);
							return;
						}
					}
					else
					{
						// ***************************************************************************
						// If we are here, we didn't get a payment_status or a txn_type
						// ***************************************************************************
						// Update the transaction status here
						m_logger.Info ("Paypal transaction "+ paypalTransactionId +" was not completed. No payment status received.");
						StoreUtility.UpdatePointTransaction (purchasePointTransactionId, (int) Constants.eTRANSACTION_STATUS.UNKNOWN, "Paypal transaction "+ paypalTransactionId + " was not completed. No payment status received.", paypalTransactionId);
						return;
					}

					// Transaction Duplication Check 
					// In this step, you should check that the "txn_id" form field, transaction ID, has not 
					// already been processed by your automation system. A good thing to do is to store the 
					// transaction ID in a database or file for duplication checking. If the transaction ID 
					// posted by PayPal is a duplicate, you should not continue your automation process for
					// this transaction. Otherwise, this could result in sending the same product to a customer twice. 


					//Seller Email Validation 
					//In this step, you simply make sure that the transaction is for your account. Your account 
					// will have specific email addresses assigned to it. You should verify that the "receiver_email" 
					// field has a value that corresponds to an email associated with your account. 
					// Get the customer payment amount according to paypal
					if (Request ["receiver_email"] != null)
					{
						string receiverEmail = Request ["receiver_email"];
						if (!receiverEmail.Equals (KanevaGlobals.PayPalBusiness))
						{
							m_logger.Error ("Paypal transaction "+ paypalTransactionId +" failed because seller email did not match!!! receiverEmail = " + receiverEmail);
							StoreUtility.UpdatePointTransaction (purchasePointTransactionId, (int) Constants.eTRANSACTION_STATUS.SELLER_EMAIL_MISMATCH, "Paypal transaction "+ paypalTransactionId +" failed because seller email did not match!!! receiverEmail = " + receiverEmail, paypalTransactionId);
							return;
						}
					}

					// Payment Validation, check item and price
					// Because any customer who is familiar with query strings can modify the cost of a seller's product, 
					// you should verify that the "payment_gross" field corresponds with the actual price of the item 
					// that the customer is purchasing. It is up to you to determine the exact price of the item the 
					// customer is purchasing using the form fields. Some common fields you may use to lookup the item
					// being purchased include "item_name" and "item_number".
					// Get the customer payment amount according to paypal
					if (Request ["mc_gross"] != null)
					{
						dCustomerPayment = Convert.ToDouble (Request ["mc_gross"]);

						// Check here against the purchase point transaction gross amount
						DataRow drPurchasePointTransaction = StoreUtility.GetPurchasePointTransaction (purchasePointTransactionId);

						if (drPurchasePointTransaction == null)
						{
							m_logger.Error ("Paypal transaction "+ paypalTransactionId +" failed because transaction could not be looked up!!!");
							StoreUtility.UpdatePointTransaction (purchasePointTransactionId, (int) Constants.eTRANSACTION_STATUS.ITEM_NOT_FOUND, "Paypal transaction "+ paypalTransactionId +" failed because transaction could not be looked up!!!", paypalTransactionId);
							return;
						}
						else
						{
							// Make sure amounts match
							if (!Convert.ToDouble (drPurchasePointTransaction ["amount_debited"]).Equals (dCustomerPayment))
							{
								m_logger.Error ("Paypal transaction "+ paypalTransactionId +" failed because of price mismatch. Paypal = " + dCustomerPayment.ToString () + ", Ours = " + drPurchasePointTransaction ["amount_debited"].ToString ());
								StoreUtility.UpdatePointTransaction (purchasePointTransactionId, (int) Constants.eTRANSACTION_STATUS.PRICE_MISMATCH, "Paypal transaction "+ paypalTransactionId +" failed because of price mismatch. Paypal = " + dCustomerPayment.ToString () + ", Ours = " + drPurchasePointTransaction ["amount_debited"].ToString (), paypalTransactionId);
								return;
							}
						}
					}

					// ***************************************************************************
					// Proceed with processing the payment
					// ***************************************************************************

					// Update the transaction status
					m_logger.Info ("Setting purchasePointTransactionId " + purchasePointTransactionId + " to verified, PayPal transaction Id is " + paypalTransactionId);
					StoreUtility.UpdatePointTransaction (purchasePointTransactionId, (int) Constants.eTRANSACTION_STATUS.VERIFIED, "", paypalTransactionId);

					// Are they purchasing a point bucket?
					if (isPointBucketPurchase)
					{
						m_logger.Info ("Completing point buckert purchase, user = " + userId + ", purchasePointTransactionId = " + purchasePointTransactionId + ", pointBucketId = " + pointBucketId + ", orderId = " + orderId);
						CompletePointBucketPurchase (userId, purchasePointTransactionId, pointBucketId, orderId);
					}
					// Are they purchasing custom k-point amounts?
					else if (isCustomKPointPurchase)
					{
						m_logger.Info ("Completing k-Point purchase, user = " + userId + ", purchasePointTransactionId = " + purchasePointTransactionId + ", orderId = " + orderId);
						CompleteCustomPointPurchase (userId, purchasePointTransactionId, orderId);
					}
                    //// Are they purchasing a pilot licence?
                    //else if (isPilotLicensePurchase)
                    //{
                    //    m_logger.Info ("Completing Pilot License Purchase, user = " + userId + ", purchasePointTransactionId = " + purchasePointTransactionId);
                    //    CompletePilotLicensePurchase (userId, purchasePointTransactionId);
                    //}

					break;
				}
				default:
				{
					// Possible fraud. Log for investigation.
					m_logger.Fatal ("Possible paypal fraud, Bad response from paypal, response = " + response + ". IP Address is " + Common.GetVisitorIPAddress());
					break;
				}
			}
		}

        ///// <summary>
        ///// GetUserLicenseSubscriptionId
        ///// </summary>
        ///// <param name="purchasePointTransactionId"></param>
        ///// <returns></returns>
        //private int GetUserLicenseSubscriptionId (int purchasePointTransactionId)
        //{
        //    DataRow drPuchasePointTransaction = StoreUtility.GetPurchasePointTransaction (purchasePointTransactionId);

        //    if (drPuchasePointTransaction != null)
        //    {
        //        // Game subscription purchase?
        //        if (!drPuchasePointTransaction ["user_license_subscription_id"].Equals (DBNull.Value))
        //        {
        //            // Update the user_license_subscription record
        //            return Convert.ToInt32 (drPuchasePointTransaction ["user_license_subscription_id"]);
        //        }
        //    }

        //    return 0;
        //}

        ///// <summary>
        ///// CompletePilotLicensePurchase
        ///// </summary>
        ///// <param name="userId"></param>
        ///// <param name="purchasePointTransactionId"></param>
        //private void CompletePilotLicensePurchase (int userId, int purchasePointTransactionId)
        //{
        //    int userLicenseSubscriptionId = GetUserLicenseSubscriptionId (purchasePointTransactionId);
        //    if (userLicenseSubscriptionId > 0)
        //    {
        //        StoreUtility.UpdateUserLicenseSubscription (userLicenseSubscriptionId, (int) Constants.eUSER_LICENSE_SUBSCRIPTION_STATUS.ACTIVE, true);
        //    }
        //}

		/// <summary>
		/// CompleteCustomPointPurchase
		/// </summary>
		private void CompleteCustomPointPurchase (int userId, int purchasePointTransactionId, int orderId)
		{
			try
			{
				// Get the k-point amount to credit
				Double kpointAmount = StoreUtility.GetPurchasePointTransactionAmount (purchasePointTransactionId, Constants.CURR_KPOINT);

				// Custom amount
				if (kpointAmount.Equals (0.0))
				{
					// Mark it as item not found
					StoreUtility.UpdatePointTransaction (purchasePointTransactionId, (int) Constants.eTRANSACTION_STATUS.ITEM_NOT_FOUND, "No k-point amount found", "");
					return;
				}

				// Continue with automation processing if all steps succeeded.

				// If credit card passes, credit k-point balance, write transaction record
				//UsersUtility.AddToUserBalance (userId, Constants.CURR_KPOINT, kpointAmount);
				(new UserFacade()).AdjustUserBalance (userId, Constants.CURR_KPOINT, kpointAmount, Constants.CASH_TT_BOUGHT_CREDITS);

				// Where they purchasing assets? This would have meant they tried to purchase assets but need to buy K-points
				// to complete the transaction
				if (orderId > 0)
				{
					CompletePendingOrder (userId, orderId);
				}
			}
			catch (Exception exc)
			{
				m_logger.Error ("Error in CompleteCustomPointPurchase", exc);
			}
		}

		/// <summary>
		/// CompletePointBucketPurchase
		/// </summary>
		private void CompletePointBucketPurchase (int userId, int purchasePointTransactionId, int pointBucketId, int orderId)
		{
			try
			{
				// Update user balances with the K-Point amounts
				Double pointBucketAmount = 0;
				Double pointBucketFreeAmount = 0;
				string pointBucketKEIPoint = Constants.CURR_KPOINT;

                DataRow drPointBucket = StoreUtility.GetPromotion(pointBucketId);

				// No point bucket found here means something messed up somewhere
				if (drPointBucket == null)
				{
					// Mark it as item not found
					StoreUtility.UpdatePointTransaction (purchasePointTransactionId, (int) Constants.eTRANSACTION_STATUS.ITEM_NOT_FOUND, "No Matching Point Bucket Found", "");
				}
				else
				{
					pointBucketKEIPoint = drPointBucket ["kei_point_id"].ToString ();
					pointBucketAmount = Convert.ToDouble (drPointBucket ["kei_point_amount"]);
					pointBucketFreeAmount = Convert.ToDouble (drPointBucket ["free_points_awarded_amount"]);
				}

				// Continue with automation processing if all steps succeeded.

				// If credit card passes, credit k-point balance, write transaction record
				//UsersUtility.AddToUserBalance (userId, pointBucketKEIPoint, pointBucketAmount);
                (new UserFacade()).AdjustUserBalance(userId, pointBucketKEIPoint, pointBucketAmount, Constants.CASH_TT_BOUGHT_CREDITS);

				// Any bonus points?
				if (pointBucketFreeAmount > 0)
				{
                    (new UserFacade()).AdjustUserBalance(userId, drPointBucket["free_kei_point_id"].ToString(), pointBucketFreeAmount, Constants.CASH_TT_BOUGHT_CREDITS);
				}

				// Where they purchasing assets? This would have meant they tried to purchase assets but need to buy K-points
				// to complete the transaction
				if (orderId > 0)
				{
                    DataRow drOrder = StoreUtility.GetOrder (orderId);
                    if (Convert.ToInt32(drOrder["transaction_status_id"]) == (int) Constants.eORDER_STATUS.PENDING_PURCHASE)
                    {
                        if (CompletePendingOrder (userId, orderId) > 0)
                        {
                            // add any extra wok items or access pass here
                            StoreUtility.AddOrderItemsToUserInventory (orderId, pointBucketId, userId, UsersUtility.GetUserGender(userId));

                            // Successfull k-point purchase here
                            MailUtilityWeb.SendKpointPurchaseEmail (userId, orderId);
                        }
                    }
				}
			}
			catch (Exception exc)
			{
				m_logger.Error ("Error in CompletePointBucketPurchase", exc);
			}
		}

		/// <summary>
		/// CompletePendingOrder - this completes a pending order, a pending order is an order that attempted but
		/// they needed purchase some k-Points first.
		/// </summary>
		/// <param name="orderId"></param>
		private int CompletePendingOrder (int userId, int orderId)
		{
			try
			{
				// Mark it as completed, Add the subscriptions, Deduct points
                return StoreUtility.PurchaseOrderNow (userId, orderId, "");
			}
			catch (Exception exc)
			{
				m_logger.Error ("Error in CompletePendingOrder", exc);
                return 0;
			}
		}


		/// <summary>
		/// GetCustomKPointVariables
		/// </summary>
		private void  GetCustomKPointVariables (ref int purchasePointTransactionId, ref int userId, ref int orderId, string itemNumber)
		{
			try
			{
				char [] splitter  = {'-'};
				string [] values = itemNumber.Split (splitter);

				// Format is like this 'CA1000-PPT5' where 1000 is the Custom amount and 5 is the point_transaction_id
				if (values.Length == 2)
				{				
					// Get our transactionID
					purchasePointTransactionId = Convert.ToInt32 (values [1].Substring (3));

					// Get the user from the transaction id
					DataRow drPurchasePointTransaction = StoreUtility.GetPurchasePointTransaction (purchasePointTransactionId);
					if (drPurchasePointTransaction != null)
					{
						userId = Convert.ToInt32 (drPurchasePointTransaction ["user_id"]);
					}
				}
				// Custom amount plus asset or community purchase
				// NOTE : THIS MEANS, the asset is not purchased until they pay here!!!
				// Format is like this 'CA1000-PPT5-PT3'
				// where 1000 is the custom point amount, 5 is the point_transaction_id, 3 is the order id
				else if (values.Length == 3)
				{								
					// Get our transactionID
					purchasePointTransactionId = Convert.ToInt32 (values [1].Substring (3));

					// Get the user from the transaction id
					DataRow drPurchasePointTransaction = StoreUtility.GetPurchasePointTransaction (purchasePointTransactionId);
					if (drPurchasePointTransaction != null)
					{
						userId = Convert.ToInt32 (drPurchasePointTransaction ["user_id"]);
						orderId = Convert.ToInt32 (drPurchasePointTransaction ["order_id"]);
					}
				}
			}
			catch (Exception exc)
			{
				m_logger.Error ("Error in GetCustomKPointVariables",  exc);
			}
		}

		/// <summary>
		/// GetPointBucketVariables
		/// </summary>
		private void GetPointBucketVariables (ref int pointBucketId, ref int purchasePointTransactionId, 
			ref int userId, ref int orderId, string itemNumber)
		{
			try
			{
				char [] splitter  = {'-'};
				string [] values = itemNumber.Split (splitter);

				// Format is like this 'PB3-PPT5' where 3 is the point bucket ID and 5 is the point_transaction_id
				if (values.Length == 2)
				{								
					// Get our transactionID
					purchasePointTransactionId = Convert.ToInt32 (values [1].Substring (3));

					// Get the user from the transaction id
					DataRow drPurchasePointTransaction = StoreUtility.GetPurchasePointTransaction (purchasePointTransactionId);
					if (drPurchasePointTransaction != null)
					{
						userId = Convert.ToInt32 (drPurchasePointTransaction ["user_id"]);
						pointBucketId = Convert.ToInt32 (drPurchasePointTransaction ["point_bucket_id"]);
					}
				}
				// Point bucket plus asset or community purchase
				// NOTE : THIS MEANS, the asset is not purchased until they pay here!!!
				// Format is like this 'PB3-PPT5-PT3'
				// where 3 is the point bucket ID, 5 is the point_transaction_id, 3 is the order id
				else if (values.Length == 3)
				{								
					// Get our transactionID
					purchasePointTransactionId = Convert.ToInt32 (values [1].Substring (3));

					// Get the user from the transaction id
					DataRow drPurchasePointTransaction = StoreUtility.GetPurchasePointTransaction (purchasePointTransactionId);
					if (drPurchasePointTransaction != null)
					{
						userId = Convert.ToInt32 (drPurchasePointTransaction ["user_id"]);
						orderId = Convert.ToInt32 (drPurchasePointTransaction ["order_id"]);
						pointBucketId = Convert.ToInt32 (drPurchasePointTransaction ["point_bucket_id"]);
					}
				}
			}
			catch (Exception exc)
			{
				m_logger.Error ("Error in GetPointBucketVariables", exc);
			}
		}

		/// <summary>
		/// GetPilotLicenseVariables
		/// </summary>
		private void GetPilotLicenseVariables (ref int purchasePointTransactionId, ref int userId, ref int licenseSubscription, string itemNumber)
		{
			try
			{
				char [] splitter2  = {'-'};
				string [] values2 = itemNumber.Split (splitter2);

				// Format is like this 'PL-SUB1-PPT5' where 3 is the point bucket ID, SUB is the license subscription, and 5 is the point_transaction_id
				if (values2.Length == 3)
				{	
					// License subscription ID
					licenseSubscription = Convert.ToInt32 (values2 [1].Substring (3));
				
					// Get our transactionID
					purchasePointTransactionId = Convert.ToInt32 (values2 [2].Substring (3));

					// Get the user from the transaction id
					DataRow drPurchasePointTransaction = StoreUtility.GetPurchasePointTransaction (purchasePointTransactionId);
					if (drPurchasePointTransaction != null)
					{
						userId = Convert.ToInt32 (drPurchasePointTransaction ["user_id"]);
					}
				}
			}
			catch (Exception exc)
			{
				m_logger.Error ("Error in GetPilotLicenseVariables", exc);
			}
		}

		private int GetTransactionStatusFromPaypalPaymentStatus (string paymentStatus)
		{
			switch (paymentStatus)
			{
				case "Canceled_Reversal":
				{
					return (int) Constants.eTRANSACTION_STATUS.PP_CANCELED_REVERSAL;
				}
				case "Denied":
				{
					return (int) Constants.eTRANSACTION_STATUS.DENIED;
				}
				case "Failed":
				{
					return (int) Constants.eTRANSACTION_STATUS.FAILED;
				}
				case "Pending":
				{
					return (int) Constants.eTRANSACTION_STATUS.PENDING;
				}
				case "Refunded":
				{
					return (int) Constants.eTRANSACTION_STATUS.REFUNDED;
				}
				case "Reversed":
				{
					return (int) Constants.eTRANSACTION_STATUS.PP_REVERSED;
				}
				default:
				{
					return (int) Constants.eTRANSACTION_STATUS.UNKNOWN;
				}
			}
		}

		// This helper method encodes a string correctly for an HTTP POST
		private string Encode (string oldValue)
		{
			string newValue = oldValue.Replace ("\"", "'");
			newValue = System.Web.HttpUtility.UrlEncode (newValue);
			newValue = newValue.Replace ("%2f", "/");
			return newValue;
		}

		private ILog m_logger = LogManager.GetLogger ("Billing");

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
