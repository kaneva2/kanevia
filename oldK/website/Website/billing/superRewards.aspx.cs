///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;
using log4net;
using System.Threading;

namespace KlausEnt.KEP.Kaneva
{
    public partial class superRewards : BasePage
    {
        #region Declarations
            uint _swTransactionID = 0;
            uint _creditsAwarded = 0;
            uint _creditsAllTime = 0;
            uint _userId = 0;
            uint _offerIdentifier = 0;
            string _securityHash = "";
            string _superRewardsKey = "";
            int _wokTransactionLogID = 0;
            int result = 0;
            SuperRewardsTransaction srt = new SuperRewardsTransaction();
            private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            //get parameters validate and process transaction
            if (GetRequestParams() && ValidateTransaction())
            {
                try
                {
                    //check to see if it was already rewarded
                    int numberRewarded = (new TransactionFacade()).SuperRewardsCreditAlreadyGiven(_swTransactionID);

                    if (numberRewarded == 0)
                    {
                        //add credits to users account
                        result = GetUserFacade.AdjustUserBalance((int)_userId, "KPOINT", (double)_creditsAwarded, (ushort)Constants.CASH_TT_SUPER_REWARDS, ref _wokTransactionLogID);

                        if (_wokTransactionLogID > 0)
                        {
                            result = (int)SuperRewardsTransaction.eSR_AWARD_STATUS.SUCCEED;
                        }
                        else
                        {
                            result = (int)SuperRewardsTransaction.eSR_AWARD_STATUS.FAIL;
                        }
                    }
                    else
                    {
                        //indicate that it has already been successfully awarded
                        result = (int)SuperRewardsTransaction.eSR_AWARD_STATUS.ALREADY_REWARDED;
                    }
                }
                catch (Exception ex)
                {
                    m_logger.Warn("Error in super rewards: unable to adjust user balance", ex);
                }
            }

            //write transaction to log
            try
            {
                //create super rewards object
                srt.AwardStatus = (short)result;
                srt.SuperRewardsTransactionID = _swTransactionID;
                srt.CreditsAwarded = _creditsAwarded;
                srt.CreditsAllTime = _creditsAllTime;
                srt.UserID = _userId;
                srt.SuperRewardsOfferID = _offerIdentifier;
                srt.SecurityKey = _securityHash;
                srt.RequestingIPAddress = Common.GetVisitorIPAddress();
                srt.WokTransactionLogID = (uint)_wokTransactionLogID;

                //insert record into table
                GetTransactionFacade.LogSuperRewardsTransaction(srt);

                //create new thread and execute backup
                Thread backUp = new Thread(new ThreadStart(WriteRewardsToSQLServer));
                backUp.Priority = ThreadPriority.Normal;
                backUp.Start();

            }
            catch (Exception ex)
            {
                m_logger.Warn("Error in super rewards: items sent in URL: " + _swTransactionID + ":" + _creditsAwarded + ":" + _userId + ":" + _superRewardsKey + ":" + _securityHash,ex);
            }

            //send success post
            if ((result == (int)SuperRewardsTransaction.eSR_AWARD_STATUS.SUCCEED) || (result == (int)SuperRewardsTransaction.eSR_AWARD_STATUS.ALREADY_REWARDED))
            {
                //send success post
                Response.Clear();
                Response.Write("1");
            }
            else
            {
                //send failed post
                Response.Clear();
                Response.Write("0");
            }

        }

        #region Helper Methods

        //backs up superrewards data to a separate database
        private void WriteRewardsToSQLServer()
        {
            try
            {
                (new TransactionFacade()).LogSuperRewardsTransaction_SQLSrv(srt);
            }
            catch (Exception ex)
            {
                m_logger.Warn("Error in super rewards: back up to SQL Server failed: " + srt.SuperRewardsTransactionID + ":" + srt.CreditsAwarded + ":" + srt.UserID + ":" + srt.SecurityKey, ex);
            }
        }

        //checks to see if another attempt is being made to get credits for a transaction that 
        //has already bee successfully awarded
        private bool CheckForDuplicateAwardAttempt()
        {
            _superRewardsKey = Configuration.SuperRewardsKey;
            string HashComponents = _swTransactionID + ":" + _creditsAwarded + ":" + _userId + ":" + _superRewardsKey;
            string MD5Hash = UsersUtility.MakeHash(HashComponents);
            return _securityHash.Equals(MD5Hash);
        }


        private bool ValidateTransaction()
        {
            _superRewardsKey = Configuration.SuperRewardsKey;
            string HashComponents = _swTransactionID + ":" + _creditsAwarded + ":" + _userId + ":" + _superRewardsKey;
            string MD5Hash = UsersUtility.MakeHash(HashComponents);

            bool isValidated = _securityHash.Equals(MD5Hash);

            //sets the result to indicate that it failed security check
            if (!isValidated)
            {
                result = (int)SuperRewardsTransaction.eSR_AWARD_STATUS.SECURITY_FAILURE;
            }
            return isValidated;
        }

        //get all possible parameters
        private bool GetRequestParams()
        {
            bool requiredParamsGiven = true;

            //check to see if there is a transaction id provided
            if (Request["id"] != null)
            {
                _swTransactionID = Convert.ToUInt32(Request["id"].ToString());
            }
            else
            {
                requiredParamsGiven = false;
                result = (int)SuperRewardsTransaction.eSR_AWARD_STATUS.INVALID_PARAMETERS;
            }

            //check to see if the credits earned were provided
            try
            {
                if (Request["new"] != null)
                {
                    _creditsAwarded = Convert.ToUInt32(Request["new"].ToString());
                }
                else
                {
                    requiredParamsGiven = false;
                    result = (int)SuperRewardsTransaction.eSR_AWARD_STATUS.INVALID_PARAMETERS;
                }
            }
            catch (FormatException)
            {
            }

            //check to see if the total credits purchased to date wasa provided
            try
            {
                if (Request["total"] != null)
                {
                    _creditsAllTime = Convert.ToUInt32(Request["total"].ToString());
                }
            }
            catch (FormatException)
            {
            }

            //check to see if user id was returned
            try
            {
                if (Request["uid"] != null)
                {
                    _userId = Convert.ToUInt32(Request["uid"].ToString());
                }
                else
                {
                    requiredParamsGiven = false;
                    result = (int)SuperRewardsTransaction.eSR_AWARD_STATUS.INVALID_PARAMETERS;
                }
            }
            catch (FormatException)
            {
            }

            //check to see if offer identifier was returned
            if (Request["oid"] != null)
            {
                _offerIdentifier = Convert.ToUInt32(Request["oid"].ToString());
            }

            //check to see if there is a validation key sent back
            if (Request["sig"] != null)
            {
                _securityHash = Request["sig"].ToString();
            }
            else
            {
                requiredParamsGiven = false;
                result = (int)SuperRewardsTransaction.eSR_AWARD_STATUS.INVALID_PARAMETERS;
            }

            return requiredParamsGiven;
        }

        #endregion

    }
}
