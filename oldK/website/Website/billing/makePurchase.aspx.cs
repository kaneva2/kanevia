///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for makePurchase.
	/// </summary>
	public class makePurchase : MainTemplatePage
	{
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Currently Billing is disabled, except for admin testing
            if (!KanevaGlobals.EnablePaypal)
            {
                if (!IsAdministrator ())
                {
                    return;
                }
            }

			string link = "";
			string successURL = Server.HtmlEncode ("http://" + KanevaGlobals.SiteName + "/billing/payPalSuccess.aspx");
			string failureURL = Server.HtmlEncode ("http://" + KanevaGlobals.SiteName + "/billing/payPalFailure.aspx");

			//int transactionId;
			string itemName="error";
            string itemNumber = "error";
            string amount = "error";

			int orderId = 0;
            int promotion_id = 0;
            int iPointTransactionId = 0;

			// Buy Point Bucket
            if ((Request["pb"] != null) && (Request["pOID"] != null))
			{
                try
                {
                    //get the request parameters
                    promotion_id = Convert.ToInt32(Request["pb"]);
                    orderId = Convert.ToInt32(Request["pOID"]);


                    // get the order information
                    DataRow drOrder = StoreUtility.GetOrder (orderId, KanevaWebGlobals.CurrentUser.UserId);

                    //get the purchase point id
                    iPointTransactionId = Convert.ToInt32(drOrder["point_transaction_id"]);

                    //get the purchase point information
                    DataRow drPPT = StoreUtility.GetPurchasePointTransaction(iPointTransactionId);

				    // Get the promotion information
                    DataRow drPointBucket = StoreUtility.GetPromotion(promotion_id);

                    //populate the parameter information
                    itemName = Server.HtmlEncode("Kaneva " + drPointBucket["bundle_title"].ToString());

                    amount = Server.HtmlEncode(Convert.ToDouble(drPPT["amount_debited"]).ToString());

                    itemNumber = Server.HtmlEncode("PB" + drPointBucket["promotion_id"].ToString());

                    // Add our transaction id to the item number
                    itemNumber += "-PPT" + drOrder["point_transaction_id"].ToString ();

                    // Add order id to the item number
                    itemNumber += "-PT" + orderId.ToString ();

                    StoreUtility.UpdatePointTransaction(iPointTransactionId, (int)Constants.eTRANSACTION_STATUS.WAITING_VERIFICATION, "", "");
                }
                catch (Exception)
                {}

                link = KanevaGlobals.PayPalURL + "/xclick/business=" + Server.HtmlEncode (KanevaGlobals.PayPalBusiness) + "&item_name=" + itemName + "&item_number=" + itemNumber + "&amount=" + amount + "&no_shipping=1&return=" + successURL + "&cancel_return=" + failureURL + "&no_note=1&currency_code=USD";
			}
			// Buying a custom point amount?
/*			else if (Request ["kpAmount"] != null)
			{
				// Get the amount of custom k-points to purchase
				Double pAmount = Convert.ToDouble (Request ["kpAmount"]);

				itemName = Server.HtmlEncode (KanevaGlobals.FormatKPoints (pAmount) + " K-Points");
				itemNumber = Server.HtmlEncode ("CA" + pAmount.ToString ());
				amount = Server.HtmlEncode (StoreUtility.ConvertKPointsToDollars (pAmount).ToString ());

				// Record the transaction in the database, marked as pending
				transactionId = StoreUtility.PurchasePoints (KanevaWebGlobals.GetUserId (), (int) Constants.eTRANSACTION_STATUS.WAITING_VERIFICATION, itemName, 0, (int) Constants.ePAYMENT_METHODS.PAYPAL, 
					StoreUtility.ConvertKPointsToDollars (pAmount), pAmount, 0, 0, Common.GetVisitorIPAddress());

				// Add our transaction id to the item number
				itemNumber = itemNumber + "-PPT" + transactionId.ToString ();

				// Are they also trying to purchase a asset(s) they did not have enough K-Points for?
				if (Request ["pOID"] != null)
				{
					// Add this to the item name
					orderId = Convert.ToInt32 (Request ["pOID"]);
					itemNumber = itemNumber + "-PT" + orderId.ToString ();
					StoreUtility.UpdatePointTransaction (transactionId, orderId);
					StoreUtility.UpdateOrderPointTranasactionId (orderId, transactionId);
				}

				// Are they also trying to purchase a community they did not have enough K-Points for?
				if (Request ["pCommOID"] != null)
				{
					// Add this to the item name
					purchaseCommunityOrderId = Convert.ToInt32 (Request ["pCommOID"]);
					itemNumber = itemNumber + "-PT" + purchaseCommunityOrderId.ToString ();
					StoreUtility.UpdatePointTransaction (transactionId, purchaseCommunityOrderId);
					StoreUtility.UpdateOrderPointTranasactionId (purchaseCommunityOrderId, transactionId);
				}

				link = KanevaGlobals.PayPalURL + "/xclick/business=" + Server.HtmlEncode (KanevaGlobals.PayPalBusiness) + "&item_name=" + itemName + "&item_number=" + itemNumber + "&amount=" + amount + "&no_shipping=1&return=" + successURL + "&cancel_return=" + failureURL + "&no_note=1&currency_code=USD";
			}
			// Buy Subscription
			else if (Request ["sub"] != null)
			{
				// ARe they buying a pilot license?
				if (Request ["sub"].Equals ("pilot"))
				{
					int userId;

					// Get the license subscriptions
					DataRow drLicenseSubscription = StoreUtility.GetLicenseSubscription ((int) Constants.eLICENSE_SUBSCRIPTIONS.PILOT);

					// It is a new user registering
					if (Request ["newUserId"] != null)
					{
						userId = Convert.ToInt32 (Request ["newUserId"]);
						successURL = Server.HtmlEncode ("http://" + KanevaGlobals.SiteName + "/registerComplete.aspx");
					}
					else
					{
						userId = KanevaWebGlobals.GetUserId ();
					}
					
					itemName = Server.HtmlEncode ("Enhanced Account");
					itemNumber = Server.HtmlEncode ("PL-SUB" + (int) Constants.eLICENSE_SUBSCRIPTIONS.PILOT);
					amount = Server.HtmlEncode (StoreUtility.ConvertKPointsToDollars (Convert.ToDouble (drLicenseSubscription ["amount"])).ToString ("#0.00"));

					// Record a User_License_subscription
					int userLicenseSubscriptionId = StoreUtility.InsertUserLicenseSubscription (userId, (int) Constants.eLICENSE_SUBSCRIPTIONS.PILOT, "Y", (int) Constants.eUSER_LICENSE_SUBSCRIPTION_STATUS.PENDING);

					// Record the transaction in the database, marked as pending
					transactionId = StoreUtility.PurchasePoints (userId, (int) Constants.eTRANSACTION_STATUS.WAITING_VERIFICATION, itemName, 0, (int) Constants.ePAYMENT_METHODS.PAYPAL, 
						Convert.ToDouble (amount), 0, 0, userLicenseSubscriptionId, Common.GetVisitorIPAddress());

					// Add our transaction id to the item number
					itemNumber = itemNumber + "-PPT" + transactionId.ToString ();

					// Reattemp? sra=1 means reattempt on failure based on paypal's rules, remove this if we DO NOT want to reattempt
					//string strReattemp = "&sra=1";  
					string strReattemp = "";  
					string strAmount = "&a3=" + amount;
					string strCycle = "&t3=" + StoreUtility.GetPayPalLengthCodes (Convert.ToInt32 (drLicenseSubscription ["length_of_subscription"]));

					// no_note=1 means not promted for a note
					// p3=1 means length of each billing cycle
					// a3=10.00 means the subscription is 10 dollars
					// t3=M is monthy, t3=D is daily, W Weekly, Y Yearly
					// src=1 means it is a recurring subscription

					link = KanevaGlobals.PayPalURL + "/subscriptions/business=" + Server.HtmlEncode (KanevaGlobals.PayPalBusiness) + "&item_name=" + itemName + "&item_number=" + itemNumber + "&no_shipping=1&return=" + successURL + "&cancel_return=" + failureURL + "&no_note=1&currency_code=USD" + strAmount + "&p3=1" + strCycle + "&src=1" + strReattemp;

					// Cancel code
					// https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_subscr-find&alias=test%40test222.com

				}

			}
*/
			// Page to make a paypal purchase
			Response.Redirect (link);

			// EMAIL URL
			//https://www.sandbox.paypal.com/xclick/business=

			// FORM URL
			// https://www.sandbox.paypal.com/cgi-bin/webscr

		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
