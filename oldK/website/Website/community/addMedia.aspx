<%@ Page language="c#" Codebehind="addMedia.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.addMedia" %>

<link href="../css/home.css" rel="stylesheet" type="text/css" />		
<link href="../css/kanevaSC.css" rel="stylesheet" type="text/css"/>
<link href="../css/friends.css" rel="stylesheet" type="text/css"/>
<link href="../css/kanevaText.css" type="text/css" rel="stylesheet">

<table width="100%"  border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="1" height="15"></td>
	</tr>
</table>
<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td>
			<asp:ValidationSummary ShowMessageBox="False" ShowSummary="True" Class="formError" id="valSum" DisplayMode="BulletList" runat="server" HeaderText="Please correct the following errors:"/>
		</td>		
	</tr>
</table>
<table width="990" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td width="6" class="frTopLeft"></td>
		<td colspan="3" class="frTop"></td>
		<td width="7" class="frTopRight"></td>
	</tr>
	<tr> 
		<td rowspan="3" bgcolor="#f1f1f2" class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1"></td>
		<td width="10" rowspan="3" valign="top" bgcolor="#f1f1f2" class="frBgInt1">
			
		</td>
		<td height="50" valign="middle" class="frBgInt1">
			<img id="imgMediaLibrary" runat="server" src="~/images/titles/add_to_channel.gif">
			
			<br/><br/>
			<table cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr>
					<td class="insideBoxText11"> 
						Add media to the <b><asp:Label id="lblChannel" runat="server"/></b> Community<br/>
						Select media items below
						<br/><br/>
					</td>
					<td align="right">
						
						<img runat="server" src="~/images/spacer.gif" width="10" height="6"/></td>
				</tr>
			</table>
			
			
		</td>
		<td width="10" rowspan="3" valign="top" bgcolor="#f1f1f2" class="frBgInt1"></td>
		<td rowspan="3" class="frBorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1"></td>
	</tr>
	<tr>
		<td valign="top" bgcolor="#f1f1f2" class="frBgInt1">
					

			<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
				<tr>
					<td height="7" class="intBorderCTopLeft"></td>
					<td colspan="8" class="intBorderCTop"></td>
					<td class="intBorderCTopRight"></td>
				</tr>
				<tr>
					<td height="20" class="intBorderCBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1"></td>
					<td valign="middle" align="center" class="intBorderCBgInt1"><input type="checkbox" class="formKanevaCheck" id="chkSelect" onclick="javascript:SelectAll(this,this.checked);"/></td>
					<td width="1" class="sepTit1"><img runat="server" src="~/images/spacer.gif" width="1" height="1"></td>
					<td width="1"  class="intBorderCBorderLeft"></td>
					<td width="1" class="sepTit1"><img runat="server" src="~/images/spacer.gif" width="1" height="1"></td>
					<td valign="top" class="intBorderCBgInt1" width="24">&nbsp;</td>			
					<td colspan="3" valign="middle" class="intBorderCBgInt1" align="left"><span class="content">Media Name</span></td>
					<td class="intBorderCBorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1"></td>
				</tr>
				<tr>
					<td height="7" class="intBorderCBottomLeft"></td>
					<td colspan="8" class="intBorderCBottom"></td>
					<td class="intBorderCBottomRight"></td>
				</tr>
				
				
				<asp:Repeater runat="server" id="rptAssets">  
					<ItemTemplate>
						<tr class="intColor1">
							<td height="125" class="intBorderC1BorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="110" /></td>
							<td align="center" class="intColor1">
								<asp:checkbox id="chkEdit" CssClass="Filter2" runat="server"/>						
								<input type="hidden" runat="server" id="hidAssetId" value='<%#DataBinder.Eval(Container.DataItem, "asset_id")%>'> 
							</td>
							<td width="1" class="sepInt1"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
							<td align="center" class="intColor1">
								<!--image holder-->
								<table border="0" cellpadding="0" cellspacing="0" width="94" height="94" align="center">
									<tr>
										<td class="boxWhiteTopLeft"><img runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
										<td class="boxWhiteTop" ><img runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
										<td class="boxWhiteTopRight"><img runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
									</tr>
									<tr>
										<td class="boxWhiteLeft" ><img runat="server" src="~/images/spacer.gif" width="1" height="4" /></td>
										<td class="boxWhiteContent" height="80" align="center" valign="middle">
											<a href='<%# GetAssetDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "asset_id")))%>'><img border="0" style="margin: 10px;" src='<%#GetMediaImageURL (DataBinder.Eval(Container.DataItem, "thumbnail_medium_path").ToString () ,"me", Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_id")), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_type_id")), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "thumbnail_gen")))%>' border="0"/></a>
										</td>
										<td class="boxWhiteRight"><img runat="server" src="~/images/spacer.gif" width="1" height="4"/></td>
									</tr>
									<tr>
										<td class="boxWhiteBottomLeft" ><img runat="server" src="~/images/spacer.gif" width="4" height="4" /></td>
										<td class="boxWhiteBottom"><img runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
										<td class="boxWhiteBottomRight"><img runat="server" src="~/images/spacer.gif" width="4" height="4" /></td>
									</tr>
								</table>								
								<asp:Label ID="lblMessage" runat="server" />
								<!--end image holder-->			   
							</td>
							<td width="1" class="sepInt1"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
							<td valign="top" class="intColor1"><br /></td>
							<td valign="middle" class="intColor1" align="left">
								<span class="insideTextNoBold">Name:</span>&nbsp;<a style="font-size:110%" href='<%# GetAssetDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "asset_id")))%>'><%# TruncateWithEllipsis (Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "name").ToString ()), 40) %></a>
								<br/><span class="insideTextNoBold">Owner:</span>&nbsp;<a class="insideTextNoBold" href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "username").ToString ())%>><%# DataBinder.Eval(Container.DataItem, "username") %></a>
								<br/><span class="insideTextNoBold">Status:&nbsp;<A runat="server" HREF='#' id="hlStatus" /></span><span class="insideTextNoBold">
								<br/>
								<br/><asp:Label ID="lblTeaser" runat="server"/></span><span class="insideTextNoBold">&nbsp;
								<br /><br />Type:&nbsp;<%# GetAssetTypeName(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "asset_type_id")))%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Size: <%# FormatImageSize (DataBinder.Eval (Container.DataItem, "file_size")) %></span><span class="insideTextNoBold"> <br />Published: <%# FormatDateTime (DataBinder.Eval (Container.DataItem, "created_date")) %></span><br/>
								<span class="insideTextNoBold"> Communities: <asp:label id="lblChannels" runat="server"/></span>
							</td>
							<td valign="top" class="intColor1"></td>
							<td valign="middle" class="intColor1">
								
							</td>
							<td class="intBorderC1BorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
		  				</tr>
					</ItemTemplate>
					<AlternatingItemTemplate>
						<tr class="intColor2">
							<td height="125" class="intBorderC1BorderLeftColor1"><img runat="server" src="~/images/spacer.gif" width="1" height="110" /></td>
							<td align="center" class="intColor2">
								<asp:checkbox id="chkEdit" CssClass="Filter2" runat="server"/>						
								<input type="hidden" runat="server" id="hidAssetId" value='<%#DataBinder.Eval(Container.DataItem, "asset_id")%>'> 
							</td>
							<td width="1" class="sepInt1"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
							<td align="center" class="intColor2">
								<!--image holder-->
								<table border="0" cellpadding="0" cellspacing="0" width="94" height="94" align="center">
									<tr>
										<td class="boxWhiteTopLeft"><img runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
										<td class="boxWhiteTop" ><img runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
										<td class="boxWhiteTopRight"><img runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
									</tr>
									<tr>
										<td class="boxWhiteLeft" ><img runat="server" src="~/images/spacer.gif" width="1" height="4" /></td>
										<td class="boxWhiteContent" height="80" align="center" valign="middle">
											<a href='<%# GetAssetDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "asset_id")))%>'><img border="0" style="margin: 10px;" src='<%#GetMediaImageURL (DataBinder.Eval(Container.DataItem, "thumbnail_medium_path").ToString () ,"me", Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_id")), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_type_id")), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "thumbnail_gen")))%>' border="0"/></a>
										</td>
										<td class="boxWhiteRight"><img runat="server" src="~/images/spacer.gif" width="1" height="4"/></td>
									</tr>
									<tr>
										<td class="boxWhiteBottomLeft" ><img runat="server" src="~/images/spacer.gif" width="4" height="4" /></td>
										<td class="boxWhiteBottom"><img runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
										<td class="boxWhiteBottomRight"><img runat="server" src="~/images/spacer.gif" width="4" height="4" /></td>
									</tr>
								</table>								
								<asp:Label ID="lblMessage" runat="server" />
								<!--end image holder-->			   
							</td>
							<td width="1" class="sepInt1"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
							<td valign="top" class="intColor2"><br /></td>
							<td valign="middle" class="intColor2" align="left">
								<span class="insideTextNoBold">Name:</span>&nbsp;<a style="font-size:110%" href='<%# GetAssetDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "asset_id")))%>'><%# TruncateWithEllipsis (Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "name").ToString ()), 40) %></a>
								<br/><span class="insideTextNoBold">Owner:</span>&nbsp;<a class="insideTextNoBold" href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "username").ToString ())%>><%# DataBinder.Eval(Container.DataItem, "username") %></a>
								<br/><span class="insideTextNoBold">Status:&nbsp;<A runat="server" HREF='#' id="hlStatus" /></span><span class="insideTextNoBold">
								<br/>
								<br/><asp:Label ID="lblTeaser" runat="server"/></span><span class="insideTextNoBold">&nbsp;
								<br /><br />Type:&nbsp;<%# GetAssetTypeName(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "asset_type_id")))%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Size: <%# FormatImageSize (DataBinder.Eval (Container.DataItem, "file_size")) %></span><span class="insideTextNoBold"> <br />Published: <%# FormatDateTime (DataBinder.Eval (Container.DataItem, "created_date")) %></span><br/>
								<span class="insideTextNoBold"> Communities: <asp:label id="lblChannels" runat="server"/></span>
							</td>
							<td valign="top" class="intColor2"></td>
							<td valign="middle" class="intColor2">

							</td>
							<td class="intBorderC1BorderRightColor2"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
		  				</tr>
					</AlternatingItemTemplate>
				</asp:Repeater>
			
				<tr>
					<td height="15" class="intBorderC1BorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1"/></td>
					<td colspan="6" class="intColor1" align="left">
						
					</td>
					<td colspan="2" class="intColor1" align="right">
						<asp:button id="btnAddToChannels" runat="Server" CausesValidation="True" onClick="btnUpdate_Click" class="Filter2" Text="  Add Selected Media to Community  "/>
					
					</td>
					<td class="intBorderC1BorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1"/></td>
				</tr>
				
				<tr>
					<td height="7" class="intBorderC1BottomLeft"></td>
					<td colspan="8" class="intBorderC1Bottom"></td>
					<td class="intBorderC1BottomRight"></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" bgcolor="#f1f1f2"><img runat="server" src="~/images/spacer.gif" width="1" height="10"></td>
	</tr>
	<tr>
		<td class="frBottomLeft"></td>
		<td colspan="3" class="frBottom"></td>
		<td class="frBottomRight"></td>
	</tr>
</table>