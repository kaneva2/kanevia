///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva.channel;
using KlausEnt.KEP.Kaneva.framework.widgets;
using log4net;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for commEdit.
	/// </summary>
	public class commEdit : MainTemplatePage
	{
		protected commEdit () 
		{
			Title = "Community Edit";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			// They must be logged in
			if (!Request.IsAuthenticated)
			{
				Response.Redirect (GetLoginURL ());
				return;
			}

			// Enable Max Lengths of TextAreas
			txtDescription.Attributes.Add ("MaxLength", "255");
			if (!ClientScript.IsClientScriptBlockRegistered (GetType (), "taMaxLength"))
			{
				string strScript = "<script language=JavaScript> addEvent(window, \"load\", textAreasInit);</script>";
				ClientScript.RegisterClientScriptBlock (GetType (), "taMaxLength", strScript);
			}

            int communityId = GetCommunityId ();

			// Make sure they are at least a owner of this community
			if (communityId > 0 && !(CommunityUtility.IsCommunityOwner (communityId, GetUserId ())))
			{
				Response.Redirect (ResolveUrl ("~/community/noAccess.aspx?M=NM"));
				return;
			}

			// Do this on a postback since they may have just added a new one or are editing an existing.
			if (IsPostBack && !IsNewCommunity ())
			{
				HeaderNav.MyChannelsNav.ChannelId = communityId;

				if (Request ["pageId"] != null)
				{
					HeaderNav.MyChannelsNav.PageId = Convert.ToInt32 (Request ["pageId"]);
				}
			}

			
			// Set Nav
			if (!IsNewCommunity ())
			{
				HeaderNav.MyChannelsNav.ActiveTab = NavMyChannels.TAB.ADMIN;
				HeaderNav.SetNavVisible(HeaderNav.MyChannelsNav);
			}
            HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB._UPDATE_;
			HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.CHANNELS;
			HeaderNav.SetNavVisible(HeaderNav.MyKanevaNav,2);
			
			InitURLData(communityId);

			if (!IsPostBack)
			{
				// Get the community data
				DataRow drCommunity = CommunityUtility.GetCommunity (communityId);

				if (IsNewCommunity ())
				{
					// Don't show the cancel 
					btnCancel.Visible = false;
					btnUpdate.Visible = false;
					btnLaunch.Visible = true;

					// Insert the community in new status so they can upload banners on add new
					communityId = CommunityUtility.InsertCommunity (IsAdministrator (), 0, 0, "New", "New",
                        GetUserId(), KanevaWebGlobals.CurrentUser.Username, "Y", "N", "Y", "Y", false, (int) Constants.eCOMMUNITY_STATUS.NEW);

					// Add a default General forum to every community
					int forumCategory = ForumUtility.InsertForumCategory (communityId, "General", "General");
					ForumUtility.InsertForum (communityId, "General", "General", forumCategory);

					// Add the default modules for the new community
					bool communityDefaultModules = InsertCommunityDefaultModules (communityId);

					// Save it for later
					ViewState ["newCommunityId"] = communityId;

					if (communityId == 2)
					{
						ShowErrorOnStartup ("You are only allowed to create " + KanevaGlobals.MaxNumberOfCummunitiesPerUser + " active communities.");
						btnUpdate.Visible = false;
						return;
					}

					// Is it still zero? There was some kind of problem here.
					if ( (communityId == 0) || (! communityDefaultModules) )
					{
						m_logger.Error ("Error creating community. User = " + GetUserId () + " at " + KanevaGlobals.GetCurrentDateTime ().ToLongDateString ());
						ShowErrorOnStartup ("Error creating community.");
						return;
					}

					// Now get the one we just inserted
					drCommunity = CommunityUtility.GetCommunity (communityId, Constants.eCOMMUNITY_STATUS.NEW);

					// Set the defaults
					drCommunity ["name"] = "";
					drCommunity ["description"] = "";


					BindThemeData (1);
				}
				else
				{
					communityId = Convert.ToInt32 (drCommunity ["community_id"]);
					
					HeaderNav.MyChannelsNav.ChannelId = communityId;

					if (Request ["pageId"] != null)
					{
						HeaderNav.MyChannelsNav.PageId = Convert.ToInt32 (Request ["pageId"]);
					}

					trURLTitle.Visible = true;
					trURLBody.Visible = true;

					
					//Select the current category type.
					if ( Convert.ToInt32(drCommunity ["category_id"].ToString()) > 2 )
					{
						drpCategory.SelectedValue = drCommunity ["category_id"].ToString();
					}
                    //Select the current place type.
                    if (Convert.ToInt32(drCommunity["place_type_id"].ToString()) >= 0)
                    {
                        drpPlace.SelectedValue = drCommunity["place_type_id"].ToString();
                    }

					//since it's not a new community don't show the theme selection.
					tblTheme.Visible = false;

					//since it's not a new community we will give the option to disable members publishing rights.
					membersPublish.Visible = true;
				}

				// Set up the Upload link
				btnUploadThumbnail.Attributes.Add ("onclick", GetUploadThumbnailLink (communityId));

				txtTags.Text = drCommunity ["keywords"].ToString ();
				txtTitle.Text = Server.HtmlDecode (drCommunity ["name"].ToString ());
				txtDescription.Text = Server.HtmlDecode (drCommunity ["description"].ToString ());

                if (IsAdministrator ())
                {
                    drpStatus.Items.Add(new ListItem("Internal", "I"));
                }

                // "Unknown" is most likely internal for a non admin
				SetDropDownIndex (drpStatus, drCommunity ["is_public"].ToString (), true, "Internal");
				
				// Content types
				cbRestricted.Checked = drCommunity ["is_adult"].ToString ().ToUpper().Equals("Y");
				cbOver21.Checked = drCommunity ["over_21_required"].ToString ().ToUpper().Equals("Y");

				//drpStatus.Enabled = IsNewCommunity ();

				chkAllowPublish.Checked = Convert.ToInt32 (drCommunity ["allow_publishing"]).Equals (1);

				imgThumbnail.Src = GetBroadcastChannelImageURL (drCommunity ["thumbnail_large_path"].ToString (), "la");


				//Bind category dropdownlist
                CommunityFacade communityFacade = new CommunityFacade();
                drpCategory.DataSource = communityFacade.GetCommunityCategories();
				drpCategory.DataTextField = "CategoryName";
				drpCategory.DataValueField = "CategoryId";
                
				drpCategory.DataBind ();
				
				drpCategory.Items.Insert (0, new ListItem ("Select...", ""));

                //Bind place type dropdownlist

                drpPlace.DataSource = communityFacade.GetCommunityPlaceTypes();
                drpPlace.DataTextField = "Name";
                drpPlace.DataValueField = "PlaceTypeId";

                drpPlace.DataBind();

                drpPlace.Items.Insert(0, new ListItem("Select...", ""));


			}
			revTitle.ValidationExpression = Constants.VALIDATION_REGEX_CHANNEL_NAME;
			revTitle.ErrorMessage = "Title " + Constants.VALIDATION_REGEX_CHANNEL_NAME_ERROR_MESSAGE;

			regtxtKeywords.ValidationExpression = Constants.VALIDATION_REGEX_TAG;
			regtxtKeywords.ErrorMessage = Constants.VALIDATION_REGEX_TAG_ERROR_MESSAGE;

			// Admins are allowed to set over 21 status
			trOver21.Visible = IsAdministrator ();
			trRestricted.Visible = IsCurrentUserAdult ();
			AddBreadCrumb (new BreadCrumb ("Start/Edit Community", GetCurrentURL (), "", 0));

			InitThemeSelection ();
		}


		#region Theme Selection

		/// <summary>
		/// InitThemeSelection
		/// </summary>
		private void InitThemeSelection ()
		{
			const int defaultTemplate = 1;
			
			int templateId = 0;

			DataRow drChannel = CommunityUtility.GetCommunity(_channelId);
			
			if(drChannel ["template_id"] != DBNull.Value )
			{
				 templateId = Convert.ToInt32 (drChannel ["template_id"]);
			}


			
			int standardTemplateId = defaultTemplate;

			if (templateId > 0)
			{
				DataRow drTemplate = WidgetUtility.GetTemplate (templateId);
				if (drTemplate != null)
				{
					standardTemplateId = Convert.ToInt32 (drTemplate ["standard_template_id"]);
				}
			}

			// Show the preview image
			DataRow drStandardTemplate = WidgetUtility.GetStandardTemplate (standardTemplateId);
			if (drStandardTemplate != null)
			{
				imgThemePreview.Src = ResolveUrl (drStandardTemplate ["preview_url"].ToString ());
			}

			// Check the current one
			string scriptString = "<script language=JavaScript>";
			scriptString += "function setChecked(){setCheckedValue (document.all.rbTheme, " + standardTemplateId + ");}\n";
			scriptString += "window.onload=setChecked";
			scriptString += "</script>";

            if (!ClientScript.IsClientScriptBlockRegistered(GetType(), "SetChecked"))
			{
                ClientScript.RegisterStartupScript(GetType(), "SetChecked", scriptString);
			}
		}

		private void BindThemeData (int pageNumber)
		{
			int themesPerPage = Int32.MaxValue;

			string orderby = "standard_template_id" + " " + "ASC";

			PagedDataTable pds = WidgetUtility.GetStandardTemplates (orderby, pageNumber, themesPerPage);
			dlThemes.DataSource = pds;
			dlThemes.DataBind ();

		}

		protected string GetPreviewJavascript (string previewURL)
		{
			return "javascript:document.frmMain." + imgThemePreview.ClientID + ".src='" + ResolveUrl (previewURL) + "'";
		}

		#endregion Theme Selection


		#region Helper Methods

        /// <summary>
        /// Get a subscription for a given length
        /// </summary>
        /// <param name="dtCommunitySubscription"></param>
        /// <param name="lengthOfSubscription"></param>
        /// <returns></returns>
        private DataRow GetSubscription (DataTable dtCommunitySubscription, int lengthOfSubscription)
        {
            DataRow[] drResult = dtCommunitySubscription.Select ("length_of_subscription = " + lengthOfSubscription);
            if (drResult.Length > 0)
            {
                return drResult[0];
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Update (Save) the channel info
        /// </summary>
        /// <returns>boolean</returns>
        protected bool UpdateChannel ()
		{
			if (!Page.IsValid) 
			{
				return false;
			}

            // Check to make sure user did not enter any "potty mouth" words
            if (KanevaWebGlobals.isTextRestricted (txtTitle.Text, Constants.eRESTRICTION_TYPE.POTTY_MOUTH) ||
                KanevaWebGlobals.isTextRestricted (txtDescription.Text, Constants.eRESTRICTION_TYPE.POTTY_MOUTH) ||
                KanevaWebGlobals.isTextRestricted (txtTags.Text, Constants.eRESTRICTION_TYPE.POTTY_MOUTH))
            {
                spnAlertMsg.InnerText = Constants.VALIDATION_REGEX_RESTRICTED_WORD_MESSAGE;
                spnAlertMsg.Attributes.Add ("class", "errBox black");
                spnAlertMsg.Style.Add ("display", "block");
                MagicAjax.AjaxCallHelper.Write ("scroll(0,0);");
                return false;
            }

			int result = 0;
			int communityId = GetCommunityId ();

			// Update the image
			result = CommunityUtility.UpdateCommunity (communityId, Convert.ToInt32(drpCategory.SelectedValue), Convert.ToInt32(drpPlace.SelectedValue), Server.HtmlEncode (txtTitle.Text.Trim ()), Server.HtmlEncode (txtDescription.Text),
				Server.HtmlEncode (drpStatus.SelectedValue), Server.HtmlEncode (cbRestricted.Checked ? "Y" : "N") ,"Y",
				"Y", (chkAllowPublish.Checked)?1:0, false);

			spItemNameMsg.InnerHtml = string.Empty;
			
			// Was it successful?
			if (result == 1)
			{
				spItemNameMsg.InnerHtml = "Community " + txtTitle.Text + " already exists, please change the title.";
				return false;
			}

			// Update the owner tags
			DataRow drCommunity = CommunityUtility.GetCommunity (communityId);
			UsersUtility.UpdateChannelTag (Convert.ToInt32 (drCommunity ["creator_id"]), communityId,  Server.HtmlEncode (KanevaGlobals.GetNormalizedTags(txtTags.Text)));
			
			// Update over21 column
			if (trOver21.Visible && IsAdministrator ())
			{
				CommunityUtility.UpdateCommunity (communityId, cbOver21.Checked);
			}

			if (Convert.ToInt32 (Request ["communityId"]).Equals (0))
			{
				// If this is a new community and we are "launching" then let's update the Control Panel with the title.
				CommunityUtility.UpdateNewControlPanelTitle (ViewState["newControl_panel_id"].ToString(), txtTitle.Text.Trim());

				// Set the default notification while we are at it.
				CommunityUtility.UpdateCommunityNotifications(communityId, GetUserId (), 1);
			}
			
			// If a theme is selected then set it here.
			if (Request ["rbTheme"] != null)
			{		
				int templateId = Convert.ToInt32 (drCommunity ["template_id"]);
				int standardTemplateId = 0;
			
				// If they don't have a template, insert one now
				if (templateId.Equals (0))
				{
					templateId = WidgetUtility.InsertTemplate (1);
					CommunityUtility.UpdateTemplate(communityId, templateId);
				}
				
						
				if (KanevaGlobals.IsNumeric (Request ["rbTheme"].ToString ()))
				{
					standardTemplateId = Convert.ToInt32 (Server.HtmlEncode (Request ["rbTheme"].ToString ()));
				}
			

				if (standardTemplateId > 0)		 
				{
					CommunityUtility.UpdateUserStandardTemplate (communityId, standardTemplateId);
				}
			}

            // Handle case when they set the status some other than members only
            if (!drpStatus.SelectedValue.Equals ("N"))
            {
                // Mark all pending members as Members, this community is not members only
                CommunityUtility.AcceptAllPendingMembers (communityId);
            }

			return true;
		}

		/// <summary>
		/// GetUploadLink
		/// </summary>
		/// <returns></returns>
		private string GetUploadThumbnailLink (int communityId)
		{
			return "javascript: window.open('" + ResolveUrl ("~/community/thumbnailUpload.aspx?communityId=" + communityId) + "','add','toolbar=no,width=420,height=150,menubar=no,scrollbars=no');";
		}

		/// <summary>
		/// GetCommunityId
		/// </summary>
		/// <returns></returns>
		private int GetCommunityId ()
		{
			if (Convert.ToInt32 (Request ["communityId"]).Equals (0))
			{
				// This is an add new and was already inserted above
				return Convert.ToInt32 (ViewState ["newCommunityId"]);
			}
			else
			{
				return Convert.ToInt32 (Request ["communityId"]);
			}
		}

		/// <summary>
		/// IsNewCommunity
		/// </summary>
		/// <returns></returns>
		private bool IsNewCommunity ()
		{
			int communityId = Convert.ToInt32 (Request ["communityId"]);
			return (communityId == 0);
		}

		private void InitURLData(int communityId)
		{
			// Show current URL
			DataRow drChannel = CommunityUtility.GetCommunity (communityId);

			if (!drChannel ["url"].Equals (DBNull.Value))
			{
				lblURL.Style.Add("font-weight","bold");
				lblURL.Text = "http://" + KanevaGlobals.SiteName + "/" + drChannel ["url"].ToString ();
				txtURL.Visible = false;
				btnRegister.Visible = false;
				revURL.Enabled = false;
				spnRegistered.Visible = true;
			}
			else
			{
				// Set up regular expression validators
				revURL.ValidationExpression = Constants.VALIDATION_REGEX_USERNAME;
				lblURL.Text = "http://" + KanevaGlobals.SiteName + "/";
			}
		}

		/// <summary>
		/// Insert the default set of modules for a new community
		/// </summary>
		private bool InsertCommunityDefaultModules (int communityId)
		{
			string module_title = "Control Panel";

			int page_id = PageUtility.AddLayoutPage(true, communityId, "Home", 0, 0, 1);

			if ( page_id == 0)
			{
				return false;
			}

			// Always insert module_control_panel - must be first
			//new control panel to be populated with community name
			//module_title = WidgetUtility.GetDefaultTitle(page_id);
			int control_panel_id = WidgetUtility.InsertLayoutModuleChannelControlPanel (module_title);
			
			// Save this for later			
			ViewState ["newControl_panel_id"] = control_panel_id;

			if ( control_panel_id == 0)
			{
				return false;
			}

			PageUtility.AddLayoutPageModule(control_panel_id, page_id, (int) Constants.eMODULE_TYPE.CHANNEL_CONTROL_PANEL, (int) Constants.eMODULE_ZONE.HEADER, 1);

			// Always insert module_channel_description

			module_title = WebCache.GetModuleTitle( (int)Constants.eMODULE_TYPE.CHANNEL_DESCRIPTION);

			int channel_description_id = WidgetUtility.InsertLayoutModuleChannelDescription(module_title, Constants.DEFAULT_CHANNEL_NUM_MODERATORS_TO_SHOW, Constants.DEFAULT_CHANNEL_SHOW_MODERATORS);

			if ( channel_description_id == 0)
			{
				return false;
			}

			int module_sequence = 1;

			PageUtility.AddLayoutPageModule(channel_description_id, page_id, (int) Constants.eMODULE_TYPE.CHANNEL_DESCRIPTION, (int) Constants.eMODULE_ZONE.BODY, module_sequence);

			module_sequence++;

			// Members module

			module_title = WebCache.GetModuleTitle( (int)Constants.eMODULE_TYPE.CHANNEL_MEMBERS);

			int channel_members_id = WidgetUtility.InsertLayoutModuleChannelMembers(module_title);

			if ( channel_members_id == 0)
			{
				return false;
			}

			PageUtility.AddLayoutPageModule(channel_members_id, page_id, (int) Constants.eMODULE_TYPE.CHANNEL_MEMBERS, (int) Constants.eMODULE_ZONE.BODY, module_sequence);

			module_sequence++;

			// Place holder for multiple pictures
			module_sequence++;

			// Always insert module_events

			module_title = WebCache.GetModuleTitle( (int)Constants.eMODULE_TYPE.CHANNEL_EVENTS);

			int event_id = WidgetUtility.InsertLayoutModuleEvents(module_title);

			if ( event_id == 0)
			{
				return false;
			}

			PageUtility.AddLayoutPageModule(event_id, page_id, (int) Constants.eMODULE_TYPE.CHANNEL_EVENTS, (int) Constants.eMODULE_ZONE.BODY, module_sequence);

			module_sequence++;

			// Always insert module_counter

			module_title = WebCache.GetModuleTitle( (int)Constants.eMODULE_TYPE.MY_COUNTER);

			int counter_id = WidgetUtility.InsertLayoutModuleCounter(module_title);

			if ( counter_id == 0)
			{
				return false;
			}

			PageUtility.AddLayoutPageModule(counter_id, page_id, (int) Constants.eMODULE_TYPE.MY_COUNTER, (int) Constants.eMODULE_ZONE.BODY, module_sequence);

			module_sequence = 1; // switching to right column


			module_title = WebCache.GetModuleTitle( (int)Constants.eMODULE_TYPE.BLOGS );

			int blog_module_id = WidgetUtility.InsertLayoutModuleBlogs(module_title);

			if ( blog_module_id == 0)
			{
				return false;
			}

			PageUtility.AddLayoutPageModule(blog_module_id, page_id, (int) Constants.eMODULE_TYPE.BLOGS, (int) Constants.eMODULE_ZONE.COLUMN, module_sequence);

			module_sequence++;

			module_title = WebCache.GetModuleTitle( (int)Constants.eMODULE_TYPE.MUSIC_PLAYER);

			// Music
			int music_module_id = WidgetUtility.InsertLayoutModuleStores ( (int) Constants.eMODULE_TYPE.MUSIC_PLAYER, module_title);

			if ( music_module_id == 0)
			{
				return false;
			}

			PageUtility.AddLayoutPageModule(music_module_id, page_id, (int) Constants.eMODULE_TYPE.MUSIC_PLAYER, (int) Constants.eMODULE_ZONE.COLUMN, module_sequence);

			module_sequence++;

			// Games
			module_title = WebCache.GetModuleTitle( (int)Constants.eMODULE_TYPE.GAMES_PLAYER);

			int games_module_id = WidgetUtility.InsertLayoutModuleStores ( (int) Constants.eMODULE_TYPE.GAMES_PLAYER, module_title);

			if ( games_module_id == 0)
			{
				return false;
			}

			PageUtility.AddLayoutPageModule(games_module_id, page_id, (int) Constants.eMODULE_TYPE.GAMES_PLAYER, (int) Constants.eMODULE_ZONE.COLUMN, module_sequence);

			module_sequence++;

			// Video
			module_title = WebCache.GetModuleTitle( (int)Constants.eMODULE_TYPE.VIDEO_PLAYER);

			int video_module_id = WidgetUtility.InsertLayoutModuleStores ( (int) Constants.eMODULE_TYPE.VIDEO_PLAYER, module_title);

			if ( video_module_id == 0)
			{
				return false;
			}

			PageUtility.AddLayoutPageModule(video_module_id, page_id, (int) Constants.eMODULE_TYPE.VIDEO_PLAYER, (int) Constants.eMODULE_ZONE.COLUMN, module_sequence);

			module_sequence++;

			module_title = WebCache.GetModuleTitle( (int)Constants.eMODULE_TYPE.CHANNEL_FORUM);

			int forum_module_id = WidgetUtility.InsertLayoutModuleChannelForum(module_title);

			if ( forum_module_id == 0)
			{
				return false;
			}

			PageUtility.AddLayoutPageModule(forum_module_id, page_id, (int) Constants.eMODULE_TYPE.CHANNEL_FORUM, (int) Constants.eMODULE_ZONE.COLUMN, module_sequence);

			return true;
        }

        #endregion Helper Methods


        #region Event Handlers

        /// <summary>
		/// btnClearThumb_Click
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnClearThumb_Click (object sender, EventArgs e)
		{
			int communityId = GetCommunityId ();
			CommunityUtility.DeleteCommunityThumbs (communityId);
		}

		/// <summary>
		/// Cancel Add/Update a community
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnCancel_Click (object sender, EventArgs e) 
		{
			// If community id is 0, go back to listings
			int communityId = Convert.ToInt32 (Request ["communityId"]);
			if (communityId == 0)
			{
				Response.Redirect (ResolveUrl ("~/bookmarks/channelTags.aspx"));
			}

			if (Request ["returnURL"] != null)
			{
				Response.Redirect (Request ["returnURL"], false);
			}
			else
			{
				DataRow drChannel = CommunityUtility.GetCommunity (communityId);
				Response.Redirect (GetBroadcastChannelUrl (drChannel ["name_no_spaces"].ToString ()));
			}
		}

		/// <summary>
		/// Add/Update a community
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnUpdate_Click (object sender, EventArgs e) 
		{
			if (!UpdateChannel ())
			{
				return;
			}

            if (IsNewCommunity())
            {
                // Show viral stuff
                Response.Redirect("~/community/CommunityViral.aspx?communityId=" + GetCommunityId());
                return;
            }

			DataRow drChannel = CommunityUtility.GetCommunity (GetCommunityId ());
			Response.Redirect (GetBroadcastChannelUrl (drChannel ["name_no_spaces"].ToString ()));
		}

        /// <summary>
        /// Add/Update image
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnImgUpdate_Click (object sender, ImageClickEventArgs e) 
		{
			if (!UpdateChannel ())
			{
				return;
			}

            if (IsNewCommunity())
            {
                // Show viral stuff
                Response.Redirect("~/community/viral/CommunityViral.aspx?communityId=" + GetCommunityId());
                return;
            }

			DataRow drChannel = CommunityUtility.GetCommunity (GetCommunityId ());
			Response.Redirect (GetBroadcastChannelUrl (drChannel ["name_no_spaces"].ToString ()));
		}

        /// <summary>
        /// Register a community URL
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
		protected void btnRegister_Click (object sender, EventArgs e)
		{
			if (revURL.IsValid)
			{
				// If URL has not already been set
				if (txtURL.Enabled && txtURL.Text.Trim() != string.Empty)
				{
                    // Check to make sure user did not enter any "potty mouth" words
                    if (KanevaWebGlobals.isTextRestricted (txtURL.Text, Constants.eRESTRICTION_TYPE.POTTY_MOUTH))
                    {
                        spnAlertMsg.InnerText = Constants.VALIDATION_REGEX_RESTRICTED_WORD_MESSAGE;
                        spnAlertMsg.Attributes.Add ("class", "errBox black");
                        spnAlertMsg.Style.Add ("display", "block");
                        return;
                    }

                    //Check to make sure user did not enter any "reserved" words
                    if (KanevaWebGlobals.isTextRestricted(txtURL.Text, Constants.eRESTRICTION_TYPE.RESERVED))
                    {
                        spnAlertMsg.InnerText = Constants.VALIDATION_REGEX_RESERVED_WORD_MESSAGE;
                        spnAlertMsg.Attributes.Add("class", "errBox black");
                        spnAlertMsg.Style.Add("display", "block");
                        return;
                    }
                    
                    // Upadate URL
					int result = CommunityUtility.UpdateChannelURL (GetCommunityId (), Server.HtmlEncode (txtURL.Text.Trim()));

					// Was it successful?
					if (result == 1)
					{
						divRegistrationErr.InnerHtml = "Community URL http://" + KanevaGlobals.SiteName + "/" + txtURL.Text.Trim() + " already exists, please change the URL.";
						divRegistrationErr.Visible = true;
					}
					else
					{
						lblURL.Style.Add("font-weight","bold");
						lblURL.Text = "http://" + KanevaGlobals.SiteName + "/" + txtURL.Text.Trim();
						txtURL.Visible = false;
						btnRegister.Visible = false;
						revURL.Enabled = false;
						spnRegistered.Visible = true;
					}
				}
			}
		}

		#endregion
		

		#region Declarations

        protected int _channelId;

        protected HtmlInputFile inpAvatar;
		protected TextBox txtTitle, txtDescription, txtTags;
		protected DropDownList drpStatus, drpCategory, drpPlace;
		protected CheckBox chkAllowPublish, chkFreeItems, chkPayItems, chkLocked, chkPolls, chkUptime, chkIP;
		protected CheckBox cbRestricted, cbOver21;
		protected HtmlTableRow	trOver21;
		protected HtmlTableRow	trRestricted;
		
		protected HtmlGenericControl membersPublish;
        protected HtmlContainerControl spnAlertMsg;

		// Theme selection
		protected HtmlTable tblTheme;
		protected DataList dlThemes;
		protected HtmlImage imgThemePreview;

		protected HtmlImage imgThumbnail;
		protected HtmlInputButton btnUploadImage;
		protected Button btnUpdate, btnCancel;
		protected ImageButton btnLaunch;

		protected HtmlInputButton		btnUploadThumbnail;
		protected HtmlGenericControl spItemNameMsg;

		protected RegularExpressionValidator revTitle, regtxtKeywords;

		// URL
		protected RegularExpressionValidator	revURL;
		protected TextBox						txtURL;
		protected Label							lblURL;
		protected Button						btnRegister;
		protected HtmlTableRow					trURLTitle, trURLBody;
		protected HtmlGenericControl divRegistrationErr;
		protected HtmlGenericControl spnRegistered;
		protected ValidationSummary valSum;
		protected RequiredFieldValidator rfItemName;
		protected RequiredFieldValidator rftxtDescription;
		protected RequiredFieldValidator rftxtTags;
		protected Button btnClearThumb;

		// Logger
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);
	
        #endregion Declerations


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

	}
}

