///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva.channel;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for memberGroups.
	/// </summary>
	public class memberGroups : SortedBasePage
	{
		protected memberGroups () 
		{
			Title = "Member groups";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			if(Request.IsAuthenticated)
			{
				if(GetRequestParams())
				{
					//check if user is allowed to edit this channel
					if(!IsUserAllowedToEdit())
					{
						RedirectToHomePage ();
					}
				}
				else
				{
					//invalid request params
					//if the user logged in, send him to his home page
					//if not, redirect to login page
					RedirectToHomePage ();
				}
			}
			else
			{
				Response.Redirect (this.GetLoginURL ());
			}
			

			// Set Nav
            HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB._UPDATE_;
			HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.CHANNELS;
			HeaderNav.MyChannelsNav.ChannelId = _channelId;
			HeaderNav.MyChannelsNav.ActiveTab = NavMyChannels.TAB.MEMBERS;
				
			HeaderNav.SetNavVisible(HeaderNav.MyKanevaNav,2);
			HeaderNav.SetNavVisible(HeaderNav.MyChannelsNav);


			if (!IsPostBack)
			{
				lbSortByName.CssClass = "selected";

				BindData (1);
				hlShowMembers.NavigateUrl = ResolveUrl("~/community/members.aspx?communityId=" + _channelId);
				hlAddMember.NavigateUrl = ResolveUrl("~/community/inviteMember.aspx?communityId=" + _channelId);
			}
		}

		/// <summary>
		/// Bind the data
		/// </summary>
		/// <param name="pageNumber"></param>
		private void BindData (int pageNumber)
		{
			BindData (pageNumber, "");
		}
		/// <summary>
		/// Bind the data
		/// </summary>
		/// <param name="pageNumber"></param>
		private void BindData (int pageNumber, string filter)
		{
			searchFilter.CurrentPage = "membergroups";
			
			// Set the sortable columns
			string orderby = CurrentSort + " " + CurrentSortOrder;
			int pgSize = searchFilter.NumberOfPages;

			PagedDataTable pds = CommunityUtility.GetMemberGroups(_channelId, "", orderby, pageNumber, pgSize);
			dlMemberGroups.DataSource = pds;
			dlMemberGroups.DataBind ();

			pgTop.NumberOfPages = Math.Ceiling ((double) pds.TotalCount / pgSize).ToString ();
			pgTop.DrawControl ();

			// The results
			lblSearch.Text = GetResultsText (pds.TotalCount, pageNumber, pgSize, pds.Rows.Count);
		}

		
		#region Helper Methods
		/// <summary>
		/// GetMembersInGroupLink
		/// </summary>
		/// <param name="memberGroupId"></param>
		/// <returns></returns>
		protected string GetMembersInGroupLink (int memberGroupId)
		{
			return ResolveUrl ("~/community/membersInGroup.aspx?memberGroupId=" + memberGroupId
				+"&communityId=" + _channelId);
		}

		private bool GetRequestParams()
		{
			bool retVal = true;
			try
			{
				_channelId = Convert.ToInt32(Request.Params["communityId"]);
			}
			catch(Exception)
			{
				//invalid numbers
				retVal = false;
			}
			return retVal;
		}

		/// <summary>
		/// returns true if current user is allowed to edit this channel
		/// </summary>
		/// <returns></returns>
		private bool IsUserAllowedToEdit()
		{
			return CommunityUtility.IsCommunityModerator(_channelId, GetUserId());
		}

		protected string GetThumbnailImages(int memberGroupId, int numMembersInGroup)
		{
			PagedDataTable pds = CommunityUtility.GetMembersInGroup(memberGroupId, "uc.thumbnail_path IS NOT NULL", "", 1, 9);
			string thumbImages = string.Empty;
			string imgBlank = "~/images/group_folder_blank.gif";
			int i = 0;

			for (i=0; i<pds.Rows.Count; i++)
			{
				thumbImages += "<img src=\"";
				thumbImages += GetProfileImageURL (pds.Rows[i]["thumbnail_small_path"].ToString(), "me", pds.Rows[i]["gender"].ToString());
				thumbImages += "\" border=\"0\" width=\"30\" height=\"23\" style=\"margin:1px;\" />";
			}

			numMembersInGroup = numMembersInGroup > 9 ? 9 : numMembersInGroup;

			while (i < numMembersInGroup)
			{
				thumbImages += "<img src=\"" + GetProfileImageURL ("", "sm", "m") + "\" border=\"0\" width=\"30\" height=\"23\" style=\"margin:1px;\" />";
                i++;
			}

			while (i < 9)
			{
				thumbImages += "<img src=\"" + ResolveUrl(imgBlank) + "\" border=\"0\" width=\"30\" height=\"23\" style=\"margin:1px;\" />";
                i++;
			}

			return thumbImages;

		}
		#endregion

		#region Event Handlers
		/// <summary>
		/// Page Change
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pgTop_PageChange (object sender, PageChangeEventArgs e)
		{
			BindData (e.PageNumber);
		}

		/// <summary>
		/// Execute when the user clicks the the view type link button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void lbSortBy_Click (object sender, EventArgs e) 
		{
			lbSortByName.CssClass = lbSortByDate.CssClass = "";

			if (((LinkButton)sender).Attributes["sortby"].ToString().ToLower() == "name")
			{
				CurrentSort = "name";
				lbSortByName.CssClass = "selected";
			}
			else
			{
				CurrentSort = "created_datetime";
				lbSortByDate.CssClass = "selected";							  
			}

			BindData (pgTop.CurrentPageNumber);
		}
		protected void btnRemove_Click (Object sender, ImageClickEventArgs e)
		{
			HtmlInputHidden hidMemberGroupId;
			CheckBox chkEdit;
			int userId = GetUserId ();
			int ret = 0;
			int count = 0;
			bool isItemChecked = false;

			// Remove the friends
			foreach (DataListItem dliMemberGroup in dlMemberGroups.Items)
			{
				chkEdit = (CheckBox) dliMemberGroup.FindControl ("chkEdit");

				if (chkEdit.Checked)
				{
					isItemChecked = true;
					
					hidMemberGroupId = (HtmlInputHidden) dliMemberGroup.FindControl ("hidMemberGroupId");
					ret = CommunityUtility.DeleteMemberGroup (Convert.ToInt32 (hidMemberGroupId.Value));
				
					if (ret == 1)
						count++;
				}
			}

			if (isItemChecked)
			{
				if (count > 0)
				{
					BindData (pgTop.CurrentPageNumber);
				}
				
				if (ret != 1)
				{
					spnAlertMsg.InnerText = "An error was encountered while trying to remove Member Group.";
				}
				else
				{
					spnAlertMsg.InnerText = "Successfully removed " + count + " member group" + (count != 1 ? "s" : "") + ".";
				}
			}
			else
			{
				spnAlertMsg.InnerText = "No Member Groups were selected.";
			}

			MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('spnAlertMsg', 5000);");
		}
		/// <summary>
		/// Click the add button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnAddGroup_Click (object sender, ImageClickEventArgs e)
		{
			trAddGroup.Visible = true;

			BindData (pgTop.CurrentPageNumber);
		}
		/// <summary>
		/// Click the save button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnSaveG_Click (Object sender, System.EventArgs e) 
		{
			trAddGroup.Visible = false;
			int ret = 0;
			string groupName = inpAG.Value.Trim();

			if (groupName.Length > 0)
			{		  
				ret = CommunityUtility.InsertMemberGroup (_channelId, Server.HtmlEncode (inpAG.Value));
				inpAG.Value = "";
			
				if (ret != 1)
				{
					spnAlertMsg.InnerText = "An error was encountered while trying to add Member Group.";
				}
				else
				{
					spnAlertMsg.InnerText = "Successfully added Member Group '" + groupName + "'.";
				}

				BindData (pgTop.CurrentPageNumber);
			}
			else
			{
				spnAlertMsg.InnerText = "Please enter a Member Group name.";
			}

			MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('spnAlertMsg', 5000);");
		}
		/// <summary>
		/// Click the cancel button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnCancelG_Click (Object sender, System.EventArgs e) 
		{
			trAddGroup.Visible = false;
		}
		/// <summary>
		/// Execute when the user selects an item from the Store Filter
		/// Items to display drop down list
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void FilterChanged(object sender, FilterChangedEventArgs e)
		{
			searchFilter.SetPagesDropValue(searchFilter.NumberOfPages.ToString());
			
			BindData (1, e.Filter);
		}
		#endregion

		#region Properties
		/// <summary>
		/// DEFAULT_SORT
		/// </summary>
		/// <returns></returns>
		protected override string DEFAULT_SORT
		{
			get
			{
				return "name";
			}
		}

		#endregion

		#region Declerations
		protected Kaneva.Pager			pgTop;
		protected Kaneva.SearchFilter	searchFilter;

		protected Label			lblSearch;
		protected DataList		dlMemberGroups;
		protected HyperLink		hlShowMembers, hlAddMember;
		protected Button		btnSaveG, btnCancelG;
		protected LinkButton	lbSortByName, lbSortByDate;
		
		protected HtmlContainerControl	spnAlertMsg, spnGN;
		protected HtmlInputText			inpAG;
		protected HtmlTableRow			trAddGroup;

		private int	_channelId;
		#endregion

		#region Web Form Designer generated code

		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
			pgTop.PageChanged +=new PageChangeEventHandler (pgTop_PageChange);
			searchFilter.FilterChanged +=new FilterChangedEventHandler (FilterChanged);		
		}	
		#endregion

	}
}
