<%@ Page Language="C#" MasterPageFile="~/masterpages/ProfilePageTemplate.Master" AutoEventWireup="false" CodeBehind="CommunityPage.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.channel.CommunityPage" %>
<%@ Register TagPrefix="Kaneva" TagName="wvContainer" Src="~/mykaneva/widgets/WidgetViewContainer.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="ProfileAccess" Src="~/mykaneva/widgets/ProfileAccess.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="TCLoader" Src="~/usercontrols/TabbedControlLoader.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="MKContainer" Src="~/usercontrols/doodad/ContainerMyKaneva.ascx" %>

<asp:Content ID="cntProfileHead" runat="server" contentplaceholderid="cphHeadData" >
    <meta name="title" content="Kaneva. The Online Community and Social Network." />
    <meta name="description" content="Kaneva is the first to combine social networking and a free 3D virtual world. It's a whole new way to connect with friends. Have fun in a vibrant virtual world full of people, cool places and fun games. Create your avatar and get a free 3D home today!"/>
    <meta name="keywords" content="free virtual world, virtual world, virtual worlds, 3D world, online avatar community, mmo game, virtual reality, caneva, kaniva" />
    
    <script type="text/javascript" src="../jscript/prototype-1.6.1-min.js"></script>
    <script type="text/javascript" src="../jscript/scriptaculous/1.8.2/scriptaculous.js"></script>
    <script type="text/javascript" src="../jscript/yahoo/2.7.0/yahoo-dom-event.js"></script>
    <script type="text/javascript" src="../jscript/yahoo/2.7.0/element-min.js"></script>
    <script type="text/javascript" src="../jscript/yahoo/2.7.0/paginator-min.js"></script>
    <script type="text/javascript" src="../jscript/Communities/CommunityPage.js"></script>
    <script type="text/javascript" src="../jscript/SWFObject/swfobject-min.js"></script>
    <NOSCRIPT><META HTTP-EQUIV="refresh" CONTENT="1" URL="noJavascript.aspx" /></NOSCRIPT>

    <link href="../css/Communities/CommunityPage.css?v6" type="text/css" rel="stylesheet" />
    <asp:Literal id="cssHolder" runat="server" />
    
</asp:Content>

<asp:Content id="cntProfielLeft" runat="server" contentplaceholderid="cphLeftBody" >
    <div id="divLeftProfilePanel" runat="server" class="LeftProfilePanel">
		<kaneva:MKContainer id="wvContainer1" runat="server" ControlToLoad="/mykaneva/widgets/CommunityControlPanel.ascx" />
		<div id="noAccessLayer" runat="server">
		    <kaneva:wvContainer id="wvContainerMembers" runat="server" ControlToLoad="mykaneva/widgets/CommunityMembersList.ascx" AjaxDataPage="/mykaneva/widgets/CommunityMembersData.aspx" EditButtonLink="/community/Members.aspx" AjaxDataPageParams="pagesize=6&orderby=Rand&showgrid=false" Title="Members"/>
		    <kaneva:wvContainer id="wvContainerLeftCol" runat="server" ControlToLoad="" AjaxDataPage="" EditButtonLink="" AjaxDataPageParams="" Title=""/>
		</div>
    </div>
</asp:Content>

<asp:Content ID="cntProfileRight" runat="server" contentplaceholderid="cphRightBody" >
    <div id="divRightProfilePanel" runat="server" class="RightProfilePanel">
		<kaneva:Tcloader id="TclMainPanel" runat="server" />	
   </div>
    <div id="divCenterPanel" runat="server" class="NoAccessPanel">
		<kaneva:profileaccess id="profileAccess" runat="server" />
    </div>    
</asp:Content>
