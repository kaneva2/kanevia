///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva.channel;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for inviteMember.
	/// </summary>
	public class inviteMember : MainTemplatePage
	{
		protected inviteMember () 
		{
			Title = "Invite a member to join Community";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			if(Request.IsAuthenticated)
			{
				if(GetRequestParams())
				{
					//check if user is allowed to edit this channel
					if(!IsUserAllowedToEdit())
					{
						RedirectToHomePage ();
					}
				}
				else
				{
					//invalid request params
					//if the user logged in, send him to his home page
					//if not, redirect to login page
					RedirectToHomePage ();
				}
			}
			else
			{
				Response.Redirect (this.GetLoginURL ());
			}
			
			
			// Set Nav
            HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB._UPDATE_;
			HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.CHANNELS;
			HeaderNav.MyChannelsNav.ChannelId = _channelId;
			HeaderNav.MyChannelsNav.ActiveTab = NavMyChannels.TAB.MEMBERS;
				
			HeaderNav.SetNavVisible(HeaderNav.MyKanevaNav,2);
			HeaderNav.SetNavVisible(HeaderNav.MyChannelsNav);

			
			if (!IsPostBack)
			{
				int userId = GetUserId ();

				DataRow drChannel = CommunityUtility.GetCommunity(_channelId);
				_channelName = drChannel["name"].ToString();

                lblSubject.Text = GetSubject(KanevaWebGlobals.CurrentUser.Username);
                lblFrom.Text = KanevaWebGlobals.CurrentUser.Username;
                lblMessage.Text = GetEmailMessage(KanevaWebGlobals.CurrentUser.Username);

				spnChannelName.InnerText = "'" + _channelName + "'";
			}

		}

		
		#region Helper Methods
		private bool GetRequestParams()
		{
			bool retVal = true;
			try
			{
				_channelId = Convert.ToInt32(Request.Params["communityId"]);
			}
			catch(Exception)
			{
				//invalid numbers
				retVal = false;
			}
			return retVal;
		}

		/// <summary>
		/// returns true if current user is allowed to edit this channel
		/// </summary>
		/// <returns></returns>
		private bool IsUserAllowedToEdit()
		{
			return CommunityUtility.IsCommunityModerator(_channelId, GetUserId());
		}

		private string GetEmailMessage (string username)
		{
            return (username +
				" has invited you to join " + _channelName +" at Kaneva!" +
				"<BR><BR>" +			
				"<span style='color: #A6D2FF; font-size: 14px; font-weight: bold;'>" + txtMessage.Text + "</span>" +
				"<br><br>" +
				" Come join the fun at Kaneva and participate:<BR><BR>" +
				" * Play Games!<BR>" +
				" * Watch Movies!<BR>" +
				" * Find Members!<BR>" +
				" * Make your own TV, movie, or game community and broadcast worldwide!<BR><BR>");
			
		}

		private string GetSubject (string username)
		{
			return ("Invitation to join " + _channelName +" at Kaneva from " + username);
		}

		/// <summary>
		/// Send out an email
		/// </summary>
		/// <param name="emailAddress"></param>
		/// <returns></returns>
		private void SendEmail (string emailAddress)
		{
			// Generate the unique key code
			string keyCode = KanevaGlobals.GenerateUniqueString (20);

			// Are there to be any K-Points awarded?
			Double awardAmount = 0.0;
			string keiPointIdAward = "";

			DataRow drInvitePointAward = UsersUtility.GetInvitePointAwards ();
			if (drInvitePointAward != null)
			{
				awardAmount = Convert.ToDouble (drInvitePointAward ["point_award_amount"]);
				keiPointIdAward = drInvitePointAward ["kei_point_id"].ToString ();
			}

			// Save the invites to the invites table
			int inviteID = UsersUtility.InsertInvite (GetUserId (), emailAddress, "", keyCode, awardAmount, keiPointIdAward, _channelId);
			if (inviteID == 0)
			{
				ShowErrorOnStartup ("Error inserting invite.");
				return;
			}

			// Generate the mail message
            string message = GetEmailMessage(KanevaWebGlobals.CurrentUser.Username);

			//Append the link
			message += "<BR><BR> Click this link to join KANEVA: " +
                "<a style='color: orange;' href=\"http://" + KanevaGlobals.SiteName + KanevaGlobals.JoinLocation.Substring(1) + "?i=" + inviteID + "&k=" + keyCode + "\">http://" + KanevaGlobals.SiteName + KanevaGlobals.JoinLocation.Substring(1) + "?i=" + inviteID + "&k=" + keyCode + "</a>";

			message = "<html><body bgcolor='#ffffff' topmargin='0' leftmargin='0'><table border='0' width='100%' cellpadding='20' cellspacing='0'><tr><td width='20%'></td><td style='font-family: arial; color: #000000; font-size: 13px;' width='60%' align='center' bgcolor='#ffffff'><img src='http://" + KanevaGlobals.SiteName + "/images/email_header.jpg' border='0'><br><br><br>" + message + "<br><br><br><br></td><td width='20%'></td></tr></table></body></html>\n\n"; 

			// Send out the email
            MailUtility.SendEmail(KanevaGlobals.FromEmail, emailAddress, GetSubject(KanevaWebGlobals.CurrentUser.Username), message, true, false, 2);
		}

		#endregion

		#region Event Handlers
		/// <summary>
		/// Click send
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnSendNow_Click (object sender, ImageClickEventArgs e) 
		{
            UserFacade userFacade = new UserFacade();

			if (txtTo.Text.Trim() == string.Empty)
			{
				spnAlertMsg.InnerText = "To: is a required field";
				return;
			}

			System.Text.RegularExpressions.Regex regexEmail = new System.Text.RegularExpressions.Regex (Constants.VALIDATION_REGEX_EMAIL);

			// Validate the email addresses
			//parse the email addresses using spaces, comman and semicolons
			string [] emailAddresses = txtTo.Text.Trim ().Split (';');
			ArrayList emailList = new ArrayList() ;
			foreach(string s in emailAddresses)
			{
				string [] list = s.Trim ().Split (' ');
				foreach(string s2 in list)
				{
					string [] list2 = s2.Trim ().Split (',');
					foreach(string s3 in list2)
					{
						emailList.Add(s3);
					}
				}
			}

			foreach (string toEmail in emailList)
			{
				// Make sure it is a valid type
				if (!regexEmail.IsMatch (toEmail))
				{
					spnAlertMsg.InnerText = "Invalid email address '" + toEmail + "'";
					return;
				}

				// Make sure the email does not already exist as a registered user
				if (userFacade.EmailExists (toEmail))
				{
					spnAlertMsg.InnerText = "The email address '" + toEmail + "' is already associated with a user, please select another address.";
					return;
				}
			}

			// Send out the emails
			foreach (string toEmail in emailList)
			{
				SendEmail (toEmail);
			}

			Response.Redirect (ResolveUrl ("~/community/inviteMemberConfirm.aspx?communityId=" + _channelId));
		}
		
		protected void btnGo_Click (object sender, ImageClickEventArgs e)
		{
			Response.Redirect(ResolveUrl ("~/community/members.aspx?communityId=" + _channelId ));
		}
		#endregion

		#region Declerations
		protected TextBox					txtTo, txtMessage;
		protected Label						lblSubject, lblFrom, lblMessage;

		protected HtmlContainerControl	spnAlertMsg, spnChannelName;

		private int	_channelId;
		private string _channelName;

		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
