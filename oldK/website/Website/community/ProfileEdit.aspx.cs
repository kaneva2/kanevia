///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using log4net;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.community
{
    public partial class ProfileEdit : BasePage
    {
        #region Declarations
        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private Community currentCommunity = null;
        #endregion


        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            // They must be logged in
            if (Request.IsAuthenticated)
            {
                //get the parameters 
                GetRequestParams();

                //redirect if no community id is found
                if (COMMUNITY_ID <= 0)
                {
                    Response.Redirect(RETURN_URL);
                }

                //configure page security
                ConfigurePageSecurity();

                //send all unauthorized people away otherwise continue processing the page
                if ((COMMUNITY_ID > 0) && !IS_ALLOWED_TO_EDIT)
                {
                    Response.Redirect("~/error.aspx?error=" + (int)Constants.eERROR_TYPE.ACCESS_DENIED, true);
                }
                else
                {
                    //set the navigation
                    ((GenericPageTemplate)Master).HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
                    ((GenericPageTemplate)Master).HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB._UPDATE_;
                    ((GenericPageTemplate)Master).HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.PROFILE;
                    ((GenericPageTemplate)Master).HeaderNav.SetNavVisible(((GenericPageTemplate)Master).HeaderNav.MyKanevaNav, 2);
                    ((GenericPageTemplate)Master).HeaderNav.MyChannelsNav.ChannelId = this.COMMUNITY_ID;
                    //((GenericPageTemplate)Master).HeaderNav.MyChannelsNav.PageId = this.COMMUNITY_PAGE_ID;

                    //do only on page load not post back
                    if (!IsPostBack)
                    {
                        //attach all needed javascript
                        ConfigureJavaScript();

                        //get and set the data
                        GetProfileData();

                        //set the page to go back to
                        SetPreviousPage();
                    }

                    //set the selected value field for the background selection control
                    libraryImageSelector.ReturnField = this.txtBGPicture;
                }
            }
            else
            {
                Response.Redirect(GetLoginURL());
            }
        }
        #endregion

        #region Main Functions

        private void GetProfileData()
        {
            // "Unknown" is most likely internal for a non admin
            profileAccess.SelectedValue = this.COMMUNITY.IsPublic.ToString();

            txtBackgroundColor.Text = this.COMMUNITY.BackgroundRGB;
            txtBGPicture.Text = this.COMMUNITY.BackgroundImage;


            GetCurrentProfileImageURL ();

            //set up the community URL
            InitURLData(this.COMMUNITY.Url);

            //set the profile preferences
            cbShowGender.Checked = COMMUNITY.Preferences.ShowGender;
            cbShowLocation.Checked = COMMUNITY.Preferences.ShowLocation;
            cbShowAge.Checked = COMMUNITY.Preferences.ShowAge;

            //set the adult settings
            ORIGINAL_IS_ADULT = COMMUNITY.IsAdult;

            if (KanevaWebGlobals.CurrentUser.IsAdult)
            {
                chkContainsMature.Checked = this.COMMUNITY.IsAdult.ToString().ToUpper().Equals("Y");
            }
            else
            {
                chkContainsMature.Enabled = false;
                chkContainsMature.BackColor = System.Drawing.Color.Gray;
            }
        }

        private bool IsURLAvailable()
        {
            bool isURLAvailable = false;

            if ((COMMUNITY.Url == null || COMMUNITY.Url == "") && txtURL.Visible)
            {
                //get and uncode url
                string url = txtURL.Text.Trim();

                if ((url != null) && (url != ""))
                {
                    url = Server.HtmlEncode(url);

                    //check to see if the URL is in violation of allowed words, or if it is already in use.
                    //if it is ok add for save to database, otherwise display error message
                    if (KanevaWebGlobals.isTextRestricted(url, Constants.eRESTRICTION_TYPE.POTTY_MOUTH))
                    {
                        alertMsg.InnerText = Constants.VALIDATION_REGEX_RESTRICTED_WORD_MESSAGE;
                    }
                    else if (KanevaWebGlobals.isTextRestricted(url, Constants.eRESTRICTION_TYPE.RESERVED))
                    {
                        alertMsg.InnerText = Constants.VALIDATION_REGEX_RESERVED_WORD_MESSAGE;
                    }
                    else if (GetCommunityFacade.IsCommunityURLTaken(url, COMMUNITY_ID))
                    {
                        alertMsg.InnerText = "The URL you have selected is not available. Please Choose another";
                    }
                    else
                    {
                        //update URL
                        COMMUNITY.Url = url;
                        isURLAvailable = true;
                    }
                }
                //no URL was provided
                else
                {
                    isURLAvailable = true;
                }
            }
            else //the URL has been set before so set this to true to keep processing
            {
                isURLAvailable = true;
            }

            return isURLAvailable;
        }

        private bool UpdateChannel()
        {
            bool successful = false;
            int result = 0;

            if (Page.IsValid)
            {
                //check for injection scripts and banned words
                if (KanevaWebGlobals.isTextRestricted(txtURL.Text, Constants.eRESTRICTION_TYPE.ALL))
                {
                    alertMsg.InnerText = Constants.VALIDATION_REGEX_RESTRICTED_WORD_MESSAGE;
                }
                else if (KanevaWebGlobals.ContainsInjectScripts(txtBackgroundColor.Text) || 
                    KanevaWebGlobals.ContainsInjectScripts(txtBGPicture.Text) ||
                    KanevaWebGlobals.ContainsInjectScripts(txtURL.Text))
                {
                    m_logger.Warn("User " + KanevaWebGlobals.GetUserId() + " tried to script from IP " + Common.GetVisitorIPAddress());
                    alertMsg.InnerText = Constants.VALIDATION_REGEX_RESTRICTED_WORD_MESSAGE;
                }
                else
                {
                    //prep the URL for saving IF provided and new
                    if (IsURLAvailable())
                    {
                        //set the values entered by user
                        //community.AllowMemberEvents;
                        COMMUNITY.BackgroundImage = Server.HtmlEncode(txtBGPicture.Text);
                        COMMUNITY.BackgroundRGB = Server.HtmlEncode(txtBackgroundColor.Text);
                        COMMUNITY.CommunityId = COMMUNITY_ID;
                        COMMUNITY.IsPersonal = 1; //0 is false
                        COMMUNITY.StatusId = (int)Constants.eCOMMUNITY_STATUS.ACTIVE;
                        COMMUNITY.IsPublic = Server.HtmlEncode(profileAccess.SelectedValue);

                        //update profile preferences
                        COMMUNITY.Preferences.ShowGender = cbShowGender.Checked;
                        COMMUNITY.Preferences.ShowLocation = cbShowLocation.Checked;
                        COMMUNITY.Preferences.ShowAge = cbShowAge.Checked;

                        //update the profile adult setting
                        if (chkContainsMature.Enabled)
                        {
                            COMMUNITY.IsAdult = Server.HtmlEncode(chkContainsMature.Checked ? "Y" : "N");
                        }
                        else
                        {
                            COMMUNITY.IsAdult = "N";
                        }

                        if (COMMUNITY_ID > 0)
                        {
                            // Update the community - in this case result is #rows affected
                            result = GetCommunityFacade.UpdateCommunity(COMMUNITY);

                            if (result <= 0)
                            {
                                alertMsg.InnerText = "Unable to save your changes.";
                            }
                        }
                        //if community update succeeded perform remaining operations
                        if (result > 0)
                        {
                            string imageResults = null;

                            // update the thumbnail
                            if ((inpThumbnail.Value != null) && (inpThumbnail.Value != ""))
                            {
                                alertMsg.InnerText = imageResults = UploadImage(this.COMMUNITY_ID, true, this.inpThumbnail, this.CURRENT_USER_ID, null);

                                // User upload image so clear facebook setting for profile image
                                GetUserFacade.SetUseFacebookProfilePic (COMMUNITY.CreatorId, false);
                                // Clear the User Cache
                                GetUserFacade.InvalidateKanevaUserFacebookCache (KanevaWebGlobals.CurrentUser.UserId);

                                // User completed fame packet
                                try
                                {
                                    GetFameFacade.RedeemPacket (KanevaWebGlobals.GetUserId (), (int) PacketId.WEB_OT_ADD_PICTURE, (int) FameTypes.World);
                                }
                                catch (Exception ex)
                                {
                                    m_logger.Error ("Error awarding Packet WEB_OT_ADD_PICTURE, userid=" + KanevaWebGlobals.GetUserId (), ex);
                                }
                            }

                            // update the user object to reflect the new access pass / adult setting if any change
                            if (ORIGINAL_IS_ADULT != COMMUNITY.IsAdult)
                            {
                                GetUserFacade.UpdateUserRestriction(COMMUNITY.CreatorId, (COMMUNITY.IsAdult == "Y" ? 1 : 0));
                            }

                            if (imageResults == null)
                            {
                                successful = true;
                            }
                        }
                    }
                }
            }
            return successful;
        }

        #endregion

        #region Attributes
        private string ORIGINAL_IS_ADULT
        {
            set
            {
                ViewState["isAdult"] = value;
            }
            get
            {
                return ViewState["isAdult"].ToString();
            }
        }

        private int COMMUNITY_ID
        {
            set
            {
                ViewState["communityId"] = value;
            }
            get
            {
                if (ViewState["communityId"] == null)
                {
                    ViewState["communityId"] = -1;
                }
                return (int)ViewState["communityId"];
            }
        }

        private int CURRENT_USER_ID
        {
            set
            {
                ViewState["currentUserId"] = value;
            }
            get
            {
                if (ViewState["currentUserId"] == null)
                {
                    ViewState["currentUserId"] = GetUserId();
                }
                return (int)ViewState["currentUserId"];
            }
        }

        private bool IS_COMMUNITY_OWNER
        {
            set
            {
                ViewState["isCommunityOwner"] = value;
            }
            get
            {
                if (ViewState["isCommunityOwner"] == null)
                {
                    ViewState["isCommunityOwner"] = false;
                }
                return (bool)ViewState["isCommunityOwner"];
            }
        }

        private bool IS_ALLOWED_TO_EDIT
        {
            set
            {
                ViewState["isAllowedToEdit"] = value;
            }
            get
            {
                if (ViewState["isAllowedToEdit"] == null)
                {
                    ViewState["isAllowedToEdit"] = false;
                }
                return (bool)ViewState["isAllowedToEdit"];
            }
        }

        private bool IS_ADMINISTRATOR
        {
            set
            {
                ViewState["isAdministrator"] = value;
            }
            get
            {
                if (ViewState["isAdministrator"] == null)
                {
                    ViewState["isAdministrator"] = false;
                }
                return (bool)ViewState["isAdministrator"];
            }
        }

        private string RETURN_URL
        {
            set
            {
                ViewState["returnURL"] = value;
            }
            get
            {
                if (ViewState["returnURL"] == null)
                {
                    ViewState["returnURL"] = "";
                }
                return ViewState["returnURL"].ToString();
            }
        }

        private Community COMMUNITY
        {
            set
            {
                this.currentCommunity = value;
            }
            get
            {
                if (this.currentCommunity == null)
                {
                    this.currentCommunity = GetCommunityFacade.GetCommunity(this.COMMUNITY_ID);

                    //this is needed to pull the proxy data so it will be ready for data entry on the
                    //update 
                    int prefId = COMMUNITY.Preferences.CommunityPreferenceId; 
                }
                return this.currentCommunity;
            }
        }

        #endregion

        #region Helper Functions

        private void GetCurrentProfileImageURL ()
        {
            // if current user is owner
            if (KanevaWebGlobals.CurrentUser.UserId == CURRENT_USER_ID)
            {
                // has user chosen to user facebook profile pic?
                if (KanevaWebGlobals.CurrentUser.FacebookSettings.UseFacebookProfilePicture)
                {
                    imgThumbnail.Src = GetProfileImageURL (KanevaWebGlobals.CurrentUser.ThumbnailSmallPath,
                        "sm", KanevaWebGlobals.CurrentUser.Gender,
                        KanevaWebGlobals.CurrentUser.FacebookSettings.UseFacebookProfilePicture,
                        KanevaWebGlobals.CurrentUser.FacebookSettings.FacebookUserId);

                    btnFBProfilePic.Visible = false;
                    btnClearThumb.Enabled = true;
                }
                else
                {
                    imgThumbnail.Src = GetProfileImageURL (KanevaWebGlobals.CurrentUser.ThumbnailSmallPath,
                        "sm", KanevaWebGlobals.CurrentUser.Gender);

                    //if no image no need to clear it
                    btnClearThumb.Enabled = this.COMMUNITY.HasThumbnail;
                    btnFBProfilePic.Visible = true;
                }
            }
            else
            {
                imgThumbnail.Src = GetBroadcastChannelImageURL (this.COMMUNITY.ThumbnailLargePath, "la");

                //if no image no need to clear it
                btnClearThumb.Enabled = this.COMMUNITY.HasThumbnail;
            }
        }

        private void ResetErrorMessages()
        {
            alertMsg.Visible = false;
        }

        private void ConfigureJavaScript()
        {
            //add clear function to button
            aRemove.Attributes.Add("onclick", "ClearField('" + txtBGPicture.ClientID + "')");

            //add onchange event to the color picker
            txtBackgroundColor.Attributes.Add("onchange", "relateColor('" + txtBackgroundColorAppClrPickerStr.ClientID + "',this.value);");
            txtBackgroundColorAppClrPickerStr.NavigateUrl = "javascript:pickColorMP('" + txtBackgroundColorAppClrPickerStr.ClientID + "');";
            if (!ClientScript.IsClientScriptBlockRegistered(GetType(), "setbgcolor"))
            {
                ClientScript.RegisterStartupScript(GetType(), "setbgcolor", "<script>relateColor('" +
                    txtBackgroundColorAppClrPickerStr.ClientID + "',$('" + txtBackgroundColor.ClientID + "').value);</script>");
            }
        }

        private void SetPreviousPage()
        {
            string lastPage = null;
            try
            {
                lastPage = Request.UrlReferrer.AbsoluteUri.ToString();
            }
            catch (NullReferenceException)
            {
            }

            if (lastPage != null)
            {
                RETURN_URL = lastPage;
            }
            else if (COMMUNITY_ID > 0)
            {
                RETURN_URL = ResolveUrl("~/community/ProfilePage.aspx?communityId=" + COMMUNITY_ID.ToString());
            }
        }

        private void GetRequestParams()
        {
            //check to see if there is a name for the game (required)
            try
            {
                if (Request["communityId"] != null)
                {
                    COMMUNITY_ID = Convert.ToInt32(Request["communityId"].ToString());
                }
                else if (Request["userId"] != null)
                {
                    int personalUserId = Convert.ToInt32(Request["userId"].ToString());
                    if (personalUserId > 0)
                    {
                        UserFacade userFacade = new UserFacade();
                        COMMUNITY_ID = userFacade.GetPersonalChannelId(personalUserId);
                    }
                }

            }
            catch (Exception ex)
            {
                m_logger.Error("Profile id was not provided ", ex);
            }

            try
            {
                RETURN_URL = Request["returnURL"].ToString();
            }
            catch (Exception)
            {
            }
        }

        private void InitURLData(string communityURL)
        {
            if ((communityURL != null) && (communityURL != ""))
            {
                lblURL.Style.Add("font-weight", "bold");
                lblURL.Text = "http://" + KanevaGlobals.SiteName + "/" + communityURL;
                txtURL.Visible = false;
                revURL.Enabled = false;
                spnRegistered.Visible = true;
                urlWarning.Visible = false;
            }
            else
            {
                // Set up regular expression validators
                revURL.ValidationExpression = Constants.VALIDATION_REGEX_USERNAME;
                lblURL.Text = "http://" + KanevaGlobals.SiteName + "/";
                urlWarning.Visible = true;
            }
        }

        private void ConfigurePageSecurity()
        {
            //check to see if they are the owner
            IS_COMMUNITY_OWNER = GetCommunityFacade.IsCommunityOwner(COMMUNITY_ID, CURRENT_USER_ID);

            //check security level
            switch (KanevaWebGlobals.CheckUserAccess((int)SitePrivilege.ePRIVILEGE.USER_PROFILE_ADMIN))
            {
                case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_FULL:
                    IS_ADMINISTRATOR = true;
                    break;
                case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_READ:
                    break;
                case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_NONE:
                    break;
                default:
                    break;
            }
            //set editing right
            if (IS_ADMINISTRATOR || IS_COMMUNITY_OWNER)
            {
                IS_ALLOWED_TO_EDIT = true;
            }
        }
        #endregion

        #region Event Handlers
        /// <summary>
        /// btnClearThumb_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnClearThumb_Click(object sender, EventArgs e)
        {
            if (KanevaWebGlobals.CurrentUser.FacebookSettings.UseFacebookProfilePicture)
            {   // clear the use facebook pic setting
                GetUserFacade.SetUseFacebookProfilePic (KanevaWebGlobals.CurrentUser.UserId, false);
                
                // just in case the clear cache below is not getting cleared correctly
                KanevaWebGlobals.CurrentUser.FacebookSettings.UseFacebookProfilePicture = false;
            }
            else
            {   // clear the current images
                GetCommunityFacade.DeleteCommunityThumbs (COMMUNITY_ID);
            }
            // Clear the User Cache
            GetUserFacade.InvalidateKanevaUserFacebookCache (KanevaWebGlobals.CurrentUser.UserId);

            GetProfileData ();
        }

        /// <summary>
        /// btnFBProfilePic_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnFBProfilePic_Click (object sender, EventArgs e)
        {
            GetUserFacade.SetUseFacebookProfilePic (COMMUNITY.CreatorId, true);
            imgThumbnail.Src = KanevaGlobals.FacebookGraphUrl + 
                KanevaWebGlobals.CurrentUser.FacebookSettings.FacebookUserId + 
                KanevaGlobals.FacebookProfileImageQueryString;

            // Clear the User Cache
            GetUserFacade.InvalidateKanevaUserFacebookCache (KanevaWebGlobals.CurrentUser.UserId);

            btnClearThumb.Enabled = true;

            // User completed fame packet
            try
            {
                GetFameFacade.RedeemPacket (KanevaWebGlobals.GetUserId (), (int) PacketId.WEB_OT_ADD_PICTURE, (int) FameTypes.World);
            }
            catch (Exception ex)
            {
                m_logger.Error ("Error awarding Packet WEB_OT_ADD_PICTURE, userid=" + KanevaWebGlobals.GetUserId (), ex);
            }
        }

        /// <summary>
        /// Cancel Add/Update a community
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(RETURN_URL);
        }

        /// <summary>
        /// Add/Update a community
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            ResetErrorMessages();

            if (UpdateChannel())
            {
                Response.Redirect(RETURN_URL);
            }
            else
            {
                alertMsg.Visible = true;
            }
        }

        /// <summary>
        /// show and intialize library image selection control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void selectPhoto_Click(object sender, EventArgs e)
        {
            libraryImageSelector.Visible = true;
        }


        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion

    }
}
