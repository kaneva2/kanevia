///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using System.Collections.Generic;
using log4net;

namespace KlausEnt.KEP.Kaneva.channel
{
    public partial class ProfilePage : BasePage
    {
        #region Declarations

        private Community currentCommunity = null;
        private int communityId = 0;
        private int profileOwnerId = 0;
        private bool userHasAccess = true;
        private bool userIsAdmin = false;

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            //get the required parameters
            GetRequestParams();

            //check to see if the user is logged in
            if (Request.IsAuthenticated)
            {
                if (CommunityId > 0)
                {
                    //check the users Access
                    CheckCommunityAccess();

                    if (userHasAccess)
                    {
                        //get owner data
                        GetOwnerData();

                        //amend the ajax parameter for the embeded usercontrols
                        SetEmbededControlOptions();

                        //set the profile configurations
                        ConfigureTabbedControl();

                        divLeftProfilePanel.Visible = true;
                        divRightProfilePanel.Visible = true;
                        divCenterPanel.Visible = false;

                        //update the views for the profile
                        if (!Page.IsPostBack)
                        {
                            UpdateChannelViews();
                        }
                    }
                    else if (!userHasAccess)
                    {
                        // If community id = 0 after GetCommunity then world is not active
                        if (CurrentCommunity.CommunityId == 0)
                        {
                            Response.Redirect ("~/people/people.kaneva", true);
                        }

                        //clear controls to prevent them from processing
                        //divLeftProfilePanel.Controls.Clear();
                        divRightProfilePanel.Controls.Clear();
                        noAccessLayer.Controls.Clear();

                        //set display
                        noAccessLayer.Visible = false;
                        divLeftProfilePanel.Visible = true;
                
                        divRightProfilePanel.Visible = false;
                        divCenterPanel.Visible = true;
                    }

                    //set any personalizations of the community
                    SetPersonalizations();

                    //set the navigation
                    SetNav();

                    // Set value in HttpContext so widgets can have access to it.
                    HttpContext.Current.Items.Add("community", this.CurrentCommunity);

                    if (!IsPostBack)
                    {
                        // Request tracking
                        if (Request.Params[Constants.cREQUEST_TRACKING_URL_PARAM] != null)
                        {
                            int iResult = GetUserFacade.AcceptTrackingRequest (Request.Params[Constants.cREQUEST_TRACKING_URL_PARAM], KanevaWebGlobals.CurrentUser.UserId);
                        }
                    }

                    if (ProfileOwnerId == KanevaWebGlobals.CurrentUser.UserId)
                    {
                        wvContainerFriends.Title = "My Friends";
                        wvContainerWorlds.Title = "My Worlds";
                    }
                }
                else
                {
                    Response.Redirect("~/error.aspx?error=" + (int)Constants.eERROR_TYPE.PROFILE_NOT_FOUND, true);
                }
            }
            else if(CommunityId > 0)
            {
                Server.Transfer("ProfileLandingPage.aspx?communityId=" + CommunityId);
            }
            else
            {
                Response.Redirect("~/error.aspx?error=" + (int)Constants.eERROR_TYPE.PROFILE_NOT_FOUND, true);
            }
        }

        #endregion

        #region Helper Functions

        private void SetEmbededControlOptions()
        {
            //add extra parameter so the data page doesn't have to go to the database to get them
            wvContainerFriends.AjaxDataPageParams += "&communityType=" + this.CurrentCommunity.CommunityTypeId + "&profileOwnerId=" + ProfileOwnerId;
            wvContainerWorlds.AjaxDataPageParams += "&communityType=" + (int)CommunityType.APP_3D + "&profileOwnerId=" + ProfileOwnerId;
        }

        /// <summary>
        /// GetOwnerData
        /// </summary>
        private void GetOwnerData()
        {
            //get the user id of the profile owner (community creator)
            ProfileOwnerId = this.CurrentCommunity.CreatorId;

            //get the user object
        }

        /// <summary>
        /// ConfigurePageSecurity
        /// </summary>
        private void SetPersonalizations()
        {
            string cssString = "";

            //set background color
            if ((CurrentCommunity.BackgroundRGB != null) && (CurrentCommunity.BackgroundRGB != ""))
            {
                cssString += "body{background:" + CurrentCommunity.BackgroundRGB + " none}";
            }

            //set background image
            if ((CurrentCommunity.BackgroundImage != null) && (CurrentCommunity.BackgroundImage != ""))
            {
                if (cssString.Length > 0)
                {
                    cssString = cssString.Substring(0, cssString.Length - 5) + "url(\"" + CurrentCommunity.BackgroundImage + " \") repeat 0px 0px}";
                }
                else
                {
                    cssString += "body{background: #ffffff url(\"" + CurrentCommunity.BackgroundImage + " \") repeat 0px 0px}";
                }
            }

            //set background image
            //thisCommunity.BackgroundImage;

            if (cssString.Length > 0)
            {
                cssString = "<style type=\"text/css\">\n\r<!--\n\r" + cssString + "\n\r-->\n\r</style>";
                cssHolder.Text = cssString;

                ((ProfilePageTemplate) Master).Footer.UseBlockFooter = true;
            }
        }

        /// <summary>
        /// ConfigurePageSecurity
        /// </summary>
        private void CheckCommunityAccess ()
        {
            //set the community data in the access control
            profileAccess.CommunityData = this.CurrentCommunity;
            
            //run the check in the control
            profileAccess.CheckUserAccess ();

            if (profileAccess.IsUserBlocked())
            {
                Response.Redirect ("~/fileNotFound.aspx?profile=");
            }

            //get the result of the access check
            userHasAccess = profileAccess.UserHasAccess ();
            userIsAdmin = profileAccess.UserIsAdministrator ();
        }

        /// <summary>
        /// UpdateChannelViews
        /// </summary>
        private void UpdateChannelViews()
        {
            if (KanevaWebGlobals.CurrentUser.UserId != this.CurrentCommunity.CreatorId)
            {
                //only do this when the page is viewed by other users
                GetCommunityFacade.UpdateChannelViews(KanevaWebGlobals.CurrentUser.UserId, this.CommunityId,
                    KanevaWebGlobals.CurrentUser.BrowseAnonymously, Common.GetVisitorIPAddress());
            }
        }

        private void SetNav()
        {
            if (Request.IsAuthenticated)
            {
                //setup header nav bar
                if (KanevaWebGlobals.CurrentUser.UserId == this.CurrentCommunity.CreatorId)
                {
                    ((ProfilePageTemplate) Master).HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
                    ((ProfilePageTemplate) Master).HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.PROFILE;
                    ((ProfilePageTemplate) Master).HeaderNav.SetNavVisible (((ProfilePageTemplate) Master).HeaderNav.MyKanevaNav, 2);
                }
                else
                {
                    ((ProfilePageTemplate) Master).HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.NONE;
                    ((ProfilePageTemplate) Master).HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.NONE;
                    ((ProfilePageTemplate) Master).HeaderNav.SetNavVisible (((ProfilePageTemplate) Master).HeaderNav.MyKanevaNav, 2);
                }
            }
            else
            {
                ((ProfilePageTemplate) Master).HeaderNav.MyKanevaNav.Visible = false;
                ((ProfilePageTemplate) Master).HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.NONE;
            }
        }

        private void GetRequestParams()
        {
            try
            {
                if (Request["communityId"] != null)
                {
                    //get channelId and find the default page
                    this.CommunityId = Convert.ToInt32(Request["communityId"]);
                }
                else
                {
                    this.CommunityId = GetPersonalChannelId();
                }
            }
            catch (Exception)
            {
            }
        }

        private void ConfigureTabbedControl()
        {
            // Create the tab control
            IList<TabDisplay> tabValues = new List<TabDisplay> ();

            tabValues.Add (LoadTab (Constants.TabType.BLAST, true));
            tabValues.Add (LoadTab (Constants.TabType.FRIEND));
            tabValues.Add (LoadTab (Constants.TabType.PERSONAL_INFO));
            tabValues.Add (LoadTab (Constants.TabType.PICTURE));
            tabValues.Add (LoadTab (Constants.TabType.VIDEO));
            tabValues.Add (LoadTab (Constants.TabType.MUSIC));
            tabValues.Add (LoadTab (Constants.TabType.GAME));
            tabValues.Add (LoadTab (Constants.TabType.WORLDS));
            tabValues.Add (LoadTab (Constants.TabType.BLOG));
            tabValues.Add (LoadTab (Constants.TabType.FAME));

            //configure the tabbed control
            TclMainPanel.TabValues = tabValues;
            TclMainPanel.HideEmptyTabs = false;
        }
            
        private TabDisplay LoadTab (string tabType)
        {
            return LoadTab (tabType, false);
        }
        private TabDisplay LoadTab (string tabType, bool isDefaultTab)
        {
            TabDisplay tab = new TabDisplay ();

            switch (tabType)
            {
                case Constants.TabType.BLAST:
                    {
                        tab.TabName = Constants.TabType.BLAST;
                        //      tab.ContainerToUse = "~/mykaneva/widgets/WidgetViewContainer.ascx";
                        tab.ControlToLoad = ResolveUrl ("~/mykaneva/blast/BlastsActive.ascx");
                        tab.AjaxDataPageParams = "?communityId=" + CommunityId;
                        tab.AjaxDataPage = "";
                        break;
                    }
                case Constants.TabType.FRIEND:
                    {
                        tab.TabName = Constants.TabType.FRIEND;
                        tab.AjaxDataPage = ResolveUrl ("~/mykaneva/widgets/FriendsData.aspx");
                        tab.AjaxDataPageParams = "?profileOwnerId=" + ProfileOwnerId + "&showgrid=true";
                        tab.ControlToLoad = "";
                        tab.AllowAlphaSorting = true;
                        tab.AllowSorting = true;
                        break;
                    }
                case Constants.TabType.PERSONAL_INFO:
                    {
                        tab.TabName = Constants.TabType.PERSONAL_INFO;
                        tab.ControlToLoad = ResolveUrl ("~/mykaneva/widgets/ProfileInfo.ascx");
                        tab.AjaxDataPageParams = "?profileOwnerId=" + ProfileOwnerId;
                        tab.AjaxDataPage = "";
                        break;
                    }
                case Constants.TabType.PICTURE:
                    {
                        tab.TabName = Constants.TabType.PICTURE;
                        tab.AjaxDataPage = ResolveUrl ("~/mykaneva/widgets/PhotosData.aspx");
                        tab.AjaxDataPageParams = "?communityId=" + CommunityId + "&communityType=" + (int) CommunityType.USER;
                        tab.AllowAlphaSorting = false;
                        tab.AllowSorting = true;
                        break;
                    }
                case Constants.TabType.VIDEO:
                    {
                        tab.TabName = Constants.TabType.VIDEO;
                        tab.AjaxDataPage = ResolveUrl ("~/mykaneva/widgets/MediaData.aspx");
                        tab.AjaxDataPageParams = "?communityId=" + CommunityId + "&assetTypeId=2&communityType=" + (int) CommunityType.USER;
                        tab.AllowAlphaSorting = false;
                        tab.AllowSorting = true;
                        break;
                    }
                case Constants.TabType.MUSIC:
                    {
                        tab.TabName = Constants.TabType.MUSIC;
                        tab.AjaxDataPage = ResolveUrl ("~/mykaneva/widgets/MediaData.aspx");
                        tab.AjaxDataPageParams = "?communityId=" + CommunityId + "&assetTypeId=4&communityType=" + (int) CommunityType.USER;
                        tab.AllowAlphaSorting = false;
                        tab.AllowSorting = true;
                        break;
                    }
                case Constants.TabType.GAME:
                    {
                        tab.TabName = Constants.TabType.GAME;
                        tab.AjaxDataPage = ResolveUrl ("~/mykaneva/widgets/MediaData.aspx");
                        tab.AjaxDataPageParams = "?communityId=" + CommunityId + "&assetTypeId=1&communityType=" + (int) CommunityType.USER;
                        tab.AllowAlphaSorting = false;
                        tab.AllowSorting = true;
                        break;
                    }
                case Constants.TabType.COMMUNITIES:
                    {
                        tab.TabName = Constants.TabType.COMMUNITIES;
                        tab.AjaxDataPage = ResolveUrl ("~/mykaneva/widgets/MyCommunitiesData.aspx");
                        tab.AjaxDataPageParams = "?profileOwnerId=" + ProfileOwnerId + "&communityId=" + CommunityId + "&communityType=" + (int) CommunityType.COMMUNITY;
                        tab.ControlToLoad = "";
                        tab.AllowAlphaSorting = true;
                        tab.AllowSorting = true;
                        break;
                    }
                case Constants.TabType.BLOG:
                    {
                        tab.TabName = Constants.TabType.BLOG;
                        tab.AjaxDataPage = ResolveUrl ("~/mykaneva/widgets/BlogData.aspx");
                        tab.AjaxDataPageParams = "?communityId=" + CommunityId + "&communityType=" + (int) CommunityType.USER;
                        tab.ControlToLoad = "";
                        break;
                    }
                case Constants.TabType.FAME:
                    {
                        tab.TabName = Constants.TabType.FAME;
                        tab.AjaxDataPage = ResolveUrl ("~/mykaneva/widgets/ProfileFameData.aspx");
                        tab.AjaxDataPageParams = "?communityId=" + CommunityId;
                        tab.ControlToLoad = "";
                        break;
                    }
                case Constants.TabType.WORLDS:
                    {
                        tab.TabName = Constants.TabType.WORLDS;
                        tab.AjaxDataPage = ResolveUrl ("~/mykaneva/widgets/MyWorldsData.aspx");
                        tab.AjaxDataPageParams = "?profileOwnerId=" + ProfileOwnerId + "&communityId=" + CommunityId + "&communityType=" + (int) CommunityType.APP_3D;
                        tab.ControlToLoad = "";
                        tab.AllowAlphaSorting = false;
                        tab.AllowSorting = false;
                        break;
                    }
            }

            if (isDefaultTab) TclMainPanel.DefaultTab = tab;
            return tab;
        }
 
        #endregion

        #region Attributes

        public int CommunityId
        {
            get
            {
                return this.communityId;
            }
            set
            {
                this.communityId = value;
            }
        }

        protected int CommunityTypeToUse()
        {
            return (int)CommunityType.COMMUNITY;
        }

        public int ProfileOwnerId
        {
            get
            {
                return this.profileOwnerId;
            }
            set
            {
                this.profileOwnerId = value;
            }
        }

        //public Community CurrentCommunity
        //{
        //    get
        //    {
        //        try
        //        {
        //            if (ViewState["CurrentCommunity"] == null)
        //            {
        //                ViewState["CurrentCommunity"] = GetCommunityFacade.GetCommunity(this.CommunityId);
        //            }
        //            return (Community)ViewState["CurrentCommunity"];
        //        }
        //        catch { return new Community(); }
        //    }
        //    set
        //    {
        //        ViewState["CurrentCommunity"] = value;
        //    }
        //}

        public Community CurrentCommunity
        {
            get
            {
                try
                {
                    if (currentCommunity == null)
                    {
                        currentCommunity = GetCommunityFacade.GetCommunity(this.CommunityId);
                    }
                    return currentCommunity;
                }
                catch { return new Community(); }
            }
            set
            {
                currentCommunity = value;
            }
        }

        #endregion

        #region Web Form Designer generated code

        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new EventHandler(this.Page_Load);
        }

        #endregion

    }
}
