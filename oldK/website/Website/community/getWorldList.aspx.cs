///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.community
{
    public partial class getWorldList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Return the world list
            string worldName = "";

            if (Request["name"] != null)
            {
                worldName = Request["name"].ToString();
            }

            GameFacade gameFacade = new GameFacade();
            PagedList<Community> communities = gameFacade.SearchWorldPrepopulation (worldName, "name desc", 1, Int32.MaxValue);

            Response.Clear();

            foreach (Community community in communities)
            {
                Response.Write(community.Name + ";");
            }
        }
    }
}