///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using KlausEnt.KEP.Kaneva.mykaneva.widgets;
using System.Collections.Generic;
using log4net;

namespace KlausEnt.KEP.Kaneva.channel
{
    public partial class CommunityPage : BasePage
    {
        #region Declarations

        private Community currentCommunity = null;
        private int communityId = 0;
        private bool userHasAccess = false;
        private bool userIsAdmin = false;
        private int communityOwnerId = 0;
        private int defaultTabId = 0;
        private IList<CommunityTab> commTabs; // = new List<CommunityTab> ();
        private bool suppressWidgetPagination = false;
        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        #endregion

        #region Page Load

        protected void Page_Load(object sender, EventArgs e)
        {
            //prevent error on cross-page postbacks
            if (!IsCrossPagePostBack)
            {
                //get the required parameters
                GetRequestParams();

                if (CommunityId > 0)
                {
                    // Make sure this is not a personal community
                    if (this.CurrentCommunity.IsPersonal == 1)
                    {
                        Response.Redirect ("~/channel/" + this.CurrentCommunity.NameNoSpaces + ".people");
                    }
                    
                    //check the users Access
                    CheckCommunityAccess();

                    if (userHasAccess)
                    {
                        //get owner data
                        GetOwnerData();

                        //amend the ajax parameter for the embeded usercontrols
                        SetEmbededControlOptions();

                        //set the profile configurations
                        ConfigureTabbedControl ();

                        divLeftProfilePanel.Visible = true;
                        divRightProfilePanel.Visible = true;
                        divCenterPanel.Visible = false;

                        //update the views for the profile
                        if (!Page.IsPostBack)
                        {
                            UpdateChannelViews();
                        }
                    }
                    else if (!userHasAccess)
                    {
                        // If community id = 0 after GetCommunity then world is not active
                        if (CurrentCommunity.CommunityId == 0)
                        {
                            Response.Redirect ("~/community/commDrillDown.aspx", true);
                        }

                        //clear controls to prevent them from processing
                        //divLeftProfilePanel.Controls.Clear();
                        divRightProfilePanel.Controls.Clear();
                        noAccessLayer.Controls.Clear();

                        //set display
                        noAccessLayer.Visible = false;
                        divLeftProfilePanel.Visible = true;
                        divRightProfilePanel.Visible = false;
                        divCenterPanel.Visible = true;
                    }

                    //set any personalizations of the community
                    SetPersonalizations();

                    //set the navigation
                    SetNav();

                    // Set value in HttpContext so widgets can have access to it.
                    HttpContext.Current.Items.Add ("community", this.CurrentCommunity);
                }
                else
                {
            //        Response.Redirect("~/error.aspx?error=" + (int)Constants.eERROR_TYPE.COMMUNITY_NOT_FOUND, true);
                    Response.Redirect ("~/communities/commDrillDown.aspx", true);
                }
            }
//            else
//            {
//                Response.Redirect(this.GetLoginURL());
//            }

            if (!IsPostBack)
            {
                // Request tracking
                if (Request.Params[Constants.cREQUEST_TRACKING_URL_PARAM] != null && GetUserFacade.GetTrackingRequestType(Request[Constants.cREQUEST_TRACKING_URL_PARAM].ToString()) != Constants.cREQUEST_TRACKING_REQUEST_TYPE_ID_WEB_WORLD_CREATION)
                {
                    int iResult = GetUserFacade.AcceptTrackingRequest(Request.Params[Constants.cREQUEST_TRACKING_URL_PARAM], KanevaWebGlobals.CurrentUser.UserId);
                }
            }
        }

        #endregion

        #region Helper Functions

        private void SetEmbededControlOptions ()
        {
            //add extra parameter so the data page doesn't have to go to the database to get them
            wvContainerLeftCol.ID = "wvContainerEvents";
            wvContainerLeftCol.ControlToLoad = "mykaneva/widgets/EventsList.ascx";
            wvContainerLeftCol.AjaxDataPage = "/mykaneva/widgets/EventsData.aspx";
            wvContainerLeftCol.EditButtonLink = "/mykaneva/events/eventForm.aspx";
            wvContainerLeftCol.AjaxDataPageParams = "pagesize=6&showgrid=false&communityType=" + this.CurrentCommunity.CommunityTypeId + "&profileOwnerId=" + CommunityOwnerId;
            wvContainerLeftCol.Title = "Events";

            wvContainerMembers.AjaxDataPageParams += "&communityType=" + this.CurrentCommunity.CommunityTypeId + "&profileOwnerId=" + CommunityOwnerId;
        }

        /// <summary>
        /// GetOwnerData
        /// </summary>
        private void GetOwnerData()
        {
            //get the user id of the profile owner (community creator)
            CommunityOwnerId = this.CurrentCommunity.CreatorId;

            //get the user object
        }

        /// <summary>
        /// ConfigurePageSecurity
        /// </summary>
        private void SetPersonalizations()
        {
            string cssString = "";

            //set background color
            if ((CurrentCommunity.BackgroundRGB != null) && (CurrentCommunity.BackgroundRGB != ""))
            {
                cssString += "body{background:" + CurrentCommunity.BackgroundRGB + " none}";
            }

            //set background image
            if ((CurrentCommunity.BackgroundImage != null) && (CurrentCommunity.BackgroundImage != ""))
            {
                if (cssString.Length > 0)
                {
                    cssString = cssString.Substring(0, cssString.Length - 5) + "url(\"" + CurrentCommunity.BackgroundImage + " \") repeat 0 0}";
                }
                else
                {
                    cssString += "body{background: #fff url(\"" + CurrentCommunity.BackgroundImage + " \") repeat 0 0}";
                }
            }

            //set background image
            //thisCommunity.BackgroundImage;

            if(cssString.Length > 0)
            {
                cssString = "<style type=\"text/css\">\n\r<!--\n\r" + cssString + "\n\r-->\n\r</style>";
                cssHolder.Text = cssString;

                ((ProfilePageTemplate) Master).Footer.UseBlockFooter = true;
            }
        }

        /// <summary>
        /// ConfigurePageSecurity
        /// </summary>
        private void CheckCommunityAccess ()
        {
            //set the community data in the access control
            profileAccess.CommunityData = this.CurrentCommunity;
            
            //run the check in the control
            profileAccess.CheckUserAccess ();

            if (profileAccess.IsUserBlocked ())
            {
                Response.Redirect ("~/fileNotFound.aspx?profile=");
            }

            //get the result of the access check
            userHasAccess = profileAccess.UserHasAccess ();
            userIsAdmin = profileAccess.UserIsAdministrator ();
        }

        /// <summary>
        /// UpdateChannelViews
        /// </summary>
        private void UpdateChannelViews()
        {
            if (KanevaWebGlobals.CurrentUser.UserId != this.CurrentCommunity.CreatorId)
            {
                //only do this when the page is viewed by other users
                GetCommunityFacade.UpdateChannelViews(KanevaWebGlobals.CurrentUser.UserId, this.CommunityId,
                    KanevaWebGlobals.CurrentUser.BrowseAnonymously, Common.GetVisitorIPAddress());
            }
        }

        private void SetNav()
        {
            if (Request.IsAuthenticated)
            {
                if (KanevaWebGlobals.CurrentUser.UserId == this.CurrentCommunity.CreatorId)
                {
                    ((ProfilePageTemplate) Master).HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
                    ((ProfilePageTemplate) Master).HeaderNav.SetNavVisible (((ProfilePageTemplate) Master).HeaderNav.MyKanevaNav, 2);

                    if (CurrentCommunity.CommunityTypeId == (int) CommunityType.APP_3D)
                    {
                        ((ProfilePageTemplate) Master).HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.MY3DAPPS;
                    }
                    else
                    {
                       ((ProfilePageTemplate) Master).HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.CHANNELS;
                    }
                }
                else
                {
                    ((ProfilePageTemplate) Master).HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.CONNECT;
                    ((ProfilePageTemplate) Master).HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.NONE;
                    ((ProfilePageTemplate) Master).HeaderNav.SetNavVisible (((ProfilePageTemplate) Master).HeaderNav.MyKanevaNav, 2);
                }
            }
            else
            {
                ((ProfilePageTemplate) Master).HeaderNav.MyKanevaNav.Visible = false;
                ((ProfilePageTemplate) Master).HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.NONE;
            }
        }

        private void GetRequestParams()
        {
            try
            {
                if (Request["communityId"] != null)
                {
                    //get channelId and find the default page
                    this.CommunityId = Convert.ToInt32 (Request["communityId"]);
                }

                if (Request["tab"] != null && Request["tab"].Length > 0)
                {
                    defaultTabId = Convert.ToInt32(Request["tab"]);
                }

                if (Request["swpag"] != null)
                {
                    suppressWidgetPagination = Convert.ToBoolean(Request["swpag"]);
                }
            }
            catch (Exception){}
        }

        private void ConfigureTabbedControl ()
        {
            // Create the tab control
            IList<TabDisplay> tabValues = new List<TabDisplay> ();

            bool displayAll = true;
            foreach (CommunityTab tab in CommunityTabs) 
            {
                if (tab.CommunityId > 0) 
                {
                    displayAll = false;
                    break;
                }
            }

            foreach (CommunityTab tab in CommunityTabs) 
            {
                if (displayAll || tab.CommunityId > 0)
                {
                    // If tabs have not been configured yet, set Blast as the default
                    if ((displayAll && tab.TabId == (int) Constants.eTab.Blast && defaultTabId == 0) || tab.TabId == defaultTabId)
                    {
                        tab.IsDefault = true;
                    }
                    else if (defaultTabId > 0)
                    {
                        tab.IsDefault = false;
                    }

                    // Per Animesh request, continue to remove badges and leaderboards from non 3dapps.
                    if (Is3DApp || (tab.TabId != (int)Constants.eTab.Badges && tab.TabId != (int)Constants.eTab.Leaders))
                    {
                        tabValues.Add(LoadTab(tab, tab.IsDefault));
                    }
                }
            }

            //configure the tabbed control
            TclMainPanel.TabValues = tabValues;
            TclMainPanel.HideEmptyTabs = false;
        }

        private TabDisplay LoadTab (CommunityTab commTab, bool isDefaultTab)
        {
            TabDisplay tab = new TabDisplay ();

            switch (commTab.TabId)
            {
                case (int) Constants.eTab.About:
                    {
                        tab.TabName = commTab.TabName; //Constants.TabType.ABOUT;
                        tab.TabId = commTab.TabId;
                        tab.ControlToLoad = ResolveUrl ("~/mykaneva/widgets/About.ascx");
                        tab.AjaxDataPageParams = "?communityId=" + CommunityId;
                        break;
                    }
                case (int) Constants.eTab.Badges:
                    {
                        tab.TabName = commTab.TabName; //Constants.TabType.BADGE;
                        tab.TabId = commTab.TabId;
                        tab.AjaxDataPage = ResolveUrl ("~/mykaneva/widgets/BadgesData.aspx");
                        tab.AjaxDataPageParams = "?communityId=" + CommunityId + "&appCommunityId=" + CommunityId;
                        break;
                    }
                case (int) Constants.eTab.Blast:
                    {
                        tab.TabName = commTab.TabName; //Constants.TabType.BLAST;
                        tab.TabId = commTab.TabId;
                        tab.ControlToLoad = ResolveUrl ("~/mykaneva/blast/BlastsActive.ascx");
                        tab.AjaxDataPageParams = "?communityId=" + CommunityId;
                        break;
                    }
                case (int) Constants.eTab.Blog:
                    {
                        tab.TabName = commTab.TabName; //Constants.TabType.BLOG;
                        tab.TabId = commTab.TabId;
                        tab.AjaxDataPage = ResolveUrl ("~/mykaneva/widgets/BlogData.aspx");
                        tab.AjaxDataPageParams = "?communityId=" + CommunityId + "&communityType=" + (int) CommunityType.COMMUNITY;
                        break;
                    }
                case (int) Constants.eTab.Events:
                    {
                        tab.TabName = commTab.TabName; //Constants.TabType.EVENT;
                        tab.TabId = commTab.TabId;
                        tab.AjaxDataPage = ResolveUrl ("~/myKaneva/widgets/EventsData.aspx");
                        tab.AjaxDataPageParams = "?communityId=" + CommunityId;
                        break;
                    }
                case (int) Constants.eTab.Forums:
                    {
                        tab.TabName = commTab.TabName; //Constants.TabType.FORUM;
                        tab.TabId = commTab.TabId;
                        tab.AjaxDataPage = ResolveUrl ("~/mykaneva/widgets/ForumsData.aspx");
                        tab.AjaxDataPageParams = "?communityId=" + CommunityId;
                        break;
                    }
                case (int) Constants.eTab.Games:
                    {
                        tab.TabName = commTab.TabName; //Constants.TabType.GAME;
                        tab.TabId = commTab.TabId;
                        tab.AjaxDataPage = ResolveUrl ("~/mykaneva/widgets/MediaData.aspx");
                        tab.AjaxDataPageParams = "?communityId=" + CommunityId + "&assetTypeId=1&communityType=" + (int) CommunityType.COMMUNITY;
                        tab.AllowAlphaSorting = false;
                        tab.AllowSorting = true;
                        break;
                    }
                case (int) Constants.eTab.Leaders:
                    {
                        tab.TabName = commTab.TabName; //Constants.TabType.LEADERBOARD;
                        tab.TabId = commTab.TabId;
                        tab.AjaxDataPage = ResolveUrl ("~/mykaneva/widgets/LeaderBoardData.aspx");
                        tab.AjaxDataPageParams = "?communityId=" + CommunityId + "&gameId=" + CurrentCommunity.WOK3App.GameId;
                        break;
                    }
                case (int) Constants.eTab.Members:
                    {
                        tab.TabName = commTab.TabName; //Constants.TabType.MEMBER;
                        tab.TabId = commTab.TabId;
                        tab.AjaxDataPage = ResolveUrl ("~/mykaneva/widgets/CommunityMembersData.aspx");
                        tab.AjaxDataPageParams = "?communityId=" + CommunityId;
                        tab.AllowAlphaSorting = true;
                        tab.AllowSorting = true;
                        break;
                    }
                case (int) Constants.eTab.Music:
                    {
                        tab.TabName = commTab.TabName; //Constants.TabType.MUSIC;
                        tab.TabId = commTab.TabId;
                        tab.AjaxDataPage = ResolveUrl ("~/mykaneva/widgets/MediaData.aspx");
                        tab.AjaxDataPageParams = "?communityId=" + CommunityId + "&assetTypeId=4&communityType=" + (int) CommunityType.COMMUNITY;
                        tab.AllowAlphaSorting = false;
                        tab.AllowSorting = true;
                        break;
                    }
                case (int) Constants.eTab.Photos:
                    {
                        tab.TabName = commTab.TabName; //Constants.TabType.PICTURE;
                        tab.TabId = commTab.TabId;
                        tab.AjaxDataPage = ResolveUrl ("~/mykaneva/widgets/PhotosData.aspx");
                        tab.AjaxDataPageParams = "?communityId=" + CommunityId + "&communityType=" + (int) CommunityType.COMMUNITY;
                        tab.AllowAlphaSorting = false;
                        tab.AllowSorting = true;
                        break;
                    }
                case (int) Constants.eTab.Premium_Items:
                    {
                        tab.TabName = commTab.TabName; //Constants.TabType.PREMIUM_ITEMS;
                        tab.TabId = commTab.TabId;
                        tab.AjaxDataPage = ResolveUrl ("~/mykaneva/widgets/PremiumItemsData.aspx");
                        tab.AjaxDataPageParams = "?communityId=" + CommunityId + "&gameId=" + CurrentCommunity.WOK3App.GameId;
                        break;
                    }
                case (int) Constants.eTab.Videos:
                    {
                        tab.TabName = commTab.TabName; //Constants.TabType.VIDEO;
                        tab.TabId = commTab.TabId;
                        tab.AjaxDataPage = ResolveUrl ("~/mykaneva/widgets/MediaData.aspx");
                        tab.AjaxDataPageParams = "?communityId=" + CommunityId + "&assetTypeId=2&communityType=" + (int) CommunityType.COMMUNITY;
                        tab.AllowAlphaSorting = false;
                        tab.AllowSorting = true;
                        break;
                    }
            }
            if (isDefaultTab) TclMainPanel.DefaultTab = tab;
            return tab;
        }

        #endregion

        #region Attributes

        public IList<CommunityTab> CommunityTabs
        {
            get
            {
                try
                {
                    if (commTabs == null)
                    {
                        commTabs = GetCommunityFacade.GetCommunityTabs (CurrentCommunity.CommunityId, 3);
                    }
                    return commTabs;
                }
                catch { return new List<CommunityTab> (); }
            }
        }

        public int CommunityId
        {
            get
            {
                return this.communityId;
            }
            set
            {
                this.communityId = value;
            }
        }

        public int CommunityOwnerId
        {
            get
            {
                return this.communityOwnerId;
            }
            set
            {
                this.communityOwnerId = value;
            }
        }

        public Community CurrentCommunity
        {
            get
            {
                try
                {
                    if (currentCommunity == null)
                    {
                        currentCommunity = GetCommunityFacade.GetCommunity(this.CommunityId);

                        if (currentCommunity.CommunityId > 0 &&
                            currentCommunity.CommunityTypeId == (int) CommunityType.APP_3D)
                        {
                            int x = currentCommunity.WOK3App.GameId;
                        }
                    }
                    return currentCommunity;
                }
                catch { return new Community(); }
            }
            set
            {
                currentCommunity = value;
            }
        }

        public bool UserIsAdmin
        {
            get
            {
                return this.userIsAdmin;
            }
            set
            {
                this.userIsAdmin = value;
            }
        }
        
        private bool Is3DApp
        {
            get
            {
                if (CurrentCommunity.CommunityTypeId == (int) CommunityType.APP_3D ||
                    CurrentCommunity.CommunityTypeId == (int) CommunityType.FAME)
                {
                    return true;
                }
                return false;
            }
        }

        #endregion

        #region Web Form Designer generated code

        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new EventHandler(this.Page_Load);
        }

        #endregion

    }
}
