<%@ Page Language="C#" MasterPageFile="~/masterpages/GenericPageTemplate.Master" ValidateRequest="False" AutoEventWireup="false" CodeBehind="ProfileEdit.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.community.ProfileEdit" %>
<%@ Register TagPrefix="Kaneva" TagName="LibraryImages" Src="../usercontrols/SelectPhotoFromLibrary.ascx" %>

<asp:Content ID="cnt_ProfileHead" runat="server" contentplaceholderid="cph_HeadData" >
    <meta name="title" content="Kaneva. The Online Community and Social Network." />
    <meta name="description" content="Kaneva is the first to combine social networking and a free 3D virtual world. It's a whole new way to connect with friends. Have fun in a vibrant virtual world full of people, cool places and fun games. Create your avatar and get a free 3D home today!"/>
    <meta name="keywords" content="free virtual world, virtual world, virtual worlds, 3D world, online avatar community, mmo game, virtual reality, caneva, kaniva" />
    
    <NOSCRIPT><META HTTP-EQUIV="refresh" CONTENT="1" URL="noJavascript.aspx" /></NOSCRIPT>
    
    <link href="../css/Communities/ProfileEdit.css" type="text/css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="cntBottomeJS" runat="server" contentplaceholderid="cphBottomPageJS" >
    <script type="text/javascript" src="../jscript/prototype-1.6.1-min.js"></script>
    <script type="text/javascript" src="../jscript/Communities/ProfileEdit-min.js"></script>
    <script type="text/javascript" src="../jscript/PageLayout/colorpicker.js"></script>
</asp:Content>

   
<asp:Content ID="mainContent" runat="server" contentplaceholderid="cph_Body">
    <div id="editBody">
	    <div id="messageArea">
	        <asp:ValidationSummary ShowMessageBox="False" ShowSummary="True" cssclass="errBox" id="valSum" DisplayMode="BulletList" runat="server" HeaderText="Please correct the following errors:" />
	        <div id="alertMsg" runat="server" class="errBox" visible="false"></div>
	    </div>
        <div id="pageHeader">
            <div>
                <h6><span>EDIT:</span>Profile Options</h6>
            </div>
        </div>

        <div id="profilePhoto">
            <h4>Profile Photo</h4>
            <div>
                <div class="photo">
                    <img src="" alt="" id="imgThumbnail" runat="server" />
                </div>
                <div class="photoDescription info">We recommend 200 x 200</div>                
            </div>                
	        <div class="photoLink">
                <input runat="server" ID="inpThumbnail" type="file" size="45" /><span class="spacer"></span>
			    <asp:linkbutton id="btnClearThumb" OnClientClick="return confirm('This will remove your personal photo, are you sure you wish to proceed?');" onclick="btnClearThumb_Click" runat="Server" Text="Remove" CausesValidation="False"></asp:linkbutton>
	            <asp:linkbutton id="btnFBProfilePic" clientidmode="Static" onclick="btnFBProfilePic_Click" runat="Server" Text="Use my Facebook Profile Photo" CausesValidation="False"></asp:linkbutton>
	        </div>  
	    </div>

        <div id="viewOptions">
            <h4>View Options</h4>
			<div id="accessGroups">
			    Who can see this profile? 
			    <asp:RadioButtonList id="profileAccess" CssClass="items" RepeatDirection="Horizontal" RepeatLayout="Flow" AutoPostBack="false" CausesValidation="false" runat="server">
			        <asp:ListItem Text="Anyone" Value="Y" ></asp:ListItem>
			        <asp:ListItem Text="Only Me" Value="I"></asp:ListItem>
			        <asp:ListItem Text="Friends Only" Value="N"></asp:ListItem>
			    </asp:RadioButtonList>
			</div>
			<div id="showOptions">
                <asp:CheckBox ID="cbShowGender" Runat="server" Text="Show Gender" /><br />
                <asp:CheckBox ID="cbShowLocation" Runat="server" Text="Show Location" /><br />
                <asp:CheckBox ID="cbShowAge" Runat="server" Text="Show Age" /><br />
   			</div>
        </div>        
        
		<div id="contentRestrictions">
            <h4>Age and Content Restrictions</h4>
			<div style="margin-bottom:10px">
				<asp:checkbox runat="server" id="chkContainsMature"/>&nbsp;My profile contains Mature content that may not be appropriate for viewers under 18 years old.
			</div>
	    </div>
		
		<div id="backgroundOptions">
            <h4>Background (Optional)</h4>
            <p>
			    Background Color<span style="padding-left:50px"></span><asp:hyperlink cssclass="colorPicker" id="txtBackgroundColorAppClrPickerStr" Runat="server" ImageUrl="..\images\colorpickmask.gif" CausesValidation="False"></asp:hyperlink>
			    <asp:textbox id="txtBackgroundColor" runat="server" MaxLength="8"></asp:textbox>
			</p>
			<p>
			    Background Photo URL<span style="padding-left:16px"></span><asp:textbox id="txtBGPicture" style="width:400px" runat="server" MaxLength="255"></asp:textbox><span class="spacer"></span><A href="javascript:void(0);" runat="server" id="aRemove">Clear Photo URL</A>
                <span class="spacer"></span><asp:linkbutton PostBackUrl="#library" id="selectPhoto" onclick="selectPhoto_Click" runat="Server" Text="Select from Library..." CausesValidation="False"></asp:linkbutton>
			</p>
			    <!-- start hidden div to allow user to select from their media library-->
			<div id="libraryImages" runat="server">
			    <a name="library"></a>
			    <kaneva:LibraryImages id="libraryImageSelector" runat="server" Visible="false" />
			</div>
			<!-- end div -->           
        </div>
        
		<div id="communityURL">
            <div id="divRegistrationErr" style="MARGIN-TOP: 15px; COLOR: red" runat="server" visible="false"></div>
            <h4>Reserve Your Profile URL</h4>
            <!-- START CONTENT -->
            <span class="info">This short URL allows you to promote your profile in blogs and email signatures with a simple URL that you define</span>
            <p>
                <SPAN id="spnRegistered" runat="server" visible="false">Your URL has already been registered.</span><br />
                <asp:label id="lblURL" runat="server"></asp:label>
                <asp:textbox id="txtURL" runat="server" maxlength="50" Width="230"></asp:textbox><br />			
			    <asp:regularexpressionvalidator id="revURL" runat="server" controltovalidate="txtURL" display="Dynamic" text="URL should be at least four characters and can contain only letters, numbers and underscore."></asp:regularexpressionvalidator>           
            </p>
            <p>
                <div id="urlWarning" runat="server"><span class="warning">This short URL is permanent and cannot be changed after you reserve it.</span></div>
            </p>
            <!-- END CONTENT -->																			
		</div>
       
		<div id="submitArea">
            <asp:linkbutton class="btnsmall grey" id="btnUpdate" onclick="btnUpdate_Click" runat="Server" Text="Save Changes" CausesValidation="True"></asp:linkbutton>
            <asp:linkbutton class="btnsmall grey" id="btnCancel" onclick="btnCancel_Click" runat="Server" Text="Cancel" CausesValidation="False"></asp:linkbutton>
		</div>
	</div>
    
</asp:Content>