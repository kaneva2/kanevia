<%@ Page Language="C#" MasterPageFile="~/masterpages/ProfilePageTemplate.Master" AutoEventWireup="false" CodeBehind="ProfilePage.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.channel.ProfilePage" %>
<%@ Register TagPrefix="Kaneva" TagName="wvContainer" Src="~/mykaneva/widgets/WidgetViewContainer.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="ProfileAccess" Src="~/mykaneva/widgets/ProfileAccess.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="TCLoader" Src="~/usercontrols/TabbedControlLoader.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="MKContainer" Src="~/usercontrols/doodad/ContainerMyKaneva.ascx" %>

<asp:Content ID="cntProfileHead" runat="server" contentplaceholderid="cphHeadData" >
    <meta name="title" content="Kaneva. The Online Community and Social Network." />
    <meta name="description" content="Kaneva is the first to combine social networking and a free 3D virtual world. It's a whole new way to connect with friends. Have fun in a vibrant virtual world full of people, cool places and fun games. Create your avatar and get a free 3D home today!"/>
    <meta name="keywords" content="free virtual world, virtual world, virtual worlds, 3D world, online avatar community, mmo game, virtual reality, caneva, kaniva" />
    
    <script type="text/javascript" src="../jscript/prototype-1.6.1-min.js"></script>
    <script type="text/javascript" src="../jscript/scriptaculous/1.8.2/scriptaculous.js"></script>
    <script type="text/javascript" src="../jscript/yahoo/2.7.0/yahoo-dom-event.js"></script>
    <script type="text/javascript" src="../jscript/yahoo/2.7.0/element-min.js"></script>
    <script type="text/javascript" src="../jscript/yahoo/2.7.0/paginator-min.js"></script>
    <script type="text/javascript" src="../jscript/Communities/CommunityPage-min.js"></script>
    <script type="text/javascript" src="../jscript/SWFObject/swfobject-min.js"></script>
    
    <script type="text/javascript" src="../jscript/dw_tooltip/dw_event.js"></script>
    <script type="text/javascript" src="../jscript/dw_tooltip/dw_viewport.js"></script>
    <script type="text/javascript" src="../jscript/dw_tooltip/dw_tooltip.js"></script>

    <NOSCRIPT><META HTTP-EQUIV="refresh" CONTENT="1" URL="noJavascript.aspx" /></NOSCRIPT>

    <link href="../css/Communities/ProfilePage.css?v5" type="text/css" rel="stylesheet" />
    <asp:Literal id="cssHolder" runat="server" />
    
</asp:Content>	 

<asp:Content id="cntProfielLeft" runat="server" contentplaceholderid="cphLeftBody" >
    <div id="divLeftProfilePanel" runat="server" class="LeftProfilePanel">
		<kaneva:MKContainer id="wvContainer1" runat="server" ControlToLoad="mykaneva/widgets/ProfileControlPanel.ascx" />
		<div id="noAccessLayer" runat="server">
            <kaneva:wvContainer id="wvFame" runat="server" ControlToLoad="mykaneva/widgets/FameList.ascx" AjaxDataPage="/mykaneva/widgets/FameData.aspx" AjaxDataPageParams="pagesize=5" Title="Kaneva Fame"/>
		    <kaneva:wvContainer id="wvContainerFriends" runat="server" ControlToLoad="mykaneva/widgets/FriendsList.ascx" AjaxDataPage="/mykaneva/widgets/FriendsData.aspx" EditButtonLink="/mykaneva/friendsGroups.aspx" AjaxDataPageParams="pagesize=6&orderby=Rand&showgrid=false" Title="Friends"/>
		    <kaneva:wvContainer id="wvContainerWorlds" runat="server" ControlToLoad="mykaneva/widgets/MyCommunitiesList.ascx" AjaxDataPage="/mykaneva/widgets/MyWorldsData.aspx" EditButtonLink="/mykaneva/my3dapps.aspx" AjaxDataPageParams="pagesize=4&showgrid=false" Title="Worlds"/>
		</div>
    </div>
</asp:Content>

<asp:Content ID="cntProfileRight" runat="server" contentplaceholderid="cphRightBody" >
    <div id="divRightProfilePanel" runat="server" class="RightProfilePanel">
		<kaneva:Tcloader id="TclMainPanel" runat="server" />	
   </div>
    <div id="divCenterPanel" runat="server" class="NoAccessPanel">
		<kaneva:profileaccess id="profileAccess" runat="server" />
    </div>    
</asp:Content>
