<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CommunityInviteWithMessage.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.CommunityInviteWithMessage" %>

<link id="styleSheet" rel="stylesheet" href="../../css/Kaneva.css" type="text/css"/>
<link id="Link1" rel="stylesheet" href="../../css/navigation2.css" type="text/css"/>
<link href="../../css/themes-join/template.css" rel="stylesheet" type="text/css">

<style type="text/css">
<!--
img {
	border-width: 0px;
	border-style: none;
}
.title {
	padding-bottom:15px;
	border-bottom:solid 1px #dddddd;
}
.title h1 {
	float: left;
}
.title .indicator {
	background: url("../../images/connectwithfriends/status_backer.gif") repeat-x 0px 0px;
	height: 33px;
	float: right;
	width: 311px;
}
.title .indicator .lEnd {
	float: left;
	width: 10px;
	background: url("../../images/connectwithfriends/status_leftEnd.gif") no-repeat 0px 0px;
	height: 33px;
}
.title .indicator .step1 {
	float: left;
	width: 91px;
	background: url("../../images/connectwithfriends/status_1.gif") no-repeat 0px 0px;
	height: 33px;
	text-indent: -5000px;
}
.title .indicator .step1_on {
	background: url("../../images/connectwithfriends/status_1-on.gif") no-repeat 0px 0px;
	float: left;
	width: 91px;
	height: 33px;
	text-indent: -5000px;
}
.title .indicator .step2 {
	float: left;
	width: 103px;
	background: url("../../images/connectwithfriends/status_2.gif") no-repeat 0px 0px;
	height: 33px;
	text-indent: -5000px;
	margin-right: 3px;
	margin-left: 3px;
}
.title .indicator .step2_on {
	background: url("../../images/connectwithfriends/status_2-on.gif") no-repeat 0px 0px;
	float: left;
	width: 103px;
	height: 33px;
	text-indent: -5000px;
}
.title .indicator .step3 {
	float: left;
	width: 91px;
	background: url("../../images/connectwithfriends/status_3.gif") no-repeat 0px 0px;
	height: 33px;
	text-indent: -5000px;
}
.title .indicator .step3_on {
	background: url("../../images/connectwithfriends/status_3-on.gif") no-repeat 0px 0px;
	float: left;
	width: 91px;
	height: 33px;
	text-indent: -5000px;
}
.title .indicator .rEnd {
	float: left;
	width: 10px;
	background: url("../../images/connectwithfriends/status_rightEnd.gif") no-repeat 0px 0px;
	height: 33px;
}
.col1 {
	width: 248px;
	float: left;
}
.col1 p {
	padding-left: 20px;
}
.col1 h3 {
	margin: 30px 0px 0px;
	padding: 0px 0px 0px 20px;
}
.flag {
	background: url("../../images/connectwithfriends/flag.gif") no-repeat right 0px;
	height: 33px;
	text-align: right;
	padding-right: 20px;
	line-height: 1em;
	margin-top: 15px;
}
.flag a:link, .flag a:visited {
	color: #FFFFFF;
	text-decoration: none;
	font-weight: bold;
}
.flag a:hover {
	background-color:transparent;
	font-weight: bold;
	color: #476163;
	text-decoration: none;
}
.col1 .exMTop {
	margin-top: 60px;
}
.col2 {
	float: right;
	width: 512px;
	background: url("../../images/connectwithfriends/col2_backer.jpg") no-repeat 0px 0px;
	height: 374px;
	padding-top: 14px;
}
.col2 h3 {
	padding-left: 20px;
	padding-top: 3px;
	margin: 0px;
	display: block;
}
.col2 h5 {
	margin: 0px 0px 0px 20px;
	padding: 0px;
	font-size: 10px;
	line-height: 14px;
}
.col2 ul {
	margin: 0px;
	padding: 0px 0px 0px 8px;
	list-style: none;
}
.col2 li {
	float: left;
}
.col2 table.cif_emailTable {
	margin: 0px 0px 5px 20px;
	padding: 0px;
	width: 475px;
	border-collapse:collapse;
	border-width: 0px;
	border-style: none;
}
.col2 table.cif_emailTable tr {}
.col2 table.cif_emailTable th {
	font-weight: bold;
	text-align: right;
	width: 75px;
	vertical-align: top;
	padding: 4px;
}
.col2 table.cif_emailTable td {
	padding: 4px;
	text-align: left;
	vertical-align: top;
}


.col2 td a:link, .col2 td a:visited {
	background: url("../../images/connectwithfriends/btn_other.jpg") no-repeat center 0px;
	display: block;
	height: 25px;
	width: 90px;
	text-decoration: none;
	line-height: 2.5em;
	color: #018AAA;
	font-weight: bold;
}
.col2 td a:hover {
	background-color: transparent;
	color: #476163;
	text-decoration: none;
}
.importArea {
	margin: 0px;
	padding: 25px 0px;
}
.skipBtn {
	text-align: right;
	padding-right: 45px;
	padding-top: 20px;
	background: url("../../images/connectwithfriends/skip_btn.gif") no-repeat right 10px;
	height: 31px;
	padding-bottom: 10px;
}
.disclaimer {
	font-size: 10px;
	line-height: 14px;
	margin-left: 20px;
	padding: 0px;
	margin-top: 0px;
}
.clear {
	clear: both;
}
-->
</style>

<table cellpadding="0" cellspacing="0" border="0" width="790" align="center">
                    <tr>
                      <td colspan="3"><img runat="server" src="~/images/spacer.gif" width="1" height="14" /></td>
                    </tr>
                    <tr>
                      <td colspan="3"><table  border="0" cellpadding="0" cellspacing="0" width="100%">
                          <tr>
                            <td class="frTopLeft"></td>
                            <td class="frTop"></td>
                            <td class="frTopRight"></td>
                          </tr>
                          <tr>
                            <td class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" id="Img2" width="1" height="1" /></td>
                            <td valign="top" class="frBgIntMembers"><table width="100%" border="0" cellpadding="0" cellspacing="0" >
                                <tr>
                                  <td width="770" align="left"  valign="top"><table cellpadding="0" cellspacing="0" border="0"  width="100%">
                                      <tr>
                                        <td valign="top"><div id="templateHeader"><img runat="server" src="~/images/header_border.gif" alt="header_border.gif" width="768" height="120" border="0" />
                                            <h1>Kaneva Join Process</h1>
                                          </div></td>
                                      </tr>
                                      <tr>
                                        <td width="790" valign="top"><table border="0" cellspacing="0" cellpadding="0" width="100%">
                                            <tr><td>
 <!-- START WIZARD CONTENT -->

        <table border="0" cellspacing="0" cellpadding="0" width="99%">
          <tr>
            <td class="title"><h1>Connect with Your Friends</h1>
            <!-- <div class="indicator"> <div class="lEnd"><!-- Left End Cap</div> <div class="step1_on">Step 1: Create Your User ID</div> <div class="step2">Step 2: Invite Friends</div> <div class="step3">Step 3: Download</div> <div class="rEnd"><!-- Right End Cap</div> <div class="clear"><!-- clear the floats </div> </div>-->
            <div class="clear"><!-- clear the floats --></div>
              <span id="spnCoBrandCopy"></span>
              </td>
          </tr>
          <tr>
            <td align="center"><div style="width:85%;"> <span id="valSum$ajaxdest" name="__ajaxmark">
                <div id="valSum" class="errBox black" style="color:Black;margin-top:10px;display:none;"> </div>
                <span id="spnAlertMsg" runat="server" class="errBox black" style="display:none;width:80%;"></span>
                </span> <span id="cvBlank$ajaxdest" name="__ajaxmark"></span> </div>
              <br />
            </td>
          </tr>
          <tr>
            <td>
            <div style="text-align:left">
              <div class="flag" style="text-align:right; font-weight:bold; padding-right:0px; padding-top:10px; width:248px"><span style="margin-right:20px">Enter email addresses</span></div>
            </div>
            <div class="col1">
              <p class="flag"><asp:linkbutton CausesValidation="false" text="Choose your address book" id="lbAddressBook" oncommand="lbAddressBook_Click" tooltip="Choose your address book" runat="server" style="margin-right:20px;"/></p>
              <p class="flag">Sign in</p>
              <h3>Invite friends and earn Rewards:</h3>
              <p>For every five friends that join, you&#8217;ll earn 500 Rewards. ! You&#8217;ll also get an additional 1,000 bonus Rewards after they sign in to the 3D world!</p>
                    
            </div>
            <div class="col2">
              <table class="cif_emailTable">
                <tr>
                  <th>From:&nbsp;</th>
                  <td><asp:label runat="server" id="lblName"></asp:label></td>
                </tr>
                <tr>
                  <th></th>
                  <td><asp:label runat="server" id="lblRealName"></asp:label></td>
                </tr>
                <tr>
                  <th>To:&nbsp;</th>
                  <td><asp:TextBox ID="txtTo" TextMode="multiline" Rows="5" style="width:300px" MaxLength="500" runat="server"/></td>
                </tr>
                <tr>
                  <th>Message:&nbsp;</th>
                  <td><asp:TextBox ID="txtMessage" TextMode="multiline" Rows="5" style="width:300px" MaxLength="500" runat="server"/></td>
                </tr>
                <tr>
                  <th></th>
                  <td style="padding-top:5px">
                    <asp:imagebutton id="btnSendInvites" causesvalidation="false" runat="server" onclick="btnSendInvites_Click" imageurl="~/images/registration_flow/btn_sendInvite.gif" alternatetext="Send Invite" />
                    <asp:imagebutton id="btnCancel" causesvalidation="false" runat="server" onclick="btnCancel_Click" imageurl="~/images/registration_flow/btn_cancel.gif" alternatetext="Cancel" />
                  </td>
                </tr>
              </table>
              <h5>Kaneva will NOT store or share your private information!</h5>
              <p class="disclaimer">Any information you import (including your address book) is for your own private use. <br>
              For more info, see our <a href="http://www.kaneva.com/overview/privacy.aspx" target="_blank">Privacy Policy</a>.</p>
            </div>
            <div class="clear"><!-- clearthe floats --></div>
            <div class="skipBtn"><asp:linkbutton CausesValidation="false" text="Return" id="lbCancel" oncommand="lbCancel_Click" tooltip="Return" runat="server"/></div>
            </td>
          </tr>
        </table>
      <!-- END WIZARD CONTENT -->
      </td>
      </tr>
                                          </table></td>
                                      </tr>
                                    </table></td>
                                </tr>
                              </table></td>
                            <td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" id="Img4" width="1" height="1" /></td>
                          </tr>
                          <tr>
                            <td class="frBottomLeft"></td>
                            <td class="frBottom"></td>
                            <td class="frBottomRight"></td>
                          </tr>
                        </table></td>
                    </tr>
                  </table>