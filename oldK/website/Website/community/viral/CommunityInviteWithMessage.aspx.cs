///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva.channel;
using KlausEnt.KEP.Kaneva.framework.widgets;
using log4net;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for CommunityInviteWithMessage.
	/// </summary>
	public partial class CommunityInviteWithMessage : MainTemplatePage
	{
		protected CommunityInviteWithMessage () 
		{
			Title = "Community Invite";
		}

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                lblName.Text = KanevaWebGlobals.CurrentUser.Username;
                lblRealName.Text = KanevaWebGlobals.CurrentUser.DisplayName;
            }
        }

        /// <summary>
        /// lbAddressBook_Click
        /// </summary>
        protected void lbAddressBook_Click(object sender, CommandEventArgs e)
        {
            Response.Redirect("~/community/viral/CommunityViralEmail.aspx?communityId=" + GetCommunityId());
        }

        /// <summary>
        /// lbCancel_Click
        /// </summary>
        protected void lbCancel_Click(object sender, CommandEventArgs e)
        {
            DataRow drChannel = CommunityUtility.GetCommunity(GetCommunityId());
            Response.Redirect(GetBroadcastChannelUrl(drChannel["name_no_spaces"].ToString()));
        }

         /// <summary>
        /// Click event fired for btnSendInvites
        /// </summary>
        /// <returns></returns>
        protected void btnSendInvites_Click(object sender, ImageClickEventArgs e)
        {
            UserFacade userFacade = new UserFacade();
            DataRow drChannel = CommunityUtility.GetCommunity(GetCommunityId());
            System.Text.RegularExpressions.Regex regexEmail = new System.Text.RegularExpressions.Regex(Constants.VALIDATION_REGEX_EMAIL);

            // Validate the email addresses
            //parse the email addresses using spaces, comman and semicolons
            string[] emailAddresses = txtTo.Text.Trim().Split(';');
            ArrayList emailList = new ArrayList();
            foreach (string s in emailAddresses)
            {
                string[] list = s.Trim().Split(' ');
                foreach (string s2 in list)
                {
                    string[] list2 = s2.Trim().Split(',');
                    foreach (string s3 in list2)
                    {
                        if (s3.Trim() != string.Empty)
                            emailList.Add(s3);
                    }
                }
            }

            ArrayList emailListMember = new ArrayList();

            //foreach (string toEmail in emailList)
            for (int i = emailList.Count - 1; i >= 0; i--)
            {
                string sToEmail = (string)emailList[i];

                // Make sure it is a valid type
                if (!regexEmail.IsMatch(sToEmail))
                {
                    spnAlertMsg.InnerText = "Invalid email address '" + sToEmail + "'.";
                    spnAlertMsg.Style.Add("display", "block");
                    return;
                }

                // Make sure the email does not already exist as a registered user
                if (userFacade.EmailExists(sToEmail))
                {
                    // 0002538: from CK - Friend center - Allow to skip already e-mails in system and send to everyone ... 
                    //errMessage += "The email address \\'" + toEmail + "\\' is already associated with a user, please select another address.\\n";
                    emailListMember.Add(emailList[i]);
                    emailList.RemoveAt(i);
                }
            }


            int countSent = 0;
            int countNotSent = 0;

            if (emailList.Count > 0)
            {
                int userId = GetUserId();

                // Send out the emails
                for (int i = emailList.Count - 1; i >= 0; i--)
                {
                    string toEmail = emailList[i].ToString();

                    // check to see if user can send another invite to this person
                    DataRow drUser = UsersUtility.GetInvite(toEmail, userId);

                    if ((drUser != null) && (Convert.ToDateTime(drUser["reinvite_date"]).AddDays(14) > DateTime.Now))
                    {
                        emailList.RemoveAt(i);
                        countNotSent++;
                    }
                    else
                    {
                        SendEmail(toEmail, drChannel["name"].ToString());
                        countSent++;
                    }
                }

                // We are done
                Response.Redirect(GetBroadcastChannelUrl(drChannel["name_no_spaces"].ToString()));
            }
            else
            {
                spnAlertMsg.InnerText = "No emails sent because all email addresses belong to current Kaneva members.";
                spnAlertMsg.Style.Add("display", "block");
            }   
            
        }

        /// <summary>
        /// SendEmail
        /// </summary>
        /// <param name="toEmailAddress"></param>
        private void SendEmail(string toEmailAddress, string channelName)
        {
            // Generate the unique key code
            string keyCode = KanevaGlobals.GenerateUniqueString (20);

            // Are there to be any K-Points awarded?
            Double awardAmount = 0.0;
            string keiPointIdAward = ""; 

            DataRow drInvitePointAward = UsersUtility.GetInvitePointAwards ();
            if (drInvitePointAward != null)
            {
                awardAmount = Convert.ToDouble (drInvitePointAward["point_award_amount"]);
                keiPointIdAward = drInvitePointAward["kei_point_id"].ToString ();
            }

            // Save the invites to the invites table
            int inviteID = UsersUtility.InsertInvite(KanevaWebGlobals.CurrentUser.UserId, toEmailAddress, toEmailAddress, keyCode, awardAmount, keiPointIdAward);
            if (inviteID == 0)
            {
                ShowErrorOnStartup ("Error inserting invite.");
                return;
            }

            MailUtilityWeb.SendCommunityInvitation(GetUserId(), GetCommunityId (), channelName,  toEmailAddress, toEmailAddress, txtMessage.Text, inviteID, keyCode);
        }

                /// <summary>
        /// Click event fired for btnCancel
        /// </summary>
        /// <returns></returns>
        protected void btnCancel_Click(object sender, ImageClickEventArgs e)
        {
            DataRow drChannel = CommunityUtility.GetCommunity(GetCommunityId());
            Response.Redirect(GetBroadcastChannelUrl(drChannel["name_no_spaces"].ToString()));
        }

        /// <summary>
        /// GetCommunityId
        /// </summary>
        /// <returns></returns>
        private int GetCommunityId()
        {
            if (Request["communityId"] != null)
            {
                return Convert.ToInt32(Request["communityId"]);
            }

            return 0;
        }
    }
}
