///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva.channel;
using KlausEnt.KEP.Kaneva.framework.widgets;
using log4net;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for CommunityViral.
	/// </summary>
	public partial class CommunityViral : MainTemplatePage
	{
		protected CommunityViral () 
		{
			Title = "Community Viral";
		}
        
        protected void Page_Load(object sender, EventArgs e)
        {
            ucCommunityViral.CommunityId = Convert.ToInt32 (Request["communityId"]);
        }

        protected KlausEnt.KEP.Kaneva.usercontrols.CommunityViral ucCommunityViral;
    }
}
