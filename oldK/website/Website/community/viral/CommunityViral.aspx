<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CommunityViral.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.CommunityViral" %>

<%@ Register TagPrefix="Kaneva" TagName="CommunityViral" Src="../../usercontrols/CommunityViral.ascx" %>
    
<link href="../../css/home.css" rel="stylesheet" type="text/css">
<link href="../../css/kanevaSC.css" rel="stylesheet" type="text/css">
<link href="../../css/friends.css" rel="stylesheet" type="text/css">
<link href="../../css/new.css" type="text/css" rel="stylesheet">
                                                  
<!-- Main Container -->
<table border="0" cellspacing="0" cellpadding="0" class="newcontainer" width="990" align="center">
	<tr>
		<td>
			<table  border="0" cellpadding="0" cellspacing="0" width="100%" class="newcontainerborder">
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>				
				<tr>
					<td class=""><img id="Img2" runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
					<td valign="top" class="newdatacontainer">
					<!-- Start Content -->			
																															
	
						<!-- Description Section -->
						<!-- Start Container Wrapper -->
						<table border="0" cellspacing="0" cellpadding="0" width="99%">
							<tr>
								<td valign="top" align="center">
									<table border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="850" >
												<div class="module">
													<span class="ct"><span class="cl"></span></span>
													<!-- START CONTENT -->
							                            <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
							                            <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
							                            <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
							                            <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
							                            

                                                        <!-- Start new Pop-up -->
                                                        <kaneva:CommunityViral runat="server" id="ucCommunityViral"/>
                                                        <!-- End new Pop-up -->
    

													<!-- END CONTENT -->																			
													<span class="cb"><span class="cl"></span></span>
												</div>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!-- End Container Wrapper -->

					<!-- End Content -->
					</td>
					<td class=""><img id="Img4" runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
				</tr>				
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
				<tr>
					<td><img id="Img5" runat="server" src="~/images/spacer.gif" width="1" height="14" /></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
			