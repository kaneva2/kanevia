<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CommunityViralEmail.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.CommunityViralEmail" %>

<%@ Register TagPrefix="Kaneva" TagName="ConnWithFriends" Src="../../usercontrols/ConnectWithFriends.ascx" %>

<asp:literal id="litCSS" runat="server" />	

<link href="../../css/themes-join/template.css" rel="stylesheet" type="text/css" />	

<table cellpadding="0" cellspacing="0" border="0" width="790" align="center">
	<tr>
		<td colspan="3"><img runat="server" src="~/images/spacer.gif" width="1" height="14" id="Img12"/></td>
	</tr>
	<tr>
		<td colspan="3">
			<table  border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="frTopLeft"></td>
					<td class="frTop"></td>
					<td class="frTopRight"></td>
				</tr>
				<tr>
					<td class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" id="Img13" width="1" height="1" /></td>
					<td valign="top" class="frBgIntMembers">
						<table width="100%" border="0" cellpadding="0" cellspacing="0" >
							<tr>
								<td width="770" align="left"  valign="top">
									<table cellpadding="0" cellspacing="0" border="0"  width="100%">
										<tr>
											<td valign="top">
												<div id="templateHeader"><img runat="server" src="~/images/header_border.gif" alt="header_border.gif" width="768" height="120" border="0" id="Img14"><h1>Kaneva Join Process</h1></div>
											</td>
										</tr>
										<tr>
											<td width="790" valign="top">
												
												<table border="0" cellspacing="0" cellpadding="0" width="100%">
													<tr>
														<!-- START WIZARD CONTENT -->
														<td valign="top" align="center">

														    <!-- Start new Pop-up -->
                                                                <kaneva:ConnWithFriends runat="server" id="ucConnWithFriends" ExitUrl="community/CommunityPage.aspx"/>
                                                                <!-- End new Pop-up -->
																																																																																
														</td>
														<!-- END WIZARD CONTENT -->
													</tr>
												</table>
											
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" id="Img15" width="1" height="1" /></td>
				</tr>
				<tr>
					<td class="frBottomLeft"></td>
					<td class="frBottom"></td>
					<td class="frBottomRight"></td>
				</tr>
			</table>				
		</td>
	</tr>
</table>
<br/><br/><br/><br/>
