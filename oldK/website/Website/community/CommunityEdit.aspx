<%@ Page Language="C#" MasterPageFile="~/masterpages/GenericPageTemplate.Master" ValidateRequest="False" AutoEventWireup="false" CodeBehind="CommunityEdit.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.CommunityEdit" %>
<%@ Register TagPrefix="Kaneva" TagName="LibraryImages" Src="../usercontrols/SelectPhotoFromLibrary.ascx" %>

<asp:Content ID="cnt_ProfileHead" runat="server" contentplaceholderid="cph_HeadData" >
    <meta name="title" content="Kaneva. The Online Community and Social Network." />
    <meta name="description" content="Kaneva is the first to combine social networking and a free 3D virtual world. It's a whole new way to connect with friends. Have fun in a vibrant virtual world full of people, cool places and fun games. Create your avatar and get a free 3D home today!"/>
    <meta name="keywords" content="free virtual world, virtual world, virtual worlds, 3D world, online avatar community, mmo game, virtual reality, caneva, kaniva" />
    
    <NOSCRIPT><META HTTP-EQUIV="refresh" CONTENT="1" URL="noJavascript.aspx" /></NOSCRIPT>
    
    <script language="JavaScript" type="text/javascript" src="../jscript/prototype-1.6.1-min.js"></script>
    <script type="text/javascript" src="../jscript/Communities/CommunityEdit-min.js"></script>
    <script language="Javascript" src="../jscript/PageLayout/colorpicker.js"></script>
    <link href="../css/Communities/CommunityEdit.css?v2" type="text/css" rel="stylesheet" />

	<script type="text/javascript">
		function ConfigDefaultTab(cbx) {
			var rbl = document.getElementsByName($('<%= rblDefaultTab.ClientID + "_0" %>').name);
			if (cbx.checked == true) {
				rbl[0].disabled = false;
			}
			else {
				rbl[0].disabled = true;
				rbl[1].checked = true;
			}
		}
	
	
	</script>
</asp:Content>

<asp:Content ID="mainContent" runat="server" contentplaceholderid="cph_Body">
    <div id="editBody">
	    <div id="messageArea">
	        <asp:ValidationSummary ShowMessageBox="False" ShowSummary="True" cssclass="errBox" id="valSum" DisplayMode="BulletList" runat="server" HeaderText="Please correct the following errors:" />
	        <div id="alertMsg" runat="server" class="errBox" visible="false"></div>
	    </div>
        <div id="pageHeader">
            <div>
                <h6><span>EDIT:</span> <asp:literal id="litCommType1" runat="server"></asp:literal> Info</h6>
            </div>
        </div>
        <div id="describeCommity">
            <h4>Describe your <asp:literal id="litCommType2" runat="server"></asp:literal></h4>
			<p>
			    <span>Name <asp:RequiredFieldValidator id="rfItemName" runat="server" Display="Dynamic" ErrorMessage="Name is a required field." Text="*" ControlToValidate="txtTitle"></asp:RequiredFieldValidator></span>
			    <asp:RegularExpressionValidator id="revTitle" runat="server" Display="Dynamic" Text="*" ControlToValidate="txtTitle" EnableClientScript="True"></asp:RegularExpressionValidator><br />
			    <asp:TextBox id="txtTitle" runat="server" Width="550" MaxLength="50"></asp:TextBox>
			</p>
			<p>
			    <span style="line-height:16px;">Brief Description <asp:RequiredFieldValidator id="rftxtDescription" runat="server" Display="Dynamic" ErrorMessage="Description is a required field." Text="*" ControlToValidate="txtDescription"></asp:RequiredFieldValidator></span><br />
			    <asp:TextBox id="txtDescription" runat="server" Width="550" Rows="4" MaxLength="255" TextMode="multiline"></asp:TextBox>
			    <SPAN id="spItemNameMsg" style="FONT-SIZE: 11px; MARGIN: 3px 0px 0px 10px; COLOR: red" runat="server"></SPAN>
			</p>
            <div id="divCommunityOnly" runat="server">
			<p>
			    <span>Type <asp:RequiredFieldValidator id="rfdrpPlace" runat="Server" InitialValue="" ErrorMessage="Please select a Type." Display="Dynamic" Text="*" ControlToValidate="drpPlace"  EnableClientScript="True"></asp:RequiredFieldValidator></span><br />
			    <asp:DropDownList id="drpPlace" runat="Server" Width="250px"></asp:DropDownList>
			</p>
			<p>
			    <span>Category <asp:RequiredFieldValidator id="rfdrpCategory" runat="Server" InitialValue="" ErrorMessage="Please select a category." Display="Dynamic" Text="*" ControlToValidate="drpCategory"  EnableClientScript="True"></asp:RequiredFieldValidator></span><br />
			    <asp:DropDownList id="drpCategory" runat="Server" Width="250px"></asp:DropDownList>
			</p>
			<p>
			    <span>Tags (separated by spaces)</span><br />
			    <asp:TextBox id="txtTags" runat="server" Width="380" MaxLength="100"></asp:TextBox>
			    <asp:RequiredFieldValidator id="rftxtTags" runat="server" Display="Dynamic" ErrorMessage="Please provide at least one Tag in the Tags field." Text="*" ControlToValidate="txtTags"></asp:RequiredFieldValidator>
			    <asp:RegularExpressionValidator id="regtxtKeywords" runat="server" Display="Dynamic" Text="*" ControlToValidate="txtTags" EnableClientScript="True"></asp:RegularExpressionValidator><br />
                <span class="info"><strong>What's a Tag?</strong> Tags are descriptive words that make finding a <asp:literal id="litCommType3" runat="server"></asp:literal> easier. They describe what a <asp:literal id="litCommType7" runat="server"></asp:literal> is about, like "cooking" or "photography."</span>
            </p>   
            </div>         
        </div>
        <div id="restrictions">
            <h4>Set age and content restrictions</h4>
			<div id="restrictedArea" runat="server">
			    <asp:checkbox id="cbRestricted" runat="server"></asp:checkbox><strong>&nbsp;Restricted</strong>&nbsp;(Check this box if anything in your <asp:literal id="litCommType4" runat="server"></asp:literal> is inappropriate for visitors under 18.)
			</div>
			<div id="over21Area" runat="server">
			    <asp:checkbox id="cbOver21" runat="server"></asp:checkbox><strong>&nbsp;21 and over only</strong>&nbsp;Set this option to ensure only members 21 and older are able to access your Kaneva <asp:literal id="litCommType6" runat="server"></asp:literal> and Virtual World hangout.
			</div>
			<p>
			    <span style="line-height:16px;">Access Type</span><br />
			    <asp:DropDownList id="drpStatus" runat="Server" Width="150px"></asp:DropDownList>
			    <A id="A1" href="http://www.kaneva.com/community/CommunityPage.aspx?communityId=80805&pageId=2168401" target="_resource" runat="server"><span class="spacer"></span>What is Members Only?</A>
            </p>
            <asp:checkbox id="chkAllowPublish" runat="server"></asp:checkbox>&nbsp;Allow all members of the <asp:literal id="litCommType5" runat="server"></asp:literal> to publish photos, videos, music, and games.            
        </div>        
        <div id="divTabContainer" runat="server" visible="false">
		<div id="tabs">
			<h4>Set tabs to be displayed</h4>
			<div id="tablist">
				<asp:checkboxlist id="cblTabs" repeatcolumns="2" cellpadding="2" width="300" runat="server">
				</asp:checkboxlist>
			</div>
			<div id="tabdefault">
				<stong>Set Default Tab:</stong>
				<asp:radiobuttonlist id="rblDefaultTab" runat="server" clientidmode="Static"></asp:radiobuttonlist>
			</div>		
		</div>
		</div>
		<div id="profilePhoto">
            <h4><asp:literal id="litPhotoText" runat="server" Text="Community Photo"></asp:literal></h4>
            <div>
                <div class="photo">
                    <img src="" alt="" id="imgThumbnail" runat="server" />
                </div>
                <div class="photoDescription info">
                    <br /><br />
	                <b>We recommend 200 x 150 pixels.</b>		        
                </div>                
            </div>                
	        <div class="photoLink">
                <input runat="server" ID="inpThumbnail" type="file" size="45" /><span class="spacer"></span>
			    <asp:linkbutton id="btnClearThumb" OnClientClick="return confirm('This will remove your community photo, are you sure you wish to proceed?');" onclick="btnClearThumb_Click" runat="Server" Text="Remove" CausesValidation="False"></asp:linkbutton>
	        </div>  
	    </div>
        <div id="backgroundOptions">
            <h4>Background (Optional):</h4>
            <p>
			    Background Color<span style="padding-left:50px"></span><asp:hyperlink cssclass="colorPicker" id="txtBackgroundColorAppClrPickerStr" Runat="server" ImageUrl="..\images\colorpickmask.gif" CausesValidation="False"></asp:hyperlink>
			    <asp:textbox id="txtBackgroundColor" runat="server" MaxLength="8"></asp:textbox>
			</p>
			<p>
			    Background Photo URL<span style="padding-left:16px"></span><asp:textbox id="txtBGPicture" style="width:400px" runat="server" MaxLength="255"></asp:textbox><span class="spacer"></span><A href="javascript:void(0);" runat="server" id="aRemove">Clear Photo URL</A>
                <span class="spacer"></span><asp:linkbutton PostBackUrl="#library" id="selectPhoto" onclick="selectPhoto_Click" runat="Server" Text="Select from Library..." CausesValidation="False"></asp:linkbutton>
			</p>
			    <!-- start hidden div to allow user to select from their media library-->
			<div id="libraryImages" runat="server">
			    <a name="library"></a>
			    <kaneva:LibraryImages id="libraryImageSelector" runat="server" Visible="false" />
			</div>
			<!-- end div -->           
        </div>
        <div id="communityURL">
            <div id="divRegistrationErr" style="MARGIN-TOP: 15px; COLOR: red" runat="server" visible="false"></div>
            <h4>Reserve Your <asp:literal id="litCommType9" runat="server"></asp:literal> URL</h4>
            <!-- START CONTENT -->
            <p>
                <SPAN id="spnRegistered" runat="server" visible="false">Your URL has already been registered.</span><br />
                <asp:label id="lblURL" runat="server"></asp:label>
                <asp:textbox id="txtURL" runat="server" maxlength="50" Width="230"></asp:textbox><br />			
			    <asp:regularexpressionvalidator id="revURL" runat="server" controltovalidate="txtURL" display="Dynamic" text="URL should be at least four characters and can contain only letters, numbers and underscore."></asp:regularexpressionvalidator>           
            </p>
            <p>
                <div id="urlWarning" runat="server"><span class="warning">This short URL is permanent and cannot be changed after you reserve it.</span></div>
            </p>
            <!-- END CONTENT -->																			
       </div>
       <div id="submitArea">
            <asp:imageButton id="btnLaunch"  ImageUrl="~/images/buttons/btnCommunityLaunch.gif" onclick="btnNew_Click" runat="Server" visible="false" CausesValidation="true" ></asp:imageButton>
            <asp:linkbutton class="btnsmall grey" id="btnUpdate" onclick="btnUpdate_Click" runat="Server" Text="Save Changes" CausesValidation="True"></asp:linkbutton>&nbsp;&nbsp;
            <asp:linkbutton class="btnsmall grey" id="btnCancel" onclick="btnCancel_Click" runat="Server" Text="Cancel" CausesValidation="False"></asp:linkbutton>
       </div>
    </div>
    
</asp:Content>