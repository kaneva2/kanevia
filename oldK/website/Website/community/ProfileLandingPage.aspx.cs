///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Web.UI.HtmlControls;
using Kaneva.BusinessLayer.BusinessObjects;
using log4net;

namespace KlausEnt.KEP.Kaneva.channel
{
    public partial class ProfileLandingPage : BasePage
    {
        #region Declarations
        private string userDisplayName = "";

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        #endregion

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            int communityId = Convert.ToInt32(Request["communityId"]);
            Community currentCommunity = GetCommunityFacade.GetCommunity(communityId);


            if (!IsPostBack)
            {
                try
                {
                    // Make sure we have a valid community
                    if (currentCommunity.CommunityId > 0)
                    {
                        // If the user is authenticated, show them the full profile page.
                        if (Request.IsAuthenticated)
                        {
                            Server.Transfer("ProfilePage.aspx?communityId=" + currentCommunity.CommunityId);
                        }
                        // If the user is not authenticated, go ahead and set up the profile and widgets.
                        else
                        {
                            int profileOwnerId = currentCommunity.CreatorId;
                            User userProfileOwner = GetUserFacade.GetUser(profileOwnerId);

                            BindUserData(userProfileOwner);
                            BindOthersNamedSameWidgetData(userProfileOwner);
                            BindOthersNamedSimilarWidgetData(userProfileOwner);
                            SetRegistrationLinks(userProfileOwner.Username);

                            // Set the metadata
                            ((GenericLandingPageTemplate)Master).Title = "Kaneva - Imagine What You Can Do";
                            ((GenericLandingPageTemplate)Master).MetaDataTitle = "Kaneva. The Online Community and Social Network.";
                            ((GenericLandingPageTemplate)Master).MetaDataDescription =
                                "Kaneva is the first to combine social networking and a free 3D virtual world. It's a whole new way to connect with friends. Have fun in a vibrant virtual world full of people, cool places and fun games. Create your avatar and get a free 3D home today!";
                            ((GenericLandingPageTemplate)Master).MetaDataKeywords =
                                "free virtual world, virtual world, virtual worlds, 3D world, online avatar community, mmo game, virtual reality, caneva, kaniva";
                        }
                    }
                    else
                    {
                        Response.Redirect("~/error.aspx?error=" + (int)Constants.eERROR_TYPE.PROFILE_NOT_FOUND, true);
                    }
                }
                catch
                {
                    Response.Redirect("~/error.aspx?error=" + (int)Constants.eERROR_TYPE.PROFILE_NOT_FOUND, true);
                }
            }
        }

        #endregion

        #region Data-Binding Functions
        private void BindUserData(User user)
        {
           // Name fields
            UserDisplayName = user.DisplayName;

            // Profile Info
            pProfileInfo.InnerHtml = user.Username + "<br />" +
                                     user.Location + "<br />" +
                                     (user.Gender.Equals("M") ? "Male" : "Female") +
                                     (user.Ustate == Constants.ONLINE_USTATE_ON ? "<br /><img src=\"" + ResolveUrl ("~/images/widgets/widget_online_icon.gif") + "\"></img>&nbsp;&nbsp;Online" : "");

            // Profile picture
            imgProfilePhoto.Src = GetUserLargeProfileImageURL(user);
            imgProfilePhoto.Alt = user.DisplayName;
        }

        private void BindOthersNamedSameWidgetData(User user)
        {
            PagedList<User> usersWithSameName = GetUserFacade.GetOthersWithSimilarName(user.DisplayName, user.Username, true, 1, 5);
            rptOthersNamedSame.DataSource = usersWithSameName;
            rptOthersNamedSame.DataBind();

            ulOthersNamedSame.Visible = (usersWithSameName.TotalCount > 0);
        }

        private void BindOthersNamedSimilarWidgetData(User user)
        {
            PagedList<User> usersWithSimilarName = GetUserFacade.GetOthersWithSimilarName(user.DisplayName, user.Username, false, 1, 5);
            int numNeeded = 0;

            // If we don't have enough, lets pad it with random
            if (usersWithSimilarName.TotalCount < 5)
            {
                numNeeded = 5 - Convert.ToInt32(usersWithSimilarName.TotalCount);

                PagedList<User> otherInterestingUsers = GetUserFacade.SearchUsers(
                    false, false, string.Empty,
                    true, true, true, 18, 0,
                    string.Empty, string.Empty, 0,
                    0, 0, 0, -1, 0, -1, string.Empty,
                    string.Empty, string.Empty, string.Empty,
                    string.Empty, string.Empty, string.Empty,
                    string.Empty, false, string.Empty, string.Empty,
                    Constants.SEARCH_ORDERBY_LAST_LOGGED_IN, 1, 5);

                for (int i = 0; i < numNeeded; i++)
                {
                    usersWithSimilarName.Add(otherInterestingUsers[i]);
                }
                usersWithSimilarName.TotalCount = Convert.ToUInt32(usersWithSimilarName.Count);
            }
            
            rptOthersNamedSimilar.DataSource = usersWithSimilarName;
            rptOthersNamedSimilar.DataBind();

            // If we filled this totally at random, change the title.
            if (numNeeded == 5)
            {
                HtmlContainerControl header = (HtmlContainerControl)rptOthersNamedSimilar.Controls[0].Controls[0].FindControl("h2SimilarName");
                header.InnerText = "Other Interesting Profiles";
            }

            ulOthersNamedSimilar.Visible = true;
        }
        #endregion

        #region Helper Functions
        private string GetUserLargeProfileImageURL(User user)
        {
            string imagePath = "";

            if (user.MatureProfile && !KanevaWebGlobals.CurrentUser.HasAccessPass)
            {
                imagePath += KanevaGlobals.ImageServer;
                imagePath += user.Gender.ToUpper().Equals("M") ? "/KanevaIconMale_la.gif" : "/KanevaIconFemale_la.gif";
            }
            else
            {
                imagePath = GetProfileImageURL(user.ThumbnailLargePath, "la", user.Gender, user.FacebookSettings.UseFacebookProfilePicture, user.FacebookSettings.FacebookUserId);
            }
            return imagePath;
        }

        protected string GetUserSmallProfileImageURL(object userObject)
        {
            User user = (User)userObject;
            string imagePath = "";

            if (user.MatureProfile && !KanevaWebGlobals.CurrentUser.HasAccessPass)
            {
                imagePath += KanevaGlobals.ImageServer;
                imagePath += user.Gender.ToUpper().Equals("M") ? "/KanevaIconMale_sm.gif" : "/KanevaIconFemale_sm.gif";
            }
            else
            {
                imagePath = GetProfileImageURL(user.ThumbnailSmallPath, "sm", user.Gender, user.FacebookSettings.UseFacebookProfilePicture, user.FacebookSettings.FacebookUserId);
            }
            return imagePath;
        }

        protected string GetUserProfileLink(object userObject)
        {
            User user = (User)userObject;
            return ResolveUrl("~/community/" + user.Username + ".people");
        }

        private void SetRegistrationLinks(string userName)
        {
            string regLink = ResolveUrl("~/register/kaneva/registerinfo.aspx");
            regLink += "?lpage=profile";
            regLink += "&uname=" + userName;

            hiRegisterLink.Value = regLink;
            btnViewProfile.HRef = regLink;
            btnAddFriend.HRef = regLink;
            aJoinToday.HRef = regLink;
            aProfileJoinLink.HRef = regLink;
        }
        #endregion

        #region Attributes
        public string UserDisplayName
        {
            get
            {
                return userDisplayName;
            }
            set
            {
                userDisplayName = value;
            }
        }
        #endregion

        #region Web Form Designer generated code

        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new EventHandler(this.Page_Load);
        }

        #endregion

    }
}
