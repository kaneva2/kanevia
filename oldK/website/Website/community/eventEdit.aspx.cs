///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for eventEdit.
	/// </summary>
	public class eventEdit : BasePage
	{
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Old page, users should not see this page. Redirecting to my kaneva
            RedirectToHomePage();

            if(Request.IsAuthenticated)
			{
				if(!GetRequestParams())
				{
					//invalid request params
					//if the user logged in, send him to his home page
					//if not, redirect to login page
					RedirectToHomePage ();
				}

                if (!IsPostBack)
                {
                    BindData();
                }
            }
			else
			{
				Response.Redirect (this.GetLoginURL ());
			}
		}
		
		private bool GetRequestParams()
		{
			bool retVal = true;
			try
			{
				_eventId = Convert.ToInt32 (Request ["eventId"]);
				if(_eventId <= 0 )
				{
					ChannelId = Convert.ToInt32 (Request ["communityId"]);
				}else
				{
					DataRow drEvent = CommunityUtility.GetEvent(_eventId);
					ChannelId = Convert.ToInt32(drEvent["community_id"]);
				}
			}
			catch(Exception)
			{
				//invalid numbers
				retVal = false;
			}
			return retVal;
		}

		private void BindData()
		{
			string title = "";
			string desc = "";
			string location = "";
			int eventTypeId = 1;
			DateTime startTime = DateTime.Now;
			DateTime endTime = DateTime.Now.AddMinutes(30);
			bool validAccess = false;
			if(_eventId > 0)
			{
				//existing event
				DataRow drEvent = CommunityUtility.GetEvent(_eventId);
				title = drEvent["title"].ToString();
				desc = drEvent["details"] == DBNull.Value ? "" : drEvent["details"].ToString();
				location = drEvent["location"] == DBNull.Value ? "" : drEvent["location"].ToString();
				eventTypeId = Convert.ToInt32(drEvent["type_id"]);
				startTime = Convert.ToDateTime(drEvent["start_time"]);
				endTime = Convert.ToDateTime(drEvent["end_time"]);

				int eventOwnerId = Convert.ToInt32(drEvent["user_id"]);
				validAccess = CommunityUtility.IsCommunityModerator(ChannelId, GetUserId()) || 
					eventOwnerId == GetUserId();
			}else
			{
				//new event
				DataRow drChannel = CommunityUtility.GetCommunity(ChannelId);
				validAccess = CommunityUtility.IsCommunityModerator(ChannelId, GetUserId()) ||
					((string)drChannel["allow_member_events"] == "1" &&
					CommunityUtility.IsActiveCommunityMember(ChannelId, GetUserId()));
			}

			if (validAccess)
			{
				InitOptions();

				txtTitle.Text = title;
				txtDescription.Text = desc;
				txtLocation.Text = location;

				SetDropDownSelection(ddlEventType, eventTypeId.ToString());
                txtEventStart.Text = startTime.Month.ToString () + "/" + startTime.Day.ToString () + "/" + startTime.Year.ToString ();
                
                SetDropDownSelection(ddlHourStart, startTime.Hour.ToString());
				SetDropDownSelection(ddlMinuteStart, startTime.Minute.ToString());

                txtEventEnd.Text = endTime.Month.ToString () + "/" + endTime.Day.ToString () + "/" + endTime.Year.ToString ();
                
                SetDropDownSelection(ddlHourEnd, endTime.Hour.ToString());
				SetDropDownSelection(ddlMinuteEnd, endTime.Minute.ToString());
			}else
			{
				//access granted
				pnlInvalidAccess.Visible = true;
				pnlEdit.Visible = false;
				pnlEdit.Enabled = false;
			}
		}

		private void InitOptions()
		{
			ddlHourStart.DataSource = Constants.OPT_HOUR;
			ddlHourStart.DataValueField = "Key";
			ddlHourStart.DataTextField = "Value";
			ddlHourStart.DataBind();

			ddlMinuteStart.DataSource = Constants.OPT_MINUTE;
			ddlMinuteStart.DataValueField = "Key";
			ddlMinuteStart.DataTextField = "Value";
			ddlMinuteStart.DataBind();

			ddlHourEnd.DataSource = Constants.OPT_HOUR;
			ddlHourEnd.DataValueField = "Key";
			ddlHourEnd.DataTextField = "Value";
			ddlHourEnd.DataBind();

			ddlMinuteEnd.DataSource = Constants.OPT_MINUTE;
			ddlMinuteEnd.DataValueField = "Key";
			ddlMinuteEnd.DataTextField = "Value";
			ddlMinuteEnd.DataBind();

			ddlEventType.DataSource = Constants.EVENT_TYPES;
			ddlEventType.DataValueField = "Key";
			ddlEventType.DataTextField = "Value";
			ddlEventType.DataBind();
		}

		private void SetDropDownSelection(DropDownList list, string selection)
		{
			ListItem listItemEventType = list.Items.FindByValue(selection);
			if(listItemEventType != null)
			{
				list.SelectedIndex = list.Items.IndexOf(listItemEventType);
			}
		}

		private void btnSave_Click(object sender, EventArgs e)
		{
			//save event
			string title = txtTitle.Text;
			string desc = txtDescription.Text;
			string location = txtLocation.Text;
			int eventTypeId = Convert.ToInt32(ddlEventType.SelectedItem.Value);
			int timeZoneId = 0;
			
            string[] strStart = txtEventStart.Text.Split('/');

            DateTime startTime = new DateTime(
                Convert.ToInt32 (strStart[2]),
                Convert.ToInt32 (strStart[0]),
                Convert.ToInt32 (strStart[1]),
                Convert.ToInt32 (ddlHourStart.SelectedItem.Value),
                Convert.ToInt32 (ddlMinuteStart.SelectedItem.Value), 0);

            string[] strEnd = txtEventEnd.Text.Split ('/');

            DateTime endTime = new DateTime (
               Convert.ToInt32 (strEnd[2]),
               Convert.ToInt32 (strEnd[0]),
               Convert.ToInt32 (strEnd[1]),
               Convert.ToInt32 (ddlHourEnd.SelectedItem.Value),
               Convert.ToInt32 (ddlMinuteEnd.SelectedItem.Value), 0);

            if (startTime >= endTime)
            {
                cvStartTime.IsValid = false;
                return;
            }

            if (_eventId > 0)
			{
				CommunityUtility.SaveEvent(_eventId, ChannelId, GetUserId(),title,
					desc, location, startTime, endTime, eventTypeId, timeZoneId);
			}
            else
			{
                // Metrics tracking for accepts
                string requestId = GetUserFacade.InsertTrackingRequest(Constants.cREQUEST_TRACKING_REQUEST_TYPE_ID_EVENT_INVITE, KanevaWebGlobals.CurrentUser.UserId, 0, 0);
                string requestTypeSentIdEvent = GetUserFacade.InsertTrackingTypeSent(requestId, Constants.cREQUEST_TRACKING_MESSAGE_TYPE_ID_WEBSITE, 0);

				CommunityUtility.InsertEvent(ChannelId, GetUserId(),title,
                    desc, location, startTime, endTime, eventTypeId, timeZoneId, requestTypeSentIdEvent);
			}

			string javascript = "<script language=JavaScript>window.opener.location = window.opener.location; window.close()</script>";
			ClientScript.RegisterStartupScript(GetType (), "CloseAndUpdateParent",javascript);
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
			btnSave.Click += new EventHandler(btnSave_Click);
		}
		#endregion

		public int EventId
		{
			get { return _eventId; }
			set { _eventId = value; }
		}

		private int ChannelId
		{
			get
			{
				if (ViewState["ChannelId"] == null || ViewState["ChannelId"].ToString() == "")
				{
					ViewState["ChannelId"] = "0";
				}
				return Convert.ToInt32(ViewState["ChannelId"].ToString());
			}
			set
			{
				ViewState["ChannelId"] = value;
			}
        }


        #region Declerations

        protected Button btnSave;
        private int _eventId;

        protected TextBox txtTitle;
        protected DropDownList ddlHourStart;
        protected DropDownList ddlMinuteStart;

        protected DropDownList ddlHourEnd;
        protected DropDownList ddlMinuteEnd;

        protected DropDownList ddlEventType;
        protected TextBox txtLocation;
        protected TextBox txtDescription;
        protected TextBox txtEventStart, txtEventEnd;

        protected Panel pnlEdit;
        protected Panel pnlInvalidAccess;

        protected ValidationSummary valSum;
        protected CustomValidator cvStartTime;

        #endregion Declerations
    }
}
