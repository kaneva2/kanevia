<%@ Page Language="C#" MasterPageFile="~/masterpages/GenericLandingPageTemplate.Master" AutoEventWireup="false" CodeBehind="ProfileLandingPage.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.channel.ProfileLandingPage" %>

<asp:Content ID="cntHeaderCSS" runat="server" contentplaceholderid="cphHeadCSS" >
    <link rel="stylesheet" type="text/css" href="../css/landing_page/profile/profile.css" />
</asp:Content>

<asp:Content ID="cntBottomJS" runat="server" contentplaceholderid="cphBottomPageJS" >
    <script type="text/javascript" src="../jscript/landing_page/profile.js"></script>
</asp:Content>

<asp:Content ID="cntHeader" runat="server" contentplaceholderid="cphHeader" >
    <div class="headerLinks">
        <a id="aJoinToday" runat="server" href="">Join Today</a>&nbsp;|&nbsp;<span class="showSignIn">Sign In</span>
    </div>
</asp:Content>

<asp:Content id="cntBody" runat="server" contentplaceholderid="cphBody" >
    <input type="hidden" id="hiRegisterLink" runat="server" class="hiRegisterLink"/>
    <div id="divProfilePreview" class="profilePreview">
        <div class="profilePhoto">
            <img id="imgProfilePhoto" runat="server" src="" alt="" />
        </div>
        <h1><%=TruncateWithEllipsis(UserDisplayName, 45)%></h1>
        <p id="pProfileInfo" class="profileInfo" runat="server"></p>
        <a id="aProfileJoinLink" runat="server" href="" class="joinLink">Join Kaneva and become friends with <%=TruncateWithEllipsis(UserDisplayName, 45)%>.</a>
        <hr />
        <div class="promiseContainer">
            <p>Explore a large number of Worlds made by our <br />community and have fun with friends.</p>
            <p>You can also:</p>
            <ul>
                <li>Play games with <strong class="userName"><%=TruncateWithEllipsis(UserDisplayName, 40)%>.</strong></li>
                <li>Chat with <strong class="userName"><%=TruncateWithEllipsis(UserDisplayName, 40)%></strong>.</li>
                <li>Experience <strong class="userName"><%=TruncateWithEllipsis(UserDisplayName, 40)%>'s</strong> 3D worlds.</li>
            </ul>
        </div>
        <div class="buttonContainer">
            <a id="btnViewProfile" runat="server" href="" class="viewProfileButton"></a>
            <a id="btnAddFriend" runat="server" href="" class="addFriendButton"></a>
        </div>
    </div>
    <div class="profilePreviewShadow"></div>
    <ul id="ulOthersNamedSame" runat="server" class="profileList" visible="false">
        <h2>Others Named <%=TruncateWithEllipsis(UserDisplayName, 18)%></h2>
        <asp:Repeater ID="rptOthersNamedSame" runat="server">
            <ItemTemplate>
                <li class="profileListItem" data-profilelink='<%#GetUserProfileLink(Container.DataItem) %>'>
                    <img src='<%#GetUserSmallProfileImageURL(Container.DataItem) %>' alt='<%#DataBinder.Eval(Container.DataItem, "DisplayName")%>' width="50px" height="50px"/>
                    <div class="userNames">
                        <%#TruncateWithEllipsis(Convert.ToString(DataBinder.Eval(Container.DataItem, "DisplayName")), 22) %><br />
                        <span><%#DataBinder.Eval(Container.DataItem, "UserName") %></span>
                    </div>
                    <br class="clear" />
                </li>
            </ItemTemplate>
            <SeparatorTemplate>
                <hr />
            </SeparatorTemplate>
        </asp:Repeater>
    </ul>
    <ul id="ulOthersNamedSimilar" runat="server" class="profileList" visible="false">
        <asp:Repeater ID="rptOthersNamedSimilar" runat="server">
            <HeaderTemplate>
            <h2 id="h2SimilarName" runat="server">Others With a Similar Name</h2>
            </HeaderTemplate>
            <ItemTemplate>
                <li class="profileListItem" data-profilelink='<%#GetUserProfileLink(Container.DataItem) %>'>
                    <img src='<%#GetUserSmallProfileImageURL(Container.DataItem) %>' alt='<%#DataBinder.Eval(Container.DataItem, "DisplayName")%>' width="50px" height="50px"/>
                    <div class="userNames">
                        <%#TruncateWithEllipsis(Convert.ToString(DataBinder.Eval(Container.DataItem, "DisplayName")), 22) %><br />
                        <span><%#DataBinder.Eval(Container.DataItem, "UserName") %></span>
                    </div>
                    <br class="clear" />
                </li>
            </ItemTemplate>
            <SeparatorTemplate>
                <hr />
            </SeparatorTemplate>
        </asp:Repeater>
    </ul>
    <br class="clear" />
</asp:Content>

<asp:Content id="cntFooter" runat="server" contentplaceholderid="cphFooter" >
    <!--[if lte IE 8]><img class="footerRuleImage" src="../../images/landing_page/hr_917x2_footerBreak.jpg" alt="" width="923px" height="2px"/><![endif]-->
    <hr class="footerRuleTop" />
    <hr class="footerRuleBottom" />
</asp:Content>