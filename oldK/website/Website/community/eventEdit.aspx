<%@ Page language="c#" Codebehind="eventEdit.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.eventEdit" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head>
    <title>Edit Event</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
 
	<script type="text/javascript" src="../jscript/yahoo/yahoo-min.js"></script>  
	<script type="text/javascript" src="../jscript/yahoo/event-min.js" ></script>   
	<script type="text/javascript" src="../jscript/yahoo/dom-min.js" ></script>   
	<script type="text/javascript" src="../jscript/yahoo/calendar-min.js"></script>

	<link href="../css/yahoo/calendar.css" type="text/css" rel="stylesheet">   
	<link href="../css/csr.css" rel="stylesheet" type="text/css" />

	<style >
		#cal1Container { display:none; position:absolute; z-index:100;}
		#cal2Container { display:none; position:absolute; z-index:100;}
	</style>

	<script type="text/javascript"><!--

	YAHOO.namespace("example.calendar");
    
	function handleEventStartDate(type,args,obj) {
		var dates = args[0]; 
		var date = dates[0];
		var year = date[0], month = date[1], day = date[2];
		
		var txtDate1 = document.getElementById("txtEventStart");
		txtDate1.value = month + "/" + day + "/" + year;
		YAHOO.example.calendar.cal1.hide();
	}
	
	function handleEventEndDate(type,args,obj) {
		var dates = args[0]; 
		var date = dates[0];
		var year = date[0], month = date[1], day = date[2];
		
		var txtDate2 = document.getElementById("txtEventEnd");
		txtDate2.value = month + "/" + day + "/" + year;
		YAHOO.example.calendar.cal2.hide();
	}
	
	function initEventStart() {
		// Event Calendar
		YAHOO.example.calendar.cal1 = new YAHOO.widget.Calendar ("cal1", "cal1Container", { iframe:true, zIndex:1000, mindate:"1/1/2008", title:"Choose a date:", close:true } );   
		YAHOO.example.calendar.cal1.render();
		
		// Listener to show the Event Calendar when the button is clicked   
		YAHOO.util.Event.addListener("imgSelectStart", "click", YAHOO.example.calendar.cal1.show, YAHOO.example.calendar.cal1, true);   
		YAHOO.example.calendar.cal1.selectEvent.subscribe(handleEventStartDate, YAHOO.example.calendar.cal1, true);
	}

	function initEventEnd() {
		// Event Calendar
		YAHOO.example.calendar.cal2 = new YAHOO.widget.Calendar ("cal2", "cal2Container", { iframe:true, zIndex:1000, mindate:"1/1/2008", title:"Choose a date:", close:true } );   
		YAHOO.example.calendar.cal2.render();
		
		// Listener to show the Event Calendar when the button is clicked   
		YAHOO.util.Event.addListener("imgSelectEnd", "click", YAHOO.example.calendar.cal2.show, YAHOO.example.calendar.cal2, true);   
		YAHOO.example.calendar.cal2.selectEvent.subscribe(handleEventEndDate, YAHOO.example.calendar.cal2, true);
	}
//--> </script>  

  </head>
 
 
  <body MS_POSITIONING="GridLayout">
  
    <form id="Form1" method="post" runat="server">    
    <div>
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<td>
					<table border="0" cellpadding="0" cellspacing="0" width="100%">
						<tr>
							<td class="popupBar">
								<table width="100%" border="0" cellspacing="5" cellpadding="0">
									<tr>
										<td style="font-family: arial; font-size: 18px; color:orange;">
											Edit Event
										</td>

										<td>
											<div align="right">
												<a href="javascript:window.close()" style="text-decoration: none;">Close
													<img src="../images/delete_large.gif" width="11" height="11" border="0" align="absmiddle"></a></div>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td bgcolor="#999999">
								<img runat="server" src="~/images/spacer.gif" width="1" height="2"></td>
						</tr>
					</table>					
				</td>
			</tr>
		</table>
		<asp:panel ID="pnlEdit" Runat="server" >
			<br />
			<center>
			<div style="width:400px;text-align:left;">
			<asp:ValidationSummary ShowMessageBox="False" ShowSummary="True" Class="formError" id="valSum" DisplayMode="BulletList" runat="server" HeaderText="Please correct the following errors:"/>
			</div>
			</center>
			<br />
			<table border="0" cellpadding="0" cellspacing="0" width="100%" style="font-family:arial;">
				<tr>
					<td align=left>
					<table cellpadding="4" cellspacing="0" border="0" width="100%">        
						<tr>
							<td>Title</td>
							<td><asp:TextBox ID="txtTitle" Runat="server" Columns="60" MaxLength="100" />
							<asp:RequiredFieldValidator ID="rfvTxtTitle" Enabled="True" ControlToValidate="txtTitle" Text="*" 
							ErrorMessage="Title is a required field." Display="Dynamic" runat="server"/>						
							</td>
						</tr>
						<tr>
							<td>Start Time</td>
							<td>
								<asp:TextBox ID="txtEventStart" class="formKanevaText" style="width:80px" MaxLength="10" runat="server" />
								<img id="imgSelectStart" name="imgSelectStart" onload="initEventStart();" src="../images/blast/cal_16.gif"/> 
								<div id="cal1Container"></div>
								
								<asp:DropDownList ID="ddlHourStart" Runat="server" />
								<asp:DropDownList ID="ddlMinuteStart" Runat="server" />
								<asp:customvalidator id="cvStartTime" runat="server" errormessage="The Start Time must be before the End Time." text="*"></asp:customvalidator>
							</td>
						</tr>
						<tr>
							<td>End Time</td>
							<td>
								<asp:TextBox ID="txtEventEnd" class="formKanevaText" style="width:80px" MaxLength="10" runat="server" />
								<img id="imgSelectEnd" name="imgSelectEnd" onload="initEventEnd();" src="../images/blast/cal_16.gif"/> 
								<div id="cal2Container"></div>

								<asp:DropDownList ID="ddlHourEnd" Runat="server" />
								<asp:DropDownList ID="ddlMinuteEnd" Runat="server" />
							</td>
						</tr>
						<tr>
							<td>Type</td>
							<td><asp:DropDownList ID="ddlEventType" Runat="server" /></td>
						</tr>
						<tr>
							<td>Location</td>
							<td><asp:TextBox ID="txtLocation" Runat="server" TextMode="MultiLine" Rows="4" Columns="50" /></td>
						</tr>
						<tr>
							<td>Description</td>
							<td><asp:TextBox ID="txtDescription" Runat="server" TextMode="MultiLine" Rows="10"  Columns="50" /></td>
						</tr>
						<tr>
							<td nowrap style="padding-left: 10px; padding-right: 5px;" colspan="2">
								<hr style="border: 1px solid #dfdfdf; width: 100%; text-align: right;">
							</td>
						</tr>
					</table>
					</td>
				</tr>
				<tr>
					<td style="text-align: right; padding-right: 10px; padding-top: 20px;">
						<input type="button" onclick="javascript: window.close();" Value=" cancel " style="cursor: hand; font-weight: bold;"/>&nbsp;&nbsp;<asp:Button ID="btnSave" Runat="server" CssClass="bodyBold" Text="save changes" style="cursor: hand; font-weight: bold;" />
					</td>
				</tr>
			</table>
		</asp:Panel>
		<asp:Panel ID="pnlInvalidAccess" Runat="server" Visible="False" >
			You are not allowed to edit this event
		</asp:Panel>
	</div>
	</form>	
  </body>
</html>