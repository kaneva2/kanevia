///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace  KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for commJoinPrefs.
	/// </summary>
	public class commJoinPrefs : MainTemplatePage
	{
		protected commJoinPrefs () 
		{
			Title = "Join Community - Notification Preferences";
		}

		/// <summary>
		/// Page_Load
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Page_Load (object sender, System.EventArgs e)
		{
			// Has thier email been validated?
			tblNotValidated.Visible = (KanevaWebGlobals.CurrentUser.StatusId.Equals ((int) Constants.eUSER_STATUS.REGNOTVALIDATED));
            lblEmail.Text = KanevaWebGlobals.CurrentUser.Email;
		}

		/// <summary>
		/// Update Click
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnUpdate_Click (object sender, EventArgs e) 
		{
			int channelId = Convert.ToInt32 (Request ["communityId"]);

			UsersUtility.UpdateChannelPreferences (GetUserId (), channelId, Convert.ToInt32 (rblNotifications.SelectedValue), 1, 1, 1, 1, 1, 1);

			bool bGoToCommunity = false;
			if (Request ["gtc"] != null)
			{
				bGoToCommunity = Request ["gtc"].ToString ().Equals ("Y");
			}

			if (bGoToCommunity)
			{
				DataRow drChannel = CommunityUtility.GetCommunity (channelId);
				Response.Redirect (GetBroadcastChannelUrl (drChannel ["name_no_spaces"].ToString ()));
			}
			else
			{
				NavigateBackToBreadCrumb (1);
			}
		}

		/// <summary>
		/// Cancel Click
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnCancel_Click (object sender, EventArgs e) 
		{
			NavigateBackToBreadCrumb (1);
		}

		/// <summary>
		/// btnRegEmail_Click
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnRegEmail_Click (object sender, EventArgs e) 
		{
            MailUtilityWeb.SendRegistrationEmail(KanevaWebGlobals.CurrentUser.Username, KanevaWebGlobals.CurrentUser.Email, KanevaWebGlobals.CurrentUser.KeyValue);
			ShowErrorOnStartup ("Your validation email has been sent.", false);
		}

		protected RadioButtonList rblNotifications;
		protected CheckBox chkItemNotify, chkBlogNotify, chkPostNotify, chkItemReviewNotify, chkBlogCommentsNotify, chkReplyNotify;

		protected HtmlTable tblNotValidated;

		protected Label lblEmail;

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
