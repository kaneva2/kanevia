﻿<%@ Page Language="C#" MasterPageFile="~/masterpages/GenericPageTemplate.Master" AutoEventWireup="true" CodeBehind="Install3dLB.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.community.Install3dLB" %>


<asp:Content ID="cntProfileHead" runat="server" contentplaceholderid="cph_HeadData" >
    
	<script type="text/javascript" src="../jscript/base/base.js"></script>
    <NOSCRIPT><META HTTP-EQUIV="refresh" CONTENT="1" URL="noJavascript.aspx" /></NOSCRIPT>
    <link href="css/myKaneva_NEW.css" type="text/css" rel="stylesheet" />

    <style>

    body {
	   
	    background-image: none;
	    }
	    a:hover{
	    background-color:white;
    }
    
    .PageContainer {background-color:#fff;width:1004px;height:100%;border-top:solid 1px #fff;}
    
    /* FOOTER  - alters footer to work with this page */
.footerAuth {width:720px;clear:both;}
.footerAuth a:link, .footerAuth a:visited, .footerAuth a:active, .footerAuth a:hover {color:#000;}
.footerSection .footerSecTitle {color:#999;border-bottom:solid 1px #999;}
.footer a:link, .footer a:visited, .footer a:active, .footer a:hover {color:#000;}
.footer {color:#999;}
.footerSectionHorizontal span.footerSecTitle, .footerAuth div.footerVersion {color:#999;} 

    </style>
</asp:Content>


<asp:Content id="cntBody" runat="server" contentplaceholderid="cph_Body" >

<div class="content" style="height:800px;text-align:center;background-color:#fff;" id="divContent">    

    <div style="background-color:white;padding-top:10px;"> 
            <a id="aDownload" runat="server" href="~/community/Install3dLB.aspx">
            <img id="Img1" runat="server" BORDER=0 src="~/images/download/thanksfordownload_internal.png" />
            </a>
            
            <hr style="background-color:#dbe5e6;color:#dbe5e6;margin:20px 0;" />
    </div>

    <div>
        <div style="padding-top:15px;"></div>

        <img id="imgDownload" runat="server" src="~/images/download/explorer.png" />
    </div>

</div>

</asp:Content>
