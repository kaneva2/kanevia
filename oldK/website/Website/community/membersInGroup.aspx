<%@ Page language="c#" Codebehind="membersInGroup.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.membersInGroup" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../usercontrols/Pager.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="SearchFilter" Src="../usercontrols/SearchFilter.ascx" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>

<link href="../css/home.css" rel="stylesheet" type="text/css" />		
<link href="../css/kanevaSC.css" rel="stylesheet" type="text/css"/>
<link href="../css/friends.css" rel="stylesheet" type="text/css"/>
<link href="../css/kanevaText.css" type="text/css" rel="stylesheet">
<link href="../css/kanevaBroadBand.css" rel="stylesheet" type="text/css" />

<link href="../css/new.css" rel="stylesheet" type="text/css" />

<script src="../jscript/prototype.js" type="text/javascript" language="javascript"></script>
		
<table border="0" cellspacing="0" cellpadding="0"  width="990" align="center">
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" id="Img1"/></td>
	</tr>
	<tr>
		<td>
			<table  border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="frTopLeft"></td>
					<td class="frTop"></td>
					<td class="frTopRight"></td>
				</tr>
				<tr>
					<td class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img2"/></td>
					<td valign="top" class="frBgIntMembers">
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr colspan="2">
								<td>
								
									<!-- TOP STATUS BAR -->
									<div id="pageheader">
										<table cellpadding="0" cellspacing="0" border="0" width="99%">
											<tr>
												<td align="left">
													<h1><asp:label id="lblGroup" runat="server" /></h1>
												</td>
									
												<td align="right" valign="middle">
													<table cellpadding="0" cellspacing="0" border="0" width="690">
														<tr>
															<td>	
																<ajax:ajaxpanel id="ajpTopStatusBar" runat="server">
																<table cellpadding="0" cellspacing="0" border="0" width="100%">
																	<tr>
																		<td class="headertout" width="175">
																			<asp:label runat="server" id="lblSearch" />
																		</td>
																		<td class="searchnav" width="260"> Sort by: 
																			<asp:linkbutton id="lbSortByName" runat="server" sortby="Name"  onclick="lbSortBy_Click">Name</asp:linkbutton> | 
																			<asp:linkbutton id="lbSortByDate" runat="server" sortby="Date" onclick="lbSortBy_Click">Date Added</asp:linkbutton>
																		</td>
																		<td align="left" width="130">
																			<kaneva:searchfilter runat="server" id="searchFilter" loc="top" hidethumbview="true" showfilterlists="false" assetsperpagelist="10,20,40" />
																		</td>
																		<td class="searchnav" align="right" width="210" nowrap>
																			<kaneva:pager runat="server" isajaxmode="True" id="pgTop" maxpagestodisplay="5" shownextprevlabels="true" />
																		</td>
																	</tr>
																</table>
																</ajax:ajaxpanel>
															</td>	
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</div>
									<!-- END TOP STATUS BAR -->		
								
								</td>
							</tr>
							<tr>
								<td width="968" align="left"  valign="top">
									<table cellpadding="0" cellspacing="0" border="0"  width="968">
										<tr>
											<td width="250" valign="top">
												<div class="module toolbar">
													<span class="ct"><span class="cl"></span></span>
													<h2>Tools & Actions <span class="hlink"></span></h2>
													<table cellspacing="0" cellpadding="10" border="0" width="99%">
														<tr>
															<td align="center">
																<table cellspacing="0" cellpadding="3" border="0" width="100%">
																	<tr>
																		<td>Remove selected from Member Group:</td>
																		<td valign="bottom">
																		
																			<ajax:ajaxpanel id="ajpRemoveButton" runat="server">
																				<asp:imagebutton imageurl="~/images/button_remove.gif" alt="Remove" width="77" height="23" border="0" id="btnRemove" runat="server" onclick="btnRemove_Click"></asp:imagebutton>
																			</ajax:ajaxpanel>
																			
																		</td>
																	</tr>
																</table>
																
																<hr /><br />
																
																<table cellspacing="0" cellpadding="3" border="0" width="100%">
																	<tr>
																		<td colspan="2">
																			<asp:hyperlink runat="server" id="hlShowGroups" text="View Community Member Groups" causesvalidation="False"/>
																			<br/><br/>
																			<asp:hyperlink runat="server" id="hlShowMembers" class="bodyBold" text="View Community Member List" style="vertical-align: top;" causesvalidation="False"/>
																			<br/><br/>
																			<asp:hyperlink runat="server" id="hlAddMember" class="bodyBold" text="Add Member" style="vertical-align: top;" causesvalidation="False"/>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
													<span class="cb"><span class="cl"></span></span>
												</div>
											</td>
											<td width="20"><img runat="server" src="~/images/spacer.gif" width="20" height="1" id="Img3"/></td>
											<td width="698" valign="top" align="center">
												
												<ajax:ajaxpanel id="ajpFriendsList" runat="server">
												<div class="module whitebg">
													<span class="ct"><span class="cl"></span></span>
													<h2 class="alertmessage"><span id="spnAlertMsg" runat="server"></span></h2>
													
													<div align="left" style="padding:5px 10px;">Select: <a href="javascript:void(0);" onclick="Select_All(true);">All</a> | <a href="javascript:void(0);" onclick="Select_All(false);">None</a></div>
													
													<div align="left" style="padding-left:3px;">
													
														<asp:datalist visible="true" runat="server" showfooter="False" id="dlMembers"
															cellpadding="5" cellspacing="0" repeatcolumns="5" repeatdirection="Horizontal" 
															itemstyle-horizontalalign="Center" itemstyle-width="126">
																						
															<itemtemplate>
															
																<!-- MEMBERS BOX -->
																<div class="framesize-medium">
																	<div class="frame">
																		<span class="ct"><span class="cl"></span></span>
																		<div class="imgconstrain">
																			<a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "name_no_spaces").ToString ())%>>
																				<img runat="server" src='<%#GetProfileImageURL (DataBinder.Eval(Container.DataItem, "thumbnail_medium_path").ToString (), "me", DataBinder.Eval(Container.DataItem, "gender").ToString ())%>' border="0"/>
																			</a>
																		</div>
																		<p><span class="content"><a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "name_no_spaces").ToString ())%>><%# TruncateWithEllipsis (DataBinder.Eval(Container.DataItem, "username").ToString(), 9) %></a></span></p>
																		<p class="location"><%# DataBinder.Eval(Container.DataItem, "location") %></p>
																		<p class="location"><%# GetAccountType(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "account_type_id"))) %></p>
																		<p class="<%# Convert.ToInt32(DataBinder.Eval(Container.DataItem, "online")).Equals(1)?"online":"" %>">&nbsp;&nbsp;<asp:label cssclass="online" runat="server" visible='<%# Convert.ToInt32(DataBinder.Eval(Container.DataItem, "online")).Equals(1) %>' text="online" id="lblOnline"/>&nbsp;</p>
																		<p align="center"><span style="padding: 2px;margin-right:8px;"><asp:checkbox runat="server" id="chkEdit" class="formKanevaCheck"/></span></p>
																		<p align="center"><asp:hyperlink id="hlEdit" runat="server" navigateurl='<%#GetMemberEditLink (Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "user_id")))%>' tooltip="Edit">edit</asp:hyperlink>&nbsp;<asp:hyperlink id="hlTransfer" visible='<%# ShowTransfer (Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "account_type_id")), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "community_id")))%> ' runat="server" navigateurl='<%#GetTransferLink (Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "user_id")), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "community_id")))%>' tooltip="Transfer Ownership">transfer</asp:hyperlink></p>
																		<span class="cb"><span class="cl"></span></span>
																		<input type="hidden" runat="server" id="hidMemberId" value='<%#DataBinder.Eval(Container.DataItem, "user_id")%>' name="hidMemberId">
																	</div>
																</div>		
																<!-- END MEMBERS BOX -->
																	
															</itemtemplate>
															
														</asp:datalist>	
																											
													</div>
													
													<span class="cb"><span class="cl"></span></span>
												</div>
												</ajax:ajaxpanel>
												
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img5"/></td>
				</tr>
				<tr>
					<td class="frBottomLeft"></td>
					<td class="frBottom"></td>
					<td class="frBottomRight"></td>
				</tr>
				<tr>
					<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" id="Img6"/></td>
				</tr>
			</table>
		</td>
	</tr>
</table>


