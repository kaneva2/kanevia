<%@ Page language="c#" Codebehind="commJoinPrefs.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.commJoinPrefs" %>

<link href="../css/new.css" rel="stylesheet" type="text/css" />
<link href="../css/friends.css" rel="stylesheet" type="text/css" />

<table border="0" cellspacing="0" cellpadding="0" width="990" align="center">
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" /></td>
	</tr>
	<tr>
		<td>
			<table  border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="frTopLeft"></td>
					<td class="frTop"></td>
					<td class="frTopRight"></td>
				</tr>
				<tr>
					<td class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
					<td valign="top" class="frBgIntMembers">
						
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr colspan="2">
								<td>
								
									<!-- TOP STATUS BAR -->
									<div id="pageheader">
										<table cellpadding="0" cellspacing="0" border="0" width="99%">
											<tr>
												<td align="left">
													<h1>Join World - Notification Preferences</h1>
												</td>
											</tr>
										</table>
									</div>
									<!-- END TOP STATUS BAR -->	
								
								</td>
							</tr>
							<tr>
								<td width="968" align="center"  valign="top">
									<br/>
									<table runat="server" id="tblNotValidated" cellpadding="0" cellspacing="0" border="0" width="98%">
										<tr>
											<td width="98%" valign="top" align="center">
												<div class="module whitebg">
													<span class="ct"><span class="cl"></span></span>
														<h2>NOTICE: Your current address '<asp:label runat="server" id="lblEmail" cssclass="datestamp"/>' has not been validated.</h2>
														<br/>
														<table cellpadding="5" cellspacing="0" border="0" width="90%">
															<tr>
																<td align="center">
																   To validate, click the 'Send Validation' button below, check your email and follow the instructions in the email. Email notifications will not be sent to this account until verification has been completed.
																	<br/><br/>
																	<asp:button id="btnRegEmail" align="right" runat="Server" onclick="btnRegEmail_Click" class="Filter2" text=" Send Validation " causesvalidation="False"/>
																	
																	<br/><br/>
																	<font size="1" face="Verdana"><span class="dateStamp">NOTE: The email will be sent from "kaneva@kaneva.com", if you are using a spam or blocking filter, please check your junk mail folder or add this address so you will receive the email.<br><i>If you do not receive an email within 4 hours please contact support.</i></span></font>
																	<br/>
																</td>
															</tr>
														</table>
														
													<span class="cb"><span class="cl"></span></span>
												</div>
											</td>
										</tr>
									</table>

									
									<table cellpadding="0" cellspacing="0" border="0" width="98%">
										<tr>
											<td width="98%" valign="top" align="center">
												<div class="module whitebg">
													<span class="ct"><span class="cl"></span></span>
														<h2>Email notifications help keep you informed of World activity</h2>
														<br/><br/>
														<asp:radiobuttonlist id="rblNotifications" runat="server" repeatdirection="Vertical" cssclass="Filter2" cellpadding="0">
															<asp:listitem text="Daily Digest - Receive all notifications in daily email" runat="server" value="2" id="Listitem1" name="Listitem1"/>
															<asp:listitem text="Weekly Digest - Receive all notifications in weekly email" runat="server" value="3" id="Listitem2" name="Listitem2"/>
															<asp:listitem text="None - I do not wish to receive any emails for this World" runat="server" value="0" selected="True" id="Listitem3" name="Listitem3"/>
														</asp:radiobuttonlist>
														<br/><br/>
														<asp:button id="btnCancel" runat="Server" onclick="btnCancel_Click" class="Filter2" causesvalidation="False" text="  cancel  " visible="False"/>&nbsp;&nbsp;<asp:button id="btnUpdate" runat="Server" onclick="btnUpdate_Click" class="Filter2" causesvalidation="True" text="  submit  "/>
														<br/><br/>

													<span class="cb"><span class="cl"></span></span>
												</div>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						
					</td>
					<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
				</tr>
				<tr>
					<td class="frBottomLeft"></td>
					<td class="frBottom"></td>
					<td class="frBottomRight"></td>
				</tr>
				<tr>
					<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" /></td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<br/><br/><br/>