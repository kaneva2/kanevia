<%@ Page language="c#" Codebehind="eventView.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.eventView" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head>
    <title>Event</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
  </head>
  <body MS_POSITIONING="GridLayout">
  
    <form id="Form1" method="post" runat="server">    
    <div>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<td>
					<table border="0" cellpadding="0" cellspacing="0" width="100%">
						<tr>
							<td class="popupBar">
								<table width="100%" border="0" cellspacing="5" cellpadding="0">
									<tr>
										<td style="font-family: arial; font-size: 18px; color:orange;">
											View Event
										</td>
										<td>
											<div align="right">
												<a href="javascript:window.close()" style="text-decoration: none;">Close
													<img src="../images/delete_large.gif" width="11" height="11" border="0" align="absmiddle"></a></div>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td bgcolor="#999999">
								<img runat="server" src="~/images/spacer.gif" width="1" height="2"></td>
						</tr>
					</table>					
				</td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" style="font-family:arial;">
			<tr>
				<td align=left>
				<table cellpadding="0" cellspacing="0" border="0" width="100%">        
					<tr>
						<td>Title</td>
						<td><asp:Label ID="lblTitle" Runat="server" />
						</td>
					</tr>
					<tr>
						<td>Start Time</td>
						<td><asp:Label ID="lblStartTime" Runat="server" />
						</td>
					</tr>
					<tr>
						<td>End Time</td>
						<td><asp:Label ID="lblEndTime" Runat="server" />
						</td>
					</tr>
					<%--
					<tr>
						<td>Time Zone</td>
						<td><asp:Label ID="lblTimeZone" Runat="server" /></td>
					</tr>
					--%>
					<tr>
						<td>Type</td>
						<td><asp:Label ID="lblEventType" Runat="server" /></td>
					</tr>
					<tr>
						<td>Location</td>
						<td><asp:Label ID="lblLocation" Runat="server" /></td>
					</tr>
					<tr>
						<td>Description</td>
						<td><asp:Label ID="lblDescription" Runat="server" /></td>
					</tr>
					<tr>
						<td nowrap style="padding-left: 10px; padding-right: 5px;" colspan="2">
							<hr style="border: 1px solid #dfdfdf; width: 100%; text-align: right;">
						</td>
					</tr>
				</table>
				</td>
			</tr>
		</table>
	</div>
	</form>	
  </body>
</html>