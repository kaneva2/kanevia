///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for commInServers.
	/// </summary>
	public class commInServers : ServerBasePage
	{
		public commInServers ()
		{
			Title = "Community Servers";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			int communityId = Convert.ToInt32 (Request ["communityId"]);

			// If this is a private community, make sure they are a member
			if (!CommunityUtility.HasCommunityAccess (communityId, GetUserId (), Request.IsAuthenticated, KanevaWebGlobals.CurrentUser.IsAdult))
			{
				Response.Redirect (ResolveUrl ("~/community/noAccess.aspx?M=PR"));
				return;	
			}
			DataRow drCommunity = CommunityUtility.GetCommunity (communityId);

			if (!IsPostBack)
			{
				BindData (1);

			}

			//btnAddAsset.Visible = UsersUtility.IsUserAdministrator ();
		}

		/// <summary>
		/// Is the current user a moderator of this community?
		/// </summary>
		protected bool IsCommunityModerator ()
		{
			return CommunityUtility.IsCommunityModerator (Convert.ToInt32 (Request ["communityId"]), GetUserId ());
		}

		/// <summary>
		/// Click the refresh button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnRefresh_Click (Object sender, ImageClickEventArgs e) 
		{
			BindData (pgTop.CurrentPageNumber);
		}

		private void pg_PageChange (object sender, PageChangeEventArgs e)
		{
			BindData (e.PageNumber);
		}

		/// <summary>
		///  Bind the data
		/// </summary>
		/// <param name="pageNumber"></param>
		private void BindData (int pageNumber)
		{
			// Set the sortable columns
			SetHeaderSortText (dgrdAsset);
			string orderby = CurrentSort + " " + CurrentSortOrder;

			// Servers
			string filter = "";
			// If they are not admin, don't show staging games
			if (!IsAdministrator ())
			{
				filter = "gl.license_type <> " + (int) Constants.eLICENSE_SUBSCRIPTIONS.STAGING;
			}
			PagedDataTable pds = ServerUtility.GetServers (Convert.ToInt32 (Request ["communityId"]), KanevaGlobals.ServersShowDaysOld, orderby, filter, pageNumber, KanevaGlobals.ServersPerPage);
			pgTop.NumberOfPages = Math.Ceiling ((double) pds.TotalCount / KanevaGlobals.ServersPerPage).ToString ();

			dgrdAsset.DataSource = pds;
			dgrdAsset.DataBind ();

			if (pds.TotalCount == 0)
			{
				lblNoItems.Text = "No game servers found";
			}

			// The results
			lblSearch.Text = GetResultsText (pds.TotalCount, pageNumber, KanevaGlobals.ServersPerPage, pds.Rows.Count);
		}

		/// <summary>
		/// They clicked to sort a column
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected override void btnSort_Click (object sender, System.EventArgs e)
		{
			base.btnSort_Click (sender, e);
			BindData (pgTop.CurrentPageNumber);
		}

		protected Kaneva.Pager pgTop;
		protected DataGrid dgrdAsset;
		protected Label lblSearch, lblNoItems;

		#region Web Form Designer generated code

		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
			pgTop.PageChanged +=new PageChangeEventHandler (pg_PageChange);
		}
		#endregion
	}
}