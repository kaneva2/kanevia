<%@ Page language="c#" MasterPageFile="~/masterpages/GenericPageTemplate.Master" Codebehind="commDrillDown.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.commDrillDown" %>
<%@ Register TagPrefix="Kaneva" TagName="SearchFilter" Src="../usercontrols/SearchFilter.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../usercontrols/Pager.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:content contentplaceholderid="cph_HeadData" id="cntHeader" runat="server">
<meta name="title" content="Kaneva. A Unique Online Community and 3D Social Network." />
<meta name="description" content="Join Kaneva Communities! Each Community has a 3D hang-out in the Virtual World. Get your invite and go In-World to meet up and chat in 3D."/>
<meta name="keywords" content="online community, social network, meet people, friends, networking, games, mmo, movies, 3D, virtual world, sharing photos, video, blog, bands, music, rate pics, digital, rpg, entertainment, join groups, forums, online social networking, caneva, kaneeva, kanneva, kineva" />
<link href="../css/new.css" type="text/css" rel="stylesheet">
<link href="../css/shadow.css" type="text/css" rel="stylesheet">
<link href="../css/base/base_new.css?v1" type="text/css" rel="stylesheet">

<!--CSS file (default YUI Sam Skin) -->
<link href="../css/yahoo/autocomplete/skins/sam/autocomplete.css" rel="stylesheet" type="text/css">


<!--[if IE]> <style type="text/css">@import "../css/new_IE.css";</style> <![endif]-->
<style type="text/css">
    .worldInfoRow
    {
        line-height:20px;
    }    
    .worldInfoRow .requestCount
    {
        background:url(../images/graphic_22x20_RedRequests.png) no-repeat 0px 0px;
        width:22px;
        height:20px;
        display:block;
        float:left;
        font-weight:bold;
        font-size:10px; 
        color:#fff;
        text-align:center; 
        padding-bottom:5px;  
    }
    a.btnmedium {padding-top:0;height:27px;}
    li {margin:2px 0;}
    td.searchtxt {font-size:12px;font-weight:bold;padding:3px 0 0 5px;}
    .searchnav a.selected {border:1px solid #ebebeb;}
    .searchnav a:hover, .searchnav a.selected:hover {text-decoration:underline;}
    .noresults {margin-top:30px;}
    .newcontainerborder {border-collapse:separate;}
    table.thumb_table p.commName {padding-bottom:4px;}
    a.btnsmall.blue {padding-top:6px;height:21px;margin-bottom:10px;}
    .new {color:#ee2d2a;padding-left:8px;}
    .PageContainer {background-color:#fff;width:1004px;height:100%;border:solid 1px #ccc;border-top:none;padding-top:0;}
    #divData {margin:20px 0 0 20px;}
    .shordesc {overflow:hidden;width:170px;}

/* styles for this implementation */
#myAutoComplete {
    width:15em; /* set width for widget here */
    padding-bottom:2em;
    display:block;
}
</style>

 
</asp:content>

<asp:content contentplaceholderid="cphBottomPageJS" id="cntJSBottom" runat="server">
<script src="../jscript/kaneva.js" type="text/javascript"></script>
<script src="../jscript/prototype.js" type="text/javascript"></script>
<script src="../jscript/scriptaculous/scriptaculous.js" type="text/javascript"></script>
<script src="../jscript/scriptaculous/effects.js" type="text/javascript"></script>

<!-- Dependencies -->
<script type="text/javascript" src="../jscript/yahoo/2.3.0/yahoo-min.js"></script>
<script type="text/javascript" src="../jscript/yahoo/2.3.0/dom-min.js"></script>
<script type="text/javascript" src="../jscript/yahoo/2.3.0/yahoo-dom-event.js"></script>
									
<script type="text/javascript" src="../jscript/yahoo/2.3.0/connection-min.js"></script>  
<script type="text/javascript" src="../jscript/yahoo/2.3.0/autocomplete-min.js"></script>
<script type="text/javascript" src="../jscript/yahoo/2.3.0/animation-min.js"></script>

    
<script type="text/javascript">

    // An XHR DataSource
    var myServer = "./getWorldList.aspx";
    var myDataSource = new YAHOO.widget.DS_XHR(myServer, [";", ","]);;

    myDataSource.responseType = YAHOO.widget.DS_XHR.TYPE_FLAT;
    myDataSource.scriptQueryParam = "name";

    var myAutoComp = new YAHOO.widget.AutoComplete('<%=txtKeywords.ClientID%>', 'myContainer', myDataSource);
    myAutoComp.minQueryLength = 2;
    myAutoComp.typeAhead = false;
    myAutoComp.useShadow = true;
    myAutoComp.allowBrowserAutocomplete = false;
    myAutoComp.forceSelection = false;

    myAutoComp.maxResultsDisplayed = 25;
</script>

</asp:content>


<asp:Content ID="cntUpload" runat="server" ContentPlaceHolderID="cph_Body">					


    <div id="divLoading">
        <table cellspacing="0" cellpadding="0" border="0">
            <tr>
                <th height="300" class="loadingText" id="divLoadingText">
                    Loading...
                    <div style="display: none">
                        <img src="../images/progress_bar.gif" /></div>
                </th>
            </tr>
        </table>
    </div>
    <div id="divData" style="display: none">
									
		<table cellpadding="0" cellspacing="0" border="0" width="100%">
			<tr>
				<td width="783" valign="top">
												
					<table border="0" cellspacing="0" cellpadding="0" width="100%">
						<tr>
							<td align="center">
                                			
								<div class="module">
									<span class="ct"><span class="cl"></span></span>

									<asp:Panel id="pnlBasicSearch" DefaultButton="btnSearch" runat="server"> 
										<!-- basic search -->
										<table cellpadding="0" cellspacing="0" border="0" width="98%" align="center">
 											<tr>
												<td valign="top" width="380">
													<table cellpadding="5" cellspacing="0" border="0">
														<tr>
															<td nowrap="nowrap" class="searchtxt">Find <asp:literal id="litFindType" runat="server" text="Communities"></asp:literal></td>
															<td align="left">
                                                                 <div class="yui-skin-sam">
                                                                    <div id="myAutoComplete">
																	    <asp:textbox id="txtKeywords" class="formKanevaText" style="width:250px;margin:3px 0 0 6px;" maxlength="50" runat="server"></asp:textbox>
                                                                        <div id="myContainer"></div>  
																    </div>
                                                                </div>            
                                                                
															</td>
														</tr>
													</table>
																			
												</td>
												<td width="106" align="right">
													<asp:updatepanel id="up1" runat="server">
														<contenttemplate>	
															<asp:linkbutton runat="server" class="btnxsmall grey" id="btnSearch" text="Search" onclick="btnSearch_Click"></asp:linkbutton>
														</contenttemplate>
													</asp:updatepanel>
												</td>
												<td valign="middle" class="small">
													<asp:checkbox id="chkPhotoRequired" runat="server" checked="true" /> Only show Worlds with Thumbnails<br/>
												</td>
											</tr>
										</table>
									</asp:Panel>
									<span class="cb"><span class="cl"></span></span>
								</div>
							</td>
						</tr>
					</table>
												
					<asp:updatepanel id="up2" runat="server">
						<contenttemplate>
												
					<table border="0" cellspacing="0" cellpadding="0" width="100%">
						<tr>
							<td width="180" valign="top">
								<div class="module whitebg">
									<span class="ct"><span class="cl"></span></span>
																
		                                <div id="div_AccessPassFilter" runat="server" style="text-align:left;padding-left:14px;margin-bottom:10px;font-size:11px;">
                                            <asp:CheckBox id="cbx_AccessPass" runat="server" AutoPostBack="true" OnCheckedChanged="APCheckChanged" /> Show only <a id="lnk_AccesPass" href="#" runat="server" >Access Pass</a> <span style="padding-left:24px">content</span> 
		                                </div>
																
									<div id="sortlinks">
										<ul>
											<li class="header" id="liBrowseLabel" runat="server">Browse</li>
											<li id="liBrowse_Relevance" runat="server"><asp:linkbutton text="Relevance" id="lbBrowse_Relevance" commandname="relevance" oncommand="lbBrowse_Click" tooltip="View results by relevance" runat="server" /></li>
											<li id="liBrowse_Raves" runat="server"><asp:linkbutton text="Most Raves" id="lbBrowse_Raves" commandname="raves" oncommand="lbBrowse_Click" tooltip="View results by number of raves" runat="server" /></li>
											<li id="liBrowse_Views" runat="server"><asp:linkbutton text="Most Views" id="lbBrowse_Views" commandname="views" oncommand="lbBrowse_Click" tooltip="View results by number of views" runat="server" /></li>
											<li id="liBrowse_Members" runat="server"><asp:linkbutton text="Most Members" id="lbBrowse_Members" commandname="members" oncommand="lbBrowse_Click" tooltip="View results by number of members" runat="server" /></li>
											<li id="liBrowse_Newest" runat="server"><asp:linkbutton text="Newest" id="lbBrowse_Newest" commandname="newest" oncommand="lbBrowse_Click" tooltip="View results by newest added" runat="server" /></li>
                                            <li id="liBrowse_MostActive" runat="server" visible="false"><asp:linkbutton text="Most Popular" id="lbBrowse_MostActive" commandname="active" oncommand="lbBrowse_Click" tooltip="View results by World activity" runat="server" /></li>
                                            <asp:Repeater id="rptGameCategories" runat="server" visible="false" onitemdatabound="rptGameCategories_ItemDataBound">
                                                <ItemTemplate>
                                                    <li id="liBrowse_GameCategories" runat="server"><asp:linkbutton text='<%# (DataBinder.Eval(Container.DataItem, "description").ToString()) %>' id="lbBrowse_GameCategories" commandname='<%# (DataBinder.Eval(Container.DataItem, "description").ToString()) %>' oncommand="lbBrowse_Click" runat="server" /></li>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                            <li id="liBrowse_AccessPass" runat="server" visible="false"><asp:linkbutton text="Access Pass" id="lbBrowse_AccessPass" commandname="aponly" oncommand="lbBrowse_Click" tooltip="View only access pass results." runat="server" /></li>
                                            <li>&nbsp;</li>
                                            <li id="liBrowse_MostVisited" runat="server" visible="false"><asp:linkbutton text="Most Visited" id="lbBrowse_MostVisited" commandname="visits" oncommand="lbBrowse_Click" tooltip="View results by most visited" runat="server" /></li>
                                            <li id="liBrowse_Requests" runat="server" visible="false"><asp:linkbutton text="Requests" id="lbBrowse_Requests" commandname="requests" oncommand="lbBrowse_Click" tooltip="View results by most world requests" runat="server" /><span class="new" id="spnNew3DAppRequests" runat="server"></span></li>
                                            </ul>
										<ul id="ulTime" runat="server">
											<li class="header">Time</li>
											<li id="liTime_Today" runat="server"><asp:linkbutton text="Today" id="lbTime_Today" commandname="today" oncommand="lbTime_Click" tooltip="View communities added today" runat="server" /></li>
											<li id="liTime_Week" runat="server"><asp:linkbutton text="This Week" id="lbTime_Week" commandname="week" oncommand="lbTime_Click" tooltip="View communities added within the past week" runat="server" /></li>
											<li id="liTime_Month" runat="server"><asp:linkbutton text="This Month" id="lbTime_Month" commandname="month" oncommand="lbTime_Click" tooltip="View communities added within the past month" runat="server" /></li>
											<li id="liTime_All" runat="server"><asp:linkbutton text="All Time" id="lbTime_All" commandname="alltime" oncommand="lbTime_Click" tooltip="View all communities" runat="server" /></li>
										</ul>
										<ul id="ulPlace" runat="server">
											<li class="header">Place</li>
											<li><asp:RequiredFieldValidator id="rfdrpPlace" runat="Server" InitialValue="" Display="Dynamic" Text="*" ControlToValidate="drpPlace"  EnableClientScript="True"></asp:RequiredFieldValidator>
											<asp:DropDownList id="drpPlace" runat="Server" Width="150px" tooltip="View communities by 3D Hangout type"></asp:DropDownList></li>
										</ul>
									</div>
									<span class="cb"><span class="cl"></span></span>
								</div>

														
														
							</td>
							<td valign="top" align="center">
														
								<table border="0" cellspacing="0" cellpadding="0" width="96%" class="nopadding" style="margin-left:20px;">
                                    <tr>
                                        <td width="170" rowspan="2" align="left"><h1 style="font-weight:bold;color:#333;" id="h1ResultsCommType" runat="server">Communities</h1></td>
                                        <td id="tdResultViewMode" runat="server" height="16" class="searchnav" align="right"><kaneva:searchfilter visible="false" runat="server" id="filterTop" loc="top" hidethumbview="true" showfilterlists="false" assetsperpagelist="12,24,40" /> Showing as: 
                                            <asp:linkbutton id="lbThumbTop" runat="server" view="Thumb"  onclick="lbViewAs_Click">Thumbnails</asp:linkbutton>&nbsp;| 
											<asp:linkbutton id="lbDetailTop" runat="server" view="Detail" onclick="lbViewAs_Click">Details</asp:linkbutton>
										</td>
                                    </tr>
                                    <tr>
                                        <td height="16">
											<table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                <tr>
                                                    <td class="searchnav" align="right" valign="bottom" nowrap="nowrap">
                                                        <kaneva:pager runat="server"  id="pgTop" maxpagestodisplay="5" shownextprevlabels="true" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr id="trSearchFilterDesc" runat="server">
                                        <td colspan="2" align="left"><h2 style="color:#6f6f6f;"><span id="spnSearchFilterDesc" runat="server"></span></h2></td>
                                    </tr>
                                </table>
			
						
								<!-- CHANNEL DETAIL VIEW -->
								<asp:datalist visible="true" runat="server" enableviewstate="False" showfooter="False" width="590" id="dlChannelDetail" style="margin-top:10px;"
									cellpadding="5" cellspacing="0" border="0" itemstyle-horizontalalign="Center" repeatcolumns="2" repeatdirection="Horizontal" cssclass="detail_table"
                                    OnItemDataBound="dlChannelDetail_ItemDataBound">
									<itemtemplate>	
																	
										<div class="module whitebg">
											<span class="ct"><span class="cl"></span></span>
											<table cellpadding="0" cellspacing="0" border="0" width="280">
												<tr>
													<td width="85" valign="top" align="center">
														<div class="framesize-small">
															<div id="divIsAdult" class="restricted" runat="server"></div>
															<div class="imgconstrain">
																<a id="aCommPic" runat="server" href='' style="cursor: hand;">
																<img id="Img1" runat="server" src='' alt='' border="0"/></a>
															</div>
														</div>
													</td>																		 
													<td align="left" valign="top" width="264" style="padding-bottom:0px;">
														<p><a id="aCommName" runat="server" href='' title=""></a></p>
														<div style="height:56px;margin-top:4px;">
														<p id="pDescription" class="shordesc" runat="server"></p>
														</div>
													</td>
												</tr>
												<tr>
													<td colspan="2" style="padding:0 5px 0 10px;">
														<table cellpadding="0" cellspacing="0" width="100%" border="0" class="stats">
															<tr id="trCommunityInfo" runat="server" visible="false">
																<td align="left" width="36%" style="padding-top:0px;">
																	<p class="small" id="pRaves" runat="server"></p>
																	<p class="small" id="pViews" runat="server"></p>
																	<p class="small" id="pMembers" runat="server"></p>
																</td>
																<td>
																	<table cellpadding="0" cellspacing="0" width="100%" border="0" class="stats">
																		<tr>
																			<td align="right" width="85%">
																				<p class="small">Owner:</p>
																				<p class="small"><a id="aCommCreator1" runat="server" href="" title=""></a></p>
																			</td>
																			<td>
																				<a id="aCommCreator2" runat="server" href="" style="cursor:hand;">
																				<img id="Img2" runat="server" src='' alt='' width="30" height="30" hspace="5" border="0"/></a>
																			</td>
																		</tr>
																	</table>
																</td>	
															</tr>
                                                            <tr id="trWorldInfo" runat="server" visible="false" class="worldInfoRow">
                                                                <td id="tdRequestCount" runat="server" align="left" width="50%" style="padding:0 0 0 12px;font-size:11px;">
                                                                    <span id="spnRequestCount" class="requestCount" runat="server"></span>&nbsp;&nbsp;requests
																</td>
                                                                <td align="left" width="50%" style="padding:10px 17px 5px 0;line-height:14px;">
                                                                    <asp:linkbutton id="btnMeet3D" class="btnsmall blue" style="padding-top:4px;height:22px;" runat="server" CausesValidation="False" text="Play Now"></asp:linkbutton>
                                                                </td>
                                                            </tr>
														</table>	
													</td>
												</tr>
											</table>
											<span class="cb"><span class="cl"></span></span>
										</div>
																	
									</itemtemplate>
								</asp:datalist>
								<!-- END CHANNEL DETAIL VIEW -->
															
								<!-- CHANNEL THUMB VIEW -->
								<asp:datalist visible="False" runat="server" enableviewstate="False" showfooter="False" width="99%" id="dlChannelThumb"
									cellpadding="0" cellspacing="0" border="0" itemstyle-width="25%"  itemstyle-horizontalalign="Center" repeatcolumns="3" repeatdirection="Horizontal" cssclass="thumb_table" onitemdatabound="dlChannelThumb_ItemDataBound">
																
									<itemtemplate>
																																											
										<div class="framesize-medium">
											<div id="divIsAdult" class="restricted" runat="server"></div>
											<div>
												<div class="imgconstrain" style="border-style:none;">
													<a id="aCommPic" runat="server">
														<img id="imgCommPic" runat="server" border="0" />
													</a>
												</div>	
											</div>
										</div>
										<p class="commName"><a id="aCommName" runat="server"></a></p>
                                        <p><asp:linkbutton  id="btnPlayNow" runat="server" class="btnxsmall blue">Play Now</asp:linkbutton></p>
										<p id="pRavesAndViews" runat="server" class="small"></p>
										<p id="pMembers" runat="server" class="small"></p>
																	
									</itemtemplate>
															
								</asp:datalist>		
								<!-- END CHANNEL THUMB VIEW -->
													
								<div id="divNoResults" runat="server" visible="false" class="noresults"></div>
						
								<div class="formspacer"></div>
															
								<table cellpadding="0" cellspacing="0" border="0" width="92%">
									<tr>
										<td align="left">
											<asp:label id="lblResultsBottom" runat="server" visible="false"></asp:label>
										</td>
										<td class="searchnav" align="right" valign="bottom" nowrap="nowrap">
											<kaneva:pager runat="server" id="pgBottom" maxpagestodisplay="5" shownextprevlabels="true" />
										</td>
									</tr>
								</table>
									
								<div class="formspacer"></div>
															
														

							</td>
						</tr>
					</table>
												
						</contenttemplate>
					</asp:updatepanel>
												
				</td>
											
				<td width="10"></td>
											
				<td width="175" align="right" valign="top">
												
                    <div id="ccom"><a class="btnmedium grey" style="padding-top:6px;height:21px;" id="aCreateCommunity" runat="server" href="~/community/StartWorld.aspx" visible="false">Create A World</a></div>
                    <div id="c3dapp">
						<a class="btnmedium grey" style="padding-top:6px;height:21px;" id="aCreate3DApp" runat="server" visible="false" href="~/community/StartWorld.aspx">Create a World</a><br />
						<a class="btnmedium grey" style="padding-top:6px;height:21px;" id="aMyWorlds" runat="server" visible="false" href="~/mykaneva/my3dapps.aspx">My Worlds</a>
					</div>
												
				</td>
			</tr>
		</table>
		

	</div>



</asp:Content>			