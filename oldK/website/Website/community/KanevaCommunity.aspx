<%@ Page Language="C#" MasterPageFile="~/masterpages/GenericLandingPageTemplate.Master" AutoEventWireup="false" CodeBehind="KanevaCommunity.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.community.KanevaCommunity" %>

<asp:Content ID="cntCommunityMetaData" runat="server" contentplaceholderid="cphHeader" >
    <asp:Literal ID="litHeader" runat="server"></asp:Literal>
    <script type="text/javascript" src="../jscript/prototype-1.6.1.js"></script>
    <script type="text/javascript" src="../jscript/scriptaculous/1.8.2/scriptaculous.js"></script>
    <script type="text/javascript" src="../jscript/SWFObject/swfobject.js"></script>
    <script type="text/javascript" src="../jscript/base/Base.js"></script>
    <NOSCRIPT><META HTTP-EQUIV="refresh" CONTENT="1" URL="noJavascript.aspx" /></NOSCRIPT>
    
</asp:Content>

<asp:Content ID="cntKanevaCommunityBody" runat="server" contentplaceholderid="cphBody" >
    <asp:Literal ID="litContent" runat="server"></asp:Literal>


	<style>
	.pageBody {padding:0 0 20px 0;width:1000px;background:#fff;border:1px solid #ccc;border-top:none;}
	</style>
</asp:Content>