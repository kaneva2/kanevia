<%@ Page language="c#" Codebehind="commInServers.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.commInServers" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../usercontrols/Pager.ascx" %>

<center>
<table cellpadding="0" cellspacing="0" border="0" width="900">
	<tr>
		<td valign="middle" class="belowFilter" colspan="2" width="400" height="23" align="left">&nbsp;<asp:imagebutton runat="server" ID="btnRefresh" ImageUrl="~/images/button_refresh.gif" alt="Refresh Server List" class="Filter2" Text="refresh" OnClick="btnRefresh_Click"/>&nbsp;</td><td colspan="5" width="500" height="23" align="right" class="belowFilter2" valign="middle"><br><br><asp:Label runat="server" id="lblSearch" CssClass="assetCommunity"/><br><br><Kaneva:Pager runat="server" id="pgTop"/><br><br></td>						
	</tr>
	<tr>
		<td colspan="9" class="FullBorders">
			<asp:DataGrid EnableViewState="False" runat="server" ShowFooter="False" Width="100%" id="dgrdAsset" cellpadding="0" cellspacing="0" border="0" AutoGenerateColumns="False" AllowSorting="True">  
				<HeaderStyle CssClass="lineItemColHead"/>
				<ItemStyle CssClass="lineItemEven"/>
				<AlternatingItemStyle CssClass="lineItemOdd"/>
				<Columns>
					<asp:TemplateColumn HeaderText="type" ItemStyle-Width="1%" ItemStyle-HorizontalAlign="center">
						<ItemTemplate>
							<%# GetGameType (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "license_type"))) %>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="server name" SortExpression="server_name" ItemStyle-Width="25%" ItemStyle-HorizontalAlign="Left">
						<ItemTemplate>
							<%# TruncateWithEllipsis (Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "server_name").ToString ()), 30) %>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="game" SortExpression="game_name" ItemStyle-Width="25%" ItemStyle-HorizontalAlign="Center">
						<ItemTemplate>
							<a class="dateStamp" href='<%# GetAssetDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "asset_id"))) %>'><%# TruncateWithEllipsis (Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "game_name").ToString ()), 30) %></a>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="players" SortExpression="number_of_players" ItemStyle-Width="9%" ItemStyle-HorizontalAlign="Center">
						<ItemTemplate>
							<%# DataBinder.Eval (Container.DataItem, "number_of_players") %>/<%# DataBinder.Eval (Container.DataItem, "max_players") %>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="uptime" SortExpression="last_ping_datetime" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Center">
						<ItemTemplate>
							<%# GetDateTimeDifference (DataBinder.Eval (Container.DataItem, "engine_started_date"),Convert.ToInt32(DataBinder.Eval (Container.DataItem, "elapsed_seconds")), Convert.ToInt32(DataBinder.Eval (Container.DataItem, "status_id"))) %>
						</ItemTemplate>
					</asp:TemplateColumn>
				</Columns>
			</asp:datagrid>
		</td>
	</tr>
	<tr>
		<td colspan="8" align="center" class="orangeHeader3"><br><br><asp:Label runat="server" id="lblNoItems"/></td>
	</tr>

</table>

</center>
<br><br>