///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Threading;
using log4net;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for commJoin.
	/// </summary>
	public class commJoin : MainTemplatePage
	{
		private void Page_Load(object sender, System.EventArgs e)
		{
			// They must be logged in
			if (!Request.IsAuthenticated)
			{
				// Add community id here to draw the channel nav in case they are already in a public channel
				string loginURL = GetLoginURL();

				if (loginURL.IndexOf("communityId") < 0)
				{
					loginURL += "&communityId=" + _communityId;
				}
				
				Response.Redirect (loginURL);
				return;
			}

            GetRequestParams();

            // What do they want to do?
			if (_bjoin)
			{
                GetCommunityFacade.JoinCommunity(_communityId, KanevaWebGlobals.CurrentUser.UserId);

                Community community = GetCommunityFacade.GetCommunity(_communityId);
                Response.Redirect(GetBroadcastChannelUrl(community.NameNoSpaces));
			}
			else
			{
                // They are trying to quit
                CommunityMember communityMember = GetCommunityFacade.GetCommunityMember(_communityId, KanevaWebGlobals.CurrentUser.UserId);
                if (communityMember.StatusId != (UInt32)CommunityMember.CommunityMemberStatus.LOCKED)
                {
                    GetCommunityFacade.UpdateCommunityMember(_communityId, KanevaWebGlobals.CurrentUser.UserId, (UInt32)CommunityMember.CommunityMemberStatus.DELETED);
                }

                NavigateBackToBreadCrumb(1);
			}
		}

        private void GetRequestParams()
        {
            //check to see if there is a name for the game (required)
            try
            {
                if (Request["communityId"] != null)
                {
                    _communityId = Convert.ToInt32(Request["communityId"].ToString());
                }
            }
            catch (Exception ex)
            {
                m_logger.Error("CommunityId was not provided ", ex);
            }

            try
            {
                if (Request["join"] != null)
                {
                    _bjoin = Request["join"].ToString().ToUpper().Equals("Y");
                }
            }
            catch (Exception)
            {
            }
        }

        #region Declerations

        private Int32 _communityId = 0;
        private bool _bjoin = false;
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #endregion Declerations

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
