///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva.channel;

using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for members.
	/// </summary>
	public class members : SortedBasePage
	{
		protected members () 
		{
			Title = "Community Members";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			if(Request.IsAuthenticated)
			{
				if(GetRequestParams())
				{
					//check if user is allowed to edit this channel
					if(!IsUserAllowedToEdit())
					{
						RedirectToHomePage ();
					}
				}
				else
				{
					//invalid request params
					//if the user logged in, send him to his home page
					//if not, redirect to login page
					RedirectToHomePage ();
				}
			}
			else
			{
				Response.Redirect(this.GetLoginURL());
			}

			// Set Nav
            HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB._UPDATE_;
			HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.CHANNELS;
			HeaderNav.SetNavVisible(HeaderNav.MyKanevaNav,2);
			
			if (!IsPostBack)
			{
				lbSortByName.CssClass = "selected";

				BindData (1);

				LoadMemberGroupDropdown(_channelId);

				hlShowGroups.NavigateUrl = "~/community/memberGroups.aspx?communityId=" + _channelId;
				hlAddMember.NavigateUrl = "~/community/inviteMember.aspx?communityId=" + _channelId;
			}
		}

		/// <summary>
		/// Bind the data
		/// </summary>
		/// <param name="pageNumber"></param>
		private void BindData (int pageNumber)
		{
			BindData (pageNumber, "");
		}
		/// <summary>
		/// Bind the data
		/// </summary>
		/// <param name="pageNumber"></param>
		private void BindData (int pageNumber, string filter)
		{
			searchFilter.CurrentPage = "members";
			
			string orderby = CurrentSort + " " + CurrentSortOrder;
			int pgSize = searchFilter.NumberOfPages;

			string filterBy = "";
			if(Request["accountTypeId"] != null)
			{
				//filter by acc type
				filterBy += " cm.account_type_id = " + Request["accountTypeId"];
			}

			PagedDataTable pds = CommunityUtility.GetCommunityUsers(_channelId, (UInt32) CommunityMember.CommunityMemberStatus.ACTIVE, true, false, false, false, 
				filterBy, orderby, pageNumber, pgSize);
			dlMembers.DataSource = pds;
			dlMembers.DataBind ();

			pgTop.NumberOfPages = Math.Ceiling ((double) pds.TotalCount / pgSize).ToString ();
			pgTop.DrawControl ();

            Community c = GetCommunityFacade.GetCommunity (_channelId);
            worldName.InnerText = c.Name;
            worldName.HRef = GetBroadcastChannelUrl (c.NameNoSpaces) + "?tab=" + (int) Constants.eTab.Members;

			// The results
			lblSearch.Text = GetResultsText (pds.TotalCount, pageNumber, pgSize, pds.Rows.Count);
		}


		#region Helper Methods
		/// <summary>
		/// GetMemberEditLink
		/// </summary>
		public string GetMemberEditLink (int userId)
		{
			return (ResolveUrl ("~/community/commInMemberEdit.aspx?communityId=" + Convert.ToInt32 (Request ["communityId"]) + "&userId=" + userId + "&returnURL=~/community/commInMembers.aspx"));
		}

		/// <summary>
		/// GetTransferLink
		/// </summary>
		public string GetTransferLink (int userId, int communityId)
		{
			return (ResolveUrl ("~/community/commInTransfer.aspx?communityId=" + Convert.ToInt32 (Request ["communityId"]) + "&userId=" + userId));
		}

		/// <summary>
		/// ShowTransfer
		/// </summary>
		/// <returns></returns>
		protected bool ShowTransfer (int accountTypeId, int communityId)
		{
			// You cannot transfer it to the existing channel owner
			if (accountTypeId.Equals ((int) CommunityMember.CommunityMemberAccountType.OWNER))
			{
				return false;
			}

			// Now only Site Admins can transfer http://qa4/mantis/view.php?id=3793
			//return CommunityUtility.IsCommunityOwner (communityId, GetUserId ());
			return IsAdministrator ();
		}

		protected string GetAccountType(int accountTypeId)
		{
			if (accountTypeId.Equals ((int) CommunityMember.CommunityMemberAccountType.MODERATOR))
			{
				return "moderator";
			}
			else if (accountTypeId.Equals ((int) CommunityMember.CommunityMemberAccountType.OWNER))
			{
				return "owner";
			}
			else 
				return "";
		}

		private bool GetRequestParams()
		{
			bool retVal = true;
			try
			{
				_channelId = Convert.ToInt32(Request.Params["communityId"]);
			}
			catch(Exception)
			{
				//invalid numbers
				retVal = false;
			}
			return retVal;
		}

		/// <summary>
		/// returns true if current user is allowed to edit this channel
		/// </summary>
		/// <returns></returns>
		private bool IsUserAllowedToEdit()
		{
			return CommunityUtility.IsCommunityModerator(_channelId, GetUserId());
		}

		/// <summary>
		/// Load the list of Friend Groups in the drop down
		/// </summary>
		/// <returns></returns>
		protected void LoadMemberGroupDropdown (int channelId)
		{
			drpMemberGroups.Items.Clear ();
			
			DataTable Groups = CommunityUtility.GetMemberGroups (channelId); 

			drpMemberGroups.Items.Add (CreateListItem ("Pick a Member Group", "", true));

			if (Groups.Rows.Count > 0)
			{
				drpMemberGroups.Items.Add (CreateListItem ("Add to group:", "", true));

				for (int i = 0; i < Groups.Rows.Count; i++)
				{
					drpMemberGroups.Items.Add (new ListItem (Server.HtmlDecode (Groups.Rows[i]["name"].ToString()), Groups.Rows[i]["id"].ToString()));
				}
			}

			drpMemberGroups.SelectedIndex = 0;
		}

		#endregion

		#region Event Handlers
		/// <summary>
		/// Click the add button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void hlAddGroup_Click (Object sender, System.EventArgs e) 
		{
			lbAddGroup.Visible = false;
			inpAG.Visible = true;
			btnSaveG.Visible = true;
			btnCancelG.Visible = true;
			spnGN.Visible = true;
			hlShowGroups.Visible = false;

			BindData (pgTop.CurrentPageNumber);
		}

		/// <summary>
		/// Click the invite a member button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnInviteMember_Click (Object sender, ImageClickEventArgs e) 
		{
			Server.Transfer (ResolveUrl ("~/community/inviteMember.aspx"));
		}

		/// <summary>
		/// Execute when the user clicks the the view type link button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void lbSortBy_Click (object sender, EventArgs e) 
		{
			lbSortByName.CssClass = lbSortByDate.CssClass = "";

			if (((LinkButton)sender).Attributes["sortby"].ToString().ToLower() == "name")
			{
				CurrentSort = "username";
				lbSortByName.CssClass = "selected";
			}
			else
			{
				CurrentSort = "added_date";
				lbSortByDate.CssClass = "selected";							  
			}

			BindData (pgTop.CurrentPageNumber);
		}
		/// <summary>
		/// Execute when the user clicks the the Add button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnAdd_Click (object sender, ImageClickEventArgs e)
		{
			HtmlInputHidden hidMemberId;
			CheckBox chkEdit;
			int ret = 0;
			int count = 0;
			bool isItemChecked = false;

			if (drpMemberGroups.SelectedValue != "")
			{
				if (KanevaGlobals.IsNumeric (drpMemberGroups.SelectedValue))
				{
					int memberGroupId = Convert.ToInt32 (drpMemberGroups.SelectedValue);

					foreach (DataListItem dliMember in dlMembers.Items)
					{
						chkEdit = (CheckBox) dliMember.FindControl ("chkEdit");

						if (chkEdit.Checked)
						{
							isItemChecked = true;

							hidMemberId = (HtmlInputHidden) dliMember.FindControl ("hidMemberId");
							ret = CommunityUtility.InsertMemberInGroup (_channelId, memberGroupId, 
								Convert.ToInt32 (hidMemberId.Value));
						
							if (ret == 1)
								count++;
						}
					}

					if (isItemChecked)
					{
						if (count > 0)
						{
							BindData (pgTop.CurrentPageNumber);
						}
						
						if (ret != 1)
						{
							spnAlertMsg.InnerText = "An error was encountered while trying to add member to a member group.";
						}
						else
						{
							spnAlertMsg.InnerText = "Successfully added " + count + " member" + (count != 1 ? "s" : "") + " to member group.";
						}
					}
					else
					{
						spnAlertMsg.InnerText = "No members were selected.";
					}
				}
			}
			else
			{
				spnAlertMsg.InnerText = "Member group was not selected.";
			}

			MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('spnAlertMsg', 5000);");
		}
		
		/// <summary>
		/// Execute when the user clicks the Delete button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnDelete_Click (object sender, ImageClickEventArgs e)
		{
			HtmlInputHidden hidMemberId;
			CheckBox chkEdit;
			int ret = 0;
			int count = 0;
			bool isItemChecked = false;

			// Remove the members
			foreach (DataListItem dliMember in dlMembers.Items)
			{
				chkEdit = (CheckBox) dliMember.FindControl ("chkEdit");

				if (chkEdit.Checked)
				{
					isItemChecked = true;

					hidMemberId = (HtmlInputHidden) dliMember.FindControl ("hidMemberId");

                    // Don't delete if they are the community owner

                    if (!CommunityUtility.IsCommunityOwner(_channelId, Convert.ToInt32(hidMemberId.Value), false))
                    {
                        ret = GetCommunityFacade.UpdateCommunityMember(_channelId, Convert.ToInt32(hidMemberId.Value), 
						    (UInt32) CommunityMember.CommunityMemberStatus.DELETED);

                        if (ret == 1)
                            count++;
                    }
				}
			}

			if (isItemChecked)
			{
				if (count > 0)
				{
					BindData (pgTop.CurrentPageNumber);
				}
				
				if (ret != 1)
				{
					spnAlertMsg.InnerText = "An error was encountered while trying to remove members.";
				}
				else
				{
					spnAlertMsg.InnerText = "Successfully removed " + count + " member" + (count != 1 ? "s" : "") + ".";
				}
			}
			else
			{
				spnAlertMsg.InnerText = "No members were selected.";
			}

			MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('spnAlertMsg', 5000);");
		}
		/// <summary>
		/// Page Change
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pgTop_PageChange (object sender, PageChangeEventArgs e)
		{
			BindData (e.PageNumber);
		}
		/// <summary>
		/// Execute when the user selects an item from the Store Filter
		/// Items to display drop down list
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void FilterChanged(object sender, FilterChangedEventArgs e)
		{
			searchFilter.SetPagesDropValue(searchFilter.NumberOfPages.ToString());
			
			BindData (1, e.Filter);
		}

		#endregion

		#region Properties
		/// <summary>
		/// DEFAULT_SORT
		/// </summary>
		/// <returns></returns>
		protected override string DEFAULT_SORT
		{
			get
			{
				return "username";
			}
		}

		#endregion

		#region Decleraions
		protected Kaneva.Pager pgTop;
		protected Kaneva.SearchFilter	searchFilter;

		protected CheckBox		chkEdit;
		protected DropDownList	drpMemberGroups;
		protected Label			lblSearch;
		protected LinkButton	lbSortByName, lbSortByDate;
		protected DataList		dlMembers;
		protected LinkButton	lbAddGroup;
		protected HyperLink		hlShowGroups;
		protected HyperLink		hlAddMember;
		protected Button		btnCancelG, btnSaveG;
		protected ImageButton	btnAddMember, btnAddGroup;
		
		protected HtmlInputText			inpAG;
		protected HtmlGenericControl	spnGN;
        protected HtmlContainerControl spnAlertMsg;
        protected HtmlAnchor worldName;
		
		private int _channelId;

		#endregion

		#region Web Form Designer generated code

		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
			pgTop.PageChanged +=new PageChangeEventHandler (pgTop_PageChange);
			searchFilter.FilterChanged +=new FilterChangedEventHandler (FilterChanged);		
		}
		#endregion
	}
}
