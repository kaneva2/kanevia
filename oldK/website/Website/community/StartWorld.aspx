﻿<%@ Page Language="C#" MasterPageFile="~/masterpages/GenericPageTemplate.Master" ValidateRequest="True" AutoEventWireup="false" CodeBehind="StartWorld.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.StartWorld" %>

<asp:Content ID="cnt_ProfileHead" runat="server" ContentPlaceHolderID="cph_HeadData">
    <meta name="title" content="Kaneva. The Online Community and Social Network." />
    <meta name="description" content="Kaneva is the first to combine social networking and a free 3D virtual world. It's a whole new way to connect with friends. Have fun in a vibrant virtual world full of people, cool places and fun games. Create your avatar and get a free 3D home today!" />
    <meta name="keywords" content="free virtual world, virtual world, virtual worlds, 3D world, online avatar community, mmo game, virtual reality, caneva, kaniva" />
    <noscript>
        <meta http-equiv="refresh" content="1" url="noJavascript.aspx" />
    </noscript>
    <script type="text/javascript" src="../jscript/kaneva.js"></script>
    <script type="text/javascript" src="../jscript/prototype-1.6.1-min.js"></script>
    <link href="../css/Communities/CommunityCreate.css?v9" type="text/css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="mainContent" runat="server" ContentPlaceHolderID="cph_Body">
    <asp:HiddenField ID="hfAsyncPollingResponder" runat="server" ClientIDMode="Static" OnValueChanged="hfAsyncPollingResponder_ValueChanged" />
    <div id="divLoading" runat="server" clientidmode="Static">
        <table height="300" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <th class="loadingText" id="divLoadingText" runat="server" clientidmode="Static">
                    Loading...
                    <div style="display: none">
                        <img src="../images/progress_bar.gif">
                    </div>
                </th>
            </tr>
        </table>
    </div>
    <div id="divData" style="display: none" runat="server" clientidmode="Static">
        <div id="contentBody">
            <div id="alertMsg" name="alertMsg" clientidmode="Static" runat="server" class="errBox" visible="false"></div>
            <div id="pageHeader">
                <h6><span></span>Create Your World</h6>
            </div>
            <div id="commityBody" runat="server" ClientIDMode="static" visible="true">
                <asp:UpdatePanel ID="upTemplateInfo" ChildrenAsTriggers="False" runat="server" UpdateMode="Conditional"
                        RenderMode="Block" ClientIDMode="Static">
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="drpTemplate" EventName="SelectedIndexChanged" />
                        <asp:AsyncPostBackTrigger ControlID="txtTitle" EventName="TextChanged" />
                    </Triggers>
                    <ContentTemplate>
                        <div id="templateInfo">
                            <img id="imgTemplatePreview" runat="server" alt="Template Preview" height="275" width="203" src="#" clientidmode="static" />
                            <div id="divTemplateDescription">
                                <p id="pTemplateDescription" runat="server"></p>
                                <asp:repeater id="rptTemplateFeatures" runat="server">
                                    <HeaderTemplate><ul></HeaderTemplate>
                                    <ItemTemplate>
                                        <li><%#DataBinder.Eval(Container, "DataItem").ToString() %></li>
                                    </ItemTemplate>
                                    <FooterTemplate></ul></FooterTemplate>
                                </asp:repeater>
                                <span id="litExtraData" runat="server"><br/><br/><a href="http://docs.kaneva.com/mediawiki/index.php/Main_Page#Developers">Learn more about developer API's and documentation</a></span>
                            </div>   
                        </div>
                        <div id="communityInfo">
                            <div id="vipUpsell" runat="server" clientidmode="Static" visible="false">
                                <img src="../images/icons/icon_VIP_pass.jpg" alt="VIP icon" />
                                <p>Become a VIP member to create a Town world.</p>
                                <a class="button btnsmall blue" id="a1" runat="server" clientidmode="Static" href="~/mykaneva/passDetails.aspx?pass=true&passId=74">Get VIP Pass</a>
                            </div>
                            <p>World Information</p>
                            <asp:DropDownList ID="drpTemplate" runat="server" Width="306" OnSelectedIndexChanged="drpTemplate_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                            <span>
                                <asp:RequiredFieldValidator ID="rfItemName" runat="server" Display="None" ErrorMessage="World Name is a required field." ControlToValidate="txtTitle" EnableClientScript="False"></asp:RequiredFieldValidator>
                            </span>
                            <asp:TextBox ID="txtTitle" runat="server" MaxLength="50" OnTextChanged="txtTitle_TextChanged" AutoPostBack="true" ClientIDMode="Static"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="revTitle" runat="server" Display="None" ControlToValidate="txtTitle" EnableClientScript="False"></asp:RegularExpressionValidator>
                            <br />
                            <span id="spnCheckUsername" runat="server"></span>
                            <span>
                                <asp:RequiredFieldValidator ID="rftxtDescription" runat="server" Display="None"
                                    ErrorMessage="World Description is a required field." ControlToValidate="txtDescription" EnableClientScript="False"></asp:RequiredFieldValidator></span><br />
                            <asp:TextBox ID="txtDescription" runat="server" Rows="4" MaxLength="255"
                                TextMode="multiline" ClientIDMode="Static"></asp:TextBox>
                            <br />
                            <br />
                            <a class="button playNow btnsmall blue" id="aLaunch" runat="server" clientidmode="Static"
                                href="javascript:void(0);" onclick="javascript:Create3Dapp();"
                                onserverclick="bntCreate_Click">Create</a>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <br style="clear: both;" />
            </div>
            <div id="divCreateSuccess" runat="server" visible="false" ClientIDMode="static">
                <p class="successMsg">Your World is loading...</p><br />
                <p>If Kaneva doesn't open within 30 seconds, <asp:linkbutton id="btnGoToWorld" runat="server" title="Go to your World!" CausesValidation="False">Click Here</asp:linkbutton></p>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="../jscript/Communities/CommunityCreate.js"></script>
    <script type="text/javascript">
        var $j = jQuery.noConflict();

        $j(document).ready(function () {
            $j('#txtTitle').watermark(' Enter World Name');
            $j('#txtDescription').watermark(' Enter World Description');
        });
    </script>
</asp:Content>
