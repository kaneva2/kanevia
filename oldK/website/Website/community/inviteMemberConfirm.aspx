<%@ Page language="c#" Codebehind="inviteMemberConfirm.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.inviteMemberConfirm" %>

<link href="../css/home.css" rel="stylesheet" type="text/css" />		
<link href="../css/kanevaSC.css" rel="stylesheet" type="text/css"/>
<link href="../css/friends.css" rel="stylesheet" type="text/css"/>
<link href="../css/kanevaText.css" type="text/css" rel="stylesheet">
<link href="../css/kaneva.css" type="text/css" rel="stylesheet">

<link href="../css/new.css" rel="stylesheet" type="text/css" />

<table border="0" cellspacing="0" cellpadding="0" class="newcontainer" align="center">
	<tr>
		<td>
			<table  border="0" cellpadding="0" cellspacing="0" width="100%" class="newcontainerborder">
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
				<tr>
					<td class=""><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img2"/></td>
					<td valign="top" class="newdatacontainer">


						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
									<div id="pageheader">
										<table cellpadding="0" cellspacing="0" border="0" width="99%">
											<tr><td align="left"><h1>Member request email sent!</h1></td></tr>
										</table>
									</div>
								</td>
							</tr>
							<tr>
								<td align="center"  valign="top">
									<table cellpadding="0" cellspacing="0" border="0"  width="98%">
										<tr>
											<td valign="top" align="center">
												<div class="module whitebg">
													<span class="ct"><span class="cl"></span></span>
																										
														<table border="0" cellspacing="0" cellpadding="8" width="98%">
															<tr>
																<td align="center">
																	<table cellpadding="0" cellspacing="0" border="0" width="99%">
																		<tr>
																			<td align="center" valign="top" class="bodyText">																				
																				When your requested member accepts your invitation they will appear in your members list.<br> 
																				Check your list periodically to see when they have joined. As with all emails,<br>
																				it may take some time for your requested member to respond.<br/><br/><br/>
																				<input runat="server" type="button" value="back to members" style="cursor: hand;" id="btnBack" name="Button1"/>
																			</td>
																		</tr>
																	</table>
	
																</td>
															</tr>
														</table>
															
													<span class="cb"><span class="cl"></span></span>
												</div>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>

 						
					</td>
					<td class=""><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img3"/></td>
				</tr>
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
