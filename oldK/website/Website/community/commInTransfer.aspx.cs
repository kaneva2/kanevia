///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva.channel;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for commInTransfer.
	/// </summary>
	public class commInTransfer : MainTemplatePage
	{
		protected commInTransfer () 
		{
			Title = "Transfer Community";
		}

		/// <summary>
		/// Page_Load
		/// </summary>
		private void Page_Load (object sender, System.EventArgs e)
		{
			_channelId = Convert.ToInt32 (Request ["communityId"]);

			// Set Nav
            HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB._UPDATE_;
			HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.CHANNELS;
			HeaderNav.MyChannelsNav.ChannelId = _channelId;
			HeaderNav.MyChannelsNav.ActiveTab = NavMyChannels.TAB.MEMBERS;
				
			HeaderNav.SetNavVisible(HeaderNav.MyKanevaNav,2);
			HeaderNav.SetNavVisible(HeaderNav.MyChannelsNav);
		}

		/// <summary>
		/// Update Click
		/// </summary>
		protected void btnUpdate_Click (object sender, EventArgs e) 
		{
			int userId = Convert.ToInt32 (Request ["userId"]);
			DataRow drChannel = CommunityUtility.GetCommunity (_channelId);

			// Now only Site Admins can transfer http://qa4/mantis/view.php?id=3793
			//if (CommunityUtility.IsCommunityOwner (_channelId, GetUserId ()))
			if (IsAdministrator ())
			{
				// Update the ownership
				if (CommunityUtility.TransferChannel (_channelId, GetUserId (), userId, Convert.ToInt32 (drChannel ["creator_id"])))
				{
					// Send private message to new owner
					string channelName = drChannel ["name"].ToString ();
                    string strMessage = "<b>" + KanevaWebGlobals.CurrentUser.Username + " </b> has decided to turn the ownership of <b>" + channelName + "</b> community over to you. " +
						" Congratulations. You can go to this community by clicking here <a style='color: green;' href=" + KanevaGlobals.GetBroadcastChannelUrl (drChannel ["name_no_spaces"].ToString ()) + ">" + channelName + "</a>";

                    // Insert a private message
                    UserFacade userFacade = new UserFacade();
                    Message message = new Message(0, GetUserId(), userId, "Transfer of community ownership - " + channelName,
                        strMessage, new DateTime(), 0, (int)Constants.eMESSAGE_TYPE.ALERT, 0, "U", "S");

                    userFacade.InsertMessage(message);
				}
			}
				
			Response.Redirect ("~/community/members.aspx?communityId=" + Convert.ToInt32 (Request ["communityId"]));
		}

		/// <summary>
		/// Cancel Click
		/// </summary>
		protected void btnCancel_Click (object sender, EventArgs e) 
		{
			Response.Redirect ("~/community/members.aspx?communityId=" + Convert.ToInt32 (Request ["communityId"]));
		}

		protected RadioButtonList rblNotifications;
		protected CheckBox chkItemNotify, chkBlogNotify, chkPostNotify, chkItemReviewNotify, chkBlogCommentsNotify, chkReplyNotify;

		protected HtmlTable tblNotValidated;

		protected Label lblEmail;

		private int _channelId;

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
