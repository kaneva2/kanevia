///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using log4net;
using System.IO;

namespace KlausEnt.KEP.Kaneva.community
{
    public partial class KanevaCommunity : BasePage
    {
        #region Declarations
        string metaData = "";
        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            // Bring in the content
            if (Request.QueryString["communitypath"] != null)
            {
                litContent.Text = readFile("../community/kaneva/" + Request.QueryString["communitypath"]);
                litHeader.Text = metaData;
            }
            else
            {
                //litContent.Text = readFile("../3d-virtual-world/content-files/whats-new.html");
            }
        }
        #endregion

        #region Functions
        private String readFile(string path)
        {
            string title = "";
            string metaDataTitle = "";
            string metaDataDescription = "";
            string metaDataKeywords = "";
            string javaScript = "";
            string styleSheets = "";

            string pageContent = "";
            //string pageContent = (string)Global.Cache()[path];
            if (pageContent == null || pageContent.Length.Equals(0))
            {
                try
                {
                    String result;
                    StreamReader sr = File.OpenText(Server.MapPath(path));
                    {
                        title = sr.ReadLine();
                        ((GenericLandingPageTemplate)Master).Title = title.Substring(title.IndexOf(":") + 1).Trim();

                        metaDataTitle = sr.ReadLine();
                        metaData += metaDataTitle.Substring(metaDataTitle.IndexOf(":") + 1).Trim() + "\n\r";

                        metaDataDescription = sr.ReadLine();
                        metaData += metaDataDescription.Substring(metaDataDescription.IndexOf(":") + 1).Trim() + "\n\r";

                        metaDataKeywords = sr.ReadLine();
                        metaData += metaDataKeywords.Substring(metaDataKeywords.IndexOf(":") + 1).Trim() + "\n\r";

                        javaScript = sr.ReadLine();
                        metaData += javaScript.Substring(javaScript.IndexOf(":") + 1).Trim() + "\n\r";

                        styleSheets = sr.ReadLine();
                        metaData += styleSheets.Substring(styleSheets.IndexOf(":") + 1).Trim() + "\n\r";

                        sr.ReadLine();

                        result = sr.ReadToEnd();
                        // Close and clean up the StreamReader
                        sr.Close();
                    }
                    pageContent = result;
                }
                catch (Exception exc)
                {
                    m_logger.Error("Error retrieving file, fname =" + path, exc);
                    pageContent = "";
                }
                // Add to the cache                
                Global.Cache().Insert(path, pageContent, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
            }
            return pageContent;
        }


        #endregion

        #region Web Form Designer generated code

        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new EventHandler(this.Page_Load);
        }

        #endregion

    }
}
