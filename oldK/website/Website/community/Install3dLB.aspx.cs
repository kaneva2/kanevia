///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Kaneva.community
{
    public partial class Install3dLB : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // If not a windows box, go directly to mykaneva page
            if (!Request.Browser.Platform.ToLower().Contains("win"))
            {
                Response.Redirect(ResolveUrl("~/default.aspx"));
            }

            if (!IsPostBack)
            {
                // Browser specific image
                if (Request.Browser.Browser.ToLower().Contains("firefox"))
                {
                    imgDownload.Src = ResolveUrl("~/images/download/firefox.png");
                }
                else if (Request.Browser.Browser.ToLower().Contains("chrome"))
                {
                    imgDownload.Src = ResolveUrl("~/images/download/chrome.png");
                }

                // Start the client download.
                if (!Page.ClientScript.IsClientScriptBlockRegistered(GetType(), "downloadstartup"))
                {
                    string javascript = "<script type=\"text/javascript\">function show() { location.href = '" + KanevaSetupInstallURL() + "';} window.onload = show;</script>";
                    Page.ClientScript.RegisterStartupScript(GetType(), "downloadstartup", javascript);
                }
            }

            // Set Nav
            ((GenericPageTemplate)Master).HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
            ((GenericPageTemplate)Master).HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.NONE;
            ((GenericPageTemplate)Master).HeaderNav.SetNavVisible(((GenericPageTemplate)Master).HeaderNav.MyKanevaNav);
        }

        #region Declerations

        protected HtmlImage imgDownload;

        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}
