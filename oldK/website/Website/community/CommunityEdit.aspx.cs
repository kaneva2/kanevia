///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using log4net;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using System.Collections.Generic;

namespace KlausEnt.KEP.Kaneva
{
    public partial class CommunityEdit : BasePage
    {
        #region Declarations
        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private Community currentCommunity = null;
        #endregion


        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            

            // They must be logged in
            if (Request.IsAuthenticated)
            {
                //get the parameters 
                GetRequestParams();



                // Users should not get to this page any longer.  It has been replaced with new edit page
                Response.Redirect ("~/community/3dapps/AppManagement.aspx?communityid=" + COMMUNITY_ID);



                //configure page security
                ConfigurePageSecurity();

                //send all unauthorized people away otherwise continue processing the page
                if ((COMMUNITY_ID > 0) && !IS_ALLOWED_TO_EDIT)
                {
                    Response.Redirect("~/error.aspx?error=" + (int)Constants.eERROR_TYPE.ACCESS_DENIED, true);
                }
                else
                {
                    //set the navigation
                    ((GenericPageTemplate)Master).HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
                    ((GenericPageTemplate)Master).HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB._UPDATE_;
                    ((GenericPageTemplate)Master).HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.CHANNELS;
                    ((GenericPageTemplate)Master).HeaderNav.SetNavVisible(((GenericPageTemplate)Master).HeaderNav.MyKanevaNav, 2);

                    //do only on page load not post back
                    if (!IsPostBack)
                    {
                        //bind the drop downs
                        BindCommunityCategories();
                        BindCommunityPlaceTypes();
                        BindCommunityStatus();

                        //attach all needed javascript
                        ConfigureJavaScript();

                        // Set Community type name
                        SetCommunityTypeText ();

                        //if an existing community load the data
                        if (COMMUNITY_ID > 0)
                        {
                            //set the navigation
                            ((GenericPageTemplate)Master).HeaderNav.MyChannelsNav.ChannelId = this.COMMUNITY_ID;
                            //((GenericPageTemplate)Master).HeaderNav.MyChannelsNav.PageId = this.COMMUNITY_PAGE_ID;
                            
                            //get and set the data
                            GetCommunityData();

                            // Load the tab configuration
                            if (COMMUNITY.CommunityTypeId == (int) CommunityType.APP_3D)
                            {
                                txtTitle.Enabled = false;
                                GetTabData ();
                            }
                        }
                        else
                        {
                            // Don't show the cancel 
                            btnCancel.Visible = false;
                            btnUpdate.Visible = false;
                            btnLaunch.Visible = true;

                            //if no image no need to clear it
                            btnClearThumb.Enabled = false;

                            //set this to configure the URL area for new communities
                            InitURLData(null);
                        }

                        //set the page to go back to
                        SetPreviousPage();

                    }
                    // Admins are allowed to set over 21 status
                    over21Area.Visible = IS_ADMINISTRATOR && this.COMMUNITY.CommunityTypeId.Equals((int)CommunityType.COMMUNITY);
                    restrictedArea.Visible = IsCurrentUserAdult();
                    divCommunityOnly.Visible = !(this.COMMUNITY.CommunityTypeId.Equals((int)CommunityType.APP_3D));

                    //set the selected value field for the background selection control
                    libraryImageSelector.ReturnField = this.txtBGPicture;            

                    //set validation values
                    revTitle.ValidationExpression = Constants.VALIDATION_REGEX_CHANNEL_NAME;
                    revTitle.ErrorMessage = "Title " + Constants.VALIDATION_REGEX_CHANNEL_NAME_ERROR_MESSAGE;
                    regtxtKeywords.ValidationExpression = Constants.VALIDATION_REGEX_TAG;
                    regtxtKeywords.ErrorMessage = Constants.VALIDATION_REGEX_TAG_ERROR_MESSAGE;
                }
            }
            else
            {
                Response.Redirect(GetLoginURL());
            }
        }
        #endregion

        #region Main Functions

        private void GetCommunityData()
        {
            //set the category of the community
            //drpCategory.SelectedValue = (this.COMMUNITY.CategoryId > 2 ? this.COMMUNITY.CategoryId.ToString() : "0");

            //set the plave type of the community
            drpPlace.SelectedValue = (this.COMMUNITY.PlaceTypeId >= 0 ? this.COMMUNITY.PlaceTypeId.ToString() : "0");

            txtTags.Text = this.COMMUNITY.Keywords;
            txtTitle.Text = Server.HtmlDecode(this.COMMUNITY.Name);
            txtDescription.Text = Server.HtmlDecode(this.COMMUNITY.Description);

            // "Unknown" is most likely internal for a non admin
            drpStatus.SelectedValue = this.COMMUNITY.IsPublic.ToString();

            txtBackgroundColor.Text = this.COMMUNITY.BackgroundRGB;
            txtBGPicture.Text = this.COMMUNITY.BackgroundImage;

            // Content types
            cbRestricted.Checked = this.COMMUNITY.IsAdult.ToString().ToUpper().Equals("Y");
            cbOver21.Checked = this.COMMUNITY.Over21Required;

            chkAllowPublish.Checked = (this.COMMUNITY.AllowPublishing).Equals(1);

            imgThumbnail.Src = GetBroadcastChannelImageURL(this.COMMUNITY.ThumbnailLargePath, "la");

            //if no image no need to clear it
            btnClearThumb.Enabled = this.COMMUNITY.HasThumbnail;

            //set up the community URL
            InitURLData(this.COMMUNITY.Url);

            CommunityTypeName = this.COMMUNITY.WOK3App.GameId > 0 ? "World" : "Community";
        }

        private bool IsURLAvailable()
        {
            bool isURLAvailable = false;

            if ((COMMUNITY.Url == null || COMMUNITY.Url == "") && txtURL.Visible)
            {
                //get and uncode url
                string url = txtURL.Text.Trim();

                if ((url != null) && (url != ""))
                {
                    url = Server.HtmlEncode(url);

                    //check to see if the URL is in violation of allowed words, or if it is already in use.
                    //if it is ok add for save to database, otherwise display error message
                    if (KanevaWebGlobals.isTextRestricted(url, Constants.eRESTRICTION_TYPE.POTTY_MOUTH))
                    {
                        alertMsg.InnerText = Constants.VALIDATION_REGEX_RESTRICTED_WORD_MESSAGE;
                    }
                    else if (KanevaWebGlobals.isTextRestricted(url, Constants.eRESTRICTION_TYPE.RESERVED))
                    {
                        alertMsg.InnerText = Constants.VALIDATION_REGEX_RESERVED_WORD_MESSAGE;
                    }
                    else if (GetCommunityFacade.IsCommunityURLTaken(url, COMMUNITY_ID))
                    {
                        alertMsg.InnerText = "The URL you have selected is not available. Please Choose another";
                    }
                    else
                    {
                        //update URL
                        COMMUNITY.Url = url;
                        isURLAvailable = true;
                    }
                }
                //no URL was provided
                else
                {
                    isURLAvailable = true;
                }
            }
            else //the URL has been set before so set this to true to keep processing
            {
                isURLAvailable = true;
            }

            return isURLAvailable;
        }

        private bool UpdateChannel()
        {
            bool successful = false;
            int result = 0;

            if (Page.IsValid)
            {
                // Check to make sure user did not enter any "potty mouth" words
                if (KanevaWebGlobals.isTextRestricted(txtTitle.Text, Constants.eRESTRICTION_TYPE.POTTY_MOUTH) ||
                    KanevaWebGlobals.isTextRestricted(txtDescription.Text, Constants.eRESTRICTION_TYPE.POTTY_MOUTH) ||
                    KanevaWebGlobals.isTextRestricted(txtTags.Text, Constants.eRESTRICTION_TYPE.POTTY_MOUTH) ||
                    KanevaWebGlobals.isTextRestricted(txtURL.Text, Constants.eRESTRICTION_TYPE.ALL))
                {
                    alertMsg.InnerText = Constants.VALIDATION_REGEX_RESTRICTED_WORD_MESSAGE;
                }
                else if (KanevaWebGlobals.ContainsInjectScripts(txtBackgroundColor.Text) ||
                    KanevaWebGlobals.ContainsInjectScripts(txtBGPicture.Text) ||
                    KanevaWebGlobals.ContainsInjectScripts(txtTitle.Text) ||
                    KanevaWebGlobals.ContainsInjectScripts(txtDescription.Text) ||
                    KanevaWebGlobals.ContainsInjectScripts(txtTags.Text) ||
                    KanevaWebGlobals.ContainsInjectScripts(txtURL.Text))
                {
                    m_logger.Warn("User " + KanevaWebGlobals.GetUserId() + " tried to script from IP " + Common.GetVisitorIPAddress());
                    alertMsg.InnerText = Constants.VALIDATION_REGEX_RESTRICTED_WORD_MESSAGE;
                }
                else
                {
                    //prep the URL for saving IF provided and new
                    if (IsURLAvailable())
                    {
                        //set the values entered by user
                        //community.AllowMemberEvents;
                        COMMUNITY.AllowPublishing = ((chkAllowPublish.Checked) ? 1 : 0);
                        COMMUNITY.BackgroundImage = Server.HtmlEncode(txtBGPicture.Text);
                        COMMUNITY.BackgroundRGB = Server.HtmlEncode(txtBackgroundColor.Text);
                        
                        COMMUNITY.CommunityId = COMMUNITY_ID;
                        COMMUNITY.Description = Server.HtmlEncode(txtDescription.Text);
                        COMMUNITY.IsAdult = Server.HtmlEncode(cbRestricted.Checked ? "Y" : "N");
                        COMMUNITY.IsPublic = Server.HtmlEncode(drpStatus.SelectedValue);
                        
                        COMMUNITY.Name = Server.HtmlEncode(txtTitle.Text.Trim());
                        COMMUNITY.NameNoSpaces = COMMUNITY.Name.Replace(" ", "");
                        COMMUNITY.Over21Required = cbOver21.Checked;
                        
                        COMMUNITY.IsPersonal = 0; //0 is false
                        COMMUNITY.StatusId = (int)CommunityStatus.ACTIVE;

                        if (!(this.COMMUNITY.CommunityTypeId.Equals((int)CommunityType.APP_3D)))
                        {
                            //COMMUNITY.CategoryId = Convert.ToInt32(drpCategory.SelectedValue);
                            COMMUNITY.Keywords = Server.HtmlEncode(FilterChannelTags(txtTags.Text));
                            COMMUNITY.PlaceTypeId = Convert.ToInt32(drpPlace.SelectedValue);
                        }

                        //check to see if name is already taken
                        if (!GetCommunityFacade.IsCommunityNameTaken((COMMUNITY.IsPersonal == 1), COMMUNITY.NameNoSpaces, COMMUNITY_ID))
                        {
                            if (COMMUNITY_ID > 0)
                            {
                                // Update the community - in this case result is #rows affected
                                result = GetCommunityFacade.UpdateCommunity(COMMUNITY);

                                if (result <= 0)
                                {
                                    alertMsg.InnerText = "Unable to save your changes.";
                                }
                            }
                            else
                            {
                                //properties to set only on creation
                                COMMUNITY.CreatorUsername = KanevaWebGlobals.CurrentUser.Username;
                                COMMUNITY.Email = KanevaWebGlobals.CurrentUser.Email;
                                COMMUNITY.CreatorId = CURRENT_USER_ID;
                                COMMUNITY.CommunityTypeId = (int)CommunityType.COMMUNITY;

                                //check to make sure they haven't exceeded their allots community count
                                int numberOfActiveCommunities = GetCommunityFacade.GetNumberOfCommunities(COMMUNITY.CreatorId, (int)CommunityStatus.ACTIVE);

                                if ((numberOfActiveCommunities > KanevaGlobals.MaxNumberOfCommunitiesPerUser) && !this.IS_ADMINISTRATOR)
                                {
                                    alertMsg.InnerText = "You are only allowed to create " + KanevaGlobals.MaxNumberOfCommunitiesPerUser + " active communities.";
                                }
                                else
                                {
                                    //insert new community - in this case result is community id
                                    COMMUNITY_ID = COMMUNITY.CommunityId = result = GetCommunityFacade.InsertCommunity(COMMUNITY);

                                    if (result > 0)
                                    {
                                        //add the creator to the community
                                        CommunityMember member = new CommunityMember();
                                        member.CommunityId = COMMUNITY.CommunityId;
                                        member.UserId = this.CURRENT_USER_ID;
                                        member.AccountTypeId = (int)CommunityMember.CommunityMemberAccountType.OWNER;
                                        member.Newsletter = "Y";
                                        member.InvitedByUserId = 0;
                                        member.AllowAssetUploads = "Y";
                                        member.AllowForumUse = "Y";
                                        member.StatusId = (int)CommunityMember.CommunityMemberStatus.ACTIVE;

                                        GetCommunityFacade.InsertCommunityMember(member);

                                        // Add the forums
                                        // Add a default General forum to every community
                                        int forumCategory = GetForumFacade.InsertForumCategory(COMMUNITY_ID, "General", "General");
                                        GetForumFacade.InsertForum(COMMUNITY_ID, "General", "General", forumCategory);
                                    }
                                    else
                                    {
                                        alertMsg.InnerText = "Unable to save your changes.";
                                    }
                                }
                            }

                            // Save the tabs to display
                            if (COMMUNITY.CommunityId > 0 && COMMUNITY.CommunityTypeId == (int) CommunityType.APP_3D)
                            {
                                foreach (ListItem li in cblTabs.Items)
                                {
                                    GetCommunityFacade.UpdateCommunityTabs (Convert.ToInt32 (li.Value),
                                        COMMUNITY.CommunityId, COMMUNITY.CommunityTypeId, li.Selected,
                                        Convert.ToInt32 (rblDefaultTab.SelectedValue) == Convert.ToInt32 (li.Value) ? true : false);
                                }
                            }

                            //if community update succeeded perform remaining operations
                            if (result > 0)
                            {
                                string imageResults = null;

                                // update the thumbnail
                                if ((inpThumbnail.Value != null) && (inpThumbnail.Value != ""))
                                {
                                    alertMsg.InnerText = imageResults = UploadImage(this.COMMUNITY_ID, this.inpThumbnail, this.CURRENT_USER_ID, null);
                                }

                                if (imageResults == null)
                                {
                                    successful = true;
                                }
                            }
                        }
                        else
                        {
                            alertMsg.InnerText = "Community " + txtTitle.Text + " already exists, please change the title.";
                        }
                    }
                }
            }
            return successful;
        }

        private void GetTabData ()
        {
            IList<CommunityTab> tabs = GetCommunityFacade.GetCommunityTabs (COMMUNITY.CommunityId, COMMUNITY.CommunityTypeId);
            //IList<CommunityTab> tabs = GetCommunityFacade.GetCommunityTabsByCommunityType (COMMUNITY.CommunityTypeId);

            // Do this check because communities that have not been configured yet will not have any rows
            // in the community_tabs table, so by default we show all the tabs until user changes config.
            bool selectAll = true;
            foreach (CommunityTab ct in tabs)
            {
                if (ct.CommunityId > 0)
                {
                    selectAll = false;
                    break;
                }
            }

            foreach (CommunityTab ct in tabs)
            {
                ListItem li = new ListItem (ct.TabName, ct.TabId.ToString ());
                li.Enabled = ct.IsConfigurable;
                if (ct.CommunityId > 0 || selectAll) li.Selected = true;
                if (!ct.IsConfigurable) li.Selected = true;
                cblTabs.Items.Add (li);

                // Default Tab radio buttons
                if (ct.TabId == (int) Constants.eTab.About)
                {
                    // Add the About selection
                    ListItem liAbout = new ListItem (Constants.TabType.ABOUT, ((int) Constants.eTab.About).ToString ());
                    liAbout.Enabled = false;
                    liAbout.Selected = false;
                    li.Attributes.Add ("onclick", "ConfigDefaultTab(this);");

                    // Add the Blast selection
                    ListItem liBlast = new ListItem (Constants.TabType.BLAST, ((int) Constants.eTab.Blast).ToString ());
                    liBlast.Enabled = true;

                    if (ct.CommunityId > 0 || selectAll)
                    {
                        liAbout.Enabled = true;
                        if (ct.IsDefault)
                        {
                            liAbout.Selected = true;
                        }
                        else
                        {
                            liBlast.Selected = true;
                        }
                    }
                    else
                    {
                        liBlast.Selected = true;
                    }

                    rblDefaultTab.Items.Add (liAbout);
                    rblDefaultTab.Items.Add (liBlast);
                }
            }

            // Set Tab section visible
            divTabContainer.Visible = true;
        }

        public void SetCommunityTypeText ()
        {
            string typeName = "community";
            string typeNameCap = "Community";
            if (this.COMMUNITY.CommunityTypeId.Equals ((int) CommunityType.APP_3D))
            {
                typeName = "World";
                typeNameCap = "World";
                litPhotoText.Text = "Thumbnail of Your World";
            }

            litCommType1.Text = litCommType9.Text = typeNameCap;
            litCommType2.Text = litCommType3.Text = litCommType4.Text =
                litCommType5.Text = litCommType6.Text = litCommType7.Text = typeName;
        }
        
        #endregion

        #region Attributes
        private int COMMUNITY_ID
        {
            set
            {
                ViewState["communityId"] = value;
            }
            get
            {
                if (ViewState["communityId"] == null)
                {
                    ViewState["communityId"] = -1;
                }
                return (int)ViewState["communityId"];
            }
        }

        private int CURRENT_USER_ID
        {
            set
            {
                ViewState["currentUserId"] = value;
            }
            get
            {
                if (ViewState["currentUserId"] == null)
                {
                    ViewState["currentUserId"] = GetUserId();
                }
                return (int)ViewState["currentUserId"];
            }
        }

        private bool IS_COMMUNITY_OWNER
        {
            set
            {
                ViewState["isCommunityOwner"] = value;
            }
            get
            {
                if (ViewState["isCommunityOwner"] == null)
                {
                    ViewState["isCommunityOwner"] = false;
                }
                return (bool)ViewState["isCommunityOwner"];
            }
        }

        private bool IS_ALLOWED_TO_EDIT
        {
            set
            {
                ViewState["isAllowedToEdit"] = value;
            }
            get
            {
                if (ViewState["isAllowedToEdit"] == null)
                {
                    ViewState["isAllowedToEdit"] = false;
                }
                return (bool)ViewState["isAllowedToEdit"];
            }
        }

        private bool IS_ADMINISTRATOR
        {
            set
            {
                ViewState["isAdministrator"] = value;
            }
            get
            {
                if (ViewState["isAdministrator"] == null)
                {
                    ViewState["isAdministrator"] = false;
                }
                return (bool)ViewState["isAdministrator"];
            }
        }

        private string RETURN_URL
        {
            set
            {
                ViewState["returnURL"] = value;
            }
            get
            {
                if (ViewState["returnURL"] == null)
                {
                    ViewState["returnURL"] = "";
                }
                return ViewState["returnURL"].ToString();
            }
        }

        private Community COMMUNITY
        {
            set
            {
                this.currentCommunity = value;
            }
            get
            {
                if (this.currentCommunity == null)
                {
                    this.currentCommunity = GetCommunityFacade.GetCommunity(this.COMMUNITY_ID);
                }
                return this.currentCommunity;
            }
        }

        private string CommunityTypeName
        {
            set
            {
                ViewState["commTypeName"] = value;
            }
            get
            {
                if (ViewState["commTypeName"] == null)
                {
                    ViewState["commTypeName"] = "Community";
                }
                return ViewState["commTypeName"].ToString ();
            }
        }

        #endregion

        #region Helper Functions

        private void ResetErrorMessages()
        {
            alertMsg.Visible = false;
            spItemNameMsg.InnerHtml = "";
        }

        private void ConfigureJavaScript()
        {
            //add clear function to button
            aRemove.Attributes.Add("onclick", "ClearField('" + txtBGPicture.ClientID + "')");
            
            //add onchange event to the color picker
            txtBackgroundColor.Attributes.Add("onchange", "relateColor('" + txtBackgroundColorAppClrPickerStr.ClientID + "',this.value);");
            txtBackgroundColorAppClrPickerStr.NavigateUrl = "javascript:pickColorMP('" + txtBackgroundColorAppClrPickerStr.ClientID + "');";
            if (!ClientScript.IsClientScriptBlockRegistered(GetType(), "setbgcolor"))
            {
                ClientScript.RegisterStartupScript(GetType(), "setbgcolor", "<script>relateColor('" +
                    txtBackgroundColorAppClrPickerStr.ClientID + "',$('" + txtBackgroundColor.ClientID + "').value);</script>");
            }
        }

        private void SetPreviousPage()
        {
            string lastPage = null;
            try
            {
                lastPage = Request.UrlReferrer.AbsoluteUri.ToString();
            }
            catch (NullReferenceException)
            {
            }

            if (lastPage != null)
            {
                RETURN_URL = lastPage;
            }
            else if (COMMUNITY_ID == 0)
            {
                RETURN_URL = ResolveUrl("~/bookmarks/channelTags.aspx");
            }
            else
            {
                RETURN_URL = GetBroadcastChannelUrl(COMMUNITY.NameNoSpaces);
            }

            ////set the page bread crumb
            //AddBreadCrumb(new BreadCrumb("Start/Edit Community", GetCurrentURL(), "", 0));
        }

        private void BindCommunityStatus()
        {
            drpStatus.Items.Add(new ListItem("Public", "Y"));
            drpStatus.Items.Add(new ListItem("Members Only", "N"));
            if (IS_ADMINISTRATOR)
            {
                drpStatus.Items.Add(new ListItem("Internal", "I"));
            }
        }

        private void BindCommunityCategories()
        {
            //Bind category dropdownlist
            drpCategory.DataSource = GetCommunityFacade.GetCommunityCategories();
            drpCategory.DataTextField = "CategoryName";
            drpCategory.DataValueField = "CategoryId";

            drpCategory.DataBind();

            drpCategory.Items.Insert(0, new ListItem("Select...", ""));
        }

        private void BindCommunityPlaceTypes()
        {
            //Bind place type dropdownlist
            drpPlace.DataSource = GetCommunityFacade.GetCommunityPlaceTypes();
            drpPlace.DataTextField = "Name";
            drpPlace.DataValueField = "PlaceTypeId";

            drpPlace.DataBind();

            drpPlace.Items.Insert(0, new ListItem("Select...", ""));
        }

        private void GetRequestParams()
        {
            //check to see if there is a name for the game (required)
            try
            {
                COMMUNITY_ID = Convert.ToInt32(Request["communityId"].ToString());
            }
            catch (Exception ex)
            {
                m_logger.Error("Community id was not provided ", ex);
            }

            try
            {
                RETURN_URL = Request["returnURL"].ToString();
            }
            catch (Exception)
            {
            }
        }

        private void InitURLData(string communityURL)
        {
            if ((communityURL != null) && (communityURL != ""))
            {
                lblURL.Style.Add("font-weight", "bold");
                lblURL.Text = "http://" + KanevaGlobals.SiteName + "/" + communityURL;
                txtURL.Visible = false;
                revURL.Enabled = false;
                spnRegistered.Visible = true;
                urlWarning.Visible = false;
            }
            else
            {
                // Set up regular expression validators
                revURL.ValidationExpression = Constants.VALIDATION_REGEX_USERNAME;
                lblURL.Text = "http://" + KanevaGlobals.SiteName + "/";
                urlWarning.Visible = true;
            }
        }

        private void ConfigurePageSecurity()
        {
            //check to see if they are the owner
            IS_COMMUNITY_OWNER = GetCommunityFacade.IsCommunityOwner(COMMUNITY_ID, CURRENT_USER_ID);
            
            //check security level
            switch (KanevaWebGlobals.CheckUserAccess((int)SitePrivilege.ePRIVILEGE.COMMUNITY_PROFILE_ADMIN))
            {
                case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_FULL:
                    IS_ADMINISTRATOR = true;
                    break;
                case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_READ:
                    break;
                case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_NONE:
                    break;
                default:
                    break;
            }
            //set editing right
            if (IS_ADMINISTRATOR || IS_COMMUNITY_OWNER)
            {                    
                IS_ALLOWED_TO_EDIT = true;
            }
        }
        #endregion

        #region Event Handlers
        /// <summary>
        /// btnClearThumb_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnClearThumb_Click(object sender, EventArgs e)
        {
            GetCommunityFacade.DeleteCommunityThumbs(COMMUNITY_ID);
            GetCommunityData();
        }

        /// <summary>
        /// Cancel Add/Update a community
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(RETURN_URL);
        }

        /// <summary>
        /// Add/Update a community
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            ResetErrorMessages();

            if (UpdateChannel())
            {
                Response.Redirect(RETURN_URL);
            }
            else
            {
                alertMsg.Visible = true;
            }
        }

        /// <summary>
        /// Add/Update image
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnNew_Click(object sender, ImageClickEventArgs e)
        {
            ResetErrorMessages();

            if (UpdateChannel())
            {
                // Show viral stuff
                Response.Redirect("~/community/viral/CommunityViral.aspx?communityId=" + this.COMMUNITY_ID);
            }
            else
            {
                alertMsg.Visible = true;
            }
        }

        /// <summary>
        /// show and intialize library image selection control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void selectPhoto_Click(object sender, EventArgs e)
        {
            libraryImageSelector.Visible = true;
        }

        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion

    }
}
