﻿<%@ Page Language="C#" MasterPageFile="~/masterpages/GenericPageTemplate.Master" AutoEventWireup="true" CodeBehind="Install3dWorld.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.community.Install3dWorld" %>

<asp:Content ID="cntProfileHead" runat="server" contentplaceholderid="cph_HeadData" >

	<script type="text/javascript" src="../jscript/base/base.js"></script>
    <NOSCRIPT><META HTTP-EQUIV="refresh" CONTENT="1" URL="noJavascript.aspx" /></NOSCRIPT>
    <link href="css/myKaneva_NEW.css" type="text/css" rel="stylesheet" />

    <style>

    body {
	    background-image: none;
	    }
	    a:hover{
	    background-color:white;
    }
    .PageContainer {background-color:#fff;width:1004px;height:100%;border-top:solid 1px #fff;}
    </style>
</asp:Content>

<asp:Content id="cntBody" runat="server" contentplaceholderid="cph_Body" >



<div class="content" style="height:800px;text-align:center;padding-top:10px;background-color:#fff;" id="divContent">    

    <div style="background-color:white;"> 
            <a id="aDownload" runat="server" href="~/community/Install3dLB.aspx">
            <img id="Img1" runat="server" BORDER=0 src="~/images/download/download_internal.png" />
            </a>

            <hr style="background-color:#dbe5e6;color:#dbe5e6;margin:40px 0;" />
    </div>


    <div style"background-color: #F6f6f6;">
        <div style="padding-top:15px;"></div>

        <img id="imgDownload" runat="server" src="~/images/download/explorer.png" />
    </div>

</div>

<style>
	/* Temp hack to remove white background if validate message is displaying in header */	
.nav_wrapper .validate {background:transparent;border:none;}
    
</style>
</asp:Content>

