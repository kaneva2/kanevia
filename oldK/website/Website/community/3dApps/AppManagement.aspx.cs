///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using log4net;
using Kaneva.BusinessLayer.BusinessObjects;
using System.Collections.Generic;
using Kaneva.BusinessLayer.Facade;
using SiteManagement;
using SiteManagement.usercontrols;

namespace KlausEnt.KEP.Kaneva
{
    public partial class AppManagement : BasePage
    {
        #region Declarations
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private bool canEdit = false;
        private bool isProfileAdmin = false;
        #endregion

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            Messages.Visible = false;
            Messages.InnerText = "";

            if (Request.IsAuthenticated)
            {

                if (!Page.IsPostBack)
                {
                    //gets the menu items based on URL parameters
                    GetRequestParams();

                    //get community related information
                    GetCommunityInfo();

                    //check the users access to this page
                    CheckUserAccess();

                    if (isProfileAdmin || this.canEdit)
                    {
                        //loads all the user's communities into a pull down
                        LoadUserCommunities();

                        //loads and configures the side menu
                        LoadNavigation();
                    }
                    else
                    {
                        Response.Redirect("~/error.aspx?error=" + (int)Constants.eERROR_TYPE.ACCESS_DENIED, true);
                    }
                }

                //set the navigation
                ((GenericPageTemplate) Master).HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
                ((GenericPageTemplate) Master).HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB._UPDATE_;
                ((GenericPageTemplate) Master).HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.MY3DAPPS;
                ((GenericPageTemplate) Master).HeaderNav.SetNavVisible (((GenericPageTemplate) Master).HeaderNav.MyKanevaNav, 2);
                ((GenericPageTemplate) Master).HeaderNav.MyChannelsNav.ChannelId = this.CommunityId;
                
                //loads the selected user control
                LoadSelectedControl();
            }
            else
            {
                Response.Redirect(this.GetLoginURL());
            }
        }
        #endregion

        #region Functions

        private void LoadUserCommunities()
        {
            PagedList<Community> userCommunities = new PagedList<Community>();
            Community userSelect = new Community();
            userSelect.CommunityId = 0;
            userSelect.Name = "-- SELECT --";
            userCommunities.Add(userSelect);

            if (OwnerId > 0)
            {
                //this will get games owned by the user's company
                userCommunities = GetCommunityFacade.GetUserCommunities(OwnerId, new int[] { (int)CommunityType.APP_3D, (int)CommunityType.COMMUNITY, (int)CommunityType.HOME }, "c.name", "place_type_id<99", 1, Int32.MaxValue);
            }
            userCommunities.Insert(0, userSelect);

            availableCommunities.DataSource = userCommunities;
            availableCommunities.DataTextField = "Name";
            availableCommunities.DataValueField = "CommunityId";
            availableCommunities.DataBind();

            try
            {
                availableCommunities.SelectedValue = CommunityId.ToString();
            }
            catch (Exception)
            {
                availableCommunities.SelectedIndex = 0;
            }
        }

        private void LoadNavigation()
        {
            try
            {
                // setup Go 3D link
                Community community = GetCommunityFacade.GetCommunity (CommunityId);
                ConfigureCommunityMeetMe3D (btnMeet3D, community.WOK3App.GameId, community.CommunityId, community.Name, ""); 

                // 3dapp specific links
                if (community.WOK3App.GameId > 0)
                {
                    lbBadges.Visible = true;
                    lbLeaderboard.Visible = true;
                }
                else
                {
                    lbBadges.Visible = false;
                    lbLeaderboard.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Messages.Visible = true;
                Messages.InnerText = "Error While Loading Menu: " + ex.Message;
                m_logger.Error("Error loading world management center. ", ex);
            }
        }

        private void LoadSelectedControl()
        {
            //load usercontrol
            try
            {
                if (!string.IsNullOrWhiteSpace(ActiveControl))
                {
                    //make sure control can access communityId
                    Session.Remove("AppManagementSelectedCommunityId");
                    Session.Add("AppManagementSelectedCommunityId", CommunityId);

                    //make usercontrol visible
                    ucShell.Visible = true;

                    //load the control				
                    this.ucShell.Controls.Clear();
                    Control activeControl = Page.LoadControl("UserControls/usertools/" + ActiveControl);
                    activeControl.ID = ActiveControl.Replace(".", "_");
                    this.ucShell.Controls.Add(activeControl);

                    //register any necessary events
                    if (activeControl is KlausEnt.KEP.Kaneva.usercontrols.GameData)
                    {
                        ((KlausEnt.KEP.Kaneva.usercontrols.GameData)activeControl).GameDataRefresh += OnGameDataRefresh;
                    }
                }
                else
                {
                    // Load World Info by default
                    ClearSelectedMenu();
                    lbWorldInfo.Attributes.Add("class", "selected");
                    ActiveControl = "GameData.ascx";
                    LoadSelectedControl();
                }
            }
            catch (Exception ex)
            {
                Messages.Visible = true;
                Messages.InnerText = "Error While Loading Control: " + ex.Message;
                m_logger.Error("Error While Loading Control ", ex);
            }
        }
        
        #endregion

        #region Helper Functions

        private void ClearSelectedMenu()
        {
            foreach (Control control in ulSubMenu.Controls)
            {
                try
                {
                    ((LinkButton)control).Attributes.Add("class", "");
                }
                catch { }
            }
        }

        /// <summary>
        /// Get community info based on community id
        /// </summary>
        private void GetCommunityInfo()
        {
            try
            {
                Community sltdCommunity = GetCommunityFacade.GetCommunity(CommunityId);
                selectedGame.Text = sltdCommunity.Name;
                OwnerId = sltdCommunity.CreatorId;
            }
            catch (Exception ex)
            {
                m_logger.Error("Error retrieving World data.", ex);
            }
        }

        /// <summary>
        /// Checks the current user's access level
        /// </summary>
        private void CheckUserAccess()
        {
            //check the privilege level to see if they are an admin
            this.isProfileAdmin = HasWritePrivileges((int)SitePrivilege.ePRIVILEGE.STAR_ADMIN);

            if (!isProfileAdmin)
            {
                //get the user the game is associated with
                int ownerId = GetCommunityFacade.GetCommunity(CommunityId).CreatorId;
                canEdit = ownerId.Equals(KanevaWebGlobals.CurrentUser.UserId);
            }
        }

        //get all possible parameters
        private void GetRequestParams()
        {
            // Get the community Id
            if (Request["communityId"] != null)
            {
                try
                {
                    CommunityId = Convert.ToInt32(Request["communityId"]);
                }
                catch (Exception ex)
                {
                    m_logger.Error("Error loading parameters for world management center. ", ex);
                }
            }

            // Get the community Id
            if (Request["control"] != null)
            {
                try
                {
                    ActiveControl = Request["control"].ToString () + ".ascx";
                    lbTransactions.Attributes.Add ("class", "selected");
                }
                catch (Exception ex)
                {
                    m_logger.Error ("Error loading parameters for world management center. ", ex);
                }
            }

        }

        #endregion

        #region Attributes

        protected string ActiveControl
        {
            get
            {

                if (ViewState["activeControl"] == null)
                {
                    ViewState["activeControl"] = "";
                }

                return ViewState["activeControl"].ToString();
            }
            set
            {
                ViewState["activeControl"] = value;
            }
        }

        private int CommunityId
        {
            set
            {
                ViewState["communityId"] = value;
            }
            get
            {
                if (ViewState["communityId"] == null)
                {
                    ViewState["communityId"] = 0;
                }

                return Convert.ToInt32(ViewState["communityId"]);
            }
        }

        private int OwnerId //company id
        {
            set
            {
                ViewState["ownerId"] = value;
            }
            get
            {
                if (ViewState["ownerId"] == null)
                {
                    ViewState["ownerId"] = 0;
                }

                return Convert.ToInt32(ViewState["ownerId"]);
            }
        }

        #endregion

        #region Event Handlers

        protected void Nav_Command(Object sender, CommandEventArgs e)
        {
            //clear all previous styles
            ClearSelectedMenu();

            //first get a reference to the repeater item that sent the event
            LinkButton clickedButton = (LinkButton)sender;
            clickedButton.Attributes.Add("class", "selected");

            try
            {
                ActiveControl = e.CommandArgument.ToString();

                if (ActiveControl == "WorldProfilePage")
                {
                    Community comm = GetCommunityFacade.GetCommunity(CommunityId);
                    Response.Redirect(GetBroadcastChannelUrl(comm.NameNoSpaces));
                }
                else
                {
                    //tell the user control that the postback needs to be treated as an initial load
                    Session.Remove("AppManagementUCReload");
                    Session.Add("AppManagementUCReload", true);

                    //load the selected control
                    LoadSelectedControl();
                }
            }
            catch (FormatException fe)
            {
                Messages.Visible = true;
                Messages.InnerText = "Unable to load selected menu option.";
                m_logger.Error("Error during world control selection.", fe);
            }

        }

        protected void availableCommunities_SelectedIndexChanged (Object sender, EventArgs e)
        {
            try
            {
                CommunityId = Convert.ToInt32 (availableCommunities.SelectedValue);
                GetCommunityInfo();

                if (CommunityId > 0)
                {
                    Response.Redirect("AppManagement.aspx?communityid=" + CommunityId);
                }
                else
                {
                    Messages.Visible = true;
                    Messages.InnerText = "Please select a world.";
                }
            }
            catch (FormatException fe)
            {
                Messages.Visible = true;
                Messages.InnerText = "Unable to load selected world.";
                m_logger.Error ("Error during world selection.", fe);
            }
        }

        protected void OnGameDataRefresh(object sender, EventArgs e)
        {
            //gets the menu items based on URL parameters
            GetRequestParams();

            //get community related information
            GetCommunityInfo();

            //loads all the users games into a pull down
            LoadUserCommunities();

            //loads and configures the side menu
            LoadNavigation();
        }

        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion

    }
}
