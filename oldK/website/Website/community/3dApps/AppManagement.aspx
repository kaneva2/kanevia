<%@ Page Language="C#" ValidateRequest="false" MasterPageFile="~/masterpages/GenericPageTemplate.Master" AutoEventWireup="false" CodeBehind="AppManagement.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.AppManagement" %>

<asp:Content ID="cntCommunityMetaData" runat="server" contentplaceholderid="cph_HeadData" >
    <asp:Literal ID="litHeader" runat="server"></asp:Literal>
    <script type="text/javascript" src="../../jscript/prototype-1.6.1.js"></script>
    <script type="text/javascript" src="../../jscript/SWFObject/swfobject.js"></script>
    <script type="text/javascript" src="../../jscript/Communities/3DApps/AppManagement.js"></script>
    
    <NOSCRIPT><META HTTP-EQUIV="refresh" CONTENT="1" URL="noJavascript.aspx" /></NOSCRIPT>
    <link href="../../css/base/base_new.css" type="text/css" rel="stylesheet" />
    <link href="../../css/base/footer.css" type="text/css" rel="stylesheet" />
    <link href="../../css/Communities/3DApps/AppManagement.css" type="text/css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="cntJSBottom" runat="server" contentplaceholderid="cphBottomPageJS">


</asp:Content>

<asp:Content ID="cntKanevaCommunityBody" runat="server" contentplaceholderid="cph_Body" >
    <div class="ContentWrapper">
        <asp:Literal ID="litContent" runat="server"></asp:Literal>
	    <div id="messageArea">
            <div id="Messages" runat="server" class="errBox" visible="false"></div>
	    </div>
        <div id="pageHeader">
            <div id="gameInfo">
                <h6><asp:Label runat="server" id="selectedGame" CssClass="selectedGame" visible="true" /></h6>
            </div>
            <div id="gameSelect">
                <asp:DropDownList ID="availableCommunities" Width="200px" CausesValidation="false" AutoPostBack="true" OnSelectedIndexChanged="availableCommunities_SelectedIndexChanged" runat="server"></asp:DropDownList>
            </div>
        </div>
        <div id="sidenav">
            <ul id="ulSubMenu" runat="server">
                <li><asp:LinkButton id="lbWorldInfo" runat="server" OnCommand="Nav_Command" CausesValidation="false" CommandArgument='GameData.ascx' >World Info</asp:LinkButton></li>
                <li><asp:LinkButton id="lbWorldProfile" runat="server" OnCommand="Nav_Command" CausesValidation="false" CommandArgument='WorldProfilePage' >World Profile Page</asp:LinkButton></li>
                <li><asp:LinkButton id="lbTransactions" runat="server" OnCommand="Nav_Command" CausesValidation="false" CommandArgument='GameTransactions.ascx' >World Credit Balance</asp:LinkButton></li>
                <li><asp:LinkButton id="lbBadges" runat="server" OnCommand="Nav_Command" CausesValidation="false" CommandArgument='GameAchievements.ascx' >Badges</asp:LinkButton></li>
                <li><asp:LinkButton id="lbLeaderboard" runat="server" OnCommand="Nav_Command" CausesValidation="false" CommandArgument='GameLeaderboard.ascx' >Leaderboard</asp:LinkButton></li>
            </ul> 			
			<div><asp:linkbutton class="btnmedium blue" id="btnMeet3D" runat="server" CausesValidation="False" text="Play Now"></asp:linkbutton></div> 					
       </div>
        <div id="content">
            <asp:PlaceHolder runat="server" id="ucShell" Visible="false"></asp:PlaceHolder>
        </div>
    </div>

    <!-- Google Code for Dev World Web mgt Conversion Page -->
    <script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 1065938872;
    var google_conversion_language = "en";
    var google_conversion_format = "3";
    var google_conversion_color = "ffffff";
    var google_conversion_label = "hYOBCLi8mwIQuN-j_AM";
    var google_conversion_value = 0;
    /* ]]> */
    </script>
    <script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>
    <div style="display:inline;">
    <img height="1" width="1" style="border-style:none;" alt="" src="http://www.googleadservices.com/pagead/conversion/1065938872/?label=hYOBCLi8mwIQuN-j_AM&amp;guid=ON&amp;script=0"/>
    </div>
    </noscript>

</asp:Content>
