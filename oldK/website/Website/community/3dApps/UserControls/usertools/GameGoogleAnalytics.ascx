﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GameGoogleAnalytics.ascx.cs" Inherits="KlausEnt.KEP.SiteManagement.GameGoogleAnalytics" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>
<%@ Register TagPrefix="CE" Namespace="CuteEditor" Assembly="CuteEditor" %>

<link href="../../css/base/base.css" type="text/css" rel="stylesheet">
<link href="../../css/Communities/GameGoogleAnalytics.css" type="text/css" rel="stylesheet">

<div id="gaContainer">

    <div id="divGATitle" class="title">Google Analytics Integration</div>
    <div id="divError" runat="server" class="error" style="display:none;"></div>

    <div class="textbox"> <span class="label">Google Analytics Account Number:</span> <asp:TextBox ID="txtAccountID" runat="server" width="230"/></div>
    <div class="text">Eg: UA-XXXXX-XX</div>
    <div class="text"><a href="http://docs.kaneva.com/mediawiki/index.php/Main_Page">How do I find my Google Analytics Account?</a></div>

	<div class="updateButtons">
		<asp:button id="btnSave" runat="Server" CausesValidation="true" onclick="btnSave_Click" Text="Save Settings"/>&nbsp;&nbsp;
	</div>

</div>
