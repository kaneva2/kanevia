<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GameLeaderboard.ascx.cs" Inherits="KlausEnt.KEP.SiteManagement.GameLeaderboard" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>

<link href="../../css/base/base.css" type="text/css" rel="stylesheet">
<link href="../../css/Communities/GameLeaderboard.css" type="text/css" rel="stylesheet">

<div id="lbContainer">

	<div id="divEditTitle" runat="server" class="title"></div>
	<div id="divError" runat="server" class="error" style="display:none;"></div>

	<div class="textbox"><span class="label">Name:</span> <asp:TextBox ID="txtName" runat="server" width="260"/></div>
	<div class="textbox"><span class="label">Label:</span> <asp:TextBox ID="txtLabel" runat="server" width="260"/></div>
	
	<div class="type"><span class="label">Type:</span> <asp:radiobuttonlist id="rblType" repeatlayout="table" repeatdirection="horizontal" runat="server">
		<asp:listitem text="General" value="0" selected></asp:listitem>
		<asp:listitem text="Currency ($X,XXX.XX)" value="1"></asp:listitem>
		<asp:listitem text="Time (HH:MM.SS)" value="2"></asp:listitem>
	</asp:radiobuttonlist></div>
	
	<div class="sort"><span class="label">Sort:</span> <asp:radiobuttonlist id="rblOrder" repeatlayout="table" repeatdirection="horizontal" runat="server">
		
	</asp:radiobuttonlist></div>
	<div class="resetButton"><asp:button id="btnReset" runat="Server" CausesValidation="false" Text="Reset Leaderboard"/></div>
	
	<div class="updateButtons">
		<asp:button id="btnSave" runat="Server" CausesValidation="true" onclick="btnSave_Click" Text="Save"/>&nbsp;&nbsp;
	</div>

</div>
