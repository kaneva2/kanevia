<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="GamePremiumItems.ascx.cs" Inherits="KlausEnt.KEP.SiteManagement.GamePremiumItems" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../Pager.ascx" %>

<link href="../../css/base/base.css" type="text/css" rel="stylesheet">
<link href="../../css/Communities/GamePremiumItems.css" type="text/css" rel="stylesheet">


<div id="piContainer">

	<div id="piData">
		<div class="buttons">
			<asp:LinkButton cssclass="buttonFixed" id="lbNew" runat="server" causesvalidation="false" onclick="lbNew_Click"><span class="add">Add an Item</span></asp:LinkButton>
			<asp:LinkButton cssclass="buttonlarge" id="lbDelete" onclick="lbDelete_Click" runat="server" causesvalidation="false"><span class="delete">Delete Selected Item</span></asp:LinkButton>
		</div>
		<div id="divErrorTop" runat="server" class="error errtop" style="display:none;"></div>
		
		<asp:Repeater id="rptPremiumItems" runat="server" >
            <HeaderTemplate>
                <div class="header">
                    <div class="chkbox"><input type="checkbox" style="visibility:hidden;" /></div>
                    <div class="thumb">&nbsp;</div>
                    <div class="name">Item Name</div>
                    <div class="id">Item ID</div>
                    <div class="price">Price</div>
                    <div class="qty">Items<br />Sold</div>
                    <div class="sales">Total<br />Sales</div>
                    <div class="status">Status</div>
                    <div class="approval">Approval</div>
                    <div class="edit"></div>
				</div>
            </HeaderTemplate>
            <ItemTemplate>
                <div class="row">
                    <div class="chkbox"><asp:checkbox id="chkEdit" runat="server" globalid='<%# (DataBinder.Eval(Container.DataItem, "GlobalId")) %>' cssclass='<%# (DataBinder.Eval(Container.DataItem, "GlobalId")) %>' /></div>
                    <div class="thumb">
                   		<div class="frame1" style="width:40px;overflow:hidden;">
							<img id="Img2" height="25" runat="server" src='<%# GetItemImageURL (DataBinder.Eval(Container.DataItem, "ThumbnailSmallPath").ToString (), "sm") %>' border="0"/>
						</div>
					</div>
                    <div class="name"><%# (DataBinder.Eval(Container.DataItem, "Name")) %></div>
                    <div class="id"><%# (DataBinder.Eval(Container.DataItem, "GlobalId")) %></div>
                    <div class="price"><%# (DataBinder.Eval(Container.DataItem, "DesignerPrice")) %></div>
                    <div class="qty"><%# (DataBinder.Eval(Container.DataItem, "NumberSoldOnWeb")) %></div>
                    <div class="sales"><%# (DataBinder.Eval(Container.DataItem, "SalesTotal")) %></div>
                    <div class="status"><%# Convert.ToInt32(DataBinder.Eval(Container.DataItem, "ItemActive")) == 1 ? "Public" : "Private" %></div>
                    <div class="approval"><%# GetItemApprovalStatusDesc (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "ApprovalStatus")))%></div>
                    <div class="edit"><asp:LinkButton id="lbn_edit" runat="server" commandargument='<%# (DataBinder.Eval(Container.DataItem, "GlobalId")) %>' causesvalidation="false" oncommand="lbEdit_Click">edit</asp:LinkButton></div>
				</div>
            </ItemTemplate>
        </asp:Repeater>
                    
        <div class="approvalRequest"><asp:LinkButton id="lbRequestApproval" runat="server" causesvalidation="false" onclick="lbRequestApproval_Click"><span>Request Approval For Selected Items</span></asp:LinkButton></div>
        <div class="results"><asp:Label runat="server" id="lblSearch"/></div>
        <div class="pager"><Kaneva:Pager runat="server" id="pgTop" maxpagestodisplay="5" shownextprevlabels="true"/></div>
        
        <!-- Edit Section -->           
		<div id="divDetails" runat="server" class="detailsContainer">
			<div id="divEditTitle" runat="server" class="title"></div>
			<div id="divError" runat="server" class="error" style="display:none;"></div>
			<div style="float:left;width:200px;">
				<div class="frame1" style="margin-bottom:8px;">
					<div class="imgconstrain1" style="width:90px;height:90px;border:solid 1px #999;">
						<img id="imgThumbnail" runat="server" border="0" />
					</div>	
				</div>
				<input runat="server" class="inputText" id="inpThumbnail" type="file" style="width:200px;" />
				<div class="note">We recommend 90x90</div>
			</div>
			<div class="dataContainer">
				<div><div class="label">Item name:</div> <asp:TextBox ID="txtItemName" runat="server" width="260"/> <span>Item ID:</span> <asp:Label cssclass="id" ID="txtGlobalId" runat="server" visible="true" /></div>
				<div class="desc"><div class="label">Description:</div> <asp:TextBox ID="txtItemDesc" runat="server" TextMode="MultiLine" Rows="4" Width="400" /></div>
			</div>
			<div class="priceContainer" id="divPriceContainer" runat="server">
				<div class="price"><span>Price (in Credits):</span> <asp:TextBox id="txtPrice" runat="server" width="40" text="0" /><span class="note">*See the 3D App Policy for more information.</span></div>
				<div class="status"><span>Status:</span> <asp:DropDownList ID="ddlStatus" runat="server" width="80px" /></div>
				<div class="approval"><span>Approval:</span> <asp:DropDownList id="ddlApproval" runat="server" width="92px" /></div>
				<div class="reason" id="divReason" runat="server"><div class="label">Reason:</div> <asp:TextBox ID="txtDenyReason" enabled="false" maxlength="80" runat="server" TextMode="MultiLine" Rows="3" Width="300" /></div>
			</div>
			<div class="updateButtons">
                <asp:button id="btnUpdatePremiumItems" runat="Server" onClick="btnUpdatePremiumItems_Click" CausesValidation="False" Text="Save"/>&nbsp;&nbsp;
                <asp:button id="btnPremiumItemsCancel" runat="Server" CausesValidation="False" onClick="btnPremiumItemsCancel_Click" Text="Cancel"/>
			</div>
        </div>
		<a name="details" href="javascript:void(0);"></a>
		<a id="aScroll" href="#details" style="visibility:hidden;"></a>
	</div>


</div>

<script type="text/javascript">
if ($('<%= divDetails.ClientID %>').style.display == 'block')
{
	$('aScroll').click();
}
</script>

