﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GameAbout.ascx.cs" Inherits="KlausEnt.KEP.SiteManagement.GameAbout" %>

<link href="../../css/base/base.css" type="text/css" rel="stylesheet">
<link href="../../css/Communities/GameGoogleAnalytics.css" type="text/css" rel="stylesheet">

<br/><br/>

<style type="text/css">
    .meet3D {background:url(../../images/icons/icon_3dapps_17x17.gif) no-repeat 8px 4px;text-indent:30px;display:block;color:#018aaa;}

</style>



<div id="gaContainer">

    <div id="divGATitle" class="title">Welcome to your 3DApp Management Page</div>
    <div id="divError" runat="server" class="error" style="display:none;"></div>

    
    <div class="textbox">
    
        <ul>
             <li>The first thing you'll want to do is travel to your 3DApp<br/><br/>
                <div><asp:linkbutton class="buttonFixed" id="btnMeet3D" runat="server" CausesValidation="False"><span class="meet3D" id="spnMeet3D" runat="server">Go 3D!</span></asp:linkbutton></div> 
                <br/>
            </li>
            <li>To change the icon for your game click "Game Information" in the list to the left</li>
            <li><a target="_blank" href="http://docs.kaneva.com/mediawiki/index.php/3DAPP:Hosted">Learn more about Kaneva hosted 3DApps</a></li>
            <li><a target="_blank" href="http://docs.kaneva.com/mediawiki/index.php/Main_Page#Developers">Learn more about 3DApp development</a></li>
        </ul>
   

    </div>
    

	<div class="updateButtons">
		
	</div>

</div>