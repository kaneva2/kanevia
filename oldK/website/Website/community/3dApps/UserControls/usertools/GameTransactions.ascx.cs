using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Specialized;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using log4net;
using KlausEnt.KEP.Kaneva;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.usercontrols
{
    public partial class GameTransactions : BaseUserControl
    {
        #region Declarations

        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);
        private bool isAdministrator = false;
        private int pageNum = 1;
        private int pageSize = 50;
        private uint totalPrice = 0;
        private int selectedGameId = 0;
        protected HtmlContainerControl dialogAvailable, dialogCredits, dialogTotalAfterRedeem, transContainer;
        protected HtmlTable tblRedeem;
        protected HtmlAnchor btnRedeem;

        #endregion Declerations
        
        #region Page Load

        protected void Page_Load (object sender, EventArgs e)
        {
            if (Request.IsAuthenticated)
            {
                //check security level
                switch (CheckUserAccess ((int) SitePrivilege.ePRIVILEGE.STAR_ADMIN))
                {
                    case (int) SitePrivilege.eACCESS_LEVEL.ACCESS_FULL:
                        isAdministrator = true;
                        break;
                    case (int) SitePrivilege.eACCESS_LEVEL.ACCESS_READ:
                    case (int) SitePrivilege.eACCESS_LEVEL.ACCESS_NONE:
                    default:
                        break;
                }

                if (((Session["SelectedGameId"] != null) && Convert.ToInt32 (Session["SelectedGameId"]) > 0) ||
                    ((Session["AppManagementSelectedCommunityId"] != null) && Convert.ToInt32 (Session["AppManagementSelectedCommunityId"]) > 0))
                {
                    // Load the current user's data the first time
                    SelectedCommunityId = Convert.ToInt32 (Session["AppManagementSelectedCommunityId"]);

                    SelectedGameId = Session["SelectedGameId"] != null ? Convert.ToInt32 (Session["SelectedGameId"]) : SelectedGameId;

                    //get the company id the game is associated with
                    bool canEdit = GetCommunityFacade.IsCommunityOwner (SelectedCommunityId, KanevaWebGlobals.CurrentUser.UserId);

                    if (!canEdit && !isAdministrator)
                    {
                        Response.Redirect ("~/default.aspx");
                    }
                    else
                    {
                        //community id check is to allow kaneva website to make use of this usercontrol
                        if (!IsPostBack || (Request["communityId"] != null))
                        {
                            // Log the CSR activity
                            GetSiteManagementFacade.InsertCSRLog (KanevaWebGlobals.CurrentUser.UserId, "CSR - Viewing game transaction information page", 0, SelectedGameId);

                            //configure page
                            ConfigurePageForUse ();
                        }
                    }
                }
            }
            else
            {
                Response.Redirect ("~/default.aspx");
            }
        }

        #endregion Page Load

        #region Functions

        private void ConfigurePageForUse ()
        {
            ShowTotals ();
            BindTransactions (false);
        }

        public void BindTransactions (bool redeem)
        {
            //get the games servers for the game
            try
            {
                //get the premium items from the database
                PagedList<PurchaseTransactionPremiumItem> trans = GetTransactionFacade.GetPurchaseTransactionsByGame (SelectedCommunityId, "", OrderBy, PageNum, PageSize);

                if (trans.TotalCount > 0)
                {
                    pgTop.NumberOfPages = Math.Ceiling ((double) trans.TotalCount / pageSize).ToString ();
                    pgTop.DrawControl ();

                    rptTransactions.DataSource = trans;
                    rptTransactions.DataBind ();

                    // The results
                    lblSearch.Text = GetResultsText (trans.TotalCount, PageNum, pageSize, trans.Count); 
                }
                else
                {
                    transContainer.Visible = false;
                    pNoTrans.Visible = true;
                }
            }
            catch (Exception ex)
            {
                m_logger.Error ("Error Loading premium items.", ex);
            }
        }

        private PremiumItemRedeemAmounts GetUserTransactionBalances ()
        {
            return GetTransactionFacade.GetTotalTransactionCreditAmountsByGame (SelectedCommunityId, KanevaGlobals.PremiumItemDaysPendingBeforeRedeem);
        }

        private void ShowTotals ()
        {
            PremiumItemRedeemAmounts amts = GetUserTransactionBalances ();

            divTotalCredits.InnerText = amts.EarnedTotal.ToString ("#,###");
            divTotalCash.InnerText = amts.EarnedAvailable.ToString ("#,###");

            ConfigureRedeemDialog (amts);
        }

        public int GetUserRedemptionAmount (int amt)
        {
            try
            {   // If decimal value, rounds up. 2.0 = 2, 2.1 = 3, 2.7 = 3
                return Convert.ToInt32(Math.Ceiling (amt * KanevaGlobals.PremiumItemUserRedemptionPercentage));
            }
            catch
            {
                return 0;
            }
        }

        private void ConfigureRedeemDialog (PremiumItemRedeemAmounts amts)
        {
            int available = GetUserRedemptionAmount (amts.Available);
            int credits = (int) KanevaWebGlobals.CurrentUser.Balances.Credits;

            if (available > 0)
            {
                dialogAvailable.InnerText = available.ToString ("#,###");
                dialogCredits.InnerText = credits > 0 ? credits.ToString ("#,###") : "0";
                dialogTotalAfterRedeem.InnerText = (available + credits).ToString ("#,###");
                // This adds code to open dialog when redeem button is clicked
                litRedeemClick.Text = "$j('#dialog').dialog('open');";
            }
            else
            {
                btnRedeem.Attributes.Add ("class", "btnmedium grey disable");
                btnRedeem.Disabled = true;
                // Do not open dialog when redeem button clicked if nothing to redeem
                litRedeemClick.Text = "";
            }
        }

        /// <summary>
        /// Return the users privilege settings. All privileges available and their corresponding 
        /// access levels.
        /// </summary>
        /// <returns></returns>
        public int CheckUserAccess (int privilegeId)
        {
            int accessLevel = (int) SitePrivilege.eACCESS_LEVEL.ACCESS_NONE;
            try
            {
                //users privileges
                NameValueCollection privileges = KanevaWebGlobals.CurrentUser.SitePrivileges;

                //find their access level for the privilege being checked
                accessLevel = Convert.ToInt32 (privileges[privilegeId.ToString ()]);
            }
            catch { }

            return accessLevel;
        }
        
        #endregion Functions

        #region Event Handlers

        public void rptTransactions_ItemDataBound (object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HtmlTableCell status = (HtmlTableCell) e.Item.FindControl ("status");

                DateTime purchaseDate = Convert.ToDateTime (DataBinder.Eval (e.Item.DataItem, "PurchaseDate"));
                PurchaseTransactionPremiumItem.RedeemStatus redeem = (PurchaseTransactionPremiumItem.RedeemStatus)DataBinder.Eval (e.Item.DataItem, "RedemptionStatus");

                if (redeem == PurchaseTransactionPremiumItem.RedeemStatus.Pending)
                {   //Pending
                    status.InnerHtml = "<span>Pending</span>";
                }
                else if (redeem == PurchaseTransactionPremiumItem.RedeemStatus.Available)
                {   //Available
                    status.InnerHtml = "<span class=\"available\">Available</span>";
                }
                else
                {   // Redeemed
                    status.InnerHtml = "<span class=\"redeemed\">Redeemed</span>";   
                }
            }            
        }

        /// <summary>
        /// Page Change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pgTop_PageChange (object sender, PageChangeEventArgs e)
        {
            PageNum = e.PageNumber;
            BindTransactions (false);
        }

        protected void btnDoRedeem_Click (object sender, EventArgs e)
        {
            if (Request.IsAuthenticated)
            {
                // Verify user has credits to redeem            
                PremiumItemRedeemAmounts amts = GetUserTransactionBalances();
                if (amts.Available > 0)
                {
                    // Redeem credits                     
                    try
                    {
                        int successfulPayment = -1;

                        // Update Redeem dialog 
                        int ret = GetTransactionFacade.RedeemPremiumItemCredits (SelectedCommunityId, KanevaGlobals.PremiumItemDaysPendingBeforeRedeem);

                        if (ret > 0)
                        {
                            successfulPayment = GetUserFacade.AdjustUserBalance (KanevaWebGlobals.CurrentUser.UserId,
                                Constants.CURR_CREDITS, amts.EarnedAvailable, Constants.CASH_TT_PREMIUM_ITEM_CREDIT_REDEEM);

                            if (successfulPayment == 0)
                            {
                                // update credit totals in header
                                ScriptManager.RegisterStartupScript (this, GetType (), "fortune", "GetFortune();", true);
                            }
                            else
                            {
                                // Log issue
                                m_logger.Error ("AdjustUserBalance FAILED. UserId = " + KanevaWebGlobals.CurrentUser.UserId + ", Amount = " + amts.EarnedAvailable);
                            }
                        }
                        else
                        {
                            // Log issue
                            m_logger.Error ("FAILED to Redeem Premin Item Credits. CommunityId = " + SelectedCommunityId + ", UserId = " + KanevaWebGlobals.CurrentUser.UserId);
                        }
                    }
                    catch (Exception)
                    { }

                    // Update totals
                    ShowTotals ();

                    BindTransactions (true);
                }
            }
        }


        #endregion Event Handlers

        #region Attributes

        private int PageNum
        {
            set
            {
                this.pageNum = value;
            }
            get
            {
                return this.pageNum;
            }
        }
        private int PageSize
        {
            set
            {
                this.pageSize = value;
            }
            get
            {
                return this.pageSize;
            }
        }
        /// <summary>
        /// DEFAULT_SORT
        /// </summary>
        /// <returns></returns>
        public string OrderBy
        {
            get
            {
                return "purchase_id DESC";
            }
        }

        private uint TotalPrice
        {
            get {return totalPrice;}
            set {totalPrice = value;}
        }

        /// <summary>
        /// SELECTED_COMMUNITY_ID
        /// </summary>
        /// <returns></returns>
        private int SelectedCommunityId
        {
            get
            {
                return (int) ViewState["community_id"];
            }
            set
            {
                ViewState["community_id"] = value;
            }
         }

        /// <summary>
        /// SELECTED_Game_ID
        /// </summary>
        /// <returns></returns>
        private int SelectedGameId
        {
            get
            {
                if (selectedGameId > 0)
                {
                    return selectedGameId;
                }
                else
                {
                    selectedGameId = GetCommunityFacade.GetCommunityGameId (SelectedCommunityId);
                    return selectedGameId;
                }
            }
            set
            {
                selectedGameId = value;
            }

        }

        #endregion Attributes

        #region Web Form Designer generated code

        override protected void OnInit (EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent ();
            base.OnInit (e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent ()
        {
            this.Load += new System.EventHandler (this.Page_Load);
            pgTop.PageChanged += new PageChangeEventHandler (pgTop_PageChange);
        }

        #endregion Web Form Designer generated code
    }
}