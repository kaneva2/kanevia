using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using log4net;

namespace KlausEnt.KEP.Kaneva.usercontrols
{
    public partial class GameData : BaseUserControl
    {
        #region Declarations

        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private Community _communityToEdit = null;
        private bool _ucInitialLoad = false;

        public event EventHandler GameDataRefresh;

        #endregion

        #region Page Load

        protected void Page_Load(object sender, EventArgs e)
        {
            if (GetControlParameters() && CheckUserAccess())
            {
                divErrorData.Style.Add("display", "none");

                if (!IsPostBack || UCInitialLoad)
                {
                    ConfigurePageForUse();
                }
                else
                {
                    // This needs to be set, regardless of postback
                    libraryImageSelector.ReturnField = this.txtBGPicture;
                }
            }
            else
            {
                Response.Redirect(GetLoginURL());
            }
        }

        #endregion

        #region Helper Functions

        private bool GetControlParameters()
        {
            // Some params are required for page function.  Check them here.
            bool gotAllRequiredParams = (Session["AppManagementSelectedCommunityId"] != null);

            if (Session["AppManagementSelectedCommunityId"] != null)
            {
                CommunityToEdit = GetCommunityFacade.GetCommunity(Convert.ToInt32(Session["AppManagementSelectedCommunityId"]), true);
                Session.Remove("AppManagementSelectedCommunityId");
            }

            if (Session["AppManagementUCReload"] != null)
            {
                UCInitialLoad = (Session["AppManagementUCReload"] != null && Convert.ToBoolean(Session["AppManagementUCReload"]));
                Session.Remove("AppManagementUCReload");
            }

            return gotAllRequiredParams;
        }

        private bool CheckUserAccess()
        {
            return (Request.IsAuthenticated) &&
                (IsAdmin || GetCommunityFacade.IsCommunityOwner(CommunityToEdit.CommunityId, KanevaWebGlobals.CurrentUser.UserId));
        }

        private void GetCommunityAccessPassInfo(out bool hasPass, out bool passSetByAdmin)
        {
            DataTable dtGamePasses;
            if (CommunityToEdit.WOK3App != null && CommunityToEdit.WOK3App.GameId > 0)
            {
                dtGamePasses = GetGameFacade.GetGamePasses(CommunityToEdit.WOK3App.GameId);
            }
            else
            {
                dtGamePasses = GetCommunityFacade.GetCommunityPasses(CommunityToEdit.CommunityId, CommunityToEdit.CommunityTypeId);
            }

            hasPass = false;
            passSetByAdmin = false;
            if (dtGamePasses != null && dtGamePasses.Rows.Count > 0)
            {
                foreach (DataRow row in dtGamePasses.Rows)
                {
                    if (Int32.Parse(row["pass_group_id"].ToString()) == Configuration.AccessPassGroupID)
                    {
                        hasPass = true;
                        passSetByAdmin = row["set_by_admin"].ToString() == "Y";
                        break;
                    }
                }
            }
        }

        private void ConfigurePageForUse()
        {
            try
            {
                if (CommunityToEdit.CommunityId > 0)
                {
                    //configure page javascript
                    ConfigureJavaScript();

                    //set validation values
                    revName.ValidationExpression = Constants.VALIDATION_REGEX_CHANNEL_NAME;
                    revName.ErrorMessage = Constants.VALIDATION_REGEX_GAME_NAME_ERROR_MESSAGE;

                    // Per Animesh, don't allow editing of 3dapp game names
                    if (CommunityToEdit.WOK3App != null && CommunityToEdit.WOK3App.GameId > 0)
                    {
                        txtGameName.Enabled = false;

                        // Per Animesh and Jeff, show GameId if user is a site admin
                        if (KanevaWebGlobals.CheckUserAccess((int)SitePrivilege.ePRIVILEGE.SITE_ADMIN) == (int)SitePrivilege.eACCESS_LEVEL.ACCESS_FULL)
                        {
                            lblGameId.Text = "<div class=\"section\"><span class=\"label\">Game Id: </span>" + CommunityToEdit.WOK3App.GameId.ToString() + "</div><br/>";
                            lblGameId.Visible = true;
                        }

                        divAPSection.Visible = false;
                    }

                    // Initialize textfields
                    txtGameName.Text = CommunityToEdit.Name;
                    txtDescription.Text = Server.HtmlDecode(CommunityToEdit.Description);
                    txtBGPicture.Text = CommunityToEdit.BackgroundImage;
                    libraryImageSelector.ReturnField = this.txtBGPicture;
                    txtBackgroundColor.Text = CommunityToEdit.BackgroundRGB;
                    InitURLData();

                    // Initialize checkboxes
                    bool hasAccessPass;
                    bool passSetByAdmin;
                    GetCommunityAccessPassInfo(out hasAccessPass, out passSetByAdmin);

                    cbIsAdult.Checked = hasAccessPass;
                    if (hasAccessPass && passSetByAdmin && !IsAdmin)
                    {
                        cbIsAdult.Enabled = false;
                        apDisableReason.InnerText = "The Access Pass setting of this World was set by an admin.";
                    }
                    else if (!hasAccessPass && !IsAdmin && !KanevaWebGlobals.CurrentUser.HasAccessPass)
                    {
                        cbIsAdult.Enabled = false;
                        apDisableReason.InnerText = "You must have an active Access Pass subscription to change this setting.";
                    }

                    BindProfileTabData();

                    // Initialize thumbnaila
                    imgPreview.Src = GetBroadcastChannelImageURL(CommunityToEdit.ThumbnailMediumPath, "me");
                    selectPhoto.PostBackUrl = Request.Url.AbsoluteUri + "#library";
                    
                    // Initialize dropdowns
                    PopulateCommunityCategories();

                    // Don't let user delete their home world
                    btnDelete.Visible = (KanevaWebGlobals.CurrentUser.HomeCommunityId != CommunityToEdit.CommunityId);

                    // Set Access
                    rdoOpen.Checked = false;
                    rdoClosed.Checked = false;
                    rdoOwnerOnly.Checked = false;
                    if (CommunityToEdit.IsPublic.Equals ("N"))
                    {
                        rdoClosed.Checked = true;
                    }
                    else if (CommunityToEdit.IsPublic.Equals ("I"))
                    {
                        rdoOwnerOnly.Checked = true;
                    }
                    else
                    {
                        rdoOpen.Checked = true;
                    }
                }
                else
                {
                    ShowMessage("No World information found");
                    m_logger.Error("No World information found");
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Error while loading World data");
                m_logger.Error("Error GameData: while loading World data", ex);
            }
        }

        private void ConfigureJavaScript()
        {
            // Include colorpicker
            if (!Page.ClientScript.IsClientScriptIncludeRegistered("colorpicker"))
            {
                Page.ClientScript.RegisterClientScriptInclude("colorpicker", ResolveUrl("~/jscript/PageLayout/colorpicker.js"));
            }

            // Re-initialize litJS (easiest to do so here)
            litJS.Text =
                        @"function ConfigDefaultTab(cbx) {
                            var rbl = document.getElementsByName($('" + rblDefaultTab.ClientID + @"_0').name);
                            if (cbx.checked == true) {
                                rbl[0].disabled = false;
                            }
                            else {
                                rbl[0].disabled = true;
                                rbl[1].checked = true;
                            }
                        }
                        var selectedCategories = new Array()
                        function categorySelected(obj)
                        {										 
                          if (obj.checked)
                          {    
                            //a new checkbox is selected, pop the first selection if 3 are already selected
                            if (selectedCategories.length == 3)
                            {											   
                              firstEle = selectedCategories.shift();
                              firstEle.checked = false;
                            }
                            selectedCategories.push(obj);  
                          }else
                          {		   
                            var index = -1;		  
                            //remove it from the array if it exists
                            for (var i=0; i < selectedCategories.length; i++)
                            {
                              if (selectedCategories[i] == obj)
                              {
                                index = i;
                                break;          
                              }
                            }
                            if (index >= 0)
                            {
                              //and rearrange the list
                              for (var i=index; i < selectedCategories.length-1; i++)
                              {
                                selectedCategories[i] = selectedCategories[i+1];
                              }
                              selectedCategories.pop();
                            }
                          }
                        }";

            //add clear function to button
            aRemove.Attributes.Add("onclick", "ClearField('" + txtBGPicture.ClientID + "')");

            //add onchange event to the color picker
            txtBackgroundColor.Attributes.Add("onchange", "relateColor('" + txtBackgroundColorAppClrPickerStr.ClientID + "',this.value);");
            txtBackgroundColorAppClrPickerStr.NavigateUrl = "javascript:pickColorMP('" + txtBackgroundColorAppClrPickerStr.ClientID + "');";
            if (!Page.ClientScript.IsClientScriptBlockRegistered(GetType(), "setbgcolor"))
            {
                Page.ClientScript.RegisterStartupScript(GetType(), "setbgcolor", "<script>relateColor('" +
                    txtBackgroundColorAppClrPickerStr.ClientID + "',$('" + txtBackgroundColor.ClientID + "').value);</script>");
            }
        }

        private void PopulateCommunityCategories()
        {
            CommunityCategories = GetCommunityFacade.GetCommunityCategories(CommunityToEdit.CommunityId);

            dlCategories.DataSource = GetCommunityFacade.GetCommunityCategories();
            dlCategories.DataBind();
        }

        private void BindProfileTabData()
        {
            // Everyone just get's the tabs for 3dapps now
            IList<CommunityTab> tabs = GetCommunityFacade.GetCommunityTabs(CommunityToEdit.CommunityId, (int)CommunityType.APP_3D);

            cblTabs.Items.Clear();
            rblDefaultTab.Items.Clear();

            // Do this check because communities that have not been configured yet will not have any rows
            // in the community_tabs table, so by default we show all the tabs until user changes config.
            bool selectAll = true;
            foreach (CommunityTab ct in tabs)
            {
                if (ct.CommunityId > 0)
                {
                    selectAll = false;
                    break;
                }
            }

            foreach (CommunityTab ct in tabs)
            {
                ListItem li = new ListItem(ct.TabName, ct.TabId.ToString());
                li.Enabled = ct.IsConfigurable;
                if (ct.CommunityId > 0 || selectAll) li.Selected = true;
                if (!ct.IsConfigurable) li.Selected = true;
                cblTabs.Items.Add(li);

                // Default Tab radio buttons
                if (ct.TabId == (int)Constants.eTab.About)
                {
                    // Add the About selection
                    ListItem liAbout = new ListItem(Constants.TabType.ABOUT, ((int)Constants.eTab.About).ToString());
                    liAbout.Enabled = false;
                    liAbout.Selected = false;
                    li.Attributes.Add("onclick", "ConfigDefaultTab(this);");

                    // Add the Blast selection
                    ListItem liBlast = new ListItem(Constants.TabType.BLAST, ((int)Constants.eTab.Blast).ToString());
                    liBlast.Enabled = true;

                    if (ct.CommunityId > 0 || selectAll)
                    {
                        liAbout.Enabled = true;
                        if (ct.IsDefault)
                        {
                            liAbout.Selected = true;
                        }
                        else
                        {
                            liBlast.Selected = true;
                        }
                    }
                    else
                    {
                        liBlast.Selected = true;
                    }

                    rblDefaultTab.Items.Add(liAbout);
                    rblDefaultTab.Items.Add(liBlast);
                }
            }
        }

        private void InitURLData()
        {
            if ((CommunityToEdit.Url != null) && (CommunityToEdit.Url != ""))
            {
                lblURL.Style.Add("font-weight", "bold");
                lblURL.Text = "http://" + KanevaGlobals.SiteName + "/" + CommunityToEdit.Url;
                txtURL.Visible = false;
                revURL.Enabled = false;
                spnRegistered.Visible = true;
                urlWarning.Visible = false;
            }
            else
            {
                // Set up regular expression validators
                revURL.ValidationExpression = Constants.VALIDATION_REGEX_USERNAME;
                lblURL.Text = "http://" + KanevaGlobals.SiteName + "/";
                urlWarning.Visible = true;
            }
        }

        private bool IsURLAvailable()
        {
            bool isURLAvailable = false;

            if ((CommunityToEdit.Url == null || CommunityToEdit.Url == "") && txtURL.Visible)
            {
                //get and uncode url
                string url = txtURL.Text.Trim();

                if ((url != null) && (url != ""))
                {
                    url = Server.HtmlEncode(url);

                    //check to see if the URL is in violation of allowed words, or if it is already in use.
                    //if it is ok add for save to database, otherwise display error message
                    if (KanevaWebGlobals.isTextRestricted(url, Constants.eRESTRICTION_TYPE.POTTY_MOUTH))
                    {
                        ShowMessage(Constants.VALIDATION_REGEX_RESTRICTED_WORD_MESSAGE, true);
                    }
                    else if (KanevaWebGlobals.isTextRestricted(url, Constants.eRESTRICTION_TYPE.RESERVED))
                    {
                        ShowMessage(Constants.VALIDATION_REGEX_RESERVED_WORD_MESSAGE, true);
                    }
                    else if (GetCommunityFacade.IsCommunityURLTaken(url, CommunityToEdit.CommunityId))
                    {
                        ShowMessage("The URL you have selected is not available. Please Choose another", true);
                    }
                    else
                    {
                        //update URL
                        CommunityToEdit.Url = url;
                        isURLAvailable = true;
                    }
                }
                //no URL was provided
                else
                {
                    isURLAvailable = true;
                }
            }
            else //the URL has been set before so set this to true to keep processing
            {
                isURLAvailable = true;
            }

            return isURLAvailable;
        }

        private void ShowMessage(string msg)
        {
            ShowMessage(msg, true);
        }

        private void ShowMessage(string msg, bool isError)
        {
            divErrorData.InnerHtml = msg;
            divErrorData.Attributes.Add("class", isError ? "error" : "confirm");
            divErrorData.Style.Add("display", "block");
        }

        #endregion

        #region Attributes

        private Community CommunityToEdit
        {
            get { return _communityToEdit; }
            set { _communityToEdit = value; }
        }

        private bool UCInitialLoad
        {
            get { return _ucInitialLoad; }
            set { _ucInitialLoad = value; }
        }

        private bool IsAdmin
        {
            get { return (KanevaWebGlobals.CheckUserAccess((int)SitePrivilege.ePRIVILEGE.STAR_ADMIN) == (int)SitePrivilege.eACCESS_LEVEL.ACCESS_FULL); }
        }

        private List<CommunityCategory> CommunityCategories
        {
            get
            {
                if (ViewState["CommunityCategories"] == null)
                {
                    return null;
                }
                return (List<CommunityCategory>)ViewState["CommunityCategories"];
            }
            set
            {
                ViewState["CommunityCategories"] = value;
            }
        }

        private bool IsGameWorld
        {
            get
            {
                if ((CommunityToEdit.CommunityTypeId == (int)CommunityType.COMMUNITY && 
                    (CommunityToEdit.TemplateId == 11 || CommunityToEdit.TemplateId == 12))
                   )
                {
                    return true;
                }
                return false;
            }
        }

        #endregion

        #region Event Handlers

        protected bool GetCatChecked(int catId)
        {
            bool isChecked = false;
            //searh stored categories
            if ((CommunityCategories != null) && (CommunityCategories.Count > 0))
            {
                foreach (CommunityCategory cat in CommunityCategories)
                {
                    if (cat.CategoryId == catId)
                    {
                        isChecked = true;
                        break;
                    }
                }
            }
            return isChecked;
        }

        protected void dlCategories_OnDataBind(object sender, DataListItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item ||
                e.Item.ItemType == ListItemType.AlternatingItem)
            {
                // Retrieve the Label control in the current DataListItem.
                CheckBox checkBox = (CheckBox)e.Item.FindControl("chkCategory");

                if (checkBox != null && checkBox.Checked)
                {
                    litJS.Text += "categorySelected($('" + checkBox.ClientID + "'));";
                }
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            if (GetCommunityFacade.IsCommunityOwner(CommunityToEdit.CommunityId, KanevaWebGlobals.CurrentUser.UserId))
            {
                // Don't chagne this to Facade until everything is implemented there!!!
                // Facade did not implement deleteing members, 
                //      removing assets and adjusting counts, 
                //      set last update so it would be removed from search, 
                //      call correct SP so wok items get taken care of.
                CommunityUtility.DeleteCommunity(CommunityToEdit.CommunityId, KanevaWebGlobals.CurrentUser.UserId);
            }

            Response.Redirect("~/mykaneva/my3dApps.aspx");

        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                // Server side validation
                Page.Validate();
                if (!Page.IsValid)
                {
                    string errMsg = "";
                    for (int i = 0; i < Page.Validators.Count; i++)
                    {
                        if (!Page.Validators[i].IsValid)
                        {
                            errMsg += Page.Validators[i].ErrorMessage + "  ";
                        }
                    }
                    ShowMessage(errMsg);
                    return;
                }

                // Check to make sure user did not enter any "potty mouth" or reserved words
                if (KanevaWebGlobals.isTextRestricted(txtGameName.Text, Constants.eRESTRICTION_TYPE.POTTY_MOUTH) ||
                    KanevaWebGlobals.isTextRestricted(txtDescription.Text, Constants.eRESTRICTION_TYPE.POTTY_MOUTH) ||
                    KanevaWebGlobals.isTextRestricted((txtBGPicture != null ? txtBGPicture.Text : ""), Constants.eRESTRICTION_TYPE.POTTY_MOUTH) ||
                    KanevaWebGlobals.isTextRestricted((txtBackgroundColor != null ? txtBackgroundColor.Text : ""), Constants.eRESTRICTION_TYPE.POTTY_MOUTH) ||
                    KanevaWebGlobals.isTextRestricted((txtURL != null ? txtURL.Text : ""), Constants.eRESTRICTION_TYPE.POTTY_MOUTH))
                {
                    m_logger.Warn("User " + KanevaWebGlobals.CurrentUser.UserId + " tried to script from IP " + Common.GetVisitorIPAddress());
                    ShowMessage(Constants.VALIDATION_REGEX_RESTRICTED_WORD_MESSAGE);
                    return;
                }
                else if (KanevaWebGlobals.ContainsInjectScripts(txtGameName.Text) ||
                    KanevaWebGlobals.ContainsInjectScripts(txtDescription.Text) ||
                    KanevaWebGlobals.ContainsInjectScripts((txtBGPicture != null ? txtBGPicture.Text : "")) ||
                    KanevaWebGlobals.ContainsInjectScripts((txtBackgroundColor != null ? txtBackgroundColor.Text : "")) ||
                    KanevaWebGlobals.ContainsInjectScripts((txtURL != null ? txtURL.Text : "")))
                {
                    m_logger.Warn("User " + KanevaWebGlobals.CurrentUser.UserId + " tried to script from IP " + Common.GetVisitorIPAddress());
                    ShowMessage("Your input contains invalid scripting, please remove script code and try again.");
                    return;
                }

                // Veryify name does not already exist
                if (GetCommunityFacade.IsCommunityNameTaken(false, txtGameName.Text.Replace(" ", ""), CommunityToEdit.CommunityId) &&
                    !(CommunityToEdit.WOK3App != null && CommunityToEdit.WOK3App.GameId > 0))
                {
                    ShowMessage("Name " + txtGameName.Text + " already exists, please chose a different name.");
                    return;
                }
                
                // Save community profile information
                if (IsURLAvailable())
                {
                    // Profile background
                    // Per Animesh, don't allow editing of 3dapp game names
                    if (!(CommunityToEdit.WOK3App != null && CommunityToEdit.WOK3App.GameId > 0))
                    {
                        CommunityToEdit.Name = Server.HtmlEncode(txtGameName.Text);
                    }
                    CommunityToEdit.NameNoSpaces = CommunityToEdit.Name.Replace(" ", "");
                    CommunityToEdit.Description = Server.HtmlEncode(txtDescription.Text);
                    CommunityToEdit.BackgroundImage = Server.HtmlEncode(txtBGPicture.Text);
                    CommunityToEdit.BackgroundRGB = Server.HtmlEncode(txtBackgroundColor.Text);
                    
                    // Per Jeff, we need to updata pass groups too
                    bool alreadyHasPass;
                    bool passSetByAdmin;
                    GetCommunityAccessPassInfo(out alreadyHasPass, out passSetByAdmin);

                    if (CommunityToEdit.WOK3App == null || CommunityToEdit.WOK3App.GameId <= 0)
                    {
                        if ((IsAdmin || KanevaWebGlobals.CurrentUser.HasAccessPass) && cbIsAdult.Checked)
                        {
                            CommunityToEdit.IsAdult = "Y";

                            if (!alreadyHasPass)
                            {
                                GetCommunityFacade.AddCommunityPass(KanevaWebGlobals.CurrentUser.UserId, IsAdmin, CommunityToEdit.CommunityId, CommunityToEdit.CommunityTypeId, Configuration.AccessPassGroupID);
                            }
                        }
                        else if (!cbIsAdult.Checked && (IsAdmin || !passSetByAdmin))
                        {
                            GetCommunityFacade.DeleteCommunityPass(KanevaWebGlobals.CurrentUser.UserId, IsAdmin, CommunityToEdit.CommunityId, CommunityToEdit.CommunityTypeId, Configuration.AccessPassGroupID);
                            CommunityToEdit.IsAdult = "N";
                        }
                    }

                    // Set Access
                    if (rdoClosed.Checked)
                    {
                        CommunityToEdit.IsPublic = "N";
                    }
                    else if (rdoOwnerOnly.Checked)
                    {
                        CommunityToEdit.IsPublic = "I";
                    }
                    else
                    {
                        CommunityToEdit.IsPublic = "Y";
                    }

                    // Perform the update
                    GetCommunityFacade.UpdateCommunity(CommunityToEdit);

                    // Profile tabs
                    foreach (ListItem li in cblTabs.Items)
                    {
                        GetCommunityFacade.UpdateCommunityTabs(Convert.ToInt32(li.Value),
                            CommunityToEdit.CommunityId, (int)CommunityType.APP_3D, li.Selected,
                            Convert.ToInt32(rblDefaultTab.SelectedValue) == Convert.ToInt32(li.Value) ? true : false);
                    }

                    // Categories
                    GetCommunityFacade.DeleteCommunityCategories(CommunityToEdit.CommunityId);
                    foreach (DataListItem dliCategory in dlCategories.Items)
                    {
                        CheckBox chkCategory = (CheckBox)dliCategory.FindControl("chkCategory");

                        if (chkCategory.Checked)
                        {
                            HtmlInputHidden hidCatId = (HtmlInputHidden)dliCategory.FindControl("hidCatId");
                            GetCommunityFacade.InsertCommunityCategory(CommunityToEdit.CommunityId, Convert.ToInt32(hidCatId.Value));
                        }
                    }

                    string uploadImageMsg = "";
                    try
                    {
                        //save thumbnail
                        if ((browseTHUMB != null) && (browseTHUMB.Value != "") && (CommunityToEdit.ThumbnailMediumPath != browseTHUMB.Value))
                        {
                            uploadImageMsg = ((BasePage)Page).UploadImage(CommunityToEdit.CommunityId, browseTHUMB, KanevaWebGlobals.CurrentUser.UserId, null);
                            Community reloadedCommunity = GetCommunityFacade.GetCommunity(CommunityToEdit.CommunityId);
                            CommunityToEdit.ThumbnailMediumPath = reloadedCommunity.ThumbnailMediumPath;
                        }
                    }
                    catch (Exception) { }

                    //indicated a successful save
                    if (string.IsNullOrEmpty(uploadImageMsg))
                    {
                        ShowMessage("Your changes were saved successfully.", false);
                    }
                    else
                    {
                        ShowMessage(uploadImageMsg + "<br />Your other changes were saved successfully.", false);
                    }
                }
            }
            catch (Exception exc)
            {
                m_logger.Error("Error saving world ", exc);
                ShowMessage("Error saving World " + exc.Message);
            }

            ConfigurePageForUse();

            // Tell any interested parties that the data has refreshed
            if (GameDataRefresh != null)
            {
                GameDataRefresh(this, new EventArgs());
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(GetBroadcastChannelUrl(CommunityToEdit.NameNoSpaces));
        }

        protected void selectPhoto_Click(object sender, EventArgs e)
        {
            libraryImageSelector.Visible = true;
        }

        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }

        #endregion

    }
}
