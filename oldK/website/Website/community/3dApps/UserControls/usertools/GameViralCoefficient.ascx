<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GameViralCoefficients.ascx.cs" Inherits="KlausEnt.KEP.SiteManagement.GameViralCoefficients" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>
<%@ Register TagPrefix="CE" Namespace="CuteEditor" Assembly="CuteEditor" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../Pager.ascx" %>

<link href="../../css/base/base.css" type="text/css" rel="stylesheet">
<link href="../../css/Communities/GameViralCoefficient.css" type="text/css" rel="stylesheet">

<div id="vcContainer">

    <div id="divVCTitle" class="title">Viral Coefficient</div>
    <div id="divVCNote" class="note">Viral Coefficient Goal is to Achieve 1.1 or higher</div>
    <div id="divErrorTop" runat="server" class="error" style="display:none;"></div>
    
    <div id="vcData">
        <div id="data">
            <asp:repeater id="rptViralCoefficients" runat="server">
                <HeaderTemplate>
                    <div class="header">
                        <div class="dates">Date Range</div>
                        <div class="invitees_count">Avg # of Invitees per Invite</div>
                        <div class="small">*</div>
                        <div class="accepted_invites">Accepted Invites</div>
                        <div class="small">*</div>
                        <div class="inviters">Inviters</div>
                        <div class="small">=</div>
                        <div class="viral_coefficient">Viral Coefficient</div>
				    </div>
                </HeaderTemplate>
                <itemtemplate>
                    <div class="row">
                        <div class="coefficientInfo">
                            <div class="dates"><%# DataBinder.Eval(Container.DataItem, "date_range").ToString()%></div>
						    <div class="invitees_count"><%# DataBinder.Eval(Container.DataItem, "avg_invitees_by_inviter", "{0:0.0000}")%></div>
                            <div class="small"></div>
                            <div class="accepted_invites"><%# DataBinder.Eval(Container.DataItem, "accepted_invites", "{0:0.00}")%>%</div>
					        <div class="small"></div>
                            <div class="inviters"><%# DataBinder.Eval(Container.DataItem, "inviters_over_total_visitors", "{0:0.00}")%>%</div>
                            <div class="small"></div>
                            <div class="viral_coefficient"><%# DataBinder.Eval(Container.DataItem, "viral_coefficient", "{0:0.0000}")%></div>
                        </div>
                    </div>
                </itemtemplate>
            </asp:repeater>
        </div>

        <div class="results"><asp:Label runat="server" id="lblSearch"/></div>
        
    
        <a name="details" href="javascript:void(0);"></a>
		<a id="aScroll" href="#details" style="visibility:hidden;"></a>
    </div>
</div>
