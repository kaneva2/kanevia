<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="GameData.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.usercontrols.GameData" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>
<%@ Register TagPrefix="CE" Namespace="CuteEditor" Assembly="CuteEditor" %>
<%@ Register TagPrefix="Kaneva" TagName="LibraryImages" Src="~/usercontrols/SelectPhotoFromLibrary.ascx" %>

<link href="../../css/base/base_new.css" type="text/css" rel="stylesheet" />
<link href="../../css/Communities/GameData.css?v4" type="text/css" rel="stylesheet" />
 
<div id="infoContainer">
	
	<!--BASIC INFO -->
    <div id="infoData">
		<div class="title"><h4>World Info</h4></div>

		<div id="divErrorData" runat="server" class="error"></div>

		<div style="clear:both;"></div>
		<div class="section">
			<div class="name">
                <div class="label">Name:</div> <asp:TextBox ID="txtGameName" MaxLength="40" runat="server"/>
                <div class="inlineSubmitContainer">
		            <asp:linkbutton id="btnInlineSave" runat="Server" onClick="btnUpdate_Click" CausesValidation="False" Text="Save" CssClass="btnxsmall grey gdatabutton"></asp:linkbutton>
		            <asp:linkbutton id="btnInlineCancel" runat="Server" CausesValidation="False" onClick="btnCancel_Click" Text="Cancel" CssClass="btnxsmall grey gdatabutton"></asp:linkbutton>
		        </div>
				<asp:RegularExpressionValidator id="revName" runat="server" Display="Dynamic" Text="*" ControlToValidate="txtGameName" EnableClientScript="True"></asp:RegularExpressionValidator> 
				<asp:RequiredFieldValidator ID="rfItemName" ControlToValidate="txtGameName" Text="*" ErrorMessage="Game name is a required field." Display="Dynamic" runat="server"/>
			</div>
		</div>

        <asp:Label id="lblGameId" runat="server" Visible="false"/>

        <div id="profilePhoto">
			<div class="label">World Thumbnail</div>
            <div class="bump-right-15 bump-down-10">
		        <div class="photo">
                    <img runat="server" id="imgPreview"  />
                </div>
                <div class="photoDescription">
                    <span>We recommend 200 x 150</span>		        
                </div>                
	            <div class="photoLink">
                    <input type="file" runat="server" id="browseTHUMB" size="45" />
	            </div> 
            </div> 
		</div>		

        <hr class="divider" />

		<div class="section description">
			<div>
                <div class="label">Brief Description:</div> 
                <div class="bump-right-15 bump-down-10">
                    <asp:RequiredFieldValidator id="rftxtDescription" runat="server" Display="Dynamic" ErrorMessage="Description is a required field." Text="*" ControlToValidate="txtDescription"></asp:RequiredFieldValidator>
			        <asp:TextBox cssclass="txtbox" ID="txtDescription" TextMode="multiline" Rows="3" MaxLength="1000" runat="server"/>
		        </div>
            </div>
        </div>

		<div class="section category">
			<div><div class="label">Category:</div>  <asp:CustomValidator id="cvCategories" Text="*" Display="Dynamic" ErrorMessage="You must select at least one category." runat="server" Enabled="False"/></div>                     
		    <div class="bump-right-15 bump-down-10">
                <asp:DataList runat="server" EnableViewState="True" ShowFooter="False" Width="100%"  
				    id="dlCategories" cellpadding="0" cellspacing="0" border="0"
				    RepeatColumns="4" RepeatDirection="Horizontal" onitemdatabound="dlCategories_OnDataBind" >
			        <ItemStyle HorizontalAlign="left"/>
			        <ItemTemplate>
				        <label><asp:CheckBox id="chkCategory" runat="server" Checked='<%#GetCatChecked (Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "CategoryId")))%>' onclick="javascript:categorySelected(this)"/><%# DataBinder.Eval(Container.DataItem, "CategoryName")%></label>
				        <input type="hidden" runat="server" id="hidCatId" value='<%#DataBinder.Eval(Container.DataItem, "CategoryId")%>' NAME="hidCatId" /> 
			        </ItemTemplate>
		        </asp:DataList>
            </div>
		</div>

        <hr class="divider" />

        <div class="section tabs">
            <div><div class="label">Profile Tabs:</div></div>   
			<div class="bump-right-15 bump-down-10">
                <div class="tablist">
				    <asp:checkboxlist id="cblTabs" repeatcolumns="2" runat="server"></asp:checkboxlist>
			    </div>
			    <div class="tabdefault">
				    <div>Set Default Tab:</div>
                    <div class="bump-right-15 bump-down-10">
				        <asp:radiobuttonlist id="rblDefaultTab" runat="server" clientidmode="Static"></asp:radiobuttonlist>
                    </div>
			    </div>		
		    </div>
        </div>
	
		<hr class="divider" />
		
        <div class="section backgroundOptions">
            <div class="label">Background (Optional)</div>
            <div class="bump-right-15 bump-down-10">
                <div>
			        Background Color<span style="padding-left:30px"></span><asp:hyperlink cssclass="colorPicker" id="txtBackgroundColorAppClrPickerStr" Runat="server" ImageUrl="~/images/colorpickmask.gif" CausesValidation="False"></asp:hyperlink>
			        <asp:textbox id="txtBackgroundColor" runat="server" MaxLength="8"></asp:textbox>
			    </div>
			    <div>
			        Background Photo URL<span style="padding-left:17px"></span><asp:textbox id="txtBGPicture" runat="server" MaxLength="255"></asp:textbox>
                    <span style="padding-left:6px"></span><a href="javascript:void(0);" runat="server" id="aRemove">Clear Photo URL</a>
                    <span style="padding-left:12px"></span><asp:linkbutton PostBackUrl="#library" id="selectPhoto" onclick="selectPhoto_Click" runat="Server" Text="Select from Library..." CausesValidation="False"></asp:linkbutton>
			    </div>
            </div>
			    <div id="libraryImages" runat="server">
			        <a name="library"></a>
			        <kaneva:LibraryImages id="libraryImageSelector" runat="server" Visible="false" />
			    </div>
        </div>

        <hr class="divider" />

        <div class="section communityURL">
            <div id="divRegistrationErr" style="MARGIN-TOP: 15px; COLOR: red" runat="server" visible="false"></div>
            <div><div class="label">Reserve Your World URL</div></div>
            <div class="bump-right-15 bump-down-10">
                <span id="spnRegistered" runat="server" visible="false">Your URL has already been registered.</span>
                <asp:label id="lblURL" runat="server"></asp:label>
                <asp:textbox id="txtURL" runat="server" maxlength="50" Width="230"></asp:textbox><br />			
			    <asp:regularexpressionvalidator id="revURL" runat="server" controltovalidate="txtURL" display="Dynamic" text="URL should be at least four characters and can contain only letters, numbers and underscore."></asp:regularexpressionvalidator>           
                <div id="urlWarning" runat="server" class="bump-down-10"><span class="warning">This short URL is permanent and cannot be changed after you reserve it.</span></div>
            </div>																		
       </div>

       <hr class="divider" />
		
        <div id="divAPSection" runat="server">
            <div class="section">
		        <div class="label">Access Pass</div>
                <div class="bump-right-15 bump-down-10">
			        <div><asp:checkbox id="cbIsAdult" runat="server" />Only users with Access Pass may visit the World.</div>
                    <div class="subLabel" id="apDisableReason" runat="server"></div>
                </div>
            </div>

            <hr class="divider" />
        </div>
		
        <div class="section">
		    <div class="label">Access Type</div>
            <div class="bump-right-15 bump-down-10">
                <div><asp:RadioButton groupname="rdoAccess" id="rdoOpen" runat="server" /> Open<br /><span class="subLabel">Anyone can go to the World.</span></div>
                <div><asp:RadioButton groupname="rdoAccess" id="rdoClosed" runat="server" /> Closed (Private)<br /><span class="subLabel">Only members can go to the World. Others must request access.</span></div>
                <div><asp:RadioButton groupname="rdoAccess" id="rdoOwnerOnly" runat="server" /> Only Owners<br /><span class="subLabel">Only owner and moderator can access.</span></div>
            </div>
        </div>
                           				
		<!-- SUBMIT BUTTONS -->
        <div class="submitContainer">
            <asp:linkbutton id="btnUpdate" runat="Server" onClick="btnUpdate_Click" CausesValidation="False" Text="Save" CssClass="btnxsmall grey gdatabutton"></asp:linkbutton>
			<asp:linkbutton id="btnCancel" runat="Server" CausesValidation="False" onClick="btnCancel_Click" Text="Cancel" CssClass="btnxsmall grey gdatabutton"></asp:linkbutton>
			<asp:linkbutton id="btnDelete" runat="Server" OnClientClick="return confirm('Are you sure you want to delete this World? The 3D World and World profile page will be deleted. This cannot be undone.');" onClick="btnDelete_Click" class="delete_link" CausesValidation="False" Text="Delete World"></asp:linkbutton>
		</div>
	</div>

</div>

<script type="text/javascript">
    <asp:literal id="litJS" runat="server"></asp:literal>

    jQuery(document).ready(function () {
        jQuery('#chkPhysics').change(function(){
            if(jQuery('#chkPhysics').prop( "checked" )) {
                jQuery('#physics').hide();    
            } else {
                jQuery('#physics').show();
            }
        });
    });
        
</script>
