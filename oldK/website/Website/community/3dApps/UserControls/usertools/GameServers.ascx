<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GameServers.ascx.cs" Inherits="KlausEnt.KEP.SiteManagement.GameServers" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>
<link href="../../css/base/base.css" type="text/css" rel="stylesheet">
<link href="../../css/Communities/GameServers.css" type="text/css" rel="stylesheet">


<div id="divKanevaHosted"  style="display: none">
    <br /><br /><br /><br /><br />
     This server is being hosted by Kaneva.
     <br /><br /><br /><br />
</div>

<div id="serverContainer">

	<div id="serverData">
		<div class="buttons">
			<asp:LinkButton cssclass="buttonFixed" id="lbn_NewServer" runat="server" causesvalidation="false" onclick="lbn_NewServer_Click"><span class="add">Add a Server</span></asp:LinkButton>
		</div>
		<ajax:ajaxpanel runat="server">
		<div id="divErrorServer" runat="server" class="error" style="display:none;"></div>
		</ajax:ajaxpanel>
		
		<!-- Server List -->
		<ajax:ajaxpanel id="ajpGameServers">	
			
			<asp:gridview id="dg_GameServers" runat="server" 
				onRowDataBound="dg_GameServers_RowDataBound" 
				onSorting="dg_GameServers_Sorting" 
				OnRowCreated="dg_GameServers_OnRowCreated" 
				OnRowDeleting="dg_GameServers_RowDeleting"  
				OnPageIndexChanging="dg_GameServers_PageIndexChanging" 
				OnRowEditing="dg_GameServers_RowEditing" 
				AllowSorting="False" borderstyle="None" borderwidth="0" cellspacing="0" cellpadding="0" PageSize="10" 
				AllowPaging="True" autogeneratecolumns="False" width="100%" cssclass="" gridlines="none">
							
				<RowStyle cssclass="row"></RowStyle>
				<HeaderStyle cssclass="header"></HeaderStyle>
				
				<Columns>
					<asp:BoundField HeaderText="Server ID" DataField="server_id" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
					<asp:BoundField HeaderText="Game ID" DataField="game_id" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
					<asp:BoundField HeaderText="Visibility ID" DataField="visibility_id" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
					<asp:BoundField HeaderText="Modifiers ID" DataField="modifiers_id" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
					<asp:BoundField HeaderText="Server" DataField="server_name" SortExpression="server_name" ReadOnly="True" ConvertEmptyStringToNull="False"  headerstyle-horizontalalign="left"></asp:BoundField>
					<asp:TemplateField HeaderText="Visible" headerstyle-horizontalalign="left">
						<ItemTemplate>
							<%#GetGameVisibiltyDescription(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "visibility_id")))%>
						</ItemTemplate>
					</asp:TemplateField>																	
					<asp:TemplateField HeaderText="Status" headerstyle-horizontalalign="left">
						<ItemTemplate>
							<%#GetServerStatusDescription(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "server_status_id")))%>
						</ItemTemplate>
					</asp:TemplateField>
				   <asp:BoundField HeaderText="Server Started" DataField="server_started_date" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
				   <asp:BoundField HeaderText="IP Address" DataField="ip_address" ReadOnly="True" ConvertEmptyStringToNull="False" headerstyle-horizontalalign="left"></asp:BoundField>
				   <asp:BoundField HeaderText="Port" DataField="port" ReadOnly="True" ConvertEmptyStringToNull="False" headerstyle-horizontalalign="left"></asp:BoundField>
				   <asp:BoundField HeaderText="Conc" DataField="number_of_players" SortExpression="number_of_players" ReadOnly="True" ConvertEmptyStringToNull="False" headerstyle-horizontalalign="left"></asp:BoundField>
				   <asp:BoundField HeaderText="Max Players" DataField="max_players" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
				   <asp:BoundField HeaderText="Last Ping" DataField="last_ping_datetime" SortExpression="last_ping_datetime" ReadOnly="True" ConvertEmptyStringToNull="False" headerstyle-horizontalalign="left" ></asp:BoundField>
				   <asp:BoundField HeaderText="Last Start" DataField="server_started_date" SortExpression="server_started_date" ReadOnly="True" ConvertEmptyStringToNull="False" headerstyle-horizontalalign="left"></asp:BoundField>
				   <asp:CommandField CausesValidation="False" ShowEditButton="True"></asp:CommandField>        
				   <asp:TemplateField>
					   <ItemTemplate>
						   | <asp:LinkButton id="lbn_confirmServerDelete" causesvalidation="false" Runat="server" OnClientClick="return confirm('Are you sure you want to delete this server?');" CommandName="Delete">Delete</asp:LinkButton>
					   </ItemTemplate>
				   </asp:TemplateField>
				</Columns>
			</asp:gridview>

			<!-- Edit Section -->
			<div id="divDetails" class="detailsContainer" runat="server" visible="false">
				<div id="divEditTitle" runat="server" class="title"></div>
				<div id="divError" runat="server" class="error" style="display:none;"></div>
				<div class="name">
					<span>Server Name:</span>
					<asp:TextBox ID="txtServerName" class="" runat="server"/>
					<asp:Label runat="server" style="color:red" id="serverNameRequired" visible="false">*</asp:Label>
				</div>
				<div class="port">
					<span>Port:</span>
					<asp:TextBox id="tbx_PortNumber" runat="server" class=""/>
					<asp:Label runat="server" style="color:red" id="portNumberRequired" visible="false">*</asp:Label>
				</div>
				<div class="visibility">
					<span>Visibility:</span>
					<asp:DropDownList class="" id="drpServerVisibility" runat="Server"/>
				</div>
				<div class="editButtons">
					<asp:button id="btnUpdateServer" runat="server" onClick="btnUpdateServer_Click" CausesValidation="False" Text="Save"/>
					<asp:button id="btnServerCancel" runat="Server" CausesValidation="False" onClick="btnServerCancel_Click" Text="Cancel"/>
				</div>
				<asp:Label id="hdn_ServerListIndex" visible="false" runat="server" />
				<asp:Label id="hdn_ServerID" visible="false" runat="server" />
				<asp:Label id="hdn_ServerStartedDate" visible="false" runat="server" />
				<asp:Label id="hdn_IPAddress" visible="false" runat="server" />
				<asp:Label id="hdn_NumberOfPlayers" visible="false" runat="server" />
				<asp:Label id="hdn_MaxPlayers" visible="false" runat="server" />
				<asp:Label id="hdn_LastPingDate" visible="false" runat="server" />
			</div>
		</ajax:ajaxpanel>
	</div>

	<!-- PATCH URL -->	
	<div id="patchData">
		<div class="buttons">
			<asp:LinkButton id="lbn_NewPatchURL" runat="server" cssclass="buttonFixed" causesvalidation="false" onclick="lbn_NewPatchURL_Click"><span class="add">Add PatchURL</span></asp:LinkButton>
		</div>
		
		<ajax:ajaxpanel id="ajpPatchURL" runat="server">
			<asp:gridview id="dg_PatchURLS" runat="server" 
				onRowDataBound="dg_PatchURLS_RowDataBound" 
				onsorting="dg_PatchURLS_Sorting" 
				OnRowCreated="dg_PatchURLS_OnRowCreated" 
				OnPageIndexChanging="dg_PatchURLS_PageIndexChanging" 
				OnRowEditing="dg_PatchURLS_RowEditing" 
				OnRowDeleting="dg_PatchURLS_RowDeleting" 
				AllowSorting="False" autogeneratecolumns="False" width="100%" cssclass="patchList"  
				border="0" cellpadding="0" cellspacing="0" PageSize="8" AllowPaging="True"
				gridlines="none">
							
				<RowStyle cssclass="row"></RowStyle>
				<HeaderStyle cssclass="header"></HeaderStyle>
				
				<Columns>
					<asp:BoundField HeaderText="URL Patch ID" DataField="patchURL_id" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
					<asp:BoundField HeaderText="Patch ID" DataField="patch_id" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
					<asp:BoundField HeaderText="Game ID" DataField="game_id" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
					<asp:BoundField HeaderText="Patch URL" DataField="patch_url" SortExpression="patch_url" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
					<asp:CommandField CausesValidation="False" ShowEditButton="True" itemstyle-cssclass="editPatch"></asp:CommandField>        
					<asp:TemplateField itemstyle-cssclass="deletePatch">
						<ItemTemplate>
							| <asp:LinkButton id="lbn_confirmPatchDelete" causesvalidation="false" Runat="server" OnClientClick="return confirm('Are you sure you want to delete this patch URL?');" CommandName="Delete">Delete</asp:LinkButton>
						</ItemTemplate>
					</asp:TemplateField>
				</Columns>
			</asp:gridview>

			<!-- Edit Section -->
			<div id="divPatchDetails" class="detailsContainer" runat="server" visible="false">
				<div id="divEditPatchTitle" runat="server" class="title"></div>
				<div id="divPatchError" runat="server" class="error" style="display:none;"></div>
				<div class="url">
					<span>Patch URL:</span>
					<asp:TextBox id="txtPatchURL" class="formKanevaText" runat="server"/>
					<asp:Label runat="server" style="color:red" id="patchURLRequired" visible="false">*</asp:Label>
				</div>
				<div class="editButtons">
					<asp:button id="btnUpdatePatchURL" runat="Server" onClick="btnUpdatePatchURL_Click" CausesValidation="False" Text="Save"/>
					<asp:button id="btnPatchURLCancel" runat="Server" CausesValidation="False" onClick="btnPatchURLCancel_Click" Text="Cancel"/>
				</div>
				
				<asp:Label id="hdn_PatchIndex" visible="false" runat="server" />
				<asp:Label id="hdn_patchURLID" runat="server" visible="false" />
				<asp:Label id="hdn_PatchId" visible="false" runat="server" />
				<asp:Label id="hdn_GameId" runat="server" visible="false" />
			</div>
		</ajax:ajaxpanel>
	</div>

</div>