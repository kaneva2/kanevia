<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GameTransactions.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.usercontrols.GameTransactions" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../../../../usercontrols/Pager.ascx" %>

<link href="../../css/Communities/GameTransactions.css" type="text/css" rel="stylesheet">
	
	<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js"></script>
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css" type="text/css" media="all" />

  <script>

  	$j(function () {
  		var trigger = "";
  		$j("#dialog").dialog({
  			autoOpen: false,
  			show: {
  				effect: "fade",
  				duration: 600
  			},
  			hide: {
  				effect: "fade",
  				duration: 600
  			},
  			resizable: false,
  			height: 230,
  			width: 475,
  			draggable: false,
  			modal: true,
  			buttons: [
				{
					text: "Cancel",
					"class": 'button-medium button-cancel',
					click: function () {
						$j(this).dialog("close");
					}
				},
				{
					text: "Redeem",
					"class": 'button-medium button-redeem',
					click: function () {
						trigger = "redeem";
						$j(this).dialog('close');
					}
				}
			],
  			close: function (event) {
  				if (trigger == "redeem")
  					$j("#btnDoRedeem").click();
  			},
  			open: function () {
  				$j(this).parent().find('button:nth-child(2)').focus();	
  			}
  		});

  		$j("#btnRedeem").click(function () {
			<asp:literal id="litRedeemClick" runat="server"></asp:literal>
  		});

  	});
  </script>
  <style>  
	.ui-dialog {padding:0;border-radius:0;border-width:2px;}
	.ui-dialog-titlebar {background-image:none;border-radius:0;border:none;height:26px;text-align:center;}
	.ui-dialog .ui-dialog-title {margin:auto;float:none;font-size:14px;display:inline-block;padding-top:4px;}
	.ui-dialog-titlebar-close {display:none;}
	.ui-widget-content {background-image:none;}
	.ui-dialog .ui-dialog-buttonpane {border-top:none;text-align:center;padding:.3em 0 1em 0;}
	.ui-dialog .ui-dialog-buttonpane .ui-dialog-buttonset {float:none;margin:auto;}
	.ui-widget-overlay {background-color:#000;background-image:none;opacity:0.65;filter: Alpha(Opacity=65);}
	.ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default {border:none;}
	
	.ui-dialog .ui-dialog-buttonpane .button-medium {padding-top:0;text-decoration:none;height:28px;width:145px;padding:0;font-size:14px;font-weight:bold;text-align:center;}
	.ui-dialog .ui-dialog-buttonpane .button-medium:hover {background-position:-147px 0;text-decoration:none;}
	.ui-dialog .ui-dialog-buttonpane .button-medium:active {background-position:-294px 0;}
	.ui-dialog .ui-dialog-buttonpane .button-redeem {background:url(../../images/buttons/button_439x27_mediumOrange.png) no-repeat 0 0;color:#fff;text-shadow:1px 1px #dc924f;}
	.ui-dialog .ui-dialog-buttonpane .button-cancel {background:url(../../images/buttons/button_439x27_mediumGrey.png) no-repeat 0 0;color:#333;text-shadow:1px 1px #fff;}
	.ui-dialog .ui-dialog-buttonpane .ui-button .ui-button-text {display:inline;padding:0;position:relative;top:-2px;}
	.grey.btnmedium.disable:link {color:#999;background-position:-147px 0;cursor:default;}
	.grey.btnmedium.disable:hover {color:#999;background-position:-147px 0;cursor:default;}
	.transconfirm {width:93%;margin-top:8px;}
	.transconfirm tr td {font-size:13px;padding:6px 0;}
	.transconfirm td.amt {font-weight:bold;text-align:right;}
	.transconfirm tr.creditbalance td {padding-bottom:12px;}
	.transconfirm tr.redeemtotal {border-top:1px solid #999;}
	.transconfirm tr.redeemtotal td {padding-top:12px;font-weight:bold;}
	.pagerContainer a:link {font-size:11px;}
  </style>

<div id="dialog" title="Transaction Confirmation" runat="server" clientidmode="static" style="display:none;">
  <table id="tblRedeem" class="transconfirm" runat="server">
	<tr>
		<td>Available World Credits</td>
		<td class="amt"><span id="dialogAvailable" runat="server">0</span></td>
	</tr>
	<tr class="creditbalance">
		<td>Your Credit Balance</td>
		<td class="amt"><span id="dialogCredits" runat="server">0</span></td>
	</tr>
	<tr class="redeemtotal">
		<td>Your Credit Balance after Redeem</td>
		<td class="amt"><span id="dialogTotalAfterRedeem" runat="server">0</span></td>
	</tr>
  </table>			  
</div>

<div id="transContainer">
	
	<div class="title btmborder">Current Balances</div>

<asp:updatepanel id="upTrans" runat="server">
	<contenttemplate>

	<div id="currentBalance">
		<div class="credits">
			<div class="heading">Earned Credits</div>
			<div class="totalbalance" id="divTotalCredits" clientidmode="Static" runat="server"></div>
		</div>
		<div class="cash">
			<div class="heading">Available Credits</div>
			<div class="totalbalance" id="divTotalCash" clientidmode="Static" runat="server"></div>
		</div>
		<div class="redeem"><a href="javascript:void(0);" class="btnmedium grey" id="btnRedeem" runat="server" clientidmode="static">Redeem Credits</a></div>
	</div>

	<div class="info noasterisk">Pending credits are available for redemption 7 days after transaction. 70% of sales goes towards Earned Credits.
		<span style="display:none;">See <a href="#">Redemption Policy</a> for more information.</span></div>
	
	<span id="transContainer" runat="server">
		<asp:repeater id="rptTransactions" runat="server" OnItemDataBound="rptTransactions_ItemDataBound">
			<headertemplate>
				<div class="title">Transaction History</div>
				<table>
					<thead>
						<tr class="head">
							<th class="date">Date</th>
							<th>Purchaser</th>
							<th>Item</th>
							<th>Price</th>
							<th>Earned</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody>
			</headertemplate>
			<itemtemplate>
						<tr class="row">
							<td class="date"><%# Convert.ToDateTime(DataBinder.Eval(Container.DataItem, "PurchaseDate")).ToString("MM/dd/yyyy") %></td>
							<td class="buyer"><%# (DataBinder.Eval(Container.DataItem, "BuyerName")) %></td>
							<td class="name"><%# (DataBinder.Eval(Container.DataItem, "DisplayName")) %></td>
							<td class="price"><%# (DataBinder.Eval(Container.DataItem, "ItemPrice")) %></td>
							<td class="total"><%# GetUserRedemptionAmount(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "TotalPrice"))) %></td>
							<td class="status" runat="server" id="status"><span class="available">Redeemed</span></td>
						</tr>
			</itemtemplate>
			<footertemplate>
					</tbody>
				</table>
			</footertemplate>
		</asp:repeater>
		<div class="pagerContainer"><asp:Label runat="server" id="lblSearch" CssClass="results"></asp:Label>  <Kaneva:Pager runat="server" id="pgTop"></Kaneva:Pager></div>

		<asp:button id="btnDoRedeem" style="visibility:hidden;" runat="server" clientidmode="Static" text="" onclick="btnDoRedeem_Click" />
	</span>
	
	<p id="pNoTrans" class="nodata" runat="server" visible="false">To receive donations from others, you must place the Donation Box object from your Game System into your World.</p>
	
	</contenttemplate>
</asp:updatepanel>

</div>



