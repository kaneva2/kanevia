///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for commOrder.
	/// </summary>
	public class commOrder : MainTemplatePage
	{
		private void Page_Load(object sender, System.EventArgs e)
		{
			int communityId = Convert.ToInt32 (Request ["communityId"]);

			DataRow drCommunity = CommunityUtility.GetCommunity (communityId);
			DataTable dtCommunitySubs = CommunityUtility.GetCommunitySubscriptions (communityId, true);

			if (!IsPostBack)
			{
				// Default community subscription amount
				Double dSubscriptionAmount = Convert.ToDouble (dtCommunitySubs.Rows [0]["subscription_amount"]);

				// Titles
				lblAssetName.Text = "Purchase Subscription to (" + drCommunity ["name"].ToString () + ")";
				lblAssetName2.Text = drCommunity ["name"].ToString ();
				lblAssetDesc.Text = drCommunity ["description"].ToString ();

				// Populate subscriptions dropdown
				for (int i = 0; i < dtCommunitySubs.Rows.Count; i ++)
				{
					drpSubscription.Items.Add (new ListItem ("K" + dtCommunitySubs.Rows [i]["subscription_amount"].ToString () + " (1 " + dtCommunitySubs.Rows [i]["name"].ToString () + ")", dtCommunitySubs.Rows [i]["community_subscription_id"].ToString ()));
				}

				UpdateScreen (dSubscriptionAmount);
			}

			// Set up purchase button
			btnMakePurchase.CausesValidation = false;
			btnMakePurchase.Attributes.Add ("onclick", "javascript: if (typeof(Page_ClientValidate) == 'function') if (!Page_ClientValidate()){return false;};this.value='Please wait...';this.disabled = true;" + ClientScript.GetPostBackEventReference(this.btnMakePurchase, "", false));
		}

		/// <summary>
		/// Update the K-Point Dropdown
		/// </summary>
		/// <param name="dSubscriptionAmount"></param>
		private void UpdateScreen (Double dSubscriptionAmount)
		{
			Double dUserKPointTotal = GetUserPointTotal ();
			Double dUserKPointAfterPurchase = dUserKPointTotal - dSubscriptionAmount;

			Double dKpointsShortage = 0;
			bool bNeedToPurchaseKPoints = (dUserKPointAfterPurchase < 0);
			if (bNeedToPurchaseKPoints)
			{
				dKpointsShortage = Math.Abs (dUserKPointAfterPurchase);
			}
				
			lblKPointTotal.Text = KanevaGlobals.FormatKPoints (dUserKPointTotal);

			// Set up KPoint dropdown
			DataTable dtPointBuckets;
			if (dKpointsShortage > 0)
			{
                dtPointBuckets = StoreUtility.GetActivePromotions(Constants.CURR_KPOINT, dKpointsShortage);
			}
			else
			{
                dtPointBuckets = StoreUtility.GetActivePromotions(Constants.CURR_KPOINT, 0);
			}

			// If we didn't get any point buckets, make them purchase a custom amount
			if (dtPointBuckets.Rows.Count == 0)
			{
				DataRow drCustom = dtPointBuckets.NewRow ();
				drCustom ["display"] = KanevaGlobals.FormatKPoints (dKpointsShortage, false, false) + " (" + KanevaGlobals.FormatCurrency (dKpointsShortage / 100) + ")";
                drCustom["promotion_id"] = "999999";
				drCustom ["kei_point_amount"] = dKpointsShortage;
				drCustom ["dollar_amount"] = dKpointsShortage / 100;
				dtPointBuckets.Rows.Add (drCustom);
			}

			drpKPoints.Items.Clear ();
			drpKPoints.DataTextField = "display";
            drpKPoints.DataValueField = "promotion_id";
			drpKPoints.DataSource = dtPointBuckets;
			drpKPoints.DataBind ();

			// Is a credit card purchase required?
			if (bNeedToPurchaseKPoints)
			{
				// They have to buy something, so balance will be balance - amount + purchased kpoints
				lblKPointAfterPurchase.Text = KanevaGlobals.FormatKPoints (dUserKPointTotal - dSubscriptionAmount + Convert.ToDouble (dtPointBuckets.Rows [0]["kei_point_amount"]) + Convert.ToDouble (dtPointBuckets.Rows [0]["free_points_awarded_amount"]));
				lblCashRequired.Text = KanevaGlobals.FormatCurrency (Convert.ToDouble (dtPointBuckets.Rows [0]["dollar_amount"]));

				lblKPointTotal.ForeColor = System.Drawing.Color.Red;
			}
			else
			{
				// Add a Please Select a Package
				drpKPoints.Items.Insert (0, new ListItem ("Please Select a Package...", "0"));	

				// They are initially going to be on "Please Select a Package...", so label is just balance minus amount
				lblKPointAfterPurchase.Text = KanevaGlobals.FormatKPoints (dUserKPointAfterPurchase);
				lblCashRequired.Text = KanevaGlobals.FormatCurrency (0.0);

				lblKPointTotal.ForeColor = System.Drawing.Color.Blue;
			}

			// Buy Points label visibility
			lblBuyPoints.Visible = (!bNeedToPurchaseKPoints);
			lblInsufficientPoints.Visible = (bNeedToPurchaseKPoints);
		}

		/// <summary>
		/// The make purchase event handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnMakePurchase_Click (object sender, EventArgs e) 
		{
			int userId = GetUserId ();
			int communityId = Convert.ToInt32 (Request ["communityId"]);
			int communityMemberSubscriptionId = 0;

			// Attempting to make a purchase
			int communitySubscriptionId = Convert.ToInt32 (drpSubscription.SelectedValue);
			DataRow drCommSubscription = CommunityUtility.GetCommunitySubscription (communitySubscriptionId);
			DataRow drCommunity = CommunityUtility.GetCommunity (communityId);
			Double dSubscriptionAmount = Convert.ToDouble (drCommSubscription ["subscription_amount"]);

			Double dMpointBalance = UsersUtility.getUserBalance (userId, Constants.CURR_MPOINT);
			Double dKpointBalance = UsersUtility.getUserBalance (userId, Constants.CURR_KPOINT);

			// Validate amounts
			int selectedKPointBucket = Convert.ToInt32 (drpKPoints.SelectedValue);

			// Did they want to buy KPoints?
			if (selectedKPointBucket > 0)
			{
                DataRow drPointBucket = StoreUtility.GetPromotion(selectedKPointBucket);

				Double pointBucketAmount = 0;
				Double pointBucketFreeAmount = 0;
				string pointBucketKEIPoint = Constants.CURR_KPOINT;
				Double dollarAmount = 0;

				// No point bucket found here means we didn't have one big enough for this purchase
				// Custom amount
				if (drPointBucket == null)
				{
					Double dKpointsShortage = Math.Abs (GetUserPointTotal () - dSubscriptionAmount);

					// Charge the card for the amount needed
					pointBucketKEIPoint = Constants.CURR_KPOINT;
					pointBucketAmount = dKpointsShortage;
					pointBucketFreeAmount = 0;
					dollarAmount = dKpointsShortage / 100;

					// Validate and Charge credit card here
					ShowErrorOnStartup ("Need to implement custom payment amounts.");
					return;
				}
				else
				{
					pointBucketKEIPoint = drPointBucket ["kei_point_id"].ToString ();
					pointBucketAmount = Convert.ToDouble (drPointBucket ["kei_point_amount"]);
					pointBucketFreeAmount = Convert.ToDouble (drPointBucket ["free_points_awarded_amount"]);
					dollarAmount = Convert.ToDouble (drPointBucket ["dollar_amount"]);

					// Does the user have enough K-Points to purchase now?
					if (dSubscriptionAmount < (dMpointBalance + dKpointBalance))
					{
						// A a subscription record
						communityMemberSubscriptionId = StoreUtility.InsertCommunityMemberSubscription (userId, communityId, communitySubscriptionId, dSubscriptionAmount, (int) Constants.eCOMMUNITY_MEMBER_SUBSCRIPTION_STATUS.COMPLETED);

						// Go ahead and deduct the K-points now and purchase the Community.
						//StoreUtility.PurchaseAssetNow (userId, 0, 0, communityMemberSubscriptionId, dSubscriptionAmount, communityId, dMpointBalance, Convert.ToInt32 (drCommunity ["creator_id"]));
						PurchaseCommunity (userId, communityMemberSubscriptionId, (int) Constants.eORDER_STATUS.COMPLETED);

						// Validate and Charge credit card here
						Response.Redirect (ResolveUrl ("~/billing/makePurchase.aspx?pb=" + drpKPoints.SelectedValue));
						return;
					}
					else
					{
						// Pending subscription
						communityMemberSubscriptionId = StoreUtility.InsertCommunityMemberSubscription (userId, communityId, communitySubscriptionId, dSubscriptionAmount, (int) Constants.eCOMMUNITY_MEMBER_SUBSCRIPTION_STATUS.PENDING);

						// Insert a pending purchase transaction
						// Do the transaction only after they purchase enough K-Points
						int orderId = PurchaseCommunity (userId, communityMemberSubscriptionId, (int) Constants.eORDER_STATUS.PENDING_PURCHASE);

						// Validate and Charge credit card here
						Response.Redirect (ResolveUrl ("~/billing/makePurchase.aspx?pb=" + drpKPoints.SelectedValue + "&pCommOID=" + orderId));
						return;
					}
				}				
			}
			
			// They are not purchasing k-points from here down

			// A final check
			if (dSubscriptionAmount > (dMpointBalance + dKpointBalance))
			{
				throw new Exception ("Not enough points to purchase this item!!!!");
			}

			// Purchase the Community
			communityMemberSubscriptionId = StoreUtility.InsertCommunityMemberSubscription (userId, communityId, communitySubscriptionId, dSubscriptionAmount, (int) Constants.eCOMMUNITY_MEMBER_SUBSCRIPTION_STATUS.COMPLETED);
			PurchaseCommunity (userId, communityMemberSubscriptionId, (int) Constants.eORDER_STATUS.COMPLETED);

			// Make them a valid member
			CommunityUtility.UpdateCommunityMember (communityId, userId, (UInt32) CommunityMember.CommunityMemberStatus.ACTIVE);
	
			Response.Redirect ("commInFeat.aspx?communityId=" + communityId + "&ttCat=inCommunity&subtab=feat");
		}

		/// <summary>
		/// Purchase a community now
		/// </summary>
		private int PurchaseCommunity (int userId, int communityMemberSubscriptionId, int orderStatusId)
		{
			// Create a new order
			int orderId = StoreUtility.CreateOrder (userId, Request.UserHostAddress, orderStatusId);
			DataRow drOrder = StoreUtility.GetOrder (orderId);

			// Add the item
			int orderItemId = StoreUtility.InsertOrderItem (drOrder, 0, 1, "");

			// Add the subscription if needed
			StoreUtility.UpdateOrderItemCommunitySubscription (communityMemberSubscriptionId, orderItemId);

			return orderId;
		}

		/// <summary>
		/// GetUserPointTotal
		/// </summary>
		/// <returns></returns>
		private Double GetUserPointTotal ()
		{
			return UsersUtility.GetUserPointTotal (GetUserId ());
		}

		/// <summary>
		/// They changed the dropdown
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void drpSubscription_Change (Object sender, EventArgs e)
		{
			drpKPoints_Change (sender, e);
			DataRow drCommSubscription = CommunityUtility.GetCommunitySubscription (Convert.ToInt32 (drpSubscription.SelectedValue));
			Double dSubscriptionAmount = Convert.ToDouble (drCommSubscription ["subscription_amount"]);
			UpdateScreen (dSubscriptionAmount);
		}

		/// <summary>
		/// They changed the dropdown
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void drpKPoints_Change (Object sender, EventArgs e)
		{
			int selectedKPointBucket = Convert.ToInt32 (drpKPoints.SelectedValue);

			// Update the user's end point balance
			int communityId = Convert.ToInt32 (Request ["communityId"]);

			DataRow drCommunity = CommunityUtility.GetCommunity (communityId);
			DataTable dtCommunitySubs = CommunityUtility.GetCommunitySubscriptions (communityId, true);

			Double dUserKPointTotal = GetUserPointTotal ();
			DataRow drCommSubscription = CommunityUtility.GetCommunitySubscription (Convert.ToInt32 (drpSubscription.SelectedValue));
			Double dSubscriptionAmount = Convert.ToDouble (drCommSubscription ["subscription_amount"]);

			// Are they purchaseing any?
			if (selectedKPointBucket > 0)
			{
                DataRow drPointBucket = StoreUtility.GetPromotion(selectedKPointBucket);
				
				Double pointBucketAmount = 0;

				// No point bucket found here means we didn't have one big enough for this purchase
				if (drPointBucket == null)
				{
					// Charge the card for the amount needed
					pointBucketAmount = dUserKPointTotal - dSubscriptionAmount;
				}
				else
				{
					pointBucketAmount = Convert.ToDouble (drPointBucket ["kei_point_amount"]) + Convert.ToDouble (drPointBucket ["free_points_awarded_amount"]);
				}

				lblKPointAfterPurchase.Text = KanevaGlobals.FormatKPoints (dUserKPointTotal - dSubscriptionAmount + pointBucketAmount);
			}
			else
			{
				lblKPointAfterPurchase.Text = KanevaGlobals.FormatKPoints (dUserKPointTotal - dSubscriptionAmount);
			}

			SetDropDownIndex (drpKPoints, selectedKPointBucket.ToString ());
		}

		protected Label lblAssetName, lblAssetName2, lblAssetDesc;
		protected Label lblKPointTotal, lblKPointAfterPurchase, lblCashRequired;
		protected Label lblBuyPoints, lblInsufficientPoints;

		protected HtmlTable tblLogin, tblRegister;
		protected HtmlTable tblPurchase, tblRegisterSubmit;

		protected Button btnMakePurchase;

		protected DropDownList drpKPoints, drpSubscription;


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
