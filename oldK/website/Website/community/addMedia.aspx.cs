///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for addMedia.
	/// </summary>
	public class addMedia : StoreBasePage
	{
		/// <summary>
		/// Constructor
		/// </summary>
		protected addMedia () 
		{
			Title = "Add Media to a Community";
		}

		/// <summary>
		/// Page_Load
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Page_Load (object sender, System.EventArgs e)
		{
			if(Request.IsAuthenticated)
			{
				if(!GetRequestParams())
				{
					//invalid request params
					RedirectToHomePage ();
				}
			}
			else
			{
				Response.Redirect (this.GetLoginURL ());
			}

			//setup header nav bar
			if(_channelId == this.GetPersonalChannelId())
			{
				//user's own channel
                HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB._UPDATE_;
			}
			else
			{
				//navs go to home-my channels for all channels if the user is an admin
                HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB._UPDATE_;
			}


			if (!IsPostBack)
			{
				BindData();
			}


			DataRow drChannel = CommunityUtility.GetCommunity (_channelId);
			lblChannel.Text = drChannel ["name"].ToString (); 

			AddBreadCrumb (new BreadCrumb ("Add Media", GetCurrentURL (), "", 0));
		}

		private bool GetRequestParams()
		{
			bool retVal = true;
			try
			{
				if(Request ["communityId"] != null)
				{
					//get channelId and find the default page
					_channelId = Convert.ToInt32 (Request ["communityId"]);
				}
				else
				{
					_channelId = GetPersonalChannelId ();
				}
			}
			catch(Exception)
			{
				//invalid numbers
				retVal = false;
			}
			return retVal;
		}

		private void BindData()
		{
			DataTable dtAssets = StoreUtility.GetAssetsInChannel (KanevaWebGlobals.CurrentUser.CommunityId, true, true,
				0, "", "name", 1, Int32.MaxValue);

			rptAssets.DataSource = dtAssets;
			rptAssets.DataBind();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnUpdate_Click (object sender, EventArgs e) 
		{
			CheckBox chkEdit;
			HtmlInputHidden hidAssetId;

			int channelId = _channelId;

			// Server validation
			if (Page.IsValid) 
			{
				foreach (RepeaterItem rptChannel in rptAssets.Items)
				{
					chkEdit = (CheckBox) rptChannel.FindControl ("chkEdit");

					if (chkEdit.Checked)
					{
						hidAssetId = (HtmlInputHidden) rptChannel.FindControl ("hidAssetId");
						int assetId = Convert.ToInt32 (hidAssetId.Value);

						// Moderators are allowed to upload
						if(CommunityUtility.IsCommunityModerator (channelId, GetUserId()))
						{
							StoreUtility.InsertAssetChannel (assetId, channelId);
						}
						else
						{
							// Does the channel allow uploads?
							if (CommunityUtility.IsChannelPublishable (channelId))
							{
								StoreUtility.InsertAssetChannel (assetId, channelId);
							}
						}
					}
				}
			}

			DataRow drChannel = CommunityUtility.GetCommunity (_channelId);
			Response.Redirect (GetBroadcastChannelUrl (drChannel ["name_no_spaces"].ToString ()));
		}

		/// <summary>
		/// Cancel click
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnCancel_Click (object sender, EventArgs e) 
		{
			DataRow drChannel = CommunityUtility.GetCommunity (_channelId);
			Response.Redirect (GetBroadcastChannelUrl (drChannel ["name_no_spaces"].ToString ()));
		}
		
		private void rptAssets_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if(e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				Label lblMessage = (Label) e.Item.FindControl("lblMessage");
				Label lblChannels = (Label) e.Item.FindControl("lblChannels");
				Label lblTeaser = (Label) e.Item.FindControl("lblTeaser");
				
				int ownerId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "owner_id"));
				int statusId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "status_id"));
				string username = DataBinder.Eval(e.Item.DataItem, "username").ToString();

				//only asset owner and admin can edit the asset
				bool canEditAsset = ownerId == GetUserId() || IsAdministrator(); 
						
				if(ownerId != GetUserId() && !this.IsAdministrator())
				{
					//its a connected media
					lblMessage.Text = "Connected media from " + username;
				}

				lblMessage.Visible = !canEditAsset;

				HtmlAnchor hlStatus = (HtmlAnchor) e.Item.FindControl("hlStatus");

				if(ownerId != GetUserId())
				{
					hlStatus.Attributes.Add("onclick", 
						"javascript:window.open('" + ResolveUrl ("~/asset/publishStatus.aspx#ConnectedMedia','add','toolbar=no,width=600,height=450,menubar=no,scrollbars=yes,status=yes').focus();return false;"));
					hlStatus.InnerHtml = "Connected Media";
					
				}
				else
				{
					hlStatus.Attributes.Add("onclick", GetStatusLink (Convert.ToInt32 (DataBinder.Eval(e.Item.DataItem, "publish_status_id"))));
					hlStatus.InnerHtml = GetStatusText (Convert.ToInt32 (DataBinder.Eval (e.Item.DataItem, "publish_status_id")), 
						Convert.ToInt32 (DataBinder.Eval (e.Item.DataItem, "status_id")), Convert.ToInt32 (DataBinder.Eval (e.Item.DataItem, "asset_id")));
				}

				//load channels
				PagedDataTable pdtChannels = StoreUtility.GetChannelsSharingAsset (Convert.ToInt32(DataBinder.Eval (e.Item.DataItem, "asset_id")), KanevaWebGlobals.CurrentUser.HasAccessPass, false, "", 1, 3);

				string strChannels = "";

				for (int i = 0; i < pdtChannels.Rows.Count; i ++)
				{
					if (!pdtChannels.Rows [i]["community_id"].Equals (DBNull.Value) && !pdtChannels.Rows [i]["name"].Equals (DBNull.Value))
					{
						if (pdtChannels.Rows [i]["is_personal"].Equals (1))
						{
							strChannels += "<a href=\"" + GetPersonalChannelUrl (pdtChannels.Rows [i]["name_no_spaces"].ToString ()) + "\" title=\"" + pdtChannels.Rows [i]["name"].ToString () + "\">" + pdtChannels.Rows [i]["name"].ToString () + "</a>  ";
						}
						else
						{
							strChannels += "<a href=\"" + GetBroadcastChannelUrl (pdtChannels.Rows [i]["name_no_spaces"].ToString ()) + "\" title=\"" + pdtChannels.Rows [i]["name"].ToString () + "\">" + pdtChannels.Rows [i]["name"].ToString () + "</a>  ";
						}
					}
				}

				lblChannels.Text = strChannels;

				
				string teaser = DataBinder.Eval (e.Item.DataItem, "teaser") == null ? "" :
					DataBinder.Eval (e.Item.DataItem, "teaser").ToString();
				if(teaser.Length > 0)
				{
					lblTeaser.Text = TruncateWithEllipsis(teaser,200);
					lblTeaser.Visible = true;
				}
				else
				{
					lblTeaser.Text = teaser;
					lblTeaser.Visible = false;
				}
			}
		}

		/// <summary>
		/// Get the status link
		/// </summary>
		protected string GetStatusLink (int torrentStatus)
		{
			return "javascript:window.open('" + ResolveUrl ("~/asset/publishStatus.aspx#" + torrentStatus + "','add','toolbar=no,width=600,height=450,menubar=no,scrollbars=yes,status=yes').focus();return false;");
		}
		
		protected Repeater rptAssets;

		protected Label lblChannel;
		
		private int		_channelId;

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
			rptAssets.ItemDataBound += new RepeaterItemEventHandler (rptAssets_ItemDataBound);
		}
		#endregion
	}
}