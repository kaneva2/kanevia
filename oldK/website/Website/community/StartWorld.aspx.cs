///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Data;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using log4net;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using System.Collections.Generic;
using System.Xml;
using System.Text;
using System.Net;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace KlausEnt.KEP.Kaneva
{
    public partial class StartWorld : BasePage
    {
        #region Declarations

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private const UInt32 ERR_PARSING_FAILED = 0xFFFFFFFF;

        protected Literal litCheckName;

        #endregion


        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            // They must be logged in
            if (Request.IsAuthenticated)
            {
                //set the navigation
                SetNav();

                divData.Visible = true;

                if (!IsPostBack)
                {
                    // Don't allow going to this page if WoK is not installed
                    if (KanevaWebGlobals.CurrentUser.HasWOKAccount)
                    {
                        revTitle.ValidationExpression = Constants.VALIDATION_REGEX_CHANNEL_NAME;
                        revTitle.ErrorMessage = Constants.VALIDATION_REGEX_CHANNEL_NAME_ERROR_MESSAGE;

                        txtTitle.Focus();

                        // Bind the dropdown
                        BindWorldTemplates();

                        // Initialize the template preview
                        UpdateTemplatePreview(Convert.ToInt32(drpTemplate.Items[0].Value));
                    }
                    else
                    {
                        Response.Redirect("~/community/install3d.kaneva");
                    }
                }
            }
            else
            {
                Response.Redirect(GetLoginURL());
            }
        }
        #endregion


        #region Attributes

        private bool IS_ADMINISTRATOR
        {
            set
            {
                ViewState["isAdministrator"] = value;
            }
            get
            {
                if (ViewState["isAdministrator"] == null)
                {
                    ViewState["isAdministrator"] = false;
                }
                return (bool)ViewState["isAdministrator"];
            }
        }

        private string RETURN_URL
        {
            set
            {
                ViewState["returnURL"] = value;
            }
            get
            {
                if (ViewState["returnURL"] == null)
                {
                    ViewState["returnURL"] = "";
                }
                return ViewState["returnURL"].ToString();
            }
        }

        #endregion

        #region Helper Functions

        private void BindWorldTemplates()
        {
            int[] template_statuses;
            int[] templatePassGroups;

            if (IsAdministrator())
            {
                template_statuses = new int[] { Convert.ToInt32(WorldTemplate.WorldTemplateStatus.ACTIVE), Convert.ToInt32(WorldTemplate.WorldTemplateStatus.ADMIN_ONLY) };
                templatePassGroups = new int[] { 0, Configuration.AccessPassGroupID, Configuration.VipPassGroupID };
            }
            else
            {
                template_statuses = new int[] { Convert.ToInt32(WorldTemplate.WorldTemplateStatus.ACTIVE) };

                if (KanevaWebGlobals.CurrentUser.HasVIPPass)
                {
                    templatePassGroups = new int[] { 0, Configuration.AccessPassGroupID, Configuration.VipPassGroupID };
                }
                else if (KanevaWebGlobals.CurrentUser.HasAccessPass)
                {
                    templatePassGroups = new int[] { 0, Configuration.AccessPassGroupID };
                }
                else
                {
                    templatePassGroups = new int[] { 0 };
                }
            }

            drpTemplate.DataTextField = "Name";
            drpTemplate.DataValueField = "TemplateId";
            drpTemplate.DataSource = GetGameFacade.GetWorldTemplates(template_statuses, templatePassGroups, KanevaWebGlobals.CurrentUser.UserId, KanevaWebGlobals.CurrentUser.SignupDate, 1, 10);
            drpTemplate.DataBind();
        }

        private void ResetErrorMessages()
        {
            alertMsg.Visible = false;
            spnCheckUsername.InnerText = string.Empty;
            txtTitle.Attributes.Remove("class");
            txtDescription.Attributes.Remove("class");
        }

        private void SetPreviousPage()
        {
            string lastPage = null;
            try
            {
                lastPage = Request.UrlReferrer.AbsoluteUri.ToString();
            }
            catch (NullReferenceException)
            {
            }

            if (lastPage != null)
            {
                RETURN_URL = lastPage;
            }
        }

        private void SetNav()
        {
            // Set Nav
            ((GenericPageTemplate) Master).HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.CREATE;
            //((GenericPageTemplate)Master).HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.MY3DAPPS;
            //((GenericPageTemplate)Master).HeaderNav.SetNavVisible(((GenericPageTemplate)Master).HeaderNav.MyKanevaNav, 6);
        }

        /// <summary>
        /// Cancel Add/Update a community
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(RETURN_URL);
        }

        protected void bntCreate_Click(object sender, EventArgs e)
        {
            ResetErrorMessages();

            int errorCode = 0;
            string errorMsg = string.Empty;
            string title = Server.HtmlEncode(txtTitle.Text.Trim());
            string urlSafeTitle = Server.UrlEncode(title);
            string description = Server.HtmlEncode(txtDescription.Text.Trim());
            string urlSafeDescription = Server.UrlEncode(description);

            int communityId = 0;
            WorldTemplate template = GetGameFacade.GetWorldTemplate(Convert.ToInt32(drpTemplate.SelectedValue), true);

            if (ValidatePage() > 0)
            {
                UserExperimentGroup userExperimentGroup = GetExperimentFacade.GetUserExperimentGroupCommon(KanevaWebGlobals.CurrentUser.UserId, "376e2bf4-9167-11e6-84f2-a3cf223dc93b");
                if (userExperimentGroup != null && userExperimentGroup.GroupId.Equals("6bf3b789-9167-11e6-84f2-a3cf223dc93b"))
                {
                    // We're doing this the new-fangled asynchronous way! 
                    string token = GetGameFacade.CreateWorldFromTemplateAsync(KanevaWebGlobals.CurrentUser.UserId, title, description, template.TemplateId, 0, Global.RequestRabbitMQChannel());

                    // Start polling.
                    string pollingJS = "<script language=\"javaScript\">CheckWorldCreation('" + token + "', 10000, 30, 0);</script>";
                    ClientScript.RegisterStartupScript(Page.GetType(), "WoKLaunch", pollingJS);

                    divLoadingText.InnerHtml = "Please wait while your World is being created, this may take a few minutes...<br/><img src=\"../images/progress_bar.gif\">";
                    divData.Visible = false;
                }
                else
                {
                    communityId = GetGameFacade.CreateWorldFromTemplate(template, title, urlSafeTitle, description, urlSafeDescription, KanevaWebGlobals.CurrentUser,
                    IsAdministrator(), UsersUtility.IsUserBetatester(), KanevaGlobals.WokGameName, out errorCode, out errorMsg);

                    if (communityId > 0)
                    {
                        LogWorldCreation();

                        // Metrics request tracking
                        string requestId = GetUserFacade.InsertTrackingRequest(Constants.cREQUEST_TRACKING_REQUEST_TYPE_ID_WEB_WORLD_CREATION, KanevaWebGlobals.CurrentUser.UserId, 0, 0);
                        string requestTypeSentId = GetUserFacade.InsertTrackingTypeSent(requestId, Constants.cREQUEST_TRACKING_MESSAGE_TYPE_ID_WEBSITE, KanevaWebGlobals.CurrentUser.UserId);

                        // Trigger "Play Now" click, and configure button for backup
                        int gameId = GetGameFacade.GetGameIdByCommunityId(communityId);
                        string communityName = txtTitle.Text.Trim();

                        ConfigureCommunityMeetMe3D(btnGoToWorld, gameId, communityId, communityName, requestTypeSentId);

                        string launchJS = "<script language=\"javaScript\">" + StpUrl.GetPluginJS(Request.IsAuthenticated, KanevaWebGlobals.CurrentUser.HasWOKAccount, gameId, communityId, communityName, "", requestTypeSentId, KanevaWebGlobals.CurrentUser.UserId, false) + "</script>";
                        launchJS = launchJS.Replace("javascript:", "");
                        launchJS = launchJS.Replace("return false;", "");
                        launchJS = launchJS.Replace("return showNotAvailable", "showNotAvailable");
                        ClientScript.RegisterStartupScript(Page.GetType(), "WoKLaunch", launchJS);

                        divCreateSuccess.Visible = true;
                        commityBody.Visible = false;
                    }
                    else
                    {
                        ShowAlertMessage(errorMsg);
                        aLaunch.Disabled = true;
                    }
                }
            }
        }

        protected void drpTemplate_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearFormInputs();
            UpdateTemplatePreview(Convert.ToInt32(drpTemplate.SelectedItem.Value));
        }

        private void ClearFormInputs()
        {
            ResetErrorMessages();
            txtTitle.Text = string.Empty;
            txtDescription.Text = string.Empty;
        }

        private void UpdateTemplatePreview(int templateId)
        {
            // Get the template
            WorldTemplate template = GetGameFacade.GetWorldTemplate(templateId);

            // Set the image src
            imgTemplatePreview.Src = GetWorldTemplateImageURL(template.PreviewImagePathLarge);

            // Set the template description
            pTemplateDescription.InnerText = template.Description;

            litExtraData.Style.Add ("display", "none");
            if (template.IncubatorHosted)
            {
                litExtraData.Style.Add ("display", "block");
            }

            // Set template features
            rptTemplateFeatures.DataSource = template.FeatureList;
            rptTemplateFeatures.DataBind();

            // Show VIP Upsell
            if ((!KanevaWebGlobals.CurrentUser.HasVIPPass && !IsAdministrator()) && template.UpsellPassGroupId == Configuration.VipPassGroupID)
            {
                vipUpsell.Visible = true;
            }
            else
            {
                vipUpsell.Visible = false;
            }
        }

        /// <summary>
        /// Log page view in Google Analytics directly from the server.
        /// Based on "GA" function in WOK client lua code, Source/WokScripts/GameFiles/ClientScripts/GoogleAnalytics.lua.
        /// </summary>
        private void LogWorldCreation()
        {
            Random randomClass = new Random();

            // 1: Define request parameters
            string versionNumber    = "4.7.2";                                  // GA tracking code version number
            string pageURI          = "KanevaWeb";                                   // Page URI
            string gifUniqueId      = randomClass.Next(0x7fffffff).ToString();  // Unique ID generated for each GIF request to prevent caching of the GIF image.     
            int visitor             = Guid.NewGuid().GetHashCode();             // Random visitor value

            // 2: Build the request url to GA
            string url = "http://www.google-analytics.com/__utm.gif?";
			url += "utmwv="     + versionNumber;
            url += "&utmn="     + gifUniqueId;      
            url += "&utmhn="    + "worlds.kaneva.com";         
            url += "&utmr=-";
			url += "&utmp=/"    + Server.UrlEncode(pageURI);          
			url += "&utmac="    + KanevaGlobals.GoogleAnalyticsAccountWorlds;
            url += "&utmcc=__utma%3D999.999.999." + visitor + "." + DateTime.Now.GetHashCode() + ".1%3B";

            // 3: Make the web call to log the page view
            try
            {
                WebRequest request = HttpWebRequest.Create(url);
                request.Timeout = 1000;
                request.Headers.Add("Accept-Language", "EN-US");
                using (request.GetResponse())
                {
                    // Ignore the response, we don't care
                }
            }
            catch (Exception)
            {
                // Ignore the exception for now
            }
        }

        private int ValidatePage()
        {
            Page.Validate();
            if (!Page.IsValid)
            {
                aLaunch.Disabled = true;
                
                List<string> errorList = new List<string>();
                foreach (BaseValidator validator in Page.Validators)
                {
                    if(!validator.IsValid)
                    {
                        errorList.Add(validator.ErrorMessage);

                        if (validator.ControlToValidate == "txtTitle")
                        {
                            txtTitle.Attributes["class"] = "error";
                        }
                        else if (validator.ControlToValidate == "txtDescription")
                        {
                            txtDescription.Attributes["class"] = "error";
                        }
                    }
                }
                ShowAlertMessageList(errorList);

                return 0;
            }

            // Everything's good
            return 1;
        }

        private void ShowAlertMessage(string message)
        {
            alertMsg.InnerHtml = "Please correct the following error(s):<ul><li>" + message + "</li></ul>";
            alertMsg.Visible = true;
        }

        private void ShowAlertMessageList(List<string> messages)
        {
            alertMsg.InnerHtml = "Please correct the following error(s):<ul>";

            foreach (string msg in messages)
            {
                alertMsg.InnerHtml += "<li>" + msg + "</li>";
            }

            alertMsg.InnerHtml += "</ul>";
            alertMsg.Visible = true;
        }

        protected void txtTitle_TextChanged(object sender, EventArgs e)
        {
            revTitle.Validate();

            if (revTitle.IsValid)
            {
                if (txtTitle.Text.Trim() != string.Empty)
                {
                    if (GetCommunityFacade.IsCommunityNameTaken(false, txtTitle.Text.Trim().Replace(" ", ""), 0))
                    {
                        aLaunch.Disabled = true;
                        spnCheckUsername.InnerText = "Not available we recommend " + GetSuggestedName(txtTitle.Text.Trim());
                        spnCheckUsername.Attributes.Add("class", "failure");
                        txtTitle.Focus();
                    }
                    else
                    {
                        spnCheckUsername.InnerText = "Available";
                        spnCheckUsername.Attributes.Add("class", "success");
                    }
                }
            }
            else
            {
                aLaunch.Disabled = true;
                spnCheckUsername.InnerText = "Name must be 4-20 characters, and can contain upper or lower-case letters, spaces, numbers, dashes (-), and underscores (_) only.";
                spnCheckUsername.Attributes.Add("class", "failure");
            }

            txtDescription.Focus();
        }

        /// <summary>
        /// GetSuggestedName
        /// </summary>
        /// <param name="originalName"></param>
        /// <returns></returns>
        private string GetSuggestedName (string originalName)
        {
            int nameAddition = 1;

            while (GetCommunityFacade.IsCommunityNameTaken(false, originalName + nameAddition.ToString().Trim().Replace(" ", ""), 0))
            {
                nameAddition += 1;
            }

            return originalName + nameAddition.ToString(); 
        }

        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion

        protected void hfAsyncPollingResponder_ValueChanged(object sender, EventArgs e)
        {
            string result = hfAsyncPollingResponder.Value;
            if (!string.IsNullOrWhiteSpace(result))
            {
                JToken obj = JsonConvert.DeserializeObject<JToken>(result);
                string status = obj["Status"].ToString();
                string message = obj["Message"].ToString();

                if (status.Equals("Success"))
                    AsyncCreationComplete();
                else
                    AsyncCreationError(message);
            }
        }

        public void AsyncCreationComplete()
        {
            // Get the community Id
            int communityId = GetCommunityFacade.GetCommunityIdFromName(Server.HtmlEncode(txtTitle.Text.Trim()), false);

            LogWorldCreation();

            // Metrics request tracking
            string requestId = GetUserFacade.InsertTrackingRequest(Constants.cREQUEST_TRACKING_REQUEST_TYPE_ID_WEB_WORLD_CREATION, KanevaWebGlobals.CurrentUser.UserId, 0, 0);
            string requestTypeSentId = GetUserFacade.InsertTrackingTypeSent(requestId, Constants.cREQUEST_TRACKING_MESSAGE_TYPE_ID_WEBSITE, KanevaWebGlobals.CurrentUser.UserId);

            // Trigger "Play Now" click, and configure button for backup
            int gameId = GetGameFacade.GetGameIdByCommunityId(communityId);
            string communityName = txtTitle.Text.Trim();

            ConfigureCommunityMeetMe3D(btnGoToWorld, gameId, communityId, communityName, requestTypeSentId);

            string launchJS = "<script language=\"javaScript\">" + StpUrl.GetPluginJS(Request.IsAuthenticated, KanevaWebGlobals.CurrentUser.HasWOKAccount, gameId, communityId, communityName, "", requestTypeSentId, KanevaWebGlobals.CurrentUser.UserId, false) + "</script>";
            launchJS = launchJS.Replace("javascript:", "");
            launchJS = launchJS.Replace("return false;", "");
            launchJS = launchJS.Replace("return showNotAvailable", "showNotAvailable");
            ClientScript.RegisterStartupScript(Page.GetType(), "WoKLaunch", launchJS);

            divCreateSuccess.Visible = true;
            commityBody.Visible = false;
        }

        public void AsyncCreationError(string errorMsg)
        {
            ShowAlertMessage(errorMsg);
            aLaunch.Disabled = true;
        }
    }
}
