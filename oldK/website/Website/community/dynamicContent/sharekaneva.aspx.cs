///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Text;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using log4net;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;



namespace KlausEnt.KEP.Kaneva.community
{
    public class shareKaneva : System.Web.UI.Page 
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request["get"] != null)
            {
                dataSwitch(Request["get"]);
            }
            else
            {
                Response.Write("Data unavailable.");
            }                                    
        }

        protected void dataSwitch(string strGet)
        {            
            switch (strGet)
            {
                case "shareLbBasic":
                    shareLbBasic();
                    break;

                case "shareLb":                    
                    shareLb();
                    break;

                case "badgeInfo":
                    badgeInfo();
                    break;

                default:
                    break;
            }

        }

        protected void badgeInfo()
        {
            try
            {
                if (Request.IsAuthenticated)
                {
                    Response.Write(KanevaWebGlobals.CurrentUser.UserId + "," + KanevaWebGlobals.CurrentUser.Username + ","
                        + KanevaWebGlobals.CurrentUser.FirstName + " " + KanevaWebGlobals.CurrentUser.LastName + ","
                        + KanevaWebGlobals.CurrentUser.CommunityId + "," 
                        + GetPersonalChannelUrl(KanevaWebGlobals.CurrentUser.NameNoSpaces).ToString() + ","
                        + UsersUtility.GetProfileImageURL(KanevaWebGlobals.CurrentUser.ThumbnailMediumPath ));
                }
                else
                {
                    Response.Write(" ");
                }
            }
            catch (Exception exc)
            {
                m_logger.Error("Error writing user info. ", exc);
            }
        }

        protected void shareLbBasic()
        {   
            try
            {
                DataTable dtLeaderboard = (DataTable)Global.Cache()["cShareLeaderboardBasic"];
                if (dtLeaderboard == null)
                {
                    MarketingFacade marketingFacade = new MarketingFacade();
                    dtLeaderboard = marketingFacade.GetShareLeaderboard("completed", 10);
                    Global.Cache().Insert("cShareLeaderboardBasic", dtLeaderboard, null, DateTime.Now.AddMinutes(15), System.Web.Caching.Cache.NoSlidingExpiration);                    
                }

                StringBuilder sb = new StringBuilder("<ol>\n");

                for (int i = 0; i < dtLeaderboard.Rows.Count; i++)
                {
                   sb.Append("<li>" + dtLeaderboard.Rows[i]["display_name"] + "</li>\n");                
                }
                sb.Append("</ol>\n");

                Response.Write(sb.ToString());
            }
            catch (Exception exc)
            {
                m_logger.Error("Error writing user info. ", exc);
            }
        }

        protected void shareLb()
        {
            DataTable dtLeaderboard = (DataTable)Global.Cache()["cShareLeaderboard"];
            if (dtLeaderboard == null)
            {
                MarketingFacade marketingFacade = new MarketingFacade();
                dtLeaderboard = marketingFacade.GetShareLeaderboard("completed", 50);
                
                // Add column to hold the rank
                DataColumn column = new DataColumn();
                column = new DataColumn();
                column.DataType = System.Type.GetType("System.String");
                column.AllowDBNull = false;
                column.ColumnName = "rank";
                column.DefaultValue = 0;
                dtLeaderboard.Columns.Add(column);

                // get the appropriate rank for each player
                int iRank = 1;

                for (int i = 0; i < dtLeaderboard.Rows.Count; i++)
                {
                    dtLeaderboard.Rows[i]["rank"] = iRank;
                    iRank++;
                }

                // Cache DataTable
                Global.Cache().Insert("cShareLeaderboard", dtLeaderboard, null, DateTime.Now.AddMinutes(15), System.Web.Caching.Cache.NoSlidingExpiration);
            }                                    


            StringBuilder sb = new StringBuilder("<table id=\"results\" >");
            sb.Append("<tr><th>Rank</th><th>Name</th><th>Clicks</th><th>Registered</th><th>Completed</th></tr>");
            for (int i = 0; i < dtLeaderboard.Rows.Count; i++)
            {
                sb.Append("<tr>");
                sb.Append("<td>" + dtLeaderboard.Rows[i]["rank"] + "</td>");
                sb.Append("<td><a href=\"" + GetPersonalChannelUrl(dtLeaderboard.Rows[i]["name_no_spaces"].ToString()).ToString() + "\">");
                sb.Append("<img src=\"" + UsersUtility.GetProfileImageURL(dtLeaderboard.Rows[i]["thumbnail_small_path"].ToString()) + "\" /></a> ");
                sb.Append("<a href=\"" + GetPersonalChannelUrl(dtLeaderboard.Rows[i]["name_no_spaces"].ToString()).ToString() + "\">");
                sb.Append( dtLeaderboard.Rows[i]["display_name"] + "</a></td>");
                sb.Append("<td>" + dtLeaderboard.Rows[i]["clicks"] + "</td>");
                sb.Append("<td>" + dtLeaderboard.Rows[i]["registered"] + "</td>");
                sb.Append("<td>" + dtLeaderboard.Rows[i]["completed"] + "</td>");
                sb.Append("</tr>");
            }
            sb.Append("</table>");

            Response.Write(sb.ToString());
        }
        
        protected string GetPersonalChannelUrl(string nameNoSpaces)
        {
            return KanevaGlobals.GetPersonalChannelUrl(nameNoSpaces);
        }

        public string GetProfileImageURL(string imagePath, string defaultSize, string gender)
        {
            return UsersUtility.GetProfileImageURL(imagePath, defaultSize, gender);
        }

        #region Declarations

        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion

    }
}