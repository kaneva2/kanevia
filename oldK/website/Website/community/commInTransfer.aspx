<%@ Page language="c#" Codebehind="commInTransfer.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.commInTransfer" %>

<link href="../css/home.css" rel="stylesheet" type="text/css" />		
<link href="../css/kanevaSC.css" rel="stylesheet" type="text/css"/>
<link href="../css/friends.css" rel="stylesheet" type="text/css"/>
<link href="../css/kanevaText.css" type="text/css" rel="stylesheet">
<link href="../css/kaneva.css" type="text/css" rel="stylesheet">

<link href="../css/new.css" rel="stylesheet" type="text/css" />

<table border="0" cellspacing="0" cellpadding="0" class="newcontainer" align="center">
	<tr>
		<td>
			<table  border="0" cellpadding="0" cellspacing="0" width="100%" class="newcontainerborder">
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
				<tr>
					<td class=""><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img2"/></td>
					<td valign="top" class="newdatacontainer">


						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
									<div id="pageheader">
										<table cellpadding="0" cellspacing="0" border="0" width="99%">
											<tr><td align="left"><h1>Transfer Community</h1></td></tr>
										</table>
									</div>
								</td>
							</tr>
							<tr>
								<td align="center"  valign="top">
									<table cellpadding="0" cellspacing="0" border="0"  width="98%">
										<tr>
											<td valign="top" align="center">
												<div class="module whitebg">
													<span class="ct"><span class="cl"></span></span>
													<h2>Are you sure you want to make another member the owner?</h2>
													
														<table border="0" cellspacing="0" cellpadding="8" width="98%">
															<tr>
																<td align="center">
																	<br>
																	<table cellpadding="0" cellspacing="0" border="0">
																		<tr>
																			<td>
																				<ul>
																				<h2 style="background: #FFFFFF;padding-left:0px;">Transfering ownership will result in the following:</h2><br/>
																				1) Loss of any future royalties earned by this community<br>
																				2) Loss of control of community<br>
																				3) This action can not be reversed except by the new community owner<br>
																				4) You would become a community member with only membership rights<br>
																				</ul>
																			</td>
																		</tr>	
																	</table>	
																</td>
															</tr>
															<tr>
																<td align="right"><asp:button id="btnCancel" runat="Server" onclick="btnCancel_Click" class="Filter2" causesvalidation="False" text="       cancel       "/>&nbsp;&nbsp;<asp:button id="btnUpdate" runat="Server" onclick="btnUpdate_Click" class="Filter2" causesvalidation="True" text="  transfer ownership  "/>&nbsp;&nbsp;&nbsp;</td>
															</tr>
														</table>
															
													<span class="cb"><span class="cl"></span></span>
												</div>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>

 						
					</td>
					<td class=""><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img3"/></td>
				</tr>
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
			</table>
		</td>
	</tr>
</table>




	
