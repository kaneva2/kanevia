<%@ Page language="c#" Codebehind="thumbnailUpload.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.thumbnailUpload" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head>
    <title>Upload Thumbnail</title>
    <link rel="stylesheet" href="../css/Kaneva.css" type="text/css" /> 
    <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
  </head>
	<body OnLoad="self.focus(); document.forms.frmMain.btnUpload.disabled=true;" style="background-color:#ffffff;" topmargin="0" leftmargin="0">
	<img runat="server" src="~/images/head/logok.gif" border="0" ID="Img3">
	<table width="100%" border="0" align="center" cellpadding="1" cellspacing="0" style="background-color:#ffffff;" >
	<form runat="server" method="post" enctype="multipart/form-data" name="frmMain" id="frmMain">
	<tr> 
	<td width="100%">
		<br />
		<table width="100%" border="0" cellspacing="0" cellpadding="1">
		<tr> 
		<td width="20%" align="right" class="bodyText">Photo:&nbsp;&nbsp;</td>
		<td width="80%"><input runat="server" class="Filter2" ID="inpThumbnail" type="file" size="45" onFocus="document.forms.frmMain.btnUpload.disabled=false;" onClick="document.forms.frmMain.btnUpload.disabled=false;" />
			</td>
		</tr>
		<tr align="center"> 
		<td colspan="2" class="bodyText" align="center">Photos must be of the jpg, jpeg, gif<br/>and have a maximum file size of 200KB</td>
		</tr>
		</table>
		</td>
	</tr>
	<tr align="right"> 
	<td><br/><asp:button id="btnUpload" runat="Server" onClick="btnUpload_Click" class="Filter2" Text=" Upload "/>&nbsp;<input type="button" class="Filter2" name="cancel" value=" Cancel " onClick="window.close()">&nbsp;&nbsp;&nbsp;<br/><br/></td>
	</tr>
	</form>
	</table>
	</body>
</html>
