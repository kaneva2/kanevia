<%@ Page language="c#" Codebehind="noAccess.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.noAccess" %>
<%@ Register TagPrefix="Kaneva" TagName="HeaderText" Src="../usercontrols/HeaderText.ascx" %>

<Kaneva:HeaderText runat="server" Text="Whoops! You can't access this World because..."/>
<br>
<center>
<table cellpadding="20" cellspacing="1" border="0" width="730">
	<tr>
	<td class="bodyText" align="left"><br>
	
	You may not be able to access this World because one or more of the following conditions apply:
	<br><br> 
	<ul>
		<asp:Label id="lblAdult" runat="server">
			<li> You�re visiting a restricted World without first logging into Kaneva.<br>
			<i> Use your browser�s Back button to go to the sign in page and sign in, or <a runat="server" id="joinLink">join Kaneva now!</a></i>
			<br><br>
		</asp:Label>
		<asp:Label id="lblAdultYoung" runat="server">
			<li> You�re visiting a restricted World and you are not of age.<br>
			<i>There�s lots of other great things to browse at Kaneva, or wait until you�re old enough.</i>
			<br><br>
		</asp:Label>
		<asp:Label id="lblPrivate" runat="server">
			<li> This is a members-only World and you are not one of its members.<br>
			<i> You must first request to join the World and get approval from the World owner or moderator.</i>
			<br><br>
		</asp:Label>
			<asp:Label id="lblModerator" Visible="False" runat="server">
			<li> You're visiting a World page that requires moderator or owner privileges.<br>
			<i> You must be a World moderator or owner to access this page.</i>
			<br><br>
		</asp:Label>
	</ul>
	<br><br><br>
	
	<span class="dateStamp">NOTE: If you believe you are receiving this page in error, please contact support via the <a runat="server" href="~/suggestions.aspx?mode=F&category=Feedback" class="showingCount" ID="A1">Support Page</a></span>
	<br><br><br>
	
		<div align="center"><asp:button id="btnOK" runat="Server" onClick="btnOK_Click" class="Filter2" Text="      back      "/></div>
	
	</td>
	</tr>
</table>
</center>
