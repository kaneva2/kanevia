<%@ Page language="c#" Codebehind="inviteMember.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.inviteMember" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>

<link href="../css/home.css" rel="stylesheet" type="text/css" />		
<link href="../css/kanevaSC.css" rel="stylesheet" type="text/css"/>
<link href="../css/friends.css" rel="stylesheet" type="text/css"/>
<link href="../css/kanevaText.css" type="text/css" rel="stylesheet">

<link href="../css/new.css" rel="stylesheet" type="text/css" />

<script src="../jscript/prototype.js" type="text/javascript" language="javascript"></script>


<table border="0" cellspacing="0" cellpadding="0" class="newcontainer" align="center">
	<tr>
		<td> 
			<table  border="0" cellpadding="0" cellspacing="0" width="100%" class="newcontainerborder">
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
				<tr>
					<td class=""><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img21"/></td>
					<td valign="top" class="newdatacontainer">  
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
								
									<!-- TOP STATUS BAR -->
									<div id="pageheader">
										<table cellpadding="0" cellspacing="0" border="0" width="99%">
											<tr>
												<td align="left">
													<h1>Invite a Member to Join <span id="spnChannelName" runat="server"></span></h1>
												</td>
												
												<td align="right" valign="middle">
													
												</td>
											</tr>
										</table>
									</div>
									<!-- END TOP STATUS BAR -->	
								
								</td>
							</tr>
							<tr>
								<td width="968" align="left"  valign="top">
									<table cellpadding="0" cellspacing="0" border="0"  width="968">
										<tr>
											<td width="250" valign="top">
												<div class="module toolbar">
													<span class="ct"><span class="cl"></span></span>
													<h2>Tools & Actions <span class="hlink"></span></h2>
													<table cellspacing="0" cellpadding="10" border="0" width="99%">
														<tr>
															<td align="center">
											
																<table cellspacing="0" cellpadding="3" border="0" width="100%">
																	<tr>
																		<td>View Member List:</td>
																		<td valign="bottom">
																			<asp:imagebutton imageurl="~/images/button_go.gif" alt="Fo" width="57" height="23" border="0" onclick="btnGo_Click" id="btnGo" runat="server" causesvalidation="false"></asp:imagebutton>
																		</td>
																	</tr>
																</table>				  
															</td>
														</tr>
														
													</table>
										 				
													<span class="cb"><span class="cl"></span></span>
												</div>
											</td>
											<td width="20"><img runat="server" src="~/images/spacer.gif" width="20" height="1" id="Img22"/></td>
											<td width="698" valign="top" align="center">
												
													<div class="module whitebg">
													<span class="ct"><span class="cl"></span></span>
													
													<ajax:ajaxpanel id="ajpMessageArea" runat="server">
													<table border="0" cellspacing="0" cellpadding="0" width="99%" height="23">
														<tr>																			  
															<td align="left" valign="bottom"><h2 class="alertmessage" style="background:none;padding-top:6px;">
																<span id="spnAlertMsg" runat="server"></span></h2>						
															</td>
															<td align="right" style="padding-right: 10px;"><asp:imagebutton id="btnSend_Top" imageurl="~/images/button_sendnow.gif" onclick="btnSendNow_Click" alternatetext="Send Now!" width="77" height="23" border="0" runat="server"/></td>
														</tr>
													</table>
													</ajax:ajaxpanel>	
													<hr /><br />
													
													<div align="left" style="margin-left:5px;">
														<br/>
														<table width="98%" border="0" cellspacing="0" cellpadding="8">
															<tr>
																<td align="right" nowrap class="insideBoxText11"><strong>Subject:</strong></td>
																<td><img runat="server" src="~/images/spacer.gif" width="1" height="6" /></td>
																<td align="left"><asp:label cssclass="insideBoxText11" id="lblSubject" runat="server"/></td>
															</tr>
															<tr>
																<td align="right" nowrap class="insideBoxText11"><strong>From:</strong></td>
																<td><img runat="server" src="~/images/spacer.gif" width="1" height="6" id="Img1"/></td>
																<td align="left"><asp:label cssclass="insideBoxText11" id="lblFrom" runat="server"/></td>
															</tr>
															<tr>
																<td align="right" nowrap class="insideBoxText11"><strong><span style="color: red;">*</span>&nbsp;To:</strong></td>
																<td><img runat="server" src="~/images/spacer.gif" width="1" height="6" id="Img2"/></td>
																<td align="left"><asp:textbox id="txtTo" cssclass="insideBoxText11" width="320px" maxlength="300" runat="server"/>
																	<br>(Email addresses separated by spaces, commas or semicolons)</td>
															</tr>
															<tr>
																<td align="right" valign="top" class="insideBoxText11"><strong>Optional <br/>Personal Message:</strong></td>
																<td><img runat="server" src="~/images/spacer.gif" width="1" height="6" id="Img3"/></td>
																<td align="left"><asp:textbox id="txtMessage" textmode="multiline" rows="5" cssclass="insideBoxText11" width="470" maxlength="100" runat="server"/></td>
															</tr>
															<tr>
																<td align="right" valign="top" nowrap class="insideBoxText11"><strong>Message:</strong></td>
																<td><img runat="server" src="~/images/spacer.gif" width="1" height="6" id="Img4"/></td>
																<td align="left"><asp:label cssclass="insideBoxText11" id="lblMessage" runat="server" /></td>
															</tr>
															<tr>			 
																<td colspan="3" align="right">
																	<ajax:ajaxpanel id="ajpBottomSendButton" runat="server">
																		<asp:imagebutton id="btnSend_Bottom" imageurl="~/images/button_sendnow.gif" onclick="btnSendNow_Click" alternatetext="Send Now!" width="77" height="23" border="0" runat="server"/> 
																	</ajax:ajaxpanel>
																</td>
															</tr>
														</table>			 

													</div>
														
													<span class="cb"><span class="cl"></span></span>
												</div>
												
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td class=""><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img24"/></td>
				</tr>
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
			</table>
		</td>
	</tr>
</table>



