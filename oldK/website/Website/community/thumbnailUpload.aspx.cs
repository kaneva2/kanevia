///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Drawing;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

using System.ComponentModel;
using System.Data;
using System.Web;
using System.IO;
using log4net;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for thumbnailUpload.
	/// </summary>
	public class thumbnailUpload : System.Web.UI.Page
	{
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnUpload_Click (object sender, EventArgs e) 
		{
            CommunityFacade communityFacade = new CommunityFacade();
			int communityId = Convert.ToInt32 (Request ["communityId"]);

			if (!CommunityUtility.IsCommunityModerator (communityId, KanevaWebGlobals.GetUserId (), true))
			{
				ShowErrorOnStartup ("You must be a moderator to change the photo!");
				return;
			}

            // Files are now stored in content repository
            string contentRepository = "";
            if (KanevaGlobals.ContentServerPath != null)
            {
                contentRepository = KanevaGlobals.ContentServerPath;
            }
            else
            {
                ShowErrorOnStartup("No Content server path (content repository)");
                return;
                //Response.Write("No Content server path (content repository)");
            }

            //declare the file name of the image
            string filename = null;

            //create PostedFile
            HttpPostedFile File = inpThumbnail.PostedFile;
            
            try
            {
                //generate newpath
                string newPath = Path.Combine(contentRepository, KanevaWebGlobals.GetUserId().ToString());

                //upload image from client to image repository
                ImageHelper.UploadImageFromUser(ref filename, File, newPath, KanevaGlobals.MaxUploadedImageSize);

                if (Request["share"] == null)
                {
                    //generate newpath and channel id
                    string imagePath = newPath + Path.DirectorySeparatorChar + filename;
                    communityFacade.UpdateCommunity(communityId, 0, false, imagePath, File.ContentType, true);

                    //create any and all thumbnails needed
                    ImageHelper.GenerateAllCommuntyThumbnails(filename, imagePath, contentRepository, KanevaWebGlobals.GetUserId(), communityId);

                    // If successfull, send javascript to update mykaneva page, close the window
                    if (!Page.ClientScript.IsClientScriptBlockRegistered(GetType(), "startup"))
                    {
                        DataRow drChannel = CommunityUtility.GetCommunity(communityId);

                        if (drChannel["thumbnail_large_path"].Equals(DBNull.Value))
                        {
                            drChannel = CommunityUtility.GetCommunity(communityId, Constants.eCOMMUNITY_STATUS.NEW);
                        }

                        string javascript = "<script language=JavaScript>window.opener.document.getElementById(\"imgThumbnail\").src='" + CommunityUtility.GetBroadcastChannelImageURL(drChannel["thumbnail_large_path"].ToString(), "la") + "';window.close()</script>";
                        Page.ClientScript.RegisterStartupScript(GetType(), "startup", javascript);
                    }
                }
                else
                {
                    // This is the above, except without updating a community.
                    string strImgToReplace = Request["share"].ToString();
                    
                    //generate newpath and channel id
                    string imagePath = newPath + Path.DirectorySeparatorChar + filename;

                    string fileStoreHack = "";
                    if (KanevaGlobals.FileStoreHack != "")
                    {
                        fileStoreHack = KanevaGlobals.FileStoreHack;
                    }

                    // If successfull, send javascript to update mykaneva page, close the window
                    if (!Page.ClientScript.IsClientScriptBlockRegistered(GetType(), "startup"))
                    {

                        string javascript = "<script language=JavaScript>window.opener.document.getElementById(\""+ strImgToReplace +"\").src='"+ KanevaGlobals.ImageServer + "/" + fileStoreHack + communityId + "/" + filename +"';window.close()</script>";
                        Page.ClientScript.RegisterStartupScript(GetType(), "startup", javascript);
                    }
                }


            }
            catch (Exception ex)
            {
                ShowErrorOnStartup(ex.Message);
            }
        }


		/// <summary>
		/// Show an error message on startup
		/// </summary>
		/// <param name="errorMessage"></param>
		protected void ShowErrorOnStartup (string errorMessage)
		{
			string scriptString = "<script language=JavaScript>";
            scriptString += "alert ('" + errorMessage + "');";
            //scriptString += "alert ('Please correct the following errors:\\n\\n" + errorMessage + "');";
            scriptString += "</script>";

            if (!Page.ClientScript.IsClientScriptBlockRegistered(GetType(), "ShowError"))
			{
                Page.ClientScript.RegisterStartupScript(GetType(), "ShowError", scriptString);
			}
		}

		protected HtmlInputFile inpThumbnail;

		// Logger
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}

