<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>
<%@ Register TagPrefix="Kaneva" TagName="HeaderText" Src="../usercontrols/HeaderText.ascx" %>
<%@ Page language="c#" ValidateRequest="False" Codebehind="commEdit.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.commEdit" %>

<link href="../css/home.css" rel="stylesheet" type="text/css">
<link href="../css/kanevaSC.css" rel="stylesheet" type="text/css">
<link href="../css/friends.css" rel="stylesheet" type="text/css">
<link href="../css/new.css" type="text/css" rel="stylesheet">
                                                  
<!-- Main Container -->
<table border="0" cellspacing="0" cellpadding="0"  width="990" align="center">
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" /></td>
	</tr>
	<tr>
		<td>
			<table  border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="frTopLeft"></td>
					<td class="frTop"></td>
					<td class="frTopRight"></td>
				</tr>				
				<tr>
					<td class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
					<td valign="top" class="frBgIntMembers">
					<!-- Start Content -->			
																															
						<ajax:ajaxpanel ID="Ajaxpanel1" runat="server">
					   						
						<span style="color:#13667a;font-size:15px;font-weight:700;padding:5px 0px 5px 70px;display:block;">
						Communities allow you to express your interests and connect with people who share your passions.<br/>
						</span>
																		   
						<div style="width:100%;text-align:center;padding:5px 0 10px 0;">
						<asp:ValidationSummary ShowMessageBox="False" ShowSummary="True" cssclass="errBox black" id="valSum" DisplayMode="BulletList"
						runat="server" HeaderText="<br/><br />Please correct the following errors:" width="60%" />
						<span id="spnAlertMsg" runat="server" class="errBox black" style="display:none;width:60%;"></span>
						</div>
																		  
						<!-- Custom URL -->
						<!-- Start Container Wrapper -->
						<table border="0" cellspacing="0" cellpadding="0" width="100%">
							<tr id="trURLTitle" visible="false" runat="server">
								<td> 
									<table border="0" cellspacing="0" cellpadding="0" width="99%">
										<tr>
											<td valign="top" align="center">
												<table border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td width="850" >
															<div class="module whitebg" >
																<span class="ct"><span class="cl"></span></span>
																<!-- START CONTENT -->
																<h2>Reserve a Web address (URL) for your community</h2>
																<table width="800" border="0">
 																	<tr id="trURLBody" visible="false" runat="server">
																		<td>
																			<SPAN id="spnRegistered" runat="server" visible="false">Your URL has already been registered.</span>
																			<br />
																			Create your own Web address for your community by entering a name below:<br />

																			<asp:label id="lblURL" runat="server"></asp:label>
																			<asp:textbox class="biginput" id="txtURL" runat="server" maxlength="50" Width="230"></asp:textbox><br />

																			<asp:regularexpressionvalidator id="revURL" runat="server" controltovalidate="txtURL" display="Dynamic" text="URL should be at least four characters and can contain only letters, numbers and underscore."></asp:regularexpressionvalidator>

																			<DIV style="COLOR: red">Important: The Web address (URL) you enter is permanent. You cannot change it later.</DIV>

																			<asp:button id="btnRegister" onclick="btnRegister_Click" runat="Server" text=" Register URL "
																			causesvalidation="true" cssclass="Filter2"></asp:button>

																			<DIV id="divRegistrationErr" style="MARGIN-TOP: 15px; COLOR: red" runat="server" visible="false"></DIV>			
																		</td>
																	</tr>
																</table>

																<!-- END CONTENT -->																			
																<span class="cb"><span class="cl"></span></span>
															</div>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!-- End Container Wrapper -->
						<!-- End Custom URL -->


						<!-- Description Section -->
						<!-- Start Container Wrapper -->
						<table border="0" cellspacing="0" cellpadding="0" width="99%">
							<tr>
								<td valign="top" align="center">
									<table border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="850" >
												<div class="module" style="position:relative; z-index:1;">
													<span class="ct"><span class="cl"></span></span>
													<!-- START CONTENT -->
													<table border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td style="padding-left:10px;">

																<h2>Describe your community</h2>

																<table width="400" border="0" cellspacing="3" >
																	<tr>
																		<td  colspan="3">
																			<span style="line-height:16px;">Name <asp:RequiredFieldValidator id="rfItemName" runat="server" Display="Dynamic" ErrorMessage="Title is a required field."
																			Text="*" ControlToValidate="txtTitle"></asp:RequiredFieldValidator></span>
																			
																			<asp:RegularExpressionValidator id="revTitle" runat="server" Display="Dynamic" Text="*" ControlToValidate="txtTitle"
																				EnableClientScript="True"></asp:RegularExpressionValidator>
																			<br />
																			<asp:TextBox class="biginput" id="txtTitle" runat="server" Width="350" MaxLength="50"></asp:TextBox>
																			<br />
																		</td>
																	</tr>
																	<tr>
																		<td colspan="3">		
																			<span style="line-height:16px;">Brief Description <asp:RequiredFieldValidator id="rftxtDescription" runat="server" Display="Dynamic" ErrorMessage="Description is a required field."
																			Text="*" ControlToValidate="txtDescription"></asp:RequiredFieldValidator></span>
																			
																			<asp:TextBox class="biginput" id="txtDescription" runat="server" Width="350" Rows="4" TextMode="multiline"></asp:TextBox>
																			
																			<SPAN id="spItemNameMsg" style="FONT-SIZE: 11px; MARGIN: 3px 0px 0px 10px; COLOR: red" runat="server"></SPAN>
																		</td>
																	</tr>
																	<tr>
																		<td valign="top">
																			<span style="line-height:16px;">Category <asp:RequiredFieldValidator id="rfdrpCategory" runat="Server" InitialValue="" Display="Dynamic" Text="*" ControlToValidate="drpCategory"  EnableClientScript="True"></asp:RequiredFieldValidator></span>
																			<br />
																			<asp:DropDownList id="drpCategory" runat="Server" Width="150px"></asp:DropDownList>
																		</td>
																		<td>&nbsp;</td>
																		<td valign="top"></td>
																	</tr>
																	<tr>
																		<td valign="top">
																			<span style="line-height:16px;">Place <asp:RequiredFieldValidator id="rfdrpPlace" runat="Server" InitialValue="" Display="Dynamic" Text="*" ControlToValidate="drpPlace"  EnableClientScript="True"></asp:RequiredFieldValidator></span>
																			<br />
																			<asp:DropDownList id="drpPlace" runat="Server" Width="150px"></asp:DropDownList>
																		</td>
																		<td>&nbsp;</td>
																		<td valign="top"></td>
																	</tr>
																	<tr>
																		<td>
																			<span style="line-height:16px;">Tags (separated by spaces)</span>
																			<br />
																			<asp:TextBox class="biginput" id="txtTags" runat="server" Width="380" MaxLength="100"></asp:TextBox><br />
																			<asp:RequiredFieldValidator id="rftxtTags" runat="server" Display="Dynamic" ErrorMessage="Please provide at least one Tag in the Tags field." Text="*" ControlToValidate="txtTags"></asp:RequiredFieldValidator>
																			<asp:RegularExpressionValidator id="regtxtKeywords" runat="server" Display="Dynamic" Text="*" ControlToValidate="txtTags" EnableClientScript="True"></asp:RegularExpressionValidator>

																			<span style="line-height:16px;"><strong>What's a Tag?</strong> Tags are descriptive words that make finding a community easier. 
																			They describe what a community is about, like "cooking" or "photography."</span>
																		</td>
																	</tr>	
																</table>

															</td>
															<td valign="top">

																<!-- Photo Section -->
																<div class="module whitebg" >
																	<span class="ct"><span class="cl"></span></span>

																	<h2>Upload a community photo</h2>
				
																	<table width="400" border="0" style="padding-left:10px;">
																		<tr>
																			<td style="padding-left:10px;">
																				Upload a logo or photo that describes your community.<br /><br />

																				<INPUT class="Filter2" id="btnUploadThumbnail" type="button" value=" upload your own!" runat="server"></input>
																				&nbsp;
																				<asp:button id="btnClearThumb" onclick="btnClearThumb_Click" runat="Server" Text="clear" CssClass="Filter2" CausesValidation="False"></asp:button><br />

																				<span style="line-height:16px;">(Note: Photos should be saved using a JPG, JPEG or GIF format. We recommend using a photo that is sized 150x110.)</span><br /><br />

																				<DIV style="BORDER-RIGHT: #e0e0e0 1px solid; BORDER-TOP: #e0e0e0 1px solid; VERTICAL-ALIGN: middle; OVERFLOW: hidden; BORDER-LEFT: #e0e0e0 1px solid; WIDTH: 130px; BORDER-BOTTOM: #e0e0e0 1px solid; HEIGHT: 98px; BACKGROUND-COLOR: #ffffff; TEXT-ALIGN: center"><IMG id="imgThumbnail" border="0" runat="server"></DIV>

																				Image preview
																				<br /><br />
																			</td>
																		</tr>
																	</table>

																	<span class="cb"><span class="cl"></span></span>
																</div>
																<!-- End photo section -->

															</td>
														<tr>
													</table>
													<!-- END CONTENT -->																			
													<span class="cb"><span class="cl"></span></span>
												</div>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!-- End Container Wrapper -->


						<!-- Start Themes Section -->
						<!-- Start Container Wrapper -->
						<table border="0" cellspacing="0" cellpadding="0" width="99%" id="tblTheme" runat="server">
							<tr>
								<td valign="top" align="center">
									<table border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="850" >
												<div class="module whitebg" >
													<span class="ct"><span class="cl"></span></span>
													<!-- START CONTENT -->
													<h2>Choose a theme</h2>

													<table width="800" border="0">
														<tr>
															<td >
																<div style="height: 340px; overflow: scroll;">
																	<!-- Themes -->
																	<asp:datalist id="dlThemes" runat="server" Repeatdirection="Horizontal" width="100%" Visible="true"
																		ShowFooter="False" cellpadding="2" cellspacing="2" border="0" RepeatColumns="1">
																		<ItemStyle HorizontalAlign="Left" />
																		<ItemTemplate>
																			<table >
																				<tr>
																					<td>
																						<img runat="server" src='<%# DataBinder.Eval(Container.DataItem, "preview_url") %>' title='<%# DataBinder.Eval(Container.DataItem, "description") %>'  width="160" border="1"/>
																					</td>
																					<td>&nbsp;</td>
																					<td >
																						<span class="dateStamp">
																							<%# DataBinder.Eval(Container.DataItem, "name") %>
																						</span><br />
																						<input id="rbTheme" name="rbTheme" type="radio" value='<%# DataBinder.Eval( Container.DataItem, "standard_template_id")%>' onclick="<%# GetPreviewJavascript (DataBinder.Eval(Container.DataItem, "preview_url").ToString ()) %>"/>
																					</td>
																				</tr>
																			</table>
																		</ItemTemplate>
																</asp:datalist>
																</div>
															</td>
															<td></td>
															<td valign="top" >
																<!-- Theme Preview -->
																<IMG runat="server" id="imgThemePreview" src="~\images\imgBgTheme.gif" Height="255" Width="340">
																
															</td>
														</tr>
													</table>

													<!-- END CONTENT -->																			
													<span class="cb"><span class="cl"></span></span>
												</div>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!-- End Container Wrapper -->


						<!-- Start Container Wrapper -->
						<table border="0" cellspacing="0" cellpadding="0" width="99%">
							<tr>
								<td valign="top" align="center">
									<table border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="850" >
												<div class="module whitebg" >
													<span class="ct"><span class="cl"></span></span>
													<!-- START CONTENT -->
													<h2>Set age and content restrictions</h2>

													<table width="800" border="0">
														<tr>
															<td>
																<table>
																	<tr id="trRestricted" runat="server" visible="false">
																		<td>
																			<asp:checkbox id="cbRestricted" runat="server"></asp:checkbox><strong>Restricted</strong>
																			(Check this box if anything in your community is inappropriate for visitors under 18.)
																		</td>
																	</tr>
																</table>
																<table>
																	<tr id="trOver21" runat="server">
																		<td>
																			<asp:checkbox id="cbOver21" runat="server"></asp:checkbox><strong>21 and over only</strong> 
																			Set this option to ensure only members 21 and older are able to access your Kaneva Community and Virtual World hang-out.<br />
																			<br />
																		</td>
																	</tr>
																</table>

																<span style="line-height:16px;">Access Type</span><br />
																<asp:DropDownList id="drpStatus" runat="Server" Width="150px">
																	<asp:ListItem Value="Y"> Public </asp:ListItem>
																	<asp:ListItem Value="N"> Members Only </asp:ListItem>
																</asp:DropDownList>&nbsp;&nbsp;
																<A href="http://www.kaneva.com/community/CommunityPage.aspx?communityId=80805&pageId=2168401" target="_resource" runat="server">What is Members Only?</A><br />

																<div id="membersPublish" runat="server" visible="false">
																	<asp:checkbox class="formKanevaCheck" id="chkAllowPublish" runat="server"></asp:checkbox>Allow all members of the community to publish photos, videos, music, and games.<br />
																</div>
															</td>
														</tr>
													</table>

													<!-- END CONTENT -->																			
													<span class="cb"><span class="cl"></span></span>
												</div>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!-- End Container Wrapper -->


						<!-- Save / Cancel sections -->
						<!-- Start Container Wrapper -->
						<table border="0" cellspacing="0" cellpadding="0" width="99%">
							<tr>
								<td valign="top" align="center">
									<table border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="850" >
												<!-- START CONTENT -->
												<table width="800" border="0" >
													<tr>
														<td align="right">
															<asp:imageButton class="" id="btnLaunch"  ImageUrl="~/images/buttons/btnCommunityLaunch.gif" onclick="btnImgUpdate_Click" runat="Server" visible="false" ></asp:imageButton>
															<asp:button class="Filter2" id="btnUpdate" onclick="btnUpdate_Click" runat="Server" Text="Save Changes" CausesValidation="True"></asp:button>&nbsp;&nbsp;
															<asp:button class="Filter2" id="btnCancel" onclick="btnCancel_Click" runat="Server" Text="Cancel" CausesValidation="False"></asp:button>
															<br /><br />
														</td>
													</tr>
												</table>
												<!-- END CONTENT -->																			

											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!-- End Container Wrapper -->

						</ajax:ajaxpanel>

					<!-- End Content -->
					</td>
					<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
				</tr>				
				<tr>
					<td class="frBottomLeft"></td>
					<td class="frBottom"></td>
					<td class="frBottomRight"></td>
				</tr>
				<tr>
					<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" /></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
			