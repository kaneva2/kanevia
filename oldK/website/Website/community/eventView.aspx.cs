///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for eventView.
	/// </summary>
	public class eventView : BasePage
	{
		private	int	_eventId;

		protected Label lblTitle;
		protected Label lblStartTime;
		protected Label lblEndTime;
		//protected Label lblTimeZone;
		protected Label lblEventType;
		protected Label lblLocation;
		protected Label lblDescription;

		private void Page_Load(object sender, System.EventArgs e)
		{
            // Old page, users should not see this page. Redirecting to my kaneva
            RedirectToHomePage();
            
            if (Request.IsAuthenticated)
			{
				if(GetRequestParams())
				{
//					//check if user is allowed to edit this channel
//					if(!IsUserAllowedToView())
//					{
//						RedirectToHomePage ();
//					}
				}
				else
				{
					//invalid request params
					//if the user logged in, send him to his home page
					//if not, redirect to login page
					RedirectToHomePage ();
				}
			}
			else
			{
				//Response.Redirect(this.GetLoginURL());
			}

			if(!IsPostBack)
			{
				BindData();
			}
		}
		private bool GetRequestParams()
		{
			bool retVal = true;
			try
			{
				_eventId = Convert.ToInt32 (Request ["eventId"]);
				if(_eventId <= 0 )
				{
					ChannelId = Convert.ToInt32 (Request ["communityId"]);
				}
				else
				{
					DataRow drEvent = CommunityUtility.GetEvent(_eventId);
					ChannelId = Convert.ToInt32(drEvent["community_id"]);
				}
			}
			catch(Exception)
			{
				//invalid numbers
				retVal = false;
			}
			return retVal;
		}

		/// <summary>
		/// returns true if current user is allowed to edit this channel
		/// </summary>
		/// <returns></returns>
		private bool IsUserAllowedToView()
		{
			bool retVal = false;
			if(Request.IsAuthenticated)
			{
				retVal = CommunityUtility.IsCommunityModerator(ChannelId, GetUserId());
				//					||
				//					(allowMemberEvent && CommunityUtility.IsActiveCommunityMember(
				//					_channelId, GetUserId()));
			}
			return retVal;
		}

		private void BindData()
		{
			string title = "";
			string desc = "";
			string location = "";
			int eventTypeId = 1;
			//int timeZoneId = 1;
			DateTime startTime = DateTime.Now;
			DateTime endTime = DateTime.Now.AddMinutes(30);
			if(_eventId > 0)
			{
				DataRow drEvent = CommunityUtility.GetEvent(_eventId);
				title = drEvent["title"].ToString();
				desc = drEvent["details"] == DBNull.Value ? "" : drEvent["details"].ToString();
				location = drEvent["location"] == DBNull.Value ? "" : drEvent["location"].ToString();
				eventTypeId = Convert.ToInt32(drEvent["type_id"]);
				//timeZoneId = Convert.ToInt32(drEvent["time_zone_id"]);
				startTime = Convert.ToDateTime(drEvent["start_time"]);
				endTime = Convert.ToDateTime(drEvent["end_time"]);
			}

			lblTitle.Text = title;
			lblDescription.Text = desc;
			lblLocation.Text = location;
			lblStartTime.Text = startTime.ToString();
			lblEndTime.Text = endTime.ToString();
			//lblTimeZone;
//			if(Constants.EVENT_TYPES.Contains(eventTypeId))
//			{
//				lblEventType.Text = Constants.EVENT_TYPES[eventTypeId].ToString();
//			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion

		public int EventId
		{
			get { return _eventId; }
			set { _eventId = value; }
		}

		private int ChannelId
		{
			get
			{
				if (ViewState["ChannelId"] == null || ViewState["ChannelId"].ToString() == "")
				{
					ViewState["ChannelId"] = "0";
				}
				return Convert.ToInt32(ViewState["ChannelId"].ToString());
			}
			set
			{
				ViewState["ChannelId"] = value;
			}
		}
	}
}
