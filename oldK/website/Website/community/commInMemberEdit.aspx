<%@ Page language="c#" Codebehind="commInMemberEdit.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.commInMemberEdit" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>

<link href="../css/home.css" rel="stylesheet" type="text/css" />		
<link href="../css/kanevaSC.css" rel="stylesheet" type="text/css"/>
<link href="../css/friends.css" rel="stylesheet" type="text/css"/>
<link href="../css/kanevaText.css" type="text/css" rel="stylesheet">

<link href="../css/new.css" rel="stylesheet" type="text/css" />

<script src="../jscript/prototype.js" type="text/javascript" language="javascript"></script>


<table border="0" cellspacing="0" cellpadding="0" class="newcontainer" align="center">
	<tr>
		<td> 
			<table  border="0" cellpadding="0" cellspacing="0" width="100%" class="newcontainerborder">
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
				<tr>
					<td class=""><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img21"/></td>
					<td valign="top" class="newdatacontainer">  
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
								
									<!-- TOP STATUS BAR -->
									<div id="pageheader">
										<table cellpadding="0" cellspacing="0" border="0" width="99%">
											<tr>
												<td align="left">
													<h1>World Member Setup</h1>
												</td>
												
												<td align="right" valign="middle">Set important World member permissions</td>
											</tr>
										</table>
									</div>
									<!-- END TOP STATUS BAR -->	
								
								</td>
							</tr>
							<tr>
								<td width="968" align="left"  valign="top">
									<table cellpadding="0" cellspacing="0" border="0"  width="968">
										<tr>
											<td width="250" valign="top">
												<div class="module toolbar">
													<span class="ct"><span class="cl"></span></span>
													<h2>Tools & Actions <span class="hlink"></span></h2>
													<table cellspacing="0" cellpadding="10" border="0" width="99%">
														<tr>
															<td align="center">
											
																<table cellspacing="0" cellpadding="3" border="0" width="100%">
																	<tr>
																		<td valign="bottom">
																		 
																			<a id="aChanMembers" runat="server">View World Member List</a>
																			<br/><br/>
																			<a id="aChanMemberGroups" runat="server">View World Member Groups</a>
																			<br/><br/>
																			<a id="aAddMember" runat="server">Add Member</a>
																		
																		</td>
																	</tr>
																</table>				  
															</td>
														</tr>
														
													</table>
										 				
													<span class="cb"><span class="cl"></span></span>
												</div>
											</td>
											<td width="20"><img runat="server" src="~/images/spacer.gif" width="20" height="1" id="Img22"/></td>
											<td width="698" valign="top" align="center">
												
													<div class="module whitebg">
													<span class="ct"><span class="cl"></span></span>
													
													<ajax:ajaxpanel id="ajpMessageArea" runat="server">
														<h2 class="alertmessage"><span id="spnAlertMsg" runat="server"></span></h2>
													</ajax:ajaxpanel>	
													
													
													<div align="left" style="margin-left:5px;">
														
														<table width="98%" border="0" cellspacing="0" cellpadding="4">
															<tr>
																<td align="right" nowrap class="insideBoxText11"><strong>Member:</strong></td>
																<td align="left"><strong><asp:label runat="server" id="lblUsername"/></strong></td>
															</tr>
															<tr>
																<td align="right" nowrap class="insideBoxText11"><strong>Role:</strong></td>
																<td align="left"><asp:DropDownList class="filter2" id="drpRoles" runat="Server" Width="200px"/></td>
															</tr>
															<tr>
																<td align="right" nowrap class="insideBoxText11"><strong>Can use forums:</strong></td>
																<td align="left"><input class="Filter2" type="checkbox" runat="server" id="chkUseForums" name="chkUseForums"/></td>
															</tr>
															<tr>
																<td align="right" valign="top" class="insideBoxText11"><strong>Suspended:</strong></td>
																<td align="left"><input class="Filter2" type="checkbox" runat="server" id="chkSuspended" name="chkSuspended"/></td>
															</tr>
															<tr>			 
																<td colspan="3" align="center"><br/>
																	<a id="aCancel" runat="server">
																	<img src="~/images/button_cancel.gif" runat="server" alt="Cancel" width="57" height="23" border="0" /></a>
																	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																	<ajax:ajaxpanel id="ajpBottomSendButton" runat="server">
																		<asp:imagebutton id="btnSave" imageurl="~/images/button_save.gif" onclick="btnSave_Click" alternatetext="Save" width="77" height="23" border="0" runat="server"/> 
																	</ajax:ajaxpanel>
																</td>
															</tr>
														</table>

													</div>
														
													<span class="cb"><span class="cl"></span></span>
												</div>
												
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td class=""><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img24"/></td>
				</tr>
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
			</table>
		</td>
	</tr>
</table>




