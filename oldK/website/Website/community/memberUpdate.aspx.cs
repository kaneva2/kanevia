///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for memberUpdate.
	/// </summary>
	public class memberUpdate : BasePage
	{
		private int _channelId;
		private int _userId;
		private void Page_Load(object sender, System.EventArgs e)
		{
			if(Request.IsAuthenticated)
			{
				if(GetRequestParams())
				{
					//check if user is allowed to edit this channel
					if (!IsUserAllowedToEdit())
					{
						RedirectToHomePage ();
					}
				}
				else
				{
					//invalid request params
					//if the user logged in, send him to his home page
					//if not, redirect to login page
					RedirectToHomePage ();
				}
			}
			else
			{
				Response.Redirect(this.GetLoginURL());
			}
			
			// Make sure they are a moderator
			if (CommunityUtility.IsCommunityModerator (_channelId, KanevaWebGlobals.GetUserId ()))
			{
                GetCommunityFacade.UpdateCommunityMember(_channelId, _userId, Convert.ToUInt32(Request["action"]));
			}

			Response.Redirect (ResolveUrl ("~/community/members.aspx?communityId="+_channelId));
		}

		private bool GetRequestParams()
		{
			bool retVal = true;
			try
			{
				_channelId = Convert.ToInt32 (Request ["communityId"]);
				_userId = Convert.ToInt32 (Request ["userId"]);
			}
			catch(Exception)
			{
				//invalid numbers
				retVal = false;
			}
			return retVal;
		}

		/// <summary>
		/// returns true if current user is allowed to edit this channel
		/// </summary>
		/// <returns></returns>
		private bool IsUserAllowedToEdit()
		{
			return CommunityUtility.IsCommunityModerator(_channelId, GetUserId());
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
