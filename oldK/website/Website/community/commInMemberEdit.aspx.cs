///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva.channel;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for commInMemberEdit.
	/// </summary>
	public class commInMemberEdit : MainTemplatePage
	{
		protected commInMemberEdit () 
		{
			Title = "World Member Edit";
		}

		private void Page_Load (object sender, System.EventArgs e)
		{
			if(Request.IsAuthenticated)
			{
				if(GetRequestParams())
				{
					//check if user is allowed to edit this channel
					if(!IsUserAllowedToEdit())
					{
						RedirectToHomePage ();
					}
				}
				else
				{
					//invalid request params
					//if the user logged in, send him to his home page
					//if not, redirect to login page
					RedirectToHomePage ();
				}
			}
			else
			{
				Response.Redirect (this.GetLoginURL ());
			}
			
			DataRow drCommunity = CommunityUtility.GetCommunity (_channelId);

			// Set Nav
            HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB._UPDATE_;
			HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.CHANNELS;
			HeaderNav.MyChannelsNav.ChannelId = _channelId;
			HeaderNav.MyChannelsNav.ActiveTab = NavMyChannels.TAB.MEMBERS;
				
			HeaderNav.SetNavVisible(HeaderNav.MyKanevaNav,2);
			HeaderNav.SetNavVisible(HeaderNav.MyChannelsNav);


			if (!IsPostBack)
			{
                UserFacade userFacade = new UserFacade();
                User user = userFacade.GetUser(_userId);
                lblUsername.Text = user.Username;

                CommunityFacade communityFacade = new CommunityFacade();
                CommunityMember communityMember = communityFacade.GetCommunityMember(_channelId, _userId);

				// only show if they are an owner
                if (communityMember.AccountTypeId.Equals((int) CommunityMember.CommunityMemberAccountType.OWNER))
				{
                    drpRoles.Items.Add(new ListItem("Owner", ((int)CommunityMember.CommunityMemberAccountType.OWNER).ToString()));
				}

                drpRoles.Items.Add(new ListItem("Moderator", ((int)CommunityMember.CommunityMemberAccountType.MODERATOR).ToString()));
                drpRoles.Items.Add(new ListItem("Subscriber", ((int)CommunityMember.CommunityMemberAccountType.SUBSCRIBER).ToString()));

                SetDropDownIndex(drpRoles, communityMember.AccountTypeId.ToString());

                chkUseForums.Checked = communityMember.AllowForumUse.Equals("Y");
                chkSuspended.Checked = communityMember.StatusId.Equals((UInt32)CommunityMember.CommunityMemberStatus.LOCKED);

                ViewState["statusId"] = communityMember.StatusId;

				// If they are the community creator, don't let them change any status's
			
				if (Convert.ToInt32 (drCommunity ["creator_id"]).Equals (_userId))
				{
					drpRoles.Enabled = false;
					chkSuspended.Disabled = true;
					chkUseForums.Disabled = true;
				}

				aCancel.HRef = ResolveUrl ("~/community/members.aspx?communityId=" + _channelId );

				aChanMembers.HRef = ResolveUrl ("~/community/members.aspx?communityId=" + _channelId );
				aChanMemberGroups.HRef = ResolveUrl ("~/community/memberGroups.aspx?communityId=" + _channelId );
				aAddMember.HRef = ResolveUrl ("~/community/inviteMember.aspx?communityId=" + _channelId );
			}
		}

		
		#region Helper Methods
		private bool GetRequestParams()
		{
			bool retVal = true;
			try
			{
				_channelId = Convert.ToInt32(Request.Params["communityId"]);
				_userId = Convert.ToInt32 (Request ["userId"]);
			}
			catch(Exception)
			{
				//invalid numbers
				retVal = false;
			}
			return retVal;
		}

		/// <summary>
		/// returns true if current user is allowed to edit this channel
		/// </summary>
		/// <returns></returns>
		private bool IsUserAllowedToEdit()
		{
			return CommunityUtility.IsCommunityModerator(_channelId, GetUserId());
		}

		#endregion

		#region Event Handlers
		/// <summary>
		/// Update a community member
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnSave_Click (object sender, ImageClickEventArgs e) 
		{
			int communityId = Convert.ToInt32 (Request ["communityId"]);
			int userId = Convert.ToInt32 (Request ["userId"]);
			int ret = 0;

            CommunityFacade communityFacade = new CommunityFacade();
            CommunityMember communityMember = communityFacade.GetCommunityMember(communityId, userId);

            // Wrapping this in a try/catch instead of relying on checking ret == 1.
            // There are several legitimate cases where ret might == 0, including
            // hitting the update button without making a change.
			try
			{
                ret = CommunityUtility.UpdateCommunityMember(communityId, userId, Convert.ToInt32(drpRoles.SelectedValue),
                "N", "Y", (chkUseForums.Checked ? "Y" : "N"));

				// Suspend
				if (chkSuspended.Checked)
				{
                    ret = GetCommunityFacade.UpdateCommunityMember(communityId, userId, (UInt32)CommunityMember.CommunityMemberStatus.LOCKED);
				}
				else
				{
					// They are not suspended, update em if they are currently locked
					if (Convert.ToUInt32 (ViewState ["statusId"]).Equals ((UInt32) CommunityMember.CommunityMemberStatus.LOCKED))
					{
                        ret = GetCommunityFacade.UpdateCommunityMember(communityId, userId, (UInt32)CommunityMember.CommunityMemberStatus.ACTIVE);
					}
				}
			}
			catch
			{
				spnAlertMsg.InnerText = "There was an error saving changes.";
				return;
			}
			
			spnAlertMsg.InnerText = "Changes saved successfully.";
		}

		#endregion

		#region Declerations
		protected Label			lblUsername;
		protected DropDownList	drpRoles;
		
		protected HtmlAnchor			aChanMembers, aChanMemberGroups, aAddMember;
		protected HtmlInputCheckBox		chkUseForums, chkSuspended;
		protected HtmlAnchor			aCancel;
		protected HtmlContainerControl	spnAlertMsg;

		private int _channelId;
		private int _userId;
		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
