///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections;
using log4net;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using System.Xml;

namespace KlausEnt.KEP.Kaneva
{
    /// <summary>
    /// Summary description for commDrillDown.
    /// </summary>
    public class commDrillDown : BasePage
    {
        protected commDrillDown()
        {
        }

        private void Page_Load(object sender, System.EventArgs e)
        {
            if (!IsPostBack)
            {
                CommunityFacade communityFacade = new CommunityFacade();

                drpPlace.DataSource = communityFacade.GetCommunityPlaceTypes();
                drpPlace.DataTextField = "Name";
                drpPlace.DataValueField = "PlaceTypeId";

                drpPlace.DataBind();

                drpPlace.Items.Insert(0, new ListItem("All", "-1"));

                rptGameCategories.DataSource = GetCommunityFacade.GetCommunityCategories();
                rptGameCategories.DataBind();

                // Initialize search params
                InitializeSearch(true);
            }

            //setup header nav bar
            ((GenericPageTemplate) Master).HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.CONNECT;

            ((GenericPageTemplate) Master).Title = "Kaneva Communities. Explore, Join or Create a Community.";

            //set the access pass filter toggle disable and invisible then process
            cbx_AccessPass.Enabled = div_AccessPassFilter.Visible = false;
            liBrowse_AccessPass.Visible = false;

            toggleBrowseLinks(false);

            // Is user logged in?
            if (Request.IsAuthenticated)
            {
                //configure the access pass only filter
                if (KanevaWebGlobals.CurrentUser.IsAdult)
                {
                    //set the Access Pass link value
                    lnk_AccesPass.HRef = GetAccessPassLink();

                    //disable/enable show access pass only based on users access pass status
                    cbx_AccessPass.Enabled = KanevaWebGlobals.CurrentUser.HasAccessPass;
                    div_AccessPassFilter.Visible = false;
                }
            }
        }


        #region Helper Methods

        /// <summary>
        /// Called to display the correct Browse Links when searching/browsing
        /// </summary>
        /// <param name="isSearch"></param>
        private void toggleBrowseLinks(bool isSearch)
        {
            // Show appropriate links for 3d apps
            tdResultViewMode.Visible = false;
            liBrowse_Relevance.Visible = isSearch;
            liBrowse_Views.Visible = isSearch;
            liBrowse_Raves.Visible = isSearch;
            liBrowse_Newest.Visible = isSearch;
            liBrowse_Members.Visible = isSearch;
            liBrowse_MostActive.Visible = !isSearch;
            liBrowse_MostVisited.Visible = !isSearch;
            rptGameCategories.Visible = !isSearch;
            liBrowse_AccessPass.Visible = (!isSearch && KanevaWebGlobals.CurrentUser.IsAdult && KanevaWebGlobals.CurrentUser.HasAccessPass);
        }

        /// <summary>
        /// BindData
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="filter"></param>
        private void BindData(int pageNumber)
        {
            // How many results do we display?
            int pgSize = filterTop.NumberOfPages;

            // Set current page
            pgTop.CurrentPageNumber = pageNumber;
            pgBottom.CurrentPageNumber = pageNumber;

            // Clear existing results
            dlChannelThumb.Visible = false;
            dlChannelDetail.Visible = false;
            divNoResults.Visible = false;

            spnSearchFilterDesc.InnerText = GetSearchFilterSummary();

            if (Is_Detail_View)
            {
                lbThumbTop.CssClass = "";
                lbDetailTop.Enabled = false;
                lbDetailTop.CssClass = "selected";
                lbDetailTop.Enabled = true;

                pgSize = 10;
            }
            else
            {
                lbDetailTop.CssClass = "";
                lbThumbTop.Enabled = false;
                lbThumbTop.CssClass = "selected";
                lbThumbTop.Enabled = true;

                pgSize = 15;
            }

            SearchChannels(pageNumber, pgSize);

            // Show Pager
            pgTop.DrawControl();
            pgBottom.DrawControl();

            // Log the search
            UserFacade userFacade = new UserFacade();
            userFacade.LogSearch(GetUserId(), Search_String, Search_Type);
        }
        /// <summary>
        /// SearchChannels
        /// </summary>
        /// <param name="pgNumber"></param>
        /// <param name="pgSize"></param>
        private void SearchChannels(int pgNumber, int pgSize)
        {
            string strCountry = "";
            int totalResultCount = 0;
            int resultCount = 0;
            object dataSource = null;
            int placeId = -1;

            // If they are logged in filter by country if not searching
            if (Request.IsAuthenticated && Search_String.Length.Equals(0))
            {
                // Take this out per AS 4/13.09
                //strCountry = KanevaWebGlobals.CurrentUser.Country;
                // Put this back per AS 4/24.09
                // Is user in the list of countries we populate?
                DataRow[] drCountry = WebCache.GetCountriesSummarized().Select("country = '" + KanevaWebGlobals.CurrentUser.Country + "'");

                // And they have the preference
                if (drCountry.Length > 0 && KanevaWebGlobals.CurrentUser.Preference.FilterByCountry)
                {
                    strCountry = drCountry[0]["country"].ToString();
                }
            }

            DataTable dtGame;
                
            if (Order_By == Constants.SEARCH_ORDERBY_MOST_ACTIVE)
            {
                // Per Billy, remove AP Worlds from all but the AP category
                dtGame = GetGameFacade.MostPopulatedCombined (ShowOnlyAccessPass, true, GetUserId (), ref totalResultCount, pgNumber, pgSize, KanevaGlobals.WokGameId, "", "", 0,0, -1);
            }
            else if (Order_By == Constants.SEARCH_ORDERBY_VISITS)
            {
                // Note: We cach the results of the Top3DApps() function as an xml string
                string cacheKey = "SearchTop3DAppsWeb" + pgNumber + "p" + pgSize + "A" + ShowOnlyAccessPass;
                string xmlResponse = (string)Cache[cacheKey];
                XmlDocument doc = new XmlDocument();
                    
                // If we don't have any apps in cache, run the file and build the cache.
                if (xmlResponse == null)
                {
                    dtGame = GetGameFacade.Top3Dapps(ShowOnlyAccessPass, true, pgNumber, pgSize, ref totalResultCount, KanevaGlobals.WokGameId, true);

                    doc.LoadXml(dtGame.DataSet.GetXml());
                    xmlResponse = "<top3dapps>"
                                    + "<totalresultcount>" 
                                        + totalResultCount 
                                    + "</totalresultcount>" 
                                    + doc.OuterXml 
                                + "</top3dapps>";

                    Cache.Insert(cacheKey, xmlResponse, null, DateTime.Now.AddSeconds(60), System.Web.Caching.Cache.NoSlidingExpiration);
                }
                // If we have a cached response, process the xml to load the datatable.
                else
                {
                    DataSet dsGame = new DataSet();

                    doc.LoadXml(xmlResponse);

                    XmlNode root = doc.DocumentElement;
                    totalResultCount = Convert.ToInt32(root.FirstChild.InnerText);

                    dsGame.ReadXml(new System.IO.StringReader(root.FirstChild.NextSibling.OuterXml.ToString()));
                    dtGame = dsGame.Tables[0];
                }
            }
            else if (Order_By == cORDER_BY_GAME_CATEGORY && !string.IsNullOrEmpty(Selected_Game_Category))
            {
                dtGame = GetGameFacade.GetWorldsByCategory(ShowOnlyAccessPass, !ShowOnlyAccessPass, ref totalResultCount, pgNumber, pgSize, KanevaGlobals.WokGameId, Selected_Game_Category);
            }
            else if (Order_By == Constants.SEARCH_ORDERBY_REQUESTS)
            {
                dtGame = GetGameFacade.GetMostRequested (ShowOnlyAccessPass, true, GetUserId (), ref totalResultCount, pgNumber, pgSize, KanevaGlobals.WokGameId);
            }
            else
            {
                dtGame = GetGameFacade.Search3DApps (ShowOnlyAccessPass, true,
                    Server.HtmlEncode(Search_String), chkPhotoRequired.Checked, strCountry,
                    new int[] { (int)CommunityType.APP_3D, (int)CommunityType.COMMUNITY, (int)CommunityType.HOME }, SetOrderByValue(), pgNumber, pgSize, placeId, false, false, GetUserId(), KanevaGlobals.WokGameId, ref totalResultCount);
            }
            dataSource = dtGame;

            // this is used to limit the number of pages to 5 when browsing
            if (Search_String.Length > 0)   // Searching
            {
                pgTop.NumberOfPages = Math.Ceiling((double)totalResultCount / pgSize).ToString();
                pgBottom.NumberOfPages = Math.Ceiling((double)totalResultCount / pgSize).ToString();

                // Set up browse area
                ulTime.Visible = false;
                //ulPlace.Visible = false;
                liBrowseLabel.InnerText = "Sort by";
                toggleBrowseLinks(true);
            }
            else  // Browsing
            {
                if (Math.Ceiling((double)totalResultCount / pgSize) < 5 || AllowedToEdit())
                {
                    pgTop.NumberOfPages = Math.Ceiling((double)totalResultCount / pgSize).ToString();
                    pgBottom.NumberOfPages = Math.Ceiling((double)totalResultCount / pgSize).ToString();
                }
                else
                {
                    pgTop.NumberOfPages = "5";
                    pgBottom.NumberOfPages = "5";
                }

                // Set up browse area (communities only, don't show on Worlds page)
                ulTime.Visible = false;
                liBrowseLabel.InnerText = "Browse";
                liBrowse_Relevance.Visible = false;
            }


            if (Is_Detail_View && totalResultCount > 0)
            {
                dlChannelDetail.DataSource = dataSource;
                dlChannelDetail.DataBind();
                dlChannelDetail.Visible = true;
            }
            else if (totalResultCount > 0)
            {
                dlChannelThumb.DataSource = dataSource;
                dlChannelThumb.DataBind();
                dlChannelThumb.Visible = true;
            }
            else
            {
                ShowNoResults();
            }

            ShowResultsCount((int)totalResultCount, pgTop.CurrentPageNumber, filterTop.NumberOfPages, resultCount);
        }

        /// <summary>
        /// ShowResultsCount
        /// </summary>
        /// <param name="total_count"></param>
        /// <param name="page_num"></param>
        /// <param name="count_per_page"></param>
        /// <param name="current_page_count"></param>
        private void ShowResultsCount(int total_count, int page_num, int count_per_page, int current_page_count)
        {
            // Display the showing #-# of ### count
            lblResultsBottom.Text = KanevaGlobals.GetResultsText(total_count, page_num, count_per_page, current_page_count, true,
                "%START%-%END% of %TOTAL%");
        }

        /// <summary>
        /// ShowNoResults
        /// </summary>
        private void ShowNoResults()
        {
            divNoResults.InnerHtml = "<div class='noresults'><br/>Your search did not find any matching Worlds.</div>";
            divNoResults.Visible = true;
        }
        /// <summary>
        /// SetOrderByValue
        /// </summary>
        private string SetOrderByValue()
        {
            CurrentSortOrder = "";

            // How they are sorted
            if (Order_By.Equals(Constants.SEARCH_ORDERBY_RELEVANCE))
            {
                // Newest
                CurrentSort = "";
            }
            else if (Order_By.Equals(Constants.SEARCH_ORDERBY_NEWEST))
            {
                // Newest
                CurrentSort = "created_date";
                CurrentSortOrder = "DESC";
            }
            else if (Order_By.Equals(Constants.SEARCH_ORDERBY_RAVES))
            {
                // Most raves
                CurrentSort = "number_of_diggs";
                CurrentSortOrder = "DESC";
            }
            else if (Order_By.Equals(Constants.SEARCH_ORDERBY_MEMBERS))
            {
                // Most members
                CurrentSort = "number_of_members";
                CurrentSortOrder = "DESC";
            }
            else // Views
            {
                CurrentSort = "number_of_views";
                CurrentSortOrder = "DESC";
            }

            // Note that Most Active and Most Visited are not set here.  They are not true
            // ordering clauses, and are instead distinct queries.

            // Set sort and order by
            return (CurrentSort + " " + CurrentSortOrder).Trim();
        }
        /// <summary>
        /// SetOrderByType
        /// </summary>
        private void SetOrderByType()
        {
            try
            {
                if (Order_By == string.Empty) // Set order by if not already in viewstate
                {
                    if (Request[Constants.QUERY_STRING_ORDERBY] != null && Request[Constants.QUERY_STRING_ORDERBY] != string.Empty)
                    {
                        switch (Server.UrlDecode(Request[Constants.QUERY_STRING_ORDERBY].ToString().ToLower()))
                        {
                            case Constants.SEARCH_ORDERBY_RELEVANCE:
                            case Constants.SEARCH_ORDERBY_RAVES:
                            case Constants.SEARCH_ORDERBY_VIEWS:
                            case Constants.SEARCH_ORDERBY_MEMBERS:
                            case Constants.SEARCH_ORDERBY_NEWEST:
                            case Constants.SEARCH_ORDERBY_MOST_ACTIVE:
                            case Constants.SEARCH_ORDERBY_VISITS:
                                Order_By = Server.UrlDecode(Request[Constants.QUERY_STRING_ORDERBY].ToString());
                                break;
                            default:
                                Order_By = cORDER_BY_DEFAULT_3DAPPS;
                                break;
                        }
                    }
                    else
                    {
                        Order_By = cORDER_BY_DEFAULT_3DAPPS;
                    }
                }
            }
            catch (Exception)
            {
                Order_By = cORDER_BY_DEFAULT;
            }
        }
        
        private void SetCommunityTypeDisplay()
        {
            aCreate3DApp.Visible = true;
            aMyWorlds.Visible = true;
            aCreateCommunity.Visible = false;

            h1ResultsCommType.InnerHtml = "Worlds";
            litFindType.Text = "Worlds";

            ulPlace.Visible = false;
            rptGameCategories.Visible = true;
        }

        /// <summary>
        /// SetWithinTimeFrameType
        /// </summary>
        private void SetWithinTimeFrameType()
        {
            try
            {
                if (New_Within < 0) // Get the type from the radio button selected
                {
                    if (Request[Constants.QUERY_STRING_WITHIN] != null && Request[Constants.QUERY_STRING_WITHIN] != string.Empty)
                    {
                        switch (Convert.ToInt32(Request[Constants.QUERY_STRING_WITHIN]))
                        {
                            case 0:
                            case 1:
                            case 7:
                            case 30:
                                New_Within = Convert.ToInt32(Request[Constants.QUERY_STRING_WITHIN]);
                                break;
                            default:
                                New_Within = cNEW_WITHIN_DEFAULT_3DAPP;
                                break;
                        }
                    }
                    else
                    {
                        New_Within = cNEW_WITHIN_DEFAULT_3DAPP;
                    }
                }
            }
            catch (Exception)
            {
                New_Within = cNEW_WITHIN_DEFAULT;	 //all time
            }
        }

        /// <summary>
        /// SetSearchKeywords
        /// </summary>
        private void SetSearchKeywords()
        {
            try
            {
                if (Search_String == string.Empty)
                {
                    if (Request[Constants.QUERY_STRING_KEYWORD] != null && Request[Constants.QUERY_STRING_KEYWORD] != string.Empty)
                    {
                        Search_String = Server.UrlDecode(Request[Constants.QUERY_STRING_KEYWORD].ToString());

                        Order_By = Constants.SEARCH_ORDERBY_RELEVANCE;
                        New_Within = 0;

                        txtKeywords.Text = Search_String;
                        txtKeywords.Style.Add("color", "#535353");
                    }
                    else
                    {
                        Search_String = string.Empty;
                    }
                }
            }
            catch (Exception)
            {
                Search_String = string.Empty;
            }
        }

        protected bool AllowedToEdit()
        {
            bool allowEditing = false;

            //check security level
            switch (KanevaWebGlobals.CheckUserAccess((int)SitePrivilege.ePRIVILEGE.COMMUNITY_PROFILE_ADMIN))
            {
                case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_FULL:
                    allowEditing = true;
                    break;
                case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_READ:
                    break;
                case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_NONE:
                    break;
                default:
                    break;
            }

            return allowEditing;
        }

        /// <summary>
        /// SetBrowseLinks
        /// </summary>
        private void SetBrowseLinks()
        {
            liBrowse_Relevance.Attributes.Add("class", "");
            liBrowse_Raves.Attributes.Add("class", "");
            liBrowse_Views.Attributes.Add("class", "");
            liBrowse_Members.Attributes.Add("class", "");
            liBrowse_Newest.Attributes.Add("class", "");
            liBrowse_MostActive.Attributes.Add("class", "");
            liBrowse_MostVisited.Attributes.Add("class", "");
            liBrowse_AccessPass.Attributes.Add("class", "");
            liBrowse_Requests.Attributes.Add ("class", "");

            switch (Order_By)
            {
                case Constants.SEARCH_ORDERBY_RELEVANCE:
                    liBrowse_Relevance.Attributes.Add("class", cCSS_CLASS_SELECTED);
                    break;
                case Constants.SEARCH_ORDERBY_RAVES:
                    liBrowse_Raves.Attributes.Add("class", cCSS_CLASS_SELECTED);
                    break;
                case Constants.SEARCH_ORDERBY_VIEWS:
                    liBrowse_Views.Attributes.Add("class", cCSS_CLASS_SELECTED);
                    break;
                case Constants.SEARCH_ORDERBY_MEMBERS:
                    liBrowse_Members.Attributes.Add("class", cCSS_CLASS_SELECTED);
                    break;
                case Constants.SEARCH_ORDERBY_NEWEST:
                    liBrowse_Newest.Attributes.Add("class", cCSS_CLASS_SELECTED);
                    break;
                case Constants.SEARCH_ORDERBY_MOST_ACTIVE:
                    liBrowse_MostActive.Attributes.Add("class", cCSS_CLASS_SELECTED);
                    break;
                case Constants.SEARCH_ORDERBY_VISITS:
                    liBrowse_MostVisited.Attributes.Add("class", cCSS_CLASS_SELECTED);
                    break;
                case Constants.SEARCH_ORDERBY_REQUESTS:
                    liBrowse_Requests.Attributes.Add("class", cCSS_CLASS_SELECTED);
                    break;
                case cORDER_BY_AP_ONLY:
                    liBrowse_AccessPass.Attributes.Add ("class", cCSS_CLASS_SELECTED);
                    break;
                case cORDER_BY_GAME_CATEGORY:
                case "none":
                    break;
            }

            // Reset game category links if necessary
            if (rptGameCategories.Visible == true)
            {
                HtmlContainerControl lItem = null;
                LinkButton lButton = null;

                foreach (RepeaterItem rItem in rptGameCategories.Items)
                {
                    lItem = (HtmlContainerControl)rItem.FindControl("liBrowse_GameCategories");
                    lButton = (LinkButton)lItem.FindControl("lbBrowse_GameCategories");

                    if (Order_By == cORDER_BY_GAME_CATEGORY && lButton.CommandName == Selected_Game_Category)
                    {
                        lItem.Attributes.Add("class", cCSS_CLASS_SELECTED);
                    }
                    else
                    {
                        lItem.Attributes.Add("class", "");
                    }
                }
            }

            if (Request.IsAuthenticated && KanevaWebGlobals.CurrentUser.Stats.Number3DAppRequests > 0)
            {
                liBrowse_Requests.Visible = true;
                spnNew3DAppRequests.InnerText = "(" + GetGameFacade.GetTotalNotificationAppRequests (KanevaWebGlobals.CurrentUser.UserId) + ")";
            }
        }
        /// <summary>
        /// SetTimeLinks
        /// </summary>
        private void SetTimeLinks()
        {
            liTime_Today.Attributes.Add("class", "");
            liTime_Week.Attributes.Add("class", "");
            liTime_Month.Attributes.Add("class", "");
            liTime_All.Attributes.Add("class", "");

            switch (New_Within)
            {
                case (int)Constants.eSEARCH_TIME_FRAME.TODAY:
                    liTime_Today.Attributes.Add("class", cCSS_CLASS_SELECTED);
                    break;
                case (int)Constants.eSEARCH_TIME_FRAME.WEEK:
                    liTime_Week.Attributes.Add("class", cCSS_CLASS_SELECTED);
                    break;
                case (int)Constants.eSEARCH_TIME_FRAME.MONTH:
                    liTime_Month.Attributes.Add("class", cCSS_CLASS_SELECTED);
                    break;
                case (int)Constants.eSEARCH_TIME_FRAME.ALL_TIME:
                    liTime_All.Attributes.Add("class", cCSS_CLASS_SELECTED);
                    break;
            }
        }
        /// <summary>
        /// InitializeNewSearch
        /// </summary>
        private void InitializeSearch(bool Set_Keyword)
        {
            if (!IsPostBack)
            {
                Is_Detail_View = true;
            }

            // Set labels to be Communities or 3D Apps
            SetCommunityTypeDisplay();

            // Set OrderBy Type
            SetOrderByType();

            // Set Within Type
            SetWithinTimeFrameType();

            // Set Search keyword
            if (Set_Keyword)
            {
                SetSearchKeywords();
            }

            // Set selected Find links
            SetBrowseLinks();
            SetTimeLinks();

            BindData(1);
        }
        /// <summary>
        /// GetSearchFilterSummary
        /// </summary>
        private string GetSearchFilterSummary()
        {
            string browse_txt = string.Empty;
            string time_txt = string.Empty;
            string place_txt = string.Empty;

            switch (Order_By)
            {
                case Constants.SEARCH_ORDERBY_RAVES:
                    browse_txt = "Most Raves";
                    break;
                case Constants.SEARCH_ORDERBY_VIEWS:
                    browse_txt = "Most Views";
                    break;
                case Constants.SEARCH_ORDERBY_MEMBERS:
                    browse_txt = "Most Members";
                    break;
                case Constants.SEARCH_ORDERBY_NEWEST:
                    browse_txt = "Newest";
                    break;
                case Constants.SEARCH_ORDERBY_MOST_ACTIVE:
                    browse_txt = "Most Active";
                    break;
                case Constants.SEARCH_ORDERBY_VISITS:
                    browse_txt = "Most Visited";
                    break;
                case cORDER_BY_GAME_CATEGORY:
                    browse_txt = Selected_Game_Category;
                    break;
                case cORDER_BY_AP_ONLY:
                    browse_txt = "Access Pass";
                    break;
                default:
                    browse_txt = "Best Match";
                    break;
            }

            switch (New_Within)
            {
                case (int)Constants.eSEARCH_TIME_FRAME.TODAY:
                    time_txt = "Today";
                    break;
                case (int)Constants.eSEARCH_TIME_FRAME.WEEK:
                    time_txt = "This Week";
                    break;
                case (int)Constants.eSEARCH_TIME_FRAME.MONTH:
                    time_txt = "This Month";
                    break;
                case (int)Constants.eSEARCH_TIME_FRAME.ALL_TIME:
                    time_txt = "All Time";
                    break;
            }

            switch (Convert.ToInt16(drpPlace.SelectedValue))
            {
                case (int)Constants.eHANGOUT_PLACE_TYPE.OTHER:
                    place_txt = "Other";
                    break;
                case (int)Constants.eHANGOUT_PLACE_TYPE.ARCADE:
                    place_txt = "Arcades";
                    break;
                case (int)Constants.eHANGOUT_PLACE_TYPE.ART_GALLERY:
                    place_txt = "Art Galleries";
                    break;
                case (int)Constants.eHANGOUT_PLACE_TYPE.CASINO:
                    place_txt = "Casinos";
                    break;
                case (int)Constants.eHANGOUT_PLACE_TYPE.DANCE_CLUB:
                    place_txt = "Dance Clubs";
                    break;
                case (int)Constants.eHANGOUT_PLACE_TYPE.FESTIVAL:
                    place_txt = "Festival";
                    break;
                case (int)Constants.eHANGOUT_PLACE_TYPE.GAME_LOUNGE:
                    place_txt = "Game Lounges";
                    break;
                case (int)Constants.eHANGOUT_PLACE_TYPE.MUSIC_VENUE:
                    place_txt = "Music Venues";
                    break;
                case (int)Constants.eHANGOUT_PLACE_TYPE.MUSUEM:
                    place_txt = "Museums";
                    break;
                case (int)Constants.eHANGOUT_PLACE_TYPE.PLACE_OF_WORSHIP:
                    place_txt = "Places of Worship";
                    break;
                case (int)Constants.eHANGOUT_PLACE_TYPE.SPORTS_BAR:
                    place_txt = "Sports Bars";
                    break;
                case (int)Constants.eHANGOUT_PLACE_TYPE.STORE:
                    place_txt = "Stores";
                    break;
                case (int)Constants.eHANGOUT_PLACE_TYPE.THEATER:
                    place_txt = "Theaters";
                    break;
                case (int)Constants.eHANGOUT_PLACE_TYPE.THEME_PARK:
                    place_txt = "Theme Parks";
                    break;
                case (int)Constants.eHANGOUT_PLACE_TYPE.WEDDING_CHAPEL:
                    place_txt = "Wedding Chapels";
                    break;
                case -1:
                    place_txt = "All Worlds";
                    break;
            }

            return browse_txt;
        }

        protected void AdCreated(Object source, AdCreatedEventArgs e)
        {
            if (e.AdProperties["HeaderText"].ToString() != "")
            {

                litHeaderText.Text = e.AdProperties["HeaderText"].ToString();
                arHeader.Visible = false;
            }
        }

        #endregion


        #region Event Handlers

        protected void APCheckChanged(object sender, EventArgs e)
        {
            ShowOnlyAccessPassFilter = cbx_AccessPass.Checked;
            BindData(1);
        }

        /// <summary>
        /// Execute when the user clicks the the Search button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (KanevaWebGlobals.ContainsInjectScripts(txtKeywords.Text))
            {
                m_logger.Warn("User " + KanevaWebGlobals.GetUserId() + " tried to script from IP " + Common.GetVisitorIPAddress());
                ShowErrorOnStartup("Your input contains invalid scripting, please remove script code and try again.", true);
                return;
            }

            //Search_String = txtKeywords.Text.ToLower().Trim() != cTEXTBOX_INSTRUCTION ? txtKeywords.Text : "";
            Search_String = txtKeywords.Text.Trim();

            Order_By = Constants.SEARCH_ORDERBY_NEWEST;
            if (Search_String.Length > 0)
            {
                Order_By = Constants.SEARCH_ORDERBY_RELEVANCE;
            }

            New_Within = (int)Constants.eSEARCH_TIME_FRAME.ALL_TIME;

            InitializeSearch(false);
        }
        /// <summary>
        /// Execute when the user clicks the the view type link button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lbViewAs_Click(object sender, EventArgs e)
        {
            if (((LinkButton)sender).Attributes["View"].ToString().ToLower() == "thumb")
            {
                Is_Detail_View = false;
            }
            else
            {
                Is_Detail_View = true;
            }

            BindData(1);
        }
        /// <summary>
        /// Execute when the user clicks the Browse type link button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lbBrowse_Click(object sender, CommandEventArgs e)
        {
            bool usingDefault = false;

            switch (e.CommandName)
            {
                case Constants.SEARCH_ORDERBY_RAVES:
                case Constants.SEARCH_ORDERBY_VIEWS:
                case Constants.SEARCH_ORDERBY_MEMBERS:
                case Constants.SEARCH_ORDERBY_NEWEST:
                case Constants.SEARCH_ORDERBY_RELEVANCE:
                case Constants.SEARCH_ORDERBY_MOST_ACTIVE:
                case Constants.SEARCH_ORDERBY_VISITS:
                case Constants.SEARCH_ORDERBY_REQUESTS:
                case cORDER_BY_AP_ONLY:
                    Order_By = e.CommandName;
                    break;
                default:
                    usingDefault = true;
                    Order_By = Constants.SEARCH_ORDERBY_NEWEST;
                    break;
            }

            if (e.CommandName == cORDER_BY_AP_ONLY)
            {
                ShowOnlyAccessPassCategory = true;
            }
            else
            {
                ShowOnlyAccessPassCategory = false;
            }

            // If the above fails to find a match, check to see if we're using one of the game categories
            if (rptGameCategories.Visible == true && usingDefault)
            {
                HtmlContainerControl lItem = null;
                LinkButton lButton = null;

                foreach (RepeaterItem rItem in rptGameCategories.Items)
                {
                    lItem = (HtmlContainerControl)rItem.FindControl("liBrowse_GameCategories");
                    lButton = (LinkButton)lItem.FindControl("lbBrowse_GameCategories");

                    if (lButton.CommandName == e.CommandName)
                    {
                        Order_By = cORDER_BY_GAME_CATEGORY;
                        Selected_Game_Category = e.CommandName;
                        break;
                    }
                }
            }

            SetBrowseLinks();

            BindData(1);
        }
        /// <summary>
        /// Execute when the user clicks the Time range link button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lbTime_Click(object sender, CommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "today":
                    New_Within = (int)Constants.eSEARCH_TIME_FRAME.TODAY;
                    break;
                case "week":
                    New_Within = (int)Constants.eSEARCH_TIME_FRAME.WEEK;
                    break;
                case "month":
                    New_Within = (int)Constants.eSEARCH_TIME_FRAME.MONTH;
                    break;
                case "alltime":
                    New_Within = (int)Constants.eSEARCH_TIME_FRAME.ALL_TIME;
                    break;
            }

            SetTimeLinks();

            BindData(1);
        }

        /// <summary>
        /// Execute when the user clicks the Place drop down
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lbPlace_Click(object sender, EventArgs e)
        {

            PlaceType = Convert.ToInt32(drpPlace.SelectedValue);

            BindData(1);
        }

        /// <summary>
        /// Called after binding data to the channel detail list. Handles differences between
        /// Worlds and Communities.
        /// </summary>
        /// <param name="sender">The Detail DataList</param>
        /// <param name="e">The Event</param>
        protected void dlChannelDetail_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            // Get shared controls between communities and worlds
            HtmlContainerControl divIsAdult = (HtmlContainerControl)e.Item.FindControl("divIsAdult");
            HtmlAnchor aCommPic = (HtmlAnchor)e.Item.FindControl("aCommPic");
            HtmlImage img1 = (HtmlImage)e.Item.FindControl("Img1");
            HtmlAnchor aCommName = (HtmlAnchor)e.Item.FindControl("aCommName");
            HtmlContainerControl pDescription = (HtmlContainerControl)e.Item.FindControl("pDescription");

            // Set the data for the controls
            divIsAdult.Visible = (DataBinder.Eval(e.Item.DataItem, "is_adult").Equals ("Y"));
            aCommPic.HRef = GetBroadcastChannelUrl (DataBinder.Eval (e.Item.DataItem, "name_no_spaces").ToString ());
            img1.Src = GetBroadcastChannelImageURL (DataBinder.Eval(e.Item.DataItem, "thumbnail_small_path").ToString (), "sm");
            img1.Alt = "Visit the " + Server.HtmlDecode(DataBinder.Eval (e.Item.DataItem, "Name").ToString()) + " Community";
            aCommName.HRef = GetBroadcastChannelUrl (DataBinder.Eval (e.Item.DataItem, "name_no_spaces").ToString ());
            aCommName.Title = "Visit the " + Server.HtmlDecode(DataBinder.Eval (e.Item.DataItem, "Name").ToString()) + " Community";
            aCommName.InnerText = TruncateWithEllipsis (Server.HtmlDecode (DataBinder.Eval (e.Item.DataItem, "Name").ToString ()), 20);
            pDescription.InnerText = TruncateWithEllipsis (Server.HtmlDecode (DataBinder.Eval (e.Item.DataItem, "Description").ToString ()), 50);

            // Toggle visiblity of world and community detail rows
            HtmlContainerControl communityRow = (HtmlContainerControl)e.Item.FindControl("trCommunityInfo");
            communityRow.Visible = false;
            HtmlContainerControl worldRow = (HtmlContainerControl)e.Item.FindControl("trWorldInfo");
            worldRow.Visible = true;

            // Fill request container
            HtmlContainerControl requestCountCell = (HtmlContainerControl)e.Item.FindControl("tdRequestCount");
            if (Order_By == Constants.SEARCH_ORDERBY_REQUESTS)
            {
                int requestCount = Convert.ToInt32 (DataBinder.Eval (e.Item.DataItem, "number_of_requests"));
                if (requestCount <= 0)
                {
                    requestCountCell.InnerText = "";
                }
                else
                {
                    HtmlContainerControl requestCountContainer = (HtmlContainerControl)requestCountCell.FindControl("spnRequestCount");
                    requestCountContainer.InnerText = requestCount.ToString ();
                }
            }
            else
            {
                requestCountCell.InnerText = "";
            }

            // Configure Play Now button
            LinkButton btnMeet3D = (LinkButton) e.Item.FindControl ("btnMeet3D");
            ConfigureCommunityMeetMe3D(btnMeet3D, Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "game_id")), Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "community_id")), Convert.ToString(DataBinder.Eval(e.Item.DataItem, "Name")), TrackingRequestId);

            img1.Alt = "Visit the " + Server.HtmlDecode (DataBinder.Eval (e.Item.DataItem, "Name").ToString ()) + " World";
            aCommName.Title = "Visit the " + Server.HtmlDecode (DataBinder.Eval (e.Item.DataItem, "Name").ToString ()) + " World";
        }

        /// <summary>
        /// Called after binding data to the channel thumbnail list. Handles differences between
        /// Worlds and Communities.
        /// </summary>
        /// <param name="sender">The Detail DataList</param>
        /// <param name="e">The Event</param>
        protected void dlChannelThumb_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            // Get shared controls between communities and worlds
            HtmlContainerControl divIsAdult = (HtmlContainerControl)e.Item.FindControl("divIsAdult");
            HtmlContainerControl pRavesAndViews = (HtmlContainerControl)e.Item.FindControl("pRavesAndViews");
            HtmlContainerControl pMembers = (HtmlContainerControl)e.Item.FindControl("pMembers");
            HtmlAnchor aCommPic = (HtmlAnchor)e.Item.FindControl("aCommPic");
            HtmlImage imgCommPic = (HtmlImage)e.Item.FindControl("imgCommPic");
            HtmlAnchor aCommName = (HtmlAnchor)e.Item.FindControl("aCommName");
            LinkButton btnPlayNow = (LinkButton)e.Item.FindControl("btnPlayNow");

            // Set the data for the controls
            divIsAdult.Visible = (DataBinder.Eval(e.Item.DataItem, "is_adult").Equals("Y"));
            aCommPic.HRef = GetBroadcastChannelUrl(DataBinder.Eval(e.Item.DataItem, "name_no_spaces").ToString());
            aCommPic.Title = "Visit the " + Server.HtmlDecode(DataBinder.Eval(e.Item.DataItem, "Name").ToString()) + " Community";
            imgCommPic.Src = GetBroadcastChannelImageURL(DataBinder.Eval(e.Item.DataItem, "thumbnail_small_path").ToString(), "me");
            imgCommPic.Alt = "Visit the " + Server.HtmlDecode(DataBinder.Eval(e.Item.DataItem, "Name").ToString()) + " Community";
            aCommName.HRef = GetBroadcastChannelUrl(DataBinder.Eval(e.Item.DataItem, "name_no_spaces").ToString());
            aCommName.Title = Server.HtmlDecode(DataBinder.Eval(e.Item.DataItem, "Name").ToString());
            aCommName.InnerText = TruncateWithEllipsis(Server.HtmlDecode(DataBinder.Eval(e.Item.DataItem, "Name").ToString()), 16);
            pRavesAndViews.Visible = false;
            pMembers.Visible = false;
            btnPlayNow.Visible = true;
            LinkButton btnMeet3D = (LinkButton)e.Item.FindControl("btnMeet3D");
            ConfigureCommunityMeetMe3D(btnMeet3D, Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "game_id")), Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "community_id")), Convert.ToString(DataBinder.Eval(e.Item.DataItem, "Name")), TrackingRequestId);
        }
        
        /// <summary>
        /// Called to 'Love & Dating' category for minors
        /// </summary>
        protected void rptGameCategories_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            // Hide 'Love & Dating' category for minors
            if (!KanevaWebGlobals.CurrentUser.IsAdult && DataBinder.Eval(e.Item.DataItem, "description").ToString() == "Love & Dating")
            {
                e.Item.Visible = false;
            }
        }

        #region Pager & Filter control events
        /// <summary>
        /// Execute when the user selects a page change link from the Pager control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pg_PageChange(object sender, PageChangeEventArgs e)
        {
            BindData(e.PageNumber);
        }
        /// <summary>
        /// Execute when the user selects an item from the Store Filter
        /// Items to display drop down list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FilterChanged(object sender, FilterChangedEventArgs e)
        {
            BindData(1);
        }
        #endregion
        #endregion


        #region Properties

        /// <summary>
        /// indicator to show only access pass or not
        /// </summary>
        private bool ShowOnlyAccessPass
        {
            get
            {
                return (ShowOnlyAccessPassFilter || ShowOnlyAccessPassCategory);
            }
        }

        /// <summary>
        /// Called to limit a browse selection to Access Pass items
        /// </summary>
        private bool ShowOnlyAccessPassFilter
        {
            get
            {
                if (ViewState["showOnlyAPFilter"] == null)
                {
                    return false;
                }
                else
                {
                    return (bool)ViewState["showOnlyAPFilter"];
                }
            }
            set
            {
                ViewState["showOnlyAPFilter"] = value;
            }
        }

        /// <summary>
        /// Called to browse Access Pass Items
        /// </summary>
        private bool ShowOnlyAccessPassCategory
        {
            get
            {
                if (ViewState["showOnlyAPCategory"] == null)
                {
                    return false;
                }
                else
                {
                    return (bool)ViewState["showOnlyAPCategory"];
                }
            }
            set
            {
                ViewState["showOnlyAPCategory"] = value;
            }
        }

        /// <summary>
        /// searchTab
        /// </summary>
        private int Search_Type
        {
            get
            {
                return Convert.ToInt32(Constants.eCHANNEL_SEARCH_TYPE.NON_PERSONAL);
            }
        }
        /// <summary>
        /// newWithin
        /// </summary>
        private int New_Within
        {
            set
            {
                ViewState["New_Within"] = value;
            }
            get
            {
                if (ViewState["New_Within"] == null)
                {
                    return -1;
                }
                else
                {
                    return Convert.ToInt32(ViewState["New_Within"]);
                }
            }
        }
        /// <summary>
        /// placeType
        /// </summary>
        private int PlaceType
        {
            set
            {
                ViewState["PlaceType"] = value;
            }
            get
            {
                if (ViewState["PlaceType"] == null)
                {
                    return -1;
                }
                else
                {
                    return Convert.ToInt32(ViewState["PlaceType"]);
                }
            }
        }
        /// <summary>
        /// Search_String
        /// </summary>
        private string Search_String
        {
            set
            {
                ViewState["Search_String"] = value.Trim();
            }
            get
            {
                if (ViewState["Search_String"] == null)
                {
                    return string.Empty;
                }
                else
                {
                    return ViewState["Search_String"].ToString();
                }
            }
        }

        /// <summary>
        /// orderBy
        /// </summary>
        private string Order_By
        {
            set
            {
                ViewState["Order_By"] = value;
            }
            get
            {
                if (ViewState["Order_By"] == null)
                {
                    return string.Empty; //cORDER_BY_DEFAULT;
                }
                else
                {
                    return ViewState["Order_By"].ToString();
                }
            }
        }

        /// <summary>
        /// isDetailView
        /// </summary>
        private bool Is_Detail_View
        {
            set
            {
                ViewState["Is_Detail_View"] = value;
            }
            get
            {
                if (ViewState["Is_Detail_View"] == null)
                {
                    return false;
                }
                else
                {
                    return Convert.ToBoolean(ViewState["Is_Detail_View"]);
                }
            }
        }

        /// <summary>
        /// selected game category
        /// </summary>
        private string Selected_Game_Category
        {
            set
            {
                ViewState["Selected_Game_Category"] = value;
            }
            get
            {
                if (ViewState["Selected_Game_Category"] == null)
                {
                    return string.Empty;
                }
                else
                {
                    return ViewState["Selected_Game_Category"].ToString();
                }
            }
        }

        private string TrackingRequestId
        {
            set
            {
                ViewState["TrackingRequestId"] = value;
            }
            get
            {
                if (ViewState["TrackingRequestId"] == null && KanevaWebGlobals.CurrentUser.UserId > 0)
                {
                    string requestId = GetUserFacade.InsertTrackingRequest(Constants.cREQUEST_TRACKING_REQUEST_TYPE_ID_EXPLORE_WORLDS_PLAY_NOW, KanevaWebGlobals.CurrentUser.UserId, 0, 0);
                    ViewState["TrackingRequestId"] = GetUserFacade.InsertTrackingTypeSent(requestId, Constants.cREQUEST_TRACKING_MESSAGE_TYPE_ID_WEBSITE, KanevaWebGlobals.CurrentUser.UserId);
                }
                return (ViewState["TrackingRequestId"] ?? string.Empty).ToString();
            }
        }

        /// <summary>
        /// Current sort expression
        /// </summary>
        public string CurrentSort
        {
            get
            {
                if (ViewState["cs"] == null)
                {
                    return DEFAULT_SORT;
                }
                else
                {
                    return ViewState["cs"].ToString ();
                }
            }
            set
            {
                ViewState["cs"] = value;
            }
        }

        /// <summary>
        /// Current sort order
        /// </summary>
        public string CurrentSortOrder
        {
            get
            {
                if (ViewState["cso"] == null)
                {
                    return DEFAULT_SORT_ORDER;
                }
                else
                {
                    return ViewState["cso"].ToString ();
                }
            }
            set
            {
                ViewState["cso"] = value;
            }
        }

        protected virtual string DEFAULT_SORT
        {
            get
            {
                return "a.name";
            }
        }

        /// <summary>
        /// DEFAULT_SORT_ORDER
        /// </summary>
        /// <returns></returns>
        protected virtual string DEFAULT_SORT_ORDER
        {
            get
            {
                return "ASC";
            }
        }
        
        # endregion


        #region Declerations

        protected Kaneva.Pager pgTop, pgBottom;
        protected Kaneva.SearchFilter filterTop;
        protected Kaneva.usercontrols.TagCloud ucTagCloud;

        protected LinkButton btnSearch;
        protected TextBox txtKeywords;
        protected LinkButton lbThumbTop, lbDetailTop;
        protected LinkButton lbBrowse_Relevance, lbBrowse_Raves, lbBrowse_Views, lbBrowse_Members, lbBrowse_Newest, lbBrowse_MostActive, lbBrowse_MostVisited, lbBrowse_AccessPass;
        protected LinkButton lbTime_Today, lbTime_Week, lbTime_Month, lbTime_All;
        protected DropDownList drpPlace;
        protected Repeater rptGameCategories;
        protected CheckBox chkPhotoRequired;
        protected Label lblResultsBottom;
        //protected Label			lblCount;
        protected Literal litPng;

        protected AdRotator arHeader;
        protected Literal litHeaderText;
        protected HtmlImage imgRegister;
        protected HtmlAnchor aRegister;

        protected DataList dlChannelDetail, dlChannelThumb;
        protected HtmlAnchor lnk_AccesPass, aCreate3DApp, aCreateCommunity, aMyWorlds;
        protected CheckBox cbx_AccessPass;
        protected HtmlContainerControl divNoResults, div_AccessPassFilter;
        protected HtmlContainerControl liBrowse_Relevance, liBrowse_Raves, liBrowse_Views, liBrowse_Members, liBrowse_Newest, liBrowse_MostActive, liBrowse_MostVisited, liBrowse_AccessPass, liBrowse_Requests;
        protected HtmlContainerControl liTime_Today, liTime_Week, liTime_Month, liTime_All;
        protected HtmlContainerControl liBrowseLabel;
        protected HtmlContainerControl spnSearchFilterDesc, spnNew3DAppRequests;
        protected HtmlContainerControl ulTime, ulPlace;
        protected HtmlContainerControl trSearchFilterDesc;
        protected HtmlContainerControl tdResultViewMode;

        protected HtmlContainerControl h1ResultsCommType;
        protected Literal litFindType;

        private string cCSS_CLASS_SELECTED = "selected";

        private const string cORDER_BY_DEFAULT = Constants.SEARCH_ORDERBY_MEMBERS;
        private const string cORDER_BY_DEFAULT_3DAPPS = Constants.SEARCH_ORDERBY_MOST_ACTIVE;

        private const string cORDER_BY_GAME_CATEGORY= "gamecategory";
        private const string cORDER_BY_AP_ONLY = "aponly";

        private const int cNEW_WITHIN_DEFAULT = (int)Constants.eSEARCH_TIME_FRAME.WEEK;
        private const int cNEW_WITHIN_DEFAULT_3DAPP = (int)Constants.eSEARCH_TIME_FRAME.ALL_TIME;

        private const int cMOST_VISITED_SEARCH_CONSTRAINT = 100;

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #endregion


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            pgTop.PageChanged += new PageChangeEventHandler(pg_PageChange);
            pgBottom.PageChanged += new PageChangeEventHandler(pg_PageChange);
        }
        #endregion
    }
}
