<%@ Page language="c#" Codebehind="commOrder.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.commOrder" %>
<%@ Register TagPrefix="Kaneva" Namespace="KlausEnt.KEP.Kaneva.WebControls" Assembly="Kaneva.WebControls" %>

<asp:ValidationSummary ShowMessageBox="True" ShowSummary="False" Class="formError" id="valSum" DisplayMode="BulletList" runat="server" HeaderText="Please correct the following errors:"/>

<table cellpadding="20" cellspacing="0" border="0" width="560" ID="Table2">
	<tr>
	<td class="dateStamp">
		<span class="subHead"><asp:Label runat="server" id="lblAssetName"/></span><br><br>
						
	<br>
	<table ID="tblPurchase" runat="server" cellpadding="2" cellspacing="0" border="0" width="520" style="border: 1px solid #bbbbbb;">
		<tr>
		<td align="right" valign="middle" class="filter2">
		<img runat="server" src="~/images/spacer.gif" width="30" height="1"/><span class="header01"><b>Community Details: </b>&nbsp;
		</td>
		<td colspan="4" valign="middle"><span class="header01"><b>Community Purchase<b></b></span>
		</td>							
		</tr>
		<tr>
		<td align="center" valign="middle" class="header02" bgcolor="#999999">Community
		</td>
		<td class="header02" align="center" bgcolor="#999999" width="50%">Description
		</td>
		<td class="header02" bgcolor="#999999" align="center">Price</td>
		</tr>
		<tr>
		<td align="center" valign="middle" class="bodyText">
			<asp:Label runat="server" id="lblAssetName2"/>
		</td>
		<td class="bodyText" align="center">
			<u><asp:Label runat="server" id="lblAssetDesc"/></u>
		</td>
		<td class="bodyText" align="right" nowrap><b><asp:DropDownList class="filter2" id="drpSubscription" runat="Server" OnSelectedIndexChanged="drpSubscription_Change" AutoPostBack="True"/></b></td>
		</tr>
		<tr>							
		<td colspan="2" bgcolor="#dddddd" align="right" class="dateStamp">Your current &#1036;-Point total:</td>
		<td class="bodyText" align="right" bgcolor="#dddddd" nowrap><b><asp:Label runat="server" id="lblKPointTotal"/></b></td>
		</tr>
		<tr>
		<td colspan="2" align="right" valign="middle" class="bodyText">
			<span style="color: red;"><asp:Label runat="server" id="lblInsufficientPoints">*Insufficient &#1036;-Points to make purchase. Buy more &#1036;-Points:</asp:Label></span><asp:Label runat="server" id="lblBuyPoints">Buy &#1036;-Points?</asp:Label>
		</td>
		<td class="bodyText" align="right">
			<asp:DropDownList class="filter2" id="drpKPoints" runat="Server" OnSelectedIndexChanged="drpKPoints_Change" AutoPostBack="True"/>
		</td>
		</tr>
		<tr>							
		<td colspan="2" bgcolor="#dddddd" align="right" class="dateStamp">Your &#1036;-Point balance after this purchase:</td>
		<td class="bodyText" align="right" bgcolor="#dddddd" nowrap><b><asp:Label runat="server" id="lblKPointAfterPurchase"/></b></td>
		</tr>
		<tr>
		<td colspan="2" align="right" valign="middle" class="bodyText">
		<span class="dateStamp">Minimum Cash required for this transaction:</span>
		</td>
		<td class="bodyText" align="right"><b style="color: green;"><asp:Label runat="server" id="lblCashRequired"/></b></td>
		</tr>
		<tr>
			<td align="right" colspan="4"><br>
			<input type="button" class="bodyText" value="cancel" style="cursor: hand;" onclick="javascript: history.back()"/><asp:button id="btnMakePurchase" runat="Server" CausesValidation="False" onClick="btnMakePurchase_Click" class="Filter2" Text="make purchase"/><img runat="server" src="~/images/spacer.gif" width="10" height="10"/></td>
		</tr>
	</table>

	</td>
	</tr>
</table>
