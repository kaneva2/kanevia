///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Web;
using System.Net;
using System.Text;
using System.IO;

namespace KlausEnt.KEP.Kaneva
{
    public partial class oauth2callback : System.Web.UI.Page
    {
        string targetUrl = "https://api.cloudsponge.com/auth";

        protected void Page_Load (object sender, EventArgs e)
        {
            lblContent.Text = HttpProxy (targetUrl, Request);
        }

        /// <summary>
        /// Proxies a web request to the given URI without following redirection. 
        /// If the URI returns a redirect response, the redirect is returned. 
        /// </summary>
        /// <param name="endpoint">The URI to proxy a call to</param>
        /// <param name="origRequest">The request to be proxied</param>
        /// <returns>The content from the endpoint</returns>
        private string HttpProxy (string endpoint, HttpRequest origRequest)
        {
            // append the incoming querystring to the CloudSponge auth endpoint
            string uri = endpoint + "?" + origRequest.QueryString;
            // format the form data as: name1=value1&name2=value2&..
            string bodyParameters = origRequest.Form.ToString ();
            string method = origRequest.HttpMethod;

            HttpWebRequest webRequest = (HttpWebRequest) WebRequest.Create (uri);
            // Don't follow redirects, api.cloudsponge.com should return a redirect that should
            // be passed to the client UA 
            webRequest.AllowAutoRedirect = false;
            webRequest.Method = method;
            if (method == "POST")
            {
                // WindowsLive POSTs the token, so this page should POST to api.cloudsponge.com
                webRequest.ContentType = "application/x-www-form-urlencoded";
                byte[] bytes = Encoding.ASCII.GetBytes (bodyParameters);
                Stream os = null;
                try
                { // send the Post
                    webRequest.ContentLength = bytes.Length;   // Count bytes to send
                    os = webRequest.GetRequestStream ();
                    os.Write (bytes, 0, bytes.Length);         // Send it
                }
                catch (WebException ex)
                {
                    return "An error occurred while importing contacts: " + ex.Message;
                }
                finally
                {
                    if (os != null)
                    {
                        os.Close ();
                    }
                }
            }

            try
            {
                // get the response
                WebResponse webResponse = webRequest.GetResponse ();
                if (webResponse == null)
                {
                    return "An error occurred while importing contacts.";
                }

                // typically, this should redirect
                if (webResponse.Headers["Location"] != null)
                {
                    Response.Redirect (webResponse.Headers["Location"], true); // redirect and end processing now, nothing after this line will be executed.
                }

                // If we got here, the response didn't redirect so read the response and render it in the page. 
                StreamReader sr = new StreamReader (webResponse.GetResponseStream ());
                return sr.ReadToEnd ().Trim ();
            }
            catch (WebException ex)
            {
                return "An error occurred while importing contacts: " + ex.Message;
            }
        }
    }
}