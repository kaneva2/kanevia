<%@ Page language="c#" Codebehind="people.aspx.cs" MasterPageFile="~/masterpages/GenericPageTemplate.Master" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.people" %>
<%@ Register TagPrefix="Kaneva" TagName="SearchFilter" Src="../usercontrols/SearchFilter.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../usercontrols/Pager.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
			
<asp:content contentplaceholderid="cph_HeadData" id="cntHeader" runat="server">
			
<meta name="title" content="Kaneva. A Unique Online Community and 3D Social Network." />
<meta name="description" content="Create your Profile today to get a 3D Virtual World home to call your own. Make friends now and meet up with them in-world later."  />
<meta name="keywords" content="online community, social network, meet people, friends, networking, games, mmo, movies, 3D, virtual world, sharing photos, video, blog, bands, music, rate pics, digital, rpg, entertainment, join groups, forums, online social networking, caneva, kaneeva, kanneva, kineva" />

<link href="../css/new.css" type="text/css" rel="stylesheet">
<link href="../css/shadow.css" type="text/css" rel="stylesheet">
<link href="../css/base/buttons_new.css" type="text/css" rel="stylesheet">
<!--[if IE]> <style type="text/css">@import "../css/new_IE.css";</style> <![endif]-->

<script type="text/javascript" src="../jscript/kaneva.js"></script>

<style>

a.btnlarge {height:23px;width:173px;display:block;padding:4px 0 0 0;font-size:14px;font-weight:bold;text-align:center;}
a.btnlarge {margin:10px auto 20px auto;padding-top:6px;height:21px;}
a.btnxsmall {float:none;margin:0 auto;}
a.grey {color:#333;text-shadow:1px 1px #fff;}
.btnlarge.grey:link {background:url(../images/buttons/button_523x27_largeGrey.png) no-repeat 0 0;}
.btnlarge.grey:hover {background-position:-175px 0;text-decoration:none;}
.btnlarge.grey:active {background-position:-350px 0;}
.framesize-medium {text-align:center;}
.framesize-medium .imgconstrain {display:inline-block;;width:50px;height:50px;overflow:hidden;margin:0 auto 6px 0;}
.framesize-small .imgconstrain {display:inline-block;;width:50px;height:50px;overflow:hidden;margin:0 auto 6px 0;}
.framesize-small {border:none;}
.PageContainer {background-color:#fff;width:1004px;height:100%;border:solid 1px #ccc;border-top:none;padding-top:0;}
#divData {margin:0 0 0 20px;padding-top:0;}
#browseMenu .submenu {padding:0 0 10px 0;margin:5px 0 0 0;border-bottom:1px solid #dfdfdf;display:block;}
#browseMenu .submenu table {border-spacing: 5px;}
#browseMenu .submenu table input {margin:3px 3px 3px 4px;}
table {border-collapse:separate;}
strong {font-weight:bold;}
</style>

<script language="javascript" src="../jscript/prototype.js"></script>

</asp:content>

<asp:Content ID="cntUpload" runat="server" ContentPlaceHolderID="cph_Body">

    <div id="divLoading">
        <table height="300" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <th class="loadingText" id="divLoadingText">
                    Loading...
                    <div style="display: none">
                        <img src="../images/progress_bar.gif"></div>
                </th>
            </tr>
        </table>
    </div>

    <div id="divData" style="display: none">

<table cellpadding="0" cellspacing="0" border="0" class="newcontainer" align="center">
	<tr>
		<td colspan="3">
			<table  border="0" cellpadding="0" cellspacing="0" width="100%" class="newcontainerborder">
				<tr>
					<td class=""><img id="Img2" runat="server" src="~/images/spacer.gif" width="1" height="1"/></td>
					<td valign="top" class="newdatacontainer">
						<table width="100%" border="0" cellpadding="0" cellspacing="0" >
							<tr>
								<td width="968" align="left"  valign="top">
									<a name="top"></a>
									<table cellpadding="0" cellspacing="0" border="0" width="100%">
										<tr>
											<td width="783" valign="top">
											
												<asp:updatepanel id="up1" runat="server">
													<contenttemplate>

												<table border="0" cellspacing="0" cellpadding="0" width="100%">
													<tr>
														<td align="center">
												
															<div class="module">
																<span class="ct"><span class="cl"></span></span>
																
																<!-- basic search -->
																<table cellpadding="0" cellspacing="0" border="0" width="98%" align="center">
																	<tr>
																		
																		<td valign="top" width="380">
																			
																			<table cellpadding="5" cellspacing="0" border="0" style="margin-top:5px;">													  
																				<tr>
																					<td nowrap><h4>Find People</h4></td>
																					<td><asp:textbox id="txtKeywords" class="formKanevaText" style="width:250px;margin-left:10px;" maxlength="50" runat="server"></asp:textbox></td>
																				</tr>
																			</table>
																																												  
																		</td>										
																		<td width="90" align="center"><asp:linkbutton runat="server" class="btnxsmall grey" id="btnSearch" text="Search" onclick="btnSearch_Click"></asp:linkbutton></td>
																		<td valign="top" class="small">
																			<div id="divRestricted" runat="server">
																				Show Restricted Content: <asp:label id="lblShowRestricted" runat="server">No</asp:label>
																				<asp:hyperlink id="hlChangeProfile" runat="server">Change Profile Setting</asp:hyperlink></div>
																			<asp:checkbox id="chkPhotoRequired" runat="server" checked />Must Contain Thumbnail Photo<br/>
																		</td>
																	</tr>											
													
																</table>
																
																<span class="cb"><span class="cl"></span></span>
															</div>
														</td>
													</tr>
												</table>			 
												
													</contenttemplate>
												</asp:updatepanel>
												
												<table border="0" cellspacing="0" cellpadding="0" width="100%">
													<tr>
														<td width="180" valign="top">
															<div class="module whitebg">
																<span class="ct"><span class="cl"></span></span>
																
																<div id="sortlinks">
																	
																	<span id="spnSortBy" runat="server" style="display:none;">
																	<asp:updatepanel id="up2" runat="server">
																		<contenttemplate>
																	<ul>
																		<li class="header">Sort By</li>
																		<li id="liBrowse_Relevance" runat="server"><asp:linkbutton text="Relevance" id="lbBrowse_Relevance" commandname="relevance" oncommand="lbBrowse_Click" tooltip="View results by relevance" runat="server" /></li>
																		<li id="liBrowse_Active" runat="server"><asp:linkbutton text="Most Active" id="lbBrowse_Active" commandname="active" oncommand="lbBrowse_Click" tooltip="View results by most active members" runat="server" /></li>
																		<li id="liBrowse_Updated" runat="server"><asp:linkbutton text="Recently Updated" id="lbBrowse_Updated" commandname="recent_update" oncommand="lbBrowse_Click" tooltip="View results by most recently updated profiles" runat="server" /></li>
																		<li id="liBrowse_Login" runat="server"><asp:linkbutton text="Last Logged in" id="lbBrowse_Login" commandname="last_login" oncommand="lbBrowse_Click" tooltip="View results by last logged in members" runat="server" /></li>
																		<li id="liBrowse_Newest" runat="server"><asp:linkbutton text="Newest" id="lbBrowse_Newest" commandname="newest" oncommand="lbBrowse_Click" tooltip="View results by newest added" runat="server" /></li>
																	</ul>
																	
																	<ul>
																		<li class="header">Sort By</li>
																		<li>
																			<table cellpadding="0" cellspacing="5" border="0" width="92%" style="margin-left:-7px;">
																				<tr>
																					<td>
																						<asp:dropdownlist class="insideBoxText11" id="drpCountry_Search" runat="Server" style="width:150px;" />
																					</td>
																				</tr>
																				<tr>
																					<td>Within <asp:dropdownlist class="insideBoxText11" id="drpWithinMiles_Search" runat="Server">
																							<asp:listitem value="0" selected>Any</asp:listitem>
																							<asp:listitem value="10">10</asp:listitem>
																							<asp:listitem value="20">20</asp:listitem>
																							<asp:listitem value="50">50</asp:listitem>
																							<asp:listitem value="100">100</asp:listitem>
																							<asp:listitem value="250">250</asp:listitem>   
																						</asp:dropdownlist>&nbsp; miles of 
																					</td>												   
																				</tr>
																				<tr>
																					<td><asp:textbox id="txtZipcode_Search" size="10" maxlength="50" style="width: 100px;" cssclass="insideBoxText11" runat="server"></asp:textbox><br><span class="small">Zip or Cty/State</span></td>
																				</tr>
																			</table>
																		</li>
																	</ul>
																																		
																	<center><asp:linkbutton id="btnUpdate_Search" class="btnxsmall grey" runat="server" onclick="btnUpdate_Search_Click" text="Update"></asp:linkbutton></center>
																	<br/>
																	
																		</contenttemplate>
																	</asp:updatepanel>
																	</span>
																	
																		
																	<span id="spnBrowse" runat="server" style="display:block;">
																	
			                                                        <div id="div_AccessPassFilter" runat="server" style="text-align:left; padding-left:4px; padding-bottom:4px; margin-bottom:10px;font-size:11px;">
                                                                        <asp:CheckBox id="cbx_AccessPass" runat="server" AutoPostBack="true" OnCheckedChanged="APCheckChanged" /> Show only <a id="lnk_AccesPass" href="#" runat="server" >Access Pass</a> <span style="padding-left:24px">content</span> 
		                                                            </div>

																<div id="browseMenu">
																		<div onclick="SwitchMenu('sub1')" class="menutitle"><img id="sub1img" src="../images/arrow_pointdown.gif" />  Gender</div>
																		<span id="sub1" class="submenu">	
																			<table cellpadding="0" cellspacing="5" border="0" width="99%">
																				<tr>																		   
																					<td valign="top">
																						<asp:radiobuttonlist runat="server" repeatdirection="Horizontal" repeatlayout="Table"   
																							borderwidth="0" cellpadding="2" id="rblGender"  style="border-spacing:5px;border-collapse:separate;">
																							<asp:listitem selected value="both">All</asp:listitem>
																							<asp:listitem value="male">Men</asp:listitem>
																							<asp:listitem value="female">Women</asp:listitem>
																						</asp:radiobuttonlist>
																					</td>
																				</tr>
																			</table>																			   
																		</span>
																		
																		<div onclick="SwitchMenu('sub2')" class="menutitle"><img id="sub2img" src="../images/arrow_pointdown.gif" />  Age</div>
																		<span id="sub2" class="submenu">	
																			
																			<table cellpadding="0" cellspacing="5" border="0" width="99%">
																				<tr>
																					<td align="left">
																						<asp:dropdownlist class="insideBoxText11" id="drpAgeFrom" runat="Server" style="width:50px"/>
																						&nbsp;to
																						<asp:dropdownlist class="insideBoxText11" id="drpAgeTo" runat="Server" style="width:50px"/>
																					</td>
																				</tr>
																			</table>
																			
																		</span>
																		
																		<div onclick="SwitchMenu('sub3')" class="menutitle"><img id="sub3img" src="../images/arrow_pointdown.gif" />  Location</div>
																		<span id="sub3" class="submenu">	
																			<table cellpadding="0" cellspacing="5" border="0" width="99%">
																				
																				<tr>
																					<td>
																						<asp:dropdownlist class="insideBoxText11" id="drpCountry" runat="Server" style="width:158px;" />
																					</td>
																				</tr>
																				<tr>
																					<td>Within <asp:dropdownlist class="insideBoxText11" id="drpWithinMiles" runat="Server">
																							<asp:listitem value="0" selected>Any</asp:listitem>
																							<asp:listitem value="10">10</asp:listitem>
																							<asp:listitem value="20">20</asp:listitem>
																							<asp:listitem value="50">50</asp:listitem>
																							<asp:listitem value="100">100</asp:listitem>
																							<asp:listitem value="250">250</asp:listitem>   
																						</asp:dropdownlist>&nbsp; miles of 
																					</td>												   
																				</tr>
																				<tr>
																					<td><asp:textbox id="txtZipcode" size="10" maxlength="50" style="width: 100px;" cssclass="insideBoxText11" runat="server"></asp:textbox><br><span class="small">Zip or Cty/State</span></td>
																				</tr>
																				<tr>
																					<td style="padding-top:5px;">
																					    <strong>Hometown:</strong><br/>
                                                                                        <asp:textbox id="txtHometown" class="formKanevaText" style="width:160px;" maxlength="80" runat="server"></asp:textbox>
																					</td>
																				</tr>
																			</table>
																		</span>
																		
																		<div onclick="SwitchMenu('sub4')" class="menutitle"><img id="sub4img" src="../images/arrow_pointdown.gif" />  Sort Order</div>
																		<span id="sub4" class="submenu">	
																			
																			<table cellpadding="0" cellspacing="5" border="0" width="99%">
																				<tr>
																					<td align="left">
																						<asp:dropdownlist class="insideBoxText11" id="drpSortOrder" runat="Server">
																							<asp:listitem value="active">Most Active</asp:listitem>
																							<asp:listitem value="newest" selected>Newest</asp:listitem>
																							<asp:listitem value="recent_update">Recently Updated</asp:listitem>
																							<asp:listitem value="last_login">Last Logged In</asp:listitem>   
																						</asp:dropdownlist>
																					</td>
																				</tr>
																			</table>
																			
																		</span>
																		
																		<div onclick="SwitchMenu('sub5')" class="menutitle"><img id="sub5img" src="../images/arrow_pointright.gif" />  Interests </div>
																		<span id="sub5" class="submenu">	
																			<table cellpadding="0" cellspacing="5" border="0" width="99%">
																				<tr>
																					<td valign="top">
																					    <strong>Interest:</strong><br>
                                                                                        <asp:textbox id="txtInterests" class="formKanevaText" style="width:160px;" maxlength="80" runat="server"></asp:textbox>
																					</td>
																				</tr>
																			</table>																		
																		</span>
																		
																		<div onclick="SwitchMenu('sub6')" class="menutitle"><img id="sub6img" src="../images/arrow_pointright.gif" />  Relationship & more</div>
																		<span id="sub6" class="submenu">	
																			<table cellpadding="0" cellspacing="5" border="0" width="99%">
																				<tr>
																					<td valign="top">
																						<strong>Relationship:</strong><br>
																						<asp:checkbox id="chkSingle" runat="server"/>Single<br/>
																						<asp:checkbox id="chkIna" runat="server"/>In a relationship<br/>
																						<asp:checkbox id="chkMarried" runat="server"/>Married<br/>
																						<asp:checkbox id="chkDivorced" runat="server"/>Divorced<br/>
																						<asp:checkbox id="chkSwinger" runat="server"/><asp:label id="lblSwinger" runat="server">Swinger<br/></asp:label>
																						<asp:checkbox id="chkFreaky" runat="server"/><asp:label id="lblFreaky" runat="server">Freaky<br/></asp:label>
																						<br>
																						<strong>Orientation:</strong><br>
																						<asp:checkbox id="chkStraight" runat="server"/>Straight<br/>
																						<asp:checkbox id="chkGay" runat="server"/>Gay/lesbian<br/>
																						<asp:checkbox id="chkBi" runat="server"/>Bi-sexual<br/>
																						<asp:checkbox id="chkFreak" runat="server"/><asp:label id="lblFreak" runat="server">Freak<br/></asp:label>
																						<asp:checkbox id="chkNotSure" runat="server"/>Not sure<br/>
																						
																						<div id="Div1" visible="false" runat="server">
																						<br>
																						<strong>Looking for:</strong><br>
																						<asp:checkbox id="chkFriendship" runat="server"/>Friendship<br/>
																						<asp:checkbox id="chkDating" runat="server"/>Dating<br/>
																						<asp:checkbox id="chkRelationship" runat="server"/>A relationship<br/>
																						<asp:checkbox id="chkRandom" runat="server"/>Random play<br/>
																						<asp:checkbox id="chkAnything" runat="server"/>Anything and everything<br/>
																						<asp:checkbox id="chkNetworking" runat="server"/>Networking<br/>
																						<asp:checkbox id="chkBusiness" runat="server"/>Marketing/Business<br/>
																						</div>
																					</td>
																				</tr>
																			</table>																		
																		</span>
																		
																		<div onclick="SwitchMenu('sub7')" class="menutitle"><img id="sub7img" src="../images/arrow_pointright.gif" />  Ethnicity & Religion</div>
																		<span id="sub7" class="submenu">	
																			<table cellpadding="0" cellspacing="5" border="0" width="99%" >
																				<tr>
																					<td align="left"> 
																					<strong>Ethnicity:</strong><br>
																						<asp:dropdownlist runat="server" class="insideBoxText11" id="drpEthnicity" style="width:150px">
																							<asp:listitem value="">Any Ethnicity</asp:listitem>
																							<asp:listitem value="Asian">Asian</asp:listitem>
																							<asp:listitem value="Black">Black</asp:listitem>
																							<asp:listitem value="East Indian">East Indian</asp:listitem>
																							<asp:listitem value="European">European</asp:listitem>
																							<asp:listitem value="Latino/Hispanic">Latino/Hispanic</asp:listitem>
																							<asp:listitem value="Middle Eastern">Middle Eastern</asp:listitem>
																							<asp:listitem value="Mixed">Mixed</asp:listitem>
																							<asp:listitem value="Native American">Native American</asp:listitem>
																							<asp:listitem value="Other">Other</asp:listitem>
																							<asp:listitem value="Pacific Rim">Pacific Rim</asp:listitem>
																							<asp:listitem value="White">White</asp:listitem>
																						</asp:dropdownlist>
																					<br><br>
																					<strong>Religion:</strong><br>
																						<asp:dropdownlist runat="server" class="insideBoxText11" id="drpReligion" style="width:150px">
																							<asp:listitem value="">Any Religion</asp:listitem>
																							<asp:listitem value="Agnostic">Agnostic</asp:listitem>
																							<asp:listitem value="Atheist">Atheist</asp:listitem>
																							<asp:listitem value="Buddhist">Buddhist</asp:listitem>
																							<asp:listitem value="Catholic">Catholic</asp:listitem>
																							<asp:listitem value="Christian (Other)">Christian (Other)</asp:listitem>
																							<asp:listitem value="Hindu">Hindu</asp:listitem>
																							<asp:listitem value="Jewish">Jewish</asp:listitem>
																							<asp:listitem value="Mormon">Mormon</asp:listitem>
																							<asp:listitem value="Muslim">Muslim</asp:listitem>
																							<asp:listitem value="Other">Other</asp:listitem>
																							<asp:listitem value="Protestant">Protestant</asp:listitem>
																							<asp:listitem value="Scientologist">Scientologist</asp:listitem>
																							<asp:listitem value="Taoist">Taoist</asp:listitem>
																							<asp:listitem value="Wiccan">Wiccan</asp:listitem>
																						</asp:dropdownlist>
																					</td>
																				</tr>
																			</table>																		
																		</span>
																		
																		<div onclick="SwitchMenu('sub8')" class="menutitle"><img id="sub8img" src="../images/arrow_pointright.gif" />  Education</div>
																		<span id="sub8" class="submenu">	
																			<table cellpadding="0" cellspacing="5" border="0" width="99%">
																				<tr>
																					<td valign="top">
																					    <strong>School:</strong><br/>
                                                                                        <asp:textbox id="txtSchool" class="formKanevaText" style="width:160px;" maxlength="80" runat="server"></asp:textbox>
																					</td>
																				</tr>
																				<tr>
																					<td>
																						<strong>Education:</strong><br/>
																						<asp:checkbox id="chkDropout" runat="server"/>Dropout<br/>
																						<asp:checkbox id="chkHighSchool" runat="server"/>High School<br/>
																						<asp:checkbox id="chkSomeCollege" runat="server"/>Some college<br/>
																						<asp:checkbox id="chkInCollege" runat="server"/>In college<br/>
																						<asp:checkbox id="chkGraduate" runat="server"/>Bachelors degree<br/>
																						<asp:checkbox id="chkMasters" runat="server"/>Masters degree<br/>
																						<asp:checkbox id="chkDoctorate" runat="server"/>Doctorate<br/>
																					</td>
																				</tr>
																			</table>
																		</span>
																		
																		<div onclick="SwitchMenu('sub9')" class="menutitle"><img id="sub9img" src="../images/arrow_pointright.gif" />  Height & Habits</div>
																		<span id="sub9" class="submenu">	
																			<table cellpadding="0" cellspacing="5" border="0" width="99%">
																				<tr>
																					<td align="left"><strong>Height from:</strong>
																					<br>
																						<asp:dropdownlist runat="server" class="insideBoxText11" id="drpHeightFeet">
																							<asp:listitem  value="0">-</asp:listitem>
																							<asp:listitem  value="3">3</asp:listitem>
																							<asp:listitem  value="4">4</asp:listitem>
																							<asp:listitem  value="5">5</asp:listitem>
																							<asp:listitem  value="6">6</asp:listitem>
																							<asp:listitem  value="7">7</asp:listitem>
																						</asp:dropdownlist> ft
																						<asp:dropdownlist runat="server" class="insideBoxText11" id="drpHeightInches">
																							<asp:listitem  value="-1">-</asp:listitem>
																							<asp:listitem  value="0">0</asp:listitem>
																							<asp:listitem  value="1">1</asp:listitem>
																							<asp:listitem  value="2">2</asp:listitem>
																							<asp:listitem  value="3">3</asp:listitem>
																							<asp:listitem  value="4">4</asp:listitem>
																							<asp:listitem  value="5">5</asp:listitem>
																							<asp:listitem  value="6">6</asp:listitem>
																							<asp:listitem  value="7">7</asp:listitem>
																							<asp:listitem  value="8">8</asp:listitem>
																							<asp:listitem  value="9">9</asp:listitem>
																							<asp:listitem  value="10">10</asp:listitem>
																							<asp:listitem  value="11">11</asp:listitem>
																						</asp:dropdownlist> inches <br/>
																						
																						<strong>to:</strong><br>
																						
																						<asp:dropdownlist runat="server" class="insideBoxText11" id="drpHeightFeetTo">
																							<asp:listitem  value="0">-</asp:listitem>
																							<asp:listitem  value="3">3</asp:listitem>
																							<asp:listitem  value="4">4</asp:listitem>
																							<asp:listitem  value="5">5</asp:listitem>
																							<asp:listitem  value="6">6</asp:listitem>
																							<asp:listitem  value="7">7</asp:listitem>
																						</asp:dropdownlist> ft
																						<asp:dropdownlist runat="server" class="insideBoxText11" id="drpHeightInchesTo">
																							<asp:listitem  value="-1">-</asp:listitem>
																							<asp:listitem  value="0">0</asp:listitem>
																							<asp:listitem  value="1">1</asp:listitem>
																							<asp:listitem  value="2">2</asp:listitem>
																							<asp:listitem  value="3">3</asp:listitem>
																							<asp:listitem  value="4">4</asp:listitem>
																							<asp:listitem  value="5">5</asp:listitem>
																							<asp:listitem  value="6">6</asp:listitem>
																							<asp:listitem  value="7">7</asp:listitem>
																							<asp:listitem  value="8">8</asp:listitem>
																							<asp:listitem  value="9">9</asp:listitem>
																							<asp:listitem  value="10">10</asp:listitem>
																							<asp:listitem  value="11">11</asp:listitem>
																						</asp:dropdownlist> inches 
																					</td>
																				</tr>
																				<tr>
																					<td>
																					
																						<br>
																						<strong>Smoking:</strong><br>
																						<asp:radiobuttonlist runat="server" repeatdirection="Vertical" repeatlayout="Flow" id="rblSmoke">
																							<asp:listitem value="Yes">Yes</asp:listitem>
																							<asp:listitem value="No">No</asp:listitem>
																							<asp:listitem selected value="">Don't Care</asp:listitem>
																						</asp:radiobuttonlist><br><br>
																						<strong>Drinking:</strong><br>
																						<asp:radiobuttonlist runat="server" repeatdirection="Vertical" repeatlayout="Flow" id="rblDrink">
																							<asp:listitem value="Yes">Yes</asp:listitem>
																							<asp:listitem value="No">No</asp:listitem>
																							<asp:listitem selected value="">Don't Care</asp:listitem>
																						</asp:radiobuttonlist>
																						
																					</td>
																				</tr>
																			</table>	
																			
																		</span>
																	</div>
																		
																	<asp:updatepanel id="up3" runat="server">
																		<contenttemplate>
																			<center><asp:linkbutton id="btnUpdate" class="btnxsmall grey" style="margin-top:15px;" runat="server" onclick="btnUpdate_Click" text="Update"></asp:linkbutton></center>
																		</contenttemplate>
																	</asp:updatepanel>
																	<div style="text-align:left;padding:10px 5px 0 5px;">
																	<asp:checkbox id="chkOnlineNow" runat="server"/><span id="Span2" runat="server">Online now</span><br/>
																	<span id="spnRestrictedContent" runat="server"><asp:checkbox id="chkRestrictedPeople" runat="server"/>Restricted content<br/></span>
																	</div>
																	</span>
																	
																		
																</div>
																
																<span class="cb"><span class="cl"></span></span>
															</div>
															
														</td>
														<td valign="top" align="center">
														
															<asp:updatepanel id="up4" runat="server">
																<contenttemplate>
															
															<table border="0" cellspacing="0" cellpadding="0" width="92%" class="nopadding">
                                                                <tr>
                                                                    <td width="170" rowspan="2"><h1>People</h1></td>
                                                                    <td height="16" class="searchnav" align="right"><kaneva:searchfilter visible="false" runat="server" id="filterTop" loc="top" hidethumbview="true" showfilterlists="false" assetsperpagelist="12,24,40" /> Showing as: 
                                                                    	<asp:linkbutton id="lbThumbTop" runat="server" view="Thumb"  onclick="lbViewAs_Click">Thumbnails</asp:linkbutton>&nbsp;| 
																		<asp:linkbutton id="lbDetailTop" runat="server" view="Detail" onclick="lbViewAs_Click">Details</asp:linkbutton>
																	</td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="16">
																		<table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                            <tr>
                                                                                <td class="searchnav" align="right" valign="bottom" nowrap>
                                                                                    <kaneva:pager runat="server" id="pgTop" maxpagestodisplay="5" shownextprevlabels="true" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2"><h2><span id="spnSearchFilterDesc" runat="server"></span></h2></td>
                                                                </tr>
                                                            </table>
			
						
															<!-- PEOPLE DETAIL VIEW -->
															<asp:datalist visible="true" runat="server" enableviewstate="False" showfooter="False" width="590" id="dlPeopleDetail" style="margin-top:5px;"
																cellpadding="5" cellspacing="0" border="0" itemstyle-horizontalalign="Center" repeatcolumns="2" repeatdirection="Horizontal" cssclass="detail_table">
																
																<itemtemplate>	
																	
																	<div class="module whitebg">
																		<span class="ct"><span class="cl"></span></span>
																		<table cellpadding="0" cellspacing="0" border="0" width="280">
																			<tr>
																				<td width="85" valign="top" align="center">
																					<div class="framesize-small">
																						<div id="Div2" class="restricted" runat="server" visible='<%# Convert.ToInt32(DataBinder.Eval(Container.DataItem, "MatureProfile")).Equals(1) %>'></div>
																						<div class="">
																							<div class="imgconstrain">
																								<a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "NameNoSpaces").ToString ())%> style="cursor: hand;">
																									<img id="Img5" runat="server" src='<%#GetProfileImageURL (DataBinder.Eval(Container.DataItem, "ThumbnailSquarePath").ToString (), "sq", DataBinder.Eval(Container.DataItem, "Gender").ToString (), Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "FacebookSettings.UseFacebookProfilePicture")), Convert.ToUInt64(DataBinder.Eval(Container.DataItem, "FacebookSettings.FacebookUserId")) )%>' border="0"/></a>
																							</div>	
																						</div>
																					</div>
																				</td>																		 
																				<td align="left" valign="top" width="264" style="padding-bottom:0px;">
																					<p><a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "NameNoSpaces").ToString ())%> style="cursor:hand;"><%# DataBinder.Eval(Container.DataItem, "DisplayName") %></a></p>
																					<p>"<%# DataBinder.Eval(Container.DataItem, "Username") %>"</p>
																					<div style="height:56px;">
																					<p><%# TruncateWithEllipsis (Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "Location").ToString ()), 25)%></p>
																						<br/>
																						<span class="small details" style="height:8px;display:block;""><%# GetOnlineText (DataBinder.Eval(Container.DataItem, "Ustate").ToString ()) %></span>
																					</div>
																				</td>
																			</tr>
																			<tr>
																				<td colspan="2" style="padding:0 8px 0 10px;">
																					<table cellpadding="0" cellspacing="0" width="100%" border="0" class="stats">
																						<tr>
																							<td align="left" width="36%" style="padding-top:0px;">
																								<p class="small">Raves: <%# (Convert.ToDouble(DataBinder.Eval (Container.DataItem, "NumberOfDiggs"))).ToString("N0") %></p>
																								<p class="small">Views: <%# (Convert.ToDouble(DataBinder.Eval (Container.DataItem, "NumberOfViews"))).ToString("N0") %></p>
																							</td>
																							<td align="right">
																								<p class="small">Member Since:<br/><%# FormatDate((DateTime)(DataBinder.Eval(Container.DataItem, "SignupDate")))%></p>
																							</td>	
																						</tr>
																					</table>	
																				</td>
																			</tr>
																		</table>
																		<span class="cb"><span class="cl"></span></span>
																	</div>
																	
																</itemtemplate>
															</asp:datalist>
															<!-- END PEOPLE DETAIL VIEW -->
																																  
															<!-- PEOPLE THUMB VIEW -->
															<asp:datalist visible="true" runat="server" enableviewstate="False" showfooter="False" width="99%" id="dlPeopleThumb"
																cellpadding="0" cellspacing="0" border="0" itemstyle-width="25%"  itemstyle-horizontalalign="Center" itemstyle-verticalalign="Top" 
																repeatcolumns="4" repeatdirection="Horizontal" cssclass="thumb_table" style="padding:0px;">
																
																<itemtemplate>													   
																																											
																	<div class="framesize-medium">
																		<div id="Div3" class="restricted" runat="server" visible='<%# Convert.ToInt32(DataBinder.Eval(Container.DataItem, "MatureProfile")).Equals(1) %>'></div>
																		<div class="">
																			<div class="imgconstrain">
																				<a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "NameNoSpaces").ToString ())%> title='<%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "Username").ToString ())%>' style="cursor: hand;">
																					<img id="Img6" runat="server" src='<%#GetProfileImageURL (DataBinder.Eval(Container.DataItem, "ThumbnailSquarePath").ToString (), "sq", DataBinder.Eval(Container.DataItem, "Gender").ToString (), Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "FacebookSettings.UseFacebookProfilePicture")), Convert.ToUInt64(DataBinder.Eval(Container.DataItem, "FacebookSettings.FacebookUserId")) )%>' border="0"/>
																				</a>
																			</div>	
																		</div>
																	</div>
																	<p><a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "NameNoSpaces").ToString ()) %>><%# TruncateWithEllipsis (DataBinder.Eval(Container.DataItem, "DisplayName").ToString(), 16) %></a></p>
																	<p>"<%# TruncateWithEllipsis (DataBinder.Eval(Container.DataItem, "Username").ToString(), 16) %>"</p>
																	<p class="small"><%# TruncateWithEllipsis (DataBinder.Eval(Container.DataItem, "Location").ToString(),18) %></p>
																	<span class="small thumbnail" style="height:8px;display:block;"><%# GetOnlineText (DataBinder.Eval(Container.DataItem, "Ustate").ToString ()) %></span>
																	<p class="small" runat="server" visible="<%#AllowedToEdit()%>" id="P1">
																		<asp:hyperlink id="hlRestrict" runat="server" visible='<%# !Convert.ToInt32(DataBinder.Eval(Container.DataItem, "MatureProfile")).Equals(1) %>' navigateurl='<%#GetRestrictScript ((int) DataBinder.Eval(Container.DataItem, "UserId"), true) %>' tooltip="Restrict" text="Restrict"/>
																		<asp:hyperlink id="hlRemoveRestriction" runat="server" visible='<%# Convert.ToInt32(DataBinder.Eval(Container.DataItem, "MatureProfile")).Equals(1) %>' navigateurl='<%#GetRestrictScript ((int) DataBinder.Eval(Container.DataItem, "UserId"), false) %>' tooltip="Remove Restriction" text="Un-Restrict"/>
																		&nbsp;&nbsp;&nbsp;
																		<asp:hyperlink id="hlBan" runat="server" visible='<%# !Convert.ToInt32(DataBinder.Eval(Container.DataItem, "StatusId")).Equals(5) %>' navigateurl='<%#GetBanScript ((int) DataBinder.Eval(Container.DataItem, "UserId"), true) %>' tooltip="Ban" text="Ban"/>
																		<asp:hyperlink id="hlRemoveBan" runat="server" visible='<%# Convert.ToInt32(DataBinder.Eval(Container.DataItem, "StatusId")).Equals(5) %>' navigateurl='<%#GetBanScript ((int) DataBinder.Eval(Container.DataItem, "UserId"), false) %>' tooltip="Remove Ban" text="Remove Ban"/>
																	</p>
																	
																</itemtemplate>
															
															</asp:datalist>		
															<!-- END PEOPLE THUMB VIEW -->
																
													
															<div id="divNoResults" runat="server" visible="false"></div>
						
															<div class="formspacer"></div>
															
															<table cellpadding="0" cellspacing="0" border="0" width="92%">
																<tr>
																	<td><asp:label id="lblResultsBottom" runat="server"></asp:label></td>
																	
																	<td class="searchnav" align="right" valign="bottom" nowrap>
																		<kaneva:pager runat="server" id="pgBottom" maxpagestodisplay="5" shownextprevlabels="true" />
																	</td>
																</tr>
															</table>
									
																</contenttemplate>
															</asp:updatepanel>
															
															<div class="formspacer"></div>
															
															
														</td>
													</tr>
												</table>
								
											</td>
											
											<td width="10"></td>
											
											<td width="175" align="center" valign="top">
												
												<div class="fb" id="fbConnect" runat="server" visible="false">
													<span style="display:none;">
														<h2 runat="server" id="h3InviteTitle" visible="false" style="margin:0;padding:0 0 3px 0;font-size:14px;text-align:center;color:#073b53;">Invite Your Friends!</h2>
													
														<a id="aRegister" visible="false" runat="server" href="javascript:void(0);" onclick="FBLogin(FBAction.CONNECT);" class="btnlarge grey"><span class="addfriend" id="spanRegister" runat="server">Invite Friends</span></a>
													
														<img id="imgFBConnect" style="cursor:pointer;" runat="server" onclick="FBLogin(FBAction.CONNECT);" runat="server" src="~/images/facebook/FB_ConnectwithFriendsstacked.png" />
														<div runat="server" id="divInviteFriends" visible="false">or you can invite friends<br />by <a id="A1" href="~/mykaneva/invitefriend.aspx" runat="server">email</a></div>
													</span>
												</div>

												<a class="black btnmedium" href="~/mykaneva/invitefriend.aspx" runat="server"><span>Invite Friends</span></a>
												
												<div class="module whitebg" style="display:none;">
													<span class="ct"><span class="cl"></span></span>
														<table id="_ctl97_tblGoogleAdds" cellpadding="0" cellspacing="0">
															<tr>
																<td align="center" style="padding-bottom:5px">
																<div id="goog">
																<script type="text/javascript"><!--
																	google_ad_client = "pub-7044624796952740";
																	//google_ad_client = "pub-googleIamTesting";
																	google_ad_width = 160;
																	google_ad_height = 600;
																	google_ad_format = "160x600_as";
																	google_ad_type = "text";
																	//2006-09-26: Search Results
																	google_ad_channel ="5505473784";
																	google_color_border = "FFFFFF";
																	google_color_bg = "FFFFFF";
																	google_color_link = "018AAA";
																	google_color_text = "333333";
																	google_color_url = "666666";
																	<asp:Literal runat="server" id="litAdTest"/>;
																	//--></script>
																	<script type="text/javascript"
																	src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
																	</script>
																</div>
																</td>
															</tr>
														</table>
														
													<span class="cb"><span class="cl"></span></span>
												</div>
											</td>
										</tr>
									</table>
		
								</td>
							</tr>
						</table>
					</td>
					<td class=""><img id="Img7" runat="server" src="~/images/spacer.gif" width="1" height="1"/></td>
				</tr>
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
			</table>				
		</td>
	</tr>
</table>

<input type="hidden" runat="server" id="txtUserId" value='0' name="txtUserId"> 

<asp:button id="btnBanUser" onclick="BanUser" runat="server" visible="false"></asp:button>
<asp:button id="btnRemoveUserBan" onclick="RemoveUserBan" runat="server" visible="false"></asp:button>
<asp:button id="btnRestrictUser" onclick="RestrictUser" runat="server" visible="false"></asp:button>
<asp:button id="btnRemoveUserRestriction" onclick="RemoveUserRestriction" runat="server" visible="false"></asp:button>

</div>
			
<script type="text/javascript">
function scroll()
{
	document.location.href = '#top';
}

var isNewSearch = true;

</script>



<script type="text/javascript">

/***********************************************
* Switch Menu script- by Martial B of http://getElementById.com/
* Modified by Dynamic Drive for format & NS4/IE4 compatibility
* Visit http://www.dynamicdrive.com/ for full source code
***********************************************/

var persistmenu="yes" //"yes" or "no". Make sure each SPAN content contains an incrementing ID starting at 1 (id="sub1", id="sub2", etc)
var persisttype="sitewide" //enter "sitewide" for menu to persist across site, "local" for this page only
var arSections = new Array(9)

if (document.getElementById) { //DynamicDrive.com change	  
	document.write('<style type="text/css">\n')
	document.write('.submenu{display: none;}\n')
	document.write('</style>\n')
												  
	if(document.getElementById)
	{
		var ar = $('browseMenu').getElementsByTagName("span");
		var j = 0;
		for (var i=0; i<ar.length; i++)
		{
			if (ar[i].className=="submenu") //DynamicDrive.com change
			{
				if (i < 5)
					ar[i].style.display = "block";
				else
					ar[i].style.display = "none";
			
				arSections[j] = ar[i];
				j++;
			}
		}	
	}
}

function SwitchMenu(obj){				
	if(document.getElementById){   
	var el = document.getElementById(obj);
	var ar = document.getElementById("browseMenu").getElementsByTagName("span"); 
		if(el.style.display != "block"){ 
			el.style.display = "block";
			$(obj+'img').src = arrow_down.src;
		}else{
			el.style.display = "none";
			$(obj+'img').src = arrow_right.src;
		}
	}	
}

function get_cookie(Name) {		
	var search = Name + "="
	var returnvalue = "";
	if (document.cookie.length > 0) {
		offset = document.cookie.indexOf(search)
		if (offset != -1) { 
			offset += search.length
			end = document.cookie.indexOf(";", offset);
			if (end == -1) end = document.cookie.length;
				returnvalue=unescape(document.cookie.substring(offset, end))
		}
	}
	return returnvalue;
}

function onloadfunction(){	 
	//if (persistmenu=="yes"){
	//	var cookiename=(persisttype=="sitewide")? "switchmenu" : window.location.pathname
	//	var cookievalue=get_cookie(cookiename)
	//	if (cookievalue!="")
	//		document.getElementById(cookievalue).style.display="block"
	//}
	for (var i=0;i<arSections.length; i++)
	{
		$(arSections[i].id).style.display = arSections[i].style.display;
	}
}

function savemenustate(){					
	var inc=1, blockid=""
	while (document.getElementById("sub"+inc)){
		if (document.getElementById("sub"+inc).style.display=="block"){
			blockid="sub"+inc
			break
		}
		inc++
	}
	var cookiename=(persisttype=="sitewide")? "switchmenu" : window.location.pathname
	var cookievalue=(persisttype=="sitewide")? blockid+";path=/" : blockid
	document.cookie=cookiename+"="+cookievalue
}

if (window.addEventListener)
	window.addEventListener("load", onloadfunction, false)
else if (window.attachEvent)
	window.attachEvent("onload", onloadfunction)
else if (document.getElementById)
	window.onload=onloadfunction

if (persistmenu=="yes" && document.getElementById)
	window.onunload=savemenustate

// default visibility
//$('spnBrowse').style.display = 'block';
//$('spnSortBy').style.display = 'none';
		
function Search_Click (isSearch)
{									 
	var IsAuthenticated = <asp:literal id="litIsAuthenticated" runat="server" />;
	var splitKeywords = $('txtKeywords').value.strip().split(' ');
	
	for(i = 0; i < splitKeywords.length; i++)
	{
	    if (splitKeywords[i].strip().length == 1 || splitKeywords[i].strip().length == 2)
	    {
	        alert('Search terms must be at least 3 characters!');
	        return false;
	    }
    }
	 
	if ( ($('txtKeywords').value.strip().length == 0 && isSearch) || !isSearch )
	{												 
		$('spnBrowse').style.display = 'block';
		$('spnSortBy').style.display = 'none';
	}
	else if (isSearch && isNewSearch)
	{												 
		$('spnBrowse').style.display = 'none';
		$('spnSortBy').style.display = 'block';
		
		$('liBrowse_Active').className = ""; 
		$('liBrowse_Updated').className = "";  
		$('liBrowse_Login').className = "";  
		$('liBrowse_Newest').className = "";
		
		if (IsAuthenticated)
		{
			$('liBrowse_Relevance').className = "selected";
		}
		else
		{
			$('liBrowse_Newest').className = "selected";
		}
	}
}

var arrow_down = new Image(16,12); 
arrow_down.src="../images/arrow_pointdown.gif"; 
var arrow_right = new Image(16,12); 
arrow_right.src="../images/arrow_pointright.gif"; 


var isNewSearch = true;
function SetSearchType (is_new)
{
	if (is_new)
		isNewSearch = true;
	else
		isNewSearch = false;	
}

function CheckKey (e) 
{	
	var key;	 
	
	if(window.event)
		key = window.event.keyCode;     //IE
	else
		key = e.which;
		
	if (key == 13)
	{
		if (isNewSearch)
		{
			Search_Click(true);
			__doPostBack('btnSearch','');
		}
		else
		{
			__doPostBack('btnUpdate_Search','');
		}
	}
}
</script>
<style>.newcontainerborder {padding-top:20px;}</style>
</asp:Content>