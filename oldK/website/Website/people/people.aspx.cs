///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva.usercontrols;
using log4net;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for people.
	/// </summary>
	public class people : BasePage
	{
		protected people () 
		{
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
            string GoogleScript = "<script>$('goog').innerHTML=$('goog').innerHTML;</script>";
            ClientScript.RegisterStartupScript(GetType(), "GoogleScript", GoogleScript);	
			
			if (!IsPostBack)
			{								   
				Show_Browse_Params = true;

				// Initialize search params
				InitializeSearch();
			}

			txtKeywords.Attributes.Add ("onkeypress", "CheckKey(event);");
			txtKeywords.Attributes.Add ("onfocus", "SetSearchType(true);");
			txtZipcode_Search.Attributes.Add ("onkeypress", "CheckKey(event);");
			txtZipcode_Search.Attributes.Add ("onfocus", "SetSearchType(false);");

			string scriptString = "";

			btnUpdate.Attributes.Add ("onclick", "return Search_Click(false);");
			btnSearch.Attributes.Add ("onclick", "return Search_Click(true);");

		
			// Set Nav
            ((GenericPageTemplate) Master).HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.CONNECT;

            ((GenericPageTemplate) Master).Title = "Kaneva People. Meet Friends and Hang-out.";

            //set the access pass filter toggle disable and invisible then process
            cbx_AccessPass.Enabled = div_AccessPassFilter.Visible = false;


			// Is user logged in?
			if (Request.IsAuthenticated)
			{
                
                //if (IsCurrentUserAdult())
                //to have the ability to toggle the restricted setting user must have access pass
                //can not have access pass if not an adult
                if (UserHasMaturePass())
                {
					hlChangeProfile.NavigateUrl = ResolveUrl("~/mykaneva/settings.aspx");
                    lblShowRestricted.Text = KanevaWebGlobals.CurrentUser.HasAccessPass ? "Yes" : "No";
				}
				else
				{
					divRestricted.Visible = false;
				}

                //configure the access pass only filter
                if (KanevaWebGlobals.CurrentUser.IsAdult)
                {
                    //set the Access Pass link value
                    lnk_AccesPass.HRef = GetAccessPassLink();

                    //disable/enable show access pass only based on users access pass status
                    cbx_AccessPass.Enabled = KanevaWebGlobals.CurrentUser.HasAccessPass;
                    div_AccessPassFilter.Visible = true;
                }

//                aRegister.HRef = ResolveUrl("~/myKaneva/inviteFriend.aspx");
                
                // Has user logged into wok?
                if (KanevaWebGlobals.CurrentUser.HasWOKAccount)
                {
                }
                else
                {
//                    aRegister.HRef = ResolveUrl("~/community/install3d.kaneva");
                }

                fbConnect.Visible = true;
                if (KanevaWebGlobals.CurrentUser.FacebookSettings.FacebookUserId > 0)
                {
                    imgFBConnect.Visible = false;
                    aRegister.Visible = true;
                }
                else
                {
                    h3InviteTitle.Visible = true;
                    divInviteFriends.Visible = true;
                }
            }											
			else   // user not logged in
			{
                hlChangeProfile.NavigateUrl = GetLoginURL();
                fbConnect.Visible = false;
            }

			litIsAuthenticated.Text = Request.IsAuthenticated ? "true" : "false";

			// Show correct advertisements
			AdvertisementSettings Settings = (AdvertisementSettings) Application ["Advertisement"];

			// Set mode of Google Ads
			if (Settings.Mode.Equals (AdvertisingMode.Live))
			{
				litAdTest.Visible = false;
			}
			else if (Settings.Mode.Equals (AdvertisingMode.Testing))
			{
				litAdTest.Text = "google_adtest = \"on\"";
			}

            if (AllowedToEdit())
			{
				// Add the javascript for Banning User
				scriptString = "<script language=\"javaScript\">\n<!--\n function BanUser (id) {\n";
				scriptString += "	$('txtUserId').value = id;\n";			// save the page number clicked to the hidden field
				scriptString += "	" + ClientScript.GetPostBackEventReference (btnBanUser, "") + ";\n";  // call the __doPostBack function to post back the form and execute the PageClick event
				scriptString += "}\n// -->\n";
				scriptString += "</script>";

                if (!ClientScript.IsClientScriptBlockRegistered(GetType (), "BanUser"))
				{
                    ClientScript.RegisterClientScriptBlock(GetType(), "BanUser", scriptString);
				}

				// Add the javascript for Removing User Ban
				scriptString = "<script language=\"javaScript\">\n<!--\n function RemoveBan (id) {\n";
				scriptString += "	$('txtUserId').value = id;\n";			// save the page number clicked to the hidden field
				scriptString += "	" + ClientScript.GetPostBackEventReference (btnRemoveUserBan, "") + ";\n";				// call the __doPostBack function to post back the form and execute the PageClick event
				scriptString += "}\n// -->\n";
				scriptString += "</script>";

                if (!ClientScript.IsClientScriptBlockRegistered(GetType(), "RemoveUserBan"))
				{
                    ClientScript.RegisterClientScriptBlock(GetType(), "RemoveUserBan", scriptString);
				}
				
				// Add the javascript for Restricting User
				scriptString = "<script language=\"javaScript\">\n<!--\n function RestrictUser (id) {\n";
				scriptString += "	$('txtUserId').value = id;\n";			// save the page number clicked to the hidden field
                scriptString += "	" + ClientScript.GetPostBackEventReference(btnRestrictUser, "") + ";\n";				// call the __doPostBack function to post back the form and execute the PageClick event
				scriptString += "}\n// -->\n";
				scriptString += "</script>";

                if (!ClientScript.IsClientScriptBlockRegistered(GetType(), "RestrictUser"))
				{
                    ClientScript.RegisterClientScriptBlock(GetType(), "RestrictUser", scriptString);
				}

				// Add the javascript for Removing User Restriction
				scriptString = "<script language=\"javaScript\">\n<!--\n function RemoveRestriction (id) {\n";
				scriptString += "	$('txtUserId').value = id;\n";
                scriptString += "	" + ClientScript.GetPostBackEventReference(btnRemoveUserRestriction, "") + ";\n";				
				scriptString += "}\n// -->\n";
				scriptString += "</script>";

                if (!ClientScript.IsClientScriptBlockRegistered(GetType(), "RemoveUserRestriction"))
				{
                    ClientScript.RegisterClientScriptBlock(GetType(), "RemoveUserRestriction", scriptString);
				}
				
			}
		}

		
		#region Helper Methods
        
        
        protected bool AllowedToEdit()
        {
            bool allowEditing = false;

            //check security level
            switch (KanevaWebGlobals.CheckUserAccess((int)SitePrivilege.ePRIVILEGE.USER_PROFILE_ADMIN))
            {
                case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_FULL:
                    allowEditing = true;
                    break;
                case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_READ:
                    break;
                case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_NONE:
                    break;
                default:
                    break;
            }

            return allowEditing;
		}
		
        /// <summary>
		/// BindData
		/// </summary>
		/// <param name="pageNumber"></param>
		/// <param name="filter"></param>
		private void BindData (int pageNumber, string filter)
		{
			// How many results do we display?
			int pgSize = filterTop.NumberOfPages;					
			
			//SET PAGE SIZE FOR THUMGS & DETAILS
			// Set current page
			pgTop.CurrentPageNumber = pageNumber;
			pgBottom.CurrentPageNumber = pageNumber;

			// Clear existing results
			dlPeopleThumb.Visible	= false;
			dlPeopleDetail.Visible = false;

			divNoResults.Visible = false;

			spnSearchFilterDesc.InnerText = GetSearchFilterSummary();
			
			if (Is_Detail_View)
			{
				lbThumbTop.CssClass = "";
				lbDetailTop.Enabled = false;
				lbDetailTop.CssClass = "selected";
				lbDetailTop.Enabled = true;

				pgSize = 8;
			}
			else
			{
				lbDetailTop.CssClass = "";
				lbThumbTop.Enabled = false;
				lbThumbTop.CssClass = "selected";
				lbThumbTop.Enabled = true;

				pgSize = 20;
			}
			
			SearchPeople (filter, pageNumber, pgSize);

			// Show Pager
			pgTop.DrawControl ();
			pgBottom.DrawControl ();
																
			// Log the search
            UserFacade userFacade = new UserFacade();
            userFacade.LogSearch(GetUserId(), Search_String, Search_Type);	
		}
		
        /// <summary>
		/// SearchPeople
		/// </summary>
		/// <param name="searchString"></param>
		/// <param name="filter"></param>
		/// <param name="pgNumber"></param>
		/// <param name="pgSize"></param>
		private void SearchPeople(string filter, int pgNumber, int pgSize)
		{
			string relationship = GetRelationships ();
			string orientation = GetOrientations ();
			string education = GetEducations ();
			string country = "";
            string hometown = "";
            UserFacade userFacade = new UserFacade();

            GetGenderSelection();

            PagedList<User> plUsers = new PagedList<User>();

            if (isUpdate)
			{
				country = drpCountry.SelectedValue;
			}
			else if (isUpdateSearch)
			{
				country = drpCountry_Search.SelectedValue;
			}

            string interests = Server.HtmlEncode (txtInterests.Text.Trim ());

            if (txtSchool.Text.Trim ().Length > 0)
            {
                interests += Server.HtmlEncode("\"school" + txtSchool.Text.Trim() + "\"");
            }

            if (txtHometown.Text.Trim ().Length > 0)
            {
                hometown = Server.HtmlEncode(txtHometown.Text.Trim());
            }

            bool showMature = false;

            //leaving as is - access pass member should be able to uncheck restricted field if they choose
            //if non access pass they should even have the option to check or uncheck 
            if (isUpdate)
            {
                //showMature = KanevaWebGlobals.CurrentUser.HasAccessPass && chkRestrictedPeople.Checked;
                showMature = chkRestrictedPeople.Checked;
            }
            else
            {
                if ((Search_String != null) && (Search_String.Trim().Length > 0))
                {
                    showMature = true;
                }
                else
                {
                    showMature = KanevaWebGlobals.CurrentUser.HasAccessPass;
                }
            }

            // Get the data
            if (!Show_Browse_Params)
            {
                // Make sure correct country is set for Search mode
                country = drpCountry_Search.SelectedValue;

                plUsers = userFacade.SearchUsers(ShowOnlyAccessPass, showMature, Server.HtmlEncode(Search_String),
                    true, true, chkPhotoRequired.Checked, 18, 0,
                    country, txtZipcode_Search.Text.Trim(), Convert.ToInt32(drpWithinMiles_Search.SelectedValue),
                    New_Within, 0, 0, -1, 0, -1, string.Empty, string.Empty, string.Empty, string.Empty,
                    string.Empty, string.Empty, string.Empty, string.Empty, false, "", filter, SetOrderByValue(), pgNumber, pgSize);
            }
            else
            {
                plUsers = userFacade.SearchUsers(ShowOnlyAccessPass, showMature, Server.HtmlEncode(Search_String),
                    Show_Male, Show_Female, chkPhotoRequired.Checked,
                    Convert.ToInt32(drpAgeFrom.SelectedValue), Convert.ToInt32(drpAgeTo.SelectedValue),
                    country, txtZipcode.Text.Trim(), Convert.ToInt32(drpWithinMiles.SelectedValue), New_Within, 0,
                    Convert.ToInt32(drpHeightFeet.SelectedValue), Convert.ToInt32(drpHeightInches.SelectedValue),
                    Convert.ToInt32(drpHeightFeetTo.SelectedValue), Convert.ToInt32(drpHeightInchesTo.SelectedValue),
                    Server.HtmlEncode(drpEthnicity.SelectedValue.Trim()), Server.HtmlEncode(drpReligion.SelectedValue.Trim()),
                    relationship, orientation, education, Server.HtmlEncode(rblDrink.SelectedValue.Trim()),
                    Server.HtmlEncode(rblSmoke.SelectedValue.Trim()), interests, chkOnlineNow.Checked, hometown, filter, SetOrderByValue(), pgNumber, pgSize);
            }

            pgTop.NumberOfPages = Math.Ceiling((double)plUsers.TotalCount / pgSize).ToString();
            pgBottom.NumberOfPages = Math.Ceiling((double)plUsers.TotalCount / pgSize).ToString();

            // Display thumbnail or detail results?
            if (Is_Detail_View && plUsers.TotalCount > 0)
            {
                dlPeopleDetail.Visible = true;
                dlPeopleDetail.DataSource = plUsers;
                dlPeopleDetail.DataBind();
            }
            else if (plUsers.TotalCount > 0)
            {
                dlPeopleThumb.Visible = true;
                dlPeopleThumb.DataSource = plUsers;
                dlPeopleThumb.DataBind();
            }
            else
            {
                ShowNoResults();
            }

            ShowResultsCount(Convert.ToInt32(plUsers.TotalCount), pgTop.CurrentPageNumber, filterTop.NumberOfPages, plUsers.Count);
		}

		/// <summary>
		/// ShowResultsCount
		/// </summary>
		/// <param name="total_count"></param>
		/// <param name="page_num"></param>
		/// <param name="count_per_page"></param>
		/// <param name="current_page_count"></param>
		private void ShowResultsCount(int total_count, int page_num,  int count_per_page, int current_page_count)
		{
			// Display the showing #-# of ### count
			string results = string.Empty;

			if (total_count > 0)
			{
				 results = "Results " + KanevaGlobals.GetResultsText(total_count, page_num, count_per_page, 
					 current_page_count, true, "%START%-%END% of %TOTAL%");
			}
			
			lblResultsBottom.Text = results;
		}

		/// <summary>
		/// ShowNoResults
		/// </summary>
		private void ShowNoResults()
		{
			divNoResults.InnerHtml = "<div class='noresults'><br/><br/>Your search did not find any matching People.</div>";
			divNoResults.Visible = true;
		}

        /// <summary>
        /// SetOrderByValue
        /// </summary>
        private string SetOrderByValue()
        {
            CurrentSortOrder = "";

            // How they are sorted
            if (Order_By.Equals(Constants.SEARCH_ORDERBY_RELEVANCE))
            {
                // Most Relevant
                CurrentSort = "";
            }
            else if (Order_By.Equals(Constants.SEARCH_ORDERBY_LAST_LOGGED_IN))
            {
                // Last Logged In
                CurrentSort = "last_login";
                CurrentSortOrder = "DESC";
            }
            else if (Order_By.Equals(Constants.SEARCH_ORDERBY_MOST_ACTIVE))
            {
                // Most Active
                CurrentSort = "last_login";
                CurrentSortOrder = "DESC";
            }
            else if (Order_By.Equals(Constants.SEARCH_ORDERBY_RECENTLY_UPDATED))
            {
                // Recently Updated
                CurrentSort = "last_update";
                CurrentSortOrder = "DESC";
            }
            else
            {
                // Newest
                CurrentSort = "signup_date";
                CurrentSortOrder = "DESC";
            }

            // Set sort and order by
            return (CurrentSort + " " + CurrentSortOrder).Trim();
        }
		
        /// <summary>
		/// SetOrderByType
		/// </summary>
		private void SetOrderByType()
		{
			try
			{
				if (Order_By == string.Empty) // Set order by if not already in viewstate
				{
					if (Request[Constants.QUERY_STRING_ORDERBY] != null && Request[Constants.QUERY_STRING_ORDERBY] != string.Empty)
					{
						switch ( Server.UrlDecode(Request[Constants.QUERY_STRING_ORDERBY].ToString ().ToLower ()) )
						{
							case Constants.SEARCH_ORDERBY_LAST_LOGGED_IN:
							case Constants.SEARCH_ORDERBY_RECENTLY_UPDATED:
							case Constants.SEARCH_ORDERBY_MOST_ACTIVE:
							case Constants.SEARCH_ORDERBY_NEWEST:
							case Constants.SEARCH_ORDERBY_RELEVANCE:
								Order_By = Server.UrlDecode(Request[Constants.QUERY_STRING_ORDERBY].ToString());
								break;
							default:
								Order_By = cORDER_BY_DEFAULT;
								break;
						}
					}
					else
					{
						Order_By = drpSortOrder.SelectedValue;
					}
				}
			}
			catch (Exception)
			{
				Order_By = cORDER_BY_DEFAULT;
			}
		}

		/// <summary>
		/// SetSearchKeywords
		/// </summary>
		private void SetSearchKeywords()
		{
			try
			{
				if (Search_String == string.Empty) 
				{
					if (Request[Constants.QUERY_STRING_KEYWORD] != null && Request[Constants.QUERY_STRING_KEYWORD] != string.Empty)
					{
						Search_String = Server.UrlDecode(Request[Constants.QUERY_STRING_KEYWORD].ToString());

						Order_By = Constants.SEARCH_ORDERBY_RELEVANCE;
						New_Within = 0;

						txtKeywords.Text = Search_String;
						txtKeywords.Style.Add ("color", "#535353");
					}
					else
					{
						Search_String = string.Empty;
					}
				}
			}
			catch (Exception)
			{
				Search_String = string.Empty;	 
			}
		}
		
        /// <summary>
		/// SetBrowseLinks
		/// </summary>
		private void SetBrowseLinks()
		{
			liBrowse_Relevance.Attributes.Add ("class", "");
			liBrowse_Active.Attributes.Add ("class", "");
			liBrowse_Updated.Attributes.Add ("class", "");
			liBrowse_Login.Attributes.Add ("class", "");
			liBrowse_Newest.Attributes.Add ("class", "");

			switch (Order_By)
			{
				case Constants.SEARCH_ORDERBY_RELEVANCE:
					liBrowse_Relevance.Attributes.Add ("class", cCSS_CLASS_SELECTED);
					break;
				case Constants.SEARCH_ORDERBY_MOST_ACTIVE:
					liBrowse_Active.Attributes.Add ("class", cCSS_CLASS_SELECTED);
					break;
				case Constants.SEARCH_ORDERBY_RECENTLY_UPDATED:
					liBrowse_Updated.Attributes.Add ("class", cCSS_CLASS_SELECTED);
					break;
				case Constants.SEARCH_ORDERBY_LAST_LOGGED_IN:
					liBrowse_Login.Attributes.Add ("class", cCSS_CLASS_SELECTED); 
					break;
				case Constants.SEARCH_ORDERBY_NEWEST:
					liBrowse_Newest.Attributes.Add ("class", cCSS_CLASS_SELECTED);
					break;
			}
		}

        /// <summary>
		/// InitializeNewSearch
		/// </summary>
		private void InitializeSearch()
		{
			if (!IsPostBack)
			{
				Is_Detail_View = false;	

				// Set up Browse Country list
				drpCountry.DataTextField = "Name";
				drpCountry.DataValueField = "CountryId";
				drpCountry.DataSource = WebCache.GetCountries ();
				drpCountry.DataBind ();

				drpCountry.Items.Insert (0, new ListItem ("United States", Constants.COUNTRY_CODE_UNITED_STATES));
				drpCountry.Items.Insert (0, new ListItem ("Anywhere", ""));

                // Set values based on user profile settings
                if (Request.IsAuthenticated)
                {
                    // Default the country and filter it on load
                    SetDropDownIndex (drpCountry, KanevaWebGlobals.CurrentUser.Country.ToString());
                }
                else
                {
                    drpCountry.Items[0].Selected = true;
                }

				// Set up Search Country list
				drpCountry_Search.DataTextField = "Name";
                drpCountry_Search.DataValueField = "CountryId";
				drpCountry_Search.DataSource = WebCache.GetCountries ();
				drpCountry_Search.DataBind ();

				drpCountry_Search.Items.Insert (0, new ListItem ("United States", Constants.COUNTRY_CODE_UNITED_STATES));
				drpCountry_Search.Items.Insert (0, new ListItem ("Anywhere", ""));

				drpCountry_Search.Items[0].Selected = true;


				drpAgeTo.Items.Add (new ListItem ("-", "0"));
				for (int i = 18; i < 86; i++)
				{
					drpAgeFrom.Items.Add (new ListItem (i.ToString (), i.ToString ()));	
					drpAgeTo.Items.Add (new ListItem (i.ToString (), i.ToString ()));
				}

                //set the ability to see restricted materials based on if they have an access pass or not
                //minors can not have access pass
                if (UserHasMaturePass())
                {
                    spnRestrictedContent.Visible = true;
                    chkRestrictedPeople.Checked = KanevaWebGlobals.CurrentUser.HasAccessPass;
                }
                else
                {
                    chkRestrictedPeople.Checked = spnRestrictedContent.Visible = false;
                }
			}
			
			// Set OrderBy Type
			SetOrderByType();
			liBrowse_Relevance.Visible = Request.IsAuthenticated;
			
			// Set Search keyword
			SetSearchKeywords();

            // Set Interests
            SetInterests ();

            // Set Schools
            SetSchools ();

            // Set Hometown
            SetHometown ();

            // Set Any Personal Attributes
            SetPersonalAttributes ();

            // Set location
            SetLocation();

            // Only hide restricted if they are logged in an under age
            //if (!IsCurrentUserAdult(false))

            //hide these settings if they are not an access pass holder
            //minors can not be access pass holders
            if (!UserHasMaturePass())
            {
				chkFreaky.Visible = false;
				chkSwinger.Visible = false;
				chkFreak.Visible = false;

				lblSwinger.Visible = false;
				lblFreaky.Visible = false;
				lblFreak.Visible = false;
			}
		
			if (!Show_Browse_Params)
			{
				SetBrowseLinks();
			}

			BindData(1, "");
		}

        /// <summary>
        /// ExpandLeftMenuSection
        /// </summary>
        private void ExpandLeftMenuSection (string obj)
        {
            if (obj.Length > 0)
            {
                ClientScript.RegisterStartupScript (GetType (), "expandMenuSection", "<script language=JavaScript> Event.observe(window, 'load', function () {SwitchMenu('" + obj + "');});</script>");
            }
        }

        /// <summary>
        /// SetSchools
        /// </summary>
        private void SetSchools ()
        {
            if (!IsPostBack)
            {
                // Schools
                if (Request[Constants.QUERY_STRING_SCHOOL] != null && Request[Constants.QUERY_STRING_SCHOOL] != string.Empty)
                {
                    txtSchool.Text = Server.UrlDecode (Request[Constants.QUERY_STRING_SCHOOL].ToString ());
                    ExpandLeftMenuSection ("sub8");
                }
            }
        }

        /// <summary>
        /// SetHometown
        /// </summary>
        private void SetHometown ()
        {
            if (!IsPostBack)
            {
                // Hometown
                if (Request[Constants.QUERY_STRING_HOMETOWN] != null && Request[Constants.QUERY_STRING_HOMETOWN] != string.Empty)
                {
                    txtHometown.Text = Server.UrlDecode (Request[Constants.QUERY_STRING_HOMETOWN].ToString ());
                }
            }
        }

        /// <summary>
        /// SetInterests
        /// </summary>
        private void SetInterests ()
        {
            if (!IsPostBack)
            {
                // Interests
                if (Request[Constants.QUERY_STRING_INTEREST] != null && Request[Constants.QUERY_STRING_INTEREST] != string.Empty)
                {
                    txtInterests.Text = Server.UrlDecode (Request[Constants.QUERY_STRING_INTEREST].ToString ());
                    ExpandLeftMenuSection ("sub5");
                }
            }
        }

        private void SetPersonalAttributes ()
        {
            if (!IsPostBack)
            {
                // Children
                if (Request[Constants.QUERY_STRING_CHILDREN] != null && Request[Constants.QUERY_STRING_CHILDREN] != string.Empty)
                {
                    //NOTE: children not currently supported in search
                }

                // Education
                if (Request[Constants.QUERY_STRING_EDUCATION] != null && Request[Constants.QUERY_STRING_EDUCATION] != string.Empty)
                {
                    SetEducation (Server.UrlDecode (Request[Constants.QUERY_STRING_EDUCATION].ToString ()));
                    ExpandLeftMenuSection ("sub8");
                }

                // Ethnicity
                if (Request[Constants.QUERY_STRING_ETHNICITY] != null && Request[Constants.QUERY_STRING_ETHNICITY] != string.Empty)
                {
                    SetEthnicityReligion (Constants.QUERY_STRING_ETHNICITY, Server.UrlDecode (Request[Constants.QUERY_STRING_ETHNICITY].ToString ()));
                    ExpandLeftMenuSection ("sub7");
                }

                // Height
                if (Request[Constants.QUERY_STRING_HEIGHT] != null && Request[Constants.QUERY_STRING_HEIGHT] != string.Empty)
                {
                    SetHeight (Server.UrlDecode (Request[Constants.QUERY_STRING_HEIGHT].ToString ()));
                    ExpandLeftMenuSection ("sub9");
                }

                // Income
                if (Request[Constants.QUERY_STRING_INCOME] != null && Request[Constants.QUERY_STRING_INCOME] != string.Empty)
                {
                    //NOTE: income not currently supported in search
                }

                // Sexual Orientation
                if (Request[Constants.QUERY_STRING_ORIENTATION] != null && Request[Constants.QUERY_STRING_ORIENTATION] != string.Empty)
                {
                    SetRelationshipOrientation (Constants.QUERY_STRING_ORIENTATION, Server.UrlDecode (Request[Constants.QUERY_STRING_ORIENTATION].ToString ()));
                    ExpandLeftMenuSection ("sub6");
                }

                // Relationship
                if (Request[Constants.QUERY_STRING_RELATIONSHIP] != null && Request[Constants.QUERY_STRING_RELATIONSHIP] != string.Empty)
                {
                    SetRelationshipOrientation (Constants.QUERY_STRING_RELATIONSHIP, Server.UrlDecode (Request[Constants.QUERY_STRING_RELATIONSHIP].ToString ()));
                    ExpandLeftMenuSection ("sub6");
                }

                // Religion
                if (Request[Constants.QUERY_STRING_RELIGION] != null && Request[Constants.QUERY_STRING_RELIGION] != string.Empty)
                {
                    SetEthnicityReligion (Constants.QUERY_STRING_RELIGION, Server.UrlDecode (Request[Constants.QUERY_STRING_RELIGION].ToString ()));
                    ExpandLeftMenuSection ("sub7");
                }

                // Drinking
                if (Request[Constants.QUERY_STRING_DRINKING] != null && Request[Constants.QUERY_STRING_DRINKING] != string.Empty)
                {
                    SetHabits (Constants.QUERY_STRING_DRINKING, Server.UrlDecode (Request[Constants.QUERY_STRING_DRINKING].ToString ()));
                    ExpandLeftMenuSection ("sub9");
                }

                // Smoking
                if (Request[Constants.QUERY_STRING_SMOKING] != null && Request[Constants.QUERY_STRING_SMOKING] != string.Empty)
                {
                    SetHabits (Constants.QUERY_STRING_SMOKING, Server.UrlDecode (Request[Constants.QUERY_STRING_SMOKING].ToString ()));
                    ExpandLeftMenuSection ("sub9");
                }
            }
        }

        /// <summary>
        /// SetHabits
        /// </summary>
        private void SetHabits (string category, string value)
        {
            try
            {
                if (category == Constants.QUERY_STRING_SMOKING)
                {
                    switch (value.ToLower())
                    {
                        case "no":
                            rblSmoke.SelectedValue = "No";
                            break;

                        case "yes":
                            rblSmoke.SelectedValue = "Yes";
                            break;

                        default: // No Answer
                            rblSmoke.SelectedValue = "";
                            break;
                    }
                }
                else if (category == Constants.QUERY_STRING_DRINKING)
                {
                    switch (value.ToLower())
                    {
                        case "no":
                            rblDrink.SelectedValue = "No";
                            break;

                        case "yes":
                            rblDrink.SelectedValue = "Yes";
                            break;

                        default: // No Answer
                            rblDrink.SelectedValue = "";
                            break;
                    }
                }
            }
            catch { }
        }

        private void SetHeight (string value)
        {
            try
            {
                string ft = value.Substring (0, value.IndexOf (" feet-"));
                string inch = value.Substring (value.IndexOf ("-")+1, value.IndexOf(" inches")-value.IndexOf("-")-1);

                drpHeightFeet.SelectedValue = ft;
                drpHeightInches.SelectedValue = inch;
            }
            catch { }
        }

        /// <summary>
        /// SetEducation
        /// </summary>
        private void SetEducation (string value)
        {
            try
            {
                switch (value.ToLower())
                {
                    case "bachelors degree":
                        chkGraduate.Checked = true;
                        break;

                    case "doctorate":
                        chkDoctorate.Checked = true;
                        break;

                    case "dropout":
                        chkDropout.Checked = true;
                        break;

                    case "high school":
                        chkHighSchool.Checked = true;
                        break;

                    case "in college":
                        chkInCollege.Checked = true;
                        break;

                    case "masters degree":
                        chkMasters.Checked = true;
                        break;

                    case "some college":
                        chkSomeCollege.Checked = true;
                        break;
                }
            }
            catch { }
        }

        /// <summary>
        /// SetEthnicityReligion
        /// </summary>
        private void SetEthnicityReligion (string category, string value)
        {
            try
            {
                if (category == Constants.QUERY_STRING_ETHNICITY)
                {
                   drpEthnicity.SelectedValue = value;
                }
                else if (category == Constants.QUERY_STRING_RELIGION)
                {
                    drpReligion.SelectedValue = value;
                }
            }
            catch { }
        }

        /// <summary>
        /// SetRelationshipOrientation
        /// </summary>
        private void SetRelationshipOrientation (string category, string value)
        {
            try
            {
                if (category == Constants.QUERY_STRING_RELATIONSHIP)
                {
                    switch (value.ToLower())
                    {
                        case "divorced":
                            chkDivorced.Checked = true;
                            break;

                        case "freaky":
                            chkFreaky.Checked = true;
                            break;

                        case "in a relationship":
                            chkIna.Checked = true;
                            break;

                        case "married":
                            chkMarried.Checked = true;
                            break;

                        case "single":
                            chkSingle.Checked = true;
                            break;

                        case "swinger":
                            chkSwinger.Checked = true;
                            break;
                    }
                }
                else if (category == Constants.QUERY_STRING_ORIENTATION)
                {
                    switch (value.ToLower())
                    {
                        case "bi-sexual":
                            chkBi.Checked = true;
                            break;

                        case "freak":
                            chkFreak.Checked = true;
                            break;

                        case "gay/lesbian":
                            chkGay.Checked = true;
                            break;

                        case "not sure":
                            chkNotSure.Checked = true;
                            break;

                        case "straight":
                            chkStraight.Checked = true;
                            break;
                    }
                }
            }
            catch { }
        }

        /// <summary>
        /// SetLocation
        /// </summary>
        private void SetLocation()
        {
            if (!IsPostBack)
            {
                // Country
                if (Request[Constants.QUERY_STRING_COUNTRY] != null && Request[Constants.QUERY_STRING_COUNTRY] != string.Empty)
                {
                    SetDropDownIndex (drpCountry, Server.UrlDecode(Request[Constants.QUERY_STRING_COUNTRY].ToString()));
                }

                // Zip
                if (Request[Constants.QUERY_STRING_ZIP_CODE] != null && Request[Constants.QUERY_STRING_ZIP_CODE] != string.Empty)
                {
                    txtZipcode.Text = Server.UrlDecode(Request[Constants.QUERY_STRING_ZIP_CODE].ToString());
                }
                else if (Request.IsAuthenticated)
                {
                    // CK Hotfeature
                    if (KanevaWebGlobals.CurrentUser.Country.Equals("US"))
                    {
                        txtZipcode.Text = KanevaWebGlobals.CurrentUser.ZipCode;
                        SetDropDownIndex(drpWithinMiles, "20");
                    }
                }

                if (Request[Constants.QUERY_STRING_WITHIN] != null && Request[Constants.QUERY_STRING_WITHIN] != string.Empty)
                {
                    SetDropDownIndex (drpWithinMiles, Server.UrlDecode (Request[Constants.QUERY_STRING_WITHIN].ToString ()));
                }
            }
        }

        /// <summary>
		/// GetSearchFilterSummary
		/// </summary>
		private string GetSearchFilterSummary()
		{
			string browse_txt = string.Empty;
			string gender_txt = string.Empty;

			switch (Order_By)
			{
				case Constants.SEARCH_ORDERBY_MOST_ACTIVE:
					browse_txt = "Most Active";
					break;
				case Constants.SEARCH_ORDERBY_RECENTLY_UPDATED:
					browse_txt = "Recently Updated";
					break;
				case Constants.SEARCH_ORDERBY_LAST_LOGGED_IN:
					browse_txt = "Last Logged In"; 
					break;
				case Constants.SEARCH_ORDERBY_RELEVANCE:
					browse_txt = "Relevance"; 
					break;
				default:
					browse_txt = "Newest";
					break;			
			}
			
			switch (rblGender.SelectedValue)
			{
				case "male":
					gender_txt = "Men";
					break;
				case "female":
					gender_txt = "Women";
					break;
				default:
					gender_txt = "All";
					break;
			}

			if (Search_String.Length > 0)
			{
				return "Search - " + browse_txt;
			}
			else
			{
				return "People - " + gender_txt + ", " + browse_txt;
			}
		}

        /// <summary>
        /// GetRelationships
        /// </summary>
        private string GetRelationships()
        {
            string relationship = "";

            if (chkSingle.Checked)
            {
                relationship = AddInClause(relationship, "Single");
            }
            if (chkIna.Checked)
            {
                relationship = AddInClause(relationship, "\"In a relationship\"");
            }
            if (chkMarried.Checked)
            {
                relationship = AddInClause(relationship, "Married");
            }
            if (chkDivorced.Checked)
            {
                relationship = AddInClause(relationship, "Divorced");
            }
            if (chkSwinger.Checked)
            {
                relationship = AddInClause(relationship, "Swinger");
            }
            if (chkFreaky.Checked)
            {
                relationship = AddInClause(relationship, "Freaky");
            }
            return relationship;
        }

        /// <summary>
        /// GetOrientations
        /// </summary>
        private string GetOrientations()
        {
            string orientation = "";

            if (chkStraight.Checked)
            {
                orientation = AddInClause(orientation, "Straight");
            }
            if (chkGay.Checked)
            {
                orientation = AddInClause(orientation, "\"Gay/lesbian\"");
            }
            if (chkBi.Checked)
            {
                orientation = AddInClause(orientation, "Bi-sexual");
            }
            if (chkNotSure.Checked)
            {
                orientation = AddInClause(orientation, "\"Not sure\"");
            }
            if (chkFreak.Checked)
            {
                orientation = AddInClause(orientation, "Freak");
            }

            return orientation;
        }

        /// <summary>
        /// GetEducations
        /// </summary>
        private string GetEducations()
        {
            string educations = "";

            if (chkDropout.Checked)
            {
                educations = AddInClause(educations, "Dropout");
            }
            if (chkHighSchool.Checked)
            {
                educations = AddInClause(educations, "High School");
            }
            if (chkSomeCollege.Checked)
            {
                educations = AddInClause(educations, "Some college");
            }
            if (chkInCollege.Checked)
            {
                educations = AddInClause(educations, "In college");
            }
            if (chkGraduate.Checked)
            {
                educations = AddInClause(educations, "\"Bachelors degree\"");
            }
            if (chkMasters.Checked)
            {
                educations = AddInClause(educations, "Masters degree");
            }
            if (chkDoctorate.Checked)
            {
                educations = AddInClause(educations, "Doctorate");
            }
            return educations;
        }

        /// <summary>
        /// GetLookingForSettings
        /// </summary>
        private string GetLookingForSettings()
        {
            string looking_for = "";

            if (chkFriendship.Checked)
            {
                looking_for = AddInClause(looking_for, "Friendship");
            }
            if (chkDating.Checked)
            {
                looking_for = AddInClause(looking_for, "Dating");
            }
            if (chkRelationship.Checked)
            {
                looking_for = AddInClause(looking_for, "\"A Relationship\"");
            }
            if (chkRandom.Checked)
            {
                looking_for = AddInClause(looking_for, "\"Random play\"");
            }
            if (chkAnything.Checked)
            {
                looking_for = AddInClause(looking_for, "\"Anything and everything\"");
            }
            if (chkNetworking.Checked)
            {
                looking_for = AddInClause(looking_for, "Networking");
            }
            if (chkBusiness.Checked)
            {
                looking_for = AddInClause(looking_for, "\"Marketing/Business\"");
            }
            return looking_for;
        }

        /// <summary>
        /// AddInClause
        /// </summary>
        private string AddInClause(string clause, string param)
        {
            if (clause.Length > 0)
            {
                clause += " | ";
            }
            clause += param;
            return clause;
        }

		/// <summary>
		/// GetEducations
		/// </summary>
		private void GetGenderSelection ()
		{
			Show_Male = false;
			Show_Female = false;

			if (rblGender.SelectedValue == "male")
			{
				Show_Male = true;
			}
			else if (rblGender.SelectedValue == "female")
			{
				Show_Female = true;	
			}
			else
			{
				Show_Male = true;
				Show_Female = true;	
			}
		}
		
        /// <summary>
		/// Get the action to perform
		/// </summary>
		/// <param name="accountTypeId"></param>
		/// <param name="communityId"></param>
		/// <returns></returns>
		protected string GetBanScript (int userId, bool bBan)
		{	
			if (bBan)
			{
				return "javascript:if (confirm(\"Are you sure you want to ban this user from the site?\")){BanUser (" + userId + ")};";
			}
			else
			{
				return "javascript:if (confirm(\"Are you sure you want to remove the ban for this user?\")){RemoveBan (" + userId + ")};";
			}
		}

		/// <summary>
		/// Delete a blog
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void BanUser (Object sender, EventArgs e)
		{
            if (!AllowedToEdit())
			{
				m_logger.Warn ("User " + GetUserId () + " from IP " + Common.GetVisitorIPAddress() + " tried to ban user " + txtUserId.Value + " but is not a csr or admin");
				return;
			}
            
			/* Modified to send them to the CSR tool rather than updating the user here.
            
            // Log the CSR activity
			UsersUtility.InsertCSRLog (GetUserId (), "CSR/Admin - Banning User " + txtUserId.Value, 0, Convert.ToInt32 (txtUserId.Value));

			UsersUtility.UpdateUserStatus (Convert.ToInt32 (txtUserId.Value), (int) Constants.eUSER_STATUS.LOCKED);
			BindData (pgTop.CurrentPageNumber, filterTop.CurrentFilter);
            * 
            */

            Response.Redirect("~/Administration/csr/Activity_Suspensions.aspx?userId=" + txtUserId.Value);
		}

		/// <summary>
		/// Delete a blog
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void RemoveUserBan (Object sender, EventArgs e)
		{
            if (!AllowedToEdit())
			{
				m_logger.Warn ("User " + GetUserId () + " from IP " + Common.GetVisitorIPAddress() + " tried to remove user ban " + txtUserId.Value + " but is not a csr or admin");
				return;
			}

            /* Modified to send them to the CSR tool rather than updating the user here.
            // Log the CSR activity
            UsersUtility.InsertCSRLog (GetUserId (), "CSR/Admin - Removing Ban from User " + txtUserId.Value, 0, Convert.ToInt32 (txtUserId.Value));

            UsersUtility.UpdateUserStatus (Convert.ToInt32 (txtUserId.Value), (int) Constants.eUSER_STATUS.REGNOTVALIDATED);
            BindData (pgTop.CurrentPageNumber, filterTop.CurrentFilter);
           
            * 
            */

            Response.Redirect("~/Administration/csr/Activity_Suspensions.aspx?userId=" + txtUserId.Value);
		}
		
        /// <summary>
		/// GetRestrictScript
		/// </summary>
		protected string GetRestrictScript (int userId, bool bRestrict)
		{	
			if (bRestrict)
			{
				return "javascript:if (confirm(\"Are you sure you want to mark this user profile as restricted?\")){RestrictUser (" + userId + ")};";
			}
			else
			{
				return "javascript:if (confirm(\"Are you sure you want to remove the restriction for this user?\")){RemoveRestriction (" + userId + ")};";
			}
		}

		/// <summary>
		/// Mark user as restricted
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void RestrictUser (Object sender, EventArgs e)
		{
            if (!AllowedToEdit())
			{
				m_logger.Warn ("User " + GetUserId () + " from IP " + Common.GetVisitorIPAddress() + " tried to restrict user " + txtUserId.Value + " but is not a csr or admin");
				return;
			}

			// Log the CSR activity
            SiteMgmtFacade siteMgmtFacade = new SiteMgmtFacade();
            siteMgmtFacade.InsertCSRLog(GetUserId(), "CSR/Admin - Restricting User " + txtUserId.Value, 0, Convert.ToInt32(txtUserId.Value));

            UserFacade userFacade = new UserFacade();
            userFacade.UpdateUserRestriction(Convert.ToInt32(txtUserId.Value), 1);
			BindData (pgTop.CurrentPageNumber, filterTop.CurrentFilter);
		}

		/// <summary>
		/// Remove a users restirction status
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void RemoveUserRestriction (Object sender, EventArgs e)
		{
            if (!AllowedToEdit())
			{
				m_logger.Warn ("User " + GetUserId () + " from IP " + Common.GetVisitorIPAddress() + " tried to remove user restriction " + txtUserId.Value + " but is not a site administrator");
				return;
			}

			// Log the CSR activity
            SiteMgmtFacade siteMgmtFacade = new SiteMgmtFacade();
            siteMgmtFacade.InsertCSRLog(GetUserId(), "CSR/Admin - Removing Restriction from User " + txtUserId.Value, 0, Convert.ToInt32(txtUserId.Value));

            UserFacade userFacade = new UserFacade();
            userFacade.UpdateUserRestriction(Convert.ToInt32(txtUserId.Value), 0);
			BindData (pgTop.CurrentPageNumber, filterTop.CurrentFilter);
		}
		
        #endregion


		#region Event Handlers

        protected void APCheckChanged(object sender, EventArgs e)
        {
            ShowOnlyAccessPass = cbx_AccessPass.Checked;
            BindData(1, "");
        }
	
        /// <summary>
		/// Execute when the user clicks the the Search button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnSearch_Click (object sender, EventArgs e)			  
		{
			if (KanevaWebGlobals.ContainsInjectScripts (txtKeywords.Text))
			{
				m_logger.Warn ("User " + KanevaWebGlobals.GetUserId () + " tried to script from IP " + Common.GetVisitorIPAddress());
				ShowErrorOnStartup ("Your input contains invalid scripting, please remove script code and try again.", true);
				return;
			}

            ViewState["searchType"] = "";
            Search_String = txtKeywords.Text.Trim();

			Order_By = Constants.SEARCH_ORDERBY_NEWEST;
			Show_Browse_Params = true;
			if (Search_String.Length > 0 && Request.IsAuthenticated)
			{
				Order_By = Constants.SEARCH_ORDERBY_RELEVANCE;
				Show_Browse_Params = false;
				drpCountry_Search.SelectedValue = "";
				drpWithinMiles_Search.SelectedValue = "0";
				txtZipcode_Search.Text = string.Empty;
			}
			else if (Search_String.Length > 0)
			{
				Order_By = Constants.SEARCH_ORDERBY_NEWEST;
				Show_Browse_Params = false;
			}
			
			New_Within = (int) Constants.eSEARCH_TIME_FRAME.ALL_TIME;

			InitializeSearch ();
		}
		
        /// <summary>
		/// Execute when the user clicks the the Update button in the Search section
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnUpdate_Search_Click (object sender, EventArgs e)			  
		{
			Show_Browse_Params = false;
			isUpdateSearch = true;
            ViewState["searchType"] = "update_search";

			SetOrderByType ();

			SearchPeople (string.Empty, 1, filterTop.NumberOfPages);
		}
		
        /// <summary>
		/// Execute when the user clicks the the Update button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnUpdate_Click (object sender, EventArgs e)			  
		{
			if (KanevaWebGlobals.ContainsInjectScripts (txtKeywords.Text))
			{
				m_logger.Warn ("User " + KanevaWebGlobals.GetUserId () + " tried to script from IP " + Common.GetVisitorIPAddress());
				ShowErrorOnStartup ("Your input contains invalid scripting, please remove script code and try again.", true);
				return;
			}			
			
			Order_By = drpSortOrder.SelectedValue;
			New_Within = (int) Constants.eSEARCH_TIME_FRAME.ALL_TIME;

			Show_Browse_Params = true;
			isUpdate = true;
            ViewState["searchType"] = "update";

			InitializeSearch ();

            ScriptManager.RegisterStartupScript (this, GetType (), "scroll", "scroll();", true);
		}
		
        /// <summary>
		/// Execute when the user clicks the the view type link button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void lbViewAs_Click (object sender, EventArgs e) 
		{
			if (((LinkButton)sender).Attributes["View"].ToString().ToLower() == "thumb")
			{
				Is_Detail_View = false;
			}
			else
			{
				Is_Detail_View = true;												  
			}

			BindData(1, filterTop.CurrentFilter);
		}
		
		/// <summary>
		/// Execute when the user clicks the Browse type link button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void lbBrowse_Click (object sender, CommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case Constants.SEARCH_ORDERBY_MOST_ACTIVE:
				case Constants.SEARCH_ORDERBY_RECENTLY_UPDATED:
				case Constants.SEARCH_ORDERBY_LAST_LOGGED_IN:
				case Constants.SEARCH_ORDERBY_NEWEST:
				case Constants.SEARCH_ORDERBY_RELEVANCE:
					Order_By = e.CommandName;
					break;
				default:
					Order_By = Constants.SEARCH_ORDERBY_NEWEST;
					break;
			}
    
			SetBrowseLinks();

			BindData (1, filterTop.CurrentFilter);
		}

		
		#region Pager & Filter control events
		
        /// <summary>
		/// Execute when the user selects a page change link from the Pager control
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pg_PageChange (object sender, PageChangeEventArgs e)
		{
            string searchType = ViewState["searchType"] != null ? ViewState["searchType"].ToString() : string.Empty;
            if (searchType == "update") isUpdate = true;
            else if (searchType == "update_search") isUpdateSearch = true;

            BindData (e.PageNumber, filterTop.CurrentFilter);
		}
		
        /// <summary>
		/// Execute when the user selects an item from the Store Filter
		/// Items to display drop down list
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void FilterChanged(object sender, FilterChangedEventArgs e)
		{
			BindData (1, e.Filter);
		}
		
        #endregion

        #endregion

		
		#region Properties
		
        /// <summary>
		/// searchTab
		/// </summary>
		private int Search_Type
		{
			get
			{
				return Convert.ToInt32 (Constants.eCHANNEL_SEARCH_TYPE.NON_PERSONAL);
			}
		}
		
        /// <summary>
		/// newWithin
		/// </summary>
		private int New_Within
		{
			set
			{
				ViewState ["New_Within"] = value;
			}
			get
			{
				if (ViewState ["New_Within"] == null)
				{
					return (int) Constants.eSEARCH_TIME_FRAME.ALL_TIME;
				}
				else
				{
					return Convert.ToInt32 (ViewState ["New_Within"]);
				}
			}
		}
		
        /// <summary>
		/// Search_String
		/// </summary>
		private string Search_String
		{
			set
			{
				ViewState ["Search_String"] = value.Trim();
			}
			get
			{
				if (ViewState ["Search_String"] == null)
				{
					return string.Empty; 
				}
				else
				{
					return ViewState ["Search_String"].ToString();
				}
			}
		}

		/// <summary>
		/// orderBy
		/// </summary>
		private string Order_By
		{
			set
			{
				ViewState ["Order_By"] = value;
			}
			get
			{
				if (ViewState ["Order_By"] == null)
				{
					return string.Empty; //cORDER_BY_DEFAULT;
				}
				else
				{
					return ViewState ["Order_By"].ToString();
				}
			}
		}

		/// <summary>
		/// isDetailView
		/// </summary>
		private bool Is_Detail_View
		{
			set
			{
				ViewState ["Is_Detail_View"] = value;
			}
			get
			{
				if (ViewState ["Is_Detail_View"] == null)
				{
					return false;
				}
				else
				{
					return Convert.ToBoolean (ViewState ["Is_Detail_View"]);
				}
			}
		}
		
        /// <summary>
		/// isDetailView
		/// </summary>
		private bool Show_Browse_Params
		{
			set
			{
				ViewState ["Show_Browse_Params"] = value;
			}
			get
			{
				if (ViewState ["Show_Browse_Params"] == null)
				{
					return false;
				}
				else
				{
					return Convert.ToBoolean (ViewState ["Show_Browse_Params"]);
				}
			}
		}

        /// <summary>
        /// indicator to show only access pass or not
        /// </summary>
        private bool ShowOnlyAccessPass
        {
            get
            {
                if (ViewState["showOnlyAP"] == null)
                {
                    return false;
                }
                else
                {
                    return (bool)ViewState["showOnlyAP"];
                }
            }
            set
            {
                ViewState["showOnlyAP"] = value;
            }
        }

        /// <summary>
        /// Current sort expression
        /// </summary>
        public string CurrentSort
        {
            get
            {
                if (ViewState["cs"] == null)
                {
                    return DEFAULT_SORT;
                }
                else
                {
                    return ViewState["cs"].ToString ();
                }
            }
            set
            {
                ViewState["cs"] = value;
            }
        }

        /// <summary>
        /// Current sort order
        /// </summary>
        public string CurrentSortOrder
        {
            get
            {
                if (ViewState["cso"] == null)
                {
                    return DEFAULT_SORT_ORDER;
                }
                else
                {
                    return ViewState["cso"].ToString ();
                }
            }
            set
            {
                ViewState["cso"] = value;
            }
        }

        protected virtual string DEFAULT_SORT
        {
            get
            {
                return "a.name";
            }
        }

        /// <summary>
        /// DEFAULT_SORT_ORDER
        /// </summary>
        /// <returns></returns>
        protected virtual string DEFAULT_SORT_ORDER
        {
            get
            {
                return "ASC";
            }
        }

        # endregion


		#region Declerations

		protected Kaneva.Pager			pgTop, pgBottom;
		protected Kaneva.SearchFilter	filterTop;

		protected HyperLink			hlChangeProfile, hlBan, hlRemoveBan, hlRestrict, hlRemoveRestriction;
        protected TextBox           txtKeywords, txtInterests, txtSchool, txtHometown;
		protected TextBox			txtZipcode, txtZipcode_Search;
		protected LinkButton		lbThumbTop, lbDetailTop;
		protected LinkButton		lbBrowse_Relevance, lbBrowse_Active, lbBrowse_Updated, lbBrowse_Login, lbBrowse_Newest;
		protected Literal			litAdTest, litIsAuthenticated, litPng;
		protected RadioButtonList	rblGender;
		protected RadioButtonList	rblSmoke, rblDrink;
		protected Button			btnRemoveUserBan, btnBanUser, btnRemoveUserRestriction, btnRestrictUser;
		protected CheckBox			chkPhotoRequired, chkOnlineNow;
		protected CheckBox			chkFemale, chkMale, chkPhotoPeople, chkRestrictedPeople;
		protected CheckBox			chkStraight, chkGay, chkBi, chkFreak, chkNotSure;
		protected CheckBox			chkSingle, chkIna, chkMarried, chkDivorced, chkSwinger, chkFreaky;
		protected CheckBox			chkDropout, chkHighSchool, chkSomeCollege, chkInCollege, chkGraduate, chkMasters, chkDoctorate;
		protected CheckBox			chkFriendship, chkDating, chkRelationship, chkRandom, chkAnything, chkNetworking, chkBusiness;
		protected DropDownList		drpCountry, drpWithinMiles;
		protected DropDownList		drpCountry_Search, drpWithinMiles_Search;
		protected DropDownList		drpEthnicity, drpReligion, drpUpdatedWithinPeople, drpSortOrder;
		protected DropDownList		drpAgeFrom, drpAgeTo, drpHeightFeet, drpHeightInches, drpHeightFeetTo, drpHeightInchesTo;
		protected Label				lblFreak, lblSwinger, lblFreaky;
		protected Label				lblResultsBottom, lblShowRestricted;
        protected LinkButton btnSearch, btnUpdate_Search, btnUpdate;

        protected Literal           litHeaderText;
        protected HtmlImage         imgFBConnect;

		protected DataList			dlPeopleDetail, dlPeopleThumb;

		protected HtmlInputHidden		txtUserId;
        protected HtmlAnchor lnk_AccesPass, aRegister;
        protected CheckBox cbx_AccessPass;
        protected HtmlContainerControl divNoResults, divRestricted, div_AccessPassFilter;
        protected HtmlContainerControl spnSortBy, spnBrowse, fbConnect, h3InviteTitle, divInviteFriends;
		protected HtmlContainerControl	spnSearchFilterDesc, spnRestrictedContent;
		protected HtmlContainerControl	liBrowse_Relevance, liBrowse_Active, liBrowse_Updated, liBrowse_Login, liBrowse_Newest;

		private string cCSS_CLASS_SELECTED = "selected";
		private const string cORDER_BY_DEFAULT = Constants.SEARCH_ORDERBY_NEWEST;
		//private const string cTEXTBOX_INSTRUCTION = "enter search text";

		private bool	Show_Male, Show_Female;

        private bool isUpdate = true;
        private bool isUpdateSearch = false;

		// Logger
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

		#endregion


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
			pgTop.PageChanged +=new PageChangeEventHandler (pg_PageChange);
			pgBottom.PageChanged +=new PageChangeEventHandler (pg_PageChange);
			filterTop.FilterChanged +=new FilterChangedEventHandler (FilterChanged);
		}
		#endregion

	}
}
