<%@ Page Language="C#" MasterPageFile="~/masterpages/IndexPageTemplate.Master" AutoEventWireup="false" CodeBehind="passDetails.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.passDetails" %>

<asp:Content ID="cnt_passDetails" runat="server" contentplaceholderid="cph_Body" >
<style>
#wrapper {border:1px solid #ccc;border-top:none;background-color:#fff;width:1004px;margin:0 auto;height:auto !important;height:100%;min-height:100%;padding-bottom:5em;clear:both;display:inline-block;}
.PageContainer {background-color:#fff;width:1004px;height:100%;border-top:solid 1px #fff;}
</style>
    <div class="wrapper PageContainer">
        <div id="div_passDetailsImage" class="passDetailsImage" runat="server">
        </div>
        
        <!-- Level One Access Pass -->
        <div id="div_PassDetailsContent" class="contentContainer" runat="server">
        </div>
        
        <div class="clear"><!-- clear the floats --></div>
        <div class="spTryPass"><asp:LinkButton ID="lb_trialPass" runat="server">TRY ME OUT</asp:LinkButton></div>
        <div class="clear"><!-- clear the floats --></div>
    </div>
</asp:Content>