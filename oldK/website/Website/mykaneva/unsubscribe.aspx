<%@ Page language="c#" MasterPageFile="~/masterpages/GenericLandingPageTemplate.Master" Codebehind="unsubscribe.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.unsubscribe" %>

<asp:Content ID="cntHeaderCSS" runat="server" contentplaceholderid="cphHeadCSS" >
    <link href="../css/themes-join/template.css" rel="stylesheet" type="text/css">
    <link href="../css/base/base_new1.css" type="text/css" rel="stylesheet" />
    <link href="../css/base/buttons_new.css?v1" type="text/css" rel="stylesheet">

    <style>
    .pageBody {padding:0 0 20px 0;background:#fff;border:1px solid #ccc;border-top:none;}
    .datacontainer {width:700px;margin:0 auto;padding-top:50px;padding-bottom:50px;}
    p {margin:20px 0 0 0;line-height:16px;display:block;}
        .no-top-margin {
            margin-top: 0;
        }
    h6 {font-size:22px;font-family:verdana, arial, sans-serif;padding:0;margin:0;font-weight:bold;}
    .img-ppl {margin-left:30px;}
    a.btnlarge {margin:15px 0 0 0;padding-top:5px;}
    a:link {font-size:12px;}
    .footer ul {text-align:center;}
    </style>
</asp:Content>

<asp:Content id="cntBody" runat="server" contentplaceholderid="cphBody" >

    <div class="datacontainer">
        <h6 id="pgTitle" runat="server">Before You Go</h6>

            <div id="dvOptions" runat="server">
                <p></p>
                <br />                                                   
                <strong>No</strong>, just update my communication preferences in My Kaneva.<br />
                <br />
                <asp:button id="btnPrefrences" onclick="btnPrefrences_Click" runat="server" text="Prefrences" /><br />
                <br />
                <strong>Yes</strong>, unsubscribe me from all Kaneva email communication. This excludes forgotten password requests and credit purchase transactions. <br />
                <br />                                                   
                <asp:button id="btnNoComm" onclick="btnNoComm_Click" runat="server" text="Unsubscribe from all Kaneva communications" /><br />
            </div>
            <br />
            <div id="dvUnsubForm" runat="server" visible="false">    
                <table cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td>
                            <p class="no-top-margin">Combining a social network and a virtual world, Kaneva brings profiles and entertainment to the 3D realm in a modern-day,
                                online digital world full of real friends and good times. Kaneva provides a whole new way � a more human way � to connect 
                                with friends online places and entertainment.  <b>Best of all it is FREE!</b>
                            </p>
                            <p>
                                Are you sure you don�t want to give Kaneva shot?<br/>
 		                        <a class="btnlarge blue" id="lbJoin" href="<%= ResolveUrl(KlausEnt.KEP.Kaneva.KanevaGlobals.JoinLocation) %>">Join Kaneva</a>
                            </p>
                            
                            <br />
                            <asp:Panel ID="Panel1" runat="server" DefaultButton="lbUnsubscribe"> 
                            <p>If you would like to no longer recieve communications from Kaneva via email, Enter your email address and click on the Unsubscribe button.</p>                                                
                            <p>
                                <asp:textbox id="txtEmail" runat="server"></asp:textbox> <span class="error" id="spnError" runat="server"></span>                                                     
		                        <asp:LinkButton class="btnlarge grey" id="lbUnsubscribe" runat="server" text="Unsubscribe" causesvalidation="false" onclick="btnUnsubscribe_Click"></asp:LinkButton>
                            </p>
                            </asp:Panel>
                            </td>
                        <td>
                            <img class="img-ppl" border="0" runat="server" src="~/images/email/ppl_image.jpg" />
                        </td>
                    </tr>
                </table>                                                     
            </div>
            <p id="msgStatus" runat="server" visible="false"></p>                                                        
            <p>Kaneva is committed to maintaining the trust and confidence of our members. In particular, we want you to know that we are not in the business of selling or renting individuals' personal data to other companies for marketing purposes. Read more in our <a href="/overview/privacy.aspx">Privacy Policy</a>.</p>
    </div>


</asp:Content>