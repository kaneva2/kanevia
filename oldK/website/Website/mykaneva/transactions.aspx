<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="transactions.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.mykaneva.transactions" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../usercontrols/Pager.ascx" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>

<link href="../css/new.css" type="text/css" rel="stylesheet">

<script src="../jscript/prototype.js" type="text/javascript" language="javascript"></script>

<script type="text/JavaScript">
function changeBG(obj) {
	obj.style.background = "url('../images/media/tabsMedia.gif')";
}
function changeBG2(objeto) {
	objeto.style.background = "url('../images/media/tabsMedia_on.gif')";
}
function changeBGB(objeto) {
	objeto.style.background = "url('../images/media/tabsMedia2.gif')";
}
function changeBGB2(objeto) {
	objeto.style.background = "url('../images/media/tabsMedia2_on.gif')";
}
</script>

<style type="text/css">
<!--
#ulHover a:hover {background-color:#214f73;color:#b3d6f2;cursor:hand;}
.display
{
	position: static !important;
	position: realtive;
}
.btnPadding
{
	padding-left:48px;
	_padding-left:30px;	/* IE only hack */
}
#librarynav li.transactionnav span.navicon {
	background: transparent url("../images/fortuneRewards.png") no-repeat 0 0;
	}
.gm {font-size:.8em;color:#999;}
-->
</style>
												

<div id="divLoading">
	<table height="450" cellSpacing="0" cellPadding="0" width="100%" border="0" id="Table1">
		<tr>
			<th class="loadingText" id="divLoadingText">Loading...</th>
		</tr>
	</table>
</div>
					
<div id="divData" style="display: none">
	<!-- Main Body structure table -->
	<table cellSpacing="0" cellPadding="0" class="newcontainer" align="center" border="0">
		<tr>
			<td>
				<table cellSpacing="0" cellPadding="0" width="100%" border="0" class="newcontainerborder">
					<tr>
						<td class=""></td>
						<td class=""></td>
						<td class=""></td>
					</tr>
					<tr>
						<td class=""><img id="img7" height="1" runat="server" src="~/images/spacer.gif" width="1"></td>
						<td class="newdatacontainer" vAlign="top">
							<table cellSpacing="0" cellPadding="0" width="100%" border="0"> 
							<!-- BEGIN TOP STATUS BAR -->
								<tr>
									<td>
										<div id="pageheader">
											<table cellSpacing="0" cellPadding="0" width="99%" border="0">
												<tr>
													<td align="left">
														<h1>My Transactions</h1>
													</td>
													<td vAlign="middle" class="searchnav" align="right">
															
														<ajax:ajaxpanel id="ajpFilterPanel" runat="server">

															<table cellSpacing="0" cellPadding="0" width="700" border="0">
																<tr> 
																	<!-- Begin page results Filter --->
																	<td class="searchnav" nowrap align="right" valign="top" style="height:30px;">
																		<asp:label runat="server" id="lblResults_Top" cssclass="insideTextNoBold" /><br />
																		<kaneva:pager runat="server" isajaxmode="True" id="pgTop" onpagechanged="pg_PageChange" maxpagestodisplay="5" shownextprevlabels="true" />
																	</td> 
																	<!-- End page results Filter --->
																</tr>
															</table>
															
														</ajax:ajaxpanel>
															
													</td>
												</tr>
											</table>
										</div>
									</td>
								</tr>
								<!-- END TOP STATUS BAR --> 
								<!-- BEGIN MAIN BODY -->
								<tr>
									<td valign="top" align="left" width="968">
										<table cellspacing="0" cellpadding="0" width="100%" border="0">
											<tr>
												<!-- Start Side Panel Controls -->
												<td valign="top" align="center" width="246">
														
													<ajax:ajaxpanel id="ajpResults" runat="server">

														<table cellspacing="0" cellpadding="0" width="100%" border="0">
															<tr>
																<td align="center">
																						  
																	<!-- smart nav div-->
																	<div class="module">
																		<span class="ct"><span class="cl"></span></span>
																		<ul style="margin-top:8px;text-align:center;padding:0;">
																			<a runat="server" href="~/mykaneva/manageCredits.aspx"><img runat="server" src="~/images/wizard_btn_addcredits.gif" border="0" /></a></ul>
																		<span class="cb"><span class="cl"></span></span>
																	</div>
																				
																			
																	<div class="module playlistbox">
																		<span class="ct"><span class="cl"></span></span>
																		<h2>Transactions</h2>
																		<ul id="librarynav" >
																		
																			<li id="liFame" runat="server" class="transactionnav selected">
																				&nbsp;<asp:linkbutton id="lbFame" runat="server" oncommand="Nav_OnClick" commandargument="fame" name="Fame"><span class="navicon"></span>Fame</asp:linkbutton>
																			</li>	
																			<li id="liRewards" runat="server" class="transactionnav">
																				&nbsp;<asp:linkbutton id="lbRewards" runat="server" oncommand="Nav_OnClick" commandargument="rewards" name="Rewards"><span class="navicon"></span>Rewards</asp:linkbutton>
																			</li>	
																			<li id="liPurchases" runat="server" class="transactionnav">
																				&nbsp;<asp:linkbutton id="lbPurchases" runat="server" oncommand="Nav_OnClick" commandargument="purchases" name="Purchases"><span class="navicon"></span>Purchases</asp:linkbutton>
																			</li>	
																			<li id="liTrades" runat="server" class="transactionnav">
																				&nbsp;<asp:linkbutton id="lbTrades" runat="server" oncommand="Nav_OnClick" commandargument="trades" name="Trades"><span class="navicon"></span>Trades</asp:linkbutton>
																			</li>	
																			<li id="liGifts" runat="server" class="transactionnav">
																				&nbsp;<asp:linkbutton id="lbGifts" runat="server" oncommand="Nav_OnClick" commandargument="gifts" name="Gifts"><span class="navicon"></span>Gifts</asp:linkbutton>
																			</li>	
																			<li id="liCashPurchases" runat="server" class="transactionnav">
																				&nbsp;<asp:linkbutton id="lbCashPurchases" runat="server" oncommand="Nav_OnClick" commandargument="cash purchases" name="Cash Purchases"><span class="navicon"></span>Cash Purchases</asp:linkbutton>
																			</li>	
																			<li id="liRaves" runat="server" class="transactionnav">
																				&nbsp;<asp:linkbutton id="lbRaves" runat="server" oncommand="Nav_OnClick" commandargument="raves" name="Raves"><span class="navicon"></span>Raves</asp:linkbutton>
																			</li>
																			<li id="liPremiumItems" runat="server" class="transactionnav">
																				&nbsp;<asp:linkbutton id="lbPremiumItems" runat="server" oncommand="Nav_OnClick" commandargument="premium items" name="Premium Items"><span class="navicon"></span>Premium Items</asp:linkbutton>
																			</li>
																				
																		</ul>
																		<br />			
																		<span class="cb"><span class="cl"></span></span>
																	</div>
																
																
																	<div class="module playlistbox" style="display:none;">
																		<span class="ct"><span class="cl"></span></span>
																		<div id="divDesc" runat="server">
																		
																		</div>
																		<span class="cb"><span class="cl"></span></span>
																	</div>
																
																</td>					   
															</tr>
														</table>
														
													</ajax:ajaxpanel>
														
												</td>
												<!-- end Side Panel Controls -->
												<td width="14">
													<img id="img10" height="1" runat="server" src="~/images/spacer.gif" width="14">
												</td>
												
												<td valign="top" width="708">
													<table cellspacing="0" cellpadding="0" width="100%" border="0">
														<tr>
															<td>
																<!--START RIGHTCOL-->
																<div class="module whitebg">
																	<span class="ct"><span class="cl"></span></span>
																		
																		<ajax:ajaxpanel id="ajpControlBar" runat="server">
	
																			<table cellspacing="0" cellpadding="0" width="98%" border="0">
																				<tr>
																					<td align="left" valign="top">
																						<h2><asp:label runat="server" id="lblViewName" /></h2>
																						<div class="errBox black" style="position:relative;top:-6;display:none;" runat="server" id="divMessages"></div>
																					</td>
																				</tr>
																			</table>
																							
																		</ajax:ajaxpanel>
																		
																		<!-- END management controls -->
																		<!-- Begin display region -->
																		<table width="701" height="360" cellspacing="0" cellpadding="0" border="0">
																			<tr>
																				<td colspan="3" style="padding:0;text-align:center" valign="top">
																		
																					<ajax:ajaxpanel id="ajpDataRepeaters" runat="server">
																						
																						<div id="divNoResults" runat="server" style="display:none;text-align:center;">No Transactions found.</div>			
																					
																						<!-- repeaters go here -->	
																						<div id="divFameRpt" runat="server" style="display:none;">
																						<asp:repeater id="rptFameTransactions" runat="server">
																							<headertemplate>
																								<table cellpadding="4" cellspacing="0" border="0" width="700" class="data" style="margin-left:1px;">
																									<tr>
																										<td style="background-color:#fdfdfc;width:80px;">Date</th>
																										<td style="background-color:#fdfdfc;width:320px;">Achievement</th>
																										<td style="background-color:#fdfdfc;">Rewards</th>
																									</tr>
																							</headertemplate>
																							<itemtemplate>
																									<tr>
																										<td align="left"><%# Convert.ToDateTime(DataBinder.Eval (Container.DataItem, "LevelDate")).ToShortDateString() %></td>
																										<td align="left"><%# DataBinder.Eval (Container.DataItem, "FameTypeName") %> : Level <%#DataBinder.Eval (Container.DataItem, "LevelNumber")%></td>
																										<td align="left"><%# DisplayRewards (DataBinder.Eval (Container.DataItem, "LevelAwards"), DataBinder.Eval (Container.DataItem, "RewardsAwarded").ToString ())%></td>
																									</tr>
																							</itemtemplate>
																							<alternatingitemtemplate>
																									<tr class="altrow">
																										<td align="left"><%# Convert.ToDateTime(DataBinder.Eval (Container.DataItem, "LevelDate")).ToShortDateString() %></td>
																										<td align="left"><%# DataBinder.Eval (Container.DataItem, "FameTypeName") %> : Level <%#DataBinder.Eval (Container.DataItem, "LevelNumber")%></td>
																										<td align="left"><%# DisplayRewards (DataBinder.Eval (Container.DataItem, "LevelAwards"), DataBinder.Eval (Container.DataItem, "RewardsAwarded").ToString ())%></td>
																									</tr>
																							</alternatingitemtemplate>
																							<footertemplate>
																								</table>
																							</footertemplate>
																						</asp:repeater>
																						</div>
																						
																						<div id="divRewardsRpt" runat="server" style="display:none;">
																						<asp:repeater id="rptRewards" runat="server">
																							<headertemplate>
																								<table cellpadding="4" cellspacing="0" border="0" width="700" class="data" style="margin-left:1px;">
																									<tr>
																										<td style="background-color:#fdfdfc;width:80px;">Date</th>
																										<td style="background-color:#fdfdfc;width:520px;">Achievement</th>
																										<td style="background-color:#fdfdfc;">Amount</th>
																									</tr>
																							</headertemplate>
																							<itemtemplate>
																									<tr>
																										<td align="left"><%# Convert.ToDateTime(DataBinder.Eval (Container.DataItem, "RewardDate")).ToShortDateString() %></td>
																										<td align="left"><%# DataBinder.Eval (Container.DataItem, "TransactionDescription") + " : " + DataBinder.Eval (Container.DataItem, "EventName") %></td>
																										<td align="left"><%# DataBinder.Eval (Container.DataItem, "RewardAmount") %></td>
																									</tr>
																							</itemtemplate>
																							<alternatingitemtemplate>
																									<tr class="altrow">
																										<td align="left"><%# Convert.ToDateTime(DataBinder.Eval (Container.DataItem, "RewardDate")).ToShortDateString() %></td>
																										<td align="left"><%# DataBinder.Eval (Container.DataItem, "TransactionDescription") + " : " + DataBinder.Eval (Container.DataItem, "EventName") %></td>
																										<td align="left"><%# DataBinder.Eval (Container.DataItem, "RewardAmount") %></td>
																									</tr>
																							</alternatingitemtemplate>
																							<footertemplate>
																								</table>
																							</footertemplate>
																						</asp:repeater>
																						</div>
																					
																						<div id="divPurchasesRpt" runat="server" style="display:none;">
																							<div id="divPurchaseTypes" style="display:block;margin:10px 0;text-align:center;">
																								<asp:linkbutton id="lbItemPurchases" runat="server" oncommand="SetPurchaseType" commandargument="items" text="Purchased Items"></asp:linkbutton> - 
																								<asp:linkbutton id="lbRavePurchases" runat="server" oncommand="SetPurchaseType" commandargument="raves" text="Purchased Raves"></asp:linkbutton>
																							</div>
																							
																							<div id="divPurchasesNoResults" runat="server" style="display:none;text-align:center;margin-top:30px;">No purchases found.</div>
																							
																							<div id="divPurchases" runat="server" style="display:none;">
																								<asp:repeater id="rptPurchases" runat="server">
																									<headertemplate>
																										<table cellpadding="4" cellspacing="0" border="0" width="700" class="data" style="margin-left:1px;">
																											<tr>
																												<td style="background-color:#fdfdfc;width:80px;">Date</th>
																												<td style="background-color:#fdfdfc;width:410px;">Purchased</th>
																												<td style="background-color:#fdfdfc;width:70px;" align="center">Quantity</th>
																												<td style="background-color:#fdfdfc;width:70px;">Item Price</th>
																												<td style="background-color:#fdfdfc;width:70px;">Total</th>
																											</tr>
																									</headertemplate>
																									<itemtemplate>
																											<tr>
																												<td align="left"><%# Convert.ToDateTime(DataBinder.Eval (Container.DataItem, "PurchaseDate")).ToShortDateString() %></td>
																												<td align="left"><%# (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "TransactionTypeId")) == 29 ? GetMegaRaveDesc (DataBinder.Eval (Container.DataItem, "GlobalId")) : DataBinder.Eval (Container.DataItem, "DisplayName") + (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "TransactionTypeId")) == 4 ? " (Return)" : "")) + (Convert.ToInt32(DataBinder.Eval (Container.DataItem, "ItemPurchaseTransactionType")) == 2 ? "  <span class='gm'>(GM Free Purchase)</span>" : "")%></td>
																												<td align="center"><%# DataBinder.Eval (Container.DataItem, "Quantity") %></td>
																												<td align="left"><%# FormatPrice(DataBinder.Eval (Container.DataItem, "ItemPrice")) + ((DataBinder.Eval (Container.DataItem, "KeiPointId").ToString () == "GPOINT" ? " R" : " C"))%></td>
																												<td align="left"><%# CalcTotal (Convert.ToInt32(DataBinder.Eval (Container.DataItem, "Quantity")), Convert.ToInt32(DataBinder.Eval (Container.DataItem, "ItemPrice")), Convert.ToInt32(DataBinder.Eval (Container.DataItem, "ItemPurchaseTransactionType"))) + ((DataBinder.Eval (Container.DataItem, "KeiPointId").ToString () == "GPOINT" ? " R" : " C"))%></td>
																											</tr>
																									</itemtemplate>
																									<alternatingitemtemplate>
																											<tr class="altrow">
																												<td align="left"><%# Convert.ToDateTime(DataBinder.Eval (Container.DataItem, "PurchaseDate")).ToShortDateString() %></td>
																												<td align="left"><%# (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "TransactionTypeId")) == 29 ? GetMegaRaveDesc (DataBinder.Eval (Container.DataItem, "GlobalId")) : DataBinder.Eval (Container.DataItem, "DisplayName") + (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "TransactionTypeId")) == 4 ? " (Return)" : "")) + (Convert.ToInt32(DataBinder.Eval (Container.DataItem, "ItemPurchaseTransactionType")) == 2 ? "  <span class='gm'>(GM Free Purchase)</span>" : "")%></td>
																												<td align="center"><%# DataBinder.Eval (Container.DataItem, "Quantity") %></td>
																												<td align="left"><%# FormatPrice(DataBinder.Eval (Container.DataItem, "ItemPrice")) + ((DataBinder.Eval (Container.DataItem, "KeiPointId").ToString () == "GPOINT" ? " R" : " C")) %></td>
																												<td align="left"><%# CalcTotal (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "Quantity")), Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "ItemPrice")), Convert.ToInt32(DataBinder.Eval (Container.DataItem, "ItemPurchaseTransactionType"))) + ((DataBinder.Eval (Container.DataItem, "KeiPointId").ToString () == "GPOINT" ? " R" : " C"))%></td>
																											</tr>
																									</alternatingitemtemplate>
																									<footertemplate>
																										</table>
																									</footertemplate>
																								</asp:repeater>
																							</div>
																						</div>
																						
																						<div id="divTradesRpt" runat="server" style="display:none;">
																						<asp:repeater id="rptTrades" runat="server">
																							<headertemplate>
																								<table cellpadding="4" cellspacing="0" border="0" width="700" class="data" style="margin-left:1px;">
																									<tr>
																										<td style="background-color:#fdfdfc;width:80px;">Date</th>
																										<td style="background-color:#fdfdfc;width:80px;">Action</th>
																										<td style="background-color:#fdfdfc;width:180px;">Person</th>
																										<td style="background-color:#fdfdfc;width:220px;">Item</th>
																										<td style="background-color:#fdfdfc;" align="center">Quantity</th>
																										<td style="background-color:#fdfdfc;width:70px;" align="center">Cost</th>
																									</tr>
																							</headertemplate>
																							<itemtemplate>
																									<tr>
																										<td align="left"><%# Convert.ToDateTime(DataBinder.Eval (Container.DataItem, "TransactionDate")).ToShortDateString() %></td>
																										<td align="left"><%# GetTradeActionType (DataBinder.Eval (Container.DataItem, "ToId")) %></td>
																										<td align="left"><%# GetUsernameFromUserId (DataBinder.Eval (Container.DataItem, "ToId"), DataBinder.Eval (Container.DataItem, "FromId")) %></td>
																										<td align="left"><%# GetTradedItem (DataBinder.Eval (Container.DataItem, "ItemName"), DataBinder.Eval (Container.DataItem, "KeiPointId"))%></td>
																										<td align="center"><%# DataBinder.Eval (Container.DataItem, "Quantity") %></td>
																										<td align="center" nowrap><%# Math.Abs(Convert.ToInt32(DataBinder.Eval (Container.DataItem, "TransactionAmount"))) + ((DataBinder.Eval (Container.DataItem, "KeiPointId").ToString () == "GPOINT" ? " R" : " C"))%></td>
																									</tr>
																							</itemtemplate>
																							<alternatingitemtemplate>
																									<tr class="altrow">
																										<td align="left"><%# Convert.ToDateTime (DataBinder.Eval (Container.DataItem, "TransactionDate")).ToShortDateString ()%></td>
																										<td align="left"><%# GetTradeActionType (DataBinder.Eval (Container.DataItem, "ToId"))%></td>
																										<td align="left"><%# GetUsernameFromUserId (DataBinder.Eval (Container.DataItem, "ToId"), DataBinder.Eval (Container.DataItem, "FromId")) %></td>
																										<td align="left"><%# GetTradedItem (DataBinder.Eval (Container.DataItem, "ItemName"), DataBinder.Eval (Container.DataItem, "KeiPointId"))%></td>
																										<td align="center"><%# DataBinder.Eval (Container.DataItem, "Quantity") %></td>
																										<td align="center" nowrap><%# Math.Abs(Convert.ToInt32(DataBinder.Eval (Container.DataItem, "TransactionAmount"))) + ((DataBinder.Eval (Container.DataItem, "KeiPointId").ToString () == "GPOINT" ? " R" : " C"))%></td>
																									</tr>
																							</alternatingitemtemplate>
																							<footertemplate>
																								</table>
																							</footertemplate>
																						</asp:repeater>
																						</div>
																						
																						<div id="divGiftsRpt" runat="server" style="display:none;">
																						<asp:repeater id="rptGifts" runat="server">
																							<headertemplate>
																								<table cellpadding="4" cellspacing="0" border="0" width="700" class="data" style="margin-left:1px;">
																									<tr>
																										<td style="background-color:#fdfdfc;width:80px;">Date</th>
																										<td style="background-color:#fdfdfc;width:70px;">Action</th>
																										<td style="background-color:#fdfdfc;width:180px;">Person</th>
																										<td style="background-color:#fdfdfc;width:220px;">Gift</th>
																										<td style="background-color:#fdfdfc;" align="center">Quantity</th>
																									</tr>
																							</headertemplate>
																							<itemtemplate>
																									<tr>
																										<td align="left"><%# Convert.ToDateTime(DataBinder.Eval (Container.DataItem, "TransactionDate")).ToShortDateString() %></td>
																										<td align="left"><%# GetGiftActionType (DataBinder.Eval (Container.DataItem, "ToId")) %></td>
																										<td align="left"><%# GetUsernameFromUserId (DataBinder.Eval (Container.DataItem, "ToId"), DataBinder.Eval (Container.DataItem, "FromId")) %></td>
																										<td align="left"><%# DataBinder.Eval (Container.DataItem, "ItemName") %></td>
																										<td align="center"><%# DataBinder.Eval (Container.DataItem, "Quantity") %></td>
																									</tr>
																							</itemtemplate>
																							<alternatingitemtemplate>
																									<tr class="altrow">
																										<td align="left"><%# Convert.ToDateTime (DataBinder.Eval (Container.DataItem, "TransactionDate")).ToShortDateString ()%></td>
																										<td align="left"><%# GetGiftActionType (DataBinder.Eval (Container.DataItem, "ToId"))%></td>
																										<td align="left"><%# GetUsernameFromUserId (DataBinder.Eval (Container.DataItem, "ToId"), DataBinder.Eval (Container.DataItem, "FromId")) %></td>
																										<td align="left"><%# DataBinder.Eval (Container.DataItem, "ItemName")%></td>
																										<td align="center"><%# DataBinder.Eval (Container.DataItem, "Quantity") %></td>
																									</tr>
																							</alternatingitemtemplate>
																							<footertemplate>
																								</table>
																							</footertemplate>
																						</asp:repeater>
																						</div>
																						
																						<div id="divCashPurchasesRpt" runat="server" style="display:none;">
																						<asp:repeater id="rptCashPurchases" runat="server">
																							<headertemplate>
																								<table cellpadding="4" cellspacing="0" border="0" width="700" class="data" style="margin-left:1px;">
																									<tr>
																										<td style="background-color:#fdfdfc;width:80px;">Date</th>
																										<td style="background-color:#fdfdfc;width:90px;" align="center">Order Number</th>
																										<td style="background-color:#fdfdfc;width:470px;">Purchase</th>
																										<td style="background-color:#fdfdfc;width:60px;">Price</th>
																									</tr>
																							</headertemplate>
																							<itemtemplate>
																									<tr>
																										<td align="left"><%# Convert.ToDateTime (DataBinder.Eval (Container.DataItem, "transaction_date")).ToShortDateString ()%></td>
																										<td align="center"><a href="historyorderdetails.aspx?orderId=<%# DataBinder.Eval(Container.DataItem, "order_id")%>"><%# DataBinder.Eval(Container.DataItem, "order_id")%></a></td>
																										<td align="left"><%# DataBinder.Eval (Container.DataItem, "ppt_description")%></td>
																										<td align="left"><%# FormatCurrency (DataBinder.Eval (Container.DataItem, "amount_debited")) %></td>
																									</tr>
																							</itemtemplate>
																							<alternatingitemtemplate>
																									<tr class="altrow">
																										<td align="left"><%# Convert.ToDateTime (DataBinder.Eval (Container.DataItem, "transaction_date")).ToShortDateString ()%></td>
																										<td align="center"><a href="historyorderdetails.aspx?orderId=<%# DataBinder.Eval(Container.DataItem, "order_id")%>"><%# DataBinder.Eval(Container.DataItem, "order_id")%></a></td>
																										<td align="left"><%# DataBinder.Eval (Container.DataItem, "ppt_description")%></td>
																										<td align="left"><%# FormatCurrency (DataBinder.Eval (Container.DataItem, "amount_debited")) %></td>
																									</tr>
																							</alternatingitemtemplate>
																							<footertemplate>
																								</table>
																							</footertemplate>
																						</asp:repeater>
																						</div>							 
																						
																						<div id="divRavesRpt" runat="server" style="display:none;">
																							<div id="divRaveTypes" style="display:block;text-align:center;">
																								<asp:linkbutton id="lbRavedMe" runat="server" oncommand="SetRaveType" commandargument="false" commandname="isRaver" text="Raved Me and My Stuff"></asp:linkbutton> - 
																								<asp:linkbutton id="lbRavedByMe" runat="server" oncommand="SetRaveType" commandargument="true" commandname="isRaver" text="Raved by Me"></asp:linkbutton>
																							</div>
																							<div id="divRaveItemTypes" style="margin:10px 0;display:block;text-align:center;">
																								<asp:linkbutton id="lbAssetRaves" runat="server" oncommand="SetRaveItemType" commandargument="assets" text="Media"></asp:linkbutton>
																								<asp:linkbutton id="lbProfileCommunities" runat="server" oncommand="SetRaveItemType" commandargument="profilecommunities" text="Profile/Communities"></asp:linkbutton>
																								<asp:linkbutton id="lb3dHomeHangouts" runat="server" oncommand="SetRaveItemType" commandargument="3dhomehangouts" text="3D Home/Hangouts"></asp:linkbutton>
																								<asp:linkbutton id="lbUGC" runat="server" oncommand="SetRaveItemType" commandargument="ugc" text="UGC"></asp:linkbutton>
																							</div>
																							
																							<div id="divRaveNoResults" runat="server" style="display:none;text-align:center;margin-top:30px;">No Transactions found.</div>
																							
																							<div id="divRavesMyStuff" runat="server" style="display:none;">
																								<asp:repeater id="rptRavesMyStuff" runat="server">
																									<headertemplate>												
																										<table cellpadding="4" cellspacing="0" border="0" width="700" class="data" style="margin-left:1px;">
																											<tr>
																												<td style="background-color:#fdfdfc;width:90px;">Date</th>
																												<td style="background-color:#fdfdfc;width:450px;">What was Raved</th>
																												<td style="background-color:#fdfdfc;width:160px;">Who Raved</th>
																											</tr>
																									</headertemplate>
																									<itemtemplate>
																											<tr>
																												<td align="left"><%# Convert.ToDateTime (DataBinder.Eval (Container.DataItem, "TransactionDate")).ToShortDateString ()%></td>
																												<td align="left"><%# GetWhatWasRaved (DataBinder.Eval (Container.DataItem, "ItemId"), DataBinder.Eval (Container.DataItem, "ItemName"), DataBinder.Eval (Container.DataItem, "ItemType"), DataBinder.Eval (Container.DataItem, "IsMegaRave")) %></td>
																												<td align="left"><%# GetPersonalChannelLink (DataBinder.Eval (Container.DataItem, "RaverId"), DataBinder.Eval (Container.DataItem, "RaverName"))%></td>
																											</tr>
																									</itemtemplate>						   
																									<alternatingitemtemplate>
																											<tr class="altrow">
																												<td align="left"><%# Convert.ToDateTime (DataBinder.Eval (Container.DataItem, "TransactionDate")).ToShortDateString ()%></td>
																												<td align="left"><%# GetWhatWasRaved (DataBinder.Eval (Container.DataItem, "ItemId"), DataBinder.Eval (Container.DataItem, "ItemName"), DataBinder.Eval (Container.DataItem, "ItemType"), DataBinder.Eval (Container.DataItem, "IsMegaRave")) %></td>
																												<td align="left"><%# GetPersonalChannelLink (DataBinder.Eval (Container.DataItem, "RaverId"), DataBinder.Eval (Container.DataItem, "RaverName"))%></td>
																											</tr>
																									</alternatingitemtemplate>
																									<footertemplate>
																										</table>
																									</footertemplate>
																								</asp:repeater>
																							</div>

																							<div id="divRavesByMe" runat="server" style="display:none;">
																								<asp:repeater id="rptRavesByMe" runat="server">
																									<headertemplate>												
																										<table cellpadding="4" cellspacing="0" border="0" width="700" class="data" style="margin-left:1px;">
																											<tr>
																												<td style="background-color:#fdfdfc;width:90px;">Date</th>
																												<td style="background-color:#fdfdfc;width:450px;">What I Raved</th>
																												<td style="background-color:#fdfdfc;width:160px;">Owner</th>																												  
																											</tr>
																									</headertemplate>
																									<itemtemplate>
																											<tr>
																												<td align="left"><%# Convert.ToDateTime (DataBinder.Eval (Container.DataItem, "TransactionDate")).ToShortDateString ()%></td>
																												<td align="left"><%# GetWhatWasRaved (DataBinder.Eval (Container.DataItem, "ItemId"), DataBinder.Eval (Container.DataItem, "ItemName"), DataBinder.Eval (Container.DataItem, "ItemType"), DataBinder.Eval (Container.DataItem, "IsMegaRave")) %></td>
																												<td align="left"><%# GetPersonalChannelLink (DataBinder.Eval (Container.DataItem, "ItemOwnerId"), DataBinder.Eval (Container.DataItem, "ItemOwnerName"))%></td>
																											</tr>
																									</itemtemplate>						   
																									<alternatingitemtemplate>
																											<tr class="altrow">
																												<td align="left"><%# Convert.ToDateTime (DataBinder.Eval (Container.DataItem, "TransactionDate")).ToShortDateString ()%></td>
																												<td align="left"><%# GetWhatWasRaved (DataBinder.Eval (Container.DataItem, "ItemId"), DataBinder.Eval (Container.DataItem, "ItemName"), DataBinder.Eval (Container.DataItem, "ItemType"), DataBinder.Eval (Container.DataItem, "IsMegaRave")) %></td>
																												<td align="left"><%# GetPersonalChannelLink (DataBinder.Eval (Container.DataItem, "ItemOwnerId"), DataBinder.Eval (Container.DataItem, "ItemOwnerName"))%></td>
																											</tr>
																									</alternatingitemtemplate>
																									<footertemplate>
																										</table>
																									</footertemplate>
																								</asp:repeater>
																							</div>
																						</div>

																						<div id="divPremiumItemsRpt" runat="server" style="display:none;">
																							<div id="divPremiumItemsNoResults" runat="server" style="display:none;">No purchases found.</div>
																							
																							<div id="divPremiumItems" runat="server" style="display:none;">
																								<asp:repeater id="rptPremiumItems" runat="server">
																									<headertemplate>
																										<table cellpadding="4" cellspacing="0" border="0" width="700" class="data" style="margin-left:1px;">
																											<tr>
																												<td style="background-color:#fdfdfc;width:80px;">Date</th>
																												<td style="background-color:#fdfdfc;width:410px;">Purchased</th>
																												<td style="background-color:#fdfdfc;width:70px;" align="center">Quantity</th>
																												<td style="background-color:#fdfdfc;width:70px;">Item Price</th>
																												<td style="background-color:#fdfdfc;width:70px;">Total</th>
																											</tr>
																									</headertemplate>
																									<itemtemplate>
																											<tr>
																												<td align="left"><%# Convert.ToDateTime(DataBinder.Eval (Container.DataItem, "PurchaseDate")).ToShortDateString() %></td>
																												<td align="left"><%# (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "TransactionTypeId")) == 29 ? GetMegaRaveDesc (DataBinder.Eval (Container.DataItem, "GlobalId")) : DataBinder.Eval (Container.DataItem, "DisplayName") + (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "TransactionTypeId")) == 4 ? " (Return)" : ""))%></td>
																												<td align="center"><%# DataBinder.Eval (Container.DataItem, "Quantity") %></td>
																												<td align="left"><%# DataBinder.Eval (Container.DataItem, "ItemPrice") + ((DataBinder.Eval (Container.DataItem, "KeiPointId").ToString () == "GPOINT" ? " R" : " C"))%></td>
																												<td align="left"><%# CalcTotal (Convert.ToInt32(DataBinder.Eval (Container.DataItem, "Quantity")), Convert.ToInt32(DataBinder.Eval (Container.DataItem, "ItemPrice")),0) + ((DataBinder.Eval (Container.DataItem, "KeiPointId").ToString () == "GPOINT" ? " R" : " C"))%></td>
																											</tr>
																									</itemtemplate>
																									<alternatingitemtemplate>
																											<tr class="altrow">
																												<td align="left"><%# Convert.ToDateTime(DataBinder.Eval (Container.DataItem, "PurchaseDate")).ToShortDateString() %></td>
																												<td align="left"><%# (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "TransactionTypeId")) == 29 ? GetMegaRaveDesc (DataBinder.Eval (Container.DataItem, "GlobalId")) : DataBinder.Eval (Container.DataItem, "DisplayName") + (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "TransactionTypeId")) == 4 ? " (Return)" : ""))%></td>
																												<td align="center"><%# DataBinder.Eval (Container.DataItem, "Quantity") %></td>
																												<td align="left"><%# DataBinder.Eval (Container.DataItem, "ItemPrice") + ((DataBinder.Eval (Container.DataItem, "KeiPointId").ToString () == "GPOINT" ? " R" : " C")) %></td>
																												<td align="left"><%# CalcTotal (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "Quantity")), Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "ItemPrice")),0) + ((DataBinder.Eval (Container.DataItem, "KeiPointId").ToString () == "GPOINT" ? " R" : " C"))%></td>
																											</tr>
																									</alternatingitemtemplate>
																									<footertemplate>
																										</table>
																									</footertemplate>
																								</asp:repeater>
																							</div>
																						</div>

																					</ajax:ajaxpanel>
																			
																				</td>
																			</tr>
																			
																		</table>
																		
																	<span class="cb"><span class="cl"></span></span>
																</div>
																<!--END RIGHTCOL-->
															</td>
														</tr>
													</table>
												</td>
												
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td valign="top" align="right" style="padding-right:8px;">
									
										<ajax:ajaxpanel id="ajpBottomPager" runat="server">
											<img height="10" runat="server" src="~/images/spacer.gif" width="1">
											<asp:label runat="server" id="lblResults_Bottom" cssclass="insideTextNoBold" /><br />
											<kaneva:pager id="pgBottom" onpagechanged="pg_PageChange" maxpagestodisplay="5" runat="server" isajaxmode="True" shownextprevlabels="true"></kaneva:pager>
										</ajax:ajaxpanel>
										
									</td>
								</tr>
							</table>
						</td>
						<td class="">
							<img id="Img5" height="1" runat="server" src="~/images/spacer.gif" width="1">
						</td>
					</tr>
					<tr>
						<td class=""></td>
						<td class=""></td>
						<td class=""></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<!-- END MAIN BODY -->
</div>
										
									


				  