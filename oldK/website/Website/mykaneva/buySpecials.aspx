<%@ page language="C#" masterpagefile="~/masterpages/IndexPageTemplate.Master" autoeventwireup="false" codebehind="buySpecials.aspx.cs" inherits="KlausEnt.KEP.Kaneva.buySpecials" %>

<%@ register tagprefix="Kaneva" tagname="WizardNavBar" src="../usercontrols/checkoutwizardnavbar.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:content id="cnt_buySpecials" runat="server" contentplaceholderid="cph_Body">
	<script type="text/javascript"><!--

		var divConfirmDuplicatePass_ID = '<%= divConfirmDuplicatePass.ClientID %>';

		function hideConfirmDiv() {
			var div = document.getElementById(divConfirmDuplicatePass_ID);
			div.style.visibility = "hidden";
		}

//--> </script>
	<div class="wrapper PageContainer">
		<div class="manageCredits">
			<ul class="">
				<li><span style="color: #13667a; font-size: 10pt; font-weight: bold; padding-left: 20px;">Manage Your Account</span></li>
				<li id="Li3" class=""><a href="~/mykaneva/billingInfoSummary.aspx" runat="server" id="a4" class="nohover">Manage Billing Settings</a> </li>
				<li id="Li1" class=""><a href="~/mykaneva/transactions.aspx?CashP=Y" runat="server" id="a2" class="nohover">View Purchase History</a> </li>
				<li id="Li2" class="" style="width: 75px;"><a href="~/mykaneva/creditfaq.aspx" runat="server" id="a3" class="nohover">Credit FAQs</a> </li>
				<li style="width: 130px; padding-left: 54px;"><a href="buyCreditPackages.aspx">
					<img src="../images/purchase_flow/btnCreditBundles.gif" alt="Credit Bundles" style="vertical-align: top" /></a> </li>
				<li><a href="buySpecials.aspx" id="lnk_Specials" runat="server">
					<img src="../images/purchase_flow/btn_viewSpecials.gif" alt="Credit Bundles" style="vertical-align: top" /></a> <a href="buySpecials.aspx?pass=true" id="lnk_Passes" runat="server">
						<img src="../images/purchase_flow/btn_viewPasses.gif" alt="Credit Bundles" style="vertical-align: top" /></a> </li>
			</ul>
		</div>
	<asp:updatepanel id="upHidePurchase" runat="server">
		<contenttemplate>
            <div style="width:100%;text-align:center;padding-left:100px;"><div id="spnMessage"  runat="server" class="alertmessage" visible="false"></div></div>
            <div class="module">
                <span class="ct"><span class="cl"></span></span>
        
                <!-- new content -->
	            <div class="spclDiv">
                    <div id="div_specialsHeading" runat="server"><h1>Current Specials &#8212; Limited Time Only!</h1></div>
                    <div id="div_passessHeading" runat="server"><h1>Passes & Subscriptions</h1></div>
                        <asp:Repeater id="rptSpecials" runat="server">
                        <ItemTemplate>
                            <!-- Start Special -->
                            <div class="spBorder" id="divOuterAdContainer" runat="server">
								<div id="divAdRotator" runat="server" class="spContainer" style="padding-top:0px;" >
								    
								</div>
								                                												
                                <div class="spContainer" id="divAdContainer" runat="server">
                                    <div class="spAdImg"><img src="images/purchase_flow/sp_move-in.jpg" alt="Kaneva's Move in Special" /></div>        
                                    <div class="spTxt">
                                        <h2 id="specialHeading" visible="false" runat="server"><%# DataBinder.Eval(Container.DataItem, "BundleTitle").ToString()%></h2>
                                        <h2 id="specialSub1" visible="false" runat="server"><%# DataBinder.Eval(Container.DataItem, "BundleSubheading1").ToString()%></h2>
                                        <h2 id="specialSub2" visible="false" runat="server"><%# DataBinder.Eval(Container.DataItem, "BundleSubheading2").ToString()%></h2>
                                        <!--<h3 id="specialSub3" visible="false" runat="server"><%# DataBinder.Eval(Container.DataItem, "PromotionalPackageLabel").ToString()%></h3>-->

                                        <input runat="server" id="ihd_promotionId" type="hidden" value='<%# DataBinder.Eval(Container.DataItem, "PromotionId").ToString() %>' />
                                        <p visible="false" id="parHeading" runat="server">Plus you'll get these item(s)!</p>
                                        <asp:DataList id="dlSpecialItems" runat="server" ShowFooter="False" onitemdatabound="dlSpecialItems_ItemDataBound" cellpadding="0" cellspacing="0" RepeatColumns="2" RepeatDirection="Horizontal">
                                            <ItemTemplate>
                                                    <table id="tblspecialItems" border="0" cellpadding="2" cellspacing="0">
                                                        <tr style="line-height:10px;">
                                                            <td id="itemquantity" runat="server" valign="top" align="left"><%# TruncateWithEllipsis(DataBinder.Eval(Container.DataItem, "quantity").ToString(),22) %></td>
                                                            <td id="itemname" runat="server" valign="top" align="left" ><%# TruncateWithEllipsis(DataBinder.Eval(Container.DataItem, "alternate_description").ToString() != "" ? DataBinder.Eval(Container.DataItem, "alternate_description").ToString() : DataBinder.Eval(Container.DataItem, "item_description").ToString(), 22)%></td>
                                                        </tr>
                                                    </table>
                                            </ItemTemplate>
                                        </asp:DataList>
                                    </div>
                                    <div class="clear"><!-- clear the floats --></div>
                                </div>
                                <div id="specialPricing" runat="server" class="spPrice" visible="false">
                                  <table>
                                    <tr>
                                      <th><!-- empty --></th>
                                      <th id="thSpecialSavings" runat="server">plus <%# DataBinder.Eval(Container.DataItem, "KeiPointAmount").ToString()%> Credits &#8211; a <asp:Label ID="lblSavings" runat="server" /> savings!</th>
                                    </tr>
                                    <tr>
                                      <td style="width:25px;"><!--<input name="" type="checkbox" value="" />--></td>
                                      <td>Buy Now for<br />
                                        $<%# DataBinder.Eval(Container.DataItem, "DollarAmount").ToString()%>
                                      </td>
                                    </tr>
                                    <tr>
                                      <td><!-- empty --></td>
                                      <td>
                                          <div class="spPricePP"><asp:LinkButton ID="lb_PurchaseSpecialPP" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PromotionId").ToString() %>' CommandName="paypal" OnCommand="Purchase_Command" ></asp:LinkButton></div>
                                          <div class="spPriceCredit"><asp:LinkButton ID="lb_PurchaseSpecial" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PromotionId").ToString() %>' CommandName="credit" OnCommand="Purchase_Command" ></asp:LinkButton></div>
                                      </td>
                                    </tr>
                                  </table>
                                </div>
                                <div id="passPricing" runat="server" class="pPrice" visible="false" >
                                    <div class="pTrialPass">
                                        <asp:LinkButton ID="lbFreeTrial" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PromotionId").ToString() %>' CommandName="trial" OnCommand="Purchase_Command" ><asp:Label ID="lbl_DaysFree" runat="server" /> DAY FREE TRIAL</asp:LinkButton>
                                    </div>
                                    <div class="pLearnMore">
                                        <asp:LinkButton ID="lbLearnMore" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PromotionId").ToString() %>' CommandName="learn" OnCommand="Purchase_Command" >Learn More </asp:LinkButton>
                                    </div>
                                </div>
                                
                                <div class="clear"><!-- clear the floats --></div>
                            </div>
                            <!-- end Special -->
                            
                         </ItemTemplate>
                    </asp:Repeater> 
                        <!-- <div class="addSpclBtn"><a href="">Add to Cart</a></div> -->
                        <!-- End Column 1 -->
                        </div>
                <div class="clear"><!-- clear the floats --></div>
                <span class="cb"><span class="cl"></span></span> 
            </div>
            
			<div id="divConfirmDuplicatePass" runat="server" class="popupWarning" style="text-align:center;" visible="false">
				<div>
					<asp:Literal id="confirmMessage" runat="server" />
				</div>
				<div>
					<br />
					<asp:Button id="btn_ConfirmDuplicatePurchase" causesvalidation="false" CommandName="buyDupPass" OnCommand="Purchase_Command" runat="server" Text="Continue" />
					<!--<asp:Button id="btn_CancelDuplicatePurchase" causesvalidation="false" onClientClick="javascript:" Text="Cancel" />-->
					<input id="Button1" type="button" onclick="hideConfirmDiv()" value="Cancel" />
				</div>
			</div>

		</contenttemplate>
	</asp:updatepanel>
	</div>
</asp:content>
