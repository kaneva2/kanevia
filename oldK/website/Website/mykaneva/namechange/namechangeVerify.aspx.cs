///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using log4net;
using System.Security.Cryptography;
//using System.Security.Principal;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
    public partial class namechangeVerify : MainTemplatePage
    {
        protected namechangeVerify () 
		{
			Title = "Kaneva Name Change";
		}

        protected void Page_Load (object sender, EventArgs e)
        {
            // They must be logged in
            if (!Request.IsAuthenticated)
            {
                Response.Redirect (GetLoginURL ());
                return;
            }

            // Check to make sure there is a value for name change cost.  If not, then don't let
            // user continue changing their name.  This is just to make sure all config files have 
            // the NameChangeCost value and it was not forgotten by mistake.
            if (System.Configuration.ConfigurationManager.AppSettings["NameChangeCost"] == null)
            {
                Response.Redirect ("~/mykaneva/general.aspx", false);
            }

            spnCurrentName.InnerText = KanevaWebGlobals.CurrentUser.Username;

            if (Request.QueryString["u"] != null && Request.QueryString["u"].ToString () != string.Empty)
            {
                spnChangeTo.InnerText = Request.QueryString["u"].ToString ();
                nameName.InnerText = spnChangeTo.InnerText;
            }
            else
            {
                Response.Redirect ("~/mykaneva/namechange/namechangeSelect.aspx", false);
            }

            // Add onclick event to password text field
            txtPassword.Attributes.Add ("onkeyup", "if(checkForEnterKey(event))" + this.ClientScript.GetPostBackEventReference (btnChange, "") + ";");
            //txtPassword.Attributes.Add ("onkeyup", "if(checkForEnterKey(event))$('btnChange.click();");  

            aBack.HRef = ResolveUrl ("~/mykaneva/namechange/namechangeSelect.aspx?u=" + Server.UrlEncode (spnChangeTo.InnerText));
            spnCost.InnerText = System.Configuration.ConfigurationManager.AppSettings["NameChangeCost"].ToString () + " credits";
                      
            this.SetFocus (txtPassword);
        }

        protected void btnChange_Click (object sender, EventArgs e)
        {
            UserFacade userFacade = new UserFacade();

            try
            {
                if (!Page.IsValid)
                {
                    return;
                }

                string newUserName = spnChangeTo.InnerText;
                string oldUserName = KanevaWebGlobals.CurrentUser.Username;
                string queryString = "";
                int userId = KanevaWebGlobals.CurrentUser.UserId;
                decimal amount = Convert.ToDecimal (System.Configuration.ConfigurationManager.AppSettings["NameChangeCost"]);

                // Verify username is still available
                if (UsersUtility.GetUserIdFromUsername (newUserName) > 0)
                {
                    cvBlank.IsValid = false;
                    cvBlank.ErrorMessage = "The Nickname already exists. Please change your Nickname.";
                    valSum.Style.Add ("display", "block");
                    return;
                }

                // Create an order
                int orderId = CreateOrder (userId, amount);
                if (orderId > 0)
                {
                    int roleMembership = 0;
                    string strPassword = Server.HtmlEncode (txtPassword.Text.Trim ());

                    // Verify user entered correct password
                    int validLogin = UsersUtility.Authorize (KanevaWebGlobals.CurrentUser.Email, strPassword, 0, ref roleMembership, Common.GetVisitorIPAddress(), true);

                    if (validLogin != (int) Constants.eLOGIN_RESULTS.SUCCESS)
                    {
                        // Password is not correct, do not proceed
                        cvBlank.IsValid = false;
                        cvBlank.ErrorMessage = "The password is not correct. Please reenter your password.";
                        valSum.Style.Add ("display", "block");
                        return;
                    }
                    
                    //if (UsersUtility.UpdateUserName (userId, newUserName) == 1)
                    int ret = userFacade.UpdateUserName(userId, oldUserName, newUserName);
                    if (ret == 0)
                    {
                        // Update the password with new username
                        byte[] salt = new byte[9];
                        new RNGCryptoServiceProvider ().GetBytes (salt);
                        string hashPassword = FormsAuthentication.HashPasswordForStoringInConfigFile (UsersUtility.MakeHash (strPassword + newUserName.ToLower ()) + Convert.ToBase64String (salt), "MD5");

                        byte[] keyvalue = new byte[10];
                        new RNGCryptoServiceProvider ().GetBytes (keyvalue);

                        // Update user record with new hashed password
                        userFacade.UpdatePassword(userId, hashPassword, Convert.ToBase64String(salt));

                        //// Reset user cookie
                        //DataRow drUser = UsersUtility.GetUser (userId);
                        //FormsAuthentication.SetAuthCookie (drUser["userName"].ToString (), false);
                        
                        // Complete this order
                        CompletePendingOrder (userId, orderId);
                    }
                    else
                    {
                        throw new Exception ("Failed to update user record with new name.");
                    }
                }
                else
                {
                    throw new Exception ("Error creating order record for name change.");
                }

                // Redirect to complete page
                Response.Redirect ("~/mykaneva/namechange/namechangeComplete.aspx" + queryString, false);
            }
            catch (Exception ex)
            {
                // Display err message
                cvBlank.IsValid = false;
                cvBlank.ErrorMessage = "An error was encountered while processing your request. Err: " + ex.Message;
                valSum.Style.Add ("display", "block");
                m_logger.Error ("Error saving name change: userId=" + KanevaWebGlobals.CurrentUser.UserId.ToString(), ex);
            }
        }

        private int CreateOrder (int userId, decimal amount)
        {
            DataRow drOrder = null;

            if (Request["orderId"] != null && Request["orderId"] != string.Empty)
            {
                try
                {
                    drOrder = StoreUtility.GetOrder (Convert.ToInt32 (Request["orderId"]), userId);
                }
                catch { }
            }

            int orderId = 0;
            if (drOrder != null && drOrder["order_id"].ToString () != string.Empty &&
                Convert.ToInt32 (drOrder["transaction_status_id"]) == (int) Constants.eORDER_STATUS.CHECKOUT &&
                Convert.ToInt32 (drOrder["user_id"]) == userId)
            {
                orderId = Convert.ToInt32 (drOrder["order_id"]);
            }
            else
            {
                orderId = StoreUtility.CreateOrder (userId, Common.GetVisitorIPAddress(), (int) Constants.eORDER_STATUS.CHECKOUT, amount);
            } 

            // Set the purchase type
            StoreUtility.SetPurchaseType (orderId, Constants.ePURCHASE_TYPE.NAME_CHANGE);

            return orderId;
        }

        private int CompletePendingOrder (int userId, int orderId)
        {
            try
            {
                // Mark it as completed, Add the subscriptions, Deduct points
                return StoreUtility.PurchaseOrderNow (userId, orderId, Constants.CASH_TT_NAME_CHANGE);
            }
            catch (Exception ex)
            {
                m_logger.Error ("Error in CompletePendingOrder", ex);
            }

            return -1;
        }

        #region Delerations

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

        #endregion
    }
}
