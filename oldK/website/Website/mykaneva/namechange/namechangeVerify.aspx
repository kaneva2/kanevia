<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="namechangeVerify.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.namechangeVerify" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>

<script src="../../jscript/prototype.js" type="text/javascript" language="javascript"></script>
<link href="../../css/new.css" type="text/css" rel="stylesheet">

<style>

#namePic {
position: absolute;
top: -61px;
}

#nameName {
position: relative;
top: -40px;
left: 0;
z-index: 4;
font-size: 16px;
color: #ddf3fe;
width: 100%;
display: block;
text-align: center;
font-weight: normal;
}

hr {
	padding: 0;
	margin: 10px 0 10px 0;
}

p {
	padding: 0;
	margin: 10px 0 0 0;
}
</style>


<ajax:ajaxpanel id="ajpCheckUsername" runat="server">		

<asp:requiredfieldvalidator id="rfPassword" runat="server" controltovalidate="txtPassword" text="*" errormessage="Please enter your password"
	display="none"></asp:requiredfieldvalidator>

<table border="0" cellspacing="0" cellpadding="0" class="newcontainer" align="center">
	<tr>
		<td>
			<table  border="0" cellpadding="0" cellspacing="0" width="100%" class="newcontainerborder">
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
				<tr>
					<td class=""><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
					<td valign="top" class="newdatacontainer">
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="968" align="left"  valign="top">
									
									<table cellpadding="0" cellspacing="0" border="0"  width="100%">
										<tr>
											<td width="968" valign="top">

												<div id="wizard">
												<!-- WIDTH OF ALL SPANS SHOULD ADD UP TO 958 PX -->
													<ul id="wizardProgress">
														<li id="startcap"></li>
														<li id="ucWizardNavBar_liOne" class="completed"><span id="one" style="width: 310px;"><a id="ucWizardNavBar_aOne" class="nohover" style="cursor:default;"><h3>Nickname Change Overview</h3></span></a></li>
														<li id="ucWizardNavBar_liTwo" class="completed"><span id="two" style="width: 225px;"><a id="ucWizardNavBar_aTwo" class="nohover" style="cursor:default;"><h3>Change Nickname</h3></span></a></li>
														<li id="ucWizardNavBar_liThree" class="active"><span id="three" style="width: 272px;"><a id="ucWizardNavBar_aThree" class="nohover" style="cursor:default;"><h3>Confirm New Nickname</h3></a></span></li>
														<li id="ucWizardNavBar_liFour" class="incomplete"><span id="four" class="last" style="width: 151px;"><a id="ucWizardNavBar_aFour" class="nohover" style="cursor:default;"><h3>Finished</h3></a></span></li>
													</ul>			
												</div>
												
											</td>
										</tr>
										<tr>
											<td width="980" valign="top">
											
												<table border="0" cellspacing="0" cellpadding="0" width="100%">
													<tr>
														<!-- START WIZARD CONTENT -->
																												
														<td valign="top" align="center">
															
															<table border="0" width="880" cellspacing="0" cellpadding="0">
																<tr>
																	<td >
																		<h1 style="padding-bottom:10px;text-align:left;">Nickname Change <span class="large">&#187; Sure about this?</span></h1>
																		
																		<table border="0" width="900" cellspacing="0" cellpadding="0" height="290">
																			<tr>
																				<td align="center" valign="top">
																					<table id="Table11" width="900" cellspacing="0" cellpadding="0" border="0">
																						<tr>
																							<td align="left" valign="top" width="500">
																							
																									<p class="large">You are about to permanently change your Nickname from:</p>
																									
																									<p class="center"><span id="spnCurrentName" runat="server" class="large bold">(Old nickname)</span> to  <span id="spnChangeTo" runat="server" class="large bold">(New nickname)</span></p>
																									
																									<p>You will be charged <span id="spnCost" runat="server" class="large bold"></span>. Are you OK with this?</p>
																									
																									<hr>
																									
																									<div style="width:100%;margin-bottom:10px;">
																										<asp:validationsummary cssclass="errBox black" id="valSum" runat="server" showmessagebox="False" showsummary="True"
																											displaymode="BulletList" headertext="Please correct the following errors:" forecolor="black"></asp:validationsummary>
																										<asp:customvalidator id="cvBlank" runat="server" display="None" enableclientscript="False"></asp:customvalidator>
																									</div>		

																									<table id="Table1" cellspacing="0" cellpadding="0" width="500"  border="0" class="wizform">
																										<tr>
																											<td width="245><span id="spnFirstName" class="large">Password:</span><br>
																												<asp:textbox class="biginput" id="txtPassword" runat="server" maxlength="20" textmode="Password" tabindex="13" width="220"></asp:textbox></td>																								
																											<td valign="bottom" style="padding-bottom: 3px;"></td>
																										</tr>
																									</table>
																									<span class="note" style="display: block; width:220px;">For your security, please re-enter the password associated with your Kaneva account.</span>
																									
																									<hr>
																									
																									<table border="0" cellspacing="0" cellpadding="0" width="100%">
																										<tr>
																	
																											<td><a id="aBack" href="~/mykaneva/namechange/namechangeSelect.aspx" runat="server">(&#171;) No, take me back a step</a></td>
																											<td>
																												<asp:imagebutton id="btnChange" runat="server" onclick="btnChange_Click" imageurl="~/images/wizard_btn_changename.gif" text="Change My Name Now" style="height:41px;width:323px;border-width:0px;"/>
																											</td>
																										</tr>
																									</table>
																					
																									<table border="0" cellspacing="0" cellpadding="0" width="97%">
																										<tr>
																											<td width="50%" class="left">
																												
																												<div style="display:block; color: #ba7804; font-size: 14px; text-align: left; padding: 10px;">
																													
																												</div>
																											</td>
																											
																										</tr>
																									</table>
                                                                                                     
																							</td>
																							<td valign="top" width="400" align="left">   
																								<div  style="position: relative;">
																									<span id="nameName" runat="server">payLess</span>
																									<img class="png" id="namePic" runat="server" src="~/images/nickname3.png" alt="Change My Nickname" width="400" height="301" border="0">
																								</div>
																							</td>
																						
																						</tr>
																						
																					</table>
																				</td>
																			</tr>
																		</table>
																		
																	</td>
																</tr>
															</table>
														</td>	
														
														<!-- END WIZARD CONTENT -->
													</tr>
												</table>
											
											</td>
											
										</tr>
										
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td class=""><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
				</tr>
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
				
			</table>
		</td>
	</tr>
</table>


</ajax:ajaxpanel>
