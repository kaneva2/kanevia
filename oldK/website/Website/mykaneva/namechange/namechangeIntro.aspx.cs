///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Kaneva
{
    public partial class namechangeIntro : MainTemplatePage
    {
        protected namechangeIntro () 
		{
			Title = "Kaneva Name Change";
		}

        protected void Page_Load (object sender, EventArgs e)
        {
            // They must be logged in
            if (!Request.IsAuthenticated)
            {
                Response.Redirect (GetLoginURL ());
                return;
            }

            // Check to make sure there is a value for name change cost.  If not, then don't let
            // user continue changing their name.  This is just to make sure all config files have 
            // the NameChangeCost value and it was not forgotten by mistake.
            if (System.Configuration.ConfigurationManager.AppSettings["NameChangeCost"] == null)
            {
                Response.Redirect ("~/mykaneva/general.aspx");
            }

            if (!IsPostBack)
            {
                // Check to see if they have enough credits to purchase name change
                if (UsersUtility.GetUserPointTotal (KanevaWebGlobals.CurrentUser.UserId) <
                    Convert.ToDouble (System.Configuration.ConfigurationManager.AppSettings["NameChangeCost"]))
                {
                    // add code to not allow user beyond this point and display message
                    string scriptString;
                    scriptString = "<script language=\"javaScript\">\n<!--\n function needCredits () {\n";
                    scriptString += "	$('divNeedCredits').style.display = 'block';\n";
                    scriptString += "   return false;";
                    scriptString += "}\n// -->\n";
                    scriptString += "</script>";

                    if (!ClientScript.IsClientScriptBlockRegistered (GetType (), "needCredits"))
                    {
                        ClientScript.RegisterClientScriptBlock (GetType (), "needCredits", scriptString);
                    }

                    btnContinue.OnClientClick = "return needCredits();";
                }
                else
                {
                    btnContinue.OnClientClick = "javascript:document.location.href='namechangeSelect.aspx';return false;";
                }

                this.SetFocus (btnContinue);

                spnNickname.InnerText = KanevaWebGlobals.CurrentUser.Username;
                nameName.InnerText = KanevaWebGlobals.CurrentUser.Username;

                spnCost.InnerText = System.Configuration.ConfigurationManager.AppSettings["NameChangeCost"].ToString () + " credits";
            }

            // Set nav bar
            HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
            HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.ACCOUNT;
            HeaderNav.MyAccountNav.ActiveTab = NavAccount.TAB.GENERAL;
            HeaderNav.SetNavVisible (HeaderNav.MyKanevaNav, 2);
            HeaderNav.SetNavVisible (HeaderNav.MyAccountNav);
        }
    }
}
