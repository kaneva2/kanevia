<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="namechangeComplete.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.namechangeComplete" %>

<link href="../../css/new.css" type="text/css" rel="stylesheet">

<style>

#namePic {
position: absolute;
top: -61px;
}

#nameName {
position: relative;
top: -40px;
left: 0;
z-index: 4;
font-size: 16px;
color: #ddf3fe;
width: 100%;
display: block;
text-align: center;
font-weight: normal;
}

hr {
	padding: 0;
	margin: 10px 0 10px 0;
}

p {
	padding: 0;
	margin: 10px 0 0 0;
}
</style>
	   

<table border="0" cellspacing="0" cellpadding="0" class="newcontainer" align="center">
	<tr>
		<td>
			<table  border="0" cellpadding="0" cellspacing="0" width="100%" class="newcontainerborder">
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
				<tr>
					<td class=""><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
					<td valign="top" class="newdatacontainer">
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="968" align="left"  valign="top">
									
									<table cellpadding="0" cellspacing="0" border="0"  width="100%">
										<tr>
											<td width="968" valign="top">

												<div id="wizard">
												<!-- WIDTH OF ALL SPANS SHOULD ADD UP TO 958 PX -->
													<ul id="wizardProgress">
														<li id="startcap"></li>
														<li id="ucWizardNavBar_liOne" class="completed"><span id="one" style="width: 310px;"><a id="ucWizardNavBar_aOne" class="nohover" style="cursor:default;"><h3>Nickname Change Overview</h3></span></a></li>
														<li id="ucWizardNavBar_liTwo" class="completed"><span id="two" style="width: 225px;"><a id="ucWizardNavBar_aTwo" class="nohover" style="cursor:default;"><h3>Change Nickname</h3></span></a></li>
														<li id="ucWizardNavBar_liThree" class="completed"><span id="three" style="width: 272px;"><a id="ucWizardNavBar_aThree" class="nohover" style="cursor:default;"><h3>Confirm New Nickname</h3></a></span></li>
														<li id="ucWizardNavBar_liFour" class="active"><span id="four" class="last" style="width: 151px;"><a id="ucWizardNavBar_aFour" class="nohover" style="cursor:default;"><h3>Finished</h3></a></span></li>
													</ul>			
												</div>
												
											</td>
										</tr>
										<tr>
											<td width="980" valign="top">
											
												<table border="0" cellspacing="0" cellpadding="0" width="100%">
													<tr>
														<!-- START WIZARD CONTENT -->
																												
														<td valign="top" align="center">
															
															<table border="0" width="880" cellspacing="0" cellpadding="0">
																<tr>
																	<td >
																		<h1 style="padding-bottom:10px;text-align:left;">Nickname Change <span class="large">&#187; Done!</span></h1>
																		
																		<table border="0" width="900" cellspacing="0" cellpadding="0" height="290">
																			<tr>
																				<td align="center" valign="top">
																					<table id="Table11" width="900" cellspacing="0" cellpadding="0" border="0">
																						<tr>
																							<td align="left" valign="top" width="500">
																							
																								<p class="large">Your Nickname is now:  <span id="spnUsername" runat="server" class="large bold">(New nickname)</span></p>
																								
																								<p>You'll receive a confirmation email at the address <span id="spnEmail" runat="server" class="large bold">(address)</span>.</p>
																								
																								<hr>
																					
																								<a id="aPofileUrl" runat="server" href="#">Take me to my Profile</a>
																									
																							</td>
																							<td valign="top" width="400" align="left">   
																								<div  style="position: relative;">
																									<span id="nameName" runat="server">payLess</span>
																									<img class="png" id="namePic" runat="server" src="~/images/nickname4.png" alt="Change My Nickname" width="400" height="301" border="0">
																								</div>
																							</td>
																						
																						</tr>
																						
																					</table>
																					<br />
																				</td>
																			</tr>
																		</table>
																		
																	</td>
																</tr>
															</table>
														</td>	
														
														<!-- END WIZARD CONTENT -->
													</tr>
												</table>
											
											</td>
											
										</tr>
										
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td class=""><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
				</tr>
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
				
			</table>
		</td>
	</tr>
</table>

