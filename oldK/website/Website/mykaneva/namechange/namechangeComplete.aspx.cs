///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Kaneva
{
    public partial class namechangeComplete : MainTemplatePage
    {
        protected namechangeComplete ()
        {
            Title = "Kaneva Name Change";
        }

        protected void Page_Load (object sender, EventArgs e)
        {
            // They must be logged in
            if (!Request.IsAuthenticated)
            {
                Response.Redirect (GetLoginURL ());
                return;
            }

            spnUsername.InnerText = KanevaWebGlobals.CurrentUser.Username;
            nameName.InnerText = KanevaWebGlobals.CurrentUser.Username;
            spnEmail.InnerText = KanevaWebGlobals.CurrentUser.Email;
            aPofileUrl.HRef = GetPersonalChannelUrl (KanevaWebGlobals.CurrentUser.NameNoSpaces);
        }
    }
}
