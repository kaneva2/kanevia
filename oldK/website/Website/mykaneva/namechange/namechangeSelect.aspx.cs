///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Kaneva
{
    public partial class namechangeSelect : MainTemplatePage
    {
        protected namechangeSelect () 
		{
			Title = "Kaneva Name Change";
		}

        protected void Page_Load (object sender, EventArgs e)
        {
            // They must be logged in
            if (!Request.IsAuthenticated)
            {
                Response.Redirect (GetLoginURL ());
                return;
            }

            // Check to make sure there is a value for name change cost.  If not, then don't let
            // user continue changing their name.  This is just to make sure all config files have 
            // the NameChangeCost value and it was not forgotten by mistake.
            if (System.Configuration.ConfigurationManager.AppSettings["NameChangeCost"] == null)
            {
                Response.Redirect ("~/mykaneva/general.aspx");
            }

            // Display current username
            spnCurrentName.InnerText = KanevaWebGlobals.CurrentUser.Username;
            
            // Set up regular expression validators
            revUsername.ValidationExpression = Constants.VALIDATION_REGEX_USERNAME;

            // Set nav bar
            HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
            HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.ACCOUNT;
            HeaderNav.MyAccountNav.ActiveTab = NavAccount.TAB.GENERAL;
            HeaderNav.SetNavVisible (HeaderNav.MyKanevaNav, 2);
            HeaderNav.SetNavVisible (HeaderNav.MyAccountNav);

           
            // Add onclick event to username text field
            txtUserName.Attributes.Add ("onkeyup", "showName();if(checkForEnterKey(event))" + this.ClientScript.GetPostBackEventReference (btnContinue, "") + ";");

            if (!IsPostBack)
            {
                // this case is for return link from the Verify page
                if (Request.QueryString["u"] != null && Request.QueryString["u"].ToString () != string.Empty)
                {
                    txtUserName.Text = Request.QueryString["u"].ToString ();
                    nameName.InnerText = txtUserName.Text;
                }
            }

            if (txtUserName.Text.Trim ().Length > 0)
            {
                nameName.InnerText = txtUserName.Text.Trim ();
            }

            this.SetFocus (txtUserName);
        }

        protected void btnCheckUsername_Click (object sender, ImageClickEventArgs e)
        {
            revUsername.Validate ();

            if (revUsername.IsValid)
            {
                if (txtUserName.Text.Trim () != string.Empty)
                {
                    if (KanevaWebGlobals.isTextRestricted(txtUserName.Text.Trim (), Constants.eRESTRICTION_TYPE.ALL))
                    {
                        spnCheckUsername.InnerText = "Invalid";
                        spnCheckUsername.Attributes.Add ("class", "failure");
                    }
                    else if (UsersUtility.GetUserIdFromUsername (txtUserName.Text.Trim ()) > 0)
                    {
                        spnCheckUsername.InnerText = "Not available";
                        spnCheckUsername.Attributes.Add ("class", "failure");
                    }
                    else
                    {
                        spnCheckUsername.InnerText = "Available";
                        spnCheckUsername.Attributes.Add ("class", "success");
                    }
                }

                nameName.InnerHtml = txtUserName.Text;
            }
            else
            {
                spnCheckUsername.InnerText = "Invalid name";
                spnCheckUsername.Attributes.Add ("class", "failure");
            }
        }

        protected void btnContinue_Click (object sender, EventArgs e)
        {
            Page.Validate ();
            if (!Page.IsValid)
            {
                return;
            }

            string strUserName = txtUserName.Text.Trim ();

            if (UsersUtility.GetUserIdFromUsername (strUserName) > 0 || UsersUtility.IsReservedName (strUserName))
            {
                cvBlank.IsValid = false;
                cvBlank.ErrorMessage = "The Nickname already exists or is not allowed. Please change your Nickname.";
                valSum.Style.Add ("display", "block");
                return;
            }

            Response.Redirect ("namechangeVerify.aspx?u=" + Server.UrlEncode(strUserName));  
        }
    }
}
