<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="namechangeIntro.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.namechangeIntro" %>

<script src="../../jscript/prototype.js" type="text/javascript" language="javascript"></script>
<link href="../../css/new.css" type="text/css" rel="stylesheet">
<!--[if IE 6]>
<link rel="stylesheet" href="css/new_IE.css" type="text/css" />
<![endif]-->
					 
<style>

#namePic {
position: absolute;
top: -61px;
}

#nameName {
position: relative;
top: -40px;
left: 0;
z-index: 4;
font-size: 16px;
color: #ddf3fe;
width: 100%;
display: block;
text-align: center;
font-weight: normal;
}

hr {
	padding: 0;
	margin: 10px 0 10px 0;
}

p {
	padding: 0;
	margin: 10px 0 0 0;
}
</style>


<table border="0" cellspacing="0" cellpadding="0" class="newcontainer" align="center">
	<tr>
		<td>
			<table  border="0" cellpadding="0" cellspacing="0" width="100%" class="newcontainerborder">
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
				<tr>
					<td class=""><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
					<td valign="top" class="newdatacontainer">
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="968" align="left"  valign="top">
									
									<table cellpadding="0" cellspacing="0" border="0"  width="100%">
										<tr>
											<td width="968" valign="top">

												<div id="wizard">
												<!-- WIDTH OF ALL SPANS SHOULD ADD UP TO 958 PX -->
													<ul id="wizardProgress">
														<li id="startcap"></li>
														<li id="ucWizardNavBar_liOne" class="active"><span id="one" style="width: 310px;"><a id="ucWizardNavBar_aOne" class="nohover" style="cursor:default;"><h3>Nickname Change Overview</h3></span></a></li>
														<li id="ucWizardNavBar_liTwo" class="incomplete"><span id="two" style="width: 225px;"><a id="ucWizardNavBar_aTwo" class="nohover" style="cursor:default;"><h3>Change Nickname</h3></span></a></li>
														<li id="ucWizardNavBar_liThree" class="incomplete"><span id="three" style="width: 272px;"><a id="ucWizardNavBar_aThree" class="nohover" style="cursor:default;"><h3>Confirm New Nickname</h3></a></span></li>
														<li id="ucWizardNavBar_liFour" class="incomplete"><span id="four" class="last" style="width: 151px;"><a id="ucWizardNavBar_aFour" class="nohover" style="cursor:default;"><h3>Finished</h3></a></span></li>
													</ul>			
												</div>
												
											</td>
										</tr>
										<tr>
											<td width="980" valign="top">
											
												<table border="0" cellspacing="0" cellpadding="0" width="100%">
													<tr>
														<!-- START WIZARD CONTENT -->
																												
														<td valign="top" align="center">
															
															<table border="0" width="880" cellspacing="0" cellpadding="0" style="position:relative;">
																<tr>
																	<td>
																		
																		<div style="position:relative;">
																		
																			<div id='divRestrictions' style='display:none;position:absolute;left:-40px;top:-20px;margin:0;padding:15px;background-color:transparent;text-align:justify;font-size:12px;width:auto;z-index:6;'>
																				<div style='width:520px;height:320px;display:block; border:3px solid #f7ce52; padding:10px; background-color:#ffff99;'>
																					<div style="text-align: right;"><a href="javascript:void(0);" onmouseover="" onfocus="this.blur();" onclick="$('divRestrictions').style.display='none';" >Close</a></div>
																					<div><h1>Nickname Restrictions</h1></div>
			
																					<div style="margin:14px 0 20px 0;">It's your responsibility to make sure the Nickname you choose follows these guidelines - our system will not automatically keep you from choosing a Nickname that is in violation.</div>
																						
																					<div><ul>
																						<li>Nicknames may not contain profanity, racial slurs, sexual connotations, names of famous people or of Kaneva staff.</li>
																						<li>Nicknames may not advertise or include the name of a product or company.</li>
																						<li>Nicknames may not violate our <a runat="server" href="~/overview/guidelines.aspx">Policies and Guidelines</a>.</li>
																						<li>No refunds or reversals will be permitted once your request is submitted.</li>
																						<li>Items you've uploaded will still be tagged with the Nickname you had at the time.</li>
																					</ul></div>
																						
																					<div>If your chosen nickname is found to not meet our guidelines, it will be immediately changed by our staff (as will your password), and <span class="bold">you will receive an email</span> at the address we have on file.</div>
																						
																				</div>
																			</div>
																				
																			<div id="divNeedCredits" style="display:none;position:absolute;left:-40px;top:-20px;margin:0;padding:15px;background-color:transparent;text-align:justify;font-size:12px;width:auto;z-index:6;">
																				<div style="width:520px;height:320px;display:block; border:3px solid #f7ce52; padding:10px; background-color:#ffff99;">
		
																					<div style="text-align: right;"><a href="javascript:void(0);" onmouseover="" onfocus="this.blur();" onclick="$('divNeedCredits').style.display='none';" >Close</a></div>
																					<br />
																					<div><h1>Not Enough Credits</h1></div>
																				
																					<div style="margin:14px 0 20px 0;">If you'd like to change your Nickname you'll need to purchase more Credits.</div>
																					<br />
																					<div><a runat="server" href="~/mykaneva/manageCredits.aspx" class="large">Buy Credits Now</a></div>
																				
																				</div>	
																			</div>
																		</div>

																		<h1 style="padding-bottom:10px;text-align:left;">Nickname Change <span class="large">&#187; Changing Your Nickname</span></h1>
																		
																		<table border="0" width="900" cellspacing="0" cellpadding="0" height="290">
																			<tr>
																				<td align="center" valign="top">
																					<table id="Table11" width="900" cellspacing="0" cellpadding="0" border="0">
																						<tr>
																							<td align="left" valign="top" width="500">
                                                                                               
																								<p class="large">Your Nickname <span class="large bold" id="spnNickname" runat="server">(nickname)</span> is shown above your Avatar in-world, and is connected to everything you do at Kaneva, so changing it is no small feat. But for the low, low price of <span id="spnCost" runat="server" class="large bold"></span> we're happy to oblige.</p>
																								
																								
																								<h2>Here's how it works:</h2>
																								<ol class="large">
																									<li>Read about <a runat="server" href="javascript:void(0);" onclick="javascript:$('divRestrictions').style.display='block';" class="large">Nickname Restrictions</a> to avoid the heartache of forfeiting your name-change fee.<br /><br /></li>
																									<li>Sign out of the World of Kaneva 3D program.<br /><br /></li>
																									<li>Click <span class="bold">Continue</span> to begin.</li>
																								</ol>
																							
																								
																								<p>You'll still have your Credits and Rewards, your Friends, your Profile, your Home and Inventory. But you'll have a brand new Nickname!</p>
																								<p class="note">Note: Any comments you've made or items you've uploaded on Kaneva.com, will still show your old Nickname, but will link to your Profile (which will show your new Nickname).</p>

																								
																								
																								<hr>
																								
																								<table border="0" cellspacing="0" cellpadding="0" width="100%">
																									<tr>							 
																										<td align="right">
																											
																											<asp:imagebutton id="btnContinue" runat="server" text="Continue" borderstyle="None" imageurl="~/images/wizard_btn_continue.gif" alternatetext="Continue" style="height:41px;width:135px;border-width:0px;"/>

																										</td>						 
																									</tr>
																								</table>
                                                                                                     
																							</td>
																							<td valign="top" width="400" align="left">   
																								<div  style="position: relative;">
																									<span id="nameName" runat="server">paynomind</span>
																									<img class="png" src="~/images/nickname1.png" runat="server" alt="Change My Nickname" width="400" height="301" border="0" id="namePic">
																								</div>
																							</td>
																						
																						</tr>
																						
																					</table>
																					<br />
																				</td>
																			</tr>
																		</table>
																		
																		
																	</td>
																</tr>
															</table>
														</td>	
														
														<!-- END WIZARD CONTENT -->
													</tr>
												</table>
											
											</td>
											
										</tr>
										
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td class=""><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
				</tr>
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
				
			</table>
		</td>
	</tr>
</table>

<br /><br />