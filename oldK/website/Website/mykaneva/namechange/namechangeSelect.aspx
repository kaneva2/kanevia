<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="namechangeSelect.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.namechangeSelect" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>

<script src="../../jscript/prototype.js" type="text/javascript" language="javascript"></script>
<link href="../../css/new.css" type="text/css" rel="stylesheet">

<style>

#namePic {
position: absolute;
top: -61px;
}

#nameName {
position: relative;
top: -40px;
left: 0;
z-index: 4;
font-size: 16px;
color: #ddf3fe;
width: 100%;
display: block;
text-align: center;
font-weight: normal;
}

hr {
	padding: 0;
	margin: 10px 0 10px 0;
}

p {
	padding: 0;
	margin: 10px 0 0 0;
}
</style>

<script language="javascript">
function showName()
{
	$('nameName').innerHTML = $('txtUserName').value;
}
</script>	   
	   
	
<ajax:ajaxpanel id="ajpCheckUsername" runat="server">		
		
<asp:requiredfieldvalidator id="rfUsername" runat="server" controltovalidate="txtUserName" text="*" 
	errormessage="You must enter a nickname." display="none"></asp:requiredfieldvalidator>
<asp:regularexpressionvalidator id="revUsername" runat="server" controltovalidate="txtUserName" text="" display="none" 
	errormessage="Nicknames should be 4-20 characters and a combination of upper and lower-case letters, numbers, and underscores (_) only. Do not use spaces."></asp:regularexpressionvalidator>


<table border="0" cellspacing="0" cellpadding="0" class="newcontainer" align="center">
	<tr>
		<td>
			<table  border="0" cellpadding="0" cellspacing="0" width="100%" class="newcontainerborder">
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
				<tr>
					<td class=""><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
					<td valign="top" class="newdatacontainer">
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="968" align="left"  valign="top">
									
									<table cellpadding="0" cellspacing="0" border="0"  width="100%">
										<tr>
											<td width="968" valign="top">

												<div id="wizard">
												<!-- WIDTH OF ALL SPANS SHOULD ADD UP TO 958 PX -->
													<ul id="wizardProgress">
														<li id="startcap"></li>
														<li id="ucWizardNavBar_liOne" class="completed"><span id="one" style="width: 310px;"><a id="ucWizardNavBar_aOne" class="nohover" style="cursor:default;"><h3>Nickname Change Overview</h3></span></a></li>
														<li id="ucWizardNavBar_liTwo" class="active"><span id="two" style="width: 225px;"><a id="ucWizardNavBar_aTwo" class="nohover" style="cursor:default;"><h3>Change Nickname</h3></span></a></li>
														<li id="ucWizardNavBar_liThree" class="incomplete"><span id="three" style="width: 272px;"><a id="ucWizardNavBar_aThree" class="nohover" style="cursor:default;"><h3>Confirm New Nickname</h3></a></span></li>
														<li id="ucWizardNavBar_liFour" class="incomplete"><span id="four" class="last" style="width: 151px;"><a id="ucWizardNavBar_aFour" class="nohover" style="cursor:default;"><h3>Finished</h3></a></span></li>
													</ul>			
												</div>
												
											</td>
										</tr>
										<tr>
											<td width="980" valign="top" style="position: relative;">
											
												<table border="0" cellspacing="0" cellpadding="0" width="100%">
													<tr>
														<!-- START WIZARD CONTENT -->
														
														<td valign="top" align="center">
															
															<table border="0" width="880" cellspacing="0" cellpadding="0">
																<tr>
																	<td >
																		<h1 style="padding-bottom:10px;text-align:left;">Nickname Change <span class="large">&#187; Choose your new Nickname</span></h1>
																	
																		<table border="0" width="900" cellspacing="0" cellpadding="0" height="290">
																			<tr>
																				<td align="center" valign="top">
																					<table id="Table11" width="900" cellspacing="0" cellpadding="0" border="0">
																						<tr>
																							<td align="left" valign="top" width="500">
																							
																								<p class="large">Current Nickname: <span id="spnCurrentName" runat="server" class="bold">(nickname)</span></p>
																																   
																								<hr>
																								
																								<div style="width:100%;margin-bottom:10px;">
																									<asp:validationsummary cssclass="errBox black" id="valSum" runat="server" showmessagebox="False" showsummary="True"
																										displaymode="BulletList" headertext="Please correct the following errors:" forecolor="black"></asp:validationsummary>
																									<asp:customvalidator id="cvBlank" runat="server" display="None" enableclientscript="False"></asp:customvalidator>
																								</div>		

																								<table id="Table1" cellspacing="0" cellpadding="0" width="400"  border="0" class="wizform">
																									<tr>
																										<td><span class="large">New Nickname:</span><br>
																										<asp:textbox class="biginput" id="txtUserName" runat="server" maxlength="20" tabindex="1" style="width:250px;"></asp:textbox>
																										<td valign="bottom" style="padding-bottom: 3px;">
																											<asp:imagebutton id="btnCheckUsername" onclick="btnCheckUsername_Click" tabindex="2" runat="server"
																												width="124"
																												imageurl="~/images/wizard_btn_checkavail.gif"
																												alternatetext="Check Availability"
																												height="21"
																												border="0"
																												imagealign="AbsMiddle"
																												causesvalidation="False"></asp:imagebutton>
																										</td>
																									</tr>
																								</table>
																								
																								<span class="note" style="display: block; width:250px;">Note: Names must be 4-20 characters, and can contain upper or lower-case letters, numbers, and underscores (_) only. No spaces.</span>
																								<span id="spnCheckUsername" runat="server" style="float:right;position:relative;top:-56px;left:-16px;text-align:left;width:80px;"></span>
																								<hr>
																								
																								<table border="0" cellspacing="0" cellpadding="0" width="100%">
																									<tr>
																										<td align="right">
																										
																											<asp:imagebutton runat="server" id="btnContinue" onclick="btnContinue_Click" imageurl="~/images/wizard_btn_continue.gif" alternatetext="Continue" causesvalidation="False" tabindex="3" style="height:41px;width:135px;border-width:0px;"/>

																										</td>
																									</tr>
																								</table>
                                                                                                     
																							</td>
																							<td valign="top" width="400" align="left">   
																								<div  style="position: relative;">
																									<span id="nameName" runat="server">???????</span>
																									<img class="png" id="namePic" runat="server" src="~/images/nickname2.png" alt="Change My Nickname" width="400" height="301" border="0">
																								</div>
																							</td>
																						</tr>
																						
																					</table>
																				</td>
																			</tr>
																		</table>
																		
																	</td>
																</tr>
															</table>
														</td>	
														
														<!-- END WIZARD CONTENT -->
													</tr>
												</table>
											
											</td>
											
										</tr>
										
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td class=""><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
				</tr>
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
				
			</table>
		</td>
	</tr>
</table>
</ajax:ajaxpanel>
