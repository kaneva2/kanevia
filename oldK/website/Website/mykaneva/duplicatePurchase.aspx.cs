///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Kaneva
{
    public partial class duplicatePurchase : MainTemplatePage
    {
        protected duplicatePurchase() 
		{
			Title = "Whoops! You can't purchase this item again because...";
		}
       
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        
        protected void btnOK_Click(object sender, EventArgs e)
        {
            if (Request.IsAuthenticated)
            {
                Response.Redirect("~/default.aspx");
            }
            else
            {
                Response.Redirect(GetLoginURL());
            }
        }
    }
}
