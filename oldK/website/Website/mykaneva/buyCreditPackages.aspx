<%@ Page Language="C#" MasterPageFile="~/masterpages/IndexPageTemplate.Master" AutoEventWireup="false" CodeBehind="buyCreditPackages.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.buyCreditPackages" %>
<%@ Register TagPrefix="Kaneva" TagName="WizardNavBar" Src="../usercontrols/checkoutwizardnavbar.ascx" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>

<asp:Content ID="cnt_buySpecials" runat="server" contentplaceholderid="cph_Body" >

<style>
.on1, .on1_
{
	font-family: verdana, arial, sans-serif;
	color: #026b84;
	padding: 0px;
	margin: 0;
	background-color: #caedfd;
}
</style>

    <div class="wrapper PageContainer">
        <div class="manageCredits">
            <ul>
                <li><span style="color: #13667a; font-size: 10pt; font-weight:bold; padding-left:20px;">Manage Your Account</span></li>
                <li id="Li3" class="">
                    <a href="~/mykaneva/billingInfoSummary.aspx" runat="server" id="a4" class="nohover">Manage Billing Settings</a>
                </li>
                <li id="Li1" class="">
                    <a href="~/mykaneva/transactions.aspx?CashP=Y" runat="server" id="a2" class="nohover">View Purchase History</a>
                </li>
                <li id="Li2" class="">
                    <a href="~/mykaneva/creditfaq.aspx" runat="server" id="a3" class="nohover">Credit FAQs</a>
                </li>
            </ul>
        </div>
        <!--<div class="progressBar">
            <ul class="">
                <li id="" class="">
                    <h3>Select Package</h3>
                </li>
                <li id="Li1" class="">
                    <h3>Payment</h3>
                </li>
                <li id="Li2" class="">
                    <h3>Receipt</h3>
                </li>
            </ul>
        </div>-->
        <ajax:ajaxpanel id="ajpackagess" runat="server">
        <!-- begin new content -->
        <div style="width: 100%;"><strong><span id="spnMessage"  runat="server" class="alertmessage"></span></strong></div>
        <div class="module">
            <span class="ct"><span class="cl"></span></span>
            <div class="pkgeDiv">
                <p><br /></p>
                <h1>Current Credit Balance: <asp:Label id="lblUsersBalance" runat="server" /></h1>
                <ul class="creditBullets">
                    <li>Credits and Rewards are the currency of The Virtual World of Kaneva.</li>
                    <li>Credits enable you to purchase all sorts of items from a variety of locations within the World, such as accessories for your home or clothing to wear. Rewards can be used for many of the same items, but cannot be used for some purchases that require Credits only.</li>
                    <li style="height:8px;"></li> 
                </ul>
                <div class="arrow"><img src="../images/purchase_flow/arrow.jpg" alt="Step 1"/></div><span style="color: #61B4DE; font-size: 11pt;font-weight:bold; margin-top: 0px">Step 1</span> <span style="color: #13667a; font-size: 9pt; font-weight:bold;">Select your Credit package:</span>
                <p><br /></p>
                
                <div class="pkgeTable_ds">
                    <div class="pkgeTable_top"><!-- Top Cap --></div>
                    <div class="pkgeTable_head">
                        <!-- Table/Repeater to list out the offered promotions -->
                        <asp:repeater id="rptCredits" runat="server">
                            <HeaderTemplate>
                               <table class="pkgeTable">
                                   <tr>
                                      <th class="w1"><!-- column for the radio buttons --></th>
                                      <th class="w4">Credits</th>
                                      <th class="w2">Price</th>

                                      <th class="w3">Bonus Rewards</th>
        
                                    </tr>	
                            </HeaderTemplate>
                            <itemtemplate>
		                            <tr id="drPromtion" runat="server">
			                            <td class="w1">
			                                <asp:RadioButton runat="server" id="rdb_Credit" groupname="creditPackages" autopostback="true" oncheckedchanged="rdb_Credit_CheckedChanged" />
			                                <asp:Label ID="lblCreditPackageID" runat="server" Visible="false" text='<%# DataBinder.Eval(Container.DataItem, "PromotionId")%>' />
			                            </td>
			                            <td class="w4"><asp:Label ID="lblCreditAmount" runat="server" Text='<%# Convert.ToInt32(DataBinder.Eval(Container.DataItem, "KeiPointAmount"))%>' /></td>
			                            <td class="w2"><!--$<span style="text-decoration: line-through;"><asp:Label visible="false" ID="lblOrgPrice" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "OriginalPrice")%>' /></span>-->  $<asp:Label ID="lblPrice" runat="server" Text='<%# CalculateDiscount(Convert.ToString(DataBinder.Eval(Container.DataItem, "DollarAmount"))) %>' /></td>
			                          
			                            <td class="w3"><asp:Label ID="lblSavings" runat="server" /><!--<asp:Label ID="lblMostPopular" CssClass="most_popular" Visible="false" runat="server" Text="Most Popular!" /><div id="divMostPopular" runat="server" visible="false" class="most_popular">Most Popular!</div><div id="divSpecialsBurst" runat="server" visible="false" class="burstHolder"><div class="pkgeTable_burst">Best Buy!</div></div>--></td >
		                            </tr>
                            </itemtemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:repeater>                                                       
                   </div>
                    <div class="pkgeTable_dsBtm"><!-- Bottom Drop Shadow --></div>
                </div>
 
				<div class="clear"><!-- clear the floats --></div>

            </div>
            <div class="adCol">
                <a href="buySpecials.aspx"><img runat="server" src="~/images/purchase_flow/specialDeals_2.jpg" alt="Special offers"/></a>
                <a href="buySpecials.aspx?pass=true" id="aAP" runat="server"><img runat="server" src="~/images/purchase_flow/passesArt.jpg" alt="Passes"/></a>
                <!--
                <p>Limited time offers on exclusive items&#8230;</p>
                -->
                
                <!--<h2><a href="buySpecials.aspx">.</a></h2> -->
                <!--
                <p>&#8230;plus a great deal on Credits!</p>
                -->
            </div>              
            
                               
            <div class="clear"><!-- clear the floats --></div>

            <table style="margin-top:10px;">
				<tr>
					<td style="padding-left:30px;">
						<img src="../images/purchase_flow/arrow.jpg" alt="Step 2" />
					</td>
					<td style="color: #61B4DE;font-size:11pt;font-weight:bold;white-space:nowrap">Step 2</td>
       
					<td style="color:#13667a;font-size:9pt; font-weight:bold;white-space:nowrap;padding-left:4px;">Select your method of payment:</td>
          
					<td>
						<div class="payPalBtn">
							<asp:LinkButton id="btnPurchasePayPal" runat="Server" onclick="btnPurchasePayPal_Click" >Pay Pal</asp:LinkButton>
						</div>
					</td>
					<td>
						<div class="creditCardBtn">
						<asp:LinkButton id="btnPurchaseCredit" runat="Server" onclick="btnPurchaseCredits_Click" >Credit Card</asp:LinkButton>
						</div>
					</td>
				</tr>
            </table>

            <span class="cb"><span class="cl"></span></span> 
        </div>
        </ajax:ajaxpanel>
     </div>

</asp:Content>
