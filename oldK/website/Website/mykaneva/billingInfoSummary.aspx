<%@ Page Language="C#" MasterPageFile="~/masterpages/IndexPageTemplate.Master" AutoEventWireup="false" CodeBehind="billingInfoSummary.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.billingInfoSummary" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>


<asp:Content ID="cnt_billingSummary" runat="server" contentplaceholderid="cph_Body" >
    
<style>
    body {
	font-family: verdana, arial, sans-serif;
	color: #656565;
	}
</style>

    <div class="wrapper  PageContainer" style="min-height:300px;">
        <!--<div class="manageAccountSettings">
            <ul class="">
                <li><span style="color: #13667a; font-size: 10pt; font-weight:bold; padding-left:20px;">Manage Account Settings</span></li>
                <li id="Li3" class="">
                    <a href="~/mykaneva/billing.aspx" runat="server" id="a4" class="nohover">Update Billing Address</a>
                </li>
                <li id="Li1" class="">
                    <a href="~/mykaneva/credithistory.aspx" runat="server" id="a2" class="nohover">View Purchase History</a>
                </li>
                <li id="Li2" class="" style="width: 75px;">
                    <a href="~/mykaneva/creditfaq.aspx" runat="server" id="a3" class="nohover">Credit FAQs</a>
                </li>
                
              
                <li style="width:130px; padding-left: 90px;">
                  <a href="buyCreditPackages.aspx"><img src="../images/purchase_flow/btnCreditBundles.gif" alt="Credit Bundles" style="vertical-align:top" /></a></li>
               
            </ul>
        </div>     --> <br /><br />  
        <ajax:ajaxpanel id="ajpackagess" runat="server">
            <asp:validationsummary cssclass="errBox" id="valSum" runat="server" showmessagebox="False" showsummary="True"
            displaymode="SingleParagraph" style="margin-top:10px;width:48%;" headertext="" forecolor="black"></asp:validationsummary>
            <asp:customvalidator id="cvBlank" runat="server" display="None" enableclientscript="False"></asp:customvalidator>
            <br />					
            <div><strong><span id="spnMessage"  runat="server" class="alertmessage"></span></strong></div>
            <div class="information">
                <div class="module" style="line-height:2px; width:100%; margin-bottom:20px">
                       <span class="ct"><span class="cl"></span></span>			   
		                <table cellpadding="10" cellspacing="0" border="0" style="margin:0px 10px 0px 10px">
			                <tr><th><h1>Billing Address</h1></th></tr>
			                <tr><td><p class="note">Billing address must match billing address of credit card used.</p></td></tr>
			                <tr><td><span id="spnFullName" runat="server"></span></td></tr>
			                <tr><td><span id="spnAddress1" runat="server"></span></td></tr>
			                <tr><td><span id="spnAddress2" runat="server"></span></td></tr>
			                <tr>
				                <td align="left">
					                <span id="spnCity" runat="server"></span>
					                <span id="spnState" runat="server"></span>&nbsp;&nbsp;&nbsp;<span id="spnPostalCode" runat="server"></span>
				                </td>
			                </tr>
			                <tr><td><span id="spnCountry"  runat="server"></span></td></tr>
			                <tr><td><span id="spnPhoneNumber" runat="server"></span></td></tr>
			                <tr><td>&nbsp;</td></tr>
			                <tr><td><a href="~/mykaneva/billing.aspx" runat="server" id="lbUpdateBillingAddress" class="nohover">Update Billing Address</a></td></tr>
		                </table>
			            <span class="cb"><span class="cl"></span></span>
			     </div>
			     <div class="module" style="width:100%; float:left; display:none" visible="false">
	                    <span class="ct"><span class="cl"></span></span>			   
		                <table cellpadding="10" cellspacing="0" border="0" style="margin:0px 10px 0px 10px">
			                <tr>
				                <th><h3>Credit Card Info</h3></th>
			                </tr>
			                <tr>
				                <td><span id="spnCardType" runat="server"></span></td>
			                </tr>
			                <tr>
				                <td><span id="spnCardNumber" runat="server"></span></td>
			                </tr>
			                <tr>
				                <td><span id="spnCardExpiration" runat="server"></span></td>
			                </tr>
		                </table>
		                <br />
                    <span class="cb"><span class="cl"></span></span> 
                </div>
	            <div class="clear"><!-- clear the floats --></div>
            </div>
            <div class="subscriptions">
                <div class="module" style="width:100%">
                    <span class="ct"><span class="cl"></span></span>			   
                    <table cellpadding="10" cellspacing="0" border="0" style="line-height:2px; width:100%">
                        <tr>
	                        <th><h1 style="margin-bottom:10px;">Current Subscriptions</h1></th>
                        </tr>
                        <asp:Repeater id="rptSubscriptions" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td style="padding-left:20px"><b><asp:Label ID="lblSubscriptionName" runat="server" /></b></td>
                                    <td rowspan="4"><asp:LinkButton ID="lbn_SelectSubscForRemoval" runat="server" Text="Cancel my subscription" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UserSubscriptionId").ToString() %>' CommandName="startCancel" OnCommand="Cancel_Command" ></asp:LinkButton></td>
                                </tr>
                                <tr>
                                    <td style="padding-left:20px">$<%# DataBinder.Eval(Container.DataItem, "Price").ToString() %> / <asp:Label ID="lbl_BillingPeriod" runat="server" /></td>
                                </tr>
                                <tr>
                                    <td style="padding-left:20px">Last Payment - <asp:Label ID="lblLastPayment" runat="server" /></td>
                                </tr>
                                <tr>
                                    <td style="padding-left:20px"><asp:label id="lblNextPayment" runat="server" /> - <%# FormatDate(Convert.ToDateTime(DataBinder.Eval(Container.DataItem, "EndDate"))) %></td>
                                </tr>
                                <tr >
                                    <td style="padding-left:20px" colspan="2"><hr style="width:100%;height:1px;"/></td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                        </table>
                    <span class="cb"><span class="cl"></span></span> 
                </div>
                <div id="whyCancel" runat="server" style="padding-left:20px; text-align:left" visible="false" >
                    <h5>Why do you want to cancel your subscription?</h5>
                    <asp:TextBox id="tbx_CancelReason" runat="server" TextMode="MultiLine" Width="94%" Rows="6"></asp:TextBox>
                    <div id="div_reasons2Keep" runat="server">
                    </div>
                    <table border="0" cellpadding="10px">
                        <tr>
                            <td style="padding-left:0px">
                                <div class="btnImSure"><asp:LinkButton ID="lbn_CancelSubscription" runat="server" Text="Yes I'm Sure" CommandName="excuteCancel" OnCommand="Cancel_Command"></asp:LinkButton></div>
                            </td>
                            <td>
                                <div class="btnNotSure"><asp:LinkButton ID="lbn_Cancel" runat="server" Text="No I'm Not" CommandArgument="0" CommandName="cancelCancel" OnCommand="Cancel_Command"></asp:LinkButton></div>
                            </td>
                        </tr>
                    </table>
                  </div>
            </div>
        </ajax:ajaxpanel>        
    </div>

</asp:Content>