<%@ Page language="c#" Codebehind="incomeItemsSold.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.incomeItemsSold" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../usercontrols/Pager.ascx" %>

<link href="../css/home.css" rel="stylesheet" type="text/css" />		
<link href="../css/kanevaSC.css" rel="stylesheet" type="text/css"/>
<link href="../css/friends.css" rel="stylesheet" type="text/css"/>
<link href="../css/kanevaText.css" type="text/css" rel="stylesheet">

<table width="80%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><img runat="server" src="~/images/spacer.gif" width="1" height="14"/></td>
  </tr>
  <tr>    
  </tr>
  <tr>
    <td>
	<table border="0" cellpadding="0" cellspacing="0" width="990">
      <tr>
        <td valign="top">
        <table  border="0" cellpadding="0" cellspacing="0" width="990">
          <tr>
            <td class="frTopLeft"></td>
            <td class="frTop"></td>
            <td class="frTopRight"></td>
          </tr>
          <tr>
            <td  class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1"/></td>
            <td valign="top"  class="frBgIntMembers">
				
                <table  border="0" cellpadding="0" cellspacing="0" width="100%" >
                  <tr>
                    <td class="boxInsideTopLeft"><img runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
                    <td class="boxInsideTop" ><img runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
                    <td  class="boxInsideTopRight"><img runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
                  </tr>
                  <tr class="boxInside">
                    <td class="boxInsideleft" ><img runat="server" src="~/images/spacer.gif" width="1" height="4"/></td>
                    <td align="center" class="boxInsideContent"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
                        <td><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
                        <td><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
                      </tr>
                      <tr>
                        <td><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
                        <td width="100%" class="textGeneralGray11">
							<asp:Label runat="Server" id="lblMonth"/>
                        </td>
                        <td><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
                      </tr>
                      <tr>
                        <td><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
                        <td><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
                        <td><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
                      </tr>
                    </table></td>
                    <td class="boxInsideRight"><img runat="server" src="~/images/spacer.gif" width="1" height="4"/></td>
                  </tr>
                  <tr>
                    <td class="boxInsideBottomLeft" ><img runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
                    <td class="boxInsideBottom2"><img runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
                    <td class="boxInsideBottomRight"><img runat="server" src="~/images/spacer.gif" width="4" height="4" /></td>
                  </tr>
              </table></td>
            <td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1"/></td>
          </tr>
          <tr>
            <td class="frBottomLeft"></td>
            <td class="frBottom"></td>
            <td class="frBottomRight"></td>
          </tr>
          <tr>
            <td><img runat="server" src="~/images/spacer.gif" width="1" height="10"/></td>
          </tr>
        </table></td>
        </tr>
      <tr>
        <td valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
          <tr>
            <td class="intBorderB1TopLeft"></td>
            <td class="intBorderB1Top"></td>
            <td class="intBorderB1TopRight"></td>
          </tr>
          <tr>
            <td class="intBorderB1BorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
            <td valign="top" class="intBorderB1BgInt1"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
              <!--DWLayoutTable-->
                <tr>
                  <td width="100%" height="20" valign="top" align="right">
				  <table width="100%" border="0" cellpadding="0" cellspacing="0" align="right">
                    <!--DWLayoutTable-->
                    <tr>
                      <td height="3" colspan="5" align="right" valign="middle"></td>
                  </tr>
                    <tr>
                      <td colspan="5" align="right" valign="middle"><img runat="server" src="~/images/spacer.gif" width="1" height="5" /></td>
                    </tr>
                    <tr>
                      <td width="544" height="9" align="right" valign="middle"><!--DWLayoutEmptyCell-->&nbsp;</td>
                      <td width="5">&nbsp;</td>
                      <td width="422" align="right" valign="middle"><span class="insideTextNoBold"><asp:Label runat="server" id="lblSearch"/></span><img runat="server" src="~/images/spacer.gif" width="6" height="8" /><Kaneva:Pager runat="server" id="pgTop"/></td>
                    </tr>
                  </table></td>
                </tr>
            </table></td>
            <td class="intBorderB1BorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
          </tr>
          <tr>
            <td class="intBorderB1BottomLeft"></td>
            <td class="intBorderB1Bottom"></td>
            <td class="intBorderB1BottomRight"></td>
          </tr>
        </table>
          <table width="100%"  border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><img runat="server" src="~/images/spacer.gif" width="1" height="10" /></td>
            </tr>
          </table>
          <table  border="0" cellpadding="0" cellspacing="0" width="990">
          <tr>
            <td class="frTopLeft"></td>
            <td class="frTop"></td>
            <td class="frTopRight"></td>
          </tr>
          <tr>
            <td  class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1"/></td>
            <td valign="top"  class="frBgIntMembers"><table  border="0" cellpadding="0" cellspacing="0" width="100%" >
                  <tr>
                    <td class="boxInsideTopLeft"><img runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
                    <td class="boxInsideTop" ><img runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
                    <td  class="boxInsideTopRight"><img runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
                  </tr>
                  <tr class="boxInside">
                    <td class="boxInsideleft" ><img runat="server" src="~/images/spacer.gif" width="1" height="4" /></td>
                    <td align="center" class="boxInsideContent"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="6" height="6"><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
                        <td width="6" height="6"><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
                        <td width="6" height="6"><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
                      </tr>
                      <tr>
                        <td width="6" height="6"><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
                        <td width="100%">
                        <table width="100%"  border="0" align="letf" cellpadding="0" cellspacing="0">
                          <tr>
                            <td width="7" class="intBorderCTopLeft"><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
                            <td colspan="3" class="intBorderCTop"><img runat="server" src="~/images/spacer.gif" width="200" height="7"/></td>
                            <td width="1" class="intBorderCTop"><img runat="server" src="~/images/spacer.gif" width="1" height="7"/></td>
                            <td class="intBorderCTop"><img runat="server" src="~/images/spacer.gif" width="100" height="7"/></td>
                            <td width="7" class="intBorderCTopRight"><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
                          </tr>
                          <tr align="left">
                            <td class="intBorderCBorderLeft"><img runat="server" src="~/images/spacer.gif" width="6" height="2"/></td>
                            <td colspan="3" class="intBorderCBgInt1">
								<table width="80%"  border="0" cellspacing="0" cellpadding="0">
									<tr>
									<td width="15" height="7"><img runat="server" src="~/images/spacer.gif" width="16" height="7"/> </td>
									<td width="97%" ><span class="content">Item</span></td>
									</tr>
								</table>
							</td>
                            <td width="1" class="sepTit1"><img runat="server" src="~/images/spacer.gif" width="1" height="8"/></td>
                            <td width="280" class="intBorderCBgInt1">
								<table width="80%"  border="0" cellspacing="0" cellpadding="0">
									<tr>
									<td width="15" height="7"><img runat="server" src="~/images/spacer.gif" width="15" height="7"/></td>
									<td width="332" ><span class="content">Downloads</span></td>
									</tr>
								</table>
                            </td>
                            <td class="intBorderCBorderRight">&nbsp;</td>
                          </tr>
                          <tr>
                            <td class="intBorderCBottomLeft"><img runat="server" src="~/images/spacer.gif" width="7" height="7"/></td>
                            <td colspan="3" class="intBorderCBottom"><img runat="server" src="~/images/spacer.gif" width="7" height="7"/></td>
                            <td width="1" class="intBorderCBottom"><img runat="server" src="~/images/spacer.gif" width="1" height="7"/></td>
                            <td class="intBorderCBottom"><img runat="server" src="~/images/spacer.gif" width="7" height="7"/></td>
                            <td class="intBorderCBottomRight"><img runat="server" src="~/images/spacer.gif" width="7" height="7"/></td>
                          </tr>
                          
                          <asp:Repeater runat="server" id="rptTransactions" EnableViewState="False">
							<ItemTemplate>
								 <tr align="left" class="intColor1">
									<td class="intBorderC1BorderLeft1"><img runat="server" src="~/images/spacer.gif" width="1" height="60"/></td>
									<td class="intColor1" width="16"><img runat="server" src="~/images/spacer.gif" width="15" height="7"/></td>
									<td width="60" class="intColor1" align="left">
										<table border="0" cellpadding="0" cellspacing="0" width="40" height="40" align="left">
										<tr>
											<td class="boxWhiteTopLeft"><img runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
											<td class="boxWhiteTop" ><img runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
											<td class="boxWhiteTopRight"><img runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
										</tr>
										<tr>
											<td class="boxWhiteLeft" ><img runat="server" src="~/images/spacer.gif" width="1" height="4" /></td>
											<td class="boxWhiteContent" align="center" valign="middle"><a href='<%# GetAssetDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "asset_id")))%>'><img border="0" height="40" width="40" src='<%#GetMediaImageURL (DataBinder.Eval(Container.DataItem, "thumbnail_medium_path").ToString () ,"me", Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_id")), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_type_id")), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "thumbnail_gen")))%>' border="0"/></a></td>
											<td class="boxWhiteRight"><img runat="server" src="~/images/spacer.gif" width="1" height="4"/></td>
										</tr>
										<tr>
											<td class="boxWhiteBottomLeft" ><img runat="server" src="~/images/spacer.gif" width="4" height="4" /></td>
											<td class="boxWhiteBottom"><img runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
											<td class="boxWhiteBottomRight"><img runat="server" src="~/images/spacer.gif" width="4" height="4" /></td>
										</tr>
										</table>
									</td>
									<td width="278"><a title='<%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "name").ToString ())%>' href='<%# GetAssetDetailsLink (Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_id")))%>'><%# TruncateWithEllipsis (Server.HtmlDecode (DataBinder.Eval(Container.DataItem, "name").ToString ()), 45) %></a></td>
									<td width="1" class="sepInt1"><img runat="server" src="~/images/spacer.gif" width="1" height="7"/></td>
									<td class="textGeneralGray11"><img runat="server" src="~/images/spacer.gif" width="16" height="7"/><%# DataBinder.Eval (Container.DataItem, "count") %></td>
									<td class="intBorderC1BorderRight1">&nbsp;</td>
								</tr>
							</ItemTemplate>
							<AlternatingItemTemplate>
								 <tr align="left" class="intColor2">
									<td class="intBorderC1BorderLeft1"><img runat="server" src="~/images/spacer.gif" width="1" height="60"/></td>
									<td class="intColor2" width="16"><img runat="server" src="~/images/spacer.gif" width="15" height="7"/></td>
									<td width="60" class="intColor2" align="left">
										<table border="0" cellpadding="0" cellspacing="0" width="40" height="40" align="left">
										<tr>
											<td class="boxWhiteTopLeft"><img runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
											<td class="boxWhiteTop" ><img runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
											<td class="boxWhiteTopRight"><img runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
										</tr>
										<tr>
											<td class="boxWhiteLeft" ><img runat="server" src="~/images/spacer.gif" width="1" height="4" /></td>
											<td class="boxWhiteContent" align="center" valign="middle"><a href='<%# GetAssetDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "asset_id")))%>'><img border="0" height="40" width="40" src='<%#GetMediaImageURL (DataBinder.Eval(Container.DataItem, "thumbnail_medium_path").ToString () ,"me", Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_id")), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_type_id")), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "thumbnail_gen")))%>' border="0"/></a></td>
											<td class="boxWhiteRight"><img runat="server" src="~/images/spacer.gif" width="1" height="4"/></td>
										</tr>
										<tr>
											<td class="boxWhiteBottomLeft" ><img runat="server" src="~/images/spacer.gif" width="4" height="4" /></td>
											<td class="boxWhiteBottom"><img runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
											<td class="boxWhiteBottomRight"><img runat="server" src="~/images/spacer.gif" width="4" height="4" /></td>
										</tr>
										</table>
									</td>
									<td width="278"><a title='<%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "name").ToString ())%>' href='<%# GetAssetDetailsLink (Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_id")))%>'><%# TruncateWithEllipsis (Server.HtmlDecode (DataBinder.Eval(Container.DataItem, "name").ToString ()), 45) %></a></td>
									<td width="1" class="sepInt1"><img runat="server" src="~/images/spacer.gif" width="1" height="7"/></td>
									<td class="textGeneralGray11"><img runat="server" src="~/images/spacer.gif" width="16" height="7"/><%# DataBinder.Eval (Container.DataItem, "count") %></td>
									<td class="intBorderC1BorderRight1">&nbsp;</td>
								</tr>
							</AlternatingItemTemplate>
                          </asp:Repeater>
           
                          <tr>
                            <td class="intBorderC1BottomLeft"><img runat="server" src="~/images/spacer.gif" width="7" height="7"/></td>
                            <td colspan="3" class="intBorderC1Bottom"><img runat="server" src="~/images/spacer.gif" width="7" height="7"/></td>
                            <td width="1" class="intBorderC1Bottom"><img runat="server" src="~/images/spacer.gif" width="1" height="7"/></td>
                            <td class="intBorderC1Bottom"><img runat="server" src="~/images/spacer.gif" width="7" height="7"/></td>
                            <td class="intBorderC1BottomRight"><img runat="server" src="~/images/spacer.gif" width="7" height="7"/></td>
                          </tr>
                        </table></td>
                        <td width="6" height="6"><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
                      </tr>
                      <tr>
                        <td width="6" height="6"><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
                        <td width="6" height="6"><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
                        <td width="6" height="6"><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
                      </tr>
                    </table></td>
                    <td class="boxInsideRight"><img runat="server" src="~/images/spacer.gif" width="1" height="4"/></td>
                  </tr>
                  <tr>
                    <td class="boxInsideBottomLeft" ><img runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
                    <td class="boxInsideBottom2"><img runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
                    <td class="boxInsideBottomRight"><img runat="server" src="~/images/spacer.gif" width="4" height="4" /></td>
                  </tr>
              </table></td>
            <td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1"/></td>
          </tr>
          <tr>
            <td class="frBottomLeft"></td>
            <td class="frBottom"></td>
            <td class="frBottomRight"></td>
          </tr>
          <tr>
            <td><img runat="server" src="~/images/spacer.gif" width="1" height="14"/></td>
          </tr>
        </table></td>
        </tr>      
    </table></td>
  </tr>
</table>