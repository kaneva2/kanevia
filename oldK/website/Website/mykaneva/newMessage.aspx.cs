///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using MagicAjax;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using log4net;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for NewMessage.
	/// </summary>
	public class newMessage : MainTemplatePage
	{
		protected newMessage () 
		{
			Title = "Compose New Kaneva Message";
		}

		private void Page_Load (object sender, System.EventArgs e)
		{
			int userId = GetUserId ();

            if (userId <= 0)
            {
                Response.Redirect(GetLoginURL());
                return;
            }


            // REDIRECT ALL REQUESTS TO NEW MESSAGE PAGE IN MESSAGE CENTER
            Response.Redirect (ResolveUrl("~/mykaneva/messageCenter.aspx?mc=me&" + Request.QueryString));


            
            //if (!divSuccess.Visible)
            if (divSuccess.Style["display"] == "none")
			{
				// User wants to Share an item
				if (Request ["URL"] != null && Request ["assetId"] != null)
				{
					if (!IsPostBack)
					{
						// Show the Catchpa?
						if (GetUserId () > 0)
						{
                            int iNumberAssetShares = (int) KanevaWebGlobals.CurrentUser.Stats.NumberAssetShares;

							int modResult = iNumberAssetShares % KanevaGlobals.ShareCatchpaCount;
							if (iNumberAssetShares > 0 && modResult.Equals (0))
							{
	//*							CaptchaControl1.Visible = true;
                                CaptchaControl1.Style.Add ("display", "hidden");
							}
						}

						// Get URL & set in text body
						txtSubject.Text	= "Check out this item on Kaneva!";
						txtBody.Text = "I hope you'll like this as much as I did.<br><br>";
						
						SetToOptions(MSG_TYPE.SHARE);	  
					}
				}
				else  // Send Message Request
				{
					if (!Request.IsAuthenticated && Request["URL"] == null)
					{
						Response.Redirect (GetLoginURL ());
						return;
					}
										 
					if (!IsPostBack)
					{                       
                        // Is it a reply?
						if (Request ["messageId"] != null)
						{
							int messageId = Convert.ToInt32 (Request ["messageId"]);
							if (messageId > 0)
							{
								DataRow drMessage = UsersUtility.GetUserMessage (GetUserId (), messageId);
								txtUsername.Text = drMessage ["username"].ToString ();
								txtSubject.Text = KanevaGlobals.Truncate ("RE:" + drMessage ["subject"].ToString (), 100);
								txtBody.Text = "<BR><BR><B>--Previous Message--</B><BR><B>From:</B>&nbsp;" +  drMessage ["username"].ToString () + "<BR><B>Sent:</B>&nbsp;" +  FormatDateTime (drMessage ["message_date"]) + "<BR><BR>" + drMessage ["message"].ToString ();
								hidMessageId.Value = messageId.ToString();

                                //gets the user info to whom the message is being sent
                                int recipientId = UsersUtility.GetUserIdFromUsername(txtUsername.Text);
                                //check to see if sender is a minor trying to contact a mature profile
                                CheckForMessageFromMinor(recipientId, userId);
                            }
						
							SetToOptions(MSG_TYPE.PRIVATE);
							optUser.Checked = true;
						}
							// Is it a new pm from public profile?
						else if (Request ["userId"] != null)
						{
                            int recipientUserId = Convert.ToInt32 (Request["userId"]);
                            toUserId = recipientUserId;
                            
                            //gets the user info to whom the message is being sent
                            UserFacade userFacade = new UserFacade ();
                            User recipient = userFacade.GetUser (recipientUserId);

                            //check to see if sender is a minor trying to contact a mature profile
                            CheckForMessageFromMinor (recipient.UserId, userId);

                            txtUsername.Text = recipient.Username;
						
							//additional logic to handle when coming from meetme 3d
							//if meetme3d request info found set options accourdingly
							if (Request ["meetMe3D"] != null)
							{
								//set hyperlink value
                                litMM3DLocation.Text = "<A href=\"" + ResolveUrl ("~/kgp/playwok.aspx") + "?goto=A" + recipient.UserId.ToString () + "&ILC=MM3D&link=mmnow\">Visit " + recipient.Username + " 3D home</a>";
								//set MM3D Message
								lblMM3DMessage.Text = "Currently this member is not in the 3D Virtual World of Kaneva. You can send a private message to say you stopped by or you can visit their home.";
								//set subject
								txtSubject.Text = "I stopped by to meet you ...";
								//configure message options
								SetToOptions(MSG_TYPE.MEET_ME_3D);
							}
							else
							{
								SetToOptions(MSG_TYPE.PRIVATE);
							}
							optUser.Checked = true;
						}
							// Tell others about this user
						else if (Request ["aboutId"] != null)
						{
     //*                       trSubject.Visible = false;
                            trSubject.Style.Add ("display", "none");

                            txtSubject.Text	= "Please take a look at this cool profile!";
							txtBody.Text = "<br>  This Kaneva profile is awesome!<br><br>";
						
							SetToOptions(MSG_TYPE.TELL_OTHERS);
							optUser.Checked = true;
						}
							// Tell others about this channel
						else if (Request ["communityId"] != null)
						{
     //*                       trSubject.Visible = false;
                            trSubject.Style.Add ("display", "none");

							// Show the Catchpa?
							if (GetUserId () > 0)
							{
                                int iNumberCommShares = (int) KanevaWebGlobals.CurrentUser.Stats.NumberCommunityShares;

								int modResult = iNumberCommShares % KanevaGlobals.ShareCatchpaCount;
								if (iNumberCommShares > 0 && modResult.Equals (0))
								{
		//*							CaptchaControl1.Visible = true;
                                    CaptchaControl1.Style.Add ("display", "hidden");
								}
							}

							txtSubject.Text	= "Kaneva Community that might interest you";
							txtBody.Text = "<br><br>I thought you might be interested in this Kaneva community!";
						
							SetToOptions(MSG_TYPE.TELL_OTHERS);
							optUser.Checked = true;
						}
						else
						{
							SetToOptions(MSG_TYPE.SHARE);
							optUser.Checked = true;
						}
					}
				}
			
	//*			trInsertMediaLink.Visible = false;
                trInsertMediaLink.Style.Add ("display", "none");

				if (Request.IsAuthenticated)
				{
					//logic to keep this link form showing during meet me 3D page
					if(Request ["meetMe3D"] == null)
					{
	//*					trInsertMediaLink.Visible = true;
                        trInsertMediaLink.Style.Add ("display", "block");
					}
				}
			
				if (!IsPostBack)
				{
					if (Request.UrlReferrer != null)
					{
						ViewState.Add ("AbsoluteUri", Request.UrlReferrer.AbsoluteUri);
					}
					else
					{
						AddBreadCrumb (new BreadCrumb ("Compose Message", GetCurrentURL (), "", 0));
					}
				}

				//				functionalize this stuff if it works
				if (IsPostBack)
				{
					BuildPersonalMessageUrl();
				}
			}
																						 
			// Set Nav
			if (Request.IsAuthenticated)
			{
                HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
				HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.MESSAGES;
				HeaderNav.SetNavVisible(HeaderNav.MyKanevaNav,2);
				//HeaderNav.SetNavVisible(HeaderNav.MyMessagesNav);
			
				//HeaderNav.MyMessagesNav.ActiveTab = NavMessages.TAB.INBOX;
				if (Request ["bulletin"] != null)
				{
					//HeaderNav.MyMessagesNav.ActiveTab = NavMessages.TAB.NONE;
				}
			}

            btnAddFriend.Attributes["onclick"] = "javascript:return " +
                    "confirm('Send a friend request to this user?')";

			txtBody.EditorBodyStyle = "font-size:10px;font-family:Verdana;";
			litEditorId.Text = txtBody.ClientID;
			litSiteName.Text = KanevaGlobals.SiteName;
		}

		
		#region Helper Methods

        //function to prevent minors from contacting people with their profiles set to restricted
        private void CheckForMessageFromMinor (int recipientId, int messageSenderid)
        {
            UserFacade userFacade = new UserFacade ();
            User recipient = userFacade.GetUser (recipientId);
            //gets the user info who is sending the message
            User messageSender = userFacade.GetUser (messageSenderid);

            //check to see if sender is a minor trying to contact a mature profile
            if (recipient.MatureProfile && !messageSender.IsAdult)
            {
                Response.Redirect (GetPersonalChannelUrl (recipient.NameNoSpaces));
                /*ShowErrMsg("You can not send messages to members who have their profile marked as mature");
                if (Request.UrlReferrer != null)
                {
                    Response.Redirect(Request.UrlReferrer.AbsoluteUri.ToString());
                }
                else
                {
                    Response.Redirect(GetPersonalChannelUrl(recipient.NameNoSpaces));
                }*/
            }
        }

        protected void BuildPersonalMessageUrl ()
        {
            if (Request["assetId"] != null)
            {
                string url = "http://" + KanevaGlobals.SiteName + Server.UrlDecode (Request["URL"].ToString ());
                if (Request.IsAuthenticated)
                {
                    url += url.IndexOf ("?") > 0 ? "&" : "?";
                    url += "refId=" + this.GetUserId ();
                }
                strUrl = "Here's the link:<br><a href ='" + url + "'>" + url + "</a><br><br>Personal message:<br><br>";
            }
            // Tell others about this user
            else if (Request["aboutId"] != null)
            {
                UserFacade userFacade = new UserFacade ();
                User user = userFacade.GetUser (Convert.ToInt32 (Request["aboutId"]));

                strUrl = "Here's the link:<br>  <a href ='" + GetPersonalChannelUrl (user.NameNoSpaces) + "'>" +
                    GetPersonalChannelUrl (user.NameNoSpaces) + "</a><br><br>Personal message:<br><br>";
            }
            // Tell others about this channel
            else if (Request["communityId"] != null)
            {
                int channelId = Convert.ToInt32 (Request["communityId"]);

                DataRow drChannel = CommunityUtility.GetCommunity (channelId);

                bool isPersonal = CommunityUtility.IsChannelPersonal (drChannel);
                string channelUrl = null;

                if (isPersonal)
                {
                    channelUrl = KanevaGlobals.GetPersonalChannelUrl (drChannel["name_no_spaces"].ToString ());
                }
                else
                {
                    channelUrl = KanevaGlobals.GetBroadcastChannelUrl (drChannel["name_no_spaces"].ToString ());
                }
                strUrl = "Here's the link:<br><a href ='" + channelUrl + "'>" + channelUrl + "</a><br><br>Personal message:<br><br>";
            }
        }

        private void SetToOptions (MSG_TYPE msgType)
		{
			// table rows
	//*		trEmail.Visible = false;
	//*		trUsername.Visible = false;
	//*		trMeetMe3DLink.Visible = false;
            trEmail.Style.Add ("display", "none");
            trUsername.Style.Add ("display", "none");
            trMeetMe3DLink.Style.Add ("display", "none");
            trInsertMediaLink.Style.Add ("display", "none");


			// radio buttons
	//*		optEmail.Visible = false;
	//*		optUser.Visible = false;
            optEmail.Style.Add ("display", "none");
            optUser.Style.Add ("display", "none");

			switch (msgType)
			{
				case MSG_TYPE.PRIVATE:
	//*				trUsername.Visible = true;
                    trUsername.Style.Add ("display", "block");
					txtUsername.Enabled = false;
					break;
				case MSG_TYPE.MEET_ME_3D:
	//*				trUsername.Visible = true;
                    trUsername.Style.Add ("display", "block");
					txtUsername.Enabled = false;
	//*				trMeetMe3DLink.Visible = true;
	//*				trInsertMediaLink.Visible = false;     -- moved up top
                    trMeetMe3DLink.Style.Add ("display", "block");
					break;

				case MSG_TYPE.SHARE:
				case MSG_TYPE.TELL_OTHERS:
					if (!Request.IsAuthenticated)
					{
						optEmail.Checked = true;
					}
					else
					{
	//*					trUsername.Visible = true;
	//*					optEmail.Visible = true;
	//*					optUser.Visible = true;
                        trUsername.Style.Add ("display", "block");
                        optEmail.Style.Add ("display", "block");
                        optUser.Style.Add ("display", "block");
					}
	//*				trEmail.Visible = true;
                    trEmail.Style.Add ("display", "none");
					break;

				default:
					if (Request.IsAuthenticated)
					{
    //*					trEmail.Visible = true;
	//*					trUsername.Visible = true;
	//*					optEmail.Visible = true;
	//*					optUser.Visible = true;
                        trEmail.Style.Add ("display", "block");
                        trUsername.Style.Add ("display", "block");
                        optEmail.Style.Add ("display", "block");
                        optUser.Style.Add ("display", "block");
					}
					else
					{	
	//*					trEmail.Visible = true;
						optEmail.Checked = true;
	//*					optEmail.Visible = false;
                        trEmail.Style.Add ("display", "block");
                        optEmail.Style.Add ("display", "none");
                        
					}
					
					break;
						
			}
		}
		
		/// <summary>
		/// SendMessage
		/// </summary>
		/// <param name="toId"></param>
		private int SendMessage (int toId, string strSubject)
		{
            CommunityFacade communityFacade = new CommunityFacade();

            //check for minor user to prevent work around by using tell others
            CheckForMessageFromMinor(toId, GetUserId());

            // confirm the user can send a pm to a non-friend (user has not exceeded daily max)
            if (!IsAdministrator ())
            {
                if (UsersUtility.ConfirmCanSendNonFriendPM (GetUserId (), toId).Equals (Constants.ERR_USER_EXCEEDED_NON_FRIEND_MESSAGE_COUNT))
                {
                    return Constants.ERR_USER_EXCEEDED_NON_FRIEND_MESSAGE_COUNT;
                }
            }

            
            // If this was a share, record it in the db
			if (Request ["communityId"] != null)
			{
                communityFacade.ShareCommunity(Convert.ToInt32(Request["communityId"]), GetUserId(), toId, "");
			}

			// Update message to show user replied to message
			if (hidMessageId.Value != string.Empty && Convert.ToInt32(hidMessageId.Value) > 0)
			{
				UsersUtility.UpdateMessageReply(GetUserId (), Convert.ToInt32 (Request ["messageId"]));
			}
            
            UserFacade userFacade = new UserFacade();

            // Insert the message
            Message message = new Message(0, GetUserId(), toId, strSubject,
                strUrl + txtBody.Text, new DateTime(), 0, (int)Constants.eMESSAGE_TYPE.PRIVATE_MESSAGE, 0, "U", "S");

            userFacade.InsertMessage(message);

            User userTo = userFacade.GetUser(toId);

            if (GetUserId() > 0)
            {
                if (Convert.ToInt32(userTo.NotifyAnyoneMessages).Equals(1))
                {
                    // They want emails for all messages
                    MailUtilityWeb.SendPrivateMessageNotificationEmail(userTo.Email, strSubject, message.MessageId, message.MessageText, userTo.UserId, GetUserId());
                }
                else if (Convert.ToInt32(userTo.NotifyFriendMessages).Equals(1) && userFacade.AreFriends (toId, GetUserId()))
                {
                    // They want to be emailed for messages from friends
					MailUtilityWeb.SendPrivateMessageNotificationEmail(userTo.Email, strSubject, message.MessageId, message.MessageText, userTo.UserId, GetUserId());
                }
            }

            return message.MessageId;

		}

		/// <summary>
		/// Show success notice for type of message sent.
		/// </summary>
		/// <returns></returns>

		private void ShowSuccess(MSG_TYPE msgType, string name)
		{
			ShowSuccess(msgType, name, 0, 0);
		}
		private void ShowSuccess(MSG_TYPE msgType, string name, int userId)
		{
			ShowSuccess(msgType, name, 0, userId);
		}	/// <summary>
		/// Show success notice for type of message sent.
		/// </summary>
		/// <returns></returns>
		private void ShowSuccess(MSG_TYPE msgType, string name, int channelId, int userId)
		{
            UserFacade userFacade = new UserFacade();
			string msg = string.Empty;
			string link = string.Empty;
			string link_desc = string.Empty;
			string type = "Message";
            
			switch (msgType)
			{
				case MSG_TYPE.PRIVATE:
				{
                    User user = userFacade.GetUser(userId);

                    lnkMember.HRef = GetPersonalChannelUrl(user.NameNoSpaces);
                    imgAvatar.Src = ResolveUrl (GetProfileImageURL (user.ThumbnailSmallPath));
	//*				divAvatar.Visible = true;
                    divAvatar.Style.Add ("display", "block");
                    msg = "Message sent to <a href=" + GetPersonalChannelUrl(user.NameNoSpaces) + ">" + name + "</a>";
					link = ResolveUrl("~/mykaneva/mailbox.aspx");
					link_desc = "Message Inbox";
					break;
				}

				case MSG_TYPE.MEET_ME_3D:
				{
                    User user = userFacade.GetUser(userId);

	//*				divAvatar.Visible = false;
                    divAvatar.Style.Add ("display", "none");
					trMeetMe3DSuccess.Style.Add ("display", "block");
					lblMM3DSuccessMessage.Text = "Currently this member is not in the 3D Virtual World of Kaneva.";
                    litMM3DLocation2.Text = "<A href=\"" + ResolveUrl ("~/kgp/playwok.aspx") + "?goto=A" + Request["userId"].ToString () + "&ILC=MM3D&link=mmnow\">Visit " + user.Username + " 3D home</a>";
                    msg = "Message sent to <a href=" + GetPersonalChannelUrl(user.NameNoSpaces) + ">" + name + "</a>";
					link = ResolveUrl("~/mykaneva/mailbox.aspx");
					link_desc = "Message Inbox";
	//*				tdSuccessMsg.Visible = false;
                    tdSuccessMsg.Style.Add ("display", "none");
					break;
				}

				case MSG_TYPE.EMAIL:
					msg = "Message sent to " + name;
					link = ResolveUrl("~/mykaneva/mailbox.aspx");
					link_desc = "Message Inbox";
					break;
			}

			h1Title.InnerText = "Success";
			lblGoDesc.Text = link_desc;
			lnkSuccessGo.HRef = link;
			tdSuccessMsg.InnerHtml = msg + ".";
			spnAlertMsgSuccess.InnerText = type + " successfully sent.";

            divSuccess.Style.Add ("display", "block");
            divSendMessage.Style.Add ("display", "none");
		}

		private void ShowErrMsg(string msg)
		{
            spnAlertMsg.InnerHtml = msg;
            spnAlertMsg.Style.Add ("display", "block");
		}

        private void AddFriend ()
        {
            if (Request.IsAuthenticated)
            {
                // Add them as a friend
                int ret = GetUserFacade.InsertFriendRequest (GetUserId (), toUserId,
                    Page.Request.CurrentExecutionFilePath, Global.RequestRabbitMQChannel());

                if (ret.Equals (1))	// already a friend
                {
                    AjaxCallHelper.WriteAlert ("This member is already your friend.");
                }
                else if (ret.Equals (2))  // pending friend request
                {
                    AjaxCallHelper.WriteAlert ("You have already sent this member a friend request.");
                }
                else
                {
                    // send request email
                    MailUtilityWeb.SendFriendRequestEmail (GetUserId (), toUserId);
                    ShowErrMsg("A friend request was sent to user " + UsersUtility.GetUserName(toUserId) + ".");
                }
            }
        }

        #endregion


		#region Event Handlers
		
        /// <summary>
		/// Cancel Event Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnCancel_Click (object sender, ImageClickEventArgs e) 
		{
			Response.Redirect ("~/mykaneva/mailbox.aspx");
		}

		/// <summary>
		/// Update Event Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnSend_Click (object sender, ImageClickEventArgs e) 
		{
            CommunityFacade communityFacade = new CommunityFacade();
            MediaFacade mediaFacade = new MediaFacade();

			int userId = 0;
			string strSubject = Server.HtmlEncode (txtSubject.Text.Trim ());
			spnAlertMsg.InnerText = string.Empty;

			//if this user has triggered the security code requirement
			//check their security code before continuing
			if (CaptchaControl1.Visible)
			{
				if (!CaptchaControl1.UserValidated)
				{
					ShowErrMsg ("Unique security code did not match.");
					return;
				}
			}

			// Verify user entered a message
            if (txtBody.Text.Trim() == "")
            {
                ShowErrMsg ("Please enter a message.");
                return;   
            }

            // Check to make sure user did not enter any "potty mouth" words
            if (KanevaWebGlobals.isTextRestricted (txtBody.Text, Constants.eRESTRICTION_TYPE.POTTY_MOUTH) ||
                KanevaWebGlobals.isTextRestricted (strSubject, Constants.eRESTRICTION_TYPE.POTTY_MOUTH))
            {
                spnAlertMsg.InnerText = Constants.VALIDATION_REGEX_RESTRICTED_WORD_MESSAGE;
                spnAlertMsg.Attributes.Add ("class", "errBox black");
                spnAlertMsg.Style.Add ("display", "block");
                return;
            }

            // Default the subject
			if (strSubject.Length.Equals (0))
			{
				strSubject = NO_SUBJECT;
			}
		
			if (!Request.IsAuthenticated && Request["URL"] == null)
			{
				Response.Redirect (GetLoginURL ());
			}

			// send to an email address
			if (optEmail.Checked)
			{
				string email = txtEmail.Text.Trim ();

				if (email == string.Empty)
				{
					ShowErrMsg("Please enter a valid email address.");
					return;
				}

				// Are they sharing a channel or asset?
				if (Request ["communityId"] != null)
				{
					int channelId = Convert.ToInt32 (Request ["communityId"]);
                    communityFacade.ShareCommunity(channelId, GetUserId(), 0, email);
					MailUtilityWeb.ShareChannelWithFriend (GetUserId (), email, channelId, "A Kaneva visitor", strSubject + "<br><br>" + txtBody.Text, Page);
				}
				else if (Request ["aboutId"] != null)
				{
					int aboutId = Convert.ToInt32 (Request ["aboutId"]);
                    //communityFacade.ShareCommunity (channelId, GetUserId (), 0, email);
					MailUtilityWeb.ShareProfileWithFriend (GetUserId (), email, aboutId, "A Kaneva visitor", strSubject + "<br><br>" + txtBody.Text, Page);
				}
				else if (Request ["assetId"] != null)
				{
					int assetId = Convert.ToInt32 (Request ["assetId"]);
                    DataRow drAsset = StoreUtility.GetAsset(assetId);
                    mediaFacade.ShareAsset(assetId, GetUserId(), Common.GetVisitorIPAddress(), email, -1);
                    MailUtility.ShareAssetWithFriend(GetUserId(), email, assetId, "visitor", strSubject + "<br><br>" + txtBody.Text, drAsset, GetAssetTypeName(Convert.ToInt32 (drAsset["asset_type_id"])), Page);
				}
				else
				{
                    MailUtility.SendEmail ("Kaneva visitor", email, strSubject, txtBody.Text, true, false, 2);
				}

				ShowSuccess(MSG_TYPE.EMAIL, email);
				return;
			}
				// Send to a Kaneva user
			else if (optUser.Checked)
			{
				// Look up the user id via the username
				userId = UsersUtility.GetUserIdFromUsername (txtUsername.Text.Trim ());

				if (userId == 0)
				{
					ShowErrMsg("Please provide a valid Kaneva username.");
					return;
				}

				// This is the case they are sending a PM, must be logged in
				if (!Request.IsAuthenticated)
				{
					Response.Redirect (GetLoginURL ());
					return;
				}

                int retCode = SendMessage (userId, strSubject);

                if (retCode.Equals (-99))
				{
					ShowErrMsg("Message not sent. This member has blocked you from sending messages to them.");
					return;
				}
                else if (retCode.Equals (Constants.ERR_USER_EXCEEDED_NON_FRIEND_MESSAGE_COUNT))
                {   // user has exceeded number of sent non-friend messages for today
                    ShowErrMsg ("You have exceeded the maximum daily number of private messages sent " +
                        " to non-friends (" + KanevaGlobals.MaxNumberNonFriendMessagesPerDay.ToString () + 
                        "). In order to send this message, the person will need to add you as a friend." +
                        "<br/><br/><a href=\"javascript:void(0);\" " +
                        " onclick=\"$('" + btnAddFriend.ClientID + "').click();" + "\">Become a Friend</a>");
                    return;
                }
                {
                    // If this is an asset share
                    if (Request["assetId"] != null)
                    {
                        mediaFacade.ShareAsset(Convert.ToInt32(Request["assetId"]),
                            GetUserId (), Common.GetVisitorIPAddress(), null, userId);
                    }
                }

				if (Request ["meetMe3D"] != null)
				{
					ShowSuccess(MSG_TYPE.MEET_ME_3D, txtUsername.Text.Trim (), userId);
				}
				else
				{
					ShowSuccess(MSG_TYPE.PRIVATE, txtUsername.Text.Trim (), userId);
				}
				return;
			}
			else
			{
				ShowErrMsg("Please select a send to option.");
				return;
			}
		}

		protected void lbBack_Click (object sender, EventArgs e)
		{
			if (ViewState ["AbsoluteUri"] != null && ViewState ["AbsoluteUri"].ToString () != string.Empty)
			{
				Response.Redirect (ViewState ["AbsoluteUri"].ToString ());
			}
			else
			{
				NavigateBackToBreadCrumb(2);
			}
		}

        protected void btnAddFriend_Click (object sender, EventArgs e)
        {
            // Must be logged in
            if (!Request.IsAuthenticated)
            {
                AjaxCallHelper.WriteAlert ("Please sign in to add this user as a friend");
                return;
            }

            // Can't friend yourself
            if (GetUserId ().Equals (toUserId))
            {
                AjaxCallHelper.WriteAlert ("You cannot add yourself as a friend.");
                return;
            }

            AddFriend ();
        }
        
        #endregion Event Handlers


        #region Properties

        private int toUserId
        {
            get
            {
                if (ViewState["toUserId"] == null)
                {
                    ViewState["toUserId"] = 0;
                }
                return (int) ViewState["toUserId"];
            }
            set
            {
                ViewState["toUserId"] = value;
            }
        }

        #endregion Properties


        #region Declerations

        protected CuteEditor.Editor txtBody;

		protected PlaceHolder	phBreadCrumb;
		protected TextBox		txtSubject, txtUsername, txtEmail;
		protected Label			lblGoDesc, lblMM3DMessage, lblMM3DSuccessMessage;
		protected Literal		litEditorId, litSiteName, litMM3DLocation, litMM3DLocation2;
        protected LinkButton btnAddFriend;		

		protected HtmlInputRadioButton	optUser, optEmail;
		protected HtmlInputHidden		hidLinkURL, hidMessageId;
        protected HtmlTableRow trEmail, trUsername, trMeetMe3DLink, trMeetMe3DSuccess, trInsertMediaLink, trSubject;
		protected HtmlTableCell			tdSuccessMsg;
		protected HtmlTable				tblComposeMessage;
		protected HtmlContainerControl	spnAlertMsg, spnAlertMsgSuccess, divSuccess, divSendMessage;
		protected HtmlContainerControl	divBackToChannel, h1Title, divAvatar;
		protected HtmlAnchor			lnkSuccessGo, lnkBackToChannel, lnkMember;
		protected HtmlImage				imgAvatar;
		
        protected WebControlCaptcha.CaptchaControl CaptchaControl1;

		private const string NO_SUBJECT = "No subject";
        protected string		strUrl = "";

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

        private enum MSG_TYPE
		{
			BULLETIN_FRIEND_GROUP,
			BULLETIN_ALL_FRIENDS,
			BULLETIN_CHANNEL_MEMBERS,
			PRIVATE,
			SHARE,
			TELL_OTHERS,
			EMAIL,
			MEET_ME_3D
		}
		
		#endregion


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
