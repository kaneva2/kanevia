<%@ Page language="c#" MasterPageFile="~/masterpages/GenericPageTemplate.Master" ValidateRequest="False" Codebehind="personal.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.personal" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>

<asp:Content ID="cnt_PersonalHead" runat="server" contentplaceholderid="cph_HeadData" >
    <meta name="title" content="Kaneva. The Online Community and Social Network." />
    <meta name="description" content="Kaneva is the first to combine social networking and a free 3D virtual world. It's a whole new way to connect with friends. Have fun in a vibrant virtual world full of people, cool places and fun games. Create your avatar and get a free 3D home today!"/>
    <meta name="keywords" content="free virtual world, virtual world, virtual worlds, 3D world, online avatar community, mmo game, virtual reality, caneva, kaniva" />
    
    <NOSCRIPT><META HTTP-EQUIV="refresh" CONTENT="1" URL="noJavascript.aspx" /></NOSCRIPT>

    
    <script type="text/javascript" src="../jscript/prototype.js"></script>
    <script type="text/javascript" src="../jscript/scriptaculous/scriptaculous.js"></script>

    <!-- Dependencies -->
    <script type="text/javascript" src="../jscript/yahoo/2.3.0/yahoo-min.js"></script>
    <script type="text/javascript" src="../jscript/yahoo/2.3.0/dom-min.js"></script>
    <script type="text/javascript" src="../jscript/yahoo/2.3.0/yahoo-dom-event.js"></script>
    									
    <script type="text/javascript" src="../jscript/yahoo/2.3.0/connection-min.js"></script>  
    <script type="text/javascript" src="../jscript/yahoo/2.3.0/autocomplete-min.js"></script>
    <script type="text/javascript" src="../jscript/yahoo/2.3.0/animation-min.js"></script>

    <script type="text/javascript" src="../jscript/kanevaLightbox.js"></script>

    <script type="text/javascript" src="../jscript/mykaneva/personal.js"></script>

    <!--CSS file (default YUI Sam Skin) -->
    <link href="../css/yahoo/autocomplete/skins/sam/autocomplete.css" rel="stylesheet" type="text/css">
    <link href="../css/lightbox.css" rel="stylesheet" type="text/css">
</asp:Content>

<asp:Content ID="mainContent" runat="server" contentplaceholderid="cph_Body">
<link href="../css/mykaneva/personal.css" rel="stylesheet" type="text/css">
<script language="javascript">
var remBtnId = '<asp:literal id="litRemoveButtonClientId" runat="server"/>';
//this gets the prefix .NET is appending to all the controls and sets it to the global variable in 
//the interest.js page
SetPageNamePrefix('<%=lnkAddSchool.NamingContainer.ClientID%>_');
var addBtnId = '<asp:literal id="litAddButtonClientId" runat="server"/>';
</script>
    <!--<ajax:AjaxPanel runat="server">-->
    <div id="editBody">
	    <div id="messageArea">
	        <asp:ValidationSummary ShowMessageBox="False" ShowSummary="True" cssclass="errBox" id="valSum" DisplayMode="BulletList" runat="server" HeaderText="Please correct the following errors:" />
	        <div id="alertMsg" runat="server" class="errBox" visible="false"></div>
            <ajax:ajaxpanel id="ajpMessage" runat="server">
                <span id="spnAlertMsg" class="infoBox" runat="server" style="display:none;"></span>
            </ajax:ajaxpanel>

	    </div>
        <div id="pageHeader">
            <div>
                <h6>EDIT: Personal Info</h6>
            </div>
            <div class="restrictions">
                <a href="~/myKaneva/profile.aspx" class="btnlarge grey" id="changeRestrictions" runat="server">Edit My Interests</a>
            </div>
        </div>
        <div id="personal">
            <h4>Personal</h4>
            <ajax:ajaxpanel id="ajpHereFor">
            <div id="tdHereFor" runat="server" class="yui-skin-sam data" style="margin-bottom:10px"></div>
            </ajax:ajaxpanel>
			<div class="personalRow">
			    <div class="personalCol1">
			        Relationship:&nbsp;
			    </div>    
			    <div class="personalCol2">
			        <asp:Dropdownlist runat="server" class="biginput" ID="drpRelationship" style="width:150px">
					    <asp:ListItem  value="">No Answer</asp:ListItem>
					    <asp:ListItem  value="Single">Single</asp:ListItem>
					    <asp:ListItem  value="In a relationship">In a relationship</asp:ListItem>
					    <asp:ListItem  value="Married">Married</asp:ListItem>
					    <asp:ListItem  value="Divorced">Divorced</asp:ListItem>
				    </asp:Dropdownlist>
				</div>
				<div class="personalCol1">
					Education:&nbsp;
			    </div>    
			    <div class="personalCol2">
					<asp:Dropdownlist runat="server" class="biginput" ID="drpEducation" style="width:150px">
						<asp:ListItem  value="">No Answer</asp:ListItem>
						<asp:ListItem  value="Dropout">Dropout</asp:ListItem>
						<asp:ListItem  value="High School">High School</asp:ListItem>
						<asp:ListItem  value="Some college">Some college</asp:ListItem>
						<asp:ListItem  value="In college">In college</asp:ListItem>
						<asp:ListItem  value="Bachelors Degree">Bachelors Degree</asp:ListItem>
						<asp:ListItem  value="Masters Degree">Masters Degree</asp:ListItem>
						<asp:ListItem  value="Doctorate">Doctorate</asp:ListItem>
					</asp:Dropdownlist>
				</div>
			</div>
			<div class="personalRow" >
			    <div id="trSexOrientation" runat="server">
			        <div class="personalCol1" >
			            Sexual Orientation:&nbsp;
			        </div>    
			        <div class="personalCol2">
					    <asp:Dropdownlist runat="server" class="biginput" ID="drpOrientation" style="width:150px">
						    <asp:ListItem  value="">No Answer</asp:ListItem>
						    <asp:ListItem  value="Straight">Straight</asp:ListItem>
						    <asp:ListItem  value="Gay/lesbian">Gay/lesbian</asp:ListItem>
						    <asp:ListItem  value="Bi-sexual">Bi-sexual</asp:ListItem>
						    <asp:ListItem  value="Not sure">Not sure</asp:ListItem>
					    </asp:Dropdownlist>
				    </div>
				</div>
				<div class="personalCol1">
					Income:&nbsp;
			    </div>    
			    <div class="personalCol2">
					<asp:Dropdownlist runat="server" class="biginput" ID="drpIncome" style="width:230px">
						<asp:ListItem  value="">No Answer</asp:ListItem>
						<asp:ListItem  value="1">< $20,000</asp:ListItem>
						<asp:ListItem  value="2">Between $20,000 and $30,000</asp:ListItem>
						<asp:ListItem  value="3">Between $30,000 and $40,000</asp:ListItem>
						<asp:ListItem  value="4">Between $40,000 and $50,000</asp:ListItem>
						<asp:ListItem  value="5">Between $60,000 and $80,000</asp:ListItem>
						<asp:ListItem  value="6">Between $80,000 and $100,000</asp:ListItem>
						<asp:ListItem  value="7">Between $100,000 and $150,000</asp:ListItem>
						<asp:ListItem  value="8">$150,000+</asp:ListItem>
					</asp:Dropdownlist>
				</div>
			</div>
			<div class="personalRow" >
			    <div class="personalCol1">
			        Religion:&nbsp;
			    </div>    
			    <div class="personalCol2">
					<asp:Dropdownlist runat="server" class="biginput" ID="drpReligion" style="width:150px">
						<asp:ListItem  value="">No Answer</asp:ListItem>
						<asp:ListItem  value="Agnostic">Agnostic</asp:ListItem>
						<asp:ListItem  value="Atheist">Atheist</asp:ListItem>
						<asp:ListItem  value="Buddhist">Buddhist</asp:ListItem>
						<asp:ListItem  value="Catholic">Catholic</asp:ListItem>
						<asp:ListItem  value="Christian (Other)">Christian (Other)</asp:ListItem>
						<asp:ListItem  value="Hindu">Hindu</asp:ListItem>
						<asp:ListItem  value="Jewish">Jewish</asp:ListItem>
						<asp:ListItem  value="Mormon">Mormon</asp:ListItem>
						<asp:ListItem  value="Muslim">Muslim</asp:ListItem>
						<asp:ListItem  value="Other">Other</asp:ListItem>
						<asp:ListItem  value="Protestant">Protestant</asp:ListItem>
						<asp:ListItem  value="Scientologist">Scientologist</asp:ListItem>
						<asp:ListItem  value="Taoist">Taoist</asp:ListItem>
						<asp:ListItem  value="Wiccan">Wiccan</asp:ListItem>
					</asp:Dropdownlist>		
				</div>
				<div class="personalCol1">
					Height:&nbsp;
			    </div>    
			    <div class="personalCol2">
					<asp:Dropdownlist runat="server" class="biginput" ID="drpHeightFeet">
						<asp:ListItem  value="0">-</asp:ListItem>
						<asp:ListItem  value="3">3</asp:ListItem>
						<asp:ListItem  value="4">4</asp:ListItem>
						<asp:ListItem  value="5">5</asp:ListItem>
						<asp:ListItem  value="6">6</asp:ListItem>
						<asp:ListItem  value="7">7</asp:ListItem>
					</asp:Dropdownlist> Feet
					<asp:Dropdownlist runat="server" class="biginput" ID="drpHeightInches">
						<asp:ListItem  value="-1">-</asp:ListItem>
						<asp:ListItem  value="0">0</asp:ListItem>
						<asp:ListItem  value="1">1</asp:ListItem>
						<asp:ListItem  value="2">2</asp:ListItem>
						<asp:ListItem  value="3">3</asp:ListItem>
						<asp:ListItem  value="4">4</asp:ListItem>
						<asp:ListItem  value="5">5</asp:ListItem>
						<asp:ListItem  value="6">6</asp:ListItem>
						<asp:ListItem  value="7">7</asp:ListItem>
						<asp:ListItem  value="8">8</asp:ListItem>
						<asp:ListItem  value="9">9</asp:ListItem>
						<asp:ListItem  value="10">10</asp:ListItem>
						<asp:ListItem  value="11">11</asp:ListItem>
					</asp:Dropdownlist> Inches 
				</div>
			</div>
			<div class="personalRow" >
			    <div class="personalCol1">
			        Ethnicity:&nbsp;
			    </div>    
			    <div class="personalCol2">
					<asp:Dropdownlist runat="server" class="biginput" ID="drpEthnicity" style="width:150px">
						<asp:ListItem  value="">No Answer</asp:ListItem>
						<asp:ListItem  value="Asian">Asian</asp:ListItem>
						<asp:ListItem  value="Black">Black</asp:ListItem>
						<asp:ListItem  value="East Indian">East Indian</asp:ListItem>
						<asp:ListItem  value="European">European</asp:ListItem>
						<asp:ListItem  value="Latino/Hispanic">Latino/Hispanic</asp:ListItem>
						<asp:ListItem  value="Middle Eastern">Middle Eastern</asp:ListItem>
						<asp:ListItem  value="Mixed">Mixed</asp:ListItem>
						<asp:ListItem  value="Native American">Native American</asp:ListItem>
						<asp:ListItem  value="Other">Other</asp:ListItem>
						<asp:ListItem  value="Pacific Rim">Pacific Rim</asp:ListItem>
						<asp:ListItem  value="White">White</asp:ListItem>
					</asp:Dropdownlist>
				</div>
				<div id="trSmoking" runat="server" visible="false" >
				    <div class="personalCol1">
					    Smoking:&nbsp;
			        </div>    
			        <div class="personalCol2">
					    <asp:Dropdownlist runat="server" class="biginput" ID="drpSmoking" style="width:150px">
						    <asp:ListItem  value="">No Answer</asp:ListItem>
						    <asp:ListItem  value="Yes">Yes</asp:ListItem>
						    <asp:ListItem  value="No">No</asp:ListItem>
					    </asp:Dropdownlist>
				    </div>
				</div>
			</div>
			<div class="personalRow" >
			    <div class="personalCol1">
					Children:&nbsp;
			    </div>    
			    <div class="personalCol2">
					<asp:Dropdownlist runat="server" class="biginput" ID="drpChildren" style="width:150px">
						<asp:ListItem  value="">No Answer</asp:ListItem>
						<asp:ListItem  value="Don't want any">Don't want any</asp:ListItem>
						<asp:ListItem  value="One day">One day</asp:ListItem>
						<asp:ListItem  value="Undecided">Undecided</asp:ListItem>
						<asp:ListItem  value="I am a parent">I am a parent</asp:ListItem>
					</asp:Dropdownlist>
				</div>
				<div id="trDrinking" runat="server" visible="false" >
				    <div class="personalCol1">
					    Drinking:&nbsp;
			        </div>    
			        <div class="personalCol2">
					    <asp:Dropdownlist runat="server" class="biginput" ID="drpDrinking" style="width:150px">
						    <asp:ListItem  value="">No Answer</asp:ListItem>
						    <asp:ListItem  value="Yes">Yes</asp:ListItem>
						    <asp:ListItem  value="No">No</asp:ListItem>
					    </asp:Dropdownlist>
				    </div>
				</div>
			</div>
			<div class="personalRow" >
			    <div class="personalCol1">
					Hometown:&nbsp;
			    </div>    
			    <div class="personalCol2">
                    <asp:textbox id="txtHometown" runat="server" class="biginput" maxlength="100" size="20" />
				</div>
				<div class="personalCol1">
				</div>
				<div class="personalCol2">
				</div>
			</div>
        </div>
        <div id="education">
            <h4>Education/School</h4>
			<ajax:ajaxpanel id="ajpSchools" runat="server">
			<div id="trSchool1" runat="server" style="margin-bottom:20px">
			    <div class="personalRow accountText">
			        <div class="schoolCol1">
    			        State, Province, District, Division:&nbsp;
    			    </div>
			        <div class="schoolCol2">
                        <asp:textbox id="txtState1" name="txtState1" runat="server" class="biginput" maxlength="25" size="20" />
                    </div>
                </div>
			    <div class="personalRow accountText">
			        <div class="schoolCol1">
			            City:&nbsp;
    			    </div>
			        <div class="schoolCol2">
                        <asp:textbox id="txtCity1" runat="server" class="biginput" maxlength="25" size="20" />
                    </div>
                </div>
			    <div class="personalRow accountText">
			        <div class="schoolCol1">
			            School:&nbsp;
    			    </div>
			        <div class="schoolCol2 accountText yui-skin-sam">
			            <div id="myAutoComplete">
                            <asp:textbox id="txtSchool1" runat="server" class="biginput" maxlength="80" size="20" />
                            <div id="myContainer_s1"></div>
                        </div>
                    </div>
                </div>
			    <div class="personalRow accountText">
			        <div class="schoolCol1">&nbsp;
    			    </div>
			        <div class="schoolCol2">                    
			            <asp:linkbutton id="btnRemoveSchool1" onclientclick="return confirmDelete();" oncommand="lbRemoveSchool_Click" commandargument="0" runat="server" >
			                <img alt="add" id="img3" style="border:none" runat="server" src="~/images/del_sm.gif" />&nbsp;Remove this School...
			            </asp:linkbutton>
                    </div>
                </div>
		    </div>
			<div id="trSchool2" runat="server" style="margin-bottom:20px">
			    <div class="personalRow accountText">
			        <div class="schoolCol1">
    			        State, Province, District, Division:&nbsp;
    			    </div>
			        <div class="schoolCol2">
                        <asp:textbox id="txtState2" runat="server" class="biginput" maxlength="25" size="20" />
                    </div>
                </div>
			    <div class="personalRow accountText">
			        <div class="schoolCol1">
			            City:&nbsp;
    			    </div>
			        <div class="schoolCol2">
                        <asp:textbox id="txtCity2" runat="server" class="biginput" maxlength="25" size="20" />
                    </div>
                </div>
			    <div class="personalRow accountText">
			        <div class="schoolCol1">
			            School:&nbsp;
    			    </div>
			        <div class="schoolCol2">
			            <asp:textbox id="txtSchool2" runat="server" class="biginput" maxlength="80" size="20" />
                    </div>
                </div>
			    <div class="personalRow accountText">
			        <div class="schoolCol1">&nbsp;
    			    </div>
			        <div class="schoolCol2">                    
                        <asp:linkbutton id="btnRemoveSchool2" onclientclick="return confirmDelete();" oncommand="lbRemoveSchool_Click" commandargument="0" runat="server" >
			                <img alt="add" id="img4" style="border:none" runat="server" src="~/images/del_sm.gif" />&nbsp;Remove this School...
                        </asp:linkbutton>
                    </div>
                </div>
		    </div>
			<div id="trSchool3" runat="server" style="margin-bottom:20px">
			    <div class="personalRow accountText">
			        <div class="schoolCol1">
			            State, Province, District, Division:&nbsp;
    			    </div>
			        <div class="schoolCol2">
                        <asp:textbox id="txtState3" runat="server" class="biginput" maxlength="25" size="20" />
                    </div>
                </div>
			    <div class="personalRow accountText">
			        <div class="schoolCol1">
			            City:&nbsp;
     			    </div>
			        <div class="schoolCol2">
                       <asp:textbox id="txtCity3" runat="server" class="biginput" maxlength="25" size="20" />
                    </div>
                </div>
			    <div class="personalRow accountText">
			        <div class="schoolCol1">
			            School:&nbsp;
    			    </div>
			        <div class="schoolCol2">
                        <asp:textbox id="txtSchool3" runat="server" class="biginput" maxlength="80" size="20" />
                     </div>
               </div>
			    <div class="personalRow accountText">
			        <div class="schoolCol1">&nbsp;
    			    </div>
			        <div class="schoolCol2">                    
                        <asp:linkbutton id="btnRemoveSchool3" onclientclick="return confirmDelete();" oncommand="lbRemoveSchool_Click" commandargument="0" runat="server" >
			                <img alt="add" id="img5" style="border:none" runat="server" src="~/images/del_sm.gif" />&nbsp;Remove this School...
                        </asp:linkbutton>
                    </div>
                </div>
		    </div>
			<div id="trSchool4" runat="server" style="margin-bottom:20px">
			    <div class="personalRow accountText">
			        <div class="schoolCol1">
			            State, Province, District, Division:&nbsp;
    			    </div>
			        <div class="schoolCol2">
                        <asp:textbox id="txtState4" runat="server" class="biginput" maxlength="25" size="20" />
                    </div>
                </div>
			    <div class="personalRow accountText">
			        <div class="schoolCol1">
			            City:&nbsp;
    			    </div>
			        <div class="schoolCol2">
                        <asp:textbox id="txtCity4" runat="server" class="biginput" maxlength="25" size="20" />
                    </div>
               </div>
			    <div class="personalRow accountText">
			        <div class="schoolCol1">
			            School:&nbsp;
    			    </div>
			        <div class="schoolCol2">
                        <asp:textbox id="txtSchool4" runat="server" class="biginput" maxlength="80" size="20" />
                    </div>
                </div>
			    <div class="personalRow accountText">
			        <div class="schoolCol1">&nbsp;
    			    </div>
			        <div class="schoolCol2">                    
                        <asp:linkbutton id="btnRemoveSchool4" onclientclick="return confirmDelete();" oncommand="lbRemoveSchool_Click" commandargument="0" runat="server" >
			                <img alt="add" id="img6" style="border:none" runat="server" src="~/images/del_sm.gif" />&nbsp;Remove this School...
                        </asp:linkbutton>
                    </div>
                </div>
		    </div>
			<div id="trSchool5" runat="server" style="margin-bottom:20px">
			    <div class="personalRow accountText">
			        <div class="schoolCol1">
			            State, Province, District, Division:&nbsp;
    			    </div>
			        <div class="schoolCol2">
                        <asp:textbox id="txtState5" runat="server" class="biginput" maxlength="25" size="20" />
                    </div>
                </div>
			    <div class="personalRow accountText">
			        <div class="schoolCol1">
			            City:&nbsp;
    			    </div>
			        <div class="schoolCol2">
                        <asp:textbox id="txtCity5" runat="server" class="biginput" maxlength="25" size="20" />
                    </div>
                </div>
			    <div class="personalRow accountText">
			        <div class="schoolCol1">
			            School:&nbsp;
    			    </div>
			        <div class="schoolCol2">
                        <asp:textbox id="txtSchool5" runat="server" class="biginput" maxlength="80" size="20" />
                    </div>
                </div>
			    <div class="personalRow accountText">
 			        <div class="schoolCol1">&nbsp;
    			    </div>
			        <div class="schoolCol2">                    
                       <asp:linkbutton id="btnRemoveSchool5" onclientclick="return confirmDelete();" oncommand="lbRemoveSchool_Click" commandargument="0" runat="server" >
			                <img alt="add" id="img1" style="border:none" runat="server" src="~/images/del_sm.gif" />&nbsp;Remove this School...
                       </asp:linkbutton>
                    </div>
                </div>
		    </div>
			<div id="Div1" runat="server" >
			    <div class="personalRow accountText">
 			        <div class="schoolCol1">&nbsp;
    			    </div>
			        <div class="schoolCol2">                    
		                <a href="javascript:void(0);" id="lnkAddSchool" runat="server" onclick="addSchool(this);">
		                    <img alt="add" id="imgAdd_21" style="border:none" runat="server" src="~/images/plus_sm.gif" />
		                    Add another school...
		                </a>
                    </div>
                </div>
		    </div>
		    </ajax:ajaxpanel>   			
        </div>        
        <div id="communityURL">
            <div id="div2" style="MARGIN-TOP: 15px; COLOR: red" runat="server" visible="false"></div>
            <h4>Reserve Your Community URL</h4>
												            
            <!-- START CONTENT -->
			<span id="spnURLDesc" runat="server">This short URL allows you to promote your profile in blogs and email signatures with a simple URL that you define.<br/></span>
            <p>
			    <span id="spnRegistered" runat="server" visible="false"><b>Your short URL has been reserved. </b></span>
				<asp:label id="lblURL" runat="server" />
				<asp:textbox id="txtURL" style="width:260px" cssclass="biginput" maxlength="50" runat="server"/><span style="padding-left:10px"><asp:linkbutton id="btnRegister" onclick="btnRegister_Click" runat="Server" class="Filter2" causesvalidation="true" text=" Reserve URL "/></span>
				<asp:regularexpressionvalidator id="revURL" runat="server" text="The short URL should be at least four characters and can contain only letters, numbers and underscore." display="Dynamic" controltovalidate="txtURL"/>
            </p>
            <p>
                <div id="trURLRegister" runat="server">
				    <div>
				        <span class="warning">This short URL is permanent and cannot be changed after you reserve it.</span>
				    </div> 
				    <div id="divRegistrationErr" style="color:red;margin-top:15px;" runat="server" visible="false"></div>
				</div>
            </p>
            <!-- END CONTENT -->																			
       </div>
       <div id="submitArea">
            <asp:linkbutton id="btn1" runat="server" onclick="btnUpdate_Click" causesvalidation="true" cssclass="btnmedium orange" text="Save Changes"></asp:linkbutton>
			<asp:linkbutton id="btnCancel" onclick="btnCancel_Click" runat="Server" cssclass="btnxsmall orange" Text="Close" CausesValidation="False"></asp:linkbutton>
       </div>
    </div>
    <!--</ajax:AjaxPanel>-->
    
<div id="lightbox">
	<table width="100%" cellpadding="2" cellspacing="0" border="0">
		<tr valign="middle"><td class="titleLB"><div style="float:left;text-align:center;width:95%;">&nbsp;&nbsp;Did you mean to enter multiple interests?</div><div class="closeBtn" title=" Close " onclick="deactivateLB();">X</div></td></tr>
		<tr>
			<td align="center"><br />
				<table width="80%" height="95%" cellpadding="2" cellspacing="0" border="0">
					<tr valign="top">
						<td align="left" width="50%" valign="top">
							<span id="spnMultiple"></span><br />
						</td>
						<td width="20" rowspan="2"><img id="Img2" runat="server" src="~/images/spacer.gif" width="20" height="1" /></td>
						<td align="center" width="50%" valign="top">
							<span id="spnSingle"></span>
						</td>
					</tr>
					<tr valign="bottom">
						<td align="left">
							<ajax:ajaxpanel id="Ajaxpanel2" runat="server">
								<asp:button id="btnAddMultiple" onclick="btnAdd_Click" runat="server" text="Multiple Interests"/>
							</ajax:ajaxpanel>
						</td>
						<td align="center">
							<ajax:ajaxpanel id="Ajaxpanel3" runat="server">
							<asp:button id="btnAddSingle" onclick="btnAdd_Click" runat="server" text="Single Interest"/>
							</ajax:ajaxpanel>
						</td>
					</tr>
				</table><br />
			</td>
		</tr>
	</table>
</div>

<ajax:ajaxpanel id="Ajaxpanel1" runat="server">
<asp:button id="btnAdd" onclick="btnAdd_Click" runat="server" style="display:none;"/>
<asp:button id="btnRemove" onclick="btnRemove_Click" runat="server" style="display:none;"/>
<input type="hidden" value="0" id="hidIc_id" runat="server" />
<input type="hidden" value="" id="hidInterest" runat="server" />
<input type="hidden" value="0" id="hidInterestId" runat="server" />
</ajax:ajaxpanel>  
  
</asp:Content>


                      

