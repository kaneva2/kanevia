///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for incomeReport.
	/// </summary>
	public class incomeReport : SortedBasePage
	{
		protected incomeReport () 
		{
			Title = "Income Reports";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			// They must be logged in
			if (!Request.IsAuthenticated)
			{
				Response.Redirect (GetLoginURL ());
				return;
			}

			if (!IsPostBack)
			{
				LoadData ();
			}
		}

		/// <summary>
		/// LoadDAta
		/// </summary>
		private void LoadData ()
		{
			int userId = GetUserId ();

			// Total royalties earned, paid or not
			lblTotalRoyalty.Text = FormatCurrency (StoreUtility.ConvertKPointsToDollars (StoreUtility.GetTotalRoyaltiesEarned (userId)));

			// The paid royalties
			DataRow drIncomeTotal = StoreUtility.GetRoyaltyPaidTotals (userId);

			lblTotalPaidGross.Text = FormatCurrency (drIncomeTotal ["total_amount"]);
			lblTotalPaid.Text = FormatCurrency (drIncomeTotal ["amount_paid"]);
			lblTotalTax.Text = FormatCurrency (drIncomeTotal ["tax_withheld"]); 
			if (drIncomeTotal ["payment_date"].Equals (DBNull.Value))
			{
				lblTotalPaidDate.Text = "N/A";
			}
			else
			{
				lblTotalPaidDate.Text = FormatDate (drIncomeTotal ["payment_date"]);
			}

			BindData (1);


		}

		/// <summary>
		/// Bind the data
		/// </summary>
		/// <param name="pageNumber"></param>
		private void BindData (int pageNumber)
		{
			int ITEMS_PER_PAGE = 5;

			// Set the sortable columns
			SetHeaderSortText (dgrdPayments);
			string orderby = CurrentSort + " " + CurrentSortOrder;

			PagedDataTable pds = StoreUtility.GetPayments (GetUserId (), "", orderby, pageNumber, ITEMS_PER_PAGE);
			dgrdPayments.DataSource = pds;
			dgrdPayments.DataBind ();

			pgTop.NumberOfPages = Math.Ceiling ((double) pds.TotalCount / ITEMS_PER_PAGE).ToString ();
			pgTop.DrawControl ();

			// The results
			lblSearch.Text = GetResultsText (pds.TotalCount, pageNumber, ITEMS_PER_PAGE, pds.Rows.Count);
		}

		/// <summary>
		/// They clicked to sort a column
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected override void btnSort_Click (object sender, System.EventArgs e)
		{
			base.btnSort_Click (sender, e);
			BindData (pgTop.CurrentPageNumber);
		}

		/// <summary>
		/// Page Change
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pgTop_PageChange (object sender, PageChangeEventArgs e)
		{
			BindData (e.PageNumber);
		}

		/// <summary>
		/// DEFAULT_SORT
		/// </summary>
		/// <returns></returns>
		protected override string DEFAULT_SORT
		{
			get
			{
				return "period_end_date";
			}
		}

		protected PlaceHolder phBreadCrumb;
		protected Label lblSearch;
		protected Kaneva.Pager pgTop;
		protected DataGrid dgrdPayments;

		// Totals
		protected Label lblTotalRoyalty, lblTotalPaid, lblTotalPaidGross, lblTotalTax, lblTotalPaidDate;

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
