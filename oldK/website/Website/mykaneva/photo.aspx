<%@ Page language="c#" Codebehind="photo.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.photo" %>

<link href="../css/home.css" rel="stylesheet" type="text/css" />		
<link href="../css/kanevaSC.css" rel="stylesheet" type="text/css"/>
<link href="../css/friends.css" rel="stylesheet" type="text/css"/>
<link href="../css/kanevaText.css" type="text/css" rel="stylesheet">
<link href="../css/account.css" type="text/css" rel="stylesheet">
<link href="../css/new.css" type="text/css" rel="stylesheet">

<table cellpadding="0" cellspacing="0" width="990" align="center">
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="1" height="10" /></td>
	</tr>	
	<!--MAIN FRAME-->
	<tr>
		<td>
			<table border="0" cellpadding="0" cellspacing="0" align="left" width="100%" >									
				<tr>
					<td class="frTopLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
					<td class="frTop"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
					<td class="frTopRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
				</tr>
				<tr>
					<td class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" width="6" height="1" /></td>
					<td valign="top" class="frBgInt1" align="left" width="100%">
						<table cellpadding="0" cellspacing="0" align="left" width="90%">
							<tr>
								<td colspan="3"><img runat="server" src="~/images/spacer.gif" width="1" height="5" /></td>
							</tr>							
							<tr>
								<td width="5"><img runat="server" src="~/images/spacer.gif" width="5" height="1" /></td>
								<td>
								<!--inner frame-->
									<table cellspacing="0" cellpadding="0" border="0" align="left">			
										<tr>
											
											<td class="accountText" align="left">&nbsp;<b>Upload Personal Profile Photo</b></td>
										</tr>
										<tr>
											<td><img runat="server" src="~/images/spacer.gif" width="1" height="20" /></td>
										</tr>
										<tr>
											<td>
												<table cellpadding="0" cellspacing="0" align="left" border="0">
													<tr>
														<td><img runat="server" src="~/images/spacer.gif" width="20" height="1" /></td>
														<td width="150" align="center" valign="top">
															
															<div class="framesize-large">
															<div class="frame">
																<span class="ct"><span class="cl"></span></span>
																<div class="imgconstrain">
																				<img runat="server" id="imgAvatar" src="~/images/KanevaIcon01.gif" border="0"/>
															</div>
															<span class="cb"><span class="cl"></span></span>
																</div>
															</div>
															
														</td>
														<td><img runat="server" src="~/images/spacer.gif" width="40" height="1" /></td>
														<td valign="top">
															<table cellpadding="0" cellspacing="0" align="left">
																<tr>
																	<td class="accountText" align="left">Upload a photo that shows you at your best! More people view your items when you 
																		provide a personal photo.  The photo will be displayed on your profile and next to posts that you make.</td>
																</tr>
																<tr>
																	<td><img runat="server" src="~/images/spacer.gif" width="1" height="15" /></td>
																</tr>
																<tr>
																	<td class="accountText" align="left" valign="top">
																		Acceptable formats include JPG, JPEG or GIF photos under <asp:label runat="server" id="lblMaxSize"/> that meet the 
																		<a runat="server" href="~/overview/TermsAndConditions.aspx?subtab=trm" target="_resource">Terms & Conditions</a>.
																	</td>
																</tr>
																<tr>
																	<td><img runat="server" src="~/images/spacer.gif" width="1" height="15" /></td>
																</tr>
																<tr>
																	<td class="accountText" align="left" valign="top">Copyrighted photos are NOT allowed.<br/>
																		Users posting explicit photos will be banned.</td>
																</tr>																																		
																<tr>
																	<td><img runat="server" src="~/images/spacer.gif" width="1" height="15" id="Img1"/></td>
																</tr>
																<tr>
																	<td class="accountText"><div style="margin-bottom:8px;">We recommend 800x600.</div>
																		<input runat="server" class="Filter2" id="inpAvatar" type="file" size="55" onfocus="document.forms.frmMain.btnUpload.disabled=false;" onclick="document.forms.frmMain.btnUpload.disabled=false;" name="inpAvatar"/>
																		<div style="margin:10px 0 0 289px;">
																		<asp:button id="btnUpload" runat="Server" onclick="btnUpload_Click" class="Filter2" text=" Upload "/>
																		</div>
																	</td>
																</tr>
															</table>
														</td>
														
													</tr>
													<tr>
														<td colspan="4"><img runat="server" src="~/images/spacer.gif" width="1" height="10" /></td>
													</tr>
												</table>
											</td>
										</tr>
										
									</table>
								<!--end inner iframe--->									
								</td>
								<td width="5"><img runat="server" src="~/images/spacer.gif" width="5" height="1" /></td>
							</tr>
							<tr>
								<td colspan="3"><img runat="server" src="~/images/spacer.gif" width="1" height="5" /></td>
							</tr>								
						</table>
					</td>
					<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" width="6" height="1" /></td>
				</tr>
				<tr>
					<td class="frBottomLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
					<td class="frBottom"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
					<td class="frBottomRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
				</tr>
			</table>
		</td>
	</tr>
	<!--END MAIN FRAME--->							
</table>
	
	
<br><br>
