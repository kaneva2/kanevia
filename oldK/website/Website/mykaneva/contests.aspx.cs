///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
    public partial class contests : BasePage
    {
        protected void Page_Load (object sender, EventArgs e)
        {
            // Set Nav
            ((GenericPageTemplate) Master).HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
            ((GenericPageTemplate) Master).HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.NONE;
            ((GenericPageTemplate) Master).HeaderNav.SetNavVisible (((GenericPageTemplate) Master).HeaderNav.MyKanevaNav);

            if (!IsPostBack)
            {
                if (Request["contestId"] != null && Request["contestId"].ToString ().Length > 0)
                {
                    try
                    {
                        ShowContestDetails (Convert.ToUInt64 (Request["contestId"]));

                        // Request tracking
                        if (Request.Params[Constants.cREQUEST_TRACKING_URL_PARAM] != null)
                        {
                            int iResult = GetUserFacade.AcceptTrackingRequest(Request.Params[Constants.cREQUEST_TRACKING_URL_PARAM], KanevaWebGlobals.CurrentUser.UserId);
                        }

                        return;
                    }
                    catch { }
                }
                    
                ConfigureDisplay (DisplayPage.DEFAULT);
            }
        }

        private void ConfigureDisplay (DisplayPage page)
        {
            upTabs.Visible = false;
            upSearch.Visible = false;
            upSearchResults.Visible = false;
            upDetails.Visible = false;
            aBack.Visible = false;

            switch (page)
            {
                case DisplayPage.DETAILS:
                    {
                        upDetails.Visible = true;
                        aBack.Visible = true;
                        break;
                    }

                case DisplayPage.SEARCH_RESULTS:
                    {
                        upSearch.Visible = true;
                        upSearchResults.Visible = true;
                        aBack.Visible = true;
                        break;
                    }

                default:
                    {
                        upTabs.Visible = true;
                        upSearch.Visible = true;
                        break;
                    }
            }
        }

        private void ShowContestDetails (UInt64 contestId)
        {
            if (ucContestDetails.ShowContest (contestId))
            {
                ConfigureDisplay (DisplayPage.DETAILS);
            }
            else
            {
                ConfigureDisplay (DisplayPage.DEFAULT);
            }
        }

        private void ShowSearchResults ()
        {
            if (ucContestSearchResults.Search (txtSearch.Text))
            {
                ConfigureDisplay (DisplayPage.SEARCH_RESULTS);
            }
            else
            {
                ConfigureDisplay (DisplayPage.DEFAULT);
            }
        }

        protected void bntShowDetails_Click (object sender, EventArgs e)
        {
            UInt64 contestId = Convert.ToUInt64 (hidContestId.Value);
            ShowContestDetails (contestId);
        }

        protected void aBack_Click (object sender, EventArgs e)
        {
            ConfigureDisplay (DisplayPage.DEFAULT);
        }

        protected void bntSearch_Click (object sender, EventArgs e)
        {
            if (txtSearch.Text.Trim().Length < 3)
            {
                return;
            }

            ShowSearchResults ();
        }

        public static int CanUserEnterContest (UInt64 contestId, string currentUserIP, bool compareIP)
        {
            if (KanevaWebGlobals.CurrentUser.StatusId.Equals ((int) Constants.eUSER_STATUS.REGVALIDATED))
            {
                UserFame userFame = new FameFacade ().GetUserFame (KanevaWebGlobals.CurrentUser.UserId, (int) FameTypes.World);
                if (userFame.CurrentLevel.LevelNumber < Configuration.ContestMinimumFameLevelToEnter)
                {
                    return (int)eCONTEST_ACCESS.Below_Min_Fame_Level;
                }

                if (compareIP)
                {
                    if (new ContestFacade ().IsUserIPSameAsContestOwner (contestId, currentUserIP))
                    {
                        return (int)eCONTEST_ACCESS.IP_Match;
                    }
                }
                return (int)eCONTEST_ACCESS.Success;
            }
            return (int) eCONTEST_ACCESS.Email_Not_Validated;
        }

        public static int CanUserCreateContest
        {
            get
            {
                if (KanevaWebGlobals.CurrentUser.StatusId.Equals ((int) Constants.eUSER_STATUS.REGVALIDATED))
                {
                    UserFame userFame = new FameFacade().GetUserFame (KanevaWebGlobals.CurrentUser.UserId, (int) FameTypes.World);
                    if (userFame.CurrentLevel.LevelNumber < Configuration.ContestMinimumFameLevelToCreate)
                    {
                        return (int) eCONTEST_ACCESS.Below_Min_Fame_Level;    
                    }
                    return (int) eCONTEST_ACCESS.Success;
                }
                return (int) eCONTEST_ACCESS.Email_Not_Validated; ;
            }
        }

        private enum DisplayPage
        {
            DEFAULT = 1,
            DETAILS = 2,
            SEARCH_RESULTS = 3
        }

        public enum eCONTEST_ACCESS
        {
            Success = 0,
            Email_Not_Validated = 1,
            Below_Min_Fame_Level = 2,
            IP_Match = 3
        }
    }
}