<%@ Page language="c#" Codebehind="friendsPending.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.mykaneva.friendsPending" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../usercontrols/Pager.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="SearchFilter" Src="../usercontrols/SearchFilter.ascx" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>

<link href="../css/home.css" rel="stylesheet" type="text/css" />		
<link href="../css/kanevaSC.css" rel="stylesheet" type="text/css"/>
<link href="../css/friends.css" rel="stylesheet" type="text/css"/>
<link href="../css/kanevaText.css" type="text/css" rel="stylesheet">
<link href="../css/kaneva.css" type="text/css" rel="stylesheet">

<link href="../css/new.css" rel="stylesheet" type="text/css" />

<script src="../jscript/prototype.js" type="text/javascript" language="javascript"></script>
		
<table border="0" cellspacing="0" cellpadding="0"  width="990" align="center">
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" id="Img1"/></td>
	</tr>
	<tr>
		<td>
			<table  border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="frTopLeft"></td>
					<td class="frTop"></td>
					<td class="frTopRight"></td>
				</tr>
				<tr>
					<td class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1"/></td>
					<td valign="top" class="frBgIntMembers">
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr colspan="2">
								<td>
								
									<!-- TOP STATUS BAR -->
									<div id="pageheader">
										<table cellpadding="0" cellspacing="0" border="0" width="99%">
											<tr>
												<td align="left">
													<h1>Friend Requests Sent</h1>
												</td>
									
												<td align="right" valign="middle">
													<table cellpadding="0" cellspacing="0" border="0" width="690">
														<tr>
															<td>	
																<ajax:ajaxpanel id="ajpTopStatusBar" runat="server">
																<table cellpadding="0" cellspacing="0" border="0" width="100%">
																	<tr>
																		<td class="headertout" width="175">
																			<asp:label runat="server" id="lblSearch" />
																		</td>
																		<td class="searchnav" width="260"> Sort by: 
																			<asp:linkbutton id="lbSortByDate" runat="server" sortby="Date" onclick="lbSortBy_Click">Date Sent</asp:linkbutton> | 
																			<asp:linkbutton id="lbSortByName" runat="server" sortby="Name"  onclick="lbSortBy_Click">Name</asp:linkbutton>
																		</td>
																		<td align="left" width="130">
																			<kaneva:searchfilter runat="server" id="searchFilter" loc="top" hidethumbview="true" showfilterlists="false" assetsperpagelist="10,20,40" />
																		</td>
																		<td class="searchnav" align="right" width="210" nowrap>
																			<kaneva:pager runat="server" isajaxmode="True" id="pgTop" maxpagestodisplay="5" shownextprevlabels="true" />
																		</td>
																	</tr>
																</table>
																</ajax:ajaxpanel>
															</td>	
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</div>
									<!-- END TOP STATUS BAR -->		
								
								</td>
							</tr>
							<tr>
								<td width="968" align="left"  valign="top">
									<table cellpadding="0" cellspacing="0" border="0"  width="968">
										<tr>
											<td width="250" valign="top">
												<div class="module toolbar">
													<span class="ct"><span class="cl"></span></span>
													<h2>Tools & Actions <span class="hlink"></span></h2>
													<table cellspacing="0" cellpadding="10" border="0" width="99%">
														<tr>
															<td align="center">
																<table cellspacing="0" cellpadding="3" border="0" width="100%">
																	<tr>
																		<td>Resend selected:</td>
																		<td>
																			
																			<ajax:ajaxpanel id="ajpFriendGroups" runat="server">
																				<asp:imagebutton imageurl="~/images/button_resend.gif" alt="Resend" width="77" height="23" border=0 id="btnResend" vspace="2" runat="server" onclick="btnResend_Click"></asp:imagebutton>
																			</ajax:ajaxpanel>
																			
																		</td>
																	</tr>
																	<tr>
																		<td>Cancel Selected:</td>
																		<td>
																			
																			<ajax:ajaxpanel id="ajpCancelButton" runat="server">
																				<asp:imagebutton imageurl="~/images/button_cancel.gif" alt="Cancel" width="57" height="23" border="0" vspace="2" id="btnCancel" runat="server" onclick="btnCancel_Click"></asp:imagebutton>
																			</ajax:ajaxpanel>
																			
																		</td>
																	</tr>
																	<tr>
																		<td colspan="2" style="padding-top:20px;">
																			Friends who accepted are in your <a runat="server" href="~/mykaneva/friends.aspx">Friends List</a>. 
																		</td>
																	</tr>
																	<tr>
																		<td colspan="2" style="padding-top:20px;">
																			Resending Friend Requests is allowed every 14 days. 
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
													<span class="cb"><span class="cl"></span></span>
												</div>
											</td>
											<td width="20"><img runat="server" src="~/images/spacer.gif" width="20" height="1" id="Img3"/></td>
											<td width="698" valign="top" align="center">
												
												<ajax:ajaxpanel id="ajpFriendsList" runat="server">
												<div class="module whitebg">
													<span class="ct"><span class="cl"></span></span>
													<h2 class="alertmessage"><span id="spnAlertMsg" runat="server"></span></h2>
													
													<div align="left" style="padding-left:3px;">
													
													<asp:repeater id="rptFriends" runat="server">
														
														<headertemplate>	
															<table width="99%" cellspacing="0" cellpadding="5" border="0" class="data">
																<tr>
																	<th align="center" width="60" bgcolor="#fdfdfc" class="small">Select:<br><a href="javascript:void(0);" onclick="Select_All(true);">All</a> | <a href="javascript:void(0);" onclick="Select_All(false);">None</a></th>
																	<th bgcolor="#fdfdfc">Friend Request Profile</th>
																	<th align="center" bgcolor="#fdfdfc">Date Sent</th>
																	<th bgcolor="#fdfdfc">Request Status</th>
																</tr>
														</headertemplate>
														
														<itemtemplate>
														     															
															<tr class="altrow">
																<td align="center"><asp:checkbox runat="server" id="chkEdit" style="horizontal-align: right;" /></td>
																<td align="left">
																	<table cellpadding="0" cellspacing="0" border="0">
																		<tr>
																			<td>
																				<!-- FRIENDS BOX -->
																				<div class="framesize-xsmall">
																					<div class="frame">
																						<span class="ct"><span class="cl"></span></span>
																							<div class="imgconstrain" style="text-align:left;">
																								<a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "name_no_spaces").ToString ())%>>
																								<img runat="server" src='<%#GetProfileImageURL (DataBinder.Eval(Container.DataItem, "thumbnail_medium_path").ToString (), "sm", DataBinder.Eval(Container.DataItem, "gender").ToString ())%>' border="0"  width="45" height="33"/>
																								</a>
																							</div>
																						<span class="cb"><span class="cl"></span></span>
																					</div>
																				</div>		
																				<!-- END FRIENDS BOX -->
																				<input type="hidden" runat="server" id="hidUserId" value='<%#DataBinder.Eval(Container.DataItem, "user_id")%>' name="hidUserId">
																				<input type="hidden" runat="server" id="hidDateSent" value='<%#DataBinder.Eval(Container.DataItem, "request_date")%>' name="hidDateSent">
																			</td>
																			<td align="left"><p><a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "name_no_spaces").ToString ())%> ><%# DataBinder.Eval(Container.DataItem, "username") %></a></p></td>
																		</tr>
																	</table>
																	
																</td>
																<td align="center"><%# FormatDateOnly(DataBinder.Eval(Container.DataItem, "request_date")) %></td>
																<td align="center">
																	<p>Outstanding</p>
																	<asp:imagebutton imageurl="~/images/button_resend.gif" alt="Resend" width="77" height="23" border="0" vspace="2" id="btnResendMsg" commandname="cmdResend" runat="server"></asp:imagebutton>
																</td>
															</tr>
															
														</itemtemplate>
														
														<alternatingitemtemplate>
														     															
															<tr>
																<td align="center"><asp:checkbox runat="server" id="chkEdit" style="horizontal-align: right;" /></td>
																<td>
																	<table cellspadding="0" cellspacing="0" border="0">
																		<tr>
																			<td>
																				<!-- FRIENDS BOX -->
																				<div class="framesize-xsmall">
																					<div class="frame">
																						<span class="ct"><span class="cl"></span></span>
																							<div class="imgconstrain" style="text-align:left;">
																								<a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "name_no_spaces").ToString ())%>>
																								<img runat="server" src='<%#GetProfileImageURL (DataBinder.Eval(Container.DataItem, "thumbnail_medium_path").ToString (), "sm", DataBinder.Eval(Container.DataItem, "gender").ToString ())%>' border="0"  width="45" height="33"/>
																								</a>
																							</div>
																						<span class="cb"><span class="cl"></span></span>
																					</div>
																				</div>		
																			<!-- END FRIENDS BOX -->
																			<input type="hidden" runat="server" id="hidUserId" value='<%#DataBinder.Eval(Container.DataItem, "user_id")%>' name="hidUserId">
																			<input type="hidden" runat="server" id="hidDateSent" value='<%#DataBinder.Eval(Container.DataItem, "request_date")%>' name="hidDateSent">
																			</td>
																			<td><p><a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "name_no_spaces").ToString ())%> ><%# DataBinder.Eval(Container.DataItem, "username") %></a></p></td>
																		</tr>
																	</table>
																	
																</td>
																<td align="center"><%# FormatDateOnly(DataBinder.Eval(Container.DataItem, "request_date")) %></td>
																<td align="center">
																	<p>Outstanding</p>
																	<asp:imagebutton imageurl="~/images/button_resend.gif" alt="Resend" width="77" height="23" border="0" vspace="2" id="btnResendMsg" commandname="cmdResend" runat="server"></asp:imagebutton>
																</td>
															</tr>
															
														</alternatingitemtemplate>
														
														<footertemplate>
														
															</table>
															
														</footertemplate>
														
													</asp:repeater>
																											
													</div>
													
													<span class="cb"><span class="cl"></span></span>
												</div>
												</ajax:ajaxpanel>
												
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img5"/></td>
				</tr>
				<tr>
					<td class="frBottomLeft"></td>
					<td class="frBottom"></td>
					<td class="frBottomRight"></td>
				</tr>
				<tr>
					<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" id="Img6"/></td>
				</tr>
			</table>
		</td>
	</tr>
</table>

