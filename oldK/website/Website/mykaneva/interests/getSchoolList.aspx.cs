///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;

namespace KlausEnt.KEP.Kaneva
{
    public partial class getSchoolList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Return the schools list
            string strSchools = "";

            if (Request["sch"] != null)
            {
                strSchools = Request["sch"].ToString();
            }

            DataTable dtSchools = UsersUtility.GetSchools(strSchools, 30);

            Response.Clear();

            for (int i = 0; i < dtSchools.Rows.Count; i++)
            {
                Response.Write(dtSchools.Rows[i]["name"].ToString() + ";");
            }
        }
    }
}
