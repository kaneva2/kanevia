///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;

namespace KlausEnt.KEP.Kaneva
{
    public partial class getInterestList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Return the interests list
            int iCategory = 0;
            string strInterest = "";

            if (Request ["intr"] != null)
            {
                strInterest = Request["intr"].ToString();
            }

            if (Request["cat"] != null)
            {
                iCategory = Convert.ToInt32 (Request["cat"]);
            }

            DataTable dtInterests = UsersUtility.GetInterests(strInterest, iCategory, 30);
            
            Response.Clear ();

            for (int i = 0; i < dtInterests.Rows.Count; i++)
            {
                Response.Write(dtInterests.Rows[i]["interest"].ToString() + ";");
            }
        }
    }
}
