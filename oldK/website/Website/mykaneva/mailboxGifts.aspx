<%@ Page language="c#" Codebehind="mailboxGifts.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.mailboxGifts" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../usercontrols/Pager.ascx" %>

<link href="../css/home.css" rel="stylesheet" type="text/css" />		
<link href="../css/kanevaSC.css" rel="stylesheet" type="text/css"/>
<link href="../css/friends.css" rel="stylesheet" type="text/css"/>
<link href="../css/kanevaText.css" type="text/css" rel="stylesheet">

<table width="100%"  border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="1" height="15"></td>
	</tr>
</table>
	<table width="990" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td width="6" class="frTopLeft"></td>
			<td colspan="3" class="frTop"></td>
			<td width="7" class="frTopRight"></td>
		</tr>
		<tr> 
			<td rowspan="3" bgcolor="#f1f1f2" class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1"></td>
			<td width="10" rowspan="3" valign="top" bgcolor="#f1f1f2" class="frBgInt1"></td>
			<td height="40" valign="middle" class="frBgInt1">
				<!-- MESSAGE CENTER MENU  -->
				<table width="100%"  border="0" cellspacing="0" cellpadding="0">
				<tr>
				<td width="95"><img runat="server" src="~/images/message/titMessageCenter.gif" width="90" height="15"></td>
				<td width="200" align="left"></td>
				<td align="center">&nbsp;				  </td>
				<td align="right"></td>
				</tr>
				</table>
				<!-- END  MESSAGE CENTER MENU  -->
			</td>
			<td width="10" rowspan="3" valign="top" bgcolor="#f1f1f2" class="frBgInt1"></td>
			<td rowspan="3" class="frBorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1"></td>
		</tr>
		<tr>
			<td valign="top" bgcolor="#f1f1f2" class="frBgInt1">
		<!-- ACTION MENU -->
			<table width="100%" height="45" border="0" cellpadding="0" cellspacing="0" align="center">
				<tr>
					<td width="7" class="intBorderTopLeft"></td>
					<td class="intBorderTop"></td>
					<td width="7" class="intBorderTopRight"></td>
				</tr>
				<tr>
					<td class="intBorderBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1"></td>
					<td valign="middle" class="intBorderBgInt1">				  
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="70" align="center"><span class="insideTextNoBold">Actions</span></td>
								<td width="640" align="left">
									<asp:DropDownList ID="drpActions" CssClass="content" runat="server" style="width:250px" OnSelectedIndexChanged="drpAction_Change" AutoPostBack="True">
									</asp:DropDownList>
								</td>
								<td width="120" align="left" valign="middle">
									<a runat="server" href="~/myKaneva/newMessage.aspx"><img runat="server" src="~/images/buttons/sendMessage.gif" width="107" height="26" border="0"></a>
								</td>
							</tr>
						</table>
					</td>
				<td class="intBorderBorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1"></td>
			</tr>
			<tr>
				<td class="intBorderBottomLeft"></td>
				<td class="intBorderBottom"></td>
				<td class="intBorderBottomRight"></td>
			</tr>
		</table>
		<!-- END ACTION MENU -->
			<!-- IMAGE SPACER -->
			<table width="100%"  border="0" cellspacing="0" cellpadding="0">
			<tr>
			<td class="intBorderBgInt1"><img runat="server" src="~/images/spacer.gif" width="1" height="9"></td>
			</tr>
			</table>
			<!-- END IMAGE SPACER -->
			<!-- DISPLAY OPTIONS -->
			<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
				<tr>
					<td class="intBorderB1TopLeft"></td>
					<td class="intBorderB1Top"></td>
					<td class="intBorderB1TopRight"></td>
				</tr>
				<tr>
					<td class="intBorderB1BorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1"></td>
					<td valign="top" class="intBorderB1BgInt1">
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="115" align="center"><span class="insideTextNoBold"></span></td>
								<td>
									
								</td>
								<td width="120">&nbsp; </td>
								<td align="right"></td>
								<td align="center" width="10"></td>
								<td align="right">
									<asp:Label runat="server" id="lblSearch" CssClass="insideTextNoBold"/><img runat="server" src="~/images/spacer.gif" width="10" height="1">
									<Kaneva:Pager runat="server" id="pgTop"/>
								</td>
							</tr>
					</table>
				</td>
				<td class="intBorderB1BorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1"></td>
			</tr>
			<tr>
				<td class="intBorderB1BottomLeft"></td>
				<td class="intBorderB1Bottom"></td>
				<td class="intBorderB1BottomRight"></td>
			</tr>
		</table>
		<!-- END DISPLAY OPTIONS -->
		<!-- IMAGE SPACER -->
			<table width="100%"  border="0" cellspacing="0" cellpadding="0">
			<tr>
			<td class="intBorderBgInt1"><img runat="server" src="~/images/spacer.gif" width="1" height="2"></td>
			</tr>
			</table>
		<!-- END IMAGE SPACER -->
		<!-- PLACE OLDERS -->
			<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
				<tr>
					<td class="intBorderCTopLeft"></td>
					<td colspan="11" class="intBorderCTop"></td>
					<td class="intBorderCTopRight"></td>
				</tr>
				<!-- Header -->
				<tr>
					<td class="intBorderCBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1"></td>
					<td valign="middle" align="center" class="intBorderCBgInt1">
						<input type="checkbox" id="chkSelect" class="formKanevaCheck" onclick="javascript:SelectAll(this,this.checked);"/>
					</td>
					<td width="1" class="sepTit"><img runat="server" src="~/images/spacer.gif" width="1" height="1"></td>
					<td valign="top" class="intBorderCBgInt1">&nbsp;</td>
					<td width="1" class="sepTit"><img runat="server" src="~/images/spacer.gif" width="1" height="1"></td>
					<td valign="top" class="intBorderCBgInt1">&nbsp;</td>
					<td width="1" class="sepTit"><img runat="server" src="~/images/spacer.gif" width="1" height="1"></td>
					<td valign="middle" class="intBorderCBgInt1" align="left"><img runat="server" src="~/images/spacer.gif" width="15" height="1"><span class="content">From</span></td>
					<td width="1" class="sepTit"><img runat="server" src="~/images/spacer.gif" width="1" height="1"></td>
					<td valign="middle" class="intBorderCBgInt1" align="left"><img runat="server" src="~/images/spacer.gif" width="15" height="1"><span class="content">Subject</span></td>
					<td width="1" class="sepTit"><img runat="server" src="~/images/spacer.gif" width="1" height="1"></td>
					<td valign="middle" class="intBorderCBgInt1" align="left"><img runat="server" src="~/images/spacer.gif" width="15" height="1"><span class="content">Date</span></td>
					<td class="intBorderCBorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1"></td>
				</tr>
				<!-- End Header -->
				  
				<asp:Repeater runat="server" id="rptMailBox">  
					<ItemTemplate>
						<tr>
							<td rowspan="3" class="intBorderC1BorderLeft">&nbsp;</td>
							<td rowspan="3" align="center" class="intColor1">
								<asp:checkbox runat="server" id="chkEdit" style="horizontal-align: right;" />
									<input type="hidden" runat="server" id="hidMessageId" value='<%#DataBinder.Eval(Container.DataItem, "message_id")%>'>
								</td>
							<td rowspan="3" class="sepInt1"><img runat="server" src="~/images/spacer.gif" width="1" height="110" /></td>
							<td rowspan="3" align="center" valign="middle" class="intColor1">
								<img runat="server" src='<%#GetTypeImage (Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "type")), DataBinder.Eval(Container.DataItem, "to_viewable").ToString())%>'/>
							</td>
							<td rowspan="3" class="sepInt1"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
							<td rowspan="3" align="center" class="intColor1"><!--image holder-->
								<table border="0" cellpadding="0" cellspacing="0" width="94" height="94" align="center">
									<tr>
									<td class="boxWhiteTopLeft"><img runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
									<td class="boxWhiteTop" ><img runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
									<td class="boxWhiteTopRight"><img runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
									</tr>
									<tr>
									<td class="boxWhiteLeft" ><img runat="server" src="~/images/spacer.gif" width="1" height="4" /></td>
									<td class="boxWhiteContent" height="80" align="center" valign="middle">
										<a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "name_no_spaces").ToString ()) %>>
											<img runat="server" src='<%#GetProfileImageURL (DataBinder.Eval(Container.DataItem, "thumbnail_small_path").ToString ())%>' border="0"/>
										</a>
									</td>
									<td class="boxWhiteRight"><img runat="server" src="~/images/spacer.gif" width="1" height="4"/></td>
									</tr>
									<tr>
									<td class="boxWhiteBottomLeft" ><img runat="server" src="~/images/spacer.gif" width="4" height="4" /></td>
									<td class="boxWhiteBottom"><img runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
									<td class="boxWhiteBottomRight"><img runat="server" src="~/images/spacer.gif" width="4" height="4" /></td>
									</tr>
								</table>
								<!--end image holder-->
							</td>
							<td rowspan="3" class="sepInt1"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
							<td align="left" valign="top" class="intColor1"><br />
								<img runat="server" src="~/images/spacer.gif" width="15" height="8" />
									<a class="intColor1" href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "name_no_spaces").ToString ()) %>>
										<%#TruncateWithEllipsis (DataBinder.Eval(Container.DataItem, "username").ToString (), 20)%>
									</a>
								</td>
							<td rowspan="3" class="sepInt1"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
							<td align="left" valign="top" class="intColor1"><br />
								<span class="content"><img runat="server" src="~/images/spacer.gif" width="15" height="8" />
								<a runat="server" class=<%#GetMessageCssClass (DataBinder.Eval(Container.DataItem, "to_viewable"))%> title='<%# RemoveHTML (TruncateWithEllipsis (DataBinder.Eval(Container.DataItem, "message").ToString (), 100))%>' href='<%# "giftMessageDetails.aspx?messageId=" + DataBinder.Eval(Container.DataItem, "message_id").ToString ()%>'>
									<%# TruncateWithEllipsis (DataBinder.Eval(Container.DataItem, "subject").ToString (), 48)%>
								</a></span>
							</td>
							<td rowspan="3" class="sepInt1"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
							<td align="left" valign="top" class="intColor1"><br />
								<span class="insideTextNoBold"><img runat="server" src="~/images/spacer.gif" width="15" height="8" /><%#FormatDate (DataBinder.Eval(Container.DataItem, "message_date"))%></span></td>
							<td rowspan="3" class="intBorderC1BorderRight">&nbsp;</td>
						</tr>
							<tr>
							<td height="0" align="left" valign="top" class="intColor1">&nbsp;</td>
							<td height="0" align="left" valign="top" class="intColor1">&nbsp;</td>
							<td height="0" align="left" valign="top" class="intColor1">&nbsp;</td>
						</tr>
							<tr>
							<td height="2" align="left" valign="top" class="intColor1">&nbsp;</td>
							<td height="2" align="left" valign="top" class="intColor1">&nbsp;</td>
							<td height="2" align="left" valign="top" class="intColor1">&nbsp;</td>
						</tr>
					</ItemTemplate>
					<AlternatingItemTemplate>
						<tr>
							<td rowspan="3" class="intBorderC1BorderLeftColor2">&nbsp;</td>
							<td rowspan="3" align="center" class="intColor2">
								<asp:checkbox runat="server" id="chkEdit" style="horizontal-align: right;" />
									<input type="hidden" runat="server" id="hidMessageId" value='<%#DataBinder.Eval(Container.DataItem, "message_id")%>'>
								</td>
							<td rowspan="3" class="sepInt1"><img runat="server" src="~/images/spacer.gif" width="1" height="110" /></td>
							<td rowspan="3" align="center" valign="middle" class="intColor2">
								<img runat="server" src='<%#GetTypeImage (Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "type")), DataBinder.Eval(Container.DataItem, "to_viewable"))%>'/>
							</td>
							<td rowspan="3" class="sepInt1"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
							<td rowspan="3" align="center" class="intColor2"><!--image holder-->
								<table border="0" cellpadding="0" cellspacing="0" width="94" height="94" align="center">
									<tr>
									<td class="boxWhiteTopLeft"><img runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
									<td class="boxWhiteTop" ><img runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
									<td class="boxWhiteTopRight"><img runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
									</tr>
									<tr>
									<td class="boxWhiteLeft" ><img runat="server" src="~/images/spacer.gif" width="1" height="4" /></td>
									<td class="boxWhiteContent" height="80" align="center" valign="middle">
										<a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "name_no_spaces").ToString ()) %>>
											<img runat="server" src='<%#GetProfileImageURL (DataBinder.Eval(Container.DataItem, "thumbnail_small_path").ToString ())%>' border="0"/>
										</a>
									</td>
									<td class="boxWhiteRight"><img runat="server" src="~/images/spacer.gif" width="1" height="4"/></td>
									</tr>
									<tr>
									<td class="boxWhiteBottomLeft" ><img runat="server" src="~/images/spacer.gif" width="4" height="4" /></td>
									<td class="boxWhiteBottom"><img runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
									<td class="boxWhiteBottomRight"><img runat="server" src="~/images/spacer.gif" width="4" height="4" /></td>
									</tr>
								</table>
								<!--end image holder-->
							</td>
							<td rowspan="3" class="sepInt1"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
							<td align="left" valign="top" class="intColor2"><br />
								<img runat="server" src="~/images/spacer.gif" width="15" height="8" />
									<a class="intColor2" href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "name_no_spaces").ToString ()) %>>
										<%#TruncateWithEllipsis (DataBinder.Eval(Container.DataItem, "username").ToString (), 20)%>
									</a>
								</td>
							<td rowspan="3" class="sepInt1"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
							<td align="left" valign="top" class="intColor2"><br />
								<span class="content"><img runat="server" src="~/images/spacer.gif" width="15" height="8" />
								<a runat="server" class=<%#GetMessageCssClass (DataBinder.Eval(Container.DataItem, "to_viewable"))%> title='<%# RemoveHTML (TruncateWithEllipsis (DataBinder.Eval(Container.DataItem, "message").ToString (), 100))%>' href='<%# "giftMessageDetails.aspx?messageId=" + DataBinder.Eval(Container.DataItem, "message_id").ToString ()%>'>
									<%# TruncateWithEllipsis (DataBinder.Eval(Container.DataItem, "subject").ToString (), 48)%>
								</a></span>
							</td>
							<td rowspan="3" class="sepInt1"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
							<td align="left" valign="top" class="intColor2"><br />
								<span class="insideTextNoBold"><img runat="server" src="~/images/spacer.gif" width="15" height="8" /><%#FormatDate (DataBinder.Eval(Container.DataItem, "message_date"))%></span></td>
							<td rowspan="3" class="intBorderC1BorderRightColor2">&nbsp;</td>
						</tr>
							<tr>
							<td height="0" align="left" valign="top" class="intColor2">&nbsp;</td>
							<td height="0" align="left" valign="top" class="intColor2">&nbsp;</td>
							<td height="0" align="left" valign="top" class="intColor2">&nbsp;</td>
						</tr>
							<tr>
							<td height="2" align="left" valign="top" class="intColor2">&nbsp;</td>
							<td height="2" align="left" valign="top" class="intColor2">&nbsp;</td>
							<td height="2" align="left" valign="top" class="intColor2">&nbsp;</td>
						</tr>
					</AlternatingItemTemplate>
				</asp:Repeater>			
				<tr>
					<td class="intBorderC1BottomLeft"></td>
					<td colspan="11" class="intBorderC1Bottom"></td>
					<td class="intBorderC1BottomRight"></td>
				</tr>
			</table>
			
		</td>
	</tr>
	<tr>
		<td valign="top" bgcolor="#F1F1F2"><img runat="server" src="~/images/spacer.gif" width="1" height="10"></td>
	</tr>
	<tr>
		<td class="frBottomLeft"></td>
		<td colspan="3" class="frBottom"></td>
		<td class="frBottomRight"></td>
	</tr>
</table>