///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using System.Collections.Generic;
using log4net;

namespace KlausEnt.KEP.Kaneva
{
    public partial class billingInfoSummary : BasePage
    {
        #region Declarations

        private string _styleSheets = "\n<link href=\"../css/purchase_flow/billing_info_summary.css\" rel=\"stylesheet\" type=\"text/css\" />";
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private const int ADD_DATE = 0;
        private const int SUB_DATE = 1;

        #endregion

        private void Page_Load(object sender, System.EventArgs e)
		{
			if (!KanevaGlobals.EnableCheckout)
			{
				if (!IsAdministrator())
				{
                    Response.Redirect("~/error.aspx?error=" + (int)Constants.eERROR_TYPE.CHECKOUT_CLOSED);
                }
			}

			// They must be logged in
			if (!Request.IsAuthenticated)
			{
				Response.Redirect (GetLoginURL ());
				return;
			}

			if (!IsPostBack)
			{
                BindCurrentSubscriptions();
				BindBillingData ();
			}

            // Set Nav
            // Set Nav and style sheets
            ((IndexPageTemplate)Master).HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
            ((IndexPageTemplate)Master).HeaderNav.MyKanevaNav.ActiveTab =  NavMyKaneva.TAB.ACCOUNT;;
            ((IndexPageTemplate)Master).HeaderNav.MyAccountNav.ActiveTab = NavAccount.TAB.CREDITS; ;
            ((IndexPageTemplate)Master).HeaderNav.SetNavVisible(((IndexPageTemplate)Master).HeaderNav.MyKanevaNav, 2);
            ((IndexPageTemplate) Master).HeaderNav.SetNavVisible (((IndexPageTemplate) Master).HeaderNav.MyAccountNav, 3);

            ((IndexPageTemplate)Master).CustomCSS = _styleSheets;

        }


        #region Main Functions

        /// <summary>
        /// BindCurrentSubscriptions ()
        /// </summary>
        private void BindCurrentSubscriptions()
        {
            try
            {
                //get all active subscriptions
                List<UserSubscription> userSubscriptions = KanevaWebGlobals.CurrentUser.UserOwnedSubscriptions;
                rptSubscriptions.DataSource = userSubscriptions;
                rptSubscriptions.DataBind();

                //check to see if the user has VIP/AP combo
            }
            catch (Exception ex)
            {
                m_logger.Error("Error retrieving user subscriptions", ex);
            }
        }

        /// <summary>
        /// CancelSubscription
        /// </summary>
        private void CancelSubscription(string userSubscriptionId)
        {
            bool succeed = false;
            bool bSystemDown = false;
            string userErrorMessage = "";

            try
            {

                //hide the div
                whyCancel.Visible = false;

                //create the class instances
                SubscriptionFacade subFacade = new SubscriptionFacade();
                CybersourceAdaptor cybersourceAdaptor = new CybersourceAdaptor();

                //get the user subscription to be cancelled
                UserSubscription userSubscription = subFacade.GetUserSubscription(Convert.ToUInt32(userSubscriptionId));

                //logic to protect against users trying to hack a force cancel on grandfathered Access Passes
                //should be able to remove eventually
                //check for legacy and non renewing subscriptions
                bool autorenew = userSubscription.AutoRenew;
                string subscriptionIdentifier = userSubscription.SubscriptionIdentifier;
                if ((!autorenew) || (subscriptionIdentifier == null) || (subscriptionIdentifier.Length < 5))
                {
                    spnMessage.InnerText = "This subscription may not be cancelled.";
                    return;
                }


                //cancel the subscription the user requested cancelling
                userSubscription.UserCancelled = true;
                userSubscription.UserCancellationReason = tbx_CancelReason.Text;
                userSubscription.CancelledDate = DateTime.Now;

                if (userSubscription.IsLegacy)
                {
                    succeed = cybersourceAdaptor.CancelSubscription(ref bSystemDown, ref userErrorMessage, userSubscription);
                }
                else
                {
                    // Update DB
                    userSubscription.StatusId = Subscription.SubscriptionStatus.Cancelled;
                    subFacade.UpdateUserSubscription(userSubscription);

                    // No need to call cybersource
                    succeed = true;
                }
                
                if (succeed)
                {

                    //tickle the wok servers
                    (new RemoteEventSender()).BroadcastItemChangeEvent(RemoteEventSender.ObjectType.PlayerPassList, RemoteEventSender.ChangeType.UpdateObject, userSubscription.UserId);

                    //new confirm with redirect as requested by business
                    //bypasses the rebind 
                    string operation = "alert('Subscription successfully cancelled.');";
                    operation += "\n\r window.location.replace(\"" + this.ResolveUrl("~/mykaneva/buySpecials.aspx?pass=true") + "\");";
                    MagicAjax.AjaxCallHelper.Write(operation);

                    //run specialized logic to offer VIP/AP Combo users the option to keep/get one of the other passes
                    //covered under the VIPAP Combo
                    if (userSubscription.SubscriptionId == (uint)KanevaGlobals.VipPassGroupID)
                    {
                        ////get the subscription Id of the AP Pass
                        //Subscription apSubscription = subFacade.GetSubscriptionByPromotionId((uint)KanevaGlobals.AccessPassGroupID);

                        ////get the users access pass subscription
                        //UserSubscription usersAPSubscription = subFacade.GetUserSubscription((uint)apSubscription.SubscriptionId, (uint)GetUserId());

                        ////adjust the price back to the normal AP price
                        //usersAPSubscription.Price = apSubscription.Price;
                        //subFacade.UpdateUserSubscription(usersAPSubscription);

                        ////adjust the price on cyber source as well
                        //succeed = cybersourceAdaptor.UpdateSubscription(ref bSystemDown, ref userErrorMessage, usersAPSubscription);

                        //if (!succeed)
                        //{
                        //    //display success message
                        //    spnMessage.InnerText = userErrorMessage;
                        //    return;
                        //}
                    }
                }

                //rebind the data
                BindCurrentSubscriptions();
                BindBillingData();

                //display results message
                spnMessage.InnerText = succeed ? "Subscription successfully cancelled." : userErrorMessage;

            }

            catch (Exception ex)
            {
                m_logger.Error("Error trying to cancel cybersource subscription", ex);
                spnMessage.InnerText = "Error during cancellation of subscription.";
            }
            MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('spnMessage', 5000);");
        }

        /// <summary>
        /// BindBillingData ()
        /// </summary>
        protected void BindBillingData()
        {
            try
            {
                //get most recent address id
                int mostRecentAddress = UsersUtility.GetLastAddressId(GetUserId());

                if (mostRecentAddress > 0)
                {
                    DataRow drAddresses = UsersUtility.GetAddress(GetUserId(), mostRecentAddress);

                        // address fields
                        spnFullName.InnerHtml = (drAddresses["name"]).ToString();
                        spnAddress1.InnerHtml = (drAddresses["address1"]).ToString();
                        spnAddress2.InnerHtml = (drAddresses["address2"]).ToString();
                        spnCity.InnerHtml = (drAddresses["city"]).ToString() + ((drAddresses["city"]).ToString() != "" ? ", " : "");
                        spnState.InnerHtml = (drAddresses["state_name"]).ToString();
                        spnPostalCode.InnerHtml = (drAddresses["zip_code"]).ToString();
                        spnCountry.InnerHtml = (drAddresses["country_name"]).ToString();//((Country)WebCache.GetCountry(billInfo.Country)).Name;
                        spnPhoneNumber.InnerHtml = (drAddresses["phone_number"]).ToString().Length == 10 ? FormatPhoneNum((drAddresses["phone_number"]).ToString()) : (drAddresses["phone_number"]).ToString();
 
                }
                else  // No saved billing info
                {
                    spnAddress2.InnerHtml = "Address not entered";
                }
            }
            catch(Exception ex)
            {
                m_logger.Error("Error user billing info", ex);
            }
        }

        #endregion

        #region Helper Methods

        /// <summary>
        /// Grab the reasons to keep the subscription selected for cancellation
        /// </summary>
        private void DisplayReasons2Keep(uint userSubscriptionID)
        {
            try
            {
                SubscriptionFacade subfac = new SubscriptionFacade();
                uint subscriptionID = subfac.GetUserSubscription(userSubscriptionID).SubscriptionId;
                Subscription subscription2Cancel = WebCache.GetSubscription(subscriptionID);
                div_reasons2Keep.InnerHtml = HttpUtility.HtmlDecode(subscription2Cancel.Reasons2Keep);
            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// Format the phone number for display
        /// </summary>
        private string FormatPhoneNum (string num)
        {
            return num.Substring (0, 3) + "-" + num.Substring (3, 3) + "-" + num.Substring (6, 4);
        }

        /// <summary>
        /// GetSubcriptionLengthFromKanevaConstants
        /// </summary>
        /// <param name="subTermFrequency"></param>
        /// <returns></returns>
        public string ReturnTermAsString(Subscription.SubscriptionTerm subTermFrequency)
        {
            string displayTerm = "";
            switch ((int)subTermFrequency)
            {
                case (int)Subscription.SubscriptionTerm.BiMonthly:
                    displayTerm = "semi-monthly";
                    break;
                case (int)Subscription.SubscriptionTerm.BiWeekly:
                    displayTerm = "bi-week";
                    break;
                case (int)Subscription.SubscriptionTerm.Monthly:
                    displayTerm = "month";
                    break;
                case (int)Subscription.SubscriptionTerm.Weekly:
                    displayTerm = "week";
                    break;
                case (int)Subscription.SubscriptionTerm.Yearly:
                    displayTerm = "year";
                    break;
                case (int)Subscription.SubscriptionTerm.Quarterly:
                    displayTerm = "quarter";
                    break;
                case (int)Subscription.SubscriptionTerm.SemiAnnually:
                    displayTerm = "semi-annually";
                    break;
                case (int)Subscription.SubscriptionTerm.None:
                default:
                    break;
            }
            return displayTerm;
        }

        /// <summary>
        /// GetSubcriptionLengthFromKanevaConstants
        /// </summary>
        /// <param name="subTermFrequency"></param>
        /// <returns></returns>
        public DateTime AdjustByTermLength(Subscription.SubscriptionTerm termLength, int operation, DateTime baseDate)
        {
            DateTime newDate = baseDate;

            switch ((int)termLength)
            {
                case (int)Subscription.SubscriptionTerm.BiMonthly:
                    newDate = baseDate.AddMonths((operation == ADD_DATE ? 2 : -2));
                    break;
                case (int)Subscription.SubscriptionTerm.BiWeekly:
                    newDate = baseDate.AddDays((operation == ADD_DATE ? 14 : -14));
                    break;
                case (int)Subscription.SubscriptionTerm.Monthly:
                    newDate = baseDate.AddMonths((operation == ADD_DATE ? 1 : -1));
                    break;
                case (int)Subscription.SubscriptionTerm.Weekly:
                    newDate = baseDate.AddDays((operation == ADD_DATE ? 7 : -7));
                    break;
                case (int)Subscription.SubscriptionTerm.Yearly:
                    newDate = baseDate.AddYears((operation == ADD_DATE ? 1 : -1));
                    break;
                case (int)Subscription.SubscriptionTerm.Quarterly:
                    newDate = baseDate.AddMonths((operation == ADD_DATE ? 3 : -3));
                    break;
                case (int)Subscription.SubscriptionTerm.SemiAnnually:
                    newDate = baseDate.AddDays((operation == ADD_DATE ? 182 : -182));
                    break;
                case (int)Subscription.SubscriptionTerm.None:
                default:
                    break;
            }

            return newDate;
        }


        #endregion 

        #region Event Handlers

        public void Cancel_Command(Object sender, CommandEventArgs e)
        {
            //select the appropriate action
            switch (e.CommandName)
            {
                case "startCancel":
                    whyCancel.Visible = true;
                    DisplayReasons2Keep(Convert.ToUInt32(e.CommandArgument));
                    lbn_CancelSubscription.CommandArgument = e.CommandArgument.ToString();
                    break;
                case "cancelCancel":
                    whyCancel.Visible = false;
                    break;
                case "excuteCancel":
                    //get which payment method was selected and process
                    CancelSubscription(e.CommandArgument.ToString());
                    break;
            }

        }

        private void rptSubscriptions_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                
                //get name of subscription
                uint suscriptionId = Convert.ToUInt32(DataBinder.Eval(e.Item.DataItem, "SubscriptionId"));

                //set the value in the control
                Label lbl_BillingPeriod = (Label)e.Item.FindControl("lbl_BillingPeriod");
                Label lbl_LastPayment = (Label)e.Item.FindControl("lblLastPayment");
                Label lbl_SubscriptionName = (Label)e.Item.FindControl("lblSubscriptionName");
                Label lbl_NextPayment = (Label)e.Item.FindControl("lblNextPayment");
                LinkButton lbnSelectSubscForRemoval = (LinkButton)e.Item.FindControl("lbn_SelectSubscForRemoval");

                //catch error incase this fails so the other data will still display
                try
                {
                    Subscription subscription = (new SubscriptionFacade()).GetSubscription(suscriptionId);
                    lbl_SubscriptionName.Text = subscription.Name;
                }
                catch (Exception ex)
                {
                    m_logger.Error("Error retrieving subscription data", ex);
                }

                DateTime endDateNextPayment = Convert.ToDateTime(DataBinder.Eval(e.Item.DataItem, "EndDate"));
                Subscription.SubscriptionTerm billingPeriod = (Subscription.SubscriptionTerm) Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "RenewalTerm"));

                //set the billing period
                lbl_BillingPeriod.Text = ReturnTermAsString(billingPeriod);
                if (Convert.ToDateTime(DataBinder.Eval(e.Item.DataItem, "FreeTrialEndDate")) >= DateTime.Now)
                {
                    lbl_LastPayment.Text = "Free Trial Period";
                }
                else
                {
                    lbl_LastPayment.Text = FormatDate(AdjustByTermLength(billingPeriod, SUB_DATE, endDateNextPayment));
                }

                //additional logic to handle if an subscription has been cancelled but is still active (end date not reached)
                int subscriptionStatusId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "StatusId"));
                if (subscriptionStatusId == (int)Subscription.SubscriptionStatus.Cancelled)
                {
                    lbl_NextPayment.Text = "Valid Through";
                    lbnSelectSubscForRemoval.Visible = false;
                }
                else
                {
                    lbl_NextPayment.Text = "Next Payment";
                    lbnSelectSubscForRemoval.Visible = true;
                }

                //check for legacy and non renewing subscriptions
                bool autorenew = Convert.ToBoolean(DataBinder.Eval(e.Item.DataItem, "AutoRenew"));
                string subscriptionIdentifier = DataBinder.Eval(e.Item.DataItem, "SubscriptionIdentifier").ToString();
                if ((!autorenew) || (subscriptionIdentifier == null) || (subscriptionIdentifier.Length < 5))
                {
                    lbnSelectSubscForRemoval.Visible = false;
                }
            }
            catch (Exception ex)
            {
                m_logger.Error("Error retrieving user subscriptions", ex);
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            this.rptSubscriptions.ItemDataBound += new System.Web.UI.WebControls.RepeaterItemEventHandler(this.rptSubscriptions_ItemDataBound);
        }
        #endregion

        #endregion
    }
}
