///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for dashboard.
	/// </summary>
	public class dashboard : MainTemplatePage
	{
		protected dashboard () 
		{
			Title = "Dashboard";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			// They must be logged in
			if (!Request.IsAuthenticated)
			{
				Response.Redirect (GetLoginURL ());
				return;
			}

			if (!IsPostBack)
			{
                User user = KanevaWebGlobals.CurrentUser;

				// Mailbox
				BindMessageData (user);

				// Channels
				BindChannelData (user);

				// Friends
                BindFriendData(user.UserId);

				// Account
				BindAccountData(user);

				// Media Stats
				BindMediaStats(user);

				// My Media
                BindMyMediaData(user.UserId, user.CommunityId);

				// Contributions
                BindContributionData(user);
			}
			
			// Set Nav
			HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
			HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.ACCOUNT;
			HeaderNav.MyAccountNav.ActiveTab = NavAccount.TAB.STATS;
			HeaderNav.SetNavVisible(HeaderNav.MyKanevaNav,2);
            HeaderNav.SetNavVisible (HeaderNav.MyAccountNav, 3);

		}

	
		#region Helper Methods
		
		// Messages
		private void BindMessageData (User user)
		{
			PagedDataTable pds = UsersUtility.GetUserMessages (user.UserId, "", "message_date DESC", 1, 5);
			rptMailBox.DataSource = pds;
			rptMailBox.DataBind ();

			lblMailboxNoResults.Text = GetResultsText (pds.TotalCount, 1, 5, pds.Rows.Count, true);

            lblPMCount.Text = user.Stats.NumberOfInboxMessages.ToString();
            lblRequestCount.Text = user.Stats.NumberOfRequests.ToString();
		}

		// Channels
		private void BindChannelData (User user)
		{
            lblCommOwn.Text = user.Stats.CommunitiesIOwn.ToString();
            lblCommModerator.Text = user.Stats.CommunitiesIModerate.ToString();
            lblCommBelong.Text = user.Stats.CommunitiesIBelongTo.ToString();
				
			aIOwn.HRef = ResolveUrl ("~/bookmarks/channelTags.aspx");
			aIMod.HRef = ResolveUrl ("~/bookmarks/channelTags.aspx");
			aIBelong.HRef = ResolveUrl ("~/bookmarks/channelTags.aspx");

            lblPendingComm.Text = CommunityUtility.GetPendingCommunitiesForUser(user.UserId).ToString();
            lblPendingCommMembers.Text = CommunityUtility.GetPendingCommunityMembersForUser(user.UserId).ToString();

			// personal channel
			lblPCUserName.Text = user.Username;
			imgAvatar.Src = UsersUtility.GetProfileImageURL (user.ThumbnailSmallPath, "sm", user.Gender);
            imgAvatar.Alt = user.Username;

			// using previously retrieved value for num friends
			lblPCNumberOfFriends.Text = lblCurrentFriends.Text;
            DataRow drChannelStats = CommunityUtility.GetTotalChannelStats(user.UserId, (int)CommunityMember.CommunityMemberAccountType.OWNER);
			if ( drChannelStats != null )
			{
				lblPCNumberOfDiggs.Text		= drChannelStats["number_of_diggs"].ToString();
				lblPCNumberTimesShared.Text	= drChannelStats["number_times_shared"].ToString();
				lblPCNumberOfViews.Text		= drChannelStats["number_of_views"].ToString();
			}


            PagedDataTable pdsChannels = UsersUtility.GetUserCommunities(user.UserId, "cm.account_type_id ASC, cs.number_of_members DESC", "", 1, 4);
			repeaterChannels.DataSource = pdsChannels;
			repeaterChannels.DataBind ();
		}
		// Friends
		private void BindFriendData (int userId)
		{
            UserFacade userFacade = new UserFacade();

            dlFriends.DataSource = userFacade.GetFriends(userId, "", "", 1, 10); ;
			dlFriends.DataBind ();

			lblCurrentFriends.Text = KanevaWebGlobals.CurrentUser.Stats.NumberOfFriends.ToString ();
            lblPendingFriends.Text = Convert.ToString(KanevaWebGlobals.CurrentUser.Stats.NumberOfRequests + KanevaWebGlobals.CurrentUser.Stats.NumberOfPending);
		}

		// My Account
		private void BindAccountData(User user)
		{
			// Income
            lblKPoint.Text = KanevaGlobals.FormatKPoints(UsersUtility.getUserBalance(user.UserId, Constants.CURR_KPOINT) + UsersUtility.getUserBalance(user.UserId, Constants.CURR_MPOINT), true, false);

			// Participation
			lblUniqueVisitors.Text = CommunityUtility.GetUniqueVistors (user.CommunityId).ToString ();
            lblTraffic.Text = FormatImageSize(UsersUtility.GetTotalTraffic(user.UserId).ToString());
            lblShares.Text = user.NumberTimesShared.ToString();
            lblScore.Text = user.NumberOfDiggs.ToString(); ;
		}

		// Media Stats
		private void BindMediaStats(User user)
		{
			// My Media Stats
				
			// Set the sortable columns
			//SetHeaderSortText (dgrdItemsSold);
			//			string orderby = CurrentSort + " " + CurrentSortOrder;
			
			DateTime startMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);

			DataTable dtMonthTotals = new DataTable ();
			dtMonthTotals.Columns.Add (new DataColumn ("purchase_month", System.Type.GetType("System.DateTime") ));
			dtMonthTotals.Columns.Add (new DataColumn ("month", System.Type.GetType("System.String") ));
			dtMonthTotals.Columns.Add (new DataColumn ("year", System.Type.GetType("System.String") ));
			dtMonthTotals.Columns.Add (new DataColumn ("items_sold", System.Type.GetType("System.Int32") ));
			dtMonthTotals.Columns.Add (new DataColumn ("total_royalty_amount", System.Type.GetType("System.Double") ));

			int numMonthShowing = 6;
			DateTime signUpMonth = DateTime.MinValue;

			signUpMonth =  user.SignupDate;
			signUpMonth = new DateTime(signUpMonth.Year, signUpMonth.Month, 1);

			for (int i = 0; i < numMonthShowing && startMonth.CompareTo(signUpMonth) >= 0 ; i++)
			{
				AddMonthlyTotal (startMonth, dtMonthTotals);
				startMonth = startMonth.AddMonths(-1);
			}

			rptItemsSold.DataSource = dtMonthTotals;
			rptItemsSold.DataBind ();
		}
		// My Media
		private void BindMyMediaData(int userId, int channelId)
		{
			lblPubSize.Text = KanevaGlobals.FormatImageSize (UsersUtility.GetDiskUsage (userId));

            DataRow drMediaCount = StoreUtility.GetChannelMediaCounts(channelId);

            string sumTotal = (Convert.ToUInt32(drMediaCount["sum_video"]) +
                Convert.ToUInt32(drMediaCount["sum_picture"]) +
                Convert.ToUInt32(drMediaCount["sum_music"]) +
                Convert.ToUInt32(drMediaCount["sum_game"])).ToString ();  
                //Convert.ToUInt32(drMediaCount["sum_pattern"]) +
                //Convert.ToUInt32(drMediaCount["sum_widget"]) +
                //Convert.ToUInt32(drMediaCount["sum_tv"])).ToString();

			lblVideoCount.Text = drMediaCount ["sum_video"].ToString ();
			lblPhotoCount.Text = drMediaCount ["sum_picture"].ToString ();
			lblMusicCount.Text =  drMediaCount ["sum_music"].ToString ();
			lblGamesCount.Text = drMediaCount ["sum_game"].ToString ();
            lblItemsForSale.Text = sumTotal.ToString();
            lblItemsSold.Text = StoreUtility.GetItemsSoldForUser(userId, "oi.asset_id IS NOT NULL").ToString();

			// Bandwidth for last month
			DateTime dtMonth = GetMonth ();
			PagedDataTable ItemsThisMonth = StoreUtility.GetSoldItemsForMonth (userId, dtMonth, "", 1, Int32.MaxValue);
			long size = 0;
			for (int i = 1; i < ItemsThisMonth.TotalCount; i++)
			{
				size = size + Convert.ToInt32(ItemsThisMonth.Rows [i]["file_size"])* Convert.ToInt32(ItemsThisMonth.Rows [i]["count"]);				
			}
			lblBandwidth.Text = KanevaGlobals.FormatImageSize(size);

            
				
			DataRow drLastItemSold = StoreUtility.GetLastItemSoldForUser (userId);
			if (drLastItemSold == null)
			{
				lblItemSold.Text = "none";
			}
			else
			{
				lblItemSold.Text = TruncateWithEllipsis (drLastItemSold ["name"].ToString (), 35);
			}
		}

		// Contributions
        private void BindContributionData(User user)
		{
            lblBlogEntry.Text = user.Stats.NumberOfBlogs.ToString(); 
            lblComments.Text = user.Stats.NumberOfComments.ToString(); 
            lblAssetDiggs.Text = user.Stats.NumberOfAssetDiggs.ToString(); 
            lblPosts.Text = user.Stats.NumberForumPosts.ToString();
            lblLogins.Text = user.Stats.NumberOfLogins.ToString(); 
            lblMyProfile.Text = user.NumberOfViews.ToString(); 
		}

		/// <summary>
		/// GetStyle for messages
		/// </summary>
		/// <param name="theType"></param>
		/// <returns></returns>
		protected string GetStyle (int theType)
		{
			switch (theType)
			{
				case (int) Constants.eMESSAGE_TYPE.PRIVATE_MESSAGE:
				{
					return "color:black;";
				}
				case (int) Constants.eMESSAGE_TYPE.ALERT:
				{
					return "color:red;";
				}
//				case (int) Constants.eMESSAGE_TYPE.FRIEND_REQUEST:
//				{
//					return "color:green;";
//				}
//				case (int) Constants.eMESSAGE_TYPE.MEMBER_REQUEST:
//				{
//					return "color:green;";
//				}
				default:
				{
					return "color:black;";
				}
			}
		}

		/// <summary>
		/// GetMonthlyTotal
		/// </summary>
		/// <param name="dtMonth"></param>
		/// <returns></returns>
		private void AddMonthlyTotal (DateTime dtSearch, DataTable dtMonthTotals)
		{
			DataRow drResult = StoreUtility.GetMonthlyTotal (GetUserId (), dtSearch);

			if (drResult != null)
			{
				DataRow drMonthTotal = dtMonthTotals.NewRow ();
				drMonthTotal ["purchase_month"] = dtSearch;
				drMonthTotal ["month"] = dtSearch.ToString ("MMMM");
				drMonthTotal ["year"] = dtSearch.ToString ("yyyy");;
				drMonthTotal ["items_sold"] = Convert.ToInt32 (drResult ["items_sold"]);
				drMonthTotal ["total_royalty_amount"] = StoreUtility.ConvertKPointsToDollars (Convert.ToDouble (drResult ["total_royalty_amount"]));
				dtMonthTotals.Rows.Add (drMonthTotal);
				drMonthTotal.AcceptChanges ();
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="dtDate"></param>
		/// <returns></returns>
		protected string GetItemsSoldLink (DateTime dtDate)
		{
			return ResolveUrl ("~/mykaneva/incomeItemsSold.aspx?subsubtab=monthly&month=" + dtDate.Month + "&year=" + dtDate.Year);
		}
		/// <summary>
		/// GetMonth
		/// </summary>
		/// <returns></returns>
		private DateTime GetMonth ()
		{
			if (Request ["year"] == null || Request ["month"] == null)
			{
				return KanevaGlobals.GetCurrentDateTime ();
			}
			else
			{
				return new DateTime (Convert.ToInt32 (Request ["year"]), Convert.ToInt32 (Request ["month"]), 1);
			}

		}

		
		#endregion
		
		#region Declerations
		protected PlaceHolder phBreadCrumb;
		
		// Mailbox
		protected Repeater	rptMailBox;
		protected Label		lblMailboxNoResults, lblPMCount, lblRequestCount;

		// Store
		protected Label		lblItemsForSale, lblItemsSold, lblItemSold, lblItemSoldDate, lblPubSize, lblBandwidth;
		protected Label		lblLastSubDate;
		protected Label		lblVideoCount, lblPhotoCount, lblMusicCount, lblGamesCount;

		// Friends
		protected Label		lblCurrentFriends, lblPendingFriends, lblExFriends;
		protected DataList	dlFriends;

		// Income
		protected Label		lblKPoint;

		// News
		protected DataGrid	dgrdNews;

		// My Media Stats
		protected Repeater	rptItemsSold;

		// Participation
		protected Label		lblUniqueVisitors, lblTraffic, lblShares, lblScore;
		protected Label		lblBlogEntry, lblComments, lblAssetDiggs, lblPosts, lblLogins, lblMyProfile;

		// Communities
		protected Label			lblCommOwn, lblCommModerator, lblCommBelong, lblPendingComm, lblPendingCommMembers;
		protected HtmlAnchor	aIOwn, aIMod, aIBelong;
		protected Repeater		repeaterChannels;
		protected HtmlImage		imgAvatar;

		protected	Label		lblPCUserName;
		protected	Label		lblPCNumberOfFriends;
		protected	Label		lblPCNumberOfDiggs;
		protected	Label		lblPCNumberTimesShared;
		protected	Label		lblPCNumberOfViews;
		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
