<%@ Page language="c#" MasterPageFile="~/masterpages/GenericPageTemplate.Master" ValidateRequest="False" Codebehind="inviteFriend.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.inviteFriend" %>
<%@ Register TagPrefix="uc" TagName="ImportContacts" Src="~/usercontrols/ImportContacts.ascx" %>

<asp:Content ID="cnt_ProfileHead" runat="server" ContentPlaceHolderID="cph_HeadData">
<link href="../css/new.css" type="text/css" rel="stylesheet">
<style>
</style>
<script>function OnLoad() {return;}</script>
</asp:content>


<asp:Content ID="cntUpload" runat="server" ContentPlaceHolderID="cph_Body">
	<div id="thetop"></div>
	<div id="feedback" class="feedback hidden"><p></p></div>
												
	<div class="invitecontainer">

		<h1 class="title" id="h1Title" onclick="showMessage('this is the test message');">Invite Your Friends To Kaneva</h1>
		<hr />
		<ul class="steps" >
			<li id="menu_step1" class="step1 active"><p>Step 1<br /><span>Choose Service</span></p></li>
			<li id="menu_step2" class="step2"><p>Step 2<br /><span>Find Friends</span></p></li>
			<li id="menu_step3" class="step3"><p>Step 3<br /><span>Invite Friends</span></p></li>
		</ul>
		<hr />

		<div class="stepcontainer">
				
			<!-- Invite via Email Section -->
			<uc:importcontacts id="ucImportContacts" runat="server"></uc:importcontacts>

		</div>
	
	</div>
						

<script>
	function step(num) {
		$j('#menu_step1').removeClass('active');
		$j('#menu_step2').removeClass('active');
		$j('#menu_step3').removeClass('active');
		$j('#menu_step' + num).addClass('active');
	}
</script>

</asp:Content>



																						