<%@ page language="c#" codebehind="history.aspx.cs" autoeventwireup="false" inherits="KlausEnt.KEP.Kaneva.history" %>

<%@ register tagprefix="Kaneva" tagname="Pager" src="../usercontrols/Pager.ascx" %>
<link href="../css/home.css" rel="stylesheet" type="text/css" />
<link href="../css/kanevaSC.css" rel="stylesheet" type="text/css" />
<link href="../css/friends.css" rel="stylesheet" type="text/css" />
<link href="../css/kanevaText.css" type="text/css" rel="stylesheet">
<link id="styleSheet" rel="stylesheet" href="../css/new.css" type="text/css" />

			<table border="0" cellpadding="0" cellspacing="0" class="newcontainer" align="center">
				<tr>
					<td valign="top">
						<table border="0" cellpadding="0" cellspacing="0" width="100%" class="newcontainerborder">
							<tr>
								<td class=""></td>
								<td class=""></td>
								<td class=""></td>
							</tr>
							<tr>
								<td class="">
									<img runat="server" src="~/images/spacer.gif" width="1" height="1" />
								</td>
								<td valign="top" class="newdatacontainer">

									<div style="width:100%;text-align:right;margin-bottom:14px;font-size:11px;">
									<span style="margin-right:20px;">
										<asp:label runat="server" id="lblSearch" />
									</span>
									<img id="Img1" runat="server" src="~/images/spacer.gif" width="6" height="8" /><kaneva:pager runat="server" id="pgTop" />
									</div>

									<table border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr>
											<td class="boxInsideTopLeft">
												<img id="Img2" runat="server" src="~/images/spacer.gif" width="4" height="4" />
											</td>
											<td class="boxInsideTop">
												<img id="Img3" runat="server" src="~/images/spacer.gif" width="4" height="1" />
											</td>
											<td class="boxInsideTopRight">
												<img id="Img4" runat="server" src="~/images/spacer.gif" width="4" height="4" />
											</td>
										</tr>
										<tr class="boxInside">
											<td class="boxInsideleft">
												<img id="Img5" runat="server" src="~/images/spacer.gif" width="1" height="4" />
											</td>
											<td align="center" class="boxInsideContent">
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td width="6" height="6">
															<img id="Img6" runat="server" src="~/images/spacer.gif" width="6" height="6" />
														</td>
														<td width="6" height="6">
															<img id="Img7" runat="server" src="~/images/spacer.gif" width="6" height="6" />
														</td>
														<td width="6" height="6">
															<img id="Img8" runat="server" src="~/images/spacer.gif" width="6" height="6" />
														</td>
													</tr>
													<tr>
														<td width="6" height="6">
															<img id="Img9" runat="server" src="~/images/spacer.gif" width="6" height="6" />
														</td>
														<td width="100%">
															<table width="100%" border="0" align="letf" cellpadding="0" cellspacing="0">
																<tr>
																	<td width="7" class="intBorderCTopLeft">
																		<img id="Img10" runat="server" src="~/images/spacer.gif" width="6" height="6" />
																	</td>
																	<td colspan="3" class="intBorderCTop">
																		<img id="Img11" runat="server" src="~/images/spacer.gif" width="200" height="7" />
																	</td>
																	<td width="1" class="intBorderCTop">
																		<img id="Img12" runat="server" src="~/images/spacer.gif" width="1" height="7" />
																	</td>
																	<td class="intBorderCTop">
																		<img id="Img13" runat="server" src="~/images/spacer.gif" width="100" height="7" />
																	</td>
																	<td width="1" class="intBorderCTop">
																		<img id="Img14" runat="server" src="~/images/spacer.gif" width="1" height="7" />
																	</td>
																	<td class="intBorderCTop">
																		<img id="Img15" runat="server" src="~/images/spacer.gif" width="300" height="7" />
																	</td>
																	<td width="7" class="intBorderCTopRight">
																		<img id="Img16" runat="server" src="~/images/spacer.gif" width="6" height="6" />
																	</td>
																</tr>
																<tr align="left">
																	<td class="intBorderCBorderLeft">
																		<img id="Img17" runat="server" src="~/images/spacer.gif" width="6" height="2" />
																	</td>
																	<td colspan="3" class="intBorderCBgInt1">
																		<table width="80%" border="0" cellspacing="0" cellpadding="0">
																			<tr>
																				<td width="15" height="7">
																					<img id="Img18" runat="server" src="~/images/spacer.gif" width="16" height="7" />
																				</td>
																				<td width="97%">
																					<span class="content">Item</span>
																				</td>
																			</tr>
																		</table>
																	</td>
																	<td width="1" class="sepTit1">
																		<img id="Img19" runat="server" src="~/images/spacer.gif" width="1" height="8" />
																	</td>
																	<td width="280" class="intBorderCBgInt1">
																		<table width="80%" border="0" cellspacing="0" cellpadding="0">
																			<tr>
																				<td width="15" height="7">
																					<img id="Img20" runat="server" src="~/images/spacer.gif" width="15" height="7" />
																				</td>
																				<td width="332">
																					<span class="content">View Date</span>
																				</td>
																			</tr>
																		</table>
																	</td>
																	<td width="1">
																		<img id="Img21" runat="server" src="~/images/spacer.gif" width="1" height="8" />
																	</td>
																	<td width="300" class="intBorderCBgInt1">
																		<table width="80%" border="0" cellspacing="0" cellpadding="0">
																			<tr>
																				<td width="15" height="7">
																				</td>
																				<td width="97%">
																				</td>
																			</tr>
																		</table>
																	</td>
																	<td class="intBorderCBorderRight">
																		&nbsp;
																	</td>
																</tr>
																<tr>
																	<td class="intBorderCBottomLeft">
																		<img id="Img22" runat="server" src="~/images/spacer.gif" width="7" height="7" />
																	</td>
																	<td colspan="3" class="intBorderCBottom">
																		<img id="Img23" runat="server" src="~/images/spacer.gif" width="7" height="7" />
																	</td>
																	<td width="1" class="intBorderCBottom">
																		<img id="Img24" runat="server" src="~/images/spacer.gif" width="1" height="7" />
																	</td>
																	<td class="intBorderCBottom">
																		<img id="Img25" runat="server" src="~/images/spacer.gif" width="7" height="7" />
																	</td>
																	<td width="1" class="intBorderCBottom">
																		<img id="Img26" runat="server" src="~/images/spacer.gif" width="1" height="7" />
																	</td>
																	<td class="intBorderCBottom">
																		<img id="Img27" runat="server" src="~/images/spacer.gif" width="7" height="7" />
																	</td>
																	<td class="intBorderCBottomRight">
																		<img id="Img28" runat="server" src="~/images/spacer.gif" width="7" height="7" />
																	</td>
																</tr>
																<asp:repeater runat="server" id="rptTransactions" enableviewstate="False">
							<ItemTemplate>
								 <tr align="left" class="intColor1">
									<td class="intBorderC1BorderLeft1"><img id="Img29" runat="server" src="~/images/spacer.gif" width="1" height="60"/></td>
									<td class="intColor1" width="16"><img id="Img30" runat="server" src="~/images/spacer.gif" width="15" height="7"/></td>
									<td width="60" class="intColor1" align="left">
										<div class="framesize-small">
											<div class="frame">
											<div id="Div1" class="restricted" runat="server" visible='<%# IsMature( Convert.ToInt32(DataBinder.Eval(Container.DataItem, "AssetRatingId")) )%>'></div>
												<span class="ct"><span class="cl"></span></span>
												<div class="imgconstrain">
													<a title='<%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "Name").ToString ())%>' href='<%# GetAssetDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "AssetId")))%>'>
														<img border="0" src='<%#GetMediaImageURL (DataBinder.Eval(Container.DataItem, "ThumbnailSmallPath").ToString () ,"sm", Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "AssetId")), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "AssetTypeId")), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "ThumbnailGen")))%>' border="0"/>
													</a>
												</div>		
												<span class="cb"><span class="cl"></span></span>
											</div>
										</div>
									</td>
									<td width="278"><a title='<%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "name").ToString ())%>' href='<%# GetAssetDetailsLink (Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "AssetId")))%>'><%# TruncateWithEllipsis (Server.HtmlDecode (DataBinder.Eval(Container.DataItem, "Name").ToString ()), 45) %></a></td>
									<td width="1" class="sepInt1"><img id="Img31" runat="server" src="~/images/spacer.gif" width="1" height="7"/></td>
									<td class="textGeneralGray11"><img id="Img32" runat="server" src="~/images/spacer.gif" width="16" height="7"/><%# FormatDateTime (DataBinder.Eval(Container.DataItem, "CreatedDate")) %></td>
									<td width="1"><img id="Img33" runat="server" src="~/images/spacer.gif" width="1" height="7"/></td>
									<td class="textGeneralGray11"></td>
									<td class="intBorderC1BorderRight1">&nbsp;</td>
								</tr>
							</ItemTemplate>
							<AlternatingItemTemplate>
								 <tr align="left" class="intColor2">
									<td class="intBorderC1BorderLeft1"><img id="Img34" runat="server" src="~/images/spacer.gif" width="1" height="60"/></td>
									<td class="intColor2" width="16"><img id="Img35" runat="server" src="~/images/spacer.gif" width="15" height="7"/></td>
									<td width="60" class="intColor2" align="left">
										<div class="framesize-small">
											<div class="frame">
											<div id="Div2" class="restricted" runat="server" visible='<%# IsMature( Convert.ToInt32(DataBinder.Eval(Container.DataItem, "AssetRatingId")) )%>'></div>
												<span class="ct"><span class="cl"></span></span>
												<div class="imgconstrain">
													<a title='<%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "Name").ToString ())%>' href='<%# GetAssetDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "AssetId")))%>'>
														<img border="0" src='<%#GetMediaImageURL (DataBinder.Eval(Container.DataItem, "ThumbnailSmallPath").ToString () ,"sm", Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "AssetId")), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "AssetTypeId")), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "ThumbnailGen")))%>' border="0"/>
													</a>
												</div>		
												<span class="cb"><span class="cl"></span></span>
											</div>
										</div>
									</td>
									<td width="278"><a title='<%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "Name").ToString ())%>' href='<%# GetAssetDetailsLink (Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "AssetId")))%>'><%# TruncateWithEllipsis (Server.HtmlDecode (DataBinder.Eval(Container.DataItem, "Name").ToString ()), 45) %></a></td>
									<td width="1" class="sepInt1"><img id="Img36" runat="server" src="~/images/spacer.gif" width="1" height="7"/></td>
									<td class="textGeneralGray11"><img id="Img37" runat="server" src="~/images/spacer.gif" width="16" height="7"/><%# FormatDateTime (DataBinder.Eval(Container.DataItem, "CreatedDate")) %></td>
									<td width="1"><img id="Img38" runat="server" src="~/images/spacer.gif" width="1" height="7"/></td>
									<td class="textGeneralGray11"></td>
									<td class="intBorderC1BorderRight1">&nbsp;</td>
								</tr>
							</AlternatingItemTemplate>
                          </asp:repeater>
																<tr>
																	<td class="intBorderC1BottomLeft">
																		<img id="Img39" runat="server" src="~/images/spacer.gif" width="7" height="7" />
																	</td>
																	<td colspan="3" class="intBorderC1Bottom">
																		<img id="Img40" runat="server" src="~/images/spacer.gif" width="7" height="7" />
																	</td>
																	<td width="1" class="intBorderC1Bottom">
																		<img id="Img41" runat="server" src="~/images/spacer.gif" width="1" height="7" />
																	</td>
																	<td class="intBorderC1Bottom">
																		<img id="Img42" runat="server" src="~/images/spacer.gif" width="7" height="7" />
																	</td>
																	<td width="1" class="intBorderC1Bottom">
																		<img id="Img43" runat="server" src="~/images/spacer.gif" width="1" height="7" />
																	</td>
																	<td class="intBorderC1Bottom">
																		<img id="Img44" runat="server" src="~/images/spacer.gif" width="7" height="7" />
																	</td>
																	<td class="intBorderC1BottomRight">
																		<img id="Img45" runat="server" src="~/images/spacer.gif" width="7" height="7" />
																	</td>
																</tr>
															</table>
														</td>
														<td width="6" height="6">
															<img id="Img46" runat="server" src="~/images/spacer.gif" width="6" height="6" />
														</td>
													</tr>
													<tr>
														<td width="6" height="6">
															<img id="Img47" runat="server" src="~/images/spacer.gif" width="6" height="6" />
														</td>
														<td width="6" height="6">
															<img id="Img48" runat="server" src="~/images/spacer.gif" width="6" height="6" />
														</td>
														<td width="6" height="6">
															<img id="Img49" runat="server" src="~/images/spacer.gif" width="6" height="6" />
														</td>
													</tr>
												</table>
											</td>
											<td class="boxInsideRight">
												<img id="Img50" runat="server" src="~/images/spacer.gif" width="1" height="4" />
											</td>
										</tr>
										<tr>
											<td class="boxInsideBottomLeft">
												<img runat="server" src="~/images/spacer.gif" width="4" height="4" />
											</td>
											<td class="boxInsideBottom2">
												<img runat="server" src="~/images/spacer.gif" width="4" height="1" />
											</td>
											<td class="boxInsideBottomRight">
												<img runat="server" src="~/images/spacer.gif" width="4" height="4" />
											</td>
										</tr>
									</table>

								</td>
								<td class="">
									<img runat="server" src="~/images/spacer.gif" width="1" height="1" />
								</td>
							</tr>
							<tr>
								<td class=""></td>
								<td class=""></td>
								<td class=""></td>
							</tr>
						</table>
					
						
						
								
					</td>
				</tr>
			</table>
