﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="findFriends.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.findFriends" %>
<%@ Register TagPrefix="UserControl" TagName="FacebookJS" Src="~/usercontrols/FacebookJavascriptInclude.ascx" %>

<!--[if IE 6]>
<link rel="stylesheet" href="../css/new_IE.css" type="text/css" />
<![endif]-->
<link href="../css/themes-join/template.css" rel="stylesheet" type="text/css">	
<link href="../css/registration_flow/facebookconnect.css?v2" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../jscript/colorbox/jquery-1.7.1.min.js"></script>	
<usercontrol:FacebookJS runat="server"></usercontrol:FacebookJS>
<style type="text/css">
#footer {text-align:center;}
</style>
<script>var $j = jQuery.noConflict();</script>
<div id="fb-root"></div> 
<br />
<table class="findfriend" border="0" cellpadding="0" cellspacing="0" width="790">
	<tr>
		<td class="frTopLeft"></td>
		<td class="frTop"></td>
		<td class="frTopRight"></td>
	</tr>
	<tr>
		<td class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" id="Img2" width="1" height="1" /></td>
		<td valign="top" class="frBgIntMembers">
			<h1 id="h1Title" runat="server"></h1>
			<p id="pDesc" runat="server">&nbsp;</p>
			<a id="aSkip" clientidmode="Static" runat="server" href="~/register/kaneva/registerCompleted.aspx" runat="server">Skip Step  &#187;</a>

			<div class="listContainer">			 
				<div id="divFBFriends" class="fbFriendsList" runat="server" visible="false">
					<asp:datalist id="dlInviteFromFB" runat="server" showfooter="False" width="99%" 
						cellpadding="0" cellspacing="0" border="1" repeatcolumns="2" itemstyle-width="50%" 
						horizontalalign="Center" repeatdirection="Horizontal" repeatlayout="table">	
						<itemtemplate>
							<div class="fbFriend" onmouseover="this.style.backgroundColor='#f0f8cf';" onmouseout="this.style.backgroundColor='#fff';"><img src='<%# DataBinder.Eval(Container.DataItem, "Picture") %>' /><div><%# Server.HtmlDecode (DataBinder.Eval(Container.DataItem, "Name").ToString()) %></div>
								<button onclick='SendMsg(<%# Server.HtmlDecode (DataBinder.Eval(Container.DataItem, "Id").ToString())%>, <%# CurrentUser_UserId() %>)'>Invite</button></div>
						</itemtemplate>
					</asp:datalist>
				</div>

				<div id="divKanevaFriends" class="fbFriendsKaneva" runat="server" visible="false">
					<asp:datalist id="dlFBFriendsOnKaneva" runat="server" showfooter="False"
						cellpadding="0" cellspacing="0" border="1" repeatcolumns="5" onitemdatabound="fbFriendsKaneva_ItemDataBound"  
						horizontalalign="Center" repeatdirection="Horizontal" repeatlayout="flow">	
						<itemtemplate>
							<div class="fbFriendKan">
								<a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "NameNoSpaces").ToString ())%> style="cursor: hand;"><img style="width:50px;height:50px;" id="Img5" runat="server" src='<%#GetProfileImageURL (DataBinder.Eval(Container.DataItem, "ThumbnailMediumPath").ToString (), "me", DataBinder.Eval(Container.DataItem, "Gender").ToString (), Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "FacebookSettings.UseFacebookProfilePicture")), Convert.ToUInt64(DataBinder.Eval(Container.DataItem, "FacebookSettings.FacebookUserId"))  )%>' border="0"/></a>
								<p class="displayName"><a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "NameNoSpaces").ToString ())%> style="cursor:hand;"><%# TruncateWithEllipsis (DataBinder.Eval(Container.DataItem, "DisplayName").ToString(), 13) %></a></p>
								<p class="username">"<%# TruncateWithEllipsis (DataBinder.Eval(Container.DataItem, "Username").ToString(), 13) %>"</p>
								<p style="width:127px;height:37px;margin-top:15px;">
									<asp:imagebutton class="button" id="btnAddFriend" imageurl="~/images/facebook/addfriend.png" width="93" height="37" runat="server" commandargument="" commandname="" />
									<img class="loading" id="imgloading<%# DataBinder.Eval (Container.DataItem, "UserId").ToString() %>" alt="" src="../images/ajax-loader.gif" />
								</p>
							</div>
						</itemtemplate>
					</asp:datalist>
				</div>
			</div>
			<br />
			<asp:imagebutton class="sendAll" id="btnSendAll" onclick="btnSendAll_Click" imageurl="~/images/facebook/sendrequesttoall.png" width="262" height="31" runat="server" visible="false"></asp:imagebutton>
			<br />
		</td>
		<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" id="Img4" width="1" height="1" /></td>
	</tr>
	<tr>
		<td class="frBorderLeftGray"></td>
		<td class="frBgIntMembersGray">
			<asp:button class="next" onclick="btnNext_Click" id="btnNext" runat="server" text="Next Step" causesvalidation="False" />
		</td>
		<td class="frBorderRightGray"></td>
	</tr>
	<tr>
		<td class="frBottomLeftGray"></td>
		<td class="frBottomGray"></td>
		<td class="frBottomRightGray"></td>
	</tr>
</table>				
<br/><br/>
<style type="text/css">
#footer {display:none;}
</style>