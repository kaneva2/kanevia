<%@ Page language="c#" Codebehind="manageCredits.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.mykaneva.manageCredits" %>

<link href="../css/new.css" rel="stylesheet" type="text/css"/>
<link href="../css/shadow.css" rel="stylesheet" type="text/css">

<table border="0" cellspacing="0" cellpadding="0" width="990" align="center">
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" /></td>
	</tr>
	<tr>
		<td>
			<table  border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="frTopLeft"></td>
					<td class="frTop"></td>
					<td class="frTopRight"></td>
				</tr>
				<tr>
					<td class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
					<td valign="top" class="frBgIntMembers">
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
								
									<!-- TOP STATUS BAR -->
									<div id="pageheader">
										<table cellpadding="0" cellspacing="0" border="0" width="99%">
											<tr>
												<td align="left">
													<h1>Manage Credits</h1>
												</td>
									
												<td align="right" valign="middle">
													<table cellpadding="0" cellspacing="0" border="0" width="690">
														<tr>
															<td>	
																<span id='ajpTopStatusBar$RBS_Holder'><span id="ajpTopStatusBar" ajaxcall="async">
																<table cellpadding="0" cellspacing="0" border="0" width="100%">
																	<tr>
																		<td class="headertout" width="175"></td>
																		<td class="searchnav" width="260"></td>
																		<td align="left" width="130"></td>
																		<td class="searchnav" align="right" width="210" nowrap></td>
																	</tr>
																</table>
																</span></span>
															</td>	
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</div>
									<!-- END TOP STATUS BAR -->		
								
								</td>
							</tr>
							<tr>
								<td width="968" align="left"  valign="top">
								
									<table border="0" cellspacing="0" cellpadding="0" width="100%">
										<tr>
											<!-- START WIZARD CONTENT -->
											<td valign="top" align="center">
												
												<table border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td width="250" valign="top">
															
															<div class="module toolbar">
																<span class="ct"><span class="cl"></span></span>
																<h2>Tools & Actions <span class="hlink"></span></h2>
																<table cellspacing="0" cellpadding="10" border="0" width="99%" style="margin-bottom:16px;">
																	<tr>
																		<td align="center">
																			<table cellspacing="0" cellpadding="5" border="0" width="100%">
																				<tr>
																					<td align="center"><a class="nohover" onmouseover="this.style.cursor='pointer'" onfocus="this.blur();" href="~/mykaneva/buycredits.aspx" runat="server" id="aPurchase"><img src="~/images/button_addcredits.gif" id="imgAddCredits" runat="server" alt="Purchase Credits" width="153" height="23" border="0"></a></td>
																				</tr>
																				<tr>
																					<td align="center"><a href="~/mykaneva/billing.aspx" runat="server" id="aBillingAddress" class="nohover"><img src="~/images/button_updatebilling.gif" alt="Update your billing address" runat="server" width="153" height="23" border="0"></a></td>
																				</tr>
																				<tr style="display:none;">
																					<td align="center"><a href="~/mykaneva/credithistory.aspx" runat="server" id="aPurchaseHistory" class="nohover"><img src="~/images/button_viewhistory.gif" alt="View your credit purchase history" runat="server" width="153" height="23" border="0"></a></td>
																				</tr>
																				<tr>
																					<td align="center"><a href="~/mykaneva/creditfaq.aspx" runat="server" id="aCreditFAQ" class="nohover"><img src="~/images/button_creditfaqs.gif" alt="View credits Frequently Asked Questions" runat="server" width="153" height="23" border="0"></a></td>
																				</tr>
																			</table>
																		</td>
																	</tr>
																</table>
																<span class="cb"><span class="cl"></span></span>
															</div>
														
														</td>
														<td width="20"><img runat="server" src="~/images/spacer.gif" width="20" height="1" /></td>
														<td width="698" align="center">
															
															<div class="module fullpage">
															<span class="ct"><span class="cl"></span></span>
															
															<!-- POP UP MESSAGING : Email Address Not Validated -->
															<div id='PopUp1' style='display: none; position: absolute; left:0; top: -3px; margin: 0; padding: 15px; background-color: transparent; text-align: justify; font-size: 12px; width: auto; z-index: 2000;'>
																<div style=' display: block; border: 3px solid #f7ce52; padding: 10px; background-color: #FFFFFF;'>
																	<table border="0" cellspacing="0" cellpadding="3" width="640" height="135">
																		<tr>
																			<td><h2>Notice</h2>You must validate your email address to access this feature. <a href="~/mykaneva/general.aspx" runat="server">Validate now</a></td>
																		</tr>
																		
																		<tr>
																			<td><div style="text-align: right;"><a href="#" onmouseover="" onfocus="this.blur();" onclick="document.getElementById('PopUp1').style.display='none';return false;" >Close</a></div></td>
																		</tr>
																	</table>
																</div>
															</div>
															<!-- POP UP MESSAGING : User Has Not Logged Into World -->
															<div id='PopUp2' style='display: none; position: absolute; left:0; top: -3px; margin: 0; padding: 15px; background-color: transparent; text-align: justify; font-size: 12px; width: auto; z-index: 2000;'>
																<div style=' display: block; border: 3px solid #f7ce52; padding: 10px; background-color: #FFFFFF;'>
																	<table border="0" cellspacing="0" cellpadding="3" width="640" height="135">
																		<tr>
																			<td><h2>Notice</h2>You must sign-in into the Virtual World of Kaneva at least once before you can buy credits. Please <a href="~/kgp/playwok.aspx" runat="server">sign-in to the world</a> and come back to buy credits.</td>
																		</tr>
																		
																		<tr>
																			<td><div style="text-align: right;"><a href="#" onmouseover="" onfocus="this.blur();" onclick="document.getElementById('PopUp2').style.display='none';return false;" >Close</a></div></td>
																		</tr>
																	</table>
																</div>
															</div>
															<!-- POP UP MESSAGING -->
															<table border="0" cellspacing="0" cellpadding="10"  width="670" align="center" class="wizform">
																<tr>
																	<td>
																	
																		<table border="0" cellspacing="0" cellpadding="0" width="98%">
																			<tr>
																				<td width="80"><img src="~/images/buynow_items.gif" runat=server alt="tokens.gif" border="0"></td>
																				<td width="250">
																					<p>Credits are the currency of The Virtual World of Kaneva.</p>
																					<p>Credits enable you to purchase all sorts of items from a variety of locations within the World, such as accessories for your home or clothing to wear.</p>
																					<p>Add credits to your balance now!</p>
																				</td>
																				<td align="center">
																					<div class="module">
																					<span class="ct"><span class="cl"></span></span>
																					<h2>Current Credit Balance</h2>
																						<table border="0" cellspacing="0" cellpadding="0" width="90%" class="nopadding">
																							<tr>
																								<td class="whitebox" align="center" style="padding: 8px;"><h1 style="text-align: center; padding-bottom: 8px;"><span runat="server" id="spnCreditCount">0</span> Credits</h1>
																								<a class="nohover" onmouseover="this.style.cursor='pointer'" onfocus="this.blur();" href="~/mykaneva/buycredits.aspx" runat="server" id="aAddCredits"><img src="~/images/wizard_btn_addcredits2.gif" id="imgAddCreditsNow" runat="server" alt="Add Credits" width="136" height="25" align="center" border="0"></a></td>
																							</tr>
																						</table>
																						<span class="cb"><span class="cl"></span></span>
																					</div>
																				</td>
																			</tr>
																			<tr>
																				<td></td>
																				<td colspan="2" style="text-align:left;padding-left:10px;"><img runat="server" src="~/images/paypal-allcreditcards.gif" width="282" height="31" /></td>
																			</tr>
																		</table>
																		
																	</td>
																</tr>
															</table>
															
															<span class="cb"><span class="cl"></span></span>
															</div>
															
															<div id="smallpage_spacer"></div>
															<div id="smallpage_spacer"></div>
															
														</td>
													</tr>
												</table>
												
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
				</tr>
				<tr>
					<td class="frBottomLeft"></td>
					<td class="frBottom"></td>
					<td class="frBottomRight"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>

