///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Kaneva.BusinessLayer.Facade;
using KlausEnt.KEP.Kaneva.usercontrols;

namespace KlausEnt.KEP.Kaneva.mykaneva
{
	/// <summary>
	/// Summary description for buyCredits.
	/// </summary>
	public class buyCredits : MainTemplatePage
	{
		protected buyCredits () 
		{
			Title = "Purchase Credits";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			if (!KanevaGlobals.EnableCheckout)
			{
				if (!IsAdministrator())
				{
                    Response.Redirect("~/error.aspx?error=" + (int)Constants.eERROR_TYPE.CHECKOUT_CLOSED);
                }
			}

			VerifyUserAccess();

            //check for error messages
            if (Request["message"] != null && Request["message"] != string.Empty)
            {
                switch (Request["message"].ToString())
                {
                    case "1":
                        spnMessage.InnerText = "Please select a credit amount.";
                        errorReload = true;
                        break;
                }
            }

			if ((!IsPostBack ) || errorReload)
			{                
                //get the preview date from the request - if there is one process the page as a preview page
                DateTime previewDate = DateTime.Now;
                if (Request["previewDate"] != null && Request["previewDate"] != string.Empty)
                {
                    previewDate = Convert.ToDateTime(Request["previewDate"].ToString());
                    btnPurchase.Visible = false;
                }

                //check to see if user has any purchase records at all
                bool isFirstTimePurchase = StoreUtility.GetUserNonAccessPassTransactionCount (KanevaWebGlobals.CurrentUser.UserId) > 0 ? false : true;
                if (IsAdministrator ())
                {
                    isFirstTimePurchase = false;
                }

                //get active promotions
                DataTable dtPromotions = StoreUtility.GetActivePromotionsForUser (KanevaWebGlobals.CurrentUser.UserId, Constants.CURR_KPOINT, !isFirstTimePurchase, previewDate);
                DataTable dtSpecials = dtPromotions.Copy();
			
				DataColumn[] dcPk = new DataColumn[1];
                dcPk[0] = dtPromotions.Columns["promotion_id"];
				dtPromotions.PrimaryKey = dcPk;
					
                //grab first promotion as default
                if (dtPromotions.Rows.Count > 0)
                {
                    DefaultPromotionID = Convert.ToInt32(dtPromotions.Rows[0]["promotion_id"]);
                }

                //reduces the promotions copy to just the available specials 
                int promoTypeId = 0;
                foreach (DataRow row in dtSpecials.Rows)
                {
                    promoTypeId = Convert.ToInt32 (row["promotional_offers_type_id"]);
                    if (promoTypeId != (int) Constants.ePROMOTION_TYPE.SPECIAL &&
                        promoTypeId != (int) Constants.ePROMOTION_TYPE.SPECIAL_FIRST_TIME &&
                        promoTypeId != (int) Constants.ePROMOTION_TYPE.SPECIAL_ONE_TIME)
                    {
                        row.Delete();
                    }
                }
                dtSpecials.AcceptChanges();

                
                if (dtSpecials.Rows.Count > 0)
                {
                    // check for any first time purchase only items
                    DataRow[] defaultSpec = dtSpecials.Select ("promotional_offers_type_id=" + (int)Constants.ePROMOTION_TYPE.SPECIAL_FIRST_TIME);

                    if (defaultSpec.Length > 0)
                    {
                        // set first time purchase item as default
                        DefaultPromotionID = Convert.ToInt32 (defaultSpec[0]["promotion_id"]);
                    }
                    else
                    {
                        // no first time only items, so check for one time purchase items
                        defaultSpec = dtSpecials.Select ("promotional_offers_type_id=" + (int) Constants.ePROMOTION_TYPE.SPECIAL_ONE_TIME);

                        if (defaultSpec.Length > 0)
                        {
                            // set one time purchase item as default
                            DefaultPromotionID = Convert.ToInt32 (defaultSpec[0]["promotion_id"]);
                        }
                        else
                        {
                            // no one time purchase items, so check for general specials
                            defaultSpec = dtSpecials.Select ("promotional_offers_type_id=" + (int) Constants.ePROMOTION_TYPE.SPECIAL);

                            if (defaultSpec.Length > 0)
                            {
                                // set special item as default
                                DefaultPromotionID = Convert.ToInt32 (defaultSpec[0]["promotion_id"]);
                            }
                        }
                    }
                }


                if (dtSpecials.Rows.Count > 0)
                {
                    divPackages.Visible = false;
                    divSpecials.Visible = true;
                }
                else
                {
                    divPackages.Visible = true;
                    divSpecials.Visible = false;
                }

                rptSpecials.DataSource = dtSpecials;
                rptSpecials.DataBind();
                
                // add credit offerings to the list
				rptCredits.DataSource = dtPromotions;
				rptCredits.DataBind ();

                selectedPackage.Value = DefaultPromotionID.ToString();
			}

			// Set Nav
            HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;

			//setup wizard nav bar
			ucWizardNavBar.ActiveStep = CheckoutWizardNavBar.STEP.STEP_1;
		}

		#region Helper Methods
		
		private void VerifyUserAccess()
		{
			// They must be logged in
			if (!Request.IsAuthenticated)
			{
				Response.Redirect (GetLoginURL ());
			}
		}

		protected string FormatCredits(UInt64 credits)
		{
			try
			{
				return credits.ToString("N0");
			}
			catch
			{
				return "";
			}
		}

		#endregion

		#region Event Handlers
		/// <summary>
		/// They want to purchase a point bucket
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnPurchase_Click (object sender, ImageClickEventArgs e) 
		{
			if (!Request.IsAuthenticated)
			{
				Response.Redirect (GetLoginURL());
				return;
			}
													
			int userId = GetUserId ();

			DataRow drOrder = null;
			bool newOrder = true;

			if (Request ["orderId"] != null && Request ["orderId"] != string.Empty)
			{
				try
				{
					drOrder = StoreUtility.GetOrder( Convert.ToInt32(Request["orderId"]), userId );
				}
				catch {}
			}																	 
																				 
			int orderId = 0;
			if ( drOrder != null && drOrder ["order_id"].ToString () != string.Empty && 
				Convert.ToInt32(drOrder ["transaction_status_id"]) == (int) Constants.eORDER_STATUS.CHECKOUT &&
				Convert.ToInt32(drOrder ["user_id"]) == GetUserId () )		
			{
				orderId = Convert.ToInt32( drOrder ["order_id"] );
				newOrder = false;															
			}																				
			else																			
			{
				orderId = StoreUtility.CreateOrder (userId, Common.GetVisitorIPAddress(), (int) Constants.eORDER_STATUS.CHECKOUT);
			}

			// Set the purchase type
			StoreUtility.SetPurchaseType (orderId, Constants.ePURCHASE_TYPE.KPOINT_ONLY);
			int creditId = 0;
            if (this.selectedPackage.Value != "")
			{
                if (KanevaGlobals.IsNumeric(selectedPackage.Value))
				{
                    creditId = Convert.ToInt32(selectedPackage.Value);
				}
			}

            if (creditId == 0)
            {
                Response.Redirect(ResolveUrl("~/mykaneva/buyCredits.aspx?message=1&orderId=" + orderId.ToString())); ;
            }

            DataRow drPointBucket = StoreUtility.GetPromotion(creditId);

			// Point Bucket item name
			string itemName = Server.HtmlEncode (drPointBucket ["display"].ToString ());

            if (drPointBucket["promotion_description"] != null && drPointBucket["promotion_description"].ToString() != string.Empty)
			{
                itemName = Server.HtmlEncode(drPointBucket["promotion_description"].ToString());
			}

			int transactionId = 0;
			
			if (newOrder)
			{
				// Record the transaction in the database, marked as checkout
				transactionId = StoreUtility.PurchasePoints (userId, (int) Constants.eTRANSACTION_STATUS.CHECKOUT, itemName, 0, 
					(int) Constants.ePAYMENT_METHODS.KANEVA_CYBERSOURCE, Convert.ToDouble (drPointBucket ["dollar_amount"]), 
					Convert.ToDouble (drPointBucket ["kei_point_amount"]), Convert.ToDouble (drPointBucket ["free_points_awarded_amount"]), 
					0, Common.GetVisitorIPAddress());

				// Record the point bucket purchased
				StoreUtility.UpdatePointTransactionPointBucket (transactionId, Convert.ToInt32 (drPointBucket ["promotion_id"]));
			}
			else
			{
				transactionId = Convert.ToInt32(drOrder ["point_transaction_id"]);

				// updated existing order
				StoreUtility.UpdatePointTransactionPointBucket (transactionId, Convert.ToInt32 (drPointBucket ["promotion_id"]), itemName, Convert.ToDouble (drPointBucket ["dollar_amount"]), Convert.ToDouble (drPointBucket ["kei_point_amount"]), Convert.ToDouble (drPointBucket ["free_points_awarded_amount"]));
			}

			// Set the order id on the point purchase transaction
			StoreUtility.UpdatePointTransaction (transactionId, orderId);
			StoreUtility.UpdateOrderPointTranasactionId (orderId, transactionId);

			// Send them to select a payment method
			Response.Redirect (ResolveUrl ("~/checkout/billinginfo.aspx?orderId=" + orderId.ToString()));;
		}

        private void rptCredits_ItemCreated(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                HtmlTableRow htrw = (HtmlTableRow)e.Item.FindControl("drPromtion");
                HtmlInputRadioButton rdobtn = (HtmlInputRadioButton)e.Item.FindControl("rbCredit");
                int promoID = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "promotion_id").ToString());
                bool applySelectedClass = true;
                //assign them all the same name
                rdobtn.Name = "promoGroup";

                int promoTypeId = Convert.ToInt32 (DataBinder.Eval (e.Item.DataItem, "promotional_offers_type_id"));

                if (promoTypeId == (int) Constants.ePROMOTION_TYPE.SPECIAL ||
                    promoTypeId == (int) Constants.ePROMOTION_TYPE.SPECIAL_FIRST_TIME ||
                    promoTypeId == (int) Constants.ePROMOTION_TYPE.SPECIAL_ONE_TIME)
                {
                    string liststyle = "";
                    string rowstyle = DataBinder.Eval(e.Item.DataItem, "highlight_color").ToString();
                    string fontcolor = DataBinder.Eval(e.Item.DataItem, "special_font_color").ToString();
                    if (rowstyle != "")
                    {
                        liststyle = "background-color:" + rowstyle;
                        applySelectedClass = false;
                    }
                    
                    if (fontcolor != "")
                    {
                        HtmlTableCell item1 = (HtmlTableCell)e.Item.FindControl("tddescription");
                        HtmlTableCell item2 = (HtmlTableCell)e.Item.FindControl("tddollaramount");

                        item1.Attributes.Add("style", "color:" + fontcolor);
                        item2.Attributes.Add("style", "color:" + fontcolor);
                    }

                    if (liststyle != "")
                    {
                        htrw.Attributes.Add("style", liststyle);
                    }
                }
                if (promoID == DefaultPromotionID)
                {
                    rdobtn.Checked = true;
                    if (applySelectedClass)
                    {
                        htrw.Attributes.Add ("class", "on1");
                    }
                }
                else
                {
                    rdobtn.Checked = false;
                }
            }
            catch (Exception)
            { }

        }

        private void rptSpecials_ItemCreated(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                string tblstyle = "";
                string divstyle = "";

                HtmlTable tbSpecialBox = (HtmlTable)e.Item.FindControl("specialbox");
                HtmlContainerControl divSticker = (HtmlContainerControl)e.Item.FindControl("sticker");

                string backgroundImage = DataBinder.Eval(e.Item.DataItem, "special_background_image").ToString();
                string backgroundColor = DataBinder.Eval(e.Item.DataItem, "special_background_color").ToString();
                string fontcolor = DataBinder.Eval(e.Item.DataItem, "special_font_color").ToString();
                if (backgroundImage != "")
                {
                    tblstyle = "background:transparent url('" + backgroundImage + "') no-repeat top left ";
                }
                if (backgroundColor != "")
                {
                    if (tblstyle != "")
                    {
                        tblstyle += ";background-color:" + backgroundColor;
                    }
                    else
                    {
                        tblstyle = "background-color:" + backgroundColor;
                    }
                }

                if (fontcolor != "")
                {
                    CurrentFontColor = fontcolor;

                    if (tblstyle != "")
                    {
                        tblstyle += ";border: 2px solid " + fontcolor;
                    }
                    else
                    {
                        tblstyle += "border: 2px solid " + fontcolor;
                    }

                    HtmlGenericControl item1 = (HtmlGenericControl)e.Item.FindControl("specialHeading");
                    HtmlGenericControl item2 = (HtmlGenericControl)e.Item.FindControl("specialSub1");
                    HtmlGenericControl item3 = (HtmlGenericControl)e.Item.FindControl("specialSub2");
                    HtmlGenericControl item4 = (HtmlGenericControl)e.Item.FindControl("specialSub3");
                    HtmlGenericControl item5 = (HtmlGenericControl)e.Item.FindControl("parHeading");
                  

                    item1.Attributes.Add("style", "color:" + fontcolor);
                    item2.Attributes.Add("style", "color:" + fontcolor);
                    item3.Attributes.Add("style", "color:" + fontcolor);
                    item4.Attributes.Add("style", "color:" + fontcolor);
                    item5.Attributes.Add("style", "color:" + fontcolor);

                }


                string specialSticker = DataBinder.Eval(e.Item.DataItem, "special_sticker_image").ToString();
                divstyle = "background:transparent url('" + specialSticker + "') no-repeat top left ";


                tbSpecialBox.Attributes.Add("style", tblstyle);
                divSticker.Attributes.Add("style", divstyle);

            }
            catch (Exception)
            { }

        }

        private void rptSpecials_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                dlSpecialItems = (DataList)e.Item.FindControl("dlSpecialItems");
                string promoId = ((HtmlInputHidden)e.Item.FindControl("ihd_promotionId")).Value;

                DataTable dtPromotionItems = StoreUtility.GetPromotionalOfferItemsByGender(Convert.ToInt32(promoId), KanevaWebGlobals.CurrentUser.Gender);
                dlSpecialItems.DataSource = dtPromotionItems;
                dlSpecialItems.DataBind();
            }
            catch (Exception)
            { }

        }

        public void dlSpecialItems_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            HtmlTableCell item1 = (HtmlTableCell)e.Item.FindControl("itemquantity");
            HtmlTableCell item2 = (HtmlTableCell)e.Item.FindControl("itemname");
            item1.Attributes.Add("style", "width: 20px; color:" + CurrentFontColor);
            item2.Attributes.Add("style", "color:" + CurrentFontColor);
        }

		#endregion

        #region Properties

        private int MoveInSpecialID
        {
            get
            {
                if (ViewState["_moveinID"] == null)
                {
                    ViewState["_moveinID"] = 0;
                }
                return (int) ViewState["_moveinID"];
            }
            set
            {
                ViewState["_moveinID"] = value;
            }
        }

        public string CurrentFontColor
        {
            get
            {
                if (ViewState["_currentColor"] == null)
                {
                    ViewState["_currentColor"] = "#000000";
                }
                return ViewState["_currentColor"].ToString ();
            }
            set
            {
                ViewState["_currentColor"] = value;
            }
        }

        private int DefaultPromotionID
        {
            get
            {
                if (ViewState["_defaultPromo"] == null)
                {
                    ViewState["_defaultPromo"] = 0;
                }
                return (int) ViewState["_defaultPromo"];
            }
            set
            {
                ViewState["_defaultPromo"] = value;
            }
        }

        #endregion

        #region Declerations

        protected Repeater rptCredits, rptSpecials;
        protected DataList dlSpecialItems;
        protected Literal litScript, litSpecialCredits, litSpecialAmount, litFirstTimeBuyer;
        protected ImageButton btnPurchase;

        protected HtmlContainerControl spnMessage;
        protected HtmlContainerControl sticker, divSpecials, divPackages;
        protected HtmlContainerControl divSpecialDescription;
        protected HtmlInputHidden ihd_promotionId, selectedPackage;
        protected HtmlTable specialbox;

        private bool errorReload = false;


        protected CheckoutWizardNavBar ucWizardNavBar;

        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
            this.rptCredits.ItemCreated += new System.Web.UI.WebControls.RepeaterItemEventHandler(this.rptCredits_ItemCreated);
            this.rptSpecials.ItemCreated += new System.Web.UI.WebControls.RepeaterItemEventHandler(this.rptSpecials_ItemCreated);
            this.rptSpecials.ItemDataBound += new System.Web.UI.WebControls.RepeaterItemEventHandler(this.rptSpecials_ItemDataBound);
		}
		#endregion
	}
}
