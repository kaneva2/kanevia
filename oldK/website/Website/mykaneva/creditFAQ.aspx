<%@ Page language="c#" Codebehind="creditFAQ.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.creditFAQ" %>


<link href="../css/new.css" rel="stylesheet" type="text/css" />

<table border="0" cellspacing="0" cellpadding="0" class="newcontainer" align="center">
	<tr>
		<td>
			<table  border="0" cellpadding="0" cellspacing="0" width="100%" class="newcontainerborder">
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
				<tr>
					<td class=""><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
					<td valign="top" class="newdatacontainer">
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr colspan="2">
								<td>
								
									<!-- TOP STATUS BAR -->
									<div id="pageheader">
										<table cellpadding="0" cellspacing="0" border="0" width="99%">
											<tr>
												<td align="left">
													<h1>Manage Credits</h1>
												</td>
									
											</tr>
										</table>
									</div>
									<!-- END TOP STATUS BAR -->		
								
								</td>
							</tr>
							<tr>
								<td width="968" align="left"  valign="top">
								<br>
									<table border="0" cellspacing="0" cellpadding="0" width="100%">
										<tr>
											<!-- START WIZARD CONTENT -->
											<td valign="top" align="center">
												
												<table border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td width="250" valign="top">
															<div class="module toolbar">
																<span class="ct"><span class="cl"></span></span>
																<table cellspacing="0" cellpadding="10" border="0" width="99%">
																	<tr>
																		<td align="center">
																			<table cellspacing="0" cellpadding="5" border="0" width="100%">
																				<tr>
																					<td align="center"><a class="nohover" onmouseover="this.style.cursor='pointer'" onfocus="this.blur();" href="~/mykaneva/buycredits.aspx" runat="server" id="aPurchase"><img src="~/images/button_addcredits.gif" id="imgAddCredits" runat="server" alt="Purchase Credits" width="153" height="23" border="0"></a></td>
																				</tr>
																				<tr>
																					<td align="center"><a href="~/mykaneva/billing.aspx" runat="server" id="aBillingAddress" class="nohover"><img src="~/images/button_updatebilling.gif" alt="Update your billing address" runat="server" width="153" height="23" border="0" id="Img2"></a></td>
																				</tr>
																				<tr>
																					<td align="center"><a href="~/mykaneva/credithistory.aspx" runat="server" id="aPurchaseHistory" class="nohover"><img src="~/images/button_viewhistory.gif" alt="View your credit purchase history" runat="server" width="153" height="23" border="0" id="Img4"></a></td>
																				</tr>
																			</table>
																		</td>
																	</tr>
																</table>
																<span class="cb"><span class="cl"></span></span>
															</div>
														
														
														</td>
														<td width="20"><img runat="server" src="~/images/spacer.gif" id="Img3" width="20" height="1" /></td>
														<td width="698" align="center">
														
															<div class="module fullpage">
															<span class="ct"><span class="cl"></span></span>
															
															<table border="0" cellspacing="0" cellpadding="0" width="90%" align="center">
																<tr>
																	<td>
																		<h2>Credits FAQs</h2>
																		
																		<ul class="greenarrow">
																			<li><a href="#q1">What are Kaneva Credits?</a></li>
																			<li><a href="#q2">What are Rewards?</a></li>
																			<li><a href="#q3">How do I purchase Credits?</a></li>
																			<li><a href="#q4">What can I purchase with Credits?</a></li>
																			<li><a href="#q5">Can I return my 3D items for a Credits refund?</a></li>
																			<li><a href="#q6">How do I check my Credits balance?</a></li>
																			<li><a href="#q7">Can I give my purchased Credits to another Kaneva member?</a></li>
																			<li><a href="#q8">Can I exchange my Credits for cash?</a></li>
																			<li><a href="#q9">Can I sell Credits to other players?</a></li>
																			<li><a href="#q10">Can I sell my account to someone else?</a></li>
																			<li><a href="#q11">What happens to my Credits if I close my Kaneva account?</a></li>
																			<li><a href="#q12">I was unable to purchase in-World Credits because my credit card was not accepted.  What happened?</a></li>
																		</ul>
																		
																		<hr class="contentsep">
																		
																		<a name="q1"></a>
																		<h2 class="question">What are Kaneva Credits?</h2>
																		Kaneva Credits are the currency used in the Virtual World of Kaneva. In addition to providing great value, Credits give you maximum flexibility and put you in control of how and when you want to use them. Credits can be securely purchased from the Kaneva web site.  After you buy Credits, you can purchase 3D items &#8211; clothes, furniture, gifts, property deeds and more -- in the Virtual World Mall .
																		<a href="#top" class="floatright">back to top</a>

																		<hr class="contentsep">
																		
																		<a name="q2"></a>
																		<h2 class="question">What are Rewards?</h2>
																		Rewards are awarded to all new players as starter cash to help pay for initial in-world purchases and provided as incentive for special events, special offers and competitions.   While Rewards can be used to buy items from stores, they cannot be used to buy items from other players and cannot be traded or sold.
																		<a href="#top" class="floatright">back to top</a>

																		<hr class="contentsep">
																		
																		<a name="q3"></a>
																		<h2 class="question">How do I purchase Credits? </h2>
																		You can purchase Credits with a major credit card any time after you register and sign on as a Kaneva Member to the Virtual World of Kaneva.  There are several Credits packages available starting at $9.99.  To purchase: 
																		
																		<ul class="greenarrow">
																			<li>Sign in to Kaneva</li>
																			<li>Click My Kaneva</li>
																			<li>Click My Account</li>
																			<li>Click Credits</li>
																		</ul>
																		<br>
																		From here, you can purchase Credits, view your Credits account balance, update your billing address and view your purchase history.
																		<br><br>
																		You will also find links to Buy Credits section inside the World and from the Virtual World of Kaneva Community.
																		<a href="#top" class="floatright">back to top</a>

																		<hr class="contentsep">
																		
																		<a name="q4"></a>
																		<h2 class="question">What can I purchase with Credits?</h2>
																		You can buy a broad array of 3D items, including:
																		<ul class="greenarrow">
																			<li>Clothes for your avatar</li>
																			<li>Furniture, electronics, and accessories for your home and hangout</li>
																			<li>Home design and renovation products &#8211; staircases, walls, doorways, platforms</li>
																			<li>Special animations</li>
																		</ul>
																		<a href="#top" class="floatright">back to top</a>

																		<hr class="contentsep">
																		
																		<a name="q5"></a>
																		<h2 class="question">Can I return my 3D items for a Credits refund?</h2>
																		Yes, but you'll only receive half your original purchase price in return.  You can sell your items to other players &#8211; it's up to you to negotiate the best price.  You can also trade items with other players.
																		<br><br>
																		By comparison, items purchased with Rewards cannot be returned, traded or sold.
																		<a href="#top" class="floatright">back to top</a>

																		<hr class="contentsep">
																		
																		<a name="q6"></a>
																		<h2 class="question">How do I check my Credits balance?</h2>
																		To keep tabs on your Virtual World Credits balance&#8230;
																		<ul class="greenarrow">
																			<li>Sign in to Kaneva</li>
																			<li>Click My Kaneva</li>
																			<li>Click My Account</li>
																			<li>Click Credits</li>
																		</ul>
																		<a href="#top" class="floatright">back to top</a>

																		<hr class="contentsep">
																		
																		<a name="q7"></a>
																		<h2 class="question">Can I give my purchased Credits to another Kaneva member?</h2>
																		Yes, in the Virtual World, you can offer your credits to another member through a Trade (think of it as a cash gift).  To make the exchange in-World&#8230;
																		<ul class="greenarrow">
																			<li>Right click on the person's Avatar
																			<li>Click the Trade button
																			<li>In the Credits box, indicate the number of Credits you are giving away
																			<li>Click the "check mark" to confirm the amount
																			<li>The person who is receiving your Credits can either accept or reject your offer
																		</ul>
																		<a href="#top" class="floatright">back to top</a>

																		<hr class="contentsep">
																		
																		<a name="q8"></a>
																		<h2 class="question">Can I exchange my Credits for cash?</h2>
																		No, Kaneva does not support Credits for cash exchange. 
																		<a href="#top" class="floatright">back to top</a>

																		<hr class="contentsep">
																		
																		<a name="q9"></a>
																		<h2 class="question">Can I sell Credits to other players?</h2>
																		Kaneva does not currently offer a method of secure transfer of Credits.  While you are free to sell your Credits to others, both buyers and sellers do so at their own risk.  Kaneva accepts no responsibility for inter-player sales of Credits, accounts or any other items.
																		<a href="#top" class="floatright">back to top</a>

																		<hr class="contentsep">
																		
																		<a name="q10"></a>
																		<h2 class="question">Can I sell my account to someone else?</h2>
																		Kaneva does not currently offer a method of secure transfer of accounts.  While you are free to sell your account to others, both buyers and sellers do so at their own risk.  Kaneva accepts no responsibility for inter-player sales of Credits, accounts or any other items.
																		<a href="#top" class="floatright">back to top</a>

																		<hr class="contentsep">
																		
																		<a name="q11"></a>
																		<h2 class="question">What happens to my Credits if I close my Kaneva account?</h2>
																		If you decide to discontinue your Kaneva membership, you will automatically lose all Credits and Rewards accrued to your account.  Kaneva does not provide refunds for Credits.  The same policy applies for banned Member accounts.
																		<a href="#top" class="floatright">back to top</a>

																		<hr class="contentsep">
																		
																		<a name="q12"></a>
																		<h2 class="question">I was unable to purchase in-World Credits because my credit card was not accepted.  What happened?</h2>
																		The most common reasons for credit card failure are:
																		<ul class="greenarrow">
																			<li>The credit card number was not entered correctly. </li>
																			<li>The billing address was not entered correctly.</li>
																			<li>The name on the card was not entered correctly, or was not entered as it is printed on the card. </li>
																			<li>The credit card is not in the list of accepted payment methods (we currently accept Visa, MasterCard, American Express and Discover). </li>
																			<li> The CVC (also called a CSC) 3- or 4-digit code was not entered or was not entered correctly. </li>
																			<li>The card is expired, or the expiration date was entered incorrectly. </li>
																			<li>There are no funds available on the credit card to validate it. We send a $1.00 USD authorization to insure a credit card is valid. While this is not a billing, the card must have at least $1.00 USD available on it to pass validation. </li>
																			<li>Your charge limit has been reached, and/or your bank is not authorizing any more transactions. </li>
																			<li>If outside the U.S., your card may not be setup for international/overseas transactions. </li>
																		</ul>
																		<br>
																		If you are confident that you entered all information correctly, contact your credit card provider to determine why your transaction was not authorized.
																		<a href="#top" class="floatright">back to top</a>

																		<div class="formspacer"></div>
																	
																	
																	</td>
																</tr>
															</table>
															
															
														
															<span class="cb"><span class="cl"></span></span>
															</div>
															
															<div id="smallpage_spacer"></div>
															<div id="smallpage_spacer"></div>
														</td>
													</tr>
												</table>
					
									
												
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td class=""><img runat="server" src="~/images/spacer.gif" id="Img5" width="1" height="1" /></td>
				</tr>
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
				
			</table>
		</td>
	</tr>
</table>
