<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="duplicatePurchase.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.duplicatePurchase" %>

<link href="../css/new.css" type="text/css" rel="stylesheet">

<br/><br />
<center>
    <table border="0" cellspacing="0" cellpadding="0" width="100%" align="center">
	<tr>
		<td valign="top" align="center">
			<table  border="0" cellpadding="0" cellspacing="0" width="75%">
				<tr>
					<td class="frTopLeft"></td>
					<td class="frTop"></td>
					<td class="frTopRight"></td>
				</tr>
				<tr>
					<td class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img2"/></td>
					<td valign="top" class="frBgIntMembers">
									
						<table cellpadding="0" cellspacing="0" border="0" width="100%">
							<tr>
								<td valign="top" align="center">
									<div class="module whitebg">
										<span class="ct"><span class="cl"></span></span>
											<h2>You already have an Access Pass</h2>
											<table cellpadding="6" cellspacing="0" border="0" width="95%">
												<tr>
													<td align="left" valign="top">
													    You can only have one Access Pass at a time. Access Passes cannot be gifted to other
                                                        members. An access pass is good for one year, you will be allowed to renew prior to
                                                        the access pass expiration date.
                                                        <br /><br /><br />
                                                    </td>
													<td align="right" valign="top"><br /><asp:button id="btnOK" runat="server" text="Visit My Kaneva" onclick="btnOK_Click"></asp:button>
													</td>
												</tr>
											</table>
										<span class="cb"><span class="cl"></span></span>
									</div>
								</td>
							</tr>
						</table>
						
					</td>
					<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img3"/></td>
				</tr>
				<tr>
					<td class="frBottomLeft"></td>
					<td class="frBottom"></td>
					<td class="frBottomRight"></td>
				</tr>
				<tr>
					<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" id="Img4"/></td>
				</tr>
			</table>
		</td>
	</tr>
</table></table>
</center>
<br /><br />