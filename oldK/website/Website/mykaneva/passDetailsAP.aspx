﻿<%@ Page Language="C#" MasterPageFile="~/masterpages/IndexPageTemplate.Master" AutoEventWireup="true" CodeBehind="passDetailsAP.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.passDetailsAP" %>

<asp:Content ID="cnt_passDetails" runat="server" contentplaceholderid="cph_Body" >
<!-- Google Website Optimizer Control Script -->
<script>
	function utmx_section() { } function utmx() { }
	(function () {
		var k = '3204893232', d = document, l = d.location, c = d.cookie; function f(n) {
			if (c) {
				var i = c.indexOf(n + '='); if (i > -1) {
					var j = c.indexOf(';', i); return escape(c.substring(i + n.
length + 1, j < 0 ? c.length : j))
				} 
			} 
		} var x = f('__utmx'), xx = f('__utmxx'), h = l.hash;
		d.write('<sc' + 'ript src="' +
'http' + (l.protocol == 'https:' ? 's://ssl' : '://www') + '.google-analytics.com'
+ '/siteopt.js?v=1&utmxkey=' + k + '&utmx=' + (x ? x : '') + '&utmxx=' + (xx ? xx : '') + '&utmxtime='
+ new Date().valueOf() + (h ? '&utmxhash=' + escape(h.substr(1)) : '') +
'" type="text/javascript" charset="utf-8"></sc' + 'ript>')
	})();
</script><script>         	utmx("url", 'A/B');</script>
<!-- End of Google Website Optimizer Control Script -->
<!-- Google Website Optimizer Tracking Script -->
<script type="text/javascript">
	var _gaq = _gaq || [];
	_gaq.push(['gwo._setAccount', 'UA-5755114-4']);
	_gaq.push(['gwo._trackPageview', '/3204893232/test']);
	(function () {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();
</script>
<!-- End of Google Website Optimizer Tracking Script -->

    <ul class="pageCap"style="margin-top: 10px">
      <li class="topL"><!-- left top --></li>
      <li class="whiteBacker"><!-- center top --></li>
      <li class="topR"><!-- right top --></li>
    </ul>
    <div class="wrapper">
        <div id="div_passDetailsImage" class="passDetailsImage" runat="server">
        </div>
        
        <!-- Level One Access Pass -->
        <div id="div_PassDetailsContent" class="contentContainer" runat="server">
        </div>
        
        <div class="clear"><!-- clear the floats --></div>
        <div class="spTryPass" style="width:200px;float:right;position:relative;top:-100px;"><asp:LinkButton ID="lb_trialPass" runat="server">TRY ME OUT</asp:LinkButton></div>
        <div class="clear"><!-- clear the floats --></div>

    </div>
    <ul class="pageCap">
      <li class="btmL"><!-- left top --></li>
      <li class="whiteBacker"><!-- center top --></li>
      <li class="btmR"><!-- right top --></li>
    </ul>
</asp:Content>