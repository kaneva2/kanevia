///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using log4net;
using System.Text.RegularExpressions;

namespace KlausEnt.KEP.Kaneva.mykaneva
{
    public partial class CreateContest : BasePage
    {
        protected void Page_Load (object sender, EventArgs e)
        {
            if (!Request.IsAuthenticated)
            {
                RedirectToHomePage ();
            }

            // If user trying to open this page directly, redirect to contests page
//            if (!IsPostBack && Request.UrlReferrer != null && Request.UrlReferrer.AbsolutePath != ResolveUrl ("~/mykaneva/contests.aspx"))
//            {
//                Response.Redirect (ResolveUrl("~/mykaneva/contests.aspx"));
//            }

            // Check to make sure user can enter a contest
            int access = contests.CanUserCreateContest;
            switch (access)
            {
                case (int) contests.eCONTEST_ACCESS.Email_Not_Validated:
                    {
                        upNoAccess.Visible = true;
                        spnValidEmail.Visible = true;
                        WriteColorBoxResizeJS ("150", "300");
                        btnOk.Attributes.Add ("onclick", "parent.parent.location='general.aspx';parent.$j.colorbox.close(); return false;");
                        return;
                    }

                case (int) contests.eCONTEST_ACCESS.Below_Min_Fame_Level:
                    {
                        upNoAccess.Visible = true;
                        spnLevelToCreate.Visible = true;
                        litLevelToCreate.Text = Configuration.ContestMinimumFameLevelToCreate.ToString ();
                        WriteColorBoxResizeJS ("150", "300");
                        return;
                    }

                default:
                    break;
            }

  //          WriteColorBoxResizeJS ("600", "460");
            
            if (!IsPostBack)
            {
                upCreate.Visible = true;
 //               SetCalendarDates ();
            }
        }

        #region Methods

        private void SetCalendarDates ()
        {
 //           litCalMinDate.Text = DateTime.Now.AddDays (1).ToShortDateString();
 //           litCalMaxDate.Text = DateTime.Now.AddDays (Configuration.ContestMaxDays).ToShortDateString ();
            
            // Set value in label next to calendar icon
 //           litMaxDays.Text = Configuration.ContestMaxDays.ToString ();
        }

        private bool ValidateTextValue (string value, string fieldName)
        {
            // Check for any inject scripts
            if (KanevaWebGlobals.ContainsInjectScripts (value))
            {
                m_logger.Warn ("User " + KanevaWebGlobals.CurrentUser.UserId.ToString () + " tried to script from IP " + Common.GetVisitorIPAddress());
                ShowError ("Your input contains invalid scripting, please remove script code and try again.");
                return false;
            }

            // Check to make sure user did not enter any "potty mouth" words
            if (KanevaWebGlobals.isTextRestricted (value, Constants.eRESTRICTION_TYPE.POTTY_MOUTH))
            {
                ShowError ("Your input includes some restricted words.  Please remove these and try again.");
                return false;
            }

            if (value.Trim ().Length == 0)
            {
                ShowError ("Please enter a " + fieldName + ".");
                return false;
            }

            if (value.Length > KanevaGlobals.MaxContestLength)
            {
                ShowError ("Your " + fieldName + " is too long");
                return false;
            }
            return true;
        }
        // This is used to resize the colorbox lightbox in Firefox.  For whatever reason
        // FF will not resize correctly to fit the page when it loads so we have to 
        // resize it manually with this javascript
        private void WriteColorBoxResizeJS (string height)
        {
            WriteColorBoxResizeJS (height, "");
        }
        private void WriteColorBoxResizeJS (string height, string width)
        {
            litDocHeight.Text = height + ";";
            if (width.Length > 0)
            {
                litDocHeight.Text += "iW = " + width + ";";
            }
   //         litDocHeight.Text += "parent.$j.fn.colorbox.myResize(iW, iH);";
            litDocHeight.Text += "parent.$j.fn.colorbox.resize({innerWidth:'" + width + "px', innerHeight:'" + height + "'});";
        }
        private void ShowError (string err)
        {
            if (isConfirmationPage)
            {
                divConfirmMsg.InnerText = err;
            }
            else
            {
                divMsg.InnerText = err;
            }
        }
        private void ClearErrMsg ()
        {
            divConfirmMsg.InnerText = "";
        }
        private int GetAmount ()
        {
            int amt = 0;
            if (rbAmt1.Checked)
            {
                return Convert.ToInt32(rbAmt1.Value);
            }
            else if (rbAmt2.Checked)
            {
                return Convert.ToInt32 (rbAmt2.Value);
            }
            else if (rbAmt3.Checked)
            {
                return Convert.ToInt32 (rbAmt3.Value);
            }
            else if (rbAmt4.Checked)
            {
                return Convert.ToInt32 (rbAmt4.Value);
            }
            else if (rbAmt6.Checked)
            {
                return Convert.ToInt32 (rbAmt6.Value);
            }
            else if (rbAmt7.Checked)
            {
                try
                {
                    amt = Convert.ToInt32 (txtAmount.Value);
                }
                catch { } return amt;
            }
            else 
            {
                return 0;
            }
        }
        private void CheckCreditAmount (int amt)
        {
            if (KanevaWebGlobals.CurrentUser.Balances.Credits < amt)
            {
                // User does not have enough credits for this contest
                ShowNotEnoughCreditsMsg (amt);
                return;
            }
            litCreditAmount.Text = amt.ToString ();
            notEnoughCredits.Visible = false;
            contestCost.Visible = true;
            aGetCredits.Visible = false;
            aSave.Visible = true;
            divTerms.Visible = true;
        }
        private void ShowNotEnoughCreditsMsg (int amt)
        {
            litContestCost.Text = amt.ToString ();
            litUserCredits.Text = KanevaWebGlobals.CurrentUser.Balances.Credits.ToString ();

            aGetCredits.Visible = true;
            aSave.Visible = false;
            notEnoughCredits.Visible = true;
            contestCost.Visible = false;
            divConfirmMsg.Visible = false;
            divTerms.Visible = false;
        }

        #endregion Methods

        #region Event Handlers

        public void btnNext_Click (object sender, EventArgs e)
        {
            try
            {
                ClearErrMsg ();
                
                // Confirm user is logged in
                if (!Request.IsAuthenticated)
                {
                    RedirectToHomePage ();
                }

                if (!ValidateTextValue (txtTitle.Text, "Title"))
                {
                    rfvTitle.IsValid = false;
                    return;
                }
                if (!ValidateTextValue (txtDesc.Text, "Description"))
                {
                    rfvDesc.IsValid = false;
                    return;
                }

                if (rbAmt7.Checked)
                {
                    string strAmt = txtAmount.Value.Trim ();
                    if (strAmt.Length == 0)
                    {
                        rfvOtherAmt.IsValid = false;
                        return;
                    }
                    try
                    {
                        int amt = Convert.ToInt32 (strAmt);
                        if (amt < 4000 || amt > 100000)
                        {
                            rvAmount.IsValid = false;
                            ShowError (rvAmount.ErrorMessage);
                            return;
                        }
                    }
                    catch
                    {
                        rvAmount.IsValid = false;
                        return;
                    }
                }

                NewContest c = TempContest;

                c.Title = txtTitle.Text;

                c.Description = txtDesc.Text; 
                c.PrizeAmount = GetAmount ();
                c.CreatedDate = DateTime.Now;
                c.Owner.UserId = KanevaWebGlobals.CurrentUser.UserId;

                // Set the end date including time
                DateTime endDate = Convert.ToDateTime (txtEndDate.Value);
                c.EndDate = new DateTime (endDate.Year, endDate.Month, endDate.Day, DateTime.Now.Hour, DateTime.Now.AddMinutes(-5).Minute, DateTime.Now.Second);

                c.SendBlast = cbBlastMyKaneva.Checked;

                TempContest = c;

                litTitle.Text = c.Title;
                litDescription.Text = c.Description;
                litPrizeAmount.Text = c.PrizeAmount.ToString ();
                litTimeRemaining.Text = c.TimeRemaining;
                litCreator.Text = KanevaWebGlobals.CurrentUser.Username;
                imgThumb.Src = GetProfileImageURL (KanevaWebGlobals.CurrentUser.ThumbnailSquarePath, "sq", 
                    KanevaWebGlobals.CurrentUser.Gender, KanevaWebGlobals.CurrentUser.FacebookSettings.UseFacebookProfilePicture, 
                    KanevaWebGlobals.CurrentUser.FacebookSettings.FacebookUserId);

                upCreate.Visible = false;
                upConfirm.Visible = true;

                // Show number of credits contest will cost
                CheckCreditAmount (c.PrizeAmount);

                // Set min num entries in refund note -- removed with new wording. 
                //litMinimumEntries.Text = Configuration.ContestMinumumEntries.ToString ();
            }
            catch (Exception ex)
            {
                m_logger.Error ("Error creating Temp Contest for New Contest Preview. ", ex);
                ShowError (ex.Message);
            }
        }

        public void btnSave_Click (object sender, EventArgs e)
        {
            try
            {
                ClearErrMsg ();
                isConfirmationPage = true;

                // Confirm user is logged in
                if (!Request.IsAuthenticated)
                {
                    RedirectToHomePage ();
                }

                // Confirm Contest Info
                if (!ValidateTextValue (TempContest.Title, "Title") ||
                    !ValidateTextValue (TempContest.Description, "Description"))
                {
                    return;
                }

                if (TempContest.PrizeAmount < 4000 || TempContest.PrizeAmount > 100000)
                {
                    divConfirmMsg.InnerText = "The Prize Amount must be between 4,000 and 100,000.";
                    return;
                }

                // Verify user has enough credits
                // Display message about not enough credits
                // Does the user have enough credits?
                UInt64 iCredits = Convert.ToUInt64 (UsersUtility.GetUserPointTotal (KanevaWebGlobals.CurrentUser.UserId).ToString ());

                if ((iCredits < Convert.ToUInt64 (TempContest.PrizeAmount)))
                {
                    ShowNotEnoughCreditsMsg (TempContest.PrizeAmount);
                    return;
                }
                                                 
                // Create the contest
                int wokTransLogId = 0;
                TempContest.Title = Server.HtmlEncode (TempContest.Title);
                TempContest.Description = Server.HtmlEncode (TempContest.Description);
                UInt64 contestId = GetContestFacade.CreateContest (TempContest, -TempContest.PrizeAmount, (int) TransactionType.eTRANSACTION_TYPES.CASH_TT_CONTEST, 
                    (int) TransactionType.eCONTEST_TRANSACTION_TYPES.CONTEST_TT_CREATE, Constants.CURR_KPOINT, ref wokTransLogId);

                if (contestId > 1)
                {
                    // Send Blast
                    if (TempContest.SendBlast)
                    {
                        UserFacade userFacade = new UserFacade();
                        string requestId = userFacade.InsertTrackingRequest(Constants.cREQUEST_TRACKING_REQUEST_TYPE_ID_CONTEST_CREATED, KanevaWebGlobals.CurrentUser.UserId, 0, 0);
                        string requestTypeSentIdBlast = userFacade.InsertTrackingTypeSent(requestId, Constants.cREQUEST_TRACKING_MESSAGE_TYPE_ID_BLAST, 0);

                        GetBlastFacade.SendContestCreatedBlast(KanevaWebGlobals.CurrentUser.UserId, ResolveUrl("~/mykaneva/contests.aspx?contestId=" + contestId.ToString() + "&" + Constants.cREQUEST_TRACKING_URL_PARAM + "=" + requestTypeSentIdBlast),
                            TempContest.Title, TempContest.PrizeAmount, KanevaWebGlobals.CurrentUser.Username,
                            KanevaWebGlobals.CurrentUser.NameNoSpaces, KanevaWebGlobals.CurrentUser.ThumbnailSquarePath);
                    }

                    // Close Window. reload contest list with new contest added
                    Page.ClientScript.RegisterStartupScript (this.GetType (), "SaveBtn", "parent.parent.parent.GetFortune();parent.parent.SetDisplayOrder('createdate');parent.$j.colorbox.close();", true);
                }
                else
                {
                    m_logger.Error ("Error creating New Contest.");
                    ShowError ("New contest was not created.  Please try again.");
                }
            }
            catch (Exception ex)
            {
                m_logger.Error ("Error creating Saving New Contest. ", ex);
                ShowError (ex.Message);
            }
        }

        public void btnBack_Click (object sender, EventArgs e)
        {
            if (!Request.IsAuthenticated)
            {
                RedirectToHomePage ();
            }

            upCreate.Visible = true;
            upConfirm.Visible = false;
        }

        #endregion Event Handlers

        #region Properties

        private NewContest TempContest
        {
            get {
                if (ViewState["TempContest"] != null)
                {
                    return (NewContest) ViewState["TempContest"];
                }
                return new NewContest ();
            }
            set 
            {
                ViewState["TempContest"] = value;
            }
        }

        [Serializable]
        private class NewContest : Contest
        {
            private bool _SendBlast = false;

            public NewContest () : base () { }

            public NewContest (UInt64 contestId, DateTime createdDate, DateTime endDate, string title,
                string description, int prizeAmount, bool isActive, int status, int numEntries, int numVotes,
                int numComments, int ownerId, string owner_username, string gender, string thumbnailSmallPath,
                string thumbnailMediumPath, string thumbnailLargePath, string thumbnailSquarePath, UInt64 popularVoteWinnerEntryId,
                UInt64 ownerVoteWinnerEntryId, DateTime currentDate, bool sendBlast)

                : base (contestId, createdDate, endDate, title, description, prizeAmount,
                isActive, status, numEntries, numVotes, numComments, ownerId, owner_username, gender, thumbnailSmallPath,
                thumbnailMediumPath, thumbnailLargePath, thumbnailSquarePath, popularVoteWinnerEntryId, ownerVoteWinnerEntryId, currentDate)
            {
                this._SendBlast = sendBlast;
            }

            public bool SendBlast
            {
                get { return _SendBlast; }
                set { _SendBlast = value; }
            }
        }

        #endregion Properties

        #region Declerations

        private bool isConfirmationPage = false;
        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

        #endregion Declerations
    }
}