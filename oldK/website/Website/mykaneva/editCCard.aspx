<%@ Page language="c#" Codebehind="editCCard.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.editCCard" %>
<%@ Register TagPrefix="Kaneva" TagName="MyKanNav" Src="myKanNav.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="HeaderText" Src="../usercontrols/HeaderText.ascx" %>
<%@ Register TagPrefix="Kaneva" Namespace="KlausEnt.KEP.Kaneva.WebControls" Assembly="Kaneva.WebControls" %>

<Kaneva:MyKanNav runat="server" id="rnNavigation" SubTab="billing"/>
<div class="container">
<center>

<br/>
<table runat="server" cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr><td>
		<asp:ValidationSummary ShowMessageBox="False" ShowSummary="True" Class="formError" id="valSum" DisplayMode="BulletList" runat="server" HeaderText="Please correct the following errors:"/>
	</td></tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr class="lineItemOdd">
		<td align="right" valign="middle" class="filter2"><br>
		<font color="red">*</font> <b>Name On Card:</b>&nbsp;
		</td>
		<td><br>
			<asp:TextBox ID="txtNameOnCard" class="Filter2" style="width:350px" MaxLength="100" runat="server"/>
			<asp:RequiredFieldValidator ID="rftxtNameOnCard" ControlToValidate="txtNameOnCard" Text="*" ErrorMessage="Name on Card is a required field." Display="Dynamic" runat="server"/>
		</td>
	</tr>
	<tr class="lineItemEven">
		<td align="right" valign="middle" class="filter2">
		<font color="red">*</font> <b>Card Type:</b>&nbsp; 
		</td>
		<td colspan="3">
			<Kaneva:CardTypesListBox class="filter2" id="drpCardType" runat="server" style="width:125px" Height="80px" Rows="1">
				<asp:ListItem Value="CardTypes Here">CardTypes Here</asp:ListItem>
			</Kaneva:CardTypesListBox>							
		</td>
	</tr>		
	<tr class="lineItemOdd">
		<td align="right" valign="middle" class="filter2">
		<font color="red">*</font> <b>Card Number:</b>&nbsp;
		</td>
		<td>
			<asp:Label id="lblCardNumber" runat="server"/>
		</td>
	</tr>
	<tr class="lineItemEven">
		<td align="right" valign="middle" class="filter2">
		<font color="red">*</font> <b>Expiration Date:</b>&nbsp; 
		</td>
		<td>
			<asp:DropDownList class="filter2" id="drpMonth" runat="Server" style="width:80px">
				<asp:ListItem Value="01">01 (Jan)</asp:ListItem>
				<asp:ListItem Value="02">02 (Feb)</asp:ListItem>
				<asp:ListItem Value="03">03 (Mar)</asp:ListItem>
				<asp:ListItem Value="04">04 (Apr)</asp:ListItem>
				<asp:ListItem Value="05">05 (May)</asp:ListItem>
				<asp:ListItem Value="06">06 (Jun)</asp:ListItem>
				<asp:ListItem Value="07">07 (Jul)</asp:ListItem>
				<asp:ListItem Value="08">08 (Aug)</asp:ListItem>
				<asp:ListItem Value="09">09 (Sep)</asp:ListItem>
				<asp:ListItem Value="10">10 (Oct)</asp:ListItem>
				<asp:ListItem Value="11">11 (Nov)</asp:ListItem>
				<asp:ListItem Value="12">12 (Dec)</asp:ListItem>
			</asp:DropDownList>
			&nbsp;<asp:DropDownList class="filter2" runat="server" id="drpYear" style="width:60px"> 
				<asp:ListItem Value="2007">2007</asp:ListItem>
				<asp:ListItem Value="2008">2008</asp:ListItem>
				<asp:ListItem Value="2009">2009</asp:ListItem>
				<asp:ListItem Value="2010">2010</asp:ListItem>
				<asp:ListItem Value="2011">2011</asp:ListItem>
				<asp:ListItem Value="2012">2012</asp:ListItem>
				<asp:ListItem Value="2013">2013</asp:ListItem>
				<asp:ListItem Value="2014">2014</asp:ListItem>
				<asp:ListItem Value="2015">2015</asp:ListItem>
				<asp:ListItem Value="2016">2016</asp:ListItem>
				<asp:ListItem Value="2017">2017</asp:ListItem>
				<asp:ListItem Value="2018">2018</asp:ListItem>
				<asp:ListItem Value="2019">2019</asp:ListItem>
				<asp:ListItem Value="2020">2020</asp:ListItem>
			</asp:DropDownList>
		</td>
	</tr>
	<tr>
		<td colspan="2"><br>
		</td>
	</tr>	
	<tr>
		<td colspan="2" align="right"><asp:button runat="Server" CausesValidation="True" onClick="btnCancel_Click" class="Filter2" Text="  cancel  "/>&nbsp;<asp:button runat="Server" CausesValidation="True" onClick="btnUpdateCard_Click" class="Filter2" Text="   submit   "/></td>
	</tr>				
</table>
	
</center>
</div>	<br><br><br><br>