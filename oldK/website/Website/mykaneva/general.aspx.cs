///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using System.Web.Security;
using System.Security.Cryptography;
using KlausEnt.KEP.Kaneva.usercontrols;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using MagicAjax;


namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for general.
	/// </summary>
	public class general : MainTemplatePage
	{
		protected general () 
		{
			Title = "My Account Settings";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			// They must be logged in
			if (!Request.IsAuthenticated)
			{
				Response.Redirect (GetLoginURL ());
				return;
			}

			if (!IsPostBack)
			{
				LoadData ();
			}

            // Check to make sure there is a value for name change cost.  If not, then don't let
            // user continue changing their name.  This is just to make sure all config files have 
            // the NameChangeCost value and it was not forgotten by mistake.
            if (System.Configuration.ConfigurationManager.AppSettings["NameChangeCost"] == null)
            {
                btnChangeNickname.Visible = false;
            }
            
            // Set up regular expression validators
			revEmail.ValidationExpression = Constants.VALIDATION_REGEX_EMAIL;
			revPassword.ValidationExpression = Constants.VALIDATION_REGEX_PASSWORD;
			
			// Set Nav
			HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
			HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.ACCOUNT;
			HeaderNav.MyAccountNav.ActiveTab = NavAccount.TAB.GENERAL;
			HeaderNav.SetNavVisible(HeaderNav.MyKanevaNav,2);
			HeaderNav.SetNavVisible(HeaderNav.MyAccountNav, 3);

            // Determine which email valdiation text they see.
		}

		
		#region Helper Methods

		/// <summary>
		/// LoadData
		/// </summary>
		/// <param name="userid"></param>
		private void LoadData ()
		{
            UserFacade userFacade = new UserFacade();
            User user = KanevaWebGlobals.CurrentUser;
	
			// Is the email address validated?
            
			bool valid = false;
			try
			{
                valid = (user.StatusId.Equals((int)Constants.eUSER_STATUS.REGVALIDATED) && !KanevaWebGlobals.CurrentUser.EmailStatus.Equals((int)Constants.eEMAIL_STATUS.BOUNCED));
			}
			catch(Exception)
			{
			}

			//set text and display settings
			if(valid)
			{
                lblEmailValidation.Text = "\"" + user.Email + "\" has been validated";
				btnRegEmail.Visible = false;
                divValidateMsg.Visible = false;
                divValidateAgainMsg.Visible = false;
                divVerifyBouncedMsg.Visible = false;
			}
			else
			{
                if (KanevaWebGlobals.CurrentUser.EmailStatus.Equals((int)Constants.eEMAIL_STATUS.BOUNCED))
                {
                    lblEmailValidation.Text = "\"" + user.Email + "\", we're unable to reach you!";
                    btnRegEmail.Visible = true;
                    divVerifyBouncedMsg.Visible = true;

                    // For now we are resetting the flag when they view this page whether they re-verify or not.
                    // Call was made so the user doesn't feel nagged.
                    userFacade.UpdateUserEmailStatus(user.UserId, (int)Constants.eEMAIL_STATUS.OK);


                }
                else if(KanevaWebGlobals.CurrentUser.StatusId.Equals((int)Constants.eUSER_STATUS.REGNOTVALIDATED))
                {
                    lblEmailValidation.Text = "\"" + user.Email + "\" has not been verified";
                    btnRegEmail.Visible = true;

                    // If the user has already received the one time registration award show the re-validate message.        
                    FameFacade fameFacade = new FameFacade();
                    if (fameFacade.IsOneTimePacketRedeemed(KanevaWebGlobals.CurrentUser.UserId, (int)PacketId.WEB_OT_EMAIL_VALIATION))
                    {
                        divValidateAgainMsg.Visible = true;
                    }
                    else
                    {
                        divValidateMsg.Visible = true;
                    }                                
                }
			}

			// Set up rest of form
			divUsername.InnerText = KanevaGlobals.TruncateWithEllipsis (user.Username, 50);

			txtDisplayName.Text = Server.HtmlDecode (user.DisplayName);
			txtEmail.Text = Server.HtmlDecode (user.Email);			
			txtConfirmEmail.Text = Server.HtmlDecode (user.Email);

            try
            {
                lblDOB.Text = FormatDate (Convert.ToDateTime (user.BirthDate));
            }
            catch (Exception) { }

            txtPostalCode.Text = Server.HtmlDecode (user.ZipCode);

            // Set up country dropdown
            drpCountry.DataTextField = "Name";
            drpCountry.DataValueField = "CountryId";
            drpCountry.DataSource = WebCache.GetCountries ();
            drpCountry.DataBind ();

            drpCountry.Items.Insert (0, new ListItem ("United States", Constants.COUNTRY_CODE_UNITED_STATES));

            SetDropDownIndex (drpCountry, Server.HtmlDecode (user.Country));

            if (user.Gender.ToLower () == "m")
            {
                lblGender.Text = "Male";
            }
            else
            {
                lblGender.Text = "Female";
            }

            // Facebook Settings
            if (KanevaWebGlobals.CurrentUser.FacebookSettings.FacebookUserId > 0)
            {
                spnFBConnected.Visible = true;
                if (!IsPostBack)
                {
                    rblFacebook.Items[1].Attributes.Add ("onclick", "return ShowConfirm();");
                }
            }
            else  // Not connected
            {
                spnFBNotConnected.Visible = true;
            }
		} 

		#endregion

		#region Event Handlers

		/// <summary>
		/// btnUpdate_Click
		/// </summary>
		protected void btnUpdate_Click (object sender, EventArgs e) 
		{
            UserFacade userFacade = new UserFacade();
            bool informationChanged = false;

            if (drpCountry.SelectedValue.ToUpper () == "US")
            {
                rftxtPostalCode.Enabled = true;
            }
            Page.Validate ();
            if (!Page.IsValid) 
			{
				return;
			}

            string displayName = Server.HtmlEncode(txtDisplayName.Text.Trim());
            string zip = Server.HtmlEncode(txtPostalCode.Text.Trim());
            string country = Server.HtmlEncode(drpCountry.SelectedValue);

            
            // Make sure it is not on the invalid user names list
            if (KanevaWebGlobals.isTextRestricted (displayName, Constants.eRESTRICTION_TYPE.ALL))
            {
                rfdisplayname.ErrorMessage = "Your name contains a restricted word.  Please change your name.";
                rfdisplayname.IsValid = false;
                valSum.Style.Add ("display", "block");
                return;
            }

            // Check to make sure user did not enter any "potty mouth" words
            if (KanevaWebGlobals.isTextRestricted (zip, Constants.eRESTRICTION_TYPE.POTTY_MOUTH))
            {
                rfdisplayname.ErrorMessage = "Invalid Zip Code. Please enter a vaild Zip.";
                rfdisplayname.IsValid = false;
                valSum.Style.Add ("display", "block");
                return;
            }

            int userId = GetUserId ();

            //did they change their display name, zip code, or country? If so set changed flag
            if (!KanevaWebGlobals.CurrentUser.DisplayName.Equals(displayName) ||
                !KanevaWebGlobals.CurrentUser.ZipCode.Equals(zip) ||
                !KanevaWebGlobals.CurrentUser.Country.Equals(country))
            {
                informationChanged = true;
            }

			// Did they change email? Now they must validate it.
            if (!KanevaWebGlobals.CurrentUser.Email.ToLower().Equals(txtEmail.Text.Trim().ToLower()))
			{
				// They cannot change their email if it is already in use
				if (userFacade.EmailExists (txtEmail.Text))
				{
					ShowErrorOnStartup ("The email address you provided is already associated with another Kaneva account. Please provide another email address.");
					return;
				}

                if (KanevaWebGlobals.CurrentUser.StatusId.Equals((int)Constants.eUSER_STATUS.REGVALIDATED))
				{
					userFacade.UpdateUserStatus (userId, (int) Constants.eUSER_STATUS.REGNOTVALIDATED);
				}
                informationChanged = true;

			}

			// Are they changing the password?
            if (cbPassword.Checked)
            {
                if (txtPassword.Text.Trim().Length > 0)
                {
                    string strPassword = Server.HtmlEncode(txtPassword.Text.Trim());
                    string strUserName = Server.HtmlEncode(KanevaWebGlobals.CurrentUser.Username.Trim());

                    // Update the password
                    byte[] salt = new byte[9];
                    new RNGCryptoServiceProvider().GetBytes(salt);
                    string hashPassword = FormsAuthentication.HashPasswordForStoringInConfigFile(UsersUtility.MakeHash(strPassword + strUserName.ToLower()) + Convert.ToBase64String(salt), "MD5");

                    byte[] keyvalue = new byte[10];
                    new RNGCryptoServiceProvider().GetBytes(keyvalue);

                    userFacade.UpdatePassword(userId, hashPassword, Convert.ToBase64String(salt));

                    informationChanged = true;
                }
            }

			// Update user if any info was changed
            if (informationChanged)
            {
                userFacade.UpdateUserAccount(userId, displayName, Server.HtmlEncode(txtEmail.Text.Trim()), country, zip);

                MagicAjax.AjaxCallHelper.WriteAlert ("Your changes were made successfully.");
                Response.Redirect("~/mykaneva/general.aspx");
            }
            else
            {
                MagicAjax.AjaxCallHelper.WriteAlert("No changes were made.");
            }

		}

		/// <summary>
		/// btnUpdate_Click
		/// </summary>
		protected void btnCancel_Click (object sender, EventArgs e) 
		{
			LoadData ();
		}

		/// <summary>
		/// btnRegEmail_Click
		/// </summary>
		protected void btnRegEmail_Click (object sender, EventArgs e) 
		{
            // If the user has already received the one time registration award send them the email change template.        
            FameFacade fameFacade = new FameFacade();
            if (fameFacade.IsOneTimePacketRedeemed(KanevaWebGlobals.CurrentUser.UserId, (int)PacketId.WEB_OT_EMAIL_VALIATION))
            {
                MailUtilityWeb.SendEmailChange(KanevaWebGlobals.CurrentUser.Username, KanevaWebGlobals.CurrentUser.Email, KanevaWebGlobals.CurrentUser.KeyValue, "general");
                ShowErrorOnStartup("Your validation email has been sent.", false);
            }
            else
            {
                MailUtilityWeb.SendVerificationEmail(KanevaWebGlobals.CurrentUser.Username, KanevaWebGlobals.CurrentUser.Email, KanevaWebGlobals.CurrentUser.KeyValue, "general");
                ShowErrorOnStartup("Your validation email has been sent.", false);
            }
 		}

        /// <summary>
        /// btnChangeNickname_Click
        /// </summary>
        protected void btnChangeNickname_Click (object sender, EventArgs e)
        {
            Response.Redirect ("~/mykaneva/namechange/nameChangeIntro.aspx");   
        }

        protected void rblFacebook_SelectedIndexChanged (object sender, EventArgs e)
        {
            if (((RadioButtonList) sender).SelectedValue == "disconnect")
            {
                if (GetUserFacade.DisconnectUserFromFacebook (KanevaWebGlobals.CurrentUser.UserId) > 0)
                {
                    spnFBConnected.Visible = false;
                    spnFBNotConnected.Visible = true;
                }
                rblFacebook.SelectedIndex = 0;

                // Clear the User Cache
                GetUserFacade.InvalidateKanevaUserFacebookCache (KanevaWebGlobals.CurrentUser.UserId);
            }
        }

        #endregion

		#region Declarations
		
        // Account
        protected Label lbl_Newpassword, lbl_Confirmpassword;
		protected TextBox	txtUserName, txtEmail, txtConfirmEmail;
		protected TextBox	txtPassword, txtConfirmPassword, txtDisplayName;
        protected CheckBox cbPassword;
        protected RadioButtonList rblFacebook;
        protected Label lblDOB, lblGender;
        protected TextBox txtPostalCode;
        protected DropDownList drpCountry;
        protected RegularExpressionValidator revEmail, revPassword;
        protected RequiredFieldValidator rftxtPostalCode, rfdrpCountry;

        protected LinkButton btnChangeNickname;

        protected HtmlContainerControl divUsername, divValidateMsg, divValidateAgainMsg, divVerifyBouncedMsg, spnFBNotConnected, spnFBConnected;

		// Not validated email
		protected Label			lblEmailValidation;
		protected Button		btnRegEmail;
		
        protected LinkButton btnCancel, btnUpdate;

		protected ValidationSummary			valSum;
		protected RequiredFieldValidator	rfdisplayname, rfEmail;
		protected CompareValidator			cmpEmail, cmpPassword;

		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion

	}
}
