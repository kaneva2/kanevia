<%@ Page language="c#" Codebehind="inviteHistory.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.inviteHistory" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../usercontrols/Pager.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="SearchFilter" Src="../usercontrols/SearchFilter.ascx" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>

<link href="../css/home.css" rel="stylesheet" type="text/css" />		
<link href="../css/kanevaSC.css" rel="stylesheet" type="text/css"/>
<link href="../css/friends.css" rel="stylesheet" type="text/css"/>
<link href="../css/kanevaText.css" type="text/css" rel="stylesheet">
<link href="../css/kaneva.css" type="text/css" rel="stylesheet">

<link href="../css/new.css" rel="stylesheet" type="text/css" />

<script src="../jscript/prototype.js" type="text/javascript" language="javascript"></script>

<table border="0" cellspacing="0" cellpadding="0"  width="990" align="center">
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="1" height="14"/></td>
	</tr>
	<tr>
		<td>
			<table  border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="frTopLeft"></td>
					<td class="frTop"></td>
					<td class="frTopRight"></td>
				</tr>
				<tr>
					<td class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1"/></td>
					<td valign="top" class="frBgIntMembers">
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
								
									<!-- TOP STATUS BAR -->
									<div id="pageheader">
										<table cellpadding="0" cellspacing="0" border="0" width="99%">
											<tr>
												<td  align="left">
													<h1>Manage your invites</h1>
												</td>
									
												<td align="right" valign="middle">
													<table cellpadding="0" cellspacing="0" border="0" width="690">
														<tr>
															<td>	
																<ajax:ajaxpanel id="ajpTopStatusBar" runat="server">
																<table cellpadding="0" cellspacing="0" border="0" width="100%">
																	<tr>
																		<td class="headertout" width="175">
																			<asp:label runat="server" id="lblSearch" />
																		</td>
																		<td class="searchnav" width="260"> Sort by: 
																			<asp:linkbutton id="lbSortByDate" runat="server" sortby="Date" onclick="lbSortBy_Click">Date Sent</asp:linkbutton> | 
																			<asp:linkbutton id="lbSortByEmail" runat="server" sortby="Email" onclick="lbSortBy_Click">Email Address</asp:linkbutton>
																		</td>
																		<td align="left" width="130">
																			<kaneva:searchfilter runat="server" id="searchFilter" loc="top" hidethumbview="true" showfilterlists="false" assetsperpagelist="10,20,40" />
																		</td>
																		<td class="searchnav" align="right" width="210" nowrap>
																			<kaneva:pager runat="server" isajaxmode="True" id="pgTop" maxpagestodisplay="5" shownextprevlabels="true" />
																		</td>
																	</tr>
																</table>
																</ajax:ajaxpanel>
															</td>	
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</div>
									<!-- END TOP STATUS BAR -->		
								
								</td>
							</tr>
							<tr>
								<td width="968" align="left"  valign="top">
									<table cellpadding="0" cellspacing="0" border="0"  width="968">
										<tr>
											<td width="250" valign="top">
												<div class="module toolbar">
													<span class="ct"><span class="cl"></span></span>
													<h2>Tools & Actions <span class="hlink"></span></h2>
													<table cellspacing="0" cellpadding="10" border="0" width="99%">
														<tr>
															<td align="center">
																
																<a runat="server" href="~/mykaneva/inviteFriend.aspx">Invite a Friend</a>
																
																<hr /><br />
																
																<table cellspacing="0" cellpadding="3" border="0" width="100%">
																	<tr>
																		<td>Delete selected:</td>
																		<td valign="bottom">
																		
																			<ajax:ajaxpanel id="ajpDeleteButton" runat="server">
																				<asp:imagebutton imageurl="~/images/button_delete.gif" alt="Delete" width="77" height="23" border="0" id="btnDeleteInvites" runat="server" onclick="btnDelete_Click"></asp:imagebutton>
																			</ajax:ajaxpanel>
																			
																		</td>
																	</tr>
																</table>
																
																<hr /><br />
																
																<p align="left">You have gained <span id="spnFamePts" runat="server"></span> Fame points by inviting new members to Kaneva.</p>
																<br />
																<p align="left">Friends who accepted are in your <a runat="server" href="~/mykaneva/friendsGroups.aspx">Friends List</a></p>
																<br />
																<p align="left">You can only resend an invitation once every two weeks. The resend button will be active once the two week period has ended.</p>
																
															</td>
														</tr>
														
													</table>
													<span class="cb"><span class="cl"></span></span>
												</div>
											</td>
											<td width="20"><img runat="server" src="~/images/spacer.gif" width="20" height="1" id="Img3"/></td>
											<td width="698" valign="top" align="center">
												
												<ajax:ajaxpanel id="ajpHistoryList" runat="server">
												<div class="module whitebg">
													<span class="ct"><span class="cl"></span></span>
													<h2 class="alertmessage"><span id="spnAlertMsg" runat="server"></span></h2>
												
													<div align="left" style="padding-left:3px;">
													
													<!-- Friend Requests -->
													<asp:repeater runat="server" id="rptInvites">  
														
														<headertemplate>
															<table width="99%" cellspacing="0" cellpadding="5" border="0" class="data">
																<tr>
																	<th align="center" width="60" bgcolor="#fdfdfc" class="small">Select:<br><a href="javascript:void(0);" onclick="Select_All(true);">All</a> | <a href="javascript:void(0);" onclick="Select_All(false);">None</a></th>
																	<th bgcolor="#fdfdfc">Date</th>
																	<th align="center" bgcolor="#fdfdfc">Attempts</th>
																	<th align="center" bgcolor="#fdfdfc">Email</th>
																	<th bgcolor="#fdfdfc">Status</th>
																	<th bgcolor="#fdfdfc">Actions</th>
																</tr>
														</headertemplate>
														
														<itemtemplate>
															
															<tr class="altrow">
																<td align="center" height="65"><asp:checkbox runat="server" id="chkEdit" /></td>
																<td align="center"><%# FormatDateOnly(DataBinder.Eval(Container.DataItem, "reinvite_date")) %></td>
																<td align="center"><%# DataBinder.Eval(Container.DataItem, "attempts") %></td>
																<td align="center"><%# DataBinder.Eval(Container.DataItem, "email") %></td>
																<td align="center"><%# GetStatusName(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "invite_status_id"))) %></td>
																<td align="center">	
																	<div><asp:imagebutton imageurl="~/images/button_resend.gif" alternatetext="Resend" runat="server" width="77" height="23" border="0" vspace="2" id="btnResend" commandname="cmdResend" /></div>
																	<asp:imagebutton imageurl="~/images/button_delete.gif" alternatetext="Delete" runat="server" width="77" height="23" border="0" vspace="2" id="btnDelete" commandname="cmdDelete" />
																	<input type="hidden" runat="server" id="hidInviteId" value='<%#DataBinder.Eval(Container.DataItem, "invite_id")%>' name="hidInviteId">
																</td>	  
															</tr>
																																													
														</itemtemplate>
														
														<alternatingitemtemplate>
															
															<tr>
																<td align="center" height="65"><asp:checkbox runat="server" id="chkEdit" /></td>
																<td align="center"><%# FormatDateOnly(DataBinder.Eval(Container.DataItem, "reinvite_date")) %></td>
																<td align="center"><%# DataBinder.Eval(Container.DataItem, "attempts") %></td>
																<td align="center"><%# DataBinder.Eval(Container.DataItem, "email") %></td>
																<td align="center"><%# GetStatusName(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "invite_status_id"))) %></td>
																<td align="center">	
																	<div><asp:imagebutton imageurl="~/images/button_resend.gif" alternatetext="Resend" runat="server" width="77" height="23" border="0" vspace="2" id="btnResend" commandname="cmdResend" /></div>
																	<asp:imagebutton imageurl="~/images/button_delete.gif" alternatetext="Delete" runat="server" width="77" height="23" border="0" vspace="2" id="btnDelete" commandname="cmdDelete" />
																	<input type="hidden" runat="server" id="hidInviteId" value='<%#DataBinder.Eval(Container.DataItem, "invite_id")%>' name="hidInviteId">
																</td>	  
															</tr>
																	
														</alternatingitemtemplate>
														
														<footertemplate>
														
															</table>
														
														</footertemplate>
														
													</asp:repeater>
													
													</div>
											
													<span class="cb"><span class="cl"></span></span>
												</div>
												</ajax:ajaxpanel>
												
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img6"/></td>
				</tr>
				<tr>
					<td class="frBottomLeft"></td>
					<td class="frBottom"></td>
					<td class="frBottomRight"></td>
				</tr>
				<tr>
					<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" id="Img7"/></td>
				</tr>
			</table>
		</td>
	</tr>
</table>

