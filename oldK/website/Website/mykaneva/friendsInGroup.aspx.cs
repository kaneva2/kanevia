///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva.channel;

using System.Collections.Generic;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.mykaneva
{
	/// <summary>
	/// Summary description for friendsInGroup.
	/// </summary>
	public class friendsInGroup : SortedBasePage
	{
		protected friendsInGroup () 
		{
			Title = "Friends in Group";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			if(Request.IsAuthenticated)
			{
				if(GetRequestParams())
				{
					//check if user is allowed to edit this channel
					if(!IsUserAllowedToEdit())
					{
						RedirectToHomePage ();
					}
				}
				else
				{
					//invalid request params
					//if the user logged in, send him to his home page
					//if not, redirect to login page
					RedirectToHomePage ();
				}
			}
			else
			{
				Response.Redirect(this.GetLoginURL());
			}

			if (!IsPostBack)
			{
				CurrentSort = "username";
				lbSortByName.CssClass = "selected";

				BindData (1, _userId);
			}

			// Set Nav
            HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
			HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.FRIENDS;
			HeaderNav.MyFriendNav.ActiveTab = NavFriend.TAB.NONE;
			HeaderNav.SetNavVisible(HeaderNav.MyKanevaNav,2);
			HeaderNav.SetNavVisible(HeaderNav.MyFriendNav);

			int friendGroupId = Convert.ToInt32 (Request ["friendGroupId"]);

            UserFacade userFacade = new UserFacade();
            FriendGroup friendGroup = userFacade.GetFriendGroup(friendGroupId, _userId);
            lblGroup.Text = friendGroup.Name;
		}

		/// <summary>
		/// Bind the data
		/// </summary>
		/// <param name="pageNumber"></param>
		private void BindData (int pageNumber, int userId)
		{
			BindData (pageNumber, userId, "");
		}
		/// <summary>
		/// Bind the data
		/// </summary>
		/// <param name="pageNumber"></param>
		private void BindData (int pageNumber, int userId, string filter)
		{
			searchFilter.CurrentPage = "friendsingroup";

			string orderby = CurrentSort + " " + CurrentSortOrder;
			int pgSize = searchFilter.NumberOfPages;

            int friendGroupId = Convert.ToInt32(Request["friendGroupId"]);

            UserFacade userFacade = new UserFacade();
            FriendGroup friendGroup = userFacade.GetFriendGroup(friendGroupId, userId);

            IList<Friend> friends = userFacade.GetFriendsInGroup(friendGroupId, userId, "", orderby, pageNumber, pgSize);

            dlFriends.DataSource = friends;
			dlFriends.DataBind ();

            pgTop.NumberOfPages = Math.Ceiling((double)friendGroup.FriendCount / pgSize).ToString();
			pgTop.DrawControl ();

			// The results
            lblSearch.Text = GetResultsText ((int) friendGroup.FriendCount, pageNumber, pgSize, friends.Count);
		}

		
		#region Helper Methods
		/// <summary>
		/// Get values from the request object
		/// </summary>
		/// <returns></returns>
		private bool GetRequestParams()
		{
			bool retVal = true;
			try
			{
				_userId = this.GetUserId();
			}
			catch(Exception)
			{
				//invalid numbers
				retVal = false;
			}
			return retVal;
		}

		/// <summary>
		/// returns true if current user is allowed to edit this channel
		/// </summary>
		/// <returns></returns>
		private bool IsUserAllowedToEdit()
		{
			return IsAdministrator() || _userId == this.GetUserId();
		}

		#endregion

		#region Event Handlers
		/// <summary>
		/// Page Change
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pgTop_PageChange (object sender, PageChangeEventArgs e)
		{
			BindData (e.PageNumber, _userId);
		}

		/// <summary>
		/// Execute when the user clicks the the view type link button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void lbSortBy_Click (object sender, EventArgs e) 
		{
			lbSortByName.CssClass = lbSortByDate.CssClass = "";

			if (((LinkButton)sender).Attributes["sortby"].ToString().ToLower() == "name")
			{
				CurrentSort = "username";
				lbSortByName.CssClass = "selected";
			}
			else
			{
				CurrentSort = "glued_date";
				lbSortByDate.CssClass = "selected";							  
			}

			BindData (pgTop.CurrentPageNumber, _userId);
		}
		/// <summary>
		/// Execute when the user selects an item from the Store Filter
		/// Items to display drop down list
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void FilterChanged(object sender, FilterChangedEventArgs e)
		{
			searchFilter.SetPagesDropValue(searchFilter.NumberOfPages.ToString());
			
			BindData (1, _userId);
		}
		/// <summary>
		/// Execute when the user clicks the Remove button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnRemove_Click (object sender, ImageClickEventArgs e)
		{
			HtmlInputHidden hidFriendId;
			CheckBox chkEdit;
			int ret = 0;
			int count = 0;
			bool isItemChecked = false;

			// Remove the friends
			foreach (DataListItem dliFriend in dlFriends.Items)
			{
				chkEdit = (CheckBox) dliFriend.FindControl ("chkEdit");

				if (chkEdit.Checked)
				{
					isItemChecked = true;

					hidFriendId = (HtmlInputHidden) dliFriend.FindControl ("hidFriendId");
					ret = UsersUtility.RemoveFriendFromGroup (_userId, Convert.ToInt32 (Request ["friendGroupId"]), Convert.ToInt32 (hidFriendId.Value));
				
					if (ret == 1)
						count++;
				}
			}

			if (isItemChecked)
			{
				if (count > 0)
				{
					BindData (pgTop.CurrentPageNumber, _userId);
				}
				
				if (ret != 1)
				{
					spnAlertMsg.InnerText = "An error was encountered while trying to remove Friends.";
				}
				else
				{
					spnAlertMsg.InnerText = "Successfully removed " + count + " friend" + (count != 1 ? "s" : "") + ".";
				}
			}
			else
			{
				spnAlertMsg.InnerText = "No friends were selected.";
			}

			MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('spnAlertMsg', 5000);");
		}
		#endregion

		#region Properties
		/// <summary>
		/// DEFAULT_SORT
		/// </summary>
		/// <returns></returns>
		protected override string DEFAULT_SORT
		{
			get
			{
				return "username";
			}
		}

		#endregion

		#region Declerations
		protected Kaneva.Pager pgTop;
		protected Kaneva.SearchFilter searchFilter;

		protected Label lblSearch, lblGroup;
		protected LinkButton lbSortByName, lbSortByDate;
		protected HtmlContainerControl	spnAlertMsg;

		protected DataList dlFriends;

		private int _userId;
		#endregion

		#region Web Form Designer generated code

		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
			pgTop.PageChanged +=new PageChangeEventHandler (pgTop_PageChange);
			searchFilter.FilterChanged +=new FilterChangedEventHandler (FilterChanged);		
		}
		#endregion
	}
}
