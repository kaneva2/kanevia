<%@ Page language="c#" Codebehind="editAddress.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.editAddress" %>
<%@ Register TagPrefix="Kaneva" TagName="MyKanNav" Src="myKanNav.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="HeaderText" Src="../usercontrols/HeaderText.ascx" %>

<Kaneva:HeaderText runat="server" Text="Billing Info - Edit Address"/>
<Kaneva:MyKanNav runat="server" id="rnNavigation" SubTab="billing"/>
<center>
<br/>
<table runat="server" cellpadding="0" cellspacing="0" border="0" width="730">
	<tr><td>
		<asp:ValidationSummary ShowMessageBox="False" ShowSummary="True" Class="formError" id="valSum" DisplayMode="BulletList" runat="server" HeaderText="Please correct the following errors:"/>
	</td></tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="730">
	<tr class="lineItemOdd">
		<td align="right" valign="middle" class="filter2">
			<br>
			<font class="requiredField">*</font> <b>Full Name:</b>&nbsp;
		</td>
		<td>
			<br>
			<asp:TextBox ID="txtFullName" class="Filter2" style="width:350px" MaxLength="100" runat="server"/>
			<asp:RequiredFieldValidator ID="rfUsername" ControlToValidate="txtFullName" Text="*" ErrorMessage="Full Name is a required field." Display="Dynamic" runat="server"/>
		</td>
	</tr>
	<tr class="lineItemEven">
		<td align="right" valign="middle" class="filter2">
		<font class="requiredField">*</font> <b>Address 1:</b>&nbsp; 
		</td>
		<td>
			<asp:TextBox ID="txtAddress1" class="Filter2" style="width:350px" MaxLength="100" runat="server"/>
				<asp:RequiredFieldValidator ID="rfAddress1" ControlToValidate="txtAddress1" Text="*" ErrorMessage="Address1 is a required field." Display="Dynamic" runat="server"/>
		</td>
	</tr>		
	<tr class="lineItemOdd">
		<td align="right" valign="middle" class="filter2">
		<b>Address 2:</b>&nbsp;
		</td>
		<td>
			<asp:TextBox ID="txtAddress2" class="Filter2" style="width:350px" MaxLength="80" runat="server"/>
		</td>
	</tr>
	<tr class="lineItemEven">
		<td align="right" valign="middle" class="filter2">
		<font class="requiredField">*</font> <b>City:</b>&nbsp; 
		</td>
		<td>
			<asp:TextBox ID="txtCity" class="Filter2" style="width:200px" MaxLength="100" runat="server"/>
				<asp:RequiredFieldValidator ID="rftxtCity" ControlToValidate="txtCity" Text="*" ErrorMessage="City is a required field." Display="Dynamic" runat="server"/>
		</td>
	</tr>		
	<tr class="lineItemOdd">
		<td align="right" valign="middle" class="filter2">
			<font class="requiredField">**</font> <b>State:</b>&nbsp;
		</td>
		<td>
			<asp:Dropdownlist runat="server" class="Filter2" ID="drpState" style="width:250px">
			</asp:Dropdownlist>
			<asp:RequiredFieldValidator ID="rfdrpState" ControlToValidate="drpState" Text="*" ErrorMessage="State is a required field when country is United States or Canada." Display="Dynamic" runat="server"/>
		</td>
	</tr>
	<tr class="lineItemEven">
		<td align="right" valign="middle" class="filter2">
			<font class="requiredField">**</font> <b>Postal Code:</b>&nbsp;
		</td>
		<td>
			<asp:TextBox ID="txtPostalCode" class="Filter2" style="width:100px" MaxLength="25" runat="server"/>
			<asp:RequiredFieldValidator ID="rftxtPostalCode" ControlToValidate="txtPostalCode" Text="*" ErrorMessage="Postal code is a required field." Display="Dynamic" runat="server"/>
		</td>
	</tr>
	<tr class="lineItemOdd">
		<td align="right" valign="middle" class="filter2">
			<font class="requiredField">*</font> <b>Country:</b>&nbsp;
		</td>
		<td>
			<asp:Dropdownlist runat="server" class="Filter2" ID="drpCountry">
			</asp:Dropdownlist>
			<asp:RequiredFieldValidator ID="rfdrpCountry" ControlToValidate="drpCountry" Text="*" ErrorMessage="Country is a required field." Display="Dynamic" runat="server"/>
		</td>
	</tr>
	<tr class="lineItemEven">
		<td align="right" valign="middle" class="filter2">
			<font class="requiredField">*</font> <b>Phone Number:</b>&nbsp;
		</td>
		<td>
			<asp:TextBox ID="txtPhoneNumber" class="Filter2" style="width:100px" MaxLength="15" runat="server"/>
			<asp:RequiredFieldValidator ID="rftxtPhoneNumber" ControlToValidate="txtPhoneNumber" Text="*" ErrorMessage="Phone Number is a required field." Display="Dynamic" runat="server"/>
		</td>
	</tr>
	<tr>
		<td colspan="2" align="right"><asp:button runat="Server" CausesValidation="False" onClick="btnCancel_Click" class="Filter2" Text="  cancel  "/>&nbsp;<asp:button id="btnContinue" runat="Server" CausesValidation="False" onClick="btnUpdateAddress_Click" class="Filter2" Text="  submit  "/></td>
	</tr>		
	<tr>
		<td colspan="2" align="left" class="bodyText">
			<br/>
			<font class="requiredField">*</font> Indicates required fields.<br/>
			<font class="requiredField">**</font> Indicates required fields for United States and Canada.<br/>
		</td>
	</tr>
</table>		

</center><br><br><br><br>