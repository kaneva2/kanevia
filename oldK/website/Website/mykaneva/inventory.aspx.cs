///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Xml;

using System.Collections.Generic;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using log4net;

namespace KlausEnt.KEP.Kaneva.mykaneva
{
    public partial class inventory : SortedBasePage
    {
        protected void Page_Load (object sender, EventArgs e)
        {
            // They must be logged in
            if (!Request.IsAuthenticated)
            {
                Response.Redirect (GetLoginURL ());
                return;
            }

            BindData (1, 1);

            // add event to search text field
            txtSearch.Attributes.Add ("onkeydown", "CheckKey()");

            // Add the javascript for deleting comments
            string scriptString = "<script language=\"javaScript\">\n<!--\n function CheckKey () {\n";
            scriptString += "if (event.keyCode == 13)\n";
            scriptString += "{\n";
            scriptString += this.ClientScript.GetPostBackEventReference (btnSearch, "", false) + ";\n";
            scriptString += "}}\n// -->\n";
            scriptString += "</script>";

            if (!ClientScript.IsClientScriptBlockRegistered (GetType (), "CheckKey"))
            {
                ClientScript.RegisterClientScriptBlock (GetType (), "CheckKey", scriptString);
            }
            
            // Set Nav
            HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
            HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.ACCOUNT;
            HeaderNav.MyAccountNav.ActiveTab = NavAccount.TAB.INVENTORY;
            HeaderNav.SetNavVisible (HeaderNav.MyKanevaNav, 2);
            HeaderNav.SetNavVisible (HeaderNav.MyAccountNav, 3);
        }


        #region Helper Methods

        /// <summary>
        /// BindData
        /// </summary>
        private void BindData (int pageNumber, int userId)
        {
            try
            {
                if (!IsPostBack)
                {
                    BindCategories ();
                    SearchString = "";

                    // Select the All Inventory Items node from categores tree view
                    TreeNode tn = tvCategories.FindNode ("Categories/0");
                    if (tn != null)
                    {
                        // select default node
                        tn.Select ();

                        CategoryId = Convert.ToInt32 (tn.Value);
                        CategoryName = tn.Text;

                        // get inventory items in default node category
                        BindInventory (pageNumber);
                    }
                    else
                    {
                        CategoryId = 4;
                        CategoryName = "Homes"; 
                        BindInventory (pageNumber);
                    }

                    btnMyMerch.Attributes.Add ("onclick", "window.open('http://" + KanevaGlobals.ShoppingSiteName + "/MyInventory.aspx','mywindow','')");
                }
            }
            catch (Exception ex)
            {
                ShowErrMessage ("There was an error retrieving the inventory categories.", ex.Message);
            }
        }

        /// <summary>
        /// Get the categories and bind to tree view control
        /// </summary>
        private void BindCategories ()
        {
            // Get the categories xml
            XmlDocument xmlDoc = new XmlDocument ();
            xmlDoc.LoadXml (GetCachedCategoryXML ());

            // The category tree contains both 'For Men' and 'For Women' nodes.  We need to only show
            // the children under the valid node for the current user.  The categories tree looks like this:
            // Clothing
            //    For Men
            //       Tops
            //       Bottoms
            //       Shoes
            //       etc...
            // For a male user we only want to show the Clothing node with Tops,Bottoms,etc. as children and 
            // not display the 'For Men' node.  The code below finds the relevant piece of the xml based on gender
            // and rewrites it to remove the non-relevant gender node and rewrite the relevant gender node.

            // Set the xpath for the clothing node
            string xpathClothing = "Categories/Level1[@name='Clothing']";
            
            // Set the gender description based on current users gender.  The gender description
            // has to match the item category names that are in the database
            string genderDesc = "For Women";
            if (KanevaWebGlobals.CurrentUser.Gender.ToUpper () == "M")
            {
                genderDesc = "For Men";
            }

            // Get the node for the relevant gender (ie. 'For Men' if current user is male) and
            // get the nodes inner xml which is all it's children and sub-children
            string genderNode = xmlDoc.SelectSingleNode (xpathClothing + "/Level2[@name='" + genderDesc + "']").InnerXml;

            // Select the 'Clothing' node and set it's inner xml value which will be it's new children.  The
            // new children will be Tops, Bottoms, Shoes, etc.
            xmlDoc.SelectSingleNode (xpathClothing).InnerXml = genderNode;

            // Create an XmlDataSource since this is what we need to bind to the treeview control
            XmlDataSource xmlDS = new XmlDataSource ();
            
            // Set the XmlDataSource data value to the categories xml
            xmlDS.Data = xmlDoc.InnerXml;
            xmlDS.ID = "xmlDSCats";

            // Set the tree view's data source and bind the data
            tvCategories.DataSource = xmlDS;
            tvCategories.DataBind ();

            // Set the top node (Categories) so that it will not fire a click event when user clicks on it
            tvCategories.FindNode ("Categories").SelectAction = TreeNodeSelectAction.None;
            tvCategories.FindNode ("Categories").ImageUrl = "";
        }

        /// <summary>
        /// Get the cached categories xml string
        /// </summary>
        private string GetCachedCategoryXML ()
        {
            string cacheKey = "_ShopCatsXML";
            string xmlCategories = string.Empty;
            if (Cache[cacheKey] != null)
            {
                xmlCategories = Cache[cacheKey].ToString ();
            }

            // If not cached version of xml string, create it
            if (xmlCategories == string.Empty)
            {
                // Add to the cache
                xmlCategories = CreateCategoriesXML ();
                Cache.Insert (cacheKey, xmlCategories, null, DateTime.Now.AddMinutes (15), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return xmlCategories;
        }

        /// <summary>
        /// Add a category node to the xml
        /// </summary>
        private void AddCategoryNode (ItemCategory itemCategory)
        {
            List<ItemCategory> itemCategories = GetCachedCategories (itemCategory.ItemCategoryId);
            
            // If the current category has children
            if (itemCategories.Count > 0)
            {
                if (itemCategory.ParentCategoryId != 0)
                {
                    indentLevel++;
                }

                // Start element & Attributes
                xmlWriter.WriteStartElement ("Level" + indentLevel);
                xmlWriter.WriteAttributeString ("name", itemCategory.Name);
                xmlWriter.WriteAttributeString ("catId", itemCategory.ItemCategoryId.ToString ());

                // Walk the tree and get all child nodes
                foreach (ItemCategory ic in itemCategories)
                {
                    AddCategoryNode (ic);
                }

                // End element
                xmlWriter.WriteEndElement ();
  
                if (indentLevel > 1) indentLevel--;
            }
            else
            {
                // Write start & end elements and attributes
                xmlWriter.WriteStartElement ("Level" + indentLevel);
                xmlWriter.WriteAttributeString ("name", itemCategory.Name);
                xmlWriter.WriteAttributeString ("catId", itemCategory.ItemCategoryId.ToString ());
                xmlWriter.WriteEndElement ();
            }
        }

        /// <summary>
        /// Create XmlDocument object containing invetory categories
        /// </summary>
        private string CreateCategoriesXML ()
        {
            ShoppingFacade shoppingFacade = new ShoppingFacade ();

            // Get the Parent categories
            List<ItemCategory> itemParentCategories = GetCachedCategories ((uint) (0));

            // Add top category
            StringBuilder sb = new StringBuilder ();
            xmlWriter = XmlWriter.Create (sb);
            xmlWriter.WriteStartElement ("Categories");

            // Create the ALL node
            xmlWriter.WriteStartElement ("Level1");
            xmlWriter.WriteAttributeString ("name", "All Inventory Items");
            xmlWriter.WriteAttributeString ("catId", "0");
            xmlWriter.WriteEndElement ();

            // Walk the tree and get all child nodes
            foreach (ItemCategory ic in itemParentCategories)
            {
                if (ic.ItemCategoryId > 0)
                {
                    AddCategoryNode (ic);
                }
            }
            
            // Write xml
            xmlWriter.WriteEndElement ();
            xmlWriter.Close ();

            return sb.ToString ();
        }

        /// <summary>
        /// Get the cached categories list
        /// </summary>
        private List<ItemCategory> GetCachedCategories (uint ItemCategoryId)
        {
            ShoppingFacade shoppingFacade = new ShoppingFacade ();
            string cacheKey = "_ShopCats" + ItemCategoryId.ToString ();
            List<ItemCategory> itemCategories = (List<ItemCategory>) Cache[cacheKey];
         
            // if no cached object for category id children, get them from db
            if (itemCategories == null)
            {
                // Add to the cache
                itemCategories = shoppingFacade.GetItemCategoriesByParentCategoryId (ItemCategoryId);
                Cache.Insert (cacheKey, itemCategories, null, DateTime.Now.AddMinutes (15), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return itemCategories;
        }

        /// <summary>
        /// Get the inventory data and show results
        /// </summary>
        private void BindInventory (int pageNumber)
        {
            try
            {
                // Hide error/message box
                divMessages.Style.Add ("display", "none");

                // populate category name label
                lblCategoryName.Text = CategoryName;
                
                // Set the sortable columns
                CurrentSort = "display_name";
                CurrentSortOrder = "DESC";
                string orderby = ""; //CurrentSort + " " + CurrentSortOrder;

                PagedDataTable pdt;

                // if CategoryId = 0 and there is a search string value, do inventory search
                if (CategoryId == 0 && SearchString.Length > 0)
                {
                    pdt = UsersUtility.SearchUserInvetory (KanevaWebGlobals.CurrentUser.UserId, SearchString, orderby, pageNumber, _PGSIZE);
                }
                else // else get inventory items by category
                {
                    pdt = UsersUtility.GetUserInvetoryByCategory (KanevaWebGlobals.CurrentUser.UserId, CategoryId, orderby, pageNumber, _PGSIZE);
                }

                if (pdt.Rows.Count > 0)
                {
                    rptInventory.DataSource = pdt;
                    rptInventory.DataBind ();
                    divInventory.Style.Add ("display", "block");
                    divNoResults.Style.Add ("display", "none");
                }
                else
                {
                    divInventory.Style.Add ("display", "none");
                    divNoResults.InnerHtml = "No " + CategoryName + " inventory items found.";
                    divNoResults.Style.Add ("display", "block");
                }

                ShowResults (Convert.ToUInt32(pdt.TotalCount), pdt.Rows.Count, pageNumber, _PGSIZE);
            }
            catch (Exception ex)
            {
                ShowErrMessage ("There was an error retrieving your inventory items.", ex.Message);
            }
        }

        /// <summary>
        /// Return the url to details page of item on shopping site 
        /// </summary>
        public string GetItemDetailLink (string globalId)
        {
            return "http://" + KanevaGlobals.ShoppingSiteName + "/ItemDetails.aspx?gId=" + globalId;  
        }

        /// <summary>
        /// Show the Pagers and results count 
        /// </summary>
        private void ShowResults (uint totalCount, int count, int pageNumber, int pgSize)
        {                                                                                           
            // populate top pager
            pgTop.NumberOfPages = Math.Ceiling ((double) totalCount / pgSize).ToString ();
            pgTop.CurrentPageNumber = pageNumber;
            pgTop.DrawControl ();

            // populate bottom pager
            pgBottom.CurrentPageNumber = pageNumber;
            pgBottom.NumberOfPages = Math.Ceiling ((double) totalCount / pgSize).ToString ();
            pgBottom.DrawControl ();

            // The results
            lblResults_Top.Text = lblResults_Top.Text = GetResultsText (totalCount, pageNumber, pgSize, count);
            lblResults_Bottom.Text = lblResults_Top.Text;
        }

        /// <summary>
        /// Display the error message box 
        /// </summary>
        private void ShowErrMessage (string msg, string exception)
        {
            m_logger.Warn (exception);
            divMessages.InnerHtml = msg + " " + exception;
            divMessages.Style.Add ("display", "block");
        }

        /// <summary>
        /// Get the text description for invnentory status
        /// </summary>
        public string GetInventoryStatus (string invType, string invLoc)
        {
            if (invLoc.ToLower () == "pnd inv")
            {
                return "Pending";
            }
            else if (invType == "P")
            {
                return "In Use";
            }
            else
            {
                return "In Storage";
            }
        }

        #endregion Helper Methods


        #region Event Handlers

        /// <summary>
        /// Click event for Pager
        /// </summary>
        protected void pg_PageChange (object sender, PageChangeEventArgs e)
        {
            pgTop.CurrentPageNumber = e.PageNumber;
            pgTop.DrawControl ();
            pgBottom.CurrentPageNumber = e.PageNumber;
            pgBottom.DrawControl ();

            // get the inventory items
            BindInventory (e.PageNumber);
        }

        /// <summary>
        /// Click event for Search button
        /// </summary>
        protected void btnSearch_Click (object sender, EventArgs e)
        {
            // check to make sure input does not contain any injection scripts
            if (KanevaWebGlobals.ContainsInjectScripts (txtSearch.Value))
            {
                ShowErrMessage ("Your input contains invalid scripting, please remove script code and try again.", "User " + KanevaWebGlobals.GetUserId () + " tried to script from IP " + Common.GetVisitorIPAddress());
                return;
            }

            // if a node is selected, unselect it
            if (tvCategories.SelectedNode != null)
            {
                tvCategories.SelectedNode.Selected = false;
            }

            // set search value
            SearchString = txtSearch.Value.Trim ();

            // clear search text box
            txtSearch.Value = "";
            
            // set the category values
            CategoryId = 0;
            CategoryName = "Search Results: " + SearchString;

            //search for inventory
            BindInventory (1);
        }

        /// <summary>
        /// Click event for Categories tree view control
        /// </summary>
        protected void tvCategories_OnClick (object sender, EventArgs e)
        {
            // get the treeview control
            TreeView tv = (TreeView) sender;

            // Set the category values
            CategoryId = Convert.ToInt32 (tv.SelectedValue);
            CategoryName = tv.SelectedNode.Text;
            SearchString = "";

            // get the inventory items
            BindInventory (1);
        }

        #endregion Event Handlers


        #region Properties

        /// <summary>
        /// Holds the current search string value
        /// </summary>
        private string SearchString
        {
            set
            {
                ViewState["SearchString"] = value.Trim ();
            }
            get
            {
                if (ViewState["SearchString"] == null)
                {
                    return string.Empty; 
                }
                else
                {
                    return ViewState["SearchString"].ToString ();
                }
            }
        }

        /// <summary>
        /// Holds the current category id value
        /// </summary>
        private int CategoryId
        {
            set
            {
                ViewState["CategoryId"] = value;
            }
            get
            {
                if (ViewState["CategoryId"] == null)
                {
                    return 0;
                }
                else
                {
                    return Convert.ToInt32(ViewState["CategoryId"]);
                }
            }
        }

        /// <summary>
        /// Holds the current category name value
        /// </summary>
        private string CategoryName
        {
            set
            {
                ViewState["CategoryName"] = value.Trim ();
            }
            get
            {
                if (ViewState["CategoryName"] == null)
                {
                    return string.Empty;
                }
                else
                {
                    return ViewState["CategoryName"].ToString ();
                }
            }
        }
        
        #endregion Properties


        #region Declerations

        private int indentLevel = 1;
        private XmlWriter xmlWriter;

        const int _PGSIZE = 20;

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

        #endregion Declerations
    }
}
