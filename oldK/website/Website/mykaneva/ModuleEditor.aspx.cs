///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva.framework.widgets;
using KlausEnt.KEP.Kaneva.mykaneva.widgets;

namespace KlausEnt.KEP.Kaneva.mykaneva
{
	/// <summary>
	/// Summary description for ModuleEditor.
	/// </summary>
	public class ModuleEditor : BasePage
	{
		private int		_pageModuleId;
		private int		_moduleId;
		private int		_zoneId;
		private int		_sequence;
		private int		_pageId;
		private bool	_from_edit;

		protected PlaceHolder placeHolderEditorControl;
		protected Label lblHeader;

		private void Page_Load(object sender, System.EventArgs e)
		{
			if(Request.IsAuthenticated)
			{
				if(!GetRequestParams())
				{
					//invalid request params
					//if the user logged in, send him to his home page
					//if not, redirect to login page
					RedirectToHomePage ();
				}
			}else
			{
				Response.Redirect (this.GetLoginURL ());
			}

			LoadModuleEditorControl();
		}

		
		/// <summary>
		/// parse request params, returns false if any params are invliad
		/// </summary>
		/// <returns></returns>
		private bool GetRequestParams()
		{
			bool retVal = true;
			try
			{
				string modidStr = Request.Params["modid"];
				if(modidStr.StartsWith("new"))
				{
					char seperator = '_';
					int begin = modidStr.IndexOf(seperator) + 1;
					int end = modidStr.IndexOf(seperator, begin);
					ModuleId = Int32.Parse(modidStr.Substring(begin, end - begin));
					begin = end + 1;
					end = modidStr.IndexOf(seperator, begin);
					ZoneId = Int32.Parse(modidStr.Substring(begin, end - begin));
					begin = end + 1;
					end = modidStr.IndexOf(seperator, begin);
					Sequence = Int32.Parse(modidStr.Substring(begin, end - begin));

					//"new_3_3_0_ms__id6196"
					//new module
					_pageModuleId = -1;
				}
				else
				{
					_pageModuleId = Int32.Parse(modidStr);
				}

				_pageId = Int32.Parse(Request.Params["pageId"]);

				_from_edit = Request.Params["fromEdit"] != null ? Convert.ToBoolean( Request.Params["fromEdit"].ToString() ) : false;
			}catch(Exception)
			{
				retVal = false;
			}

			return retVal;
		}

		/// <summary>
		/// returns true if current user is allowed to edit this module
		/// </summary>
		/// <returns></returns>
		private bool IsUserAllowedToEdit(int channelId)
		{
			return IsAdministrator() || CommunityUtility.IsCommunityModerator(channelId, GetUserId());
		}

		/// <summary>
		/// load the editor control into the page
		/// </summary>
		private void LoadModuleEditorControl()
		{
			DataRow drChannel = PageUtility.GetChannelByPageId(_pageId);
			//check if user is allowed to edit this channel
			if(!IsUserAllowedToEdit(Convert.ToInt32(drChannel["community_id"])))
			{
				RedirectToHomePage ();
			}

			placeHolderEditorControl.Controls.Clear();
			
			int modulePageId = 0;

			if(_pageModuleId > 0)
			{
				DataRow dr = WidgetUtility.GetLayoutPageModule(PageModuleId);

				if(dr != null)
				{
					ModuleId = Int32.Parse(dr["module_id"].ToString());
					modulePageId = Int32.Parse(dr["module_page_id"].ToString());
				}
			}

			lblHeader.Text = WebCache.GetModuleTitle( ModuleId );

			ModuleEditBaseControl control = null;
			switch(ModuleId)
			{
				case (int) Constants.eMODULE_TYPE.MENU:
					control = (ModuleEditBaseControl) LoadControl ("widgets/ModuleMenuEdit.ascx");
					placeHolderEditorControl.Controls.Add(control);
					break;

                case (int)Constants.eMODULE_TYPE.FAME_PANEL:
                    control = (ModuleEditBaseControl)LoadControl("widgets/ModuleFameEdit.ascx");
                    placeHolderEditorControl.Controls.Add(control);
                    break;


				case (int) Constants.eMODULE_TYPE.TITLE_TEXT:
					control = (ModuleEditBaseControl) LoadControl ("widgets/ModuleTitleEdit.ascx");
					placeHolderEditorControl.Controls.Add(control);
					break;

				case (int) Constants.eMODULE_TYPE.FRIENDS:
					control = (ModuleEditBaseControl) LoadControl ("widgets/ModuleFriendsEdit.ascx");
					placeHolderEditorControl.Controls.Add(control);
					break;

				case (int) Constants.eMODULE_TYPE.BLOGS:
					control = (ModuleEditBaseControl) LoadControl ("widgets/ModuleBlogEdit.ascx");
					placeHolderEditorControl.Controls.Add(control);
					break;

				case (int) Constants.eMODULE_TYPE.CHANNELS:
					control = (ModuleEditBaseControl) LoadControl ("widgets/ModuleChannelEdit.ascx");
					placeHolderEditorControl.Controls.Add(control);
					break;

				case (int) Constants.eMODULE_TYPE.COMMENTS:
					control = (ModuleEditBaseControl) LoadControl ("widgets/ModuleCommentsEdit.ascx");
					placeHolderEditorControl.Controls.Add(control);
					break;
				
				case (int) Constants.eMODULE_TYPE.HTML_CONTENT:
					control = (ModuleEditBaseControl) LoadControl ("widgets/ModuleHtmlEdit.ascx");
					placeHolderEditorControl.Controls.Add(control);
					break;

                case (int)Constants.eMODULE_TYPE.HTML_CONTENT_BASIC:
                    control = (ModuleEditBaseControl)LoadControl("widgets/ModuleHtml2Edit.ascx");
                    placeHolderEditorControl.Controls.Add(control);
                    break;

				case (int) Constants.eMODULE_TYPE.USER_UPLOAD:
					control = (ModuleEditBaseControl) LoadControl ("widgets/ModuleUserUploadEdit.ascx");
					placeHolderEditorControl.Controls.Add(control);
					break;

				case (int) Constants.eMODULE_TYPE.SYSTEM_STATS:
					control = (ModuleEditBaseControl) LoadControl ("widgets/ModuleStatsEdit.ascx");
					placeHolderEditorControl.Controls.Add(control);
					break;
				
				case (int) Constants.eMODULE_TYPE.MY_COUNTER:
					control = (ModuleEditBaseControl) LoadControl ("widgets/ModuleCounterEdit.ascx");
					placeHolderEditorControl.Controls.Add(control);
					break;

				case (int) Constants.eMODULE_TYPE.MY_PICTURE:
					control = (ModuleEditBaseControl) LoadControl ("widgets/ModuleMyPictureEdit.ascx");
					placeHolderEditorControl.Controls.Add(control);
					break;
				
				case (int) Constants.eMODULE_TYPE.CHANNEL_OWNER:
					control = (ModuleEditBaseControl) LoadControl ("widgets/ModuleMyPictureEdit.ascx");
					placeHolderEditorControl.Controls.Add(control);
					break;
				
				case (int) Constants.eMODULE_TYPE.NEW_PEOPLE:
					control = (ModuleEditBaseControl) LoadControl ("widgets/ModuleNewPeopleEdit.ascx");
					placeHolderEditorControl.Controls.Add(control);
					break;

				case (int) Constants.eMODULE_TYPE.MULTIPLE_PICTURES:
					control = (ModuleEditBaseControl) LoadControl ("widgets/ModuleMultiplePicturesEdit.ascx");
					placeHolderEditorControl.Controls.Add(control);
					break;

				case (int) Constants.eMODULE_TYPE.SLIDE_SHOW:
					control = (ModuleEditBaseControl) LoadControl ("widgets/ModuleSlideShowEdit.ascx");
					placeHolderEditorControl.Controls.Add(control);
					break;

				case (int) Constants.eMODULE_TYPE.MUSIC_PLAYER:
					control = (ModuleEditBaseControl) LoadControl ("widgets/ModuleStoreEdit.ascx");
					placeHolderEditorControl.Controls.Add(control);
					break;

				case (int) Constants.eMODULE_TYPE.VIDEO_PLAYER:
					control = (ModuleEditBaseControl) LoadControl ("widgets/ModuleStoreEdit.ascx");
					placeHolderEditorControl.Controls.Add(control);
					break;

				case (int) Constants.eMODULE_TYPE.GAMES_PLAYER:
					control = (ModuleEditBaseControl) LoadControl ("widgets/ModuleStoreEdit.ascx");
					placeHolderEditorControl.Controls.Add(control);
					break;

				case (int) Constants.eMODULE_TYPE.SINGLE_PICTURE:
					control = (ModuleEditBaseControl) LoadControl ("widgets/ModuleSinglePictureEdit.ascx");
					placeHolderEditorControl.Controls.Add(control);
					break;

				case (int) Constants.eMODULE_TYPE.PROFILE:
					control = (ModuleEditBaseControl) LoadControl ("widgets/ModuleProfileEdit.ascx");
					placeHolderEditorControl.Controls.Add(control);
					break;
				
				case (int) Constants.eMODULE_TYPE.CHANNEL_MEMBERS:
					control = (ModuleEditBaseControl) LoadControl ("widgets/ModuleChannelMembersEdit.ascx");
					placeHolderEditorControl.Controls.Add(control);
					break;

				case (int) Constants.eMODULE_TYPE.CONTROL_PANEL:
					control = (ModuleEditBaseControl) LoadControl ("widgets/ModuleControlPanelEdit.ascx");
					placeHolderEditorControl.Controls.Add(control);
					break;
				
				case (int) Constants.eMODULE_TYPE.CHANNEL_EVENTS:
					control = (ModuleEditBaseControl) LoadControl ("widgets/ModuleEventsEdit.ascx");
					placeHolderEditorControl.Controls.Add(control);
					break;
				
				case (int) Constants.eMODULE_TYPE.CHANNEL_FORUM:
					control = (ModuleEditBaseControl) LoadControl ("widgets/ModuleChannelForumEdit.ascx");
					placeHolderEditorControl.Controls.Add(control);
					break;
				
				case (int) Constants.eMODULE_TYPE.CHANNEL_CONTROL_PANEL:
					control = (ModuleEditBaseControl) LoadControl ("widgets/ModuleChannelControlPanelEdit.ascx");
					placeHolderEditorControl.Controls.Add(control);
					break;
				
				case (int) Constants.eMODULE_TYPE.CHANNEL_DESCRIPTION:
					control = (ModuleEditBaseControl) LoadControl ("widgets/ModuleChannelDescriptionEdit.ascx");
					placeHolderEditorControl.Controls.Add(control);
					break;

				case (int) Constants.eMODULE_TYPE.MIXED_MEDIA:
					control = (ModuleEditBaseControl) LoadControl ("../mykaneva/widgets/ModuleMixedMediaEdit.ascx");
					placeHolderEditorControl.Controls.Add(control);
					break;
				
//				case (int) Constants.eMODULE_TYPE.CHANNEL_MEMBERS_MAP:
//					control = (ModuleEditBaseControl) LoadControl ("../mykaneva/widgets/ModuleChannelMembersMapEdit.ascx");
//					placeHolderEditorControl.Controls.Add(control);
//					break;

				case (int) Constants.eMODULE_TYPE.OMM_VIDEO_PLAYER:
					control = (ModuleEditBaseControl) LoadControl ("../mykaneva/widgets/ModuleMediaEdit.ascx");
					placeHolderEditorControl.Controls.Add(control);
					break;
				
				case (int) Constants.eMODULE_TYPE.OMM_VIDEO_PLAYLIST:
					control = (ModuleEditBaseControl) LoadControl ("../mykaneva/widgets/ModuleMediaPlaylistEdit.ascx");
					placeHolderEditorControl.Controls.Add(control);
					break;
				
				case (int) Constants.eMODULE_TYPE.OMM_MUSIC_PLAYER:
					control = (ModuleEditBaseControl) LoadControl ("../mykaneva/widgets/ModuleMediaEdit.ascx");
					placeHolderEditorControl.Controls.Add(control);
					break;
				
				case (int) Constants.eMODULE_TYPE.OMM_MUSIC_PLAYLIST:
					control = (ModuleEditBaseControl) LoadControl ("../mykaneva/widgets/ModuleMediaPlaylistEdit.ascx");
					placeHolderEditorControl.Controls.Add(control);
					break;
				
				case (int) Constants.eMODULE_TYPE.TOP_CONTRIBUTORS:
					control = (ModuleEditBaseControl) LoadControl ("../mykaneva/widgets/ModuleChannelTopContributorsEdit.ascx");
					placeHolderEditorControl.Controls.Add(control);
					break;

				case (int) Constants.eMODULE_TYPE.CONTEST_SUBMISSIONS:
					control = (ModuleEditBaseControl) LoadControl ("../mykaneva/widgets/ModuleContestSubmissionsEdit.ascx");
					placeHolderEditorControl.Controls.Add(control);
					break;

				case (int) Constants.eMODULE_TYPE.CONTEST_UPLOAD:
					control = (ModuleEditBaseControl) LoadControl ("../mykaneva/widgets/ModuleContestUploadEdit.ascx");
					placeHolderEditorControl.Controls.Add(control);
					break;

				case (int) Constants.eMODULE_TYPE.CONTEST_GET_RAVED:
					control = (ModuleEditBaseControl) LoadControl ("../mykaneva/widgets/ModuleContestGetRavedEdit.ascx");
					placeHolderEditorControl.Controls.Add(control);
					break;

				case (int) Constants.eMODULE_TYPE.USER_GIFTS:
					control = (ModuleEditBaseControl) LoadControl ("../mykaneva/widgets/ModuleGiftListEdit.ascx");
					placeHolderEditorControl.Controls.Add(control);
					break;

				case (int) Constants.eMODULE_TYPE.CONTEST_PROFILE_HEADER:
					control = (ModuleEditBaseControl) LoadControl ("../mykaneva/widgets/ModuleContestProfileHeaderEdit.ascx");
					placeHolderEditorControl.Controls.Add(control);
					break;

				case (int) Constants.eMODULE_TYPE.COMMUNITY_TOP_BANNER:
					control = (ModuleEditBaseControl) LoadControl ("../mykaneva/widgets/ModuleCommunityTopBannerEdit.ascx");
					placeHolderEditorControl.Controls.Add(control);
					break;

				case (int) Constants.eMODULE_TYPE.CONTEST_TOP_RESULTS:
					control = (ModuleEditBaseControl) LoadControl ("../mykaneva/widgets/ModuleContestTopVoteGettersEdit.ascx");
					placeHolderEditorControl.Controls.Add(control);
					break;

				case (int) Constants.eMODULE_TYPE.PROFILE_PORTAL:
					control = (ModuleEditBaseControl) LoadControl ("../mykaneva/widgets/ModuleProfilePortalEdit.ascx");
					placeHolderEditorControl.Controls.Add(control);
					break;

				case (int) Constants.eMODULE_TYPE.CHANNEL_PORTAL:
					control = (ModuleEditBaseControl) LoadControl ("../mykaneva/widgets/ModuleChannelPortalEdit.ascx");
					placeHolderEditorControl.Controls.Add(control);
					break;

				case (int) Constants.eMODULE_TYPE.LEADER_BOARD:
					control = (ModuleEditBaseControl) LoadControl ("../mykaneva/widgets/ModuleLeaderBoardEdit.ascx");
					placeHolderEditorControl.Controls.Add(control);
					break;

				case (int) Constants.eMODULE_TYPE.KANEVA_LEADER_BOARD:
					control = (ModuleEditBaseControl) LoadControl ("../mykaneva/widgets/ModuleKanevaLeaderBoardEdit.ascx");
					placeHolderEditorControl.Controls.Add(control);
					break;

				case (int) Constants.eMODULE_TYPE.NEWEST_MEDIA:
					control = (ModuleEditBaseControl) LoadControl ("../mykaneva/widgets/ModuleNewestMediaEdit.ascx");
					placeHolderEditorControl.Controls.Add(control);
					break;

				case (int) Constants.eMODULE_TYPE.HOT_NEW_STUFF:
					control = (ModuleEditBaseControl) LoadControl ("../mykaneva/widgets/ModuleHotNewStuffEdit.ascx");
					placeHolderEditorControl.Controls.Add(control);
					break;

				case (int) Constants.eMODULE_TYPE.BILLBOARD:
					control = (ModuleEditBaseControl) LoadControl ("../mykaneva/widgets/ModuleBillBoardEdit.ascx");
					placeHolderEditorControl.Controls.Add(control);
					break;

				case (int) Constants.eMODULE_TYPE.MOST_VIEWED:
					control = (ModuleEditBaseControl) LoadControl ("../mykaneva/widgets/ModuleMostViewedEdit.ascx");
					placeHolderEditorControl.Controls.Add(control);
					break;

                case (int) Constants.eMODULE_TYPE.INTERESTS:
                    control = (ModuleEditBaseControl) LoadControl ("../mykaneva/widgets/ModuleInterestsEdit.ascx");
                    placeHolderEditorControl.Controls.Add (control);
                    break;

                case (int) Constants.eMODULE_TYPE.PERSONAL:
                    control = (ModuleEditBaseControl) LoadControl ("../mykaneva/widgets/ModulePersonalEdit.ascx");
                    placeHolderEditorControl.Controls.Add (control);
                    break;

                default:
					break;
			}
			if(control != null)
			{
				control.ChannelId = Convert.ToInt32(drChannel["community_id"]);
				control.PageModuleId = modulePageId;
				control.ModuleId = _moduleId;
				control.ZoneId = _zoneId;
				control.Sequence = _sequence;
				control.PageId = _pageId;
				control.ReturningToView = !_from_edit;
			}
			
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion

		public int PageModuleId
		{
			get { return _pageModuleId; }
			set { _pageModuleId = value; }
		}

		public int ModuleId
		{
			get { return _moduleId; }
			set { _moduleId = value; }
		}

		public int ZoneId
		{
			get { return _zoneId; }
			set { _zoneId = value; }
		}

		public int Sequence
		{
			get { return _sequence; }
			set { _sequence = value; }
		}
	}
}
