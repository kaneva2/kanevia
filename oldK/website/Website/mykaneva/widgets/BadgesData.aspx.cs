///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects.API;
using log4net;

namespace KlausEnt.KEP.Kaneva.mykaneva.widgets
{
    public partial class BadgesData : BasePage
    {
        #region Declarations
        
        private int communityId = 0;
        private int appCommunityId = 0;
        private uint badgeId = 0;
        private uint badgePoints = 0;
        private string badgeName = "";
        private string badgeDesc = "";

        private int profileOwnerId = 0;
        private int userId = 0;
        private bool isProfileOwner = false;
        private bool isProfileAdmin = false;

        private int pageSize = 10;
        private int pageNum = 1;
        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

        #endregion
        
        #region PageLoad
        
        protected void Page_Load (object sender, EventArgs e)
        {
//            if (Request.IsAuthenticated)
//            {
                try
                {
                    //clears local cache so data is always new
                    Response.ContentType = "text/plain";
                    Response.CacheControl = "no-cache";
                    Response.AddHeader ("Pragma", "no-cache");
                    Response.Expires = -1;

                    GetRequestValues ();
                    CheckUserAccess ();

                    // Configure page based on viewing from user profile or community
                    ConfigurePage ();

                    // Show the totals
                    GetSummaryData ();

                    // Show the badges
                    BindData ();
                }
                catch (Exception ex)
                {
                    m_logger.Error ("Error loading Forums widget. ", ex);

                    divNoData.InnerText = "We're sorry but the data could not be retrieved.";
                    divNoData.Visible = true;

                    // Always call this method so the totalRecords value will get updated
                    // and the pager will be hidden if it is not needed
                    litJS.Text = "<script type=\"text/javascript\">SetAjaxPager(1,0);</script>";
                }
//            }
        }
        
        #endregion

        #region Functions

        private void ConfigurePage ()
        {
            if (ViewOnProfilePage)
            {
                // Set back button
                aBack.HRef = "javascript:ProfileFame(" + CommunityId + ");";
                aBack.Visible = true;
                                                  
                // Hide Manage Button
                aEdit.Visible = false;

                // Show the 3D App Name
                gameName.InnerText = GetCommunityFacade.GetCommunity (AppCommunityId).Name;
            }
            else 
            {
                if (UserCanEdit ())
                {
                    // Set manage button
                    aEdit.HRef = this.ResolveUrl ("~/community/3dapps/AppManagement.aspx?communityid=" + CommunityId);
                    aEdit.Visible = true;
                    
                    // Hide back button
                    aBack.Visible = false;
                }
                else
                {
                    aEdit.Visible = false;
                    aBack.Visible = false;
                    //divTitle.Visible = false;
                }

                gameName.InnerText = "You have earned the following badges.";
                gameName.Style.Add ("color", "#3d77c4");
                gameName.Style.Add ("font-weight", "normal");
            }
        }

        private void GetSummaryData ()
        {
            try
            {
                Dictionary<string, int> dSum = GetGameFacade.GetAchievementsSummaryForApp (AppCommunityId, ShowFameForUserId);

                if (dSum.Count > 0)
                {
                    int val = 0;

                    // Get users total points 
                    if (dSum.TryGetValue ("UserTotalPoints", out val))
                    {
                        spnUserPoints.InnerText = val.ToString ();
                    }

                    // Get total points of all achievements
                    if (dSum.TryGetValue ("TotalPoints", out val))
                    {
                        spnTotalPoints.InnerText = val.ToString ();
                    }

                    // Get users total number of achievements
                    if (dSum.TryGetValue ("UserTotalAchievements", out val))
                    {
                        spnUserBadges.InnerText = val.ToString ();
                    }

                    // Get total number of achievements for 3D App
                    if (dSum.TryGetValue ("TotalAchievements", out val))
                    {
                        spnTotalBadges.InnerText = val.ToString ();
                    }
                }
            }
            catch { }
        }

        private void BindData ()
        {
            // Check to see if owner has created a leaderboard for this 3D App
            PagedList<UserAchievementData> achievements = GetGameFacade.GetUserAchievementsForApp (AppCommunityId, ShowFameForUserId, "", PageNum, 200);
            PagedList<UserAchievementData> unachieved = new PagedList<UserAchievementData> ();
            PagedList<UserAchievementData> achieved = new PagedList<UserAchievementData> ();
            
            foreach (UserAchievementData uad in achievements)
            {
                if (uad.CreatedDate == DateTime.MinValue)
                {
                    unachieved.Add (uad);
                }
                else
                {
                    achieved.Add (uad);
                }   
            }
            achieved.TotalCount = (uint)achieved.Count;
            unachieved.TotalCount = (uint)unachieved.Count;
            if (unachieved.TotalCount > 0)
            {
                rptUnearned.DataSource = unachieved;
                rptUnearned.DataBind ();
            }
            else
            {
                divUnearnedBadges.Visible = true;
            }

            if (achieved.TotalCount > 0)
            {
                rptBadges.DataSource = achieved;    
                rptBadges.DataBind ();
            }
            else
            {
                divNoEarnedBadges.Visible = true;
            }
            
            if (unachieved.TotalCount + achieved.TotalCount == 0)
            {
                divNoData.Visible = true;
                divNoData.InnerHtml = "<div>This World does not have any badges.</div>";

                // Don't show the summary section
                divSummary.Visible = false;

                // Always call this method so the totalRecords value will get updated
                // and the pager will be hidden if it is not needed
                litJS.Text = "<script type=\"text/javascript\">SetAjaxPager(1,0);</script>";

                return;
            }
            // Always call this method so the totalRecords value will get updated
            // and the pager will be hidden if it is not needed
            //litJS.Text = "<script type=\"text/javascript\">SetAjaxPager(" + PageSize.ToString () + "," + achieved.TotalCount.ToString () + ");</script>";
        
            // Always hide the pager since we are displaying in two columns
            litJS.Text = "<script type=\"text/javascript\">SetAjaxPager(1,0);</script>";
        }
        
        private void GetRequestValues ()
        {
            // Get the current community Id
            if (Request["communityId"] != null)
            {
                try
                {
                    CommunityId = Convert.ToInt32 (Request["communityId"]);
                }
                catch { }
            }

            // Get the community Id
            if (Request["appCommunityId"] != null)
            {
                try
                {
                    AppCommunityId = Convert.ToInt32 (Request["appCommunityId"]);
                }
                catch { }
            }

            // Get items per page count
            if (Request["pagesize"] != null)
            {
                try
                {
                    PageSize = Convert.ToInt32 (Request["pagesize"]);
                }
                catch { }
            }

            // Get the current page number
            if (Request["p"] != null)
            {
                try
                {
                    PageNum = Convert.ToInt32 (Request["p"]);
                }
                catch { }
            }

            // Get the user Id from the parameters 
            if (Request["profileOwnerId"] != null)
            {
                try
                {
                    ProfileOwnerId = Convert.ToInt32 (Request["profileOwnerId"]);
                }
                catch { }
            }
            else
            {
                try
                {
                    ProfileOwnerId = GetCommunityFacade.GetCommunity (CommunityId).CreatorId;
                }
                catch { }
            }
        }

        /// <summary>
        /// Checks the current user's access level
        /// </summary>
        private void CheckUserAccess ()
        {
            //get the users Id
            userId = GetUserId ();

            //see if they are the profile/community owner
            isProfileOwner = userId.Equals (profileOwnerId);

            if (!isProfileOwner)
            {
                //check the privilege level to see if they are an admin
                this.isProfileAdmin = HasWritePrivileges ((int) SitePrivilege.ePRIVILEGE.COMMUNITY_PROFILE_ADMIN);
            }
        }

        private bool UserCanEdit ()
        {
            bool allowEditing = false;

            //check security level
            switch (KanevaWebGlobals.CheckUserAccess ((int) SitePrivilege.ePRIVILEGE.COMMUNITY_PROFILE_ADMIN))
            {
                case (int) SitePrivilege.eACCESS_LEVEL.ACCESS_FULL:
                    allowEditing = true;
                    break;
                case (int) SitePrivilege.eACCESS_LEVEL.ACCESS_READ:
                    break;
                case (int) SitePrivilege.eACCESS_LEVEL.ACCESS_NONE:
                    break;
                default:
                    break;
            }

            return allowEditing;
        }

        public string GetAcquisitionDate (object obj)
        {
            try
            {
                DateTime dt = Convert.ToDateTime (obj);
                if (dt > DateTime.MinValue)
                {
                    return "Acquired " + dt.ToShortDateString ();
                }
                else
                {
                    return "Locked";
                }
            }
            catch { }
            return "Locked";
        }

 
        #endregion Functions


        #region Properties

        private int ProfileOwnerId
        {
            set { this.profileOwnerId = value; }
            get { return this.profileOwnerId; }
        }
        private int CommunityId
        {
            set { this.communityId = value; }
            get { return this.communityId; }
        }
        private int AppCommunityId
        {
            set { this.appCommunityId = value; }
            get { return this.appCommunityId; }
        }
        private int PageNum
        {
            set { this.pageNum = value; }
            get { return this.pageNum; }
        }
        private int PageSize
        {
            set { this.pageSize = value; }
            get { return this.pageSize; }
        }
        private uint BadgeId
        {
            set { this.badgeId = value; }
            get { return this.badgeId; }
        }
        private string BadgeName
        {
            set { this.badgeName = value; }
            get { return this.badgeName; }
        }
        private string BadgeDesc
        {
            set { this.badgeDesc = value; }
            get { return this.badgeDesc; }
        }
        private uint BadgePoints
        {
            set { this.badgePoints = value; }
            get { return this.badgePoints; }
        }
        private int ShowFameForUserId
        {
            get 
            {
                // Viewing fame for a user on their profile page
                if (ViewOnProfilePage)
                {
                    return ProfileOwnerId;
                }
                else  // Viewing fame on the 3D App community page
                {
                    return KanevaWebGlobals.CurrentUser.UserId;
                }
            }
        }
        private bool ViewOnProfilePage
        {
            get { return CommunityId != AppCommunityId; }
        }

        #endregion Properties

    }
}