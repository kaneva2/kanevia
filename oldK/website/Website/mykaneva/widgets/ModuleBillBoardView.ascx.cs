///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System.Web.UI;
using KlausEnt.KEP.Kaneva.framework.widgets;

namespace KlausEnt.KEP.Kaneva.mykaneva.widgets
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	using MagicAjax;

	/// <summary>
	///		Summary description for ModuleBillBoardView.
	/// </summary>
	public class ModuleBillBoardView : ModuleViewBaseControl
	{

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			if (!IsPostBack)
			{
				BindData (1);
			}

			//open the editor window when user clicks edit button
			lbEdit.Attributes ["onclick"] = "window.open('../mykaneva/ModuleEditor.aspx?modid=" + this.InstanceId
				+"&pageId=" + PageId + "' , 'ModuleEditor', 'width=750,height=680,scrollbars=1,resizable=1,status=1,toolbar=0'); return false;";

			if (!IsPostBack)
			{
				if (Request ["assetId"] != null && Request ["contestId"] != null)
				{
					LaunchVideo (Convert.ToInt32 (Request ["assetId"]), Convert.ToInt32 (Request ["contestId"]));
				}
			}
		}

		/// <summary>
		/// BindData
		/// </summary>
		private void BindData (int pageNumber)
		{
			tblEdit.Visible = ShowEdit;
			bool showMature = false;
			int results2Display = 3;
			int resultsPerPage = 20;
			PagedDataTable dtTopRanked = null;
			DataRow drBillBoard =  null;
			int image_size = (int) Constants.ePICTURE_SIZE.SMALL;

			try
			{
				drBillBoard = WidgetUtility.GetModuleBillboard (InstanceId);
			}
			catch(Exception ex)
			{
				string error = ex.Message;
				lbl_Error.Text = "Unable to configure control.";
			}


			if (drBillBoard != null)
			{
				try
				{
					//gather textual settings
					divTitle.InnerHtml	= drBillBoard ["title"].ToString();
					lblIntroText.Text = drBillBoard ["header"].ToString ();
					lblFooterText.Text = drBillBoard ["footer"].ToString ();

					results2Display = Convert.ToInt32 (drBillBoard ["results_to_return"]);
					resultsPerPage = Convert.ToInt32 (drBillBoard ["results_per_page"]);

                    showMature = KanevaWebGlobals.CurrentUser.HasAccessPass;

					//sets the size of the images
					image_size = Convert.ToInt32(drBillBoard ["image_size"]);
					switch(image_size)
					{
							//sets various pieces needed for image size
						case (int) Constants.ePICTURE_SIZE.THUMBNAIL:
							imageSizeStyleSheet  = "framesize-xsmall";
							imagePath = "thumbnail_small_path";
							imageSizeAbbrv = "sm";
							break;
						case (int) Constants.ePICTURE_SIZE.LARGE:
							imageSizeStyleSheet  = "framesize-large";
							imagePath = "thumbnail_large_path";
							imageSizeAbbrv = "la";
							break;
						case (int) Constants.ePICTURE_SIZE.MEDIUM:
							imageSizeStyleSheet  = "framesize-medium";
							imagePath = "thumbnail_medium_path";
							imageSizeAbbrv = "me";
							break;
						case (int) Constants.ePICTURE_SIZE.SMALL:
						default:
							imageSizeStyleSheet  = "framesize-small";
							imagePath = "thumbnail_small_path";
							imageSizeAbbrv = "sm";
							break;
					}

					//chose appropriate query and execute it
					//value of 30 hardcoded for time frame due to time constraint and lack of need for other time periods 
					//at this point GetBillboardItems designed to accept 1, 7, 30, 365 - works with leaderboard time
					//periods table
					dtTopRanked = StoreUtility.GetBillboardItems (imagePath, imageSizeAbbrv, results2Display, showMature, true, 1, this.ChannelId, 30 ); 

					if((dtTopRanked != null)&&(dtTopRanked.Rows.Count > 0))
					{
						// Add column to hold the rank
						DataColumn column = new DataColumn (); 
						column = new DataColumn (); 
						column.DataType = System.Type.GetType ("System.String"); 
						column.AllowDBNull = false; 
						column.ColumnName = "rank"; 
						column.DefaultValue = 0;
						dtTopRanked.Columns.Add(column);

						// get the appropriate rank for each tag
						int iRank = 1;
						if ((dtTopRanked != null)&&(dtTopRanked.Rows.Count > 0))
						{
							for (int i = 0; i < dtTopRanked.Rows.Count; i ++)
							{
								//pdtSongCloud.Rows[i]["font_size"] = GetTagFontSize(i/5);
								dtTopRanked.Rows[i]["rank"] = iRank;

								//check to see if the next one has the same numeber of raves

								// first check to see if there is another one after this one?
								if (i < dtTopRanked.Rows.Count - 1)
								{
									// Does the next one have the same amount of raves?
									if (!Convert.ToInt32 (dtTopRanked.Rows[i]["number_of_diggs"]).Equals (Convert.ToInt32 (dtTopRanked.Rows[i + 1]["number_of_diggs"])))
									{
										// No change in rank
										iRank++;
									}
								}
							}
						}
	
						PagedDataSource pdsBillboard = new PagedDataSource();
						pdsBillboard.DataSource = dtTopRanked.DefaultView;
						pdsBillboard.AllowPaging = true;

						// Set the number of items you wish to display per page
						pdsBillboard.PageSize = resultsPerPage; 
						pdsBillboard.CurrentPageIndex = pageNumber - 1;

						//sets display position of the submition information (toggles HTML)
						int info_placement = Convert.ToInt32(drBillBoard ["info_placement"]);
						switch(info_placement)
						{
							case (int) Constants.eASSET_INFO_POSITION.BOTTOM:
								//sets dataSource
								dlBottomDisplay.DataSource = pdsBillboard;
								dlBottomDisplay.DataBind ();
								break;
							case (int) Constants.eASSET_INFO_POSITION.RIGHT:
							default:
								//sets dataSource
								dlRightDisplay.DataSource = pdsBillboard;
								dlRightDisplay.DataBind();
								break;
						}

						this.pgTop.Visible = true;
						this.pgBottom.Visible = true;

						pgTop.NumberOfPages = Math.Ceiling ((double) pdsBillboard.DataSourceCount / resultsPerPage).ToString ();
						pgTop.DrawControl ();

						pgBottom.NumberOfPages = Math.Ceiling ((double) pdsBillboard.DataSourceCount / resultsPerPage).ToString ();
						pgBottom.DrawControl ();

						// The results
						lblSearch.Text = GetResultsText (pdsBillboard.DataSourceCount, pageNumber, resultsPerPage, pdsBillboard.Count, "");
					}
					else
					{
						lbl_Noresults.Visible = true;
						this.pgTop.Visible = false;
						this.pgBottom.Visible = false;
						lblSearch.Text = "";
					}

				}
				catch(Exception ex)
				{
					string error = ex.Message;
					lbl_Error.Text = "Unable to populate control.";
				}

			}
			else
			{
				divTitle.InnerHtml	= "title goes here";
				lblIntroText.Text = "header goes here";
				lblFooterText.Text = "footer goes here";
			}
		}


		/// <summary>
		/// Page Change
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void pager_PageChange (object sender, PageChangeEventArgs e)
		{
			pgTop.CurrentPageNumber = e.PageNumber;
			pgTop.DrawControl ();
			pgBottom.CurrentPageNumber = e.PageNumber;
			pgBottom.DrawControl ();

			BindData (e.PageNumber);
			InitThickBox ();
		}

		/// <summary>
		/// Sets configurable Image Size 
		/// </summary>
		protected string imagePath
		{
			get
			{
				if(ViewState["_imagePath"] == null)
				{
					ViewState["_imagePath"] = "thumbnail_medium_path";
				}
				return ViewState["_imagePath"].ToString();
			}
			set
			{
				ViewState["_imagePath"] = value;
			}
		}

		/// <summary>
		/// Sets configurable Image Size Style sheet tag
		/// </summary>
		protected string imageSizeStyleSheet 
		{
			get
			{
				if(ViewState["_imageStyleSheet"] == null)
				{
					ViewState["_imageStyleSheet"] = "framesize-medium";
				}
				return ViewState["_imageStyleSheet"].ToString();
			}
			set
			{
				ViewState["_imageStyleSheet"] = value;
			}
		}

		/// <summary>
		/// Sets configurable Image Size Abbrv
		/// </summary>
		protected string imageSizeAbbrv 
		{
			get
			{
				if(ViewState["_imageSizeAbbrv"] == null)
				{
					ViewState["_imageSizeAbbrv"] = "me";
				}
				return ViewState["_imageSizeAbbrv"].ToString();
			}
			set
			{
				ViewState["_imageSizeAbbrv"] = value;
			}
		}

		/// <summary>
		/// Get the results text
		/// </summary>
		/// <returns></returns>
		public string GetResultsText (int TotalCount, int pageNumber, int resultsPerPage, int currentPageCount, string searchString)
		{
			if ( TotalCount > 0 )
			{
				if (searchString.Length > 0)
				{
					return KanevaGlobals.GetResultsText (TotalCount, pageNumber, resultsPerPage, currentPageCount, true) + " matching '" + searchString + "'";
				}
				else
				{
					return KanevaGlobals.GetResultsText (TotalCount, pageNumber, resultsPerPage, currentPageCount, true);
				}
			}
			else
			{
				if (searchString.Length > 0)
				{
					return "No results matching '" + searchString + "'";
				}
				else
				{
					return "No results found";
				}
			}
		}

		/// <summary>
		/// FormatDateTimeSpan
		/// </summary>
		/// <returns></returns>
		protected string FormatDateTimeSpan (Object dtDate)
		{
			return KanevaGlobals.FormatDateTimeSpan(dtDate);
		}

		/// <summary>
		/// Get Asset Details Link
		/// </summary>
		/// <param name="assetId"></param>
		/// <returns></returns>
		public string GetAssetContestLink (int assetId, int contestId)
		{
			return "contestPlayer.aspx?TB_iframe=true&height=530&width=750&assetId=" + assetId + "&contestId=" + contestId;
		}

		/// <summary>
		/// Get ThickBox code
		/// </summary>
		/// <param name="assetId"></param>
		/// <returns></returns>
		public string GetThickBoxJScript (string title, int assetId, int contestId)
		{
			return "TB_show('" + KanevaGlobals.CleanJavascript (title) + "','" + GetAssetContestLink (assetId, contestId) + "',false);" +
				"this.blur();" +
				"return false;";
		}

		private void InitThickBox ()
		{
			if (MagicAjaxContext.Current.IsAjaxCallForPage (Page))
			{
				//AjaxCallHelper.WriteLine ("alert('call init');TB_init();");
			}
		}

		private void LaunchVideo (int assetId, int contestId)
		{
			DataRow drAsset = StoreUtility.GetAsset (assetId);

			string scriptString = "<script language=\"javascript\" type=\"text/javascript\">";
			// Try 1 = scriptString += "$(document).ready(TB_show('" + KanevaGlobals.CleanJavascript (drAsset ["name"]).ToString () + "','" + GetAssetContestLink (assetId, contestId) + "',false));";
			//// Try 2 = not wok on staging firefox	scriptString += "$(document).ready(function(){";
			////									scriptString += GetThickBoxJScript (drAsset ["name"].ToString (), assetId, contestId);  //"TB_show('" + KanevaGlobals.CleanJavascript (drAsset ["name"].ToString ()) + "','" + GetAssetContestLink (assetId, contestId) + "',false)";
			scriptString += "window.onload = function(){TB_show('" + KanevaGlobals.CleanJavascript (drAsset ["name"].ToString ()) + "','" + GetAssetContestLink (assetId, contestId) + "',false);}";
			scriptString += "</script>";



            if (!Page.ClientScript.IsClientScriptBlockRegistered(GetType(), "LaunchVideo"))
			{
                Page.ClientScript.RegisterStartupScript(GetType(), "LaunchVideo", scriptString);
			}
		}


		/// <summary>
		/// btnDelete_Click
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void lbDelete_Click (object sender, EventArgs e)
		{
			WidgetUtility.DeleteModuleContestSubmissions (InstanceId);
			FireModuleDeletedEvent();
		}

		protected	HtmlTable	tblEdit;
		protected	LinkButton	lbEdit, lbDelete;
		protected	System.Web.UI.HtmlControls.HtmlTableCell infoRight;
		protected	System.Web.UI.HtmlControls.HtmlTableCell tdRankBottom;
		protected	System.Web.UI.HtmlControls.HtmlTableCell tdRankRight;
		protected	System.Web.UI.HtmlControls.HtmlTableCell tdRightAsset;
		protected	System.Web.UI.HtmlControls.HtmlTableCell tdRightArtist;
		protected	System.Web.UI.HtmlControls.HtmlTableCell tdBottomAsset;
		protected	System.Web.UI.HtmlControls.HtmlTableCell tdBottomArtist;
		protected	System.Web.UI.HtmlControls.HtmlTableRow infoBottom;

		protected DataList dlRightDisplay;
		protected DataList dlBottomDisplay;

		protected HtmlInputText txtSearch;
		protected ImageButton imgSearch;
		protected const int cMIN_SEARCH_WORD_LENGTH = 3;
		protected Kaneva.Pager	pgTop, pgBottom;

		protected Label lblIntroText, lblCloudTitle, lblSearch, lblFooterText, lbl_Error, lbl_Noresults;
		protected	HtmlGenericControl	divTitle;

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
			lbDelete.Attributes["onclick"] = "javascript:return " +
				"confirm('Are you sure you want to delete this widget?');"; 

			this.lbDelete.Click += new EventHandler(lbDelete_Click);
			this.pgTop.PageChanged +=new PageChangeEventHandler (pager_PageChange);
			this.pgBottom.PageChanged +=new PageChangeEventHandler (pager_PageChange);
		}
		#endregion
	}
}
