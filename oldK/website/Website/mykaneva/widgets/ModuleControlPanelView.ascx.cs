///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva.mykaneva.widgets
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	using MagicAjax;
	using KlausEnt.KEP.Kaneva.framework.widgets;

	/// <summary>
	///		Summary description for ModuleControlPanelView.
	/// </summary>
	public class ModuleControlPanelView : ModuleViewBaseControl
	{
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			if (!IsPostBack)
			{
				BindData();
              
			}

			if (GetUserId ().Equals (ProfileOwnerId))
			{
				
				btnAddFriend.Attributes["onclick"] = "javascript:" +
					"alert('You cannot add yourself as a friend.');return (false);";
			}
			else
			{
				
				btnAddFriend.Attributes["onclick"] = "javascript:return " +
					"confirm('Send a friend request to this user?')"; 
			}

			//open the editor window when user clicks edit button
            lbEdit.Attributes["onclick"] = "window.open('../mykaneva/ModuleEditor.aspx?modid=" + this.InstanceId
                +"&pageId=" + PageId + "' , 'ModuleEditor', 'width=750,height=680,scrollbars=1,resizable=1,status=1,toolbar=0'); return false;";
            lbEdit.Attributes["onclick"] = "window.open('../mykaneva/ModuleEditor.aspx?modid=" + this.InstanceId
                + "&pageId=" + PageId + "' , 'ModuleEditor', 'width=750,height=680,scrollbars=1,resizable=1,status=1,toolbar=0'); return false;";

			SetBlockedText ();

			lbDelete.Visible = false;

			if ( Zone.Equals(Constants.eMODULE_ZONE.HEADER) || Zone.Equals(Constants.eMODULE_ZONE.FOOTER) )
			{
				controlSection1.Style.Add ("width", "33%");
				controlSection2.Style.Add ("width", "33%");
				controlSection3.Style.Add ("width", "33%");
			}
			else if ( TotalWidth > 570 )
			{
				controlSection1.Style.Add ("width", "98%");
				controlSection2.Style.Add ("width", "45%");
				controlSection3.Style.Add ("width", "45%");
			}
			else
			{
				controlSection1.Style.Add ("width", "98%");
				controlSection2.Style.Add ("width", "98%");
				controlSection3.Style.Add ("width", "98%");
			}

			divUrl.Style.Add ("width", (TotalWidth-30).ToString ());

            // Rave
            if (!IsPostBack)
            {
                // Which rave graphic do we show?	 
                RaveFacade raveFacade = new RaveFacade ();
                if (!raveFacade.IsCommunityRaveFree(KanevaWebGlobals.CurrentUser.UserId, ChannelId, RaveType.eRAVE_TYPE.SINGLE))
                {
                    btnVote.CssClass = "RavePlus";
                }
                else
                {
                    btnVote.CssClass = "raveBtn";
                }
                litItemId.Text = ChannelId.ToString ();
            }
		}

		#region Helper Methods

        private void BindData ()
        {
            tblEdit.Visible = ShowEdit;

            // If user has at least 1 photo, then display Show More Photos link
            DataRow drMedia = StoreUtility.GetChannelMediaCounts(ChannelId);

            aPictures.Visible = false;

            try
            {
                if (Convert.ToUInt32 (drMedia["sum_picture"]) > 0)
                {
                    aPictures.Visible = true;
                    aPictures.HRef = ResolveUrl ("~/mykaneva/pictures.aspx?userId=" + ProfileOwnerId);
                }
            }
            catch { }

            //gather USER data 
            UserFacade userFacade = new UserFacade ();
            User userProfileOwner = userFacade.GetUser (ProfileOwnerId);

            //if profile owner is the same as the viewing user, hide the meet me in three-D fly out
            //else continue checks.
            if (GetUserId () != ProfileOwnerId)
            {
                //check to see if browsing user is logged in/a member of kaneva
                if (Request.IsAuthenticated)
                {
                    //browsing user is a member of WOK
                    if (KanevaWebGlobals.CurrentUser.HasWOKAccount)
                    {
                        //check to see if profile owner is a wok member
                        //profile owner and browsing user are WOK members
                        if (userProfileOwner.WokPlayerId > 0)
                        {
                            //if profile owner is logged in to WOK set string to go to their location - for now always set to go to their location
                            if (KanevaWebGlobals.IsInWorld (userProfileOwner.Ustate))
                            {
                                //litHotLinkLocation.Text = "<A class=\"trigger\" href=\"" + ResolveUrl ("~/kgp/playwok.aspx") + "?goto=P" + ProfileOwnerId.ToString () + "&ILC=MM3D&link=mmnowin\">";
                                //litDisplayText.Text = "Meet Me 3D";
                                btnMeetMe3D.Attributes.Add("href",ResolveUrl ("~/kgp/playwok.aspx") + "?goto=P" + ProfileOwnerId.ToString () + "&ILC=MM3D&link=mmnowin");
                            }
                            else
                            {
                                //litHotLinkLocation.Text = "<A class=\"trigger\" href=\"" + ResolveUrl ("~/myKaneva/newMessage.aspx") + "?userId=" + this.ProfileOwnerId.ToString () + "&meetMe3D=A\">";
                                //litHotLinkLocation.Text = "<A class=\"trigger\" href=\"" + ResolveUrl("~/kgp/playwok.aspx") + "?goto=A" + ProfileOwnerId.ToString() + "&ILC=MM3D&link=mmnow\">";
                                //litDisplayText.Text = "Meet Me 3D";
                                btnMeetMe3D.Attributes.Add("href", ResolveUrl("~/myKaneva/newMessage.aspx") + "?userId=" + this.ProfileOwnerId.ToString() + "&meetMe3D=A");
                            }
                        }
                        //profile owner is not a wok member (browsing user is)
                        else
                        {
                            //give user option of going in world anyway
                            //litHotLinkLocation.Text = "<A class=\"trigger\" href=\"" + ResolveUrl ("~/kgp/playwok.aspx") + "?goto=C0&ILC=MM3D&link=private\">";
                            //litDisplayText.Text = "Meet Me 3D";
                            btnMeetMe3D.Attributes.Add("href", ResolveUrl("~/kgp/playwok.aspx") + "?goto=C0&ILC=MM3D&link=private");
                        }
                    }
                    //browsing user is not a memeber of WOK (profile owner status unknown) send user to download page
                    else
                    {
                        //litHotLinkLocation.Text = "<A class=\"trigger\" href=\"" + ResolveUrl ("~/kgp/playwok.aspx") + "?ILC=MM3D&link=getinto\">";
                        //litDisplayText.Text = "Meet Me 3D";
                        btnMeetMe3D.Attributes.Add("href", ResolveUrl("~/kgp/playwok.aspx") + "?ILC=MM3D&link=getinto");
                    }
                }
                // browsingUser is not logged in show only login/join kaneva (no one's WOK status know)
                else
                {
                    //litHotLinkLocation.Text = "<A class=\"trigger\" href=\"" + ResolveUrl("~/loginSecure.aspx?logretURL=") + ResolveUrl("~/kgp/playwok.aspx?ILC=MM3D&link=joinsignin") + "\">";
                    //litHotLinkLocation.Text = "<A class=\"trigger\" href=\"" + ResolveUrl ("~/loginSecure.aspx") + "?ILC=MM3D&link=joinsignin\">";
                    //litDisplayText.Text = "Meet Me 3D";
                    btnMeetMe3D.Attributes.Add("href", ResolveUrl("~/loginSecure.aspx") + "?ILC=MM3D&link=joinsignin");
                }
            }
            else
            {
                //show link buttons to WOK (profile owner is member of WOK)
                if (userProfileOwner.WokPlayerId > 0)
                {
                    //litHotLinkLocation.Text = "<A class=\"trigger\" href=\"" + ResolveUrl ("~/kgp/playwok.aspx") + "?goto=A" + ProfileOwnerId.ToString () + "&ILC=MM3D&link=gotomyhome\">";
                    //litDisplayText.Text = "Go To My 3D Home";
                    btnMeetMe3D.Attributes.Add("href", ResolveUrl("~/kgp/playwok.aspx") + "?goto=A" + ProfileOwnerId.ToString() + "&ILC=MM3D&link=gotomyhome");
                }
                //show link to download WOK (profile owner is NOT member of WOK)
                else
                {
                    //litHotLinkLocation.Text = "<A class=\"trigger\" href=\"" + ResolveUrl ("~/kgp/playwok.aspx") + "?ILC=MM3D&link=getinto\">";
                    //litDisplayText.Text = "Go To My 3D Home";
                    btnMeetMe3D.Attributes.Add("href", ResolveUrl("~/kgp/playwok.aspx") + "?ILC=MM3D&link=getinto");
                }
            }

            imgAvatar.Src = UsersUtility.GetProfileImageURL (userProfileOwner.ThumbnailMediumPath, "me", userProfileOwner.Gender);
            imgAvatar.Alt = userProfileOwner.Username;

            // set up the image to display using highslide
            aAvatar.HRef = ResolveUrl ("~/mykaneva/PictureProfileDetail.aspx?userId=" + ProfileOwnerId);
            aAvatar.Attributes.Add ("onclick", "return hs.expand(this, { easing: 'easeOutBack', easingClose: 'easeInBack', src: '" +
                UsersUtility.GetProfileImageURL (userProfileOwner.ThumbnailXlargePath, "xl", userProfileOwner.Gender) + "' })");


            lblUserName.Text = userProfileOwner.Username;
            lblDisplayName.Text = userProfileOwner.DisplayName;

            //set users online/inworld status
            litOnline.Text = GetOnlineText (userProfileOwner.Ustate);

            //indicate if the user has a VIP membership or not
            divVIP.Visible = userProfileOwner.HasVIPPass;

            int age = -1;
            string gender = "";
            string location = "";
            // get birth date
            age = userProfileOwner.Age;

            // get gender
            gender = userProfileOwner.Gender == "M" ? "Male" : "Female";

            // get 'location'
            location = userProfileOwner.Location;

            DataRow drControlPanel = WidgetUtility.GetModuleControlPanel (InstanceId);

            if (drControlPanel != null)
            {
                if (drControlPanel["title"] != DBNull.Value && drControlPanel["title"].ToString () != "")
                {
                    lblTitle.Text = drControlPanel["title"].ToString ();
                    lblTitle.Visible = true;
                }
                else
                {
                    lblTitle.Visible = false;
                }

                // if age should be shown
                if (drControlPanel["show_age"].ToString () == "1")
                {
                    lblAge.Visible = true;
                    lblAge.Text = "" + age + " Years Old";
                }
                else
                {
                    lblAge.Visible = false;
                }

                // if gender should be shown
                if (drControlPanel["show_gender"].ToString () == "1")
                {
                    lblGender.Visible = true;
                    lblGender.Text = "" + gender;
                }
                else
                {
                    lblGender.Visible = false;
                }

                // if location should be shown
                if (drControlPanel["show_location"].ToString () == "1")
                {
                    lblLocation.Visible = true;
                    lblLocation.Text = location;
                }
                else
                {
                    lblLocation.Visible = false;
                }
            }

            // The permalink
            string permLink = "";

            if (userProfileOwner.URL.Length.Equals (0))
            {
                permLink = KanevaGlobals.GetPersonalChannelUrl (userProfileOwner.NameNoSpaces);
            }
            else
            {
                permLink = "http://" + KanevaGlobals.SiteName + "/" + userProfileOwner.URL;
            }

            hlPerm.Text = permLink;
            hlPerm.Attributes.Add ("alt", permLink);
            hlPerm.NavigateUrl = permLink;

            DataRow drSystemStats = CommunityUtility.GetCommunityStats (ChannelId);
            if (drSystemStats != null)
            {
                lblLastLogin.Text = Convert.ToDateTime (drSystemStats["last_login"].ToString ()).ToShortDateString ();
                lblLastUpdate.Text = Convert.ToDateTime (drSystemStats["last_update"].ToString ()).ToShortDateString ();
            }
        }

        /// <summary>
		/// Set the blocked text
		/// </summary>
		private void SetBlockedText ()
		{
            UserFacade userFacade = new UserFacade();

			if (userFacade.IsUserBlocked (GetUserId (), ProfileOwnerId))
			{
				
                btnBlockUser.CssClass = "UnblockUser";
			}	
			else
			{
				
                btnBlockUser.CssClass = "BlockUser";
			}
		}

		private void AddFriend ()
		{
			if (Request.IsAuthenticated)
			{
				// Add them as a friend
				int ret = GetUserFacade.InsertFriendRequest (GetUserId (), ProfileOwnerId);
				
				if (ret.Equals (1))	// already a friend
				{
					AjaxCallHelper.WriteAlert ("This member is already your friend.");
				}
				else if (ret.Equals (2))  // pending friend request
				{
					AjaxCallHelper.WriteAlert ("You have already sent this member a friend request.");
				}
				else
				{
                    // send request email
                    MailUtilityWeb.SendFriendRequestEmail(GetUserId(), ProfileOwnerId);
				}
				
			}
		}
		
        #endregion
		

        #region Event Handlers

		/// <summary>
		/// Handle a profile vote
		/// </summary>
		protected void btnVote_Click (object sender, CommandEventArgs e)
		{
			if (!Request.IsAuthenticated)
			{
				Response.Redirect (GetLoginURL ());
			}

            RaveFacade raveFacade = new RaveFacade ();
            int assetId = Convert.ToInt32 (Request["assetId"]);

            RaveType.eRAVE_TYPE eRaveType = e.CommandArgument.ToString ().Equals ("megarave") ? RaveType.eRAVE_TYPE.MEGA : RaveType.eRAVE_TYPE.SINGLE;


            if (!raveFacade.IsCommunityRaveFree (KanevaWebGlobals.CurrentUser.UserId, ChannelId,
                eRaveType) && !this.IsUserAdministrator ())
            {                
                // Call popFacebox
                if (eRaveType == RaveType.eRAVE_TYPE.MEGA)
                {
                    AjaxCallHelper.Write ("popFaceboxMega();");
                }
                else
                {
                    AjaxCallHelper.Write ("popFacebox();");
                }
            }
            else  // first time rave
            {
                // First rave is free.  Insert a normal rave.
                raveFacade.RaveCommunity (KanevaWebGlobals.CurrentUser.UserId, ChannelId, WebCache.GetRaveType (eRaveType), "");

                btnVote.CssClass = "RavePlus";
            }
		}

		private void lbDelete_Click(object sender, EventArgs e)
		{
			//			WidgetUtility.DeleteModuleControlPanel(InstanceId);
			//			FireModuleDeletedEvent();
		}

		/// <summary>
		/// Handle a report abuse
		/// </summary>
		protected void btnReportAbuse_Click (object sender, System.EventArgs e)
		{																													   
			Response.Redirect (ResolveUrl ("~/suggestions.aspx?mode=WB&category=KANEVA%20Web%20Site&rurl=" + Server.UrlEncode(Request.Url.ToString())));
		}

		private void btnAddFriend_Click(object sender, EventArgs e)
		{
			// Must be logged in
			if (!Request.IsAuthenticated)
			{
				AjaxCallHelper.WriteAlert ("Please sign in to add this user as a friend");
				return;
			}

			// Can't friend yourself
			if (GetUserId ().Equals (ProfileOwnerId))
			{
				AjaxCallHelper.WriteAlert ("You cannot add yourself as a friend.");
				return;
			}

			AddFriend ();
			//Response.Redirect (Request.CurrentExecutionFilePath + "?communityId=" + ChannelId;
		}

		/// <summary>
		/// Handle a profile vote
		/// </summary>
		protected void btnTellOthers_Click (object sender, System.EventArgs e)
		{
			Response.Redirect(ResolveUrl ("~/myKaneva/newMessage.aspx?aboutId=" + this.ProfileOwnerId));
		}

		/// <summary>
		/// Handle a block user
		/// </summary>
		protected void btnBlockUser_Click (object sender, System.EventArgs e)
		{
			// Must be logged in
			if (!Request.IsAuthenticated)
			{
				AjaxCallHelper.WriteAlert ("Please sign in to block/unblock this user");
				return;
			}
			
			// Can't block yourself
			if (GetUserId ().Equals (ProfileOwnerId))
			{
				AjaxCallHelper.WriteAlert ("You cannot block yourself.");
				return;
			}

            UserFacade userFacade = new UserFacade();

            if (userFacade.IsUserBlocked(GetUserId(), ProfileOwnerId))
			{
				UsersUtility.UnBlockUser (GetUserId (), ProfileOwnerId);
			}
			else
			{
				UsersUtility.BlockUser (GetUserId (), ProfileOwnerId);
			}

			SetBlockedText ();
		}

		private void btnSendMessage_Click(object sender, EventArgs e)
		{
			Response.Redirect(ResolveUrl ("~/myKaneva/newMessage.aspx?userId=" + this.ProfileOwnerId));
		}

		#endregion


        #region Declerations
        
        protected LinkButton lbEdit, lbDelete;
        protected ImageButton lblOnline;
        protected HtmlImage voteImg;
        protected LinkButton lbMegaRave;

        protected HtmlTable tblEdit;
        protected Label lblTitle;

        protected Label lblUserName, lblDisplayName;
        protected HtmlImage imgAvatar, imgAdd, imgMsg;
        protected LinkButton btnAddFriend;
        protected LinkButton btnSendMessage;
        protected LinkButton btnBlockUser;
        protected LinkButton btnReportAbuse;
        protected LinkButton btnTellOthers;
        protected LinkButton btnMeetMe3D;
        protected Label lblAge;
        protected Label lblGender;
        protected Label lblLocation;
        protected Label lblLastLogin;
        protected Label lblLastUpdate;
        protected HyperLink hlPerm;
        protected LinkButton btnVote;
        protected Literal litOnline, litDisplayText, litHotLinkLocation;
        protected Literal litWokUrl;
        protected Literal litItemId;

        protected HtmlContainerControl controlSection1, controlSection2, controlSection3;
        protected HtmlContainerControl divUrl, divVIP;
        protected HtmlAnchor aPictures;

        protected HtmlAnchor aAvatar;

        protected Literal litIndexTools;

        #endregion


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
			btnAddFriend.Click += new EventHandler(btnAddFriend_Click);
			btnSendMessage.Click += new EventHandler(btnSendMessage_Click);
			lbDelete.Click += new EventHandler(lbDelete_Click);
			btnVote.Command += new CommandEventHandler(btnVote_Click);
		}
		#endregion
	}
}
