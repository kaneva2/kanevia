///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;


namespace KlausEnt.KEP.Kaneva.mykaneva.widgets
{
    public partial class WidgetLoader : System.Web.UI.Page
    {
        #region Declarations

        private int _ownerId = 0;
        //private int _modulePageId = 0;
        private int _pageId = 0;
        private int _moduleId = 0;
        private int _zoneId = 0;
        private int _instanceId = 0;
        private int _sequence = 0;
        private int _channelId = 0;
        private bool _canEdit = false;

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            //configure respose object
            Response.ContentType = "text/plain";
            Response.CacheControl = "no-cache";
            Response.AddHeader("Pragma", "no-cache");
            Response.Expires = -1;

            //get the required parameters
            GetRequestParams();

            //load the usercontrol 
            LoadControl();

        }

        #region Functions

        //get all parameters
        private void GetRequestParams()
        {
            try
            {
                if ((Request["oi"] != null) && (Request["oi"] != ""))
                {
                    _ownerId = Convert.ToInt32(Request["oi"].ToString());
                }
                //if ((Request["mpi"] != null) && (Request["mpi"] != ""))
                //{
                //    _modulePageId = Convert.ToInt32(Request["mpi"].ToString());
                //}
                if ((Request["pi"] != null) && (Request["pi"] != ""))
                {
                    _pageId = Convert.ToInt32(Request["pi"].ToString());
                }
                if ((Request["mi"] != null) && (Request["mi"] != ""))
                {
                    _moduleId = Convert.ToInt32(Request["mi"].ToString());
                }
                if ((Request["zi"] != null) && (Request["zi"] != ""))
                {
                    _zoneId = Convert.ToInt32(Request["zi"].ToString());
                }
                if ((Request["ii"] != null) && (Request["ii"] != ""))
                {
                    _instanceId = Convert.ToInt32(Request["ii"].ToString());
                }
                if ((Request["si"] != null) && (Request["si"] != ""))
                {
                    _sequence = Convert.ToInt32(Request["si"].ToString());
                }
                if ((Request["ci"] != null) && (Request["ci"] != ""))
                {
                    _channelId = Convert.ToInt32(Request["ci"].ToString());
                }
                if ((Request["ed"] != null) && (Request["ed"] != ""))
                {
                    _canEdit = Convert.ToBoolean(Request["ed"].ToString());
                }
            }
            catch (Exception)
            {
            }
        }

        private void LoadControl()
        {
            //load usercontrol
            try
            {
                //load the control
                //widgetShell.Controls.Clear();
                ModuleViewBaseControl activeControl = (ModuleViewBaseControl)Page.LoadControl(GetControlPath(_moduleId));
                
                activeControl.ProfileOwnerId = _ownerId;
                activeControl.PageId = _pageId;
                activeControl.ModuleId = _moduleId;
                activeControl.Zone = (Constants.eMODULE_ZONE) _zoneId;
                activeControl.InstanceId = _instanceId;
                activeControl.ChannelId = _channelId;
                activeControl.ShowEdit = _canEdit;

                widgetShell.Controls.Add(activeControl);
                widgetShell.Visible = true;
            }
            catch (Exception ex)
            {
                Messages.Visible = true;
                Messages.BackColor = System.Drawing.Color.White;
                Messages.ForeColor = System.Drawing.Color.DarkRed;
                Messages.Text = "Error While Loading Control: " + ex.Message;
            }
        }

        private string GetControlPath(int moduleId)
        {
            string controlPath = "";

            switch (moduleId)
            {
                case (int)Constants.eMODULE_TYPE.TITLE_TEXT:
                    controlPath = "ModuleTitleView.ascx";
                    break;

                case (int)Constants.eMODULE_TYPE.BLOGS:
                    controlPath = "ModuleBlogView.ascx";
                    break;

                case (int)Constants.eMODULE_TYPE.PROFILE:
                    controlPath = "ModuleProfileView.ascx";
                    break;

                case (int)Constants.eMODULE_TYPE.FAME_PANEL:
                    controlPath = "ModuleFameView.ascx";
                    break;

                case (int)Constants.eMODULE_TYPE.FRIENDS:
                    controlPath = "ModuleFriendsView.ascx";
                    break;

                case (int)Constants.eMODULE_TYPE.CHANNELS:
                    controlPath = "ModuleChannelView.ascx";
                    break;

                case (int)Constants.eMODULE_TYPE.MENU:
                    controlPath = "ModuleMenuView.ascx";
                    break;

                case (int)Constants.eMODULE_TYPE.COMMENTS:
                    controlPath = "ModuleCommentsView.ascx";
                    break;
                case (int)Constants.eMODULE_TYPE.USER_UPLOAD:
                    controlPath = "ModuleUserUploadView.ascx";
                    break;

                case (int)Constants.eMODULE_TYPE.HTML_CONTENT:
                    controlPath = "ModuleHtmlView.ascx";
                    break;

                case (int)Constants.eMODULE_TYPE.HTML_CONTENT_BASIC:
                    controlPath = "ModuleHtml2View.ascx";
                    break;

                case (int)Constants.eMODULE_TYPE.SYSTEM_STATS:
                    controlPath = "ModuleStatsView.ascx";
                    break;

                case (int)Constants.eMODULE_TYPE.MY_COUNTER:
                    controlPath = "ModuleCounterView.ascx";
                    break;

                case (int)Constants.eMODULE_TYPE.MY_PICTURE:
                    controlPath = "ModuleMyPictureView.ascx";
                    break;

                case (int)Constants.eMODULE_TYPE.NEW_PEOPLE:
                    controlPath = "ModuleNewPeopleView.ascx";
                    break;

                case (int)Constants.eMODULE_TYPE.MULTIPLE_PICTURES:
                    controlPath = "ModuleMultiplePicturesView.ascx";
                    break;

                case (int)Constants.eMODULE_TYPE.SLIDE_SHOW:
                    controlPath = "ModuleSlideShowView.ascx";
                    break;

                case (int)Constants.eMODULE_TYPE.GAMES_PLAYER:
                    controlPath = "ModuleStoreView.ascx";
                    break;

                case (int)Constants.eMODULE_TYPE.MUSIC_PLAYER:
                    controlPath = "ModuleStoreView.ascx";
                    break;

                case (int)Constants.eMODULE_TYPE.VIDEO_PLAYER:
                    controlPath = "ModuleStoreView.ascx";
                    break;

                case (int)Constants.eMODULE_TYPE.SINGLE_PICTURE:
                    controlPath = "ModuleSinglePictureView.ascx";
                    break;

                case (int)Constants.eMODULE_TYPE.CONTROL_PANEL:
                    controlPath = "ModuleControlPanelView.ascx";
                    break;

                case (int)Constants.eMODULE_TYPE.MIXED_MEDIA:
                    controlPath = "ModuleMixedMediaView.ascx";
                    break;

                case (int)Constants.eMODULE_TYPE.CHANNEL_OWNER:
                    controlPath = "ModuleMyPictureView.ascx";
                    break;

                case (int)Constants.eMODULE_TYPE.CHANNEL_MEMBERS:
                    controlPath = "ModuleChannelMembersView.ascx";
                    break;

                case (int)Constants.eMODULE_TYPE.CHANNEL_EVENTS:
                    controlPath = "ModuleEventsView.ascx";
                    break;

                case (int)Constants.eMODULE_TYPE.CHANNEL_FORUM:
                    controlPath = "ModuleChannelForumView.ascx";
                    break;

                case (int)Constants.eMODULE_TYPE.CHANNEL_CONTROL_PANEL:
                    controlPath = "ModuleChannelControlPanelView.ascx";
                    break;

                case (int)Constants.eMODULE_TYPE.CHANNEL_DESCRIPTION:
                    controlPath = "ModuleChannelDescriptionView.ascx";
                    break;

                case (int)Constants.eMODULE_TYPE.OMM_VIDEO_PLAYER:
                    controlPath = "ModuleMediaView.ascx";
                    break;

                case (int)Constants.eMODULE_TYPE.OMM_VIDEO_PLAYLIST:
                    controlPath = "ModuleMediaPlaylistView.ascx";
                    break;

                case (int)Constants.eMODULE_TYPE.OMM_MUSIC_PLAYER:
                    controlPath = "ModuleMediaView.ascx";
                    break;

                case (int)Constants.eMODULE_TYPE.OMM_MUSIC_PLAYLIST:
                    controlPath = "ModuleMediaPlaylistView.ascx";
                    break;

                case (int)Constants.eMODULE_TYPE.TOP_CONTRIBUTORS:
                    controlPath = "ModuleChannelTopContributorsView.ascx";
                    break;

                case (int)Constants.eMODULE_TYPE.CONTEST_UPLOAD:
                    controlPath = "ModuleContestUploadView.ascx";
                    break;

                case (int)Constants.eMODULE_TYPE.CONTEST_SUBMISSIONS:
                    controlPath = "ModuleContestSubmissionsView.ascx";
                    break;

                case (int)Constants.eMODULE_TYPE.CONTEST_GET_RAVED:
                    controlPath = "ModuleContestGetRavedView.ascx";
                    break;

                case (int)Constants.eMODULE_TYPE.USER_GIFTS:
                    controlPath = "ModuleGiftListView.ascx";
                    break;

                case (int)Constants.eMODULE_TYPE.CONTEST_PROFILE_HEADER:
                    controlPath = "ModuleContestProfileHeaderView.ascx";
                    break;

                case (int)Constants.eMODULE_TYPE.COMMUNITY_TOP_BANNER:
                    controlPath = "ModuleCommunityTopBannerView.ascx";
                    break;

                case (int)Constants.eMODULE_TYPE.LEADER_BOARD:
                    controlPath = "ModuleLeaderBoardView.ascx";
                    break;

                case (int)Constants.eMODULE_TYPE.KANEVA_LEADER_BOARD:
                    controlPath = "ModuleKanevaLeaderBoardView.ascx";
                    break;

                case (int)Constants.eMODULE_TYPE.HOT_NEW_STUFF:
                    controlPath = "ModuleHotNewStuffView.ascx";
                    break;

                case (int)Constants.eMODULE_TYPE.BILLBOARD:
                    controlPath = "ModuleBillBoardView.ascx";
                    break;

                case (int)Constants.eMODULE_TYPE.NEWEST_MEDIA:
                    controlPath = "ModuleNewestMediaView.ascx";
                    break;

                case (int)Constants.eMODULE_TYPE.CONTEST_TOP_RESULTS:
                    controlPath = "ModuleContestTopVoteGettersView.ascx";
                    break;

                case (int)Constants.eMODULE_TYPE.PROFILE_PORTAL:
                    controlPath = "ModuleProfilePortalView.ascx";
                    break;

                case (int)Constants.eMODULE_TYPE.CHANNEL_PORTAL:
                    controlPath = "ModuleChannelPortalView.ascx";
                    break;

                case (int)Constants.eMODULE_TYPE.MOST_VIEWED:
                    controlPath = "ModuleMostViewedView.ascx";
                    break;

                case (int)Constants.eMODULE_TYPE.INTERESTS:
                    controlPath = "ModuleInterestsView.ascx";
                    break;

                case (int)Constants.eMODULE_TYPE.PERSONAL:
                    controlPath = "ModulePersonalView.ascx";
                    break;

                default:
                    break;
            }

            return controlPath;
        }

        #endregion

    }
}
