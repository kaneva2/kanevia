﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContestDetails.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.mykaneva.ContestDetails" %>


<script type="text/javascript"><!--
var $j = jQuery.noConflict();

function DisplayErrMsg(){ 		
	$j.colorbox({inline:true, href:"#errmsg", innerWidth:404, overlayClose:false});
}
function DisplayMessage(msg){ 
	$j("#spanErrMsg").text(msg);	
	$j.colorbox({inline:true, href:"#errmsg", innerWidth:404, overlayClose:false});
}
function enter(evt)
{	
	var charCode = (evt.which) ? evt.which : window.event.keyCode; 
    if (charCode == 13) {return false;} 
}

var commentCtrlPath = '<asp:literal id="litCommentsFilePath" runat="server" />';

//########
// Author: ricocheting.com
// Version: v2.0
// Date: 2011-03-31
// Description: displays the amount of time until the "dateFuture" entered below.

// NOTE: the month entered must be one less than current month. ie; 0=January, 11=December
// NOTE: the hour is in 24 hour format. 0=12am, 15=3pm etc
// format: dateFuture1 = new Date(year,month-1,day,hour,min,sec)
// example: dateFuture1 = new Date(2003,03,26,14,15,00) = April 26, 2003 - 2:15:00 pm
//########

dateEnd = <asp:literal id="litCountdownDate" runat="server"></asp:literal>;
var t;

function GetCount(ddate,iid){
	dateNow = new Date();	//grab current date
	amount = ddate.getTime() - dateNow.getTime();	//calc milliseconds between dates
	delete dateNow;

	// if time is already past
	if(amount < 0){
		$(iid).innerHTML="Ended";
		window.document.URL = '<asp:literal id="litRefreshUrl" runat="server"></asp:literal>';
		clearTimeout(t);
	}
	// else date is still good
	else{
		mins=0;secs=0;out="";
		amount = Math.floor(amount/1000);//kill the "milliseconds" so just secs
		mins=Math.floor(amount/60);//minutes
		amount=amount%60;
		secs=Math.floor(amount);//seconds
		out += "00:";
		out += (mins <= 9 ? '0' : '') + mins + ":";
		out += (secs <= 9 ? '0' : '') + secs;
		$(iid).innerHTML=out;

		setTimeout(function(){GetCount(ddate,iid)}, 1000);
	}
}

if (dateEnd > -1) {			  
	window.onload = function () {
		GetCount(dateEnd, 'divTimeRemaining');
	};
}
-->
</script>


<div id="details">
			
	<div class="contest" id="divContestContainer" runat="server">		
		<div class="left">
			<div class="amount"><div class="value">
				<asp:literal id="litPrizeAmount" runat="server"></asp:literal></div>
				<div>CREDITS</div>
			</div>
			<div class="time">
				<div class="label">TIME<br />LEFT</div>
				<div class="remaining" id="divTimeRemaining" clientidmode="static" runat="server"><asp:literal id="litTimeRemaining" runat="server"></asp:literal></div>
			</div><asp:hiddenfield id="hidContestId" runat="server" />
			
			<asp:updatepanel updatemode="Conditional" childrenastriggers="false">
				<contenttemplate>
					<div class="clearit"><a class="buttonnarrow" href="javascript:void(0);" id="btnFollow" runat="server" onserverclick="btnFollow_Click"><span class="follow" id="followBtnText" runat="server">Follow</span></a></div>
				</asp:updatepanel>
			</asp:updatepanel>

			<asp:linkbutton runat="server" id="btnDeleteContest" visible="false"  onclientclick="if(!confirm('Are you sure you want to Delete this Contest?')){return false;};" onclick="btnDeleteContest_Click">Delete</asp:linkbutton>
		</div>
		<div class="middle">
			<div class="datacontainer">
				<div class="title"><a href="javascript:void(0);"><asp:literal id="litTitle" runat="server"></asp:literal></a></div>
				<asp:updatepanel updatemode="Conditional" childrenastriggers="false">
					<contenttemplate>
						<div class="data">
							<div class="entries"><asp:literal id="litNumEntries" runat="server"></asp:literal> Entries</div>
							<div class="votes"><span id="votecount"><asp:literal id="litNumVotes" runat="server"></asp:literal></span> Votes</div>
						</div>	
					</contenttemplate>
				</asp:updatepanel>
			</div>
			<div class="ownercontainer">
				<div class="framesize-xsmall">
					<div id="Div2" class="restricted" runat="server" visible='false'></div>
					<div class="frame">
						<div class="imgconstrain">
							<a href="" id="aThumb" runat="server">
								<img src="" id="imgOwnerThumb" runat="server" />
							</a>
						</div>	
					</div>
				</div>
				<div class="creator">Created by:<br /><span class="name"><a href="" id="aOwnerName" runat="server"><asp:literal id="litOwnerUsername" runat="server"></asp:literal></a></span></div>
			</div>			
			<div class="desc"><asp:literal id="litDescription" runat="server"></asp:literal></div>

		</div>
	</div>

	<div id="usernotify" runat="server" clientidmode="static" visible="false">
		<div class="txtwinner" id="divWinnerIs" runat="server" visible="false">
			<div id="notifyimg"><img src="~/images/contests/contests_trophy_44x63.png" width="44" height="63" runat="server" /></div>
			<div>This Contest is over, and the Winner is...</div>
		</div>
		<div class="txt" id="divPickWinner" runat="server" visible="false">
			This Contest is over, please click on the Winner button to pick your Winner for this contest.<br />
			<span>If you do not vote for a winner, the entry with the most votes will win.</span>
		</div>
		<div class="txt" id="divNotEnoughEntries" runat="server" visible="false">
			This Contest is over, but there were not enough entries.<br />
			<span id="spnNotEnoughEntries" runat="server">You can still pick a Winner or we can refund your credits.</span>
		</div>
		<div class="txt" id="divNoEntries" runat="server" visible="false">
			This Contest is over, but there were no entries.<br />
			<span id="spnNoEntries" runat="server">Since no one entered, your Credits have been refunded.</span>
		</div>
		<div class="txt" id="divNoWinnerYet" runat="server" visible="false">
			This Contest is over, no more Entries will be accepted.<br />
			<span>Check back in a few days to see who is picked as the Winner.</span>
		</div>
	</div>
	<div id="noaccess" runat="server" clientidmode="Static" visible="false">
		<div class="heading">We're Sorry.</div>
		<div>You must have a valid email address<br />
			<span id="spnLevelToCreate" runat="server" visible="false">and a World Fame Level of <asp:literal id="litLevelToCreate" runat="server"></asp:literal> or higher<br /></span>
			to Vote or Enter a Contest</div>
		<div id="buttons">
			<div class="btncontainer">
				<a class="buttonnarrow" id="a1" href="javascript:void(0);" onclick="parent.$j.colorbox.close(); return false;"><span class="search">OK</span></a>
			</div>
		</div>
	</div>

	<asp:repeater id="rptWinner" runat="server" onitemdatabound="rptEntries_ItemDataBound" onitemcommand="rptEntries_ItemCommand">
		<itemtemplate>
			<div id="divWinner" runat="server" class="feed winner">
				<div class="feed-imgTextContainer">
					<div class="feed-imgContainer">
						<div class="feedImg" id="divThumbnail" runat="server">
							<a href="<%# GetPersonalChannelUrl(DataBinder.Eval (Container.DataItem, "Owner.Username").ToString ()) %>"><img src="<%# GetProfileImageURL (DataBinder.Eval(Container.DataItem, "Owner.ThumbnailSquarePath").ToString (), "sq", DataBinder.Eval(Container.DataItem, "Owner.Gender").ToString (), Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "Owner.FacebookSettings.UseFacebookProfilePicture")), Convert.ToUInt64(DataBinder.Eval(Container.DataItem, "Owner.FacebookSettings.FacebookUserId")) ) %>" border="0" /></a>
						</div>
						<div class="vote" id="votebox" runat="server">
							<div id="divVoteCnt" runat="server">
							<asp:imagebutton id="btnVote" imageurl="~/images/contests/vote.gif" width="70" height="26" runat="server" commandargument="" commandname="" />
							
							</div>
						</div>
						<img class="ribbon" src="~/images/icons/icon_blueribbon_16x23.gif" width="16" height="23" runat="server" />
						<div class="winnertype" id="divOwnersChoice" runat="server">Owner's<br />Choice</div>
					</div>
					<div class="feed-textContainer">
						<div class="feedText">
							<h4><a href="<%# GetPersonalChannelUrl(DataBinder.Eval (Container.DataItem, "Owner.Username").ToString ()) %>"><%# DataBinder.Eval (Container.DataItem, "Owner.Username").ToString () %></a>:</h4>
							<p><%# Server.HtmlDecode(DataBinder.Eval (Container.DataItem, "Description").ToString ()) %></p> 
						
							<div class="time">(<%# FormatDateTimeSpan (DataBinder.Eval (Container.DataItem, "CreatedDate"), DataBinder.Eval (Container.DataItem, "CurrentDate"))%>) </div>
					
						</div><!--feedText-->
					</div>
				</div>
			</div>
		</itemtemplate>
	</asp:repeater>

	<asp:updatepanel updatemode="Conditional" childrenastriggers="false">
		<contenttemplate>

			<div id="comments">
				<h2>Contest Comments</h2>
				<div id="contestcomments">
				<asp:PlaceHolder ID="phContestComments" runat="server" visible="true"></asp:PlaceHolder>
				</div>	
			</div>

  			<asp:button id="btnReload" clientidmode="Static" text="" runat="server" onclick="btnReload_Click" style="visibility:hidden;height:1px;" />

	<div id="enter" clientidmode="Static" runat="server" visible="false">
		<a id="btnCreate" class="addentry create" runat="server"><img id="Img1" src="~/images/contests/btn_contest_enter_227x41.gif" widgth="227" height="41" runat="server" /></a>
	</div>


			<div id="entries">
				<h2>Contest Entries</h2>

				<asp:repeater id="rptEntries" runat="server" onitemdatabound="rptEntries_ItemDataBound" onitemcommand="rptEntries_ItemCommand">
					<itemtemplate>
						<div id='<%# "div" + DataBinder.Eval (Container.DataItem, "ContestEntryId").ToString() %>' class="feed">
							<div class="feed-imgTextContainer">
								<div class="feed-imgContainer">
									<div class="feedImg" id="divThumbnail" runat="server">
										<a href="<%# GetPersonalChannelUrl(DataBinder.Eval (Container.DataItem, "Owner.Username").ToString ()) %>"><img src="<%# GetProfileImageURL (DataBinder.Eval(Container.DataItem, "Owner.ThumbnailSquarePath").ToString (), "sq", DataBinder.Eval(Container.DataItem, "Owner.Gender").ToString (), Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "Owner.FacebookSettings.UseFacebookProfilePicture")), Convert.ToUInt64(DataBinder.Eval(Container.DataItem, "Owner.FacebookSettings.FacebookUserId")) ) %>" border="0" /></a>
									</div>
									<div class="vote_entry" id="votebox" runat="server">
										<div id="divVoteCnt" runat="server">0</div>
										<asp:imagebutton class="votebtn" id="btnVote" imageurl="~/images/contests/vote.gif" width="70" height="26" runat="server" commandargument="" commandname="" />
										<img id="imgloading<%# DataBinder.Eval (Container.DataItem, "ContestEntryId").ToString() %>" alt="" src="/images/ajax-loader_vote_trans.gif" style="display:none;margin-top:3px;" />
									</div>																			
								</div>
								<div class="feed-textContainer">
									<div class="feedText">
										<h4><a href="<%# GetPersonalChannelUrl(DataBinder.Eval (Container.DataItem, "Owner.Username").ToString ()) %>"><%# DataBinder.Eval (Container.DataItem, "Owner.Username").ToString () %></a>:</h4>
										<p><%# Server.HtmlDecode(DataBinder.Eval (Container.DataItem, "Description").ToString ()) %></p> 
						
										<asp:Literal ID="litBlocking" runat="server"></asp:Literal>
						
										<div class="time">(<%# FormatDateTimeSpan (DataBinder.Eval (Container.DataItem, "CreatedDate"), DataBinder.Eval (Container.DataItem, "CurrentDate"))%>) <asp:linkbutton runat="server" id="aDeleteEntry" visible="false" onclientclick="if(!confirm('Are you sure you want to Delete this Entry?')){return false;};" commandargument="" commandname="">Delete</asp:linkbutton></div>
					
									</div><!--feedText-->
									<asp:PlaceHolder ID="phEntryComments" runat="server"></asp:PlaceHolder>
								</div>
							</div>
 							<asp:HiddenField ID="hidContestId" runat="server" Value='<%# Convert.ToInt64 (DataBinder.Eval (Container.DataItem, "ContestId")) %>' />
							<asp:HiddenField ID="hidContestEntryID" runat="server" Value='<%# Convert.ToInt64 (DataBinder.Eval (Container.DataItem, "ContestEntryId")) %>' />
							<asp:HiddenField ID="hidNumberOfComments" runat="server" Value='<%# Convert.ToInt64 (DataBinder.Eval (Container.DataItem, "NumberOfComments")) %>' />
					   </div>  
					   
					</itemtemplate>
				</asp:repeater>

				<div id="divNoData" class="nodata" runat="server" visible="false">No entries yet.  Be the first person to <a class="addentry" id="btnEnterNoEntries" runat="server">Enter this Contest</a></div>
				<div id="divNoDataEnded" class="nodata" runat="server" visible="false">No entries.</div>

				<asp:HiddenField ID="hidContestIdWinner" clientidmode="Static" runat="server" Value="0" />
				<asp:HiddenField ID="hidContestEntryIDWinner" clientidmode="Static" runat="server" Value="0" />
			</div>

			<div style="display:none;">
				<div id="errmsg" class="errmsg">
					<div class="heading">We're Sorry.</div>
					<div><span clientidmode="Static" id="spanErrMsg" runat="server"></span><br /><br /></div>
					<div id="buttons">
						<div class="btncontainer">
							<a class="buttonnarrow" id="btnOk" runat="server" clientidmode="Static" href="javascript:void(0);" onclick="parent.$j.colorbox.close(); return false;"><span class="search">OK</span></a>
						</div><br />
					</div>
				</div>
			</div>

		</contenttemplate>
		<triggers>
			<asp:AsyncPostBackTrigger ControlID="btnReload" EventName="Click" />
			<asp:AsyncPostBackTrigger ControlID="btnVote" EventName="Click" />
		</triggers> 
	</asp:updatepanel>

</div>
