﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GameFameData.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.mykaneva.widgets.GameFameData" %>

<div id="gameFame">

	<asp:repeater id="rptBadges" runat="server" >
		<itemtemplate>
			<div class="dataRow" id="rowContainer" runat="server">
				<div class="framesize-small">
					<div class="frame">
						<div class="imgconstrain">
							<img border="0" src='<%# GetAchievementImageURL (DataBinder.Eval(Container.DataItem, "ImageUrl").ToString(), "me") %>' border="0"/>
						</div>	
					</div>
				</div>
				<div class="info">		    
					<div class="name"><%# DataBinder.Eval(Container.DataItem, "Name").ToString() %></div>
					<div class="desc"><%# DataBinder.Eval(Container.DataItem, "Description").ToString() %></div>
				</div>
				<div class="gameBadges">		 
					<div class="points"><%# DataBinder.Eval(Container.DataItem, "Points").ToString() %> points</div>
					<div class="date"></div>
				</div>					
			</div>
		</itemtemplate>						
	</asp:repeater>

	<div class="noData" id="divNoData" runat="server" visible="false"></div>

</div>
