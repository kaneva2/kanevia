<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="BlogData.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.mykaneva.widgets.BlogData" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../../usercontrols/Pager.ascx" %>

<asp:Literal id="litJS" runat="server"></asp:Literal>

<div id="blogs">
								 
    <div class="edit" id="blogEdit" runat="server">
        <a class="btnmedium grey" id="addBlogEntry" runat="server"><span>Add an Entry</span></a>
        <a class="btnmedium grey" id="manageBlog" runat="server"><span>Manage Blog</span></a>
        <hr class="dataSeparator" />
    </div> 
    <asp:repeater id="repeaterBlogs" runat="server">
        <itemtemplate>
            <div class="dataRow">
                <div class="data">
                    <div class="date"><asp:label id="lblDate" runat="server" /></div>
		            <div class="title"><asp:label id="lblTitle" runat="server" /></div>
		            <div class="body"><%# DataBinder.Eval(Container.DataItem, "BodyText") %></div>
		        </div>
   				<div class="separator"><hr class="dataSeparator" /></div>
	            <div class="editLinks">
                    <div class="comments">
		                <asp:hyperlink id="hlComments2" runat="server"><img id="Img5" src="../images/widgets/widget_commet_bt.gif" width="21" height="21" border="0"/></asp:hyperlink><br />
		                <asp:hyperlink id="hlComments" runat="server"><asp:label id="lblNumComments" runat="server" />&nbsp;Comments</asp:hyperlink>
		            </div>
                    <div class="detail">
		                <asp:hyperlink id="hlDetail2" runat="server"><img id="Img3" src="../images/widgets/widget_blog_info.gif" width="22" height="22" border="0" alt=""/></asp:hyperlink><br />																								
		                <asp:hyperlink id="hlDetail" runat="server" text="Details" />
		            </div>
                    <div class="edit" id="divEdit" runat="server" visible="false">
		                <asp:hyperlink id="hlEdit2" runat="server" visible="False"><img id="Img1" src="../images/widgets/widget_editc_bt.gif" width="20" height="21" border="0" alt=""/></asp:hyperlink><br />
		                <asp:hyperlink id="hlEdit" runat="server" visible="False">Edit</asp:hyperlink>
		            </div> 
		        </div>
	        </div>
	    </itemtemplate>
    </asp:Repeater>

	<div class="noData" id="divNoData" runat="server" visible="false">No blogs have been posted for this profile or World.</div>

</div>							
