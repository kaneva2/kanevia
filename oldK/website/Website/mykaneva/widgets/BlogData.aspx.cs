///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using System.Collections.Generic;
using KlausEnt.KEP.Kaneva.framework.widgets;
using log4net;


namespace KlausEnt.KEP.Kaneva.mykaneva.widgets
{
    public partial class BlogData : BasePage
    {
        #region Declarations
        int communityId = 0;
        private int pageSize = 10;
        private bool showEdit = false;
        private Community community = new Community();
        private int pageNum = 1;
        private string orderBy = "b.created_date DESC";
        private int profileOwnerId = 0;
        private bool isProfileOwner = false;
        private bool isProfileAdmin = false;
        private int userId = 0;
        private int communityTypeId = (int)CommunityType.USER;
        private string communityType = ""; 

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #endregion

        #region PageLoad
        protected void Page_Load(object sender, EventArgs e)
        {
//            if (Request.IsAuthenticated)
//            {
                //clears local cache so data is always new
                Response.ContentType = "text/plain";
                Response.CacheControl = "no-cache";
                Response.AddHeader("Pragma", "no-cache");
                Response.Expires = -1;
                try
                {
                    GetRequestValues();
                    CheckUserAccess();
                    BindData();
                    SetEditOptions();
                }
                catch (Exception ex)
                {
                    m_logger.Error("Error loading Blog widget. ", ex);

                    divNoData.InnerText = "We're sorry but the data could not be retrieved.";
                    divNoData.Visible = true;

                    // Always call this method so the totalRecords value will get updated
                    // and the pager will be hidden if it is not needed
                    litJS.Text = "<script type=\"text/javascript\">SetAjaxPager(1,0);</script>";
                }
//            }
        }
        #endregion

        #region Helper Functions

        private void SetEditOptions()
        {


            if (isProfileOwner || isProfileAdmin)
            {
                blogEdit.Visible = true;

                //set upload href
                addBlogEntry.HRef = this.ResolveUrl("~/blog/blogEdit.aspx?topicId=0&communityId=" + CommunityId);
                manageBlog.HRef = this.ResolveUrl("~/blog/blog.aspx?communityId=" + CommunityId);
                showEdit = true;

                //set callout
                if (divNoData.Visible)
                {
                    divNoData.InnerHtml += "<div>Would you like to <a href=\"" + addBlogEntry.HRef + "\" alt=\"add blog entry\" >Add an Entry</a>" +
                        " or <a href=\"" + manageBlog.HRef + "\" alt=\"manage this blog\" >Manage this Blog</a>?</div>";
                }
            }
            else
            {
                blogEdit.Visible = false;
            }
        }

        /// <summary>
        /// Checks the current user's access level
        /// </summary>
        private void CheckUserAccess()
        {
            int privilegeId = 0;

            //get the users Id
            userId = GetUserId();

            //see if they are the profile/community owner
            isProfileOwner = userId.Equals(profileOwnerId);

            if (!isProfileOwner)
            {
                //get the appropriate privilege id based on the community type
                switch (CommunityTypeId)
                {
                    case (int)CommunityType.USER:
                        privilegeId = (int)SitePrivilege.ePRIVILEGE.USER_PROFILE_ADMIN;
                        break;
                    case (int)CommunityType.COMMUNITY:
                    case (int)CommunityType.APP_3D:
                        privilegeId = (int)SitePrivilege.ePRIVILEGE.COMMUNITY_PROFILE_ADMIN;
                        break;
                }

                //check the privilege level to see if they are an admin
                this.isProfileAdmin = HasWritePrivileges(privilegeId);
            }
        }

        #endregion

        #region Functions

        private void GetRequestValues()
        {
            // Get the community Id
            if (Request["communityId"] != null)
            {
                try
                {
                    CommunityId = Convert.ToInt32(Request["communityId"]);
                }
                catch { }
            }

            // Get orderby field
            if (Request["orderby"] != null)
            {
                try
                {
                    switch (Request["orderby"].ToString().ToLower())
                    {
                        case "date":
                        default:
                            OrderBy = "b.created_date DESC";
                            break;
                    }
                }
                catch { }
            }

            // Get items per page count
            if (Request["pagesize"] != null)
            {
                try
                {
                    PageSize = Convert.ToInt32(Request["pagesize"]);
                }
                catch { }
            }

            // Get the current page number
            if (Request["p"] != null)
            {
                try
                {
                    PageNum = Convert.ToInt32(Request["p"]);
                }
                catch { }
            }

            // Get the type of community 
            if (Request["communityType"] != null)
            {
                try
                {
                    CommunityTypeId = Convert.ToInt32(Request["communityType"]);
                }
                catch { }
            }

            // Get the user Id from the parameters 
            if (Request["profileOwnerId"] != null)
            {
                try
                {
                    ProfileOwnerId = Convert.ToInt32(Request["profileOwnerId"]);
                }
                catch { }
            }
            else
            {
                try
                {
                    ProfileOwnerId = GetCommunityFacade.GetCommunity(CommunityId).CreatorId;
                }
                catch { }
            }
        }

        private void GetCommunityData()
        {
            community = GetCommunityFacade.GetCommunity(communityId);
        }

        private void BindData()
        {
            //pull back community data
            //GetCommunityData();

            PagedList<Blog> pds = this.GetBlogFacade.GetBlogs(CommunityId, "", OrderBy, PageNum, PageSize);

            if (pds.TotalCount > 0)
            {
                repeaterBlogs.DataSource = pds;
                repeaterBlogs.DataBind();
            }
            else
            {
                switch (CommunityTypeId)
                {
                    case (int)CommunityType.USER:
                        communityType = "profile";
                        break;
                    case (int)CommunityType.COMMUNITY:
                    case (int)CommunityType.APP_3D:
                        communityType = "World";
                        break;
                }

                divNoData.Visible = true;
                divNoData.InnerHtml = "<div>No blog entries found for this " + communityType + ".</div>";
            }

            // Always call this method so the totalRecords value will get updated
            // and the pager will be hidden if it is not needed
            litJS.Text = "<script type=\"text/javascript\">SetAjaxPager(" + PageSize.ToString () + "," + pds.TotalCount.ToString () + ");</script>";
        }

        #endregion

        #region Properties

        private int CommunityTypeId
        {
            set { this.communityTypeId = value; }
            get { return this.communityTypeId; }
        }
        private int ProfileOwnerId
        {
            set { this.profileOwnerId = value; }
            get { return this.profileOwnerId; }
        }
        private int CommunityId
        {
            set { this.communityId = value; }
            get { return this.communityId; }
        }
        private int PageNum
        {
            set { this.pageNum = value; }
            get { return this.pageNum; }
        }
        private int PageSize
        {
            set { this.pageSize = value; }
            get { return this.pageSize; }
        }
        private string OrderBy
        {
            set { this.orderBy = value; }
            get { return this.orderBy; }
        }

        #endregion Properties

        #region Event Handlers

        private void repeaterBlogs_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                Label lblTitle = (Label)e.Item.FindControl("lblTitle");
                Label lblDate = (Label)e.Item.FindControl("lblDate");

                lblTitle.Text = (string)DataBinder.Eval(e.Item.DataItem, "Subject");
                lblDate.Text = Convert.ToDateTime(DataBinder.Eval(e.Item.DataItem, "CreatedDate").ToString()).ToString("MMM d, yyyy");

                HtmlContainerControl divEdit = (HtmlContainerControl) e.Item.FindControl ("divEdit");

                int blogId = Int32.Parse(DataBinder.Eval(e.Item.DataItem, "BlogId").ToString());

                if (showEdit)
                {
                    divEdit.Visible = true;
                }
                else
                {
                    divEdit.Visible = false;
                }
                string editUrl = Page.ResolveUrl("~/blog/blogComments.aspx?communityId=" + CommunityId + "&topicId=" + blogId);
                string detailUrl = Page.ResolveUrl("~/blog/" + blogId + ".blog");
                HyperLink hlDetail = (HyperLink)e.Item.FindControl("hlDetail");
                HyperLink hlDetail2 = (HyperLink)e.Item.FindControl("hlDetail2");
                HyperLink hlComments = (HyperLink)e.Item.FindControl("hlComments");
                HyperLink hlComments2 = (HyperLink)e.Item.FindControl("hlComments2");

                hlDetail.NavigateUrl = detailUrl;
                hlDetail2.NavigateUrl = detailUrl;

                hlComments.NavigateUrl = editUrl;
                hlComments2.NavigateUrl = editUrl;

                Label lblNumComments = (Label)e.Item.FindControl("lblNumComments");
                lblNumComments.Text = DataBinder.Eval(e.Item.DataItem, "NumberOfComments").ToString();
            }
        }

        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            this.repeaterBlogs.ItemDataBound += new RepeaterItemEventHandler(this.repeaterBlogs_ItemDataBound);
        }
        #endregion
    }
}
