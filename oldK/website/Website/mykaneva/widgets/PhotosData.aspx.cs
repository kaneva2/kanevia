///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using log4net;

namespace KlausEnt.KEP.Kaneva.mykaneva.widgets
{
    public partial class PhotosData : BasePage
    {
        #region Declerations

        private int communityId = 0;
        private string orderBy = "a.created_date DESC";
        private int pageSize = 20;
        private int pageNum = 1;
        private int profileOwnerId = 0;
        private bool isProfileOwner = false;
        private bool isProfileAdmin = false;
        private int userId = 0;
        private int communityTypeId = (int)CommunityType.USER;
        private string communityType = ""; 

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

        #endregion Declerations

        #region Page Load
        
        protected void Page_Load (object sender, EventArgs e)
        {
            try
            {
                //clears local cache so data is always new
                Response.ContentType = "text/plain";
                Response.CacheControl = "no-cache";
                Response.AddHeader("Pragma", "no-cache");
                Response.Expires = -1;

                photosEdit.Visible = false;

                GetRequestValues();
                CheckUserAccess();
                BindData();
                SetEditOptions();
            }
            catch (Exception ex)
            {
                m_logger.Error ("Error loading Photos widget. ", ex);

                divNoData.InnerText = "We're sorry but the data could not be retrieved.";
                divNoData.Visible = true;

                // Always call this method so the totalRecords value will get updated
                // and the pager will be hidden if it is not needed
                litJS.Text = "<script type=\"text/javascript\">SetAjaxPager(1,0);</script>";
            }
        }

        #endregion Page Load

        #region Helper Functions

        private void SetEditOptions()
        {
            if (isProfileOwner || isProfileAdmin)
            {
                //set upload href
                uploadMedia.HRef = this.ResolveUrl("~/mykaneva/upload.aspx?communityId=" + CommunityId + "&userId=" + profileOwnerId);
                manageMedia.HRef = ResolveUrl("~/asset/publishedItemsNew.aspx?communityId=" + CommunityId + "&userId=" + profileOwnerId);                
                photosEdit.Visible = true;
                
                //set callout
                if (divNoData.Visible)
                {
                    divNoData.InnerHtml += "<div>Would you like to <a href=\"" + uploadMedia.HRef + "\" alt=\"upload media\" >Upload Media</a>" +
                        " or go to your <a href=\"" + manageMedia.HRef + "\" alt=\"media library\" >Media Library</a>?</div>";
                }
            }
            else
            {
                photosEdit.Visible = false;
            }
        }

        /// <summary>
        /// Checks the current user's access level
        /// </summary>
        private void CheckUserAccess()
        {
            int privilegeId = 0;

            //get the users Id
            userId = GetUserId();

            //see if they are the profile/community owner
            isProfileOwner = userId.Equals(profileOwnerId);

            if (!isProfileOwner)
            {
                //get the appropriate privilege id based on the community type
                switch (CommunityTypeId)
                {
                    case (int)CommunityType.USER:
                        privilegeId = (int)SitePrivilege.ePRIVILEGE.USER_PROFILE_ADMIN;
                        break;
                    case (int)CommunityType.COMMUNITY:
                    case (int)CommunityType.APP_3D:
                        privilegeId = (int)SitePrivilege.ePRIVILEGE.COMMUNITY_PROFILE_ADMIN;
                        break;
                }

                //check the privilege level to see if they are an admin
                this.isProfileAdmin = HasWritePrivileges(privilegeId);
            }
        }

        #endregion

        #region Functions

        private void BindData ()
        {            
            // Get the photos
            PagedList<Asset> photos = GetMediaFacade.GetAccessibleAssetsInCommunity (CommunityId, 0,
                KanevaWebGlobals.CurrentUser.HasAccessPass, (int) Constants.eASSET_TYPE.ALL,
                GetUserId (), "(a.asset_type_id = " + (int) Constants.eASSET_TYPE.PICTURE + 
                " or a.asset_type_id = " + (int) Constants.eASSET_TYPE.PATTERN + ")", OrderBy, PageNum, PageSize);

            if (photos.TotalCount > 0)
            {
                dlPhotos.DataSource = photos;
                dlPhotos.DataBind();
            }
            else
            {
                switch (CommunityTypeId)
                {
                    case (int)CommunityType.USER:
                        communityType = "profile";
                        break;
                    case (int)CommunityType.COMMUNITY:
                    case (int)CommunityType.APP_3D:
                        communityType = "World";
                        break;
                }

                divNoData.Visible = true;
                divNoData.InnerHtml = "<div>No photos found for this " + communityType + ".</div>";
            }

            // Always call this method so the totalRecords value will get updated
            // and the pager will be hidden if it is not needed
            litJS.Text = "<script type=\"text/javascript\">SetAjaxPager(" + PageSize.ToString () + "," + photos.TotalCount.ToString () + ");</script>";
        }

        private void GetRequestValues ()
        {
            // Get the community Id
            if (Request["communityId"] != null)
            {
                try
                {
                    CommunityId = Convert.ToInt32 (Request["communityId"]);
                }
                catch { }
            }

            // Get orderby field
            if (Request["orderby"] != null)
            {
                try
                {
                    if (Request["orderby"] != null)
                    {
                        OrderBy = Request["orderby"].ToString();
                        switch (OrderBy.ToLower())
                        {
                            case "raves":
                                OrderBy = "ass.number_of_diggs DESC, a.name";
                                break;
                            case "views":
                                OrderBy = "ass.number_of_downloads DESC";
                                break;
                            case "rand":
                                OrderBy = "Rand()";
                                break;
                            case "date":
                                OrderBy = "a.created_date DESC";
                                break;
                            case "alpha":
                            default:
                                OrderBy = "name ASC";
                                break;
                        }
                    }
                }
                catch { }
            }

            // Get items per page count
            if (Request["pagesize"] != null)
            {
                try
                {
                    PageSize = Convert.ToInt32 (Request["pagesize"]);
                }
                catch { }
            }

            // Get the current page number
            if (Request["p"] != null)
            {
                try
                {
                    PageNum = Convert.ToInt32 (Request["p"]);
                }
                catch { }
            }

            // Get the type of community 
            if (Request["communityType"] != null)
            {
                try
                {
                    CommunityTypeId = Convert.ToInt32(Request["communityType"]);
                }
                catch { }
            }

            // Get the user Id from the parameters 
            if (Request["profileOwnerId"] != null)
            {
                try
                {
                    ProfileOwnerId = Convert.ToInt32(Request["profileOwnerId"]);
                }
                catch { }
            }
            else
            {
                try
                {
                    ProfileOwnerId = GetCommunityFacade.GetCommunity(CommunityId).CreatorId;
                }
                catch { }
            }
        }
        
        #endregion Functions

        #region Properties

        private int CommunityTypeId
        {
            set { this.communityTypeId = value; }
            get { return this.communityTypeId; }
        }
        private int ProfileOwnerId
        {
            set { this.profileOwnerId = value; }
            get { return this.profileOwnerId; }
        }
        private int CommunityId
        {
            set { this.communityId = value; }
            get { return this.communityId; }
        }
        private int PageNum
        {
            set { this.pageNum = value; }
            get { return this.pageNum; }
        }
        private int PageSize
        {
            set { this.pageSize = value; }
            get { return this.pageSize; }
        }
        private string OrderBy
        {
            set { this.orderBy = value; }
            get { return this.orderBy; }
        }

        #endregion Properties
    }
}
