///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using log4net;
using System.Collections.Generic;

namespace KlausEnt.KEP.Kaneva.mykaneva.widgets
{
    public partial class ProfileControlPanel : ModuleViewBaseControl
    {
        #region Declerations

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

        #endregion Declerations

        #region Page Load

        protected void Page_Load (object sender, EventArgs e)
        {
            // Put user code to initialize the page here
            if (!IsPostBack)
            {
                try
                {
                    GetRequestParams ();
                    BindData ();
                }
                catch (Exception ex)
                {
                    m_logger.Error ("Error loading Community Control Panel widget. ", ex);
                    Response.Write ("<div class=\"error\">We're sorry but the data you requested could not be retrieved.</div>");
                }
            }

            // This has to be here so that it will get set every time page loads including post backs.  
            // If not, then the click event handler does not work 
            SetBlockButton (GetUserFacade.GetUser (ProfileOwnerId));
        }

        #endregion Page Load
        
        #region Functions

        private void BindData ()
        {
            // Configures control panel
            ConfigureControlPanel ();

            // Configure hangout 3d
            ConfigureMeetMe3D ();

            // Configure the rave button
            ConfigureRaves ();
        }

        private void ConfigureControlPanel ()
        {
            // Get USER data 
            User userProfileOwner = GetUserFacade.GetUser (ProfileOwnerId);

            divUserName.InnerText = Server.HtmlDecode(userProfileOwner.Username);
            divDisplayName.InnerText = Server.HtmlDecode(userProfileOwner.DisplayName);

            litProfileName.Text = Server.HtmlDecode(userProfileOwner.Username);

            // Setup the Add friend button
            if (GetUserId ().Equals (userProfileOwner.UserId))
            {
                aAddFriend.Attributes["onclick"] = "javascript:" +
                    "alert('You cannot add yourself as a friend.');return (false);";
            }
            else
            {
                if (Request.IsAuthenticated)
                {
                    aAddFriend.Attributes["onclick"] = "javascript:return " +
                        "confirm('Send a friend request to this user?')";
                }
                // If not logged in then user will get sent to login page
            }

            // Set links
            aTellOthers.HRef = ResolveUrl ("~/myKaneva/newMessage.aspx?aboutId=" + userProfileOwner.UserId);
            aSendMessage.HRef = ResolveUrl ("~/myKaneva/newMessage.aspx?userId=" + userProfileOwner.UserId);

            hlEdit.NavigateUrl = ResolveUrl ("~/community/ProfileEdit.aspx?communityId=" + userProfileOwner.CommunityId);

            //show avatar image based on AP settings and viewing users AP pass ownership
            if (userProfileOwner.MatureProfile && !KanevaWebGlobals.CurrentUser.HasAccessPass && (KanevaWebGlobals.CurrentUser.UserId != userProfileOwner.UserId))
            {
                string imagePath = KanevaGlobals.ImageServer;

                imagePath += userProfileOwner.Gender.ToUpper().Equals("M") ? "/KanevaIconMale_sq.gif" : "/KanevaIconFemale_sq.gif";
                imgAvatar.Src = imagePath;
            }
            else
            {
                imgAvatar.Src = GetProfileImageURL(userProfileOwner.ThumbnailSquarePath, "sq", userProfileOwner.Gender, userProfileOwner.FacebookSettings.UseFacebookProfilePicture, userProfileOwner.FacebookSettings.FacebookUserId);
            }
            imgAvatar.Alt = userProfileOwner.Username;
            divRestricted.Visible = userProfileOwner.MatureProfile;

            // set up the image to display using highslide.  If profile is Mature and user viewing
            // profile does not have AP then don't show popout of avatar img
            if (!userProfileOwner.MatureProfile ||
                (userProfileOwner.MatureProfile && KanevaWebGlobals.CurrentUser.HasAccessPass))
            {
                aAvatar.HRef = ResolveUrl ("~/mykaneva/PictureProfileDetail.aspx?userId=" + userProfileOwner.UserId);
                aAvatar.Attributes.Add ("onclick", "return hs.expand(this, { easing: 'easeOutBack', easingClose: 'easeInBack', src: '" +
                    GetProfileImageURL (userProfileOwner.ThumbnailXlargePath, "xl", userProfileOwner.Gender, userProfileOwner.FacebookSettings.UseFacebookProfilePicture, userProfileOwner.FacebookSettings.FacebookUserId) + "' })");
            }

            divUserName.InnerText = Server.HtmlDecode(userProfileOwner.Username);
            divDisplayName.InnerText = Server.HtmlDecode(userProfileOwner.DisplayName);

            //set users online/inworld status
            if (userProfileOwner.Ustate == Constants.ONLINE_USTATE_ON)
            {
                divStatus.InnerHtml = "<img src=\"" + ResolveUrl ("~/images/widgets/widget_online_icon.gif") + "\"></img>&nbsp;&nbsp;Online";
            }

            ////indicate if the user has a VIP/AP membership or not
            imgVIP.Visible = userProfileOwner.HasVIPPass;
            imgAP.Visible = userProfileOwner.HasAccessPass && userProfileOwner.MatureProfile;
            // if profile is AP and viewing user does not have AP hide profile owners AP img (this image
            // shows if this profile owner has purchased AP)
            if (userProfileOwner.MatureProfile && !KanevaWebGlobals.CurrentUser.HasAccessPass)
            {
                imgAP.Visible = false;
            }


            // Get the community so we can check preferences
            Community community = GetCommunityFacade.GetCommunity (userProfileOwner.CommunityId);

            if (community.CommunityId > 0)
            {
                // if age should be shown
                if (community.Preferences.ShowAge)
                {
                    divAge.Visible = true;
                    divAge.InnerText = "" + userProfileOwner.Age +" Years Old";
                }
                else
                {
                    divAge.Visible = false;
                }

                // if gender should be shown
                if (community.Preferences.ShowGender)
                {
                    divGender.Visible = true;
                    divGender.InnerText = userProfileOwner.Gender == "M" ? "Male" : "Female"; ;
                }
                else
                {
                    divGender.Visible = false;
                }

                // if location should be shown
                if (community.Preferences.ShowLocation)
                {
                    divLocation.Visible = true;
                    divLocation.InnerText = userProfileOwner.Location;
                }
                else
                {
                    divLocation.Visible = false;
                }
            }

            if (userProfileOwner.URL.Length.Equals (0))
            {
                divUrl.InnerText = KanevaGlobals.GetPersonalChannelUrl (userProfileOwner.NameNoSpaces);
            }
            else
            {
                divUrl.InnerText = "http://" + KanevaGlobals.SiteName + "/" + userProfileOwner.URL;
            }

            // Set visibility of title bar
            if ((userProfileOwner.UserId == KanevaWebGlobals.CurrentUser.UserId) || (this.IsUserAdministrator()) )
            {
                widget_head.Visible = true;       
            }
            else
            {
                widget_head.Visible = false;
            }

            // Get the summary counts
            spnNumVideos.InnerText = community.Stats.VideoCount.ToString();
            spnNumFriends.InnerText = userProfileOwner.Stats.NumberOfFriends.ToString ();
            spnNumPhotos.InnerText = community.Stats.PhotoCount.ToString();
            spnNumMusic.InnerText = community.Stats.MusicCount.ToString();

            // Set up the links for summary count labels
            aVideos.InnerText = Constants.TabType.VIDEO;
            aVideos.Attributes.Add ("onclick","javascript:$('tab" + Constants.TabType.VIDEO + "').click();");
            aFriends.InnerText = Constants.TabType.FRIEND;
            aFriends.Attributes.Add ("onclick", "javascript:$('tab" + Constants.TabType.FRIEND + "').click();");
            aPhotos.InnerText = Constants.TabType.PICTURE;
            aPhotos.Attributes.Add ("onclick", "javascript:$('tab" + Constants.TabType.PICTURE + "').click();");
            aMusic.InnerText = Constants.TabType.MUSIC;
            aMusic.Attributes.Add ("onclick", "javascript:$('tab" + Constants.TabType.MUSIC + "').click();");
            
            // Display Facebook icon if account connected to Facebook account
            if (userProfileOwner.FacebookSettings.FacebookUserId > 0)
            {
                aFB.Visible = true;
                aFB.HRef = "http://www.facebook.com/" + userProfileOwner.FacebookSettings.FacebookUserId;
                aFB.Title = "View " + KanevaWebGlobals.CurrentUser.DisplayName + " Facebook Profile";
            }

            //display the membership date
            lblMemberSince.Text = Convert.ToDateTime (userProfileOwner.SignupDate.ToString ()).ToShortDateString ();

            // Add Post back event for Add Friend button called by call to action text in
            // other widgets that include a Add as a Friend link
            litPostBackEvt.Text = Page.ClientScript.GetPostBackEventReference (aAddFriend, "");
        }

        private void ConfigureMeetMe3D()
        {
            User userProfileOwner = GetUserFacade.GetUser (ProfileOwnerId);
            ConfigureUserMeetMe3D(btnMeet3D, userProfileOwner.Username, "");
        }

        private void ConfigureRaves ()
        {
            // Which rave graphic do we show?	 
            if (!GetRaveFacade.IsCommunityRaveFree (KanevaWebGlobals.CurrentUser.UserId, CommunityId, RaveType.eRAVE_TYPE.SINGLE))
            {
                spnRave.Attributes.Add ("class", "ravePlus");
                spnRave.InnerText = "RavePlus This";
            }
            else
            {
                spnRave.Attributes.Add ("class", "rave");
                spnRave.InnerText = "Rave This";
            }
            litCommunityId.Text = CommunityId.ToString ();
        }

        private void GetRequestParams ()
        {
            try
            {
                if (CommunityId == 0)
                {
                    if (HttpContext.Current.Items["community"] != null)
                    {
                        Community community = (Community) (HttpContext.Current.Items["community"]);
                        CommunityId = community.CommunityId;
                        this.ProfileOwnerId = community.CreatorId;
                    }
                    else if (Request["communityId"] != null)
                    {
                        //get channelId and find the default page
                        CommunityId = Convert.ToInt32 (Request["communityId"]);
                    }
                    if (Request["userId"] != null)
                    {
                        //get channelId and find the default page
                        this.ProfileOwnerId = Convert.ToInt32 (Request["userId"]);
                    }
                }
            }
            catch (Exception exc)
            {
                m_logger.Error("Error in GetRequestParams", exc);
            }
        }

        private void RaveProfile (RaveType.eRAVE_TYPE eRaveType)
        {
            if (!GetRaveFacade.IsCommunityRaveFree (KanevaWebGlobals.CurrentUser.UserId, CommunityId,
                eRaveType) && !this.IsUserAdministrator ())
            {
                // Call popFacebox
                if (eRaveType == RaveType.eRAVE_TYPE.MEGA)
                {
                    ScriptManager.RegisterClientScriptBlock (litJS, GetType (), "popfacemega", "popFaceboxMega();", true);

					Page.ClientScript.RegisterClientScriptBlock(GetType(), "gaRaveEvent", "callGAEvent('Profile', 'MegaRave', 'MegaRaveClick');", true);
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock (litJS, GetType (), "popface", "popFacebox();", true);

					Page.ClientScript.RegisterClientScriptBlock(GetType(), "gaRaveEvent", "callGAEvent('Profile', 'RavePlus', 'RavePlusClick');", true);
                }
            }
            else  // first time rave
            {
                // First rave is free.  Insert a normal rave.
                int raveOk = GetRaveFacade.RaveCommunity(KanevaWebGlobals.CurrentUser.UserId, CommunityId, WebCache.GetRaveType(eRaveType), "");
                if (raveOk == 0)
                {
                    User userProfileOwner = GetUserFacade.GetUser(ProfileOwnerId);
                    Dictionary<string, string> metricData = new Dictionary<string, string> 
                    {
                        {"username", KanevaWebGlobals.CurrentUser.Username},
                        {"source", Page.Request.CurrentExecutionFilePath},
                        {"target", userProfileOwner.Username}
                    };
                    (new MetricsFacade()).InsertGenericMetricsLog("player_rave_log", metricData, Global.RequestRabbitMQChannel());
                }

				Page.ClientScript.RegisterClientScriptBlock(GetType(), "gaRaveEvent", "callGAEvent('Profile', 'Rave', 'RaveClick');", true);

                spnRave.Attributes.Add ("class", "ravePlus");
                spnRave.InnerText = "RavePlus This";

                // Update the rave count in the Fame Widget
                ScriptManager.RegisterClientScriptBlock (litJS, GetType (), "ravecount", "$(raveCountCtrlName).innerText=(eval($(raveCountCtrlName).innerText)+1);", true);
            }
        }

        /// <summary>
        /// Set the blocked text
        /// </summary>
        private void SetBlockButton (User userProfileOwner)
        {
            if (userProfileOwner.UserId != KanevaWebGlobals.CurrentUser.UserId && !GetUserFacade.IsUserGM(userProfileOwner.UserId))
            {
                if (GetUserFacade.IsUserBlocked (GetUserId (), ProfileOwnerId))
                {
                    lbBlock.Attributes.Remove ("onclick");
                    lbBlock.ServerClick += new EventHandler (btnBlockUser_Click);
                    lbBlock.InnerText = "UnBlock User";
                }
                else
                {
                    lbBlock.Attributes.Add ("onclick", "showBlock('../block.aspx?blockUserId=" + userProfileOwner.UserId.ToString () + "&blockUsername=" + userProfileOwner.Username + "');");
                    lbBlock.ServerClick -= btnBlockUser_Click;
                    lbBlock.InnerText = "Block/Report User";
                }
            }
            else
            {
                lbBlock.Visible = false;
            }
        }

        #endregion Functions

        #region Event Handlers

        /// <summary>
        /// Handle a profile vote
        /// </summary>
        protected void btnRave_Click (object sender, EventArgs e)
        {
            if (!Request.IsAuthenticated)
            {
                Response.Redirect (GetLoginURL ());
            }
            RaveProfile (RaveType.eRAVE_TYPE.SINGLE);
        }
        protected void btnMegaRave_Click (object sender, EventArgs e)
        {
            if (!Request.IsAuthenticated)
            {
                Response.Redirect (GetLoginURL ());
            }
            RaveProfile (RaveType.eRAVE_TYPE.MEGA);
        }

        /// <summary>
        /// Handle a block user
        /// </summary>
        protected void btnBlockUser_Click (object sender, System.EventArgs e)
        {
            // Must be logged in
            if (!Request.IsAuthenticated)
            {
                Response.Redirect (GetLoginURL ());
            }

            // Can't block yourself
            if (GetUserId ().Equals (ProfileOwnerId))
            {
                ScriptManager.RegisterClientScriptBlock (litJS, GetType (), "blockalert", "alert('You cannot block yourself.');", true);
                return;
            }

            if (GetUserFacade.IsUserBlocked(GetUserId(), ProfileOwnerId))
            {
                GetUserFacade.UnBlockUser (GetUserId (), ProfileOwnerId);
            }

            SetBlockButton (GetUserFacade.GetUser (ProfileOwnerId));
        }

        protected void aAddFriend_Click (object sender, EventArgs e)
        {
            if (!Request.IsAuthenticated)
            {
                Response.Redirect (GetLoginURL ());
            }

            string msg = "";
			if (!AddFriend(GetUserId(), ProfileOwnerId, ref msg))
			{
	//			AjaxCallHelper.WriteAlert(msg);
                ScriptManager.RegisterClientScriptBlock (litJS, GetType (), "blockalert", "alert('" + msg + "');", true);
			}
			else
			{
				Page.ClientScript.RegisterClientScriptBlock(GetType(), "gaAddFriendEvent", " callGAEvent('Profile', 'AddFriend');", true);
			}
        }

        #endregion
    }
}