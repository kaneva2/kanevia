<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CommunityControlPanel.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.mykaneva.widgets.CommunityControlPanel" %>
<%@ Register TagPrefix="UserControl" TagName="LightBox" Src="../../usercontrols/LightBox.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<usercontrol:lightbox runat="server" id="ucLightBox" />

<LINK href="../css/meetme3d.css" type="text/css" rel="stylesheet">

<script type="text/javascript">    
var $j = jQuery.noConflict();	  
var communityId = <asp:literal id="litCommunityId" runat="server"></asp:literal>;    
function GetCommunityId () { return communityId; }
$j(document).ready(function()
{				
	popFacebox=function()
	{
		$j.facebox({ iframe:'../buyRave.aspx?communityId=' + communityId + '&type=single' });
	}
	popFaceboxMega=function()
	{
		$j.facebox({ iframe:'../buyRave.aspx?communityId=' + communityId + '&type=mega' });
	}	
});

var raveCountCtrlName = '<asp:literal id="litCtrlRaveCount" runat="server"></asp:literal>'; 

// used to allow anchor click events in non-IE browsers
if (typeof HTMLElement != 'undefined' && !HTMLElement.prototype.click)
	HTMLElement.prototype.click = function () {
		var evt = this.ownerDocument.createEvent('MouseEvents');
		evt.initMouseEvent('click', true, true, this.ownerDocument.defaultView, 1, 0, 0, 0, 0, false, false, false, false, 0, null);
		this.dispatchEvent(evt);
	}
</script>

<div id='<asp:literal id="litCPId" runat="server"></asp:literal>' class='<asp:literal id="litCPClassName" runat="server"></asp:literal>'>

	<div id="widget_head" class="widgetHeaders" runat="server"> 
		<div class="widgetTitle" id="divTitle" runat="server"></div>
		<div class="widgetEdit" id="divEditArea" runat="server" visible="false">
			<asp:hyperlink id="hlEdit" tooltip="edit this widget" runat="server"><span>edit</span></asp:hyperlink>
		</div>
	</div>

	<div class="widget_container">

	<div class="imageContainer" >
		<div class="framesize-large">
			<div id="divRestricted" class="restricted" runat="server" visible="false"></div>
			<div class="frame">
				<div class="imgconstrain">
					<img runat="server" id="imgAvatar" src="~/images/KanevaIcon01.gif" border="0" />
				</div>	
			</div>
		</div>
	</div>

    <div id="divManageThumbnailContainer" runat="server" class="manageThumnailContainer" visible="false">
        <a id="aManageThumbnail" runat="server" href="javascript:void(0);">Change Thumbnail</a>
    </div>

	<div class="infoContainer" >	
		<div class="name" id="divCommunityName" runat="server"></div>
		<p class="description" id="divDescription" runat="server"></p>
	</div>

	<asp:linkbutton cssclass="btnsmall blue meet3d" id="btnMeet3D" runat="server" CausesValidation="False" text="Play Now"></asp:linkbutton>

	<div id="links">
			
		<div style="padding-left:0;height:16px;margin:6px 0;">
			<asp:updatepanel id="upRave" runat="server" style="padding:0;margin:0;">
				<contenttemplate>
					<asp:linkbutton cssclass="" id="btnVote2" runat="server" onclick="btnVote_Click" CausesValidation="False" ><span class="rave" id="spnRave" runat="server"></span></asp:linkbutton>
				</contenttemplate>
			</asp:updatepanel>
		</div>	
            		
		<div class="addfriend"><a id="aTellOthers" runat="server">Invite Friends</a></div>
    	<div class="event" id="divEvent" runat="server"><a id="aEvent" runat="server">Add Event</a></div>
		
		<div class="raves">
			<asp:updatepanel id="Updatepanel1" runat="server" style="padding:0;margin:0;">
				<contenttemplate>
					Raves (<span id="spnNumRaves" runat="server"></span>)
				</contenttemplate>
			</asp:updatepanel>
		</div>

		<div class="members"><a href="javascript:$('tabMembers').click();" id="aMembers" runat="server"></a> (<span id="spnNumMembers" runat="server"></span>)</div>
		<div class="contact" id="divContact" runat="server"><a id="aContact" runat="server">Contact Owner</a></div>
		<div class="manage" id="divManage" runat="server"><a id="aManage" runat="server">Manage World</a></div>
			
		<div class="report"><a id="aReportAbuse" runat="server">Report Abuse</a></div>
		<div class="quit" id="divQuit" runat="server" visible="false"><a id="aQuitChannel" runat="server">Quit World</a></div>
 		
		<asp:label cssclass="pending" id="PendingDivide" runat="server" Visible="False">Membership Pending</asp:label>	
	</div>					

	<div class="smallStaticText url" id="commUrl" runat="server"></div>

	</div>

</div>
