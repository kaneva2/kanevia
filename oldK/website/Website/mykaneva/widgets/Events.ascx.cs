///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Kaneva.BusinessLayer.BusinessObjects;
using KlausEnt.KEP.Kaneva.framework;

namespace KlausEnt.KEP.Kaneva.mykaneva.widgets
{
	public partial class Events : ModuleViewBaseControl
	{
		#region Declarations

		private Community community = new Community();

		#endregion Declarations

		#region Page Load

		protected void Page_Load(object sender, EventArgs e)
		{
			/*
			if (  BasePage.IsAdministrator())
			{
				//show add delete controls if user has correct permissions
				divAddDelete.Visible = true;
			}
			*/
		}

		#endregion Page Load

		#region Properties

		public Community CurrentCommunity
		{
			get
			{
				try
				{
					if (this.community.CommunityId == 0)
					{
						this.community = (Community)(HttpContext.Current.Items["community"]);
					}
				}
				catch { }

				return this.community;
			}
		}

		#endregion Properties
	}
}