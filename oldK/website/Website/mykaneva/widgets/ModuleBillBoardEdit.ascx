<%@ Register TagPrefix="CE" Namespace="CuteEditor" Assembly="CuteEditor" %>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="ModuleBillBoardEdit.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.mykaneva.widgets.ModuleBillBoardEdit" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<style>.tableTopLine { BORDER-TOP: #666666 1px solid }
</style>
<table style="FONT-FAMILY: arial" cellSpacing="0" cellPadding="0" width="100%" border="0">
	<tr>
		<td align="left">
			<table cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td style="PADDING-BOTTOM: 5px; PADDING-TOP: 15px">
						<table cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td class="popupBody" style="PADDING-LEFT: 10px" vAlign="top" width="250"><b>Title: </b>
								</td>
								<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 10px" vAlign="top"><CE:EDITOR id="txtTitle" BackColor="#ededed" runat="server" MaxTextLength="2000" width="100%"
										Height="200px" ShowHtmlMode="False" ConfigurationPath="~/CuteSoft_Client/CuteEditor/Configuration/AutoConfigure/asset.config" AutoConfigure="None"></CE:EDITOR></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td style="PADDING-BOTTOM: 5px; PADDING-TOP: 15px">
						<table cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td class="popupBody" style="PADDING-LEFT: 10px" vAlign="top" width="250"><b>Header 
										Text: </b>
								</td>
								<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 10px" vAlign="top"><CE:EDITOR id="txtIntroText" BackColor="#ededed" runat="server" MaxTextLength="100000" width="100%"
										Height="200px" ShowHtmlMode="True" ConfigurationPath="~/CuteSoft_Client/CuteEditor/Configuration/AutoConfigure/asset.config" AutoConfigure="None"></CE:EDITOR></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td style="PADDING-BOTTOM: 5px; PADDING-TOP: 15px">
						<table cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td class="popupBody" style="PADDING-LEFT: 10px" vAlign="top" width="250"><b># Results 
										To Return: </b>
								</td>
								<td>
									<asp:dropdownlist class="Filter2" style="PADDING-LEFT: 10px" id="ddlResultsToReturn" runat="server"
										size="1"></asp:dropdownlist><asp:comparevalidator id="cpv_Results2Return" runat="server" Operator="NotEqual" ControlToValidate="ddlResultsToReturn"
										ValueToCompare="-1" Font-Bold="True" Font-Italic="True" ErrorMessage="Please select the number of results to return."></asp:comparevalidator>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td style="PADDING-BOTTOM: 5px; PADDING-TOP: 15px">
						<table cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td class="popupBody" style="PADDING-LEFT: 10px" vAlign="top" width="250"><b>Results 
										Per Page: </b>
								</td>
								<td>
									<asp:dropdownlist class="Filter2" style="PADDING-LEFT: 10px" id="ddl_ResultsPerPage" runat="server"
										size="1"></asp:dropdownlist><asp:comparevalidator id="cpv_ResultsPerPage" runat="server" Operator="NotEqual" ControlToValidate="ddl_ResultsPerPage"
										ValueToCompare="-1" Font-Bold="True" Font-Italic="True" ErrorMessage="Please select the number of results to display per page."></asp:comparevalidator>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td style="PADDING-BOTTOM: 5px; PADDING-TOP: 15px">
						<table cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td class="popupBody" style="PADDING-LEFT: 10px" vAlign="top" width="250"><b>Show 
										Mature Content: </b>
								</td>
								<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 10px" vAlign="top" align="left"><asp:checkbox class="formKanevaText" id="cbxShowMature" runat="server" Text="Show Mature"></asp:checkbox></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td style="PADDING-BOTTOM: 5px; PADDING-TOP: 15px">
						<table cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td class="popupBody" style="PADDING-LEFT: 10px" vAlign="top" width="250"><b>Select 
										Image Size: </b>
								</td>
								<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 10px" vAlign="top" align="left">
									<asp:radiobuttonlist id="rblSize" runat="server" RepeatDirection="Horizontal" CssClass="Filter2" CellPadding="5"></asp:radiobuttonlist></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td style="PADDING-BOTTOM: 5px; PADDING-TOP: 15px">
						<table cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td class="popupBody" style="PADDING-LEFT: 10px" vAlign="top" width="250"><b>Media 
										Information Location: </b>
								</td>
								<td class="formKanevaText"><asp:radiobutton id="rbnRight" runat="server" Font-Size="11pt" GroupName="mediaLocation" Checked="True"
										Text="Display On Right"></asp:radiobutton>&nbsp;<asp:radiobutton id="rbnBottom" runat="server" Font-Size="11pt" GroupName="mediaLocation" Text="Display Below"></asp:radiobutton>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td style="PADDING-BOTTOM: 5px; PADDING-TOP: 15px">
						<table cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td class="popupBody" style="PADDING-LEFT: 10px" vAlign="top" width="250"><b>Footer 
										Text: </b>
								</td>
								<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 10px" vAlign="top"><CE:EDITOR id="footerEditer" BackColor="#ededed" runat="server" MaxTextLength="100000" width="100%"
										Height="200px" ShowHtmlMode="True" ConfigurationPath="~/CuteSoft_Client/CuteEditor/Configuration/AutoConfigure/asset.config" AutoConfigure="None"></CE:EDITOR></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 10px" noWrap>
						<hr style="BORDER-RIGHT: #dfdfdf 1px solid; BORDER-TOP: #dfdfdf 1px solid; BORDER-LEFT: #dfdfdf 1px solid; WIDTH: 100%; BORDER-BOTTOM: #dfdfdf 1px solid; TEXT-ALIGN: right">
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td style="PADDING-RIGHT: 10px; PADDING-TOP: 20px; TEXT-ALIGN: right"><input style="FONT-WEIGHT: bold; CURSOR: hand" onclick="window.close()" type="button" value=" cancel ">
			&nbsp;&nbsp;
			<asp:button id="btnSave" style="FONT-WEIGHT: bold; CURSOR: hand" Text=" save changes " Runat="server"></asp:button></td>
	</tr>
</table>
