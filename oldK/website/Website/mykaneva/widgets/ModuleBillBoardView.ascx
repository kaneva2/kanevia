<%@ Control Language="c#" AutoEventWireup="false" Codebehind="ModuleBillBoardView.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.mykaneva.widgets.ModuleBillBoardView" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../../usercontrols/Pager.ascx" %>

<LINK href="../css/thickbox.css" type="text/css" rel="stylesheet">
<link href="../css/showCaseWidgets.css" type="text/css" rel="stylesheet">

<script language="javascript" src="../jscript/jquery-compressed.js"></script>
<script language="javascript" src="../jscript/thickbox.js"></script>

<ajax:ajaxpanel id="Ajaxpanel1" runat="server">

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="widgetContent">
	<tr>
		<td width="100%" id="pageOpacity">
		<div id="widgetBorder">
		<b class="outerFrame" id="outerFrame">
		<b class="outerFrame1"><b></b></b>
		<b class="outerFrame2"><b></b></b>
		<b class="outerFrame3"></b>
		<b class="outerFrame4"></b>
		<b class="outerFrame5"></b>
		</b> 
		<div class="outerFrame_content" id="outerFrame_content">
		<!-- Your Content Goes Here -->
			<table cellpadding="0" cellspacing="0" border="0" width="100%">
				<tr>
					<td>
						<div id="widgetHeader">
							<b class="innerFrame">
							<b class="innerFrame1"><b></b></b>
							<b class="innerFrame2"><b></b></b>
							<b class="innerFrame3"></b>
							<b class="innerFrame4"></b>
							<b class="innerFrame5"></b>
							</b> 
							<div class="innerFrameTitle_content">
							<!-- Your Content Title Goes Here -->
								<table cellpadding="0" cellspacing="0" border="0" width="100%">
									<tr>
										<td class="widgetTitle">
											<div id="divTitle" runat="server"/>
										</td>
										<td align="right">
											<table cellpadding="0" cellspacing="0" border="0" id="tblEdit" runat="server">
												<tr>
													<td nowrap>
														<asp:linkbutton id="lbEdit" runat="server" cssclass="widgetEditButton" title="edit this widget">edit</asp:linkbutton><asp:linkbutton id="lbDelete" runat="server" title="remove this widget" cssclass="widgetRemoveButton">X</asp:linkbutton>
													</td>
												</tr>
											</table>	
										</td>
									</tr>
								</table>
							</div>
							<b class="innerFrame">
							<b class="innerFrame5"></b>
							<b class="innerFrame4"></b>
							<b class="innerFrame3"></b>
							<b class="innerFrame2"><b></b></b>
							<b class="innerFrame1"><b></b></b>
							</b>
						</div> 
					</td>
				</tr>
				<tr>
					<td><img runat="server" src="~/images/spacer.gif" border="0" width="1" height="1" id="Img4"/></td>
				</tr>				
				<tr>
					<td valign="top" height="100%">
						<div id="widgetBody">
							<b class="innerFrame">
							<b class="innerFrame1"><b></b></b>
							<b class="innerFrame2"><b></b></b>
							<b class="innerFrame3"></b>
							<b class="innerFrame4"></b>
							<b class="innerFrame5"></b>
							</b> 
							<div class="innerFrame_content">
								<!-- Your Content Goes Here -->
								
								
								<asp:label id=lbl_Error runat="server" ForeColor="#C00000" Font-Bold="True" Font-Italic="True"></asp:label>
								<table width="100%" cellpadding="0" cellspacing="0" border="0">
									<tr valign="middle">
										<td class="widgetText">
											<asp:Label id="lblIntroText" runat="server"/>
										</td>
									</tr>
								</table>
								
								
								<table cellpadding="5" cellspacing="20" border="0" style="width:100%">
									<tr>
										<td style="width:50%" valign="top">
											<!-- Search Results -->
											<table cellpadding="5" cellspacing="0" border="0" style="width:100%;">
												<tr>
													<td class="widgetText widgetContestSearchResults" style="border-top: 1px dashed #cccccc; border-bottom: 1px dashed #cccccc">
														<table border="0" cellpadding="0" cellspacing="0" width="100%">
															<tr>
																<td class="widgetText" align="left" style="padding:0 0px 0 10px">
																	<asp:label runat="server" id="lblSearch" cssclass="widgetText"/>		
																</td>
																<td  class="widgetText" align="right" style="padding-right:10px">
																	<kaneva:pager runat="server" isajaxmode="True" id="pgTop"/>
																</td>
															</tr>
															<asp:Label id="lbl_Noresults" runat="server" Visible="False" BackColor="White" BorderColor="Blue"
																Font-Size="Large">No results were found.</asp:Label>
														</table>
													</td>
												</tr>
												<tr>
													<td class="widgetContestSearchResults">
														<asp:datalist visible="true" runat="server" showfooter="False" id="dlRightDisplay"
															cellpadding="0" cellspacing="20" border="0" repeatcolumns="1" repeatdirection="Horizontal" Width="100%">
															<ItemStyle HorizontalAlign="center" CssClass="widgetText"></ItemStyle>
															<itemtemplate>
																<table cellpadding="0" cellspacing="10" border="0" style="width:100%">
																	<tr>
																		<td rowspan="2" runat="server" id="tdRankRight" valign="middle" style="width:10%" class="widgetText" align="middle">
																			<span style="font-size:12px" class="smallrank">Rank</span><br/>
																			<span style="font-size:22px" class="rank"><%# DataBinder.Eval(Container.DataItem, "rank")%></span>
																		</td>
																		<td valign="top" style="width:30%" align="center">
																			<div class="<%= imageSizeStyleSheet %>">
																				<div class="frame">
																					<div class="restricted" runat="server" visible='<%# Convert.ToInt32(DataBinder.Eval(Container.DataItem, "mature_profile")).Equals(1) %>' ID="Div2"></div>
																					<span class="ct"><span class="cl"></span></span>
																					<div class="imgconstrain" >
																						<!-- <a title='<%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "name").ToString ())%>' href='javascript:void(0);' onclick="<%# GetThickBoxJScript (DataBinder.Eval (Container.DataItem, "name").ToString (),Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "asset_id")),-1)%>" style="width:100%;height:100%">-->
																						<a title='<%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "name").ToString ())%>' href='<%# GetAssetDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "asset_id")))%>' style="width:100%;height:100%">
																							<img border="0" src='<%# GetMediaImageURL (DataBinder.Eval(Container.DataItem, "thumbnail_path").ToString () , DataBinder.Eval(Container.DataItem, "thumbnail_size").ToString (), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_id")), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_type_id")), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "thumbnail_gen"))) %>' />
																						</a>
																						<img style="z-index:2; position: absolute; right: 0px; display: block; bottom: 0px" border="0" alt='<%#GetAssetTypeName(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "asset_type_id")))%>' title='<%#GetAssetTypeName(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "asset_type_id")))%>' src='<%#GetAssetTypeIcon(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "asset_type_id")))%>' border="0"/>
																					</div>
																					<span class="cb"><span class="cl"></span></span>
																				</div>
																			</div>
																		</td>
																		<td style="width:60%" class="widgetText" id="tdRightAsset" runat="server">
																			<!--<p class="small"><a href='javascript:void(0);' onclick="<%# GetThickBoxJScript (DataBinder.Eval (Container.DataItem, "name").ToString (),Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "asset_id")),-1)%>"><%# TruncateWithEllipsis (DataBinder.Eval(Container.DataItem, "name").ToString(),18)%></a></p>-->
																			<p class="small"><a class="contestUploaded" href='<%# GetAssetDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "asset_id")))%>'><%# TruncateWithEllipsis (DataBinder.Eval(Container.DataItem, "name").ToString(),18)%></a></p>
																			<p class="small">From: <a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "owner_username").ToString ())%>><%# TruncateWithEllipsis (DataBinder.Eval (Container.DataItem, "owner_username").ToString(),18) %></a></p>
																			<p class="small">Raves: <span class="contestYesVotes"><%# DataBinder.Eval(Container.DataItem, "number_of_diggs")%></span></p>
																		</td>
																	</tr>
																</table>	
															</itemtemplate>
														</asp:datalist>
														<asp:datalist visible="true" runat="server" showfooter="False" id="dlBottomDisplay"
															cellpadding="0" cellspacing="20" border="0" repeatcolumns="1" repeatdirection="Horizontal" Width="100%">
															<ItemStyle HorizontalAlign="center" CssClass="widgetText"></ItemStyle>
															<itemtemplate>
																<table cellpadding="0" cellspacing="0" border="0" style="width:100%">
																	<tr>
																		<td rowspan="2" runat="server" id="tdRankBottom" valign="middle" style="width:10%" class="widgetText" align="middle">
																			<span style="font-size:12px" class="smallrank">Rank</span><br/>
																			<span style="font-size:22px"class="rank"><%# DataBinder.Eval(Container.DataItem, "rank")%></span>
																		</td>
																		<td valign="top" style="width:30%" align="center">
																			<div class="<%= imageSizeStyleSheet %>">
																				<div class="frame">
																					<div class="restricted" runat="server" visible='<%# Convert.ToInt32(DataBinder.Eval(Container.DataItem, "mature_profile")).Equals(1) %>' ID="Div1"></div>
																					<span class="ct"><span class="cl"></span></span>
																					<div class="imgconstrain" >
																						<!-- <a title='<%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "name").ToString ())%>' href='javascript:void(0);' onclick="<%# GetThickBoxJScript (DataBinder.Eval (Container.DataItem, "name").ToString (),Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "asset_id")),-1)%>" style="width:100%;height:100%">-->
																						<a title='<%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "name").ToString ())%>' href='<%# GetAssetDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "asset_id")))%>'  style="width:100%;height:100%">
																							<img border="0" src='<%# GetMediaImageURL (DataBinder.Eval(Container.DataItem, "thumbnail_path").ToString () , DataBinder.Eval(Container.DataItem, "thumbnail_size").ToString (), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_id")), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_type_id")), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "thumbnail_gen"))) %>' />
																						</a>
																						<img style="z-index:2; position: absolute; right: 0px; display: block; bottom: 0px" border="0" alt='<%#GetAssetTypeName(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "asset_type_id")))%>' title='<%#GetAssetTypeName(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "asset_type_id")))%>' src='<%#GetAssetTypeIcon(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "asset_type_id")))%>' border="0"/>
																					</div>
																					<span class="cb"><span class="cl"></span></span>
																				</div>
																			</div>
																		</td>
																	</tr>
																	<tr>
																		<td class="widgetText" align="center" id="tdBottomAsset" runat="server">
																			<!--<p class="small"><a href='javascript:void(0);' onclick="<%# GetThickBoxJScript (DataBinder.Eval (Container.DataItem, "name").ToString (),Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "asset_id")),-1)%>"><%# TruncateWithEllipsis (DataBinder.Eval(Container.DataItem, "name").ToString(),18)%></a></p>-->
																			<p class="small"><a class="contestUploaded" href='<%# GetAssetDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "asset_id")))%>'><%# TruncateWithEllipsis (DataBinder.Eval(Container.DataItem, "name").ToString(),18)%></a></p>
																			<p class="small">From: <a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "owner_username").ToString ())%>><%# TruncateWithEllipsis (DataBinder.Eval (Container.DataItem, "owner_username").ToString(),18) %></a></p>
																			<p class="small">Raves: <span class="contestYesVotes"><%# DataBinder.Eval(Container.DataItem, "number_of_diggs")%></span></p>
																		</td>
																	</tr>
																</table>
															</itemtemplate>
														</asp:datalist>
													</td>
												</tr>
												<tr>
													<td class="widgetText widgetContestSearchResults" style="border-top: 1px dashed #cccccc; border-bottom: 1px dashed #cccccc" align="right">
														<kaneva:pager isajaxmode="True" runat="server" id="pgBottom"/>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
								<table cellpadding="0" cellspacing="0" border="0">
									<tr valign="middle">
										<td class="widgetText">
											<asp:Label id="lblFooterText" runat="server"/>
										</td>
									</tr>
								</table>
								
								
								
								<!-- Your Content Ends Here -->
							</div>
							<b class="innerFrame">
							<b class="innerFrame5"></b>
							<b class="innerFrame4"></b>
							<b class="innerFrame3"></b>
							<b class="innerFrame2"><b></b></b>
							<b class="innerFrame1"><b></b></b>
							</b>
						</div> 
					</td>
				</tr>
			</table>
		</div>
		<b class="outerFrame">
		<b class="outerFrame5"></b>
		<b class="outerFrame4"></b>
		<b class="outerFrame3"></b>
		<b class="outerFrame2"><b></b></b>
		<b class="outerFrame1"><b></b></b>
		</b>
		</div> 
		</td>
	</tr>
	<!---END CHANNEL LIST--->
</table>
<br/>
</ajax:ajaxpanel>

