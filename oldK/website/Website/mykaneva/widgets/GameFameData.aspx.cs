///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects.API;
using log4net;

namespace KlausEnt.KEP.Kaneva.mykaneva.widgets
{
    public partial class GameFameData : BasePage
    {
        #region Declarations

        int communityId = 0;
        uint badgeId = 0;
        uint badgePoints = 0;
        string badgeName = "";
        string badgeDesc = "";

        private int profileOwnerId = 0;
        private bool isProfileOwner = false;
        private bool isProfileAdmin = false;
        private int userId = 0;

        private int pageSize = 10;
        private int pageNum = 1;
        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

        #endregion

        #region PageLoad

        protected void Page_Load (object sender, EventArgs e)
        {
            if (Request.IsAuthenticated)
            {
                try
                {
                    //clears local cache so data is always new
                    Response.ContentType = "text/plain";
                    Response.CacheControl = "no-cache";
                    Response.AddHeader ("Pragma", "no-cache");
                    Response.Expires = -1;

                    GetRequestValues ();
                    CheckUserAccess ();

                    // Show the badges
                    BindData ();
                    SetEditOptions ();
                }
                catch (Exception ex)
                {
                    m_logger.Error ("Error loading Forums widget. ", ex);

                    divNoData.InnerText = "We're sorry but the data could not be retrieved.";
                    divNoData.Visible = true;

                    // Always call this method so the totalRecords value will get updated
                    // and the pager will be hidden if it is not needed
  //                  litJS.Text = "<script type=\"text/javascript\">SetAjaxPager(1,0);</script>";
                }
            }
        }

        #endregion

        #region Functions

        private void BindData ()
        {
            // Check to see if owner has created a leaderboard for this 3D App
            PagedList<Achievement> achievements = GetGameFacade.AchievementForApp (CommunityId, "", PageNum, PageSize);

            if (achievements.TotalCount > 0)
            {
                rptBadges.DataSource = achievements;
                rptBadges.DataBind ();
            }
            else
            {
                divNoData.Visible = true;
                divNoData.InnerHtml = "<div>This 3D App does not have any badges.</div>";

                // Always call this method so the totalRecords value will get updated
                // and the pager will be hidden if it is not needed
 //               litJS.Text = "<script type=\"text/javascript\">SetAjaxPager(1,0);</script>";

                return;
            }
            // Always call this method so the totalRecords value will get updated
            // and the pager will be hidden if it is not needed
//            litJS.Text = "<script type=\"text/javascript\">SetAjaxPager(" + PageSize.ToString () + "," + achievements.TotalCount.ToString () + ");</script>";
        }

        private void GetRequestValues ()
        {
            // Get the community Id
            if (Request["communityId"] != null)
            {
                try
                {
                    CommunityId = Convert.ToInt32 (Request["communityId"]);
                }
                catch { }
            }

            // Get the blase Id
            if (Request["badgeId"] != null)
            {
                try
                {
                    BadgeId = Convert.ToUInt32 (Request["badgeId"]);
                }
                catch { }
            }

            // Get items per page count
            if (Request["pagesize"] != null)
            {
                try
                {
                    PageSize = Convert.ToInt32 (Request["pagesize"]);
                }
                catch { }
            }

            // Get the current page number
            if (Request["p"] != null)
            {
                try
                {
                    PageNum = Convert.ToInt32 (Request["p"]);
                }
                catch { }
            }

            // Get the user Id from the parameters 
            if (Request["profileOwnerId"] != null)
            {
                try
                {
                    ProfileOwnerId = Convert.ToInt32 (Request["profileOwnerId"]);
                }
                catch { }
            }
            else
            {
                try
                {
                    ProfileOwnerId = GetCommunityFacade.GetCommunity (CommunityId).CreatorId;
                }
                catch { }
            }
        }

        /// <summary>
        /// Checks the current user's access level
        /// </summary>
        private void CheckUserAccess ()
        {
            //get the users Id
            userId = GetUserId ();

            //see if they are the profile/community owner
            isProfileOwner = userId.Equals (profileOwnerId);

            if (!isProfileOwner)
            {
                //check the privilege level to see if they are an admin
                this.isProfileAdmin = HasWritePrivileges ((int) SitePrivilege.ePRIVILEGE.COMMUNITY_PROFILE_ADMIN);
            }
        }

        public bool AllowedToEdit ()
        {
            bool allowEditing = false;

            //check security level
            switch (KanevaWebGlobals.CheckUserAccess ((int) SitePrivilege.ePRIVILEGE.COMMUNITY_PROFILE_ADMIN))
            {
                case (int) SitePrivilege.eACCESS_LEVEL.ACCESS_FULL:
                    allowEditing = true;
                    break;
                case (int) SitePrivilege.eACCESS_LEVEL.ACCESS_READ:
                    break;
                case (int) SitePrivilege.eACCESS_LEVEL.ACCESS_NONE:
                    break;
                default:
                    break;
            }

            return allowEditing;
        }

        private void SetEditOptions ()
        {
            if ((isProfileOwner || this.isProfileAdmin))
            {
//                editBadges.HRef = this.ResolveUrl ("~/community/3dapps/AppManagement.aspx?communityid=" + CommunityId);
//                divEdit.Visible = true;
            }
        }

        #endregion Functions

        #region Properties

        private int ProfileOwnerId
        {
            set { this.profileOwnerId = value; }
            get { return this.profileOwnerId; }
        }
        private int CommunityId
        {
            set { this.communityId = value; }
            get { return this.communityId; }
        }
        private int PageNum
        {
            set { this.pageNum = value; }
            get { return this.pageNum; }
        }
        private int PageSize
        {
            set { this.pageSize = value; }
            get { return this.pageSize; }
        }
        private uint BadgeId
        {
            set { this.badgeId = value; }
            get { return this.badgeId; }
        }
        private string BadgeName
        {
            set { this.badgeName = value; }
            get { return this.badgeName; }
        }
        private string BadgeDesc
        {
            set { this.badgeDesc = value; }
            get { return this.badgeDesc; }
        }
        private uint BadgePoints
        {
            set { this.badgePoints = value; }
            get { return this.badgePoints; }
        }

        #endregion Properties
    }
}