<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Photos.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.mykaneva.widgets.Photos" %>

<script type="text/javascript"><!--
document.observe("dom:loaded", InitAjax);
var orderBy = '';
function InitAjax () { AjaxReqInit (GetAjaxParams(),'<%= ResolveUrl("~/mykaneva/widgets/PhotosData.aspx") %>'); }
function SetDisplayOrder (obj) { orderBy=obj.id; InitAjax(); }
function GetAjaxParams () {	return 'communityId=<%= CurrentCommunity.CommunityId %>&orderby='+orderBy; }
--> 
</script> 

<div>
	<div><a id="aUpload" runat="server">Upload Media</a>   <a id="aMediaLibrary" runat="server">Media Library</a></div>
	<div>Sort by: <a href="javascript:void(0);" onclick="setDisplayOrder(this);" id="date">Newest</a> |  <a href="javascript:void(0);" onclick="setDisplayOrder(this);" id="raves">Most Popular</a></div>
	
	<div id="divAjaxData"><img id="Img1" class="imgLoading" runat="server" src="~/images/ajax-loader.gif" height="24" width="24" alt="Loading..." border="0"/><div class="loadingText">Loading...</div></div> 
 
	<div class="modulePaging">
		<div id="divAjaxPager"><!-- pager --></div>  
	</div>
</div>