<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProfileControlPanel.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.mykaneva.widgets.ProfileControlPanel" %>

<script type="text/javascript" src="../jscript/highslide/highslide.js"></script>
<script type="text/javascript" src="../jscript/highslide/easing_equations.js"></script>
<script type="text/javascript" src="../jscript/highslide/highslide_cfg_profile.js"></script>

<script type="text/javascript">    
var $j = jQuery.noConflict();	  
var communityId = <asp:literal id="litCommunityId" runat="server"></asp:literal>;   
var profileName = '<asp:literal id="litProfileName" runat="server"></asp:literal>';   
function GetCommunityId () { return communityId; }
$j(document).ready(function()
{				
	popFacebox=function()
	{
	    $j.facebox({ iframe:'../buyRave.aspx?communityId=' + communityId + '&type=single&raveProfileName=' + profileName });
	}
	popFaceboxMega=function()
	{
	    $j.facebox({ iframe:'../buyRave.aspx?communityId=' + communityId + '&type=mega&raveProfileName=' + profileName });
	}
	showBlock = function(url) { 	
		$j.facebox({ iframe:url });
	}
});
function AddFriend()
{
	<asp:literal id="litPostBackEvt" runat="server" />;
}
var raveCountCtrlName = 'lblRaves'; 

// used to allow anchor click events in non-IE browsers
if (typeof HTMLElement != 'undefined' && !HTMLElement.prototype.click) {
    HTMLElement.prototype.click = function () {
        var evt = this.ownerDocument.createEvent('MouseEvents');
        evt.initMouseEvent('click', true, true, this.ownerDocument.defaultView, 1, 0, 0, 0, 0, false, false, false, false, 0, null);
        this.dispatchEvent(evt);
    }
}
function setBlockText()
{	
	$j("#lbBlock").text("UnBlock User").prop( "onclick", null ).on("click", function() {
		<%= Page.ClientScript.GetPostBackEventReference(btnBlock, String.Empty) %>;
	}); 
	$j.facebox.close();	
}
</script>

<div id="controlPanel">


	<div id="widget_head" class="widgetHeaders" runat="server"> 
		<div class="widgetTitle" id="divTitle" runat="server">Profile</div>
		<div class="widgetEdit" id="divEditArea" runat="server">
			<asp:hyperlink id="hlEdit" tooltip="edit this widget" runat="server"><span>edit</span></asp:hyperlink>
		</div>
	</div>

	<div class="widget_container">

		<div class="imageContainer" >
			<div class="framesize-square">
				<div id="divRestricted" class="restricted" runat="server" visible="false"></div>
				<div class="frame">
					<div class="imgconstrain">
						<a id="aAvatar" runat="server" class="nohover highslide slow"><img runat="server" id="imgAvatar" src="~/images/KanevaIcon01.gif" border="0" /></a>
					</div>	
				</div>
			</div>
			<div class="passes" id="divPasses" runat="server"><img src="~/images/pass_content.gif" width="15" height="15" alt="Access Pass" id="imgAP" runat="server" /><img src="~/images/VIPBadge.gif" width="15" height="15" alt="VIP" id="imgVIP" runat="server" /></div>
		</div>

		<div class="infoContainer">
			<div class="userName" id="divUserName" runat="server"></div>
			<div class="displayName" id="divDisplayName" runat="server"></div>
			<div class="location" id="divLocation" runat="server"></div>
			<div class="gender" id="divGender" runat="server"></div>
			<div class="age" id="divAge" runat="server"></div>
			<div class="status" id="divStatus" runat="server"></div>
		</div>


		<div class="buttonsConatainer">	
			<!-- Left column buttons -->
			<div class="buttonsLeftCol">
 				<asp:updatepanel id="upRave" runat="server">
					<contenttemplate>
						<asp:linkbutton cssclass="btnsmall grey" id="btnRave" runat="server" onclick="btnRave_Click" CausesValidation="False" ><span class="rave" id="spnRave" runat="server"></span></asp:linkbutton>
					</contenttemplate>
				</asp:updatepanel>
				<div><a class="btnsmall grey" id="aSendMessage" runat="server"><span class="sendMessage">Send Message</span></a></div>
				<asp:updatepanel id="up" runat="server">
					<contenttemplate>
						<div><a class="btnsmall grey" id="aAddFriend" runat="server" onserverclick="aAddFriend_Click" href="javascript:void(0);"><span class="addFriend">Add Friend</span></a></div>
					</contenttemplate>
				</asp:updatepanel>
			</div>

			<!-- Right column buttons -->
			<div class="buttonsRightCol">
 				<asp:updatepanel id="upMegaRave" runat="server">
					<contenttemplate>
						<asp:linkbutton class="btnsmall grey" id="btnMegaRave" runat="server" onclick="btnMegaRave_Click" CausesValidation="False" ><span class="megaRave">MegaRave This</span></asp:linkbutton>
					</contenttemplate>
				</asp:updatepanel>
			
				<div><asp:linkbutton class="btnsmall grey" id="btnMeet3D" runat="server" CausesValidation="False"><span class="meet3D">Meet Me 3D</span></asp:linkbutton></div>
			
				<div><a class="btnsmall grey" id="aTellOthers" runat="server"><span class="tellOthers">Tell Others</span></a></div>
			</div>
		</div>

		<div id="summaryCounts">
			<div class="sumCountLeftCol">
				<div class="videos"><a href="javascript:void(0);" runat="server" id="aVideos"></a> (<span id="spnNumVideos" runat="server"></span>)</div>
				<div class="friends"><a href="javascript:void(0);" runat="server" id="aFriends"></a> (<span id="spnNumFriends" runat="server"></span>)</div>
			</div>
			<div class="sumCountRightCol">	
				<div class="photos"><a href="javascript:void(0);" runat="server" id="aPhotos"></a> (<span id="spnNumPhotos" runat="server"></span>)</div>
				<div class="music"><a href="javascript:void(0);" runat="server" id="aMusic"></a> (<span id="spnNumMusic" runat="server"></span>)</div>
			</div>
		</div>
	
		<a href="" class="fbIcon" target="_blank" id="aFB" visible="false" runat="server"><img src="~/images/facebook/fbsmallicon_gray.png" width="14" height="14" runat="server" border="0" /></a>   
	
		<div class="smallStaticText member">Member Since: <asp:label id="lblMemberSince" runat="server"></asp:label></div>
		<div class="smallStaticText url" id="divUrl" runat="server"></div>

		<asp:updatepanel id="upBlockUser" runat="server">
			<contenttemplate>
				<div class="reportAbuse">
					<a href="javascript:void(0);" id="lbBlock" runat="server" clientidmode="static"></a>
				</div>
				<asp:literal id="litJS" runat="server"></asp:literal>
				<asp:button id="btnBlock" runat="server" onclick="btnBlockUser_Click" style="visibility:hidden;" />
			</contenttemplate>
		</asp:updatepanel>

	</div>



</div>

