<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CommunityMembers.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.mykaneva.widgets.CommunityMembers" %>

<script type="text/javascript"><!--
document.observe("dom:loaded", InitAjax);
var orderBy=''; var filter='';
function InitAjax () { AjaxReqInit (GetAjaxParams(),'<%= ResolveUrl("~/mykaneva/widgets/CommunityMembersData.aspx") %>'); }
function SetDisplayOrder (obj) { orderBy=obj.value; filter=''; InitAjax(); }
function SetFilter (obj){ filter=obj.innerText; orderBy=''; $('selSortBy').value='alpha'; InitAjax(); }
function GetAjaxParams () {	return 'communityId=<%= CurrentCommunity.CommunityId %>&filter='+filter+'&orderby='+orderBy; }
--> 
</script> 


<div>
	<div id="memberFilter">
		<a href="javascript:void(0);" onclick="SetFilter(this);">A</a>|
		<a href="javascript:void(0);" onclick="SetFilter(this);">B</a>|
		<a href="javascript:void(0);" onclick="SetFilter(this);">C</a>|
		<a href="javascript:void(0);" onclick="SetFilter(this);">D</a>|
		<a href="javascript:void(0);" onclick="SetFilter(this);">E</a>|
		<a href="javascript:void(0);" onclick="SetFilter(this);">F</a>|
		<a href="javascript:void(0);" onclick="SetFilter(this);">G</a>|
		<a href="javascript:void(0);" onclick="SetFilter(this);">H</a>|
		<a href="javascript:void(0);" onclick="SetFilter(this);">I</a>|
		<a href="javascript:void(0);" onclick="SetFilter(this);">J</a>|
		<a href="javascript:void(0);" onclick="SetFilter(this);">K</a>|
		<a href="javascript:void(0);" onclick="SetFilter(this);">L</a>|
		<a href="javascript:void(0);" onclick="SetFilter(this);">M</a>|
		<a href="javascript:void(0);" onclick="SetFilter(this);">N</a>|
		<a href="javascript:void(0);" onclick="SetFilter(this);">O</a>|
		<a href="javascript:void(0);" onclick="SetFilter(this);">P</a>|
		<a href="javascript:void(0);" onclick="SetFilter(this);">Q</a>|
		<a href="javascript:void(0);" onclick="SetFilter(this);">R</a>|
		<a href="javascript:void(0);" onclick="SetFilter(this);">S</a>|
		<a href="javascript:void(0);" onclick="SetFilter(this);">T</a>|
		<a href="javascript:void(0);" onclick="SetFilter(this);">U</a>|
		<a href="javascript:void(0);" onclick="SetFilter(this);">V</a>|
		<a href="javascript:void(0);" onclick="SetFilter(this);">W</a>|
		<a href="javascript:void(0);" onclick="SetFilter(this);">X</a>|
		<a href="javascript:void(0);" onclick="SetFilter(this);">Y</a>|
		<a href="javascript:void(0);" onclick="SetFilter(this);">Z</a>|
	</div>
	<div id="memberSortBy">
	sort by: 
	<select id="selSortBy" onchange="SetDisplayOrder(this);">
		<option value="alpha">Alphabetical</option>
		<option value="raves">Most Raves</option>
		<option value="views">Most Views</option>
		<option value="date">Newest</option>
	</select>
	</div>
	<div id="divAjaxData"><img id="Img1" class="imgLoading" runat="server" src="~/images/ajax-loader.gif" height="24" width="24" alt="Loading..." border="0"/><div class="loadingText">Loading...</div></div> 
 
	<div class="modulePaging">
		<div id="divAjaxPager"><!-- pager --></div>  
	</div>
       
</div>
