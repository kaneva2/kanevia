///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Web.UI;
using KlausEnt.KEP.Kaneva.usercontrols;

namespace KlausEnt.KEP.Kaneva.mykaneva.widgets
{
	/// <summary>
	/// Summary description for ModuleEditBaseControl.
	/// </summary>
	public class ModuleEditBaseControl : BaseUserControl
	{
		#region Variables

		private int		_channelId;
		private int		_pageModuleId;
		private int		_pageId;
		private int		_moduleId;
		private int		_zoneId;
		private int		_sequence;
		private bool	_returning_to_view = false;	// once this control closes, whether it is returning to view mode

		#endregion

		public ModuleEditBaseControl() : base()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		#region Methods

		/// <summary>
		/// close the edit module and update the parent page with the new module info
		/// <param name="newId"></param>
		/// </summary>
		protected void CloseAndUpdateParent(int newId)
		{
			//reload parent window and close this one
			string javascript = "";

			if (ReturningToView)
			{
				javascript = "<script language=JavaScript>window.opener.location = window.opener.location; window.close()</script>";
			}
			else
			{
				if (newId > -1)
				{
					javascript = "<script language=JavaScript>window.opener.widget_page.AddWidgetInstance('" + newId + "'); window.close()</script>";
				}
				else
				{
					javascript = "<script language=JavaScript>window.close()</script>";
				}
			}

            Page.ClientScript.RegisterStartupScript(GetType(), "CloseAndUpdateParent", javascript);
		}

		#endregion

		#region Properties

		public int PageModuleId
		{
			get { return _pageModuleId; }
			set { _pageModuleId = value; }
		}

		public int ModuleId
		{
			get { return _moduleId; }
			set { _moduleId = value; }
		}

		public int ZoneId
		{
			get { return _zoneId; }
			set { _zoneId = value; }
		}

		public int Sequence
		{
			get { return _sequence; }
			set { _sequence = value; }
		}

		public int PageId
		{
			get { return _pageId; }
			set { _pageId = value; }
		}

		public bool ReturningToView
		{
			get { return _returning_to_view; }
			set { _returning_to_view = value; }
		}

		public int ChannelId
		{
			get { return _channelId; }
			set { _channelId = value; }
		}

		#endregion
	}
}
