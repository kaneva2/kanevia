///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva.mykaneva.widgets

{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	using MagicAjax;
	using KlausEnt.KEP.Kaneva.framework.widgets;

	/// <summary>
	///		Summary description for ModuleProfilePortalView.
	/// </summary>
	public class ModuleProfilePortalView : ModuleViewBaseControl
	{
		protected	LinkButton	lbEdit, lbDelete;
		protected	ImageButton	lblOnline;
		protected	HtmlImage	voteImg;


		protected	HtmlTable	tblEdit;
		protected	Label		lblTitle;

		protected	Label		lblUserName;
		protected	HtmlImage	imgAvatar, imgAdd, imgMsg;
		protected	LinkButton	btnAddFriend, btnAddFriend2;
		protected	LinkButton	btnSendMessage, btnSendMessage2;
		protected	LinkButton	btnBlockUser, btnBlockUser2;
		protected	LinkButton	btnReportAbuse, btnReportAbuse2;
		protected	LinkButton	btnTellOthers, btnTellOthers2;
		protected	Label		lblAge;
		protected	Label		lblGender;
		protected	Label		lblLocation;
		protected	Label		lblMemberSince;
		protected	Label		lblLastLogin;
		protected	Label		lblLastUpdate;
		protected	HyperLink	hlPerm;
		protected	LinkButton	btnVote, btnVote2;
		protected	Literal		litOnline, litFlyoutImage, litFlyoutContent;

		protected	HtmlContainerControl	controlSection1, controlSection2, controlSection3;
		protected	HtmlContainerControl	divUrl;
		protected	HtmlAnchor	aPictures;
		protected	MagicAjax.UI.Controls.AjaxPanel Ajaxpanel2;
		protected	MagicAjax.UI.Controls.AjaxPanel Ajaxpanel1;

		protected   HtmlAnchor aAvatar;

		protected Literal litIndexTools;

		// Digg someone
		protected	Label		lblVotes;

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			if(!IsPostBack)
			{
				BindData();
			}

			if (GetUserId ().Equals (ProfileOwnerId))
			{
				 
				btnAddFriend2.Attributes["onclick"] = "javascript:" +
					"alert('You cannot add yourself as a friend.');return (false);";
			}
			else
			{
				
				btnAddFriend2.Attributes["onclick"] = "javascript:return " +
					"confirm('Send a friend request to this user?')"; 
			}

			//open the editor window when user clicks edit button
			lbEdit.Attributes["onclick"] = "window.open('../mykaneva/ModuleEditor.aspx?modid=" + this.InstanceId
				+"&pageId=" + PageId + "' , 'ModuleEditor', 'width=750,height=680,scrollbars=1,resizable=1,status=1,toolbar=0'); return false;";

			
			SetBlockedText ();

			lbDelete.Visible = false;

			if ( Zone.Equals(Constants.eMODULE_ZONE.HEADER) || Zone.Equals(Constants.eMODULE_ZONE.FOOTER) )
			{
				controlSection1.Style.Add ("width", "33%");
				controlSection2.Style.Add ("width", "33%");
				controlSection3.Style.Add ("width", "33%");
			}
			else if ( TotalWidth > 570 )
			{
				controlSection1.Style.Add ("width", "98%");
				controlSection2.Style.Add ("width", "45%");
				controlSection3.Style.Add ("width", "45%");
			}
			else
			{
				controlSection1.Style.Add ("width", "98%");
				controlSection2.Style.Add ("width", "98%");
				controlSection3.Style.Add ("width", "98%");
			}

			divUrl.Style.Add ("width", (TotalWidth-30).ToString ());
													  
		}

		private void BindData()
		{
			tblEdit.Visible = ShowEdit;

			// If user has at least 1 photo, then display Show More Photos link
            //DataRow drMedia = StoreUtility.GetUserMediaCounts (ProfileOwnerId);

            DataRow drMedia = StoreUtility.GetChannelMediaCounts(ChannelId);
            aPictures.Visible = false;
			
			try 
			{
				if (Convert.ToUInt32(drMedia["sum_picture"]) > 0)
				{
					aPictures.Visible = true;
					aPictures.HRef = ResolveUrl ("~/mykaneva/pictures.aspx?userId=" + ProfileOwnerId);
				}
			} 
			catch {}


			//gather USER data 
            UserFacade userFacade = new UserFacade();
            User userProfileOwner = userFacade.GetUser(ProfileOwnerId);

			//if profile owner is the same as the viewing user
			//hide the meet me in three-D fly out
			//else continue checks.
			if (GetUserId() != ProfileOwnerId)
			{
				//check to see if browsing user is logged in/a member of kaneva
				if (Request.IsAuthenticated)
				{
					//browsing user is a member of WOK
                    if (KanevaWebGlobals.CurrentUser.HasWOKAccount)
					{
						//check to see if profile owner is a wok member
						//profile owner and browsing user are WOK members
                        if (userProfileOwner.WokPlayerId > 0)
						{
							litFlyoutImage.Text = "<IMG height=\"100\" alt=\"Virtual World of Kaneva\" src=\"../images/meetme_worldshot.jpg\" width=\"270\" border=\"0\" />";
							litFlyoutContent.Text = "<p><A class=nohover href=" + ResolveUrl("~/kgp/playwok.aspx") + "?goto=P" + ProfileOwnerId.ToString() + "><IMG height=\"30\" alt=\"Meet Me In 3D\" src=\"../images/button_meetme3d.gif\" width=\"135\" border=\"0\" /></a></p>";
						}
						//profile owner is not a wok member (browsing user is)
						else
						{
							//allow user option of going in world anyway
							litFlyoutImage.Text = "<A class=nohover href=" + ResolveUrl("~/kgp/playwok.aspx") + "?goto=A" + ProfileOwnerId.ToString() + "><IMG height=\"100\" alt=\"Login to WOK\" src=\"../images/Go2MyHome.jpg\" width=\"270\" border=\"0\"></a>";
							litFlyoutContent.Text = "<p><A class=nohover href=" + ResolveUrl("~/kgp/playwok.aspx") + "?goto=C1>Go To Mall</a></p>";
						}
					}							
					//browsing user is not a memeber of WOK (profile owner status unknown) send user to download page
					else
					{
						litFlyoutImage.Text = "<a class=\"meetme\" href=\"http://www.kaneva.com/woktest\"><IMG height=\"100\" alt=\"Download the WOK Client Now\" src=\"../images/download_client.jpg\" width=\"270\" border=\"0\" /></a>";
						//litFlyoutContent.Text = "<a class=\"meetme\" href=\"http://www.kaneva.com/woktest\"><IMG height=\"30\" alt=\"Download WOK\" src=\"../images/button_dowloadWOK.gif\" width=\"135\" border=\"0\" /></a>";
					}
				}
				//browsingUser is not logged in show only login/join kaneva (no one's WOK status know)
				else 
				{
					litFlyoutImage.Text = "<IMG height=\"100\" alt=\"Join Kaneva\" src=\"../images/join_kaneva.jpg\" width=\"270\" border=\"0\">";
					litFlyoutContent.Text = "<p><a class=\"meetme nohover\" href=\"../loginSecure.aspx\"><IMG height=\"30\" alt=\"Meet Me In 3D\" src=\"../images/JoinButton.gif\" width=\"135\" border=\"0\" /></a></p>";
				}
			}
			else
			{
				//show link buttons to WOK (profile owner is member of WOK)
                if (userProfileOwner.WokPlayerId > 0)
				{
					litFlyoutImage.Text = "<A class=nohover href=" + ResolveUrl("~/kgp/playwok.aspx") + "?goto=A" + ProfileOwnerId.ToString() + "><IMG height=\"100\" alt=\"Login to WOK\" src=\"../images/Go2MyHome.jpg\" width=\"270\" border=\"0\"></a>";
					litFlyoutContent.Text = "<p><A class=nohover href=" + ResolveUrl("~/kgp/playwok.aspx") + "?goto=A" + ProfileOwnerId.ToString() + ">Go To Mall</a></p>";
				}
				//show link to download WOK (profile owner is NOT member of WOK)
				else
				{
					litFlyoutImage.Text = "<a class=\"meetme\" href=\"http://www.kaneva.com/woktest\"><IMG height=\"100\" alt=\"Download the WOK Client Now\" src=\"../images/download_client.jpg\" width=\"270\" border=\"0\" /></a>";
					//litFlyoutContent.Text = "<a class=\"meetme\" href=\"http://www.kaneva.com/woktest\"><IMG height=\"30\" alt=\"Download WOK\" src=\"../images/button_dowloadWOK.gif\" width=\"135\" border=\"0\" /></a>";
				}
			}

            imgAvatar.Src = UsersUtility.GetProfileImageURL(userProfileOwner.ThumbnailMediumPath, "me", userProfileOwner.Gender);
            imgAvatar.Alt = userProfileOwner.Username;

			aAvatar.HRef = ResolveUrl ("~/mykaneva/PictureProfileDetail.aspx?userId=" + ProfileOwnerId);

            lblUserName.Text = userProfileOwner.Username;

            if (userProfileOwner.Online.Equals (1) == userProfileOwner.ShowOnline)
			{
				litOnline.Text = "<img src=\"" + ResolveUrl("~/images/widgets/widget_online_icon.gif") + "\"></img>&nbsp;&nbsp;Online";
			}

			int age = -1;
			string gender = "";
			string location = "";

            age = userProfileOwner.Age;

			// get gender
            gender = userProfileOwner.Gender == "M" ? "Male" : "Female";

			// get 'location'
            location = userProfileOwner.Location;

			//DataRow drControlPanel = WidgetUtility.GetModuleControlPanel(InstanceId);
			DataRow drControlPanel = WidgetUtility.GetLayoutModuleProfilePortal(InstanceId);

			if (drControlPanel != null)
			{			

				// if age should be shown
				if ( drControlPanel ["show_age"].ToString () == "1" )
				{
					lblAge.Visible = true;
					lblAge.Text = "" + age + " Years Old";
				}
				else
				{
					lblAge.Visible = false;
				}

				// if gender should be shown
				if ( drControlPanel ["show_gender"].ToString () == "1" )
				{
					lblGender.Visible = true;
					lblGender.Text = "" + gender;
				}
				else
				{
					lblGender.Visible = false;
				}

				// if location should be shown
				if ( drControlPanel ["show_location"].ToString () == "1" )
				{
					lblLocation.Visible = true;
					lblLocation.Text = location;
				}
				else
				{
					lblLocation.Visible = false;
				}
			}

			//digg
			lblVotes.Text = userProfileOwner.NumberOfDiggs.ToString ();

			// The permalink
			string permLink = "";
			
			if (userProfileOwner.URL.Length.Equals (0))
			{
				permLink = KanevaGlobals.GetPersonalChannelUrl (userProfileOwner.NameNoSpaces);
			}
			else
			{
				permLink = "http://" + KanevaGlobals.SiteName + "/" + userProfileOwner.URL;
			}
			
			hlPerm.Text = permLink;
			hlPerm.Attributes.Add ("alt",permLink);
			hlPerm.NavigateUrl = permLink;

			DataRow drSystemStats = CommunityUtility.GetCommunityStats (ChannelId);
			if ( drSystemStats != null )
			{
				lblMemberSince.Text	= Convert.ToDateTime (drSystemStats ["signup_date"].ToString ()).ToShortDateString ();
				lblLastLogin.Text	= Convert.ToDateTime (drSystemStats ["last_login"].ToString ()).ToShortDateString ();
				lblLastUpdate.Text	= Convert.ToDateTime (drSystemStats ["last_update"].ToString ()).ToShortDateString ();
			}
		}

		#region Methods

		/// <summary>
		/// Set the blocked text
		/// </summary>
		private void SetBlockedText ()
		{
            UserFacade userFacade = new UserFacade();
            if (userFacade.IsUserBlocked(GetUserId(), ProfileOwnerId))
			{
				btnBlockUser.Text = "Unblock User";
			}	
			else
			{
				btnBlockUser.Text = "Block User";
			}
		}

		private void AddFriend ()
		{
			if (Request.IsAuthenticated)
			{
				// Add them as a friend
				int ret = GetUserFacade.InsertFriendRequest (GetUserId (), ProfileOwnerId);
				
				if (ret.Equals (1))	// already a friend
				{
					AjaxCallHelper.WriteAlert ("This member is already your friend.");
				}
				else if (ret.Equals (2))  // pending friend request
				{
					AjaxCallHelper.WriteAlert ("You have already sent this member a friend request.");
				}
				else
				{
                    // send request email
                    MailUtilityWeb.SendFriendRequestEmail(GetUserId(), ProfileOwnerId);                    
				}
				
			}
		}

		
		/*private string meetMe3D
		{
			get
			{
				return _meetMe3D;
			}
			set
			{
				_meetMe3D = value;
			}
		}*/

		#endregion

		#region Event Handlers

		/// <summary>
		/// Handle a profile vote
		/// </summary>
		protected void btnVote_Click (object sender, EventArgs e)
		{
			if (!Request.IsAuthenticated)
			{
				Response.Redirect (GetLoginURL ());
			}

			if (CommunityUtility.InsertDigg (GetUserId (), ChannelId).Equals (1))
			{
				AjaxCallHelper.WriteAlert ("You have already raved this community.");
			}
			else
			{
				lblVotes.Text = Convert.ToString (Convert.ToInt32 (lblVotes.Text) + 1);
			}

			//disables buttons if user is not an admin
			if(!this.IsUserAdministrator())
			{
				btnVote.Enabled = false;
				btnVote2.Enabled = false;
				btnVote.Style.Add("cursor","default");
				btnVote2.Style.Add("cursor","default");

				//show icon indicating the he item has been raved
				this.voteImg.Src = ResolveUrl("~/images/widgets/widget_raved.gif");
				voteImg.Style.Add("cursor","default");

			}

		}

		private void lbDelete_Click(object sender, EventArgs e)
		{
			//			WidgetUtility.DeleteModuleControlPanel(InstanceId);
			//			FireModuleDeletedEvent();
		}

		/// <summary>
		/// Handle a report abuse
		/// </summary>
		protected void btnReportAbuse_Click (object sender, System.EventArgs e)
		{																													   
			Response.Redirect (ResolveUrl ("~/suggestions.aspx?mode=WB&category=KANEVA%20Web%20Site&rurl=" + Server.UrlEncode(Request.Url.ToString())));
		}

		private void btnAddFriend_Click(object sender, EventArgs e)
		{
			// Must be logged in
			if (!Request.IsAuthenticated)
			{
				AjaxCallHelper.WriteAlert ("Please sign in to add this user as a friend");
				return;
			}

			// Can't friend yourself
			if (GetUserId ().Equals (ProfileOwnerId))
			{
				AjaxCallHelper.WriteAlert ("You cannot add yourself as a friend.");
				return;
			}

			AddFriend ();
			//Response.Redirect (Request.CurrentExecutionFilePath + "?communityId=" + ChannelId;
		}

		/// <summary>
		/// Handle a profile vote
		/// </summary>
		protected void btnTellOthers_Click (object sender, System.EventArgs e)
		{
			Response.Redirect(ResolveUrl ("~/myKaneva/newMessage.aspx?aboutId=" + this.ProfileOwnerId));
		}

		/// <summary>
		/// Handle a block user
		/// </summary>
		protected void btnBlockUser_Click (object sender, System.EventArgs e)
		{
			// Must be logged in
			if (!Request.IsAuthenticated)
			{
				AjaxCallHelper.WriteAlert ("Please sign in to block/unblock this user");
				return;
			}
			
			// Can't block yourself
			if (GetUserId ().Equals (ProfileOwnerId))
			{
				AjaxCallHelper.WriteAlert ("You cannot block yourself.");
				return;
			}

            UserFacade userFacade = new UserFacade();
			if (userFacade.IsUserBlocked (GetUserId (), ProfileOwnerId))
			{
				UsersUtility.UnBlockUser (GetUserId (), ProfileOwnerId);
			}
			else
			{
				UsersUtility.BlockUser (GetUserId (), ProfileOwnerId);
			}

			SetBlockedText ();
		}

		private void btnSendMessage_Click(object sender, EventArgs e)
		{
			Response.Redirect(ResolveUrl ("~/myKaneva/newMessage.aspx?userId=" + this.ProfileOwnerId));
		}

		#endregion
		
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
			//btnAddFriend.Click += new EventHandler(btnAddFriend_Click);
			btnAddFriend2.Click += new EventHandler(btnAddFriend_Click);
			//btnSendMessage.Click += new EventHandler(btnSendMessage_Click);
			btnSendMessage2.Click += new EventHandler(btnSendMessage_Click);

			//			lbDelete.Attributes["onclick"] = "javascript:return " +
			//				"confirm('Are you sure you want to delete this widget?')"; 

			lbDelete.Click += new EventHandler(lbDelete_Click);
			btnVote.Click += new EventHandler(btnVote_Click);
			btnVote2.Click += new EventHandler(btnVote_Click);
		}
		#endregion
	}
}
