///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Web.UI;

namespace KlausEnt.KEP.Kaneva.mykaneva.widgets
{
	/// <summary>
	/// Summary description for IModuleView.
	/// </summary>
	public class ModuleViewBaseControl : KlausEnt.KEP.Kaneva.usercontrols.BaseUserControl
	{
		private		int		_instance_id;
		private		bool	_showEdit;
		private		bool	_showDelete;
		private		int		_page_id;
		private		int		_moduleId;
		private		int		_channelId;

		private		int		_width;
		private		Constants.eMODULE_ZONE _zone;

		public ModuleViewBaseControl() : base()
		{
		}

		public delegate void ModuleDeletedEventHandler(object sender, ModuleDeletedEventArgs e);
		public event ModuleDeletedEventHandler ModuleDeleted;

		protected void FireModuleDeletedEvent()
		{
			FireModuleDeletedEvent(false);
		}

		protected void FireModuleDeletedEvent(bool reload_parent)
		{
			if( ModuleDeleted != null)
			{
				ModuleDeleted(this, new ModuleDeletedEventArgs(reload_parent));
			}
		}

		public int ProfileOwnerId
		{
            get
            {
                if (ViewState["ProfileOwnerId"] == null)
                {
                    ViewState["ProfileOwnerId"] = 0;
                }
                return (int) ViewState["ProfileOwnerId"];
            }
            set
            {
                ViewState["ProfileOwnerId"] = value;
            }
		}

		public int InstanceId
		{
			get { return _instance_id; }
			set { _instance_id = value; }
		}

		public int ModuleId
		{
			get { return _moduleId; }
			set { _moduleId = value; }
		}

		public int PageId
		{
			get { return _page_id; }
			set { _page_id = value; }
		}

		/// <summary>
		/// whether or not to show the edit buttons
		/// </summary>
		public bool ShowEdit
		{
			get { return _showEdit; }
			set { _showEdit = value; }
		}

		/// <summary>
		/// whether or not to show the delete button
		/// </summary>
		public bool ShowDelete
		{
			get { return _showDelete; }
			set { _showDelete = value; }
		}

		public Constants.eMODULE_ZONE Zone
		{
			get { return _zone; }
			set { _zone = value; }
		}

		public int TotalWidth
		{
			get { return _width; }
			set { _width = value; }
		}

        public virtual int ChannelId
        {
            get { return _channelId; }
            set { _channelId = value; }
        }

        public int CommunityId
		{
            get
            {
                if (ViewState["CommunityId"] == null)
                {
                    ViewState["CommunityId"] = 0;
                }
                return (int) ViewState["CommunityId"];
            }
            set
            {
                ViewState["CommunityId"] = value;
            }
        }

		/// <summary>
		/// Return the current user id
		/// </summary>
		/// <returns></returns>
		protected int GetUserId ()
		{
			return KanevaWebGlobals.GetUserId ();
		}

		/// <summary>
		/// GetPhotoURLFromSize
		/// </summary>
		/// <param name="size"></param>
		/// <returns></returns>
		public string GetPhotoURLFromSize (int size, System.Data.DataRow drv)
		{
			switch ( size )
			{
				case (int) Constants.ePICTURE_SIZE.THUMBNAIL:
					return GetPhotoImageURL (drv ["thumbnail_small_path"].ToString(), "sm");

				case (int) Constants.ePICTURE_SIZE.SMALL:
					return GetPhotoImageURL (drv ["thumbnail_medium_path"].ToString(), "me");

				case (int) Constants.ePICTURE_SIZE.MEDIUM:
					return GetPhotoImageURL (drv ["thumbnail_large_path"].ToString(), "la");

				case (int) Constants.ePICTURE_SIZE.LARGE:
					return GetPhotoImageURL (drv ["thumbnail_xlarge_path"].ToString(), "la");

				case (int) Constants.ePICTURE_SIZE.ACTUAL:
					return GetPhotoImageURL (drv ["image_full_path"].ToString(), "la");

				case (int) Constants.ePICTURE_SIZE.RELATIVE:
					return GetPhotoImageURL (drv ["thumbnail_large_path"].ToString(), "la");

				default:
					return GetPhotoImageURL (drv ["thumbnail_large_path"].ToString(), "la");
			}
		}
	}

	public class ModuleDeletedEventArgs : EventArgs
	{
		bool _reload_parent;

		public ModuleDeletedEventArgs(bool reload_parent) : base()
		{
			_reload_parent = reload_parent;
		}

		public bool ReloadParent
		{
			get { return _reload_parent; }
			set { _reload_parent = value; }
		}
	}

}
