﻿<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="MyWorldsData.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.mykaneva.widgets.MyWorldsData" %>

<asp:Literal id="litJS" runat="server"></asp:Literal>

<div id="worlds">

<div id="communitiesEdit" runat="server" class="edit">
    <a href="~/mykaneva/my3dapps.aspx" class="btnlarge grey" id="editWorlds" runat="server">Manage Worlds</a>
</div>																										

<asp:datalist visible="true" runat="server" showfooter="False" width="99%" id="dlCommunities" 
	cellpadding="0" cellspacing="0" border="0" 
	repeatcolumns="1" horizontalalign="Center" repeatdirection="Horizontal" gridlines="none" 
	repeatlayout="table" OnItemDataBound="dlCommunities_ItemDataBound" >	
	<headertemplate><hr class="dataSeparator top" /></headertemplate>
	<itemtemplate>
		<div class="dataRow">																				  
		    <div class="image">
				<div class="framesize-small">
					<div id="Div2" class="restricted" runat="server" visible='<%# (DataBinder.Eval(Container.DataItem, "IsAdult").Equals ("Y")) %>'></div>
					<div class="frame">
						<div class="imgconstrain">
							<a href='<%# GetBroadcastChannelUrl (DataBinder.Eval (Container.DataItem, "NameNoSpaces").ToString ())%>' id="aImgPlay" runat="server">
								<img id="Img2" runat="server" src='<%# GetBroadcastChannelImageURL (DataBinder.Eval(Container.DataItem, "ThumbnailSmallPath").ToString (), "sm") %>' border="0"/>
							</a>
						</div>	
					</div>
				</div>
				<div class="private" runat="server" id="communityAccess"></div>
			</div>
		    <div class="data">
		         <div class="name"><a href='<%# GetBroadcastChannelUrl (DataBinder.Eval (Container.DataItem, "NameNoSpaces").ToString ())%>' id="aNamePlay" runat="server"><%# TruncateWithEllipsis(DataBinder.Eval(Container.DataItem, "Name").ToString(), 40)%></a></div>
		         <div class="owner"><span>Owner:</span> <a class="dateStamp" href='<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "CreatorUsername").ToString ())%>'><%# DataBinder.Eval(Container.DataItem, "CreatorUsername") %></a></div>
		         <div class="counts"><span>Raves: <%# DataBinder.Eval(Container.DataItem, "Stats.NumberOfDiggs")%></span></div>
		         <p class="desc"><%# TruncateWithEllipsis (DataBinder.Eval (Container.DataItem, "Description").ToString (), 80) %></p>
		    </div>
		</div>
	</itemtemplate>
	<separatortemplate><hr class="dataSeparator" /></separatortemplate>
</asp:datalist>	 
			   
<asp:repeater id="rptCommunities" runat="server" >
	<itemtemplate>
		<div class="dataRowSideLight">
		    <div class="image">
				<div class="framesize-small">
					<div id="Div2" class="restricted" runat="server" visible='<%# (DataBinder.Eval(Container.DataItem, "IsAdult").Equals ("Y")) %>'></div>
					<div class="frame">
						<div class="imgconstrain">
							<a href='<%# GetBroadcastChannelUrl (DataBinder.Eval (Container.DataItem, "NameNoSpaces").ToString ())%>' id="aImgPlay" runat="server">
								<img id="Img2" runat="server" src='<%# GetBroadcastChannelImageURL (DataBinder.Eval(Container.DataItem, "ThumbnailSmallPath").ToString (), "sm") %>' border="0"/>
							</a>
						</div>	
					</div>
				</div>
			</div>
		    <div class="data">
		         <div class="name"><a href='<%# GetBroadcastChannelUrl (DataBinder.Eval (Container.DataItem, "NameNoSpaces").ToString ())%>' id="aNamePlay" runat="server"><%# TruncateWithEllipsis(DataBinder.Eval(Container.DataItem, "Name").ToString(), 40)%></a></div>
		         <div class="owner"><span>owner:</span> <a class="dateStamp" href='<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "CreatorUsername").ToString ())%>'><%# DataBinder.Eval(Container.DataItem, "CreatorUsername") %></a></div>
		         <div class="counts"><span>Raves: <%# DataBinder.Eval(Container.DataItem, "Stats.NumberOfDiggs")%></span></div>
		         <div class="desc"><%# TruncateWithEllipsis (DataBinder.Eval (Container.DataItem, "Description").ToString (), 70) %></div>
		    </div>
		</div>
	</itemtemplate>	
	<separatortemplate><hr class="dataSeparator" /></separatortemplate>
</asp:repeater>
										   
<div class="noData" id="divNoData" runat="server" visible="false">No Communities found.</div>

</div>
