///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.mykaneva.widgets
{
    public partial class FacebookActions : BasePage
    {
        #region Declerations

        private ulong _fbUserId = 0;
        private string _accessToken = "";
        private ActionToPerform _currentAction;
        private FacebookUser _fbUser = new FacebookUser();

        #endregion Declerations

        protected void Page_Load (object sender, EventArgs e)
        {
            //clears local cache so data is always new
            Response.ContentType = "text/plain";
            Response.CacheControl = "no-cache";
            Response.AddHeader ("Pragma", "no-cache");
            Response.Expires = -1;

            GetRequestValues ();

            PerformAction ();
        }

        private void GetRequestValues ()
        {
            if (Request["accessToken"] != null)
            {
                try
                {
                    AccessToken = Request["accessToken"].ToString();

                    bool isError = false;
                    string errMsg = "";
                    FBUser = GetSocialFacade.GetCurrentUser (AccessToken, out isError, out errMsg);
                }
                catch { }
            }

            CurrentAction = ActionToPerform.ConnectToFacebook;
            if (Request["ca"] != null)
            {
                try
                {
                    CurrentAction = (ActionToPerform) (Convert.ToInt32 (Request["ca"]));
                }
                catch
                {
                    CurrentAction = ActionToPerform.ConnectToFacebook;
                }
            }
        }

        private void PerformAction ()
        {
            if (CurrentAction == ActionToPerform.ConnectToFacebook)
            {
                ConnectToFacebook ();
            }
            else if (CurrentAction == ActionToPerform.DisconnectFromFacebook)
            {
                DisconnectFromFacebook ();
            }
        }

        private void ConnectToFacebook ()
        {
            try
            {
                // If user is already connected, then we just want to update the access token 
                // they are only trying to login.
                if (KanevaWebGlobals.CurrentUser.FacebookSettings.FacebookUserId > 0)
                {
                    GetUserFacade.SetFacebookAccessToken (FBUser.Id, AccessToken);
                }
                else
                {
                    if (FBUser.Id > 0)
                    {
                        int ret = GetUserFacade.ConnectUserToFacebook (KanevaWebGlobals.CurrentUser.UserId, FBUser.Id, AccessToken);
                    }
                    else
                    {
                        // We do not have a valid Facebook user object
                        Response.Redirect (Request.UrlReferrer.AbsoluteUri);
                    }
                }
                // Clear facebook settings cache
                GetUserFacade.InvalidateKanevaUserFacebookCache (KanevaWebGlobals.CurrentUser.UserId);
            }
            catch { }

            Response.Redirect ("~/mykaneva/invitefriend.aspx?service=facebook");
        }

        private void DisconnectFromFacebook ()
        { }

        #region Properties

        private enum ActionToPerform
        {
            ConnectToFacebook = 0,
            DisconnectFromFacebook = 1
        }

        private ActionToPerform CurrentAction
        {
            set { this._currentAction = value; }
            get { return this._currentAction; }
        }
        
        private ulong FacebookUserId
        {
            set { this._fbUserId = value; }
            get { return this._fbUserId; }
        }

        private string AccessToken
        {
            set { this._accessToken = value; }
            get { return this._accessToken; }
        }

        private FacebookUser FBUser
        {
            set { this._fbUser = value; }
            get { return this._fbUser; }
        }

        #endregion Properties
    }
}