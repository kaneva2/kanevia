<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Games.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.mykaneva.widgets.Games" %>

<script type="text/javascript"><!--
document.observe("dom:loaded", InitAjax);
var orderBy = '';
function InitAjax () { AjaxReqInit (GetAjaxParams(),'<%= ResolveUrl("~/mykaneva/widgets/MediaData.aspx") %>'); }
function SetDisplayOrder (obj) { orderBy=obj.id; InitAjax(); }
function GetAjaxParams () {	return 'communityId=<%= CurrentCommunity.CommunityId %>&orderby='+orderBy+'&assetTypeId=1'; }
--> 
</script> 

<div>
	<div><a id="aUpload" runat="server">Upload Media</a>   <a id="aMediaLibrary" runat="server">Media Library</a></div>
	<div>Sort by: <a href="javascript:void(0);" onclick="SetDisplayOrder(this);" id="date">Newest</a> |  <a href="javascript:void(0);" onclick="SetDisplayOrder(this);" id="raves">Most Popular</a></div>
	
	<div id="divAjaxData"><img id="Img1" class="imgLoading" runat="server" src="~/images/ajax-loader.gif" height="24" width="24" alt="Loading..." border="0"/><div class="loadingText">Loading...</div></div> 
 
	<div class="moduleMedia">
		<div id="divAjaxPager"><!-- pager --></div>  
	</div>
</div>