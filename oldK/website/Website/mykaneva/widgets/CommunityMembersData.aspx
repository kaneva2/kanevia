<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CommunityMembersData.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.mykaneva.widgets.CommunityMembersData" %>

<asp:Literal id="litJS" runat="server"></asp:Literal>

<div id="communityMembers">

<div class="edit" id="membersEdit" runat="server">
    <a class="btnlarge grey" id="editMembers" runat="server">Manage Members</a>
</div> 

<asp:datalist visible="true" runat="server" showfooter="False" width="100%" id="dlMembers" onitemdatabound="dlMembers_ItemDataBound"
	cellpadding="0" cellspacing="0" border="0" repeatcolumns="3" horizontalalign="Center" repeatdirection="Horizontal" 
	 bordercolor="Red" repeatlayout="table" >	
    <headertemplate><hr class="dataSeparator top" /></headertemplate>
	<itemtemplate>
		<div class="dataRow">
			<div style="height:70px;width:52px;float:left;">
				<div class="framesize-square">
					<div id="Div2" class="restricted" runat="server" visible='<%# Convert.ToBoolean (DataBinder.Eval(Container.DataItem, "IsMatureProfile")) %>'></div>
					<div class="frame">
						<div class="imgconstrain">
							<a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "UserName").ToString ())%>>
								<img id="Img2" runat="server" src='<%# GetProfileImageURL (DataBinder.Eval(Container.DataItem, "ThumbnailSquarePath").ToString (), "sq", DataBinder.Eval(Container.DataItem, "Gender").ToString (), Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "FacebookSettings.UseFacebookProfilePicture")), Convert.ToUInt64(DataBinder.Eval(Container.DataItem, "FacebookSettings.FacebookUserId")) )%>' border="0"/>
							</a>
						</div>	
					</div>
				</div>
				<div class="status" id="divOnlineStatus" runat="server"></div>
			</div>
			<div class="data">		    
		        <p class="userName"><a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "UserName").ToString ())%>><%# TruncateWithEllipsis (DataBinder.Eval (Container.DataItem, "UserName").ToString (), 11)%></a></p>
				<p class="displayName"><%# TruncateWithEllipsis (DataBinder.Eval(Container.DataItem, "DisplayName").ToString (), 16) %></p>
		        <p class="role" id="divRole" runat="server"></p>
		        <p class="date" id="divJoinDate" runat="server"></p>
		    </div>
		</div>
	</itemtemplate>	

</asp:datalist>	 
			   
<asp:repeater id="rptMembers" runat="server" onitemdatabound="rptMembers_ItemDataBound" >
	<itemtemplate>
		<div class="dataRowSideLight">
			<div class="framesize-square">
				<div id="Div1" class="restricted" runat="server" visible='<%# Convert.ToBoolean (DataBinder.Eval(Container.DataItem, "IsMatureProfile")) %>'></div>
				<div class="frame">
					<div class="imgconstrain">
						<a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "UserName").ToString ())%>>
							<img id="Img1" runat="server" src='<%# GetProfileImageURL (DataBinder.Eval(Container.DataItem, "ThumbnailSquarePath").ToString (), "sq", DataBinder.Eval(Container.DataItem, "Gender").ToString (), Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "FacebookSettings.UseFacebookProfilePicture")), Convert.ToUInt64(DataBinder.Eval(Container.DataItem, "FacebookSettings.FacebookUserId")) )%>' border="0"/>
						</a>
					</div>	
				</div>
			</div>
		    <div class="data">		    
		        <a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "UserName").ToString ())%>><%# TruncateWithEllipsis (DataBinder.Eval (Container.DataItem, "UserName").ToString (), 11)%></a><br />
				<div><%# DataBinder.Eval(Container.DataItem, "DisplayName").ToString () %></div>
		        <div class="role" id="divRole2" runat="server"></div>
		        <div class="status" id="divOnlineStatus2" runat="server"></div>
		    </div>
		</div>
	</itemtemplate>	
</asp:repeater>
										   
<div id="divNoData" runat="server" visible="false"></div>

</div>