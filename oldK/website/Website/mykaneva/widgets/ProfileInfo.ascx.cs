///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.UI;
//using KlausEnt.KEP.Kaneva.framework.widgets;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using log4net;

namespace KlausEnt.KEP.Kaneva.mykaneva.widgets
{
    public partial class ProfileInfo : ModuleViewBaseControl
    {

        #region Declerations

        private int profileOwnerId = 0;
        private int userId = 0;
        private bool isProfileOwner = false;
        private bool isProfileAdmin = false;

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #endregion Declerations

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            // Put user code to initialize the page here
//            if (Request.IsAuthenticated)
//            {
                try
                {
                    //get require parameters
                    GetRequestValues();

                    //get viewing user's access rights
                    CheckUserAccess();

                    //bind the data
                    if (!IsPostBack)
                    {
                        //bind the interest data
                        BindInterestData();

                        //bind the personal info data
                        BindPersonalData();

                        //add the client scripts
                        AddInterestClientScript();
                        AddSchoolClientScript();
                    }
                    SetProfileEditOptions();
                }
                catch (Exception ex)
                {
                    m_logger.Error("Error loading Profile Data widget. ", ex);
                    Response.Write("<div class=\"error\">We're sorry but the data you requested could not be retrieved.</div>");
                }
//            }
        }
        #endregion

        #region Attributes
        public Community CurrentCommunity
        {
            get
            {
                try
                {
                    return (Community)(HttpContext.Current.Items["community"]);
                }
                catch
                {
                    return new Community(); ;
                }
            }
        }
        #endregion

        #region Main Functions

        /// <summary>
        /// Bind the Data
        /// </summary>
        private void BindInterestData()
        {
            // if the user is logged in, then get the common interests
            DataTable dtCommon = null;
            if (Request.IsAuthenticated && !isProfileOwner)
            {
                dtCommon = GetInterestsFacade.GetCommonInterests(profileOwnerId, userId);
            }

            //get the user's interest
            DataTable dtInterests = GetInterestsFacade.GetUserInterests(profileOwnerId);

            //get the interest for their count
            int availableCategoriesCount = GetInterestsFacade.GetInterestCategories().Count;
            int categoryCount = 1;

            if (dtInterests != null && dtInterests.Rows.Count > 0)
            {
                string category = string.Empty;
                bool match = false;
                int j = 0;

                //create outter table to make two column format as requested by business
                HtmlTable wrapperTable = new HtmlTable();
                wrapperTable.Border = 0;
                wrapperTable.CellPadding = 5;
                wrapperTable.CellSpacing = 0;
                wrapperTable.Width = "100%";
                wrapperTable.ID = "wrapperTable";

                HtmlTableRow wrapperRow1 = new HtmlTableRow();
                HtmlTableRow wrapperRow2 = new HtmlTableRow();

                HtmlTableCell cellSpan = new HtmlTableCell();
                cellSpan.ColSpan = 2;
                cellSpan.Align = "left";
                cellSpan.Width = "100%";
                cellSpan.ID = "aboutMeInfo";
                cellSpan.InnerHtml = string.Empty;

                HtmlTableCell cellLeft = new HtmlTableCell();
                cellLeft.Align = "left";
                cellLeft.Width = "50%";
                cellLeft.ID = "left_values";
                cellLeft.VAlign = "top";
                cellLeft.InnerHtml = string.Empty;

                HtmlTableCell cellRight = new HtmlTableCell();
                cellRight.Align = "right";
                cellRight.Width = "50%";
                cellRight.ID = "right_values";
                cellRight.VAlign = "top";
                cellRight.InnerHtml = string.Empty;

                for (int i = 0; i < dtInterests.Rows.Count; i++)
                {
                    if (category != dtInterests.Rows[i]["category_text"].ToString())
                    {
                        
                        category = dtInterests.Rows[i]["category_text"].ToString();

                        HtmlTable table = new HtmlTable();
                        table.Border = 0;
                        table.CellPadding = 5;
                        table.CellSpacing = 0;
                        table.Width = "99%";

                        HtmlTableRow row = new HtmlTableRow();
                        HtmlTableCell cellInt = new HtmlTableCell();

                        cellInt.Align = "left";
                        cellInt.Width = "100%";
                        cellInt.ID = "td_" + dtInterests.Rows[i]["ic_id"].ToString();
                        cellInt.InnerHtml = string.Empty;

                        match = false;

                        cellInt.InnerHtml = "<div class=\"categoryTitle\">" + dtInterests.Rows[i]["category_text"].ToString() + "</div>";

                        // check to see if current item is also an interest of the
                        // user that is viewing the profile
                        if (dtCommon != null && dtCommon.Rows.Count > 0 && j < dtCommon.Rows.Count &&
                            Convert.ToInt32(dtCommon.Rows[j]["interest_id"]) == Convert.ToInt32(dtInterests.Rows[i]["interest_id"]))
                        {
                            match = true;
                            j++;
                        }

                        // create interest item contain span.  This span is used by javascript to 
                        // add the new interst items to using it's innerHTML propery
                        cellInt.InnerHtml += "<div class=\"interestData\"><span id=\"spnIC_" + dtInterests.Rows[i]["ic_id"].ToString () + "\">";

                        if (dtInterests.Rows[i]["interest"] != null && dtInterests.Rows[i]["interest"].ToString().Length > 0)
                        {
                            cellInt.InnerHtml += "<span id=\"spnInt_" + dtInterests.Rows[i]["interest_id"].ToString () + "\" nowrap>";

                            if (!match && !isProfileOwner && Request.IsAuthenticated)
                            {
                                cellInt.InnerHtml += "<a class=\"add\" href=\"javascript:void(0);\" title=\"Add " +
                                    Server.HtmlEncode (dtInterests.Rows[i]["interest"].ToString ()) + " to my " +
                                    dtInterests.Rows[i]["category_text"].ToString () + " interests" +
                                    "\" ><img border=\"0\" onclick=\"addInt" + this.ClientID.ToString () + "(this," +
                                    dtInterests.Rows[i]["interest_id"].ToString () + "," +
                                    dtInterests.Rows[i]["ic_id"].ToString () + ");\" src=\"" +
                                    ResolveUrl ("~/images/plus_sm.gif") + "\" /></a>";
                            }

                            cellInt.InnerHtml += "<a id=\"aInt_" + dtInterests.Rows[i]["interest_id"].ToString() +
                                "\" href=\"" + ResolveUrl("~/people/people.kaneva?" + Constants.QUERY_STRING_INTEREST +
                                "=" + Server.UrlEncode(dtInterests.Rows[i]["interest"].ToString())) + "\"><span " +
                                " id=\"spnTxt_" + dtInterests.Rows[i]["interest_id"].ToString() + "\" ";

                            if (match)
                            {
                       //         cellInt.InnerHtml += " style=\"background-color:yellow;\"";
                                cellInt.InnerHtml += " class=\"hilite\"";
                            }

                            cellInt.InnerHtml += ">" + Server.HtmlEncode(dtInterests.Rows[i]["interest"].ToString()) + "</span></a>";

                            if (isProfileAdmin)
                            {
                                cellInt.InnerHtml += " <a title=\"Delete " + Server.HtmlEncode(dtInterests.Rows[i]["interest"].ToString()) +
                                    "\" href=\"javascript:if (confirm('Delete " + Server.HtmlEncode(dtInterests.Rows[i]["interest"].ToString()) +
                                    "')) admDel" + this.ClientID.ToString() + "(this," +
                                    dtInterests.Rows[i]["interest_id"].ToString() + ");\">(*)</a>";
                            }

                            cellInt.InnerHtml += ", </span>";
                        }

                        // get all intersts for a particular category
                        bool sameCategory = true;
                        int k = i + 1;
                        while ((k < dtInterests.Rows.Count) && sameCategory)
                        {
                            match = false;

                            if (category == dtInterests.Rows[k]["category_text"].ToString())
                            {
                                if (dtCommon != null && dtCommon.Rows.Count > 0 && j < dtCommon.Rows.Count &&
                                    Convert.ToInt32(dtCommon.Rows[j]["interest_id"]) == Convert.ToInt32(dtInterests.Rows[k]["interest_id"]))
                                {
                                    match = true;
                                    j++;
                                }

                                cellInt.InnerHtml += "<span id=\"spnInt_" + dtInterests.Rows[k]["interest_id"].ToString () + "\" nowrap>";

                                if (!match && !isProfileOwner && Request.IsAuthenticated)
                                {
                                    cellInt.InnerHtml += "<a class=\"add\" href=\"javascript:void(0);\" title=\"Add " +
                                        Server.HtmlEncode (dtInterests.Rows[k]["interest"].ToString ()) + " to my " +
                                        dtInterests.Rows[k]["category_text"].ToString () + " interests" +
                                        "\" ><img border=\"0\" onclick=\"addInt" + this.ClientID.ToString () + "(this," +
                                        dtInterests.Rows[k]["interest_id"].ToString () + "," +
                                        dtInterests.Rows[k]["ic_id"].ToString () + ");\" src=\"" +
                                        ResolveUrl ("~/images/plus_sm.gif") + "\" /></a>";
                                }

                                cellInt.InnerHtml += "<a id=\"aInt_" + dtInterests.Rows[k]["interest_id"].ToString() +
                                "\" href=\"" + ResolveUrl("~/people/people.kaneva?" + Constants.QUERY_STRING_INTEREST +
                                "=" + Server.UrlEncode(dtInterests.Rows[k]["interest"].ToString())) + "\"><span " +
                                " id=\"spnTxt_" + dtInterests.Rows[k]["interest_id"].ToString() + "\" ";

                                if (match)
                                {
                        //            cellInt.InnerHtml += " style=\"background-color:yellow;\"";
                                    cellInt.InnerHtml += " class=\"hilite\"";
                                }

                                cellInt.InnerHtml += ">" + Server.HtmlEncode(dtInterests.Rows[k]["interest"].ToString()) + "</span></a>";

                                if (isProfileAdmin)
                                {
                                    cellInt.InnerHtml += " <a title=\"Delete " + Server.HtmlEncode(dtInterests.Rows[k]["interest"].ToString()) +
                                        "\" href=\"javascript:if (confirm('Delete " + Server.HtmlEncode(dtInterests.Rows[k]["interest"].ToString()) +
                                        "')) admDel" + this.ClientID.ToString() + "(this," +
                                        dtInterests.Rows[k]["interest_id"].ToString() + ");\">(*)</a>";
                                }

                                cellInt.InnerHtml += ", </span>";

                                k++;
                            }
                            else
                            {
                                sameCategory = false;
                                k--;
                            }
                        }

                        //update i to the new count
                        i = k;

                        // close out interest item contain span and remove the comma
                        // after the last item in the list
                        cellInt.InnerHtml = cellInt.InnerHtml.Remove(cellInt.InnerHtml.LastIndexOf(", </span>"), 2) + "</span></div>";

                        // Add cells to the row control
                        row.Cells.Add(cellInt);

                        // Add the row to the table
                        table.Rows.Add(row);

                        // Add new table to the container table cell
                        //minus one to account of the about me category
                        if (categoryCount <= ((availableCategoriesCount - 1)/2))
                        {
                            cellLeft.Controls.Add(table);
                            categoryCount++;
                        }
                        else
                        {
                            cellRight.Controls.Add(table);
                        }

                        //tdInterests.Controls.Add(table);
                    }
                }

                //add the first row of the wrapper table
                wrapperRow1.Cells.Add(cellLeft);
                wrapperRow1.Cells.Add(cellRight);
                wrapperTable.Rows.Add(wrapperRow1);

                // now lets add the the About Me info
                UserFacade userFacade = new UserFacade();
                User user = userFacade.GetUser(profileOwnerId);

                if (user.Description.Length > 0)
                {
                    HtmlTable table = new HtmlTable();
                    table.Border = 0;
                    table.CellPadding = 5;
                    table.CellSpacing = 0;
                    table.Width = "99%";

                    HtmlTableRow row = new HtmlTableRow();
                    HtmlTableCell cellInt = new HtmlTableCell();

                    cellInt.Align = "left";
                    cellInt.Width = "100%";

                    cellInt.InnerHtml = "<div class=\"categoryTitle\">About Me</div>";
                    cellInt.InnerHtml += "<div class=\"categoryData\">" + user.Description + "</div>";

                    row.Cells.Add(cellInt);
                    table.Rows.Add(row);
                    //tdInterests.Controls.AddAt(0, table);
                    cellSpan.Controls.Add(table);
                }

                //add the about me info to wrapper table
                wrapperRow2.Cells.Add(cellSpan);
                wrapperTable.Rows.Insert(0, wrapperRow2);

                //add everything to the page
                tdInterests.Controls.Add(wrapperTable);
            }
            else
            {
                if (this.isProfileOwner)
                {
                    divInterestsHeader.Visible = true;
                    interestCallout.Visible = true;
                }
                else
                {
                    interestCallout.Visible = true;
                    divInterestsHeader.Visible = false;
                    interestCallout.InnerText = "No Interest Found for this Member.";
                }
            }
        }

        //---- begin Personal Info Functions ------------------

        /// <summary>
        /// Bind the data
        /// </summary>
        private void BindPersonalData()
        {
            // if the user is logged in, then get the common interests
            DataTable dtCommon = null;
            if (Request.IsAuthenticated && !isProfileOwner)
            {
                dtCommon = GetInterestsFacade.GetCommonInterests(profileOwnerId, userId);
            }

            int hereForCount = GetIAmHereFor();

            int schoolCount = GetSchools();

            int personlCount = GetPersonalInfo();

            if (isProfileOwner && ((hereForCount == 0) || (schoolCount == 0) || (personlCount == 0)))
            {
                personalCallout.Visible = true;
            }
            else if (!isProfileOwner && (hereForCount == 0) && (schoolCount == 0) && (personlCount == 0))
            {
                personalCallout.Visible = true;
                personalCallout.InnerText = "No Personal Info Found for this Member.";
            }
        }


        /// <summary>
        /// Get the 'I am Here For' data
        /// </summary>
        private int GetIAmHereFor()
        {
            // if the user is logged in, then get the common interests
            DataTable dtCommon = null;
            if (Request.IsAuthenticated && !isProfileOwner)
            {
                dtCommon = GetInterestsFacade.GetCommonInterestsByCategory(profileOwnerId, userId, 21);
            }

            DataTable dtInterests = GetInterestsFacade.GetUserInterestsByCategory(profileOwnerId, 21);

            if (dtInterests != null && dtInterests.Rows.Count > 0)
            {
                string category = string.Empty;
                bool match = false;
                int j = 0;

                for (int i = 0; i < dtInterests.Rows.Count; i++)
                {
                    if (category != dtInterests.Rows[i]["category_text"].ToString())
                    {
                        category = dtInterests.Rows[i]["category_text"].ToString();

                        HtmlTable table = new HtmlTable();
                        table.Border = 0;
                        table.CellPadding = 0;
                        table.CellSpacing = 0;
                        table.Width = "99%";

                        HtmlTableRow row = new HtmlTableRow();
                        HtmlTableCell cellInt = new HtmlTableCell();

                        cellInt.Align = "left";
                        cellInt.Width = "100%";
                        cellInt.Style["padding"] = "5px 0 5px 0";
                        cellInt.ID = "td_" + dtInterests.Rows[i]["ic_id"].ToString();
                        cellInt.InnerHtml = string.Empty;

                        match = false;

                        cellInt.InnerHtml = "<div class=\"category\"><span class=\"categoryTitle\">" + dtInterests.Rows[i]["category_text"].ToString() + "</span>";

                        // check to see if current item is also an interest of the
                        // user that is viewing the profile
                        if (dtCommon != null && dtCommon.Rows.Count > 0 && j < dtCommon.Rows.Count &&
                            Convert.ToInt32(dtCommon.Rows[j]["interest_id"]) == Convert.ToInt32(dtInterests.Rows[i]["interest_id"]))
                        {
                            match = true;
                            j++;
                        }

                        // create interest item contain span.  This span is used by javascript to 
                        // add the new interst items to using it's innerHTML propery
                        cellInt.InnerHtml += "<span id=\"spnIC_" + dtInterests.Rows[i]["ic_id"].ToString () + "\">";

                        if (dtInterests.Rows[i]["interest"] != null && dtInterests.Rows[i]["interest"].ToString().Length > 0)
                        {
                            cellInt.InnerHtml += "<span class=\"categoryData\" id=\"spnInt_" + dtInterests.Rows[i]["interest_id"].ToString () + "\" nowrap>";

                            if (!match && !this.isProfileOwner && Request.IsAuthenticated)
                            {
                                cellInt.InnerHtml += "<a class=\"add\" href=\"javascript:void(0);\" title=\"Add " +
                                    Server.HtmlEncode (dtInterests.Rows[i]["interest"].ToString ()) + " to my " +
                                    dtInterests.Rows[i]["category_text"].ToString () + " interests" +
                                    "\" ><img border=\"0\" onclick=\"addInt" + this.ClientID.ToString () + "(this," +
                                    dtInterests.Rows[i]["interest_id"].ToString () + "," +
                                    dtInterests.Rows[i]["ic_id"].ToString () + ",'" +
                                    this.ClientID.ToString () + "');\" src=\"" +
                                    ResolveUrl ("~/images/plus_sm.gif") + "\" /></a>";
                            }

                            cellInt.InnerHtml += "<a id=\"aInt_" + dtInterests.Rows[i]["interest_id"].ToString () +
                                "\" href=\"" + ResolveUrl("~/people/people.kaneva?" + Constants.QUERY_STRING_INTEREST +
                                "=" + Server.UrlEncode(dtInterests.Rows[i]["interest"].ToString())) + "\"><span " +
                                " id=\"spnTxt_" + dtInterests.Rows[i]["interest_id"].ToString() + "\" ";

                            if (match)
                            {
                       //         cellInt.InnerHtml += " style=\"background-color:yellow;\"";
                                cellInt.InnerHtml += " class=\"hilite\"";
                            }

                            cellInt.InnerHtml += ">" + Server.HtmlEncode(dtInterests.Rows[i]["interest"].ToString()) + "</span></a>";

                            if (this.isProfileAdmin)
                            {
                                cellInt.InnerHtml += " <a title=\"Delete " + Server.HtmlEncode(dtInterests.Rows[i]["interest"].ToString()) +
                                    "\" href=\"javascript:if (confirm('Delete " + Server.HtmlEncode(dtInterests.Rows[i]["interest"].ToString()) +
                                    "')) admDel" + this.ClientID.ToString() + "(this," +
                                    dtInterests.Rows[i]["interest_id"].ToString() + ");\">(*)</a>";
                            }

                            cellInt.InnerHtml += ", </span>";
                        }

                        // get all intersts for a particular category
                        while (++i < dtInterests.Rows.Count)
                        {
                            match = false;

                            if (category == dtInterests.Rows[i]["category_text"].ToString())
                            {
                                if (dtCommon != null && dtCommon.Rows.Count > 0 && j < dtCommon.Rows.Count &&
                                    Convert.ToInt32(dtCommon.Rows[j]["interest_id"]) == Convert.ToInt32(dtInterests.Rows[i]["interest_id"]))
                                {
                                    match = true;
                                    j++;
                                }

                                cellInt.InnerHtml += "<span class=\"categoryData\" id=\"spnInt_" + dtInterests.Rows[i]["interest_id"].ToString () + "\" nowrap>";

                                if (!match && !this.isProfileOwner && Request.IsAuthenticated)
                                {
                                    cellInt.InnerHtml += "<a class=\"add\" href=\"javascript:void(0);\" title=\"Add " +
                                        Server.HtmlEncode (dtInterests.Rows[i]["interest"].ToString ()) + " to my " +
                                        dtInterests.Rows[i]["category_text"].ToString () + " interests" +
                                        "\" ><img border=\"0\" onclick=\"addInt" + this.ClientID.ToString () + "(this," +
                                        dtInterests.Rows[i]["interest_id"].ToString () + "," +
                                        dtInterests.Rows[i]["ic_id"].ToString () + ",'" +
                                        this.ClientID.ToString () + "');\" src=\"" +
                                        ResolveUrl ("~/images/plus_sm.gif") + "\" /></a>";
                                }

                                cellInt.InnerHtml += "<a id=\"aInt_" + dtInterests.Rows[i]["interest_id"].ToString () +
                                "\" href=\"" + ResolveUrl("~/people/people.kaneva?" + Constants.QUERY_STRING_INTEREST +
                                "=" + Server.UrlEncode(dtInterests.Rows[i]["interest"].ToString())) + "\"><span " +
                                " id=\"spnTxt_" + dtInterests.Rows[i]["interest_id"].ToString() + "\" ";

                                if (match)
                                {
                          //          cellInt.InnerHtml += " style=\"background-color:yellow;\"";
                                    cellInt.InnerHtml += " class=\"hilite\"";
                                }

                                cellInt.InnerHtml += ">" + Server.HtmlEncode(dtInterests.Rows[i]["interest"].ToString()) + "</span></a>";

                                if (this.isProfileAdmin)
                                {
                                    cellInt.InnerHtml += " <a title=\"Delete " + Server.HtmlEncode(dtInterests.Rows[i]["interest"].ToString()) +
                                        "\" href=\"javascript:if (confirm('Delete " + Server.HtmlEncode(dtInterests.Rows[i]["interest"].ToString()) +
                                        "')) admDel" + this.ClientID.ToString() + "(this," +
                                        dtInterests.Rows[i]["interest_id"].ToString() + ");\">(*)</a>";
                                }

                                cellInt.InnerHtml += ", </span>";
                            }
                            else
                            {
                                i--;
                                break;
                            }
                        }

                        if (i == dtInterests.Rows.Count) i--;

                        // close out interest item contain span and remove the comma
                        // after the last item in the list
                        cellInt.InnerHtml = cellInt.InnerHtml.Remove(cellInt.InnerHtml.LastIndexOf(", </span>"), 2) + "</span></div>";

                        // Add cells to the row control
                        row.Cells.Add(cellInt);

                        // Add the row to the table
                        table.Rows.Add(row);

                        // Add new table to the container table cell
                        tdHereFor.Controls.Add(table);
                    }
                }
            }

            return ((dtInterests == null) ? 0 : dtInterests.Rows.Count);
        }

        /// <summary>
        /// Get the schools 
        /// </summary>
        private int GetSchools()
        {
            // if the user is logged in, then get the common interests
            DataTable dtCommon = null;
            if (Request.IsAuthenticated && !isProfileOwner)
            {
                dtCommon = GetInterestsFacade.GetCommonSchools(profileOwnerId, userId);
            }

            DataTable dtSchools = GetInterestsFacade.GetUserSchools(profileOwnerId);

            if (dtSchools != null && dtSchools.Rows.Count > 0)
            {
                string category = string.Empty;
                bool match = false;
                int j = 0;

                HtmlTable table = new HtmlTable();
                table.Border = 0;
                table.CellPadding = 0;
                table.CellSpacing = 0;
                table.Width = "99%";

                HtmlTableRow row = new HtmlTableRow();
                HtmlTableCell cellInt = new HtmlTableCell();

                cellInt.Align = "left";
                cellInt.Width = "100%";
                cellInt.Style["padding"] = "5px 0 5px 0";
                cellInt.InnerHtml = "<span class=\"categoryTitle\">Schools:</span> ";

                for (int i = 0; i < dtSchools.Rows.Count; i++)
                {
                    match = false;

                    // check to see if current item is also an interest of the
                    // user that is viewing the profile
                    if (dtCommon != null && dtCommon.Rows.Count > 0 && j < dtCommon.Rows.Count &&
                        Convert.ToInt32(dtCommon.Rows[j]["school_id"]) == Convert.ToInt32(dtSchools.Rows[i]["school_id"]))
                    {
                        match = true;
                        j++;
                    }

                    cellInt.InnerHtml += "<span class=\"categoryData\" id=\"spnInt_" + dtSchools.Rows[i]["school_id"].ToString () +
                        "\" nowrap>";

                    if (!match && !isProfileOwner && Request.IsAuthenticated)
                    {
                        cellInt.InnerHtml += "<a class=\"add\" href=\"javascript:void(0);\" title=\"Add " +
                            Server.HtmlEncode (dtSchools.Rows[i]["name"].ToString ()) + " to my schools" +
                            "\" ><img border=\"0\" onclick=\"addSch" + this.ClientID.ToString () + "(this," +
                            dtSchools.Rows[i]["school_id"].ToString () + ");\" src=\"" +
                            ResolveUrl ("~/images/plus_sm.gif") + "\" /></a>";
                    }
                    
                    cellInt.InnerHtml += "<a id=\"aInt_" + dtSchools.Rows[i]["school_id"].ToString() +
                        "\" href=\"" + ResolveUrl("~/people/people.kaneva?" + Constants.QUERY_STRING_SCHOOL +
                        "=" + Server.UrlEncode(dtSchools.Rows[i]["name"].ToString())) + "\"><span " +
                        " id=\"spnTxt_" + dtSchools.Rows[i]["school_id"].ToString() + "\" ";

                    if (match)
                    {
               //         cellInt.InnerHtml += " style=\"background-color:yellow;\"";
                        cellInt.InnerHtml += " class=\"hilite\"";
                    }

                    cellInt.InnerHtml += ">" + Server.HtmlEncode(dtSchools.Rows[i]["name"].ToString()) + "</span></a>";

         //           if (!match && !isProfileOwner && Request.IsAuthenticated)
         //           {
         //               cellInt.InnerHtml += " <a href=\"javascript:void(0);\" title=\"Add " +
         //                   Server.HtmlEncode(dtSchools.Rows[i]["name"].ToString()) + " to my schools" +
         //                   "\" ><img border=\"0\" onclick=\"addSch" + this.ClientID.ToString() + "(this," +
         //                   dtSchools.Rows[i]["school_id"].ToString() + ");\" src=\"" +
         //                   ResolveUrl("~/images/plus_sm.gif") + "\" /></a>";
         //           }
                    cellInt.InnerHtml += ", </span>";
                }

                // close out interest item contain span and remove the comma
                // after the last item in the list
                cellInt.InnerHtml = cellInt.InnerHtml.Remove(cellInt.InnerHtml.LastIndexOf(", </span>"), 2) + "</span>";

                // Add cells to the row control
                row.Cells.Add(cellInt);

                // Add the row to the table
                table.Rows.Add(row);

                // Add new table to the container table cell
                tdSchools.Controls.Add(table);

                trSchools.Visible = true;
            }

            return ((dtSchools == null) ? 0 : dtSchools.Rows.Count);
        }

        /// <summary>
        /// Get the presonal data 
        /// </summary>
        private int GetPersonalInfo()
        {
            int infoProvidedCount = 0;

            DataRow drUserPersonal = GetUserFacade.GetUserPersonalInfo(this.profileOwnerId);

            if (drUserPersonal != null && drUserPersonal.Table.Rows.Count > 0)
            {
                // save these so that when both are populated we will print out
                // one string that includes both values
                string height_feet = "";
                string height_inches = "";
                string perInfo = "";
                bool columnToggle = true;
                trPersonal.Visible = false;

                HtmlTable wrapperTable = new HtmlTable();
                wrapperTable.Border = 0;
                wrapperTable.CellPadding = 3;
                wrapperTable.CellSpacing = 2;
                wrapperTable.Width = "100%";
                wrapperTable.ID = "personalInfoTable";

                HtmlTableRow wrapperRow = new HtmlTableRow();

                for (int i = 0; i < drUserPersonal.Table.Columns.Count; i++)
                {
                    string column_name = drUserPersonal.Table.Columns[i].ColumnName.Trim();

                    string header = "";
                    string body = drUserPersonal.Table.Rows[0].ItemArray[i] != null ? drUserPersonal.Table.Rows[0].ItemArray[i].ToString().Trim() : "";
                    string param = string.Empty;

                    switch (column_name)
                    {
                        // General | Personal
                        case "relationship":
                            header = "Relationship:";
                            body = Server.HtmlDecode(body);
                            param = Constants.QUERY_STRING_RELATIONSHIP;
                            break;

                        case "orientation":
                            header = "Orientation:";
                            body = Server.HtmlDecode(body);
                            param = Constants.QUERY_STRING_ORIENTATION;
                            break;

                        case "children":
                            header = "Children:";
                            body = Server.HtmlDecode(body);
                            //param = Constants.QUERY_STRING_CHILDREN;
                            break;

                        case "education":
                            header = "Education:";
                            body = Server.HtmlDecode(body);
                            param = Constants.QUERY_STRING_EDUCATION;
                            break;

                        case "income":
                            {
                                header = "Income:";
                                body = Server.HtmlDecode(body);
                                //param = Constants.QUERY_STRING_INCOME;

                                switch (body)
                                {
                                    case "1":
                                        body = "< $20,000";
                                        break;

                                    case "2":
                                        body = "Between $20,000 and $30,000";
                                        break;

                                    case "3":
                                        body = "Between $30,000 and $40,000";
                                        break;

                                    case "4":
                                        body = "Between $40,000 and $50,000";
                                        break;

                                    case "5":
                                        body = "Between $60,000 and $80,000";
                                        break;

                                    case "6":
                                        body = "Between $80,000 and $100,000";
                                        break;

                                    case "7":
                                        body = "Between $100,000 and $150,000";
                                        break;

                                    case "8":
                                        body = "$150,000+";
                                        break;

                                    default:
                                        body = "";
                                        break;
                                }
                                break;
                            }

                        case "height_feet":
                            height_feet = Server.HtmlDecode(body);
                            body = "";
                            break;

                        case "height_inches":
                            height_inches = Server.HtmlDecode(body);
                            body = "";
                            break;

                        case "smoke":
                            header = "Smoking:";
                            body = Server.HtmlDecode(body);
                            param = Constants.QUERY_STRING_SMOKING;
                            break;

                        case "drink":
                            header = "Drinking:";
                            body = Server.HtmlDecode(body);
                            param = Constants.QUERY_STRING_DRINKING;
                            break;

                        case "religion":
                            header = "Religion:";
                            body = Server.HtmlDecode(body);
                            param = Constants.QUERY_STRING_RELIGION;
                            break;

                        case "ethnicity":
                            header = "Ethnicity:";
                            body = Server.HtmlDecode(body);
                            param = Constants.QUERY_STRING_ETHNICITY;
                            break;

                        case "hometown":
                            header = "Hometown:";
                            body = Server.HtmlDecode(body);
                            param = Constants.QUERY_STRING_HOMETOWN;
                            break;

                        default:
                            break;

                    }

                    if (height_feet != "" && height_inches != "")
                    {
                        header = "Height:";
                        body = height_feet + " feet-" + height_inches + " inches";
                        param = Constants.QUERY_STRING_HEIGHT;

                        height_feet = "";
                        height_inches = "";
                    }

                    if (header != "" && body != "" && body != "No Answer")
                    {
                        //for tracking if the user neeeds to be prompted to enter more data
                        infoProvidedCount++;

                        perInfo += "<span class=\"categoryTitle\">" + header + "</span> ";

                        //NOTE: for now we can not search on income and children so,
                        // we will not give these items a link to the personal page
                        if (param != string.Empty && header.ToLower() != "income" && header.ToLower() != "children")
                        {
                            perInfo += " <a href=\"" + ResolveUrl("~/people/people.kaneva?" +
                                param + "=" + Server.UrlEncode(body)) +
                                "\">" + Server.HtmlEncode(body) + "</a> ";
                        }
                        else
                        {
                            perInfo += " <span>" + Server.HtmlEncode(body) + "</span> ";
                        }

                        //add to data table (html)
                        if (perInfo.Length > 0)
                        {
                            HtmlTableCell column = new HtmlTableCell();
                            column.Align = "left";
                            column.Width = "50%";
                            column.InnerHtml = perInfo;
                            
                            //split data up to left and right columns
                            if (columnToggle)
                            {
                                //make new row
                                wrapperRow = new HtmlTableRow();

                                //add the first column
                                wrapperRow.Cells.Add(column);

                                //toggle the column
                                columnToggle = false;
                            }
                            else
                            {
                                //add the second column
                                wrapperRow.Cells.Add(column);

                                //set the class for css styling
                                wrapperRow.Attributes.Add("class", "dataLines");

                                //add the row to the table
                                wrapperTable.Rows.Add(wrapperRow);

                                //toggle the column
                                columnToggle = true;
                            }

                            //clear for next round
                            perInfo = "";
                            trPersonal.Visible = true;
                        }
                    }
                }

                if (trPersonal.Visible)
                {
                    tdPersonal.Controls.Add(wrapperTable);
                    trPersonal.Visible = true;
                }
            }

            return ((drUserPersonal == null) ? 0 : (infoProvidedCount/drUserPersonal.Table.Rows.Count));
        }
        //---- end Personal Info Functions ------------------
        #endregion

        #region Helper Methods

        private void SetProfileEditOptions()
        {
            //personal info edit set
			lbAddInfoInt.Visible = isProfileOwner;
			lbAdmDelInt.Visible = isProfileAdmin;
            lbAddSch.Visible = isProfileOwner;
            personalInfoEdit.Visible = isProfileOwner;
            
            //personal interest edit set
            lbAdmDel.Visible = isProfileAdmin;
            interestEdit.Visible = isProfileOwner;
            lbAddInt.Visible = isProfileOwner;
        }

        /// <summary>
        /// Gets any parameters the page needs from the POst/Get
        /// </summary>
        private void GetRequestValues()
        {
            // Get the user Id from the parameters 
            if (Request["userId"] != null)
            {
                try
                {
                    profileOwnerId = Convert.ToInt32(Request["userId"]);
                }
                catch { }
            }
            else
            {
                profileOwnerId = CurrentCommunity.CreatorId;
            }
        }

        /// <summary>
        /// Checks the current user's access level
        /// </summary>
        private void CheckUserAccess()
        {
            //get the users Id
            userId = GetUserId();

            //see if they are the profile/community owner
            isProfileOwner = userId.Equals(profileOwnerId);

            if (!isProfileOwner)
            {
                //check the privilege level to see if they are an admin
                this.isProfileAdmin = HasWritePrivileges((int)SitePrivilege.ePRIVILEGE.USER_PROFILE_ADMIN);
            }
        }

        /// <summary>
        /// Add the Client Script for the interests
        /// </summary>
        private void AddInterestClientScript()
        {
            string scriptString = "<script language=\"javaScript\">\n<!--\n";
            scriptString += "function addInt" + this.ClientID.ToString() + "(t, id, ic_id) {\n";
            scriptString += "   $('" + hidId.ClientID.ToString() + "').value = id;\n";
            scriptString += "   $('" + hidIcId.ClientID.ToString() + "').value = ic_id;\n";
            scriptString += "   t.style['display'] = 'none';\n";
       //     scriptString += "   $('spnTxt_'+id).style['border'] = '1px solid red';\n";
       //     scriptString += "   $('spnTxt_'+id).style['background-color'] = 'yellow';\n";
            scriptString += "   $('spnTxt_'+id).className = 'hilite';\n";
            scriptString += MagicAjax.AjaxCallHelper.GetAjaxCallEventReference(lbAddInt) + ";\n";
            scriptString += "}\n// -->\n";
            scriptString += "</script>";

            if (!Page.ClientScript.IsClientScriptBlockRegistered("addInt" + this.ClientID.ToString()))
            {
                Page.ClientScript.RegisterClientScriptBlock(GetType(), "addInt" + this.ClientID.ToString(), scriptString);
            }

            // Add script for admin delete all
            if (IsUserAdministrator())
            {
                scriptString = string.Empty;
                scriptString = "<script language=\"javaScript\">\n<!--\n";
                scriptString += "function admDel" + this.ClientID.ToString() + "(t, id) {\n";
                scriptString += "   $('" + hidId.ClientID.ToString() + "').value = id;\n";
                scriptString += "   $('spnInt_'+id).style['display'] = 'none';\n";
                scriptString += MagicAjax.AjaxCallHelper.GetAjaxCallEventReference(lbAdmDelInt) + ";\n";
                scriptString += "}\n// -->\n";
                scriptString += "</script>";

                if (!Page.ClientScript.IsClientScriptBlockRegistered("admDel" + this.ClientID.ToString()))
                {
                    Page.ClientScript.RegisterClientScriptBlock(GetType(), "admDel" + this.ClientID.ToString(), scriptString);
                }
            }
        }

        /// <summary>
        /// Add Client Script for schools
        /// </summary>
        private void AddSchoolClientScript()
        {
            string scriptString = "<script language=\"javaScript\">\n<!--\n";
            scriptString += "function addSch" + this.ClientID.ToString() + "(t, id) {\n";
            scriptString += "   $('" + hidSchId.ClientID.ToString() + "').value = id;\n";
            scriptString += "   t.style['display'] = 'none';\n";
            scriptString += "   $('spnTxt_'+id).style['border'] = '1px solid yellow';\n";
            scriptString += "   $('spnTxt_'+id).style['background-color'] = 'yellow';\n";
            scriptString += MagicAjax.AjaxCallHelper.GetAjaxCallEventReference(lbAddSch) + ";\n";
            scriptString += "}\n// -->\n";
            scriptString += "</script>";

            if (!Page.ClientScript.IsClientScriptBlockRegistered("addSch" + this.ClientID.ToString()))
            {
                Page.ClientScript.RegisterClientScriptBlock(GetType(), "addSch" + this.ClientID.ToString(), scriptString);
            }
        }

        #endregion

        #region Event Handlers

        //-------- begin personal info events ---------------

        /// <summary>
        /// Click event for linkbutton lbAdmDel
        /// </summary>
        protected void lbAdmDelInt_Click(object sender, EventArgs e)
        {
            if (this.isProfileAdmin)
            {
                try
                {
                    // get the interest id and interest category id
                    int interestId = Convert.ToInt32(hidId.Value);

                    // add interst to user
                    GetInterestsFacade.RemoveInterestFromAllUsers(interestId);
                }
                catch { }
            }
        }

        /// <summary>
        /// Click event for add school button
        /// </summary>
        protected void lbAddSch_Click(object sender, EventArgs e)
        {
            if (Request.IsAuthenticated)
            {
                try
                {
                    // get the interest id and interest category id
                    int schoolId = Convert.ToInt32(hidSchId.Value);

                    if (GetInterestsFacade.GetUserSchools(KanevaWebGlobals.CurrentUser.UserId).Rows.Count > 5)
                    {
                        MagicAjax.AjaxCallHelper.WriteAlert("You can only have 5 schools.  You must remove a school before you can add a new one.");
                    }
                    else
                    {
                        // add interst to user
                        GetInterestsFacade.AssociateSchoolToUser(userId, schoolId);
                    }
                }
                catch { }
            }
        }

        /// <summary>
        /// Click event for add interest button
        /// </summary>
        protected void lbAddInfoInt_Click(object sender, EventArgs e)
        {
            if (Request.IsAuthenticated)
            {
                try
                {
                    // get the interest id and interest category id
                    int interestId = Convert.ToInt32(hidId.Value);
                    int icId = Convert.ToInt32(hidIcId.Value);

                    // add interst to user
                    GetInterestsFacade.AssignInterestToUser(this.userId, interestId, icId);
                }
                catch { }
            }
        }

        /// <summary>
        /// Click event for edit this widget 
        /// </summary>
        protected void lbEditPersonal_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/mykaneva/personal.aspx");
        }
        
        //-------- end personal info events ---------------

        //-------- begin interest events ------------------

        /// <summary>
        /// Click event for linkbutton lbAddInt
        /// </summary>
        protected void lbAddInt_Click(object sender, EventArgs e)
        {
            if (Request.IsAuthenticated)
            {
                try
                {
                    // get the interest id and interest category id
                    int interestId = Convert.ToInt32(hidId.Value);
                    int icId = Convert.ToInt32(hidIcId.Value);

                    // add interst to user
                    GetInterestsFacade.AssignInterestToUser(this.userId, interestId, icId);
                }
                catch { }
            }
        }

        /// <summary>
        /// Click event for linkbutton lbAdmDel
        /// </summary>
        protected void lbAdmDel_Click(object sender, EventArgs e)
        {
            if (isProfileAdmin)
            {
                try
                {
                    // get the interest id and interest category id
                    int interestId = Convert.ToInt32(hidId.Value);
                    int icId = Convert.ToInt32(hidIcId.Value);

                    // add interst to user
                    GetInterestsFacade.RemoveInterestFromAllUsers(interestId);
                }
                catch { }
            }
        }

        /// <summary>
        /// Click event for linkbutton lbEditInterests
        /// </summary>
        protected void lbEditInterests_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/mykaneva/profile.aspx");
        }

        //-------- end interest events ------------------

        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new EventHandler(this.Page_Load);
        }
        #endregion

    }
}
