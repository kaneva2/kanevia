<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ProfileInfo.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.mykaneva.widgets.ProfileInfo" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>

<div id="personalInfo">

    <input type="hidden" id="hidId" value="0" runat="server" />
    <input type="hidden" id="hidIcId" value="0" runat="server" />
    <input type="hidden" id="hidSchId" value="0" runat="server" />
    
	<div id="personal">
		<div class="edit" id="personalInfoEdit" runat="server">
			<a class="btntiny grey" runat="server" onserverclick="lbEditPersonal_Click">Edit</a>
			<hr class="dataSeparator" />
		</div> 
		<div id="data">
			<div>
				<div id="trHereFor" class="dataGrouping" runat="server">
					<div id="tdHereFor" runat="server"></div>
				</div>
				<div id="trPersonal" class="dataGrouping" runat="server">
					<div id="tdPersonal" runat="server"></div>
				</div>
				<div id="trSchools" class="dataGrouping" runat="server">
					<div id="tdSchools" runat="server"></div>
				</div>
			</div>
		</div>
		<div runat="server" id="personalCallout" class="callout" visible="false"><a class="share" runat="server" href="~/mykaneva/personal.aspx">Share more about you</a></div>
		<ajax:ajaxpanel id="Ajaxpanel2" runat="server">
		    <asp:linkbutton id="lbAddInfoInt" onclick="lbAddInfoInt_Click" runat="server" />
		    <asp:linkbutton id="lbAdmDelInt" onclick="lbAdmDelInt_Click" runat="server" visible="false"/>
		    <asp:linkbutton id="lbAddSch" onclick="lbAddSch_Click" runat="server" />
		</ajax:ajaxpanel>
	</div>

	<div id="interests">
	    <div class="header" id="divInterestsHeader" runat="server">My Interests</div>
		<div class="edit" id="interestEdit" runat="server" style="text-align:right">
			<a id="A1" class="btntiny grey" runat="server" onserverclick="lbEditInterests_Click">Edit</a>
			<hr class="dataSeparator" />
		</div> 
		<div class="data">
			<div id="tdInterests" runat="server">
			    <!-- catogories go here -->
			</div>
		</div>
		<div runat="server" id="interestCallout" class="callout" visible="false"><a class="share" href="~/mykaneva/profile.aspx" runat="server">Share your interests</a></div>
		<ajax:ajaxpanel id="Ajaxpanel1" runat="server">
		    <asp:linkbutton id="lbAddInt" onclick="lbAddInt_Click" runat="server" />
		    <asp:linkbutton id="lbAdmDel" onclick="lbAdmDel_Click" runat="server" visible="false"/>
	    </ajax:ajaxpanel>
	</div> 
	
</div>