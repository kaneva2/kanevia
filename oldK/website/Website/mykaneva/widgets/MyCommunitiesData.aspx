<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="MyCommunitiesData.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.mykaneva.widgets.MyCommunitiesData" %>

<asp:Literal id="litJS" runat="server"></asp:Literal>

<div id="communities">

<div id="communitiesEdit" runat="server" class="edit">
    <a href="../bookmarks/channelTags.aspx" class="btnlarge grey" id="editCommunities" runat="server">Manage Communities</a>
</div>																										

<asp:datalist visible="true" runat="server" showfooter="False" width="99%" id="dlCommunities" 
	onitemdatabound="dlCommunities_ItemDataBound" cellpadding="0" cellspacing="0" border="0" 
	repeatcolumns="1" horizontalalign="Center" repeatdirection="Horizontal" gridlines="none" 
	repeatlayout="table" >	
	<headertemplate><hr class="dataSeparator top" /></headertemplate>
	<itemtemplate>
		<div class="dataRow">																				  
		    <div class="image">
				<div class="framesize-small">
					<div id="Div2" class="restricted" runat="server" visible='<%# (DataBinder.Eval(Container.DataItem, "IsAdult").Equals ("Y")) %>'></div>
					<div class="frame">
						<div class="imgconstrain">
							<a href='<%# GetBroadcastChannelUrl (DataBinder.Eval (Container.DataItem, "NameNoSpaces").ToString ())%>'>
								<img id="Img2" runat="server" src='<%# GetBroadcastChannelImageURL (DataBinder.Eval(Container.DataItem, "ThumbnailSmallPath").ToString (), "sm") %>' border="0"/>
							</a>
						</div>	
					</div>
				</div>
				<div class="role"><asp:label id="lblMemberType" runat="server"/></div>
			</div>
		    <div class="data">
		         <div class="name"><a href='<%# GetBroadcastChannelUrl (DataBinder.Eval (Container.DataItem, "NameNoSpaces").ToString ())%>'><%# TruncateWithEllipsis(DataBinder.Eval(Container.DataItem, "Name").ToString(), 40)%></a></div>
		         <div class="owner"><span>Owner:</span> <a class="dateStamp" href='<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "CreatorUsername").ToString ())%>'><%# DataBinder.Eval(Container.DataItem, "CreatorUsername") %></a></div>
		         <div class="counts"><span>Members: <%# DataBinder.Eval(Container.DataItem, "Stats.NumberOfMembers")%></span><span>Raves: <%# DataBinder.Eval(Container.DataItem, "Stats.NumberOfDiggs")%></span><!--<span>Media: <asp:label id="mediaCount" runat="server"/></span>--></div>
		         <div class="private"><%# (DataBinder.Eval(Container.DataItem, "IsPublic").ToString().Equals("N") ? "Private" : "") %></div>
		         <p class="desc"><%# TruncateWithEllipsis (DataBinder.Eval (Container.DataItem, "Description").ToString (), 80) %></p>
		    </div>
		</div>
	</itemtemplate>
	<separatortemplate><hr class="dataSeparator" /></separatortemplate>
</asp:datalist>	 
			   
<asp:repeater id="rptCommunities" runat="server" onitemdatabound="rptCommunities_ItemDataBound" >
	<itemtemplate>
		<div class="dataRowSideLight">
		    <div class="image">
				<div class="framesize-small">
					<div id="Div2" class="restricted" runat="server" visible='<%# (DataBinder.Eval(Container.DataItem, "IsAdult").Equals ("Y")) %>'></div>
					<div class="frame">
						<div class="imgconstrain">
							<a href='<%# GetBroadcastChannelUrl (DataBinder.Eval (Container.DataItem, "NameNoSpaces").ToString ())%>'>
								<img id="Img2" runat="server" src='<%# GetBroadcastChannelImageURL (DataBinder.Eval(Container.DataItem, "ThumbnailSmallPath").ToString (), "sm") %>' border="0"/>
							</a>
						</div>	
					</div>
				</div>
				<div><asp:label id="lblMemberType2" runat="server"/></div>
			</div>
		    <div class="data">
		         <div class="name"><a href='<%# GetBroadcastChannelUrl (DataBinder.Eval (Container.DataItem, "NameNoSpaces").ToString ())%>'><%# TruncateWithEllipsis(DataBinder.Eval(Container.DataItem, "Name").ToString(), 40)%></a></div>
		         <div class="owner"><span>owner:</span> <a class="dateStamp" href='<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "CreatorUsername").ToString ())%>'><%# DataBinder.Eval(Container.DataItem, "CreatorUsername") %></a></div>
		         <div class="counts"><span>Members: <%# DataBinder.Eval(Container.DataItem, "Stats.NumberOfMembers")%></span><span>Raves: <%# DataBinder.Eval(Container.DataItem, "Stats.NumberOfDiggs")%></span><!--<span>Media: <asp:label id="mediaCount2" runat="server"/></span>--></div>
		         <div class="desc"><%# TruncateWithEllipsis (DataBinder.Eval (Container.DataItem, "Description").ToString (), 70) %></div>
		    </div>
		</div>
	</itemtemplate>	
	<separatortemplate><hr class="dataSeparator" /></separatortemplate>
</asp:repeater>
										   
<div class="noData" id="divNoData" runat="server" visible="false">No Communities found.</div>

</div>
