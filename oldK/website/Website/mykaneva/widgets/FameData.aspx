<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="FameData.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.mykaneva.widgets.FameData" %>

<!--Fame HERE--->											
<div id="userFameSmall">

	<div class="fameStats">											    											       
		<div class="raves">Raves: <asp:label id="lblRaves" runat="server" text="0" clientidmode="static"></asp:label></div>
		<div class="counts"><div class="badges">Badges <span id="spnUserBadges" runat="server"></span> of <span id="spnTotalBadges" runat="server"></span></div>
			<div class="points"><span id="spnUserPoints" runat="server"></span> of <span id="spnTotalPoints" runat="server"></span> points</div></div>
	</div>	

	<asp:repeater id="rptFameAppsSmall" runat="server" onitemdatabound="rptFameAppsSmall_ItemDataBound">
		<itemtemplate>
			<div class="dataRowSideLight" id="Div1">
				<div class="framesize-small">
					<div class="frame">
						<div class="imgconstrain">
							<a href='<%# GetFameChannelUrl (DataBinder.Eval (Container.DataItem, "NameNoSpaces").ToString (),Convert.ToInt32(DataBinder.Eval (Container.DataItem, "CommunityId")))%>'>
								<img src='<%# GetBroadcastChannelImageURL (DataBinder.Eval(Container.DataItem, "ThumbnailSmallPath").ToString (), "sm") %>' border="0" id="Img1"/>
							</a>
						</div>	
					</div>
				</div>
				<div class="info">		    
					<div class="name"><a href='<%# GetFameChannelUrl (DataBinder.Eval (Container.DataItem, "NameNoSpaces").ToString (),Convert.ToInt32(DataBinder.Eval (Container.DataItem, "CommunityId")))%>'><%# DataBinder.Eval(Container.DataItem, "Name").ToString() %></a></div>
					<div class="level"><%# GetLevel (Convert.ToInt32(DataBinder.Eval (Container.DataItem, "CommunityId"))) %></div>
				</div>
				<div class="gameBadges">		 
					<div class="thumbs"><a href="javascript:void(0);" runat="server" id="aBadges"><%# GetMostRecentAchievementThumbnails (Convert.ToInt32(DataBinder.Eval (Container.DataItem, "CommunityId")), 1) %></a></div>
				</div>
				<div class="title"><a href="javascript:void(0);" runat="server" id="aBadges1"><%# GetAchievementCounts (Convert.ToInt32(DataBinder.Eval (Container.DataItem, "CommunityId"))) %></a></div>
			</div>
		</itemtemplate>						
		<footertemplate><div class="famespacer"></div></footertemplate>
	</asp:repeater>

	<div class="noData" id="divNoData" runat="server" visible="false"></div>

</div>
