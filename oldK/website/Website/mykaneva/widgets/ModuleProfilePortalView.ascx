<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="ModuleProfilePortalView.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.mykaneva.widgets.ModuleProfilePortalView" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<script language="javascript" src="../jscript/flyout.js"></script>
<LINK href="../css/meetme3d.css" type="text/css" rel="stylesheet">
<ajax:ajaxpanel id="Ajaxpanel1" runat="server">
	<TABLE id="widgetContent" cellSpacing="0" cellPadding="0" width="100%" border="0">
		<TR>
			<TD id="pageOpacity" width="100%">
				<DIV id="widgetBorder"><B class="outerFrame" id="outerFrame"><!-- style="display:none;"--><B class="outerFrame1"><B></B></B><B class="outerFrame2"><B></B></B><B class="outerFrame3"></B><B class="outerFrame4"></B><B class="outerFrame5"></B></B>
					<DIV class="outerFrame_content" id="outerFrame_content"><!-- style="padding:0px;background:transparent;border-width=0px;"-->  <!-- Your Content Goes Here -->
						<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
							<TR>
								<TD>
									<DIV id="widgetHeader"><B class="innerFrame"><B class="innerFrame1"><B></B></B><B class="innerFrame2"><B></B></B><B class="innerFrame3"></B><B class="innerFrame4"></B><B class="innerFrame5"></B></B>
										<DIV class="innerFrameTitle_content"><!-- Your Content Title Goes Here -->
											<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
												<TR>
													<TD class="widgetTitle">
														<asp:label id="lblTitle" runat="server"></asp:label></TD>
													<TD align="right">
														<TABLE id="tblEdit" cellSpacing="0" cellPadding="0" border="0" runat="server">
															<TR>
																<TD noWrap>
																	<asp:linkbutton id="lbEdit" title="edit this widget" runat="server" cssclass="widgetEditButton">edit</asp:linkbutton>
																	<asp:linkbutton id="lbDelete" title="remove this widget" runat="server" cssclass="widgetRemoveButton">X</asp:linkbutton></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
											</TABLE>
										</DIV>
										<B class="innerFrame"><B class="innerFrame5"></B><B class="innerFrame4"></B><B class="innerFrame3">
											</B><B class="innerFrame2"><B></B></B><B class="innerFrame1"><B></B></B></B>
									</DIV>
								</TD>
							</TR>
							<TR>
								<TD><IMG id="Img1" height="1" src="~/images/spacer.gif" width="1" border="0" runat="server"></TD>
							</TR>
							<TR>
								<TD vAlign="top" height="100%">
									<DIV id="widgetBody"><B class="innerFrame"><B class="innerFrame1"><B></B></B><B class="innerFrame2"><B></B></B><B class="innerFrame3"></B><B class="innerFrame4"></B><B class="innerFrame5"></B></B>
										<DIV class="innerFrame_content">
										<!-- Your Content Goes Here -->
											<TABLE id="controlpanel" cellSpacing="0" cellPadding="0" align="center" width="100%" border="0">
												<TR vAlign="bottom">
													<TD align="center">
														<DIV class="container" id="divContainer">
															<DIV class="float" id="controlSection1" style="PADDING-BOTTOM: 10px" runat="server" width="98%">
																<TABLE cellSpacing="0" cellPadding="0" width="280px" align="center" border="0"> 
																<!--STARTS AN ELEMENT--->
																	<TR> 
																	<!--thumb section--->
																		<TD vAlign="top" align="left" width="100">
																			<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																				<TR>
																					<TD vAlign="top">
																						<DIV class="framesize-medium">
																							<DIV class="frame noborder"><SPAN class="ct"><SPAN class="cl"></SPAN></SPAN>
																								<DIV class="imgconstrain"><A id="aAvatar" class="nohover" runat="server"><IMG id="imgAvatar" src="~/images/KanevaIcon01.gif" border="0" runat="server"></A>
																								</DIV>
																								<P>
																									<TABLE cellSpacing="0" cellPadding="0" align="center" border="0">
																										<TR>
																											<TD class="widgetText" align="left">
																												<asp:literal id="litOnline" runat="server"></asp:literal></TD>
																										</TR>
																									</TABLE>
																								</P>
																								<SPAN class="cb"><SPAN class="cl"></SPAN></SPAN>
																							</DIV>
																						</DIV>
																					</TD>
																				</TR>
																			</TABLE>
																		</TD> <!--end thumb section-->
																		<TD vAlign="top" ><img src="http://www.kaneva.com/images/spacer.gif" border="0" width="10" height="1" /></td>
																		<td valign="top">
																			<TABLE cellSpacing="0" cellPadding="0" width="100%" align="left" border="0">
																				<TR>
																					<TD class="widgetTextBold" noWrap align="left">
																						<asp:label id="lblUserName" runat="server"></asp:label></TD>
																				</TR>
																				<TR>
																					<TD class="widgetText small" align="left">
																						<asp:label id="lblLocation" runat="server"></asp:label></TD>
																				</TR>
																				<TR>
																					<TD class="widgetText small" align="left">
																						<asp:label id="lblGender" runat="server"></asp:label></TD>
																				</TR>
																				<TR>
																					<TD class="widgetText small" align="left">
																						<asp:label id="lblAge" runat="server"></asp:label></TD>
																				</TR>
																				<TR>
																					<TD noWrap><A class="widgetText" id="aPictures" style="TEXT-DECORATION: underline" runat="server">See 
																							More Photos</A></TD>
																				</TR>
																			</TABLE>
																		</TD>
																	</TR> <!--END ONE ELEMENT-->
																</TABLE>
															</DIV>
															<DIV class="float" id="controlSection2" runat="server" style="padding-bottom: 10px; width:98%;">
																<TABLE cellSpacing="0" cellPadding="0" width="100%" align="center" border="0"> <!--STARTS AN ELEMENT--->
																	<TR>
																		<TD vAlign="top">
																			<TABLE cellSpacing="0" cellPadding="0" align="center" border="0">
																				<TR>
																					<TD class="widgetText9" align="center">
																						<DIV id="meetme3d"><A class="flyoutTrigger" href="#" rel="flyout"></A><!--FLYOUT CONTENT -->
																							<DIV id="flyout">
																								<DIV id="flyoutcontent"><!--literal to hold image-->
																									<asp:literal id="litFlyoutImage" runat="server"></asp:literal><!--literal to hold link buttons -->
																									<asp:literal id="litFlyoutContent" runat="server"></asp:literal>
																								</DIV>
																							</DIV>
																						</DIV>
																						<SCRIPT language="javaScript">
																					<!--
																					cssdropdown.startchrome("meetme3d")
																					
																					// -->
																						</SCRIPT>
																					</TD>
																				</TR>
																			</TABLE><br>
																		</TD>
																	</TR>
																</TABLE>
																<TABLE cellSpacing="0" cellPadding="0" align="center" width="280px" border="0"> 
																<!--STARTS AN ELEMENT--->
																	<TR>
																		<TD vAlign="top">
																			<TABLE cellSpacing="0" cellPadding="0" align="left" border="0">
																				<!--
																				<TR>
																					<TD class="widgetText small" align="left" width="100">Member since:</TD>
																					<TD class="widgetText small" align="left">
																						<asp:label id="lblMemberSince" runat="server"></asp:label></TD>
																				</TR>
																				-->
																				<TR>
																					<TD class="widgetText small" align="left">Last login:</TD>
																					<TD class="widgetText small" align="left">
																						<asp:label id="lblLastLogin" runat="server"></asp:label></TD>
																				</TR>
																				<TR>
																					<TD class="widgetText small" align="left">Last update:</TD>
																					<TD class="widgetText small" align="left">
																						<asp:label id="lblLastUpdate" runat="server"></asp:label></TD>
																				</TR>
																			</TABLE>
																		</TD>
																		<TD vAlign="top">
																		    
																			<TABLE cellSpacing="0" cellPadding="0" align="center" width="75px" border="0">
																				<TR>
																					<TD class="widgetText9" align="center">
																						<ajax:ajaxpanel id="Ajaxpanel2" runat="server">
																							<asp:linkbutton id="btnVote" runat="server">
																								<img id="voteImg" runat="server" src="~/images/widgets/widget_raveit_bt.gif" width="18"
																									height="18" border="0" style="CURSOR: hand" alt="" /></asp:linkbutton>
																						</ajax:ajaxpanel></TD>
																				</TR>
																				
																				<TR>
																					<TD class="widgetText9" align="center">
																						<asp:linkbutton id="btnVote2" runat="server">Raves</asp:linkbutton></TD>
																				</TR>
																				
																				<TR>
																					<TD class="widgetText9" align="center">
																						<asp:label id="lblVotes" runat="server" text="0"></asp:label></TD>
																				</TR>
																				
																			</TABLE>
																			
																		</TD>
																	</TR> <!--END ONE ELEMENT-->
																</TABLE>
															</DIV>
															<DIV class="float" id="controlSection3" style="PADDING-BOTTOM: 0px; VERTICAL-ALIGN: top; HEIGHT: 60px"
																runat="server">
																<TABLE height="100%" cellSpacing="0" cellPadding="0" align="center" width="280px" border="0">
																	<TR>
																		<TD vAlign="top" height="60">
																			<TABLE cellSpacing="0" cellPadding="0" width="280" align="left" border="0">
																				<TR>
																					<TD vAlign="bottom" align="center">
																						<asp:linkbutton id="btnAddFriend2" runat="server">
																							<img runat="server" src="~/images/widgets/widget_addToFriends_bt.gif" height="16"
																								border="0" alt="" ID="Img18" /></asp:linkbutton></TD>
																					<TD><IMG id="Img19" height="1" src="~/images/spacer.gif" width="15" border="0" runat="server"></TD>
																					<TD vAlign="bottom" align="center">
																						<asp:linkbutton id="btnSendMessage2" runat="server">
																							<img runat="server" src="~/images/widgets/widget_sendMessage_bt.gif" height="16"
																								border="0" alt="" ID="Img20" /></asp:linkbutton></TD>
																					<TD><IMG id="Img21" height="1" src="~/images/spacer.gif" width="15" border="0" runat="server"></TD>
																					<TD vAlign="bottom" align="center">
																						<asp:linkbutton id="btnTellOthers2" onclick="btnTellOthers_Click" runat="server">
																							<img runat="server" src="~/images/widgets/widget_tellOthers_stop_bt.gif" height="16"
																								border="0" alt="" ID="Img22" /></asp:linkbutton></TD>
																					<TD><IMG id="Img23" height="1" src="~/images/spacer.gif" width="15" border="0" runat="server"></TD>
																					<TD vAlign="bottom" align="center">
																						<asp:linkbutton id="btnReportAbuse2" onclick="btnReportAbuse_Click" runat="server">
																							<img runat="server" src="~/images/widgets/widget_reportAbuse_bt.gif" height="16"
																								border="0" alt="" ID="Img24" /></asp:linkbutton></TD>
																					<TD><IMG id="Img25" height="1" src="~/images/spacer.gif" width="15" border="0" runat="server"></TD>
																					<TD vAlign="bottom" align="center">
																						<asp:linkbutton id="btnBlockUser2" onclick="btnBlockUser_Click" runat="server">
																							<img runat="server" src="~/images/widgets/widget_blockUser_iframe.gif" height="16"
																								border="0" alt="" ID="Img26" /></asp:linkbutton></TD>
																				</TR>
																				<TR>
																					<TD colSpan="9"><IMG id="Img27" height="2" src="~/images/spacer.gif" width="1" border="0" runat="server"></TD>
																				</TR>
																				<TR>
																					<TD align="center" width="50">
																						<asp:linkbutton id="btnAddFriend" runat="server" cssclass="widgetText9">Add to Friends</asp:linkbutton></TD>
																					<TD><IMG id="Img28" height="1" src="~/images/spacer.gif" width="15" border="0" runat="server"></TD>
																					<TD align="center" width="50">
																						<asp:linkbutton id="btnSendMessage" runat="server" cssclass="widgetText9">Send Message</asp:linkbutton></TD>
																					<TD><IMG id="Img29" height="1" src="~/images/spacer.gif" width="15" border="0" runat="server"></TD>
																					<TD align="center" width="50">
																						<asp:linkbutton id="btnTellOthers" onclick="btnTellOthers_Click" runat="server" cssclass="widgetText9">Tell Others</asp:linkbutton></TD>
																					<TD><IMG id="Img30" height="1" src="~/images/spacer.gif" width="15" border="0" runat="server"></TD>
																					<TD align="center" width="50">
																						<asp:linkbutton id="btnReportAbuse" onclick="btnReportAbuse_Click" runat="server" cssclass="widgetText9">Report Abuse</asp:linkbutton></TD>
																					<TD><IMG id="Img31" height="1" src="~/images/spacer.gif" width="15" border="0" runat="server"></TD>
																					<TD align="center" width="50">
																						<asp:linkbutton id="btnBlockUser" onclick="btnBlockUser_Click" runat="server" cssclass="widgetText9">Block User</asp:linkbutton></TD>
																				</TR>
																			</TABLE>
																		</TD>
																	</TR>
																</TABLE>
															</DIV>
														</DIV>
													</TD>
												</TR>
												<TR>
													<TD>
														<DIV class="widgetTextBold" id="divUrl" style="PADDING-LEFT: 10px" align="left" runat="server">
															<asp:hyperlink class="widgetLinkBt" id="hlPerm" runat="server"></asp:hyperlink></DIV>
													</TD>
												</TR>
											</TABLE>
										</DIV>
										<B class="innerFrame"><B class="innerFrame5"></B><B class="innerFrame4"></B><B class="innerFrame3">
											</B><B class="innerFrame2"><B></B></B><B class="innerFrame1"><B></B></B></B>
									</DIV>
								</TD>
							</TR>
						</TABLE>
					</DIV>
					<B class="outerFrame"><!-- style="display:none;" --><B class="outerFrame5"></B><B class="outerFrame4"></B><B class="outerFrame3"></B><B class="outerFrame2"><B></B></B><B class="outerFrame1"><B></B></B></B></DIV>
			</TD>
		</TR> <!---END CHANNEL LIST---></TABLE>
	<BR>
</ajax:ajaxpanel>
