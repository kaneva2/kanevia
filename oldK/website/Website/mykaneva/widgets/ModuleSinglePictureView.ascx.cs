///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System.Web.UI;
using KlausEnt.KEP.Kaneva.framework.widgets;

namespace KlausEnt.KEP.Kaneva.mykaneva.widgets
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

	/// <summary>
	///		Summary description for ModuleSinglePictureView.
	/// </summary>
	public class ModuleSinglePictureView : ModuleViewBaseControl
	{
		protected	LinkButton		lbEdit, lbDelete;

		protected	HtmlTable		tblEdit, tblPhoto;
		protected	Label			lblTitle;

		protected	HtmlImage		imgPicture;
		protected	HtmlTableCell	tdPicture;
		protected	HtmlContainerControl spnUploadMedia;
		protected	HtmlAnchor		aPicture;

		private		int				_asset_id	= 0;
		private		int				_alignment	= 1;	// center

		private void Page_Load(object sender, System.EventArgs e)
		{
			if (!IsPostBack)
			{
				BindData();
			}

			//open the editor window when user clicks edit button
			lbEdit.Attributes["onclick"] = "window.open('../mykaneva/ModuleEditor.aspx?modid=" + this.InstanceId
				+"&pageId=" + PageId + "' , 'ModuleEditor', 'width=750,height=680,scrollbars=1,resizable=1,status=1,toolbar=0'); return false;";

		}

		private void BindData()
		{
			tblEdit.Visible = ShowEdit;

			// init
			lblTitle.Visible = false;
			imgPicture.Style["display"] = "none";

			// show the call to action if it's the owner
			string channelUpload = string.Empty;
            if (ChannelId != GetUserFacade.GetPersonalChannelId(ProfileOwnerId))
			{
				channelUpload = "?communityId=" + ChannelId + "&userId=" + GetUserId();
			}
			spnUploadMedia.InnerHtml = "<a href=\"" + ResolveUrl("~/mykaneva/upload.aspx") + channelUpload + "\">Upload Photos" + "</a>";
			
			if (ShowEdit)
			{
				spnUploadMedia.Visible = true;
			}

			Constants.ePICTURE_SIZE pic_size = Constants.ePICTURE_SIZE.ACTUAL;

			if ( 0 == _asset_id )
			{
				DataRow drSinglePicture = WidgetUtility.GetModuleSinglePicture(InstanceId);
				if (drSinglePicture != null)
				{
					if ( drSinglePicture["title"] != DBNull.Value && drSinglePicture["title"].ToString() != "")
					{
						lblTitle.Text = drSinglePicture["title"].ToString();
						lblTitle.Visible = true;
					}

					pic_size	= (Constants.ePICTURE_SIZE)Convert.ToInt32( drSinglePicture["size"].ToString() );
					_alignment	= Convert.ToInt32( drSinglePicture["alignment"].ToString() );

					_asset_id	= Convert.ToInt32( drSinglePicture["asset_id"].ToString() );
				}
			}

			if ( _asset_id != 0 )
			{
				DataRow drAsset = StoreUtility.GetAsset( _asset_id );
				if ( drAsset != null )
				{
                    //check to see if it is mature and shouldnt be shown
                    bool isMature = drAsset["mature"].ToString().Equals("Y");
                    if (isMature && !KanevaWebGlobals.CurrentUser.HasAccessPass)
                    {
                        return;
                    }

					// if there is a record (it hasn't been deleted)
					if ( drAsset["publish_status_id"] != DBNull.Value )
					{
						// if a row comes back and it's not marked for deletion, then this asset must be published and not deleted
						if ( pic_size == Constants.ePICTURE_SIZE.RELATIVE )
						{
							imgPicture.Src = StoreUtility.GetPhotoImageURL (drAsset ["image_full_path"].ToString (), "la");
							imgPicture.Width = GetPictureWidth();
						}
						else
						{
							imgPicture.Src = GetPhotoURLFromSize ((int) pic_size, drAsset);
						}

						aPicture.HRef = GetPictureDetailsLink( _asset_id );

						imgPicture.Attributes["alt"] = drAsset["name"].ToString();
						imgPicture.Style["display"] = "";
						spnUploadMedia.Visible = false;
						tblPhoto.Visible = true;

						switch( _alignment )
						{
							case 0:
								tdPicture.Align = "Left";
								break;

							case 1:
								tdPicture.Align = "Center";
								break;

							case 2:
								tdPicture.Align = "Right";
								break;

						}
					}
				}
				
			}
			
		}

		private int GetPictureWidth()
		{
			return TotalWidth - 10;
		}

		public string GetPictureDetailsLink( int asset_id )
		{
			return Page.ResolveUrl ("~/asset/" + asset_id + ".media");
		}

		#region Event Handlers

		private void lbDelete_Click(object sender, EventArgs e)
		{
			WidgetUtility.DeleteModuleSinglePicture(InstanceId);
			FireModuleDeletedEvent();
		}

		#endregion

		#region Properties

		public int AssetId
		{
			get { return _asset_id; }
			set { _asset_id = value; }
		}

		public int Alignment
		{
			get { return _alignment; }
			set { _alignment = value; }
		}

		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);

			lbDelete.Attributes["onclick"] = "javascript:return " +
				"confirm('Are you sure you want to delete this widget?')"; 

			lbDelete.Click += new EventHandler(lbDelete_Click);
		}
		#endregion
	}
}
