<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MediaData.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.mykaneva.widgets.MediaData" %>

<asp:Literal id="litJS" runat="server"></asp:Literal>

<div id="media">

<div class="edit" id="mediaEdit" runat="server">
    <a class="btnmedium grey" id="uploadMedia" runat="server">Upload Media</a>
    <a class="btnmedium grey" id="manageMedia" runat="server">Media Library</a>
	<hr class="dataSeparator" />
</div> 

<asp:repeater id="rptMedia" runat="server">
	<itemtemplate>
		<div class="dataRow">
			<div class="framesize-medium">
				<div id="Div2" class="restricted" runat="server" visible='<%# IsMature (Convert.ToInt32 ( DataBinder.Eval(Container.DataItem, "AssetRatingId")))%>'></div>
				<div class="frame">
					<div class="imgconstrain">
						<a title='View <%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "Name").ToString ())%>' href='<%# GetAssetDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "AssetId")))%>'>
							<img border="0" src='<%# GetMediaImageURL (DataBinder.Eval(Container.DataItem, "ThumbnailMediumPath").ToString () ,"me", Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "AssetId")), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "AssetTypeId")), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "ThumbnailGen")))%>' border="0"/>
						</a>
					</div>	
				</div>
			</div>
			<div class="data">
				<div class="name"><a title='View <%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "Name").ToString ())%>' href='<%# GetAssetDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "AssetId")))%>'><%# Server.HtmlDecode (DataBinder.Eval(Container.DataItem, "Name").ToString()) %></a></div>
				<div class="desc"><%# Server.HtmlDecode (DataBinder.Eval(Container.DataItem, "Teaser").ToString()) %></div>
				<div class="raves">Raves: <%# DataBinder.Eval (Container.DataItem, "Stats.NumberOfDiggs").ToString ()%></div>
				<div class='<%# GetClass(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "AssetTypeId")))%>'>Views: <%# DataBinder.Eval (Container.DataItem, "Stats.NumberOfDownloads").ToString() %></div>
			</div>		
		</div>
	</itemtemplate>
	<separatortemplate><hr class="dataSeparator" /></separatortemplate>
</asp:repeater>


<div class="noData" id="divNoData" runat="server" visible="false"></div>

</div>
