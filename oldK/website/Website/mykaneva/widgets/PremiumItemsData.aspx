<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PremiumItemsData.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.mykaneva.widgets.PremiumItemsData" %>

<asp:Literal id="litJS" runat="server"></asp:Literal>

<div id="premiumItems">

<div class="dlContainer" id="divDataList" runat="server">

<div class="edit" id="premItemsEdit" runat="server">
    <a class="button" id="managePremItems" runat="server"><span>Media Premium Items</span></a>
	<hr class="dataSeparator" />
</div>											 

<asp:datalist visible="true" runat="server" showfooter="False" width="99%" id="dlPremItems" 
	cellpadding="0" cellspacing="0" border="0" repeatcolumns="2" itemstyle-width="50%" 
	horizontalalign="Center" repeatdirection="Horizontal" repeatlayout="table" itemstyle-verticalalign="top" >	
	<itemtemplate>
		<div class="dataRow">
			<div class="framesize-large">
				<div class="restricted" runat="server" visible='false'></div>
				<div class="frame">
					<div class="imgconstrain">
						<a title='View <%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "DisplayName").ToString ())%>' href='javascript:void(0);'>
							<img border="0" src='<%# GetPremiumItemImageURL (DataBinder.Eval (Container.DataItem, "ThumbnailLargePath").ToString (), "lg") %>' border="0"/>
						</a>
					</div>	
				</div>
			</div>
			<div class="data">
				<div class="name"><%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "DisplayName").ToString ())%></div>
				<p><%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "Description").ToString ())%></p>
				<div class="price"><%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "DesignerPrice").ToString ())%> credits</div>
				<div><a class="btnxsmall orange" id="aBuyNow" href='javascript:popBuyNow(<%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "GlobalId").ToString ())%>);'><span class="buynow">Buy Now</span></a></div>
			</div>
		</div>
	</itemtemplate>	
</asp:datalist>									 

<script type="text/javascript">    
var $j_buynow = jQuery.noConflict();	  
$j_buynow(document).ready(function()
{				
	popBuyNow=function(globalId)
	{
		$j_buynow.facebox({ iframe:'/buyNow.aspx?globalId=' + globalId + '&' });
	}
});
</script>

</div>

<div class="rptContainer" id="divRepeater" runat="server">

	<asp:repeater id="rptPremItems" runat="server">
		<itemtemplate>
			<div class="dataRow">
				<div class="framesize-large">
					<div id="Div1" class="restricted" runat="server" visible='false'></div>
					<div class="frame">
						<div class="imgconstrain">
							<a title='View <%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "DisplayName").ToString ())%>' href='javascript:void(0);'>
								<img border="0" src='<%# GetPremiumItemImageURL (DataBinder.Eval (Container.DataItem, "ThumbnailLargePath").ToString (), "lg") %>' border="0"/>
							</a>
						</div>	
					</div>
				</div>
				<div class="data">
					<div class="name"><%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "DisplayName").ToString ())%></div>
					<p><%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "Description").ToString ())%></p>
					<div class="price"><%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "DesignerPrice").ToString ())%> credits</div>
					<div><a class="btnxsmall orange" id="aBuyNow" href='javascript:popBuyNowLft(<%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "GlobalId").ToString ())%>);'><span class="buynow">Buy Now</span></a></div>
				</div>
			</div>
		</itemtemplate>	
	</asp:repeater>

<script type="text/javascript">    
var $j_buynowlft = jQuery.noConflict();	  
$j_buynowlft(document).ready(function()
{				
	popBuyNowLft=function(globalId)
	{
		$j_buynowlft.facebox({ iframe:'/buyNow.aspx?globalId=' + globalId + '&' });
	}
});
</script>

</div>

<div class="noData" id="divNoData" runat="server" visible="false"></div>

</div>