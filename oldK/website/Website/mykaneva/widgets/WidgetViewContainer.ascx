<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="WidgetViewContainer.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.mykaneva.widgets.WidgetViewContainer" %>

<div id="widget_frame">	
    <div id="widget_head" class="widgetHeaders" runat="server"> 
        <div class="widgetTitle" id="divTitle" runat="server"></div>
        <div class="widgetEdit" id="divEditArea" runat="server" visible="false">
			<asp:hyperlink id="hlEdit" tooltip="edit this widget" runat="server"><span>edit</span></asp:hyperlink>
		</div>
    </div>
    <div id="widget_body" runat="server">
        <div id="error"></div>
        <div>
			<asp:PlaceHolder runat="server" id="phWidgetShell"><div class="loadingData" id="divLoading" runat="server"></div><div class="loadingText">Loading...</div></asp:PlaceHolder>
		</div>
    </div>
    <div class="modulePaging">
		<div id="divPager"><!-- widget pager --></div>  
	</div>
</div>


<script type="text/javascript">
<!--
   document.observe("dom:loaded", function(){InitWidgetData ('<%= AjaxDataPage %>', '<%= widget_body.ClientID %>', 'error', '<%= AjaxDataPageParams %>', '<%= PageCount %>')});
//--> 
</script> 