///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using log4net;

namespace KlausEnt.KEP.Kaneva.mykaneva
{
    public partial class ContestDetails : Kaneva.mykaneva.widgets.ModuleViewBaseControl
    {
        protected void Page_Load (object sender, EventArgs e)
        {
            //clears local cache so data is always new
            Response.ContentType = "text/html";
            Response.CacheControl = "no-cache";
            Response.AddHeader ("Pragma", "no-cache");
            Response.Expires = -1;

            // They must be logged in
            if (!Request.IsAuthenticated)
            {
                Response.Redirect (GetLoginURL ());
                return;
            }

            litCommentsFilePath.Text = ResolveUrl ("~/usercontrols/doodad/");
        }
        
        #region Methods

        public bool ShowContest (UInt64 contestId)
        {
            Contest c = GetContestFacade.GetContest (contestId);

            if (c.ContestId == 0)
            {
                return false;
            }

            if (c.Status == (int) eCONTEST_STATUS.Inactive || c.Status == (int) eCONTEST_STATUS.Deleted)
            {
                return false;
            }

            litDescription.Text = Server.HtmlDecode(c.Description);
            litTitle.Text = Server.HtmlDecode(c.Title);
   
            ConfigureTimeRemaining (c);
            
            litPrizeAmount.Text = c.PrizeAmount.ToString ();
            litOwnerUsername.Text = c.Owner.Username;
            litNumEntries.Text = c.NumberOfEntries.ToString ();
            litNumVotes.Text = c.NumberOfVotes.ToString ();
            aThumb.HRef = GetPersonalChannelUrl (c.Owner.Username);
            aOwnerName.HRef = GetPersonalChannelUrl (c.Owner.Username);
            imgOwnerThumb.Src = GetProfileImageURL (c.Owner.ThumbnailSquarePath, "sq", c.Owner.Gender, c.Owner.FacebookSettings.UseFacebookProfilePicture, c.Owner.FacebookSettings.FacebookUserId);
            hidContestId.Value = c.ContestId.ToString ();

            btnReload.Text = c.ContestId.ToString ();
            OwnerHasVoted = false;
            ContestOwnerUserId = c.Owner.UserId;

            if (c.IsContestActive)           
            {
                IsContestActive = true;

                ClearAllMessages ();
                btnCreate.HRef = ResolveUrl ("~/mykaneva/ContestAddEntry.aspx?id=" + c.ContestId.ToString ());
                btnEnterNoEntries.HRef = ResolveUrl ("~/mykaneva/ContestAddEntry.aspx?id=" + c.ContestId.ToString ());
              
                // If user has already voted, they can't vote again
                EntriesUserHasVotedOn = GetContestFacade.GetEntriesUserHasVotedOn (c.ContestId, KanevaWebGlobals.CurrentUser.UserId);

                // Owner can't enter contest
                if (KanevaWebGlobals.CurrentUser.UserId != c.Owner.UserId)
                {
                    enter.Visible = true;
                }                                               
                
                // Config the follow button
                IsUserFollowing = GetContestFacade.IsUserFollowingContest (c.ContestId, KanevaWebGlobals.CurrentUser.UserId);
                ConfigFollowButton (true);

                divContestContainer.Attributes.Add ("class", "contest");

                if (CanDelete ())
                {
                    btnDeleteContest.Visible = true;
                }
            }
            else   // Contest has ended
            {
                // Hide Follow Button
                ConfigFollowButton (false);

                UserHasVoted = true;
                OwnerHasVoted = true;
                if (c.Owner.UserId == KanevaWebGlobals.CurrentUser.UserId &&
                    c.OwnerVoteWinnerEntryId == 0 &&
                    c.EndDate.AddDays (Configuration.ContestNumDaysToPickWinner) > DateTime.Now)
                {   // contest owner and no winner has been selected
                    OwnerHasVoted = false;
                    UserHasVoted = false;
                }

                IsContestActive = false;

                ShowContestEndedMsg (c);

                divContestContainer.Attributes.Add ("class", "contest ended");
            }

            ShowContestComments (c);
            ShowContestEntries (c);

            litRefreshUrl.Text = ResolveUrl ("~/mykaneva/contests.aspx?contestId=" + c.ContestId.ToString());
            return true;
        }

        private void ClearAllMessages ()
        {
            usernotify.Visible = false;
            divNoEntries.Visible = false;
            divNotEnoughEntries.Visible = false;
            divPickWinner.Visible = false;
            divNoWinnerYet.Visible = false;
            divWinnerIs.Visible = false;
        }

        private void ShowContestEndedMsg (Contest c)
        {
            usernotify.Visible = true;
            divNoEntries.Visible = false;
            divNotEnoughEntries.Visible = false;
            divPickWinner.Visible = false;
            divNoWinnerYet.Visible = false;
            divWinnerIs.Visible = false;

            if (c.Owner.UserId == KanevaWebGlobals.CurrentUser.UserId && c.OwnerVoteWinnerEntryId == 0)
            {   // Contest Owner
                if (c.NumberOfEntries == 0)
                {
                    divNoEntries.Visible = true;
                    return;
                }
                if (c.EndDate.AddDays (Configuration.ContestNumDaysToPickWinner) > DateTime.Now)
                {   // Still within window for owner to vote
                    if (c.NumberOfEntries < Configuration.ContestMinumumEntries)
                    {
                        divNotEnoughEntries.Visible = true;
                        return;
                    }
                    if (c.OwnerVoteWinnerEntryId == 0)
                    {
                        divPickWinner.Visible = true;
                        return;
                    }
                }
                else
                {   // Contest has ended and no winner was chosen
                    divNotEnoughEntries.Visible = true;
                    spnNotEnoughEntries.Visible = false;
                    return;
                }
            }
            else if (c.OwnerVoteWinnerEntryId == 0)
            {
                if (c.NumberOfEntries == 0)
                {
                    divNoEntries.Visible = true;
                    spnNoEntries.Visible = false;
                    return;
                }
                if (c.NumberOfEntries < Configuration.ContestMinumumEntries)
                {
                    // Is still within time period where owner can pick winner?
                    if (c.EndDate.AddDays (Configuration.ContestNumDaysToPickWinner) < DateTime.Now)
                    {
                        divNotEnoughEntries.Visible = true;
                        spnNotEnoughEntries.Visible = false;
                        return;
                    }
                }
                divNoWinnerYet.Visible = true;
                return;
            }

            divWinnerIs.Visible = true;
            ShowTheWinner (c);
        }

        private void ShowContestComments (Contest c)
        {
            KlausEnt.KEP.Kaneva.usercontrols.ContestComments commentWidget = (KlausEnt.KEP.Kaneva.usercontrols.ContestComments) Page.LoadControl (ResolveUrl ("~/usercontrols/doodad/ContestComments.ascx")); ;
            commentWidget.ContestEntryId = 0;
            commentWidget.ContestId = Convert.ToInt64 (c.ContestId);
            commentWidget.NumberOfComments = c.NumberOfComments;                          
            commentWidget.ShowCommentsBox = CanUserAddComments;
            phContestComments.Controls.Add (commentWidget);
        }

        private void ShowContestEntries (Contest c)
        {
            rptEntries.DataSource = c.ContestEntries;
            rptEntries.DataBind ();

            divNoDataEnded.Visible = false;
            divNoData.Visible = false;

            if (c.ContestEntries.Count == 0)
            {
                if (c.EndDate > DateTime.Now && (KanevaWebGlobals.CurrentUser.UserId != c.Owner.UserId))
                {
                    divNoData.Visible = true;
                }
                else
                {
                    divNoDataEnded.Visible = true;
                }
            }
        }

        private void ConfigureTimeRemaining (Contest c)
        {
            if (c.TimeSpanRemaining.Days == 0 && c.TimeSpanRemaining.Hours == 0 && c.TimeSpanRemaining.Seconds > -1)
            {   // show countdown clock
                // format  (year,month-1,day,hour,min,sec)
                litCountdownDate.Text = "new Date(" + c.EndDate.Year + "," + (c.EndDate.Month-1) + "," +
                    c.EndDate.Day + "," + c.EndDate.Hour + "," + c.EndDate.Minute + "," + c.EndDate.Second + ")";

                divTimeRemaining.Attributes.Add ("class", "remaining countdown alert");
                return;
            }
            litCountdownDate.Text = "-99";
            litTimeRemaining.Text = c.TimeRemaining;

            if (c.TimeSpanRemaining.Days == 0)
            {
                if (c.EndDate > DateTime.Now)
                {
                    divTimeRemaining.Attributes.Add ("class", "remaining alert");
                }
            } 
        }

        private void ConfigFollowButton (bool showFollowButton)
        {
            if (!showFollowButton)
            {
                btnFollow.Visible = false;
                return;
            }
            if (IsUserFollowing)
            {
                followBtnText.Attributes.Add ("class", "unfollow");
                followBtnText.InnerText = "Unfollow";
                btnFollow.Title = "Unfollow";
            }
            else
            {
                followBtnText.Attributes.Add ("class", "follow");
                followBtnText.InnerText = "Follow";
                btnFollow.Title = "Follow";
            }
        }

        private void ShowTheWinner (Contest c)
        {
            IEnumerable<ContestEntry> ceQuery =
                from contestentry in c.ContestEntries
                where contestentry.ContestEntryId == c.OwnerVoteWinnerEntryId  
                select contestentry;

            if (ceQuery != null)
            {
                rptWinner.DataSource = ceQuery;
                rptWinner.DataBind ();
            }
        }

        private void SendMessage (eCONTEST_MSG_TYPES type, Contest contest, int toUserId)
        {
            string subj = "";
            string msg = "";
            int fromUserId = 1;

            if (GetContestFacade.GetContestMessageDetails (type, contest, KanevaGlobals.SiteName, ref subj, ref msg))
            {
                // Insert the message
                Message message = new Message (0, fromUserId, toUserId, subj,
                    msg, new DateTime (), 0, (int) Constants.eMESSAGE_TYPE.PRIVATE_MESSAGE, 0, "U", "S");

                GetUserFacade.InsertMessage (message);

                User userTo = GetUserFacade.GetUser (toUserId);
                if (userTo.UserId > 0)
                {
                    if (Convert.ToInt32 (userTo.NotifyAnyoneMessages).Equals (1))
                    {
                        // They want emails for all messages
                        MailUtilityWeb.SendPrivateMessageNotificationEmail (userTo.Email, message.Subject, message.MessageId, message.MessageText, userTo.UserId, fromUserId);
                    }
                }
            }
        }

        private bool CanUserEnterContest (UInt64 contestId)
        {
            if (IsUserValidatedToEnterContest)
            {
                if (GetContestFacade.IsUserIPSameAsContestOwner (contestId, Common.GetVisitorIPAddress()))
                {
                    return false;
                }
                return true;
            }
            return false;
        }

        private void ShowErrMessage (string msg)
        {
            spanErrMsg.InnerHtml = msg;
            Page.ClientScript.RegisterStartupScript (this.GetType (), "myScript", "<script language=JavaScript>DisplayErrMsg();</script>"); 
        }

        private void SetContestWinner ()
        {
            try
            {
                if (hidContestEntryIDWinner.Value == "" || hidContestIdWinner.Value == "")
                {
                    m_logger.Error ("Error setting contest winner. Hidden field value for winning entry or contest id was empty.");
                    ShowErrMessage ("We encountered an error trying to save the winner.  Please try selecting the winner agein.");
                    return;
                }

                UInt64 contestId = Convert.ToUInt64 (hidContestIdWinner.Value);
                UInt64 winningEntryId = Convert.ToUInt64 (hidContestEntryIDWinner.Value);
                Contest contest = GetContestFacade.GetContest (contestId);

                // Only process winner if winner has not already been selected 
                if (contest.OwnerVoteWinnerEntryId == 0)
                {
                    // Update contest with winner
                    if (GetContestFacade.UpdateContestOwnerVoteWinner (contestId, winningEntryId) > 0)
                    {
                        // Pay winner contest amount
                        int wokTransId = 0;

                        ContestEntry winner = GetContestFacade.GetContestEntry (winningEntryId);

                        // If popular winner has not been set then set it also
                        if (contest.PopularVoteWinnerEntryId == 0)
                        {
                            GetContestFacade.SetPopularWinner (contest.ContestId);
                        }

                        if (winner.ContestId != contestId)
                        {
                            m_logger.Error ("Error contest winning entry does not match contest.");
                            ShowErrMessage ("We encountered an error trying to save the winning entry.  Please try selecting the winner agein.");
                            return;
                        }

                        try
                        {
                            wokTransId = GetContestFacade.MakeContestPayment (winner.Owner.UserId, winner.ContestId, Constants.CURR_KPOINT, Convert.ToDouble (contest.PrizeAmount), (int) TransactionType.eTRANSACTION_TYPES.CASH_TT_CONTEST, (int) TransactionType.eCONTEST_TRANSACTION_TYPES.CONTEST_TT_WINNER);
                        }
                        catch (Exception ex)
                        {
                            m_logger.Error ("Error adjusting user balance for winning contest id " + contestId.ToString () + ". ", ex);
                            return;
                        }

                        if (wokTransId > 0)
                        {
                            GetContestFacade.UpdateContestStatus (winner.ContestId, (int) eCONTEST_STATUS.Ended_Winner_Paid);

                            // Used to hold user ids that have have been sent emails so we don't duplicate
                            HashSet<int> ids = new HashSet<int> ();

                            // Send email to all entrants including winner
                            if (contest.ContestEntries.Count > 0)
                            {
                                foreach (ContestEntry entry in contest.ContestEntries)
                                {
                                    try
                                    {
                                        if (entry.ContestEntryId == winner.ContestEntryId)
                                        {
                                            // Send email to winner
                                            SendMessage (eCONTEST_MSG_TYPES.You_Won, contest, entry.Owner.UserId);
                                        }
                                        else
                                        {
                                            SendMessage (eCONTEST_MSG_TYPES.Entered_Did_Not_Win, contest, entry.Owner.UserId);
                                        }

                                        ids.Add (entry.Owner.UserId);
                                    }
                                    catch { }
                                }
                            }

                            // Send email to user who voted for winner
                            IList<ContestUser> voters = GetContestFacade.GetContestEntryVoters (winner.ContestEntryId, "", "");
                            if (voters.Count > 0)
                            {
                                foreach (ContestUser voter in voters)
                                {
                                    try
                                    {
                                        if (!ids.Contains (voter.UserId))
                                        {
                                            SendMessage (eCONTEST_MSG_TYPES.Voted_On_Winner, contest, voter.UserId);
                                            ids.Add (voter.UserId);
                                        }
                                    }
                                    catch { }
                                }
                            }

                            // Send email to followers
                            IList<ContestUser> followers = GetContestFacade.GetContestFollowers (winner.ContestId, "", "");
                            if (followers.Count > 0)
                            {
                                foreach (ContestUser follower in followers)
                                {
                                    try
                                    {
                                        if (!ids.Contains (follower.UserId))
                                        {
                                            SendMessage (eCONTEST_MSG_TYPES.Voted_On_Winner, contest, follower.UserId);
                                        }
                                    }
                                    catch { }
                                }
                            }

                            UserFacade userFacade = new UserFacade();
                            string requestId = userFacade.InsertTrackingRequest(Constants.cREQUEST_TRACKING_REQUEST_TYPE_ID_CONTEST_WINNER, KanevaWebGlobals.CurrentUser.UserId, 0, 0);
                            string requestTypeSentIdBlast = userFacade.InsertTrackingTypeSent(requestId, Constants.cREQUEST_TRACKING_MESSAGE_TYPE_ID_BLAST, 0);

                            // Send blast of winner
                            GetBlastFacade.SendContestWinnerBlast (winner.Owner.UserId,
                                ResolveUrl("~/mykaneva/contests.aspx?contestId=" + contest.ContestId.ToString() + "&" + Constants.cREQUEST_TRACKING_URL_PARAM + "=" + requestTypeSentIdBlast),
                                contest.Title, contest.PrizeAmount, winner.Owner.Username,
                                winner.Owner.Username, winner.Owner.ThumbnailSquarePath);

                            ShowContest (contest.ContestId);
                        }
                        else
                        {
                            ShowErrMessage ("We encountered an error trying to pay the winner.  Please contact support.");
                            return;
                        }
                    }
                    else
                    {
                        m_logger.Error ("Error saving contest winner. Update contest winner failed.");
                        ShowErrMessage ("We encountered an error trying to update the winner.  Please try selecting the winner agein.");
                        return;
                    }
                }
                else
                {
                    m_logger.Error ("Error saving contest winner. Winner has already been selected.");
                    ShowErrMessage ("A winner has already been selected for this contest.  Can not select another winner.");
                    return;
                }
            }
            catch (Exception ex)
            {
                m_logger.Error ("Error trying to process contest winner.", ex);
                ShowErrMessage (ex.Message);
                return;
            }
        }

        protected bool CanDelete ()
        {
            bool allowEditing = false;

            //check security level
            switch (KanevaWebGlobals.CheckUserAccess ((int) SitePrivilege.ePRIVILEGE.USER_DATA_ADMIN))
            {
                case (int) SitePrivilege.eACCESS_LEVEL.ACCESS_FULL:
                    allowEditing = true;
                    break;
                case (int) SitePrivilege.eACCESS_LEVEL.ACCESS_READ:
                    break;
                case (int) SitePrivilege.eACCESS_LEVEL.ACCESS_NONE:
                    break;
                default:
                    break;
            }
            return allowEditing;
        }

        #endregion Methods

        #region Event Handlers

        public void btnReload_Click (object sender, EventArgs e)
        {
            Contest c = GetContestFacade.GetContest (Convert.ToUInt64 (((Button) sender).Text));     
            litNumEntries.Text = c.NumberOfEntries.ToString ();
            ShowContestEntries (c);
            ShowContestComments (c);
        }

        public void btnFollow_Click (object sender, EventArgs e)
        {
            try
            {
                Contest c = GetContestFacade.GetContest (Convert.ToUInt64 (hidContestId.Value));

                if (((HtmlAnchor) sender).Title == "Follow")
                {
                    GetContestFacade.FollowContest (c.ContestId, KanevaWebGlobals.CurrentUser.UserId);
                    // Update the Follow button to Unfollow
                    IsUserFollowing = true;

                    UserFacade userFacade = new UserFacade();
                    string requestId = userFacade.InsertTrackingRequest(Constants.cREQUEST_TRACKING_REQUEST_TYPE_ID_CONTEST_FOLLOWING, KanevaWebGlobals.CurrentUser.UserId, 0, 0);
                    string requestTypeSentIdBlast = userFacade.InsertTrackingTypeSent(requestId, Constants.cREQUEST_TRACKING_MESSAGE_TYPE_ID_BLAST, 0);

                    // Send follow Blast
                    GetBlastFacade.SendContestFollowingBlast (KanevaWebGlobals.CurrentUser.UserId,
                        ResolveUrl("~/mykaneva/contests.aspx?contestId=" + c.ContestId.ToString() + "&" + Constants.cREQUEST_TRACKING_URL_PARAM + "=" + requestTypeSentIdBlast),
                        c.Title, c.PrizeAmount, KanevaWebGlobals.CurrentUser.Username,
                        KanevaWebGlobals.CurrentUser.NameNoSpaces, KanevaWebGlobals.CurrentUser.ThumbnailSquarePath);
                }
                else
                {
                    GetContestFacade.UnFollowContest (c.ContestId, KanevaWebGlobals.CurrentUser.UserId);
                    IsUserFollowing = false;
                }
                ConfigFollowButton (true);

                ShowContestEntries (c);
                ShowContestComments (c);
            }
            catch { }
        }

        public void btnDeleteContest_Click (object sender, EventArgs e)
        {
            try
            {
                int rc = GetContestFacade.DeleteContest (Convert.ToUInt64 (hidContestId.Value), KanevaWebGlobals.CurrentUser.UserId);
                if (rc > 0)
                {
                    Response.Redirect ("~/mykaneva/contests.aspx");
                }
                else
                {
                    ShowErrMessage ("Contest was not Deleted. Please try again.");
                }
            }
            catch (Exception ex)
            {
                m_logger.Error ("Error deleting contest. Contest id = " + hidContestId.Value, ex);
                ShowErrMessage ("Error deleting contest. " + ex.Message);
            }
        }

        protected void rptEntries_ItemDataBound (Object Sender, RepeaterItemEventArgs e)
        {
            if ((e.Item.ItemType == ListItemType.Item) || (e.Item.ItemType == ListItemType.AlternatingItem))
            {
                HiddenField hidContestID = ((HiddenField) e.Item.FindControl ("hidContestID"));
                HiddenField hidContestEntryID = ((HiddenField) e.Item.FindControl ("hidContestEntryID"));
                HiddenField hidNumberOfComments = ((HiddenField) e.Item.FindControl ("hidNumberOfComments"));
                PlaceHolder phEntryComments = ((PlaceHolder) e.Item.FindControl ("phEntryComments"));
                HtmlContainerControl divVoteCnt = ((HtmlContainerControl) e.Item.FindControl ("divVoteCnt"));
                HtmlContainerControl votebox = ((HtmlContainerControl) e.Item.FindControl ("votebox"));
                ImageButton btnVote = ((ImageButton) e.Item.FindControl ("btnVote"));

                if (phEntryComments != null)
                {
                    KlausEnt.KEP.Kaneva.usercontrols.ContestComments commentWidget = (KlausEnt.KEP.Kaneva.usercontrols.ContestComments) Page.LoadControl (ResolveUrl ("~/usercontrols/doodad/ContestComments.ascx")); ;
                    commentWidget.ContestId = Convert.ToInt64 (hidContestID.Value);
                    commentWidget.ContestEntryId = Convert.ToInt64 (hidContestEntryID.Value);
                    commentWidget.NumberOfComments = Convert.ToInt32 (hidNumberOfComments.Value);
                    commentWidget.ShowCommentsBox = CanUserAddComments;
                    phEntryComments.Controls.Add (commentWidget);
                }

                // If user has already voted on this contest, they can't vote again
                ContestEntry entry = (ContestEntry) e.Item.DataItem;
                divVoteCnt.InnerText = entry.NumberOfVotes.ToString ();
                
                if (IsContestActive)
                {
                    votebox.Attributes.Add ("class", "vote_entry");
                    
                    // Check if user has already voted on this entry
                    if (EntriesUserHasVotedOn.Contains (entry.ContestEntryId))
                    {
                        btnVote.ImageUrl = ResolveUrl ("~/images/contests/voted.gif");
                        btnVote.Enabled = false;
                    }
                    // Owner can't vote until contest is over
                    else if (KanevaWebGlobals.CurrentUser.UserId == ContestOwnerUserId)
                    {   
                        btnVote.Visible = false;
                        votebox.Attributes.Add ("class", "votenobtn");
                    }
                    else if (!CanUserVoteOnContest)
                    {
                        btnVote.Attributes.Add ("onclick", "DisplayMessage('You must have a valid email address to vote.');return false;");
                        btnVote.CommandName = "";
                        btnOk.Attributes.Add ("onclick", "parent.parent.location='general.aspx';parent.$j.colorbox.close(); return false;");
                    }
                    else
                    {
                        btnVote.CommandArgument = entry.ContestId.ToString () + "," + entry.ContestEntryId.ToString ();
                        btnVote.Attributes.Add ("onclick", "Vote(" + entry.ContestId.ToString () + "," + entry.ContestEntryId + ",'" + divVoteCnt.ClientID + "',this.id);return false;");
                        btnVote.ImageUrl = ResolveUrl ("~/images/contests/vote.gif");
                    }
                }
                else  // Contest has ended
                {
                    btnVote.Visible = false;
                    votebox.Attributes.Add ("class", "votenobtn");

                    if (!OwnerHasVoted && !IsContestActive && KanevaWebGlobals.CurrentUser.UserId == ContestOwnerUserId)
                    {
                        btnVote.Visible = true;
                        btnVote.Attributes.Add ("onclick", "javascript:$('hidContestIdWinner').value='" + entry.ContestId.ToString () + "';$('hidContestEntryIDWinner').value='" + entry.ContestEntryId.ToString () + "';return confirm('You sure you want this as the winner?');");
                        btnVote.CommandName = "winner";
                        btnVote.CommandArgument = "winner";
                        btnVote.ImageUrl = ResolveUrl ("~/images/contests/btn_winner_70x26.gif");
                        votebox.Attributes.Add ("class", "vote_entry");
                    }
                }
                

                // Delete link
                if (CanDelete ())
                {
                    LinkButton aDeleteEntry = ((LinkButton) e.Item.FindControl ("aDeleteEntry"));

                    if (aDeleteEntry != null)
                    {
                        aDeleteEntry.Visible = true;
                        aDeleteEntry.CommandArgument = entry.ContestId.ToString () + "," + entry.ContestEntryId.ToString ();
                        aDeleteEntry.CommandName = "delentry";
                    }
                }
            }
        }

        protected void rptEntries_ItemCommand (Object sender, RepeaterCommandEventArgs e)
        {
            if (e.CommandArgument.ToString ().Length > 0 && e.CommandName.ToString ().Length > 0)
            {
                try
                {
                    if (e.CommandName.ToString () == "winner")
                    {
                        SetContestWinner ();
                    }
                    else
                    {
                        string[] vals = e.CommandArgument.ToString ().Split (',');

                        UInt64 contestId = Convert.ToUInt64 (vals[0]);
                        UInt64 contestEntryId = Convert.ToUInt64 (vals[1]);

                        // verify user can vote
                        if (e.CommandName.ToString () == "vote")
                        {
                            if (!UserHasVoted)
                            {
                                GetContestFacade.InsertVote (KanevaWebGlobals.CurrentUser.UserId, KanevaWebGlobals.CurrentUser.IpAddress, contestId, contestEntryId);

                                Contest c = GetContestFacade.GetContest (contestId);
                                ((HtmlContainerControl) e.Item.FindControl ("divVoteCnt")).InnerText = c.NumberOfVotes.ToString ();
                                litNumVotes.Text = c.NumberOfVotes.ToString ();

                                UserHasVoted = true;

                                ShowContestEntries (c);
                                ShowContestComments (c);

                                UserFacade userFacade = new UserFacade();
                                string requestId = userFacade.InsertTrackingRequest(Constants.cREQUEST_TRACKING_REQUEST_TYPE_ID_CONTEST_VOTED, KanevaWebGlobals.CurrentUser.UserId, 0, 0);
                                string requestTypeSentIdBlast = userFacade.InsertTrackingTypeSent(requestId, Constants.cREQUEST_TRACKING_MESSAGE_TYPE_ID_BLAST, 0);

                                // Sent user voted Blast
                                GetBlastFacade.SendContestVotedBlast (KanevaWebGlobals.CurrentUser.UserId,
                                    ResolveUrl("~/mykaneva/contests.aspx?contestId=" + c.ContestId.ToString() + "&" + Constants.cREQUEST_TRACKING_URL_PARAM + "=" + requestTypeSentIdBlast),
                                    c.Title, c.PrizeAmount, KanevaWebGlobals.CurrentUser.Username,
                                    KanevaWebGlobals.CurrentUser.NameNoSpaces, KanevaWebGlobals.CurrentUser.ThumbnailSquarePath);
                            }
                        }
                        else if (e.CommandName.ToString () == "delentry")
                        {
                            if (CanDelete ())
                            {
                                ContestEntry ce = GetContestFacade.GetContestEntry (contestEntryId);
                                if (ce.ContestEntryId > 0)
                                {
                                    GetContestFacade.DeleteContestEntry (ce, KanevaWebGlobals.CurrentUser.UserId);
                                    
                                    Contest c = GetContestFacade.GetContest (contestId);
                                    ShowContestEntries (c);
                                    ShowContestComments (c);

                                    litNumEntries.Text = c.NumberOfEntries.ToString ();
                                    litNumVotes.Text = c.NumberOfVotes.ToString ();
                                }
                            }
                        }
                    }
                }
                catch { }
            }
        }
        
        #endregion Event Handlers

        #region Properties

        private bool CanUserVoteOnContest
        {
            get
            {
                if (KanevaWebGlobals.CurrentUser.StatusId.Equals ((int) Constants.eUSER_STATUS.REGVALIDATED))
                {
                    return true;
                }
                return false;
            }
        }

        private IList<UInt64> EntriesUserHasVotedOn
        {
            get
            {
                try
                {
                    List<UInt64> entryVotes = ViewState["EntriesUserHasVotedOn"] as List<UInt64>;
                    if (entryVotes == null)
                    {
                        return new List<UInt64> ();
                    }
                    return entryVotes;
                }
                catch
                {
                    return new List<UInt64> ();
                }
            }
            set
            {
                ViewState["EntriesUserHasVotedOn"] = value;
            }
        }
        
        private bool CanUserAddComments
        {
            get
            {
                return IsContestActive && KanevaWebGlobals.CurrentUser.HasWOKAccount;
            }
        }

        private bool UserHasVoted
        {
            get
            {
                try
                {
                    if (ViewState["UserHasVoted"] != null && ViewState["UserHasVoted"].ToString ().Length > 0)
                    {
                        return Convert.ToBoolean (ViewState["UserHasVoted"]);
                    }
                    return false;
                }
                catch
                {
                    return false;
                }
            }
            set
            {
                ViewState["UserHasVoted"] = value;
            }
        }

        private bool OwnerHasVoted
        {
            get
            {
                try
                {
                    if (ViewState["OwnerHasVoted"] != null && ViewState["OwnerHasVoted"].ToString ().Length > 0)
                    {
                        return Convert.ToBoolean (ViewState["OwnerHasVoted"]);
                    }
                    return false;
                }
                catch
                {
                    return false;
                }
            }
            set
            {
                ViewState["OwnerHasVoted"] = value;
            }
        }
        
        private bool IsUserFollowing
        {
            get
            {
                try
                {
                    if (ViewState["IsUserFollowing"] != null && ViewState["IsUserFollowing"].ToString ().Length > 0)
                    {
                        return Convert.ToBoolean (ViewState["IsUserFollowing"]);
                    }
                    return false;
                }
                catch
                {
                    return false;
                }
            }
            set
            {
                ViewState["IsUserFollowing"] = value;
            }
        }

        private bool IsContestActive
        {
            get
            {
                try
                {
                    if (ViewState["IsContestActive"] != null && ViewState["IsContestActive"].ToString ().Length > 0)
                    {
                        return Convert.ToBoolean (ViewState["IsContestActive"]);
                    }
                    return true;
                }
                catch
                {
                    return false;
                }
            }
            set
            {
                ViewState["IsContestActive"] = value;
            }
        }

        private bool IsUserValidatedToEnterContest
        {
            get
            {
                if (KanevaWebGlobals.CurrentUser.StatusId.Equals ((int) Constants.eUSER_STATUS.REGVALIDATED))
                {
                    UserFame userFame = GetFameFacade.GetUserFame (KanevaWebGlobals.CurrentUser.UserId, (int) FameTypes.World);
                    if (userFame.CurrentLevel.LevelNumber < Configuration.ContestMinimumFameLevelToEnter)
                    {
                        return false;
                    }
                    return true;
                }
                return false;
            }
        }

        private bool IsUserValidatedToVoteOnContest
        {
            get
            {
                if (KanevaWebGlobals.CurrentUser.StatusId.Equals ((int) Constants.eUSER_STATUS.REGVALIDATED))
                {
                    return true;
                }
                return false;
            }
        }

        private int ContestOwnerUserId
        {
            get
            {
                try
                {
                    if (ViewState["ContestOwnerUserId"] != null && ViewState["ContestOwnerUserId"].ToString ().Length > 0)
                    {
                        return Convert.ToInt32 (ViewState["ContestOwnerUserId"]);
                    }
                    return 0;
                }
                catch
                {
                    return 0;
                }
            }
            set
            {
                ViewState["ContestOwnerUserId"] = value;
            }
        }

        #endregion Properties

        #region Declerations

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

        #endregion Declerations
    }
}