﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContestSearchResults.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.mykaneva.ContestSearchResults" %>

<div class="clearit"></div>
<asp:Literal id="litJS" runat="server"></asp:Literal>

<div id="sort" runat="server" clientidmode="static" class="clearit">Sort by: 
	<asp:linkbutton class="sort" id="btnSortNewest" runat="server" oncommand="btnSort_Click" commandargument="created">Most Recent</asp:linkbutton> | 
	<asp:linkbutton class="sort" id="btnSortTimeRemaining" runat="server" oncommand="btnSort_Click" commandargument="remaining">Time Left</asp:linkbutton> | 
	<asp:linkbutton class="sort" id="btnSortAmount" runat="server" oncommand="btnSort_Click" commandargument="amount">Top Prizes</asp:linkbutton> | 
	<asp:linkbutton class="sort" id="btnSortActivity" runat="server" oncommand="btnSort_Click" commandargument="hot">Hot Contests</asp:linkbutton> 
</div>

<div id="contests">

	<asp:repeater id="rptContests" runat="server" onitemdatabound="rptContests_ItemDataBound">
		<itemtemplate>
			<div class='contest <%# Convert.ToBoolean(DataBinder.Eval (Container.DataItem, "IsContestActive")) ? "" : "ended" %>'>
				<div class="left">
					<div class="amount">
						<div class="value"><%# DataBinder.Eval (Container.DataItem, "PrizeAmount") %></div>
						<div>CREDITS</div>
					</div>
					<div class="time">
						<div class="label">TIME<br />LEFT</div>
						<div id="divTimeRemaining" runat="server" class="remaining"><%# DataBinder.Eval (Container.DataItem, "TimeRemaining") %></div>
					</div>
					<div class="clearit"><a class="buttonnarrow" href="javascript:void(0);" id="btnFollow" runat="server"><span class="follow" id="followBtnText" runat="server">Follow</span></a></div>
				</div>
				<div class="middle">
					<div class="title"><a href="javascript:void(0);" onclick="ShowDetails(<%# DataBinder.Eval (Container.DataItem, "ContestId") %>);"><%# DataBinder.Eval (Container.DataItem, "Title") %></a></div>
					<div class="data">
						<div class="winner" id="divAllContestsWinner" runat="server" visible="false">WINNER ANNOUNCED!</div>
						<div class="entries"><%# DataBinder.Eval (Container.DataItem, "NumberOfEntries") %> Entries</div>
						<div class="votes"><%# DataBinder.Eval (Container.DataItem, "NumberOfVotes") %> Votes</div>
					</div>	
					<div class="desc" id="divDesc" runat="server"></div>
				</div>
				<div class="right">
					
					<div class="framesize-xsmall">
						<div id="Div2" class="restricted" runat="server" visible='false'></div>
						<div class="frame">
							<div class="imgconstrain">
								<a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "Owner.UserName").ToString ())%>>
									<img id="Img2" runat="server" src='<%#GetProfileImageURL (DataBinder.Eval(Container.DataItem, "Owner.ThumbnailSmallPath").ToString (), "sm", DataBinder.Eval(Container.DataItem, "Owner.Gender").ToString ())%>' border="0"/>
								</a>
							</div>	
						</div>
					</div>

					<div class="creator">Created by:<br /><span class="name"><a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "Owner.UserName").ToString ())%>><%# DataBinder.Eval (Container.DataItem, "Owner.Username") %></a></span></div>
				</div>
			</div>
		</itemtemplate>
	</asp:repeater>

	<div id="divNoData" class="nodata" runat="server" visible="false">No Contests Found</div>
		
</div>
<div class="clearit" ></div>