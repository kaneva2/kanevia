///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using log4net;

namespace KlausEnt.KEP.Kaneva.mykaneva.widgets
{
    public partial class CommunityControlPanel : ModuleViewBaseControl
    {
        #region Declerations

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

        #endregion Declerations

        #region Page Load

        protected void Page_Load (object sender, EventArgs e)
        {
            // Put user code to initialize the page here
            if (!IsPostBack)
            {
                try
                {
                    GetRequestParams();
                    BindData ();
                }
                catch (Exception ex)
                {
                    m_logger.Error("Error loading World Control Panel widget. ", ex);
                    Response.Write ("<div class=\"error\">We're sorry but the data you requested could not be retrieved.</div>");
                }
            }
        }

        #endregion Page Load

        #region Functions

        private void BindData ()
        {
            // Configures control panel
            ConfigureControlPanel ();

            // Configure hangout 3d
            ConfigureMeetMe3D ();

            // Configure the rave button
            ConfigureRaves ();
        }

        private void ConfigureControlPanel ()
        {
            // Get the community
            Community community = new CommunityFacade ().GetCommunity (CommunityId);

            divCommunityName.InnerText = community.Name;
            divDescription.InnerText = community.Description;
            spnNumMembers.InnerText = community.Stats.NumberOfMembers.ToString ();

            // Setup message and join links, we'll add them later
            spnJoin = new HtmlGenericControl ("span");
            spnJoin.Attributes["class"] = "join";

            aJoinChannel = new HtmlAnchor ();
            aJoinChannel.Attributes["class"] = "";
            aJoinChannel.Controls.Add (spnJoin);

            // Get the summary counts
            spnNumMembers.InnerText = community.Stats.NumberOfMembers.ToString ();
            spnNumRaves.InnerText = community.Stats.NumberOfDiggs.ToString ();

            divTitle.InnerText = "World Settings";

            //Join or Already a member?
            aJoinChannel.Attributes["onclick"] = "javascript:return " +
                "confirm('Join this World?')";

            if (GetCommunityFacade.IsActiveCommunityMember (CommunityId, GetUserId ()))
            {
                aJoinChannel.Visible = false;
                if (KanevaWebGlobals.CurrentUser.UserId != community.CreatorId)
                {
                    divQuit.Visible = true;
                }
            }
            else
            {
                aJoinChannel.Visible = true;
                divQuit.Visible = false;
            }

            // Testing to see if the user's membership is pending.
            CommunityMember communityMember = new CommunityFacade ().GetCommunityMember (CommunityId, GetUserId ());

            if (communityMember.StatusId == (UInt32) CommunityMember.CommunityMemberStatus.PENDING)
            {
                PendingDivide.Visible = true;
                aJoinChannel.Visible = false;
                divQuit.Visible = false;

                aMembers.HRef = "javascript:void(0);";
            }

            if (GetUserId ().Equals (community.CreatorId))
            {
                aQuitChannel.Attributes["onclick"] = "javascript:" +
                    "alert('World owners cannot quit the World.\\nYou may either delete or transfer ownership of the World.');return (false);";
            }
            else
            {
                aQuitChannel.Attributes["onclick"] = "javascript:return " +
                    "confirm('Quit this World?')";
            }

            // Set links
            aReportAbuse.HRef = ResolveUrl ("~/suggestions.aspx?mode=WB&category=KANEVA%20Web%20Site");
            aTellOthers.HRef = ResolveUrl ("~/community/viral/CommunityViral.aspx?communityId=" + community.CommunityId);
            aContact.HRef = ResolveUrl ("~/myKaneva/newMessage.aspx?userId=" + community.CreatorId + "&communityId=" + community.CommunityId);
            aManage.HRef = ResolveUrl ("~/community/3dapps/AppManagement.aspx?communityId=" + community.CommunityId);
            aManageThumbnail.HRef = ResolveUrl ("~/community/3dapps/AppManagement.aspx?communityId=" + community.CommunityId);
            aJoinChannel.HRef = ResolveUrl ("~/community/commJoin.aspx?communityId=" + community.CommunityId + "&join=Y");
            aQuitChannel.HRef = ResolveUrl ("~/community/commJoin.aspx?communityId=" + community.CommunityId + "&join=N");
            aEvent.HRef = ResolveUrl ("~/mykaneva/events/eventForm.aspx?communityId=" + community.CommunityId); 

            //show avatar image based on AP settings and viewing users AP pass ownership
            if ((community.IsAdult.ToUpper ().Equals ("Y") && !KanevaWebGlobals.CurrentUser.HasAccessPass) ||
                community.ThumbnailLargePath.Equals (string.Empty))
            {
                imgAvatar.Src = KanevaGlobals.ImageServer + "/defaultworld_la.jpg";
            }
            else
            {
                imgAvatar.Src = GetBroadcastChannelImageURL (community.ThumbnailLargePath, "la");
                aJoinChannel.Visible = false;
            }
            divRestricted.Visible = community.IsAdult.Equals ("Y");

            string communityUrl = KanevaGlobals.GetBroadcastChannelUrl (community.NameNoSpaces.ToString ());
            if (!community.Url.Equals (string.Empty))
            {
                communityUrl = "http://" + KanevaGlobals.SiteName + "/" + community.Url;
            }
            else
            {
                communityUrl = "http://" + KanevaGlobals.SiteName + "/channel/" + community.NameNoSpaces + ".channel";
            }

            commUrl.InnerText = communityUrl;
            commUrl.Visible = false;

            if ((KanevaWebGlobals.CurrentUser.UserId == community.CreatorId) || (this.IsUserAdministrator ()))
            {
                divEditArea.Visible = true;
                divManageThumbnailContainer.Visible = (community.CommunityTypeId == (int) CommunityType.APP_3D); // 3d apps only
                hlEdit.NavigateUrl = ResolveUrl ("~/community/3dapps/AppManagement.aspx?communityId=" + community.CommunityId);
                widget_head.Visible = true;
            }
            else
            {
                widget_head.Visible = false;
            }

            // Set up summary count label links to select corresponding tab
            aMembers.InnerText = Constants.TabType.MEMBER;

            litCPId.Text = "controlPanel3DApp";
            litCPClassName.Text = "";
            litCPClassName.Text = "controlPanelFame";

            aJoinChannel.Visible = false;
            divContact.Visible = false;
            divManage.Visible = false;
            divEvent.Visible = false;

            if (KanevaWebGlobals.CurrentUser.UserId == community.CreatorId)
            {
                divManage.Visible = true;
                divEvent.Visible = true;
            }
            else
            {
                divContact.Visible = true;
            }

            spnJoin.InnerText = "Become a Fan";

            KlausEnt.KEP.Kaneva.channel.CommunityPage parent = (KlausEnt.KEP.Kaneva.channel.CommunityPage) this.Page;

            // If community is AP and user does not have AP, then they can't join or rave community
            if ((community.IsAdult.ToUpper ().Equals ("Y") && !KanevaWebGlobals.CurrentUser.HasAccessPass))
            {
                aJoinChannel.Visible = false;
                btnVote2.Visible = false;
            }
        }
        
        private void ConfigureMeetMe3D ()
        {
            Community community = GetCommunityFacade.GetCommunity(CommunityId);
            ConfigureCommunityMeetMe3D(btnMeet3D, community.WOK3App.GameId, community.CommunityId, community.Name, TrackingRequestId);
        }

        private void ConfigureRaves ()
        {
            if (!GetCommunityFacade.IsActiveCommunityMember (CommunityId, KanevaWebGlobals.CurrentUser.UserId))
            {
                spnRave.Attributes.Add ("class", "rave");
                spnRave.InnerText = "Rave";
            }
            else
            {
                spnRave.Attributes.Add ("class", "rave");
                spnRave.InnerText = "Raved";
            }

            litCommunityId.Text = CommunityId.ToString ();
            litCtrlRaveCount.Text = spnNumRaves.ClientID;
        }

        private void GetRequestParams()
        {
            try
            {
                if (CommunityId == 0)
                {
                    if (HttpContext.Current.Items["community"] != null)
                    {
                        CommunityId = ((Community) (HttpContext.Current.Items["community"])).CommunityId;
                    }
                    else if(Request["communityId"] != null)
                    {
                        //get channelId and find the default page
                        CommunityId = Convert.ToInt32 (Request["communityId"]);
                    }
                }
            }
            catch (Exception exc)
            {
                m_logger.Error("Error in GetRequestParams", exc);
            }
        }

        #endregion Functions

        #region Declerations

        protected Literal litCPId, litCPClassName;
        protected HtmlAnchor aJoinChannel;
        protected HtmlGenericControl spnJoin;

        #endregion Declerations

        #region Properties
        /// <summary>
        /// Tracking Request for the Play Now button
        /// </summary>
        private string TrackingRequestId
        {
            set
            {
                ViewState["TrackingRequestId"] = value;
            }
            get
            {
                if (ViewState["TrackingRequestId"] == null && KanevaWebGlobals.CurrentUser.UserId > 0)
                {
                    // Only do a passthrough of RTSID parameter if this is a newly created world and we're coming directly from the Create Your World page.
                    // This is a special case, in which traversal to world should be attributed to creation.
                    if (Request[Constants.cREQUEST_TRACKING_URL_PARAM] != null && GetUserFacade.GetTrackingRequestType(Request[Constants.cREQUEST_TRACKING_URL_PARAM].ToString()) == Constants.cREQUEST_TRACKING_REQUEST_TYPE_ID_WEB_WORLD_CREATION)
                    {
                        ViewState["TrackingRequestId"] = Request[Constants.cREQUEST_TRACKING_URL_PARAM].ToString();
                    }
                    else
                    {
                        string requestId = GetUserFacade.InsertTrackingRequest(Constants.cREQUEST_TRACKING_REQUEST_TYPE_ID_WORLD_PROFILE_PLAY_NOW, KanevaWebGlobals.CurrentUser.UserId, 0, 0);
                        ViewState["TrackingRequestId"] = GetUserFacade.InsertTrackingTypeSent(requestId, Constants.cREQUEST_TRACKING_MESSAGE_TYPE_ID_WEBSITE, KanevaWebGlobals.CurrentUser.UserId);
                    }
                }
                return (ViewState["TrackingRequestId"] ?? string.Empty).ToString();
            }
        }
        #endregion

        #region Event Handlers

        /// <summary>
        /// Handle a profile vote
        /// </summary>
        protected void btnVote_Click (object sender, EventArgs e)
        {
            if (!Request.IsAuthenticated)
            {
                Response.Redirect (GetLoginURL ());
            }

            if (GetRaveFacade.GetCommunityRaveCountByUser(KanevaWebGlobals.CurrentUser.UserId, CommunityId) == 0)
            {
                // Insert rave and join
                GetRaveFacade.RaveCommunity(KanevaWebGlobals.CurrentUser.UserId, CommunityId, WebCache.GetRaveType(RaveType.eRAVE_TYPE.SINGLE), "");

                // Update rave count
                spnNumRaves.InnerText = ((Convert.ToInt32(spnNumRaves.InnerText)) + 1).ToString();

                // GA Event
                Page.ClientScript.RegisterClientScriptBlock(GetType(), "gaRaveEvent", "callGAEvent('Community', 'Rave', 'RaveClick');", true);
            }
            else
            {
                // Just join the world
                GetCommunityFacade.JoinCommunity(CommunityId, KanevaWebGlobals.CurrentUser.UserId);
            }

            Response.Redirect (Request.RawUrl);
        }

        #endregion

    }
}
