///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using log4net;

namespace KlausEnt.KEP.Kaneva.mykaneva.widgets
{
    public partial class CommunityMembersData : BasePage
    {
        #region Declerations

        private int communityId = 0;
        private int userId = 0;
        private string orderBy = "cm.account_type_id, u.username";
        private int pageSize = 15;
        private bool showGrid = true;
        private string filter = "";
        private int pageNum = 1;
        private int profileOwnerId = 0;
        private bool isProfileOwner = false;
        private bool isProfileAdmin = false;

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

        #endregion Declerations

        #region Page Load

        protected void Page_Load (object sender, EventArgs e)
        {
            try
            {
                //clears local cache so data is always new
                Response.ContentType = "text/plain";
                Response.CacheControl = "no-cache";
                Response.AddHeader("Pragma", "no-cache");
                Response.Expires = -1;

                GetRequestValues();
                CheckUserAccess();
                BindData();
                SetEditOptions();
            }
            catch (Exception ex)
            {
                m_logger.Error("Error loading World Members widget for CommunityId = " + CommunityId + ". ", ex);

                divNoData.InnerText = "We're sorry but the data could not be retrieved.";
                divNoData.Visible = true;

                // Always call this method so the totalRecords value will get updated
                // and the pager will be hidden if it is not needed
                litJS.Text = "<script type=\"text/javascript\">SetAjaxPager(1,0);</script>";
            }
        }

        #endregion Page Load

        #region Functions

        private void BindData ()
        {

            if (!Request.IsAuthenticated)
            {
                Response.End();
            }

            if (MemberFilter.Length > 0)
            {
                m_logger.Debug("User from IP " + Common.GetVisitorIPAddress() +  " tried filter " + MemberFilter);
            }

            //bind channels
            PagedList<CommunityMember> cMembers = new CommunityFacade().GetCommunityMembers(CommunityId, 1, false, false, false, false, "", OrderBy, PageNum, PageSize, true, MemberFilter);

            if (cMembers.TotalCount > 0)
            {
                if (ShowGrid)
                {
                    dlMembers.DataSource = cMembers;
                    dlMembers.DataBind ();
                }
                else
                {
                    rptMembers.DataSource = cMembers;
                    rptMembers.DataBind ();
                }
            }
            else
            {
                divNoData.Visible = true;
                divNoData.InnerHtml = "<div>No members found for this World.</div>" ;
            }

            // Always call this method so the totalRecords value will get updated
            // and the pager will be hidden if it is not needed
            if (ShowGrid)
            {
                litJS.Text = "<script type=\"text/javascript\">SetAjaxPager(" + pageSize.ToString () + "," + cMembers.TotalCount.ToString () + ");</script>";
            }
        }

        private void GetRequestValues ()
        {
            // Get the community Id
            if (Request["communityId"] != null)
            {
                try
                {
                    CommunityId = Convert.ToInt32 (Request["communityId"]);
                }
                catch { }
            }

            if (Request["showgrid"] != null)
            {
                showGrid = Convert.ToBoolean (Request["showgrid"]);
            }

            // Get the Show Grid value
            if (Request["filter"] != null && Request["filter"].ToString ().Length > 0)
            {
                try
                {
                    MemberFilter = Request["filter"].ToString () + "%";
                }
                catch { }{
                }

            }

            // Get the Show Grid value
            if (Request["showgrid"] != null)
            {
                try
                {
                    ShowGrid = Convert.ToBoolean (Request["showgrid"]);
                }
                catch { }
            }

            // Get orderby field
            if (Request["orderby"] != null)
            {
                try
                {
                    if (Request["orderby"] != null)
                    {
                        OrderBy = Request["orderby"].ToString ();
                        switch (OrderBy.ToLower())
                        {
                            case "raves":
                                OrderBy = "cs.number_of_diggs DESC";
                                break;
                            case "views":
                                OrderBy = "cs.number_of_views DESC";
                                break;
                            case "date":
                                OrderBy = "cm.added_date DESC";
                                break;
                            case "rand":
                                OrderBy = "last_login DESC";
                                break;
                            default:
                                OrderBy = "cm.account_type_id, u.username";
                                break;
                        }
                    }
                }
                catch { }
            }

            // Get items per page count
            if (Request["pagesize"] != null)
            {
                try
                {
                    PageSize = Convert.ToInt32 (Request["pagesize"]);
                }
                catch { }
            }

            // Get the current page number
            if (Request["p"] != null)
            {
                try
                {
                    PageNum = Convert.ToInt32 (Request["p"]);
                }
                catch { }
            }

            // Get the user Id from the parameters 
            if (Request["profileOwnerId"] != null)
            {
                try
                {
                    ProfileOwnerId = Convert.ToInt32(Request["profileOwnerId"]);
                }
                catch { }
            }
            else
            {
                try
                {
                    ProfileOwnerId = GetCommunityFacade.GetCommunity(CommunityId).CreatorId;
                }
                catch { }
            }
        }

        #endregion Functions

        #region Helper Functions

        private void SetEditOptions()
        {
            membersEdit.Visible = false;

            if (ShowGrid)
            {
                if ((isProfileOwner || this.isProfileAdmin))
                {
                    editMembers.HRef = this.ResolveUrl("~/community/members.aspx?communityId=" + CommunityId);
                    membersEdit.Visible = true;
                }
                else
                {
                    //set callout
                    if (divNoData.Visible)
                    {
                        divNoData.InnerHtml += "<div>Would you like to <a href=\"" + ResolveUrl("~/community/commJoin.aspx?communityId=" + CommunityId + "&join=Y") + 
                            "\" onclick=\"javascript:return confirm('Join This World?')\" alt=\"upload media\" >Join this World</a>?</div>";
                    }              
                }
            }
        }

        /// <summary>
        /// Checks the current user's access level
        /// </summary>
        private void CheckUserAccess()
        {
            //get the users Id
            userId = GetUserId();

            //see if they are the profile/community owner
            isProfileOwner = userId.Equals(profileOwnerId);

            if (!isProfileOwner)
            {
                //check the privilege level to see if they are an admin
                this.isProfileAdmin = HasWritePrivileges((int)SitePrivilege.ePRIVILEGE.COMMUNITY_PROFILE_ADMIN);
            }
        }


        #endregion

        #region Event Handlers

        protected void rptMembers_ItemDataBound (object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                int userId = Int32.Parse (DataBinder.Eval (e.Item.DataItem, "UserId").ToString ());
                bool isOnline = DataBinder.Eval (e.Item.DataItem, "Online").ToString () == "1";

                HtmlContainerControl divOnlineStatus = (HtmlContainerControl)e.Item.FindControl("divOnlineStatus2");

                if (isOnline)
                {
                    divOnlineStatus.InnerHtml = "<img src=\"" + ResolveUrl ("~/images/widgets/widget_online_icon.gif") + "\"></img>&nbsp;&nbsp;Online";
                }

                HtmlContainerControl divRole = (HtmlContainerControl)e.Item.FindControl("divRole2");

                divRole.Visible = true;
                int accType = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "AccountTypeId"));
                switch (accType)
                {
                    //show owner and member only
                    case (int)CommunityMember.CommunityMemberAccountType.OWNER:
                        divRole.InnerText = "OWNER";
                        break;
                    case (int)CommunityMember.CommunityMemberAccountType.MODERATOR:
                        divRole.InnerText = "MODERATOR";
                        break;
                    default:
                        divRole.Visible = false;
                        break;
                }
            }
        }

        protected void dlMembers_ItemDataBound (object sender, DataListItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                int userId = Int32.Parse (DataBinder.Eval (e.Item.DataItem, "UserId").ToString ());
                bool isOnline = DataBinder.Eval (e.Item.DataItem, "Online").ToString () == "1";

                HtmlContainerControl divOnlineStatus = (HtmlContainerControl) e.Item.FindControl ("divOnlineStatus");

                if (isOnline)
                {
                    divOnlineStatus.InnerHtml = "<img src=\"" + ResolveUrl ("~/images/widgets/widget_online_icon.gif") + "\"></img>&nbsp;Online";
                }

                HtmlContainerControl divJoinDate = (HtmlContainerControl) e.Item.FindControl ("divJoinDate");
                divJoinDate.InnerText = "Member since " + Convert.ToDateTime (
                    DataBinder.Eval (e.Item.DataItem, "AddedDate")).ToString ("MM/dd/yy");

                HtmlContainerControl divRole = (HtmlContainerControl) e.Item.FindControl ("divRole");

                divRole.Visible = true;
                int accType = Convert.ToInt32 (DataBinder.Eval (e.Item.DataItem, "AccountTypeId"));
                switch (accType)
                {
                    //show owner and member only
                    case (int) CommunityMember.CommunityMemberAccountType.OWNER:
                        divRole.InnerText = "OWNER";
                        break;
                    case (int) CommunityMember.CommunityMemberAccountType.MODERATOR:
                        divRole.InnerText = "MODERATOR";
                        break;
                    default:
                        divRole.InnerText = " ";
                        //divRole.Visible = false;
                        break;
                }
            }
        }

        #endregion Event Handlers

        #region Properties

        private int ProfileOwnerId
        {
            set { this.profileOwnerId = value; }
            get { return this.profileOwnerId; }
        }
        private int CommunityId
        {
            set { this.communityId = value; }
            get { return this.communityId; }
        }
        private int PageNum
        {
            set { this.pageNum = value; }
            get { return this.pageNum; }
        }
        private int PageSize
        {
            set { this.pageSize = value; }
            get { return this.pageSize; }
        }
        private string MemberFilter
        {
            set { this.filter = value; }
            get { return this.filter; }
        }
        private bool ShowGrid
        {
            set { this.showGrid = value; }
            get { return this.showGrid; }
        }
        private string OrderBy
        {
            set { this.orderBy = value; }
            get { return this.orderBy; }
        }

        #endregion Properties
    }
}
