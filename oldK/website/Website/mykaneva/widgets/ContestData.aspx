﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ContestData.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.mykaneva.widgets.ContestData" %>

<div class="clearit"></div>
<asp:Literal id="litJS" runat="server"></asp:Literal>

<div id="usernotify" clientidmode="Static" runat="server" visible="false">
	<div id="notifyimg"><img runat="server" id="imgnotify" /></div>
	<div class="notifycontainer">
	<asp:repeater id="rptContestsNeedWinner" runat="server">
		<headertemplate>
			<div class="heading">Your Contest has ended, please pick a winner.</div>
		</headertemplate>
		<itemtemplate>
			<div class="subheading"><a href="javascript:void(0);" onclick="ShowDetails(<%# DataBinder.Eval (Container.DataItem, "ContestId") %>);"><%# DataBinder.Eval (Container.DataItem, "Title") %></a></div>
		</itemtemplate>	
	</asp:repeater>
	<asp:repeater id="rptContestsWon" runat="server">
		<headertemplate>
			<div class="heading">Congratulations!  You've won a Contest!</div>
		</headertemplate>
		<itemtemplate>
			<div class="subheading"><a href="javascript:void(0);" onclick="ShowDetails(<%# DataBinder.Eval (Container.DataItem, "ContestId") %>);"><%# DataBinder.Eval (Container.DataItem, "Title") %></a></div>
		</itemtemplate>	
	</asp:repeater>
	</div>
	<div class="notifyfooter"></div>
</div>

<div id="sort" class="clearit">Sort by: 
	<a class="sort" id="sortNewest" runat="server" href="javascript:void(0);" onclick="SetDisplayOrder ('createdate');">Most Recent</a> | 
	<a class="sort" id="sortTimeRemaining" runat="server" href="javascript:void(0);" onclick="SetDisplayOrder ('remaining');">Time Left</a> | 
	<a class="sort" id="sortAmount" runat="server" href="javascript:void(0);" onclick="SetDisplayOrder ('amount');">Top Prizes</a> | 
	<a class="sort" id="sortActivity" runat="server" href="javascript:void(0);" onclick="SetDisplayOrder ('hot');">Hot Contests</a> 
</div>

<div id="contests">

	<asp:repeater id="rptContests" runat="server" onitemdatabound="rptContests_ItemDataBound">
		<itemtemplate>
			<div class='contest <%# Convert.ToBoolean(DataBinder.Eval (Container.DataItem, "IsContestActive")) ? "" : "ended" %>'>
				<div class="left">
					<div class="amount">
						<div class="value"><%# DataBinder.Eval (Container.DataItem, "PrizeAmount") %></div>
						<div>CREDITS</div>
					</div>
					<div class="time">
						<div class="label">TIME<br />LEFT</div>
						<div id="divTimeRemaining" runat="server" class="remaining"><%# DataBinder.Eval (Container.DataItem, "TimeRemaining") %></div>
					</div>
					<div class="clearit"><a class="buttonnarrow" href="javascript:void(0);" id="btnFollow" runat="server"><span class="follow" id="followBtnText" runat="server">Follow</span></a></div>
				</div>
				<div class="middle">
					<div class="title"><a href="javascript:void(0);" onclick="ShowDetails(<%# DataBinder.Eval (Container.DataItem, "ContestId") %>);"><%# Server.HtmlDecode(DataBinder.Eval (Container.DataItem, "Title").ToString()) %></a></div>
					<div class="data">
						<div class="winner" id="divAllContestsWinner" runat="server" visible="false">WINNER ANNOUNCED!</div>
						<div class="entries"><%# DataBinder.Eval (Container.DataItem, "NumberOfEntries") %> Entries</div>
						<div class="votes"><%# DataBinder.Eval (Container.DataItem, "NumberOfVotes") %> Votes</div>
					</div>	
					<div class="desc" id="divDesc" runat="server"></div>
				</div>
				<div class="right">
					
					<div class="framesize-xsmall">
						<div id="Div2" class="restricted" runat="server" visible='false'></div>
						<div class="frame">
							<div class="imgconstrain">
								<a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "Owner.UserName").ToString ())%>>
									<img id="Img2" runat="server" src='<%# GetProfileImageURL (DataBinder.Eval(Container.DataItem, "Owner.ThumbnailSquarePath").ToString (), "sq", DataBinder.Eval(Container.DataItem, "Owner.Gender").ToString (), Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "Owner.FacebookSettings.UseFacebookProfilePicture")), Convert.ToUInt64(DataBinder.Eval(Container.DataItem, "Owner.FacebookSettings.FacebookUserId")) )%>' border="0"/>
								</a>
							</div>	
						</div>
					</div>

					<div class="creator">Created by:<br /><span class="name"><a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "Owner.UserName").ToString ())%>><%# DataBinder.Eval (Container.DataItem, "Owner.Username") %></a></span></div>
				</div>
			</div>
		</itemtemplate>
	</asp:repeater>

	<div id="divNoData" class="nodata" runat="server" visible="false">No Contests Found</div>
		
</div>
<div class="clearit" ></div>
