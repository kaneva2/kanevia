///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.mykaneva.widgets
{
    public partial class ContestMyData : BasePage
    {
        #region Declerations

        private string orderBy = "is_active DESC, end_date ASC";
        private string filterBy = "status > " + (int) eCONTEST_STATUS.Inactive + " AND archived = 0";
        private int pageSize = 1000;
        private int pageNum = 1;

        // Logger
        //       private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

        #endregion Declerations
        
        protected void Page_Load (object sender, EventArgs e)
        {
            //clears local cache so data is always new
            Response.ContentType = "text/html";
            Response.CacheControl = "no-cache";
            Response.AddHeader ("Pragma", "no-cache");
            Response.Expires = -1;

            GetMyContests ();

            GetContestsEntered ();

            GetContestsFollowing ();

            GetContestsVotedOn ();

            // Always call this method so the totalRecords value will get updated
            // and the pager will be hidden if it is not needed
            litJS.Text = "<script type=\"text/javascript\">SetAjaxPager(1,0);</script>";

        }

        private void GetMyContests ()
        {
            PagedList<Contest> pdtMyContests = GetContestFacade.GetContests (KanevaWebGlobals.CurrentUser.UserId, orderBy, filterBy, pageNum, pageSize);
            if (pdtMyContests.TotalCount > 0)
            {
                rptMyContests.DataSource = pdtMyContests;
                rptMyContests.DataBind ();
            }
            else
            {
                divMyContestNoData.Visible = true;
            }
        }

        private void GetContestsEntered ()
        {
            PagedList<Contest> pdtEntered = GetContestFacade.GetContestsEntered (KanevaWebGlobals.CurrentUser.UserId, orderBy, filterBy, pageNum, pageSize);
            if (pdtEntered.TotalCount > 0)
            {
                rptContestsEntered.DataSource = pdtEntered;
                rptContestsEntered.DataBind ();
            }
            else
            {
                divEnteredNoData.Visible = true;
            }
        }

        private void GetContestsFollowing ()
        {
            PagedList<Contest> pdtFollowing = GetContestFacade.GetContestsFollowing (KanevaWebGlobals.CurrentUser.UserId, orderBy, filterBy, pageNum, pageSize);
            if (pdtFollowing.TotalCount > 0)
            {
                rptContestsFollowing.DataSource = pdtFollowing;
                rptContestsFollowing.DataBind ();
            }
            else
            {
                divFollowingNoData.Visible = true;
            }
        }

        private void GetContestsVotedOn ()
        {
            PagedList<Contest> pdtVoted = GetContestFacade.GetContestsVotedOn (KanevaWebGlobals.CurrentUser.UserId, orderBy, filterBy, pageNum, pageSize);
            if (pdtVoted.TotalCount > 0)
            {
                rptContestsVoted.DataSource = pdtVoted;
                rptContestsVoted.DataBind ();
            }
            else
            {
                divVotedNoData.Visible = true;
            }
        }

        protected void rptMyContests_ItemDataBound (Object Sender, RepeaterItemEventArgs e)
        {
            if ((e.Item.ItemType == ListItemType.Item) || (e.Item.ItemType == ListItemType.AlternatingItem))
            {
                HtmlGenericControl followBtnText = ((HtmlGenericControl) e.Item.FindControl ("followBtnText"));
                HtmlAnchor btnFollow = ((HtmlAnchor) e.Item.FindControl ("btnFollowMyContests"));
                HtmlContainerControl divWinner = ((HtmlContainerControl) e.Item.FindControl ("divMyContestsWinner"));
                HtmlContainerControl divDesc = ((HtmlContainerControl) e.Item.FindControl ("divMyContestsDesc"));
                
                // If user has already voted on this contest, they can't vote again
                Contest c = (Contest) e.Item.DataItem;

                // Time Remaining style
                string timeCSS = "remaining";

                if (c.IsContestActive)
                {
                    if (GetContestFacade.IsUserFollowingContest (c.ContestId, KanevaWebGlobals.CurrentUser.UserId))
                    {
                        followBtnText.Attributes.Add ("class", "unfollow");
                        followBtnText.InnerText = "Unfollow";
                        btnFollow.Title = "Unfollow";
                    }
                    else
                    {
                        followBtnText.Attributes.Add ("class", "follow");
                        followBtnText.InnerText = "Follow";
                        btnFollow.Title = "Follow";
                    }

                    btnFollow.Attributes.Add ("onclick", "Follow(" + c.ContestId.ToString () + ",'" + followBtnText.ClientID + "')");

                    if (c.TimeSpanRemaining.Days == 0)
                    {
                        timeCSS += " alert";
                    }
                    ((HtmlGenericControl) e.Item.FindControl ("divTimeRemaining")).Attributes.Add ("class", timeCSS);
                }
                else
                {
                    btnFollow.Visible = false;
                }

                divDesc.InnerText = TruncateWithEllipsis ((Common.HtmlRemoval.StripTagsCharArray (Server.HtmlDecode (c.Description)).Replace ("&nbsp;", " ")), 136);

                // Has winner been selected?
                if (c.OwnerVoteWinnerEntryId > 0)
                {
                    divWinner.Visible = true;
                }
            }
        }

        protected void rptContestsEntered_ItemDataBound (Object Sender, RepeaterItemEventArgs e)
        {
            if ((e.Item.ItemType == ListItemType.Item) || (e.Item.ItemType == ListItemType.AlternatingItem))
            {
                HtmlGenericControl followBtnText = ((HtmlGenericControl) e.Item.FindControl ("followBtnTextEntered"));
                HtmlAnchor btnFollow = ((HtmlAnchor) e.Item.FindControl ("btnFollowEntered"));
                HtmlContainerControl divWinner = ((HtmlContainerControl) e.Item.FindControl ("divEnterWinner"));
                HtmlContainerControl divDesc = ((HtmlContainerControl) e.Item.FindControl ("divEnteredDesc"));
                
                // If user has already voted on this contest, they can't vote again
                Contest c = (Contest) e.Item.DataItem;

                // Time Remaining style
                string timeCSS = "remaining";

                if (c.IsContestActive)
                {
                    if (GetContestFacade.IsUserFollowingContest (c.ContestId, KanevaWebGlobals.CurrentUser.UserId))
                    {
                        followBtnText.Attributes.Add ("class", "unfollow");
                        followBtnText.InnerText = "Unfollow";
                        btnFollow.Title = "Unfollow";
                    }
                    else
                    {
                        followBtnText.Attributes.Add ("class", "follow");
                        followBtnText.InnerText = "Follow";
                        btnFollow.Title = "Follow";
                    }

                    btnFollow.Attributes.Add ("onclick", "Follow(" + c.ContestId.ToString () + ",'" + followBtnText.ClientID + "')");

                    if (c.TimeSpanRemaining.Days == 0)
                    {
                        timeCSS += " alert";
                    }
                    ((HtmlGenericControl) e.Item.FindControl ("divTimeRemainingEntered")).Attributes.Add ("class", timeCSS);
                }
                else
                {
                    btnFollow.Visible = false;
                }

                divDesc.InnerText = TruncateWithEllipsis ((Common.HtmlRemoval.StripTagsCharArray (Server.HtmlDecode (c.Description)).Replace ("&nbsp;", " ")), 136);

                // Has winner been selected?
                if (c.OwnerVoteWinnerEntryId > 0)
                {
                    divWinner.Visible = true;
                }
            }
        }

        protected void rptContestsFollowing_ItemDataBound (Object Sender, RepeaterItemEventArgs e)
        {
            if ((e.Item.ItemType == ListItemType.Item) || (e.Item.ItemType == ListItemType.AlternatingItem))
            {
                HtmlGenericControl followBtnText = ((HtmlGenericControl) e.Item.FindControl ("followBtnTextFollow"));
                HtmlAnchor btnFollow = ((HtmlAnchor) e.Item.FindControl ("btnFollowFollowing"));
                HtmlContainerControl divWinner = ((HtmlContainerControl) e.Item.FindControl ("divFollowWinner"));
                HtmlContainerControl divDesc = ((HtmlContainerControl) e.Item.FindControl ("divFollowingDesc"));
                
                // If user has already voted on this contest, they can't vote again
                Contest c = (Contest) e.Item.DataItem;

                // Time Remaining style
                string timeCSS = "remaining";

                if (c.IsContestActive)
                {
                    if (GetContestFacade.IsUserFollowingContest (c.ContestId, KanevaWebGlobals.CurrentUser.UserId))
                    {
                        followBtnText.Attributes.Add ("class", "unfollow");
                        followBtnText.InnerText = "Unfollow";
                        btnFollow.Title = "Unfollow";
                    }
                    else
                    {
                        followBtnText.Attributes.Add ("class", "follow");
                        followBtnText.InnerText = "Follow";
                        btnFollow.Title = "Follow";
                    }

                    btnFollow.Attributes.Add ("onclick", "Follow(" + c.ContestId.ToString () + ",'" + followBtnText.ClientID + "')");

                    if (c.TimeSpanRemaining.Days == 0)
                    {
                        timeCSS += " alert";
                    }
                    ((HtmlGenericControl) e.Item.FindControl ("divTimeRemainingFollow")).Attributes.Add ("class", timeCSS);
                }
                else
                {
                    btnFollow.Visible = false;
                }

                divDesc.InnerText = TruncateWithEllipsis ((Common.HtmlRemoval.StripTagsCharArray (Server.HtmlDecode (c.Description)).Replace ("&nbsp;", " ")), 136);

                // Has winner been selected?
                if (c.OwnerVoteWinnerEntryId > 0)
                {
                    divWinner.Visible = true;
                }
            }
        }

        protected void rptContestsVoted_ItemDataBound (Object Sender, RepeaterItemEventArgs e)
        {
            if ((e.Item.ItemType == ListItemType.Item) || (e.Item.ItemType == ListItemType.AlternatingItem))
            {
                HtmlGenericControl followBtnText = ((HtmlGenericControl) e.Item.FindControl ("followBtnTextVoted"));
                HtmlAnchor btnFollow = ((HtmlAnchor) e.Item.FindControl ("btnFollowVoted"));
                HtmlContainerControl divWinner = ((HtmlContainerControl) e.Item.FindControl ("divVoteWinner"));
                HtmlContainerControl divDesc = ((HtmlContainerControl) e.Item.FindControl ("divVotedDesc"));
                
                // If user has already voted on this contest, they can't vote again
                Contest c = (Contest) e.Item.DataItem;

                // Time Remaining style
                string timeCSS = "remaining";

                if (c.IsContestActive)
                {
                    if (GetContestFacade.IsUserFollowingContest (c.ContestId, KanevaWebGlobals.CurrentUser.UserId))
                    {
                        followBtnText.Attributes.Add ("class", "unfollow");
                        followBtnText.InnerText = "Unfollow";
                        btnFollow.Title = "Unfollow";
                    }
                    else
                    {
                        followBtnText.Attributes.Add ("class", "follow");
                        followBtnText.InnerText = "Follow";
                        btnFollow.Title = "Follow";
                    }

                    btnFollow.Attributes.Add ("onclick", "Follow(" + c.ContestId.ToString () + ",'" + followBtnText.ClientID + "')");

                    if (c.TimeSpanRemaining.Days == 0)
                    {
                        timeCSS += " alert";
                    }
                    ((HtmlGenericControl) e.Item.FindControl ("divTimeRemainingVoted")).Attributes.Add ("class", timeCSS);
                }
                else
                {
                    btnFollow.Visible = false;
                }

                divDesc.InnerText = TruncateWithEllipsis ((Common.HtmlRemoval.StripTagsCharArray (Server.HtmlDecode (c.Description)).Replace ("&nbsp;", " ")), 136);

                // Has winner been selected?
                if (c.OwnerVoteWinnerEntryId > 0)
                {
                    divWinner.Visible = true;
                }
            }
        }
    }
}