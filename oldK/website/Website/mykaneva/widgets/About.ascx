﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="About.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.mykaneva.widgets.About" %>
<%@ Register TagPrefix="CE" Namespace="CuteEditor" Assembly="CuteEditor" %>

<asp:Literal id="litJS" runat="server"></asp:Literal>

<script language="javascript">
    function toggle2() {
        var ele = $('divShow');
        var text = $('btnEdit');

        if (ele.style.display == "block") {
            ele.style.display = "none";
            text.innerText = "Edit";
        }
        else {
            ele.style.display = "block";
            text.innerText = "Hide";
        }

        adjustLineHeight();
    }

    function adjustLineHeight() {
        $j('.content span').each(function () {
            var x = $j(this).css('font-size');
            $j(this).css('line-height', x);
        });
    }
</script>


<div id="about">
    
    <div id="divPlayNow" class="content" runat="server" visible="false">
        <a class="btnmedium blue" href="#" id="btnMeet3D" runat="server">Play Now</a>
    </div> 
    																			
    <div class="content" id="divContent" runat="server"></div>	

	<div class="noData" id="divNoData" runat="server" visible="false">No information about this World has been entered.</div>


	<div class="editor" id="divEdit" runat="server" visible="true">

        <a id="btnEdit" clientidmode="Static" class="btnxsmall grey" href="javascript:void(0);" onclick="toggle2()" runat="server">Edit</a>

        <div id="divShow" style="display:none">


		<CE:Editor id="txtAbout" style="margin:0 0 5px 0;" AllowPasteHtml="False" 
			EditorOnPaste="PasteAsHTML" usephysicalformattingtags="true"
			ShowBottomBar="True" EnableContextMenu="False" BackColor="#ebebeb" 
			runat="server" enableobjectresizing="false" 
			enablestripscripttags="true" MaxTextLength="0" MaxHTMLLength="0" 
			width="620px" Height="300px" ShowHtmlMode="false" AutoConfigure="None"
			ConfigurationPath="~/CuteSoft_Client/CuteEditor/Configuration/AutoConfigure/profile.config" ></CE:Editor>

		<div id="divSave"><a id="lnkSave" class="btnxsmall grey" runat="server" onserverclick="lnkSave_Click">Save</a></div>

        </div>
	</div>

</div>
