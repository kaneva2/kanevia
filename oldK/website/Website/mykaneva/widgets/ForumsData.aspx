<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="ForumsData.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.mykaneva.widgets.ForumsData" %>

<asp:Literal id="litJS" runat="server"></asp:Literal>

<div id="forums">

    <div class="edit" id="forumEdit" runat="server">
        <a class="btnmedium grey" id="adminForum" runat="server">Forum Admin</a>
    </div> 
	<hr class="dataSeparator" />

	<asp:repeater id="repeaterForum" runat="server">
		<headertemplate>
			<div class="header">
				<div class="topics">Recent Topics Updated</div>
				<div class="poster">Poster</div>
				<div class="date">Post On</div>
				<div class="replies">Replies</div>
				<hr class="dataSeparator" />
			</div>
		</headertemplate>
		<itemtemplate>
			<div class="data">
				<div class="post"><asp:hyperlink id="hlForumPost" runat="server"/></div>
				<div class="image">
					<a href='<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "LastPostNameNoSpaces").ToString ())%>' >
						<img id="Img1" alt="image" runat="server" src='<%#GetProfileImageURL (DataBinder.Eval(Container.DataItem, "LastThumbnailSmallPath").ToString (), "sm", "M")%>' border="0" align="middle" width="20" height="20"/>
					</a>
				</div>
				<div class="date"><%# FormatDate(DataBinder.Eval (Container.DataItem, "CreatedDate")) %></div>
				<div class="replies"><%# DataBinder.Eval (Container.DataItem, "NumberOfReplies") %></div>
			</div>
		</itemtemplate>
		<separatortemplate><hr class="dataSeparator" /></separatortemplate>
	</asp:Repeater>


	<div class="noData" id="divNoData" runat="server" visible="false"></div>

 	<separatortemplate><hr class="dataSeparator" /></separatortemplate>

	<div id="links">
        <asp:hyperlink cssclass="btnmedium green" id="hlAddTopic" runat="server">Add New Topic</asp:hyperlink>
        <asp:hyperlink cssclass="btnmedium green" id="hlForum" runat="server">View Forums</asp:hyperlink>	       
    </div>
</div>

