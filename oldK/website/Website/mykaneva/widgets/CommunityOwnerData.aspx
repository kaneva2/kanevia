<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CommunityOwnerData.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.mykaneva.widgets.CommunityOwnerData" %>

<div>
	<div>avatar pic</div>
	<div>
		<div id="divUserName" runat="server">User Name</div>
		<div id="divDisplayName" runat="server">Real Name</div>
		<div id="divLocation" runat="server">City, St</div>
		<div id="divGender" runat="server">Gender</div>
		<div id="divAge" runat="server">Age</div>
		<div><a id="aPhotos" runat="server">See more photos</a></div>
	</div>
	<div id="divOnline" runat="server">online status</div>
	
	<div id="divModerators" runat="server">moderators list:
		<asp:repeater id="rpModerators" runat="server" >
			<itemtemplate><a href="<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "UserName").ToString ())%>"><%# DataBinder.Eval(Container.DataItem, "UserName").ToString () %></a>, </itemtemplate>
		</asp:repeater>
	</div>
	
	<br /><br />
</div>