///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.mykaneva.widgets
{
    public partial class ProfileAccess : KlausEnt.KEP.Kaneva.usercontrols.BaseUserControl
    {
        #region Declarations

        private int userId = 0;
        private bool userHasaccess = false;
        private bool isCommunityOwner = false;
        private bool isUserBlocked = false;
        private bool userHasAP = false;
        private bool userIsAdult = false;
        private bool userOver21 = false;
        private bool isMember = false;
        private bool isFriend = false;
        private bool isCommunityAdministrator = false;
        private Community community = new Community();


        #endregion

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                CheckUserAccess();
            }
        }
        #endregion

        #region Helper Functions

        private void GetUserData()
        {
            userId = KanevaWebGlobals.CurrentUser.UserId;
            isCommunityOwner = CommunityData.CreatorId.Equals(userId);
            userHasAP = KanevaWebGlobals.CurrentUser.HasAccessPass;
            userOver21 = KanevaWebGlobals.CurrentUser.Over21;
            userIsAdult = KanevaWebGlobals.CurrentUser.IsAdult;
            int privilegeId = 0;

            switch (CommunityData.CommunityTypeId)
            {
                case (int)CommunityType.USER:
                    privilegeId = (int)SitePrivilege.ePRIVILEGE.USER_PROFILE_ADMIN;
                    break;
                case (int)CommunityType.COMMUNITY:
                case (int)CommunityType.APP_3D:
                case (int)CommunityType.FAME:
                    privilegeId = (int)SitePrivilege.ePRIVILEGE.COMMUNITY_PROFILE_ADMIN;
                    break;
            }

            //check security level
            switch (KanevaWebGlobals.CheckUserAccess(privilegeId))
            {
                case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_FULL:
                case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_READ:
                    isCommunityAdministrator = true;
                    break;
                case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_NONE:
                    break;
                default:
                    break;
            }

        }

        private void ResetAccesWarnings()
        {
            divUnauthorized.Visible = false;
            divAPProfile.Visible = false;
            divAPCommunity.Visible = false;
            divMembersOnly.Visible = false;
            divOver21.Visible = false;
            divCommPrivate.Visible = false;
            divProfilePrivate.Visible = false;
            divFriend.Visible = false;
        }

        #endregion

        #region Functions
        /// <summary>
        /// CheckUserAccess - checks if user should have access or not
        /// returns true or false
        /// </summary>
        /// return bool
        public void CheckUserAccess()
        {
            // reset page for processing
            ResetAccesWarnings();
            SetButtonURLs();

            //if user is authenticated process their access rights
            //otherwise display not logged in message
            if (Request.IsAuthenticated && (CommunityData.CommunityId > 0))
            {
                //get user information
                GetUserData();

                    // Check if the visiting user is blocked from viewing this profile
                if (!IsUserAdministrator () && !isCommunityOwner)
                {
                    if (GetUserFacade.IsUserBlocked (CommunityData.CreatorId, KanevaWebGlobals.CurrentUser.UserId))
                    {
                        userHasaccess = false;
                        isUserBlocked = true;
                        return;
                    }
                }
                //skip all this if you they are an admin or CSR
                if (!isCommunityAdministrator && !isCommunityOwner && !IsUserAdministrator() && !UsersUtility.IsUserCSR())
                {
                    //check access and set community type related values
                    switch (CommunityData.CommunityTypeId)
                    {
                        case (int)CommunityType.USER:
                            CheckUserProfileAccess();
                            break;
                        case (int)CommunityType.COMMUNITY:
                        case (int)CommunityType.APP_3D:
                        case (int)CommunityType.HOME:
                        case (int)CommunityType.FAME:
                            CheckCommunityAccess ();
                            break;
                    }
                }
                else
                {
                    //set the access state
                    userHasaccess = true;
                }
            }
            else  // if user is not logged in, check to see if they can view profile
            {
                bool isProfile = (CommunityData.CommunityTypeId == (int)CommunityType.USER);

                //check for access pass requirement
                if (AccessPassRequired ())
                {
                    if (isProfile)
                    {
                        divAPProfile.Visible = true;
                    }
                    else
                    {
                        divAPCommunity.Visible = true;
                    }
                    userHasaccess = false;
                }
                else if (community.IsPublic.Equals ("N"))
                {
                    if (isProfile)
                    {
                        divFriend.Visible = true;
                    }
                    else
                    {
                        divMembersOnly.Visible = true;
                    }
                    userHasaccess = false;
                }
                //check to see if community is private
                else if (community.IsPublic.Equals ("I"))
                {
                    if (isProfile)
                    {
                        divProfilePrivate.Visible = true;
                    }
                    else
                    {
                        divCommPrivate.Visible = true;
                    }
                    userHasaccess = false;
                }
                else if (isProfile && (community.OwnerAge < 18))
                {
                    userHasaccess = false;
                    Response.Redirect (GetLoginURL ());
                }
                else if (Over21Required ())
                {
                    divOver21.Visible = true;
                    userHasaccess = false;
                }
                else
                {
                    userHasaccess = true;
                }
            }
        }

        /// <summary>
        /// CheckUserProfile
        /// </summary>
        private void CheckUserProfileAccess()
        {
            //see if they are friends or not
            if (!isCommunityOwner)
            {
                isFriend = (new UserFacade()).AreFriends(community.CreatorId, this.userId);
            }

            //check for access pass requirement
            if (community.IsPublic.Equals("N") && !isCommunityOwner && !isFriend)
            {
                divFriend.Visible = true;
                userHasaccess = false;
            }
            //check to see if community is private
            //per Billy, also show profile as private if user is not an adult and profile requires AP
            else if (((AccessPassRequired() && !userIsAdult) || community.IsPublic.Equals("I")) && !isCommunityOwner)
            {
                divProfilePrivate.Visible = true;
                userHasaccess = false;
            }
            //everything is a go
            else
            {
                userHasaccess = true;
            }
        }

        /// <summary>
        /// CheckCommunity
        /// </summary>
        private void CheckCommunityAccess()
        {
            //set membership status
            CommunityMember communityMember = new CommunityFacade().GetCommunityMember(community.CommunityId, userId);
            isMember = (communityMember.StatusId == (UInt32)CommunityMember.CommunityMemberStatus.ACTIVE);

            //check for access pass requirement
            if (AccessPassRequired() && !isCommunityOwner && !userHasAP)
            {
                divAPCommunity.Visible = true;
                userHasaccess = false;
            }
            //check for over 21
            else if (Over21Required() && !isCommunityOwner && !userOver21)
            {
                divOver21.Visible = true;
                userHasaccess = false;
            }
            //check for user as member 
            else if (community.IsPublic.Equals("N") && !isCommunityOwner && !isMember)
            {
                divMembersOnly.Visible = true;
                userHasaccess = false;
            }
            //check to see if community is private
            else if (community.IsPublic.Equals("I") && !isCommunityOwner)
            {
                divCommPrivate.Visible = true;
                userHasaccess = false;
            }
            //everything is a go
            else
            {
                userHasaccess = true;
            }
        }

        /// <summary>
        /// Check3DAppAccess
        /// </summary>
        private void Check3DAppAccess()
        {
            //check normal community settings
            CheckCommunityAccess();

            //check any addition settings
        }

        private void SetButtonURLs()
        {
            getAcessPass.HRef = ResolveUrl("~/mykaneva/passDetails.aspx?pass=true&passID=" + KanevaGlobals.AccessPassPromotionID);
            
            
            getAccessPassComm.HRef = ResolveUrl ("~/mykaneva/passDetails.aspx?pass=true&passID=" + KanevaGlobals.AccessPassPromotionID);
            goBackCommAP.Attributes.Add ("onclick", "javacript:history.go(" + (IsPostBack ? "-2" : "-1") + ");return false;"); 


            sendFriendRequest.Attributes.Add("onclick", "javascript:if (confirm('Send a friend request to this member?') ) { SendFriendRequest(); } else { return false; }");
            sendMemberRequest.Attributes.Add("onclick", "javascript:if (confirm('Join this World?') ) { JoinCommunity(); return false; } else { return false; }");
            
            goBackFriend.Attributes.Add("onclick", "javacript:history.go(" + (IsPostBack ? "-2" : "-1") + ");return false;");
            goBackPrivate.Attributes.Add("onclick", "javacript:history.go(" + (IsPostBack ? "-2" : "-1") + ");return false;");
            sendMessagePPrivate.HRef = ResolveUrl("~/myKaneva/newMessage.aspx?userId=" + this.community.CreatorId);
            sendMessageCPrivate.HRef = ResolveUrl("~/myKaneva/newMessage.aspx?userId=" + this.community.CreatorId);

            goBackMember.Attributes.Add("onclick", "javacript:history.go(" + (IsPostBack ? "-2" : "-1") + ");return false;");
            goBackCommPrivate.Attributes.Add("onclick", "javacript:history.go(" + (IsPostBack ? "-2" : "-1") + ");return false;");
            goBackOver21.Attributes.Add("onclick", "javacript:history.go(" + (IsPostBack ? "-2" : "-1") + ");return false;");
            signIn.Attributes.Add("onclick", "location.href='" + GetLoginURL() + "';return false;");
        }

        public void SendFriendRequest()
        {
            // Add them as a friend
            if (GetUserFacade.InsertFriendRequest(userId, CommunityData.CreatorId,
                Page.Request.CurrentExecutionFilePath, Global.RequestRabbitMQChannel()).Equals(0))
            {
                MailUtilityWeb.SendFriendRequestEmail(userId, CommunityData.CreatorId);
            }
            Response.Redirect(ResolveUrl("~/people/people.kaneva"),true);
        }

        public void JoinCommunity()
        {
            Response.Redirect(ResolveUrl("~/community/commJoin.aspx?communityId=" + CommunityData.CommunityId + "&join=Y"));
        }

        #endregion


        #region Attributes

        private bool Over21Required()
        {
            return CommunityData.Over21Required && CommunityData.IsPersonal == 1;
        }

        private bool AccessPassRequired()
        {
            return CommunityData.IsAdult.Equals("Y");
        }

        /// <summary>
        /// UserHasAccess - function that returns to the parent page or control whether the user has access to the community or not
        /// </summary>
        /// <returns></returns>
        public bool UserHasAccess()
        {
            return this.userHasaccess;
        }

        public bool UserIsAdministrator()
        {
            return isCommunityAdministrator;
        }

        public bool IsUserBlocked()
        {
            return this.isUserBlocked;
        }

        public Community CommunityData
        {
            get 
            { 
                return this.community; 
            }
            set 
            {
                this.community = value; 
            }
        }

        #endregion

        #region Events

        protected void SendFriendRequest_Click(object sender, EventArgs e)
        {
            SendFriendRequest();
        }

        protected void JoinCommunity_Click(object sender, EventArgs e)
        {
            JoinCommunity();
        }

        #endregion
    }
}
