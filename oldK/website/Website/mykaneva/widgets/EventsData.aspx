<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="EventsData.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.mykaneva.widgets.EventsData" %>

<asp:Literal id="litJS" runat="server"></asp:Literal>

<div id="events">

<div class="dlContainer" id="divDataList" runat="server">

    <div class="edit" id="eventsEdit" runat="server" visible="false">
        <a class="btnxsmall grey" id="addEvent" runat="server"><span>Add Event</span></a>        
	    <hr class="dataSeparator" />
    </div>
														 
	<asp:repeater visible="true" runat="server" id="rptEventsList" onitemdatabound="rptEventsList_ItemDataBound">	
		<headertemplate>									
			<div class="header">
				<div class="title">Event</div>
				<div class="time">Invites</div>
			</div>
		</headertemplate>
		<itemtemplate>
			<div class="dataRow">
				<div class="title">
					<a href="../mykaneva/events/eventDetail.aspx?event=<%# DataBinder.Eval (Container.DataItem, "EventId") %>"><%# DataBinder.Eval (Container.DataItem, "Title") %></a> hosted by <a id="eventHostname" style="font-weight:normal;font-size:1.1em;" href="" runat="server">&nbsp;</a>
					<p class="time"><%# FormatStartTime (DataBinder.Eval (Container.DataItem, "StartTime"))%></p>
					<p><%# TruncateWithEllipsis (DataBinder.Eval (Container.DataItem, "Details").ToString (), 180) %></p>
				</div>
				<div class="invites">
					<div><div><span id="invitesAccepted" runat="server">&nbsp;</span></div>Accepted</div>
					<div><div><span id="invitesDeclined" runat="server">&nbsp;</span></div>Declined</div>
					<div><div><span id="invitesOutstanding" runat="server">&nbsp;</span></div>Outstanding</div>
				</div>
			</div>
		</itemtemplate>
	</asp:repeater>	 
		   
</div>

<div class="rptContainer" id="divRepeater" runat="server">

	<asp:repeater id="rptEvents" runat="server" onitemdatabound="rptEvents_ItemDataBound" >
		<headertemplate>
			<table class="event">
		</headertemplate>
		<itemtemplate>
				<tr>
					<td><asp:HyperLink ID="hlEventName" runat="server" class="eventtitle"></asp:HyperLink></td>
					<td class="date"><asp:Literal ID="litEventWhen" runat="server"></asp:Literal></td>
				</tr>
		</itemtemplate>
		<footertemplate>
			</table>
		</footertemplate>	
	</asp:repeater>	

</div>

<div class="noData" id="divNoData" runat="server" visible="false"></div>

</div>	