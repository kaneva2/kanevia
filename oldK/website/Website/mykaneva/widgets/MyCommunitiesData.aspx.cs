///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using log4net;

namespace KlausEnt.KEP.Kaneva.mykaneva.widgets
{
    public partial class MyCommunitiesData : BasePage
    {
        #region Declerations

        private int communityId = 0;
        private int communityTypeId = (int)CommunityType.COMMUNITY;
        private int userId = 0;
        private string orderBy = "name DESC";
        private int pageSize = 15;
        private bool showGrid = true;
        private string filter = "";
        private int pageNum = 1;

        private int profileOwnerId = 0;
        private bool isProfileOwner = false;
        private bool isProfileAdmin = false;

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #endregion Declerations

        #region Page Load

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //clears local cache so data is always new
                Response.ContentType = "text/plain";
                Response.CacheControl = "no-cache";
                Response.AddHeader("Pragma", "no-cache");
                Response.Expires = -1;

                GetRequestValues();
                CheckUserAccess();
                BindData();
                SetEditOptions();
            }
            catch (Exception ex)
            {
                m_logger.Error("Error loading Communities widget. ", ex);

                divNoData.InnerText = "We're sorry but the data could not be retrieved.";
                divNoData.Visible = true;

                // Always call this method so the totalRecords value will get updated
                // and the pager will be hidden if it is not needed
                litJS.Text = "<script type=\"text/javascript\">SetAjaxPager(1,0);</script>";
            }
        }

        #endregion Page Load


        #region Functions

        private void BindData()
        {
            // If user does not have AP, then don't show AP communities
            string strFilter = (KanevaWebGlobals.CurrentUser.HasAccessPass || isProfileOwner) ? "" : " is_adult='N' ";
            // Exclude zone purachase holder communities
            strFilter += (strFilter.Length > 0 ? " AND " : "") + "place_type_id < 99"; 

            int[] communityTypeIds = { Convert.ToInt32 (CommunityType.APP_3D), Convert.ToInt32 (CommunityType.COMMUNITY), Convert.ToInt32 (CommunityType.HOME), Convert.ToInt32 (CommunityType.FAME) };
            PagedList<Community> communities = GetCommunityFacade.GetUserCommunities (ProfileOwnerId, communityTypeIds, OrderBy, strFilter, PageNum, PageSize, false, Filter);

            if (communities.TotalCount > 0)
            {
                if (ShowGrid)
                {
                    dlCommunities.DataSource = communities;
                    dlCommunities.DataBind();
                }
                else
                {
                    rptCommunities.DataSource = communities;
                    rptCommunities.DataBind();
                }
            }
            else
            {
                divNoData.Visible = true;
                divNoData.InnerHtml = "<div>No communities found.</div>";
            }

            // Always call this method so the totalRecords value will get updated
            // and the pager will be hidden if it is not needed
            if (ShowGrid)
            {
                litJS.Text = "<script type=\"text/javascript\">SetAjaxPager(" + PageSize.ToString() + "," + communities.TotalCount.ToString() + ");</script>";
            }
        }

        private void GetRequestValues()
        {
            // Get the community Id
            if (Request["communityId"] != null)
            {
                try
                {
                    CommunityId = Convert.ToInt32(Request["communityId"]);
                }
                catch (Exception exc)
                {
                    m_logger.Error("Error in GetRequestValues", exc);
                }
            }

            // Get the user Id from the parameters 
            if (Request["profileOwnerId"] != null)
            {
                try
                {
                    ProfileOwnerId = Convert.ToInt32(Request["profileOwnerId"]);
                }
                catch (Exception exc)
                {
                    m_logger.Error("Error in GetRequestValues", exc);
                }
            }
            else
            {
                try
                {
                    ProfileOwnerId = GetCommunityFacade.GetCommunity(CommunityId).CreatorId;
                }
                catch (Exception exc)
                {
                    m_logger.Error("Error in GetRequestValues", exc);
                }
            }

            // Get the community filter value
            if (Request["filter"] != null && Request["filter"].ToString().Length > 0)
            {
                try
                {
                    Filter =  Request["filter"].ToString();
                }
                catch (Exception exc)
                {
                    m_logger.Error("Error in GetRequestValues", exc);
                }
            }

            // Get the Show Grid value
            if (Request["showgrid"] != null)
            {
                try
                {
                    ShowGrid = Convert.ToBoolean(Request["showgrid"]);
                }
                catch (Exception exc)
                {
                    m_logger.Error("Error in GetRequestValues", exc);
                }
            }

            // Get the type of community to select
            if (Request["communityType"] != null)
            {
                try
                {
                    CommunityTypeId = Convert.ToInt32(Request["communityType"]);
                }
                catch (Exception exc)
                {
                    m_logger.Error("Error in GetRequestValues", exc);
                }
            }

            // Get orderby field
            if (Request["orderby"] != null)
            {
                try
                {
                    if (Request["orderby"] != null)
                    {
                        OrderBy = Request["orderby"].ToString();
                        switch (OrderBy.ToLower())
                        {
                            case "raves":
                                OrderBy = "number_of_diggs DESC";
                                break;
                            case "views":
                                OrderBy = "number_of_views DESC";
                                break;
                            case "rand":
                                // This needs to be randomized in code, not db!!!
                                OrderBy = "c.last_update DESC";
                                break;
                            case "date":
                                OrderBy = "added_date DESC";
                                break;
                            default:
                                OrderBy = "name ASC";
                                break;
                        }
                    }
                }
                catch (Exception exc)
                {
                    m_logger.Error("Error in GetRequestValues", exc);
                }
            }

            // Get items per page count
            if (Request["pagesize"] != null)
            {
                try
                {
                    PageSize = Convert.ToInt32(Request["pagesize"]);
                }
                catch (Exception exc)
                {
                    m_logger.Error("Error in GetRequestValues", exc);
                }
            }

            // Get the current page number
            if (Request["p"] != null)
            {
                try
                {
                    PageNum = Convert.ToInt32(Request["p"]);
                }
                catch (Exception exc)
                {
                    m_logger.Error("Error in GetRequestValues", exc);
                }
            }
        }

        #endregion Functions

        #region Helper Functions

        private void SetEditOptions()
        {
            if (ShowGrid && (isProfileOwner || isProfileAdmin))
            {
                communitiesEdit.Visible = true;

                //set callout
                divNoData.InnerHtml += "<div>Would you like to <a href=\"" + ResolveUrl("~/community/channel.kaneva") + "\" alt=\"Join a Community\" >Join a community</a>?</div>";
            }
            else
            {
                communitiesEdit.Visible = false;
            }
        }

        /// <summary>
        /// Checks the current user's access level
        /// </summary>
        private void CheckUserAccess()
        {
            int privilegeId = 0;

            //get the users Id
            userId = GetUserId();

            //see if they are the profile/community owner
            isProfileOwner = userId.Equals(profileOwnerId);

            if (!isProfileOwner)
            {
                //get the appropriate privilege id based on the community type
                switch (CommunityTypeId)
                {
                    case (int)CommunityType.USER:
                        privilegeId = (int)SitePrivilege.ePRIVILEGE.USER_PROFILE_ADMIN;
                        break;
                    case (int)CommunityType.COMMUNITY:
                    case (int)CommunityType.APP_3D:
                        privilegeId = (int)SitePrivilege.ePRIVILEGE.COMMUNITY_PROFILE_ADMIN;
                        break;
                }

                //check the privilege level to see if they are an admin
                this.isProfileAdmin = HasWritePrivileges(privilegeId);
            }
        }


        #endregion

        #region Event Handlers

        /// <summary>
        /// Click event for edit this widget 
        /// </summary>
        protected void editCommunities_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/bookmarks/channelTags.aspx");
        }

        protected void rptCommunities_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                Label lblMemberType = (Label)e.Item.FindControl("lblMemberType2");
                //Label mediaCount = (Label)e.Item.FindControl("mediaCount2");

                //mediaCount.Text = Convert.ToString(((Community)e.Item.DataItem).Stats.MusicCount + ((Community)e.Item.DataItem).Stats.PatternCount +
                //((Community)e.Item.DataItem).Stats.PhotoCount + ((Community)e.Item.DataItem).Stats.TvCount +
                //((Community)e.Item.DataItem).Stats.VideoCount + ((Community)e.Item.DataItem).Stats.WidgetCount);

                List<CommunityMember> members = ((List<CommunityMember>)DataBinder.Eval(e.Item.DataItem, "MemberList"));
                switch (((CommunityMember)members[0]).AccountTypeId)
                {
                    case (int)CommunityMember.CommunityMemberAccountType.OWNER:
                            lblMemberType.Text = "Owner";
                            break;
                    case (int)CommunityMember.CommunityMemberAccountType.MODERATOR:
                            lblMemberType.Text = "Moderator";
                            break;
                    case (int)CommunityMember.CommunityMemberAccountType.SUBSCRIBER:
                            lblMemberType.Text = "Belong to";
                            break;
                }
            }
        }

        protected void dlCommunities_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                Label lblMemberType = (Label)e.Item.FindControl("lblMemberType");
                //Label mediaCount = (Label)e.Item.FindControl("mediaCount");

                //mediaCount.Text = Convert.ToString(((Community)e.Item.DataItem).Stats.MusicCount + ((Community)e.Item.DataItem).Stats.PatternCount +
                //((Community)e.Item.DataItem).Stats.PhotoCount + ((Community)e.Item.DataItem).Stats.TvCount +
                //((Community)e.Item.DataItem).Stats.VideoCount + ((Community)e.Item.DataItem).Stats.WidgetCount);

                List<CommunityMember> members = ((List<CommunityMember>)DataBinder.Eval(e.Item.DataItem, "MemberList"));
                switch (((CommunityMember)members[0]).AccountTypeId)
                {
                    case (int)CommunityMember.CommunityMemberAccountType.OWNER:
                        lblMemberType.Text = "Owner";
                        break;
                    case (int)CommunityMember.CommunityMemberAccountType.MODERATOR:
                        lblMemberType.Text = "Moderator";
                        break;
                    case (int)CommunityMember.CommunityMemberAccountType.SUBSCRIBER:
                        lblMemberType.Text = "Belong to";
                        break;
                }
            }
        }

        #endregion Event Handlers

        #region Properties

        private int CommunityTypeId
        {
            set { this.communityTypeId = value; }
            get { return this.communityTypeId; }
        }
        private int CommunityId
        {
            set { this.communityId = value; }
            get { return this.communityId; }
        }
        private int ProfileOwnerId
        {
            set { this.profileOwnerId = value; }
            get { return this.profileOwnerId; }
        }
        private int PageNum
        {
            set { this.pageNum = value; }
            get { return this.pageNum; }
        }
        private int PageSize
        {
            set { this.pageSize = value; }
            get { return this.pageSize; }
        }
        private string Filter
        {
            set { this.filter = value; }
            get { return this.filter; }
        }
        private bool ShowGrid
        {
            set { this.showGrid = value; }
            get { return this.showGrid; }
        }
        private string OrderBy
        {
            set { this.orderBy = value; }
            get { return this.orderBy; }
        }

        #endregion Properties

        #region Web Form Designer generated code

        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new EventHandler(this.Page_Load);
        }

        #endregion

    }
}
