<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModuleChannelInfoView.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.mykaneva.widgets.ModuleChannelInfoView" %>
<table cellpadding="0" cellspacing="0" width="100%" id="widgetContent">
	<!-- SINGLE PHOTO-->
	<tr>
		<td width="100%" id="pageOpacity">
		<div id="widgetBorder">
		<b class="outerFrame" id="outerFrame">
		<b class="outerFrame1"><b></b></b>
		<b class="outerFrame2"><b></b></b>
		<b class="outerFrame3"></b>
		<b class="outerFrame4"></b>
		<b class="outerFrame5"></b>
		</b> 
		<div class="outerFrame_content" id="outerFrame_content">
		<!-- Your Content Goes Here -->
			<table cellpadding="0" cellspacing="0" border="0" width="100%">
				<tr>
					<td>
						<div id="widgetHeader">
							<b class="innerFrame">
							<b class="innerFrame1"><b></b></b>
							<b class="innerFrame2"><b></b></b>
							<b class="innerFrame3"></b>
							<b class="innerFrame4"></b>
							<b class="innerFrame5"></b>
							</b> 
							<div class="innerFrameTitle_content">
							<!-- Your Content Title Goes Here -->
								<table cellpadding="0" cellspacing="0" width="100%">
									<tr>
										<td class="widgetTitle">
											<asp:label id="lblTitle" runat="server"/>
										</td>
										<td align="right">
											<table cellpadding="0" cellspacing="0" border="0" id="tblEdit" runat="server">
												<tr>
													<td nowrap>
														<asp:linkbutton id="lbEdit" runat="server" cssclass="widgetEditButton" title="edit this widget">edit</asp:linkbutton><asp:linkbutton id="lbDelete" runat="server" title="remove this widget" cssclass="widgetRemoveButton">X</asp:linkbutton>
													</td>
												</tr>
											</table>											
										</td>
									</tr>
								</table>
							</div>
							<b class="innerFrame">
							<b class="innerFrame5"></b>
							<b class="innerFrame4"></b>
							<b class="innerFrame3"></b>
							<b class="innerFrame2"><b></b></b>
							<b class="innerFrame1"><b></b></b>
							</b>
						</div> 
					</td>
				</tr>
				<tr>
					<td><img runat="server" src="~/images/spacer.gif" border="0" width="1" height="1" id="Img4"/></td>
				</tr>				
				<tr>
					<td valign="top" height="100%">
						<div id="widgetBody">
							<b class="innerFrame">
							<b class="innerFrame1"><b></b></b>
							<b class="innerFrame2"><b></b></b>
							<b class="innerFrame3"></b>
							<b class="innerFrame4"></b>
							<b class="innerFrame5"></b>
							</b> 
							<div class="innerFrame_content">
							<!-- Your Content Goes Here -->
								<table cellpadding="0" cellspacing="0" border="0" width="100%">
									<!--STARTS AN ELEMENT--->																			
									<tr>
										<!--thumb section--->
										<td valign="top" width="100%" id="tdPicture" runat="server">
											<table border="0" cellpadding="0" cellspacing="0" width="147" align="center">
												<tr>
													<td class="boxWhiteTopLeft"><img runat="server" src="~/images/spacer.gif" width="4" height="4" id="Img11"/></td>
													<td class="boxWhiteTop" ><img runat="server" src="~/images/spacer.gif" width="4" height="1" id="Img12"/></td>
													<td class="boxWhiteTopRight"><img runat="server" src="~/images/spacer.gif" width="4" height="4" id="Img13"/></td>
												</tr>
												<tr>
													<td class="boxWhiteLeft" ><img runat="server" src="~/images/spacer.gif" width="1" height="4" id="Img14"/></td>
													<td class="boxWhiteContent" align="center" valign="middle">
														<table cellpadding="0" cellspacing="0" class="widgetMyPhoto">
															<tr>
																<td>
																	<div style="width: 130px; height: 98px; background-color: #FFFFFF; overflow: hidden; vertical-align: middle; text-align:center; border: 1px solid #e0e0e0;">
																		<img runat="server" id="imgAvatar" src="~/images/KanevaIcon01.gif" border="0" />
																	</div>
																</td>
															</tr>
														</table>
													</td>
													<td class="boxWhiteRight"><img runat="server" src="~/images/spacer.gif" width="1" height="4" id="Img15"/></td>
												</tr>
												<tr>
													<td class="boxWhiteBottomLeft" ><img runat="server" src="~/images/spacer.gif" width="4" height="4" id="Img16"/></td>
													<td class="boxWhiteBottom"><img runat="server" src="~/images/spacer.gif" width="4" height="1" id="Img17"/></td>
													<td class="boxWhiteBottomRight"><img runat="server" src="~/images/spacer.gif" width="4" height="4" id="Img18"/></td>
												</tr>
											</table>	
										</td>
										<td>
										<asp:repeater id="rpStats" runat="server" >
											<itemtemplate>
												<div class="floatStats">
													<table cellpadding="0" cellspacing="0" align="center" border="0" width="100%">
														<tr>
															<td class="widgetTextBold" nowrap width="90" style="text-align:left;padding-right:10px;"><%# DataBinder.Eval(Container.DataItem, "header") %></td>
															<td class="widgetText" style="text-align:left;padding-right:10px;"><%# DataBinder.Eval(Container.DataItem, "body") %></td>
														</tr>
														<tr><td colspan="3"><img id="Img1" runat="server" src="~/images/spacer.gif" width="1" height="5" /></td></tr>
													</table>														
												</div>
											</itemtemplate>			
										</asp:repeater>
										
										</td>
										<!--end thumb section-->
									</tr>
									<tr><td colspan="2"><img id="Img2" runat="server" src="~/images/spacer.gif" border="0" width="1" height="5"/></td></tr>
									<tr>
										<td colspan="2" align="center">
											<table border="0" cellpadding="2" cellspacing="0">
												<tr>
													<td valign="top" align="left" nowrap width="130" class="widgetTextBold" >
														Owner:
													</td>	
													<td align="left" class="widgetText" >
														<asp:hyperlink id="hlOwner" runat="server" />
													</td>
												</tr>
											<asp:panel id="pnlModerators" runat="server">
												<tr>
													<td valign="top" align="left" nowrap width="130" class="widgetTextBold" >
														Moderators:
													</td>	
													<td align="left" class="widgetText" >
														<asp:repeater id="rpModerators" runat="server" >
															<itemtemplate><asp:hyperlink id="hlModerator" runat="server"/> </itemtemplate>
														</asp:repeater>
													</td>
												</tr>
											</asp:panel>
											</table>
										</td>
									</tr>
									<tr><td colspan="3"><img id="Img3" runat="server" src="~/images/spacer.gif" border="0" width="1" height="5"/></td></tr>
									<!--END ONE ELEMENT-->																																																			
								</table>
							</div>
							<b class="innerFrame">
							<b class="innerFrame5"></b>
							<b class="innerFrame4"></b>
							<b class="innerFrame3"></b>
							<b class="innerFrame2"><b></b></b>
							<b class="innerFrame1"><b></b></b>
							</b>
						</div> 
					</td>
				</tr>
			</table>
		</div>
		<b class="outerFrame">
		<b class="outerFrame5"></b>
		<b class="outerFrame4"></b>
		<b class="outerFrame3"></b>
		<b class="outerFrame2"><b></b></b>
		<b class="outerFrame1"><b></b></b>
		</b>
		</div> 
		</td>
	</tr>
	<!-- END SINGLE PHOTO -->
							
</table>

<br>