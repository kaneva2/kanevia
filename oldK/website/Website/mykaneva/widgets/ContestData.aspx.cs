///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using log4net;

namespace KlausEnt.KEP.Kaneva.mykaneva.widgets
{
    public partial class ContestData : BasePage
    {
        #region Declerations

        private string orderBy = "created_date DESC";
        private string filterBy = "";
        private int pageSize = 20;
        private int pageNum = 1;
        private SortLink currentSort = SortLink.Newest;

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

        #endregion Declerations
        
        protected void Page_Load (object sender, EventArgs e)
        {
            // They must be logged in
            if (!Request.IsAuthenticated)
            {
                Response.Redirect (GetLoginURL ());
                return;
            }

            try
            {
                // Load JS files
                RegisterJavaScript ("jscript/doodad/ContestFollow.js");

                //clears local cache so data is always new
                Response.ContentType = "text/html";
                Response.CacheControl = "no-cache";
                Response.AddHeader ("Pragma", "no-cache");
                Response.Expires = -1;

                GetRequestValues ();
                
                // Show the contests
                BindData ();

                // Show Banner?
                ConfigureNotificationBanner ();
            }
            catch (Exception ex)
            {
                m_logger.Error ("Error loading Forums widget. ", ex);

                divNoData.InnerText = "We're sorry but the data could not be retrieved.";
                divNoData.Visible = true;

                // Always call this method so the totalRecords value will get updated
                // and the pager will be hidden if it is not needed
                litJS.Text = "<script type=\"text/javascript\">SetAjaxPager(1,0);</script>";
            }
        }

        public void BindData ()
        {
            int PageSize = 10;
            string filter = "status > 0 AND archived = 0";
                                                                
            PagedList<Contest> contests = GetContestFacade.GetContests (0, OrderBy, filter, PageNum, PageSize);

            if (contests.TotalCount > 0)
            {
                rptContests.DataSource = contests;
                rptContests.DataBind ();
            }
            else
            {
                divNoData.Visible = true;
                // Always call this method so the totalRecords value will get updated
                // and the pager will be hidden if it is not needed
                litJS.Text = "<script type=\"text/javascript\">SetAjaxPager(1,0);</script>";

                return;
            }
            
            // Always call this method so the totalRecords value will get updated
            // and the pager will be hidden if it is not needed
            string total = contests.TotalCount > PageSize ? contests.TotalCount.ToString () : "0"; 
            litJS.Text = "<script type=\"text/javascript\">SetAjaxPager(" + PageSize.ToString () + "," + total + ");</script>";

            SetSelectedLink ();
        }

        private void ConfigureNotificationBanner ()
        {
            try
            {
                // Get list of contests that user needs to select winner
                PagedList<Contest> pdlNeed = GetContestFacade.GetContestsNeedWinner (KanevaWebGlobals.CurrentUser.UserId, OrderBy, "NOW() < DATE_ADD(end_date, INTERVAL 3 DAY)", 1, 10);
                imgnotify.Src = "";
                if (pdlNeed.TotalCount > 0)
                {
                    rptContestsNeedWinner.DataSource = pdlNeed;
                    rptContestsNeedWinner.DataBind ();
                    usernotify.Visible = true;
                    imgnotify.Src = ResolveUrl ("~/images/contests/alert_51x48.png");
                }

                // Get list of contests user has won -- only show for 7 days past contest end date
                PagedList<Contest> pdlWon = GetContestFacade.GetContestsUserWon (KanevaWebGlobals.CurrentUser.UserId, OrderBy, "NOW() < DATE_ADD(end_date, INTERVAL 7 DAY)", 1, 10);
                if (pdlWon.TotalCount > 0)
                {
                    rptContestsWon.DataSource = pdlWon;
                    rptContestsWon.DataBind ();
                    usernotify.Visible = true;
                    imgnotify.Src = ResolveUrl ("~/images/contests/contests_trophy_44x63.png");
                }
            }
            catch (Exception ex)
            {
                m_logger.Error ("Error configuring notification banner. ", ex);
                usernotify.Visible = false;
            }
        }

        private void GetRequestValues ()
        {
            // Get orderby field
            try
            {
                if (Request["orderby"] != null)
                {
                    OrderBy = Request["orderby"].ToString ();
                    switch (OrderBy.ToLower ())
                    {
                        case "remaining":   //Time Left
                            OrderBy = "is_active DESC, end_date ASC";
                            CurrentSort = SortLink.Time_Remaining;
                            break;
                        case "amount":      //Top Prizes
                            OrderBy = "is_active DESC, prize_amount DESC";
                            CurrentSort = SortLink.Amount;
                            break;
                        case "hot":         //Hot Contests
                            OrderBy = "is_active DESC, num_entries DESC";
                            CurrentSort = SortLink.Activity;
                            break;
                        case "createdate":  //Most Recent
                        default:
                            OrderBy = "is_active DESC, created_date DESC";
                            CurrentSort = SortLink.Newest;
                            break;
                    }
                }
            }
            catch { }

            // Get items per page count
            if (Request["pagesize"] != null)
            {
                try
                {
                    PageSize = Convert.ToInt32 (Request["pagesize"]);
                }
                catch { }
            }

            // Get the current page number
            if (Request["p"] != null)
            {
                try
                {
                    PageNum = Convert.ToInt32 (Request["p"]);
                }
                catch { }
            }
        }

        private void SetSelectedLink ()
        {
            sortActivity.Attributes.Add ("class", "sort");
            sortAmount.Attributes.Add ("class", "sort");
            sortTimeRemaining.Attributes.Add ("class", "sort");
            sortNewest.Attributes.Add ("class", "sort");
            switch (CurrentSort)
            {
                case SortLink.Activity:
                    sortActivity.Attributes.Add ("class", " selected");
                    sortActivity.HRef = "";
                    sortActivity.Attributes.Remove ("onclick");
                    break;
                case SortLink.Amount:
                    sortAmount.Attributes.Add ("class", " selected");
                    sortAmount.HRef = "";
                    sortAmount.Attributes.Remove ("onclick"); 
                    break;
                case SortLink.Time_Remaining:
                    sortTimeRemaining.Attributes.Add ("class", " selected");
                    sortTimeRemaining.HRef = "";
                    sortTimeRemaining.Attributes.Remove ("onclick");
                    break;
                case SortLink.Newest:
                default:
                    sortNewest.Attributes.Add ("class", " selected");
                    sortNewest.HRef = "";
                    sortNewest.Attributes.Remove ("onclick");
                    break;
            }
        }

        protected void rptContests_ItemDataBound (Object Sender, RepeaterItemEventArgs e)
        {
            if ((e.Item.ItemType == ListItemType.Item) || (e.Item.ItemType == ListItemType.AlternatingItem))
            {
                HtmlGenericControl followBtnText = ((HtmlGenericControl) e.Item.FindControl ("followBtnText"));
                HtmlAnchor btnFollow = ((HtmlAnchor) e.Item.FindControl ("btnFollow"));
                HtmlContainerControl divWinner = ((HtmlContainerControl) e.Item.FindControl ("divAllContestsWinner"));
                HtmlContainerControl divDesc = ((HtmlContainerControl) e.Item.FindControl ("divDesc"));

                // If user has already voted on this contest, they can't vote again
                Contest c = (Contest) e.Item.DataItem;
                
                // Time Remaining style
                string timeCSS = "remaining";

                if (c.IsContestActive)
                {
                    if (GetContestFacade.IsUserFollowingContest (c.ContestId, KanevaWebGlobals.CurrentUser.UserId))
                    {
                        followBtnText.Attributes.Add ("class", "unfollow");
                        followBtnText.InnerText = "Unfollow";
                        btnFollow.Title = "Unfollow";
                    }
                    else
                    {
                        followBtnText.Attributes.Add ("class", "follow");
                        followBtnText.InnerText = "Follow";
                        btnFollow.Title = "Follow";
                    }

                    btnFollow.Attributes.Add ("onclick", "Follow(" + c.ContestId.ToString () + ",'" + followBtnText.ClientID + "')");

                    if (c.TimeSpanRemaining.Days == 0)
                    {
                        timeCSS += " alert";
                    }
                    ((HtmlGenericControl) e.Item.FindControl ("divTimeRemaining")).Attributes.Add ("class", timeCSS);
                }
                else
                {
                    btnFollow.Visible = false;
                }

                divDesc.InnerText = TruncateWithEllipsis ((Common.HtmlRemoval.StripTagsCharArray(Server.HtmlDecode(c.Description)).Replace("&nbsp;", " ")), 136);


                // Has winner been selected?
                if (c.OwnerVoteWinnerEntryId > 0)
                {
                    divWinner.Visible = true;
                }
            }
        }

        #region Properties

        private int PageNum
        {
            set { this.pageNum = value; }
            get { return this.pageNum; }
        }
        private int PageSize
        {
            set { this.pageSize = value; }
            get { return this.pageSize; }
        }
        private string OrderBy
        {
            set { this.orderBy = value; }
            get { return this.orderBy; }
        }
        private string FilterBy
        {
            set { this.filterBy = value; }
            get { return this.filterBy; }
        }
        private SortLink CurrentSort
        {
            set { this.currentSort = value; }
            get { return this.currentSort; }
        }

        private enum SortLink
        {
            Newest = 1,
            Time_Remaining = 2,
            Amount = 3,
            Activity = 4
        }

        #endregion Properties
    }
}