///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using System.Collections.Generic;
using log4net;


namespace KlausEnt.KEP.Kaneva.mykaneva.widgets
{
    public partial class ForumsData : BasePage
    {
        #region Declarations
        int communityId = 0;
        string communityName = "";
        bool activeMember = true;

        private int profileOwnerId = 0;
        private bool isProfileOwner = false;
        private bool isProfileAdmin = false;
        private int userId = 0;

        private int pageSize = 10;
        private int pageNum = 1;
        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #endregion

        #region PageLoad
        protected void Page_Load(object sender, EventArgs e)
        {
//            if (Request.IsAuthenticated)
//            {
                try
                {
                    //clears local cache so data is always new
                    Response.ContentType = "text/plain";
                    Response.CacheControl = "no-cache";
                    Response.AddHeader("Pragma", "no-cache");
                    Response.Expires = -1;

                    GetRequestValues();
                    CheckUserAccess();
                    BindData();
                    SetEditOptions();
                }
                catch (Exception ex)
                {
                    m_logger.Error("Error loading Forums widget. ", ex);

                    divNoData.InnerText = "We're sorry but the data could not be retrieved.";
                    divNoData.Visible = true;

                    // Always call this method so the totalRecords value will get updated
                    // and the pager will be hidden if it is not needed
                    litJS.Text = "<script type=\"text/javascript\">SetAjaxPager(1,0);</script>";
                }
//            }
        }
        #endregion

        #region Properties

        private int ProfileOwnerId
        {
            set { this.profileOwnerId = value; }
            get { return this.profileOwnerId; }
        }
        private int CommunityId
        {
            set { this.communityId = value; }
            get { return this.communityId; }
        }
        private int PageNum
        {
            set { this.pageNum = value; }
            get { return this.pageNum; }
        }
        private int PageSize
        {
            set { this.pageSize = value; }
            get { return this.pageSize; }
        }

        #endregion Properties

        #region Helper Functions

        private void SetEditOptions()
        {
            forumEdit.Visible = false;

            if (isProfileOwner || this.isProfileAdmin)
            {
                //set upload href
                adminForum.HRef = this.ResolveUrl("~/forum/forumTopics.aspx?forumId=0&communityId=" + CommunityId);
                forumEdit.Visible = true;

                //set callout
                if (divNoData.Visible)
                {
                    divNoData.InnerHtml += "<div>Would you like to <a href=\"" + adminForum.HRef + "\" alt=\"add forum entry\" >Add an Entry</a>?</div>";
                }
            }
        }

        /// <summary>
        /// Checks the current user's access level
        /// </summary>
        private void CheckUserAccess()
        {
            //get the users Id
            userId = GetUserId();

            //see if they are the profile/community owner
            isProfileOwner = userId.Equals(profileOwnerId);

            if (!isProfileOwner)
            {
                //check the privilege level to see if they are an admin
                this.isProfileAdmin = HasWritePrivileges((int)SitePrivilege.ePRIVILEGE.COMMUNITY_PROFILE_ADMIN);
            }
        }

        #endregion


        #region Functions

        private void GetRequestValues()
        {
            // Get the community Id
            if (Request["communityId"] != null)
            {
                try
                {
                    CommunityId = Convert.ToInt32(Request["communityId"]);
                }
                catch { }
            }

            // Get items per page count
            if (Request["pagesize"] != null)
            {
                try
                {
                    PageSize = Convert.ToInt32(Request["pagesize"]);
                }
                catch { }
            }

            // Get the current page number
            if (Request["p"] != null)
            {
                try
                {
                    PageNum = Convert.ToInt32(Request["p"]);
                }
                catch { }
            }

            // Get the user Id from the parameters 
            if (Request["profileOwnerId"] != null)
            {
                try
                {
                    ProfileOwnerId = Convert.ToInt32(Request["profileOwnerId"]);
                }
                catch { }
            }
            else
            {
                try
                {
                    ProfileOwnerId = GetCommunityFacade.GetCommunity(CommunityId).CreatorId;
                }
                catch { }
            }

        }

        private void BindData()
        {
            string strForumId = string.Empty;

            // If the user is not a member then they should not be able to add a topic.
            CommunityMember communityMember = GetCommunityFacade.GetCommunityMember(CommunityId, GetUserId());

            if (communityMember.StatusId != (UInt32)CommunityMember.CommunityMemberStatus.ACTIVE)
            {
                activeMember = false; ;
            }
            
            //get the forum topics

            PagedList<ForumTopic> pdtPosts = GetForumFacade.GetLatestForumTopics(CommunityId, PageNum, PageSize);

            if (pdtPosts.TotalCount > 0)
            {
                repeaterForum.DataSource = pdtPosts;
                repeaterForum.DataBind ();
            }
            else
            {
                divNoData.Visible = true;
                divNoData.InnerHtml = "<div>No forums found for this World.</div>";
            }

            // Always call this method so the totalRecords value will get updated
            // and the pager will be hidden if it is not needed
            litJS.Text = "<script type=\"text/javascript\">SetAjaxPager(" + PageSize.ToString () + "," + pdtPosts.TotalCount.ToString () + ");</script>";

            //set forum page link and text
            hlForum.NavigateUrl = ResolveUrl ("~/forum/forumTopics.aspx?forumId=0&communityId=" + CommunityId.ToString ());
            hlForum.Text = "View Forums";

            //get the number of forums
            int cnt = GetForumFacade.GetForumCount(communityId);

            if ((cnt > 0) && activeMember)
            {
                if (cnt > 1)
                {
                    hlAddTopic.NavigateUrl = Page.ResolveUrl("~/forum/forumTopics.aspx?forumId=0&communityId=" + CommunityId.ToString());
                    strForumId = "0";
                }
                else
                {
                    IList<Forum> forums = GetForumFacade.GetForums(communityId);

                    if (forums.Count > 0)
                    {
                        hlForum.NavigateUrl = Page.ResolveUrl("~/forum/forumTopics.aspx?forumId=" + forums[0].ForumId.ToString() + "&communityId=" + CommunityId.ToString());
                        hlForum.Text = "View All Topics";
                    }

                    strForumId = forums[0].ForumId.ToString();
                }

                hlAddTopic.NavigateUrl = ResolveUrl("~/forum/forumEditTopic.aspx?topicId=0&forumId=" + strForumId + "&communityId=" + CommunityId.ToString());

                if (!Request.IsAuthenticated)
                {
                    hlAddTopic.Visible = false;
                }
            }
            else
            {
                hlAddTopic.Visible = false;
            }
        }

        #endregion

        #region Event Handlers

        private void repeaterForum_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                //gets the community name from the forum data to prevent extra call to DB
                if (communityName == "")
                {
                    communityName = ((ForumTopic)e.Item.DataItem).CreatedNameNoSpaces;
                }

                //gets all necessary controls
                HyperLink hlForumPost = (HyperLink)e.Item.FindControl("hlForumPost");
                Label lblDescription = (Label)e.Item.FindControl("lblDescription");

                //sets values
                hlForumPost.Text = KanevaGlobals.TruncateWithEllipsis(
                    Server.HtmlDecode(DataBinder.Eval(e.Item.DataItem, "subject").ToString()), 38);

                hlForumPost.NavigateUrl = Page.ResolveUrl("~/forum/forumThreads.aspx?topicId=" + ((ForumTopic)e.Item.DataItem).TopicId +
                    "&communityId=" + communityId + "#" + 0);
            }
        }

        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            this.repeaterForum.ItemDataBound += new RepeaterItemEventHandler(
                this.repeaterForum_ItemDataBound);
        }
        #endregion

    }
}
