///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI.WebControls;
using System.Web.UI;
using KlausEnt.KEP.Kaneva.framework.widgets;

using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva.mykaneva.widgets
{
    public partial class ModuleChannelInfoView : ModuleViewBaseControl
    {

        private void Page_Load(object sender, System.EventArgs e)
        {
            if (!IsPostBack)
            {
                BindData();
            }

            //open the editor window when user clicks edit button
            lbEdit.Attributes["onclick"] = "window.open('../mykaneva/ModuleEditor.aspx?modid=" + this.InstanceId
                + "&pageId=" + PageId + "' , 'ModuleEditor', 'width=750,height=680,scrollbars=1,resizable=1,status=1,toolbar=0'); return false;";

        }

        private void BindData()
        {
            tblEdit.Visible = ShowEdit;

            //get the module data
            DataRow drChannelInfo = WidgetUtility.GetModuleChannelInfo(InstanceId);
            if (drChannelInfo != null)
            {
                if (drChannelInfo["title"] != DBNull.Value && drChannelInfo["title"].ToString() != "")
                {
                    lblTitle.Text = drChannelInfo["title"].ToString();
                    lblTitle.Visible = true;
                }
                else
                {
                    lblTitle.Visible = false;
                }

                int channelId = Convert.ToInt32(drChannelInfo["channel_id"]);

                bool showMod = drChannelInfo["show_moderators"].ToString() == "1";
                pnlModerators.Visible = showMod;
                if (showMod)
                {
                    int numModerators = Convert.ToInt32(drChannelInfo["num_moderators"]);
                    PagedDataTable pds = CommunityUtility.GetCommunityUsers(channelId, 0,
                        false, false, false, false,
                        " account_type_id = " + (int)CommunityMember.CommunityMemberAccountType.MODERATOR,
                        "", 1, numModerators);
                    if (pds.TotalCount > 0)
                    {
                        rpModerators.DataSource = pds;
                        rpModerators.DataBind();
                    }
                    else
                    {
                        pnlModerators.Visible = false;
                    }
                }

                //get the community stats
                DataRow drSystemStats = CommunityUtility.GetCommunityStats(channelId);

                string userName = drSystemStats["creator_username"].ToString();
                string profileUrl = KanevaGlobals.GetPersonalChannelUrl(drSystemStats["creator_username"].ToString());
                bool isPersonal = drSystemStats["is_personal"].ToString() == "1";

                hlOwner.NavigateUrl = profileUrl;
                hlOwner.Text = userName;

                imgAvatar.Src = GetBroadcastChannelImageURL(drSystemStats["thumbnail_large_path"].ToString(), "la");

                //repeater for stats
                if (drSystemStats != null)
                {
                    DataTable dtUserProfile = new DataTable();
                    dtUserProfile.Columns.Add("header");
                    dtUserProfile.Columns.Add("body");

                    // save these so that when both are populated we will print out
                    // one string that includes both values
                    string height_feet = "";
                    string height_inches = "";

                    for (int i = 0; i < drSystemStats.Table.Columns.Count; i++)
                    {
                        string column_name = drSystemStats.Table.Columns[i].ColumnName;

                        string header = "";
                        string body = drSystemStats.Table.Rows[0].ItemArray[i] != null ? drSystemStats.Table.Rows[0].ItemArray[i].ToString() : "";

                        if (isPersonal)
                        {
                            switch (column_name)
                            {
                                //signup_date, last_login, number_forum_posts,number_of_views, last_update

                                case "last_login":
                                    header = "Last login:";
                                    body = body != "" ? DateTime.Parse(body).ToShortDateString() : "";
                                    break;

                                case "signup_date":
                                    header = "Member since:";
                                    body = body != "" ? DateTime.Parse(body).ToShortDateString() : "";
                                    break;

                                case "number_of_topics":
                                    header = "Forum posts:";
                                    break;

                                case "number_of_views":
                                    header = "Views:";
                                    break;

                                case "last_update":
                                    header = "Last updated:";
                                    body = body != "" ? DateTime.Parse(body).ToShortDateString() : "";
                                    break;
                                //channel stuff
                                default:
                                    break;

                            }
                        }
                        else
                        {
                            switch (column_name)
                            {
                                //c.number_of_views, c.number_of_members, c.number_of_topics, 
                                //c.creator_username, c.created_date, c.last_edit
                                case "number_of_views":
                                    header = "Views:";
                                    break;
                                case "number_of_members":
                                    header = "Members:";
                                    break;
                                case "number_of_topics":
                                    header = "Forum topics:";
                                    break;
                                case "num_assets":
                                    header = "Media files:";
                                    break;
                                case "created_date":
                                    header = "Date created:";
                                    body = body != "" ? DateTime.Parse(body).ToShortDateString() : "";
                                    break;
                                case "creator_username":
                                    header = "Community owner:";
                                    body = "<a href=\"" + GetPersonalChannelUrl(drSystemStats["creator_username"].ToString()) + "\" >" + body + "</a>";
                                    break;
                                case "last_edit":
                                    header = "Last updated:";
                                    body = body != "" ? DateTime.Parse(body).ToShortDateString() : "";
                                    break;

                                case "number_times_shared":
                                    header = "Times shared:";
                                    break;
                                default:
                                    break;

                            }
                        }

                        if (height_feet != "" && height_inches != "")
                        {
                            header = "Height";
                            body = height_feet + " feet, " + height_inches + " inches";

                            height_feet = "";
                            height_inches = "";
                        }

                        if (header != "" && body != "")
                        {
                            DataRow dr_new = dtUserProfile.NewRow();
                            dr_new["header"] = header;
                            dr_new["body"] = body;
                            dtUserProfile.Rows.Add(dr_new);
                        }
                    }

                    rpStats.DataSource = dtUserProfile;
                    rpStats.DataBind();

                }


            }

        }


        private void rpModerators_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                int userId = Int32.Parse(DataBinder.Eval(e.Item.DataItem, "user_id").ToString());
                string userName = DataBinder.Eval(e.Item.DataItem, "username").ToString();
                string profileUrl = KanevaGlobals.GetPersonalChannelUrl(DataBinder.Eval(e.Item.DataItem, "username").ToString());
                HyperLink hlModerator = (HyperLink)e.Item.FindControl("hlModerator");

                hlModerator.NavigateUrl = profileUrl;
                hlModerator.Text = userName;
            }
        }

        #region Event Handlers

        private void lbDelete_Click(object sender, EventArgs e)
        {
            WidgetUtility.DeleteModuleChannelInfo(InstanceId);
            FireModuleDeletedEvent();
        }

        #endregion

        #region Properties

        public string Username
        {
            get
            {
                if (ViewState["Username"] != null)
                {
                    return (string)ViewState["Username"];
                }
                else
                {
                    ViewState["Username"] = UsersUtility.GetUserNameFromId(ProfileOwnerId).ToUpper();

                    return (string)ViewState["Username"];
                }
            }
        }

        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);

            lbDelete.Attributes["onclick"] = "javascript:return " +
                "confirm('Are you sure you want to delete this widget?')";

            lbDelete.Click += new EventHandler(lbDelete_Click);
            this.rpModerators.ItemDataBound += new RepeaterItemEventHandler(rpModerators_ItemDataBound);
        }
        #endregion
    }
}
