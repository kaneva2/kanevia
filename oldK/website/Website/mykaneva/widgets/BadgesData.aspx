﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BadgesData.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.mykaneva.widgets.BadgesData" %>

<asp:Literal id="litJS" runat="server"></asp:Literal>
						
<div id="badges">

<div class="title" id="divTitle" runat="server">
    <h2 id="gameName" runat="server"></h2>
    <div>
		<a id="aBack" class="btnxsmall grey" runat="server"><span>< Back</span></a>
		<a class="btnmedium grey" id="aEdit" runat="server">Edit Badges</a>
	</div>
</div>
<div class="gameStats" id="divSummary" runat="server">											    											       
    <div class="counts"><div class="badges">Badges <span id="spnUserBadges" runat="server"></span> of <span id="spnTotalBadges" runat="server"></span></div>
	<div class="points"><span id="spnUserPoints" runat="server"></span> of <span id="spnTotalPoints" runat="server"></span> points</div></div>
</div>	

<div style="width:49%;border:1px solid #eee;float:left;">
<h2 style="padding:10px auto;background-color:#ccc;line-height:25px;margin:0;">Earned Badges</h2>
<asp:repeater id="rptBadges" runat="server">
	<itemtemplate>
		<div class="dataRow" id="rowContainer" runat="server">
			<div class="framesize-small">
				<div class="frame">
					<div class="imgconstrain">
						<img border="0" src='<%# GetAchievementImageURL (DataBinder.Eval(Container.DataItem, "ImageUrl").ToString(), "me") %>' border="0"/>
					</div>	
				</div>
			</div>
		    <div class="info" style="width:220px;">		    
		        <div class="name"><%# DataBinder.Eval(Container.DataItem, "Name").ToString() %></div>
				<div class="desc"><%# DataBinder.Eval(Container.DataItem, "Description").ToString() %></div>
				<div class="points"><%# DataBinder.Eval(Container.DataItem, "Points").ToString() %> <%# Convert.ToInt32(DataBinder.Eval(Container.DataItem, "Points")) == 1 ? "point" : "points" %></div>
				<div class="date"><%# GetAcquisitionDate (DataBinder.Eval(Container.DataItem, "CreatedDate")) %></div>
		    </div>					
		</div>
	</itemtemplate>						
</asp:repeater>
<div class="noData" id="divNoEarnedBadges" runat="server" visible="false">You have not earned any Badges<br />for this world.</div>									  
</div>

<div style="width:49%;border:1px solid #eee;float:right;">
<h2 style="padding:10px auto;background-color:#ccc;line-height:25px;margin:0;">Unearned Badges</h2>
<asp:repeater id="rptUnearned" runat="server">
	<itemtemplate>
		<div class="dataRow" id="Div1" runat="server">
			<div class="framesize-small <%# Convert.ToDateTime(DataBinder.Eval(Container.DataItem, "CreatedDate")) > DateTime.MinValue ? "" : "rowOff" %>" style="z-index:1">
				<div class="frame">
					<div class="imgconstrain">
						<img border="0" src='<%# GetAchievementImageURL (DataBinder.Eval(Container.DataItem, "ImageUrl").ToString(), "me") %>' border="0"/>
					</div>	
				</div>
			</div>
		    <div class="info" style="width:220px;">		    
		        <div class="name"><%# DataBinder.Eval(Container.DataItem, "Name").ToString() %></div>
				<div class="desc"><%# DataBinder.Eval(Container.DataItem, "Description").ToString() %></div>
				<div class="points"><%# DataBinder.Eval(Container.DataItem, "Points").ToString() %>  <%# Convert.ToInt32(DataBinder.Eval(Container.DataItem, "Points")) == 1 ? "point" : "points" %></div>
				<div class="date"><%# GetAcquisitionDate (DataBinder.Eval(Container.DataItem, "CreatedDate")) %></div>
		    </div>					
		</div>
	</itemtemplate>						
</asp:repeater>
<div class="noData" id="divUnearnedBadges" runat="server" visible="false">You have earned all the Badges for this world.</div>									  
</div>



<div class="noData" id="divNoData" runat="server" visible="false"></div>

</div>
