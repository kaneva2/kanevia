///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva.mykaneva.widgets
{
    public partial class MyCommunities : ModuleViewBaseControl
    {
        #region Declerations

        private Community community = new Community();

        #endregion Declerations

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #region Properties

        public Community CurrentCommunity
        {
            get
            {
                try
                {
                    if (this.community.CommunityId == 0)
                    {
                        this.community = (Community)(HttpContext.Current.Items["community"]);
                    }
                }
                catch { }

                return this.community;
            }
        }

        #endregion Properties

    }
}