<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LeaderboardData.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.mykaneva.widgets.LeaderboardData" %>

<asp:Literal id="litJS" runat="server"></asp:Literal>
						
<div id="leaderboard">

<div class="edit" id="leadersEdit" runat="server" visible="false">
    <a class="btnmedium grey" id="editLeaders" runat="server">Edit Leaderboard</a>
    <br /><br />
</div> 

<asp:repeater id="rptLeaders" runat="server" onitemdatabound="rptLeaders_ItemDataBound" >
    <headertemplate>
		<div class="header">
			<div class="rank">Rank</div>
			<div class="player">Player</div>
		</div>
    </headertemplate>
	<itemtemplate>
		<div class="dataRow" id="rowContainer" runat="server">
			<div class="rank">
				<div><%# DataBinder.Eval(Container.DataItem, "Rank").ToString() %></div>
			</div>
			<div class="framesize-square">
				<div id="Div1" class="restricted" runat="server" visible='<%# Convert.ToBoolean (DataBinder.Eval(Container.DataItem, "MatureProfile")) %>'></div>
				<div class="frame">
					<div class="imgconstrain">
						<a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "UserName").ToString ())%>>
							<img id="Img1" runat="server" src='<%# GetProfileImageURL (DataBinder.Eval(Container.DataItem, "ThumbnailSmallPath").ToString (), "sq", DataBinder.Eval(Container.DataItem, "Gender").ToString ())%>' border="0"/>
						</a>
					</div>	
				</div>
			</div>
		    <div class="player">		    
		        <a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "UserName").ToString ())%>><%# TruncateWithEllipsis (DataBinder.Eval (Container.DataItem, "UserName").ToString (), 20)%></a><br />
				<div><%# DataBinder.Eval(Container.DataItem, "DisplayName").ToString () %></div>
				<div class="playerTitle"><%# DataBinder.Eval(Container.DataItem, "Title").ToString () %></div>
				<div class="level">Level: <span id="spnLevel" runat="server"><%# DataBinder.Eval(Container.DataItem, "LevelNumber").ToString () %></span></div>
		    </div>
		    <div class="points">		 
				<div class="label"><%# DataBinder.Eval(Container.DataItem, "ColumnName").ToString () %>:</div>
				<div class="value"><%# DataBinder.Eval(Container.DataItem, "CurrentValue").ToString () %></div>
				<div class="remove"><asp:hyperlink id="hlRemove" runat="server" visible='<%# AllowedToEdit() %>' navigateurl='<%# GetRemovePlayerScript ((int) DataBinder.Eval(Container.DataItem, "UserId")) %>' text="Remove Player" /></div>
		    </div>					
		</div>
	</itemtemplate>						
</asp:repeater>
										   
<div class="noData" id="divNoData" runat="server" visible="false"></div>

</div>
	