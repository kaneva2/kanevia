///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using log4net;

namespace KlausEnt.KEP.Kaneva.mykaneva.widgets
{
    public partial class PremiumItemsData : BasePage
    {
        #region Declerations

        private int communityId = 0;
        private int gameId = 0;
        private string orderBy = "name DESC";
        private int pageSize = 20;
        private int pageNum = 1;
        private bool showGrid = true;
        private bool suppressWidgetPagination = false;

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

        #endregion Declerations

        #region Page Load

        protected void Page_Load (object sender, EventArgs e)
        {
            try
            {
                //clears local cache so data is always new
                Response.ContentType = "text/plain";
                Response.CacheControl = "no-cache";
                Response.AddHeader("Pragma", "no-cache");
                Response.Expires = -1;

                premItemsEdit.Visible = false;

                GetRequestValues();
                BindData();
            }
            catch (Exception ex)
            {
                m_logger.Error ("Error loading Premium Items widget. ", ex);

                divNoData.InnerText = "We're sorry but the data could not be retrieved.";
                divNoData.Visible = true;

                // Always call this method so the totalRecords value will get updated
                // and the pager will be hidden if it is not needed
                litJS.Text = "<script type=\"text/javascript\">SetAjaxPager(1,0);</script>";
            }
        }

        #endregion Page Load

        #region Functions

        private void BindData ()
        {
            bool showPrivate = false;
            bool showNonApproved = false;

            // Get the premium items                                              
            if (GetCommunityFacade.IsCommunityModerator (CommunityId, KanevaWebGlobals.CurrentUser.UserId))
            {
                showPrivate = true;
                showNonApproved = true;
            }
            
            PagedList<WOKItem> premiumItems = GetShoppingFacade.GetPremiumItems (GameId, PageSize, PageNum, OrderBy, showPrivate, showNonApproved);
                                                     
            if (premiumItems.TotalCount > 0)
            {
                if (ShowGrid)
                {
                    divDataList.Visible = true;
                    divRepeater.Visible = false;

                    dlPremItems.DataSource = premiumItems;
                    dlPremItems.DataBind ();
                }
                else
                {
                    divDataList.Visible = false;
                    divRepeater.Visible = true;

                    rptPremItems.DataSource = premiumItems;
                    rptPremItems.DataBind ();
                }
            }
            else
            {
                if (ShowGrid)
                {
                    divDataList.Visible = true;
                    divRepeater.Visible = false;
                }
                else
                {
                    divDataList.Visible = false;
                    divRepeater.Visible = true;
                }

                divNoData.Visible = true;
                divNoData.InnerHtml = "<div>No premium items found for this World.</div>";
            }

            if (!suppressWidgetPagination)
            {
                // Always call this method so the totalRecords value will get updated
                // and the pager will be hidden if it is not needed
                litJS.Text = "<script type=\"text/javascript\">SetAjaxPager(" + PageSize.ToString() + "," + premiumItems.TotalCount.ToString() + ");</script>";
            }
        }

        private void GetRequestValues ()
        {
            // Get the community Id
            if (Request["communityId"] != null)
            {
                try
                {
                    CommunityId = Convert.ToInt32 (Request["communityId"]);
                }
                catch { }
            }

            // Get the community Id
            if (Request["gameId"] != null)
            {
                try
                {
                    GameId = Convert.ToInt32 (Request["gameId"]);
                }
                catch { }
            }

            // Get orderby field
            if (Request["orderby"] != null)
            {
                try
                {
                    if (Request["orderby"] != null)
                    {
                        OrderBy = Request["orderby"].ToString ();
                        switch (OrderBy.ToLower ())
                        {
                            case "raves":
                                OrderBy = ""; //"ass.number_of_diggs DESC, a.name";
                                break;
                            case "views":
                                OrderBy = ""; //"ass.number_of_downloads DESC";
                                break;
                            case "rand":
                                OrderBy = ""; //"Rand()";
                                break;
                            case "date":
                                OrderBy = ""; //"a.created_date DESC";
                                break;
                            case "alpha":
                            default:
                                OrderBy = "name ASC";
                                break;
                        }
                    }
                }
                catch { }
            }

            // Get items per page count
            if (Request["pagesize"] != null)
            {
                try
                {
                    PageSize = Convert.ToInt32 (Request["pagesize"]);
                }
                catch { }
            }

            // Get the current page number
            if (Request["p"] != null)
            {
                try
                {
                    PageNum = Convert.ToInt32 (Request["p"]);
                }
                catch { }
            }

            if (Request["showgrid"] != null)
            {
                showGrid = Convert.ToBoolean (Request["showgrid"]);
            }

            if (Request["swpag"] != null)
            {
                suppressWidgetPagination = Convert.ToBoolean(Request["swpag"]);
            }
        }

        #endregion Functions

        #region Properties

        private int CommunityId
        {
            set { this.communityId = value; }
            get { return this.communityId; }
        }
        private int GameId
        {
            set { this.gameId = value; }
            get 
            {
                if (this.gameId == 0)
                {
                    if (this.communityId > 0)
                    {
                        this.gameId = GetCommunityFacade.GetCommunity (this.communityId).WOK3App.GameId;
                    }
                }
                return this.gameId; 
            }
        }
        private int PageNum
        {
            set { this.pageNum = value; }
            get { return this.pageNum; }
        }
        private int PageSize
        {
            set { this.pageSize = value; }
            get { return this.pageSize; }
        }
        private string OrderBy
        {
            set { this.orderBy = value; }
            get { return this.orderBy; }
        }
        private bool ShowGrid
        {
            set { this.showGrid = value; }
            get { return this.showGrid; }
        }

        #endregion Properties

    }
}
