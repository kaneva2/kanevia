///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Kaneva.BusinessLayer.BusinessObjects.API;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using log4net;

namespace KlausEnt.KEP.Kaneva.mykaneva.widgets
{
    public partial class ProfileFameData : BasePage
    {
        #region Declarations

        private int profileOwnerId = 0;
        private int communityId = 0;
        private int pageSize = 25;
        private int pageNum = 1;
        private bool showSmall = false;
        private Dictionary<string, int> dictSummary;

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

        #endregion


        #region PageLoad

        protected void Page_Load (object sender, EventArgs e)
        {
//            if (Request.IsAuthenticated)
//            {
                try
                {
                    //clears local cache so data is always new
                    Response.ContentType = "text/plain";
                    Response.CacheControl = "no-cache";
                    Response.AddHeader ("Pragma", "no-cache");
                    Response.Expires = -1;

                    GetRequestValues ();

                    // Get Fame Totals
                    GetSummaryData ();

                    // Show the badges
                    BindData ();
                }
                catch (Exception ex)
                {
                    m_logger.Error ("Error loading Forums widget. ", ex);

                    divNoData.InnerText = "We're sorry but the data could not be retrieved.";
                    divNoData.Visible = true;

                    // Always call this method so the totalRecords value will get updated
                    // and the pager will be hidden if it is not needed
                    litJS.Text = "<script type=\"text/javascript\">SetAjaxPager(1,0);</script>";
                }
//            }
        }

        #endregion


        #region Functions

        private void GetSummaryData ()
        {
            try
            {
                // Get Raves
                lblRaves.Text = GetUserFacade.GetUser (ProfileOwnerId).NumberOfDiggs.ToString ();
                
                Dictionary<string, int> dSum = GetGameFacade.GetAchievementsSummaryForAllApps (ProfileOwnerId);

                if (dSum.Count > 0)
                {
                    int val = 0;

                    // Get users total points 
                    if (dSum.TryGetValue ("UserTotalPoints", out val))
                    {
                        spnUserPoints.InnerText = val.ToString ();
                    }

                    // Get total points of all achievements
                    if (dSum.TryGetValue ("TotalPoints", out val))
                    {
                        spnTotalPoints.InnerText = val.ToString ();
                    }

                    // Get users total number of achievements
                    if (dSum.TryGetValue ("UserTotalAchievements", out val))
                    {
                        spnUserBadges.InnerText = val.ToString ();
                    }

                    // Get total number of achievements for 3D App
                    if (dSum.TryGetValue ("TotalAchievements", out val))
                    {
                        spnTotalBadges.InnerText = val.ToString ();
                    }
                }
            }
            catch { }
        }

        private void BindData ()
        {
            // Check to see if owner has created a leaderboard for this 3D App
            PagedList<Community> communities = GetGameFacade.GetGamesInWhichUserHasFame (ProfileOwnerId, PageNum, PageSize, "name");

            if (communities.TotalCount > 0)
            {
                if (ShowSmallWidget)
                {
                    rptFameAppsSmall.DataSource = communities;
                    rptFameAppsSmall.DataBind ();
                }
                else
                {
                    rptFameApps.DataSource = communities;
                    rptFameApps.DataBind ();
                }
            }
            else
            {
                divNoData.Visible = true;
                divNoData.InnerHtml = "<div></div>";

                // Always call this method so the totalRecords value will get updated
                // and the pager will be hidden if it is not needed
                litJS.Text = "<script type=\"text/javascript\">SetAjaxPager(1,0);</script>";

                return;
            }
            // TEMPORARY - temporarily not showing pager until we have time to work on pager so it will work
            // correctly since we are adding 1 or more items to the community list for Kaneva Fame, Designer Fame, etc.
            litJS.Text = "<script type=\"text/javascript\">SetAjaxPager(1,0);</script>";
        }

        private void GetRequestValues ()
        {
            // Get the community Id
            if (Request["communityId"] != null)
            {
                try
                {
                    CommunityId = Convert.ToInt32 (Request["communityId"]);
                }
                catch { }
            }

            // Show widget in left columb
            if (Request["showsmall"] != null)
            {
                ShowSmallWidget = Convert.ToBoolean (Request["showsmall"]);
            }

            // Get items per page count
            if (Request["pagesize"] != null)
            {
                try
                {
                    PageSize = Convert.ToInt32 (Request["pagesize"]);
                }
                catch { }
            }

            // Get the current page number
            if (Request["p"] != null)
            {
                try
                {
                    PageNum = Convert.ToInt32 (Request["p"]);
                }
                catch { }
            }

            // Get the user Id from the parameters 
            if (Request["profileOwnerId"] != null)
            {
                try
                {
                    ProfileOwnerId = Convert.ToInt32 (Request["profileOwnerId"]);
                }
                catch { }
            }
            else
            {
                try
                {
                    ProfileOwnerId = GetCommunityFacade.GetCommunity (CommunityId).CreatorId;
                }
                catch { }
            }
        }

        /// <summary>
        /// GetEditBadgeScript
        /// </summary>
        protected string GetGameFameScript (string appCommunityId)
        {
            try
            {
                int commId = Convert.ToInt32 (appCommunityId);
                if (commId > 100 || commId == (int)FameTypes.World)
                {
                    return "<a href=\"javascript:GameFame (" + appCommunityId + "," + CommunityId + ");\">More Badges &#187;</a>";
                }
            }
            catch { }
            return "";
        }

        protected string GetAchievementCounts (int appCommunityId)
        {
            try
            {
                DictSummary = GetGameFacade.GetAchievementsSummaryForApp (appCommunityId, ProfileOwnerId);

                if (DictSummary.Count > 0)
                {
                    int val = 0;
                    string strOut = "";

                    // Get users user achievement count 
                    if (DictSummary.TryGetValue ("UserTotalAchievements", out val))
                    {
                        strOut = val.ToString () + " of "; ;
                    }
                    // Get users total achievement count 
                    if (DictSummary.TryGetValue ("TotalAchievements", out val))
                    {
                        strOut += val.ToString () + " Badges"; ;
                    }

                    return strOut;
                }
            }
            catch { }
            return "";
        }

        protected string GetPointValues (int appCommunityId)
        {
            try
            {
                if (DictSummary == null)
                {
                    DictSummary = GetGameFacade.GetAchievementsSummaryForApp (appCommunityId, ProfileOwnerId);
                }

                if (DictSummary.Count > 0)
                {
                    int val = 0;
                    string strOut = "";

                    // Get users user achievement count 
                    if (DictSummary.TryGetValue ("UserTotalPoints", out val))
                    {
                        strOut = val.ToString () + " of "; ;
                    }
                    // Get users total achievement count 
                    if (DictSummary.TryGetValue ("TotalPoints", out val))
                    {
                        strOut += val.ToString () + " Points"; ;
                    }

                    DictSummary = null;
                    return strOut;
                }
            }
            catch { }
            DictSummary = null;
            return "";
        }

        protected string GetTitle (int appCommunityId)
        {
            try
            {
                // Fame types will are currently 1-4, all community ids are greater than 100
                if (appCommunityId > 100)
                {
                    return GetGameFacade.GetTitle (appCommunityId, ProfileOwnerId);
                }
                else
                {
                    // check to make sure this as a valid fame type and then get user fame
                    if (appCommunityId == (int) FameTypes.World ||
                        appCommunityId == (int) FameTypes.Fashion)
                    {                                             // appCommunityId == Fame Type Id
                        return GetUserFacade.GetPlayerTitleByFameType (ProfileOwnerId, appCommunityId);
                    }
                }
            }
            catch { }

            return "";
        }

        protected string GetLevel (int appCommunityId)
        {
            try
            {
                if (appCommunityId > 100)
                {
                    LevelValue lv = GetGameFacade.GetLevelValue (appCommunityId, ProfileOwnerId);
                    if (lv.CommunityId > 0)
                    {
                        return "Level " + lv.LevelNumber.ToString ();
                    }
                }
                else
                {
                    // check to make sure this as a valid fame type and then get user fame
                    if (appCommunityId == (int) FameTypes.World ||
                        appCommunityId == (int) FameTypes.Fashion)
                    {
                        UserFame uf = GetFameFacade.GetUserFame (ProfileOwnerId, appCommunityId);
                        return "Level " + uf.CurrentLevel.LevelNumber.ToString ();
                    }
                }
            }
            catch { }

            return "";
        }

        protected string GetMostRecentAchievementThumbnails (int appCommunityId, int count)
        {
            try
            {
                string strOut = "";
                PagedList<UserAchievementData> uad = GetGameFacade.GetUserAchievementsForApp (appCommunityId, ProfileOwnerId, "created_date DESC", 1, count);

                for (int i=0; i < uad.Count; i++)
                {
                    if (uad[i].ImageUrl.Length > 0 && uad[i].UserId > 0 )
                    {
                        strOut += "<div class=\"imgconstrain\"><img src=\"" + GetAchievementImageURL (uad[i].ImageUrl, "sm") + "\" /></div>";
                    }
                }
                return strOut;
            }
            catch { }

            return "";
        }

        protected string GetFameChannelUrl (string nameNoSpaces, int appCommunityId)
        {
            switch (appCommunityId)
            {
                case (int) FameTypes.Fashion:
                    return "http://www.kaneva.com/3d-virtual-world/fame-how-to.kaneva";

                default:
                    return GetBroadcastChannelUrl (nameNoSpaces);
            }
        }
        
        #endregion Functions


        #region Properties

        private int ProfileOwnerId
        {
            set { this.profileOwnerId = value; }
            get { return this.profileOwnerId; }
        }
        private int CommunityId
        {
            set { this.communityId = value; }
            get { return this.communityId; }
        }
        private int PageNum
        {
            set { this.pageNum = value; }
            get { return this.pageNum; }
        }
        private int PageSize
        {
            set { this.pageSize = value; }
            get { return this.pageSize; }
        }
        private bool ShowSmallWidget
        {
            set { this.showSmall = value; }
            get { return this.showSmall; }
        }
        private Dictionary<string,int> DictSummary
        {
            set { this.dictSummary = value; }
            get { return this.dictSummary; }
        }

        #endregion Properties
    }
}