<%@ Control Language="c#" AutoEventWireup="false" Codebehind="ModuleStatsView.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.mykaneva.widgets.ModuleStatsView" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>


<table cellpadding="0" cellspacing="0" width="100%" id="widgetContent">
	<!-- STATS-->
	<tr>
		<td width="100%" id="pageOpacity">
		<div id="widgetBorder">
		<b class="outerFrame" id="outerFrame">
		<b class="outerFrame1"><b></b></b>
		<b class="outerFrame2"><b></b></b>
		<b class="outerFrame3"></b>
		<b class="outerFrame4"></b>
		<b class="outerFrame5"></b>
		</b> 
		<div class="outerFrame_content" id="outerFrame_content">
		<!-- Your Content Goes Here -->
			<table cellpadding="0" cellspacing="0" border="0" width="100%">
				<tr>
					<td>
						<div id="widgetHeader">
							<b class="innerFrame">
							<b class="innerFrame1"><b></b></b>
							<b class="innerFrame2"><b></b></b>
							<b class="innerFrame3"></b>
							<b class="innerFrame4"></b>
							<b class="innerFrame5"></b>
							</b> 
							<div class="innerFrameTitle_content">
							<!-- Your Content Title Goes Here -->
								<table cellpadding="0" cellspacing="0" width="100%">
									<tr>
										<td class="widgetTitle">
											<asp:label runat="server" id="lblTitle"/>
										</td>
										<td align="right">
											<table cellpadding="0" cellspacing="0" border="0" id="tblEdit" runat="server">
												<tr>
													<td nowrap>
														<asp:linkbutton id="lbEdit" runat="server" cssclass="widgetEditButton" title="edit this widget">edit</asp:linkbutton><asp:linkbutton id="lbDelete" runat="server" title="remove this widget" cssclass="widgetRemoveButton">X</asp:linkbutton>
													</td>
												</tr>
											</table>											
										</td>
									</tr>
								</table>
							</div>
							<b class="innerFrame">
							<b class="innerFrame5"></b>
							<b class="innerFrame4"></b>
							<b class="innerFrame3"></b>
							<b class="innerFrame2"><b></b></b>
							<b class="innerFrame1"><b></b></b>
							</b>
						</div> 
					</td>
				</tr>
				<tr>
					<td><img runat="server" src="~/images/spacer.gif" border="0" width="1" height="1" id="Img4"/></td>
				</tr>				
				<tr>
					<td valign="top" height="100%">
						<div id="widgetBody">
							<b class="innerFrame">
							<b class="innerFrame1"><b></b></b>
							<b class="innerFrame2"><b></b></b>
							<b class="innerFrame3"></b>
							<b class="innerFrame4"></b>
							<b class="innerFrame5"></b>
							</b> 
							<div class="innerFrame_content">
							<!-- Your Content Goes Here -->
									<table cellpadding="0" cellspacing="0" border="0" width="100%">
										<!--STARTS AN ELEMENT--->																			
										<tr>
											<td><img runat="server" src="~/images/spacer.gif" border="0" width="10" height="1"/></td>
											<!--thumb section--->
											<td valign="top" width="100%" align="center">
											
										<asp:repeater id="rpStats" runat="server" >
											<itemtemplate>
											
												<div class="floatStats">
													<table cellpadding="0" cellspacing="0" align="center" border="0" width="100%">
														<tr>
															<td class="widgetTextBold" nowrap width="90" style="text-align:left;padding-right:10px;"><%# DataBinder.Eval(Container.DataItem, "header") %></td>
															<td class="widgetText" style="text-align:left;padding-right:10px;"><%# DataBinder.Eval(Container.DataItem, "body") %></td>
														</tr>
														<tr><td colspan="3"><img runat="server" src="~/images/spacer.gif" width="1" height="5" /></td></tr>
													</table>														
												</div>
												
											</itemtemplate>			
										</asp:repeater>
																																																																									
											</td>
											<!--end thumb section-->
											<td><img runat="server" src="~/images/spacer.gif" border="0" width="10" height="1"/></td>
										</tr>																					
										<!--END ONE ELEMENT-->																																																			
									</table>
							</div>
							<b class="innerFrame">
							<b class="innerFrame5"></b>
							<b class="innerFrame4"></b>
							<b class="innerFrame3"></b>
							<b class="innerFrame2"><b></b></b>
							<b class="innerFrame1"><b></b></b>
							</b>
						</div> 
					</td>
				</tr>
			</table>
		</div>
		<b class="outerFrame">
		<b class="outerFrame5"></b>
		<b class="outerFrame4"></b>
		<b class="outerFrame3"></b>
		<b class="outerFrame2"><b></b></b>
		<b class="outerFrame1"><b></b></b>
		</b>
		</div> 
		</td>
	</tr>
	<!-- END STATS -->
							
</table>



<br>
