<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Events.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.mykaneva.widgets.Events" %>
<script type="text/javascript"><!--
document.observe("dom:loaded", InitAjax);
var orderBy = '';
function InitAjax () { AjaxReqInit (GetAjaxParams(),'<%= ResolveUrl("~/mykaneva/widgets/EventsData.aspx") %>'); }
function SetDisplayOrder (obj) { orderBy=obj.id; InitAjax(); }
function GetAjaxParams () {	return 'communityId=<%= CurrentCommunity.CommunityId %>&orderby='+orderBy; }
--> 
</script> 

<div>	
	<a href="javascript:void(0);" onclick="setDisplayOrder(this);" id="date">All</a> |  
	<a href="javascript:void(0);" onclick="setDisplayOrder(this);" id="upcoming">Upcoming</a> |
	<a href="javascript:void(0);" onclick="setDisplayOrder(this);" id="raves">Past</a>

	<div id="divAddDelete" runat="server" visible="false">
		<a href="../mykaneva/events/eventForm.aspx?communityId=<%= CurrentCommunity.CommunityId %>">Add Event</a>			
		<input type="button" value="Delete Selected" />	
	</div>
			
	
	<div id="divAjaxData">
		<img id="Img1" class="imgLoading" runat="server" src="~/images/ajax-loader.gif" 
		height="24" width="24" alt="Loading..." border="0"/>
		<div class="loadingText">Loading...</div>
	</div> 
	
	<!-- Select All Check Box -->
	<input type="checkbox" id="cbEdit" class="formKanevaCheck" onclick="javascript:SelectAll(this,this.checked);" style="dispalay:none;"/>

 
	<div class="modulePaging">
		<div id="divAjaxPager"><!-- pager --></div>  
	</div>
</div>