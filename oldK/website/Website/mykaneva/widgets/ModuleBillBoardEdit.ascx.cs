///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using KlausEnt.KEP.Kaneva.framework.widgets;

namespace KlausEnt.KEP.Kaneva.mykaneva.widgets
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	using log4net;

	/// <summary>
	///		Summary description for ModuleBillBoardEdit.
	/// </summary>
	public class ModuleBillBoardEdit : ModuleEditBaseControl
	{
		#region Declarations

		protected CuteEditor.Editor txtTitle;
		protected CuteEditor.Editor txtIntroText;
		protected System.Web.UI.WebControls.DropDownList ddlResultsToReturn;
		protected System.Web.UI.WebControls.CompareValidator cpv_Results2Return;
		protected System.Web.UI.WebControls.CheckBox cbxShowMature;
		protected System.Web.UI.WebControls.RadioButton rbnRight;
		protected System.Web.UI.WebControls.RadioButton rbnBottom;
		protected CuteEditor.Editor footerEditer;
		protected System.Web.UI.WebControls.DropDownList ddl_ResultsPerPage;
		protected System.Web.UI.WebControls.CompareValidator cpv_ResultsPerPage;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.WebControls.RadioButtonList rblSize;
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);
		
		#endregion

		private void Page_Load(object sender, System.EventArgs e)
		{
			if (!IsPostBack)
			{
				BindData();
			}
		}

		private void BindData()
		{

			//populate Media results to show pull down

			this.ddlResultsToReturn.Items.Add(new ListItem("- SELECT - ", Constants.NOTHING_SELECTED.ToString()));
			for(int i=10; i<=100; i+=10 )
			{
				this.ddlResultsToReturn.Items.Add (new ListItem (i.ToString(), i.ToString()));
			}

			this.ddl_ResultsPerPage.Items.Add(new ListItem("- SELECT - ", Constants.NOTHING_SELECTED.ToString()));
			for(int i=1; i <= 10; i++)
			{
				this.ddl_ResultsPerPage.Items.Add (new ListItem (i.ToString(), i.ToString()));
			}

			//populate the image size options list	
			rblSize.Items.Add( new ListItem( "Thumbnail", ((int) Constants.ePICTURE_SIZE.THUMBNAIL).ToString() ));		
			rblSize.Items.Add( new ListItem( "Small", ((int)Constants.ePICTURE_SIZE.SMALL).ToString() ) );
			rblSize.Items.Add( new ListItem( "Medium", ((int)Constants.ePICTURE_SIZE.MEDIUM).ToString() ) );
			rblSize.Items.Add( new ListItem( "Large", ((int)Constants.ePICTURE_SIZE.LARGE).ToString() ) );

			DataRow drContest = WidgetUtility.GetLayoutBillboard (PageModuleId);

			if (drContest != null)
			{
				if ( drContest ["title"] != DBNull.Value )
					txtTitle.Text = drContest ["title"].ToString();
				else
					txtTitle.Text = "";
				if (drContest ["footer"] != DBNull.Value )
				{
					footerEditer.Text = drContest ["footer"].ToString ();
				}
				else
				{
					footerEditer.Text = "";
				}
				try
				{
					this.ddlResultsToReturn.SelectedValue = Server.HtmlDecode (drContest ["results_to_return"].ToString ());
				}
				catch (Exception ex)
				{
					ddlResultsToReturn.SelectedValue = Constants.NOTHING_SELECTED.ToString();
					m_logger.Error ("FAILURE modulePageId = " + PageModuleId, ex);
				}
				try
				{
					this.ddl_ResultsPerPage.SelectedValue = Server.HtmlDecode (drContest ["results_per_page"].ToString ());
				}
				catch (Exception ex)
				{
					ddlResultsToReturn.SelectedValue = Constants.NOTHING_SELECTED.ToString();
					m_logger.Error ("FAILURE modulePageId = " + PageModuleId, ex);
				}

				try
				{
					rblSize.SelectedValue = drContest ["image_size"].ToString ();
				}
				catch (Exception ex)
				{
					rblSize.SelectedValue = ((int)Constants.ePICTURE_SIZE.MEDIUM).ToString();
					m_logger.Error ("FAILURE modulePageId = " + PageModuleId, ex);
				}

				try
				{
					switch(Convert.ToInt32(drContest ["info_placement"]))
					{
						case (int) Constants.eASSET_INFO_POSITION.BOTTOM:
							this.rbnBottom.Checked = true;
							this.rbnRight.Checked = false;
							break;
						case (int) Constants.eASSET_INFO_POSITION.RIGHT:
						default:
							this.rbnBottom.Checked = false;
							this.rbnRight.Checked = true;
							break;
					}
				}
				catch (Exception)
				{}

				txtIntroText.Text = drContest ["header"].ToString ();
				cbxShowMature.Checked = drContest ["show_Mature"].ToString() == "1" ? true : false;
			}
			else
			{
				// set defaults
				txtTitle.Text = WebCache.GetModuleTitle( ModuleId );
				this.ddlResultsToReturn.SelectedValue = Constants.NOTHING_SELECTED.ToString();
				this.ddl_ResultsPerPage.SelectedValue = Constants.NOTHING_SELECTED.ToString();
				rblSize.SelectedValue = ((int)Constants.ePICTURE_SIZE.MEDIUM).ToString();
				this.rbnBottom.Checked = false;
				this.rbnRight.Checked = true;
				cbxShowMature.Checked = true;

			}
		}

		#region Event Handlers

		private void btnSave_Click(object sender, EventArgs e)
		{
			int newId = -1;

			// Check for any inject scripts
			if (KanevaWebGlobals.ContainsInjectScripts (txtIntroText.Text) || KanevaWebGlobals.ContainsInjectScripts (footerEditer.Text))
			{
				m_logger.Warn ("User " + KanevaWebGlobals.GetUserId () + " tried to script from IP " + Request.UserHostAddress);
				ShowErrorOnStartup ("Your input contains invalid scripting, please remove script code and try again.", true);
				return;
			}

			if(PageModuleId > 0)
			{
				WidgetUtility.SaveLayoutModuleBillboard (PageModuleId, this.cbxShowMature.Checked, txtTitle.Text, txtIntroText.Text, Convert.ToInt32(Server.HtmlEncode(ddlResultsToReturn.SelectedValue)), this.rbnRight.Checked, footerEditer.Text, Convert.ToInt32(Server.HtmlEncode(this.ddl_ResultsPerPage.SelectedValue)), Convert.ToInt32(this.rblSize.SelectedValue));
			}
			else
			{
				PageModuleId = WidgetUtility.InsertLayoutModuleBillboard ( txtTitle.Text, this.cbxShowMature.Checked, txtIntroText.Text, Convert.ToInt32(Server.HtmlEncode(ddlResultsToReturn.SelectedValue)), this.rbnRight.Checked, footerEditer.Text, Convert.ToInt32(Server.HtmlEncode(this.ddl_ResultsPerPage.SelectedValue)), Convert.ToInt32(this.rblSize.SelectedValue));
				if (PageModuleId != -1)
					newId = PageUtility.AddLayoutPageModule( PageModuleId, PageId, ModuleId, ZoneId, Sequence );
			}
			
			CloseAndUpdateParent(newId);
		}

		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
