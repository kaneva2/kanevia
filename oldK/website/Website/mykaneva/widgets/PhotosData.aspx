<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PhotosData.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.mykaneva.widgets.PhotosData" %>

<asp:Literal id="litJS" runat="server"></asp:Literal>

<div id="photos">

<div id="photosEdit" runat="server" class="edit">
    <a class="btnmedium grey" id="uploadMedia" runat="server">Upload Media</a>
    <a class="btnmedium grey" id="manageMedia" runat="server">Media Library</a>
	<hr class="dataSeparator" />
</div> 

<asp:datalist visible="true" runat="server" showfooter="False" width="99%" id="dlPhotos" 
	cellpadding="0" cellspacing="0" border="0" repeatcolumns="4" itemstyle-width="25%" 
	horizontalalign="Center" repeatdirection="Horizontal" repeatlayout="table" >	
	<itemtemplate>
		<div class="dataRow">
			
			<div class="framesize-medium">
				<div id="Div2" class="restricted" runat="server" visible='<%# IsMature (Convert.ToInt32 ( DataBinder.Eval(Container.DataItem, "AssetRatingId")))%>'></div>
				<div class="frame">
					<div class="imgconstrain">
						<a title='View <%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "Name").ToString ())%>' href='<%# GetAssetDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "AssetId")))%>'>
							<img border="0" src='<%# GetMediaImageURL (DataBinder.Eval(Container.DataItem, "ThumbnailMediumPath").ToString () ,"me", Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "AssetId")), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "AssetTypeId")), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "ThumbnailGen")))%>' border="0"/>
						</a>
					</div>	
				</div>
			</div>
			
			<div class="name"><asp:label id="lblMemberType" text='<%# Server.HtmlDecode (TruncateWithEllipsis(DataBinder.Eval(Container.DataItem, "Name").ToString(),18)) %>' runat="server"/></div>
		</div>
	</itemtemplate>	
</asp:datalist>	 

<div class="noData" id="divNoData" runat="server" visible="false"></div>

</div>
