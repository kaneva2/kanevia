///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.BusinessObjects.API;
using Kaneva.BusinessLayer.Facade;
using log4net;

namespace KlausEnt.KEP.Kaneva.mykaneva.widgets
{
    public partial class LeaderboardData : BasePage
    {
        #region Declerations

        private int communityId = 0;
        private int userId = 0;
        private int pageSize = 10;
        private string filter = "";
        private int pageNum = 0;
        private int profileOwnerId = 0;
        private bool isProfileOwner = false;
        private bool isProfileAdmin = false;

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

        #endregion Declerations

        #region Page Load

        protected void Page_Load (object sender, EventArgs e)
        {
            try
            {
                //clears local cache so data is always new
                Response.ContentType = "text/plain";
                Response.CacheControl = "no-cache";
                Response.AddHeader ("Pragma", "no-cache");
                Response.Expires = -1;

                GetRequestValues ();
                CheckUserAccess ();

                if (AllowedToEdit())
                {
                    if (Request["del"] != null)
                    {
                        if (Request["userId"] != null)
                        {
                            try
                            {
                                int userIdToRemove = Convert.ToInt32 (Request["userId"]);

                                // Only allow admin or owner
                                GetGameFacade.RemovePlayerFromLeaderBoard (CommunityId, userIdToRemove);
                            }
                            catch { }
                        }
                    }
                }

                BindData ();
                SetEditOptions ();
            }
            catch (Exception ex)
            {
                m_logger.Error ("Error loading Leaderboard widget. ", ex);

                divNoData.InnerText = "We're sorry but the data could not be retrieved.";
                divNoData.Visible = true;

                // Always call this method so the totalRecords value will get updated
                // and the pager will be hidden if it is not needed
                litJS.Text = "<script type=\"text/javascript\">SetAjaxPager(1,0);</script>";
            }
        }

        #endregion Page Load

        #region Functions

        private void BindData ()
        {
            // Check to see if owner has created a leaderboard for this 3D App
            global::Kaneva.BusinessLayer.BusinessObjects.API.Leaderboard lb = GetGameFacade.GetLeaderBoard (CommunityId);
            if (lb.CommunityId == 0)
            {
                divNoData.Visible = true;
                divNoData.InnerHtml = "<div>This World does not have a leaderboard</div>";
                
                // Always call this method so the totalRecords value will get updated
                // and the pager will be hidden if it is not needed
                litJS.Text = "<script type=\"text/javascript\">SetAjaxPager(1,0);</script>";
                
                return;
            }

            // Bind Leaderboard values
            PagedList<UserPlayerData> upd = new GameFacade ().GetLeaderBoardValuesWithAllData (CommunityId, KanevaWebGlobals.CurrentUser.UserId, true, PageNum, PageSize);

            int initialPage = PageNum;
            if (upd.TotalCount > 0)
            {
                rptLeaders.DataSource = upd;
                rptLeaders.DataBind ();

                //** Working here on trying to get pager starting at a specific index and then
                //** paging from that point
                // If this is the initial page containing on current user, we need to calculate what
                // page we are on and pass to the pager to set as it's initial page
                //if (PageNum == 0)
                //{
                //    initialPage = (int)System.Math.Ceiling(Convert.ToDecimal (upd.List[0].Rank) / Convert.ToDecimal (PageSize));
                //}
            }
            else
            {
                divNoData.Visible = true;
                divNoData.InnerHtml = "<div>You haven't tried this World yet.</div>" +
                    "<div>Would you like to <a href=\"http://" + KanevaGlobals.SiteName + 
                    "/kgp/playwok.aspx?goto=U" + GetCommunityFacade.GetCommunityGameId (CommunityId) + 
                    "&ILC=MM3D&link=private\">go to this World?</a></div>";
            }
            // Always call this method so the totalRecords value will get updated
            // and the pager will be hidden if it is not needed
            litJS.Text = "<script type=\"text/javascript\">SetAjaxPager(" + pageSize.ToString () + "," + upd.TotalCount.ToString () + ");</script>";
            

            //** Code for using start index with pager
            //litJS.Text = "<script type=\"text/javascript\">SetAjaxPager(" + pageSize.ToString () + "," + upd.TotalCount.ToString () + "," + initialPage.ToString() + ");</script>";
        }

        private void GetRequestValues ()
        {
            // Get the community Id
            if (Request["communityId"] != null)
            {
                try
                {
                    CommunityId = Convert.ToInt32 (Request["communityId"]);
                }
                catch { }
            }

            // Get the Show Grid value
            if (Request["filter"] != null && Request["filter"].ToString ().Length > 0)
            {
                try
                {
                    MemberFilter = "u.username LIKE '" + Request["filter"].ToString () + "%'";
                }
                catch { }
            }

            // Get items per page count
            if (Request["pagesize"] != null)
            {
                try
                {
                    PageSize = Convert.ToInt32 (Request["pagesize"]);
                }
                catch { }
            }

            // Get the current page number
            if (Request["p"] != null)
            {
                try
                {
                    PageNum = Convert.ToInt32 (Request["p"]);
                }
                catch { }
            }

            // Get the user Id from the parameters 
            if (Request["profileOwnerId"] != null)
            {
                try
                {
                    ProfileOwnerId = Convert.ToInt32 (Request["profileOwnerId"]);
                }
                catch { }
            }
            else
            {
                try
                {
                    ProfileOwnerId = GetCommunityFacade.GetCommunity (CommunityId).CreatorId;
                }
                catch { }
            }
        }

        #endregion Functions

        #region Helper Functions

        private void SetEditOptions ()
        {
            if ((isProfileOwner || this.isProfileAdmin))
            {
                editLeaders.HRef = this.ResolveUrl ("~/community/3dapps/AppManagement.aspx?communityid=" + CommunityId);
                leadersEdit.Visible = true;        
            }
        }

        /// <summary>
        /// Checks the current user's access level
        /// </summary>
        private void CheckUserAccess ()
        {
            //get the users Id
            userId = GetUserId ();

            //see if they are the profile/community owner
            isProfileOwner = userId.Equals (profileOwnerId);

            if (!isProfileOwner)
            {
                //check the privilege level to see if they are an admin
                this.isProfileAdmin = HasWritePrivileges ((int) SitePrivilege.ePRIVILEGE.COMMUNITY_PROFILE_ADMIN);
            }
        }

        public bool AllowedToEdit ()
        {
            bool allowEditing = false;

            //check security level
            switch (KanevaWebGlobals.CheckUserAccess ((int) SitePrivilege.ePRIVILEGE.COMMUNITY_PROFILE_ADMIN))
            {
                case (int) SitePrivilege.eACCESS_LEVEL.ACCESS_FULL:
                    allowEditing = true;
                    break;
                case (int) SitePrivilege.eACCESS_LEVEL.ACCESS_READ:
                    break;
                case (int) SitePrivilege.eACCESS_LEVEL.ACCESS_NONE:
                    break;
                default:
                    break;
            }

            return allowEditing;
        }

        /// <summary>
        /// GetRemovePlayerScript
        /// </summary>
        protected string GetRemovePlayerScript (int userId)
        {
            return "javascript:if (confirm(\"Are you sure you want to remove this user from the leaderboard?\")){RemovePlayer (" + userId + "," + CommunityId + ")};";
        }
       
        #endregion

        #region Event Handlers

        protected void rptLeaders_ItemDataBound (object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                int userId = Int32.Parse (DataBinder.Eval (e.Item.DataItem, "UserId").ToString ());

                if (userId == KanevaWebGlobals.CurrentUser.UserId)
                {
                    HtmlContainerControl divRowContainer = (HtmlContainerControl) e.Item.FindControl ("rowContainer");
                    if (divRowContainer != null)
                    {
                        divRowContainer.Attributes.Add ("class", "dataRow currentUser"); 
                    }
                }
 //               bool isOnline = DataBinder.Eval (e.Item.DataItem, "Online").ToString () == "1";

 //               HtmlContainerControl divOnlineStatus = (HtmlContainerControl) e.Item.FindControl ("divOnlineStatus2");

 //               if (isOnline)
 //               {
 //                   divOnlineStatus.InnerHtml = "<img src=\"" + ResolveUrl ("~/images/widgets/widget_online_icon.gif") + "\"></img>&nbsp;&nbsp;Online";
 //               }

            }
        }

        #endregion Event Handlers

        #region Properties

        private int ProfileOwnerId
        {
            set { this.profileOwnerId = value; }
            get { return this.profileOwnerId; }
        }
        private int CommunityId
        {
            set { this.communityId = value; }
            get { return this.communityId; }
        }
        private int PageNum
        {
            set { this.pageNum = value; }
            get { return this.pageNum; }
        }
        private int PageSize
        {
            set { this.pageSize = value; }
            get { return this.pageSize; }
        }
        private string MemberFilter
        {
            set { this.filter = value; }
            get { return this.filter; }
        }

        #endregion Properties
    }
}
