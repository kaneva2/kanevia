<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="FriendsData.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.mykaneva.widgets.FriendsData" %>

<asp:Literal id="litJS" runat="server"></asp:Literal>

<div id="friends">

<div id="friendsEdit" runat="server" class="edit">
    <a href="../mykaneva/friendsGroups.aspx" class="btnlarge grey" id="editFriends" runat="server">Manage My Friends</a>
</div> 

<asp:datalist visible="true" runat="server" showfooter="False" id="dlFriends" onitemdatabound="dlFriends_ItemDataBound"
cellpadding="0" cellspacing="0" border="0" repeatcolumns="3" horizontalalign="Center" repeatdirection="Horizontal"
 repeatlayout="table" >	
	<headertemplate><hr class="dataSeparator top" /></headertemplate>
	<itemtemplate>
		<div class="dataRow" onmouseover="this.style.backgroundColor='#f9f9f9;'" onmouseout="this.style.backgroundColor='#fff;'">
			<div class="framesize-square">
				<div id="Div2" class="restricted" runat="server" visible='<%# Convert.ToBoolean (DataBinder.Eval(Container.DataItem, "IsMatureProfile")) %>'></div>
				<div class="frame">
					<div class="imgconstrain">
						<a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "UserName").ToString ())%>>
							<img id="Img2" runat="server" src='<%#GetProfileImageURL (DataBinder.Eval(Container.DataItem, "ThumbnailSquarePath").ToString (), "sq", DataBinder.Eval(Container.DataItem, "Gender").ToString (), Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "FacebookSettings.UseFacebookProfilePicture")), Convert.ToUInt64(DataBinder.Eval(Container.DataItem, "FacebookSettings.FacebookUserId")) )%>' border="0"/>
						</a>
					</div>	
				</div>
			</div>
		    <div class="data">
		        <div><a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "UserName").ToString ())%>><%# TruncateWithEllipsis (DataBinder.Eval (Container.DataItem, "UserName").ToString (), 11)%></a></div>
		        <div><%# DataBinder.Eval(Container.DataItem, "DisplayName").ToString () %></div>
		        <div class="status" id="divOnlineStatus" runat="server"></div>
		    </div>
		  </div>
	</itemtemplate>	
	<separatortemplate><hr class="dataSeparator" /></separatortemplate>

</asp:datalist>	 
			   
<asp:repeater id="rptFriends" runat="server" onitemdatabound="rptFriends_ItemDataBound" >
	<itemtemplate>
		<div class="dataRowSideLight">
			<div class="framesize-square">
				<div id="Div1" class="restricted" runat="server" visible='<%# Convert.ToBoolean (DataBinder.Eval(Container.DataItem, "IsMatureProfile")) %>'></div>
				<div class="frame">
					<div class="imgconstrain">
						<a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "UserName").ToString ())%>>
							<img id="Img1" runat="server" src='<%#GetProfileImageURL (DataBinder.Eval(Container.DataItem, "ThumbnailSquarePath").ToString (), "sq", DataBinder.Eval(Container.DataItem, "Gender").ToString (), Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "FacebookSettings.UseFacebookProfilePicture")), Convert.ToUInt64(DataBinder.Eval(Container.DataItem, "FacebookSettings.FacebookUserId")) )%>' border="0"/>
						</a>
					</div>	
				</div>
			</div>
			<div class="data">
		        <div class="userName"><a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "UserName").ToString ())%>><%# TruncateWithEllipsis (DataBinder.Eval (Container.DataItem, "UserName").ToString (), 25)%></a></div>
		        <div class="displayName"><%# DataBinder.Eval(Container.DataItem, "DisplayName").ToString () %></div>
		        <div class="status" id="onlineStatus" runat="server"></div>
			</div>	
		</div>		
	</itemtemplate>	
</asp:repeater>
										   
<div class="noData" id="divNoData" runat="server" visible="false"></div>

</div>

