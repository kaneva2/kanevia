<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProfileAccess.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.mykaneva.widgets.ProfileAccess" %>

<div class="accessDenied" id="divUnauthorized" runat="server">
    <h6>Please Sign In.</h6>
    <hr />
    <p><asp:button id="signIn" runat="server" text="Sign In"></asp:button></p>
</div>

<div class="accessDenied" id="divAPProfile" runat="server">
    <h6>This profile can only be seen by people with an Access Pass</h6>
    <hr />
    <div class="messageContainer">
        <p>This profile has taken advantage of our Access Pass subscription and has set this profile so that it can only be seen by other people with Access Pass.</p>
        <p>What are you waiting for? Get Yours now.</p>
        <p class="btns"><a class="btnxlarge orange" id="getAcessPass" runat="server"><span>Buy Access Pass</span></a>
		<a class="btntiny grey" id="goBackProfileAP" href="javascript:void(0);" runat="server"><span>< Back</span></a></p>
        <p class="promo">Kaneva's Access Pass includes:</p>
        <ul>
            <li>18+ member only crowd</li>
            <li>Search, meet and socialize with adults only</li>
            <li>Exclusive adult only items and Worlds</li>
            <li>Access, view, and share adult only content</li>
			<li>Shop for adult only apparel including sexy lingerie</li>
            <li>Get intimate with animations and adult only interactions</li>
         </ul> 
     </div>          
</div>

<div class="accessDenied" id="divAPCommunity" runat="server">
    <h6>This World can only be seen by people with an Access Pass</h6>
    <hr />
    <div class="messageContainer">
        <p>This World has taken advantage of our Access Pass subscription and has set this World so that it can only be seen by other people with Access Pass.</p>
        <p>What are you waiting for? Get Yours now.</p>
        <p class="btns"><a class="btnxlarge orange" id="getAccessPassComm" runat="server"><span>Buy Access Pass</span></a>	<a class="btntiny grey" id="goBackCommAP" href="javascript:void(0);" runat="server"><span>< Back</span></a></p>
        <p class="promo">Kaneva's Access Pass includes:</p>
        <ul>
            <li>18+ member only crowd</li>
            <li>Search, meet and socialize with adults only</li>
            <li>Exclusive adult only items and Worlds</li>
            <li>Access, view, and share adult only content</li>
			<li>Shop for adult only apparel including sexy lingerie</li>
            <li>Get intimate with animations and adult only interactions</li>
         </ul>
     </div>   
</div>

<div class="accessDenied" id="divOver21" runat="server">
    <h6>If you want to enjoy this World ...</h6>
    <hr />
    <div class="messageContainer">
        <p>You must be age 21 or older.<br/><br/><span class="note">Kaneva members can restrict access to their Worlds if their content is tailored for specific audiences.</span></p>
        <p><a class="btnxsmall grey" id="goBackOver21" href="javascript:void(0);" runat="server"><span>< Back</span></a></p>
    </div>
    <div class="privacyMessage">
        <p>
            <u><b>Privacy Settings for Kaneva Members</b></u><br/>
            Kaneva members have the option to set their media, profiles, and Worlds to "Closed" or "Only Owner". When the item is set to Closed, then only friends within the member's private
            network are allowed to view detailed information such as personal interests and friends. Items set to Only Owner are only viewable by the member. These settings are 
			part of a broad effort at Kaneva to help protect the privacy of our members and provide a safe environment for all members to connect online.
		</p>    
    </div>
</div>

<div class="accessDenied" id="divProfilePrivate" runat="server">
    <h6>This Profile is Private</h6>
    <hr />    
    <div class="messageContainer">
        <p>The profile you are trying to access has been set to private by its owner and is currently not available for public viewing.</p>
        <p class="btns">
            <a class="btnlarge grey" id="sendMessagePPrivate" runat="server"><span>Send Message</span></a>
            <a class="btnxsmall grey" href="javascript:void(0);" id="goBackPrivate" runat="server"><span>< Back</span></a>
		</p>
    </div>
    <div class="privacyMessage">
        <p>
            <u><b>Privacy Settings for Kaneva Members</b></u><br/>
            Kaneva members have the option to set their media, profiles, and Worlds to "Closed" or "Only Owner". When the item is set to Closed, then only friends within the member's private
            network are allowed to view detailed information such as personal interests and friends. Items set to Only Owner are only viewable by the member. These settings are 
			part of a broad effort at Kaneva to help protect the privacy of our members and provide a safe environment for all members to connect online.
		</p>    
    </div>    
</div>

<div class="accessDenied" id="divCommPrivate" runat="server">
    <h6>This World is Private</h6>
    <hr /> 
    <div class="messageContainer">
        <p>The World you are trying to access has been set to private by its owner and is currently not available for public viewing.</p>
        <p class="btns">
            <a class="btnmedium grey" id="sendMessageCPrivate" runat="server"><span>Send Message</span></a>
			<a class="btnxsmall grey" href="javascript:void(0);" id="goBackCommPrivate" runat="server"><span>< Back</span></a>
		</p>
    </div>
    <div class="privacyMessage">
        <p>
            <u><b>Privacy Settings for Kaneva Members</b></u><br/>
            Kaneva members have the option to set their media, profiles, and Worlds to "Closed" or "Only Owner". When the item is set to Closed, then only friends within the member's private
            network are allowed to view detailed information such as personal interests and friends. Items set to Only Owner are only viewable by the member. These settings are 
			part of a broad effort at Kaneva to help protect the privacy of our members and provide a safe environment for all members to connect online.
		</p>    
    </div>    
</div>

<div class="accessDenied" id="divFriend" runat="server">
    <h6>This Profile is Viewable by Friends Only</h6>
    <hr />  
    <div class="messageContainer">
        <p>This profile has been set to be viewable by friends only. If you would like to be this member's friend send this member a friend request.  Once approved, you will be given access to this member profile.</p>
        <p class="btns">
            <a class="btnmedium grey" href="javascript:void(0);" id="sendFriendRequest" runat="server"><span>Friend Request</span></a>
			<a class="btnxsmall grey" href="javascript:void(0);" id="goBackFriend" runat="server"><span>< Back</span></a>
		</p>
    </div>
    <div class="privacyMessage">
        <p>
            <u><b>Privacy Settings for Kaneva Members</b></u><br/>
            Kaneva members have the option to set their media, profiles, and Worlds to "Closed" or "Only Owner". When the item is set to Closed, then only friends within the member's private
            network are allowed to view detailed information such as personal interests and friends. Items set to Only Owner are only viewable by the member. These settings are 
			part of a broad effort at Kaneva to help protect the privacy of our members and provide a safe environment for all members to connect online.
		</p>    
    </div>    
</div>

<div class="accessDenied" id="divMembersOnly" runat="server">
    <h6>This World is Viewable by Members Only</h6>
    <hr />   
    <div class="messageContainer">
        <p>This World has been set to be viewable by members only. If you would like to join this World send this World a membership request.  Once approved, you will be given access to this World.</p>
        <p>
            <a class="btnlarge grey" href="javascript:void(0);" id="sendMemberRequest" runat="server"><span>Request Membership</span></a>
			<a class="btnxsmall grey" href="javascript:void(0);" id="goBackMember" runat="server"><span>< Back</span></a>
		</p>
    </div>
    <div class="privacyMessage">
        <p>
            <u><b>Privacy Settings for Kaneva Members</b></u><br/>
            Kaneva members have the option to set their media, profiles, and Worlds to "Closed" or "Only Owner". When the item is set to Closed, then only friends within the member's private
            network are allowed to view detailed information such as personal interests and friends. Items set to Only Owner are only viewable by the member. These settings are 
			part of a broad effort at Kaneva to help protect the privacy of our members and provide a safe environment for all members to connect online.
		</p>    
    </div>    
</div>

<asp:Button Id="addFriend" runat="server" OnClick="SendFriendRequest_Click" style="display:none" />
<asp:Button Id="joinCommunity" runat="server" OnClick="JoinCommunity_Click" style="display:none" />

<script type="text/javascript" language="javascript">
<!--

var friendAddId = '<%=addFriend.ClientID%>';
var communityAddId = '<%=joinCommunity.ClientID%>';

function SendFriendRequest()
{
    $(friendAddId).click();
}

function JoinCommunity()
{
    $(communityAddId).click();   
}

//-->
</script>
