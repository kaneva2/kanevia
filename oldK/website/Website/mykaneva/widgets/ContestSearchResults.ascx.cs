///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.mykaneva
{
    public partial class ContestSearchResults : Kaneva.mykaneva.widgets.ModuleViewBaseControl
    {
        #region Declerations

        private string orderBy = "end_date DESC";
        private string filterBy = "";
        private int pageSize = 20;
        private int pageNum = 1;
        private SortLink currentSort = SortLink.Newest;

        #endregion Declerations

        protected void Page_Load (object sender, EventArgs e)
        {
            // They must be logged in
            if (!Request.IsAuthenticated)
            {
                Response.Redirect (GetLoginURL ());
                return;
            }

            try
            {
                // Load JS files
                RegisterJavaScript ("jscript/doodad/ContestFollow.js");

                //clears local cache so data is always new
                Response.ContentType = "text/html";
                Response.CacheControl = "no-cache";
                Response.AddHeader ("Pragma", "no-cache");
                Response.Expires = -1;

   //             GetRequestValues ();

                // Show the contests
   //             BindData ();
            }
            catch (Exception)
            {
                //          m_logger.Error ("Error loading Forums widget. ", ex);

                divNoData.InnerText = "We're sorry but the data could not be retrieved.";
                divNoData.Visible = true;

                // Always call this method so the totalRecords value will get updated
                // and the pager will be hidden if it is not needed
                litJS.Text = "<script type=\"text/javascript\">SetAjaxPager(1,0);</script>";
            }
        }

        //pass in "" for best match
        //otherwise pass in DB column ASC or DB column DESC
        //num_entries DESC
        //num_votes DESC
        //etc
        //also you can do this...
        //is_active DESC, num_votes DESC
        //forgot i included is_active
        //search by title, description, username
        //sort by - best match, prize_amount, num_entries, num votes, comments, created, end 
        //all sorts can be asc or desc except for best match
  
        public bool Search (string searchString)
        {
            SearchString = searchString;
            return BindData ();
        }

        private bool BindData ()
        {
            int PageSize = 10;

            PagedList<Contest> contests = GetContestFacade.SearchContests (0, SearchString, OrderBy, PageNum, PageSize);

            if (contests.TotalCount > 0)
            {
                rptContests.DataSource = contests;
                rptContests.DataBind ();

                sort.Visible = true;
            }
            else
            {
                divNoData.Visible = true;
                sort.Visible = false;
                // Always call this method so the totalRecords value will get updated
                // and the pager will be hidden if it is not needed
                litJS.Text = "<script type=\"text/javascript\">SetAjaxPager(1,0);</script>";
            }

            // Always call this method so the totalRecords value will get updated
            // and the pager will be hidden if it is not needed
            litJS.Text = "<script type=\"text/javascript\">SetAjaxPager(" + PageSize.ToString () + "," + contests.TotalCount.ToString () + ");</script>";

            SetSelectedLink ();
            return true;
        }

        private void SetSelectedLink ()
        {
            btnSortActivity.CssClass = "sort";
            btnSortAmount.CssClass = "sort";
            btnSortTimeRemaining.CssClass = "sort";
            btnSortNewest.CssClass = "sort";
            switch (CurrentSort)
            {
                case SortLink.Activity:
                    btnSortActivity.CssClass = " selected";
                    break;
                case SortLink.Amount:
                    btnSortAmount.CssClass = " selected";
                    break;
                case SortLink.Time_Remaining:
                    btnSortTimeRemaining.CssClass = " selected";
                    break;
                case SortLink.Newest:
                    btnSortNewest.CssClass = " selected";
                    break;
            }
        }

        protected void btnSort_Click (Object sender, CommandEventArgs e)
        {
            switch (e.CommandArgument.ToString())
            {
                case "hot":
                    {
                        OrderBy = "num_entries DESC";
                        CurrentSort = SortLink.Activity;
                        break;
                    }
                case "amount":
                    {
                        OrderBy = "prize_amount DESC";
                        CurrentSort = SortLink.Amount;
                        break;
                    }
                case "remaining":
                    {
                        OrderBy = "end_date DESC";
                        CurrentSort = SortLink.Time_Remaining;
                        break;
                    }
                case "created":
                    {
                        OrderBy = "created_date DESC";
                        CurrentSort = SortLink.Newest;
                        break;
                    }
                default:
                    {
                        OrderBy = "";
                        CurrentSort = SortLink.Best_Match;
                        break;
                    }
            }
                
            BindData ();
        }

        protected void rptContests_ItemDataBound (Object Sender, RepeaterItemEventArgs e)
        {
            if ((e.Item.ItemType == ListItemType.Item) || (e.Item.ItemType == ListItemType.AlternatingItem))
            {
                HtmlGenericControl followBtnText = ((HtmlGenericControl) e.Item.FindControl ("followBtnText"));
                HtmlAnchor btnFollow = ((HtmlAnchor) e.Item.FindControl ("btnFollow"));
                HtmlContainerControl divWinner = ((HtmlContainerControl) e.Item.FindControl ("divAllContestsWinner"));
                HtmlContainerControl divDesc = ((HtmlContainerControl) e.Item.FindControl ("divDesc"));
                
                // If user has already voted on this contest, they can't vote again
                Contest c = (Contest) e.Item.DataItem;

                // Time Remaining style
                string timeCSS = "remaining";

                if (c.IsContestActive)
                {
                    if (GetContestFacade.IsUserFollowingContest (c.ContestId, KanevaWebGlobals.CurrentUser.UserId))
                    {
                        followBtnText.Attributes.Add ("class", "unfollow");
                        followBtnText.InnerText = "Unfollow";
                        btnFollow.Title = "Unfollow";
                    }
                    else
                    {
                        followBtnText.Attributes.Add ("class", "follow");
                        followBtnText.InnerText = "Follow";
                        btnFollow.Title = "Follow";
                    }

                    btnFollow.Attributes.Add ("onclick", "Follow(" + c.ContestId.ToString () + ",'" + followBtnText.ClientID + "')");

                    if (c.TimeSpanRemaining.Days == 0)
                    {
                        timeCSS += " alert";
                    }
                    ((HtmlGenericControl) e.Item.FindControl ("divTimeRemaining")).Attributes.Add ("class", timeCSS);
                }
                else
                {
                    btnFollow.Visible = false;
                }

                divDesc.InnerText = TruncateWithEllipsis ((Common.HtmlRemoval.StripTagsCharArray (Server.HtmlDecode (c.Description))), 136);

                // Has winner been selected?
                if (c.OwnerVoteWinnerEntryId > 0)
                {
                    divWinner.Visible = true;
                }
            }
        }

        #region Properties

        private int PageNum
        {
            set { this.pageNum = value; }
            get { return this.pageNum; }
        }
        private int PageSize
        {
            set { this.pageSize = value; }
            get { return this.pageSize; }
        }
        private string OrderBy
        {
            set { this.orderBy = value; }
            get { return this.orderBy; }
        }
        private string SearchString
        {
            get
            {
                try
                {
                    if (ViewState["SearchString"] != null)
                    {
                        return ViewState["SearchString"].ToString ();
                    }
                    return "";
                }
                catch
                {
                    return "";
                }
            }
            set
            {
                ViewState["SearchString"] = value;
            }
        }
        private string FilterBy
        {
            set { this.filterBy = value; }
            get { return this.filterBy; }
        }
        private SortLink CurrentSort
        {
            set { this.currentSort = value; }
            get { return this.currentSort; }
        }

        private enum SortLink
        {
            Newest = 1,
            Time_Remaining = 2,
            Amount = 3,
            Activity = 4,
            Best_Match = 5
        }

        #endregion Properties
    }
}