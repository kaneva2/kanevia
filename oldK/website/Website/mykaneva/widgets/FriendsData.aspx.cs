///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using log4net;

namespace KlausEnt.KEP.Kaneva.mykaneva.widgets
{
    public partial class FriendsData : BasePage
    {
        #region Declerations

        private int communityId = 0;
        private int userId = 0;
        private string orderBy = "number_of_diggs DESC";
        private int pageSize = 30;
        private bool showGrid = true;
        private string filter = "";
        private int pageNum = 1;

        private int profileOwnerId = 0;
        private bool isProfileOwner = false;
        private bool isProfileAdmin = false;

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #endregion Declerations

        #region Page Load

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //clears local cache so data is always new
                Response.ContentType = "text/plain";
                Response.CacheControl = "no-cache";
                Response.AddHeader("Pragma", "no-cache");
                Response.Expires = -1;

                GetRequestValues();
                CheckUserAccess();
                BindData();
                SetEditOptions();
            }
            catch (Exception ex)
            {
                m_logger.Error("Error loading Friends widget. ", ex);

                divNoData.InnerText = "We're sorry but the data could not be retrieved.";
                divNoData.Visible = true;

                // Always call this method so the totalRecords value will get updated
                // and the pager will be hidden if it is not needed
                litJS.Text = "<script type=\"text/javascript\">SetAjaxPager(1,0);</script>";
            }
        }

        #endregion Page Load


        #region Functions

        private void BindData()
        {
            PagedList<Friend> friends = GetUserFacade.GetFriends(ProfileOwnerId, 0, "", OrderBy, PageNum, PageSize, (KanevaWebGlobals.CurrentUser.HasAccessPass || isProfileOwner), FriendFilter);

            if (friends.TotalCount > 0)
            {
                if (ShowGrid)
                {
                    dlFriends.DataSource = friends;
                    dlFriends.DataBind();
                }
                else
                {
                    rptFriends.DataSource = friends;
                    rptFriends.DataBind();
                }
            }
            else
            {
                divNoData.Visible = true;
                divNoData.InnerHtml = "<div>No friends found.</div>";
            }

            // Always call this method so the totalRecords value will get updated
            // and the pager will be hidden if it is not needed
            if (ShowGrid)
            {
                litJS.Text = "<script type=\"text/javascript\">SetAjaxPager(" + PageSize.ToString() + "," + friends.TotalCount.ToString() + ");</script>";
            }
        }

        private void GetRequestValues()
        {
            // Get the community Id
            if (Request["communityId"] != null)
            {
                try
                {
                    CommunityId = Convert.ToInt32(Request["communityId"]);
                }
                catch (Exception exc)
                {
                    m_logger.Error("Error in GetRequestValues", exc);
                }
            }

            // Get the user Id from the parameters 
            if (Request["profileOwnerId"] != null)
            {
                try
                {
                    ProfileOwnerId = Convert.ToInt32(Request["profileOwnerId"]);
                }
                catch (Exception exc)
                {
                    m_logger.Error("Error in GetRequestValues", exc);
                }
            }
            else
            {
                try
                {
                    ProfileOwnerId = GetCommunityFacade.GetCommunity(CommunityId).CreatorId;
                }
                catch { }
            }

            // Get the Show Grid value
            if (Request["filter"] != null && Request["filter"].ToString().Length > 0)
            {
                try
                {
                    FriendFilter = Request["filter"].ToString() + "%";
                }
                catch (Exception exc)
                {
                    m_logger.Error("Error in GetRequestValues", exc);
                }
            }

            // Get the Show Grid value
            if (Request["showgrid"] != null)
            {
                try
                {
                    ShowGrid = Convert.ToBoolean(Request["showgrid"]);
                }
                catch (Exception exc)
                {
                    m_logger.Error("Error in GetRequestValues", exc);
                }
            }

            // Get orderby field
            if (Request["orderby"] != null)
            {
                try
                {
                    if (Request["orderby"] != null)
                    {
                        OrderBy = Request["orderby"].ToString();
                        switch (orderBy.ToLower())
                        {
                            case "raves":
                                OrderBy = "number_of_diggs DESC";
                                break;
                            case "views":
                                OrderBy = "number_of_views DESC";
                                break;
                            case "rand":
                                OrderBy = "last_login DESC";
                                break;
                            case "date":
                                OrderBy = "glued_date DESC";
                                break;
                            case "alpha":
                            default:
                                OrderBy = "username ASC";
                                break;
                        }
                    }
                }
                catch (Exception exc)
                {
                    m_logger.Error("Error in GetRequestValues", exc);
                }
            }

            // Get items per page count
            if (Request["pagesize"] != null)
            {
                try
                {
                    PageSize = Convert.ToInt32(Request["pagesize"]);
                }
                catch (Exception exc)
                {
                    m_logger.Error("Error in GetRequestValues", exc);
                }
            }

            // Get the current page number
            if (Request["p"] != null)
            {
                try
                {
                    PageNum = Convert.ToInt32(Request["p"]);
                }
                catch (Exception exc)
                {
                    m_logger.Error("Error in GetRequestValues", exc);
                }
            }
        }

        #endregion Functions

        #region Helper Functions

        private void SetEditOptions()
        {
            friendsEdit.Visible = false;

            if (ShowGrid)
            {
                if (isProfileOwner || this.isProfileAdmin)
                {
                    friendsEdit.Visible = true;
                    //set callout
                    if (divNoData.Visible)
                    {
                        divNoData.InnerHtml += "<div>Would you like to <a href=\"" + ResolveUrl("~/people/people.kaneva") + "\" alt=\"Find a Friend\" >Find a Friend</a>?</div>";
                    }
                }
                else
                {
                    if (divNoData.Visible)
                    {
                        divNoData.InnerHtml += "<div>Would you like to <a href=\"javascript:if (confirm('Send a friend request to this user?'))AddFriend();\" " +
                        " alt=\"Add as Friend\" >Add them as your Friend</a>?</div>";
                    }
                }
            }
        }

        /// <summary>
        /// Checks the current user's access level
        /// </summary>
        private void CheckUserAccess()
        {
            //get the users Id
            userId = GetUserId();

            //see if they are the profile/community owner
            isProfileOwner = userId.Equals(profileOwnerId);

            if (!isProfileOwner)
            {
                //check the privilege level to see if they are an admin
                this.isProfileAdmin = HasWritePrivileges((int)SitePrivilege.ePRIVILEGE.USER_PROFILE_ADMIN);
            }
        }


        #endregion

        #region Event Handlers

        /// <summary>
        /// Click event for edit this widget 
        /// </summary>
        protected void editFriends_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/mykaneva/friendsGroups.aspx");
        }

        protected void rptFriends_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                bool isOnline = DataBinder.Eval(e.Item.DataItem, "UState").ToString() == Constants.ONLINE_USTATE_ON;

                HtmlContainerControl divOnlineStatus = (HtmlContainerControl)e.Item.FindControl("onlineStatus");

                if (isOnline)
                {
                    divOnlineStatus.InnerHtml = "<img src=\"" + ResolveUrl("~/images/widgets/widget_online_icon.gif") + "\"></img>&nbsp;&nbsp;Online";
                }
            }
        }

        protected void dlFriends_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                bool isOnline = DataBinder.Eval(e.Item.DataItem, "UState").ToString() == Constants.ONLINE_USTATE_ON;

                HtmlContainerControl divOnlineStatus = (HtmlContainerControl)e.Item.FindControl("divOnlineStatus");

                if (isOnline)
                {
                    divOnlineStatus.InnerHtml = "<img src=\"" + ResolveUrl("~/images/widgets/widget_online_icon.gif") + "\"></img>&nbsp;&nbsp;Online";
                }
            }
        }

        #endregion Event Handlers

        #region Properties

        private int CommunityId
        {
            set { this.communityId = value; }
            get { return this.communityId; }
        }
        private int ProfileOwnerId
        {
            set { this.profileOwnerId = value; }
            get { return this.profileOwnerId; }
        }
        private int PageNum
        {
            set { this.pageNum = value; }
            get { return this.pageNum; }
        }
        private int PageSize
        {
            set { this.pageSize = value; }
            get { return this.pageSize; }
        }
        private string FriendFilter
        {
            set { this.filter = value; }
            get { return this.filter; }
        }
        private bool ShowGrid
        {
            set { this.showGrid = value; }
            get { return this.showGrid; }
        }
        private string OrderBy
        {
            set { this.orderBy = value; }
            get { return this.orderBy; }
        }

        #endregion Properties

        #region Web Form Designer generated code

        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new EventHandler(this.Page_Load);
        }

        #endregion

    }
}
