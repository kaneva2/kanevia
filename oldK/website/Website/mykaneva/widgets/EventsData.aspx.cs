///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using System.Collections.Generic;
using log4net;

namespace KlausEnt.KEP.Kaneva.mykaneva.widgets
{
	public partial class EventsData : BasePage
    {
        #region Declerations

        private string filter = "";
		private int communityId = 0;
        private int pageSize = 20;
        private bool showGrid = true;
        private int pageNum = 1;
        int startTime = (int) EventsUtility.eEVENT_START_TIME_FILTER.UPCOMING;
        string orderBy = "start_time";
        private int profileOwnerId = 0;
        private bool isProfileOwner = false;
        private bool isProfileAdmin = false;
        private int userId = 0;

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);
        
        #endregion Declerations

        #region Page Load

        private void Page_Load(object sender, System.EventArgs e)
		{
            try
            {
                if (!IsPostBack)
                {
                    //clears local cache so data is always new
                    Response.ContentType = "text/plain";
                    Response.CacheControl = "no-cache";
                    Response.AddHeader("Pragma", "no-cache");
                    Response.Expires = -1;
                    
                    GetRequestValues();
                    CheckUserAccess();
                    BindData();
                    SetEditOptions();
                }
            }
            catch (Exception ex)
            {
                m_logger.Error ("Error loading Events widget. ", ex);

                divNoData.InnerText = "We're sorry but the data could not be retrieved.";
                divNoData.Visible = true;

                // Always call this method so the totalRecords value will get updated
                // and the pager will be hidden if it is not needed
                litJS.Text = "<script type=\"text/javascript\">SetAjaxPager(1,0);</script>";
            }
        }

        #endregion Page Load

        #region Helper Functions

        private void SetEditOptions()
        {
            if (ShowGrid && isProfileOwner || this.isProfileAdmin)
            {
                //set upload href
                addEvent.HRef = this.ResolveUrl("~/mykaneva/events/eventForm.aspx?communityId=" + CommunityId);
                eventsEdit.Visible = true;

                //set callout
                if (divNoData.Visible)
                {
                    divNoData.InnerHtml += "<div>Would you like to <a href=\"" + addEvent.HRef + "\" alt=\"add event\" >Add an event</a>?</div>";
                }
            }
            else
            {
                eventsEdit.Visible = false;
            }
        }

        /// <summary>
        /// Checks the current user's access level
        /// </summary>
        private void CheckUserAccess()
        {
            //get the users Id
            userId = GetUserId();

            //see if they are the profile/community owner
            isProfileOwner = userId.Equals(profileOwnerId);

            if (!isProfileOwner)
            {
                //check the privilege level to see if they are an admin
                this.isProfileAdmin = HasWritePrivileges((int)SitePrivilege.ePRIVILEGE.COMMUNITY_PROFILE_ADMIN);
            }
        }

        #endregion

        #region Functions

        private void BindData()
		{
            GetStartTime ();

            PagedList <Event> events = GetEventFacade.GetWorldEvents (CommunityId, StartTime, OrderBy, PageNum, PageSize);

            if (events.Count > 0)
            {
                if (ShowGrid)
                {
                    divDataList.Visible = true;
                    divRepeater.Visible = false;

                    rptEventsList.DataSource = events;
                    rptEventsList.DataBind ();
                }
                else
                {
                    divDataList.Visible = false;
                    divRepeater.Visible = true;

                    rptEvents.DataSource = events;
                    rptEvents.DataBind ();                 
                }
            }
            else
            {
                if (ShowGrid)
                {
                    divDataList.Visible = true;
                    divRepeater.Visible = false;
                }
                else
                {
                    divDataList.Visible = false;
                    divRepeater.Visible = true;
                }

                divNoData.Visible = true;
                divNoData.InnerHtml = "<div>No events found for this World.<div>";
            }

            if (events.TotalCount == 1)
            {
                // There is an issue with the pager displaying if there is only one record in the
                // dataset.  This will hide the pager if there is only 1 record returned
                litJS.Text = "<script type=\"text/javascript\">SetAjaxPager(1,0);</script>";
            }
            else
            {
                litJS.Text = "<script type=\"text/javascript\">SetAjaxPager(" + PageSize.ToString () + "," + events.TotalCount.ToString () + ");</script>";
            }
		}

        private void GetStartTime ()
        {
            switch (DateFilter)
            {
                case "all":
                    StartTime = (int) EventsUtility.eEVENT_START_TIME_FILTER.ALL;
                    break;
                case "past":
                    StartTime = (int) EventsUtility.eEVENT_START_TIME_FILTER.PAST;
                    break;
                default:       // upcoming
                    StartTime = (int) EventsUtility.eEVENT_START_TIME_FILTER.UPCOMING;
                    break;
            }
        }

		private void GetRequestValues()
		{
			// Get the community Id
			if (Request["communityId"] != null)
			{
				try
				{
					CommunityId = Convert.ToInt32(Request["communityId"]);
				}
				catch { }
			}


			// Get the Show Grid value
			if (Request["filter"] != null && Request["filter"].ToString().Length > 0)
			{
				try
				{
					DateFilter =  Request["filter"].ToString().ToLower();
				}
				catch { }
			}

            // Get orderby field
            if (Request["orderby"] != null)
            {
                try
                {
                    if (Request["orderby"] != null)
                    {
                        OrderBy = Request["orderby"].ToString();
                        switch (orderBy.ToLower())
                        {
                            case "rand":
                                OrderBy = "Rand()";
                                break;
                            default:
                                break;
                        }
                    }
                }
                catch { }
            }

			// Get items per page count
			if (Request["pagesize"] != null)
			{
				try
				{
					PageSize = Convert.ToInt32(Request["pagesize"]);
				}
				catch { }
			}

            if (Request["showgrid"] != null)
            {
                showGrid = Convert.ToBoolean(Request["showgrid"]);
            }

			// Get the current page number
			if (Request["p"] != null)
			{
				try
				{
					PageNum = Convert.ToInt32(Request["p"]);
				}
				catch { }
			}

            // Get the user Id from the parameters 
            if (Request["profileOwnerId"] != null)
            {
                try
                {
                    ProfileOwnerId = Convert.ToInt32(Request["profileOwnerId"]);
                }
                catch { }
            }
            else
            {
                try
                {
                    ProfileOwnerId = GetCommunityFacade.GetCommunity(CommunityId).CreatorId;
                }
                catch { }
            }

        }

        /// <summary>
        /// Delete selected items
        /// </summary>

        protected string FormatStartTime (object objDate)
        {
            try
            {
                DateTime dtDate = Convert.ToDateTime (objDate);

                System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo ("en-US", true);
                string date = dtDate.ToString ("D", culture);
                string time = dtDate.ToString ("h:mm tt", culture);
                return date + " at " + time;
            }
            catch {
                return "";
            }
        }

        private string FormatFutureTimeSpan (DateTime dtDate, bool showAnnivYears, bool formatForEvents)
        {
            string timespan = string.Empty;

            if (dtDate.Equals (DateTime.MinValue))
                return "";

            bool bDayIsLeapYear = System.DateTime.IsLeapYear (dtDate.Year);
            bool currentYearIsLeapYear = System.DateTime.IsLeapYear (DateTime.Today.Year);
            int span = 0;

            if (bDayIsLeapYear && !currentYearIsLeapYear && DateTime.Today.Month > 2)
            {
                span = (dtDate.DayOfYear - 1) - DateTime.Today.DayOfYear;
            }
            else if (!bDayIsLeapYear && currentYearIsLeapYear && DateTime.Today.Month > 2)
            {
                span = (dtDate.DayOfYear + 1) - DateTime.Today.DayOfYear;
            }
            else // both or neither are leap year or date is prior to Feb 29
            {
                span = dtDate.DayOfYear - DateTime.Today.DayOfYear;
            }

            if (span == 0)
            {
                timespan = " today";
                if (formatForEvents)
                {
                    timespan = "";
                    if (dtDate.TimeOfDay < DateTime.Now.TimeOfDay)
                    {
                        timespan = "Now";
                    }
                    else
                    {
                        int hrs = dtDate.TimeOfDay.Hours - DateTime.Now.TimeOfDay.Hours;
                        if (hrs == 0)
                        {
                            int mins = dtDate.TimeOfDay.Minutes - DateTime.Now.TimeOfDay.Minutes;
                            timespan = "In " + mins.ToString () + (mins > 1 ? " minutes" : " minute");
                        }
                        else
                        {
                            timespan = "In " + hrs.ToString () + (hrs > 1 ? " hours" : " hour");
                        }
                    }
                }
            }
            else if (span == 1)
            {
                timespan = " tomorrow";
                if (formatForEvents) timespan = "In 1 day";
            }
            else if (span > 1)
            {
                timespan = " in " + span + " days";
                if (formatForEvents) timespan = "In " + span + " days";
            }

            if (showAnnivYears)
            {
                try
                {
                    int numYrs = DateTime.Now.Year - dtDate.Year;
                    timespan += " (" + numYrs.ToString () + " year" + (numYrs > 1 ? "s)" : ")");
                }
                catch { }
            }

            return timespan;
        }

        #endregion Functions

        #region Event Handlers

        /// <summary>
        /// Repeater rptEvents ItemDataBound Event Handler
        /// </summary>
        protected void rptEvents_ItemDataBound (Object Sender, RepeaterItemEventArgs e)
        {
            string x = e.Item.ToString ();

            // Execute the following logic for Items and Alternating Items.
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                string eventTitle = DataBinder.Eval (e.Item.DataItem, "Title").ToString ();
                int eventId = Convert.ToInt32 (DataBinder.Eval (e.Item.DataItem, "EventId"));
                int userId = Convert.ToInt32 (DataBinder.Eval (e.Item.DataItem, "UserId"));
                string location = DataBinder.Eval (e.Item.DataItem, "Location").ToString ();
                int communityId = Convert.ToInt32 (DataBinder.Eval (e.Item.DataItem, "CommunityId"));
                string tracking_request_GUID = DataBinder.Eval (e.Item.DataItem, "TrackingRequestGUID").ToString ();

                HyperLink eventLink = (HyperLink)e.Item.FindControl("hlEventName");
                eventLink.Text = eventTitle;
                eventLink.NavigateUrl = ResolveUrl("~/mykaneva/events/eventDetail.aspx?event=" + eventId);

                Community community = GetCommunityFacade.GetCommunity (communityId);

                DateTime startDate = Convert.ToDateTime (DataBinder.Eval (e.Item.DataItem, "StartTime"));

                string startTimespan = FormatFutureTimeSpan (startDate, false, true);
                ((Literal) e.Item.FindControl ("litEventWhen")).Text = startTimespan;
            }
        }

        protected void rptEventsList_ItemDataBound (Object Sender, RepeaterItemEventArgs e)
        {
            string x = e.Item.ToString ();

            // Execute the following logic for Items and Alternating Items.
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                try
                {
                    int eventId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "EventId"));
                    int userId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "UserId"));

                    // Only show counts if owner of world or owner of event is viewing
                    if (KanevaWebGlobals.CurrentUser.UserId.Equals(userId) || KanevaWebGlobals.CurrentUser.UserId.Equals(ProfileOwnerId))
                    {
                        EventInviteCounts eic = GetEventFacade.GetEventInviteCounts(eventId);
                        ((HtmlContainerControl)e.Item.FindControl("invitesOutstanding")).InnerHtml = eic.InvitesOutstanding.ToString();
                        ((HtmlContainerControl)e.Item.FindControl("invitesAccepted")).InnerHtml = eic.IntitesAccepted.ToString();
                        ((HtmlContainerControl)e.Item.FindControl("invitesDeclined")).InnerHtml = eic.InvitesDeclined.ToString();
                    }

                    string username = GetUserFacade.GetUserName(userId);
                    ((HtmlAnchor)e.Item.FindControl("eventHostname")).HRef = GetPersonalChannelUrl(username);
                    ((HtmlAnchor)e.Item.FindControl("eventHostname")).InnerText = username;
                }
                catch { }
            }
        }

        #endregion Event Handlers

        #region Properties

        private int ProfileOwnerId
        {
            set { this.profileOwnerId = value; }
            get { return this.profileOwnerId; }
        }
        private bool ShowGrid
        {
            set { this.showGrid = value; }
            get { return this.showGrid; }
        }

        private string DateFilter
		{
			set { this.filter = value; }
			get { return this.filter; }
		}
		private int CommunityId
		{
			set { this.communityId = value; }
			get { return this.communityId; }
		}
		private int PageNum
		{
			set { this.pageNum = value; }
			get { return this.pageNum; }
		}
		private int PageSize
		{
			set { this.pageSize = value; }
			get { return this.pageSize; }
		}
        private int StartTime
        {
            set { this.startTime = value; }
            get { return this.startTime; }
        }
        private string OrderBy
        {
            set { this.orderBy = value; }
            get { return this.orderBy; }
        }

        #endregion Properties

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion

    }
}
