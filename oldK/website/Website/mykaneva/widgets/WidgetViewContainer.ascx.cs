///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using KlausEnt.KEP.Kaneva.usercontrols;

namespace KlausEnt.KEP.Kaneva.mykaneva.widgets
{
    public partial class WidgetViewContainer : ContainerControls
    {
        #region Declarations

        private int pageCount = 0;
        private Community community = new Community();
        
        #endregion

        #region Page Load
        
        protected void Page_Load(object sender, EventArgs e)
        {
            // Load control from property ControlToLoad
            if (CurrentCommunity != null && CurrentCommunity.CommunityId > 0)
            {
                ConfigureSecurity ();
                
                // If no data page provided (data not loaded via Ajax) then
                // don't display the loading image
                if (this.AjaxDataPage.Length == 0)
                {
                    divLoading.Visible = false;
                }

                //set the on action for the edit button
                hlEdit.NavigateUrl = this.EditButtonLink + "?communityId=" + this.CurrentCommunity.CommunityId;

                //set the visibilty for the header area
                widget_head.Visible = true; // this.HideHeader;

                //set the title for the widget
                divTitle.InnerText = this.Title;

                //set the visibility for the edit button
                divEditArea.Visible = this.IsAllowedToEdit;

                // Add the community id tot he paramaters and any other values specified
                // to be passed to data page
                if (this.AjaxDataPageParams.Length > 0)
                {
                    this.AjaxDataPageParams += "&communityId=" + this.CurrentCommunity.CommunityId;
                }
                else
                {
                    this.AjaxDataPageParams = "communityId=" + this.CurrentCommunity.CommunityId;
                }

                // Load the control
                LoadControl();
            }
            else
            {
                this.AjaxDataPage = "";
                this.Visible = false;
            }
        }

        #endregion

        #region Helper Methods

        protected override void LoadControl()
        {
            ModuleViewBaseControl userControl = (ModuleViewBaseControl)Page.LoadControl("~/" + base.ControlToLoad);
            userControl.CommunityId = this.CurrentCommunity.CommunityId;

            // Place web user control to place holder control
            phWidgetShell.Controls.Add(userControl);
        }

        private void ConfigureSecurity ()
        {
            //check to see if they are the owner
            this.IsCommunityOwner = this.CurrentCommunity.CreatorId == KanevaWebGlobals.CurrentUser.UserId;

            //get the appropriate privilege id based on the community type
            int privilegeId = 0;
            switch (CurrentCommunity.CommunityTypeId)
            {
                case (int)CommunityType.USER:
                    privilegeId = (int)SitePrivilege.ePRIVILEGE.USER_PROFILE_ADMIN;
                    break;
                case (int)CommunityType.COMMUNITY:
                case (int)CommunityType.APP_3D:
                    privilegeId = (int)SitePrivilege.ePRIVILEGE.COMMUNITY_PROFILE_ADMIN;
                    break;
            }

            //check security level
            switch (KanevaWebGlobals.CheckUserAccess (privilegeId))
            {
                case (int) SitePrivilege.eACCESS_LEVEL.ACCESS_FULL:
                    this.IsAdministrator = true;
                    break;
                case (int) SitePrivilege.eACCESS_LEVEL.ACCESS_READ:
                    break;
                case (int) SitePrivilege.eACCESS_LEVEL.ACCESS_NONE:
                    break;
                default:
                    break;
            }
            //set editing right
            if (this.IsAdministrator || this.IsCommunityOwner)
            {
                this.IsAllowedToEdit = true;
            }
        }

        #endregion Helper Methods


        #region Attributes

        private bool IsCommunityOwner
        {
            set
            {
                ViewState["isCommunityOwner"] = value;
            }
            get
            {
                if (ViewState["isCommunityOwner"] == null)
                {
                    ViewState["isCommunityOwner"] = false;
                }
                return (bool) ViewState["isCommunityOwner"];
            }
        }

        private bool IsAllowedToEdit
        {
            set
            {
                ViewState["isAllowedToEdit"] = value;
            }
            get
            {
                if (ViewState["isAllowedToEdit"] == null)
                {
                    ViewState["isAllowedToEdit"] = false;
                }
                return (bool) ViewState["isAllowedToEdit"];
            }
        }

        private bool IsAdministrator
        {
            set
            {
                ViewState["isAdministrator"] = value;
            }
            get
            {
                if (ViewState["isAdministrator"] == null)
                {
                    ViewState["isAdministrator"] = false;
                }
                return (bool) ViewState["isAdministrator"];
            }
        }

        public int PageCount
        {
            set
            {
                pageCount = value;
            }
            get
            {
                return pageCount;
            }
        }

        public Community CurrentCommunity
        {
            get
            {
                try
                {
                    if (this.community.CommunityId == 0)
                    {
                        this.community = (Community) (HttpContext.Current.Items["community"]);
                    }
                }
                catch { }
                
                return this.community;
            }
        }

        #endregion 

        #region Web Form Designer generated code

        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new EventHandler(this.Page_Load);
        }

        #endregion

    }
}