///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.mykaneva.widgets
{
    public partial class ContestActions : BasePage
    {
        #region Declerations

        private UInt64 contestId = 0;
        private UInt64 contestEntryId = 0;
        private int userId = 0;
        private string action = "";
        private ActionToPerform currentAction;

        #endregion Delcaration

        protected void Page_Load (object sender, EventArgs e)
        {
            //clears local cache so data is always new
            Response.ContentType = "text/plain";
            Response.CacheControl = "no-cache";
            Response.AddHeader ("Pragma", "no-cache");
            Response.Expires = -1;

            GetRequestValues ();

            PerformAction ();
        }

        private void PerformAction ()
        {
            if (ContestId > 0)
            {
                if (CurrentAction == ActionToPerform.Follow)
                {
                    FollowContest ();
                }
                else if (CurrentAction == ActionToPerform.Vote)
                {
                    int numVotes = 0;
                    if (VoteForContestEntry (ref numVotes))
                    {
                        Response.Write (numVotes);
                    }
                    else
                    {
                        Response.Write ("");
                    }
                }
            }
        }

        private bool VoteForContestEntry (ref int numVotes)
        {
            try
            {                 
                Contest c = GetContestFacade.GetContest (ContestId);

                if (c.ContestId > 0)
                {
                    if (ContestEntryId > 0)
                    {
                        // verify user can vote
                        if (!CanUserVoteOnContest)
                        {
                            return false;
                        }

                        // If user has already voted on this contest, they can't vote again
                        if (GetContestFacade.HasUserVotedOnContestEntry (ContestEntryId, KanevaWebGlobals.CurrentUser.UserId))
                        {
                            return false;
                        }

                        // Verify user voting is not the owner
                        if (KanevaWebGlobals.CurrentUser.UserId == c.Owner.UserId)
                        {   // Owner can't vote until contest is over
                            return false;
                        }

                        // Vote
                        numVotes = GetContestFacade.InsertVote (KanevaWebGlobals.CurrentUser.UserId, KanevaWebGlobals.CurrentUser.IpAddress, c.ContestId, ContestEntryId);

                        if (numVotes > 0)
                        {
                            // Sent user voted Blast
                            /* Removing blast for vote per Prod Mgmt.
                            GetBlastFacade.SendContestVotedBlast (KanevaWebGlobals.CurrentUser.UserId,
                                ResolveUrl ("~/mykaneva/contests.aspx?contestId=" + c.ContestId.ToString ()),
                                c.Title, c.PrizeAmount, KanevaWebGlobals.CurrentUser.Username,
                                KanevaWebGlobals.CurrentUser.NameNoSpaces, KanevaWebGlobals.CurrentUser.ThumbnailSmallPath);
                            */
                            return true;
                        }
                    }
                }
            }
            catch {}

            return false;
        }

        private void FollowContest ()
        {
            if (FollowAction.ToUpper () == "FOLLOW")
            {
                if (GetContestFacade.FollowContest (ContestId, KanevaWebGlobals.CurrentUser.UserId) == 1)
                {
                    Response.Write ("Unfollow");
                    return;
                }
            }
            else if (FollowAction.ToUpper () == "UNFOLLOW")
            {
                if (GetContestFacade.UnFollowContest (ContestId, KanevaWebGlobals.CurrentUser.UserId) == 1)
                {
                    Contest c = GetContestFacade.GetContest (ContestId);

                    // Send follow Blast
                    /* Removing blast for follow per Prod Mgmt
                    GetBlastFacade.SendContestFollowingBlast (KanevaWebGlobals.CurrentUser.UserId,
                        ResolveUrl ("~/mykaneva/contests.aspx?contestId=" + ContestId.ToString ()),
                        c.Title, c.PrizeAmount, KanevaWebGlobals.CurrentUser.Username,
                        KanevaWebGlobals.CurrentUser.NameNoSpaces, KanevaWebGlobals.CurrentUser.ThumbnailSmallPath);
                    */

                    Response.Write ("Follow");
                    return;
                }
            }
        }

        private void GetRequestValues ()
        {
            // Get the contest Id
            if (Request["cId"] != null)
            {
                try
                {
                    ContestId = Convert.ToUInt64 (Request["cId"]);
                }
                catch { }
            }

            // Get the contest Entry Id
            if (Request["ceId"] != null)
            {
                try
                {
                    ContestEntryId = Convert.ToUInt64 (Request["ceId"]);
                }
                catch { }
            }

            // Get the action to perform
            if (Request["a"] != null)
            {
                try
                {
                    FollowAction = Request["a"].ToString ();
                }
                catch { }
            }

            CurrentAction = ActionToPerform.Follow;
            if (Request["ca"] != null)
            {
                try
                {
                    CurrentAction = (ActionToPerform)(Convert.ToInt32(Request["ca"]));
                }
                catch 
                {
                    CurrentAction = ActionToPerform.Follow;
                }
            }
        }

        #region Properties

        private bool CanUserVoteOnContest
        {
            get
            {
                if (KanevaWebGlobals.CurrentUser.StatusId.Equals ((int) Constants.eUSER_STATUS.REGVALIDATED))
                {
                    return true;
                }
                return false;
            }
        }

        private int UserId
        {
            set { this.userId = value; }
            get { return this.userId; }
        }

        private UInt64 ContestId
        {
            set { this.contestId = value; }
            get { return this.contestId; }
        }

        private UInt64 ContestEntryId
        {
            set { this.contestEntryId = value; }
            get { return this.contestEntryId; }
        }

        private string FollowAction
        {
            set { this.action = value; }
            get { return this.action; }
        }

        private ActionToPerform CurrentAction
        {
            set { this.currentAction = value; }
            get { return this.currentAction; }
        }

        private enum ActionToPerform
        {
            Follow = 0,
            Vote = 1
        }
        
        #endregion Properties
    }
}