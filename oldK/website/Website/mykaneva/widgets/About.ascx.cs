///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Kaneva.BusinessLayer.BusinessObjects;
using log4net;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.mykaneva.widgets
{
    public partial class About : Kaneva.mykaneva.widgets.ModuleViewBaseControl
    {
        protected void Page_Load (object sender, EventArgs e)
        {
            try
            {
                GetRequestValues ();
                CheckUserAccess ();
                SetEditOptions ();
                ShowAboutText ();

                if (CurrentCommunity.CommunityTypeId == (int)CommunityType.APP_3D)
                {
                    // Show "Play Now"
                    divPlayNow.Visible = true;
                    ConfigureCommunityMeetMe3D (btnMeet3D, CurrentCommunity.WOK3App.GameId, CurrentCommunity.CommunityId, CurrentCommunity.Name, TrackingRequestId);
                }

                if (!Page.ClientScript.IsStartupScriptRegistered("AboutLineHeightFix"))
                {
                    Page.ClientScript.RegisterStartupScript(Page.GetType(), "AboutLineHeightFix", "<script type=\"text/javascript\">adjustLineHeight()</script>");
                }
            }
            catch (Exception ex)
            {
                m_logger.Error ("Error loading Blog widget. ", ex);

                divNoData.InnerText = "We're sorry but the data could not be retrieved.";
                divNoData.Visible = true;
            }
        }

        private void ShowAboutText ()              
        {
            try
            {
                string text = GetCommunityFacade.GetCommunityAboutText (CommunityId);

                if (text.Length.Equals (0))
                {
                    // Per Billy/AS email don't show this.
                   divNoData.Visible = false;
                }
                else
                {
                    divNoData.Visible = false;
                }

                divContent.InnerHtml = text;
                txtAbout.Text = text;
            }
            catch { }
        }

        private void GetRequestValues ()
        {
            // Get the community Id
            if (Request["communityId"] != null)
            {
                try
                {
                    CommunityId = Convert.ToInt32 (Request["communityId"]);
                }
                catch { }
            }

            // Get the user Id from the parameters 
            if (Request["profileOwnerId"] != null)
            {
                try
                {
                    ProfileOwnerId = Convert.ToInt32 (Request["profileOwnerId"]);
                }
                catch { }
            }
            else
            {
                try
                {
                    ProfileOwnerId = GetCommunityFacade.GetCommunity (CommunityId).CreatorId;
                }
                catch { }
            }
        }

        private void SetEditOptions ()
        {
            divEdit.Visible = false;
            if (this.isProfileOwner || this.isProfileAdmin)
            {
                divEdit.Visible = true;
            }
        }

        /// <summary>
        /// Checks the current user's access level
        /// </summary>
        private void CheckUserAccess ()
        {
            //see if they are the profile/community owner
            isProfileOwner = KanevaWebGlobals.CurrentUser.UserId.Equals (ProfileOwnerId);

            if (!isProfileOwner)
            {
                //check the privilege level to see if they are an admin
                this.isProfileAdmin = HasWritePrivileges ((int) SitePrivilege.ePRIVILEGE.COMMUNITY_PROFILE_ADMIN);
            }
        }

        /// <summary>
        /// Send a Blast
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkSave_Click (object sender, EventArgs e)
        {
            txtAbout.Text = txtAbout.Text.Trim ();

            if (Request.IsAuthenticated)
            {
                // Check for any inject scripts
                if (KanevaWebGlobals.ContainsInjectScripts (txtAbout.Text))
                {
                    m_logger.Warn ("User " + KanevaWebGlobals.CurrentUser.UserId.ToString () + " tried to script from IP " + Common.GetVisitorIPAddress());
                    ShowErrorOnStartup ("Your input contains invalid scripting, please remove script code and try again.", true);
                    return;
                }

                // Check to make sure user did not enter any "potty mouth" words
                if (KanevaWebGlobals.isTextRestricted (txtAbout.Text, Constants.eRESTRICTION_TYPE.POTTY_MOUTH))
                {
                    ShowErrorOnStartup (Constants.VALIDATION_REGEX_RESTRICTED_WORD_MESSAGE);
                    return;
                }

                KlausEnt.KEP.Kaneva.channel.CommunityPage commPage = (KlausEnt.KEP.Kaneva.channel.CommunityPage) this.Page;

                // Verify user is admin and can enter text
                if (KanevaWebGlobals.CurrentUser.UserId == GetCommunityFacade.GetCommunity (CommunityId).CreatorId ||
                    commPage.UserIsAdmin)    
                {                             
                    // Save the about text
                    GetCommunityFacade.UpdateCommunityAboutText (CommunityId, txtAbout.Text);

                    ShowAboutText ();
                }

                if (CurrentCommunity.CommunityTypeId == (int) CommunityType.APP_3D)
                {
                    // This is used to make sure the tab does not go back to the default tab if it is not the About tab 
                    string js = "";
                    string tabName = "";
                    try
                    {
                        KlausEnt.KEP.Kaneva.usercontrols.TabbedControlLoader tcLoader = (KlausEnt.KEP.Kaneva.usercontrols.TabbedControlLoader) this.Parent.NamingContainer;
                        //tcLoader.TabValues.IndexOf (;
                        foreach (TabDisplay tab in tcLoader.TabValues)
                        {                                                          
                            if (tab.TabId.Equals ((int) Constants.eTab.About))
                            {
                                js = "DisplayHiddenControl('wrapper_" + tab.TabName.ToLower () + "');";
                                tabName = "tab" + tab.TabName;
                                break;
                            }
                        }
                    }
                    catch { }

                    Page.ClientScript.RegisterStartupScript (GetType (), "SelectTab", "<script type=\"text/javascript\">SetSelectedTab($('" + tabName + "'));" + js + "</script>");
                }
            }
        }

 

        #region Properties

        public Community CurrentCommunity
        {
            get
            {
                try
                {
                    if (this.community.CommunityId == 0)
                    {
                        if (HttpContext.Current.Items["community"] != null)
                        {
                            this.community = (Community) (HttpContext.Current.Items["community"]);
                        }
                    }
                }
                catch
                {
                    return new Community (); ;
                }

                return this.community;
            }
        }

        /// <summary>
        /// Tracking Request for the Play Now button
        /// </summary>
        private string TrackingRequestId
        {
            set
            {
                ViewState["TrackingRequestId"] = value;
            }
            get
            {
                if (ViewState["TrackingRequestId"] == null && KanevaWebGlobals.CurrentUser.UserId > 0)
                {
                    // Only do a passthrough of RTSID parameter if this is a newly created world and we're coming directly from the Create Your World page.
                    // This is a special case, in which traversal to world should be attributed to creation.
                    if (Request[Constants.cREQUEST_TRACKING_URL_PARAM] != null && GetUserFacade.GetTrackingRequestType(Request[Constants.cREQUEST_TRACKING_URL_PARAM].ToString()) == Constants.cREQUEST_TRACKING_REQUEST_TYPE_ID_WEB_WORLD_CREATION)
                    {
                        ViewState["TrackingRequestId"] = Request[Constants.cREQUEST_TRACKING_URL_PARAM].ToString();
                    }
                    else
                    {
                        string requestId = GetUserFacade.InsertTrackingRequest(Constants.cREQUEST_TRACKING_REQUEST_TYPE_ID_WORLD_PROFILE_PLAY_NOW, KanevaWebGlobals.CurrentUser.UserId, 0, 0);
                        ViewState["TrackingRequestId"] = GetUserFacade.InsertTrackingTypeSent(requestId, Constants.cREQUEST_TRACKING_MESSAGE_TYPE_ID_WEBSITE, KanevaWebGlobals.CurrentUser.UserId);
                    }
                }
                return (ViewState["TrackingRequestId"] ?? string.Empty).ToString();
            }
        }

        #endregion Properties

        #region Declerations

        private bool isProfileOwner = false;
        private bool isProfileAdmin = false;
        private Community community = new Community ();

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

        #endregion Declerations
    }
}