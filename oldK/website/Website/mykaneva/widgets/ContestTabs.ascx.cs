///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva.mykaneva
{
    public partial class ContestTabs : Kaneva.mykaneva.widgets.ModuleViewBaseControl
    {
        protected void Page_Load (object sender, EventArgs e)
        {
            ConfigureTabbedControl ();
        }

        private void ConfigureTabbedControl ()
        {
            // Create the tab control
            IList<TabDisplay> tabValues = new List<TabDisplay> ();

            tabValues.Add (LoadTab (ContestTabType.CONTESTS, true));
            tabValues.Add (LoadTab (ContestTabType.MY_CONTESTS));

            //configure the tabbed control
            TclMainPanel.TabValues = tabValues;
            TclMainPanel.HideEmptyTabs = false;
        }

        private TabDisplay LoadTab (string tabType)
        {
            return LoadTab (tabType, false);
        }
        private TabDisplay LoadTab (string tabType, bool isDefaultTab)
        {
            TabDisplay tab = new TabDisplay ();

            switch (tabType)
            {
                case ContestTabType.CONTESTS:
                    {
                        tab.TabName = ContestTabType.CONTESTS;
                        tab.AjaxDataPage = ResolveUrl ("~/mykaneva/widgets/ContestData.aspx");
                        tab.AjaxDataPageParams = "?type=all";
                        tab.ControlToLoad = "";
                        tab.AllowAlphaSorting = false;
                        tab.AllowSorting = false;
                        break;
                    }
                case ContestTabType.MY_CONTESTS:
                    {
                        tab.TabName = ContestTabType.MY_CONTESTS;
                        tab.AjaxDataPage = ResolveUrl ("~/mykaneva/widgets/ContestMyData.aspx");
                        tab.AjaxDataPageParams = "?type=my";
                        tab.ControlToLoad = "";
                        tab.AllowAlphaSorting = false;
                        tab.AllowSorting = false;
                        break;
                    }
            }

            if (isDefaultTab) TclMainPanel.DefaultTab = tab;
            return tab;
        }


        public class ContestTabType
        {
            public const string CONTESTS = "Contests";
            public const string MY_CONTESTS = "My Contests";
        }
    }
}