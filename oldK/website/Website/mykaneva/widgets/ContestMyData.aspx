﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ContestMyData.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.mykaneva.widgets.ContestMyData" %>

<asp:Literal id="litJS" runat="server"></asp:Literal>

<script type="text/javascript">
	function ScrollTo(node) {
		try{window.scrollTo(0, $(node).offsetTop);}
		catch (err){}
	}

</script>
<div id="sort" class="clearit">Jump to: 
	<a href="#my" onclick="javascript:ScrollTo('my')">Your Contests</a> | 
	<a href="#enter" onclick="javascript:ScrollTo('enter')">Entered</a> | 
	<a href="#follow" onclick="javascript:ScrollTo('follow')">Following</a> | 
	<a href="#vote" onclick="javascript:ScrollTo('vote')">Voted</a> 
</div>

<div id="mycontests">

	<div class="secheading topsection" name="my" id="my">YOUR CONTESTS</div>
	<asp:repeater id="rptMyContests" runat="server" onitemdatabound="rptMyContests_ItemDataBound">
		<itemtemplate>
			<div class='contest <%# Convert.ToBoolean(DataBinder.Eval (Container.DataItem, "IsContestActive")) ? "" : "ended" %>'>
				<div class="left">
					<div class="amount">
						<div class="value"><%# DataBinder.Eval (Container.DataItem, "PrizeAmount") %></div>
						<div>CREDITS</div>
					</div>
					<div class="time">
						<div class="label">TIME<br />LEFT</div>
						<div id="divTimeRemaining" runat="server" class="remaining"><%# DataBinder.Eval (Container.DataItem, "TimeRemaining") %></div>
					</div>
					<div class="clearit"><a class="buttonnarrow" href="javascript:void(0);" id="btnFollowMyContests" runat="server"><span class="follow" id="followBtnText" runat="server">Follow</span></a></div>
				</div>
				<div class="middle">
					<div class="title"><a href="javascript:void(0);" onclick="ShowDetails(<%# DataBinder.Eval (Container.DataItem, "ContestId") %>);"><%# DataBinder.Eval (Container.DataItem, "Title") %></a></div>
					<div class="data">
						<div class="winner" id="divMyContestsWinner" runat="server" visible="false">WINNER ANNOUNCED!</div>
						<div class="entries"><%# DataBinder.Eval (Container.DataItem, "NumberOfEntries") %> Entries</div>
						<div class="votes"><%# DataBinder.Eval (Container.DataItem, "NumberOfVotes") %> Votes</div>
					</div>	
					<div class="desc" id="divMyContestsDesc" runat="server"><%# TruncateWithEllipsis(DataBinder.Eval (Container.DataItem, "Description").ToString(), 136) %></div>
				</div>
				<div class="right">
					
					<div class="framesize-xsmall">
						<div id="Div2" class="restricted" runat="server" visible='false'></div>
						<div class="frame">
							<div class="imgconstrain">
								<a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "Owner.UserName").ToString ())%>>
									<img id="Img2" runat="server" src='<%#GetProfileImageURL (DataBinder.Eval(Container.DataItem, "Owner.ThumbnailSquarePath").ToString (), "sq", DataBinder.Eval(Container.DataItem, "Owner.Gender").ToString (), Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "Owner.FacebookSettings.UseFacebookProfilePicture")), Convert.ToUInt64(DataBinder.Eval(Container.DataItem, "Owner.FacebookSettings.FacebookUserId")) )%>' border="0"/>
								</a>
							</div>	
						</div>
					</div>

					<div class="creator">Created by:<br /><span class="name"><a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "Owner.UserName").ToString ())%>><%# DataBinder.Eval (Container.DataItem, "Owner.Username") %></a></span></div>
				</div>
			</div>
		</itemtemplate>
		<separatortemplate><hr class="divider" /></separatortemplate>
	</asp:repeater>
	<div class="nodata" id="divMyContestNoData" runat="server" visible="false">You have not Created any contests.</div>


	<div class="section_spacer"></div>
	<div class="secheading" name="enter" id="enter">CONTESTS YOU'VE ENTERED</div>
	<asp:repeater id="rptContestsEntered" runat="server" onitemdatabound="rptContestsEntered_ItemDataBound">
		<itemtemplate>
			<div class='contest <%# Convert.ToBoolean(DataBinder.Eval (Container.DataItem, "IsContestActive")) ? "" : "ended" %>'>
				<div class="left">
					<div class="amount">
						<div class="value"><%# DataBinder.Eval (Container.DataItem, "PrizeAmount") %></div>
						<div>CREDITS</div>
					</div>
					<div class="time">
						<div class="label">TIME<br />LEFT</div>
						<div id="divTimeRemainingEntered" runat="server" class="remaining"><%# DataBinder.Eval (Container.DataItem, "TimeRemaining") %></div>
					</div>
					<div class="clearit"><a class="buttonnarrow" href="javascript:void(0);" id="btnFollowEntered" runat="server"><span class="follow" id="followBtnTextEntered" runat="server">Follow</span></a></div>
				</div>
				<div class="middle">
					<div class="title"><a href="javascript:void(0);" onclick="ShowDetails(<%# DataBinder.Eval (Container.DataItem, "ContestId") %>);"><%# DataBinder.Eval (Container.DataItem, "Title") %></a></div>
					<div class="data">
						<div class="winner" id="divEnterWinner" runat="server" visible="false">WINNER ANNOUNCED!</div>
						<div class="entries"><%# DataBinder.Eval (Container.DataItem, "NumberOfEntries") %> Entries</div>
						<div class="votes"><%# DataBinder.Eval (Container.DataItem, "NumberOfVotes") %> Votes</div>
					</div>	
					<div class="desc" id="divEnteredDesc" runat="server"><%# TruncateWithEllipsis(DataBinder.Eval (Container.DataItem, "Description").ToString(), 136) %></div>
				</div>
				<div class="right">
					
					<div class="framesize-xsmall">
						<div id="Div3" class="restricted" runat="server" visible='false'></div>
						<div class="frame">
							<div class="imgconstrain">
								<a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "Owner.UserName").ToString ())%>>
									<img id="Img1" runat="server" src='<%#GetProfileImageURL (DataBinder.Eval(Container.DataItem, "Owner.ThumbnailSmallPath").ToString (), "sm", DataBinder.Eval(Container.DataItem, "Owner.Gender").ToString (), Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "Owner.FacebookSettings.UseFacebookProfilePicture")), Convert.ToUInt64(DataBinder.Eval(Container.DataItem, "Owner.FacebookSettings.FacebookUserId")) )%>' border="0"/>
								</a>
							</div>	
						</div>
					</div>

					<div class="creator">Created by:<br /><span class="name"><a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "Owner.UserName").ToString ())%>><%# DataBinder.Eval (Container.DataItem, "Owner.Username") %></a></span></div>
				</div>
			</div>
		</itemtemplate>
		<separatortemplate><hr class="divider" /></separatortemplate>
	</asp:repeater>
	<div class="nodata" id="divEnteredNoData" runat="server" visible="false">You have not Entered any contests.</div>


	<div class="section_spacer"></div>
	<div class="secheading" name="follow" id="follow">CONTESTS YOU'RE FOLLOWING</div>
	<asp:repeater id="rptContestsFollowing" runat="server" onitemdatabound="rptContestsFollowing_ItemDataBound">
		<itemtemplate>
			<div class='contest <%# Convert.ToBoolean(DataBinder.Eval (Container.DataItem, "IsContestActive")) ? "" : "ended" %>'>
				<div class="left">
					<div class="amount">
						<div class="value"><%# DataBinder.Eval (Container.DataItem, "PrizeAmount") %></div>
						<div>CREDITS</div>
					</div>
					<div class="time">
						<div class="label">TIME<br />LEFT</div>
						<div id="divTimeRemainingFollow" runat="server" class="remaining"><%# DataBinder.Eval (Container.DataItem, "TimeRemaining") %></div>
					</div>
					<div class="clearit"><a class="buttonnarrow" href="javascript:void(0);" id="btnFollowFollowing" runat="server"><span class="follow" id="followBtnTextFollow" runat="server">Follow</span></a></div>
				</div>
				<div class="middle">
					<div class="title"><a href="javascript:void(0);" onclick="ShowDetails(<%# DataBinder.Eval (Container.DataItem, "ContestId") %>);"><%# DataBinder.Eval (Container.DataItem, "Title") %></a></div>
					<div class="data">
						<div class="winner" id="divFollowWinner" runat="server" visible="false">WINNER ANNOUNCED!</div>
						<div class="entries"><%# DataBinder.Eval (Container.DataItem, "NumberOfEntries") %> Entries</div>
						<div class="votes"><%# DataBinder.Eval (Container.DataItem, "NumberOfVotes") %> Votes</div>
					</div>	
					<div class="desc" id="divFollowingDesc" runat="server"><%# TruncateWithEllipsis(DataBinder.Eval (Container.DataItem, "Description").ToString(), 136) %></div>
				</div>
				<div class="right">
					
					<div class="framesize-xsmall">
						<div id="Div5" class="restricted" runat="server" visible='false'></div>
						<div class="frame">
							<div class="imgconstrain">
								<a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "Owner.UserName").ToString ())%>>
									<img id="Img3" runat="server" src='<%#GetProfileImageURL (DataBinder.Eval(Container.DataItem, "Owner.ThumbnailSmallPath").ToString (), "sm", DataBinder.Eval(Container.DataItem, "Owner.Gender").ToString (), Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "Owner.FacebookSettings.UseFacebookProfilePicture")), Convert.ToUInt64(DataBinder.Eval(Container.DataItem, "Owner.FacebookSettings.FacebookUserId")) )%>' border="0"/>
								</a>
							</div>	
						</div>
					</div>

					<div class="creator">Created by:<br /><span class="name"><a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "Owner.UserName").ToString ())%>><%# DataBinder.Eval (Container.DataItem, "Owner.Username") %></a></span></div>
				</div>
			</div>
		</itemtemplate>
		<separatortemplate><hr class="divider" /></separatortemplate>
	</asp:repeater>
	<div class="nodata" id="divFollowingNoData" runat="server" visible="false">You are not Following any contests.</div>


	<div class="section_spacer"></div>
	<div class="secheading" name="vote" id="vote">CONTESTS YOU'VE VOTED ON</div>
	<asp:repeater id="rptContestsVoted" runat="server" onitemdatabound="rptContestsVoted_ItemDataBound">
		<itemtemplate>
			<div class='contest <%# Convert.ToBoolean(DataBinder.Eval (Container.DataItem, "IsContestActive")) ? "" : "ended" %>'>
				<div class="left">
					<div class="amount">
						<div class="value"><%# DataBinder.Eval (Container.DataItem, "PrizeAmount") %></div>
						<div>CREDITS</div>
					</div>
					<div class="time">
						<div class="label">TIME<br />LEFT</div>
						<div id="divTimeRemainingVoted" runat="server" class="remaining"><%# DataBinder.Eval (Container.DataItem, "TimeRemaining") %></div>
					</div>
					<div class="clearit"><a class="buttonnarrow" href="javascript:void(0);" id="btnFollowVoted" runat="server"><span class="follow" id="followBtnTextVoted" runat="server">Follow</span></a></div>
				</div>
				<div class="middle">
					<div class="title"><a href="javascript:void(0);" onclick="ShowDetails(<%# DataBinder.Eval (Container.DataItem, "ContestId") %>);"><%# DataBinder.Eval (Container.DataItem, "Title") %></a></div>
					<div class="data">
						<div class="winner" id="divVoteWinner" runat="server" visible="false">WINNER ANNOUNCED!</div>
						<div class="entries"><%# DataBinder.Eval (Container.DataItem, "NumberOfEntries") %> Entries</div>
						<div class="votes"><%# DataBinder.Eval (Container.DataItem, "NumberOfVotes") %> Votes</div>
					</div>	
					<div class="desc" id="divVotedDesc" runat="server"><%# TruncateWithEllipsis(DataBinder.Eval (Container.DataItem, "Description").ToString(), 136) %></div>
				</div>
				<div class="right">
					
					<div class="framesize-xsmall">
						<div id="Div7" class="restricted" runat="server" visible='false'></div>
						<div class="frame">
							<div class="imgconstrain">
								<a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "Owner.UserName").ToString ())%>>
									<img id="Img4" runat="server" src='<%#GetProfileImageURL (DataBinder.Eval(Container.DataItem, "Owner.ThumbnailSmallPath").ToString (), "sm", DataBinder.Eval(Container.DataItem, "Owner.Gender").ToString (), Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "Owner.FacebookSettings.UseFacebookProfilePicture")), Convert.ToUInt64(DataBinder.Eval(Container.DataItem, "Owner.FacebookSettings.FacebookUserId")) )%>' border="0"/>
								</a>
							</div>	
						</div>
					</div>

					<div class="creator">Created by:<br /><span class="name"><a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "Owner.UserName").ToString ())%>><%# DataBinder.Eval (Container.DataItem, "Owner.Username") %></a></span></div>
				</div>
			</div>
		</itemtemplate>
		<separatortemplate><hr class="divider" /></separatortemplate>
	</asp:repeater>
	<div class="nodata" id="divVotedNoData" runat="server" visible="false">You have not Voted on any contests.</div>



</div>
<div class="clearit" ></div>
