///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.mykaneva.widgets
{
    public partial class InviteFriend : BasePage
    {
        #region Declerations

        private int friendId = 0;

        #endregion Delcaration
        
        protected void Page_Load (object sender, EventArgs e)
        {
            //clears local cache so data is always new
            Response.ContentType = "text/plain";
            Response.CacheControl = "no-cache";
            Response.AddHeader ("Pragma", "no-cache");
            Response.Expires = -1;

            try
            {
                GetRequestValues ();

                SendFriendRequest ();
            }
            catch
            {
                Response.Write ("");
            }
        }

        public void SendFriendRequest ()
        {
            int userId = KanevaWebGlobals.CurrentUser.UserId;
            string strOut = "";

            // Can't friend yourself
            if (userId.Equals (friendId))
            {
                 // Do nothing
            }
            // Add them as a friend
            int ret = GetUserFacade.InsertFriendRequest (userId, friendId,
                Page.Request.CurrentExecutionFilePath, Global.RequestRabbitMQChannel());
            if (ret.Equals (0))	// already a friend
            { }
            else if (ret.Equals (2))  // pending friend request
            { }
            else if (ret.Equals (1))  // success
            {
                // send request email
                MailUtilityWeb.SendFriendRequestEmail (userId, friendId);
                strOut = friendId.ToString();
            }
            Response.Write (strOut);
        }

        private void GetRequestValues ()
        {
            // Get the User Id to send request to
            if (Request["uId"] != null)
            {
                try
                {
                    FriendId = Convert.ToInt32 (Request["uId"]);
                }
                catch { }
            }
        }

        private int FriendId
        {
            set { this.friendId = value; }
            get { return this.friendId; }
        }
    }
}