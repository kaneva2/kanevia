///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.mykaneva.widgets
{
    public partial class CommunityOwnerData : BasePage
    {
        #region Declerations

        private int communityId = 0;

        #endregion Declerations

        protected void Page_Load (object sender, EventArgs e)
        {
            //clears local cache so data is always new
            Response.ContentType = "text/plain";
            Response.CacheControl = "no-cache";
            Response.AddHeader("Pragma", "no-cache");
            Response.Expires = -1;

            BindData ();
        }

        #region Functions

        private void BindData ()
        {
            // Get the community owner 
            User user = new UserFacade ().GetCommunityOwnerByCommunityId (this.CommunityId);

            divUserName.InnerText = user.Username;
            divDisplayName.InnerText = user.DisplayName;
            divLocation.InnerText = user.Location;
            divAge.InnerText = user.Age.ToString ();
            divGender.InnerText = user.Gender;
            divOnline.InnerText = user.Online == 1 ? "online" : "offline";

            // Get list of community moderators
            GetModeratorList ();
        }

        private void GetModeratorList ()
        {
            try
            {
                PagedList<CommunityMember> communityModerators = new CommunityFacade ().GetCommunityMembers (this.CommunityId, 0,
                    false, false, false, false, " account_type_id = " + (int) CommunityMember.CommunityMemberAccountType.MODERATOR,
                    "", 1, 10, true, "");

                if (communityModerators.Count > 0)
                {
                    rpModerators.DataSource = communityModerators;
                    rpModerators.DataBind ();
                }
                else
                {
                    divModerators.Visible = false;
                }
            }
            catch { }
        }

        private void GetRequestParams ()
        {
            try
            {
                if (Request["communityId"] != null)
                {
                    this.CommunityId = Convert.ToInt32 (Request["communityId"]);
                }
            }
            catch { }
        }

        #endregion Functions

        #region Properties

        private int CommunityId
        {
            get
            {
                return this.communityId;
            }
            set
            {
                this.communityId = value;
            }
        }

        #endregion Properties
    }
}
      