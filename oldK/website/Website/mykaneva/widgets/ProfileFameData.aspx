﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProfileFameData.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.mykaneva.widgets.ProfileFameData" %>

<asp:Literal id="litJS" runat="server"></asp:Literal>

<!--Fame HERE--->											
<div id="userFame">

    <div class="fameStats">											    											       
        <div class="raves">Raves: <asp:label id="lblRaves" runat="server" text="0"></asp:label></div>
        <div class="counts"><div class="badges">Badges <span id="spnUserBadges" runat="server"></span> of <span id="spnTotalBadges" runat="server"></span></div>
			<div class="points"><span id="spnUserPoints" runat="server"></span> of <span id="spnTotalPoints" runat="server"></span> points</div></div>
    </div>	

	<div class="dataContainer">
	<asp:repeater id="rptFameApps" runat="server">
		<itemtemplate>
			<div class="dataRow" id="rowContainer" runat="server">
				<div class="framesize-medium">
					<div class="frame">
						<div class="imgconstrain">
							<a href='<%# GetFameChannelUrl (DataBinder.Eval (Container.DataItem, "NameNoSpaces").ToString (),Convert.ToInt32(DataBinder.Eval (Container.DataItem, "CommunityId")))%>'>
								<img runat="server" src='<%# GetBroadcastChannelImageURL (DataBinder.Eval(Container.DataItem, "ThumbnailMediumPath").ToString (), "me") %>' border="0" id="Img5"/>
							</a>
						</div>	
					</div>
				</div>
				<div class="info">		    
					<div class="name"><a href='<%# GetFameChannelUrl (DataBinder.Eval (Container.DataItem, "NameNoSpaces").ToString (),Convert.ToInt32(DataBinder.Eval (Container.DataItem, "CommunityId")))%>'><%# DataBinder.Eval(Container.DataItem, "Name").ToString() %></a></div>
					<div class="title"><%# GetTitle (Convert.ToInt32(DataBinder.Eval (Container.DataItem, "CommunityId"))) %></div>
					<div class="level"><%# GetLevel (Convert.ToInt32(DataBinder.Eval (Container.DataItem, "CommunityId"))) %></div>
				</div>
				<div class="gameBadges">		 
					<div class="count"><%# GetAchievementCounts (Convert.ToInt32(DataBinder.Eval (Container.DataItem, "CommunityId"))) %></div>
					<div class="points"><%# GetPointValues (Convert.ToInt32(DataBinder.Eval (Container.DataItem, "CommunityId"))) %></div>
					<div class="thumbs"><%# GetMostRecentAchievementThumbnails (Convert.ToInt32(DataBinder.Eval (Container.DataItem, "CommunityId")), 5) %></div>
					<div class="more"><%# GetGameFameScript (DataBinder.Eval(Container.DataItem, "CommunityId").ToString()) %></div>
				</div>					
			</div>
		</itemtemplate>						
	</asp:repeater>

	<asp:repeater id="rptFameAppsSmall" runat="server">
		<itemtemplate>
			<div class="dataRowSideLight" id="Div1">
				<div class="framesize-small">
					<div class="frame">
						<div class="imgconstrain">
							<a href='<%# GetBroadcastChannelUrl (DataBinder.Eval (Container.DataItem, "NameNoSpaces").ToString ())%>'>
								<img src='<%# GetBroadcastChannelImageURL (DataBinder.Eval(Container.DataItem, "ThumbnailSmallPath").ToString (), "sm") %>' border="0" id="Img1"/>
							</a>
						</div>	
					</div>
				</div>
				<div class="info">		    
					<div class="name"><a href='<%# GetBroadcastChannelUrl (DataBinder.Eval (Container.DataItem, "NameNoSpaces").ToString ())%>'><%# DataBinder.Eval(Container.DataItem, "Name").ToString() %></a></div>
					<div class="title"><%# GetAchievementCounts (Convert.ToInt32(DataBinder.Eval (Container.DataItem, "CommunityId"))) %></div>
					<div class="level"><%# GetLevel (Convert.ToInt32(DataBinder.Eval (Container.DataItem, "CommunityId"))) %></div>
				</div>
				<div class="gameBadges">		 
					<div class="thumbs"><%# GetMostRecentAchievementThumbnails (Convert.ToInt32(DataBinder.Eval (Container.DataItem, "CommunityId")), 1) %></div>
				</div>
			</div>
		</itemtemplate>						
		<footertemplate><div class="famespacer"></div></footertemplate>
	</asp:repeater>

	<div class="noData" id="divNoData" runat="server" visible="false"></div>
	</div>

</div>
