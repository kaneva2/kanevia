///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Kaneva.mykaneva
{
	/// <summary>
	/// Summary description for manageCredits.
	/// </summary>
	public class manageCredits : MainTemplatePage
	{
		protected manageCredits () 
		{
			Title = "Manage My Credits";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			if (!KanevaGlobals.EnableCheckout)
			{
				if (!IsAdministrator())
				{
                    Response.Redirect("~/error.aspx?error=" + (int)Constants.eERROR_TYPE.CHECKOUT_CLOSED);
                }
			}

			// They must be logged in
			if (!Request.IsAuthenticated)
			{
				Response.Redirect (GetLoginURL ());
			}

            spnCreditCount.InnerText = Convert.ToUInt64(UsersUtility.GetUserPointTotal (GetUserId ()).ToString()).ToString("N0");

			// Set Nav
			HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
			HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.ACCOUNT;
			HeaderNav.MyAccountNav.ActiveTab = NavAccount.TAB.CREDITS;
			HeaderNav.SetNavVisible(HeaderNav.MyKanevaNav,2);
			HeaderNav.SetNavVisible(HeaderNav.MyAccountNav);

            if (UsersUtility.GetUserBillingSubscriptionId(KanevaWebGlobals.CurrentUser.UserId).Length > 0 && KanevaGlobals.EnableCheckout_B)
            {
                aBillingAddress.HRef = ResolveUrl ("~/mykaneva/billingInfoSummary.aspx");
            }
		}

		#region Helper Methods

		#endregion

		#region Declerations

		protected HtmlContainerControl	spnCreditCount;
        protected HtmlAnchor aAddCredits, aPurchase, aBillingAddress;
		protected HtmlImage				imgAddCredits, imgAddCreditsNow;

		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
