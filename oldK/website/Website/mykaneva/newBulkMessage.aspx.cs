///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using System.Collections;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva.mykaneva
{
	/// <summary>
	/// Summary description for newBulkMessage.
	/// </summary>
	public class newBulkMessage : MainTemplatePage
	{
		protected newBulkMessage () 
		{
			Title = "Compose New Bulk Kaneva Message";
		}

		private void Page_Load (object sender, System.EventArgs e)
		{
			// Must be logged in
			if (!Request.IsAuthenticated)
			{
				Response.Redirect (GetLoginURL ());
				return;
			}

			// Only admins can send bulk Private Messages
			if (!IsAdministrator ())
			{
				Response.Redirect (ResolveUrl ("~/mykaneva/newMessage.aspx"));
				return;
			}

			// check to see if we are posting back trying to upload files
			try
			{
				if ( Request.Files[0] != null )
				{
					UploadFile();
				}
			}
			catch{}

			if (!IsPostBack)
			{
				btnUpload.Attributes.Add("onclick", "document.forms[0].submit();");
			}

			if (!divSuccess.Visible)
			{											
				trInsertMediaLink.Visible = false;
				if (Request.IsAuthenticated)
				{
					trInsertMediaLink.Visible = true;
				}
			}

			// Set Nav
			if (Request.IsAuthenticated)
			{
                HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
				HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.MESSAGES;
				HeaderNav.SetNavVisible(HeaderNav.MyKanevaNav,2);
				HeaderNav.SetNavVisible(HeaderNav.MyMessagesNav);
				HeaderNav.MyMessagesNav.ActiveTab = NavMessages.TAB.NONE;
			}

			txtBody.EditorBodyStyle = "font-size:10px;font-family:Verdana;";
			litEditorId.Text = txtBody.ClientID;
			litSiteName.Text = KanevaGlobals.SiteName;
		}

		
		#region Helper Methods
		
		/// <summary>
		/// AddFile
		/// </summary>
		/// <param name="File"></param>
		/// <param name="itemName"></param>
		private void UploadFile ()
		{
			try
			{
				HttpPostedFile File = Request.Files[0];

				//	int maximumBufferSize = 4096;
				byte [] transferBuffer;

				transferBuffer = new byte [File.ContentLength];
			
				int bytesRead;
				bytesRead = File.InputStream.Read (transferBuffer, 0, transferBuffer.Length);
				//	System.Text.StringBuilder sb = new System.Text.StringBuilder(File.InputStream.ToString()); 	
			

				System.Text.ASCIIEncoding enc = new System.Text.ASCIIEncoding();
				string str = enc.GetString(transferBuffer);

			
				// Define a regular expression for repeated words.
				string rxEmail = "\\b\\w+\\b";
			
				Regex rx = new Regex(rxEmail, RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace);
        
				// Find matches.
				MatchCollection matches = rx.Matches(str);

				string userNames = string.Empty;

				// add each email to output string
				foreach (Match match in matches)
				{
					if (userNames.Length > 0)
					{
						userNames += ",";
					}

					userNames += match.Value;   
				}

				txtUsername.Text = userNames;
			}
			catch {}

			divUploadCSV.Visible = false;
			divImportCSV.Visible = true;
		}

		protected void btnImportCSV_Click (object sender, ImageClickEventArgs e)
		{
			btnUpload.Attributes.Add("onclick", "document.forms[0].submit();");

			divImportCSV.Visible = false;
			divUploadCSV.Visible = true;
		}

		/// <summary>
		/// SendMessage
		/// </summary>
		/// <param name="toId"></param>
		private int SendMessage (int toId, string strSubject)
		{
            // Insert a private message
            UserFacade userFacade = new UserFacade();
            Message message = new Message(0, GetUserId(), toId, strSubject,
                txtBody.Text, new DateTime(), 0, (int)Constants.eMESSAGE_TYPE.PRIVATE_MESSAGE, 0, "U", "S");

            return userFacade.InsertMessage(message);
		}
		
		/// <summary>
		/// Show success notice for type of message sent.
		/// </summary>
		/// <returns></returns>
		private void ShowSuccess ()
		{
			string msg = string.Empty;
			string link = string.Empty;
			string link_desc = string.Empty;
			string type = "Message";

//			DataRow drUser = UsersUtility.GetUser(userId);
//			lnkMember.HRef = GetPersonalChannelUrl (drUser["name_no_spaces"].ToString());
//			imgAvatar.Src = ResolveUrl(GetProfileImageURL(drUser["thumbnail_small_path"].ToString()));
//			divAvatar.Visible = true;
			msg = "Messages sent to CSV List";
			link = ResolveUrl("~/mykaneva/mailbox.aspx");
			link_desc = "Message Inbox";


			h1Title.InnerText = "Success";
			lblGoDesc.Text = link_desc;
			lnkSuccessGo.HRef = link;
			tdSuccessMsg.InnerHtml = msg + ".";
			spnAlertMsgSuccess.InnerText = type + " successfully sent.";

			tblComposeMessage.Visible = false;
			divSuccess.Visible = true;
		}

		private void ShowErrMsg(string msg)
		{
			spnAlertMsg.InnerText = msg;

			if (MagicAjax.MagicAjaxContext.Current.IsAjaxCallForPage(Page))
			{
				MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('spnAlertMsg', 10000);");
			}
			else
			{
				if (!ClientScript.IsClientScriptBlockRegistered(GetType (), "spanalertmsg"))
				{
                    ClientScript.RegisterStartupScript(GetType(), "spanalertmsg", "<script language=JavaScript>ShowConfirmMsg('spnAlertMsg', 10000);</script>");
				}
			}
		}
		#endregion

		#region Event Handlers
		/// <summary>
		/// Cancel Event Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnCancel_Click (object sender, ImageClickEventArgs e) 
		{
			Response.Redirect ("~/mykaneva/mailbox.aspx");
		}

		/// <summary>
		/// Update Event Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnSend_Click (object sender, ImageClickEventArgs e) 
		{
			int userId = 0;
			string strSubject = Server.HtmlEncode (txtSubject.Text.Trim ());
			spnAlertMsg.InnerText = string.Empty;

			// Default the subject
			if (strSubject.Length.Equals (0))
			{
				strSubject = NO_SUBJECT;
			}
		
			if (!Request.IsAuthenticated && Request["URL"] == null)
			{
				Response.Redirect (GetLoginURL ());
			}

			// Validate the email addresses
			//parse the email addresses using spaces, comman and semicolons
			string [] usernames = txtUsername.Text.Trim ().Split (';');
			ArrayList usernameList = new ArrayList() ;
			foreach (string s in usernames)
			{
				string [] list = s.Trim ().Split (' ');
				foreach (string s2 in list)
				{
					string [] list2 = s2.Trim ().Split (',');
					foreach (string s3 in list2)
					{
						if (s3.Trim() != string.Empty)
							usernameList.Add (s3);
					}
				}
			}


			ArrayList usernameListMember = new ArrayList();
			
			//foreach (string toEmail in emailList)
			for (int i = usernameList.Count - 1; i >= 0; i --)
			{
				string sUsername = (string) usernameList [i];

				userId = UsersUtility.GetUserIdFromUsername (sUsername.Trim ());

				if (userId == 0)
				{
					usernameListMember.Add (usernameList [i]);
					usernameList.RemoveAt (i);
					spnAlertMsg.InnerText = "Invalid username '" + sUsername + "'.";
				}
				else
				{
					
				}
			}

			if (usernameList.Count > 0)
			{
				// Send out the emails
				for (int i = usernameList.Count - 1; i >= 0; i --)
				{
					string toUsername = usernameList [i].ToString ();

					userId = UsersUtility.GetUserIdFromUsername (toUsername.Trim ());

					if (SendMessage (userId, strSubject).Equals (-99))
					{
						ShowErrMsg ("Message not sent. This member " + toUsername + " has blocked you from sending messages to them.");
					}
					
				}

				// Show the summary page
				ShowSuccess ();
			}
			else
			{
				ShowErrMsg ("No private messages sent because all usernames were invalid.");	
			}
		}

		protected void lbBack_Click (object sender, EventArgs e)
		{
			Response.Redirect ("~/mykaneva/mailbox.aspx");
		}

		#endregion
		
		#region Declerations
		protected CuteEditor.Editor txtBody;

		protected TextBox		txtSubject, txtUsername;
		protected Label			lblGoDesc;
		protected Literal		litEditorId, litSiteName;

		protected HtmlInputHidden		hidLinkURL, hidMessageId;
		protected HtmlTableCell			tdSuccessMsg;
		protected HtmlTable				tblComposeMessage;
		protected HtmlContainerControl	spnAlertMsg, spnAlertMsgSuccess, divSuccess;
		protected HtmlContainerControl	h1Title, divAvatar;
		protected HtmlAnchor			lnkSuccessGo, lnkBackToChannel, lnkMember;
		protected HtmlImage				imgAvatar;
		protected HtmlTableRow			trInsertMediaLink;
		protected ImageButton			btnUpload, btnImportCSV;
		protected HtmlContainerControl	divUploadCSV, divImportCSV;

		private const string NO_SUBJECT = "No subject";
		
		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
