<%@ Page language="c#" Codebehind="billing.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.billing" %>
<%@ Register TagPrefix="Kaneva" Namespace="KlausEnt.KEP.Kaneva.WebControls" Assembly="Kaneva.WebControls" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>

					
<link href="../css/new.css" rel="stylesheet" type="text/css">
<link href="../css/shadow.css" rel="stylesheet" type="text/css">

<ajax:ajaxpanel id="ajpAddresses" runat="server">

<table border="0" cellspacing="0" cellpadding="0" class="newcontainer" align="center">
	<tr>
		<td>
			<table  border="0" cellpadding="0" cellspacing="0" width="100%"class="newcontainerborder">
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
				<tr>
					<td class=""><img runat="server" src="~/images/spacer.gif" width="1" height="1"/></td>
					<td valign="top" class="newdatacontainer">
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
								
									<!-- TOP STATUS BAR -->
									<a name="top"></a>
									<div id="pageheader">
										<table cellpadding="0" cellspacing="0" border="0" width="99%">
											<tr>
												<td align="left">
													<h1>Update Billing Address</h1>
												</td>
											</tr>
										</table>
									</div>
									<!-- END TOP STATUS BAR -->		
								
								</td>
							</tr>
							<tr>
								<td width="968" align="left"  valign="top">
								
									<table cellpadding="0" cellspacing="0" border="0" width="100%">
										<tr>
											<td width="970" valign="top">
											
												<br/>
												<table border="0" cellspacing="0" cellpadding="0" width="816">
													<tr>
														<td align="right"><a href="~/mykaneva/billingInfoSummary.aspx" runat="server" id="a4" class="nohover">Back to Billing Settings</a></td>
													</tr>
												</table>
												<br/>
		
												<table id="tblAddresses" visible="false" runat="server" border="0" cellspacing="0" cellpadding="0" width="100%">
													<tr>
														<td valign="top" align="center">
															
															<table border="0" cellspacing="0" cellpadding="0">
																<tr>
																	<td width="670">
																		
																		<div class="module whitebg fullpage" style="margin-bottom:14px;">
																		<span class="ct"><span class="cl"></span></span>
																		<h1>Select Billing Address</h1>
																				
																		<asp:datalist runat="server" enableviewstate="True" showfooter="False" width="97%" id="dlAddress"
																			cellpadding="6" cellspacing="0" border="0" repeatcolumns="2" repeatdirection="Horizontal" 
																			horizontalalign="Center" cssclass="wizform nopadding">
																			
																			<itemtemplate>			   
																				<table cellspacing="0" cellpadding="1" align="left" border="0" class="unselected" width="310">
																					<tr>
																						<td rowspan="8" class="radiobg"><img runat="server" src="~/images/spacer.gif" width="5" height="1"/></td>
																						<td rowspan="8" width="5"><input type="hidden" runat="server" id="hidAddressId" value='<%# DataBinder.Eval(Container.DataItem, "address_id")%>' name="hidAddressId"></td>
																					</tr>
																					<tr><td><%# DataBinder.Eval (Container.DataItem, "name")%></td></tr>
																					<tr><td><%# DataBinder.Eval (Container.DataItem, "address1")%></td></tr>
																					<tr><td><%# DataBinder.Eval (Container.DataItem, "address2")%></td></tr>
																					<tr>
																						<td align="left">
																							<table border="0" cellspacing="0" cellpadding="0" align="left" style="margin:0 0;">
																								<tr>
																									<td style="padding-right:20px;"><%# DataBinder.Eval (Container.DataItem, "city")%></td>
																									<td style="padding-right:20px;"><%# DataBinder.Eval (Container.DataItem, "state_name")%></td>
																									<td><%# DataBinder.Eval (Container.DataItem, "zip_code")%></td>
																								</tr>
																							</table>
																						</td>
																					</tr>
																					<tr><td><%# DataBinder.Eval (Container.DataItem, "country_name")%></td></tr>
																					<tr><td><%# DataBinder.Eval (Container.DataItem, "phone_number")%></td></tr>
																					<tr><td>
																						<asp:linkbutton id="lbEdit" runat="Server" causesvalidation="False" commandname="cmdEdit">edit</asp:linkbutton> 
																						&nbsp;<asp:linkbutton id="lbDelete" runat="Server" causesvalidation="False" commandname="cmdDelete">delete</asp:linkbutton></td></tr>
																				</table>
																			</itemtemplate>
																			
																		</asp:datalist>
																	 
																		<span class="cb"><span class="cl"></span></span>
																		</div>
																		 
																	</td>
																</tr>
															</table>
															
														</td>
													</tr>
												</table>
											
												<table border="0" cellspacing="0" cellpadding="0" width="100%">
													<tr>
														<!-- START WIZARD CONTENT -->
														<td valign="top" align="center">
															
															<table border="0" cellspacing="0" cellpadding="0">
																<tr>
																	<td width="670">
																		
																		<div class="module fullpage">
																		<span class="ct"><span class="cl"></span></span>
																		<h1>Add Billing Address</h1>
																		
																		<table border="0" cellspacing="0" cellpadding="0" width="460" align="center" class="wizform">
																			<tr>																												  
																				<td align="center">
																					<asp:validationsummary showmessagebox="False" showsummary="True" class="formError" id="valSum" displaymode="BulletList" runat="server" headertext="Please correct the following errors:"/>
																					<br/>
																					<table cellspacing="0" cellpadding="0" width="470" align="left" border="0">					  
																						<tr>
																							<td align="left">Full Name*<br>
																							<asp:textbox id="txtFullName" class="biginput" style="width:455" maxlength="100" runat="server"/>
																							<asp:requiredfieldvalidator id="rfUsername" controltovalidate="txtFullName" text="" errormessage="Full Name is a required field." display="None" runat="server"/>
																							</td>
																						</tr>
																						
																						<tr><td class="formspacer"></td></tr>
																						
																						<tr>
																							<td align="left">Address 1*<br>
																							<asp:textbox id="txtAddress1" class="biginput" style="width:455px" maxlength="100" runat="server"/>
																							<asp:requiredfieldvalidator id="rfAddress1" controltovalidate="txtAddress1" text="" errormessage="Address1 is a required field." display="None" runat="server"/>
																							</td>
																						</tr>
																						
																						<tr><td class="formspacer"></td></tr>
																						
																						<tr>
																							<td align="left">Address 2<br>
																							<asp:textbox id="txtAddress2" class="biginput" style="width:455px" maxlength="80" runat="server"/>
																							</td>
																						</tr>
																						
																						<tr><td class="formspacer"></td></tr>
																						
																						<tr>
																							<td align="left">
																								<table border="0" cellspacing="0" cellpadding="0" align="left" style="margin:0 0;">
																									<tr>
																										<td colspan="2">City*<br>
																										<asp:textbox id="txtCity" class="biginput" style="width:200px" maxlength="100" runat="server"/>
																										<asp:requiredfieldvalidator id="rftxtCity" controltovalidate="txtCity" text="" errormessage="City is a required field." display="None" runat="server"/>
																										</td>
																										<td width="20"></td>
																										<td colspan="2">State**<br>
																											<asp:dropdownlist runat="server" class="biginput" id="drpState" style="width:80px"></asp:dropdownlist>
																											<asp:requiredfieldvalidator id="rfdrpState" controltovalidate="drpState" text="" errormessage="State is a required field when country is United States or Canada." display="None" runat="server"/>
																										</td>
																										<td width="20"></td>
																										<td>Zip Code**<br>
																											<asp:textbox id="txtPostalCode" class="biginput" style="width:100px" size="9" maxlength="25" runat="server"/>
																											<asp:requiredfieldvalidator id="rftxtPostalCode" controltovalidate="txtPostalCode" text="" errormessage="Postal code is a required field." display="None" runat="server"/><img src="images/spacer.gif" height="1" width="15" />
																										</td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																						<tr><td class="formspacer"></td></tr>
																						
																						<tr>
																							<td align="left">Country*<br>
																								
																								<asp:dropdownlist runat="server" class="biginput" id="drpCountry" style="width: 300px;"></asp:dropdownlist>
																								<asp:requiredfieldvalidator id="rfdrpCountry" controltovalidate="drpCountry" text="" errormessage="Country is a required field." display="None" runat="server"/>

																							</td>
																						</tr>
																						<tr><td class="formspacer"></td></tr>
																						<tr>
																							<td align="left">Phone Number*<br>
																							<asp:textbox id="txtPhoneNumber" class="biginput" style="width:200px" maxlength="15" runat="server"/>
																							<asp:requiredfieldvalidator id="rftxtPhoneNumber" controltovalidate="txtPhoneNumber" text="" errormessage="Phone Number is a required field." display="None" runat="server"/>
																							</td>
																						</tr>
																						<tr><td class="formspacer"></td></tr>
																						<tr>
																							<td class="note" align="left">* Indicates Required Field.<br>** Indicates required fields for United States and Canada only.</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																		</table>
																		
																		<br><br>
																		
																		<table border="0" cellspacing="0" cellpadding="5" width="97%">
																			<tr>
																				<td align="right"><input type="hidden" runat="server" id="hidUpdateId" name="hidUpdateId" runat="server">					   
																					<asp:button id="btnAddAddress" runat="Server" causesvalidation="False" onclick="btnAddAddress_Click" class="Filter2" text="  Add New Address  "/>
																					<asp:button id="btnUpdateAddress" runat="Server" visible="false" causesvalidation="False" onclick="btnUpdateAddress_Click" class="Filter2" text="  Update Address  "/>
																				</td>								  
																			</tr>
																		</table>
																		<span class="cb"><span class="cl"></span></span>
																		</div>
																	
																	</td>
																</tr>
															</table>
															
														</td>
														
														<!-- END WIZARD CONTENT -->
													</tr>
												</table>
											
											</td>
											
										</tr>
										
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td class=""><img runat="server" src="~/images/spacer.gif" id="Img5" width="1" height="1" /></td>
				</tr>
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
				
			</table>
		</td>
	</tr>
</table>
</ajax:ajaxpanel>

<script language="javascript">
function scroll()
{
	document.location.href = '#top';
}
</script>

