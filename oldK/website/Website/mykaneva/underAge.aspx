<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="underAge.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.underAge" %>

<link href="../css/new.css" type="text/css" rel="stylesheet">


<table border="0" cellspacing="0" cellpadding="0" class="newcontainer" align="center">
	<tr>
		<td>
			<table  border="0" cellpadding="0" cellspacing="0" width="100%" class="newcontainerborder">
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
				<tr>
					<td class=""><img id="Img1" runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
					<td valign="top" class="newdatacontainer" align="center">
									
						<table cellpadding="0" cellspacing="0" border="0" width="70%" style="margin:40px 0;">
							<tr>
								<td valign="top" align="center">
									<div class="module whitebg">
										<span class="ct"><span class="cl"></span></span>
											<h2>This feature is for members over 18 years of age</h2>
											<table cellpadding="6" cellspacing="0" border="0" width="95%">
												<tr>
													<td align="left" valign="top" style="line-height:14px;">
													    TIP: When users in the Virtual World right-click on your Avatar, they automatically see
                                                        elements of your Profile -- including your profile picture, your interests, and your friends.
                                                        <br /><br />
                                                        Virtual World users can also click a link to view your full Profile on the Kaneva website. 
                                                        This is one of the neat 2D-to-3D features that makes Kaneva so unique.
														<br /><br /><br /><br />
														<asp:button id="btnOK" runat="server" text="Visit My Kaneva" onclick="btnOK_Click"></asp:button>
													</td>
												</tr>
											</table>
										<span class="cb"><span class="cl"></span></span>
									</div>
								</td>
							</tr>
						</table>
						
					</td>
					<td class=""><img id="Img2" runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
				</tr>
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
			</table>
			
		</td>
		
	</tr>
</table>