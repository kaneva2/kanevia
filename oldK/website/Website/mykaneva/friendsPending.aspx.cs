///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva.channel;
using log4net;

namespace KlausEnt.KEP.Kaneva.mykaneva
{
	/// <summary>
	/// Summary description for friendsPending.
	/// </summary>
	public class friendsPending : SortedBasePage
	{
		protected friendsPending () 
		{
			Title = "Pending Friend Requests";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			if(Request.IsAuthenticated)
			{
                // Redirect to the new Media Center page
                Response.Redirect (ResolveUrl("~/mykaneva/messagecenter.aspx?mc=frs"));

                
                if(GetRequestParams())
				{
					//check if user is allowed to edit this channel
					if(!IsUserAllowedToEdit())
					{
						RedirectToHomePage ();
					}
				}
				else
				{
					//invalid request params
					//if the user logged in, send him to his home page
					//if not, redirect to login page
					RedirectToHomePage ();
				}
			}
			else
			{
				Response.Redirect(this.GetLoginURL());
			}

			if (!IsPostBack)
			{
				CurrentSort = "request_date";
				lbSortByDate.CssClass = "selected";

				BindData (1, _userId);
			}

			// Set Nav
			HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
			HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.FRIENDS;
			HeaderNav.MyFriendNav.ActiveTab = NavFriend.TAB.REQUESTS_SENT;
			HeaderNav.SetNavVisible(HeaderNav.MyKanevaNav,2);
			HeaderNav.SetNavVisible(HeaderNav.MyFriendNav);
		}

		/// <summary>
		/// Bind the data
		/// </summary>
		/// <param name="pageNumber"></param>
		private void BindData (int pageNumber, int userId)
		{
			BindData (pageNumber, userId, "");	
		}
		/// <summary>
		/// Bind the data
		/// </summary>
		/// <param name="pageNumber"></param>
		private void BindData (int pageNumber, int userId, string filter)
		{
			searchFilter.CurrentPage = "friendrequestsent";

			// Set the sortable columns
			string orderby = CurrentSort + " " + CurrentSortOrder;
			int pgSize = searchFilter.NumberOfPages;

			PagedDataTable pds = UsersUtility.GetOutgoingPendingFriends(userId, filter, orderby, true, 
				pageNumber, pgSize);

			rptFriends.DataSource = pds;
			rptFriends.DataBind ();

			pgTop.NumberOfPages = Math.Ceiling ((double) pds.TotalCount / pgSize).ToString ();
			pgTop.DrawControl ();

			// The results
			lblSearch.Text = GetResultsText (pds.TotalCount, pageNumber, pgSize, pds.Rows.Count);
		}

		
		#region Helper Methods
		private bool GetRequestParams()
		{
			bool retVal = true;
			try
			{
				if (IsAdministrator ())
				{
					if (Request ["userId"] != null)
					{
						_userId = Convert.ToInt32 (Request ["userId"]);
					}
					_userId = (_userId > 0) ? _userId : this.GetUserId ();
				}
				else
				{
					_userId = this.GetUserId ();
				}
			}
			catch(Exception)
			{
				//invalid numbers
				retVal = false;
			}
			return retVal;
		}

		/// <summary>
		/// returns true if current user is allowed to edit this channel
		/// </summary>
		/// <returns></returns>
		private bool IsUserAllowedToEdit()
		{
			return IsAdministrator() || _userId == this.GetUserId();
		}

		public string FormatDateOnly(object date)
		{
			DateTime dt = (DateTime) date;

			return dt.ToString("MMMM") + " " + dt.Day.ToString() + ", " + dt.Year.ToString(); // FormatDate(dt);
		}
		public string CanResendInvite(object date)
		{
			DateTime dt = (DateTime) date;
	
			if (dt.AddDays(14) > DateTime.Now)
			{
				return "false";
			}
			else
			{
				return "true";
			}
		}
		#endregion

		#region Event Handlers
		/// <summary>
		/// Click the resend button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnResend_Click (Object sender, ImageClickEventArgs e) 
		{
			HtmlInputHidden hidUserId, hidDateSent;
			CheckBox chkEdit;
			int ret = 99;
			int count = 0;
			int countNotSent = 0;
			bool isItemChecked = false;

			spnAlertMsg.InnerText = string.Empty;

			// Remove the friends
			foreach (RepeaterItem rptFriend in rptFriends.Items)
			{
				chkEdit = (CheckBox) rptFriend.FindControl ("chkEdit");

				if (chkEdit.Checked)
				{
					isItemChecked = true;

					hidDateSent = (HtmlInputHidden) rptFriend.FindControl ("hidDateSent");
					DateTime dtSent = Convert.ToDateTime(hidDateSent.Value.ToString());

					if (dtSent.AddDays(14) > DateTime.Now)
					{
						countNotSent++;
					}
					else
					{
						hidUserId = (HtmlInputHidden) rptFriend.FindControl ("hidUserId");
						// ret = UsersUtility.SendFriendRequestEmail (_userId, Convert.ToInt32 (hidUserId.Value));
                        ret = MailUtilityWeb.SendFriendRequestEmail(_userId, Convert.ToInt32(hidUserId.Value));

						if (ret == 1)
							count++;
					}
				}
			}

			if (isItemChecked)
			{
				if (count > 0)
				{
					BindData (pgTop.CurrentPageNumber, _userId);
				}
				
				if (ret < 1)
				{
					spnAlertMsg.InnerText = "An error was encountered while trying to resend friend request.";
				}
				else
				{
					if (count > 0)
					{
						spnAlertMsg.InnerText = "Successfully resent " + count + " friend request" + (count != 1 ? "s" : "") + ".";
					}
					if (countNotSent > 0)
					{
						spnAlertMsg.InnerText += "  " + countNotSent.ToString() + (countNotSent != 1 ? " were" : " was") + " outside of appropriate time frame.";
						MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('spnAlertMsg', 10000);");
						return;
					}
				}
			}
			else
			{
				spnAlertMsg.InnerText = "No friend requests were selected.";
			}

			MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('spnAlertMsg', 5000);");
		}
		/// <summary>
		/// Click the cancel button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnCancel_Click (Object sender, ImageClickEventArgs e) 
		{
			HtmlInputHidden hidUserId;
			CheckBox chkEdit;
			int ret = 0;
			int count = 0;
			bool isItemChecked = false;

			foreach (RepeaterItem dliMessage in rptFriends.Items)
			{
				chkEdit = (CheckBox) dliMessage.FindControl ("chkEdit");

				if (chkEdit.Checked)
				{
					isItemChecked = true;
					
					hidUserId = (HtmlInputHidden) dliMessage.FindControl ("hidUserId");
					int friendId = Convert.ToInt32(hidUserId.Value);
                    ret = GetUserFacade.DeleteFriendRequest(_userId, friendId);
				
					if (ret == 1)
						count++;
				}
			}

			if (isItemChecked)
			{
				if (count > 0)
				{
					BindData (pgTop.CurrentPageNumber, _userId);
				}
				
				if (ret != 1)
				{
					spnAlertMsg.InnerText = "An error was encountered while trying to remove Friend Request.";
				}
				else
				{
					spnAlertMsg.InnerText = "Successfully removed " + count + " friend request" + (count != 1 ? "s" : "") + ".";
				}
			}
			else
			{
				spnAlertMsg.InnerText = "No friend requests were selected.";
			}

			MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('spnAlertMsg', 5000);");
		}

		private void rptFriends_ItemCommand(object source, RepeaterCommandEventArgs e)
		{
			int ret = 0;
			string command = e.CommandName;
			
			if (command.Equals("cmdResend"))
			{
				HtmlInputHidden hidUserId = (HtmlInputHidden) e.Item.FindControl("hidUserId");
				int friendId = Convert.ToInt32(hidUserId.Value);
				//ret = UsersUtility.SendFriendRequestEmail (_userId, friendId);
                ret = MailUtilityWeb.SendFriendRequestEmail(_userId, friendId);

				if (ret == 1)
				{
					// Success
					BindData (pgTop.CurrentPageNumber, _userId);

					spnAlertMsg.InnerText = "Successfully resent friend request to 1 friend.";
				}
				else
				{
					// Error
					spnAlertMsg.InnerText = "An error was encountered while trying to resend friend request.";
				}

				MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('spnAlertMsg', 5000);");
			}
		}
		private void rptFriends_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
			{
				ImageButton btnResend = (ImageButton) e.Item.FindControl("btnResendMsg");
				CheckBox chkEdit = (CheckBox) e.Item.FindControl("chkEdit");

				DateTime dt = (DateTime)DataBinder.Eval(e.Item.DataItem, "request_date");
				
				if (dt.AddDays(14) > DateTime.Now)
				{
					btnResend.Visible = false;
				}
				else
				{
					btnResend.Visible = true;
				}
			}
		}

		/// <summary>
		/// Execute when the user selects an item from the Store Filter
		/// Items to display drop down list
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void FilterChanged(object sender, FilterChangedEventArgs e)
		{
			searchFilter.SetPagesDropValue(searchFilter.NumberOfPages.ToString());
			
			BindData (1, _userId, e.Filter);
		}
		/// <summary>
		/// Page Change
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pgTop_PageChange (object sender, PageChangeEventArgs e)
		{
			BindData (e.PageNumber, _userId);
		}

		/// <summary>
		/// Execute when the user clicks the the view type link button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void lbSortBy_Click (object sender, EventArgs e) 
		{
			lbSortByName.CssClass = lbSortByDate.CssClass = "";

			if (((LinkButton)sender).Attributes["sortby"].ToString().ToLower() == "name")
			{
				CurrentSort = "u.username";
				CurrentSortOrder = "ASC";
				lbSortByName.CssClass = "selected";
			}
			else
			{
				CurrentSort = "request_date";
				CurrentSortOrder = "DESC";
				lbSortByDate.CssClass = "selected";							  
			}

			BindData (pgTop.CurrentPageNumber, _userId);
		}
		#endregion

		#region Declerations
		protected Kaneva.Pager pgTop;
		protected Kaneva.SearchFilter searchFilter;

		protected Label			lblSearch;
		protected Repeater		rptFriends;
		protected LinkButton	lbSortByName, lbSortByDate;
		protected ImageButton	btnResend, btnCancel;

		protected HtmlContainerControl	spnAlertMsg;
		
		private int _userId;
		#endregion

		#region Properties
		/// <summary>
		/// DEFAULT_SORT
		/// </summary>
		/// <returns></returns>
		protected override string DEFAULT_SORT
		{
			get
			{
				return "username";
			}
		}

		/// <summary>
		/// DEFAULT_SORT ORDER
		/// </summary>
		/// <returns></returns>
		protected override string DEFAULT_SORT_ORDER
		{
			get
			{
				return "DESC";
			}
		}

		#endregion

		// Logger
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

		#region Web Form Designer generated code

		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
			pgTop.PageChanged +=new PageChangeEventHandler (pgTop_PageChange);
			searchFilter.FilterChanged +=new FilterChangedEventHandler (FilterChanged);		

			this.rptFriends.ItemCommand +=new RepeaterCommandEventHandler (rptFriends_ItemCommand);
			this.rptFriends.ItemDataBound += new RepeaterItemEventHandler (rptFriends_ItemDataBound);
		}
		#endregion
	}
}

