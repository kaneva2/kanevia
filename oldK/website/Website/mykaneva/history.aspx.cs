///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for history.
	/// </summary>
	public class history : SortedBasePage
	{
		protected history () 
		{
			Title = "My View History";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			// They must be logged in
			if (!Request.IsAuthenticated)
			{
				Response.Redirect (GetLoginURL ());
				return;
			}

			if (!IsPostBack)
			{
				int userId = GetUserId ();

				BindTransactionData (1, userId);
			}

			// Set nav
            HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
			HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.ACCOUNT;
			HeaderNav.MyAccountNav.ActiveTab = NavAccount.TAB.NONE;
			HeaderNav.SetNavVisible(HeaderNav.MyKanevaNav,2);
			HeaderNav.SetNavVisible(HeaderNav.MyAccountNav);
		}

		/// <summary>
		/// Bind the data
		/// </summary>
		/// <param name="pageNumber"></param>
		private void BindTransactionData (int pageNumber, int userId)
		{
			// Set the sortable columns
			string orderby = CurrentSort + " " + CurrentSortOrder;

            MediaFacade mediaFacade = new MediaFacade();
            PagedList<AssetView> plAssetView = mediaFacade.GetUserViewHistory (userId, orderby, pageNumber, KanevaGlobals.TransactionsPerPage);
            rptTransactions.DataSource = plAssetView;
			rptTransactions.DataBind ();

            pgTop.NumberOfPages = Math.Ceiling((double)plAssetView.TotalCount / KanevaGlobals.TransactionsPerPage).ToString();
			pgTop.DrawControl ();

			// The results
            lblSearch.Text = GetResultsText(plAssetView.TotalCount, pageNumber, KanevaGlobals.TransactionsPerPage, plAssetView.Count);
		}


		/// <summary>
		/// Page Change
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pgTop_PageChange (object sender, PageChangeEventArgs e)
		{
			BindTransactionData (e.PageNumber, GetUserId ());
		}



		/// <summary>
		/// They clicked to sort a column
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected override void btnSort_Click (object sender, System.EventArgs e)
		{
			base.btnSort_Click (sender, e);
			BindTransactionData (pgTop.CurrentPageNumber, GetUserId ());
		}

		/// <summary>
		/// DEFAULT_SORT
		/// </summary>
		/// <returns></returns>
		protected override string DEFAULT_SORT
		{
			get
			{
				return "view_id";
			}
		}

		/// <summary>
		/// DEFAULT_SORT_ORDER
		/// </summary>
		/// <returns></returns>
		protected override string DEFAULT_SORT_ORDER
		{
			get
			{
				return "DESC";
			}
		}

		protected Kaneva.Pager pgTop;
		protected Repeater rptTransactions;
		protected PlaceHolder phBreadCrumb;
		protected Label lblSearch;


		#region Web Form Designer generated code

		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
			pgTop.PageChanged +=new PageChangeEventHandler (pgTop_PageChange);			
		}
		#endregion
	}
}
