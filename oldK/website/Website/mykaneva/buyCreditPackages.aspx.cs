///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva.usercontrols;
using Kaneva.BusinessLayer.BusinessObjects;
using System.Collections.Generic;
using Kaneva.BusinessLayer.Facade;
using MagicAjax;

namespace KlausEnt.KEP.Kaneva
{
    public partial class buyCreditPackages : BasePage
    {
        #region Declarations

        private bool errorReload = false;
        List<Promotion> dlCredits = new List<Promotion>();
        private string promotionTypeFilter = "";
        private const int PAY_PAL = 1;
        private const int CREDIT_CARD = 0;
        private int most_popular_promotion_id = 0;
        private double vipDiscount = 0.0;

        private string _styleSheets = "\n<link href=\"../css/purchase_flow/purchase_package.css\" rel=\"stylesheet\" type=\"text/css\" />\n";


        protected CheckoutWizardNavBar ucWizardNavBar;

        #endregion

        protected buyCreditPackages() 
		{
			Title = "Purchase Credits";
		}

        protected void Page_Load(object sender, EventArgs e)
        {
            //set is administrator variable to prevent multiple function calls
            bool _isAdministrator = IsAdministrator();
            int _userID = KanevaWebGlobals.CurrentUser.UserId;


            //if check out not enabled and they are not an administrator redirect them
            if (!KanevaGlobals.EnableCheckout)
            {
                if (!_isAdministrator)
                {
                    Response.Redirect("~/error.aspx?error=" + (int)Constants.eERROR_TYPE.CHECKOUT_CLOSED);
                }
            }

            //verify the user is registered and signed in - if not do not allow them to purchase
            //redirects user
            VerifyUserAuthenticated();
            GetVIPDiscount();

            //process the page if it is not a post back or an error message reload
            if ((!IsPostBack) || errorReload)
            {
                //get the preview date from the request - if there is one process the page as a preview page
                DateTime previewDate = DateTime.Now;
                if (Request["previewDate"] != null && Request["previewDate"] != string.Empty)
                {
                    previewDate = Convert.ToDateTime(Request["previewDate"].ToString());
                    btnPurchasePayPal.Visible = false;
                    btnPurchaseCredit.Visible = false;
                }

                //set the purchase filter for first time purchase check and initial variable values
                promotionTypeFilter = ((int)Constants.ePROMOTION_TYPE.CREDITS).ToString();

                //get active promotions
                dlCredits = GetPromotionsFacade.GetActivePromotions(promotionTypeFilter, Constants.CURR_KPOINT, 0.0, previewDate);

                //find the best selling promotion and store it for marking
                most_popular_promotion_id = (new PromotionsFacade()).GetBestSellingPromotion(promotionTypeFilter);

                // add credit offerings to the list
                rptCredits.DataSource = dlCredits;
                rptCredits.DataBind();
            }

            // Set Nav and style sheets
            ((IndexPageTemplate) Master).HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
            ((IndexPageTemplate)Master).CustomCSS = _styleSheets;

            //get users balance
            int userBalance = 0;
            try
            {
                userBalance = (int)UsersUtility.getUserBalance(GetUserId(), "KPOINT");
            }
            catch{}
            lblUsersBalance.Text = userBalance.ToString ("#,###") +" Credits";

            //// Only show AP add to users 18 and older
            //if (KanevaWebGlobals.CurrentUser.IsAdult)
            //{
            //    aAP.Visible = true;
            //}
            //else
            //{
            //    aAP.Visible = false;
            //}
        }

        #region Helper Methods

        private void VerifyUserAuthenticated()
        {
            // They must be logged in
            if (!Request.IsAuthenticated)
            {
                Response.Redirect(GetLoginURL());
            }
        }

        private void GetVIPDiscount()
        {
            try
            {
                //get users user subscription id
                if (KanevaWebGlobals.CurrentUser.HasVIPPass)
                {
                    SubscriptionFacade subscriptionFacade = new SubscriptionFacade();
                    UserSubscription userSubscript = subscriptionFacade.GetUserSubscriptionByPassGroupId((uint)KanevaGlobals.VipPassGroupID, (uint)GetUserId());
                    this.vipDiscount = (userSubscript.DiscountPercent / 100.00);
                }
            }
            catch (Exception)
            {

            }
        }

        protected string CalculateDiscount(string price)
        {
            double discountPrice = 0.0;

            try
            {
                discountPrice = Math.Round(Convert.ToDouble(price) * (1.0 - vipDiscount),2);
            }
            catch (FormatException)
            {

            }
            return discountPrice.ToString();
        }

        protected string AppendSuperRewardsParameters()
        {
            string srParams = "";

            try
            {
                User user = (new UserFacade()).GetUser(GetUserId());
                srParams += Configuration.SuperRewardsURL + "&uid=" + user.UserId + "&gender=" + user.Gender + "&age=" + user.Age;
            }
            catch (Exception)
            {
            }

            return srParams;
        }

        protected string FormatCredits(UInt64 credits)
        {
            try
            {
                return credits.ToString("N0");
            }
            catch
            {
                return "";
            }
        }

        private void ResetCreditListing(Repeater repeater)
        {
            HtmlTableRow row;
            RadioButton rbutton;

            foreach (RepeaterItem rItem in repeater.Items)
            {
                //find controls on this repeater item that needs editing
                row = (HtmlTableRow)rItem.FindControl("drPromtion");
                rbutton = (RadioButton)rItem.FindControl("rdb_Credit");

                //se them as needed
                rbutton.Checked = false;
                row.Attributes.Remove("class");
                row.Attributes.Add("class", "off");
            }
        }

        private void ResetBestValues(Repeater repeater)
        {
            foreach (RepeaterItem rItem in repeater.Items)
            {
                ((HtmlGenericControl)rItem.FindControl("divSpecialsBurst")).Visible = false;
            }
        }
        
        private void SelectThePackage(RepeaterItem ritem)
        {
            //get the select radio button
            RadioButton rbutton = (RadioButton)ritem.FindControl("rdb_Credit");

            //gets the selected row
            HtmlTableRow selectedRow = ((HtmlTableRow)((rbutton.Parent).Parent));

            //finds the hidden label with the package id for that row
            SelectSpecialId = ((Label)ritem.FindControl("lblCreditPackageID")).Text;

            //checks the radio button
            rbutton.Checked = true;

            //sets the class for the selected row that highlights it           
            selectedRow.Attributes.Add("class", "on1");
        }

        #endregion

        #region Main Functions

        private void PurchaseCredits(int PaymentMethod)
        {
            //vaidate their authentication
            if (!Request.IsAuthenticated)
            {
                Response.Redirect(GetLoginURL());
                return;
            }

            int creditId = 0;

            //must have selected a package to continue
            if ((SelectSpecialId != null) && (SelectSpecialId != ""))
            {
                if (KanevaGlobals.IsNumeric(SelectSpecialId))
                {
                    creditId = Convert.ToInt32(SelectSpecialId);
                    int userId = GetUserId();

                    DataRow drOrder = null;
                    bool newOrder = true;


                    if (Request["orderId"] != null && Request["orderId"] != string.Empty)
                    {
                        try
                        {
                            drOrder = StoreUtility.GetOrder(Convert.ToInt32(Request["orderId"]), userId);
                        }
                        catch { }
                    }

                    int orderId = 0;
                    if (drOrder != null && drOrder["order_id"].ToString() != string.Empty &&
                        Convert.ToInt32(drOrder["transaction_status_id"]) == (int)Constants.eORDER_STATUS.CHECKOUT &&
                        Convert.ToInt32(drOrder["user_id"]) == GetUserId())
                    {
                        orderId = Convert.ToInt32(drOrder["order_id"]);
                        newOrder = false;
                    }
                    else
                    {
                        orderId = StoreUtility.CreateOrder(userId, Common.GetVisitorIPAddress(), (int)Constants.eORDER_STATUS.CHECKOUT);
                    }

                    // Set the purchase type
                    StoreUtility.SetPurchaseType(orderId, Constants.ePURCHASE_TYPE.KPOINT_ONLY);

                    DataRow drPointBucket = StoreUtility.GetPromotion(creditId);

                    // Point Bucket item name
                    string itemName = Server.HtmlEncode(drPointBucket["display"].ToString());

                    if (drPointBucket["promotion_description"] != null && drPointBucket["promotion_description"].ToString() != string.Empty)
                    {
                        itemName = Server.HtmlEncode(drPointBucket["promotion_description"].ToString());
                    }

                    int transactionId = 0;

                    if (newOrder)
                    {
                        // Record the transaction in the database, marked as checkout
                        transactionId = StoreUtility.PurchasePoints(userId, (int)Constants.eTRANSACTION_STATUS.CHECKOUT, itemName, 0,
                            (int)Constants.ePAYMENT_METHODS.KANEVA_CYBERSOURCE, Math.Round((1.0 - vipDiscount) * Convert.ToDouble(drPointBucket["dollar_amount"]),2),
                            Convert.ToDouble(drPointBucket["kei_point_amount"]), Convert.ToDouble(drPointBucket["free_points_awarded_amount"]),
                            0, Common.GetVisitorIPAddress());

                        // Record the point bucket purchased
                        StoreUtility.UpdatePointTransactionPointBucket(transactionId, Convert.ToInt32(drPointBucket["promotion_id"]));
                    }
                    else
                    {
                        transactionId = Convert.ToInt32(drOrder["point_transaction_id"]);

                        // updated existing order
                        StoreUtility.UpdatePointTransactionPointBucket(transactionId, Convert.ToInt32(drPointBucket["promotion_id"]), itemName, Math.Round((1.0 - vipDiscount) * Convert.ToDouble(drPointBucket["dollar_amount"]), 2), Convert.ToDouble(drPointBucket["kei_point_amount"]), Convert.ToDouble(drPointBucket["free_points_awarded_amount"]));
                    }

                    // Set the order id on the point purchase transaction
                    StoreUtility.UpdatePointTransaction(transactionId, orderId);
                    StoreUtility.UpdateOrderPointTranasactionId(orderId, transactionId);

                    //handle according to payment method
                    if (PaymentMethod == CREDIT_CARD)
                    {
                        // Send them to select a payment method
                        Response.Redirect(ResolveUrl("~/checkout/billinginfo.aspx?orderId=" + orderId.ToString())); ;
                    }
                    else if (PaymentMethod == PAY_PAL)
                    {
                        StoreUtility.UpdatePointTransaction(transactionId, Constants.ePAYMENT_METHODS.PAYPAL);
                        Response.Redirect("~/checkout/orderConfirm.aspx?orderId=" + orderId.ToString() + "&pass=" + (Request.QueryString["pass"] != null ? Request.QueryString["pass"].ToString() : ""), false);
                    }

                }
                else
                {
                    spnMessage.InnerText = "Unable to process package.";

                    if (MagicAjax.MagicAjaxContext.Current.IsAjaxCall)
                    {
                        MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + spnMessage.ClientID + "', 5000);");
                    }
                    else
                    {
                        Page.ClientScript.RegisterClientScriptBlock(GetType(), "ShowConfirmMsg('" + spnMessage.ClientID + "', 5000);", "UP");
                    }
                }
            }
            else
            {
                spnMessage.InnerText = "Please select a credit amount.";
                if (MagicAjax.MagicAjaxContext.Current.IsAjaxCall)
                {
                    MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('" + spnMessage.ClientID + "', 5000);");
                }
                else
                {
                    Page.ClientScript.RegisterClientScriptBlock (GetType() ,"ShowConfirmMsg('" + spnMessage.ClientID + "', 5000);", "SC");
                }
            }

        }

        #endregion

        #region Event Handlers

        private void rptCredits_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem != null)
            {

                //calculate savings
                decimal originalPrice = Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "OriginalPrice"));
                decimal actualPrice = Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "DollarAmount"));
                decimal discount = 0.0m;
				

                // incorporate VIP discount to the actual price so thaat the calculated discount is consistent
                actualPrice = Math.Round(actualPrice * (1.0m - Convert.ToDecimal(vipDiscount)), 2);

                //handle case where original price was forgotten
                originalPrice = (originalPrice <= 0.0m) ? (actualPrice) : originalPrice;

                if (originalPrice < actualPrice)
                {
                    discount = Math.Round((1 - (originalPrice / actualPrice)) * -100, 0);
                }
                else
                {
                    discount = Math.Round((1 - (actualPrice / originalPrice)) * 100, 0);
                }

                //set the value in the control
                Label lbl_savings = (Label)e.Item.FindControl("lblSavings");
                //lbl_savings.Text = discount + "%!";
				lbl_savings.Text = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "FreePointsAwardedAmount")).ToString();

                //indicate which package is the most popular
                int promotionId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "PromotionId"));
                if (promotionId == most_popular_promotion_id)
                {
                    //((HtmlGenericControl)e.Item.FindControl("divMostPopular")).Visible = true;
                    ((Label)e.Item.FindControl("lblMostPopular")).Visible = true;
                }

                //store the best discount price and set it as the special item
                if (BestDiscount < discount)
                {
                    BestDiscount = discount;

                    Repeater repeater = (Repeater)sender;

                    //reset all the styles and button
                    ResetCreditListing(repeater);

                    SelectThePackage(e.Item);

                    //reset all the best values
                    ResetBestValues(repeater);

                    //sets the image best by flag
                    ((HtmlGenericControl)e.Item.FindControl("divSpecialsBurst")).Visible = true;
                }
            }
        }

        protected void rdb_Credit_CheckedChanged(object sender, EventArgs e)
        {
            //gets the repeater row
            RepeaterItem ritem = (RepeaterItem)((Control)sender).NamingContainer;

            //reset all the styles and button
            ResetCreditListing((Repeater)ritem.Parent);

            SelectThePackage(ritem);
        }

        /// <summary>
        /// They want to purchase a point bucket with credit card via kaneva
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnPurchaseCredits_Click(object sender, EventArgs e)
        {
            //purchase with credit card
            PurchaseCredits(CREDIT_CARD);
        }
            /// <summary>
        /// They want to purchase a point bucket with credit card via kaneva
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnPurchasePayPal_Click(object sender, EventArgs e)
        {
            //purchase with pay pal
            PurchaseCredits(PAY_PAL);
        }

        #endregion

        #region Properties

        private int MoveInSpecialID
        {
            get
            {
                if (ViewState["_moveinID"] == null)
                {
                    ViewState["_moveinID"] = 0;
                }
                return (int)ViewState["_moveinID"];
            }
            set
            {
                ViewState["_moveinID"] = value;
            }
        }

        private string SelectSpecialId
        {
            get
            {
                if (ViewState["_SelectSpecialId"] == null)
                {
                    ViewState["_SelectSpecialId"] = "";
                }
                return ViewState["_SelectSpecialId"].ToString();
            }
            set
            {
                ViewState["_SelectSpecialId"] = value;
            }
        }

        private int DefaultPromotionID
        {
            get
            {
                if (ViewState["_defaultPromo"] == null)
                {
                    ViewState["_defaultPromo"] = 0;
                }
                return (int)ViewState["_defaultPromo"];
            }
            set
            {
                ViewState["_defaultPromo"] = value;
            }
        }
        private decimal BestDiscount
        {
            get
            {
                if (ViewState["_bestDiscount"] == null)
                {
                    ViewState["_bestDiscount"] = 0.0m;
                }
                return (decimal)ViewState["_bestDiscount"];
            }
            set
            {
                ViewState["_bestDiscount"] = value;
            }
        }

        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            this.rptCredits.ItemDataBound += new System.Web.UI.WebControls.RepeaterItemEventHandler(this.rptCredits_ItemDataBound);
        }
        #endregion
    }
}
