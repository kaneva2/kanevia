///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using log4net;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for messageDetails.
	/// </summary>
	public class giftMessageDetails : MainTemplatePage
	{
		protected giftMessageDetails  () 
		{
			Title = "Gift Details";
		}

		private void Page_Load (object sender, System.EventArgs e)
		{

			// They must be logged in
			if (!Request.IsAuthenticated)
			{
				Response.Redirect (GetLoginURL ());
				return;
			}

			if (!IsPostBack)
			{
				int messageId = Convert.ToInt32 (Request ["messageId"]);

				DataRow drMessage = UsersUtility.GetGiftMessage (GetUserId (), messageId, true);

				if (drMessage ["username"].Equals (DBNull.Value))
				{
					// May be trying to access another users message
					m_logger.Warn ("UserId '" + GetUserId () + "' tried to access message '" + messageId + "' and nothing returned.");
					tblNoAccess.Visible = true;
					pnlMessage.Visible = false;
					return;
				}
				
				//**TODO: Update for new nav
				// Not sure where this will fall under the new navigation.  Will need to update.
                HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
				HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.MESSAGES;
				//HeaderNav.MyMessagesNav.ActiveTab = NavMessages.TAB.?????;
				HeaderNav.SetNavVisible(HeaderNav.MyKanevaNav,2);
				HeaderNav.SetNavVisible(HeaderNav.MyMessagesNav);

				lnkFrom.Text = drMessage ["username"].ToString ();
				lnkFrom.Attributes.Add ("href", GetPersonalChannelUrl (drMessage ["name_no_spaces"].ToString ()));

				aFromAvatar.HRef = GetPersonalChannelUrl (drMessage ["name_no_spaces"].ToString ());
				imgFromAvatar.Src = GetProfileImageURL (drMessage ["thumbnail_small_path"].ToString ());

				lblDate.Text = FormatDateTime (drMessage ["message_date"]);
				lblSubject.Text = drMessage ["subject"].ToString ();
				lblGiftName.Text = drMessage["name"].ToString();
				imgGift.Src = GetGiftImageURL( drMessage["gift_id"].ToString() );
				lblBody.Text = drMessage ["message"].ToString ();
				btnAccept.Enabled = drMessage["gift_status"].ToString() == Constants.GIFT_STATUS_UNACCEPTED;

				//hide spam and block buttons for message sent from logged in users
				spanSpamBlock.Visible = !drMessage ["from_id"].ToString ().Equals(this.GetUserId().ToString());

				// Mark the message as read
				// Only do this if you are the message recipient and it is in the inbox
				if (GetUserId ().Equals (Convert.ToInt32 (drMessage ["to_id"])) && Convert.ToInt32 (drMessage ["to_viewable"]).Equals ("U"))
				{
					UsersUtility.ReadMessage (GetUserId (), messageId);
				}
			}

//			this.HeaderNav.SetVisible3rdLevelNav(HeaderNav.MessageNav);

		}	

		/// <summary>
		/// Delete Event Handler
		/// </summary>
		protected void btnDelete_Click (object sender, EventArgs e) 
		{
			int userId = GetUserId ();
			int messageId = Convert.ToInt32 (Request ["messageId"]);

			UsersUtility.RejectGift( userId, messageId, true );

			UsersUtility.DeleteMessage ( userId, messageId );

			Response.Redirect (ResolveUrl ("~/mykaneva/mailboxGifts.aspx"));
		}

		protected HyperLink		lnkFrom;
		protected Label			lblDate, lblSubject, lblBody;
		protected HtmlAnchor			aFromAvatar;
		protected HtmlImage				imgFromAvatar;
		protected HtmlGenericControl	spanSpamBlock;

		protected System.Web.UI.WebControls.Label lblGiftName;
		protected System.Web.UI.HtmlControls.HtmlImage imgGift;
		protected System.Web.UI.WebControls.ValidationSummary valSum;
		protected System.Web.UI.WebControls.Button btnCancel;
		protected System.Web.UI.WebControls.Button btnDelete;
		protected System.Web.UI.WebControls.Button Button1;
		protected System.Web.UI.HtmlControls.HtmlAnchor A1;
		protected System.Web.UI.WebControls.Button btnAccept;
		protected HtmlTable tblNoAccess;
		protected Panel	pnlMessage;

		// Logger
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnAccept.Click += new System.EventHandler(this.btnAccept_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		protected void btnAccept_Click(object sender, System.EventArgs e)
		{
			int userId = GetUserId ();
			int messageId = Convert.ToInt32 (Request ["messageId"]);

			UsersUtility.Accept2DGift( userId, messageId, true );

			UsersUtility.DeleteMessage ( userId, messageId );

			Response.Redirect (ResolveUrl ("~/mykaneva/mailboxGifts.aspx"));
		}
	}
}
