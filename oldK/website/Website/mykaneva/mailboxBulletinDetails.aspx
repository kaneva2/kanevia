<%@ Page language="c#" Codebehind="mailboxBulletinDetails.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.mykaneva.mailboxBulletinDetails" %>

<link href="../css/home.css" rel="stylesheet" type="text/css" />		
<link href="../css/kanevaSC.css" rel="stylesheet" type="text/css"/>
<link href="../css/friends.css" rel="stylesheet" type="text/css"/>
<link href="../css/kanevaBroadBand.css" type="text/css" rel="stylesheet">
<link href="../css/kanevaText.css" rel="stylesheet" type="text/css"/>
<link href="../css/new.css" rel="stylesheet" type="text/css"/>
		
		
<table border="0" cellspacing="0" cellpadding="0"  width="990" align="center">
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" id="Img1"/></td>
	</tr>
	<tr>
		<td>
			<table  border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="frTopLeft"></td>
					<td class="frTop"></td>
					<td class="frTopRight"></td>
				</tr>
				<tr>
					<td class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img2"/></td>
					<td valign="top" class="frBgIntMembers">
					
						<!-- No Access Section -->
						<div id="divNoAccess" runat="server" visible="false">
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
									<div id="pageheader">
										<table cellpadding="0" cellspacing="0" border="0" width="99%">
											<tr><td align="left"><h1>Message Details</h1></td></tr>
										</table>
									</div>
								</td>
							</tr>
							<tr>
								<td width="968" align="center"  valign="top">
									<table cellpadding="0" cellspacing="0" border="0"  width="568">
										<tr>
											<td width="568" valign="top" align="center">
												<div class="module whitebg">
													<span class="ct"><span class="cl"></span></span>
													<table width="99%" border="0" cellpadding="5" cellspacing="0">
														<tr>
															<td width="99%" align="center">
																We are sorry, this message is currently unavailable on the Kaneva site. The message is either
																<br><br> 
																1. Deleted from the system<br>
																2. You are not the message recipient<br>
																3. The URL to the item is not correct
																<br><br><br>
																<span class="dateStamp">NOTE: If you believe you are receiving this page in error, please contact support via the <a runat="server" href="~/suggestions.aspx?mode=F&category=Feedback" class="showingCount" id="A2">Support Page</a></span>
																<br><br><br>
															</td>
														</tr>
													</table>
													<span class="cb"><span class="cl"></span></span>
												</div>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						</div>
						<!-- End No Access -->
					
					
						<!-- Message Details -->
						<div id="divMsgDetails" runat="server">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
								
									<!-- TOP STATUS BAR -->
									<div id="pageheader">
										<table cellpadding="0" cellspacing="0" border="0" width="99%">
											<tr>
												<td align="left"><h1>Message Detail</h1></td>
											</tr>
										</table>
									</div>
									<!-- END TOP STATUS BAR -->	
								
								</td>
							</tr>
							<tr>
								<td width="968" align="left"  valign="top">
									<table cellpadding="0" cellspacing="0" border="0"  width="968">
										<tr>
											<td width="200" valign="top">
												<div class="module toolbar">
													<span class="ct"><span class="cl"></span></span>
													<h2>Tools & Actions <span class="hlink"></span></h2>
													<table cellspacing="0" cellpadding="10" border="0" width="99%">
														<tr>
															<td align="center">
																<table cellspacing="0" cellpadding="3" border="0" width="100%">
																	<tr>
																		<td>
																			<asp:imagebutton id="btnBackTop" runat="Server" onclick="btnBack_Click" imageurl="~/images/button_cancel.gif" alternatetext="Back" width="57" height="23" border="0" vspace="2" causesvalidation="False" /><br>
																		</td>
																	</tr>
																</table>	 
															</td>
														</tr>				  
													</table>
													<span class="cb"><span class="cl"></span></span>
												</div>
											</td>
											<td width="20"><img runat="server" src="~/images/spacer.gif" width="20" height="1" id="Img3"/></td>
											<td width="748" valign="top" align="center">
												<div class="module whitebg">
													<span class="ct"><span class="cl"></span></span>
													
													<h2 class="alertmessage"><span id="spnAlertMsg" runat="server"></span></h2>
													
													<table width="99%" border="0" cellpadding="5" cellspacing="0">
														<tr>
															<td width="99%">
																<table border="0" cellspacing="0" cellpadding="0" width="100%">
																	<tr>
																		<td align="center" width="113" valign="top">
														
																			<!-- Avatar Box -->
																				<div class="framesize-small">
																					<div class="frame">
																						<span class="ct"><span class="cl"></span></span>
																							<div class="imgconstrain">
																								<a runat="server" id="aFromAvatar"><img runat="server" id="imgFromAvatar" border="0"/></a>
																							</div>
																						<span class="cb"><span class="cl"></span></span>
																					</div>
																				</div>		
																			<!-- End Avatar Box -->
																		</td>
																		<td>
																		
																			<table width="100%" border="0" cellspacing="0" cellpadding="3" class="textGeneralGray01">
																				<tr>
																					<td width="65"><strong>From:</strong></td>
																					<td width="879"><asp:hyperlink runat="server" id="lnkFrom"/></td>
																				</tr>
																				<tr>
																					<td><strong>Date:</strong></td>
																					<td><asp:label runat="server" id="lblDate"/></td>
																				</tr>
																				<tr>
																					<td><strong>Subject:</strong></td>
																					<td><asp:label runat="server" id="lblSubject"/></td>
																				</tr>
																				<tr>
																					<td colspan="2"><hr></td>
																				</tr>
																				<tr>
																					<td colspan="2">
																						<asp:label runat="server" id="lblBody"/>
																					</td>
																				</tr>
																			</table>
																		</td>
																	</tr>
																	<tr>
																		<td colspan="2"><hr><br></td>
																	</tr>
																	
																	<tr>
																		<td align="right" colspan="2"><asp:imagebutton id="btnBackBottom" runat="Server" onclick="btnBack_Click" imageurl="~/images/button_cancel.gif" alternatetext="Delete" width="57" height="23" border="0" vspace="2" causesvalidation="False" /></td>
																	</tr>
																
																</table>
															</td>
														</tr>
													</table>
																
													<span class="cb"><span class="cl"></span></span>
												</div>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						</div>
						<!-- End Message Details -->
						
						
					</td>
					<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img5"/></td>
				</tr>
				<tr>
					<td class="frBottomLeft"></td>
					<td class="frBottom"></td>
					<td class="frBottomRight"></td>
				</tr>
				<tr>
					<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" id="Img6"/></td>
				</tr>
			</table>
		</td>
	</tr>
</table>

                       