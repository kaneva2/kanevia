///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for incomeItemsSold.
	/// </summary>
	public class incomeItemsSold : StoreBasePage
	{
		protected incomeItemsSold () 
		{
			Title = "Monthly Sales Details";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			// They must be logged in
			if (!Request.IsAuthenticated)
			{
				Response.Redirect (GetLoginURL ());
				return;
			}

			if (!IsPostBack)
			{
				
				lblMonth.Text = "Views for " + GetMonth ().ToString ("MMMM yyyy");
				BindData (1);
			}

			// Set Nav
            HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB._UPDATE_;
			HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.ACCOUNT;
			HeaderNav.MyAccountNav.ActiveTab = NavAccount.TAB.STATS;
			HeaderNav.SetNavVisible(HeaderNav.MyKanevaNav,2);
			HeaderNav.SetNavVisible(HeaderNav.MyAccountNav);
		}

		/// <summary>
		/// Bind the data
		/// </summary>
		/// <param name="pageNumber"></param>
		private void BindData (int pageNumber)
		{
			DateTime dtMonth = GetMonth ();

			// Set the sortable columns
			//SetHeaderSortText (dgrdItemsSold);
			string orderby = CurrentSort + " " + CurrentSortOrder;

			PagedDataTable pds = StoreUtility.GetSoldItemsForMonth (GetUserId (), dtMonth, orderby, pageNumber, ITEMS_PER_PAGE);
			rptTransactions.DataSource = pds;
			rptTransactions.DataBind ();

			pgTop.NumberOfPages = Math.Ceiling ((double) pds.TotalCount / ITEMS_PER_PAGE).ToString ();
			pgTop.DrawControl ();

			// The results
			lblSearch.Text = GetResultsText (pds.TotalCount, pageNumber, ITEMS_PER_PAGE, pds.Rows.Count);
		}

		/// <summary>
		/// GetMonth
		/// </summary>
		/// <returns></returns>
		private DateTime GetMonth ()
		{
			if (Request ["year"] == null || Request ["month"] == null)
			{
				return KanevaGlobals.GetCurrentDateTime ();
			}
			else
			{
				return new DateTime (Convert.ToInt32 (Request ["year"]), Convert.ToInt32 (Request ["month"]), 1);
			}

		}

		/// <summary>
		/// They clicked to sort a column
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected override void btnSort_Click (object sender, System.EventArgs e)
		{
			base.btnSort_Click (sender, e);
			BindData (pgTop.CurrentPageNumber);
		}

		/// <summary>
		/// Page Change
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pgTop_PageChange (object sender, PageChangeEventArgs e)
		{
			BindData (e.PageNumber);
		}

		public const int ITEMS_PER_PAGE = 50;

		protected Repeater rptTransactions;
		protected Label lblSearch;
		protected Kaneva.Pager pgTop;
		protected PlaceHolder phBreadCrumb;

		protected Label lblMonth;

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
			pgTop.PageChanged +=new PageChangeEventHandler (pgTop_PageChange);		
		}
		#endregion
	}
}
