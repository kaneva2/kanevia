///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using MagicAjax;
using log4net;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for profile.
	/// </summary>
	public class profile2 : BasePage
	{
        protected profile2 () 
		{
			Title = "Kaneva Profile";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			// They must be logged in
			if (!Request.IsAuthenticated)
			{
				Response.Redirect (GetLoginURL ());
				return;
			}

			if (!IsPostBack)
			{
                LoadData ();
			}
                                                       
            Response.Expires = 0;
            Response.CacheControl = "no-cache";

            if (ViewState["returnPath"] == null)
            {
                if (Request.UrlReferrer != null && Request.UrlReferrer.PathAndQuery.Length > 0)
                {
                    ViewState["returnPath"] = Request.UrlReferrer.PathAndQuery;
                }
                else
                {
                    ViewState["returnPath"] = ResolveUrl ("~/default.aspx");
                }
            }
            
            litAddButtonClientId.Text = btnAdd.ClientID;
            litRemoveButtonClientId.Text = btnRemove.ClientID;
            btnAdd.Attributes.Add ("onclick", ClientScript.GetPostBackEventReference (this.btnAdd, "", false));
            btnRemove.Attributes.Add ("onclick", ClientScript.GetPostBackEventReference (this.btnRemove, "", false));

            // Set Nav
            ((GenericPageTemplate)Master).HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
            ((GenericPageTemplate)Master).HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.PROFILE;
            ((GenericPageTemplate)Master).HeaderNav.MyChannelsNav.ActiveTab = NavMyChannels.TAB.INTERESTS;
            ((GenericPageTemplate)Master).HeaderNav.MyChannelsNav.ChannelId = GetPersonalChannelId();

            ((GenericPageTemplate)Master).HeaderNav.SetNavVisible(((GenericPageTemplate)Master).HeaderNav.MyKanevaNav, 2);


        }

        #region Helper Methods

        /// <summary>
        /// Load the data.
        /// </summary>
        public void LoadData ()
        {
            int userId = GetUserId();

            // Get the About Me Info
            //don't reload this if the was an error due to improper words in an update attempt
            //user needs to see the words
            if (spnAlertMsg.InnerText == null || spnAlertMsg.InnerText == "")
            {
                txtAboutMe.Text = Server.HtmlDecode(KanevaWebGlobals.CurrentUser.Description);
            }

            // Get the Interest info
            DataTable dtInterests = GetInterestsFacade.GetInterests (userId);

            if (dtInterests != null)
            {
                string category = string.Empty;

                for (int i=0; i < dtInterests.Rows.Count; i++)
                {
                    if (category != dtInterests.Rows[i]["category_text"].ToString ())
                    {
                        category = dtInterests.Rows[i]["category_text"].ToString ();

                        // Create the container table for the category
                        HtmlTable tblContainer = new HtmlTable ();
                        tblContainer.Border = 0;
                        tblContainer.CellPadding = 3;
                        tblContainer.CellSpacing = 3;
                        tblContainer.Style["margin-top"] = "3px";
                        
                        //create the data row
                        HtmlTableRow row_tblContainer = new HtmlTableRow();
                        row_tblContainer.VAlign = "middle";


                        // Create cell to hold the category title (name)
                        HtmlTableCell cellCategoryTitle = new HtmlTableCell ();

                        cellCategoryTitle.Align = "left";
                        cellCategoryTitle.VAlign = "top";
                        cellCategoryTitle.NoWrap = true;
                        cellCategoryTitle.InnerHtml = "<strong>" + dtInterests.Rows[i]["category_text"].ToString () + ":</strong>";
                        
                        // Create cell to hold all the interests for this category
                        HtmlTableCell cellInterests = new HtmlTableCell ();
                        cellInterests.Align = "left";
                        cellInterests.VAlign = "middle";
                        
                        // Interest container div
                        cellInterests.InnerHtml = "<div id=\"divIC_" + dtInterests.Rows[i]["ic_id"].ToString () + "\">";

                        // add a new interest item
                        if (dtInterests.Rows[i]["interest"] != null && dtInterests.Rows[i]["interest"].ToString().Length > 0)
                        {
                            cellInterests.InnerHtml += "<div style=\"float:left;margin-right:2px;\"><span id=\"spnInt_" + dtInterests.Rows[i]["interest_id"].ToString () + "\">" +
                                " <a href=\"javascript:void(0);\" class=\"nohover\" title=\"Remove " +
                                    Server.HtmlEncode(dtInterests.Rows[i]["interest"].ToString ()) +
                                    "\" ><img border=\"0\" onclick=\"removeInterest(" + 
                                    dtInterests.Rows[i]["interest_id"].ToString () + "," + 
                                    dtInterests.Rows[i]["ic_id"].ToString () + ");\" src=\"" + 
                                    ResolveUrl ("~/images/del_sm.gif") + "\" /></a> " +
                                "<a id=\"aInt_" + dtInterests.Rows[i]["interest_id"].ToString () + "\" href=\"" + 
                                    ResolveUrl("~/people/people.kaneva?" + Constants.QUERY_STRING_INTEREST + "=" + 
                                    Server.UrlEncode(dtInterests.Rows[i]["interest"].ToString ())) + "\" >" + 
                                    "<span>" + Server.HtmlEncode(dtInterests.Rows[i]["interest"].ToString ()) + "</span></a> " +
                                "<span>| </span></span></div> ";
                        }
                        
                        // get all intersts for a particular category
                        while (++i < dtInterests.Rows.Count)
                        {
                            if (category == dtInterests.Rows[i]["category_text"].ToString ())
                            {
                                cellInterests.InnerHtml += "<div style=\"float:left;margin-right:2px;white-space:nowrap;\"><span id=\"spnInt_" + dtInterests.Rows[i]["interest_id"].ToString() + "\" style=\"WHITE-SPACE:nowrap;\">" +
                                    " <a href=\"javascript:void(0);\" class=\"nohover\" title=\"Remove " +
                                        Server.HtmlEncode (dtInterests.Rows[i]["interest"].ToString ()) +
                                        "\" ><img border=\"0\" onclick=\"removeInterest(" +
                                        dtInterests.Rows[i]["interest_id"].ToString () + "," +
                                        dtInterests.Rows[i]["ic_id"].ToString () + ");\" src=\"" +
                                        ResolveUrl ("~/images/del_sm.gif") + "\" /></a> " +
                                    "<a id=\"aInt_" + dtInterests.Rows[i]["interest_id"].ToString () + "\" href=\"" +
                                        ResolveUrl ("~/people/people.kaneva?" + Constants.QUERY_STRING_INTEREST + "=" +
                                        Server.UrlEncode (dtInterests.Rows[i]["interest"].ToString ())) + "\" >" +
                                        "<span>" + Server.HtmlEncode (dtInterests.Rows[i]["interest"].ToString ()) + "</span></a> " +
                                    "<span>| </span></span></div> ";
                            }
                            else
                            {
                                i--;
                                break;
                            }
                        }                               

                        if (i == dtInterests.Rows.Count) i--;

                        // close out interest div container tag
                        cellInterests.InnerHtml += "</div>";

                        //create the data entry row
                        HtmlTableRow row2_tblContainer = new HtmlTableRow();

                        // Create cell as space holder
                        HtmlTableCell cellSpace = new HtmlTableCell();
                        cellSpace.Align = "left";
                        cellSpace.VAlign = "bottom";

                        // Create cell to hold input field and buttons (images)
                        HtmlTableCell cellInput = new HtmlTableCell();
                        cellInput.Align = "left";
                        cellInput.VAlign = "middle";

                        string width = "169";
                        if (Request.Browser.Type.Substring (0, 2) == "IE")
                        {
                            width = "166";
                        }

                        cellInput.InnerHtml += "<table border=\"0\" id=\"spnAdd_" + dtInterests.Rows[i]["ic_id"].ToString () + 
                            "\" style=\"display:none;\" cellpadding=\"2\" cellspacing=\"0\"><tr valign=\"middle\"><td>" + 
                            "<div id=\"myAutoComplete\">" +
                                "<input type=\"text\" style=\"width:" + width + "px; top:-8px\" id=\"txt_" + 
                                dtInterests.Rows[i]["ic_id"].ToString () + "\" onkeypress=\"checkKey(event," +
                                dtInterests.Rows[i]["ic_id"].ToString () + ");\" onkeyup=\"checkLen (this," +
                                dtInterests.Rows[i]["ic_id"].ToString () + ");\" maxlength=\"51\" /> " +
                                "<div id=\"myContainer_" + dtInterests.Rows[i]["ic_id"].ToString () + "\"></div></div></td>" +
                            "<td><a href=\"javascript:void(0);\" class=\"nohover\" style=\"margin-left:2px;\" title=\"Save Interest\"><img class=\"imgAddInterest\" id=\"imgAdd_" +
                                dtInterests.Rows[i]["ic_id"].ToString() + "\" ic_id=\"" + dtInterests.Rows[i]["ic_id"].ToString() + "\" border=\"0\" src=\"" + 
                                ResolveUrl ("~/images/val_sm.gif") + "\" /></a></td>" +
                            "<td>&nbsp;<a href=\"javascript:void(0);\" class=\"nohover\" title=\"Cancel\"><img onclick=\"hideInput(" + 
                                dtInterests.Rows[i]["ic_id"].ToString () + ");\" border=\"0\" src=\"" +
                                ResolveUrl ("~/images/del_sm.gif") + "\"/></a></td>" +
                            "</tr><tr>" +
                            "<td colspan=\"3\" class=\"info\" style=\"padding-left:2px;white-space:no-wrap; \" id=\"tdMsg_" + dtInterests.Rows[i]["ic_id"].ToString() + "\">(start typing to see suggestions)</td>" +
                            "</tr></table>" +
                            "<a href=\"javascript:void(0)\" class=\"nohover\" title=\"Add Interest\" >" +
                            "<img onclick=\"showInput($j(this)," + dtInterests.Rows[i]["ic_id"].ToString () + 
                            ")\" id=\"imgNew_" + dtInterests.Rows[i]["ic_id"].ToString () + 
                            "\" border=\"0\" style=\"cursor: hand;\" src=\"" + ResolveUrl ("~/images/plus_sm.gif") + "\" /></a>";


                        // Add the columns to rows and rows to the table
      //                  if (cellInterests.InnerHtml.IndexOf("spnInt_") > 0)
      //                  {
                            // Add cells to the row control
                            row_tblContainer.Cells.Add(cellCategoryTitle);            //**
                            row_tblContainer.Cells.Add(cellInterests);                //**

       //                     row2_tblContainer.Cells.Add(cellSpace);
        //                    row2_tblContainer.Cells.Add(cellInput);

                            //Add rows to table
       //                     tblContainer.Rows.Add(row_tblContainer);
       //                     tblContainer.Rows.Add(row2_tblContainer);
      //                  }
      //                  else
      //                  {
                            // Add cells to the row control
      //                      row_tblContainer.Cells.Add(cellCategoryTitle);
                            row_tblContainer.Cells.Add(cellInput);                    //**

                            //Add rows to table
                            tblContainer.Rows.Add(row_tblContainer);                  //**
      //                  }

                        // Add new table to the container table cell
                        tdCategories.Controls.Add (tblContainer);
                    }
                }
            }
        }

        public int AddInterest (string interest, int ic_id)
        {
            int interest_id = GetInterestsFacade.InterestExists(ic_id, interest);

            if (interest_id == 0)
            {
                if (interest.Length > 0)
                {
                    // add interest
                    interest_id = GetInterestsFacade.AddInterest (ic_id, interest);
                }
            }

            return interest_id;
        }

        private void RedirectPage()
        {
            if (ViewState["returnPath"] != null && ViewState["returnPath"].ToString().Length > 0)
            {
                Response.Redirect(ViewState["returnPath"].ToString());
            }
            else
            {
                Response.Redirect(ResolveUrl("~/community/ProfilePage.aspx?communityId=" + GetPersonalChannelId()));
            }
        }

        #endregion


        #region Event Handlers

        /// <summary>
        /// Click event when user selects an interest value
        /// </summary>
        protected void lnkInterest_Click (object sender, EventArgs e)
        {
            Response.Redirect ("~/people/people.kaneva?interest=" + Server.UrlEncode(((HtmlAnchor)sender).InnerText));
        }

        /// <summary>
        /// Click event when user clicks a add icon
        /// </summary>
        protected void btnAdd_Click (object sender, EventArgs e)                   
        {
            try
            {
                int ic_id = Convert.ToInt32 (hidIc_id.Value);
                string interest = hidInterest.Value.Trim ();

                // Make sure input is okay
                if (KanevaWebGlobals.ContainsInjectScripts (interest))
                {
                    m_logger.Warn ("User " + KanevaWebGlobals.GetUserId () + " tried to script from IP " + Common.GetVisitorIPAddress());
                }
                else
                {
                    // Check to make sure user did not enter any "potty mouth" words
                    if (KanevaWebGlobals.isTextRestricted (interest, Constants.eRESTRICTION_TYPE.POTTY_MOUTH))
                    {
                        AjaxCallHelper.WriteAlert (Constants.VALIDATION_REGEX_RESTRICTED_WORD_MESSAGE);
                        
                        // Write out some Javascript to put the user back to the state where they can edit their entry
                        AjaxCallHelper.Write ("deactivateLB();" +                     // hide the lightbox if visible
                            "showInput($j('#imgNew_" + ic_id + "')," + ic_id + ");" +   // show the entry text box
                            "$('txt_" + ic_id + "').value='" + interest + "';");      // populate text box with entered value
                        return;
                    }

                    Button btnSender = (Button) sender;
                    int interest_id = 0;
                    string tagList = string.Empty;
                    string idList = string.Empty;
                    string intList = string.Empty;

                    // check to see if we need to enter multiple interests
                    if (btnSender.ID == "btnAddMultiple")
                    {
                        char[] seperator = new char[] {',' };
                        string[] arrInterests = interest.Split (seperator);

                        for (int i = 0; i < arrInterests.Length; i++)
                        {
                            arrInterests[i] = arrInterests[i].Trim ();

                            // Add interest or get id if already exists
                            interest_id = AddInterest (arrInterests[i], ic_id);

                            if (interest_id > 0)
                            {
                                // add interest to user
                                if (GetInterestsFacade.AssignInterestToUser(GetUserId(), interest_id, ic_id) == 0)
                                {
                                   if (idList.Length > 0)
                                    {
                                        idList += "|";
                                        intList += "|^|";
                                    }
                                    idList += interest_id.ToString ();
                                    intList += arrInterests[i];
                                }
                            }   
                        }

                        AjaxCallHelper.Write ("displayInterest('" + Server.HtmlEncode (intList.Replace ("'", "@~^~@")) + "','" + idList + "'," + ic_id + ");");
                    }
                    else // add as single entry
                    {
                        interest = interest.Trim ();
                        // Add interest or get id if already exists
                        interest_id = AddInterest (interest, ic_id);

                        if (interest_id > 0)
                        {
                            // add interest to user
                            if (GetInterestsFacade.AssignInterestToUser(GetUserId(), interest_id, ic_id) == 0)
                            {
                                // call js function to display the new added interest
                                AjaxCallHelper.Write ("displayInterest('" + Server.HtmlEncode (interest.Replace ("'", "@~^~@")) + "','" + interest_id + "'," + ic_id + ");");
                            }
                            else
                            {
                                // interest already exists for user, just reset the values
                                // of the hidden fields to defaults
                                AjaxCallHelper.Write ("resetVals();");
                            }
                        }
                        else
                        {
                            // the interest value passed in was invalid so reset the
                            // values of the hidden fields to defaults
                            AjaxCallHelper.Write ("resetVals();");
                        }
                    }
                }
            }
            catch 
            { }
        }

        /// <summary>
        /// Click event when user clicks a delete icon
        /// </summary>
        protected void btnRemove_Click (object sender, EventArgs e)
        {
            int interest_id = Convert.ToInt32 (hidInterestId.Value);
            int ic_id = Convert.ToInt32 (hidIc_id.Value);

            GetInterestsFacade.RemoveInterestFromUser(GetUserId(), interest_id);

            AjaxCallHelper.Write ("removeItem(" + interest_id + "," + ic_id + ");");
        }

        /// <summary>
        /// Click event when user clicks a delete icon
        /// </summary>
        protected void btnUpdate_Click (object sender, EventArgs e)
        {

            spnAlertMsg.Style.Add("display", "none");
            spnAlertMsg.InnerText = "";
           
            // Check for any inject scripts
            if (KanevaWebGlobals.ContainsInjectScripts (txtAboutMe.Text))
            {
                m_logger.Warn ("User " + KanevaWebGlobals.GetUserId () + " tried to script from IP " + Common.GetVisitorIPAddress());
                spnAlertMsg.InnerText = Constants.VALIDATION_REGEX_RESTRICTED_WORD_MESSAGE;
                spnAlertMsg.Attributes.Add("class", "warningBox black");
                spnAlertMsg.Style.Add("display", "block");
                
                //reload this data or else the page doesn't display the interests
                LoadData();
            }
            else if(KanevaWebGlobals.isTextRestricted (txtAboutMe.Text, Constants.eRESTRICTION_TYPE.ALL))
            {
                spnAlertMsg.InnerText = Constants.VALIDATION_REGEX_RESTRICTED_WORD_MESSAGE;
                spnAlertMsg.Attributes.Add("class", "warningBox black");
                spnAlertMsg.Style.Add("display", "block");

                //reload this data or else the page doesn't display the interests
                LoadData();
            }
            else
            {
                GetUserFacade.UpdateUser(GetUserId(), Server.HtmlEncode(txtAboutMe.Text.Trim()));

                RedirectPage();

            }
        }

        /// <summary>
        /// Click event for the cancel button
        /// </summary>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            RedirectPage();
        }

        #endregion

        #region Declerations

        protected HtmlGenericControl tdCategories;
        protected HtmlInputHidden hidIc_id, hidInterest, hidInterestId, hidTags;
        protected HtmlContainerControl spnAlertMsg;

        protected TextBox txtAboutMe;
        protected Button  btnAdd, btnRemove;
        protected Literal litAddButtonClientId, litRemoveButtonClientId;

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);
        
        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
