﻿<%@ Page Language="C#" MasterPageFile="~/masterpages/IndexPageTemplate.Master" AutoEventWireup="true" CodeBehind="passDetailsVIP.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.passDetailsVIP" %>

<asp:Content ID="cnt_passDetails" runat="server" contentplaceholderid="cph_Body" >
<style>
#wrapper {border:1px solid #ccc;border-top:none;background-color:#fff;width:1004px;margin:0 auto;height:auto !important;height:100%;min-height:100%;padding-bottom:5em;clear:both;display:inline-block;}
.PageContainer {background-color:#fff;width:1004px;height:100%;border-top:solid 1px #fff;}
</style>
   
    <div class="wrapper PageContainer">

		<div id="div_passDetailsImage" class="passDetailsImage" runat="server">

        </div>
        
		<!-- Level One Access Pass -->
        <div id="div_PassDetailsContent" class="contentContainer" runat="server">
			
			<div class="passContainer">
				<div class="imageLink" onclick="javascript:Submit();"></div>
				<div class="passText">
				
					<div class="special bold">Special: <span class="white">Exclusive Offer!</span></div>

					<div class="heading"><img src="../images/apvip/logo_vip_78x78.png" /><div>Be a VIP!</div></div>
					<div class="desc white bold">Style your Home World and Avatar with Credits.<br />
						Get a great deal when you sign-up for the <span>VIP PASS!</span></div>
		
					<div class="table bold">
						<img class="tableHeading" src="../images/apvip/logo_vip_small_33x33.gif" />
						<div class="tableHeading">Guest</div>
						<div class="row top"><div>Credits every month</div><img src="../images/apvip/check_24x19.png" /></div>
						<div class="row light"><div>Discount on all Items in Shop</div><img src="../images/apvip/check_24x19.png" /></div>
						<div class="row"><div>Exclusive Dance Moves</div><img src="../images/apvip/check_24x19.png" /></div>
						<div class="row light"><div>Special VIP Badge</div><img src="../images/apvip/check_24x19.png" /></div>
						<div class="row"><div>No 3rd party ads</div><img src="../images/apvip/check_24x19.png" /><img src="../images/apvip/check_24x19.png" /></div>
						<div class="row light"><div class="text">FREE Avatar, Clothes, and Home World!</div><img src="../images/apvip/check_24x19.png" /><img src="../images/apvip/check_24x19.png" /></div>
						<div class="footnote" style="display:none;">*Reward Points awarded 1000 per month for Monthly Passes, 12,000 instantly for Annual Passes.</div>
					</div>
		
					<div class="selectContainer" style="top:90px;">
						<div class="radioBtn">
							<span>
							    <input class="radio" type="radio" name="passId" value="91" checked="true" id="rdoBasicPass" runat="server" /><label class="white bold" style="font-size:12px;position:relative;top:1px;">Basic VIP</label><div style="font-size:12px;margin:-1px 8px 0 0;float:right;">$4.99</div><br />
							    <div style="margin:-2px 0 6px 21px;padding:0;font-size:11px;color:#ff8213;">1000 Credits</div>
							    <input class="radio" type="radio" name="passId" value="74" id="rdoPremiumPass" runat="server" /><label class="white bold" style="font-size:12px;position:relative;top:1px;">Premium VIP</label><div style="font-size:12px;margin-top:-1px;float:right;">$14.99</div>
							    <div style="margin:-2px 0 0 21px;padding:0;font-size:11px;color:#ff8213;">3000 Credits</div>
                            </span>
						</div>
						<img style="margin-top:12px;" src="../images/apvip/btn_getvip_220x41.png" onclick="javascript:Submit();" />
					</div>
					<input type="hidden" name="passId_" value='<asp:literal id="litPassId" runat="server"></asp:literal>' />
				</div>
			</div>

		</div>		 

		<div class="clear"><!-- clear the floats --></div>
        <div class="spTryPass"><asp:LinkButton ID="lb_trialPass" runat="server">TRY ME OUT</asp:LinkButton></div>
        <div class="clear"><!-- clear the floats --></div>
    
	</div>

<script type="text/javascript">
	function Submit() {
		document.forms[0].action = 'buySpecials.aspx?pass=true';
		document.forms[0].__VIEWSTATE.name = 'name';
		document.forms[0].submit(); 
	}
</script>
</asp:Content>