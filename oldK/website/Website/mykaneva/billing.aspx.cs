///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using MagicAjax;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for billing.
	/// </summary>
	public class billing : MainTemplatePage
	{
		protected billing () 
		{
			Title = "Billing Information";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			if (!KanevaGlobals.EnableCheckout)
			{
				if (!IsAdministrator())
				{
                    Response.Redirect("~/error.aspx?error=" + (int)Constants.eERROR_TYPE.CHECKOUT_CLOSED);
                }
			}

			// They must be logged in
			if (!Request.IsAuthenticated)
			{
				Response.Redirect (GetLoginURL ());
				return;
			}

			// Put user code to initialize the page here
			if (!IsPostBack)
			{
				BindData ();
			}

			// Set Nav
            HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
			HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.ACCOUNT;
			HeaderNav.MyAccountNav.ActiveTab = NavAccount.TAB.CREDITS;
			HeaderNav.SetNavVisible(HeaderNav.MyKanevaNav,2);
			HeaderNav.SetNavVisible(HeaderNav.MyAccountNav);

		}

		
		#region Helper Methods
	
		/// <summary>
		/// BindData ()
		/// </summary>
		protected void BindData ()
		{
			int userId = GetUserId ();

			DataTable dtAddresses = UsersUtility.GetAddresses (userId);
			 
			if (dtAddresses.Rows.Count > 0)
			{
				tblAddresses.Visible = true;
				dlAddress.DataSource = dtAddresses;
				dlAddress.DataBind ();
				
				if (IsPostBack)
				{
					MagicAjax.AjaxCallHelper.Write("scroll();");
				}
			}
			else
			{
				tblAddresses.Visible = false;
			}

			// Countries
			drpCountry.DataTextField = "Name";
			drpCountry.DataValueField = "CountryId";
			drpCountry.DataSource = WebCache.GetCountries ();
			drpCountry.DataBind ();

			drpCountry.Items.Insert (0, new ListItem ("United States", Constants.COUNTRY_CODE_UNITED_STATES));
			drpCountry.Items.Insert (0, new ListItem ("select...", ""));

			// States
			drpState.DataTextField = "StateCode";
			drpState.DataValueField = "StateCode";
			drpState.DataSource = WebCache.GetStates ();
			drpState.DataBind ();
			drpState.Items.Insert (0, new ListItem ("select...", ""));

			// reset all edit fields
			txtFullName.Text = string.Empty;
			txtAddress1.Text = string.Empty;
			txtAddress2.Text = string.Empty;
			txtCity.Text = string.Empty;
			drpState.SelectedValue = string.Empty;
			txtPostalCode.Text = string.Empty;
			drpCountry.SelectedValue = string.Empty;
			txtPhoneNumber.Text = string.Empty;
			hidUpdateId.Value = string.Empty;

			btnAddAddress.Visible = true;
			btnUpdateAddress.Visible = false;
		}

		/// <summary>
		/// Return the delete javascript
		/// </summary>
		public string GetDeleteAddressScript ()
		{
			return "javascript:if (!confirm(\"Are you sure you want to delete this address?\")){return false};";
		}

		private void PopulateAddressFields(int addressId)
		{
			DataRow drAddress = UsersUtility.GetAddress(GetUserId(), addressId);
	
			txtFullName.Text = drAddress ["name"].ToString ();
			txtAddress1.Text = drAddress ["address1"].ToString ();
			txtAddress2.Text = drAddress ["address2"].ToString ();
			txtCity.Text = drAddress ["city"].ToString ();
			drpState.SelectedValue = drAddress ["state_code"].ToString ();
			txtPostalCode.Text = drAddress ["zip_code"].ToString ();
			drpCountry.SelectedValue = drAddress ["country_id"].ToString ();
			txtPhoneNumber.Text = drAddress ["phone_number"].ToString ();

			hidUpdateId.Value = addressId.ToString ();

			btnAddAddress.Visible = false;
			btnUpdateAddress.Visible = true;
		}

		#endregion

		#region Event Handlers

		/// <summary>
		/// The add address event handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnAddAddress_Click (object sender, EventArgs e) 
		{			
			rfUsername.Enabled = true;
			rfAddress1.Enabled = true;
			rftxtCity.Enabled = true;

			// State is only required for US and Canada
			if (drpCountry.SelectedValue.Equals (Constants.COUNTRY_CODE_UNITED_STATES) || drpCountry.SelectedValue.Equals (Constants.COUNTRY_CODE_CANADA))
			{
				rfdrpState.Enabled = true;
				rftxtPostalCode.Enabled = true;
			}
			else
			{
				rfdrpState.Enabled = false;
				rftxtPostalCode.Enabled = false;
			}
			
			rfdrpCountry.Enabled = true;
			rftxtPhoneNumber.Enabled = true;

			Page.Validate ();

			if (!Page.IsValid) 
			{
				return;
			}

			// Insert the new billing address
			int addressId = UsersUtility.InsertAddress (GetUserId (), Server.HtmlEncode (txtFullName.Text), Server.HtmlEncode (txtAddress1.Text), 
				Server.HtmlEncode (txtAddress2.Text), Server.HtmlEncode (txtCity.Text), Server.HtmlEncode (drpState.SelectedValue),
				Server.HtmlEncode (txtPostalCode.Text), Server.HtmlEncode (txtPhoneNumber.Text), Server.HtmlEncode (drpCountry.SelectedValue));

			BindData ();
		}

		protected void btnUpdateAddress_Click (object sender, EventArgs e)
		{
			// Get the address id														  
			int addressId = Convert.ToInt32(hidUpdateId.Value);

			// Update the address
			UsersUtility.UpdateAddress(GetUserId(), addressId, 
				Server.HtmlEncode(txtFullName.Text), 
				Server.HtmlEncode(txtAddress1.Text),
				Server.HtmlEncode(txtAddress2.Text),
				Server.HtmlEncode(txtCity.Text),
				drpState.SelectedValue,
				Server.HtmlEncode(txtPostalCode.Text),
				Server.HtmlEncode(txtPhoneNumber.Text),
				drpCountry.SelectedValue);

			btnAddAddress.Visible = true;
			btnUpdateAddress.Visible = false;

			BindData();
		}

		/// <summary>
		/// rptAddress_ItemCommand
		/// </summary>
		private void dlAddress_ItemCommand (object source, DataListCommandEventArgs e)
		{
			string command = e.CommandName;			

			HtmlInputHidden hidAddressId;

			switch (command)
			{
				case "cmdEdit":
				{
					int index = e.Item.ItemIndex;
					hidAddressId = (HtmlInputHidden) dlAddress.Items [index].FindControl ("hidAddressId");

					PopulateAddressFields(Convert.ToInt32(hidAddressId.Value));
					break;
				}
				case "cmdDelete":
				{
					int index = e.Item.ItemIndex;
					hidAddressId = (HtmlInputHidden) dlAddress.Items [index].FindControl ("hidAddressId");

					UsersUtility.DeleteAddress (Convert.ToInt32 (hidAddressId.Value), GetUserId ());
					BindData ();
					break;
				}
			}
		}

		/// <summary>
		/// rptAddress_ItemDataBound
		/// </summary>
		private void dlAddress_ItemDataBound (object sender, DataListItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
			{
				LinkButton lbDelete = (LinkButton) e.Item.FindControl ("lbDelete");
				if (lbDelete != null)
				{
					lbDelete.Attributes.Add ("onclick", GetDeleteAddressScript ());
				}
			}
		}
 		
		#endregion

		#region Declerations

		/// New Address
		protected DropDownList				drpState, drpCountry;
		protected TextBox					txtFullName, txtAddress1, txtAddress2, txtCity, txtPostalCode, txtPhoneNumber;
		protected Button					btnAddAddress, btnUpdateAddress;
		protected RequiredFieldValidator	rfUsername, rfAddress1, rftxtCity, rfdrpState, rftxtPostalCode, rfdrpCountry, rftxtPhoneNumber;

		protected DataList	dlAddress;

		protected HtmlTable				tblAddresses;
		protected HtmlInputHidden		hidUpdateId;

		#endregion

		#region Web Form Designer generated code

		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
			this.dlAddress.ItemCommand	+=new DataListCommandEventHandler (dlAddress_ItemCommand);

			dlAddress.ItemDataBound +=new DataListItemEventHandler (dlAddress_ItemDataBound);
		}

		#endregion

	}
}
