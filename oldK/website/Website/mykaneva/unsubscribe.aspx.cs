///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using System.Security.Cryptography;
using System.Web.Security;
using log4net;

using KlausEnt.KEP.Kaneva.framework.widgets;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
    public partial class unsubscribe : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                int userId = Convert.ToInt32(Request.QueryString["u"]);
                UserFacade userFacade = new UserFacade();
                User user = userFacade.GetUser(userId);
                if (Request.QueryString["kv"] != user.KeyValue)
                {
                    //User not identified
                    if (Request.QueryString["email"] != null)
                    {
                        txtEmail.Text = Request.QueryString["email"].ToString();
                    }

                    showUnsubForm();
                }
                else
                {
                    txtEmail.Text = user.Email;
                }
            }
            catch
            {
                // Unable to get user Id
                showUnsubForm();
            }


        }

        protected void showUnsubForm()
        {
            dvOptions.Visible = false;
            dvUnsubForm.Visible = true;
        }

        protected void btnNoComm_Click(object sender, EventArgs e)
        {
            try
            {
                int userId = Convert.ToInt32(Request.QueryString["u"]);
                string keyvalue = Request.QueryString["kv"];

                UserFacade userFacade = new UserFacade();
                userFacade.UpdateUserNoCommunication(userId, keyvalue);
            }
            catch (Exception exc)
            {
                msgStatus.InnerText = exc.ToString();
                msgStatus.Visible = true;
                return;
            }

            dvOptions.Visible = false;

            msgStatus.InnerText = "Your preferences have been set.";
            msgStatus.Visible = true;
        }

        protected void btnUnsubscribe_Click(object sender, EventArgs e)
        {
            string email;
            int user_id;

            try
            {
                // Check email 
                if (txtEmail.Text != null && txtEmail.Text != "")
                {
                    email = txtEmail.Text;
                }
                else
                {
                    spnError.InnerText = "Please enter your email address.";
                    return;
                }

                // Check user_id

                DataRow drUser = UsersUtility.GetUserByEmail(email);
                if (drUser != null)
                {
                    user_id = Convert.ToInt32(drUser["user_id"]);
                }
                else
                {
                    user_id = 0;
                }

                UsersUtility.UnsubscribeEmail(email);

                pgTitle.InnerText = "Sorry to see you go...";
                msgStatus.InnerText = "You have been unsubscribed from all Kaneva communication. This request may take up to 7 days to process.";
                msgStatus.Visible = true;
                dvUnsubForm.Visible = false;
            }
            catch
            {
                msgStatus.InnerText = "Error unsubscribing.";
                msgStatus.Visible = true;
            }

        }

        protected void btnPrefrences_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/myKaneva/settings.aspx");
        }

        #region Declarations

        protected ImageButton btnUnsubscribe;
        protected Button btnNoComm, btnPrefrences;
        protected HtmlContainerControl dvOptions, dvUnsubForm, spnError, pgTitle, msgStatus;
        protected TextBox txtEmail;

        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion

    }
}
