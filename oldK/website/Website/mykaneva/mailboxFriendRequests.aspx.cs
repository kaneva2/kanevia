///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using log4net;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for mailboxFriendRequests.
	/// </summary>
	public class mailboxFriendRequests  : SortedBasePage
	{
		protected mailboxFriendRequests () 
		{
			Title = "Pending Friend Requests";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			// They must be logged in
			if (!Request.IsAuthenticated)
			{
				Response.Redirect (GetLoginURL ());
				return;
			}

            // Redirect to the new Media Center page
            Response.Redirect (ResolveUrl ("~/mykaneva/messagecenter.aspx?mc=fr"));

			if (!IsPostBack)
			{
				searchFilter.CurrentPage = "friendrequests";

				CurrentSort = "request_date";
				lbSortByDate.CssClass = "selected";
				
				int userId = GetUserId ();
				BindData (1, userId);
			}

			// Set Nav
			HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
			HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.FRIENDS;
			HeaderNav.MyFriendNav.ActiveTab = NavFriend.TAB.REQUESTS;
			HeaderNav.SetNavVisible(HeaderNav.MyKanevaNav,2);
			HeaderNav.SetNavVisible(HeaderNav.MyFriendNav);
		}

		/// <summary>
		/// Bind the data
		/// </summary>
		/// <param name="pageNumber"></param>
		private void BindData (int pageNumber, int userId)
		{
			BindData (pageNumber, userId, "");	
		}
		/// <summary>
		/// Bind the data
		/// </summary>
		/// <param name="pageNumber"></param>
		private void BindData (int pageNumber, int userId, string filter)
		{
			searchFilter.CurrentPage = "friendrequests";

			// Set the sortable columns
			string orderby = CurrentSort + " " + CurrentSortOrder;
			int pgSize = searchFilter.NumberOfPages;

			//PagedDataTable pds = UsersUtility.GetUserMessages (
			PagedDataTable pds = UsersUtility.GetIncomingPendingFriends(userId, filter, orderby, true, 
				pageNumber, pgSize);

			rptRequests.DataSource = pds;
			rptRequests.DataBind ();

			pgTop.NumberOfPages = Math.Ceiling ((double) pds.TotalCount / pgSize).ToString ();
			pgTop.DrawControl ();

			// The results
			lblSearch.Text = GetResultsText (pds.TotalCount, pageNumber, pgSize, pds.Rows.Count);

			tblRequests.Visible = true;
		}

		
		#region Helper Methods
		protected string ShowAge(int age)
		{
			return (age < 18 ? "< 18" : age.ToString()); 
		}
		#endregion

		#region Event Handlers
		protected void btnApprove_Click (Object sender, ImageClickEventArgs e)
		{
			int ret = 0;
			int count = 0;
			bool isItemChecked = false;
  			spnAlertMsg.InnerText = "";
            UserFacade userFacade = new UserFacade ();

			foreach (RepeaterItem dliMessage in rptRequests.Items)
			{
				CheckBox chkEdit = (CheckBox) dliMessage.FindControl ("chkEdit");

				if (chkEdit.Checked)
				{
					isItemChecked = true;
					
					HtmlInputHidden hidFriendId = (HtmlInputHidden) dliMessage.FindControl ("hidFriendId");
					int friendId = Convert.ToInt32(hidFriendId.Value);
                    ret = userFacade.AcceptFriend(KanevaWebGlobals.CurrentUser.UserId, friendId);

                    if (ret == 1)
                    {
                        count++;

                        try
                        {
                            if (userFacade.GetUser (friendId).NotifyNewFriends)
                            {
                                MailUtilityWeb.SendNewFriendNotification (friendId, KanevaWebGlobals.CurrentUser.UserId);
                            }
                        }
                        catch { }
                    }
				}
			}
			
			if (isItemChecked)
			{
				if (count > 0)
				{
					Response.Redirect("~/mykaneva/friendsaddsuccess.aspx?new=" + count.ToString());
				}
			
				if (ret != 1)
				{
					spnAlertMsg.InnerText = "An error was encountered while trying to approve Friend Request.";
				}
				else
				{
					spnAlertMsg.InnerText = "No friend requests were accepted.";
				}
			}
			else
			{
				spnAlertMsg.InnerText = "No friend requests were selected.";
			}
			
			MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('spnAlertMsg', 5000);");
		}

		protected void btnDecline_Click (Object sender, ImageClickEventArgs e)
		{
			bool isItemChecked = false;
			spnAlertMsg.InnerText = "";

			foreach (RepeaterItem dliMessage in rptRequests.Items)
			{
				CheckBox chkEdit = (CheckBox) dliMessage.FindControl ("chkEdit");

				if (chkEdit.Checked)
				{
					isItemChecked = true;
					break;
				}
			}
			
			if (isItemChecked)
			{
				// Hide Request List
				tblRequests.Visible = false;

				// Show Are You Sure message
				divConfirm.Visible = true;
			}
			else
			{
				spnAlertMsg.InnerText = "No friend requests were selected.";	

				MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('spnAlertMsg', 5000);");
			}
		}
		private void rptRequests_ItemCommand(object source, RepeaterCommandEventArgs e)
		{
			int ret = 0;
			string command = e.CommandName;
			spnAlertMsg.InnerText = "";

			if (command.Equals("cmdApprove"))
			{
				HtmlInputHidden hidFriendId = (HtmlInputHidden) e.Item.FindControl("hidFriendId");
				int friendId = Convert.ToInt32(hidFriendId.Value);
                ret = GetUserFacade.AcceptFriend (KanevaWebGlobals.CurrentUser.UserId, friendId);

				if (ret == 1)
				{
                    UserFacade userFacade = new UserFacade ();                
                    
                    if (userFacade.GetUser (friendId).NotifyNewFriends)
                    {
                        MailUtilityWeb.SendNewFriendNotification (friendId, KanevaWebGlobals.CurrentUser.UserId);
                    }
                    
                    Response.Redirect ("~/mykaneva/friendsaddsuccess.aspx?new=1");
				}
				else
				{
					// Error
					spnAlertMsg.InnerText = "An error was encountered while trying to approve friend request.";

					MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('spnAlertMsg', 5000);");
				}
			}
			else if (command.Equals("cmdDecline"))
			{
				CheckBox chkEdit = (CheckBox) e.Item.FindControl ("chkEdit");
				chkEdit.Checked = true;

				// Hide Request List
				tblRequests.Visible = false;

				// Show Are You Sure message
				divConfirm.Visible = true;
			}
		}

		protected void btnYes_Click (Object sender, ImageClickEventArgs e)
		{
			int ret = 0;
			int count = 0;
  			spnAlertMsg.InnerText = "";

			foreach (RepeaterItem dliMessage in rptRequests.Items)
			{
				CheckBox chkEdit = (CheckBox) dliMessage.FindControl ("chkEdit");

				if (chkEdit.Checked)
				{
					HtmlInputHidden hidFriendId = (HtmlInputHidden) dliMessage.FindControl ("hidFriendId");
					int friendId = Convert.ToInt32(hidFriendId.Value);
                    ret = GetUserFacade.DenyFriend(this.GetUserId(), friendId);
				
					if (ret == 1)
						count++;
				}
			}
							
			divConfirm.Visible = false;
			BindData (1, GetUserId());
			
			spnAlertMsg.InnerText = count.ToString() + " friend request" + (count != 1 ? "s were" : " was") + " declined.";
			MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('spnAlertMsg', 5000);");
		}
		
		protected void btnNo_Click (Object sender, ImageClickEventArgs e)
		{
			spnAlertMsg.InnerText = "";
			divConfirm.Visible = false;
			BindData (1, GetUserId());
		}
		/// <summary>
		/// Page Change
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pgTop_PageChange (object sender, PageChangeEventArgs e)
		{
			BindData (e.PageNumber, GetUserId ());
		}


		/// <summary>
		/// Execute when the user selects an item from the Store Filter
		/// Items to display drop down list
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void FilterChanged(object sender, FilterChangedEventArgs e)
		{
			searchFilter.SetPagesDropValue(searchFilter.NumberOfPages.ToString());
			
			BindData (1, GetUserId(), e.Filter);
		}
		/// <summary>
		/// Execute when the user clicks the the view type link button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void lbSortBy_Click (object sender, EventArgs e) 
		{
			lbSortByName.CssClass = lbSortByDate.CssClass = "";

			if (((LinkButton)sender).Attributes["sortby"].ToString().ToLower() == "name")
			{
				CurrentSort = "u.username";
				CurrentSortOrder = "ASC";
				lbSortByName.CssClass = "selected";
			}
			else
			{
				CurrentSort = "request_date";
				CurrentSortOrder = "DESC";
				lbSortByDate.CssClass = "selected";							  
			}

			BindData (pgTop.CurrentPageNumber, GetUserId());
		}
		#endregion

		#region Properties
		/// <summary>
		/// DEFAULT_SORT
		/// </summary>
		/// <returns></returns>
		protected override string DEFAULT_SORT
		{
			get
			{
				return "username";
			}
		}

		/// <summary>
		/// DEFAULT_SORT ORDER
		/// </summary>
		/// <returns></returns>
		protected override string DEFAULT_SORT_ORDER
		{
			get
			{
				return "DESC";
			}
		}

		#endregion

		#region Declerations
		protected Kaneva.Pager pgTop;
		protected Kaneva.SearchFilter searchFilter;

		protected Label lblSearch;
		protected Repeater rptRequests;	
		protected LinkButton	lbSortByName, lbSortByDate;
		protected ImageButton	btnAccept, btnDecline, btnYes;

		protected HtmlContainerControl	spnAlertMsg, divConfirm;
		protected HtmlTable				tblRequests;

		#endregion

		// Logger
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

		#region Web Form Designer generated code

		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
			pgTop.PageChanged +=new PageChangeEventHandler (pgTop_PageChange);
			searchFilter.FilterChanged +=new FilterChangedEventHandler (FilterChanged);		
			
			this.rptRequests.ItemCommand +=new RepeaterCommandEventHandler (rptRequests_ItemCommand);
		}
		#endregion
	}
}
