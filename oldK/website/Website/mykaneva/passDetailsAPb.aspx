﻿<%@ Page Language="C#" MasterPageFile="~/masterpages/IndexPageTemplate.Master" AutoEventWireup="true" CodeBehind="passDetailsAPb.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.passDetailsAPb" %>

<asp:Content ID="cnt_passDetails" runat="server" contentplaceholderid="cph_Body" >
<!-- Google Website Optimizer Tracking Script -->
<script type="text/javascript">
	var _gaq = _gaq || [];
	_gaq.push(['gwo._setAccount', 'UA-5755114-4']);
	_gaq.push(['gwo._trackPageview', '/3204893232/test']);
	(function () {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();
</script>
<!-- End of Google Website Optimizer Tracking Script -->
<style>
#wrapper {border:1px solid #ccc;border-top:none;background-color:#fff;width:1004px;margin:0 auto;height:auto !important;height:100%;min-height:100%;padding-bottom:5em;clear:both;display:inline-block;}
.PageContainer {background-color:#fff;width:1004px;height:100%;border-top:solid 1px #fff;}
</style>

    
    <div class="wrapper ap PageContainer">

		<div id="div_passDetailsImage" class="passDetailsImage" runat="server">

        </div>
        
		<!-- Level One Access Pass -->
        <div id="div_PassDetailsContent" class="contentContainer" runat="server">
			
			<div class="passContainer">
				<div class="imageLink" onclick="javascript:Submit();"></div>
				<div class="passText">
				
					<div class="special bold">Special: <span class="white">Limited Time Offer!</span></div>

					<div class="heading"><img src="../images/apvip/logo_aponly_78x78.png" /><div>All Access Pass</div></div>
					<div class="desc white bold">Explore the adult side of Kaneva with <span>ACCESS PASS!</span></div>
		
					<div class="bulletPts white">									  
						<div>18+ member only crowd</div>
						<div>Search, meet and socialize with adults only</div>
						<div>Exclusive adult only items and Worlds</div>
						<div>Access, view, and share adult only content</div>
						<div>Shop for adult only apparel including sexy lingerie</div>
						<div>Get intimate with animations and adult only interactions</div>
					</div>
		
					<div class="selectContainer">
						<div class="radioBtn">
							<span>
								<input class="radio" type="radio" name="passId" value="82"/><label class="white bold">AP Annual Pass - $29.99</label><br />
								<div class="discount">Huge Savings!</div>
								<input class="radio" type="radio" name="passId" value="68"  checked="true" /><label class="footnote bold">AP Monthly Pass - $19.99</label>
							</span>
						</div>
						<img src="../images/apvip/btn_getap_240x41.png" onclick="javascript:Submit();" />
					</div>
				</div>
			</div>

		</div>

		<div class="clear"><!-- clear the floats --></div>
        <div class="spTryPass"><asp:LinkButton ID="lb_trialPass" runat="server">TRY ME OUT</asp:LinkButton></div>
        <div class="clear"><!-- clear the floats --></div>
	</div>

<script type="text/javascript">
	function Submit() {
		document.forms[0].action = 'buySpecials.aspx?pass=true';
		document.forms[0].__VIEWSTATE.name = 'name';
		document.forms[0].submit();
	}
</script>
</asp:Content>