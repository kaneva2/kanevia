///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for editAddress.
	/// </summary>
	public class editAddress : MainTemplatePage
	{
		protected editAddress () 
		{
			Title = "Edit Address";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			// They must be logged in
			if (!Request.IsAuthenticated)
			{
				Response.Redirect (GetLoginURL ());
				return;
			}

			if (!IsPostBack)
			{
				int userId = GetUserId ();
				int addressId = Convert.ToInt32 (Request ["aId"]);

				// Countries
				drpCountry.DataTextField = "Name";
				drpCountry.DataValueField = "CountryId";
				drpCountry.DataSource = WebCache.GetCountries ();
				drpCountry.DataBind ();

				drpCountry.Items.Insert (0, new ListItem ("United States", Constants.COUNTRY_CODE_UNITED_STATES));
				drpCountry.Items.Insert (0, new ListItem ("select...", ""));

				// States
				drpState.DataTextField = "Name";
                drpState.DataValueField = "StateCode";
				drpState.DataSource = WebCache.GetStates ();
				drpState.DataBind ();
				drpState.Items.Insert (0, new ListItem ("select...", ""));

				// Address information
				DataRow drAddress = UsersUtility.GetAddress (userId, addressId);

				txtFullName.Text = Server.HtmlDecode (drAddress ["name"].ToString ()); 
				txtAddress1.Text = Server.HtmlDecode (drAddress ["address1"].ToString ());
				txtAddress2.Text = Server.HtmlDecode (drAddress ["address2"].ToString ());
				txtCity.Text = Server.HtmlDecode (drAddress ["city"].ToString ());
				txtPostalCode.Text = Server.HtmlDecode (drAddress ["zip_code"].ToString ());
				txtPhoneNumber.Text = Server.HtmlDecode (drAddress ["phone_number"].ToString ());

				SetDropDownIndex (drpState, Server.HtmlDecode (drAddress ["state_code"].ToString ()), false);
				SetDropDownIndex (drpCountry, Server.HtmlDecode (drAddress ["country_id"].ToString ()), false);	  
			}
		}

		/// <summary>
		/// btnUpdateAddress_Click
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnUpdateAddress_Click (object sender, EventArgs e)
		{
			// State is only required for US and Canada
			if (drpCountry.SelectedValue.Equals (Constants.COUNTRY_CODE_UNITED_STATES) || drpCountry.SelectedValue.Equals (Constants.COUNTRY_CODE_CANADA))
			{
				rfdrpState.Enabled = true;
				rftxtPostalCode.Enabled = true;
			}
			else
			{
				rfdrpState.Enabled = false;
				rftxtPostalCode.Enabled = false;
			}

			Page.Validate ();

			if (!Page.IsValid) 
			{
				return;
			}

			// Update address
			UsersUtility.UpdateAddress (GetUserId (), Convert.ToInt32 (Request ["aId"]), Server.HtmlEncode (txtFullName.Text),
				Server.HtmlEncode (txtAddress1.Text), Server.HtmlEncode (txtAddress2.Text), Server.HtmlEncode (txtCity.Text), 
				Server.HtmlEncode (drpState.SelectedValue), Server.HtmlEncode (txtPostalCode.Text), Server.HtmlEncode (txtPhoneNumber.Text),
				Server.HtmlEncode (drpCountry.SelectedValue));
			Response.Redirect (ResolveUrl ("~/mykaneva/billing.aspx"));
		}

		/// <summary>
		/// btnCancel_Click
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnCancel_Click (object sender, EventArgs e)
		{
			Response.Redirect (ResolveUrl ("~/mykaneva/billing.aspx"));
		}

		/// Address
		protected DropDownList drpState, drpCountry;
		protected TextBox txtFullName, txtAddress1, txtAddress2, txtCity, txtPostalCode, txtPhoneNumber;
		protected RequiredFieldValidator rfUsername, rfAddress1, rftxtCity, rfdrpState, rftxtPostalCode, rfdrpCountry, rftxtPhoneNumber;

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
