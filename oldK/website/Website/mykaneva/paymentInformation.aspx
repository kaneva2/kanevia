<%@ Page language="c#" Codebehind="paymentInformation.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.paymentInformation" %>

<asp:ValidationSummary ShowMessageBox="True" ShowSummary="False" Class="formError" id="valSum" DisplayMode="BulletList" runat="server" HeaderText="Please correct the following errors:"/>
	
<div class="container">
	<center>
	<table border="0" cellpadding="10" cellspacing="0" width="100%">
		<tr>
			<td align="left" class="bodyText">
				<br/>
				You may begin selling items and earning money on Kaneva as soon as you join. Payment will not be made until revenues are earned and tax forms are completed.
				<br/><br/>Kaneva pays once a month within 15 days of the month end provided your earned balance is greater then or equal to $10.00 US.
				<br/><br/>All sales of more then 45 days as of the end of the month are included in your earnings account. Until 45 days the sales will be held in an "unearned" account,
				and will be paid in the following month when earned.
			</td>
		</tr>
		<tr runat="server" id="trNoTaxes">
			<td class="bodyText" align="center" valign="top">
				<span class="subHead3" style="color:red;"><font size="+1">You have not submitted any tax information to Kaneva.<br.>
				If you are a U.S. person, please submit an electronic W-9. All non-U.S. persons must complete electronic form W-8</font></span>
			</td>
		</tr>
		<tr>
			<td class="bodyText" align="center" valign="top">
				<B>for U.S. persons</B>
				<asp:button id="btnSubmitW9" runat="Server" onClick="btnSubmitW9_Click" class="Filter2" Text=" submit electronic W-9"/>&nbsp;<A href="http://www.irs.gov/pub/irs-pdf/fw9.pdf" style="COLOR: #3258ba; font-size: 12px;" target="MAIN">IRS W-9 instructions</a> <a href="http://www.adobe.com/products/acrobat/readstep2.html" target="_resource"><img runat="server" src="~/images/PDF_icon2.gif" border="0" alt="Get the Adobe Acrobat Reader" ID="Img1"/></a>
				<br/><br/><B>-or-</B><br/><br/><B>for non-U.S. persons</B>
				<asp:button id="btnSubmitW8" Enabled="False" runat="Server" onClick="btnSubmitW9_Click" class="Filter2" Text=" submit electronic W-8"/>&nbsp;<A href="http://www.irs.gov/pub/irs-pdf/fw8ben.pdf" style="COLOR: #3258ba; font-size: 12px;" target="MAIN">IRS W-8 instructions</a> <a href="http://www.adobe.com/products/acrobat/readstep2.html" target="_resource"><img runat="server" src="~/images/PDF_icon2.gif" border="0" alt="Get the Adobe Acrobat Reader"/></a>
			</td>
		</tr>
	</table>

	<table cellpadding="1" cellspacing="0" border="0" width="100%" runat="server" ID="tblCurrentW9Information" style="border: 1px solid #666666;">
		<tr>
			<td colspan="3" align="right" valign="middle" style="background-color: #868686;">
			<img runat="server" src="~/images/spacer.gif" width="100" height="1"/>
			</td>
			<td colspan="2" style="background-color: #868686;" width="380">&nbsp;&nbsp;<span class="header02"><b>Forms Submitted<b></b></span>
			</td>
		</tr>
		<tr>
			<td colspan="5" align="right" valign="middle" style="background-color: #868686;">
				<asp:DataGrid runat="server" EnableViewState="False" ShowFooter="False" Width="100%" id="dgrdTax" cellpadding="0" cellspacing="0" border="0" AutoGenerateColumns="False" AllowSorting="True">  
					<HeaderStyle CssClass="lineItemColHead"/>
					<ItemStyle CssClass="lineItemEven"/>
					<AlternatingItemStyle CssClass="lineItemOdd"/>
					<Columns>
						<asp:TemplateColumn HeaderText="Form" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="middle" ItemStyle-Width="10%" ItemStyle-BorderWidth="1" ItemStyle-BorderColor="#666666">
							<ItemTemplate>
								<%# GetForm (DataBinder.Eval(Container.DataItem, "is_w9").ToString (), DataBinder.Eval(Container.DataItem, "is_w8").ToString ()) %>
							</ItemTemplate>
						</asp:TemplateColumn>
						<asp:TemplateColumn HeaderText="name" SortExpression="name" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="middle" ItemStyle-Width="60%" ItemStyle-BorderWidth="1" ItemStyle-BorderColor="#666666">
							<ItemTemplate>
								<%# TruncateWithEllipsis (DataBinder.Eval(Container.DataItem, "name").ToString (), 100) %>
							</ItemTemplate>
						</asp:TemplateColumn>
						<asp:TemplateColumn HeaderText="submitted" SortExpression="submitted_date" ItemStyle-Width="30%" ItemStyle-Font-Size="8px" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="middle" ItemStyle-BorderWidth="1" ItemStyle-BorderColor="#666666">
							<ItemTemplate>
								<%# FormatDateTime (DataBinder.Eval(Container.DataItem, "submitted_date"))%>
							</ItemTemplate>
						</asp:TemplateColumn>
					</Columns>
				</asp:datagrid>	
			</td>
		</tr>
	</table>


	<table cellpadding="0" cellspacing="1" border="0" width="730">
		<tr>							
			<td align="right"><br>
				<br><br><br>
			</td>
		</tr>
	</table>	
	</center>
</div>