<%@ Page language="c#" Codebehind="incomeMonthly.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.incomeMonthly" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../usercontrols/Pager.ascx" %>

<link href="../css/home.css" rel="stylesheet" type="text/css" />		
<link href="../css/kanevaSC.css" rel="stylesheet" type="text/css"/>
<link href="../css/friends.css" rel="stylesheet" type="text/css"/>
<link href="../css/kanevaText.css" type="text/css" rel="stylesheet">

<table width="80%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><img runat="server" src="~/images/spacer.gif" width="1" height="14"/></td>
  </tr>
  <tr>    
  </tr>
  <tr>
    <td>
	<table border="0" cellpadding="0" cellspacing="0" width="990">
      <tr>
        <td valign="top"><table  border="0" cellpadding="0" cellspacing="0" width="990">
          <tr>
            <td width="10" class="frTopLeft"></td>
            <td width="973" class="frTop"></td>
            <td width="7" class="frTopRight"></td>
          </tr>
          <tr>
            <td bgcolor="#f1f1f2" class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1"/></td>
            <td valign="top" bgcolor="#f1f1f2" class="frBgIntMembers"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td align="left">
					<table border="0" cellpadding="0" cellspacing="0">
                      <tr>
                        <td align="left">
                        
                        </td>
                      </tr>
                    </table>
                      <table width="966"  border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td bgcolor="#d8d8d8"><img runat="server" src="~/images/spacer.gif" width="10" height="1"/></td>
                        </tr>
                    </table></td>
                </tr>
              </table>
                <table  border="0" cellpadding="0" cellspacing="0" width="100%" >
                  <tr>
                    <td class="boxInsideTopLeft"><img runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
                    <td class="boxInsideTop" ><img runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
                    <td  class="boxInsideTopRight"><img runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
                  </tr>
                  <tr class="boxInside">
                    <td class="boxInsideleft" ><img runat="server" src="~/images/spacer.gif" width="1" height="4"/></td>
                    <td align="center" class="boxInsideContent"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="6" height="6"><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
                        <td width="6" height="6"><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
                        <td width="6" height="6"><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
                      </tr>
                      <tr>
                        <td width="6" height="6"><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
                        <td width="100%" class="textGeneralGray11">
							Find out what&rsquo;s the most popular media you own, or how many people viewed it by month.
							<Kaneva:Pager runat="server" id="pgTop"/>
                        </td>
                        <td width="6" height="6"><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
                      </tr>
                      <tr>
                        <td width="6" height="6"><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
                        <td width="6" height="6"><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
                        <td width="6" height="6"><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
                      </tr>
                    </table></td>
                    <td class="boxInsideRight"><img runat="server" src="~/images/spacer.gif" width="1" height="4"/></td>
                  </tr>
                  <tr>
                    <td class="boxInsideBottomLeft" ><img runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
                    <td class="boxInsideBottom2"><img runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
                    <td class="boxInsideBottomRight"><img runat="server" src="~/images/spacer.gif" width="4" height="4" /></td>
                  </tr>
              </table></td>
            <td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1"/></td>
          </tr>
          <tr>
            <td class="frBottomLeft"></td>
            <td class="frBottom"></td>
            <td class="frBottomRight"></td>
          </tr>
          <tr>
            <td><img runat="server" src="~/images/spacer.gif" width="1" height="10"/></td>
          </tr>
        </table></td>
        </tr>
      <tr>
        <td valign="top"><table  border="0" cellpadding="0" cellspacing="0" width="990">
          <tr>
            <td class="frTopLeft"></td>
            <td class="frTop"></td>
            <td class="frTopRight"></td>
          </tr>
          <tr>
            <td bgcolor="#f1f1f2" class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1"/></td>
            <td valign="top" bgcolor="#f1f1f2" class="frBgIntMembers"><table  border="0" cellpadding="0" cellspacing="0" width="100%" >
                  <tr>
                    <td class="boxInsideTopLeft"><img runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
                    <td class="boxInsideTop" ><img runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
                    <td  class="boxInsideTopRight"><img runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
                  </tr>
                  <tr class="boxInside">
                    <td class="boxInsideleft" ><img runat="server" src="~/images/spacer.gif" width="1" height="4" /></td>
                    <td align="center" class="boxInsideContent"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="6" height="6"><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
                        <td width="6" height="6"><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
                        <td width="6" height="6"><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
                      </tr>
                      <tr>
                        <td width="6" height="6"><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
                        <td width="100%"><table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0">
                          <tr>
                            <td width="7" class="intBorderCTopLeft"><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
                            <td class="intBorderCTop"><img runat="server" src="~/images/spacer.gif" width="300" height="7"/></td>
                            <td width="1" class="intBorderCTop"><img runat="server" src="~/images/spacer.gif" width="1" height="7"/></td>
                            <td class="intBorderCTop"><img runat="server" src="~/images/spacer.gif" width="300" height="7"/></td>
                            <td width="6" class="intBorderCTopRight"><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
                          </tr>
                          <tr>
                            <td class="intBorderCBorderLeft"><img runat="server" src="~/images/spacer.gif" width="6" height="2"/></td>
                            <td width="50%" align="left" class="intBorderCBgInt1"><table width="80%"  border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td width="15" height="7"><img runat="server" src="~/images/spacer.gif" width="15" height="7"/> </td>
                                  <td width="97%" class="textGeneralGray11">Month</td>
                                </tr>
                            </table></td>
                            <td width="1" class="sepTit1"><img runat="server" src="~/images/spacer.gif" width="1" height="8"/></td>
                            <td width="50%" align="left" class="intBorderCBgInt1"><table width="80%"  border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td width="15" height="7"><img runat="server" src="~/images/spacer.gif" width="15" height="7"/> </td>
                                  <td width="97%" class="textGeneralGray11">Items Viewed</td>
                                </tr>
                            </table></td>
                            <td class="intBorderCBorderRight">&nbsp;</td>
                          </tr>
                          <tr>
                            <td class="intBorderCBottomLeft"><img runat="server" src="~/images/spacer.gif" width="7" height="7"/></td>
                            <td class="intBorderCBottom"><img runat="server" src="~/images/spacer.gif" width="7" height="7"/></td>
                            <td width="1" class="intBorderCBottom"><img runat="server" src="~/images/spacer.gif" width="1" height="7"/></td>
                            <td class="intBorderCBottom"><img runat="server" src="~/images/spacer.gif" width="7" height="7"/></td>
                            <td class="intBorderCBottomRight"><img runat="server" src="~/images/spacer.gif" width="7" height="7"/></td>
                          </tr>                          
                         
							<asp:Repeater EnableViewState="False" runat="server" id="rptItemsSold">  
								<ItemTemplate>
									<tr align="left" class="intColor1">
										<td class="intBorderC1BorderLeft1">&nbsp;</td>
										<td class="intColor1"><table width="80%"  border="0" cellspacing="0" cellpadding="0">
											<tr>
											<td width="15" height="7"><img runat="server" src="~/images/spacer.gif" width="15" height="7"/></td>
											<td width="97%" >
												<a href='<%#GetItemsSoldLink (Convert.ToDateTime (DataBinder.Eval (Container.DataItem, "purchase_month")))%>'><%# DataBinder.Eval (Container.DataItem, "month")%>, <%# DataBinder.Eval (Container.DataItem, "year")%></a>
											</td>
											</tr>
										</table></td>
										<td width="1" class="sepInt1"><img runat="server" src="~/images/spacer.gif" width="1" height="7"/></td>
										<td class="intColor1">
										<table width="80%"  border="0" cellspacing="0" cellpadding="0">
											<tr>
											<td width="15" height="7"><img runat="server" src="~/images/spacer.gif" width="15" height="7"/></td>
											<td width="97%" class="textGeneralGray11"><%# DataBinder.Eval (Container.DataItem, "items_sold")%></td>
											</tr>
										</table>
										</td>
										<td class="intBorderC1BorderRight1">&nbsp;</td>
									</tr>
								</ItemTemplate>
								<AlternatingItemTemplate>
									<tr align="left" class="intColor2">
										<td class="intBorderC1BorderLeft1">&nbsp;</td>
										<td class="intColor2">
											<table width="80%"  border="0" cellspacing="0" cellpadding="0">
												<tr>
												<td width="15" height="7"><img runat="server" src="~/images/spacer.gif" width="15" height="7"/></td>
												<td width="97%" >
													<a href='<%#GetItemsSoldLink (Convert.ToDateTime (DataBinder.Eval (Container.DataItem, "purchase_month")))%>'><%# DataBinder.Eval (Container.DataItem, "month")%>, <%# DataBinder.Eval (Container.DataItem, "year")%></a>
												</td>
												</tr>
											</table>
										</td>
										<td width="1" class="sepInt1"><img runat="server" src="~/images/spacer.gif" width="1" height="7"/></td>
										<td class="intColor2">
										<table width="80%"  border="0" cellspacing="0" cellpadding="0">
											<tr>
											<td width="15" height="7"><img runat="server" src="~/images/spacer.gif" width="15" height="7"/></td>
											<td width="97%" class="textGeneralGray11"><%# DataBinder.Eval (Container.DataItem, "items_sold")%></td>
											</tr>
										</table>
										</td>
										<td class="intBorderC1BorderRight1">&nbsp;</td>
									</tr>
								</AlternatingItemTemplate>
							</asp:Repeater>
                          
                          
                          <tr align="left" >
                            <td class="intBorderC1BottomLeft"><img runat="server" src="~/images/spacer.gif" width="7" height="7"/></td>
                            <td class="intBorderC1Bottom"><img runat="server" src="~/images/spacer.gif" width="7" height="7"/></td>
                            <td width="1" class="intBorderC1Bottom"><img runat="server" src="~/images/spacer.gif" width="1" height="7"/></td>
                            <td class="intBorderC1Bottom"><img runat="server" src="~/images/spacer.gif" width="7" height="7"/></td>
                            <td class="intBorderC1BottomRight"><img runat="server" src="~/images/spacer.gif" width="7" height="7"/></td>
                          </tr>
                        </table></td>
                        <td width="6" height="6"><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
                      </tr>
                      <tr>
                        <td width="6" height="6"><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
                        <td width="6" height="6"><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
                        <td width="6" height="6"><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
                      </tr>
                    </table></td>
                    <td class="boxInsideRight"><img runat="server" src="~/images/spacer.gif" width="1" height="4"/></td>
                  </tr>
                  <tr>
                    <td class="boxInsideBottomLeft" ><img runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
                    <td class="boxInsideBottom2"><img runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
                    <td class="boxInsideBottomRight"><img runat="server" src="~/images/spacer.gif" width="4" height="4" /></td>
                  </tr>
              </table></td>
            <td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1"/></td>
          </tr>
          <tr>
            <td class="frBottomLeft"></td>
            <td class="frBottom"></td>
            <td class="frBottomRight"></td>
          </tr>
          <tr>
            <td><img runat="server" src="~/images/spacer.gif" width="1" height="14"/></td>
          </tr>
        </table></td>
        </tr>      
    </table></td>
  </tr>
</table>
