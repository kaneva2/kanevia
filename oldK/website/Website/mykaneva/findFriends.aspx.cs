///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
    public partial class findFriends : NoBorderPage
    {
        protected void Page_Load (object sender, EventArgs e)
        {
            if (!Request.IsAuthenticated)
            {
                Response.Redirect (this.GetLoginURL ());
            }

            if (!IsPostBack)
            {
                if (KanevaWebGlobals.CurrentUser.FacebookSettings.FacebookUserId > 0)
                {
                    string ids = "";

                    // Get List of Facebook friends from Facebook
                    bool isError = false;
                    string errMsg = "";
                    FacebookFriendList friends = GetSocialFacade.GetCurrentUsersFriends (KanevaWebGlobals.CurrentUser.FacebookSettings.AccessToken, out isError, out errMsg);

                    if (!isError)
                    {
                        // Check to see if any of these Facebook Id's are connected to Kaneva accounts
                        if (friends.data.Count > 0)
                        {
                            foreach (FacebookUser fbuser in friends.data)
                            {
                                ids += fbuser.Id.ToString () + ",";
                            }
                        }
                        else
                        {
                            // User has no Facebook friends.  Not likely to happen but just in case
                            // Let fall through to redirect to invite friends page
                        }

                        if (ids.Length > 0)
                        {
                            // Remove the last comma
                            ids = ids.Remove (ids.Length - 1, 1);

                            // Get any Facebook friends that have accounts connected to their Facebook account 
                            // and the current user is not already friends with on Kaneva
                            PagedList<User> fbFriends = GetUserFacade.GetUsersByFacebookUserId (KanevaWebGlobals.CurrentUser.UserId, ids, "", 1, 5000);

                            if (fbFriends.Count > 0)
                            {
                                // Show the list of friends that are already on Kaneva and invite the user to add them as friends
                                dlFBFriendsOnKaneva.DataSource = fbFriends;
                                dlFBFriendsOnKaneva.DataBind ();
                                divKanevaFriends.Visible = true;

                                h1Title.InnerHtml = "Your Friends Are Already Here!";
                                pDesc.InnerHtml = "&nbsp;";
                                btnSendAll.Visible = true;

                                // Store list of user ids in ViewState so we can use if they want to send to all
                                IList<Int32> uids = new List<Int32> ();
                                foreach (User user in fbFriends)
                                {
                                    uids.Add (user.UserId);
                                }
                                this.UIds = uids;
                                return;
                            }
                            else
                            {
                                // No Facebook friends connected on Kaneva so show Invite Facebook Friends page

                                // *** TEST CODE USED TO SET FRIENDS LIST TO 3 FRIENDS ONLY *** 
                                //FacebookFriendList ffl = new FacebookFriendList ();
                                //if (friends.data.Count > 3)
                                //{
                                //    friends.data.RemoveRange (3, friends.data.Count - 3);
                                //}
                                
                                dlInviteFromFB.DataSource = friends.data;
                                dlInviteFromFB.DataBind ();
                                divFBFriends.Visible = true;

                                h1Title.InnerHtml = "Kaneva is better with Friends";
                                pDesc.InnerHtml = "Add your Facebook friends to get started.";

                                return;
                            }
                        }
                    }
                }            

                // No Facebook friends, go to download page
                Response.Clear ();
                Response.Redirect (ResolveUrl ("~/register/kaneva/registerCompleted.aspx"));
                Response.End ();
            }
        }

        public bool AddFriend (int userId, int friendId)
        {
            // Can't friend yourself
            if (userId.Equals (friendId))
            {
                return false;
            }
            // Add them as a friend
            int ret = GetUserFacade.InsertFriendRequest (userId, friendId,
                Page.Request.CurrentExecutionFilePath, Global.RequestRabbitMQChannel());
            if (ret.Equals (0))	// already a friend
            { }
            else if (ret.Equals (2))  // pending friend request
            { }
            else if (ret.Equals (1))  // success
            {
                // send request email
                MailUtilityWeb.SendFriendRequestEmail (userId, friendId);
                return true;
            }
            return false;
        }

        protected void btnSendAll_Click (Object sender, EventArgs e)
        {
            foreach (int id in UIds)
            {
                AddFriend (KanevaWebGlobals.CurrentUser.UserId, id);
            }

            foreach (DataListItem item in dlFBFriendsOnKaneva.Items)
            {
                ImageButton ib = (ImageButton)item.FindControl ("btnAddFriend");
                if (ib != null)
                {
                    ib.ImageUrl = ResolveUrl ("~/images/facebook/requested.png");
                    ib.Enabled = false;
                }
            }
        }

        protected void btnNext_Click (Object sender, EventArgs e)
        {
            Response.Redirect ("~/register/kaneva/registerCompleted.aspx");   
        }

        protected void fbFriendsKaneva_ItemDataBound (Object Sender, DataListItemEventArgs e)
        {
            if ((e.Item.ItemType == ListItemType.Item) || (e.Item.ItemType == ListItemType.AlternatingItem))
            {
                ImageButton btnAddFriend = ((ImageButton) e.Item.FindControl ("btnAddFriend"));

                User user = (User) e.Item.DataItem;

                btnAddFriend.Attributes.Add ("onclick", "AddFriend(" + user.UserId.ToString () + ",this.id);return false;");
            }
        }

        private IList<Int32> UIds
        {
            get {
                try
                {
                    if (ViewState["UIds"] != null)
                    {
                        return (IList<Int32>) ViewState["UIds"];
                    }
                }
                catch { }
                return new List<Int32> ();
            }
            set { ViewState["UIds"] = value; }
        }

        public string CurrentUser_UserId ()
        {
            return KanevaWebGlobals.CurrentUser.UserId.ToString ();
        }
    }
}