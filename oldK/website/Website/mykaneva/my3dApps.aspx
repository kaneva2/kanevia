<%@ Page Language="C#" MasterPageFile="~/masterpages/GenericPageTemplate.Master" ValidateRequest="False" AutoEventWireup="true" CodeBehind="my3dApps.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.mykaneva.my3dApps" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../usercontrols/Pager.ascx" %>

<asp:Content ID="cnt_ProfileHead" runat="server" contentplaceholderid="cph_HeadData" >
    <meta name="title" content="Kaneva. The Online Community and Social Network." />
    <meta name="description" content="Kaneva is the first to combine social networking and a free 3D virtual world. It's a whole new way to connect with friends. Have fun in a vibrant virtual world full of people, cool places and fun games. Create your avatar and get a free 3D home today!"/>
    <meta name="keywords" content="free virtual world, virtual world, virtual worlds, 3D world, online avatar community, mmo game, virtual reality, caneva, kaniva" />
    
    <NOSCRIPT><META HTTP-EQUIV="refresh" CONTENT="1" URL="noJavascript.aspx" /></NOSCRIPT>    
    <script language="JavaScript" type="text/javascript" src="../jscript/base/Base-min.js"></script>
    <script language="JavaScript" type="text/javascript" src="../jscript/prototype-1.6.1-min.js"></script>
	<link href="../css/myKaneva/my3dApps.css" type="text/css" rel="stylesheet" />
    

</asp:Content>

<asp:Content ID="mainContent" runat="server" contentplaceholderid="cph_Body">
	
	<div id="contentBody">
	
		<div id="pageHeader">
			<div class="pageTitle">My Worlds</div>
			
			<div class="downloadBtnContainer">	
				
				<div id="c3dapp">Go To: <a id="A4" runat="server" href="~/community/channel.kaneva?3dapps=">All Worlds</a> | <a id="aCreate3DApp" runat="server" href="~/community/StartWorld.aspx"><span>Create Your World</span></a></div>
			    <div id="sort">Sort: 
					<asp:dropdownlist id="ddlSortBy" runat="server" autopostback="true" onselectedindexchanged="ddlSortBy_IndexChanged">
						<asp:listitem value="0">Last Visited</asp:listitem>
						<asp:listitem value="1">Date Created</asp:listitem>
						<asp:listitem value="2">Alphabetical</asp:listitem>
						<asp:listitem value="3">Worlds You Own</asp:listitem>
						<asp:listitem value="4">World Credit Balance</asp:listitem>
					</asp:dropdownlist>
				</div>
			</div>
		</div>
	
		<asp:Repeater runat="server" id="rptApps" OnItemDataBound="rptApps_ItemDataBound" OnItemCommand="rptApps_ItemCommand">	  
			<ItemTemplate>
					<div class="resultsRow">
						<div class="desc">
							<!--image holder-->
							<div style="height:100%;width:75px;float:left;">
							<div class="framesize-small">
								<div id="restricted" class="restricted" runat="server" visible='<%# (DataBinder.Eval(Container.DataItem, "IsAdult").Equals ("Y")) %>'></div>
								<div id="over21" class="over21" runat="server" visible='<%# (DataBinder.Eval(Container.DataItem, "Over21Required").Equals ("Y")) %>'></div>
								<div class="frame">																										
									<a href='<%# GetBroadcastChannelUrl (DataBinder.Eval (Container.DataItem, "NameNoSpaces").ToString ())%>'>
										<img runat="server" src='<%# GetBroadcastChannelImageURL (DataBinder.Eval(Container.DataItem, "ThumbnailSmallPath").ToString (), "sm") %>' border="0" id="Img5"/>
									</a>																										
								</div>
							</div>
							<div class="private" runat="server" id="communityAccess"></div>
							</div>
							<!--end image holder-->
																							
							<!-- Info -->
							<div class="info">
								<div class="name"><a class="gamename" href='<%# GetBroadcastChannelUrl (DataBinder.Eval (Container.DataItem, "NameNoSpaces").ToString ())%>'><%# TruncateWithEllipsis (DataBinder.Eval (Container.DataItem, "Name").ToString (), 40)%></a><a class="balance" id="aCreditBalance" runat="server" visible="false"><asp:literal id="litCreditBalance" runat="server"></asp:literal></a></div>
								<div class="owner"><span>Owner:</span> <a class="dateStamp" href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "CreatorUsername").ToString ())%>><%# DataBinder.Eval(Container.DataItem, "CreatorUsername") %></a></div> 
								<div class="access"><span>Access:</span> <%# GetChannelStatus (DataBinder.Eval (Container.DataItem, "IsPublic")) %></div>
								<div class="desc"><%# DataBinder.Eval(Container.DataItem, "Description") %></div>
								<div class="tags"></div>	
							</div>
						</div>
						<div class="actions">
							<!-- Actions -->
							<div class="playNowContainer"><asp:linkbutton class="buttonFixed" id="btnMeet3D" runat="server" CausesValidation="False"></asp:linkbutton></div> 
							<a href='<%# GetBroadcastChannelUrl (DataBinder.Eval (Container.DataItem, "NameNoSpaces").ToString ())%>'>View Profile</a><br />						
							<asp:HyperLink ID="manageLink" runat="server" Visible="false">Manage World</asp:HyperLink><br/>
                            <asp:imagebutton id="btnDelete" Visible="false" commandname="cmdDeleteChannel" alternatetext="Delete Community" imageurl="~/images/button_delete_lg.gif" runat="server" border="0" />
                            <input type="hidden" runat="server" id="hidChannelId" value='<%#DataBinder.Eval(Container.DataItem, "CommunityId")%>' name="hidChannelId">
						</div>	
					</div>
			</ItemTemplate>
		</asp:Repeater>
	
		<!-- Pagination -->
		<div class="pagerContainer">
			<div><asp:label runat="server" id="lblSearch" cssclass="insideTextNoBold"/></div>
			<div><Kaneva:Pager runat="server" id="pgTop"/></div>  
		</div>
	</div>
	<style>.PageContainer{padding-top:95px;}</style>
</asp:Content>