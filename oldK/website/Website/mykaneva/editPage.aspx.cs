///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using System.Web.Security;
using System.Security.Cryptography;

using System.Text;

using Kaneva.RegisterScriptsClasses;
using KlausEnt.KEP.Kaneva.channel;
using KlausEnt.KEP.Kaneva.framework.widgets;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for editPage.
	/// </summary>
	public class editPage : MainTemplatePage
	{
		protected editPage () 
		{
			Title = "Edit";
		}

		#region Variables
		
		protected int				_pageId;
		protected int				_channelId;
		private bool				_isPersonal;

        private const string RESTRICTED_ERR_MSG = "Attention\n\nThe Page Name contains a restricted word (one that is considered " +
                        " objectionable to other members). All materials published through the Kaneva site must comply " +
                        " with Kaneva�s Terms of Service.\n\nTo continue, you�ll need to change the word.";
		#endregion

		#region Page Detail Variables

		protected HtmlGenericControl lbPageGroup, lblFriendMemberOnly, lblGroupName;

		protected TextBox				txtPageName;
		protected DropDownList			ddlPageAccess;
		protected HtmlInputRadioButton	rbPublic, rbPrivate, rbFriendsOnly, rbMembersOnly;

		#endregion

		#region Manage Pages Variables

		protected DataList			dlPages;
		protected Label				lblNumberOfPages;
		
		protected HtmlInputHidden	hfChangePageID;
		protected HtmlInputHidden	hfDefaultPageID;
		protected HtmlInputHidden	hfDeletedPageID;
		protected HtmlInputHidden	hfPositions;
		protected Button			btnSaveManagePages;
		protected Button			hbChangePage;

		protected Button			btnCreate;
		protected DropDownList		ddlNewAccess;
		protected TextBox			tbNewName;
		protected DropDownList		ddlNewGroup;
		protected HtmlSelect		ddlActionList;

		private int					_channelOwnerId;

		#endregion

		#region Page Nav Variables

		protected string m_SubTab = "";

		protected HtmlTableCell tdView, tdEdit, tdSettings, tdAdmin, tdFriendMember, tdMedia, tdBlogs;
		protected Label lblChannelName, lblNoData;

		protected LinkButton		lbView;
		protected LinkButton		lbEdit;
		protected LinkButton		lbSettings;
		protected LinkButton		lbAdmin;
		protected LinkButton		lbBlogs;
		protected LinkButton		lbFriendMember;
		protected LinkButton		lbMedia;

		#endregion

		#region Edit Page Module Variables

        protected Literal           litColumnSliderJs;
        protected Repeater			rpModuleGroup;
		protected Button			btnSaveBottom;
		protected Button			btnSaveTop;
		protected HtmlInputHidden	hfSaveString;
		protected HtmlInputHidden	hfInitialLayout;
		protected HtmlInputHidden	hfLeftColumnWidth;
		protected HtmlInputHidden	hfInitialColumnWidth;

		protected RadioButton		rbLeftLayout;
		protected RadioButton		rbRightLayout;
		protected HtmlTableCell		tdContentLeft;
		protected HtmlTableCell		tdContentRight;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divWidgetBaseTemplate;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divWidgetTemplate;
		protected System.Web.UI.HtmlControls.HtmlTableCell tdContentDivider;
		protected bool				_isDefault;

		#endregion

		protected ImageButton		btnDelete;
		protected ImageButton		btnAdd;
		private int _contestId = -2;
		
		private void Page_Load(object sender, System.EventArgs e)
		{
			if (Request.IsAuthenticated)
			{
				if (GetRequestParams ())
				{
					//check if user is allowed to edit this channel
					if(!IsUserAllowedToEdit ())
					{
						RedirectToHomePage ();
					}
				}
				else
				{
					//invalid request params
					//if the user logged in, send him to his home page
					//if not, redirect to login page
					RedirectToHomePage ();
				}
			}
			else
			{
				Response.Redirect (this.GetLoginURL ());
			}


			//setup header nav bar
			if(_channelId == this.GetPersonalChannelId())
			{
				//user's own channel
				HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.PROFILE;
				Title = "Edit Profile";
			}
			else
			{
				//navs go to home-my channels for all channels if the user is an admin
				HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.CHANNELS;
				Title = "Edit Community";
			}

			HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
			
			HeaderNav.MyChannelsNav.PageId = _pageId;
			HeaderNav.MyChannelsNav.ChannelId = _channelId;
			HeaderNav.MyChannelsNav.ActiveTab = NavMyChannels.TAB.LAYOUT;
			
			HeaderNav.SetNavVisible(HeaderNav.MyKanevaNav,2);
			HeaderNav.SetNavVisible(HeaderNav.MyChannelsNav);

			
			DataRow drChannel = CommunityUtility.GetCommunity(_channelId);
			_channelOwnerId = Convert.ToInt32 (drChannel["creator_id"]);
			_isPersonal = CommunityUtility.IsChannelPersonal (drChannel);

			if ( _isPersonal )
				_isDefault = _pageId == PageUtility.GetUserDefaultPageId( GetUserId() );
			else
				_isDefault = _pageId == PageUtility.GetChannelDefaultPageId( _channelId );

            // if this is the default page do not allow column resizing
            if (_isDefault && _isPersonal)
            {
                litColumnSliderJs.Text = "dragEnable = false;";
                tdContentDivider.Attributes.Remove("onmouseover");
                txtPageName.Enabled = false;
            }
            

			InitWidgetEditing ();

            if (!IsPostBack)
            {
                DefaultPageManagePages ();
                DefaultPageDetail ();

                SetPageLayout ();
                DefaultPageWidgets ();
                InitPage ();
            }
        
            //set focus to page name
            SetFocus(txtPageName);
		}

		private bool GetRequestParams()
		{
			bool retVal = true;
			try
			{
				if(Request ["pageId"] == null || Request ["pageId"] == ""
					|| Request ["pageId"] == "0")
				{
					if(Request ["communityId"] != null)
					{
						//get channelId and find the default page
						_channelId = Convert.ToInt32 (Request ["communityId"]);
						if(CommunityUtility.IsCommunityValid(_channelId))
						{
							_pageId = PageUtility.GetChannelDefaultPageId(_channelId);
						}
						else
						{
							retVal = false;
						}
					}
					else
					{
						//no channel id passed, get personal channelId if user logged in
						if(Request.IsAuthenticated)
						{
							_channelId = GetPersonalChannelId ();
							_pageId = PageUtility.GetChannelDefaultPageId(_channelId);
						}
						else
						{
							retVal = false;
						}
					}
				}
                //added to allow for the new add a page coming from channel/channelPage
				else
				{
                    if (Request["pageId"] == "-1")
                    {
                        _channelId = Convert.ToInt32(Request["communityId"]);
                        CreateNewPage();
                    }
                    else
                    {
                        _pageId = Convert.ToInt32(Request["pageId"]);
                        DataRow drChannel = PageUtility.GetChannelByPageId(_pageId);
                        _channelId = Convert.ToInt32(drChannel["community_id"]);
                    }
				}

				// setup for page nav
				SetChannelProperties();
			}
			catch(Exception)
			{
				//invalid numbers
				retVal = false;
			}
			return retVal;
		}

		/// <summary>
		/// returns true if current user is allowed to edit this channel
		/// </summary>
		/// <returns></returns>
		private bool IsUserAllowedToEdit()
		{
			return IsAdministrator() || CommunityUtility.IsCommunityModerator(this._channelId, GetUserId());
		}

		#region Page Detail

		private void DefaultPageDetail()
		{
			DataRow drLayoutPage = PageUtility.GetLayoutPage( _pageId );
			if ( drLayoutPage != null )
			{
				txtPageName.Text = drLayoutPage["name"].ToString();
				btnDelete.Enabled = !drLayoutPage["home_page"].ToString().Equals("1"); //disable deletion of home page

				//
				// get 'accesses' needed to populate ddlAccess
				//
				ddlPageAccess.Items.Clear();
				ddlPageAccess.Items.Add(new ListItem("Public", ((int) Constants.ePAGE_ACCESS.PUBLIC).ToString()));
				if(_isPersonal)
				{
					ddlPageAccess.Items.Add(new ListItem("Friends Only", ((int) Constants.ePAGE_ACCESS.FRIENDS).ToString()));
					
					rbMembersOnly.Visible = false;
					rbFriendsOnly.Visible = true;
					lblFriendMemberOnly.InnerText = "Friends Only";
					lblGroupName.InnerText = "Friend Group:";
				}
				else
				{
					ddlPageAccess.Items.Add(new ListItem("Members Only", ((int) Constants.ePAGE_ACCESS.MEMBERS).ToString()));

					rbMembersOnly.Visible = true;
					rbFriendsOnly.Visible = false;
					lblFriendMemberOnly.InnerText = "Members Only";
					lblGroupName.InnerText = "Member Group:";
				}
				ddlPageAccess.Items.Add(new ListItem("Private", ((int) Constants.ePAGE_ACCESS.PRIVATE).ToString()));
				ddlPageAccess.Attributes.Add( "onclick", "page_detail.ChangePermission()" );

				
				int access_id = Convert.ToInt32( drLayoutPage["access_id"].ToString() );

				bool disabled = (access_id == (int) Constants.ePAGE_ACCESS.PUBLIC) || (access_id == (int) Constants.ePAGE_ACCESS.PRIVATE);
				lbPageGroup.Disabled = disabled;

				try
				{
					ddlPageAccess.SelectedValue = access_id.ToString();
					
					if (access_id == (int) Constants.ePAGE_ACCESS.PRIVATE)
						rbPrivate.Checked = true;
					else if (access_id == (int) Constants.ePAGE_ACCESS.FRIENDS)
						rbFriendsOnly.Checked = true;
					else if (access_id == (int) Constants.ePAGE_ACCESS.MEMBERS)
						rbMembersOnly.Checked = true;
					else
						rbPublic.Checked = true;

				}
				catch (Exception)
				{
				}
				
				string strGroupId = drLayoutPage["group_id"] == DBNull.Value ? "0" :
					drLayoutPage["group_id"].ToString();
				try
				{
					ddlNewGroup.SelectedValue = strGroupId;
				}
				catch (Exception)
				{
				}
			}
		}

		#endregion

		// Mange pages code
		#region Manage pages

        /// <summary>
        /// creates new channel page and redirects to it for editing
        /// 
        /// </summary>
        private void CreateNewPage()
        {
            //create a new page using default settings
            int pageId = PageUtility.AddLayoutPage(false, _channelId, "New Page",
                0,
                (int)Constants.ePAGE_ACCESS.PUBLIC, 0);

            if (_channelId == this.GetPersonalChannelId())
            {
                DataRow drTitle = WidgetUtility.GetDefaultModuleTitle(_channelId);
                if (drTitle != null)
                {
                    //copy the title widget from the home page if there is one
                    int module_page_id = WidgetUtility.InsertLayoutModuleTitle(drTitle["text"].ToString(),
                        drTitle["show_menu"].ToString().Equals("1"), drTitle["banner_path"].ToString());

                    if (module_page_id != -1)
                        PageUtility.AddLayoutPageModule(module_page_id, pageId, (int)Constants.eMODULE_TYPE.TITLE_TEXT
                            , (int)Constants.eMODULE_ZONE.HEADER, 1);
                }
            }
            else
            {
                DataRow drCCPanel = WidgetUtility.GetDefaultModuleChannelControlPanel(_channelId);
                if (drCCPanel != null)
                {
                    //copy the title widget from the home page if there is one
                    int module_page_id = WidgetUtility.InsertLayoutModuleChannelControlPanel(drCCPanel["title"].ToString(),
                        drCCPanel["show_menu"].ToString().Equals("1"), drCCPanel["banner_path"].ToString());

                    if (module_page_id != -1)
                        PageUtility.AddLayoutPageModule(module_page_id, pageId, (int)Constants.eMODULE_TYPE.CHANNEL_CONTROL_PANEL
                            , (int)Constants.eMODULE_ZONE.HEADER, 1);
                }
            }

            //redirect to the new page
            Response.Redirect(ResolveUrl("~/mykaneva/editPage.aspx?communityId=" + this._channelId + "&pageId=" + pageId));
        }

		/// <summary>
		/// set default module groups and modules
		/// </summary>
		private void DefaultPageManagePages ()
		{
			DataTable dtLayoutPages = PageUtility.GetLayoutPages( _channelId );

			//this is kind of hacky. we are using the same control to store options that come from two diff 
			//sources
			if(_isPersonal)
			{
				DataTable dtGroups = UsersUtility.GetFriendGroups( _channelOwnerId, "", "name", 1, Int32.MaxValue );
				ddlNewGroup.DataSource		= dtGroups;
				ddlNewGroup.DataValueField	= "friend_group_id";
				ddlNewGroup.DataTextField	= "name";
				ddlNewGroup.DataBind();
			}
			else
			{
				DataTable dtGroups = CommunityUtility.GetMemberGroups( _channelId, "", "name", 1, Int32.MaxValue );
				ddlNewGroup.DataSource		= dtGroups;
				ddlNewGroup.DataValueField	= "id";
				ddlNewGroup.DataTextField	= "name";
				ddlNewGroup.DataBind();
			}

			// populate NewGroup ddl

			ddlNewGroup.Items.Insert(0, new ListItem( "All", "0" ) );
			ddlNewGroup.SelectedIndex	= 0;

			// get 'accesses' needed to populate ddlAccess

			// populate NewAccess ddl
			ddlNewAccess.Items.Clear();
			ddlNewAccess.Items.Add(new ListItem("Public", ((int) Constants.ePAGE_ACCESS.PUBLIC).ToString()));
			if(_isPersonal)
			{
				ddlNewAccess.Items.Add(new ListItem("Friends Only", ((int) Constants.ePAGE_ACCESS.FRIENDS).ToString()));
			}
			else
			{
				ddlNewAccess.Items.Add(new ListItem("Members Only", ((int) Constants.ePAGE_ACCESS.MEMBERS).ToString()));
			}
			ddlNewAccess.Items.Add(new ListItem("Private", ((int) Constants.ePAGE_ACCESS.PRIVATE).ToString()));
			ddlNewAccess.SelectedIndex	= 0;
			ddlNewAccess.Attributes.Add( "onclick", "PManager.ChangePermissionNew()" );

			dlPages.DataSource = dtLayoutPages;
			dlPages.DataBind();

			lblNumberOfPages.Text = dlPages.Items.Count.ToString();

			ddlActionList.Attributes.Add( "onchange", "PManager.DoAction(this.value);" 

				+ " PManager.SavePositions('" + hfPositions.ClientID + "'); "

				+ " widget_page.GetSaveString('hfSaveString'); "

                + ClientScript.GetPostBackEventReference(btnSaveManagePages, "") + ";return false;");
			
			
		}

		private void NewPageManager()
		{
			StringBuilder msg = new StringBuilder();

			msg.AppendFormat( "var PManager = new PageManager('{0}', '{1}',\n", hfDefaultPageID.ClientID, hfDeletedPageID.ClientID );
			msg.AppendFormat( "\t'{0}', 'cbSelectAll', 'ddlActionList')\n\n", lblNumberOfPages.ClientID );

			msg.AppendFormat( "document.forms[0].onsubmit = function(){0}PManager.SavePositions('{1}'){2};\n", "{", hfPositions.ClientID, "}" );

			RegisterScripts.RegisterStartupScript(Page, "NewPageManager", msg.ToString(), true, 0 );
		}

		private void SaveAll()
		{
			int i;

			// pull out the order of the pages (page_ids)
			string[] pageids = hfPositions.Value.TrimEnd( '_' ).Split( '_' );
			
			for (i=0; i<pageids.Length; i++)
			{
				PageUtility.UpdateLayoutPageSequenceAndDefault( Convert.ToInt32( pageids[i] ), i, 
					hfDefaultPageID.Value == pageids[i] );
			}

			// pull out the pages to be removed (page_ids)
			pageids = hfDeletedPageID.Value.TrimEnd( '_' ).Split( '_' );

			// only try to make DB calls if there are pages to remove
			if ( pageids[0] != "")
			{
				for (i=0; i<pageids.Length; i++)
				{
					PageUtility.DeleteLayoutPage( Convert.ToInt32( pageids[i] ) );
				}
			}
		}

		/// <summary>
		/// Return the Change Page javascript
		/// </summary>
		/// <returns></returns>
		public string GetChangePageScript (int pageId)
		{
			return "javascript:ChangePage (" + pageId + ");";
		}

		protected void dlPages_ItemDataBound(object sender, DataListItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
			{
				//				HtmlTable tblPage		= (HtmlTable)e.Item.FindControl("tblPage");
				//				HtmlTable tblPageData	= (HtmlTable)e.Item.FindControl("tblPageData");
				//				HtmlTableRow trPageRow	= (HtmlTableRow)e.Item.FindControl("trPageRow");
				//				if (tblPage != null && tblPageData != null && trPageRow != null)
				//				{
				DataRowView drv		= (DataRowView) e.Item.DataItem;

				int pageCount = ((DataTable)(dlPages.DataSource)).Rows.Count;

				// Select the row the are currently editing
				if (_pageId.Equals (Convert.ToInt32 (drv["page_id"])))
				{
					HtmlImage pageIcon = (HtmlImage)e.Item.FindControl("pageIcon");
					pageIcon.Src = "../images/paper_green_ico.gif";

					//TODO
					// Highlight the row if there is more than one
					//						if (pageCount > 1)
					//						{
					//							trPageRow.Attributes.Add ("class", "lineItemSelected");
					//						}
				}

				// register NewPageManager script
				NewPageManager();

				// register AddPage script
				RegisterScripts.RegisterStartupScript(Page, "PManager.AddPage_" + e.Item.ClientID, 
					"PManager.AddPage('" + drv["page_id"].ToString() + "', '" + e.Item.ClientID + "')", true, 1 );

				// set default page info
				if ( drv["home_page"].ToString() == "1" )
				{
					hfDefaultPageID.Value = drv["page_id"].ToString();
				}

				Label lblTitle = (Label)e.Item.FindControl("lblTitle");
				if (lblTitle != null)
				{
					// This decode and encode is needed to handle the case where a user enters some character like >.  This
					// gets encoded to &lt; so this counts as 4 character spaces instead of 1 and the truncate does not 
					// work correctly. IE. <only> will display as &lt;only&l on the screen.
					lblTitle.Text = Server.HtmlEncode(TruncateWithEllipsis (Server.HtmlDecode(drv ["name"].ToString()), 10));
				}

				int group_id = drv["group_id"] != DBNull.Value ? Convert.ToInt32( drv["group_id"].ToString() ) : 0;
				// check that the group still exists
				if(ddlNewGroup.Items.FindByValue(group_id.ToString()) == null)
				{
					group_id = 0;
				}
			}
		}

		/// <summary>
		/// Change Pages
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void Change_Page(Object sender, EventArgs e)
		{
			// save layout
            if (hfInitialLayout.Value != hfSaveString.Value || hfInitialColumnWidth.Value != hfLeftColumnWidth.Value)
            {
                // Check to make sure user did not enter any "potty mouth" words
                if (KanevaWebGlobals.isTextRestricted (txtPageName.Text, Constants.eRESTRICTION_TYPE.POTTY_MOUTH))
                {
                    MagicAjax.AjaxCallHelper.WriteAlert (RESTRICTED_ERR_MSG);
                    return;
                }

                SavePage (hfSaveString.Value, hfLeftColumnWidth.Value);
            }

			Response.Redirect( "editPage.aspx?communityId=" + _channelId + 
				"&pageId=" + hfChangePageID.Value );
		}

		// save the layout -- remove any pages marked for deletion
		// and update the sequence of pages along with the default page
		protected void btnSaveManagePages_Click(object sender, EventArgs e)
		{
			SaveAll();

			// also save layout
            if (hfInitialLayout.Value != hfSaveString.Value || hfInitialColumnWidth.Value != hfLeftColumnWidth.Value)
            {
                // Check to make sure user did not enter any "potty mouth" words
                if (KanevaWebGlobals.isTextRestricted (txtPageName.Text, Constants.eRESTRICTION_TYPE.POTTY_MOUTH))
                {
                    MagicAjax.AjaxCallHelper.WriteAlert (RESTRICTED_ERR_MSG);
                    return;
                }

                SavePage (hfSaveString.Value, hfLeftColumnWidth.Value);
            }

			// Navigate back to the correct page
			DataRow drChannel = CommunityUtility.GetCommunity (_channelId);
			if(_isPersonal)
			{
				Response.Redirect( KanevaGlobals.GetPersonalChannelUrl ( drChannel ["name_no_spaces"].ToString ()) + "?pageId=" + _pageId );
			}
			else
			{
				Response.Redirect( KanevaGlobals.GetBroadcastChannelUrl (drChannel ["name_no_spaces"].ToString ()) + "?pageId=" + _pageId );
			}
		}

		// create a new page (and save the current settings of the others)
		protected void btnCreate_Click(object sender, EventArgs e)
		{
			SaveAll();

			// also save layout
            if (hfInitialLayout.Value != hfSaveString.Value || hfInitialColumnWidth.Value != hfLeftColumnWidth.Value)
            {
                // Check to make sure user did not enter any "potty mouth" words
                if (KanevaWebGlobals.isTextRestricted (txtPageName.Text, Constants.eRESTRICTION_TYPE.POTTY_MOUTH))
                {
                    MagicAjax.AjaxCallHelper.WriteAlert (RESTRICTED_ERR_MSG);
                    return;
                }

                SavePage (hfSaveString.Value, hfLeftColumnWidth.Value);
            }

			int pageId = PageUtility.AddLayoutPage (false, _channelId, tbNewName.Text, Convert.ToInt32( ddlNewGroup.SelectedValue ), 
				Convert.ToInt32( ddlNewAccess.SelectedValue ), 0);

			DataRow drTitle = WidgetUtility.GetDefaultModuleTitle(_channelId);
			if(drTitle != null)
			{
				//copy the title widget from the home page if there is one
				int module_page_id = WidgetUtility.InsertLayoutModuleTitle(drTitle["text"].ToString(),
					drTitle["show_menu"].ToString().Equals("1"),drTitle["banner_path"].ToString ());

				if (module_page_id != -1)
					PageUtility.AddLayoutPageModule( module_page_id, pageId, (int) Constants.eMODULE_TYPE.TITLE_TEXT
						, (int) Constants.eMODULE_ZONE.HEADER, 1 );
			}

			// stay on same page 
			Response.Redirect (Request.Url.AbsoluteUri);
		}

		private void btnDelete_Click(object sender, ImageClickEventArgs e)
		{
			// Make sure this is a page that allows deletion
            if (!_isDefault)
            {
                PageUtility.DeleteLayoutPage(_pageId);
                Response.Redirect(ResolveUrl("~/mykaneva/editPage.aspx?communityId=" + this._channelId));
            }
		}
		
		private void btnAdd_Click(object sender, ImageClickEventArgs e)
		{
            SaveAll ();

			// also save layout
            if (hfInitialLayout.Value != hfSaveString.Value || hfInitialColumnWidth.Value != hfLeftColumnWidth.Value)
            {
                // Check to make sure user did not enter any "potty mouth" words
                if (KanevaWebGlobals.isTextRestricted (txtPageName.Text, Constants.eRESTRICTION_TYPE.POTTY_MOUTH))
                {
                    MagicAjax.AjaxCallHelper.WriteAlert (RESTRICTED_ERR_MSG);
                    return;
                }

                SavePage (hfSaveString.Value, hfLeftColumnWidth.Value);
            }

            //create the new page
            CreateNewPage();
        }

		#endregion

		// Page Widget Editing
		#region Methods

		private int GetContestId(int channelId)
		{
			if (_contestId == -2)
			{
				_contestId = StoreUtility.GetContestId(channelId);
			}
			return _contestId;
		}

		// save the layout -- remove any pages marked for deletion
		// and update the sequence of pages along with the default page
		protected void btnSaveWidgets_Click (object sender, EventArgs e)
		{
            // Check to make sure user did not enter any "potty mouth" words
            if (KanevaWebGlobals.isTextRestricted (txtPageName.Text, Constants.eRESTRICTION_TYPE.POTTY_MOUTH))
            {
                MagicAjax.AjaxCallHelper.WriteAlert (Constants.VALIDATION_REGEX_RESTRICTED_WORD_MESSAGE);
                return;
            }

            // save layout
			SavePage( hfSaveString.Value, hfLeftColumnWidth.Value );

			// Navigate back to the correct page
			DataRow drChannel = CommunityUtility.GetCommunity (_channelId);
			
			Response.Redirect( ResolveUrl("~/channel/channelPage.aspx?communityId= " + _channelId + "&pageId=" + _pageId) );
		}

		/// <summary>
		/// InitWidgetEditing
		/// </summary>
		private void InitWidgetEditing ()
		{
			if (!IsPostBack)
			{
                btnSaveBottom.Attributes.Add("onclick", "widget_page.GetSaveString('hfSaveString');" + ClientScript.GetPostBackEventReference(btnSaveBottom, "") + ";return false;");
				btnSaveBottom.Attributes.Add( "language", "javascript" );

                btnSaveTop.Attributes.Add("onclick", "widget_page.GetSaveString('hfSaveString');" + ClientScript.GetPostBackEventReference(btnSaveTop, "") + ";return false;");
				btnSaveTop.Attributes.Add( "language", "javascript" );
			}
		}

		/// <summary>
		/// set default module groups and modules
		/// </summary>
		private void DefaultPageWidgets ()
		{
			if(_isPersonal)
			{
				rpModuleGroup.DataSource = WebCache.GetLayoutModuleGroups(
					(int)Constants.eMODULE_GROUP_VISIBLE.PERSON, "");
				rpModuleGroup.DataBind();
			}
			else
			{
				// 7 is admin only
				string filter = " AND module_group_id <> 7 ";
				if (IsAdministrator ())
				{
					filter = "";
				}

                rpModuleGroup.DataSource = WebCache.GetLayoutModuleGroups(
					(int)Constants.eMODULE_GROUP_VISIBLE.CHANNEL, filter);
				rpModuleGroup.DataBind();
			}
		}

		/// <summary>
		/// initialize layout of page modules
		/// </summary>
		private void InitPage()
		{
			StringBuilder msg = new StringBuilder();

			msg.Append( "function InitPage()\n{\n\tvar area;\n\n" );

            

			double left_percentage = PageUtility.GetLayoutPageLeftPercentage(_pageId);

			// keep track of number of required panels
			int num_control_panels = 0;
            int num_fame_panels = 0;
            int num_personal_panels = 0;
            int num_friend_panels = 0;
            int num_title_panels = 0;

            DataTable dtZones = WebCache.GetLayoutZones();
			foreach ( DataRow drZone in dtZones.Rows )
			{
				int zone_id = Convert.ToInt32( drZone["zone_id"] );
                if (zone_id != (int)Constants.DELETE_ZONE_ID )
				{
					int zone_width = 0;
					
					if ( (zone_id == (int)Constants.HEADER_ZONE_ID) || (zone_id == (int)Constants.FOOTER_ZONE_ID) )
						zone_width = Convert.ToInt32(drZone["width"].ToString());
                    else if (zone_id == (int)Constants.LEFT_ZONE_ID || zone_id == (int)Constants.LEFT_TOP_ZONE_ID)
					{
						zone_width = (int)((double)613 * left_percentage + 0.5); // round up
						hfInitialColumnWidth.Value = zone_width.ToString();
					}
                    else if (zone_id == (int)Constants.RIGHT_ZONE_ID )
						zone_width = 613 - (int)((double)613 * left_percentage );

                    int lockzone = 0;
                    if ((zone_id == 1 || zone_id == 6) && _isDefault && _isPersonal)
                    {
                        lockzone = 1;
                    }
                                                           
                    msg.AppendFormat( "\tarea = widget_page.AddArea($('area_{0}'),\"{1}\",{2},{3},{4});\n", 
						zone_id.ToString(), zone_id.ToString(), zone_width.ToString(), drZone["height"].ToString(), lockzone );

					msg.Append( "\n" );

					// get data for the zone

					DataTable dtPageModules = PageUtility.GetLayoutPageModules(_pageId, zone_id );
					foreach ( DataRow drModule in dtPageModules.Rows )
					{
						int edit_page = 0;
						int delete_page = 1;

						// any modules that should not display the 'edit' link
						switch( Convert.ToInt32( drModule["module_id"].ToString() ) )
						{
							// only one of these types of cp's will be on a given channel
							case (int) Constants.eMODULE_TYPE.CONTROL_PANEL:
							case (int) Constants.eMODULE_TYPE.CHANNEL_CONTROL_PANEL:
								num_control_panels++;
								delete_page = _isDefault && (num_control_panels == 1) ? 0 : 1;
								break;
                            
                            case (int)Constants.eMODULE_TYPE.FAME_PANEL:
                                num_fame_panels++;
                                delete_page = _isDefault && (num_fame_panels == 1) ? 0 : 1;
                                break;

                            case (int)Constants.eMODULE_TYPE.TITLE_TEXT:
                                num_title_panels++;
                                delete_page = _isDefault && (num_title_panels == 1) ? 0 : 1;
                                break;

                            case (int)Constants.eMODULE_TYPE.PERSONAL:
                                num_personal_panels++;
                                delete_page = _isDefault && (num_personal_panels == 1) ? 0 : 1;
                                break;

                            case (int)Constants.eMODULE_TYPE.FRIENDS:
                                num_friend_panels++;
                                delete_page = _isDefault && (num_friend_panels == 1) ? 0 : 1;
                                break;

							default:
								edit_page = 0;	// change this to 1 if you want to show edit button on widgets
								break;
						}

						msg.AppendFormat( "\tarea.AddWidget(WidgetManager.CreateWidget('{0}', 'moduleSourceDiv', '{1}', widget_page, {2}, {3}, area.locked) )\n",
							drModule["module_id"].ToString(), drModule["id"].ToString(), edit_page, delete_page );
					}
				}
				else
				{
					// delete zone is a special case
					// add delete zone
					msg.AppendFormat( "\twidget_page.AddDeletionArea($('area_{0}'),\"{1}\",{2},{3});\n", 
						drZone["zone_id"].ToString(), drZone["zone_id"].ToString(), drZone["width"].ToString(), drZone["height"].ToString() );
				}

				msg.Append( "\n" );
			}

			msg.Append( "\twidget_page.Update();\n" );

			// init code to drag column widths
			msg.Append( "\tInitColumnDragging();\n" );

			// init tooltip code
			msg.Append( "\tTooltip.init();\n" );

			// get initial layout
			msg.Append( "\twidget_page.GetSaveString('hfInitialLayout');" );

			// ensure resizing window updates widgets
			msg.Append( "window.onresize = function OnResizeWindow() { widget_page.Update(); };\n" );
			
			msg.Append( "}" );

			RegisterScripts.RegisterClientScriptBlock(Page, "initpage", msg.ToString(), true );
		}

		/// <summary>
		/// save the page layout
		/// </summary>
		/// <param name="layout"></param>
		private void SavePage( String layout, String left_column_width )
		{
			// update layout page 
			PageUtility.UpdateLayoutPage( _pageId, 
				txtPageName.Text, Convert.ToInt32( ddlNewGroup.SelectedValue ), 
				Convert.ToInt32( ddlPageAccess.SelectedValue ) );

			if ( layout.Length > 0 && left_column_width.Length > 0 )
			{
				// change 613 to DB call or constant
				PageUtility.UpdateLayoutPageLeftPercentage( _pageId, (Convert.ToDouble(left_column_width) / (double)613) );

				string[] arZones = layout.Split( ';' );

				foreach (string zone in arZones) 
				{
					string delimStr = ":";
					char[] delimiter = delimStr.ToCharArray();

					string[] arZoneModules = zone.Split( delimiter, 2 );
					
					// zone_id is at index 0
					string str_zone_id = arZoneModules[0];
					if ( str_zone_id.Length > 0 )
					{
						// the modules for the zone follow in the string at index 1
						string modules = arZoneModules[1];
						if ( modules.Length > 0 )
						{
							string[] arModules = modules.Split( ',' );

							// i is the sequence for this zone
							for (int i=0; i < arModules.Length; i++) 
							{
								int zone_id = Convert.ToInt32( str_zone_id );

								string module = arModules[i];

								string[] arModuleIds = module.Split( '_' );
								
								if ( arModuleIds[0] == "new" )
								{
									// add new module to this zone
									int module_id		= Convert.ToInt32( arModuleIds[1] );
									int module_page_id	= -1;

									string module_title = WebCache.GetModuleTitle( module_id );

									switch (module_id)
									{
										case (int) Constants.eMODULE_TYPE.TITLE_TEXT:
											module_page_id = WidgetUtility.InsertLayoutModuleTitle(module_title, true, null);
											break;

										case (int) Constants.eMODULE_TYPE.PROFILE:
											module_page_id = WidgetUtility.InsertLayoutModuleProfile(module_title);
											break;

                                        case (int)Constants.eMODULE_TYPE.FAME_PANEL:
                                            module_page_id = WidgetUtility.InsertLayoutModuleFame(module_title);
                                            break;
                                        
                                        case (int) Constants.eMODULE_TYPE.FRIENDS:
											module_page_id = WidgetUtility.InsertLayoutModuleFriends(module_title);
											break;

										case (int) Constants.eMODULE_TYPE.BLOGS:
											module_page_id = WidgetUtility.InsertLayoutModuleBlogs(module_title);
											break;

										case (int) Constants.eMODULE_TYPE.CHANNELS:
											module_page_id = WidgetUtility.InsertLayoutModuleChannels(module_title);
											break;

										case (int) Constants.eMODULE_TYPE.MENU:
											module_page_id = WidgetUtility.InsertLayoutModuleMenu();
											break;

										case (int) Constants.eMODULE_TYPE.COMMENTS:
											module_page_id = WidgetUtility.InsertLayoutModuleComments(module_title);
											break;
										
										case (int) Constants.eMODULE_TYPE.HTML_CONTENT:
											module_page_id = WidgetUtility.InsertLayoutModuleHtml(module_title);
											break;

                                        case (int)Constants.eMODULE_TYPE.HTML_CONTENT_BASIC:
                                            module_page_id = WidgetUtility.InsertLayoutModuleHtml2("");
                                            break;

										case (int) Constants.eMODULE_TYPE.SYSTEM_STATS:
										{
											string title = WidgetUtility.GetDefaultTitle(_pageId);
											module_page_id = WidgetUtility.InsertLayoutModuleSystemStats( title + " Stats" );
											break;
										}
										
										case (int) Constants.eMODULE_TYPE.MY_COUNTER:
											module_page_id = WidgetUtility.InsertLayoutModuleCounter(module_title);
											break;

										case (int) Constants.eMODULE_TYPE.MY_PICTURE:
											module_page_id = WidgetUtility.InsertLayoutModuleMyPicture(module_title);
											break;
										
										case (int) Constants.eMODULE_TYPE.NEW_PEOPLE:
											module_page_id = WidgetUtility.InsertLayoutModuleNewPeople(module_title);
											break;

										case (int) Constants.eMODULE_TYPE.MULTIPLE_PICTURES:
											module_page_id = WidgetUtility.InsertLayoutModuleMultiplePictures(module_title);
											break;

										case (int) Constants.eMODULE_TYPE.SLIDE_SHOW:
											module_page_id = WidgetUtility.InsertLayoutModuleSlideShow(module_title);
											break;

										case (int) Constants.eMODULE_TYPE.GAMES_PLAYER:
											module_page_id = WidgetUtility.InsertLayoutModuleStores ((int) Constants.eMODULE_TYPE.GAMES_PLAYER, "My Games");
											break;

										case (int) Constants.eMODULE_TYPE.MUSIC_PLAYER:
											module_page_id = WidgetUtility.InsertLayoutModuleStores ((int) Constants.eMODULE_TYPE.MUSIC_PLAYER, "My Music");
											break;

										case (int) Constants.eMODULE_TYPE.VIDEO_PLAYER:
											module_page_id = WidgetUtility.InsertLayoutModuleStores ((int) Constants.eMODULE_TYPE.VIDEO_PLAYER, "My Videos");
											break;

										case (int) Constants.eMODULE_TYPE.SINGLE_PICTURE:
											module_page_id = WidgetUtility.InsertLayoutModuleSinglePicture(module_title);
											break;

										case (int) Constants.eMODULE_TYPE.CONTROL_PANEL:
											module_page_id = WidgetUtility.InsertLayoutModuleControlPanel(module_title);
											break;
										
										case (int) Constants.eMODULE_TYPE.MIXED_MEDIA:
											module_page_id = WidgetUtility.InsertLayoutModuleMixedMedia(module_title);
											break;
										
										case (int) Constants.eMODULE_TYPE.CHANNEL_OWNER:
											module_page_id = WidgetUtility.InsertLayoutModuleMyPicture("Channel Owner");
											break;
										
										case (int) Constants.eMODULE_TYPE.CHANNEL_MEMBERS:
											module_page_id = WidgetUtility.InsertLayoutModuleChannelMembers(module_title);
											break;
										
										case (int) Constants.eMODULE_TYPE.CHANNEL_EVENTS:
											module_page_id = WidgetUtility.InsertLayoutModuleEvents(module_title);
											break;
										
										case (int) Constants.eMODULE_TYPE.CHANNEL_FORUM:
											module_page_id = WidgetUtility.InsertLayoutModuleChannelForum(module_title);
											break;
										
										case (int) Constants.eMODULE_TYPE.CHANNEL_CONTROL_PANEL:
											module_page_id = WidgetUtility.InsertLayoutModuleChannelControlPanel(module_title);
											break;
										
										case (int) Constants.eMODULE_TYPE.CHANNEL_DESCRIPTION:
											module_page_id = WidgetUtility.InsertLayoutModuleChannelDescription(module_title);
											break;
										
										case (int) Constants.eMODULE_TYPE.CHANNEL_INFO:
											module_page_id = WidgetUtility.InsertLayoutModuleChannelInfo(module_title);
											break;
										
										case (int) Constants.eMODULE_TYPE.OMM_VIDEO_PLAYER:
											module_page_id = WidgetUtility.InsertLayoutModuleOmmMedia ((int) Constants.eMODULE_TYPE.OMM_VIDEO_PLAYER, module_title);
											break;

										case (int) Constants.eMODULE_TYPE.OMM_VIDEO_PLAYLIST:
											module_page_id = WidgetUtility.InsertLayoutModuleOmmPlaylist ((int) Constants.eMODULE_TYPE.OMM_VIDEO_PLAYLIST, module_title);
											break;

										case (int) Constants.eMODULE_TYPE.OMM_MUSIC_PLAYER:
											module_page_id = WidgetUtility.InsertLayoutModuleOmmMedia ((int) Constants.eMODULE_TYPE.OMM_MUSIC_PLAYER, module_title);
											break;

										case (int) Constants.eMODULE_TYPE.OMM_MUSIC_PLAYLIST:
											module_page_id = WidgetUtility.InsertLayoutModuleOmmPlaylist ((int) Constants.eMODULE_TYPE.OMM_MUSIC_PLAYLIST, module_title);
											break;
										
										case (int) Constants.eMODULE_TYPE.TOP_CONTRIBUTORS:
											module_page_id = WidgetUtility.InsertLayoutModuleChannelTopContributors(module_title, Constants.DEFAULT_TOP_CONTRIBUTORS_NUM_TOP_CONTRIBUTORS);
											break;

										case (int) Constants.eMODULE_TYPE.CONTEST_SUBMISSIONS:
											module_page_id = WidgetUtility.InsertLayoutModuleContestSubmissions (module_title, true, GetContestId(_channelId), 20, "The Sound Stage: Click, Sample and Rave Your Favorites<br/><br/>Is there a star in this constellation of contestants? To sample -- and decide whether they deserve your Rave -- click on the image. The larger the picture the more Raves that contestant has already earned. You can Rave as many as you like -- but only once. Click the thumbnail to listen--and vote.", "Media Cloud", false, 1, true, 500);
											break;

										case (int) Constants.eMODULE_TYPE.MOST_VIEWED:
											module_page_id = WidgetUtility.InsertLayoutModuleMostViewedMedia (module_title, "Most viewed media", "Most viewed footer", false, 1, 10, 2, true, (int)Constants.ePICTURE_SIZE.MEDIUM);
											break;

										case (int) Constants.eMODULE_TYPE.CONTEST_TOP_RESULTS:
											module_page_id = WidgetUtility.InsertLayoutModuleContestTopVotes (module_title, true, GetContestId(_channelId), "Display your top submissions here", 3, true, "display footer here", 0);
											break;

										case (int) Constants.eMODULE_TYPE.CONTEST_UPLOAD:
											module_page_id = WidgetUtility.InsertLayoutModuleContestUpload (module_title, GetContestId(_channelId), "Bring it! Submit your sound here to get seen, heard -- and raved. To qualify as a contestant, you must upload an MP3 audio file along with an up-to-date photo of yourself to serve as an icon for your sound. Tip: the more impressive your photo, the more likely people will sample your sound.", "Enter Terms Here", "Enter HTML for right column layout.", "http://www.kaneva.com/channels/", "Not logged in HTML here", "Submissions ended HTML here", "Voting ended HTML here",true,true,true,true,"photo_label","keyword_label", "title_label", "artist_name", "media_label", true, "Brief Description");
											break;

										case (int) Constants.eMODULE_TYPE.CONTEST_GET_RAVED:
											module_page_id = WidgetUtility.InsertLayoutModuleContestGetRaved (module_title, GetContestId(_channelId), "Syndicate/Share this contest<br/>Share this contest with others by putting this banner on your page. Click to copy code and paste it on your page.", "http://www.kaneva.com/images/contests/bug_234X60.gif", "Add media to your web site or blog" , "http://www.kaneva.com", "", "", "Put no media HTML here", "Put have media HTML here", "Put not logged in HTML here", "http://www.kaneva.com/images/contests/bug_234X60_noupload.gif");
											break;

										case (int) Constants.eMODULE_TYPE.CONTEST_PROFILE_HEADER:
											module_page_id = WidgetUtility.InsertLayoutModuleProfileHeader(module_title, true, "", true, true, true, true, true, true, true );
											break;

										case (int) Constants.eMODULE_TYPE.COMMUNITY_TOP_BANNER:
											module_page_id = WidgetUtility.InsertLayoutModuleTopBanner(module_title, true, "", true, true, true, true, true, true, true );
											break;

										case (int) Constants.eMODULE_TYPE.LEADER_BOARD:
											module_page_id = WidgetUtility.InsertLayoutModuleLeaderBoard(module_title, "Header", "brief description", 1, 10, "footer", false, false, false, false , false, false, false, false, false, false, false, false, false, false, (int)Constants.eDEFAULTS.TIME_SPAN_MONTH, false, false, (int)Constants.eMODULE_TYPE.LEADER_BOARD);
											break;

										case (int) Constants.eMODULE_TYPE.KANEVA_LEADER_BOARD:
											module_page_id = WidgetUtility.InsertLayoutModuleLeaderBoard(module_title, "Header", "Kaneva Leader Board", 1, 10, "footer", false, false, false, false , false, false, false, false, false, false, false, false, false, false, (int)Constants.eDEFAULTS.TIME_SPAN_MONTH, false, false, (int)Constants.eMODULE_TYPE.KANEVA_LEADER_BOARD);
											break;

										case (int) Constants.eMODULE_TYPE.HOT_NEW_STUFF:
											module_page_id = WidgetUtility.InsertLayoutModuleHotNewStuff(module_title, "Header", "footer", false, 1, 10, 4, false,  (int)Constants.ePICTURE_SIZE.MEDIUM);

											break;

										case (int) Constants.eMODULE_TYPE.BILLBOARD:
											module_page_id = WidgetUtility.InsertLayoutModuleBillboard(module_title, false, "Header", 10, false, "footer", 10, (int)Constants.ePICTURE_SIZE.MEDIUM);
											break;

										case (int) Constants.eMODULE_TYPE.NEWEST_MEDIA:
											module_page_id = WidgetUtility.InsertLayoutModuleNewestMedia(module_title, "Header", "footer", false, 1, 5, 5, false, (int)Constants.ePICTURE_SIZE.MEDIUM);
											break;

										case (int) Constants.eMODULE_TYPE.USER_GIFTS:
											module_page_id = WidgetUtility.InsertLayoutModuleUserGifts ( module_title, 4 );
											break;

										case (int) Constants.eMODULE_TYPE.PROFILE_PORTAL:
											module_page_id = WidgetUtility.InsertLayoutModuleProfilePortal( module_title, true );
											break;

										case (int) Constants.eMODULE_TYPE.CHANNEL_PORTAL:
											module_page_id = WidgetUtility.InsertLayoutModuleChannelPortal( module_title );
											break;

										case (int) Constants.eMODULE_TYPE.USER_UPLOAD:
											module_page_id = WidgetUtility.InsertLayoutModuleUserUpload( module_title, "Header", 1, "", "", (new ArrayList()));
											break;

                                        case (int)Constants.eMODULE_TYPE.INTERESTS:
                                            module_page_id = WidgetUtility.InsertLayoutModuleInterests(module_title);
                                            break;

                                        case (int) Constants.eMODULE_TYPE.PERSONAL:
                                            module_page_id = WidgetUtility.InsertLayoutModulePersonal (module_title);
                                            break;

                                        default:
											break;
									}

									if (module_page_id != -1)
										PageUtility.AddLayoutPageModule( module_page_id, _pageId, module_id, zone_id, i+1 );
								}
								else
								{
									// if the zone is the delete zone, then remove this module
									if ( (int)Constants.DELETE_ZONE_ID == zone_id )
									{
										// id for layout_page_modules
										int id = -1;
										try
										{
											id = Convert.ToInt32( arModuleIds[0] );
										}
										catch(Exception)
										{
											// something bad happened, should not be here!
											return;
										}

										int module_id = PageUtility.GetLayoutPageModule_ModuleID(id);

										switch ( module_id )
										{
											case (int) Constants.eMODULE_TYPE.TITLE_TEXT:
                                                {
                                                    // if this is the default page
                                                    if (_isDefault && _isPersonal)
                                                    {
                                                        // only remove the panel if it's not the only one
                                                        // (there must be 1 panel on the default page)
                                                        if (WidgetUtility.GetLayoutPage_NumControlPanels(_pageId, module_id) > 1)
                                                            WidgetUtility.DeleteModuleFame(id);
                                                    }
                                                    else
                                                        WidgetUtility.DeleteModuleTitle(id);

                                                    break;
                                                }
                                                

											case (int) Constants.eMODULE_TYPE.PROFILE:
												WidgetUtility.DeleteModuleProfile(id);
												break;                                           

                                            case (int)Constants.eMODULE_TYPE.FAME_PANEL:
                                                {
                                                    // if this is the default page
                                                    if (_isDefault && _isPersonal)
                                                    {
                                                        // only remove the fame panel if it's not the only one
                                                        // (there must be 1 fame panel on the default page)
                                                        if (WidgetUtility.GetLayoutPage_NumControlPanels(_pageId, module_id) > 1)
                                                            WidgetUtility.DeleteModuleFame(id);
                                                    }
                                                    else
                                                        WidgetUtility.DeleteModuleFame(id);

                                                    break;
                                                }


											case (int) Constants.eMODULE_TYPE.FRIENDS:
                                                {
                                                    // if this is the default page
                                                    if (_isDefault && _isPersonal)
                                                    {
                                                        // only remove the panel if it's not the only one
                                                        // (there must be 1 panel on the default page)
                                                        if (WidgetUtility.GetLayoutPage_NumControlPanels(_pageId, module_id) > 1)
                                                            WidgetUtility.DeleteModuleFame(id);
                                                    }
                                                    else
                                                        WidgetUtility.DeleteModuleFriends(id);

                                                    break;
                                                }
                                                

											case (int) Constants.eMODULE_TYPE.BLOGS:
												WidgetUtility.DeleteModuleBlogs(id);
												break;

											case (int) Constants.eMODULE_TYPE.CHANNELS:
												WidgetUtility.DeleteModuleChannels(id);
												break;

											case (int) Constants.eMODULE_TYPE.MENU:
												WidgetUtility.DeleteModuleMenu(id);
												break;

											case (int) Constants.eMODULE_TYPE.COMMENTS:
												WidgetUtility.DeleteModuleComments(id);
												break;
											
											case (int) Constants.eMODULE_TYPE.HTML_CONTENT:
												WidgetUtility.DeleteModuleHtml(id);
												break;

                                            case (int)Constants.eMODULE_TYPE.HTML_CONTENT_BASIC:
                                                WidgetUtility.DeleteModuleHtml2(id);
                                                break;

											case (int) Constants.eMODULE_TYPE.SYSTEM_STATS:
												WidgetUtility.DeleteModuleSystemStats(id);
												break;
											
											case (int) Constants.eMODULE_TYPE.MY_COUNTER:
												WidgetUtility.DeleteModuleCounter(id);
												break;

											case (int) Constants.eMODULE_TYPE.MY_PICTURE:
												WidgetUtility.DeleteModuleMyPicture(id);
												break;
											
											case (int) Constants.eMODULE_TYPE.NEW_PEOPLE:
												WidgetUtility.DeleteModuleNewPeople(id);
												break;

											case (int) Constants.eMODULE_TYPE.MULTIPLE_PICTURES:
												WidgetUtility.DeleteModuleMultiplePictures(id);
												break;

											case (int) Constants.eMODULE_TYPE.SLIDE_SHOW:
												WidgetUtility.DeleteModuleSlideShow(id);
												break;

											case (int) Constants.eMODULE_TYPE.GAMES_PLAYER:
												WidgetUtility.DeleteModuleStores (id);
												break;

											case (int) Constants.eMODULE_TYPE.MUSIC_PLAYER:
												WidgetUtility.DeleteModuleStores (id);
												break;

											case (int) Constants.eMODULE_TYPE.VIDEO_PLAYER:
												WidgetUtility.DeleteModuleStores (id);
												break;

											case (int) Constants.eMODULE_TYPE.SINGLE_PICTURE:
												WidgetUtility.DeleteModuleSinglePicture(id);
												break;

											case (int) Constants.eMODULE_TYPE.LEADER_BOARD:
												WidgetUtility.DeleteModuleLeaderBoard(id);
												break;

											case (int) Constants.eMODULE_TYPE.KANEVA_LEADER_BOARD:
												WidgetUtility.DeleteModuleLeaderBoard(id);
												break;

											case (int) Constants.eMODULE_TYPE.HOT_NEW_STUFF:
												WidgetUtility.DeleteModuleHotNewStuff(id);
												break;

											case (int) Constants.eMODULE_TYPE.BILLBOARD:
												WidgetUtility.DeleteModuleBillboard(id);
												break;

											case (int) Constants.eMODULE_TYPE.NEWEST_MEDIA:
												WidgetUtility.DeleteModuleNewestMedia(id);
												break;

											case (int) Constants.eMODULE_TYPE.USER_UPLOAD:
												WidgetUtility.DeleteModuleUserUpload(id);
												break;

											case (int) Constants.eMODULE_TYPE.CONTROL_PANEL:
											{
												// if this is the default page
												if ( _isDefault )
												{
													// only remove the control panel if it's not the only one
													// (there must be 1 control panel on the default page)
													if ( WidgetUtility.GetLayoutPage_NumPersonalControlPanels(_pageId) > 1 )
														WidgetUtility.DeleteModuleControlPanel(id);
												}
												else
													WidgetUtility.DeleteModuleControlPanel(id);
												
												break;
											}
											
											case (int) Constants.eMODULE_TYPE.MIXED_MEDIA:
												WidgetUtility.DeleteModuleMixedMedia(id);
												break;
											
											case (int) Constants.eMODULE_TYPE.CHANNEL_OWNER:
												WidgetUtility.DeleteModuleMyPicture(id);
												break;
											
											case (int) Constants.eMODULE_TYPE.CHANNEL_MEMBERS:
												WidgetUtility.DeleteModuleChannelMembers(id);
												break;
											
											case (int) Constants.eMODULE_TYPE.CHANNEL_EVENTS:
												WidgetUtility.DeleteModuleEvents(id);
												break;
											
											case (int) Constants.eMODULE_TYPE.CHANNEL_FORUM:
												WidgetUtility.DeleteModuleChannelForum(id);
												break;
											
											case (int) Constants.eMODULE_TYPE.CHANNEL_CONTROL_PANEL:
											{
												// if this is the default page
												if ( _isDefault )
												{
													// only remove the control panel if it's not the only one
													// (there must be 1 control panel on the default page)
													if ( WidgetUtility.GetLayoutPage_NumChannelControlPanels(_pageId) > 1 )
														WidgetUtility.DeleteModuleChannelControlPanel(id);
												}

												WidgetUtility.DeleteModuleChannelControlPanel(id);
												break;
											}
											
											case (int) Constants.eMODULE_TYPE.CHANNEL_DESCRIPTION:
												WidgetUtility.DeleteModuleChannelDescription(id);
												break;

											case (int) Constants.eMODULE_TYPE.CHANNEL_INFO:
												WidgetUtility.DeleteModuleChannelInfo(id);
												break;

											case (int) Constants.eMODULE_TYPE.OMM_VIDEO_PLAYER:
												WidgetUtility.DeleteModuleOmmMedia (id);
												break;

											case (int) Constants.eMODULE_TYPE.OMM_VIDEO_PLAYLIST:
												WidgetUtility.DeleteModuleOmmPlaylist (id);
												break;

											case (int) Constants.eMODULE_TYPE.OMM_MUSIC_PLAYER:
												WidgetUtility.DeleteModuleOmmMedia (id);
												break;

											case (int) Constants.eMODULE_TYPE.OMM_MUSIC_PLAYLIST:
												WidgetUtility.DeleteModuleOmmPlaylist (id);
												break;
											
											case (int) Constants.eMODULE_TYPE.TOP_CONTRIBUTORS:
												WidgetUtility.DeleteModuleChannelTopContributors (id);
												break;

											case (int) Constants.eMODULE_TYPE.CONTEST_SUBMISSIONS:
												WidgetUtility.DeleteModuleContestSubmissions (id);
												break;

											case (int) Constants.eMODULE_TYPE.MOST_VIEWED:
												WidgetUtility.DeleteModuleMostViewedMedia (id);
												break;

											case (int) Constants.eMODULE_TYPE.CONTEST_UPLOAD:
												WidgetUtility.DeleteModuleContestUpload (id);
												break;

											case (int) Constants.eMODULE_TYPE.CONTEST_PROFILE_HEADER:
												WidgetUtility.DeleteModuleProfileHeader (id);
												break;

											case (int) Constants.eMODULE_TYPE.COMMUNITY_TOP_BANNER:
												WidgetUtility.DeleteModuleTopBanner (id);
												break;

											case (int) Constants.eMODULE_TYPE.CONTEST_TOP_RESULTS:
												WidgetUtility.DeleteModuleContestTopVotes (id);
												break;

											case (int) Constants.eMODULE_TYPE.USER_GIFTS:
												WidgetUtility.DeleteModuleUserGifts (id);
												break;

											case (int) Constants.eMODULE_TYPE.PROFILE_PORTAL:
												WidgetUtility.DeleteModuleProfilePortal(id);
												break;

											case (int) Constants.eMODULE_TYPE.CHANNEL_PORTAL:
												WidgetUtility.DeleteModuleChannelPortal(id);
												break;

                                            case (int)Constants.eMODULE_TYPE.INTERESTS:
                                                WidgetUtility.DeleteModuleInterests(id);
                                                break;

                                            case (int) Constants.eMODULE_TYPE.PERSONAL:
                                                {
                                                    // if this is the default page
                                                    if (_isDefault && _isPersonal)
                                                    {
                                                        // only remove the panel if it's not the only one
                                                        // (there must be 1 panel on the default page)
                                                        if (WidgetUtility.GetLayoutPage_NumControlPanels(_pageId, module_id) > 1)
                                                            WidgetUtility.DeleteModuleFame(id);
                                                    }
                                                    else
                                                        WidgetUtility.DeleteModulePersonal(id);

                                                    break;
                                                }                                                

                                            default:
												break;
										}
									}	
									else
									{
										// update position of existing module
										PageUtility.UpdateLayoutPageModule( Convert.ToInt32( arModuleIds[0] ), 
											zone_id, i+1 );
									}
								}
							}
						}
					}
				}
			}
		}

		protected void SetPageLayout()
		{
			StringBuilder msg = new StringBuilder();

			msg.Append( "\nvar widget_page;\n" );
			msg.Append( "widget_page = new WidgetPage($('relativeContainer'), WidgetManager, $('lyrGhostModuleBox'), $('lyrModuleSeparator'));\n" );
			msg.Append( "widget_page.widget_edit_template = \"window.open('ModuleEditor.aspx?modid=%widget_id%&pageId=" + _pageId + "&fromEdit=true', 'ModuleEditor', 'width=750,height=680,scrollbars=1,resizable=1,status=1,toolbar=0');\"\n");
			msg.Append( "addLoadEvent(InitPage);\n\n" );

			RegisterScripts.RegisterStartupScript(Page, "NewWidgetPage", msg.ToString(), true, 1);

			// generate zones
		
			// wide column (body)
            HtmlGenericControl divLeftLayoutTop = new HtmlGenericControl("div");
            divLeftLayoutTop.Style["text-align"] = "center";
            divLeftLayoutTop.Attributes.Add("align", "center");
            if (!(_isDefault && _isPersonal))
            {
                divLeftLayoutTop.Style["display"] = "hidden";
            }

            HtmlGenericControl divLeftTopChild = new HtmlGenericControl("div");
            divLeftTopChild.Style["width"] = "100%";
            divLeftTopChild.Style["text-align"] = "center";
            divLeftTopChild.Attributes.Add("class", "darkBoldVerdana12");
			            
            HtmlGenericControl divLeftLayout	= new HtmlGenericControl("div");
			divLeftLayout.Style["text-align"]	= "center";
			divLeftLayout.Attributes.Add("align","center");
			
			HtmlGenericControl divLeftChild		= new HtmlGenericControl("div");
			divLeftChild.Style["width"]			= "100%";
			divLeftChild.Style["text-align"]	= "center";
			divLeftChild.Attributes.Add("class", "darkBoldVerdana12");
			
			HtmlGenericControl divRightLayout	= new HtmlGenericControl("div");
			divRightLayout.Style["text-align"]	= "center";
			divRightLayout.Attributes.Add("align","center");

			HtmlGenericControl divRightChild	= new HtmlGenericControl("div");
			divRightChild.Style["width"]		= "100%";
			divRightChild.Style["text-align"]	= "center";
			divRightChild.Attributes.Add("class", "darkBoldVerdana12");

            

			LiteralControl lBreak				= new LiteralControl("<br/>");
			LiteralControl rBreak				= new LiteralControl("<br/>");
			
			//divLeftLayout.Attributes.Add("class","columnZone");
			divLeftLayout.ID				= "area_2";
			divLeftLayout.Style["width"]		= "347px";
			//divLeftChild.InnerText			= "left";

			//divRightLayout.Attributes.Add("class","columnZone");
			divRightLayout.ID				= "area_3";
			divRightLayout.Style["width"]		= "347px";
			//divRightChild.InnerText			= "right";

            //divRightLayoutTop.Attributes.Add("class","columnZone");
            divLeftLayoutTop.ID = "area_6";
            divLeftLayoutTop.Style["width"] = "347px";
            //divRightTopChild.InnerText			= "right";
			
			divLeftLayout.Controls.Add( divLeftChild );
			divLeftLayout.Controls.Add( lBreak );

			divRightLayout.Controls.Add( divRightChild );
			divRightLayout.Controls.Add( rBreak );

            divLeftLayoutTop.Controls.Add(divLeftTopChild);
            divLeftLayoutTop.Controls.Add(lBreak);

            tdContentLeft.Controls.Add(divLeftLayoutTop);
            tdContentLeft.Controls.Add( divLeftLayout );            
			tdContentRight.Controls.Add( divRightLayout );

			// Add the javascript for changing pages
			string scriptString = "<script language=\"javaScript\">\n<!--\n function ChangePage (id) {\n";
			scriptString += "	document.all." + hfChangePageID.ClientID + ".value = id;\n";			// save the page number clicked to the hidden field
			scriptString += "   widget_page.GetSaveString('hfSaveString');\n";
            scriptString += "	" + ClientScript.GetPostBackEventReference(hbChangePage, "") + ";\n";		// call the __doPostBack function to post back the form and execute the PageClick event
			scriptString += "}\n// -->\n";
			scriptString += "</script>";

            if (!ClientScript.IsClientScriptBlockRegistered(GetType (), "ChangePage"))
			{
                ClientScript.RegisterClientScriptBlock(GetType(), "ChangePage", scriptString);
			}

			// ensure adding a page button saves the layout
			btnCreate.Attributes.Add( "onclick", "widget_page.GetSaveString('hfSaveString');"
				+ " PManager.SavePositions('" + hfPositions.ClientID + "'); "
                + ClientScript.GetPostBackEventReference(btnCreate, "") + ";return false;");

            if (_isDefault)
            {
                btnDelete.Attributes.Add("onclick", "javascript:if (!confirm(\"You can not delete the profile home page.\")){return false;};");
            }
            else
            {
                btnDelete.Attributes.Add("onclick", "javascript:if (!confirm(\"Are you sure you want to delete this page?\")){return false;};");
            }
			
		}

		#endregion

		// Page Widget Editing
		#region Event Handlers
		
		protected void rpModuleGroup_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
			{
				Repeater rpModules = (Repeater)e.Item.FindControl("rpModules");
				if (rpModules != null)
				{
					rpModules.ItemDataBound += new RepeaterItemEventHandler(rpModules_ItemDataBound);

					DataRowView drv		= (DataRowView) e.Item.DataItem;
					int module_group_id	= Convert.ToInt32( drv["module_group_id"] );
					bool expanded		= Convert.ToBoolean( drv["expanded"] );

					if(_isPersonal)
					{
                        rpModules.DataSource = WebCache.GetLayoutModules(module_group_id,
							(int) Constants.eMODULE_VISIBLE.PERSON);
						rpModules.DataBind();
					}
					else
					{
                        rpModules.DataSource = WebCache.GetLayoutModules(module_group_id,
							(int) Constants.eMODULE_VISIBLE.CHANNEL);
						rpModules.DataBind();
					}

					Panel pnlModuleGroup			= (Panel)e.Item.FindControl("pnlModuleGroup");
					HtmlImage imgOpenBtn			= (HtmlImage)e.Item.FindControl("imgOpenBtn");
					HtmlImage imgCloseBtn			= (HtmlImage)e.Item.FindControl("imgCloseBtn");

					if ( pnlModuleGroup != null && imgOpenBtn != null && imgCloseBtn != null  )
					{
						if ( expanded )
						{
							imgOpenBtn.Style.Add("DISPLAY", "none");
						}
						else
						{
							imgCloseBtn.Style.Add("DISPLAY", "none");
							pnlModuleGroup.Style.Add("DISPLAY", "none");
						}

						imgOpenBtn.Attributes.Add("onclick", "Element.toggle('" + imgOpenBtn.ClientID + "','" + imgCloseBtn.ClientID + "','" + pnlModuleGroup.ClientID + "')");
						imgCloseBtn.Attributes.Add("onclick", "Element.toggle('" + imgOpenBtn.ClientID + "','" + imgCloseBtn.ClientID + "','" + pnlModuleGroup.ClientID + "')");
			
					}
				}
			}
		}

		protected void rpModules_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
			{
				HtmlTableCell tdModuleContainer	= (HtmlTableCell)e.Item.FindControl("tdModuleContainer");
				if ( tdModuleContainer != null )
				{
					DataRow drModule = ((DataRowView)e.Item.DataItem).Row;

					RegisterScripts.RegisterStartupScript(Page, "AddWidgetDescription_" + tdModuleContainer.ClientID, "WidgetManager.AddWidgetDescription('" + drModule["module_id"].ToString() + "', '" + drModule["title"].ToString() + "', '" + drModule["description"].ToString() + "', '" + drModule["icon_url"].ToString() + "');", true, 0 );

                    if (drModule.ItemArray[1] != null && drModule.ItemArray[1].ToString () != "Interests (v1)")
                    	RegisterScripts.RegisterStartupScript(Page, "CreateBaseWidget_" + tdModuleContainer.ClientID, "WidgetManager.CreateBaseWidget('" + drModule["module_id"].ToString() + "', '" + tdModuleContainer.ClientID + "', null);", true, 2 );
				}
			}
		}

		#endregion

		// Page Nav
		#region Page Nav

		private void SetChannelProperties()
		{
			DataRow drChannel = CommunityUtility.GetCommunity(_channelId);
			if(drChannel != null)
			{
				ChannelName = drChannel["name"].ToString();
			}
		}

		public string ChannelName
		{
			get 
			{
				if (ViewState ["ChannelName"] != null)
				{
					return ViewState ["ChannelName"].ToString();
				}
				else
				{
					return "";
				}
			} 
			set
			{
				ViewState ["ChannelName"] = value;
			}
		}

		/// <summary>
		/// The SubTab to display
		/// </summary>
		public string SubTab
		{
			get 
			{
				return m_SubTab;
			} 
			set
			{
				m_SubTab = value;
			}
		}

		/// <summary>
		/// The No Data Message to display
		/// </summary>
		public string NoDataMsg
		{
			get 
			{
				return lblNoData.Text;
			} 
			set
			{
				lblNoData.Text = value;
			}
		}
		/// <summary>
		/// The No Data Message visibility flag
		/// </summary>
		public bool NoDataMsg_Visible
		{
			get 
			{
				return lblNoData.Visible;
			} 
			set
			{
				lblNoData.Visible = value;
			}
		}

		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.dlPages.ItemDataBound += new DataListItemEventHandler(dlPages_ItemDataBound);
			this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
			this.btnSaveManagePages.Click += new System.EventHandler(this.btnSaveManagePages_Click);
			this.rpModuleGroup.ItemDataBound += new System.Web.UI.WebControls.RepeaterItemEventHandler(this.rpModuleGroup_ItemDataBound);
			btnDelete.Click +=new ImageClickEventHandler(btnDelete_Click);
			btnAdd.Click +=new ImageClickEventHandler(btnAdd_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
