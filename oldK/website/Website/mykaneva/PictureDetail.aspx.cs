///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva.mykaneva.widgets;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva.mykaneva
{
	/// <summary>
	/// Summary description for PictureDetail.
	/// </summary>
	public class PictureDetail : MainTemplatePage
	{
		//protected KanevaWeb.mykaneva.myKanNav	rnNavigation;
		protected ModuleSinglePictureView modSinglePictureView;

		protected Label			lblName;
		protected HyperLink		hlUsername, hlBack;
		protected Label			lblCreatedDate;

		private void Page_Load(object sender, System.EventArgs e)
		{
			if (!IsPostBack)
			{
				int asset_id = GetAssetId();

				modSinglePictureView.AssetId	= asset_id;
				modSinglePictureView.Alignment	= 0;

				DataRow drAsset = StoreUtility.GetAsset( asset_id );
                if (!drAsset["user_id"].Equals(DBNull.Value))
				{
                    UserFacade userFacade = new UserFacade();
                    User user = userFacade.GetUser(Convert.ToInt32(drAsset["user_id"]));

                    hlUsername.NavigateUrl = GetPersonalChannelUrl(user.NameNoSpaces);

					hlUsername.Text					= drAsset["username"].ToString();
					hlUsername.Attributes["alt"]	= hlUsername.Text;

					hlBack.NavigateUrl = GetAssetDetailsLink (asset_id);

					lblName.Text					= drAsset["name"].ToString();
					lblCreatedDate.Text				= Convert.ToDateTime( drAsset["created_date"].ToString() ).ToLongDateString();
				}
                else
                {
                    Response.Redirect("~/asset/notAvailable.aspx");
                }
			}
		}

		private int GetAssetId()
		{
			if (Request.Params["assetId"] == null)
			{
				return -1;
			}
			else
			{
				return Int32.Parse(Request.Params["assetId"].ToString());
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	
	}
}
