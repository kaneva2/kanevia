///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for incomeMonthly.
	/// </summary>
	public class incomeMonthly : SortedBasePage
	{
		protected incomeMonthly () 
		{
			Title = "Media Stats";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			// They must be logged in
			if (!Request.IsAuthenticated)
			{
				Response.Redirect (GetLoginURL ());
				return;
			}

			if (!IsPostBack)
			{
				BindData (1);
			}

			// this page has been combined with the main stats page
//			HeaderNav.FirstTab = Header.eFIRST_TAB.HOME;
//			HeaderNav.SecondTab = Header.eSECOND_TAB.HOME_STATS;
//			HeaderNav.SalesNav.SubTab = salesNav.eSALES_TAB.WHO_ARE_WATCHING;
//			HeaderNav.SetVisible3rdLevelNav(HeaderNav.SalesNav);
//			HeaderNav.InfoText = KanevaWebGlobals.GetResourceString("infoTextWhosWatching");
		}

		/// <summary>
		/// Bind the data
		/// </summary>
		/// <param name="pageNumber"></param>
		private void BindData (int pageNumber)
		{
			// Set the sortable columns
			//SetHeaderSortText (dgrdItemsSold);
			string orderby = CurrentSort + " " + CurrentSortOrder;
			
			DateTime startMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);

			DataTable dtMonthTotals = new DataTable ();
			dtMonthTotals.Columns.Add (new DataColumn ("purchase_month", System.Type.GetType("System.DateTime") ));
			dtMonthTotals.Columns.Add (new DataColumn ("month", System.Type.GetType("System.String") ));
			dtMonthTotals.Columns.Add (new DataColumn ("year", System.Type.GetType("System.String") ));
			dtMonthTotals.Columns.Add (new DataColumn ("items_sold", System.Type.GetType("System.Int32") ));
			dtMonthTotals.Columns.Add (new DataColumn ("total_royalty_amount", System.Type.GetType("System.Double") ));

            User user = KanevaWebGlobals.CurrentUser;

			int numMonthShowing = 18;
			DateTime signUpMonth = DateTime.MinValue;
			
            signUpMonth = user.SignupDate;
			signUpMonth = new DateTime(signUpMonth.Year, signUpMonth.Month, 1);
			

			for (int i = 0; i < numMonthShowing && startMonth.CompareTo(signUpMonth) >= 0 ; i++)
			{
				AddMonthlyTotal (startMonth, dtMonthTotals);
				startMonth = startMonth.AddMonths(-1);
			}

			//			PagedDataTable pds = StoreUtility.GetUserOrderItems (GetUserId (), Convert.ToInt32 (Request ["orderId"]), orderby, pageNumber, ITEMS_PER_PAGE);
			rptItemsSold.DataSource = dtMonthTotals;
			rptItemsSold.DataBind ();

			pgTop.NumberOfPages = "0"; //Math.Ceiling ((double) pds.TotalCount / ITEMS_PER_PAGE).ToString ();
			pgTop.DrawControl ();
			//
			//			// The results
			//			lblSearch.Text = GetResultsText (pds.TotalCount, pageNumber, ITEMS_PER_PAGE, pds.Rows.Count);
		}

		/// <summary>
		/// GetMonthlyTotal
		/// </summary>
		/// <param name="dtMonth"></param>
		/// <returns></returns>
		private void AddMonthlyTotal (DateTime dtSearch, DataTable dtMonthTotals)
		{
			DataRow drResult = StoreUtility.GetMonthlyTotal (GetUserId (), dtSearch);

			if (drResult != null)
			{
				DataRow drMonthTotal = dtMonthTotals.NewRow ();
				drMonthTotal ["purchase_month"] = dtSearch;
				drMonthTotal ["month"] = dtSearch.ToString ("MMMM");
				drMonthTotal ["year"] = dtSearch.ToString ("yyyy");;
				drMonthTotal ["items_sold"] = Convert.ToInt32 (drResult ["items_sold"]);
				drMonthTotal ["total_royalty_amount"] = StoreUtility.ConvertKPointsToDollars (Convert.ToDouble (drResult ["total_royalty_amount"]));
				dtMonthTotals.Rows.Add (drMonthTotal);
				drMonthTotal.AcceptChanges ();
			}
		}

		/// <summary>
		/// They clicked to sort a column
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected override void btnSort_Click (object sender, System.EventArgs e)
		{
			base.btnSort_Click (sender, e);
			BindData (pgTop.CurrentPageNumber);
		}

		/// <summary>
		/// Page Change
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pgTop_PageChange (object sender, PageChangeEventArgs e)
		{
			BindData (e.PageNumber);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="dtDate"></param>
		/// <returns></returns>
		protected string GetItemsSoldLink (DateTime dtDate)
		{
			return ResolveUrl ("~/mykaneva/incomeItemsSold.aspx?subsubtab=monthly&month=" + dtDate.Month + "&year=" + dtDate.Year);
		}

		protected Repeater rptItemsSold;
		protected Label lblSearch;
		protected Kaneva.Pager pgTop;
		protected PlaceHolder phBreadCrumb;

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
