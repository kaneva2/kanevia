<%@ Page Language="C#" MasterPageFile="~/masterpages/IndexPageTemplate.Master" AutoEventWireup="true" CodeBehind="eventDetail.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.mykaneva.events.eventDetail"  %>
<%@ Register TagPrefix="UserControl" TagName="EventBlasts" Src="~/mykaneva/events/EventBlasts.ascx" %>

<asp:Content ID="cntContestHead" runat="server" contentplaceholderid="cph_Body" >
	<script type="text/javascript" src="../../jscript/prototype-1.6.1-min.js"></script>
    <script type="text/javascript" src="../../jscript/doodad/comment.js?v3"></script>
    <script type="text/javascript" src="../../jscript/base/Base.js"></script>

    <NOSCRIPT><META HTTP-EQUIV="refresh" CONTENT="1" URL="noJavascript.aspx" /></NOSCRIPT>

	<script>
		var commentCtrlPath = '<asp:literal id="litCommentsFilePath" runat="server" />';

		function enter(evt) {
			var charCode = (evt.which) ? evt.which : window.event.keyCode;
			if (charCode == 13) { return false; }
		}
	</script>	
	<link href="../../css/base/base_new.css" type="text/css" rel="stylesheet" />
	<link href="../../css/events.css?v2" type="text/css" rel="stylesheet" />
  
	<div class="PageContainer">
	
		<div id="formContainer eventcontainer">
    
			<asp:Panel ID="pnlInvalidAccess" Runat="server" Visible="False" >
				You are not allowed to view this event
			</asp:Panel>

			<asp:Panel ID="pnlNotFound" Runat="server" Visible="False" >
				The event you are trying to view was not found.
			</asp:Panel>

			<asp:panel id="pnlDetail" Runat="server" class="eventdetail" >						
    
				<div class="leftcol">

					<div><asp:image id="imgOwner" runat="server" /></div>          
							
					<div><label>By:</label><asp:HyperLink ID="hlEventOwner" runat="server"></asp:HyperLink></div>

					<div id="divInvitesSent" class="invites" runat="server" visible="false">Note: Invites will be sent on <asp:literal id="litInvitesDate" runat="server"></asp:literal></div>
					<asp:updatepanel id="Updatepanel2" childrenastriggers="true" updatemode="Conditional" visible="true">
						<contenttemplate>
							<div class="attendees" id="divAttendees" runat="server">
								<div>Going (<asp:literal id="litAttendeeCount" runat="server"></asp:literal>)</div>
								<asp:repeater id="rptAttendees" runat="server">
									<itemtemplate>
										<a href="<%# GetPersonalChannelUrl(DataBinder.Eval (Container.DataItem, "Username").ToString ()) %>"><img class="attendee" src="<%# GetProfileImageURL (DataBinder.Eval(Container.DataItem, "ThumbnailSquarePath").ToString (), "sq", DataBinder.Eval(Container.DataItem, "Gender").ToString (), Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "FacebookSettings.UseFacebookProfilePicture")), Convert.ToUInt64(DataBinder.Eval(Container.DataItem, "FacebookSettings.FacebookUserId")) ) %>" title="<%#DataBinder.Eval (Container.DataItem, "Username").ToString ()%>" border="0" /></a>
									</itemtemplate>
								</asp:repeater>
							</div>
						</contenttemplate>
					</asp:updatepanel>
				
				</div>
			    <div class="rightcol">
				    <div class="rccontainer">
					    <h1 style="display:none;">Event Details</h1>
					
					    <div class="name"><asp:label id="lblEventName" runat="server" /></div>	
          					
          			    <p><label>When:</label> <asp:Label id="lblWhen" runat="server"></asp:Label></p>
					    <p><label>Where:</label> <asp:linkbutton id="hlLocation" runat="server"></asp:linkbutton></p>
				    </div>

				    <asp:updatepanel id="Updatepanel1" childrenastriggers="true" updatemode="Conditional" visible="true">
					    <contenttemplate>
						    <div class="buttons">
							    <asp:linkbutton id="btnAttend" class="btnmedium grey" oncommand="ActionButton_Click" commandname="attend" runat="server" text="Attend" style="display:none;"></asp:linkbutton>
							    <asp:linkbutton id="btnDecline" class="btnmedium grey" oncommand="ActionButton_Click" commandname="decline" runat="server" text="Decline" style="display:none;"></asp:linkbutton>
							    <asp:linkbutton id="btnEdit" class="btnmedium grey" runat="server" text="Edit Event" style="display:none;"></asp:linkbutton>
							    <asp:label class="statusmessage" id="lblInviteStatus" runat="server" style="display:none;"></asp:label>
						    </div>
					    </contenttemplate>
				    </asp:updatepanel>

				    <div id="divDescription" class="clearit desc wordwrap" runat="server"></div>
 							
				    <hr class="divide" />
					
				    <asp:updatepanel id="upBlasts" runat="server" childrenastriggers="true" updatemode="conditional" visible="true">
					    <contenttemplate>
						    <usercontrol:EventBlasts runat="server" id="ucEventBlasts" />
					    </contenttemplate>
				    </asp:updatepanel>
				
			    </div>
        												
			</asp:Panel>
		
		</div> <!-- End Form Container -->
	
	</div>
</asp:Content>
