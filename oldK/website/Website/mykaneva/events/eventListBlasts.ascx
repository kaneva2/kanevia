<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="eventListBlasts.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.mykaneva.events.eventListBlasts" %>
<!-- Events -->
<div class="dialog">
<div class="content">
<div class="t"></div>

<h1>Events Calendar<a id="A1" href="~/mykaneva/events/eventForm.aspx" runat="server" class="addBTN nohover png">Create Event</a></h1>

<div id="events">	 

<asp:Label EnableViewState="False" runat="Server" id="lblNoEvents" Visible="False" Text="<br/><center>You have 0 upcoming events.</center>"/>

<!-- Admin Events -->
<asp:repeater id="rptAdminEvents" runat="server" >
<headertemplate><dl id="sticky"><dt>From Kaneva</dt></headertemplate>
<itemtemplate>

<dd>
<ul>
<li><a href="<%# Page.ResolveUrl ("~/mykaneva/blast/blastDetails.aspx?entryId=" + DataBinder.Eval (Container.DataItem, "EntryId").ToString() + "&type=event") %>"><%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "EventName").ToString ()) %></a></li>
<li>When: <%# Convert.ToDateTime(DataBinder.Eval (Container.DataItem, "DateEvent")).ToString("MM/dd/yy h:mm tt") %></li>
<li>Where: <%# DataBinder.Eval (Container.DataItem, "Location").ToString () %></li>
<li>Who: <%# DataBinder.Eval (Container.DataItem, "NameNoSpaces").ToString () %></li>
</ul>
</dd>

</itemtemplate>
<footertemplate></dl></footertemplate>	
</asp:repeater>											   

<!-- Non-Admin Events -->
<asp:repeater id="rptEventsPremium" runat="server" onitemdatabound="rptEvents_ItemDataBound" >
<itemtemplate>

<asp:literal id="litEventSectionEnd" runat="server"></asp:literal>
<asp:literal id="litEventSectionStart" runat="server"></asp:literal>
<%# FormatEventBlast(Convert.ToUInt64(DataBinder.Eval(Container.DataItem, "EntryId")), DataBinder.Eval(Container.DataItem, "DateEvent"), DataBinder.Eval(Container.DataItem, "EventName").ToString(), DataBinder.Eval(Container.DataItem, "Location").ToString(), DataBinder.Eval(Container.DataItem, "NameNoSpaces").ToString(), Convert.ToInt32(DataBinder.Eval(Container.DataItem, "communityId")), (DataBinder.Eval(Container.DataItem, "HighlightCss")).ToString())%>
<asp:HyperLink ID="hlEventEdit" runat="server"></asp:HyperLink>
</itemtemplate>	
</asp:repeater>											   

<asp:literal id="litEventSectionClose" runat="server"></asp:literal>


<!-- Non-Admin Events -->
<asp:repeater id="rptEvents" runat="server" onitemdatabound="rptEvents_ItemDataBound" >
<itemtemplate>

<asp:literal id="litEventSectionEnd" runat="server"></asp:literal>
<asp:literal id="litEventSectionStart" runat="server"></asp:literal>

<%# FormatEventBlast(Convert.ToUInt64(DataBinder.Eval(Container.DataItem, "EntryId")), DataBinder.Eval(Container.DataItem, "DateEvent"), DataBinder.Eval(Container.DataItem, "EventName").ToString(), DataBinder.Eval(Container.DataItem, "Location").ToString(), DataBinder.Eval(Container.DataItem, "NameNoSpaces").ToString(), Convert.ToInt32(DataBinder.Eval(Container.DataItem, "communityId")), (DataBinder.Eval(Container.DataItem, "HighlightCss")).ToString())%>
<asp:HyperLink ID="hlEventEdit" runat="server"></asp:HyperLink>
</itemtemplate>	
</asp:repeater>											   

<asp:literal id="Literal1" runat="server"></asp:literal>


</div>					

</div>
<div class="b"><div></div></div>
</div>