﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EventBlasts.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.mykaneva.events.EventBlasts" %>
				
	<!-- Event Blasts and Comments -->
	<div id="blasts">
		<h2>Event Blasts</h2>
		<div class="blastBox" id="divBlastBox" runat="server">
			<asp:TextBox ID="txtEventBlast" onKeyPress="return enter(event);" TextMode="MultiLine" wrap="true" rows="1" class="textInput" text="Blast to this event..." maxlength="140" runat="server"/>
			<div id="blastError" class="blasterr" runat="server"></div>
			<div class="buttonsComment" id="divButtonsBlast" runat="server">
				<a id="btnAddBlast" class="button_add blast" style="display:none" onserverclick="btnAddBlast_Click" runat="server">Add Blast</a>
			</div><!-- button-->
			<asp:HiddenField id="hidEventIdBlast" runat="server" />
		</div><!-- commentBox-->

		<asp:repeater id="rptEventBlasts" runat="server" onitemdatabound="rptEventBlasts_ItemDataBound" onitemcommand="rptEventBlasts_ItemCommand">
			<itemtemplate>
				<div id='<%# "div" + DataBinder.Eval (Container.DataItem, "EventBlastId").ToString() %>' class="feed">
					<div class="feed-imgTextContainer">
						<div class="feed-imgContainer">
							<div class="feedImg" id="divThumbnail" runat="server">
								<a href="<%# GetPersonalChannelUrl(DataBinder.Eval (Container.DataItem, "Creator.Username").ToString ()) %>"><img src="<%# GetProfileImageURL (DataBinder.Eval(Container.DataItem, "Creator.ThumbnailSquarePath").ToString (), "sq", DataBinder.Eval(Container.DataItem, "Creator.Gender").ToString (), Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "Creator.FacebookSettings.UseFacebookProfilePicture")), Convert.ToUInt64(DataBinder.Eval(Container.DataItem, "Creator.FacebookSettings.FacebookUserId")) ) %>" border="0" /></a>
							</div>
						</div>
						<div class="feed-textContainer">
							<div class="feedText">
								<h4><a href="<%# GetPersonalChannelUrl(DataBinder.Eval (Container.DataItem, "Creator.Username").ToString ()) %>"><%# DataBinder.Eval (Container.DataItem, "Creator.Username").ToString () %></a><span id="spnEventCreated" runat="server">:</span></h4>
								<p id="pComment" runat="server"><%# Server.HtmlDecode(DataBinder.Eval (Container.DataItem, "Comment").ToString ()) %></p> 
						
								<asp:Literal ID="litBlocking" runat="server"></asp:Literal>
						
								<div class="time">(<%# FormatDateTimeSpan (DataBinder.Eval (Container.DataItem, "CreatedDate"), DataBinder.Eval (Container.DataItem, "CurrentDate"))%>) <asp:linkbutton runat="server" id="aDeleteEntry" visible="false" onclientclick="if(!confirm('Are you sure you want to Delete this Entry?')){return false;};" commandargument="" commandname="">Delete</asp:linkbutton></div>
					
							</div><!--feedText-->
							<asp:PlaceHolder ID="phEntryComments" runat="server"></asp:PlaceHolder>
						</div>
					</div>
 					<asp:HiddenField id="hidEventId" runat="server" Value='<%# Convert.ToInt64 (DataBinder.Eval (Container.DataItem, "EventId")) %>' />
					<asp:HiddenField id="hidEventBlastId" runat="server" Value='<%# DataBinder.Eval (Container.DataItem, "EventBlastId").ToString() %>' />
					<asp:HiddenField id="hidNumberOfComments" runat="server" Value='<%# Convert.ToInt64 (DataBinder.Eval (Container.DataItem, "NumberOfComments")) %>' />
				</div>  
					   
			</itemtemplate>
		</asp:repeater>

		<div id="divNoData" class="nodata" runat="server" visible="false">No comments about this event</div>
						
	</div>
