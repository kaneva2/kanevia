///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Collections.Generic;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.mykaneva.events
{
  public partial class eventListBlasts : KlausEnt.KEP.Kaneva.usercontrols.BaseUserControl
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      ShowEvents();
    }

    /// <summary>
    /// ShowEvents
    /// </summary>
    private void ShowEvents()
    {
      BlastFacade blastFacade = new BlastFacade();

      // Load Upcoming Events
      IList<Blast> dtEvents = blastFacade.GetUpcomingEvents(Current_User.UserId, 10);
      IList<Blast> dtAdminEvents = blastFacade.GetAdminEvents();

      if (dtEvents.Count.Equals(0) && dtAdminEvents.Count.Equals(0))
      {
        lblNoEvents.Visible = true;
      }
      else
      {
        if (dtEvents.Count > 0)
        {
          rptEvents.DataSource = dtEvents;
          rptEvents.DataBind();
        }
        if (dtAdminEvents.Count > 0)
        {
          rptAdminEvents.DataSource = dtAdminEvents;
          rptAdminEvents.DataBind();
        }
      }
    }



    protected string FormatEventBlast(UInt64 entryId, object eventDate, string eventName, string location, string creator_username, int communityId, string cssHighLight)
    {
      DateTime dt = (DateTime)eventDate;
      
      string strPreStyle = "";
      if (cssHighLight == "highlight1")
      {
        strPreStyle = "style=\"background:#FAF9A4;\"";
      }

      string strOut = "" +
      "<dd " +strPreStyle + ">" +
          "<ul>";

      if (dt >= DateTime.Now.Date.AddDays(7))
      {
        strOut += "<li class=\"dateStamp\">" + dt.ToString("MMM") + "<span>" + dt.Day.ToString() + "</span></li>";
      }

      string strLocUrl = "channel/" + creator_username + ".people";
      if (communityId != 0)
      {
        strLocUrl = "kgp/playwok.aspx?goto=C" + communityId + "&ILC=HO3D&link=hangoutnow";
      }


      strOut += "<li><a href=\"" + Page.ResolveUrl("~/mykaneva/blast/blastDetails.aspx?entryId=" + entryId) + "&type=event\">" + eventName + "</a></li>" +
          "<li>When: " + dt.ToString("MM/dd/yy h:mm tt") + "</li>" +
          "<li>Where: <a href=\""+ strLocUrl  +"\">" + location + "</a></li>" +
          "<li>Who: <a href=\"channel/" + creator_username + ".people\">" + creator_username + "</a></li>" +
          "</ul>" +
      "</dd>";

      return strOut;
    }

    /// <summary>
    /// Repeater rptEvents ItemDataBound Event Handler
    /// </summary>
    protected void rptEvents_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
    {
      string x = e.Item.ToString();

      // Execute the following logic for Items and Alternating Items.
      if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
      {
        DateTime dt = Convert.ToDateTime(DataBinder.Eval(e.Item.DataItem, "DateEvent"));
       

        if (dt.Date == DateTime.Now.Date)  // Today
        {
          if (_today)
          {
            ((Literal)e.Item.FindControl("litEventSectionStart")).Text = "<dl id=\"today\"><dt>Today</dt>";
            ((Literal)e.Item.FindControl("litEventSectionStart")).Visible = true;
            _today = false;
          }
        }
        else if (dt.Date < DateTime.Now.Date.AddDays(2)) // Tomorrow
        {
          if (!_today)
          {
            ((Literal)e.Item.FindControl("litEventSectionEnd")).Text = "</dl>";
            ((Literal)e.Item.FindControl("litEventSectionEnd")).Visible = true;
            _today = true;
          }

          if (_tomorrow)
          {
            ((Literal)e.Item.FindControl("litEventSectionStart")).Text = "<dl id=\"tomorrow\"><dt>Tomorrow</dt>";
            ((Literal)e.Item.FindControl("litEventSectionStart")).Visible = true;
            _tomorrow = false;
          }
        }
        else if (dt.Date < DateTime.Now.Date.AddDays(7))  // This Week
        {
          if (!_today || !_tomorrow)
          {
            ((Literal)e.Item.FindControl("litEventSectionEnd")).Text = "</dl>";
            ((Literal)e.Item.FindControl("litEventSectionEnd")).Visible = true;
            _today = true;
            _tomorrow = true;
          }

          if (_this_week)
          {
            ((Literal)e.Item.FindControl("litEventSectionStart")).Text = "<dl id=\"thisweek\"><dt>This Week</dt>";
            ((Literal)e.Item.FindControl("litEventSectionStart")).Visible = true;
            _this_week = false;
          }
        }
        else // Upcoming
        {
          if (!_today || !_tomorrow || !_this_week)
          {
            ((Literal)e.Item.FindControl("litEventSectionEnd")).Text = "</dl>";
            ((Literal)e.Item.FindControl("litEventSectionEnd")).Visible = true;
            _today = true;
            _tomorrow = true;
            _this_week = true;
          }

          if (_upcoming)
          {
            ((Literal)e.Item.FindControl("litEventSectionStart")).Text = "<dl id=\"upcoming\"><dt>Upcoming</dt>";
            ((Literal)e.Item.FindControl("litEventSectionStart")).Visible = true;
            _upcoming = false;
          }
        }


        litEventSectionClose.Text = "</dl>";
      }
    }

    private bool _today = true;
    private bool _tomorrow = true;
    private bool _this_week = true;
    private bool _upcoming = true;
    
    private User Current_User = KanevaWebGlobals.CurrentUser;
  }

}