///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Threading;
using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;
using KlausEnt.KEP.Kaneva.framework;
using log4net;


namespace KlausEnt.KEP.Kaneva.mykaneva.events
{
    public partial class eventForm : BasePage
    {

        #region Declerations

        protected LinkButton btnSave;
        private int _userId = 0;
        private int _birthdayUserId = 0;
        private bool _isAdmin = false;
        private bool _isRecurringChanged = false;
        private int _zoneInstanceId = 0;
        private int _zoneType = 0;
        private int _eventFlagPlacementId = 0;
        private bool _isValidFlagEvent = false;
        private int _communityId = 0;

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #endregion Declerations

        private void Page_Load (object sender, System.EventArgs e)
        {
            if (!Request.IsAuthenticated)
            {
                Response.Redirect (this.GetLoginURL ());
            }

            Response.Expires = 0;
            Response.CacheControl = "no-cache";

            //set user id
            UserId = GetUserId();

            // This will always return false now since we are hiding the admin functionality
            IsAdmin = IsAdministrator();

            if (!IsPostBack)
            {
                if (Request.UrlReferrer != null)
                {
                    PreviousHTMLPage = Request.UrlReferrer.ToString ();
                }

                try
                {
                    // Request tracking
                    if (Request.Params[Constants.cREQUEST_TRACKING_URL_PARAM] != null)
                    {
                        int iResult = GetUserFacade.AcceptTrackingRequest (Request.Params[Constants.cREQUEST_TRACKING_URL_PARAM], KanevaWebGlobals.CurrentUser.UserId);

                        // Create a new request for the create event
                        string requestId = GetUserFacade.GetTrackingRequestId ("", Constants.cREQUEST_TRACKING_REQUEST_TYPE_ID_BIRTHDAY_EMAIL_CREATE_PARTY, 1, 0, 0);
                        string requestTypeCreateBirthdayEventId = GetUserFacade.InsertTrackingTypeSent (requestId, Constants.cREQUEST_TRACKING_MESSAGE_TYPE_ID_EMAIL, KanevaWebGlobals.CurrentUser.UserId);
                        RequestTypeCreateBirthdayEventId = requestTypeCreateBirthdayEventId;
                    }
                }
                catch { }

                //determine whether or not to display admin options
                divAdmin.Visible = IsAdmin;
                divNonAdmin.Visible = !IsAdmin;

                BindData ();   
            }
            else
            {
                try
                {
                    // Since custom attributes are not passed back during post back, we will get the values from the array
                    // stored in ViewState and reapply them to the list items.  We will also set the display value for
                    // the currently selected location
                    int[] recurring = ViewState["location_can_sched_recurring"] as int[];
                    if (recurring.Length > 0)
                    {
                        int i = 0;
                        foreach (ListItem li in ddlLocation.Items)
                        {
                            li.Attributes.Add ("rec", recurring[i].ToString ());
                            
                            if (li.Selected)
                            {
                                // For the currently selected world in the list, check to see if that world can
                                // have recurring events or not and set visibility of recurring checkbox
                                cbRecurring.Style.Add ("display", "none"); 
                                if (recurring[i] > 0)
                                {
                                    if (ShowRecurringCheckbox (Convert.ToInt32 (li.Value)))
                                    {
                                        cbRecurring.Style.Add ("display", "block");
                                    }
                                }
                            }
                            i++;
                        }
                    }
                }
                catch { }
            }
        }

        #region Helper Methods

        private void BindData ()
        {
            GetRequestParams();

            // If EventFlagPlacementId > 0 then event request is coming from client and will be
            // associated with an event flag placed in a game world. This page will have different
            // behavior for an event associated with a game event flag
            if (!this.IsValidFlagEvent)
            {
                if (!GetUserWorlds())
                {
                    pnlEdit.Visible = false;
                    pnlCreateWorld.Visible = true;
                    btnCreateWorld.Attributes.Add("href", ResolveUrl("~/community/StartWorld.aspx"));
                    return;
                }
            }

            InitOptions ();
            GetUserBalances ();

            // If event id in querystring, go get that event
            if (this.EventID > 0)
            {
                if (!LoadEvent ())
                {
                    // User not allowed to edit event so just load with default values for new event
                    SetDefaultEventValues ();
                }
                litCalendarPlusDays.Text = "0";
            }
            else // load default values
            {
                SetDefaultEventValues ();
            }
        }

        public bool LoadEvent ()
        {
            try
            {
                Event evt = GetEventFacade.GetEvent (this.EventID);

                // Add event id to viewstate so we can use it to update this event
                // if user makes changes
                if (evt.EventId > 0)
                {
                    this.CommunityId = evt.CommunityId;

                    if (this.CommunityId == 0)
                    {
                        if (evt.UserId != UserId)
                        {
                            return false;
                        }
                    }
                    else if (!UserCanEditEvent(evt, this.CommunityId)) // Verify user can see this event to edit it
                    {
                        return false;
                    }
                }
                else
                {
                    // did not get event from db, so return false
                    return false;
                }

                if (evt.StatusId == (int) Event.eSTATUS_TYPE.CANCELLED)
                {   // Event was cancelled
                    return false;
                }
                else if (evt.EndTime < DateTime.Now)
                {   // Event has ended
                    return false;
                }

                Community community = GetCommunityFacade.GetCommunity(this.CommunityId);
                // Check to make sure the current user is the owner of the World
                // or they are the owner of the event
                if (!community.CreatorId.Equals(UserId) &&
                    !evt.UserId.Equals(UserId))
                {
                    return false;
                }

                txtTitle.Text = Server.HtmlDecode (evt.Title);
                txtDescription.Text = Server.HtmlDecode (evt.Details);

                // Get start date and time
                DateTime dtStart = evt.StartTime;
                txtEventStart.Text = dtStart.ToShortDateString ();
                SetDropDownSelection (ddlHourStart, dtStart.Hour.ToString ());
                SetDropDownSelection (ddlMinuteStart, dtStart.Minute.ToString ());

                // Get end date and time
                DateTime dtEnd = evt.EndTime;
                txtEventEnd.Text = dtEnd.ToShortDateString ();
                SetDropDownSelection (ddlHourEnd, dtEnd.Hour.ToString ());
                SetDropDownSelection (ddlMinuteEnd, dtEnd.Minute.ToString ());

                if (evt.UserId.Equals(community.CreatorId))
                {
                    SetDropDownSelection(ddlLocation, Server.HtmlDecode(this.CommunityId.ToString()));
                    ddlLocation.Enabled = false;
                }
                else
                {
                    litCalendarPlusDays.Text = "2";
                    eventHeadline.InnerText = "Create " + community.Name + " Event";
                    lblWorldName.InnerText = community.Name;
                    lblWorldName.Visible = true;
                    ddlLocation.Visible = false;
                    lblWorld.InnerText = "World";
                    hidLocationRec.Value = community.CommunityId.ToString();
                    hidLocationName.Value = community.Name;
                    this.EventFlagPlacementId = evt.ObjPlacementId;
                }

                // Add Values to Viewstate so we can compare on Save to see if anything changed
                ViewState.Add ("EventId", EventID);
                ViewState.Add ("StartTime", dtStart);
                ViewState.Add ("Details", txtDescription.Text);
                ViewState.Add("CommunityId", this.CommunityId);

                cbRecurring.Style.Add ("display", "none");
                divInvites.Visible = false;

                // Set the Premium value.  If editing and premium was already selected,
                // then disable the premium selection value.
                if (evt.IsPremium)
                {
                    //disable all the radio buttons
                    rbBasic.Checked = false;
                    rbBasic.Disabled = true;

                    rbPremium.Checked = true;
                    rbPremium.Disabled = true;

                    lblBasic.Disabled = true;
                    lblPremium.Disabled = true;

                    rblPremCurrency.Enabled = false;
                    basiccontain.Visible = false;
                }
                else
                {
                    rblPremCurrency.Enabled = true;
                    rblPremCurrency.Visible = true;
                    rblPremCurrency.Items[0].Selected = true;
                    
                    if (evt.IsRecurring || GetCommunityFacade.GetCommunity (evt.CommunityId).Stats.NumberOfMembers >= KanevaGlobals.WorldMemberCountToEnableRecurringEvents)
                    {
                        // Check to see if we are running in test mode
                        if (ShowRecurringCheckbox (evt.CommunityId))
                        {
                            cbRecurring.Style.Add ("display", "block");
                        }

                        if (evt.RecurringStatusId == (int) Event.eRECURRING_STATUS_TYPE.CREATED)
                        {
                            spnInvitesSentDate.InnerText = evt.StartTime.Date.AddDays (-3).ToString ("dddd, MMMM d, yyyy");
                            divInvites.Visible = true;
                        }
                    }
                }

                cbBasicFriends.Visible = false;
                cbPremFriends.Visible = false;

                this.IsValidFlagEvent = evt.ObjPlacementId > 0;

                // Enable the delete button
                btnDelete.Visible = true;
                // If this is an event tied to an event flag in-world, the user must delete the event flag
                // to remove the event so we disable the delete button and display an information message
                if (this.IsValidFlagEvent)
                {
                    btnDelete.Attributes.Add("class", "grey btnsmall disable padright");
                    btnDelete.Attributes.Add("href", "javascript:void(0);");
                    btnDelete.Style.Add("cursor", "default");
                    deleteFlagEventMessage.Visible = true;
                }
                btnSave.Text = "Save";

                cbRecurring.Checked = evt.RecurringStatusId != (int)Event.eRECURRING_STATUS_TYPE.NONE;

                // If event has already started, then disable the date and time controls
                if (dtStart.CompareTo (DateTime.Now) < 0)
                {
                    txtTitle.Enabled = false;
                    txtDescription.Enabled = false;

                    // enable all start/end controls
                    txtEventStart.Enabled = false;
                    ddlHourStart.Enabled = false;
                    ddlMinuteStart.Enabled = false;
                    txtEventEnd.Enabled = false;
                    ddlHourEnd.Enabled = false;
                    ddlMinuteEnd.Enabled = false;
                    cal1Container.Visible = false;

                    rbBasic.Disabled = true;
                    lblBasic.Disabled = true;
                    rbPremium.Disabled = true;
                    lblPremium.Disabled = true;
                    rblPremCurrency.Enabled = false;

                    btnDelete.Visible = false;
                    btnSave.Visible = false;
                }

                return true;
            }
            catch { return false; }
        }

        private int SaveEvent (int eventId, ref bool eventHasChanged)
        {
            try
            {
                // Check for injection scripts
                if (KanevaWebGlobals.ContainsInjectScripts (txtTitle.Text) ||
                    KanevaWebGlobals.ContainsInjectScripts (txtDescription.Text))
                {
                    m_logger.Warn ("User " + UserId.ToString () + " tried to script from IP " + Common.GetVisitorIPAddress());
                    ShowErrorMessage ("Your input contains invalid scripting, please remove script code and try again.");
                    return 0;
                }

                if (txtTitle.Text.Trim ().Length == 0)
                {
                    ShowErrorMessage ("Event Name is a required field.", ErrorType.Name);
                    return 0;
                }

                if (this.CommunityId == 0)
                {
                    ShowErrorMessage("Invalid world. Can not create event.", ErrorType.World);
                    return 0;
                }

                // Check to see if creator is above fame level 14
                FameFacade fameFacade = new FameFacade();
                UserFame userFame = fameFacade.GetUserFame(UserId, (int)FameTypes.World);

                if (userFame.CurrentLevel.LevelNumber < 14)
                {
                    ShowErrorMessage("Sorry, you must be at least world fame level 14  to create an event");
                    return 0;
                }

                // Basic Details
                string title = txtTitle.Text.Trim ();
                string desc = txtDescription.Text.Trim ();

                //check for bad or reserved words
                if (!IsAdmin)
                {
                    if (KanevaWebGlobals.isTextRestricted (title, Constants.eRESTRICTION_TYPE.ALL))
                    {
                        ShowErrorMessage ("Your event title has restricted words in it please correct and re-submit.");
                        return 0;
                    }
                    if (KanevaWebGlobals.isTextRestricted (desc, Constants.eRESTRICTION_TYPE.ALL))
                    {
                        ShowErrorMessage ("Your event description has restricted words in it please correct and re-submit.");
                        return 0;
                    }
                }

                bool isPrivate = false;
                // Privacy -- takes same setting as community
                if (!CommunityUtility.IsCommunityPublic(this.CommunityId))
                {
                    isPrivate = true;
                }

                // Type
                int eventTypeId = 2; //party - we don't use this currently so just use 2 as default 

                // When 
                string[] strStart = txtEventStart.Text.Split ('/');

                DateTime startTime = new DateTime (
                    Convert.ToInt32 (strStart[2]),
                    Convert.ToInt32 (strStart[0]),
                    Convert.ToInt32 (strStart[1]),
                    Convert.ToInt32 (ddlHourStart.SelectedItem.Value),
                    Convert.ToInt32 (ddlMinuteStart.SelectedItem.Value), 0);

                /*
                string[] strEnd = txtEventEnd.Text.Split ('/');
              
                DateTime endTime = new DateTime (
                   Convert.ToInt32 (strEnd[2]),
                   Convert.ToInt32 (strEnd[0]),
                   Convert.ToInt32 (strEnd[1]),
                   Convert.ToInt32 (ddlHourEnd.SelectedItem.Value),
                   Convert.ToInt32 (ddlMinuteEnd.SelectedItem.Value), 0);
                */
                DateTime endTime = startTime.AddHours (1);

                if (startTime >= endTime)
                {
                    ShowErrorMessage ("Ending time must be after start time", ErrorType.EndTime);
                    return -1;
                }

                // Give it a 15 minute buffer since this is using web clock vs db!!!!!!
                if (startTime < DateTime.Now.AddMinutes (-15) || endTime < DateTime.Now)
                {
                    ShowErrorMessage ("Start date must be in the future", ErrorType.StartTime);
                    return -1;
                }

                // Check against concurrency rules for the event date only, not recurring check
                // Has to happen before we charge, and before we save!!
                if (!IsAdmin)
                {
                    bool isRecurring = false;
                    try
                    {
                        isRecurring = Convert.ToInt32 (hidLocationRec.Value) == 0 ? false : cbRecurring.Checked && rbBasic.Checked;
                    }
                    catch { }

                    // Check to see if we have any conflicts
                    DateTime conflictDate = new DateTime ();
                    int ConcurrentCheck = GetEventFacade.ConcurrencyCheckRecurring(eventId, KanevaWebGlobals.CurrentUser.UserId, this.CommunityId, startTime, endTime, isRecurring, 0, ref conflictDate);
                    switch (ConcurrentCheck)
                    {
                        case ((int) Event.eCONCURRENCY_ERROR.GENERAL):
                            {
                                m_logger.Error("Concurrency rule check caused error: UserId=" + UserId.ToString() + " : communityId=" + this.CommunityId);
                                ShowErrorMessage ("There can only be one event at a time in a particular zone.");
                                return 0;
                            }

                        case ((int) Event.eCONCURRENCY_ERROR.TIME_OVERLAP):
                            {
                                string errMsg = "Only 1 event per World per hour can be scheduled.";
                                if (isRecurring && conflictDate > DateTime.MinValue)
                                {
                                    errMsg = "The recurring event on " + conflictDate.ToString ("dddd, MMMM d") + " cannot be created due to an event conflict on that day.";
                                }
                                ShowErrorMessage (errMsg);
                                return 0;
                            }

                        case ((int) Event.eCONCURRENCY_ERROR.EXCEED_MAX_EVENTS_PER_DAY):
                            {
                                string errMsg = "Only 2 events per day are allowed per World.";
                                if (isRecurring && conflictDate > DateTime.MinValue)
                                {
                                    errMsg = "The recurring event on " + conflictDate.ToString ("dddd, MMMM d") + " cannot be created due to an event conflict on that day.";
                                }
                                ShowErrorMessage (errMsg);
                                return 0;
                            }

                        case ((int) Event.eCONCURRENCY_ERROR.EXCEED_MAX_WORLD_EVENTS_PER_DAY):
                            {
                                string errMsg = "This world has reached it's maximum number of events for the day.";
                                ShowErrorMessage(errMsg);
                                return 0;
                            }
                    }
                }

                // Premium Status
                // If existing event is already set to premium, then keep that value, if not,
                // then use the value selected from the premium radio buttons
                int premium = (int) Event.ePRIORITY.NORMAL;
                bool currEventIsPremium = false;
                Event evt = new Event ();

                if (eventId > 0)
                {
                    try
                    {
                        evt = GetEventFacade.GetEvent (eventId);
                        if (evt.EventId > 0)
                        {
                            premium = (evt.IsPremium || rbPremium.Checked) ? 1 : 0;
                            currEventIsPremium = evt.IsPremium;
                        }
                    }
                    catch { }
                }

                //administrators dont pay and get a premium setting to indicate they are admins
                if (IsAdmin)
                {
                    premium = (int) Event.ePRIORITY.ADMINISTRATOR;
                }
                // If new event or existing event and previous value was 'not premium' event
                else if ((rbPremium.Checked && eventId == 0) || (rbPremium.Checked && !currEventIsPremium))
                {
                    //predetermine if they have enough creditsor rewards based on there selected KPOIN type
                    if (rblPremCurrency.SelectedValue.Equals ("GPOINT"))
                    {
                        if (AvailableRewards < 200.0)
                        {
                            ShowErrorMessage ("Not enough rewards.");
                            return -1;
                        }
                    }
                    else
                    {
                        if (AvailableCredits < 200.0)
                        {
                            ShowErrorMessage ("Not enough credits.");
                            return -1;
                        }
                    }

                    //try to purchase it
                    if ((eventId == 0 && (rbPremium.Checked)) || (eventId > 0 && !currEventIsPremium))
                    {
                        int result = GetEventFacade.ChargeForPremiumEvent (UserId, rblPremCurrency.SelectedValue);

                        if (result != 1)
                        {
                            if (result == -1)
                            {
                                string msg = "Not enough ";
                                msg += rblPremCurrency.SelectedValue.Equals (Constants.CURR_REWARDS) ? " rewards." : " credits."; 
                                ShowErrorMessage (msg);
                                return -1;
                            }
                            else
                            {
                                ShowErrorMessage ("Error setting event to premium.");
                                return -1;
                            }
                        }
                        else
                        {
                            premium = (int) Event.ePRIORITY.PREMIUM;
                        }
                    }
                }

                string name = ddlLocation.Visible ? ddlLocation.SelectedItem.Text : hidLocationName.Value;
                
                // Create or update the event object 
                evt.CommunityId = this.CommunityId;
                evt.UserId = UserId;
                evt.Title = Server.HtmlEncode (title);
                evt.Details = Server.HtmlEncode (desc);
                evt.Location = Server.HtmlEncode (name);
                evt.StartTime = startTime;
                evt.EndTime = endTime;
                evt.Priority = premium;
                evt.TypeId = eventTypeId;
                evt.IsPrivate = isPrivate;
                evt.InviteFriends = rbBasic.Checked ? cbBasicFriends.Checked : cbPremFriends.Checked;
                evt.ObjPlacementId = this.EventFlagPlacementId;

                // Update existing event if we have event id
                if (eventId > 0)
                {
                    if (evt.RecurringStatusId == (int) Event.eRECURRING_STATUS_TYPE.NONE && cbRecurring.Checked)
                    {
                        evt.RecurringStatusId = (int) Event.eRECURRING_STATUS_TYPE.INVITES_SENT;
                    }
                    else if ((evt.RecurringStatusId == (int) Event.eRECURRING_STATUS_TYPE.CREATED ||
                              evt.RecurringStatusId == (int) Event.eRECURRING_STATUS_TYPE.INVITES_SENT) && 
                        !cbRecurring.Checked)
                    {
                        evt.RecurringStatusId = (int) Event.eRECURRING_STATUS_TYPE.NONE;
                    }
                    // Premium events can not be recurring
                    if (evt.IsPremium)
                    {
                        evt.RecurringStatusId = (int) Event.eRECURRING_STATUS_TYPE.NONE;   
                    }
                    
                    GetEventFacade.UpdateEvent (evt);
                }
                else // new event
                {
                    // Metrics tracking for accepts
                    Community community = GetCommunityFacade.GetCommunity(this.CommunityId);

                    string requestId = GetUserFacade.InsertTrackingRequest(Constants.cREQUEST_TRACKING_REQUEST_TYPE_ID_EVENT_INVITE, KanevaWebGlobals.CurrentUser.UserId, 0, 0);
                    string requestTypeSentIdEvent = GetUserFacade.InsertTrackingTypeSent (requestId, Constants.cREQUEST_TRACKING_MESSAGE_TYPE_ID_WEBSITE, community.WOK3App.GameId);

                    evt.RecurringStatusId = cbRecurring.Checked ? (int) Event.eRECURRING_STATUS_TYPE.INVITES_SENT : (int) Event.eRECURRING_STATUS_TYPE.NONE;
                    // Premium events can not be recurring
                    if (evt.IsPremium)
                    {
                        evt.RecurringStatusId = (int) Event.eRECURRING_STATUS_TYPE.NONE;
                    }

                    eventId = GetEventFacade.InsertEvent (evt, requestTypeSentIdEvent);
                }

                if (eventId > 0)
                {
                    evt = GetEventFacade.GetEvent (eventId);
                    
                    if (EventHasChanged (evt))
                    {
                        eventHasChanged = true;
                    }

                    try
                    {
                        // If event is associated with event flag
                        if (evt.ObjPlacementId > 0)
                        {
                            PagedDataTable users = GetUsersInEventWorld(GetCommunityFacade.GetCommunity(evt.CommunityId));
                            if (users.Rows.Count > 0)
                            {
                                XmlEventCreator xml = new XmlEventCreator("ClientTickler");
                                //first encode packet information
                                xml.AddValue((int)NotificationsProfile.NotificationTypes.EventCreated);
                                xml.AddValue(evt.EventId);
                                xml.AddValue(evt.Title);
                                xml.AddValue(evt.Details);
                                xml.AddValue(evt.StartTime.ToString());
                                xml.AddValue(evt.EndTime.ToString());
                                xml.AddValue(evt.CommunityId);
                                xml.AddValue(evt.ObjPlacementId);
                                xml.AddValue(evt.StatusId);
                                xml.AddValue(eventHasChanged ? "False" : "True"); // Is this a new event?
            
                                RemoteEventSender sender = new RemoteEventSender();

                                for (int i = 0; i < users.Rows.Count; i++)
                                {
                                    xml.AddPlayer(Convert.ToInt32(users.Rows[i]["player_id"]));
                                    sender.BroadcastEventToServer(xml.GetXml()); //send event to all servers
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        m_logger.Error("Error Broadcasting Event Created event to WoK Server ", e);
                    }
                }

                return eventId;
            }
            catch (Exception exc)
            {
                m_logger.Error ("Error saving event: UserId=" + UserId.ToString () + " : ", exc);
                ShowErrorMessage ("An error occurred while trying to save the event.");
                return 0;
            }
        }

        private void PrepopulateEventValues ()
        {
            try
            {
                User birthdayUser = GetUserFacade.GetUser (this.BirthdayUserId);

                if (birthdayUser.UserId > 0)
                {
                    // Let's do some validation
                    // Verify the person creating the event is friends with the person having the birthday,
                    // mainly because this could be a back door way to find out a person's birthday
                    if (!GetUserFacade.AreFriends (KanevaWebGlobals.CurrentUser.UserId, birthdayUser.UserId))
                    {
                        return;
                    }
                    
                    DateTime birthDate = Convert.ToDateTime(birthdayUser.BirthDate);
                    DateTime startTime = new DateTime (DateTime.Now.Year, birthDate.Month, birthDate.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
                    // Verify the persons birthdate is within the next 2 weeks
                    if (DateTime.Now.AddDays (14) < startTime)
                    {
                        return;
                    }
                    
                    // Set Event Title
                    txtTitle.Text = birthdayUser.Username + "'s Birthday Party";

                    // Set Event Description
                    txtDescription.Text = "It's time to celebrate " + birthdayUser.Username + "'s birthday!";

                    // Set the Event date to users birthdate
                    DateTime endTime = startTime.AddMinutes (60);

                    txtEventStart.Text = startTime.Month.ToString () + "/" + startTime.Day.ToString () + "/" + startTime.Year.ToString ();
                    txtEventEnd.Text = endTime.Month.ToString () + "/" + endTime.Day.ToString () + "/" + endTime.Year.ToString ();
                }
            }
            catch {}
        }

        private void SetDefaultEventValues ()
        {
            try
            {
                // From a community
                if (Request.Params["communityId"] != null)
                {
                    this.CommunityId = Convert.ToInt32(Request.Params["communityId"]);
                }
                // If community id was not passed in then get the community id from the zoneInstanceId
                // and ZoneType if they were passed in via querystring
                else if (this.IsValidFlagEvent)
                {
                    this.CommunityId = GetCommunityFacade.GetCommunityIdFromWorldIds(0, 0, 0, this.ZoneInstanceId, this.ZoneType);
                }
            }
            catch { }

            Community community = new Community();
            try
            {
                if (this.CommunityId > 0)
                {
                    community = GetCommunityFacade.GetCommunity(this.CommunityId);
                }
            }
            catch { }

            DateTime startTime = DateTime.Now;
            DateTime endTime = DateTime.Now.AddMinutes (30);

            txtEventStart.Text = startTime.Month.ToString () + "/" + startTime.Day.ToString () + "/" + startTime.Year.ToString ();

            //requirements specified a default of 8PM 
            SetDropDownSelection (ddlHourStart, "20");
            SetDropDownSelection (ddlMinuteStart, "00");

            txtEventEnd.Text = endTime.Month.ToString () + "/" + endTime.Day.ToString () + "/" + endTime.Year.ToString ();

            //no event can have a start and end date that is the same so end time is required 8pm plus 30 minutes
            SetDropDownSelection (ddlHourEnd, "20");
            SetDropDownSelection (ddlMinuteEnd, "30");

            // If event is for event flag and the user creating the event is NOT the owner
            // of the Game World, then set the calendar so event can only be created up to 2 days in the future
            if (this.IsValidFlagEvent && community.CommunityId > 0)
            {
                litCalendarPlusDays.Text = "2";
                eventHeadline.InnerText = "Create " + community.Name + " Event";
                lblWorldName.InnerText = community.Name;
                lblWorldName.Visible = true;
                ddlLocation.Visible = false;
                lblWorld.InnerText = "World";
                hidLocationRec.Value = community.CommunityId.ToString();
                hidLocationName.Value = community.Name;
                if (community.CreatorId.Equals(UserId))
                {
                    litCalendarPlusDays.Text = "14";
                }
            }
            else
            {
                litCalendarPlusDays.Text = "14";
                SetDropDownSelection(ddlLocation, this.CommunityId.ToString());
            }

            // Only world owner can crate recurring events for this world
            if (community.CommunityId > 0 && community.CreatorId.Equals(UserId))
            {
                if (community.Stats.NumberOfMembers >= KanevaGlobals.WorldMemberCountToEnableRecurringEvents)
                {
                    if (ShowRecurringCheckbox(this.CommunityId))
                    {
                        cbRecurring.Style.Add ("display", "block");
                    }
                }
            }

            //only do if the user is an admin
            if (IsAdmin)
            {
                txtAdminDate.Text = startTime.Month.ToString() + "/" + startTime.Day.ToString() + "/" + startTime.Year.ToString();
                SetDropDownSelection(ddlAdminEndHour, "20");
                SetDropDownSelection(ddlAdminEndMinute, "30");
            }

            // If this is for a birthday party then we need to override some default values
            if (this.BirthdayUserId > 0)
            {
                PrepopulateEventValues ();   
            }

            if (this.CommunityId == 0)
            {
                divInvites.InnerHtml = "";
            }
        }

        private void SetDropDownSelection (DropDownList list, string selection)
        {
            ListItem listItemEventType = list.Items.FindByValue (selection);
            if (listItemEventType != null)
            {
                list.SelectedIndex = list.Items.IndexOf (listItemEventType);
            }
        }

        private void InitOptions ()
        {
            ddlHourStart.DataSource = Constants.OPT_HOUR;
            ddlHourStart.DataValueField = "Key";
            ddlHourStart.DataTextField = "Value";
            ddlHourStart.DataBind ();

            ddlMinuteStart.DataSource = Constants.OPT_MINUTE;
            ddlMinuteStart.DataValueField = "Key";
            ddlMinuteStart.DataTextField = "Value";
            ddlMinuteStart.DataBind ();

            ddlHourEnd.DataSource = Constants.OPT_HOUR;
            ddlHourEnd.DataValueField = "Key";
            ddlHourEnd.DataTextField = "Value";
            ddlHourEnd.DataBind ();

            ddlMinuteEnd.DataSource = Constants.OPT_MINUTE;
            ddlMinuteEnd.DataValueField = "Key";
            ddlMinuteEnd.DataTextField = "Value";
            ddlMinuteEnd.DataBind ();

            ddlAdminEndHour.DataSource = Constants.OPT_HOUR;
            ddlAdminEndHour.DataValueField = "Key";
            ddlAdminEndHour.DataTextField = "Value";
            ddlAdminEndHour.DataBind();

            ddlAdminEndMinute.DataSource = Constants.OPT_MINUTE;
            ddlAdminEndMinute.DataValueField = "Key";
            ddlAdminEndMinute.DataTextField = "Value";
            ddlAdminEndMinute.DataBind();
        }
                                     
        private bool GetUserWorlds ()
        {
            int[] communityTypeIds = { Convert.ToInt32 (CommunityType.APP_3D), Convert.ToInt32 (CommunityType.COMMUNITY), Convert.ToInt32 (CommunityType.HOME) };
            PagedList<Community> worlds = GetCommunityFacade.GetUserCommunities (UserId, communityTypeIds, "c.name ASC", "c.creator_id=" + KanevaWebGlobals.CurrentUser.UserId + " AND place_type_id<99", 1, 500, true);

            if (worlds.Count == 0)
            {
                return false;
            }

            // This array will hold the values for the "rec" attribute that we are adding to each list item. If rec=1 then
            // the community can have recurring events, rec=0 means no recurring event for community. Based on number
            // of members for that community
            int[] recurring = new int[worlds.Count];
            int i = 0;

            // For each world we add to the list we also add the attribute of rec= which we will use
            // on the UI to know whether to display the recurring checkbox
            foreach (Community c in worlds)
            {
                string name = c.Name;
                if (c.CommunityTypeId == (int)CommunityType.HOME)
                {
                    name = c.Name + " (Home World)";
                }
                
                ListItem li = new ListItem (name, c.CommunityId.ToString());

                string rec = c.Stats.NumberOfMembers >= KanevaGlobals.WorldMemberCountToEnableRecurringEvents ? "1" : "0";
                if (!ShowRecurringCheckbox (c.CommunityId))
                {
                    rec = "0";
                }

                li.Attributes.Add ("rec", rec);
                ddlLocation.Items.Add (li);

                // Set the visibility of the recurring checkbox based on value of first item in list
                if (ddlLocation.Items.Count == 1 && c.Stats.NumberOfMembers >= KanevaGlobals.WorldMemberCountToEnableRecurringEvents)
                {
                    if (ShowRecurringCheckbox (c.CommunityId))
                    {
                        cbRecurring.Style.Add ("display", "block");
                    }
                }
            
                // Add values to the array
                recurring[i] = c.Stats.NumberOfMembers >= KanevaGlobals.WorldMemberCountToEnableRecurringEvents ? 1 : 0;
                i++;
            }

            // Store array in ViewState. .Net does not pass the "rec" custom attribute back in the post back.  We will store
            // these values in ViewState and pull them back on PostBack.  We need this if the event fails a validation check
            // and the error message is displayed, after the post back the locations drop-down list loses this custom attribute
            ViewState["location_can_sched_recurring"] = recurring;
            return true;
        }

        // Checks to see if recurring events is running in test mode
        private bool ShowRecurringCheckbox (int communityId)
        {
            if (KanevaGlobals.RecurringEventsTestMode)
            {
                if (communityId == KanevaGlobals.RecurringEventsTestCommunityId)
                {
                    return true;
                }
                return false;
            }
            return true;
        }

        private void GetUserBalances()
        {
            AvailableCredits = UsersUtility.getUserBalance(UserId, Constants.CURR_KPOINT);
            AvailableRewards = UsersUtility.getUserBalance(UserId, Constants.CURR_GPOINT);
            divCredits.InnerText = AvailableCredits < 1 ? "0" : "Available Credits: " + AvailableCredits.ToString ("#,###");
            divRewards.InnerText = AvailableRewards < 1 ? "0" : "Available Rewards: " + AvailableRewards.ToString ("#,###");
        }

        private bool EventHasChanged (Event e)
        {
            try
            {
                DateTime startTime = Convert.ToDateTime (ViewState["StartTime"]);
                string details = ViewState["Details"].ToString ().ToLower ().Trim ();
                int communityId = Convert.ToInt32 (ViewState["CommunityId"]);

                if (e.EventId == Convert.ToInt32 (ViewState["EventId"]) &&
                    (e.StartTime != startTime ||
                     Server.HtmlDecode (e.Details.Trim ().ToLower ()) != details ||
                     e.CommunityId != communityId)
                   )
                {
                    return true;
                }
            }
            catch { }

            return false;
        }

        private bool UserCanEditEvent (Event evt, int communityId)
        {
            try 
            {
                if (IsAdmin)
                {
                    return true;
                }

                bool canEdit = false;

                // Verify user can see this event to edit it
                DataRow drCommunity = CommunityUtility.GetCommunity (communityId);

                if (drCommunity != null)
                {
                    bool isMod = CommunityUtility.IsCommunityModerator(communityId, UserId);// ||
                    bool isMem = (Convert.ToInt32(drCommunity["allow_member_events"]) == 1 &&
                        CommunityUtility.IsActiveCommunityMember(communityId, UserId));
                    bool isEventOwner = evt.UserId.Equals(UserId);

                    return (isMod || isMem || isEventOwner);
                }

                return canEdit;
            }
            catch (Exception exc)
            {
                m_logger.Error ("Error checking if user has access to edit event: UserId=" +
                    UserId.ToString() + " : EventId=" + evt.EventId +
                    " CommunityId=" + communityId + " : ", exc);
                return false;
            }
        }

        private void ShowErrorMessage (string err)
        {
            ShowErrorMessage (err, ErrorType.Other);
        }
        private void ShowErrorMessage (string err, ErrorType et)
        {
            string errClassname = "error";

            switch (et)
            {
                case (ErrorType.Name):
                    {
                        lblEventName.Attributes["class"] = errClassname;
                        txtTitle.Attributes["class"] = errClassname;
                        break;
                    }
                
            }
            
            cvBlank.IsValid = false;
            cvBlank.ErrorMessage = err;
        }

        private void SendInvites (int eventId, bool includeFriends, int currentUserId)
        {
            Event evt = GetEventFacade.GetEvent (eventId);

            // Add the Invitees to the event_invites table
            GetEventFacade.InsertEventInvitees (evt.CommunityId, evt.UserId, evt.EventId,
                (int) EventInvitee.Invite_Status.INVITED, includeFriends);

            // Get members of world in which event is being held
            PagedList<EventInvitee> invitees = GetEventFacade.GetEventInviteesByStatus (evt.EventId,
                (int) EventInvitee.Invite_Status.INVITED, "", 1, int.MaxValue);

            // Send the mail/kmail invites
            SendMessages (invitees, Event.eMESSAGE_TYPE.EventInvite, evt, GetUserFacade.GetUser(currentUserId));
        }

        private void SendEventChangedNotifications (int eventId, int currentUserId)
        {
            Event evt = GetEventFacade.GetEvent (eventId);
            
            // Get all users that have already accepted original invite and
            // send them a summary email of the changed
            PagedList<EventInvitee> invitees = GetEventFacade.GetEventInviteesByStatus (evt.EventId,
                (int)EventInvitee.Invite_Status.ACCEPTED, "", 1, int.MaxValue);

            // Send the event has changed notification email/kmail
            SendMessages (invitees, Event.eMESSAGE_TYPE.EventChangeNotification, evt, GetUserFacade.GetUser (currentUserId));


            // Get all users that have not responded to invite and send them
            // a new invite with the updated information
            invitees = GetEventFacade.GetEventInviteesByStatus (evt.EventId,
                (int) EventInvitee.Invite_Status.INVITED, "", 1, int.MaxValue);

            // Send the event has changed invite email/kmail to users that have not
            // accepted nor denied the original invite for the event
            SendMessages (invitees, Event.eMESSAGE_TYPE.EventChangeInvite, evt, GetUserFacade.GetUser (currentUserId));
        }

        private void SendMessages (PagedList<EventInvitee> invitees, Event.eMESSAGE_TYPE emt, Event evt, User CurrentUser)
        {
            if (invitees.Count > 0)
            {
                Community community = GetCommunityFacade.GetCommunity(evt.CommunityId);
                int evtGameId = community.WOK3App.GameId;

                Message message = new Message ();
                
                // only send emails if World owner is owner of event
                if (evt.UserId.Equals(community.CreatorId))
                {
                    // Send The emails/kmails
                    foreach (EventInvitee ei in invitees)
                    {
                        string requestId = string.Empty;
                        string requestTypeSentIdPM = string.Empty;
                        string requestTypeSentIdEmail = string.Empty;

                        // Determine appropriate tracking metric and email type
                        switch (emt)
                        {
                            case Event.eMESSAGE_TYPE.EventInvite:
                                requestId = GetUserFacade.GetTrackingRequestId(requestId, Constants.cREQUEST_TRACKING_REQUEST_TYPE_ID_EVENT_INVITE, CurrentUser.UserId, evtGameId, evt.CommunityId);
                                requestTypeSentIdPM = GetUserFacade.InsertTrackingTypeSent(requestId, Constants.cREQUEST_TRACKING_MESSAGE_TYPE_ID_PM, ei.UserId);

                                if (ei.EmailNotifications && ei.UserStatusId.Equals((int)Constants.eUSER_STATUS.REGVALIDATED))
                                {
                                    requestTypeSentIdEmail = GetUserFacade.InsertTrackingTypeSent(requestId, Constants.cREQUEST_TRACKING_MESSAGE_TYPE_ID_EMAIL, ei.UserId);

                                    MailUtilityWeb.SendEventEmail(evt, Event.eMESSAGE_TYPE.EventInvite.ToString(), ei.Email, ei.UserId, CurrentUser.UserId, string.Empty, requestTypeSentIdEmail);
                                }
                                break;
                            case Event.eMESSAGE_TYPE.EventChangeNotification:
                                requestId = GetUserFacade.GetTrackingRequestId(requestId, Constants.cREQUEST_TRACKING_REQUEST_TYPE_ID_EVENT_UPDATE, CurrentUser.UserId, evtGameId, evt.CommunityId);
                                requestTypeSentIdPM = GetUserFacade.InsertTrackingTypeSent(requestId, Constants.cREQUEST_TRACKING_MESSAGE_TYPE_ID_PM, ei.UserId);

                                if (ei.EmailNotifications && ei.UserStatusId.Equals((int)Constants.eUSER_STATUS.REGVALIDATED))
                                {
                                    requestTypeSentIdEmail = GetUserFacade.InsertTrackingTypeSent(requestId, Constants.cREQUEST_TRACKING_MESSAGE_TYPE_ID_EMAIL, ei.UserId);

                                    MailUtilityWeb.SendEventEmail(evt, Event.eMESSAGE_TYPE.EventChangeNotification.ToString(), ei.Email, ei.UserId, CurrentUser.UserId, string.Empty, requestTypeSentIdEmail);
                                }
                                break;
                            case Event.eMESSAGE_TYPE.EventChangeInvite:
                                requestId = GetUserFacade.GetTrackingRequestId(requestId, Constants.cREQUEST_TRACKING_REQUEST_TYPE_ID_EVENT_INVITE, CurrentUser.UserId, evtGameId, evt.CommunityId);
                                requestTypeSentIdPM = GetUserFacade.InsertTrackingTypeSent(requestId, Constants.cREQUEST_TRACKING_MESSAGE_TYPE_ID_PM, ei.UserId);

                                if (ei.EmailNotifications && ei.UserStatusId.Equals((int)Constants.eUSER_STATUS.REGVALIDATED))
                                {
                                    requestTypeSentIdEmail = GetUserFacade.InsertTrackingTypeSent(requestId, Constants.cREQUEST_TRACKING_MESSAGE_TYPE_ID_EMAIL, ei.UserId);

                                    MailUtilityWeb.SendEventEmail(evt, Event.eMESSAGE_TYPE.EventChangeInvite.ToString(), ei.Email, ei.UserId, CurrentUser.UserId, string.Empty, requestTypeSentIdEmail);
                                }
                                break;
                            case Event.eMESSAGE_TYPE.EventCancellation:
                                if (ei.EmailNotifications && ei.UserStatusId.Equals((int)Constants.eUSER_STATUS.REGVALIDATED))
                                {
                                    MailUtilityWeb.SendEventEmail(evt, Event.eMESSAGE_TYPE.EventCancellation.ToString(), ei.Email, ei.UserId, CurrentUser.UserId, string.Empty, string.Empty);
                                }
                                break;
                        }

                        if (KanevaGlobals.EnableEventKMails)
                        {
                            Event.EventMessage em = GetEventFacade.GetEventMessageDetails(emt, evt, CurrentUser.Username, ResolveUrl("~/mykaneva/events/eventDetail.aspx"), string.Empty, requestTypeSentIdPM);

                            message = new Message(0, CurrentUser.UserId, ei.UserId, em.Subject, em.Message,
                                new DateTime(), 0, (int)Constants.eMESSAGE_TYPE.PRIVATE_MESSAGE, 0, "U", "S");

                            // Send KMail
                            GetUserFacade.InsertMessage(message);
                        }
                    }
                }
            }
        }

        private void GetRequestParams ()
        {
            try
            {   // bdid (Birthday users ID will be populated if coming from the friends birthday email
                // We will use this auto populate some of the fields on the form
                if (Request["bdid"] != null)
                {
                    this.BirthdayUserId = Convert.ToInt32 (Request.Params["bdid"]);
                }
            }
            catch { }

            try
            {   // zid (Zone Instance Id will be populated if coming from event flag placed in-world
                // We will use this auto configure some of the fields on the form
                if (Request["zid"] != null)
                {
                    this.ZoneInstanceId = Convert.ToInt32(Request.Params["zid"]);
                }
            }
            catch { }

            try
            {   // zonetype (Zone Type will be populated if coming from event flag placed in-world
                // We will use this auto configure some of the fields on the form
                if (Request["zonetype"] != null)
                {
                    this.ZoneType = Convert.ToInt32(Request.Params["zonetype"]);
                }
            }
            catch { }

            try
            {   // objPlacementId (Object Placement Id will be populated if coming from event flag placed in-world
                // This param will be the placement id of the event flag used in association with this event
                // We will use this auto configure some of the fields on the form
                if (Request.Params["event"] != null)
                {
                    this.EventID = Convert.ToInt32(Request.Params["event"]);
                }
            }
            catch { }

            try
            {   // objPlacementId (Object Placement Id will be populated if coming from event flag placed in-world
                // This param will be the placement id of the event flag used in association with this event
                // We will use this auto configure some of the fields on the form
                if (Request["objPlacementId"] != null)
                {
                    this.EventFlagPlacementId = Convert.ToInt32(Request.Params["objPlacementId"]);
                }

                // check to see if this event flag already has an event associated with it
                if (this.EventID == 0 && this.EventFlagPlacementId > 0)
                {
                    this.EventID = GetEventFacade.GetEventByEventFlagPlacementId(this.EventFlagPlacementId).EventId;
                }
            }
            catch { }

            // Validate query string values if coming from in-world request
            if (this.EventFlagPlacementId > 0 && this.ZoneInstanceId > 0 && this.ZoneType > 0)
            {
                // Validate the event flag is an actual object in the world
                DataRow drEventFlag = GetShoppingFacade.GetItemByPlacementId(this.EventFlagPlacementId, this.ZoneInstanceId, KanevaWebGlobals.CurrentUser.WokPlayerId);
                this.IsValidFlagEvent = false;

                if (drEventFlag != null && Convert.ToInt32(drEventFlag["obj_placement_id"]) > 0 && this.EventID == 0 )
                {
                    this.IsValidFlagEvent = true;    
                }
            }
        }

        private bool NeedToSendInvites (Event prevEvent, Event currEvent, bool eventHasChanged)
        {
            if (prevEvent.EventId > 0)
            {
                if (prevEvent.RecurringStatusId == (int) Event.eRECURRING_STATUS_TYPE.CREATED && eventHasChanged ||
                    prevEvent.RecurringStatusId == (int) Event.eRECURRING_STATUS_TYPE.CREATED && currEvent.RecurringStatusId == (int) Event.eRECURRING_STATUS_TYPE.NONE ||
                    prevEvent.RecurringStatusId == (int) Event.eRECURRING_STATUS_TYPE.CREATED && currEvent.Priority == (int) Event.ePRIORITY.PREMIUM
                    )
                {
                    return true;      
                }
            }

            return false;
        }

        private PagedDataTable GetUsersInEventWorld(Community evtCommunity)
        {
            PagedDataTable pdtUsers = null;

            if (evtCommunity.WOK3App.GameId > 0)
            {
                pdtUsers = UsersUtility.GetUsersInGame(evtCommunity.WOK3App.GameId);
            }
            else if (evtCommunity.CommunityTypeId == (int)CommunityType.HOME)
            {
                User evtCreator = GetUserFacade.GetUser(evtCommunity.CreatorId);
                WOK3DPlace zone = GetCommunityFacade.Get3DPlace(evtCreator.WokPlayerId, Constants.APARTMENT_ZONE);
                pdtUsers = UsersUtility.GetUsersInZone(0, zone.ZoneInstanceId, zone.ZoneIndex, "p.name desc", 1, Int32.MaxValue, null);
            }
            else
            {
                WOK3DPlace zone = GetCommunityFacade.Get3DPlace(evtCommunity.CommunityId, Constants.BROADBAND_ZONE);
                pdtUsers = UsersUtility.GetUsersInZone(0, zone.ZoneInstanceId, zone.ZoneIndex, "p.name desc", 1, Int32.MaxValue, null);
            }

            return pdtUsers;
        }

        #endregion Helper Methods

        #region Event Handlers

        private void btnSave_Click (object sender, EventArgs e)
        {
            Page.Validate ();
            bool isNewEvent = false;
            Event evtBeforeSave = new Event();

            //if the page is validated process it, otherwise so error summary
            if (Page.IsValid)
            {
                this.CommunityId = ddlLocation.Visible ? Convert.ToInt32 (ddlLocation.SelectedValue) : Convert.ToInt32(hidLocationRec.Value);
                
                // check to see if it is an existing event
                if (EventID > 0)
                {
                    evtBeforeSave = GetEventFacade.GetEvent(EventID);

                    //if it is check to see if the current user has the right to edit it
                    if (!UserCanEditEvent(evtBeforeSave, this.CommunityId))
                    {
                        // User does not have permissions to edit event
                        ShowErrorMessage ("You do not have permissions to edit this event.  Please create a new event.");
                    }
                }
                // Check to make sure an event does not already exist for this event flag
                else if (this.EventFlagPlacementId > 0)
                {
                    int eventId = GetEventFacade.GetEventByEventFlagPlacementId(this.EventFlagPlacementId).EventId;
                    if (eventId > 0)
                    {
                        // An event exists for this event flag so show event detail page
                        Response.Redirect("~/mykaneva/events/eventDetail.aspx?event=" + eventId);
                        return;
                    }

                    isNewEvent = true;
                }
                else
                {
                    isNewEvent = true;
                }

                bool eventHasChanged = false;
                //save new event or submitted changes
                int savedEventId = SaveEvent (EventID, ref eventHasChanged);

                //if save was successful go to event details page
                if (savedEventId > 0)
                {
                    Event evt = GetEventFacade.GetEvent (savedEventId);
                    string qs = "";
                    
                    // Need to set this variable so we can use it to pass into the SendInvites method in the new
                    // thread because the new threw has not HTTPContext and the CurrentUser is null in that thread
                    int currentUserId = KanevaWebGlobals.CurrentUser.UserId;

                    try
                    {
                        // Request tracking
                        if (RequestTypeCreateBirthdayEventId != "")
                        {
                            int iResult = GetUserFacade.AcceptTrackingRequest (RequestTypeCreateBirthdayEventId, KanevaWebGlobals.CurrentUser.UserId);
                        }
                    }
                    catch { }

                    // Send event invites
                    if (isNewEvent)
                    {
                        bool includeFriends = (rbBasic.Checked && cbBasicFriends.Checked) || (rbPremium.Checked && cbPremFriends.Checked);

                        // We need to start a new thread to process sending the emails/kmails.  The list of
                        // invitees can be quite long.
                        // The delegate to call.
                        ThreadStart ts = delegate () { SendInvites (evt.EventId, includeFriends, currentUserId); };
                        // The thread.
                        Thread t = new Thread(ts);
                        // Run the thread.
                        t.Start();
                        
                        qs = "&create=new";

                        // Insert Event Created Blast
                        GetEventFacade.InsertEventBlast (evt.EventId, KanevaWebGlobals.CurrentUser.UserId, "created this event.");
                    }
                    else 
                    {
                        // We need to start a new thread to process sending the emails/kmails.  The list of
                        // invitees can be quite long.
                        
                        // Check to see if we need to send invites 
                        // we send invites immediately on update if an event is/was recurring
                        if (NeedToSendInvites (evtBeforeSave, evt, eventHasChanged))
                        {
                            // The delegate to call.
                            ThreadStart ts1 = delegate () { SendInvites (evt.EventId, evt.InviteFriends, currentUserId); };
                            // The thread.
                            Thread t1 = new Thread (ts1);
                            // Run the thread.
                            t1.Start ();
                        }
                        else if (eventHasChanged) // Event updated so send event changed notification
                        {
                            // The delegate to call.
                            ThreadStart ts2 = delegate () { SendEventChangedNotifications (evt.EventId, currentUserId); };
                            // The thread.
                            Thread t2 = new Thread (ts2);
                            // Run the thread.
                            t2.Start ();
                        }
                    }
                     
                    // Show event detail page
                    Response.Redirect ("~/mykaneva/events/eventDetail.aspx?event=" + savedEventId + qs);
                }
            }
            else
            {
                valSum.ShowSummary = false;
            }
        }

        /// <summary>
        /// Click event for the Cancel button
        /// </summary>
        private void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(PreviousHTMLPage);
        }

        /// <summary>
        /// Click event for the Cancel button
        /// </summary>
        private void btnDelete_Click (object sender, EventArgs e)
        {
            if (EventID != 0)
            {
                try
                {
                    Event evt = GetEventFacade.GetEvent(EventID);
                    if (evt.ObjPlacementId > 0)
                    {
                        // If this is an event tied to an event flag, don't delete the event. The user
                        // must delete the event flag in-world. UI should prevent user from selecting
                        // Delete button but this is a safety check just in case.
                        return;
                    }

                    // Delete the event
                    if (GetEventFacade.UpdateEventStatus (EventID, (int) Event.eSTATUS_TYPE.CANCELLED, KanevaWebGlobals.CurrentUser.UserId) == 1)
                    {
                        // Get the list of people who are attending so we can send them notification event was cancelled
                        PagedList<EventInvitee> invitees = GetEventFacade.GetEventInviteesByStatus (EventID, (int) EventInvitee.Invite_Status.ACCEPTED, "", 1, int.MaxValue);
                        
                        if (invitees.Count > 0)
                        {
                            Message message = new Message ();

                            foreach (EventInvitee ei in invitees)
                            {
                                // Verify they want to get event notifictions and have a validated email address
                                if (ei.EmailNotifications && ei.EmailIsValidated)
                                {
                                    // Send email
                                    MailUtilityWeb.SendEventEmail (evt, Event.eMESSAGE_TYPE.EventCancellation.ToString (), ei.Email, ei.UserId, KanevaWebGlobals.CurrentUser.UserId, string.Empty, string.Empty);
                                }

                                if (KanevaGlobals.EnableEventKMails)
                                {
                                    // Send them KMail
                                    Event.EventMessage em = GetEventFacade.GetEventMessageDetails (Event.eMESSAGE_TYPE.EventCancellation, evt, KanevaWebGlobals.CurrentUser.Username, ResolveUrl ("~/mykaneva/events/eventDetail.aspx"), string.Empty, string.Empty);

                                    message = new Message (0, KanevaWebGlobals.CurrentUser.UserId, ei.UserId, em.Subject, em.Message,
                                        new DateTime (), 0, (int) Constants.eMESSAGE_TYPE.PRIVATE_MESSAGE, 0, "U", "S");

                                    // Send KMail
                                    GetUserFacade.InsertMessage (message);
                                }
                            }
                        }

                        Response.Redirect (PreviousHTMLPage);
                    }
                }
                catch (Exception)
                {
                }
            }
        }

        #endregion Event Handlers

        #region Attributes

        private int EventID
        {
            get
            {
                if (ViewState["EventId"] == null)
                {
                    ViewState["EventId"] = 0;
                }
                return (int)ViewState["EventId"];
            }
            set
            {
                ViewState["EventId"] = value;
            }
        }

        public string PreviousHTMLPage
        {
            get
            {
                if (ViewState["previousPage"] == null)
                {
                    ViewState["previousPage"] = "~/default.aspx";
                }
                return ViewState["previousPage"].ToString();
            }
            set
            {
                ViewState["previousPage"] = value;
            }
        }

        private int UserId
        {
            get
            {
                return _userId;
            }
            set
            {
                _userId = value;
            }
        }

        private int CommunityId
        {
            get
            {
                return _communityId;
            }
            set
            {
                _communityId = value;
            }
        }
        private int BirthdayUserId
        {
            get
            {
                return _birthdayUserId;
            }
            set
            {
                _birthdayUserId = value;
            }
        }

        private int ZoneInstanceId
        {
            get
            {
                return _zoneInstanceId;
            }
            set
            {
                _zoneInstanceId = value;
            }
        }

        private int ZoneType
        {
            get
            {
                return _zoneType;
            }
            set
            {
                _zoneType = value;
            }
        }

        private int EventFlagPlacementId
        {
            get
            {
                if (ViewState["EFPId"] == null)
                {
                    ViewState["EFPId"] = 0;
                }
                return (int)ViewState["EFPId"];
            }
            set
            {
                ViewState["EFPId"] = value;
            }
        }

        private bool IsValidFlagEvent
        {
            get
            {
                return _isValidFlagEvent;
            }
            set
            {
                _isValidFlagEvent = value;
            }
        }

        private bool IsAdmin
        {
            get
            {
                return _isAdmin;
            }
            set
            {
                _isAdmin = false; // value;
            }
        }

        private double AvailableRewards
        {
          get
          {
            if (ViewState["usersRewards"] == null)
            {
              ViewState["usersRewards"] = 0.0;
            }
            return (double)ViewState["usersRewards"];
          }
          set
          {
            ViewState["usersRewards"] = value;
          }
        }

        private double AvailableCredits
        {
          get
          {
            if (ViewState["usersCredits"] == null)
            {
              ViewState["usersCredits"] = 0.0;
            }
            return (double)ViewState["usersCredits"];
          }
          set
          {
            ViewState["usersCredits"] = value;
          }
        }

        private enum ErrorType
        {
            Name,
            World,
            Desc,
            StartTime,
            EndTime,
            Other
        }

        public string RequestTypeCreateBirthdayEventId
        {
            get
            {
                if (ViewState["ReqeustTypeCreateBirthdayEventId"] == null)
                {
                    return "";
                }
                return ViewState["ReqeustTypeCreateBirthdayEventId"].ToString ();
            }
            set
            {
                ViewState["ReqeustTypeCreateBirthdayEventId"] = value;
            }
        }

        private bool IsRecurringChanged
        {
            get
            {
                return _isRecurringChanged;
            }
            set
            {
                _isRecurringChanged = value;
            }
        }

        #endregion Attributes

        #region Web Form Designer generated code
        override protected void OnInit (EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent ();
            base.OnInit (e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent ()
        {
            this.Load += new System.EventHandler (this.Page_Load);
            btnSave.Click += new EventHandler (btnSave_Click);
            btnCancel.Click += new EventHandler (btnCancel_Click);
            btnDelete.Click += new EventHandler(btnDelete_Click);
        }
        #endregion

    }
}
