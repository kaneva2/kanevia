<%@ Page Language="C#" MasterPageFile="~/MASTERPAGES/IndexPageTemplate.Master" AutoEventWireup="false" CodeBehind="eventForm.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.mykaneva.events.eventForm" Title="Kaneva Event Management" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cph_Body" runat="server">

	<script src="../../jscript/jquery-ui/jquery-ui-1.10.0.custom.js" type="text/javascript" ></script>
	
	<link href="../../css/base/base_new.css" type="text/css" rel="stylesheet" />
	<link href="../../css/events.css?v6" type="text/css" rel="stylesheet" />
	<link href="../../jscript/jquery-ui/css/ui-darkness/jquery-ui-1.10.0.custom.css" type="text/css" rel="stylesheet" />   
	<script>
	function SaveEvent() {
		ShowLoading(true, 'Saving event and creating invites...<br/><img src="../../images/progress_bar.gif">');
	}
	var $j = jQuery.noConflict();
	var maxDays = <asp:literal id="litCalendarPlusDays" runat="server"></asp:literal>;

	$j(document).ready(function () {
		$j("#rbPremium").click(function () {
			$j("#premcontain").show(100);
			$j(".cbfriends.recur").css("display", "none");
		});

		$j("#rbBasic").click(function () {
			$j("#premcontain").hide(200);
			    showrecurring();
		});

		$j("#txtEventStart").datepicker({
			minDate: 0,
			maxDate: maxDays,
			buttonImage: "../../images/mykaneva/calendar_20x20.png",
			buttonImageOnly: true,
			showOn: 'button'
		});
	    //		$("#txtEventStart").datepicker('option', 'maxDate', $("#Panel2nrTextBox1").val());

		$j("#txtAdminDate").datepicker({
			minDate: 0,
			buttonImage: "../../images/mykaneva/calendar_20x20.png",
			buttonImageOnly: true,
			showOn: 'button'
		});

		$j("#ddlLocation").change(function () {
			showrecurring();
		});

		function showrecurring() {
			$j(".cbfriends.recur").css("display", "none");
			$j("#divInvites").css("display", "none");
			
			if ($j('option:selected', $j("#ddlLocation")).attr('rec') > 0 && $j("#rbBasic").is(':checked')) {
				$j(".cbfriends.recur").css("display", "block");
				$j("#divInvites").css("display", "block");
			}

			$j("#hidLocationRec").val($j('option:selected', $j("#ddlLocation")).attr('rec'));
		}
	});
	</script>
	<div class="PageContainer">  
    
		<div id="divLoading">
			<table height="300" cellspacing="0" cellpadding="0" border="0">
				<tr>
					<th class="loadingText" id="divLoadingText">
						Loading...
						<div style="display: none"><img src="../images/progress_bar.gif"></div>
					</th>
				</tr>
			</table>
		</div>

		<div id="divData" style="display: none">

        <div>
		    <asp:validationsummary cssclass="errBox" id="valSum" runat="server" showmessagebox="false" showsummary="True" 
			    displaymode="BulletList" headertext="Please correct the following error(s):" forecolor="#333333" />
		    <asp:customvalidator id="cvBlank" runat="server" display="None" enableclientscript="False"></asp:customvalidator>
	    </div>

		<div id="formContainer">
			
			<asp:Panel ID="pnlInvalidAccess" Runat="server" Visible="False" >
				You are not allowed to edit this event
			</asp:Panel>			 

			<asp:panel ID="pnlEdit" Runat="server">						

				<h1 id="eventHeadline" runat="server">World Event Creation</h1>
				<div class="tagline">Attendees will earn 1000 rewards for coming to your event.</div>
					<div class="leftcol">
						<div>
							<label id="lblEventName" runat="server">Event Name</label>
							<asp:TextBox ID="txtTitle" Runat="server" MaxLength="100" TabIndex="1"  />
						</div>
						<div>
							<label id="lblWorld" runat="server">Your World</label>
							<asp:DropDownList id="ddlLocation" clientidmode="Static" Runat="server" TabIndex="3" />
                            <label id="lblWorldName" runat="server" class="worldname" visible="false"></label>
							<asp:hiddenfield id="hidLocationRec" runat="server" clientidmode="Static" value="0" />
							<asp:hiddenfield id="hidLocationName" runat="server" value="" />
						</div>
						<div class="desc">													 
							<label id="lblDesc" runat="server">Description</label>
							<asp:TextBox ID="txtDescription" Runat="server" TextMode="MultiLine" Rows="6" TabIndex="4" />
						</div>
					</div>
																							  
					<div class="rightcol">
						<div class="date">
							<div id="cal1Container" runat="server" clientidmode="static"></div><div id="cal2Container"></div>
							<label id="lblStartDate">Start Date</label>
							<asp:TextBox id="txtEventStart" clientidmode="static" MaxLength="10" onfocus="blur();" runat="server" TabIndex="5" />
							<asp:DropDownList ID="ddlHourStart" Runat="server" TabIndex="6" />
							<asp:DropDownList ID="ddlMinuteStart" Runat="server" TabIndex="7" /> 
							<div class="tz">EST</div>
						</div>
																																		 
						<div id="divRecurring" class="recurring" runat="server">
							<asp:checkbox id="cbRecurring" clientidmode="Static" class="cbfriends recur" runat="server" text="Recur Weekly at Same Day and Time" checked="false" />
							<div id="divInvites" runat="server" clientidmode="Static" visible="false">Note: Invites will be sent on <span id="spnInvitesSentDate" runat="server"></span>. 
								Invites will be sent immediately if any changes are made.</div>	
						</div>

						<div class="eventtype" id="divNonAdmin" runat="server"><!-- Premium Event Section -->
							<div class="type"><input type="radio" name="rblEventType" id="rbBasic" clientidmode="static" runat="server" checked /> <label id="lblBasic" runat="server">Basic Event</label></div>
							<div class="typecontainer" id="basiccontain" runat="server" clientidmode="static">
								<div class="note clearit">Basic Events are only seen by World Members or your friends if you invite them.</div>
								<asp:checkbox id="cbBasicFriends" class="cbfriends" checked="true" runat="server" text="Invite Friends" />
							</div>
							<div class="type premium"><input type="radio" name="rblEventType" id="rbPremium" clientidmode="static" runat="server" /> <label id="lblPremium" runat="server">Premium Event</label></div>
							<div class="note premiumnote clearit">Premium Events are promoted to everyone.</div>
							<div class="typecontainer prem" id="premcontain" runat="server" clientidmode="static">
								<asp:checkbox id="cbPremFriends" class="cbfriends" checked="true" runat="server" text="Invite Friends" />
								<hr />
								<asp:RadioButtonList class="amounts" ID="rblPremCurrency" runat="server" TabIndex="120">
									<asp:ListItem Text="200 Credits" Value="KPOINT" selected="True"></asp:ListItem> 
									<asp:ListItem Text="200 Rewards" Value="GPOINT"></asp:ListItem>									               
								</asp:RadioButtonList>
								<div class="balances">
									<div id="divCredits" runat="server"></div>
									<div id="divRewards" runat="server"></div>
								</div>  
								<div class="note clearit">(The fee only applies to the host, not the guests)</div>
							</div> 
						</div>
						<div class="admin" id="divAdmin" runat="server">
							<fieldset>
							<legend>Administrators Only</legend>
							<div>
								<label><asp:CheckBox id="chkKanevaBlast" runat="server" text=""/> Kaneva Blast to Entire Site</label><div id="cal3Container"></div>
							</div>
										
							<asp:RadioButtonList id="rblColor" runat="server" RepeatDirection="Horizontal" >
								<asp:ListItem title="Critical" Value="alert"><span style="color:red;">Red</span></asp:ListItem>
								<asp:ListItem title="Sponsor" Value="highlight2"><span style="color:#e7923e;">Orange</span></asp:ListItem>
								<asp:ListItem title="Sale" Value="highlight3"><span style="color:green;">Green</span></asp:ListItem>
								<asp:ListItem title="Highlight" Value="highlight1"><span style="color:yellow;background-color:#aaaaaa">Yellow</span></asp:ListItem>
								<asp:ListItem Selected="True" Value="">None</asp:ListItem>
							</asp:RadioButtonList>
									
							<div class="date">
								<label>Expire After</label>
								<asp:TextBox ID="txtAdminDate" clientidmode="static" class="formKanevaText" onfocus="blur();" MaxLength="10" runat="server" />
									
								<asp:DropDownList ID="ddlAdminEndHour" Runat="server" TabIndex="90" />
								<asp:DropDownList ID="ddlAdminEndMinute" Runat="server" TabIndex="100" />
								<div class="tz">EST</div>
							</div>
							</fieldset>
						</div>
					</div>                                                                
					
					<!--
					<table class="dateTime">
						<tr>
							<td style="width:85px; text-align:right"><label>End Date:&nbsp;</label></td>
							<td><asp:TextBox ID="txtEventEnd" class="formKanevaText" style="width:80px" MaxLength="10" runat="server" TabIndex="80" />
								<img id="imgSelectEnd" name="imgSelectEnd" onload="initEventEnd();" src="../../images/blast/cal_16.gif"/> 
								</td>
							<td  style="width:60px; text-align:right"><label>Time:&nbsp;</label></td>
							<td>
              					<asp:DropDownList ID="ddlHourEnd" Runat="server" TabIndex="90" />
								<asp:DropDownList ID="ddlMinuteEnd" Runat="server" TabIndex="100" /> <span class="evFormNote">EST</span>
							</td>
						</tr>											
					</table> 								         
					<asp:customvalidator id="cvStartTime"  style="margin-left:20px;" errormessage="" text="The Start Time must be before the End Time."></asp:customvalidator>
					-->
	                             
					<div class="btncontainer clearit">
						<asp:linkbutton ID="btnSave" class="grey btnsmall padright" Text="Create Event" onclientclick="SaveEvent();" Runat="server" TabIndex="140"></asp:linkbutton>
      					<asp:linkbutton ID="btnDelete" class="grey btnsmall padright" Text="Delete" Runat="server" clientidmode="static" Visible="false" TabIndex="130" causesvalidation="false"></asp:linkbutton>
      					<asp:linkbutton ID="btnCancel" class="grey btnsmall" Text="Cancel" Runat="server" TabIndex="130" causesvalidation="false"></asp:linkbutton>
					</div>
        		
                    <center><p id="deleteFlagEventMessage" runat="server" visible="false">To delete a player event you must remove the event flag from the World.<br /><br /></p></center>

			</asp:panel>

			<asp:panel id="pnlCreateWorld" runat="server" visible="false">

				<div class="createworld">
					<div class="headline">Events are only for Worlds</div>
					<div class="small">Click below to create your first World</div>
					<asp:linkbutton id="btnCreateWorld" class="orange btnxlarge" runat="server" text="Create World"></asp:linkbutton>
				</div>

			</asp:panel>

		</div> <!-- End Form Container -->
	
		</div>
	
	</div> <!-- End Page Container -->

</asp:Content>
