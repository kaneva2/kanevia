///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using log4net;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.mykaneva.events
{
    public partial class eventDetail : BasePage
    {
        protected void Page_Load (object sender, EventArgs e)
        {
            if (!Request.IsAuthenticated)
            {
                Response.Redirect (this.GetLoginURL ());
            }

            if (!IsPostBack)
            {
                GetRequestParams ();
                if (CurrentEvent.EventId > 0)
                {
                    // If user has chosen Accept or Decline from an external link then
                    // we need to do that action before loading the event
                    if (this.CurrentAction == EventAction.Attend)
                    {
                        GetEventFacade.UpdateEventInviteStatus (CurrentEvent.EventId,
                            KanevaWebGlobals.CurrentUser.UserId, (int) CurrentAction, CurrentEvent.IsPremium);

                        // If event is going on right now, take them to the event in world
                        if (GetEventFacade.IsEventHappeningNow(CurrentEvent))
                        {
                            Response.Redirect ("~/kgp/playwok.aspx?goto=U" + GetGameFacade.GetGameIdByCommunityId(CurrentEvent.CommunityId) + "&ILC=MM3D&link=private");
                        }
                    }

                    litCommentsFilePath.Text = ResolveUrl ("~/usercontrols/doodad/");
                    LoadEvent (this.CurrentEvent.EventId);

                    // Set value in HttpContext so widgets can have access to it.
                    HttpContext.Current.Items.Add ("event", CurrentEvent);
                }
                else
                {
                    Response.Redirect ("~/default.aspx");
                }
            }
        }

        #region Helper Methods

        public void LoadEvent (int eventId)
        {
            if (CurrentEvent.EventId == 0)
            {
                // display error message - event not found
                pnlNotFound.Visible = true;
                pnlDetail.Visible = false;
                return;
            }

            lblEventName.Text = Server.HtmlDecode (CurrentEvent.Title);
            divDescription.InnerHtml = "<label>Description:</label> " + Server.HtmlDecode (CurrentEvent.Details);
            hlLocation.Text = CurrentEvent.Location;

            // Formate the date
            string ampm = CurrentEvent.StartTime.ToString ("tt").ToLower ();
            lblWhen.Text = CurrentEvent.StartTime.ToString ("dddd, MMMM ") + Common.GetDaySuffix (CurrentEvent.StartTime.Day) + ", " +
                string.Format ("{0:h:mm}{1}", CurrentEvent.StartTime, ampm) + " EST";


            Community community = GetCommunityFacade.GetCommunity (CurrentEvent.CommunityId);
            ConfigureCommunityMeetMe3D (hlLocation, community.WOK3App.GameId, community.CommunityId, community.Name, CurrentEvent.TrackingRequestGUID);

            User user = GetUserFacade.GetUser (CurrentEvent.UserId);

            // Image
            imgOwner.ImageUrl = GetProfileImageURL (user.ThumbnailSquarePath, "sq", user.Gender);

            // User
            hlEventOwner.NavigateUrl = GetPersonalChannelUrl (user.Username);
            hlEventOwner.Text = user.Username;

            // Check if owner is viewing the event
            if (KanevaWebGlobals.CurrentUser.UserId != CurrentEvent.UserId)
            {
                // Get the user's invite
                EventInvitee invitee = GetEventFacade.GetEventInvitee (KanevaWebGlobals.CurrentUser.UserId, eventId);

                ShowEventButtonStatusBar (invitee.InviteStatus);
            }
            else // Event Creator
            {
                if (this.IsNewEvent)
                {
                    lblInviteStatus.Text = "Event Created Successfully";
                    lblInviteStatus.Style.Add ("display", "block");
                }
                else
                {
                    // Show edit link
                    btnEdit.Style.Add ("display", "block");
                    btnEdit.Attributes.Add ("href", ResolveUrl ("~/mykaneva/events/eventForm.aspx?event=" + CurrentEvent.EventId));
                }
            }

            if (CurrentEvent.StatusId == (int) Event.eSTATUS_TYPE.CANCELLED)
            {
                // Don't show any buttons, just message
                lblInviteStatus.Text = "This Event Was Cancelled.";
                lblInviteStatus.Style.Add ("display", "block");
                btnEdit.Style.Add ("display", "none;");

                // Don't show any comments
                ucEventBlasts.Visible = false;

                // Don't show attendee list
                divAttendees.Visible = false;

                return;
            }

            HasEventStarted ();

            // Show attendee thumbnails
            GetAttendeeList ();
        }

        private void HasEventStarted ()
        {
            bool eventHasStarted = false;

            if (GetEventFacade.IsEventHappeningNow(CurrentEvent))
            {
                lblInviteStatus.Text = "This event is Happening Now!!";
                lblInviteStatus.Style.Add ("display", "block");
                eventHasStarted = true;
            }
            else if (CurrentEvent.EndTime < DateTime.Now)
            {
                // Event Has Ended
                lblInviteStatus.Text = "This event has ended.";
                lblInviteStatus.Style.Add ("display", "block");
                eventHasStarted = true;
            }

            if (eventHasStarted)
            {
                btnAttend.Style.Add ("display", "none");
                btnDecline.Style.Add ("display", "none");
                btnEdit.Style.Add ("display", "none");
            }
        }

        private void ShowEventButtonStatusBar (EventInvitee.Invite_Status inviteStatus)
        {
            lblInviteStatus.Text = "";
            lblInviteStatus.Style.Add ("display", "none");
            btnAttend.Style.Add ("display", "none");
            btnDecline.Style.Add ("display", "none");

            if (inviteStatus == EventInvitee.Invite_Status.ACCEPTED)
            {
                // Show the "You're Going" message
                lblInviteStatus.Text = "You're Going!";
                lblInviteStatus.Style.Add ("display", "block");
            }
            else if (inviteStatus == EventInvitee.Invite_Status.DECLINED)
            {
                // Show page as read only & Decline message
                lblInviteStatus.Text = "You declined the invitation to this event.";
                lblInviteStatus.Style.Add ("display", "block");
            }
            else if (CurrentEvent.IsPremium)
            {
                // If user was not explicitly invited to event or user was invited and has not responded
                // then show Attend/Decline buttons. Anyone can select to attend a Premium Event
                if (inviteStatus == EventInvitee.Invite_Status.NOT_INVITED ||
                    inviteStatus == EventInvitee.Invite_Status.INVITED)
                {
                    btnAttend.Style.Add ("display", "block");
                    btnDecline.Style.Add ("display", "block");
                }
            }
            else // Basic Event
            {
                if (inviteStatus == EventInvitee.Invite_Status.INVITED)
                {
                    // Show Attend/Decline buttons
                    btnAttend.Style.Add ("display", "block");
                    btnDecline.Style.Add ("display", "block");
                }
                else if (inviteStatus == EventInvitee.Invite_Status.NOT_INVITED)
                {
                    // Show page as read only
                    lblInviteStatus.Text = "Earn 1000 rewards a day for going to an event.";
                    lblInviteStatus.Style.Add ("display", "block");
                }
            }
        }
        
        private void GetRequestParams ()
        {
            try
            {
                if (Request["event"] != null)
                {
                    //get channelId and find the default page
                    this.EventId = Convert.ToInt32 (Request["event"]);
                }
            }
            catch {}

            try
            {
                if (Request["action"] != null)
                {
                    string ea = (Request["action"]).ToString().ToLower();

                    if (ea == EventAction.Attend.ToString ().ToLower ())
                    {
                        this.CurrentAction = EventAction.Attend;
                    }
                    else if (ea == EventAction.Decline.ToString ().ToLower ())
                    {
                        this.CurrentAction = EventAction.Decline;
                    }
                    else
                    {
                        this.CurrentAction = EventAction.None;
                    }
                }
            }
            catch { }

            try
            {
                if (Request["create"] != null)
                {
                    //get channelId and find the default page
                    if (Request["create"].ToString () == "new")
                    {
                        this.IsNewEvent = true;
                    }
                }
            }
            catch { }

            try
            {
                // Request tracking
                if (Request.Params[Constants.cREQUEST_TRACKING_URL_PARAM] != null)
                {
                    int iResult = GetUserFacade.AcceptTrackingRequest(Request.Params[Constants.cREQUEST_TRACKING_URL_PARAM], KanevaWebGlobals.CurrentUser.UserId);
                }
            }
            catch { }
        }

        private void GetAttendeeList ()
        {
            if (currentEvent.RecurringStatusId == (int) Event.eRECURRING_STATUS_TYPE.CREATED)
            {
                litInvitesDate.Text = CurrentEvent.StartTime.Date.AddDays (-3).ToString("dddd, MMMM d, yyyy");
                divInvitesSent.Visible = true;
                divAttendees.Visible = false;
                return;
            }
            
            PagedList <EventInvitee> attendees = GetEventFacade.GetEventInviteesByStatus (currentEvent.EventId, 
                (int)EventInvitee.Invite_Status.ACCEPTED, "date_last_modified DESC", 1, 20);

            if (attendees.Count > 0)
            {
                rptAttendees.DataSource = attendees;
                rptAttendees.DataBind ();
                litAttendeeCount.Text = attendees.TotalCount.ToString ();
                divAttendees.Visible = true;
            }
            else
            {
                divAttendees.Visible = false;
            }
        }
        
        #endregion Helper Methods


        #region Event Handlers

        public void ActionButton_Click (Object sender, CommandEventArgs e)
        {
            GetRequestParams ();

            if (CurrentEvent.EventId > 0)
            {
                EventInvitee.Invite_Status inviteStatus = EventInvitee.Invite_Status.NOT_INVITED;

                if (e.CommandName == "attend")
                {
                    CurrentAction = EventAction.Attend;
                    inviteStatus = EventInvitee.Invite_Status.ACCEPTED;
                }
                else if (e.CommandName == "decline")
                {
                    CurrentAction = EventAction.Decline;
                    inviteStatus = EventInvitee.Invite_Status.DECLINED;
                }

                GetEventFacade.UpdateEventInviteStatus (CurrentEvent.EventId,
                KanevaWebGlobals.CurrentUser.UserId, (int) CurrentAction, CurrentEvent.IsPremium);

                // Set value in HttpContext so widgets can have access to it.
                if (HttpContext.Current.Items["event"] == null)
                {
                    HttpContext.Current.Items.Add ("event", CurrentEvent);
                }

                if (inviteStatus == EventInvitee.Invite_Status.ACCEPTED)
                {
                    ucEventBlasts.Refresh ();
                    GetAttendeeList ();
                }
                ShowEventButtonStatusBar (inviteStatus);
            }
        }

        #endregion Event Handlers

        #region Properties

        public int EventId
        {
            get
            {
                return this.eventId;
            }
            set
            {
                this.eventId = value;
            }
        }

        private bool IsNewEvent
        {
            get
            {
                return this.isNewEvent;
            }
            set
            {
                this.isNewEvent = value;
            }
        }

        public Event CurrentEvent
        {
            get
            {
                try
                {
                    if (this.currentEvent.EventId == 0)
                    {
                        if (HttpContext.Current.Items["event"] != null)
                        {
                            this.currentEvent = (Event) (HttpContext.Current.Items["event"]);
                        }
                        else
                        {
                            this.currentEvent = GetEventFacade.GetEvent (Convert.ToInt32 (Request["event"]));
                        }
                    }
                }
                catch
                {
                    return new Event (); ;
                }

                return this.currentEvent;
            }
            set
            {
                currentEvent = value;
            }
        }

        private EventAction CurrentAction
        {
            get
            {
                return this.action;
            }
            set
            {
                this.action = value;
            }
        }

        private enum EventAction
        {
            None = 0,
            Attend = 2,
            Decline = 3
        }

        #endregion Properties

        #region Declerations

        private Event currentEvent = new Event ();
        private EventAction action = EventAction.None;
        private int eventId = 0;
        private bool isNewEvent = false;

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

        #endregion Declerations


    }
}
