///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using log4net;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.mykaneva.events
{
    public partial class EventBlasts : Kaneva.mykaneva.widgets.ModuleViewBaseControl
    {
        protected void Page_Load (object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitBlasts ();
                BindEventBlasts ();
            }
        }

        private void InitBlasts ()
        {
            if (CanUserBlastToEvent ())
            {
                // Set up add comments
                txtEventBlast.Attributes.Add("onfocus", "ShowAddComment($j(this),$j('#" + btnAddBlast.ClientID + "'),defBlastText);");
                txtEventBlast.Attributes.Add("onblur", "ResetAddComment($j(this),defBlastText);");
                txtEventBlast.Attributes.Add("onkeyup", "ResizeTextArea($j(this));");
                divBlastBox.Visible = true;
            }
            else
            {
                divBlastBox.Visible = false;
            }
        }

        private void BindEventBlasts ()
        {
            BindEventBlasts (this.CurrentEvent.EventId);
        }

        private void BindEventBlasts (int eventId)
        {
            PagedList<EventBlast> eb = GetEventFacade.GetEventBlasts (eventId, 1, Int16.MaxValue, "created_date ASC");

            if (eb.Count == 0)
            {
                divNoData.Visible = true;
            }
            else
            {
                divNoData.Visible = false;
                rptEventBlasts.DataSource = eb;
                rptEventBlasts.DataBind ();
            }

            hidEventIdBlast.Value = eventId.ToString();
        }

        private void ShowError (string msg)
        {
            ShowError (msg, true);
        }
        private void ShowError (bool showMsg)
        {
            ShowError ("", showMsg);
        }
        private void ShowError (string msg, bool showMsg)
        {
            blastError.InnerHtml = "";
            blastError.Style.Add ("display", "none");

            if (showMsg)
            {
                blastError.InnerHtml = msg;
                blastError.Style.Add ("display", "block");
                btnAddBlast.Style.Add ("display", "block");
            }
        }

        private bool CanUserBlastToEvent ()
        {
            if (CurrentEvent.EndTime < DateTime.Now)
            {
                return false;
            }
            if (KanevaWebGlobals.CurrentUser.UserId == CurrentEvent.UserId)
            {
                // User is event creator
                return true;
            }
            if (!KanevaWebGlobals.CurrentUser.HasWOKAccount)
            {
                return false;
            }

            // Is user attending the event
            return IsUserAttendingEvent;
        }
        private bool CanDelete (int blastCreatorId)
        {
            if (CurrentEvent.EndTime < DateTime.Now)
            {
                return false;
            }
            if (KanevaWebGlobals.CurrentUser.UserId == blastCreatorId ||
                GetUserFacade.IsUserAdministrator ())
            {
                return true;
            }
            return false;
        }

        #region Event Handlers

        protected void rptEventBlasts_ItemDataBound (Object Sender, RepeaterItemEventArgs e)
        {
            if ((e.Item.ItemType == ListItemType.Item) || (e.Item.ItemType == ListItemType.AlternatingItem))
            {
                HiddenField hidEventId = ((HiddenField) e.Item.FindControl ("hidEventId"));
                HiddenField hidEventBlastId = ((HiddenField) e.Item.FindControl ("hidEventBlastId"));
                HiddenField hidNumberOfComments = ((HiddenField) e.Item.FindControl ("hidNumberOfComments"));
                PlaceHolder phEntryComments = ((PlaceHolder) e.Item.FindControl ("phEntryComments"));

                if (phEntryComments != null)
                {
                    KlausEnt.KEP.Kaneva.usercontrols.EventComments commentWidget = (KlausEnt.KEP.Kaneva.usercontrols.EventComments) Page.LoadControl (ResolveUrl ("~/usercontrols/doodad/EventComments.ascx"));
                    commentWidget.EventId = Convert.ToInt32 (hidEventId.Value);
                    commentWidget.EventBlastId = hidEventBlastId.Value;
                    commentWidget.NumberOfComments = Convert.ToInt32 (hidNumberOfComments.Value);
                    commentWidget.ShowCommentsBox = CanUserBlastToEvent ();
                    phEntryComments.Controls.Add (commentWidget);
                }

                // If user has already voted on this contest, they can't vote again
                EventBlast blast = (EventBlast) e.Item.DataItem;

                if (blast.Creator.UserId == CurrentEvent.UserId &&
                    blast.Comment == "created this event.")
                {
                    HtmlContainerControl spnEventCreated = ((HtmlContainerControl) e.Item.FindControl ("spnEventCreated"));
                    HtmlContainerControl pComment = ((HtmlContainerControl) e.Item.FindControl ("pComment"));

                    spnEventCreated.InnerHtml = " <span class=\"eventcreated\"> " + blast.Comment + "</span>";
                    pComment.Visible = false; ;
                }

                // Delete link
                if (CanDelete (blast.Creator.UserId))
                {
                    LinkButton aDeleteEntry = ((LinkButton) e.Item.FindControl ("aDeleteEntry"));

                    if (aDeleteEntry != null)
                    {
                        aDeleteEntry.Visible = true;
                        aDeleteEntry.CommandArgument = blast.EventId.ToString () + "," + blast.EventBlastId.ToString ();
                        aDeleteEntry.CommandName = "delentry";
                    }
                }
            }
        }

        protected void rptEventBlasts_ItemCommand (Object sender, RepeaterCommandEventArgs e)
        {
            if (e.CommandArgument.ToString ().Length > 0 && e.CommandName.ToString ().Length > 0)
            {
                try
                {
                    string[] vals = e.CommandArgument.ToString ().Split (',');

                    int eventId = Convert.ToInt32 (vals[0]);
                    string eventBlastId = vals[1].ToString ();

                    // verify user can vote
                    if (e.CommandName.ToString () == "delentry")
                    {
                        EventBlast blast = GetEventFacade.GetEventBlast (eventBlastId);
                        if (CanDelete (blast.Creator.UserId) && blast.EventBlastId != string.Empty)
                        {
                            //                     GetEventFacade.DeleteEventComment

                            BindEventBlasts ();
                        }
                    }

                }
                catch { }
            }
        }

        protected void btnAddBlast_Click (Object sender, EventArgs e)
        {
            try
            {
                txtEventBlast.Text = txtEventBlast.Text.Trim ();
                int eventId = Convert.ToInt32 (hidEventIdBlast.Value);
                
                ShowError (false);

                if (Request.IsAuthenticated)
                {
                    // Check for any inject scripts
                    if (KanevaWebGlobals.ContainsInjectScripts (txtEventBlast.Text))
                    {
                        BindEventBlasts ();
                        m_logger.Warn ("User " + KanevaWebGlobals.CurrentUser.UserId.ToString () + " tried to script from IP " + Common.GetVisitorIPAddress());
                        ShowError ("Your input contains invalid scripting, please remove script code and try again.");
                        return;
                    }

                    // Check to make sure user did not enter any "potty mouth" words
                    if (KanevaWebGlobals.isTextRestricted (txtEventBlast.Text, Constants.eRESTRICTION_TYPE.POTTY_MOUTH))
                    {
                        BindEventBlasts ();
                        ShowError (Constants.VALIDATION_REGEX_RESTRICTED_WORD_MESSAGE);
                        return;
                    }

                    if (txtEventBlast.Text.Length == 0 || txtEventBlast.Text == "Blast to this event...")
                    {
                        BindEventBlasts ();
                        ShowError ("Please enter a blast.  You can not submit a blank blast.");
                        return;
                    }

                    if (txtEventBlast.Text.Length > KanevaGlobals.MaxCommentLength)
                    {
                        BindEventBlasts ();
                        ShowError ("Your blast is too long.  Blasts must be " + KanevaGlobals.MaxCommentLength.ToString () + " characters or less");
                        return;
                    }

                    if (eventId == 0)
                    {
                        ShowError ("There was a problem adding your blast.  Please try again.");
                        return;
                    }

                    string thumbnailSmallPath = KanevaWebGlobals.CurrentUser.ThumbnailSmallPath;
                    string senderName = KanevaWebGlobals.CurrentUser.Username;


                    // This code is used to insert the path for the default profile pic for a user if they
                    // have not uploaded a profile pic yet.  Blast does not store the user's gender thus
                    // why we have to prepopulate the thumbnail_path field with default profile image

                    /********** NOT SURE WE NEED THIS
                    if ((isCommunityPersonal || (!isCommunityPersonal && !isCommunityBlast))
                        && thumbnailSmallPath.Length.Equals (0))
                    {
                        if (Current_User.Gender.ToUpper ().Equals ("F"))
                        {
                            thumbnailSmallPath = "/KanevaIconFemale_sm.gif";
                        }
                        else
                        {
                            thumbnailSmallPath = "/KanevaIconMale_sm.gif";
                        }
                    }
                     * */

                    if (KanevaWebGlobals.CurrentUser.FacebookSettings.UseFacebookProfilePicture &&
                        KanevaWebGlobals.CurrentUser.FacebookSettings.FacebookUserId > 0)
                    {
                        thumbnailSmallPath = GetFacebookProfileImageUrl (KanevaWebGlobals.CurrentUser.FacebookSettings.FacebookUserId, "sq");
                    }

                    // Add the blast
                    GetEventFacade.InsertEventBlast (eventId, KanevaWebGlobals.CurrentUser.UserId, Server.HtmlEncode (txtEventBlast.Text));


                    // If owner is adding a blast, then email all event attendees 
                    if (CurrentEvent.UserId == KanevaWebGlobals.CurrentUser.UserId)
                    {
                        Event evt = GetEventFacade.GetEvent(eventId);
                        Community community = GetCommunityFacade.GetCommunity(evt.CommunityId);
                        int evtGameId = community.WOK3App.GameId;

                        // Get all the users attending this event
                        PagedList<EventInvitee> invitees = GetEventFacade.GetEventInviteesByStatus (CurrentEvent.EventId, (int)EventInvitee.Invite_Status.ACCEPTED, "", 1, int.MaxValue);

                        if (invitees.Count > 0)
                        {
                            Message message = new Message ();

                            foreach (EventInvitee ei in invitees)
                            {
                                string requestId = string.Empty;

                                if (ei.EmailNotifications)
                                {
                                    requestId = GetUserFacade.GetTrackingRequestId(requestId, Constants.cREQUEST_TRACKING_REQUEST_TYPE_ID_EVENT_BLAST, KanevaWebGlobals.CurrentUser.UserId, evtGameId, evt.CommunityId);
                                    string requestTypeSentIdEmail = GetUserFacade.InsertTrackingTypeSent(requestId, Constants.cREQUEST_TRACKING_MESSAGE_TYPE_ID_EMAIL, ei.UserId);

                                    MailUtilityWeb.SendEventEmail (CurrentEvent, Event.eMESSAGE_TYPE.EventOwnerBlastNotification.ToString (), ei.Email, ei.UserId, KanevaWebGlobals.CurrentUser.UserId, txtEventBlast.Text, requestTypeSentIdEmail);
                                }

                                if (KanevaGlobals.EnableEventKMails)
                                {
                                    requestId = GetUserFacade.GetTrackingRequestId(requestId, Constants.cREQUEST_TRACKING_REQUEST_TYPE_ID_EVENT_BLAST, KanevaWebGlobals.CurrentUser.UserId, evtGameId, evt.CommunityId);
                                    string requestTypeSentIdPM = GetUserFacade.InsertTrackingTypeSent (requestId, Constants.cREQUEST_TRACKING_MESSAGE_TYPE_ID_PM, ei.UserId);

                                    Event.EventMessage em = GetEventFacade.GetEventMessageDetails (Event.eMESSAGE_TYPE.EventOwnerBlastNotification, CurrentEvent, KanevaWebGlobals.CurrentUser.Username, ResolveUrl ("~/mykaneva/events/eventDetail.aspx"), txtEventBlast.Text, requestTypeSentIdPM);

                                    // Send them a Kmail
                                    message = new Message (0, KanevaWebGlobals.CurrentUser.UserId, ei.UserId, em.Subject, em.Message,
                                        new DateTime (), 0, (int) Constants.eMESSAGE_TYPE.PRIVATE_MESSAGE, 0, "U", "S");

                                    // Send KMail
                                    GetUserFacade.InsertMessage (message);
                                }
                            }
                        }
                    }
                    else // If attendee is adding a blast, then email the event owner
                    {
                        try
                        {
                            Event evt = GetEventFacade.GetEvent(eventId);
                            Community community = GetCommunityFacade.GetCommunity(evt.CommunityId);
                            int evtGameId = community.WOK3App.GameId;

                            string requestId = string.Empty;

                            User eventOwner = GetUserFacade.GetUser (CurrentEvent.UserId);
                            if (eventOwner.NotificationPreference.NotifyEventInvites)
                            {
                                requestId = GetUserFacade.GetTrackingRequestId(requestId, Constants.cREQUEST_TRACKING_REQUEST_TYPE_ID_EVENT_BLAST, KanevaWebGlobals.CurrentUser.UserId, evtGameId, evt.CommunityId);
                                string requestTypeSentIdEmail = GetUserFacade.InsertTrackingTypeSent(requestId, Constants.cREQUEST_TRACKING_MESSAGE_TYPE_ID_EMAIL, eventOwner.UserId);

                                MailUtilityWeb.SendEventEmail (CurrentEvent, Event.eMESSAGE_TYPE.EventAttendeeBlastNotification.ToString (), eventOwner.Email, eventOwner.UserId, KanevaWebGlobals.CurrentUser.UserId, txtEventBlast.Text, requestTypeSentIdEmail);
                            }

                            if (KanevaGlobals.EnableEventKMails)
                            {
                                requestId = GetUserFacade.GetTrackingRequestId(requestId, Constants.cREQUEST_TRACKING_REQUEST_TYPE_ID_EVENT_BLAST, KanevaWebGlobals.CurrentUser.UserId, evtGameId, evt.CommunityId);
                                string requestTypeSentIdPM = GetUserFacade.InsertTrackingTypeSent (requestId, Constants.cREQUEST_TRACKING_MESSAGE_TYPE_ID_PM, eventOwner.UserId);

                                Message message = new Message ();
                                Event.EventMessage em = GetEventFacade.GetEventMessageDetails (Event.eMESSAGE_TYPE.EventAttendeeBlastNotification, CurrentEvent, KanevaWebGlobals.CurrentUser.Username, ResolveUrl ("~/mykaneva/events/eventDetail.aspx"), txtEventBlast.Text, requestTypeSentIdPM);

                                // Send them a Kmail to the event owner
                                message = new Message (0, KanevaWebGlobals.CurrentUser.UserId, CurrentEvent.UserId, em.Subject, em.Message,
                                    new DateTime (), 0, (int) Constants.eMESSAGE_TYPE.PRIVATE_MESSAGE, 0, "U", "S");

                                // Send KMail
                                GetUserFacade.InsertMessage (message);
                            }
                        }
                        catch { }
                    }


                    // GA Event Call
                    Page.ClientScript.RegisterClientScriptBlock (GetType (), "gaEventBlastAddedEvent", "callGAEvent('Category','EventBlast','TextBlast');", true);
                }

                txtEventBlast.Text = "";
                BindEventBlasts (eventId);
            }
            catch { }
        }

        #endregion Event Handlers

        #region Properties

        public Event CurrentEvent
        {
            get
            {
                try
                {
                    if (this.currentEvent.EventId == 0)
                    {
                        if (HttpContext.Current.Items["event"] != null)
                        {
                            this.currentEvent = (Event) (HttpContext.Current.Items["event"]);
                        }
                        else
                        {
                            this.currentEvent = GetEventFacade.GetEvent (Convert.ToInt32 (Request["event"]));
                        }
                    }
                }
                catch
                {
                    return new Event (); ;
                }

                return this.currentEvent;
            }
        }

        private bool IsUserAttendingEvent
        {
            get{
                try
                {
                    if (this.isUserAttendingEvent == null)
                    {
                        this.isUserAttendingEvent = GetEventFacade.IsUserAttendingEvent (KanevaWebGlobals.CurrentUser.UserId, this.CurrentEvent.EventId);
                    }

                    return (bool)this.isUserAttendingEvent;
                }
                catch
                {
                    return false;
                }
            }
        }

        public void Refresh ()
        {
            InitBlasts ();
            BindEventBlasts ();
        }

        #endregion Properties

        #region Declerations

        private Event currentEvent = new Event();
        private bool? isUserAttendingEvent = null;

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

        #endregion Declerations

    }
}