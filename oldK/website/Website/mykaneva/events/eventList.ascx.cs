///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using MagicAjax;
using System.Web.Security;
using log4net;
using System.IO;
using System.Net;

using System.Collections.Generic;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.mykaneva.events
{
  public partial class eventList : KlausEnt.KEP.Kaneva.usercontrols.BaseUserControl

  {
    protected void Page_Load(object sender, EventArgs e)
    {
      BindData();
    }

    private void BindData()
    {
      //
      // Events
      ShowEvents ();
    }

    /// <summary>
    /// ShowEvents
    /// </summary>
    private void ShowEvents()
    {
      // Admin events
      string eventsFilterAdmin = " AND premium = 2 ";
      PagedDataTable dtEventsAdmin = EventsUtility.GetEventsMyKaneva(KanevaWebGlobals.CurrentUser.UserId, Constants.EVENTS_ALL, "start_time ASC", 1, 10, true, false, eventsFilterAdmin);

      // Premium events
      string eventsFilterPremium = " AND premium = 1 ";
      PagedDataTable dtEventsPremium = EventsUtility.GetEventsMyKaneva(KanevaWebGlobals.CurrentUser.UserId, Constants.EVENTS_ALL, "start_time ASC", 1, 3, true, false, eventsFilterPremium);

      // Regular events
      string eventsFilter = " AND premium = 0 ";
      PagedDataTable dtEvents = EventsUtility.GetEventsMyKaneva(KanevaWebGlobals.CurrentUser.UserId, Constants.EVENTS_ALL, "start_time ASC", 1, 3, true, false, eventsFilter);
            
      if (dtEvents.Rows.Count.Equals(0) )
      {
        lblNoEvents.Visible = true;
      }
      else
      {
        if (dtEventsAdmin.Rows.Count > 0)
        {
          rptEventsAdmin.DataSource = dtEventsAdmin;
          rptEventsAdmin.DataBind();
        }

        if (dtEventsPremium.Rows.Count > 0)
        {
          rptEventsPremium.DataSource = dtEventsPremium;
          rptEventsPremium.DataBind();
        }

        if (dtEvents.Rows.Count > 0)
        {
          rptEvents.DataSource = dtEvents;
          rptEvents.DataBind();
        }
      
      }

    }

    /// <summary>
    /// Repeater rptEvents ItemDataBound Event Handler
    /// </summary>
    protected void rptEvents_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
    {
      string x = e.Item.ToString();

      // Execute the following logic for Items and Alternating Items.
      if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
      {        
        string eventTitle = DataBinder.Eval(e.Item.DataItem, "Name").ToString();
        int eventId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "event_id"));
        int userId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "user_id"));
        string eventStart = Convert.ToDateTime(DataBinder.Eval(e.Item.DataItem, "start_time")).ToString("MM/dd/yy h:mm tt");
        string location = DataBinder.Eval(e.Item.DataItem, "location").ToString();
        int communityId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "user_id"));
        string username = DataBinder.Eval(e.Item.DataItem, "Host").ToString();
        int premium = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "premium"));        

        
        ((HyperLink)e.Item.FindControl("hlEventName")).Text = eventTitle;
        ((HyperLink)e.Item.FindControl("hlEventName")).NavigateUrl = "~/mykaneva/events/eventDetail.aspx?event=" + eventId;

        ((HyperLink)e.Item.FindControl("hlEventEdit")).Text = "Edit";
        ((HyperLink)e.Item.FindControl("hlEventEdit")).NavigateUrl = "~/mykaneva/events/eventForm.aspx?event=" + eventId;
        ((HyperLink)e.Item.FindControl("hlEventEdit")).Visible = userId == KanevaWebGlobals.CurrentUser.UserId ? true : false;

        ((Literal)e.Item.FindControl("litEventWhen")).Text = eventStart;
        // Convert.ToDateTime(DataBinder.Eval (Container.DataItem, "start_time")).ToString("MM/dd/yy h:mm tt")

        ((HyperLink)e.Item.FindControl("hlEventWhere")).Text = location;
        ((HyperLink)e.Item.FindControl("hlEventWhere")).NavigateUrl = "~/kgp/playwok.aspx?goto=C"+ communityId +"&ILC=HO3D&link=hangoutnow";

        ((HyperLink)e.Item.FindControl("hlEventWho")).Text = username;
        ((HyperLink)e.Item.FindControl("hlEventWho")).NavigateUrl = GetPersonalChannelUrl(username).ToString(); ;
      }
    }

    // private 
    private User Current_User = KanevaWebGlobals.CurrentUser;

  }
}