<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="eventList.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.mykaneva.events.eventList" %>
<div class="dialog">
<div class="content">
<div class="t"></div>

<h1>Events Calendar<a id="A1" href="~/mykaneva/events/eventForm.aspx" runat="server" class="addBTN nohover png">Create Event</a></h1>

<div id="events">	 

<asp:Label EnableViewState="False" runat="Server" id="lblNoEvents" Visible="False" Text="<br/><center>You have 0 upcoming events.</center>"/>

<!-- Admin Events -->
<asp:repeater id="rptEventsAdmin" runat="server"  onitemdatabound="rptEvents_ItemDataBound"  >
<headertemplate><dl id="sticky"><dt>From Kaneva</dt></headertemplate>
<itemtemplate>
<dd>
<ul>
<li><asp:HyperLink ID="hlEventName" runat="server"></asp:HyperLink> <asp:HyperLink ID="hlEventEdit" runat="server"></asp:HyperLink></li>
<li>When: <asp:Literal ID="litEventWhen" runat="server"></asp:Literal></li>
<li>Where: <asp:HyperLink ID="hlEventWhere" runat="server"></asp:HyperLink></li>
<li>Who:<asp:HyperLink ID="hlEventWho" runat="server"></asp:HyperLink></li>
</ul>
</dd>
</itemtemplate>
<footertemplate></dl></footertemplate>	
</asp:repeater>		
									   
<!-- Non-Admin Events -->
<asp:repeater id="rptEventsPremium" runat="server" onitemdatabound="rptEvents_ItemDataBound" >
<headertemplate><dl id=""><dt>Premium Events</dt></headertemplate>
<itemtemplate>
<dd style="background-color:#FAF9A4;">
<ul>
<li><asp:HyperLink ID="hlEventName" runat="server"></asp:HyperLink> <asp:HyperLink ID="hlEventEdit" runat="server"></asp:HyperLink></li>  
<li>When: <asp:Literal ID="litEventWhen" runat="server"></asp:Literal></li>
<li>Where: <asp:HyperLink ID="hlEventWhere" runat="server"></asp:HyperLink></li>
<li>Who:<asp:HyperLink ID="hlEventWho" runat="server"></asp:HyperLink></li>
</ul>
</dd> 
</itemtemplate>	
<footertemplate></dl></footertemplate>	
</asp:repeater>											   

<!-- Non-Admin Events -->
<asp:repeater id="rptEvents" runat="server" onitemdatabound="rptEvents_ItemDataBound" >
<headertemplate><dl id=""><dt>Events</dt></headertemplate>
<itemtemplate>
<dd>
<ul>
<li><asp:HyperLink ID="hlEventName" runat="server"></asp:HyperLink> <asp:HyperLink ID="hlEventEdit" runat="server"></asp:HyperLink></li>  
<li>When: <asp:Literal ID="litEventWhen" runat="server"></asp:Literal></li>
<li>Where: <asp:HyperLink ID="hlEventWhere" runat="server"></asp:HyperLink></li>
<li>Who:<asp:HyperLink ID="hlEventWho" runat="server"></asp:HyperLink></li>
</ul>
</dd> 
</itemtemplate>	
<footertemplate></dl></footertemplate>	
</asp:repeater>	


</div>					

</div>
<div class="b"><div></div></div>
</div>
