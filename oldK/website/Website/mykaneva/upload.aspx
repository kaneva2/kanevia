<%@ Page language="c#" MasterPageFile="~/masterpages/GenericPageTemplate.Master" Codebehind="upload.aspx.cs" ValidateRequest="false"  AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.upload" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="cntUpload" runat="server" ContentPlaceHolderID="cph_Body">					

<LINK href="../css/home.css" type="text/css" rel="stylesheet">
<LINK href="../css/kanevaSC.css" type="text/css" rel="stylesheet">
<LINK href="../css/friends.css" type="text/css" rel="stylesheet">
<LINK href="../css/kanevaText.css" type="text/css" rel="stylesheet">
<LINK href="../css/kanevaBroadBand.css" type="text/css" rel="stylesheet">
<LINK href="../css/account.css" type="text/css" rel="stylesheet">
<LINK href="../css/new.css" type="text/css" rel="stylesheet">
<LINK href="../css/base/buttons.css" type="text/css" rel="stylesheet">
<script src="../jscript/kaneva.js" type="text/javascript"></script>
<script src="../jscript/prototype.js" type="text/javascript"></script>
<script src="../jscript/multifile.js" type="text/javascript"></script>
<script language="javascript" src="../jscript/dw_tooltip/dw_event.js"></script>
<script language="javascript" src="../jscript/dw_tooltip/dw_viewport.js"></script>
<script language="javascript" src="../jscript/dw_tooltip/dw_tooltip.js"></script>
<style>div#tipDiv {
  position:absolute; 
  visibility:hidden; 
  left:100px; 
  top:-300px; 
  z-index:10000;
  background-color:#FFFFCC; 
  border:1px solid #336; 
  width:150px; 
  padding:4px;
  color:#000; 
  font-size:11px; 
  line-height:1.2;
}
</style>
<script type="text/javascript">
<!--
window.onload = ttINIT;

function ttINIT ()
{	
	Tooltip.init();	  
	OnLoad();	 
}

function agreementChanged(cbAgreement, source)
{				
	var files_table = $('files_table');
	var UPLOAD = 1;
	var YOUTUBE = 2;
	var TV = 3;
	var WIDGET = 4;
	var GAME = 5;
	
	switch(source)
	{
		case UPLOAD:
			if (cbAgreement.checked)
			{
				if (files_table.rows[2].cells[1].innerText != '')
				{
					$j('#btnUpload').prop('disabled', false);
					$j('#btnUpload').attr("src",imgUploadBtn[0].src);
				}
				else
				{
					$j('#btnUpload').prop('disabled', true);
					$j('#btnUpload').attr("src",imgUploadBtn[1].src);
				}
			}
			else
			{
				$j('#btnUpload').prop('disabled', true);
				$j('#btnUpload').attr("src",imgUploadBtn[1].src);
			}
			break;
		case YOUTUBE:
			if (cbAgreement.checked)
			{
				$j('#btnYouTubeEmbed').prop('disabled', false);
				$j('#btnYouTubeEmbed').attr("src",imgYouTubeUploadBtn[0].src);
			}
			else
			{
				$j('#btnYouTubeEmbed').prop('disabled', true);
				$j('#btnYouTubeEmbed').attr("src",imgYouTubeUploadBtn[1].src);
			}
			break;
		case TV:
			if (cbAgreement.checked)
			{
				$j('#btnTVEmbed').prop('disabled', false);
				$j('#btnTVEmbed').attr("src",imgTVUploadBtn[0].src);
			}
			else
			{
				$j('#btnTVEmbed').prop('disabled', true);
				$j('#btnTVEmbed').attr("src",imgTVUploadBtn[1].src);
			}
			break;
		case WIDGET:
			if (cbAgreement.checked)
			{
				$j('#btnWidgetEmbed').prop('disabled', false);
				$j('#btnWidgetEmbed').attr("src",imgWidgetUploadBtn[0].src);
			}
			else
			{
				$j('#btnWidgetEmbed').prop('disabled', true);
				$j('#btnWidgetEmbed').attr("src",imgWidgetUploadBtn[1].src);
			}
			break;
		case GAME:
			if (cbAgreement.checked)
			{
				$j('#btnAddGame').prop('disabled', false);
				$j('#btnAddGame').attr("src",imgWidgetUploadBtn[0].src);
			}
			else
			{
				$j('#btnAddGame').prop('disabled', true);
				$j('#btnAddGame').attr("src",imgWidgetUploadBtn[1].src);
			}
			break;
	}

}

// get button images
var imgAddBtn = new Array();
imgAddBtn[0] = new Image(139,30); 
imgAddBtn[0].src = '../images/button_addtolist.gif'; 
imgAddBtn[1] = new Image(139,30);
imgAddBtn[1].src = '../images/button_addtolist_dis.gif';

var imgUploadBtn = new Array();
imgUploadBtn[0] = new Image(185,40); 
imgUploadBtn[0].src = '../images/button_uploadmedia.gif'; 
imgUploadBtn[1] = new Image(185,40);
imgUploadBtn[1].src = '../images/button_uploadmedia_dis.gif';

var imgYouTubeUploadBtn = new Array();
imgYouTubeUploadBtn[0] = new Image(185,40); 
imgYouTubeUploadBtn[0].src = '../images/button_addvideo.gif'; 
imgYouTubeUploadBtn[1] = new Image(185,40);
imgYouTubeUploadBtn[1].src = '../images/button_addvideo_dis.gif';

var imgTVUploadBtn = new Array();
imgTVUploadBtn[0] = new Image(185,40); 
imgTVUploadBtn[0].src = '../images/button_addtv.gif'; 
imgTVUploadBtn[1] = new Image(185,40);
imgTVUploadBtn[1].src = '../images/button_addtv_dis.gif';

var imgWidgetUploadBtn = new Array();
imgWidgetUploadBtn[0] = new Image(185,40); 
imgWidgetUploadBtn[0].src = '../images/button_addwidget.gif'; 
imgWidgetUploadBtn[1] = new Image(185,40);
imgWidgetUploadBtn[1].src = '../images/button_addwidget_dis.gif';

var isAddBtnDisabled = true;

function doBtnAddClick(obj)
{	  						 
	if (!isAddBtnDisabled)
		$j('#btnAdd').click();
}
function showAlert(msg)
{
	alert(msg);
	return false;
}

<asp:literal id="litScript" runat="server"/>

// this code fixes an IE bug where it submits image button coordinates as floats instead
// of int. Without this, submission for Youtube, TV Channels and Widgets. 
Sys.WebForms.PageRequestManager.getInstance()._origOnFormActiveElement = Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive;
Sys.WebForms.PageRequestManager.getInstance()._onFormElementActive = function (element, offsetX, offsetY) {
	if (element.tagName.toUpperCase() === 'INPUT' && element.type === 'image') {
		offsetX = Math.floor(offsetX);
		offsetY = Math.floor(offsetY);
	}
	this._origOnFormActiveElement(element, offsetX, offsetY);
};

//-->
</script>

<input id="hidTorrentSize" type="hidden" value="1" name="hidTorrentSize" runat="server">
<div id="divLoading" sytle="text-align:center;">
	<table height="300" cellSpacing="0" cellPadding="0" border="0" style="margin:auto;">
		<tr>
			<th class="loadingText" id="divLoadingText">
				Loading...
				<div style="display:none"><IMG src="../images/progress_bar.gif"></div>
			</th>
		</tr>
	</table>
</div>
<div id="divData" style="display:none;">
	<span id="spnToolTipHolder"></span>
	<asp:updatepanel id="upHidePurchase" runat="server">
		<contenttemplate>
			<asp:linkbutton id="btnFileBrowse" onclick="FileBrowse_Click" runat="server" CausesValidation="False" style="display:none;"></asp:linkbutton>
			<asp:linkbutton id="btnAddFileToQueue" onclick="FileAdd_Click" runat="server" causesvalidation="False" style="display:none;"></asp:linkbutton>
		</contenttemplate>
	</asp:updatepanel>
	
	<table cellSpacing="0" cellPadding="0" class="newcontainer" align="center" border="0">
		<tr>
			<td>
				<table cellSpacing="0" cellPadding="0" width="100%" border="0" class="newcontainerborder">
					<tr>
						<td class=""></td>
						<td class=""></td>
						<td class=""></td>
					</tr>
					<tr>
						<td class=""><IMG height="1" src="~/images/spacer.gif" width="1" runat="server"></td>
						<td class="newdatacontainer" vAlign="top">
							<table cellSpacing="0" cellPadding="0" width="100%" border="0">
								<!-- TOP STATUS BAR -->
								<tr>
									<td>
										<div id="pageheader">
											<table cellSpacing="0" cellPadding="0" width="99%" border="0">
												<tr>
													<td nowrap="nowrap" align="left">
														<table cellpadding="0" cellspacing="0" border="0">
															<tr>
																<td><h1>Add Media</h1></td>
																<td width="20px">&nbsp;</td>
																<td>
																	<asp:hyperlink id="back" runat="server" visible="false" tooltip="return to Previous location"></asp:hyperlink>
																</td>
															</tr>
														</table>
													</td>
													<td vAlign="middle" align="right">
														<table cellSpacing="0" cellPadding="0" width="690" border="0">
															<tr>
																<td class="headertout" width="175"></td>
																<td class="searchnav" width="260"></td>
																<td align="left" width="130"></td>
																<td class="searchnav" noWrap align="right" width="210"><A id="hlSupportedFormat" onclick="javascript:window.open('../asset/supportedMediaFiles.aspx','add','toolbar=no,width=600,height=450,menubar=no,scrollbars=yes,status=yes').focus();return false;"
																		href="#">Supported file formats</A>
																</td>
															</tr>
														</table>
													</td>																					
												</tr>
											</table>
										</div>
									</td>
								</tr>
								<!-- END TOP STATUS BAR -->
								<tr>
									<td width="968" align="left"  valign="top">
										<table cellpadding="0" cellspacing="0" border="0"  width="100%">
											<tr>
												<td width="968" valign="top">
													<table border="0" cellspacing="0" cellpadding="0" width="100%">
														<tr>
															<!-- START CONTENT -->
															<td valign="top" align="center">
																<table border="0" cellspacing="0" cellpadding="0">
																	<tr>
																		<td width="968">
																			<div class="module whitebg fullpage">
																				<span class="ct"><span class="cl"></span></span>
																				<table cellSpacing="0" cellPadding="0" align="center" class="wizform" border="0">
																					<tr>
																						<td>
																							<div class="frame">
																								<span class="ct"><span class="cl"></span></span>
																								<!-- TOOLBAR -->
																								<table id="mediauploadNav" cellSpacing="0" cellPadding="0" border="0">
																									<tr>
																										<td valign="top">
																											<ul>
																												<li id="liUpload" runat="server"><asp:linkbutton id="lbUpload" runat="server" CausesValidation="False" enabled="false" CommandName="tabChoice1" CommandArgument="1" text="Add Photos" /></li>
																												<li id="liYouTube" runat="server"><asp:linkbutton id="lbYouTubeEmbed" runat="server" CausesValidation="False" CommandName="tabChoice2" CommandArgument="2" text="Add YouTube" /></li>
																												<li id="liFlash" runat="server"><asp:linkbutton id="lbFlashWidgets" runat="server" CausesValidation="False" CommandName="tabChoice4" CommandArgument="4" text="Add Flash Widgets" /></li>
																												<li id="liFlashGame" runat="server"><asp:linkbutton id="lbFlashGame" runat="server" CausesValidation="False" CommandName="tabChoice5" CommandArgument="5" text="Add Flash Games" /></li>
																											</ul>
																										</td>
																									</tr>
																								</table>																	
																								<span class="cb"><span class="cl"></span></span>
																							</div>
																							<div class="shadowsep"></div>
																						</td>
																					</tr>
																					<!-- Begin the Upload body of the page -->
																					<tr id="tbrUpload" runat="server" style="display:none;">
																						<td align="left">
																							<table cellSpacing="0" cellPadding="0" width="940" border="0">
																								<tr>
																									<td align="center" width="440">
																										<div class="module">
																											<span class="ct"><span class="cl"></span></span>
																											<h2 id="uploadOne">
																												Details
																												<span class="hlink note"><strong><span class="alertmessage">*</span>Required Fields</strong></span>
																											</h2>
																											<br>
																											<table cellSpacing="0" cellPadding="2" width="350" border="0" class="evenpadding">
																												<tr>
																													<td colSpan="2">Select My File:<span class="alertmessage">*</span></td>
																												</tr>
																												<tr>
																													<td>
																														<input class="formKanevaText" id="file_0" style="WIDTH: 300px" type="file" size="43" name="file_0" runat="server" clientidmode="Static" width="300px"> 
																														<input id="hidFile" type="hidden" name="hidFile" runat="server" clientidmode="static">
																														<asp:updatepanel id="upFileName" runat="server">
																															<contenttemplate>	
																																<span id="spnFileErr" style="color:red" runat="server" clientidmode="static"></span>
																															</contenttemplate>
																														</asp:updatepanel>
																														<br>
																													</td>
																													<td vAlign="top" align="right">
																														<IMG onmouseover="doTooltip(event,'Max file size: 100 MB. Max length: 10 minutes. You can upload a maximum of 5 files.')" onmouseout="hideTip();" src="~/images/button_tooltip.gif" border="0" runat="server">
																													</td>
																												</tr>
																												<tr>
																													<td class="insideTextNoBold" noWrap colSpan="2">
																														Title:<span class="alertmessage">*</span>
																													</td>
																												</tr>
																												<tr>
																													<td>
																														<asp:updatepanel id="upFileTitle" runat="server">
																															<contenttemplate>
																																<asp:textbox class="formKanevaText" id="txtTitle" runat="server" clientidmode="static" Width="300" name="txtTitle" maxlength="300"></asp:textbox>&nbsp; 
																																<asp:requiredfieldvalidator id="rfvTitle" runat="server" controltovalidate="txtTitle" text="* Please enter a title." errormessage="Title is a required field." display="Dynamic" clientidmode="static"></asp:requiredfieldvalidator>
																															</contenttemplate>
																														</asp:updatepanel>
																														<br>
																													</td>
																													<td vAlign="top" align="right">
																														<IMG onmouseover="doTooltip(event,'The Title defaults to your file name.  You may change it now, or after your upload it.')" onmouseout="hideTip();" src="~/images/button_tooltip.gif" border="0" runat="server">
																													</td>
																												</tr>
																												
																												<TR id="rwPattern">
																													<TD colSpan="2">
																														<asp:updatepanel id="upPattern" runat="server">
																															<contenttemplate>
																																<TABLE border="0" cellpadding="0" cellspacing="0">
																																	<TR>
																																		<TD vAlign="top" align="right">
																																			<asp:RadioButton id="rdoPhoto" checked runat="server" AutoPostBack="True" GroupName="pictpatrn" />
																																		</TD>
																																		<TD><asp:Label id="lblPhoto" runat="server">Photo � for displaying on your Profile and in-world.</asp:Label></TD>
																																	</TR>
																																	<TR>
																																		<TD vAlign="top" align="right">
																																			<asp:RadioButton id="rdoPattern" runat="server" name="rdoPattern" AutoPostBack="true" GroupName="pictpatrn" />
																																		</TD>
																																		<TD><asp:Label id="lblPattern" runat="server">Pattern � for texturing world furniture, walls and floors.</asp:Label></TD>
																																	</TR>
																																</TABLE>
																															</contenttemplate>
																														</asp:updatepanel>	
																													</TD>
																												</TR>
																												
																												<tr>
																													<td class="insideTextNoBold" noWrap colSpan="2">Select a Category:</td>
																												</tr>
																												<tr>
																													<td class="insideTextNoBold">
																														<asp:updatepanel id="upCategories" runat="server">
																															<contenttemplate>
																																<asp:dropdownlist class="insideTextNoBold" id="ddlCategories" runat="server" clientidmode="Static" enabled="False" Width="300" name="ddlCategories" datatextfield="name" datavaluefield="category_id"><asp:listitem></asp:listitem></asp:dropdownlist>
																																<asp:requiredfieldvalidator id="rfvCategory" runat="server" controltovalidate="ddlCategories" text="* Please select a category." errormessage="Category is a required field." display="Dynamic"></asp:requiredfieldvalidator>
																															</contenttemplate>
																														</asp:updatepanel>
																														<br>
																													</td>
																													<td vAlign="top" align="right">
																														<IMG id="Img1" onmouseover="doTooltip(event,'Selecting a Category will help others find your file.')" onmouseout="hideTip();" src="~/images/button_tooltip.gif" border="0" runat="server">
																													</td>
																												</tr>
																												<tr>
																													<td class="insideTextNoBold" noWrap colSpan="2">Tags/Keywords:</td>
																												</tr>
																												<tr>
																													<td>
																														<asp:updatepanel id="updatepanel1" runat="server">
																															<contenttemplate>
																																<INPUT class="formKanevaText" id="txtTags" style="width:300px" type="text" maxLength="250" name="txtTags" runat="server" clientidmode="static"> 
																																<asp:regularexpressionvalidator id="revTags" runat="server" controltovalidate="txtTags" text="* Use spaces between words" display="Dynamic" enableclientscript="True"></asp:regularexpressionvalidator>&nbsp;&nbsp;(use spaces between words) 
																															</contenttemplate>
																														</asp:updatepanel>
																														<br>
																														<br>
																													</td>
																													<td vAlign="top" align="right">
																														<IMG id="Img2" onmouseover="doTooltip(event,'Tags are unique keywords that help to categorize your media and allow viewers to easily search. Separate each word with a space.')" onmouseout="hideTip();" src="~/images/button_tooltip.gif" border="0" runat="server">
																													</td>
																												</tr>
																												<tr>
																													<td class="insideTextNoBold" noWrap align="left" colSpan="2">Brief Description:</td>
																												</tr>
																												<tr>
																													<td>
																														<asp:textbox id="txtDescription" runat="server" clientidmode="static" Width="300" rows="3" textmode="MultiLine" wrap="True"></asp:textbox>
																														<br>
																														<br>
																													</td>
																													<td vAlign="top" align="right">
																														<IMG onmouseover="doTooltip(event,'This description is used in all listed views of your media.  You may also change it later from the Edit Media form.  Please limit your description to 1000 characters or less.')" onmouseout="hideTip();" src="~/images/button_tooltip.gif" border="0" runat="server">
																													</td>
																												</tr>
																												<tr>
																													<td class="textGeneralGray01" align="left" colSpan="2">
																														<table cellSpacing="0" cellPadding="0" width="100%" border="0">
																															<tr>
																																<td class="textGeneralGray01" width="44%">Make this media file:</td>
																																<td class="textGeneralGray01" width="23%">
																																	<input id="radPublic" type="radio" value="radPublic" name="radType" runat="server" clientidmode="static" checked></input>
																																	<label>Public</label>
																																</td>
																																<td class="textGeneralGray01" width="23%">
																																	<input id="radPrivate" type="radio" value="radPrivate" name="radType" runat="server" clientidmode="static"></input>
																																	<label>Private</label>
																																</td>
																																<td align="right" width="10%">
																																	<IMG onmouseover="doTooltip(event,'You may change access to this file once it is uploaded.')"
																																		onmouseout="hideTip();" src="~/images/button_tooltip.gif" border="0" runat="server">
																																</td>
																															</tr>
																															<tr>
																																<td class="note" colSpan="4">
																																	Media marked as public can be both viewed and shared by Kaneva members. Media 
																																	marked as Private cannot be shared and can only be viewed when you're signed on 
																																	to your account.
																																</td>
																															</tr>
																														</table>
																														<br>
																													</td>
																												</tr>
																												<tr>
																													<td class="textGeneralGray01" align="left" colSpan="2">
																														<table id="UploadTags" cellSpacing="0" cellPadding="0" width="100%" border="0" runat="server"
																															visible="false">
																															<tr>
																																<td class="textGeneralGray01" colSpan="2">
																																	<asp:literal id="ltl_Instructions" runat="server"></asp:literal>
																																</td>
																															</tr>
																															<tr id="tagRow1" runat="server" visible="false">
																																<td class="textGeneralGray01" width="30%">
																																	<asp:label id="lbl_Description_1" runat="server" backcolor="Transparent"></asp:label>
																																	<asp:label id="lblASTReq1" runat="server" visible="false" cssclass="alertmessage">*</asp:label>
																																</td>
																																<td class="textGeneralGray01">
																																	<asp:updatepanel id="upTag1" runat="server">
																																		<contenttemplate>
																																			<asp:textbox id="tbx_TagField1" runat="server" clientidmode="static"></asp:textbox>
																																			<asp:requiredfieldvalidator id="rfv_tagField1" runat="server" visible="False" controltovalidate="tbx_TagField1"
																																				errormessage="required" display="Dynamic"></asp:requiredfieldvalidator>
																																		</contenttemplate>
																																	</asp:updatepanel>
																																</td>
																															</tr>
																															<tr id="tagRow2" runat="server" visible="false">
																																<td class="textGeneralGray01" width="30%">
																																	<asp:label id="lbl_Description_2" runat="server" backcolor="Transparent"></asp:label>
																																	<asp:label id="lblASTReq2" runat="server" visible="false" cssclass="alertmessage">*</asp:label>
																																</td>
																																<td class="textGeneralGray01">
																																	<asp:updatepanel id="upTag2" runat="server">
																																		<contenttemplate>
																																			<asp:textbox id="tbx_TagField2" runat="server" clientidmode="static"></asp:textbox>
																																			<asp:requiredfieldvalidator id="rfv_tagField2" runat="server" visible="False" controltovalidate="tbx_TagField2"
																																				errormessage="required" display="Dynamic"></asp:requiredfieldvalidator>
																																		</contenttemplate>
																																	</asp:updatepanel>
																																</td>
																															</tr>
																															<tr id="tagRow3" runat="server" visible="false">
																																<td class="textGeneralGray01" wisth="30%">
																																	<asp:label id="lbl_Description_3" runat="server" backcolor="Transparent"></asp:label>
																																	<asp:label id="lblASTReq3" runat="server" visible="false" cssclass="alertmessage">*</asp:label>
																																</td>
																																<td class="textGeneralGray01">
																																	<asp:updatepanel id="upTag3" runat="server">
																																		<contenttemplate>
																																			<asp:textbox id="tbx_TagField3" runat="server" clientidmode="static"></asp:textbox>
																																			<asp:requiredfieldvalidator id="rfv_tagField3" runat="server" visible="False" controltovalidate="tbx_TagField3"
																																				errormessage="required" display="Dynamic"></asp:requiredfieldvalidator>
																																		</contenttemplate>
																																	</asp:updatepanel>
																																</td>
																															</tr>  
																															<tr id="tagRow4" runat="server" visible="false">
																																<td class="textGeneralGray01" with="30%">
																																	<asp:label id="lbl_Description_4" runat="server" backcolor="Transparent"></asp:label>
																																	<asp:label id="lblASTReq4" runat="server" visible="false" cssclass="alertmessage">*</asp:label>
																																</td>	
																																<td class="textGeneralGray01">
																																	<asp:updatepanel id="upTag4" runat="server">
																																		<contenttemplate>
																																			<asp:textbox id="tbx_TagField4" runat="server" clientidmode="static"></asp:textbox>
																																			<asp:requiredfieldvalidator id="rfv_tagField4" runat="server" visible="False" controltovalidate="tbx_TagField4"
																																				errormessage="required" display="Dynamic"></asp:requiredfieldvalidator>
																																		</contenttemplate>
																																	</asp:updatepanel>
																																</td>
																															</tr>
																															<tr id="tagRow5" runat="server" visible="false">
																																<td class="textGeneralGray01" width="30%">
																																	<asp:label id="lbl_Description_5" runat="server" backcolor="Transparent"></asp:label>
																																	<asp:label id="lblASTReq5" runat="server" visible="false" cssclass="alertmessage">*</asp:label>
																																</td>
																																<td class="textGeneralGray01">
																																	<asp:updatepanel id="upTag5" runat="server">
																																		<contenttemplate>
																																			<asp:textbox id="tbx_TagField5" runat="server" clientidmode="static"></asp:textbox>
																																			<asp:requiredfieldvalidator id="rfv_tagField5" runat="server" visible="False" controltovalidate="tbx_TagField5"
																																				errormessage="required" display="Dynamic"></asp:requiredfieldvalidator>
																																		</contenttemplate>
																																	</asp:updatepanel>
																																</td>
																															</tr>
																															<tr id="tagRow6" runat="server" visible="false">
																																<td class="textGeneralGray01" width="30%">
																																	<asp:label id="lbl_Description_6" runat="server" backcolor="Transparent"></asp:label>
																																	<asp:label id="lblASTReq6" runat="server" visible="false" cssclass="alertmessage">*</asp:label>
																																</td>
																																<td class="textGeneralGray01">
																																	<asp:updatepanel id="upTag6" runat="server">
																																		<contenttemplate>
																																			<asp:textbox id="tbx_TagField6" runat="server" clientidmode="static"></asp:textbox>
																																			<asp:requiredfieldvalidator id="rfv_tagField6" runat="server" visible="False" controltovalidate="tbx_TagField6"
																																				errormessage="required" display="Dynamic"></asp:requiredfieldvalidator>
																																		</contenttemplate>
																																	</asp:updatepanel>
																																</td>
																															</tr>
																															<tr>
																																<td class="textGeneralGray01" colSpan="2">
																																	<input id="tbx_ChnlOwnr_Tags" type="hidden" name="tbx_ChnlOwnr_Tags" runat="server" clientidmode="static">
																																</td>
																															</tr>
																														</table>
																													</td>
																												</tr>
																												<tr>
																													<td class="textGeneralGray01" align="left" colSpan="2">
																														<input id="hidFileCount" type="hidden" name="hidFileCount">
																														<table cellSpacing="0" cellPadding="0" width="300" border="0" id="tblAccessPass" runat="server">
																															<tr>
																																<td class="insideTextNoBold" noWrap align="left" width="300">
																																	<IMG src="~/images/pass_content.gif" width="15" align="absBottom" runat="server"> <strong>
																																		Mark as <a id="lnk_AccessPass" href="#" runat="server" >Access Pass</a> content<span style="padding-left:24px"></strong> <input class="formKanevaCheck" id="chkRestricted" type="checkbox">
																																</td>
																															</tr>
																															<tr>
																																<td class="note" >
																																	Check this field if the media may not be appropriate for Kaneva Members who are 
																																	under the age of 18.
																																</td>
																															</tr>
																														</table>
																													</td>
																												</tr>
																											</table>
																											<span class="cb">
																												<span class="cl"></span>
																											</span>
																										</div>
																											
																									</td>
																									<td vAlign="top" width="180" background="../images/bg_upload_addtolist.jpg">
																										<h2 id="uploadTwo"></h2>
																										<asp:updatepanel id="upAddButton" runat="server">
																											<contenttemplate>
																												<INPUT id="hidValidateStatus" type="hidden" name="hidValidateStatus" runat="server">
																												<TABLE cellSpacing="0" cellPadding="0" width="180" border="0">
																													<TR>
																														<TD align="center">
																															<INPUT class="Filter2bold" id="btnAdd" style="left:-1000px;position:absolute" type="button"	value="add file" name="btnAdd" runat="server" clientidmode="static"> 
																																<IMG class="Filter2bold" id="imgAdd" onclick="javascript:doBtnAddClick(this);return false;" src="../images/button_addtolist_dis.gif" name="imgAdd" runat="server" clientidmode="static">
																														</TD>
																													</TR>
																													<TR>
																														<TD align="center"><BR>
																															<P class="note" style="WIDTH: 120px">Enter details then add file to the 
																																list.</P>
																														</TD>
																													</TR>
																												</TABLE>
																											</contenttemplate>
																										</asp:updatepanel>
																									</td>
																									<td vAlign="top" align="center" width="360">
																										<div class="module ">
																											<span class="ct">
																												<span class="cl"></span>
																											</span>
																											<h2 id="uploadThree">
																												Pictures to Upload
																											</h2>
																											<p class="note" style="PADDING-LEFT: 25px; PADDING-BOTTOM: 10px; PADDING-TOP: 10px"
																												align="left">A maximum of 5 files can be uploaded at once.</p>
																											<table class="data" id="files_table" cellSpacing="0" cellPadding="7" width="320" border="0">
																												<tr bgColor="#f3f3f3">	
																														<th width="10"></th>
																														<th class="name" width="200">
																															File Name</th>
																														<th noWrap width="60">
																															File Type</th>
																														<th align="center" width="50">
																															Remove</th>	
																												</tr>																					
																												<tr>
																													<td colSpan="4">
																														<script>
																															<!-- Create an instance of the multiSelector class, pass it the output target and the max number of files -->
																															var multi_selector = new MultiSelector( document.getElementById( 'files_table' ), 5 );
																														</script>
																													</td>
																												</tr>
																												<tr>
																													<td class="note" align="center">1</td>
																													<td></td>
																													<td align="center"></td>
																													<td align="center"></td>
																												</tr>
																												<tr>
																													<td class="note" align="center">2</td>
																													<td></td>
																													<td align="center"></td>
																													<td align="center"></td>
																												</tr>
																												<tr>
																													<td class="note" align="center">3</td>
																													<td></td>
																													<td align="center"></td>
																													<td align="center"></td>
																												</tr>
																												<tr>
																													<td class="note" align="center">4</td>
																													<td></td>
																													<td align="center"></td>
																													<td align="center"></td>
																												</tr>
																												<tr>
																													<td class="note" align="center">5</td>
																													<td></td>
																													<td align="center"></td>
																													<td align="center"></td>
																												</tr>
																											</table>
																											<br>
																											<br>
																											<table cellSpacing="0" cellPadding="0" width="350" border="0">
																												<tr>
																													<td align="right">
																														<table cellSpacing="0" cellPadding="5" width="330" border="0">
																															<tr>
																																<td>
																																	<input class="formKanevaCheck" id="cbAgreement" disabled onclick="javascript:agreementChanged(this, 1);" type="checkbox">
																																</td>
																																<td class="insideTextNoBold" noWrap align="right">
																																	I agree these files meet the Kaneva <A href="../overview/TermsAndConditions.aspx?subtab=trm" target="_resource">
																																		Terms &amp; Conditions</A>
																																</td>
																															</tr>
																														</table>
																													</td>
																												</tr>
																											</table>
																											<table cellSpacing="0" cellPadding="0" width="350" border="0">
																												<tr>
																													<td align="center">
																														<input id="btnUpload" disabled onclick="return UploadFiles();" type="image" src="~/images/button_uploadmedia_dis.gif"
																															name="btnUpload" runat="server" clientidmode="static" causesvalidation="false"> 
																													</td>
																												</tr>
																											</table>
																											<span class="cb">
																												<span class="cl"></span>
																											</span>
																										</div>
																										<br>
																										<br>
																										<table cellSpacing="0" cellPadding="0" width="370" border="0">
																											<tr>
																												<td align="center">
																													<table cellSpacing="0" cellPadding="0" width="300" border="0">
																														<tr>
																															<td class="note" vAlign="top" align="center">
																																Please note: Depending on file size, the upload may require several minutes. 
																																Having trouble with the upload?
																																<br>
																																<A runat="server" id="aHelp" target="_resource">Read our Troubleshooting guide</A>.
																																<br>
																																<br>
																																<br>
																																�Files must be less than <strong>100MB</strong>.
																																<br>
																																� Copyrighted media is NOT allowed.
																																<br>
																																� Users posting explicit material will be banned.
																																<br>
																															</td>
																														</tr>
																													</table>
																												</td>
																											</tr>
																										</table>
																									</td>								   
																								</tr>
																							</table>											 
																							<asp:updatepanel id="updatepanel2" runat="server">
																								<contenttemplate>
																									<div id="divSwfUpload" runat="server" clientidmode="static" style="display:none;top:-500;left:220;border:2px solid black;background-color:White;width:500px;height:300px;z-order:10;position:relative;">
																										<div style="width:500px;padding:20px;text-align:left;">
																											<h2>You Are Uploading a Flash (.swf) File</h2>
																											<br />
																											<p>If the file is an interactive Flash game, click Add Flash Game.</p>
																											<p style="text-align:center;"><asp:button id="btnAddFlashGame" runat="server" clientidmode="static" onclientclick="$('divSwfUpload').style.display='none';" causesvalidation="false" oncommand="tabClicked_Command" commandargument="5" text=" Add Flash Game" /></p>
																											<br /><br />  
																											<p>If the file is a Flash widget and NOT an interactive game, click Add Flash Widget.</p>
																											<p style="text-align:center;"><asp:button id="btnAddFlashWidget" runat="server" clientidmode="static" onclientclick="$('divSwfUpload').style.display='none';" causesvalidation="false" oncommand="tabClicked_Command" commandargument="4" text="Add Flash Widget" /></p>
																											<br /><br /><br />
																											<p style="text-align:center;"><input type="button" id="btnCancelFlashUpload" onclick="$('divSwfUpload').style.display='none';" value="      Close     " /></p>
																										</div>
																									</div>
																								</contenttemplate>
																							</asp:updatepanel>
																						</td>
																					</tr>
																					<!-- end the upload body of the page -->
																					<!-- begin youtube body -->
																					<tr id="tbrYouTube" runat="server" style="display:none;">
																						<td align="left">
																							<table cellSpacing="0" cellPadding="0" width="940" border="0">
																								<tr>
																									<td align="center" width="400">
																										<div class="module whitebg">
																											<span class="ct">
																												<span class="cl"></span>
																											</span>
																											<h2>
																												Video Details
																												<span class="hlink note">
																													<strong>
																														<span class="alertmessage">*</span>
																														Required Fields
																													</strong>
																												</span>
																											</h2>
																											<br>
																											<!-- this section builds the left hand information section for the YouTube configuration-->
																											<table cellSpacing="0" cellPadding="0" width="330" border="0" class="evenpadding">
																												<tr>
																													<td colSpan="2">
																														Video Embed Code:<span class="alertmessage">*</span>
																													</td>
																												</tr>
																												<tr>											  
																													<td>
																														<asp:updatepanel id="updatepanel60" runat="server">
																															<contenttemplate>
																																<asp:textbox class="formKanevaText" id="tbYouTubeLink"  runat="server" clientidmode="static" Width="300" name="YTtxtLink" rows="2" textmode="MultiLine"></asp:textbox>&nbsp; 
																																<asp:requiredfieldvalidator id="rfvtbYouTubeLink"  runat="server" clientidmode="static" validationgroup="youtube" controltovalidate="tbYouTubeLink" text="* paste your YouTube Embed Code" errormessage="YouTube embed code is a required field" display="Dynamic"></asp:requiredfieldvalidator>
																															</contenttemplate>
																														</asp:updatepanel>
																														<br>											   
																													</td>
																													<td vAlign="top" align="right">
																														<IMG id="Img3" onmouseover="doTooltip(event,'Make sure you have completely highlighted the correct embed link.')" onmouseout="hideTip();" src="~/images/button_tooltip.gif" border="0" runat="server">
																													</td>
																												</tr>											
																												<tr>
																													<td colSpan="2">
																														Title of Video:<span class="alertmessage">*</span>
																													</td>
																												</tr>
																												<tr>
																													<td>
																														<asp:updatepanel id="updatepanel3" runat="server">
																															<contenttemplate>
																																<asp:textbox class="formKanevaText" id="tbYouTubeTitle" runat="server" clientidmode="static" Width="300" name="YTtxtTitle" maxlength="300"></asp:textbox>&nbsp; 
																																<asp:requiredfieldvalidator id="rfvtbYouTubeTitle" runat="server" clientidmode="static" validationgroup="youtube" controltovalidate="tbYouTubeTitle" text="* Please enter a title." errormessage="Title is a required field." display="Dynamic"></asp:requiredfieldvalidator>
																															</contenttemplate>
																														</asp:updatepanel>
																														<br>
																													</td>
																													<td vAlign="top" align="right">
																														<IMG id="Img5" onmouseover="doTooltip(event,'A title for your YouTube movie is recommended.')"
																															onmouseout="hideTip();" src="~/images/button_tooltip.gif" border="0" runat="server">
																													</td>
																												</tr>
																												
																												<tr>
																													<td class="insideTextNoBold" noWrap colSpan="2">Select a Category:</td>
																												</tr>
																												<tr>
																													<td class="insideTextNoBold">
																														<asp:updatepanel id="updatepanel4" runat="server">
																															<contenttemplate>
																																<asp:dropdownlist class="insideTextNoBold" id="ddlCategoriesYT" runat="server" clientidmode="static" enabled="true" Width="300" name="ddlCategoriesYT" datatextfield="name" datavaluefield="category_id"></asp:dropdownlist>
																																<asp:requiredfieldvalidator id="rfvCatagoriesYT" runat="server" clientidmode="static" validationgroup="youtube" controltovalidate="ddlCategoriesYT" text="* Please select a category." errormessage="Category is a required field." display="Dynamic"></asp:requiredfieldvalidator>
																															</contenttemplate>
																														</asp:updatepanel>
																														<br>
																													</td>
																													<td vAlign="top" align="right">
																														<IMG id="Img4" onmouseover="doTooltip(event,'Selecting a Category will help others find your file.')" onmouseout="hideTip();" src="~/images/button_tooltip.gif" border="0" runat="server">
																													</td>
																												</tr>
																												
																												<tr>
																													<td colSpan="2">Tags/Keywords:</td>
																												</tr>
																												<tr>
																													<td>
																														<asp:updatepanel id="updatepanel5" runat="server">
																															<contenttemplate>
																																<INPUT class="formKanevaText" id="tbTagsYouTube" style="WIDTH: 300px" type="text" maxLength="250" name="tbTagsYouTube" runat="server"> 
																																<asp:regularexpressionvalidator id="REVtbTagsYouTube" runat="server" clientidmode="static" validationgroup="youtube" controltovalidate="tbTagsYouTube" text="* Use spaces between words" display="Dynamic" enableclientscript="True"></asp:regularexpressionvalidator>&nbsp;&nbsp;(use spaces between words) 
																															</contenttemplate>
																														</asp:updatepanel>
																														<br>
																														<br>
																													</td>
																													<td vAlign="top" align="right">
																														<IMG id="Img7" onmouseover="doTooltip(event,'Tags are unique keywords that help to categorize your media and allow viewers to easily search. Separate each word with a space.')"
																															onmouseout="hideTip();" src="~/images/button_tooltip.gif" border="0" runat="server">
																													</td>
																												</tr>
																												<tr>
																													<td class="insideTextNoBold" noWrap align="left" colSpan="2">Brief Description:</td>
																												</tr>
																												<tr>
																													<td>
																														<asp:updatepanel id="upYTdesc" runat="server">
																															<contenttemplate>
																																<asp:textbox id="tbBriefDescYouTube" runat="server" clientidmode="static" Width="300" wrap="True" textmode="MultiLine"
																																	rows="3"></asp:textbox>
																															</contenttemplate>
																														</asp:updatepanel>
																														<BR>
																														<BR>
																													</td>
																													<td vAlign="top" align="right">
																														<IMG id="Img8" onmouseover="doTooltip(event,'This description is used in all listed views of your media.  You may also change it later from the Edit Media form.  Please limit your description to 1000 characters or less.')"
																															onmouseout="hideTip();" src="~/images/button_tooltip.gif" border="0" runat="server">
																													</td>
																												</tr>
																												<tr>
																													<td class="textGeneralGray01" align="left" colSpan="2">
																														<asp:updatepanel id="upYTprivate" runat="server">
																															<contenttemplate>
																																<table cellSpacing="0" cellPadding="0" width="100%" border="0">
																																	<tr>
																																		<td class="textGeneralGray01" width="44%">Make this media file:</td>
																																		<td class="textGeneralGray01" width="23%">
																																			<input id="Radio1" type="radio" CHECKED value="radPublic" name="radTypeYT" runat="server"></input>
																																			<label>Public</label>
																																		</td>
																																		<td class="textGeneralGray01" width="23%">
																																			<input id="Radio2" type="radio" value="radPrivate" name="radTypeYT" runat="server" clientidmode="static"></input>
																																			<label>Private</label>
																																		</td>
																																		<td align="right" width="10%">
																																			<IMG id="Img9" onmouseover="doTooltip(event,'You may change access to this file once it is uploaded.')"
																																				onmouseout="hideTip();" src="~/images/button_tooltip.gif" border="0" runat="server">
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="note" colSpan="4">
																																			Media marked as public can be both viewed and shared by Kaneva members. Media 
																																			marked as Private cannot be shared and can only be viewed when you're signed on 
																																			to your account.
																																		</td>
																																	</tr>
																																</table>
																															</contenttemplate>
																														</asp:updatepanel>
																													</td>	
																												</tr>
																												<tr>
																													<td class="textGeneralGray01" align="left" colSpan="2">
																														<asp:updatepanel id="up2" runat="server">
																															<contenttemplate>
																																<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0" id="tblAccessPassYouTube" runat="server">
																																	<TR>
																																		<TD class="insideTextNoBold" noWrap align="left" width="300"><br />
																																			<IMG id="IMG35" src="~/images/pass_content.gif" width="15" align="absBottom" runat="server"> <strong>
																																			Mark as <a id="lnk_AccessPassYouTube" href="#" runat="server" >Access Pass</a> content<span style="padding-left:24px"></strong> <INPUT class="formKanevaCheck" id="chkRestrictedYoutube" type="checkbox" runat="server">
																																		</TD>
																																	</TR>
																																	<TR>
																																		<TD class="note">Check this field if the media may not be appropriate for Kaneva 
																																			Members who are under the age of 18.
																																		</TD>
																																	</TR>
																																</TABLE>
																															</contenttemplate>
																														</asp:updatepanel>
																													</td>
																												</tr>									  
																												<tr>
																													<td align="left" colSpan="2">
																														<asp:updatepanel id="up3" runat="server">
																															<contenttemplate>
																																<table cellSpacing="0" cellPadding="0" width="100%" border="0">
																																	<tr>
																																		<td>
																																			<input class="formKanevaCheck" runat="server" clientidmode="static" id="cbAgreementYouTube" onclick="javascript:agreementChanged(this, 2);"
																																				type="checkbox">
																																		</td>
																																		<td class="insideTextNoBold" noWrap align="left">
																																			I agree these files meet the Kaneva <A href="../overview/TermsAndConditions.aspx?subtab=trm" target="_resource">
																																				Terms &amp; Conditions</A>
																																		</td>
																																	</tr>
																																</table>					
																															</contenttemplate>
																														</asp:updatepanel>
																													</td>
																												</tr>
																												<tr>
																													<td align="left" colSpan="2">
																														<table cellSpacing="0" cellPadding="0" width="100%" border="0">
																															<tr>
																																<td align="center">
																																	<asp:updatepanel id="up4" runat="server">
																																		<contenttemplate>
																																			<BR>
																																			<INPUT id="btnYouTubeEmbed" type="image" src="~/images/button_addvideo_dis.gif"
																																				name="btnYouTubeEmbed" runat="server" clientidmode="static" causesvalidation="true" validationgroup="youtube"> 
																																		</contenttemplate>
																																	</asp:updatepanel>
																																</td>
																															</tr>
																														</table>					  
																													</td>
																												</tr>
																											</table>
																											<span class="cb"><span class="cl"></span></span>
																											</div>
																											<!-- END section that builds the left hand information section for the YouTube configuration-->
																										</td>
																										<td vAlign="top" width="180" background="../images/bg_upload_addtolist.jpg"></td>
																										<!-- Begin informational section of the  Youtube body -->
																										<td vAlign="top" align="center" width="360">
																											<div class="module whitebg">
																												<span class="ct">
																													<span class="cl"></span>
																												</span>
																												<h2>How to quickly add a YouTube Video</h2>
																												<table class="data" cellSpacing="0" cellPadding="7" width="320" border="0">
																													<tr>
																														<td>
																															<ol>
																																<li>From YouTube, copy the entire �embed� code.</li>
																																<li>Paste the embed code in the �Link to Video� field.</li>
																																<li>Add details about the video and click "Add Video".</li>
																															</ol>
																														</td>
																													</tr>
																													<tr>
																														<td height="20"></td>
																													</tr>
																													<tr>
																														<td >
																															<span class="alertmessage">*</span>The video will be added to your Kaneva media 
																															library and can be played on your site and in the virtual world.
																														</td>
																													</tr>
																												</table>
																												
																												<div class="formspacer"></div>
																												<div class="formspacer"></div>
																												<div class="formspacer"></div>
																												<div class="formspacer"></div>
																												<div class="formspacer"></div>
																												<div class="formspacer"></div>
																												
																												<span class="cb">
																													<span class="cl"></span>
																												</span>
																											</div>
																										</td>
																										<!-- end informational section of the  Youtube body -->
																									</tr>
																								</table>
																						</td>
																					</tr>
																					<!-- end youtube body -->
																					
																					<!-- begin flash widget body -->
																					<tr id="tbrFlash" runat="server" style="display:none;">
																						<td align="left">
																							<table cellSpacing="0" cellPadding="0" width="940" border="0">
																								<tr>
																									<td align="center" width="400">
																										<div class="module whitebg">
																											<span class="ct">
																												<span class="cl"></span>
																											</span>
																											<h2>
																												Widget Details
																												<span class="hlink note">
																													<strong>
																														<span class="alertmessage">*</span>
																														Required Fields
																													</strong>
																												</span>
																											</h2>
																											<br>
																											<!-- this section builds the left hand information section for the Flash Widget configuration-->
																											<table cellSpacing="0" cellPadding="0" width="330" border="0" class="evenpadding">
																												<tr>
																													<td colSpan="2">
																														Widget Embed Code:<span class="alertmessage">*</span>
																													</td>
																												</tr>
																												<tr>
																													<td>
																														<asp:textbox class="formKanevaText" id="tbxFLashLink" runat="server" clientidmode="static" Width="300" name="tbxFLashLink" rows="2" textmode="MultiLine"></asp:textbox>&nbsp; 
																														<asp:requiredfieldvalidator id="rfvtbxFLashLink" runat="server" clientidmode="static" validationgroup="widget" controltovalidate="tbxFLashLink" text="* paste your Widget Embed Code" errormessage="Widget embed code is a required field" display="Dynamic"></asp:requiredfieldvalidator>
																														<br>
																													</td>
																													<td vAlign="top" align="right">
																														<IMG id="Img17" onmouseover="doTooltip(event,'Make sure you have completely highlighted the correct embed link.')" onmouseout="hideTip();" src="~/images/button_tooltip.gif" border="0" runat="server">
																													</td>
																												</tr>
																												<tr>
																													<td colSpan="2">
																														Widget Name/Title:<span class="alertmessage">*</span>
																													</td>
																												</tr>
																												<tr>
																													<td>
																														<asp:updatepanel id="up13" runat="server">
																															<contenttemplate>
																																<asp:textbox class="formKanevaText" id="tbxFLashTitle" runat="server" clientidmode="static" Width="300" name="tbxFLashTitle" maxlength="300"></asp:textbox>&nbsp; 
																																<asp:requiredfieldvalidator id="rfvtbxFLashTitle" runat="server" clientidmode="static" validationgroup="widget" controltovalidate="tbxFLashTitle" text="* Please enter a title." errormessage="Title is a required field." display="Dynamic"></asp:requiredfieldvalidator>
																															</contenttemplate>
																														</asp:updatepanel>
																														<br>
																													</td>
																													<td vAlign="top" align="right">
																														<IMG id="Img18" onmouseover="doTooltip(event,'The Title defaults to your file name.  You may change it now, or after your upload it.')"
																															onmouseout="hideTip();" src="~/images/button_tooltip.gif" border="0" runat="server">
																													</td>
																												</tr>
																												
																												<tr>
																													<td class="insideTextNoBold" noWrap colSpan="2">Select a Category:</td>
																												</tr>
																												<tr>
																													<td class="insideTextNoBold">
																														<asp:updatepanel id="up14" runat="server">
																															<contenttemplate>
																																<asp:dropdownlist class="insideTextNoBold" id="ddlCategoriesFL" runat="server" clientidmode="static" enabled="True" Width="300" name="ddlCategoriesFL" datatextfield="name" datavaluefield="category_id"></asp:dropdownlist>
																																<asp:requiredfieldvalidator id="rfvCatagoriesFL" runat="server" clientidmode="static" validationgroup="widget" controltovalidate="ddlCategoriesFL" text="* Please select a category." errormessage="Category is a required field." display="Dynamic"></asp:requiredfieldvalidator>
																															</contenttemplate>
																														</asp:updatepanel>
																														<br>
																													</td>
																													<td vAlign="top" align="right">
																														<IMG id="Img19" onmouseover="doTooltip(event,'Selecting a Category will help others find your file.')" onmouseout="hideTip();" src="~/images/button_tooltip.gif" border="0" runat="server">
																													</td>
																												</tr>
																												
																												<tr>
																													<td colSpan="2">Tags/Keywords:</td>
																												</tr>
																												<tr>
																													<td>
																														<asp:updatepanel id="up15" runat="server">
																															<contenttemplate>
																																<INPUT class="formKanevaText" id="tbxFlashTags" style="WIDTH: 300px" type="text" maxLength="250" name="tbxFlashTags" runat="server"> 
																																<asp:regularexpressionvalidator id="revtbxFlashTags" runat="server" clientidmode="static" validationgroup="widget" controltovalidate="tbxFlashTags" text="* Use spaces between words" display="Dynamic" enableclientscript="True"></asp:regularexpressionvalidator>&nbsp;&nbsp;(use spaces between words) 
																															</contenttemplate>
																														</asp:updatepanel>
																														<br>
																														<br>
																													</td>
																													<td vAlign="top" align="right">
																														<IMG id="Img20" onmouseover="doTooltip(event,'Tags are unique keywords that help to categorize your media and allow viewers to easily search. Separate each word with a space.')"
																															onmouseout="hideTip();" src="~/images/button_tooltip.gif" border="0" runat="server">
																													</td>
																												</tr>
																												<tr>
																													<td class="insideTextNoBold" noWrap align="left" colSpan="2">Brief Description:</td>
																												</tr>
																												<tr>
																													<td>
																														<asp:updatepanel id="up16" runat="server">
																															<contenttemplate>
																																<asp:textbox id="tbxFlashDescription" runat="server" clientidmode="static" Width="300" wrap="True" textmode="MultiLine" rows="3"></asp:textbox>
																															</contenttemplate>
																														</asp:updatepanel>
																														<BR>
																														<BR>
																													</td>
																													<td vAlign="top" align="right">
																														<IMG id="Img21" onmouseover="doTooltip(event,'This description is used in all listed views of your media.  You may also change it later from the Edit Media form.  Please limit your description to 1000 characters or less.')"
																															onmouseout="hideTip();" src="~/images/button_tooltip.gif" border="0" runat="server">
																													</td>
																												</tr>
																												<tr>
																													<td class="textGeneralGray01" align="left" colSpan="2">
																														<asp:updatepanel id="up17" runat="server">
																															<contenttemplate>
																																<table cellSpacing="0" cellPadding="0" width="100%" border="0">
																																	<tr>
																																		<td class="textGeneralGray01" width="44%">Make this media file:</td>
																																		<td class="textGeneralGray01" width="23%">
																																			<input id="rdoFlashPublic" type="radio" CHECKED value="radPublic" name="radTypeWidget" runat="server"></input>
																																			<label>Public</label>
																																		</td>
																																		<td class="textGeneralGray01" width="23%">
																																			<input id="rdoFlashPrivate" type="radio" value="radPrivate" name="radTypeWidget" runat="server"></input>
																																			<label>Private</label>
																																		</td>
																																		<td align="right" width="10%">
																																			<IMG id="Img22" onmouseover="doTooltip(event,'You may change access to this file once it is uploaded.')"
																																				onmouseout="hideTip();" src="~/images/button_tooltip.gif" border="0" runat="server">
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="note" colSpan="4">
																																			Media marked as public can be both viewed and shared by Kaneva members. Media 
																																			marked as Private cannot be shared and can only be viewed when you're signed on 
																																			to your account.
																																		</td>
																																	</tr>
																																</table>
																															</contenttemplate>
																														</asp:updatepanel>
																													</td>	
																												</tr>
																												<tr>
																													<td class="textGeneralGray01" align="left" colSpan="2">
																														<asp:updatepanel id="up18" runat="server">
																															<contenttemplate>
																																<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0" id="tblAccessPassFlash" runat="server">
																																	<TR>
																																		<TD class="insideTextNoBold" noWrap align="left" width="300"><br />
																																			<IMG id="IMG16" src="~/images/pass_content.gif" width="15" align="absBottom" runat="server"> <strong>
																																			Mark as <a id="lnk_AccessPassFlash" href="#" runat="server" >Access Pass</a> content<span style="padding-left:24px"></strong> <INPUT class="formKanevaCheck" id="chkRestrictedWidget" type="checkbox" runat="server" clientidmode="static" NAME="chkRestrictedWidget">
																																		</TD>
																																	</TR>
																																	<TR>
																																		<TD class="note">Check this field if the media may not be appropriate for Kaneva 
																																			Members who are under the age of 18.
																																		</TD>
																																	</TR>
																																</TABLE>
																															</contenttemplate>
																														</asp:updatepanel>
																													</td>
																												</tr>
																												<tr>
																													<td align="left" colSpan="2">
																														<asp:updatepanel id="up19" runat="server">
																															<contenttemplate>
																																<table cellSpacing="0" cellPadding="0" width="100%" border="0">
																																	<tr>
																																		<td>
																																			<input class="formKanevaCheck" runat="server" clientidmode="static" id="cbAgreementWidget" onclick="javascript:agreementChanged(this, 4);"
																																				type="checkbox" NAME="cbAgreementWidget">
																																		</td>
																																		<td class="insideTextNoBold" noWrap align="left">
																																			I agree these files meet the Kaneva <A href="../overview/TermsAndConditions.aspx?subtab=trm" target="_resource">
																																				Terms &amp; Conditions</A>
																																		</td>
																																	</tr>
																																</table>
																															</contenttemplate>
																														</asp:updatepanel>
																													</td>
																												</tr>
																												<tr>
																													<td align="left" colSpan="2">
																														<table cellSpacing="0" cellPadding="0" width="100%" border="0">
																															<tr>
																																<td align="center">
																																	<asp:updatepanel id="up20" runat="server">
																																		<contenttemplate>
																																			<BR>
																																			<INPUT id="btnWidgetEmbed" type="image" src="~/images/button_addwidget_dis.gif"
																																				name="btnWidgetEmbed" runat="server" clientidmode="static" validationgroup="widget" causesvalidation="true"> 
																																		</contenttemplate>
																																	</asp:updatepanel>
																																</td>
																															</tr>
																														</table>
																													</td>
																												</tr>
																											</table>
																											<span class="cb"><span class="cl"></span></span>
																											</div>
																											<!-- END section that builds the left hand information section for the Flash Widget configuration-->
																										</td>
																										<td vAlign="top" width="180" background="../images/bg_upload_addtolist.jpg"></td>
																										<!-- Begin informational section of the Flash Widget body -->
																										<td vAlign="top" align="center" width="360">
																											<div class="module whitebg">
																												<span class="ct">
																													<span class="cl"></span>
																												</span>
																												<h2>How to add a Flash Widget</h2>
																												<table class="data" cellSpacing="0" cellPadding="7" width="320" border="0">
																													<tr>
																														<td>
																															<ol>
																																<li>Open a new Web browser window and go to a Web site that contains a flash widget you�d like to add (like <a href="http://xfacts.com/widgets " target="_blank">xfacts.com/widgets</a> or <a href="http://www.widgetbox.com" target="_blank">www.widgetbox.com</a>). Note: Make sure the Web site containing the flash widget offers embedded codes. If you�re not sure, check the Help.</li>
																																<li>Copy the embed widget code from the site.</li>
																																<li>In the Kaneva Media Library "Add Media" window, paste the code in the <b>Widget Embed Code </b> field. </li>
																																<li>Type a name for the widget, and any other details you�d like.</li>
																																<li>Click <b>Add Media</b></li>
																															</ol>
																														</td>
																													</tr>
																													<tr>
																														<td height="20"></td>
																													</tr>
																													<tr>
																														<td >
																															<span class="alertmessage">*</span>The Flash Widget will be added to your Kaneva media library and can be played on both your Web profile and on your 3D TV in the Virtual World.
																														</td>
																													</tr>
																												</table>
																												
																												<div class="formspacer"></div>
																												<div class="formspacer"></div>
																												<div class="formspacer"></div>
																												<div class="formspacer"></div>
																												<div class="formspacer"></div>
																												<div class="formspacer"></div>
																												
																												<span class="cb">
																													<span class="cl"></span>
																												</span>
																											</div>
																										</td>
																										<!-- end informational section of the Flash Widget body -->
																									</tr>
																							</table>
																						</td>
																					</tr>
																					<!-- end flash widget body -->
																					<!-- begin flash games body -->
																					<tr id="tbrFlashGame" runat="server" style="display:none;">
																						<td align="left">
																							<table cellSpacing="0" cellPadding="0" width="940" border="0">
																								<tr>
																									<td align="center" width="100%">
																										<div class="module whitebg">
																											<span class="ct">
																												<span class="cl"></span>
																											</span>
																											<h2 style="padding-left:5px">
																												Game Details
																												<span class="hlink note">
																													<strong>
																														<span class="alertmessage">*</span>
																														Required Fields
																													</strong>
																												</span>
																											</h2>																			
																											<div style="width:85%;margin:auto;">									
																												<asp:updatepanel id="up21" runat="server">
																													<contenttemplate>
																														<asp:validationsummary cssclass="errBox black" id="valSum" validationgroup="game" runat="server" clientidmode="static" showmessagebox="False" showsummary="True"
																															displaymode="BulletList" style="margin:10px auto;width:90%;padding:0 0 10px 0;" headertext="" forecolor="black" visible="false"></asp:validationsummary>
																														<asp:customvalidator id="cvBlank" runat="server" clientidmode="static" validationgroup="game" display="None" enableclientscript="False"></asp:customvalidator>
																													</contenttemplate>
																												</asp:updatepanel>														  
																											</div>													
																											<!-- this section builds the left hand information section for the Games configuration-->
																											<table cellSpacing="0" cellPadding="0" width="840" border="0" style="padding:10px 20px 20px 20px;">
																												<tr>
																													<td width="420" valign="top">
																														<table cellSpacing="0" cellPadding="0" width="370" border="0" class="evenpadding" style="margin:0px;">
																															<tr>
																																<td colSpan="2">
																																	Title:<span class="alertmessage">*</span>
																																</td>
																															</tr>
																															<tr>													  
																																<td>
																																	<asp:updatepanel id="up22" runat="server">
																																		<contenttemplate>
																																			<asp:textbox class="formKanevaText" id="txtGameTitle" runat="server" clientidmode="static" Width="330" name="txtGameTitle" maxlength="300"></asp:textbox><br /> 
																																			<asp:requiredfieldvalidator id="rfvGameTitle" enableclientscript="true" runat="server" clientidmode="static" validationgroup="game" controltovalidate="txtGameTitle" text="Please enter a title." display="Dynamic"></asp:requiredfieldvalidator>
																																		</contenttemplate>
																																	</asp:updatepanel>
																																	<br />											  
																																</td>
																																<td vAlign="top" align="right">
																																	<IMG id="Img25" onmouseover="doTooltip(event,'The Title defaults to your file name.  You may change it now, or after your upload it.')"
																																		onmouseout="hideTip();" src="~/images/button_tooltip.gif" border="0" runat="server">
																																</td>
																															</tr>
																															<tr>
																																<td class="insideTextNoBold" noWrap colSpan="2">Icon:</td>
																															</tr>
																															<tr>
																																<td>
																																	<input class="formKanevaText" id="inpGameThumbnail" type="file" size="43" name="inpGameThumbnail" runat="server"><br /> 
																																	<asp:updatepanel id="up23" runat="server">
																																		<contenttemplate>
																																			<span id="spnThumbnailErr" style="COLOR: red" runat="server"></span>
																																		</contenttemplate>
																																	</asp:updatepanel>
																																	<br />
																																</td>
																																<td vAlign="top" align="right">
																																	<IMG id="IMG26" onmouseover="doTooltip(event,'This image will appear as the icon for your game and the screenshot on the 3D arcade machine. Files should be .jpg or .gif files, 150 x 150 pixels, and under 200K.')" onmouseout="hideTip();" src="~/images/button_tooltip.gif" border="0" runat="server">
																																</td>
																															</tr>
																															<tr>
																																<td colSpan="2">Description (including copyright info):</td>
																															</tr>
																															<tr>
																																<td>
																																	<asp:textbox id="txtGameDescription" runat="server" clientidmode="static" Width="330" rows="3" textmode="MultiLine" wrap="True"></asp:textbox>
																																	<br />
																																</td>
																																<td vAlign="top" align="right">
																																	<IMG id="Img27" onmouseover="doTooltip(event,'This description is used in all listed views of your media.  You may also change it later from the Edit Media form.  Please limit your description to 1000 characters or less.')"
																																		onmouseout="hideTip();" src="~/images/button_tooltip.gif" border="0" runat="server">
																																</td>
																															</tr>
																															<tr>
																																<td class="insideTextNoBold" noWrap align="left" colSpan="2">Game Instructions:</td>
																															</tr>
																															<tr>																		  
																																<td>
																																	<asp:textbox id="txtGameInstructions" runat="server" clientidmode="static" Width="330" rows="3" maxlength="5000" textmode="MultiLine" wrap="True"></asp:textbox>
																																	<br />
																																</td>																	  
																																<td vAlign="top" align="right">
																																	<IMG id="Img28" onmouseover="doTooltip(event,'Describe how to play your game (e.g. Use your arrows to move, spacebar to jump).')"
																																		onmouseout="hideTip();" src="~/images/button_tooltip.gif" border="0" runat="server">
																																</td>
																															</tr>
																															<tr>
																																<td class="textGeneralGray01" align="left" colSpan="2">Company Name:</td>
																															</tr>
																															<tr>
																																<td>
																																	<asp:textbox class="formKanevaText" id="txtCompanyName" runat="server" clientidmode="static" Width="330" name="txtCompanyName" maxlength="300"></asp:textbox>&nbsp; 
																																</td>
																																<td vAlign="top" align="right">
																																	<IMG id="Img32" onmouseover="doTooltip(event,'Your company name will display in the game details.')"
																																		onmouseout="hideTip();" src="~/images/button_tooltip.gif" border="0" runat="server">
																																</td>
																															</tr>
																															<tr>
																																<td class="textGeneralGray01" align="left" colSpan="2">Tags/Keywords:</td>
																															</tr>
																															<tr>
																																<td>
																																	<asp:updatepanel id="up24" runat="server">
																																		<contenttemplate>
																																			<INPUT class="formKanevaText" id="txtGameTags" style="WIDTH: 330px" type="text" maxLength="250" name="txtGameTags" runat="server"><br /> 
																																			<asp:regularexpressionvalidator id="revGameTags" runat="server" clientidmode="static" validationgroup="game" controltovalidate="txtGameTags" text="Use spaces between words" display="Dynamic" enableclientscript="True"></asp:regularexpressionvalidator><br />(use spaces between words) 
																																		</contenttemplate>
																																	</asp:updatepanel>
																																	<br />
																																</td>
																																<td vAlign="top" align="right">
																																	<IMG id="Img31" onmouseover="doTooltip(event,'Tags are unique keywords that help to categorize your media and allow viewers to easily search. Separate each word with a space.')" onmouseout="hideTip();" src="~/images/button_tooltip.gif" border="0" runat="server">
																																</td>
																															</tr>
																														</table>
																													</td>
																													<td width="420" valign="top" align="right">
																														<table cellSpacing="0" cellPadding="0" width="370" border="0" class="evenpadding" style="margin:0px;">
																															<tr>
																																<td colSpan="2">
																																	Category:<span class="alertmessage">*</span>
																																</td>								  
																															</tr>
																															<tr>
																																<td>
																																	<asp:updatepanel id="up25" runat="server">
																																		<contenttemplate>
																																			<asp:dropdownlist class="insideTextNoBold" id="ddlCategoriesGame" runat="server" clientidmode="static" enabled="True" Width="330" name="ddlCategoriesGame" datatextfield="name" datavaluefield="category_id"></asp:dropdownlist>
																																			<asp:requiredfieldvalidator id="rfvGameCategory" runat="server" clientidmode="static" validationgroup="game" controltovalidate="ddlCategoriesGame" text="Please select a category." display="Dynamic"></asp:requiredfieldvalidator>
																																		</contenttemplate>
																																	</asp:updatepanel>
																																	<br />							  
																																</td>
																																<td vAlign="top" align="right">
																																	<IMG id="Img33" onmouseover="doTooltip(event,'Selecting a Category will help others find your file.')" onmouseout="hideTip();" src="~/images/button_tooltip.gif" border="0" runat="server">
																																</td>
																															</tr>
																															<tr>
																																<td class="insideTextNoBold" colSpan="2">You can either upload a file from your computer OR link to it using the Embed Code.</td>
																															</tr>
																															<tr>
																																<td class="insideTextNoBold" colspan="2">
																																	<div class="module"><span class="ct"><span class="cl"></span></span>
																																	<table cellSpacing="0" cellPadding="0" width="350" border="0" class="evenpadding">
																																		<tr>
																																			<td colSpan="2">Upload File (.swf only):</td>
																																		</tr>
																																		<tr>
																																			<td>  
																																				<input class="formKanevaText" id="inpGameFile" type="file" size="41" name="inpGameFile" runat="server"><br /> 
																																				<asp:updatepanel id="up26" runat="server">
																																					<contenttemplate>
																																						<input type="hidden" id="hidGameFileName" runat="server" clientidmode="static" />
																																						<span id="spnGameFileErr" style="COLOR: red" runat="server"></span>
																																					</contenttemplate>
																																				</asp:updatepanel>
																																				<br />
																																			</td>
																																			<td vAlign="top" align="right">
																																				<IMG id="IMG34" onmouseover="doTooltip(event,'Max file size: 100 MB. Max length: 10 minutes. You can upload a maximum of 5 files.')" onmouseout="hideTip();" src="~/images/button_tooltip.gif" border="0" runat="server">
																																			</td>
																																		</tr>
																																		<tr><td colspan="2" align="center"><strong>-OR-</strong></td></tr>
																																		<tr>
																																			<td colSpan="2">Embed Code:</td>
																																		</tr>
																																		<tr>
																																			<td>
																																				<asp:textbox class="formKanevaText" id="txtGameLink" runat="server" clientidmode="static" Width="300" name="txtGameLink" rows="2" textmode="MultiLine"></asp:textbox>&nbsp; 
																																				<asp:requiredfieldvalidator id="rfvGameLink" runat="server" clientidmode="static" validationgroup="game" controltovalidate="txtGameLink" text="You must either select a .swf file to upload or enter an embed code." display="Dynamic"></asp:requiredfieldvalidator>
																																				<br>
																																			</td>
																																			<td vAlign="top" align="right">
																																				<IMG id="Img24" onmouseover="doTooltip(event,'Make sure you have completely highlighted the correct embed link.')" onmouseout="hideTip();" src="~/images/button_tooltip.gif" border="0" runat="server">
																																			</td>
																																		</tr>
																																	</table>
																																	<span class="cb"><span class="cl"></span></span></div>
																																</td>
																															</tr>
																															<tr>
																																<td class="textGeneralGray01" align="left" colSpan="2">
																																	<asp:updatepanel id="up27" runat="server">
																																		<contenttemplate>
																																			<table cellSpacing="0" cellPadding="0" width="100%" border="0">
																																				<tr>
																																					<td class="textGeneralGray01" width="44%">Make this media file:</td>
																																					<td class="textGeneralGray01" width="23%">
																																						<input id="radGamePublic" type="radio" CHECKED value="radGamePublic" name="radTypeGame" runat="server"></input>
																																						<label>Public</label>
																																					</td>
																																					<td class="textGeneralGray01" width="23%">
																																						<input id="radGamePrivate" type="radio" value="radGamePrivate" name="radTypeGame" runat="server"></input>
																																						<label>Private</label>
																																					</td>
																																					<td align="right" width="10%">
																																						<IMG id="Img29" onmouseover="doTooltip(event,'You may change access to this file once it is uploaded.')"
																																							onmouseout="hideTip();" src="~/images/button_tooltip.gif" border="0" runat="server">
																																					</td>
																																				</tr>
																																				<tr>
																																					<td class="note" colSpan="4">
																																						Media marked as public can be both viewed and shared by Kaneva members. Media 
																																						marked as Private cannot be shared and can only be viewed when you're signed on 
																																						to your account.
																																					</td>
																																				</tr>
																																			</table>
																																		</contenttemplate>
																																	</asp:updatepanel>
																																</td>	
																															</tr>
																															<tr>
																																<td class="textGeneralGray01" align="left" colSpan="2">
																																	<asp:updatepanel id="up28" runat="server">
																																		<contenttemplate>
																																			<table cellSpacing="0" cellPadding="0" width="100%" border="0" id="tblAccessPassGames" runat="server">
																																				<tr>
																																					<td class="insideTextNoBold" noWrap align="left" width="300"><br />
																																						<IMG id="IMG23" src="~/images/pass_content.gif" width="15" align="absBottom" runat="server"> <strong>
																																						Mark as <a id="lnk_AccessPassGames" href="#" runat="server" >Access Pass</a> content<span style="padding-left:24px"></strong> <INPUT class="formKanevaCheck" id="chkRestrictedGame" type="checkbox" runat="server" clientidmode="static" name="chkRestrictedGame">
																																					</td>
																																				</tr>
																																				<tr>
																																					<td class="note">Check this field if the media may not be appropriate for Kaneva 
																																						Members who are under the age of 18.
																																					</td>
																																				</tr>
																																			</table>
																																		</contenttemplate>
																																	</asp:updatepanel>
																																</td>
																															</tr>
																															<tr>
																																<td align="left" colSpan="2">
																																	<asp:updatepanel id="up29" runat="server">
																																		<contenttemplate>
																																			<table cellSpacing="0" cellPadding="0" width="100%" border="0">
																																				<tr>
																																					<td>
																																						<input class="formKanevaCheck" runat="server" clientidmode="static" id="cbAgreementGame" onclick="javascript:agreementChanged(this, 5);"
																																							type="checkbox" NAME="cbAgreementGame">
																																					</td>
																																					<td class="insideTextNoBold" noWrap align="left">
																																						I agree these files meet the Kaneva <A href="../overview/TermsAndConditions.aspx?subtab=trm" target="_resource">
																																							Terms &amp; Conditions</A>
																																					</td>
																																				</tr>
																																			</table>
																																		</contenttemplate>
																																	</asp:updatepanel>
																																</td>
																															</tr>
																															<tr>
																																<td align="left" colSpan="2">
																																	<table cellSpacing="0" cellPadding="0" width="100%" border="0">			
																																		<tr>
																																			<td align="center">		
																																				<br/>
																																				<asp:imagebutton id="btnAddGame" validationgroup="game" causesvalidation="true" onclick="btnAddGame_Click" enabled="false" imageurl="~/images/button_addwidget_dis.gif" runat="server" clientidmode="static" class="Filter2" Text=" Upload " /> 
																																			</td>
																																		</tr>						
																																	</table>
																																</td>
																															</tr>
																														</table>																			
																													</td>
																												</tr>
																											</table>
																																										
																											<span class="cb"><span class="cl"></span></span>
																										</div>
																										<!-- END section that builds the left hand information section for the YouTube configuration-->
																									</td>
																								</tr>
																							</table>
																						</td>
																					</tr>
																					<!-- end flash game body -->
																				</table>
																				<span class="cb"><span class="cl"></span></span>
																			</div>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
																				
													</table>
													
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
						<td class="">
							<IMG height="1" src="~/images/spacer.gif" width="1" runat="server">
						</td>
					</tr>
					<tr>
						<td class=""></td>
						<td class=""></td>
						<td class=""></td>
					</tr>
					<tr>
						<td>
							<IMG height="14" src="~/images/spacer.gif" width="1" runat="server">
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</div>

<style type="text/css">
#vipbtn a, #vipbtn a:hover, #vipbtn a:visited,{cursor:pointer;color:#000;text-decoration:none;}
</style>
<div id="divGameUploading" style="display:none;width:400;border:solid 2px black;background-color:White;z-index:20px;position:relative;top:-400px;left:100px;">
	<table height="100" cellSpacing="0" cellPadding="0" width="400" border="0">
		<tr>
			<th class="loadingText">
				Please wait while file is uploaded...
				<div style="DISPLAY: block"><IMG src="../images/progress_bar.gif"></div>
			</th>
		</tr>
	</table>
</div>

</asp:Content>  
