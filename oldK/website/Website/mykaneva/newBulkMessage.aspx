<%@ Page language="c#" Codebehind="newBulkMessage.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.mykaneva.newBulkMessage" %>
<%@ Register TagPrefix="Kaneva" TagName="HeaderText" Src="../usercontrols/HeaderText.ascx" %>
<%@ Register TagPrefix="CE" Namespace="CuteEditor" Assembly="CuteEditor" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>

<link href="../css/home.css" rel="stylesheet" type="text/css" />		
<link href="../css/kanevaSC.css" rel="stylesheet" type="text/css"/>
<link href="../css/friends.css" rel="stylesheet" type="text/css"/>
<link href="../css/kanevaText.css" type="text/css" rel="stylesheet">
<link href="../css/kanevaBroadBand.css" rel="stylesheet" type="text/css" />		
<link href="../css/NEW.css" rel="stylesheet" type="text/css" />		

<script src="../jscript/prototype.js" type="text/javascript" language="javascript"></script>


<script type="text/javascript">

function paste(str)
{
	// get the cute editor instance
	var editor1 = document.getElementById('<asp:literal id="litEditorId" runat="server" />');

	var url = 'http://<asp:literal id="litSiteName" runat="server"/>' + str;
	
	// pasting the specified HTML into a range within a editor document 
	editor1.PasteHTML('<a href="' + url + '">' + url + '</a>');
}
</script>

<table border="0" cellspacing="0" cellpadding="0"  width="990" align="center">
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" /></td>
	</tr>
	<tr>
		<td>
			<table  border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="frTopLeft"></td>
					<td class="frTop"></td>
					<td class="frTopRight"></td>
				</tr>
				<tr>
					<td class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
					<td valign="top" class="frBgIntMembers">
						
						<table width="100%"  border="0" cellspacing="0" cellpadding="0"">
							<tr>
								<td>
								
									<!-- TOP STATUS BAR -->
									<div id="pageheader">
										<table cellpadding="0" cellspacing="0" border="0" width="99%">
											<tr>
												<td align="left">
													<ajax:ajaxpanel id="ajpTitle" runat="server">
													<h1 id="h1Title" runat="server">Compose Bulk Message</h1>
													</ajax:ajaxpanel>
												</td>
												<td align="right" valign="middle">
													<table cellpadding="0" cellspacing="0" border="0" width="690">
														<tr>
															<td class="headertout" width="175"></td>
															<td class="searchnav" width="260"></td>
															<td align="left" width="130"></td>
															<td class="searchnav" align="right" width="210" nowrap></td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</div>
									<!-- END TOP STATUS BAR -->	
								
								</td>
							</tr>
							<tr>
								<td width="968" align="left"  valign="top">
									
									<ajax:ajaxpanel id="ajpSuccess" runat="server">
									<div id="divSuccess" runat="server" visible="false">
									<table cellpadding="0" cellspacing="0" border="0" width="968">
										<tr>
											<td width="250" valign="top">
												<div class="module toolbar">
													<span class="ct"><span class="cl"></span></span>
													<h2>Tools & Actions <span class="hlink"></span></h2>
													<table cellspacing="0" cellpadding="10" border="0" width="99%">
														<tr>
															<td align="center">
																<table cellspacing="0" cellpadding="3" border="0" width="100%">
																	<tr>
																		<td align="left">
																			<asp:label id="lblGoDesc" runat="server"></asp:label>	
																		</td>
																		<td align="right">
																			<a id="lnkSuccessGo" runat="server">
																			<img src="~/images/button_go.gif" id="btnGoSuccess" runat="server" causesvalidation="False" alt="Go" width="57" height="23" border="0" vspace="2" /><br>
																			</a>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>										
													</table>
													<span class="cb"><span class="cl"></span></span>
												</div>
											</td>
											<td width="20"><img runat="server" src="~/images/spacer.gif" width="20" height="1" id="Img1"/></td>
											<td width="748" valign="top" align="center">
												
												<div class="module whitebg">
													<span class="ct"><span class="cl"></span></span>
													
													<ajax:ajaxpanel id="ajpAlertMsgSuccess" runat="server">
													<h2 class="alertmessage"><span id="spnAlertMsgSuccess" runat="server"></span></h2>
													</ajax:ajaxpanel>
													
													<table width="99%" id="dlFriends" border="0" cellpadding="5" cellspacing="0">
														<tr>
															<td width="99%"><br/>															
																<table width="100%" border="0" cellspacing="0" cellpadding="5">
																	<tr>
																		<td align="left" width="60">
																			<!-- FRIENDS BOX -->
																			<div class="framesize-xsmall" id="divAvatar" runat="server" visible="false">
																				<div class="frame">
																					<span class="ct"><span class="cl"></span></span>
																						<div class="imgconstrain" style="text-align:left;">
																							<a id="lnkMember" runat="server"><img runat="server" id="imgAvatar" border="0" width="45" height="33"/></a>
																						</div>
																					<span class="cb"><span class="cl"></span></span>
																				</div>
																			</div>		
																			<!-- END FRIENDS BOX -->
																		</td>
																		<td id="tdSuccessMsg" runat="server" align="left"></td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>			
															
													<span class="cb"><span class="cl"></span></span>
												</div>
											</td>
										</tr>
									</table>
									</div>
									</ajax:ajaxpanel>
						
						
									
									<table cellpadding="0" cellspacing="0" border="0" width="968" id="tblComposeMessage" runat="server" visible="true">
										<tr>
											<td width="200" valign="top">
												<div class="module toolbar">
													<span class="ct"><span class="cl"></span></span>
													<h2>Tools & Actions <span class="hlink"></span></h2>
													<table cellspacing="0" cellpadding="10" border="0" width="99%">
														<tr>
															<td align="center">
																<table cellspacing="0" cellpadding="3" border="0" width="100%">
																	<tr>
																		<td>
																			
																			<asp:imagebutton imageurl="~/images/button_sendnow.gif" id="btnSend" runat="server" alternateText="Send Now" onclick="btnSend_Click" width="77" height="23" border="0" vspace="2" /><br>
																			
																		</td>
																	</tr>
																	<tr>
																		<td>
																			<asp:imagebutton imageurl="~/images/button_cancel.gif" id="btnCancel" runat="server" causesvalidation="False" onclick="btnCancel_Click" alt="Cancel" width="57" height="23" border="0" vspace="2" /><br>
																		</td>
																	</tr>
																</table>
																<hr><br>						   
																<p align="left"><asp:linkbutton id="lbBack" runat="server" causesvalidation="False" onclick="lbBack_Click">Back</asp:linkbutton></p>
															</td>
														</tr>										
													</table>
													<span class="cb"><span class="cl"></span></span>
												</div>
											</td>
											<td width="20"><img runat="server" src="~/images/spacer.gif" width="20" height="1" /></td>
											<td width="748" valign="top" align="center">
												
												<div class="module whitebg">
													<span class="ct"><span class="cl"></span></span>
													
													
													<h2 class="alertmessage"><span id="spnAlertMsg" runat="server"></span></h2>
													
													
													<table width="99%" id="dlFriends" border="0" cellpadding="5" cellspacing="0">
														<tr>
															<td width="99%">
																
																<table width="100%" border="0" cellspacing="0" cellpadding="5">
																	
																	
																	<tr id="trUsername" runat="server">
																		<td align="right" nowrap class="textGeneralGray01"></td>
																		<td align="left" nowrap class="textGeneralGray01"><br/><br/><br/><br/>To:</td>
																		<td>&nbsp;</td>
																		<td align="left" class="textGeneralGray03">
																			<ajax:ajaxpanel id="ajpUploadCSV" runat="server">
																				
																				<table cellspacing="0" cellspadding="10" width="300" border="0" class="data">
																						<tr>
																							<td align="center">
																								<div id="divImportCSV" runat="server" visible="true">
																									<asp:imagebutton runat="server" imageurl="~/images/button_importaddresses.gif" alternatetext="Import Addresses" id="btnImportCSV" width="153" height="23" border="0" align="right" onclick="btnImportCSV_Click" />
																								</div>
																							
																								<div id="divUploadCSV" runat="server" visible="false">
																									<p align="left">Locate CSV file on your hard drive:</p>
																									<input id="fileCSV" runat="server" type="file" name="fileCSV" class="formKanevaText" size="38" style="width: 275px;" />&nbsp;<asp:imagebutton imageurl="~/images/button_upload_contacts.gif" alternatetext="Upload Contacts File" width="126" height="23" border="0" runat="server" id="btnUpload" causesvalidation="false" />
																									<br>
																								</div>
																							</td>
																						</tr>
																					</table>
																				
																				<asp:textbox id="txtUsername" cssclass="formKanevaText" style="width:600px" runat="server"/>
																				(usernames separated by spaces, commas or semicolons)
																			</ajax:ajaxpanel>
																			
																		</td>
																		<td width="48%" align="left" class="textGeneralGray01">&nbsp;</td>
																	</tr>
																	<tr>
																		<td align="right" nowrap class="textGeneralGray01"></td>
																		<td align="right" nowrap class="textGeneralGray01">Subject:</td>
																		<td>&nbsp;</td>
																		<td align="left" class="textGeneralGray03">												
																			<asp:textbox id="txtSubject" class="formKanevaText" style="width:300px" maxlength="100" runat="server"/>
																		</td>
																		<td width="48%" align="left" class="textGeneralGray01">&nbsp;</td>
																	</tr>
																	<tr id="trInsertMediaLink" runat="server" visible="false">
																		<td align="right" nowrap class="textGeneralGray01"></td>
																		<td colspan="3">
																			<span style="padding-left:146px;">
																				<a href='#' onclick="javascript:window.open('selectMedia.aspx?tbName=txtModulePicture','add','toolbar=no,width=550,height=400,menubar=no,scrollbars=yes').focus();return false;">Insert Link from your Media Uploads...</a>
																			</span>
																		</td>
																		<td width="48%" align="left" class="textGeneralGray01">&nbsp;</td>
																	</tr>
																	<tr>
																		<td colspan="5" nowrap class="textGeneralGray01"><img runat="server" src="~/images/spacer.gif" width="1" height="4" /></td>
																	</tr>  <input type="hidden" id="hidLinkURL" runat="server" value="" name="hidLinkURL"/>
																</table>
																
																<table border="0" cellspacing="0" cellpadding="0" width="100%">
																	<tr>																																											   
																		<td>
																			<ce:editor id="txtBody" runat="server" maxtextlength="100000" bgcolor="#ededed" width="100%" height="200px" showhtmlmode="False" configurationpath="~/CuteSoft_Client/CuteEditor/Configuration/AutoConfigure/forum.config" autoconfigure="None" ></ce:editor>
																			<br />
																																																		
																				<asp:imagebutton imageurl="~/images/button_sendnow.gif" id="btnSendBottom" runat="server" alternatetext="Send Now" onclick="btnSend_Click" width="77" height="23" border="0" vspace="2" imagealign="Right" /><br>
																																																											
																		</td>
																	</tr>
																</table>
																<input type="hidden" runat="server" id="hidMessageId" name="hidMessageId">
															</td>
														</tr>
													</table>
													<span class="cb"><span class="cl"></span></span>
												</div>
											</td>
										</tr>
									</table>
									
									
									
								</td>
							</tr>
						</table>
						
					</td>
					<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
				</tr>
				<tr>
					<td class="frBottomLeft"></td>
					<td class="frBottom"></td>
					<td class="frBottomRight"></td>
				</tr>
				<tr>
					<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" /></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
