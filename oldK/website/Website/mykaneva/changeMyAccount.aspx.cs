///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for changeMyAccount.
	/// </summary>
	public class changeMyAccount : MainTemplatePage
	{
		protected changeMyAccount () 
		{
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			if (!IsPostBack)
			{
				int userId = GetUserId ();

				DataRow drUserLicense = StoreUtility.GetActiveUserLicenseSubscription (userId);

				// Disable commercial license for now
				optPlanCommercial.Disabled = true;

				if (drUserLicense == null || drUserLicense ["license_subscription_id"] == null)
				{
					optPlanFree.Checked = true;
					btnCancelSubscription.Visible = false;
				}
				// Pilot?
				else if (Convert.ToInt32 (drUserLicense ["license_subscription_id"]) == (int) Constants.eLICENSE_SUBSCRIPTIONS.PILOT)
				{
					optPlanStandard.Checked = true;
					btnCancelSubscription.Visible = true;
					optPlanFree.Disabled = true;

					// Is it pending to be cancelled?
					DataRow drActivePilotLicense = StoreUtility.GetActiveUserLicenseSubscription (userId);
					if (!drActivePilotLicense ["user_cancelled"].Equals (DBNull.Value))
					{
						if (drActivePilotLicense ["user_cancelled"].ToString ().Equals ("Y"))
						{
							lblMessage.Text = "Your license subscription has been cancelled and will end on " + FormatDateTime (drActivePilotLicense ["end_date"]);
							btnCancelSubscription.Visible = false;
						}
					}
				}
				// Commercial?
				else if (Convert.ToInt32 (drUserLicense ["license_subscription_id"]) == (int) Constants.eLICENSE_SUBSCRIPTIONS.COMMERCIAL)
				{
					optPlanCommercial.Checked = true;
					btnCancelSubscription.Visible = false;
					optPlanFree.Disabled = true;
					optPlanStandard.Disabled = true;
				}
				else
				{
					optPlanFree.Checked = true;
					btnCancelSubscription.Visible = false;
				}
			}

			btnCancelSubscription.Attributes.Add ("onMouseUp", "javascript: window.open('" + KanevaGlobals.PayPalURL + "/cgi-bin/webscr?cmd=_subscr-find&alias=" + KanevaGlobals.PayPalBusiness + "','paypal','toolbar=yes menubar=yes scrollbars=yes');");
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnUpdate_Click (object sender, EventArgs e) 
		{
			int userId = GetUserId ();

			// License Type
			DataRow drUserLicense = StoreUtility.GetActiveUserLicenseSubscription (userId);

			if (drUserLicense == null)
			{
				// Are they buying?
				if (optPlanStandard.Checked)
				{
					Response.Redirect ("~/billing/makePurchase.aspx?sub=pilot");
				}
			}
			else
			{
				if (optPlanFree.Checked)
				{
					ShowErrorOnStartup ("Send to paypal subscription cancel screens.");
				}
			}

			Response.Redirect (ResolveUrl("~/mykaneva/myKaneva.kaneva"));
		}

		protected HtmlInputRadioButton optPlanStandard, optPlanFree, optPlanCommercial;

		protected HtmlInputButton btnCancelSubscription;

		protected Label lblMessage;

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
