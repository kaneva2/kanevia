<%@ Page language="c#" Codebehind="friendGroups.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.friendGroups" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../usercontrols/Pager.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="SearchFilter" Src="../usercontrols/SearchFilter.ascx" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>

<link href="../css/home.css" rel="stylesheet" type="text/css" />		
<link href="../css/kanevaSC.css" rel="stylesheet" type="text/css"/>
<link href="../css/friends.css" rel="stylesheet" type="text/css"/>
<link href="../css/kanevaText.css" type="text/css" rel="stylesheet">

<link href="../css/new.css" rel="stylesheet" type="text/css" />

<script src="../jscript/prototype.js" type="text/javascript" language="javascript"></script>


<table border="0" cellspacing="0" cellpadding="0"  width="990" align="center">
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" /></td>
	</tr>
	<tr>
		<td>
			<table  border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="frTopLeft"></td>
					<td class="frTop"></td>
					<td class="frTopRight"></td>
				</tr>
				<tr>
					<td class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
					<td valign="top" class="frBgIntMembers">
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
								
									<!-- TOP STATUS BAR -->
									<div id="pageheader">
										<table cellpadding="0" cellspacing="0" border="0" width="99%">
											<tr>
												<td  align="left">
													<h1>Friends Groups</h1>
												</td>
												
												<td align="right" valign="middle">
													<table cellpadding="0" cellspacing="0" border="0" width="690">
														<tr>
															<td>	
																<ajax:ajaxpanel id="ajpTopStatusBar" runat="server">
																<table cellpadding="0" cellspacing="0" border="0" width="100%">
																	<tr>
																		<td class="headertout" width="175">
																			<asp:label runat="server" id="lblSearch" />
																		</td>
																		<td class="searchnav" width="260"> Sort by: 
																			<asp:linkbutton id="lbSortByName" runat="server" sortby="Name"  onclick="lbSortBy_Click">Name</asp:linkbutton> | 
																			<asp:linkbutton id="lbSortByDate" runat="server" sortby="Date" onclick="lbSortBy_Click">Date Added</asp:linkbutton>
																		</td>
																		<td align="left" width="130">
																			<kaneva:searchfilter runat="server" id="searchFilter" loc="top" hidethumbview="true" showfilterlists="false" assetsperpagelist="10,25,40" />
																		</td>
																		<td class="searchnav" align="right" width="210" nowrap>
																			<kaneva:pager runat="server" isajaxmode="True" id="pgTop" maxpagestodisplay="5" shownextprevlabels="true" />
																		</td>
																	</tr>
																</table>
																</ajax:ajaxpanel>
															</td>	
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</div>
									<!-- END TOP STATUS BAR -->	
								
								</td>
							</tr>
							<tr>
								<td width="968" align="left"  valign="top">
									<table cellpadding="0" cellspacing="0" border="0"  width="968">
										<tr>
											<td width="250" valign="top">
												<div class="module toolbar">
													<span class="ct"><span class="cl"></span></span>
													<h2>Tools & Actions <span class="hlink"></span></h2>
													
													<table cellspacing="0" cellpadding="10" border="0" width="99%">
														<tr>
															<td align="center">
																<ajax:ajaxpanel id="ajpAddButton" runat="server">
																<table cellspacing="0" cellpadding="3" border="0" width="100%">
																	<tr>
																		<td>Create a new Friend Group:</td>
																		
																		<td valign="bottom">
																			<asp:imagebutton imageurl="~/images/button_add.gif" alt="Add" width="57" height="23" border=0 id="btnAdd" runat="server" onclick="btnAddGroup_Click" causesvalidation="False" ></asp:imagebutton>
																		</td>
																		
																	</tr>
																	<tr runat="server" id="trAddGroup" visible="false">
																		<td colspan="2">	
																			
																			<span id="spnGN" runat="server">group name:</span><br/>
																			<input id="inpAG" type="text" runat="server" name="inpAG"/><br/>
																			<asp:button runat="server" id="btnSaveG" imageurl="" text=" save " causesvalidation="False" onclick="btnSaveG_Click"/> 
																			<asp:button runat="server" id="btnCancelG" imageurl="" text=" cancel " causesvalidation="False" onclick="btnCancelG_Click"/>
																			
																		</td>
																		
																	</tr>
																</table>
																</ajax:ajaxpanel>	
																<br />
																
																
																<table cellspacing="0" cellpadding="3" border="0" width="100%">
																	<tr>
																		<td>Remove selected Group:</td>
																		<td valign="bottom">
																		
																			<ajax:ajaxpanel id="ajpRemoveButton" runat="server">
																				<asp:imagebutton imageurl="~/images/button_remove.gif" alt="Remove" width="77" height="23" border="0" id="btnRemove" runat="server" onclick="btnRemove_Click"></asp:imagebutton>
																			</ajax:ajaxpanel>
																			
																		</td>
																	</tr>
																</table>
																
															</td>
														</tr>
													</table>
														
													<span class="cb"><span class="cl"></span></span>
												</div>
												
											</td>
											<td width="20"><img runat="server" src="~/images/spacer.gif" width="20" height="1" /></td>
											<td width="698" valign="top" align="center">
												
 												<ajax:ajaxpanel id="ajpFriendGroups" runat="server">
 												<div class="module whitebg">
													<span class="ct"><span class="cl"></span></span>
													<h2 class="alertmessage"><span id="spnAlertMsg" runat="server"></span></h2>
													
													<div align="left" style="padding:5px 10px;">Select: <a href="javascript:void(0);" onclick="Select_All(true);">All</a> | <a href="javascript:void(0);" onclick="Select_All(false);">None</a></div>
													
													<div align="left" style="padding-left:3px;">
													<asp:datalist runat="server" enableviewstate="True" showfooter="False" id="dlFriendGroups" 
														cellpadding="5" cellspacing="0" border="0" repeatcolumns="5" repeatdirection="Horizontal"
														itemstyle-horizontalalign="Center" itemstyle-width="128">
														
														<itemtemplate>
														
															<!-- FRIENDS BOX -->
															<div class="framesize-medium">
																<div class="frame">
																	<span class="ct"><span class="cl"></span></span>
																		<div class="imgconstrain">
																			<a title='<%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "name").ToString ())%>' href='<%#GetFriendsInGroupLink (Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "friend_group_id")))%>'>
																			<%# GetThumbnailImages( Convert.ToInt32(DataBinder.Eval(Container.DataItem, "friend_group_id")), Convert.ToInt32(DataBinder.Eval(Container.DataItem, "friend_count")) ) %> 
																			</a>
																		</div>
																	<p><span class="content"><a title='<%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "name").ToString ())%>' href='<%#GetFriendsInGroupLink (Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "friend_group_id")))%>'><%# TruncateWithEllipsis (Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "name").ToString ()), 13) %></a></span></p>
																	<p class="location"><%# DataBinder.Eval(Container.DataItem, "friend_count") %> Friends</p>
																	<p align="center"><span style="padding: 2px;"><asp:checkbox runat="server" id="chkEdit" class="formKanevaCheck"/></span></p>
																	<span class="cb"><span class="cl"></span></span>
																</div>
															</div>		
															<!-- END FRIENDS BOX -->
															<input type="hidden" runat="server" id="hidFriendGroupId" value='<%#DataBinder.Eval(Container.DataItem, "friend_group_id")%>' name="hidFriendGroupId">
														
											
														</itemtemplate>
													</asp:datalist>
													</div>
													
													<span class="cb"><span class="cl"></span></span>
												</div>
												</ajax:ajaxpanel>
												
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
				</tr>
				<tr>
					<td class="frBottomLeft"></td>
					<td class="frBottom"></td>
					<td class="frBottomRight"></td>
				</tr>
				<tr>
					<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" /></td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<!-- Container Table -->

