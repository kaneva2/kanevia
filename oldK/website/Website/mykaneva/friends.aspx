<%@ Page language="c#" Codebehind="friends.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.friends" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../usercontrols/Pager.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="SearchFilter" Src="../usercontrols/SearchFilter.ascx" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>

<link href="../css/home.css" rel="stylesheet" type="text/css" />		
<link href="../css/kanevaSC.css" rel="stylesheet" type="text/css"/>
<link href="../css/friends.css" rel="stylesheet" type="text/css"/>
<link href="../css/kanevaText.css" type="text/css" rel="stylesheet">

<link href="../css/new.css" rel="stylesheet" type="text/css" />

<script src="../jscript/prototype.js" type="text/javascript" language="javascript"></script>


<table border="0" cellspacing="0" cellpadding="0"  width="990" align="center">
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" /></td>
	</tr>
	<tr>
		<td> 
			<table  border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="frTopLeft"></td>
					<td class="frTop"></td>
					<td class="frTopRight"></td>
				</tr>
				<tr>
					<td class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
					<td valign="top" class="frBgIntMembers">  
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
								
									<!-- TOP STATUS BAR -->
									<div id="pageheader">
										<table cellpadding="0" cellspacing="0" border="0" width="99%">
											<tr>
												<td align="left">
													<h1>My Friends List</h1>
												</td>
												
												<td align="right" valign="middle">
													<table cellpadding="0" cellspacing="0" border="0" width="690">
														<tr>
															<td>	
																<ajax:ajaxpanel id="ajpTopStatusBar" runat="server">
																<table cellpadding="0" cellspacing="0" border="0" width="100%">
																	<tr>
																		<td class="headertout" width="175">
																			<asp:label runat="server" id="lblSearch" />
																		</td>
																		<td class="searchnav" width="260"> Sort by: 
																			<asp:linkbutton id="lbSortByName" runat="server" sortby="Name"  onclick="lbSortBy_Click">Name</asp:linkbutton> | 
																			<asp:linkbutton id="lbSortByDate" runat="server" sortby="Date" onclick="lbSortBy_Click">Date Added</asp:linkbutton>
																		</td>
																		<td align="left" width="130">
																			<kaneva:searchfilter runat="server" id="searchFilter" loc="top" hidethumbview="true" showfilterlists="false" assetsperpagelist="12,24,40" />
																		</td>
																		<td class="searchnav" align="right" width="210" nowrap>
																			<kaneva:pager runat="server" isajaxmode="True" id="pgTop" maxpagestodisplay="5" shownextprevlabels="true" />
																		</td>
																	</tr>
																</table>
																</ajax:ajaxpanel>
															</td>	
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</div>
									<!-- END TOP STATUS BAR -->	
								
								</td>
							</tr>
							<tr>
								<td width="968" align="left"  valign="top">
									<table cellpadding="0" cellspacing="0" border="0"  width="968">
										<tr>
											<td width="250" valign="top">
												<div class="module toolbar">
													<span class="ct"><span class="cl"></span></span>
													<h2>Tools & Actions <span class="hlink"></span></h2>
													<table cellspacing="0" cellpadding="10" border="0" width="99%">
														<tr>
															<td align="center">
											
																<table cellspacing="0" cellpadding="3" border="0" width="100%">
																	<tr>
																		<td colspan="2">Add selected to a friend group:</td>
																	</tr>
																	<tr>
																		<td>
																			
																			<ajax:ajaxpanel id="ajpFriendGroups" runat="server">
																				<asp:dropdownlist id="drpFriendGroups" runat="server" style="width:150px" class="content"></asp:dropdownlist>
																			</ajax:ajaxpanel>
																						   
																		</td>
																		<td valign="bottom">
																		
																			<ajax:ajaxpanel id="ajpAddButton" runat="server">
																				<asp:ImageButton imageurl="~/images/button_add.gif" alt="Add" width="57" height="23" border=0 id="btnAdd" runat="server" onclick="btnAdd_Click"></asp:imagebutton>
																			</ajax:ajaxpanel>
																			
																		</td>
																	</tr>
																</table>				  
																
																<hr /><br />
																
																<table cellspacing="0" cellpadding="3" border="0" width="100%">
																	<tr>
																		<td>Remove selected from Friends List:	</td>
																		<td valign="bottom">
																		
																			<ajax:ajaxpanel id="ajpDeleteButton" runat="server">
																				<asp:ImageButton imageurl="~/images/button_remove.gif" alt="Delete" width="77" height="23" border="0" id="btnDelete" runat="server" onclick="btnDelete_Click"></asp:imagebutton>
																			</ajax:ajaxpanel>
																			
																		</td>
																	</tr>
																</table>
																
															</td>
														</tr>
														
													</table>
										 				
													<span class="cb"><span class="cl"></span></span>
												</div>
											</td>
											<td width="20"><img runat="server" src="~/images/spacer.gif" width="20" height="1" /></td>
											<td width="698" valign="top" align="center">
												
												<ajax:ajaxpanel id="ajpFriendsList" runat="server">
												<div class="module whitebg">
													<span class="ct"><span class="cl"></span></span>
													<h2 class="alertmessage"><span id="spnAlertMsg" runat="server"></span></h2>
													
													<div align="left" style="padding:5px 10px;">Select: <a href="javascript:void(0);" onclick="Select_All(true);">All</a> | <a href="javascript:void(0);" onclick="Select_All(false);">None</a></div>
													
													<div align="left" style="margin-left:5px;">
													<asp:datalist visible="true" runat="server" showfooter="False" id="dlFriends"
														cellpadding="5" cellspacing="0" repeatcolumns="6" repeatdirection="Horizontal" 
														itemstyle-horizontalalign="Center" itemstyle-width="104">
														<itemtemplate>
															<!-- FRIENDS BOX -->
															<div class="framesize-small">
																<div class="frame">
																	<span class="ct"><span class="cl"></span></span>
																	<div class="imgconstrain">
																		<a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "NameNoSpaces").ToString ())%> title='<%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "Username").ToString ())%>' style="cursor: hand;">
																			<img runat="server" src='<%#GetProfileImageURL (DataBinder.Eval(Container.DataItem, "ThumbnailMediumPath").ToString (), "me", DataBinder.Eval(Container.DataItem, "Gender").ToString ())%>' border="0" ID="Img1"/>
																		</a>
																	</div>	
																	<p><span class="content"><a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "NameNoSpaces").ToString ())%>><%# TruncateWithEllipsis (DataBinder.Eval(Container.DataItem, "Username").ToString(), 9) %></a></span></p>
																	<p class="location"><%# TruncateWithEllipsis (DataBinder.Eval(Container.DataItem, "Location").ToString(), 9) %></p>
																<%# GetOnlineText (DataBinder.Eval(Container.DataItem, "Ustate").ToString ()) %>
																	<p align="center"><span style="padding: 2px;"><asp:checkbox runat="server" id="chkEdit" style="horizontal-align: right;" /></span></p>
																	<span class="cb"><span class="cl"></span></span>
																	<input type="hidden" runat="server" id="hidFriendId" value='<%#DataBinder.Eval(Container.DataItem, "UserId")%>' name="hidFriendId">
																</div>
															</div>
															<!-- END FRIENDS BOX -->
															
														</itemtemplate>
													</asp:datalist>	
													</div>
														
													<span class="cb"><span class="cl"></span></span>
												</div>
												</ajax:ajaxpanel>
												
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
				</tr>
				<tr>
					<td class="frBottomLeft"></td>
					<td class="frBottom"></td>
					<td class="frBottomRight"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>

