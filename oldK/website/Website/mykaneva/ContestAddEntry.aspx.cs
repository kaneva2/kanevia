///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using log4net;

namespace KlausEnt.KEP.Kaneva.mykaneva
{
    public partial class ContestAddEntry : BasePage
    {
        protected void Page_Load (object sender, EventArgs e)
        {
            // They must be logged in
            if (!Request.IsAuthenticated)
            {
                Response.Redirect (GetLoginURL ());
                return;
            }

            // If user trying to open this page directly, redirect to contests page
//            if (!IsPostBack && Request.UrlReferrer != null && Request.UrlReferrer.AbsolutePath != ResolveUrl ("~/mykaneva/contests.aspx"))
//            {
//                Response.Redirect (ResolveUrl ("~/mykaneva/contests.aspx"));
//            }

            if (Request["id"] == null || Request["id"].ToString ().Length == 0)
            {
                m_logger.Error ("Error opening add contest entry dailog.  Contest Id from query string was null");
                ShowMessage ("We encountered an error trying to find the contest.  Please close this dialog and try again.");
                return;
            }

            try
            {
                ContestId = Convert.ToUInt64 (Request["id"]);

                if (ContestId > 0)
                {
                    entercontest.Visible = false;

                    // Check to make sure user can enter a contest
                    int access = contests.CanUserEnterContest (ContestId, Common.GetVisitorIPAddress(), Configuration.ContestCompareCurrentUserIP);
                    switch (access)
                    {
                        case (int) contests.eCONTEST_ACCESS.Email_Not_Validated:
                            {
                                noaccess.Visible = true;
                                spnValidEmail.Visible = true;
                                btnOk.Attributes.Add ("onclick", "parent.parent.location='general.aspx';parent.$j.colorbox.close(); return false;");
                                WriteColorBoxResizeJS ("150", "300");
                                return;
                            }

                        case (int) contests.eCONTEST_ACCESS.Below_Min_Fame_Level:
                            {
                                noaccess.Visible = true;
                                spnLevelToEnter.Visible = true;
                                litLevelToEnter.Text = Configuration.ContestMinimumFameLevelToEnter.ToString ();
                                WriteColorBoxResizeJS ("150", "300");
                                return;
                            }

                        case (int) contests.eCONTEST_ACCESS.IP_Match:
                            {
                                divRules.Visible = true;
                                WriteColorBoxResizeJS ("150", "300");
                                return;
                            }

                        default:
                            {
                                entercontest.Visible = true;
                                WriteColorBoxResizeJS ("360", "480");
                                break;
                            }
                    }

 //                   WriteColorBoxResizeJS ("370");
                }
                else
                {
                    ShowMessage ("We could not find the contest you want to enter.  Please close this dialog and try again.");
                }
            }
            catch (Exception ex)
            {
                m_logger.Error ("Error opening add contest entry dailog.", ex);
                ShowMessage ("We encountered an error.  Please try entering this contest again.");
            }
         }

        // This is used to resize the colorbox lightbox in Firefox.  For whatever reason
        // FF will not resize correctly to fit the page when it loads so we have to 
        // resize it manually with this javascript
        private void WriteColorBoxResizeJS (string height)
        {
            WriteColorBoxResizeJS (height, "");
        }
        private void WriteColorBoxResizeJS (string height, string width)
        {
 //           litDocHeight.Text = height + ";";
 //           if (width.Length > 0)
 //           {
 //               litDocHeight.Text += "iW = " + width + ";";
  //          }
 //           litDocHeight.Text += "parent.$j.fn.colorbox.myResize(iW, iH);";
            litDocHeight.Text += "parent.$j.fn.colorbox.resize({innerWidth:'" + width + "px', innerHeight:'" + height + "'});";
        }

        private void ShowMessage (string msg)
        {
            litMessage.Text = msg;
            divMessage.Visible = true;
            noaccess.Visible = false;
            entercontest.Visible = false;
            WriteColorBoxResizeJS ("150", "280");
        }
        private bool ValidateTextValue (string value, string fieldName)
        {
            // Check for any inject scripts
            if (KanevaWebGlobals.ContainsInjectScripts (value))
            {
                m_logger.Warn ("User " + KanevaWebGlobals.CurrentUser.UserId.ToString () + " tried to script from IP " + Common.GetVisitorIPAddress());
                ShowMessage ("Your input contains invalid scripting, please remove script code and try again.");
                return false;
            }

            // Check to make sure user did not enter any "potty mouth" words
            if (KanevaWebGlobals.isTextRestricted (value, Constants.eRESTRICTION_TYPE.POTTY_MOUTH))
            {
                ShowMessage ("Your input includes some restricted words.  Please remove these and try again.");
                return false;
            }

            if (value.Trim ().Length == 0)
            {
                ShowMessage ("Please enter a " + fieldName + ".");
                return false;
            }

            if (value.Length > KanevaGlobals.MaxContestLength)
            {
                ShowMessage ("Your " + fieldName + " is too long");
                return false;
            }
            return true;
        }

        public void btnSubmit_Click (object sender, EventArgs e)
        {
            // Confirm user is logged in
            if (!Request.IsAuthenticated)
            {
                RedirectToHomePage ();
            }

            if (!ValidateTextValue (txtEntryDesc.Text, "Description"))
            {
                rfvDesc.IsValid = false;
                return;
            }

            ContestEntry entry = new ContestEntry ();
            entry.ContestId = ContestId;  
            entry.Owner.UserId = KanevaWebGlobals.CurrentUser.UserId;
            entry.Description = Server.HtmlEncode (txtEntryDesc.Text);

            if (entry.ContestId > 0)
            {
                int entryId = GetContestFacade.InsertContestEntry (entry);
                if (entryId > 0)
                {
                    if (cbBlast.Checked)
                    {
                        Contest c = GetContestFacade.GetContest (entry.ContestId);
                        if (c.ContestId > 0)
                        {
                            UserFacade userFacade = new UserFacade();
                            string requestId = userFacade.InsertTrackingRequest(Constants.cREQUEST_TRACKING_REQUEST_TYPE_ID_CONTEST_ENTERED, KanevaWebGlobals.CurrentUser.UserId, 0, 0);
                            string requestTypeSentIdBlast = userFacade.InsertTrackingTypeSent(requestId, Constants.cREQUEST_TRACKING_MESSAGE_TYPE_ID_BLAST, 0);

                            GetBlastFacade.SendContestEnteredBlast (KanevaWebGlobals.CurrentUser.UserId,
                                ResolveUrl("~/mykaneva/contests.aspx?contestId=" + c.ContestId.ToString() + "&" + Constants.cREQUEST_TRACKING_URL_PARAM + "=" + requestTypeSentIdBlast),
                                c.Title, c.PrizeAmount, KanevaWebGlobals.CurrentUser.Username,
                                KanevaWebGlobals.CurrentUser.NameNoSpaces, KanevaWebGlobals.CurrentUser.ThumbnailSmallPath,
                                KanevaWebGlobals.CurrentUser.Gender);
                        }
                    }
                }
                else
                {
                    ShowMessage ("We encountered an error saving your entry.  Please try entering this contest again.");
                }
            }
            else
            {
                ShowMessage ("We encountered an error creating your entry.  Please try entering this contest again.");
            }

            // Close Window. reload contest list with new contest added           parent.parent.SetDisplayOrder('createdate');parent.$j.colorbox.close();
            Page.ClientScript.RegisterStartupScript (this.GetType (), "SaveBtn", "parent.parent.$('btnReload').click();parent.$j.colorbox.close();", true);
        }

        private UInt64 ContestId
        {
            get
            {
                try
                {
                    if (ViewState["id"] != null && ViewState["id"].ToString ().Length > 0)
                    {
                        return Convert.ToUInt64 (ViewState["id"]);
                    }
                    return 0;
                }
                catch
                {
                    return 0;
                }
            }
            set
            {
                ViewState["id"] = value;
            }
        }

        #region Declerations

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

        #endregion Declerations
    }
}