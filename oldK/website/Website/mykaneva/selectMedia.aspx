<%@ Page language="c#" Codebehind="selectMedia.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.selectMedia" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>

<link href="../css/kaneva.css" rel="stylesheet" type="text/css" />
<link href="../css/friends.css" rel="stylesheet" type="text/css" />
<link href="../css/PageLayout/popup.css" type="text/css" rel="stylesheet">
<link href="../css/new.css" type="text/css" rel="stylesheet">

<script type="text/JavaScript">
	
	var active = '<asp:literal id="litActiveTab" runat="server"/>';
	var img = new Array();
	
	img[0] = new Image(109,40); 
	img[0].src = "../images/tabsMedia.gif"; 

	img[1] = new Image(109,40);
	img[1].src = "../images/tabsMedia_on.gif";
	
	function Swap(t){
		t.style.background = 'url('+img[0].src+')';
		return true;
	}

	function SwapBack(t, o){
		if (t.id != active)
			t.style.background = 'url('+img[1].src+')';
		return true;
	}
	
	function setActiveTab(t)
	{
		active = t;
	}
	
	function setHilite(t)
	{
		setActiveTab(t);
		t.className = 'selected';
		return false;	
	}
</script>

<table border="0" cellspacing="0" cellpadding="10" width="95%" bgcolor="#EEEEEE">
	<tr>
		<td align="left">
		
			<div class="module whitebg">
			<span class="ct"><span class="cl"></span></span>
			<h2>Select Media</h2>
			<br>	
			
			<!-- Tabs -->
				<ajax:ajaxpanel id="ajpResultsTabs" runat="server">
					<input type="hidden" id="hidSearchType" value="" runat="server" name="hidSearchType"/>

					<table border="0" cellpadding="0" cellspacing="0"  >
						<tr>
							<td><img src="~/images/spacer.gif" runat="server" width="10" height="1" id="Img1"/></td>
							<td align="left">
								<table border="0" cellpadding="0" cellspacing="0" width="109" >
									<tr>
										<td id="tdVideoTab" runat="server" colspan="3" onmouseover="Swap(this);" onmouseout="SwapBack(this);" width="109" height="25" align="center">
											<table border="0" cellpadding="4" cellspacing="0"  >
												<tr>
													<td valign="middle"><asp:linkbutton id="lbVideoIcon" runat="server" commandname="video" oncommand="SearchTab_Changed"><img runat="server" src="~/images/videos.gif" width="14" height="20" border="0" id="Img6"/></asp:linkbutton></td>
													<td nowrap align="center" class="insideBoxText11"><asp:linkbutton id="lbVideo" runat="server" commandname="video" oncommand="SearchTab_Changed">Videos</asp:linkbutton></td>
												</tr>
											</table>	
										</td>
									</tr>
								</table>
							</td>
							<td width="5"><img runat="server" src="~/images/spacer.gif" width="2" height="1" id="Img7"/></td>
							<td align="left">
								<table border="0" cellpadding="0" cellspacing="0" width="109">
									<tr>
										<td id="tdPhotoTab" runat="server" colspan="3" onmouseover="Swap(this);" onmouseout="SwapBack(this);" width="109" height="25" align="center">
											<table border="0" cellpadding="4" cellspacing="0" >
												<tr>
													<td valign="middle"><asp:linkbutton id="lbPhotoIcon" runat="server" commandname="photo" oncommand="SearchTab_Changed"><img runat="server" src="~/images/photos.gif" width="20" height="20" border="0" id="Img8"/></asp:linkbutton></td>
													<td nowrap class="insideBoxText11" align="center"><asp:linkbutton id="lbPhoto" runat="server" commandname="photo" oncommand="SearchTab_Changed">Photos</asp:linkbutton></td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
							<td width="5"><img runat="server" src="~/images/spacer.gif" width="2" height="1" id="Img9"/></td>
							<td align="center">
								<table border="0" cellpadding="0" cellspacing="0" width="109">
									<tr>
										<td id="tdMusicTab" runat="server" colspan="3" onmouseover="Swap(this);" onmouseout="SwapBack(this);" width="109" height="25" align="center">
											<table border="0" cellpadding="4" cellspacing="0" >
												<tr>
													<td valign="middle"><asp:linkbutton id="lbMusicIcon" runat="server" commandname="music" oncommand="SearchTab_Changed"><img runat="server" src="~/images/music.gif" width="15" height="20" border="0" align="left" id="Img10"/></asp:linkbutton></td>
													<td nowrap class="insideBoxText11" align="center"><asp:linkbutton id="lbMusic" runat="server" commandname="music" oncommand="SearchTab_Changed">Music</asp:linkbutton></td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
							<td width="5"><img runat="server" src="~/images/spacer.gif" width="2" height="1" id="Img11"/></td>
							<td align="left">
								<table border="0" cellpadding="0" cellspacing="0" width="109">
									<tr>
										<td id="tdGameTab" runat="server" colspan="3" onmouseover="Swap(this);" onmouseout="SwapBack(this);" width="109" height="25" align="center">
											<table border="0" cellpadding="4" cellspacing="0" >
												<tr>
													<td valign="middle"><asp:linkbutton id="lbGameIcon" runat="server" commandname="games" oncommand="SearchTab_Changed"><img runat="server" src="~/images/games.gif" width="16" height="20" border="0" id="Img12"/></asp:linkbutton></td>
													<td nowrap class="insideBoxText11" align="center"><asp:linkbutton id="lbGame" runat="server" commandname="games" oncommand="SearchTab_Changed">Games</asp:linkbutton></td>
												</tr>
											</table>	
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</ajax:ajaxpanel>
				<!-- END TABS -->
					
				<br/>
				Media Groups: <asp:dropdownlist id="ddlGroup" runat="server" style="width:200px" class="Filter2" autopostback="true" />
				<br/>
				
				<ajax:ajaxpanel id="AjaxpanelSearchResults" runat="server">
				<asp:datalist visible="true" runat="server" showfooter="False" width="95%" id="dlMedia" cellpadding="2" 
					cellspacing="2" border="0" repeatcolumns="2" repeatdirection="Horizontal" cssclass="data">
					
					<itemstyle cssclass="lineItemThumb" horizontalalign="Center"/>
					<itemtemplate>
						<table border="0" cellspacing="0" cellpadding="0" class="data" >
							<tr>
								<td valign="middle">
									<input id="rbPicture" name="rbPicture" type="radio" value='<%# DataBinder.Eval( Container.DataItem, "asset_id") %>' onclick='document.frmMain.ihSinglePictureId.value=this.value' />
								</td>
								
								<td width="90%" align="left" valign="top">
									<img runat="server" src='<%#GetMediaImageURL (DataBinder.Eval(Container.DataItem, "thumbnail_medium_path").ToString () ,"me", Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_id")), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_type_id")))%>' border="0" style="border: 1px solid white;"/>
								</td>	
							</tr>
							<tr valign="top">	
								<td>&nbsp;</td>
								<td align="left" valign="top">
									<span class="insideTextNoBold" id="spnImageTitle" runat="server"></span>
								</td>
						</table>
					</itemtemplate>
					
				</asp:datalist>	
				</ajax:ajaxpanel>
			

				<table cellpadding="0" cellspacing="0" border="0" width="98%" style="text-align: right; padding: 10px 10px 0 0;">
					<tr>
						<td align="right">
							<strong><span id="spnAlertMsg" runat="server" class="alertmessage"></span></strong>
						</td>
					</tr>
					<tr>
						<td >
							<input type="button" onclick="javascript: window.close();" value="   Cancel   " class="filter2"/>&nbsp;&nbsp;<asp:button id="btnInsert" runat="server" onclick="btnInsert_Click" cssclass="filter2" text=" Insert Media Link "/>
						</td>
					</tr>
				</table>
			
			<span class="cb"><span class="cl"></span></span>
		</div>
		
		</td>
	</tr>
</table>
<input type="hidden" name="ihSinglePictureId" id="ihSinglePictureId" runat="server" />





