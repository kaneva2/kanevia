///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// <summary>
	/// Summary description for mailboxFriendRequests.
	/// </summary>
	public class inviteHistory : SortedBasePage
	{
		protected inviteHistory () 
		{
			Title = "Friend Invite History";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			// They must be logged in
			if (!Request.IsAuthenticated)
			{
				Response.Redirect (GetLoginURL ());
				return;
			}

            Response.Redirect (ResolveUrl("~/mykaneva/MessageCenter.aspx?mc=frs"));

			if (!IsPostBack)
			{
				CurrentSort = "reinvite_date";
				lbSortByDate.CssClass = "selected";
				
				int userId = GetUserId ();
                
                // had to add this to correct issue where filter is returning 12 as default page size value
                // if no value has been set.  Page size of 12 is used on some pages. Since this control is used
                // in many places, added a public way to set number of pages (DefaultNumberOfPages);
                searchFilter.CurrentPage = "invitehistory2"; 
                if (searchFilter.NumberOfPages == 12)
                {
                    searchFilter.DefaultNumberOfPages = 10;
                }

                BindData (1, userId);
			}

            // Set Nav
            HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
            HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.FRIENDS;
            HeaderNav.MyFriendNav.ActiveTab = NavFriend.TAB.MANAGE_INVITES;
            HeaderNav.SetNavVisible (HeaderNav.MyKanevaNav, 2);
            HeaderNav.SetNavVisible (HeaderNav.MyFriendNav);
        }

		/// <summary>
		/// Bind the data
		/// </summary>
		/// <param name="pageNumber"></param>
		private void BindData (int pageNumber, int userId)
		{
			BindData (pageNumber, userId, "");	
		}
		/// <summary>
		/// Bind the data
		/// </summary>
		/// <param name="pageNumber"></param>
		private void BindData (int pageNumber, int userId, string filter)
		{
			spnAlertMsg.InnerText = string.Empty;

			searchFilter.CurrentPage = "invitehistory2";

			if (filter.Length > 0)
			{
				filter += " AND ";
			}
			filter += " invite_status_id IN (" + (int) Constants.eINVITE_STATUS.REGISTERED + ", " +
                    (int)Constants.eINVITE_STATUS.UNREGISTER + ", " +
                    (int)Constants.eINVITE_STATUS.WOK_LOGIN + ", " +
                    (int)Constants.eINVITE_STATUS.DECLINED + ", " +
                    (int)Constants.eINVITE_STATUS.MAX_IP +  ")";

			// Set the sortable columns
			string orderby = CurrentSort + " " + CurrentSortOrder;
			int pgSize = searchFilter.NumberOfPages;

			PagedDataTable pds = UsersUtility.GetAllInvites(userId, filter, orderby, pageNumber, pgSize);

			rptInvites.DataSource = pds;
			rptInvites.DataBind ();

			pgTop.NumberOfPages = Math.Ceiling ((double) pds.TotalCount / pgSize).ToString ();
			pgTop.DrawControl ();

			// The results
			lblSearch.Text = GetResultsText (pds.TotalCount, pageNumber, pgSize, pds.Rows.Count);

            // Get Fame Pts for invites
            FameFacade fameFacade = new FameFacade ();                            
		    DataRow drFamePts = fameFacade.GetFamePointsFromInvitingFriends (KanevaWebGlobals.CurrentUser.UserId);

            if (drFamePts != null)
            {
                spnFamePts.InnerText = drFamePts["total_points"].ToString ();         
            }
        }
                                                                          
		
		#region Helper Methods

		/// <summary>
		/// Send out an email
		/// </summary>
		/// <param name="emailAddress"></param>
		/// <returns></returns>
		private void SendEmail (int inviteId)
		{
			// Get the existing invite
			DataRow drInvite = UsersUtility.GetInvite(inviteId);

            if (drInvite != null)
            {
                string keyCode = drInvite["key_value"].ToString ();
                string toEmail = drInvite["email"].ToString ();
                string toName = drInvite["to_name"].ToString ();

                DataRow drNewUser = UsersUtility.GetUserByEmail (toEmail);

                // Does this email already belong to a Kaneva member?
                if (drNewUser == null)
                {
                    // Get the current user
                    string username = KanevaWebGlobals.CurrentUser.Username;
                    string additionalMsg = "";

                    // Send out the email										   
                    MailUtilityWeb.SendInvitation (KanevaWebGlobals.CurrentUser.UserId, toEmail, toName, additionalMsg, inviteId, keyCode, Page);
                }
            }
		}

		private string GetEmailMessage (string username)
		{
			return ("Hey there, <br><br> I wanted to share with you a cool Web Site and Virtual World called " +
				"<a style='color: green;' href=\"http://" + KanevaGlobals.SiteName + "\">Kaneva.</a>" +
				"<br>Join me at Kaneva and we can meet up on the web and in 3D!" +
				"<BR><BR>");
			
		}
		public string FormatDateOnly(object date)
		{
			DateTime dt = (DateTime) date;

			return dt.ToString("MMMM") + " " + dt.Day.ToString() + ", " + dt.Year.ToString(); 
		}
		protected string GetStatusName(int statusId)
		{
			string status = string.Empty;

			switch (statusId)
			{
				case (int) Constants.eINVITE_STATUS.REGISTERED:
					status = "Joined";
					break;
				case (int) Constants.eINVITE_STATUS.UNREGISTER:
					status = "Not Joined";
					break;
                case (int)Constants.eINVITE_STATUS.MAX_IP:
                    status = "Same IP*";
                    break;
                case (int)Constants.eINVITE_STATUS.WOK_LOGIN:
                    status = "Joined In World";
                    break;
                case (int)Constants.eINVITE_STATUS.DECLINED:
                    status = "Declined";
                    break;
				default:
					status = "Not Joined";
					break;
			}

			return status;
		}
		
        #endregion Helper Methods


		#region Event Handlers

		protected void btnDelete_Click (Object sender, ImageClickEventArgs e)
		{
			HtmlInputHidden hidInviteId;
			CheckBox chkEdit;
			int ret = 0;
			int count = 0;
			bool isItemChecked = false;

			// Remove the friends
			foreach (RepeaterItem rptInvite in rptInvites.Items)
			{
				chkEdit = (CheckBox) rptInvite.FindControl ("chkEdit");

				if (chkEdit.Checked)
				{
					isItemChecked = true;

					hidInviteId = (HtmlInputHidden) rptInvite.FindControl ("hidInviteId");
					ret = UsersUtility.UpdateInvite (Convert.ToInt32(hidInviteId.Value), (int) Constants.eINVITE_STATUS.DELETED);
				
					if (ret == 1)
						count++;
				}
			}

			if (isItemChecked)
			{
				if (count > 0)
				{
					BindData (1, GetUserId());
				}
				
				if (ret != 1)
				{
					spnAlertMsg.InnerText = "An error was encountered while trying to delete friend invite.";
				}
				else
				{
					spnAlertMsg.InnerText = "Successfully deleted " + count + " friend invite" + (count != 1 ? "s" : "") + ".";
				}
			}
			else
			{
				spnAlertMsg.InnerText = "No friend invites were selected.";
			}

			MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('spnAlertMsg', 5000);");
		}

		private void rptInvites_ItemCommand(object source, RepeaterCommandEventArgs e)
		{
			int ret = 0;
			string command = e.CommandName;
			spnAlertMsg.InnerText = "";

			HtmlInputHidden hidInviteId = (HtmlInputHidden) e.Item.FindControl("hidInviteId");
			int inviteId = Convert.ToInt32(hidInviteId.Value);

			if (command.Equals("cmdResend"))
			{
				//resend the invite message
				SendEmail(inviteId);

				//?? what if user signs up on their own, do all invites get updated??
				ret = UsersUtility.ResendInvite (inviteId);
				
				if (ret == 1)
				{
					BindData (pgTop.CurrentPageNumber, GetUserId());

					// Success
					spnAlertMsg.InnerText = "Successfully resent 1 friend invite.";
				}
				else
				{
					// Error
					spnAlertMsg.InnerText = "An error was encountered while trying to resend friend invite.";
				}
			}
			else if (command.Equals("cmdDelete"))
			{
				ret = UsersUtility.UpdateInvite (inviteId, (int) Constants.eINVITE_STATUS.DELETED);
				
				if (ret == 1)
				{
					BindData (pgTop.CurrentPageNumber, GetUserId());

					// Success
					spnAlertMsg.InnerText = "Successfully deleted 1 friend invite.";
				}
				else
				{
					// Error
					spnAlertMsg.InnerText = "An error was encountered while trying to delete friend invite.";
				}
			}

			MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('spnAlertMsg', 5000);");
		}

		private void rptInvites_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
			{
				ImageButton btnResend = (ImageButton) e.Item.FindControl("btnResend");
				CheckBox chkEdit = (CheckBox) e.Item.FindControl("chkEdit");

				DateTime dt = (DateTime)DataBinder.Eval(e.Item.DataItem, "reinvite_date");
				int statusId = (int)DataBinder.Eval(e.Item.DataItem, "invite_status_id");

				if ( (dt.AddDays(14) > DateTime.Now) || 
                     (statusId == (int)Constants.eINVITE_STATUS.REGISTERED) ||
                     (statusId == (int)Constants.eINVITE_STATUS.DECLINED) )
				{
					btnResend.Visible = false;
				}
				else
				{
					btnResend.Visible = true;
				}
			}
		}
		/// <summary>
		/// Page Change
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pgTop_PageChange (object sender, PageChangeEventArgs e)
		{
			BindData (e.PageNumber, GetUserId ());
		}

		/// <summary>
		/// Execute when the user selects an item from the Store Filter
		/// Items to display drop down list
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void FilterChanged(object sender, FilterChangedEventArgs e)
		{
			searchFilter.SetPagesDropValue(searchFilter.NumberOfPages.ToString());
			
			BindData (1, GetUserId(), e.Filter);
		}
		/// <summary>
		/// Execute when the user clicks the the view type link button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void lbSortBy_Click (object sender, EventArgs e) 
		{
			lbSortByEmail.CssClass = lbSortByDate.CssClass = "";

			if (((LinkButton)sender).Attributes["sortby"].ToString().ToLower() == "email")
			{
				CurrentSort = "email";
				CurrentSortOrder = "ASC";
				lbSortByEmail.CssClass = "selected";
			}
			else
			{
				CurrentSort = "reinvite_date";
				CurrentSortOrder = "DESC";
				lbSortByDate.CssClass = "selected";							  
			}

			BindData (pgTop.CurrentPageNumber, GetUserId());
		}
		
        #endregion Event Handlers


		#region Properties

		/// <summary>
		/// DEFAULT_SORT
		/// </summary>
		/// <returns></returns>
		protected override string DEFAULT_SORT
		{
			get
			{
				return "username";
			}
		}

		/// <summary>
		/// DEFAULT_SORT ORDER
		/// </summary>
		/// <returns></returns>
		protected override string DEFAULT_SORT_ORDER
		{
			get
			{
				return "DESC";
			}
		}

		#endregion Properties


		#region Declerations
		
        protected Kaneva.Pager pgTop;
		protected Kaneva.SearchFilter searchFilter;

		protected Label			lblSearch;
		protected Repeater		rptInvites;	
		protected LinkButton	lbSortByEmail, lbSortByDate;
		protected ImageButton	btnResend, btnDelete;

        protected HtmlContainerControl spnAlertMsg, spnFamePts;
		
        #endregion Declerations


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
			pgTop.PageChanged +=new PageChangeEventHandler (pgTop_PageChange);
			searchFilter.FilterChanged +=new FilterChangedEventHandler (FilterChanged);		
			
			this.rptInvites.ItemCommand +=new RepeaterCommandEventHandler (rptInvites_ItemCommand);
			this.rptInvites.ItemDataBound += new RepeaterItemEventHandler (rptInvites_ItemDataBound);
		}
		#endregion
	}
}
