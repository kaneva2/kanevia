<%@ Page language="c#" Codebehind="mailbox.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.mailbox" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../usercontrols/Pager.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="SearchFilter" Src="../usercontrols/SearchFilter.ascx" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>

<link href="../css/home.css" rel="stylesheet" type="text/css" />		
<link href="../css/kanevaSC.css" rel="stylesheet" type="text/css"/>
<link href="../css/friends.css" rel="stylesheet" type="text/css"/>
<link href="../css/kanevaText.css" type="text/css" rel="stylesheet">
<link href="../css/new.css" type="text/css" rel="stylesheet">

<script src="../jscript/prototype.js" type="text/javascript" language="javascript"></script>

<table border="0" cellspacing="0" cellpadding="0"  width="990" align="center">
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" /></td>
	</tr>
	<tr>
		<td>
			<table  border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="frTopLeft"></td>
					<td class="frTop"></td>
					<td class="frTopRight"></td>
				</tr>
				<tr>
					<td class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
					<td valign="top" class="frBgIntMembers">
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr colspan="2">
								<td>
								
									<!-- TOP STATUS BAR -->
									<div id="pageheader">
										<table cellpadding="0" cellspacing="0" border="0" width="99%">
											<tr>
												<td align="left">
													<h1>Private Messages</h1>
												</td>
									
												<td align="right" valign="middle">
													<table cellpadding="0" cellspacing="0" border="0" width="690">
														<tr>
															<td>	
																<ajax:ajaxpanel id="ajpTopStatusBar" runat="server">
																<table cellpadding="0" cellspacing="0" border="0" width="100%">
																	<tr>
																		<td class="headertout" width="175">
																			<asp:label runat="server" id="lblSearch" />
																		</td>
																		<td class="searchnav" width="260"> Sort by: 
																			<asp:linkbutton id="lbSortByDate" runat="server" sortby="Date" onclick="lbSortBy_Click">Date Received</asp:linkbutton> | 
																			<asp:linkbutton id="lbSortByName" runat="server" sortby="Name"  onclick="lbSortBy_Click">From</asp:linkbutton>
																		</td>
																		<td align="left" width="130">
																			<kaneva:searchfilter runat="server" id="searchFilter" loc="top" hidethumbview="true" showfilterlists="false" assetsperpagelist="10,20,40" />
																		</td>
																		<td class="searchnav" align="right" width="210" nowrap>
																			<kaneva:pager runat="server" isajaxmode="True" id="pgTop" maxpagestodisplay="5" shownextprevlabels="true" />
																		</td>
																	</tr>
																</table>
																</ajax:ajaxpanel>
															</td>	
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</div>
									<!-- END TOP STATUS BAR -->	
								
								</td>
							</tr>
							<tr>
								<td width="968" align="left"  valign="top">
									<table cellpadding="0" cellspacing="0" border="0"  width="968">
										<tr>
											<td width="250" valign="top">
												<div class="module toolbar">
													<span class="ct"><span class="cl"></span></span>
													<h2>Tools & Actions <span class="hlink"></span></h2>
													<table cellspacing="0" cellpadding="10" border="0" width="99%">
														<tr>
															<td align="center">
																<table cellspacing="0" cellpadding="3" border="0" width="100%">
																	<tr>
																		<td>Delete Selected Messages:</td>
																		<td align="center">
																			
																			<ajax:ajaxpanel id="ajpDeleteButton" runat="server">
																				<asp:imagebutton imageurl="~/images/button_delete.gif" alt="Delete" width="77" height="23" border="0" vspace="2" id="btnDelete" runat="server" onclick="btnDelete_Click"></asp:imagebutton>
																			</ajax:ajaxpanel>
																			
																		</td>
																	</tr>		  
																	<tr>
																		<td colspan="2">
																			<br/><br/><br/>
																			<asp:hyperlink id="lnkViewSent" runat="server">View Sent Private Messages</asp:hyperlink>
																		</td>
																	</tr>	  
																</table>
															</td>
														</tr>
													</table>
													<span class="cb"><span class="cl"></span></span>
												</div>
											</td>
											<td width="20"><img runat="server" src="~/images/spacer.gif" width="20" height="1" /></td>
											<td width="698" valign="top" align="center">
												
												<ajax:ajaxpanel id="ajpMessageList" runat="server">
												<div class="module whitebg">
													<span class="ct"><span class="cl"></span></span>
													<h2 class="alertmessage"><span id="spnAlertMsg" runat="server"></span></h2>
													
													<!-- Messages -->
													<table width="99%" id="dlFriends" border="0" cellpadding="5" cellspacing="0">
														<tr>
															<td align="center" width="99%">
															
															<div align="left" style="padding-left:3px;">

																	<table id="MessageFound" runat="server" width="99%" cellspacing="0" cellpadding="5" border="0" class="data">
																		<tr >
																			<th align="center" width="62" bgcolor="#fdfdfc" class="small">Select:<br><a href="javascript:void(0);" onclick="Select_All(true);">All</a> | <a href="javascript:void(0);" onclick="Select_All(false);">None</a></th>
																			<th align="center" width="51" bgcolor="#fdfdfc">Status</th>
																			<th bgcolor="#fdfdfc" class="name">Message</th>
																		</tr>
																	</table>
																	<table id="noMessageFound" runat="server" width="99%" cellspacing="0" cellpadding="5" border="0" class="data">
																		<tr>
																			<th bgcolor="#fdfdfc" class="name">You currently have no private messages.</th>
																		</tr>
																	</table>

															<asp:repeater runat="server" id="rptMailBox">  
																
																<headertemplate>
																	<table width="99%" cellspacing="0" cellpadding="5" border="0" class="data">
																</headertemplate>
																
																<itemtemplate>
																	
																	<tr class="altrow">
																		<td width="60" align="center"><asp:checkbox runat="server" id="chkEdit" style="horizontal-align: right;" /></td>
																		<td width="50" align="center">
																			<img runat="server" src='<%# GetTypeImage (Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "type")), DataBinder.Eval(Container.DataItem, "to_viewable").ToString (), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "replied")))%>'/>
																			<p class="note""><%# GetMailStatusText(DataBinder.Eval(Container.DataItem, "to_viewable").ToString(), Convert.ToInt32(DataBinder.Eval(Container.DataItem, "replied")))%></p>
																		</td>
																		<td width="500">
																			<table cellpadding="0" cellspacing="0" border="0">
																				<tr>
																					<td align="right" width="36" rowspan="2">
																						<a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "name_no_spaces").ToString ()) %>><img runat="server" src='<%#GetProfileImageURL (DataBinder.Eval(Container.DataItem, "thumbnail_small_path").ToString ())%>' width="30" border="0" hspace="5"/></a>
																					</td>
																					<td><h1><a runat="server" class=<%#GetMessageCssClass (DataBinder.Eval(Container.DataItem, "to_viewable").ToString ())%> title='<%# RemoveHTML (TruncateWithEllipsis (DataBinder.Eval(Container.DataItem, "message").ToString (), 100))%>' href='<%# "messageDetails.aspx?messageId=" + DataBinder.Eval(Container.DataItem, "message_id").ToString ()%>'>
																							<%# TruncateWithEllipsis (DataBinder.Eval(Container.DataItem, "subject").ToString (), 48)%>
																						</a></h1>
																					</td>
																				</tr>
																				<tr>
																					<td>
																						<p>From: <a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "name_no_spaces").ToString ()) %>>
																							<%#TruncateWithEllipsis (DataBinder.Eval(Container.DataItem, "username").ToString (), 20)%>
																						</a> | Sent: <%#FormatDate (DataBinder.Eval(Container.DataItem, "message_date"))%></p>
																					</td>
																				</tr>
																			</table><input type="hidden" runat="server" id="hidMessageId" value='<%#DataBinder.Eval(Container.DataItem, "message_id")%>' name="hidMessageId">
																		</td>
																	</tr>
																	
																</itemtemplate>
																
																<alternatingitemtemplate>
																	
																	<tr>
																		<td width="50" align="center"><asp:checkbox runat="server" id="chkEdit" style="horizontal-align: right;" /></td>
																		<td width="50" align="center">
																			<img runat="server" src='<%# GetTypeImage (Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "type")), DataBinder.Eval(Container.DataItem, "to_viewable").ToString (), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "replied")))%>'/>
																			<p class="note""><%# GetMailStatusText(DataBinder.Eval(Container.DataItem, "to_viewable").ToString(), Convert.ToInt32(DataBinder.Eval(Container.DataItem, "replied")))%></p>
																		</td>
																		<td width="500">
																			<table cellpadding="0" cellspacing="0" border="0">
																				<tr>
																					<td align="right" width="36" rowspan="2">
																						<a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "name_no_spaces").ToString ()) %>><img runat="server" src='<%#GetProfileImageURL (DataBinder.Eval(Container.DataItem, "thumbnail_small_path").ToString ())%>' width="30" border="0" hspace="5"/></a>
																					</td>
																					<td><h1><a runat="server" class=<%#GetMessageCssClass (DataBinder.Eval(Container.DataItem, "to_viewable").ToString ())%> title='<%# RemoveHTML (TruncateWithEllipsis (DataBinder.Eval(Container.DataItem, "message").ToString (), 100))%>' href='<%# "messageDetails.aspx?messageId=" + DataBinder.Eval(Container.DataItem, "message_id").ToString ()%>'>
																							<%# TruncateWithEllipsis (DataBinder.Eval(Container.DataItem, "subject").ToString (), 48)%>
																						</a></h1>
																					</td>
																				</tr>
																				<tr>
																					<td>
																						<p>From: <a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "name_no_spaces").ToString ()) %>>
																							<%#TruncateWithEllipsis (DataBinder.Eval(Container.DataItem, "username").ToString (), 20)%>
																						</a> | Sent: <%#FormatDate (DataBinder.Eval(Container.DataItem, "message_date"))%></p>
																					</td>
																				</tr>
																			</table><input type="hidden" runat="server" id="hidMessageId" value='<%#DataBinder.Eval(Container.DataItem, "message_id")%>' name="hidMessageId">
																		</td>
																	</tr>
																	
																</alternatingitemtemplate>
																
																<footertemplate>
																	</table>
																</footertemplate>
																
															</asp:repeater>			
														
														</td>
													</tr>
												</table>
												<span class="cb"><span class="cl"></span></span>
												</div>
												</ajax:ajaxpanel>
												
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
				</tr>
				<tr>
					<td class="frBottomLeft"></td>
					<td class="frBottom"></td>
					<td class="frBottomRight"></td>
				</tr>
				<tr>
					<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" /></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
				  
