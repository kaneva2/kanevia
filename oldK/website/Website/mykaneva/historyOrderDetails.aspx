<%@ Page language="c#" Codebehind="historyOrderDetails.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.historyOrderDetails" %>

<link href="../css/new.css" rel="stylesheet" type="text/css">
<link href="../css/shadow.css" rel="stylesheet" type="text/css">

<table border="0" cellspacing="0" cellpadding="0"  width="990" align="center">
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" id="Img1" width="1" height="14" /></td>
	</tr>
	<tr>
		<td>
			<table  border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="frTopLeft"></td>
					<td class="frTop"></td>
					<td class="frTopRight"></td>
				</tr>
				<tr>
					<td class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img2"/></td>
					<td valign="top" class="frBgIntMembers">
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="968" align="left"  valign="top">
								
									<!-- TOP STATUS BAR -->
									<div id="pageheader">
										<table cellpadding="0" cellspacing="0" border="0" width="99%">
											<tr>
												<td align="left">
													<h1>View Order Details</h1>
												</td>
									
												<td align="right" valign="middle">
													<table cellpadding="0" cellspacing="0" border="0" width="690">
														<tr>
															<td>	
																<span id='ajpTopStatusBar$RBS_Holder'><span id="ajpTopStatusBar" ajaxcall="async">
																<table cellpadding="0" cellspacing="0" border="0" width="100%">
																	<tr>
																		<td class="headertout" width="175"></td>
																		<td class="searchnav" width="260"></td>
																		<td align="left" width="130"></td>
																		<td class="searchnav" align="right" width="210" nowrap></td>
																	</tr>
																</table>
																</span></span>
															</td>	
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</div>
									<!-- END TOP STATUS BAR -->	
									
									<table cellpadding="0" cellspacing="0" border="0"  width="100%">
										<tr>
											<td width="970" valign="top" align="center">
											
												<table border="0" cellspacing="0" cellpadding="0" width="99%%">
													<tr>
														<!-- START WIZARD CONTENT -->
														<td valign="top" align="center">
															
															<table border="0" cellspacing="0" cellpadding="0">
																<tr>
																	<td width="970">
																		
																		<div class="module whitebg fullpage">
																		<span class="ct"><span class="cl"></span></span>
																				
																		<table border="0" cellspacing="0" cellpadding="0" align="center" class="wizform" style="width:99%">
																			<tr>
																				<td align="center">
																					
																					<table width="670" cellspacing="0" cellpadding="0" border="0">
																						<tr>
																							<td><br/>
																								<table border="0" cellspacing="0" cellpadding="0" width="94%" align="center" class="data nopadding">
																									<tr>
																										<td colspan="2" style="padding: 8px 0 8px 5px;"><h2>Completed Order Summary</h2></td>
																									</tr>
																									<tr>
																										<td align="left" width="35%">Order Number</td>
																										<td align="left" width="65%"><asp:label id="lblOrderNumber" runat="server"/></td>
																									</tr>
																									<tr>
																										<td align="left">Description</td>
																										<td align="left"><asp:label id="lblDescription" runat="server"/></td>
																									</tr>
																									<tr>
																										<td align="left">Amount Paid</td>
																										<td align="left"><asp:label id="lblAmountPaid" runat="server"/></td>
																									</tr>
																									<tr>
																										<td align="left">Credits Received</td>
																										<td align="left"><asp:label id="lblKPoints" runat="server"/></td>
																									</tr>
																									<tr>
																										<td align="left">Transaction Date</td>
																										<td align="left"><asp:label id="lblDate" runat="server"/></td>
																									</tr>
																									<tr>
																										<td align="left">Payment Method</td>
																										<td align="left">
																											<table border="0" cellspacing="0" cellpadding="0" align="left">
																												<tr>
																													<td><asp:label id="lblName" runat="server"/></td>
																												</tr>
																												<tr>
																													<td><asp:label id="lblCardType" runat="server"/></td>
																												</tr>
																												<tr>
																													<td><asp:label id="lblCardNumber" runat="server"/></td>
																												</tr>
																											</table>
																										</td>
																									</tr>
																									<tr id="trBillAddress" runat="server">
																										<td align="left">Billing Address</td>
																										<td align="left">
																											<table border="0" cellspacing="0" cellpadding="0" align="left" width="100%">
																												<tr>
																													<td align="left"><asp:label id="lblAddress1" runat="server"/>
																														<asp:label id="lblAddress2" runat="server"/></td>
																												</tr>
																												<tr>
																													<td align="left"><asp:label id="lblCity" runat="server"/>, <asp:label id="lblState" runat="server"/> <asp:label id="lblZip" runat="server"/></td>
																												</tr>
																												<tr>
																													<td align="left"><asp:label id="lblCountry" runat="server"/></td>
																												</tr>
																											</table>
																										</td>
																									</tr>
																								</table>
																								<br/><br/>
																								<table border="0" cellspacing="0" cellpadding="0" width="97%">
																									<tr>
																										<td align="right"><a href="~/mykaneva/transactions.aspx?CashP=Y" runat="server">Back to Transactions</a><br/><br/></td>
																									</tr>
																								</table>
																								<br/>
																							</td>
																						</tr>
																					</table>
																					
																				</td>
																			</tr>
																		</table>
																		
																		<span class="cb"><span class="cl"></span></span>
																		</div>
																	</td>
																</tr>
															</table>
															
														</td>
														
														<!-- END WIZARD CONTENT -->
													</tr>
												</table>
											
											</td>
											
										</tr>
										
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" id="Img5" width="1" height="1" /></td>
				</tr>
				<tr>
					<td class="frBottomLeft"></td>
					<td class="frBottom"></td>
					<td class="frBottomRight"></td>
				</tr>
				
			</table>
		</td>
	</tr>
</table>
