<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="buyAccess.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.mykaneva.buyAccess" %>
<%@ Register TagPrefix="Kaneva" TagName="WizardNavBar" Src="../usercontrols/checkoutwizardnavbar.ascx" %>

<link href="../css/shadow.css" rel="stylesheet" type="text/css">
<link href="../css/new.css" rel="stylesheet" type="text/css">

<!--[if IE]> <style type="text/css">@import "../css/new_IE.css";</style> <![endif]-->

<table border="0" cellspacing="0" cellpadding="0"  width="990" align="center">
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" id="Img1" width="1" height="14" /></td>
	</tr>
	<tr>
		<td>
			<table  border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="frTopLeft"></td>
					<td class="frTop"></td>
					<td class="frTopRight"></td>
				</tr>
				<tr>
					<td class="frBorderLeft"><img id="Img2" runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
					<td valign="top" class="frBgIntMembers">
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="968" align="left"  valign="top">
									
									<table cellpadding="0" cellspacing="0" border="0"  width="100%">
										<tr>
											<td width="968" valign="top">
												
												<kaneva:wizardnavbar runat="server" id="ucWizardNavBar"/>
												
											</td>
										</tr>
										<tr>
											<td width="970" valign="top">
											
												<table border="0" cellspacing="0" cellpadding="0" width="100%">
													<tr>
														<!-- START WIZARD CONTENT -->
														<td valign="top">
															
															<table border="0" cellspacing="0" cellpadding="0">
																<tr>
																	<td width="968">
																				
																		<table border="0" cellspacing="0" cellpadding="0"  align="center">
																			<tr>
																				<td align="left">
																					<div class="bodyModule">
																					<span class="ct"><span class="cl"></span></span>
																						<table width="968" cellSpacing="0" cellPadding="0" border="0">
																							<tr>
																								<td align="left" valign="top" width="140">
																									
																									<div  style="position: relative; left: -30px; top: -110px;"><img class="png" src="../images/access_pass.png" alt="Access Pass" width="191" height="380" border="0"></div>
																									
																								</td>
																								<td valign="top" width="828" align="left">
																								
																								
																									<div class="pitch">
																										<h1 class="center">Your ticket to special privileges for members 18 and over.</h1>
																										<h2 class="center">Get in on exclusive content, 3D hangouts, special events and items.</h2>
																										<h3 class="center">Be part of the Access community!</h3>
																										
																									</div>
																										
																									
																									<table border="0" cellspacing="0" cellpadding="0" align="center" width="730">
																										<tr>
																											<td colspan="2" class="large">
																											
																												<hr>
																												<br>Limited introductory pricing for an annual Access Pass includes:
																												<br><br>
																											</td>
																										</tr>
																										<tr>
																											<td width="360">
																												<ul class="large">
																													<li>Access to restricted (18+) areas and content</li>
																													<li>Exclusive clothing and items</li>
																													<li>Access to exclusive spaces and events</li>
																												</ul>
																											</td>
																											<td width="360">
																												<ul class="large">
																													<li>Ability to use items with other pass holders</li>
																													<li>Features to restrict your home to allow only other Access Pass holders to enter</li>
																												</ul>
																											</td>
																										</tr>
																										<tr>
																											<td colspan="2" class="large">
																											<br>
																											<div class="module noshadow bluebg3">
																												<span class="ct"><span class="cl"></span></span>
																												
																													<table border="0" cellspacing="0" cellpadding="0" width="98%" class="nopadding aligncenter">
																														<tr>
																															<td width="45" rowspan="2" valign="top"><div style="position: relative; top: -8px; left: -4px;"><img class="png" runat="server" src="~/images/access_pass_small.png" alt="Access Pass" width="45" height="58" border="0"></div></td>
																															<td valign="top">
																																
																																<h1 class="purple">Access Pass Combo Pack</h1>
																																<p class="bold alignleft">Includes an annual Access Pass <span style="font-size:9px;">(a $14.99 value good for one year)</span><br />PLUS...</p>
																																<br>
																																<ul class="purparrow nopadding">	
																																    <li>4000 Credits</li>																																
																																	<li>For men only...exclusive boxer shorts and wearable tattoo</li>
																																	<li>For women only...exclusive cat-suit costume and sexy lingerie</li>
																																</ul>
																													
																															</td>
																															<td><div style="position: relative; top: -13px; right: -10px;"><img class="png" runat="server" src="~/images/accesspass_combo.png" alt="Access Pass Combo Pack" width="273" height="140" border="0"></div></td>
																														</tr>
																														<tr>
																															<td colspan="2">
																																<table border="0" cellspacing="0" cellpadding="0" width="100%">
																																	<tr>
																																		<td></td>
																																		<td class="large italic purple" align="right">Get the <strong>Combo Pack</strong> including an Access Pass for only $39.99</td>
																																		<td align="right" width="140"><asp:imagebutton id="btnBuyAPPlus" runat="server" imageurl="~/images/btn_buynow_purp.gif" cssclass="purpleBtn" commandname="access_pass_plus" commandargument="access_pass_plus" oncommand="btnPurchase_Click" alternatetext="Buy Now" height="32" width="109" /></td>
																																	</tr>
																																</table>
																															
																															</td>
																														</tr>
																													</table>
																												<span class="cb"><span class="cl"></span></span>
																												</div>
																												                                                            
																												<div class="module noshadow ltGray">
																												<span class="ct"><span class="cl"></span></span>
																												
																													<table border="0" cellspacing="0" cellpadding="0" width="98%" class="nopadding aligncenter" style="margin-top: -5px;">
																														<tr>
																															<td width="45" rowspan="2" valign="top"><div style="position: absolute; top: -8px; left: -4px;"><img class="png" runat="server" src="~/images/access_pass_small.png" alt="Access Pass" width="45" height="58" border="0"></div></td>
																															<td valign="top">
																																<h1 class="pink">Access Pass</h1>
																																<p class="bold alignleft">Includes an Access Pass good for one year</p>
																															</td>
																															<td class="large italic pink" align="right"><strong>Access Pass</strong> only $14.99</td>      
																															<td align="right" width="140">
																															<asp:imagebutton id="btnBuyAP" runat="server" imageurl="~/images/btn_buynow_pink.gif" cssclass="pinkBtn" commandname="access_pass" commandargument="access_pass" oncommand="btnPurchase_Click" alternatetext="Buy Now" height="32" width="109" /></td>
																														</tr>
																													</table>
																												                                                                                                           
																												<span class="cb"><span class="cl"></span></span>
																												</div>
																												
																												<br><br>
																												<p>Note: Access Pass may not be given as a gift.</p>
																												<p>For related information visit Kaneva's <a href="#">Rules of Conduct</a> and <a href="#">Safety Center</a>.</p>
																												
																											</td>
																										</tr>
																									</table>
																									<br>																					
																									
																									
																								
																								</td>
																							
																							</tr>
																						</table>
																						
																						
																					<span class="cb"><span class="cl"></span></span>
																					</div>
																					
																				</td>
																			</tr>
																		</table>
																		
																	</td>
																</tr>
															</table>
															
														</td>
														
														<!-- END WIZARD CONTENT -->
													</tr>
												</table>

												<span id="spnMessage" runat="server" />
																							
											</td>
											
										</tr>
										
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td class="frBorderRight"><img id="Img3" runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
				</tr>
				<tr>
					<td class="frBottomLeft"></td>
					<td class="frBottom"></td>
					<td class="frBottomRight"></td>
				</tr>
				
			</table>
		</td>
	</tr>
</table>
