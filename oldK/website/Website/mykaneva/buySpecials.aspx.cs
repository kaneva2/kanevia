///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva.usercontrols;
using Kaneva.BusinessLayer.BusinessObjects;
using System.Collections.Generic;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
    public partial class buySpecials : BasePage
    {
        #region Declarations

        protected Repeater rptCredits;
        protected DataList dlSpecialItems;
        protected Literal litScript, litSpecialCredits, litSpecialAmount, litFirstTimeBuyer;
        protected ImageButton btnPurchase;

        protected HtmlContainerControl sticker;
        protected HtmlContainerControl divSpecialDescription;
        protected HtmlInputHidden ihd_promotionId, selectedPackage;
        protected HtmlTable specialbox;

        private bool showPassConfig = false; //shows pass config if true specials config otherwise
        private bool showMaturePasses = false;
        List<Promotion> dlSpecials = new List<Promotion>();
        private string promotionTypeFilter = "";
        private int oneTimeSpecialId = 0;
        private string _styleSheets = "\n<link href=\"../css/purchase_flow/purchase_special.css\" rel=\"stylesheet\" type=\"text/css\" />";
        private string _javascript = "<script type=\"text/javascript\" src=\"../jscript/prototype-1.6.0.3-min.js\"></script>";

        #endregion

        protected buySpecials() 
		{
            Title = "Select Your Special";
		}

		private void Page_Load(object sender, EventArgs e)
		{
            //verify the user is registered and signed in - if not do not allow them to purchase
            //redirects user
            VerifyUserAuthenticated ();
            
            //set is administrator variable to prevent multiple function calls
            bool _isAdministrator = IsAdministrator();
            int _userID = KanevaWebGlobals.CurrentUser.UserId;

            //if check out not enabled and they are not an administrator redirect them
			if (!KanevaGlobals.EnableCheckout)
			{
                if (!_isAdministrator)
				{
                    Response.Redirect("~/error.aspx?error=" + (int)Constants.eERROR_TYPE.CHECKOUT_CLOSED);
                }
			}

            // if user meets eligibility sets a flag to allow them to see access pass packages
            VerifyMaturePassEligibility();

            //process the page if it is not a post back or an error message reload
			//if (!IsPostBack )
			{                
                //get the preview date from the request - if there is one process the page as a preview page
                DateTime previewDate = DateTime.Now;
                if (Request["previewDate"] != null && Request["previewDate"] != string.Empty)
                {
                    previewDate = Convert.ToDateTime(Request["previewDate"].ToString());
                }

                DataTable onetimePurchases = new DataTable();

                //Process the page as the "Passes page"
                if ((Request["pass"] != null) && (Request["pass"].ToString() == "true"))
                {
                    //check and see if user has choosen to purchase from a pass details page and auto process it
                    if (Request["passId"] != null)
                    {
                        try
                        {
                            SelectSpecialId = Request["passId"].ToString();
                            ProcessTrial(Convert.ToInt32(SelectSpecialId), "trial");
                        }
                        catch (FormatException)
                        {
                            spnMessage.Visible = true;
                            spnMessage.InnerText = "Unable to process your trial request.";
                        }
                    }

                    //show page in pass configuration
                    showPassConfig = true;
                    lnk_Passes.Visible = false;
                    lnk_Specials.Visible = true;
                    div_passessHeading.Visible = true;
                    div_specialsHeading.Visible = false;

                    //create filter for passes
                    promotionTypeFilter = ((int)Promotion.ePROMOTION_TYPE.SUBSCRIPTION_PASS).ToString();

                    //add on the access promotion type IF they are old enough to buy one or an Admin
                    if (showMaturePasses || _isAdministrator)
                    {
                        promotionTypeFilter += "," + ((int)Promotion.ePROMOTION_TYPE.MATURE_SUBSCRIPTION_PASS).ToString();
                    }
                }
                else //process the page as the "Specials page"
                {
                    //hide page's pass configuration
                    showPassConfig = false;
                    lnk_Passes.Visible = true;
                    lnk_Specials.Visible = false;
                    div_passessHeading.Visible = false;
                    div_specialsHeading.Visible = true;

                    //set the purchase filter for first time purchase check and initial variable values
                    promotionTypeFilter = ((int)Promotion.ePROMOTION_TYPE.SPECIAL).ToString() + "," + ((int)Promotion.ePROMOTION_TYPE.SPECIAL_FIRST_TIME).ToString() + "," + ((int)Promotion.ePROMOTION_TYPE.SPECIAL_ONE_TIME).ToString();
                    bool isFirstTimePurchase = false;

                    //check to see if the user has done any of the one time or exclusive purchases before - don't do for administrators
                    //don't check credits
                    if (!_isAdministrator)
                    {
                        //see if they are a firs time purchaser or not
                        isFirstTimePurchase = GetPromotionsFacade.GetUserTransactionCountInclusive(_userID, promotionTypeFilter) > 0 ? false : true;

                        //get any one time purchases they have made if they are not a 1st time purchaser
                        if (!isFirstTimePurchase)
                        {
                            onetimePurchases = GetPromotionsFacade.GetUserPurchasesByType(_userID, (int)Promotion.ePROMOTION_TYPE.SPECIAL_ONE_TIME);
                        }
                    }

                    //set up promotional filter for returning packages
                    if (isFirstTimePurchase || _isAdministrator)
                    {
                        promotionTypeFilter = (int)Promotion.ePROMOTION_TYPE.SPECIAL + "," + (int)Promotion.ePROMOTION_TYPE.SPECIAL_FIRST_TIME + "," + (int)Promotion.ePROMOTION_TYPE.SPECIAL_ONE_TIME;
                    }
                    else
                    {
                        promotionTypeFilter = (int)Promotion.ePROMOTION_TYPE.SPECIAL + "," + (int)Promotion.ePROMOTION_TYPE.SPECIAL_ONE_TIME;
                    }
                }

                //get active promotions
                if (promotionTypeFilter != "")
                {
                    dlSpecials = GetPromotionsFacade.GetActivePromotions(promotionTypeFilter, Constants.CURR_KPOINT, 0.0, previewDate);
                }


                // A/B Test VIP Tiers
                string vipExperimentId = "7c5f1ad8-a12d-11e6-84f2-a3cf223dc93b";
                //string vipGroupA = "8b67bc97-a12d-11e6-84f2-a3cf223dc93b";
                string vipGroupB = "9b6d0499-a12d-11e6-84f2-a3cf223dc93b";

                ExperimentFacade experimentFacade = new ExperimentFacade();
                UserExperimentGroup userExperimentGroup = experimentFacade.GetUserExperimentGroupCommon(KanevaWebGlobals.CurrentUser.UserId, vipExperimentId);

                // If user is not in Group B, then remove the 4.99 special
                if (userExperimentGroup != null && userExperimentGroup.GroupId != vipGroupB)
                {
                    int idx = dlSpecials.FindIndex(p => p.PromotionId == 91);
                    if (idx > -1)
                    {
                        dlSpecials.RemoveAt(idx);
                    }
                }
                // -- End A/B test code


                //if there are any special returned proces the results
                if (dlSpecials.Count > 0)
                {
                    //configure div to show correct data vs messaging
                    //divPackages.Visible = false;
                    //divSpecials.Visible = true;
                    //remove any one time specials if they have already bought them
                    if (onetimePurchases.Rows.Count > 0)
                    {
                        foreach (DataRow row in onetimePurchases.Rows)
                        {
                            //set the onetimePurchase id for use by the List delegate
                            try
                            {
                                oneTimeSpecialId = Convert.ToInt32(row[""]);
                            }
                            catch (Exception)
                            {
                                //nothing is done because the oneTimeSpecialId will get either the last value (which is already removed)
                                //or the default value
                            }
                            //this function and associated delegate required to search because List don't have
                            //the querying capability of datatables
                            dlSpecials.RemoveAll(IsOneTimeSpecial);
                        }
                    }

                    // Reorder in the following order: VIP, AP Monthly, AP Annual, UP
                    // Sku was the only existing item I found that would accomplish this order thus
                    // why the list is being ordered on the sku
                    dlSpecials.Sort (delegate (Promotion p1, Promotion p2) { return p1.Sku.CompareTo (p2.Sku); });
                }
                else
                {
                    //divPackages.Visible = true;
                    //divSpecials.Visible = false;
                }

                rptSpecials.DataSource = dlSpecials;
                rptSpecials.DataBind();
			}

			// Set Nav and style sheets
            ((IndexPageTemplate)Master).HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
            ((IndexPageTemplate)Master).CustomCSS = _styleSheets;
            ((IndexPageTemplate)Master).CustomJavaScript = _javascript;

			//setup wizard nav bar
			//ucWizardNavBar.ActiveStep = CheckoutWizardNavBar.STEP.STEP_1;
		}

		#region Helper Methods
		
		private void VerifyUserAuthenticated()
		{
			// They must be logged in
			if (!Request.IsAuthenticated)
			{
                Response.Redirect (GetLoginURL ());
			}
		}

        //delegate function used in LIst FindAll search
        private bool IsOneTimeSpecial(Promotion promo)
        {
            return promo.PromotionId == oneTimeSpecialId;
        }

        private void VerifyMaturePassEligibility()
        {
            // User must be 18 or older
            if (KanevaWebGlobals.CurrentUser.IsAdult)
            {
                //if they meet this criteria they can see access passes
                showMaturePasses = true;
            }

        }

		protected string FormatCredits(UInt64 credits)
		{
			try
			{
				return credits.ToString("N0");
			}
			catch
			{
				return "";
			}
		}

		#endregion

        #region Functions

        private void PurchaseOffer(string PaymentMethod)
        {
            if (!Request.IsAuthenticated)
            {
                Response.Redirect(GetLoginURL());
                return;
            }

            int specialsId = 0;

            //must have selected a package to continue
            if ((SelectSpecialId != null) && (SelectSpecialId != ""))
            {
                if (KanevaGlobals.IsNumeric(SelectSpecialId))
                {
                    specialsId = Convert.ToInt32(SelectSpecialId);
                    int userId = GetUserId();

                    DataRow drOrder = null;
                    bool newOrder = true;


                    if (Request["orderId"] != null && Request["orderId"] != string.Empty)
                    {
                        try
                        {
                            drOrder = StoreUtility.GetOrder(Convert.ToInt32(Request["orderId"]), userId);
                        }
                        catch { }
                    }

                    int orderId = 0;
                    if (drOrder != null && drOrder["order_id"].ToString() != string.Empty &&
                        Convert.ToInt32(drOrder["transaction_status_id"]) == (int)Constants.eORDER_STATUS.CHECKOUT &&
                        Convert.ToInt32(drOrder["user_id"]) == GetUserId())
                    {
                        orderId = Convert.ToInt32(drOrder["order_id"]);
                        newOrder = false;
                    }
                    else
                    {
                        orderId = StoreUtility.CreateOrder(userId, Common.GetVisitorIPAddress(), (int)Constants.eORDER_STATUS.CHECKOUT);
                    }

                    DataRow drPromotionalInfo = StoreUtility.GetPromotion(specialsId);

                    // Point Bucket item name
                    string itemDescription = Server.HtmlEncode(drPromotionalInfo["display"].ToString());

                    if (drPromotionalInfo["promotion_description"] != null && drPromotionalInfo["promotion_description"].ToString() != string.Empty)
                    {
                        itemDescription = Server.HtmlEncode(drPromotionalInfo["promotion_description"].ToString());
                    }

                    int transactionId = 0;

                    if (newOrder)
                    {
                        // Record the transaction in the database, marked as checkout
                        transactionId = StoreUtility.PurchasePoints(userId, (int)Constants.eTRANSACTION_STATUS.CHECKOUT, itemDescription, 0,
                            (int)Constants.ePAYMENT_METHODS.KANEVA_CYBERSOURCE, Convert.ToDouble(drPromotionalInfo["dollar_amount"]),
                            Convert.ToDouble(drPromotionalInfo["kei_point_amount"]), Convert.ToDouble(drPromotionalInfo["free_points_awarded_amount"]),
                            0, Common.GetVisitorIPAddress());

                        // Record the point bucket purchased
                        StoreUtility.UpdatePointTransactionPointBucket(transactionId, Convert.ToInt32(drPromotionalInfo["promotion_id"]));
                    }
                    else
                    {
                        transactionId = Convert.ToInt32(drOrder["point_transaction_id"]);

                        // updated existing order
                        StoreUtility.UpdatePointTransactionPointBucket(transactionId, Convert.ToInt32(drPromotionalInfo["promotion_id"]), itemDescription, Convert.ToDouble(drPromotionalInfo["dollar_amount"]), Convert.ToDouble(drPromotionalInfo["kei_point_amount"]), Convert.ToDouble(drPromotionalInfo["free_points_awarded_amount"]));
                    }

                    // Set the order id on the point purchase transaction
                    StoreUtility.UpdatePointTransaction(transactionId, orderId);
                    StoreUtility.UpdateOrderPointTranasactionId(orderId, transactionId);

                    //handle according to payment method
                    if (PaymentMethod == "credit")
                    {
                        // Send them to select a payment method
                        StoreUtility.UpdatePointTransaction (transactionId, Constants.ePAYMENT_METHODS.KANEVA_CYBERSOURCE);

                        // Set the purchase type
                        StoreUtility.SetPurchaseType(orderId, Constants.ePURCHASE_TYPE.KPOINT_ONLY);

                        Response.Redirect(ResolveUrl("~/checkout/billinginfo.aspx?orderId=" + orderId.ToString())); ;
                    }
                    else if (PaymentMethod == "paypal")
                    {
                        StoreUtility.UpdatePointTransaction(transactionId, Constants.ePAYMENT_METHODS.PAYPAL);

                        // Set the purchase type
                        StoreUtility.SetPurchaseType(orderId, Constants.ePURCHASE_TYPE.KPOINT_ONLY);

                        Response.Redirect(ResolveUrl("~/checkout/orderConfirm.aspx?orderId=" + orderId.ToString()));
                    }
                    else if (PaymentMethod == "trial")
                    {
                        StoreUtility.UpdatePointTransaction(transactionId, Constants.ePAYMENT_METHODS.KANEVA_CYBERSOURCE);

                        // Set the purchase type
                        StoreUtility.SetPurchaseType(orderId, Constants.ePURCHASE_TYPE.SUBSCRIPTION);

                        // Send them to select a payment method
                        Response.Redirect(ResolveUrl("~/checkout/billinginfo.aspx?orderId=" + orderId.ToString() + "&pass=" + (Request.QueryString["pass"] != null ? Request.QueryString["pass"].ToString() : "")), false);
                    }

                }
                else
                {
                    spnMessage.InnerText = "Unable to process package.";
                    ScriptManager.RegisterStartupScript (this, GetType (), "ShowConfirm", "ShowConfirmMsg('" + spnMessage.ClientID + "', 5000);", true);
                }
            }
            else
            {
                spnMessage.InnerText = "Please select a credit amount.";
                ScriptManager.RegisterStartupScript (this, GetType (), "ShowConfirm", "ShowConfirmMsg('" + spnMessage.ClientID + "', 5000);", true);
            }
        }

        private string GetTrialPeriod(string promotionID)
        {
            string trialPeriod = "0";

            try
            {
                //get the associate subscription
                Subscription subscrpt = (new SubscriptionFacade()).GetSubscriptionByPromotionId(Convert.ToUInt32(promotionID));
                trialPeriod = subscrpt.DaysFree.ToString();
            }
            catch (Exception)
            {
            }

            return trialPeriod;
        }

        private bool DoesUserAlreadyHaveSubscriptionWithThisPass(int promotionID, string PaymentMethod)
        {
            bool userHasPassAlready = false;
            string promotionIDS = "";

            //if the warning dialog is already visible do not process again
            if (!divConfirmDuplicatePass.Visible)
            {
                //get the other promotions that have the same passes that this one does
                List<Promotion> promotions = new PromotionsFacade().GetOtherPromotionsWithSamePassGroups((uint)promotionID);

                if (promotions.Count > 0)
                {
                    //turn into a string listing
                    foreach (Promotion promo in promotions)
                    {
                        promotionIDS += promo.PromotionId + ",";
                    }

                    //return the result
                    promotionIDS = promotionIDS.Substring(0, promotionIDS.Length - 1);

                    //get all user subscriptions that match this list
                    List<UserSubscription> userSubscriptionsWithPass = new SubscriptionFacade().GetUserSubscriptionsByPromotionIds(promotionIDS, (uint)GetUserId());

                    //set return value
                    userHasPassAlready = (userSubscriptionsWithPass.Count > 0 ? true : false);

                    //display the warning dialog if the user has any overlapping subscriptions
                    if (userHasPassAlready)
                    {
                        //generate the list of subscriptions the user has with this pass
                        string subscrptionsWPass = "";
                        foreach (UserSubscription usersub in userSubscriptionsWithPass)
                        {
                            //get the subscriptionName
                            Subscription subscription = WebCache.GetSubscription(usersub.SubscriptionId);
                            subscrptionsWPass += subscription.Name + "<br />";
                        }

                        //set the disply message
                        confirmMessage.Text = "<center><h1>Warning</h1></center> <br /> <p>You already have the following active subscription" + (userSubscriptionsWithPass.Count > 1 ? "s:" : ":") +
                            "</p>" + subscrptionsWPass + " <p> that contain" + (userSubscriptionsWithPass.Count > 1 ? "" : "s") + " the pass(es) that this subscription provides.</p>";

                        //show the confirm dialog
                        divConfirmDuplicatePass.Visible = true;

                        //set the command arguement
                        btn_ConfirmDuplicatePurchase.CommandArgument = promotionID.ToString();
                        btn_ConfirmDuplicatePurchase.CommandName = PaymentMethod;
                    }
                }
            }

            return userHasPassAlready;
        }

        private void ProcessTrial(int promotionID, string PaymentMethod)
        {
            // Added this check here to accomodate the VIP page that we don't require users to be authenticated
            // to view.  Check here so we don't make these db calls and then make the user login and call
            // all this code again.
            if (!Request.IsAuthenticated)
            {
                Response.Redirect (GetLoginURL ());
                return;
            }

            //does intial check to see if the user has purchased this promotion before
            if ((new SubscriptionFacade()).DoesUserCurrentlyHaveThisPromotion((uint)promotionID, (uint)GetUserId()))
            {
                //extra logic to make sure that even if they dont have this promotion already that they haven't purchased 
                //other promotions with the same underlying pass groups under it


                spnMessage.Visible = true;
                spnMessage.InnerText = "You already own the item you selected.";
            }
            else
            {
                //check to see if this promotion is expired before proceeding
                bool promotionIsExpired = false;
                try
                {
                    promotionIsExpired = ((new PromotionsFacade()).GetPromotionsByPromoId(promotionID)).PromotionEnd < DateTime.Now;
                }
                catch (Exception) { }

                if (promotionIsExpired)
                {
                    spnMessage.Visible = true;
                    spnMessage.InnerText = "This item is no longer available.";
                }
                else
                {
                    //check to see if the user has a subscription already that has this pass
                    if (!DoesUserAlreadyHaveSubscriptionWithThisPass(promotionID, PaymentMethod))
                    {
                        //if not get which payment method was selected and process
                        PurchaseOffer(PaymentMethod);
                    }
                }
            }
        }

        #endregion

        #region Event Handlers

        private void rptSpecials_ItemCreated(object sender, RepeaterItemEventArgs e)
        {
            bool userDisplayOptions = true;
            try
            {
                string adContainerStyle = "";

                //get the div the dynamic ad info is in
                HtmlGenericControl adContainer = (HtmlGenericControl)e.Item.FindControl("divAdContainer");
                HtmlGenericControl adOuterContainer = (HtmlGenericControl)e.Item.FindControl("divOuterAdContainer");
                HtmlGenericControl adRotator = (HtmlGenericControl)e.Item.FindControl("divAdRotator");

                //set any background images for the special
                string backgroundImage = DataBinder.Eval(e.Item.DataItem, "SpecialBackgroundImage").ToString();

                if (backgroundImage != "")
                {
                    adContainerStyle = "background:transparent url('" + backgroundImage + "') no-repeat top left ";
                }

                //configures pricing section based on if the page is for passes or specials
                if (showPassConfig)
                {
                    //get the div the dynamic ad info is in
                    HtmlGenericControl pricingContainer = (HtmlGenericControl)e.Item.FindControl("passPricing");
                    pricingContainer.Visible = true;
                    if (Request["previewDate"] != null && Request["previewDate"] != string.Empty)
                    {
                        ((LinkButton)e.Item.FindControl("lbFreeTrial")).Enabled = false;
                    }
                }
                else
                {
                    //get the div the dynamic ad info is in
                    HtmlGenericControl pricingContainer = (HtmlGenericControl)e.Item.FindControl("specialPricing");
                    pricingContainer.Visible = true;
                    if (Request["previewDate"] != null && Request["previewDate"] != string.Empty)
                    {
                        ((LinkButton)e.Item.FindControl("lb_PurchaseSpecial")).Enabled = false;
                        ((LinkButton)e.Item.FindControl("lb_PurchaseSpecialPP")).Enabled = false;
                    }
                }

                ////set any special sticker images for the special (mini image div inside the main image)
                //HtmlContainerControl divSticker = (HtmlContainerControl)e.Item.FindControl("sticker");
                //string specialSticker = DataBinder.Eval(e.Item.DataItem, "SpecialStickerImage").ToString();
                //if (specialSticker != "")
                //{
                //    divstyle = "background:transparent url('" + specialSticker + "') no-repeat top left ";
                //}

                //check to see if the user has opted to use the custom display options for font color, text, etc
                // if not skip that formatting
                userDisplayOptions = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "UseDisplayOptions")) == 1 ? true : false;

                if (userDisplayOptions)
                {
                    string backgroundColor = DataBinder.Eval(e.Item.DataItem, "SpecialBackgroundColor").ToString();
                    string fontcolor = DataBinder.Eval(e.Item.DataItem, "SpecialFontColor").ToString();

                    //get all the affected controls
                    HtmlGenericControl item1 = (HtmlGenericControl)e.Item.FindControl("specialHeading");
                    HtmlGenericControl item2 = (HtmlGenericControl)e.Item.FindControl("specialSub1");
                    HtmlGenericControl item3 = (HtmlGenericControl)e.Item.FindControl("specialSub2");
                    HtmlGenericControl item4 = (HtmlGenericControl)e.Item.FindControl("specialSub3");
                    HtmlGenericControl item5 = (HtmlGenericControl)e.Item.FindControl("parHeading");

                    //make the controls visible
                    item1.Visible = true;
                    item2.Visible = true;
                    item3.Visible = true;
                    item4.Visible = true;
                    item5.Visible = true;

                    if (backgroundColor != "")
                    {
                        if (adContainerStyle != "")
                        {
                            adContainerStyle += ";background-color:" + backgroundColor;
                        }
                        else
                        {
                            adContainerStyle = "background-color:" + backgroundColor;
                        }
                    }

                    if (fontcolor != "")
                    {
                        CurrentFontColor = fontcolor;

                        item1.Attributes.Add("style", "color:" + fontcolor);
                        item2.Attributes.Add("style", "color:" + fontcolor);
                        item3.Attributes.Add("style", "color:" + fontcolor);
                        item4.Attributes.Add("style", "color:" + fontcolor);
                        item5.Attributes.Add("style", "color:" + fontcolor);

                    }
                }

                //adContainer.Attributes.Add("style", adContainerStyle);
                adOuterContainer.Attributes.Add("style", adContainerStyle);
                adRotator.Attributes.Add("style", adContainerStyle);
                //divSticker.Attributes.Add("style", divstyle);

            }
            catch (Exception)
            { }

        }

        private void rptSpecials_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                bool userDisplayOptions = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "UseDisplayOptions")) == 1 ? true : false;

                //calculate savings
                decimal originalPrice = Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "OriginalPrice"));
                decimal actualPrice = Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "DollarAmount"));
                decimal discount = 0.0m;

                //handle case where original price was forgotten
                originalPrice = (originalPrice <= 0.0m) ? (actualPrice) : originalPrice;

                if (originalPrice < actualPrice)
                {
                    discount = Math.Round((1 - (originalPrice / actualPrice)) * -100, 0);
                }
                else
                {
                    discount = Math.Round((1 - (actualPrice / originalPrice)) * 100, 0);
                }

                //set the value in the control
                Label lbl_savings = (Label)e.Item.FindControl("lblSavings");
                lbl_savings.Text = discount + "%";

                if (userDisplayOptions)
                {
                    dlSpecialItems = (DataList)e.Item.FindControl("dlSpecialItems");
                    string promoId = ((HtmlInputHidden)e.Item.FindControl("ihd_promotionId")).Value;

                    string genderList = "'" + KanevaWebGlobals.CurrentUser.Gender + "','" + Constants.GENDER_BOTH + "'";
                    DataTable dtPromotionItems = GetPromotionsFacade.GetPromotionalOfferItemsByGender(Convert.ToInt32(promoId), genderList);
                    dlSpecialItems.DataSource = dtPromotionItems;
                    dlSpecialItems.DataBind();

                    //hide the text if no items are given
                    if (dtPromotionItems.Rows.Count <= 0)
                    {
                        ((HtmlGenericControl)e.Item.FindControl("parHeading")).Visible = false;
                    }
                }
                if (showPassConfig)
                {
                    //get the subscription 
                    string promoId = ((HtmlInputHidden)e.Item.FindControl("ihd_promotionId")).Value;
                    Label lblDaysFree = (Label)e.Item.FindControl("lbl_DaysFree");

                    //get the number of days of free trial
                    string daysFree = GetTrialPeriod(promoId);

                    //set number of days free 
                    lblDaysFree.Text = daysFree;

                    //if there is no free trial period change display
                    if(daysFree == "0")
                    {
                        ((LinkButton)e.Item.FindControl("lbFreeTrial")).Text = "BUY NOW!";
                        lblDaysFree.Visible = false;
                    }
                    
                }

                //hide credit fields if there are no credits given
                if ((Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "KeiPointAmount")) <= 0) || (originalPrice <= actualPrice))
                {
                    ((HtmlTableCell)e.Item.FindControl("thspecialSavings")).Visible = false;
                }

                //get the div the dynamic ad info is in
                HtmlGenericControl adContainer = (HtmlGenericControl)e.Item.FindControl("divAdContainer");
                HtmlGenericControl adRotator = (HtmlGenericControl)e.Item.FindControl("divAdRotator");

                //set any background images for the special
                string adRotatorPath = DataBinder.Eval(e.Item.DataItem, "AdRotatorPath").ToString();

                // Temp code to not display .swf files so we don't have to remove them from the db
                if (adRotatorPath != "" && 1 == 2)
                {
                    bool autoPlay = true;
                    string dimensions = " height=\"201\" width=\"586\" ";
                    //string height = "201";
                    //string width = "586";

                    //register the function for each adrotator
                    adRotator.InnerHtml = "<div style=\"top:-8px;left:2px;position:relative;width:100%;height:100%;\"><OBJECT classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000' " +
                    " codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0' " +
                    " id=\"swfStream\" " + dimensions + ">" +
                    " <param name='movie' value=\"" + adRotatorPath + "\">" +
                    " <param name='quality' value=\"high\">" +
                    " <param name='loop' value=\"true\">" +
                    " <param name='wmode' value=\"transparent\">" +
                    " <PARAM NAME='AutoStart' VALUE=\"" + autoPlay + "\">" +
                    " <EMBED src=\"" + adRotatorPath + "\" quality='high' " + dimensions +
                    " loop=\"true\" autostart=\"" + autoPlay + "\" type='application/x-shockwave-flash'" +
                    " pluginspage='http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash' wmode=\"transparent\">" +
                    " </EMBED> " +
                    "</OBJECT></div>";

                    adRotator.Visible = true;
                    adContainer.Visible = false;
                }
                else
                {
                    adRotator.Visible = false;
                    adContainer.Visible = true;
                }
                

            }
            catch (Exception)
            { }

        }

        public void dlSpecialItems_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            HtmlTableCell item1 = (HtmlTableCell)e.Item.FindControl("itemquantity");
            HtmlTableCell item2 = (HtmlTableCell)e.Item.FindControl("itemname");
            item1.Attributes.Add("style", "width: 20px; color:" + CurrentFontColor);
            item2.Attributes.Add("style", "color:" + CurrentFontColor);
        }

        /// <summary>
        /// Button selects all the checked promotions and processes them
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnPurchase_Click(object sender, EventArgs e)
        {
            //currently not in use but the idea is to create a coma delimited list 
            //and store in the SelectSpecialId. Then the PurchaseSpecials can be modified to 
            //parse and handle the multiple purchases
        }

        /// <summary>
        /// Button purchase the item whos button was clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Purchase_Command(Object sender, CommandEventArgs e)
        {
            //get the promotional id from the button of the special clicked
            SelectSpecialId = e.CommandArgument.ToString();

            //select the appropriate action
            switch(e.CommandName)
            {
                case "trial":
                    ProcessTrial(Convert.ToInt32(SelectSpecialId),e.CommandName);
                    break;
                case "credit":
                case "paypal":
                    //get which payment method was selected and process
                    PurchaseOffer(e.CommandName);
                    break;
                case "learn":
                    Response.Redirect(ResolveUrl("passDetails.aspx?pass=true&passId=" + SelectSpecialId));
                    break;
                case "buyDupPass":
                    //get which payment method was selected and process
                    ProcessTrial(Convert.ToInt32(SelectSpecialId), e.CommandName);
                    break;
                case "cancelDupPass":
                    divConfirmDuplicatePass.Visible = false;
                    break;
            }

        }

		#endregion

        #region Properties

        private string SelectSpecialId
        {
            get
            {
                if (ViewState["_SelectSpecialId"] == null)
                {
                    ViewState["_SelectSpecialId"] = "";
                }
                return ViewState["_SelectSpecialId"].ToString();
            }
            set
            {
                ViewState["_SelectSpecialId"] = value;
            }
        }

        private int MoveInSpecialID
        {
            get
            {
                if (ViewState["_moveinID"] == null)
                {
                    ViewState["_moveinID"] = 0;
                }
                return (int) ViewState["_moveinID"];
            }
            set
            {
                ViewState["_moveinID"] = value;
            }
        }

        public string CurrentFontColor
        {
            get
            {
                if (ViewState["_currentColor"] == null)
                {
                    ViewState["_currentColor"] = "#000000";
                }
                return ViewState["_currentColor"].ToString ();
            }
            set
            {
                ViewState["_currentColor"] = value;
            }
        }

        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
            this.rptSpecials.ItemCreated += new System.Web.UI.WebControls.RepeaterItemEventHandler(this.rptSpecials_ItemCreated);
            this.rptSpecials.ItemDataBound += new System.Web.UI.WebControls.RepeaterItemEventHandler(this.rptSpecials_ItemDataBound);
		}
		#endregion
	}
}
