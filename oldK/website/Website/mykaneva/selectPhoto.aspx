<%@ Page language="c#" Codebehind="selectPhoto.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.selectPhoto" %>

<link href="../css/kaneva.css" rel="stylesheet" type="text/css" />
<link href="../css/friends.css" rel="stylesheet" type="text/css" />
<LINK href="../css/PageLayout/popup.css" type="text/css" rel="stylesheet">

<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td colspan="2" style="padding-top:5px;padding-bottom:10px;padding-left:10px;padding-right:5px;" class="channelStyle">
			Select a background photo
		</td>
	</tr>
	<tr>
		<td valign="top" class="popupBody" style="padding-left:10px" width="20%">
			<b>Photo Set: </b>
		</td>
		<td nowrap style="padding-left:10px;padding-right:5px;" class="popupBody">
			<asp:DropdownList ID="ddlGroup" runat="server" style="width:200px" class="Filter2" autopostback="true" />
		</td>
	</tr>
</table>
				
<asp:DataList Visible="true" runat="server" ShowFooter="False" Width="100%" id="dlPictures" cellpadding="2" cellspacing="2" border="1" RepeatColumns="2" RepeatDirection="Horizontal" >
	<ItemStyle CssClass="lineItemThumb" HorizontalAlign="Center"/>
	<ItemTemplate>
		<table border="0" cellspacing="0" cellpadding="0" >
			<tr>
				<td valign="middle">
					<input id="rbPicture" name="rbPicture" type="radio" value='<%# DataBinder.Eval( Container.DataItem, "asset_id") %>' onclick='document.frmMain.ihSinglePictureId.value=this.value' />
				</td>
				
				<td width="90%" align="left" valign="top">
					<div style="width: 60px;">
						<img runat="server" src='<%#GetMediaImageURL (DataBinder.Eval(Container.DataItem, "thumbnail_medium_path").ToString () ,"me", Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_id")), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_type_id")))%>' border="0" style="border: 1px solid white;" />
					</div>
				</td>	
			</tr>
			<tr valign="top">	
				<td>&nbsp;</td>
				<td align="left" valign="top">
					<span class="insideTextNoBold" id="spnImageTitle" runat="server"></span>
				</td>
		</table>
	</ItemTemplate>
	
</asp:DataList>	

<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td style="text-align: right; padding-right: 10px; padding-top: 20px;">
			<input type="button" onclick="javascript: window.close();" Value="   Cancel   " class="filter2"/>&nbsp;&nbsp;<asp:Button ID="btnSave" Runat="server" CssClass="filter2" Text=" Save Changes "/>
		</td>
	</tr>
</table>
<br/>

<input type="hidden" name="ihSinglePictureId" id="ihSinglePictureId" runat="server" />
