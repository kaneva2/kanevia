///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using log4net;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.mykaneva
{
    public partial class MessageCenter : BasePage // StoreBasePage
    {
        private string CurrentSort = "";
        private string CurrentSortOrder = "DESC";

        protected void Page_Load (object sender, EventArgs e)
        {
            // They must be logged in
            if (!Request.IsAuthenticated)
            {
                Response.Redirect (GetLoginURL ());
                return;
            }

            GetAllCounts ();

            if (!IsPostBack)
            {
                ConfigAllNavButtons ();

                LoadInitialPage ();
            }

            // Go 3D Link
            ConfigureUserMeetMe3D(btnGo3D, KanevaWebGlobals.CurrentUser.Username, "");
        }

        // Messages
        #region **** Messages ****

        #region -- Methods [Messages] --
        private void BindMessages (int pageNumber)
		{
			BindMessages (pageNumber, "");
		}
		private void BindMessages (int pageNumber, string filter)
		{
            CurrentSort = "message_date";
            string orderby = CurrentSort + " " + CurrentSortOrder;
            SetPageTitle ("My Messages");
            HideAllPanels ();

			if (filter.Length > 0)
			{
				filter += " AND ";
			}
			filter += " m.type IN (" + (int) Constants.eMESSAGE_TYPE.ALERT + ", " + (int) Constants.eMESSAGE_TYPE.PRIVATE_MESSAGE + ")";

            PagedList<UserMessage> msgs = GetUserFacade.GetUserMessages (GetUserId (), filter, orderby, pageNumber, PageSize);

            rptMessages.DataSource = msgs;
            rptMessages.DataBind ();

            if (msgs.TotalCount > 0)
            {
                rptMessages.DataSource = msgs;
                rptMessages.DataBind ();

                // Set up the pager
                pagerMessages.NumberOfPages = Math.Ceiling ((double) msgs.TotalCount / PageSize).ToString ();
                pagerMessages.CurrentPageNumber = pageNumber;
                pagerMessages.DrawControl ();

                // The results
                lblMessagesResults.Text = GetResultsText (msgs.TotalCount, pageNumber, PageSize, msgs.Count);
                upMessages.Visible = true;
                divMessagesButtonRow.Visible = true;
            }
            else
            {
                upMessages.Visible = false;
                string msg = "There are currently no messages in your Inbox.";
                string callout = "";
                ShowNoResults (msg, callout);
            }

            // Set nav for Message
            SetNav (RequestType.MESSAGES);
        }
        private void DeleteMessages ()
        {
            HtmlInputHidden hidMessageId;
            CheckBox chkEdit;
            int ret = 0;
            int count = 0;
     
            // Remove the messages
            foreach (RepeaterItem item in rptMessages.Items)
            {
                chkEdit = (CheckBox) item.FindControl ("chkMsgEdit");

                if (chkEdit.Checked)
                {
                    hidMessageId = (HtmlInputHidden) item.FindControl ("hidMessageId");

                    if (!UsersUtility.IsMessageRecipient (Convert.ToInt32 (hidMessageId.Value), GetUserId ()))
                    {
                        m_logger.Warn ("User " + GetUserId () + " from IP " + Common.GetVisitorIPAddress() + " tried to delete message " + hidMessageId.Value + " but is not the message owner");
                        return;
                    }

                    ret = UsersUtility.DeleteMessage (GetUserId(), Convert.ToInt32 (hidMessageId.Value));

                    if (ret == 1)
                        count++;
                }
            }

            if (count > 0)
            {
                BindMessages (1);
                // Update count in nav
                GetMessageCount ();
            }

            ShowAlertMsg (count.ToString () + " message" + (count != 1 ? "s were" : " was") + " deleted.");
        }
        #endregion  -- Methods [Messages] --

        #region  -- Event Handlers [Messages] --
        protected void rptMessages_ItemCommand (object source, RepeaterCommandEventArgs e)
        {
            string command = e.CommandName;
            HideAlertMsg ();

            if (command.Equals ("Details"))
            {
                try
                {
                    HideAllPanels ();
                    ShowMessageDetails (Convert.ToInt32 (((HtmlInputHidden) e.Item.FindControl ("hidMessageId")).Value), false);
                }
                catch { }
            }
        }
        protected void rptMessages_ItemDataBound (object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                try
                {
                    if (DataBinder.Eval (e.Item.DataItem, "ToViewable").ToString().Equals("U"))
                    {
                        LinkButton btnMsgDetails = (LinkButton) e.Item.FindControl ("btnMsgDetails");
                        ((HtmlContainerControl) btnMsgDetails.Parent).Attributes.Add ("class", "row unread");
                    }
                }
                catch {
                }
            }
        }
        protected void btnDeleteMessage_Click (object sender, EventArgs e)
        {
            bool isItemChecked = false;
            HideAlertMsg ();

            foreach (RepeaterItem item in rptMessages.Items)
            {
                CheckBox chkEdit = (CheckBox) item.FindControl ("chkMsgEdit");

                if (chkEdit.Checked)
                {
                    ShowModal (RequestType.MESSAGES, mpeDeleteMessage);
                    return;
                }
            }

            if (!isItemChecked)
            {
                ShowAlertMsg ("No message were selected.");
                SetNav (RequestType.MESSAGES);
            }
        }
        protected void btnMessageMarkRead_Click (object sender, EventArgs e)
        {
            int count = 0;
            bool isItemChecked = false;
            
            foreach (RepeaterItem item in rptMessages.Items)
            {
                if (((CheckBox) item.FindControl ("chkMsgEdit")).Checked)
                {
                    isItemChecked = true;
                    
                    // Check to make sure item in unread
                    if ((((HtmlInputHidden) item.FindControl ("hidToViewable")).Value).Equals ("U"))
                    {
                        UsersUtility.ReadMessage (GetUserId (), Convert.ToInt32 (((HtmlInputHidden) item.FindControl ("hidMessageId")).Value));
                        count++;
                    }
                }
            }

            if (isItemChecked)
            {
                if (count > 0)
                {
                    BindMessages (1);
                    ShowAlertMsg ("Successfully marked " + count + " message" + (count != 1 ? "s" : "") + " as read.");
                    GetMessageCount ();
                }
                else
                {
                    ShowAlertMsg ("No messages were marked as read.");
                }
            }
            else
            {
                ShowAlertMsg ("No messages were selected.");
            }

        }
        private void pagerMessages_PageChange (object sender, PageChangeEventArgs e)
        {
            BindMessages (e.PageNumber);
        }
        #endregion   -- Event Handlers [Messages] --

        #endregion **** Messages ****

        // Messages Sent
        #region **** Messages Sent ****

        #region -- Methods [Messages Sent] --
        private void BindMessagesSent (int pageNumber)
        {
            BindMessagesSent (pageNumber, "");
        }
        private void BindMessagesSent (int pageNumber, string filter)
        {
            CurrentSort = "message_date";
            CurrentSortOrder = "DESC";
            string orderby = CurrentSort + " " + CurrentSortOrder;
            SetPageTitle ("Sent Messages");
            HideAlertMsg ();

            PagedList<UserMessage> msgs = GetUserFacade.GetSentMessages (GetUserId (), "", orderby, pageNumber, PageSize);

            rptMessagesSent.DataSource = msgs; // pds;
            rptMessagesSent.DataBind ();

            if (msgs.TotalCount > 0)
            {
                rptMessagesSent.DataSource = msgs;
                rptMessagesSent.DataBind ();

                // Set up the pager
                pagerMessagesSent.NumberOfPages = Math.Ceiling ((double) msgs.TotalCount / PageSize).ToString ();
                pagerMessagesSent.CurrentPageNumber = pageNumber;
                pagerMessagesSent.DrawControl ();

                // The results
                lblMessagesSentResults.Text = GetResultsText (msgs.TotalCount, pageNumber, PageSize, msgs.Count);
                upMessagesSent.Visible = true;
                divMessagesSentButtonRow.Visible = true;
            }
            else
            {
                upMessagesSent.Visible = false;
                string msg = "You currently have no sent messages.";
                string callout = "";
                ShowNoResults (msg, callout);
            }

            // Set nav for Message
            SetNav (RequestType.MESSAGES_SENT);
        }
        private void DeleteMessagesSent ()
        {
            HtmlInputHidden hidMessageId;
            CheckBox chkEdit;
            int ret = 0;
            int count = 0;
            int userId = GetUserId ();

            // Remove the messages
            foreach (RepeaterItem item in rptMessagesSent.Items)
            {
                chkEdit = (CheckBox) item.FindControl ("chkMSEdit");

                if (chkEdit.Checked)
                {
                    hidMessageId = (HtmlInputHidden) item.FindControl ("hidMessageSentId");

                    if (!UsersUtility.IsMessageRecipient (Convert.ToInt32 (hidMessageId.Value), GetUserId ()))
                    {
                        m_logger.Warn ("User " + GetUserId () + " from IP " + Common.GetVisitorIPAddress() + " tried to delete message " + hidMessageId.Value + " but is not the message owner");
                        return;
                    }

                    ret = UsersUtility.DeleteSentMessage (GetUserId(), Convert.ToInt32 (hidMessageId.Value));

                    if (ret == 1)
                        count++;
                }
            }

            if (count > 0)
            {
                BindMessagesSent (1);
            }

            ShowAlertMsg (count.ToString () + " sent message" + (count != 1 ? "s were" : " was") + " deleted.");
        }
        #endregion  -- Methods [Messages Sent] --

        #region  -- Event Handlers [Messages Sent] --
        protected void rptMessagesSent_ItemCommand (object source, RepeaterCommandEventArgs e)
        {
            string command = e.CommandName;
            HideAlertMsg ();

            if (command.Equals ("Details"))
            {
                try
                {
                    HideAllPanels ();
                    ShowMessageDetails (Convert.ToInt32 (((HtmlInputHidden) e.Item.FindControl ("hidMessageSentId")).Value), true);
                }
                catch { }
            }
        }
        protected void btnDeleteMessageSent_Click (object sender, EventArgs e)
        {
            bool isItemChecked = false;
            HideAlertMsg ();

            foreach (RepeaterItem item in rptMessagesSent.Items)
            {
                CheckBox chkEdit = (CheckBox) item.FindControl ("chkMSEdit");

                if (chkEdit.Checked)
                {
                    ShowModal (RequestType.MESSAGES_SENT, mpeDeleteMessageSent);
                    return;
                }
            }

            if (!isItemChecked)
            {
                ShowAlertMsg ("No message were selected.");
                SetNav (RequestType.MESSAGES_SENT);
            }
        }
        private void pagerMessagesSent_PageChange (object sender, PageChangeEventArgs e)
        {
            BindMessagesSent (e.PageNumber);
        }
        #endregion   -- Event Handlers [Messages Sent] --

        #endregion **** Messages Sent ****

        // Message Details
        #region **** Message Details ****

        #region -- Methods [Message Details] --
        private void ShowMessageDetails (int messageId, bool isSentMsg)
        {
            try
            {
                HideAllPanels ();

                UserMessage umsg = GetUserFacade.GetUserMessage (GetUserId (), messageId);

                if (umsg.MessageId.Equals (0))
                {
                    // May be trying to access another users message
                    m_logger.Warn ("UserId '" + GetUserId () + "' tried to access message '" + messageId + "' and nothing returned.");
                    //               divNoAccess.Visible = true;
                    //               divMsgDetails.Visible = false;
                    return;
                }

                btnFrom.Text = umsg.UserName;
                btnFrom.Attributes.Add ("href", GetPersonalChannelUrl (umsg.CommunityNameNoSpaces));

                aFromThumb.HRef = GetPersonalChannelUrl (umsg.CommunityNameNoSpaces);
                imgThumb.Src = GetProfileImageURL (umsg.ThumbnailSquarePath, "sq", "M", umsg.OwnerFacebookSettings.UseFacebookProfilePicture, umsg.OwnerFacebookSettings.FacebookUserId);

                lblDate.Text = FormatDateTime (umsg.MessageDate);
                lblSubject.Text = Server.HtmlDecode (umsg.Subject);
                divBody.InnerHtml = umsg.MessageText;
                hidDetailMessageId.Value = umsg.MessageId.ToString();

                upMessageDetails.Visible = true;
                
                // Hide the Back to Blasts button 
                divBackButton.Visible = false;

                // Hide spam and block buttons if the current user is the sender or
                // if the sender id = 1 which is the Kaneva admin user
                if (umsg.FromId == 1 || umsg.FromId.Equals (GetUserId ()))
                {
                    divSpanBlockTop.Visible = false;
                    divSpanBlockBtm.Visible = false;
                }
                else
                {
                    divSpanBlockTop.Visible = true;
                    divSpanBlockBtm.Visible = true;
                }
                
                try
                {   // If user sent msg, then don't mark it read
                    if (isSentMsg)
                    {
                        // Set the Back to Messages button values
                        btnMessageDetailBackTop.CommandArgument = RequestType.MESSAGES_SENT.ToString ();
                        btnMessageDetailBackBtm.CommandArgument = RequestType.MESSAGES_SENT.ToString ();
                        hidDetailMessageType.Value = RequestType.MESSAGES_SENT.ToString ();
                    }
                    else
                    {
                        // Set the Back to Messages button values
                        btnMessageDetailBackTop.CommandArgument = RequestType.MESSAGES.ToString ();
                        btnMessageDetailBackBtm.CommandArgument = RequestType.MESSAGES.ToString ();
                        hidDetailMessageType.Value = RequestType.MESSAGES.ToString ();

                        // Mark message read
                        UsersUtility.ReadMessage (GetUserId (), messageId);
                    }
                }
                catch { }
            }
            catch (Exception ex)
            {
                m_logger.Error (ex);
            }
            
            // Set nav for Message
            SetNav (RequestType.MESSAGE_EDITOR);
            GetMessageCount ();
        }
        private void ConfigMessageDetailsAsInitial()
        {
            // Get the message id, either from hidden input or url string
            hidDetailMessageId.Value = (Request["msgid"] == null ? "" : Request["msgid"].ToString());

            try
            {
                ShowMessageDetails(Int32.Parse(hidDetailMessageId.Value), false);

                if (!upMessageDetails.Visible)
                {
                    BindMessages(1);
                }
            }
            catch 
            {
                BindMessages(1);
            }
        }
        private bool BlockUser (int blockUserId)
        {
            try
            {
                if (!GetUserFacade.IsUserBlocked (GetUserId (), blockUserId))
                {
                    UsersUtility.BlockUser (GetUserId (), blockUserId);
                }

                return true;
            }
            catch { }

            return false;
        }
        private void DeleteMessageDetail ()
        {
            try
            {
                int ret = 0;

                if (hidDetailMessageType.Value.Equals (RequestType.MESSAGES.ToString ()))
                {   // Remove Message in Inbox
                    ret = UsersUtility.DeleteMessage (GetUserId (), Convert.ToInt32 (hidDetailMessageId.Value));
                }
                else // Sent Message
                {
                    ret = UsersUtility.DeleteSentMessage (GetUserId (), Convert.ToInt32 (hidDetailMessageId.Value));
                }

                if (ret == 1)
                {
                    HideAllPanels ();
                        
                    if (hidDetailMessageType.Value.Equals (RequestType.MESSAGES.ToString ()))
                    {
                        BindMessages (1);
                        // Update count in nav
                        GetMessageCount ();
                    }
                    else
                    {
                        BindMessagesSent (1);
                    }

                    ShowAlertMsg ("The message was deleted.");
                    return;
                }
            }
            catch { }
            
            ShowAlertMsg ("The message was not deleted.");
        }
        private void ReportSpamMessage ()
        {
            System.Data.DataRow drMessage = UsersUtility.GetUserMessage (GetUserId (), Convert.ToInt32 (hidDetailMessageId.Value));

            BlockUser (Convert.ToInt32 (drMessage["from_id"]));

            User user = GetUserFacade.GetUser ("support");

            if (user.UserId > 0)
            {
                // Insert a private message
                Message message = new Message (0, GetUserId (), user.UserId, "Spam Alert",
                    "<BR><BR><B>--Spam Message--</B><BR><B>From:</B>&nbsp;" + drMessage["username"].ToString () + "<BR><B>Sent:</B>&nbsp;" + FormatDateTime (drMessage["message_date"]) + "<BR><BR>" + drMessage["message"].ToString (),
                    new DateTime (), 0, (int) Constants.eMESSAGE_TYPE.PRIVATE_MESSAGE, 0, "U", "S");

                GetUserFacade.InsertMessage (message);
            }

            ShowAlertMsg ("Message has been marked as spam.");
        }
        private void BlockKanevaUser ()
        {
            System.Data.DataRow drMessage = UsersUtility.GetUserMessage (GetUserId (), Convert.ToInt32 (hidDetailMessageId.Value));

            int fromUserId = Convert.ToInt32 (drMessage["from_id"]);
            if (!GetUserId ().Equals (fromUserId))
            {
                BlockUser (fromUserId);
                ShowAlertMsg ("User has been blocked.");
            }
            else
            {
                ShowAlertMsg ("You cannot block yourself.");
            }
        }
        #endregion  -- Methods [Message Details] --

        #region  -- Event Handlers [Message Details] --
        protected void btnReportSpam_Click (object sender, EventArgs e)
        {
            HideAlertMsg ();
            ShowModal (RequestType.SPAM, mpeDeleteMessageDetail);
        }
        protected void btnBlockUser_Click (object sender, EventArgs e)
        {
            HideAlertMsg ();
            ShowModal (RequestType.BlOCK_USER, mpeDeleteMessageDetail);
        }
        protected void btnBackToMessages_ItemCommand (object sender, CommandEventArgs e)
        {
            string msgType = e.CommandArgument.ToString ();
            if (msgType.Equals(RequestType.MESSAGES.ToString()))
            {
                HideAllPanels ();
                // Update the undread message count
                GetMessageCount ();
                BindMessages (1);
            }
            else if (msgType.Equals(RequestType.MESSAGES_SENT.ToString ()))
            {
                HideAllPanels ();
                BindMessagesSent (1);
            }
        }
        protected void btnDeleteMessageDetail_Click (object sender, EventArgs e)
        {
            HideAlertMsg ();
            ShowModal (RequestType.MESSAGE_DETAIL, mpeDeleteMessageDetail);
        }
        protected void btnMessageReply_Click (object sender, EventArgs e)
        {
            HideAllPanels ();
            // Config Message
            msg.Reset();
            msg.MessageId = Convert.ToInt32 (hidDetailMessageId.Value);
            msg.MsgType = MSG_TYPE.PRIVATE;
            ShowMessageEditor (msg);
        }
        #endregion  -- Event Handlers [Message Details] --

        #endregion **** Message Details ****

        // Message Editor
        #region **** Message Editor ****

        #region -- Methods [Message Editor] --
        private void ConfigMessageEditor ()
        {
            msg.Reset ();

            // Is it an asset share msg?
            if (!String.IsNullOrEmpty (Request["URL"]) && !String.IsNullOrEmpty(Request["assetId"]))
            {
                msg.AssetId = Convert.ToInt32 (Request["assetId"]);
                msg.Url = Server.UrlDecode (Request["URL"].ToString ());
                msg.MsgType = MSG_TYPE.SHARE;
            }
                // Is it a Private Message to a user?
            else if (!String.IsNullOrEmpty(Request["userId"]))
            {
                msg.ToUserId = Convert.ToInt32 (Request["userId"]);
                msg.MsgType = MSG_TYPE.PRIVATE;

                // Check to see if this is a message to community owner
                if (!String.IsNullOrEmpty (Request["communityId"]))
                {
                    msg.CommunityId = Convert.ToInt32 (Request["communityId"]);
                    msg.MsgType = MSG_TYPE.PRIVATE_COMM_OWNER;
                }
            }
                // Is it a Tell others about this user
            else if (!String.IsNullOrEmpty(Request["aboutId"]))
            {
                msg.AboutId = Convert.ToInt32 (Request["aboutId"]);
                msg.MsgType = MSG_TYPE.TELL_OTHERS_USER;
            }
                // Tell others about this community - I don't think this is still used but adding just in case
            else if (!String.IsNullOrEmpty (Request["communityId"]))
            {
                msg.CommunityId = Convert.ToInt32 (Request["communityId"]);
                msg.MsgType = MSG_TYPE.TELL_OTHERS_COMMUNITY;
            }
            else
            {
                msg.MsgType = MSG_TYPE.PRIVATE;
            }

            // Show the Editor
            ShowMessageEditor (msg);
        }
        private void ShowMessageEditor (MsgTypeAndIds msg)
        {
            SetPageTitle ("Compose Messages");
            HideAllPanels ();
            
            // Hide the Back to Blasts button 
            divBackButton.Visible = false;
            
            // Set default for TO field
            txtToMsgEditor.Visible = false;
            btnToMsgEditor.Visible = true;
            txtSubjectMsgEditor.Enabled = true;
                
            if (msg.MsgType.Equals (MSG_TYPE.PRIVATE))
            {
                if (msg.MessageId > 0) // Reply Message
                {
                    UserMessage usermsg = GetUserFacade.GetUserMessage (GetUserId (), msg.MessageId);
                    
                    btnToMsgEditor.Text = usermsg.UserName;
                    btnToMsgEditor.Attributes.Add ("href", GetPersonalChannelUrl (btnToMsgEditor.Text));
                    txtSubjectMsgEditor.Text = KanevaGlobals.Truncate ("RE:" + Server.HtmlDecode (usermsg.Subject), 100);
                    ceEditor.Text = "<BR><BR><B>--Previous Message--</B><BR><B>From:</B>&nbsp;" + usermsg.UserName + "<BR><B>Sent:</B>&nbsp;" + FormatDateTime (usermsg.MessageDate) + "<BR><BR>" + usermsg.MessageText;
                    hidEditorMessageId.Value = msg.MessageId.ToString ();

                    //check to see if sender is a minor trying to contact a mature profile
                    CheckForMessageFromMinor (usermsg.ToId, GetUserId ());
                }
                else // New Message
                {
                    int recipientUserId = msg.ToUserId;
                    User recipient = GetUserFacade.GetUser (recipientUserId);

                    //check to see if sender is a minor trying to contact a mature profile
                    CheckForMessageFromMinor (recipient.UserId, GetUserId ());
                    // Set the TO field
                    btnToMsgEditor.Text = recipient.Username;
                    btnToMsgEditor.Attributes.Add ("href", GetPersonalChannelUrl (recipient.Username));
                    txtSubjectMsgEditor.Text = "";
                }
            }
            else if (msg.MsgType.Equals (MSG_TYPE.PRIVATE_COMM_OWNER))
            {
                int recipientUserId = msg.ToUserId;
                User recipient = GetUserFacade.GetUser (recipientUserId);

                //check to see if sender is a minor trying to contact a mature profile
                CheckForMessageFromMinor (recipient.UserId, GetUserId ());
                // Set the TO field
                btnToMsgEditor.Text = recipient.Username;
                btnToMsgEditor.Attributes.Add ("href", GetPersonalChannelUrl (recipient.Username));
                txtSubjectMsgEditor.Text = "";
            }
            else if (msg.MsgType.Equals (MSG_TYPE.SHARE))
            {
                txtToMsgEditor.Visible = true;
                btnToMsgEditor.Visible = false;
                txtSubjectMsgEditor.Text = "Check out this item on Kaneva!";
                ceEditor.Text = "I hope you'll like this as much as I did.<br><br>";
            }
            else if (msg.MsgType.Equals (MSG_TYPE.TELL_OTHERS_USER))
            {
                txtToMsgEditor.Visible = true;
                btnToMsgEditor.Visible = false;
                txtSubjectMsgEditor.Enabled = false;
                txtSubjectMsgEditor.Text = "Please take a look at this cool profile!";
                ceEditor.Text = "<br>This Kaneva profile is awesome!<br><br>";
            }
            else if (msg.MsgType.Equals (MSG_TYPE.TELL_OTHERS_COMMUNITY))
            {
                txtToMsgEditor.Visible = true;
                btnToMsgEditor.Visible = false;
                txtSubjectMsgEditor.Enabled = false;
                txtSubjectMsgEditor.Text = "Kaneva World that might interest you";
                ceEditor.Text = "<br><br>I thought you might be interested in this Kaneva World!";
            }

            // If not date displays, then show current date
            if (lblDateMsgEditor.Text.Length == 0)
            {
                // Set the Date
                lblDateMsgEditor.Text = FormatDateTime (DateTime.Now);
            }
                    
            // Put MsgTypeAndIds object in ViewState
            ViewState["MsgTypeAndIds"] = msg;

            BuildPersonalMessageUrl (msg);
            ceEditor.Text += msg.Url;

            // Config the editor
            ceEditor.EditorBodyStyle = "font-size:12px;font-family:verdana";
            
            // Literals used for client side Javascript paste() method
            litEditorId.Text = ceEditor.ClientID;
            litSiteName.Text = KanevaGlobals.SiteName;

            // Show the editor panel
            upMessageEditor.Visible = true;

            // Set nav for Message
            SetNav (RequestType.MESSAGE_EDITOR);
        }
        private void BuildPersonalMessageUrl (MsgTypeAndIds msg)
        {
            if (msg.MsgType.Equals(MSG_TYPE.SHARE))
            {
                string url = "http://" + KanevaGlobals.SiteName + msg.Url;
                if (Request.IsAuthenticated)
                {
                    url += url.IndexOf ("?") > 0 ? "&" : "?";
                    url += "refId=" + this.GetUserId ();
                }
                msg.Url =  "Here's the link:<br><a href ='" + url + "'>" + url + "</a><br><br>Personal message:<br><br>";
            }
            // Tell others about this user
            else if (msg.MsgType.Equals(MSG_TYPE.TELL_OTHERS_USER))
            {
                User user = GetUserFacade.GetUser (msg.AboutId);

                msg.Url = "Here's the link:<br>  <a href ='" + GetPersonalChannelUrl (user.NameNoSpaces) + "'>" +
                    GetPersonalChannelUrl (user.NameNoSpaces) + "</a><br><br>Personal message:<br><br>";
            }
            // Tell others about this channel
            else if (msg.MsgType.Equals(MSG_TYPE.TELL_OTHERS_COMMUNITY) ||
                msg.MsgType.Equals (MSG_TYPE.PRIVATE_COMM_OWNER))
            {
                Community community = GetCommunityFacade.GetCommunity (msg.CommunityId);

                bool isPersonal = CommunityUtility.IsChannelPersonal (community.CommunityId);
                string communityUrl = null;

                if (isPersonal)
                {
                    communityUrl = KanevaGlobals.GetPersonalChannelUrl (community.NameNoSpaces);
                }
                else
                {
                    communityUrl = KanevaGlobals.GetBroadcastChannelUrl (community.NameNoSpaces);
                }
                msg.Url = "Here's the link:<br><a href ='" + communityUrl + "'>" + communityUrl + "</a><br><br>Personal message:<br><br>";
            }
        }
        private void CheckForMessageFromMinor (int recipientId, int messageSenderid)
        {
            User recipient = GetUserFacade.GetUser (recipientId);
            User messageSender = GetUserFacade.GetUser (messageSenderid);

            //check to see if sender is a minor trying to contact a mature profile
            if (recipient.MatureProfile && !messageSender.IsAdult)
            {
                Response.Redirect (GetPersonalChannelUrl (recipient.NameNoSpaces));
            }
        }
        private int SendMessage (MsgTypeAndIds msg, string strSubject)
        {
            //check for minor user to prevent work around by using tell others
            CheckForMessageFromMinor (msg.ToUserId, GetUserId ());

            // confirm the user can send a pm to a non-friend (user has not exceeded daily max)
            if (!IsAdministrator ())
            {
                if (UsersUtility.ConfirmCanSendNonFriendPM (GetUserId (), msg.ToUserId).Equals (Constants.ERR_USER_EXCEEDED_NON_FRIEND_MESSAGE_COUNT))
                {
                    return Constants.ERR_USER_EXCEEDED_NON_FRIEND_MESSAGE_COUNT;
                }

                if (GetUserFacade.IsUserBlocked(msg.ToUserId, KanevaWebGlobals.CurrentUser.UserId) || !KanevaWebGlobals.CurrentUser.HasWOKAccount)
                {
                    return Constants.ERR_USER_BLOCKED_FROM_SENDING_MESSAGE;
                }
            }

            // If this was a share, record it in the db
            if (msg.MsgType.Equals(MSG_TYPE.TELL_OTHERS_COMMUNITY) ||
                msg.MsgType.Equals (MSG_TYPE.PRIVATE_COMM_OWNER))
            {
                GetCommunityFacade.ShareCommunity (msg.CommunityId, GetUserId (), msg.ToUserId, "");
            }

            // Update message to show user replied to message
            if (msg.MessageId > 0)
            {
                UsersUtility.UpdateMessageReply (GetUserId (), msg.MessageId);
            }

            // Insert the message
            Message message = new Message (0, GetUserId (), msg.ToUserId, strSubject,
                msg.Url + ceEditor.Text, new DateTime (), 0, (int) Constants.eMESSAGE_TYPE.PRIVATE_MESSAGE, 0, "U", "S");

            GetUserFacade.InsertMessage (message);

            User userTo = GetUserFacade.GetUser (msg.ToUserId);
            if (GetUserId () > 0)
            {
                if (Convert.ToInt32 (userTo.NotifyAnyoneMessages).Equals (1))
                {
                    // They want emails for all messages
                    MailUtilityWeb.SendPrivateMessageNotificationEmail (userTo.Email, strSubject, message.MessageId, message.MessageText, userTo.UserId, GetUserId ());
                }
                else if (Convert.ToInt32 (userTo.NotifyFriendMessages).Equals (1) && GetUserFacade.AreFriends (msg.ToUserId, GetUserId ()))
                {
                    // They want to be emailed for messages from friends
                    MailUtilityWeb.SendPrivateMessageNotificationEmail (userTo.Email, strSubject, message.MessageId, message.MessageText, userTo.UserId, GetUserId ());
                }
            }

            return message.MessageId;
       }
        #endregion  -- Methods [Message Editor] --

        #region  -- Event Handlers [Message Editor] --
        protected void btnSendMessage_Click (object sender, EventArgs e)
        {
            int userId = 0;
            string strSubject = Server.HtmlEncode (txtSubjectMsgEditor.Text.Trim ());

            // Verify user entered a message
            if (ceEditor.Text.Trim () == "")
            {
                ShowAlertMsg ("Please enter a message.", false);
                return;
            }

            // Check for injections scripts
            if (KanevaWebGlobals.ContainsInjectScripts (ceEditor.Text) ||
                KanevaWebGlobals.ContainsInjectScripts (txtSubjectMsgEditor.Text) ||
                KanevaWebGlobals.ContainsInjectScripts (txtToMsgEditor.Text))
            {
                m_logger.Warn ("User " + GetUserId () + " tried to script from IP " + Common.GetVisitorIPAddress());
                ShowAlertMsg ("Your input contains invalid scripting, please remove script code and try again.", false);
                return;
            }

            // Check to make sure user did not enter any "potty mouth" words
            if (KanevaWebGlobals.isTextRestricted (ceEditor.Text, Constants.eRESTRICTION_TYPE.POTTY_MOUTH) ||
                KanevaWebGlobals.isTextRestricted (strSubject, Constants.eRESTRICTION_TYPE.POTTY_MOUTH))
            {
                ShowAlertMsg (Constants.VALIDATION_REGEX_RESTRICTED_WORD_MESSAGE, false);
                return;
            }

            // Default the subject
            if (strSubject.Length.Equals (0))
            {
                strSubject = NO_SUBJECT;
            }

            // Get the MsgTypeAndId Object from viewstate
            msg = (MsgTypeAndIds) ViewState["MsgTypeAndIds"];

            // Look up the user id via the username
            if (btnToMsgEditor.Visible)
            {                  
                msg.ToUserId = GetUserFacade.GetUserIdFromUsername (btnToMsgEditor.Text.Trim ());
            }
            else
            {
                msg.ToUserId = GetUserFacade.GetUserIdFromUsername (txtToMsgEditor.Text.Trim ());
            }

            if (msg.ToUserId == 0)
            {
                ShowAlertMsg ("Please provide a valid Kaneva username.", false);
                return;
            }

            // This is the case they are sending a PM, must be logged in
            if (!Request.IsAuthenticated)
            {
                Response.Redirect (GetLoginURL ());
                return;
            }

            int retCode = SendMessage (msg, strSubject);

            if (retCode.Equals (-99))
            {
                ShowAlertMsg ("Message not sent. This member has blocked you from sending messages to them.");
                return;
            }
            else if (retCode.Equals (Constants.ERR_USER_EXCEEDED_NON_FRIEND_MESSAGE_COUNT))
            {   // user has exceeded number of sent non-friend messages for today
                ShowAlertMsg ("You have exceeded the maximum daily number of private messages sent " +
                    " to non-friends (" + KanevaGlobals.MaxNumberNonFriendMessagesPerDay.ToString () +
                    "). In order to send this message, the person will need to add you as a friend."); //**+
                //**      "<br/><br/><a href=\"javascript:void(0);\" " +
                //**      " onclick=\"$('" + btnAddFriend.ClientID + "').click();" + "\">Become a Friend</a>");
                return;
            }

            // If this is an asset share
            if (msg.MsgType.Equals (MSG_TYPE.SHARE) && msg.AssetId > 0)
            {
                GetMediaFacade.ShareAsset (msg.AssetId, GetUserId (), Common.GetVisitorIPAddress(), null, userId);
            }

            BindMessages (1);
            ShowAlertMsg ("Your Message has been Sent.");
        }
        protected void btnCancelEditor_Click (object sender, EventArgs e)
        {
            try
            {
                
                ShowMessageDetails (Convert.ToInt32 (hidEditorMessageId.Value), true);
            }
            catch
            {
                BindMessages (1);
            }
        }
        #endregion  -- Event Handlers [Message Editor] --

        #region -- Declerations [Message Editor] --

        [Serializable]
        private class MsgTypeAndIds
        {
            private int toUserId = 0;
            private int communityId = 0;
            private int assetId = 0;
            private int aboutId = 0;
            private int messageId = 0;
            private string url = "";
            private MSG_TYPE msgType = MSG_TYPE.PRIVATE;

            public MsgTypeAndIds () {}
            public MSG_TYPE MsgType
            {
                set { msgType = value; }
                get { return msgType; }
            }
            public int ToUserId
            {
                set { toUserId = value; }
                get { return toUserId; }
            }
            public int AssetId
            {
                set { assetId = value; }
                get { return assetId; }
            }
            public int AboutId
            {
                set { aboutId = value; }
                get { return aboutId; }
            }
            public int CommunityId
            {
                set { communityId = value; }
                get { return communityId; }
            }
            public int MessageId
            {
                set { messageId = value; }
                get { return messageId; }
            }
            public string Url
            {
                set { url = value; }
                get { return url; }
            }

            public void Reset ()
            {
                toUserId = 0;
                communityId = 0;
                assetId = 0;
                aboutId = 0;
                messageId = 0;
                url = "";
                msgType = MSG_TYPE.PRIVATE;
            }
        }
        private MsgTypeAndIds msg = new MsgTypeAndIds (); 
        private enum MSG_TYPE
        {
            PRIVATE,
            PRIVATE_COMM_OWNER,
            SHARE,
            TELL_OTHERS_USER,
            TELL_OTHERS_COMMUNITY,
            EMAIL,
            MEET_ME_3D
        }
        private const string NO_SUBJECT = "No subject";

        #endregion -- Declerations [Message Editor] --

        #endregion **** Message Editor ****

        // Friend Requests
        #region **** Friend Requests ****

        #region -- Methods [Friend Requests] --
        /// <summary>
        /// Bind the data
        /// </summary>
        /// <param name="pageNumber"></param>
        private void BindFriendRequests (int pageNumber)
        {
            BindFriendRequests (pageNumber, "");
        }
        /// <summary>
        /// Bind the data
        /// </summary>
        /// <param name="pageNumber"></param>
        private void BindFriendRequests (int pageNumber, string filter)
        {
            CurrentSort = "request_date";
            string orderby = CurrentSort + " " + CurrentSortOrder; 
            SetPageTitle ("Friend Requests");
            HideAlertMsg ();

            PagedDataTable pds = UsersUtility.GetIncomingPendingFriends (GetUserId (), filter, orderby, true, pageNumber, PageSize);

            if (pds.TotalCount > 0)
            {
                rptFriendRequests.DataSource = pds;
                rptFriendRequests.DataBind ();

                // Set up the pager
                pagerFriendReq.NumberOfPages = Math.Ceiling ((double) pds.TotalCount / PageSize).ToString ();
                pagerFriendReq.CurrentPageNumber = pageNumber;
                pagerFriendReq.DrawControl ();

                // The results
                lblFriendRequestsResults.Text = GetResultsText (pds.TotalCount, pageNumber, PageSize, pds.Rows.Count);
                divFriendRequests.Style.Add ("display", "block");
                divFriendReqButtonRow.Style.Add ("display", "block");
            }
            else
            {
                divFriendRequests.Style.Add ("display", "none");
                string msg = "There are currently no outstanding Friend Requests.";
                string callout = "Would you like to <a href=\"" + ResolveUrl ("~/people/people.kaneva") + "\">Find Friends</a> or " +
                    " view <a href=\"javascript:$('" + btnFriendInvitesSent.ClientID + "').click();\">Sent Friend Invites</a>?";
                ShowNoResults (msg, callout);
            }

            // Set nav for Friend Requests
            SetNav (RequestType.FRIEND_REQ);
        }
        /// <summary>
        /// Bind the data
        /// </summary>
        /// <param name="pageNumber"></param>
        private void BindFriendsAdded (int pageNumber, int userId, string filter)
        {
            try
            {
                CurrentSort = "u.username";
                string orderby = CurrentSort + " " + CurrentSortOrder; 
                int pgSize = 50; 
                SetPageTitle("Friend Requests");
                HideAlertMsg ();

                PagedList<Friend> pds = GetUserFacade.GetFriends (userId, filter, orderby, pageNumber, pgSize);

                dlFriendsAdded.DataSource = pds;
                dlFriendsAdded.DataBind ();

                HideAllPanels ();
                upFriendRequestsAccepted.Visible = true;

//** should this use ShowAlertMsg ()?
                divNewFriendsCount.InnerText = "You have successfully added " + 
                pds.TotalCount.ToString() + " friend" + (pds.TotalCount == 1 ? "" : "s");

                SetNav (RequestType.FRIEND_REQ);
            }
            catch
            {
                BindFriendRequests (1);
            }
        }
        private void DeleteFriendRequests ()
        {
            int ret = 0;
            int count = 0;
            HideAlertMsg ();

            foreach (RepeaterItem item in rptFriendRequests.Items)
            {
                CheckBox chkEdit = (CheckBox) item.FindControl ("chkFREdit");

                if (chkEdit.Checked)
                {
                    int friendId = Convert.ToInt32 (((HtmlInputHidden) item.FindControl ("hidFriendId")).Value);
                    ret = GetUserFacade.DenyFriend (GetUserId (), friendId);

                    if (ret == 1)
                        count++;
                }
            }

            if (count > 0)
            {
                BindFriendRequests (1);
                // Update count in nav
                GetFriendRequestCount ();
            }

            ShowAlertMsg (count.ToString () + " friend request" + (count != 1 ? "s were" : " was") + " ignored.");
        }
        #endregion -- Methods [Friend Requests] --

        #region -- Event Handlers [Friend Requests] --
        protected void rptFriendRequests_ItemCommand (object source, RepeaterCommandEventArgs e)
        {
            int ret = 0;
            string command = e.CommandName;
            HideAlertMsg ();

            if (command.Equals ("cmdApprove"))
            {
                HtmlInputHidden hidFriendId = (HtmlInputHidden) e.Item.FindControl ("hidFriendId");
                int friendId = Convert.ToInt32 (hidFriendId.Value);
                ret = GetUserFacade.AcceptFriend (KanevaWebGlobals.CurrentUser.UserId, friendId);
             
                if (ret == 1)
                {
                    UserFacade userFacade = new UserFacade ();

                    if (userFacade.GetUser (friendId).NotifyNewFriends)
                    {
                        MailUtilityWeb.SendNewFriendNotification (friendId, KanevaWebGlobals.CurrentUser.UserId);
                    }

                    BindFriendsAdded (1, KanevaWebGlobals.CurrentUser.UserId, "glued_date > ADDDATE(NOW(),INTERVAL -15 SECOND)");
                    // Update count in nav
                    GetFriendRequestCount ();
                }
                else
                {
                    // Error
                    ShowAlertMsg ("An error was encountered while trying to approve friend request.");
                    SetNav (RequestType.FRIEND_REQ);
                }
            }
            else if (command.Equals ("cmdDecline"))     
            {
                CheckBox chkEdit = (CheckBox) e.Item.FindControl ("chkFREdit");
                chkEdit.Checked = true;

                // Show Confirm Dialog
                AjaxControlToolkit.ModalPopupExtender mpe = (AjaxControlToolkit.ModalPopupExtender) e.Item.FindControl ("mpeFriendReq");
                if (mpe != null)
                {
                    ShowModal (RequestType.FRIEND_REQ, mpe);
                }
            }
        }
        /// <summary>
        /// btnApproveFriends_Click
        /// </summary>
        protected void btnApproveFriends_Click (Object sender, EventArgs e)
        {
            int ret = 0;
            int count = 0;
            bool isItemChecked = false;
            HideAlertMsg ();

            foreach (RepeaterItem dliMessage in rptFriendRequests.Items)
            {
                CheckBox chkEdit = (CheckBox) dliMessage.FindControl ("chkFREdit");

                if (chkEdit.Checked)
                {
                    isItemChecked = true;

                    HtmlInputHidden hidFriendId = (HtmlInputHidden) dliMessage.FindControl ("hidFriendId");
                    int friendId = Convert.ToInt32 (hidFriendId.Value);
                    ret = GetUserFacade.AcceptFriend (KanevaWebGlobals.CurrentUser.UserId, friendId);

                    if (ret == 1)
                    {
                        count++;

                        try
                        {
                            if (GetUserFacade.GetUser (friendId).NotifyNewFriends)
                            {
                                MailUtilityWeb.SendNewFriendNotification (friendId, KanevaWebGlobals.CurrentUser.UserId);
                            }
                        }
                        catch { }
                    }
                }
            }

            if (isItemChecked)
            {
                if (count > 0)
                {
                    BindFriendsAdded (1, KanevaWebGlobals.CurrentUser.UserId, "glued_date > ADDDATE(NOW(),INTERVAL -3 SECOND)");
                    // Update count in nav
                    GetFriendRequestCount ();
                    return;
                }

                if (ret != 1)
                {
                    ShowAlertMsg ("An error was encountered while trying to approve Friend Request.");
                }
                else
                {
                    ShowAlertMsg ("No friend requests were accepted.");
                }
            }
            else
            {
                ShowAlertMsg ("No friend requests were selected.");
            }

            SetNav (RequestType.FRIEND_REQ);
        }
        /// <summary>
        /// btnIgnoreFriends_Click
        /// </summary>
        protected void btnIgnoreFriends_Click (Object sender, EventArgs e)
        {
            bool isItemChecked = false;
            HideAlertMsg ();

            foreach (RepeaterItem dliMessage in rptFriendRequests.Items)
            {
                CheckBox chkEdit = (CheckBox) dliMessage.FindControl ("chkFREdit");

                if (chkEdit.Checked)
                {
                    ShowModal (RequestType.FRIEND_REQ, mpeIgnoreFriends);
                    return;
                }
            }

            if (!isItemChecked)
            {
                ShowAlertMsg ("No friend requests were selected.");
                SetNav (RequestType.FRIEND_REQ);
            }
        }
        /// <summary>
        /// Page Change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void pagerFriendReq_PageChange (object sender, PageChangeEventArgs e)
        {
            BindFriendRequests (e.PageNumber);
        }
        #endregion -- Event Handlers [Friend Requests] --

        #endregion **** Friend Requests ****

        // Friend Invites >>> NOT DISPLAYING THIS SECTION - THIS WAS THE OLD FRIEND REQUESTS SENT PAGE <<<<
        #region **** Friend Invites ****

        #region -- Methods [Friend Invites] --
        /// <summary>
        /// Bind the data
        /// </summary>
        /// <param name="pageNumber"></param>
        private void BindFriendInvites (int pageNumber)
        {
            BindFriendInvites (pageNumber, "");
        }
        /// <summary>
        /// Bind the data
        /// </summary>
        /// <param name="pageNumber"></param>
        private void BindFriendInvites (int pageNumber, string filter)
        {
            CurrentSort = "request_date";
            string orderby = CurrentSort + " " + CurrentSortOrder;
            SetPageTitle ("Friend Requests Sent");
            HideAlertMsg ();

            PagedDataTable pds = UsersUtility.GetOutgoingPendingFriends (GetUserId (), filter, orderby, true, pageNumber, PageSize);

            if (pds.TotalCount > 0)
            {
                rptFriendInvites.DataSource = pds;
                rptFriendInvites.DataBind ();

                // Set up the pager
                pagerFriendInvites.NumberOfPages = Math.Ceiling ((double) pds.TotalCount / PageSize).ToString ();
                pagerFriendInvites.CurrentPageNumber = pageNumber;
                pagerFriendInvites.DrawControl ();

                // The results
                lblFriendInvitesResults.Text = GetResultsText (pds.TotalCount, pageNumber, PageSize, pds.Rows.Count);
                upFriendInvites.Visible = true;
                divFriendInvitesButtonRow.Visible = true;
            }
            else
            {
                upFriendInvites.Visible = false;
                string msg = "There are currently no outstanding Sent Friend Invites.";
                string callout = "Would you like to <a href=\"" + ResolveUrl ("~/people/people.kaneva") + "\">Find Friends</a> or " +
                    " view <a href=\"javascript:$('" + btnFriendRequests.ClientID + "').click();\">Friend Requests</a>?";
                ShowNoResults (msg, callout);
            }

            // Set nav for Friend Requests Sent
            SetNav (RequestType.FRIEND_REQ_SENT);
        }
        /// <summary>
        /// CanResendInvite
        /// </summary>
        public string CanResendInvite (object date)
        {
            DateTime dt = (DateTime) date;

            if (dt.AddDays (14) > DateTime.Now)
            {
                return "false";
            }
            else
            {
                return "true";
            }
        }
        #endregion -- Methods [Friend Invites] --

        #region -- Event Handlers  [Friend Invites] --
        /// <summary>
        /// rptFriendRequestsSent_ItemCommand
        /// </summary>
        protected void rptFriendInvites_ItemCommand (object source, RepeaterCommandEventArgs e)
        {
            int ret = 0;
            string command = e.CommandName;
            HideAlertMsg ();

            if (command.Equals ("cmdResend"))
            {
                HtmlInputHidden hidUserId = (HtmlInputHidden) e.Item.FindControl ("hidInvitesUserId");
                int friendId = Convert.ToInt32 (hidUserId.Value);
                GetUserFacade.UpdateFriendRequestSentDate (GetUserId (), friendId);
                ret = MailUtilityWeb.SendFriendRequestEmail (GetUserId(), friendId);

                if (ret == 1)
                {
                    // Success
                    BindFriendInvites (pagerFriendInvites.CurrentPageNumber);

                    ShowAlertMsg ("Successfully resent friend request to 1 friend.");
                }
                else
                {
                    // Error
                    ShowAlertMsg ("An error was encountered while trying to resend friend request.");
                }
            }

            SetNav (RequestType.FRIEND_REQ_SENT);
        }
        /// <summary>
        /// Item Databound Event Handler
        /// </summary>
        protected void rptFriendInvites_ItemDataBound (object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                LinkButton btnResend = (LinkButton) e.Item.FindControl ("btnResendFriendInvite");
                CheckBox chkEdit = (CheckBox) e.Item.FindControl ("chkFIEdit");

                DateTime dt = (DateTime) DataBinder.Eval (e.Item.DataItem, "request_date");

                if (dt.AddDays (14) > DateTime.Now)
                {
                    btnResend.Enabled = false;
                    HtmlContainerControl divResendButton = (HtmlContainerControl) e.Item.FindControl ("divResendInviteButton");
                    divResendButton.Attributes.Add ("class", "button disabled");
                }
                else
                {
                    btnResend.Enabled = true;
                }
            }
        }
        /// <summary>
        /// btnFriendRequestsSentCancel_Click
        /// </summary>
        protected void btnFriendInvitesCancel_Click (Object sender, EventArgs e)
        {
            int ret = 0;
            int count = 0;
            bool isItemChecked = false;

            foreach (RepeaterItem item in rptFriendInvites.Items)
            {
                if (((CheckBox) item.FindControl ("chkFIEdit")).Checked)
                {
                    isItemChecked = true;

                    ret = GetUserFacade.DeleteFriendRequest (GetUserId(), Convert.ToInt32 (((HtmlInputHidden) item.FindControl ("hidInviteUserId")).Value));

                    if (ret == 1)
                        count++;
                }
            }

            if (isItemChecked)
            {
                if (count > 0)
                {
                    BindFriendInvites (pagerFriendInvites.CurrentPageNumber);
                    ShowAlertMsg ("Successfully removed " + count + " friend request" + (count != 1 ? "s" : "") + ".");
                    return;
                }
                else
                {
                    ShowAlertMsg ("An error was encountered while trying to remove Friend Request.");
                }
            }
            else
            {
                ShowAlertMsg ("No friend requests were selected.");
            }

            SetNav (RequestType.FRIEND_REQ_SENT);
        }
        /// <summary>
        /// btnResendFriendReq_Click
        /// </summary>
        protected void btnResendFriendInvites_Click (Object sender, EventArgs e)
        {
            int ret = 99;
            int count = 0;
            int countNotSent = 0;
            bool isItemChecked = false;
            HideAlertMsg ();

            // Remove the friends
            foreach (RepeaterItem rptFriend in rptFriendInvites.Items)
            {
                if (((CheckBox) rptFriend.FindControl ("chkFIEdit")).Checked)
                {
                    isItemChecked = true;

                    DateTime dtSent = Convert.ToDateTime (((HtmlInputHidden) rptFriend.FindControl ("hidDateSent")).Value);

                    if (dtSent.AddDays (14) > DateTime.Now)
                    {
                        countNotSent++;
                    }
                    else
                    {
                        int friendId = Convert.ToInt32 (((HtmlInputHidden) rptFriend.FindControl ("hidSentReqUserId")).Value);
                        GetUserFacade.UpdateFriendRequestSentDate (GetUserId (), friendId);
                        ret = MailUtilityWeb.SendFriendRequestEmail (GetUserId (), friendId);

                        if (ret == 1)
                            count++;
                    }
                }
            }

            if (isItemChecked)
            {
                if (count > 0)
                {
                    BindFriendInvites (pagerFriendInvites.CurrentPageNumber);
                }

                if (ret < 1)
                {
                    ShowAlertMsg ("An error was encountered while trying to resend friend request.");
                }
                else
                {
                    string alertMsg = "";
                    if (count > 0)
                    {
                        alertMsg = "Successfully resent " + count + " friend request" + (count != 1 ? "s" : "") + ".";
                    }
                    if (countNotSent > 0)
                    {
                        alertMsg += "  " + countNotSent.ToString () + "friend request" + (countNotSent != 1 ? "s are" : " is") + " outside of appropriate time frame.";
                    }
                    if (alertMsg.Length > 0)
                    {
                        ShowAlertMsg (alertMsg);
                    }
                }
            }
            else
            {
                ShowAlertMsg ("No friend requests were selected.");
                SetNav (RequestType.FRIEND_REQ_SENT);
            }
        }
        /// <summary>
        /// Page Change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void pagerFriendInvites_PageChange (object sender, PageChangeEventArgs e)
        {
            BindFriendInvites (e.PageNumber);
        }
        #endregion -- Event Handers  [Friend Invites] --

        #endregion **** Friend Invites ****

        // Friend Requests Sent
        #region **** Friend Requests Sent ****

        #region -- Methods [Friend Requests Sent] --
        /// <summary>
        /// Bind the data
        /// </summary>
        /// <param name="pageNumber"></param>
        private void BindFriendRequestsSent (int pageNumber)
        {
            BindFriendRequestsSent (pageNumber, "");
        }
        private void BindFriendRequestsSent (int pageNumber, string filter)
        {
            CurrentSort = "reinvite_date";
            string orderby = CurrentSort + " " + CurrentSortOrder;
            SetPageTitle ("Sent Friend Invites");
            HideAlertMsg ();

            // Set the search filter
            if (filter.Length > 0)
            {
                filter += " AND ";
            }
            filter += " invite_status_id IN (" + (int) Constants.eINVITE_STATUS.REGISTERED + ", " +
                    (int) Constants.eINVITE_STATUS.UNREGISTER + ", " +
                    (int) Constants.eINVITE_STATUS.WOK_LOGIN + ", " +
                    (int) Constants.eINVITE_STATUS.DECLINED + ", " +
                    (int) Constants.eINVITE_STATUS.MAX_IP + ")";

            PagedDataTable pds = UsersUtility.GetAllInvites (GetUserId (), filter, orderby, pageNumber, PageSize);

            if (pds.TotalCount > 0)
            {
                rptFriendRequestsSent.DataSource = pds;
                rptFriendRequestsSent.DataBind ();

                // Set up the pager
                pagerFriendReqSent.NumberOfPages = Math.Ceiling ((double) pds.TotalCount / PageSize).ToString ();
                pagerFriendReqSent.CurrentPageNumber = pageNumber;
                pagerFriendReqSent.DrawControl ();

                // The results
                lblFriendRequestsSentResults.Text = GetResultsText (pds.TotalCount, pageNumber, PageSize, pds.Rows.Count);
                divFriendRequestsSent.Style.Add ("display", "block");
                divFriendReqSentButtonRow.Visible = true;
            }
            else
            {
                divFriendRequestsSent.Style.Add ("display", "none");
                string msg = "There are currently no outstanding Friend Invites.";
                string callout = "Would you like to <a href=\"" + ResolveUrl ("~/people/people.kaneva") + "\">Find Friends</a> or " +
                    " view <a href=\"javascript:$('" + btnFriendRequests.ClientID + "').click();\">Friend Requests</a>?";
                ShowNoResults (msg, callout);
            }

            // Set nav for Friend Requests Sent
            SetNav (RequestType.FRIEND_REQ_SENT);

            // Get Fame Pts for invites
 //           FameFacade fameFacade = new FameFacade ();
 //           DataRow drFamePts = fameFacade.GetFamePointsFromInvitingFriends (KanevaWebGlobals.CurrentUser.UserId);

 //           if (drFamePts != null)
 //           {
 //               spnFamePts.InnerText = drFamePts["total_points"].ToString ();
 //           }
        }
        private void SendEmail (int inviteId)
        {
            // Get the existing invite
            System.Data.DataRow drInvite = UsersUtility.GetInvite (inviteId);

            if (drInvite != null)
            {
                string keyCode = drInvite["key_value"].ToString ();
                string toEmail = drInvite["email"].ToString ();
                string toName = drInvite["to_name"].ToString ();

                User newUser = GetUserFacade.GetUserByEmail (toEmail);

                // Does this email already belong to a Kaneva member?
                if (newUser.UserId == 0)
                {
                    // Send out the email										   
                    MailUtilityWeb.SendInvitation (GetUserId(), toEmail, toName, "", inviteId, keyCode, Page);
                }
            }
        }
        //
        protected string GetStatusName (int statusId)
        {
            string status = string.Empty;

            switch (statusId)
            {
                case (int) Constants.eINVITE_STATUS.REGISTERED:
                    status = "Joined";
                    break;
                case (int) Constants.eINVITE_STATUS.UNREGISTER:
                    status = "Not Joined";
                    break;
                case (int) Constants.eINVITE_STATUS.MAX_IP:
                    status = "Same IP*";
                    break;
                case (int) Constants.eINVITE_STATUS.WOK_LOGIN:
                    status = "Joined In World";
                    break;
                case (int) Constants.eINVITE_STATUS.DECLINED:
                    status = "Declined";
                    break;
                default:
                    status = "Not Joined";
                    break;
            }

            return status;
        }
        #endregion -- Methods [Friend Requests Sent] --

        #region -- Event Handlers [Friend Requests Sent] --
        //
        protected void rptFriendRequestsSent_ItemDataBound (object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                LinkButton btnResend = (LinkButton) e.Item.FindControl ("btnResendFriendReq");
                CheckBox chkEdit = (CheckBox) e.Item.FindControl ("chkFRSEdit");

                DateTime dt = (DateTime) DataBinder.Eval (e.Item.DataItem, "reinvite_date");
                int statusId = (int) DataBinder.Eval (e.Item.DataItem, "invite_status_id");

                if ((dt.AddDays (14) > DateTime.Now) ||
                     (statusId == (int) Constants.eINVITE_STATUS.REGISTERED) ||
                     (statusId == (int) Constants.eINVITE_STATUS.DECLINED))
                {
                    btnResend.Enabled = false;
                    HtmlContainerControl divResendButton = (HtmlContainerControl) e.Item.FindControl ("divResendFriendReqButton");
                    divResendButton.Attributes.Add ("class", "btnxsmall grey disabled");
                }
                else
                {
                    btnResend.Enabled = true;
                }
            }
        }
        //
        protected void rptFriendRequestsSent_ItemCommand (object source, RepeaterCommandEventArgs e)
        {
            try
            {
                int ret = 0;
                string command = e.CommandName;
                HideAlertMsg ();

                int inviteId = Convert.ToInt32 (((HtmlInputHidden) e.Item.FindControl ("hidInviteId")).Value);

                if (command.Equals ("Resend"))
                {
                    //resend the invite message
                    SendEmail (inviteId);

                    // Resend Invite
                    ret = UsersUtility.ResendInvite (inviteId);

                    if (ret == 1)
                    {
                        BindFriendRequestsSent (1);
                        ShowAlertMsg ("Successfully resent 1 friend invite.");
                    }
                    else
                    {
                        // Error
                        ShowAlertMsg ("An error was encountered while trying to resend friend invite.");
                    }
                }
                else if (command.Equals ("Remove"))
                {
                    ret = UsersUtility.UpdateInvite (inviteId, (int) Constants.eINVITE_STATUS.DELETED);

                    if (ret == 1)
                    {
                        BindFriendRequestsSent (1);
                        ShowAlertMsg ("Successfully removed 1 friend invite.");
                    }
                    else
                    {
                        ShowAlertMsg ("An error was encountered while trying to remove a sent friend invite.");
                    }
                }
            }
            catch (Exception ex)
            {
                m_logger.Error ("Error encountered trying to process ItemCommand. " + ex);
                ShowAlertMsg ("An error was encountered while trying to process your request.");
            } 
        }
        protected void btnFriendRequestsSentRemove_Click (Object sender, EventArgs e)
        {
            int ret = 0;
            int count = 0;
            int inviteId = 0;
            bool isItemChecked = false;

            foreach (RepeaterItem item in rptFriendRequestsSent.Items)
            {
                if (((CheckBox) item.FindControl ("chkFRSEdit")).Checked)
                {
                    isItemChecked = true;
                    inviteId = Convert.ToInt32 (((HtmlInputHidden) item.FindControl ("hidInviteId")).Value);

                    if (inviteId > 0)
                    {
                        ret = UsersUtility.UpdateInvite (inviteId, (int) Constants.eINVITE_STATUS.DELETED);

                        inviteId = 0;
                        if (ret == 1)
                            count++;
                    }
                }
            }

            if (isItemChecked)
            {
                if (count > 0)
                {
                    BindFriendRequestsSent (1);
                    ShowAlertMsg ("Successfully removed " + count + " friend invite" + (count != 1 ? "s" : "") + ".");
                    return;
                }
                else
                {
                    ShowAlertMsg ("An error was encountered while trying to remove Friend Invite.");
                }
            }
            else
            {
                ShowAlertMsg ("No friend requests were selected.");
            }

            SetNav (RequestType.FRIEND_REQ_SENT);
        }
        protected void btnResendFriendReq_Click (Object sender, EventArgs e)
        {
            int count = 0;
            bool isItemChecked = false;
            HideAlertMsg ();

            foreach (RepeaterItem item in rptFriendRequestsSent.Items)
            {
                if (((CheckBox) item.FindControl ("chkFRSEdit")).Checked)
                {
                    isItemChecked = true;

                    int inviteId = Convert.ToInt32 (((HtmlInputHidden) item.FindControl ("hidInviteId")).Value);
                    
                    //resend the invite message
                    SendEmail (inviteId);

                    // Resend Invite
                    if (UsersUtility.ResendInvite (inviteId) == 1)
                    {
                        count++;
                    }
                }
            }

            if (isItemChecked)
            {
                if (count > 0)
                {
                    BindFriendRequestsSent (1);
                    ShowAlertMsg ("Successfully resent " + count + " friend invite" + (count != 1 ? "s." : "."));
                }
                else
                {
                    ShowAlertMsg ("No friend requests were resent.");
                }
            }
            else
            {
                ShowAlertMsg ("No friend requests were selected.");
            }

            SetNav (RequestType.FRIEND_REQ);
        }
        public void pagerFriendReqSent_PageChange (object sender, PageChangeEventArgs e)
        {
            BindFriendRequestsSent (e.PageNumber);
        }
        #endregion -- Event Handlers [Friend Requests Sent] --

        #endregion **** Friend Requests Sent ****

        // Community Requests
        #region **** Community Requests ****

        #region -- Methods [Community Requests] --
        /// <summary>
        /// Bind the data
        /// </summary>
        /// <param name="pageNumber"></param>
        private void BindCommunityRequests (int pageNumber)
        {
            BindCommunityRequests (pageNumber, "");
        }
        /// <summary>
        /// Bind the data
        /// </summary>
        /// <param name="pageNumber"></param>
        private void BindCommunityRequests (int pageNumber, string filter)
        {
            CurrentSort = "cm2.added_date";
            string orderby = CurrentSort + " " + CurrentSortOrder;
            SetPageTitle ("World Approvals");
            HideAllPanels ();

            PagedDataTable pds = CommunityUtility.GetPendingMembers (GetUserId (), "", orderby, pageNumber, PageSize);

            if (pds.TotalCount > 0)
            {
                rptCommunityRequests.DataSource = pds;
                rptCommunityRequests.DataBind ();

                // Set up the pager
                pagerCommunityReq.NumberOfPages = Math.Ceiling ((double) pds.TotalCount / PageSize).ToString ();
                pagerCommunityReq.CurrentPageNumber = pageNumber;
                pagerCommunityReq.DrawControl ();

                // The results
                lblCommunityRequestsResults.Text = GetResultsText (pds.TotalCount, pageNumber, PageSize, pds.Rows.Count);
                upCommunityRequests.Visible = true;
                divCommunityReqButtonRow.Visible = true;
            }
            else
            {
                upCommunityRequests.Visible = false;
                string msg = "There are currently no outstanding World Approvals.";
                string callout = "Would you like to <a href=\"" + ResolveUrl ("~/community/channel.kaneva") + "\">Find Worlds</a>?";
                ShowNoResults (msg, callout);
            }

            // Set nav for Community Requests
            SetNav (RequestType.COMMUNITY_REQ);
        }
        private void BindMembersAdded (int count, string listUserIds)
        {
            try
            {
                SetPageTitle ("World Approvals");

                PagedList<User> members = GetUserFacade.GetUsers ("u.user_id IN (" + listUserIds + ")", "username", 1, 50);

                dlMembersAdded.DataSource = members;
                dlMembersAdded.DataBind ();

                HideAllPanels ();
                upCommunityRequestsAccepted.Visible = true;

                divNewMembersCount.InnerText = "You have successfully added " +
                members.TotalCount.ToString () + " World member" + (members.TotalCount == 1 ? "" : "s");
                
                SetNav (RequestType.COMMUNITY_REQ);
            }
            catch
            {
                BindCommunityRequests (1);
            }
        }
        private void DeleteCommunityRequests ()
        {
            int ret = 0;
            int count = 0;
            HideAlertMsg ();

            foreach (RepeaterItem item in rptCommunityRequests.Items)
            {
                CheckBox chkEdit = (CheckBox) item.FindControl ("chkCREdit");

                if (chkEdit.Checked)
                {
                    int memberId = Convert.ToInt32 (((HtmlInputHidden) item.FindControl ("hidUserId")).Value);
                    int channelId = Convert.ToInt32 (((HtmlInputHidden) item.FindControl ("hidChannelId")).Value);

                    ret = GetCommunityFacade.UpdateCommunityMember(channelId, memberId, (UInt32)CommunityMember.CommunityMemberStatus.REJECTED);

                    if (ret == 1)
                        count++;
                }
            }

            if (count > 0)
            {
                BindCommunityRequests (1);
                // Update count in nav
                GetCommunityRequestsCount ();
            }

            ShowAlertMsg (count.ToString () + " World Approval" + (count != 1 ? "s were" : " was") + " ignored.");
        }
        #endregion -- Methods [Community Requests] --

        #region -- Event Handlers [Community Requests] --
        protected void rptCommunityRequests_ItemCommand (object source, RepeaterCommandEventArgs e)
        {
            int ret = 0;
            string command = e.CommandName;
            HideAlertMsg ();

            if (command.Equals ("cmdApprove"))
            {
                HtmlInputHidden hidUserId = (HtmlInputHidden) e.Item.FindControl ("hidUserId");
                HtmlInputHidden hidChannelId = (HtmlInputHidden) e.Item.FindControl ("hidChannelId");

                int memberId = Convert.ToInt32 (hidUserId.Value);
                int channelId = Convert.ToInt32 (hidChannelId.Value);

                if (CommunityUtility.IsCommunityModerator (channelId, GetUserId ()))
                {
                    ret = GetCommunityFacade.UpdateCommunityMember(channelId, memberId, (UInt32)CommunityMember.CommunityMemberStatus.ACTIVE);

                    if (ret == 1)
                    {
                        BindMembersAdded (1, memberId.ToString ());
                        // Update count in nav
                        GetCommunityRequestsCount ();
                    }
                    else
                    {
                        // Error
                        ShowAlertMsg ("An error was encountered while trying to approve a World Approval.");
                    }
                }
                else
                {
                    // Error
                    ShowAlertMsg ("You are not authorized to approve/decline membership requests for this World.");
                }
            }
            else if (command.Equals ("cmdDecline"))
            {
                CheckBox chkCREdit = (CheckBox) e.Item.FindControl ("chkCREdit");
                chkCREdit.Checked = true;

                // Show Confirm Dialog
                AjaxControlToolkit.ModalPopupExtender mpe = (AjaxControlToolkit.ModalPopupExtender) e.Item.FindControl ("mpeMemberReq");
                if (mpe != null)
                {
                    ShowModal (RequestType.COMMUNITY_REQ, mpe);
                }
            }
        }
        protected void btnApproveMember_Click (Object sender, EventArgs e)
        {
            int ret = 0;
            int count = 0;
            bool isItemChecked = false;
            string listUserIds = string.Empty;
            HideAlertMsg ();

            foreach (RepeaterItem dliMessage in rptCommunityRequests.Items)
            {
                CheckBox chkEdit = (CheckBox) dliMessage.FindControl ("chkCREdit");

                if (chkEdit.Checked)
                {
                    isItemChecked = true;

                    HtmlInputHidden hidUserId = (HtmlInputHidden) dliMessage.FindControl ("hidUserId");
                    HtmlInputHidden hidChannelId = (HtmlInputHidden) dliMessage.FindControl ("hidChannelId");

                    int memberId = Convert.ToInt32 (hidUserId.Value);
                    int channelId = Convert.ToInt32 (hidChannelId.Value);

                    ret = GetCommunityFacade.UpdateCommunityMember(channelId, memberId, (UInt32)CommunityMember.CommunityMemberStatus.ACTIVE);

                    if (ret == 1)
                    {
                        if (listUserIds.Length > 0)
                        {
                            listUserIds += "," + memberId.ToString ();
                        }
                        else
                        {
                            listUserIds += memberId.ToString ();
                        }

                        count++;
                    }
                }
            }

            if (isItemChecked)
            {
                if (count > 0)
                {
                    BindMembersAdded (count, listUserIds);

                    // Update count in nav
                    GetCommunityRequestsCount ();

                    return;
                }

                if (ret != 1)
                {
                    ShowAlertMsg ("An error was encountered while trying to approve the World Approval.");
                }
                else
                {
                    ShowAlertMsg ("No World Approvals were approved.");
                }
            }
            else
            {
                ShowAlertMsg("No World Approvals were selected.");
            }

            SetNav (RequestType.COMMUNITY_REQ);
        }
        protected void btnIgnoreMember_Click (Object sender, EventArgs e)
        {
            bool isItemChecked = false;
            HideAlertMsg ();         

            foreach (RepeaterItem dliMessage in rptCommunityRequests.Items)
            {
                CheckBox chkEdit = (CheckBox) dliMessage.FindControl ("chkCREdit");

                if (chkEdit.Checked)
                {
                    ShowModal (RequestType.COMMUNITY_REQ, mpeIgnoreMembers);
                    break;
                }
            }

            if (!isItemChecked)
            {
                ShowAlertMsg("No World Approvals were selected.");
                SetNav (RequestType.COMMUNITY_REQ);
            }
        }
        protected void btnYesCommunityRequest_Click (Object sender, EventArgs e)
        {
            int ret = 0;
            int count = 0;
            HideAlertMsg ();

            foreach (RepeaterItem dliMessage in rptCommunityRequests.Items)
            {
                CheckBox chkEdit = (CheckBox) dliMessage.FindControl ("chkCREdit");

                if (chkEdit.Checked)
                {
                    HtmlInputHidden hidUserId = (HtmlInputHidden) dliMessage.FindControl ("hidUserId");
                    HtmlInputHidden hidChannelId = (HtmlInputHidden) dliMessage.FindControl ("hidChannelId");

                    int memberId = Convert.ToInt32 (hidUserId.Value);
                    int channelId = Convert.ToInt32 (hidChannelId.Value);

                    ret = GetCommunityFacade.UpdateCommunityMember(channelId, memberId, (UInt32)CommunityMember.CommunityMemberStatus.REJECTED);

                    if (ret == 1)
                        count++;
                }
            }

            if (count > 0)
            {
                BindCommunityRequests (1);
                // Update count in nav
                GetCommunityRequestsCount ();
            }

            ShowAlertMsg(count.ToString() + " World Approval" + (count != 1 ? "s were" : " was") + " ignored.");
        }
        protected void pagerCommunityReq_PageChange (object sender, PageChangeEventArgs e)
        {
            BindCommunityRequests (e.PageNumber);
        }
        #endregion -- Event Handlers [Community Requests] --

        #endregion **** Community Requests ****

        // Community Invites
        #region **** Community Invites ****

        #region -- Methods [Community Invites] --
        /// <summary>
        /// Bind the data
        /// </summary>
        /// <param name="pageNumber"></param>
        private void BindCommunityInvites (int pageNumber)
        {
            BindCommunityInvites (pageNumber, "");
        }
        /// <summary>
        /// Bind the data
        /// </summary>
        /// <param name="pageNumber"></param>
        private void BindCommunityInvites (int pageNumber, string filter)
        {
            CurrentSort = "message_date";
            string orderby = CurrentSort + " " + CurrentSortOrder;
            SetPageTitle("World Invites");
            HideAlertMsg ();

            if (filter.Length > 0)
            {
                filter += " AND ";
            }
            filter += " m.type IN (" + (int) Constants.eMESSAGE_TYPE.COMMUNITY_INVITES + ")";

            PagedDataTable pds = UsersUtility.GetUserMessages (GetUserId (), filter, orderby, pageNumber, PageSize);

            if (pds.TotalCount > 0)
            {
                rptCommunityInvites.DataSource = pds;
                rptCommunityInvites.DataBind ();

                // Set up the pager
                pagerCommInvites.NumberOfPages = Math.Ceiling ((double) pds.TotalCount / PageSize).ToString ();
                pagerCommInvites.CurrentPageNumber = pageNumber;
                pagerCommInvites.DrawControl ();

                // The results
                lblCommInvitesResults.Text = GetResultsText (pds.TotalCount, pageNumber, PageSize, pds.Rows.Count);
                upCommunityInvites.Visible = true;
            }
            else
            {
                upCommunityInvites.Visible = false;
                string msg = "There are currently no outstanding World Invites.";
                string callout = ""; 
                ShowNoResults (msg, callout);
            }

            // Set nav for Community Invites
            SetNav (RequestType.COMMUNITY_INV);
        }
        private void DeleteCommunityInvites ()
        {
            int ret = 0;
            int count = 0;
            HideAlertMsg ();

            foreach (RepeaterItem item in rptCommunityInvites.Items)
            {
                CheckBox chkEdit = (CheckBox) item.FindControl ("chkCIEdit");

                if (chkEdit.Checked)
                {
                    int messageId = Convert.ToInt32 (((HtmlInputHidden) item.FindControl ("hidCommInviteMsgId")).Value);
                    
                    ret = UsersUtility.DeleteMessage (GetUserId(), messageId);

                    if (ret == 1)
                        count++;
                }
            }

            if (count > 0)
            {
                BindCommunityInvites (1);
            }

            ShowAlertMsg(count.ToString() + " World invite" + (count != 1 ? "s were" : " was") + " ignored.");
        }
        #endregion -- Methods [Community Invites] --

        #region -- Event Handlers [Community Invites] --
        /// <summary>
        /// rptCommunityInvites_ItemCommand
        /// </summary>
        protected void rptCommunityInvites_ItemCommand (object source, RepeaterCommandEventArgs e)
        {
            string command = e.CommandName;
            HideAlertMsg ();

            int messageId = Convert.ToInt32 (((HtmlInputHidden) e.Item.FindControl ("hidCommInviteMsgId")).Value);
            int communityId = Convert.ToInt32 (((HtmlInputHidden) e.Item.FindControl ("hidCommInviteCommunityId")).Value);

            if (command.Equals ("cmdJoin"))
            {
                try
                {
                    UsersUtility.DeleteMessage (GetUserId(), messageId);
                }
                catch { }
                
                Response.Redirect (ResolveUrl("~/community/commJoin.aspx?communityId=" + communityId + "&join=Y"));
                return;       
            }
            else if (command.Equals ("cmdIgnore"))
            {
                CheckBox chkCREdit = (CheckBox) e.Item.FindControl ("chkCIEdit");
                chkCREdit.Checked = true;

                // Show Confirm Dialog
                AjaxControlToolkit.ModalPopupExtender mpe = (AjaxControlToolkit.ModalPopupExtender) e.Item.FindControl ("mpeCommInvite");
                if (mpe != null)
                {
                    ShowModal (RequestType.COMMUNITY_INV, mpe);
                }
            }
        }
        /// <summary>
        /// btnIgnoreCommInvite_Click
        /// </summary>
        protected void rptCommunityInvites_ItemDataBound (object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                try
                {
                    HtmlAnchor aCommunity = (HtmlAnchor) e.Item.FindControl ("aCommunityName");

                    if (aCommunity != null)
                    {
                        Community community = GetCommunityFacade.GetCommunity ((int) DataBinder.Eval (e.Item.DataItem, "channel_id"));

                        if (community.CommunityId > 0)
                        {
                            aCommunity.InnerText = community.Name;
                            aCommunity.HRef = GetBroadcastChannelUrl (community.NameNoSpaces);
                        }
                    }
                }
                catch { }
            }
        }
        /// <summary>
        /// btnIgnoreCommInvite_Click
        /// </summary>
        protected void btnIgnoreCommInvite_Click (Object sender, EventArgs e)
        {
            bool isItemChecked = false;
            HideAlertMsg ();

            foreach (RepeaterItem dliMessage in rptCommunityInvites.Items)
            {
                CheckBox chkEdit = (CheckBox) dliMessage.FindControl ("chkCIEdit");

                if (chkEdit.Checked)
                {
                    ShowModal (RequestType.COMMUNITY_INV, mpeIgnoreCommInvite);
                    return;
                }
            }

            if (!isItemChecked)
            {
                ShowAlertMsg("No World invites were selected.");
            }
        }
        protected void pagerCommInvites_PageChange (object sender, PageChangeEventArgs e)
        {
            BindCommunityInvites (e.PageNumber);
        }
        #endregion -- Event Handlers [Community Invites] --

        #endregion **** Community Invites ****

        // World Requests
        #region **** World Requests ****

        #region -- Methods [World Requests] --
        /// <summary>
        /// Bind the data
        /// </summary>
        /// <param name="pageNumber"></param>
        private void Bind3DAppRequests (int pageNumber)
        {
            Bind3DAppRequests (pageNumber, "");
        }
        /// <summary>
        /// Bind the data
        /// </summary>
        /// <param name="pageNumber"></param>
        private void Bind3DAppRequests (int pageNumber, string filter)
        {
            CurrentSort = "message_date";
            string orderby = CurrentSort + " " + CurrentSortOrder;
            SetPageTitle ("World Requests");
            HideAlertMsg ();

            if (filter.Length > 0)
            {
                filter += " AND ";
            }
            //filter += " m.type IN (" + (int) Constants.eMESSAGE_TYPE.ALERT + ")";
            filter += " m.type IN (" + (int) Constants.eMESSAGE_TYPE.APP_REQUEST + ")";

            PagedDataTable pds = UsersUtility.GetUserMessages (GetUserId (), filter, orderby, pageNumber, PageSize);

            if (pds.TotalCount > 0)
            {
                rptAppRequests.DataSource = pds;
                rptAppRequests.DataBind ();

                // Set up the pager
                pagerAppRequests.NumberOfPages = Math.Ceiling ((double) pds.TotalCount / PageSize).ToString ();
                pagerAppRequests.CurrentPageNumber = pageNumber;
                pagerAppRequests.DrawControl ();

                // The results
                lblAppRequestsResults.Text = GetResultsText (pds.TotalCount, pageNumber, PageSize, pds.Rows.Count);
                up3DAppRequests.Visible = true;
            }
            else
            {
                up3DAppRequests.Visible = false;
                string msg = "There are currently no outstanding World Requests.";
                string callout = ""; //"Would you like to <a href=\"" + ResolveUrl ("~/people/people.kaneva") + "\">Find Friends</a> or " +
                //             " view <a href=\"javascript:$('" + btnFriendRequests.ClientID + "').click();\">Friend Requests</a>?";
                ShowNoResults (msg, callout);
            }

            // Set nav for Community Requests
            SetNav (RequestType.WORLDS);
        }
        #endregion -- Methods [World Requests] --

        #region -- Event Handlers [World Requests] --
        protected void rptAppRequests_ItemCommand (object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string command = e.CommandName;
                HideAlertMsg ();
                int messageId = 0;
                int fromUserId = 0;
                int blockCommunityId = 0;

                try { messageId = Convert.ToInt32 (((HtmlInputHidden) e.Item.FindControl ("hidAppReqMsgId")).Value); } catch { }
                try { fromUserId = Convert.ToInt32 (((HtmlInputHidden) e.Item.FindControl ("hidAppReqFromUserId")).Value); } catch { }
                try { blockCommunityId = Convert.ToInt32 (((HtmlInputHidden) e.Item.FindControl ("hidAppReqCommunityId")).Value); } catch { }

                if (command.Equals ("Delete"))  // Delete the request
                {
                    if (UsersUtility.DeleteMessage (GetUserId(), messageId) == 1)
                    {   // Success
                        try {
                            ((HtmlContainerControl) e.Item.FindControl ("reqButtons")).Style.Add ("display", "none");
                            ((HtmlContainerControl) e.Item.FindControl ("reqIgnoredMsg")).Style.Add ("display", "block");
                            ((HtmlContainerControl) e.Item.FindControl ("divOptions")).Style.Add ("display", "block");
                        } catch { }

                        // Update the count in the nav
                        Get3DAppRequestCount ();
                    }
                    else
                    {
                        ShowAlertMsg ("An error was encountered while trying to delete the request.");
                    }
                }
                else if (command.Equals ("BlockUser")) // Block the sender of request
                {
                    // Can't block yourself
                    if (GetUserId ().Equals (fromUserId))
                    {
                        ShowAlertMsg ("You cannot block yourself.");
                        return;
                    }

                    if (GetUserFacade.AddBlastBlock (GetUserId (), fromUserId, blockCommunityId) == 1)
                    {
                        try
                        {
                            UsersUtility.DeleteMessage (GetUserId (), messageId);
                            // Update the count in the nav
                            Get3DAppRequestCount ();
                            Bind3DAppRequests (1);
                        }
                        catch { }

                        ShowAlertMsg ("Requests from the user have been blocked.");
                    }
                    else
                    {
                        ShowAlertMsg ("An error was encountered while trying to block this World.");
                    }
                }
                else if (command.Equals ("BlockApp"))  // Block the World
                {
                    if (GetUserFacade.AddBlastBlock (GetUserId (), 0, blockCommunityId) == 1)
                    {
                        try
                        {
                            UsersUtility.DeleteMessage (GetUserId (), messageId);
                            // Update the count in the nav
                            Get3DAppRequestCount ();
                            Bind3DAppRequests (1);
                        }
                        catch { }

                        ShowAlertMsg ("This World blasts have been blocked.");
                    }
                    else
                    {
                        ShowAlertMsg ("An error was encountered while trying to block this World.");
                    }
                }
                else if (command.Equals ("Accept"))  // Accept & Go 3D
                {
                    if (e.CommandArgument != null && e.CommandArgument.ToString ().Length > 0 && e.CommandArgument.ToString ().IndexOf ("playwok.aspx") > 0)
                    {
                        // Delete the request
                        try
                        {
                            UsersUtility.DeleteMessage (GetUserId (), messageId);
                            // Update the count in the nav
                            Get3DAppRequestCount ();
                        }
                        catch { }
                        
                        Response.Redirect (Server.HtmlDecode (e.CommandArgument.ToString ()));
                    }
                    else // Add default link to WOK
                    {
                        // some older messages did not set the chanel_id so this value could be null in the db and
                        // 0 here.  If so then we will just send them in world to their home.
                        if (blockCommunityId > 0)
                        {
                            try
                            {
                                Game game = GetGameFacade.GetGameByGameId (GetCommunityFacade.GetCommunityGameId (blockCommunityId));

                                if (game.GameId > 0)
                                {
                                    Response.Redirect (ResolveUrl ("~/kgp/playwok.aspx") + "?goto=U" + game.GameId + "&ILC=MM3D&link=private");
                                }
                            }
                            catch { }  // do nothing, want to fall through to redirect below
                        }
                        
                        // Generic catch all if we can't figure out what game to send them to
                        Response.Redirect (ResolveUrl ("~/kgp/playwok.aspx"));
                    }
                }
            }
            catch (Exception ex)
            {
                m_logger.Error ("Error encountered trying to process ItemCommand. " + ex);
                ShowAlertMsg ("An error was encountered while trying to process your request.");
            }
        }
        protected void rptAppRequests_ItemDataBound (object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                try
                {
                    HtmlContainerControl divMsg = (HtmlContainerControl) e.Item.FindControl ("divMsg");
                    divMsg.InnerHtml = (DataBinder.Eval (e.Item.DataItem, "message")).ToString ();
                    
                    Game game = GetGameFacade.GetGameByGameId (GetCommunityFacade.GetCommunityGameId (Convert.ToInt32 (DataBinder.Eval (e.Item.DataItem, "channel_id"))));
                    LinkButton btnBlockApp = (LinkButton) e.Item.FindControl ("btnBlockApp");

                    if (game != null && game.GameId > 0)
                    {
                        // Get the Accept button
                        LinkButton btnAcceptGo3D = (LinkButton) e.Item.FindControl ("btnAcceptGo3D");

                        // Find the inline Accept link
                        string blast = (DataBinder.Eval (e.Item.DataItem, "message")).ToString ();
                        int indx = blast.IndexOf ("aAccept");
                        if (indx > 0)
                        {
                            // Parse out the anchor tag href
                            string strHref = "href=";
                            int startIndx = blast.IndexOf (strHref, indx + strHref.Length);
                            int len = blast.IndexOf ('"', startIndx + strHref.Length + 1) - (startIndx + strHref.Length);
                            string href = blast.Substring (startIndx + strHref.Length+1, len-1);

                            // Add Javascript to anchor tag
                            blast = blast.Replace (href, "javascript:void(0);\" style=\"display:none;");

                            btnAcceptGo3D.CommandArgument = Server.HtmlEncode (href);
                        }
                        else
                        {
                            // Generate link for Accept & Go 3D button
                            btnAcceptGo3D.CommandArgument = game.GameId.ToString();
                        }
                        
                        divMsg.InnerHtml = blast;
                    }
                    else
                    {
                        btnBlockApp.Visible = false;
                    }
                }
                catch { }
            }
        }
        /// <summary>
        /// Page Change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void pagerAppRequests_PageChange (object sender, PageChangeEventArgs e)
        {
            Bind3DAppRequests (e.PageNumber);
        }
        #endregion -- Event Handlers [World Requests] --

        #endregion **** World Requests ****

        // Activities
        private void BindActivities (int pageNumber, bool myActivities)
        {
            SetPageTitle ("My " + (myActivities ? "" : "Friends ") + "Activities");
            HideAlertMsg ();
            
            if (ucActivities.BindNotifications (pageNumber, myActivities) > 0) //pds.TotalCount > 0)
            {
                upActivities.Visible = true;

                // Set up the pager
  //              pagerAppRequests.NumberOfPages = Math.Ceiling ((double) pds.TotalCount / PageSize).ToString ();
  //              pagerAppRequests.CurrentPageNumber = pageNumber;
  //              pagerAppRequests.DrawControl ();

                // The results
  //              lblAppRequestsResults.Text = GetResultsText (pds.TotalCount, pageNumber, PageSize, pds.Rows.Count);
  //              up3DAppRequests.Visible = true;
            }
            else
            {
                up3DAppRequests.Visible = false;
                string msg = "There are currently no activity items to display.";
                string callout = ""; //"Would you like to <a href=\"" + ResolveUrl ("~/people/people.kaneva") + "\">Find Friends</a> or " +
                //             " view <a href=\"javascript:$('" + btnFriendRequests.ClientID + "').click();\">Friend Requests</a>?";
                ShowNoResults (msg, callout);
            }

            // Set nav for Community Requests
            if (myActivities)
            {
                SetNav (RequestType.ACTIVITY);
            }
            else
            {
                SetNav (RequestType.ACTIVITY_FRIEND);
            }
        }


        #region Summary Counts
        private void GetAllCounts ()
        {
            GetMessageCount ();
            GetFriendRequestCount ();
            GetCommunitySummaryCount ();
            GetCommunityRequestsCount ();
            GetCommunityInviteCount ();
            Get3DAppRequestCount ();
            GetActivityCount ();
        }
        private void GetMessageCount ()
        {
            int cnt = 0;
            spnNewMessages.Visible = false;

            try
            {
                cnt = KanevaWebGlobals.CurrentUser.Stats.NumberOfNewMessages;
            }
            catch { }

            if (cnt > 0)
            {
                spnNewMessages.InnerText = "(" + cnt.ToString () + ")";
                spnNewMessages.Visible = true;
            }
        }
        private void GetFriendRequestCount ()
        {
            int cnt = 0;
            spnNewFriendRequests.Visible = false;

            try
            {
                cnt = KanevaWebGlobals.CurrentUser.Stats.NumberOfRequests;
            }
            catch { }

            if (cnt > 0)
            {
                spnNewFriendRequests.InnerText = "(" + cnt.ToString () + ")";
                spnNewFriendRequests.Visible = true;
            }
        }
        private int GetCommunitySummaryCount ()
        {
            double cnt = 0;
            
            try
            {
                cnt = CommunityUtility.GetPendingMembersCount (KanevaWebGlobals.CurrentUser.UserId) +
                    KanevaWebGlobals.CurrentUser.Stats.NumberCommunityInvites;
            }
            catch { }

            return Convert.ToInt32(cnt);
        }
        private void GetCommunityRequestsCount ()
        {
            double cnt = 0;
            approval.Visible = false;

            try
            {
                cnt = CommunityUtility.GetPendingMembersCount (KanevaWebGlobals.CurrentUser.UserId);
            }
            catch { }

            if (cnt > 0)
            {
                spnNewCommunityRequests.InnerText = "  (" + cnt.ToString () + ")";
                spnNewCommunityRequests.Visible = true;
                approval.Visible = true;
            }
        }
        private void GetCommunityInviteCount()
        {
            int cnt = 0;
            spnNewCommunityInvites.Visible = false;

            try
            {
                cnt = KanevaWebGlobals.CurrentUser.Stats.NumberCommunityInvites;
            }
            catch { }

            if (cnt > 0)
            {
                spnNewCommunityInvites.InnerText = "(" + cnt.ToString() + ")";
                spnNewCommunityInvites.Visible = true;
            }
        }
        private void GetWorldsSummaryCount()
        {
            double cnt = 0;

            try
            {
                cnt = KanevaWebGlobals.CurrentUser.Stats.Number3DAppRequests + GetCommunitySummaryCount ();
            }
            catch { }
        }
        private void Get3DAppRequestCount ()
        {
            int cnt = 0; 
            spnNew3DAppRequests.Visible = false;

            try
            {
                cnt = KanevaWebGlobals.CurrentUser.Stats.Number3DAppRequests;
            }
            catch { }

            if (cnt > 0)
            {
                spnNew3DAppRequests.InnerText = "(" + cnt.ToString () + ")";
                spnNew3DAppRequests.Visible = true;
            }
        }
        private void GetActivityCount ()
        { }
        #endregion Summary Counts

        #region Common 
        
        #region -- Methods --
        protected string ShowAge (int age)
        {
            return (age < 18 ? "< 18" : age.ToString ());
        }
        protected void HideAllPanels ()
        {
            HideAlertMsg ();
            upMessages.Visible = false;
            upMessagesSent.Visible = false;
            upMessageDetails.Visible = false;
            upMessageEditor.Visible = false;
            divFriendRequests.Style.Add ("display", "none");
            upFriendRequestsAccepted.Visible = false;
            divFriendRequestsSent.Style.Add ("display", "none");
            upCommunityRequests.Visible = false;
            upCommunityRequestsAccepted.Visible = false;
            upCommunityInvites.Visible = false;
            up3DAppRequests.Visible = false;
            upActivities.Visible = false;
        }
        public void HideAlertMsg ()
        {
            divAlertMsg.InnerText = "";
            divAlertMsg.Visible = false;
            upNoResults.Visible = false;
        }
        public void SetPageTitle (string title)
        {
            litPageName.Text = title;
            divBackButton.Visible = true;
        }
        protected void ShowAlertMsg (string msg)
        {
            ShowAlertMsg (msg, true);
        }
        protected void ShowAlertMsg (string msg, bool setTimer)
        {
            divAlertMsg.InnerText = msg;
            divAlertMsg.Visible = true;

            litJS.Text = "<script type=\"text/javascript\">ShowMsg_('divAlertContainer', 5000, " + 
                setTimer.ToString().ToLower() + ");</script>";
        }
        public void ShowNoResults (string msg, string callout)
        {
            divNoResultMsg.InnerHtml = msg;
            divNoResultCallout.InnerHtml = callout;
            upNoResults.Visible = true;
        }
        protected void ShowModal (RequestType type, AjaxControlToolkit.ModalPopupExtender mpe)
        {
            switch (type)
            {
                case RequestType.MESSAGES:
                    btnOkConfirm.CommandName = RequestType.MESSAGES.ToString ();
                    divConfirmMessage.InnerText = "Are you sure you want to delete this Message?";
                    mpe.Show ();
                    break;
                case RequestType.MESSAGES_SENT:
                    btnOkConfirm.CommandName = RequestType.MESSAGES_SENT.ToString ();
                    divConfirmMessage.InnerText = "Are you sure you want to delete this Sent Message?";
                    mpe.Show ();
                    break;
                case RequestType.MESSAGE_DETAIL:
                    btnOkConfirm.CommandName = RequestType.MESSAGE_DETAIL.ToString ();
                    divConfirmMessage.InnerText = "Are you sure you want to delete this Message?";
                    mpe.Show ();
                    break;
                case RequestType.FRIEND_REQ:
                    btnOkConfirm.CommandName = RequestType.FRIEND_REQ.ToString ();
                    divConfirmMessage.InnerText = "Are you sure you want to ignore this Friend Request?";
                    mpe.Show ();
                    break;
                case RequestType.COMMUNITY_REQ:
                    btnOkConfirm.CommandName = RequestType.COMMUNITY_REQ.ToString ();
                    divConfirmMessage.InnerText = "Are you sure you want to ignore this World Approval?";
                    mpe.Show ();
                    break;
                case RequestType.COMMUNITY_INV:
                    btnOkConfirm.CommandName = RequestType.COMMUNITY_INV.ToString ();
                    divConfirmMessage.InnerText = "Are you sure you want to ignore this World Invite?";
                    mpe.Show ();
                    break;
                case RequestType.WORLDS:
                    btnOkConfirm.CommandName = RequestType.WORLDS.ToString ();
                    divConfirmMessage.InnerText = "Are you sure you want to ignore this World Request?";
                    mpe.Show ();
                    break;
                case RequestType.BlOCK_USER:
                    btnOkConfirm.CommandName = RequestType.BlOCK_USER.ToString ();
                    divConfirmMessage.InnerText = "Are you sure you want to block this member?";
                    mpe.Show ();
                    break;
                case RequestType.SPAM:
                     btnOkConfirm.CommandName = RequestType.SPAM.ToString ();
                    divConfirmMessage.InnerHtml = "Are you sure you want to report this as Spam?<br/>This member will also be blocked.";
                    mpe.Show ();
                    break;
            }
        }
        protected void ConfigAllNavButtons ()
        {
            // Messages
            btnNavMessages.CommandName = NavType.MSG.ToString ();
            btnNavMyMessages.CommandName = NavType.MSG.ToString ();
            btnNavSentMessages.CommandName = NavType.MSG_SENT.ToString ();
            // Friends
            btnNavFriends.CommandName = NavType.FR_REQ.ToString ();
            btnNavFriendInvites.CommandName = NavType.FR_REQ.ToString ();
            btnFriendInvitesSent.CommandName = NavType.FR_REQ_SENT.ToString ();
            // Community
            btnNavCommunityRequests.CommandName = NavType.COMM_REQ.ToString ();
            btnNavCommunityInvites.CommandName = NavType.COMM_INV.ToString ();
            // 3D Apps
            btnNav3DAppRequests.CommandName = NavType.APP_REQ.ToString ();
            // Activities
            btnNavActivity.CommandName = NavType.ACT_FR.ToString ();
            btnNavActivityMine.CommandName = NavType.ACT_MY.ToString ();
            btnNavActivityFriends.CommandName = NavType.ACT_FR.ToString ();

            // Inline buttons
            btnFriendRequests.CommandName = NavType.FR_REQ.ToString ();
            btnFriendRequestsSent.CommandName = NavType.FR_REQ_SENT.ToString ();
        }
        private void LoadInitialPage ()
        {
            string page = String.IsNullOrEmpty (Request["mc"]) ? "" : Request["mc"].ToString ();

            switch (page)
            {
                case "msg": // Messages
                    BindMessages (1);
                    break;
                case "ms":  // Messages Sent
                    BindMessagesSent (1);
                    break;
                case "me":  // Message Editor
                    ConfigMessageEditor ();
                    break;
                case "md":  // Message Details
                    ConfigMessageDetailsAsInitial();
                    break;
                case "fr":  // Friend requests
                    BindFriendRequests (1);
                    break;
                case "frs": // Community requests
                    BindFriendRequestsSent (1);
                    break;
                case "cr":  // Community requests
                    BindCommunityRequests (1);
                    break;
                case "ar":  // App (3D) requests
                    Bind3DAppRequests (1);
                    break;
                case "ci":  // Community invites
                    BindCommunityInvites (1);
                    break;
                case "act": // Activities
                    BindActivities (1, false);
                    break;
                default:
                    BindMessages (1);
                    break;
            }
        }
        private void ResetSideNav ()
        {
            // -- Clear any class names
            // Messages
            SetNavClassName (btnNavMyMessages, "");
            SetNavClassName (btnNavSentMessages, "");
            // Friends
            SetNavClassName (btnNavFriendInvites, "");
            // Community
            SetNavClassName (btnNavCommunityRequests, "");
            SetNavClassName(btnNavCommunityInvites, "");
            // 3D Apps
            SetNavClassName (btnNav3DAppRequests, "");
            // Activities
            SetNavClassName (btnNavActivityMine, "");
            SetNavClassName (btnNavActivityFriends, "");

            // -- Collapse and expanded sub navs
            SetNavVisibility (subNavMessages, "none");
            SetNavVisibility (subNavFriends, "none");
            SetNavVisibility (subNavCommunity, "none");
            SetNavVisibility (subNavActivity, "none");

            SetNavSectionClassName (btnNavMessages, "");
            SetNavSectionClassName (btnNavFriends, "");
            SetNavSectionClassName (btnNavActivity, "");
        }
        private void SetNavClassName (LinkButton navObj, string className)
        {
            ((HtmlContainerControl) navObj.Parent).Attributes.Add ("class", className);    
        }
        private void SetNavSectionClassName (LinkButton navObj, string className)
        {
            navObj.Attributes.Add ("class", className);
        }
        private void SetNavVisibility (HtmlGenericControl navObj, string displayType)
        {
            navObj.Attributes.CssStyle.Add ("display", displayType);
        }
        private void SetNav (RequestType rt)
        {
            ResetSideNav ();
            GetCommunitySummaryCount ();

            switch (rt)
            {
                case RequestType.MESSAGES:
                    // Side Nav
                    SetNavVisibility (subNavMessages, "block");
                    SetNavClassName (btnNavMyMessages, "selected");
                    SetNavSectionClassName (btnNavMessages, "selected");
                    break;
                case RequestType.MESSAGES_SENT:
                    // Side Nav
                    SetNavVisibility (subNavMessages, "block");
                    SetNavClassName (btnNavSentMessages, "selected");
                    SetNavSectionClassName (btnNavMessages, "selected");
                    break;
                case RequestType.MESSAGE_DETAIL:
                case RequestType.MESSAGE_EDITOR:
                    // Side Nav
                    SetNavVisibility (subNavMessages, "block");
                    SetNavSectionClassName (btnNavMessages, "selected");
                    break;
                case RequestType.WORLDS:
                    // Side Nav
                    SetNavClassName (btnNav3DAppRequests, "selected");
                    break;
                case RequestType.COMMUNITY_REQ:
                    // Side Nav
                    SetNavClassName (btnNavCommunityRequests, "selected");
                    break;
                case RequestType.COMMUNITY_INV:
                    // Side Nav
                    SetNavClassName(btnNavCommunityInvites, "selected");
                    break;
                case RequestType.FRIEND_REQ:
                    // Side Nav
                    SetNavVisibility (subNavFriends, "block");
                    SetNavClassName (btnNavFriendInvites, "selected");
                    SetNavSectionClassName (btnNavFriends, "selected");
                    break;
                case RequestType.FRIEND_REQ_SENT:
                    // Side Nav
                    SetNavVisibility (subNavFriends, "block");
                    SetNavSectionClassName (btnNavFriends, "selected");
                    break;
                case RequestType.ACTIVITY:
                    // Side Nav
                    SetNavVisibility (subNavActivity, "block");
                    SetNavClassName (btnNavActivityMine, "selected");
                    SetNavSectionClassName (btnNavActivity, "selected");
                    break;
                case RequestType.ACTIVITY_FRIEND:
                    // Side Nav
                    SetNavVisibility (subNavActivity, "block");
                    SetNavClassName (btnNavActivityFriends, "selected");
                    SetNavSectionClassName (btnNavActivity, "selected");
                    break;
            }
        }
        /// <summary>
        /// FormatDateOnly
        /// </summary>
        public string FormatDateOnly (object date)
        {
            DateTime dt = (DateTime) date;

            return dt.ToString ("MMMM") + " " + dt.Day.ToString () + ", " + dt.Year.ToString (); // FormatDate(dt);
        }
        #endregion -- Methods --

        #region -- Event Handlers --
        protected void btnBack_Click (Object sender, CommandEventArgs e)
        {
            HideAllPanels ();

            switch (e.CommandName)
            {
                case "FriendRequests":
                    BindFriendRequests (1);
                    break;
                case "CommunityRequests":
                    BindCommunityRequests (1);
                    break;
                default:
                    BindFriendRequests (1);
                    break;
            }
        }
        protected void btnOkConfirm_Click (Object sender, CommandEventArgs e)
        {
                // Messages
            if (e.CommandName.Equals (RequestType.MESSAGES.ToString ()))
            {
                DeleteMessages ();
            }
                // Sent Messages
            else if (e.CommandName.Equals (RequestType.MESSAGES_SENT.ToString ()))
            {
                DeleteMessagesSent ();
            }
            else if (e.CommandName.Equals (RequestType.MESSAGE_DETAIL.ToString ()))
            {
                DeleteMessageDetail ();
            }
                // Friend Requests
            else if (e.CommandName.Equals (RequestType.FRIEND_REQ.ToString ()))
            {
                DeleteFriendRequests ();
            }
                // Community Requests
            else if (e.CommandName.Equals (RequestType.COMMUNITY_REQ.ToString ()))
            {
                DeleteCommunityRequests ();
            }
                // Community Invites
            else if (e.CommandName.Equals (RequestType.COMMUNITY_INV.ToString ()))
            {
                DeleteCommunityInvites ();
            }
                // Block User
            else if (e.CommandName.Equals (RequestType.BlOCK_USER.ToString ()))
            {
                BlockKanevaUser ();
            }
                // Report Message as Spam
            else if (e.CommandName.Equals (RequestType.SPAM.ToString ()))
            {
                ReportSpamMessage ();
            }
        }
        protected void btnCPNav_Click (Object sender, CommandEventArgs e)
        {
            HideAllPanels ();          
            
            switch ((NavType) Enum.Parse (typeof (NavType), (e.CommandName)))
            {
                case NavType.MSG:
                    BindMessages (1);
                    break;
                case NavType.MSG_SENT:
                    BindMessagesSent (1);
                    break;
                case NavType.FR_REQ:
                    BindFriendRequests (1);
                    break;
                case NavType.FR_REQ_SENT:
                    BindFriendRequestsSent (1);
                    break;
                case NavType.COMM_REQ:
                    BindCommunityRequests (1);
                    break;
                case NavType.COMM_INV:
                    BindCommunityInvites (1);
                    break;
                case NavType.APP_REQ:
                    Bind3DAppRequests (1);
                    break;
                case NavType.ACT_MY:
                    BindActivities (1, true);
                    break;
                case NavType.ACT_FR:
                    BindActivities (1, false);
                    break;
                default:
                    break;
            }
        }
        #endregion -- Event Handlers --

        protected enum RequestType
        {
            MESSAGES,
            MESSAGES_SENT,
            MESSAGE_DETAIL,
            MESSAGE_EDITOR,
            FRIEND_REQ,
            FRIEND_REQ_SENT,
            COMMUNITY_REQ,
            COMMUNITY_INV,
            WORLDS,
            ACTIVITY,
            ACTIVITY_FRIEND,
            BlOCK_USER,
            SPAM
        }
        protected enum NavType
        {
            MSG,
            MSG_SENT,
            FR_REQ,
            FR_REQ_SENT,
            COMM_REQ,
            COMM_INV,
            APP_REQ,
            ACT_MY,
            ACT_FR
        }
        #endregion Common

        #region Declerations

        const int PageSize = 10;

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

        #endregion Declerations

        #region Web Form Designer generated code

        override protected void OnInit (EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent ();
            base.OnInit (e);
        }
        
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent ()
        {
            pagerMessages.PageChanged += new PageChangeEventHandler (pagerMessages_PageChange);
            pagerMessagesSent.PageChanged += new PageChangeEventHandler (pagerMessagesSent_PageChange);
            pagerFriendReq.PageChanged += new PageChangeEventHandler (pagerFriendReq_PageChange);
            pagerFriendReqSent.PageChanged += new PageChangeEventHandler (pagerFriendReqSent_PageChange);
            pagerCommunityReq.PageChanged += new PageChangeEventHandler (pagerCommunityReq_PageChange);
            pagerAppRequests.PageChanged += new PageChangeEventHandler (pagerAppRequests_PageChange);
            pagerCommInvites.PageChanged += new PageChangeEventHandler (pagerCommInvites_PageChange);
        }
        #endregion

    }
}