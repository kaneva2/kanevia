///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for creditHistory.
	/// </summary>
	public class creditHistory : SortedBasePage
	{
		protected creditHistory () 
		{
			Title = "My Credit Card Purchases";
		}

		private void Page_Load (object sender, System.EventArgs e)
		{
			if (!KanevaGlobals.EnableCheckout)
			{
				if (!IsAdministrator())
				{
                    Response.Redirect("~/error.aspx?error=" + (int)Constants.eERROR_TYPE.CHECKOUT_CLOSED);
                }
			}

			// They must be logged in
			if (!Request.IsAuthenticated)
			{
				Response.Redirect (GetLoginURL ());
				return;
			}

			if (!IsPostBack)
			{
				BindCreditTransactionData (1, GetUserId ());		
			}

			// Set navigation
            HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
			HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.ACCOUNT;
			HeaderNav.MyAccountNav.ActiveTab = NavAccount.TAB.CREDITS;
			HeaderNav.SetNavVisible(HeaderNav.MyKanevaNav,2);
			HeaderNav.SetNavVisible(HeaderNav.MyAccountNav);
		}
		
		#region Helper Methods
		
		/// <summary>
		/// Bind the data
		/// </summary>
		/// <param name="pageNumber"></param>
		private void BindCreditTransactionData (int pageNumber, int userId)
		{
			// Set the sortable columns
			string orderby = CurrentSort + " " + CurrentSortOrder;
			string filter =  " o.transaction_status_id = " + ((int) Constants.eORDER_STATUS.COMPLETED).ToString ();

			PagedDataTable pds = StoreUtility.GetUserCreditTransactions (userId, filter, orderby, pageNumber, KanevaGlobals.CreditTransactionsPerPage);
			rptOrders.DataSource = pds;
			rptOrders.DataBind ();
		}

		public string DecryptString(object obj)
		{
            if (obj != null && obj.ToString().Length > 0)
            {
			    return KanevaGlobals.Decrypt(obj.ToString ());
            }
            else
            {
                return "";
            }
		}
		
		public string FormatCredits(object obj)
		{
			return (Convert.ToUInt64(obj)).ToString ("N0");
		}

        public string FormatPaymentType (object objName, object objCC)
        {
            if (objName.ToString ().ToLower () == "paypal")
            {
                return objName.ToString ();
            }
            else
            {
                return objName.ToString () + "<br/>xxxx-xxxx-xxxx-" + DecryptString (objCC.ToString ());
            }
        }
		#endregion
		
		#region Properties

		/// <summary>
		/// DEFAULT_SORT
		/// </summary>
		/// <returns></returns>
		protected override string DEFAULT_SORT
		{
			get
			{
				return "ppt.point_transaction_id";
			}
		}

		/// <summary>
		/// DEFAULT_SORT_ORDER
		/// </summary>
		/// <returns></returns>
		protected override string DEFAULT_SORT_ORDER
		{
			get
			{
				return "DESC";
			}
		}

		
		#endregion

		#region Declerations

		protected Repeater		rptOrders;

		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
