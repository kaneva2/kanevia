///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for selectPhoto.
	/// </summary>
	public class selectPhoto : NoBorderPage
	{
		protected selectPhoto () 
		{
			Title = "Select a photo";
		}

		private void Page_Load (object sender, System.EventArgs e)
		{
			if (!IsPostBack)
			{
				BindData( KanevaWebGlobals.GetUserId() );
			}
		}

		/// <summary>
		/// BindData
		/// </summary>
		private void BindData (int userId)
		{
			int channelId = GetPersonalChannelId ();

			// Groups
			ddlGroup.Items.Add( new ListItem( "All", "-1" ) );

			if ( userId != -1 )
			{
                PagedList<AssetGroup> pdtAssetGroups = GetMediaFacade.GetAssetGroups(channelId, "", "", 1, Int32.MaxValue);
				if (pdtAssetGroups.Count > 0)
				{
					for (int i = 0; i < pdtAssetGroups.Count; i++)
					{
						ddlGroup.Items.Add (new ListItem (pdtAssetGroups[i].Name, pdtAssetGroups[i].AssetGroupId.ToString()));
					}
				}
			}

			int group_id = Convert.ToInt32( ddlGroup.SelectedValue );
			BindPictures( group_id, userId, channelId );
		}

		private void BindPictures( int group_id, int user_id, int channelId )
		{
			PagedDataTable pds;

			// if the group id is valid, get pictures for that group
			if ( group_id != -1 )
			{
				string orderby = "asset_id" + " " + "ASC";

				pds = StoreUtility.GetAssetsInGroup( group_id, channelId, (int) Constants.eASSET_TYPE.PICTURE, orderby, "", 1, Int32.MaxValue );
			}
				// otherwise get all pics
			else
			{
				string orderby = "created_date" + " " + "ASC";
				string filter = "a.status_id <> " + (int) Constants.eASSET_STATUS.MARKED_FOR_DELETION;
				int assetTypeId = (int) Constants.eASSET_TYPE.PICTURE;

				pds = StoreUtility.GetAssetsInChannel (channelId, true, true, assetTypeId, filter, orderby, 1, Int32.MaxValue);
			}

			dlPictures.DataSource = pds;
			dlPictures.DataBind();
		}

		/// <summary>
		/// dlPictures_ItemDataBound
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void dlPictures_ItemDataBound (object sender, DataListItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
			{
				DataRowView drv						= (DataRowView) e.Item.DataItem;
				HtmlGenericControl spnImageTitle	= (HtmlGenericControl) e.Item.FindControl("spnImageTitle");

				if ( spnImageTitle != null )
				{
					spnImageTitle.InnerText = TruncateWithEllipsis (drv ["name"].ToString() , 200 );
				}
			}
		}

		/// <summary>
		/// ddlGroup_SelectedIndexChanged
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void ddlGroup_SelectedIndexChanged (object sender, EventArgs e)
		{
			int group_id = Convert.ToInt32 (ddlGroup.SelectedValue );

			BindPictures (group_id, KanevaWebGlobals.GetUserId (), GetPersonalChannelId ());
		}

		/// <summary>
		/// btnSave_Click
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnSave_Click (object sender, EventArgs e)
		{
			string tbName = "txtBGPicture";
			int assetId = Convert.ToInt32 (ihSinglePictureId.Value);

			if (Request ["tbName"] != null)
			{
				tbName = Request ["tbName"].ToString ();
			}

			// If successfull, send javascript to update mykaneva page, close the window
            if (!ClientScript.IsClientScriptBlockRegistered(GetType (), "startup"))
			{
				DataRow drAsset = StoreUtility.GetAsset (assetId);
				string javascript = "<script language=JavaScript>window.opener.document.frmMain." + tbName + ".value='" + StoreUtility.GetPhotoImageURL (drAsset ["image_full_path"].ToString (), "la") + "';window.close();</script>";
                ClientScript.RegisterStartupScript(GetType(), "startup", javascript);
			}
		}

		protected Button			btnSave;
		protected DropDownList		ddlGroup;
		protected DataList			dlPictures;
		protected HtmlInputHidden	ihSinglePictureId;

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
			this.dlPictures.ItemDataBound += new DataListItemEventHandler(dlPictures_ItemDataBound);
			this.ddlGroup.SelectedIndexChanged += new EventHandler(ddlGroup_SelectedIndexChanged);
			this.btnSave.Click += new EventHandler(this.btnSave_Click);
		}
		#endregion
	}
}
