<%@ Page language="c#" Codebehind="giftMessageDetails.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.giftMessageDetails" %>
<link href="../css/home.css" rel="stylesheet" type="text/css">
	<link href="../css/kanevaSC.css" rel="stylesheet" type="text/css">
		<link href="../css/friends.css" rel="stylesheet" type="text/css">
			<link href="../css/kanevaBroadBand.css" type="text/css" rel="stylesheet">
				<link href="../css/kanevaText.css" rel="stylesheet" type="text/css">
					<br>
					<table runat="server" id="tblNoAccess" Visible="False" cellpadding="25" cellspacing="1"
						border="0" width="900">
						<tr>
							<td class="bodyText" align="left"><br>
								We are sorry, this message is currently unavailable on the Kaneva site. The 
								message is either
								<br>
								<br>
								1. Deleted from the system<br>
								2. You are not the message recipient<br>
								3. The URL to the item is not correct
								<br>
								<br>
								<br>
								<span class="dateStamp">NOTE: If you believe you are receiving this page in error, please contact support via the <a runat="server" href="~/suggestions.aspx?mode=F&amp;category=Feedback" class="showingCount"
										ID="A1">Support Page</a></span>
								<br>
								<br>
								<br>
							</td>
						</tr>
					</table>
					<asp:panel runat="server" ID="pnlMessage" Visible="True">
						<asp:ValidationSummary class="formError" id="valSum" runat="server" HeaderText="Please correct the following errors:"
							DisplayMode="BulletList" ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
						<TABLE cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
							<TR>
								<TD>
									<TABLE cellSpacing="0" cellPadding="0" width="990" align="center" border="0">
										<TR>
											<TD vAlign="top">
												<TABLE cellSpacing="0" cellPadding="0" width="990" border="0">
													<TR>
														<TD class="frTopLeft"></TD>
														<TD class="frTop"></TD>
														<TD class="frTopRight"></TD>
													</TR>
													<TR>
														<TD class="frBorderLeft"><IMG height="1" src="~/images/spacer.gif" width="1" runat="server"></TD>
														<TD class="frBgIntMembers" vAlign="top">
															<TABLE class="boxInside" cellSpacing="0" cellPadding="0" width="100%" border="0">
																<TR>
																	<TD class="boxInsideTopLeft"><IMG height="4" src="~/images/spacer.gif" width="4" runat="server"></TD>
																	<TD class="boxInsideTop"><IMG height="1" src="~/images/spacer.gif" width="4" runat="server"></TD>
																	<TD class="boxInsideTopRight"><IMG height="4" src="~/images/spacer.gif" width="4" runat="server"></TD>
																</TR>
																<TR class="boxInside">
																	<TD class="boxInsideleft"><IMG height="4" src="~/images/spacer.gif" width="1" runat="server"></TD>
																	<TD class="boxInsideContent">
																		<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																			<TR>
																				<TD><IMG height="4" src="~/images/spacer.gif" width="6" runat="server"></TD>
																				<TD><IMG height="4" src="~/images/spacer.gif" width="6" runat="server"></TD>
																				<TD><IMG height="4" src="~/images/spacer.gif" width="6" runat="server"></TD>
																			</TR>
																			<TR>
																				<TD><IMG height="6" src="~/images/spacer.gif" width="6" runat="server"></TD>
																				<TD class="textGeneralGray01" align="left" width="100%"><IMG height="1" src="~/images/spacer.gif" width="2" runat="server">Message 
																					Details</TD>
																				<TD><IMG height="6" src="~/images/spacer.gif" width="6" runat="server"></TD>
																			</TR>
																			<TR>
																				<TD><IMG height="4" src="~/images/spacer.gif" width="6" runat="server"></TD>
																				<TD><IMG height="4" src="~/images/spacer.gif" width="6" runat="server"></TD>
																				<TD><IMG height="4" src="~/images/spacer.gif" width="6" runat="server"></TD>
																			</TR>
																		</TABLE>
																	</TD>
																	<TD class="boxInsideRight"><IMG height="4" src="~/images/spacer.gif" width="1" runat="server"></TD>
																</TR>
																<TR>
																	<TD class="boxInsideBottomLeft"><IMG height="4" src="~/images/spacer.gif" width="4" runat="server"></TD>
																	<TD class="boxInsideBottom"><IMG height="1" src="~/images/spacer.gif" width="4" runat="server"></TD>
																	<TD class="boxInsideBottomRight"><IMG height="4" src="~/images/spacer.gif" width="4" runat="server"></TD>
																</TR>
															</TABLE>
															<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																<TR>
																	<TD class="boxInsideTopLeft"><IMG height="4" src="~/images/spacer.gif" width="4" runat="server"></TD>
																	<TD class="boxInsideTop"><IMG height="1" src="~/images/spacer.gif" width="4" runat="server"></TD>
																	<TD class="boxInsideTopRight"><IMG height="4" src="~/images/spacer.gif" width="4" runat="server"></TD>
																</TR>
																<TR class="boxInside">
																	<TD class="boxInsideleft"><IMG height="4" src="~/images/spacer.gif" width="1" runat="server"></TD>
																	<TD class="boxInsideContent" align="center">
																		<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																			<TR>
																				<TD><IMG height="4" src="~/images/spacer.gif" width="6" runat="server"></TD>
																				<TD><IMG height="4" src="~/images/spacer.gif" width="6" runat="server"></TD>
																				<TD><IMG height="4" src="~/images/spacer.gif" width="6" runat="server"></TD>
																			</TR>
																			<TR>
																				<TD><IMG height="6" src="~/images/spacer.gif" width="6" runat="server"></TD>
																				<TD class="textGeneralGray01" align="left" width="100%"><TABLE cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
																						<TR>
																							<TD class="textGeneralGray01" vAlign="top" align="center" width="50" colSpan="3" rowSpan="3">
																								<TABLE height="90" cellSpacing="0" cellPadding="0" width="94" align="center" border="0">
																									<TR>
																										<TD class="boxWhiteTopLeft"><IMG height="4" src="~/images/spacer.gif" width="4" runat="server"></TD>
																										<TD class="boxWhiteTop"><IMG height="1" src="~/images/spacer.gif" width="1" runat="server"></TD>
																										<TD class="boxWhiteTopRight"><IMG height="4" src="~/images/spacer.gif" width="4" runat="server"></TD>
																									</TR>
																									<TR>
																										<TD class="boxWhiteLeft"><IMG height="4" src="~/images/spacer.gif" width="1" runat="server"></TD>
																										<TD class="boxWhiteContent" vAlign="middle" align="center" height="80"><A id="aFromAvatar" runat="server"><IMG id="imgFromAvatar" border="0" runat="server"></A></TD>
																										<TD class="boxWhiteRight"><IMG height="4" src="~/images/spacer.gif" width="1" runat="server"></TD>
																									</TR>
																									<TR>
																										<TD class="boxWhiteBottomLeft"><IMG height="4" src="~/images/spacer.gif" width="4" runat="server"></TD>
																										<TD class="boxWhiteBottom"><IMG height="1" src="~/images/spacer.gif" width="4" runat="server"></TD>
																										<TD class="boxWhiteBottomRight"><IMG height="4" src="~/images/spacer.gif" width="4" runat="server"></TD>
																									</TR>
																								</TABLE>
																							</TD>
																							<TD class="textGeneralGray01" align="center" width="10" rowSpan="3"></TD>
																							<TD class="textGeneralGray01" align="left">
																								<TABLE class="textGeneralGray01" cellSpacing="0" cellPadding="0" width="100%" border="0">
																									<TR>
																										<TD width="65">From:</TD>
																										<TD width="879">
																											<asp:Hyperlink id="lnkFrom" runat="server"></asp:Hyperlink></TD>
																									</TR>
																									<TR>
																										<TD>Date:</TD>
																										<TD>
																											<asp:Label id="lblDate" runat="server"></asp:Label></TD>
																									</TR>
																									<TR>
																										<TD>Subject:</TD>
																										<TD>
																											<asp:Label id="lblSubject" runat="server"></asp:Label></TD>
																									</TR>
																									<TR>
																										<TD>Gift:</TD>
																										<TD>
																											<asp:Label id="lblGiftName" runat="server"></asp:Label></TD>
																										<TD><IMG id="imgGift" border="0" runat="server"></A></TD>
																									</TR>
																								</TABLE>
																							</TD>
																						</TR>
																						<TR>
																							<TD class="textGeneralGray01">
																								<asp:Label id="lblBody" runat="server"></asp:Label></TD>
																						</TR>
																						<TR>
																							<TD class="textGeneralGray01" align="left">&nbsp;</TD>
																						</TR>
																						<TR>
																							<TD class="bbGrayLine" noWrap colSpan="5"><IMG height="1" src="~/images/spacer.gif" width="1" runat="server"></TD>
																						</TR>
																						<TR>
																							<TD class="textGeneralGray01" noWrap align="right" colSpan="5"><IMG height="8" src="~/images/spacer.gif" width="1" runat="server"></TD>
																						</TR>
																						<TR>
																							<TD class="textGeneralGray01" noWrap align="right" colSpan="5">
																								<asp:button class="Filter2" id="btnAccept" onclick="btnAccept_Click" runat="Server" Text="     accept   "
																									CausesValidation="False"></asp:button>&nbsp;&nbsp;<SPAN id="spanAcceptBlock" runat="server"> 
<asp:button class="Filter2" id="btnCancel" onclick="btnBack_Click" runat="Server" Text="      back      "
																										CausesValidation="False"></asp:button>&nbsp;&nbsp;<SPAN id="spanSpamBlock" runat="server"> 
<asp:button class="Filter2" id="btnSpam" onclick="btnSpam_Click" runat="Server" Text=" report as spam "
																											CausesValidation="False"></asp:button>&nbsp;&nbsp; 
<asp:button class="Filter2" id="btnBlock" onclick="btnBlock_Click" runat="Server" Text=" block this user "
																											CausesValidation="False"></asp:button>&nbsp;&nbsp;</SPAN> 
<asp:button class="Filter2" id="btnDelete" onclick="btnDelete_Click" runat="Server" Text=" delete & reject "
																										CausesValidation="False"></asp:button>&nbsp;&nbsp; 
<asp:button class="Filter2" id="Button1" onclick="btnReply_Click" runat="Server" Text="      reply      "></asp:button><IMG height="6" src="~/images/spacer.gif" width="10" runat="server"></SPAN></TD>
																						</TR>
																						<TR>
																							<TD class="textGeneralGray01" noWrap colSpan="5"><IMG height="1" src="~/images/spacer.gif" width="1" runat="server"></TD>
																						</TR>
																					</TABLE>
																				</TD>
																				<TD><IMG height="6" src="~/images/spacer.gif" width="6" runat="server"></TD>
																			</TR>
																			<TR>
																				<TD><IMG height="4" src="~/images/spacer.gif" width="6" runat="server"></TD>
																				<TD><IMG height="4" src="~/images/spacer.gif" width="6" runat="server"></TD>
																				<TD><IMG height="4" src="~/images/spacer.gif" width="6" runat="server"></TD>
																			</TR>
																		</TABLE>
																	</TD>
																	<TD class="boxInsideRight"><IMG height="4" src="~/images/spacer.gif" width="1" runat="server"></TD>
																</TR>
																<TR>
																	<TD class="boxInsideBottomLeft"><IMG height="4" src="~/images/spacer.gif" width="4" runat="server"></TD>
																	<TD class="boxInsideBottom2"><IMG height="1" src="~/images/spacer.gif" width="4" runat="server"></TD>
																	<TD class="boxInsideBottomRight"><IMG height="4" src="~/images/spacer.gif" width="4" runat="server"></TD>
																</TR>
															</TABLE>
														</TD>
														<TD class="frBorderRight"><IMG height="1" src="~/images/spacer.gif" width="1" runat="server"></TD>
													</TR>
													<TR>
														<TD class="frBottomLeft"></TD>
														<TD class="frBottom"></TD>
														<TD class="frBottomRight"></TD>
													</TR>
													<TR>
														<TD><IMG height="14" src="~/images/spacer.gif" width="1" runat="server"></TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
										<TR>
											<TD vAlign="top">&nbsp;</TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
						</TABLE>
					</asp:panel>
