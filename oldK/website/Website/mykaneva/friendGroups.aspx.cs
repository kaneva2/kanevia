///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva.channel;

using System.Collections.Generic;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for friendGroups.
	/// </summary>
	public class friendGroups : SortedBasePage
	{
		protected friendGroups () 
		{
			Title = "Friend groups";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			if(Request.IsAuthenticated)
			{
				if(GetRequestParams())
				{
					//check if user is allowed to edit this channel
					if(!IsUserAllowedToEdit())
					{
						RedirectToHomePage ();
					}
				}
				else
				{
					//invalid request params
					//if the user logged in, send him to his home page
					//if not, redirect to login page
					RedirectToHomePage ();
				}
			}
			else
			{
				Response.Redirect (this.GetLoginURL ());
			}

			if (!IsPostBack)
			{
				lbSortByName.CssClass = "selected";

				BindData (1, _userId);
			}

			// Set Nav
			HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.MY_KANEVA;
			HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.FRIENDS;
			HeaderNav.MyFriendNav.ActiveTab = NavFriend.TAB.GROUPS;
			HeaderNav.SetNavVisible(HeaderNav.MyKanevaNav,2);
			HeaderNav.SetNavVisible(HeaderNav.MyFriendNav);
		}

		/// <summary>
		/// Bind the data
		/// </summary>
		/// <param name="pageNumber"></param>
		private void BindData (int pageNumber, int userId)
		{
			BindData (pageNumber, userId, "");
		}
		/// <summary>
		/// Bind the data
		/// </summary>
		/// <param name="pageNumber"></param>
		private void BindData (int pageNumber, int userId, string filter)
		{
			searchFilter.CurrentPage = "friendgroups";
			
			// Set the sortable columns
			string orderby = CurrentSort + " " + CurrentSortOrder;
			int pgSize = searchFilter.NumberOfPages;

			PagedDataTable pds = UsersUtility.GetFriendGroups (userId, "", orderby, pageNumber, pgSize);
			dlFriendGroups.DataSource = pds;
			dlFriendGroups.DataBind ();

			pgTop.NumberOfPages = Math.Ceiling ((double) pds.TotalCount / pgSize).ToString ();
			pgTop.DrawControl ();

			// The results
			lblSearch.Text = GetResultsText (pds.TotalCount, pageNumber, pgSize, pds.Rows.Count);
		}

		
		#region Helper Methods
		/// <summary>
		/// GetFriendsInGroupLink
		/// </summary>
		/// <param name="friendGroupId"></param>
		/// <returns></returns>
		protected string GetFriendsInGroupLink (int friendGroupId)
		{
			return ResolveUrl ("~/mykaneva/friendsInGroup.aspx?friendGroupId=" + friendGroupId);
		}
		private bool GetRequestParams()
		{
			bool retVal = true;
			try
			{
				_userId = this.GetUserId();
			}
			catch(Exception)
			{
				//invalid numbers
				retVal = false;
			}
			return retVal;
		}

		/// <summary>
		/// returns true if current user is allowed to edit this channel
		/// </summary>
		/// <returns></returns>
		private bool IsUserAllowedToEdit()
		{
			return IsAdministrator() || _userId == this.GetUserId();
		}

		protected string GetThumbnailImages(int friendGroupId, int numFriendsInGroup)
		{
            UserFacade userFacade = new UserFacade();
            IList<Friend> friends = userFacade.GetFriendsInGroup(friendGroupId, _userId, "com.thumbnail_path IS NOT NULL", "", 1, 9);

			string thumbImages = string.Empty;
			string imgBlank = "~/images/group_folder_blank.gif";
			int i = 0;

            for (i = 0; i < friends.Count; i++)
			{
				thumbImages += "<img src=\"";
                thumbImages += GetProfileImageURL(friends[i].ThumbnailSmallPath, "me", friends[i].Gender);
				thumbImages += "\" border=\"0\" width=\"30\" height=\"23\" style=\"margin:1px;\" />";
			}

			numFriendsInGroup = numFriendsInGroup > 9 ? 9 : numFriendsInGroup;

			while (i < numFriendsInGroup)
			{
				thumbImages += "<img src=\"" + GetProfileImageURL ("", "sm", "m") + "\" border=\"0\" width=\"30\" height=\"23\" style=\"margin:1px;\" />";
                i++;
			}

			while (i < 9)
			{
				thumbImages += "<img src=\"" + ResolveUrl(imgBlank) + "\" border=\"0\" width=\"30\" height=\"23\" style=\"margin:1px;\" />";
                i++;
			}

			return thumbImages;

		}
		#endregion

		#region Event Handlers
		/// <summary>
		/// Click the add button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnAddGroup_Click (object sender, ImageClickEventArgs e)
		{
			trAddGroup.Visible = true;

			BindData (pgTop.CurrentPageNumber, _userId);
		}
		/// <summary>
		/// Click the remove button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnRemove_Click (object sender, ImageClickEventArgs e)
		{
			HtmlInputHidden hidFriendGroupId;
			CheckBox chkEdit;
			int ret = 0;
			int count = 0;
			bool isItemChecked = false;

			// Remove the friends
			foreach (DataListItem dliFriendGroup in dlFriendGroups.Items)
			{
				chkEdit = (CheckBox) dliFriendGroup.FindControl ("chkEdit");

				if (chkEdit.Checked)
				{
					isItemChecked = true;
					
					hidFriendGroupId = (HtmlInputHidden) dliFriendGroup.FindControl ("hidFriendGroupId");
					ret = UsersUtility.DeleteFriendGroup (_userId, Convert.ToInt32 (hidFriendGroupId.Value));
				
					if (ret == 1)
						count++;
				}
			}

			if (isItemChecked)
			{
				if (count > 0)
				{
					BindData (pgTop.CurrentPageNumber, _userId);
				}
				
				if (ret != 1)
				{
					spnAlertMsg.InnerText = "An error was encountered while trying to remove Friend Group.";
				}
				else
				{
					spnAlertMsg.InnerText = "Successfully removed " + count + " friend group" + (count != 1 ? "s" : "") + ".";
				}
			}
			else
			{
				spnAlertMsg.InnerText = "No Friend Groups were selected.";
			}

			MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('spnAlertMsg', 5000);");
		}
		/// <summary>
		/// Click the save button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnSaveG_Click (Object sender, System.EventArgs e) 
		{
			trAddGroup.Visible = false;
			int ret = 0;
			string groupName = inpAG.Value.Trim();

			if (groupName.Length > 0)
			{
				ret = UsersUtility.InsertFriendGroup (_userId, Server.HtmlEncode (inpAG.Value));
				inpAG.Value = "";
			
				if (ret != 1)
				{
					spnAlertMsg.InnerText = "An error was encountered while trying to add Friend Group.";
				}
				else
				{
					spnAlertMsg.InnerText = "Successfully added Friend Group '" + groupName + "'.";
				}

				BindData (pgTop.CurrentPageNumber, _userId);
			}
			else
			{
				spnAlertMsg.InnerText = "Please enter a Friend Group name.";
			}

			MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('spnAlertMsg', 5000);");
		}

		/// <summary>
		/// Click the cancel button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnCancelG_Click (Object sender, System.EventArgs e) 
		{
			trAddGroup.Visible = false;
		}
		protected void btnGo_Click (object sender, ImageClickEventArgs e)
		{}
		/// <summary>
		/// Execute when the user clicks the the view type link button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void lbSortBy_Click (object sender, EventArgs e) 
		{
			lbSortByName.CssClass = lbSortByDate.CssClass = "";

			if (((LinkButton)sender).Attributes["sortby"].ToString().ToLower() == "name")
			{
				CurrentSort = "name";
				lbSortByName.CssClass = "selected";
			}
			else
			{
				CurrentSort = "created_datetime";
				lbSortByDate.CssClass = "selected";							  
			}

			BindData (pgTop.CurrentPageNumber, _userId);
		}
		/// <summary>
		/// Page Change
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pgTop_PageChange (object sender, PageChangeEventArgs e)
		{
			BindData (e.PageNumber, _userId);
		}
		/// <summary>
		/// Execute when the user selects an item from the Store Filter
		/// Items to display drop down list
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void FilterChanged(object sender, FilterChangedEventArgs e)
		{
			searchFilter.SetPagesDropValue(searchFilter.NumberOfPages.ToString());
			
			BindData (1, _userId, e.Filter);
		}
		/// <summary>
		/// They clicked to sort a column
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected override void btnSort_Click (object sender, System.EventArgs e)
		{
			base.btnSort_Click (sender, e);
			BindData (pgTop.CurrentPageNumber, _userId);
		}

		#endregion

		#region Properties
		/// <summary>
		/// DEFAULT_SORT
		/// </summary>
		/// <returns></returns>
		protected override string DEFAULT_SORT
		{
			get
			{
				return "name";
			}
		}

		#endregion

		#region Declerations
		protected Label					lblSearch;
		protected DataList				dlFriendGroups;
		protected LinkButton			lbSortByName, lbSortByDate;
		protected LinkButton			lbSelectAll;
		protected Label					lblResultsTop;
		protected DropDownList			drpFriendGroups;
		protected Button				btnSaveG, btnCancelG;

		protected HtmlContainerControl	spnAlertMsg, spnGN;
		protected HtmlInputText			inpAG;
		protected HtmlTableRow			trAddGroup;

		protected Kaneva.SearchFilter	searchFilter;
		protected Kaneva.Pager			pgTop;

		protected int _userId;

		#endregion

		#region Web Form Designer generated code

		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
			pgTop.PageChanged +=new PageChangeEventHandler (pgTop_PageChange);
			searchFilter.FilterChanged +=new FilterChangedEventHandler (FilterChanged);		
		}	
		#endregion

	}
}