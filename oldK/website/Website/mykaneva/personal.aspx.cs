///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva.usercontrols;
using KlausEnt.KEP.Kaneva;
using log4net;
using MagicAjax;
using Kaneva.BusinessLayer.Facade;

using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for personal.
	/// </summary>
    public class personal : BasePage					
	{															
		protected personal ()									
		{														
			Title = "Personal Information";						
		}

        private void Page_Load(object sender, System.EventArgs e)
		{
			// They must be logged in
			if (!Request.IsAuthenticated)
			{
				Response.Redirect (GetLoginURL ());
				return;
			}

			int userId = GetUserId();

			if (!IsPostBack)
			{
				LoadData ();
                spnAlertMsg.Style.Add ("display", "none");
			}

            if (ViewState["returnPath"] == null)
            {
                if (Request.UrlReferrer != null && Request.UrlReferrer.PathAndQuery.Length > 0)
                {
                    ViewState["returnPath"] = Request.UrlReferrer.PathAndQuery;
                }
                else
                {
                    ViewState["returnPath"] = ResolveUrl ("~/default.aspx");
                }
            }

            spnAlertMsg.InnerText = string.Empty;
            spnAlertMsg.Style.Add("display", "none");
          
            //set needed javascript things
            litAddButtonClientId.Text = btnAdd.ClientID;
            litRemoveButtonClientId.Text = btnRemove.ClientID;
            btnAdd.Attributes.Add ("onclick", ClientScript.GetPostBackEventReference (this.btnAdd, "", false));
            btnRemove.Attributes.Add ("onclick", ClientScript.GetPostBackEventReference (this.btnRemove, "", false));

			// Set Nav
            //set the navigation

            ((GenericPageTemplate)Master).HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
            ((GenericPageTemplate)Master).HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.PROFILE;
            ((GenericPageTemplate)Master).HeaderNav.MyChannelsNav.ActiveTab = NavMyChannels.TAB.PERSONAL;
            ((GenericPageTemplate)Master).HeaderNav.MyChannelsNav.ChannelId = GetPersonalChannelId();

            ((GenericPageTemplate)Master).HeaderNav.SetNavVisible(((GenericPageTemplate)Master).HeaderNav.MyKanevaNav, 2);
		}

		#region Helper Methods

		/// <summary>
		/// LoadData
		/// </summary>
		/// <param name="userid"></param>
		private void LoadData ()
		{
            User user = KanevaWebGlobals.CurrentUser;
			
            // Get Schools
            InitSchoolData (user.UserId);

            // Get the 'I am here for' Data
            InitHereForData(user.UserId);
            
            // Get Personal Data 
            InitPersonalData(user.UserId);
            
            // Get URL Data
            InitURLData(user.UserId);
		}

        /// <summary>
        /// Initialize the School Data
        /// </summary>
        private void InitSchoolData (int userId)
        {
            trSchool1.Attributes["class"] = "";
            trSchool2.Attributes["class"] = "hide";
            trSchool3.Attributes["class"] = "hide";
            trSchool4.Attributes["class"] = "hide";
            trSchool5.Attributes["class"] = "hide";

            // Clear all school entry fields
            txtSchool1.Text = txtSchool2.Text = txtSchool3.Text = txtSchool4.Text = txtSchool5.Text = string.Empty;
            txtCity1.Text = txtCity2.Text = txtCity3.Text = txtCity4.Text = txtCity5.Text = string.Empty;
            txtState1.Text = txtState2.Text = txtState3.Text = txtState4.Text = txtState5.Text = string.Empty;

            lnkAddSchool.Attributes["class"] = "";

            DataTable dtSchools = GetInterestsFacade.GetUserSchools (userId);

            if (dtSchools != null && dtSchools.Rows.Count > 0)
            {
                // Added this check because at least 1 user had more than 5 schools and page was erroring out
                // when they tried to go to this page because there are only 5 sets of controls to display schools.
                // This is a quick fix.  At some point we need to change these to all dynamically created controls
                // so it will not matter how many schools the user has associated with their profile. Added check
                // to personal widget so user can not add new school if they already have 5.  10/20/08
                int maxCount = dtSchools.Rows.Count > 5 ? 5 : dtSchools.Rows.Count;

                TextBox txtSchool = null;
                TextBox txtCity = null;
                TextBox txtState = null;
                LinkButton btnRemoveSchool = null;
                HtmlGenericControl trSchool = null;

                for (int i = 0; i < maxCount; i++)
                {
                    switch (i + 1)
                    {
                        case 1:
                            txtSchool = txtSchool1;
                            txtCity = txtCity1;
                            txtState = txtState1;
                            btnRemoveSchool = btnRemoveSchool1;
                            trSchool = trSchool1;
                            break;
                        case 2:
                            txtSchool = txtSchool2;
                            txtCity = txtCity2;
                            txtState = txtState2;
                            btnRemoveSchool = btnRemoveSchool2;
                            trSchool = trSchool2;
                            break;
                        case 3:
                            txtSchool = txtSchool3;
                            txtCity = txtCity3;
                            txtState = txtState3;
                            btnRemoveSchool = btnRemoveSchool3;
                            trSchool = trSchool3;
                            break;
                        case 4:
                            txtSchool = txtSchool4;
                            txtCity = txtCity4;
                            txtState = txtState4;
                            btnRemoveSchool = btnRemoveSchool4;
                            trSchool = trSchool4;
                            break;
                        case 5:
                            txtSchool = txtSchool5;
                            txtCity = txtCity5;
                            txtState = txtState5;
                            btnRemoveSchool = btnRemoveSchool5;
                            trSchool = trSchool5;
                            break;
                    }

                    trSchool.Attributes["class"] = "";
                    btnRemoveSchool.CommandArgument = dtSchools.Rows[i]["school_id"].ToString();

                    if (dtSchools.Rows[i]["name"] != null)
                    {
                        txtSchool.Text = Server.HtmlDecode(dtSchools.Rows[i]["name"].ToString());
                    }
                    if (dtSchools.Rows[i]["city"] != null)
                    {
                        txtCity.Text = Server.HtmlDecode(dtSchools.Rows[i]["city"].ToString());
                    }
                    if (dtSchools.Rows[i]["state"] != null)
                    {
                        txtState.Text = Server.HtmlDecode(dtSchools.Rows[i]["state"].ToString());
                    }

                    //((HtmlTableRow) FindControl ("trSchool" + (i + 1))).Attributes["class"] = "";
                    //((LinkButton) FindControl ("btnRemoveSchool" + (i + 1))).CommandArgument = dtSchools.Rows[i]["school_id"].ToString ();

                    //if (dtSchools.Rows[i]["name"] != null)
                    //{
                    //    ((TextBox) FindControl ("txtSchool" + (i + 1))).Text = Server.HtmlDecode (dtSchools.Rows[i]["name"].ToString ());
                    //}
                    //if (dtSchools.Rows[i]["city"] != null)
                    //{
                    //    ((TextBox) FindControl ("txtCity" + (i + 1))).Text = Server.HtmlDecode (dtSchools.Rows[i]["city"].ToString ());
                    //}
                    //if (dtSchools.Rows[i]["state"] != null)
                    //{
                    //    ((TextBox) FindControl ("txtState" + (i + 1))).Text = Server.HtmlDecode (dtSchools.Rows[i]["state"].ToString ());
                    //}
                }


                if (maxCount == 5)
                {
                    lnkAddSchool.Attributes["class"] = "hide";
                }
            }
        }

        /// <summary>
        /// Initialize the 'I am Here For' Data
        /// </summary>
        private void InitHereForData (int userId)           
        {
            // Get the Interest info
            DataTable dtInterests = GetInterestsFacade.GetInterestByCategory (userId, 21);

            if (dtInterests != null)
            {
                string category = string.Empty;

                for (int i = 0; i < dtInterests.Rows.Count; i++)
                {
                    if (category != dtInterests.Rows[i]["category_text"].ToString ())
                    {
                        category = dtInterests.Rows[i]["category_text"].ToString ();

                        // Create the container table for the category
                        HtmlTable tblContainer = new HtmlTable ();
                        HtmlTableRow row_tblContainer = new HtmlTableRow ();

                        tblContainer.Border = 0;
                        tblContainer.CellPadding = 3;
                        tblContainer.CellSpacing = 3;
                        tblContainer.Style["margin-top"] = "3px";
                        row_tblContainer.VAlign = "middle";

                        // Create cell to hold the category title (name)
                        HtmlTableCell cellCategoryTitle = new HtmlTableCell ();

                        cellCategoryTitle.Align = "left";
                        cellCategoryTitle.VAlign = "top";
                        cellCategoryTitle.NoWrap = true;
                        cellCategoryTitle.InnerHtml = dtInterests.Rows[i]["category_text"].ToString () + ":";

                        // Create cell to hold all the interests for this category
                        HtmlTableCell cellInterests = new HtmlTableCell ();

                        cellInterests.Align = "left";
                        cellInterests.VAlign = "middle";

                        // Interest container div
                        cellInterests.InnerHtml = "<div id=\"divIC_" + dtInterests.Rows[i]["ic_id"].ToString () + "\">";

                        // add a new interest item
                        if (dtInterests.Rows[i]["interest"] != null && dtInterests.Rows[i]["interest"].ToString ().Length > 0)
                        {
                            cellInterests.InnerHtml += "<div style=\"float:left;margin-right:2px;\"><span id=\"spnInt_" + dtInterests.Rows[i]["interest_id"].ToString () + "\">" +
                                " <a href=\"javascript:void(0);\" class=\"nohover\" title=\"Remove " +
                                    Server.HtmlEncode (dtInterests.Rows[i]["interest"].ToString ()) +
                                    "\" ><img border=\"0\" onclick=\"removeInterest(" +
                                    dtInterests.Rows[i]["interest_id"].ToString () + "," +
                                    dtInterests.Rows[i]["ic_id"].ToString () + ");\" src=\"" +
                                    ResolveUrl ("~/images/del_sm.gif") + "\" /></a> " +
                                "<a id=\"aInt_" + dtInterests.Rows[i]["interest_id"].ToString () + "\" href=\"" +
                                    ResolveUrl ("~/people/people.kaneva?" + Constants.QUERY_STRING_INTEREST + "=" +
                                    Server.UrlEncode (dtInterests.Rows[i]["interest"].ToString ())) + "\" >" +
                                    "<span>" + Server.HtmlEncode (dtInterests.Rows[i]["interest"].ToString ()) + "</span></a> " +
                                "<span>| </span></span></div> ";
                        }

                        // get all intersts for a particular category
                        while (++i < dtInterests.Rows.Count)
                        {
                            if (category == dtInterests.Rows[i]["category_text"].ToString ())
                            {
                                cellInterests.InnerHtml += "<div style=\"float:left;margin-right:2px;white-space:nowrap;\"><span id=\"spnInt_" + dtInterests.Rows[i]["interest_id"].ToString () + "\" style=\"WHITE-SPACE:nowrap;\">" +
                                    " <a href=\"javascript:void(0);\" class=\"nohover\" title=\"Remove " +
                                        Server.HtmlEncode (dtInterests.Rows[i]["interest"].ToString ()) +
                                        "\" ><img border=\"0\" onclick=\"removeInterest(" +
                                        dtInterests.Rows[i]["interest_id"].ToString () + "," +
                                        dtInterests.Rows[i]["ic_id"].ToString () + ");\" src=\"" +
                                        ResolveUrl ("~/images/del_sm.gif") + "\" /></a> " +
                                    "<a id=\"aInt_" + dtInterests.Rows[i]["interest_id"].ToString () + "\" href=\"" +
                                        ResolveUrl ("~/people/people.kaneva?" + Constants.QUERY_STRING_INTEREST + "=" +
                                        Server.UrlEncode (dtInterests.Rows[i]["interest"].ToString ())) + "\" >" +
                                        "<span>" + Server.HtmlEncode (dtInterests.Rows[i]["interest"].ToString ()) + "</span></a> " +
                                    "<span>| </span></span></div> ";
                            }
                            else
                            {
                                i--;
                                break;
                            }
                        }

                        if (i == dtInterests.Rows.Count) i--;

                        // close out interest div container tag
                        cellInterests.InnerHtml += "</div>";

                        // Create cell to hold input field and buttons (images)
                        HtmlTableCell cellInput = new HtmlTableCell ();
                        cellInput.Align = "left";
                        cellInput.VAlign = "bottom";

                        string width = "169";
                        if (Request.Browser.Type.Substring (0, 2) == "IE")
                        {
                            width = "166";
                        }

                        cellInput.InnerHtml += "<table border=\"0\" id=\"spnAdd_" + dtInterests.Rows[i]["ic_id"].ToString () +
                            "\" style=\"display:none;\" cellpadding=\"2\" cellspacing=\"0\"><tr valign=\"middle\"><td>" +
                            "<div id=\"myAutoComplete\">" +
                                "<input type=\"text\" style=\"width:" + width + "px; top:-8px\" id=\"txt_" +
                                dtInterests.Rows[i]["ic_id"].ToString () + "\" onkeypress=\"checkKey(event," +
                                dtInterests.Rows[i]["ic_id"].ToString () + ");\" onkeyup=\"checkLen (this," +
                                dtInterests.Rows[i]["ic_id"].ToString () + ");\" maxlength=\"51\" /> " +
                                "<div id=\"myContainer_" + dtInterests.Rows[i]["ic_id"].ToString () + "\"></div></div></td>" +
                            "<td><a href=\"javascript:void(0);\" class=\"nohover\" style=\"margin-left:2px;\" title=\"Save Interest\"><img onclick=\"addInterest(this," +
                                dtInterests.Rows[i]["ic_id"].ToString () + ");\" id=\"imgAdd_" +
                                dtInterests.Rows[i]["ic_id"].ToString () + "\" border=\"0\" src=\"" +
                                ResolveUrl ("~/images/val_sm.gif") + "\" /></a></td>" +
                            "<td>&nbsp;<a href=\"javascript:void(0);\" class=\"nohover\" title=\"Cancel\"><img onclick=\"hideInput(this," +
                                dtInterests.Rows[i]["ic_id"].ToString () + ");\" border=\"0\" src=\"" +
                                ResolveUrl ("~/images/del_sm.gif") + "\"/></a></td>" +
                            "</tr><tr>" +
                            "<td colspan=\"3\" class=\"info\" style=\"padding-left:2px;white-space:no-wrap; \" id=\"tdMsg_" + dtInterests.Rows[i]["ic_id"].ToString() + "\">(start typing to see suggestions)</td>" +
                            "</tr></table>" +
                            "<a href=\"javascript:void(0)\" class=\"nohover\" title=\"Add Interest\" >" +
                            "<img onclick=\"showInput(this," + dtInterests.Rows[i]["ic_id"].ToString () +
                            ")\" id=\"imgNew_" + dtInterests.Rows[i]["ic_id"].ToString () +
                            "\" border=\"0\" style=\"cursor: hand;\" src=\"" + ResolveUrl ("~/images/plus_sm.gif") + "\" /></a>";

                        // Add cells to the row control
                        row_tblContainer.Cells.Add (cellCategoryTitle);
                        row_tblContainer.Cells.Add (cellInterests);

                        HtmlTableRow row_Input = new HtmlTableRow ();
                        HtmlTableCell cellEmpty = new HtmlTableCell ();
                        row_Input.Cells.Add (cellEmpty);
                        row_Input.Cells.Add (cellInput);

                        // Add the row to the table
                        tblContainer.Rows.Add (row_tblContainer);
                        tblContainer.Rows.Add (row_Input);

                        // Add new table to the container table cell
                        tdHereFor.Controls.Add (tblContainer);
                    }
                }
            }
        }

        /// <summary>
        /// Initialize the Personal Data
        /// </summary>
        private void InitPersonalData (int userId)
        {
            // Add freaky to adults
            if (KanevaWebGlobals.CurrentUser.IsAdult)
            {
                drpRelationship.Items.Add (new ListItem ("Swinger", "Swinger"));
                drpRelationship.Items.Add (new ListItem ("Freaky", "Freaky"));

                drpOrientation.Items.Add (new ListItem ("Freak", "Freak"));

                trSexOrientation.Visible = true;
                trSmoking.Visible = true;
                trDrinking.Visible = true;
            }

            DataRow drUserProfile = GetUserFacade.GetUserProfile (userId);
            if (drUserProfile != null)
            {
                SetDropDownIndex (drpRelationship, Server.HtmlDecode (drUserProfile["relationship"].ToString ()));
                SetDropDownIndex (drpOrientation, Server.HtmlDecode (drUserProfile["orientation"].ToString ()));
                SetDropDownIndex (drpChildren, Server.HtmlDecode (drUserProfile["children"].ToString ()));
                SetDropDownIndex (drpEducation, Server.HtmlDecode (drUserProfile["education"].ToString ()));
                SetDropDownIndex (drpIncome, Server.HtmlDecode (drUserProfile["income"].ToString ()));
                SetDropDownIndex (drpHeightFeet, Server.HtmlDecode (drUserProfile["height_feet"].ToString ()));
                SetDropDownIndex (drpHeightInches, Server.HtmlDecode (drUserProfile["height_inches"].ToString ()));
                SetDropDownIndex (drpSmoking, Server.HtmlDecode (drUserProfile["smoke"].ToString ()));
                SetDropDownIndex (drpDrinking, Server.HtmlDecode (drUserProfile["drink"].ToString ()));

                SetDropDownIndex (drpReligion, Server.HtmlDecode (drUserProfile["religion"].ToString ()));
                SetDropDownIndex (drpEthnicity, Server.HtmlDecode (drUserProfile["ethnicity"].ToString ()));

                if (drUserProfile["hometown"] != null)
                {
                    txtHometown.Text = Server.HtmlEncode (drUserProfile["hometown"].ToString ());
                }
            }
        }

        /// <summary>
        /// Initialize the URL Data
        /// </summary>
        private void InitURLData (int userId)
		{
			// Show current URL							  
			Community community = GetCommunityFacade.GetCommunity (GetPersonalChannelId());

            if (!community.Url.Equals(DBNull.Value) && (community.Url.Length > 0))
			{
				lblURL.Style.Add("font-weight","bold");
                lblURL.Text = "http://" + KanevaGlobals.SiteName + "/" + community.Url;
				txtURL.Visible = false;
				btnRegister.Visible = false;
				revURL.Enabled = false;
                spnURLDesc.Visible = false;
                trURLRegister.Visible = false;
                spnRegistered.Visible = true;
            }
			else
			{
				// Set up regular expression validators
				revURL.ValidationExpression = Constants.VALIDATION_REGEX_USERNAME;
				lblURL.Text = "http://" + KanevaGlobals.SiteName + "/";
			}
		}

		/// <summary>
		/// AddDropDownPermissions
		/// </summary>
		private void AddDropDownPermissions (DropDownList drpDropDown)
		{
			drpDropDown.Items.Insert (0, new ListItem ("Everyone who is signed in", "K"));
			drpDropDown.Items.Insert (0, new ListItem ("All friends", "A"));
			drpDropDown.Items.Insert (0, new ListItem ("Only Me", "M"));
			drpDropDown.Items.Insert (0, new ListItem ("Everyone including visitors", "Y"));
		}

		/// <summary>
		/// AddDropDownIMs
		/// </summary>
		private void AddDropDownIMs (DropDownList drpDropDown)
		{
			drpDropDown.Items.Insert (0, new ListItem ("Yahoo", "Yahoo"));
			drpDropDown.Items.Insert (0, new ListItem ("MSN", "MSN"));
			drpDropDown.Items.Insert (0, new ListItem ("ICQ", "ICQ"));
			drpDropDown.Items.Insert (0, new ListItem ("AIM", "AIM"));
		}

        /// <summary>
        /// Save the school info
        /// </summary>
        private bool SaveSchools ()
        {
            TextBox txtSchool = null;
            TextBox txtCity = null;
            TextBox txtState = null;

            for (int i = 1; i <= 5; i++)
            {
                try
                {
                    //TextBox txtCity = (TextBox)FindControl("txtSchool" + i);
                    //add due to issue with find control caused by Master page
                    switch(i)
                    {
                        case 1:
                            txtSchool = txtSchool1;
                            txtCity = txtCity1;
                            txtState = txtState1;
                            break;
                        case 2:
                            txtSchool = txtSchool2;
                            txtCity = txtCity2;
                            txtState = txtState2;
                            break;
                        case 3:
                            txtSchool = txtSchool3;
                            txtCity = txtCity3;
                            txtState = txtState3;
                            break;
                        case 4:
                            txtSchool = txtSchool4;
                            txtCity = txtCity4;
                            txtState = txtState4;
                            break;
                        case 5:
                            txtSchool = txtSchool5;
                            txtCity = txtCity5;
                            txtState = txtState5;
                            break;
                    }

                    if (txtSchool != null)
                    {
                        if (txtSchool.Text.Trim() != string.Empty)
                        {
                            //TextBox txtCity = (TextBox) FindControl ("txtCity" + i);
                            //TextBox txtState = (TextBox) FindControl ("txtState" + i);

                            // Check to make sure user did not enter any "potty mouth" words
                            if (KanevaWebGlobals.isTextRestricted (txtSchool.Text, Constants.eRESTRICTION_TYPE.POTTY_MOUTH) ||
                                KanevaWebGlobals.isTextRestricted (txtCity.Text, Constants.eRESTRICTION_TYPE.POTTY_MOUTH) ||
                                KanevaWebGlobals.isTextRestricted (txtState.Text, Constants.eRESTRICTION_TYPE.POTTY_MOUTH))
                            {
                                spnAlertMsg.InnerText = Constants.VALIDATION_REGEX_RESTRICTED_WORD_MESSAGE;
                                spnAlertMsg.Attributes.Add("class", "warningBox black");
                                return false;
                            }
                            
                            if (!KanevaWebGlobals.ContainsInjectScripts (txtSchool.Text) &&
                                !KanevaWebGlobals.ContainsInjectScripts (txtCity.Text) &&
                                !KanevaWebGlobals.ContainsInjectScripts (txtState.Text))
                            {
                                int schoolId = GetInterestsFacade.SchoolExists (Server.HtmlEncode (txtSchool.Text));
                                if (schoolId > 0)
                                {
                                    GetInterestsFacade.AssociateSchoolToUser(GetUserId(), schoolId);
                                }
                                else
                                {
                                    schoolId = GetInterestsFacade.AddSchool(Server.HtmlEncode(txtSchool.Text),
                                        Server.HtmlEncode (txtCity.Text), Server.HtmlEncode (txtState.Text));

                                    if (schoolId > 0)
                                    {
                                        GetInterestsFacade.AssociateSchoolToUser(GetUserId(), schoolId);
                                    }
                                }
                            }
                            else
                            {
                                spnAlertMsg.InnerText = Constants.VALIDATION_REGEX_RESTRICTED_WORD_MESSAGE;
                                spnAlertMsg.Attributes.Add("class", "warningBox black");
                                // Log it
                                m_logger.Warn ("User " + KanevaWebGlobals.GetUserId () + " tried to script from IP " + Common.GetVisitorIPAddress());
                            }
                        }
                    }
                }
                catch { }
            }

            return true;
        }

        /// <summary>
        /// Add the new interest
        /// </summary>
        public int AddInterest (string interest, int ic_id)
        {
            int interest_id = GetInterestsFacade.InterestExists (ic_id, interest);

            if (interest_id == 0)
            {
                if (interest.Length > 0)
                {
                    // add interest
                    interest_id = GetInterestsFacade.AddInterest(ic_id, interest);
                }
            }

            return interest_id;
        }

        private void RedirectPage()
        {
            if (ViewState["returnPath"] != null && ViewState["returnPath"].ToString().Length > 0)
            {
                Response.Redirect(ViewState["returnPath"].ToString());
            }
            else
            {
                Response.Redirect(ResolveUrl("~/community/ProfilePage.aspx?communityId=" + GetPersonalChannelId()));
            }
        }

        #endregion

		#region Event Handlers

        /// <summary>
        /// Click event for the Register URL button
        /// </summary>
        protected void btnRegister_Click (object sender, EventArgs e)
		{
			if (revURL.IsValid)
			{
                // Check to make sure user did not enter any "potty mouth" words
                if (KanevaWebGlobals.isTextRestricted (txtURL.Text, Constants.eRESTRICTION_TYPE.POTTY_MOUTH))
                {
                    spnAlertMsg.InnerText = Constants.VALIDATION_REGEX_RESTRICTED_WORD_MESSAGE;
                    spnAlertMsg.Attributes.Add ("class", "errBox black");
                    spnAlertMsg.Style.Add ("display", "block");
                    return;
                }

                // Check to make sure user did not enter any "reserved" words
                if (KanevaWebGlobals.isTextRestricted(txtURL.Text, Constants.eRESTRICTION_TYPE.RESERVED))
                {
                    spnAlertMsg.InnerText = Constants.VALIDATION_REGEX_RESERVED_WORD_MESSAGE;
                    spnAlertMsg.Attributes.Add("class", "errBox black");
                    spnAlertMsg.Style.Add("display", "block");
                    return;
                }

                // If URL has not already been set
				if (txtURL.Enabled && txtURL.Text.Trim() != string.Empty)
				{
					int channelId;

					if (IsAdministrator() && Request ["communityId"] != null && Request ["communityId"] != "")
					{
						channelId = Convert.ToInt32 (Request ["communityId"]);
					}
					else
					{
						channelId = GetPersonalChannelId ();
					}

					// Upadate URL
                    int result = GetCommunityFacade.UpdateCommunityURL(channelId, Server.HtmlEncode(txtURL.Text.Trim()));

					// Was it successful?
					if (result == 1)
					{
						divRegistrationErr.InnerHtml = "Channel URL http://" + KanevaGlobals.SiteName + "/" + txtURL.Text.Trim() + " already exists, please change the URL.";
						divRegistrationErr.Visible = true;
					}
					else
					{
						lblURL.Style.Add("font-weight","bold");
						lblURL.Text = "http://" + KanevaGlobals.SiteName + "/" + txtURL.Text.Trim();
						txtURL.Visible = false;
						btnRegister.Visible = false;
						revURL.Enabled = false;
						spnRegistered.Visible = true;
                        spnAlertMsg.InnerText = "URL registered.";
                    }
				}
			}
		}
		
        /// <summary>
		/// Click event for the Update (Save) button
		/// </summary>
		protected void btnUpdate_Click (object sender, EventArgs e) 
		{

            spnAlertMsg.InnerText = "";
            
            if (!Page.IsValid) 
			{
				return;
			}

            try
            {
                int userId = GetUserId();

                string homeTown = string.Empty;
                if (KanevaWebGlobals.ContainsInjectScripts(txtHometown.Text))
                {
                    m_logger.Warn("User " + KanevaWebGlobals.GetUserId() + " tried to script from IP " + Common.GetVisitorIPAddress());
                    spnAlertMsg.InnerText = Constants.VALIDATION_REGEX_RESTRICTED_WORD_MESSAGE;
                    spnAlertMsg.Attributes.Add("class", "warningBox black");
                    spnAlertMsg.Style.Add("display", "block");
                    return;
                }
                else
                {
                    homeTown = txtHometown.Text.Trim();
                }

                // Check to make sure user did not enter any "potty mouth" words
                if (KanevaWebGlobals.isTextRestricted(homeTown, Constants.eRESTRICTION_TYPE.POTTY_MOUTH))
                {
                    spnAlertMsg.InnerText = Constants.VALIDATION_REGEX_RESTRICTED_WORD_MESSAGE;
                    spnAlertMsg.Attributes.Add("class", "warningBox black");
                    spnAlertMsg.Style.Add("display", "block");
                    return;
                }

                // Update Schools
                if (!SaveSchools())
                {
                    if (spnAlertMsg.InnerText == "")
                    {
                        spnAlertMsg.InnerText = "Error: Unable to save your school changes.";
                        spnAlertMsg.Attributes.Add("class", "errBox black");
                    }
                    spnAlertMsg.Style.Add("display", "block");
                    return;
                }

                // Update user profile
                GetUserFacade.UpdateUserProfile(userId, Server.HtmlEncode(drpRelationship.SelectedValue),
                    Server.HtmlEncode(drpOrientation.SelectedValue), Server.HtmlEncode(drpReligion.SelectedValue),
                    Server.HtmlEncode(drpEthnicity.SelectedValue), Server.HtmlEncode(drpChildren.SelectedValue),
                    Server.HtmlEncode(drpEducation.SelectedValue), Server.HtmlEncode(drpIncome.SelectedValue),
                    Convert.ToInt32(Server.HtmlEncode(drpHeightFeet.SelectedValue)),
                    Convert.ToInt32(Server.HtmlEncode(drpHeightInches.SelectedValue)),
                    Server.HtmlEncode(drpSmoking.SelectedValue), Server.HtmlEncode(drpDrinking.SelectedValue),
                    Server.HtmlEncode(homeTown));

                RedirectPage();

            }
            catch (Exception)
            {
                spnAlertMsg.InnerText = "Error: Unable to save your changes.";
                spnAlertMsg.Attributes.Add("class", "errBox black");
                spnAlertMsg.Style.Add("display", "block");
            }
        }

		/// <summary>
		/// Click event for the cancel button
		/// </summary>
		protected void btnCancel_Click (object sender, EventArgs e) 
		{
            RedirectPage();
        }

        /// <summary>
        /// Click event when user clicks a add icon
        /// </summary>
        protected void btnAdd_Click (object sender, EventArgs e)
        {
            try
            {
                int ic_id = Convert.ToInt32 (hidIc_id.Value);
                string interest = hidInterest.Value.Trim ();

                // Make sure input is okay
                if (KanevaWebGlobals.ContainsInjectScripts (interest))
                {
                    m_logger.Warn ("User " + KanevaWebGlobals.GetUserId () + " tried to script from IP " + Common.GetVisitorIPAddress());
                }
                else
                {
                    // Check to make sure user did not enter any "potty mouth" words
                    if (KanevaWebGlobals.isTextRestricted (interest, Constants.eRESTRICTION_TYPE.POTTY_MOUTH))
                    {
                        AjaxCallHelper.WriteAlert (Constants.VALIDATION_REGEX_RESTRICTED_WORD_MESSAGE);

                        // Write out some Javascript to put the user back to the state where they can edit their entry
                        AjaxCallHelper.Write ("deactivateLB();" +                     // hide the lightbox if visible
                            "showInput($('imgNew_" + ic_id + "')," + ic_id + ");" +   // show the entry text box
                            "$('txt_" + ic_id + "').value='" + interest + "';");      // populate text box with entered value
                        return;
                    }
                    
                    Button btnSender = (Button) sender;
                    int interest_id = 0;
                    string tagList = string.Empty;
                    string idList = string.Empty;
                    string intList = string.Empty;

                    // check to see if we need to enter multiple interests
                    if (btnSender.ID == "btnAddMultiple")
                    {
                        char[] seperator = new char[] { ',' };
                        string[] arrInterests = interest.Split (seperator);

                        for (int i = 0; i < arrInterests.Length; i++)
                        {
                            // Add interest or get id if already exists
                            interest_id = AddInterest (arrInterests[i], ic_id);

                            if (interest_id > 0)
                            {
                                // add interest to user
                                if (GetInterestsFacade.AssignInterestToUser(GetUserId(), interest_id, ic_id) == 0)
                                {
                                    if (idList.Length > 0)
                                    {
                                        idList += "|";
                                        intList += "|^|";
                                    }
                                    idList += interest_id.ToString ();
                                    intList += arrInterests[i];
                                }
                            }
                        }

                        AjaxCallHelper.Write ("displayInterest('" + Server.HtmlEncode (intList.Replace ("'", "@~^~@")) + "','" + idList + "'," + ic_id + ");");
                        // AjaxCallHelper.Write ("displayInterest('" + Server.HtmlEncode (intList.Replace ("'", "@~^~@") + "','" + idList + "'," + ic_id + ");"));
                    }
                    else // add as single entry
                    {
                        // Add interest or get id if already exists
                        interest_id = AddInterest (interest, ic_id);

                        if (interest_id > 0)
                        {
                            // add interest to user
                            if (GetInterestsFacade.AssignInterestToUser(GetUserId(), interest_id, ic_id) == 0)
                            {
                                // call js function to display the new added interest
                                AjaxCallHelper.Write ("displayInterest('" + Server.HtmlEncode (interest.Replace ("'", "@~^~@")) + "','" + interest_id + "'," + ic_id + ");");
                            }
                            else
                            {
                                // interest already exists for user, just reset the values
                                // of the hidden fields to defaults
                                AjaxCallHelper.Write ("resetVals();");
                            }
                        }
                        else
                        {
                            // the interest value passed in was invalid so reset the
                            // values of the hidden fields to defaults
                            AjaxCallHelper.Write ("resetVals();");
                        }
                    }
                }
            }
            catch
            { }
        }

        /// <summary>
        /// Click event when user clicks a delete icon for Interest
        /// </summary>
        protected void btnRemove_Click (object sender, EventArgs e)
        {
            int interest_id = Convert.ToInt32 (hidInterestId.Value);
            int ic_id = Convert.ToInt32 (hidIc_id.Value);

            GetInterestsFacade.RemoveInterestFromUser(GetUserId(), interest_id);

            AjaxCallHelper.Write ("removeItem(" + interest_id + "," + ic_id + ");");
        }

        /// <summary>
        /// Click event when user clicks a delete link to remove a school
        /// </summary>
        protected void lbRemoveSchool_Click (object sender, CommandEventArgs e)
        {
            try
            {
                spnAlertMsg.Style.Add ("display", "none");
                
                if (Convert.ToInt32 (e.CommandArgument) > 0)
                {
                    GetInterestsFacade.RemoveSchoolFromUser(GetUserId(), Convert.ToInt32(e.CommandArgument));
                }

                InitSchoolData (GetUserId ());
            }
            catch { }
        }

 		#endregion
		

		#region Declerations
        protected HtmlContainerControl spnAlertMsg;
        protected ContentPlaceHolder mainContent;
		// Schools
        protected HtmlGenericControl trSchool1, trSchool2, trSchool3, trSchool4, trSchool5;
        protected TextBox txtSchool1, txtSchool2, txtSchool3, txtSchool4, txtSchool5;
        protected TextBox txtCity1, txtCity2, txtCity3, txtCity4, txtCity5;
        protected TextBox txtState1, txtState2, txtState3, txtState4, txtState5;
        protected HtmlAnchor lnkAddSchool;
        
        // Personal Info
        protected DropDownList drpRelationship, drpOrientation, drpChildren, drpEducation;
        protected DropDownList drpIncome, drpHeightFeet, drpHeightInches, drpSmoking, drpDrinking;
        protected DropDownList drpReligion, drpEthnicity;

        protected HtmlGenericControl trSexOrientation, trDrinking, trSmoking;
        protected HtmlGenericControl tdHereFor;
        protected HtmlInputHidden hidIc_id, hidInterest, hidInterestId;

        protected TextBox txtHometown;
        protected Button btnAdd, btnRemove;
        protected Literal litAddButtonClientId, litRemoveButtonClientId;

		// URL
		protected RegularExpressionValidator	revURL;
		protected TextBox						txtURL;
		protected Label							lblURL;
		protected LinkButton					btnRemoveSchool1, btnRemoveSchool2, btnRemoveSchool3, btnRemoveSchool4, btnRemoveSchool5, btnRegister;
		protected HtmlContainerControl			divRegistrationErr;
        protected HtmlContainerControl          spnRegistered, spnURLDesc;
        protected HtmlGenericControl trURLRegister;

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);
        
        #endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
