///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Data;
using System.Text;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva.channel;
using KlausEnt.KEP.Kaneva.framework.widgets;

using System.Collections.Generic;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for friends.
	/// </summary>
	public class friends : SortedBasePage
	{
		protected friends () 
		{
			Title = "My Friends";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			if(Request.IsAuthenticated)
			{
				if(GetRequestParams())
				{
					//check if user is allowed to edit this channel
					if(!IsUserAllowedToEdit())
					{
						RedirectToHomePage ();
					}
				}
				else
				{
					//invalid request params
					//if the user logged in, send him to his home page
					//if not, redirect to login page
					RedirectToHomePage ();
				}
			}
			else
			{
				Response.Redirect (this.GetLoginURL ());
			}

			if (!IsPostBack)
			{
				CurrentSort = "username";
				lbSortByName.CssClass = "selected";

				BindData (1, _userId);

				LoadFriendGroupDropdown (_userId);
			}

			// Set Nav
            HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
			HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.FRIENDS;
			HeaderNav.MyFriendNav.ActiveTab = NavFriend.TAB.GROUPS;
			HeaderNav.SetNavVisible(HeaderNav.MyKanevaNav,2);
			HeaderNav.SetNavVisible(HeaderNav.MyFriendNav);
		}

		/// <summary>
		/// Bind the data
		/// </summary>
		/// <param name="pageNumber"></param>
		private void BindData (int pageNumber, int userId)
		{
			BindData (pageNumber, userId, "");
		}
		/// <summary>
		/// Bind the data
		/// </summary>
		/// <param name="pageNumber"></param>
		private void BindData (int pageNumber, int userId, string filter)
		{
			searchFilter.CurrentPage = "friends";
			
			string orderby = CurrentSort + " " + CurrentSortOrder;
			int pgSize = searchFilter.NumberOfPages;

            int numberOfFriends = KanevaWebGlobals.CurrentUser.Stats.NumberOfFriends;

            UserFacade userFacade = new UserFacade();
            IList<Friend> friends = userFacade.GetFriends(userId, filter, orderby, pageNumber, pgSize);

            dlFriends.DataSource = friends;
			dlFriends.DataBind ();

            pgTop.NumberOfPages = Math.Ceiling((double)numberOfFriends / pgSize).ToString();
			pgTop.DrawControl ();

			// The results
            lblSearch.Text = GetResultsText(numberOfFriends, pageNumber, pgSize, friends.Count);
		}


		#region Helper Methods
		/// <summary>
		/// Get values from the request object
		/// </summary>
		/// <returns></returns>
		private bool GetRequestParams()
		{
			bool retVal = true;
			try
			{
				_userId = this.GetUserId();
			}
			catch(Exception)
			{
				//invalid numbers
				retVal = false;
			}
			return retVal;
		}

		/// <summary>
		/// returns true if current user is allowed to edit this channel
		/// </summary>
		/// <returns></returns>
		private bool IsUserAllowedToEdit()
		{
			return IsAdministrator() || _userId == this.GetUserId();
		}

		/// <summary>
		/// Load the list of Friend Groups in the drop down
		/// </summary>
		/// <returns></returns>
		protected void LoadFriendGroupDropdown (int userId)
		{
			drpFriendGroups.Items.Clear ();
			
			PagedDataTable Groups = UsersUtility.GetFriendGroups (userId, "", "name", 1, Int32.MaxValue);

			drpFriendGroups.Items.Add (CreateListItem ("Pick a Friend Group", "", true));

			if (Groups.Rows.Count > 0)
			{
				drpFriendGroups.Items.Add (CreateListItem ("Add to group:", "", true));

				for (int i = 0; i < Groups.Rows.Count; i++)
				{
					drpFriendGroups.Items.Add (new ListItem (Server.HtmlDecode (Groups.Rows[i]["name"].ToString()) + " (" + Groups.Rows[i]["friend_count"].ToString() + ")", Groups.Rows[i]["friend_group_id"].ToString()));
				}
			}

			drpFriendGroups.SelectedIndex = 0;
		}

		#endregion

		#region Event Handlers
		/// <summary>
		/// Execute when the user clicks the the view type link button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void lbSortBy_Click (object sender, EventArgs e) 
		{
			lbSortByName.CssClass = lbSortByDate.CssClass = "";

			if (((LinkButton)sender).Attributes["sortby"].ToString().ToLower() == "name")
			{
				CurrentSort = "username";
				lbSortByName.CssClass = "selected";
			}
			else
			{
				CurrentSort = "glued_date";
				lbSortByDate.CssClass = "selected";							  
			}

			BindData (pgTop.CurrentPageNumber, _userId);
		}
		/// <summary>
		/// Execute when the user clicks the the Add button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnAdd_Click (object sender, ImageClickEventArgs e)
		{
			HtmlInputHidden hidFriendId;
			CheckBox chkEdit;
			int ret = 0;
			int count = 0;
			bool isItemChecked = false;

			if (drpFriendGroups.SelectedValue != "")
			{
				if (KanevaGlobals.IsNumeric (drpFriendGroups.SelectedValue))
				{
					int friendGroupId = Convert.ToInt32 (drpFriendGroups.SelectedValue);

					foreach (DataListItem dliFriend in dlFriends.Items)
					{
						chkEdit = (CheckBox) dliFriend.FindControl ("chkEdit");

						if (chkEdit.Checked)
						{
							isItemChecked = true;

							hidFriendId = (HtmlInputHidden) dliFriend.FindControl ("hidFriendId");
							ret = UsersUtility.InsertFriendInGroup (_userId, friendGroupId, Convert.ToInt32 (hidFriendId.Value));
						
							if (ret == 1)
								count++;
						}
					}

					if (isItemChecked)
					{
						if (count > 0)
						{
							BindData (pgTop.CurrentPageNumber, _userId);
							LoadFriendGroupDropdown (_userId);
						}
						
						if (ret != 1)
						{
							spnAlertMsg.InnerText = "An error was encountered while trying to add Friends.";
						}
						else
						{
							spnAlertMsg.InnerText = "Successfully added " + count + " friend" + (count != 1 ? "s" : "") + ".";
						}
					}
					else
					{
						spnAlertMsg.InnerText = "No friends were selected.";
					}
				}
			}
			else
			{
				spnAlertMsg.InnerText = "Friend group was not selected.";
			}

			MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('spnAlertMsg', 5000);");
		}
		
		/// <summary>
		/// Execute when the user clicks the Delete button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnDelete_Click (object sender, ImageClickEventArgs e)
		{
			HtmlInputHidden hidFriendId;
			CheckBox chkEdit;
			int ret = 0;
			int count = 0;
			bool isItemChecked = false;

			// Remove the friends
			foreach (DataListItem dliFriend in dlFriends.Items)
			{
				chkEdit = (CheckBox) dliFriend.FindControl ("chkEdit");

				if (chkEdit.Checked)
				{
					isItemChecked = true;

					hidFriendId = (HtmlInputHidden) dliFriend.FindControl ("hidFriendId");
                    ret = GetUserFacade.DeleteFriend(_userId, Convert.ToInt32(hidFriendId.Value));
				
					if (ret == 1)
						count++;
				}
			}

			if (isItemChecked)
			{
				if (count > 0)
				{
					BindData (pgTop.CurrentPageNumber, _userId);
					LoadFriendGroupDropdown (_userId);
				}
				
				if (ret != 1)
				{
					spnAlertMsg.InnerText = "An error was encountered while trying to remove Friends.";
				}
				else
				{
					spnAlertMsg.InnerText = "Successfully removed " + count + " friend" + (count != 1 ? "s" : "") + ".";
				}
			}
			else
			{
				spnAlertMsg.InnerText = "No friends were selected.";
			}

			MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('spnAlertMsg', 5000);");
		}
		/// <summary>
		/// Page Change
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pgTop_PageChange (object sender, PageChangeEventArgs e)
		{
			BindData (e.PageNumber, _userId);
		}
		/// <summary>
		/// Execute when the user selects an item from the Store Filter
		/// Items to display drop down list
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void FilterChanged(object sender, FilterChangedEventArgs e)
		{
			searchFilter.SetPagesDropValue(searchFilter.NumberOfPages.ToString());
			
			BindData (1, _userId, e.Filter);
		}

		#endregion

		#region Declerations
		protected Label					lblSearch;
		protected DataList				dlFriends;
		protected LinkButton			lbSortByName, lbSortByDate;
		protected LinkButton			lbSelectAll;
		protected Label					lblResultsTop;
		protected DropDownList			drpFriendGroups;
		protected HtmlContainerControl	spnAlertMsg;

		protected Kaneva.SearchFilter	searchFilter;
		protected Kaneva.Pager			pgTop;

		protected int _userId;


		#endregion

		#region Properties
		/// <summary>
		/// DEFAULT_SORT
		/// </summary>
		/// <returns></returns>
		protected override string DEFAULT_SORT
		{
			get
			{
				return "username";
			}
		}

		#endregion

		#region Web Form Designer generated code

		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
			pgTop.PageChanged +=new PageChangeEventHandler (pgTop_PageChange);
			searchFilter.FilterChanged +=new FilterChangedEventHandler (FilterChanged);		
		}
		#endregion
	}
}
