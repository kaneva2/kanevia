///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using log4net;

namespace KlausEnt.KEP.Kaneva.mykaneva
{
	/// <summary>
	/// Summary description for mailboxBulletin.
	/// </summary>
	public class mailboxBulletin : SortedBasePage
	{
		protected mailboxBulletin () 
		{
			Title = "Bulletin Center";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			// They must be logged in
			if (!Request.IsAuthenticated)
			{
				Response.Redirect (GetLoginURL ());
				return;
			}

			if (!IsPostBack)
			{
				searchFilter.CurrentPage = "bulletincenter";

				CurrentSort = "message_date";
				lbSortByDate.CssClass = "selected";

				int userId = GetUserId ();
				BindData (1, userId);
			}

			// Set Nav
            HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
			HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.MESSAGES;
			HeaderNav.MyMessagesNav.ActiveTab = NavMessages.TAB.BULLETINS;
			HeaderNav.SetNavVisible(HeaderNav.MyKanevaNav,2);
			HeaderNav.SetNavVisible(HeaderNav.MyMessagesNav);


		}

		private void BindData (int pageNumber, int userId)
		{
			BindData (pageNumber, userId, "");
		}
		/// <summary>
		/// Bind the data
		/// </summary>
		/// <param name="pageNumber"></param>
		private void BindData (int pageNumber, int userId, string filter)
		{
            //searchFilter.CurrentPage = "bulletins";
			
            //// Set the sortable columns
            ////SetHeaderSortText (rptBulletins);				  
            //string orderby = CurrentSort + " " + CurrentSortOrder;			   
            //int pgSize = searchFilter.NumberOfPages;

            //PagedDataTable pds = UsersUtility.GetBulletins (userId, "", orderby, pageNumber, pgSize);
            //rptBulletins.DataSource = pds;
            //rptBulletins.DataBind ();


            ////added to provide use a textual clue that there are no messages to the user
            //if(pds.Rows.Count < 1)
            //{
            //    this.noMessageFound.Visible = true;
            //    this.MessageFound.Visible = false;
            //}
            //else
            //{
            //    this.noMessageFound.Visible = false;
            //    this.MessageFound.Visible = true;
            //}

            //pgTop.NumberOfPages = Math.Ceiling ((double) pds.TotalCount / pgSize).ToString ();
            //pgTop.DrawControl ();
																 
            //// The results
            //lblSearch.Text = GetResultsText (pds.TotalCount, pageNumber, pgSize, pds.Rows.Count);
		}

		
		#region Helper Methods
		protected string GetMailStatusText (string toViewable)
		{
			string statusText = "Unopened";

            if (toViewable.Equals("U"))
			{
				statusText = "Opened";
			}
			
			return statusText;
		}
		public string GetTypeImage (string toViewable)
		{
			return this.GetTypeImage((int) Constants.eMESSAGE_TYPE.PRIVATE_MESSAGE, toViewable);
		}
		#endregion

		#region Event Handlers
		/// <summary>
		/// Page Change
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pgTop_PageChange (object sender, PageChangeEventArgs e)
		{
			BindData (e.PageNumber, GetUserId ());
		}

		/// <summary>
		/// Execute when the user clicks the the view type link button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void lbSortBy_Click (object sender, EventArgs e) 
		{
			lbSortByName.CssClass = lbSortByDate.CssClass = "";

			if (((LinkButton)sender).Attributes["sortby"].ToString().ToLower() == "name")
			{
				CurrentSort = "username";
				CurrentSortOrder = "ASC";
				lbSortByName.CssClass = "selected";
			}
			else
			{
				CurrentSort = "message_date";
				CurrentSortOrder = "DESC";
				lbSortByDate.CssClass = "selected";							  
			}

			BindData (pgTop.CurrentPageNumber, GetUserId ());
		}

		/// <summary>
		/// Execute when the user selects an item from the Store Filter
		/// Items to display drop down list
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void FilterChanged(object sender, FilterChangedEventArgs e)
		{
			searchFilter.SetPagesDropValue(searchFilter.NumberOfPages.ToString());
			
			BindData (1, GetUserId(), e.Filter);
		}
		/// <summary>
		/// Click the send message button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnSendMessage_Click (Object sender, ImageClickEventArgs e) 
		{
			Server.Transfer (ResolveUrl ("~/myKaneva/newMessage.aspx"));
		}

		#endregion


		#region Properties
		/// <summary>
		/// DEFAULT_SORT
		/// </summary>
		/// <returns></returns>
		protected override string DEFAULT_SORT
		{
			get
			{
				return "message_date";
			}
		}

		/// <summary>
		/// DEFAULT_SORT ORDER
		/// </summary>
		/// <returns></returns>
		protected override string DEFAULT_SORT_ORDER
		{
			get
			{
				return "DESC";
			}
		}

		#endregion

		#region Declerations
		protected Kaneva.Pager pgTop;
		protected Kaneva.SearchFilter searchFilter;
		protected System.Web.UI.HtmlControls.HtmlTable MessageFound;
		protected System.Web.UI.HtmlControls.HtmlTable noMessageFound;
		protected Label			lblSearch;
		protected Repeater		rptBulletins;	
		protected LinkButton	lbSortByName, lbSortByDate;

		protected HtmlContainerControl	spnAlertMsg, pStatusTxt;
		
		private const int MESSAGES_PER_PAGE = 25;
		protected PlaceHolder phBreadCrumb;

		// Logger
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);
		#endregion


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
			pgTop.PageChanged +=new PageChangeEventHandler (pgTop_PageChange);
			searchFilter.FilterChanged +=new FilterChangedEventHandler (FilterChanged);		
		}
		#endregion
	}
}
