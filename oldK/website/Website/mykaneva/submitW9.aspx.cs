///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for submitW9.
	/// </summary>
	public class submitW9 : MainTemplatePage
	{
		protected submitW9 () 
		{
			Title = "Submit W9";
		}

		private void Page_Load (object sender, System.EventArgs e)
		{
			// They must be logged in
			if (!Request.IsAuthenticated)
			{
				Response.Redirect (GetLoginURL ());
				return;
			}

			if (!IsPostBack)
			{
				int userId = GetUserId ();

				// User information
                txtName.Text = KanevaWebGlobals.CurrentUser.FirstName + " " + KanevaWebGlobals.CurrentUser.LastName;
				lblCountry.Text = ""; //drUser ["country_name"].ToString ();

				// Payment information
//				DataRow drUserPaymentInfo = UsersUtility.GetUserPaymentInfo (userId);
//				txtCompanyName.Text = drUserPaymentInfo ["company_name"].ToString ();
//				txtAddress.Text = drUserPaymentInfo ["address"].ToString ();
//				txtAddress2.Text = drUserPaymentInfo ["address2"].ToString ();
//				txtCity.Text = drUserPaymentInfo ["city"].ToString ();
//				txtState.Text = drUserPaymentInfo ["state"].ToString ();
//				txtTelephone.Text = drUserPaymentInfo ["phone_number"].ToString ();
//
//				string usResident = drUserPaymentInfo ["us_resident"].ToString ();
//				string individual = drUserPaymentInfo ["individual"].ToString ();
//
//				// US Resident?
//				if (usResident.Equals ("Y"))
//				{
//					rdoUSResident.Checked = true;
//					txtSSN.Enabled = true;
//					txtTIN.Enabled = true;
//				}
//				else
//				{
//					rdoNonUS.Checked = true;
//					txtSSN.Enabled = false;
//					txtTIN.Enabled = false;
//				}
//
//				txtSSN.Text = drUserPaymentInfo ["ssn"].ToString ();
//				txtTIN.Text = drUserPaymentInfo ["tin"].ToString ();
//
//				// Set up javascripts
//				rdoUSResident.Attributes.Add ("onclick", "javascript:document.frmMain." + txtSSN.ClientID + ".disabled=false;document.frmMain." + txtTIN.ClientID + ".disabled=false;");
//				rdoNonUS.Attributes.Add ("onclick", "javascript:document.frmMain." + txtSSN.ClientID + ".disabled=true;document.frmMain." + txtTIN.ClientID + ".disabled=true;");
			}


			btnUpdate.Attributes.Add ("onclick", "if (LicenseCheck ()){return false};if (typeof(Page_ClientValidate) == 'function') Page_ClientValidate();");

			string scriptString = "<script language=JavaScript>\n";
			scriptString += "function LicenseCheck (){\n";
			scriptString += " var errorMsg = '';\n";
			scriptString += " var bError = false;\n";
			scriptString += " if (document.frmMain." + chkCertification.ClientID + ".checked == false)\n";
			scriptString += " {errorMsg += 'You must accept the Certification Statement by checking the declaration check box.\\n';\n bError=true;}\n";
			scriptString += " if (bError)\n";
			scriptString += " {alert (errorMsg);}\n";
			scriptString += "return bError;\n";
			scriptString += "}</script>";

            if (!ClientScript.IsClientScriptBlockRegistered(GetType (), "LicenseCheck"))
			{
                ClientScript.RegisterStartupScript(GetType(), "LicenseCheck", scriptString);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnUpdate_Click (object sender, EventArgs e) 
		{
			Page.Validate ();

			if (!Page.IsValid)
			{
				return;
			}

			int userId = GetUserId ();

			if (txtSSN.Text.Trim ().Length.Equals (0) && txtTIN.Text.Trim ().Length.Equals (0))
			{
				ShowErrorOnStartup ("You must enter in either a Social Security Number or Employer Identification Number ");
				return;
			}

			UsersUtility.InsertUserPaymentInfo (userId, txtName.Text, txtCompanyName.Text, txtAddress.Text, txtAddress2.Text,
				txtCity.Text, txtState.Text, txtPostalCode.Text, txtTelephone.Text, "Y", "U", txtSSN.Text, txtTIN.Text,
				"Y", "N", Request.UserHostAddress);

			Response.Redirect ("~/mykaneva/paymentInformation.aspx");
		}

		protected HtmlInputRadioButton rdoUSResident, rdoNonUS;
		protected TextBox txtTIN, txtSSN;

		protected DropDownList drpCountry;

		protected Label lblPostalCode, lblCountry;

		protected TextBox txtName, txtCompanyName, txtAddress, txtAddress2, txtCity, txtState, txtTelephone, txtPostalCode;
		protected CheckBox chkCertification;

		protected Button btnUpdate;

		#region Web Form Designer generated code

		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
