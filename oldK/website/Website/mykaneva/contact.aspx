<%@ Page language="c#" Codebehind="contact.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.contact" %>

<link href="../css/home.css" rel="stylesheet" type="text/css" />		
<link href="../css/kanevaSC.css" rel="stylesheet" type="text/css"/>
<link href="../css/friends.css" rel="stylesheet" type="text/css"/>
<link href="../css/kanevaText.css" type="text/css" rel="stylesheet">
<link href="../css/account.css" type="text/css" rel="stylesheet">

<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td>
			<asp:ValidationSummary ShowMessageBox="False" ShowSummary="True" Class="formError" id="valSum" DisplayMode="BulletList" runat="server" HeaderText="Please correct the following errors:"/>
		</td>
	</tr>
</table>
<table cellpadding="0" cellspacing="0" width="990" align="center">
	<!--MAIN FRAME-->
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="1" height="10" /></td>
	</tr>	
	<tr>
		<td>
			<table border="0" cellpadding="0" cellspacing="0" align="left" width="100%" >									
				<tr>
					<td class="frTopLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
					<td class="frTop"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
					<td class="frTopRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
				</tr>
				<tr>
					<td class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" width="6" height="1" /></td>
					<td valign="top" class="frBgInt1" align="center" width="100%">
						<table cellpadding="0" cellspacing="0" align="left" width="100%">
							<tr>
								<td colspan="3"><img runat="server" src="~/images/spacer.gif" width="1" height="5" /></td>
							</tr>							
							<tr>
								<td width="5"><img runat="server" src="~/images/spacer.gif" width="5" height="1" /></td>
								<td>
								<!--inner frame-->
									<table border="0" cellpadding="0" cellspacing="0" align="left" width="100%" >
										<tr>
											<td colspan="3" class="accountText" align="left">&nbsp;&nbsp;List your instant messenger names you decide to share with friends or everyone</td>
											
										</tr>
										<tr>
											<td colspan="3"><img runat="server" src="~/images/spacer.gif" width="1" height="10"/></td>
										</tr>																			
										<tr>
											<td class="frTopLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
											<td class="frTop"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
											<td class="frTopRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
										</tr>
										<tr>
											<td class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" width="6" height="1" /></td>
											<td valign="top" class="frBgInt1" align="center" width="100%">
												<table cellpadding="0" cellspacing="0" align="left" width="100%">
													<tr>
														<td colspan="3"><img runat="server" src="~/images/spacer.gif" width="1" height="5" /></td>
													</tr>							
													<tr>
														<td width="5"><img runat="server" src="~/images/spacer.gif" width="5" height="1" /></td>
														<td>
														<!--inner frame-->
															<table border="0" cellpadding="0" cellspacing="0" width="100%" >
																<tr>
																	<td colspan="3">
																		<table cellspacing="0" cellpadding="0" border="0" align="left" width="100%">
																			<tr>
																				<td class="accountTitle" align="left">Instant Messaging</td>
																				<td class="accountText" align="center">Who can view?</td>
																				
																			</tr>
																		</table>
																	</td>

																</tr>
																<tr>
																	<td colspan="3"><img runat="server" src="~/images/spacer.gif" width="1" height="10"/></td>
																</tr>											
																<tr>
																	<td class="boxInsideTopLeft"><img runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
																	<td class="boxInsideTop" ><img runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
																	<td  class="boxInsideTopRight"><img runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
																</tr>
																<tr class="boxInside">
																	<td class="boxInsideleft" ><img runat="server" src="~/images/spacer.gif" width="1" height="4" /></td>
																	<td class="boxInsideContent">
																		<!--CONTENT-->
																		<table cellpadding="0" cellspacing="0" width="100%" border="0" align="center" >
																		
																				<tr>
																					<td colspan="6"><img runat="server" src="~/images/spacer.gif" width="4" height="10"/></td>
																				</tr>													
																				<tr>
																					<td align="right" valign="middle" width="150" class="accountText">
																								Username #1:&nbsp;
																					</td>
																					<td valign="middle" align="left">
																						<asp:TextBox ID="txtUsername1" class="formKanevaText" style="width:200px" MaxLength="50" runat="server"/>&nbsp;
																					</td>
																					<td><img runat="server" src="~/images/spacer.gif" width="10" height="1"/></td>
																					<td >
																																											
																						<asp:Dropdownlist runat="server" class="formKanevaSelect" ID="drpIM1" style="width:80px"/>
																					</td>
																					<td><img runat="server" src="~/images/spacer.gif" width="40" height="1"/></td>
																					<td align="left">
																						<asp:Dropdownlist runat="server" class="formKanevaSelect" ID="drpUsername1Rights" style="width:180px"/>
																					</td>
																				</tr>
																				<tr><td><img runat="server" src="~/images/spacer.gif" width="1" height="5"/></td></tr>
																				<tr>
																					<td align="right" valign="middle" width="150" class="accountText">
																								Username #2:&nbsp;
																					</td>
																					<td valign="middle" align="left">
																						<asp:TextBox ID="txtUsername2" class="formKanevaText" style="width:200px" MaxLength="50" runat="server"/>&nbsp;
																					</td>
																					<td><img runat="server" src="~/images/spacer.gif" width="10" height="1"/></td>
																					<td >
																																											
																						<asp:Dropdownlist runat="server" class="formKanevaSelect" ID="drpIM2" style="width:80px"/>
																					</td>
																					<td><img runat="server" src="~/images/spacer.gif" width="40" height="1"/></td>
																					<td align="left">
																						<asp:Dropdownlist runat="server" class="formKanevaSelect" ID="drpUsername2Rights" style="width:180px"/>
																					</td>
																				</tr>
																				<tr><td><img runat="server" src="~/images/spacer.gif" width="1" height="5"/></td></tr>
																				<tr>
																					<td align="right" valign="middle" width="150" class="accountText">
																								Username #3:&nbsp;
																					</td>
																					<td valign="middle" align="left">
																						<asp:TextBox ID="txtUsername3" class="formKanevaText" style="width:200px" MaxLength="50" runat="server"/>&nbsp;
																					</td>
																					<td><img runat="server" src="~/images/spacer.gif" width="10" height="1"/></td>
																					<td >
																																											
																						<asp:Dropdownlist runat="server" class="formKanevaSelect" ID="drpIM3" style="width:80px"/>
																					</td>
																					<td><img runat="server" src="~/images/spacer.gif" width="40" height="1"/></td>
																					<td align="left">
																						<asp:Dropdownlist runat="server" class="formKanevaSelect" ID="drpUsername3Rights" style="width:180px"/>
																					</td>
																				</tr>
																				<tr><td><img runat="server" src="~/images/spacer.gif" width="1" height="5"/></td></tr>
																				<tr>
																					<td align="right" valign="middle" width="150" class="accountText">
																								Username #4:&nbsp;
																					</td>
																					<td valign="middle" align="left">
																						<asp:TextBox ID="txtUsername4" class="formKanevaText" style="width:200px" MaxLength="50" runat="server"/>&nbsp;
																					</td>
																					<td><img runat="server" src="~/images/spacer.gif" width="10" height="1"/></td>
																					<td >
																																											
																						<asp:Dropdownlist runat="server" class="formKanevaSelect" ID="drpIM4" style="width:80px"/>
																					</td>
																					<td><img runat="server" src="~/images/spacer.gif" width="40" height="1"/></td>
																					<td align="left">
																						<asp:Dropdownlist runat="server" class="formKanevaSelect" ID="drpUsername4Rights" style="width:180px"/>
																					</td>
																				</tr>																					
																																									
																																									

																				<tr>
																					<td colspan="6"><img runat="server" src="~/images/spacer.gif" width="4" height="10"/></td>
																				</tr>																																						
																		</table>										
														
																			
																										
																<!-- end content-->	
																	</td>
																	<td class="boxInsideRight"><img runat="server" src="~/images/spacer.gif" width="1" height="4"/></td>
																</tr>
																<tr>
																	<td class="boxInsideBottomLeft" ><img runat="server" src="~/images/spacer.gif" width="4" height="4" /></td>
																	<td class="boxInsideBottom2"><img runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
																	<td class="boxInsideBottomRight"><img runat="server" src="~/images/spacer.gif" width="4" height="4" /></td>
																</tr>
															</table>					
								
														<!--end inner iframe--->									
														</td>
														<td width="5"><img runat="server" src="~/images/spacer.gif" width="5" height="1" /></td>
													</tr>
													<tr>
														<td colspan="3"><img runat="server" src="~/images/spacer.gif" width="1" height="5" /></td>
													</tr>								
												</table>
											</td>
											<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" width="6" height="1" /></td>
										</tr>
										<tr>
											<td class="frBottomLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
											<td class="frBottom"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
											<td class="frBottomRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
										</tr>
									</table>									
														
		
								<!--end inner iframe--->									
								</td>
								<td width="5"><img runat="server" src="~/images/spacer.gif" width="5" height="1" /></td>
							</tr>
							<tr>
								<td colspan="3"><img runat="server" src="~/images/spacer.gif" width="1" height="10" /></td>
							</tr>
							<tr>
								<td colspan="2" align="right">
									<asp:button id="btnCancel" runat="Server" CausesValidation="False" onClick="btnCancel_Click" class="Filter2" Text="  cancel  "/>&nbsp;&nbsp;<asp:button id="btnUpdate" runat="Server" onClick="btnUpdate_Click" class="Filter2" Text="  submit  "/>
								</td>
								<td><img runat="server" src="~/images/spacer.gif" width="1" height="10" /></td>
							</tr>																
						</table>
					</td>
					<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" width="6" height="1" /></td>
				</tr>
				<tr>
					<td class="frBottomLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
					<td class="frBottom"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
					<td class="frBottomRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
				</tr>
			</table>
		</td>
	</tr>
	<!--END MAIN FRAME--->							
</table>

<table cellpadding="4" cellspacing="0" border="0" width="100%">
<!--
	<tr>
		<td class="orangeHeader2" colspan="3">
			Email
		</td>
	</tr>
	<tr class="lineItemEven">
		<td colspan="2"><br>
		</td>
		<td class="filter2">
			Who can view?
		</td>
	</tr>
	<tr class="lineItemOdd">
		<td Width="150px" align="right" valign="top" class="filter2"><br>
			<b>Email:</b>&nbsp;
		</td>
		<td>
			<asp:Label CssClass="filter2" runat="server" id="lblEmail"/>
		</td>
		<td align="left">
			<asp:Dropdownlist runat="server" class="Filter2" ID="drpEmailRights" style="width:150px"/>
		</td>
	</tr>
-->	
</table>	

