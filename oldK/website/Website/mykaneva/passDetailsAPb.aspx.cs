///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
    public partial class passDetailsAPb : BasePage
    {
        #region Declarations

        private string _styleSheets = "\n<link href=\"../css/purchase_flow/passDetailsVIP.css\" rel=\"stylesheet\" type=\"text/css\" />";
        int promoID = 0;
        private bool showMaturePasses = true;

        #endregion

        #region Page load
        protected void Page_Load (object sender, EventArgs e)
        {
            if (GetRequestParams ())
            {
                //verify the user is registered and signed in - if not do not allow them to purchase
                //redirects user
                VerifyUserAuthenticated ();

                // if user meets eligibility sets a flag to allow them to see access pass packages
                VerifyMaturePassEligibility ();

                //load the images for the page based on provided promoID
                DisplayPassDetails ();

                // Set Nav and style sheets
                ((IndexPageTemplate) Master).HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
                ((IndexPageTemplate) Master).CustomCSS = _styleSheets;

            }
            else
            {
                Response.Redirect (ResolveUrl ("buyaccess.aspx"));
            }
        }
        #endregion

        #region Helper Functions

        private void DisplayPassDetails ()
        {
            try
            {
                if (showMaturePasses)
                {
                    //                   SubscriptionFacade subFac = new SubscriptionFacade ();

                    //get the Pass in question
                    //                   Subscription subscript = subFac.GetSubscriptionByPromotionId ((uint) promoID);

                    //set the display image and text area
                    //                                       div_passDetailsImage.Style.Add("background", "url('" + subscript.LearnMoreImage + "') no-repeat 0px 0px");

                    //display the selected pass details information
                    //                                       div_PassDetailsContent.InnerHtml = HttpUtility.HtmlDecode(subscript.LearnMoreContent);

                    //set the link button's postback URL
                    //                   lb_trialPass.PostBackUrl = ResolveUrl ("buySpecials.aspx?pass=true&passId=" + promoID);


                    //    div_passDetailsImage.Style.Add ("background", "url('../images/apvip/vip_billboard_dance_955x494.jpg') no-repeat 0px 0px");
                }
                else
                {
                    Response.Redirect ("~/mykaneva/underage.aspx");
                }
            }
            catch (Exception)
            {
            }
        }

        //get all possible parameters
        private bool GetRequestParams ()
        {
            bool requiredParamsGiven = true;

            //check to see if there is a transaction id provided
            if ((Request["pass"] == null) || (Request["pass"].ToString () != "true"))
            {
                requiredParamsGiven = false;
            }

            //assume there is a promoID provided and try to convert
            try
            {
                promoID = Convert.ToInt32 (Request["passID"]);
            }
            catch (FormatException)
            {
                requiredParamsGiven = false;
            }

            return requiredParamsGiven;
        }

        private void VerifyUserAuthenticated ()
        {
            // They must be logged in
            if (!Request.IsAuthenticated)
            {
                Response.Redirect (GetLoginURL ());
            }
        }

        private void VerifyMaturePassEligibility ()
        {

            SubscriptionFacade subFac = new SubscriptionFacade ();

            //get the Pass in question
            Subscription subscript = subFac.GetSubscriptionByPromotionId ((uint) promoID);

            //get all the pass groups associated with the subscription
            DataTable passGroups = subFac.GetPassGroupsAssociatedWSubscription (subscript.SubscriptionId);

            //process for each pass group associated with the subscription
            foreach (DataRow row in passGroups.Rows)
            {
                int passGroupId = Convert.ToInt32 (row["pass_group_id"]);
                if ((passGroupId == (int) KanevaGlobals.AccessPassGroupID) && (!KanevaWebGlobals.CurrentUser.IsAdult))
                {
                    showMaturePasses = false;
                }
            }
        }

        #endregion
    }
}