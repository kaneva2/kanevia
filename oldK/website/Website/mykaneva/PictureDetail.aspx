<%@ Page language="c#" Codebehind="PictureDetail.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.mykaneva.PictureDetail" %>
<%@ Register TagPrefix="Module" TagName="SinglePictureView" Src="widgets/ModuleSinglePictureView.ascx" %>

<link id="styleSheet" rel="stylesheet" href="../css/new.css" type="text/css"/>

<table cellpadding="0" cellspacing="0" border="0" class="newcontainer" align="center">
	<tr colspan="3">
		<td align="center">
			<table  border="0" cellpadding="0" cellspacing="0" width="100%" class="newcontainerborder">
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
				<tr>
					<td class=""><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
					<td valign="top" class="newdatacontainer" align="center">
					
						<table width="100%">
							<tr>
								<td align="left" class="header01">
									<asp:HyperLink id="hlBack" runat="server" Text="<< Back" /><br><br>
									<asp:Label id="lblName" runat="server" class="bodyBold" /></bold>&nbsp;uploaded by&nbsp;<asp:HyperLink id="hlUsername" runat="server" />&nbsp;on&nbsp;<asp:Label id="lblCreatedDate" runat="server" />
								</td>
							</tr>
							
							<tr>
								<td align="left">
									<Module:SinglePictureView runat="server" id="modSinglePictureView" />
								</td>
							</tr>

						</table>
					
					</td>
					<td class=""><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
				</tr>
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
			</table>				
		</td>
	</tr>
</table>