///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Collections.Generic;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
    public partial class getFriendList : System.Web.UI.Page
    {
        protected void Page_Load (object sender, EventArgs e)
        {
            // Return the friends list
            string friendName = "";

            if (Request["name"] != null)
            {
                friendName = Request["name"].ToString ();
            }

            UserFacade userFacade = new UserFacade ();
            PagedList<Friend> friends = userFacade.SearchFriends (KanevaWebGlobals.CurrentUser.UserId, "(display_name like '" + friendName + "%' OR username like '" + friendName + "%')", "display_name desc", 1, Int32.MaxValue);

            Response.Clear ();

            foreach (Friend friend in friends)
            {
                Response.Write (friend.DisplayName + " (" + friend.Username +")," + friend.UserId + ";");
            }
        }
    }
}
