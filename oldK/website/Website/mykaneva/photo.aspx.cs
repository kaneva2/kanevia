///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using log4net;

using System.Collections.Generic;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
    /// <summary>
    /// Summary description for photo.
    /// </summary>
    public class photo : MainTemplatePage
    {
        protected photo()
        {
            Title = "Photo";
        }

        private void Page_Load(object sender, System.EventArgs e)
        {
            // They must be logged in
            if (!Request.IsAuthenticated)
            {
                Response.Redirect(GetLoginURL());
                return;
            }

            int userId = GetUserId();
            lblMaxSize.Text = KanevaGlobals.FormatImageSize(KanevaGlobals.MaxUploadedAvatarSize);

            if (!IsPostBack)
            {
                imgAvatar.Src = UsersUtility.GetProfileImageURL(KanevaWebGlobals.CurrentUser.ThumbnailLargePath, "la", KanevaWebGlobals.CurrentUser.Gender);
                imgAvatar.Alt = KanevaWebGlobals.CurrentUser.Username;

                //if UrlReferrer comes from ModuleEditor don't process url
                //fix for FireFox reopening edit dialog window when coming from change image in the edit window
                //(yes this is hacky but can't think of something better off hand)
                if (Request.UrlReferrer != null)
                {
                    if(Request.UrlReferrer.ToString().IndexOf("ModuleEditor") >= 0)
                    {
                        ViewState["rurl"] =  GetPersonalChannelUrl (KanevaWebGlobals.CurrentUser.NameNoSpaces);
                    }
                    else
                    {
                        ViewState["rurl"] = Request.UrlReferrer.AbsoluteUri.ToString();
                    }
                }
                else
                {
                    ViewState["rurl"] = ResolveUrl("~/default.aspx");
                }
            }

            btnUpload.Enabled = false;

            // Set Nav
            HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
            HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.PROFILE;
            HeaderNav.MyChannelsNav.ActiveTab = NavMyChannels.TAB.PROFILE_PHOTO;
            HeaderNav.MyChannelsNav.ChannelId = GetPersonalChannelId();

            HeaderNav.SetNavVisible(HeaderNav.MyKanevaNav, 2);
            HeaderNav.SetNavVisible(HeaderNav.MyChannelsNav);
        }

        /// <summary>
        /// Upload an avatar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnUpload_Click(object sender, EventArgs e)
        {
            CommunityFacade communityFacade = new CommunityFacade();

            // They must be logged in
            if (!Request.IsAuthenticated)
            {
                Response.Redirect(GetLoginURL());
                return;
            }

            // Files are now stored in content repository
            string contentRepository = "";
            if (KanevaGlobals.ContentServerPath != null)
            {
                contentRepository = KanevaGlobals.ContentServerPath;
            }
            else
            {
                ShowErrorOnStartup("No Content server path (content repository)", false);
                return;
            }

            //declare the file name of the image
            string filename = null;

            //create PostedFile
            HttpPostedFile File = inpAvatar.PostedFile;

            try
            {
                //generate newpath
                string newPath = Path.Combine(contentRepository, KanevaWebGlobals.GetUserId().ToString());

                //upload image from client to image repository
                ImageHelper.UploadImageFromUser(ref filename, File, newPath, KanevaGlobals.MaxUploadedAvatarSize);

                //generate newpath and channel id
                string imagePath = newPath + Path.DirectorySeparatorChar + filename;
                int channelId = KanevaWebGlobals.CurrentUser.CommunityId;
                communityFacade.UpdateCommunity(channelId, KanevaWebGlobals.GetUserId(), true, imagePath, File.ContentType, true);

                //create any and all thumbnails needed
                ImageHelper.GenerateAllChannelThumbnails(filename, imagePath, contentRepository, GetUserId(), channelId);

                imgAvatar.Src = UsersUtility.GetProfileImageURL(KanevaWebGlobals.CurrentUser.ThumbnailLargePath, "la", KanevaWebGlobals.CurrentUser.Gender);
                imgAvatar.Alt = KanevaWebGlobals.CurrentUser.Username;

                // User completed fame packet
                try
                {
                    FameFacade fameFacade = new FameFacade ();
                    fameFacade.RedeemPacket (KanevaWebGlobals.GetUserId (), (int)PacketId.WEB_OT_ADD_PICTURE, (int)FameTypes.World);
                }
                catch (Exception ex)
                {
                    m_logger.Error ("Error awarding Packet WEB_OT_ADD_PICTURE, userid=" + KanevaWebGlobals.GetUserId (), ex);
                }

                Response.Redirect(ViewState["rurl"].ToString());
            }
            catch (Exception ex)
            {
                ShowErrorOnStartup(ex.Message, false);
            }
        }

        protected HtmlInputFile inpAvatar;
        protected Label lblMaxSize;
        protected HtmlImage imgAvatar;
        protected Button btnUpload;

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}
