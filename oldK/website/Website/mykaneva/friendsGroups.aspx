<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="friendsGroups.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.mykaneva.friendsGroups" %>
<%@ Register TagPrefix="Kaneva" TagName="StoreFilter" Src="../usercontrols/StoreFilter.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../usercontrols/Pager.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="SearchFilter" Src="../usercontrols/SearchFilter.ascx" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>
<%@ Register TagPrefix="UserControl" TagName="FacebookJS" Src="~/usercontrols/FacebookJavascriptInclude.ascx" %>

<!--CSS file (default YUI Sam Skin) -->
<link href="../css/new.css" type="text/css" rel="stylesheet">
<link href="../css/yahoo/autocomplete/skins/sam/autocomplete.css" rel="stylesheet" type="text/css">


<script src="../jscript/prototype.js" type="text/javascript" language="javascript"></script>

<!-- Dependencies -->
<script type="text/javascript" src="../jscript/yahoo/2.3.0/yahoo-min.js"></script>
<script type="text/javascript" src="../jscript/yahoo/2.3.0/dom-min.js"></script>
<script type="text/javascript" src="../jscript/yahoo/2.3.0/yahoo-dom-event.js"></script>
									
<script type="text/javascript" src="../jscript/yahoo/2.3.0/connection-min.js"></script>  
<script type="text/javascript" src="../jscript/yahoo/2.3.0/autocomplete-min.js"></script>
<script type="text/javascript" src="../jscript/yahoo/2.3.0/animation-min.js"></script>

<script type="text/javascript">

function IsIE ()
{
	return ((navigator.userAgent.indexOf('MSIE') != -1) && (navigator.userAgent.indexOf('Win') != -1));
}

function checkKey (e, obj) 
{	
	// check for enter key
	if (checkForEnterKey (e))
	{                      
		if (obj.id == 'txtFriendName')
		{
			if ($('btnSearch').style.display != 'none')
			{
				 $('btnSearch').focus();  
				 if (!IsIE()) {$('btnSearch').click();}  
			}
			else if ($('btnAddToGroup').style.display != 'none')
			{
				 $('btnAddToGroup').focus();
				 if (!IsIE()) {$('btnAddToGroup').click();}
			}
		}
		else if (obj.id == 'txtNewGroup')
		{
			$('btnSaveGroup').focus();
			if (!IsIE()) {$('btnSaveGroup').click();}
		}
	} 
}

function addFriendToGroup ()
{
    var strName = $('txtFriendName').value.replace(/\s{1,}/g," ").strip();
	
    // if text box is empty, return
    if (strName.length < 1) return;

    $('hidFriendName').value = strName;	
 	 
	AJAXCbo.DoAjaxCall('btnAdd','','async');  
}
</script>

<script type="text/JavaScript">
function changeBG(obj) {
	obj.style.background = "url('../images/media/tabsMedia.gif')";
}
function changeBG2(objeto) {
	objeto.style.background = "url('../images/media/tabsMedia_on.gif')";
}
function changeBGB(objeto) {
	objeto.style.background = "url('../images/media/tabsMedia2.gif')";
}
function changeBGB2(objeto) {
	objeto.style.background = "url('../images/media/tabsMedia2_on.gif')";
}

function CheckAll(t) 
{
	Select_All(t.checked);
}
</script>

<style type="text/css">
.groupSelected {background-color:yellow;cursor:hand;}
.groupNotSelected {background-color:white;cursor:hand;}
#ulFriendGroups a:hover {background-color:#214f73;color:#b3d6f2;cursor:hand;}
.display{position:static !important;position:realtive;}
.btnPadding{padding-left:48px;_padding-left:30px;	/* IE only hack */}
.fb {margin:20px 0;}
.fb h2 {clear:both;margin-bottom:6px;font-size:14px;text-align:center;color:#073b53;}
.fb img {margin:0;padding:0;clear:both;cursor:hand;}
.framesize-square, div.frame {width:40px;height:40px;background-color:transparent;padding:0;text-align:left;}
div.frame {overflow:hidden;}
</style>
<usercontrol:FacebookJS runat="server"></usercontrol:FacebookJS>
<div id="fb-root"></div>

<div id="divLoading">
	<table height="450" cellSpacing="0" cellPadding="0" width="100%" border="0" id="Table1">
		<tr>
			<th class="loadingText" id="divLoadingText">Loading...</th>
		</tr>
	</table>
</div>
					
<div id="divData" style="display: none">
	<!-- Main Body structure table -->
	<table cellSpacing="0" cellPadding="0" class="newcontainer" align="center" border="0">
		<tr>
			<td>
				<table cellSpacing="0" cellPadding="0" width="100%" border="0" class="newcontainerborder">
					<tr>
						<td class=""></td>
						<td class=""></td>
						<td class=""></td>
					</tr>
					<tr>
						<td class=""><img id="IMG7" height="1" runat="server" src="~/images/spacer.gif" width="1"></td>
						<td class="newdatacontainer" vAlign="top">
							<table cellSpacing="0" cellPadding="0" width="100%" border="0"> 
							<!-- BEGIN TOP STATUS BAR -->
								<tr>
									<td>
										<div id="pageheader">
											<table cellSpacing="0" cellPadding="0" width="99%" border="0">
												<tr>
													<td align="left">
														<h1>My Friends Groups</h1>
													</td>
													<td vAlign="middle" align="right">
															
														<ajax:ajaxpanel id="ajpFilterPanel" runat="server">

															<table cellSpacing="0" cellPadding="0" width="700" border="0">
																<tr> 
																	<!-- Begin All Sort Filter --->
																	<td align="left">
																		<table id="filterTop_tblFilter" cellSpacing="2" cellPadding="2" width="100%" border="0">
																			<tr>
																				<td align="center">
																					<Kaneva:StoreFilter id="storeFilter" runat="server" AssetType="0" HideAlphaNumericsPullDown="false" HideAlphaNumerics="true"
																						HideItemsPerPagePullDown="true" HideThumbView="True" IsAjaxMode="false" onfilterchanged="storeFilter_FilterChanged"></Kaneva:StoreFilter></td>
																			</tr>
																		</table>
																	</td> 
																	<!-- End All Sort Filter ---> 
																	<!-- Begin Show Filter --->
																	<td align="left">
																		<table id="filterTop_tblFilter" cellSpacing="0" cellPadding="0" width="100%" border="0">
																			<tr>
																				<td align="center">
																					<kaneva:searchfilter runat="server" id="searchFilter" loc="top" onfilterchanged="searchFilter_FilterChanged"
																						hidethumbview="true" showfilterlists="false" assetsperpagelist="10,20,50,100" />
																				</td>
																			</TR>
																		</table>
																	</td> 
																	<!-- End Show Filter ---> 
																	<!-- Begin page results Filter --->
																	<td class="searchnav" nowrap align="right" width="168">
																		<asp:label runat="server" id="lblResults_Top" cssclass="insideTextNoBold" /><br />
																		<kaneva:pager runat="server" isajaxmode="True" id="pgTop" onpagechanged="pg_PageChange" maxpagestodisplay="5" shownextprevlabels="true" />
																	</td> 
																	<!-- End page results Filter --->
																</tr>
															</table>
															
														</ajax:ajaxpanel>
															
													</td>
												</tr>
											</table>
										</div>
									</td>
								</tr>
								<!-- END TOP STATUS BAR --> 
								<!-- BEGIN MAIN BODY -->
								<tr>
									<td valign="top" align="left" width="968">
										<table cellspacing="0" cellpadding="0" width="100%" border="0">
											<tr>
												<!-- Start Side Panel Controls -->
												<td valign="top" align="center" width="246">
														
													<ajax:ajaxpanel id="ajpFriendGroups" runat="server">

														<table cellspacing="0" cellpadding="0" width="100%" border="0">
															<tr>
																<td align="center">
																	
																	<!-- smart nav div-->
																	<div class="module">
																		<span class="ct"><span class="cl"></span></span>
																		<h2>Groups</h2>
																		<ul id="librarynav">
																			
																			<asp:repeater id="rptFriendGroups" runat="server" onitemcommand="rptFriendGroups_ItemCommand" onitemdatabound="rptFriendGroups_ItemDataBound">
													
																				<itemtemplate>
																				
																					<li id="liFriendGroup" runat="server" class="friendgroup">
																						&nbsp;<asp:linkbutton id="lbFriendGroup" runat="server" name='<%# Server.HtmlDecode(DataBinder.Eval(Container.DataItem, "name").ToString ()) %>'>
																							<span class="navicon"></span><%# TruncateWithEllipsis (Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "name").ToString ()), 20)%>
																							<span style="font-size:11px;">(<%# DataBinder.Eval(Container.DataItem, "friend_count") %>)</span>
																						</asp:linkbutton>
																					</li>
																					
																				</itemtemplate>
																				
																			</asp:repeater>
																			
																		</ul>
																					
																		<br />			
																		<div id="divAddButton" style="text-align:left;" class="btnPadding">
																			<asp:button id="btnAddNewGroup" style="display:block;" onclientclick="$('divAddButton').style.display='none';$('divAddTxtBox').style.display='block';$('txtNewGroup').focus();return false;" runat="server" text="Create New Group" />
																		</div>
																		<div id="divAddTxtBox" style="display:none;width:100%;">
																			<asp:textbox id="txtNewGroup" runat="server" style="width:130;" value="" maxlength="60" onkeypress="return checkKey(event, this);" />
																			<asp:button id="btnSaveGroup" runat="server" text="Save" onclick="btnSaveGroup_Click" />
																		</div>
											
																		<span class="cb"><span class="cl"></span></span>
																	</div>
																				
																	<div class="fb" id="fbConnect" runat="server" visible="false">
																		<h2>Invite Your Friends!</h2>
																		<img onclick="FBLogin(FBAction.CONNECT);" runat="server" src="~/images/facebook/FB_ConnectwithFriends.png" />
																		<div>or you can invite friends by <a href="~/mykaneva/invitefriend.aspx" runat="server">email</a></div>
																	</div>
																	
																	<a href="~/mykaneva/invitefriend.aspx" runat="server" id="aInvite" visible="false"><img src="~/images/facebook/Grey_InviteFriendsnarrow.png" id="btnInvite" runat="server" visible="false" border="0" /></a>		
																</td>
															</tr>
														</table>
														
													</ajax:ajaxpanel>
														
												</td>
												<!-- end Side Panel Controls -->
												<td width="14">
													<img id="IMG10" height="1" runat="server" src="~/images/spacer.gif" width="14">
												</td>
												<!-- BEGIN MEDIA DISPLAY AREA -->
												<td valign="top" width="708">
													<table cellspacing="0" cellpadding="0" width="100%" border="0">
														<tr>
															<td>
																<!--START RIGHTCOL-->
																<div class="module whitebg">
																	<span class="ct"><span class="cl"></span></span>
																		
																		<ajax:ajaxpanel id="ajpFriendBar" runat="server">
	
																			<table cellspacing="0" cellpadding="0" width="98%" border="0">
																				<tr style="height: 42px">
																					<td align="left" valign="top">
																						<h2><asp:label runat="server" id="lblGroupName" /></h2>
																						<asp:label style="position:relative;top:-6;" runat="server" id="messages" visible="true" />
																					</td>
																				</tr>
																			</table>
																							
																			<table id="controlbox" cellspacing="0" cellpadding="0" border="0" style="width:701px;height:40px;">
																				<!-- Begin Media management controls -->
																				<tr>
																					<!-- delete selected friend (button) -->
																					<td align="center" width="30%">
																						<table id="TABLE4" cellspacing="0" cellpadding="0" width="160" border="0">
																							<tr>
																								<td align="left"></td>
																							</tr>
																						</table>
																					</td>
																					<!-- remove selected friend from group (button) -->
																					<td align="center" width="37%">
																						<table id="TABLE2" cellspacing="0" cellpadding="0" width="184" border="0">
																							<tr>
																								<td align="left">
																									<asp:linkbutton class="button" id="lbnRemoveFromGroup" onclick="lbnRemoveFromGroup_Click" title="Remove selected friends from friend group." runat="server" causesvalidation="False" text="Remove from Group">Remove from Group
																										<img src="../images/btnico_delete.gif" title="Remove selected friends from friend group." width="24" height="24" border="0" style="margin-right:2px;_margin-right:22px;">
																									</asp:linkbutton>
																									
																									<span id="spnFriendsToGroup" runat="server">
																									<asp:dropdownlist id="ddlGroupNames" title="Add selected friends to a group." width="152px" runat="server" autopostback="False" style="font-size:12px;margin-top:3px;">
																									</asp:dropdownlist>
																									<asp:linkbutton class="nohover" id="lbnFriendsToGroup" onclick="lbnFriendsToGroup_Click" title="Add selected friends to a group." runat="server" causesvalidation="False">
																										<img src="../images/btnico_add.gif" width="24" title="Add selected friends to a group." height="24" border="0" />
																									</asp:linkbutton></span>
																								</td>
																							</tr>
																						</table>
																					</td>
																				
																					<!-- delete friend from group or friends list -->
																					<td align="center">
																						<table id="TABLE5" cellspacing="0" cellpadding="0" width="160" border="0">
																							<tr>
																								<td align="left">
																									<asp:linkbutton class="button" id="lbnDeleteFriend" onclick="lbnDeleteFriend_Click" title="Delete selected people from your friends." runat="server" causesvalidation="False" text="Delete from Friends">Delete from Friends
																										<img src="../images/btnico_delete.gif" title="Delete selected people from your friends." width="24" height="24" border="0">
																									</asp:linkbutton>
																								
																									<asp:linkbutton class="button" id="lbnDeleteFriendGroup" title="Delete current friend group." runat="server" onclick="lbnDeleteFriendGroup_Click" causesvalidation="False" text="Delete This Group">Delete This Group
																										<img href="#" src="../images/btnico_delete.gif" title="Delete current friend group." width="24" height="24" border="0" style="margin-right:2px;_margin-right:16px;">
																									</asp:linkbutton>
																								</td>
																							</tr>
																						</table>
																					</td>
																				</tr>
																			</table>
																		
																		</ajax:ajaxpanel>
																		
																		<!-- END Media management controls -->
																		<!-- Begin Media display region -->
																		<table width="701" cellspacing="0" cellpadding="0" border="0" class="data">
																			<tr>
																				<td colspan="3" height="20px">
																					<table width="99%" cellspacing="0" cellpadding="0" border="0" height="34px">
																						<tr>
																							<td align="left" valign="top">
																								<!-- basic search -->
																								<table cellpadding="0" cellspacing="0" border="0" width="99%">
																									<tr>
																										<td width="100px" align="right">
																								
																											<ajax:ajaxpanel id="ajpSearchLabel" runat="server">
																												<h4 id="h4SearchLabel" runat="server" style="text-align:right;width:100%;">Add to Group:</h4>
																											</ajax:ajaxpanel>
																										 
																										</td>
																										
																										<td width="250px" align="left" class="yui-skin-sam">
																											<div id="myAutoComplete" style="display:inline;">
																												<input type="text" style="width:200px;" class="display" id="txtFriendName" 
																													onkeypress="checkKey(event,this);" 
																													maxlength="51" />
																												<div id="myContainer"></div>  
																											</div>
																										</td>
																								
																										<td align="left">
																										
																											<ajax:ajaxpanel id="ap2" runat="server">
																												<asp:button runat="server" id="btnSearch" text="Find Friend" style="width:120px;" onclick="btnSearch_Click" onclientclick="javasript:$('hidFriendName').value=$('txtFriendName').value;" />
																												<asp:button runat="server" id="btnAddToGroup" text="Add to Group" style="width:120px;" onclientclick="addFriendToGroup()"  />
																											</ajax:ajaxpanel>
																										
																										</td>
																									</tr>
																								</table>
																								
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																	
																			<tr>
																				<td colspan="3" style="padding:0;">
																		
																					<ajax:ajaxpanel id="ajpFriendRepeater" runat="server">
													
																						<asp:repeater id="rptFriends" runat="server">
																								
																							<headertemplate>
																								<table cellpadding="0" border="0" cellspacing="0" width="100%" style="z-index:1;">
																									<tr>
																										<td width="30" align="center" style="padding:0;border-bottom:1px solid #f1f1f1;">
																											<asp:checkbox id="cbxSelectAll" onclick="CheckAll(this);" runat="server"  />
																										</td>
																										<td align="left" nowrap style="border-bottom:1px solid #f1f1f1;border-left:1px solid #f1f1f1;">Name<img id="IMG3" height="1" runat="server" src="~/images/spacer.gif" width="490"></td>
																										<td align="left" style="border-bottom:1px solid #f1f1f1;border-left:1px solid #f1f1f1;">Status<img id="IMG8" height="1" runat="server" src="~/images/spacer.gif" width="60"></td>
																									</tr>
																							</headertemplate>
																					
																							<itemtemplate>
																								<tr>
																									<td width="30" align="center" style="padding:0;border-bottom: 1px solid #f1f1f1;">
																										<asp:checkbox id="chkEdit" cssclass="Filter2" runat="server" />
																										<input type="hidden" runat="server" id="hidUserId" value='<%#DataBinder.Eval(Container.DataItem, "UserId")%>' name="hidUserId">
																									</td>
																									<td style="border-bottom:1px solid #f1f1f1;border-left:1px solid #f1f1f1;">
																										<!--image holder-->
																										<table border="0" cellpadding="0" cellspacing="0" width="491" class="nopadding">
																											<tr>
																												<td align="right" width="36">
																													<!-- FRIENDS BOX -->
																													<div class="framesize-square">
																														<div class="frame" class="width:40px;overflow:hidden;">
																															<a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "NameNoSpaces").ToString ())%>>
																															<img id="Img9" runat="server" src='<%#GetProfileImageURL (DataBinder.Eval(Container.DataItem, "ThumbnailSquarePath").ToString (), "sq", DataBinder.Eval(Container.DataItem, "Gender").ToString (), Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "FacebookSettings.UseFacebookProfilePicture")), Convert.ToUInt64(DataBinder.Eval(Container.DataItem, "FacebookSettings.FacebookUserId")) )%>' border="0" width="40" />
																															</a>
																														</div>
																													</div>		
																													<!-- END FRIENDS BOX -->
																												</td>
																												<td>
																													<div style="margin-left:10px;"><a runat="server" id="mediaTitle" style="font-size: 110%; text-decoration: none" href='<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "NameNoSpaces").ToString ())%>'><%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "UserName").ToString ()) %></a>
																													<br /><span style="font-size:10px;"><%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "DisplayName").ToString ())%></span></div>
																												</td>
																											</tr>
																										</table>
																										<!--end image holder-->
																									</td>
																									<td valign="middle" align="left" style="padding:0;border-bottom:1px solid #f1f1f1;border-left:1px solid #f1f1f1;"><%# GetOnlineText (DataBinder.Eval(Container.DataItem, "Ustate").ToString ()) %></td>
																								</tr>  
																							</itemtemplate>
																					
																							<alternatingitemtemplate>
																								<tr class="altrow">
																									<td width="30" align="center" style="padding:0;border-bottom:1px solid #f1f1f1;">
																										<asp:checkbox id="chkEdit" cssclass="Filter2" runat="server" />
																										<input type="hidden" runat="server" id="hidUserId" value='<%#DataBinder.Eval(Container.DataItem, "UserId")%>' name="hidUserId">
																									</td>
																									<td style="border-bottom:1px solid #f1f1f1;border-left:1px solid #f1f1f1;">
																										<!--image holder-->
																										<table border="0" cellpadding="0" cellspacing="0" width="491" class="nopadding">
																											<tr>
																												<td align="right" width="36">
																													<!-- FRIENDS BOX -->
																													<div class="framesize-square">
																														<div class="frame">
																															<a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "NameNoSpaces").ToString ())%>>
																															<img id="Img4" runat="server" src='<%#GetProfileImageURL (DataBinder.Eval(Container.DataItem, "ThumbnailSquarePath").ToString (), "sq", DataBinder.Eval(Container.DataItem, "Gender").ToString (), Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "FacebookSettings.UseFacebookProfilePicture")), Convert.ToUInt64(DataBinder.Eval(Container.DataItem, "FacebookSettings.FacebookUserId")) )%>' border="0" width="40"/>
																															</a>
																														</div>
																													</div>		
																													<!-- END FRIENDS BOX -->
																												</td>
																												<td>
																													<div style="margin-left:10px;"><a runat="server" id="A3" style="font-size: 110%; text-decoration: none" href='<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "NameNoSpaces").ToString ())%>'><%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "UserName").ToString ()) %></a>
																													<br /><span style="font-size:10px;"><%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "DisplayName").ToString ())%></span></div>
																												</td>
																											</tr>
																										</table>
																										<!--end image holder-->
																									</td>
																									<td valign="middle" align="left" style="padding:0;border-bottom:1px solid #f1f1f1;border-left:1px solid #f1f1f1;"><%# GetOnlineText (DataBinder.Eval(Container.DataItem, "Ustate").ToString ()) %></td>
																								</tr>  
																							</alternatingitemtemplate>
																					
																							<footertemplate>
																								</table>
																							</footertemplate>
																					
																						</asp:repeater>
																									
																					</ajax:ajaxpanel>
																			
																				</td>
																			</tr>
																			
																		</table>
																	
																	<span class="cb"><span class="cl"></span></span>
																</div>
																<!--END RIGHTCOL-->
															</td>
														</tr>
													</table>
												</td>
												<!-- END MEDIA DISPLAY AREA -->
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td valign="top" align="right" style="padding-right:8px;">
									
										<ajax:ajaxpanel id="ajpBottomPager" runat="server">
											<img id="IMG11" height="10" runat="server" src="~/images/spacer.gif" width="1">
											<asp:label runat="server" id="lblResults_Bottom" cssclass="insideTextNoBold" /><br />
											<kaneva:pager id="pgBottom" onpagechanged="pg_PageChange" maxpagestodisplay="5" runat="server" isajaxmode="True" shownextprevlabels="true"></kaneva:pager>
										</ajax:ajaxpanel>
										
									</td>
								</tr>
							</table>
						</td>
						<td class="">
							<img id="Img5" height="1" runat="server" src="~/images/spacer.gif" width="1">
						</td>
					</tr>
					<tr>
						<td class=""></td>
						<td class=""></td>
						<td class=""></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<!-- END MAIN BODY -->
	<input id="AssetId" type="hidden" value="0" runat="server" name="AssetId">
	<asp:button id="DeleteAsset"  runat="server" visible="false"></asp:button>
</div>
										
									

<ajax:ajaxpanel runat="server">
<asp:button id="btnAdd" onclick="btnAdd_Click" runat="server" style="display:none;"/>
<input type="hidden" value=""  id="hidFriendName" runat="server" />
<input type="hidden" value="0" id="hidFriendId" runat="server" />
<input type="hidden" value="" id="hidGroupName" runat="server" />
<input type="hidden" value="0" id="hidGroupId" runat="server" />
</ajax:ajaxpanel>



<HEAD><META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE"></HEAD>
<script type="text/javascript">   

// An XHR DataSource
var myServer = "./friends/getFriendList.aspx";
var myDataSource = new YAHOO.widget.DS_XHR(myServer, [";", ","]);

myDataSource.responseType = YAHOO.widget.DS_XHR.TYPE_FLAT;
myDataSource.scriptQueryParam = "name";  

var myAutoComp = new YAHOO.widget.AutoComplete('txtFriendName','myContainer', myDataSource);   
myAutoComp.minQueryLength = 2;
myAutoComp.typeAhead = false;
myAutoComp.useShadow = true; 
myAutoComp.allowBrowserAutocomplete = false;
myAutoComp.forceSelection = true;

myAutoComp.itemSelectEvent.subscribe(onItemSelect);

function onItemSelect (sType, aArgs)
{
	try
	{
		var sArgs = new String(aArgs[2]);
		var aData = sArgs.split(",")
		$('hidFriendId').value = aData[1];	  
	}
	catch (err)
	{
		$('hidFriendId').value = 0;	 
	}
}
	
</script>

<style type="text/css">
/* styles for this implementation */
#myAutoComplete {
    width:15em; /* set width for widget here */
    padding-bottom:2em;
    display:block;
}

* {
    font-family: Verdana, Helvetica;
    font-size: 10pt;
}
.highslide-html {
    background-color: white;
}
.highslide-html-blur {
}
.highslide-html-content {
	position: absolute;
    display: none;
}
.highslide-loading {
    display: block;
	color: black;
	font-size: 8pt;
	font-family: sans-serif;
	font-weight: bold;
    text-decoration: none;
	padding: 2px;
	border: 1px solid black;
    background-color: white;
    
    padding-left: 22px;
    background-image: url(highslide/graphics/loader.white.gif);
    background-repeat: no-repeat;
    background-position: 3px 1px;
}
a.highslide-credits,
a.highslide-credits i {
    padding: 2px;
    color: silver;
    text-decoration: none;
	font-size: 10px;
}
a.highslide-credits:hover,
a.highslide-credits:hover i {
    color: white;
    background-color: gray;
}


/* Styles for the popup */
.highslide-wrapper {
	background-color: white;
}
.highslide-wrapper .highslide-html-content {
    width: 400px;
    padding: 5px;
}
.highslide-wrapper .highslide-header div {
}
.highslide-wrapper .highslide-header ul {
	margin: 0;
	padding: 0;
	text-align: right;
}
.highslide-wrapper .highslide-header ul li {
	display: inline;
	padding-left: 1em;
}
.highslide-wrapper .highslide-header ul li.highslide-previous, .highslide-wrapper .highslide-header ul li.highslide-next {
	display: none;
}
.highslide-wrapper .highslide-header a {
	font-weight: bold;
	color: gray;
	text-transform: uppercase;
	text-decoration: none;
}
.highslide-wrapper .highslide-header a:hover {
	color: black;
}
.highslide-wrapper .highslide-header .highslide-move a {
	cursor: move;
}
.highslide-wrapper .highslide-footer {
	height: 11px;
}
.highslide-wrapper .highslide-footer .highslide-resize {
	float: right;
	height: 11px;
	width: 11px;
	background: url(highslide/graphics/resize.gif);
}
.highslide-wrapper .highslide-body {
}
.highslide-move {
    cursor: move;
}
.highslide-resize {
    cursor: nw-resize;
}

/* These must be the last of the Highslide rules */
.highslide-display-block {
    display: block;
}
.highslide-display-none {
    display: none;
}
</style>
