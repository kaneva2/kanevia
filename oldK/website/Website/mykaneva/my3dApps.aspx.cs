///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Web.SessionState;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.mykaneva
{
	public partial class my3dApps : BasePage
	{
		protected void Page_Load(object sender, EventArgs e)
		{									
			//check to see if the user is logged in
			if (Request.IsAuthenticated)
			{				
				//set the navigation
				SetNav();

				if (!IsPostBack)
				{
					GetRequestParams ();
                    BindApps (1, GetUserId ());
				}
			}
			else
			{
				Response.Redirect(this.GetLoginURL());
			}

		}

        #region Helper Methods

        private void SetNav()
		{
			// Set Nav
			((GenericPageTemplate)Master).HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
			((GenericPageTemplate)Master).HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.MY3DAPPS;
			((GenericPageTemplate)Master).HeaderNav.SetNavVisible(((GenericPageTemplate)Master).HeaderNav.MyKanevaNav, 6);			
		}

        private void BindApps (int pageNumber, int userId)                                                                      
		{
            int worldsPerPage = 10;

            int[] communityTypeIds = { Convert.ToInt32 (CommunityType.APP_3D), Convert.ToInt32 (CommunityType.COMMUNITY), Convert.ToInt32 (CommunityType.HOME), Convert.ToInt32 (CommunityType.FAME) };
            PagedList<Community> worlds = GetCommunityFacade.GetUserTopWorlds (KanevaWebGlobals.CurrentUser.UserId, communityTypeIds, OrderBy, SearchFilter, pageNumber, worldsPerPage, IncludeCreditBalance, KanevaGlobals.PremiumItemDaysPendingBeforeRedeem);

            rptApps.DataSource = worlds;
			rptApps.DataBind();


            pgTop.NumberOfPages = Math.Ceiling ((double) worlds.TotalCount / worldsPerPage).ToString ();
			pgTop.DrawControl();

            // The results                                                      
            lblSearch.Text = GetResultsText (worlds.TotalCount, pageNumber, worldsPerPage, worlds.Count);

            if (worlds.TotalCount < 1)
			{
				//TODO show msg in the page
				//rnNavigation.NoDataMsg = "You belong to 0 communities.";
				//rnNavigation.NoDataMsg_Visible = true;
			}
		}
					 
		private string GetNotificationSummary(int val)
		{
			string notify = string.Empty;

			switch (val)
			{
				case (int)Constants.eCOMMUNITY_NOTIFICATIONS.DAILY_EMAILS:
					{
						notify = "Daily";
						break;
					}
				case (int)Constants.eCOMMUNITY_NOTIFICATIONS.WEEKLY_EMAILS:
					{
						notify = "Weekly";
						break;
					}
				case (int)Constants.eCOMMUNITY_NOTIFICATIONS.INDIVIDUAL_EMAIL:
					{
						notify = "As they happen";
						break;
					}
				default:
					{
						notify = "Never";
						break;
					}
			}

            return "I want World updates: <strong>" + notify + "</strong>";
		}

		private string GetDeleteScript()
		{
			StringBuilder sbDelete = new StringBuilder();
			sbDelete.Append(" javascript: ");
			sbDelete.Append(@" if(!confirm('Are you sure you want to delete this World? \r");
			sbDelete.Append(@"This action will delete all forums, blogs, and everything \r");
			sbDelete.Append("associated with this World.')) return false;");
			return sbDelete.ToString();
		}

		private string GetQuitScript()
		{
			StringBuilder sbQuit = new StringBuilder();
			sbQuit.Append(" javascript: ");
			sbQuit.Append(" if(!confirm('Are you sure you want to quit selected Worlds?')) return false;");
			return sbQuit.ToString();
		}

        private void SetOrderBy (string orderByVal)
        {
            switch (orderByVal)
            {
                case "1":
                    OrderBy = "c.created_date DESC";
                    break;
                case "2":
                    OrderBy = "c.name ASC";
                    break;
                case "3":
                    SearchFilter = "c.creator_id = " + KanevaWebGlobals.CurrentUser.UserId;
                    break;
                case "4":
                    OrderBy = "world_credit_balance DESC, c.name ASC";
                    IncludeCreditBalance = true;
                    break;
                default:
                    OrderBy = "last_touched DESC";
                    break;
            }

            ddlSortBy.SelectedValue = orderByVal;
        }

        private void GetRequestParams ()
        {
            try
            {
                if (Request["sortby"] != null)
                {
                    if (Request["sortby"].ToString () == "World Credit Balance")
                    {
                        SetOrderBy ("4");
                    }
                    else
                    {
                        SetOrderBy (Request["sortby"].ToString ());
                    }
                }
                else
                {
                    SetOrderBy ("1");
                }
            }
            catch { SetOrderBy ("1"); }
        }

        #endregion Helper Methods

        #region Event Handlers

        public void pgTop_PageChange (object sender, PageChangeEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(ddlSortBy.SelectedValue))
            {
                SetOrderBy(ddlSortBy.SelectedValue);
            }

            BindApps (e.PageNumber, GetUserId ());
        }

		public void btnDownloadResources_OnClick(Object sender, EventArgs e)
		{
			Response.Redirect(KanevaGlobals._3dAppDownloadResourcesURL);
		}
        
        public void rptApps_ItemDataBound (object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                int communityId = Convert.ToInt32 (DataBinder.Eval (e.Item.DataItem, "CommunityId"));
                HyperLink manageLink = (HyperLink) e.Item.FindControl ("manageLink");
                manageLink.NavigateUrl = "../community/3dapps/AppManagement.aspx?communityId=" + communityId;

                ImageButton btnDelete = (ImageButton) e.Item.FindControl ("btnDelete");
                
                HtmlContainerControl divNotifySummary = (HtmlContainerControl) e.Item.FindControl ("divNotifySummary");
                HtmlContainerControl divNotification = (HtmlContainerControl) e.Item.FindControl ("divNotification");
                HtmlContainerControl divNotifyOptions = (HtmlContainerControl) e.Item.FindControl ("divNotifyOptions");
                HtmlContainerControl spnNotify = (HtmlContainerControl) e.Item.FindControl ("spnNotify");


                // Set the links for go in world
                LinkButton btnMeet3D = (LinkButton) e.Item.FindControl ("btnMeet3D");
                int gameId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "WOK3App.GameId"));

                ConfigureCommunityMeetMe3D (btnMeet3D, gameId, communityId, Convert.ToString (DataBinder.Eval (e.Item.DataItem, "Name")), "");
                
                List<CommunityMember> members = ((List<CommunityMember>) DataBinder.Eval (e.Item.DataItem, "MemberList"));
                switch (((CommunityMember) members[0]).AccountTypeId)
                {
                    case (int) CommunityMember.CommunityMemberAccountType.OWNER:
                        {
                            RadioButtonList rblNotify = (RadioButtonList) e.Item.FindControl ("rblNotify");
                            if (rblNotify != null)
                            {
                                rblNotify.SelectedValue = DataBinder.Eval (e.Item.DataItem, "notifications").ToString ();
                                rblNotify.Visible = true;

                                divNotifySummary.InnerHtml = GetNotificationSummary (Convert.ToInt32 (rblNotify.SelectedValue));
                                divNotifySummary.Visible = true;
                            }

                            manageLink.Visible = true;

                            btnDelete.Attributes.Add ("onclick", GetDeleteScript ());

                            HtmlContainerControl communityAccess = (HtmlContainerControl) e.Item.FindControl ("communityAccess");
                            if (communityAccess != null)
                            {
                                string access = DataBinder.Eval (e.Item.DataItem, "IsPublic").ToString ();
                                if (access.Equals ("N"))
                                {
                                    communityAccess.InnerText = "Private";
                                }
                                else if (access.Equals ("I"))
                                {
                                    communityAccess.InnerText = "Owner Only";
                                }
                                else
                                {
                                    communityAccess.InnerText = "Open";
                                }
                            }

                            //Check Credit Balance
                            PremiumItemRedeemAmounts amts = GetTransactionFacade.GetTotalTransactionCreditAmountsByGame (communityId, KanevaGlobals.PremiumItemDaysPendingBeforeRedeem);
                            if (amts.Available > 0)
                            {
                                HtmlAnchor aCreditBalance = (HtmlAnchor) e.Item.FindControl ("aCreditBalance");
                                Literal litCreditBalance = (Literal) e.Item.FindControl ("litCreditBalance");
                                aCreditBalance.HRef = ResolveUrl ("~/community/3dapps/AppManagement.aspx?communityid=" + communityId + "&control=gametransactions");
                                aCreditBalance.Visible = true;
                                litCreditBalance.Text = "World Credit Balance: " + amts.EarnedAvailable.ToString ();
                            }
                                              
                            break;
                        }
                    case (int) CommunityMember.CommunityMemberAccountType.MODERATOR:
                        {
                            RadioButtonList rblNotify = (RadioButtonList) e.Item.FindControl ("rblNotify");
                            if (rblNotify != null)
                            {
                                rblNotify.SelectedValue = DataBinder.Eval (e.Item.DataItem, "notifications").ToString ();
                                rblNotify.Visible = true;

                                divNotifySummary.InnerHtml = GetNotificationSummary (Convert.ToInt32 (rblNotify.SelectedValue));
                                divNotifySummary.Visible = true;
                            }

                            btnDelete.Visible = false;
                            break;
                        }
                    case (int) CommunityMember.CommunityMemberAccountType.SUBSCRIBER:
                        {
                            RadioButtonList rblNotify = (RadioButtonList) e.Item.FindControl ("rblNotify");
                            if (rblNotify != null)
                            {
                                rblNotify.SelectedValue = DataBinder.Eval (e.Item.DataItem, "notifications").ToString ();
                                rblNotify.Visible = true;

                                divNotifySummary.InnerHtml = GetNotificationSummary (Convert.ToInt32 (rblNotify.SelectedValue));
                                divNotifySummary.Visible = true;
                            }

                            btnDelete.Visible = false;
                            break;
                        }
                }
            }
        }

        /// <summary>
        /// rptAssets_ItemCommand
        /// </summary>
        public void rptApps_ItemCommand (object source, RepeaterCommandEventArgs e)
        {
            string command = e.CommandName;
            int userId = GetUserId ();
            int index = e.Item.ItemIndex;

            HtmlInputHidden hidChannelId = (HtmlInputHidden) rptApps.Items[index].FindControl ("hidChannelId");
            int communityId = Convert.ToInt32 (hidChannelId.Value);

            switch (command)
            {
                case "cmdDeleteChannel":
                    {
                        if (IsAdministrator () || GetCommunityFacade.IsCommunityOwner (communityId, userId))
                        {
                            // Don't chagne this to Facade until everything is implemented there!!!
                            // Facade did not implement deleteing members, 
                            //      removing assets and adjusting counts, 
                            //      set last update so it would be removed from search, 
                            //      call correct SP so wok items get taken care of.
                            CommunityUtility.DeleteCommunity (communityId, userId);
                        }

                        break;
                    }
            }

            if (!string.IsNullOrWhiteSpace(ddlSortBy.SelectedValue))
            {
                SetOrderBy(ddlSortBy.SelectedValue);
            }

            BindApps (pgTop.CurrentPageNumber, userId);
        }

        public void ddlSortBy_IndexChanged (object sender, EventArgs e)
        {
            SetOrderBy (ddlSortBy.SelectedValue);
            pgTop.CurrentPageNumber = 1;
            BindApps (1, GetUserId ());
        }

        #endregion Event Handlers

        #region Properties

        private bool IncludeCreditBalance
        {
            set { includeCreditBalance = value; }
            get { return includeCreditBalance; }
        }

        private string OrderBy
        {
            set { orderBy = value; }
            get { return orderBy; }
        }

        private string SearchFilter
        {
            set { filter = value; }
            get { return filter; }
        }

        #endregion Properties

        #region Declerations

        protected HyperLink manageLink;
        protected Kaneva.Pager pgTop;
        private bool includeCreditBalance = false;
        private string orderBy = "";
        private string filter = "";

        #endregion Declerations

        #region Web Form Designer generated code

        override protected void OnInit (EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent ();
            base.OnInit (e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent ()
        {
            this.Load += new System.EventHandler (this.Page_Load);
            pgTop.PageChanged += new PageChangeEventHandler (pgTop_PageChange);
        }
        
        #endregion
    }
}
