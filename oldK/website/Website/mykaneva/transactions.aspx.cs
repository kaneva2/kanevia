///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Collections.Generic;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using log4net;

namespace KlausEnt.KEP.Kaneva.mykaneva
{
    public partial class transactions : SortedBasePage
    {
        protected void Page_Load (object sender, EventArgs e)
        {
            // They must be logged in
            if (!Request.IsAuthenticated)
            {
                Response.Redirect (GetLoginURL ());
                return;
            }

            if (!IsPostBack)
            {
                ClearSelectedTransactionType ();

                if (Request["CP"] != null && Request["CP"].ToString().Equals("Y"))
                {
                    liPurchases.Attributes.Add ("class", "transactionnav selected");
                    BindPurchases(1);
                }
                else if (Request["CashP"] != null && Request["CashP"].ToString ().Equals ("Y"))
                {
                    liCashPurchases.Attributes.Add ("class", "transactionnav selected");
                    BindCashPurchases (1);
                }
                else if (Request["Raves"] != null && Request["Raves"].ToString ().Equals ("Y"))
                {
                    liRaves.Attributes.Add ("class", "transactionnav selected");
                    BindRaves (1);
                }
                else if (Request["PI"] != null && Request["PI"].ToString ().Equals ("Y"))
                {
                    liPremiumItems.Attributes.Add ("class", "transactionnav selected");
                    BindPremiumItems (1);
                }
                else
                {
                    liFame.Attributes.Add ("class", "transactionnav selected");
                    BindFame (1);
                }
            }

            // Set Nav
            HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
            HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.ACCOUNT;
            HeaderNav.MyAccountNav.ActiveTab = NavAccount.TAB.TRANSACTIONS;
            HeaderNav.SetNavVisible (HeaderNav.MyKanevaNav, 2);
            HeaderNav.SetNavVisible (HeaderNav.MyAccountNav, 3);
        }


        #region Helper Methods

        /// <summary>
        /// Bind to correct transaction type
        /// </summary>
        private void BindData (int pageNumber)
        {
            // Hide everything
            HideAllRepeaters ();

            // Get the transaction type
            switch (CurrentTransType)
            {
                case "fame":
                    BindFame (pageNumber);
                    break;

                case "rewards":
                    BindRewards (pageNumber);
                    break;

                case "purchases":
                    BindPurchases (pageNumber);
                    break;

                case "trades":
                    BindTrades (pageNumber);
                    break;

                case "gifts":
                    BindGifts (pageNumber);
                    break;

                case "cash purchases":
                    BindCashPurchases (pageNumber);
                    break;

                case "raves":
                    BindRaves (pageNumber);
                    break;
                
                case "premium items":
                    BindPremiumItems (pageNumber);
                    break;
            }
        }

        /// <summary>
        /// Reset the Transaction Type Menu 
        /// </summary>
        private void ClearSelectedTransactionType ()
        {
            liFame.Attributes.Add ("class", "transactionnav");
            liRewards.Attributes.Add ("class", "transactionnav");
            liPurchases.Attributes.Add ("class", "transactionnav");
            liTrades.Attributes.Add ("class", "transactionnav");
            liGifts.Attributes.Add ("class", "transactionnav");
            liCashPurchases.Attributes.Add ("class", "transactionnav");
            liRaves.Attributes.Add ("class", "transactionnav");
            liPremiumItems.Attributes.Add ("class", "transactionnav");
        }

        /// <summary>
        /// Hide all the reapeater displays
        /// </summary>
        private void HideAllRepeaters ()
        {
            divFameRpt.Style.Add ("display", "none");
            divRewardsRpt.Style.Add ("display", "none");
            divPurchasesRpt.Style.Add ("display", "none");
            divTradesRpt.Style.Add ("display", "none");
            divGiftsRpt.Style.Add ("display", "none");
            divCashPurchasesRpt.Style.Add ("display", "none");
            divRavesRpt.Style.Add ("display", "none");
            divPremiumItemsRpt.Style.Add ("display", "none");

            divNoResults.Style.Add ("display", "none");
            divMessages.Style.Add ("display", "none");
        }

        /// <summary>
        /// Get the Fame Transactions 
        /// </summary>
        private void BindFame (int pageNumber)
        {
            try
            {
                CurrentTransType = "fame";
                lblViewName.Text = "Fame";

                // Set the sortable columns
                CurrentSort = "level_date";
                CurrentSortOrder = "DESC";

                string orderby = CurrentSort + " " + CurrentSortOrder;

                // Get the Fame transactions
                TransactionFacade transFacade = new TransactionFacade ();
                PagedList<FameTransaction> fameTrans = transFacade.GetFameTransactionsByUser (KanevaWebGlobals.CurrentUser.UserId, KanevaWebGlobals.CurrentUser.Gender, "", orderby, pageNumber, _PGSIZE);

                if (fameTrans.TotalCount > 0)
                {
                    rptFameTransactions.DataSource = fameTrans;
                    rptFameTransactions.DataBind ();
                    divFameRpt.Style.Add ("display", "block");
                }
                else
                {
                    // show no transactions found message
                    divNoResults.InnerHtml = "No Fame Transactions found.";
                    divNoResults.Style.Add ("display", "block");
                }

                ShowResults (fameTrans.TotalCount, fameTrans.Count, pageNumber, _PGSIZE);
            }
            catch (Exception ex)
            {
                ShowErrMessage ("There was an error retreiving your fame transactions. ", ex.Message);   
            }
        }

        /// <summary>
        /// Get the Rewards Transactions 
        /// </summary>
        private void BindRewards (int pageNumber)             
        {
            try
            {
                CurrentTransType = "rewards";
                lblViewName.Text = "Rewards";
                
                // Set the sortable columns
                CurrentSort = "reward_log_id";
                CurrentSortOrder = "DESC";

                string orderby = CurrentSort + " " + CurrentSortOrder;
                int pgSize = 20;

                divNoResults.Style.Add ("display", "none");

                TransactionFacade transFacade = new TransactionFacade ();
                PagedList<RewardTransaction> rewardTrans = transFacade.GetRewardTransactionsByUser (KanevaWebGlobals.CurrentUser.UserId, "", orderby, pageNumber, pgSize);

                if (rewardTrans.TotalCount > 0)
                {
                    rptRewards.DataSource = rewardTrans;
                    rptRewards.DataBind ();
                    divRewardsRpt.Style.Add ("display", "block");        
                }
                else
                {
                    // show no transactions found message
                    divNoResults.InnerHtml = "No Reward Transactions found.";
                    divNoResults.Style.Add ("display", "block");
                }

                ShowResults (rewardTrans.TotalCount, rewardTrans.Count, pageNumber, pgSize);
            }
            catch (Exception ex)
            {
                ShowErrMessage ("There was an error retreiving your reward transactions. ", ex.Message);
            }
        }

        /// <summary>
        /// Get the Purchase Transactions 
        /// </summary>
        private void BindPurchases (int pageNumber)           
        {
            try
            {
                // Clear error message if it is visible
                divMessages.Style.Add ("display", "none");
                
                CurrentTransType = "purchases";
                lblViewName.Text = "Purchases";

                // Set the sortable columns
                CurrentSort = "purchase_date";
                CurrentSortOrder = "DESC";

                string orderby = CurrentSort + " " + CurrentSortOrder;
                
                // Get the Purchase transactions
                TransactionFacade transFacade = new TransactionFacade ();
                PagedList<PurchaseTransaction> purchaseTrans = null;

                if (ePurchaseType.Raves == _PurchaseType)
                {
                    purchaseTrans = transFacade.GetPurchasesRaveTransactionsByUser (KanevaWebGlobals.CurrentUser.UserId, "", orderby, pageNumber, _PGSIZE);
                }
                else // All other purchases
                {
                    purchaseTrans = transFacade.GetPurchasesTransactionsByUser (KanevaWebGlobals.CurrentUser.UserId, "use_type <> " + WOKItem.USE_TYPE_PREMIUM.ToString (), orderby, pageNumber, _PGSIZE);
                } 


                if (purchaseTrans != null && purchaseTrans.TotalCount > 0)
                {
                    rptPurchases.DataSource = purchaseTrans;
                    rptPurchases.DataBind ();
                    divPurchases.Style.Add ("display", "block");

                    divPurchasesNoResults.Style.Add ("display", "none;");

                    ShowResults (purchaseTrans.TotalCount, purchaseTrans.Count, pageNumber, _PGSIZE);
                }
                else
                {
                    // show no transactions found message
                    divPurchasesNoResults.InnerHtml = "No Purchase Transactions found.";
                    divPurchasesNoResults.Style.Add ("display", "block");
                    divPurchases.Style.Add ("display", "none");

                    ShowResults (0, 0, pageNumber, _PGSIZE);
                }

                divPurchasesRpt.Style.Add ("display", "block");
                SetPurchaseTransactionTabs ();
            }
            catch (Exception ex)
            {
                ShowErrMessage ("There was an error retreiving your purchase transactions. ", ex.Message);
            }
        }

        /// <summary>
        /// Get the Trades Transactions 
        /// </summary>
        private void BindTrades (int pageNumber)              
        {
            try
            {
                CurrentTransType = "trades";
                lblViewName.Text = "Trades";

                // Set the sortable columns
                CurrentSort = "w.created_date";
                CurrentSortOrder = "DESC";
                string orderby = CurrentSort + " " + CurrentSortOrder;
                
                // Get the Trade transactions
                TransactionFacade transFacade = new TransactionFacade ();                     
                PagedList<TradeTransaction> tradeTrans = transFacade.GetTradeTransactionsByUser (KanevaWebGlobals.CurrentUser.UserId, orderby, pageNumber, _PGSIZE);

                if (tradeTrans.TotalCount > 0)
                {
                    rptTrades.DataSource = tradeTrans;
                    rptTrades.DataBind ();
                    divTradesRpt.Style.Add ("display", "block");
                }
                else
                {
                    // show no transactions found message
                    divNoResults.InnerHtml = "No Trade Transactions found.";
                    divNoResults.Style.Add ("display", "block");
                }

                ShowResults (tradeTrans.TotalCount, tradeTrans.Count, pageNumber, _PGSIZE);
            }
            catch (Exception ex)
            {
                ShowErrMessage ("There was an error retreiving your trade transactions. ", ex.Message);
            }
        }

        /// <summary>
        /// Get the Gift Transactions 
        /// </summary>
        private void BindGifts (int pageNumber)               
        {
            try
            {
                CurrentTransType = "gifts";
                lblViewName.Text = "Gifts";

                // Set the sortable columns
                CurrentSort = "mg.gift_id";
                CurrentSortOrder = "ASC";
                string orderby = CurrentSort + " " + CurrentSortOrder;
                
                // Get the Gift transactions
                TransactionFacade transFacade = new TransactionFacade ();                     
                PagedList<GiftTransaction> giftTrans = transFacade.GetGiftTransactionsByUser (KanevaWebGlobals.CurrentUser.UserId, "", orderby, pageNumber, _PGSIZE);

                if (giftTrans.TotalCount > 0)
                {
                    rptGifts.DataSource = giftTrans;
                    rptGifts.DataBind ();
                    divGiftsRpt.Style.Add ("display", "block");
                }
                else
                {
                    // show no transactions found message
                    divNoResults.InnerHtml = "No Gift Transactions found.";
                    divNoResults.Style.Add ("display", "block");
                }

                ShowResults (giftTrans.TotalCount, giftTrans.Count, pageNumber, _PGSIZE);
            }
            catch (Exception ex)
            {
                ShowErrMessage ("There was an error retreiving your gift transactions. ", ex.Message);
            }
        }

        /// <summary>
        /// Get the Cash Purchase Transactions 
        /// </summary>
        private void BindCashPurchases (int pageNumber)
        {
            try
            {
                CurrentTransType = "cash purchases";
                lblViewName.Text = "Cash Purchases";

                // Set the sortable columns
                CurrentSort = "ppt.transaction_date";
                CurrentSortOrder = "DESC";
                string orderby = CurrentSort + " " + CurrentSortOrder;
                string filter = " o.transaction_status_id = " + ((int) Constants.eORDER_STATUS.COMPLETED).ToString ();

                // Get the Cash Purchase transactions
                PagedDataTable pdtCashPurchases = StoreUtility.GetUserCreditTransactions (KanevaWebGlobals.CurrentUser.UserId, filter, orderby, pageNumber, _PGSIZE);
                
                if (pdtCashPurchases.TotalCount > 0)
                {
                    rptCashPurchases.DataSource = pdtCashPurchases;
                    rptCashPurchases.DataBind ();
                    divCashPurchasesRpt.Style.Add ("display", "block");
                }
                else
                {
                    // show no transactions found message
                    divNoResults.InnerHtml = "No Cash Purchase Transactions found.";
                    divNoResults.Style.Add ("display", "block");
                }

                ShowResults (Convert.ToUInt32 (pdtCashPurchases.TotalCount), pdtCashPurchases.Rows.Count, pageNumber, _PGSIZE);
            }
            catch (Exception ex)
            {
                ShowErrMessage ("There was an error retreiving your cash purchase transactions. ", ex.Message);
            }
        }

        /// <summary>
        /// Get the Raves Transactions 
        /// </summary>
        private void BindRaves (int pageNumber)
        {
            try
            {
                // Clear error message if it is visible
                divMessages.Style.Add ("display", "none");

                CurrentTransType = "raves";
                lblViewName.Text = "Raves";
                string orderby = "";

                CurrentSortOrder = "DESC";
                
                string filter = ""; // " o.transaction_status_id = " + ((int) Constants.eORDER_STATUS.COMPLETED).ToString ();
                PagedList<RaveTransaction> pdtRaves = null;

                if (eRaveItemType.Assets == _RaveItemType)
                {
                    CurrentSort = "digg_id";
                    orderby = CurrentSort + " " + CurrentSortOrder;

                    // Get the Asset Rave transactions
                    pdtRaves = new TransactionFacade ().GetAssetRaveTransactionsByUser (KanevaWebGlobals.CurrentUser.UserId, _IsRaver, filter, orderby, pageNumber, _PGSIZE);
                }
                else if (eRaveItemType.ProfileCommunities == _RaveItemType)
                {
                    CurrentSort = "create_date";
                    orderby = CurrentSort + " " + CurrentSortOrder;

                    // Get the Profile and Community Rave transactions
                    pdtRaves = new TransactionFacade ().GetCommunityRaveTransactionsByUser (KanevaWebGlobals.CurrentUser.UserId, _IsRaver, "", "", orderby, pageNumber, _PGSIZE);
                }
                else if (eRaveItemType.WokHomesHangouts == _RaveItemType)
                {
                    if (KanevaWebGlobals.CurrentUser.HasWOKAccount)
                    {
                        CurrentSort = "create_date";
                        orderby = CurrentSort + " " + CurrentSortOrder;

                        // Get the Profile and Community Rave transactions
                        pdtRaves = new TransactionFacade ().Get3DHomeRaveTransactionsByUser (KanevaWebGlobals.CurrentUser.UserId, _IsRaver, filter, orderby, pageNumber, _PGSIZE);
                    }
                }
                else // eRaveItemType.UGC
                {
                    CurrentSort = "digg_id";
                    orderby = CurrentSort + " " + CurrentSortOrder;

                    // Get the UGC items Rave transactions
                    pdtRaves = new TransactionFacade ().GetUGCRaveTransactionsByUser (KanevaWebGlobals.CurrentUser.UserId, _IsRaver, filter, orderby, pageNumber, _PGSIZE);
                }


                if (pdtRaves != null && pdtRaves.TotalCount > 0)
                {
                    if (_IsRaver)
                    {
                        rptRavesByMe.DataSource = pdtRaves;
                        rptRavesByMe.DataBind ();
                        divRavesByMe.Style.Add ("display", "block");
                        divRavesMyStuff.Style.Add ("display", "none");
                    }
                    else
                    {
                        rptRavesMyStuff.DataSource = pdtRaves;
                        rptRavesMyStuff.DataBind ();
                        divRavesMyStuff.Style.Add ("display", "block");
                        divRavesByMe.Style.Add ("display", "none");
                    }

                    divRaveNoResults.Style.Add ("display", "none;");

                    ShowResults (Convert.ToUInt32 (pdtRaves.TotalCount), pdtRaves.List.Count, pageNumber, _PGSIZE);
                }
                else
                {
                    // show no transactions found message
                    divRaveNoResults.InnerHtml = "No Rave Transactions found.";
                    divRaveNoResults.Style.Add ("display", "block");
                    divRavesMyStuff.Style.Add ("display", "none");
                    divRavesByMe.Style.Add ("display", "none");

                    ShowResults (0, 0, pageNumber, _PGSIZE);
                }

                divRavesRpt.Style.Add ("display", "block");
                SetRaveTransactionTabs ();
            }
            catch (Exception ex)
            {
                ShowErrMessage ("There was an error retreiving your Rave transactions. ", ex.Message);
            }
        }

        /// <summary>
        /// Get the Purchase Transactions 
        /// </summary>
        private void BindPremiumItems (int pageNumber)
        {
            try
            {
                // Clear error message if it is visible
                divMessages.Style.Add ("display", "none");

                lblViewName.Text = "Premium Items";

                // Set the sortable columns
                CurrentSort = "purchase_date";
                CurrentSortOrder = "DESC";

                string orderby = CurrentSort + " " + CurrentSortOrder;

                // Get the Purchase transactions
                TransactionFacade transFacade = new TransactionFacade ();
                PagedList<PurchaseTransaction> purchaseTrans = null;

                purchaseTrans = transFacade.GetPurchasesTransactionsByUser (KanevaWebGlobals.CurrentUser.UserId, "use_type = " + WOKItem.USE_TYPE_PREMIUM.ToString (), orderby, pageNumber, _PGSIZE);

                if (purchaseTrans != null && purchaseTrans.TotalCount > 0)
                {
                    rptPremiumItems.DataSource = purchaseTrans;
                    rptPremiumItems.DataBind ();
                    divPremiumItems.Style.Add ("display", "block");

                    divPremiumItemsNoResults.Style.Add ("display", "none;");

                    ShowResults (purchaseTrans.TotalCount, purchaseTrans.Count, pageNumber, _PGSIZE);
                }
                else
                {
                    // show no transactions found message
                    divPremiumItemsNoResults.InnerHtml = "No Premium Item Purchase Transactions found.";
                    divPremiumItemsNoResults.Style.Add ("display", "block");
                    divPremiumItems.Style.Add ("display", "none");

                    ShowResults (0, 0, pageNumber, _PGSIZE);
                }

                divPremiumItemsRpt.Style.Add ("display", "block");
            }
            catch (Exception ex)
            {
                ShowErrMessage ("There was an error retreiving your Premium Item purchase transactions. ", ex.Message);
            }
        }

        /// <summary>
        /// Show the Pagers and results count 
        /// </summary>
        private void ShowResults (uint totalCount, int count, int pageNumber, int pgSize)
        {
            // Populate top pager
            pgTop.NumberOfPages = Math.Ceiling ((double) totalCount / pgSize).ToString ();
            pgTop.CurrentPageNumber = pageNumber;
            pgTop.DrawControl ();

            // Populate bottom pager
            pgBottom.NumberOfPages = Math.Ceiling ((double) totalCount / pgSize).ToString ();
            pgBottom.CurrentPageNumber = pageNumber;
            pgBottom.DrawControl ();

            // The results
            lblResults_Top.Text = lblResults_Top.Text = GetResultsText (totalCount, pageNumber, pgSize, count);
            lblResults_Bottom.Text = lblResults_Top.Text;
        }

        /// <summary>
        /// Format the display of Level Awards
        /// </summary>
        protected string DisplayRewards (object levelAwards, string rewards)
        {
            string awardList = "";
            
            try
            {
                if (levelAwards != null)
                {
                    IList<LevelAwards> list = (List<LevelAwards>) levelAwards;

                    // For each of the level awards passed in, format it for display.  Multiple 
                    // rewards will be displayed as comma seperated list of items
                    foreach (LevelAwards la in list)
                    {
                        if (awardList.Length > 0) awardList += ", ";
                        awardList += la.Quantity + " - " + la.Name;
                    }
                }

                if (rewards.Length > 0)
                {
                    awardList += awardList.Length > 0 ? ", " : "";
                    awardList += rewards + " Rewards";
                }
            }
            catch { }
            return awardList;
        }

        /// <summary>
        /// Calculate total amount
        /// </summary>
        protected string CalcTotal (int qty, int cost, int itemPurchaseTransType)
        {
            if (itemPurchaseTransType == 2)
            {
                return "0";
            }
            return (qty * cost).ToString("N0");
        }

        protected string FormatPrice (object price)
        {
            return Convert.ToInt32(price).ToString("N0");
        }

        /// <summary>
        /// Format the credit amount
        /// </summary>
        public string FormatCredits (object obj)
        {
            return (Convert.ToUInt64 (obj)).ToString ("N0");
        }

        /// <summary>
        /// Format the traded item for display
        /// </summary>
        public string GetTradedItem (object name, object pointId)
        {
            string val = "";

            try
            {
                // if traded item is an inventory item, then we display the item name, 
                // if not, display the type of points traded (Reward or Credit)
                string itemName = name.ToString ();
                string keiPointId = pointId.ToString ();

                if (itemName.Length > 0)
                {
                    val = itemName;
            }                               
                else
                {
                    val = keiPointId == "GPOINT" ? "Rewards" : "Credits";
                }
            }
            catch { }

            return val;
        }

        /// <summary>
        /// Format the trade transaction type display
        /// </summary>
        public string GetTradeActionType (object id)          
        {
            int toId = Convert.ToInt32 (id);
            return (KanevaWebGlobals.CurrentUser.UserId == toId ? "Traded - From" : "Traded - To");
        }

        /// <summary>
        /// Format the trade action type display
        /// </summary>
        public string GetGiftActionType (object id)           
        {
            int toId = Convert.ToInt32 (id);
            return (KanevaWebGlobals.CurrentUser.UserId == toId ? "Received" : "Gave");
        }

        /// <summary>
        /// Get the username from the useid
        /// </summary>
        public string GetUsernameFromUserId (object idTo, object idFrom)  
        {
            int toId = Convert.ToInt32 (idTo);
            int fromId = Convert.ToInt32 (idFrom);
                                                           
            return UsersUtility.GetUserNameFromId (toId == KanevaWebGlobals.CurrentUser.UserId ? fromId : toId);
        }

        /// <summary>
        /// Display the error message box 
        /// </summary>
        private void ShowErrMessage (string msg, string exception)
        {
            m_logger.Warn (exception);
            divMessages.InnerHtml = msg + " " + exception;
            divMessages.Style.Add ("display", "block");
        }

        private void SetRaveTransactionTabs ()
        {
            if (_IsRaver)
            {
                lbRavedByMe.Text = "[ Raved by Me ]";
                lbRavedMe.Text = "Raved Me and My Stuff";
                
                lbRavedByMe.Style.Add ("color", "red");
                lbRavedMe.Style.Add ("color", "black");
            }
            else
            {
                lbRavedByMe.Text = "Raved by Me";
                lbRavedMe.Text = "[ Raved Me and My Stuff ]";

                lbRavedMe.Style.Add ("color", "red");
                lbRavedByMe.Style.Add ("color", "black");
            }

            ResetRaveItemTypeTabs ();
            switch (_RaveItemType)
            {
                case eRaveItemType.Assets:
                    lbAssetRaves.Text = "[ " + lbAssetRaves.Text + " ]";
                    lbAssetRaves.Style.Add ("color", "red");
                    break;
                case eRaveItemType.ProfileCommunities:
                    lbProfileCommunities.Text = "[ " + lbProfileCommunities.Text + " ]";
                    lbProfileCommunities.Style.Add ("color", "red");
                    break;
                case eRaveItemType.WokHomesHangouts:
                    lb3dHomeHangouts.Text = "[ " + lb3dHomeHangouts.Text + " ]";
                    lb3dHomeHangouts.Style.Add ("color", "red");
                    break;
                case eRaveItemType.UGC:
                    lbUGC.Text = "[ " + lbUGC.Text + " ]";
                    lbUGC.Style.Add ("color", "red");
                    break;
            }
        }

        private void ResetRaveItemTypeTabs ()
        {
            lbAssetRaves.Text = "Media";
            lbProfileCommunities.Text = "Profile/Communities";
            lb3dHomeHangouts.Text = "3D Home/Hangouts";
            lbUGC.Text = "UGC";

            lbAssetRaves.Style.Add ("color", "black");
            lbProfileCommunities.Style.Add ("color", "black");
            lb3dHomeHangouts.Style.Add ("color", "black");
            lbUGC.Style.Add ("color", "black");
        }
        
        private void SetPurchaseTransactionTabs ()
        {
            ResetPurchaseTypeTabs ();
            switch (_PurchaseType)
            {
                case ePurchaseType.Items:
                    lbItemPurchases.Text = "[ " + lbItemPurchases.Text + " ]";
                    lbItemPurchases.Style.Add ("color", "red");
                    break;
                case ePurchaseType.Raves:
                    lbRavePurchases.Text = "[ " + lbRavePurchases.Text + " ]";
                    lbRavePurchases.Style.Add ("color", "red");
                    break;
            }
        }

        private void ResetPurchaseTypeTabs ()
        {
            lbItemPurchases.Text = "Item Purchases";
            lbRavePurchases.Text = "Rave Purchases";

            lbItemPurchases.Style.Add ("color", "black");
            lbRavePurchases.Style.Add ("color", "black");
        }

        public string GetPersonalChannelLink (object ownerid, object ownername)
        {
            int ownerId = Convert.ToInt32 (ownerid);
            string ownerName = ownername.ToString ();
            string ownerNameNoSpaces = ownerName.Replace (" ", "");

            return "<a href=\"" + GetPersonalChannelUrl (ownerNameNoSpaces) + "\">" + ownerName + "</a>";
        }

        public string GetWhatWasRaved (object itemid, object itemname, object itemtype, object ismegarave)
        {
            try
            {
                int itemId = Convert.ToInt32 (itemid);
                string itemName = itemname.ToString ();
                RaveTransaction.eItemType eItemType = (RaveTransaction.eItemType) itemtype;
                bool isMegaRave = Convert.ToBoolean (ismegarave);

                switch (eItemType)
                {
                    case RaveTransaction.eItemType.ASSET_GAME:
                        return "Game - <a href=\"" + ResolveUrl ("~/asset/" + itemId + ".media") + "\">" + itemName + "</a>";
                    case RaveTransaction.eItemType.ASSET_MUSIC:
                        return "Music - <a href=\"" + ResolveUrl ("~/asset/" + itemId + ".media") + "\">" + itemName + "</a>";
                    case RaveTransaction.eItemType.ASSET_PATTERN:
                        return "Pattern - <a href=\"" + ResolveUrl ("~/asset/" + itemId + ".media") + "\">" + itemName + "</a>";
                    case RaveTransaction.eItemType.ASSET_PHOTO:
                        return "Photo - <a href=\"" + ResolveUrl ("~/asset/" + itemId + ".media") + "\">" + itemName + "</a>";
                    case RaveTransaction.eItemType.ASSET_TV_CHANNEL:
                        return "TV Channel - <a href=\"" + ResolveUrl ("~/asset/" + itemId + ".media") + "\">" + itemName + "</a>";
                    case RaveTransaction.eItemType.ASSET_VIDEO:
                        return "Video - <a href=\"" + ResolveUrl ("~/asset/" + itemId + ".media") + "\">" + itemName + "</a>";
                    case RaveTransaction.eItemType.ASSET_WIDGET:
                        return "Widget - <a href=\"" + ResolveUrl ("~/asset/" + itemId + ".media") + "\">" + itemName + "</a>";
                    
                    case RaveTransaction.eItemType.WEB_COMMUNITY:
                        return "Community - <a href=\"" + ResolveUrl ("~/community/" + itemName.Replace (" ", "") + ".channel") + "\">" + itemName + "</a>";

                    case RaveTransaction.eItemType.WEB_PROFILE:
                    case RaveTransaction.eItemType.WOK_AVATAR:
                        string megaRave = isMegaRave ? "<span style=\"color:red;margin-left:10px;\">Mega Rave</span>" : "";
                        return "Profile/3D Avatar - <a href=\"" + ResolveUrl ("~/community/" + itemName.Replace (" ", "") + ".people") + "\">" + itemName + "</a>" + megaRave;

                    case RaveTransaction.eItemType.WOK_HANGOUT:
                        return "3d Hangout - " + itemName;
                    
                    case RaveTransaction.eItemType.WOK_HOME:
                        return "3d Home - " + itemName;

                    case RaveTransaction.eItemType.UGC:
                        return "UGC - " + itemName;

                    default:
                        return "";
                }
            }
            catch 
            {
                return "";
            }
        }

        public string GetMegaRaveDesc (object globalId)
        {
            try
            {
                Community community = new CommunityFacade ().GetCommunity (Convert.ToInt32 (globalId));

                return "Mega Rave - <a href=\"" +
                    "/channel/" + community.NameNoSpaces + (community.IsPersonal == 1 ? ".people" : ".channel") +
                    "\">" + community.Name + "</a>";
            }
            catch
            {
                return "Mega Rave";
            }
        }

        #endregion Helper Methods


        #region Event Handlers

        /// <summary>
        /// Click event for Pager
        /// </summary>
        protected void pg_PageChange (object sender, PageChangeEventArgs e)
        {
            BindData (e.PageNumber);
        }

        /// <summary>
        /// Click event for Transaction Nav Menu
        /// </summary>
        protected void Nav_OnClick (object sender, CommandEventArgs e)
        {
            try
            {
                ClearSelectedTransactionType ();
                HideAllRepeaters ();

                string trans_type = e.CommandArgument.ToString ();

                switch (e.CommandArgument.ToString ())
                {
                    case "fame":
                        liFame.Attributes.Add ("class", "transactionnav selected");
                        BindFame (1);
                        break;

                    case "rewards":
                        liRewards.Attributes.Add ("class", "transactionnav selected");
                        BindRewards (1);
                        break;

                    case "purchases":
                        liPurchases.Attributes.Add ("class", "transactionnav selected");
                        BindPurchases (1);
                        break;

                    case "trades":
                        liTrades.Attributes.Add ("class", "transactionnav selected");
                        BindTrades (1);
                        break;

                    case "gifts":
                        liGifts.Attributes.Add ("class", "transactionnav selected");
                        BindGifts (1);
                        break;

                    case "cash purchases":
                        liCashPurchases.Attributes.Add ("class", "transactionnav selected");
                        BindCashPurchases (1);
                        break;

                    case "raves":
                        liRaves.Attributes.Add ("class", "transactionnav selected");
                        BindRaves (1);
                        break;

                    case "premium items":
                        liPremiumItems.Attributes.Add ("class", "transactionnav selected");
                        BindPremiumItems (1);
                        break;
                }
            }
            catch (Exception)
            {}
        }

        protected void SetRaveType (object sender, CommandEventArgs e)
        {
            _IsRaver = Convert.ToBoolean (e.CommandArgument);
            BindRaves (1);
        }

        protected void SetRaveItemType (object sender, CommandEventArgs e)
        {
            switch (e.CommandArgument.ToString ())
            {
                case "assets":
                    _RaveItemType = eRaveItemType.Assets;
                    break;
                case "profilecommunities":
                    _RaveItemType = eRaveItemType.ProfileCommunities;
                    break;
                case "3dhomehangouts":
                    _RaveItemType = eRaveItemType.WokHomesHangouts;
                    break;
                case "ugc":
                    _RaveItemType = eRaveItemType.UGC;
                    break;
                default:
                    _RaveItemType = eRaveItemType.ProfileCommunities;
                    break;
            }
            BindRaves (1);
        }

        protected void SetPurchaseType (object sender, CommandEventArgs e)
        {
            switch (e.CommandArgument.ToString ())
            {
                case "items":
                    _PurchaseType = ePurchaseType.Items;
                    break;
                case "raves":
                    _PurchaseType = ePurchaseType.Raves;
                    break;
                default:
                    _PurchaseType = ePurchaseType.Items;
                    break;
            }
            BindPurchases (1);
        }
        
        #endregion Event Handlers


        #region Properties

        /// <summary>
        /// Current Transaction Type
        /// </summary>
        public string CurrentTransType
        {
            set
            {
                ViewState["CurrentTransType"] = value.Trim ();
            }
            get
            {
                if (ViewState["CurrentTransType"] == null)
                {
                    return string.Empty; //cORDER_BY_DEFAULT;
                }
                else
                {
                    return ViewState["CurrentTransType"].ToString ();
                }
            }
        }

        private bool _IsRaver
        {
            set
            {
                ViewState["IsRaver"] = value;
            }
            get
            {
                if (ViewState["IsRaver"] == null)
                {
                    return false; 
                }
                else
                {
                    return Convert.ToBoolean(ViewState["IsRaver"]);
                }
            }
        }

        private eRaveItemType _RaveItemType
        {
            set
            {
                ViewState["RaveItemType"] = value;
            }
            get
            {
                if (ViewState["RaveItemType"] == null || ViewState["RaveItemType"].ToString().Length == 0)
                {
                    return eRaveItemType.Assets;
                }
                else
                {
                    return (eRaveItemType) ViewState["RaveItemType"];
                }
            }
        }

        private ePurchaseType _PurchaseType
        {
            set
            {
                ViewState["PurchaseType"] = value;
            }
            get
            {
                if (ViewState["PurchaseType"] == null || ViewState["PurchaseType"].ToString ().Length == 0)
                {
                    return ePurchaseType.Items;
                }
                else
                {
                    return (ePurchaseType) ViewState["PurchaseType"];
                }
            }
        }

        private enum eRaveItemType
        {
            Assets = 1,
            ProfileCommunities = 2,
            WokHomesHangouts = 3,
            UGC = 4
        }

        private enum ePurchaseType
        {
            Items = 1,
            Raves = 2
        }

        #endregion Properties


        #region Declerations

        const int _PGSIZE = 20;
        
        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

        #endregion Declerations
    }
}
