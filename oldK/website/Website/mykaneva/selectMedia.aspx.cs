///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for selectMedia.
	/// </summary>
	public class selectMedia : NoBorderPage
	{
		protected selectMedia () 
		{
			Title = "Select Media To Insert";
		}

		private void Page_Load (object sender, System.EventArgs e)
		{
			if (!IsPostBack)
			{
				searchType = (int) Constants.eASSET_TYPE.PICTURE;
				InitializeClientScript();
				SetSearchTabs();
				BindData( KanevaWebGlobals.GetUserId() );
			}
		}

		/// <summary>
		/// BindData
		/// </summary>
		private void BindData (int userId)
		{
			int channelId = GetPersonalChannelId ();

			// Groups
			if (ddlGroup.Items.Count == 0)
			{
				ddlGroup.Items.Add( new ListItem( "All", "-1" ) );
			}

			if ( userId != -1 )
			{
                PagedList<AssetGroup> pdtAssetGroups = GetMediaFacade.GetAssetGroups(channelId, "", "", 1, Int32.MaxValue);
				if (pdtAssetGroups.Count > 0)
				{
					for (int i = 0; i < pdtAssetGroups.Count; i++)
					{
						ddlGroup.Items.Add (new ListItem (pdtAssetGroups[i].Name, pdtAssetGroups[i].AssetGroupId.ToString()));
					}
				}
			}

			int group_id = Convert.ToInt32( ddlGroup.SelectedValue );
			
			BindPictures( group_id, userId, channelId, searchType);
		}

		private void BindPictures( int group_id, int user_id, int channelId, int assetTypeId )
		{
			PagedDataTable pds;

			// if the group id is valid, get pictures for that group
			if ( group_id != -1 )
			{
				string orderby = "asset_id" + " " + "ASC";

				pds = StoreUtility.GetAssetsInGroup( group_id, channelId, assetTypeId, orderby, "", 1, Int32.MaxValue );
			}
				// otherwise get all pics
			else
			{
				string orderby = "created_date" + " " + "ASC";
				string filter = "a.status_id <> " + (int) Constants.eASSET_STATUS.MARKED_FOR_DELETION;
				//int assetTypeId = (int) Constants.eASSET_TYPE.PICTURE;

				pds = StoreUtility.GetAssetsInChannel (channelId, true, true, assetTypeId, filter, orderby, 1, Int32.MaxValue);
			}

			dlMedia.DataSource = pds;
			dlMedia.DataBind();
		}

		
		#region Helper Methods
		/// <summary>
		/// SetSearchTabs
		/// </summary>
		private void SetSearchTabs()
		{
			string activeTab = string.Empty;

			// Set Tab styles
			lbPhoto.CssClass = "";
			lbMusic.CssClass = "";
			lbGame.CssClass = "";
			lbVideo.CssClass = "";
			
			tdPhotoTab.Style.Add ("background", "url(../images/tabsMedia_on.gif)");
			tdMusicTab.Style.Add ("background", "url(../images/tabsMedia_on.gif)");
			tdGameTab.Style.Add ("background", "url(../images/tabsMedia_on.gif)");
			tdVideoTab.Style.Add ("background", "url(../images/tabsMedia_on.gif)");

			// Show which tab is highlighted
			switch (searchType)
			{
				case (int) Constants.eASSET_TYPE.PICTURE:
				{
					lbPhoto.CssClass = "selected";
					tdPhotoTab.Style.Add ("background", "");
					tdPhotoTab.Style.Add ("background", "url(../images/tabsMedia.gif)");
					activeTab = tdPhotoTab.ClientID;
					break;
				}
				case (int) Constants.eASSET_TYPE.MUSIC:
				{
					lbMusic.CssClass = "selected";
					tdMusicTab.Style.Add ("background", "url(../images/tabsMedia.gif)");
					activeTab = tdMusicTab.ClientID;
					break;
				}
				case (int) Constants.eASSET_TYPE.GAME:
				{
					lbGame.CssClass = "selected";
					tdGameTab.Style.Add ("background", "url(../images/tabsMedia.gif)");
					activeTab = tdGameTab.ClientID;
					break;
				}
				case (int) Constants.eASSET_TYPE.VIDEO:
				{
					lbVideo.CssClass = "selected";
					tdVideoTab.Style.Add ("background", "url(../images/tabsMedia.gif)");
					activeTab = tdVideoTab.ClientID;
					break;
				}
				default:
				{
					lbPhoto.CssClass = "selected";
					tdPhotoTab.Style.Add ("background", "");
					tdPhotoTab.Style.Add ("background", "url(../images/tabsMedia.gif)");
					activeTab = tdPhotoTab.ClientID;
					break;
				}
			}

			hidSearchType.Value = searchType.ToString();
			litActiveTab.Text = activeTab;
		}

		/// <summary>
		/// InitializeClientScript
		/// </summary>
		private void InitializeClientScript()
		{
			// Add onclick event to results tab text
			lbVideo.Attributes.Add("onclick", "return setActiveTab('tdVideoTab');");
			lbPhoto.Attributes.Add("onclick", "return setActiveTab('tdPhotoTab');");
			lbMusic.Attributes.Add("onclick", "return setActiveTab('tdMusicTab');");
			lbGame.Attributes.Add("onclick", "return setActiveTab('tdGameTab');");

			// Add onclick event t results tab icon 
			lbVideoIcon.Attributes.Add("onclick", "return setActiveTab('tdVideoTab');");
			lbPhotoIcon.Attributes.Add("onclick", "return setActiveTab('tdPhotoTab');");
			lbMusicIcon.Attributes.Add("onclick", "return setActiveTab('tdMusicTab');");
			lbGameIcon.Attributes.Add("onclick", "return setActiveTab('tdGameTab');");
		}
		#endregion

		#region Event Handlers
		/// <summary>
		/// dlMedia_ItemDataBound
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void dlMedia_ItemDataBound (object sender, DataListItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
			{
				DataRowView drv						= (DataRowView) e.Item.DataItem;
				HtmlGenericControl spnImageTitle	= (HtmlGenericControl) e.Item.FindControl("spnImageTitle");

				if ( spnImageTitle != null )
				{
					spnImageTitle.InnerText = TruncateWithEllipsis (drv ["name"].ToString() , 200 );
				}
			}
		}

		/// <summary>
		/// ddlGroup_SelectedIndexChanged
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void ddlGroup_SelectedIndexChanged (object sender, EventArgs e)
		{
			int group_id = Convert.ToInt32 (ddlGroup.SelectedValue );

			BindPictures (group_id, KanevaWebGlobals.GetUserId (), GetPersonalChannelId (), searchType);
		}

		/// <summary>
		/// btnSave_Click
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnInsert_Click (object sender, EventArgs e)
		{
			if (ihSinglePictureId.Value == string.Empty)
			{
				spnAlertMsg.InnerText = "You have not selected any media to insert.";
				return;
			}

			int assetId = Convert.ToInt32 (ihSinglePictureId.Value);

			// If successfull, send javascript to update mykaneva page, close the window
			if (!ClientScript.IsClientScriptBlockRegistered (GetType (), "startup"))
			{
				//DataRow drAsset = StoreUtility.GetAsset (assetId);
				string javascript = "<script language=JavaScript>window.opener.paste('" + GetAssetDetailsLink (assetId) + "');window.close();</script>";
                ClientScript.RegisterStartupScript(GetType(), "startup", javascript);
			}					  
		}

		/// <summary>
		/// Execute when user clicks new search results tab
		/// </summary>
		protected void SearchTab_Changed (object sender, CommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "video":
					searchType = (int) Constants.eASSET_TYPE.VIDEO;
					break;
				case "photo":
					searchType = (int) Constants.eASSET_TYPE.PICTURE;
					break;
				case "music":
					searchType = (int) Constants.eASSET_TYPE.MUSIC;
					break;
				case "games":
					searchType = (int) Constants.eASSET_TYPE.GAME;
					break;
			}
    
			SetSearchTabs();

			BindData (GetUserId());
		}

		#endregion

		#region Properties
		/// <summary>
		/// searchTab
		/// </summary>
		private int searchType
		{
			set
			{
				ViewState ["searchType"] = value;
			}
			get
			{
				if (ViewState ["searchType"] == null)
				{
					return 0;
				}
				else
				{
					return Convert.ToInt32 (ViewState ["searchType"]);
				}
			}
		}
		#endregion

		#region Declerations
		protected Button			btnSave;
		protected DropDownList		ddlGroup;
		protected DataList			dlMedia;
		protected LinkButton		lbChannel, lbPeople, lbVideo, lbMusic, lbPhoto, lbGame;
		protected LinkButton		lbChannelIcon, lbPeopleIcon, lbVideoIcon, lbMusicIcon, lbPhotoIcon, lbGameIcon;
		protected LinkButton		lbPeopleSearch, lbChannelSearch, lbVideoSearch, lbMusicSearch, lbPhotoSearch, lbGameSearch;
		protected Literal			litActiveTab;

		protected HtmlTableCell			tdChannelTab, tdPeopleTab, tdVideoTab, tdPhotoTab, tdMusicTab, tdGameTab;
		protected HtmlInputHidden		hidSearchType;
		protected HtmlGenericControl	spnAlertMsg;
		protected HtmlInputHidden		ihSinglePictureId;

		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
			this.dlMedia.ItemDataBound += new DataListItemEventHandler(dlMedia_ItemDataBound);
			this.ddlGroup.SelectedIndexChanged += new EventHandler(ddlGroup_SelectedIndexChanged);
		}
		#endregion
	}
}
