///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for friendUpdate.
	/// </summary>
	public class friendUpdate : System.Web.UI.Page
	{
		private void Page_Load(object sender, System.EventArgs e)
		{
            UserFacade userFacade = new UserFacade();
			int userId = KanevaWebGlobals.GetUserId ();
			int friendId = Convert.ToInt32 (Request ["userId"]);
			bool bAccept = Request ["accept"].ToString ().Equals ("Y");

			if (bAccept)
			{
                userFacade.AcceptFriend(userId, friendId);
			}
			else
			{
                userFacade.DenyFriend(userId, friendId);
			}

			Response.Redirect (ResolveUrl ("~/myKaneva/mailboxFriendRequests.aspx"));
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
