///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for editCCard.
	/// </summary>
	public class editCCard : MainTemplatePage
	{
		protected editCCard () 
		{
			Title = "Edit Credit Card";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			// They must be logged in
			if (!Request.IsAuthenticated)
			{
				Response.Redirect (GetLoginURL ());
				return;
			}

			if (!IsPostBack)
			{
				int userId = GetUserId ();
				int billingInformationId = Convert.ToInt32 (Request ["bId"]);
			
				DataRow drBillingInfo = UsersUtility.GetCreditCard (userId, billingInformationId);

				lblCardNumber.Text = Server.HtmlDecode (ShowCreditCardNumber (drBillingInfo ["card_number"].ToString ()));
				txtNameOnCard.Text = Server.HtmlDecode (drBillingInfo ["name_on_card"].ToString ());
				SetDropDownIndex (drpMonth, Server.HtmlDecode (drBillingInfo ["exp_month"].ToString ()), true, Server.HtmlDecode (drBillingInfo ["exp_month"].ToString ()));
				SetDropDownIndex (drpYear, Server.HtmlDecode (drBillingInfo ["exp_year"].ToString ()), true, Server.HtmlDecode (drBillingInfo ["exp_year"].ToString ()));

				try
				{
					for (int i = 0; i < drpCardType.Items.Count; i++)
					{
						if (Server.HtmlDecode (drBillingInfo ["card_type"].ToString ()).Equals (drpCardType.Items [i].Text))
						{
							drpCardType.SelectedIndex = i;
						}
					}
				}
				catch (Exception)
				{}
			}
		}

		/// <summary>
		/// btnUpdateCard_Click
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnUpdateCard_Click (object sender, EventArgs e)
		{
			if (!Page.IsValid) 
			{
				return;
			}

			// Update the billing information
			UsersUtility.UpdateBillingInfo (GetUserId (), Convert.ToInt32 (Request ["bId"]), Server.HtmlEncode (txtNameOnCard.Text), Server.HtmlEncode (drpCardType.SelectedItem.Text), Server.HtmlEncode (drpMonth.SelectedValue), Server.HtmlEncode (drpYear.SelectedValue));
			Response.Redirect (ResolveUrl ("~/mykaneva/billing.aspx"));
		}

		/// <summary>
		/// btnCancel_Click
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnCancel_Click (object sender, EventArgs e)
		{
			Response.Redirect (ResolveUrl ("~/mykaneva/billing.aspx"));
		}

		// card info
		protected Label lblCardNumber;
		protected TextBox txtNameOnCard;
		protected DropDownList drpMonth, drpYear;
		protected KlausEnt.KEP.Kaneva.WebControls.CardTypesListBox drpCardType;

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
