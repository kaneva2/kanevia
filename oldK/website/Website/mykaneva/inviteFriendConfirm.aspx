<%@ Page language="c#" Codebehind="inviteFriendConfirm.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.inviteFriendConfirm" %>

<link href="../css/home.css" rel="stylesheet" type="text/css" />		
<link href="../css/kanevaSC.css" rel="stylesheet" type="text/css"/>
<link href="../css/friends.css" rel="stylesheet" type="text/css"/>
<link href="../css/kanevaText.css" type="text/css" rel="stylesheet">

<table border="0" cellpadding="0" cellspacing="0">
		<tr><td><img runat="server" src="~/images/spacer.gif" width="1" height="10"></td></tr>
		<tr>
			<td>
				<!---CONTEN BG BORDER--->
				<table border="0" cellpadding="0" cellspacing="0" width="990" >
					<tr>
						<td class="frTopLeft"></td>
						<td class="frTop"></td>
						<td class="frTopRight"></td>
					</tr>
					<tr> 
						<td class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1"></td>
						<td valign="top" class="frBgInt1" align="left">
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tr><td colspan="3"><img runat="server" src="~/images/spacer.gif" width="1" height="5"></td></tr>
								<!--HEAD-->
								<tr>
									<td><img runat="server" src="~/images/spacer.gif" width="4" height="1"></td>
									<td>
										<table  border="0" cellpadding="0" cellspacing="0" width="967" class="boxInside">
										  <tr>
											<td class="boxInsideTopLeft"><img runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
											<td class="boxInsideTop" ><img runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
											<td  class="boxInsideTopRight"><img runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
										  </tr>
										  <tr class="boxInside">
											<td class="boxInsideleft" ><img runat="server" src="~/images/spacer.gif" width="1" height="4" /></td>
											<td class="boxInsideContent">
												<table border="0" cellpadding="0" cellspacing="0">
													<tr>
														<td><img runat="server" src="~/images/spacer.gif" width="0" height="8"/></td>
														<td   align="left" > 
															<table border="0" cellpadding="0" cellspacing="0">
																<tr><td colspan="3"><img runat="server" src="~/images/spacer.gif" width="1" height="2"/></td></tr>
																<tr>
																	<td><img runat="server" src="~/images/spacer.gif" width="5" height="1"/></td>
																	<td class="insideBoxText11">Friend request email sent!</td>
																</tr>
															</table>
															
														</td>
														<td><img runat="server" src="~/images/spacer.gif" width="10" height="1"/></td>
													</tr>
												</table>
											</td>
											<td class="boxInsideRight"><img runat="server" src="~/images/spacer.gif" width="1" height="4"/></td>
										  </tr>
										  <tr>
											<td class="boxInsideBottomLeft" ><img runat="server" src="~/images/spacer.gif" width="4" height="4" /></td>
											<td class="boxInsideBottom"><img runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
											<td class="boxInsideBottomRight"><img runat="server" src="~/images/spacer.gif" width="4" height="4" /></td>
										  </tr>
										</table>
									</td>
									<td><img runat="server" src="~/images/spacer.gif" width="5" height="1"></td>
								</tr>
								<!--END HEAD-->
								<!--CONTENT-->
								<tr>
									<td><img runat="server" src="~/images/spacer.gif" width="4" height="1"></td>
									<td>
										<table  border="0" cellpadding="0" cellspacing="0" width="967" class="boxInside">
										  <tr>
											<td class="boxInsideTopLeft"><img runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
											<td class="boxInsideTop" ><img runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
											<td  class="boxInsideTopRight"><img runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
										  </tr>
										  <tr class="boxInside">
											<td class="boxInsideleft" ><img runat="server" src="~/images/spacer.gif" width="1" height="4" /></td>
											<td class="boxInsideContent" align="center">
												<table border="0" cellpadding="0" cellspacing="0">
													<tr><td colspan="3"><img runat="server" src="~/images/spacer.gif" width="1" height="5"/></td></tr>
													<tr>
														<td><img runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
														<td  class="insideBoxText11" align="left" >
															<!--form-->
															<table width="949" border="0" cellpadding="0" cellspacing="0" class="bbBoxInside">
																<tr>
																  <td class="bbBoxInsideTopLeft"></td>
																  <td class="bbBoxInsideTop"></td>
																  <td class="bbBoxInsideTopRight"></td>
																</tr>
																<tr>
																  <td class="bbBoxInsideLeft"><img runat="server" src="~/images/spacer.gif" width="5" height="1"/></td>
																  <td class="bbBoxInsideContent" align="center" valign="top">									
																	<table border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
																		<tr><td colspan="2"><img runat="server" src="~/images/spacer.gif" width="1" height="10"></td></tr>
																		<tr>
																			<td><img runat="server" src="~/images/spacer.gif" width="10" height="1"/></td>
																			<td  align="center" class="insideBoxText11" >When your requested friend accepts your invitation they will appear in your friends list.</td>
																		</tr>
																		<tr>
																			<td><img runat="server" src="~/images/spacer.gif" width="10" height="1"/></td>
																			<td  align="center" class="insideBoxText11" >Check your list periodically to see when they have joined. As with all emails, <br/>it may take some time for your requested friend to respond.</td>
																		</tr>																		
																		<tr><td colspan="2"><img runat="server" src="~/images/spacer.gif" width="1" height="25"/></td></tr>																																														
																		<tr>
																			<td><img runat="server" src="~/images/spacer.gif" width="10" height="1"/></td>
																			<td  align="center" class="insideBoxText11" ><asp:Button id="btnBack" runat="server" Text="back to friends" style="cursor: hand;" /></td>
																		</tr>																		
																		<tr><td colspan="2"><img runat="server" src="~/images/spacer.gif" width="1" height="5"></td></tr>
																	</table>
																	</td>
																	  <td class="bbBoxInsideRight"><img runat="server" src="~/images/spacer.gif" width="5" height="1"/></td>
																	</tr>
																	<tr>
																	  <td class="bbBoxInsideBottomLeft"></td>
																	  <td class="bbBoxInsideBottom"></td>
																	  <td class="bbBoxInsideBottomRight"></td>
																</tr>
														  </table>
															<!--End form-->															
														</td>
														<td><img runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
													</tr>
													<tr><td colspan="3"><img runat="server" src="~/images/spacer.gif" width="1" height="5"></td></tr>
												</table>
											</td>
											<td class="boxInsideRight"><img runat="server" src="~/images/spacer.gif" width="1" height="4"/></td>
										  </tr>
										  <tr>
											<td class="boxInsideBottomLeft" ><img runat="server" src="~/images/spacer.gif" width="4" height="4" /></td>
											<td class="boxInsideBottom"><img runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
											<td class="boxInsideBottomRight"><img runat="server" src="~/images/spacer.gif" width="4" height="4" /></td>
										  </tr>
										</table>
									</td>
									<td><img runat="server" src="~/images/spacer.gif" width="5" height="1"></td>
								</tr>
								<tr><td colspan="3"><img runat="server" src="~/images/spacer.gif" width="1" height="5"></td></tr>
								<!--END CONTENT-->
								
							</table>
						</td>
						<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1"></td>
					</tr>
					<tr>
						<td class="frBottomLeft"></td>
						<td class="frBottom"></td>
						<td class="frBottomRight"></td>
					</tr>
				</table>
			<!---CONTEN BG BORDER--->
			</td>
		</tr>	
</table>
