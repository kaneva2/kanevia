<%@ Page language="c#" Codebehind="mailboxFriendRequests.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.mailboxFriendRequests" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../usercontrols/Pager.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="SearchFilter" Src="../usercontrols/SearchFilter.ascx" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>

<link href="../css/home.css" rel="stylesheet" type="text/css" />		
<link href="../css/kanevaSC.css" rel="stylesheet" type="text/css"/>
<link href="../css/friends.css" rel="stylesheet" type="text/css"/>
<link href="../css/kanevaText.css" type="text/css" rel="stylesheet">
<link href="../css/kaneva.css" type="text/css" rel="stylesheet">

<link href="../css/new.css" rel="stylesheet" type="text/css" />

<script src="../jscript/prototype.js" type="text/javascript" language="javascript"></script>

<table border="0" cellspacing="0" cellpadding="0"  width="990" align="center">
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" /></td>
	</tr>
	<tr>
		<td>
			<table  border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="frTopLeft"></td>
					<td class="frTop"></td>
					<td class="frTopRight"></td>
				</tr>
				<tr>
					<td class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
					<td valign="top" class="frBgIntMembers">
			
			
						<ajax:ajaxpanel id="ajpConfirm" runat="server">
						<div id="divConfirm" runat="server" visible="false">
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
									<div id="pageheader">
										<table cellpadding="0" cellspacing="0" border="0" width="99%">
											<tr><td align="left"><h1>Decline Friend Request</h1></td></tr>
										</table>
									</div>
								</td>
							</tr>
							<tr>
								<td width="968" align="center"  valign="top">
									<table cellpadding="0" cellspacing="0" border="0"  width="568">
										<tr>
											<td width="568" valign="top" align="center">
												<div class="module whitebg">
													<span class="ct"><span class="cl"></span></span>
													<h2>Are you sure you want to decline this Friend Request?</h2>
													<table width="99%" id="dlFriends" border="0" cellpadding="5" cellspacing="0">
														<tr>
															<td width="99%" align="center">
																<table border="0" cellspacing="0" cellpadding="0" width="60%">
																	<tr>
																		<td align="left"><asp:imagebutton imageurl="~/images/button_yes.gif" alt="Approve Friend" width="57" height="23" border="0" vspace="2" id="btnYes" runat="server" onclick="btnYes_Click"></asp:imagebutton></td>
																		<td align="right"><asp:imagebutton imageurl="~/images/button_no.gif" alt="Decline Friend" width="57" height="23" border="0" vspace="2" id="btnNo" runat="server" onclick="btnNo_Click"></asp:imagebutton></td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
													<span class="cb"><span class="cl"></span></span>
												</div>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						</div>
						</ajax:ajaxpanel>
			
			
						<ajax:ajaxpanel id="ajpRequestsTable" runat="server">
						<table width="100%"  border="0" cellspacing="0" cellpadding="0" id="tblRequests" runat="server" visible="true">
							<tr>
								<td>
								
									<!-- TOP STATUS BAR -->
									<div id="pageheader">
										<table cellpadding="0" cellspacing="0" border="0" width="99%">
											<tr>
												<td  align="left">
													<h1>Friend Requests</h1>
												</td>
									
												<td align="right" valign="middle">
													<table cellpadding="0" cellspacing="0" border="0" width="690">
														<tr>
															<td>	
																<ajax:ajaxpanel id="ajpTopStatusBar" runat="server">
																<table cellpadding="0" cellspacing="0" border="0" width="100%">
																	<tr>
																		<td class="headertout" width="175">
																			<asp:label runat="server" id="lblSearch" />
																		</td>
																		<td class="searchnav" width="260"> Sort by: 
																			<asp:linkbutton id="lbSortByDate" runat="server" sortby="Date" onclick="lbSortBy_Click">Date Received</asp:linkbutton> | 
																			<asp:linkbutton id="lbSortByName" runat="server" sortby="Name"  onclick="lbSortBy_Click">Name</asp:linkbutton>
																		</td>
																		<td align="left" width="130">
																			<kaneva:searchfilter runat="server" id="searchFilter" loc="top" hidethumbview="true" showfilterlists="false" assetsperpagelist="10,20,40" />
																		</td>
																		<td class="searchnav" align="right" width="210" nowrap>
																			<kaneva:pager runat="server" isajaxmode="True" id="pgTop" maxpagestodisplay="5" shownextprevlabels="true" />
																		</td>
																	</tr>
																</table>
																</ajax:ajaxpanel>
															</td>	
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</div>
									<!-- END TOP STATUS BAR -->		
								
								</td>
							</tr>
							<tr>
								<td width="968" align="left"  valign="top">
									<table cellpadding="0" cellspacing="0" border="0"  width="968">
										<tr>
											<td width="250" valign="top">
												<div class="module toolbar">
													<span class="ct"><span class="cl"></span></span>
													<h2>Tools & Actions <span class="hlink"></span></h2>
													<table cellspacing="0" cellpadding="10" border="0" width="99%">
														<tr>
															<td align="center">
																<table cellspacing="0" cellpadding="3" border="0" width="100%">
																	<tr>
																		<td>Approve or Decline selected Friend Requests:</td>
																	</tr>
																	<tr>
																		<td align="center">
																			
																			<ajax:ajaxpanel id="ajpApproveButton" runat="server">
																				<asp:imagebutton imageurl="~/images/button_approve.gif" alt="Accept" width="77" height="23" border="0" vspace="2" id="btnApprove" runat="server" onclick="btnApprove_Click"></asp:imagebutton>
																			</ajax:ajaxpanel>
																		
																			<br/>
																		
																			<ajax:ajaxpanel id="ajpDeclineButton" runat="server">
																				<asp:imagebutton imageurl="~/images/button_decline.gif" alt="Decline" width="77" height="23" border="0" vspace="2" id="btnDecline" runat="server" onclick="btnDecline_Click"></asp:imagebutton>
																			</ajax:ajaxpanel>
																		
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
													<span class="cb"><span class="cl"></span></span>
												</div>
											</td>
											<td width="20"><img runat="server" src="~/images/spacer.gif" width="20" height="1" /></td>
											<td width="698" valign="top" align="center">
												
												<ajax:ajaxpanel id="ajpFriendsList" runat="server">
												<div class="module whitebg">
													<span class="ct"><span class="cl"></span></span>
													<h2 class="alertmessage"><span id="spnAlertMsg" runat="server"></span></h2>
												
													<div align="left" style="padding-left:3px;">
													
													<!-- Friend Requests -->
													<asp:repeater runat="server" id="rptRequests">  
														
														<headertemplate>
															<table width="99%" cellspacing="0" cellpadding="5" border="0" class="data">
																<tr>
																	<th align="center" width="60" bgcolor="#fdfdfc" class="small">Select:<br><a href="javascript:void(0);" onclick="Select_All(true);">All</a> | <a href="javascript:void(0);" onclick="Select_All(false);">None</a></th>
																	<th bgcolor="#fdfdfc">Friend Request Profile</th>
																	<th align="center" bgcolor="#fdfdfc">Age</th>
																	<th align="center" bgcolor="#fdfdfc">Gender</th>
																	<th bgcolor="#fdfdfc">Location</th>
																	<th bgcolor="#fdfdfc">Actions</th>
																</tr>
														</headertemplate>
														
														<itemtemplate>
															
															<tr class="altrow">
																<td align="center"><asp:checkbox runat="server" id="chkEdit" /></td>
																<td>
																	<table cellspadding="0" cellspacing="0" border="0">
																		<tr>
																			<td>
																				<!-- FRIENDS BOX -->
																				<div class="framesize-xsmall">
																					<div class="frame">
																						<span class="ct"><span class="cl"></span></span>
																							<div class="imgconstrain" style="text-align:left;">
																								<a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "name_no_spaces").ToString ()) %>><img runat="server" src='<%# GetProfileImageURL (DataBinder.Eval(Container.DataItem, "thumbnail_small_path").ToString ()) %>' border="0" width="45" height="33"/></a>
																							</div>
																						<span class="cb"><span class="cl"></span></span>
																					</div>
																				</div>		
																			<!-- END FRIENDS BOX -->
																			<input type="hidden" runat="server" id="hidFriendId" value='<%#DataBinder.Eval(Container.DataItem, "user_id")%>' name="hidFriendId">
																			</td>
																			<td><p><a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "name_no_spaces").ToString ()) %>><%# DataBinder.Eval(Container.DataItem, "username") %></a></td>
																		</tr>
																	</table>
																	
																</td>
																<td align="center"><%# ShowAge(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "age"))) %></td>
																<td align="center"><%# DataBinder.Eval(Container.DataItem, "gender") %></td>
																<td align="center"><%# TruncateWithEllipsis(DataBinder.Eval(Container.DataItem, "location").ToString(), 18) %></td>
																<td align="center">
																	
																	<asp:imagebutton runat="server" imageurl="~/images/button_approve.gif" alt="Approve" width="77" height="23" border="0" vspace="2" commandname="cmdApprove" id="btnApproveFriend" /><br/>
																	<asp:imagebutton runat="server" imageurl="~/images/button_decline.gif" alt="Decline" width="77" height="23" border="0" vspace="2" commandname="cmdDecline" id="btnDeclineFriend" />
																
																</td>
															</tr>
															
														</itemtemplate>
														
														<alternatingitemtemplate>
															
															<tr>
																<td align="center"><asp:checkbox runat="server" id="chkEdit" /></td>
																<td>
																	<table cellspadding="0" cellspacing="0" border="0">
																		<tr>
																			<td>
																				<!-- FRIENDS BOX -->
																				<div class="framesize-xsmall">
																					<div class="frame">
																						<span class="ct"><span class="cl"></span></span>
																							<div class="imgconstrain" style="text-align:left;">
																								<a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "name_no_spaces").ToString ()) %>><img runat="server" src='<%# GetProfileImageURL (DataBinder.Eval(Container.DataItem, "thumbnail_small_path").ToString ()) %>' border="0" width="45" height="33" id="Img1"/></a>
																							</div>
																						<span class="cb"><span class="cl"></span></span>
																					</div>
																				</div>		
																			<!-- END FRIENDS BOX -->
																			<input type="hidden" runat="server" id="hidFriendId" value='<%#DataBinder.Eval(Container.DataItem, "user_id")%>' name="hidFriendId">
																			</td>
																			<td><p><a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "name_no_spaces").ToString ()) %>><%# DataBinder.Eval(Container.DataItem, "username") %></a></td>
																		</tr>
																	</table>
																	
																</td>
																<td align="center"><%# ShowAge(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "age"))) %></td>
																<td align="center"><%# DataBinder.Eval(Container.DataItem, "gender") %></td>
																<td align="center"><%# TruncateWithEllipsis(DataBinder.Eval(Container.DataItem, "location").ToString(), 18) %></td>
																<td align="center">
																	
																	<asp:imagebutton runat="server" imageurl="~/images/button_approve.gif" alt="Approve" width="77" height="23" border="0" vspace="2" commandname="cmdApprove" id="btnApproveFriend" /><br/>
																	<asp:imagebutton runat="server" imageurl="~/images/button_decline.gif" alt="Decline" width="77" height="23" border="0" vspace="2" commandname="cmdDecline" id="btnDeclineFriend" />
																
																</td>
															</tr>
															
														</alternatingitemtemplate>
														
														<footertemplate>
														
															</table>
														
														</footertemplate>
														
													</asp:repeater>
													
													</div>
											
													<span class="cb"><span class="cl"></span></span>
												</div>
												</ajax:ajaxpanel>
												
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						</ajax:ajaxpanel>

						
					</td>
					<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
				</tr>
				<tr>
					<td class="frBottomLeft"></td>
					<td class="frBottom"></td>
					<td class="frBottomRight"></td>
				</tr>
				<tr>
					<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" /></td>
				</tr>
			</table>
		</td>
	</tr>
</table>

