<%@ Page language="c#" Codebehind="buyCredits_B.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.mykaneva.buyCredits_B" %>

<link href="../css/new.css" rel="stylesheet" type="text/css">
<link href="../css/shadow.css" rel="stylesheet" type="text/css">
<link href="../css/temp_purchaseFlow.css" rel="stylesheet" type="text/css">

<style>
.on1 td, .on1_ td
{
	font-family: verdana, arial, sans-serif;
	color: #026b84;
	padding: 0px;
	margin: 0;
	background-color: #caedfd;
}
</style>
<script type="text/javascript">

function hilite(row)
{
	row.style.cursor = 'pointer';
	if (row.className == 'on1') return;
	
	if (row.className == 'on1_')
	{
		row.className = '';
	}	
	else if (row.style.backgroundColor == null || row.style.backgroundColor == '')
	{
		row.className = 'on1_';
	}
}

function toggle(row)
{
	if (row.style.backgroundColor == "" || row.className == "off")
	{
		row.className = 'on1';
	}
	   
	var el = document.frmMain.elements;
	var promotion_id = 0;
	//resets the radio buttons and selects the row clicked on
	for (var i=0; i < el.length; i++)
	{
		if (el[i].type.toLowerCase() == 'radio')
		{
			if (el[i].parentNode.parentNode == row)
			{
				el[i].checked = true;
				promotion_id = el[i].value;
			}
			else
			{
				el[i].checked = false;
				if (el[i].parentNode.parentNode.className == "on1")
				{
					el[i].parentNode.parentNode.className = "off";
				}
			}
		}
	}
	//changes the hidden field value to reflect the selected promotion
	var selectedPromotion = document.getElementById('selectedPackage');
	selectedPromotion.value = promotion_id;
}


</script>

<table border="0" cellspacing="0" cellpadding="0"  width="990" align="center">
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" id="Img1" width="1" height="14" /></td>
	</tr>
	<tr>
		<td>
			<table  border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="frTopLeft"></td>
					<td class="frTop"></td>
					<td class="frTopRight"></td>
				</tr>
				<tr>
					<td class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
					<td valign="top" class="frBgIntMembers">
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="968" align="left"  valign="top">
									
									<table cellpadding="0" cellspacing="0" border="0"  width="100%">
										<tr>
											<td width="968" valign="top" align="center">
												<div class="progressBar">
													<!-- WIDTH OF ALL SPANS SHOULD ADD UP TO 958 PX -->
													<ul class="">
														<li id="liSelect" class="">
															<h3>Select Package</h3>
														</li>
														<li id="liPayment" class="">
															<h3>Payment</h3>
														</li>
														<li id="liReceipt" class="">
															<h3>Receipt</h3>
														</li>
													</ul>
												</div>	
											</td>
										</tr>
										<tr>
											<td width="980" valign="top">
											
												<table border="0" cellspacing="0" cellpadding="0" width="100%">
													<tr>
														<!-- START WIZARD CONTENT -->
																												
														<td valign="top" align="center">
															
															<table border="0" width="880px" cellspacing="0" cellpadding="0">
																<tr>
																	<td >

																		<p style="width: 100%;"><strong><span id="spnMessage" runat="server" class="alertmessage"></span></strong></p>
																		
																		<div class="module fullpage">
																		<span class="ct"><span class="cl"></span></span>
																				
																		<table border="0" width="840" cellspacing="0" cellpadding="0" align="center" class="wizform">
																			<tr>
																				<td align="center">
																					<table id="Table11" width="800" cellspacing="0" cellpadding="0" border="0">
																						<tr>
																							<td align="left" valign="top" width="600">
                                                                                                <div id="divSpecials" runat="server" ><h1>Current Specials</h1></div>
                                                                                                <div id="divPackages" runat="server" >
                                                                                                    <h1>Purchase Credits</h1>
                                                                                                    <p>Select the amount of Credits you would like to purchase.</p>
																                                    <p>The more you purchase the bigger the bonus.</p>
																                                    <p>Credits are the currency of The Virtual World of Kaneva.</p>
																	                                <p>Credits enable you to purchase all sorts of items from a variety of locations within the World, such as accessories for your home or clothing to wear.
																	                                Add credits to your balance now!</p>
																	                            </div>																			                    
																							    <asp:Repeater id="rptSpecials" runat="server">
                                                                                                    <ItemTemplate>
                                                                                                        <table id="specialbox" class="specialbox" border="0" runat="server" cellpadding="0" cellspacing="0" width="98%">
                                                                                                            <tr>
                                                                                                                <td><h1 id="specialHeading" runat="server"><%# DataBinder.Eval(Container.DataItem, "bundle_title").ToString () %></h1></td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td><h2 id="specialSub1" runat="server"><%# DataBinder.Eval(Container.DataItem, "bundle_subheading1").ToString () %></h2></td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td><h2 id="specialSub2" runat="server"><%# DataBinder.Eval(Container.DataItem, "bundle_subheading2").ToString () %></h2></td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td><h3 id="specialSub3" runat="server"><%# DataBinder.Eval(Container.DataItem, "promotional_package_label").ToString()%></h3></td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td valign="top" style="padding-left:20px; padding-bottom:10px">
                                                                                                                    <input runat="server" id="ihd_promotionId" type="hidden" value='<%# DataBinder.Eval(Container.DataItem, "promotion_id").ToString() %>' />
									                                                                                <p id="parHeading" runat="server">Plus you'll get these item(s)!</p>
			                                                                                                        <asp:DataList id="dlSpecialItems" runat="server" ShowFooter="False" width="500px" onitemdatabound="dlSpecialItems_ItemDataBound" cellpadding="0" cellspacing="0" border="0" RepeatColumns="2" RepeatDirection="Horizontal">
																												        <ItemTemplate>
																														        <table id="tblspecialItems" border="0" cellpadding="2" cellspacing="0" width="100%">
																														            <tr>
																														                <td id="itemquantity" runat="server" valign="top" align="left"><%# TruncateWithEllipsis(DataBinder.Eval(Container.DataItem, "quantity").ToString(),22) %></td>
																														                <td id="itemname" runat="server" valign="top" align="left" ><%# TruncateWithEllipsis(DataBinder.Eval(Container.DataItem, "alternate_description").ToString() != "" ? DataBinder.Eval(Container.DataItem, "alternate_description").ToString() : DataBinder.Eval(Container.DataItem, "item_description").ToString(), 22)%></td>
																														            </tr>
																														        </table>
																												        </ItemTemplate>
			                                                                                                        </asp:DataList>

			                                                                                                    </td>
                                                                                                            </tr>
																										    <tr>
																										        <td>
																										            <div id="sticker" runat="server" class="badgeSpecial"></div>
																										        </td>
																										    </tr>
                                                                                                        </table>
                                                                                                        <br />
                                                                                                        <br />
                                                                                                    </ItemTemplate>
                                                                                                </asp:Repeater> 
																							</td>
																							<td valign="top" width="240" align="left">   
																								<div class="module whitebg">	
																								    <span class="ct"><span class="cl"></span></span>
																								    <h2>Choose Your Package:</h2>
																								    <input type="hidden" runat="server" id="selectedPackage" />
                                                                                                    <!-- Table/Repeater to list out the offered promotions -->
																								    <table id="tblCreditAmounts" cellspacing="0" cellpadding="0" align="center" border="0" class="buycreds" width="90%">
																								    <asp:repeater id="rptCredits" runat="server" >
																									    <itemtemplate>
																											    <tr id="drPromtion" runat="server" onclick="toggle(this);" onmouseover="hilite(this);" onmouseout="hilite(this);">
																												    <td valign="middle" style="width:10px"><input type="radio" runat="server" id="rbCredit" value='<%# DataBinder.Eval(Container.DataItem, "promotion_id")%>' /></td>
																												    <td valign="middle" id="tddescription" runat="server">
																												        <label><%# DataBinder.Eval(Container.DataItem, "promotion_list_heading")%></label>
																												        <span style="color:#999999;padding:0px;margin-left:5px;position:relative;top:-6px;"><%# DataBinder.Eval(Container.DataItem, "promotion_description")%></span>
																												    </td >
																												    <td valign="middle" id="tddollaramount" runat="server"><label>$<%# DataBinder.Eval(Container.DataItem, "dollar_amount")%></label></td>
																											    </tr>
																									    </itemtemplate>
																								    </asp:repeater> 
																								    </table>
																								    <!-- end promotions Section -->
    																								
																								    <br />
																								    <asp:imagebutton id="btnPurchase" imageurl="~/images/wizard_btn_beginpurchase.gif" runat="Server" onclick="btnPurchase_Click" alternatetext="Begin Purchase" width="200" height="41" border="0"/>
    																								
																								    <span class="cb"><span class="cl"></span></span>
																								</div>
																							</td>
																						
																						</tr>
																					</table>
																				</td>
																			</tr>
																		</table>
																	
																		<span class="cb"><span class="cl"></span></span>
																		</div>
																	</td>
																</tr>
															</table>
														</td>	
														
														<!-- END WIZARD CONTENT -->
													</tr>
												</table>
											
											</td>
											
										</tr>
										
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
				</tr>
				<tr>
					<td class="frBottomLeft"></td>
					<td class="frBottom"></td>
					<td class="frBottomRight"></td>
				</tr>
				
			</table>
		</td>
	</tr>
</table>