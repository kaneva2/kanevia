<%@ Page language="c#" Codebehind="ModuleEditor.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.mykaneva.ModuleEditor" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head>
    <title>Edit</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
  </head>
  <body MS_POSITIONING="GridLayout">
  
    <form id="Form1" method="post" runat="server">    
    <div>
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<td>
					<table border="0" cellpadding="0" cellspacing="0" width="100%">
						<tr>
							<td class="popupBar">
								<table width="100%" border="0" cellspacing="5" cellpadding="0">
									<tr>
										<td style="font-family: arial; font-size: 18px; color:orange;">
											Edit: <asp:Label id="lblHeader" runat="server" />
										</td>

										<td>
											<div align="right">
												<a href="javascript:window.close()" style="text-decoration: none;">Close
													<img src="../images/delete_large.gif" width="11" height="11" border="0" align="absmiddle"></a></div>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td bgcolor="#999999">
								<img runat="server" src="~/images/spacer.gif" width="1" height="2"></td>
						</tr>
					</table>					
				</td>
			</tr>
		</table>
		<asp:PlaceHolder Runat="server" ID="placeHolderEditorControl" />
	</div>
	</form>	
  </body>
</html>
