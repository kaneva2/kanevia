<%@ Page language="c#" Codebehind="creditHistory.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.creditHistory" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../usercontrols/Pager.ascx" %>


<link href="../css/new.css" rel="stylesheet" type="text/css">
<link href="../css/shadow.css" rel="stylesheet" type="text/css">

<table border="0" cellspacing="0" cellpadding="0"  width="990" align="center">
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" /></td>
	</tr>
	<tr>
		<td>
			<table  border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="frTopLeft"></td>
					<td class="frTop"></td>
					<td class="frTopRight"></td>
				</tr>
				<tr>
					<td class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
					<td valign="top" class="frBgIntMembers">
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="968" align="left"  valign="top">
									
									<!-- TOP STATUS BAR -->
									<div id="pageheader">
										<table cellpadding="0" cellspacing="0" border="0" width="99%">
											<tr>
												<td align="left">
													<h1>Credit Purchase History</h1>
												</td>
									
												<td align="right" valign="middle">
													<table cellpadding="0" cellspacing="0" border="0" width="690">
														<tr>
															<td>	
																<span id='ajpTopStatusBar$RBS_Holder'><span id="ajpTopStatusBar" ajaxcall="async">
																<table cellpadding="0" cellspacing="0" border="0" width="100%">
																	<tr>
																		<td class="headertout" width="175"></td>
																		<td class="searchnav" width="260"></td>
																		<td align="left" width="130"></td>
																		<td class="searchnav" align="right" width="210" nowrap></td>
																	</tr>
																</table>
																</span></span>
															</td>	
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</div>
									<!-- END TOP STATUS BAR -->		
								
									<table cellpadding="0" cellspacing="0" border="0" width="100%">
										<tr>
											<td width="970" valign="top" align="center">
											
												<table border="0" cellspacing="0" cellpadding="0" width="99%">
													<tr>
														<!-- START WIZARD CONTENT -->
														<td valign="top" align="center">
																		
															<div class="module whitebg fullpage">
															<span class="ct"><span class="cl"></span></span>
																	
															<table border="0" cellspacing="0" cellpadding="0" align="center" class="wizform" style="width: 99%;">
																<tr>
																	<td align="center">
																		
																		<table width="99%" cellspacing="0" cellpadding="0" border="0">
																			<tr>
																				<td align="center">
																					<br>
																					
																					<asp:repeater id="rptOrders" runat="server">
																					
																						<headertemplate>
																						
																							<table border="0" cellspacing="0" cellpadding="5" width="94%" align="center" class="data nopadding">
																								<tr>
																									<td colspan="5" style="padding: 8px 0 8px 5px;"><h2>Completed Order Summary</h2></td>
																								</tr>
																								<tr>
																									<td align="left" class="bold" bgcolor="#fdfdfc">Order Number:</td>
																									<td align="center" class="bold" bgcolor="#fdfdfc">Amount</td>
																									<td align="center" class="bold" bgcolor="#fdfdfc">Credits Received</td>
																									<td align="center" class="bold" bgcolor="#fdfdfc">Payment Method</td>
																									<td align="left" class="bold" bgcolor="#fdfdfc">Purchase Date</td>
																								</tr>
																							
																						</headertemplate>
																						
																						<itemtemplate>
																						
																							<tr>
																								<td align="left"><a href="historyorderdetails.aspx?orderId=<%# DataBinder.Eval(Container.DataItem, "order_id")%>"><strong><%# DataBinder.Eval(Container.DataItem, "order_id")%></strong></a><br><%# DataBinder.Eval(Container.DataItem, "ppt_description")%></td>
																								<td align="center"><%# FormatCurrency (DataBinder.Eval(Container.DataItem, "amount_debited"))%></td>
																								<td align="center"><%# FormatCredits (DataBinder.Eval(Container.DataItem, "kpoints")) %></td>
																								<td align="center" nowrap><%# FormatPaymentType (DataBinder.Eval (Container.DataItem, "name"), DataBinder.Eval (Container.DataItem, "card_number")) %></td>
																								<td align="right" nowrap><%# FormatDateTime (DataBinder.Eval(Container.DataItem, "transaction_date"))%></td>
																							</tr>	
																						
																						</itemtemplate>
																						
																						<alternatingitemtemplate>
																						
																							<tr class="altrow">
																								<td align="left"><a href="historyorderdetails.aspx?orderId=<%# DataBinder.Eval(Container.DataItem, "order_id")%>"><strong><%# DataBinder.Eval(Container.DataItem, "order_id")%></strong></a><br><%# DataBinder.Eval(Container.DataItem, "ppt_description")%></td>
																								<td align="center"><%# FormatCurrency (DataBinder.Eval(Container.DataItem, "amount_debited"))%></td>
																								<td align="center"><%# FormatCredits (DataBinder.Eval(Container.DataItem, "kpoints")) %></td>
																								<td align="center" nowrap><%# FormatPaymentType (DataBinder.Eval (Container.DataItem, "name"), DataBinder.Eval (Container.DataItem, "card_number")) %></td>
																								<td align="right" nowrap><%# FormatDateTime (DataBinder.Eval(Container.DataItem, "transaction_date"))%></td>
																							</tr>
																						
																						</alternatingitemtemplate>
																						
																						<footertemplate>
																						
																							</table>
																						
																						</footertemplate>
																						
																					</asp:repeater> 
																					
																					<br><br>
																					<table border="0" cellspacing="0" cellpadding="0" width="94%">
																						<tr>
																							<td align="right"><a href="~/mykaneva/managecredits.aspx" runat="server">Back to Manage Credits</a></td>
																						</tr>
																					</table>
																				
																				</td>
																			</tr>
																		</table>
																		
																	</td>
																</tr>
															</table>
														
															<span class="cb"><span class="cl"></span></span>
															</div>
												
															<div id="smallpage_spacer"></div>
															
														</td>
													</tr>
												</table>
												
											</td>
										</tr>
									</table>
									
								</td>
							</tr>
						</table>
					
					</td>
					<td class="frBorderRight"><img src="~/images/spacer.gif" runat="server" width="1" height="1" /></td>
				</tr>
				<tr>
					<td class="frBottomLeft"></td>
					<td class="frBottom"></td>
					<td class="frBottomRight"></td>
				</tr>
				
			</table>
		</td>
	</tr>
</table>
