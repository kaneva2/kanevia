<%@ Page language="c#" Codebehind="settings.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.settings" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>

<link href="../css/new.css" type="text/css" rel="stylesheet">




<table border="0" cellspacing="0" cellpadding="0" class="newcontainer" align="center">
	<tr>
		<td>
			<table border="0" cellpadding="0" cellspacing="0" width="100%" class="newcontainerborder">
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
				<tr>
					<td class=""><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
					<td valign="top" class="newdatacontainer">
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
								
									<!-- TOP STATUS BAR -->
									<div id="pageheader">
										<table cellpadding="0" cellspacing="0" border="0" width="99%">
											<tr>
												<td align="left">
													<h1>My Account Preferences</h1>
													<h3>Select your content, privacy, and email notification preferences below.</h3>
												</td>
											</tr>
										</table>
									</div>
									<!-- END TOP STATUS BAR -->		
									<div style="width:100%;text-align:center;">
									<ajax:ajaxpanel id="Ajaxpanel25" runat="server">
										<asp:validationsummary cssclass="errBox black" id="valSum" validationgroup="game" runat="server" showmessagebox="False" showsummary="True"
											displaymode="SingleParagraph" style="margin-top:10px;width:80%;text-align:center;" headertext="" forecolor="black" visible="false"></asp:validationsummary>
										<asp:customvalidator id="cvBlank" runat="server" validationgroup="game" display="None" enableclientscript="False"></asp:customvalidator>
									</ajax:ajaxpanel>
									</div>
								</td>
							</tr>
							<tr>
								<td width="968" align="left"  valign="top">
								<br>
									<table border="0" cellspacing="0" cellpadding="0" width="100%">
										<tr>
											<td width="478" valign="top" align="center">												
												<div class="module whitebg">
												<span class="ct"><span class="cl"></span></span>
												
													<h2>Privacy Settings</h2>	
													<table cellpadding="3" cellspacing="0" width="90%" align="center">
														<tr>
															<td rowspan="2" valign="top"><asp:checkbox cssclass="formKanevaCheck" runat="server" id="chkBrowse"/></td>
															<td class="bold">Browse anonymously</td>
														</tr>
														<tr>
															<td class="note">Set this option if you do not want others to know that you visited their profile or community.</td>
														</tr>
														<tr>
															<td rowspan="2" valign="top"><asp:checkbox cssclass="formKanevaCheck" runat="server" id="chkOnlineStatus"/></td>
															<td class="bold">Show On-Site/In-World status</td>
														</tr>
														<tr>
															<td class="note">Set this option if you want others to see whether or not you are currently logged onto the site or are in-world.</td>
														</tr>
													</table>
													<br>
												<span class="cb"><span class="cl"></span></span>
												</div>
												
												<div class="module whitebg">
												<span class="ct"><span class="cl"></span></span>
												
													<h2>Country Filter Settings</h2>	
													<table cellpadding="3" cellspacing="0" width="90%" align="center">
														<tr>
															<td rowspan="2" valign="top"><asp:checkbox cssclass="formKanevaCheck" runat="server" id="chkCountryFilter"/></td>
															<td class="bold">Filter By Country</td>
														</tr>
														<tr>
															<td class="note">Set this option if you want to filter web and world content by your country.</td>
														</tr>
														<tr>
															<td colspan="2"><img id="Img5" runat="server" src="~/images/spacer.gif" width="1" height="10" /></td>
														</tr>
														
													</table>
													<br>
												<span class="cb"><span class="cl"></span></span>
												</div>
												
												<div class="module whitebg">
												<span class="ct"><span class="cl"></span></span>
												
													<h2>Always Purchase With Currency Settings</h2>								   
													<table cellpadding="3" cellspacing="0" width="90%" align="center">
														<tr>
															<td rowspan="2" valign="top"><asp:checkbox cssclass="formKanevaCheck" onclick="javascript:if(this.checked){document.getElementById('chkAlwaysUseRewards').checked=false;}" runat="server" id="chkAlwaysUseCredits"/></td>
															<td class="bold">Always Purchase with Credits</td>
														</tr>
														<tr>
															<td class="note">Set this option if you want to always use Dredits when making purchases.</td>
														</tr>
														<tr>
															<td colspan="2"><img id="Img6" runat="server" src="~/images/spacer.gif" width="1" height="10" /></td>
														</tr>
														<tr>
															<td rowspan="2" valign="top"><asp:checkbox cssclass="formKanevaCheck" onclick="javascript:if(this.checked){document.getElementById('chkAlwaysUseCredits').checked=false;}" runat="server" id="chkAlwaysUseRewards"/></td>
															<td class="bold">Always Purchase with Rewards</td>
														</tr>
														<tr>
															<td class="note">Set this option if you want to always use Rewards when making purchases. Does not apply to Worlds.</td>
														</tr>
													</table>																	  
													<br>
												<span class="cb"><span class="cl"></span></span>
												</div>
																								
												<div class="module whitebg">
												<span class="ct"><span class="cl"></span></span>
												
													<h2>Request Blocking</h2>								   
													<table cellpadding="3" cellspacing="0" width="90%" align="center">
														<tr>
															<td><a href="blast/blastFilters.aspx">Manage</a></td>
														</tr>
														
													</table>																	  
													<br>
												<span class="cb"><span class="cl"></span></span>
												</div>												

											</td>
											<td width="10"></td>
											<td width="568" valign="top" colspan="3">
												<div class="module whitebg">
												<span class="ct"><span class="cl"></span></span>			                                        
													<h2>Email Settings</h2>	
													<table cellpadding="3" cellspacing="0" width="90%" align="center">
														<tr>
															<td rowspan="2" valign="top"><asp:checkbox cssclass="formKanevaCheck" runat="server" id="chkNewsletter"/></td>
															<td class="bold">Yes, send me the Kaneva Newsletter</td>
														</tr>
														<tr>
															<td class="note">a monthly email with news, special offers, and events in Kaneva. (Note: Add newsletter@kaneva.com to your Address Book.)</td>
														</tr>
														<tr>
															<td colspan="2"><img runat="server" src="~/images/spacer.gif" width="1" height="10" /></td>
														</tr>
														
														<tr>
															<td rowspan="2" valign="top"><asp:checkbox cssclass="formKanevaCheck" runat="server" id="chkUpdates"/></td>
															<td class="bold">Yes, send me Kaneva update<s/td>
														</tr>
														<tr>
															<td class="note">occasional emails with information on how to get the most from Kaneva. (Note: Add insider@kaneva.com to your Address Book.)</td>
														</tr>
														<tr>
															<td colspan="2"><img id="Img2" runat="server" src="~/images/spacer.gif" width="1" height="10" /></td>
														</tr>
														
														<tr>
															<td rowspan="2" valign="top"><ajax:ajaxpanel runat="server"><asp:checkbox cssclass="formKanevaCheck" runat="server" id="chkNotifications" autopostback="true" OnCheckedChanged="toggleFriendNotifications" /></ajax:ajaxpanel></td>
															<td class="bold">Yes, send me Friend Notifications</td>
														</tr>
														<tr>
															<td class="note">regular emails that notify me when I receive a private message, comment, friend request, 
															blog comment, and/or event invitation from another Kaneva member.  (Note: Add kaneva@kaneva.com to 
															your Address Book.) Check options below:</td>
														</tr>
														<tr>
															<td colspan="2"><img id="Img3" runat="server" src="~/images/spacer.gif" width="1" height="10" /></td>
														</tr>
														
														<tr>
														<td></td>
														<td>
															<ajax:ajaxpanel runat="server">
														    <table>														    														   														
														        <tr>
															        <td valign="top"><asp:checkbox cssclass="formKanevaCheck" runat="server" id="chkBlogcomments"/></td>
															        <td class="">Send me Blog Comments </td>
														        </tr>														        														        
														        <tr>
															        <td valign="top"><asp:checkbox cssclass="formKanevaCheck" runat="server" id="chkProfile"/></td>
															        <td class="">Send me Profile Comments</td>
														        </tr>														        
														        <tr>
															        <td valign="top"><asp:checkbox cssclass="formKanevaCheck" runat="server" id="chkFriendMessage"/></td>
															        <td class="">Send me Friend Messages (private messages from my friends)</td>
														        </tr>														        
														        <tr>
															        <td valign="top"><asp:checkbox cssclass="formKanevaCheck" runat="server" id="chkAnyoneMessage"/></td>
															        <td class="">Send me Member Messages (private messages from other Kaneva members)</td>
														        </tr>
														        <tr>
															        <td valign="top"><asp:checkbox cssclass="formKanevaCheck" runat="server" id="chkFriendRequest"/></td>
															        <td class="">Send me Friend Requests</td>
														        </tr>
														        <tr>
															        <td valign="top"><asp:checkbox cssclass="formKanevaCheck" runat="server" id="chkNewFriends"/></td>
															        <td class="">Send me New Friend Notications</td>
														        </tr>
														        <tr>
															        <td valign="top"><asp:checkbox cssclass="formKanevaCheck" runat="server" id="chkBlastComments"/></td>
															        <td class="">Send me Blast Comments</td>
														        </tr>
														        <tr>
															        <td valign="top"><asp:checkbox cssclass="formKanevaCheck" runat="server" id="chkMediaComments"/></td>
															        <td class="">Send me Media Comments</td>
														        </tr>
														        <tr>
															        <td valign="top"><asp:checkbox cssclass="formKanevaCheck" runat="server" id="chkShopComments"/></td>
															        <td class="">Send me Shop Comments and Purchases</td>
														        </tr>
														        <tr>
															        <td valign="top"><asp:checkbox cssclass="formKanevaCheck" runat="server" id="chkEventInvites"/></td>
															        <td class="">Send me Event Invites and Comments</td>
														        </tr>
														        <tr>
															        <td valign="top"><asp:checkbox cssclass="formKanevaCheck" runat="server" id="chkFriendBirthdays"/></td>
															        <td class="">Send me Friends' Birthdays</td>
														        </tr>
														        <tr>
															        <td valign="top"><asp:checkbox cssclass="formKanevaCheck" runat="server" id="chkWorldBlasts"/></td>
															        <td class="">Send me World Blast Emails</td>
														        </tr>
                                                                <tr>
															        <td valign="top"><asp:checkbox cssclass="formKanevaCheck" runat="server" id="chkWorldRequests"/></td>
															        <td class="">Send me World Request Emails</td>
														        </tr>
														        <tr>
															        <td colspan="2"><img id="Img8" runat="server" src="~/images/spacer.gif" width="1" height="10" /></td>
														        </tr>
															</table>
															</ajax:ajaxpanel>														
														</td>
														</tr>
														<tr>
														    <td colspan=2><p>Kaneva is committed to maintaining the trust and confidence of our members. We will keep your personal 
														    information private. Click here to read our Privacy Policy.</p>   
														    <br />                                                         
                                                            <p>Can't find your Kaneva emails? Check your junk mail folder. To ensure you receive our emails, add 
                                                            kaneva@kaneva.com to your Address Book.</p></td>
														</tr>
														
													</table>
													

												<span class="cb"><span class="cl"></span></span>
												</div>	
											</td>
										</tr>
									</table>   
									<div style="text-align:right;width:99%;padding-bottom:8px;">
										<asp:button id="btnCancel" runat="Server" causesvalidation="False" onclick="btnCancel_Click" class="Filter2" text="  cancel  "/>&nbsp;&nbsp;
										<asp:button id="btnUpdate" runat="Server" onclick="btnUpdate_Click" class="Filter2" text="  submit  "/>
									</div>
								</td>
							</tr>
						</table>
					</td>
					<td class=""><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
				</tr>
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
				
			</table>
		</td>
	</tr>
</table>
<br/>