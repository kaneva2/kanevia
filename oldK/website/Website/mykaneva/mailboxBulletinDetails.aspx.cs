///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using log4net;

namespace KlausEnt.KEP.Kaneva.mykaneva
{
	/// <summary>
	/// Summary description for mailboxBulletinDetails.
	/// </summary>
	public class mailboxBulletinDetails : MainTemplatePage
	{
		protected mailboxBulletinDetails () 
		{
			Title = "Bulletin Details";
		}

		private void Page_Load (object sender, System.EventArgs e)
		{

			// They must be logged in
			if (!Request.IsAuthenticated)
			{
				Response.Redirect (GetLoginURL ());
				return;
			}

			if (!IsPostBack)
			{
				int messageId = Convert.ToInt32 (Request ["bulletinId"]);

				DataRow drMessage = UsersUtility.GetBulletin (GetUserId (), messageId);

				if (drMessage ["username"].Equals (DBNull.Value))
				{
					// May be trying to access another users message
					m_logger.Warn ("UserId '" + GetUserId () + "' tried to access bulletin '" + messageId + "' and nothing returned.");
					divNoAccess.Visible = true;
					divMsgDetails.Visible = false;
					return;
				}

				lnkFrom.Text = drMessage ["username"].ToString ();
				lnkFrom.Attributes.Add ("href", GetPersonalChannelUrl (drMessage ["name_no_spaces"].ToString ()));

				aFromAvatar.HRef = GetPersonalChannelUrl (drMessage ["name_no_spaces"].ToString ());
				imgFromAvatar.Src = GetProfileImageURL (drMessage ["thumbnail_small_path"].ToString ());

				lblDate.Text = FormatDateTime (drMessage ["message_date"]);
				lblSubject.Text = drMessage ["subject"].ToString ();
				lblBody.Text = drMessage ["message"].ToString ();
			}

			// Set Nav
            HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
			HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.MESSAGES;
			HeaderNav.MyMessagesNav.ActiveTab = NavMessages.TAB.BULLETINS;
			HeaderNav.SetNavVisible(HeaderNav.MyKanevaNav,2);
			HeaderNav.SetNavVisible(HeaderNav.MyMessagesNav);
		}	

		
		#region Event Handlers
		/// <summary>
		/// Back Event Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnBack_Click (object sender, ImageClickEventArgs e) 
		{
			if (Request ["returnURL"] != null)
			{
				Response.Redirect (ResolveUrl (Request ["returnURL"].ToString ()));
			}

			Response.Redirect (ResolveUrl ("~/mykaneva/mailboxBulletin.aspx"));
		}

		#endregion

		#region Declerations
		
		protected HyperLink		lnkFrom;
		protected HtmlAnchor	aFromAvatar;
		protected HtmlImage		imgFromAvatar;
		protected Label			lblDate, lblSubject, lblBody;
		
		protected HtmlContainerControl	divNoAccess, divMsgDetails;
		protected HtmlGenericControl	spanSpamBlock, spnAlertMsg;

		// Logger
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);
		
		#endregion


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}