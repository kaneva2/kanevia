<%@ Page language="c#" Codebehind="urlSelection.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.urlSelection" %>

<link href="../css/kaneva.css" rel="stylesheet" type="text/css" />
<link href="../css/kanevaText.css" type="text/css" rel="stylesheet">
<link href="../css/kanevaSC.css" rel="stylesheet" type="text/css"/>
<link href="../css/friends.css" rel="stylesheet" type="text/css"/>

<br/><br/>  
<table cellpadding="0" cellspacing="0" border="0" style="width:100%">
	<tr>
		<td>
			<asp:ValidationSummary ShowMessageBox="False" ShowSummary="True" Class="formError" id="valSum" DisplayMode="BulletList" runat="server" HeaderText="Please correct the following errors:"/>
		</td>
	</tr>
</table>      
<table border="0" cellpadding="0" cellspacing="0" width="80%">
	<tr>
		<td align="left" class="insideTextNoBold">Reserve your World URL for World <b id="bChannelName" runat="server"></b><br/><br/></td>
	</tr>
	<tr>
		<td align="left" class="insideTextNoBold" style="padding-bottom:6px;">
			This short URL allows you to promote your World on blogs and email signatures with a simple URL that you define.<br/>
		</td>
	</tr>
	<tr>
		<td align="left" class="insideTextNoBold">
			http://<asp:Label runat="server" id="lblHost"/>/<asp:textbox id="txtURL" style="width:300px" class="formKanevaText" maxlength="50" runat="server"/>
			<asp:regularexpressionvalidator id="revURL" runat="server" text="*" errormessage="URL must be at least four characters and can contain only letters, numbers and underscores." display="Dynamic" controltovalidate="txtURL"/>
		</td>
	</tr>
	<tr>
		<td align="left" class="insideTextNoBold">
			<br/><br/>
			<span color="red">Warning:</span> The URL for this World is permanent and cannot be changed after you reserve it. 
		</td>
	</tr>
 </table>
 <table width="80%"  border="0" cellspacing="0" cellpadding="0">
    <tr>
    <td width="100%" align="right"><asp:button id="btnCancel" runat="Server" CausesValidation="False" onClick="btnCancel_Click" class="Filter2" Text="    cancel    "/></td>
    <td><span class="textGeneralGray01"><img runat="server" src="~/images/spacer.gif" width="2" height="8"/></span></td>
    <td width="6%" align="right"><asp:button id="btnUpdate" runat="Server" onClick="btnUpdate_Click" class="Filter2" Text="    submit    "/></td>
    </tr>
</table>
	
<br/><br/><br/><br/>