///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for paymentInformation.
	/// </summary>
	public class paymentInformation : SortedBasePage
	{
		protected paymentInformation () 
		{
			Title = "Payment Information";
		}

		private void Page_Load (object sender, System.EventArgs e)
		{
			// They must be logged in
			if (!Request.IsAuthenticated)
			{
				Response.Redirect (GetLoginURL ());
				return;
			}

			if (!IsPostBack)
			{
				int userId = GetUserId ();

//				// User information
//				DataRow drUser = UsersUtility.GetUser (userId);
//				lblFirstName.Text = drUser ["first_name"].ToString ();
//				lblLastName.Text = drUser ["last_name"].ToString ();
//				lblPostalCode.Text = drUser ["zip_code"].ToString ();
//				lblCountry.Text = drUser ["country_name"].ToString ();

				// Payment information
				DataTable dtUserPaymentInfo = UsersUtility.GetUserPaymentInfo (userId, "");

				if (dtUserPaymentInfo.Rows.Count == 0)
				{
					trNoTaxes.Visible = true;
					tblCurrentW9Information.Visible = false;
				}
				else
				{
					trNoTaxes.Visible = false;
					tblCurrentW9Information.Visible = true;
					
					BindData ();
				}

			}
		}

		/// <summary>
		/// They clicked to sort a column
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected override void btnSort_Click (object sender, System.EventArgs e)
		{
			base.btnSort_Click (sender, e);
			BindData ();
		}

		/// <summary>
		/// DEFAULT_SORT
		/// </summary>
		/// <returns></returns>
		protected override string DEFAULT_SORT
		{
			get
			{
				return "submitted_date";
			}
		}

		private void BindData ()
		{
			// Set the sortable columns
			SetHeaderSortText (dgrdTax);
			string orderby = CurrentSort + " " + CurrentSortOrder;

			DataTable dtUserPaymentInfo = UsersUtility.GetUserPaymentInfo ( GetUserId (), orderby);

			dgrdTax.DataSource = dtUserPaymentInfo;
			dgrdTax.DataBind ();
		}

		/// <summary>
		/// GetForm
		/// </summary>
		/// <param name="isW9"></param>
		/// <param name="isW8"></param>
		/// <returns></returns>
		protected string GetForm (string isW9, string isW8)
		{
			if (isW8.Equals ("Y"))
			{
				return "W-8";
			}
			else
			{
				return "W-9";
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnSubmitW9_Click (object sender, EventArgs e) 
		{
			Response.Redirect (ResolveUrl ("~/mykaneva/submitW9.aspx?subsubtab=payment"));
		}

		protected PlaceHolder phBreadCrumb;
		protected DataGrid dgrdTax;

		protected HtmlTable tblCurrentW9Information;
		protected HtmlTableRow trNoTaxes;

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
