<%@ Page language="c#" ValidateRequest="False" Codebehind="pageSettings.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.pageSettings" %>

<link href="../css/new.css" rel="stylesheet" type="text/css" />

<link href="../css/editWidgets.css" rel="stylesheet" type="text/css" />
<script language="Javascript" src="../jscript/PageLayout/colorpicker.js"></SCRIPT>
<script language="javascript" src="../jscript/prototype.js"></script>


<script language="Javascript" type="text/javascript">
function showAdvanced()
{
	$('tblMoreOptions').style.display="";
	$('advancedLink').style.display="none"; 
	$('lessLink').style.display="";
}
function showLess()
{
	$('tblMoreOptions').style.display="none";
	$('advancedLink').style.display=""; 
	$('lessLink').style.display="none";	
}
</script>


<table cellSpacing="0" cellPadding="0" width="990" align="center" border="0">
	<tr>
		<td><img id="Img6" height="14" runat="server" src="~/images/spacer.gif" width="1"></td>
	</tr>
	<tr>
		<td>
			<table cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td class="frTopLeft"></td>
					<td class="frTop"></td>
					<td class="frTopRight"></td>
				</tr>
				<tr>
					<td class="frBorderLeft"><img id="IMG7" height="1" runat="server" src="~/images/spacer.gif" width="1"></td>
					<td class="frBgIntMembers" vAlign="top">

						<table cellSpacing="0" cellPadding="0" border="0">
							<tr>
								<td height="5" colspan="3"><IMG src="~/images/spacer.gif" border="0" runat="server" height="5"></td>
							</tr>
							<tr>
								<td  vAlign="top">
									<table cellSpacing="0" cellPadding="0" border="0">
										<tr>
											<td vAlign="top">
												<table class="bgcolorConetent" cellSpacing="0" cellPadding="0" border="0">
													<tr>
														<td><IMG src="~/images/content/insideLeftCornerTop.gif" border="0" runat="server"></td>
														<td class="insideTopBoxContent" width="100%"></td>
														<td align="right"><IMG src="~/images/content/insideRightCornerTop.gif" border="0" runat="server"></td>
													</tr>
													<tr>
														<td class="insideLeftBoxContent"></td>
														<td align="left" vAlign="top"> 
															<table cellSpacing="0" cellPadding="0" border="0" width="220">
																<tr>
																	<td height="6"><IMG src="~/images/spacer.gif" border="0" runat="server" height="6"></td>
																</tr>
																<tr>
																	<td>
																		<table cellSpacing="0" cellPadding="0" width="100%" border="0">
																			<tr>
																				<td width="8"></td>
																				<td class="channelStyle" vAlign="top" align="left">Themes</td>
																				<td vAlign="top" align="right"></td>
																			</tr>
																		</table>
																	</td>
																</tr>
																<tr>
																	<td height="11"><IMG src="~/images/spacer.gif" border="0" runat="server" height="11"></td>
																</tr>
																<tr>
																	<td align="center">
																		<table class="bgBoxInside" cellSpacing="0" cellPadding="0" width="210" border="0">
																			<tr>
																				<td><IMG src="~/images/content/insideLeftCornerTop.gif" border="0" runat="server"></td>
																				<td class="insideTopBoxContent"></td>
																				<td align="right"><IMG src="~/images/content/insideRightCornerTop.gif" border="0" runat="server"></td>
																			</tr>
																			<tr>
																				<td class="insideLeftBoxContent"></td>
																				<td>
																					<table style="width:200px" cellSpacing="0" cellPadding="4" border="0">
																						<tr>
																							<td>
																								<asp:datalist id="dlThemes" runat="server" Repeatdirection="Horizontal" width="100%" Visible="true"
																									ShowFooter="False" cellpadding="2" cellspacing="2" border="0" RepeatColumns="1">
																									<ItemStyle HorizontalAlign="Left" />
																									<ItemTemplate>
																										<input id="rbTheme" name="rbTheme" type="radio" value='<%# DataBinder.Eval( Container.DataItem, "standard_template_id")%>' onclick="<%# GetPreviewJavascript (DataBinder.Eval(Container.DataItem, "preview_url").ToString ()) %>"/>
																										<img runat="server" src='<%# DataBinder.Eval(Container.DataItem, "preview_url") %>' title='<%# DataBinder.Eval(Container.DataItem, "description") %>' height="30" width="30" border="1"/>
																										<span class="dateStamp">
																											<%# DataBinder.Eval(Container.DataItem, "name") %>
																										</span>
																									</ItemTemplate>
																								</asp:datalist>
																								
																							</td>
																						</tr>
																					</table>
																				</td>
																				<td class="insideRightBoxContent"></td>
																			</tr>
																			<tr>
																				<td><IMG src="~/images/content/insideLeftCornerBottom.gif" border="0" runat="server"></td>
																				<td class="insideBottomBoxContent"></td>
																				<td align="right"><IMG src="~/images/content/insideRightCornerBottom.gif" border="0" runat="server"></td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
														</td>
														<td class="insideRightBoxContent"></td>
													</tr>
													<tr>
														<td class="insideLeftBoxContent"></td>
														<td height="5"></td>
														<td class="insideRightBoxContent"></td>
													</tr>
													<tr>
														<td width="5"><IMG src="~/images/content/insideLeftCornerBottom.gif" border="0" runat="server"></td>
														<td class="insideBottomBoxContent" width="100%"></td>
														<td align="right"><IMG src="~/images/content/insideRightCornerBottom.gif" border="0" runat="server"></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
								<td width="15">&nbsp;&nbsp;</td>
								<td vAlign="top">
									<!--addin borders corner -->
									<table class="bgBoxChannel" cellSpacing="0" cellPadding="0" width="650" border="0">
										<tr>
											<td width="5"><IMG src="~/images/content/leftCornerTop.gif" border="0" runat="server"></td>
											<td class="topBoxContent" width="100%"></td>
											<td align="right"><IMG src="~/images/content/rightCornerTop.gif" border="0" runat="server"></td>
										</tr>
										<tr>
											<td class="insideleftBoxContent"></td>
											<td>
												<table class="bgBoxChannel" cellSpacing="0" cellPadding="0" width="100%" border="0">
													<tr>
														<td class="textBodyChannel" vAlign="top" align="left" height="5"><IMG src="~/images/spacer.gif" border="0" runat="server" height="5"></td>
													</tr>
													<tr>
														<td width="8">&nbsp;</td>
														<td>
															<table cellSpacing="0" cellPadding="0" width="100%" border="0">
																<tr>
																	<td width="4"></td>
																	<td class="channelStyle" vAlign="middle" height="23">Theme Preview</td>
																</tr>
																<tr>
																	<td height="6" colspan="2"><IMG src="~/images/spacer.gif" border="0" runat="server" height="6"></td>
																</tr>
															</table>
														</td>
														<td width="15">&nbsp;&nbsp;&nbsp;</td>
														<td vAlign="top" align="right">
															<table class="widgetCellSC" cellSpacing="0" cellPadding="0" border="0">
																<tr>
																	<td vAlign="top" align="left" colSpan="3">
																		<table cellSpacing="0" cellPadding="0" width="100%" border="0">
																			<tr>
																				<td width="80" colspan="3"></td>
																				<td align="right">
																					<table border="0" cellpadding="0" cellspacing="0">
																						<tr>
																							<td align="center" valign="middle"><A href="javascript:resetForm();" class="nohover"><IMG runat="server" src="~\images\buttons\reset_bt.gif" border="0" /></A></td>
																							<td>&nbsp;</td>
																							<td align="center" valign="middle" class="bgbutton"><asp:button id="btnUpdateCustomize" class="transparentButton" onclick="btnUpdateThemeCustomize_Click" style="_cursor:hand;cursor:pointer;" 
																									text="        " runat="Server" BorderColor="Transparent" BackColor="Transparent" BorderStyle="None" BorderWidth="0px" Width="38px" Height="23px"></asp:button></td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
														</td>
														<td>&nbsp;</td>
													</tr>
												</table>

												<table class="bgBoxChannel" cellSpacing="0" cellPadding="0" width="203" border="0">
													<tr>
														<td><IMG src="~/images/content/insideLeftCornerTop.gif" border="0" runat="server"></td>
														<td class="insideTopBoxContent" width="100%"></td>
														<td align="right"><IMG src="~/images/content/insideRightCornerTop.gif" border="0" runat="server"></td>
													</tr>
													<tr>
														<td class="insideLeftBoxContent"></td>
														<td>
															<table class="bgBoxChannel" style="width:100%" height="180" cellSpacing="0" cellPadding="0" border="0">
																<tr>
																	<td class="textBodyChannel" vAlign="top" align="left">
																		<table border="0" cellpadding="0" cellspacing="0">
																			<tr>
																				<td height="5"><IMG src="~/images/spacer.gif" border="0" runat="server" height="5"></td>
																			</tr>
																			<tr>
																				<td width="10"></td>
																				<td class="textBodyChannel"></td>
																			</tr>
																		</table>
																	</td>
																	<td vAlign="top" align="left"></td>
																</tr>
																<tr>
																	<td vAlign="top" align="center" colSpan="2">
																		<table cellSpacing="0" cellPadding="0" width="170" border="0">
																			<tr>
																				<td><IMG src="~/images/content/leftTopCornerGray.gif" border="0" runat="server"></td>
																				<td class="insideTopBoxGrayContent" width="100%"></td>
																				<td align="right"><IMG src="~/images/content/rightTopCornerGray.gif" border="0" runat="server"></td>
																			</tr>
																			<tr>
																				<td class="insideLeftBoxGrayContent"></td>
																				<td class="bgBoxInsideChannel">
																					<table cellSpacing="0" cellPadding="0" width="170" border="0">
																						<tr>
																							<td colSpan="3" height="5"><IMG src="~/images/spacer.gif" border="0" runat="server" height="5"></td>
																						</tr>
																						<tr>
																							<td width="5"></td>																			
																							<td align="center"><IMG runat="server" id="imgThemePreview" src="~\images\imgBgTheme.gif" Height="255" Width="340"></td>
																							<td width="5"></td>
																						</tr>
																						<tr>
																							<td colSpan="3" height="5"><IMG src="~/images/spacer.gif" border="0" runat="server" height="5"></td>
																						</tr>
																					</table>
																				</td>
																				<td class="insideRightBoxGrayContent"></td>
																			</tr>
																			<tr>
																				<td><IMG src="~/images/content/leftBottomCornerGray.gif" border="0" runat="server"></td>
																				<td class="insideBottomBoxGrayContent" width="100%"></td>
																				<td align="right"><IMG src="~/images/content/rightBottomCornerGray.gif" border="0" runat="server"></td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
														</td>
														<td class="insideRightBoxContent"></td>
													</tr>
													<tr>
														<td width="5"><IMG src="~/images/content/insideLeftCornerBottom.gif" border="0" runat="server"></td>
														<td class="insideBottomBoxContent" width="100%"></td>
														<td align="right"><IMG src="~/images/content/insideRightCornerBottom.gif" border="0" runat="server"></td>
													</tr>
												</table>
											</td>
											<td class="insiderightBoxContent"></td>
										</tr>
										<tr>
											<td class="insideleftBoxContent"></td>
											<td height="14"><IMG src="~/images/spacer.gif" border="0" runat="server" height="14"></td>
											<td class="insiderightBoxContent"></td>
										</tr>
										<tr>
											<td class="insideleftBoxContent"></td>
											<td vAlign="middle" >
												<!--   Advanced Options  -->
												<table cellSpacing="0" cellPadding="0" border="0">
													<tr>
														<!--Space Left-->
														<td width="8">&nbsp;</td>
														<td>
															<table cellSpacing="0" cellPadding="0" width="100%" border="0">
																<tr>
																	<td width="4"></td>
																	<td class="channelStyle" vAlign="middle" height="23">Custom Settings</td>
																</tr>
																<tr>
																	<td height="6" colspan="2"><IMG src="~/images/spacer.gif" border="0" runat="server" height="6"></td>
																</tr>
															</table>
											
															<!-- Begin Background -->
															<table class="bgBoxInside" cellSpacing="0" cellPadding="0" border="0">
																<tr>
																	<td><IMG src="~/images/content/insideLeftCornerTop.gif" border="0" runat="server"></td>
																	<td class="insideTopBoxContent" width="100%"></td>
																	<td align="right"><IMG src="~/images/content/insideRightCornerTop.gif" border="0" runat="server"></td>
																</tr>
																<tr>
																	<td class="insideLeftBoxContent"></td>
																	<td>
																		<table cellSpacing="0" cellPadding="0" width="690" border="0">
																			<tr>
																				<td width="6"></td>
																				<td class="channelStyle" align="left" height="23">Background (Optional):</td>
																				<td class="channelStyle"></td>
																				<td width="5"></td>										
																			</tr>
																		</table>
																	</td>
																	<td class="insideRightBoxContent"></td>
																</tr>
																<tr>
																	<td class="insideLeftBoxContent"></td>
																	<td>
																		<table cellSpacing="0" cellPadding="0" border="0">
																			<tr>
																				<td vAlign="top">
																					<table cellSpacing="0" cellPadding="0" width="690" border="0">
																						<tr>
																							<td><IMG src="~/images/content/leftTopCornerGray.gif" border="0" runat="server"></td>
																							<td class="insideTopBoxGrayContent" width="100%"></td>
																							<td align="right"><IMG src="~/images/content/rightTopCornerGray.gif" border="0" runat="server"></td>
																						</tr>
																						<tr>
																							<td class="insideLeftBoxGrayContent"></td>
																							<td class="bgBoxInsideChannel" width="100%">
																								<table cellSpacing="0" cellPadding="4" border="0">
																									<tr id="bgtr1">
																										<td><asp:hyperlink class="colorPicker" id="txtBackgroundColorAppClrPickerStr" Runat="server" ImageUrl="..\images\colorpickmask.gif"></asp:hyperlink></td>
																										<td vAlign="middle" align="left"><asp:textbox class="textBoxInput" id="txtBackgroundColor" runat="server" MaxLength="8"></asp:textbox></td>
																										<td class="textBodyChannel">Background Color</td>
																									</tr>
																									<tr>
																										<td>&nbsp;</td>
																										<td><asp:textbox class="textBoxInput" id="txtBGPicture" style="width:300px" runat="server" MaxLength="255"></asp:textbox></td>
																										<td class="textBodyChannel">Background Photo URL
																											&nbsp;&nbsp;<A href="javascript:void(0);" runat="server" id="aRemove">Clear Photo</A>
																											&nbsp;&nbsp;<A HREF='#' onclick="javascript:window.open('./selectPhoto.aspx?tbName=txtBGPicture','add','toolbar=no,width=500,height=400,menubar=no,scrollbars=yes').focus();return false;">Select from Library...</A>
																										</td>
																									</tr>
																									<tr id="bgtr2" >
																										<td>&nbsp;</td>
																										<td><asp:dropdownlist class="textBoxInput" id="drpRepeat" style="width:150px" runat="Server"></asp:dropdownlist></td>
																										<td class="textBodyChannel">Background Photo options</td>
																									</tr>
																								</table>
																							</td>
																							<td class="insideRightBoxGrayContent"></td>
																						</tr>
																						<tr>
																							<td><IMG src="~/images/content/leftBottomCornerGray.gif" border="0" runat="server"></td>
																							<td class="insideBottomBoxGrayContent" width="100%"></td>
																							<td align="right"><IMG src="~/images/content/rightBottomCornerGray.gif" border="0" runat="server"></td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																			<tr>
																				<td align="left"></td>
																			</tr>
																		</table>
																	</td>
																	<td class="insideRightBoxContent"></td>
																</tr>
																<tr>
																	<td class="insideLeftBoxContent"></td>
																	<td height="5"><IMG src="~/images/spacer.gif" border="0" runat="server" height="5"></td>
																	<td class="insideRightBoxContent"></td>
																</tr>
																<tr>
																	<td><IMG src="~/images/content/insideLeftCornerBottom.gif" border="0" runat="server"></td>
																	<td class="insideBottomBoxContent" width="100%"></td>
																	<td align="right"><IMG src="~/images/content/insideRightCornerBottom.gif" border="0" runat="server"></td>
																</tr>
															</table>
															<!-- End Background -->	
								
															<br />
															<a href="javascript:void(0);" onclick="showAdvanced()" id="advancedLink" style="font-size: 12px;">Show more options</a> 
															<a href="javascript:void(0);" onclick="showLess()" id="lessLink" style="display:none;font-size: 12px;">Show less options</a>
															<br /><br />										
											
															<!-- Begin More Options -->
															<table class="bgBoxInside" cellSpacing="0" cellPadding="0" width="690" border="0" id="tblMoreOptions" style="display:none;">
																<tr>
																	<td><IMG src="~/images/content/insideLeftCornerTop.gif" border="0" runat="server"></td>
																	<td class="insideTopBoxContent" width="100%"></td>
																	<td align="right"><IMG src="~/images/content/insideRightCornerTop.gif" border="0" runat="server"></td>
																</tr>
																<tr>
																	<td class="insideLeftBoxContent"></td>
																	<td>
																		<table cellSpacing="0" cellPadding="0" border="0">
																			<tr>
																				<td vAlign="top">
																				
																					<!-- ############### Widget Style ############## -->
																					<table cellSpacing="0" cellPadding="0" width="690" border="0" id="widgetStyle" >
																						<tr>
																							<td colspan="3" align="left">
																								<table cellSpacing="0" cellPadding="0" border="0">
																									<tr>
																										<td width="6"></td>
																										<td class="channelStyle" align="left" height="23">Widget Style:</td>
																										<td class="channelStyle"></td>
																										<td width="5"></td>										
																									</tr>
																								</table>
																							</td>
																						</tr>
																						<tr>
																							<td><IMG src="~/images/content/leftTopCornerGray.gif" border="0" runat="server"></td>
																							<td class="insideTopBoxGrayContent" width="100%"></td>
																							<td align="right"><IMG src="~/images/content/rightTopCornerGray.gif" border="0" runat="server"></td>
																						</tr>
																						<tr>
																							<td class="insideLeftBoxGrayContent"></td>
																							<td class="bgBoxInsideChannel" width="100%">
																								<table style="width:100%" cellSpacing="0" cellPadding="4" border="0" id="widgetStyle">
																									<tr>
																										<td width="30"><asp:hyperlink class="colorPicker" id="txtModuleHeaderColorAppClrPickerStr" Runat="server" ImageUrl="..\images\colorpickmask.gif"></asp:hyperlink></td>
																										<td><asp:textbox class="textBoxInput" id="txtModuleHeaderColor" runat="server" MaxLength="8"></asp:textbox></td>
																										<td class="textBodyChannel" align="left" width="300">Header Color</td>
																									</tr>
																									<tr>
																										<td><asp:hyperlink class="colorPicker" id="txtModuleBgColorAppClrPickerStr" Runat="server" ImageUrl="..\images\colorpickmask.gif"></asp:hyperlink></td>
																										<td><asp:textbox class="textBoxInput" id="txtModuleBgColor" runat="server" MaxLength="8"></asp:textbox></td>
																										<td class="textBodyChannel" align="left" width="300">Background Color</td>
																									</tr>
																									<tr>
																										<td>&nbsp;</td>
																										<td><asp:textbox class="textBoxInput" id="txtModulePicture" runat="server" MaxLength="255"></asp:textbox>
																										<td class="textBodyChannel">Background Photo URL
																											&nbsp;&nbsp;<A href="javascript:void(0);" runat="server" id="aWidRemove">Clear Photo</A>
																											&nbsp;&nbsp;<A HREF='#' onclick="javascript:window.open('./selectPhoto.aspx?tbName=txtModulePicture','add','toolbar=no,width=500,height=400,menubar=no,scrollbars=yes').focus();return false;">Select from Library...</A>
																										</td>
																									</tr>
																									<tr>
																										<td><asp:hyperlink class="colorPicker" id="txtModuleBorColorAppClrPickerStr" Runat="server" ImageUrl="..\images\colorpickmask.gif"></asp:hyperlink></td>
																										<td><asp:textbox class="textBoxInput" id="txtModuleBorColor" runat="server" MaxLength="8"></asp:textbox></td>
																										<td class="textBodyChannel">Border Color</td>
																									</tr>
																								</table>
																							<td class="insideRightBoxGrayContent"></td>
																						</tr>
																						<tr>
																							<td><IMG src="~/images/content/leftBottomCornerGray.gif" border="0" runat="server"></td>
																							<td class="insideBottomBoxGrayContent" width="100%"></td>
																							<td align="right"><IMG src="~/images/content/rightBottomCornerGray.gif" border="0" runat="server"></td>
																						</tr>
																					</table>
																					<br />
																					<!-- ############ Page Style ############ -->
																					<table cellSpacing="0" cellPadding="0" width="690" border="0" id="pageStyle" >
																						<tr>
																							<td colspan="3" align="left">
																								<table cellSpacing="0" cellPadding="0" border="0">
																									<tr>
																										<td width="6"></td>
																										<td class="channelStyle" align="left" height="23">Page Style:</td>
																										<td class="channelStyle"></td>
																										<td width="5"></td>										
																									</tr>
																								</table>
																							</td>
																						</tr>
																						<tr>
																							<td><IMG src="~/images/content/leftTopCornerGray.gif" border="0" runat="server"></td>
																							<td class="insideTopBoxGrayContent" width="100%"></td>
																							<td align="right"><IMG src="~/images/content/rightTopCornerGray.gif" border="0" runat="server"></td>
																						</tr>
																						<tr>
																							<td class="insideLeftBoxGrayContent"></td>
																							<td class="bgBoxInsideChannel" width="100%">
																								<table style="width:100%" cellSpacing="0" cellPadding="4" border="0">
																									<tr>
																										<td align="center" width="30"><asp:hyperlink class="colorPicker" id="txtFontColorAppClrPickerStr" Runat="server" ImageUrl="..\images\colorpickmask.gif"></asp:hyperlink></td>
																										<td align="left" width="142"><asp:textbox class="textBoxInput" id="txtFontColor" runat="server" MaxLength="8"></asp:textbox></td>
																										<td class="textBodyChannel" width="515">Font Color</td>
																									</tr>
																									<tr>
																										<td>&nbsp;</td>
																										<td><asp:dropdownlist class="textBoxInput" style="width:130px" id="drpGeneralFont" runat="Server"></asp:dropdownlist></td>
																										<td class="textBodyChannel">Font Type</td>
																									</tr>
																									<tr>
																										<td>&nbsp;</td>
																										<td><asp:dropdownlist class="textBoxInput" id="drpGeneralFontSize" runat="Server"></asp:dropdownlist></td>
																										<td class="textBodyChannel" align="left">Font Size</td>
																									</tr>
																									<tr>
																										<td align="center" width="30"><asp:hyperlink class="colorPicker" id="txtHyperlinkColorAppClrPickerStr" Runat="server" ImageUrl="..\images\colorpickmask.gif"></asp:hyperlink></td>
																										<td><asp:textbox class="textBoxInput" id="txtHyperlinkColor" runat="server" MaxLength="8"></asp:textbox></td>
																										<td class="textBodyChannel" align="left">Hyperlink Color</td>
																									</tr>
																									<tr>
																										<td align="center" width="30"><asp:hyperlink class="colorPicker" id="txtHyperlinkHoverColorAppClrPickerStr" Runat="server" ImageUrl="..\images\colorpickmask.gif"></asp:hyperlink></td>
																										<td align="left"><asp:textbox class="textBoxInput" id="txtHyperlinkHoverColor" runat="server" MaxLength="8"></asp:textbox></td>
																										<td class="textBodyChannel" align="left">Hyperlink Mouseover Color</td>
																									</tr>
																									<tr align="center">
																										<td><asp:hyperlink class="colorPicker" id="txtHyperlinkVisitedColorAppClrPickerStr" Runat="server"
																												ImageUrl="..\images\colorpickmask.gif"></asp:hyperlink></td>
																										<td align="left"><asp:textbox class="textBoxInput" id="txtHyperlinkVisitedColor" runat="server" MaxLength="8"></asp:textbox></td>
																										<td class="textBodyChannel" align="left">Hyperlink Visited Color</td>
																									</tr>
																									<tr align="center">
																										<td></td>
																										<td align="left">
																											<asp:dropdownlist class="textBoxInput" id="drpOpacity" runat="Server">
																												<asp:ListItem  value="-1">change...</asp:ListItem>
																												<asp:ListItem  value="-1">none</asp:ListItem>
																												<asp:ListItem  value="90">Slight</asp:ListItem>
																												<asp:ListItem  value="75">Moderate</asp:ListItem>
																												<asp:ListItem  value="60">Heavy</asp:ListItem>
																												<asp:ListItem  value="40">Extreme</asp:ListItem>
																											</asp:dropdownlist>
																										</td>
																										<td class="textBodyChannel" align="left">Opacity</td>
																									</tr>
																									<tr align="center">
																										<td></td>
																										<td colspan="2" align="left">
																											<span class="textBodyChannel" align="left">Custom CSS</span><br/>
																											<asp:TextBox ID="txtCustomCSS" TextMode="multiline" Rows="15" style="width:600px" class="formKanevaText" runat="server"/>
																										</td>
																									</tr>
																								</table>
																							</td>
																							<td class="insideRightBoxGrayContent"></td>
																						</tr>
																						<tr>
																							<td><IMG src="~/images/content/leftBottomCornerGray.gif" border="0" runat="server"></td>
																							<td class="insideBottomBoxGrayContent" width="100%"></td>
																							<td align="right"><IMG src="~/images/content/rightBottomCornerGray.gif" border="0" runat="server"></td>
																						</tr>
																					</table>														
											
																				</td>
																			</tr>
																			<tr>
																				<td align="left"></td>
																			</tr>
																		</table>
																	</td>
																	<td class="insideRightBoxContent"></td>
																</tr>
																<tr>
																	<td class="insideLeftBoxContent"></td>
																	<td height="5"><IMG src="~/images/spacer.gif" border="0" runat="server" height="5"></td>
																	<td class="insideRightBoxContent"></td>
																</tr>
																<tr>
																	<td><IMG src="~/images/content/insideLeftCornerBottom.gif" border="0" runat="server"></td>
																	<td class="insideBottomBoxContent" width="100%"></td>
																	<td align="right"><IMG src="~/images/content/insideRightCornerBottom.gif" border="0" runat="server"></td>
																</tr>
															</table>								
															<!-- End More Options -->									

														</td>
														<td width="5">&nbsp;</td>
													</tr>
												</table>
											</td>
											<td class="insiderightBoxContent"></td>
										</tr>
										<tr>
											<td class="insideleftBoxContent"></td>
											<td height="5"><IMG src="~/images/spacer.gif" border="0" runat="server" height="5"></td>
											<td class="insiderightBoxContent"></td>
										</tr>
										<tr>
											<td align="left" width="5"><IMG src="~/images/content/leftCornerBottom.gif" border="0" runat="server"></td>
											<td class="bottomBoxContent" width="100%"></td>
											<td align="right"><IMG src="~/images/content/rightCornerBottom.gif" border="0" runat="server"></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td height="5" colspan="3"><IMG id="IMG1" src="~/images/spacer.gif" border="0" runat="server" height="5"></td>
							</tr>
						</table>

					<td class="frBorderRight">
						<img id="Img5" height="1" runat="server" src="~/images/spacer.gif" width="1">
					</td>
				</tr>
				<tr>
					<td class="frBottomLeft"></td>
					<td class="frBottom"></td>
					<td class="frBottomRight"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
