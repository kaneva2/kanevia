///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva.channel;
using KlausEnt.KEP.Kaneva.mykaneva;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for mediaFavorites.
	/// </summary>
	public class mediaFavorites : StoreBasePage
	{
		protected mediaFavorites () 
		{
			Title = "Connected Media";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			if(Request.IsAuthenticated)
			{
				if(!GetRequestParams())
				{
					RedirectToHomePage ();
				}
				else
				{
					//check if user is allowed to edit this channel
					if(!IsUserAllowedToEdit())
					{
						RedirectToHomePage ();
					}
				}
			}
			else
			{
				//must log in to do this
				Response.Redirect (this.GetLoginURL());
			}

			Response.Expires = 0;
			Response.CacheControl = "no-cache";
			

			if (!IsPostBack)
			{
				BindLinks();

				AddTag ();
			}

			// Set nav
            HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
			HeaderNav.SetNavVisible(HeaderNav.MyKanevaNav,2);
			HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.MEDIA_UPLOADS;
			HeaderNav.MyMediaUploadsNav.ActiveTab = NavMediaUploads.TAB.LIBRARY;
			HeaderNav.MyMediaUploadsNav.CommunityId = _channelId;
			HeaderNav.MyMediaUploadsNav.UserId = _userId;
				
			HeaderNav.SetNavVisible(HeaderNav.MyMediaUploadsNav);


			
			// Add the javascript for deleting Assets from the cart
			string scriptString = "<script language=\"javaScript\">\n<!--\n function DeleteAssetTag (id) {\n";
			scriptString += "	document.all.hidAssetId.value = id;\n";			// save the asset number clicked to the hidden field
			scriptString += "	" + ClientScript.GetPostBackEventReference (this.btnDeleteAssetTag, "") + ";\n";
			scriptString += "}\n// -->\n";
			scriptString += "</script>";

            if (!ClientScript.IsClientScriptBlockRegistered(GetType (), "DeleteBM"))
			{
                ClientScript.RegisterClientScriptBlock(GetType(), "DeleteBM", scriptString);
			}

			this.btnDeleteSelected.Attributes.Add("onclick","javascript: return confirm(\"Are you sure you want to remove selected items from your connected media?\");");

		}

		private bool GetRequestParams()
		{
			bool retVal = true;
			try
			{
				if (IsAdministrator ())
				{
					if (Request ["userId"] != null)
					{
						_userId = Convert.ToInt32 (Request ["userId"]);
					}
					_userId = (_userId > 0) ? _userId : this.GetUserId ();
					_channelId = CommunityUtility.GetPersonalChannelId (_userId);
				}
				else
				{
					_userId = this.GetUserId ();
					_channelId = this.GetPersonalChannelId ();
				}
			}
			catch(Exception)
			{
				//invalid numbers
				retVal = false;
			}
			return retVal;
		}

		/// <summary>
		/// returns true if current user is allowed to edit this channel
		/// </summary>
		/// <returns></returns>
		private bool IsUserAllowedToEdit()
		{
			return IsAdministrator() || _userId == this.GetUserId();
		}

		/// <summary>
		/// bind filter links and item nubmer count
		/// </summary>
		private void BindLinks()
		{
			// Get the media counts
            DataRow drMediaCount = StoreUtility.GetChannelMediaCounts(_channelId);

			lblAllCount.Text = drMediaCount ["sum_total"].ToString ();
			lblVideoCount.Text = drMediaCount ["sum_video"].ToString ();
			lblPhotoCount.Text = drMediaCount ["sum_picture"].ToString ();
			lblMusicCount.Text =  drMediaCount ["sum_music"].ToString ();
			lblGamesCount.Text = drMediaCount ["sum_game"].ToString ();

			// Get the connected media count
			lblConnectedMediaCount.Text = StoreUtility.GetConnectedMediaCount (_channelId).ToString ();

			hlAll.NavigateUrl = "../asset/publishedItemsNew.aspx?communityId=" + _channelId;
			hlVideo.NavigateUrl = hlAll.NavigateUrl + "&assetTypeId=" + (int) Constants.eASSET_TYPE.VIDEO;
			hlPhoto.NavigateUrl = hlAll.NavigateUrl + "&assetTypeId=" + (int) Constants.eASSET_TYPE.PICTURE;
			hlMusic.NavigateUrl = hlAll.NavigateUrl + "&assetTypeId=" + (int) Constants.eASSET_TYPE.MUSIC;
			hlGames.NavigateUrl = hlAll.NavigateUrl + "&assetTypeId=" + (int) Constants.eASSET_TYPE.GAME;
			hlConnectedMedia.NavigateUrl = "../mykaneva/mediaFavorites.aspx?userId=" + this._userId;
			hlConnectedMedia.Style.Add("color","red");
		}

		/// <summary>
		/// Add the tag to the tag cart
		/// </summary>
		private void AddTag ()
		{
			if (Request ["assetId"] != null)
			{
				int assetId = Convert.ToInt32 (Request ["assetId"]);

				StoreUtility.AddTag( _userId, _channelId, assetId, Request.UserHostAddress );
			}

			// Show the items in the cart
			BindData (1, "");
		}

		/// <summary>
		/// Bind the cart reapeter
		/// </summary>
		private void BindData (int pageNumber, string filter)
		{
			BindLinks();

			PagedDataTable dtOrderItems = StoreUtility.GetConnectedMedia (_channelId, _userId, "ac.created_date DESC", pageNumber, filStore.NumberOfPages);
			rptItems.DataSource = dtOrderItems;
			rptItems.DataBind ();

			// Are there items in the cart?
			if (dtOrderItems.Rows.Count == 0)
			{
				lblCartMessage.Text = "There are currently no items tagged.";
			}
			else
			{
				lblCartMessage.Text = GetResultsText (dtOrderItems.TotalCount, pageNumber, filStore.NumberOfPages, dtOrderItems.Rows.Count);
			}

			pgTop.NumberOfPages = Math.Ceiling ((double) dtOrderItems.TotalCount / filStore.NumberOfPages).ToString ();
			pgTop.DrawControl ();
		}

		/// <summary>
		/// Return the delete javascript
		/// </summary>
		/// <param name="TopicID"></param>
		/// <returns></returns>
		public string GetDeleteTagScript (int assetId)
		{
			return "javascript:if (confirm(\"Are you sure you want to remove it from you connected media?\")){DeleteAssetTag (" + assetId + ")};";
		}

		/// <summary>
		/// Delete an Asset from the cart
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void Delete_AssetTag (Object sender, EventArgs e)
		{
			//also remove it from personal channel
			StoreUtility.RemoveAssetFromChannel(_userId, Convert.ToInt32 (hidAssetId.Value), _channelId);
			pg_PageChange (this, new PageChangeEventArgs (pgTop.CurrentPageNumber));
		}

		private void pg_PageChange (object sender, PageChangeEventArgs e)
		{
			BindData (e.PageNumber, filStore.CurrentFilter);
		}

		private void filStore_FilterChanged(object sender, FilterChangedEventArgs e)
		{
			pgTop.CurrentPageNumber = 1;
			BindData (1, e.Filter);
		}

		private void btnDeleteSelected_Click(object sender, EventArgs e)
		{
			CheckBox chkEdit;
			HtmlInputHidden hidAssetId;

			foreach (RepeaterItem dgiAsset in rptItems.Items)
			{
				chkEdit = (CheckBox) dgiAsset.FindControl ("chkEdit");

				if (chkEdit.Checked)
				{
					hidAssetId = (HtmlInputHidden) dgiAsset.FindControl ("hidAssetItemId");
					//also remove it from personal channel
					StoreUtility.RemoveAssetFromChannel(_userId, Convert.ToInt32 (hidAssetId.Value), _channelId);
				}
			}
			pg_PageChange (this, new PageChangeEventArgs (pgTop.CurrentPageNumber));
		}

		protected Kaneva.Pager pgTop;
		protected Kaneva.StoreFilter filStore;

		protected Label lblHeading;
		protected Label lblCartMessage;
		protected PlaceHolder phBreadCrumb;
		protected Button btnDeleteAssetTag;

		// Cart
		protected Repeater rptItems;
		protected System.Web.UI.HtmlControls.HtmlInputHidden hidAssetId;

		protected HyperLink hlAll;
		protected HyperLink hlVideo;
		protected HyperLink hlPhoto;
		protected HyperLink hlMusic;
		protected HyperLink hlGames;
		protected HyperLink hlConnectedMedia;

		protected Label lblAllCount;
		protected Label lblVideoCount;
		protected Label lblPhotoCount;
		protected Label lblMusicCount;
		protected Label lblGamesCount;
		protected Label lblConnectedMediaCount;
		protected HtmlContainerControl pnlConnectedMedia;

		protected DropDownList drpActions;
		protected Button btnDeleteSelected;

		private int _channelId;
		private int _userId;

		#region Web Form Designer generated code

		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
			pgTop.PageChanged +=new PageChangeEventHandler (pg_PageChange);
			filStore.FilterChanged	+=new FilterChangedEventHandler(filStore_FilterChanged);
			btnDeleteSelected.Click +=new EventHandler(btnDeleteSelected_Click);
		}

		#endregion
	}
}
