///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Collections.Generic;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using log4net;

namespace KlausEnt.KEP.Kaneva.mykaneva
{
    public partial class friendsGroups : SortedBasePage
    {
        protected void Page_Load (object sender, EventArgs e)
        {
            if (!Request.IsAuthenticated)
            {
                Response.Redirect (this.GetLoginURL ());
            }

            if (!IsPostBack)
            {
                CurrentSort = "username";
                storeFilter.AlphaNumericCurrentSort = "display_name";

                BindData (1, KanevaWebGlobals.CurrentUser.UserId, 0);

                GetFriendGroups (KanevaWebGlobals.CurrentUser.UserId);
                lblGroupName.Text = "All"; 
            }

            // Set Nav
            HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
            HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.FRIENDS;
            //HeaderNav.MyFriendNav.ActiveTab = NavFriend.TAB.GROUPS;
            HeaderNav.SetNavVisible (HeaderNav.MyKanevaNav, 2);
            //HeaderNav.SetNavVisible (HeaderNav.MyFriendNav);
        }

        /// <summary>
        /// Bind the data
        /// </summary>
        /// <param name="pageNumber"></param>
        private void BindData (int pageNumber, int userId, int friendGroupId)
        {
            BindData ( pageNumber, userId, friendGroupId, 0);
        }
        private void BindData (int pageNumber, int userId, int friendGroupId, int friendId)
        {
            searchFilter.CurrentPage = "friends";
            this.CurrentPageNumber = pageNumber;

            string orderby = CurrentSort + " " + CurrentSortOrder;
            int pgSize = searchFilter.NumberOfPages;
            
            string friendFilter = CURRENT_FILTERING;

            UserFacade userFacade = new UserFacade ();
            PagedList<Friend> friends;

            if (friendGroupId == 0)
            {
                friends = userFacade.GetFriends (userId, friendId, friendFilter, orderby, pageNumber, pgSize);
            }
            else
            {
                friends = userFacade.GetFriendsInGroup (friendGroupId, userId, friendFilter, orderby, pageNumber, pgSize);
            }
            
            // set this property, it is needed on the web page
            FriendGroupId = friendGroupId;

            rptFriends.DataSource = friends;        
            rptFriends.DataBind ();

            pgTop.NumberOfPages = Math.Ceiling ((double) friends.TotalCount / pgSize).ToString ();
            pgTop.DrawControl ();
            pgBottom.NumberOfPages = Math.Ceiling ((double) friends.TotalCount / pgSize).ToString ();
            pgBottom.DrawControl ();
                                    
            // The results
            lblResults_Bottom.Text = lblResults_Top.Text = GetResultsText (friends.TotalCount, pageNumber, pgSize, friends.Count);

            if (FriendGroupId > 0)
            {
                lbnDeleteFriend.Style.Add ("display", "none");
                lbnRemoveFromGroup.Style.Add ("display", "inline");
                spnFriendsToGroup.Style.Add ("display", "none");
                lbnDeleteFriendGroup.Style.Add ("display", "inline");
                
                btnSearch.Style.Add ("display", "none");
                btnAddToGroup.Style.Add ("display", "inline");

                h4SearchLabel.InnerText = "Add to Group:";
            }
            else
            {
                lbnDeleteFriend.Style.Add ("display", "inline");
                lbnRemoveFromGroup.Style.Add ("display", "none");
                spnFriendsToGroup.Style.Add ("display", "inline");
                lbnDeleteFriendGroup.Style.Add ("display", "none");

                btnSearch.Style.Add ("display", "inline");
                btnAddToGroup.Style.Add ("display", "none");

                h4SearchLabel.InnerText = "Search:";
            }

            // Do we need to show connect to FB Info?
            fbConnect.Visible = false;
            btnInvite.Visible = true;
            aInvite.Visible = true;
            if (KanevaWebGlobals.CurrentUser.FacebookSettings.FacebookUserId == 0)
            {
                fbConnect.Visible = true;
                btnInvite.Visible = false;
                aInvite.Visible = false;
            }
        }


        #region Helper Methods

        /// <summary>
        /// GetFriendGroups
        /// </summary>
        private void GetFriendGroups (int userId)
        {
            PagedDataTable pdtGroups = UsersUtility.GetFriendGroups (userId, "", "name", 1, Int32.MaxValue);
            PagedDataTable pdtGroupsForDropDown = pdtGroups;

            DataRow drAll = pdtGroups.NewRow ();
            drAll["friend_group_id"] = "0";
            drAll["name"] = "All";
            drAll["friend_group_id"] = "0";
            drAll["friend_count"] = UsersUtility.GetFriends(KanevaWebGlobals.CurrentUser.UserId, "", 1, Int32.MaxValue, "", false, false, false, false).TotalCount;  //KanevaWebGlobals.CurrentUser.Stats.NumberOfFriends; 

            pdtGroups.Rows.InsertAt (drAll, 0);
            rptFriendGroups.DataSource = pdtGroups;
            rptFriendGroups.DataBind ();
            
            
            // add to drop down before adding all group
            DataRow drAddToGroup = pdtGroupsForDropDown.NewRow ();
            drAddToGroup["friend_group_id"] = "0";
            drAddToGroup["name"] = "Add to Group:";
            drAddToGroup["friend_group_id"] = "0";
            drAddToGroup["friend_count"] = "0";

            pdtGroupsForDropDown.Rows.InsertAt (drAddToGroup, 0);
            
            ddlGroupNames.DataTextField = "name";
            ddlGroupNames.DataValueField = "friend_group_id";
            ddlGroupNames.DataSource = pdtGroupsForDropDown;
            ddlGroupNames.DataBind ();
        }

        /// <summary>
        /// GetOnlineText -- override method on BasePage
        /// </summary>
        protected new string GetOnlineText (string ustate)
        {
            //default to no status if error occurs
            try
            {
                string strUserState = KanevaWebGlobals.GetUserState (ustate);
                if (strUserState.Length > 0)
                {
                    return "<span class=\"online\">" + strUserState + "</span>";
                }
                else
                {
                    return "";
                }
            }
            catch (InvalidCastException)
            {
                return "";
            }
        }

        /// <summary>
        /// RemoveFriend
        /// </summary>
        protected void RemoveFriend (int userId)
        {
            UserFacade userFacade = new UserFacade();

            if (FriendGroupId > 0)  // remove from friend group
            {
                UsersUtility.RemoveFriendFromGroup (KanevaWebGlobals.CurrentUser.UserId, FriendGroupId, userId);
            }
            else // remove from friends list
            {
                // remove from friends
                userFacade.DeleteFriend (KanevaWebGlobals.CurrentUser.UserId, userId);
                // remove from all friend groups
                UsersUtility.RemoveFriendFromAllGroups (KanevaWebGlobals.CurrentUser.UserId, userId);
            }
        }

        /// <summary>
        /// ShowMessage
        /// </summary>
        protected void ShowMessage (bool isErr, string msg)
        {
            messages.ForeColor = isErr ? System.Drawing.Color.DarkRed : System.Drawing.Color.DarkGreen;
            messages.Text = msg; 
            MagicAjax.AjaxCallHelper.Write ("ShowConfirmMsg('messages', 5000);");
        }

        #endregion


        #region Event Handlers

        /// <summary>
        /// ItemDataBound event for Friend Groups repeater
        /// </summary>
        protected void rptFriendGroups_ItemDataBound (object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                string friendGroupId = DataBinder.Eval (e.Item.DataItem, "friend_group_id").ToString ();

                LinkButton lbFriendGroup = (LinkButton) e.Item.FindControl ("lbFriendGroup");
                lbFriendGroup.CommandArgument = friendGroupId;
                lbFriendGroup.CommandName = "view";
                lbFriendGroup.ToolTip = "View friends in the " + DataBinder.Eval (e.Item.DataItem, "name").ToString () + " group";

                HtmlContainerControl liFriendGroup = (HtmlContainerControl) e.Item.FindControl ("liFriendGroup");
                liFriendGroup.Attributes.Add ("onclick", "javascript:$('hidGroupId').value='" + friendGroupId + "';");

                if (liFriendGroup != null && 
                    Convert.ToInt32(DataBinder.Eval (e.Item.DataItem, "friend_group_id")) == this.FriendGroupId)
                {
                    liFriendGroup.Attributes.Add ("class", "friendgroup selected");
                }
            }
        }

        /// <summary>
        /// ItemCommand event for Friend Groups repeater
        /// </summary>
        protected void rptFriendGroups_ItemCommand (object sender, RepeaterCommandEventArgs e)
        {
            string command = e.CommandName;
            string cmdarg = e.CommandArgument.ToString ();

            CurrentPageNumber = 1;
            pgTop.CurrentPageNumber = 1;
            pgBottom.CurrentPageNumber = 1;

            FriendGroupId = Convert.ToInt32 (e.CommandArgument);

            Repeater rptFriendGroups = (Repeater) e.Item.Parent;
            
            // Clear selected class from all repeater items
            if (rptFriendGroups != null)
            {
                for (int i = 0; i < rptFriendGroups.Items.Count; i++)
                {
                    if (rptFriendGroups.Items[i].ItemType == ListItemType.Item || rptFriendGroups.Items[i].ItemType == ListItemType.AlternatingItem)
                    {
                        HtmlContainerControl liFriendGroup = (HtmlContainerControl) rptFriendGroups.Items[i].FindControl ("liFriendGroup");

                        liFriendGroup.Attributes.Add ("class", "friendgroup");
                    }
                }
            }

            HtmlContainerControl liSelectedFriendGroup = (HtmlContainerControl) e.Item.FindControl ("liFriendGroup");
            if (liSelectedFriendGroup != null)
            {
                liSelectedFriendGroup.Attributes.Add ("class", "friendgroup selected");
                lblGroupName.Text = ((LinkButton) e.Item.FindControl ("lbFriendGroup")).Attributes["name"];
            }

            SearchString = "";
            BindData (1, KanevaWebGlobals.CurrentUser.UserId, FriendGroupId, 0);
        }

        /// <summary>
        /// Click event for Search button
        /// </summary>
        protected void btnSearch_Click (object sender, EventArgs e)
        {
            if (KanevaWebGlobals.ContainsInjectScripts (hidFriendName.Value) ||
                KanevaWebGlobals.ContainsInjectScripts (hidFriendId.Value))
            {
                m_logger.Warn ("User " + KanevaWebGlobals.GetUserId () + " tried to script from IP " + Common.GetVisitorIPAddress());
                ShowErrorOnStartup ("Your input contains invalid scripting, please remove script code and try again.", true);
                return;
            }

            SearchString = "";

            int friendUserId = 0;
            try
            {
                if (hidFriendId.Value.Length > 0)
                {
                    friendUserId = Convert.ToInt32 (hidFriendId.Value);
                }
            }
            catch { }

            //search for items
            BindData (1, KanevaWebGlobals.CurrentUser.UserId, 0, friendUserId);
        }

        /// <summary>
        /// Click event for Delete Friend Group linkbutton
        /// </summary>
        protected void lbnDeleteFriendGroup_Click (object sender, EventArgs e)
        {
            try
            {
                if (FriendGroupId > 0)  // remove friend group
                {
                    if (UsersUtility.DeleteFriendGroup (KanevaWebGlobals.CurrentUser.UserId, FriendGroupId) > 0)
                    {
                        FriendGroupId = 0;
                        BindData (pgTop.CurrentPageNumber, KanevaWebGlobals.CurrentUser.UserId, FriendGroupId);
                        //rebind for counts
                        GetFriendGroups (KanevaWebGlobals.CurrentUser.UserId);
                        lblGroupName.Text = "All";

                        ShowMessage (false, "Friend group deleted.");
                    }
                    else
                    {
                        ShowMessage (true, "Friend group was not deleted.");
                    }
                }
            }
            catch (Exception ex)
            {
                m_logger.Error ("Error trying to delete friend group : ", ex);
                ShowMessage (true, "Friend group was not deleted.");
            }
        }

        /// <summary>
        /// Click event for Pager
        /// </summary>
        protected void pg_PageChange (object sender, PageChangeEventArgs e)
        {
            pgTop.CurrentPageNumber = e.PageNumber;
            pgTop.DrawControl ();
            pgBottom.CurrentPageNumber = e.PageNumber;
            pgBottom.DrawControl ();
            BindData (e.PageNumber, KanevaWebGlobals.CurrentUser.UserId, FriendGroupId);
        }

        /// <summary>
        /// FilterChanged event for the store filter
        /// </summary>
        protected void storeFilter_FilterChanged (object sender, FilterChangedEventArgs e)
        {
            pgTop.CurrentPageNumber = 1;
            pgBottom.CurrentPageNumber = 1;
            CURRENT_FILTERING = e.Filter;
            BindData (1, KanevaWebGlobals.CurrentUser.UserId, FriendGroupId);
        }

        /// <summary>
        /// FilterChanged event for the search filter
        /// </summary>
        protected void searchFilter_FilterChanged (object sender, FilterChangedEventArgs e)
        {
            pgTop.CurrentPageNumber = 1;
            pgBottom.CurrentPageNumber = 1;
            BindData (1, KanevaWebGlobals.CurrentUser.UserId, FriendGroupId);
        }

        /// <summary>
        /// Click event for add new friend group button
        /// </summary>
        protected void btnSaveGroup_Click (object sender, EventArgs e)
        {
            try
            {
                int ret = 0;
                string groupName = txtNewGroup.Text.Trim ();

                if (groupName.Length > 0)
                {
                    if (KanevaWebGlobals.ContainsInjectScripts (groupName))
                    {
                        m_logger.Warn ("User " + KanevaWebGlobals.CurrentUser.UserId + " tried to script from IP " + Common.GetVisitorIPAddress());
                    }
                    else
                    {
                        ret = UsersUtility.InsertFriendGroup (KanevaWebGlobals.CurrentUser.UserId, Server.HtmlEncode (groupName));
                        txtNewGroup.Text = "";

                        if (ret != 1)
                        {
                            ShowMessage (false, "An error was encountered while trying to add Friend Group.");
                        }
                        else
                        {
                            ShowMessage (false, "Successfully added Friend Group '" + groupName + "'.");
                        }
                    }

                    GetFriendGroups (KanevaWebGlobals.CurrentUser.UserId);
                    BindData (CurrentPageNumber, KanevaWebGlobals.CurrentUser.UserId, 0);

                //    Repeater rptFriendGroups = (Repeater) e.Item.Parent;

                    
                    // Clear selected class from all repeater items
                    if (rptFriendGroups != null && rptFriendGroups.Items.Count > 0)
                    {
                        for (int i = 0; i < rptFriendGroups.Items.Count; i++)
                        {
                            if (rptFriendGroups.Items[i].ItemType == ListItemType.Item || rptFriendGroups.Items[i].ItemType == ListItemType.AlternatingItem)
                            {
                                HtmlContainerControl liFriendGroup = (HtmlContainerControl) rptFriendGroups.Items[i].FindControl ("liFriendGroup");

                                if (i == 0)
                                {
                                    liFriendGroup.Attributes.Add ("class", "friendgroup selected");
                                }
                                else
                                {
                                    liFriendGroup.Attributes.Add ("class", "friendgroup");
                                }
                            }
                        }
                    }

                }
                else
                {
                    ShowMessage (true, "Please enter a Friend Group name.");
                }
            }
            catch (Exception ex)
            {
                m_logger.Error ("Error trying to add a friend group : ", ex);
                ShowMessage (true, "Friend group was not added.");
            }
        }

        /// <summary>
        /// Click event for Add Friends to Group linkbutton
        /// </summary>
        protected void btnAdd_Click (object sender, EventArgs e)
        {
            try
            {
                int friendId = Convert.ToInt32 (hidFriendId.Value);
                string friendName = hidFriendName.Value.Trim ();
                                 
                // Make sure input is okay
                if (KanevaWebGlobals.ContainsInjectScripts (friendName))
                {
                    m_logger.Warn ("User " + KanevaWebGlobals.GetUserId () + " tried to script from IP " + Common.GetVisitorIPAddress());
                }
                else
                {
                    // verify the userid sent is actually a friend
                    if (GetUserFacade.AreFriends(KanevaWebGlobals.CurrentUser.UserId, friendId))
                    {
                        // make sure friend does not already belong to this group
                        if (!UsersUtility.IsFriendInGroup (FriendGroupId, friendId))
                        {
                            // add user to friend group
                            UsersUtility.InsertFriendInGroup (KanevaWebGlobals.CurrentUser.UserId, FriendGroupId, friendId);

                            GetFriendGroups (KanevaWebGlobals.CurrentUser.UserId);
                            BindData (CurrentPageNumber, KanevaWebGlobals.CurrentUser.UserId, FriendGroupId);

                            ShowMessage (false, friendName + " added to friend group.");
                        }
                        else
                        {
                            ShowMessage (true, "User already belongs to friend group.");
                        }
                    }
                    else
                    {
                        ShowMessage (true, "User does not belong to your friends list.");
                    }
                }
            }
            catch (Exception ex)
            {
                m_logger.Error ("Error trying to add user to friend group : ", ex);
                ShowMessage (true, "User was not added to friend group.");
            }
        }

        /// <summary>
        /// Click event for Delete Friend linkbutton
        /// </summary>
        protected void lbnDeleteFriend_Click (object sender, EventArgs e)
        {
            try
            {
                CheckBox chkEdit;
                HtmlInputHidden hidUserId;
                int deletionCount = 0;
                int friendId = 0;
                FriendGroupId = 0;

                foreach (RepeaterItem friend in rptFriends.Items)
                {
                    chkEdit = (CheckBox) friend.FindControl ("chkEdit");

                    if (chkEdit.Checked)
                    {
                        friendId = 0;

                        hidUserId = (HtmlInputHidden) friend.FindControl ("hidUserId");
                        friendId = Convert.ToInt32 (hidUserId.Value);

                        if (friendId > 0)
                        {
                            RemoveFriend (Convert.ToInt32 (hidUserId.Value));
                            deletionCount++;
                        }
                    }
                }

                //message to user
                if (deletionCount == 0)
                {
                    ShowMessage (true, "No friends were selected.");
                }
                else
                {
                    BindData (pgTop.CurrentPageNumber, KanevaWebGlobals.CurrentUser.UserId, FriendGroupId);
                    //rebind for counts
                    GetFriendGroups (KanevaWebGlobals.CurrentUser.UserId);

                    ShowMessage (false, deletionCount + " user" + (deletionCount > 1 ? "s were " : " was ") + "deleted from your friends list.");
                }
            }
            catch (Exception ex)
            {
                m_logger.Error ("Error trying to delete a friend : ", ex);
                ShowMessage (true, "User was not deleted from friends list.");
            }
        }

        /// <summary>
        /// Click event for Add Friend to Group linkbutton
        /// </summary>
        protected void lbnFriendsToGroup_Click (object sender, EventArgs e)
        {
            try
            {
                CheckBox chkEdit;
                HtmlInputHidden hidUserId;
                int addCount = 0;
                int friendId = 0;

                int FriendGroupId = Convert.ToInt32 (ddlGroupNames.SelectedValue);

                if (FriendGroupId > 0)
                {
                    foreach (RepeaterItem friend in rptFriends.Items)
                    {
                        chkEdit = (CheckBox) friend.FindControl ("chkEdit");

                        if (chkEdit.Checked)
                        {
                            friendId = 0;

                            hidUserId = (HtmlInputHidden) friend.FindControl ("hidUserId");
                            friendId = Convert.ToInt32 (hidUserId.Value);

                            // verify the userid sent is actually a friend
                            if (GetUserFacade.AreFriends(KanevaWebGlobals.CurrentUser.UserId, friendId))
                            {
                                // make sure friend does not already belong to this group
                                if (!UsersUtility.IsFriendInGroup (FriendGroupId, friendId))
                                {
                                    // add user to friend group
                                    UsersUtility.InsertFriendInGroup (KanevaWebGlobals.CurrentUser.UserId, FriendGroupId, friendId);
                                    addCount++;
                                }
                            }
                        }
                    }
                    //message to user
                    if (addCount == 0)
                    {
                        ShowMessage (true, "No friends were added.");
                    }
                    else
                    {
                        FriendGroupId = 0;
                        BindData (pgTop.CurrentPageNumber, KanevaWebGlobals.CurrentUser.UserId, FriendGroupId);
                        //rebind for counts
                        GetFriendGroups (KanevaWebGlobals.CurrentUser.UserId);
                        ddlGroupNames.SelectedIndex = 0;

                        ShowMessage (false, addCount + " friend" + (addCount > 1 ? "s were " : " was ") + "added to friend group.");
                    }
                }
                else
                {
                    ShowMessage (true, "Please select a Friend Group.");
                }
            }
            catch (Exception ex)
            {
                m_logger.Error ("Error trying to add user to friend group : ", ex);
                ShowMessage (true, "User was not added to friend group.");
            }
        }

        /// <summary>
        /// Click event for Remove Friend from Group linkbutton
        /// </summary>
        protected void lbnRemoveFromGroup_Click (object sender, EventArgs e)
        {
            try
            {
                CheckBox chkEdit;
                HtmlInputHidden hidUserId;
                int deletionCount = 0;
                int friendId = 0;

                foreach (RepeaterItem friend in rptFriends.Items)
                {
                    chkEdit = (CheckBox) friend.FindControl ("chkEdit");

                    if (chkEdit.Checked)
                    {
                        friendId = 0;

                        hidUserId = (HtmlInputHidden) friend.FindControl ("hidUserId");
                        friendId = Convert.ToInt32 (hidUserId.Value);

                        if (friendId > 0)
                        {
                            RemoveFriend (Convert.ToInt32 (hidUserId.Value));
                            deletionCount++;
                        }
                    }
                }

                //message to user
                if (deletionCount == 0)
                {
                    ShowMessage (true, "No friends were selected.");
                }
                else
                {
                    BindData (pgTop.CurrentPageNumber, KanevaWebGlobals.CurrentUser.UserId, FriendGroupId);
                    //rebind for counts
                    GetFriendGroups (KanevaWebGlobals.CurrentUser.UserId);

                    string msg = deletionCount + " user" + (deletionCount > 1 ? "s were " : " was ") + "deleted from " +
                        (FriendGroupId > 1 ? "friend group." : "your friends.");
                    ShowMessage (false, msg);
                }
            }
            catch (Exception ex)
            {
                m_logger.Error ("Error trying to remove user from friend group : ", ex);
                ShowMessage (true, "User was not delete from friend group.");
            }
        }

        #endregion


        #region Properties

        /// <summary>
        /// categoryId
        /// </summary>
        protected int FriendGroupId
        {
            set
            {
                ViewState["FriendGroupId"] = value;
            }
            get
            {
                if (ViewState["FriendGroupId"] == null)
                {
                    return 0;   // All
                }
                else
                {
                    return Convert.ToInt32 (ViewState["FriendGroupId"]);
                }
            }
        }

        /// <summary>
        /// Current Page Number
        /// </summary>
        private int CurrentPageNumber
        {
            set
            {
                ViewState["CurrentPageNumber"] = value;
            }
            get
            {
                if (ViewState["CurrentPageNumber"] == null)
                {
                    return 1;   // All
                }
                else
                {
                    return Convert.ToInt32 (ViewState["CurrentPageNumber"]);
                }
            }
        }

        /// <summary>
        /// SearchString
        /// </summary>
        private string SearchString
        {
            set
            {
                ViewState["SearchString"] = value.Trim ();
            }
            get
            {
                if (ViewState["SearchString"] == null)
                {
                    return string.Empty; //cORDER_BY_DEFAULT;
                }
                else
                {
                    return ViewState["SearchString"].ToString ();
                }
            }
        }

        /// <summary>
        /// Current filtering
        /// </summary>
        protected string CURRENT_FILTERING
        {
            get
            {
                if (ViewState["CurrentFilter"] == null)
                {
                    return "";
                }
                else
                {
                    return ViewState["CurrentFilter"].ToString ();
                }
            }
            set
            {
                ViewState["CurrentFilter"] = value;
            }
        }

        #endregion


        #region Declerations

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

        #endregion
    }
}
