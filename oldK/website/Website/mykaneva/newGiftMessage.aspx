<%@ Page language="c#" Codebehind="newGiftMessage.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.newGiftMessage" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../usercontrols/Pager.ascx" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>
<%@ Register TagPrefix="CE" Namespace="CuteEditor" Assembly="CuteEditor" %>

<link href="../css/home.css" rel="stylesheet" type="text/css" />		
<link href="../css/kanevaSC.css" rel="stylesheet" type="text/css"/>
<link href="../css/friends.css" rel="stylesheet" type="text/css"/>
<link href="../css/kanevaBroadBand.css" rel="stylesheet" type="text/css" />	
<LINK href="../css/store.css" type="text/css" rel="stylesheet">
<LINK href="../css/kanevaText.css" type="text/css" rel="stylesheet">

<script language="javascript" src="../jscript/prototype.js"></script>
<script language="javascript" src="../jscript/dw_tooltip/dw_event.js" ></script>
<script language="javascript" src="../jscript/dw_tooltip/dw_viewport.js" ></script>
<script language="javascript" src="../jscript/dw_tooltip/dw_tooltip.js" ></script>
<script language="javascript" src="../jscript/rollover.js"></script>

<style>
div#tipDiv {width:300px;}
</style>

<script type="text/javascript"><!--

window.onload = ttINIT;

function ttINIT ()
{
	Tooltip.init();
}

//--> </script> 


<table cellpadding="0" cellspacing="0" border="0" style="width:100%">
	<tr>
		<td>
			<asp:ValidationSummary ShowMessageBox="False" ShowSummary="True" Class="formError" id="valSum" DisplayMode="BulletList" runat="server" HeaderText="Please correct the following errors:"/>
		</td>
	</tr>
</table>


<table width="990" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td colspan="3"><img runat="server" src="~/images/spacer.gif" width="1" height="14" /></td>
	</tr>
	<tr>
		<td valign="top">
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
				<td>
	<table border="0" cellpadding="0" cellspacing="0" width="990">
      <tr>
        <td valign="top"><table  border="0" cellpadding="0" cellspacing="0" width="990">
          <tr>
            <td class="frTopLeft"></td>
            <td class="frTop"></td>
            <td class="frTopRight"></td>
          </tr>
          <tr>
            <td bgcolor="#f1f1f2" class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1"/></td>
            <td valign="top" bgcolor="#f1f1f2" class="frBgIntMembers"><table  border="0" cellpadding="0" cellspacing="0" width="100%" class="boxInside">
              <tr>
                <td class="boxInsideTopLeft"><img runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
                <td class="boxInsideTop" ><img runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
                <td  class="boxInsideTopRight"><img runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
              </tr>
              <tr class="boxInside">
                <td class="boxInsideleft" ><img runat="server" src="~/images/spacer.gif" width="1" height="4" /></td>
                <td class="boxInsideContent"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td><img runat="server" src="~/images/spacer.gif" width="6" height="4"/></td>
                    <td><img runat="server" src="~/images/spacer.gif" width="6" height="4"/></td>
                    <td><img runat="server" src="~/images/spacer.gif" width="6" height="4"/></td>
                  </tr>
                  <tr>
                    <td><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
                    <td width="100%" class="textGeneralGray01" align="left"><img runat="server" src="~/images/spacer.gif" width="2" height="1"/>Give a Gift</td>
                    <td><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
                  </tr>
                  <tr>
                    <td><img runat="server" src="~/images/spacer.gif" width="6" height="4"/></td>
                    <td><img runat="server" src="~/images/spacer.gif" width="6" height="4"/></td>
                    <td><img runat="server" src="~/images/spacer.gif" width="6" height="4"/></td>
                  </tr>
                </table></td>
                <td class="boxInsideRight"><img runat="server" src="~/images/spacer.gif" width="1" height="4"/></td>
              </tr>
              <tr>
                <td class="boxInsideBottomLeft" ><img runat="server" src="~/images/spacer.gif" width="4" height="4" /></td>
                <td class="boxInsideBottom"><img runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
                <td class="boxInsideBottomRight"><img runat="server" src="~/images/spacer.gif" width="4" height="4" /></td>
              </tr>
            </table>
            <table  border="0" cellpadding="0" cellspacing="0" width="100%" >
                <tr>
                  <td class="boxInsideTopLeft"><img runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
                  <td class="boxInsideTop" ><img runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
                  <td  class="boxInsideTopRight"><img runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
                </tr>
                <tr class="boxInside">
                  <td class="boxInsideleft" ><img runat="server" src="~/images/spacer.gif" width="1" height="4" /></td>
                  <td align="center" class="boxInsideContent"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="6" height="6"><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
                        <td width="6" height="6"><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
                        <td width="6" height="6"><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
                      </tr>
                      <tr>
                        <td width="6" height="6"><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
                        <td width="100%"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td><table width="100%" border="0" cellpadding="0" cellspacing="0" >
                              <tr>
                                <td class="bbBoxInsideTopLeft"></td>
                                <td class="bbBoxInsideTop"></td>
                                <td class="bbBoxInsideTopRight"></td>
                              </tr>
                              <tr>
                                <td class="bbBoxInsideLeft"><img runat="server" src="~/images/spacer.gif" width="5" height="1"/></td>
                                <td class="bbBoxInsideContent" align="left">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="left">
                                    <tr>
                                      <td width="5" height="5"><img runat="server" src="~/images/spacer.gif" width="5" height="5"/></td>
                                      <td width="5" height="5"><img runat="server" src="~/images/spacer.gif" width="5" height="10"/></td>
                                      <td width="5" height="5"><img runat="server" src="~/images/spacer.gif" width="5" height="5"/></td>
                                    </tr>
                                    <tr>
                                      <td width="5" height="5"><img runat="server" src="~/images/spacer.gif" width="5" height="5"/></td>
                                      <td width="100%">
											<table width="100%" border="0" cellspacing="0" cellpadding="0" align="left">
												<tr id="trUsername" runat="server">
													<td width="12%" align="right" nowrap class="textGeneralGray01"><img runat="server" src="~/images/spacer.gif" width="3" height="5" id="Img1A"/></td>
													<td width="2%" align="right" nowrap class="textGeneralGray01">
														
													</td>
													<td width="4%" align="left" nowrap class="textGeneralGray01">
														<img runat="server" src="~/images/spacer.gif" width="5" height="5"/>Kaneva Member:
													</td>
													<td width="1%"><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
													<td width="40%" class="textGeneralGray01" align="left">
														<asp:TextBox ID="txtUsername" CssClass="formKanevaText" style="width:300px" MaxLength="100" runat="server"/>
													</td>
													<td width="48%" rowspan="6" align="left" class="textGeneralGray01" valign="top">Enter one Kaneva Member to which to send a gift, and select a single gift from the list below.</td>
												</tr>
												<tr>
													<td colspan="5" align="right" nowrap class="textGeneralGray01"><img runat="server" src="~/images/spacer.gif" width="1" height="6"/></td>
												</tr>
												<tr>
													<td align="right" nowrap class="textGeneralGray01">&nbsp;</td>
													<td align="right" nowrap class="textGeneralGray01">&nbsp;</td>
													<td align="left" nowrap class="textGeneralGray01"><img runat="server" src="~/images/spacer.gif" width="5" height="5"/>Subject :</td>
													<td>&nbsp;</td>
													<td align="left" class="textGeneralGray03">												
														<asp:TextBox ID="txtSubject" class="formKanevaText"  style="width:300px" MaxLength="100" runat="server"/>
														<asp:RequiredFieldValidator ID="rftxtSubject" ControlToValidate="txtSubject" Text="*" ErrorMessage="Subject is a required field." Display="Dynamic" runat="server"/>
													</td>
													<td width="48%" align="left" class="textGeneralGray01">&nbsp;</td>
												</tr>
												<tr>
													<td colspan="6" nowrap class="textGeneralGray01"><img runat="server" src="~/images/spacer.gif" width="1" height="4"/></td>
												</tr>  <input type="hidden" id="hidLinkURL" runat="server" value="" name="hidLinkURL"/>
											</table>
										</td>
                                      <td width="5" height="5"><img runat="server" src="~/images/spacer.gif" width="5" height="5"/></td>
                                    </tr>
                                    <tr>
                                      <td width="5" height="5"><img runat="server" src="~/images/spacer.gif" width="5" height="5"/></td>
                                      <td width="5" height="5"><img runat="server" src="~/images/spacer.gif" width="5" height="7"/></td>
                                      <td width="5" height="5"><img runat="server" src="~/images/spacer.gif" width="5" height="5"/></td>
                                    </tr>
                                </table></td>
                                <td class="bbBoxInsideRight"><img runat="server" src="~/images/spacer.gif" width="5" height="1"/></td>
                              </tr>
                              <tr>
                                <td class="bbBoxInsideBottomLeft"></td>
                                <td class="bbBoxInsideBottom"></td>
                                <td class="bbBoxInsideBottomRight"></td>
                              </tr>
                            </table></td>
                          </tr>
                          <tr>
                            <td>&nbsp;</td>
                          </tr>
                          <tr>
                            <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td align="left" class="textGeneralGray01"><span class="textGeneralGray01"><img runat="server" src="~/images/spacer.gif" width="1" height="3"/></span></td>
                              </tr>
                              <tr>
                                <td class="bbGrayLine"><img runat="server" src="~/images/spacer.gif" width="1" height="1"/></td>
                              </tr>
                              <tr>
                                <td><span class="textGeneralGray01"><img runat="server" src="~/images/spacer.gif" width="1" height="8"/></span>									</td>
                              </tr>
                              <tr>
                                <td><span class="textGeneralGray01"><img runat="server" src="~/images/spacer.gif" width="1" height="8"/></span></td>
                              </tr>
                              <tr>
                                <td align="center">
									<CE:Editor id="txtBody" runat="server" MaxTextLength="100000" bgcolor="#ededed" width="100%" Height="200px" ShowHtmlMode="False" ConfigurationPath="~/CuteSoft_Client/CuteEditor/Configuration/AutoConfigure/forum.config" AutoConfigure="None" ></CE:Editor>
									<asp:RequiredFieldValidator ID="rftxtBody" ControlToValidate="txtBody" Text="*" ErrorMessage="Body text is a required field." Display="Dynamic" runat="server"/>
								</td>
                              </tr>
                              <tr>
                                <td align="left"><span class="textGeneralGray01"><img runat="server" src="~/images/spacer.gif" width="1" height="8"/></span></td>
                              </tr>
                              <tr>
                                <td class="bbGrayLine"><img runat="server" src="~/images/spacer.gif" width="1" height="1"/></td>
                              </tr>
                              <tr>
                                <td align="left"><span class="textGeneralGray01"><img runat="server" src="~/images/spacer.gif" width="1" height="8"/></span></td>
                              </tr>
                              <tr>
                                <td align="right"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                                  <tr>
									<ajax:ajaxpanel id="AjaxpanelMessage" runat="server">
                                    <td align="left" nowrap class="textGeneralGray01" >
                                    Currently selected gift: <asp:label runat="server" id="lblSelection" cssclass="insideBoxText11"/>
                					<asp:label runat="server" id="lblGiftId" visible="false" />
                                    </td>
                                    </ajax:ajaxpanel>
                                    <td align="right"><asp:button id="btnCancel" runat="Server" CausesValidation="False" onClick="btnCancel_Click" class="Filter2" Text="    cancel    "/></td>
                                    <td><span class="textGeneralGray01"><img runat="server" src="~/images/spacer.gif" width="2" height="8"/></span></td>
                                    <td width="6%" align="right"><asp:button id="btnUpdate" runat="Server" onClick="btnUpdate_Click" class="Filter2" Text="    submit    "/></td> 
                                  </tr>
                                </table></td>
                              </tr>
                            </table></td>
                          </tr>
                        </table></td>
                        <td width="6" height="6"><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
                      </tr>
                      <tr>
                        <td width="6" height="6"><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
                        <td width="6" height="6"><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
                        <td width="6" height="6"><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
                      </tr>
                  </table></td>
                  <td class="boxInsideRight"><img runat="server" src="~/images/spacer.gif" width="1" height="4"/></td>
                </tr>
                <tr>
                  <td class="boxInsideBottomLeft" ><img runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
                  <td class="boxInsideBottom2"><img runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
                  <td class="boxInsideBottomRight"><img runat="server" src="~/images/spacer.gif" width="4" height="4" /></td>
                </tr>
            </table></td>
            <td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1"/></td>
          </tr>
          <tr>
            <td class="frBottomLeft"></td>
            <td class="frBottom"></td>
            <td class="frBottomRight"></td>
          </tr>
          <tr>
            <td><img runat="server" src="~/images/spacer.gif" width="1" height="14"/></td>
          </tr>
        </table></td>
        </tr>
      <tr>
        <td valign="top">&nbsp;</td>
      </tr>      
    </table>				</td>
				</tr>
				<tr>
					<td valign="top">
						<table  border="0" cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<td>
									<ajax:ajaxpanel id="AjaxpanelGifts" runat="server">
									
									<!-- Catalog -->
									<table id="tblSearchResults" runat="server"  border="0" cellpadding="0" cellspacing="0" width="100%" visible="False">
										<tr>
											<td class="frTopLeft"></td>
											<td class="frTop"></td>
											<td class="frTopRight"></td>
										</tr>
										<tr>
											<td bgcolor="#f1f1f2" class="frBorderLeft"><img src="../images/spacer.gif" width="1" height="1"></td>
											<td valign="top" bgcolor="#f1f1f2" class="frBgIntMembers">
												
												<table  border="0" cellpadding="0" cellspacing="0" class="boxInside" width="100%">
													<tr>
														<td class="boxInsideTopLeft"><img runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
														<td class="boxInsideTop" ><img runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
														<td class="boxInsideTopRight"><img runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
													</tr>
													
													<tr class="boxInside">
														<td class="boxInsideleft" ><img runat="server" src="~/images/spacer.gif" width="1" height="4" /></td>
														<td class="boxInsideContent" align="left">
															
															<table border="0" cellpadding="0" cellspacing="0" width="100%">
																<tr><td colspan="2"><img runat="server" src="~/images/spacer.gif" width="1" height="4" /></td></tr>
																<tr>
																	<td colspan="2">
																		<table border="0" cellpadding="0" cellspacing="0" width="100%">
																			<tr>
																				<td class="insideBoxText11" align="left" style="padding:0 10px 0 10px;">
																					<asp:label runat="server" id="lblSearch" cssclass="insideBoxText11"/>		
																				</td>
																				<td  class="insideBoxText11" align="right" style="padding-right:10px;">
																					<kaneva:pager runat="server" IsAjaxMode="True" id="pgTop"/>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
																<tr><td  colspan="2"><img runat="server" src="~/images/spacer.gif" width="7" height="10"></td></tr>	
															
																<tr>
																	<td>
																		<asp:DataGrid OnItemCommand="dgrdCatalog_ItemCommand" EnableViewState="True" runat="server" showheader="False" ShowFooter="False" Width="100%" id="dgrdCatalog" cellpadding="0" cellspacing="0" border="0" AutoGenerateColumns="False" AllowSorting="True" style="border: 0px hidden white;">  
																			<Columns>
																
																				<asp:templatecolumn>
																					<itemtemplate>
																						
																						<!-- Formatting top start -->
																							<table border="0" cellpadding="0" cellspacing="0" width="100%">
																								<tr>
																									<td><img runat="server" src="~/images/spacer.gif" width="5" height="1" /></td>
																									<td>
																										<table width="100%" border="0" cellpadding="0" cellspacing="0" align="left">
																											<tr>
																												<td class="frTopLeft"></td>
																												<td class="frTop"></td>
																												<td class="frTopRight"></td>
																											</tr>
																											<tr> 
																												<td class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
																												<td valign="top" bgcolor="f1f1f2" class="frBgInt1" align="left">
																								
																													<!-- Formatting top end -->
																													<!-- Media Start -->
																													<table cellpadding="0" cellspacing="0" border="0" width="100%">
																														<tr>
																															<td rowspan="9"><img runat="server" src="~/images/spacer.gif" width="5" height="1" /></td>
																															<td align="left" class="textGeneralGray12" colspan="4">
																																<table width="100%" border="0" cellpadding="0" cellspacing="0">
																																	<tr valign="top">
																																		<td align="left" class="textGeneralGray12"><%# DataBinder.Eval(Container.DataItem, "name") %></td>
																																	</tr>
																																</table>
																															</td>
																															<td rowspan="9"><img runat="server" src="~/images/spacer.gif" width="5" height="1" id="Img10"/></td>
																														</tr>
																														<tr>
																															<td colspan="4"><img runat="server" src="~/images/spacer.gif" width="1" height="5" /></td>
																														</tr>
																														<tr>
																															<td align="left" colspan="2" rowspan="3" width="135">
																																<table border="0" cellpadding="0" cellspacing="0" width="135">
																																	<tr>
																																		<td class="bbSchBoxInsideTopLeft"></td>
																																		<td class="bbSchBoxInsideTop"></td>
																																		<td class="bbSchBoxInsideTopRight"></td>
																																	</tr>
																																	<tr>
																																		<td class="bbSchBoxInsideLeft"><img runat="server" src="~/images/spacer.gif" width="3" height="1"/></td>
																																		<td width="126">
																																			<table border="0" cellpadding="0" cellspacing="0" >
																																				<tr>
																																					<td class="bbSchBoxInsideTopLeft2"></td>
																																					<td class="bbSchBoxInsideTop2"></td>
																																					<td class="bbSchBoxInsideTopRight2"></td>
																																				</tr>
																																				<tr>
																																					<td class="bbSchBoxInsideLeft2"><img runat="server" src="~/images/spacer.gif" width="3" height="1"/></td>
																																					<td align="center">
																																						<div style="width: 120px; height: 77px; background-color: #FFFFFF; overflow: hidden; vertical-align: middle; border: 1px solid #e0e0e0;">
																																						<asp:imagebutton causesValidation="false" style="cursor:hand;" runat="server" commandname='<%# DataBinder.Eval (Container.DataItem, "name")%>' commandargument='<%# DataBinder.Eval (Container.DataItem, "gift_id")%>'  imageurl='<%# GetGiftImageURL( DataBinder.Eval (Container.DataItem, "gift_id").ToString() ) %>' ToolTip='<%# GetGiftToolTip() %>' cssclass="filter2" />
																																						</div>
																																						
																																					</td>
																																					<td class="bbSchBoxInsideRight2"><img runat="server" src="~/images/spacer.gif" width="3" height="1"/></td>
																																				</tr>
																																				<tr>
																																					<td class="bbSchBoxInsideBottomLeft2"></td>
																																					<td class="bbSchBoxInsideBottom2"></td>
																																					<td class="bbSchBoxInsideBottomRight2"></td>
																																				</tr>
																																			</table>
																																		</td>
																																		<td class="bbSchBoxInsideRight"><img runat="server" src="~/images/spacer.gif" width="3" height="1"/></td>
																																	</tr>
																																	<tr>
																																		<td class="bbSchBoxInsideBottomLeft"></td>
																																		<td class="bbSchBoxInsideBottom"></td>
																																		<td class="bbSchBoxInsideBottomRight"></td>
																																	</tr>
																																		<tr>
																																			<td/>
																																			<td align="center" class="textGeneralGray11" valign="top">
																																				Price: <%# GetPrice( DataBinder.Eval (Container.DataItem, "price").ToString() ) %>
																																			</td>
																																			
																																		</tr>
																																</table>
																															</td>
																															<td width="8" rowspan="3" align="left"><img runat="server" src="~/images/spacer.gif" width="8" height="1" id="Img6"/></td>
																															<td align="left" class="textGeneralGray11" valign="top">
																																<%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "description").ToString ())%>
																															</td>
																														</tr>
																														<tr>
																															<td align="left"><img runat="server" src="~/images/spacer.gif" width="390" height="4" /></td>
																														</tr>																													
																														<tr>
																															<td align="left" valign="bottom">
																															</td>
																														</tr>
																														<tr><td colspan="4"><img runat="server" src="~/images/spacer.gif" width="1" height="8" /></td></tr>
																														<tr>
																															<td colspan="4"><img runat="server" src="~/images/spacer.gif" width="1" height="6" /></td>
																														</tr>
																														

																													</table>
																												<!-- Media End -->
																												<!-- Formatting bottom start -->
																												</td>
																												<td bgcolor="f1f1f2" class="frBorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
																											</tr>
																											<tr>
																												<td class="frBottomLeft"></td>
																												<td class="frBottom"></td>
																												<td class="frBottomRight"></td>
																											</tr>
																										</table>
																									</td>
																									<td><img runat="server" src="~/images/spacer.gif" width="5" height="1" /></td>
																								</tr>
																								<tr><td><img runat="server" src="~/images/spacer.gif" width="5" height="8" id="Img5"/></td></tr>																					
																							</table>
																																											
																						
																						<!-- Formatting bottom end -->
																					</itemtemplate>
																				</asp:templatecolumn>
																
																			</Columns>
																		</asp:datagrid>
																	
																	</td>
																</tr>
																<tr><td  colspan="2"><img runat="server" src="~/images/spacer.gif" width="7" height="10"></td></tr>
																	<td colspan="2"> 
																		<table border="0" cellpadding="0" cellspacing="0" width="100%">
																			<tr>
																				<td  class="insideBoxText11" align="left"></td>
																				<td width="5" ><img runat="server" src="~/images/spacer.gif" width="10" height="10" /></td>
																				<td  class="insideBoxText11" align="right">
																					<Kaneva:Pager runat="server" IsAjaxMode="True" id="pgBottom"/>
																				</td>
																				<td><img runat="server" src="~/images/spacer.gif" width="2" height="1"/></td>
																			</tr>
																		</table>
																	</td>
																</tr>
																<tr><td  colspan="2"><img runat="server" src="~/images/spacer.gif" width="5" height="10"></td></tr>	

															</table>
														</td>
														<td class="boxInsideRight"><img runat="server" src="~/images/spacer.gif" width="1" height="4"/></td>
													</tr>
													<tr>
														<td class="boxInsideBottomLeft" ><img runat="server" src="~/images/spacer.gif" width="4" height="4" /></td>
														<td class="boxInsideBottom"><img runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
														<td class="boxInsideBottomRight"><img runat="server" src="~/images/spacer.gif" width="4" height="4" /></td>
													</tr>
												</table>
												
											</td>
											<td class="frBorderRight"><img src="../images/spacer.gif" width="1" height="1"></td>
										</tr>
										<tr>
											<td class="frBottomLeft"></td>
											<td class="frBottom"></td>
											<td class="frBottomRight"></td>
										</tr>
										<tr>
											<td><img src="../images/spacer.gif" width="1" height="14"></td>
										</tr>
									</table>
									<!-- end search results -->

									</ajax:ajaxpanel>
						</table>	 
					</td>
				</tr>
			</table>
		</td>
		<td><img runat="server" src="~/images/spacer.gif" width="14" height="1"/></td>
	</tr>
    <tr>
        <td align="left" valign="top" colspan="3"></td>
	</tr>
</table>


