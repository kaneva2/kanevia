﻿<%@ Page Language="C#" validaterequest="false" MasterPageFile="~/masterpages/GenericPageTemplate.Master" AutoEventWireup="true" CodeBehind="contests.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.contests" %>
<%@ Register TagPrefix="Kaneva" TagName="MKContainer" Src="~/usercontrols/doodad/ContainerMyKaneva.ascx" %>
<%@ Register TagPrefix="UserControl" TagName="ContestWidget" Src="~/usercontrols/ContestWidget.ascx" %>
<%@ Register TagPrefix="UserControl" TagName="ColorBox" Src="~/usercontrols/ColorBox.ascx" %>
<%@ Register TagPrefix="UserControl" TagName="ContestTabs" Src="~/mykaneva/widgets/ContestTabs.ascx" %>
<%@ Register TagPrefix="UserControl" TagName="ContestDetails" Src="~/mykaneva/widgets/ContestDetails.ascx" %>
<%@ Register TagPrefix="UserControl" TagName="ContestSearchResults" Src="~/mykaneva/widgets/ContestSearchResults.ascx" %>

<asp:Content ID="cntContestHead" runat="server" contentplaceholderid="cph_HeadData" >

	<script type="text/javascript" src="../jscript/prototype-1.6.1-min.js"></script>
    <script type="text/javascript" src="../jscript/contests.js?v2"></script>
    <script type="text/javascript" src="../jscript/yahoo/2.7.0/yahoo-dom-event.js"></script>
    <script type="text/javascript" src="../jscript/yahoo/2.7.0/element-min.js"></script>
    <script type="text/javascript" src="../jscript/yahoo/2.7.0/paginator-min.js"></script>
	<script type="text/javascript" src="../jscript/scriptaculous/1.8.2/effects.js"></script>

    <NOSCRIPT><META HTTP-EQUIV="refresh" CONTENT="1" URL="noJavascript.aspx" /></NOSCRIPT>

    <link href="../css/contests.css?v2" type="text/css" rel="stylesheet" />     

</asp:Content>

<asp:Content id="cntBody" runat="server" contentplaceholderid="cph_Body" >

<usercontrol:colorbox runat="server" id="ucColorBox" />

<script type="text/javascript">
var $j = jQuery.noConflict();

$j(document).ready(function () {
	$j(".iframe").colorbox({ iframe: true, innerWidth: 600, innerHeight: 456, scrolling: false, overlayClose: false, closeButton: false });
	$j(".addentry").colorbox({ iframe: true, innerWidth: 400, innerHeight: 200, scrolling: false, overlayClose: false, closeButton: false });
	$j("#txtSearch").keyup(function (event) {var keycode = (event.keyCode ? event.keyCode : event.which);if (keycode == '13') {$j("#btnSearch").click();}});
});
function ShowDetails(id){$j("#hidContestId").val(id); $j("#bntShowDetails").click();}
</script>
<asp:button id="btnSearch" clientidmode="Static" runat="server" onclick="bntSearch_Click" style="visibility:hidden;" />
<asp:button id="bntShowDetails" clientidmode="Static" runat="server" onclick="bntShowDetails_Click" style="visibility:hidden;" />
<input type="hidden" value="" id="hidContestId" clientidmode="Static" runat="server" />
<div id="contest_container">

	<!-- Left Column -->
	<div id="leftRail">
		<kaneva:mkcontainer id="MKcontainerProfile" runat="server" ControlToLoad="usercontrols/doodad/UserProfile.ascx" />
		<kaneva:mkcontainer id="MKcontainerMessageSummary" runat="server" ControlToLoad="usercontrols/doodad/MessageSummary.ascx" />
	</div>
	
	<div id="datacontainer">										 
		<!-- Center Column -->
		<div id="sectionTitle">Creative Contests</div> <div id="back"><a class="button" id="aBack" runat="server" visible="false" href="~/mykaneva/contests.aspx"><span>< Back to Contests</span></a></div>
		<asp:updatepanel id="upSearch" runat="server" childrenastriggers="true" updatemode="conditional">
			<contenttemplate>
				<div id="search"><span>Search Contests:</span> <asp:textbox id="txtSearch" runat="server" clientidmode="Static"></asp:textbox>
					<a class="btnxsmall grey" id="aSearch" runat="server" clientidmode="Static" onserverclick="bntSearch_Click">Search</a>
					<a id="btnCreate" href="~/mykaneva/createcontest.aspx" runat="server" class="iframe create"><img id="Img1" src="~/images/contests/btn_CreateContest_257x41.gif" runat="server" width="257" height="41" border="0"/></a>
				</div>										
			</contenttemplate>								
		</asp:updatepanel>
																   
		<div class="clearit"></div>
		<div id="centerRail">

		<asp:updatepanel id="upDetails" runat="server" childrenastriggers="true" updatemode="conditional" visible="false">
			<contenttemplate>
				<usercontrol:ContestDetails runat="server" id="ucContestDetails" />
			</contenttemplate>
		</asp:updatepanel>

		<asp:updatepanel id="upTabs" runat="server" childrenastriggers="true" updatemode="conditional">
			<contenttemplate>
				<usercontrol:ContestTabs runat="server" id="ucContestTabs" />
			</contenttemplate>
		</asp:updatepanel>										 

		<asp:updatepanel id="upSearchResults" runat="server" childrenastriggers="true" updatemode="conditional" visible="false">
			<contenttemplate>
				<usercontrol:ContestSearchResults runat="server" id="ucContestSearchResults" />
			</contenttemplate>
		</asp:updatepanel>										 
		</div>
	
		<!-- Right Column -->
		<div id="rightRail">
		<asp:updatepanel id="Updatepanel1" runat="server" childrenastriggers="false" updatemode="conditional">
			<contenttemplate>
			<usercontrol:ContestWidget runat="server" MyContest="true"></usercontrol:ContestWidget>
			<usercontrol:ContestWidget runat="server" TopPrizes="true"></usercontrol:ContestWidget>
			<usercontrol:ContestWidget runat="server" HotContests="true"></usercontrol:ContestWidget>
		</contenttemplate>
		</asp:updatepanel>
		</div>
	</div>

</div>

</asp:Content>
