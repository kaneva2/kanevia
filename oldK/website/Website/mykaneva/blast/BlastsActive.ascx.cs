///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer;
using log4net;
using System.Text.RegularExpressions;

namespace KlausEnt.KEP.Kaneva.mykaneva.blast
{
    public partial class BlastsActive : Kaneva.mykaneva.widgets.ModuleViewBaseControl
    {
        protected void Page_Load (object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (CurrentCommunity.CommunityId > 0)
                {
                    SetFilterCookie ("blastFilterCommId" + CurrentCommunity.CommunityId.ToString ());
                }
                else
                {
                    SetFilterCookie ("blastFilter");
                    blastBox.Style.Add ("display", "block");
                }

                pgActiveBlasts.CurrentPageNumber = 1;
                BindBlasts (1);

                // Set the max len of a blast
                litMaxLen.Text = KanevaGlobals.MaxBlastLength.ToString ();
                spnRemain.InnerText = KanevaGlobals.MaxBlastLength.ToString ();
                spnMax.InnerText = KanevaGlobals.MaxBlastLength.ToString ();
            }

            // Load the needed JS libraries
            RegisterJavaScript ("/jscript/dw_tooltip/dw_event.js");
            RegisterJavaScript ("/jscript/dw_tooltip/dw_viewport.js");
            RegisterJavaScript ("/jscript/dw_tooltip/dw_tooltip.js");

            litCommentsFilePath.Text = ResolveUrl ("~/usercontrols/doodad/");

            if (KanevaWebGlobals.CurrentUser.FacebookSettings.FacebookUserId == 0)
            {
                liFacebook.Visible = false;
            }
        }


        #region Helper Methods

        private void BindBlasts (int pageNumberActive)
        {
            // Set Blast Type links
            aBlastTypeVideo.HRef = ResolveUrl ("~/mykaneva/blast/newblast.aspx?bt=" + ((int) BlastTypes.VIDEO).ToString () + "&communityId=" + this.CurrentCommunity.CommunityId.ToString ());
            aBlastTypePhoto.HRef = ResolveUrl ("~/mykaneva/blast/newblast.aspx?bt=" + ((int) BlastTypes.PHOTO).ToString () + "&communityId=" + this.CurrentCommunity.CommunityId.ToString ());
            aBlastTypeLink.HRef = ResolveUrl ("~/mykaneva/blast/newblast.aspx?bt=" + ((int) BlastTypes.LINK).ToString () + "&communityId=" + this.CurrentCommunity.CommunityId.ToString ());
            aBlastTypePlace.HRef = ResolveUrl ("~/mykaneva/blast/newblast.aspx?bt=" + ((int) BlastTypes.PLACE).ToString () + "&communityId=" + this.CurrentCommunity.CommunityId.ToString ());

            // Get Active Blasts
            GetActiveBlasts (pageNumberActive);
        }

        private void SetFilterCookie (string filterName)
        {
            System.Web.HttpCookie aCookie;

            //set pulldown
            if (Request.Cookies[filterName] != null)
            {
                if ((Request.Cookies[filterName].Value != null) && (Request.Cookies[filterName].Value != ""))
                {
                    drpActiveBlasts.SelectedValue = Request.Cookies[filterName].Value;
                }
                else
                {
                    Response.Cookies[filterName].Value = drpActiveBlasts.SelectedValue;
                    Response.Cookies[filterName].Expires = DateTime.Now.AddYears (1);
                }
            }
            else
            {
                aCookie = new HttpCookie (filterName);
                aCookie.Expires = DateTime.Now.AddYears (1);
                aCookie.Value = drpActiveBlasts.SelectedValue;
                Response.Cookies.Add (aCookie);
            }
        }

        private void GetActiveBlasts (int pageNumber)
        {
            BlastFacade blastFacade = new BlastFacade ();

            try
            {
                PagedList<Blast> blasts = null;
                int MaxBlasts = 20;

                // Get community blasts
                if (this.CurrentCommunity.CommunityId > 0)
                {
                    ConfigureCommunityBlasts ();

                    // Get blasts based on filter selected
                    if (drpActiveBlasts.SelectedValue.Equals ("My"))  // My Blasts
                    {
                        blasts = blastFacade.GetCommunityActiveBlasts (CurrentCommunity.CommunityId, Current_User.UserId, false, MaxBlasts + 1, pageNumber);
                    }
                    else // All Blasts
                    {
                        int senderId = CurrentCommunity.IsPersonal.Equals (1) ? CurrentCommunity.CreatorId : 0;
                        blasts = blastFacade.GetCommunityActiveBlasts (CurrentCommunity.CommunityId, senderId, CurrentCommunity.IsPersonal.Equals (1), MaxBlasts + 1, pageNumber);
                    }
                }
                else
                {
                    if (drpActiveBlasts.SelectedValue.Equals ("Fr"))  // Friends Only
                    {
                        blasts = blastFacade.GetFriendActiveBlasts (Current_User.UserId, MaxBlasts + 1, pageNumber);
                    }
                    else if (drpActiveBlasts.SelectedValue.Equals ("My"))  // My Blasts
                    {
                        blasts = blastFacade.GetMyActiveBlasts (Current_User.UserId, MaxBlasts + 1, pageNumber);
                    }
                    else //All Blasts
                    {
                        blasts = blastFacade.GetActiveBlasts (Current_User.UserId, Current_User.UserId, MaxBlasts + 1, pageNumber);

                        if (pageNumber == 1)
                        {
                            rptAdmBlasts.DataSource = WebCache.GetAdminBlasts ();
                            rptAdmBlasts.DataBind ();
                        }
                    }
                }

                // Bind the Blasts
                rptActiveBlasts.DataSource = blasts;
                rptActiveBlasts.DataBind ();

                // The results
                int currentPageCount = 0;
                pgActiveBlasts.CurrentPageNumber = pageNumber;
                if ((pgActiveBlasts.NumberOfPages == "0") || (pageNumber == 1))
                {
                    pgActiveBlasts.NumberOfPages = "1";
                }

                if (blasts.Count > MaxBlasts)
                {
                    currentPageCount = MaxBlasts;
                    int numberOfPages = Convert.ToInt32 (pgActiveBlasts.NumberOfPages);
                    if (pageNumber >= numberOfPages)
                    {
                        pgActiveBlasts.NumberOfPages = Convert.ToString (numberOfPages + 1);
                    }
                }
                else
                {
                    currentPageCount = blasts.Count;
                }

                pgActiveBlasts.DrawControl ();

                if (blasts.TotalCount == 0)
                {
                    // Is community or profile
                    if (CurrentCommunity.IsPersonal.Equals (1))
                    {
                        // Is user owner or friend
                        if (Current_User.UserId == CurrentCommunity.CreatorId ||
                            GetUserFacade.AreFriends (Current_User.UserId, CurrentCommunity.CreatorId))
                        {
                            // Don't display any call to action
                        }
                        else // Non-friend
                        {
                            if (Request.IsAuthenticated)
                            {
                                divNoData.InnerHtml = "<div>No Blasts Found for this Member.</div>" +
                                    "<div>Would you like to <a href=\"" + ResolveUrl ("~/myKaneva/newMessage.aspx?userId=") + CurrentCommunity.CreatorId.ToString () +
                                    "\" alt=\"Send a Message\" >Send a Message</a>" +
                                    " or <a href=\"javascript:if (confirm('Send a friend request to this user?'))AddFriend();" +
                                    "\" alt=\"Become a Friend\" >Become a Friend</a>?</div>";
                            }
                            else
                            {
                                divNoData.InnerHtml = "<div>No Blasts Found for this Member.</div>" +
                                    "<div>Would you like to <a href=\"" + ResolveUrl ("~/myKaneva/newMessage.aspx?userId=") + CurrentCommunity.CreatorId.ToString () +
                                    "\" alt=\"Send a Message\" >Send a Message</a>" +
                                    " or <a href=\"javascript:if (confirm('You must be logged in to add a friend.  Do you want to login now?'))AddFriend();" +
                                    "\" alt=\"Become a Friend\" >Become a Friend</a>?</div>";
                            }
                            divNoData.Visible = true;
                        }
                    }
                    else  // Community
                    {
                        // (Community owner or community member) or blasts on MyKaneva Page
                        if (Current_User.UserId == CurrentCommunity.CreatorId ||
                            GetCommunityFacade.IsActiveCommunityMember (CurrentCommunity.CommunityId, Current_User.UserId) ||
                            CurrentCommunity.CreatorId == 0)
                        {
                            // Don't display any call to action
                        }
                        else // Non-Member
                        {
                            divNoData.InnerHtml = "<div>No blasts found for this World.</div>" +
                                "<div>Would you like to <a href=\"" +
                                ResolveUrl ("~/myKaneva/newMessage.aspx?userId=" + community.CreatorId + "&communityId=" + community.CommunityId) +
                                "\" alt=\"Send a Message\" >Send a Message</a>" +
                                " or go to your <a onclick=\"javascript:return confirm('Join this World?')\" " +
                                "href=\"" + ResolveUrl ("~/community/commJoin.aspx?communityId=" + CurrentCommunity.CommunityId + "&join=Y") +
                                "\" alt=\"Join World\" >Join this World</a>?</div>";
                            divNoData.Visible = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                m_logger.Error ("Error getting blasts. UserId=" + Current_User.UserId + ", CommunityId = " + CommunityId, ex);
            }
        }

        private void ConfigureCommunityBlasts ()
        {
            // Is current user the community owner
            if (CurrentCommunity.CreatorId != Current_User.UserId)
            {
                if (CurrentCommunity.IsPersonal.Equals (1))
                {
                    IsUserFriend = GetUserFacade.AreFriends (Current_User.UserId, CurrentCommunity.CreatorId);
                    if (IsUserFriend)
                    {
                        blastBox.Style.Add ("display", "block");
                    }
                    else
                    {
                        blastBox.Style.Add ("display", "none");
                    }
                }
                else
                {
                    IsUserActiveCommunityMember = new CommunityFacade ().IsActiveCommunityMember (this.CurrentCommunity.CommunityId, Current_User.UserId);
                    if (IsUserCommunityAdmin () || IsUserActiveCommunityMember)
                    {
                        blastBox.Style.Add ("display", "block");
                    }
                    else
                    {
                        blastBox.Style.Add ("display", "none");
                    }
                }
            }
            else
            {
                blastBox.Style.Add ("display", "block");
            }

            // hide Facebook checkbox
            liFacebook.Visible = false;

            // remove My Friends from the blasts filter
            drpActiveBlasts.Items.Remove (drpActiveBlasts.Items.FindByText ("My Friends"));
        }

        private bool IsUserCommunityAdmin ()
        {
            //check security level
            int privilegeId = 0;

            //get the appropriate privilege id based on the community type
            switch (CurrentCommunity.CommunityTypeId)
            {
                case (int) CommunityType.USER:
                    privilegeId = (int) SitePrivilege.ePRIVILEGE.USER_PROFILE_ADMIN;
                    break;
                case (int) CommunityType.COMMUNITY:
                case (int) CommunityType.APP_3D:
                case (int) CommunityType.FAME:
                    privilegeId = (int) SitePrivilege.ePRIVILEGE.COMMUNITY_PROFILE_ADMIN;
                    break;
            }

            return HasWritePrivileges (privilegeId); ;
        }

        /// <summary>
        /// GetBlastIcon
        /// </summary>
        public string GetBlastIcon (int blastType, string thumbnailSquarePath)
        {
            switch (blastType)
            {
                case (int) BlastTypes.BASIC:
                case (int) BlastTypes.EVENT:
                case (int) BlastTypes.LINK:
                case (int) BlastTypes.PHOTO:
                case (int) BlastTypes.PLACE:
                case (int) BlastTypes.VIDEO:
                    return "<img src=\"" + GetProfileImageURL (thumbnailSquarePath, "sq", "M") + "\" border=\"0\" width=\"30\" />";

                case (int) BlastTypes.ADMIN:
                    return "<img src=\"" + ResolveUrl ("~/images/blast/admin-blast-logo_22x21.gif") + "\" alt=\"Kaneva Admin\" width=\"22\" height=\"21\" border=\"0\">";


                // Passive Activities

                case (int) BlastTypes.BLOG:
                    return "<img src=\"" + ResolveUrl ("~/images/activity_blog.gif") + "\" border=\"0\" width=\"16\" height=\"16\" />";

                case (int) BlastTypes.FORUM_POST:
                    return "<img src=\"" + ResolveUrl ("~/images/activity_forum.gif") + "\" border=\"0\" width=\"16\" height=\"16\" />";

                case (int) BlastTypes.JOIN_COMMUNITY:
                    return "<img src=\"" + ResolveUrl ("~/images/activity_community.gif") + "\" border=\"0\" width=\"16\" height=\"16\" />";

                case (int) BlastTypes.MEDIA_COMMENT:
                    return "<img src=\"" + ResolveUrl ("~/images/activity_comment.gif") + "\" border=\"0\" width=\"16\" height=\"16\" />";

                case (int) BlastTypes.PROFILE_UPDATE:
                    return "<img src=\"" + ResolveUrl ("~/images/activity_profile.gif") + "\" border=\"0\" width=\"16\" height=\"16\" />";

                case (int) BlastTypes.RAVE_3D_HANGOUT:
                    return "<img src=\"" + ResolveUrl ("~/images/activity_rave.gif") + "\" border=\"0\" width=\"16\" height=\"16\" />";

                case (int) BlastTypes.RAVE_3D_HOME:
                    return "<img src=\"" + ResolveUrl ("~/images/activity_rave.gif") + "\" border=\"0\" width=\"16\" height=\"16\" />";

                case (int) BlastTypes.SEND_GIFT:
                    return "<img src=\"" + ResolveUrl ("~/images/activity_gift.gif") + "\" border=\"0\" width=\"16\" height=\"16\" />";

                default:
                    return "";
            }
        }

        /// <summary>
        /// FormatBlast
        /// </summary>
        protected string FormatBlast (Object obj)
        {
            Blast blast = (Blast) obj;
            UInt64 entryId = blast.EntryId;
            int blastType = blast.BlastType;
            string subject = blast.Subj;
            string strBlastText = blast.DiaryEntry;

            if (blastType.Equals ((int) BlastTypes.PHOTO))
            {
                strBlastText = strBlastText.Insert (strBlastText.IndexOf ("<img ") + 5, " width=\"120\" ");
            }
            else if (blastType.Equals ((int) BlastTypes.CAMERA_SCREENSHOT))
            {
                // Link to Profile world and remove text
                strBlastText = strBlastText.Replace("<p>Click image to visit</p>", "");

                // Change image link
                if (blast.CommunityId > 0)
                {
                    CommunityFacade communityFacade = new CommunityFacade();
                    Community community = communityFacade.GetCommunity( Convert.ToInt32 (blast.CommunityId));
                    strBlastText = strBlastText.Insert(strBlastText.IndexOf("<img "), "<A href=\"" + GetBroadcastChannelUrl(community.NameNoSpaces) + "\">");
                    strBlastText = strBlastText.Insert(strBlastText.Length, "</A>");

                    // Set image width
                    strBlastText = strBlastText.Insert(strBlastText.IndexOf("<img ") + 5, " style=\"cursor:pointer;\" width=\"400\" ");
                }

                subject = subject.Replace ("javascript:void(0);", GetBroadcastChannelUrl(blast.NameNoSpaces));
            }

            if ((blastType.Equals ((int) BlastTypes.RAVE_3D_APP) || blastType.Equals ((int) BlastTypes.CAMERA_SCREENSHOT)) &&
                blast.OriginatorId == 0)
            {
                strBlastText = subject + "<br />" + strBlastText;
            }

            // Resize for Videos
            if (blastType.Equals ((int) BlastTypes.VIDEO))
            {
                // Need the htmldecoded version for vidoe embed codes
                strBlastText = Server.HtmlDecode (blast.DiaryEntry);

                // Read & resize the current width
                int iMAxYoutubeWidth = 320;
                string strWidthFindString = "width=\"";
                int iStartIndexOfWidth = strBlastText.IndexOf (strWidthFindString, 0, StringComparison.CurrentCultureIgnoreCase);

                if (iStartIndexOfWidth > -1)
                {
                    int iEndIndexOfWidth = strBlastText.IndexOf ("\"", iStartIndexOfWidth + strWidthFindString.Length);

                    if (iEndIndexOfWidth > -1)
                    {
                        string strWidth = strBlastText.Substring (iStartIndexOfWidth + strWidthFindString.Length, iEndIndexOfWidth - (iStartIndexOfWidth + strWidthFindString.Length));
                        if (KanevaGlobals.IsNumeric (strWidth)) // && Convert.ToInt32 (strWidth) > iMAxYoutubeWidth)
                        {
                            strBlastText = strBlastText.Replace ("th=\"" + strWidth.ToString () + "\"", "th=\"" + iMAxYoutubeWidth.ToString () + "\"");
                        }
                    }
                }

                // Resize the height
                string strHeightFindString = "height=\"";
                int iStartIndexOfHeight = strBlastText.IndexOf (strHeightFindString, 0, StringComparison.CurrentCultureIgnoreCase);

                if (iStartIndexOfHeight > -1)
                {
                    int iEndIndexOfHeight = strBlastText.IndexOf ("\"", iStartIndexOfHeight + strHeightFindString.Length);

                    if (iEndIndexOfHeight > -1)
                    {
                        string strHeight = strBlastText.Substring (iStartIndexOfHeight + strHeightFindString.Length, iEndIndexOfHeight - (iStartIndexOfHeight + strHeightFindString.Length));
                        if (KanevaGlobals.IsNumeric (strHeight))
                        {
                            strBlastText = strBlastText.Replace ("ght=\"" + strHeight + "\"", "ght=\"265\"");
                        }
                    }
                }  // end of height resize


                string strVideoIdFindString = ".com/v/";
                int iStartIndexOfVideoId = strBlastText.IndexOf (strVideoIdFindString, 0, StringComparison.CurrentCultureIgnoreCase);

                // If is YouTube video
                if (iStartIndexOfVideoId > -1)
                {
                    int iEndIndexOfVideoId = strBlastText.IndexOf ("&", iStartIndexOfVideoId + strVideoIdFindString.Length);

                    if (iEndIndexOfVideoId > -1)
                    {
                        string strVideoId = strBlastText.Substring (iStartIndexOfVideoId + strVideoIdFindString.Length, iEndIndexOfVideoId - (iStartIndexOfVideoId + strVideoIdFindString.Length));
                        if (strVideoId.Length > 0)
                        {
                            strBlastText = SetVideoAutoPlay (strBlastText, "src=\"", "\"", "autoplay=1&", true);

                            strBlastText = "<div>" +
                                "<div id=\"divThumbContainer" + entryId.ToString () + "\" class=\"vidThumbContainer\" onclick=\"javascript:$('divVidPlay" +
                                    entryId.ToString () + "').style.float='none';$('divVidPlay" +
                                    entryId.ToString () + "').style.display='block';$('divThumbContainer" + entryId.ToString () + "').style.display='none';\">" +
                                "<img class=\"vidImgThumb\" src=\"http://i1.ytimg.com/vi/" + strVideoId + "/default.jpg\"/>" +
                                "<img src=\"" + ResolveUrl ("~/images/mykaneva/arrow_play.png") + "\" class=\"vidImgPlay\" />" +
                                "</div>" +
                                "<div id=\"divVidPlay" + entryId.ToString () + "\" style=\"display:none;\">" + strBlastText + "</div>" +
                                "<div class=\"feedVideoText\">" + subject + "</div>" +
                                "</div>";
                        }
                    }
                }    // Kaneva embedded video
                else if (strBlastText.IndexOf ("<embed ") > -1)
                {
                    Asset video = new MediaFacade ().GetAsset (GetVideoId (strBlastText, "assetId%3d-", "&"));
                    if (video != null && video.AssetId > 0)
                    {
                        strBlastText = SetVideoAutoPlay (strBlastText, "startMe=", "\"", "&autoStart=true", false);

                        strBlastText = "<div>" +
                           "<div id=\"divThumbContainer" + entryId.ToString () + "\" class=\"vidThumbContainer\" onclick=\"javascript:$('divVidPlay" +
                               entryId.ToString () + "').style.float='none';$('divVidPlay" +
                               entryId.ToString () + "').style.display='block';$('divThumbContainer" + entryId.ToString () + "').style.display='none';\" >" +
                           "<img class=\"vidImgThumb\" width=\"120\" src=\"" + KanevaGlobals.ImageServer + "/" + video.ThumbnailSmallPath + "\"/>" +
                           "<img src=\"" + ResolveUrl ("~/images/mykaneva/arrow_play.png") + "\" class=\"vidImgPlay\" />" +
                           "</div>" +
                           "<div id=\"divVidPlay" + entryId.ToString () + "\" style=\"display:none;\">" + strBlastText + "</div>" +
                           "<div class=\"feedVideoText\">" + subject + "</div>" +
                           "</div>";
                    }
                }
            }

            return strBlastText;
        }

        private string GetMeetMe3DLink (Blast blast)
        {
            Community blastCommunity = GetCommunityFacade.GetCommunity ((int) blast.CommunityId);

            // Configure play now link
            string rtsidRegex = Constants.cREQUEST_TRACKING_URL_PARAM + @"=([^""]+)""";
            // Changed this to pull from subject instead of diarytext because diary text no longer includes 
            // URL that has the ?RTSID= query string param. 
            string requestTypesSentId = Regex.Match (blast.Subj, rtsidRegex).Groups[1].Value;
            int gameId = GetCommunityFacade.GetCommunityGameId ((int) blast.CommunityId);

            if (blastCommunity.IsPersonal > 0)
            {   // This is for a users home
                return CommunityMeetMe3DLink (-1, (int) blast.CommunityId, blast.CommunityName, requestTypesSentId);
            }
            else
            {
                return CommunityMeetMe3DLink (gameId, (int) blast.CommunityId, blast.CommunityName, requestTypesSentId);
            }
        }

        private int GetVideoId (string fullString, string strFindString, string endingString)
        {
            int ret = 0;
            string strFindResult = "";
            int iStartIndexOfFindString = fullString.IndexOf (strFindString, 0, StringComparison.CurrentCultureIgnoreCase);

            if (iStartIndexOfFindString > -1)
            {
                int iEndIndexOfFindString = fullString.IndexOf (endingString, iStartIndexOfFindString + strFindString.Length);

                if (iEndIndexOfFindString > -1)
                {
                    strFindResult = fullString.Substring (iStartIndexOfFindString + strFindString.Length, iEndIndexOfFindString - (iStartIndexOfFindString + strFindString.Length));
                }
            }  // end of height resize

            if (strFindResult != "")
            {
                try 
                {
                    ret = Convert.ToInt32 (strFindResult);
                } 
                catch { }
            }

            return ret;
        }

        private string SetVideoAutoPlay (string embedCodeString, string startString, string endingString, string addString, bool isYouTube)
        {
            // add autoplay param                                     
            string strResult = "";
            int iStartIndexOfFindString = embedCodeString.IndexOf (startString, 0, StringComparison.CurrentCultureIgnoreCase);

            if (isYouTube)
            {
                // Search for autostart param
                string strAutoStartString = "autoplay=0";
                int iStartAutoStartString = embedCodeString.IndexOf (strAutoStartString, 0, StringComparison.CurrentCultureIgnoreCase);

                if (iStartAutoStartString > -1)
                {
                    return embedCodeString.Replace (strAutoStartString, "autoplay=1");
                }
            }
            else  // Kaneva video
            {
                // Search for autostart param
                string strAutoStartString = "autoStart=false";
                int iStartAutoStartString = embedCodeString.IndexOf (strAutoStartString, 0, StringComparison.CurrentCultureIgnoreCase);

                if (iStartAutoStartString > -1)
                {
                    return embedCodeString.Replace (strAutoStartString, "autoStart=true");
                }
            }

            // Add the new string 
            if (iStartIndexOfFindString > -1)
            {
                int iEndIndexOfFindString = embedCodeString.IndexOf (endingString, iStartIndexOfFindString + startString.Length);

                if (iEndIndexOfFindString > -1)
                {
                    strResult = embedCodeString.Insert (iEndIndexOfFindString, addString);
                }
            }

            return strResult;
        }

        /// <summary>
        /// FormatAdminBlast
        /// </summary>
        protected string FormatAdminBlast (UInt64 entryId, int blastType, string subject, string diaryEntry)
        {
            BlastFacade blastFacade = new BlastFacade ();
            return blastFacade.FormatBlast (blastType, subject, Page.ResolveUrl ("~/mykaneva/blast/blastDetails.aspx?entryId=" + entryId), diaryEntry);
        }

        /// <summary>
        /// FormatReplies
        /// </summary>
        protected string FormatReplies (int replies, UInt64 entryId)
        {
            try
            {
                if (replies > 0)
                {
                    return "<span class=\"reply\"><a href=\"" + Page.ResolveUrl ("~/mykaneva/blast/blastDetails.aspx?entryId=" + entryId) + "\">" + replies.ToString () + "</a></span>";
                }
                else
                {
                    return "<span class=\"replyNow\"><a href=\"" + Page.ResolveUrl ("~/mykaneva/blast/blastDetails.aspx?entryId=" + entryId) + "\">reply now</a></span>";
                }
            }
            catch
            {
                return "<span class=\"replyNow\"><a href=\"" + Page.ResolveUrl ("~/mykaneva/blast/blastDetails.aspx?entryId=" + entryId) + "\">reply now</a></span>";
            }
        }

        private void ShowError (string msg)
        {
            ShowError (msg, true);
        }
        private void ShowError (bool showMsg)
        {
            ShowError ("", showMsg);
        }
        private void ShowError (string msg, bool showMsg)
        {
            blastError.InnerHtml = "";
            blastError.Visible = false;

            if (showMsg)
            {
                blastError.InnerHtml = msg;
                blastError.Visible = true;
            }
        }

        private void SetActiveTab ()
        {
            if (CurrentCommunity.CommunityTypeId == (int) CommunityType.APP_3D ||
                CurrentCommunity.CommunityTypeId == (int) CommunityType.FAME)
            {
                // This is used to make sure the tab does not go back to the default tab if it is not the Blast tab 
                string js = "";
                string tabName = "";
                try
                {
                    KlausEnt.KEP.Kaneva.usercontrols.TabbedControlLoader tcLoader = (KlausEnt.KEP.Kaneva.usercontrols.TabbedControlLoader) this.Parent.NamingContainer;
                    //tcLoader.TabValues.IndexOf (;
                    foreach (TabDisplay tab in tcLoader.TabValues)
                    {
                        if (tab.TabId.Equals ((int) Constants.eTab.Blast))
                        {
                            js = "DisplayHiddenControl('wrapper_" + tab.TabName.ToLower () + "');";
                            tabName = "tab" + tab.TabName;
                            break;
                        }
                    }
                }
                catch { }

                Page.ClientScript.RegisterStartupScript (GetType (), "SelectTab", "<script type=\"text/javascript\">SetSelectedTab($('" + tabName + "'));" + js + "</script>");
            }
        }

        #endregion Helper Methods


        #region Event Handlers

        /// <summary>
        /// Page Change Event for Active Blasts
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pgActiveBlasts_PageChange (object sender, PageChangeEventArgs e)
        {
            pgActiveBlasts.CurrentPageNumber = e.PageNumber;
            pgActiveBlasts.DrawControl ();
            BindBlasts (e.PageNumber);
        }

        /// <summary>
        /// Send a Blast
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkBlast_Click (object sender, EventArgs e)
        {
            BlastFacade blastFacade = new BlastFacade ();
            txtBlast.Text = txtBlast.Text.Trim ();
            ShowError (false);

            if (Request.IsAuthenticated)
            {
                // Check for any inject scripts
                if (KanevaWebGlobals.ContainsInjectScripts (txtBlast.Text))
                {
                    BindBlasts (1);
                    m_logger.Warn ("User " + Current_User.UserId.ToString () + " tried to script from IP " + Common.GetVisitorIPAddress());
                    ShowError ("Your input contains invalid scripting, please remove script code and try again.");
                    SetActiveTab ();
                    return;
                }

                // Check to make sure user did not enter any "potty mouth" words
                if (KanevaWebGlobals.isTextRestricted (txtBlast.Text, Constants.eRESTRICTION_TYPE.POTTY_MOUTH))
                {
                    BindBlasts (1);
                    ShowError (Constants.VALIDATION_REGEX_RESTRICTED_WORD_MESSAGE);
                    SetActiveTab ();
                    return;
                }

                if (txtBlast.Text.Length == 0 || txtBlast.Text == "Blast what you are doing...")
                {
                    BindBlasts (1);
                    ShowError ("Please enter a blast.  You can not submit a blank blast.");
                    SetActiveTab ();
                    return;
                }

                if (txtBlast.Text.Length > KanevaGlobals.MaxCommentLength)
                {
                    BindBlasts (1);
                    ShowError ("Your blast is too long.  Blasts must be " + KanevaGlobals.MaxCommentLength.ToString () + " characters or less");
                    SetActiveTab ();
                    return;
                }

                string thumbnailSmallPath = Current_User.ThumbnailSquarePath;
                bool isCommunityBlast = false;
                bool isCommunityPersonal = false;
                string senderName = Current_User.Username;

                if (CurrentCommunity.CommunityId > 0)
                {
                    if (CurrentCommunity.CreatorId == Current_User.UserId)
                    {
                        thumbnailSmallPath = CurrentCommunity.ThumbnailSmallPath;
                        senderName = CurrentCommunity.Name;
                    }

                    isCommunityBlast = true;

                    if (CurrentCommunity.IsPersonal.Equals (1))
                    {
                        isCommunityPersonal = true;
                        isCommunityBlast = false;
                    }
                }

                // Verify user can send blast to this community/user
                if (isCommunityPersonal &&
                    CurrentCommunity.CreatorId != Current_User.UserId &&
                    !GetUserFacade.AreFriends (CurrentCommunity.CreatorId, Current_User.UserId) &&
                    !HasWritePrivileges ((int) SitePrivilege.ePRIVILEGE.USER_PROFILE_ADMIN))
                {
                    // Users are not friends, can not send blast to this person
                    BindBlasts (1);
                    ShowError ("You must be friends with this person to blast on their profile page.");
                    return;
                }
                else if (isCommunityBlast &&
                    CurrentCommunity.CreatorId != Current_User.UserId &&
                    !GetCommunityFacade.IsActiveCommunityMember (CurrentCommunity.CommunityId, Current_User.UserId) &&
                    !HasWritePrivileges ((int) SitePrivilege.ePRIVILEGE.COMMUNITY_PROFILE_ADMIN))
                {
                    // Users are not friends, can not send blast to this person
                    BindBlasts (1);
                    ShowError ("You must be a member of this World to blast on this World page.");
                    SetActiveTab ();
                    return;
                }
                else if (isCommunityBlast)
                {
                    //  Members must be fame level 20 or above
                    CommunityFacade communityFacade = new CommunityFacade ();
                    CommunityMember cm = communityFacade.GetCommunityMember (community.CommunityId, Current_User.UserId);

                    if (cm.AccountTypeId.Equals (3))
                    {
                        FameFacade fameFacade = new FameFacade ();
                        UserFame userFame = fameFacade.GetUserFame (Current_User.UserId, (int) FameTypes.World);

                        if (userFame.LevelId < global::Kaneva.BusinessLayer.Facade.Configuration.LevelIdToBlastCommunity)
                        {
                            BindBlasts (1);
                            ShowError ("Members must be World Fame Level " + global::Kaneva.BusinessLayer.Facade.Configuration.LevelIdToBlastCommunity.ToString () + " to blast to this World.");
                            return;
                        }
                        else if(!KanevaWebGlobals.CurrentUser.HasWOKAccount)
                        {
                            BindBlasts(1);
                            ShowError("Please login to the Kaneva client to enable blasting to this world.");
                            return;
                        }
                    }

                }

                // This code is used to insert the path for the default profile pic for a user if they
                // have not uploaded a profile pic yet.  Blast does not store the user's gender thus
                // why we have to prepopulate the thumbnail_path field with default profile image
                if ((isCommunityPersonal || (!isCommunityPersonal && !isCommunityBlast))
                    && thumbnailSmallPath.Length.Equals (0))
                {
                    if (Current_User.Gender.ToUpper ().Equals ("F"))
                    {
                        thumbnailSmallPath = "/KanevaIconFemale_sq.gif";
                    }
                    else
                    {
                        thumbnailSmallPath = "/KanevaIconMale_sq.gif";
                    }
                }

                if (!isCommunityPersonal && !isCommunityBlast &&
                    Current_User.FacebookSettings.UseFacebookProfilePicture &&
                    Current_User.FacebookSettings.FacebookUserId > 0)
                {
                    thumbnailSmallPath = GetFacebookProfileImageUrl (Current_User.FacebookSettings.FacebookUserId, "sq");
                }

                if (blastFacade.SendTextBlast (Current_User.UserId, CurrentCommunity.CommunityId,
                    CurrentCommunity.Name, isCommunityPersonal, senderName, Current_User.NameNoSpaces,
                    Server.HtmlEncode (txtBlast.Text), "", thumbnailSmallPath, isCommunityBlast).Equals (1))
                {
                    if (chkFB.Checked)
                    {
                        bool isError = false;
                        string errMsg = string.Empty;
                        GetSocialFacade.PostToCurrentUsersWall (KanevaWebGlobals.CurrentUser.FacebookSettings.AccessToken, txtBlast.Text, out isError, out errMsg);
                    }

                    // If blast is from a community and the owner of that community is sending the blast,
                    // then we email all the members of that community the new blast
                    if (!isCommunityPersonal && KanevaWebGlobals.CurrentUser.UserId == CurrentCommunity.CreatorId)
                    {
                        // Get list of members of community, filter out community owner which is the current user
                        PagedList<CommunityMember> members = GetCommunityFacade.GetCommunityMembers (CurrentCommunity.CommunityId, (UInt32) CommunityMember.CommunityMemberStatus.ACTIVE,
                            false, false, false, false, "u.user_id <> " + KanevaWebGlobals.CurrentUser.UserId, "", 1, int.MaxValue, true, "");

                        if (members.Count > 0)
                        {
                            foreach (CommunityMember member in members)
                            {
                                if (member.NotificationPreference.NotifyWorldBlasts && member.StatusId == (uint) User.eUSER_STATUS.REGVALIDATED)
                                {
                                    // They want emails for world/community blasts
                                    MailUtilityWeb.SendBlastNotificationEmail (isCommunityPersonal, member.Email, Server.HtmlEncode (txtBlast.Text), member.UserId, Current_User.UserId, CurrentCommunity.CommunityId);
                                }
                            }
                        }
                    }

                    //Send notification user if blasted to their profile
                    if (isCommunityPersonal && CurrentCommunity.CreatorId != Current_User.UserId)
                    {
                        UserFacade userFacade = new UserFacade ();
                        User userTo = userFacade.GetUser (CurrentCommunity.CreatorId);

                        if (userTo.NotificationPreference.NotifyBlastComments)
                        {
                            // They want emails for blasts
                            MailUtilityWeb.SendBlastNotificationEmail (isCommunityPersonal, userTo.Email, Server.HtmlEncode (txtBlast.Text), userTo.UserId, Current_User.UserId);
                        }
                    }

                    Page.ClientScript.RegisterClientScriptBlock (GetType (), "jsConfirm", "ShowConfirmMsg('spnAlertMsg', 5000);", true);

                    // GA Event Call
                    Page.ClientScript.RegisterClientScriptBlock (GetType (), "gaBlastEvent", "callGAEvent('Category','Blast' , 'TextBlast');", true);
                }
            }

            txtBlast.Text = "";
            BindBlasts (1);

            SetActiveTab ();
        }

        /// <summary>
        /// Change blast filter
        /// </summary>
        protected void drpActiveBlasts_SelectedIndexChanged (object sender, EventArgs e)
        {
            if (drpActiveBlasts.SelectedValue != "")
            {
                string commId = "";
                commId = CurrentCommunity.CommunityId > 0 ? "CommId" + CurrentCommunity.CommunityId.ToString () : "";

                Response.Cookies["blastFilter" + commId].Value = drpActiveBlasts.SelectedValue;
                Response.Cookies["blastFilter" + commId].Expires = DateTime.Now.AddYears (1);
            }

            BindBlasts (1);
        }

        /// <summary>   
        /// Repeater rptActiveBlasts ItemDataBound Event Handler
        /// </summary>
        protected void rptActiveBlasts_ItemDataBound (Object Sender, RepeaterItemEventArgs e)
        {
            if ((e.Item.ItemType == ListItemType.Item) || (e.Item.ItemType == ListItemType.AlternatingItem))
            {
                // get the blast details
                Blast blast = (Blast)e.Item.DataItem;

                PlaceHolder phComments = ((PlaceHolder) e.Item.FindControl ("phComments"));
               
                if (phComments != null)
                {
                    KlausEnt.KEP.Kaneva.usercontrols.BlastComments commentWidget = (KlausEnt.KEP.Kaneva.usercontrols.BlastComments) Page.LoadControl (ResolveUrl ("~/usercontrols/doodad/BlastComments.ascx")); ;
                    commentWidget.EntryId = Convert.ToInt64(blast.EntryId);
                    commentWidget.NumberOfComments = Convert.ToInt32(blast.NumberOfReplies);
                    commentWidget.ShowCommentsBox = CanUserAddComments;
                    phComments.Controls.Add (commentWidget);
                }

                // Configure the from > to part of the blast               
               // string toFromHtml = "";
               // string thumbHtml = "";

                //if (blast.CommunityId > 0)
                //{
                //    if (blast.IsCommunityPersonal)  // Personal Community (profile)
                //    {
                //        //string toName = "";
                //        // Achievement blast sent from a 3D App
                //        if (blast.BlastType == (int)BlastTypes.ACHIEVEMENT_3DAPP)
                //        {
                //            //toFromHtml = "<a href=\"" + GetBroadcastChannelUrl(blast.CommunityName.Replace(" ", "")) + "\">" + blast.CommunityName + "</a>";
                //            //toName = "<a href=\"" + GetPersonalChannelUrl(blast.SenderName.Replace(" ", "")) + "\">" + blast.SenderName + "</a>";
                //            //toFromHtml = toName + " <span style=\"font-weight:normal\">just earned a</span> Badge <span style=\"font-weight:normal\">in</span> " + toFromHtml;

                //            //thumbHtml = "<a href=\"" + GetPersonalChannelUrl(blast.CommunityName) + "\"><img src=\"" + GetProfileImageURL(blast.ThumbnailSmallPath, "sq", "M") + "\" border=\"0\" /></a>";
                //        }
                //        else
                //        {
                //            //if (CurrentCommunity.Name != blast.CommunityName && (blast.CommunityName != Current_User.Username))
                //            //{
                //            //    string communityUrl = "";
                //            //    if (blast.BlastType == (int)BlastTypes.REQUEST_3DAPP)
                //            //    {
                //            //        communityUrl = GetBroadcastChannelUrl(blast.CommunityName.Replace(" ", ""));
                //            //    }
                //            //    else
                //            //    {
                //            //        communityUrl = GetPersonalChannelUrl(blast.CommunityName.Replace(" ", ""));
                //            //    }
                //            //    toName = " &#187; <a href=\"" + communityUrl + "\">" + blast.CommunityName + "</a>";
                //            //}
                //            //toFromHtml = "<a href=\"" + GetPersonalChannelUrl(blast.SenderName.Replace(" ", "")) + "\">" + blast.SenderName + "</a>" + toName;
                //            //thumbHtml = "<a href=\"" + GetPersonalChannelUrl(blast.SenderName) + "\"><img src=\"" + GetProfileImageURL(blast.ThumbnailSmallPath, "sq", "M") + "\" border=\"0\" /></a>";
                //        }
                //    }
                //    else  // Non-Profile Community
                //    {
                //        if (blast.BlastType == (int)BlastTypes.RAVE_3D_APP)
                //        { // Rave 3dApp
                //            //string senderChannelURL = GetPersonalChannelUrl(blast.SenderName.Replace(" ", ""));
                //            //thumbHtml = "<a href=\"" + senderChannelURL + "\"><img src=\"" + GetProfileImageURL(blast.ThumbnailSmallPath, "sq", "M", true) + "\" border=\"0\" /></a>";

                //            //((HtmlContainerControl)e.Item.FindControl("h4ToFrom")).Visible = false;

                            

                //            // Configure play now link
                //            string rtsidRegex = Constants.cREQUEST_TRACKING_URL_PARAM + @"=([^""]+)""";
                //            string requestTypesSentId = Regex.Match(blast.DiaryEntry, rtsidRegex).Groups[1].Value;
                            
                //            LinkButton btnPlayNow = (LinkButton)e.Item.FindControl("btnPlayNow");
                //            int gameId = GetCommunityFacade.GetCommunityGameId((int)blast.CommunityId);
                //            ConfigureCommunityMeetMe3D(btnPlayNow, gameId, (int)blast.CommunityId, blast.CommunityName, requestTypesSentId);
                //            btnPlayNow.Visible = true;
                //        }
                //        else if (blast.BlastType == (int)BlastTypes.CAMERA_SCREENSHOT)
                //        { // Camera screenshot
                //            Community blastCommunity = GetCommunityFacade.GetCommunity((int)blast.CommunityId);
                            
                //            //string senderChannelURL = GetPersonalChannelUrl(blast.SenderName.Replace(" ", ""));
                //            //thumbHtml = "<a href=\"" + senderChannelURL + "\"><img src=\"" + GetProfileImageURL(blast.ThumbnailSmallPath, "sq", "M", true) + "\" border=\"0\" /></a>";

                //            //((HtmlContainerControl)e.Item.FindControl("h4ToFrom")).Visible = false;

                            

                //            // Configure play now link
                //            string rtsidRegex = Constants.cREQUEST_TRACKING_URL_PARAM + @"=([^""]+)""";
                //            string requestTypesSentId = Regex.Match(blast.DiaryEntry, rtsidRegex).Groups[1].Value;

                //            LinkButton btnPlayNow = (LinkButton)e.Item.FindControl("btnPlayNow");

                //            if (blastCommunity.IsPersonal > 0)
                //            {
                //                ConfigureUserMeetMe3D(btnPlayNow, blast.SenderName, requestTypesSentId);
                //            }
                //            else
                //            {
                //                int gameId = GetCommunityFacade.GetCommunityGameId((int)blast.CommunityId);
                //                ConfigureCommunityMeetMe3D(btnPlayNow, gameId, (int)blast.CommunityId, blast.CommunityName, requestTypesSentId);
                //            }

                //            btnPlayNow.Visible = true;
                //        }
                //        else if (blast.SenderName == blast.CommunityName)
                //        { // Sender is owner of community
                //            //toFromHtml = "<a href=\"" + GetBroadcastChannelUrl(blast.SenderName.Replace(" ", "")) + "\">" + blast.SenderName + "</a>";
                //            //thumbHtml = "<a href=\"" + GetBroadcastChannelUrl(blast.SenderName.Replace(" ", "")) + "\"><img src=\"" + GetBroadcastChannelImageURL(blast.ThumbnailSmallPath, "sm") + "\" border=\"0\" /></a>";
                //        }
                //        else
                //        {
                //            //string toName = "";
                //            //if (CurrentCommunity.CommunityId.Equals(0))
                //            //{
                //            //    toName = " &#187; <a href=\"" + GetBroadcastChannelUrl(blast.CommunityName.Replace(" ", "")) + "\">" + blast.CommunityName + "</a>";
                //            //}
                //            //toFromHtml = "<a href=\"" + GetPersonalChannelUrl(blast.SenderName.Replace(" ", "")) + "\">" + blast.SenderName + "</a>" + toName;
                //            //thumbHtml = "<a href=\"" + GetPersonalChannelUrl(blast.SenderName) + "\"><img src=\"" + GetProfileImageURL(blast.ThumbnailSmallPath, "sq", "M") + "\" border=\"0\" /></a>";
                //        }
                //    }
                //}
                //else if (blast.BlastType == (int)BlastTypes.CAMERA_SCREENSHOT)
                //{ // Camera screenshot
                //    Community blastCommunity = GetCommunityFacade.GetCommunity((int)blast.CommunityId);
                    
                //    //string senderChannelURL = GetPersonalChannelUrl(blast.SenderName.Replace(" ", ""));
                //    //thumbHtml = "<a href=\"" + senderChannelURL + "\"><img src=\"" + GetProfileImageURL(blast.ThumbnailSmallPath, "sq", "M", true) + "\" border=\"0\" /></a>";

                //    //((HtmlContainerControl)e.Item.FindControl("h4ToFrom")).Visible = false;

                    

                //    // Configure play now link
                //    string rtsidRegex = Constants.cREQUEST_TRACKING_URL_PARAM + @"=([^""]+)""";
                //    string requestTypesSentId = Regex.Match(blast.DiaryEntry, rtsidRegex).Groups[1].Value;

                //    LinkButton btnPlayNow = (LinkButton)e.Item.FindControl("btnPlayNow");

                //    if (blastCommunity.IsPersonal > 0)
                //    {
                //        ConfigureUserMeetMe3D(btnPlayNow, blast.SenderName, requestTypesSentId);
                //    }
                //    else
                //    {
                //        int gameId = GetCommunityFacade.GetCommunityGameId((int)blast.CommunityId);
                //        ConfigureCommunityMeetMe3D(btnPlayNow, gameId, (int)blast.CommunityId, blast.CommunityName, requestTypesSentId);
                //    }

                //    btnPlayNow.Visible = true;
                //}
                //else  // Sent from MyKaneva
                //{
                //    //toFromHtml = "<a href=\"" + GetPersonalChannelUrl(blast.SenderName.Replace(" ", "")) + "\">" + blast.SenderName + "</a>";
                //    //thumbHtml = "<a href=\"" + GetPersonalChannelUrl(blast.SenderName) + "\"><img src=\"" + GetProfileImageURL(blast.ThumbnailSmallPath, "sq", "M", true) + "\" border=\"0\" /></a>";
                //}

                //((HtmlContainerControl)e.Item.FindControl("spnToFrom")).InnerHtml = toFromHtml;
                //((HtmlContainerControl)e.Item.FindControl("divThumbnail")).InnerHtml = thumbHtml;

                // Block Link
                if (blast.BlastType.Equals ((int) BlastTypes.REQUEST_3DAPP))
                {
                    string strBlock = "<div class=\"time\">" +
                    "<a href=\"javascript:BlockBlast(0, " + blast.CommunityId + "," + blast.EntryId + ");\">Block " + blast.CommunityName + "</a>" +
                    " | " +
                    " <a href=\"javascript:BlockBlast(" + blast.SenderId + ", " + blast.CommunityId + "," + blast.EntryId + ");\">Block requests from " + blast.SenderName + "</a>" +
                    "</div>";

                    Literal litBlocking = ((Literal) e.Item.FindControl ("litBlocking"));
                    litBlocking.Text = strBlock;
                }
            }
        }

        /// <summary>
        /// GetDeleteVisibility
        /// </summary>
        /// <param name="blast"></param>
        /// <returns></returns>
        protected bool GetDeleteVisibility(Blast blast)
        {
            if (Convert.ToInt32(blast.SenderId).Equals(KanevaWebGlobals.CurrentUser.UserId) || UsersUtility.IsUserAdministrator() || blast.BlastType.Equals((int)BlastTypes.REQUEST_3DAPP))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// GetFromVisibility
        /// </summary>
        /// <param name="blast"></param>
        /// <returns></returns>
        protected bool GetFromVisibility(Blast blast)
        {
            if (blast.CommunityId > 0)
            {
                if (!blast.IsCommunityPersonal)  // Personal Community (profile)
                {
                    if (blast.BlastType == (int)BlastTypes.RAVE_3D_APP)
                    { // Rave 3dApp
                        return false;
                    }
                    else if (blast.BlastType == (int)BlastTypes.CAMERA_SCREENSHOT)
                    { // Camera screenshot
                        return false;
                    }
                }
            }
            else if (blast.BlastType == (int)BlastTypes.CAMERA_SCREENSHOT)
            { // Camera screenshot
                return false;
            }

            return true;
        }

        /// <summary>
        /// GetToFromText
        /// </summary>
        /// <param name="blast"></param>
        /// <returns></returns>
        protected string GetToFromText (Blast blast)
        {
            string toFromHtml = "";

            if (blast.CommunityId > 0)
            {
                if (blast.IsCommunityPersonal)  // Personal Community (profile)
                {
                    string toName = "";

                    // Achievement blast sent from a 3D App
                    if (blast.BlastType == (int)BlastTypes.ACHIEVEMENT_3DAPP)
                    {
                        toFromHtml = "<a href=\"" + GetBroadcastChannelUrl(blast.CommunityName.Replace(" ", "")) + "\">" + blast.CommunityName + "</a>";
                        toName = "<a href=\"" + GetPersonalChannelUrl(blast.SenderName.Replace(" ", "")) + "\">" + blast.SenderName + "</a>";
                        toFromHtml = toName + " <span style=\"font-weight:normal\">just earned a</span> Badge <span style=\"font-weight:normal\">in</span> " + toFromHtml;
                    }
                    else
                    {
                        if (CurrentCommunity.Name != blast.CommunityName && (blast.CommunityName != Current_User.Username))
                        {
                            string communityUrl = "";
                            if (blast.BlastType == (int)BlastTypes.REQUEST_3DAPP)
                            {
                                communityUrl = GetBroadcastChannelUrl(blast.CommunityName.Replace(" ", ""));
                            }
                            else
                            {
                                communityUrl = GetPersonalChannelUrl(blast.CommunityName.Replace(" ", ""));
                            }
                            toName = " &#187; <a href=\"" + communityUrl + "\">" + blast.CommunityName + "</a>";
                        }
                        toFromHtml = "<a href=\"" + GetPersonalChannelUrl(blast.SenderName.Replace(" ", "")) + "\">" + blast.SenderName + "</a>" + toName;
                    }
                }
                else  // Non-Profile Community
                {
                    if (blast.BlastType == (int)BlastTypes.RAVE_3D_APP)
                    { // Rave 3dApp
                    }
                    else if (blast.BlastType == (int)BlastTypes.CAMERA_SCREENSHOT)
                    { // Camera screenshot
            
                    }
                    else if (blast.SenderName == blast.CommunityName)
                    { // Sender is owner of community
                        toFromHtml = "<a href=\"" + GetBroadcastChannelUrl(blast.SenderName.Replace(" ", "")) + "\">" + blast.SenderName + "</a>";
                    }
                    else
                    {
                        string toName = "";
                        if (CurrentCommunity.CommunityId.Equals(0))
                        {
                            toName = " &#187; <a href=\"" + GetBroadcastChannelUrl(blast.CommunityName.Replace(" ", "")) + "\">" + blast.CommunityName + "</a>";
                        }
                        toFromHtml = "<a href=\"" + GetPersonalChannelUrl(blast.SenderName.Replace(" ", "")) + "\">" + blast.SenderName + "</a>" + toName;
                    }
                }
            }
            else if (blast.BlastType == (int)BlastTypes.CAMERA_SCREENSHOT)
            { // Camera screenshot

            }
            else  // Sent from MyKaneva
            {
                toFromHtml = "<a href=\"" + GetPersonalChannelUrl(blast.SenderName.Replace(" ", "")) + "\">" + blast.SenderName + "</a>";
            }

            return toFromHtml;
        }


        /// <summary>
        /// MeetMe3DLink
        /// </summary>
        /// <param name="blast"></param>
        /// <returns></returns>
        protected string MeetMe3DLink (Blast blast)
        {
            if (blast.CommunityId > 0)
            {
                if (!blast.IsCommunityPersonal)  // Personal Community (profile)
                {

                    // Non-Profile Community
                    if (blast.BlastType == (int)BlastTypes.RAVE_3D_APP)
                    { // Rave 3dApp

                        // Configure play now link
                        string rtsidRegex = Constants.cREQUEST_TRACKING_URL_PARAM + @"=([^""]+)""";
                        string requestTypesSentId = Regex.Match(blast.DiaryEntry, rtsidRegex).Groups[1].Value;

                        int gameId = 0; GetCommunityFacade.GetCommunityGameId((int)blast.CommunityId);
                        return CommunityMeetMe3DLink(gameId, (int)blast.CommunityId, blast.CommunityName, requestTypesSentId);
                    }
                    else if (blast.BlastType == (int)BlastTypes.CAMERA_SCREENSHOT)
                    { // Camera screenshot
                        Community blastCommunity = GetCommunityFacade.GetCommunity((int)blast.CommunityId);

                        // Configure play now link
                        string rtsidRegex = Constants.cREQUEST_TRACKING_URL_PARAM + @"=([^""]+)""";
                        string requestTypesSentId = Regex.Match(blast.DiaryEntry, rtsidRegex).Groups[1].Value;

                        if (blastCommunity.IsPersonal > 0)
                        {
                            return UserMeetMe3DLink(blast.SenderName, requestTypesSentId);
                        }
                        else
                        {
                            int gameId = GetCommunityFacade.GetCommunityGameId((int)blast.CommunityId);
                            return CommunityMeetMe3DLink(gameId, (int)blast.CommunityId, blast.CommunityName, requestTypesSentId);
                        }
                    }

                }
            }
            else if (blast.BlastType == (int)BlastTypes.CAMERA_SCREENSHOT)
            {
                // Camera screenshot
                Community blastCommunity = GetCommunityFacade.GetCommunity((int)blast.CommunityId);

                // Configure play now link
                string rtsidRegex = Constants.cREQUEST_TRACKING_URL_PARAM + @"=([^""]+)""";
                string requestTypesSentId = Regex.Match(blast.DiaryEntry, rtsidRegex).Groups[1].Value;

                if (blastCommunity.IsPersonal > 0)
                {
                    return UserMeetMe3DLink(blast.SenderName, requestTypesSentId);
                }
                else
                {
                    int gameId = GetCommunityFacade.GetCommunityGameId((int)blast.CommunityId);
                    return CommunityMeetMe3DLink(gameId, (int)blast.CommunityId, blast.CommunityName, requestTypesSentId);
                }
            }

            return "";
        }

        /// <summary>
        /// MeetMe3DVisible
        /// </summary>
        /// <param name="blast"></param>
        /// <returns></returns>
        protected bool MeetMe3DVisible (Blast blast)
        {
            if (blast.CommunityId > 0)
            {
                if (!blast.IsCommunityPersonal)  // Personal Community (profile)
                {

                    // Non-Profile Community
                    if (blast.BlastType == (int)BlastTypes.RAVE_3D_APP)
                    { // Rave 3dApp
                        return true;
                    }
                    else if (blast.BlastType == (int)BlastTypes.CAMERA_SCREENSHOT)
                    { // Camera screenshot
                        return true;
                    }

                }
            }
            else if (blast.BlastType == (int)BlastTypes.CAMERA_SCREENSHOT)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// GetThumbnail
        /// </summary>
        /// <param name="blast"></param>
        /// <returns></returns>
        protected string GetThumbnail (Blast blast)
        {
            string thumbHtml = "";

            if (blast.CommunityId > 0)
            {
                if (blast.IsCommunityPersonal)  // Personal Community (profile)
                {
                    // Achievement blast sent from a 3D App
                    if (blast.BlastType == (int)BlastTypes.ACHIEVEMENT_3DAPP)
                    {
                        thumbHtml = "<a href=\"" + GetPersonalChannelUrl(blast.CommunityName) + "\"><img src=\"" + GetProfileImageURL(blast.ThumbnailSmallPath, "sq", "M") + "\" border=\"0\" /></a>";
                    }
                    else
                    {
                        thumbHtml = "<a href=\"" + GetPersonalChannelUrl(blast.SenderName) + "\"><img src=\"" + GetProfileImageURL(blast.ThumbnailSmallPath, "sq", "M") + "\" border=\"0\" /></a>";
                    }
                }
                else  // Non-Profile Community
                {
                    if (blast.BlastType == (int)BlastTypes.RAVE_3D_APP)
                    { // Rave 3dApp
                        string senderChannelURL = GetPersonalChannelUrl(blast.SenderName.Replace(" ", ""));
                        thumbHtml = "<a href=\"" + senderChannelURL + "\"><img src=\"" + GetProfileImageURL(blast.ThumbnailSmallPath, "sq", "M", true) + "\" border=\"0\" /></a>";
                    }
                    else if (blast.BlastType == (int)BlastTypes.CAMERA_SCREENSHOT)
                    { // Camera screenshot
                        string senderChannelURL = GetPersonalChannelUrl(blast.SenderName.Replace(" ", ""));
                        thumbHtml = "<a href=\"" + senderChannelURL + "\"><img src=\"" + GetProfileImageURL(blast.ThumbnailSmallPath, "sq", "M", true) + "\" border=\"0\" /></a>";
                    }
                    else if (blast.SenderName == blast.CommunityName)
                    { // Sender is owner of community
                        thumbHtml = "<a href=\"" + GetBroadcastChannelUrl(blast.SenderName.Replace(" ", "")) + "\"><img src=\"" + GetBroadcastChannelImageURL(blast.ThumbnailSmallPath, "sm") + "\" border=\"0\" /></a>";
                    }
                    else
                    {
                        thumbHtml = "<a href=\"" + GetPersonalChannelUrl(blast.SenderName) + "\"><img src=\"" + GetProfileImageURL(blast.ThumbnailSmallPath, "sq", "M") + "\" border=\"0\" /></a>";
                    }
                }
            }
            else if (blast.BlastType == (int)BlastTypes.CAMERA_SCREENSHOT)
            { // Camera screenshot
                string senderChannelURL = GetPersonalChannelUrl(blast.SenderName.Replace(" ", ""));
                thumbHtml = "<a href=\"" + senderChannelURL + "\"><img src=\"" + GetProfileImageURL(blast.ThumbnailSmallPath, "sq", "M", true) + "\" border=\"0\" /></a>";

            }
            else  // Sent from MyKaneva
            {
                thumbHtml = "<a href=\"" + GetPersonalChannelUrl(blast.SenderName) + "\"><img src=\"" + GetProfileImageURL(blast.ThumbnailSmallPath, "sq", "M", true) + "\" border=\"0\" /></a>";
            }

            return thumbHtml;
        }

        protected override void OnInit (EventArgs e)
        {
            pgActiveBlasts.PageChanged += new PageChangeEventHandler (pgActiveBlasts_PageChange);

            base.OnInit (e);
        }

        #endregion Event Handlers


        #region Properties

        public Community CurrentCommunity
        {
            get
            {
                try
                {
                    if (this.community.CommunityId == 0)
                    {
                        if (HttpContext.Current.Items["community"] != null)
                        {
                            this.community = (Community) (HttpContext.Current.Items["community"]);
                        }
                    }
                }
                catch
                {
                    return new Community (); ;
                }

                return this.community;
            }
        }

        private bool IsUserActiveCommunityMember
        {
            get { return this.isMember; }
            set { isMember = value; }
        }
        private bool IsUserCummunityOwner
        {
            get { return Current_User.UserId == CurrentCommunity.CreatorId; }
        }
        private bool IsUserFriend
        {
            get { return this.isFriend; }
            set { isFriend = value; }
        }
        private bool CanUserAddComments
        {
            get
            {
                if ((IsUserActiveCommunityMember && KanevaWebGlobals.CurrentUser.HasWOKAccount) || IsUserCummunityOwner) { return true; }
                else if (IsUserCommunityAdmin ()) { return true; }
                else if (CurrentCommunity.CommunityId == 0) { return true; }
                else if (CurrentCommunity.IsPersonal.Equals (1) && isFriend) { return true; }
                else { return false; }
            }
        }

        #endregion Properties


        #region Declarations

        // private 
        private User Current_User = KanevaWebGlobals.CurrentUser;
        private Community community = new Community ();
        private bool isMember = false;
        private bool isFriend = false;

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

        #endregion Declerations
    }
}
