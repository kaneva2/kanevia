<%@ Page language="c#" Codebehind="default.aspx.cs" ValidateRequest="False" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.mykaneva.blast._default" %>
<%@ Register TagPrefix="CE" Namespace="CuteEditor" Assembly="CuteEditor" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>Kaneva: Blast!</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
    <meta name="CODE_LANGUAGE" content="C#" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="../../css/new.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="../../jscript/OffSiteBlasts.js"></script>
	<script type="text/javascript" >
	<!--
		function CloseWindow()
		{
			this.close();
		}
	-->
	</script>
  </head>
  <body onload="GetOffsiteBlastInfo()">
    <form id="Form1" method="post" runat="server">
		<input type="hidden" runat="server" id="sourceURL" name="sourceURL" />
		<input type="hidden" runat="server" id="sourceLinks" name="firstLoad" />
		<input type="hidden" runat="server" id="tabChoice" name="tabChoice" />
		<input type="hidden" runat="server" id="sourceTitle" name="sourceTitle" />
		<input type="hidden" runat="server" id="sourceSelectedText" name="sourceSelectedText" />
		<input type="hidden" runat="server" id="sourceImages" name="sourceImages" />
		<input type="hidden" runat="server" id="sourceMovies" name="sourceMovies" />
		<table border="0" cellspacing="0" cellpadding="0" align="center" style="height:100%">
			<tr id="loginRow" runat="server" visible="false">
				<td align="center">
				    <img runat="server" src="~/images/blasttokaneva_logo.gif" alt="Kaneva" /><br /><br />
					<table width="280" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="6" class="frTopLeft"></td>
							<td colspan="3" class="frTop"></td>
							<td width="7" class="frTopRight"></td>
						</tr>
						<tr>
							<td  class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img1"/></td>
							<td width="10" valign="top"  class="frBgInt1"></td>
							<td valign="middle" align="center" class="frBgInt1"><img runat="server" src="~/images/spacer.gif" width="1" height="10" ID="Img2"/>
								<table width="100%" height="89" border="0" cellpadding="0" cellspacing="0">
									<!--DWLayoutTable-->
									<tr>
										<td width="10" rowspan="9"><img runat="server" src="~/images/spacer.gif" width="1" height="5" ID="Img3"/></td>
										<td height="0" colspan="2"></td>
										<td width="10"></td>
										<td width="65"></td>
										<td width="10" rowspan="9"><img runat="server" src="~/images/spacer.gif" width="1" height="5" ID="Img4"/></td>
									</tr>
									<tr>
										<td height="19" colspan="2" style="padding-left: 10px"><input type="text" style="width: 160px;" class="formKanevaText" maxlength="100" id="txtUserName" runat="server" name="txtUserName"></td>
										<td><img runat="server" src="~/images/spacer.gif" width="10" height="5" ID="Img5"/></td>
										<td class="insideBoxText11" align="left">email</td>
									</tr>
									<tr>
										<td height="5" colspan="4"><img runat="server" src="~/images/spacer.gif" width="10" height="5" ID="Img6"/></td>
									</tr>
									<tr>
										<td height="19" colspan="2" style="padding-left: 10px"><input type="password" style="width: 160px;" class="formKanevaText" maxlength="30" id="txtPassword" runat="server" name="txtPassword"></td>
										<td></td>
										<td class="insideBoxText11" align="left">password</td>
									</tr>
									<tr>
										<td height="0" colspan="2"></td>
										<td></td>
										<td></td>
									</tr>
									<tr>
										<td height="15" colspan="4"><img runat="server" src="~/images/spacer.gif" width="10" height="15" ID="Img7"/></td>
									</tr>
									<tr>
										<td height="10" colspan="4"><img runat="server" src="~/images/spacer.gif" width="10" height="10" ID="Img8"/></td>
									</tr>
									<tr>
										<td height="0" colspan="2"></td>
										<td></td>
										<td></td>
									</tr>
									<tr>
										<td height="1">
										</td>
										<td style="padding-left: 10px">
											<asp:imagebutton id="imgLogin" runat="server" causesvalidation="False" alternatetext="Sign in to Kaneva" imageurl="~/images/buttons/btnSingIn.gif" onclick="imgLogin_Click" width="58" height="24" border="0"/>
										</td>
										<td>
										</td>
										<td style="padding-right: 10px">
											<!--<a runat="server" id="aLostPassword" style="vertical-align:top;"><img runat="server" src="~/images/buttons/btnForgotPassword.gif" width="115" height="22" border="0" id="Img10"/></a>-->
										</td>
										<td></td>
										<td></td>
									</tr>
								</table>
							</td>
							<td width="10" valign="top" class="frBgInt1"></td>
							<td class="frBorderRight"><img alt="spacer" runat="server" src="~/images/spacer.gif" width="1" height="1" ID="Img9"/></td>
						</tr>
						<tr>
							<td class="frBottomLeft"></td>
							<td colspan="3" class="frBottom"></td>
							<td class="frBottomRight"></td>
						</tr>
					</table>
				</td>								
			</tr>
			<tr id="blastSuccess" runat="server" visible="true">
				<td>
					<table width="280" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td align="center" >
								<label id="pblastDone" align="center" style="font-size: 64px">DONE!</label>
							</td>
						</tr>
						<tr>
							<td align="center"><a runat="server" href="#" onclick=" CloseWindow();" ID="hypBlastDone">Close this window</a> or wait <label id="lb_time" /></td>
						</tr>
					</table>
				</td>								
			</tr>
			<tr id="blastRow" runat="server" visible="false">
				<td width="500" align="left">
					<label id="loading" align="center" style="font-size:64px; height:500px; padding-top:160px; padding-left:80px; display:block">Loading...</label>
					<table border="0" cellspacing="0" cellpadding="0" >
						<tr>
							<td class="small italic" align="left">
								<div class="frame">
								<span class="ct"><span class="cl"></span></span>
									<table id="tabNav" cellSpacing="0" cellPadding="0" border="0">
										<tr>
											<td valign="top">
												<ul>
													<li runat="server" id="liText"><asp:linkbutton id="lbBlastText" runat="server" CommandName="1" OnCommand="ChangeBlastType_Command">Text</asp:linkbutton></li>
													<li runat="server" id="liVideo"><asp:linkbutton id="lbBlastVideo" runat="server" CommandName="13" OnCommand="ChangeBlastType_Command">Video</asp:linkbutton></li>
													<li runat="server" id="liPhoto"><asp:linkbutton id="lbBlastPhoto" runat="server" CommandName="14" OnCommand="ChangeBlastType_Command">Photo</asp:linkbutton></li>
													<li runat="server" id="liLink"><asp:linkbutton id="lbBlastLink" runat="server" CommandName="15" OnCommand="ChangeBlastType_Command">Link</asp:linkbutton></li>
												</ul>	
												<input type="hidden" id="hidBlastType" value="1" runat="server" name="hidBlastType"/>
											</td>
										</tr>
									</table>	
									<span class="cb"><span class="cl"></span></span>
								</div>
							</td>
						</tr>
					</table>
					<div class="module  fullpage">
					<span class="ct"><span class="cl"></span></span>

					<!-- Text Blast -->
					<table id="tblBlastText" runat="server" border="0" cellspacing="0" cellpadding="0" width="67%" align="center" height="80%">
						<tr>
							<td align="center" valign="top">
								<CE:Editor style="margin:0 0 5 0;" id="txtBlast" AllowPasteHtml="False" EditorOnPaste="PastePureText" ShowBottomBar="False" EnableContextMenu="False" BackColor="#ededed" runat="server" MaxTextLength="2000" width="450px" Height="280px" ShowHtmlMode="false" ConfigurationPath="~/CuteSoft_Client/CuteEditor/Configuration/AutoConfigure/blast.config" AutoConfigure="None" ></CE:Editor>
							</td>
						</tr>
					</table>
					
					<!-- Video Blast -->
					<table id="tblBlastVideo" runat="server" Visible="False" border="0" cellspacing="0" cellpadding="0" width="67%" align="center" height="80%">
						<tr>
							<td align="center" valign="top">
								<table border="0" cellspacing="0" cellpadding="0" width="67%" align="center">
									<tr>
										<td><span class="large bold">Video Embed Code</span><br><span class="small">This can be embed code from Kaneva or YouTube</span></td>
									</tr>
									<tr>
										<td>
											<asp:TextBox ID="txtVideoEmbed" TextMode="multiline" Rows="4" Cols="50" style="margin:0 0 5 0; width:450px;" MaxLength="140" runat="server"/>
										</td>
									</tr>
								</table>
								<br><br>	
								<table border="0" cellspacing="0" cellpadding="0" width="67%" align="center">
									<tr>
										<td style="padding: 0 0 8px 0;"><span class="large bold">Caption</span></td>
									</tr>
									<tr>
										<td>
											<CE:Editor style="margin:0 0 5 0;" id="txtVideoCaption" AllowPasteHtml="False" EditorOnPaste="PastePureText" ShowBottomBar="False" EnableContextMenu="False" BackColor="#ededed" runat="server" MaxTextLength="2000" width="450px" Height="130px" ShowHtmlMode="false" ConfigurationPath="~/CuteSoft_Client/CuteEditor/Configuration/AutoConfigure/blast.config" AutoConfigure="None" ></CE:Editor>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					
					<!-- Photo Blast -->
					<table id="tblBlastPhoto" runat="server" Visible="False" border="0" cellspacing="0" cellpadding="0" width="67%" align="center" height="80%">
						<tr>
							<td align="center" valign="top">
							
								<table id="tblURL" border="0" cellspacing="0" cellpadding="0" width="67%" align="center">
									<tr>
										<td><span class="large bold">Photo URL</span></td>
									</tr>
									<tr>
										<td>
											<asp:TextBox ID="txtPhotoURL" class="formKanevaText" style="width:450px" MaxLength="300" runat="server"/><br>	
											<small>Example: http://www.kaneva.com/images/klogo.gif</small>
										</td>
									</tr>
								</table>
								
								<table border="0" cellspacing="0" cellpadding="0" width="100%" align="center">
									<tr>
										<td style="padding: 0px 0 0 0;">
											<div id="photoList" runat="server" style="background-color:#dddddd; overflow:auto; width:100%; height:100px;" >
											</div><br>
										</td>
									</tr>
								</table>
								
								<table border="0" cellspacing="0" cellpadding="0" width="67%" align="center">
									<tr>
										<td style="padding: 8px 0 0 0;"><span class="large bold">Caption</span></td>
									</tr>
									<tr>
										<td>
											<CE:Editor style="margin:0 0 5 0;" id="txtPhotoCaption" AllowPasteHtml="False" EditorOnPaste="PastePureText" ShowBottomBar="False" EnableContextMenu="False" BackColor="#ededed" runat="server" MaxTextLength="2000" width="450px" Height="100px" ShowHtmlMode="false" ConfigurationPath="~/CuteSoft_Client/CuteEditor/Configuration/AutoConfigure/blast.config" AutoConfigure="None" ></CE:Editor>
										</td>
									</tr>
								</table>
								
							</td>
						</tr>
					</table>
					
					<!-- Link Blast -->
					<table id="tblBlastLink" runat="server" Visible="False" border="0" cellspacing="0" cellpadding="0" width="67%" align="center" height="80%">
						<tr>
							<td align="center" valign="top">																					
								<table border="0" cellspacing="0" cellpadding="0" width="67%" align="center">
									<tr>
										<td><span class="large bold">Name</span></td>
									</tr>
									<tr>
										<td>
											<asp:TextBox ID="txtLinkName" class="formKanevaText" style="width:450px" MaxLength="40" runat="server"/>
										</td>
									</tr>
									<tr>
										<td style="padding: 8px 0 0 0;"><span class="large bold">URL</span></td>
									</tr>
									<tr>
										<td>
											<asp:TextBox ID="txtLinkURL" class="formKanevaText" style="width:400px" MaxLength="200" runat="server"/>
										</td>
									</tr>
									<tr>
										<td style="padding: 8px 0 0 0;"><span class="large bold">Description</span></td>
									</tr>
									<tr>
										<td>
											<CE:Editor style="margin:0 0 5 0;" id="txtLinkDescription" AllowPasteHtml="False" EditorOnPaste="PastePureText" ShowBottomBar="False" EnableContextMenu="False" BackColor="#ededed" runat="server" MaxTextLength="2000" width="450px" Height="150px" ShowHtmlMode="false" ConfigurationPath="~/CuteSoft_Client/CuteEditor/Configuration/AutoConfigure/blast.config" AutoConfigure="None" ></CE:Editor>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					
					<table class="clearboth" border="0" cellspacing="0" cellpadding="0" width="67%" align="center">
						<tr>
							<td><a runat="server" href="#" onclick=" CloseWindow();" ID="A7">Cancel</a></td>
							<td class="large" align="right">
								<asp:button style="width: 100px;" id="btnBlast" onclick="btnBlast_Click" runat="server" Text="Send Blast"></asp:button>
							</td>
						</tr>
					</table>
																																																													
					<span class="cb"><span class="cl"></span></span>
					</div>
				
				</td>
			</tr>
		</table>
    </form>
	
  </body>
</html>
