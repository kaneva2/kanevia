///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva.channel;
using log4net;
using MagicAjax;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for blastPrefs.
	/// </summary>
	public class blastPrefs : StoreBasePage
	{

		#region Declarations

		protected System.Web.UI.WebControls.CheckBox chkBlogcomments;
		protected System.Web.UI.WebControls.CheckBox chkProfiles;
		protected System.Web.UI.WebControls.CheckBox chkComment;
		protected System.Web.UI.WebControls.CheckBox chkForum;
		protected System.Web.UI.WebControls.CheckBox chkJoinCommunity;
		protected System.Web.UI.WebControls.CheckBox chkSendGift;
		protected System.Web.UI.WebControls.CheckBox chkShareNone;
		protected System.Web.UI.WebControls.CheckBox chkShowBlogcomments;
		protected System.Web.UI.WebControls.CheckBox chkShowProfiles;
		protected System.Web.UI.WebControls.CheckBox chkShowComment;
		protected System.Web.UI.WebControls.CheckBox chkShowForum;
		protected System.Web.UI.WebControls.CheckBox chkShowJoinCommunity;
		protected System.Web.UI.WebControls.CheckBox chkShowSendGift;
		protected System.Web.UI.WebControls.CheckBox chkShowNone;
		protected System.Web.UI.WebControls.Button btnCancel;
		protected System.Web.UI.WebControls.Button btnUpdate;
		protected System.Web.UI.WebControls.Label messages;
		protected System.Web.UI.HtmlControls.HtmlImage Img5;

		// Logger
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

		#endregion
	
		protected blastPrefs () 
		{
			Title = "Blast Preferences";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			if (!Request.IsAuthenticated)
			{
				Response.Redirect (this.GetLoginURL ());
			}

			//if(IsUserAllowedToEdit ())
			if (!IsPostBack)
			{
				messages.Text = "";
				BindData();
			}

		}


		#region Event Handlers

		private void chkShareNone_CheckedChanged(object sender, System.EventArgs e)
		{
			privacy_SetAllChecked(!chkShareNone.Checked);
		}

		private void chkShowNone_CheckedChanged(object sender, System.EventArgs e)
		{
			visibility_SetAllChecked(!chkShowNone.Checked);
		}


		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			// Go back to my kaneva
			Response.Redirect (ResolveUrl ("~/default.aspx"));				
		}

		private void visibility_SetAllChecked(bool selectAll)
		{
			if(selectAll)
			{
				chkShowBlogcomments.Checked = false;
				chkShowBlogcomments.Enabled = true;
				chkShowProfiles.Checked = false;
				chkShowProfiles.Enabled = true;
				chkShowComment.Checked = false;
				chkShowComment.Enabled = true;
				chkShowForum.Checked = false;
				chkShowForum.Enabled = true;
				chkShowJoinCommunity.Checked = false;
				chkShowJoinCommunity.Enabled = true;
			}
			else
			{
				chkShowBlogcomments.Checked = false;
				chkShowBlogcomments.Enabled = false;
				chkShowProfiles.Checked = false;
				chkShowProfiles.Enabled = false;
				chkShowComment.Checked = false;
				chkShowComment.Enabled = false;
				chkShowForum.Checked = false;
				chkShowForum.Enabled = false;
				chkShowJoinCommunity.Checked = false;
				chkShowJoinCommunity.Enabled = false;
			}
		}

		private void privacy_SetAllChecked(bool selectAll)
		{
			if(selectAll)
			{
				chkBlogcomments.Checked = false;
				chkBlogcomments.Enabled = true;
				chkProfiles.Checked = false;
				chkProfiles.Enabled = true;
				chkComment.Checked = false;
				chkComment.Enabled = true;
				chkForum.Checked = false;
				chkForum.Enabled = true;
				chkJoinCommunity.Checked = false;
				chkJoinCommunity.Enabled = true;
			}
			else
			{
				chkBlogcomments.Checked = false;
				chkBlogcomments.Enabled = false;
				chkProfiles.Checked = false;
				chkProfiles.Enabled = false;
				chkComment.Checked = false;
				chkComment.Enabled = false;
				chkForum.Checked = false;
				chkForum.Enabled = false;
				chkJoinCommunity.Checked = false;
				chkJoinCommunity.Enabled = false;
			}
		}

		private void btnUpdate_Click(object sender, System.EventArgs e)
		{
			char[] blastShowBinary = new char[9];
			char[] blastPrivacyBinary = new char[9];

			//create binary representation
			//*NOTE*/ this could have been done throough simple string concatination but I wanted to ensure that 
			//items were placed in the correct spot in the sequence. This way helps to ensure that 
			blastPrivacyBinary[(int)Constants.eBLAST_PREFERENCES.PROFILES] = (Convert.ToInt32(chkProfiles.Checked).ToString())[0];
			blastPrivacyBinary[(int)Constants.eBLAST_PREFERENCES.BLOG_COMMENT] = (Convert.ToInt32(chkBlogcomments.Checked).ToString())[0];
			blastPrivacyBinary[(int)Constants.eBLAST_PREFERENCES.COMMENTS] = (Convert.ToInt32(chkComment.Checked).ToString())[0];
			blastPrivacyBinary[(int)Constants.eBLAST_PREFERENCES.FORUMS] = (Convert.ToInt32(chkForum.Checked).ToString())[0];
            blastPrivacyBinary[(int) Constants.eBLAST_PREFERENCES.ADD_FRIEND] = '0';
            blastPrivacyBinary[(int)Constants.eBLAST_PREFERENCES.JOIN_COMMUNITY] = (Convert.ToInt32(chkJoinCommunity.Checked).ToString())[0];
            blastPrivacyBinary[(int)Constants.eBLAST_PREFERENCES.RAVE_HOME] = '0';
            blastPrivacyBinary[(int) Constants.eBLAST_PREFERENCES.RAVE_HANGOUT] = '0';
            blastPrivacyBinary[(int)Constants.eBLAST_PREFERENCES.SEND_GIFT] = '0';

			blastShowBinary[(int)Constants.eBLAST_PREFERENCES.PROFILES] = (Convert.ToInt32(chkShowProfiles.Checked).ToString())[0];
			blastShowBinary[(int)Constants.eBLAST_PREFERENCES.BLOG_COMMENT] = (Convert.ToInt32(chkShowBlogcomments.Checked).ToString())[0];
			blastShowBinary[(int)Constants.eBLAST_PREFERENCES.COMMENTS] = (Convert.ToInt32(chkShowComment.Checked).ToString())[0];
			blastShowBinary[(int)Constants.eBLAST_PREFERENCES.FORUMS] = (Convert.ToInt32(chkShowForum.Checked).ToString())[0];
            blastShowBinary[(int) Constants.eBLAST_PREFERENCES.ADD_FRIEND] = '0';
            blastShowBinary[(int) Constants.eBLAST_PREFERENCES.JOIN_COMMUNITY] = (Convert.ToInt32 (chkShowJoinCommunity.Checked).ToString ())[0];
            blastShowBinary[(int) Constants.eBLAST_PREFERENCES.RAVE_HOME] = '0';
            blastShowBinary[(int) Constants.eBLAST_PREFERENCES.RAVE_HANGOUT] = '0';
            blastShowBinary[(int) Constants.eBLAST_PREFERENCES.SEND_GIFT] = '0';

			//create integer representation
			int blastPrivacyPreferences = Convert.ToInt32(new string(blastPrivacyBinary), 2);
			int blastShowPreferences = Convert.ToInt32(new string(blastShowBinary), 2);

			// save to data base
            UserFacade userFacade = new UserFacade();
            userFacade.UpdateBlastPreferences(GetUserId(), blastPrivacyPreferences, blastShowPreferences);
				
			// Go back to my kaneva
			Response.Redirect (ResolveUrl ("~/default.aspx"));		

		}

		#endregion

		#region HelperFunctions

		/*private bool IsUserAllowedToEdit()
		{
			return IsAdministrator() || CommunityUtility.IsCommunityModerator(
				_channelId, GetUserId());
		}*/

		private void PopulatePrivacyPreferences(UInt32 setting, string preferences, User user)
		{

			if(setting == 0)
			{
				this.chkShareNone.Checked = true;
				privacy_SetAllChecked(false);
			}
			else
			{
				//populate the check Boxes
				chkBlogcomments.Checked = user.BlastPrivacyBlogs;
				chkProfiles.Checked = user.BlastPrivacyProfiles;
				chkComment.Checked = user.BlastPrivacyComments;
				chkForum.Checked = user.BlastPrivacyForums;
				chkJoinCommunity.Checked = user.BlastPrivacyJoinCommunity;
				chkSendGift.Checked = user.BlastPrivacySendGift;
			}
		}

        private void PopulateShowPreferences(UInt32 setting, string preferences, User user)
		{

			if(setting == 0)
			{
				this.chkShowNone.Checked = true;
				visibility_SetAllChecked(false);
			}
			else
			{
				//populate the check Boxes
				chkShowBlogcomments.Checked = user.BlastShowBlogs;
				chkShowProfiles.Checked = user.BlastShowProfiles;
				chkShowComment.Checked = user.BlastShowComments;
				chkShowForum.Checked = user.BlastShowForums;
				chkShowJoinCommunity.Checked = user.BlastShowJoinCommunity;
				chkShowSendGift.Checked = user.BlastShowSendGift;
			}
		}

		#endregion


		#region Primary Functions
		
		private void BindData()
		{
			User user = KanevaWebGlobals.CurrentUser;
			if(user == null)
			{
				messages.ForeColor = System.Drawing.Color.DarkRed;
				messages.Text = "Sorry. We were unable to retreive your blast privacy configuration settings.";
				MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('messages', 5000);");
			}
			else
			{
				try
				{
					PopulatePrivacyPreferences(user.BlastPrivacyPermissions, user.BlastPrivacyPermissionsBase2, user);
					PopulateShowPreferences(user.BlastShowPermissions, user.BlastShowPermissionsBase2, user);
				}
				catch(FormatException ex)
				{
					m_logger.Error ("Blast Settings Error during settings retreival.", ex);
					messages.ForeColor = System.Drawing.Color.DarkRed;
					messages.Text = "Sorry. We were unable to retreive your blast privacy configuration settings.";
					MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('messages', 5000);");
				}

			}

		}

		#endregion

		#region Web Form Designer generated code

		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.chkShareNone.CheckedChanged += new System.EventHandler(this.chkShareNone_CheckedChanged);
			this.chkShowNone.CheckedChanged += new System.EventHandler(this.chkShowNone_CheckedChanged);
			this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			this.Load += new System.EventHandler(this.Page_Load);
		}

		#endregion




	}
}
