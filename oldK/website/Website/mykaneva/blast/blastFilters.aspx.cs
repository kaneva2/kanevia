///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;


namespace KlausEnt.KEP.Kaneva.mykaneva.blast
{
	public partial class blastFilters : StoreBasePage
	{
		int blockCommunityId = 0;
		
		protected void Page_Load(object sender, EventArgs e)
		{
			// They must be logged in
			if (!Request.IsAuthenticated)
			{
				Response.Redirect(GetLoginURL());
				return;
			}

			if (!IsPostBack)
			{
				loadData();

				doActions();
			}

			Title = "Kaneva - My Account - Blast Preferences";

			// Set Nav
			HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
			HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.ACCOUNT;
			HeaderNav.MyAccountNav.ActiveTab = NavAccount.TAB.PREFERENCES;
			HeaderNav.SetNavVisible(HeaderNav.MyKanevaNav, 2);
			HeaderNav.SetNavVisible(HeaderNav.MyAccountNav);
		}

		protected void doActions()
		{
			

		}

		protected void btnBlockCommunity_Click(object sender, EventArgs e)
		{			
			//blockCommunity(txtBlockCommunity.Text, false);
			//lblStatus.Text = "Block Community Click";
		}

		protected void btnBlockPerson_Click(object sender, EventArgs e)
		{			
			//blockCommunity(txtBlockCommunity.Text, true);
			//lblStatus.Text = "Block Person Click";
		}

		protected void blockCommunity(string blockCommunityName, bool isPersonal)
		{
			CommunityFacade communityFacade = new CommunityFacade();
			blockCommunityId = communityFacade.GetCommunityIdFromName(blockCommunityName, isPersonal);

			if (blockCommunityId == 0)
			{
				lblStatus.Text = "Unable to find community.";
			}
			else
			{
				lblStatus.Text += "<br /> blockCommunityId = " + blockCommunityId.ToString();

				int blockUserId = 0;
				
				UserFacade userFacade = new UserFacade();
				userFacade.AddBlastBlock(KanevaWebGlobals.CurrentUser.UserId, blockUserId, blockCommunityId);
			}
			// Response.Redirect("blastFilters.aspx");

		}

		protected void loadData()
		{			
			int userId = KanevaWebGlobals.CurrentUser.UserId;
			string filter = "";
			string orderby = "";
			int pageNumber = 1;
			int pageSize = 1000;

			UserFacade userFacade = new UserFacade();
			
			filter = " blocked_user_id = 0 ";
			rptBlockedCommunities.DataSource = userFacade.GetBlastBlocks(userId, filter, orderby, pageNumber, pageSize);
			rptBlockedCommunities.DataBind();

			filter = " blocked_user_id > 0 ";
			rptBlockedPlayerCommunities.DataSource = userFacade.GetBlastBlocks(userId, filter, orderby, pageNumber, pageSize);
			rptBlockedPlayerCommunities.DataBind();

		}

		protected void rptBlockedCommunities_ItemCommand(object source, RepeaterCommandEventArgs e)
		{						
			string command = e.CommandName;
			lblStatus.Text = "";
			int blockedUserId = 0;
			int blockedCommunityId = 0;

			try
			{

				HtmlInputHidden hidBlockedCommunityId = (HtmlInputHidden)e.Item.FindControl("hidBlockedCommunityId");
				blockedCommunityId = Convert.ToInt32(hidBlockedCommunityId.Value);

				HtmlInputHidden hidBlockedUserId = (HtmlInputHidden)e.Item.FindControl("hidBlockedUserId");
				blockedUserId = Convert.ToInt32(hidBlockedUserId.Value);

				if (command.Equals("cmdUnblock"))
				{

					//unblock the community
					UserFacade userFacade = new UserFacade();
					int statusRet = userFacade.DeleteBlastBlock(KanevaWebGlobals.CurrentUser.UserId, blockedUserId, blockedCommunityId);
															
					//refresh the data
					loadData();
					
				}
			}
			catch
			{
				lblStatus.Text = "Ut oh... There was a problem processing the unblock request. Please try again later :(";
			}
						
		}



	}
}
