<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GetBlasts.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.GetBlasts" %>

<asp:Literal id="litJS" runat="server"></asp:Literal>

<div id="activityList">														
	<asp:repeater EnableViewState="True" id="rptPassiveBlasts" runat="server" onitemdatabound="rptPassiveBlasts_ItemDataBound">
		<itemtemplate>
			<ul class="activity" id="ulContainer" runat="server"></ul>											  
		</itemtemplate>  
	</asp:Repeater>  
</div>