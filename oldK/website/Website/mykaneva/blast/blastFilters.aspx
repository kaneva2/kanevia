<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="blastFilters.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.mykaneva.blast.blastFilters" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<link href="../../css/new.css" type="text/css" rel="stylesheet">
<link href="../../css/mykaneva/my3dApps.css" type="text/css" rel="stylesheet">

<style type="text/css">
.divBlock {

	border: 1px solid #cccccc;
	height: 250px;
	width: 335px;
	overflow: scroll;
}

h2 {
	font-weight:bold;width:330px;height:14px;margin-bottom:10px;background-color:#ccc;text-align:left;padding:6px;
}

.commName {
	width: 100px; 
	overflow: hidden;
}
.userName {
	width: 100px;
	overflow: hidden;
}
</style>

<table border="0" cellspacing="0" cellpadding="0" class="newcontainer" align="center">
	<tr>
		<td>
			<table border="0" cellpadding="0" cellspacing="0" width="100%" class="newcontainerborder">
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
				<tr>
					<td class=""><img id="Img1" runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
					<td valign="top" class="newdatacontainer">

	<asp:label id="lblStatus" runat="server"></asp:label>

	<h1 class="pageTitle" style="text-align:left;">Request Blocking</h1><a href="~/mykaneva/settings.aspx" runat="server" style="float:right;">Back to Settings</a>
	<br />
	<table id="tblBlocks" width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td style="vertical-align:top;" align="center">
				<h2 >Blocked Worlds</h2>
				<div style="text-align:left;width:330px;">World</div>
				<div class="divBlock">
					<asp:repeater id="rptBlockedCommunities" runat="server" OnItemCommand="rptBlockedCommunities_ItemCommand" >
						<itemtemplate>
							<div style="height:20px;line-height:20px;text-align:left;padding-left:5px;">
							<!--
							<%# DataBinder.Eval(Container.DataItem, "UserId").ToString()%>
							<%# DataBinder.Eval(Container.DataItem, "BlockedCommunityId").ToString()%>
							-->
							<span class="commName" title="<%# DataBinder.Eval(Container.DataItem, "BlockedCommunityName").ToString()%>">
								<%# DataBinder.Eval(Container.DataItem, "BlockedCommunityName").ToString().Length > 30 ? (DataBinder.Eval(Container.DataItem, "BlockedCommunityName").ToString()).Substring(0, 27) +"..." : DataBinder.Eval(Container.DataItem, "BlockedCommunityName").ToString() %>  </span>

							<asp:linkbutton runat="server" id="btnUnblock" commandname="cmdUnblock" text="Unblock" />
							<input type="hidden" runat="server" id="hidBlockedUserId" value='<%#DataBinder.Eval(Container.DataItem, "BlockedUserId")%>' name="hidBlockedUserId">
							<input type="hidden" runat="server" id="hidBlockedCommunityId" value='<%#DataBinder.Eval(Container.DataItem, "BlockedCommunityId")%>' name="hidBlockedCommunityId">
							</div>
						</itemtemplate>
					</asp:repeater>
				</div>
			</td>
			<td style="vertical-align:top;" align="center">
				<h2>Blocked Players of Worlds</h2>
				<div style="text-align:left;width:330px;">Avatar - World</div>
				<div class="divBlock">
					<asp:repeater id="rptBlockedPlayerCommunities" runat="server" OnItemCommand="rptBlockedCommunities_ItemCommand" >
						<itemtemplate>
							<!--
							<%# DataBinder.Eval(Container.DataItem, "UserId").ToString()%>
							<%# DataBinder.Eval(Container.DataItem, "BlockedCommunityId").ToString()%>
							-->
							<span class="userName" title="<%# DataBinder.Eval(Container.DataItem, "BlockedUserName").ToString()%>"> 
								<%# DataBinder.Eval(Container.DataItem, "BlockedUserName").ToString().Length > 14 ? (DataBinder.Eval(Container.DataItem, "BlockedUserName").ToString()).Substring(0, 10) +"..." : DataBinder.Eval(Container.DataItem, "BlockedUserName").ToString() %>  </span></span> -
		
							<span class="commName" title="<%# DataBinder.Eval(Container.DataItem, "BlockedCommunityName").ToString()%>">
								<%# DataBinder.Eval(Container.DataItem, "BlockedCommunityName").ToString().Length > 14 ? (DataBinder.Eval(Container.DataItem, "BlockedCommunityName").ToString()).Substring(0, 10) +"..." : DataBinder.Eval(Container.DataItem, "BlockedCommunityName").ToString() %>  </span>
				
							<asp:linkbutton runat="server" id="btnUnblock" commandname="cmdUnblock" text="Unblock" />
							<input type="hidden" runat="server" id="hidBlockedUserId" value='<%#DataBinder.Eval(Container.DataItem, "BlockedUserId")%>' name="hidBlockedUserId">
							<input type="hidden" runat="server" id="hidBlockedCommunityId" value='<%#DataBinder.Eval(Container.DataItem, "BlockedCommunityId")%>' name="hidBlockedCommunityId">
							<br />
						</itemtemplate>
					</asp:repeater>
				</div>
			</td>
		</tr>
	</table>

					</td>
					<td class=""><img id="Img2" runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
				</tr>
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
				
			</table>
		</td>
	</tr>
</table>
<br/>

