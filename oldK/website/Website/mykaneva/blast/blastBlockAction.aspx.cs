///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Kaneva.BusinessLayer;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.mykaneva.blast
{
	public partial class blastBlockAction : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			String strAction = String.IsNullOrEmpty(Request["a"]) ? "" : Request["a"].ToString();
			String strDest = String.IsNullOrEmpty(Request["d"]) ? "" : Request["d"].ToString();

			if (strAction == "b")
			{
				try
				{
					//block
					int userId = KanevaWebGlobals.CurrentUser.UserId;
					int blockUserId = String.IsNullOrEmpty(Request["u"]) ? 0 : Convert.ToInt32(Request["u"]);
					int blockCommunityId = String.IsNullOrEmpty(Request["c"]) ? 0 : Convert.ToInt32(Request["c"]);

					UserFacade userFacade = new UserFacade();
					userFacade.AddBlastBlock(userId, blockUserId, blockCommunityId);

					if(strDest == "")
					{
						Response.Write("1");
					}
				}
				catch
				{
					if(strDest == "")
					{
						Response.Write("-1");
					}
				}
			}

			

			if (strDest == "prefs")
			{
				Response.Redirect("http://" + KanevaGlobals.SiteName + "/mykaneva/blast/blastFilters.aspx?a=b");
			}

		}
	}
}
