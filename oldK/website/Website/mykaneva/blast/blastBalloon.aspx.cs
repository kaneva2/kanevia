///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for BlastBalloon.
	/// </summary>
	public class blastBalloon : System.Web.UI.Page
	{
		private void Page_Load(object sender, System.EventArgs e)
		{
            BlastFacade blastFacade = new BlastFacade();
            UInt64 entryId = Convert.ToUInt64(Request["entryId"].ToString());
			Blast blast = null;

            blast = blastFacade.GetBlast(entryId);
            if (blast == null)
			{
                blast = blastFacade.GetAdminBlast(entryId);
			}

			// Show the blast data
            if (blast != null)
			{
				string strDiaryText = blast.DiaryEntry;
                string subject = blastFacade.FormatBlast((int)blast.BlastType, blast.Subj, Page.ResolveUrl("~/mykaneva/blast/blastDetails.aspx?entryId=" + blast.EntryId.ToString()));

				// Decode it?
                if (blast.BlastType.Equals((int) BlastTypes.VIDEO) || blast.BlastType.Equals((int) BlastTypes.PHOTO))
				{
                    strDiaryText = Server.HtmlDecode(strDiaryText);
				}
                else if (blast.BlastType.Equals((int)BlastTypes.ADMIN))
                {
                    //check to see if it is an image or an embed tag
                    int isAdminVideo = strDiaryText.IndexOf("embed");
                    int isAdminPhoto = strDiaryText.IndexOf("img");
                    if ((isAdminVideo > 0) || (isAdminPhoto > 0))
                    {
                        strDiaryText = Server.HtmlDecode(strDiaryText);
                    }
                }

				Response.Write (subject + "<br><br>" + strDiaryText);
				Response.End ();
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
