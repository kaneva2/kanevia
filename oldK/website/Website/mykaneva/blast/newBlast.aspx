<%@ Page language="c#" ValidateRequest="False" Codebehind="newBlast.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.newBlast" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>
<%@ Register TagPrefix="CE" Namespace="CuteEditor" Assembly="CuteEditor" %>

<script language="javascript" src="../../jscript/prototype.js"></script>

<script language="javascript" src="../../jscript/dw_tooltip/dw_event.js"></script>
<script language="javascript" src="../../jscript/dw_tooltip/dw_viewport.js"></script>
<script language="javascript" src="../../jscript/dw_tooltip/dw_tooltip.js"></script>

<script type="text/javascript" src="../../jscript/yahoo/yahoo-min.js"></script>  
<script type="text/javascript" src="../../jscript/yahoo/event-min.js" ></script>   
<script type="text/javascript" src="../../jscript/yahoo/dom-min.js" ></script>   
<script type="text/javascript" src="../../jscript/yahoo/calendar-min.js"></script>  

<link href="../../css/new.css" rel="stylesheet" type="text/css" />

<!--[if IE 6]>
<link rel="stylesheet" href="../../css/new_IE.css" type="text/css" />
<![endif]-->

<link href="../../css/yahoo/calendar.css" type="text/css" rel="stylesheet">   
<link href="../../css/mykaneva.css" rel="stylesheet" type="text/css" />
<link href="../../css/base/buttons_new.css" rel="stylesheet" type="text/css" />

<style>
	a.btnsmall {float:right;padding-top:6px;height:21px;}
	#cal1Container { display:none; position:absolute; z-index:100;}
	#cal2Container { display:none; position:absolute; z-index:100;}
</style>

<style>
div#tipDiv {width:250px;}
</style>

<script type="text/javascript"><!--

window.onload = ttINIT;
function ttINIT (){Tooltip.init();}

YAHOO.namespace("example.calendar");

    
	function handleSelect(type,args,obj) {
		var dates = args[0]; 
		var date = dates[0];
		var year = date[0], month = date[1], day = date[2];
		
		var txtDate1 = document.getElementById("txtAdminDate");
		txtDate1.value = month + "-" + day + "-" + year;
		YAHOO.example.calendar.cal1.hide();
	}
	function initAdminCal() {
		// Admin Calendar
		YAHOO.example.calendar.cal1 = new YAHOO.widget.Calendar ("cal1", "cal1Container", { iframe:true, zIndex:200, mindate:"1/1/2007", title:"Choose a date:", close:true } );   
		YAHOO.example.calendar.cal1.render();
		
		// Listener to show Admin Calendar when the button is clicked   
		YAHOO.util.Event.addListener("imgSelectDate", "click", YAHOO.example.calendar.cal1.show, YAHOO.example.calendar.cal1, true);   
		YAHOO.example.calendar.cal1.selectEvent.subscribe(handleSelect, YAHOO.example.calendar.cal1, true);
	}

//--> </script>  



<table border="0" cellspacing="0" cellpadding="0" class="newcontainer" align="center">
	<tr>
		<td>
			<table  border="0" cellpadding="0" cellspacing="0" width="100%" class="newcontainerborder">
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
				<tr>
					<td class=""><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
					<td valign="top" class="newdatacontainer">
	
						<!-- Blast Details -->
						<div id="divMsgDetails">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
								
									<!-- TOP STATUS BAR -->
									<div id="pageheader">
										<table cellpadding="0" cellspacing="0" border="0" width="99%">
											<tr>
												<td align="left"><h1>Choose Blast Type</h1></td>
											</tr>
										</table>
									</div>
									<!-- END TOP STATUS BAR -->	
								
								</td>
							</tr>
							<tr>
							
								<td width="968" align="center"  valign="top">
									
										
										<h2 class="alertmessage"><span id="spnAlertMsg"></span></h2>
										
										<table cellpadding="0" cellspacing="0" border="0"  width="100%">
										<tr>
											<td width="968" valign="top" align="center">
											
												<table border="0" cellspacing="0" cellpadding="0" width="99%">
													<tr>
														
														<!-- START BLAST DETAIL -->
														<td valign="top" align="center">
															
															<table border="0" cellspacing="0" cellpadding="0">
																<tr>
																	<td width="670" align="left">
																		
																	    <table border="0" cellspacing="0" cellpadding="0" width="346">
																			<tr>
																				<td  class="small italic" align="left">
																					<div class="frame">
																					<span class="ct"><span class="cl"></span></span>
																					
																						<table id="tabNav" cellSpacing="0" cellPadding="0" border="0">
																							<tr>
																								<td valign="top">
																									<ul>
																										<li runat="server" id="liText"><a runat="server" id="aBlastTypeText">Text</a></li>
                                                                                                        <li runat="server" id="liVideo"><a runat="server" id="aBlastTypeVideo">Video</a></li>
                                                                                                        <li runat="server" id="liPhoto"><a runat="server" id="aBlastTypePhoto">Photo</a></li>
																										<li runat="server" id="liLink"><a runat="server" id="aBlastTypeLink">Link</a></li>
																										<li runat="server" id="liPlace" class="last"><a runat="server" id="aBlastTypePlace">Place</a></li>
																									</ul>	
																									<input type="hidden" id="hidBlastType" value="1" runat="server" name="hidBlastType"/>
																								</td>
																							</tr>
																						</table>	
																					<span class="cb"><span class="cl"></span></span>
																					</div>
																				</td>
																			</tr>
																		</table>
																		
																		
																		
																		<div class="module  fullpage">
																		<span class="ct"><span class="cl"></span></span>
																		
																		<div id="divBlastToCommunityName" runat="server" visible="false" style="width:80%;text-align:right;font-size:12px;line-height:16px;"></div>
																		<table id="tblMyCommunities" runat="server" visible="false" border="0" cellspacing="0" cellpadding="0" width="67%" align="center">
																			<tr>
																				<td class="small italic" align="right">
																					Optional: Blast to one of your communities
																					<asp:DropDownList id="ddlChannels" runat="server"/>
																				</td>
																			</tr>
																		</table>
																		
																		<div id="divErrMsg" runat="server"></div>
																		
																		<br><br>
																		
																		<!-- Text Blast -->																																																								  
																		<table id="tblBlastText" runat="server" border="0" cellspacing="0" cellpadding="0" width="67%" align="center">																																		 
																			<tr>
																				<td align="center">
                                                                                    <asp:requiredfieldvalidator runat="server" id="rfvTextBlast" controltovalidate="txtBlast" text="* Please enter a message." errormessage="No message was submitted" display="Dynamic"></asp:requiredfieldvalidator>
																					<CE:Editor id="txtBlast" style="margin:0 0 5 0;" AllowPasteHtml="True" EditorOnPaste="PasteAsHTML" usephysicalformattingtags="true"
																						ShowBottomBar="False" EnableContextMenu="False" BackColor="#ededed" runat="server" enableobjectresizing="false" 
																						enablestripscripttags="true" MaxTextLength="140" MaxHTMLLength="500" width="450px" 
																						Height="175px" ShowHtmlMode="false" ConfigurationPath="~/CuteSoft_Client/CuteEditor/Configuration/AutoConfigure/blast.config" AutoConfigure="None" ></CE:Editor>
																				</td>
																			</tr>
																		</table>
																		
																		<!-- Video Blast -->																																																								 
																		<table id="tblBlastVideo" runat="server" Visible="False" border="0" cellspacing="0" cellpadding="0" width="67%" align="center">
																			<tr>
																				<td align="center">
																					<table border="0" cellspacing="0" cellpadding="0" width="67%" align="center">
																						<tr>
																							<td align="left">
																							    <span class="large bold">Video Embed Code</span><br>
																							    <span class="small">This can be embed code from Kaneva or YouTube</span>
																							</td>
																						</tr>
																						<tr>
																							<td>
                                                                                                <asp:requiredfieldvalidator runat="server" id="rfvVideoBlast" controltovalidate="txtVideoEmbed" text="* Please enter your video embed code." errormessage="No video was submitted" display="Dynamic"></asp:requiredfieldvalidator>	
																								<asp:TextBox ID="txtVideoEmbed" TextMode="multiline" Rows="4" Cols="50" style="margin:0 0 5 0; width:450px;" MaxLength="140" runat="server"/>
																							</td>
																						</tr>
																					</table>
																					
																					
																					<table runat="server" Visible="False" border="0" cellspacing="0" cellpadding="0" width="67%" align="center">
																						<tr>
																							<td align="left">
																								<asp:CheckBox id="chkYouTube" runat="server"/><label>Save YouTube video to My Media Library</label>
																							</td>
																						</tr>
																					</table>
																					
																					<br><br>	
																					
																					<table border="0" cellspacing="0" cellpadding="0" width="67%" align="center">
																						<tr>
																							<td align="left" style="padding:0 0 8px 0;"><span class="large bold">Caption</span></td>
																						</tr>
																						<tr>
																							<td>
																								<CE:Editor style="margin:0 0 5 0;" id="txtVideoCaption" enableobjectresizing="false"
																								AllowPasteHtml="True" EditorOnPaste="PasteAsHTML" usephysicalformattingtags="true" 
																								ShowBottomBar="False" EnableContextMenu="False" BackColor="#ededed" runat="server" 
																								MaxTextLength="140" enablestripscripttags="true" width="450px" Height="75px" ShowHtmlMode="false" 
																								ConfigurationPath="~/CuteSoft_Client/CuteEditor/Configuration/AutoConfigure/blast.config" AutoConfigure="None" ></CE:Editor>
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																		</table>																																																		 
																		
																		<!-- Photo Blast -->
																		<table id="tblBlastPhoto" runat="server" Visible="False" border="0" cellspacing="0" cellpadding="0" width="67%" align="center">
																			<tr>
																				<td align="center">
																				
																					<table id="tblURL" border="0" cellspacing="0" cellpadding="0" width="67%" align="center">
																						<tr>
																							<td align="left">
																							    <span class="large bold">Photo URL</span>
                                                                                                <asp:requiredfieldvalidator runat="server" id="rfvPhotoBlast" controltovalidate="txtPhotoURL" text="* Please enter a photo URL." errormessage="No photo URL submitted" display="Dynamic"></asp:requiredfieldvalidator>	
																							</td>
																						</tr>
																						<tr>
																							<td align="left" style="padding-top:5px;">
																								<asp:TextBox ID="txtPhotoURL" class="formKanevaText" style="width:450px" MaxLength="300" runat="server"/><br>	
																								<small>Example: http://www.kaneva.com/images/klogo.gif</small>
																								<A Visible="False" href="javascript:void(0);" runat="server" onclick="Element.hide('tblURL');Element.show('tblUpload');return(false);" class="adminLinks">upload photo instead</A>
																							</td>
																						</tr>
																					</table>
																					
																					<table id="tblUpload" style="display:none;" border="0" cellspacing="0" cellpadding="0" width="67%" align="center">
																						<tr>
																							<td><span class="large bold">Photo</span></td>
																						</tr>
																						<tr>
																							<td>
																								<input runat="server" class="Filter2" ID="inpThumbnail" type="file" style="width:450px" onFocus="document.forms.frmMain.btnUpload.disabled=false;" onClick="document.forms.frmMain.btnUpload.disabled=false;" />
																								<asp:button id="btnUpload" runat="Server" Text=" Upload "/><br>
																								<A href="javascript:void(0);" runat="server" onclick="Element.hide('tblUpload');Element.show('tblURL');return(false);" class="adminLinks">use URL instead</A>
																							</td>
																						</tr>
																					</table>
																					
																					<table border="0" cellspacing="0" cellpadding="0" width="67%" align="center">
																						<tr>
																							<td style="padding:8px 0 5px 0;" align="left"><span class="large bold">Caption</span></td>
																						</tr>
																						<tr>
																							<td>
																								<CE:Editor style="margin:0 0 5 0;" id="txtPhotoCaption" 
																								AllowPasteHtml="True" EditorOnPaste="PasteAsHTML" usephysicalformattingtags="true" enableobjectresizing="false"
																								ShowBottomBar="False" EnableContextMenu="False" BackColor="#ededed" runat="server" 
																								MaxTextLength="140" MaxHTMLLength="500" enablestripscripttags="true" width="450px" Height="75px" 
																								ShowHtmlMode="false" ConfigurationPath="~/CuteSoft_Client/CuteEditor/Configuration/AutoConfigure/blast.config" AutoConfigure="None" ></CE:Editor>
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																		</table>
																		
																		<!-- Link Blast -->
																		<table id="tblBlastLink" runat="server" Visible="False" border="0" cellspacing="0" cellpadding="0" width="67%" align="center">
																			<tr>
																				<td align="center">																					
																					<table border="0" cellspacing="0" cellpadding="0" width="67%" align="center">
																						<tr>
																							<td style="padding:8px 0 5px 0;" align="left">
																							    <span class="large bold">Name</span>
                                                                                                <asp:requiredfieldvalidator runat="server" id="rfvLinkBlast1" controltovalidate="txtLinkName" text="* Please enter a link name." errormessage="No link name submitted" display="Dynamic"></asp:requiredfieldvalidator>	
																							</td>
																						</tr>
																						<tr>
																							<td>
																								<asp:TextBox ID="txtLinkName" class="formKanevaText" style="width:450px" MaxLength="40" runat="server"/>
																							</td>
																						</tr>
																						<tr>
																							<td style="padding: 8px 0 5px 0;" align="left">
																							    <span class="large bold">URL</span>
                                                                                                <asp:requiredfieldvalidator runat="server" id="rfvLinkBlast2" controltovalidate="txtLinkURL" text="* Please enter a link URL." errormessage="No link URL submitted" display="Dynamic"></asp:requiredfieldvalidator>	
																							</td>
																						</tr>
																						<tr>
																							<td align="left" style="padding-bottom:5px;">
																								<asp:TextBox ID="txtLinkURL" class="formKanevaText" style="width:400px" MaxLength="200" runat="server"/>
																								<small>Example: http://www.kaneva.com</small>
																							</td>
																						</tr>
																						<tr>
																							<td style="padding:8px 0 5px 0;" align="left"><span class="large bold">Description</span></td>
																						</tr>
																						<tr>
																							<td>
																								<CE:Editor style="margin:0 0 5 0;" id="txtLinkDescription" 
																								AllowPasteHtml="True" EditorOnPaste="PasteAsHTML" usephysicalformattingtags="true" enableobjectresizing="false"
																								ShowBottomBar="False" EnableContextMenu="False" BackColor="#ededed" runat="server" 
																								MaxTextLength="140" MaxHTMLLength="500" enablestripscripttags="true" width="450px" 
																								Height="150px" ShowHtmlMode="false" ConfigurationPath="~/CuteSoft_Client/CuteEditor/Configuration/AutoConfigure/blast.config" AutoConfigure="None" ></CE:Editor>
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																		</table>
																		
																		<!-- Place -->
																		<table id="tblBlastPlace" runat="server" Visible="False" border="0" cellspacing="0" cellpadding="0" width="67%" align="center">
																			<tr>
																				<td align="center">																					
																					<table id="tblPlVirtualLocation" style="display:none;" border="0" cellspacing="0" cellpadding="0" width="67%" align="center">
																						<tr>
																							<td style="padding:8px 0 5px 0;" align="left"><span class="large bold">Virtual Location</span></td>
																							<td align="right"> <A href="javascript:void(0);" runat="server" onclick="Element.hide('tblPlVirtualLocation');Element.show('tblPlRealLocation');return(false);" class="adminLinks">Location</A></td>
																						</tr>
																						<tr>
																							<td colspan="2">
																								<asp:DropDownList id="ddlPlaceChannels" runat="server" style="width:435px"/>&nbsp;<img runat="server" src="~/images/blast/info_icon.gif"  onmouseover="doTooltip(event,'Join Communities to add more places.')" onmouseout="hideTip();" border="0"/>
																							</td>
																						</tr>
																					</table>
																					<table id="tblPlRealLocation" border="0" cellspacing="0" cellpadding="0" width="67%" align="center">
																						<tr>
																							<td  style="padding:8px 0 0 0;" align="left">
																							    <span class="large bold">Location Name</span>
                                                                                                <asp:requiredfieldvalidator runat="server" id="rfvPlaceBlast1" visible="true" controltovalidate="txtPlaceLocation" text="* Please enter location name." errormessage="No location name submitted" display="Dynamic"></asp:requiredfieldvalidator>	
																							</td>
																							<td align="right"> <A href="javascript:void(0);" runat="server" onclick="Element.hide('tblPlRealLocation');Element.show('tblPlVirtualLocation');return(false);" class="adminLinks">My virtual location</A></td>
																						</tr>
																						<tr>
																							<td colspan="2">
																								<asp:TextBox ID="txtPlaceLocation" class="formKanevaText" style="width:450px" MaxLength="40" runat="server"/>
																							</td>
																						</tr>
																						<tr>
																							<td colspan="2"  style="padding:8px 0 5px 0;" align="left"><span class="large bold">Address</span></td>
																						</tr>
																						<tr>
																							<td colspan="2">
																								<asp:TextBox ID="txtPlaceAddress" class="formKanevaText" style="width:450px" MaxLength="40" runat="server"/>
																							</td>
																						</tr>
																					</table>
																					
																					<table border="0" cellspacing="0" cellpadding="0" width="67%" align="center">
																						<tr>
																							<td  style="padding:8px 0 5px 0;" align="left"><span class="large bold">Description</span></td>
																						</tr>
																						<tr>
																							<td>
																								<CE:Editor style="margin:0 0 5 0;" id="txtPlaceDescription" 
																								AllowPasteHtml="True" EditorOnPaste="PasteAsHTML" usephysicalformattingtags="true" enableobjectresizing="false"
																								ShowBottomBar="False" EnableContextMenu="False" BackColor="#ededed" runat="server" 
																								MaxTextLength="140" MaxHTMLLength="500" enablestripscripttags="true" width="450px" 
																								Height="150px" ShowHtmlMode="false" ConfigurationPath="~/CuteSoft_Client/CuteEditor/Configuration/AutoConfigure/blast.config" AutoConfigure="None" ></CE:Editor>
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																		</table>
					
																		<table  border="0" cellspacing="0" cellpadding="0" align="center" width="67%" style="display:block;clear:both;margin-top:10px;">
																			<tr>
																				<td align="left">
																				    <asp:LinkButton runat="server" id="toggleAdminOpts" text="Show Admin Options" CausesValidation="false" onclick="lnkAdminToggle_Click" ></asp:LinkButton>
																				</td>
																			</tr>
																		</table>
																		
																		<br>			
																		
																		<table id="tblAdmin" runat="server" border="0" cellspacing="0" cellpadding="0" width="67%" align="center">
																			<tr>
																				<td class="large">
																					<div class="frame">
																					<span class="ct"><span class="cl"></span></span>
																						<table border="0" cellspacing="0" cellpadding="0">
																							<tr>
																								<td colspan="7" class="bold" style="padding: 0 0 8px 0;">Admin only</td>
																							</tr>
																							<tr>
																								<td colspan="7" class="large bold"><asp:CheckBox id="chkKanevaBlast" runat="server"/> Kaneva Blast to Entire Site<div id="cal1Container"></div></td>
																							</tr>
																							<tr>
																								<td class="formspacer_small"></td>
																							</tr>
																							<tr>
																								<td colspan="7">
																									<asp:RadioButtonList id="rblColor" runat="server" RepeatDirection="Horizontal">
																										<asp:ListItem title="Critical" Value="alert"><span style="color: red;">Red</span></asp:ListItem>
																										<asp:ListItem title="Sponsor" Value="highlight2"><span style="color: #e7923e;">Orange</span></asp:ListItem>
																										<asp:ListItem title="Sale" Value="highlight3"><span style="color: green;">Green</span></asp:ListItem>
																										<asp:ListItem title="Highlight" Value="highlight1"><span style="color: yellow; background-color:#aaaaaa">Yellow</span></asp:ListItem>
																										<asp:ListItem Selected="True" Value="">None</asp:ListItem>
																									</asp:RadioButtonList>
																								</td>
																							</tr>
																							<tr>
																								<td class="formspacer_small"></td>
																							</tr>
																							<tr>
																								<td class="large">
																									Expire after:
																								</td>
																								<td colspan="3" class="large" valign="top" nowrap>
																									<asp:TextBox ID="txtAdminDate" class="formKanevaText" style="width:80px" MaxLength="10" runat="server"/><img id="imgSelectDate" name="imgSelectDate" runat="server" src="~/images/blast/cal_16.gif"/><asp:RegularExpressionValidator id="revDate1" ControlToValidate="txtAdminDate" Text="*" Display="Static" ErrorMessage="Expire after date must be in format mm-dd-yyyy." EnableClientScript="True" runat="server"/>
																									<br/><small>MM-dd-yyyy</small>
																								</td>
																								<td colspan="3" valign="top" class="large">
																									:<asp:Dropdownlist runat="server" ID="drpAdminTime" style="width: 80px; vertical-align: middle;">
																									<asp:ListItem value="00:00:01">12:00 AM</asp:ListItem >
																									<asp:ListItem value="00:30:00">12:30 AM</asp:ListItem >
																									<asp:ListItem value="01:00:00">1:00 AM</asp:ListItem >
																									<asp:ListItem value="01:30:00">1:30 AM</asp:ListItem >
																									<asp:ListItem value="02:00:00">2:00 AM</asp:ListItem >
																									<asp:ListItem value="02:30:00">2:30 AM</asp:ListItem >
																									<asp:ListItem value="03:00:00">3:00 AM</asp:ListItem >
																									<asp:ListItem value="03:30:00">3:30 AM</asp:ListItem >
																									<asp:ListItem value="04:00:00">4:00 AM</asp:ListItem >
																									<asp:ListItem value="04:30:00">4:30 AM</asp:ListItem >
																									<asp:ListItem value="05:00:00">5:00 AM</asp:ListItem >
																									<asp:ListItem value="05:30:00">5:30 AM</asp:ListItem >
																									<asp:ListItem value="06:00:00">6:00 AM</asp:ListItem >
																									<asp:ListItem value="06:30:00">6:30 AM</asp:ListItem >
																									<asp:ListItem value="07:00:00">7:00 AM</asp:ListItem >
																									<asp:ListItem value="07:30:00">7:30 AM</asp:ListItem >
																									<asp:ListItem value="08:00:00">8:00 AM</asp:ListItem >
																									<asp:ListItem value="08:30:00">8:30 AM</asp:ListItem >
																									<asp:ListItem value="09:00:00">9:00 AM</asp:ListItem >
																									<asp:ListItem value="09:30:00">9:30 AM</asp:ListItem >
																									<asp:ListItem value="10:00:00">10:00 AM</asp:ListItem >
																									<asp:ListItem value="10:30:00">10:30 AM</asp:ListItem >
																									<asp:ListItem value="11:00:00">11:00 AM</asp:ListItem >
																									<asp:ListItem value="11:30:00">11:30 AM</asp:ListItem >
																									<asp:ListItem value="12:00:00">12:00 PM</asp:ListItem >
																									<asp:ListItem value="12:30:00">12:30 PM</asp:ListItem >
																									<asp:ListItem value="13:00:00">1:00 PM</asp:ListItem >
																									<asp:ListItem value="13:30:00">1:30 PM</asp:ListItem >
																									<asp:ListItem value="14:00:00">2:00 PM</asp:ListItem >
																									<asp:ListItem value="14:30:00">2:30 PM</asp:ListItem >
																									<asp:ListItem value="15:00:00">3:00 PM</asp:ListItem >
																									<asp:ListItem value="15:30:00">3:30 PM</asp:ListItem >
																									<asp:ListItem value="16:00:00">4:00 PM</asp:ListItem >
																									<asp:ListItem value="16:30:00">4:30 PM</asp:ListItem >
																									<asp:ListItem value="17:00:00">5:00 PM</asp:ListItem >
																									<asp:ListItem value="17:30:00">5:30 PM</asp:ListItem >
																									<asp:ListItem value="18:00:00">6:00 PM</asp:ListItem >
																									<asp:ListItem value="18:30:00">6:30 PM</asp:ListItem >
																									<asp:ListItem value="19:00:00">7:00 PM</asp:ListItem >
																									<asp:ListItem value="19:30:00">7:30 PM</asp:ListItem >
																									<asp:ListItem value="20:00:00" Selected="True">8:00 PM</asp:ListItem >
																									<asp:ListItem value="20:30:00">8:30 PM</asp:ListItem >
																									<asp:ListItem value="21:00:00">9:00 PM</asp:ListItem >
																									<asp:ListItem value="21:30:00">9:30 PM</asp:ListItem >
																									<asp:ListItem value="22:00:00">10:00 PM</asp:ListItem >
																									<asp:ListItem value="22:30:00">10:30 PM</asp:ListItem >
																									<asp:ListItem value="23:00:00">11:00 PM</asp:ListItem >
																									<asp:ListItem value="23:30:00">11:30 PM</asp:ListItem >
																								</asp:Dropdownlist>&nbsp;EST
																								</td>
																							</tr>																							
																						</table>
																					<span class="cb"><span class="cl"></span></span>
																					</div>
																				</td>
																			</tr>
																		</table>
																																			
																		<br /><hr /><br />
																		
																		<ajax:ajaxpanel id="pnlPreview" runat="server">
																		<div id="blastPreview" style="display:none;">
																		    
																		    <div id="divBlastConfirm" runat="server" style='position:absolute;left:60px;top:-20px;margin:0;padding:0px;background-color:transparent;text-align:justify;font-size:12px;width:auto;z-index:6;border:0'>
																				<div style='width:350px;height:150px;display:block; border:3px solid #f7ce52; padding:16px, 20px 10px 20px; background-color:#ffff99;'>
																					<div style="font: bold; font-size:12pt">Send Blast with Highlight</div>
																					<br />
																					<div><span>By sending this blast you are agreeing to Kaneva's <a id="A2" href="~/overview/termsandconditions.aspx" runat="server" target="_blank">Terms and Conditions</a>.</span></div>
																					<div style="text-align:right"><br /><br />
																						<asp:button id="btnBuyHighlight" runat="server" causesvalidation="false" text="Confirm & Send" onclick="btnPreview_Click"></asp:button>
																						<asp:button id="btnBuyHighlightCancel" runat="server" causesvalidation="false" text="Cancel" onclick="btnBuyHighlightCancel_Click" ></asp:button>
																					</div>
																				</div>
																		    </div>
																			
																			
																				<asp:checkbox id="chkPayHighlight" autopostback="true" oncheckedchanged="PayHighlight_CheckedChanged" runat="server"></asp:checkbox> <b>Buy a highlighted blast on the My Kaneva page</b> (cost = 50 credits)
																				
																				<div id="blastList" style="border-right:solid 1px #cccccc;padding:0px;">
																					
																					<ul class="blast" style="border-left:solid 1px #cccccc;border-top:solid 1px #cccccc;border-right:solid 1px #cccccc;margin:0;">
																						<li id="liBlastPreview" runat="server" class="message " onmouseover="this.style.backgroundColor='#C8E3EE';" onmouseout="this.style.backgroundColor='';">
																							<span class="icon"><asp:literal runat="server" id="litPreviewImage" /></span>
																							<asp:literal id="litBlastMsg" runat="server" /> <span class="replyNow"><a href="javascript:void(0);">reply now</a></span>
																						</li>
																					</ul>
																				
																				</div>
																					
																					<div style="margin-top:10px;">
																						<div>
																							<div style="width:18%; float:left">
																								<asp:button runat="server" id="btn_refreshPreview" onclick="btn_refreshPreview_Click" causesvalidation="false" text="Preview" Font-Size="Smaller" />
																							</div>
																							<div style="width:80%; float:right; text-align:right">
																								<asp:literal runat="server" id="currentBalance" />
																							</div>
																						</div>
																						<div style="width:100%; text-align:right">
																							<asp:literal runat="server" id="balanceSummary" />
																						</div>
																					</div>
																				
																			
																			
																		</div>
																		<hr /><br />
																		<table class="clearboth" border="0" cellspacing="0" cellpadding="0" width="67%" align="center" style="left: 0px; top: 0px">
																			<tr>
																				<td style="height: 24px"><a id="A1" runat="server" href="~/default.aspx">Cancel & Go Back</a></td>
																			
																				<td class="large" align="right" style="height: 24px">
																					<asp:button style="width: 100px;" id="btnPreviewHighlight" runat="server" text="Send Blast" onclick="btnPreviewHighlight_Click"></asp:button>
																					<asp:linkbutton class="btnsmall grey" id="btnPreview" onclick="btnPreview_Click" runat="server" text="Send Blast"></asp:linkbutton>
																				</td>
																			</tr>
																		</table>
																		</ajax:ajaxpanel>
																		<span class="cb"><span class="cl"></span></span>
																		</div>
																	
																	</td>
																</tr>
															</table>
															
														</td>
														<!-- END BLAST DETAIL -->
													</tr>
												</table>
											
											</td>
											
										</tr>
										
									</table>
									
								</td>
							</tr>
						</table>
						</div>
						<!-- End Message Details -->
					
					</td>
					<td class=""><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
				</tr>
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
			</table>
			
		</td>
		
	</tr>
</table>