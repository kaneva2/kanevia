///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva.channel;
using log4net;
using MagicAjax;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for blastDetails.
	/// </summary>
	public class blastDetails : MainTemplatePage
    {
        #region constructors

        protected blastDetails () 
		{
			Title = "Blast Details";
        }

        #endregion

        #region PageLoad

        private void Page_Load(object sender, System.EventArgs e)
		{
			if (!Request.IsAuthenticated)
			{
				Response.Redirect (this.GetLoginURL ());
			}

			if (!IsPostBack)
			{
                //logic to how or hide the delete button
                if (IsAdministrator())
                {
                    adminBlastDelete.Visible = true;
                    string script = "javascript: if(!confirm('Are you sure you want to delete the current blast? \\n This cannot be undone.')) return false;";
                    adminBlastDelete.Attributes.Add("onClick", script);
                }
                else
                {
                    adminBlastDelete.Visible = false;
                }

                BlastFacade blastFacade = new BlastFacade();

                UInt64 entryId = Convert.ToUInt64(Request["entryId"].ToString());
				
                
                Blast blast = blastFacade.GetBlast (entryId);

                
                if (blast == null)
                {
                    blast = blastFacade.GetAdminBlast(entryId);
                }

                if (blast != null)
				{
                    lnkFrom.Text = blast.NameNoSpaces;
                    lnkFrom.Attributes.Add("href", GetPersonalChannelUrl(blast.NameNoSpaces));

                    aFromAvatar.HRef = GetPersonalChannelUrl(blast.NameNoSpaces);
                    imgFromAvatar.Src = GetProfileImageURL(blast.ThumbnailSmallPath);

                    lblDate.Text = FormatDateTime(blast.DateCreated);

                    // use this formatting if blast is text or admin text
                    if (blast.BlastType == 1 || blast.BlastType == 12)
                    {
                        lblSubject.Text = blastFacade.FormatBlast ((int) blast.BlastType, blast.Subj, Page.ResolveUrl ("~/mykaneva/blast/blastDetails.aspx?entryId=" + blast.EntryId.ToString ()), blast.DiaryEntry);
                    }
                    else
                    {
                        lblSubject.Text = blastFacade.FormatBlast ((int) blast.BlastType, blast.Subj, Page.ResolveUrl ("~/mykaneva/blast/blastDetails.aspx?entryId=" + blast.EntryId.ToString ()));
                    }

                    string strDiaryText = blast.DiaryEntry;
                    int isAdminVideo = 0;
                    int isAdminPhoto = 0;

					// Decode it?
                    if (blast.BlastType.Equals((int)BlastTypes.VIDEO) || blast.BlastType.Equals((int)BlastTypes.PHOTO))
					{
                        strDiaryText = Server.HtmlDecode(strDiaryText);
					}
                    else if (blast.BlastType.Equals((int)BlastTypes.ADMIN))
                    {
                        //check to see if it is an image or an embed tag
                        isAdminVideo = strDiaryText.IndexOf("embed");
                        isAdminPhoto = strDiaryText.IndexOf("img");
                        if ((isAdminVideo > 0) || (isAdminPhoto > 0))
                        {
                            strDiaryText = Server.HtmlDecode(strDiaryText);
                        }
                        blastComments.Visible = false;
                        imgFromAvatar.Src = ResolveUrl("~/images/blast/icon_blastkaneva.gif");
                    }

                    // if video or photo, show asset details
                    if (blast.BlastType.Equals ((int) BlastTypes.VIDEO) || blast.BlastType.Equals ((int) BlastTypes.PHOTO) ||
                        blast.BlastType.Equals ((int) BlastTypes.LINK) || isAdminVideo > 0 || isAdminPhoto > 0)
                    {
                        try
                        {
                            int startPos = blast.DiaryEntry.IndexOf ("/asset/") + 7;
                            int len = blast.DiaryEntry.IndexOf (".media") - startPos;
                            string aid = blast.DiaryEntry.Substring (startPos, len);
                            int assetId = Convert.ToInt32(blast.DiaryEntry.Substring (startPos, len));
                            
                            DataRow drAsset = StoreUtility.GetAsset (assetId);
                            if (drAsset != null)
                            {
                                // asset image		
                                imgAssetThumbnail.Src = GetMediaImageURL (drAsset["thumbnail_small_path"].ToString (), "sm", assetId, Convert.ToInt32 (drAsset["asset_type_id"]), Convert.ToInt32 (drAsset["thumbnail_gen"]));
                                aAssetDetails.HRef = GetAssetDetailsLink (Convert.ToInt32 (drAsset["asset_id"]));
                                
                                // asset name
                                divTitle.InnerText = TruncateWithEllipsis (Server.HtmlDecode (drAsset["name"].ToString ()), 45);
                                spnViews.InnerText = drAsset["number_of_downloads"].ToString ();
                                spnRaves.InnerText = drAsset["number_of_diggs"].ToString ();
                                spnAdds.InnerText = drAsset["number_of_channels"].ToString ();
                                spnComments.InnerText = drAsset["number_of_comments"].ToString ();

                                if ((int) Constants.eASSET_TYPE.VIDEO == Convert.ToInt32(drAsset ["asset_type_id"]))
		                        {
                                    spnRuntime.InnerText = FormatSecondsAsTime (drAsset["run_time_seconds"]);
                                    divRuntime.Visible = true;
		                        }
                                divMediaDetails.Visible = true;
                            }
                        }
                        catch { }
                    }
                    
                    lblBlast.Text = strDiaryText;

					int communityId = Convert.ToInt32 (blast.CommunityId);
					if (communityId > 0)
					{
						DataRow drCommunity = CommunityUtility.GetCommunity (communityId);
						lnkCommunity.Text = drCommunity ["name_no_spaces"].ToString ();
						lnkCommunity.Attributes.Add ("href", GetBroadcastChannelUrl (drCommunity ["name_no_spaces"].ToString ()));
					}
					else
					{
						lnkCommunity.Visible = false;
						lblBlastedComm.Visible = false;
					}
				}

				BindComments (1, entryId);
			}

			litMaxLen.Text = KanevaGlobals.MaxBlastLength.ToString ();
        }

        #endregion

        #region Primary Functions

        private void BindComments(int pageNumber, UInt64 entryId)
		{
            BlastFacade blastFacade = new BlastFacade();
			int commentsPerPage = 20;
			
			// if commenting on an asset
			rptComments.DataSource = blastFacade.GetBlastReplies(entryId, pageNumber, commentsPerPage, "_date_created"); ;
			rptComments.DataBind();
        }

        #endregion

        #region events

        /// <summary>
        /// Blast delete
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void adminBlastDelete_Click(object sender, System.EventArgs e)
        {
            BlastFacade blastFacade = new BlastFacade();

            //calls function to delete all checked media
            if (IsAdministrator()) //check against user tampering/faking 
            {
                try
                {
                    UInt64 entryId = Convert.ToUInt64(Request["entryId"].ToString());

                    Blast blast = blastFacade.GetBlast(entryId);
                    if (blast == null)
                    {
                        blast = blastFacade.GetAdminBlast(entryId);
                    }

                    //determine which delete to use based on the blast type
                    if ((int)blast.BlastType == (int)BlastTypes.ADMIN)
                    {
                        blastFacade.DeleteAdminBlast(entryId);
                    }
                    
                    blastFacade.DeleteBlast(entryId);
                    
                    MagicAjax.AjaxCallHelper.WriteAlert("The blast was successfully deleted.");
                    Response.Redirect("~/default.aspx");
                }
                catch (Exception ex)
                {
                    m_logger.Error("Error trying to delete blast.", ex);
                    MagicAjax.AjaxCallHelper.WriteAlert("Deletion Error: Sorry we were unable to delete this blast.");
                }
            }

        }

		/// <summary>
		/// User's response to a comment
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void ReplyComment_Click (object sender, EventArgs e)
		{
            BlastFacade blastFacade = new BlastFacade();
            UInt64 entryId = Convert.ToUInt64(Request["entryId"].ToString());

			if (Request.IsAuthenticated)
			{
				// Check for any inject scripts
				if (KanevaWebGlobals.ContainsInjectScripts (txtResponse.Text))
				{
					m_logger.Warn ("User " + KanevaWebGlobals.GetUserId () + " tried to script from IP " + Common.GetVisitorIPAddress());
                    AjaxCallHelper.WriteAlert ("Your input contains invalid scripting, please remove script code and try again.");
					return;
				}

                // Check to make sure user did not enter any "potty mouth" words
                if (KanevaWebGlobals.isTextRestricted (txtResponse.Text, Constants.eRESTRICTION_TYPE.POTTY_MOUTH))
                {
                    MagicAjax.AjaxCallHelper.WriteAlert (Constants.VALIDATION_REGEX_RESTRICTED_WORD_MESSAGE);
                    return;
                }

				if (txtResponse.Text.Length == 0)
				{
					AjaxCallHelper.WriteAlert ("Please enter a reply.  You can not submit a blank reply.");
					return;
				}

				if (txtResponse.Text.Length > KanevaGlobals.MaxCommentLength)		  
				{
					AjaxCallHelper.WriteAlert ("Your reply is too long.  Reply must be " + KanevaGlobals.MaxCommentLength.ToString () + " characters or less");
					return;
				}


                if (blastFacade.InsertBlastReply(entryId, GetUserId(), Server.HtmlEncode(txtResponse.Text), Common.GetVisitorIPAddress()).Equals(-99))
				{
					AjaxCallHelper.WriteAlert ("Error inserting reply!");
					return;
				}
			}

			txtResponse.Text = "";

            BindComments(1, entryId);
        }

        #endregion

        #region Declarations

        // Logger
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

		protected HyperLink		lnkFrom, lnkCommunity;
        protected HtmlAnchor aFromAvatar, aAssetDetails;
        protected HtmlImage imgFromAvatar, imgAssetThumbnail;
		protected Label			lblDate, lblSubject, lblBlast, lblCommunity, lblBlastedComm;
		protected Literal litMaxLen;
		protected Repeater rptComments;
		protected TextBox txtResponse;
        protected HtmlGenericControl blastComments;
        protected LinkButton adminBlastDelete;

        protected HtmlContainerControl divMediaDetails, divTitle, divRuntime;
        protected HtmlContainerControl spnViews, spnRaves, spnAdds, spnComments, spnRuntime;

        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
            this.adminBlastDelete.Click += new System.EventHandler(this.adminBlastDelete_Click);
        }
		#endregion
	}
}
