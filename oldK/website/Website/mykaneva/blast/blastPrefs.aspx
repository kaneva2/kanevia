<%@ Page language="c#" Codebehind="blastPrefs.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.blastPrefs" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>

<link href="../../css/new.css" rel="stylesheet" type="text/css">

<br />
<table  border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td class="frTopLeft"></td>
		<td class="frTop"></td>
		<td class="frTopRight"></td>
	</tr>
	<tr>
		<td class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1" ></td>
		<td valign="top" class="frBgIntMembers">
			<table width="100%"  border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
					
						<!-- TOP STATUS BAR -->
						<div id="pageheader">
							<table cellpadding="0" cellspacing="0" border="0" width="99%">
								<tr>
									<td align="left" width="260px">
										<h1>Activity Settings</h1>
									</td>
									<td align="left">
										<asp:Label id="messages" runat="server"></asp:Label>
									</td>									
								</tr>
								<tr>
									<td align="left" colspan>
										<p style="padding-left: 10px; padding-top: 10px" class="large note">Acrivites are only displayed to your friends in the Activities area of My Kaneva.
									</td>
								</tr>
							</table>
						</div>
						<!-- END TOP STATUS BAR -->		
					
					</td>
				</tr>
				<tr>
					<td width="968" align="left"  valign="top">
					
						<table border="0" cellspacing="0" cellpadding="0" width="100%">
							<tr>
								<td width="48%" valign="top" align="left">
									<div class="module whitebg">
									<span class="ct"><span class="cl"></span></span>

										<h2>What activities I promote</h2>
										
										<ajax:ajaxpanel id="ajBlastPreferences" runat="server">

										<table cellpadding="3" cellspacing="0" width="95%" align="center" border="0">
											<tr>
												<td colspan="2">
													<span class="bold" style="padding-left: 10px">Publish activities when I...</span>
												</td>
											</tr>
											<tr>
												<td colspan="2">
													<ul>
														<li>
															<span class="formKanevaCheck">
																<asp:CheckBox id="chkProfiles" name="chkProfiles" runat="server" text="Update profile theme" />
															</span>
														<li>
															<span class="formKanevaCheck">
																<asp:CheckBox id="chkBlogcomments" name="chkBlogcomments" runat="server" Text="Write a blog entry" />
															</span>
														<li>
															<span class="formKanevaCheck">
																<asp:CheckBox id="chkComment" name="chkComment" runat="server" text="Comment on media details" />
															</span>
														<li>
															<span class="formKanevaCheck">
																<asp:CheckBox id="chkForum" name="chkForum" runat="server" text="Post in a forum" />
															</span>
														<li>
															<span class="formKanevaCheck">
																<asp:CheckBox id="chkJoinCommunity" name="chkJoinCommunity" runat="server" text="Join a community" />
															</span>
														<li>
															<span class="formKanevaCheck">
																<asp:CheckBox id="chkRave3DHome" visible="false" name="chkRave3DHome" runat="server" text="Rave a 3D home" />
															</span>
														<li>
															<span class="formKanevaCheck">
																<asp:CheckBox id="chkRave3DHangout" visible="false" name="chkRave3DHangout" runat="server" text="Rave a 3D hangout" />
															</span>
														<li>
															<span class="formKanevaCheck">
																<asp:CheckBox id="chkSendGift" visible="false" name="chkSendGift" runat="server" text="Send a gift" />
															</span>
														<li>
															<span class="formKanevaCheck" style="FONT-WEIGHT: bold"><br>
																<asp:CheckBox id="chkShareNone" name="chkShareNone" runat="server" autopostback="true" text="Keep all my Activities private" />
															</span>
														</li>			
													</ul>
												</td>
											</tr>
										</table>
										</ajax:ajaxpanel>
										<span class="cb"><span class="cl"></span></span>
									</div>	
								</td>
								<td width="4%" valign="top" align="left">&nbsp;</td>								
								<td width="48%" valign="top" align="left">
									<div class="module whitebg">
									<span class="ct"><span class="cl"></span></span>

										<h2>What activities I see</h2>
										
										<ajax:ajaxpanel id="Ajaxpanel1" runat="server">

										<table cellpadding="3" cellspacing="0" width="95%" align="center" border="0">
											<tr>
												<td colspan="2">
													<span class="bold" style="padding-left: 10px">Show activities when friends...</span>
												</td>
											</tr>
											<tr>
												<td colspan="2">
													<ul>
														<li>
															<span class="formKanevaCheck">
																<asp:CheckBox id="chkShowProfiles" name="chkShowProfiles" runat="server" text="Update profile theme" />
															</span>
														<li>
															<span class="formKanevaCheck">
																<asp:CheckBox id="chkShowBlogcomments" name="chkShowBlogcomments" runat="server" Text="Write a blog entry" />
															</span>
														<li>
															<span class="formKanevaCheck">
																<asp:CheckBox id="chkShowComment" name="chkComment" runat="server" text="Comment on media details" />
															</span>
														<li>
															<span class="formKanevaCheck">
																<asp:CheckBox id="chkShowForum" name="chkShowForum" runat="server" text="Post in a forum" />
															</span>
														<li>
															<span class="formKanevaCheck">
																<asp:CheckBox id="chkShowJoinCommunity" name="chkShowJoinCommunity" runat="server" text="Join a community" />
															</span>
														<li>
															<span class="formKanevaCheck">
																<asp:CheckBox id="chkShowRave3DHome" visible="false" name="chkShowRave3DHome" runat="server" text="Rave a 3D home" />
															</span>
														<li>
															<span class="formKanevaCheck">
																<asp:CheckBox id="chkShowRave3DHangout" visible="false" name="chkShowRave3DHangout" runat="server" text="Rave a 3D hangout" />
															</span>
														<li>
															<span class="formKanevaCheck">
																<asp:CheckBox id="chkShowSendGift" visible="false" name="chkShowSendGift" runat="server" text="Send a gift" />
															</span>
														<li>
															<span class="formKanevaCheck" style="FONT-WEIGHT: bold"><br>
																<asp:CheckBox id="chkShowNone" name="chkShowNone" runat="server" autopostback="true" text="Hide all friends' Activities" />
															</span>
														</li>			
													</ul>
												</td>
											</tr>
										</table>
										</ajax:ajaxpanel>
										<span class="cb"><span class="cl"></span></span>
									</div>	
								</td>
							</tr>
							<tr>
								<td colspan="3" align="right">
									<asp:button id="btnUpdate" runat="Server" CausesValidation="False" class="Filter2" Text="  submit  "/>
									<asp:button id="btnCancel" runat="Server" CausesValidation="False" class="Filter2" Text="  cancel  "/>
								</td>
							</tr>
							<tr>
								<td height="10px" colspan="3">
									&nbsp;
								</td>
							</tr>
						</table>
						
						<table border="0" cellspacing="0" cellpadding="0" width="99%">
							<tr>
								<td colspan="3">
									
									
									
									
										<table border="0" cellspacing="0" cellpadding="0" align="center">
											<tr>
												<td colspan="3"><h2>Important Note</h2></td>
											</tr>
											<tr>
												<td valign="top">
													Kaneva will never publish activities about:
													<ul class="bullet small">
														<li>People you reject as friends 
                          
														<li>People you remove from your 
                          friends 
                          
														<li>Messages 
                          
														<li>Whose profile you view 
                          
														<li>Whose photos you view 
                          
														<li>Whose notes you read 
                          
														<li>Groups and Events you decline 
                          to join 
                          
														<li>Notes and photos you delete</li>
													</ul>																
												</td>
												<td width="150"></td>
												<td valign="top">
													Kaneva may publish activities about:
													<ul class="bullet small">
														<li>Things you add to your profile 
                          
														<li>Photos you upload 
                          
														<li>Blogs you write 
                          
														<li>Communities you join or create 
                          
														<li>Awards you have received 
                          
														<li>Gifts you have been given 
                          
														<li>Content you've posted to your profile</li>
													</ul>	
												</td>
											</tr>
											<tr>
												<td colspan="3" align="center"><Br>For more info, <a href="#">visit our help page</a>.</td>
											</tr>
										</table>
									
									<br>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
		<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" id="Img5" width="1" height="1" ></td>
	</tr>
	<tr>
		<td class="frBottomLeft"></td>
		<td class="frBottom"></td>
		<td class="frBottomRight"></td>
	</tr>
	
</table>
