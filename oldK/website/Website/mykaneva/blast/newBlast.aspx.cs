///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using KlausEnt.KEP.Kaneva.channel;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using log4net;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for newBlast.
	/// </summary>
	public class newBlast : MainTemplatePage
	{
		protected newBlast () 
		{
			Title = "New Blast";
		}

        #region Page Load

		private void Page_Load(object sender, System.EventArgs e)
		{
			if (!Request.IsAuthenticated)
			{
				Response.Redirect (this.GetLoginURL ());
            }

            if (!IsPostBack)
			{
              
				// Set the screen to initial display
                SetBlastTypeDisplay ();

                //set admin options configuration
                bool isAdmin = IsAdministrator();
                toggleAdminOpts.Visible = isAdmin;
                if (isAdmin)
                {
                    chkPayHighlight.Checked = false;
                    chkPayHighlight.Visible = false;

                    txtBlast.MaxHTMLLength = 0;
                    txtBlast.MaxTextLength = 0;
                }
                else
                {
                    chkPayHighlight.Checked = false;
                    chkPayHighlight.Visible = true;
                }
                //chkPayHighlight.Checked = chkPayHighlight.Visible = !isAdmin; //admins can't buy highlighting
                tblAdmin.Visible = false;

                GetRequestValues ();

                aBlastTypeText.HRef = ResolveUrl("~/mykaneva/blast/newblast.aspx?bt=" + ((int)BlastTypes.BASIC).ToString() + "&communityId=" + CommunityId.ToString());
                aBlastTypeVideo.HRef = ResolveUrl("~/mykaneva/blast/newblast.aspx?bt=" + ((int)BlastTypes.VIDEO).ToString() + "&communityId=" + CommunityId.ToString());
                aBlastTypePhoto.HRef = ResolveUrl("~/mykaneva/blast/newblast.aspx?bt=" + ((int)BlastTypes.PHOTO).ToString() + "&communityId=" + CommunityId.ToString());
                aBlastTypeLink.HRef = ResolveUrl("~/mykaneva/blast/newblast.aspx?bt=" + ((int)BlastTypes.LINK).ToString() + "&communityId=" + CommunityId.ToString());
                aBlastTypePlace.HRef = ResolveUrl("~/mykaneva/blast/newblast.aspx?bt=" + ((int)BlastTypes.PLACE).ToString() + "&communityId=" + CommunityId.ToString());


                // If CommunityId = 0 then user is coming from MyKaneva, so show communities drop-down
                if (CommunityId == 0)
                {
                    // Set the communities dropdown
                    DataSet dataSource = new DataSet ();
                    DataTable dtOwnerCommunities = UsersUtility.GetUserCommunities (GetUserId (), "name", "cm.has_edit_rights = 'Y' AND place_type_id<99", 1, 50); //CommunityUtility.GetPublishChannels(GetUserId(), "name");
                    ddlChannels.DataSource = dtOwnerCommunities;
                    ddlChannels.DataTextField = "name";
                    ddlChannels.DataValueField = "community_id";
                    ddlChannels.DataBind ();
                    //store users channels
                    dataSource.Tables.Add (dtOwnerCommunities);
                    UserChannelsDataSource = dataSource;

                    ddlChannels.Items.Insert (0, new ListItem ("Select Community...", "0"));
                    ddlChannels.Visible = dtOwnerCommunities.Rows.Count > 0;
                    tblMyCommunities.Visible = true;
                }
                else // If CommunityId > 0, then show label of which community user is blasting to
                {
                    divBlastToCommunityName.Visible = true;
                    divBlastToCommunityName.InnerText = "Blast to " + GetCommunityFacade.GetCommunity (CommunityId).Name;
                }


                //pull user info and store
                UserName = KanevaWebGlobals.CurrentUser.Username;
                UserNameNoSpaces = KanevaWebGlobals.CurrentUser.NameNoSpaces;
                UserIcon = KanevaWebGlobals.CurrentUser.ThumbnailSquarePath;

                //pull user account info and set default highlighting
                UserBalance = UsersUtility.getUserBalance(GetUserId(), Constants.CURR_KPOINT);
                ConfigurePayHighlighting();


				// Set up event and place dropdowns
				DataTable dtUserChannels = UsersUtility.GetUserCommunities (GetUserId(), "name", "place_type_id<99", 1, 50);
                ddlPlaceChannels.DataSource = dtUserChannels;
                ddlPlaceChannels.DataTextField = "name";
                ddlPlaceChannels.DataValueField = "name";
                ddlPlaceChannels.DataBind ();
                ddlPlaceChannels.Items.Insert (0, new ListItem ("My Home", "My Home"));
                ddlPlaceChannels.Items.Insert (0, new ListItem ("Select...", ""));

                //default the calendar to current date
                txtAdminDate.Text = DateTime.Now.ToShortDateString().Replace('/', '-');

                divBlastConfirm.Visible = false;
			}

			// Date validators
			revDate1.ValidationExpression = Constants.VALIDATION_REGEX_DATE;
        }

        #endregion Page Load


        #region Helper Methods

        /// <summary>
        /// SetBlastTypeDisplay
        /// </summary>
        private void SetBlastTypeDisplay ()
        {
            int blastType = (int) BlastTypes.BASIC;

            if (Request.QueryString["bt"] != null && Request.QueryString["bt"] != string.Empty)
            {
                try
                {
                    switch (Convert.ToInt32(Request.QueryString["bt"]))
                    {
                        case (int)BlastTypes.VIDEO:
                        case (int)BlastTypes.PHOTO:
                        case (int)BlastTypes.LINK:
                        case (int)BlastTypes.EVENT:
                        case (int)BlastTypes.PLACE:
                            {
                                blastType = Convert.ToInt32 (Request.QueryString["bt"]); 
                                break;
                            }
                    }
                }
                catch {}
            }

            SetBlastTypeDisplay (blastType);
        }
        private void SetBlastTypeDisplay(int blastType)
        {
            CurrentTab = blastType;
            switch (blastType)
            {
                case (int)BlastTypes.VIDEO:
                    {
                        tblBlastText.Visible = false;
                        tblBlastVideo.Visible = true;
                        tblBlastPhoto.Visible = false;
                        tblBlastLink.Visible = false;
                        tblBlastPlace.Visible = false;

                        liText.Attributes.Clear();
                        liVideo.Attributes.Add("class", "selected");
                        liPhoto.Attributes.Clear();
                        liLink.Attributes.Clear();
                        liPlace.Attributes.Clear();

                        string adminScriptString = "<script language=JavaScript>";
                        adminScriptString += "initAdminCal();";
                        adminScriptString += "</script>";

                        if (IsAdministrator() && !ClientScript.IsClientScriptBlockRegistered(GetType(), "initAdminCal"))
                        {
                            ClientScript.RegisterStartupScript(GetType(), "initAdminCal", adminScriptString);
                        }
                        break;
                    }
                case (int)BlastTypes.PHOTO:
                    {
                        tblBlastText.Visible = false;
                        tblBlastVideo.Visible = false;
                        tblBlastPhoto.Visible = true;
                        tblBlastLink.Visible = false;
                        tblBlastPlace.Visible = false;

                        liText.Attributes.Clear();
                        liVideo.Attributes.Clear();
                        liPhoto.Attributes.Add("class", "selected");
                        liLink.Attributes.Clear();
                        liPlace.Attributes.Clear();

                        string adminScriptString = "<script language=JavaScript>";
                        adminScriptString += "initAdminCal();";
                        adminScriptString += "</script>";

                        if (IsAdministrator() && !ClientScript.IsClientScriptBlockRegistered(GetType(), "initAdminCal"))
                        {
                            ClientScript.RegisterStartupScript(GetType(), "initAdminCal", adminScriptString);
                        }
                        break;
                    }
                case (int)BlastTypes.LINK:
                    {
                        tblBlastText.Visible = false;
                        tblBlastVideo.Visible = false;
                        tblBlastPhoto.Visible = false;
                        tblBlastLink.Visible = true;
                        tblBlastPlace.Visible = false;

                        liText.Attributes.Clear();
                        liVideo.Attributes.Clear();
                        liPhoto.Attributes.Clear();
                        liLink.Attributes.Add("class", "selected");
                        liPlace.Attributes.Clear();

                        string adminScriptString = "<script language=JavaScript>";
                        adminScriptString += "initAdminCal();";
                        adminScriptString += "</script>";

                        if (IsAdministrator() && !ClientScript.IsClientScriptBlockRegistered(GetType(), "initAdminCal"))
                        {
                            ClientScript.RegisterStartupScript(GetType(), "initAdminCal", adminScriptString);
                        }
                        break;
                    }
                case (int)BlastTypes.PLACE:
                    {
                        tblBlastText.Visible = false;
                        tblBlastVideo.Visible = false;
                        tblBlastPhoto.Visible = false;
                        tblBlastLink.Visible = false;
                        tblBlastPlace.Visible = true;

                        liText.Attributes.Clear();
                        liVideo.Attributes.Clear();
                        liPhoto.Attributes.Clear();
                        liLink.Attributes.Clear();
                        liPlace.Attributes.Add("class", "selected");

                        string adminScriptString = "<script language=JavaScript>";
                        adminScriptString += "initAdminCal();";
                        adminScriptString += "</script>";

                        if (IsAdministrator() && !ClientScript.IsClientScriptBlockRegistered(GetType(), "initAdminCal"))
                        {
                            ClientScript.RegisterStartupScript(GetType(), "initAdminCal", adminScriptString);
                        }
                        break;
                    }
                case (int)BlastTypes.BASIC:
                default:
                    {
                        tblBlastText.Visible = true;
                        tblBlastVideo.Visible = false;
                        tblBlastPhoto.Visible = false;
                        tblBlastLink.Visible = false;
                        tblBlastPlace.Visible = false;

                        liText.Attributes.Add("class", "selected");
                        liVideo.Attributes.Clear();
                        liPhoto.Attributes.Clear();
                        liLink.Attributes.Clear();
                        liPlace.Attributes.Clear();

                        string adminScriptString = "<script language=JavaScript>";
                        adminScriptString += "initAdminCal();";
                        adminScriptString += "</script>";

                        if (IsAdministrator() && !ClientScript.IsClientScriptBlockRegistered(GetType(), "initAdminCal"))
                        {
                            ClientScript.RegisterStartupScript(GetType(), "initAdminCal", adminScriptString);
                        }
                       break;
                    }
            }
        }

        private string GetBlastTypeString (int blastType)
        {
            string type = "";

            switch (blastType)
            {
                case (int) BlastTypes.VIDEO:
                    type = "[video]";
                    break;

                case (int) BlastTypes.LINK:
                    type = "[link]";
                    break;

                case (int) BlastTypes.PHOTO:
                    type = "[photo]";
                    break;

                case (int) BlastTypes.PLACE:
                    type = "[place]";
                    break;
            }

            return type;
        }

        //used to be able to dynamically adjust the preview section
        private string GetTitleInfo()
        {
            BlastFacade blastFacade = new BlastFacade();

            string titleText = "";
            switch (CurrentTab)
            {
                case (int)BlastTypes.VIDEO:
                    titleText = txtVideoCaption.Text;
                    break;
                case (int)BlastTypes.PHOTO:
                    titleText = txtPhotoCaption.Text;
                    break;
                case (int)BlastTypes.LINK:
                    titleText = txtLinkDescription.Text;
                    break;
                case (int)BlastTypes.PLACE:
                    titleText = txtPlaceDescription.Text;
                    break;
                case (int)BlastTypes.BASIC:
                default:
                    titleText = txtBlast.Text;
                    break;
            }

            return TruncateWithEllipsis (blastFacade.StripHTML(titleText, ""), 28);
        }

        private bool ContainsRestrictedWords ()
        {
            bool isBad = false;

            // Text Blast
            if (tblBlastText.Visible)
            {
                // Check to make sure user did not enter any "potty mouth" words
                if (KanevaWebGlobals.isTextRestricted (txtBlast.Text, Constants.eRESTRICTION_TYPE.POTTY_MOUTH))
                {
                    isBad = true;
                }
            }
            // Video Blast
            else if (tblBlastVideo.Visible)
            {
                // Check to make sure user did not enter any "potty mouth" words
                if (KanevaWebGlobals.isTextRestricted (txtVideoCaption.Text, Constants.eRESTRICTION_TYPE.POTTY_MOUTH) ||
                    KanevaWebGlobals.isTextRestricted (txtVideoEmbed.Text, Constants.eRESTRICTION_TYPE.POTTY_MOUTH))
                {
                    isBad = true;      
                }
            }

            // Photo
            else if (tblBlastPhoto.Visible)
            {
                // Check to make sure user did not enter any "potty mouth" words
                if (KanevaWebGlobals.isTextRestricted (txtPhotoCaption.Text, Constants.eRESTRICTION_TYPE.POTTY_MOUTH))
                {
                    isBad = true;
                }
            }
            // Link
            else if (tblBlastLink.Visible)
            {
                // Check to make sure user did not enter any "potty mouth" words
                if (KanevaWebGlobals.isTextRestricted (txtLinkName.Text, Constants.eRESTRICTION_TYPE.POTTY_MOUTH) ||
                    KanevaWebGlobals.isTextRestricted (txtLinkDescription.Text, Constants.eRESTRICTION_TYPE.POTTY_MOUTH))
                {
                    isBad = true;
                }
            }
            // Place
            else if (tblBlastPlace.Visible)
            {
                // Check to make sure user did not enter any "potty mouth" words
                if (KanevaWebGlobals.isTextRestricted (txtPlaceLocation.Text, Constants.eRESTRICTION_TYPE.POTTY_MOUTH) ||
                    KanevaWebGlobals.isTextRestricted (txtPlaceAddress.Text, Constants.eRESTRICTION_TYPE.POTTY_MOUTH) ||
                    KanevaWebGlobals.isTextRestricted (txtPlaceDescription.Text, Constants.eRESTRICTION_TYPE.POTTY_MOUTH))
                {
                    isBad = true;
                }
            }

            return isBad;
        }
        private bool ContainsInjectionScripts ()
        {
            bool isBad = false;

            // Text Blast
            if (tblBlastText.Visible)
            {
                // Check to make sure user did not enter any "potty mouth" words
                if (KanevaWebGlobals.ContainsInjectScripts (txtBlast.Text))
                {
                    isBad = true;
                }
            }
            // Video Blast
            else if (tblBlastVideo.Visible)
            {
                // Check to make sure user did not enter any "potty mouth" words
                if (KanevaWebGlobals.ContainsInjectScripts (txtVideoCaption.Text) ||
                    KanevaWebGlobals.ContainsInjectScripts (txtVideoEmbed.Text))
                {
                    isBad = true;                 
                }
            }

            // Photo
            else if (tblBlastPhoto.Visible)
            {
                // Check to make sure user did not enter any "potty mouth" words
                if (KanevaWebGlobals.ContainsInjectScripts (txtPhotoCaption.Text) ||
                    KanevaWebGlobals.ContainsInjectScripts (txtPhotoURL.Text))
                {
                    isBad = true;
                }
            }
            // Link
            else if (tblBlastLink.Visible)
            {
                // Check to make sure user did not enter any "potty mouth" words
                if (KanevaWebGlobals.ContainsInjectScripts (txtLinkName.Text) ||
                    KanevaWebGlobals.ContainsInjectScripts (txtLinkDescription.Text))
                {
                    isBad = true;
                }
            }
            // Place
            else if (tblBlastPlace.Visible)
            {
                // Check to make sure user did not enter any "potty mouth" words
                if (KanevaWebGlobals.ContainsInjectScripts (txtPlaceLocation.Text) ||
                    KanevaWebGlobals.ContainsInjectScripts (txtPlaceAddress.Text)  ||
                    KanevaWebGlobals.ContainsInjectScripts (txtPlaceDescription.Text))
                {
                    isBad = true;
                }
            }

            return isBad;
        }
        private bool BlastTextExceedsMaxLen ()
        {
            bool isBad = false;

            // Text Blast
            if (tblBlastText.Visible)
            {
                if (txtBlast.Text.Length > KanevaGlobals.MaxBlastLength)
                {
                    isBad = true;
                }
            }
            // Video Blast
            else if (tblBlastVideo.Visible)
            {
                if (txtVideoCaption.Text.Length > KanevaGlobals.MaxBlastLength)
                {
                    isBad = true;
                }
            }

            // Photo
            else if (tblBlastPhoto.Visible)
            {
                if (txtPhotoCaption.Text.Length > KanevaGlobals.MaxBlastLength)
                {
                    isBad = true;
                }
            }
            // Link
            else if (tblBlastLink.Visible)
            {
                if (txtLinkDescription.Text.Length > KanevaGlobals.MaxBlastLength)
                {
                    isBad = true;
                }
            }
            // Place
            else if (tblBlastPlace.Visible)
            {
                if (txtPlaceDescription.Text.Length > KanevaGlobals.MaxBlastLength)
                {
                    isBad = true;
                }
            }

            return isBad;
        }

        /// <summary>
        /// SendBlast
        /// </summary>
        /// <returns></returns>
        public bool SendBlast()
        {
            BlastFacade blastFacade = new BlastFacade();
            string username = "";
            string userNameNoSpaces = "";
            string thumbnailPath = "";
            string communityName = "";
            int userId = GetUserId();
            int successfulBlast = 0;
            int successfulPayment = -1;
            int communityId = CommunityId;
            Community community = new Community ();
            
            // Server validation
            Page.Validate();
            if (!Page.IsValid)
            {
                return false;
            }

            if (ContainsInjectionScripts ())
            {
                m_logger.Warn ("User " + userId + " tried to script from IP " + Common.GetVisitorIPAddress());
                MagicAjax.AjaxCallHelper.WriteAlert ("Your input contains invalid scripting, please remove script code and try again.");
                return false;
            }
            if (ContainsRestrictedWords ())
            {
                MagicAjax.AjaxCallHelper.WriteAlert (Constants.VALIDATION_REGEX_RESTRICTED_WORD_MESSAGE);
                return false;
            }

            if (BlastTextExceedsMaxLen ())
            {
                MagicAjax.AjaxCallHelper.WriteAlert ("Blast message is too long.");
                return false;

            }

            string cssHighLight = "";
            bool isCommunityBlast = false;

            // Only try and get the community id from the drop down if the community id is not already set
            if (ddlChannels.Visible)
            {
                try
                {
                    communityId = Convert.ToInt32 (ddlChannels.SelectedValue);
                }
                catch (FormatException)
                {
                    communityId = 0;
                }
            }

            username = UserName;
            userNameNoSpaces = UserNameNoSpaces;

            //get sender information. Assign dependant on Community or not
            if (communityId > 0)
            {
                isCommunityBlast = true;

                community = GetCommunityFacade.GetCommunity (communityId);

                if (community.CommunityId > 0)
                {
                    thumbnailPath = KanevaWebGlobals.CurrentUser.ThumbnailSquarePath;
                    IsCommunityPersonal = community.IsPersonal.Equals (1);
                    
                    if (IsCommunityPersonal)
                    {
                        // If user is not a friend then they can not blast to this profile
                        if (!GetUserFacade.AreFriends (userId, community.CreatorId) &&
                            !HasWritePrivileges((int)SitePrivilege.ePRIVILEGE.USER_PROFILE_ADMIN))
                        {
                            MagicAjax.AjaxCallHelper.WriteAlert ("You are not friends with this user.  You must be friends to blast to this user.");
                            return false;
                        }

                        // If current user is posting to their profile, make community id 0 so blast will go
                        // out to all of the users friends.  Same as if user posted from MyKaneva page
                        if (userId == community.CreatorId)
                        {
                            communityId = 0;
                        }
                    }
                    else 
                    {
                        // If user is not an active member of community then they can not blast to community
                        if (!GetCommunityFacade.IsActiveCommunityMember (community.CommunityId, userId) &&
                            !HasWritePrivileges ((int) SitePrivilege.ePRIVILEGE.COMMUNITY_PROFILE_ADMIN))
                        {
                            MagicAjax.AjaxCallHelper.WriteAlert ("You are not a member of this World.  You must be a member to blast to this World.");
                            return false;
                        }

                        //  Members must be fame level 20 or above
                        CommunityFacade communityFacade = new CommunityFacade();
                        CommunityMember cm = communityFacade.GetCommunityMember(community.CommunityId, userId);

                        if (cm.AccountTypeId.Equals(3))
                        {
                            FameFacade fameFacade = new FameFacade();
                            UserFame userFame = fameFacade.GetUserFame(userId, (int)FameTypes.World);

                            if (userFame.LevelId < Configuration.LevelIdToBlastCommunity)
                            {
                                MagicAjax.AjaxCallHelper.WriteAlert("Members must be World Fame Level " + Configuration.LevelIdToBlastCommunity.ToString() + " to blast to this World.");
                                return false;
                            }
                        }



                        // If a user is sending a message to a community in which he is the owner, display
                        // the thumb of the community.  This will make the blast from the community instead
                        // of comming from an individual user.
                        if (userId == community.CreatorId)
                        {
                            thumbnailPath = community.ThumbnailSmallPath;
                        }
                    }

                    communityName = community.Name;

                    // Is current user the community owner?
                    if (userId == community.CreatorId)
                    {
                        username = communityName;
                        userNameNoSpaces = username.Replace (" ", "");
                    }
                }
                else  // Error retrieving community
                {
                    m_logger.Warn ("Community was not retrieved from db successfully.");
                    MagicAjax.AjaxCallHelper.WriteAlert ("An error was encountered trying to process your blast.  Please try again.");
                    return false;
                }
            }
            else  
            {
                if (KanevaWebGlobals.CurrentUser.FacebookSettings.UseFacebookProfilePicture &&
                    KanevaWebGlobals.CurrentUser.FacebookSettings.FacebookUserId > 0)
                {
                    thumbnailPath = GetFacebookProfileImageUrl (KanevaWebGlobals.CurrentUser.FacebookSettings.FacebookUserId, "sq");
                }
                else
                {
                    thumbnailPath = UserIcon;
                }
            }

            // Did they pay for a highlight?
            if ((chkPayHighlight.Visible) && (chkPayHighlight.Checked))
            {
                //verify user wants to buy logic
                try
                {
                    // Pay for the highlight
                    successfulPayment = GetUserFacade.AdjustUserBalance(userId, Constants.CURR_KPOINT, -50.0, Constants.CASH_TT_BOUGHT_ITEM);
                }
                catch (Exception)
                {
                    ShowErrorOnStartup("Not enough credits to purchase highlight.");
                    return false;
                }

                if (successfulPayment == 0)//0 is success according to AdjustUserBalance notes
                {
                    // Deduct balances XXX
                    cssHighLight = "highlight1";
                }
            }

            string blastText = "";

            // Text Blast
            if (tblBlastText.Visible)
            {
                if (chkKanevaBlast.Checked && IsAdministrator())
                {
                    cssHighLight = rblColor.SelectedItem.Value;

                    DateTime dtStartDate = KanevaGlobals.UnFormatDate(txtAdminDate.Text + " " + drpAdminTime.SelectedValue);
                    successfulBlast = blastFacade.SendAdminTextBlast (userId, dtStartDate, username, userNameNoSpaces, txtBlast.Text, cssHighLight, thumbnailPath, isCommunityBlast);
                }
                else
                {
                    successfulBlast = blastFacade.SendTextBlast (userId, communityId, communityName, IsCommunityPersonal, username, userNameNoSpaces, txtBlast.Text, cssHighLight, thumbnailPath, isCommunityBlast);
                }

                blastText = txtBlast.Text;
            }
            // Video Blast
            else if (tblBlastVideo.Visible)
            {
                try
                {
                    //check to see if it is a kaneva approved video
                    string embedCode = KanevaWebGlobals.ParseKanevaApprovedEmbedCode(txtVideoEmbed.Text, "assetId%3d-", "&");

                    if (chkKanevaBlast.Checked && IsAdministrator())
                    {
                        cssHighLight = rblColor.SelectedItem.Value;

                        DateTime dtStartDate = KanevaGlobals.UnFormatDate(txtAdminDate.Text + " " + drpAdminTime.SelectedValue);
                        successfulBlast = blastFacade.SendAdminVideoBlast(userId, dtStartDate, username, userNameNoSpaces, txtVideoCaption.Text, Server.HtmlEncode(embedCode), cssHighLight, thumbnailPath, isCommunityBlast);
                    }
                    else
                    {
                        successfulBlast = blastFacade.SendVideoBlast (userId, communityId, communityName, IsCommunityPersonal, username, userNameNoSpaces, txtVideoCaption.Text, Server.HtmlEncode (embedCode), cssHighLight, thumbnailPath, isCommunityBlast);
                    }

                    blastText = txtVideoCaption.Text + "<br/><br/>";
                }
                catch (Exception ex)
                {
                    m_logger.Warn(ex.Message + " newBlast.aspx.cs during video blast");
                    MagicAjax.AjaxCallHelper.WriteAlert(ex.Message);
                }
            }
            // Photo
            else if (tblBlastPhoto.Visible)
            {
                // Try to pull the image url out of the input string
                Match match = Regex.Match (txtPhotoURL.Text, "(http\\:\\/\\/)?[a-zA-Z0-9\\-\\.]+[a-zA-Z0-9\\-\\.]+\\.[a-zA-Z]{2,3}(?:\\/\\S*)?(?:[a-zA-Z0-9_])+\\.(?:jpg|jpeg|gif|png)", RegexOptions.IgnoreCase);

                if (match.Length > 0)
                {
                    string strLink = Server.HtmlEncode (match.Value);
                    if (!strLink.StartsWith ("http://"))
                    {
                        strLink = "http://" + strLink;
                    }

                    strLink = "<a href=\"" + strLink + "\" target=\"_blank\"><img src=\"" + strLink + "\"/></a>";

                    if (chkKanevaBlast.Checked && IsAdministrator ())
                    {
                        cssHighLight = rblColor.SelectedItem.Value;

                        DateTime dtStartDate = KanevaGlobals.UnFormatDate (txtAdminDate.Text + " " + drpAdminTime.SelectedValue);
                        successfulBlast = blastFacade.SendAdminPhotoBlast (userId, dtStartDate, username, userNameNoSpaces, strLink, txtPhotoCaption.Text, cssHighLight, thumbnailPath, isCommunityBlast);
                    }
                    else
                    {
                        successfulBlast = blastFacade.SendPhotoBlast (userId, communityId, communityName, IsCommunityPersonal, username, userNameNoSpaces, strLink, txtPhotoCaption.Text, cssHighLight, thumbnailPath, isCommunityBlast);
                    }

                    blastText = txtPhotoCaption.Text + "<br/><br/>" + strLink; 
                }
                else
                {
                    // Could not find image string.  Message user.
                    MagicAjax.AjaxCallHelper.WriteAlert ("The Photo URL is not in a recognizable format.  Please re-enter in correct format.\r\n\r\nExample: http://www.kaneva.com/images/klogo.gif");
                    return false;
                }
            }
            // Link
            else if (tblBlastLink.Visible)
            {
                Match match = Regex.Match (txtLinkURL.Text, "(http|ftp|https):\\/\\/[\\w\\-_]+(\\.[\\w\\-_]+)+([\\w\\-\\.,@?^=%&amp;:/~\\+#]*[\\w\\-\\@?^=%&amp;/~\\+#])?", RegexOptions.IgnoreCase);

                if (match.Length > 0)
                {
                    string strLink = Server.HtmlEncode (match.Value);
                    if (!strLink.StartsWith ("http://"))
                    {
                        strLink = "http://" + strLink;
                    }

                    if (chkKanevaBlast.Checked && IsAdministrator ())
                    {
                        cssHighLight = rblColor.SelectedItem.Value;

                        DateTime dtStartDate = KanevaGlobals.UnFormatDate (txtAdminDate.Text + " " + drpAdminTime.SelectedValue);
                        successfulBlast = blastFacade.SendAdminLinkBlast (userId, dtStartDate, username, userNameNoSpaces, Server.HtmlEncode (txtLinkName.Text), strLink, txtLinkDescription.Text, cssHighLight, thumbnailPath, isCommunityBlast);
                    }
                    else
                    {
                        successfulBlast = blastFacade.SendLinkBlast (userId, communityId, communityName, IsCommunityPersonal, username, userNameNoSpaces, Server.HtmlEncode (txtLinkName.Text), strLink, txtLinkDescription.Text, cssHighLight, thumbnailPath, isCommunityBlast);
                    }

                    blastText = txtLinkDescription.Text + "<br/><br/>" + "<a href=\"" + strLink + "\">" + txtLinkName.Text + "</a>";
                }
                else
                {
                    // Could not find link url string.  Message user.
                    MagicAjax.AjaxCallHelper.WriteAlert ("The Link URL is not in a recognizable format.  Please re-enter in correct format.\r\n\r\nExample: http://www.kaneva.com/");
                    return false;
                }
            }
            // Place
            else if (tblBlastPlace.Visible)
            {
                string location = Server.HtmlEncode(txtPlaceLocation.Text);
                if (ddlPlaceChannels.SelectedValue.Length > 0)
                {
                    location = ddlPlaceChannels.SelectedItem.Value;
                }

                if (chkKanevaBlast.Checked && IsAdministrator())
                {
                    cssHighLight = rblColor.SelectedItem.Value;

                    DateTime dtStartDate = KanevaGlobals.UnFormatDate(txtAdminDate.Text + " " + drpAdminTime.SelectedValue);
                    successfulBlast = blastFacade.SendAdminPlaceBlast (userId, dtStartDate, username, userNameNoSpaces, location, Server.HtmlEncode (txtPlaceAddress.Text), txtPlaceDescription.Text, cssHighLight, thumbnailPath, isCommunityBlast);
                }
                else
                {
                    successfulBlast = blastFacade.SendPlaceBlast (userId, communityId, communityName, IsCommunityPersonal, username, userNameNoSpaces, location, Server.HtmlEncode (txtPlaceAddress.Text), txtPlaceDescription.Text, cssHighLight, thumbnailPath, isCommunityBlast);
                }

                blastText = txtPlaceDescription.Text + "<br/><br/>Location: " + location + "<br/>Address: " + txtPlaceAddress.Text; 
            }

            // If community owner is blasting to the community, then send emails to all the
            // members of the community that the community created a new blast
            if (successfulBlast > 0 && !IsCommunityPersonal && KanevaWebGlobals.CurrentUser.UserId == community.CreatorId)
            {
                // Get list of members of community, filter out community owner which is the current user
                PagedList<CommunityMember> members = GetCommunityFacade.GetCommunityMembers (community.CommunityId, (UInt32) CommunityMember.CommunityMemberStatus.ACTIVE,
                    false, false, false, false, "u.user_id <> " + KanevaWebGlobals.CurrentUser.UserId, "", 1, int.MaxValue, true, "");

                // If we are in test mode then only send emails for the community set in the web.config. This is so the tester
                // will not get flooded with emails if world owners happen to send blasts during the testing time.
                if (members.Count > 0)
                {
                    foreach (CommunityMember member in members)
                    {
                        if (member.NotificationPreference.NotifyWorldBlasts && member.StatusId == (uint) global::Kaneva.BusinessLayer.BusinessObjects.User.eUSER_STATUS.REGVALIDATED)
                        {
                            // They want emails for world/community blasts
                            MailUtilityWeb.SendBlastNotificationEmail (IsCommunityPersonal, member.Email, Server.HtmlEncode (txtBlast.Text), member.UserId, KanevaWebGlobals.CurrentUser.UserId, community.CommunityId);
                        }
                    }
                }
            }


            //check success status of blast
            //if payment taken but blast failed return payment
            if ((successfulPayment == 0) && (successfulBlast <= 0))
            {
                try
                {
                    // refund for the highlight
                    GetUserFacade.AdjustUserBalance(userId, Constants.CURR_KPOINT, 50.0, Constants.CASH_TT_REFUND);
                }
                catch (Exception)
                {
                    ShowErrorOnStartup("Refund Failed.");
                    return false;
                }
            }
            else if ((successfulPayment <= 0) && (successfulBlast <= 0))
            {
                return false;
            }

            return true;
        }

        public void ConfigurePayHighlighting ()
        {
            ConfigurePayHighlighting (false);
        }
        public void ConfigurePayHighlighting (bool isPreview)
        {
            if (chkPayHighlight.Checked)
            {
                //configure buttons
                this.btn_refreshPreview.Visible = true;
                btnPreviewHighlight.Visible = true;
                btnPreview.Visible = false;

                currentBalance.Visible = true;
                balanceSummary.Visible = true;
                liBlastPreview.Attributes.Remove("class");
                liBlastPreview.Attributes.Add("class", "message highlight1");
                
                //check balance
                currentBalance.Text = "Current Credit Balance = " + UserBalance;
                if (UserBalance < 50.0)
                {
                    balanceSummary.Text = "<span style=\"color:red\">Insufficient Credits. </span><a href=\"" + ResolveUrl("~/myKaneva/buycredits.aspx") + "\">Add Credits</a>";
                }
                else
                {
                    balanceSummary.Text = "Balance after sending blast = " + (UserBalance - 50.0);
                }
                //display users balance
                
                //show preview
                if (isPreview)
                {
                    litPreviewImage.Text = "<img src=\"" + GetProfileImageURL (UserIcon, "sq", "M") + "\" border=\"0\" width=\"30\" />";
                    BlastFacade blastFacade = new BlastFacade ();
                    litBlastMsg.Text = "<a href=\"javascript:void(0);\">" + KanevaWebGlobals.CurrentUser.Username + "</a> blasts <a href=\"javascript:void(0);\">" + GetTitleInfo () + GetBlastTypeString (CurrentTab) + "</a> ";
                    litBlastMsg.Text += "<span class=\"time\">(" + FormatDateTimeSpan (DateTime.Now.AddMinutes (-1), DateTime.Now) + ")</span>";
                }
            }
            else
            {
                //configure buttons
                this.btn_refreshPreview.Visible = false;
                btnPreviewHighlight.Visible = false;
                btnPreview.Visible = true;

                currentBalance.Visible = false;
                balanceSummary.Visible = false;
                
                liBlastPreview.Attributes.Remove("class");
                liBlastPreview.Attributes.Add("class", "message");

                litPreviewImage.Text = "<img src=\"" + GetProfileImageURL (UserIcon, "sq", "M") + "\" border=\"0\" width=\"30\" />";
                litBlastMsg.Text = "<a href=\"javascript:void(0);\">" + KanevaWebGlobals.CurrentUser.Username + "</a> blasts <a href=\"javascript:void(0);\">Come check out my dance club!</a> ";
                litBlastMsg.Text += "<span class=\"time\">(" + FormatDateTimeSpan (DateTime.Now.AddMinutes (-10), DateTime.Now) + ")</span>";
            }
        }

        private void GetRequestValues ()
        {
            // Get the community Id
            if (Request["communityId"] != null && Request["communityId"] != "")
            {
                try
                {
                    CommunityId = Convert.ToInt32 (Request["communityId"]);
                }
                catch (Exception exc)
                {
                    m_logger.Error ("Error in GetRequestValues", exc);
                }
            }

        }

        #endregion


        #region Event Handlers

        /// <summary>
        /// Preview a Blast
        protected void btnPreview_Click(object sender, EventArgs e)
        {
            if (SendBlast())
            {
                try
                {
                    if (CommunityId > 0)
                    {
                        if (IsCommunityPersonal)
                        {
                            Response.Redirect ("~/channel/" + UserNameNoSpaces + ".people");
                        }
                        else
                        {
                            Response.Redirect ("~/community/CommunityPage.aspx?communityId=" + CommunityId.ToString ());
                        }
                    }
                }
                catch { }

                Response.Redirect("~/default.aspx");
            }
        }

        protected void lnkAdminToggle_Click(object sender, EventArgs e)
        {
                    
            if (tblAdmin.Visible)
            {
                toggleAdminOpts.Text = "Show Admin Options";
                tblAdmin.Visible = false;
            }
            else
            {
                toggleAdminOpts.Text = "Hide Admin Options";
                tblAdmin.Visible = true;
            }
            // reinitializes the calendar
            switch (CurrentTab)
            {
                case (int)BlastTypes.VIDEO:
                case (int)BlastTypes.PHOTO:
                case (int)BlastTypes.LINK:
                case (int)BlastTypes.PLACE:
                case (int)BlastTypes.BASIC:
                default:
                    {
                        string adminScriptString = "<script language=JavaScript>";
                        adminScriptString += "initAdminCal();";
                        adminScriptString += "</script>";

                        if (IsAdministrator() && !ClientScript.IsClientScriptBlockRegistered(GetType(), "initAdminCal"))
                        {
                            ClientScript.RegisterStartupScript(GetType(), "initAdminCal", adminScriptString);
                        }
                        break;
                    }
            }


        }

        /// <summary>
        /// ChangeBlastType_Command
        /// </summary>
        protected void ChangeBlastType_Command(object sender, CommandEventArgs e)
        {
            SetBlastTypeDisplay(Convert.ToInt32(e.CommandName));
        }

        public void PayHighlight_CheckedChanged(object sender, EventArgs e)
        {
            ConfigurePayHighlighting ();
        }

        public void btn_refreshPreview_Click (object sender, EventArgs e)
        {
            ConfigurePayHighlighting (true);
        }

        protected void btnBuyHighlightCancel_Click(object sender, EventArgs e)
        {
            divBlastConfirm.Visible = false;
            btnPreviewHighlight.Enabled = true;
            btn_refreshPreview.Enabled = true;
            chkPayHighlight.Enabled = true;
        }

        protected void btnPreviewHighlight_Click(object sender, EventArgs e)
        {
            divBlastConfirm.Visible = true;
            btnPreviewHighlight.Enabled = false;
            btn_refreshPreview.Enabled = false;
            chkPayHighlight.Enabled = false;
        }

        #endregion Event Handlers


        #region Properties

        public int CurrentTab
        {
            get
            {
                if (ViewState["currentTab"] == null)
                {
                    ViewState["currentTab"] = 0;
                }
                return (int)ViewState["currentTab"];
            }
            set
            {
                ViewState["currentTab"] = value;
            }
        }

        private DataSet UserChannelsDataSource
        {
            get
            {
                return (DataSet)ViewState["userchannels"];
            }
            set
            {
                ViewState["userchannels"] = value;
            }
        }

        private double UserBalance
        {
            get
            {
                return (double)ViewState["userbalance"];
            }
            set
            {
                ViewState["userbalance"] = value;
            }
        }

        private string UserName
        {
            get
            {
                return ViewState["username"].ToString();
            }
            set
            {
                ViewState["username"] = value;
            }
        }

        private string UserNameNoSpaces
        {
            get
            {
                return ViewState["usernamenospaces"].ToString();
            }
            set
            {
                ViewState["usernamenospaces"] = value;
            }
        }

        private string UserIcon
        {
            get
            {
                return ViewState["userThumbnail"].ToString();
            }
            set
            {
                ViewState["userThumbnail"] = value;
            }
        }

        private int CommunityId
        {
            get
            {
                if (ViewState["communityId"] != null)
                {
                    try
                    {
                        return Convert.ToInt32 (ViewState["communityId"].ToString ());
                    }
                    catch
                    {
                        return -1;
                    }
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                ViewState["communityId"] = value;
            }
        }

        private bool IsCommunityPersonal
        {
            get
            {
                return this.isCommunityPersonal;
            }
            set
            {
                this.isCommunityPersonal = value;
            }
        }

        #endregion Properties


        #region Declarations

        private bool isCommunityPersonal = false;

        protected DropDownList ddlChannels;
        protected Button btn_refreshPreview, btnBuyHighlight, btnBuyHighlightCancel, btnPreviewHighlight;
        protected LinkButton btnPreview;

        protected HtmlTable tblBlastText, tblBlastVideo, tblBlastPhoto, tblBlastLink, tblBlastPlace, tblAdmin, tblMyCommunities;
        protected LinkButton lbBlastText, lbBlastVideo, toggleAdminOpts;
		protected CheckBox chkPayHighlight;
        protected Literal litPreviewImage, litBlastMsg, currentBalance, balanceSummary;

        protected HtmlContainerControl liText, liVideo, liPhoto, liLink, liPlace, liBlastPreview;
        protected HtmlContainerControl divBlastToCommunityName, divBlastConfirm, divErrMsg;

		// Text
		protected CuteEditor.Editor txtBlast;

		// Video
		protected TextBox txtVideoEmbed;
		protected CheckBox chkYouTube;
		protected CuteEditor.Editor txtVideoCaption;


		// Photo
		protected CuteEditor.Editor txtPhotoCaption;
		protected TextBox txtPhotoURL;

		// Link
		protected TextBox txtLinkName, txtLinkURL;
		protected CuteEditor.Editor txtLinkDescription;

		// Place
		protected DropDownList ddlPlaceChannels;
		protected TextBox txtPlaceLocation, txtEventName, txtPlaceAddress;
        protected TextBox changeOccurred;
		protected CuteEditor.Editor txtPlaceDescription;

		// Admin 
		protected CheckBox chkKanevaBlast;
		protected RadioButtonList rblColor;
		protected TextBox txtAdminDate;
		protected DropDownList drpAdminTime;
		protected RegularExpressionValidator revDate1;
        protected RequiredFieldValidator rfvEventBlast2, rfvPlaceBlast1;

        protected HtmlAnchor aBlastTypeText;
        protected HtmlAnchor aBlastTypeVideo;
        protected HtmlAnchor aBlastTypePhoto;
        protected HtmlAnchor aBlastTypeLink;
        protected HtmlAnchor aBlastTypePlace;

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

        #endregion


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
        }

		#endregion
	}
}
