<%@ Page language="c#" Codebehind="blastDetails.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.blastDetails" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>

<link href="../../css/new.css" rel="stylesheet" type="text/css">

<script src="../../jscript/prototype.js" type="text/javascript" language="javascript"></script>

<script type="text/javascript"><!--

// Length of Comment
var textboxtextSave = '';
var maxChars = <asp:literal id="litMaxLen" runat="server"/>;

function CharsRemain(t, span_id)
{		
	var val = t.value.length;
	
	if (val > maxChars)
		t.value = textboxtextSave;
	else
	{		 
		$(span_id).innerHTML = maxChars-val+' ';
		textboxtextSave = t.value;
	}   
}
function ValidateLen (txtarea)
{	  
	txtarea.value = txtarea.value.stripScripts().strip();

	var len = txtarea.value.length;
	
	if (len == 0)
	{
		alert('You can not enter a blank reply.');
		return false;
	}
	else if (len > maxChars)
	{
		alert('Your reply is too long.  It must be ' + maxChars + ' characters or less.');
		return false;
	}
	
	return true;
}

//--> </script> 

<table border="0" cellspacing="0" cellpadding="0"  width="990" align="center" class="newcontainer">

	<tr>
		<td>
			<table  border="0" cellpadding="0" cellspacing="0" width="100%" class="newcontainerborder">
				<tr>
					<td valign="top" class="frBgIntMembers">
					
					
						<!-- Blast Details -->
						<div id="divMsgDetails">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
								
									<!-- TOP STATUS BAR -->
									<div id="pageheader">
										<table cellpadding="0" cellspacing="0" border="0" width="99%">
											<tr>
												<td align="left"><h1>Blast Details</h1></td>
											</tr>
										</table>
									</div>
									<!-- END TOP STATUS BAR -->	
								
								</td>
							</tr>
							<tr>
								<td width="968" align="center"  valign="top">
									
										<h2 class="alertmessage"><span id="spnAlertMsg"></span></h2>
										
										<table cellpadding="0" cellspacing="0" border="0"  width="100%">
										<tr>
											<td width="968" valign="top" align="center">
											
												<table border="0" cellspacing="0" cellpadding="0" width="99%">
													<tr>
														<td valign="top">
															<div id="inpageNav" style="margin-left:5px;">
																<a runat="server" href="~/default.aspx">Back to My Kaneva</a>
																<br /><br />
																<ajax:ajaxpanel ID="AjaxpanelAdminDelete" runat="server">
															        <asp:linkbutton runat="server" id="adminBlastDelete">Delete Blast</asp:linkbutton>
															    </ajax:ajaxpanel>
															</div>
														</td>
														<!-- START BLAST DETAIL -->
														<td valign="top" align="center">
															
															<table border="0" cellspacing="0" cellpadding="0">
																<tr>
																	<td width="670" align="center">
																		
																		<div class="module fullpage">
																		<span class="ct"><span class="cl"></span></span>
																	
																		<table border="0" cellspacing="0" cellpadding="0" width="98%" align="center">
																			<tr>
																				<td align="center" width="113" valign="top">

																
																					<!-- Avatar Box -->
																						<div class="framesize-small">
																							<div class="frame">
																								<span class="ct"><span class="cl"></span></span>
																									<div class="imgconstrain">
																										<a runat="server" id="aFromAvatar"><img runat="server" id="imgFromAvatar" border="0"/></a>
																									</div>
																								<span class="cb"><span class="cl"></span></span>
																							</div>
																						</div>		
																					<!-- End Avatar Box -->
																				</td>
																				<td>
																				
																					<table width="100%" border="0" cellspacing="0" cellpadding="3">
																						<tr>
																							<td>
																								<table border="0" cellspacing="0" cellpadding="0">
																									<tr>
																										<td width="65"><strong>From:</strong></td>
																										<td><asp:hyperlink runat="server" id="lnkFrom"/></td>
																									</tr>
																									<tr>
																										<td><strong>Date:</strong></td>
																										<td><asp:label runat="server" id="lblDate"/></td>
																									</tr>
																									<tr>
																										<td><strong>Subject:</strong></td>
																										<td><asp:label runat="server" id="lblSubject"/></td>
																									</tr>
																								</table>
																							</td>
																							<td align="left">
					
																								<table border="0" cellspacing="0" cellpadding="3">
																									<tr>
																										<td>
																										</td>
																										<td width="60" align="right"><strong><asp:Label runat="server" id="lblBlastedComm">Blasted:</asp:Label> </strong></td>
																										<td width=""> 
																											<asp:hyperlink runat="server" id="lnkCommunity"/>
																										</td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																						<tr>
																							<td colspan="2"><hr></td>
																						</tr>
																						<tr>
																							<td colspan="2" class="large" valign="top">
																								<div style="float:left;">
																									<asp:Label id="lblBlast" runat="server"/>
																								</div>
																								<div style="float:right;width:300px;border:solid 1px black;padding-bottom:8px;" visible="false" id="divMediaDetails" runat="server">
																									<div style="width:100%;height:20px;background-color:Black;text-align:center;"><a style="font-weight:bold;color:white;text-decoration:none;font-size:12px;" runat="server" id="aAssetDetails">View this Blast from a Friend >></a></div>
																									<div style="width:100%;padding:10px 10px 0 10px;">
																										<div class="framesize-small" style="float:left;margin-right:10px;">
																											<div class="frame">
																												<span class="ct"><span class="cl"></span></span>
																													<div class="imgconstrain">
																														<img runat="server" id="imgAssetThumbnail" border="0"/>
																													</div>
																												<span class="cb"><span class="cl"></span></span>
																											</div>
																										</div><div id="divTitle" runat="server" style="margin-left:10px;font-weight:bold;font-size:14px;"></div>
																									</div><br />	   
																									<div id="stats" style="float:none;">
																										<div style="float:left;font-weight:bold;width:100px;margin-left:10px;">Media stats:</div>
																										<div style="float:right;width:180px;">
																											<div>Views: <span id="spnViews" runat="server"></span></div>
																											<div>Raves: <span id="spnRaves" runat="server"></span></div>
																											<div>Added: <span id="spnAdds" runat="server"></span></div>
																											<div>Comments: <span id="spnComments" runat="server"></span></div>
																											<div id="divRuntime" runat="server" visible="false">Runtime: <span id="spnRuntime" runat="server"></span></div>
																										</div>
																										
																										
																									</div>
																								</div>
																								
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																		</table>
																		
																		<hr>
																		<br>
																		
																		<ajax:ajaxpanel ID="ajaxComments" runat="server">
																		<div id="blastComments" runat="server">
																		<table id="ucComments_tblRespond" align="center" name="tblRespond" cellpadding="0" cellspacing="0" border="0" width="75%">
																			<tr>
																				<td align="left" class="bodyText">
																					<h2><img runat="server" src="~/images/blast/icon_blast.gif" alt="icon_blast.gif" width="32" height="32" border="0"> Blast Reply</h2>
																					<span id="spnRemain">140</span> characters remaining<br/>
																					<asp:TextBox ID="txtResponse" TextMode="multiline" Rows="5" Cols="60" style="margin:0 0 5 0; width: 600px;" onkeyup="CharsRemain(this,'spnRemain');" MaxLength="140" runat="server"/><br/>
																				</td>
																			</tr>
																			<tr>
																				<td align="center">
																					<asp:button id="btnUpdateEdit" onclick="ReplyComment_Click" style="width: 100px; margin: 5px;" runat="Server" class="biginput" text="Blast Reply"/>
																					&nbsp;
																				</td>
																			</tr>
																		</table>
																		
																		<br><br>
																		
																		<table border="0" cellspacing="0" cellpadding="0" width="65%" align="center">
																			
																			<tr>
																				<td>
																					<asp:repeater id="rptComments" runat="server">
																						<itemtemplate>
																							<div class="comment">
																								<div class="commentpoint"></div>
																								<div class="framesize-xsmall commentpic">
																									<div class="frame">
																										<span class="ct"><span class="cl"></span></span>							
																											<div class="imgconstrain">
																												<a href="<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "NameNoSpaces").ToString ())%>">
																													<img src='<%#GetProfileImageURL (DataBinder.Eval(Container.DataItem, "ThumbnailSmallPath").ToString (), "sm", "M")%>' border="0" />
																												</a>
																											</div>																																				
																										<span class="cb"><span class="cl"></span></span>
																									</div>
																								</div>
																								<div class="commenttext"><h3><a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "NameNoSpaces").ToString ())%>><%# DataBinder.Eval(Container.DataItem, "NameNoSpaces")%></a><span class="hlink note"><%# FormatDateTimeSpan (DataBinder.Eval(Container.DataItem, "DateCreated"), DataBinder.Eval(Container.DataItem, "CurrentTime")) %></span></h3><%# DataBinder.Eval(Container.DataItem, "Message")%></div>
																							</div>
																						</itemtemplate>
																					</asp:repeater>
																				
																				
																				</td>
																				
																			</tr>
																		</table>
																		</div>
																		</ajax:ajaxpanel>
																		
																		<span class="cb"><span class="cl"></span></span>
																		</div>
																	
																	</td>
																</tr>
															</table>
															
														</td>
														
														<!-- END BLAST DETAIL -->
													</tr>
												</table>
											
											</td>
											
										</tr>
										
									</table>
										
										
									
								</td>
							</tr>
						</table>
						</div>
						<!-- End Message Details -->
						
						
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
