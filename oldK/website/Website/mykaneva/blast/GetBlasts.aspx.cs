///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
    public class GetBlasts : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.ContentType = "text/plain";
            Response.CacheControl = "no-cache";
            Response.AddHeader("Pragma", "no-cache");
            Response.Expires = -1;

            int pageNumberPassive = 1;

            if (Request["p"] != null)
            {
                pageNumberPassive = Convert.ToInt32(Request["p"]);
            }

            BindPassiveBlasts(pageNumberPassive);
        }

        /// <summary>
        /// BindPassiveBlasts
        /// </summary>
        /// <param name="pageNumber"></param>
        private void BindPassiveBlasts(int pageNumber)
        {
            try
            {
                User Current_User = KanevaWebGlobals.CurrentUser;

                //user imposed filters on passive blast
                string userVisibilityFilter = "";
                if (Current_User != null)
                {
                    if (Current_User.BlastShowBlogs)
                    {
                        userVisibilityFilter += (int)BlastTypes.BLOG + ",";
                    }
                    if (Current_User.BlastShowComments)
                    {
                        userVisibilityFilter += (int)BlastTypes.MEDIA_COMMENT + ",";
                    }
                    if (Current_User.BlastShowForums)
                    {
                        userVisibilityFilter += (int)BlastTypes.FORUM_POST + ",";
                    }
                    if (Current_User.BlastShowJoinCommunity)
                    {
                        userVisibilityFilter += (int)BlastTypes.JOIN_COMMUNITY + ",";
                    }
                    if (Current_User.BlastShowProfiles)
                    {
                        userVisibilityFilter += (int)BlastTypes.PROFILE_UPDATE + ",";
                    }
                    if (Current_User.BlastShowRave3dHangout)
                    {
                        userVisibilityFilter += (int)BlastTypes.RAVE_3D_HANGOUT + ",";
                    }
                    if (Current_User.BlastShowRave3dHome)
                    {
                        userVisibilityFilter += (int)BlastTypes.RAVE_3D_HOME + ",";
                    }
                    if (Current_User.BlastShowSendGift)
                    {
                        userVisibilityFilter += (int)BlastTypes.SEND_GIFT + ",";
                    }
                    //    if (Current_User.BlastShowFameAwards)
                    //    {
                    userVisibilityFilter += (int)BlastTypes.FAME + ",";
                    //    }
                    //**RAVE -- Will need to add check once profile blast setting is added
                    //    if (Current_User.)
                    //    {
            //        userVisibilityFilter += (int)BlastTypes.RAVE + ",";
            //        userVisibilityFilter += (int)BlastTypes.MEGARAVE + ",";
                    //    }
                    if (userVisibilityFilter.Length > 0)
                    {
                        userVisibilityFilter = userVisibilityFilter.Substring(0, (userVisibilityFilter.Length - 1));
                    }
                }

                int MaxBlasts = 20;

                // Get blasts
                BlastFacade blastFacade = new BlastFacade();

                if (KanevaWebGlobals.CurrentUser.Stats.NumberOfFriends > 150)
                {
                    // MaxValue is used to assume they have at least 100 active blasts, this is for users with large friends
                    // groups and it is too expensive to do a count on the total number of blasts.
                    uint MaxValue = 100;
                    PagedList<Blast> lPassiveBlasts = null;
                    lPassiveBlasts = blastFacade.GetPassiveBlastsManyFriends (KanevaWebGlobals.CurrentUser.UserId, MaxBlasts, userVisibilityFilter, pageNumber, MaxValue);

                    //Blasts
                    rptPassiveBlasts.DataSource = lPassiveBlasts;
                    rptPassiveBlasts.DataBind();

                    // Set up pager
                    litJS.Text = "<script type=\"text/javascript\">setPager(" + MaxBlasts.ToString() + "," + MaxValue.ToString () + ");</script>";
                }
                else
                {
                    PagedList<Blast> plPassiveBlasts = null;
                    plPassiveBlasts = blastFacade.GetPassiveBlasts(KanevaWebGlobals.CurrentUser.UserId, MaxBlasts, userVisibilityFilter, pageNumber);

                    //Blasts
                    rptPassiveBlasts.DataSource = plPassiveBlasts;
                    rptPassiveBlasts.DataBind();

                    // Set up pager
                    litJS.Text = "<script type=\"text/javascript\">setPager(" + MaxBlasts.ToString() + "," + plPassiveBlasts.TotalCount.ToString() + ");</script>";
                }
            }
            catch { }
        }

        /// <summary>
        /// Repeater rptBlasts ItemDataBound Event Handler
        /// </summary>
        protected void rptPassiveBlasts_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            BlastFacade blastFacade = new BlastFacade();
            string strOut = "";
            string strOutLastItem = "";
            bool showBlast = true;

            Blast currentBlast = (Blast)e.Item.DataItem;

            // Is current blast was sent by same person as the previous blast
            if (currentBlast.SenderId == _prevBlast.SenderId)
            {
                // Is current blast type the same type as the previous blast
                if (currentBlast.BlastType == _prevBlast.BlastType)
                {
                    // Roll up these blasts
                    // first item in rollup needs opening ul and div tags
                    if (_blastCounter == 0)
                    {
                        _rollupBlasts = "<ul id=\"activitiyRollup" + _rollupCount + "\" style=\"display:none;\" class=\"activity rollup\">";
                    }

                    if (_isFirstBlast) // can use _prevBlast if use record count
                    {
                        _rollupBlasts += "<li class=\"message\" onmouseover='this.style.backgroundColor=\"#C8E3EE\";' onmouseout=\"this.style.backgroundColor='';\">" +
                                            "<span class=\"icon\"></span>" +
                                            _prevBlast.Subj + "<span class=\"time\">(" + FormatDateTimeSpan(_prevBlast.DateCreated, DateTime.Now) + ")</span>" +
                                         "</li>";

                        _isFirstBlast = false;
                    }

                    _rollupBlasts += "<li class=\"message\" onmouseover='this.style.backgroundColor=\"#C8E3EE\";' onmouseout=\"this.style.backgroundColor='';\">" +
                                        "<span class=\"icon\"></span>" +
                                        currentBlast.Subj + "<span class=\"time\">(" + FormatDateTimeSpan(currentBlast.DateCreated, DateTime.Now) + ")</span>" +
                                     "</li>";

                    _blastCounter++;
                    showBlast = false;
                }
            }


            // need senderId check so that first item is not printed since we don't 
            // know if we need to include it in a roll-up or not
            if (showBlast && _prevBlast.SenderId > 0)
            {
                if (_blastCounter > 0)
                {
                    // create the rollup summary line
                    strOut = "<ul class=\"activity rollupTrigger\" onclick=\"Effect.toggle($('activitiyRollup" + _rollupCount + "'), 'appear');\">" +
                                "<li class=\"message rollTrigger\" onmouseover='this.style.backgroundColor=\"#C8E3EE\";' onmouseout=\"this.style.backgroundColor='';\">" +
                                    "<span class=\"icon\">" + blastFacade.GetBlastImg(_prevBlast.BlastType) + "</span>" +
                                    "<a href=\"" + ResolveUrl("~/channel/" + _prevBlast.SenderName + ".people") + "\">" + _prevBlast.SenderName + "</a> " + blastFacade.GetBlastText(_prevBlast.BlastType) + " multiple times. <span class=\"bold\">Click to view all " + blastFacade.GetBlastType(_prevBlast.BlastType) + ".</span>" +
                                "</li>" +
                             "</ul>";

                    //           strOut += _rollupBlasts + "</ul>";

                    _rollupCount++;

                    // check to see if we are at the last item in the list
                    if (((Repeater)Sender).Items.Count == ((PagedList<Blast>)(((Repeater)Sender).DataSource)).TotalCount - 1)
                    {
                        //_rollupBlasts += "<li class=\"message\" onmouseover='this.style.backgroundColor=\"#C8E3EE\";' onmouseout=\"this.style.backgroundColor='';\">" +
                        //                   "<span class=\"icon\"></span>" +
                        //                   _prevBlast.Subj + "<span class=\"time\">(" + FormatDateTimeSpan (_prevBlast.DateCreated, DateTime.Now) + ")</span>" +
                        //                "</li>";

                        strOutLastItem += "<ul class=\"activity\">" +
                                    "<li class=\"message\" onmouseover='this.style.backgroundColor=\"#C8E3EE\";' onmouseout=\"this.style.backgroundColor='';\">" +
                                        "<span class=\"icon\">" + blastFacade.GetBlastImg(currentBlast.BlastType) + "</span>" +
                                        currentBlast.Subj + "<span class=\"time\">(" + FormatDateTimeSpan(currentBlast.DateCreated, DateTime.Now) + ")</span>" +
                                    "</li>" +
                                 "</ul>";
                    }

                    strOut += _rollupBlasts + "</ul>" + strOutLastItem;

                    _isFirstBlast = true;
                }
                else if (_prevBlast.SenderId > 0)
                {
                    strOut = "<ul class=\"activity\">" +
                                "<li class=\"message\" onmouseover='this.style.backgroundColor=\"#C8E3EE\";' onmouseout=\"this.style.backgroundColor='';\">" +
                                    "<span class=\"icon\">" + blastFacade.GetBlastImg(_prevBlast.BlastType) + "</span>" +
                                    _prevBlast.Subj + "<span class=\"time\">(" + FormatDateTimeSpan(_prevBlast.DateCreated, DateTime.Now) + ")</span>" +
                                "</li>" +
                             "</ul>";

                    // check to see if we are at the last item in the list
                    if (((Repeater)Sender).Items.Count == ((PagedList<Blast>)(((Repeater)Sender).DataSource)).TotalCount - 1)
                    {
                        strOut += "<ul class=\"activity\">" +
                                   "<li class=\"message\" onmouseover='this.style.backgroundColor=\"#C8E3EE\";' onmouseout=\"this.style.backgroundColor='';\">" +
                                       "<span class=\"icon\">" + blastFacade.GetBlastImg(currentBlast.BlastType) + "</span>" +
                                       currentBlast.Subj + "<span class=\"time\">(" + FormatDateTimeSpan(currentBlast.DateCreated, DateTime.Now) + ")</span>" +
                                   "</li>" +
                                "</ul>";
                    }
                }

                ((HtmlContainerControl)e.Item.FindControl("ulContainer")).InnerHtml += strOut;

                _blastCounter = 0;
            }
            // checking to see if we are at the last item in the list
            else if (((Repeater)Sender).Items.Count == ((PagedList<Blast>)(((Repeater)Sender).DataSource)).TotalCount - 1)
            {
                if (_blastCounter > 0)
                {
                    // create the rollup summary line
                    strOut = "<ul class=\"activity rollupTrigger\" onclick=\"Effect.toggle($('activitiyRollup" + _rollupCount + "'), 'appear');\">" +
                                "<li class=\"message rollTrigger\" onmouseover='this.style.backgroundColor=\"#C8E3EE\";' onmouseout=\"this.style.backgroundColor='';\">" +
                                    "<span class=\"icon\">" + blastFacade.GetBlastImg(currentBlast.BlastType) + "</span>" +
                                    "<a href=\"" + ResolveUrl("~/channel/" + currentBlast.SenderName + ".people") + "\">" + currentBlast.SenderName + "</a> " + blastFacade.GetBlastText(currentBlast.BlastType) + " multiple times. <span class=\"bold\">Click to view all " + blastFacade.GetBlastType(currentBlast.BlastType) + ".</span>" +
                                "</li>" +
                             "</ul>";

                    strOut += _rollupBlasts + "</ul>";

                    //strOut += "<li class=\"message\" onmouseover='this.style.backgroundColor=\"#C8E3EE\";' onmouseout=\"this.style.backgroundColor='';\">" +
                    //            "<span class=\"icon\"></span>" +
                    //            _prevBlast.Subj + "<span class=\"time\">(" + FormatDateTimeSpan (_prevBlast.DateCreated, DateTime.Now) + ")</span>" +
                    //          "</li>" +
                    //        "</ul>";
                }
                else if (_prevBlast.SenderId == 0)     // **** do we need this case ?????
                {
                    strOut += "<ul class=\"activity\">" +
                               "<li class=\"message\" onmouseover='this.style.backgroundColor=\"#C8E3EE\";' onmouseout=\"this.style.backgroundColor='';\">" +
                                   "<span class=\"icon\">" + blastFacade.GetBlastImg(currentBlast.BlastType) + "</span>" +
                                   currentBlast.Subj + "<span class=\"time\">(" + FormatDateTimeSpan(currentBlast.DateCreated, DateTime.Now) + ")</span>" +
                               "</li>" +
                            "</ul>";
                }

                ((HtmlContainerControl)e.Item.FindControl("ulContainer")).InnerHtml += strOut;
            }
            else
            {
                ((HtmlContainerControl)e.Item.FindControl("ulContainer")).Visible = false; ;
            }

            _prevBlast = (Blast)e.Item.DataItem;
        }

        protected Repeater rptPassiveBlasts;

        private int _blastCounter = 0;
        private int _rollupCount = 0;
        private bool _isFirstBlast = true;
        private Blast _prevBlast = new Blast();
        private string _rollupBlasts = string.Empty;
        protected Literal litJS;

    }
}
