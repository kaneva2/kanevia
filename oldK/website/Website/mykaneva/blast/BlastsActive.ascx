<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BlastsActive.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.mykaneva.blast.BlastsActive" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../../usercontrols/Pager.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="BlastComments" Src="../../usercontrols/doodad/BlastComments.ascx" %>
<style type="text/css">
.shareBox {display:none;position:absolute;width:198px;height:60px;background:url('/images/blast/blastshare_box.png') no-repeat 0 0;z-index:25;}
.shareBox .shared {display:none;width:160px;color:#333;font-size:11px;position:relative;top:17px;left:23px;text-align:center;}
.shareBox .shareLoading {position:relative;left:85px;top:16px;}
</style>
<script type="text/javascript">
Event.observe(window, 'load', ttINIT); 	 
function ttINIT (){	 
    Tooltip.init();
}

// Length of Blast
var textboxtextSave = '';
var maxChars = <asp:literal id="litMaxLen" runat="server"/>;
function CharsRemain(t, span_id)
{								 
	var val = t.value.length;
	
	if (val > maxChars)
		t.value = textboxtextSave;
	else
	{	 
		$(span_id).innerHTML = maxChars-val+' ';
		textboxtextSave = t.value;
	}   
}

function enter(evt)
{	
	var charCode = (evt.which) ? evt.which : window.event.keyCode; 
    if (charCode == 13) {return false;} 
}

var commentCtrlPath = '<asp:literal id="litCommentsFilePath" runat="server" />';

function BlockBlast(ub, cb, bid) {
	var url = 'mykaneva/blast/blastBlockAction.aspx?a=b&u=' + ub + '&c=' + cb;

	new Ajax.Request(url, {
		method: 'get',
		onSuccess: function (transport) {
			if (transport.responseText == '1')
				DeleteBlast(bid);
		}
	})
}
function ShowReblastBox(id, blastId, reblastId, isComplete) {
	var linkObj = $j('#' + reblastId);
	var position = linkObj.position();
	var boxId = "#" + id;
	var boxObj = $j(boxId);		  

	if (boxObj != null) {
		try {
			if (isComplete) {	
				$j(boxId + ' .shareLoading').css('display', 'none');
				$j(boxId + ' div').css('display', 'block');
			}
			else {		
				$j(boxId + ' .shareLoading').css('display', 'block');
				$j(boxId + ' div').css('display', 'none');
			}
			$j('.shareBox').css('display', 'none');
			boxObj.toggle();
			boxObj.css('top', position.top - (boxObj.height() / 2) + (linkObj.height() / 2));
			boxObj.css('left', position.left + linkObj.width());
		}
		catch (err) {
			$j('.shareBox').css('display', 'none');
		}
	}
}
$j(document).mouseup(function (e) {
	$j('.shareBox').hide();
});
</script>


<!-- Blast -->
<asp:updatepanel id="upBlasts" runat="server" childrenastriggers="false" updatemode="conditional">
<Triggers>
    <asp:AsyncPostBackTrigger ControlID="lnkBlast" EventName="ServerClick" />
	<asp:AsyncPostBackTrigger ControlID="rptActiveBlasts" EventName="ItemCommand" />
	<asp:AsyncPostBackTrigger ControlID="drpActiveBlasts" EventName="SelectedIndexChanged" />
</Triggers>
<contenttemplate>

<div id="blastBox" class="BlastBox" runat="server" style="display:none;">
	<div id="blastComment">
		<div id="divCounter"><span id="counter"><span id="spnRemain" runat="server" clientidmode="static"></span> / <span id="spnMax" runat="server">140</span></span></div>
		<asp:TextBox ID="txtBlast" onclick="if(this.value=='Blast what you are doing...')this.value='';" text="Blast what you are doing..." TextMode="multiline" Rows="1" borderwidth="0" Columns="33" onfocus="javascript:$('counter').style.display='block';" onkeyup="CharsRemain(this,'spnRemain')" MaxLength="140" runat="server"/>
	</div>
	<div id="blastIt">
		<a id="lnkBlast" clientidmode="static" runat="server" onserverclick="lnkBlast_Click"><span>BLAST IT!</span></a>			
	</div>
	
	<div class="clearit"></div>
	<div id="blastError" runat="server" clientidmode="Static" visible="false"></div>

	<div id="blastLinks">
		<ul id="Ul1" runat="server">
			<li class="blastLinksIntro" runat="server" id="liIntro">Share:</li>
			<li class="blastLinksVideo" runat="server" id="liVideo"><a runat="server" id="aBlastTypeVideo">Video</a></li> 
			<li class="blastLinksPhoto" runat="server" id="liPhoto"><a runat="server" id="aBlastTypePhoto">Photo</a></li>
			<li class="blastLinksLink" runat="server" id="liLink"><a runat="server" id="aBlastTypeLink">Link</a></li> 
			<li class="blastLinksHome" runat="server" id="liPlace"><a runat="server" id="aBlastTypePlace">Place</a></li> 
			<li class="blastFacebook" runat="server" id="liFacebook"><asp:checkbox id="chkFB" runat="server" /></li>
			<li class="blastLinksFilter" runat="server" id="liBlastFilter">Filter &raquo; 
				<asp:Dropdownlist class="blastFilter" EnableViewState="True" runat="server" OnSelectedIndexChanged="drpActiveBlasts_SelectedIndexChanged" AutoPostBack="True" ID="drpActiveBlasts">
					<asp:ListItem value="xx">All Blasts</asp:ListItem >
					<asp:ListItem value="Fr">My Friends</asp:ListItem >
					<asp:ListItem  value="My">My Blasts</asp:ListItem >
				</asp:Dropdownlist>
			</li>
        </ul>	
    </div><!-- blastLinks-->
	
</div><!-- blastBox-->

<div class="clearit"></div>

<!-- this was added to correct a display issue with IE7 where the background colors would
not get displayed in the correct location.  Adding this seems to correct the issue. -->
<span style="line-height:1px;color:#fff;font-size:1px;">.</span>       

<div class="clearit"></div>

<asp:repeater EnableViewState="True" id="rptAdmBlasts" runat="server">
	<itemtemplate>
		<div class="admincontainer">
		<div class="feed admin <%# DataBinder.Eval(Container.DataItem, "HighlightCss") %>">
			<div class="feedImg">	
					<span class="icon"><%# GetBlastIcon (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "BlastType")), DataBinder.Eval (Container.DataItem, "ThumbnailSmallPath").ToString ())%></span>
				</div>
				<div class="feedText">	
					<p><%# FormatAdminBlast (Convert.ToUInt64 (DataBinder.Eval (Container.DataItem, "EntryId")), Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "BlastType")), DataBinder.Eval (Container.DataItem, "Subj").ToString (), DataBinder.Eval (Container.DataItem, "DiaryEntry").ToString ())%> <span>(<%# FormatDateTimeSpan (DataBinder.Eval (Container.DataItem, "DateCreated"), DataBinder.Eval (Container.DataItem, "CurrentTime"))%>)</span></p>
			</div><!--feedText--><div class="corner"></div>
		</div><!-- feed-->	
		<div class="shadow"></div>
		</div>
	</itemtemplate>
</asp:Repeater>												 

<asp:repeater EnableViewState="True" id="rptActiveBlasts" runat="server" onitemdatabound="rptActiveBlasts_ItemDataBound">
	<itemtemplate>
        <div id='<%# "div" + Convert.ToInt64 (DataBinder.Eval (Container.DataItem, "EntryId")).ToString() %>' class="feed">
			<div class="feed-imgTextContainer">
				<div class="feed-imgContainer">
					<div class="feedImg" id="divThumbnail" runat="server">
						<%#GetThumbnail ((Kaneva.BusinessLayer.BusinessObjects.Blast)Container.DataItem)%>
					</div>
				</div>
				<div class="feed-textContainer">
					<div class="feedText">
						<h4 id="h4ToFrom" runat="server" visible="<%#GetFromVisibility((Kaneva.BusinessLayer.BusinessObjects.Blast) Container.DataItem) %>"><span id="spnToFrom" runat="server"><%#GetToFromText((Kaneva.BusinessLayer.BusinessObjects.Blast)Container.DataItem)%></span>:</h4>
						<p><%# FormatBlast (Container.DataItem) %></p> 
						
						
						<asp:Literal ID="litBlocking" runat="server"></asp:Literal>
						
						<div class="time">(<%# FormatDateTimeSpan (DataBinder.Eval (Container.DataItem, "DateCreated"), DataBinder.Eval (Container.DataItem, "CurrentTime"))%>) <asp:LinkButton style="margin-right:15px;" runat="server" id="btnPlayNow" visible="<%#MeetMe3DVisible((Kaneva.BusinessLayer.BusinessObjects.Blast) Container.DataItem) %>" href="" onclick="<%#MeetMe3DLink((Kaneva.BusinessLayer.BusinessObjects.Blast) Container.DataItem) %>">Play Now</asp:LinkButton><a runat="server" onclick='<%# "DeleteComment({type: \"type.BLAST\",entryId: \"" + Convert.ToInt64 (DataBinder.Eval (Container.DataItem, "EntryId")).ToString() + "\"});return false;"%>' id="aDelete" visible="<%#GetDeleteVisibility((Kaneva.BusinessLayer.BusinessObjects.Blast) Container.DataItem) %>" href="javascript:void(0);">Delete</a></div>
						<div runat="server" id="shareBox" class="shareBox">
							<div id="shared" class="shared">This blast was successfully posted to your page</div>
							<img class="shareLoading" id="imgLoading" runat="server" src="~/images/ajax-loader.gif" />
						</div>
					
					</div><!--feedText-->
					<asp:PlaceHolder ID="phComments" runat="server"></asp:PlaceHolder>
				</div>
			</div>
			
			<asp:HiddenField ID="hidEntryID" runat="server" Value='<%# Convert.ToInt64 (DataBinder.Eval (Container.DataItem, "EntryId")) %>' />
			<asp:HiddenField ID="hidNumberOfComments" runat="server" Value='<%# Convert.ToInt64 (DataBinder.Eval (Container.DataItem, "NumberOfReplies")) %>' />
       </div><!-- feed-->
	</itemtemplate>  
</asp:Repeater>  

<!-- Begin page results Filter -->
<div id="blastPages" class="modulePaging blastPager">
	<Kaneva:Pager id="pgActiveBlasts" MaxPagesToDisplay="5" runat="server" IsAjaxMode="false"
		ShowBracketAroundCurrentPage="False" PrevBtnText="<" NextBtnText=">" PrevNextPageBtnClass="Btn"></Kaneva:Pager>&nbsp;
</div>
<!-- End page results Filter -->

<div class="noData" id="divNoData" runat="server" visible="false"></div>

</contenttemplate>
</asp:updatepanel>		

