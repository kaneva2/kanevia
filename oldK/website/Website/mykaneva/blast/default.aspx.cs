///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using log4net;
using System.Web.Security;
using KlausEnt.KEP.Kaneva.framework.utils;
//using MagicAjax;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.mykaneva.blast
{
	/// <summary>
	/// Summary description for _default.
	/// </summary>
	public class _default : System.Web.UI.Page
	{
		#region Declarations

		private string parentDomain = "";
		private string parentURL = "";
		private string parentTitle = "";
		private string parentSelectedText = "";
		private int selectedTab = -1;
		protected HtmlInputHidden sourceURL;
		protected HtmlInputHidden sourceTitle;
		protected HtmlInputHidden sourceSelectedText;
		protected HtmlInputHidden sourceImages;
		protected HtmlInputHidden sourceMovies;
		protected HtmlInputHidden tabChoice;
        protected HtmlInputHidden sourceLinks;

		// Logger
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);
		//protected MagicAjax.UI.Controls.AjaxPanel ajOffsiteBasts;
		protected HtmlTable tblLoggedIn;
		protected HtmlTable tblLogin;

		protected HtmlInputText txtUserName;
		protected HtmlInputText txtPassword;

		protected HtmlAnchor aUserName;

		protected ImageButton imgLogin;

		protected CheckBox chkRememberLogin;

		protected HtmlAnchor aLostPassword, aRegister, hypBlastDone;

		protected HtmlTable tblBlastText, tblBlastVideo, tblBlastPhoto, tblBlastLink; 
		protected LinkButton lbBlastText, lbBlastVideo;
		protected HtmlGenericControl photoList;

		//tabs
		protected HtmlControl liText, liVideo, liPhoto, liLink;

		// Text
		protected CuteEditor.Editor txtBlast;

		// Video
		protected TextBox txtVideoEmbed;
		protected CuteEditor.Editor txtVideoCaption;

		// Photo
		protected CuteEditor.Editor txtPhotoCaption;
		protected TextBox txtPhotoURL;

		// Link
		protected TextBox txtLinkName, txtLinkURL;
		protected CuteEditor.Editor txtLinkDescription;

		protected HtmlTableRow	loginRow, blastRow, blastSuccess;

		protected System.Web.UI.WebControls.LinkButton lbBlastPhoto;
		protected System.Web.UI.WebControls.LinkButton lbBlastLink;
		protected System.Web.UI.WebControls.Button btnBlast;
		protected System.Web.UI.HtmlControls.HtmlTableRow blastLoading;
		protected System.Web.UI.HtmlControls.HtmlGenericControl pblastLoading;
		protected System.Web.UI.HtmlControls.HtmlImage Img1;
		protected System.Web.UI.HtmlControls.HtmlImage Img2;
		protected System.Web.UI.HtmlControls.HtmlImage Img3;
		protected System.Web.UI.HtmlControls.HtmlImage Img4;
		protected System.Web.UI.HtmlControls.HtmlImage Img5;
		protected System.Web.UI.HtmlControls.HtmlImage Img6;
		protected System.Web.UI.HtmlControls.HtmlImage Img7;
		protected System.Web.UI.HtmlControls.HtmlImage Img8;
		protected System.Web.UI.HtmlControls.HtmlImage Img9;
		protected System.Web.UI.HtmlControls.HtmlInputHidden hidBlastType;
		protected System.Web.UI.HtmlControls.HtmlAnchor A7;


		#endregion

		#region Page Load
		private void Page_Load(object sender, System.EventArgs e)
		{
			if (!IsPostBack)
			{
                SetupOffsiteBlastPage();
			}
		}
		#endregion

		#region Events

		/// <summary>
		/// Send a Blast
		protected void btnBlast_Click (object sender, EventArgs e)
		{
			// Check for any inject scripts
			if (KanevaWebGlobals.ContainsInjectScripts (txtBlast.Text))
			{
				ShowErrorOnStartup ("Your input contains invalid scripting, please remove script code and try again.", false);
				return;
			}

			//attempts to send blast if no injection scripts found
			if(SendBlast())
			{
				blastSuccess.Visible = true;
				loginRow.Visible = false;
				blastRow.Visible = false;
                ClientScript.RegisterStartupScript (GetType(), "test", "<SCRIPT language=\"JavaScript\">Timer(5)</SCRIPT>");
			}

		}

		/// <summary>
		/// ChangeBlastType_Command
		/// </summary>
		protected void ChangeBlastType_Command (object sender, CommandEventArgs e)
		{
			SetBlastTypeDisplay (Convert.ToInt32 (e.CommandName));
		}

		/// <summary>
		/// 
		/// </summary>
		private void SetBlastTypeDisplay (int blastType)
		{
			string type = "";
			switch (blastType)
			{
				case (int) BlastTypes.VIDEO:
				{
					tblBlastText.Visible = false;
					tblBlastVideo.Visible = true;
					tblBlastPhoto.Visible = false;
					tblBlastLink.Visible = false;

					liText.Attributes.Clear	();
					liVideo.Attributes.Add ("class","selected");
					liPhoto.Attributes.Clear ();
					liLink.Attributes.Clear ();
					type = "videos";
                    txtVideoCaption.Text = "<a href=\"" + sourceURL.Value + "\" target=\"_blank\">" + sourceTitle.Value + "</a>";
					break;
				}
				case (int) BlastTypes.PHOTO:
				{
					tblBlastText.Visible = false;
					tblBlastVideo.Visible = false;
					tblBlastPhoto.Visible = true;
					tblBlastLink.Visible = false;

					liText.Attributes.Clear	();
					liVideo.Attributes.Clear ();
					liPhoto.Attributes.Add ("class","selected");
					liLink.Attributes.Clear ();
					type = "images";
                    txtPhotoCaption.Text = "<a href=\"" + sourceURL.Value + "\" target=\"_blank\">" + sourceTitle.Value + "</a>";
                    break;
				}
				case (int) BlastTypes.LINK:
				{
					tblBlastText.Visible = false;
					tblBlastVideo.Visible = false;
					tblBlastPhoto.Visible = false;
					tblBlastLink.Visible = true;

					liText.Attributes.Clear	();
					liVideo.Attributes.Clear ();
					liPhoto.Attributes.Clear ();
					liLink.Attributes.Add ("class","selected");
					type = "links";
                    txtLinkDescription.Text = "<a href=\"" + sourceURL.Value + "\" target=\"_blank\">" + sourceTitle.Value + "</a>";

					break;
				}
				case (int) BlastTypes.BASIC:
				default:
				{
					tblBlastText.Visible = true;
					tblBlastVideo.Visible = false;
					tblBlastPhoto.Visible = false;
					tblBlastLink.Visible = false;

					liText.Attributes.Add ("class","selected");
					liVideo.Attributes.Clear ();
					liPhoto.Attributes.Clear ();
					liLink.Attributes.Clear ();
					type = "text";
                    //added for intial population bug with cute editor
                    if (sourceSelectedText.Value != "")
                    {
                        this.txtBlast.Text = sourceSelectedText.Value;
                    }
                    else
                    {
                        txtBlast.Text = "<a href=\"" + sourceURL.Value + "\" target=\"_blank\">" + sourceTitle.Value + "</a>";
                    }
                    break;
				}
			}
			tabChoice.Value = type;
		}

		/// <summary>
		/// SendBlast
		/// </summary>
		/// <returns></returns>
		private bool SendBlast ()
		{
            BlastFacade blastFacade = new BlastFacade();

			// Server validation
			Page.Validate ();
			if (!Page.IsValid) 
			{
				return false;
			}

			int userId = KanevaWebGlobals.GetUserId ();
		
			string cssHighLight = "";

			// Text Blast
			if (tblBlastText.Visible)
			{
                blastFacade.SendTextBlast (userId, 0, KanevaWebGlobals.CurrentUser.Username, KanevaWebGlobals.CurrentUser.NameNoSpaces, txtBlast.Text, cssHighLight, KanevaWebGlobals.CurrentUser.ThumbnailSmallPath);
			}
				// Video Blast
            //else if (tblBlastVideo.Visible)
            //{
            //    blastFacade.SendVideoBlast(userId, 0, KanevaWebGlobals.CurrentUser.Username, KanevaWebGlobals.CurrentUser.NameNoSpaces, txtVideoCaption.Text, Server.HtmlEncode(txtVideoEmbed.Text), cssHighLight, KanevaWebGlobals.CurrentUser.ThumbnailSmallPath);
            //}

            //    // Photo
            //else if (tblBlastPhoto.Visible)
            //{
            //    if (txtPhotoURL.Text.Length > 0)
            //    {
            //        string strLink = Server.HtmlEncode (txtPhotoURL.Text);
            //        if (!strLink.StartsWith ("http://"))
            //        {
            //            strLink = "http://" + strLink;
            //        }

            //        strLink = "<img src=\"" + strLink + "\"/>";

            //        blastFacade.SendPhotoBlast(userId, 0, KanevaWebGlobals.CurrentUser.Username, KanevaWebGlobals.CurrentUser.NameNoSpaces, strLink, txtPhotoCaption.Text, cssHighLight, KanevaWebGlobals.CurrentUser.ThumbnailSmallPath);
            //    }
            //}
            //    // Link
            //else if (tblBlastLink.Visible)
            //{
            //    string strLink = Server.HtmlEncode (txtLinkURL.Text);
            //    if (!strLink.StartsWith ("http://"))
            //    {
            //        strLink = "http://" + strLink;
            //    }

            //    blastFacade.SendLinkBlast(userId, 0, KanevaWebGlobals.CurrentUser.Username, KanevaWebGlobals.CurrentUser.NameNoSpaces, Server.HtmlEncode(txtLinkName.Text), strLink, txtLinkDescription.Text, cssHighLight, KanevaWebGlobals.CurrentUser.ThumbnailSmallPath);
            //}

			return true;
		}

		/// <summary>
		/// The login click event handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void imgLogin_Click (object sender, System.Web.UI.ImageClickEventArgs e) 
		{
			//validate the user
			ValidateUser ();

            //redirect back to this page
            try
            {
                Response.Redirect(Request.UrlReferrer.AbsoluteUri);
            }
            catch (Exception)
            {
                Response.Redirect(Request.Url.AbsoluteUri);
            }
		}

		#endregion

		#region Helper Functions

        private void SetupOffsiteBlastPage()
        {
            //set login visible
            loginRow.Visible = true;
            blastRow.Visible = false;
            blastSuccess.Visible = false;

            // Is user logged in?
            if (!Request.IsAuthenticated)
            {
                loginRow.Visible = true;
                blastRow.Visible = false;
            }
            else
            {
                //set login visible
                loginRow.Visible = false;
                blastRow.Visible = true;

                // pull the parameters
                if (GetRequestParams())
                {
                    //parse the page and get the pieces
                    ParseHTMLPage offsitePage = new ParseHTMLPage(parentURL, parentDomain);
                    //get the links
                    sourceLinks.Value = offsitePage.Getlinks();
                    //get the movies
                    sourceMovies.Value = offsitePage.GetMovies();
                    //get the images
                    sourceImages.Value = offsitePage.GetPhotos();
                }

                // Set the screen to initial display
                SetBlastTypeDisplay(selectedTab);
            }

        }

		private bool GetRequestParams()
		{
			bool retVal = false;
			try
			{
				if(Request ["v"] != null)
				{
					//get channelId and find the default page
					sourceURL.Value = parentURL = Request["u"]; 
					sourceTitle.Value = parentTitle = Request["t"]; 
					sourceSelectedText.Value = parentSelectedText = Request["s"]; 
					parentDomain = Request["d"];

					try
					{
                        selectedTab = Convert.ToInt32(Request["v"]);
                    }
					catch(FormatException)
					{
						selectedTab = (int) BlastTypes.BASIC;
					}
					retVal = true;
				}

			}
			catch(Exception){}
			
			return retVal;
		}

		/// <summary>
		/// LoginUser
		/// </summary>
		private void ValidateUser ()
		{
			// Try to log in here
            UserFacade userFacade = new UserFacade();
			int roleMembership = 0;
			string email = Server.HtmlEncode (txtUserName.Value);
			string password = Server.HtmlEncode (txtPassword.Value);

			int validLogin = UsersUtility.Authorize (email, password, 0, ref roleMembership, Common.GetVisitorIPAddress(), true);

			string results = "";
			bool successfulLogin = false;

			switch (validLogin)       
			{       
				case 0:  
					results = "Not authenticated.";
					break; 
				case (int) Constants.eLOGIN_RESULTS.NOT_VALIDATED:
					successfulLogin = LoginUser (email, true);
					//					results = "You must first validate your account.";
					//					SetFocus (txtUserName);
					break;    
				case (int) Constants.eLOGIN_RESULTS.SUCCESS:  
					successfulLogin = LoginUser (email, true);
					break;   
				case (int) Constants.eLOGIN_RESULTS.USER_NOT_FOUND:
                    userFacade.InsertUserLoginIssue(0, Common.GetVisitorIPAddress(), "Email address " + email + " not found", "INVALID_EMAIL", "WEB");
					results = "Email address " + email + " was not found.";
					break;                  
				case (int) Constants.eLOGIN_RESULTS.INVALID_PASSWORD:  
				{
                    userFacade.InsertUserLoginIssue(0, Common.GetVisitorIPAddress(), "Invalid password for username " + email, "INVALID_PASSWORD", "WEB");
					m_logger.Warn ("Failed login (invalid password) for email '" + email + "' from IP " + Common.GetVisitorIPAddress());
                    userFacade.UpdateFailedLogins(email);
					results = "Invalid password.";
					break;
				}
				case (int) Constants.eLOGIN_RESULTS.NO_GAME_ACCESS:   
					results = "No access to this game.";
					break;      
				case (int) Constants.eLOGIN_RESULTS.ACCOUNT_DELETED:
					results = "This account has been deleted.";
					break;    
				case (int) Constants.eLOGIN_RESULTS.ACCOUNT_LOCKED:
				{
                    userFacade.InsertUserLoginIssue(0, Common.GetVisitorIPAddress(), "Locked account username '" + email + "' tried to sign in", "BANNED", "WEB");
					m_logger.Warn ("Locked account " + email + " tried to sign in from IP " + Common.GetVisitorIPAddress());
					results = "This account has been locked by the Kaneva administrator";
					break; 
				}
				default:            
					results = "Not authenticated.";           
					break;      
			}

			// Did they fail login?
			if (results.Length > 0)
			{
				// Show an alert
				string strScript = "<script language=JavaScript>";
				strScript += "alert(\"Login Failed: " + results + "\");";
				strScript += "</script>";

                if (!ClientScript.IsStartupScriptRegistered(GetType(), "invalidLogin"))
				{
                    ClientScript.RegisterStartupScript(GetType(), "invalidLogin", strScript);
				}
			}
		}


		/// <summary>
		/// LoginUser
		/// </summary>
		private bool LoginUser (string email, bool bPersistLogin)
		{
			try
			{
                UserFacade userFacade = new UserFacade();
				DataRow drUser = UsersUtility.GetUserFromEmail (email);
				int userId = Convert.ToInt32 (drUser ["user_id"]);
                FormsAuthentication.SetAuthCookie(userId.ToString(), bPersistLogin);

                int loginId = userFacade.UpdateLastLogin(userId, Common.GetVisitorIPAddress(), Server.MachineName);

                // Now take their current IP and and update their cookie.
                Response.Cookies["ipcheck"].Value = Common.GetVisitorIPAddress();
                Response.Cookies["ipcheck"].Expires = DateTime.Now.AddYears(1);  

				// Set the userId in the session for keeping track of current users online
				Session ["userId"] = userId;
                Session["loginId"] = loginId;

				Response.Cookies ["offSiteUser"].Value = email;
				Response.Cookies ["offSiteUser"].Expires = DateTime.Now.AddYears (1);

				// Set the blast section visible
				loginRow.Visible = false;
				blastRow.Visible = true;

				return true;
			}
			catch(Exception)
			{
				return false;
			}
		}

		/// <summary>
		/// Show an error message on startup
		/// </summary>
		/// <param name="errorMessage"></param>
		/// <summary>
		/// Show an error message on startup
		/// </summary>
		/// <param name="errorMessage"></param>
		protected void ShowErrorOnStartup (string errorMessage, bool bShowPleaseMessage)
		{
			string scriptString = "<script language=JavaScript>";
			if (bShowPleaseMessage)
			{
				scriptString += "alert ('Please correct the following errors:\\n\\n" + errorMessage + "');";
			}
			else
			{
				scriptString += "alert ('" + errorMessage + "');";
			}
			scriptString += "</script>";

            if (!ClientScript.IsClientScriptBlockRegistered(GetType(), "ShowError"))
			{
                ClientScript.RegisterStartupScript(GetType(), "ShowError", scriptString);
			}
		}

		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
