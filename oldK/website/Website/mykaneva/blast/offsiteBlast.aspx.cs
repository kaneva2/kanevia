///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Net;

namespace KlausEnt.KEP.Kaneva.mykaneva.blast
{
	/// <summary>
	/// Summary description for offsiteBlast.
	/// </summary>
	public class offsiteBlast : System.Web.UI.Page
	{

		#region Declarations

		private string parentURL = "";
		private string parentDomain = "";
		private string result = "";


		#endregion

		#region Page Load

		private void Page_Load(object sender, System.EventArgs e)
		{
			if (!IsPostBack)
			{
				// pull the parameters
				if(GetRequestParams())
				{
					//recreate source page
                    try
                    {
                        WebRequest objRequest = System.Net.HttpWebRequest.Create(parentURL);
                        WebResponse objResponse = objRequest.GetResponse();
                        using (StreamReader sr = new StreamReader(objResponse.GetResponseStream()))
                        {
                            result = sr.ReadToEnd();
                            // Close and clean up the StreamReader
                            sr.Close();
                        }
                        objResponse.Close();

                        //repair any link that are relative path
                        result = RemoveHeaderScripts(result);
                        result = ReconstructURL(result);
                        string test = FindAllImages(result);
                        string test2 = FindAllMovies_Widgets(result);
                        string test3 = FindAllLinks(result);
                        Response.Write(result);
                    }
                    catch (Exception ex)
                    {
                        string error = ex.Message;
                        Response.Write(result);
                    }
				}
			}
		}

		#endregion

		#region Helper Functions

        private string RemoveHeaderScripts(string webPageCode)
        {
            //replace
            webPageCode = webPageCode.Replace("<HEAD>", "<head>");
            webPageCode = webPageCode.Replace("</HEAD>", "</head>");
            webPageCode = webPageCode.Replace("<TITLE>", "<title>");
            webPageCode = webPageCode.Replace("</TITLE>", "</title>");

            int headSTART = webPageCode.IndexOf("<head>");
            int titleSTART = webPageCode.IndexOf("<title>");
            webPageCode = webPageCode.Replace(webPageCode.Substring(headSTART + 6, titleSTART - (headSTART + 6)), "");
           
            int headEND = webPageCode.IndexOf("</head>");
            int titleEND = webPageCode.IndexOf("</title>");
            webPageCode = webPageCode.Replace(webPageCode.Substring(titleEND + 8, headEND - (titleEND + 8)), "");

            return webPageCode;
        }

        //parses page to 
        private string FindAllImages(string webPageCode)
        {
            string images = "";

            webPageCode = webPageCode.Replace("<IMG ", "<img ");
            webPageCode = webPageCode.Replace("</IMG", "</img");

            int cursorPosition = 0;
            while (cursorPosition < webPageCode.Length)
            {
                int imgSTART = webPageCode.IndexOf("<img ", cursorPosition);
                int imgEND = 0;
                if (imgSTART >= 0)
                {
                    cursorPosition = imgSTART;

                    //this is a check to make sure a closing bracket and new <img isnt encountered
                    //before the </img> ia found
                    int closeImg = webPageCode.IndexOf(">", cursorPosition);
                    int nextImg = webPageCode.IndexOf("<img ", closeImg);
                    int closeTag = webPageCode.IndexOf("</img>", cursorPosition);

                    if ((closeTag != -1) && (closeTag < nextImg))
                    {
                        imgEND = closeTag + 6;
                    }
                    else
                    {
                        imgEND = closeImg + 1;
                    }

                    //assign the image to the container
                    images += webPageCode.Substring(imgSTART, imgEND - imgSTART) + "|";
                    //move cursor
                    cursorPosition = imgEND;
                }
                else
                {
                    cursorPosition = webPageCode.Length;
                }

            }
            return images;
        }

        private string FindAllLinks(string webPageCode)
        {
            string links = "";

            webPageCode = webPageCode.Replace("<A ", "<a ");
            webPageCode = webPageCode.Replace("</A", "</a");
            webPageCode = webPageCode.Replace("HREF", "href");

            int cursorPosition = 0;
            while (cursorPosition < webPageCode.Length)
            {
                int linkSTART = webPageCode.IndexOf("<a ", cursorPosition);
                int linkEND = 0;
                if (linkSTART >= 0)
                {
                    cursorPosition = linkSTART;

                    //this is a check to make sure a closing bracket and new <a isnt encountered
                    //before the </a> ia found
                    int closeLink = webPageCode.IndexOf(">", cursorPosition);
                    int nextLink = webPageCode.IndexOf("<a ", closeLink);
                    int closeTag = webPageCode.IndexOf("</a>", cursorPosition);

                    if ((closeTag != -1) && (closeTag < nextLink))
                    {
                        linkEND = closeTag + 4;
                    }
                    else
                    {
                        linkEND = closeLink + 1;
                    }

                    //assign the image to the container
                    links += webPageCode.Substring(linkSTART, linkEND - linkSTART) + "|";
                    //move cursor
                    cursorPosition = linkEND;
                }
                else
                {
                    cursorPosition = webPageCode.Length;
                }

            }
            return links;
        }

        private string FindAllMovies_Widgets(string webPageCode)
        {
            string movies = "";

            webPageCode = webPageCode.Replace("<EMBED ", "<embed ");
            webPageCode = webPageCode.Replace("<OBJECT ", "<object ");
            webPageCode = webPageCode.Replace("SRC=", "src=");
            webPageCode = webPageCode.Replace("NAME=", "name=");
            webPageCode = webPageCode.Replace("MOVIE", "movie");
            webPageCode = webPageCode.Replace("HTTP", "http");
            webPageCode = webPageCode.Replace(".SWF", ".swf");

            int cursorPosition = 0;
            while (cursorPosition < webPageCode.Length)
            {
                int embedindex = webPageCode.IndexOf("<embed", cursorPosition);
                int objectindex = webPageCode.IndexOf("<object", cursorPosition);
                int srcindex = 0;

                //find either the embed tag and src location or the object tag and movie location
                if (embedindex >= 0)
                {
                    srcindex = webPageCode.IndexOf("src=", embedindex);
                }
                else if (objectindex >= 0)
                {
                    srcindex = webPageCode.IndexOf("name=\"movie\"");
                }

                //if a source was found continue processing
                if (srcindex > 0)
                {
                    int httpindex = webPageCode.IndexOf("http://", srcindex);
                    if (httpindex >= 0)
                    {
                        //extra check to make sure parameters are gathered by finding end of string by "
                        int endingChar = webPageCode.IndexOf("\"", httpindex);
                        
                        //pull the movie URL
                        movies += webPageCode.Substring(httpindex, (endingChar - httpindex)) + "|";
                    }
                }

                //cursor placement
                //if embed index is larger you want to start next position there
                if((embedindex >= objectindex) && (embedindex >= 0))
                {
                    if (srcindex > 0)
                    {
                        cursorPosition = srcindex + 1;
                    }
                    else
                    {
                        cursorPosition = embedindex + 1;
                    }
                }
                else if ((objectindex >= embedindex) && (objectindex >= 0))
                {
                    if (srcindex > 0)
                    {
                        cursorPosition = srcindex + 1;
                    }
                    else
                    {
                        cursorPosition = objectindex + 1;
                    }                   
                }
                else
                {
                    cursorPosition = webPageCode.Length;
                }

            }
            return movies;
        }


        //repair any link that are relative path
        private string ReconstructURL(string webPageCode)
        {
            webPageCode = webPageCode.Replace("src=\"../", "src=\"http://" + parentDomain + "/");
            webPageCode = webPageCode.Replace("src=\"/", "src=\"http://" + parentDomain + "/");

            webPageCode = webPageCode.Replace("href=\"../", "href=\"http://" + parentDomain + "/");
            webPageCode = webPageCode.Replace("href=\"/", "href=\"http://" + parentDomain + "/");
            webPageCode = webPageCode.Replace("localhost", parentDomain);

            int i = 0;
            while (i < webPageCode.Length)
            {
                int position1 = webPageCode.IndexOf("src=\"", i);
                int position2 = webPageCode.IndexOf("href=\"", i);

                if (position1 != -1)
                {
                    string item1 = webPageCode.Substring(position1, 9);
                    if (item1 != "src=\"http")
                    {
                        string original = item1.Substring(0, 6);
                        string newSRC = "src=\"http://" + parentDomain + "/" + original.Substring(5, 1);
                        webPageCode = webPageCode.Replace(original, newSRC);
                        //webPageCode = webPageCode.Replace(item1.Substring(0,6),"src=\"http://"+ parentDomain +"/" + item1.Substring(5,6));
                    }
                }
                if (position2 != -1)
                {
                    string item2 = webPageCode.Substring(position2, 10);
                    if (item2 != "href=\"http")
                    {
                        string original = item2.Substring(0, 7);
                        string newSRC = "href=\"http://" + parentDomain + "/" + original.Substring(6, 1);
                        webPageCode = webPageCode.Replace(original, newSRC);
                        //webPageCode = webPageCode.Replace(item2.Substring(0,7),"href=\"http://"+ parentDomain +"/" + item2.Substring(6,7));
                    }
                }

                if (position1 > position2)
                {
                    if (position2 != -1)
                    {
                        i = position2 + 1;
                    }
                    else
                    {
                        i = position1 + 1;
                    }
                }
                else if (position1 < position2)
                {
                    if (position1 != -1)
                    {
                        i = position1 + 1;
                    }
                    else
                    {
                        i = position2 + 1;
                    }
                }
                else if ((position1 == -1) && (position2 == -1))
                {
                    i = webPageCode.Length;
                }
            }
            return webPageCode;
        }

		private bool GetRequestParams()
		{
			bool retVal = false;
			try
			{
				if(Request ["u"] != null)
				{
					//get channelId and find the default page
					parentURL = Request["u"]; 
					parentDomain = Request["d"]; 
					retVal = true;
				}

			}
			catch(Exception){}
			
			return retVal;
		}

		#endregion


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
