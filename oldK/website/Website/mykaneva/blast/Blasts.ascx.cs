///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Kaneva.mykaneva.blast
{
    public partial class Blasts : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.ClientScript.IsClientScriptIncludeRegistered("jscript/yahoo/2.7.0/yahoo-dom-event.js"))
            {
                Page.ClientScript.RegisterClientScriptInclude("jscript/yahoo/2.7.0/yahoo-dom-event.js", "jscript/yahoo/2.7.0/yahoo-dom-event.js");
            }

            if (!Page.ClientScript.IsClientScriptIncludeRegistered("jscript/yahoo/2.7.0/element-min.js"))
            {
                Page.ClientScript.RegisterClientScriptInclude("jscript/yahoo/2.7.0/element-min.js", "jscript/yahoo/2.7.0/element-min.js");
            }

            if (!Page.ClientScript.IsClientScriptIncludeRegistered("jscript/yahoo/2.7.0/paginator-min.js"))
            {
                Page.ClientScript.RegisterClientScriptInclude("jscript/yahoo/2.7.0/paginator-min.js", "jscript/yahoo/2.7.0/paginator-min.js");
            }
        }
    }
}