<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Blasts.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.mykaneva.blast.Blasts" %>

<script type="text/javascript"><!--

window.onload = BlastPageInit;
var pag;

function BlastPageInit (){

    var ajaxRequest = new Ajax.Request('mykaneva/Blast/GetBlasts.aspx', {
        method:       'get', 
        parameters:   '', 
        asynchronous: true,
        evalJS:true,
        evalScripts:true,
        onComplete:   showPassiveBlasts,
        onFailure: errorTest
    });

    pag = new YAHOO.widget.Paginator({   
         rowsPerPage  : 1,   
         totalRecords : 1,   
         containers   : [ pagerBlasts ],
         nextPageLinkLabel : '&gt;&gt;',
         previousPageLinkLabel : '&lt;&lt;',
         previousPageLinkClass : 'pager',
         pageLinkClass : 'pager',   
         nextPageLinkClass : 'pager',
         pageLinks : 4,
         template : "{PreviousPageLink} {PageLinks} {NextPageLink}"   
     });   
                 
    pag.render();   

    var MyApp = {   
         handlePagination : function (newState) 
         {   
                 
             var ajaxRequest = new Ajax.Request('mykaneva/Blast/GetBlasts.aspx', 
             {
                method:       'get', 
                parameters:   'p=' + newState.page, 
                asynchronous: true,
                evalJS:true,  
                evalScripts:true,
                onComplete:   showPassiveBlasts,
                onFailure: errorTest
             });

             // Update the Paginator's state   
             pag.setState(newState);   
        }   
    };   
   
    pag.subscribe('changeRequest',MyApp.handlePagination);   
}

function showPassiveBlasts(xmlHttpRequest, responseHeader) {
    xmlHttpRequest.responseText.evalScripts();
    $('pBlasts').innerHTML = xmlHttpRequest.responseText;
}

function errorTest(error)   
    {   
        var val = error.responseText;   
        //alert(val);   
    }   
    
    
function setPager (rowsPerPage, totalCount) 
{
    pag.setRowsPerPage(rowsPerPage, true);
    pag.setTotalRecords(totalCount, true);
};

//--> </script> 

<div id="pBlasts">Loading Activities...</div> 
<div class="modulePaging">
    <div id="pagerBlasts"><!-- Passive blast pager --></div>  
</div>