﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/masterpages/GenericNoBorder.Master" CodeBehind="ContestAddEntry.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.mykaneva.ContestAddEntry" %>
<%@ Register TagPrefix="CE" Namespace="CuteEditor" Assembly="CuteEditor" %>
<asp:Content ID="cntHead" runat="server" contentplaceholderid="cphHeadData" >
    <NOSCRIPT><META HTTP-EQUIV="refresh" CONTENT="1" URL="noJavascript.aspx" /></NOSCRIPT>
    <link href="../css/contests.css" type="text/css" rel="stylesheet" />     
	<script type="text/javascript" src="../jscript/colorbox/jquery-1.7.1.min.js"></script>
	<script type="text/javascript"><!--
		var $j = jQuery.noConflict(); 

		$j(document).ready(function () {
			// load 
			$j(window).bind("load", function () {
				// get doc width/height (add a bit extra for non ie) 
//				var iOffSet = ($j.browser.msie) ? 0 : 20;
//				var iW = $j(document).width(); iW=480;
//				var iH = 
				<asp:literal id="litDocHeight" runat="server"></asp:literal>
			});
		});
//-->	</script>
	<style type="text/css">
		body {background-image:none;}
		#wrapper {border:none;background-image:none;background-color:#fff;position:relative;width:auto;margin:0 auto;height:auto !important;padding-bottom:10px;}
	</style>
	
</asp:Content>
<asp:Content id="cntBody" runat="server" contentplaceholderid="cphBody" >
<div id="entercontest" runat="server" clientidmode="static">
	<div class="pagetitle">Enter the Contest</div>  

	<CE:Editor id="txtEntryDesc" style="margin:0;" AllowPasteHtml="True" EditorOnPaste="PasteText" usephysicalformattingtags="true"
	ShowBottomBar="False" EnableContextMenu="False" BackColor="#ededed" runat="server" enableobjectresizing="false" 
	enablestripscripttags="true" MaxTextLength="5000" MaxHTMLLength="5000" width="478px" EditorBodyStyle="font:normal 12px verdana;"  
	Height="200px" ShowHtmlMode="false" ConfigurationPath="~/CuteSoft_Client/CuteEditor/Configuration/AutoConfigure/contest.config" AutoConfigure="None" ></CE:Editor>

	<div id="contestblasts jquery-ui">
		<input type="checkbox" id="cbBlast" runat="server" checked="true" /> Blast this on My Kaneva
	</div>

	<asp:requiredfieldvalidator id="rfvDesc" runat="server" controltovalidate="txtEntryDesc" setfocusonerror="true" display="None" text="" errormessage="You must enter a description of your entry."></asp:requiredfieldvalidator>
	<div id="divMsg" class="msg" runat="server"><asp:validationsummary id="valSum" runat="server" displaymode="SingleParagraph" showsummary="true" /></div>

	<div class="terms" id="divTerms" runat="server" visible="true">By clicking Submit you agree to Kaneva's <a id="A1" runat="server" href="~/overview/termsandconditions.aspx" target="_blank">Terms and Conditions</a></div>
	<div id="buttons">
		<div class="btncontainer twobtn">
			<a class="button" id="aSubmit" href="javascript:void(0);" clientidmode="Static" runat="server" onserverclick="btnSubmit_Click" ><span class="search"> Submit </span></a>
			<a class="button" id="aCancel" href="javascript:void(0);" onclick="parent.$j.colorbox.close(); return false;"><span class="search">Cancel</span></a>
		</div>
	</div>
</div>
<div id="noaccess" class="noaccess enter" runat="server" clientidmode="static" visible="false">
	<div class="heading">We're Sorry.</div>
	<div>
		<span id="spnValidEmail" runat="server" visible="false">You must have a valid email address</span>
		<span id="spnLevelToEnter" runat="server" visible="false">You must have a World Fame Level of <asp:literal id="litLevelToEnter" runat="server"></asp:literal> or higher</span>
		to Enter a Contest.</div><br />
	<div id="buttons">
		<div class="btncontainer">
			<a class="buttonnarrow" runat="server" id="btnOk" href="javascript:void(0);" onclick="parent.$j.colorbox.close(); return false;"><span class="search">OK</span></a>
		</div>
	</div>
</div>
<div id="divMessage" class="noaccess enter" runat="server" clientidmode="static" visible="false">
	<div class="heading">We're Sorry.</div>
	<div class="content"><asp:literal id="litMessage" runat="server"></asp:literal></div><br />
	<div id="buttons">
		<div class="btncontainer">
			<a class="buttonnarrow" id="a2" href="javascript:void(0);" onclick="parent.$j.colorbox.close(); return false;"><span class="search">OK</span></a>
		</div>
	</div>
</div>
<div id="divRules" class="noaccess enter" runat="server" clientidmode="static" visible="false">
	<div class="heading">Warning</div>
	<div class="content">You may be in violation of Contest rules.  Please view the contest rules.</div><br />
	<div id="buttons">
		<div class="btncontainer">
			<a class="buttonnarrow" id="a3" href="javascript:void(0);" onclick="parent.$j.colorbox.close(); return false;"><span class="search">Continue</span></a>
		</div>
	</div>
</div>


</asp:Content>