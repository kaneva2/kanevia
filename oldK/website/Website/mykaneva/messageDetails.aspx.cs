///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using log4net;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for messageDetails.
	/// </summary>
	public class messageDetails : MainTemplatePage
	{
		protected messageDetails () 
		{
			Title = "Message Details";
		}

		private void Page_Load (object sender, System.EventArgs e)
		{

			// They must be logged in
			if (!Request.IsAuthenticated)
			{
				Response.Redirect (GetLoginURL ());
				return;
			}

			if (!IsPostBack)
			{
				int messageId = Convert.ToInt32 (Request ["messageId"]);

				DataRow drMessage = UsersUtility.GetUserMessage (GetUserId (), messageId);

				if (drMessage ["username"].Equals (DBNull.Value))
				{
					// May be trying to access another users message
					m_logger.Warn ("UserId '" + GetUserId () + "' tried to access message '" + messageId + "' and nothing returned.");
					divNoAccess.Visible = true;
					divMsgDetails.Visible = false;
					return;
				}
				

				if(GetUserId ().Equals (Convert.ToInt32 (drMessage ["to_id"])))
				{
					//HeaderNav.MyMessagesNav.ActiveTab = NavMessages.TAB.INBOX;					
					ViewState["isSentMsg"] = false;
				}
				else if(GetUserId ().Equals (Convert.ToInt32 (drMessage ["from_id"])))
				{
					//HeaderNav.MyMessagesNav.ActiveTab = NavMessages.TAB.INBOX;
					ViewState["isSentMsg"] = true;
				}

				// Set Nav
                HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
				HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.MESSAGES;
				HeaderNav.SetNavVisible(HeaderNav.MyKanevaNav,2);
				//HeaderNav.SetNavVisible(HeaderNav.MyMessagesNav);

				
				lnkFrom.Text = drMessage ["username"].ToString ();
				lnkFrom.Attributes.Add ("href", GetPersonalChannelUrl (drMessage ["name_no_spaces"].ToString ()));

				aFromAvatar.HRef = GetPersonalChannelUrl (drMessage ["name_no_spaces"].ToString ());
				imgFromAvatar.Src = GetProfileImageURL (drMessage ["thumbnail_small_path"].ToString ());

				lblDate.Text = FormatDateTime (drMessage ["message_date"]);
				lblSubject.Text = drMessage ["subject"].ToString ();
				lblBody.Text = drMessage ["message"].ToString ();

				//hide spam and block buttons for message sent from logged in users
				spanSpamBlock.Visible = !drMessage ["from_id"].ToString ().Equals(this.GetUserId().ToString());

				// Mark the message as read
				// Only do this if you are the message recipient and it is in the inbox
				if (GetUserId ().Equals (Convert.ToInt32 (drMessage ["to_id"])) && drMessage ["to_viewable"].Equals ("U"))
				{
					UsersUtility.ReadMessage (GetUserId (), messageId);
				}
			}
		}	

		
		#region Helper Methods
		/// <summary>
		/// Block user of a message
		/// </summary>
		/// <param name="drMessage"></param>
		/// <returns></returns>
		private bool BlockMessageUser (DataRow drMessage)
		{
			int userId = GetUserId ();
			int blockUserId = Convert.ToInt32 (drMessage ["from_id"]);
			
			// Can't block yourself
			if (userId.Equals (blockUserId))
			{
				// Check the To address
				blockUserId = Convert.ToInt32 (drMessage ["to_id"]);

				if (userId.Equals (blockUserId))
				{
					return false;
				}
			}

            UserFacade userFacade = new UserFacade();
            if (!userFacade.IsUserBlocked(userId, blockUserId))
			{
				UsersUtility.BlockUser (userId, blockUserId);
			}

			return true;
		}

		#endregion

		#region Event Handlers
		/// <summary>
		/// Reply Event Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnReply_Click (object sender, ImageClickEventArgs e) 
		{
			Response.Redirect (ResolveUrl ("~/mykaneva/newMessage.aspx?messageId=" + Request ["messageId"].ToString ()));
		}

		/// <summary>
		/// Delete Event Handler
		/// </summary>
		protected void btnDelete_Click (object sender, ImageClickEventArgs e) 
		{
			int userId = GetUserId ();
			int messageId = Convert.ToInt32 (Request ["messageId"]);

			UsersUtility.DeleteMessage ( userId, messageId );

			if ( ViewState["isSentMsg"] != null && Convert.ToBoolean(ViewState["isSentMsg"]) )
			{
				Response.Redirect (ResolveUrl ("~/mykaneva/mailboxsent.aspx"));
			}
			else
			{
				Response.Redirect (ResolveUrl ("~/mykaneva/mailbox.aspx"));
			}
		}

		/// <summary>
		/// Back Event Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void lbBack_Click (object sender, EventArgs e) 
		{
			if (Request ["returnURL"] != null)
			{
				Response.Redirect (ResolveUrl (Request ["returnURL"].ToString ()));
			}

			Response.Redirect (ResolveUrl ("~/mykaneva/mailbox.aspx"));
		}

		/// <summary>
		/// Spam Event Handler
		/// </summary>
		protected void lbSpam_Click (object sender, EventArgs e) 
		{
			// Must be logged in
			if (!Request.IsAuthenticated)
			{
				Response.Redirect (GetLoginURL ());
			}

			int messageId = Convert.ToInt32 (Request ["messageId"]);
			DataRow drMessage = UsersUtility.GetUserMessage (GetUserId (), messageId);

			BlockMessageUser (drMessage);

			DataRow drUser = UsersUtility.GetUser ("support");

			if (drUser != null)
			{
                // Insert a private message
                UserFacade userFacade = new UserFacade();
                Message message = new Message(0, GetUserId(), Convert.ToInt32(drUser["user_id"]), "Spam Alert",
                    "<BR><BR><B>--Spam Message--</B><BR><B>From:</B>&nbsp;" + drMessage["username"].ToString() + "<BR><B>Sent:</B>&nbsp;" + FormatDateTime(drMessage["message_date"]) + "<BR><BR>" + drMessage["message"].ToString(),
                    new DateTime(), 0, (int)Constants.eMESSAGE_TYPE.PRIVATE_MESSAGE, 0, "U", "S");

                userFacade.InsertMessage(message);
			}
			
			spnAlertMsg.InnerText = "Message has been marked as spam.";

			MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('spnAlertMsg', 5000);");
		}

		/// <summary>
		/// Block Event Handler
		/// </summary>
		protected void lbBlock_Click (object sender, EventArgs e) 
		{
			// Must be logged in
			if (!Request.IsAuthenticated)
			{
				Response.Redirect (GetLoginURL ());
			}

			int messageId = Convert.ToInt32 (Request ["messageId"]);
			DataRow drMessage = UsersUtility.GetUserMessage (GetUserId (), messageId);

			if (BlockMessageUser (drMessage))
			{
				//Response.Redirect (ResolveUrl ("~/mykaneva/mailbox.aspx"));
				spnAlertMsg.InnerText = "User has been blocked.";
			}
			else
			{
				//ShowErrorOnStartup ("You cannot block yourself.");
				spnAlertMsg.InnerText = "You cannot block yourself..";
			}

			MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('spnAlertMsg', 5000);");
		}

		#endregion
		
		#region Declerations
		protected PlaceHolder	phBreadCrumb;
		protected HyperLink		lnkFrom;
		protected Label			lblDate, lblSubject, lblBody;
		protected LinkButton	lbSpam, lbBlock;
		
		protected HtmlAnchor			aFromAvatar;
		protected HtmlImage				imgFromAvatar;
		protected HtmlContainerControl	divNoAccess, divMsgDetails;
		protected HtmlGenericControl	spanSpamBlock, spnAlertMsg;

		// Logger
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);
		#endregion


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
