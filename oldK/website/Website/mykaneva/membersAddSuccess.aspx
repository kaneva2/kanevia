<%@ Page language="c#" Codebehind="membersAddSuccess.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.membersAddSuccess" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../usercontrols/Pager.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="SearchFilter" Src="../usercontrols/SearchFilter.ascx" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>

<link href="../css/home.css" rel="stylesheet" type="text/css" />		
<link href="../css/kanevaSC.css" rel="stylesheet" type="text/css"/>
<link href="../css/friends.css" rel="stylesheet" type="text/css"/>
<link href="../css/kanevaText.css" type="text/css" rel="stylesheet">

<link href="../css/new.css" rel="stylesheet" type="text/css" />

<script src="../jscript/prototype.js" type="text/javascript" language="javascript"></script>


<table border="0" cellspacing="0" cellpadding="0"  width="990" align="center">
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" id="Img1"/></td>
	</tr>
	<tr>
		<td> 
			<table  border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="frTopLeft"></td>
					<td class="frTop"></td>
					<td class="frTopRight"></td>
				</tr>
				<tr>
					<td class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img2"/></td>
					<td valign="top" class="frBgIntMembers">  
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
								
									<!-- TOP STATUS BAR -->
									<div id="pageheader">
										<table cellpadding="0" cellspacing="0" border="0" width="99%">
											<tr>
												<td align="left">
													<h1>Success</h1>
												</td>
												
												<td align="right" valign="middle">
													<table cellpadding="0" cellspacing="0" border="0" width="690">
														<tr>
															<td>	
																<ajax:ajaxpanel id="ajpTopStatusBar" runat="server">
																<table cellpadding="0" cellspacing="0" border="0" width="100%">
																	<tr>
																		<td class="headertout" width="175">
																			<asp:label runat="server" id="lblSearch" />
																		</td>
																		<td class="searchnav" width="260"> Sort by: 
																			<asp:linkbutton id="lbSortByName" runat="server" sortby="Name"  onclick="lbSortBy_Click">Name</asp:linkbutton> | 
																			<asp:linkbutton id="lbSortByDate" runat="server" sortby="Date" onclick="lbSortBy_Click">Date Added</asp:linkbutton>
																		</td>
																		<td align="left" width="130">
																			<kaneva:searchfilter runat="server" id="searchFilter" loc="top" hidethumbview="true" showfilterlists="false" assetsperpagelist="12,24,40" />
																		</td>
																		<td class="searchnav" align="right" width="210" nowrap>
																			<kaneva:pager runat="server" isajaxmode="True" id="pgTop" maxpagestodisplay="5" shownextprevlabels="true" />
																		</td>
																	</tr>
																</table>
																</ajax:ajaxpanel>
															</td>	
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</div>
									<!-- END TOP STATUS BAR -->	
								
								</td>
							</tr>
							<tr>
								<td width="968" align="left"  valign="top">
									<table cellpadding="0" cellspacing="0" border="0"  width="968">
										<tr>
											<td width="250" valign="top">
												<div class="module toolbar">
													<span class="ct"><span class="cl"></span></span>
													<h2>Tools & Actions <span class="hlink"></span></h2>
													<table cellspacing="0" cellpadding="10" border="0" width="99%">
														<tr>
															<td align="center">
											
																<table cellspacing="0" cellpadding="3" border="0" width="100%">
																	<tr>
																		<td><strong>View My Communities</strong></td>
																		<td valign="bottom"><a runat="server" href="~/bookmarks/channelTags.aspx"><img runat="server" src="~/images/button_go.gif" alt="Go!" width="57" height="23" border="0"></a></td>
																	</tr>
																	<tr><td colspan="2"><hr /></td></tr>
																	<tr>
																		<td><strong>View Member Requests</strong></td>
																		<td valign="bottom"><a runat="server" href="~/mykaneva/mailboxmemberrequests.aspx"><img runat="server" src="~/images/button_go.gif" alt="Go!" width="57" height="23" border="0"></a></td>
																	</tr>
																	<tr><td colspan="2"><hr /></td></tr>
																	<tr>
																		<td colspan="2"><a runat="server" id="lnkMyProfile">&#171; Go back My Profile</a></td>
																	</tr>
																</table>
																	
															</td>
														</tr>
														
													</table>
										 				
													<span class="cb"><span class="cl"></span></span>
												</div>
											</td>
											<td width="20"><img runat="server" src="~/images/spacer.gif" width="20" height="1"/></td>
											<td width="698" valign="top" align="center">
												
												<ajax:ajaxpanel id="ajpMembersList" runat="server">
												<div class="module whitebg">
													<span class="ct"><span class="cl"></span></span>
													<h2 class="alertmessage"><span id="spnAlertMsg" runat="server"></span></h2>
													
													<div align="left" style="margin-left:5px;">
													<asp:datalist visible="true" runat="server" showfooter="False" id="dlMembers"
														cellpadding="5" cellspacing="0" repeatcolumns="6" repeatdirection="Horizontal" 
														itemstyle-horizontalalign="Center" itemstyle-width="104">
														<itemtemplate>
																
															<!-- MEMBER BOX -->
															<div class="framesize-small">
																<div class="frame">
																	<span class="ct"><span class="cl"></span></span>
																		<div class="imgconstrain">
																			<a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "NameNoSpaces").ToString ())%>>
																			<img runat="server" src='<%#GetProfileImageURL (DataBinder.Eval(Container.DataItem, "thumbnail_medium_path").ToString (), "me", DataBinder.Eval(Container.DataItem, "gender").ToString ())%>' border="0" id="Img6"/>
																			</a>
																		</div>
																	<p><span class="content"><a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "NameNoSpaces").ToString ())%>><%# TruncateWithEllipsis (DataBinder.Eval(Container.DataItem, "Username").ToString(), 9) %></a></span></p>
																	<p class="location"><%# TruncateWithEllipsis (DataBinder.Eval(Container.DataItem, "Location").ToString(), 9) %></p>
																	<p class="<%# Convert.ToInt32(DataBinder.Eval(Container.DataItem, "Online")).Equals(1)?"online":"" %>">&nbsp;&nbsp;<asp:label cssclass="online" runat="server" visible='<%# Convert.ToInt32(DataBinder.Eval(Container.DataItem, "online")).Equals(1) %>' text="online" id="lblOnline"/>&nbsp;</p>
																	<span class="cb"><span class="cl"></span></span>
																	<input type="hidden" runat="server" id="hidMemberId" value='<%#DataBinder.Eval(Container.DataItem, "user_id")%>' name="hidMemberId">
																</div>
															</div>		
															<!-- END MEMBER BOX -->
															
														</itemtemplate>
													</asp:datalist>	
													</div>
														
													<span class="cb"><span class="cl"></span></span>
												</div>
												</ajax:ajaxpanel>
												
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1"/></td>
				</tr>
				<tr>
					<td class="frBottomLeft"></td>
					<td class="frBottom"></td>
					<td class="frBottomRight"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>

