///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using System.Web.Security;
using System.Security.Cryptography;

namespace KlausEnt.KEP.Kaneva
{

	/// <summary>
	/// Summary description for contact.
	/// </summary>
	public class contact : MainTemplatePage
	{

		protected contact () 
		{
			Title = "Contact";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			// They must be logged in
			if (!Request.IsAuthenticated)
			{
				Response.Redirect (GetLoginURL ());
				return;
			}

			if (!IsPostBack)
			{
				LoadData ();
			}

			// Set Nav
			HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
		}

		/// <summary>
		/// LoadData
		/// </summary>
		/// <param name="userid"></param>
		private void LoadData ()
		{
            int userId = GetUserId();

			// Is the email address validated?
            lblEmail.Text = KanevaWebGlobals.CurrentUser.Email;

			// Set up the permission dropdowns
			PagedDataTable dtFriendGroups = UsersUtility.GetFriendGroups (userId, "", "name", 1, Int32.MaxValue);

//			drpEmailRights.DataTextField = "name";
//			drpEmailRights.DataValueField = "friend_group_id";
//			drpEmailRights.DataSource = dtFriendGroups;
//			drpEmailRights.DataBind ();
//			AddDropDownPermissions (drpEmailRights);

			drpUsername1Rights.DataTextField = "name";
			drpUsername1Rights.DataValueField = "friend_group_id";
			drpUsername1Rights.DataSource = dtFriendGroups;
			drpUsername1Rights.DataBind ();
			AddDropDownPermissions (drpUsername1Rights);

			drpUsername2Rights.DataTextField = "name";
			drpUsername2Rights.DataValueField = "friend_group_id";
			drpUsername2Rights.DataSource = dtFriendGroups;
			drpUsername2Rights.DataBind ();
			AddDropDownPermissions (drpUsername2Rights);

			drpUsername3Rights.DataTextField = "name";
			drpUsername3Rights.DataValueField = "friend_group_id";
			drpUsername3Rights.DataSource = dtFriendGroups;
			drpUsername3Rights.DataBind ();
			AddDropDownPermissions (drpUsername3Rights);

			drpUsername4Rights.DataTextField = "name";
			drpUsername4Rights.DataValueField = "friend_group_id";
			drpUsername4Rights.DataSource = dtFriendGroups;
			drpUsername4Rights.DataBind ();
			AddDropDownPermissions (drpUsername4Rights);

			AddDropDownIMs (drpIM1);
			AddDropDownIMs (drpIM2);
			AddDropDownIMs (drpIM3);
			AddDropDownIMs (drpIM4);
			
			// Email
			//SetDropDownIndex (drpEmailRights, Server.HtmlDecode (drUser ["show_email"].ToString ()));

			// IM's
			DataRow drUserInstantMessages = UsersUtility.GetUserInstantMessages (userId);

			txtUsername1.Text = Server.HtmlDecode (drUserInstantMessages ["im_username_1"].ToString ());
			SetDropDownIndex (drpIM1, Server.HtmlDecode (drUserInstantMessages ["im_type_1"].ToString ()));
			SetDropDownIndex (drpUsername1Rights, Server.HtmlDecode (drUserInstantMessages ["im_security_1"].ToString ()));

			txtUsername2.Text = Server.HtmlDecode (drUserInstantMessages ["im_username_2"].ToString ());
			SetDropDownIndex (drpIM2, Server.HtmlDecode (drUserInstantMessages ["im_type_2"].ToString ()));
			SetDropDownIndex (drpUsername2Rights, Server.HtmlDecode (drUserInstantMessages ["im_security_2"].ToString ()));

			txtUsername3.Text = Server.HtmlDecode (drUserInstantMessages ["im_username_3"].ToString ());
			SetDropDownIndex (drpIM3, Server.HtmlDecode (drUserInstantMessages ["im_type_3"].ToString ()));
			SetDropDownIndex (drpUsername3Rights, Server.HtmlDecode (drUserInstantMessages ["im_security_3"].ToString ()));

			txtUsername4.Text = Server.HtmlDecode (drUserInstantMessages ["im_username_4"].ToString ());
			SetDropDownIndex (drpIM4, Server.HtmlDecode (drUserInstantMessages ["im_type_4"].ToString ()));
			SetDropDownIndex (drpUsername4Rights, Server.HtmlDecode (drUserInstantMessages ["im_security_4"].ToString ()));
		}

		/// <summary>
		/// AddDropDownPermissions
		/// </summary>
		private void AddDropDownPermissions (DropDownList drpDropDown)
		{
			drpDropDown.Items.Insert (0, new ListItem ("Everyone who is signed in", "K"));
			drpDropDown.Items.Insert (0, new ListItem ("All friends", "A"));
			drpDropDown.Items.Insert (0, new ListItem ("Only Me", "M"));
			drpDropDown.Items.Insert (0, new ListItem ("Everyone including visitors", "Y"));
		}

		/// <summary>
		/// AddDropDownIMs
		/// </summary>
		private void AddDropDownIMs (DropDownList drpDropDown)
		{
			drpDropDown.Items.Insert (0, new ListItem ("Yahoo", "Yahoo"));
			drpDropDown.Items.Insert (0, new ListItem ("MSN", "MSN"));
			drpDropDown.Items.Insert (0, new ListItem ("ICQ", "ICQ"));
			drpDropDown.Items.Insert (0, new ListItem ("AIM", "AIM"));
		}

		/// <summary>
		/// btnUpdate_Click
		/// </summary>
		protected void btnUpdate_Click (object sender, EventArgs e) 
		{
			if (!Page.IsValid) 
			{
				return;
			}

			int userId = GetUserId ();

			//UsersUtility.UpdateUserEmailSecurity (userId, Server.HtmlEncode (drpEmailRights.SelectedValue));
			UsersUtility.UpdateUserInstantMessages (userId, Server.HtmlEncode (txtUsername1.Text),  Server.HtmlEncode (drpIM1.SelectedValue), Server.HtmlEncode (drpUsername1Rights.SelectedValue),
				Server.HtmlEncode (txtUsername2.Text),  Server.HtmlEncode (drpIM2.SelectedValue), Server.HtmlEncode (drpUsername2Rights.SelectedValue),
				Server.HtmlEncode (txtUsername3.Text),  Server.HtmlEncode (drpIM3.SelectedValue), Server.HtmlEncode (drpUsername3Rights.SelectedValue),
				Server.HtmlEncode (txtUsername4.Text),  Server.HtmlEncode (drpIM4.SelectedValue), Server.HtmlEncode (drpUsername4Rights.SelectedValue));
		}

		/// <summary>
		/// btnUpdate_Click
		/// </summary>
		protected void btnCancel_Click (object sender, EventArgs e) 
		{
			LoadData ();
		}

		protected Label lblEmail;
		protected TextBox txtUsername1, txtUsername2, txtUsername3, txtUsername4;
		protected DropDownList drpIM1, drpIM2, drpIM3, drpIM4;
		protected DropDownList drpEmailRights, drpUsername1Rights, drpUsername2Rights, drpUsername3Rights, drpUsername4Rights;

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
