///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using log4net;

using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva.mykaneva
{
	/// <summary>
	/// Summary description for mailboxMemberRequests.
	/// </summary>
	public class mailboxMemberRequests : SortedBasePage
	{
		protected mailboxMemberRequests () 
		{
			Title = "Pending Community Members";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			// They must be logged in
			if (!Request.IsAuthenticated)
			{
				Response.Redirect (GetLoginURL ());
				return;
			}

            // Redirect to the new Media Center page
            Response.Redirect (ResolveUrl ("~/mykaneva/messagecenter.aspx?mc=cr"));

			if (!IsPostBack)
			{
				CurrentSort = "cm2.added_date";
				lbSortByDate.CssClass = "selected";
				
				int userId = GetUserId ();
				BindData (1, userId);
			}
										  
			// Set nav
			HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
			HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.MESSAGES;
			HeaderNav.MyMessagesNav.ActiveTab = NavMessages.TAB.CHANNEL_REQ;
			HeaderNav.SetNavVisible(HeaderNav.MyKanevaNav,2);
			HeaderNav.SetNavVisible(HeaderNav.MyMessagesNav);
			
		}

		/// <summary>
		/// Bind the data
		/// </summary>
		/// <param name="pageNumber"></param>
		private void BindData (int pageNumber, int userId)
		{
			BindData (pageNumber, userId, "");	
		}
		/// <summary>
		/// Bind the data
		/// </summary>
		/// <param name="pageNumber"></param>
		private void BindData (int pageNumber, int userId, string filter)
		{
			searchFilter.CurrentPage = "channelrequests";

			// Set the sortable columns
			//SetHeaderSortText (rptRequests);
			string orderby = CurrentSort + " " + CurrentSortOrder;
			int pgSize = searchFilter.NumberOfPages;

			PagedDataTable pds = CommunityUtility.GetPendingMembers(userId, "",orderby,pageNumber, pgSize);

			rptRequests.DataSource = pds;
			rptRequests.DataBind ();

			pgTop.NumberOfPages = Math.Ceiling ((double) pds.TotalCount / pgSize).ToString ();
			pgTop.DrawControl ();

			// The results
			lblSearch.Text = GetResultsText (pds.TotalCount, pageNumber, pgSize, pds.Rows.Count);

			tblRequests.Visible = true;
		}

		#region Helper Methods
		protected string ShowAge(int age)
		{
			return (age < 18 ? "< 18" : age.ToString()); 
		}
		#endregion
		
		#region Event Handlers
		protected void btnApprove_Click (Object sender, ImageClickEventArgs e)
		{
			int ret = 0;
			int count = 0;
			bool isItemChecked = false;
			string listUserIds = string.Empty;

			spnAlertMsg.InnerText = string.Empty;

			foreach (RepeaterItem dliMessage in rptRequests.Items)
			{
				CheckBox chkEdit = (CheckBox) dliMessage.FindControl ("chkEdit");

				if (chkEdit.Checked)
				{
					isItemChecked = true;
					
					HtmlInputHidden hidUserId = (HtmlInputHidden) dliMessage.FindControl ("hidUserId");
					HtmlInputHidden hidChannelId = (HtmlInputHidden) dliMessage.FindControl ("hidChannelId");
				
					int memberId = Convert.ToInt32(hidUserId.Value);
					int channelId = Convert.ToInt32(hidChannelId.Value);

                    ret = GetCommunityFacade.UpdateCommunityMember(channelId, memberId, (UInt32)CommunityMember.CommunityMemberStatus.ACTIVE);
				
					if (ret == 1)
					{
						if (listUserIds.Length > 0)
						{
							listUserIds += "," + memberId.ToString();
						}
						else
						{
							listUserIds += memberId.ToString();
						}

						count++;
					}
				}
			}
			
			if (isItemChecked)
			{
				if (count > 0)
				{				   
					ShowSuccess(count, listUserIds);

                    spnSuccessAlertMsg.InnerText = count + " World request" + (count == 1 ? " was" : "s were") + " approved.";
					return;
				}
			
				if (ret != 1)
				{
                    spnAlertMsg.InnerText = "An error was encountered while trying to approve World Request.";
				}
				else
				{
                    spnAlertMsg.InnerText = "No World requests were approved.";
				}
			}
			else
			{
                spnAlertMsg.InnerText = "No World requests were selected.";
			}
			
			MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('spnAlertMsg', 5000);");
		}

		protected void btnDecline_Click (Object sender, ImageClickEventArgs e)
		{
			bool isItemChecked = false;
			spnAlertMsg.InnerText = "";

			foreach (RepeaterItem dliMessage in rptRequests.Items)
			{
				CheckBox chkEdit = (CheckBox) dliMessage.FindControl ("chkEdit");

				if (chkEdit.Checked)
				{
					isItemChecked = true;
					break;
				}
			}
			
			if (isItemChecked)
			{
				// Hide Request List
				tblRequests.Visible = false;

				// Show Are You Sure message
				divConfirm.Visible = true;
			}
			else
			{
                spnAlertMsg.InnerText = "No World requests were selected.";	

				MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('spnAlertMsg', 5000);");
			}
		}
		protected void btnYes_Click (Object sender, ImageClickEventArgs e)
		{
			int ret = 0;
			int count = 0;
			spnAlertMsg.InnerText = string.Empty;

			foreach (RepeaterItem dliMessage in rptRequests.Items)
			{
				CheckBox chkEdit = (CheckBox) dliMessage.FindControl ("chkEdit");

				if (chkEdit.Checked)
				{
					HtmlInputHidden hidUserId = (HtmlInputHidden) dliMessage.FindControl ("hidUserId");
					HtmlInputHidden hidChannelId = (HtmlInputHidden) dliMessage.FindControl ("hidChannelId");
				
					int memberId = Convert.ToInt32(hidUserId.Value);
					int channelId = Convert.ToInt32(hidChannelId.Value);

                    ret = GetCommunityFacade.UpdateCommunityMember(channelId, memberId, (UInt32)CommunityMember.CommunityMemberStatus.REJECTED);
				
					if (ret == 1)
						count++;
				}
			}
							
			divConfirm.Visible = false;
			BindData (1, GetUserId());
			
			spnAlertMsg.InnerText = count.ToString() + " channel request" + (count != 1 ? "s were" : " was") + " declined.";
			MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('spnAlertMsg', 5000);");
		}
		
		protected void btnNo_Click (Object sender, ImageClickEventArgs e)
		{
			spnAlertMsg.InnerText = string.Empty;
			divConfirm.Visible = false;
			BindData (1, GetUserId());
		}
		/// <summary>
		/// Page Change
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pgTop_PageChange (object sender, PageChangeEventArgs e)
		{
			BindData (e.PageNumber, GetUserId ());
		}

		/// <summary>
		/// Execute when the user selects an item from the Store Filter
		/// Items to display drop down list
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void FilterChanged(object sender, FilterChangedEventArgs e)
		{
			searchFilter.SetPagesDropValue(searchFilter.NumberOfPages.ToString());
			
			BindData (1, GetUserId(), e.Filter);
		}
		/// <summary>
		/// Execute when the user clicks the the view type link button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void lbSortBy_Click (object sender, EventArgs e) 
		{
			lbSortByName.CssClass = lbSortByDate.CssClass = "";

			if (((LinkButton)sender).Attributes["sortby"].ToString().ToLower() == "name")
			{
				CurrentSort = "u.username";
				CurrentSortOrder = "ASC";
				lbSortByName.CssClass = "selected";
			}
			else
			{
				CurrentSort = "cm2.added_date";
				CurrentSortOrder = "DESC";
				lbSortByDate.CssClass = "selected";							  
			}

			BindData (pgTop.CurrentPageNumber, GetUserId());
		}
		
		private void rptRequests_ItemCommand(object source, RepeaterCommandEventArgs e)
		{
			int ret = 0;
			string command = e.CommandName;
			spnAlertMsg.InnerText = "";

			if (command.Equals("cmdApprove"))
			{
				HtmlInputHidden hidUserId = (HtmlInputHidden) e.Item.FindControl ("hidUserId");
				HtmlInputHidden hidChannelId = (HtmlInputHidden) e.Item.FindControl ("hidChannelId");
				
				int memberId = Convert.ToInt32(hidUserId.Value);
				int channelId = Convert.ToInt32(hidChannelId.Value);
				
				spnAlertMsg.InnerText = string.Empty;

				if (CommunityUtility.IsCommunityModerator(channelId, GetUserId()))
				{
                    ret = GetCommunityFacade.UpdateCommunityMember(channelId, memberId, (UInt32)CommunityMember.CommunityMemberStatus.ACTIVE);
					
					if (ret == 1)
					{			  
						ShowSuccess(1, memberId.ToString());   
					
						spnSuccessAlertMsg.InnerText = "1 member request was approved.";
						return;					
					}
					else
					{
						// Error
						spnAlertMsg.InnerText = "An error was encountered while trying to approve a World request.";
					}
				}
				else
				{
					// Error
					spnAlertMsg.InnerText = "You are not authorized to approve/decline World requests for this World.";
				}

				MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('spnAlertMsg', 5000);");
			}
			else if (command.Equals("cmdDecline"))
			{
				CheckBox chkEdit = (CheckBox) e.Item.FindControl ("chkEdit");
				chkEdit.Checked = true;

				// Hide Request List
				tblRequests.Visible = false;

				// Show Are You Sure message
				divConfirm.Visible = true;
			}
		}

		private void ShowSuccess(int count, string listUserIds)
		{
			PagedDataTable pds = UsersUtility.GetUsers(0, true, "u.user_id IN (" + listUserIds + ")", "username", 1, 50);
			
			dlMembers.DataSource = pds;
			dlMembers.DataBind();

			tblRequests.Visible = false;
			divConfirm.Visible = false;
			tblSuccess.Visible = true;
		}
		#endregion

		#region Properties
		/// <summary>
		/// DEFAULT_SORT
		/// </summary>
		/// <returns></returns>
		protected override string DEFAULT_SORT
		{
			get
			{
				return "username";
			}
		}

		/// <summary>
		/// DEFAULT_SORT ORDER
		/// </summary>
		/// <returns></returns>
		protected override string DEFAULT_SORT_ORDER
		{
			get
			{
				return "DESC";
			}
		}

		#endregion

		#region Declerations
		protected Kaneva.Pager pgTop;
		protected Kaneva.SearchFilter searchFilter;

		protected Label			lblSearch;
		protected Repeater		rptRequests;
		protected DataList		dlMembers;
		protected LinkButton	lbSortByName, lbSortByDate;
		protected ImageButton	btnAccept, btnDecline, btnYes;

		protected HtmlContainerControl	spnAlertMsg, spnSuccessAlertMsg, divConfirm;
		protected HtmlTable				tblRequests, tblSuccess;

		private const int MESSAGES_PER_PAGE = 25;

		const string ACTION_APPROVE = "approve";
		const string ACTION_DENY = "deny";
		
		// Logger
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);
		#endregion

		#region Web Form Designer generated code

		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
			pgTop.PageChanged +=new PageChangeEventHandler (pgTop_PageChange);
			searchFilter.FilterChanged +=new FilterChangedEventHandler (FilterChanged);		
			
			this.rptRequests.ItemCommand +=new RepeaterCommandEventHandler (rptRequests_ItemCommand);
		}
		#endregion
	}
}