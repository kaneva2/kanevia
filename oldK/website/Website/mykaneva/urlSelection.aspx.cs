///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for urlSelection.
	/// </summary>
	public class urlSelection : MainTemplatePage
	{
		protected urlSelection () 
		{
			Title = "Reserve a URL for this World";
		}

		private void Page_Load (object sender, System.EventArgs e)
		{
			if(Request.IsAuthenticated)
			{
				if(GetRequestParams())
				{
					//check if user is allowed to edit this channel
					if(!IsUserAllowedToEdit())
					{
						RedirectToHomePage ();
					}
				}
				else
				{
					//invalid request params
					//if the user logged in, send him to his home page
					//if not, redirect to login page
					RedirectToHomePage ();
				}
			}
			else
			{
				Response.Redirect(this.GetLoginURL());
			}

			// Show current URL
			DataRow drChannel = CommunityUtility.GetCommunity (_channelId);

			if (!drChannel ["url"].Equals (DBNull.Value))
			{
				btnUpdate.Visible = false;
				txtURL.Text = drChannel ["url"].ToString ();
				txtURL.Enabled = false;
			}

			bChannelName.InnerText = drChannel["name"].ToString();

			btnUpdate.Attributes.Add ("onclick", "javascript:if (!confirm('The URL for this World will be http://" + KanevaGlobals.SiteName + "/' + document.frmMain." + txtURL.ClientID + ".value + ' and is permanent and cannot be changed later. Are you sure you want to proceed?')){return false;};");

			// Set up regular expression validators
			revURL.ValidationExpression = Constants.VALIDATION_REGEX_USERNAME;
			lblHost.Text = KanevaGlobals.SiteName;
		}

		private bool GetRequestParams()
		{
			bool retVal = true;
			try
			{
				if(Request ["communityId"] != null && Request ["communityId"] != "")
				{
					_channelId = Convert.ToInt32 (Request ["communityId"]);
				}

				if(_channelId <= 0)
				{
					_channelId = this.GetPersonalChannelId();
				}
			}
			catch(Exception)
			{
				//invalid numbers
				retVal = false;
			}
			return retVal;
		}

		/// <summary>
		/// returns true if current user is allowed to edit this channel
		/// </summary>
		/// <returns></returns>
		private bool IsUserAllowedToEdit ()
		{
			return IsAdministrator() || CommunityUtility.IsCommunityModerator (_channelId, GetUserId ());
		}

		/// <summary>
		/// Cancel Event Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnCancel_Click (object sender, EventArgs e) 
		{
			NavigateBackToBreadCrumb (1);
		}

		/// <summary>
		/// Update Event Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnUpdate_Click (object sender, EventArgs e) 
		{
			// Server validation
			if (!Page.IsValid) 
			{
				return;
			}
		
			if (!Request.IsAuthenticated)
			{
				Response.Redirect (GetLoginURL ());
			}

			// Save it here if they have rights
			if (IsUserAllowedToEdit())
			{
                int result = GetCommunityFacade.UpdateCommunityURL(_channelId, Server.HtmlEncode(txtURL.Text));

				// Was it successful?
				if (result == 1)
				{
                    ShowErrorOnStartup("World URL http://" + KanevaGlobals.SiteName + "/" + txtURL.Text + " already exists. Please choose another URL.");
					return;
				}
			}

			NavigateBackToBreadCrumb (1);
		}

		private int _channelId;
		protected TextBox txtURL;
		protected RegularExpressionValidator revURL;
		protected Label lblHost;
		protected Button btnUpdate;
		protected System.Web.UI.HtmlControls.HtmlGenericControl bChannelName;

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
