///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
    public partial class promo : BasePage
    {
        protected void Page_Load (object sender, EventArgs e)
        {
            // They must be logged in
            if (!Request.IsAuthenticated)
            {
                Response.Redirect (GetLoginURL ());
            }

            if (Request["url"] != null && Request["url"].Length > 0)
            {
                lnkIgnore.HRef = Request["url"];
            }
            else
            {
                lnkIgnore.HRef = "default.aspx";
            }
            
            // Get the VIP promo Id stored in Web Config
            litPassId.Text = KanevaGlobals.VIPPassPromotionID.ToString ();

            //load the images for the page based on provided promoID
            DisplayPassDetails ();

            // Set Nav and style sheets
            ((IndexPageTemplate) Master).CustomCSS = this.styleSheets;
            // Hide the header and footer
            ((IndexPageTemplate) Master).HeaderNav.Visible = false;
            ((IndexPageTemplate) Master).FooterNav.Visible = false;
        }

        #region Helper Functions

        private void DisplayPassDetails ()
        {
            try
            {
                /*
                lnkHome.HRef = ResolveUrl ("~/default.aspx");
                lnkSpecials.HRef = ResolveUrl ("~/mykaneva/buySpecials.aspx");
                lnkShop.HRef = "http://" + KanevaGlobals.ShoppingSiteName;
                lnkCredits.HRef = ResolveUrl ("~/mykaneva/buyCredits.aspx");
                lnkVIP.HRef = ResolveUrl ("~/default.aspx");
                lnkAP.HRef = ResolveUrl ("~/default.aspx");
                -- Client code if needed again --
                <a id="lnkHome" runat="server">Home</a> <span>|</span> 
	            <a id="lnkSpecials" runat="server">Specials</a> <span>|</span> 
	            <a id="lnkShop" runat="server">Shop</a> <span>|</span> 
	            <a id="lnkCredits" runat="server">Buy Credits</a> <span>|</span> 
	            <a id="lnkVIP" runat="server">VIP</a> <span>|</span> 
	            <a id="lnkAP" runat="server">Access Pass</a> 
                */

                if (Request.QueryString["url"] != null && Request.QueryString["url"].ToString ().Length > 0)
                {
                    lnkIgnore.HRef = Request.QueryString["url"].ToString ();
                }
                else
                {
                    lnkIgnore.HRef = ResolveUrl ("~/default.aspx");
                }
            }
            catch (Exception)
            {
            }
        }

        #endregion

        #region Declarations

        private string styleSheets = "\n<link href=\"../css/purchase_flow/passDetailsVIP.css\" rel=\"stylesheet\" type=\"text/css\" />";
        protected Literal litPassId;

        #endregion
    }
}