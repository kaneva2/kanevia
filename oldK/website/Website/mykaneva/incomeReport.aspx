<%@ Page language="c#" Codebehind="incomeReport.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.incomeReport" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../usercontrols/Pager.ascx" %>

<div class="container">

<center>
<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr><td valign="middle" class="mykan2" align="left"></td><td align="right" class="mykan3" valign="middle"><br><asp:Label runat="server" id="lblSearch" CssClass="assetCommunity"/><br><Kaneva:Pager runat="server" id="pgTop"/><br></td></tr>
</table>
</center>
<center>
<table cellspacing="0" cellpadding="0" border="0" width="100%">
	<tr>
		<td colspan="4" width="840">	
						<span class="orangeHeader2">Royalties</span>
		</td>
	</tr>
</table>
<table cellspacing="0" cellpadding="0" border="0" class="LeftRightBorders" style="width:100%;border-collapse:collapse; border-bottom: 1px solid #cccccc;">
	<tr class="lineItemColHead">
		<td>total royalty</td>
		<td>total gross paid</td>
		<td>total net paid</td>
		<td>total tax withheld</td>
		<td>last paid date</td>
	</tr>
	<tr class="lineItemEven">
		<td align="Center" valign="Middle" style="width:20%;">
			<asp:Label runat="server" id="lblTotalRoyalty" CssClass="money"/>
		</td>
		<td align="Center" valign="Middle" style="width:20%;">
			<asp:Label runat="server" id="lblTotalPaidGross" CssClass="money"/>
		</td>
		<td align="Center" valign="Middle" style="width:20%;">
			<asp:Label runat="server" id="lblTotalPaid" CssClass="money"/>
		</td>
		<td align="Center" valign="Middle" style="width:20%;">
			<asp:Label runat="server" id="lblTotalTax" CssClass="money"/>
		</td>
		<td align="Center" valign="Middle" style="width:20%;">
			<asp:Label runat="server" id="lblTotalPaidDate" CssClass="adminLinks"/>
		</td>
	</tr>
</table><span style="line-height: 10px;"><br><br></span>
<table cellspacing="0" cellpadding="0" border="0" width="100%">
	<tr>
		<td colspan="4" width="840">						
						<span class="orangeHeader2">Royalty Payments Received</span>
		</td>
	</tr>
</table>
<asp:DataGrid runat="server" EnableViewState="False" ShowFooter="False" Width="100%" id="dgrdPayments" cellpadding="0" cellspacing="0" border="0" AutoGenerateColumns="False" AllowSorting="True" CssClass="LeftRightBorders" style="border-bottom: 1px solid #cccccc;">  
	<HeaderStyle CssClass="lineItemColHead"/>
	<ItemStyle CssClass="lineItemEven"/>
	<AlternatingItemStyle CssClass="lineItemOdd"/>
	<Columns>
		<asp:TemplateColumn HeaderText="earned through" SortExpression="period_end_date" ItemStyle-Width="20%" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="middle">		
			<ItemTemplate>
				<span class="adminLinks"><%# FormatDate (DataBinder.Eval(Container.DataItem, "period_end_date")) %></span>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="paid date" SortExpression="payment_date" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="middle" ItemStyle-Width="20%">
			<ItemTemplate>
				<span class="adminLinks"><%# FormatDate (DataBinder.Eval(Container.DataItem, "payment_date")) %></span>
			</ItemTemplate>			
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="payment amount" SortExpression="amount_paid" ItemStyle-Width="20%" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="middle">
			<ItemTemplate>
				<span class="money"><%# FormatCurrency (DataBinder.Eval(Container.DataItem, "amount_paid")) %></span>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="tax withheld" SortExpression="tax_withheld" ItemStyle-Width="20%" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="middle">		
			<ItemTemplate>
				<span class="money"><%# FormatCurrency (DataBinder.Eval(Container.DataItem, "tax_withheld")) %></span>
			</ItemTemplate>
		</asp:TemplateColumn>
	</Columns>
</asp:datagrid>

</center>

</div>
