///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using log4net;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for mailbox.
	/// </summary>
	public class mailbox : SortedBasePage
	{
		protected mailbox () 
		{
			Title = "Messages";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			// They must be logged in
			if (!Request.IsAuthenticated)
			{
				Response.Redirect (GetLoginURL ());
				return;
			}

            // Redirect all requests to new new message center page
            Response.Redirect (ResolveUrl ("~/mykaneva/messagecenter.aspx?mc=msg"));





			if (!IsPostBack)
			{
				searchFilter.CurrentPage = "messageinbox";

				CurrentSort = "message_date";
				lbSortByDate.CssClass = "selected";

				int userId = GetUserId ();
				BindData (1, userId);

				lnkViewSent.NavigateUrl = ResolveUrl("~/mykaneva/mailboxSent.aspx");
			}

			// Set Nav
			HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
			HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.MESSAGES;
			//HeaderNav.MyMessagesNav.ActiveTab = NavMessages.TAB.INBOX;
			HeaderNav.SetNavVisible(HeaderNav.MyKanevaNav,2);
			//HeaderNav.SetNavVisible(HeaderNav.MyMessagesNav);
		}			

		/// <summary>
		/// Bind the data
		/// </summary>
		/// <param name="pageNumber"></param>
		private void BindData (int pageNumber, int userId)
		{
			BindData (pageNumber, userId, "");
		}
		/// <summary>
		/// Bind the data
		/// </summary>
		/// <param name="pageNumber"></param>
		private void BindData (int pageNumber, int userId, string filter)
		{
			searchFilter.CurrentPage = "inbox";
			
			// Set the sortable columns
			//SetHeaderSortText (rptMailBox);
			string orderby = CurrentSort + " " + CurrentSortOrder;
			int pgSize = searchFilter.NumberOfPages;

			if (filter.Length > 0)
			{
				filter += " AND ";
			}
			filter += " m.type IN (" + (int) Constants.eMESSAGE_TYPE.ALERT + ", " + (int) Constants.eMESSAGE_TYPE.PRIVATE_MESSAGE + ")";

			PagedDataTable pds = UsersUtility.GetUserMessages (userId, filter, orderby, pageNumber, pgSize);

			rptMailBox.DataSource = pds;
			rptMailBox.DataBind ();

			//added to provide use a textual clue that there are no messages to the user
			if(pds.Rows.Count < 1)
			{
				this.noMessageFound.Visible = true;
				this.MessageFound.Visible = false;
			}
			else
			{
				this.noMessageFound.Visible = false;
				this.MessageFound.Visible = true;
			}

			pgTop.NumberOfPages = Math.Ceiling ((double) pds.TotalCount / pgSize).ToString ();
			pgTop.DrawControl ();

			// The results
			lblSearch.Text = GetResultsText (pds.TotalCount, pageNumber, pgSize, pds.Rows.Count);
		}

		
		#region Helper Methods
		protected string GetMailStatusText (string toViewable, int replied)
		{
			string statusText = "Unopened";

            if (toViewable.Equals ("R"))
			{
				statusText = "Opened";

				if (replied > 0)
				{
					statusText = "Replied";
				}
			}
			
			return statusText;
		}
		#endregion

		#region Event Handlers
		/// <summary>
		/// Page Change
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pgTop_PageChange (object sender, PageChangeEventArgs e)
		{
			BindData (e.PageNumber, GetUserId ());
		}

		/// <summary>
		/// Execute when the user clicks the Delete button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnDelete_Click (object sender, ImageClickEventArgs e)
		{
			HtmlInputHidden hidMessageId;
			CheckBox chkEdit;
			int ret = 0;
			int count = 0;
			int userId = GetUserId ();
			bool isItemChecked = false;

			// Remove the messages
			foreach (RepeaterItem dliMessage in rptMailBox.Items)
			{
				chkEdit = (CheckBox) dliMessage.FindControl ("chkEdit");

				if (chkEdit.Checked)
				{
					isItemChecked = true;

					hidMessageId = (HtmlInputHidden) dliMessage.FindControl ("hidMessageId");
				
					if (!UsersUtility.IsMessageRecipient (Convert.ToInt32 (hidMessageId.Value), GetUserId ()))
					{
						m_logger.Warn ("User " + GetUserId () + " from IP " + Common.GetVisitorIPAddress() + " tried to delete message " + hidMessageId.Value + " but is not the message owner");
						return;
					}

					ret = UsersUtility.DeleteMessage  (userId, Convert.ToInt32 (hidMessageId.Value));
					
					if (ret == 1)
						count++;
				}
			}

			if (isItemChecked)
			{
				if (count > 0)
				{
					BindData (pgTop.CurrentPageNumber, userId);
				}
				
				if (ret != 1)
				{
					spnAlertMsg.InnerText = "An error was encountered while trying to delete messages.";
				}
				else
				{
					spnAlertMsg.InnerText = "Successfully deleted " + count + " message" + (count != 1 ? "s" : "") + ".";
				}
			}
			else
			{
				spnAlertMsg.InnerText = "No messages were selected.";
			}

			MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('spnAlertMsg', 5000);");
		}
		/// <summary>
		/// Execute when the user clicks the the view type link button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void lbSortBy_Click (object sender, EventArgs e) 
		{
			lbSortByName.CssClass = lbSortByDate.CssClass = "";

			if (((LinkButton)sender).Attributes["sortby"].ToString().ToLower() == "name")
			{
				CurrentSort = "u.username";
				CurrentSortOrder = "ASC";
				lbSortByName.CssClass = "selected";
			}
			else
			{
				CurrentSort = "message_date";
				CurrentSortOrder = "DESC";
				lbSortByDate.CssClass = "selected";							  
			}

			BindData (pgTop.CurrentPageNumber, GetUserId ());
		}

		/// <summary>
		/// Execute when the user selects an item from the Store Filter
		/// Items to display drop down list
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void FilterChanged(object sender, FilterChangedEventArgs e)
		{
			searchFilter.SetPagesDropValue(searchFilter.NumberOfPages.ToString());
			
			BindData (1, GetUserId(), e.Filter);
		}
		#endregion

		#region Properties
		/// <summary>
		/// DEFAULT_SORT
		/// </summary>
		/// <returns></returns>
		protected override string DEFAULT_SORT
		{
			get
			{
				return "message_date";
			}
		}

		/// <summary>
		/// DEFAULT_SORT ORDER
		/// </summary>
		/// <returns></returns>
		protected override string DEFAULT_SORT_ORDER
		{
			get
			{
				return "DESC";
			}
		}

		#endregion

		#region Declerations
		protected Kaneva.Pager pgTop;
		protected Kaneva.SearchFilter searchFilter;
		protected System.Web.UI.HtmlControls.HtmlTable MessageFound;
		protected System.Web.UI.HtmlControls.HtmlTable noMessageFound;

		protected Label			lblSearch;					  
		protected Repeater		rptMailBox;	
		protected LinkButton	lbSortByName, lbSortByDate;
		protected HyperLink		lnkViewSent;

		protected HtmlContainerControl	spnAlertMsg, pStatusTxt;

		#endregion

		// Logger
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

		#region Web Form Designer generated code

		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
			pgTop.PageChanged +=new PageChangeEventHandler (pgTop_PageChange);
			searchFilter.FilterChanged +=new FilterChangedEventHandler (FilterChanged);		
		}
		#endregion
	}
}
