<%@ Page language="c#" ValidateRequest="False" Codebehind="NewMessage.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.newMessage" %>
<%@ Register TagPrefix="Kaneva" TagName="HeaderText" Src="../usercontrols/HeaderText.ascx" %>
<%@ Register TagPrefix="CE" Namespace="CuteEditor" Assembly="CuteEditor" %>
<%@ Register TagPrefix="cc2" Namespace="WebControlCaptcha" Assembly="WebControlCaptcha" %>

<link href="../css/home.css" rel="stylesheet" type="text/css" />		
<link href="../css/kanevaSC.css" rel="stylesheet" type="text/css"/>
<link href="../css/friends.css" rel="stylesheet" type="text/css"/>
<link href="../css/kanevaText.css" type="text/css" rel="stylesheet">
<link href="../css/kanevaBroadBand.css" rel="stylesheet" type="text/css" />		
<link href="../css/NEW.css" rel="stylesheet" type="text/css" />		

<script src="../jscript/prototype.js" type="text/javascript" language="javascript"></script>


<script type="text/javascript">

function paste(str)
{
	// get the cute editor instance
	var editor1 = document.getElementById('<asp:literal id="litEditorId" runat="server" />');

	var url = 'http://<asp:literal id="litSiteName" runat="server"/>' + str;
	
	// pasting the specified HTML into a range within a editor document 
	editor1.PasteHTML('<a href="' + url + '">' + url + '</a>');
}
</script>

<asp:PlaceHolder id="phBreadCrumb" runat="server"/>


<table border="0" cellspacing="0" cellpadding="0"  width="990" align="center">
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" /></td>
	</tr>
	<tr>
		<td>
			<table  border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="frTopLeft"></td>
					<td class="frTop"></td>
					<td class="frTopRight"></td>
				</tr>
				<tr>
					<td class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
					<td valign="top" class="frBgIntMembers">
						
						<table width="100%"  border="0" cellspacing="0" cellpadding="0"">
							<tr>
								<td>
								
									<!-- TOP STATUS BAR -->
									<div id="pageheader">
										<table cellpadding="0" cellspacing="0" border="0" width="99%">
											<tr>
												<td align="left">
													<h1 id="h1Title" runat="server">Compose Message</h1>
												</td>
												<td align="right" valign="middle">
													<table cellpadding="0" cellspacing="0" border="0" width="690">
														<tr>
															<td class="headertout" width="175"></td>
															<td class="searchnav" width="260"></td>
															<td align="left" width="130"></td>
															<td class="searchnav" align="right" width="210" nowrap></td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</div>
									<!-- END TOP STATUS BAR -->	
								
								</td>
							</tr>
							<tr>
								<td width="968" align="left"  valign="top">
									
									<div id="divSuccess" runat="server" style="display:none;">
									<table cellpadding="0" cellspacing="0" border="0" width="968">
										<tr>
											<td width="250" valign="top">
												<div class="module toolbar">
													<span class="ct"><span class="cl"></span></span>
													<h2>Tools & Actions <span class="hlink"></span></h2>
													<table cellspacing="0" cellpadding="10" border="0" width="99%">
														<tr>
															<td align="center">
																<table cellspacing="0" cellpadding="3" border="0" width="100%">
																	<tr>
																		<td align="left">
																			<asp:label id="lblGoDesc" runat="server"></asp:label>	
																		</td>
																		<td align="right">
																			<a id="lnkSuccessGo" runat="server">
																			<img src="~/images/button_go.gif" id="btnGoSuccess" runat="server" causesvalidation="False" alt="Go" width="57" height="23" border="0" vspace="2" /><br>
																			</a>
																		</td>
																	</tr>
																	<tr>
																		<td colspan="2">
																			<div id="divBackToChannel" runat="server" visible="false">
																			<br/>
																			<a id="lnkBackToChannel" runat="server">Back to Community</a>
																			</div>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>										
													</table>
													<span class="cb"><span class="cl"></span></span>
												</div>
											</td>
											<td width="20"><img runat="server" src="~/images/spacer.gif" width="20" height="1" id="Img1"/></td>
											<td width="748" valign="top" align="center">
												
												<div class="module whitebg">
													<span class="ct"><span class="cl"></span></span>
													
													<span id="spnAlertMsgSuccess" runat="server" class="infoBox black" style="width:90%;margin-bottom:12px;"></span>
													
													<table width="99%" id="tblSuccess" border="0" cellpadding="5" cellspacing="0">
														<tr>
															<td width="99%"><br/>															
																<table width="100%" border="0" cellspacing="0" cellpadding="5">
																	<tr>
																		<td align="left" width="60">
																			<!-- BOX -->
																			<div class="framesize-xsmall" id="divAvatar" runat="server" visible="false">
																				<div class="frame">
																					<span class="ct"><span class="cl"></span></span>
																						<div class="imgconstrain" style="text-align:left;">
																							<a id="lnkMember" runat="server"><img runat="server" id="imgAvatar" border="0" width="45" height="33"/></a>
																						</div>
																					<span class="cb"><span class="cl"></span></span>
																				</div>
																			</div>		
																			<!-- END BOX -->
																		</td>
																		<td id="tdSuccessMsg" runat="server" align="left"></td>
																	</tr>
																	<tr id="trMeetMe3DSuccess" runat="server" style="display:none;">
																		<td colspan="2">
																			<table border="0" cellpadding="0" cellspacing="0" width="100%">
																				<tr>
																					<td align="left">
																						<h1 runat="server">Meet Me 3D</h1>
																					</td>
																				</tr>
																				<tr>
																					<td style="padding-left: 14px" class="textGeneralGray01">
																						<asp:label  id="lblMM3DSuccessMessage" runat="server" />
																					</td>
																				</tr>
																				<tr>
																					<td align="middle">
																						<asp:literal id="litMM3DLocation2" runat="server" visible="true" />
																					</td>
																				</tr>
																			</table>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>			
															
													<span class="cb"><span class="cl"></span></span>
												</div>
											</td>
										</tr>
									</table>
									</div>
									
									
									<div id="divSendMessage" runat="server">
									<table cellpadding="0" cellspacing="0" border="0" width="968" id="tblComposeMessage" runat="server" visible="true">
										<tr>
											<td width="200" valign="top">
												<div class="module toolbar">
													<span class="ct"><span class="cl"></span></span>
													<h2>Tools & Actions <span class="hlink"></span></h2>
													<table cellspacing="0" cellpadding="10" border="0" width="99%">
														<tr>
															<td align="center">
																<table cellspacing="0" cellpadding="3" border="0" width="100%">
																	<tr>
																		<td>
																			
																			<asp:imagebutton imageurl="~/images/button_sendnow.gif" id="btnSend" runat="server" alternateText="Send Now" onclick="btnSend_Click" width="77" height="23" border="0" vspace="2" /><br>
																			
																		</td>
																	</tr>
																	<tr>
																		<td>
																			<asp:imagebutton imageurl="~/images/button_cancel.gif" id="btnCancel" runat="server" causesvalidation="False" onclick="btnCancel_Click" alt="Cancel" width="57" height="23" border="0" vspace="2" /><br>
																		</td>
																	</tr>
																</table>
																<hr><br>						   
																<p align="left"><asp:linkbutton id="lbBack" runat="server" causesvalidation="False" onclick="lbBack_Click">Back</asp:linkbutton></p>
															</td>
														</tr>										
													</table>
													<span class="cb"><span class="cl"></span></span>
												</div>
											</td>
											<td width="20"><img runat="server" src="~/images/spacer.gif" width="20" height="1" /></td>
											<td width="748" valign="top" align="center">
												
												<div class="module whitebg">
													<span class="ct"><span class="cl"></span></span>
													
													<span id="spnAlertMsg" runat="server" class="errBox black" style="width:90%;margin-bottom:12px;display:none;"></span>
													
													<table width="99%" id="dlFriends" border="0" cellpadding="5" cellspacing="0">
														<tr>
															<td width="99%">
																
																<table width="100%" border="0" cellspacing="0" cellpadding="5">
																	
																	<tr id="trMeetMe3DLink" runat="server" visible="false">
																		<td colspan="5">
																			<table border="0" cellpadding="0" cellspacing="0" width="100%">
																				<tr>
																					<td align="left">
																						<h1 runat="server">Meet Me 3D</h1>
																					</td>
																				</tr>
																				<tr>
																					<td style="padding-left: 14px" class="textGeneralGray01">
																						<asp:label  id="lblMM3DMessage" runat="server" />
																					</td>
																				</tr>
																				<tr>
																					<td align="middle">
																						<asp:literal id="litMM3DLocation" runat="server" visible="true" />
																					</td>
																				</tr>
																			</table>
																		</td>
																	</tr>
																	<tr id="trEmail" runat="server">
																		<td align="right" nowrap class="textGeneralGray01"><strong>Send via:</strong></td>
																		<td align="left" nowrap class="textGeneralGray01"><input type="radio" class="formKanevaText" id="optEmail" name="optSendTo" runat="server" value="optEmail"/>&nbsp;Email:</td>
																		<td><img runat="server" src="~/images/spacer.gif" width="6" height="6" /></td>
																		<td align="left"><asp:textbox id="txtEmail" cssclass="formKanevaText" style="width:300px" maxlength="100" runat="server"/></td>
																		<td width="48%" align="left" class="textGeneralGray01">&nbsp;</td>
																	</tr>
																	<tr id="trUsername" runat="server">
																		<td align="right" nowrap class="textGeneralGray01"></td>
																		<td align="left" nowrap class="textGeneralGray01"><input type="radio" class="formKanevaText" id="optUser" name="optSendTo" runat="server" value="optUser"/>&nbsp;Message to Kaneva Member:</td>
																		<td>&nbsp;</td>
																		<td align="left" class="textGeneralGray03"><asp:textbox id="txtUsername" cssclass="formKanevaText" style="width:300px" maxlength="100" runat="server"/></td>
																		<td width="48%" align="left" class="textGeneralGray01">&nbsp;</td>
																	</tr>
																	<tr id="trSubject" runat="server">
																		<td align="right" nowrap class="textGeneralGray01"></td>
																		<td align="right" nowrap class="textGeneralGray01">Subject:</td>
																		<td>&nbsp;</td>
																		<td align="left" class="textGeneralGray03">												
																			<asp:textbox id="txtSubject" class="formKanevaText" style="width:300px" maxlength="100" runat="server"/>
																		</td>
																		<td width="48%" align="left" class="textGeneralGray01">&nbsp;</td>
																	</tr>
																	<tr id="trInsertMediaLink" runat="server" visible="false">
																		<td align="right" nowrap class="textGeneralGray01"></td>
																		<td colspan="3">
																			<span style="padding-left:146px;">
																				<a href='#' onclick="javascript:window.open('selectMedia.aspx?tbName=txtModulePicture','add','toolbar=no,width=550,height=400,menubar=no,scrollbars=yes').focus();return false;">Insert Link from your Media Uploads...</a>
																			</span>
																		</td>
																		<td width="48%" align="left" class="textGeneralGray01">&nbsp;</td>
																	</tr>
																	<tr>
																		<td colspan="5" nowrap class="textGeneralGray01"><img runat="server" src="~/images/spacer.gif" width="1" height="4" /></td>
																	</tr>  <input type="hidden" id="hidLinkURL" runat="server" value="" name="hidLinkURL"/>
																</table>
																
																<table border="0" cellspacing="0" cellpadding="0" width="100%">
																	<tr>																																											   
																		<td align="right">
																			<ce:editor id="txtBody" runat="server" maxtextlength="100000" bgcolor="#ededed" width="100%" height="200px" showhtmlmode="False" configurationpath="~/CuteSoft_Client/CuteEditor/Configuration/AutoConfigure/forum.config" autoconfigure="None" ></ce:editor>
																			<br />
																			<cc2:CaptchaControl id="CaptchaControl1" runat="server" Visible="False" Text="Type the code above to post your share." width="250" height="60" captchLength="5" alt="Test of alternate Tag" showsubmitbutton="false" /> <br>																													
																				
																			<asp:imagebutton imageurl="~/images/button_sendnow.gif" id="btnSendBottom" runat="server" alternatetext="Send Now" onclick="btnSend_Click" width="77" height="23" border="0" vspace="2" imagealign="Right" /><br>
																		</td>
																	</tr>
																</table>
																<input type="hidden" runat="server" id="hidMessageId" name="hidMessageId">
																
																<asp:linkbutton id="btnAddFriend" runat="server" onclick="btnAddFriend_Click" style="display:none;"></asp:linkbutton>
															</td>
														</tr>
													</table>
													<span class="cb"><span class="cl"></span></span>
												</div>
											</td>
										</tr>
									</table>
									</div>
									
									
								</td>
							</tr>
						</table>
						
					</td>
					<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
				</tr>
				<tr>
					<td class="frBottomLeft"></td>
					<td class="frBottom"></td>
					<td class="frBottomRight"></td>
				</tr>
				<tr>
					<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" /></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
	
    
    
    

