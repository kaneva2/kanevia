<%@ Page language="c#" Codebehind="mailboxBulletin.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.mykaneva.mailboxBulletin" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../usercontrols/Pager.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="SearchFilter" Src="../usercontrols/SearchFilter.ascx" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>

<link href="../css/home.css" rel="stylesheet" type="text/css" />		
<link href="../css/kanevaSC.css" rel="stylesheet" type="text/css"/>
<link href="../css/friends.css" rel="stylesheet" type="text/css"/>
<link href="../css/kanevaText.css" type="text/css" rel="stylesheet">
<link href="../css/new.css" type="text/css" rel="stylesheet">

<script src="../jscript/prototype.js" type="text/javascript" language="javascript"></script>


<table border="0" cellspacing="0" cellpadding="0"  width="990" align="center">
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" id="Img1"/></td>
	</tr>
	<tr>
		<td>
			<table  border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="frTopLeft"></td>
					<td class="frTop"></td>
					<td class="frTopRight"></td>
				</tr>
				<tr>
					<td class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img2"/></td>
					<td valign="top" class="frBgIntMembers">
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr colspan="2">
								<td>
								
									<!-- TOP STATUS BAR -->
									<div id="pageheader">
										<table cellpadding="0" cellspacing="0" border="0" width="99%">
											<tr>
												<td align="left">
													<h1>Group Bulletins</h1>
												</td>
									
												<td align="right" valign="middle">
													<table cellpadding="0" cellspacing="0" border="0" width="690">
														<tr>
															<td>	
																<ajax:ajaxpanel id="ajpTopStatusBar" runat="server">
																<table cellpadding="0" cellspacing="0" border="0" width="100%">
																	<tr>
																		<td class="headertout" width="175">
																			<asp:label runat="server" id="lblSearch" />
																		</td>
																		<td class="searchnav" width="260"> Sort by: 
																			<asp:linkbutton id="lbSortByDate" runat="server" sortby="Date" onclick="lbSortBy_Click">Date Received</asp:linkbutton> | 
																			<asp:linkbutton id="lbSortByName" runat="server" sortby="Name"  onclick="lbSortBy_Click">From</asp:linkbutton>
																		</td>
																		<td align="left" width="130">
																			<kaneva:searchfilter runat="server" id="searchFilter" loc="top" hidethumbview="true" showfilterlists="false" assetsperpagelist="10,20,40" />
																		</td>
																		<td class="searchnav" align="right" width="210" nowrap>
																			<kaneva:pager runat="server" isajaxmode="True" id="pgTop" maxpagestodisplay="5" shownextprevlabels="true" />
																		</td>
																	</tr>
																</table>
																</ajax:ajaxpanel>
															</td>	
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</div>
									<!-- END TOP STATUS BAR -->	
								
								</td>
							</tr>
							<tr>
								<td width="968" align="left"  valign="top">
									<table cellpadding="0" cellspacing="0" border="0"  width="968">
										<tr>
											<td width="250" valign="top">
												<div class="module toolbar">
													<span class="ct"><span class="cl"></span></span>
													<h2>Tools & Actions <span class="hlink"></span></h2>
													<table cellspacing="0" cellpadding="10" border="0" width="99%">
														<tr>
															<td align="center">
																<table cellspacing="0" cellpadding="3" border="0" width="100%">
																	<tr>
																		<td>Compose a Bulletin:</td>
																		<td align="center">
																			
																			<a href="~/mykaneva/newmessage.aspx?bulletin=" runat="server"><img SRC="~/images/button_go.gif" alt="Compose" width="57" height="23" border="0" vspace="2" runat="server" /></a>
																			
																		</td>
																	</tr>
																	<tr>
																		<td><br/><br/>
																			<a href="~/mykaneva/mailboxsent.aspx?type=bulletin" runat="server">View Sent Group Bulletins</a>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
													<span class="cb"><span class="cl"></span></span>
												</div>
											</td>
											<td width="20"><img runat="server" src="~/images/spacer.gif" width="20" height="1" id="Img3"/></td>
											<td width="698" valign="top" align="center">
												
												<ajax:ajaxpanel id="ajpMessageList" runat="server">
												<div class="module whitebg">
													<span class="ct"><span class="cl"></span></span>
													<h2 class="alertmessage"><span id="spnAlertMsg" runat="server"></span></h2>
													
													<!-- Messages -->
													<table width="99%" id="dlFriends" border="0" cellpadding="5" cellspacing="0">
														<tr>
															<td align="center" width="99%">
															
															<div align="left" style="padding-left:3px;">
																	<table id="MessageFound" runat="server" width="99%" cellspacing="0" cellpadding="5" border="0" class="data">
																		<tr>
																			<th bgcolor="#fdfdfc" class="name">Message</th>
																		</tr>
																	</table>
																	<table id="noMessageFound" runat="server" width="99%" cellspacing="0" cellpadding="5" border="0" class="data">
																		<tr>
																			<th bgcolor="#fdfdfc" class="name">You currently have no bulletins.</th>
																		</tr>
																	</table>
															<asp:repeater runat="server" id="rptBulletins">  
																
																<headertemplate>
																	<table width="99%" cellspacing="0" cellpadding="5" border="0" class="data">
																</headertemplate>
																
																<itemtemplate>
																	
																	<tr class="altrow">
																		<td width="500">
																			<table cellpadding="0" cellspacing="0" border="0">
																				<tr>
																					<td align="right" width="36" rowspan="2">
																						<a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "name_no_spaces").ToString ()) %>><img runat="server" src='<%#GetProfileImageURL (DataBinder.Eval(Container.DataItem, "thumbnail_small_path").ToString ())%>' width="30" border="0" hspace="5" id="Img5"/></a>
																					</td>
																					<td><h1><a runat="server" title='<%# RemoveHTML (TruncateWithEllipsis (DataBinder.Eval(Container.DataItem, "message").ToString (), 100))%>' href='<%# "mailboxBulletinDetails.aspx?bulletinId=" + DataBinder.Eval(Container.DataItem, "bulletin_id").ToString ()%>' id="A1">
																							<%# TruncateWithEllipsis (DataBinder.Eval(Container.DataItem, "subject").ToString (), 48)%>
																						</a></h1>
																					</td>
																				</tr>
																				<tr>
																					<td>
																						<p>From: <a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "name_no_spaces").ToString ()) %>>
																							<%#TruncateWithEllipsis (DataBinder.Eval(Container.DataItem, "username").ToString (), 20)%>
																						</a> | Sent: <%#FormatDate (DataBinder.Eval(Container.DataItem, "message_date"))%></p>
																					</td>
																				</tr>
																			</table><input type="hidden" runat="server" id="hidBulletinId" value='<%#DataBinder.Eval(Container.DataItem, "bulletin_id")%>' name="hidBulletinId">
																		</td>
																	</tr>
																	
																</itemtemplate>
																
																<alternatingitemtemplate>
																	
																	<tr>
																		<td width="500">
																			<table cellpadding="0" cellspacing="0" border="0">
																				<tr>
																					<td align="right" width="36" rowspan="2">
																						<a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "name_no_spaces").ToString ()) %>><img runat="server" src='<%#GetProfileImageURL (DataBinder.Eval(Container.DataItem, "thumbnail_small_path").ToString ())%>' width="30" border="0" hspace="5" id="Img7"/></a>
																					</td>
																					<td><h1><a runat="server" title='<%# RemoveHTML (TruncateWithEllipsis (DataBinder.Eval(Container.DataItem, "message").ToString (), 100))%>' href='<%# "mailboxBulletinDetails.aspx?bulletinId=" + DataBinder.Eval(Container.DataItem, "bulletin_id").ToString ()%>' id="A2">
																							<%# TruncateWithEllipsis (DataBinder.Eval(Container.DataItem, "subject").ToString (), 48)%>
																						</a></h1>
																					</td>
																				</tr>
																				<tr>
																					<td>
																						<p>From: <a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "name_no_spaces").ToString ()) %>>
																							<%#TruncateWithEllipsis (DataBinder.Eval(Container.DataItem, "username").ToString (), 20)%>
																						</a> | Sent: <%#FormatDate (DataBinder.Eval(Container.DataItem, "message_date"))%></p>
																					</td>
																				</tr>
																			</table><input type="hidden" runat="server" id="hidBulletinId" value='<%#DataBinder.Eval(Container.DataItem, "bulletin_id")%>' name="hidBulletinId">
																		</td>
																	</tr>
																	
																</alternatingitemtemplate>
																
																<footertemplate>
																	</table>
																</footertemplate>
																
															</asp:repeater>			
														
														</td>
													</tr>
												</table>
												<span class="cb"><span class="cl"></span></span>
												</div>
												</ajax:ajaxpanel>
												
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img8"/></td>
				</tr>
				<tr>
					<td class="frBottomLeft"></td>
					<td class="frBottom"></td>
					<td class="frBottomRight"></td>
				</tr>
				<tr>
					<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" id="Img9"/></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
