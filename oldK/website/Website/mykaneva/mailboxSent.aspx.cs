///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using log4net;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for mailboxSent.
	/// </summary>
	public class mailboxSent : SortedBasePage
	{
		protected mailboxSent () 
		{
			Title = "Sent Messages";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			// They must be logged in
			if (!Request.IsAuthenticated)
			{
				Response.Redirect (GetLoginURL ());
				return;
			}

            // Redirect all requests to new new message center page
            Response.Redirect (ResolveUrl ("~/mykaneva/messagecenter.aspx?mc=ms"));






			if (!IsPostBack)
			{
				CurrentSort = "message_date";
				lbSortByDate.CssClass = "selected";
				
				if (Request["type"] != null)
				{
					SetMessageType( Request["type"].ToString() );	
				}
				
				int userId = GetUserId ();
				BindData (1, userId);

				//	LoadDropdown (userId);
			}

			// Set Nav
            HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
			HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.MESSAGES;
			HeaderNav.SetNavVisible(HeaderNav.MyKanevaNav,2);
			//HeaderNav.SetNavVisible(HeaderNav.MyMessagesNav);

			if ( MESSAGE_TYPE == MSG_TYPE.MESSAGE )
			{
				//HeaderNav.MyMessagesNav.ActiveTab = NavMessages.TAB.INBOX;
			}
			/*else
			{
				HeaderNav.MyMessagesNav.ActiveTab = NavMessages.TAB.BULLETINS;
			}*/
		}

		/// <summary>
		/// Bind the data
		/// </summary>
		/// <param name="pageNumber"></param>
		private void BindData (int pageNumber, int userId)
		{
			BindData (pageNumber, userId, "");	
		}
		/// <summary>
		/// Bind the data
		/// </summary>
		/// <param name="pageNumber"></param>
		private void BindData (int pageNumber, int userId, string filter)
		{
			searchFilter.CurrentPage = "messagessent";

			string msg_type = string.Empty;
			string orderby = CurrentSort + " " + CurrentSortOrder;
			int pgSize = searchFilter.NumberOfPages;
			PagedDataTable pds;

			rptMailBox.Visible = false;
			rptBulletins.Visible = false;

			//if ( MESSAGE_TYPE == MSG_TYPE.MESSAGE)
			//{
				pds = UsersUtility.GetSentMessages (userId, "", orderby, pageNumber, pgSize);
				rptMailBox.DataSource = pds;
				rptMailBox.DataBind ();
				rptMailBox.Visible = true;
				
				msg_type = "bulletin";
				hlView.Text = "View Sent Bulletins";

				spnTitle.InnerText = "Messages Sent";
				spnSortByDate.Visible = true;
				lbSortByDate.Enabled = true;
			//}
			/*else
			{
				pds = UsersUtility.GetSentBulletins (userId, "", orderby, pageNumber, pgSize);
				rptBulletins.DataSource = pds;
				rptBulletins.DataBind ();
				rptBulletins.Visible = true;
				
				msg_type = "message";
				hlView.Text = "View Sent Messages";

				spnTitle.InnerText = "Bulletins Sent";
				spnSortByDate.Visible = false;
				lbSortByDate.Enabled = false;
			}*/

			hlView.NavigateUrl = "mailboxSent.aspx?type=" + msg_type;

			pgTop.NumberOfPages = Math.Ceiling ((double) pds.TotalCount / pgSize).ToString ();
			pgTop.DrawControl ();

			// The results
			lblSearch.Text = GetResultsText (pds.TotalCount, pageNumber, pgSize, pds.Rows.Count);
		}

		
		#region Helper Methods
		protected string GetBulletinRecipient(string channelName, string groupName)
		{
			if (channelName != string.Empty)
			{
				return channelName + " community members";					  
			}
			else if (groupName != string.Empty)
			{
				return groupName + " friend group";
			}
			else
			{
				return "All friends";
			}
		}
		private void SetMessageType(string type)
		{
			switch (type.ToLower())
			{
				case "bulletin":
					MESSAGE_TYPE = MSG_TYPE.BULLETIN;
					break;
					
				default:
					MESSAGE_TYPE = MSG_TYPE.MESSAGE;
					break;
			}
		}
		#endregion

		#region Event Handlers
		/// <summary>
		/// Page Change
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pgTop_PageChange (object sender, PageChangeEventArgs e)
		{
			MESSAGE_TYPE = CurrentMessageType;
			
			BindData (e.PageNumber, GetUserId ());
		}

		/// <summary>
		/// Execute when the user selects an item from the Store Filter
		/// Items to display drop down list
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void FilterChanged(object sender, FilterChangedEventArgs e)
		{
			MESSAGE_TYPE = CurrentMessageType;
			searchFilter.SetPagesDropValue(searchFilter.NumberOfPages.ToString());
			
			BindData (1, GetUserId(), e.Filter);
		}
		/// <summary>
		/// Execute when the user clicks the the view type link button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void lbSortBy_Click (object sender, EventArgs e) 
		{
			lbSortByName.CssClass = lbSortByDate.CssClass = "";
			MESSAGE_TYPE = CurrentMessageType;

			if (((LinkButton)sender).Attributes["sortby"].ToString().ToLower() == "name")
			{
				CurrentSort = "u.username";
				CurrentSortOrder = "ASC";
				lbSortByName.CssClass = "selected";
			}
			else
			{
				CurrentSort = "message_date";
				CurrentSortOrder = "DESC";
				lbSortByDate.CssClass = "selected";							  
			}

			BindData (pgTop.CurrentPageNumber, GetUserId());
		}
		
		/// <summary>
		/// Click the send message button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnSendMessage_Click (Object sender, ImageClickEventArgs e) 
		{
			Server.Transfer (ResolveUrl ("~/myKaneva/newMessage.aspx"));
		}

		protected void lbView_Click(Object sender, CommandEventArgs e)
		{
			if (e.CommandArgument.ToString () == "bulletins")
			{
				MESSAGE_TYPE = MSG_TYPE.BULLETIN;
			}
			else
			{
				MESSAGE_TYPE = MSG_TYPE.MESSAGE;
			}

			lbSortByName.CssClass = lbSortByDate.CssClass = "";
			CurrentSort = "message_date";
			CurrentSortOrder = "DESC";
			lbSortByDate.CssClass = "selected";							  
			
			BindData(1, GetUserId());
		}

		/// <summary>
		/// Execute when the user clicks the Delete button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnDelete_Click (object sender, ImageClickEventArgs e)
		{
			HtmlInputHidden hidMessageId;
			CheckBox chkEdit;
			int ret = 0;
			int count = 0;
			int userId = GetUserId ();
			bool isItemChecked = false;

			if ( MESSAGE_TYPE == MSG_TYPE.MESSAGE)
			{
				// Remove the messages
				foreach (RepeaterItem dliMessage in rptMailBox.Items)
				{
					chkEdit = (CheckBox) dliMessage.FindControl ("chkEdit");

					if (chkEdit.Checked)
					{
						isItemChecked = true;

						hidMessageId = (HtmlInputHidden) dliMessage.FindControl ("hidMessageId");
					
						if (!UsersUtility.IsMessageRecipient (Convert.ToInt32 (hidMessageId.Value), GetUserId ()))
						{
							m_logger.Warn ("User " + GetUserId () + " from IP " + Common.GetVisitorIPAddress() + " tried to delete message " + hidMessageId.Value + " but is not the message owner");
							return;
						}

						ret = UsersUtility.DeleteSentMessage (userId, Convert.ToInt32 (hidMessageId.Value));
						
						if (ret == 1)
							count++;
					}
				}

				if (isItemChecked)
				{
					if (count > 0)
					{
						BindData (pgTop.CurrentPageNumber, userId);
					}
					
					if (ret != 1)
					{
						spnAlertMsg.InnerText = "An error was encountered while trying to delete messages.";
					}
					else
					{
						spnAlertMsg.InnerText = "Successfully deleted " + count + " message" + (count != 1 ? "s" : "") + ".";
					}
				}
				else
				{
					spnAlertMsg.InnerText = "No messages were selected.";
				}

				MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('spnAlertMsg', 5000);");
			}
			else
			{
			}


		}

		#endregion

		#region Properties
		/// <summary>
		/// DEFAULT_SORT
		/// </summary>
		/// <returns></returns>
		protected override string DEFAULT_SORT
		{
			get
			{
				return "message_date";
			}
		}

		/// <summary>
		/// DEFAULT_SORT ORDER
		/// </summary>
		/// <returns></returns>
		protected override string DEFAULT_SORT_ORDER
		{
			get
			{
				return "DESC";
			}
		}

		private MSG_TYPE MESSAGE_TYPE
		{
			get	{ return m_msg_type; }
			set { m_msg_type = value; }
		}
		private MSG_TYPE CurrentMessageType
		{
			get
			{
				if (rptBulletins.Visible)
				{
					return MSG_TYPE.BULLETIN;
				}
				else 
				{
					return MSG_TYPE.MESSAGE;
				}
			}
		}
		#endregion

		#region Declerations
		protected Kaneva.Pager pgTop;
		protected Kaneva.SearchFilter searchFilter;

		protected Label					lblSearch;
		protected Repeater				rptMailBox, rptBulletins;	
		protected LinkButton			lbSortByName, lbSortByDate;
		protected HyperLink				hlView;
		protected HtmlContainerControl	spnAlertMsg, spnSuccessAlertMsg, divConfirm, spnTitle, spnSortByDate;

		private const int MESSAGES_PER_PAGE = 25;
		protected PlaceHolder phBreadCrumb;
		
		private MSG_TYPE m_msg_type = MSG_TYPE.MESSAGE;
		private enum MSG_TYPE
		{
			MESSAGE,
			BULLETIN
		}

		// Logger
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);
		#endregion


		#region Web Form Designer generated code

		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
			pgTop.PageChanged +=new PageChangeEventHandler (pgTop_PageChange);
			searchFilter.FilterChanged +=new FilterChangedEventHandler (FilterChanged);		
		}
		#endregion
	}
}
