<%@ Page language="c#" MasterPageFile="~/masterpages/GenericPageTemplate.Master" ValidateRequest="False" Codebehind="profile.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.profile2" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>

<asp:Content ID="cnt_ProfileInfoHead" runat="server" contentplaceholderid="cph_HeadData" >
    <meta name="title" content="Kaneva. The Online Community and Social Network." />
    <meta name="description" content="Kaneva is the first to combine social networking and a free 3D virtual world. It's a whole new way to connect with friends. Have fun in a vibrant virtual world full of people, cool places and fun games. Create your avatar and get a free 3D home today!"/>
    <meta name="keywords" content="free virtual world, virtual world, virtual worlds, 3D world, online avatar community, mmo game, virtual reality, caneva, kaniva" />
    
    <NOSCRIPT><META HTTP-EQUIV="refresh" CONTENT="1" URL="noJavascript.aspx" /></NOSCRIPT>

    
    <script type="text/javascript" src="../jscript/prototype.js"></script>
    <script type="text/javascript" src="../jscript/scriptaculous/scriptaculous.js"></script>

    <!-- Dependencies -->
    <script type="text/javascript" src="../jscript/yahoo/2.3.0/yahoo-min.js"></script>
    <script type="text/javascript" src="../jscript/yahoo/2.3.0/dom-min.js"></script>
    <script type="text/javascript" src="../jscript/yahoo/2.3.0/yahoo-dom-event.js"></script>
    									
    <script type="text/javascript" src="../jscript/yahoo/2.3.0/connection-min.js"></script>  
    <script type="text/javascript" src="../jscript/yahoo/2.3.0/autocomplete-min.js"></script>

    <script type="text/javascript" src="../jscript/kanevaLightbox.js"></script>

    <script type="text/javascript" src="../jscript/mykaneva/profile.js"></script>

    <!--CSS file (default YUI Sam Skin) -->
    <link href="../css/yahoo/autocomplete/skins/sam/autocomplete.css" rel="stylesheet" type="text/css">
    <link href="../css/lightbox.css" rel="stylesheet" type="text/css">

</asp:Content>

<asp:Content ID="mainProfileContent" runat="server" contentplaceholderid="cph_Body">
<link href="../css/mykaneva/profile.css" rel="stylesheet" type="text/css">
<script type="text/javascript" language="javascript">
    var remBtnId = '<asp:literal id="litRemoveButtonClientId" runat="server"/>';
    var addBtnId = '<asp:literal id="litAddButtonClientId" runat="server"/>';
    SetPageNamePrefix('<%=alertMsg.NamingContainer.ClientID%>_');
</script>
<script type="text/javascript" src="../jscript/jquery-ui/jquery-ui.min.js"></script>
<script>
    var $j = jQuery.noConflict();
    $j(document).ready(function () {
        $j('.imgAddInterest').click(function (event) {
            addInterest($j(this).attr('ic_id'));
        });
    });
</script>
    <div id="editBody">
	    <div id="messageArea">
	        <asp:ValidationSummary ShowMessageBox="False" ShowSummary="True" cssclass="errBox" id="valSum" DisplayMode="BulletList" runat="server" HeaderText="Please correct the following errors:" />
	        <div id="alertMsg" runat="server" class="errBox" visible="false"></div>
            <ajax:ajaxpanel id="ajpMessage" runat="server">
                <span id="spnAlertMsg" class="infoBox" runat="server" style="display:none;"></span>
            </ajax:ajaxpanel>
	    </div>
        <div id="pageHeader">
            <div>
                <h6>EDIT: My Interests</h6>
            </div>
            <div class="restrictions">
                <a href="~/myKaneva/personal.aspx" class="btnlarge grey" id="changePersonalInfo" runat="server">Edit Personal Info</a>
            </div>
        </div>
        <div id="personal">
		    <div >
		        <span style="font-weight:bold;padding-right:10px;top:-80px; position:relative">About Me:</span>
		        <asp:textbox id="txtAboutMe" columns="70" maxlength="1000" rows="6" tabindex="2" onkeyup="javascript:if($('btnUpdate').disabled)$('btnUpdate').disabled=false;" textmode="MultiLine" wrap="true" runat="server" />
		    </div>
        </div>
        <div id="interest" style="text-align:left;">
		    <div style="text-align:left;"><span style="color:#999"><b>Instructions:</b> Add your interests by clicking on the green plus sign.</span></div>
		    <div id="tdCategories" runat="server" class="yui-skin-sam data">
					<!-- catagories go here -->
			</div>

        </div>        
        <div id="submitArea">
            <asp:linkbutton id="Button1" onclick="btnUpdate_Click" runat="Server" cssclass="btnmedium orange" Text="Save Changes" CausesValidation="True"></asp:linkbutton>
            <asp:linkbutton id="btnCancel" onclick="btnCancel_Click" runat="Server" cssclass="btnxsmall orange" Text="Close" CausesValidation="False"></asp:linkbutton>
        </div>
    </div>
    
<div id="lightbox">
	<table width="100%" cellpadding="2" cellspacing="0" border="0">
		<tr valign="middle"><td class="titleLB"><div style="float:left;text-align:center;width:95%;">&nbsp;&nbsp;Did you mean to enter multiple interests?</div><div class="closeBtn" title=" Close " onclick="deactivateLB();">X</div></td></tr>
		<tr>
			<td align="center"><br />
				<table width="80%" height="95%" cellpadding="2" cellspacing="0" border="0">
					<tr valign="top">
						<td align="left" width="50%" valign="top">
							<span id="spnMultiple"></span><br />
						</td>
						<td width="20" rowspan="2"><img id="Img4" runat="server" src="~/images/spacer.gif" width="20" height="1" /></td>
						<td align="center" width="50%" valign="top">
							<span id="spnSingle"></span>
						</td>
					</tr>
					<tr valign="bottom">
						<td align="left">
							<ajax:ajaxpanel id="Ajaxpanel1" runat="server">
								<asp:button id="btnAddMultiple" onclick="btnAdd_Click" runat="server" text="Multiple Interests"/>
							</ajax:ajaxpanel>
						</td>
						<td align="center">
							<ajax:ajaxpanel id="Ajaxpanel2" runat="server">
							<asp:button id="btnAddSingle" onclick="btnAdd_Click" runat="server" text="Single Interest"/>
							</ajax:ajaxpanel>
						</td>
					</tr>
				</table><br />
			</td>
		</tr>
	</table>
</div>

<ajax:ajaxpanel ID="Ajaxpanel6" runat="server">
<asp:button id="btnAdd" onclick="btnAdd_Click" runat="server" style="display:none;"/>
<asp:button id="btnRemove" onclick="btnRemove_Click" runat="server" style="display:none;"/>
<input type="hidden" value="0" id="hidIc_id" runat="server" />
<input type="hidden" value="" id="hidInterest" runat="server" />
<input type="hidden" value="0" id="hidInterestId" runat="server" />
<input type="hidden" value="" id="hidTags" runat="server" />
</ajax:ajaxpanel>

</asp:Content>
