///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Text;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using KlausEnt.KEP.Kaneva.channel;
using KlausEnt.KEP.Kaneva.mykaneva;
using log4net;
using MagicAjax;
using KlausEnt.KEP.Kaneva.framework.widgets;
using KlausEnt.KEP.Kaneva.framework.utils;
using System.Xml;
using System.Net;
using AjaxControlToolkit;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

using Amazon.S3;
using Amazon.S3.Transfer;
using System.Collections.Generic;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for upload.
	/// </summary>
	public partial class upload : BasePage
	{
		protected upload () 
		{
			Title = "Upload Item(s)";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
            aHelp.HRef = Common.GetHelpURL(KanevaWebGlobals.CurrentUser.FirstName, KanevaWebGlobals.CurrentUser.LastName, KanevaWebGlobals.CurrentUser.Email);

			AddBreadCrumb (new BreadCrumb ("Upload", GetCurrentURL(), "", 0));

            lnk_AccessPass.HRef = GetAccessPassLink();
            lnk_AccessPassYouTube.HRef = lnk_AccessPassFlash.HRef = lnk_AccessPassGames.HRef = lnk_AccessPass.HRef;

			if(Request.IsAuthenticated)
			{
				if(!GetRequestParams())
				{
					RedirectToHomePage ();
				}
				else
				{
					//check if user is allowed to edit this channel
					if(!IsUserAllowedToEdit() && (_moduleId == -1))
					{
						RedirectToHomePage ();
					}
					else if (_moduleId != -1)
					{
						//check to see if user is a member of the channel and retreive their status - process accordingly
                        CommunityMember communityMember = GetCommunityFacade.GetCommunityMember(_channelId, GetUserId());

                        if (communityMember.StatusId.Equals (0))
						{
							Response.Redirect(ResolveUrl ("~/community/commJoin.aspx?communityId=" + _channelId + "&join=Y" + "&nav=back" ));
						}
						else
						{
							string message = "";

                            switch (communityMember.StatusId)
							{
                                case (UInt32) CommunityMember.CommunityMemberStatus.ACTIVE:
									break;
                                case (UInt32)CommunityMember.CommunityMemberStatus.DELETED:
									Response.Redirect(ResolveUrl ("~/community/commJoin.aspx?communityId=" + _channelId + "&join=Y" + "&nav=back" ));
									break;
                                case (UInt32)CommunityMember.CommunityMemberStatus.LOCKED:
									message = "You have been blocked from this World. \n Please contact the World owner for help.";
									break;
                                case (UInt32)CommunityMember.CommunityMemberStatus.PENDING:
                                    message = "Your World request is still pending. \n Please contact the World owner for help.";
									break;
                                case (UInt32)CommunityMember.CommunityMemberStatus.PENDINGPAYMENT:
                                    message = "Your member status is pending payment. \n Please contact the World owner for help.";
									break;
                                case (UInt32)CommunityMember.CommunityMemberStatus.REJECTED:
                                    message = "Your World request was rejected. \n Please contact the World owner for help.";
									break;
							}

							if (message != "")
							{
								ShowErrorOnStartup(message);
							}
						}
					}
				}
			}
			else
			{
				//must log in to do this
				Response.Redirect (this.GetLoginURL ());
			}

			// check to see if we are posting back trying to upload files
			if (Request.Form ["hidFileCount"] != null && Request.Form ["hidFileCount"].ToString () != string.Empty)
			{
				ProcessUploadedFiles (Convert.ToInt32 (Request.Form ["hidFileCount"]) );
			}
			
			//setup header nav bar
			if(_channelId == this.GetPersonalChannelId())
			{
				//user's own channel
                ((GenericPageTemplate) Master).HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.MEDIA_UPLOADS;
			}
			else
			{
				//navs go to home-my channels for all channels if the user is an admin
                ((GenericPageTemplate) Master).HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.CHANNELS;
			}

			// Set Nav
            ((GenericPageTemplate) Master).HeaderNav.SetNavVisible (((GenericPageTemplate) Master).HeaderNav.MyKanevaNav, 2);

            if (!KanevaWebGlobals.CurrentUser.IsAdult)
            {
                tblAccessPass.Style.Add ("display", "none");
                tblAccessPassYouTube.Style.Add ("display", "none");
                tblAccessPassFlash.Style.Add ("display", "none");
                tblAccessPassGames.Style.Add ("display", "none");
            }

			if (!IsPostBack)
			{
                ((GenericPageTemplate) Master).HeaderNav.MyMediaUploadsNav.CommunityId = _channelId;
                ((GenericPageTemplate) Master).HeaderNav.MyMediaUploadsNav.UserId = _userId;

				revTags.ValidationExpression = Constants.VALIDATION_REGEX_TAG;
				REVtbTagsYouTube.ValidationExpression = Constants.VALIDATION_REGEX_TAG;
				revtbxFlashTags.ValidationExpression = Constants.VALIDATION_REGEX_TAG;
                revGameTags.ValidationExpression = Constants.VALIDATION_REGEX_TAG;

				// Set up new upload form
                file_0.Attributes.Add ("onchange", "SetFields(this);");
				file_0.Attributes.Add ("onkeydown", "javascript:this.form.file_0.blur();"); 
				
				// Initialize the Add File button
                btnAdd.Attributes.Add ("onclick", ClientScript.GetPostBackEventReference (btnAddFileToQueue, ""));
                btnAdd.Disabled = true;


                litScript.Text = "function SetFields(t) { " +
                    " SetFileName(t); " +
                    ClientScript.GetPostBackEventReference (btnFileBrowse, "") +
					"}";
			
				// Set values of access radio buttons	
				radPublic.Value = ((int) Constants.eASSET_PERMISSION.PUBLIC).ToString ();
				radPrivate.Value = ((int) Constants.eASSET_PERMISSION.PRIVATE).ToString ();

                // set active tab if set in querystring
                if (Request["type"] != null && Request["type"] != string.Empty)
                {
                    try
                    {
                        SetActiveTab (Convert.ToInt32 (Request["type"].ToString ()));
                    }
                    catch { }
                }

				//check request and configure the page accordingly
				ConfigureUploadPage(_uploadTabId);

                // Initialize metrics params.
                FromGameplayMode = FromGameplayMode;
                FromObjPlacementId = FromObjPlacementId;
                FromZoneInstanceId = FromZoneInstanceId;
                FromZoneType = FromZoneType;
            }
        }


        #region Image Processing Methods

        /// <summary>
        /// ProcessGameEmbed
        /// </summary>
        /// <param name="assetID"></param>
        /// <returns>bool</returns>
        private bool ProcessGameEmbed (string assetOffsiteId, ref int assetId)
        {
            //overall try catch to keep page processing going no matter the type of failure
            //error written to site log
            //int assetId = -1;
            try
            {
                string title = txtGameTitle.Text;
                string desc = txtGameDescription.Text;
                string tags = txtGameTags.Value;
                int permission = radGamePublic.Checked ? (int) Constants.eASSET_PERMISSION.PUBLIC : (int) Constants.eASSET_PERMISSION.PRIVATE;  //public or permission
                bool isRestricted = chkRestrictedGame.Checked;  //mature
                int catagoryId = Convert.ToInt32 (this.ddlCategoriesGame.SelectedValue);

                /* Code taken from assetEdit page  */
                int isMature = (int) Constants.eASSET_RATING.GENERAL;
                if (isRestricted)
                {
                    isMature = (int) Constants.eASSET_RATING.MATURE;
                }
                
                //assumption: all you tube materials are videos
                // Create a new asset record
                assetId = StoreUtility.InsertAsset((int)Constants.eASSET_TYPE.GAME, (int)Constants.eASSET_SUBTYPE.ALL,
                    Server.HtmlEncode(title), KanevaWebGlobals.CurrentUser.UserId, KanevaWebGlobals.CurrentUser.Username,
                    (int)Constants.ePUBLISH_STATUS.PUBLISH_COMPLETE, permission,
                    isMature, catagoryId, Server.HtmlEncode(desc), Server.HtmlEncode(txtGameInstructions.Text.Trim()),
                    Server.HtmlEncode(txtCompanyName.Text.Trim()), assetOffsiteId);

                // verify new asset was added by checking new assetId

                if (assetId > 0)
                {
                    // Insert into user's personal channel
                    int personalChannelId = KanevaWebGlobals.CurrentUser.CommunityId;
                    StoreUtility.InsertAssetChannel (assetId, personalChannelId);

                    if (_channelId != personalChannelId)
                    {
                        //then insert into the broadcast channel
                        StoreUtility.InsertAssetChannel (assetId, _channelId);
                    }

                    // Update the tags
                    if (tags.Length > 0)
                    {
                        StoreUtility.UpdateAssetTags (assetId, Server.HtmlEncode (tags));
                    }
                }
                else
                {
                    // will return with assetId = 0.  Calling method will handle and display correct err msg.
                    return false;
                }
            }
            catch (Exception ex)
            {
                m_logger.Error ("Error adding new offsite asset", ex);
                return false;
            }

            //clear or reset all fields
            txtGameTitle.Text = "";
            txtGameDescription.Text = "";
            txtGameTags.Value = "";
            radGamePublic.Checked = true;
            chkRestrictedGame.Checked = false;
            txtGameLink.Text = "";
            this.cbAgreementGame.Checked = false;
            btnAddGame.Enabled = false;
            btnAddGame.ImageUrl = ResolveUrl ("~/images/button_addvideo_dis.gif");

            return true;
        }

        /// <summary>
		/// ProcessWidgetEmbed
		/// </summary>
		/// <param name="assetID"></param>
		/// <returns>bool</returns>
		private bool ProcessWidgetEmbed (string ytAssetID)
		{
			//overall try catch to keep page processing going no matter the type of failure
			//error written to site log
			int assetId = -1;
			try
			{
				string title	 = tbxFLashTitle.Text;  
				string desc		 = tbxFlashDescription.Text;
				string tags		 = tbxFlashTags.Value;
                int permission = rdoFlashPublic.Checked ? (int)Constants.eASSET_PERMISSION.PUBLIC : (int)Constants.eASSET_PERMISSION.PRIVATE;  //public or permission
				bool isRestricted = chkRestrictedWidget.Checked;  //mature
				int catagoryId = Convert.ToInt32(this.ddlCategoriesFL.SelectedValue);

				/* Code taken from assetEdit page  */			 
				int isMature = (int) Constants.eASSET_RATING.GENERAL;
				if (isRestricted)
				{
					isMature = (int) Constants.eASSET_RATING.MATURE;
				}

				//assumption: all you tube materials are videos
                assetId = StoreUtility.InsertAsset ((int) Constants.eASSET_TYPE.WIDGET, (int) Constants.eASSET_SUBTYPE.ALL, Server.HtmlEncode (title),
                    _userId, UsersUtility.GetUserNameFromId (_userId),
                    (int) Constants.ePUBLISH_STATUS.PUBLISH_COMPLETE, permission, isMature,
                    catagoryId, Server.HtmlEncode (desc), "", "",ytAssetID); 
			
				// Insert into user's personal channel
                int personalChannelId = GetUserFacade.GetPersonalChannelId(_userId);
				StoreUtility.InsertAssetChannel (assetId,personalChannelId);

				if(_channelId != personalChannelId)
				{
					//then insert into the broadcast channel
					StoreUtility.InsertAssetChannel (assetId, _channelId);
				}

				// Update the tags
				if (tags.Length > 0)
				{
					StoreUtility.UpdateAssetTags (assetId, Server.HtmlEncode (tags));
				}

			}
			catch(Exception ex)
			{
				m_logger.Error ("Error adding new offsite asset", ex);
				return false;
			}

            //generate the thumbnail
            if (SetGenericThumbnail(assetId, "GenericWidgetThumb.jpg") > 0)
            {
                ScriptManager.RegisterStartupScript (this, GetType (), "erralert1", "alert('An error occurred during thumbnail creation. \n The link was saved to your media library.');", true);
            }

			//clear or reset all fields
			tbxFLashTitle.Text = "";  
			tbxFlashDescription.Text = "";
			tbxFlashTags.Value = "";
			rdoFlashPublic.Checked = true;
			chkRestrictedWidget.Checked = false;
			tbxFLashLink.Text = "";
			this.cbAgreementWidget.Checked = false;
			btnWidgetEmbed.Disabled = true;
			btnWidgetEmbed.Src = ResolveUrl("~/images/button_addvideo_dis.gif");

			return true;
		}



		/// <summary>
		/// ProcessYouTubeEmbed
		/// </summary>
		/// <param name="assetID"></param>
		/// <returns>bool</returns>
		private bool ProcessYouTubeEmbed (string ytAssetID)
		{
			//overall try catch to keep page processing going no matter the type of failure
			//error written to site log
			int assetId = -1;
			try
			{
				string title	 = tbYouTubeTitle.Text;  
				string desc		 = tbBriefDescYouTube.Text;
				string tags		 = tbTagsYouTube.Value;
                int permission = Radio1.Checked ? (int)Constants.eASSET_PERMISSION.PUBLIC : (int)Constants.eASSET_PERMISSION.PRIVATE;  //public or permission
				bool isRestricted = chkRestrictedYoutube.Checked;  //mature
                int catagoryId = Convert.ToInt32(this.ddlCategoriesYT.SelectedValue);

				/* Code taken from assetEdit page  */			 
				int isMature = (int) Constants.eASSET_RATING.GENERAL;
				if (isRestricted)
				{
					isMature = (int) Constants.eASSET_RATING.MATURE;
				}

				//assumption: all you tube materials are videos
				// Create a new asset record
                assetId = StoreUtility.InsertAsset((int)Constants.eASSET_TYPE.VIDEO, (int)Constants.eASSET_SUBTYPE.YOUTUBE, Server.HtmlEncode(title),
                    _userId, UsersUtility.GetUserNameFromId(_userId),
                    (int)Constants.ePUBLISH_STATUS.PUBLISH_COMPLETE, permission, isMature,
                    catagoryId, Server.HtmlEncode(desc), "", "", ytAssetID); 
			
				// Insert into user's personal channel
                int personalChannelId = GetUserFacade.GetPersonalChannelId(_userId);
				StoreUtility.InsertAssetChannel (assetId,personalChannelId);

				if(_channelId != personalChannelId)
				{
					//then insert into the broadcast channel
					StoreUtility.InsertAssetChannel (assetId, _channelId);
				}

				// Update the tags
				if (tags.Length > 0)
				{
					StoreUtility.UpdateAssetTags (assetId, Server.HtmlEncode (tags));
				}

			}
			catch(Exception ex)
			{
				m_logger.Error ("Error adding new offsite asset", ex);
				return false;
			}
	
			//scrape the thumbnails and other video data
            int iGetInfoResult = ExternalURLHelper.GetYouTubeInfo (ytAssetID, _userId, assetId, m_logger);

            if (iGetInfoResult > 0)
			{
                ScriptManager.RegisterStartupScript (this, GetType (), "erralert3", "alert('An error occurred during video metadata grab. \n The link was saved to your media library.');", true);
            }

            // Handle Youtube rejected or video not found
            if (iGetInfoResult < 0)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "erralert3", "alert('This is an invalid video. \n The link was not saved to your media library.');", true);
                StoreUtility.DeleteAsset(assetId, _userId);
                return false;
            }
            else
            { 
                // User completed fame packet
                try
                {
                    GetFameFacade.RedeemPacket (KanevaWebGlobals.GetUserId (), (int) PacketId.WEB_OT_UPLOAD_YOUTUBE, (int) FameTypes.World);
                }
                catch (Exception ex)
                {
                    m_logger.Error ("Error awarding Packet WEB_OT_UPLOAD_YOUTUBE, userid=" + KanevaWebGlobals.GetUserId () + ", assetId=" + assetId, ex);
                }
            }
            
			//clear or reset all fields
			tbYouTubeTitle.Text = "";  
			tbBriefDescYouTube.Text = "";
			tbTagsYouTube.Value = "";
			Radio1.Checked = true;
			chkRestrictedYoutube.Checked = false;
			tbYouTubeLink.Text = "";
			this.cbAgreementYouTube.Checked = false;
			btnYouTubeEmbed.Disabled = true;
			btnYouTubeEmbed.Src = ResolveUrl("~/images/button_addvideo_dis.gif");

			return true;
		}

		/// <summary>
		/// ProcessUploadedFiles
		/// </summary>
		/// <param name="file_count"></param>
		private void ProcessUploadedFiles (int file_count)
		{
			string errorMessage = "";
            int assetId = 0;

			// Look for any other torrents
			for (int i=1; i <= file_count; i++)
			{
				HttpPostedFile inpFile = null;
				inpFile = Request.Files[file_count-i];
				string title	 = Request.Form ["hidTitle"+i].ToString ();  
				string desc		 = Request.Form ["hidDescription"+i].ToString ();
				string tags		 = Request.Form ["hidTags"+i].ToString ();
				int categoryId   = Convert.ToInt32 (Request.Form ["hidCategory"+i]);
				int permission	 = Convert.ToInt32 (Request.Form ["hidAccess"+i]);
				bool isRestricted = Request.Form ["hidRestricted"+i].ToString () == "0" ? false : true;
				bool isPattern = Request.Form ["hidPattern"+i].ToString () == "0" ? false : true;

				if (inpFile != null)
				{							
                    errorMessage += AddFile (inpFile, title, tags, desc, categoryId, permission, isRestricted, isPattern, ref assetId);
				}
			}

			if (assetId > 0)
            {
				Response.Redirect (ResolveUrl (ViewState ["lastURL"].ToString ()));
			}
			else
			{
				ShowErrorOnStartup (errorMessage);
			}
		}

		/// <summary>
		/// AddFile
		/// </summary>
		private string AddFile (HttpPostedFile File, string title, string tags, string description, 
			int categoryId, int permission, bool isRestricted, bool isPattern, ref int assetId)
		{
            return AddFile (File, title, tags, description, "", "", categoryId, permission, isRestricted, isPattern, ref assetId);
        }
        
        private string AddFile (HttpPostedFile File, string title, string tags, string description, string instructions, 
		    string companyName, int categoryId, int permission, bool isRestricted, bool isPattern, ref int assetId)
        {
			string filename = System.IO.Path.GetFileName (File.FileName);
			filename = StoreUtility.CleanImageFilename (filename);

			int assetTypeId = (int) Constants.eASSET_TYPE.VIDEO;

			// Make sure it is a valid file type
			string strExtensionUpperCase = System.IO.Path.GetExtension (File.FileName).ToUpper ();

			// Make sure the extensions are correct
			if (StoreUtility.IsFileVideo (strExtensionUpperCase))
			{
				assetTypeId = (int) Constants.eASSET_TYPE.VIDEO;
			}
			else if (StoreUtility.IsFilePicture (strExtensionUpperCase))
			{
				if(isPattern)
				{
					assetTypeId = (int) Constants.eASSET_TYPE.PATTERN;
				}
				else
				{
					assetTypeId = (int) Constants.eASSET_TYPE.PICTURE;
				}
			}
			else if (StoreUtility.IsFileGame (strExtensionUpperCase))
			{
				assetTypeId = (int) Constants.eASSET_TYPE.GAME;
			}
			else if (StoreUtility.IsFileMusic (strExtensionUpperCase))
			{
				assetTypeId = (int) Constants.eASSET_TYPE.MUSIC;
			}
			else
			{
				return ("File \\'" + filename + "\\' is not a supported file type. Please provide media in a Kaneva-supported format.\\n");
			}

			// Get the upload repository
			string uploadRepository = "";
			try 
			{
                uploadRepository = Configuration.UploadRepository;
			}
			catch (Exception e)
			{
				m_logger.Error ("Error reading config file", e);
				return "UploadRepository is not defined in web.config";
			}

			// Add the file
			try
			{
                // Shorten file name length if needed, really long filenames are
                // causing an issue on the backend so we're going to max them at 30 chars
                // filename variable includes extension, thus why checking against length of 34
                if (filename.Length > 34)
                {
                    filename = StoreUtility.CleanImageFilename(System.IO.Path.GetFileNameWithoutExtension (File.FileName)).Substring (0, 30) +
                        strExtensionUpperCase.ToLower();
                }

				string newPath = CreateNewRecord (ref assetId, _userId, title, filename, assetTypeId, tags, File.ContentLength, 
					uploadRepository, categoryId, description, permission, isRestricted, instructions, companyName);

                if (Configuration.UseAmazonS3Storage)
                {
                    try
                    {
                        string azS3Bucket = Configuration.AmazonS3Bucket;

                        AmazonS3Client azS3client = new AmazonS3Client (Configuration.AmazonS3AccessKeyId,
                                Configuration.AmazonS3SecretAccessKey,
                                Amazon.RegionEndpoint.USEast1);

                        TransferUtility fileTransferUtility = new TransferUtility(azS3client);

                        // Upload data from a type of System.IO.Stream.
                        fileTransferUtility.Upload(File.InputStream, azS3Bucket, newPath.Replace("\\", "/") + "/" + filename);
                    }
                    catch (AmazonS3Exception s3Exception)
                    {
                        m_logger.Error("S3 Error " + s3Exception.Message, s3Exception);
                    }


                }
                else
                {
                    // Make sure the directory exists
                    FileInfo fileInfo = new FileInfo(newPath + Path.DirectorySeparatorChar);
                    if (!fileInfo.Directory.Exists)
                    {
                        fileInfo.Directory.Create();
                    }

                    int maximumBufferSize = 4096;
                    byte[] transferBuffer;

                    if (File.ContentLength > maximumBufferSize)
                    {
                        transferBuffer = new byte[maximumBufferSize];
                    }
                    else
                    {
                        transferBuffer = new byte[File.ContentLength];
                    }

                    System.IO.FileStream fsNewFile = new System.IO.FileStream(newPath + Path.DirectorySeparatorChar + filename, System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.Write);

                    int bytesRead;
                    do
                    {
                        bytesRead = File.InputStream.Read(transferBuffer, 0, transferBuffer.Length);
                        fsNewFile.Write(transferBuffer, 0, bytesRead);
                    }
                    while (bytesRead > 0);

                    fsNewFile.Close();
                }

                // For pictures or patterns, generate the thumbs
                if (assetTypeId.Equals((int)Constants.eASSET_TYPE.PICTURE) || assetTypeId.Equals((int)Constants.eASSET_TYPE.PATTERN))
                {
                    CreatePhotoThumbs(assetId, _userId, newPath + Path.DirectorySeparatorChar + filename, filename);
                }


				
			}
			catch (UnauthorizedAccessException exc)
			{
				m_logger.Error ("Possible error with LibTorrentProxy.exe DCOM settings, allow user to launch", exc);
				return ("There was a fatal error posting the File \\'" + filename + "\\'. Please try again later.\\n");
			}
			catch (Exception exc)
			{
				m_logger.Error ("Error reading file", exc);
				return ("There was an error posting the File \\'" + filename + "\\'. Please try again.\\n");
			}

			return "";
		}

		/// <summary>
		/// Create new asset_upload and asset record
		/// </summary>
		/// <param name="user_id"></param>
		private string CreateNewRecord (ref int assetId, int user_id, string itemName, string fileName, int assetTypeId, 
			string tags, long size, string uploadRepository, int categoryId, string description, int permission, bool isRestricted,
            string instructions, string companyName)
		{
			//create a new asset
			string newItemName = itemName;
			string newPath = "";

			/* Code taken from assetEdit page  */			 
			int isMature = (int) Constants.eASSET_RATING.GENERAL;
			if (isRestricted)
			{
				isMature = (int) Constants.eASSET_RATING.MATURE;
			}

			// Categories
			int category1Id = categoryId;

			// Create a new asset record
			assetId = StoreUtility.InsertAsset (assetTypeId, 0, Server.HtmlEncode (newItemName), user_id, UsersUtility.GetUserNameFromId (user_id), 
				(int) Constants.ePUBLISH_STATUS.UPLOADED, permission, isMature, category1Id, Server.HtmlEncode (description),
                Server.HtmlEncode (instructions), Server.HtmlEncode (companyName), "");
			
			if (assetId > 0)
			{
				// Non pictures must be processed, pictures don't
				if (assetTypeId.Equals ((int) Constants.eASSET_TYPE.PICTURE) || assetTypeId.Equals ((int) Constants.eASSET_TYPE.PATTERN))
				{
					newPath = Path.Combine (Path.Combine (KanevaGlobals.ContentServerPath, user_id.ToString ()), assetId.ToString ());
					StoreUtility.UpdateAssetFilePath (assetId, fileName, newPath, size);
					StoreUtility.UpdateAssetStatus (assetId, (int) Constants.eASSET_STATUS.ACTIVE);
					StoreUtility.UpdateAssetPublishStatus (assetId, (int) Constants.ePUBLISH_STATUS.PUBLISH_COMPLETE, 0);
				}
				else
				{
					// Add non-pictures to the asset  upload table to be processed
					int assetUploadId = StoreUtility.InsertAssetUpload(user_id, fileName, uploadRepository, size, "", (int) Constants.eDS_INVENTORYTYPE.ASSETS, assetId);
					newPath = Path.Combine (Path.Combine (uploadRepository, user_id.ToString()), assetId.ToString());
					StoreUtility.UpdateAssetUploadPath (assetUploadId, newPath);
					StoreUtility.UpdateAssetUploadStatus (assetId, (int) Constants.ePUBLISH_STATUS.UPLOADED, Common.GetVisitorIPAddress());
                    
                    // Fix the media path
                    string mediaPath = "";
                    if (assetTypeId.Equals ((int) Constants.eASSET_TYPE.MUSIC) || assetTypeId.Equals ((int) Constants.eASSET_TYPE.GAME))
                    {
                        // Music
                        mediaPath = (Configuration.FileStoreHack + Path.Combine(user_id.ToString(), assetId.ToString()) + "/" + fileName);
                        mediaPath = mediaPath.Replace("\\", "/");
                        StoreUtility.UpdateAssetMediaPath(assetId, mediaPath);
                    }
                    else if (assetTypeId.Equals((int)Constants.eASSET_TYPE.VIDEO))
                    {
                        // Video
                        mediaPath = Configuration.FileStoreHack + Path.Combine(user_id.ToString(), assetId.ToString()) + "/" + System.IO.Path.GetFileNameWithoutExtension (fileName) + ".FLV";
                        mediaPath = mediaPath.Replace("\\", "/");
                        StoreUtility.UpdateAssetMediaPath(assetId, mediaPath);
                    }
				}

				// Insert into user's personal channel
				int personalChannelId =  GetPersonalChannelId();
				StoreUtility.InsertAssetChannel (assetId,personalChannelId);

				if(_channelId != personalChannelId)
				{
					//then insert into the broadcast channel
					StoreUtility.InsertAssetChannel (assetId, _channelId);
				}

				// Update the tags
				if (tags.Length > 0)
				{
					StoreUtility.UpdateAssetTags (assetId, Server.HtmlEncode (tags));
				}
			}
			else
			{
				m_logger.Error ("Failed to insert asset record user " + user_id + " filename = " + fileName);
			}

			return newPath;
        }

        #endregion


        #region Helper Methods

        /// <summary>
        /// CreateS3PhotoThumbs
		/// </summary>
        private void CreateS3PhotoThumbs(int assetId, int userId, string imagePath, string filename)
		{
			System.Drawing.Image imgOriginal = null;

			string contentRepository = "";
			if (KanevaGlobals.ContentServerPath != null)
			{
				contentRepository = KanevaGlobals.ContentServerPath;
			}
			else
			{
				m_logger.Error ("ContentServerPath not found generating Photo Thumbs");
				return;
			}

			string fileStoreHack = "";
			if (KanevaGlobals.FileStoreHack != "")
			{
				fileStoreHack = KanevaGlobals.FileStoreHack;
			}

			try
			{
                

				
				// Save the image at the specified sizes
				// Save the original path
				StoreUtility.UpdateAssetThumb (assetId, fileStoreHack + userId.ToString () + "/" + assetId.ToString() + "/" + filename, "image_full_path");

				// Small
                ImageHelper.SaveAssetThumbnail(imgOriginal, (int)Constants.PHOTO_THUMB_SMALL_HEIGHT, (int)Constants.PHOTO_THUMB_SMALL_WIDTH, "_sm", "thumbnail_small_path",
					filename, imagePath, contentRepository, userId, assetId);

				// Medium
				ImageHelper.SaveAssetThumbnail (imgOriginal, (int) Constants.PHOTO_THUMB_MEDIUM_HEIGHT, (int) Constants.PHOTO_THUMB_MEDIUM_WIDTH, "_me", "thumbnail_medium_path",
					filename, imagePath, contentRepository, userId, assetId);

				// Large
				ImageHelper.SaveAssetThumbnail (imgOriginal, (int) Constants.PHOTO_THUMB_LARGE_HEIGHT, (int) Constants.PHOTO_THUMB_LARGE_WIDTH, "_la", "thumbnail_large_path",
					filename, imagePath, contentRepository, userId, assetId);

				// XLarge
				ImageHelper.SaveAssetThumbnail (imgOriginal, (int) Constants.PHOTO_THUMB_XLARGE_HEIGHT, (int) Constants.PHOTO_THUMB_XLARGE_WIDTH, "_xl", "thumbnail_xlarge_path",
					filename, imagePath, contentRepository, userId, assetId);

				// Asset Details
				ImageHelper.SaveAssetThumbnail (imgOriginal, (int) Constants.PHOTO_THUMB_ASSETDETAILS_HEIGHT, (int) Constants.PHOTO_THUMB_ASSETDETAILS_WIDTH, "_ad", "thumbnail_assetdetails_path",
					filename, imagePath, contentRepository, userId, assetId);
			}
			catch (Exception exc)
			{
				m_logger.Error ("Error reading image path is " + imagePath, exc);
				
			}
			finally 
			{
				if (imgOriginal != null)
				{
					imgOriginal.Dispose();
				}
			}
		}

        /// <summary>
		/// CreatePhotoThumbs
		/// </summary>
		private void CreatePhotoThumbs (int assetId, int userId, string imagePath, string filename)
		{
			System.Drawing.Image imgOriginal = null;

			string contentRepository = "";
			if (KanevaGlobals.ContentServerPath != null)
			{
				contentRepository = KanevaGlobals.ContentServerPath;
			}
			else
			{
				m_logger.Error ("ContentServerPath not found generating Photo Thumbs");
				return;
			}

			string fileStoreHack = "";
			if (KanevaGlobals.FileStoreHack != "")
			{
				fileStoreHack = KanevaGlobals.FileStoreHack;
			}

			try
			{
                if (Configuration.UseAmazonS3Storage)
                {
                    string originalImage = (Configuration.ImageServer + "/" + imagePath).Replace ("\\","/");

                    HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(originalImage);
                    HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();

                    System.IO.Stream receiveStream = httpWebResponse.GetResponseStream();
                    imgOriginal = System.Drawing.Image.FromStream(receiveStream);
                }
                else
                {
				    //Create an image object from a file on disk
				    imgOriginal = System.Drawing.Image.FromFile (imagePath);
                }
                
				// Save the image at the specified sizes
				// Save the original path
				StoreUtility.UpdateAssetThumb (assetId, fileStoreHack + userId.ToString () + "/" + assetId.ToString() + "/" + filename, "image_full_path");
								
				// Small
				ImageHelper.SaveAssetThumbnail (imgOriginal, (int) Constants.PHOTO_THUMB_SMALL_HEIGHT, (int) Constants.PHOTO_THUMB_SMALL_WIDTH, "_sm", "thumbnail_small_path",
					filename, imagePath, contentRepository, userId, assetId);

				// Medium
				ImageHelper.SaveAssetThumbnail (imgOriginal, (int) Constants.PHOTO_THUMB_MEDIUM_HEIGHT, (int) Constants.PHOTO_THUMB_MEDIUM_WIDTH, "_me", "thumbnail_medium_path",
					filename, imagePath, contentRepository, userId, assetId);

				// Large
				ImageHelper.SaveAssetThumbnail (imgOriginal, (int) Constants.PHOTO_THUMB_LARGE_HEIGHT, (int) Constants.PHOTO_THUMB_LARGE_WIDTH, "_la", "thumbnail_large_path",
					filename, imagePath, contentRepository, userId, assetId);

				// XLarge
				ImageHelper.SaveAssetThumbnail (imgOriginal, (int) Constants.PHOTO_THUMB_XLARGE_HEIGHT, (int) Constants.PHOTO_THUMB_XLARGE_WIDTH, "_xl", "thumbnail_xlarge_path",
					filename, imagePath, contentRepository, userId, assetId);

				// Asset Details
				ImageHelper.SaveAssetThumbnail (imgOriginal, (int) Constants.PHOTO_THUMB_ASSETDETAILS_HEIGHT, (int) Constants.PHOTO_THUMB_ASSETDETAILS_WIDTH, "_ad", "thumbnail_assetdetails_path",
					filename, imagePath, contentRepository, userId, assetId);
			}
			catch (Exception exc)
			{
				m_logger.Error ("Error reading image path is " + imagePath, exc);
				
			}
			finally 
			{
				if (imgOriginal != null)
				{
					imgOriginal.Dispose();
				}
			}
		}
		
		/// <summary>
		/// Populate the categories
		/// </summary>
		/// <param name="assetTypeId"></param>
		private void PopulateCategory (int assetTypeId)
		{
			DropDownList visibleDDL; 

			switch(assetTypeId)
			{
                case (int)Constants.eASSET_TYPE.WIDGET:
					visibleDDL = ddlCategoriesFL;
					break;
                case (int)Constants.eASSET_TYPE.VIDEO:
                    if(_uploadTabId == Constants.UPLOAD_CONFIG.YOUTUBE)
                    {
					    visibleDDL = ddlCategoriesYT;
                    }
                    else
                    {
                        visibleDDL = ddlCategories;
                    }
					break;
                case (int)Constants.eASSET_TYPE.GAME:
                    visibleDDL = ddlCategoriesGame;
                    break;
                case (int)Constants.eASSET_TYPE.PATTERN:
                case (int)Constants.eASSET_TYPE.PICTURE:
                case (int)Constants.eASSET_TYPE.MUSIC:
                default:
					visibleDDL = ddlCategories;
					break;
			}

			//populates the upload catagory pulldown
			visibleDDL.DataSource = WebCache.GetAssetCategories (assetTypeId);
			visibleDDL.DataBind ();

            // we do this for the req. field validater on games uploads
            string defaultValue = "0";
            if (assetTypeId == (int) Constants.eASSET_TYPE.GAME)
            {
                defaultValue = "";
            }
            visibleDDL.Items.Insert (0, new ListItem ("Select...", defaultValue));
			SetDropDownIndex (visibleDDL, "");
		}

		//added until decision is made on wording and return logic
		private void SetBackButton(string url)
		{
			SetBackButton(url, "<< Back");
		}

		private void SetBackButton(string url, string name)
		{
			if ((name == null) || (name == ""))
			{
				name = "<< Back";
			}
			back.Text = name;
			back.NavigateUrl = url;
			back.Visible = true;
		}

		private bool GetRequestParams()
		{
			bool retVal = true;
			try
			{
				_userId = this.GetUserId ();

				//retreive selected tabid from url to determine which upload configuration to default to 
				if(Request ["uploadSelect"] != null)
				{
					_uploadTabId = Convert.ToInt32 (Request ["uploadSelect"]);
				}
				else
				{
					_uploadTabId = -1;
				}

				//retreive module if from url to determine if it comes from upload widget and from 
				//which one
				if(Request ["moduleId"] != null)
				{
					_moduleId = Convert.ToInt32 (Request ["moduleId"]);
				}
				else
				{
					_moduleId = -1;
				}

				if(Request ["communityId"] != null)
				{
					//get channelId and find the default page
					_channelId = Convert.ToInt32 (Request ["communityId"]);
                    /* new pages no longer use pageIds - hidden to prevent error*/
					//_pageId = Convert.ToInt32 (Request ["pageId"]);
					retVal = CommunityUtility.IsCommunityValid (_channelId);
				}
				else
				{
					_channelId = GetPersonalChannelId ();
				}

				//if not already there add to view state
				if(!Page.IsPostBack && (ViewState["lastURL"] == null))
				{
					if(_moduleId != -1)
					{
                        if (Request["communityId"] != null)
                        {
                            ViewState["lastURL"] = "~/community/CommunityPage.aspx?communityId=" + _channelId;
                        }
                        else
                        {
                            ViewState["lastURL"] = "~/community/ProfilePage.aspx?communityId=" + _channelId;
                        }
					}
					else
					{
						ViewState["lastURL"] = "~/asset/publishedItemsNew.aspx?itsact3=y&communityId=" + _channelId;
					}
				}

				SetBackButton (ViewState["lastURL"].ToString());
			}
			catch(Exception exc)
			{
				m_logger.Error ("Error getting params in upload page", exc);
				retVal = false;
			}
			return retVal;
		}

		/// <summary>
		/// returns true if current user is allowed to edit this channel
		/// </summary>
		/// <returns></returns>
		private bool IsUserAllowedToEdit()
		{
			//1. current user has to be the user passed in or be an admin
			//2. current user must have right to upload to target channel
			return (IsAdministrator() || _userId == this.GetUserId()) &&
				(CommunityUtility.IsCommunityModerator(_channelId, _userId));
		}

		/// <summary>
		/// Configure tag fields
		/// </summary>
		private void ConfigureTagFields(int module_id)
		{
			try
			{
				DataTable dtUserUpload = WidgetUtility.GetUserUploadDescriptions(module_id);
				DataRow drUserUpload = WidgetUtility.GetLayoutModuleUserUpload(module_id);

				if (dtUserUpload.Rows.Count > 0)
				{
					//make table visible
					this.UploadTags.Visible = true;

					//display instructions
					this.ltl_Instructions.Text = Convert.ToString(drUserUpload["tag_instructions"]);

					//set channel owner hidden tags 
					this.tbx_ChnlOwnr_Tags.Value = Convert.ToString(drUserUpload["channel_owner_tags"]);

					//load the description tags if any
					int currentField = 0;
					foreach(DataRow row in dtUserUpload.Rows)
					{
						string description = row["field_description"].ToString().Trim();
						if((description != null) && (description.Length > 0))
						{
							switch(currentField)
							{
								case 0:
									tagRow1.Visible = true;
									lbl_Description_1.Text = description + ":";
									lblASTReq1.Visible = rfv_tagField1.Visible = Convert.ToBoolean(row["required"]); 
									break;
								case 1:
									tagRow2.Visible = true;
									lbl_Description_2.Text = description + ":";
									lblASTReq2.Visible = rfv_tagField2.Visible = Convert.ToBoolean(row["required"]);
									break;
								case 2:
									tagRow3.Visible = true;
									lbl_Description_3.Text = description + ":";
									lblASTReq3.Visible = rfv_tagField3.Visible = Convert.ToBoolean(row["required"]);
									break;
								case 3:
									tagRow4.Visible = true;
									lbl_Description_4.Text = description + ":";
									lblASTReq4.Visible = rfv_tagField4.Visible = Convert.ToBoolean(row["required"]);
									break;
								case 4:
									tagRow5.Visible = true;
									lbl_Description_5.Text = description + ":";
									lblASTReq5.Visible = rfv_tagField5.Visible = Convert.ToBoolean(row["required"]);
									break;
								case 5:
									tagRow6.Visible = true;
									lbl_Description_6.Text = description + ":";
									lblASTReq6.Visible = rfv_tagField6.Visible = Convert.ToBoolean(row["required"]);
									break;
							}
							currentField++;
						}
					}
				}
				else
				{
					this.UploadTags.Visible = false;
				}
			}
			catch(Exception )
			{
				//string error = "Error: Unable to load the configuration";
			}

		}

		/// <summary>
		/// resets the fields for next file up load
		/// </summary>
		private void ResetUserTagFields()
		{
			if(tagRow1.Visible )
			{
				tbx_TagField1.Text = "";
			}
			if(tagRow2.Visible)
			{
				tbx_TagField2.Text = "";
			}
			if(tagRow3.Visible)
			{
				tbx_TagField3.Text = "";
			}
			if(tagRow4.Visible)
			{
				tbx_TagField4.Text = "";
			}
			if(tagRow5.Visible)
			{
				tbx_TagField5.Text = "";
			}
			if(tagRow6.Visible)
			{
				tbx_TagField6.Text = "";
			}
			
		}

		/// <summary>
		/// validate tag fields
		/// </summary>
		private bool ValidateTagFields()
		{
			bool valid = true;

			if(	rfv_tagField1.Visible)
			{
				rfv_tagField1.Validate();
				if(!rfv_tagField1.IsValid)
				{
					valid = false;
				}
			}

			if(	rfv_tagField2.Visible)
			{
				rfv_tagField2.Validate();
				if(!rfv_tagField2.IsValid)
				{
					valid = false;
				}
			}

			if(	rfv_tagField3.Visible)
			{
				rfv_tagField3.Validate();
				if(!rfv_tagField3.IsValid)
				{
					valid = false;
				}
			}

			if(	rfv_tagField4.Visible )
			{
				rfv_tagField4.Validate();
				if(!rfv_tagField4.IsValid)
				{
					valid = false;
				}
			}

			if(	rfv_tagField5.Visible)
			{
				rfv_tagField5.Validate();
				if(!rfv_tagField5.IsValid)
				{
					valid = false;
				}
			}

			if(	rfv_tagField6.Visible )
			{
				rfv_tagField6.Validate();
				if(!rfv_tagField6.IsValid)
				{
					valid = false;
				}
			}

			return valid;
		}

		private void ConfigureUploadPage(int setting)
		{
			//enable all tabs
			this.lbUpload.Enabled = true;
			this.lbYouTubeEmbed.Enabled = true;
			//this.lbTVEmbed.Enabled = true;
			this.lbFlashWidgets.Enabled = true;
            this.lbFlashGame.Enabled = true;

			//disable all configrations
            tbrYouTube.Style.Add ("display", "none");
            tbrUpload.Style.Add ("display", "none");
            tbrFlash.Style.Add ("display", "none");
            tbrFlashGame.Style.Add ("display", "none"); 

			liUpload.Attributes.Remove("class");
			liYouTube.Attributes.Remove("class");
			liFlash.Attributes.Remove("class");
            liFlashGame.Attributes.Remove ("class");

            divSwfUpload.Style.Add ("display", "none");

			switch(setting)
			{
				case Constants.UPLOAD_CONFIG.TV:
                    //tbrTV.Style.Add ("display", "block");
                    //liTV.Attributes.Add("class","selected");
                    //PopulateCategory((int)Constants.eASSET_TYPE.TV);
                    //tbTVLink.Text = "";
					break;
				case Constants.UPLOAD_CONFIG.FLASH:
                    tbrFlash.Style.Add ("display", "block");
					liFlash.Attributes.Add("class","selected");
                    PopulateCategory((int)Constants.eASSET_TYPE.WIDGET);
                    tbxFLashLink.Text = "";
					break;
				case Constants.UPLOAD_CONFIG.YOUTUBE:
                    tbrYouTube.Style.Add ("display", "block");
					liYouTube.Attributes.Add("class","selected");
                    PopulateCategory((int)Constants.eASSET_TYPE.VIDEO);
                    tbYouTubeLink.Text = "";
					break;
                case Constants.UPLOAD_CONFIG.FLASH_GAME:
                    tbrFlashGame.Style.Add ("display", "block");
                    liFlashGame.Attributes.Add ("class", "selected");
                    PopulateCategory ((int) Constants.eASSET_TYPE.GAME);                 
                    txtGameLink.Text = "";
                    // Set up new upload form
                    inpGameFile.Attributes.Add ("onkeydown", "javascript:$('" + inpGameFile.ClientID + "').blur();");
                    inpGameThumbnail.Attributes.Add ("onkeydown", "javascript:$('" + inpGameThumbnail.ClientID + "').blur();");
                    inpGameFile.Attributes.Add ("onchange", "javascript:$('" + hidGameFileName.ClientID + "').value = this.value;" +
                        ClientScript.GetPostBackEventReference (btnAddFileToQueue, ""));
                    break;
                case Constants.UPLOAD_CONFIG.UPLOAD:
				default:
                    tbrUpload.Style.Add ("display", "block");
                    rdoPattern.Style.Add ("display", "none");
                    rdoPhoto.Style.Add ("display", "none");
                    lblPattern.Visible = false;
					lblPhoto.Visible = false;
					rdoPhoto.Checked = true;
					rdoPattern.Checked = false;
                    PopulateCategory ((int) Constants.eASSET_TYPE.PICTURE);
					liUpload.Attributes.Add("class","selected");
					//check request to see if it is coming from upload widget or personal media library
					//configure accordingly 
					//set return page
					if (_moduleId != -1)
					{
						ConfigureTagFields (_moduleId);
					}
					break;
			}
		}

        private void UploadThumbnail (int assetId)
        {
            // They must be logged in
            if (!Request.IsAuthenticated)
            {
                Response.Redirect (GetLoginURL ());
                return;
            }

            // Files are now stored in content repository
            string contentRepository = "";
            if (KanevaGlobals.ContentServerPath != null)
            {
                contentRepository = KanevaGlobals.ContentServerPath;

                //declare the file name of the image
                string filename = null;

                //create PostedFile
                HttpPostedFile File = inpGameThumbnail.PostedFile;

                try
                {
                    //generate newpath
                    string newPath = Path.Combine (Path.Combine (contentRepository, KanevaWebGlobals.GetUserId ().ToString ()), assetId.ToString ());

                    //upload image from client to image repository
                    ImageHelper.UploadImageFromUser (ref filename, File, newPath, KanevaGlobals.MaxUploadedImageSize);

                    //generate content type and image path
                    string imagePath = File.FileName.Remove (0, File.FileName.LastIndexOf ("\\") + 1);
                    string origImagePath = newPath + Path.DirectorySeparatorChar + filename;
                    string contentType = File.ContentType;

                    StoreUtility.UpdateAssetImage (assetId, origImagePath, contentType);

                    //create any and all thumbnails needed
                    ImageHelper.GenerateAllAssetThumbnails (filename, origImagePath, imagePath, contentRepository, GetUserId (), assetId);
                }
                catch (Exception)
                {
                    // just catch err here.  don't want to stop upload just becuase thumbnail failed.
                }
            }
        }

        private void SetActiveTab (int assetTypeId)
        {
            switch (assetTypeId)
            {
                case (int) Constants.eASSET_TYPE.GAME:
                    _uploadTabId = Constants.UPLOAD_CONFIG.FLASH_GAME;
                    break;
                
                case (int) Constants.eASSET_TYPE.VIDEO:
                    _uploadTabId = Constants.UPLOAD_CONFIG.YOUTUBE;
                    break;
                
                case (int) Constants.eASSET_TYPE.WIDGET:
                    _uploadTabId = Constants.UPLOAD_CONFIG.FLASH;
                    break;
                
                case (int) Constants.eASSET_TYPE.MUSIC:
                case (int) Constants.eASSET_TYPE.PATTERN:
                case (int) Constants.eASSET_TYPE.PICTURE:
                default:
                    _uploadTabId = Constants.UPLOAD_CONFIG.UPLOAD;
                    break;
            }
        }
		#endregion


		#region Event Handlers

		/// <summary>
		/// toggles the screen to show or not show the appropriate configuration
		/// </summary>
		/// 
		protected void tabClicked_Command(object sender, System.Web.UI.WebControls.CommandEventArgs e)
		{
			//read tab command arguement and pass for configuration
			//any errors default to upload configuration
			try
			{
				_uploadTabId = Convert.ToInt32(e.CommandArgument);
			}
			catch(FormatException nfe)
			{
				m_logger.Error ("Invalid command arguement provided for tabs", nfe);
			}
			ConfigureUploadPage(_uploadTabId);
		}
	
		/// <summary>
		/// Execute when user clicks on the file browser
		/// </summary>
		protected void FileAdd_Click (object sender, System.EventArgs e)
		{
            // They must be logged in
            if (!Request.IsAuthenticated)
            {
                Response.Redirect (GetLoginURL ());
                return;
            }

            // Run validation
			rfvTitle.Validate();
			revTags.Validate();

            // Check to make sure user did not enter any "potty mouth" words
            if (KanevaWebGlobals.isTextRestricted (txtTitle.Text, Constants.eRESTRICTION_TYPE.POTTY_MOUTH) ||
                KanevaWebGlobals.isTextRestricted (txtTags.Value, Constants.eRESTRICTION_TYPE.POTTY_MOUTH) ||
                KanevaWebGlobals.isTextRestricted (txtDescription.Text, Constants.eRESTRICTION_TYPE.POTTY_MOUTH))
            {
                ScriptManager.RegisterStartupScript (this, GetType (), "addalert", "alert('" + PottyMouthText + "');", true);
                return;
            }

            //validation for patterns - if an image is designated a pattern must pick a catagory
			if(this.rdoPattern.Checked)
			{
				if(ddlCategories.SelectedValue == "0")
				{
					rfvCategory.IsValid = false;
					return;
				}
			}
				
			hidValidateStatus.Value = "false";

            // Check if all validators are valid
			if ( rfvTitle.IsValid && revTags.IsValid  && ValidateTagFields())
			{
				// must clear the categories list, if not then it will not repopulate
				// when another file is selected
				ddlCategories.Enabled = false;

				hidValidateStatus.Value = "true";
				
                ScriptManager.RegisterStartupScript (this, GetType (), "AddFile", "AddFile();", true);

				//reset user control required tags 
				if (UploadTags.Visible)
				{
					ResetUserTagFields();
				}

				//reset the pattern radio buttons
                this.rdoPattern.Style.Add ("display", "none");
                this.rdoPhoto.Style.Add ("display", "none");
				lblPattern.Visible = false;
				lblPhoto.Visible = false;
                imgAdd.Src = ResolveUrl("~/images/button_addtolist_dis.gif");
			}
			else
			{
				imgAdd.Src = ResolveUrl("~/images/button_addtolist.gif");
			}
		}

		private void btnWidgetEmbed_ServerClick(object sender, ImageClickEventArgs e)
		{
			//check validity if not valid stop processing
			if(!rfvtbxFLashLink.IsValid)
			{
				return;
			}

            // Check to make sure user did not enter any "potty mouth" words
            if (KanevaWebGlobals.isTextRestricted (tbxFLashTitle.Text, Constants.eRESTRICTION_TYPE.POTTY_MOUTH) ||
                KanevaWebGlobals.isTextRestricted (tbxFlashTags.Value, Constants.eRESTRICTION_TYPE.POTTY_MOUTH) ||
                KanevaWebGlobals.isTextRestricted (tbxFlashDescription.Text, Constants.eRESTRICTION_TYPE.POTTY_MOUTH))
            {
                ScriptManager.RegisterStartupScript (this, GetType (), "pottymouthalert", "alert('" + PottyMouthText + "');", true);
                return;
            }

            try
            {
                //parse the embed code to return the movie URL - this function intentionally throws an exception
                //when there is an error - need the try catch
                string strippedOutURL = KanevaWebGlobals.ParseWidgetEmbedCode(tbxFLashLink.Text);

                //process/save the embed code
                bool success = ProcessWidgetEmbed(strippedOutURL);
                if (!success)
                {
                    ScriptManager.RegisterStartupScript (this, GetType (), "savelinkwedgetalert", "alert('We were unable to save your link.');", true);
                    return;
                }

                // Record metrics 
                if (!string.IsNullOrWhiteSpace(FromObjPlacementId) &&
                    !string.IsNullOrWhiteSpace(FromZoneInstanceId) &&
                    !string.IsNullOrWhiteSpace(FromZoneType))
                {
                    Dictionary<string, string> metricData = new Dictionary<string, string>
                    {
                        {"interaction", "addedFlashWidget" },
                        {"username", KanevaWebGlobals.CurrentUser.Username},
                        {"objPlacementId", FromObjPlacementId},
                        {"frameworkEnabled", FromFrameworkEnabled.ToString()},
                        {"playerModeActive", (FromGameplayMode.Equals("player", StringComparison.InvariantCultureIgnoreCase)).ToString()},
                        {"zoneInstanceId", FromZoneInstanceId},
                        {"zoneType", FromZoneType},
                    };
                    (new MetricsFacade()).InsertGenericMetricsLog("media_player_interaction_log", metricData, Global.RequestRabbitMQChannel());
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript (this, GetType (), "widgeterralert", "alert('" + ex.Message + "');", true);
                return;
            }

			Response.Redirect (ResolveUrl (ViewState ["lastURL"].ToString ()));

		}

	

		private void btnYouTubeEmbed_ServerClick(object sender, ImageClickEventArgs e)
		{
			//check validity if not valid stop processing
			if(!rfvtbYouTubeTitle.IsValid)
			{
				return;
			}

            // Check to make sure user did not enter any "potty mouth" words
            if (KanevaWebGlobals.isTextRestricted (tbYouTubeTitle.Text, Constants.eRESTRICTION_TYPE.POTTY_MOUTH) ||
                KanevaWebGlobals.isTextRestricted (tbTagsYouTube.Value, Constants.eRESTRICTION_TYPE.POTTY_MOUTH) ||
                KanevaWebGlobals.isTextRestricted (tbBriefDescYouTube.Text, Constants.eRESTRICTION_TYPE.POTTY_MOUTH))
            {
                ScriptManager.RegisterStartupScript (this, GetType (), "pottymouthytalert", "alert('" + PottyMouthText + "');", true);
                return;
            }

            //get the youtube asset id
			//parse string to find youtube assetID

            try
            {
                //parse the embed code to return the movie URL - this function intentionally throws an exception
                //when there is an error - need the try catch
                string strippedOutURL = KanevaWebGlobals.ParseYouTubeEmbedCode(tbYouTubeLink.Text);

                //process/save the embed code
                bool success = ProcessYouTubeEmbed(strippedOutURL);
                if (!success)
                {
                    ScriptManager.RegisterStartupScript (this, GetType (), "savelinkytalert", "alert('We were unable to save your link.');", true);
                    return;
                }

                // Record metrics 
                if (!string.IsNullOrWhiteSpace(FromObjPlacementId) &&
                    !string.IsNullOrWhiteSpace(FromZoneInstanceId) &&
                    !string.IsNullOrWhiteSpace(FromZoneType))
                {
                    Dictionary<string, string> metricData = new Dictionary<string, string>
                    {
                        {"interaction", "addedVideo" },
                        {"username", KanevaWebGlobals.CurrentUser.Username},
                        {"objPlacementId", FromObjPlacementId},
                        {"frameworkEnabled", FromFrameworkEnabled.ToString()},
                        {"playerModeActive", (FromGameplayMode.Equals("player", StringComparison.InvariantCultureIgnoreCase)).ToString()},
                        {"zoneInstanceId", FromZoneInstanceId},
                        {"zoneType", FromZoneType},
                    };
                    (new MetricsFacade()).InsertGenericMetricsLog("media_player_interaction_log", metricData, Global.RequestRabbitMQChannel());
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript (this, GetType (), "yterralert", "alert('" + ex.Message + "');", true);
            }

			Response.Redirect (ResolveUrl (ViewState ["lastURL"].ToString ()));
		}

        protected void btnAddGame_Click (object sender, ImageClickEventArgs e)
        {
            bool pageError = false;
            cvBlank.ErrorMessage = "";
            cvBlank.IsValid = true;

            rfvGameLink.IsValid = true;
            
            // check to make sure user entered file to upload or embed link
            if (((inpGameFile.PostedFile == null || inpGameFile.PostedFile.ContentLength < 1) && txtGameLink.Text.Trim().Length == 0) ||
                !rfvGameTitle.IsValid)
            {
                rfvGameLink.IsValid = false;
                pageError = true;
            }

            if (!revGameTags.IsValid || !rfvGameCategory.IsValid)
            {
                pageError = true;
            }

            // if we have a thumbnail to upload also, then we need to validate it's type
            if (inpGameThumbnail.PostedFile != null && inpGameThumbnail.PostedFile.ContentLength > 0)
            {
                // Make sure it is a valid file type
                string strExtensionUpperCase = System.IO.Path.GetExtension (inpGameThumbnail.PostedFile.FileName).ToUpper ();
                if (!strExtensionUpperCase.Equals (".JPG") && !strExtensionUpperCase.Equals (".JPEG") && !strExtensionUpperCase.Equals (".GIF"))
                {
                    spnThumbnailErr.InnerHtml = "Photo must be a jpg, jpeg, or gif.";
                    pageError = true;
                }
            }

            // If there was a validation error, disable add media button and display errors
            if (pageError)
            {
                btnAddGame.Enabled = false;
                cbAgreementGame.Checked = false;
                return;
            }

            // Check to make sure user did not enter any "potty mouth" words
            if (KanevaWebGlobals.isTextRestricted (txtGameTitle.Text, Constants.eRESTRICTION_TYPE.POTTY_MOUTH) ||
                KanevaWebGlobals.isTextRestricted (txtGameTags.Value, Constants.eRESTRICTION_TYPE.POTTY_MOUTH) ||
                KanevaWebGlobals.isTextRestricted (txtGameDescription.Text, Constants.eRESTRICTION_TYPE.POTTY_MOUTH) ||
                KanevaWebGlobals.isTextRestricted (txtGameInstructions.Text, Constants.eRESTRICTION_TYPE.POTTY_MOUTH) ||
                KanevaWebGlobals.isTextRestricted (txtCompanyName.Text, Constants.eRESTRICTION_TYPE.POTTY_MOUTH))
            {
                cvBlank.ErrorMessage = Constants.VALIDATION_REGEX_RESTRICTED_WORD_MESSAGE;
                cvBlank.IsValid = false;
                valSum.Visible = true;
                return;
            }

            //create PostedFile
            HttpPostedFile File = inpGameFile.PostedFile;
            int assetId = 0;

            // check to see if embed code or filename were entered.  If both were entered
            // then we will process witht the upload file user entered.
            if (File != null && File.ContentLength > 0)
            {
                AddFile (inpGameFile.PostedFile, txtGameTitle.Text.Trim(), txtGameTags.Value, txtGameDescription.Text.Trim(), 
                    txtGameInstructions.Text.Trim(), txtCompanyName.Text.Trim(),
                    Convert.ToInt32(ddlCategoriesGame.SelectedValue), radGamePrivate.Checked ? 1 : 0, chkRestrictedGame.Checked, false, ref assetId);
            }
            else if (txtGameLink.Text.Trim ().Length > 0)
            {

                try
                {
                    //parse the embed code to return the movie URL - this function intentionally throws an exception
                    //when there is an error - need the try catch
                    string strippedOutURL = KanevaWebGlobals.ParseFlashGameEmbedCode(txtGameLink.Text);

                    //process/save the embed code
                    bool success = ProcessGameEmbed(strippedOutURL, ref assetId);
                    if (!success)
                    {
                        ScriptManager.RegisterStartupScript (this, GetType (), "savelinkgamealert", "alert('We were unable to save your link.');", true);
                        return;
                    }

                    // Record metrics 
                    if (!string.IsNullOrWhiteSpace(FromObjPlacementId) &&
                        !string.IsNullOrWhiteSpace(FromZoneInstanceId) &&
                        !string.IsNullOrWhiteSpace(FromZoneType))
                    {
                        Dictionary<string, string> metricData = new Dictionary<string, string>
                        {
                            {"interaction", "addedFlashGame" },
                            {"username", KanevaWebGlobals.CurrentUser.Username},
                            {"objPlacementId", FromObjPlacementId},
                            {"frameworkEnabled", FromFrameworkEnabled.ToString()},
                            {"playerModeActive", (FromGameplayMode.Equals("player", StringComparison.InvariantCultureIgnoreCase)).ToString()},
                            {"zoneInstanceId", FromZoneInstanceId},
                            {"zoneType", FromZoneType},
                        };
                        (new MetricsFacade()).InsertGenericMetricsLog("media_player_interaction_log", metricData, Global.RequestRabbitMQChannel());
                    }
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript (this, GetType (), "gameerralert", "alert('" + ex.Message + "');", true);
                }

            }

            // if the asset was added, check to see if they included a thumbnail pic.  If so,
            // then lets upload that file and update the asset record with the new thumbnail
            if (assetId > 0)
            {
                if (inpGameThumbnail.PostedFile != null && inpGameThumbnail.PostedFile.ContentLength > 0)
                {
                    UploadThumbnail (assetId);
                }
            }
            else
            {
                // asset was not was 0 meaning asset was not created
                cvBlank.ErrorMessage = "The game was not added to the server.  Please try the upload again.";
                cvBlank.IsValid = false;
                valSum.Visible = true;
                return;
            }

            // redirect to asset details page 
            Response.Redirect (ResolveUrl (ViewState["lastURL"].ToString ()));
        }


		/// <summary>
		/// Execute when user changes the state of the pattern check box 
		/// </summary>
		private void rdoPatternImage_CheckedChanged(object sender, EventArgs e)
		{
			if(this.rdoPattern.Checked)
			{
				PopulateCategory ((int) Constants.eASSET_TYPE.PATTERN);
			}
			else
			{
				PopulateCategory ((int) Constants.eASSET_TYPE.PICTURE);
			}
		}

		/// <summary>
		/// Execute when user clicks on the file browser
		/// </summary>
		protected void FileBrowse_Click (object sender, EventArgs e)
		{
			//int assetTypeId = (int) Constants.eASSET_TYPE.VIDEO;

			// Make sure it is a valid file type
			string strExtensionUpperCase = System.IO.Path.GetExtension (System.IO.Path.GetFileName (hidFile.Value)).ToUpper ();


            if (StoreUtility.IsFileGame (strExtensionUpperCase))
            {
                Page.ClientScript.RegisterStartupScript (GetType (), "isgame", "EnableAddButton(false);$('divSwfUpload').style.display='block';", true);
                    
                return;
            }
            else
            {
                // What file type is it?
                ddlCategories.Enabled = false;

                //hide pattern check field as a default
                rdoPattern.Style.Add ("display", "block");
                rdoPhoto.Style.Add ("display", "none");
                lblPattern.Visible = false;
                lblPhoto.Visible = false;
                rdoPhoto.Checked = true;
                rdoPattern.Checked = false;

                // Make sure the extensions are correct
                if (StoreUtility.IsFilePicture (strExtensionUpperCase))
                {
                    //assetTypeId = (int) Constants.eASSET_TYPE.PICTURE;
                    //if it is a picture make pattern row visible
                    if ((strExtensionUpperCase == ".JPG") || (strExtensionUpperCase == ".JPEG"))
                    {
                        rdoPattern.Style.Add ("display", "block");
                        rdoPhoto.Style.Add ("display", "block");
                        lblPattern.Visible = true;
                        lblPhoto.Visible = true;
                        PopulateCategory ((int) Constants.eASSET_TYPE.PICTURE);
                    }
                    else
                    {
                        rdoPattern.Style.Add ("display", "none");
                        rdoPhoto.Style.Add ("display", "none");
                        lblPattern.Visible = false;
                        lblPhoto.Visible = false;
                    }
                }
                else
                {
                    txtTitle.Text = string.Empty;
                    hidFile.Value = string.Empty;
                    btnAdd.Disabled = true;
                    Page.ClientScript.RegisterStartupScript (GetType (), "disableadd", "EnableAddButton(false);", true);
                    spnFileErr.InnerText = "Only JPG and GIF photo formats are supported.";

                    return;
                }

                spnFileErr.InnerText = string.Empty;
                txtTitle.Text = System.IO.Path.GetFileNameWithoutExtension (hidFile.Value);
                btnAdd.Disabled = false;

                ScriptManager.RegisterStartupScript (this, GetType (), "enableadd", "EnableAddButton(true);", true);
                
                imgAdd.Src = ResolveUrl ("~/images/button_addtolist.gif");
                ddlCategories.Enabled = true;
            }
		}

        protected void FileBrowseGame_Click (object sender, EventArgs e)
        {
            // NOTE: Had to take the file path and name and put it in a hidden text field before doing post back to 
            // server.  If you don't do this, the inpGameFile.PostedFile value is null when you try to view it here
            // on the server.  So we put the inpGameFile.value into hidGameFileName.value then
            // pass it back to the server so we can see the filename to check the extension.
            
            // Make sure it is a valid file type
            string strExtensionUpperCase = System.IO.Path.GetExtension (System.IO.Path.GetFileName (hidGameFileName.Value)).ToUpper ();

            spnGameFileErr.InnerText = string.Empty;

            if (!StoreUtility.IsFileGame (strExtensionUpperCase))
            {
                spnGameFileErr.InnerText = "Game file must be type .swf";
                return;
            }
        }
        
        /// <summary>
        /// Sets a generic thumbnail for media 
        /// </summary>
        /// <returns></returns>
        private int SetGenericThumbnail(int assetId, string thumbnail)
        {
            int result = 0;

            string[] thumbNailPath = thumbnail.Split('.');

            try
            {
                StoreUtility.UpdateAssetImage(assetId, thumbnail, "");

                // Small
                StoreUtility.UpdateAssetThumb(assetId, thumbNailPath[0] + "_sm." + thumbNailPath[1], "thumbnail_small_path");
                // Medium
                StoreUtility.UpdateAssetThumb(assetId, thumbNailPath[0] + "_me." + thumbNailPath[1], "thumbnail_medium_path");
                // Large
                StoreUtility.UpdateAssetThumb(assetId, thumbNailPath[0] + "_la." + thumbNailPath[1], "thumbnail_large_path");

                StoreUtility.MarkAssetThumbAsGenerated(assetId);
            }
            catch (Exception exc)
            {
                // Log any errors
                m_logger.Error("Error generating Thumbnail", exc);
                result = 1;
            }

            return result;
        }

        #endregion


        #region Declarations

        private string FromGameplayMode
        {
            set
            {
                ViewState["FromGameplayMode"] = value.Trim();
            }
            get
            {
                if (ViewState["FromGameplayMode"] == null)
                {
                    ViewState["FromGameplayMode"] = (Request.Params["FromGameplayMode"] ?? string.Empty);
                }

                return ViewState["FromGameplayMode"].ToString();
            }
        }

        private string FromObjPlacementId
        {
            set
            {
                ViewState["FromObjPlacementId"] = value.Trim();
            }
            get
            {
                if (ViewState["FromObjPlacementId"] == null)
                {
                    ViewState["FromObjPlacementId"] = (Request.Params["FromObjPlacementId"] ?? string.Empty);
                }

                return ViewState["FromObjPlacementId"].ToString();
            }
        }

        private string FromZoneInstanceId
        {
            set
            {
                ViewState["FromZoneInstanceId"] = value.Trim();
            }
            get
            {
                if (ViewState["FromZoneInstanceId"] == null)
                {
                    ViewState["FromZoneInstanceId"] = (Request.Params["FromZoneInstanceId"] ?? string.Empty);
                }

                return ViewState["FromZoneInstanceId"].ToString();
            }
        }

        private string FromZoneType
        {
            set
            {
                ViewState["FromZoneType"] = value.Trim();
            }
            get
            {
                if (ViewState["FromZoneType"] == null)
                {
                    ViewState["FromZoneType"] = (Request.Params["FromZoneType"] ?? string.Empty);
                }

                return ViewState["FromZoneType"].ToString();
            }
        }

        private bool FromFrameworkEnabled
        {
            get
            {
                return !string.IsNullOrWhiteSpace(FromGameplayMode);
            }
        }

        const string PottyMouthText = "Your entry contains a restricted word. Please modify your entry.";
        protected HtmlAnchor lnk_AccessPass, lnk_AccessPassYouTube, lnk_AccessPassFlash, lnk_AccessPassGames;
        protected HtmlTable tblAccessPass, tblAccessPassYouTube, tblAccessPassFlash, tblAccessPassGames;
        private int _channelId;
		private int _userId; //owner of the asset
		private int _moduleId; //upload module - for tag use
		private int _uploadTabId; //page configuration

		// Flash Games
        protected HtmlInputFile inpGameFile, inpGameThumbnail;
        protected DropDownList ddlCategoriesGame;
        protected HtmlTableRow tbrFlashGame;
        protected TextBox txtGameTitle;
        protected TextBox txtGameDescription, txtCompanyName, txtGameInstructions;
        protected TextBox txtGameLink;
        protected HtmlInputCheckBox cbAgreementGame, chkRestrictedGame;
        protected HtmlInputText txtGameTags;
        protected HtmlInputRadioButton radGamePublic, radGamePrivate;
        protected ImageButton btnAddGame;
        protected Button btnFileBrowseGame;
        protected HtmlContainerControl spnGameFileErr, spnThumbnailErr;
        protected HtmlInputHidden hidGameFileName;
        protected ValidationSummary valSum;
        protected CustomValidator cvBlank;
        protected RegularExpressionValidator revGameTags;
        protected RequiredFieldValidator rfvGameTitle, rfvGameLink, rfvGameCategory;

        protected HtmlContainerControl divSwfUpload;

        protected RegularExpressionValidator	revTags;
		protected RequiredFieldValidator		rfvCategory, rfvTitle, rfvtbxFLashLink, rfvCatagoriesYT, rfvCatagoriesFL;
        protected HtmlGenericControl liUpload, liYouTube, liFlash, liFlashGame;

		protected LinkButton			btnFileBrowse, btnAddFileToQueue;
		protected CustomValidator		cvInputFile;
        protected HtmlInputFile file_0;
		protected Literal				litScript, ltl_Instructions;
		protected HtmlInputHidden		hidFile, tbx_ChnlOwnr_Tags;
        protected DropDownList ddlCategories, ddlCategoriesYT, ddlCategoriesFL;
		protected HtmlInputButton		btnAdd;
		protected HtmlInputImage		btnUpload, btnYouTubeEmbed, btnWidgetEmbed;
		protected HyperLink back;

		protected HtmlImage				imgAdd;

        protected TextBox txtTitle;
		protected HtmlGenericControl	spnFileErr;
		protected Button				btnUpdate;
		protected HtmlInputHidden		hidTorrentSize, hidValidateStatus;
		protected HtmlInputRadioButton	radPublic, radPrivate;
		protected HtmlTable				UploadTags;
		protected HtmlTableRow			rwPattern, tagRow1, tagRow2, tagRow3, tagRow4, tagRow5, tagRow6;
        protected HtmlTableRow tbrYouTube, tbrUpload, tbrFlash;

		protected CheckBox					cbAgreement;
        protected TextBox txtDescription;
		protected HtmlInputText				txtTags, tbxFlashTags;
		protected Label						lbl_Description_1, lblASTReq6,lblASTReq5,lblASTReq4,lblPattern;
		protected TextBox					tbx_TagField1;
		protected RequiredFieldValidator	rfv_tagField1;
		protected Label						lbl_Description_2, lblASTReq3, lblASTReq2, lblASTReq1, lblPhoto;
		protected TextBox					tbx_TagField2;
		protected RequiredFieldValidator	rfv_tagField2;
		protected Label						lbl_Description_3;
		protected TextBox					tbx_TagField3;
		protected RequiredFieldValidator	rfv_tagField3;
		protected Label						lbl_Description_4;
		protected TextBox					tbx_TagField4;
		protected RequiredFieldValidator	rfv_tagField4;
		protected Label						lbl_Description_5;
		protected TextBox					tbx_TagField5;
		protected RequiredFieldValidator	rfv_tagField5;
		protected Label						lbl_Description_6;
		protected TextBox					tbx_TagField6;
		protected RequiredFieldValidator	rfv_tagField6;

		protected LinkButton lbUpload, lbYouTubeEmbed, lbFlashWidgets, lbFlashGame;
        protected TextBox tbYouTubeLink, tbxFLashLink, tbxFLashTitle;
        protected RegularExpressionValidator  revtbxFlashTags;
        protected HtmlInputRadioButton Radio1;
		protected HtmlInputRadioButton Radio2, rdoFlashPublic, rdoFlashPrivate ;
		protected RadioButton rdoPattern, rdoPhoto;
		protected RequiredFieldValidator rfvtbYouTubeLink;
		protected TextBox tbYouTubeTitle;
		protected RequiredFieldValidator rfvtbYouTubeTitle;
		protected RegularExpressionValidator REVtbTagsYouTube;
		protected TextBox tbBriefDescYouTube, tbxFlashDescription;
		protected HtmlInputText tbTagsYouTube;
		protected HtmlInputCheckBox cbAgreementYouTube, cbAgreementWidget;
        protected HtmlInputCheckBox chkRestrictedYoutube, chkRestrictedWidget; 

        protected HtmlAnchor aHelp;

		// Logger
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);
		
        #endregion


		#region Web Form Designer generated code
		
        override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.lbUpload.Command += new System.Web.UI.WebControls.CommandEventHandler(this.tabClicked_Command);
			this.lbYouTubeEmbed.Command += new System.Web.UI.WebControls.CommandEventHandler(this.tabClicked_Command);
			this.lbFlashWidgets.Command += new System.Web.UI.WebControls.CommandEventHandler(this.tabClicked_Command);
            this.lbFlashGame.Command += new System.Web.UI.WebControls.CommandEventHandler (this.tabClicked_Command);
            this.rdoPhoto.CheckedChanged += new System.EventHandler (this.rdoPatternImage_CheckedChanged);
			this.rdoPattern.CheckedChanged += new System.EventHandler(this.rdoPatternImage_CheckedChanged);
			this.btnYouTubeEmbed.ServerClick += new System.Web.UI.ImageClickEventHandler(this.btnYouTubeEmbed_ServerClick);
			this.btnWidgetEmbed.ServerClick += new System.Web.UI.ImageClickEventHandler(this.btnWidgetEmbed_ServerClick);
            this.Load += new System.EventHandler (this.Page_Load);
		}
		
        #endregion
	}
}
