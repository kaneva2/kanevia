<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="inventory.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.mykaneva.inventory" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../usercontrols/Pager.ascx" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>

<link href="../css/new.css" type="text/css" rel="stylesheet">

<script src="../jscript/prototype.js" type="text/javascript" language="javascript"></script>

<script type="text/JavaScript">
function changeBG(obj) {
	obj.style.background = "url('../images/media/tabsMedia.gif')";
}
function changeBG2(objeto) {
	objeto.style.background = "url('../images/media/tabsMedia_on.gif')";
}
function changeBGB(objeto) {
	objeto.style.background = "url('../images/media/tabsMedia2.gif')";
}
function changeBGB2(objeto) {
	objeto.style.background = "url('../images/media/tabsMedia2_on.gif')";
}
</script>

<style type="text/css">
<!--
.navHover a:hover {background-color:#214f73;color:#b3d6f2;cursor:hand;}

.display
{
	position: static !important;
	position: realtive;
}
.btnPadding
{
	padding-left:48px;
	_padding-left:30px;	/* IE only hack */
}
.navselected  {
/*	width: 107%;   */
	color: #FFFFFF;
/*	background: transparent url("../images/librarynav_bg.gif") no-repeat -26px 0; */
	font-weight: bold;
	}

-->
</style>
	

<div id="divLoading">
	<table height="450" cellSpacing="0" cellPadding="0" width="100%" border="0" id="Table1">
		<tr>
			<th class="loadingText" id="divLoadingText">Loading...</th>
		</tr>
	</table>
</div>
					
<div id="divData" style="display: none">
	<!-- Main Body structure table -->
	<table cellSpacing="0" cellPadding="0" class="newcontainer" align="center" border="0">
		<tr>
			<td>
				<table cellSpacing="0" cellPadding="0" width="100%" border="0" class="newcontainerborder">
					<tr>
						<td class=""></td>
						<td class=""></td>
						<td class=""></td>
					</tr>
					<tr>
						<td class=""><img id="img7" height="1" runat="server" src="~/images/spacer.gif" width="1"></td>
						<td class="newdatacontainer" vAlign="top">
							<table cellSpacing="0" cellPadding="0" width="100%" border="0"> 
							<!-- BEGIN TOP STATUS BAR -->
								<tr>
									<td>
										<div id="pageheader">
											<table cellSpacing="0" cellPadding="0" width="99%" border="0">
												<tr>
													<td align="left">
														<h1>My Inventory</h1>
													</td>
													<td vAlign="middle" class="searchnav" align="right">
															
														<ajax:ajaxpanel id="ajpFilterPanel" runat="server">

															<table cellSpacing="0" cellPadding="0" width="700" border="0">
																<tr> 
																	<!-- Begin page results Filter --->
																	<td class="searchnav" nowrap align="right" valign="top" style="height:30px;">
																		<asp:label runat="server" id="lblResults_Top" cssclass="insideTextNoBold" /><br />
																		<kaneva:pager runat="server" isajaxmode="True" id="pgTop" onpagechanged="pg_PageChange" maxpagestodisplay="5" shownextprevlabels="true" />
																	</td> 
																	<!-- End page results Filter --->
																</tr>
															</table>
															
														</ajax:ajaxpanel>
															
													</td>
												</tr>
											</table>
										</div>
									</td>
								</tr>
								<!-- END TOP STATUS BAR --> 
								<!-- BEGIN MAIN BODY -->
								<tr>
									<td valign="top" align="left" width="968">
										<table cellspacing="0" cellpadding="0" width="100%" border="0">
											<tr>
												<!-- Start Side Panel Controls -->
												<td valign="top" align="center" width="246">
														
													<ajax:ajaxpanel id="ajpResults" runat="server">

														<table cellspacing="0" cellpadding="0" width="100%" border="0">
															<tr>
																<td align="center">
																	
																	<!-- smart nav div-->
																	<div class="module">
																		<span class="ct"><span class="cl"></span></span>
																		<h2></h2>																  
																		<div style="text-align:center;">
																			<input type="button" id="btnMyMerch" runat="server" value="My Merchandise" />
																		</div>
																																				   
																		<br />			
																		<span class="cb"><span class="cl"></span></span>
																	</div>
														
																	<div class="module playlistbox">
																		<span class="ct"><span class="cl"></span></span>
																		<div style="text-align:left;">
																		
																		<asp:treeview id="tvCategories" runat="server" nodewrap="false"
																			verticalpadding="4" showlines="false"   
																			showexpandcollapse="True" onselectednodechanged="tvCategories_OnClick" cssclass="nohover">
																			
																			<rootnodestyle font-size="12"  
																				font-bold="true" 
																				verticalpadding="5" 
																				horizontalpadding="5"  
																				forecolor="#13667A" 
																				font-names="verdana, arial, sans-serif" />
																				
																			<ParentNodeStyle font-size="10"
																				verticalpadding="4" />
																			
																			<leafnodestyle verticalpadding="4" 
																				font-size="10" 
																				font-names="verdana, arial, sans-serif" /> 
																			
																			<SelectedNodeStyle width="80%" imageurl="../images/librarynav_bg.gif"  
																				verticalpadding="3" borderwidth="0" bordercolor="red" font-bold="true" horizontalpadding="3" />
																			
																			<databindings>
																			    <asp:TreeNodeBinding DataMember="Level1" TextField="name" valuefield="catId" />
																				<asp:TreeNodeBinding DataMember="Level2" TextField="name" valuefield="catId" />
																				<asp:TreeNodeBinding DataMember="Level3" TextField="name" valuefield="catId" />
																				<asp:TreeNodeBinding DataMember="Level4" TextField="name" valuefield="catId" />
																			</databindings>
																		    
																		    <NodeStyle Font-Names="Tahoma" 
																				Font-Size="8pt" 
																				ForeColor="Black" 
																				HorizontalPadding="5px"
																				NodeSpacing="0px" 
																				VerticalPadding="1px" />
																		
																		</asp:treeview>
																		</div>
																		
																		<div id="divTest" runat="server" style="text-align:left;"></div>
																		
																		<span class="cb"><span class="cl"></span></span>
																	</div>
																
																</td>
															</tr>
														</table>
														
													</ajax:ajaxpanel>
														
												</td>
												<!-- end Side Panel Controls -->
												<td width="14">
													<img id="img10" height="1" runat="server" src="~/images/spacer.gif" width="14">
												</td>
												<!-- BEGIN MEDIA DISPLAY AREA -->
												<td valign="top" width="708">
													<table cellspacing="0" cellpadding="0" width="100%" border="0">
														<tr>
															<td>
																<!--START RIGHTCOL-->
																<div class="module whitebg">
																	<span class="ct"><span class="cl"></span></span>
																		
																		<ajax:ajaxpanel id="ajpControlBar" runat="server">
	
																			<div class="errBox black" style="position:relative;display:none;width:560px;" runat="server" id="divMessages"></div>
																			
																			<table cellspacing="0" cellpadding="0" width="98%" border="0">
																				<tr style="height: 42px">
																					<td align="left" valign="top" width="320">
																						<h2><asp:label runat="server" id="lblCategoryName" /></h2>
																					</td>
																					<td align="right" width="320" style="padding-left:40px;">
																						<table cellpadding="0" cellspacing="0" border="0" >
																							<tr>
																								<td align="right">
																									<h4 id="h4SearchLabel" runat="server" style="text-align:right;margin-right:8px;">Search:</h4>
																								</td>
																								<td width="160px" align="left">
																									<input type="text" style="width:150px;" class="display" runat="server" id="txtSearch" maxlength="51" />
																								</td>
																								<td align="left">
																									<ajax:ajaxpanel id="ajp2" runat="server">
																										<asp:button runat="server" id="btnSearch" text="Search" style="width:80px;" onclick="btnSearch_Click" />
																									</ajax:ajaxpanel>
																								</td>
																							</tr>
																						</table>
																					</td>
																				</tr>
																			</table>
																							
																		</ajax:ajaxpanel>
																		
																		<!-- END management controls -->
																		<!-- Begin display region -->
																		<table width="691" height="360" cellspacing="0" cellpadding="0" border="0">
																			<tr>
																				<td colspan="3" style="padding:0;text-align:center;" valign="top">
																		
																					<ajax:ajaxpanel id="ajpDataRepeaters" runat="server">
													
																						<div id="divNoResults" runat="server" style="display:none;margin-top:20px;">No Inventory items found.</div>			
																					
																						<!-- repeaters go here -->	
																						<div id="divInventory" runat="server" style="display:none;">
																						<asp:repeater id="rptInventory" runat="server">
																							<headertemplate>
																								<table cellpadding="4" cellspacing="0" border="0" width="690" class="data" style="margin-left:1px;">
																									<tr>
																										<td style="background-color:#fdfdfc;width:500px;padding-left:5px;">Item</th>
																										<td style="background-color:#fdfdfc;width:80px;text-align:center;">Quantity</th>
																										<td style="background-color:#fdfdfc;">Status</th>
																									</tr>
																							</headertemplate>
																							<itemtemplate>
																									<tr>
																										<td align="left" style="padding-left:5px;"><a href="<%# GetItemDetailLink(DataBinder.Eval (Container.DataItem, "global_id").ToString()) %>" title="<%# Server.HtmlEncode(DataBinder.Eval (Container.DataItem, "description").ToString()) %>" ><%# DataBinder.Eval (Container.DataItem, "name") %></a></td>
																										<td align="center"><%# DataBinder.Eval (Container.DataItem, "quantity") %></td>
																										<td align="left"><%# GetInventoryStatus(DataBinder.Eval (Container.DataItem, "inventory_type").ToString(), DataBinder.Eval (Container.DataItem, "inv_location").ToString()) %></td>
																									</tr>
																							</itemtemplate>
																							<alternatingitemtemplate>
																									<tr class="altrow">
																										<td align="left" style="padding-left:5px;"><a href="<%# GetItemDetailLink(DataBinder.Eval (Container.DataItem, "global_id").ToString()) %>" title="<%# Server.HtmlEncode(DataBinder.Eval (Container.DataItem, "description").ToString()) %>" ><%# DataBinder.Eval (Container.DataItem, "name") %></a></td>
																										<td align="center"><%# DataBinder.Eval (Container.DataItem, "quantity") %></td>
																										<td align="left"><%# GetInventoryStatus(DataBinder.Eval (Container.DataItem, "inventory_type").ToString(), DataBinder.Eval (Container.DataItem, "inv_location").ToString()) %></td>
																									</tr>
																							</alternatingitemtemplate>
																							<footertemplate>
																								</table>
																							</footertemplate>
																						</asp:repeater>
																						</div>
																									
																					</ajax:ajaxpanel>
																			
																				</td>
																			</tr>
																			
																		</table>
																	
																	<span class="cb"><span class="cl"></span></span>
																</div>
																<!--END RIGHTCOL-->
															</td>
														</tr>
													</table>
												</td>
												<!-- END MEDIA DISPLAY AREA -->
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td valign="top" align="right" style="padding-right:8px;">
									
										<ajax:ajaxpanel id="ajpBottomPager" runat="server">
											<img id="Img1" height="10" runat="server" src="~/images/spacer.gif" width="1">
											<asp:label runat="server" id="lblResults_Bottom" cssclass="insideTextNoBold" /><br />
											<kaneva:pager id="pgBottom" onpagechanged="pg_PageChange" maxpagestodisplay="5" runat="server" isajaxmode="True" shownextprevlabels="true"></kaneva:pager>
										</ajax:ajaxpanel>
										
									</td>
								</tr>
							</table>
						</td>
						<td class="">
							<img id="Img5" height="1" runat="server" src="~/images/spacer.gif" width="1">
						</td>
					</tr>
					<tr>
						<td class=""></td>
						<td class=""></td>
						<td class=""></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<!-- END MAIN BODY -->
</div>
