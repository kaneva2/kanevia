<%@ Page language="c#" Codebehind="mediaFavorites.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.mediaFavorites" %>
<%@ Register TagPrefix="Kaneva" TagName="StoreFilter" Src="../usercontrols/StoreFilter.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../usercontrols/Pager.ascx" %>

<link href="../css/home.css" rel="stylesheet" type="text/css" />		
<link href="../css/kanevaSC.css" rel="stylesheet" type="text/css"/>
<link href="../css/friends.css" rel="stylesheet" type="text/css"/>
<link href="../css/kanevaText.css" type="text/css" rel="stylesheet">

<script type="text/JavaScript">
	
	function changeBG(obj) {
		obj.style.background = "url('../images/media/tabsMedia.gif')";
	}
	function changeBG2(objeto) {
		objeto.style.background = "url('../images/media/tabsMedia_on.gif')";
	}
	function changeBGB(objeto) {
		objeto.style.background = "url('../images/media/tabsMedia2.gif')";
	}
	function changeBGB2(objeto) {
		objeto.style.background = "url('../images/media/tabsMedia2_on.gif')";
	}

</script>


<asp:ValidationSummary ShowMessageBox="False" ShowSummary="True" Class="formError" id="valSum" DisplayMode="BulletList" runat="server" HeaderText="Please correct the following errors:"/>

<div ID="divLoading">
	<table BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="100%" HEIGHT="300px">
		<tr><th CLASS="loadingText" ID="divLoadingText">Loading...</th></tr>
	</table>
</div>
<div id="divData" style="DISPLAY:none"/>	

<table width="100%"  border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="1" height="15"></td>
	</tr>
</table>
<table width="990" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td width="6" class="frTopLeft"></td>
		<td colspan="3" class="frTop"></td>
		<td width="7" class="frTopRight"></td>
	</tr>
	<tr> 
		<td rowspan="3" bgcolor="#F1F1F2" class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1"></td>
		<td width="10" rowspan="3" valign="top" bgcolor="#F1F1F2" class="frBgInt1"></td>
		<td height="50" valign="middle" class="frBgInt1">
			<!-- MY MEDIA LIBRARY CENTER MENU  -->
				<table width="100%"  border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="100"><img runat="server" src="~/images/titles/titMediaLibrary.gif" width="93" height="15"></td>
						<td width="100" align="left"></td>
						<td align="right"><asp:Label runat="server" id="lblCartMessage" CssClass="insideTextNoBold"/></td>
					</tr>
				</table>			
	<!-- END  MY MEDIA LIBRARY CENTER MENU  -->
	</td>
		<td width="10" rowspan="3" valign="top" bgcolor="#F1F1F2" class="frBgInt1"></td>
		<td rowspan="3" class="frBorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1"></td>
	</tr>
	<tr>
		<td valign="top" bgcolor="#F1F1F2" class="frBgInt1">
			<!-- ACTION MENU -->
			<table width="100%"  border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td valign="middle">
						<table border="0" cellpadding="0" cellspacing="0" width="109">
                        <tr>
                          <td colspan="3" valign="middle" style="background:url(../images/media/tabsMedia_on.gif);" onmouseover="changeBG(this);" onmouseout="changeBG2(this);" width="109" height="25" >
							<table border="0" cellpadding="0" cellspacing="0"  >
                              <tr>
                                <td ><img runat="server" src="~/images/spacer.gif" width="8" height="8" /></td>
                                <td  valign="middle"><a href="#"><img runat="server" src="~/images/media-library.gif" border="0"/></a></td>
                                <td  valign="top"><img runat="server" src="~/images/spacer.gif" width="20" height="1" /></td>
                                <td  nowrap="nowrap" align="center" class="insideTextNoBold">
									<asp:HyperLink ID="hlAll" runat="server">All</asp:HyperLink> (<asp:Label ID="lblAllCount" runat="server"/>)
                                </td>
                              </tr>
                          </table></td>
                        </tr>
                    </table></td>
                    <td>&nbsp;</td>
                    <td><table border="0" cellpadding="0" cellspacing="0" width="109" >
                        <tr>
                          <td colspan="3"   style="background:url(../images/media/tabsMedia_on.gif);" onmouseover="changeBG(this);" onmouseout="changeBG2(this);" width="109" height="25" >
							<table border="0" cellpadding="0" cellspacing="0"  >
                              <tr>
                                <td ><img runat="server" src="~/images/spacer.gif" width="8" height="8" /></td>
                                <td  valign="middle"><a href="#"><img runat="server" src="~/images/videos.gif" width="14" height="20" border="0"/></a></td>
                                <td  valign="top"><img runat="server" src="~/images/spacer.gif" width="5" height="1" /></td>
                                <td   nowrap="nowrap" align="left" class="insideTextNoBold">
									<asp:HyperLink ID="hlVideo" runat="server">Videos</asp:HyperLink> (<asp:Label ID="lblVideoCount" runat="server"/>)
                                </td>
                              </tr>
                          </table></td>
                        </tr>
                    </table></td>
                    <td>&nbsp;</td>
                    <td><table border="0" cellpadding="0" cellspacing="0" width="109" >
                        <tr>
                          <td colspan="3"   style="background:url(../images/media/tabsMedia_on.gif);" onmouseover="changeBG(this);" onmouseout="changeBG2(this);" width="109" height="25" >
                          <table border="0" cellpadding="0" cellspacing="0"  >
                              <tr>
                                <td ><img runat="server" src="~/images/spacer.gif" width="8" height="8" /></td>
                                <td  valign="middle"><a href="#"><img runat="server" src="~/images/photos.gif" width="20" height="20" border="0"/></a></td>
                                <td  valign="top"><img runat="server" src="~/images/spacer.gif" width="5" height="1" /></td>
                                <td   nowrap="nowrap" align="left" class="insideTextNoBold">
									<asp:HyperLink ID="hlPhoto" runat="server">Photos</asp:HyperLink> (<asp:Label ID="lblPhotoCount" runat="server"/>)
                                </td>
                              </tr>
                          </table></td>
                        </tr>
                    </table></td>
                    <td>&nbsp;</td>
                    <td><table border="0" cellpadding="0" cellspacing="0" width="109" >
                        <tr>
                          <td colspan="3"   style="background:url(../images/media/tabsMedia_on.gif);" onmouseover="changeBG(this);" onmouseout="changeBG2(this);" width="109" height="25" >
                          <table border="0" cellpadding="0" cellspacing="0"  >
                              <tr>
                                <td ><img runat="server" src="~/images/spacer.gif" width="8" height="8" /></td>
                                <td  valign="middle"><a href="#"><img runat="server" src="~/images/music.gif" width="15" height="20" border="0"/></a></td>
                                <td  valign="top"><img runat="server" src="~/images/spacer.gif" width="5" height="1" /></td>
                                <td   nowrap="nowrap" align="left" class="insideTextNoBold">
									<asp:HyperLink ID="hlMusic" runat="server">Music</asp:HyperLink> (<asp:Label ID="lblMusicCount" runat="server"/>)
                                </td>
                              </tr>
                          </table></td>
                        </tr>
                    </table></td>
                    <td>&nbsp;</td>
                    <td><table border="0" cellpadding="0" cellspacing="0" width="109" >
                        <tr>
                          <td colspan="3"   style="background:url(../images/media/tabsMedia_on.gif);" onmouseover="changeBG(this);" onmouseout="changeBG2(this);" width="109" height="25" >
                          <table border="0" cellpadding="0" cellspacing="0"  >
                              <tr>
                                <td ><img runat="server" src="~/images/spacer.gif" width="8" height="8" /></td>
                                <td  valign="middle"><a href="#"><img runat="server" src="~/images/games.gif" width="16" height="20" border="0"/></a></td>
                                <td  valign="top"><img runat="server" src="~/images/spacer.gif" width="5" height="1" /></td>
                                <td   nowrap="nowrap" align="left" class="insideTextNoBold">
									<asp:HyperLink ID="hlGames" runat="server">Games</asp:HyperLink> (<asp:Label ID="lblGamesCount" runat="server"/>)
                                </td>
                              </tr>
                          </table></td>
                        </tr>
                    </table></td>
                    <td>&nbsp;</td>
                    <td valign="middle"><table border="0" cellpadding="0" cellspacing="0" width="168" >
                        <tr>
                          <td colspan="3" valign="middle" style="background:url(../images/media/tabsMedia2_on.gif);" onmouseover="changeBGB(this);" onmouseout="changeBGB2(this);" width="168" height="25" >
                          <table border="0" cellpadding="0" cellspacing="0"  >
                              <tr>
                                <td ><img runat="server" src="~/images/spacer.gif" width="8" height="8" /></td>
                                <td  valign="middle"><a href="#"><img runat="server" src="~/images/channel-request.gif" border="0"/></a></td>
                                <td  valign="top"><img runat="server" src="~/images/spacer.gif" width="5" height="1" /></td>
                                <td   nowrap="nowrap" align="left" class="insideTextNoBold">
                                
                                
									<span ID="pnlConnectedMedia" runat="server"><asp:HyperLink ID="hlConnectedMedia" runat="server">Connected Media</asp:HyperLink> (<asp:Label ID="lblConnectedMediaCount" runat="server"/>)</span>
                                
                                </td>
                              </tr>
                          </table></td>
                        </tr>
                    </table></td>
                    <td width="50%">&nbsp;</td>
                  </tr>
                </table></td>
              </tr>
            </table>
			
			
			<table width="100%" height="45" border="0" cellpadding="0" cellspacing="0" align="center">
				<tr>
					<td width="7" class="intBorderTopLeft"></td>
					<td class="intBorderTop"></td>
					<td width="7" class="intBorderTopRight"></td>
				</tr>
				<tr>
					<td class="intBorderBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1"></td>
					<td valign="middle" class="intBorderBgInt1">				  
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="70" align="center"><span class="insideTextNoBold">Actions</span></td>
								<td width="640" align="left">
									<asp:Button ID="btnDeleteSelected" runat="server" Text=" Delete Selected " />
								</td>
								<td width="87" align="left" valign="middle"></td>
								<td width="36" align="center" valign="middle"></td>
								<td width="100" align="left" valign="middle">
									
								</td>
							</tr>
						</table>
					</td>
				<td class="intBorderBorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1"></td>
			</tr>
			<tr>
				<td class="intBorderBottomLeft"></td>
				<td class="intBorderBottom"></td>
				<td class="intBorderBottomRight"></td>
			</tr>
		</table>
		<!-- END ACTION MENU -->
		<!-- IMAGE SPACER -->
		<table width="100%"  border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td class="intBorderBgInt1"><img runat="server" src="~/images/spacer.gif" width="1" height="9"></td>
			</tr>
		</table>
		<!-- END IMAGE SPACER -->
		<!-- DISPLAY OPTIONS -->
		<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
			<tr>
				<td class="intBorderB1TopLeft"></td>
				<td class="intBorderB1Top"></td>
				<td class="intBorderB1TopRight"></td>
			</tr>
			<tr>
				<td class="intBorderB1BorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1"></td>
				<td valign="top" class="intBorderB1BgInt1">
					<table width="100%"  border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td runat="server" id="tdFilterByChannel">
							</td>
							<td>&nbsp;</td>
							<td><Kaneva:StoreFilter runat="server" id="filStore" AssetType="0" HideThumbView="True"/>
							</td>
							<td align="center"> 
							</td>
							<td align="right"></td>
							<td align="left" width="5">&nbsp;</td>
							<td align="left"><Kaneva:Pager runat="server" id="pgTop"/></td>
						</tr>
					</table>
				</td>
				<td class="intBorderB1BorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1"></td>
			</tr>
			<tr>
				<td class="intBorderB1BottomLeft"></td>
				<td class="intBorderB1Bottom"></td>
				<td class="intBorderB1BottomRight"></td>
			</tr>
		</table>
		<!-- END DISPLAY OPTIONS -->
		<!-- IMAGE SPACER -->
		<table width="100%"  border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td class="intBorderBgInt1"><img runat="server" src="~/images/spacer.gif" width="1" height="2"></td>
			</tr>
		</table>
		<!-- END IMAGE SPACER -->
		<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
			<tr>
				<td height="7" class="intBorderCTopLeft"></td>
				<td colspan="8" class="intBorderCTop"></td>
				<td class="intBorderCTopRight"></td>
			</tr>
			<tr>
				<td height="20" class="intBorderCBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1"></td>
				<td  valign="middle" class="intBorderCBgInt1" align="center"><input type="checkbox" id="chkSelect" onclick="javascript:SelectAll(this,this.checked);"/></td>
				<td width="2" class="sepTit1"><img runat="server" src="~/images/spacer.gif" width="1" height="1"></td>
				<td valign="top" class="intBorderCBgInt1"> <span class="content" style="padding-left:8px;"><a>Media</a></span></td>
				<td width="2" class="sepTit1"><img runat="server" src="~/images/spacer.gif" width="1" height="1"></td>
				<td valign="top" class="intBorderCBgInt1">&nbsp;</td>
				<td colspan="3" valign="middle" align="left" class="intBorderCBgInt1"><span class="content"><a>Media Details</a></span></td>
				<td class="intBorderCBorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1"></td>
			</tr>
			<tr>
				<td height="7" class="intBorderCBottomLeft"></td>
				<td colspan="8" class="intBorderCBottom"></td>
				<td class="intBorderCBottomRight"></td>
			</tr>
		  		<asp:Repeater runat="server" id="rptItems" >  
		  			<ItemTemplate>						
						<tr class="intColor1">
							<td height="125" class="intBorderC1BorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="110" /></td>
							<td align="center" class="intColor1">
								<asp:checkbox id="chkEdit" CssClass="Filter2" runat="server" />
								<asp:hyperlink id="hlDelete" runat="server" ImageURL="~/images/widgets/widget_delete_bt.gif" NavigateURL='<%#GetDeleteTagScript ((int) DataBinder.Eval(Container.DataItem, "asset_id")) %>' ToolTip="Remove Connected Media"/>
							
								<input type="hidden" runat="server" id="hidAssetItemId" value='<%#DataBinder.Eval(Container.DataItem, "asset_id")%>' NAME="Hidden1"> 
							</td>
							<td width="1" class="sepInt1"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
							<td align="center" class="intColor1">
								<!--image holder-->
								<table border="0" cellpadding="0" cellspacing="0" width="94" height="94" align="center">
									<tr>
										<td class="boxWhiteTopLeft"><img runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
										<td class="boxWhiteTop" ><img runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
										<td class="boxWhiteTopRight"><img runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
									</tr>
									<tr>
										<td class="boxWhiteLeft" ><img runat="server" src="~/images/spacer.gif" width="1" height="4" /></td>
										<td class="boxWhiteContent" height="80" align="center" valign="middle">
											<a href='<%# GetAssetDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "asset_id")))%>'><img border="0" style="margin: 10px;" src='<%#GetMediaImageURL (DataBinder.Eval(Container.DataItem, "thumbnail_medium_path").ToString () ,"me", Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_id")), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_type_id")), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "thumbnail_gen")))%>' border="0"/></a>
										</td>
										<td class="boxWhiteRight"><img runat="server" src="~/images/spacer.gif" width="1" height="4"/></td>
									</tr>
									<tr>
										<td class="boxWhiteBottomLeft" ><img runat="server" src="~/images/spacer.gif" width="4" height="4" /></td>
										<td class="boxWhiteBottom"><img runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
										<td class="boxWhiteBottomRight"><img runat="server" src="~/images/spacer.gif" width="4" height="4" /></td>
									</tr>
								</table>
								<!--end image holder-->			   
							</td>
							<td width="1" class="sepInt1"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
							<td valign="top" class="intColor1"><br /></td>
							<td valign="middle" class="intColor1" align="left">
								<span class="insideTextNoBold1">Name:</span>&nbsp;<a style="font-size:110%" href='<%# GetAssetDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "asset_id")))%>'><%# TruncateWithEllipsis (Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "name").ToString ()), 40) %></a>
								<br /><span class="insideTextNoBold">Owner: <a class="insideTextNoBold" href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "username").ToString ())%>><%# DataBinder.Eval(Container.DataItem, "username") %></a>
								<br /><span class="insideTextNoBold">Type: <%# GetAssetTypeName(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "asset_type_id")))%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Size: <%# FormatImageSize (DataBinder.Eval (Container.DataItem, "file_size")) %></span>
								<br /><span class="insideTextNoBold">Tags:</span> <asp:label id="tags" runat="server" Text='<%# GetAssetTags (DataBinder.Eval(Container.DataItem, "keywords").ToString ())%>'/></span>
							</td>
							<td valign="top" class="intColor1"></td>
							
							<td class="intBorderC1BorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
		  				</tr>
					</ItemTemplate>
					<AlternatingItemTemplate>
						<tr class="intColor2">
							<td height="125" class="intBorderC1BorderLeftColor1"><img runat="server" src="~/images/spacer.gif" width="1" height="110" /></td>
							<td align="center" class="intColor2">
								<asp:checkbox id="chkEdit" CssClass="Filter2" runat="server" />
								<asp:hyperlink id="hlDelete" runat="server" ImageURL="~/images/widgets/widget_delete_bt.gif" NavigateURL='<%#GetDeleteTagScript ((int) DataBinder.Eval(Container.DataItem, "asset_id")) %>' ToolTip="Remove Connected Media"/>
							
								<input type="hidden" runat="server" id="hidAssetItemId" value='<%#DataBinder.Eval(Container.DataItem, "asset_id")%>' NAME="Hidden1"> 
							</td>
							<td width="1" class="sepInt1"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
							<td align="center" class="intColor2">
								<!--image holder-->
								<table border="0" cellpadding="0" cellspacing="0" width="94" height="94" align="center">
									<tr>
										<td class="boxWhiteTopLeft"><img runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
										<td class="boxWhiteTop" ><img runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
										<td class="boxWhiteTopRight"><img runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
									</tr>
									<tr>
										<td class="boxWhiteLeft" ><img runat="server" src="~/images/spacer.gif" width="1" height="4" /></td>
										<td class="boxWhiteContent" height="80" align="center" valign="middle">
											<a href='<%# GetAssetDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "asset_id")))%>'><img border="0" style="margin: 10px;" src='<%#GetMediaImageURL (DataBinder.Eval(Container.DataItem, "thumbnail_medium_path").ToString () ,"me", Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_id")), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_type_id")), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "thumbnail_gen")))%>' border="0"/></a>
										</td>
										<td class="boxWhiteRight"><img runat="server" src="~/images/spacer.gif" width="1" height="4"/></td>
									</tr>
									<tr>
										<td class="boxWhiteBottomLeft" ><img runat="server" src="~/images/spacer.gif" width="4" height="4" /></td>
										<td class="boxWhiteBottom"><img runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
										<td class="boxWhiteBottomRight"><img runat="server" src="~/images/spacer.gif" width="4" height="4" /></td>
									</tr>
								</table>
								<!--end image holder-->			   
							</td>
							<td width="1" class="sepInt1"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
							<td valign="top" class="intColor2"><br /></td>
							<td valign="middle" class="intColor2" align="left">
								<span class="insideTextNoBold1"><a class="insideTextNoBold1" href='<%# GetAssetDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "asset_id")))%>'><%# TruncateWithEllipsis (Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "name").ToString ()), 40) %></a>&nbsp;&nbsp;&nbsp;</span><span class="insideTextNoBold">
								<br /><br /></span><span class="insideTextNoBold">&nbsp;<br /><br />Type: <%# GetAssetTypeName(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "asset_type_id")))%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Size: <%# FormatImageSize (DataBinder.Eval (Container.DataItem, "file_size")) %></span>
								<br /><br /><span class="insideTextNoBold">Tags:</span> <asp:label id="tags" runat="server" Text='<%# GetAssetTags (DataBinder.Eval(Container.DataItem, "keywords").ToString ())%>'/>
							</td>
							<td valign="top" class="intColor2"></td>
							<td valign="top" class="intColor2"><img runat="server" src="~/images/spacer.gif" width="1" height="28"><span class="insideTextNoBold"> Owner: <a class="insideTextNoBold" href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "username").ToString ())%>><%# DataBinder.Eval(Container.DataItem, "username") %></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
							<td class="intBorderC1BorderRightColor2"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
		  				</tr>
					</AlternatingItemTemplate>
				</asp:Repeater>
				
				<!-- END PLACE HOLDERS -->
				<!-- PLACE HOLDERS -->
				<tr>
					<td height="7" class="intBorderC1BottomLeft"></td>
					<td colspan="7" class="intBorderC1Bottom"></td>
					<td class="intBorderC1Bottom"></td>
					<td class="intBorderC1BottomRight"></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" bgcolor="#F1F1F2" align="right"><img runat="server" src="~/images/spacer.gif" width="1" height="10"></td>
	</tr>
	<tr>
		<td class="frBottomLeft"></td>
		<td colspan="3" class="frBottom"></td>
		<td class="frBottomRight"></td>
	</tr>
</table>

<input type="hidden" runat="server" id="hidAssetId" value="0" NAME="hidAssetId"> 
<asp:button ID="btnDeleteAssetTag" OnClick="Delete_AssetTag" runat="server" Visible="false"></asp:button>
