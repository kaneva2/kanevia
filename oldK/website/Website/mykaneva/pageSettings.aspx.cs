///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva.channel;
using KlausEnt.KEP.Kaneva.framework.widgets;
using log4net;
using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for pageSettings.
	/// </summary>
	public class pageSettings : MainTemplatePage
	{
		//protected channelNav		rnNavigation;	
		protected int	_pageId;
		protected int	_channelId;
		protected System.Web.UI.WebControls.Button btnCancelCustomize;
		protected System.Web.UI.WebControls.Button btnUpdateCustomize;

		private bool				_isPersonal;

		protected pageSettings () 
		{
			Title = "Select your theme";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			if(Request.IsAuthenticated)
			{
				if(GetRequestParams())
				{
					//check if user is allowed to edit this channel
					if(!IsUserAllowedToEdit())
					{
						RedirectToHomePage ();
					}
				}
				else
				{
					//invalid request params
					//if the user logged in, send him to his home page
					//if not, redirect to login page
					RedirectToHomePage ();
				}
			}
			else
			{
				Response.Redirect (this.GetLoginURL ());
			}

			
			_isPersonal = CommunityUtility.IsChannelPersonal(_channelId);

			//setup header nav bar
			if(_channelId == this.GetPersonalChannelId())
			{
				//user's own channel
				HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.PROFILE;
			}
			else
			{
				HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.CHANNELS;
			}

			HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
			
			HeaderNav.MyChannelsNav.PageId = _pageId;
			HeaderNav.MyChannelsNav.ChannelId = _channelId;
			HeaderNav.MyChannelsNav.ActiveTab = NavMyChannels.TAB.DESIGN;
			
			HeaderNav.SetNavVisible(HeaderNav.MyKanevaNav,2);
			HeaderNav.SetNavVisible(HeaderNav.MyChannelsNav);

			
			if (!IsPostBack)
			{
				BindThemeData (1);
				LoadThemeCustomizeData ();
			}

			// Initialize
			InitThemeSelection ();
			InitThemeCustomize ();

			// Enable Max Lengths of TextAreas
			txtCustomCSS.Attributes.Add ("MaxLength", "6000");
            if (!ClientScript.IsClientScriptBlockRegistered (GetType(), "taMaxLength"))
			{
				string strScript = "<script language=JavaScript> addEvent(window, \"load\", textAreasInit);</script>";
                ClientScript.RegisterClientScriptBlock(GetType(), "taMaxLength", strScript);
			}
		}

		private bool GetRequestParams()
		{
			bool retVal = true;
			try
			{
				if(Request ["pageId"] == null || Request ["pageId"] == ""
					|| Request ["pageId"] == "0")
				{
					if(Request ["communityId"] != null)
					{
						//get channelId and find the default page
						_channelId = Convert.ToInt32 (Request ["communityId"]);
						if(CommunityUtility.IsCommunityValid(_channelId))
						{
							_pageId = PageUtility.GetChannelDefaultPageId(_channelId);
						}
						else
						{
							retVal = false;
						}
					}
					else
					{
						//no channel id passed, get personal channelId if user logged in
						if(Request.IsAuthenticated)
						{
							_channelId = GetPersonalChannelId ();
							_pageId = PageUtility.GetChannelDefaultPageId(_channelId);
						}
						else
						{
							retVal = false;
						}
					}
				}
				else
				{
					_pageId = Convert.ToInt32 (Request ["pageId"]);
					DataRow drChannel = PageUtility.GetChannelByPageId(_pageId);
					_channelId = Convert.ToInt32(drChannel["community_id"]);
				}
			}
			catch(Exception)
			{
				//invalid numbers
				retVal = false;
			}
			return retVal;
		}

		/// <summary>
		/// returns true if current user is allowed to edit this channel
		/// </summary>
		/// <returns></returns>
		private bool IsUserAllowedToEdit()
		{
			return IsAdministrator() || CommunityUtility.IsCommunityModerator(this._channelId, GetUserId());
		}

		// ****************************************
		// Theme Selection
		// ****************************************
		#region Theme Selection

		/// <summary>
		/// InitThemeSelection
		/// </summary>
		private void InitThemeSelection ()
		{
			const int defaultTemplate = 1;
			DataRow drChannel = CommunityUtility.GetCommunity(_channelId);
			int templateId = Convert.ToInt32 (drChannel ["template_id"]);
			int standardTemplateId = defaultTemplate;

			if (templateId > 0)
			{
				DataRow drTemplate = WidgetUtility.GetTemplate (templateId);
				if (drTemplate != null)
				{
					standardTemplateId = Convert.ToInt32 (drTemplate ["standard_template_id"]);
				}
			}

			// Show the preview image
			DataRow drStandardTemplate = WidgetUtility.GetStandardTemplate (standardTemplateId);
			if (drStandardTemplate != null)
			{
				imgThemePreview.Src = ResolveUrl (drStandardTemplate ["preview_url"].ToString ());
			}

			// Check the current one
			string scriptString = "<script language=JavaScript>";
			scriptString += "function setChecked(){setCheckedValue ($('rbTheme'), " + standardTemplateId + ");}\n";
			scriptString += "window.onload=setChecked";
			scriptString += "</script>";

            if (!ClientScript.IsClientScriptBlockRegistered(GetType(), "SetChecked"))
			{
                ClientScript.RegisterStartupScript(GetType(), "SetChecked", scriptString);
			}
		}

		protected string GetPreviewJavascript (string previewURL)
		{
			return "javascript:document.frmMain." + imgThemePreview.ClientID + ".src='" + ResolveUrl (previewURL) + "'";
		}

		/// <summary>
		/// Bind the data
		/// </summary>
		/// <param name="pageNumber"></param>
		private void BindThemeData (int pageNumber)
		{
			int themesPerPage = Int32.MaxValue;

			string orderby = "standard_template_id" + " " + "ASC";

			PagedDataTable pds = WidgetUtility.GetStandardTemplates (orderby, pageNumber, themesPerPage);
			dlThemes.DataSource = pds;
			dlThemes.DataBind ();

//			pgTop.NumberOfPages = Math.Ceiling ((double) pds.TotalCount / themesPerPage).ToString ();
//			pgTop.DrawControl ();
//
//			// The results
//			lblSearch.Text = GetResultsText (pds.TotalCount, pageNumber, themesPerPage, pds.Rows.Count);
		}

		/// <summary>
		/// btnUpdate_Click
		/// </summary>
		protected void btnUpdate_Click (object sender, EventArgs e) 
		{
			if (!Page.IsValid) 
			{
				return;
			}

			int userId = GetUserId ();
			int standardTemplateId = 0;

			if (Request ["rbTheme"] != null)
			{
				if (KanevaGlobals.IsNumeric (Request ["rbTheme"].ToString ()))
				{
					standardTemplateId = Convert.ToInt32 (Server.HtmlEncode (Request ["rbTheme"].ToString ()));
				}
			}
			
			if (standardTemplateId > 0)		 
			{
				CommunityUtility.UpdateUserStandardTemplate (_channelId, standardTemplateId);
			}

			// Navigate back to the correct page
			DataRow drChannel = CommunityUtility.GetCommunity (_channelId);
			if(_isPersonal)
			{
				Response.Redirect( KanevaGlobals.GetPersonalChannelUrl ( drChannel ["name_no_spaces"].ToString ()) + "?pageId=" + _pageId );
			}
			else
			{
				Response.Redirect( KanevaGlobals.GetBroadcastChannelUrl (drChannel ["name_no_spaces"].ToString ()) + "?pageId=" + _pageId );
			}
		}

		#endregion

		// ****************************************
		// Theme Customizing
		// ****************************************
		#region Theme Customizing

		/// <summary>
		/// InitThemeCustomize
		/// </summary>
		private void InitThemeCustomize ()
		{
			// General settings
			txtFontColor.Attributes.Add("onchange", "relateColor('" + txtFontColorAppClrPickerStr.ClientID +"',this.value);");
			txtFontColorAppClrPickerStr.NavigateUrl = "javascript:pickColor('" + txtFontColorAppClrPickerStr.ClientID + "');";
	
			txtHyperlinkColor.Attributes.Add("onchange", "relateColor('" + txtHyperlinkColorAppClrPickerStr.ClientID +"',this.value);");
			txtHyperlinkColorAppClrPickerStr.NavigateUrl = "javascript:pickColor('" + txtHyperlinkColorAppClrPickerStr.ClientID + "');";

			txtHyperlinkHoverColor.Attributes.Add("onchange", "relateColor('" + txtHyperlinkHoverColorAppClrPickerStr.ClientID +"',this.value);");
			txtHyperlinkHoverColorAppClrPickerStr.NavigateUrl = "javascript:pickColor('" + txtHyperlinkHoverColorAppClrPickerStr.ClientID + "');";
	
			txtHyperlinkVisitedColor.Attributes.Add("onchange", "relateColor('" + txtHyperlinkVisitedColorAppClrPickerStr.ClientID +"',this.value);");
			txtHyperlinkVisitedColorAppClrPickerStr.NavigateUrl = "javascript:pickColor('" + txtHyperlinkVisitedColorAppClrPickerStr.ClientID + "');";

            if (!ClientScript.IsClientScriptBlockRegistered(GetType(), "setfontcolor"))
			{
                ClientScript.RegisterStartupScript(GetType(), "setfontcolor", "<script>relateColor('" + 
					txtFontColorAppClrPickerStr.ClientID + "',$('" + txtFontColor.ClientID+ "').value);</script>");
			}

            if (!ClientScript.IsClientScriptBlockRegistered(GetType(), "setHyperLinkcolor"))
			{
                ClientScript.RegisterStartupScript(GetType(), "setHyperLinkcolor", "<script>relateColor('" + 
					txtHyperlinkColorAppClrPickerStr.ClientID + "',$('" + txtHyperlinkColor.ClientID+ "').value);</script>");
			}

            if (!ClientScript.IsClientScriptBlockRegistered(GetType(), "sethhovercolor"))
			{
                ClientScript.RegisterStartupScript(GetType(), "sethhovercolor", "<script>relateColor('" + 
					txtHyperlinkHoverColorAppClrPickerStr.ClientID + "',$('" + txtHyperlinkHoverColor.ClientID+ "').value);</script>");
			}

            if (!ClientScript.IsClientScriptBlockRegistered(GetType(), "setvisitedLinkcolor"))
			{
                ClientScript.RegisterStartupScript(GetType(), "setvisitedLinkcolor", "<script>relateColor('" + 
					txtHyperlinkVisitedColorAppClrPickerStr.ClientID + "',$('" + txtHyperlinkVisitedColor.ClientID+ "').value);</script>");
			}

			// Background
			txtBackgroundColor.Attributes.Add("onchange", "relateColor('" + txtBackgroundColorAppClrPickerStr.ClientID +"',this.value);");
			txtBackgroundColorAppClrPickerStr.NavigateUrl = "javascript:pickColor('" + txtBackgroundColorAppClrPickerStr.ClientID + "');";

            if (!ClientScript.IsClientScriptBlockRegistered(GetType(), "setbgcolor"))
			{
                ClientScript.RegisterStartupScript(GetType(), "setbgcolor", "<script>relateColor('" + 
					txtBackgroundColorAppClrPickerStr.ClientID + "',$('" + txtBackgroundColor.ClientID+ "').value);</script>");
			}			

			// Browse and remove links
			aRemove.Attributes.Add ("onclick", "javascript:$('" + txtBGPicture.ClientID + "').value=''");
			//aBrowse

			aWidRemove.Attributes.Add ("onclick", "javascript:$('" + txtModulePicture.ClientID + "').value=''");
			//aWidBrowse
			
			// Modules
			txtModuleHeaderColor.Attributes.Add("onchange", "relateColor('" + txtModuleHeaderColorAppClrPickerStr.ClientID +"',this.value);");
			txtModuleHeaderColorAppClrPickerStr.NavigateUrl = "javascript:pickColor('" + txtModuleHeaderColorAppClrPickerStr.ClientID + "');";

            if (!ClientScript.IsClientScriptBlockRegistered(GetType(), "setmodheadbgcolor"))
			{
                ClientScript.RegisterStartupScript(GetType(), "setmodheadbgcolor", "<script>relateColor('" + 
					txtModuleHeaderColorAppClrPickerStr.ClientID + "',$('" + txtModuleHeaderColor.ClientID+ "').value);</script>");
			}

			txtModuleBgColor.Attributes.Add("onchange", "relateColor('" + txtModuleBgColorAppClrPickerStr.ClientID +"',this.value);");
			txtModuleBgColorAppClrPickerStr.NavigateUrl = "javascript:pickColor('" + txtModuleBgColorAppClrPickerStr.ClientID + "');";

            if (!ClientScript.IsClientScriptBlockRegistered(GetType(), "setmodbgcolor"))
			{
                ClientScript.RegisterStartupScript(GetType(), "setmodbgcolor", "<script>relateColor('" + 
					txtModuleBgColorAppClrPickerStr.ClientID + "',$('" + txtModuleBgColor.ClientID+ "').value);</script>");
			}


			txtModuleBorColor.Attributes.Add("onchange", "relateColor('" + txtModuleBorColorAppClrPickerStr.ClientID +"',this.value);");
			txtModuleBorColorAppClrPickerStr.NavigateUrl = "javascript:pickColor('" + txtModuleBorColorAppClrPickerStr.ClientID + "');";

            if (!ClientScript.IsClientScriptBlockRegistered(GetType(), "setmodborcolor"))
			{
                ClientScript.RegisterStartupScript(GetType(), "setmodborcolor", "<script>relateColor('" + 
					txtModuleBorColorAppClrPickerStr.ClientID + "',$('" + txtModuleBorColor.ClientID+ "').value);</script>");
			}

//			txtModuleOuterBorColor.Attributes.Add("onchange", "relateColor('" + txtModuleOuterBorColorAppClrPickerStr.ClientID +"',this.value);");
//			txtModuleOuterBorColorAppClrPickerStr.NavigateUrl = "javascript:pickColor('" + txtModuleOuterBorColorAppClrPickerStr.ClientID + "');";
//
            //			if (!ClientScript.IsClientScriptBlockRegistered ("setmodouterborcolor"))
//			{
            //				ClientScript.RegisterStartupScript("setmodouterborcolor", "<script>relateColor('" + 
//					txtModuleOuterBorColorAppClrPickerStr.ClientID + "',$('" + txtModuleOuterBorColorAppClrPickerStr.ClientID+ "').value);</script>");
//			}
		}

		/// <summary>
		/// LoadThemeCustomizeData
		/// </summary>
		private void LoadThemeCustomizeData ()
		{
			DataRow drChannel = CommunityUtility.GetCommunity(_channelId);
			int templateId = Convert.ToInt32 (drChannel ["template_id"]);

			// Load font dropdowns
			DataTable dtFonts = WebCache.GetFonts ();

			drpGeneralFont.DataTextField = "name";
			drpGeneralFont.DataValueField = "font_id";
			drpGeneralFont.DataSource = dtFonts;
			drpGeneralFont.DataBind ();

			drpGeneralFont.Items.Insert (0, new ListItem ("change...", "0"));

			// Load font sizes
			LoadFontSizes (drpGeneralFontSize);

			LoadDropDowns ();

			LoadResetFormJS ();

			// Get the correct standard template (theme)
			if (templateId.Equals (0))
			{
				return;
			}
			else
			{
				// Load any custimizations
				DataRow drTemplate = WidgetUtility.GetTemplate (templateId);

				if (drTemplate != null)
				{
					int customFlag = 0;
					
					// Base font
					if (!drTemplate ["base_font"].Equals (DBNull.Value))
					{
						customFlag = 1;

						DataRow drBaseFont = WebCache.GetFont (Convert.ToInt32 (drTemplate ["base_font"]));

						if (drBaseFont != null)
						{
							txtFontColor.Text = drBaseFont ["rgb"].ToString ();
							SetDropDownIndex (drpGeneralFont, drBaseFont ["font_name_id"].ToString ());
							SetDropDownIndex (drpGeneralFontSize, drBaseFont ["pixel_size"].ToString ());
						}
					}

					// Base Link
					if (!drTemplate ["base_link"].Equals (DBNull.Value))
					{
						customFlag = 1;

                        DataRow drBaseLink = WebCache.GetFont(Convert.ToInt32(drTemplate["base_link"]));

						if (drBaseLink != null)
						{
							txtHyperlinkColor.Text = drBaseLink ["rgb"].ToString ();
						}
					}

					// Base hover
					if (!drTemplate ["base_hover"].Equals (DBNull.Value))
					{
						customFlag = 1;

                        DataRow drBaseHover = WebCache.GetFont(Convert.ToInt32(drTemplate["base_hover"]));

						if (drBaseHover != null)
						{
							txtHyperlinkHoverColor.Text = drBaseHover ["rgb"].ToString ();
						}
					}

					// Base visited
					if (!drTemplate ["base_visited"].Equals (DBNull.Value))
					{
						customFlag = 1;

                        DataRow drBaseVisited = WebCache.GetFont(Convert.ToInt32(drTemplate["base_visited"]));

						if (drBaseVisited != null)
						{
							txtHyperlinkVisitedColor.Text = drBaseVisited ["rgb"].ToString ();
						}
					}

					// Background
					txtBackgroundColor.Text = drTemplate ["background_rgb"].ToString ();
					txtBGPicture.Text = Server.HtmlDecode (drTemplate ["background_picture"].ToString ());
					SetDropDownIndex (drpRepeat, drTemplate ["background_repeat"].ToString ());

					txtModuleHeaderColor.Text = drTemplate ["module_header_rgb"].ToString ();
					txtModuleBgColor.Text = drTemplate ["module_background_rgb"].ToString ();
					txtModulePicture.Text = Server.HtmlDecode (drTemplate ["module_picture"].ToString ());
					txtModuleBorColor.Text = drTemplate ["module_border_rgb"].ToString ();

					// Opacity
					SetDropDownIndex (drpOpacity, drTemplate ["opacity"].ToString ());

					// Custom CSS
					txtCustomCSS.Text = Server.HtmlDecode (drTemplate["custom_css"].ToString ());

					// Module Background
					if (
						!drTemplate ["module_header_rgb"].Equals (DBNull.Value) || 
						!drTemplate ["module_background_rgb"].Equals (DBNull.Value) ||
						!drTemplate ["module_picture"].Equals (DBNull.Value) ||
						!drTemplate ["module_border_rgb"].Equals (DBNull.Value) ||
						!drTemplate ["module_border_width"].Equals (DBNull.Value) ||
						!drTemplate ["module_border_style"].Equals (DBNull.Value) ||

						!drTemplate ["module_outer_border_rgb"].Equals (DBNull.Value) ||
						!drTemplate ["module_outer_border_width"].Equals (DBNull.Value) ||
						!drTemplate ["opacity"].Equals (DBNull.Value) ||
						!drTemplate ["custom_css"].Equals (DBNull.Value) 
						)
					{
						customFlag = 1;
					}

					if (customFlag == 1)
					{
						ClientScript.RegisterStartupScript(GetType (), "setmodborcolor", "<script>showAdvanced();</script>");
					}
				}				
			}			
		}

		/// <summary>
		/// LoadResetFormJS
		/// </summary>
		private void LoadResetFormJS ()
		{
			string scriptString = "<script language=JavaScript>function resetForm (){";

			scriptString += "$('" + txtFontColor.ClientID + "').value='';";
			scriptString += "$('" + txtHyperlinkColor.ClientID + "').value='';";
			scriptString += "$('" + txtHyperlinkHoverColor.ClientID + "').value='';";
			scriptString += "$('" + txtHyperlinkVisitedColor.ClientID + "').value='';";
			scriptString += "$('" + txtBackgroundColor.ClientID + "').value='';";
			scriptString += "$('" + txtBGPicture.ClientID + "').value='';";
			scriptString += "$('" + txtModuleHeaderColor.ClientID + "').value='';";
			scriptString += "$('" + txtModuleBorColor.ClientID + "').value='';";
			scriptString += "$('" + txtModuleBgColor.ClientID + "').value='';";
			scriptString += "$('" + txtModulePicture.ClientID + "').value='';";
			//scriptString += "$('" + txtModuleOuterBorColor.ClientID + "').value='';";

			scriptString += "$('" + drpGeneralFont.ClientID + "').selectedIndex=0;";
			scriptString += "$('" + drpGeneralFontSize.ClientID + "').selectedIndex=0;";

			scriptString += "$('" + drpRepeat.ClientID + "').selectedIndex=0;";
			//scriptString += "$('" + drpPixels.ClientID + "').selectedIndex=0;";
			//scriptString += "$('" + drpModBorderStyle.ClientID + "').selectedIndex=0;";
			//scriptString += "$('" + drpModuleOuterBorderPixels.ClientID + "').selectedIndex=0;";

			scriptString += "$('" + drpOpacity.ClientID + "').selectedIndex=0;";
			scriptString += "$('" + txtCustomCSS.ClientID + "').value='';";


			scriptString += "}</script>";

            if (!ClientScript.IsClientScriptBlockRegistered(GetType(), "resetForm"))
			{
                ClientScript.RegisterStartupScript(GetType(), "resetForm", scriptString);
			}
		}

		/// <summary>
		/// LoadFontSizes
		/// </summary>
		/// <param name="drp"></param>
		private void LoadFontSizes (DropDownList drp)
		{
			for (int i = 6; i < 21; i++)
			{
				drp.Items.Add (new ListItem (i + "px", i.ToString ()));
			}

			drp.Items.Insert (0, new ListItem ("change...", "0"));
		}

		/// <summary>
		/// LoadDropDowns
		/// </summary>
		private void LoadDropDowns ()
		{
			drpRepeat.DataTextField = "mode";
			drpRepeat.DataValueField = "mode_id";
			drpRepeat.DataSource = WebCache.GetBgRepeatModes ();
			drpRepeat.DataBind ();
			drpRepeat.Items.Insert (0, new ListItem ("change...", "0"));

//			// Modules
//			drpPixels.Items.Add (new ListItem ("change...", ""));
//			for (int i = 0; i < 6; i++)
//			{
//				drpPixels.Items.Add (new ListItem (i + "px", i.ToString () + "px"));
//			}
//
//			drpModBorderStyle.Items.Add (new ListItem ("change...", ""));
//			drpModBorderStyle.Items.Add (new ListItem ("dashed", "dashed"));
//			drpModBorderStyle.Items.Add (new ListItem ("dotted", "dotted"));
//			drpModBorderStyle.Items.Add (new ListItem ("double", "double"));
//			drpModBorderStyle.Items.Add (new ListItem ("groove", "groove"));
//			drpModBorderStyle.Items.Add (new ListItem ("inset", "inset"));
//			drpModBorderStyle.Items.Add (new ListItem ("none", "none"));
//			drpModBorderStyle.Items.Add (new ListItem ("outset", "outset"));
//			drpModBorderStyle.Items.Add (new ListItem ("ridge", "ridge"));
//			drpModBorderStyle.Items.Add (new ListItem ("solid", "solid"));
//			drpModBorderStyle.Items.Add (new ListItem ("window-inset", "window-inset"));
//
//			drpModuleOuterBorderPixels.Items.Add (new ListItem ("change...", ""));
//			for (int i = 1; i < 6; i++)
//			{
//				drpModuleOuterBorderPixels.Items.Add (new ListItem (i + "px", i.ToString ()));
//			}
		}


		/// <summary>
		/// btnUpdateThemeCustomize_Click
		/// </summary>
		protected void btnUpdateThemeCustomize_Click (object sender, EventArgs e) 
		{
			DataRow drChannel = CommunityUtility.GetCommunity(this._channelId);
			int templateId = Convert.ToInt32 (drChannel ["template_id"]);
			
			// If they don't have a template, insert one now
			if (templateId.Equals (0))
			{
				templateId = WidgetUtility.InsertTemplate (1);
				CommunityUtility.UpdateTemplate(_channelId, templateId);
			}

			DataRow drTemplate = WidgetUtility.GetTemplate (templateId);

			int baseFontId = 0;
			int hyperlinkFontId = 0;
			int hyperlinkHoverFontId = 0;
			int hyperlinkVisitedFontId = 0;
			int opacity = -1;
			string customCSS = "";

			if (drTemplate != null)
			{
				// Base font
				if (!drTemplate ["base_font"].Equals (DBNull.Value))
				{
                    DataRow drBaseFont = WebCache.GetFont(Convert.ToInt32(drTemplate["base_font"]));

					if (drBaseFont != null)
					{
						if (HasBaseFont ())
						{
							baseFontId = Convert.ToInt32 (drTemplate ["base_font"]);
							WidgetUtility.UpdateFont (baseFontId, Convert.ToInt32 (drpGeneralFont.SelectedValue), Convert.ToInt32 (drpGeneralFontSize.SelectedValue), Server.HtmlEncode (txtFontColor.Text));
						}
						else
						{
							// Delete the font
							WidgetUtility.DeleteFont (Convert.ToInt32 (drTemplate ["base_font"]));
							baseFontId = 0;
						}
					}
				}
				else
				{
					if (HasBaseFont ())
					{
						baseFontId = WidgetUtility.InsertFont (Convert.ToInt32 (drpGeneralFont.SelectedValue), Convert.ToInt32 (drpGeneralFontSize.SelectedValue), Server.HtmlEncode (txtFontColor.Text));
					}
				}
			}

			// Hyperlink color
			if (!drTemplate ["base_link"].Equals (DBNull.Value))
			{
                DataRow drBaseLink = WebCache.GetFont(Convert.ToInt32(drTemplate["base_link"]));

				if (drBaseLink != null)
				{
					if (txtHyperlinkColor.Text.Trim ().Length > 0)
					{
						hyperlinkFontId = Convert.ToInt32 (drTemplate ["base_link"]);
						WidgetUtility.UpdateFont (hyperlinkFontId, 0, 0, Server.HtmlEncode (txtHyperlinkColor.Text.Trim ()));
					}
					else
					{
						// Delete the font
						WidgetUtility.DeleteFont (Convert.ToInt32 (drTemplate ["base_link"]));
						hyperlinkFontId = 0;
					}
				}
			}
			else
			{
				if (txtHyperlinkColor.Text.Trim ().Length > 0)
				{
					hyperlinkFontId = WidgetUtility.InsertFont (0, 0, Server.HtmlEncode (txtHyperlinkColor.Text.Trim ()));
				}
			}
			
			// Hyperlink hover
			if (!drTemplate ["base_hover"].Equals (DBNull.Value))
			{
                DataRow drHoverFont = WebCache.GetFont(Convert.ToInt32(drTemplate["base_hover"]));

				if (drHoverFont != null)
				{
					if (txtHyperlinkHoverColor.Text.Trim ().Length > 0)
					{
						hyperlinkHoverFontId = Convert.ToInt32 (drTemplate ["base_hover"]);
						WidgetUtility.UpdateFont (hyperlinkHoverFontId, 0, 0, Server.HtmlEncode (txtHyperlinkHoverColor.Text.Trim ()));
					}
					else
					{
						// Delete the font
						WidgetUtility.DeleteFont (Convert.ToInt32 (drTemplate ["base_hover"]));
						hyperlinkHoverFontId = 0;
					}
				}
			}
			else
			{
				if (txtHyperlinkHoverColor.Text.Trim ().Length > 0)
				{
					hyperlinkHoverFontId = WidgetUtility.InsertFont (0, 0, Server.HtmlEncode (txtHyperlinkHoverColor.Text.Trim ()));
				}
			}

			// Hyperlink visted
			if (!drTemplate ["base_visited"].Equals (DBNull.Value))
			{
                DataRow drVistedFont = WebCache.GetFont(Convert.ToInt32(drTemplate["base_visited"]));

				if (drVistedFont != null)
				{
					if (txtHyperlinkVisitedColor.Text.Trim ().Length > 0)
					{
						hyperlinkVisitedFontId = Convert.ToInt32 (drTemplate ["base_visited"]);
						WidgetUtility.UpdateFont (hyperlinkVisitedFontId, 0, 0, Server.HtmlEncode (txtHyperlinkVisitedColor.Text.Trim ()));
					}
					else
					{
						// Delete the font
						WidgetUtility.DeleteFont (Convert.ToInt32 (drTemplate ["base_visited"]));
						hyperlinkVisitedFontId = 0;
					}
				}
			}
			else
			{
				if (txtHyperlinkVisitedColor.Text.Trim ().Length > 0)
				{
					hyperlinkVisitedFontId = WidgetUtility.InsertFont (0, 0, Server.HtmlEncode (txtHyperlinkVisitedColor.Text.Trim ()));
				}
			}

			if (!drpOpacity.SelectedValue.Equals (""))
			{
				opacity = Convert.ToInt32 (Server.HtmlEncode (drpOpacity.SelectedValue));
			}

			customCSS = Server.HtmlEncode (txtCustomCSS.Text.Trim ());

			// Background
			string backgroundRgb = Server.HtmlEncode (txtBackgroundColor.Text.Trim ());
			string backgroundPicture = Server.HtmlEncode (txtBGPicture.Text.Trim ());
			int backgroundRepeat = Convert.ToInt32 (Server.HtmlEncode (drpRepeat.SelectedValue));

			// Module
			string moduleHeaderRgb = Server.HtmlEncode (txtModuleHeaderColor.Text.Trim ());
			string moduleBackgroundColor = Server.HtmlEncode (txtModuleBgColor.Text.Trim ());
			string modulePicture = Server.HtmlEncode (txtModulePicture.Text.Trim ());
			string moduleBorderRgb = Server.HtmlEncode (txtModuleBorColor.Text.Trim ());
			string moduleOuterBorderRgb = ""; //Server.HtmlEncode (txtModuleOuterBorColor.Text.Trim ());
			string moduleBorderWidth = "";
			string moduleBorderStyle = "";
			string moduleOuterBorderWidth = "";

			// Update the template
			WidgetUtility.UpdateTemplate (templateId, baseFontId, hyperlinkFontId, hyperlinkHoverFontId, hyperlinkVisitedFontId,
				moduleBackgroundColor, moduleBorderRgb, moduleBorderWidth, moduleBorderStyle, moduleHeaderRgb,
				backgroundRgb, backgroundPicture, backgroundRepeat, modulePicture, moduleOuterBorderRgb, moduleOuterBorderWidth, 
				opacity, customCSS);
            
            // Blast it
			if (_isPersonal)
			{
                UserFacade userFacade = new UserFacade();
                User user = userFacade.GetUser(Convert.ToInt32(drChannel["creator_id"]));

				if(user.BlastPrivacyProfiles)
				{
                    BlastFacade blastFacade = new BlastFacade();
                    blastFacade.SendProfileUpdateBlast(user.UserId, user.Username, user.NameNoSpaces);
				}
			}

			// Update the theme they selected
			btnUpdate_Click (sender, e);

			// Navigate back to the correct page
			if(_isPersonal)
			{
				Response.Redirect( KanevaGlobals.GetPersonalChannelUrl ( drChannel ["name_no_spaces"].ToString ()) + "?pageId=" + _pageId );
			}
			else
			{
				Response.Redirect( KanevaGlobals.GetBroadcastChannelUrl (drChannel ["name_no_spaces"].ToString ()) + "?pageId=" + _pageId );
			}
		}

		/// <summary>
		/// Did they enter base font info
		/// </summary>
		private bool HasBaseFont ()
		{
			return (txtFontColor.Text.Length > 0 || !drpGeneralFont.SelectedValue.Equals ("0") || !drpGeneralFont.SelectedValue.Equals ("0"));
		}

		#endregion

        #region Declerations

        // Theme selection
		protected DataList dlThemes;
		protected HtmlImage imgThemePreview;

		// General page settings
		protected DropDownList drpGeneralFont, drpGeneralFontSize, drpOpacity;
		protected TextBox txtFontColor, txtHyperlinkColor, txtHyperlinkHoverColor, txtHyperlinkVisitedColor, txtCustomCSS;
		protected HyperLink txtFontColorAppClrPickerStr, txtHyperlinkColorAppClrPickerStr, txtHyperlinkHoverColorAppClrPickerStr, txtHyperlinkVisitedColorAppClrPickerStr;


		// Background
		protected TextBox txtBackgroundColor, txtBGPicture;
		protected HyperLink txtBackgroundColorAppClrPickerStr;
		protected DropDownList drpRepeat;
		protected HtmlAnchor aRemove, aBrowse;

		// Modules
		protected TextBox txtModuleHeaderColor, txtModuleBgColor, txtModuleBorColor, txtModulePicture;
		protected HyperLink txtModuleHeaderColorAppClrPickerStr, txtModuleBgColorAppClrPickerStr, txtModuleBorColorAppClrPickerStr;		
		//protected TextBox txtModuleOuterBorColor;
		//protected HyperLink txtModuleOuterBorColorAppClrPickerStr;
		//protected DropDownList drpPixels, drpModBorderStyle, drpModuleOuterBorderPixels;
		protected HtmlAnchor aWidRemove, aWidBrowse;
		protected System.Web.UI.WebControls.Button btnCancel;
		protected System.Web.UI.WebControls.Button btnUpdate;
		protected System.Web.UI.HtmlControls.HtmlImage imgPreview;
		protected System.Web.UI.HtmlControls.HtmlImage imgWidPreview;

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

        #endregion


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

	}
}
