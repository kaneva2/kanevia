///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using System.Web.Security;
using System.Security.Cryptography;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for settings.
	/// </summary>
	public class settings : MainTemplatePage
	{
		protected settings () 
		{
			Title = "My Account Preferences";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			// They must be logged in
			if (!Request.IsAuthenticated)
			{
				Response.Redirect (GetLoginURL ());
				return;
			}

			if (!IsPostBack)
			{
				OnSave = Request.Params["onSave"];
				LoadData ();
			}

			// Set Nav
			HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
			HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.ACCOUNT;
			HeaderNav.MyAccountNav.ActiveTab = NavAccount.TAB.PREFERENCES;
			HeaderNav.SetNavVisible(HeaderNav.MyKanevaNav,2);
            HeaderNav.SetNavVisible (HeaderNav.MyAccountNav, 3);
		}

		#region Helper Methods

		/// <summary>
		/// LoadData
		/// </summary>
		/// <param name="userid"></param>
		private void LoadData ()
		{
            User user = KanevaWebGlobals.CurrentUser;
            
            chkBrowse.Checked = user.BrowseAnonymously;
			chkOnlineStatus.Checked = user.ShowOnline;			

			chkNewsletter.Checked = user.Newsletter.Equals ("Y");
            chkBlogcomments.Checked = (Convert.ToInt32(user.NotifyBlogComments).Equals(1));
            chkUpdates.Checked = (Convert.ToInt32(user.ReceiveUpdates).Equals(1));
            chkProfile.Checked = (Convert.ToInt32(user.NotifyProfileComments).Equals(1));
            chkFriendMessage.Checked = (Convert.ToInt32(user.NotifyFriendMessages).Equals(1));
            chkAnyoneMessage.Checked = (Convert.ToInt32(user.NotifyAnyoneMessages).Equals(1));
            chkFriendRequest.Checked = (Convert.ToInt32(user.NotifyFriendRequests).Equals(1));
            chkNewFriends.Checked = user.NotifyNewFriends;
			chkBlastComments.Checked = user.NotificationPreference.NotifyBlastComments;
			chkMediaComments.Checked = user.NotificationPreference.NotifyMediaComments;
			chkShopComments.Checked = user.NotificationPreference.NotifyShopCommentsPurchases;
            chkEventInvites.Checked = user.NotificationPreference.NotifyEventInvites;
            chkFriendBirthdays.Checked = user.NotificationPreference.NotifyFriendBirthdays;
            chkWorldBlasts.Checked = user.NotificationPreference.NotifyWorldBlasts;
            chkWorldRequests.Checked = user.NotificationPreference.NotifyWorldRequests;

            // Country filter
            chkCountryFilter.Checked = user.Preference.FilterByCountry;

            if (chkFriendRequest.Checked == true || chkAnyoneMessage.Checked == true || chkFriendMessage.Checked == true || chkProfile.Checked == true || chkBlogcomments.Checked == true)
            {
                chkNotifications.Checked = true;
            }
			
            // Check for Always Purchase With settings
            if (KanevaWebGlobals.CurrentUser.Preference.AlwaysPurchaseWithCurrencyType == Constants.CURR_CREDITS)
            {
                chkAlwaysUseCredits.Checked = true;
                chkAlwaysUseRewards.Checked = false;
            }
            else if (KanevaWebGlobals.CurrentUser.Preference.AlwaysPurchaseWithCurrencyType == Constants.CURR_REWARDS)
            {
                chkAlwaysUseCredits.Checked = false;
                chkAlwaysUseRewards.Checked = true;
            }
		}

		#endregion
		
		#region Event Handlers
		/// <summary>
		/// btnUpdate_Click
		/// </summary>
		protected void btnUpdate_Click (object sender, EventArgs e) 
		{
			if (!Page.IsValid) 
			{
				return;
			}

            UserFacade userFacade = new UserFacade();
			int userId = GetUserId ();

			int friendsCanComment = (int) Constants.eUSER_COMMENT_SECURITY.ALLOWED;
			int everyoneCanComment = (int) Constants.eUSER_COMMENT_SECURITY.ALLOWED;

            // Verify no more that one Always Purchase With checkbox is checked
            if (chkAlwaysUseCredits.Checked && chkAlwaysUseRewards.Checked)
            {
                cvBlank.ErrorMessage = "Please select either Always Purchase with Credits or Always Purchase with Rewards.  You can not select both.";
                cvBlank.IsValid = false;
                valSum.Visible = true;
                return;
            }

            try
            {
				// following mature vars to retain compatability
				User user = KanevaWebGlobals.CurrentUser;
				int containsMature = user.MatureProfile ? 1 : 0;
				int showMature = user.ShowMature ? 1 : 0;
				
				userFacade.UpdateUser(userId, showMature, (chkBrowse.Checked) ? 1 : 0, (chkOnlineStatus.Checked) ? 1 : 0,
                    (chkNewsletter.Checked) ? "Y" : "N", (chkBlogcomments.Checked) ? 1 : 0, (chkProfile.Checked) ? 1 : 0, (chkFriendMessage.Checked) ? 1 : 0, (chkAnyoneMessage.Checked) ? 1 : 0,
                    (chkFriendRequest.Checked) ? 1 : 0, (chkNewFriends.Checked) ? 1 : 0, friendsCanComment, everyoneCanComment, containsMature, (chkUpdates.Checked) ? 1 : 0);				

                string payWithCurrency = GetAlwaysPayWithSetting ();

                // Did country filter or Always Pay With options change?
                if (!KanevaWebGlobals.CurrentUser.Preference.FilterByCountry.Equals (chkCountryFilter.Checked) ||
                    (!KanevaWebGlobals.CurrentUser.Preference.AlwaysPurchaseWithCurrencyType.Equals (payWithCurrency))
                    )
                {
                    KanevaWebGlobals.CurrentUser.Preference.UserId = userId;
                    KanevaWebGlobals.CurrentUser.Preference.FilterByCountry = chkCountryFilter.Checked;
                    KanevaWebGlobals.CurrentUser.Preference.AlwaysPurchaseWithCurrencyType = payWithCurrency;
                    userFacade.UpdateUserPreferences (KanevaWebGlobals.CurrentUser.Preference);
                }

				// Did notification prefs chage?
				if (!(KanevaWebGlobals.CurrentUser.NotificationPreference.NotifyBlastComments == chkBlastComments.Checked) ||
					!(KanevaWebGlobals.CurrentUser.NotificationPreference.NotifyMediaComments == chkMediaComments.Checked) ||
					!(KanevaWebGlobals.CurrentUser.NotificationPreference.NotifyShopCommentsPurchases == chkShopComments.Checked) ||
                    !(KanevaWebGlobals.CurrentUser.NotificationPreference.NotifyEventInvites == chkEventInvites.Checked) ||
					!(KanevaWebGlobals.CurrentUser.NotificationPreference.NotifyFriendBirthdays == chkFriendBirthdays.Checked) ||
					!(KanevaWebGlobals.CurrentUser.NotificationPreference.NotifyWorldBlasts == chkWorldBlasts.Checked) ||
                    !(KanevaWebGlobals.CurrentUser.NotificationPreference.NotifyWorldRequests == chkWorldRequests.Checked))
				{
					KanevaWebGlobals.CurrentUser.NotificationPreference.UserId = userId;
					KanevaWebGlobals.CurrentUser.NotificationPreference.NotifyBlastComments = (chkBlastComments.Checked ? true : false);
					KanevaWebGlobals.CurrentUser.NotificationPreference.NotifyMediaComments = (chkMediaComments.Checked ? true : false);
					KanevaWebGlobals.CurrentUser.NotificationPreference.NotifyShopCommentsPurchases = (chkShopComments.Checked ? true : false);
                    KanevaWebGlobals.CurrentUser.NotificationPreference.NotifyEventInvites = (chkEventInvites.Checked ? true : false);
                    KanevaWebGlobals.CurrentUser.NotificationPreference.NotifyFriendBirthdays = (chkFriendBirthdays.Checked ? true : false);
                    KanevaWebGlobals.CurrentUser.NotificationPreference.NotifyWorldBlasts = (chkWorldBlasts.Checked ? true : false);
                    KanevaWebGlobals.CurrentUser.NotificationPreference.NotifyWorldRequests = (chkWorldRequests.Checked ? true : false);
					
					userFacade.UpdateUserNotificationPreferences(KanevaWebGlobals.CurrentUser.NotificationPreference);
				}
            }
            catch (Exception ex)
            {
                cvBlank.ErrorMessage = "An error occurred while trying to save your preferences. " + ex;
                cvBlank.IsValid = false;
                valSum.CssClass = "errBox black";
                valSum.Visible = true;
                return;
            }

            // Show Successful Save
            cvBlank.ErrorMessage = "Preferences successfully saved.";
            cvBlank.IsValid = false;
            valSum.CssClass = "infoBox black";
            valSum.Visible = true;
		}

        protected void toggleFriendNotifications(object sender, EventArgs e)
        {            
            if (chkNotifications.Checked == true)
            {
                chkBlogcomments.Checked = true;
                chkProfile.Checked = true;
                chkAnyoneMessage.Checked = true;
                chkFriendMessage.Checked = true;
                chkFriendRequest.Checked = true;
                chkNewFriends.Checked = true;
            }
            else
            {
                chkBlogcomments.Checked = false;
                chkProfile.Checked = false;
                chkAnyoneMessage.Checked = false;
                chkFriendMessage.Checked = false;
                chkFriendRequest.Checked = false;
                chkNewFriends.Checked = false;
            }
        }
        
        private string GetAlwaysPayWithSetting ()
        {
            if (chkAlwaysUseRewards.Checked)
            {
                return Constants.CURR_REWARDS;
            }
            else if (chkAlwaysUseCredits.Checked)
            {
                return Constants.CURR_CREDITS;
            }
            else
            {
                return "NOTSET";
            }
        }

		/// <summary>
		/// btnUpdate_Click
		/// </summary>
		protected void btnCancel_Click (object sender, EventArgs e) 
		{
			LoadData ();
		}

		#endregion
		
		#region Properties
		public string OnSave
		{
			get
			{
				return ViewState["OnSave"] == null ? "" : ViewState["OnSave"].ToString();
			}
			set
			{
				ViewState["OnSave"] = value;
			}
		}
		#endregion

		#region Declarations		

        protected CheckBox	chkBrowse, chkOnlineStatus, chkNewsletter, chkBlogcomments, chkProfile, chkNotifications, chkUpdates;
		protected CheckBox	chkFriendMessage, chkAnyoneMessage,	chkFriendRequest, chkShowMature, chkContainsMature, chkNewFriends;
        protected CheckBox chkCountryFilter, chkAlwaysUseCredits, chkAlwaysUseRewards, chkBlastComments, chkMediaComments,
            chkShopComments, chkEventInvites, chkFriendBirthdays, chkWorldBlasts, chkWorldRequests;
        protected HtmlAnchor lnk_AccesPass;

		protected HtmlContainerControl divAdultSettings;

        protected ValidationSummary valSum;
        protected CustomValidator cvBlank;

		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion

	}
}
