<%@ Page language="c#" Codebehind="editPage.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.editPage" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>

<link href="../css/editWidgets.css" rel="stylesheet" type="text/css" />
<link href="../css/PageLayout/gallery.css" type="text/css" rel="stylesheet">
<link href="../css/PageLayout/tab_template_select.css" type="text/css" rel="stylesheet">
<link href="../css/PageLayout/interface.css" type="text/css" rel="stylesheet">
<link href="../css/PageLayout/formating.css" type="text/css" rel="stylesheet">
<link href="../css/PageLayout/channel_base.css" type="text/css" rel="stylesheet">
<link href="../css/PageLayout/sidebar.css" type="text/css" rel="stylesheet">
<link href="../css/PageLayout/toolbar.css" type="text/css" rel="stylesheet">
<link href="../css/PageLayout/ModuleBox.css" type="text/css" rel="stylesheet">
<link href="../css/PageLayout/ModuleBoxStyle.css" type="text/css" rel="stylesheet">
<link href="../css/kanevaSC.css" rel="stylesheet" type="text/css" />
<link href="../css/new.css" rel="stylesheet" type="text/css" />
<link href="../css/homepage_0507.css" rel="stylesheet" type="text/css" />


<script language="javascript" src="../jscript/EventHandler.js"></script>
<script language="javascript" src="../jscript/Division.js"></script>
<script language="javascript" src="../jscript/WidgetManager.js"></script>
<script language="javascript" src="../jscript/WidgetPage.js"></script>
<script language="javascript" src="../jscript/Widget.js"></script>
<script language="javascript" src="../jscript/Area.js"></script>
<script language="javascript" src="../jscript/PageDetail.js"></script>

<script language="javascript" src="../jscript/PageLayout/Tools.js"></script>

<script language="javascript" src="../jscript/prototype_old.js"></script>
<script language="javascript" src="../jscript/dw_tooltip/dw_event.js"></script>
<script language="javascript" src="../jscript/dw_tooltip/dw_viewport.js"></script>
<script language="javascript" src="../jscript/dw_tooltip/dw_tooltip.js"></script>
<script language="javascript" src="../jscript/PageLayout/PageManager.js"></script>
<script language="javascript" src="../jscript/rollover.js"></script>

<script type="text/javascript">

var start_pos, delta=0;
var dragging = false;

var table1_width = 0;
var table3_width = 0;

var min_size = 200;
var midpoint = 306; // 613 / 2

function StartDragging(Event)
{
	// ensure module dragging did not steal events
	document.onmouseup		= StopDragging;
	document.onmousemove	= MouseMove;
	
	if (!document.all)
	{
		start_pos = Event.screenX;
	}
	else
	{
		start_pos = event.clientX;
	}
	
	var width = $('area_2').style.width;
	width = width.substring(0, width.length-2); // remove the 'px'
	table1_width = eval(width);

	width = $('area_3').style.width;
	width = width.substring(0, width.length-2); // remove the 'px'
	table3_width = eval(width);
	
	$('area_2').style.cursor='e-resize';
	$('area_3').style.cursor='e-resize';
	
	delta = 0;
	dragging = true;
	return false;
}

function StopDragging(Event)
{
	$('area_2').style.cursor='';
	$('area_3').style.cursor='';
	
	dragging = false;
	return false;
}

function MouseMove(Event)
{
	if (dragging)
	{
		var cur_pos;

		if (!document.all)
		{
			cur_pos = Event.screenX;
		}
		else
		{
			cur_pos = event.clientX;
		}
		
		delta = cur_pos - start_pos;
		
		if ((table1_width + delta) < min_size)
			delta = min_size - table1_width;
			
		if ((table3_width - delta) < min_size)
			delta = table3_width - min_size;
			
		if (((table1_width + delta) < midpoint + 10) && ((table1_width + delta) >= midpoint - 10))
			delta = midpoint - table1_width;
		
		$('area_2').style.width = (table1_width + delta) + "px";
		$('area_3').style.width = (table3_width - delta) + "px";
		
		$('hfLeftColumnWidth').value = table1_width + delta;
	
		area = widget_page.GetArea(1);
		area.SetWidth(table1_width + delta);
		area.UpdateBoundaries();
		area.UpdateWidgets();
		
		area = widget_page.GetArea(2);
		area.SetWidth(table3_width - delta);
		area.UpdateBoundaries();
		area.UpdateWidgets();
	}
}

function InitColumnDragging()
{
    var dragEnable = true;
    
    <asp:literal id="litColumnSliderJs" runat="server"></asp:literal>
    
    if (dragEnable)
    {
	$('tdContentDivider').onmousedown = StartDragging;
	document.onmouseup		= StopDragging;
	document.onmousemove	= MouseMove;
	}
	// set the initial value of the left column width
	var width = $('area_2').style.width;
	width = width.substring(0, width.length-2); // remove the 'px'
	$('hfLeftColumnWidth').value = eval(width);

}

</script>


<table cellSpacing="0" cellPadding="0" width="990" align="center" border="0">
	<tr>
		<td><img id="Img6" height="14" runat="server" src="~/images/spacer.gif" width="1"></td>
	</tr>
	<tr>
		<td>
			<table cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td class="frTopLeft"></td>
					<td class="frTop"></td>
					<td class="frTopRight"></td>
				</tr>
				<tr>
					<td class="frBorderLeft"><img id="IMG7" height="1" runat="server" src="~/images/spacer.gif" width="1"></td>
					<td class="frBgIntMembers" vAlign="top">

						<div style="padding:5px 15px 5px 0;" align="right">
							<table border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td><img src="../images/spacer.gif" width="0" height="3"></td>
								</tr>
								<tr>
									<td  align="center" valign="middle" class="saveChangesBt">
										<ajax:ajaxpanel runat="server" id="ajpSaveTop">
										<asp:button id="btnSaveTop" runat="server" name="btnSaveTop" style="CURSOR:hand" class="transparentButton" onclick="btnSaveWidgets_Click"  bordercolor="Transparent" backcolor="Transparent" borderstyle="None" text="                     " borderwidth="0px" width="98px" height="19px"/>
										</ajax:ajaxpanel>
									</td>
								</tr>
								<tr>
									<td><img src="../images/spacer.gif" width="1" height="3"></td>
								</tr>
							</table>
						</div>
												 
						<table border="0" cellpadding="0" cellspacing="0" width="966">
							<tr>
								<td class="frTopLeft"></td>
								<td class="frTop"><img id="IMG10" height="1" runat="server" src="~/images/spacer.gif" width="1"></td>
								<td class="frTopRight"></td>
							</tr>
							<tr>
								<td bgcolor="#f1f1f2" class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" id="Img5" width="1" height="1" /></td>
								<td valign="top" bgcolor="#f1f1f2" class="frBgIntMembers">
									<table cellspacing="0" cellpadding="0" border="0" width="100%">
										<tr>
											<td colspan="8"><strong>Pages:</strong> You can add multiple pages to your site. To edit a specific page click on an icon below.<br /><br /></td>
										</tr>
										<tr>
											<td width="5"><img runat="server" src="~/images/spacer.gif" width="5" height="1"></td>
											<td width="126"><ajax:ajaxpanel id="ajpAdd" runat="server"><asp:imagebutton runat="server" id="btnAdd" imageurl="~/images/button_addapage.gif" width="126" height="23" border="0" /></ajax:ajaxpanel></td>
											<td width="12"><img runat="server" src="~/images/spacer.gif" width="7" height="1"></td>
											<td width="126"><asp:imagebutton runat="server" id="btnDelete" imageurl="~/images/button_deletepage.gif" width="126" height="23" border="0" /></td>
											<td width="12"><img runat="server" src="~/images/spacer.gif" width="7" height="1"></td>
											<td class="editChannelsInBoxLeft"></td>
											<td class="editChannelsInBoxBg">
												<ajax:ajaxpanel id="ajpPageList" runat="server">
												<asp:datalist id="dlPages" runat="server" repeatcolumns="10" enableviewstate="True" >										
													<headertemplate>
														<table cellspacing="2" cellpadding="0" border="0" width="400">
															<tr>
																<td width="7px"><img runat="server" src="~/images/spacer.gif" width="7" height="1"></td>
													</headertemplate>
													
													<itemtemplate>
														<td align="center" style="width:60px">
															<table cellspacing="0" cellpadding="0" border="0" style="width:50px">
																<tr>
																	<td align="center"><a href="<%#GetChangePageScript ((int) DataBinder.Eval(Container.DataItem, "page_id")) %>" ><img runat="server" src="../images/paper_white_ico.gif" id="pageIcon" name="pageIcon" width="13" height="18" border="0" onmouseover="rollover(this.name,'../images/paper_green_ico.gif',this.alt,this.src)" onmouseout="rollout(this.name,'../images/paper_white_ico.gif',this.alt,this.src)"></a></td>
																</tr>
																<tr>
																	<td nowrap class="editChannelText10" align="center"><asp:label id="lblTitle" runat="server" /></td>
																</tr>
															</table>
														</td>												
													</itemtemplate>
													
													<footertemplate>
																<td><img id="Img9" runat="server" src="~/images/spacer.gif" width="7" height="1"></td>
															</tr>
														</table>	
													</footertemplate> 
												</asp:datalist>	
												</ajax:ajaxpanel>		
											</td>
											<td class="editChannelsInBoxRight"></td>
										</tr>
									</table>
								</td>
								<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" id="Img8" width="1" height="1" /></td>
							</tr>
							<tr>
								<td class="frBottomLeft"></td>
								<td class="frBottom"></td>
								<td class="frBottomRight"></td>
							</tr>
						</table>				


<!--<div class="container" style="horizontal-align:center">

		 
<table border="0" cellspacing="0" cellpadding="0" class="filter2" >
	<tr>
	<td>
	<input name="Checkbox" type="Checkbox" id="cbSelectAll" testfield="Select All" runat="server">			
				<select id="ddlActionList" runat="server" class="filter2" >
					<option value="">Select action...</option>
					<option value="">----------------------------------------</option>
					<option value="delete">Delete</option>
				</select>
			<asp:Label id="lblNumberOfPages" runat="server" />
	</td>
	<td width="100%">
		<a href="#" onclick="Element.show('newPageRow'); widget_page.Update(); ">+ add page</a>
	</td>
	</tr>
</table>
		
<!-- <asp:DataList id="dlPageList" runat="server" EnableViewState="True" ShowFooter="False" Width="100%" cellpadding="2" cellspacing="2" border="0" RepeatColumns="4" RepeatDirection="Horizontal">
	<ItemTemplate>
		<table id="tblPage" runat="server" cellpadding="0" cellspacing="0" border="0" Width="100%">
			<tr>
				<td valign="top">
					<input name="chbPage" runat="server" type="checkbox" id="chbPage" pageId='<%#(DataBinder.Eval(Container.DataItem, "page_id"))%>'  /><br>
				</td>
				<td>
					<table id="tblPageData" runat="server" cellpadding="0" cellspacing="0" border="0" style="border:solid 1px #cccccc; width:150px; height: 100px; CURSOR: hand;" onClick='<%#GetChangePageScript ((int) DataBinder.Eval(Container.DataItem, "page_id")) %>' onMouseover="this.style.backgroundColor='#eeeeee';" onMouseout="this.style.backgroundColor='';" >
						<tr id="trPageRow" runat="server">
							<td valign="top" align="center">
								<img id="imgPadlock" alt="" align="bottom" src="~/images/PageLayout/PagesManage/unlock.gif" style="border-style:None;height:9px;width:7px;border-width:0px;" runat="server" />&nbsp;<asp:Label id="lblTitle" runat="server" class="pageName"/>
								<br>
								<b><asp:Label id="lblPermission" runat="server" CssClass="pagePermissions"/></b>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</ItemTemplate>
</asp:DataList>

<div id="newPageContainer" runat="server">
	<table id="newPageRow" runat="server" cellpadding="0" cellspacing="0" border="0" style="border:solid 1px #cccccc;display:none;" width="50%" >
		<tr>
			<td align="center"><img src="~/images/PageLayout/spacer.gif" width="45" height="1" border="0" style="display:block;" runat="server"/></td>
			<td>
				<table cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td>
							<table cellpadding="3" cellspacing="0" border="0">
								<tr>
									<td><b>Access:</b></td>
									<td>
										<asp:DropDownList id="ddlNewAccess" runat="server" name="ddlNewAccess"  class="Filter2" />
									</td>
								</tr>
								<tr>
									<td colspan="2">
										<asp:Button id="btnCreate" runat="server" name="btnCreate" text="  save  "  class="Filter2" />&nbsp;
										<button type="button" class="Filter2" onclick="Element.hide('newPageRow'); widget_page.Update();">&nbsp;cancel&nbsp;</button>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</div>
		

<asp:Button id="btnSaveManagePages" style="visibility:hidden;" runat="server" name="btnSaveManagePages" text="   save pages   " class="Filter2"/>
	
</div>-->

<!-- Manage pages end, page management begin -->
<center>		  
<div class="container">			

	<span id="defBusyDiv" style="BORDER-RIGHT: #400 1px solid; PADDING-RIGHT: 10px; BORDER-TOP: #400 1px solid; DISPLAY: none; PADDING-LEFT: 10px; FONT-SIZE: 8pt; RIGHT: 0px; PADDING-BOTTOM: 3px; MARGIN: 0px; BORDER-LEFT: #400 1px solid; WIDTH: 5.5em; COLOR: #fff; PADDING-TOP: 3px; BORDER-BOTTOM: #400 1px solid; FONT-FAMILY: Verdana; POSITION: absolute; HEIGHT: 1.6em; BACKGROUND-COLOR: darkred">Loading...</span>
	<div style="WIDTH: 100%">

		<div style="WIDTH: 100%;">
		
			<!-- Page Detail -->
			
			<!--table cellpadding="0" cellspacing="0" border="0" width="100%">
				<tr>
					<td id="tdPageEditPanel" runat="server" align="center">
						<table cellpadding="3" cellspacing="0" border="0">
							<tr>
								<td style="padding-left: 7px;"><b>Name:</b></td>
								<td >
									
								</td>
								<td style="padding-left: 7px;"><b><span id="lbPageAccess" runat="server">Access:</span></b></td>
								<td>
									
								</td>
								<td style="padding-left: 7px;"><b><span id="lbPageGroup" runat="server">Group:</span></b></td>
								<td>
									<asp:DropDownList id="ddlPageGroup" runat="server" name="ddlGroup" class="Filter2" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table-->
			
			<div id="formContainer" style="border:1px solid red; display=none; visibility:hidden;">
			<asp:DropDownList id="ddlPageAccess" runat="server" name="ddlAccess" class="Filter2" />
			</div>
			<!-- end Page Detail -->
		
			<table cellSpacing="0" cellPadding="0" width="100%" border="0" onSelectStart="return false">
				<tr>
					<td><IMG style="DISPLAY: block" height="8" src="~/images/PageLayout/spacer.gif" width="220" align="bottom" border="0" runat="server" ID="Img1"></td>
					<td><IMG style="DISPLAY: block" height="8" src="~/images/PageLayout/spacer.gif" width="5" align="bottom" border="0" runat="server" ID="Img2"></td>
				</tr>
				<tr style="padding-top:10px;">
					<td id="toolBarHolder" vAlign="top">
						
						<!-- ToolBar Begin -->
						<table id="modulesSideBarTBL" cellSpacing="0" cellPadding="0" width="100%" border="0" class="WidgetsBoxBg" style="background-image:url();">
							<tr>
								<td colspan="5">
									<table cellspacing="0" cellpadding="0" border="0">
										<tr>
											<td rowspan="2" class="WidgetsBoxTopLeftSC"></td>
											<td class="WidgetsBoxTopSC" width="208"><img src="../images/spacer.gif" width="1" height="1"></td>
											<td rowspan="2" class="WidgetsBoxTopRightSC"><img src="../images/spacer.gif" width="10" height="1"></td>
										</tr>
										<tr>
											<td class="WidgetsBoxBgTop" style="background-image:url();"></td>
										</tr>
									</table>
								</td>
							</tr>
							<!-- Part title -->
							<tr>
								<td class="WidgetsBoxLeftSC"></td>
								<td><img src="../images/spacer.gif" width="8" height="1"></td>
								<td height="33">
									<table cellspacing="0" cellpadding="0" border="0" width="100%">
										<tr>
											<td class="WidgetTitleSC" height="33"><nobr>Widgets Panel</nobr></td>
											<td valign="middle" align="right" width="100%"></td>
										</tr>
									</table>
								</td>
								<td><img src="../images/spacer.gif" width="4" height="1"></td>
								<td class="WidgetsBoxRightSC"><img src="../images/spacer.gif" width="1" height="1"></td>
							</tr>
							<!-- End of part title -->

							<tr>
								<td class="WidgetsBoxLeftSC"></td>
								<td><img src="../images/spacer.gif" width="8" height="1"></td>
								<td>
									<table cellspacing="0" cellpadding="0" border="0" width="202">								
										<asp:Repeater id="rpModuleGroup" runat="server">
											<ItemTemplate>
												<!-- Separator -->
												<tr>
													<td class="WidgetsiBoxTopLeftSC"></td>
													<td class="WidgetsiBoxTopSC"></td>
													<td class="WidgetsiBoxTopRightSC"></td>
												</tr>
												<!-- Separator end -->
												<tr>
													<td><img src="../images/boxes/leftBox.gif" border="0"></td>
													<td valign="top" align="left" class="bgColorBox">
														<table cellSpacing="0" cellPadding="0" width="100%" border="0" height="100%" class="bgline">
															<tr>
																<td noWrap width="142" class="WidgetiTitleSC" align="left">
																	<asp:Label id="lblGroupTitle" runat="server" /><%#(DataBinder.Eval(Container.DataItem, "title"))%>
																</td>
																<td align="right"><img id="imgOpenBtn" runat="server" style="BORDER-RIGHT: 0px; BORDER-TOP: 0px; BORDER-LEFT: 0px; WIDTH: 42px; CURSOR: pointer; BORDER-BOTTOM: 0px; HEIGHT: 22px" alt="open" src="~/images/PageLayout/ModulesSidebar/open_btnSC.gif" align="absMiddle">
																	<img  id="imgCloseBtn" runat="server" style="BORDER-RIGHT: 0px; BORDER-TOP: 0px; BORDER-LEFT: 0px; WIDTH: 42px; CURSOR: pointer; BORDER-BOTTOM: 0px; HEIGHT: 8px" alt="close" src="~/images/PageLayout/ModulesSidebar/close_btnSC.gif" align="absMiddle"></td>
															</tr>
														</table>
													</td>
													<td align=left><img src="../images/boxes/rightBox.gif" border="0"></td>
												</tr>
												<!-- Separator -->
												<tr>
													<td class="WidgetsiBoxBottomLeftSC"></td>
													<td class="WidgetsiBoxBottomSC"></td>
													<td class="WidgetsiBoxBottomRightSC"></td>
												</tr>
												<!-- Separator end -->
												<tr>
													<td align="left" colspan="3">
														<asp:Panel id="pnlModuleGroup" runat="server" align="left">
															<table cellSpacing="0" cellPadding="0" width="100%" border="0">
																<asp:Repeater id="rpModules" runat="server">
																	<HeaderTemplate>
																		<tr>
																			<td>
																				<table cellspacing="0" cellpadding="0" border="0">
																					<tr>
																						<td rowspan="2" class="widgetsI2BoxTopLeft"></td>
																						<td class="widgetsI2BoxTop" width="192"></td>
																						<td rowspan="2" class="widgetsI2BoxTopRight"></td>
																					</tr>
																					<tr>
																						<td class="widgetsI2BoxBgTop"></td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																	</HeaderTemplate>
																	
																	<SeparatorTemplate>
																		<tr>
																			<td><table cellpadding="0" cellspacing="0" border="0" width="198"><tr><td class="widgetsI2BoxBgSep"></td></tr></table></td>
																		</tr>
																	</SeparatorTemplate>
															
																	<ItemTemplate>
																		<tr style="height: 40px" valign="top">
																			<td id="tdModuleContainer" runat="server" align="left"></td>
																		</tr>
																	</ItemTemplate>
															
																	<FooterTemplate>
																		<tr>
																			<td>
																				<table cellspacing="0" cellpadding="0" border="0">
																					<tr>
																						<td rowspan="2" class="widgetsI2BoxBottomLeft"></td>
																						<td class="widgetsI2BoxBgBottom" width="192"></td>
																						<td rowspan="2" class="widgetsI2BoxBottomRight"></td>
																					</tr>
																					<tr>
																						<td class="widgetsI2BoxBottom"></td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																	</FooterTemplate>
																</asp:Repeater>
															</table>
														</asp:Panel>
													</td>
												</tr>
											</ItemTemplate>
									
											<SeparatorTemplate>
												<tr>
													<td><img src="../images/spacer.gif" width="1" height="3"/></td>
												</tr>
											</SeparatorTemplate>
									
											<FooterTemplate>
												<tr>
													<td><img src="../images/spacer.gif" width="1" height="7"/></td>
												</tr>
											</FooterTemplate>
										</asp:Repeater>
									</table>
								</td>
								<td><img src="../images/spacer.gif" width="4" height="1"></td>
								<td class="WidgetsBoxRightSC"></td>
							</tr>
							<!-- End of part title -->

							<!-- bottom part -->
							<tr>
								<td colspan="5">
									<table cellspacing="0" cellpadding="0" border="0">
										<tr>
										<td class="frBottomLeft"></td>
										<td class="frBottom" width="208"><img src="../images/spacer.gif" width="10" height="1"></td>
										<td class="frBottomRight"></td>
									</tr>
									</table>
								</td>
							</tr>
						</table>
						<br />
						
						<!-- ToolBar End -->
					</td>
					<td vAlign="top" align="right" width="100%">
						<!-- begin Widget Base template -->
						<div id="divWidgetBaseTemplate" runat="server" align="left">
							<!--
							<div style="position: relative; height: 40px; width: 198px; visibility:visible;">
								<table width="100%" height="40" border="0" cellspacing="0" cellpadding="0" class="widgetCellSC" align="left">
									<tr>
										<td class="widgetsI2BoxVertSC" width="2"><img height="1" width="2" src="~/images/PageLayout/spacer.gif" runat="server"></td>
										<td width="38" valign="middle" onmouseover="doTooltip(event,'%widget_description%')" onmouseout="hideTip();" align="center">
											<img src="%widget_icon_url%" border="0"/></td>
										<td width="95" class="WidgetiTitleSC" onmouseover="doTooltip(event,'%widget_description%')" onmouseout="hideTip();">
											<b>%widget_title%</b><br/></td>
										<td align="center" width="65">
											<a href="javascript:widget_page.AddWidget('%widget_type%')" class="nohover"><img width="46" height="22" alt="Activate" src="~/images/buttons/btnAdd.gif" border="0" runat="server" ID="btnAct"></a></td>
										<td class="widgetsI2BoxVertSC" width="2"><img height="1" width="2" src="~/images/PageLayout/spacer.gif" runat="server"></td>
									</tr>
								</table>
							</div>
							-->
						</div>
						<!-- end Widget Base template  -->
					
						<!-- begin Widget template -->
						<div id="divWidgetTemplate" runat="server">
						<!--
							<div id="%widget_id%" style="position: absolute; height: 53px; width: 180px; visibility:hidden;">
								<div id="%widget_id%titleBar" class="titleBarSC %move%" >
									<table cellspacing="0" cellpadding="0" border="0">
										<tr>
											<td id="titleCell1_%widget_id%" width="139" class="titleBarTitleSC">%widget_title%</td>
											<td id="titleCell2_%widget_id%" width="15" height="19" valign="middle"><a id="hl%widget_id%Edit" href="javascript:widget_page.EditWidget('%widget_id%')"><img src="~/images/PageLayout/Modules/top_edit_btnSC.gif" width="15" height="16" runat="server" border="0"/></a></td>
											<td id="titleCell3_%widget_id%" width="21" align="center" height="19" valign="middle"><a id="hl%widget_id%Delete" href="javascript:widget_page.RemoveWidget('%widget_id%')"><img src="~/images/PageLayout/Modules/top_close_btnSC.gif" width="15" height="16" runat="server" border="0"/></a></td>
										</tr>
									</table>
									</div>
								<div>
									<table border="0" cellspacing="0" cellpadding="0" class="modeMoveContentBgSC">
										<tr>
											<td class="modMoveContentLeftSC"></td>
											<td width="30" valign="middle" align="left">
												<img src="%widget_icon_url%" border="0"/></td>
											<td class="mod-textSC">
												%widget_description%<br/>
												<div id="hl%widget_id%Preview"></div>
											</td>
											<td class="modMoveContentRightSC"></td>
										</tr>
										<tr>
											<td class="modMoveContentLeftBottomSC"></td>
											<td class="modMoveContentBottomSC" colspan="2"></td>
											<td class="modMoveContentRightBottomSC"></td>
										</tr>
									</table>
								</div>
							</div>
						-->
						</div>
						<!-- end Widget template -->
					
						<div id="lyrGhostModuleBox" style="BORDER-RIGHT: black 1px solid; BORDER-TOP: black 1px solid; DISPLAY: none; BACKGROUND: red; FILTER: alpha(opacity=25); BORDER-LEFT: black 1px solid; CURSOR: move; BORDER-BOTTOM: black 1px solid; POSITION: absolute; opacity: 0.25"></div>
						<div id="lyrModuleSeparator" style="DISPLAY: none; WIDTH: 180px; POSITION: absolute; HEIGHT: 10px; TEXT-ALIGN: center"><IMG src="~/images/PageLayout/modules/seperator.gif" runat="server" ID="Img4"></div>
					
						
						<table cellpadding="0" cellspacing="0" border="0" width="736">
							<tr>
								<td valign="top">
								
									<div id="relativeContainer" style="margin-left:10px;height:1px;display:inline;width:736px;text-align:left;">
										<table cellpadding="0" cellspacing="0" border="0" width="736" align="center">
											<tr>
												<td>
													<table  border="0" cellpadding="0" cellspacing="0" width="100%">
														<tr>
															<td class="frTopLeft"></td>
															<td class="frTop"><img id="IMG11" height="1" runat="server" src="~/images/spacer.gif" width="1"></td>
															<td class="frTopRight"></td>
														</tr>
														<tr>
															<td bgcolor="#f1f1f2" class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
															<td valign="top" bgcolor="#f1f1f2" class="frBgIntMembers">
																<table cellspacing="0" cellpadding="0" border="0" width="100%" height="100%">
																	<tr>
																		<td class="WidgetTitleSC2" height="20" align="left" width="80"><strong>Now Editing</strong></td>
																		<td><img runat="server" src="~/images/spacer.gif" width="5" height="1" border="0" /></td>
																		<td width="160px">
																			<asp:textbox id="txtPageName" style="width:150px" runat="server" name="tbName" type="text" cssclass="formKanevaText" maxlength="50"/>
																		</td>
																		<td class="movContMainBoxText" width="100" align="right"><nobr>View Settings</nobr>&nbsp;&nbsp;</td>
																		<td class="dottedLineVert">&nbsp;</td>
																		<td><img runat="server" src="~/images/spacer.gif" width="3" height="1" border="0" /></td>
							
																		<td valign="middle"><input id="rbPublic" onclick="changeStatus('ddlPageAccess',0);" type="radio" runat="server" value="0"/><label>Anyone</label></td>
																		<td><img runat="server" src="~/images/spacer.gif" width="3" height="1" border="0" /></td>
																		<td valign="middle"><input id="rbPrivate" onclick="changeStatus('ddlPageAccess',2);" type="radio" runat="server" value="2" /><label>Only Me</label></td>
																		<td><img runat="server" src="~/images/spacer.gif" width="3" height="1" border="0" /></td>
																		<td valign="middle"><input id="rbFriendsOnly" onclick="changeStatus('ddlPageAccess',1);" type="radio" runat="server" value="1" />
																			<input id="rbMembersOnly" onclick="changeStatus('ddlPageAccess',3);" type="radio" runat="server" value="3"/><label id="lblFriendMemberOnly" runat="server">Friends Only</label></td>
																	</tr>
																	<tr><td colspan="11" align="right" style="padding-right:22px;"><label id="lblGroupName" runat="server">Friend Group:</label> <asp:dropdownlist style="width:100px" id="ddlNewGroup" runat="server" name="ddlNewGroup" class="Filter2" disabled /></td></tr>
																</table>
																<script language="javascript">
																	initValues($("ddlPageAccess").value);
																</script>
															</td>
															<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
														</tr>
														<tr>
															<td class="frBottomLeft"></td>
															<td class="frBottom"><img id="IMG12" height="1" runat="server" src="~/images/spacer.gif" width="1"></td>
															<td class="frBottomRight"></td>
														</tr>
													</table>				
												</td>
											</tr>
										</table>
									</div>
					
								</td>
							</tr>
							<tr><td><img id="IMG19" height="16" runat="server" src="~/images/spacer.gif" width="1"></td></tr>
							<tr>
								<td>
						
									<!-- begin relativeContainer -->
									<div id="relativeContainer" style="margin-left:10px;display:inline;height:1px;width:736px;text-align:left;">
										<table cellspacing="0" cellpadding="0" border="0" width="736" class="pageLayoutBg">
											<tr>
												<td colspan="5">
													<table cellspacing="0" cellpadding="0" border="0" width="100%">
														<tr>
															<td class="frTopLeft"></td>
															<td class="frTop"><img id="IMG17" height="1" runat="server" src="~/images/spacer.gif" width="1"></td>
															<td class="frTopRight"></td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td class="WidgetsBoxLeftSC"></td>
												<td><img src="../images/spacer.gif" width="10" height="1"></td>
												<td height="33">
													<table cellspacing="0" cellpadding="0" border="0" width="100%" height="100%">
														<tr>
															<td class="WidgetTitleSC" height="33" align="left"><nobr>Drag and Drop Layout</nobr></td>
														</tr>
													</table>
													<script language="javascript">
														initValues($("ddlPageAccess").value);
													</script>
												</td>
												<td><img src="../images/spacer.gif" width="10" height="1"></td>
												<td class="WidgetsBoxRightSC"></td>
											</tr>
											<tr>
												<td class="WidgetsBoxLeftSC"></td>
												<td><img src="../images/spacer.gif" width="10" height="1"></td>
												<td>
													<table id="chanContent" cellSpacing="0" cellPadding="0" border="0">
														<tr>
															<td class="movContMainBoxTopLeft"><img src="~/images/spacer.gif" width="7" height="7" runat="server" ID="Img13"></td>
															<td class="movContMainBoxTop"></td>
															<td class="movContMainBoxTopRight"><img src="~/images/spacer.gif" width="7" height="7" runat="server" ID="Img14"></td>
														</tr>
														<tr>
															<td class="movContMainBoxLeft"></td>
															<td class="HeaderZone">
																<div class="bodyZone" id="area_1" style="BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; BORDER-BOTTOM-STYLE: none">
																	<table cellspacing="0" cellpadding="0" border="0">
																		<tr>
																			<td class="movContMainBoxText">Header</td>
																		</tr>
																	</table>
																</div>
															</td>
															<td class="movContMainBoxRight"></td>
														</tr>
														<tr>
															<td class="movContMainBoxSep1" width="100%" colspan="3">
																<table cellspacing="0" cellpadding="0" border="0" width="100%" class="movContMainBoxSepT">
																	<tr>
																		<td align="center" width="342" class="movContMainBoxText">Left</td>
																		<td align="center" width="31" class="movContMainBoxText">Center</td>
																		<td align="right" width="150" class="movContMainBoxText" >Right</td>
																		<td align="right" class="movContMainBoxTextLt" >&nbsp;&nbsp;</td>
																	</tr>
																</table>
															</td>
														</tr>
														<tr class="bodyZone">
															<td class="movContMainBoxLeft"></td>
															<td width="694" align="center">
																<table cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; BORDER-BOTTOM-STYLE: none">
																	<tr>
																		<td id="tdContentLeft" align="center" runat="server" valign="top" style="border-left-style: none; border-top-style: none; border-bottom-style: none; border-right-style: none; text-align:center;">																		
																		</td>																		
																		<td class="movConDivider" id="tdContentDivider" valign="top" onmouseover="style.cursor='e-resize'" runat="server" ></td>
																		<td id="tdContentRight" runat="server" align="center" valign="top" style="border-top-style: none; border-bottom-style: none; border-right-style: none;">
																		
																		</td>
																	</tr>
																</table>
															</td>
															<td class="movContMainBoxRight"></td>
														</tr>
														<tr>
															<td class="movContMainBoxMidTopLeft"><img src="~/images/spacer.gif" width="7" height="7" runat="server" ID="Img15"></td>
															<td class="movContMainBoxMidTop"></td>
															<td class="movContMainBoxMidTopRight"><img src="~/images/spacer.gif" width="7" height="7" runat="server" ID="Img16"></td>
														</tr>
														<tr>
															<td class="movContMainBoxLeft"></td>
															<td>
																<div valign="bottom" class="bodyZone" id="area_5" style="BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; BORDER-BOTTOM-STYLE: none">
																	<table cellspacing="0" cellpadding="0" border="0">
																		<tr>
																			<td class="movContMainBoxText">Footer</td>
																		</tr>
																	</table>
																</div>
															</td>
															<td class="movContMainBoxRight"></td>
														</tr>
														<tr>
															<td class="movContMainBoxBottomLeft"></td>
															<td class="movContMainBoxBottom"></td>
															<td class="movContMainBoxBottomRight"></td>
														</tr>
														<tr>
															<td colspan="6" align="right"><div align="right">
																<table border="0" cellpadding="0" cellspacing="0">
																	<tr>
																		<td><img src="../images/spacer.gif" width="0" height="14"></td>
																	</tr>
																	<tr>
																		<td  align="center" valign="middle" class="saveChangesBt">
																			<ajax:ajaxpanel id="ajpSaveBottom" runat="server">
																			<asp:Button id="btnSaveBottom" runat="server" name="btnSaveBottom" style="CURSOR:hand" class="transparentButton" OnClick="btnSaveWidgets_Click"  BorderColor="Transparent" BackColor="Transparent" BorderStyle="None" text="                     " BorderWidth="0px" Width="98px" Height="19px"/>
																			</ajax:ajaxpanel>
																		</td>
																	</tr>
																	<tr>
																		<td><img src="../images/spacer.gif" width="1" height="7"></td>
																	</tr>
																</table>
															</div>	</td>
														</tr>
														<tr>
															<td vAlign="top" colSpan="3">
																<div id="area_4" style="VISIBILITY: hidden">Widgets to Remove</div>
															</td>
														</tr>
													</table>
												</td>
												<td><img src="../images/spacer.gif" width="10" height="1"></td>
												<td class="WidgetsBoxRightSC"></td>
											</tr>
											<tr>
												<td colspan="5">
													<table cellspacing="0" cellpadding="0" border="0" width="100%">
														<tr>
															<td class="frBottomLeft"></td>
															<td class="frBottom"><img id="IMG18" height="1" runat="server" src="~/images/spacer.gif" width="1"></td>
															<td class="frBottomRight"></td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
										<div id="moduleSourceDiv"></div>
									</div>
						
								</td>
							</tr>
						</table>
					
						<input id="hfSaveString" type="hidden" name="hfSaveString" runat="server" >
						<input id="hfInitialLayout" type="hidden" name="hfInitialLayout" runat="server" >
						<input id="hfLeftColumnWidth" type="hidden" name="hfLeftColumnWidth" runat="server" >
						<input id="hfInitialColumnWidth" type="hidden" name="hfInitialColumnWidth" runat="server" >
						<!-- end relativeContainer -->
					</td>
				</tr>
			</table>
		
		</div>
	</div>

</div>

</center>
 
 					<td class="frBorderRight">
						<img id="Img3" height="1" runat="server" src="~/images/spacer.gif" width="1">
					</td>
				</tr>
				<tr>
					<td class="frBottomLeft"></td>
					<td class="frBottom"></td>
					<td class="frBottomRight"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>

 
<br>
<input type="hidden" runat="server" id="hfChangePageID" value="0" NAME="hfChangePageID"> 
<asp:button ID="hbChangePage" OnClick="Change_Page" runat="server" Visible="false"></asp:button>
<input id="hfDefaultPageID" runat="server" type="hidden" name="hfDefaultPageID" />
<input id="hfDeletedPageID" runat="server" type="hidden" name="hfDeletedPageID" />
<input id="hfPositions" runat="server" type="hidden" name="hfPositions"  />
<script language="javascript" type="text/javascript">
<!--
	var WidgetManager = new WidgetManager( $('divWidgetBaseTemplate'), $('divWidgetTemplate'));
	
	var page_detail = new PageDetail();

//-->
</script>

<script language="javascript" type="text/javascript">
<!--
    function OpenCustomWindow(windowUrl, windowTitle, windowParams) 
    {
        NW=window.open(windowUrl, windowTitle, windowParams);
        if (NW)
            NW.focus();
    }
//-->
</script>
