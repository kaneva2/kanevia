<%@ Page language="c#" Codebehind="submitW9.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.submitW9" %>
<%@ Register TagPrefix="Kaneva" TagName="SalesNav" Src="salesNav.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="HeaderText" Src="../usercontrols/HeaderText.ascx" %>

<Kaneva:SalesNav runat="server" id="rnNavigation" SubTab="paymentInfo"/>

<table cellpadding="0" cellspacing="0" border="0" width="900" style="border: 1px solid #cccccc;">
<tr>
<td align="center">
	<center>
	
	 <table cellpadding="0" cellspacing="0" border="0" width="750">
		<tr>
			<td><img runat="server" src="~/images/spacer.gif" width="20" height="1"/></td>
			<td>
				<asp:ValidationSummary ShowMessageBox="False" ShowSummary="True" Class="formError" id="valSum" DisplayMode="BulletList" runat="server" HeaderText="Please correct the following errors:"/>
			</td>
			<td><img runat="server" src="~/images/spacer.gif" width="20" height="1"/>
			</td>
		</tr>
	</table>
	
	<BR/>
	<table cellpadding="1" cellspacing="0" border="0" width="730" ID="Table2" style="border: 1px solid #666666;">
		<tr>
			<td colspan="2" align="center" style="background-color: #868686;">
				<span class="header02"><b>Electronic W-9 Information</b></span>
			</td>
		</tr>
		<tr>
			<td align="right" valign="middle" class="filter2" nowrap="true">
			<font color="red">*</font>Name:&nbsp; <BR>(as reported on your income tax return)&nbsp; 
			</td>
			<td class="filter2">
				<asp:TextBox ID="txtName" class="Filter2" Width="250" MaxLength="200" runat="server"/><asp:RequiredFieldValidator ID="rftxtName" ControlToValidate="txtName" Text="*" ErrorMessage="Name is a required field." Display="Dynamic" runat="server"/>
			</td>
		</tr>	
		<tr>
			<td align="right" valign="middle" class="filter2" bgcolor="#dddddd">
			Business Name:&nbsp; <BR>(if different from name)&nbsp; 
			</td>
			<td bgcolor="#dddddd">
				<asp:TextBox ID="txtCompanyName" class="Filter2" Width="250" MaxLength="200" runat="server"/>
			</td>
		</tr>
		<tr>
			<td align="right" valign="middle" class="filter2">
			<font color="red">*</font>Address:&nbsp; 
			</td>
			<td>
				<asp:TextBox ID="txtAddress" class="Filter2" Width="200" MaxLength="100" runat="server"/><asp:RequiredFieldValidator ID="rftxtAddress" ControlToValidate="txtAddress" Text="*" ErrorMessage="Address is a required field." Display="Dynamic" runat="server"/>
			</td>
		</tr>
		<tr>
			<td align="right" valign="middle" class="filter2" bgcolor="#dddddd">
			Address 2:&nbsp; 
			</td>
			<td bgcolor="#dddddd">
				<asp:TextBox ID="txtAddress2" class="Filter2" Width="200" MaxLength="100" runat="server"/>
			</td>
		</tr>		
		<tr>
			<td align="right" valign="middle" class="filter2">
			<font color="red">*</font>City:&nbsp; 
			</td>
			<td>
				<asp:TextBox ID="txtCity" class="Filter2" Width="200" MaxLength="100" runat="server"/><asp:RequiredFieldValidator ID="rftxtCity" ControlToValidate="txtCity" Text="*" ErrorMessage="City is a required field." Display="Dynamic" runat="server"/>
			</td>
		</tr>
		<tr>
			<td align="right" valign="middle" class="filter2" bgcolor="#dddddd">
			<font color="red">*</font>State:&nbsp; 
			</td>
			<td bgcolor="#dddddd">
				<asp:TextBox ID="txtState" class="Filter2" Width="200" MaxLength="100" runat="server"/><asp:RequiredFieldValidator ID="rftxtState" ControlToValidate="txtState" Text="*" ErrorMessage="State is a required field." Display="Dynamic" runat="server"/>
			</td>
		</tr>							
		<tr>
			<td align="right" valign="middle" class="filter2">
			<font color="red">*</font>Postal Code:&nbsp;
			</td>
			<td>
				<asp:TextBox ID="txtPostalCode" class="Filter2" Width="200" MaxLength="100" runat="server"/><asp:RequiredFieldValidator ID="rftxtPostalCode" ControlToValidate="txtPostalCode" Text="*" ErrorMessage="Postal Code is a required field." Display="Dynamic" runat="server"/>
			</td>
		</tr>
		<tr>
			<td align="right" valign="middle" class="filter2"  bgcolor="#dddddd">
			<font color="red">*</font>Country:&nbsp;
			</td>
			<td bgcolor="#dddddd" class="filter2">
				<asp:Label runat="server" id="lblCountry"/>
			</td>
		</tr>
		<tr>
			<td align="right" valign="middle" class="filter2">
			<font color="red">*</font>Telephone:&nbsp;
			</td>
			<td>
				<asp:TextBox ID="txtTelephone" class="Filter2" Width="100" MaxLength="25" runat="server"/><asp:RequiredFieldValidator ID="rftxtTelephone" ControlToValidate="txtTelephone" Text="*" ErrorMessage="Telephone is a required field." Display="Dynamic" runat="server"/>
			</td>
		</tr>
		<tr>
			<td colspan="2" align="center" style="background-color: #868686;">
				<span class="header02"><b>Taxpayer Identification Number (TIN)</b></span>
			</td>
		</tr>
		<tr>
			<td align="left" class="bodyText" colspan="2" style="padding: 10px;">
				Enter your TIN in the appropriate box. The TIN provided must match the name given on Line1 to avoid backup withholding. 
				For individuals, this is your social security number (SSN). However, for a resident alien, sole proprietor, or disregarded
				entity, see the Part 1 instructions on <A href="http://www.irs.gov/pub/irs-pdf/fw9.pdf" style="COLOR: #3258ba; font-size: 12px;" target="MAIN">page 3</a>. For other entities, it is your employer identification number (EIN). If you
				do not have a number, see How to get a TIN on <A href="http://www.irs.gov/pub/irs-pdf/fw9.pdf" style="COLOR: #3258ba; font-size: 12px;" target="MAIN">page 3</a>.
			</td>
		</tr>
		<tr>
			<td align="right" valign="middle" class="filter2" bgcolor="#dddddd">
				Social Security Number
				<BR>-or-<img runat="server" src="~/images/spacer.gif" width="50" height="1"/><BR>
				Employer Identification Number 
			</td>
			<td bgcolor="#dddddd" class="filter2">
				 <asp:TextBox ID="txtSSN" class="Filter2" Width="100" MaxLength="25" runat="server"/>
				<BR><BR>
				<asp:TextBox ID="txtTIN" class="Filter2" Width="100" MaxLength="25" runat="server"/>
			</td>
		</tr>		
		
	</table>

	<table border="0" cellpadding="0" cellspacing="0" width="730" style="border: 1px solid #666666;">
		<tr>
			<td align="right" valign="middle" style="background-color: #868686;">
				<img runat="server" src="~/images/spacer.gif" width="100" height="1"/>
			</td>
			<td style="background-color: #868686;" width="380">&nbsp;&nbsp;<span class="header02"><b>Certification Statement</b></span>
			</td>
		</tr>
		<tr>
			<td align="left" class="bodyText" colspan="2" style="padding: 10px;">
				Under penalties of perjury, I certify that:<br/><br/>
				&nbsp;&nbsp;1. The number shown on this form is my correct taxpayer identification number (or I am waiting for a number to be issued to me), and<br/><br/>
				&nbsp;&nbsp;2. I am not subject to backup withholding because: (a) I am exempt from backup withholding, or (b) I have not been notified by the Internal 
				Revenue Service (IRS) that I am subject to backup withholding as a reult of a failure to report all interest or dividends, or (c) the IRS has
				notified me that I am no longer subject to backup withholding, and<br/><br/>
				&nbsp;&nbsp;3. I am a U.S. person (including a U.S. resident alien). 
			</td>
		</tr>
		<tr>
			<td class="bodyText" align="right" valign="top" colspan="2" style="padding: 10px;">
				<font color="red">*</font><B>I have read and make the declaration of this certification statement.</b>&nbsp;
				<asp:CheckBox runat="server" class="Filter2" ID="chkCertification"></asp:CheckBox>&nbsp;&nbsp;&nbsp;&nbsp;
			</td>
		</tr>
	</table>

	<table cellpadding="0" cellspacing="1" border="0" width="735">
		<tr>							
			<td align="right" class="bodyText"><br>
				<i style="color:red;">*By clicking the submit button you agree to the Certification Statement &nbsp;&nbsp;<asp:button id="btnUpdate" runat="Server" onClick="btnUpdate_Click" class="Filter2" Text=" submit " CausesValidation="False"/><br><br><br>
			</td>
		</tr>
	</table>	
	</center>

<br><br>
</td>
</tr>
</table><br><br>
