﻿<%@ Page Language="C#" ValidateRequest="false" MasterPageFile="~/masterpages/GenericNoBorder.Master" AutoEventWireup="true" CodeBehind="CreateContest.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.mykaneva.CreateContest" %>
<%@ Register TagPrefix="CE" Namespace="CuteEditor" Assembly="CuteEditor" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="cntHead" runat="server" contentplaceholderid="cphHeadData" >

    <NOSCRIPT><META HTTP-EQUIV="refresh" CONTENT="1" URL="noJavascript.aspx" /></NOSCRIPT>
    <link href="../css/contests.css?v2" type="text/css" rel="stylesheet" />
	<link href="../jscript/jquery-ui/css/ui-darkness/jquery-ui-1.10.0.custom.css" type="text/css" rel="stylesheet" />     

	<script type="text/javascript" src="../jscript/yahoo/yahoo-min.js"></script>  
	<script type="text/javascript" src="../jscript/yahoo/event-min.js" ></script>   
	<script type="text/javascript" src="../jscript/yahoo/dom-min.js" ></script>   
	<script type="text/javascript" src="../jscript/yahoo/calendar-min.js"></script>
	<script type="text/javascript" src="../jscript/colorbox/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="../jscript/jquery-ui/jquery-ui.min.js"></script>

	<script type="text/javascript"><!--
		var $j = jQuery.noConflict();
		
		$j(document).ready(function () {
			$j(window).bind("load", function () {
				<asp:literal id="litDocHeight" runat="server"></asp:literal>
			});

			$j(function() {
			    $j("#txtEndDate").datepicker({ minDate: 0, maxDate: +90 });
			    $j("#imgSelectEnd").click(function() {
			        $j("#txtEndDate").focus();
			    })
			});
		});	  

		function ValidateAmt(){$j("#txtAmount").val('');}

//--> </script> 
	<style type="text/css">
		body {background-image:none;}
		#wrapper {border:none;background-image:none;background-color:#fff;position:relative;width:auto;margin:0 auto;height:auto !important;padding-bottom:10px;}
	</style>

</asp:Content>

<asp:Content id="cntBody" runat="server" contentplaceholderid="cphBody" >

<asp:updatepanel visible="false" id="upCreate" childrenastriggers="true" runat="server" updatemode="conditional">
	<contenttemplate>
		<div id="createcontest">
			<div class="pagetitle">Create a Contest</div>  

			<div id="entryfields">
				 
				<table>
					<tr><td class="lbl"><asp:requiredfieldvalidator id="rfvTitle" runat="server" controltovalidate="txtTitle" setfocusonerror="true" display="Static" text="*" errormessage="Title is required. "></asp:requiredfieldvalidator> Title:</td>
						<td class="fields"><asp:textbox id="txtTitle" runat="server" maxlength="100"></asp:textbox></td></tr>
					<tr><td class="lbl desc""><asp:requiredfieldvalidator id="rfvDesc" runat="server" controltovalidate="txtDesc" setfocusonerror="true" display="Static" text="*" errormessage=" Description is required. "></asp:requiredfieldvalidator> Description:</td>
						<td class="fields">
							<CE:Editor id="txtDesc" style="margin:0;" AllowPasteHtml="True" HyperlinkTarget="_blank" EditorOnPaste="PasteText" usephysicalformattingtags="true"
								ShowBottomBar="False" EnableContextMenu="False" BackColor="#ededed" runat="server" enableobjectresizing="false" 
								enablestripscripttags="true" MaxTextLength="5000" MaxHTMLLength="5000" width="478px" EditorBodyStyle="font:normal 12px verdana;"  
								Height="190px" ShowHtmlMode="false" ConfigurationPath="~/CuteSoft_Client/CuteEditor/Configuration/AutoConfigure/contest.config" AutoConfigure="None" ></CE:Editor>
						</td></tr>
					<tr class="xtrapad"><td class="lbl" valign="top">Prize (Credits):</td>
						<td class="fields">
							<ul>
								<li><input id="rbAmt1" type="radio" runat="server" name="rbAmount" value="4000" onclick="ValidateAmt();" checked /><label>4,000</label></li>
								<li><input id="rbAmt2" type="radio" runat="server" name="rbAmount" value="6000" onclick="ValidateAmt();" /><label>6,000</label></li>
								<li><input id="rbAmt3" type="radio" runat="server" name="rbAmount" value="10000" onclick="ValidateAmt();" /><label>10,000</label></li>
								<li><input id="rbAmt4" type="radio" runat="server" name="rbAmount" value="50000" onclick="ValidateAmt();" /><label>50,000</label></li>
								<li><input id="rbAmt6" type="radio" runat="server" name="rbAmount" value="100000" onclick="ValidateAmt();" /><label>100,000</label></li>
								<li class="nomargin"><input id="rbAmt7" type="radio" runat="server" name="rbAmount" clientidmode="Static" value="0" /><label>Other</label></li>&nbsp;<input id="txtAmount" clientidmode="Static" maxlength="6" runat="server" type="text" class="prizeamt" onfocus="$j('#rbAmt7').prop('checked', true);" /><asp:rangevalidator id="rvAmount" runat="server" type="Integer" style="float:none;" validationgroup="Range" controltovalidate="txtAmount" minimumvalue="4000" maximumvalue="100000" text="*" errormessage=" Prize amount must be between 4000 and 100,000. "></asp:rangevalidator><asp:requiredfieldvalidator id="rfvOtherAmt" runat="server" validationgroup="Range" style="float:none;" controltovalidate="txtAmount" text="*" errormessage=" Prize must be between 4000 and 100,000. "></asp:requiredfieldvalidator>
							</ul>  
						</td></tr>
					<tr class="xtrapad"><td class="lbl"><asp:requiredfieldvalidator id="rfvEndDate" runat="server" controltovalidate="txtEndDate" setfocusonerror="true" display="Static" text="*" errormessage=" End Date is required. "></asp:requiredfieldvalidator> End Date:</td>
						<td class="fields">
							<input type="text" id="txtEndDate" class="endDate" clientidmode="Static" runat="server" TabIndex="5" />
							<img id="imgSelectEnd" name="imgSelectEnd" src="../images/blast/cal_16.gif"/>
							<p class="note">(maximum <asp:literal id="litMaxDays" runat="server"></asp:literal> days)</p>
						</td>
					</tr>																											
					<tr class="xtrapad"><td class="lbl"></td><td class="fields"><asp:checkbox id="cbBlastMyKaneva" runat="server" text="" checked="true"></asp:checkbox>Blast this on My Kaneva</td></tr>
				</table>

			</div>

			<div id="divMsg" class="msg" runat="server"><asp:validationsummary id="valSum" runat="server" displaymode="SingleParagraph" showsummary="true" /></div>
			<div id="buttons">
				<div class="btncontainer twobtn">
					<a class="btnxsmall grey left" id="aNext" runat="server" onserverclick="btnNext_Click">Next</a>
					<a class="btnxsmall grey" id="aCancel" href="javascript:void(0);" onclick="parent.$j.colorbox.close(); return false;">Cancel</a>
				</div>
			</div> 
		</div>
	</contenttemplate>
</asp:updatepanel>

<asp:updatepanel visible="false" id="upConfirm" childrenastriggers="true" runat="server" updatemode="conditional">
	<contenttemplate>
		<div id="previewContainer">
			<div class="pagetitle">Create a Contest</div>  
			<div id="subtitle">Contest Preview:</div>
			<div id="previewcontest">
			
				<div id="details">

					<!-- Contest Details -->
					<div class="contest">
						<div class="left">
							<div class="amount"><div class="value">
								<asp:literal id="litPrizeAmount" runat="server"></asp:literal></div>
								<div>CREDITS</div>
							</div>
							<div class="time">
								<div class="label">TIME<br />LEFT</div>
								<div class="remaining"><asp:literal id="litTimeRemaining" runat="server"></asp:literal></div>
							</div><asp:hiddenfield id="hidContestId" runat="server" />
							<div class="clearit">
								<a class="buttonnarrow" id="aFollow" runat="server"><span class="follow">Follow</span></a>
							</div>
						</div>
						<div class="middle">
							<div class="datacontainer">
								<div class="title"><a href="javascript:void(0);"><asp:literal id="litTitle" runat="server"></asp:literal></a></div>
								<div class="data">
									<div class="entries">0 Entries</div>
									<div class="votes">0 Votes</div>
								</div>
							</div>
							<div class="ownercontainer">
								<div class="framesize-xsmall">
									<div id="Div1" class="restricted" runat="server" visible='false'></div>
									<div class="frame">
										<div class="imgconstrain">
											<a href="" id="aThumb" runat="server">
												<img href="" id="imgThumb" runat="server" />
											</a>
										</div>	
									</div>
								</div>
								<div class="creator">Created by:<br /><span class="name"><asp:literal id="litCreator" runat="server"></asp:literal></span></div>
							</div>	
							<div class="desc"><asp:literal id="litDescription" runat="server"></asp:literal></div>
						</div>
					</div>
					<!-- End Contest Details -->

				</div>
			</div>
			<div id="divConfirmMsg" class="msg" runat="server"></div>
			<div id="contestCost" class="cost" runat="server" visible="false">Creating this Contest will cost <asp:literal id="litCreditAmount" runat="server"></asp:literal> Credits*</div>
			<div id="notEnoughCredits" class="cost notenough" runat="server">
				<span class="warning">Warning: Not Enough Credits!</span><br />
				<span>Contest costs Credits, <asp:literal id="litContestCost" runat="server"></asp:literal> you only have <asp:literal id="litUserCredits" runat="server"></asp:literal></span>
			</div>
			<div class="terms" id="divTerms" runat="server" visible="true">By clicking OK you agree to Kaneva's <a runat="server" href="~/overview/termsandconditions.aspx" target="_blank">Terms and Conditions</a></div>
			<div id="buttons">
				<div class="btncontainer twobtn">
					<a visible="false" class="buttonnarrow" id="aGetCredits" runat="server" href="~/mykaneva/buyCreditPackages.aspx" target="_blank" onclick="$('btnBack').click();"><span class="search">Get Credits</span></a>
					<a class="btnxsmall grey left" id="aSave" runat="server" onserverclick="btnSave_Click">OK</a>
					<a class="btnxsmall grey" id="aBack" runat="server" clientidmode="static" onserverclick="btnBack_Click">Edit</a>
				</div>
			</div>
			<div class="note">*Note: Contests with no entries will be refunded. Contests with few entries may be entitled to a refund.</div>
			<asp:button id="btnBack" runat="server" clientidmode="Static" onclick="btnBack_Click" style="visibility:hidden;height:1px;width:1px;" />
		</div>
	</contenttemplate>
</asp:updatepanel>

<asp:updatepanel visible="false" id="upNoAccess" runat="server" updatemode="conditional">
	<contenttemplate>
		<div id="noaccess" class="noaccess">
			<div class="heading">We're Sorry.</div>
			<div>
				<span id="spnValidEmail" runat="server" visible="false">You must have a valid email address</span>
				<span id="spnLevelToCreate" runat="server" visible="false">You must have a World Fame Level of <asp:literal id="litLevelToCreate" runat="server"></asp:literal> or higher</span>
				to Start a Contest</div><br />
			<div id="buttons">
				<div class="btncontainer">
					<a class="btnxsmall grey" id="btnOk" runat="server" href="javascript:void(0);" onclick="parent.$j.colorbox.close(); return false;">OK</a>
				</div>
			</div>
		</div>
	</contenttemplate>
</asp:updatepanel>

<style>
.ui-datepicker-next-hover {background:none;}
</style>
</asp:Content>
