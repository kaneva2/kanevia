///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Kaneva.BusinessLayer.Facade;
using KlausEnt.KEP.Kaneva.usercontrols;

namespace KlausEnt.KEP.Kaneva.mykaneva
{
    public partial class buyAccess : MainTemplatePage
    {
        protected buyAccess()
        {
            Title = "Purchase Access Pass";
        }

        private void Page_Load(object sender, System.EventArgs e)
        {
            if (!KanevaGlobals.EnableCheckout)
            {
                if (!IsAdministrator())
                {
                    Response.Redirect("~/error.aspx?error=" + (int)Constants.eERROR_TYPE.CHECKOUT_CLOSED);
                }
            }

            VerifyUserAccess();

            if (!IsPostBack)
            {
                DataTable dtCredits = StoreUtility.GetActivePromotions(Constants.CURR_KPOINT, 0, (int)Constants.ePROMOTION_TYPE.ACCESS);

                for (int i = 0; i < dtCredits.Rows.Count; i++)
                {
                    if (Convert.ToInt32(dtCredits.Rows[i]["kei_point_amount"]) > 0)
                    {
                        btnBuyAPPlus.CommandArgument = dtCredits.Rows[i]["promotion_id"].ToString();
                    }
                    else
                    {
                        btnBuyAP.CommandArgument = dtCredits.Rows[i]["promotion_id"].ToString();
                    }
                }

                spnMessage.InnerText = string.Empty;
            }

            // Set Nav
            HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;

            //setup wizard nav bar
            ucWizardNavBar.ActiveStep = CheckoutWizardNavBar.STEP.STEP_1;
        }

        #region Helper Methods

        private void VerifyUserAccess()
        {
            // They must be logged in
            if (!Request.IsAuthenticated)
            {
                Response.Redirect(GetLoginURL());
            }

            // User must be 18 or older
            if (!KanevaWebGlobals.CurrentUser.IsAdult)
            {
                Response.Redirect("~/mykaneva/underage.aspx");
            }

            // They must log into WOK at least 1 time
            if (!KanevaWebGlobals.CurrentUser.HasWOKAccount)
            {
                // Redirect to credits home page
                Response.Redirect("~/mykaneva/managecredits.aspx");
            }
        }

        protected string FormatCredits(UInt64 credits)
        {
            try
            {
                return credits.ToString("N0");
            }
            catch
            {
                return "";
            }
        }

        #endregion

        #region Event Handlers
        /// <summary>
        /// They want to purchase a point bucket
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnPurchase_Click(object sender, CommandEventArgs e)
        {
            VerifyUserAccess();
            
            int userId = GetUserId();

            
            // Check to see if user already has an access pass, if so, don't let them
            // buy another one.
            DataRow drAccessPass = UsersUtility.GetPassGroup (userId, 1);

            if (drAccessPass != null)
            {
                // User already has an access pass, can't buy again
                Response.Redirect(ResolveUrl("~/mykaneva/duplicatePurchase.aspx")); 
            }
            else
            {

                DataRow drOrder = null;
                bool newOrder = true;

                if (Request["orderId"] != null && Request["orderId"] != string.Empty)
                {
                    try
                    {
                        drOrder = StoreUtility.GetOrder(Convert.ToInt32(Request["orderId"]), userId);
                    }
                    catch { }
                }

                int orderId = 0;
                if (drOrder != null && drOrder["order_id"].ToString() != string.Empty &&
                    Convert.ToInt32(drOrder["transaction_status_id"]) == (int)Constants.eORDER_STATUS.CHECKOUT &&
                    Convert.ToInt32(drOrder["user_id"]) == GetUserId())
                {
                    orderId = Convert.ToInt32(drOrder["order_id"]);
                    newOrder = false;
                }
                else
                {
                    orderId = StoreUtility.CreateOrder(userId, Common.GetVisitorIPAddress(), (int)Constants.eORDER_STATUS.CHECKOUT);
                }

                // Set the purchase type
                StoreUtility.SetPurchaseType(orderId, Constants.ePURCHASE_TYPE.ACCESS_PASS);
                int creditId = 0;
                if (e.CommandArgument != null)
                {
                    if (KanevaGlobals.IsNumeric(e.CommandArgument.ToString()))
                    {
                        creditId = Convert.ToInt32(Server.HtmlEncode(e.CommandArgument.ToString()));
                    }
                }

                if (creditId == 0)
                {
                    spnMessage.InnerText = "Please select a purchase option.";
                    return;
                }

                DataRow drPointBucket = StoreUtility.GetPromotion(creditId);

                // Point Bucket item name
                string itemName = Server.HtmlEncode(drPointBucket["display"].ToString());

                if (drPointBucket["promotion_description"] != null && drPointBucket["promotion_description"].ToString() != string.Empty)
                {
                    itemName = Server.HtmlEncode(drPointBucket["promotion_description"].ToString());
                }

                int transactionId = 0;




                if (newOrder)
                {
                    // Record the transaction in the database, marked as checkout
                    transactionId = StoreUtility.PurchasePoints(userId, (int)Constants.eTRANSACTION_STATUS.CHECKOUT, itemName, 0,
                        (int)Constants.ePAYMENT_METHODS.KANEVA_CYBERSOURCE, Convert.ToDouble(drPointBucket["dollar_amount"]),
                        Convert.ToDouble(drPointBucket["kei_point_amount"]), Convert.ToDouble(drPointBucket["free_points_awarded_amount"]),
                        0, Common.GetVisitorIPAddress());

                    // Record the point bucket purchased
                    StoreUtility.UpdatePointTransactionPointBucket(transactionId, Convert.ToInt32(drPointBucket["promotion_id"]));
                }
                else
                {
                    transactionId = Convert.ToInt32(drOrder["point_transaction_id"]);

                    // updated existing order
                    StoreUtility.UpdatePointTransactionPointBucket(transactionId, Convert.ToInt32(drPointBucket["promotion_id"]), itemName, Convert.ToDouble(drPointBucket["dollar_amount"]), Convert.ToDouble(drPointBucket["kei_point_amount"]), Convert.ToDouble(drPointBucket["free_points_awarded_amount"]));
                }

                // Set the order id on the point purchase transaction
                StoreUtility.UpdatePointTransaction(transactionId, orderId);
                StoreUtility.UpdateOrderPointTranasactionId(orderId, transactionId);

                // Send them to select a payment method
                Response.Redirect(ResolveUrl("~/checkout/billinginfo.aspx?orderId=" + orderId.ToString() + "&pass=true")); ;
            }
        }

        #endregion

    }
}