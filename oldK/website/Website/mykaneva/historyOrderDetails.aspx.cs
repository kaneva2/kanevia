///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using log4net;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for historyOrderDetails.
	/// </summary>
	public class historyOrderDetails : SortedBasePage
	{
		protected historyOrderDetails () 
		{
			Title = "Order Details";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			VerifyUserAccess();
			VerifyOrder();

			int orderId = 0;
			if (Request ["orderId"] != null)
			{
				orderId = Convert.ToInt32 (Request ["orderId"]);
			}

			if (orderId > 0)
			{
				DataRow drOrder = StoreUtility.GetOrder (orderId);

				if (drOrder ["point_transaction_id"] != DBNull.Value)
				{
					DataRow drPurchasePoints = StoreUtility.GetPurchasePointTransaction (Convert.ToInt32 (drOrder ["point_transaction_id"]));

					lblOrderNumber.Text = drPurchasePoints ["order_id"].ToString ();
					lblDescription.Text = drPurchasePoints ["description"].ToString ();
					lblAmountPaid.Text = FormatCurrency (drPurchasePoints ["amount_debited"]);
					lblKPoints.Text = FormatKpoints (drPurchasePoints ["totalPoints"]);
					lblDate.Text = FormatDateTime (drPurchasePoints ["transaction_date"]);


                    if (Convert.ToInt32 (drPurchasePoints["payment_method_id"]) == (int) Constants.ePAYMENT_METHODS.PAYPAL)
                    {
                        trBillAddress.Visible = false;

                        lblName.Text = "Paypal";
                    }
                    else if (drPurchasePoints["gift_card_id"] != null && Convert.ToInt32 (drPurchasePoints["gift_card_id"]) > 0)
                    {
                        // gift card purchase so skip billing info
                    }
                    else    // credit card
                    {
                        DataRow drBillingInfo = StoreUtility.GetOrderBillingInfo (Convert.ToInt32 (drPurchasePoints["order_billing_information_id"]));

                        lblName.Text = drBillingInfo["name_on_card"].ToString ();
                        lblCardNumber.Text = "xxxx-xxxx-xxxx-" + KanevaGlobals.Decrypt (drBillingInfo["card_number"].ToString ());
                        lblCardType.Text = drBillingInfo["card_type"].ToString ();

                        DataRow drAddress = UsersUtility.GetAddress (GetUserId (), Convert.ToInt32 (drPurchasePoints["address_id"]));

                        if (drAddress != null)
                        {
                            lblAddress1.Text = drAddress["address1"].ToString ();
                            lblAddress2.Text = drAddress["address2"].ToString ();
                            lblCity.Text = drAddress["city"].ToString ();
                            lblState.Text = drAddress["state_code"].ToString ();
                            lblZip.Text = drAddress["zip_code"].ToString ();
                            lblCountry.Text = drAddress["country_name"].ToString ();
                        }
                    }
                }
			}

			// Set navigation
            HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
			HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.ACCOUNT;
			HeaderNav.MyAccountNav.ActiveTab = NavAccount.TAB.TRANSACTIONS;
			HeaderNav.SetNavVisible(HeaderNav.MyKanevaNav,2);
			HeaderNav.SetNavVisible(HeaderNav.MyAccountNav);
		}
		
		
		#region Helper Methods
		
		private void VerifyUserAccess()
		{
			// They must be logged in
			if (!Request.IsAuthenticated)
			{
				Response.Redirect (GetLoginURL ());
			}
		}

		private void VerifyOrder ()
		{
			if (Request ["orderId"] == null)
			{
				Response.Redirect ( "~/mykaneva/managecredits.aspx" );
			}

			try
			{
				int orderId = Convert.ToInt32 (Request ["orderId"]);			
				VerifyOrder (StoreUtility.GetOrder (orderId, GetUserId()));
			}
			catch (Exception ex)
			{
				string x = ex.Message;
				Response.Redirect ( "~/mykaneva/managecredits.aspx" );
			}
		}
		private void VerifyOrder (DataRow drOrder)
		{
			try				
			{
				// Verify user is owner of this order		  
				if (drOrder == null)
				{
					// User may be trying to view someone else's order
					m_logger.Warn ("User " + GetUserId () + " tried to view orderId " + Request ["orderId"].ToString () + " from IP " + Common.GetVisitorIPAddress());
					Response.Redirect ( "~/mykaneva/managecredits.aspx" );
				}

				if (Convert.ToInt32(drOrder["transaction_status_id"]) != (int) Constants.eORDER_STATUS.COMPLETED)
				{
					m_logger.Warn ("User " + GetUserId () + " tried to view orderId " + Request ["orderId"].ToString () + " from IP " + Common.GetVisitorIPAddress() + ".  Order does not have a status of CART.");
					Response.Redirect ( "~/mykaneva/managecredits.aspx" );
				}
			}
			catch
			{
				Response.Redirect ( "~/mykaneva/managecredits.aspx" );
			}
		}

		
		#endregion
		
		#region Decleraions

		protected Label lblOrderNumber, lblDescription, lblAmountPaid, lblKPoints, lblDate;
		protected Label lblName, lblCardNumber, lblCardType;
		protected Label lblAddress1, lblAddress2, lblCity, lblState, lblZip, lblCountry;

        protected HtmlTableRow trBillAddress;
        
        // Logger
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

		#endregion

		#region Web Form Designer generated code

		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
