<%@ Page language="c#" Codebehind="dashboard.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.dashboard" %>


<link href="../css/new.css" rel="stylesheet" type="text/css">

<table border="0" cellspacing="0" cellpadding="0" class="newcontainer" align="center">
	<tr>
		<td>
			<table  border="0" cellpadding="0" cellspacing="0" width="100%" class="newcontainerborder">
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
				<tr>
					<td class=""><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
					<td valign="top" class="newdatacontainer">
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
								
									<!-- TOP STATUS BAR -->
									<div id="pageheader">
										<table cellpadding="0" cellspacing="0" border="0" width="99%">
											<tr>
												<td align="left">
													<h1>My Kaneva Stats</h1>
													<h3>View your activity and what&rsquo;s happening with your communities.</h3>
												</td>
									
												<td align="right" valign="middle">
													<table cellpadding="0" cellspacing="0" border="0">
														<tr>
															<td>	
																<a href="~/mykaneva/history.aspx" runat="server">My View History</a>
															</td>	
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</div>
									<!-- END TOP STATUS BAR -->		
								
								</td>
							</tr>
							<tr>
								<td width="968" align="left"  valign="top">
								<br>
									<table border="0" cellspacing="0" cellpadding="0" width="100%">
										<tr>
											<td valign="top" width="477">
												
												<!-- MY MESSAGES -->
												<div class="module whitebg">
												<span class="ct"><span class="cl"></span></span>
					
												<h2>My Messages</h2>
												
												<table border="0" cellpadding="0" cellspacing="0" width="95%">
													<tr>
														<td>
															<div class="frame">
															<span class="ct"><span class="cl"></span></span>
															<table border="0" cellpadding="0" cellspacing="0" width="92%">
																<tr>
																	<td align="left" wrap="nowrap"><a runat="server" href="~/mykaneva/mailbox.aspx">My Messages</a></td>
																	<td align="right"><a runat="server" href="~/mykaneva/mailbox.aspx">Messages:</a> <asp:label id="lblPMCount" runat="server"/> &nbsp;&nbsp;&nbsp;&nbsp;<a runat="server" href="~/mykaneva/mailboxFriendRequests.aspx">Requests:</a> <asp:label id="lblRequestCount" runat="server"/></td>
																</tr>
															</table>
															<span class="cb"><span class="cl"></span></span>
															</div>
														</td>
													</tr>
												</table>
												<br>
											
												<asp:repeater id="rptMailBox" runat="server">
												
													<headertemplate>
													
														<table width="93%" border="0" align="center" cellpadding="5" cellspacing="0" class="nopadding">
													
													</headertemplate>
													
													<itemtemplate>
													
														<tr>
															<td align="Center" valign="Middle" width="40">
																<a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "name_no_spaces").ToString ()) %>><img runat="server" src='<%# GetProfileImageURL (DataBinder.Eval(Container.DataItem, "thumbnail_small_path").ToString ()) %>' border="0" width="40" height="40"/></a>
															</td>
															<td align="Center" valign="Middle" width="17"><img runat="server" src='<%#GetTypeImage (Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "type")), DataBinder.Eval(Container.DataItem, "to_viewable").ToString ())%>'/></td>
															<td align="Left" valign="Middle">
																<h1>
																	<a title='<%#RemoveHTML (TruncateWithEllipsis (DataBinder.Eval(Container.DataItem, "message").ToString (), 100))%>' runat="server" href='<%# "messageDetails.aspx?messageId=" + DataBinder.Eval(Container.DataItem, "message_id").ToString ()%>'>
																	<span><%# TruncateWithEllipsis (DataBinder.Eval(Container.DataItem, "subject").ToString (), 40)%></span>
																	</a>
																</h1>
																<span class="textGeneralGray11">from <%#TruncateWithEllipsis (DataBinder.Eval(Container.DataItem, "username").ToString (), 20)%> on <%#FormatDateTime (DataBinder.Eval(Container.DataItem, "message_date"))%></span>
															</td>
														</tr>
														
													</itemtemplate>
												
													<footertemplate></footertemplate>
													
												</asp:repeater>		

														<tr>																									   
															<td colspan="3">
																<table width="100%"  border="0" cellspacing="0" cellpadding="0">
																	<tr>
																		<td width="80%">&nbsp;</td>
																		<td width="10%" align="right" valign="bottom" nowrap="nowrap" class="textGeneralGray11"><asp:label runat="server" id="lblMailboxNoResults"/></td>
																		<td width="1%"><img src="../images/spacer.gif" width="10" height="2" /></td>
																		<td width="9%" align="right" valign="bottom" nowrap="nowrap" class="textGeneralGray11"><a runat="server" href="~/myKaneva/mailbox.aspx?subtab=msg">Show All</a></td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
												
												<span class="cb"><span class="cl"></span></span>
												</div>	
												<!-- END MESSAGES -->  
											
												
											
												<!-- MY COMMUNITIES -->
												<div class="module whitebg">
												<span class="ct"><span class="cl"></span></span>
						
												<h2>My Communities</h2>
												
												<table border="0" cellpadding="0" cellspacing="0" width="95%">
													<tr>
														<td>
															<div class="frame">
															<span class="ct"><span class="cl"></span></span>
															<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom:6px">
																<tr>
																	<td align="left" nowrap class="textGeneralGray11" ><a id="aIOwn" runat="server">I own:</a> <asp:label id="lblCommOwn" runat="server"/></td>
																	<td width="1%"><img src="../images/spacer.gif" width="10" height="8" /></td>
																	<td><a id="aIMod" runat="server">I moderate:</a> <asp:label id="lblCommModerator" runat="server"/></td>
																	<td width="1%"><img src="../images/spacer.gif" width="10" height="8" /></td>
																	<td><a id="aIBelong" runat="server">I belong to:</a> <asp:label id="lblCommBelong" runat="server"/></td>
																</tr>
															</table>
															
															<table border="0" cellpadding="0" cellspacing="0" width="92%">
																<tr>
																	<td align="left" class="textGeneralGray11" >Pending Memberships: (<asp:label id="lblPendingComm" runat="server"/>)</td>
																	<td align="right" class="textGeneralGray11"><a runat="server" href="~/mykaneva/mailboxMemberRequests.aspx">Members Pending My Communities:</a> (<asp:label id="lblPendingCommMembers" runat="server"/>)</td>
																</tr>
															</table>
															<span class="cb"><span class="cl"></span></span>
															</div>
														</td>
													</tr>
												</table>
												
												<br>
												
												<table width="90%" border="0" align="center" cellpadding="5" cellspacing="0">
													<tr>
														<td width="9%">
															<img runat="server" id="imgAvatar" src="~/images/KanevaIcon01.gif" border="0" width="40" height="40"/>
														</td>
														<td class="textGeneralGray11" align="left">
															<asp:label id="lblPCUserName" runat="server" /><br>
															<asp:label id="lblPCNumberOfFriends" runat="server" /> friends 
															<asp:label id="lblPCNumberOfDiggs" runat="server" /> raves 
															<asp:label id="lblPCNumberTimesShared" runat="server" /> shares 
															<asp:label id="lblPCNumberOfViews" runat="server" /> visits 
														</td>
													</tr>

													<!-- CHANNELS -->
													<asp:repeater id="repeaterChannels" runat="server">
														<itemtemplate>
															
															<tr>
																<td width="9%">
																	<a href='<%# GetBroadcastChannelUrl (DataBinder.Eval (Container.DataItem, "name_no_spaces").ToString ())%>' style="cursor: hand;"><img runat="server" src='<%# GetBroadcastChannelImageURL (DataBinder.Eval(Container.DataItem, "thumbnail_small_path").ToString (), "sm") %>' height="40" width="40" border="0"/></a>
																</td>
																<td class="textGeneralGray11" align="left">
																	<%# DataBinder.Eval(Container.DataItem, "name") %><br>
																	<%# DataBinder.Eval(Container.DataItem, "number_of_members") %> members 
																	<%# DataBinder.Eval(Container.DataItem, "number_of_diggs") %> raves 
																	<%# DataBinder.Eval(Container.DataItem, "number_times_shared") %> shares 
																	<%# DataBinder.Eval(Container.DataItem, "number_of_views") %> visits
																</td>
															</tr>

														</itemtemplate>    
													</asp:repeater>  
														
													<tr>
														<td align="right" colspan="2"><a runat="server" href="~/bookmarks/channelTags.aspx?type=2">Show All Communities</a></td>
													</tr>
												</table>
																	
												<span class="cb"><span class="cl"></span></span>
												</div>
												<!-- END COMMUNITIES -->										  
																						
												
												<!-- MY FRIENDS -->
												<div class="module whitebg">
												<span class="ct"><span class="cl"></span></span>
												
												<h2>My Friends</h2>
												
												<table border="0" cellpadding="0" cellspacing="0" width="95%">
													<tr>
														<td>
															<div class="frame">
															<span class="ct"><span class="cl"></span></span>
															<table border="0" cellpadding="0" cellspacing="0" width="92%">
																<tr>
																	<td align="left" width="100%"><a runat="server" href="~/myKaneva/friends.aspx">My Friends</a></td>
																	<td align="right" nowrap="nowrap" class="textGeneralGray11" >My Friends: <asp:label id="lblCurrentFriends" runat="server"/>&nbsp;&nbsp;&nbsp;Pending: <asp:label id="lblPendingFriends" runat="server"/></td>
																</tr>
															</table>
															<span class="cb"><span class="cl"></span></span>
															</div>
														</td>
													</tr>
												</table>
												
												<br>
										
												<asp:datalist visible="true" runat="server" showfooter="False" width="93%" id="dlFriends" cellpadding="5" cellspacing="0" border="0" repeatcolumns="5" repeatdirection="Horizontal" >
													<itemstyle horizontalalign="Center"/>
													<itemtemplate>
													
														<a href="<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "NameNoSpaces").ToString ())%>"><img runat="server" src='<%# GetProfileImageURL (DataBinder.Eval(Container.DataItem, "ThumbnailSmallPath").ToString ()) %>' alt='<%# DataBinder.Eval(Container.DataItem, "Username") %>' border="0" width="50" height="50"/></a>
														
													</itemtemplate>
													
												</asp:datalist>
												 
												<table cellpadding="5" cellspacing="0" border="0" width="93%">
													<tr>
														<td colspan="5">
															<table width="100%"  border="0" cellspacing="0" cellpadding="0">
																<tr>
																	<td width="5%" align="right" valign="bottom" nowrap="nowrap" class="textGeneralGray11"><a runat="server" href="~/myKaneva/friends.aspx">Show All</a></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												
												<span class="cb"><span class="cl"></span></span>
												</div>	
												<!-- END FRIENDS -->
														
											</td>
											<td><img runat="server" src="~/images/spacer.gif" width="14" height="1" /></td>
											<td width="477" valign="top">
												
												<!-- MY ACCOUNT -->
												<div class="module whitebg">
												<span class="ct"><span class="cl"></span></span>
												
												<h2>My Account</h2>
												
												<table border="0" cellpadding="0" cellspacing="0" width="95%">
													<tr>
														<td>
															<div class="frame">
															<span class="ct"><span class="cl"></span></span>
															<table border="0" cellpadding="0" cellspacing="0" width="92%">
																<tr>
																	<td align="left" class="textGeneralGray11" align="left"><a runat="server" href="~/community/ProfilePage.aspx">My Profile</a></td>
																	<td align="right" class="textGeneralGray11" align="left"></td>
																</tr>
															</table>
															<span class="cb"><span class="cl"></span></span>
															</div>
														</td>
													</tr>
												</table>
					
												<br>
												
												<table width="95%"  border="0" cellspacing="0" cellpadding="0" class="data">
													
													<tr>
														<td class="textGeneralGray11" align="left">Credit Balance:</td>
														<td><asp:label id="lblKPoint" runat="server"/></td>
													</tr>
													<tr class="altrow">
														<td class="textGeneralGray11" align="left">Unique Visitors:</td>
														<td><asp:label id="lblUniqueVisitors" runat="server"/></td>
													</tr>
													<tr>
														<td class="textGeneralGray11" align="left">Total Visits:</td>
														<td><asp:label id="lblMyProfile" runat="server"/></td>
													</tr>
													<tr class="altrow">
														<td class="textGeneralGray11" align="left">Raves:</td>
														<td><asp:label id="lblScore" runat="server"/></td>
													</tr>
													<tr>
														<td class="textGeneralGray11" align="left">Shares:</td>
														<td><asp:label id="lblShares" runat="server"/></td>
													</tr>
													<tr class="altrow">
														<td class="textGeneralGray11" align="left">Logins:</td>
														<td><asp:label id="lblLogins" runat="server"/></td>
													</tr>
												</table>
												<span class="cb"><span class="cl"></span></span>
												</div>												
												<!-- END MY ACCOUNT -->
														
														
												<!-- MY MEDIA STATS -->
												<div class="module whitebg">
												<span class="ct"><span class="cl"></span></span>
												
												<h2>My Media Stats</h2>
												
												<table border="0" cellpadding="0" cellspacing="0" width="95%">
													<tr>
														<td>
															<div class="frame">
															<span class="ct"><span class="cl"></span></span>
															<table border="0" cellpadding="0" cellspacing="0" width="92%">
																<tr>
																	<td align="left" class="textGeneralGray11" align="left"><a href="/asset/publishedItemsNew.aspx">Media Uploads</a></td>
																	<td align="right" class="textGeneralGray11" align="left"></td>
																</tr>
															</table>
															<span class="cb"><span class="cl"></span></span>
															</div>
														</td>
													</tr>
												</table>
												
												<br>
											
												<table width="95%" border="0" cellspacing="0" cellpadding="0" class="data">
													<tr>
														<td class="textGeneralGray11" width="220">Month</td>
														<td class="textGeneralGray11"> Items Viewed</td>
													</tr>
													
												<asp:repeater enableviewstate="False" runat="server" id="rptItemsSold">  
														
													<itemtemplate>
														<tr class="altrow">
															<td><a href='<%#GetItemsSoldLink (Convert.ToDateTime (DataBinder.Eval (Container.DataItem, "purchase_month")))%>'><%# DataBinder.Eval (Container.DataItem, "month")%>, <%# DataBinder.Eval (Container.DataItem, "year")%></a></td>
															<td class="textGeneralGray11"> <%# DataBinder.Eval (Container.DataItem, "items_sold")%></td>
														</tr>
													</itemtemplate>
													
													<alternatingitemtemplate>
														<tr>
															<td><a href='<%#GetItemsSoldLink (Convert.ToDateTime (DataBinder.Eval (Container.DataItem, "purchase_month")))%>'><%# DataBinder.Eval (Container.DataItem, "month")%>, <%# DataBinder.Eval (Container.DataItem, "year")%></a></td>
															<td class="textGeneralGray11"> <%# DataBinder.Eval (Container.DataItem, "items_sold")%></td>
														</tr>
													</alternatingitemtemplate>
														
												</asp:repeater>

												</table>
													
												<span class="cb"><span class="cl"></span></span>
												</div>
												<!-- END MY MEDIA STATS -->
												
													
												<!-- MY MEDIA -->
												<div class="module whitebg">
												<span class="ct"><span class="cl"></span></span>
												
												<h2>My Media</h2>
												
												<table border="0" cellpadding="0" cellspacing="0" width="95%">
													<tr>
														<td>
															<div class="frame">
															<span class="ct"><span class="cl"></span></span>
															<table border="0" cellpadding="0" cellspacing="0">
																<tr>
																	<td width="28%" align="left" class="textGeneralGray11" align="left" wrap="nowrap"><a href="~/asset/publishedItemsNew.aspx" runat="server">Media Uploads</a></td>
																	<td width="4%" nowrap="nowrap" class="textGeneralGray11"><img runat="server" src="~/images/videos.gif" width="14" height="20" border="0" align="right" /></td>
																	<td width="1%"><img runat="server" src="~/images/spacer.gif"  width="1" height="1" /></td>
																	<td width="6%" nowrap="nowrap" class="textGeneralGray11" style="text-align: center;">videos<br><asp:label id="lblVideoCount" runat="server"/></td>
																	<td width="6%"><img runat="server" src="~/images/spacer.gif" width="6" height="1" /></td>
																	<td width="6%" nowrap="nowrap" class="textGeneralGray11"><img runat="server" src="~/images/photos.gif" width="19" height="19" border="0" align="right" /></td>
																	<td width="1%"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
																	<td width="6%" nowrap="nowrap"class="textGeneralGray11" style="text-align: center;">photos<br><asp:label id="lblPhotoCount" runat="server"/></td>
																	<td width="6%"><img runat="server" src="~/images/spacer.gif" width="6" height="1" /></td>
																	<td width="6%" nowrap="nowrap" class="textGeneralGray11"><img runat="server" src="~/images/music.gif" width="15" height="20" border="0" align="right" /></td>
																	<td width="1%"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
																	<td width="6%" nowrap="nowrap"class="textGeneralGray11" style="text-align: center;">music<br><asp:label id="lblMusicCount" runat="server"/></td>
																	<td width="6%"><img runat="server" src="~/images/spacer.gif" width="6" height="1" /></td>
																	<td width="6%" class="textGeneralGray11"><img runat="server" src="~/images/games.gif" id="Img70" width="16" height="20" border="0" align="right" /></td>
																	<td width="1%"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
																	<td width="9%" nowrap="nowrap" class="textGeneralGray11" style="text-align: center;">games<br><asp:label id="lblGamesCount" runat="server"/></td>
																</tr>
															</table>
															<span class="cb"><span class="cl"></span></span>
															</div>
														</td>
													</tr>
												</table>
												
												<br>
											
												<table width="95%"  border="0" cellspacing="0" cellpadding="0" class="data">
													<tr class="altrow">
														<td class="textGeneralGray11" align="left" width="220">Media Items:</td>
														<td><asp:label id="lblItemsForSale" runat="server"/></td>
													</tr>
													<tr>
														<td class="textGeneralGray11" align="left">My Media Viewed:</td>
														<td><asp:label id="lblItemsSold" runat="server"/> times</td>
													</tr>
													<tr class="altrow">
														<td class="textGeneralGray11" align="left">Last Media Viewed:</td>
														<td><asp:label id="lblItemSold" runat="server"/> <asp:label id="lblItemSoldDate" runat="server"/> </td>
													</tr>
													<tr>
														<td class="textGeneralGray11" align="left">Total Traffic:</td>
														<td><asp:label id="lblTraffic" runat="server"/></td>
													</tr>
													<tr class="altrow">
														<td class="textGeneralGray11" align="left">Last Month Traffic:</td>
														<td><asp:label id="lblBandwidth" runat="server"/></td>
													</tr>
													<tr>
														<td class="textGeneralGray11" align="left">Storage Used:</td>
														<td><asp:label id="lblPubSize" runat="server"/></td>
													</tr>
												</table>
																
												<span class="cb"><span class="cl"></span></span>
												</div>										
												<!-- END MY MEDIA -->		
														
											
												<!-- MY CONTRIBUTIONS -->
												<div class="module whitebg">
												<span class="ct"><span class="cl"></span></span>
												
												<h2>My Contributions</h2>
												
												<table border="0" cellpadding="0" cellspacing="0" width="95%">
													<tr>
														<td>
															<div class="frame">
															<span class="ct"><span class="cl"></span></span>
															<table border="0" cellpadding="0" cellspacing="0" width="92%">
																<tr>
																	<td align="left" class="textGeneralGray11" align="left"><a href="~/community/ProfilePage.aspx" runat="server">My Profile</a></td>
																	<td align="right" class="textGeneralGray11" align="left"></td>
																</tr>
															</table>
															<span class="cb"><span class="cl"></span></span>
															</div>
														</td>
													</tr>
												</table>
												
												<br>
												
												<table width="95%"  border="0" cellspacing="0" cellpadding="0" class="data">
													<tr class="altrow">
														<td class="textGeneralGray11" align="left" width="220">Blog Entries Made:</td>
														<td><asp:label id="lblBlogEntry" runat="server"/></td>
													</tr>
													<tr>
														<td class="textGeneralGray11" align="left">Comments Made:</td>
														<td><asp:label id="lblComments" runat="server"/></td>
													</tr>
													<tr class="altrow">
														<td class="textGeneralGray11" align="left">Items Raved:</td>
														<td><asp:label id="lblAssetDiggs" runat="server"/></td>
													</tr>
													<tr>
														<td class="textGeneralGray11" align="left">Forum Posts:</td>
														<td><asp:label id="lblPosts" runat="server"/></td>
													</tr>
												</table>
												
												<span class="cb"><span class="cl"></span></span>
												</div>		
												<!-- END MY CONTRIBUTIONS -->
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
								
					<td class=""><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
				</tr>
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
				
			</table>
		</td>
	</tr>
</table>
<br/>