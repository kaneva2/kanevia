///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using log4net;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for mailbox.
	/// </summary>
	public class mailboxGifts : SortedBasePage
	{
		protected mailboxGifts () 
		{
			Title = "Gifts";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			// They must be logged in
			if (!Request.IsAuthenticated)
			{
				Response.Redirect (GetLoginURL ());
				return;
			}

			if (!IsPostBack)
			{
				int userId = GetUserId ();
				BindData (1, userId);

				LoadDropdown (userId);
			}


			// Set nav
            HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
			HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.MESSAGES;
			HeaderNav.MyMessagesNav.ActiveTab = NavMessages.TAB.NONE;
			HeaderNav.SetNavVisible(HeaderNav.MyKanevaNav,2);
			HeaderNav.SetNavVisible(HeaderNav.MyMessagesNav);
		}

		/// <summary>
		/// Bind the data
		/// </summary>
		/// <param name="pageNumber"></param>
		private void BindData (int pageNumber, int userId)
		{
			// Set the sortable columns
			//SetHeaderSortText (rptMailBox);
			string orderby = CurrentSort + " " + CurrentSortOrder;

			PagedDataTable pds = UsersUtility.GetUserMessages (userId, "m.type = "
				+ (int) Constants.eMESSAGE_TYPE.GIFT
				, orderby, pageNumber, MESSAGES_PER_PAGE);
			rptMailBox.DataSource = pds;
			rptMailBox.DataBind ();

			pgTop.NumberOfPages = Math.Ceiling ((double) pds.TotalCount / MESSAGES_PER_PAGE).ToString ();
			pgTop.DrawControl ();

			// The results
			lblSearch.Text = GetResultsText (pds.TotalCount, pageNumber, MESSAGES_PER_PAGE, pds.Rows.Count);
		}

		/// <summary>
		/// Page Change
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pgTop_PageChange (object sender, PageChangeEventArgs e)
		{
			BindData (e.PageNumber, GetUserId ());
		}

		/// <summary>
		/// They clicked to sort a column
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected override void btnSort_Click (object sender, System.EventArgs e)
		{
			base.btnSort_Click (sender, e);
			BindData (pgTop.CurrentPageNumber, GetUserId ());
		}

		/// <summary>
		/// DEFAULT_SORT
		/// </summary>
		/// <returns></returns>
		protected override string DEFAULT_SORT
		{
			get
			{
				return "message_date";
			}
		}

		/// <summary>
		/// DEFAULT_SORT ORDER
		/// </summary>
		/// <returns></returns>
		protected override string DEFAULT_SORT_ORDER
		{
			get
			{
				return "DESC";
			}
		}

		/// <summary>
		/// Click the send message button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnSendMessage_Click (Object sender, ImageClickEventArgs e) 
		{
			Server.Transfer (ResolveUrl ("~/myKaneva/newMessage.aspx"));
		}


		protected void LoadDropdown (int userId)
		{
			drpActions.Items.Clear ();

			drpActions.Items.Add (CreateListItem ("Select action...", "", true));
			drpActions.Items.Add (new ListItem (Constants.DROPDOWN_SEPERATOR, ""));
			drpActions.Items.Add (new ListItem ("Trash and reject gift(s)", "del"));
			drpActions.Items.Add (new ListItem ("Accept Gift(s)", "accept"));

			drpActions.SelectedIndex = 0;
		}

		/// <summary>
		/// Change category of asset
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void drpAction_Change (Object sender, EventArgs e)
		{
			int userId = GetUserId ();
			HtmlInputHidden hidMessageId;
			CheckBox chkEdit;

			if (drpActions.SelectedValue.Equals ("del"))
			{
				// Remove the messages
				foreach (RepeaterItem dliMessage in rptMailBox.Items)
				{
					chkEdit = (CheckBox) dliMessage.FindControl ("chkEdit");

					if (chkEdit.Checked)
					{
						hidMessageId = (HtmlInputHidden) dliMessage.FindControl ("hidMessageId");

						if (!UsersUtility.IsMessageRecipient (Convert.ToInt32 (hidMessageId.Value), GetUserId ()))
						{
							m_logger.Warn ("User " + GetUserId () + " from IP " + Common.GetVisitorIPAddress() + " tried to delete message " + hidMessageId.Value + " but is not the message owner");
							return;
						}

						UsersUtility.RejectGift( userId, Convert.ToInt32(hidMessageId.Value), true );
						UsersUtility.DeleteMessage (userId, Convert.ToInt32 (hidMessageId.Value));
			
					}
				}
			}
			else if ( drpActions.SelectedValue.Equals ("accept") )
			{
				// Remove the messages
				foreach (RepeaterItem dliMessage in rptMailBox.Items)
				{
					chkEdit = (CheckBox) dliMessage.FindControl ("chkEdit");

					if (chkEdit.Checked)
					{
						hidMessageId = (HtmlInputHidden) dliMessage.FindControl ("hidMessageId");

						if (!UsersUtility.IsMessageRecipient (Convert.ToInt32 (hidMessageId.Value), GetUserId ()))
						{
							m_logger.Warn ("User " + GetUserId () + " from IP " + Common.GetVisitorIPAddress() + " tried to delete message " + hidMessageId.Value + " but is not the message owner");
							return;
						}

						UsersUtility.Accept2DGift( userId, Convert.ToInt32(hidMessageId.Value) );
						UsersUtility.DeleteMessage (userId, Convert.ToInt32 (hidMessageId.Value));
			
					}
				}
			}

			BindData (pgTop.CurrentPageNumber, GetUserId ());
		}

		protected Kaneva.Pager pgTop;

		protected Label lblSearch;
		protected DropDownList drpActions;
		protected Repeater rptMailBox;	

		private const int MESSAGES_PER_PAGE = 25;
		protected PlaceHolder phBreadCrumb;

		// Logger
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

		#region Web Form Designer generated code

		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
			pgTop.PageChanged +=new PageChangeEventHandler (pgTop_PageChange);
		}
		#endregion
	}
}
