﻿<%@ Page Language="C#" MasterPageFile="~/masterpages/IndexPageTemplate.Master" AutoEventWireup="true" CodeBehind="MessageCenter.aspx.cs" ValidateRequest="False"  Inherits="KlausEnt.KEP.Kaneva.mykaneva.MessageCenter" %>
<%@ Register TagPrefix="Kaneva" TagName="MKContainer" Src="../usercontrols/doodad/ContainerMyKaneva.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../usercontrols/Pager.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="Activities" Src="../usercontrols/Activities.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="CE" Namespace="CuteEditor" Assembly="CuteEditor" %>

<asp:Content ID="cntMessageCenter" runat="server" contentplaceholderid="cph_Body" >

<script src="../jscript/prototype-1.6.1.js" type="text/javascript" language="javascript"></script>

<style type="text/css">
<!--
@import url("../css/mykaneva/MessageCenter.css?v124");
-->
#ddmenu div{top:65px;}
</style>

<div class="PageContainer">
		
	<!-- Left Column -->
	<div id="leftRail">
		<kaneva:mkcontainer id="MKcontainerProfile" runat="server" ControlToLoad="usercontrols/doodad/UserProfile.ascx" />

		<asp:updatepanel id="upMessageSummary" runat="server" childrenastriggers="true" updatemode="conditional">
			<contenttemplate>
				<div class="navSummary" id="divMessageSummary" runat="server">
					<div class="shop"><a id="A2" href="http://shop.kaneva.com" runat="server">Shop</a></div>
					
					<div class="messages"><asp:linkbutton id="btnNavMessages" runat="server" oncommand="btnCPNav_Click" tooltip="" text="Messages"></asp:linkbutton> <span class="new" id="spnNewMessages" runat="server"></span></div>
					<div class="subNav" id="subNavMessages" runat="server">
						<div runat="server"><asp:linkbutton id="btnNavMyMessages" runat="server" oncommand="btnCPNav_Click" tooltip="View personal messages in your Inbox." text="My Messages"></asp:linkbutton></div>
						<div runat="server"><asp:linkbutton id="btnNavSentMessages" runat="server" oncommand="btnCPNav_Click" tooltip="View personal message you sent." text="Sent Messages"></asp:linkbutton></div>
					</div>

					<div class="friends"><asp:linkbutton id="btnNavFriends" runat="server" oncommand="btnCPNav_Click" tooltip="" text="Friends"></asp:linkbutton> <span class="new" id="spnNewFriendRequests" runat="server"></span></div> 
					<div class="subNav" id="subNavFriends" runat="server">
						<div runat="server"><asp:linkbutton id="btnNavFriendInvites" runat="server" oncommand="btnCPNav_Click" tooltip="View and approve people who want to be your friend." text="Friend Requests"></asp:linkbutton></div>
						<div runat="server"><a id="A4" href="~/mykaneva/inviteFriend.aspx" runat="server">Invite Friends</a></div>
						<div runat="server"><a id="A5" href="~/mykaneva/friendsGroups.aspx" runat="server">My Friends</a></div>
						<div runat="server"><a id="A6" href="~/people/people.kaneva" runat="server">Find People</a></div>
					</div>
					
					<div class="media"><a id="btnNavMedia" href="~/asset/publishedItemsNew.aspx" runat="server" tooltip="View your Media Library">Media</a></div>

					<div class="contests"><a id="btnNavContests" href="~/mykaneva/contests.aspx" runat="server">Contests</a></div>

					<div class="activity"><asp:linkbutton id="btnNavActivity" runat="server" oncommand="btnCPNav_Click" tooltip="" text="Activities"></asp:linkbutton> <span class="new" id="spnActivitySummaryCount" runat="server"></span></div>
					<div class="subNav" id="subNavActivity" runat="server">
						<div id="Div2" runat="server"><asp:linkbutton id="btnNavActivityFriends" runat="server" oncommand="btnCPNav_Click" tooltip="View your friends recent activity." text="Friends Activity"></asp:linkbutton>  <span class="new" id="spnNewActivityFriends" runat="server"></span></div>
						<div id="Div1" runat="server"><asp:linkbutton id="btnNavActivityMine" runat="server" oncommand="btnCPNav_Click" tooltip="View your recent activity." text="My Activity"></asp:linkbutton>  <span class="new" id="spnNewActivity" runat="server"></span></div>
					</div>
					
					<div class="subNav" id="subNavCommunity" runat="server">
					</div>
					
					<asp:linkbutton id="btnFriendInvitesSent" runat="server" oncommand="btnCPNav_Click" style="visibility:hidden;"></asp:linkbutton>
				</div>	
				<div class="navGo3D">
					<hr />
					<asp:linkbutton id="btnGo3D" runat="server" CssClass="btnmedium blue" text="Play Now" />
				</div><!-- button-->

				<div class="navSummary navWorlds" id="navWorlds">
					<h2>Worlds</h2>
					<div id="Div4" runat="server"><a id="A1" href="~/mykaneva/my3dapps.aspx" runat="server">My Worlds</a></div>
					<div id="Div3" runat="server"><asp:linkbutton id="btnNav3DAppRequests" runat="server" oncommand="btnCPNav_Click" tooltip="" text="Requests"></asp:linkbutton>  <span class="worldreq new" id="spnNew3DAppRequests" runat="server"></span></div>
					<div id="Div5" runat="server"><asp:linkbutton id="btnNavCommunityInvites" runat="server" oncommand="btnCPNav_Click" tooltip="" text="Invites"></asp:linkbutton>  <span class="worldreq new" id="spnNewCommunityInvites" runat="server"></span></div>
					<div id="Div8" runat="server"><a id="A3" href="~/community/StartWorld.aspx" runat="server">Create a World</a></div>
					<div id="approval" runat="server"><asp:linkbutton id="btnNavCommunityRequests" runat="server" oncommand="btnCPNav_Click" tooltip="View and approve people who want to join your members-only World." text="World Approvals"></asp:linkbutton><span class="commreq new" id="spnNewCommunityRequests" runat="server"></span></div>
				</div>

			</contenttemplate>	
		</asp:updatepanel>	
	</div>

	<!-- Right Column -->
	<div id="rightRail">
		
		<asp:updatepanel id="upTitle" runat="server" childrenastriggers="true" updatemode="conditional">
			<contenttemplate>
				<div class="pageHeader">
					<h2 class="pageTitle"><asp:literal id="litPageName" runat="server"></asp:literal></h2>
					<div class="back" id="divBackButton" runat="server" visible="true"><a id="btnBackToMyKaneva" class="btnmedium grey" runat="server" href="~/default.aspx">< Back to Blasts</a></div>
				</div>
				<div id="divAlertContainer"><div class="msgRow" id="divAlertMsg" runat="server"></div></div>
			</contenttemplate>
		</asp:updatepanel>

		<asp:updatepanel visible="false" id="upNoResults" childrenastriggers="true" runat="server" updatemode="conditional">
			<contenttemplate>
				<div id="divNoResults" runat="server" class="noResults">
					<div class="noResultMsg" id="divNoResultMsg" runat="server"></div>
					<div class="noResultCallout" id="divNoResultCallout" runat="server"></div>
				</div>
			</contenttemplate>
		</asp:updatepanel>

		<!-- Messages -->
		<asp:updatepanel visible="false" id="upMessages" childrenastriggers="true" runat="server" updatemode="conditional">
			<contenttemplate>
				<div class="messages">
				
				<div class="buttonRow" id="divMessagesButtonRow" runat="server"><div class="text">Selected:</div> 
					<div class="button"><asp:linkbutton class="btnsmall grey" id="btnMessageMarkRead" runat="server" onclick="btnMessageMarkRead_Click">Mark as Read</asp:linkbutton></div>
					<div class="button"><asp:linkbutton class="btnxsmall grey" id="btnDeleteMessage" runat="server" onclick="btnDeleteMessage_Click">Delete</asp:linkbutton></div>
					<ajaxToolkit:modalpopupextender id="mpeDeleteMessage" BackgroundCssClass="facebox_overlay facebox_overlayBG" CancelControlID="btnCancelConfirm" runat="server" popupcontrolid="pnlConfirmation" targetControlId="btnShowMsgPopup"></ajaxToolkit:modalpopupextender>
				</div>

				<asp:repeater runat="server" id="rptMessages" onitemcommand="rptMessages_ItemCommand" onitemdatabound="rptMessages_ItemDataBound">  
					<headertemplate>
						<div class="header">
							<div class="select">Select:<br><a href="javascript:void(0);" onclick="Select_All(true);">All</a> | <a href="javascript:void(0);" onclick="Select_All(false);">None</a></div>
							<div class="profile">Sender</div>
							<div class="subject">Subject</div>
						</div>
					</headertemplate>
					<itemtemplate>
						<div class="row" runat="server">												
							<div class="chkBox"><asp:checkbox runat="server" id="chkMsgEdit" /></div>
							<div class="thumb">	
								<div class="framesize-xsmall">
									<div class="frame">
										<span class="ct"><span class="cl"></span></span>
											<div class="imgconstrain">
												<a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "CommunityNameNoSpaces").ToString ()) %>><img runat="server" src='<%# GetProfileImageURL (DataBinder.Eval(Container.DataItem, "ThumbnailSquarePath").ToString (), "sq", "M", Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "OwnerFacebookSettings.UseFacebookProfilePicture")), Convert.ToUInt64(DataBinder.Eval(Container.DataItem, "OwnerFacebookSettings.FacebookUserId")) ) %>' border="0" width="30" id="Img8"/></a>
											</div>
										<span class="cb"><span class="cl"></span></span>
									</div>
								</div>		
								<input type="hidden" runat="server" id="hidMessageId" value='<%#DataBinder.Eval(Container.DataItem, "MessageId")%>' name="hidMessageId" />
								<input type="hidden" runat="server" id="hidToViewable" value='<%#DataBinder.Eval(Container.DataItem, "ToViewable")%>' name="hidToViewable" />
							</div>			
							<div class="profile">
								<a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "CommunityNameNoSpaces").ToString ()) %>><%# DataBinder.Eval(Container.DataItem, "UserName") %></a>
								<div><%# FormatDateTimeSpan (DataBinder.Eval(Container.DataItem, "MessageDate"), DataBinder.Eval (Container.DataItem, "CurrentDate")) %></div>
							</div>													
							<div class="subject"><asp:linkbutton id="btnMsgDetails" runat="server" commandname="Details" runat="server"><%# TruncateWithEllipsis (DataBinder.Eval(Container.DataItem, "Subject").ToString (), 62) %></asp:linkbutton></div>
						</div>														 
					</itemtemplate>
					<footertemplate></footertemplate>
				</asp:repeater>

 				<div class="pagerContainer">
					<div class="results"><asp:label runat="server" id="lblMessagesResults" /></div>
					<kaneva:pager runat="server" isajaxmode="False" class="pager" id="pagerMessages" maxpagestodisplay="5" shownextprevlabels="true" />
				</div>
				<asp:Button id="btnShowMsgPopup" runat="server" style="display:none" />

				</div>
			</contenttemplate>
		</asp:updatepanel>
		
		<!-- Sent Messages -->
		<asp:updatepanel visible="false" id="upMessagesSent" childrenastriggers="true" runat="server" updatemode="conditional">
			<contenttemplate>
				<div class="messages">
				
				<div class="buttonRow" id="divMessagesSentButtonRow" runat="server"><div class="text">Selected:</div> 
					<div class="button"><asp:linkbutton class="btnxsmall grey" id="btnDeleteSentMessage" runat="server" onclick="btnDeleteMessageSent_Click">Delete</asp:linkbutton></div>
					<ajaxToolkit:modalpopupextender id="mpeDeleteMessageSent" BackgroundCssClass="facebox_overlay facebox_overlayBG" CancelControlID="btnCancelConfirm" runat="server" popupcontrolid="pnlConfirmation" targetControlId="btnShowMsgSentPopup"></ajaxToolkit:modalpopupextender>
				</div>

				<asp:repeater runat="server" id="rptMessagesSent" onitemcommand="rptMessagesSent_ItemCommand">  
					<headertemplate>
						<div class="header">
							<div class="select">Select:<br><a href="javascript:void(0);" onclick="Select_All(true);">All</a> | <a href="javascript:void(0);" onclick="Select_All(false);">None</a></div>
							<div class="profile">To</div>
							<div class="subject">Subject</div>
						</div>
					</headertemplate>
					<itemtemplate>
						<div class="row" runat="server">												
							<div class="chkBox"><asp:checkbox runat="server" id="chkMSEdit" /></div>
							<div class="thumb">	
								<div class="framesize-xsmall">
									<div class="frame">
										<span class="ct"><span class="cl"></span></span>
											<div class="imgconstrain">
												<a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "CommunityNameNoSpaces").ToString ()) %>><img runat="server" src='<%# GetProfileImageURL (DataBinder.Eval(Container.DataItem, "thumbnailSquarePath").ToString (), "sq", "M", Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "OwnerFacebookSettings.UseFacebookProfilePicture")), Convert.ToUInt64(DataBinder.Eval(Container.DataItem, "OwnerFacebookSettings.FacebookUserId")) ) %>' border="0" width="30" id="Img7"/></a>
											</div>
										<span class="cb"><span class="cl"></span></span>
									</div>
								</div>		
								<input type="hidden" runat="server" id="hidMessageSentId" value='<%#DataBinder.Eval(Container.DataItem, "MessageId")%>' name="hidMessageSentId" />
								<input type="hidden" runat="server" id="Hidden2" value='<%#DataBinder.Eval(Container.DataItem, "ToViewable")%>' name="hidToViewable" />
							</div>			
							<div class="profile">
								<a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "CommunityNameNoSpaces").ToString ()) %>><%# DataBinder.Eval(Container.DataItem, "UserName") %></a>
								<div><%# FormatDateTimeSpan (DataBinder.Eval(Container.DataItem, "MessageDate"), DataBinder.Eval (Container.DataItem, "CurrentDate")) %></div>
							</div>													
							<div class="subject"><asp:linkbutton id="btnMsgSentDetails" runat="server" commandname="Details" runat="server"><%# TruncateWithEllipsis (DataBinder.Eval(Container.DataItem, "Subject").ToString (), 62) %></asp:linkbutton></div>
						</div>														 
					</itemtemplate>
					<footertemplate></footertemplate>
				</asp:repeater>

 				<div class="pagerContainer">
					<div class="results"><asp:label runat="server" id="lblMessagesSentResults" /></div>
					<kaneva:pager runat="server" isajaxmode="False" class="pager" id="pagerMessagesSent" maxpagestodisplay="5" shownextprevlabels="true" />
				</div>
				<asp:Button id="btnShowMsgSentPopup" runat="server" style="display:none" />

				</div>
			</contenttemplate>
		</asp:updatepanel>
		
		<!-- Message Details -->
		<asp:updatepanel visible="false" id="upMessageDetails" childrenastriggers="true" runat="server" updatemode="conditional">
			<contenttemplate>
				<div class="msgDetails">
					<div class="buttonRow"> 
						<div class="button"><asp:linkbutton class="btnxsmall grey" id="btnMessageReplyTop" runat="server" onclick="btnMessageReply_Click">Reply</asp:linkbutton></div>
						<div class="button"><asp:linkbutton class="btnxsmall grey" id="btnDeleteMessageDetailTop" runat="server" onclick="btnDeleteMessageDetail_Click">Delete</asp:linkbutton></div>
						<div class="report" id="divSpanBlockTop" runat="server">
							<asp:linkbutton id="btnMDReportAsSpamTop" runat="server" onclick="btnReportSpam_Click">Report as Spam</asp:linkbutton>
							<asp:linkbutton id="btnMDBlockUserTop" runat="server" onclick="btnBlockUser_Click">Block User</asp:linkbutton>
						</div>
						<div class="btnBack"><asp:linkbutton class="btnmedium grey" id="btnMessageDetailBackTop" runat="server" oncommand="btnBackToMessages_ItemCommand">< Back to Messages</asp:linkbutton></div>
						<ajaxToolkit:modalpopupextender id="mpeDeleteMessageDetail" BackgroundCssClass="facebox_overlay facebox_overlayBG" CancelControlID="btnCancelConfirm" runat="server" popupcontrolid="pnlConfirmation" targetControlId="btnShowMsgDetailPopup"></ajaxToolkit:modalpopupextender>
					</div>

					<div class="msgHeader">
						<div class="thumb">	
							<div class="framesize-square">
								<div class="frame">
									<span class="ct"><span class="cl"></span></span>
										<div class="imgconstrain">
											<a id="aFromThumb" runat="server"><img runat="server" border="0" id="imgThumb"/></a>
										</div>
									<span class="cb"><span class="cl"></span></span>
								</div>
							</div>		
							<input type="hidden" runat="server" id="hidDetailMessageId" value="" name="hidDetailMessageId" />
							<input type="hidden" runat="server" id="hidDetailMessageType" value="" name="hidDetailMessageType" />
						</div>			
						<div class="data">
							<div class="from"><div class="label">From:</div><div><asp:linkbutton id="btnFrom" runat="server"></asp:linkbutton></div></div>
							<div class="date"><div class="label">Date:</div><div><asp:label id="lblDate" runat="server"></asp:label></div></div>
							<div class="subject"><div class="label">Subject:</div><div><asp:label id="lblSubject" runat="server"></asp:label></div></div>
						</div>
					</div>
					<div class="msgBody" id="divBody" runat="server"></div>
 					
					<div class="buttonRow bottom" style="text-align:left;"> 
						<div class="button"><asp:linkbutton class="btnxsmall grey" id="btnMessageReplyBtm" runat="server" onclick="btnMessageReply_Click">Reply</asp:linkbutton></div>
						<div class="button"><asp:linkbutton class="btnxsmall grey" id="btnDeleteMessageDetailBtm" runat="server" onclick="btnDeleteMessageDetail_Click">Delete</asp:linkbutton></div>
						<div class="report" id="divSpanBlockBtm" runat="server">
							<asp:linkbutton id="btnMDReportAsSpamBtm" runat="server" onclick="btnReportSpam_Click">Report as Spam</asp:linkbutton>
							<asp:linkbutton id="btnMDBlockUserBtm" runat="server" onclick="btnBlockUser_Click">Block User</asp:linkbutton>
						</div>
						<div class="btnBack"><asp:linkbutton class="btnmedium grey" id="btnMessageDetailBackBtm" runat="server" oncommand="btnBackToMessages_ItemCommand">< Back to Messages</asp:linkbutton></div>
					</div>

					<asp:Button id="btnShowMsgDetailPopup" runat="server" style="display:none" />
				</div>
			</contenttemplate>
		</asp:updatepanel>																		  

		<!-- Message Editor -->
		<asp:updatepanel visible="false" id="upMessageEditor" childrenastriggers="true" runat="server" updatemode="conditional">
			<contenttemplate>
				<div class="msgEditor">	
					<div class="buttonRow"> 
						<div class="button"><asp:linkbutton class="btnxsmall grey" id="btnMessageSendTop" runat="server" onclick="btnSendMessage_Click">Send</asp:linkbutton></div>
						<div class="button"><asp:linkbutton class="btnxsmall grey" id="btnMessageCancelTop" runat="server" onclick="btnCancelEditor_Click">Cancel</asp:linkbutton></div>
					</div>
					<div class="msgHeader">															   
						<div class="data">
							<div class="to"><div class="label">To:</div><div><asp:linkbutton id="btnToMsgEditor" runat="server"></asp:linkbutton><asp:textbox id="txtToMsgEditor" runat="server" visible="false"></asp:textbox></div></div>
							<div class="date"><div class="label">Date:</div><div><asp:label id="lblDateMsgEditor" runat="server"></asp:label></div></div>
							<div class="subject"><div class="label">Subject:</div><div><asp:textbox id="txtSubjectMsgEditor" runat="server"></asp:textbox></div></div>
						</div>
					</div>																			   
					<div class="editor">
						<ce:editor id="ceEditor" runat="server" maxtextlength="100000" bgcolor="#ededed" width="768px" 
							height="270px" showhtmlmode="False" ShowBottomBar="False" enablestripscripttags="true" AllowPasteHtml="false" EnableStripIframeTags="true"
							enableobjectresizing="false" autoconfigure="None" EnableStripLinkTagsCodeInjection="true" EnableStripStyleTagsCodeInjection="true"     
							configurationpath="~/CuteSoft_Client/CuteEditor/Configuration/AutoConfigure/message.config" ></ce:editor>
					</div>							  	   
					<div class="buttonRow bottom" style="text-align:left;"> 
						<div class="button"><asp:linkbutton class="btnxsmall grey" id="btnMessageSendBtm" runat="server" onclick="btnSendMessage_Click">Send</asp:linkbutton></div>
						<div class="button"><asp:linkbutton class="btnxsmall grey" id="btnMessageCancelBtm" runat="server" onclick="btnCancelEditor_Click">Cancel</asp:linkbutton></div>
					</div>
					<input type="hidden" runat="server" id="hidEditorMessageId" value="" name="hidEditorMessageId" />
				</div>								 
			</contenttemplate>						 
		</asp:updatepanel>

		<!-- Community Requests -->
		<asp:updatepanel visible="false" id="upCommunityRequests" childrenastriggers="true" runat="server" updatemode="conditional">
			<contenttemplate>
				<div class="communityRequests">
				
				<div class="buttonRow" id="divCommunityReqButtonRow" runat="server"><div class="text">Selected:</div> 
					<div class="button"><asp:linkbutton class="btnxsmall grey" id="btnApproveMember" runat="server" onclick="btnApproveMember_Click">Approve</asp:linkbutton></div>
					<div class="button"><asp:linkbutton class="btnxsmall grey" id="btnIgnoreMember" runat="server" onclick="btnIgnoreMember_Click">Ignore</asp:linkbutton></div>
					<ajaxToolkit:modalpopupextender id="mpeIgnoreMembers" BackgroundCssClass="facebox_overlay facebox_overlayBG" CancelControlID="btnCancelConfirm" runat="server" popupcontrolid="pnlConfirmation" targetControlId="btnShowCRPopup"></ajaxToolkit:modalpopupextender>
				</div>

				<asp:repeater runat="server" id="rptCommunityRequests" onitemcommand="rptCommunityRequests_ItemCommand">  
					<headertemplate>
						<div class="header">
							<div class="select">Select:<br><a href="javascript:void(0);" onclick="Select_All(true);">All</a> | <a href="javascript:void(0);" onclick="Select_All(false);">None</a></div>
							<div class="profile">Friend Profile</div>
							<div class="community">World</div>
							<div class="age">Age</div>
							<div class="gender">Gender</div>
							<div class="location">Location</div>
							<div class="action">Actions</div>
						</div>
					</headertemplate>
					<itemtemplate>
						<div class="row">												
							<div class="chkBox"><asp:checkbox runat="server" id="chkCREdit" /></div>
							<div class="thumb">	
								<div class="framesize-xsmall">
									<div class="frame">
										<span class="ct"><span class="cl"></span></span>
											<div class="imgconstrain" style="text-align:left;">
												<a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "name_no_spaces").ToString ()) %>><img runat="server" src='<%# GetProfileImageURL (DataBinder.Eval(Container.DataItem, "thumbnail_square_path").ToString (), "sq", "M", Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "use_facebook_profile_picture")), Convert.ToUInt64(DataBinder.Eval(Container.DataItem, "fb_user_id")) ) %>' border="0" width="30" id="Img4"/></a>
											</div>
										<span class="cb"><span class="cl"></span></span>
									</div>
								</div>		
								<input type="hidden" runat="server" id="hidUserId" value='<%#DataBinder.Eval(Container.DataItem, "user_id")%>' name="hidUserId">
								<input type="hidden" runat="server" id="hidChannelId" value='<%#DataBinder.Eval(Container.DataItem, "community_id")%>' name="hidChannelId">
							</div>			
							<div class="profile">
								<a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "name_no_spaces").ToString ()) %>><%# DataBinder.Eval(Container.DataItem, "username") %></a>
								<div><%# FormatDateTimeSpan (DataBinder.Eval(Container.DataItem, "added_date"), DataBinder.Eval (Container.DataItem, "CurrentDate")) %></div>
							</div>
							<div class="community"><a href='<%# GetBroadcastChannelUrl (DataBinder.Eval(Container.DataItem, "community_name_no_spaces").ToString ())%>'><%# TruncateWithEllipsis (DataBinder.Eval(Container.DataItem, "community_name").ToString(), 25)%></a></div>
							<div class="age"><%# ShowAge(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "age"))) %></div>
							<div class="gender"><%# DataBinder.Eval(Container.DataItem, "gender") %></div>
							<div class="location"><%# TruncateWithEllipsis(DataBinder.Eval(Container.DataItem, "location").ToString(), 15) %></div>
							<div class="action">
								<div class="button"><asp:linkbutton class="btnxsmall grey" id="btnApproveMember" runat="server" commandname="cmdApprove">Approve</asp:linkbutton></div>
								<div class="ignore"><asp:linkbutton id="btnDeclineMember" runat="server" commandname="cmdDecline" text="Ignore"></asp:linkbutton></div>
								<ajaxToolkit:modalpopupextender id="mpeMemberReq" BackgroundCssClass="facebox_overlay facebox_overlayBG" CancelControlID="btnCancelConfirm" runat="server" popupcontrolid="pnlConfirmation" targetControlId="btnShowCRPopup"></ajaxToolkit:modalpopupextender>
							</div>									
						</div>									
					</itemtemplate>
					<footertemplate></footertemplate>
				</asp:repeater>

 				<div class="pagerContainer">
					<div class="results"><asp:label runat="server" id="lblCommunityRequestsResults" /></div>
					<kaneva:pager runat="server" isajaxmode="False" class="pager" id="pagerCommunityReq" maxpagestodisplay="5" shownextprevlabels="true" />
				</div>
				<asp:Button id="btnShowCRPopup" runat="server" style="display:none" />

				</div>
			</contenttemplate>
		</asp:updatepanel>
																		 
		<!-- Community Requests Accepted -->
		<asp:updatepanel visible="false" id="upCommunityRequestsAccepted" runat="server" updatemode="conditional">
			<contenttemplate>
				<div class="membersAddedContainer">
					<div id="divNewMembersCount" runat="server" class="membersAddedCount"></div>
					<asp:datalist visible="true" runat="server" showfooter="False" id="dlMembersAdded"
						cellpadding="5" cellspacing="0" repeatcolumns="5" repeatdirection="Horizontal" itemstyle-bordercolor="white" itemstyle-borderwidth="1" 
						itemstyle-horizontalalign="Left" itemstyle-width="142" width="100%" cssclass="membersNew">
						<itemtemplate>
							<div class="wrapper">
								<div class="framesize-medium">
									<a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "NameNoSpaces").ToString ())%>>
									<img runat="server" src='<%#GetProfileImageURL (DataBinder.Eval(Container.DataItem, "ThumbnailSquarePath").ToString (), "sq", DataBinder.Eval(Container.DataItem, "Gender").ToString ())%>' border="0" id="Img3"/>
									</a>
								</div>		
								<div><a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "NameNoSpaces").ToString ())%>><%# TruncateWithEllipsis (DataBinder.Eval(Container.DataItem, "Username").ToString(), 13) %></a></div>
								<div>"<%# TruncateWithEllipsis (DataBinder.Eval(Container.DataItem, "DisplayName").ToString(), 13) %>"</div>
								<div><%# TruncateWithEllipsis (DataBinder.Eval(Container.DataItem, "Location").ToString(), 13) %></div>
							</div>								
						</itemtemplate>
					</asp:datalist>
				</div>
				<div class="buttonRow btnRowBack"><asp:linkbutton id="btnBackToCommunityRequests" class="btnlarge grey" style="padding-top:6px;" runat="server" commandname="CommunityRequests" oncommand="btnBack_Click" >< Back to Approvals</asp:linkbutton></div>
			</contenttemplate>
		</asp:updatepanel>

		<!-- Community Invites -->
		<asp:updatepanel visible="false" id="upCommunityInvites" childrenastriggers="true" runat="server" updatemode="conditional">
			<contenttemplate>
				<div class="communityInvites">
				
				<div class="buttonRow" id="divCommunityInvitesButtonRow" runat="server"><div class="text">Selected:</div> 
					<div class="button"><asp:linkbutton id="btnIgnoreCommInvite" class="btnxsmall grey" runat="server" onclick="btnIgnoreCommInvite_Click">Ignore</asp:linkbutton></div>
					<ajaxToolkit:modalpopupextender id="mpeIgnoreCommInvite" BackgroundCssClass="facebox_overlay facebox_overlayBG" CancelControlID="btnCancelConfirm" runat="server" popupcontrolid="pnlConfirmation" targetControlId="btnShowCIPopup"></ajaxToolkit:modalpopupextender>
				</div>

				<asp:repeater runat="server" id="rptCommunityInvites" onitemcommand="rptCommunityInvites_ItemCommand" onitemdatabound="rptCommunityInvites_ItemDataBound">  
					<headertemplate>
						<div class="header">
							<div class="select">Select:<br><a href="javascript:void(0);" onclick="Select_All(true);">All</a> | <a href="javascript:void(0);" onclick="Select_All(false);">None</a></div>
							<div class="profile">Friend Profile</div>
							<div class="community">World</div>
							<div class="action">Actions</div>
						</div>
					</headertemplate>
					<itemtemplate>
						<div class="row">												
							<div class="chkBox"><asp:checkbox runat="server" id="chkCIEdit" /></div>
							<div class="thumb">	
								<div class="framesize-xsmall">
									<div class="frame" style="width:30px;height:30px;">
										<div class="imgconstrain" style="text-align:left;">
											<a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "name_no_spaces").ToString ()) %>><img runat="server" src='<%# GetProfileImageURL (DataBinder.Eval(Container.DataItem, "thumbnail_square_path").ToString (), "sq", "M", Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "use_facebook_profile_picture")), Convert.ToUInt64(DataBinder.Eval(Container.DataItem, "fb_user_id")) ) %>' border="0" width="30" id="Img5"/></a>
										</div>
									</div>
								</div>		
								<input type="hidden" runat="server" id="hidCommInviteMsgId" value='<%#DataBinder.Eval(Container.DataItem, "message_id")%>' name="hidCommInviteMsgId" />
								<input type="hidden" runat="server" id="hidCommInviteUserId" value='<%#DataBinder.Eval(Container.DataItem, "from_id")%>' name="hidCommInviteUserId" />
								<input type="hidden" runat="server" id="hidCommInviteCommunityId" value='<%#DataBinder.Eval(Container.DataItem, "channel_id")%>' name="hidCommInviteCommunityId" />
							</div>			
							<div class="profile">
								<a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "name_no_spaces").ToString ()) %>><%# DataBinder.Eval(Container.DataItem, "username") %></a>
								<div><%# FormatDateTimeSpan (DataBinder.Eval(Container.DataItem, "message_date"), DataBinder.Eval (Container.DataItem, "CurrentDate")) %></div>
							</div>
							<div class="community"><a id="aCommunityName" runat="server"></a></div>
							<div class="action">
								<div class="button"><asp:linkbutton class="btnxsmall grey" id="btnCommInviteMoreInfo" runat="server" commandname="cmdJoin">Join Now</asp:linkbutton></div>
								<div class="ignore"><asp:linkbutton id="btnIgnoreCommInvite" runat="server" commandname="cmdIgnore" text="Ignore"></asp:linkbutton></div>
								<ajaxToolkit:modalpopupextender id="mpeCommInvite" BackgroundCssClass="facebox_overlay facebox_overlayBG" CancelControlID="btnCancelConfirm" runat="server" popupcontrolid="pnlConfirmation" targetControlId="btnShowCIPopup"></ajaxToolkit:modalpopupextender>
							</div>									
						</div>									
					</itemtemplate>
					<footertemplate></footertemplate>
				</asp:repeater>

 				<div class="pagerContainer">
					<div class="results"><asp:label runat="server" id="lblCommInvitesResults" /></div>
					<kaneva:pager runat="server" isajaxmode="False" class="pager" id="pagerCommInvites" maxpagestodisplay="5" shownextprevlabels="true" />
				</div>
				<asp:Button id="btnShowCIPopup" runat="server" style="display:none" />

				</div>
			</contenttemplate>
		</asp:updatepanel>
																		  
		<!-- Friend Requests -->
		<asp:updatepanel id="upFriendRequests" childreanastriggers="true" runat="server" updatemode="Conditional">
			<contenttemplate>
				<div id="divFriendRequests" style="display:none;" class="friendRequests" runat="server">

				<div class="buttonRow" id="divFriendReqButtonRow" runat="server">
					<div>
						<div class="text">Selected:</div> 
						<div class="button"><asp:linkbutton id="btnApproveFriends" class="btnxsmall grey" runat="server" onclick="btnApproveFriends_Click">Approve</asp:linkbutton></div>
						<div class="button"><asp:linkbutton id="btnIgnoreFriends" class="btnxsmall grey" runat="server" onclick="btnIgnoreFriends_Click">Ignore</asp:linkbutton></div>
						<ajaxToolkit:modalpopupextender id="mpeIgnoreFriends" BackgroundCssClass="facebox_overlay facebox_overlayBG" CancelControlID="btnCancelConfirm" runat="server" popupcontrolid="pnlConfirmation" targetControlId="btnShowFRPopup"></ajaxToolkit:modalpopupextender>
					</div>
					<div class="backLink"><asp:linkbutton id="btnFriendRequestsSent" runat="server" oncommand="btnCPNav_Click" text="Sent Friend Invites &#187;" ></asp:linkbutton></div>
				</div>
								   
				<asp:repeater runat="server" id="rptFriendRequests" onitemcommand="rptFriendRequests_ItemCommand">  
					<headertemplate>
						<div class="header">
							<div class="select">Select:<br><a href="javascript:void(0);" onclick="Select_All(true);">All</a> | <a href="javascript:void(0);" onclick="Select_All(false);">None</a></div>
							<div class="profile">Friend Request Profile</div>
							<div class="age">Age</div>
							<div class="gender">Gender</div>
							<div class="location">Location</div>
							<div class="action">Actions</div>
						</div>
					</headertemplate>
					<itemtemplate>
						<div class="row">
							<div class="chkBox"><asp:checkbox runat="server" id="chkFREdit" /></div>
							<div class="thumb">
								<div class="framesize-xsmall">
									<div class="frame">
										<span class="ct"><span class="cl"></span></span>
											<div class="imgconstrain">
												<a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "name_no_spaces").ToString ()) %>><img id="Img1" runat="server" src='<%# GetProfileImageURL (DataBinder.Eval(Container.DataItem, "thumbnail_square_path").ToString (), "sq", "M", Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "use_facebook_profile_picture")), Convert.ToUInt64(DataBinder.Eval(Container.DataItem, "fb_user_id")) ) %>' border="0" width="30" /></a>
											</div>
										<span class="cb"><span class="cl"></span></span>
									</div>
								</div>		
								<input type="hidden" runat="server" id="hidFriendId" value='<%#DataBinder.Eval(Container.DataItem, "user_id")%>' name="hidFriendId">
							</div>
							<div class="profile">
								<a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "name_no_spaces").ToString ()) %>><%# TruncateWithEllipsis(DataBinder.Eval(Container.DataItem, "display_name").ToString(), 26) %></a>
								<p>"<%# TruncateWithEllipsis(DataBinder.Eval(Container.DataItem, "username").ToString(), 26) %>"</p>
								<div><%# FormatDateTimeSpan (DataBinder.Eval(Container.DataItem, "request_date"), DataBinder.Eval (Container.DataItem, "CurrentTime")) %></div>
							</div>
							<div class="age"><%# ShowAge(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "age"))) %></div>
							<div class="gender"><%# DataBinder.Eval(Container.DataItem, "gender") %></div>
							<div class="location"><%# TruncateWithEllipsis(DataBinder.Eval(Container.DataItem, "location").ToString(), 18) %></div>
							<div class="action">
								<div class="button"><asp:linkbutton id="btnApproveFriend" class="btnxsmall grey" runat="server" commandname="cmdApprove">Approve</asp:linkbutton></div>																			  
								<div class="ignore"><asp:linkbutton id="btnDeclineFriend" runat="server" commandname="cmdDecline" text="Ignore"></asp:linkbutton></div>
								<ajaxToolkit:modalpopupextender id="mpeFriendReq" BackgroundCssClass="facebox_overlay facebox_overlayBG" CancelControlID="btnCancelConfirm" runat="server" popupcontrolid="pnlConfirmation" targetControlId="btnShowFRPopup"></ajaxToolkit:modalpopupextender>
							</div>																																																						   
						</div>
					</itemtemplate>																																																						  
					<footertemplate></footertemplate>
				</asp:repeater>
 
 				<div class="pagerContainer">
					<div class="results"><asp:label runat="server" id="lblFriendRequestsResults" /></div>
					<kaneva:pager runat="server" isajaxmode="False" class="pager" id="pagerFriendReq" maxpagestodisplay="5" shownextprevlabels="true" />
				</div>
 				<asp:Button id="btnShowFRPopup" runat="server" style="display:none" />
												  
				</div>
			</contenttemplate>
		</asp:updatepanel>
													  
		<!-- Friend Requests Accepted -->
		<asp:updatepanel visible="false" id="upFriendRequestsAccepted" runat="server" updatemode="conditional">
			<contenttemplate>
				<div class="friendsAddedContainer">
					<div id="divNewFriendsCount" runat="server" class="friendsAddedCount"></div>
					<asp:datalist visible="true" runat="server" showfooter="False" id="dlFriendsAdded"
						cellpadding="5" cellspacing="0" repeatcolumns="5" repeatdirection="Horizontal" 
						itemstyle-horizontalalign="Left" itemstyle-width="142" width="100%" cssclass="friendsNew">
						<itemtemplate>
							<div class="wrapper">
							<!-- FRIENDS BOX -->
							<div class="framesize-medium">
								<div class="frame">
									<span class="ct"><span class="cl"></span></span>
										<div class="imgconstrain">
											<a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "NameNoSpaces").ToString ())%>>
											<img runat="server" src='<%#GetProfileImageURL (DataBinder.Eval(Container.DataItem, "ThumbnailSquarePath").ToString (), "sq", DataBinder.Eval(Container.DataItem, "Gender").ToString ())%>' border="0" id="Img2"/>
											</a>
										</div>
									<span class="cb"><span class="cl"></span></span>
								</div>
							</div>		
							<div><a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "NameNoSpaces").ToString ())%>><%# TruncateWithEllipsis (DataBinder.Eval(Container.DataItem, "Username").ToString(), 13) %></a></div>
							<div>"<%# TruncateWithEllipsis (DataBinder.Eval(Container.DataItem, "DisplayName").ToString(), 13) %>"</div>
							<div><%# TruncateWithEllipsis (DataBinder.Eval(Container.DataItem, "Location").ToString(), 13) %></div>
							<!-- END FRIENDS BOX -->
							</div>								
						</itemtemplate>
					</asp:datalist>
				</div>
				<div class="buttonRow btnRowBack"><asp:linkbutton id="btnBackToFriendRequests" class="btnmedium grey" runat="server" commandname="FriendRequests" oncommand="btnBack_Click" >< Back</asp:linkbutton></div>
			</contenttemplate>
		</asp:updatepanel>
																														   
		<!-- Friend Requests Sent -->
		<asp:updatepanel id="upFriendRequestsSent" childreanastriggers="true" runat="server" updatemode="Conditional">
			<contenttemplate>
				<div id="divFriendRequestsSent" class="friendRequestsSent" style="display:none;" runat="server">

				<div class="buttonRow" id="divFriendReqSentButtonRow" runat="server">
					<div>
						<div class="text">Selected:</div> 
						<div class="button"><asp:linkbutton id="btnResendFriendReqs" class="btnxsmall grey" runat="server" onclick="btnResendFriendReq_Click">Resend</asp:linkbutton></div>
						<div class="button"><asp:linkbutton id="btnFriendRequestsSentRemove" class="btnxsmall grey" runat="server" onclick="btnFriendRequestsSentRemove_Click">Remove</asp:linkbutton></div>
					</div>
					<div class="backLink"><asp:linkbutton id="btnFriendRequests" runat="server" oncommand="btnCPNav_Click" text="Friend Requests &#187;" ></asp:linkbutton></div>
				</div>
								   
				<asp:repeater runat="server" id="rptFriendRequestsSent" onitemcommand="rptFriendRequestsSent_ItemCommand" onitemdatabound="rptFriendRequestsSent_ItemDataBound">  
					<headertemplate>
						<div class="header">
							<div class="select">Select:<br><a href="javascript:void(0);" onclick="Select_All(true);">All</a> | <a href="javascript:void(0);" onclick="Select_All(false);">None</a></div>
							<div class="profile">Friend Email</div>
							<div class="date">Date Sent</div>
							<div class="blank"></div>
							<div class="status">Status</div>
							<div class="action">Action</div>
						</div>
					</headertemplate>
					<itemtemplate>
						<div class="row">
							<div class="chkBox"><asp:checkbox runat="server" id="chkFRSEdit" /></div>
							<div class="thumb">
								<input type="hidden" runat="server" id="hidInviteId" value='<%#DataBinder.Eval(Container.DataItem, "invite_id")%>' name="hidInviteId">
							</div>
							<div class="profile">
								<%# TruncateWithEllipsis(DataBinder.Eval(Container.DataItem, "email").ToString(), 30) %>
								<div><%# FormatDateTimeSpan (DataBinder.Eval(Container.DataItem, "invited_date"), DataBinder.Eval (Container.DataItem, "CurrentDate")) %></div>
							</div>
							<div class="date"><%# FormatDateOnly(DataBinder.Eval(Container.DataItem, "reinvite_date")) %></div>
							<div class="blank"></div>
							<div class="status"><%# GetStatusName(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "invite_status_id"))) %></div>
							<div class="action">
								<div class="button" id="divResendFriendReqButton" runat="server"><asp:linkbutton id="btnResendFriendReq" class="button" runat="server" commandname="Resend"><span>Resend</span></asp:linkbutton></div>
								<div class="remove"><asp:linkbutton id="btnRemoveFriendReqSent" runat="server" commandname="Remove" text="Remove"></asp:linkbutton></div>
							</div>
						</div>
					</itemtemplate>																																																						  
					<footertemplate></footertemplate>
				</asp:repeater>
 
 				<div class="pagerContainer">
					<div class="results"><asp:label runat="server" id="lblFriendRequestsSentResults" /></div>
					<kaneva:pager runat="server" isajaxmode="False" class="pager" id="pagerFriendReqSent" maxpagestodisplay="5" shownextprevlabels="true" />
				</div>
 				<div class="note">*Invites can be sent once every 14 days.</div>								  
				</div>
			</contenttemplate>
		</asp:updatepanel>

		<!-- Friend Invites -->
		<asp:updatepanel visible="false" id="upFriendInvites" childreanastriggers="true" runat="server" updatemode="Conditional">
			<contenttemplate>
				<div class="friendInvites">

				<div class="buttonRow" id="divFriendInvitesButtonRow" runat="server">
					<div>
						<div class="text">Selected:</div> 
						<div class="button"><asp:linkbutton id="btnResendFriendInvites" runat="server" onclick="btnResendFriendInvites_Click"><span>Resend</span></asp:linkbutton></div>
						<div class="button"><asp:linkbutton id="btnFriendInvitesSentCancel" runat="server" onclick="btnFriendInvitesCancel_Click"><span>Cancel</span></asp:linkbutton></div>
					</div>
					<div class="backLink"><asp:linkbutton id="btnFrInvites" runat="server" oncommand="btnCPNav_Click" text="Friend Requests &#187;" ></asp:linkbutton></div>
				</div>
								   
				<asp:repeater runat="server" id="rptFriendInvites" onitemcommand="rptFriendInvites_ItemCommand" onitemdatabound="rptFriendInvites_ItemDataBound">  
					<headertemplate>
						<div class="header">
							<div class="select">Select:<br><a href="javascript:void(0);" onclick="Select_All(true);">All</a> | <a href="javascript:void(0);" onclick="Select_All(false);">None</a></div>
							<div class="profile">Friend Profile</div>
							<div class="date">Date Sent</div>
							<div class="blank"></div>
							<div class="status">Status</div>
							<div class="action">Action</div>
						</div>
					</headertemplate>
					<itemtemplate>
						<div class="row">
							<div class="chkBox"><asp:checkbox runat="server" id="chkFIEdit" /></div>
							<div class="thumb">
								<div class="framesize-xsmall">
									<div class="frame">
										<span class="ct"><span class="cl"></span></span>
											<div class="imgconstrain" style="text-align:left;">
												<a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "name_no_spaces").ToString ()) %>><img id="Img1" runat="server" src='<%# GetProfileImageURL (DataBinder.Eval(Container.DataItem, "thumbnail_square_path").ToString ()) %>' border="0" width="40" /></a>
											</div>
										<span class="cb"><span class="cl"></span></span>
									</div>
								</div>		
								<input type="hidden" runat="server" id="hidInviteUserId" value='<%#DataBinder.Eval(Container.DataItem, "user_id")%>' name="hidInviteUserId">
								<input type="hidden" runat="server" id="hidInviteSent" value='<%#DataBinder.Eval(Container.DataItem, "request_date")%>' name="hidInviteSent">
							</div>
							<div class="profile">
								<a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "name_no_spaces").ToString ()) %>><%# TruncateWithEllipsis(DataBinder.Eval(Container.DataItem, "username").ToString(), 26) %></a>
								<div><%# FormatDateTimeSpan (DataBinder.Eval(Container.DataItem, "request_date"), DataBinder.Eval (Container.DataItem, "CurrentDate")) %></div>
							</div>
							<div class="date"><%# FormatDateOnly(DataBinder.Eval(Container.DataItem, "request_date")) %></div>
							<div class="blank"></div>
							<div class="status">Outstanding</div>
							<div class="action">
								<div class="button" id="divResendInviteButton" runat="server"><asp:linkbutton id="btnResendInvite" runat="server" commandname="cmdResend"><span>Resend</span></asp:linkbutton></div>
							</div>																																																						   
						</div>
					</itemtemplate>																																																						  
					<footertemplate></footertemplate>
				</asp:repeater>
 
 				<div class="pagerContainer">
					<div class="results"><asp:label runat="server" id="lblFriendInvitesResults" /></div>
					<kaneva:pager runat="server" isajaxmode="False" class="pager" id="pagerFriendInvites" maxpagestodisplay="5" shownextprevlabels="true" />
				</div>
 				<div class="note">*Requests can be sent once every 14 days.</div>								  
				</div>
			</contenttemplate>
		</asp:updatepanel>

		<!-- World Requests -->
		<asp:updatepanel visible="false" id="up3DAppRequests" childreanastriggers="true" runat="server" updatemode="Conditional">
			<contenttemplate>
				<div class="appRequests">

				<asp:repeater runat="server" id="rptAppRequests" onitemcommand="rptAppRequests_ItemCommand" onitemdatabound="rptAppRequests_ItemDataBound">  
					<headertemplate>
						<div class="header">
							<div class="profile">Friend Profile</div>
							<div class="msg">Message</div>
							<div class="action">Action</div>
						</div>
					</headertemplate>
					<itemtemplate>
						<div class="row">
							<div class="thumb">
								<div class="framesize-square">
									<div class="frame">
										<div class="imgconstrain">
											<a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "name_no_spaces").ToString ()) %>><img id="Img1" runat="server" src='<%# GetProfileImageURL (DataBinder.Eval(Container.DataItem, "thumbnail_square_path").ToString ()) %>' border="0" width="40" /></a>
										</div>
									</div>
								</div>		
								<input type="hidden" runat="server" id="hidAppReqMsgId" value='<%#DataBinder.Eval(Container.DataItem, "message_id")%>' name="hidAppReqMsgId">
								<input type="hidden" runat="server" id="hidAppReqFromUserId" value='<%#DataBinder.Eval(Container.DataItem, "from_id")%>' name="hidAppReqFromUserId">
								<input type="hidden" runat="server" id="hidAppReqCommunityId" value='<%#DataBinder.Eval(Container.DataItem, "channel_id")%>' name="hidAppReqCommunityId">
							</div>
							<div class="profile">
								<a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "name_no_spaces").ToString ()) %>><%# TruncateWithEllipsis(DataBinder.Eval(Container.DataItem, "username").ToString(), 26) %></a>
								<div><%# FormatDateTimeSpan (DataBinder.Eval(Container.DataItem, "message_date"), DataBinder.Eval (Container.DataItem, "CurrentDate")) %></div>
							</div>
							<div class="msg" id="divMsg" runat="server"></div>
							<div class="action">
								<span id="reqButtons" runat="server">
								<div class="button btnaccept"><asp:linkbutton id="btnAcceptGo3D" class="btnsmall grey" commandname="Accept" runat="server" >Accept</asp:linkbutton></div>
								<div class="button btndelete"><asp:linkbutton id="btnDelete" commandname="Delete" runat="server" ><span>X</span></asp:linkbutton></div>
								</span>
								<div id="reqIgnoredMsg" clientidmode="Static" runat="server">Request Ignored!</div>
							</div>
							<div class="optionsContainer" id="divOptions" runat="server">
								<asp:linkbutton id="btnBlockSender" runat="server" commandname="BlockUser">Ignore all requests from Sender</asp:linkbutton> 
								<asp:linkbutton id="btnBlockApp" runat="server" commandname="BlockApp">Block all requests from this World</asp:linkbutton>
							</div>																																																						   
						</div>
					</itemtemplate>																																																						  
					<footertemplate></footertemplate>
				</asp:repeater>
 
 				<div class="pagerContainer">
					<div class="results"><asp:label runat="server" id="lblAppRequestsResults" /></div>
					<kaneva:pager runat="server" isajaxmode="False" class="pager" id="pagerAppRequests" maxpagestodisplay="5" shownextprevlabels="true" />
				</div>
 				</div>
			</contenttemplate>
		</asp:updatepanel>

		<!-- Activities -->
		<asp:updatepanel visible="false" id="upActivities" childreanastriggers="true" runat="server" updatemode="Conditional">
			<contenttemplate>
				<div class="activities">
					<kaneva:activities runat="server" isajaxmode="False" class="" id="ucActivities" />
				</div>
			</contenttemplate>
		</asp:updatepanel>
													  


	</div><!-- Right Column -->

</div>

<!-- Ignore Modal Confirmation -->
<asp:Panel ID="pnlConfirmation" runat="server" style="display:none;z-index:500">
	<div class="confirmation">
		<div class="msg" id="divConfirmMessage" runat="server"></div>
		<div class="actions">
			<div class="button"><asp:linkbutton id="btnOkConfirm" runat="server" oncommand="btnOkConfirm_Click"><span>&nbsp;&nbsp;&nbsp;OK&nbsp;&nbsp;&nbsp;</span></asp:linkbutton></div>																			  
			<div class="spacer"></div>
			<div class="button"><asp:linkbutton id="btnCancelConfirm" runat="server"><span>Cancel</span></asp:linkbutton></div>																			  
		</div>
	</div>
</asp:Panel>


<script type="text/javascript">

	function paste(str) {alert('paste() called');
		// get the cute editor instance
		var editor = document.getElementById('<asp:literal id="litEditorId" runat="server" ></asp:literal>');
		var url = 'http://<asp:literal id="litSiteName" runat="server"></asp:literal>' + str;
		// pasting the specified HTML into a range within a editor document 
		editor.PasteHTML('<a href="' + url + '">' + url + '</a>');
	}
</script>


<script type="text/javascript">

function Toggle(obj, sender) {
	if (obj) {
		if (obj.style.display != 'block') {
			obj.style.display = 'block';
			sender.innerText = 'Less Options';
		}
		else {
			obj.style.display = 'none';
			sender.innerText = 'More Options';
		}
	}
}

function ShowMsg_ (objId, showDuration, setTimeout) {
	var obj = $(objId);
	if (obj) {
		obj.style.visibility = "visible";
		if (obj.style.display == "none") {
			obj.style.display = "";
		}
		if (setTimeout)
			window.setTimeout(function () { $(objId).style.display = "none"; }, showDuration);
	}
}

if (Prototype.Browser.IE) {
	document.write("<style>.confirmation{position:absolute;top:25%;left:35%;}</style>");			
}
</script>
<asp:updatepanel id="upJS" runat="server">
	<contenttemplate>
		<asp:literal id="litJS" runat="server"></asp:literal>
	</contenttemplate>
</asp:updatepanel>

</asp:content>
