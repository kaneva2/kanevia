///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections;
using MagicAjax;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for commDrillDown.
	/// </summary>
	public class newGiftMessage : SortedBasePage
	{
		protected newGiftMessage () 
		{
			Title = "Give a Gift";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			// They must be logged in
			if (!Request.IsAuthenticated)
			{
				Response.Redirect (GetLoginURL ());
				return;
			}

			if (!IsPostBack)
			{
				//setup header nav bar
                HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;

				lblSelection.Text = "none";
					
				BindData(1);
			}
		}

		/// <summary>
		/// GetCommunityEditLink
		/// </summary>
		protected string GetCommunityEditLink (int communityId)
		{
			return ResolveUrl ("~/community/commEdit.aspx?communityId=" + communityId);
		}

		/// <summary>
		/// pg_PageChange
		/// </summary>
		private void pg_PageChange (object sender, PageChangeEventArgs e)
		{
			pgTop.CurrentPageNumber = e.PageNumber;
			pgTop.DrawControl ();
			pgBottom.CurrentPageNumber = e.PageNumber;
			pgBottom.DrawControl ();
			BindData (e.PageNumber);
		}

		/// <summary>
		/// Click the search button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void imgSearch_Click (object sender, ImageClickEventArgs e) 
		{
			pgTop.CurrentPageNumber = 1;
			pgBottom.CurrentPageNumber = 1;
			BindData (1);
		}

		/// <summary>
		///  Bind the default data
		/// </summary>
		private void BindDefaultData()
		{
			dgrdCatalog.Visible = false;
			
			pgTop.NumberOfPages = "0";
			pgBottom.NumberOfPages = "0";
			pgTop.DrawControl ();
			pgBottom.DrawControl ();
		}

		/// <summary>
		///  Bind the data
		/// </summary>
		/// <param name="pageNumber"></param>
		private void BindData (int pageNumber)
		{
			CurrentSort = "name";
			CurrentSortOrder = "ASC";

			// Set the sortable columns
			SetHeaderSortText (dgrdCatalog);
			string orderby = CurrentSort + " " + CurrentSortOrder;

			// Set up search
			int pgSize = 4;

			PagedDataTable pds = StoreUtility.Get2DGiftCatalog( orderby, pageNumber, pgSize);

		
			pgTop.NumberOfPages = Math.Ceiling ((double) pds.TotalCount / pgSize).ToString ();
			pgTop.DrawControl ();

			pgBottom.NumberOfPages = Math.Ceiling ((double) pds.TotalCount / pgSize).ToString ();
			pgBottom.DrawControl ();

			dgrdCatalog.DataSource = pds;
			dgrdCatalog.DataBind ();
			dgrdCatalog.Visible = true;
			tblSearchResults.Visible = true;

			// The results
			lblSearch.Text = GetResultsText (pds.TotalCount, pageNumber, pgSize, pds.Rows.Count);
		}

		protected string GetGiftToolTip()
		{
			return "Give this gift";
		}

		# region Event Handlers

		protected void dgrdCatalog_ItemCommand (object sender, DataGridCommandEventArgs  e)
		{
			if (!Request.IsAuthenticated)
			{
				AjaxCallHelper.WriteAlert ("You must be signed to selectd a gift.");
				return;
			}

			lblSelection.Text = e.CommandName.ToString();
			lblGiftId.Text = e.CommandArgument.ToString ();
		}
		/// <summary>
		/// Cancel Event Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnCancel_Click (object sender, EventArgs e) 
		{
			Response.Redirect ("~/mykaneva/mailbox.aspx");
		}

		/// <summary>
		/// Update Event Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnUpdate_Click(object sender, EventArgs e) 
		{
			System.Diagnostics.Debug.WriteLine( lblSelection.Text );
			System.Diagnostics.Debug.WriteLine( lblGiftId.Text );

			int toUserId = 0;
			string queryParams = "";

			// Server validation
			if (!Page.IsValid) 
			{
				return;
			}
		
			if (!Request.IsAuthenticated && Request["URL"] == null)
			{
				Response.Redirect (GetLoginURL ());
			}

			// Look up the user id via the username
			toUserId = UsersUtility.GetUserIdFromUsername (txtUsername.Text.Trim ());

			if (toUserId == 0)
			{
				ShowErrorOnStartup ("Invalid username, please select a valid username.");
				return;
			}

			int giftId = 0;
			try
			{
				giftId = Convert.ToInt32( lblGiftId.Text.ToString());
			}
			catch ( FormatException )
			{
				ShowErrorOnStartup ("You must select a gift.");
				return;
			}

			int messageId = UsersUtility.GiveGift( GetUserId (), toUserId, 
				Server.HtmlEncode (txtSubject.Text), 
				txtBody.Text, giftId, true );

			switch ( messageId )
			{
				case UsersUtility.USER_IS_BLOCKED:
					ShowErrorOnStartup ("Message not sent, you are blocked from sending messages to this user!");
					return;

				case UsersUtility.INSUFFICIENT_FUNDS:
					ShowErrorOnStartup ("You do not have enough money to buy that gift!");
					return;

				case UsersUtility.GIVER_IS_RECEIVER:
					ShowErrorOnStartup ("You can't give a gift to yourself!");
					return;
			}
			
			Response.Redirect ("~/mykaneva/mailbox.aspx?" + queryParams);
		}

		#endregion

		/// <summary>
		/// DEFAULT_SORT
		/// </summary>
		/// <returns></returns>
		protected override string DEFAULT_SORT
		{
			get
			{
				return "name";
			}
		}

		protected string GetPrice( string price )
		{
			return price + Constants.CURR_KPOINT_SYMBOL_HTML;
		}

		protected Kaneva.Pager pgTop, pgBottom;

		protected DataGrid dgrdCatalog;

		protected Label lblSearch;

		protected Label lblChannelTags;
		protected HyperLink hlMature;
		protected LinkButton lbPopularMore;

		// Search
		protected CheckBox chkRestricted;
		protected TextBox txtKeywords, txtZipcode;
		protected HtmlAnchor aTooltip;
		protected HtmlGenericControl sRestictedText;
		protected HtmlTable tblSearchResults;

		protected Button btnUpdate;

		protected Label lblSelection, lblGiftId;
		protected TextBox txtSubject, txtUsername, txtEmail;
		protected CuteEditor.Editor txtBody;

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
			pgTop.PageChanged +=new PageChangeEventHandler (pg_PageChange);
			pgBottom.PageChanged +=new PageChangeEventHandler (pg_PageChange);
		}
		#endregion
	}
}
