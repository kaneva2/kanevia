<%@ Page language="c#" Codebehind="general.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.general" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>
<%@ Register TagPrefix="UserControl" TagName="FacebookJS" Src="~/usercontrols/FacebookJavascriptInclude.ascx" %>

<script src="../jscript/prototype.js" type="text/javascript" language="javascript"></script>
<link href="../css/new.css?v1" type="text/css" rel="stylesheet">
<link href="../css/base/buttons_new.css?v1" type="text/css" rel="stylesheet">
<style type="text/css">
.facebook img.fbButton {margin-top:12px;cursor:pointer;clear:both;}
.facebook h2 {margin-bottom:16px;}
.facebook p {font-weight:bold;}
.facebook p, .facebook img {margin-left:15px;}
a.btnxsmall {float:right;padding-top:6px;}
.marright {margin-right:10px;}	
</style>
<usercontrol:FacebookJS runat="server"></usercontrol:FacebookJS>
<script>
	function ShowConfirm() {
		if (confirm('   Are you sure?')) {
			frmMain.submit();	
		}
		else {
			$('rblFacebook_0').checked = true;
			$('rblFacebook_1').checked = false;
			return false;
		}
	}
</script>	
<div id="fb-root"></div>

<table border="0" cellspacing="0" cellpadding="0" class="newcontainer" align="center">
	<tr>
		<td>
			<table  border="0" cellpadding="0" cellspacing="0" width="100%" class="newcontainerborder">
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
				<tr>
					<td class=""><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
					<td valign="top" class="newdatacontainer">
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td colspan="3">
								
									<!-- TOP STATUS BAR -->
									<div id="pageheader">
										<table cellpadding="0" cellspacing="0" border="0" width="99%">
											<tr>
												<td align="left">
													<h1>Account Settings</h1>
												</td>
											</tr>
										</table>
									</div>
									<!-- END TOP STATUS BAR -->		
								
								</td>
							</tr>
							<tr>
								<td width="968" align="left"  valign="top">
								<br>
									<table border="0" cellspacing="0" cellpadding="0" width="100%">
										<tr>
											<td width="469" valign="top">
												<div class="module">
												<span class="ct"><span class="cl"></span></span>
													<h2>Account Information</h2>
													<h3>View and change your name, email and password</h3>
													<br/>
													<ajax:ajaxpanel id="ajpersonalInfo" runat="server">
																						
													<div style="width:90%;">
														<asp:validationsummary cssclass="errBox black" showmessagebox="False" showsummary="True" id="valSum" 
														displaymode="BulletList" headertext="Please correct the following errors:" runat="server" forecolor="black"  />
													</div>
													<br />
																						
													<table cellpadding="5" cellspacing="0" border="0" width="420" class="wizform">
														<!--form content-->
														<tr>
															<td align="right" width="110">Nickname:</td>
															<td>
																<div style="height:20px;float:left;font-size:15px;font-weight:bold;" runat="server" id="divUsername"></div>
																<div style="float:right;"><asp:linkbutton id="btnChangeNickname" class="btntiny grey" runat="server" causesvalidation="False" onclick="btnChangeNickname_Click" text="Change"></asp:linkbutton></div>
																
															</td>
														</tr>
														<tr>
															<td align="right">Name:</td>
															<td valign="middle" valign="middle">
																<asp:textbox id="txtDisplayName" class="biginput" maxlength="30" runat="server" width="220" />
																<asp:requiredfieldvalidator id="rfdisplayname" controltovalidate="txtDisplayName" text="*" errormessage="Display Name is a required field." display="Dynamic" runat="server" />
															</td>
														</tr>
														<tr>
															<td align="right">Email:</td>
															<td>
																<asp:textbox id="txtEmail" class="biginput" maxlength="100" runat="server" width="220" />
																<asp:requiredfieldvalidator id="rfEmail" controltovalidate="txtEmail" text="*" errormessage="Email is a required field." display="Dynamic" runat="server" />
																<asp:regularexpressionvalidator id="revEmail" controltovalidate="txtEmail" text="*" display="Static" errormessage="Invalid email address." enableclientscript="True" runat="server" />
															</td>
														</tr>
														<tr>
															<td align="right">Confirm Email:</td>
															<td>
																<asp:textbox id="txtConfirmEmail" class="biginput" maxlength="100" runat="server" width="220" />
																<asp:comparevalidator id="cmpEmail" controltovalidate="txtConfirmEmail" controltocompare="txtEmail" type="String" operator="Equal" errormessage="Email and Confirm Email must match." text="*" runat="server" />	
															</td>
														</tr>
														<tr>
															<td align="right">Gender:</td>
															<td><asp:label id="lblGender" runat="server"></asp:label></td>
														</tr>
														<tr>
															<td align="right">Date of Birth:</td>
															<td><asp:label id="lblDOB" runat="server"></asp:label></td>
														</tr>
														<tr>
															<td align="right">Zip:</td>
															<td>
																<asp:textbox id="txtPostalCode" class="biginput" size="16" maxlength="25" runat="server"/>
																<asp:requiredfieldvalidator id="rftxtPostalCode" controltovalidate="txtPostalCode" text="*" errormessage="Postal code is a required field." display="Dynamic" enabled="False" runat="server"/>
															</td>
														</tr>
														<tr>
															<td align="right">Country:</td>
															<td>
																<asp:dropdownlist runat="server" class="biginput" id="drpCountry" style="width:150px">
																</asp:dropdownlist><asp:requiredfieldvalidator id="rfdrpCountry" controltovalidate="drpCountry" text="*" errormessage="Country is a required field." display="Dynamic" runat="server"/>
															</td>
														</tr>
														<tr><td colspan="2">
															<a href="javascript:void(0)" onclick="showPW(this);">Change Password</a>
															<asp:checkbox id="cbPassword" runat="server" style="display:none;" /></td></tr>
														<tr id="trNewPW" style="display:none;">
															<td align="right">
                                                                <asp:label runat="server" id="lbl_Newpassword">New Password:</asp:label>
                                                            </td>
															<td align="left" valign="middle">
																<asp:textbox id="txtPassword" textmode="Password" class="biginput" maxlength="20" runat="server" width="220" />
																<asp:regularexpressionvalidator id="revPassword" runat="server" text="*" errormessage="Password contains illegal characters or is too short. Password must contain only letters, numbers, and underscore,<BR> and must contain at least 4 characters." display="Dynamic" controltovalidate="txtPassword" />	
															</td>
														</tr>
														<tr id="trConfirmPW" style="display:none;">
															<td align="right">
															    <asp:label runat="server" id="lbl_Confirmpassword">Confirm Password:</asp:label>
															</td>
															<td>
																<asp:textbox id="txtConfirmPassword" textmode="Password" class="biginput" maxlength="20" runat="server" width="220" />
																<asp:comparevalidator text="*" id="cmpPassword" controltovalidate="txtConfirmPassword" controltocompare="txtPassword" type="String" operator="Equal" errormessage="Password and Confirm Password must match." runat="server" />	
															</td>
														</tr>										 
														<tr>
															<td colspan="2" align="right">
																<asp:linkbutton id="btnUpdate" class="btnxsmall grey" runat="server" onclientclick="javascript:$(txtDisplayName).value = $(txtDisplayName).value.stripTags().replace('<','').replace('>','');" onclick="btnUpdate_Click" text="Submit"></asp:linkbutton>
															    <asp:linkbutton id="btnCancel" class="btnxsmall grey marright" runat="server" causesvalidation="False" onclick="btnCancel_Click" text="Cancel"></asp:linkbutton>
															</td>
														</tr>
														<!-- end form-->								   
													</table>
													
													</ajax:ajaxpanel>
												
														
													<span class="cb"><span class="cl"></span></span>
													</div>
																		
												</td>
												<td width="30"></td>
												<td width="469" align="center"  valign="top">
													<div class="module">
													<span class="ct"><span class="cl"></span></span>
													<h2>Email Verification</h2>													
													<br>													
													<h2 class="failure"><asp:label runat="server" id="lblEmailValidation" /></h2>
													<table cellpadding="10" cellspacing="0" width="93%" border="0" align="center" class="wizform">
														<tr>
															<td valign="top" align="center" colspan="2">
																<asp:button id="btnRegEmail" align="right" runat="Server" onclick="btnRegEmail_Click" class="biginput" text="&nbsp;&nbsp;&nbsp;Send Verification Email&nbsp;&nbsp;&nbsp;" causesvalidation="False" />
															</td>
														</tr>
														<tr>
															
															<td align="left">
																
																<div id="divValidateMsg" runat="server" visible="false">
																<p>Get an easy 1000 Rewards!</p>
                                                                
                                                                <p>When you joined Kaneva, we sent a verification message to the email address you provided. 
                                                                To verify your email address, open the email message and click the link.</p>
                                                                
                                                                <p>Can't find it? Check your junk mail folder. Add kaneva@kaneva.com to your 
                                                                Address Book. Click the "Send Verification Email" button above again and we'll 
                                                                send you a new one.</p>                                                                
                                                                </div>
                                                                
                                                                <div id="divValidateAgainMsg" runat="server" visible="false">																                                                                
                                                                <p>To verify your new email address, click the Send Verification Email button. Then, open the email message and click the link.</p>
                                                                
                                                                <p>Can't find it? Check your junk mail folder. Add kaneva@kaneva.com to your 
                                                                Address Book. Click the "Send Verification Email" button above again and 
                                                                we'll send you a new one.</p>                                                                
                                                                </div>
                                                                
                                                                <div id="divVerifyBouncedMsg" runat="server" visible="false">																                                                                
                                                                <p>We were unable to contact you.</p>
                                                                
                                                                <p>Your Kaneva email is bouncing, which means you're not receiving special offers and the latest news and updates.</p>
                                                                
                                                                <p>Can't find it? Check your junk mail folder (you may also want to add kaneva@kaneva.com 
                                                                to your address book).</p>  
                                                                
                                                                <p>Why is your email bouncing? 
                                                                <ul >
                                                                    <li style="list-style-type:disc;">Your email address is no longer valid: Provide a new address under 
                                                                    Account Information on the right. </li>
                                                                    <li style="list-style-type:disc;">Your mailbox is full: Delete messages from your Inbox. Then, click 
                                                                    Send Verification Email.</li>
                                                                    <li style="list-style-type:disc;">Your spam filter is filtering your Kaneva mail: Add kaneva@kaneva.com 
                                                                    to your email Address Book.</li>
                                                                    <li style="list-style-type:disc;">Your Internet Service Provider (ISP) is blocking email from Kaneva: 
                                                                    Contact your ISP and tell them you want to receive email from Kaneva. As 
                                                                    their customer, you have the right to receive messages you have opted in 
                                                                    for. Adding Kaneva to their whitelist takes only a few minutes.</li>
                                                                </ul></p>                                                                                                                          
                                                                </div>

															</td>
														</tr>
													</table>
																				
													<span class="cb"><span class="cl"></span></span>
													</div>
													
													<div class="module facebook" style="text-align:left;">
													<span class="ct"><span class="cl"></span></span>
														<h2>Facebook Settings</h2> 
														
														<span id="spnFBNotConnected" runat="server" visible="false">
															<p>Connect your Kaneva account to Facebook to share updates<br />and add friends! Earn 1000 Rewards or more!</p>
															<img class="fbButton" onclick="FBLogin(FBAction.CONNECT);" src="~/images/facebook/FB_ConnectwithFriends.png" runat="server" />
														</span>
														
														<span id="spnFBConnected" runat="server" visible="false">
															<p>Connected to Facebook</p>
															<asp:radiobuttonlist id="rblFacebook" runat="server" autopostback="true"
																repeatcolumns="2" repeatdirection="horizontal" cellpadding="10"
																OnSelectedIndexChanged="rblFacebook_SelectedIndexChanged">
																<asp:listitem text="On" value="connect" selected="true"></asp:listitem>
																<asp:listitem text="Off" value="disconnect"></asp:listitem>	
															</asp:radiobuttonlist>
														</span>
														
														<br /><br />	
													<span class="cb"><span class="cl"></span></span>
													</div>					
												</td>
												
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					<td class=""><img runat="server" src="~/images/spacer.gif" id="Img5" width="1" height="1" /></td>
				</tr>
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
				
			</table>
		</td>
	</tr>
</table>

<script type="text/javascript">
function showPW(t)
{
	if ($('trNewPW').style.display == 'none')
	{
		$('trNewPW').style.display = 'block';
		$('trConfirmPW').style.display = 'block'; 
		$('cbPassword').checked = true; 
		t.innerText = 'Hide Password';
	}
	else
	{
		$('trNewPW').style.display = 'none';
		$('trConfirmPW').style.display = 'none';
		$('cbPassword').checked = false;
		t.innerText = 'Change Password';
	}	
}
</script>