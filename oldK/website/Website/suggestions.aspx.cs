///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Net;
using log4net;
using System.Xml;

using System.Text;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for suggestions.
	/// </summary>
	public class suggestions : MainTemplatePage
	{
		protected suggestions () 
		{
		}

		private void Page_Load (object sender, System.EventArgs e)
		{
            // They must be logged in
            if (!Request.IsAuthenticated)
            {
                Response.Redirect(GetLoginURL());
                return;
            }

			//set meta data
			Title = "Kaneva Feedback & Suggestions";
			MetaDataDescription = "<meta name=\"description\" content=\"Want to help us enhance Kaneva? Please use the form below to provide us with your feedback. \" />";

			//setup header nav bar
			HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.NONE;

			// Add a nice message since this action may take a moment to submit the issue.
			btnSubmit.Attributes.Add ("onClick", "if (typeof(Page_ClientValidate) == 'function') {if (Page_ClientValidate()){ShowLoading(true,'Please wait. Your feedback is being submitted...');};}");

			// Show email?
			trEmail.Visible = !Request.IsAuthenticated;

			// Save the referrer URL
			if (!IsPostBack)
			{
				// Default it to feedback.
				string mode = "F";

				if (Request.UrlReferrer != null)
				{
					ViewState ["rurl"] = Request.UrlReferrer.AbsoluteUri.ToString ();
				}
				else
				{
					if (Request.QueryString["rurl"] != null && Request.QueryString["rurl"].ToString() != string.Empty)
					{
						ViewState ["rurl"] = Server.UrlDecode(Request.QueryString["rurl"].ToString());
					}
					else
					{
						ViewState ["rurl"] = "Unknown";
					}
				}

				// Check the mode
				if (Request ["mode"] != null)
				{
					mode = Request ["mode"].ToString ();
				}

				// Show tables based on mode
				switch (mode)
				{
					case "WB":
					{
						// Web bug
						tblWebSite.Visible = true;
						tdWebSiteDesc.Visible = true;
						lblTitle.Text = "Report Abuse";
						break;
					}

					case "B":
					{
						// Web bug
						tblBusiness.Visible = true;
						tdBusinessDesc.Visible = true;
						break;
					}

					case "I":
					{
						// Iris
						tblIris.Visible = true;
						break;
					}
					case "G":
					{
						// Game
						tblGame.Visible = true;
						break;
					}
					default:
					{
						// Includes F, Feedback
						tblFeedback.Visible = true;
						tdFeedbackDesc.Visible = true;
						break;
					}
				}

				// Did they pass in a category?
				if (Request ["category"] != null)
				{
					SetDropDownIndex (drpCategory, Request ["category"].ToString (), true);
				}


			}
		}

		#region Helper Methods
		/// <summary>
		/// Rest the form
		/// </summary>
		private void ResetForm ()
		{
			txtSummary.Text = "";
			txtComment.Text = "";
			txtEmail.Text = "";
			drpCategory.SelectedValue = "";
			chkLanguage.Checked = false;

			chkFBug.Checked = false;
			chkFPage.Checked = false;
			chkFCopyrightContent.Checked = false;
			chkFWokTech.Checked = false;
			chkFInapContent.Checked = false;
			chkFMature.Checked = false;
			chkMature.Checked = false;

			chkIReceive.Checked= false;
			chkIPublish.Checked= false;
			chkIViewing.Checked= false;
			chkIGames.Checked = false;
			chkIGUI.Checked = false;
			chkIOther.Checked = false;

			chkStudioPartner.Checked = false;
			chkDistributionPartner.Checked = false;
			chkTechPartner.Checked = false;
			chkEducationPartner.Checked = false;
			chkKGP.Checked = false;
			chkPress.Checked = false;
			chkAdvertising.Checked = false;
			chkBusMisc.Checked = false;

		}

        private ViolationReport PrepareReport(User user)
        {
            int ampersandIndex = 0;
            int equalsIndex = 0;

            ViolationReport report = new ViolationReport();

            report.CSRNotes = "";
            report.MemberComments = txtComment.Text;
            report.LastUpdated = DateTime.Now;
            report.ReportDate = DateTime.Now;
            report.ReportingUserId = (uint)user.UserId;
            report.ViolationID = (uint)0;
      //      report.ModifiersID = (uint)user.UserId;
            report.ViolationActionTypeId = (uint)ViolationActionType.eVIOLATION_ACTIONS.MONITOR;

            try
            {
                //parse referring URL to determing if a wok item or asset
                string reportedURL = Server.HtmlDecode(ViewState["rurl"].ToString());
                int gIdIndex = reportedURL.IndexOf("gId");
                int assetIndex = reportedURL.IndexOf("assetId");

                if (gIdIndex > 0)
                {
                    //parse to retrieved the global Id
                    equalsIndex = reportedURL.IndexOf('=', gIdIndex);
                    string globalId = reportedURL.Substring(equalsIndex + 1);
                    report.WokItemId = Convert.ToUInt32(globalId);

                    //use global id to retrieve owner id
                    report.OwnerId = (new ShoppingFacade()).GetItem((int)report.WokItemId).ItemCreatorId;
                }
                else if (assetIndex > 0)
                {
                    //parse to retrieve asset Id
                    equalsIndex = reportedURL.IndexOf('=', assetIndex);
                    ampersandIndex = reportedURL.IndexOf('&', assetIndex);
                    string assetId = reportedURL.Substring(equalsIndex + 1, (ampersandIndex - equalsIndex) - 1);
                    report.AssetID = Convert.ToUInt32(assetId);

                    //use asset id to retrieve owner id
                    report.OwnerId = (uint)(new MediaFacade()).GetAsset((int)report.AssetID).OwnerId;
                }
            }
            catch (Exception ex)
            {
                m_logger.Error("Error trying to parse URL for item being reported", ex);
            }

            return report;
        }

		#endregion

		/// <summary>
		/// btnSubmit_Click
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnSubmit_Click (object sender, ImageClickEventArgs e)
		{
			// Server validation
			Page.Validate ();
			if (!Page.IsValid) 
			{
				return;
			}


			try
			{
                User user = KanevaWebGlobals.CurrentUser;

                //prepare report violation object for use
                ViolationReport report = PrepareReport(user);

				// Build the Desscription
				string strChecked = "";

				// Web site bugs
				if (chkLanguage.Checked)
				{
					strChecked += "\n\tViolate KANEVA Language policy";
                    if (report.OwnerId > 0)
                    {
                        report.ViolationTypeId = (uint)ViolationType.eVIOLATION_TYPES.OFFENSIVE;
                        (new ViolationsFacade()).ReportAViolation(report);
                    }
				}
				if (chkMature.Checked)
				{
					strChecked += "\n\tMature Content";
                    if (report.OwnerId > 0)
                    {
                        report.ViolationTypeId = (uint)ViolationType.eVIOLATION_TYPES.MATURE;
                        (new ViolationsFacade()).ReportAViolation(report);
                    }
                }

				// Feedback
				if (chkFInapContent.Checked)
				{
					strChecked += "\n\tPornographic Material";
                    if (report.OwnerId > 0)
                    {
                        report.ViolationTypeId = (uint)ViolationType.eVIOLATION_TYPES.PORNOGRAPHIC;
                        (new ViolationsFacade()).ReportAViolation(report);
                    }
                }
				if (chkFBug.Checked)
				{
					strChecked += "\n\tSoftware Bug";
				}
				if (chkFPage.Checked)
				{
					strChecked += "\n\tTypographical errors";
				}
				if (chkFCopyrightContent.Checked)
				{
					strChecked += "\n\tWOK Download or Install issue";
				}
				if (chkFMature.Checked)
				{
					strChecked += "\n\tInappropriate Content";
                    if (report.OwnerId > 0)
                    {
                        report.ViolationTypeId = (uint)ViolationType.eVIOLATION_TYPES.INNAPPROPRIATE;
                        (new ViolationsFacade()).ReportAViolation(report);
                    }
                }
				if (chkFWokTech.Checked)
				{
					strChecked += "\n\tWOK Technical Issue";
				}

				// Iris 
				if (chkIReceive.Checked)
				{
					strChecked += "\n\tProblem receiving files";
				}
				if (chkIPublish.Checked)
				{
					strChecked += "\n\tPublishing problem";
				}
				if (chkIViewing.Checked)
				{
					strChecked += "\n\tViewing problem";
				}
				if (chkIGames.Checked)
				{
					strChecked += "\n\tPlaying games problem";
				}
				if (chkIGUI.Checked)
				{
					strChecked += "\n\tGUI Problem";
				}
				if (chkIOther.Checked)
				{
					strChecked += "\n\tOther problems";
				}

				// Any checked?
				if (strChecked.Length == 0)
				{
					strChecked = "None";
				}

				string strComments = "User comment = '" + txtComment.Text + "'\n" +
					"Items checked = " + strChecked + "\n" +
					"Submitted by username = '" + user.Username + "'\n" +
					"Linked from '" + ViewState ["rurl"].ToString () + "'\n" +
					"Kaneva Web Version reported from '" + KanevaGlobals.Version () + "'";

				if (Request.IsAuthenticated)
				{
					strComments += "\nEmail = " + user.Email;
				}
				else
				{
					strComments += "\nEmail = " + txtEmail.Text;
				}

				// OLD int issueId = StoreUtility.AddMantisIssue( drpCategory.SelectedValue, txtSummary.Text, strComments );
				//int issueId = StoreUtility.AddMantisIssue( "Feedback", txtSummary.Text, strComments );

                // Replace Mantis with Parature
                string paratureURL = Configuration.ParatureAPIURL; 
                string paratureToken = Configuration.ParatureAPIToken;
                string paratureCustomerId = "";

                WebClient client = new WebClient();
                client.Encoding = System.Text.Encoding.UTF8;
                string response = "";
                string xmlRequest = "";
                System.Text.UTF8Encoding objUTF8 = new System.Text.UTF8Encoding();
                byte[] postByteArray;
                byte[] responseArray;

                // Look up the user in parataure
                responseArray = client.DownloadData (paratureURL + "Customer?email=" + KanevaWebGlobals.CurrentUser.Email + "&" + paratureToken);

                string strResponse = Encoding.UTF8.GetString(responseArray);

                XmlDocument xmlResponse = new XmlDocument();

                string _byteOrderMarkUtf8 = Encoding.UTF8.GetString(Encoding.UTF8.GetPreamble());
                if (strResponse.StartsWith(_byteOrderMarkUtf8)) 
                { 
                        strResponse = strResponse.Remove(0, _byteOrderMarkUtf8.Length); 
                }

                xmlResponse.LoadXml (strResponse);

                // Did we find customer id?
                XmlNodeList enlCustomer = xmlResponse.GetElementsByTagName ("Customer");

                if (enlCustomer.Count > 0)
                {
                    XmlAttribute attId = enlCustomer [0].Attributes["id"];
                    paratureCustomerId = attId.Value;
                }

                // Did we find an existing user?
                if (paratureCustomerId.Length.Equals (0))
                {
                    // Create the user in parature
                    xmlRequest = "<Customer>" +
                        "<Email>" + KanevaWebGlobals.CurrentUser.Email + "</Email>" +
                        "<First_Name>" + KanevaWebGlobals.CurrentUser.FirstName + "</First_Name>" +
                        "<Last_Name>" + KanevaWebGlobals.CurrentUser.LastName + "</Last_Name>" +
                        "<Password>password</Password><Password_Confirm>password</Password_Confirm>" +
                        "<Sla> <Sla id=\"4857\"/> </Sla>" +
                        "<Status><Status id=\"2\"/></Status>" +
                        "<User_Name>" + KanevaWebGlobals.CurrentUser.Username + "</User_Name>" +
                        "</Customer>";

                    postByteArray = System.Text.Encoding.ASCII.GetBytes(xmlRequest);
                    responseArray = client.UploadData(paratureURL + "Customer?" + paratureToken, "POST", postByteArray);
                    response = objUTF8.GetString(responseArray).Trim();

                    xmlResponse = new XmlDocument();

                    strResponse = Encoding.UTF8.GetString(responseArray);

                    if (strResponse.StartsWith(_byteOrderMarkUtf8))
                    {
                        strResponse = strResponse.Remove(0, _byteOrderMarkUtf8.Length);
                    }

                    xmlResponse.LoadXml(strResponse);

                    enlCustomer = xmlResponse.GetElementsByTagName("Customer");

                    if (enlCustomer.Count > 0)
                    {
                        XmlAttribute attId = enlCustomer[0].Attributes["id"];
                        paratureCustomerId = attId.Value;
                    }
                }

                // Create the Ticket
                xmlRequest = "<Ticket>" +
                        "<Ticket_Customer>" +
                            "<Customer id=\"" + paratureCustomerId + "\"></Customer>" +
                        "</Ticket_Customer>" +
                        "<Custom_Field id=\"98090\"> <Option id=\"200726\" selected=\"true\" /> </Custom_Field>" +
                        "<Custom_Field id=\"98091\"> <Option id=\"200731\" selected=\"true\" /> </Custom_Field>" +
                        "<Custom_Field id=\"98099\">" + KanevaGlobals.CleanParatureText (txtSummary.Text) + "</Custom_Field>" +
                        "<Custom_Field id=\"98100\">" + KanevaGlobals.CleanParatureText (strComments) + "</Custom_Field>" +
                    "</Ticket>";

                postByteArray = System.Text.Encoding.ASCII.GetBytes(xmlRequest);
                responseArray = client.UploadData(paratureURL + "Ticket?" + paratureToken, "POST", postByteArray);
                response = objUTF8.GetString(responseArray);

				// It was a success.
				ResetForm ();
				string scriptString = "<script language=JavaScript>";
				scriptString += "alert ('Thank you. Your feedback has been successfully submitted.');";
				scriptString += "</script>";

				if (!ClientScript.IsClientScriptBlockRegistered (GetType (), "Thankyou"))
				{
                    ClientScript.RegisterStartupScript(GetType(), "Thankyou", scriptString);
				}
			}
			catch( Exception ex )
			{
				m_logger.Error ("Error with Parature web service", ex);
				ShowErrorOnStartup ("Error submitting issue to support, please try again.");
			}

		}

		#region Declerations

		protected ImageButton btnSubmit;
		protected PlaceHolder phBreadCrumb;
		protected DropDownList drpCategory;
		protected Label	lblTitle;

		protected TextBox txtSummary, txtComment, txtEmail;

		// All tables
		protected HtmlTable tblFeedback, tblIris, tblGame, tblWebSite, tblBusiness;
		protected HtmlTableCell	tdFeedbackDesc, tdBusinessDesc, tdWebSiteDesc;

		// Business
		protected CheckBox chkStudioPartner, chkDistributionPartner, chkTechPartner, chkEducationPartner, chkKGP, chkPress, chkAdvertising, chkBusMisc;

		// Web site Bug
		protected CheckBox chkLanguage;

		// Feedback
		protected CheckBox chkFBug, chkFPage, chkFCopyrightContent, chkFWokTech, chkFMature, chkNlisted, chkMature, chkFInapContent;

		// Iris
		protected CheckBox chkIReceive, chkIPublish, chkIViewing, chkIGames, chkIGUI, chkIOther;

		protected HtmlTableRow trEmail;

		// Logger
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
