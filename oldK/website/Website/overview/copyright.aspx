<%@ Page language="c#" Codebehind="copyright.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.copyright" %>

<link href="../css/friends.css" rel="stylesheet" type="text/css"/>
<link href="../css/new.css" rel="stylesheet" type="text/css" />		

<table border="0" cellspacing="0" cellpadding="0" class="newcontainer" align="center">
	<tr>
		<td>
			<table  border="0" cellpadding="0" cellspacing="0" width="100%" class="newcontainerborder">
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
				<tr>
					<td class=""><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img2"/></td>
					<td valign="top" class="newdatacontainer">
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
								
									<!-- TOP STATUS BAR -->
									<div id="pageheader">
										<table cellpadding="0" cellspacing="0" border="0" width="99%">
											<tr>
												<td nowrap align="left">
													<table cellpadding="0" cellspacing="0" border="0">
														<tr>
															<td><h1>� Copyright Policy</h1></td>
														</tr>
													</table>
												</td>
												
												<td align="right" valign="middle">
													<table cellpadding="0" cellspacing="0" border="0" width="690">
														<tr>
															<td class="headertout" width="175"></td>
															<td class="searchnav" width="260"></td>
															<td align="left" width="130"></td>
															<td class="searchnav" align="right" width="210" nowrap>
																
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</div>
									<!-- END TOP STATUS BAR -->	
								
								</td>
							</tr>
							<tr>
								<td width="968" align="left"  valign="top">
									<div class="module whitebg">
									<span class="ct"><span class="cl"></span></span>
									<table cellpadding="0" cellspacing="0" border="0"  width="98%">
										<tr>
											
											<td width="230" valign="top">
												<br>
												<table border="0" cellspacing="0" cellpadding="0" width="190">
													<tr>
														<td id="pagenav">
															<ul>
																<li>
																	<a runat="server" href="~/overview/guidelines.aspx">Member Guidelines</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/rulesofconduct.aspx">Rules of Conduct</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/termsandconditions.aspx">Terms & Conditions</a> 
																</li>
																<li>
																	<a class="selected" runat="server" href="~/overview/copyright.aspx">Copyright Policy</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/privacy.aspx">Privacy Policy</a> 
																</li>
																<li>
																	<a runat="server" href="~/community/safety.kaneva">Safety Center</a> 
																</li>
																<li>
																	<a runat="server" id="aHelp" target="_blank">Help Center</a> 
																</li>
																
															</ul>
														</td>
													</tr>																														
												</table>
												
											</td>
											<td valign="top" align="center" width="710">
												
												<table border="0" cellspacing="0" cellpadding="0" width="98%">
													<tr>
														<td>
															<br/>
															
															<div style="text-align:justify;padding-right:10px;">

<p>
Kaneva respects the intellectual property rights of others and we require that our members and merchants 
(collectively �Members�) do the same.  Members may not upload, post, reproduce, link to, distribute or 
store material protected by copyright, trademark, trade secret or other proprietary right, or which would 
violate any right of publicity, right of privacy or other right of any third party, without first obtaining 
permission of the owner of such right.
</p>
<br/>
<p>
Kaneva members remain the copyright owner of all material (software and content) that they provide on the 
community.  Use of the material in a manner which is inconsistent with the terms and conditions set forth 
herein is strictly prohibited.  Do not post, give away or sell any item that is not 100% your own unless 
you have a license to re-distribute.  Copyright infringement will not be permitted and may result in the 
suspension or termination of membership, or removal of a merchant�s store.
</p>
<br/>
<p>
Kaneva requires <a href="#proof">Proof of Trademark</a> registration or application before a member or merchant may offer any 
trademarked product for sale on the community (including, but not limited to, by way of advertising images, 
banners, articles, sales descriptions, title, read me, and licenses).  Proof of permission to use is also 
required in any case where any right of publicity, (such as in the case of famous actors and professional 
athletes,) right of privacy (such as in the case ordinary citizens,) or any other proprietary right of 
any third party is used by Kaneva Members in material uploaded to, posted on, reproduced on, linked to, 
distributed via, or stored on the community.  Members will promptly provide Kaneva with verification of 
any such permission upon Kaneva�s request.
</p>
<br/>
<h2>Kaneva�s Policy Concerning Claims of Copyright Infringement</h2>
<br/>
<p>
Kaneva adheres to the Digital Millenium Copyright Act (DMCA) and other applicable intellectual property laws.  
If you believe that any material on <u>www.kaneva.com</u> (�the Website�) infringes a copyright that you 
own or control you may file a <a href="#proof">Notification of Claimed Infringement</a> with our <a href="#agent">Designated Agent</a>.  
</p>
<br/>
<a name="notify"></a>
<h2>Copyright Notification � Official Notification of Claimed Infringement</h2>
<br/>
<p>
Any notification of claimed infringement (�Notification�) must be submitted in writing to Kaneva�s 
Designated Agent by email, fax or regular mail.  Notifications that comply with the DMCA will assist 
us in quickly responding to your request.  Please include the following information in your written Notification:
<ol>
	<li>Provide information such as your name, address, phone number, and/or email address, 
	which will allow Kaneva to contact you in writing.<br/><br/></li>       

	<li>Identify the copyrighted work that you believe has been infringed or, if you believe 
	multiple copyrighted works have been infringed, a representative list of such works.<br/><br/></li>   

	<li>Identify the material, activity or product that you claim is infringing the copyrighted 
	work(s) listed in your response to No.1 above, and provide information reasonably sufficient 
	for us to locate the material, such as the URL.<br/><br/></li> 

	<li>Include the following statement:  �I have a good faith belief that use of the material 
	set forth above is not authorized by the copyright owner, its agent, or the law.�<br/><br/></li> 
  
	<li>Include the following statement:  �The information in the notification is accurate, and 
	I swear, under penalty of perjury, that I am authorized to act on behalf of the owner of an 
	exclusive right that is allegedly infringed.�<br/><br/></li>

	<li>Apply your physical or electronic signature to the Notification.<br/><br/></li>

	<li>Send the written Notice to Kaneva�s <a href="#agent">Designated Agent</a>.<br/><br/></li>  
</ol>
</p>

<p>
Failure to include all of the above-listed information in your Notification may result in a 
delay of the processing of your request or our inability to process your request at all.  
If you knowingly materially misrepresent that the challenged product is infringing your 
copyright(s) you may be liable for damages, including costs and attorney�s fees incurred by 
Kaneva and/or the alleged infringer.  If you have any question that the challenged product 
infringes your intellectual property rights, please consult with an attorney.
</p>
<br/>
<a name="agent"></a>
<h2>Designated Agent</h2>
<br/>
<p>
Kaneva has designated an agent to receive notifications of claimed infringement.  Kaneva�s 
Designated Agent for notifications of claimed infringement can be reached as follows: 
</p>
<br/>
<p>
<span style="padding-left:20px">By Mail:</span>   
<span style="padding-left:7px">Director of Finance</span><br/>
<span style="padding-left:80px">Kaneva, Inc.</span><br/>
<span style="padding-left:80px">270 Carpenter Drive</span><br/>
<span style="padding-left:80px">Sandy Springs, GA 30328</span><br/>
</p>

<p style="padding-left:20px">By Phone:  678-367-0532

<p style="padding-left:20px">By Fax: 770-352-0077</p>

<p style="padding-left:20px">By Email: <a href="mailto:copyrights@kaneva.com">copyrights@kaneva.com</a></p>
<br/>
<h2>Notice & Takedown</h2>
<br/>
<p>
Upon receiving a notification of claimed infringement (�the Notification�) that includes the 
information set forth above Kaneva will, within 2 business days of receipt of such Notification, 
suspend the availability of the challenged product in The Kaneva Stores and suspend any payment 
otherwise due the merchant of the challenged product. 
</p>
<br/>
<p>
Promptly after receiving a Notification, Kaneva will send an email communicating the Notification 
to the affected member, or to the merchant of the challenged product and notifying the merchant 
that Kaneva has suspended the availability of the challenged product in The Kaneva Stores and 
suspended any payment otherwise due the merchant of the challenged product. 
</p>
<br/>
<p>
The merchant of the challenged product may then respond by submitting a <a href="#counter">counter notification</a> 
to Kaneva�s Designated Agent. 
</p>
<br/>
<a name="counter"></a>
<h2>Counter Notification</h2>
<br/>
<p>
A Counter Notification that complies with the DMCA (�Counter Notification�) will assist us 
to address your request and resolve the matter promptly.  Please include the following 
information in your written Counter Notification:
<ol>
	<li>Your name, address, telephone number and email address.<br/><br/></li>

	<li>Identify the challenged product the availability of which has been suspended and the 
	location, such as the URL, where the material appeared before access to it was suspended.<br/><br/></li>

	<li>If the address given above is inside the United States, include the following statement:  
	�I consent to the jurisdiction of the Federal District Court for the judicial district in which 
	the address given above is located.  I will accept service of process from the person who 
	provided notification of claimed infringement or an agent of such person.�  
	<br/><br/>
	If the address given above is outside the United States, include the following statement:   
	�I consent to the jurisdiction of the U.S. District Court for the Northern District of Georgia.  
	I will accept service of process from the person who provided notification of claimed infringement 
	or an agent of such person.�<br/><br/></li>

	<li>Include the following statement:  �I swear, under penalty of perjury, that I believe in 
	good faith that the suspension of access to the challenged product was done as the result of 
	mistake or misidentification of the challenged product.�<br/><br/></li>
	
	<li>Apply your physical or electronic signature to the Counter Notification.<br/><br/></li>

	<li>Send the written Counter Notification to Kaneva�s <a href="#agent">Designated Agent</a>.</li> 
</ol>
</p>
<br/>
<p>
Failure to include all of the above-listed information may result in a delay of the processing of 
your request or our inability to process your request at all.  If you knowingly materially misrepresent 
that the challenged product or activity does not infringe the intellectual property rights of others, 
you may be liable for damages, including costs and attorney�s fees.  If you have any question about 
whether or not the challenged product infringes the intellectual property rights of others as claimed 
please consult with your attorney.
</p>
<br/>
<p>
If no Counter Notification is received from the merchant of the challenged product within the ten (10) 
day period set forth above, Kaneva will delete the challenged product from The Kaneva Stores and reserves 
the right to withhold any payment otherwise due the merchant of the challenged product.  Kaneva will 
review all information to determine if the merchant of the challenged product is to be warned, suspended 
or have its merchant status permanently removed pursuant to Kaneva�s <a href="#repeat">Repeat Infringer Policy</a>. 
</p>
<br/>
<p>
If a Counter Notification which complies with the requirements set forth above is made, the Counter 
Notification will be forwarded to the Complaining Party and Kaneva will inform the Complaining Party 
that the challenged product will be made available again in the Kaneva Stores in 10 business days unless 
Kaneva�s Designated Agent first receives notice from the Complaining Party that the Complaining Party 
has filed an action seeking a court order to restrain the merchant of the challenged product from 
engaging in infringing activity related to the challenged product (�an Action�).
</p>
<br/>
<p>
Any notice from the Complaining Party that the Complaining Party has filed an Action should be sent to 
Kaneva�s Designated Agent.  If Kaneva�s <a href="#agent">Designated Agent</a> does not receive such notice from the 
Complaining Party within 10 business days, Kaneva will make the challenged product available again 
in the Kaneva Stores no later than 14 business days after receiving the Counter Notification.
</p>
<br/>
<p>
If Kaneva�s Designated Agent does receive notice that an Action has been commenced, the challenged 
product will be deleted from The Kaneva Stores.  Kaneva shall have the right to withhold any payment 
otherwise due the merchant of the challenged product and shall also have the right to deposit any amount 
due the merchant of the challenged product into an escrow account until the Action is final.  In the event 
that the Merchant of the challenged product prevails in the action, the Merchant may resubmit the product 
to the Kaneva Stores by submitting a copy of the judgment or settlement agreement to Kaneva�s Designated Agent.  
In the event the Merchant is found liable for infringing the intellectual property rights of the Complaining 
Party, the Complaining Party may submit a copy of the judgment or settlement agreement to Kaneva�s Designated Agent 
for the purposes of Kaneva�s <a href="#proof">Repeat Infringer Policy</a>.    
</p>
<br/>
<a name="repeat"></a> 
<h2>Repeat Infringer Policy</h2>
<br/>
<p>
In accordance with the Digital Millennium Copyright Act (DMCA) and other applicable law, it is the policy of 
Kaneva to, in appropriate circumstances, remove the store of any merchant or terminate the membership of any 
member, deemed to be a repeat infringer.  For these purposes, Kaneva will count �infringements� and, subject 
to the terms below, will remove the store of any merchant, or disable the account of any member, who has more 
than one infringement.  
</p>
<br/>
<p>
An infringement will arise every time:  (1) the merchant or member has been found by a court of competent 
jurisdiction to have infringed the copyright(s) of one or more third parties; (2) the merchant or member has 
entered into an agreement acknowledging wrongdoing to settle a claim of copyright infringement; (3) Kaneva has 
actual knowledge, even absent adjudication, that the merchant or member has committed an act of copyright 
infringement; (4) a valid and uncontested notice has been provided to Kaneva alleging facts which constitute a 
violation of Kaneva�s Copyright Policy and/or of U.S. Copyright law by the merchant or member; or (5) the merchant 
or member engages in any other blatant or flagrant infringement(s) of the intellectual property rights of one or more third parties.
</p>
<br/>
<p>
Each infringement claim is handled individually.  For the purposes of this Repeat Infringer Policy, an infringement 
may not be counted if the infringement is extremely minor and inadvertent or accidental.  In such circumstances, the 
merchant or member may be allowed to update the challenged product or item in question and may be put on a 30-day 
probation period.  Such probation could include, when appropriate,  withholding payment for a month (to cover refunds 
of a questionable item), requiring a signed upload agreement to be filed in merchant�s file at the Kaneva office, 
and longer testing time for new products so the company can review them for copyright concerns.  Kaneva may remove 
the merchant�s store or disable the member�s account if any single act of infringement appears particularly severe 
and/or intentional.  When appropriate, buyers will be notified by email from a Kaneva Admin that an item is 
�questionable� for copyright violations and be offered an update and/or a refund by gift certificate.
</p>
<br/>
<p>
In cases in which a single act of infringement appears particularly severe and/or intentional, or when a merchant 
or member is found to have more than one infringement, Kaneva will remove the merchant�s store or disable the 
member�s account as soon as practicable (in the ordinary course of business, within one week) and, when appropriate, 
will notify buyers that the item infringes the intellectual property rights of others and offer the affected buyers 
a refund by gift certificate. Any payment due the merchant will be forfeited and used for refunds and Kaneva�s 
administrative cost for the item�s sale and return.
</p>
<br/>
<p>
Kaneva reserves the right to remove a merchant�s store in The Kaneva Stores or disable the member�s account at 
its sole discretion. Allegations of infringement may form the basis for such removal and/or disabling. Kaneva 
welcomes communication from all parties concerning the products available in The Kaneva Stores and the procedures 
by which Kaneva seeks to protect the intellectual property rights of all parties.  If you believe that copyrighted 
material has been used in violation of this policy or otherwise been made available through the community in a 
manner that is not authorized by the copyright owner, its agent or the law, please follow the instructions for filing 
a <a href="#notify">Notice of Claimed Infringement</a> with Kaneva's 
<a href="#agent">Designated Agent</a>. Similarly, if you wish to bring to Kaneva�s attention 
a judgment of copyright infringement as to one of its merchants or members, you may do so by sending, via registered 
mail, a copy of such judgment to Kaneva�s <a href="agent">Designated Agent</a>.
</p>
<br/>
<h2>Kaneva�s Policy Concerning False Claims</h2>
<br/>
<p>
Under the DMCA, a party who knowingly materially misrepresents that particular material is infringing, or an 
accused party who knowingly materially misrepresents that particular material does not infringe, may be liable 
for damages, including costs and attorney�s fees incurred by Kaneva and the accused merchant or aggrieved copyright 
holder, as the case may be.  If you think but are not sure that certain intellectual property rights are or not 
being infringed, please consult with your attorney.  In addition, repeated copyright notification claims submitted 
by the same member which are properly refuted by the merchant of the challenged product may form the basis for TOS 
violation warning and/or disabling of the member�s account.
</p>
<br/>
<h2>Kaneva's Policy Concerning Trademark Claims of Infringement</h2>
<br/>
<ol>
	<li>Please select the Copyright Notification link above to officially communicate a potential trademark 
	infringement claim. Proof of Trademark has to be added to the official notice.<br/><br/></li>

	<li>Upon receiving an official Notification communication via mail or fax, Kaneva will remove the 
	product from the Kaneva Stores in question within 1 business day. Kaneva's business hours are Mon - Fri 9am to 5pm.<br/><br/></li>

	<li>Emails will be sent to both parties confirming the product's removal from the Kaneva Stores.</li>
</ol>

<div style="padding-top:10px;padding-bottom:10px"><hr style="horizontal-align:center;width:75%;height:1px;color:#000;"/></div>

<a name="proof"></a>
<h2>Proof of Trademark</h2>
<br/>
<p>
Proof of Trademark or Patent is any one of the following:

<ol>
	<li>Receipt for payment of application: Online application payment receipt,
    credit card receipt, or cancelled check<br/></li>
	<li>Letter from attorney with copy of application<br/></li>
	<li>Searchable serial number<br/></li> 
	<li>Searchable word mark.</li> 
</ol>
</p>
<br/>
<p>
Documentation has to be printable to place in the Contributor�s file.
</p>

															</div>
														</td>
													</tr>
												</table>
											</td>
											
										</tr>
									</table>
									<span class="cb"><span class="cl"></span></span>
								</div>
								</td>
								
									</td>
							</tr>
						</table>
					</td>
					<td class=""><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img3"/></td>
				</tr>
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
				<tr>
					<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" id="Img4"/></td>
				</tr>
			</table>
		</td>
	</tr>
</table>

