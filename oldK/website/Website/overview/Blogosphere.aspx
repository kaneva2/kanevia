<%@ Page language="c#" Codebehind="Blogosphere.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.Blogosphere" %>
<!--[if IE]> <style type="text/css">@import "../css/new_IE.css";</style> <![endif]-->
<center>
<table cellpadding="0" cellspacing="0" border="0" width="710">
<tr>
	<td colspan="3" align="left"><a runat="server" href="~/free-virtual-world.kaneva" ID="A4"><img border="0" style="vertical-align:middle;" runat="server" src="~/images/kaneva_logo_150x50.jpg" ID="Img4"/></a> <span style="font-family:arial; font-size: 27px; font-weight:bold;">Corporate Information</span></td>
</tr>
<tr>
	<td align="left" colspan="3"><span style="font-size:6px;"><br><br></span>
		<span style="font-size:6px;"><br><br></span>
	</td>
</tr>
<tr>
	<td>
	<span class="orangeHeader3" style="font-size: 22px;">In the News & Blogosphere</span><br><br>	
	</td>
</tr>
</table>
</center>
<center>
<table cellpadding="0" cellspacing="0" border="0" width="710">
<tr>
	<td class="bodyText"><br>

<img runat="server" src="~/images/press_ogaming.jpg" ID="Img14"><br>	
<B><A class="bulletHead" runat="server" href="http://www.ogaming.com/data/3407~ChrisKlaus.php" target="_resource" ID="A16">Chris Klaus' Kaneva Puts the Power to Create MMOs in Your Hands</A></B><BR><br>	

<img runat="server" src="~/images/press_atlantaBC.gif"><br>	
<B><A class="bulletHead" runat="server" href="http://atlanta.bizjournals.com/atlanta/stories/2005/10/03/story1.html" target="_resource" ID="A15">Georgia aims for new games</A></B><BR><br>	
	
<img runat="server" src="~/images/press_mvillage.jpg" ID="Img12"/><br>	
<B><A class="bulletHead" runat="server" href="http://www.mediavillage.com/jmheard/2005/10/27/buzz10-27-05/" target="_resource" ID="A2">Special Report from Jack Meyers on Broadband Video</A></B><BR><br>

<img runat="server" src="~/images/press_allied.gif" ID="Img11"/><br>
<B><A class="bulletHead" runat="server" href="http://allied.blogspot.com/2005/11/lets-go-to-movies.html" target="_resource" ID="A13">Let�s Go to the Movies</A></B><BR><br>

<img runat="server" src="~/images/press_ncflix.jpg" ID="Img13"/><br>
<B><A class="bulletHead" runat="server" href="http://ncflix.blogspot.com/2005/11/imf-accepting-movie-submissions.html" target="_resource" ID="A14">iMF Accepting Movie Submissions</A></B><BR><br>	
	

<img runat="server" src="~/images/press_gamasutra.gif"/><br>
<B><A class="bulletHead" runat="server" href="http://www.gamasutra.com/php-bin/news_index.php?story=6949" target="_resource" ID="A12">Kaneva Announces MMO Game Platform</A></B><BR><br>

<img runat="server" src="~/images/press_GAtech.jpg"/><br>
<B><A class="bulletHead" runat="server" href="~/overview/garage band approach.pdf" target="_resource" ID="A9">Christopher Klaus Seeks 
to Revolutionize Creation, Delivery of Digital Entertainment</A></B><BR><br>

<img runat="server" src="~/images/press_atlantaBC.gif"/><br>
<B><A class="bulletHead" runat="server" href="~/overview/ABC-Klaus2.pdf" target="_resource" ID="A11">Klaus� Next Big Idea: 
Start Online Film Festival</A></B><BR><br>

<B><A class="bulletHead" runat="server" href="http://www.filmthreat.com/FilmFestivals.asp?Id=1883" target="_resource" ID="A10">Independence MovieFest Online</A></B><BR><br>

<img runat="server" src="~/images/press_draven.jpg"/><br>
<B><A class="bulletHead" runat="server" href="http://draven99.blogspot.com/2005/10/independence-moviefest-set-to-launch.html" target="_resource" ID="A6">Independence MovieFest Set to Launch</A></B><BR><br>

<img runat="server" src="~/images/press_post.jpg" ID="Img1"/><br>
<B><A class="bulletHead" runat="server" href="http://www.postmagazine.com/ME2/dirmod.asp?sid=&amp;nm=&amp;type=news&amp;mod=News&amp;mid=9A02E3B96F2A415ABC72CB5F516B4C10&amp;tier=3&amp;nid=465144836B1E4D438C0DB4618B931FAD" target="_resource" ID="A7">Independence MovieFest Offers Opportunities for Filmmakers</A></B><BR><br>

<img runat="server" src="~/images/press_geekstreak.png" ID="Img2" class="png" /><br>
<B><A class="bulletHead" runat="server" href="http://geekstreak.com/" target="_resource" ID="A8">MMO Game Creation 
Engine Review</A></B><BR><br>	

<img runat="server" src="~/images/press_velocity.jpg" /><br>
<B><A class="bulletHead" runat="server" href="http://www.starmakercoaching.com/newsletter/sm_0505.html" target="_resource" ID="A5">Kaneva, Christopher Klaus 
Highlighted in <i>Velocity</i></A></B><BR><br>

<img runat="server" src="~/images/press_blogcritics.gif" ID="Img10"/><br>
<B><A class="bulletHead" runat="server" href="http://blogcritics.org/archives/2005/10/24/194044.php" target="_resource" ID="A3">The 
Independence MovieFest is Set to Launch</A></B><BR><br>

<img runat="server" src="~/images/press_cgm.jpg" ID="Img3"/><br>
<B><A class="bulletHead" runat="server" href="http://www.cgonline.com/content/view/656/2/" target="_resource" ID="A1">Christopher Klaus talks to Computer Games Magazine</A></B><BR><br>

<img runat="server" src="~/images/press_mmorpg.png" ID="Img5" class="png" /><br>
<B><A class="bulletHead" runat="server" href="http://www.mmorpg.com/showFeature.cfm/loadFeature/231" target="_resource">Kaneva Developer�s Journal #1: How Kaneva will Impact Game Development</A></B><BR><br>

<img runat="server" src="~/images/press_gta.jpg"/><br>
<B><A class="bulletHead" runat="server" href="http://grandtextauto.gatech.edu/2005/08/26/want-to-make-your-own-mmo/" target="_resource">Kaneva Enables Developers to Get Into the MMO Market Relatively Risk Free </A></B><BR><br>

<img runat="server" src="~/images/press_geekstreak.png" ID="Img6" class="png" /><br>
<B><A class="bulletHead" runat="server" href="http://news.geekstreak.com/content/405/Interview_with_Kaneva_CEO_Christopher_Klaus.html" target="_resource">Geekstreak Spotlights Kaneva CEO, Christopher Klaus</A></B><BR><br>

<img runat="server" src="~/images/press_gameZone.jpg" ID="Img7"/><br>
<B><A class="bulletHead" runat="server" href="http://pc.gamezone.com/news/07_18_05_07_05AM.htm" target="_resource">Create your Own Games with the Kaneva Engine</A></B><BR><br>

<img runat="server" src="~/images/press_mmorpg.png" ID="Img8" class="png" /><br>
<B><A class="bulletHead" runat="server" href="http://www.mmorpg.com/showFeature.cfm/loadFeature/149" target="_resource">Kaneva: An MMO Engine For Everyone</A></B><BR><br>

<img runat="server" src="~/images/press_allied.gif" ID="Img9"/><br>
<B><A class="bulletHead" runat="server" href="http://allied.blogspot.com/2005/06/kaneva-beta-fun-youre-invited.html" target="_resource">Kaneva: A Multi-media Flickr Meets eBay on Steroids�</A></B><BR><br>
 



 



 



</td></tr>
</table>
</center>