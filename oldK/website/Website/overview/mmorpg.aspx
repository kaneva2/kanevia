<%@ Page language="c#" Codebehind="mmorpg.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.overview.mmorpg" %>

<link href="../css/friends.css" rel="stylesheet" type="text/css"/>
<link href="../css/new.css" rel="stylesheet" type="text/css" />		

<table border="0" cellspacing="0" cellpadding="0"  width="990" align="center">
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" id="Img1"/></td>
	</tr>
	<tr>
		<td>
			<table  border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="frTopLeft"></td>
					<td class="frTop"></td>
					<td class="frTopRight"></td>
				</tr>
				<tr>
					<td class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img2"/></td>
					<td valign="top" class="frBgIntMembers">
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
								
									<!-- TOP STATUS BAR -->
									<div id="pageheader">
										<table cellpadding="0" cellspacing="0" border="0" width="99%">
											<tr>
												<td nowrap align="left">
													<table cellpadding="0" cellspacing="0" border="0">
														<tr>
															<td><h1>About Kaneva</h1></td>
														</tr>
													</table>
												</td>
												
												<td align="right" valign="middle">
													<table cellpadding="0" cellspacing="0" border="0" width="690">
														<tr>
															<td class="headertout" width="175"></td>
															<td class="searchnav" width="260"></td>
															<td align="left" width="130"></td>
															<td class="searchnav" align="right" width="210" nowrap>
																
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</div>
									<!-- END TOP STATUS BAR -->	
								
								</td>
							</tr>
							<tr>
								<td width="968" align="left"  valign="top">
									<div class="module whitebg">
									<span class="ct"><span class="cl"></span></span>
									<table cellpadding="0" cellspacing="0" border="0"  width="98%">
										<tr>
											
											<td width="230" valign="top">
												<br>
												<table border="0" cellspacing="0" cellpadding="0" width="190">
													<tr>
														<td id="pagenav">
															<ul>
																<li>
																	<a runat="server" href="~/overview/about-kaneva.aspx" id="A1">About Kaneva</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/social-networking.aspx" id="A2">Social Networking</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/online-community.aspx" id="A3">Online Community</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/virtual-world.aspx" id="A4">Virtual World</a> 
																</li>
																<li>
																	<a class="selected" runat="server" href="~/overview/mmorpg.aspx" id="A5">MMORPG</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/virtual-life.aspx" id="A6">Virtual Life</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/avatar.aspx" id="A7">Avatar</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/anime.aspx" id="A8">Anime</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/artists.aspx" id="A9">Artists</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/media-sharing.aspx" id="A10">Media Sharing</a> 
																</li>
																
															</ul>
														</td>
													</tr>																														
												</table>
												
											</td>
											<td valign="top" align="center" width="710">
												
												
												
												<table border="0" cellspacing="0" cellpadding="0" width="98%">
													<tr>
														<td><BR/>
														<h1>Developers Can Create Exciting Games with Kaneva�s Free MMORPG Platform</h1>
														<br>
<div style="text-align:justify;padding-right:10px;">
														<p>
If you are a game developer, Kaneva invites you to join our Elite Developer Program to take advantage of our 
powerful free MMORPG engine, known as Elite, to design your own "Massive Multiplayer Online Role-Playing Game" 
with ease. Over 1,500 developers are currently using our free online MMORPG offering, and we have over 60 games 
currently in development. In fact, the Virtual World of Kaneva itself was created on and is run on our free MMORPG 
platform! If you would like to build a game for commercial use, Kaneva offers developers commercial licenses as well.</p>
															<br>
															<p>
The Virtual World of Kaneva takes the idea of a free online MMORPG to the next level.  Thousands of people can 
simultaneously have fun, interact and hang out in a totally modern 3D world filled with people, entertainment 
(including movies, videos, music, photos, and casual games) and places to explore.</p>
<br>
<br>
<h2>Check Out Our Free Online MMORPG Platform</h2>
<br>
<p>
Our free online MMORPG platform is the foundation for our ever-growing World of Kaneva. In addition, we offer 
our users a way to express themselves and to find others who share their passions and their hobbies in an engaging 
virtual universe. From Profiles that can be tweaked with drag-and-drop widgets to media-rich Communities that foster 
lively discussions and debates to personalized Avatars that gather at virtual clubs and concerts, Kaneva is taking 
the concept of a free 3D MMORPG and combining it with social networking and digital entertainment to bring it to a new level.</p>
<br>
<p>
<a runat="server" id="joinLink" >Join Kaneva today and take advantage of our free MMORPG platform 
to start developing your own game today</a>.</p>

</div>
														
													
													</td>
													</tr>
												</table>
												
												
												
											</td>
											
										</tr>
									</table>
									<span class="cb"><span class="cl"></span></span>
								</div>
								</td>
								
									</td>
							</tr>
						</table>
					</td>
					<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img3"/></td>
				</tr>
				<tr>
					<td class="frBottomLeft"></td>
					<td class="frBottom"></td>
					<td class="frBottomRight"></td>
				</tr>
				<tr>
					<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" id="Img4"/></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
