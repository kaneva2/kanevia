<%@ Page language="c#" Codebehind="TermsAndConditions.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.TermsAndConditions" %>

<link href="../css/friends.css" rel="stylesheet" type="text/css"/>
<link href="../css/new.css" rel="stylesheet" type="text/css" />		

<table border="0" cellspacing="0" cellpadding="0" class="newcontainer" align="center">
	<tr>
		<td>
			<table  border="0" cellpadding="0" cellspacing="0" width="100%" class="newcontainerborder">
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
				<tr>
					<td class=""><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img2"/></td>
					<td valign="top" class="newdatacontainer">
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
								
									<!-- TOP STATUS BAR -->
									<div id="pageheader">
										<table cellpadding="0" cellspacing="0" border="0" width="99%">
											<tr>
												<td nowrap align="left">
													<table cellpadding="0" cellspacing="0" border="0">
														<tr>
															<td><h1>Rules of Conduct</h1></td>
														</tr>
													</table>
												</td>
												
												<td align="right" valign="middle">
													<table cellpadding="0" cellspacing="0" border="0" width="690">
														<tr>
															<td class="headertout" width="175"></td>
															<td class="searchnav" width="260"></td>
															<td align="left" width="130"></td>
															<td class="searchnav" align="right" width="210" nowrap>
																
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</div>
									<!-- END TOP STATUS BAR -->	
								
								</td>
							</tr>
							<tr>
								<td width="968" align="left"  valign="top">
									<div class="module whitebg">
									<span class="ct"><span class="cl"></span></span>
									<table cellpadding="0" cellspacing="0" border="0"  width="98%">
										<tr>
											
											<td width="230" valign="top">
												<br>
												<table border="0" cellspacing="0" cellpadding="0" width="190">
													<tr>
														<td id="pagenav">
															<ul>
																<li>
																	<a runat="server" href="~/overview/guidelines.aspx">Member Guidelines</a> 
																</li>
																<li>
																	<a class="selected" runat="server" href="~/overview/rulesofconduct.aspx">Rules of Conduct</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/termsandconditions.aspx">Terms & Conditions</a> 
																</li>

																<li>
																	<a runat="server" href="~/overview/copyright.aspx">Copyright Policy</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/privacy.aspx">Privacy Policy</a> 
																</li>
																<li>
																	<a runat="server" href="~/community/safety.kaneva">Safety Center</a> 
																</li>
																<li>
																	<a runat="server" id="aHelp" target="_blank">Help Center</a> 
																</li>
																
															</ul>
														</td>
													</tr>																														
												</table>
												
											</td>
											<td valign="top" align="center" width="710">
												
												<table border="0" cellspacing="0" cellpadding="0" width="98%">
													<tr>
														<td>
															<br/>
															
															<div style="text-align:justify;padding-right:10px;">
<ol>
<li>
While in Kaneva, you must respect the rights of others and their rights to enjoy the world. To this end you may not defraud, harass, threaten, impersonate, give unwanted attention or cause distress to other members.
</li>
<br />
<li>
You may not use, harassing, threatening, harmful, abusive, obscene, defamatory, hateful, racially or ethnically offensive language, in any language.
</li>
<br />
<li>
You may not post, upload harassing, threatening, harmful, abusive, obscene, defamatory, hateful, racially or ethnically offensive imagery or content. All adult material, as defined in article 21 below, must be uploaded and marked as Access Pass content.
</li>
<br />
<li>
While in Kaneva, you may not spam or flood the chat, contests, blasts, comments, or forums with repetitive text or obscene language. You may not use the chat system, the Forums or blasts, to advertise or promote external websites or companies.
</li>
<br />
<li>
You may not use information gathered on Kaneva to spam other Kaneva members with offers of goods or services.
</li>
<br />
<li>
You may not impersonate any employee or representative of Kaneva, any other person or entity, or falsely state or otherwise misrepresent yourself, your age or your affiliation with any person or entity.
</li>
<br />
<li>
You may not upload, post, transmit, share, store or otherwise make available content that would constitute, encourage or provide instructions for a criminal offense, violate the rights of any party, or that would otherwise create liability or violate any local, state, national or international law.
</li>
<br />
<li>
You may not violate any local, state, national, or international laws or regulations.
</li>
<br />
<li>
You must follow the instructions of any Kaneva employee or representative while in Kaneva.
</li>
<br />
<li>
You may not misrepresent your age in any way. This includes mis-representing your age for the purpose of crossing the boundary of the adult/minor threshold of 18 years of age. This includes: your profile, sharing your account with someone younger or older than you, chat in world, private messages, etc.
</li>
<br />
<li>
You may not solicit personal information from anyone under 18 or solicit passwords or personally identifying information for commercial or unlawful purposes.
</li>
<br />
<li>
You may not engage in any predatory or stalking conduct.
</li>
<br />
<li>
You may not post anything that exploits children or minors or participate in a group that exploits children or minors.
</li>
<br />
<li>
You may not post content involving sexualized "age play" (regressive role-play in which one or more characters is a minor) in which any participant, character or role is or may be perceived as being anything other than a consenting adult.
</li>
<br />
<li>
You may not attempt to interfere with, hack into, or decipher any transmissions to or from the servers running the Kaneva service.
</li>
<br />
<li>
You may not attempt to exploit any bug in the Kaneva service for any reason including but not limited to bugs that affect the acquisition of Credits and Rewards. In addition, you will not communicate the existence of any such exploitable bug to other Kaneva users, instead immediately reporting any bug to Kaneva using the feedback function.
</li>
<br />
<li>
You may not release any real-life information about any other members or any Kaneva employees or representatives.
</li>
<br />
<li>
You may not create duplicate threads about the same topic on the forums. This includes threads about other threads that were closed by a forum administrator.
</li>
<br />
<li>
You may not create posts or threads concerning disciplinary action taken against any member of the community, including chat logs, personal messages, bulletins, or emails between that member and the Kaneva employee. While we welcome criticism about what you find in Kaneva, you may not post this criticism as a personal attack against any member or employee of Kaneva. We welcome all feedback and suggestions presented in the proper format because we DO want to create the best experience for all Kaneva members.
</li>
<br />
<li>
Members who are 18 years of age and older and who have purchased Access Pass (the Kaneva Adult only Pass) can post material that adheres to the Access Pass content definition. If you post material that is deemed to be of an adult nature that by rule should be marked as Access Pass material but you did not mark as Access Pass, then that material will be deleted.
</li>
<br />
<li>
The definition of Access Pass content, in the view of Kaneva, contains adult material. Access Pass content may include adult themes, adult activity, and adult languages. Kaneva reserves the right to deem what is and isn't Access Pass material.
</li>
<br />
<li>
Material that will not be allowed, Access Pass format or otherwise, includes but is not exclusive to the following: racial, ethnic or religious slurs or any depiction thereof.
</li>
<br />
<li>
A community owner is responsible for ensuring that all Kaneva Rules of Conduct are met within their community, both on the 2D and 3D sides. Failure to do so may bring about that community�s suspension or possible deletion.
</li>
<br />
<li>
Misuse of the reporting system is not allowed. You may not falsely report members for things they have not done. We do consider this a form of harassment.
</li>
<br />
<li>
You may not post private conversations between yourself and any other Kaneva member, employee or Kaneva representative. These include in world "tells", 2D Private Messages or any other forms of communication within Kaneva. You may not post these on the Forums, on your profile page or upload them as media. You CAN however, take screen shots to send to support if you need to report someone for breaking the Rules of Conduct.
</li>
<br />
<li>
Kaneva fully supports our members trading amongst one another, including using Credits in exchange for jobs performed in the 3D world. However, Kaneva does not allow the advertising or selling of Kaneva currency on any Kaneva Forum, anywhere on the Kaneva 2D web pages or anywhere inside the 3D World of Kaneva. Any transaction of Kaneva currency not conducted through the Kaneva trading system is wholly the responsibility of the parties concerned and not the responsibility or liability of Kaneva.
</li>
<br />
<li>
You may not use the trade window or collision (pushing) to interfere with other members� enjoyment of the 3D world.
</li>
<br />
<li>
Contests blatantly being used to distribute Credits between users will be closed and their Credits reimbursed.
</li>
</ol>
<br />
<strong style="padding-left: 45px;">Kaneva reserves the right to make changes to these rules as deemed necessary.</strong>
														
															</div>
														</td>
													</tr>
												</table>
											</td>
											
										</tr>
									</table>
									<span class="cb"><span class="cl"></span></span>
								</div>
								</td>
							</tr>
						</table>
					</td>
					<td class=""><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img3"/></td>
				</tr>
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
				<tr>
					<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" id="Img4"/></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
