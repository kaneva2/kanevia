<%@ Page language="c#" Codebehind="productsServices.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.productsServices" %>
<center>
<table cellpadding="0" cellspacing="0" border="0" width="710">
<tr>
	<td align="left"><a runat="server" href="~/free-virtual-world.kaneva" ID="A4"><img border="0" style="vertical-align:middle;" runat="server" src="~/images/Kaneva_logo_150x50.jpg" ID="Img4"/></a> <span style="font-family:arial; font-size: 27px; font-weight:bold;">Corporate Information</span></td>
</tr>
<tr>
	<td align="left">
	</td>
</tr>
<tr>
	<td>
	<span class="orangeHeader3">Kaneva Products & Services</span>
	</td>
</tr>

<tr>
<td class="bodyText">
<br><br>
<span class="orangeHeader3">Kaneva.com</span><br><br>

Kaneva.com is the world�s first
online digital entertainment marketplace where customers can quickly download,
watch and own a wide range of DVD-quality movies and videos, play and create
online games, and participate in a dynamic community of entertainment
enthusiasts. The site includes four main sections: Watch, Play, Create and
Publish. Visitors can use Kaneva�s built in searching functionality to locate
content that best meets their interests and RSS News Feeds to automatically
receive updates. Community features include easy participation in forums, blogs
as well as the ability to post reviews and participate in online chats.
<br><br>
<span class="orangeHeader3">Kaneva Game Platform</span> <br><br>

Developed by game industry experts, the Kaneva Game Platform (KGP) is the first Massively
Multi-Player (MMO) downloadable engine for creating state-of-the-art online
games. Using this proven unified platform, game studios, modders and independent game
creators can bring games to market faster with richer content at a much lower
cost.

<p>An end-to-end, fully integrated game system, the feature-rich Kaneva Game Platform
includes a leading graphics engine, A.I. server, client, robust network
scalability, a powerful editor and full scripting. Comprehensive online tutorials
and videos provide extensive support and game creation assistance. Once a game is developed using the
platform, Kaneva then helps distribute the game on Kaneva.com with its Managed
Game Services, connecting the developer to potentially thousands of players and
enabling the developer as well as Kaneva to split the royalties and earn money.</p>
<a runat="server" href="~/overview/Kaneva Game Platform Flyer.pdf" target="resource" ID="A1">Kaneva Game Platform &amp; Services Flyer</a><br><br>
<span class="orangeHeader3">Kaneva Game Studio</span><br><br>


<p>Designed with usability
as its number one goal, the Kaneva Game Studio is an editor that can be used to
quickly and intuitively create worlds and modify and extend any existing
worlds. Kaneva Game Studio imports art assets from 3ds max�.</p>

<span class="orangeHeader3">Kaneva A.I. Server</span><br><br>

<p>The Kaneva A.I. Server plugs into the framework to provide advanced Non-Player Characters (NPC). A.I.
creates an engaging world where there are both friendly and hostile NPCs. The
players can follow emerging stories from friendly NPCs and complete missions
and quests. Hostile NPCs can provide challenges and conflicts along the way.</p>

<span class="orangeHeader3">Kaneva Game Server</span><br><br>


<p>The Kaneva Game Server is secure, highly scalable, modular and optimized to run one or many worlds and
have separate A.I. components plugged into each of them.</p>

<span class="orangeHeader3">Kaneva Game Client</span><br><br>

<p>The Kaneva Game Client includes an integrated 3D graphics engine, physics engine, and scalable and secure
communication layer. The graphics engine provides the latest state-of-the-art
advances in special effects by leveraging DirectX, such as normal mapping,
advanced particle system, and FX shaders.</p>

<span class="orangeHeader3">Kaneva Game Launcher</span><br><br>

<p>The Kaneva Game Launcher is a lightweight background application that installs, updates, and launches
games. Kaneva Game Launcher creates a shortcut and icon on the desktop to the
game for easy game launching.</p>

<span class="orangeHeader3">Kaneva Media Launcher</span><br><br>

<p>Kaneva Media Launcher is the easy
way to publish and manage Kaneva media and assets on your Windows computer. Use
the Media Launcher to easily and reliably upload digital media files as well as
receive and manage content that has been downloaded and purchased. The Kaneva
Media Launcher features secure transfers, error correction, and the auto-resume
if the connection is lost.</p>

<span class="orangeHeader3">Kaneva�Online Game Services</span><br><br>

<p>Kaneva Online Game Services provide comprehensive hosting capabilities for all 
Massively Multi-Player Online Games (MMOG) developed using the Kaneva Game 
Platform. These services also include security, availability, billing, and load 
balancing. Kaneva�s Online Game Services enable game developers to focus on 
creating great games without setting up their own costly infrastructures to 
support such an effort.</p>

<span class="orangeHeader3">Kaneva Studio</span><br><br>

<p>The Kaneva Studio is helping change the way game developers build Massively Multi-player
Online Games (MMOG) by testing and implementing new techniques not only for
game creation but also for minimizing the time to market and resources needed
for building games. Made up of some of the most talented game and creative directors, integrators, animators, modelers and
texture artists in the industry, the Kaneva Studio brings multiplayer online
games and movies to life through the complete life cycle of conception and
pre-production, production and post-production. The Kaneva Studio is the interface to getting new features and requirements in the Kaneva Game Platform by gathering those requirements from partner game
and movie studios using the Kaneva Game Platform. Kaneva Studio�s
flagship MMO game, Gorilla Paintball, available for immediate play on
Kaneva.com features multiple arenas, and a unique underground city.</p>

<span class="orangeHeader3">Kaneva Business Services</span><br><br>

<p>Kaneva Business Serivces include all of the online services needed to successfully and easily earn money and build a
business around selling digital media on Kaneva.</p>

<p>These services include: </p>

<ul><li/>Kaneva Billing for automatic
tracking of all financial matters, including subscriptions and royalties;</ul>

<ul><li/>Kaneva Reporting for audit controls
and automated tracking of the sales and downloads of all content on Kaneva and;</ul>

<ul><li/>Kaneva Promotional Services to help
promote items for sale on Kaneva and includes featured content for added
visibility and sales success.</ul>

<span class="orangeHeader3">Kaneva Library</span><br><br>

<p>The Kaneva Library is a collaborative information portal for Kaneva products. The Library includes
descriptions and tutorial lessons for Kaneva products and services.</p>

<span class="orangeHeader3">Kaneva Channels</span><br><br>

<p>Kaneva Channels allow you to post your digital items for sale, hold discussions via blogs and forums,
and manage a community of users.</p><br><br><br><br>

</td>
</tr>
</table>
</center>