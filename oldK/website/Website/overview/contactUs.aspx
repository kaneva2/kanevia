<%@ Page language="c#" Codebehind="contactUs.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.contactUs" %>

<link href="../css/friends.css" rel="stylesheet" type="text/css"/>
<link href="../css/new.css" rel="stylesheet" type="text/css" />		

<table border="0" cellspacing="0" cellpadding="0"  width="990" align="center">
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="1" height="14"/></td>
	</tr>
	<tr>
		<td>
			<table  border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="frTopLeft"></td>
					<td class="frTop"></td>
					<td class="frTopRight"></td>
				</tr>
				<tr>
					<td class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1"/></td>
					<td valign="top" class="frBgIntMembers">
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
								
									<!-- TOP STATUS BAR -->
									<div id="pageheader">
										<table cellpadding="0" cellspacing="0" border="0" width="99%">
											<tr>
												<td nowrap align="left">
													<table cellpadding="0" cellspacing="0" border="0">
														<tr>
															<td><h1>Contact Us</h1></td>
														</tr>
													</table>
												</td>
												
												<td align="right" valign="middle">
													<table cellpadding="0" cellspacing="0" border="0" width="690">
														<tr>
															<td class="headertout" width="175"></td>
															<td class="searchnav" width="260"></td>
															<td align="left" width="130"></td>
															<td class="searchnav" align="right" width="210" nowrap>
																
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</div>
									<!-- END TOP STATUS BAR -->	
								
								</td>
							</tr>
							<tr>
								<td width="968" align="left"  valign="top">
									<div class="module whitebg">
									<span class="ct"><span class="cl"></span></span>
									<table cellpadding="0" cellspacing="0" border="0"  width="98%">
										<tr>
											
											<td width="230" valign="top">
												<br>
												<table border="0" cellspacing="0" cellpadding="0" width="190">
													<tr>
														<td id="pagenav">
															<ul>
																<li>
																	<a runat="server" href="~/overview/aboutLanding.aspx">About Kaneva</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/vision.aspx">History</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/company.aspx">Leadership</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/ourvalues.aspx">Culture & Values</a> 
																</li>
																<li>
																	<a runat="server" href="~/share/default.aspx">Share Kaneva</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/advertising.aspx">Advertising & Sponsors</a> 
																</li>
																<li>
																	<a runat="server" href="~/community/news.kaneva">News / Press Center</a> 
																</li>
																<li>
																	<a class="selected" runat="server" href="~/overview/contactus.aspx">Contact Us</a> 
																</li>
																
															</ul>
														</td>
													</tr>																														
												</table>
												
											</td>
											<td valign="top" align="center" width="710">
												
												<table border="0" cellspacing="0" cellpadding="0" width="98%">
													<tr>
														<td>
															<br/>
															
															<div style="text-align:justify;padding-right:10px;">

<h2>Thanks for your interest in Kaneva.</h2>

<p>
Kaneva's headquarters are located in the fast growing Perimeter area of North Atlanta. <a href="#directions">Click here</a> to get 
directions.
</p>
<br/>
<p>
Kaneva, Inc.<br/>
270 Carpenter Drive<br/>
Sandy Springs, GA  30328<br/>
phone: (678) 367-0555<br/>
fax: (770) 352-0077<br/> 
</p>

<br/><br/>
<h2>Specific Inquiries</h2>

<p>
<table cellspacing="0" cellpadding="5" border="0">
	<tr>
		<td><a runat="server" id="aHelp" target="_blank">Feedback</a></td>
		<td><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img5"/></td>
		<td>We love hearing from you. Tell us what you think.</td>
	</tr>
	<tr>
		<td><a href="~/careers" runat="server">We're Hiring</a></td>
		<td><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img6"/></td>
		<td>Make inquiries about the latest job openings at Kaneva.</td>
	</tr>
	<tr>
		<td><a href="~/overview/advertising.aspx" runat="server">Advertising</a></td>
		<td><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img7"/></td>
		<td>Make inquiries about our advertising and sponsorship opportunities. </td>
	</tr>
	<tr>
		<td><a href="~/community/news.kaneva" runat="server">News/Press Center</a></td>
		<td><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img8"/></td>
		<td>News from the blogosphere, press releases, Kaneva blog and more.</td>
	</tr>
	<tr>
		<td><a runat="server" id="aHelpCenter" target="_blank">Help Center</a></td>
		<td><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img9"/></td>
		<td>Tutorials, tips and tricks, frequently asked questions, forum and more.</td>
	</tr>
</table>
</p>

<br/><br/><br/><br/>
<h2 id="directions">Directions to Kaneva</h2> 

<p>Follow these directions from Hartsfield-Jackson Airport to our offices in Sandy Springs area of North Atlanta:</p><br/>
<p>By Car:</p>
<ul> 
	<li>Exit Hartsfield Airport and take I-85 North to Atlanta.<br/></li>
	<li>Merge onto I-75/I-85 North through downtown Atlanta.<br/></li>
	<li>Merge onto 85N and take GA 400 North (exit 87).<br/></li>
	<li>Take exit 4B onto I-285 West.<br/></li>
	<li>Keep right at the fork, and at the top of the ramp turn RIGHT.<br/></li>
	<li>Proceed about 2.0 miles and take Exit 27 - Roswell Road.<br/></li>
	<li>Turn right onto Roswell Road and then an immediate right onto Carpenter Drive.<br/></li>
	<li>Turn left into parking lot at 270 Carpenter Drive.</li>
</ul>
<br/>

														
															</div>
														</td>
													</tr>
												</table>
											</td>
											
										</tr>
									</table>
									<span class="cb"><span class="cl"></span></span>
								</div>
								</td>
								
									</td>
							</tr>
						</table>
					</td>
					<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img3"/></td>
				</tr>
				<tr>
					<td class="frBottomLeft"></td>
					<td class="frBottom"></td>
					<td class="frBottomRight"></td>
				</tr>
				<tr>
					<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" id="Img4"/></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
