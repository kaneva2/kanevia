///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Kaneva.overview
{
	/// <summary>
	/// Summary description for social_networking.
	/// </summary>
	public class social_networking : MainTemplatePage
	{
		protected social_networking () 
		{
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here

            joinLink.HRef = ResolveUrl(KanevaGlobals.JoinLocation);
            
            Title = "Social Networking Features";
			MetaDataDescription = "<meta name=\"description\" content=\"Kaneva offers an advanced online social network that goes a step beyond other online social networking sites.\" />";
			MetaDataKeywords = "<meta name=\"keywords\" content=\"social networking sites, online social network, online social networking, free social networking\" />";
			//this one set to empty set at business request
			MetaDataTitle = null;
		}

        protected HtmlAnchor joinLink;

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
