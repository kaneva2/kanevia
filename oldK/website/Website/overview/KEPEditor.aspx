<%@ Page language="c#" Codebehind="KEPEditor.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.KEPEditor" %>
<%@ Register TagPrefix="Kaneva" TagName="HeaderText" Src="../usercontrols/HeaderText.ascx" %>

<Kaneva:HeaderText runat="server" Text="Make Your Own Games"/>

<table cellpadding="5" cellspacing="0" border="0" width="750" ID="Table1">
	<tr>
	<td><img runat="server" src="~/images/spacer.gif" width="10" height="10"/></td>
	<td valign="top" class="bodyText" width="420"><br>
	&nbsp;&nbsp;<span class="assetLink"><font size="+1">Get Published with Kaneva! <sup style="font-size:12px;">TM</sup></font></span><br><br>
<p>
Kaneva is the world's fastest growing digital entertainment marketplace where people can create, market, play and sell games, watch films and other types of digital media. A core component of Kaneva's mission is to empower game studios and developers to quickly and cost-effectively create complex Massively Multi-Player Online Games (MMOGs) of all genres and partner with Kaneva to publish them online to a growing, worldwide audience of gamers.<br><br>
</p>
<p>
Kaneva's unique model represents a paradigm shift in the industry transferring the power and freedom to the game developers responsible for game creation. Kaneva enables immediate, global distribution, with the ability to directly connect with players and earn the industry's highest royalties.
</p>
<a runat="server" href="~/create/developer.aspx?ttCat=create&subtab=dev&communityId=102" class="orangeHeader3">Go To Developer's Corner</a>&nbsp;<a runat="server" href="~/create/developer.aspx?ttCat=create&subtab=dev&communityId=102"><img runat="server" src="~/images/arrow_next.gif" border="0" style="vertical-align:middle;"/></a>
<br><br>


	</td>
	<td align="center" class="dateStamp" valign="top"><br><img runat="server" src="~/images/KGP_01.jpg"/><br><br>
	<img runat="server" src="~/images/msw.gif" ID="Img3" border="0"/><asp:imagebutton runat="server" ID="btnDownload" ImageUrl="~/images/button_WEform.gif" class="Filter2" alt="Download Now" OnClick="btnDownload_Click"/><BR>
	Kaneva Game Platform<br><span class="adminLinks"><asp:Label id="lblVersion" runat="server"/></span><br><a runat="server" target="_resource" class="dateStamp" href="http://docs.kaneva.com/bin/view/Public/KgpChangelog" ID="A1">what's new in this version?</a>

	</td>
	<td><img runat="server" src="~/images/spacer.gif" width="10" height="10"/></td>							
	</tr>
</table>	

	<br>
	<table cellpadding="5" cellspacing="0" border="0" width="750" ID="Table3">
	<tr>
	<td><img runat="server" src="~/images/spacer.gif" width="10" height="10"/></td>
	<td align="center" class="dateStamp" valign="top"><br><br><img runat="server" src="~/images/kaneva-arch-diagram.jpg"/><br>"KANEVA Architecture"<br><br>

	
	</td>
	<td valign="top" class="bodyText" colspan="2">

<span class="assetLink" style="font-size: 14px;">Build Your Game & Leave the Hosting to Us!</span><br><br>
It's a well known fact that in an MMO environment, the complexities begin when the game is launched. Significant resources are needed to build out a scalable network, monitor security, allocate servers, and troubleshoot network issues. In addition, managing the distribution of downloads and patches, handling client support and operating a complete billing and reporting system are also required. Publishing with Kaneva allows game developers to focus on the game content and not on the expense and resource requirements of hosting an MMO.<br><br>

<span class="assetLink" style="font-size: 14px;">Kaneva Game Platform<sup style="font-size:9px;">TM</sup></span><br><br>
The Kaneva Game Platform<sup>TM</sup> is a comprehensive solution designed for end-to-end MMO game development for FPS and RPG titles. The Kaneva Game Platform (KGP) features cutting edge graphics capabilities while providing backend and networking features required for supporting hundreds of thousands of simultaneous players.<br><br>
Combined with best-of-breed MMOG technology, the Kaneva Game Platform (KGP) is a fully integrated and unified system that includes the client launcher, Kaneva Game Studio, artificial intelligence and server components. Using a proven unified platform, studio resources can focus on adding features to their worlds rather than trying to assemble and integrate disparate parts together. Studios get games to market faster with richer content at a much lower cost.
<br><br>
</td>
</tr>
<tr>
<td><img runat="server" src="~/images/spacer.gif" width="20" height="10" ID="Img1"/></td>
<td colspan="3" class="bodyText"><br>

<span class="assetLink" style="font-size: 14px;">Client</span><br><br>
The Kaneva client has an integrated 3D graphics engine, physics engine, and a scalable and secure communication layer. The graphics engine provides the latest state of the art advances in special effects by leveraging DirectX, such as normal mapping, an advanced particle system, and FX shaders.
<br><br>
<span class="assetLink" style="font-size: 14px;">Kaneva Game Studio<sup style="font-size: 9px;">TM</sup></span><br><br>
Designed with usability as its number one goal, the Kaneva Game Studio can be used to quickly and intuitively create worlds and modify and extend any existing worlds. The editor imports art assets from 3ds MAX, MAYA, and Blender.
<br><br>

<span class="assetLink" style="font-size: 14px;">Artificial Intelligence (AI)</span><br><br>

The Kaneva AI component plugs into the framework to provide advanced Non-Player Characters (NPCs). AI creates an engaging world where there are both friendly and hostile NPC's. The players can follow emerging stories from friendly NPC's and complete missions and quests. Hostile NPC's can provide challenges and conflict along the way. <br><br>

<span class="assetLink" style="font-size: 14px;">Server</span><br><br>

The Kaneva server is designed to be secure, highly scalable, modular and is optimized to run one or many worlds and have separate AI components plugged into each of them.<br><br>


<br>

<span class="assetLink" style="font-size:17px;">Features</span><br><br>
The Kaneva Game Platform provides a complete environment to create and run MMO
games. KGP utilizes an extensible patent-pending blade architecture for function-
specific engines that allow for third-party or custom enhancements. The Lua/Python interface provides flexibility for gameplay and AI customization.<br><br>

<span class="assetLink" style="font-size: 14px;">Key Features:</span><br><br>
</td>
</tr>
<tr>
<td><img runat="server" src="~/images/spacer.gif" width="20" height="10" ID="Img2"/></td>
<td class="dateStamp" style="padding-left:40px; line-height: 15px;" valign="top" width="50%">
<span class="assetLink" style="font-size:14px;">MMO Games</span>
<li>MMO Focused Engine</li>
<li>Persistent Worlds</li>
<li>Built-in Network Security</li>
<li>Game Patcher and Client</li>
<br><br>
<span class="assetLink" style="font-size:14px;">Game Hosting and Management</span>
<li>Portal and Billing System</li>
<li>Internet Game Distribution</li>
<li>Community/Forums/Blogs</li>
<li>Thousands of Clients per Server</li>
<li>High Availability Fall-over Servers</li>
<li>Built-in Client/Servers Monitoring</li>
<li>Reporting</li>
<br><br>
<span class="assetLink" style="font-size:14px;">Open Platform</span>
<li>Blade Architecture for Extensibility</li>
<li>SDK's for Creating Blades (C,C++)</li>
<li>Python, Lua Scripting interfaces</li>
<li>3ds MAX, Maya, Blender Support</li>
<li>Subversion Art Asset Management</li>
<br><br>
<span class="assetLink" style="font-size:14px;">Advanced 3D Graphics Engine</span>
<li>Leverages DirectX 9.0c</li>
<li>High Polycount Characters</li>
<li>Vertex Lighting</li>
<li>Normal/Bump Mapping</li>
<li>FX Shader Support</li>
<li>Advanced Particle Effects</li>
<li>Simple and Stencil Shadows</li>
<li>Custom Camera System</li>
<li>Optimizing for MMO Games</li>
<br><br>
<span class="assetLink" style="font-size:14px;">GUI Editor</span>
<li>Intuitive Interface</li>
<li>WYSIWYG Editor</li>
<li>Realtime Rendering</li>
<li>Integrated Particle System</li>
<li>Drag and Drop Menu Editor</li>
<li>Wireframe View</li>
<li>Online Twiki Help/Tutorials</li>
<br><br>
<span class="assetLink" style="font-size:14px;">Scripting</span>
<li>Lua/Python Scripting</li>
<li>C/C++ API</li>
<li>AI, Server and Gameplay</li>
<br><br>
<span class="assetLink" style="font-size:14px;">Character Management</span>
<li>Skinned / Physiqued</li>
<li>Customizable Characters</li>
<li>Skeletal Animations</li>
<li>Inventory System</li>
<li>Auto Waist Bending</li>
<li>Animation Attached Particles</li>
<li>Animation Attached Sounds</li>
<li>Animation Missiles</li>
<li>Extensive LOD system</li>
<br><br>
<span class="assetLink" style="font-size:14px;">Sound Management</span>
<li>Music System</li>
<li>3D and Doppler</li>
<br><br>

</td>
<td class="dateStamp" style="line-height: 15px;" valign="top" width="50%" align="left">
<span class="assetLink" style="font-size:14px;">AI System</span>
<li>Pathing</li>
<li>Feeler System</li>
<li>Alternate Collisions</li>
<li>Behavior Scripting</li>
<li>Triggered AI</li>
<li>Automatic/Manual Spawning</li>
<br><br>
<span class="assetLink" style="font-size:14px;">Zone Management</span>
<li>Large & Complex World Maps</li>
<li>Zone Instancing</li>
<li>Arena Management</li>
<li>Safe Area Management</li>
<li>Zone Rules & Play Styles</li>
<li>PVP/PVE Zone Management</li>
<li>Portal Area Management</li>
<li>Water Area Management</li>
<br><br>
<span class="assetLink" style="font-size:14px;">Quest System</span>
<li>Basic Speech System</li>
<li>Grouping</li>
<li>Retrieve/Reward Quests</li>
<li>Skills, Items, Currency Rewards</li>
<li>Portal Quests</li>
<li>Epic Quests</li>
<li>Spawning AI Quests</li>
<li>Journal Quests</li>
<br><br>
<span class="assetLink" style="font-size:14px;">Player Housing</span>
<li>Internal & External Models</li>
<li>Open & Closed States</li>
<li>House Banking</li>
<li>House Vendors</li>
<li>House Vendor Management</li>
<br><br>
<span class="assetLink" style="font-size:14px;">Weapons System</span>
<li>Targeting</li>
<li>Magic and Spell System</li>
<li>FPS Weapons</li>
<li>Skeletal Visual-based</li>
<li>Laser Visual-based</li>
<li>Billboard Visual-based</li>
<li>Projectile Physics & Lighting</li>
<li>Heat Seeking</li>
<li>Push-back Effects</li>
<br><br>
<span class="assetLink" style="font-size:14px;">Explosion System</span>
<li>Mesh Visual-based</li>
<li>Key-frame Animation & Scaling</li>
<li>Facing & Rotation</li>
<li>Sound Effects</li>
<li>Lighting Effects</li>
<br><br>
<span class="assetLink" style="font-size:14px;">Environment System</span>
<li>Gravity Management</li>
<li>Fog</li>
<li>Wind & Wind Randomization</li>
<li>Ambient Lighting</li>
<li>Sun Lighting</li>
<li>Sun Visuals</li>
<li>Storm System</li>
<li>Day/Night States & Sun Position</li>
<br><br>


</td>

						
	<td><img runat="server" src="~/images/spacer.gif" width="10" height="10"/></td>							
	</tr>
</table>
<center>
<br><hr noshade size="1px" color="#cccccc" width="85%"/><br>

</center>

<br><br>

