<%@ Page language="c#" Codebehind="3-d-world.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.overview.threedWorld" %>

<link href="../css/friends.css" rel="stylesheet" type="text/css"/>

<link href="../css/new.css" rel="stylesheet" type="text/css" />		

<table border="0" cellspacing="0" cellpadding="0"  width="990" align="center">
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" /></td>
	</tr>
	<tr>
		<td>
			<table  border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="frTopLeft"></td>
					<td class="frTop"></td>
					<td class="frTopRight"></td>
				</tr>
				<tr>
					<td class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
					<td valign="top" class="frBgIntMembers">
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
								
									<!-- TOP STATUS BAR -->
									<div id="pageheader">
										<table cellpadding="0" cellspacing="0" border="0" width="99%">
											<tr>
												<td nowrap align="left">
													<table cellpadding="0" cellspacing="0" border="0">
														<tr>
															<td><h1>Online 3D Virtual World</h1></td>
														</tr>
													</table>
												</td>
												
											</tr>
										</table>
									</div>
									<!-- END TOP STATUS BAR -->	
								
								</td>
							</tr>
							<tr>
								<td width="968" align="left"  valign="top">
									<div class="module whitebg">
									<span class="ct"><span class="cl"></span></span>
									<table cellpadding="0" cellspacing="0" border="0"  width="98%">
										<tr>											
											<td width="230" valign="top">
												<br>
												<table border="0" cellspacing="0" cellpadding="0" width="190">
													<tr>
														<td id="pagenav">
															<ul>
																<li>
																	<a runat="server" href="~/overview/aboutLanding.aspx">About Kaneva</a> 
																</li>
																<li>
																	<a class="selected" runat="server" href="~/overview/3-d-world.aspx">Online 3D World</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/virtual-life.aspx">Your Virtual Life</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/avatar.aspx">Create an Avatar</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/virtual-games.aspx">Virtual Reality</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/rpg-mmo.aspx">Free MMO Game</a> 
																</li>																
																<li>
																	<a runat="server" href="~/overview/artists.aspx">Artist Network</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/online-community.aspx" id="A10">Online Community Features</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/social-networking.aspx" id="A11">Social Networking Features</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/media-sharing.aspx" id="A12">Media Sharing Features</a> 
																</li>																
															</ul>
														</td>
													</tr>																														
												</table>
												
											</td>
											<td valign="top" align="center" width="710">
												
												<table border="0" cellspacing="0" cellpadding="0" width="98%">
													<tr>
														<td>
															<div style="text-align:justify;padding-right:10px;">
<br />
<a href="../default.aspx">Home</a> > Online 3D Virtual World<br />
<br/>
<h1 style="padding-left:0px;">Discover Our 3D Virtual World</h1>
<p>															
Kaneva blurs the line between the offline and online world in a 3D virtual world where the virtual you is an 
extension of the real you. A truly unique online experience, Kaneva brings web profiles and entertainment to life. 
Join the free 3D virtual world full of real friends and good times. 
</p>
<br/>
<h2>An Online Virtual World to Make Your Own</h2>
<p>
In our online virtual world, you can invent your 3D avatar that�s as unique and stylish as you are. Decorate 
your free 3D home with your favorite photos and patterns. And with Kaneva�s 3D virtual world, you can enjoy 
your favorite videos, photos, and music on your 3D TV. The world�s more fun with friends! Invite yours and meet 
up in our online virtual world for 3D events, chats, and more!	
</p>
<br/>
<p>
<a id="joinLink" runat="server">Jump in and play! Join Kaneva�s 3-D virtual world.</a>
</p>
<br/>

															</div>														
														</td>
													</tr>
												</table>
											</td>
											
										</tr>
									</table>
									<span class="cb"><span class="cl"></span></span>
								</div>
								</td>
								
							</tr>
						</table>
					</td>
					<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
				</tr>
				<tr>
					<td class="frBottomLeft"></td>
					<td class="frBottom"></td>
					<td class="frBottomRight"></td>
				</tr>
				<tr>
					<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" /></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
