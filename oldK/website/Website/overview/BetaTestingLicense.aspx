<%@ Page language="c#" Codebehind="BetaTestingLicense.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.BetaTestingLicense" %>


<table cellpadding="20" cellspacing="0" border="0" width="100%">
<tr><td class="bodyText">
<b><span style='font-size:13.5pt;color:black' id="CP">Kaneva Beta Testing License Agreement</span></b><br><br>
<p>
This BETA TESTING LICENSE AGREEMENT (�Agreement�) is made and entered on today between you individually or your corporation (hereinafter referred to as the "TESTER"), and Klaus Entertainment, Inc. a Georgia corporation ("DEVELOPER"). 
</p><p>
WHEREAS, DEVELOPER has developed certain proprietary software products as described in more detail in Exhibit �A� attached hereto and made a part hereof (hereinafter referred to as the �Software�).
</p><p>
	WHEREAS, the Software has been developed by DEVELOPER but has not yet been thoroughly tested in a working environment to determine what further enhancements, improvements, debugging, error correction and other modifications need to be performed in order for the Software to perform to specifications or to enhance the performance specifications of the Software.
</p><p>
	WHEREAS, Tester agrees to utilize the Software in conformance with this Agreement in order to assist DEVELOPER in its assessment of the Software and its functionality and agrees to provide DEVELOPER with ongoing feedback regarding the Software and its operation.
</p><p>
	NOW THEREFORE, for good and valuable consideration, the receipt and sufficiency of which is hereby acknowledged, the parties hereby agree to the following terms and conditions:
</p><p>
<B>ARTICLE I</B><BR>
<B>LIMITED LICENSE FOR BETA TESTING</B>
</p><p>
1.1	DEVELOPER hereby grants to the Tester, a limited term, personal, non-assignable, five separate CPU licenses to use the Software and all Documentation provided by DEVELOPER in connection with the Software solely for Tester�s internal purposes and for the purpose of providing the beta testing services set forth in this Agreement.
</p><p>
1.2	The term of the license granted hereunder shall be for a term of six months from the date of execution hereof.  At the end of the initial six month term, DEVELOPER shall have the right and option to renew this Agreement on a month to month basis through the duration of the beta testing period.
</p><p>
1.3	Prior to the initiation of testing, DEVELOPER shall provide to TESTER a copy of the Software in form suitable for installation on up to five of TESTER�s development PC�s.
</p><p>
1.4	TESTER shall have the right to make a single backup copy of the Software which may be retained in a secure fashion by the TESTER during the term of this Agreement.
</p><p>
1.5	During the term of this Agreement, DEVELOPER shall provide to the TESTER such additional materials, updates, fixes and other material that are deemed appropriate by DEVELOPER to assist the TESTER in performing the testing services hereunder.
</p><p>

<B>ARTICLE II</B><BR>
<B>TESTING OF SOFTWARE</B>
</p><p>
2.1	TESTER hereby agrees to install and to test the Software and provide reporting and input to DEVELOPER solely in exchange for the license to use the Software granted by DEVELOPER within the terms and subject to the scope of this Agreement.
</p><p>
2.2	TESTER shall install, use and operate the Software on a single CPU in compliance with documentation, specifications and testing standards and procedures to be supplied by DEVELOPER.
.
</p><p>
<B>ARTICLE III</B><BR>
<B>TESTING REPORTS</B>
</p><p>
3.1	TESTER shall have a continual duty and obligation to report to DEVELOPER any malfunctioning, bugs, errors of functional deficiencies in the Software (�Software Deficiency�), or if the Software is not performing in accordance with specifications.
</p><p>
3.2	In the event that TESTER encounters and instance of any malfunctioning, bugs, errors of functional deficiencies in the Software, it may report such instance to DEVELOPER on the �Bug Report� or other similar reporting form to be provided by DEVELOPER.  Each Software Deficiency Report shall describe the nature of the Software Deficiency in detail, including the steps or functions that were executed that resulted in such Software Deficiency and any actions that were subsequently take by the TESTER following the occurrence of such Software Deficiency.  If known to the TESTER, the Software Deficiency Report shall include any suggestions that the TESTER has concerning remedying such Software Deficiency.
</p><p>
<B>ARTICLE IV</B><BR>
<B>LICENSE FEE AND TESTING COMPENSATION</B>
</p><p>
4.1	The parties agree that there shall be no monetary license fee payable during the term of this Agreement and that TESTER�s testing services shall be the sole and exclusive manner of compensating DEVELOPER for the license to use the Software granted herein.
</p><p>
4.2	The parties agree that there shall be no fee payable to the TESTER for the performance of testing services hereunder and that the grant of the license to use the Software during the term hereof shall be the sole and exclusive compensation to the TESTER for performing the services hereunder.
</p><p>
4.3	Following the end of the term of this Agreement and upon the Software achieving the performance standards desired by DEVELOPER, DEVELOPER agrees that it shall make the Software available for license to the TESTER subject to the standard end-user licensing provisions offered by DEVELOPER in connection with the Software, except that TESTER shall be offered the pricing terms for the Software license to be determined and mutually agreed upon..
 </p><p>
<B>ARTICLE V</B><Br>
<B>WARRANTY DISCLAIMERS</B>
</p><p>
5.1	TESTER understands and agrees that it is accepting the Software on an experimental basis for testing purposes only.  The purpose of the relationship established in this Agreement is to determine whether and to what extent there are Software Deficiencies.  TESTER further understands and agrees that it is anticipated that there is likely to be Software Deficiencies and that the occurrence of such Software Deficiencies is inherent in the beta testing relationship established hereunder.  Given the nature of this Agreement and the TESTER�s intended use as a beta tester of the Software, TESTER agrees that the Software should not be exclusively relied upon in connection with the operation of any aspect of its business.
</p><p>
5.2	TESTER accepts the Software, on an experimental basis, on an �AS IS� basis and �with all faults and defects.�  TESTER understands and agrees that DEVELOPER does not warrant the Software in any way, express or implied.
</p><p>
5.3	DEVELOPER DISCLAIMS AND ALL WARRANTIES, EXPRESS OR IMPIED, INCLUDING BUT NOT LIMITED TO WARRANTIES OR MERCHANTIBILITY, FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.
</p><p>
5.4	IN NO EVENT WILL DEVELOPER BE LIABLE FOR ANY INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF OR INABILITY TO USE THE PRODUCT, INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OR GOODWILL, WORK STOPPAGE, COMPUTER FAILURE OR MALFUNCTION, OR ANY AND ALL OTHER COMMERCIAL DAMAGES OR LOSSES, EVEN IF ADVISED OF THE POSSIBILITY THEREOF, AND REGARDLESS OF THE LEGAL OR EQUITABLE THEORY (CONTRACT, TORT OR OTHERWISE) UPON WHICH THE CLAIM IS BASED. DEVELOPER IS NOT RESPONSIBLE FOR ANY LIABILITY ARISING OUT OF CONTENT PROVIDED BY TESTER OR A THIRD PARTY THAT IS ACCESSED THROUGH THE PRODUCT AND/OR ANY MATERIAL LINKED THROUGH SUCH CONTENT. ANY DATA INCLUDED IN A PRODUCT UPON SHIPMENT FROM DEVELOPER IS FOR TESTING USE ONLY AND DEVELOPER HEREBY DISCLAIMS ANY AND ALL LIABILITY ARISING THEREFROM. 
</p><p>
<B>ARTICLE VI</B><BR>
<B>PROPRIETARY RIGHTS</B>
</p><p>
6.1	TESTER acknowledges and agrees that the Software is the proprietary property of DEVELOPER and that DEVELOPER is the owner of all copyrights, trademarks, patents, trade secrets and other proprietary information relative to the Software.  TESTER understands and agrees that TESTER is receiving a beta copy of the Software which shall not have been released to the general public.  As such, all information related directly or indirectly to the Software, its development, testing and all other matters are trade secrets of DEVELOPER and may not be disclosed or used by the TESTER for any purpose except for the testing services to be conducted by the TESTER hereunder.
</p><p>
6.2	Title, ownership rights, and intellectual property rights in the Software and any and all improvements, modifications, fixes, or enhancements that arise through the testing relationship, regardless of whether such items are created or suggested by TESTER, shall remain in DEVELOPER. TESTER acknowledges such ownership and intellectual property rights and will not take any action to jeopardize, limit or interfere in any manner with DEVELOPER's ownership of or rights with respect to Software. The Software is protected by copyright and other intellectual property laws and by international treaties. Title and related rights in the content accessed through the Software are the property of the applicable content owner and are protected by applicable law. The license granted under this Agreement gives TESTER no rights to such content.
</p><p>
<B>ARTICLE VII</B><BR>
<B>CONFIDENTIALITY</B>
</p><p>
7.1	Each receiving party shall at all times, both during the term hereof and for a period of at least 3 years after termination, keep in confidence all such Confidential Information (as defined below) using a standard of care such party uses with its own information of this nature, but in no event less than reasonable care. The receiving party shall not use any Confidential Information other than in the course of its permitted activities hereunder. Without the prior written consent of the disclosing party, the receiving party shall not disclose any Confidential Information except on a "need to know" basis to an employee or contractor under binding obligations or confidentiality substantially similar to those set forth herein. If a receiving party is legally compelled to disclose any of the disclosing party's Confidential Information, then, prior to such disclosure, the receiving party will (i) assert the privileged and confidential nature of the Confidential Information and (ii) cooperate fully with the disclosing party in protecting against any such disclosure and/or obtaining a protective order narrowing the scope of such disclosure and/or use of the Confidential Information. In the event such protection is not obtained, the receiving party shall disclose the Confidential Information only to the extent necessary to comply with the applicable legal requirements.
</p><p>
7.2	"Confidential Information" shall mean this Agreement, business plans, marketing plans, affiliated parties, potential customers, customer and supplier lists, pricing information, and all information a party discloses to the other which has been either (i) characterized in writing as confidential at the time of its disclosure or (ii) orally characterized as confidential at the time of disclosure, except for information which the receiving party can demonstrate: (a) is previously rightfully known to the receiving party without restriction on disclosure; (b) is or becomes, from no act or failure to act on the part of the receiving party, generally known in the relevant industry or public domain; (c) is disclosed to the receiving party by a third party as a matter of right and without restriction on disclosure; or (d) is independently developed by the receiving party without access to the Confidential Information. 
</p><p>
7.3	Notwithstanding the above, TESTER agrees to maintain the confidentiality of certain �trade secrets� of DEVELOPER forever, unless written consent otherwise is received by TESTER.  �Trade Secret� information covered by this restriction shall include any and all information relative to the Software and Documentation, the results of testing provided by TESTER, the fact that TESTER has entered into this Agreement with DEVELOPER, and the existence of the Software.  All terms and conditions with respect to Confidential Information shall pertain to �trade secrets� except that the obligation of non-disclosure shall be perpetual with respect to �trade secrets.�
</p><p>
<B>ARTICLE VIII</B><BR>
<B>FURTHER RESTRICTIONS ON USE</B>
</p><p>
8.1	Except as otherwise expressly permitted in this Agreement, TESTER may not: (i) modify or create any derivative works of the Software or documentation, including translation or localization; (ii) decompile, disassemble, reverse engineer, or otherwise attempt to derive the source code for any Software; (iii) redistribute, encumber, sell, rent, lease, sublicense, use the Software in a timesharing or service bureau arrangement, or otherwise transfer rights to any Software; (iv) copy any Software or Documentation (except for a single archival copy which must be stored on media other than a computer hard drive) or documentation (copies shall contain all the notices regarding proprietary rights that were contained in the Software originally delivered by DEVELOPER); (v) remove or alter any trademark; logo, copyright or other proprietary notices, legends, symbols or labels in the Software; (vi) modify any header files or class libraries in any Software; (vii) if applicable, create or alter tables or reports relating to any database portion of the Software (except as necessary for operating the Software); (viii) publish any results of benchmark tests run on any Software to a third party without DEVELOPER's prior written consent; (ix) use the Software on a system with more CPUs than the number licensed.
</p><p>
<B>ARTICLE IX</B><BR>
<B>MISCELLANEOUS</B>
</p><p>
9.1	This Agreement constitutes the entire agreement between the parties concerning the subject matter hereof and supersedes all prior and contemporaneous agreements and channels, whether oral or written, between the parties relating to the subject matter hereof, and all past courses of dealing or industry custom. The terms and conditions hereof shall prevail exclusively over any written instrument submitted by TESTER, including purchase order, and TESTER hereby disclaims any terms therein, except for terms therein relating to product description, quantity thereof, pricing therefore, shipment and delivery. 
</p><p>
9.2	This Agreement may be amended only by a writing signed by an executive vice president of DEVELOPER and a duly authorized representative of TESTER. 
</p><p>
9.3	Except to the extent applicable law, if any, provides otherwise, this Agreement shall be governed by the laws of the State of Georgia, U.S.A., excluding its conflict of law provisions. 
</p><p>
9.4	Any dispute hereunder will be negotiated between the parties commencing upon written notice from one party to the other. Settlement discussions and materials will be confidential and inadmissible in any subsequent proceeding without both parties' written consent. If the dispute is not resolved by negotiation within 45 days following such notice, the parties will refer the dispute to non-binding mediation conducted by a mutually agreeable mediator, with background and experience in the computer and software industries in Atlanta, Georgia (the "Venue"). The parties will share the costs of mediation. If the dispute is not resolved after 45 days of mediation, the parties will refer the dispute to binding arbitration by in the Venue. The results of any arbitration will be final and non-appealable, except that either party may petition any court of competent jurisdiction in the Venue to review any decision relating to intellectual property matters (including the scope of license rights) vacating or modifying erroneous conclusions of law or findings of fact not supported by substantial evidence. The arbitrator will render a written decision, which may be entered in and enforced by any court of competent jurisdiction, but which will have no preclusive effect in other matters involving third parties. The losing party will pay the costs of the arbitration and the reasonable legal fees and expenses of the prevailing party, as determined by the arbitrator. The parties will jointly pay arbitration costs pending a final allocation by the arbitrator. At any point in the dispute resolution process, either party may seek injunctive relief preserving the status quo pending the outcome of that process. Except as noted, the parties hereby waive any right to judicial process. The U.S. Arbitration Act rules will govern the arbitration process. Absent fraudulent concealment, neither party may raise a claim more than 3 years after it arises or any shorter period provided by applicable statutes of limitations. Notwithstanding the foregoing, DEVELOPER reserves the right to invoke the jurisdiction of any competent court to remedy or prevent violation of any provision in the Agreement relating to payment, DEVELOPER Confidential Information or DEVELOPER intellectual property. 
</p><p>
9.5	If any dispute arises under this Agreement, the prevailing party shall be reimbursed by the other party for any and all legal fees and costs associated therewith. 
</p><p>
9.6	This Agreement shall not be governed by the United Nations Convention on Contracts for the International Sale of Goods. 
</p><p>
9.7	If any provision in this Agreement should be held illegal or unenforceable by a court having jurisdiction, such provision shall be modified to the extent necessary to render it enforceable without losing its intent, or severed from this Agreement if no such modification is possible, and other provisions of this Agreement shall remain in full force and effect. 
</p><p>
9.8	The controlling language of this Agreement is English. If TESTER has received a translation into another language, it has been provided for TESTER's convenience only. 
</p><p>
9.9	A waiver by either party of any term or condition of this Agreement or any breach thereof, in any one instance, shall not waive such term or condition or any subsequent breach thereof. 
</p><p>
9.10	The provisions of this Agreement which require or contemplate performance after the expiration or termination of this Agreement shall be enforceable notwithstanding said expiration or termination. 
</p><p>
9.11	TESTER may not assign or otherwise transfer by operation of law or otherwise this Agreement or any rights or obligations herein without the prior express written consent of DEVELOPER, which will not be unreasonably withheld. 
</p><p>
9.12	This Agreement shall be binding upon and shall inure to the benefit of the parties, their successors and permitted assigns.  
</p><p>
9.13	Agreement may be executed in counterparts or by facsimile, each of which shall be deemed an original, and all of which together shall constitute one and the same agreement.  
</p><p>
9.14	Neither party shall be in default or be liable for any delay, failure in performance or interruption of service resulting directly or indirectly from any cause beyond its reasonable control.  
</p><p>
9.15	The relationship between DEVELOPER and TESTER is that of independent contractors and neither TESTER nor its agents shall have any authority to bind DEVELOPER in any way.  
</p><p>
9.16	If any DEVELOPER professional services are being provided, then such professional services are provided pursuant to the terms of a separate agreement between DEVELOPER and TESTER.  The parties acknowledge that such services are acquired independently of the Products licensed hereunder, and that provision of such services is not essential to the functionality of such Products.  
</p><p>
9.17	The headings of the sections of this Agreement are used for convenience only and shall have no substantive meaning.  
</p><p>
9.18	DEVELOPER may use TESTER's name in any customer reference list or in any press release issued by DEVELOPER regarding the licensing of the Product and/or provide TESTER's name and the names of the Products licensed by TESTER to third parties.
</p><p>
9.19	The Product is a "commercial item," as that term is defined in 48 C.F.R. 2.101 (Oct. 1995), consisting of "commercial computer software" and "commercial computer software documentation," as such terms are used in 48 C.F.R. 12.212 (Sept. 1995). Consistent with 48 C.F.R. 12.212 and 48 C.F.R. 227.7202-1 through 227.7202-4 (June 1995), all U.S. Government acquire the Software with only those rights set forth herein.
</p><p>

<B>EXHIBIT �A�</B><BR>
<B>SOFTWARE DESCRIPTION</B>
</p><p>
KANEVA upload and download functionality (Kaneva Media Launcher) and Kaneva developer software and tools (KANEVA Editor).
</p><p>
Last Updated on December 15, 2004.
</p>
</td></tr>
</table>