<%@ Page language="c#" Codebehind="online-community.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.overview.online_community" %>

<link href="../css/friends.css" rel="stylesheet" type="text/css"/>
<link href="../css/new.css" rel="stylesheet" type="text/css" />		

<table border="0" cellspacing="0" cellpadding="0"  width="990" align="center">
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" id="Img1"/></td>
	</tr>
	<tr>
		<td>
			<table  border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="frTopLeft"></td>
					<td class="frTop"></td>
					<td class="frTopRight"></td>
				</tr>
				<tr>
					<td class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img2"/></td>
					<td valign="top" class="frBgIntMembers">
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
								
									<!-- TOP STATUS BAR -->
									<div id="pageheader">
										<table cellpadding="0" cellspacing="0" border="0" width="99%">
											<tr>
												<td nowrap align="left">
													<table cellpadding="0" cellspacing="0" border="0">
														<tr>
															<td><h1>Online Community Features</h1></td>
														</tr>
													</table>
												</td>
												
											</tr>
										</table>
									</div>
									<!-- END TOP STATUS BAR -->	
								
								</td>
							</tr>
							<tr>
								<td width="968" align="left"  valign="top">
									<div class="module whitebg">
									<span class="ct"><span class="cl"></span></span>
									<table cellpadding="0" cellspacing="0" border="0"  width="98%">
										<tr>
											
											<td width="230" valign="top">
												<br>
												<table border="0" cellspacing="0" cellpadding="0" width="190">
													<tr>
														<td id="pagenav">
															<ul>
																<li>
																	<a id="A1" runat="server" href="~/overview/aboutLanding.aspx">About Kaneva</a> 
																</li>
																<li>
																	<a id="A2" runat="server" href="~/overview/3-d-world.aspx">Online 3D World</a> 
																</li>
																<li>
																	<a id="A3" runat="server" href="~/overview/virtual-life.aspx">Your Virtual Life</a> 
																</li>
																<li>
																	<a id="A4" runat="server" href="~/overview/avatar.aspx">Create an Avatar</a> 
																</li>
																<li>
																	<a id="A5" runat="server" href="~/overview/virtual-games.aspx">Virtual Reality</a> 
																</li>
																<li>
																	<a id="A6" runat="server" href="~/overview/rpg-mmo.aspx">Free MMO Game</a> 
																</li>																
																<li>
																	<a id="A7" runat="server" href="~/overview/artists.aspx">Artist Network</a> 
																</li>
																<li>
																	<a runat="server"  class="selected" href="~/overview/online-community.aspx" id="A10">Online Community Features</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/social-networking.aspx" id="A11">Social Networking Features</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/media-sharing.aspx" id="A12">Media Sharing Features</a> 
																</li>
															</ul>
														</td>
													</tr>																														
												</table>
												
											</td>
											<td valign="top" align="center" width="710">
												
												<table border="0" cellspacing="0" cellpadding="0" width="98%">
													<tr>
														<td>
															<div style="text-align:justify;padding-right:10px;">
<br />
<a href="../default.aspx">Home</a> > Online Community Features<br />	
<br/>
<h1 style="padding-left:0px;">Kaneva�s Online Community and Online Chat Community Features</h1>
<p>
Kaneva is bridging the gap between the 2D web and 3D Internet -- reinventing the 
<a href="~/3d-virtual-world/virtual-life.kaneva" runat="server">Virtual World</a> for the masses. We�ve tightly integrated 
many advanced, online community features that go beyond today�s traditional social networking online 
community and shared media capabilities. On the web and in-world, with Kaneva you can:
</p>

<p>
<ul>
	<li style="list-style:disc;">Interact easily with other members by making friend requests, posting comments, publishing your own 
	blog, adding key words called tags, raving <a href="~/people/people.kaneva" runat="server">Profiles</a>  
	and Media, sharing your favorite media, and participating in online forums</li>
	
	<li style="list-style:disc;">Start or join online <a href="~/community/channel.kaneva" runat="server">Communities</a>  
	that match your interests, form a group around those interests and get connected</li>
	 
	<li style="list-style:disc;">Participate in a free online chat community within our Virtual World to chat in real time, in 3D</li>
	 
	<li style="list-style:disc;">Upload <a href="~/watch/watch.kaneva" runat="server">movies, videos, music, or games</a>  
	and easily import them into your 3D home in the Virtual World to be viewed and discussed by the 
	Kaneva online chat community</li> 
</ul>
</p>
<br/>

<h2>More about Kaneva�s Collaborative, Social Networking Online Community Features</h2>

<p>
Get into our <a href="~/3d-virtual-world/virtual-life.kaneva" runat="server">3D Virtual World of Kaneva</a> 
and participate in an online chat community made even more real by live 3D chat and imported Profiles, 
Friends, Media and Communities. Kaneva's online chat community includes the ability to have a 3D chat 
with your avatar friends in-world, then click on their Profile to learn more about the person behind 
the Avatar. Meet up again to talk about connections you have made. 
</p>
<br/>
<p>
Kaneva�s social networking online community features provide monthly �showcase� opportunities to rise to 
the top and get recognized. From battles of the bands to the best in movies, funny video clips or fun games, 
you can be recognized on the Kaneva web and in our 3D Virtual World online chat community for what you do best. 
</p>
<br/>
<p>
<a id="joinLink" runat="server">Join Kaneva - it�s free - and be part of our 3D Virtual World and Online Chat Community.</a>
</p>

<br/>
															</div>														
														</td>
													</tr>
												</table>
												
											</td>
											
										</tr>
									</table>
									<span class="cb"><span class="cl"></span></span>
								</div>
								</td>
								
									</td>
							</tr>
						</table>
					</td>
					<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img3"/></td>
				</tr>
				<tr>
					<td class="frBottomLeft"></td>
					<td class="frBottom"></td>
					<td class="frBottomRight"></td>
				</tr>
				<tr>
					<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" id="Img4"/></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
