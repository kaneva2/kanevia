<%@ Page language="c#" Codebehind="kPress.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.kPress" %>
<center>
<table cellpadding="0" cellspacing="0" border="0" width="710">
<tr>
	<td colspan="3" align="left"><a runat="server" href="~/free-virtual-world.kaneva" ID="A4"><img border="0" style="vertical-align:middle;" runat="server" src="~/images/kaneva_logo_150x50.jpg" ID="Img4"/></a> <span style="font-family:arial; font-size: 27px; font-weight:bold;">Corporate Information</span></td>
</tr>
<tr>
	<td align="left" colspan="3"><span style="font-size:6px;"><br><br></span>
		<span style="font-size:6px;"><br><br></span>
	</td>
</tr>
<tr>
	<td>
	<span class="orangeHeader3" style="font-size: 22px;">Kaneva News & Press</span><br><br>	
	</td>
</tr>
<tr>
	<td class="bodyText">
	
<br>
<span class="dateStamp">10-25-05</span><BR><B><A runat="server" class="bulletHead" 
href="~/overview/PR10-25-05b.htm" target="_resource" ID="A5">Kaneva Empowers Game Developers to Quickly Create Complex MMO Games, Publish Online and Earn Significant Royalties</A></B><BR><I>Kaneva releases new version of Kaneva Game Platform, the industry�s first, freely downloadable Massively Multi-player Online (MMO) game platform.</I><BR><BR>	

<span class="dateStamp">10-25-05</span><BR><B><A runat="server" class="bulletHead" 
href="~/overview/PR10-25-05a.htm" target="_resource" ID="A6">Kaneva and Campus MovieFest Launch Ground-Breaking Online Film Festival, <i>Independence MovieFest</i></A></B><BR><I>iMF is the world�s first and only film festival to host all submitted movies online � from shorts to full features -- in their entirety and in DVD quality.� iMF is now accepting submissions from filmmakers the world over.</I><BR><BR>	

<span class="dateStamp">9-14-05</span><BR><B><A class="bulletHead" runat="server" href="http://www.mmorpg.com/showFeature.cfm/loadFeature/231" target="_resource" ID="A1">How Kaneva Will Impact 
Game Development</A></B><BR><I>Greg Frame, Chief Gaming Officer of Kaneva discusses Kaneva impact</I><BR><BR>	



<span class="dateStamp">6-15-05</span><BR><B><A class="bulletHead" runat="server" href="~/overview/PR7-15-05.htm" target="_resource" ID="A2">Kaneva's Fred Tanzella 
Addresses Largest Gathering of Game Developers in Georgia at 
IGDA</A></B><BR><I>Talk Focused on New, Online Game Platform that Speeds, High 
Quality Massively Multiplayer Online (MMO) Games to 
Market</I><BR><BR><span class="dateStamp">4-05-05</span><BR><B><A class="bulletHead" 
href="http://www.cgonline.com/content/view/656/2/" target=_resource>Computer 
Games Magazine - Klaus Entertainment Interview</A></B><BR><I>Christopher Klaus 
talks to Computer Games Magazine</I><BR><BR><span class="dateStamp">3-16-05</span><BR><B><A runat="server" class="bulletHead" href="~/overview/PR3-16-05.htm" target="_resource" ID="A3">Industry Leaders Convene to Explore Advancement of 
Digital Media </A></B><BR><I>Chris Klaus presents at the 2005 Living Game Worlds 
Symposium </I><BR><BR><span class="dateStamp">9-27-04</span><BR><B><A class="bulletHead"
href="http://atlanta.bizjournals.com/atlanta/stories/2004/09/27/story2.html" 
target=_resource>Video games of the future </A></B><BR><I>Internet Security 
Systems co-founder starts new venture </I><BR><BR><span class="dateStamp">7-30-04</span><BR><B><A class="bulletHead" 
href="http://www.gov.state.ga.us/press/2004/press534.shtml" 
target=_resource>Governor Perdue Announces Executive Appointments 
</A></B><BR><I>Chris Klaus is appointed to the Film, Video, and Music Advisory 
Commission </I>	
	</td>
</tr>
</table>
</center>