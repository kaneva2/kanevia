<%@ Page language="c#" Codebehind="graphics.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.graphics" %>

<br>
<center>
<table cellpadding="0" cellspacing="0" border="0" width="710">
<tr>
	<td colspan="3" align="left"><a runat="server" href="~/free-virtual-world.kaneva" ID="A4"><img border="0" style="vertical-align:middle;" runat="server" src="~/images/kaneva_logo_150x50.jpg" ID="Img4"/></a> <span style="font-family:arial; font-size: 27px; font-weight:bold;">Corporate Information</span></td>
</tr>
<tr>
	<td align="left" colspan="3"><span style="font-size:6px;"><br><br></span>
		<span style="font-size:6px;"><br><br></span>
	</td>
</tr>
<tr>
	<td colspan="3" class="dateStamp"><i>If you would like to include a link to Kaneva on your web page, feel free to use one of our official logos, buttons or banners below. If you would like to use the Kaneva logo for some other purpose, please contact us for permission at <a href="mailto:info@kaneva.com">info@kaneva.com</a></i><br><br></td>
</tr>
<tr>
	<td>
	<span class="orangeHeader3">Kaneva Logos</span><br><br>	
	</td>
</tr>
<tr>
	<td colspan="3" class="bodyText">
	<img runat="server" src="~/images/Kaneva_Logo_300x100.jpg" width="300" height="100" ID="Img5"/><br><br>
	Kaneva Logo 150 x 50 use link: <b>http://www.kaneva.com/images/Kaneva_Logo_150x50.jpg</b>	<br><br>
	Kaneva Logo 300 x 100 use link: <b>http://www.kaneva.com/images/Kaneva_Logo_300x100.jpg</b> <br><br>
	Kaneva Logo 600 x 200 use link: <b>http://www.kaneva.com/images/Kaneva_Logo_600x200.jpg</b> <br><br>
	Kaneva Logo HiRes: <b><a runat="server" href="~/images/Kaneva_Logo_hires.jpg">download</a></b> - <i class="dateStamp">right-click and 'save target as'...</i> <br><br><br>
	</td>

</tr>

<tr>
	<td colspan="3">
	<span class="orangeHeader3">Kaneva Banners</span><br><br>	
	
	<i class="dateStamp">To link to these banners use: 'http://www.kaneva.com/images/' + following image names</i><br><br>
	
	</td>
</tr>
<tr>
	<td colspan="3" align="left" class="bodyText">
	<img runat="server" src="~/images/banner_kaneva_wide728x90.jpg" ID="Img2"/><br>
	 <center>(banner_kaneva_wide728x90.jpg)</center><br><br>
	</td>
</tr>
<tr>
	<td class="bodyText">
		<img runat="server" src="~/images/banner_kaneva_sky160x600.jpg"/><br>
		(banner_kaneva_sky160x600.jpg)
	</td>
	<td valign="top" align="center" class="bodyText"><img runat="server" src="~/images/banner_kaneva_wide486x60.jpg" ID="Img1"/><br>
	(banner_kaneva_wide486x60.jpg)
	</td>
</tr>
</table>
</center>
<br><br><br>
