<%@ Page language="c#" Codebehind="artists.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.overview.artists" %>

<link href="../css/friends.css" rel="stylesheet" type="text/css"/>
<link href="../css/new.css" rel="stylesheet" type="text/css" />		

<table border="0" cellspacing="0" cellpadding="0"  width="990" align="center">
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" id="Img1"/></td>
	</tr>
	<tr>
		<td>
			<table  border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="frTopLeft"></td>
					<td class="frTop"></td>
					<td class="frTopRight"></td>
				</tr>
				<tr>
					<td class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img2"/></td>
					<td valign="top" class="frBgIntMembers">
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
								
									<!-- TOP STATUS BAR -->
									<div id="pageheader">
										<table cellpadding="0" cellspacing="0" border="0" width="99%">
											<tr>
												<td nowrap align="left">
													<table cellpadding="0" cellspacing="0" border="0">
														<tr>
															<td><h1>Artist Network</h1></td>
														</tr>
													</table>
												</td>
												
											</tr>
										</table>
									</div>
									<!-- END TOP STATUS BAR -->	
								
								</td>
							</tr>
							<tr>
								<td width="968" align="left"  valign="top">
									<div class="module whitebg">
									<span class="ct"><span class="cl"></span></span>
									<table cellpadding="0" cellspacing="0" border="0"  width="98%">
										<tr>
											
											<td width="230" valign="top">
												<br>
												<table border="0" cellspacing="0" cellpadding="0" width="190">
													<tr>
														<td id="pagenav">
															<ul>
																<li>
																	<a id="A1" runat="server" href="~/overview/aboutLanding.aspx">About Kaneva</a> 
																</li>
																<li>
																	<a id="A2" runat="server" href="~/overview/3-d-world.aspx">Online 3D World</a> 
																</li>
																<li>
																	<a id="A3" runat="server" href="~/overview/virtual-life.aspx">Your Virtual Life</a> 
																</li>
																<li>
																	<a id="A4" runat="server" href="~/overview/avatar.aspx">Create an Avatar</a> 
																</li>
																<li>
																	<a id="A5" runat="server" href="~/overview/virtual-games.aspx">Virtual Reality</a> 
																</li>
																<li>
																	<a id="A6" runat="server" href="~/overview/rpg-mmo.aspx">Free MMO Game</a> 
																</li>																
																<li>
																	<a id="A7" class="selected" runat="server" href="~/overview/artists.aspx">Artist Network</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/online-community.aspx" id="A10">Online Community Features</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/social-networking.aspx" id="A11">Social Networking Features</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/media-sharing.aspx" id="A12">Media Sharing Features</a> 
																</li>	
															</ul>
														</td>
													</tr>																														
												</table>
												
											</td>
											<td valign="top" align="center" width="710">
												
												<table border="0" cellspacing="0" cellpadding="0" width="98%">
													<tr>
														<td>
															<div style="text-align:justify;padding-right:10px;">
<br />
<a href="../default.aspx">Home</a> > Artist Network<br />	
<br/>
<h1 style="padding-left:0px;">Tap into our Growing Artist Network</h1>
<p>
For an up-and-coming band, performer, DJ or artist, finding an artist network in which you can easily showcase 
your talent and get noticed can be difficult. You want to find an online artist community that brings people 
together all over the world and provides a stage for you to share your talent and earn recognition. At Kaneva, 
we don�t mean that hypothetically we mean it literally! In the <a href="~/3d-virtual-world/virtual-life.kaneva" runat="server">3D Virtual World of Kaneva</a>, 
your music, movies, artwork or other creative expressions can be featured in virtual theaters or other public hang-outs. 
</p>
<br/>

<h2>Build a Connected Audience with Our Online Artist Community</h2>

<p>
Kaneva is about enabling artists to easily get involved in our artist network by creating their own online 
Communities to showcase their work and build a global audience. Plug into our online artist community by 
starting a Community on Kaneva and then extend that Community to 3D for a real life experience. Every Community 
on Kaneva gets a <a href="~/community/communitypage.aspx?communityid=1118&pageId=12394" runat="server">3D hangout</a> 
-- theater, media room or other virtual meeting place. 
</p>
<br/>
<p>
As an artist, you can be part of our online artist community to showcase your talent and interact with your 
audience through forums, blogs and in-world 3D chat. Once in-world, you can even put on a virtual event - at 
which you play your own music, show your videos or other artistic creations. You can also earn "raves" or 
votes that will help you climb to the top of the Kaneva site Leaderboards and get you noticed in our artist network. 
</p>
<br/>
<p>
Getting exposure for your art and talent is the name of the game in the crowded entertainment industry. By 
joining Kaneva, you gain access to an artist network full of possibilities and all the tools you need to 
upload your media, build and customize an online and 3D Community that matches your style and features all 
of your best work. Use our unique online artist community to make connections with other musicians, photographers, 
film makers, other artists, existing fans and entertainment enthusiasts. Get enough "raves" from Kaneva members 
and you and your talent will rise to the top and get noticed. 
</p>
<br/>
<p>
<a runat="server" id="joinLink">Are you ready to be a part of the artist network within 
Kaneva? Join Kaneva and start your own Profile or Community today!</a>
</p>
												
<br/>
															</div>														
														</td>
													</tr>
												</table>
											</td>
											
										</tr>
									</table>
									<span class="cb"><span class="cl"></span></span>
								</div>
								</td>
								
									</td>
							</tr>
						</table>
					</td>
					<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img3"/></td>
				</tr>
				<tr>
					<td class="frBottomLeft"></td>
					<td class="frBottom"></td>
					<td class="frBottomRight"></td>
				</tr>
				<tr>
					<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" id="Img4"/></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
