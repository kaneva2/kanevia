<%@ Page language="c#" Codebehind="whyjoin_people.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.overview.whyjoin_people" %>
 <style>
.menu1
{
	font-family: Verdana;
	font-size:15px;
	color:#646464;
	font-weight:bold;
	text-align:center;	
} 
.menu1  a
{
	font-family: Verdana;
	font-size:12px;
	color:#646464;
	font-weight:bold;
	text-align:center;	
}
.menu1 a:visited
{
	font-family: Verdana;
	font-size:12px;
	color:#646464;
	font-weight:bold;
	text-align:center;	
}
.menu1 a:hover
{
	font-family: Verdana;
	font-size:12px;
	color:#FFFFFF;
}
.menu1 div
{
	width:140px;
	padding: 4px 0px;
}
.content a
{
	font-family: Verdana;
	font-size:11px;
	color:#767661;
	text-decoration:underline;
} 
.content a:visited
{
	font-family: Verdana;
	font-size:11px;
	color:#767661;
	text-decoration:underline;
} 
.content a:hover
{
	font-family: Verdana;
	font-size:11px;
	color:#FFFFFF;
	text-decoration:underline;
} 
 .link1 a
{
	font-family: Verdana;
	font-size:14px;
	color:#767661;
	text-decoration:underline;
} 
.link1 a:visited
{
	font-family: Verdana;
	font-size:14px;
	color:#767661;
	text-decoration:underline;
} 
.link1 a:hover
{
	font-family: Verdana;
	font-size:14px;
	color:#FFFFFF;
	text-decoration:underline;
} 

</style>

<br/>

<table cellpadding="0" cellspacing="0" border="0" height="737" width="990" style="background-image:url(../images/content/meet_people.jpg);background-repeat:no-repeat">
	<tr>
		<td width="140" valign="top" class="menu1" align="left" >
			<div style="padding-top:15px;"><a href="whyjoin.aspx">Explore</a></div>
			
			<div style="padding-top:12px;"><a href="whyjoin_people.aspx">Express</a></div>

			<div style="padding-top:12px;"><a href="whyjoin_channels.aspx">Start Community</a></div>
		</td>
		
		<td width="430" style="padding-left:16px;padding-right:10px;" valign="top">

			<div class="header">Express Yourself</div>

			<div class="content">
			Kaneva is your "canvas" where you can showcase and share your passions, interests, and talents with the world �by 
			creating your own online profile, virtual self or your own Community.
			</div>

			<div class="content">
			<span class="subheader">Create your Profile</span><br/>
			Easily create your own online Profile -- your feature-rich web pages that are all about you. Upload your videos, 
			photos, music/audio, and personalize your Profile using our built-in catalog of cool themes and innovative 
			drag and drop widgets. </div>

			<div class="content">
			Use Kaneva�s Connected Media Module to play media in your Profile and publish your media anywhere 
			on the Net, including within the Virtual World of Kaneva.
			</div>

			<div class="content">
			Add and publish your own blog. Share your Profile with family, friends and other Kaneva members. 
			</div>

			<div class="content">
			<span class="subheader">Go "In World"</span><br/>
			Kaneva also enables you to express yourself in 3D. Join Kaneva�s 3D Virtual World community 
			and extend your Kaneva experience �in world�. 
			</div>

			<div class="content">
			Kaneva gives invited members the ability to create their own avatar � a totally 
			customizable 3D virtual self � and participate in a unique, next generation entertainment experience. 
			</div>

			<div class="content">
			With just a few clicks, you can completely customize your look � everything from eye color to 
			clothing to hair style � to make your own avatar mirror your real self or not. Directly from 
			your Profile, teleport yourself into our massively multiplayer world to watch thousands of 
			movies and videos and hang out with friends in your own apartment or the world�s many nightclubs, 
			theaters or cafes. Earn and purchase credits, browse and select upgrades from our catalog of in 
			world items, gifts and clothing. 
			</div>

			<div class="link1" style="padding-top:10px">
			<a id="joinLink" runat="server">Get started now</a>
			</div>
		</td>

		<td class="bodyText" width="420" style="padding:10px;line-height:14px;">
		
		</td>
	</tr>
</table>

