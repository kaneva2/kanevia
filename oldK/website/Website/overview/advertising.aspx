<%@ Page language="c#" Codebehind="advertising.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.overview.advertising" %>
<link href="../css/friends.css" rel="stylesheet" type="text/css"/>

<link href="../css/new.css" rel="stylesheet" type="text/css" />		

<table border="0" cellspacing="0" cellpadding="0"  width="990" align="center">
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" id="Img1"/></td>
	</tr>
	<tr>
		<td>
			<table  border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="frTopLeft"></td>
					<td class="frTop"></td>
					<td class="frTopRight"></td>
				</tr>
				<tr>
					<td class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img2"/></td>
					<td valign="top" class="frBgIntMembers">
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
								
									<!-- TOP STATUS BAR -->
									<div id="pageheader">
										<table cellpadding="0" cellspacing="0" border="0" width="99%">
											<tr>
												<td nowrap align="left">
													<table cellpadding="0" cellspacing="0" border="0">
														<tr>
															<td><h1>Advertising, Sponsors and Content Providers</h1></td>
														</tr>
													</table>
												</td>
																								
											</tr>
										</table>
									</div>
									<!-- END TOP STATUS BAR -->	
								
								</td>
							</tr>
							<tr>
								<td width="968" align="left"  valign="top">
									<div class="module whitebg">
									<span class="ct"><span class="cl"></span></span>
									<table cellpadding="0" cellspacing="0" border="0"  width="98%">
										<tr>
											
											<td width="230" valign="top">
												<br>
												<table border="0" cellspacing="0" cellpadding="0" width="190">
													<tr>
														<td id="pagenav">
															<ul>
																<li>
																	<a runat="server" href="~/overview/aboutLanding.aspx" id="A1">About Kaneva</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/vision.aspx" id="A2">History</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/company.aspx" id="A3">Leadership</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/ourvalues.aspx" id="A4">Culture & Values</a> 
																</li>
																<li>
																	<a runat="server" href="~/share/default.aspx">Share Kaneva</a> 
																</li>
																<li>
																	<a class="selected" runat="server" href="~/overview/advertising.aspx" id="A5">Advertising & Sponsors</a> 
																</li>
																<li>
																	<a runat="server" href="~/community/news.kaneva" id="A6">News / Press Center</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/contactus.aspx" id="A7">Contact Us</a> 
																</li>
																
															</ul>
														</td>
													</tr>																														
												</table>
												
											</td>
											<td valign="top" align="center" width="710">
												
												<table border="0" cellspacing="0" cellpadding="0" width="98%">
													<tr>
														<td>
															<br/>
															
															<div style="text-align:justify;padding-right:10px;">
		
<p>
Kaneva provides a compelling opportunity for brands to engage customers with immersive experiences 
to build awareness around your brand, company and products.  A wide variety of rich media, product 
placement and consumer interactions can be developed in the 3D Virtual World of Kaneva and the Kaneva Web site.   
</p>
<br/>
<p> 
For more information about Kaneva�s advertising, sponsorship and content opportunities, 
please contact us at <a href="mailto:bizdev@kaneva.com">bizdev@kaneva.com</a>.
</p> <br/>
		
														
															</div>
														</td>
													</tr>
												</table>
											</td>
											
										</tr>
									</table>
									<span class="cb"><span class="cl"></span></span>
								</div>
								</td>
								
									</td>
							</tr>
						</table>
					</td>
					<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img3"/></td>
				</tr>
				<tr>
					<td class="frBottomLeft"></td>
					<td class="frBottom"></td>
					<td class="frBottomRight"></td>
				</tr>
				<tr>
					<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" id="Img4"/></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
