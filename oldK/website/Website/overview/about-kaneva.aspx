<%@ Page language="c#" Codebehind="about-kaneva.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.overview.about_kaneva" %>

<link href="../css/friends.css" rel="stylesheet" type="text/css"/>

<link href="../css/new.css" rel="stylesheet" type="text/css" />		

<table border="0" cellspacing="0" cellpadding="0"  width="990" align="center">
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" id="Img1"/></td>
	</tr>
	<tr>
		<td>
			<table  border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="frTopLeft"></td>
					<td class="frTop"></td>
					<td class="frTopRight"></td>
				</tr>
				<tr>
					<td class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img2"/></td>
					<td valign="top" class="frBgIntMembers">
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
								
									<!-- TOP STATUS BAR -->
									<div id="pageheader">
										<table cellpadding="0" cellspacing="0" border="0" width="99%">
											<tr>
												<td nowrap align="left">
													<table cellpadding="0" cellspacing="0" border="0">
														<tr>
															<td><h1>About Kaneva</h1></td>
														</tr>
													</table>
												</td>
												
												<td align="right" valign="middle">
													<table cellpadding="0" cellspacing="0" border="0" width="690">
														<tr>
															<td class="headertout" width="175"></td>
															<td class="searchnav" width="260"></td>
															<td align="left" width="130"></td>
															<td class="searchnav" align="right" width="210" nowrap>
																
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</div>
									<!-- END TOP STATUS BAR -->	
								
								</td>
							</tr>
							<tr>
								<td width="968" align="left"  valign="top">
									<div class="module whitebg">
									<span class="ct"><span class="cl"></span></span>
									<table cellpadding="0" cellspacing="0" border="0"  width="98%">
										<tr>
											
											<td width="230" valign="top">
												<br>
												<table border="0" cellspacing="0" cellpadding="0" width="190">
													<tr>
														<td id="pagenav">
															<ul>
																<li>
																	<a class="selected" runat="server" href="~/overview/about-kaneva.aspx" id="A1">About Kaneva</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/social-networking.aspx" id="A2">Social Networking</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/online-community.aspx" id="A3">Online Community</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/virtual-world.aspx" id="A4">Virtual World</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/mmorpg.aspx" id="A5">MMORPG</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/virtual-life.aspx" id="A6">Virtual Life</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/avatar.aspx" id="A7">Avatar</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/anime.aspx" id="A8">Anime</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/artists.aspx" id="A9">Artists</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/media-sharing.aspx" id="A10">Media Sharing</a> 
																</li>
																
															</ul>
														</td>
													</tr>																														
												</table>
												
											</td>
											<td valign="top" align="center" width="710">
												
												
												
												<table border="0" cellspacing="0" cellpadding="0" width="98%">
													<tr>
														<td><br/>
														<h1>About Kaneva</h1>
														<br>
<div style="text-align:justify;padding-right:10px;">
<p>
Kaneva brings together people, entertainment and fun. It is the only place where Profiles, Friends, Media and favorite 
Communities can be teleported from the web into a 3D Virtual World full of people, entertainment and fun.
</p> 
<br/>
<p>
Kaneva offers an interactive entertainment experience that is different from 
<a runat="server" href="~/overview/social-networking.aspx">social networking sites</a>. We go 
beyond social networking and <a runat="server" href="~/overview/media-sharing.aspx">free video sharing</a>, 
and we offer more than just an <a runat="server" href="~/overview/online-community.aspx">online chat community</a>. 
Gone are the days when you just lean back and watch entertainment. Kaneva invites everyone to lean in, engage 
and interact with People, Entertainment and Communities...on the web and in a Virtual World. 
</p> 
<br/>
<p>
You can express yourself on the web and <a runat="server" href="~/overview/virtual-life.aspx">create a virtual life</a> 
that extends your Kaneva Profile, friends, favorite media and Communities into a 3D entertainment playground. Join 
Kaneva and create your own easily customizable Profile all about you. Then get active -- upload media, join or start 
special interest Communities, make friends, and interact on the web -- share, comment, rave, tag. You can also browse 
and enjoy a wide range of media including videos, photos, music/audio, and games. Start or join Communities including 
everything from <a runat="server" href="~/overview/anime.aspx">anime</a> to hip hop music. 
</p> 
<br/>
<p>
Get your invite to go in-world, and enter our 3D <a runat="server" href="~/overview/virtual-world.aspx">online virtual world</a> 
where you can make your virtual life an extension of your real life. 
<a runat="server" href="~/overview/avatar.aspx">Create your own avatar</a> as well as your 3D home. All of your 
media will go with you in-world and play on your virtual TV or go into picture frames on the wall of your 
virtual pad. Your friends on Kaneva will also be your friends in the 3D Virtual World of Kaneva. Kaneva is 
closely connecting people through a growing <a runat="server" href="~/overview/artists.aspx">artist network</a> -- 
giving the community the ability to find, meet and socialize with one another around 
their passions and interests in a way that is exciting, fun and rewarding.
</p> 
<br/>
<p>
For those interested in developing their own in-world game or even their own virtual world, Kaneva offers access to 
our free <a runat="server" href="~/overview/mmorpg.aspx">MMORPG</a> game platform, the same engine on which the 
Virtual World of Kaneva was built. Be part of our Elite Developer program and make your game or world one of 
over 60 currently in development.
</p>
</div>											
													</td>
													</tr>
												</table>
											</td>
											
										</tr>
									</table>
									<span class="cb"><span class="cl"></span></span>
								</div>
								</td>
								
									</td>
							</tr>
						</table>
					</td>
					<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img3"/></td>
				</tr>
				<tr>
					<td class="frBottomLeft"></td>
					<td class="frBottom"></td>
					<td class="frBottomRight"></td>
				</tr>
				<tr>
					<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" id="Img4"/></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
