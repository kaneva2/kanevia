///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Kaneva.overview
{
	/// <summary>
	/// Summary description for learnMore_channels.
	/// </summary>
	public class learnMore_channels : MainTemplatePage
	{
		protected learnMore_channels () 
		{
			Title = "Learn more - Channels";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
            joinLink.HRef = ResolveUrl(KanevaGlobals.JoinLocation);
            
			string siteName = "http://" + KanevaGlobals.SiteName;
			string status301 = "301 Moved Permanently";
			Response.Status = status301;
            Response.AddHeader("Location", "http://support.kaneva.com/ics/support/default.asp?deptID=5433");   
			Response.End();
		}

        protected HtmlAnchor joinLink;

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
