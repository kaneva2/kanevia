///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using log4net;

using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for contactUs.
	/// </summary>
	public class contactUs : MainTemplatePage
	{	
		// Logger
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

		protected contactUs () 
		{
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			//set meta data
			Title = "Contact Us";
			MetaDataDescription = "<meta name=\"description\" content=\"We welcome your comments, suggestions and any questions you have about using Kaneva. If you have suggestions for improvements or would like to report a site bug... \" />";

            aHelp.HRef = Common.GetHelpURL(KanevaWebGlobals.CurrentUser.FirstName, KanevaWebGlobals.CurrentUser.LastName, KanevaWebGlobals.CurrentUser.Email);
            aHelpCenter.HRef = Common.GetHelpURL(KanevaWebGlobals.CurrentUser.FirstName, KanevaWebGlobals.CurrentUser.LastName, KanevaWebGlobals.CurrentUser.Email);
		}

        protected HtmlAnchor aHelp, aHelpCenter;
		
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
