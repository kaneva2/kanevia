<%@ Page language="c#" Codebehind="company.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.company" %>

<link href="../css/friends.css" rel="stylesheet" type="text/css"/>

<link href="../css/new.css" rel="stylesheet" type="text/css" />		

<table border="0" cellspacing="0" cellpadding="0"  width="990" align="center">
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" id="Img1"/></td>
	</tr>
	<tr>
		<td>
			<table  border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="frTopLeft"></td>
					<td class="frTop"></td>
					<td class="frTopRight"></td>
				</tr>
				<tr>
					<td class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img2"/></td>
					<td valign="top" class="frBgIntMembers">
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
								
									<!-- TOP STATUS BAR -->
									<div id="pageheader">
										<table cellpadding="0" cellspacing="0" border="0" width="99%">
											<tr>
												<td nowrap align="left">
													<table cellpadding="0" cellspacing="0" border="0">
														<tr>
															<td><h1>Leadership</h1></td>
														</tr>
													</table>
												</td>
												
												<td align="right" valign="middle">
													<table cellpadding="0" cellspacing="0" border="0" width="690">
														<tr>
															<td class="headertout" width="175"></td>
															<td class="searchnav" width="260"></td>
															<td align="left" width="130"></td>
															<td class="searchnav" align="right" width="210" nowrap>
																
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</div>
									<!-- END TOP STATUS BAR -->	
								
								</td>
							</tr>
							<tr>
								<td width="968" align="left"  valign="top">
									<div class="module whitebg">
									<span class="ct"><span class="cl"></span></span>
									<table cellpadding="0" cellspacing="0" border="0"  width="98%">
										<tr>
											
											<td width="230" valign="top">
												<br>
												<table border="0" cellspacing="0" cellpadding="0" width="190">
													<tr>
														<td id="pagenav">
															<ul>
																<li>
																	<a runat="server" href="~/overview/aboutLanding.aspx" id="A1">About Kaneva</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/vision.aspx" id="A2">History</a> 
																</li>
																<li>
																	<a class="selected" runat="server" href="~/overview/company.aspx" id="A3">Leadership</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/ourvalues.aspx" id="A4">Culture & Values</a> 
																</li>
																<li>
																	<a runat="server" href="~/share/default.aspx">Share Kaneva</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/advertising.aspx" id="A5">Advertising & Sponsors</a> 
																</li>
																<li>
																	<a runat="server" href="~/community/news.kaneva" id="A6">News / Press Center</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/contactus.aspx" id="A7">Contact Us</a> 
																</li>
																
															</ul>
														</td>
													</tr>																														
												</table>
												
											</td>
											<td valign="top" align="center" width="710">
												
												<table border="0" cellspacing="0" cellpadding="0" width="98%">
													<tr>
														<td>
															<br/>
															
															<div style="text-align:justify;padding-right:10px;">
		
			<div style="float:left;margin-right:10px;"><img src="~/images/avatars/exec-klaus-avatar.jpg" width="70" height="67" runat="server" id="Img5"> </div>
			<h2 style="padding-top:4px;">Christopher W. Klaus - Founder / Chief Executive Officer</h2> 
			
			<p>
			Focused on driving overall vision and strategy, Christopher W. Klaus is the founder and Chief Executive Officer 
			of Kaneva, a rapidly growing social entertainment world. Christopher is also highly regarded as one of the 
			youngest and brightest Internet pioneers of our time as well as a foremost philanthropist and community leader.
			</p>
			<br/>
			<p>
			At Kaneva (Latin for �canvas�), Christopher has been leading the drive to build a unique destination on the Web 
			where anyone can create a Profile or Community, join an interactive community to share and rave media and interact 
			with a rapidly growing audience; and even teleport into a modern day, easy to use 3D Virtual World to connect with 
			one another and experience the next generation in online entertainment.
			</p>
			<br/>
			<p>
			Christopher founded Kaneva with the vision of embracing the new era of content creation and harnessing the power 
			of the Internet � to democratize entertainment and provide opportunities for everyone from artists to consumers 
			to share and interact with entertainment online in an entirely new way.
			</p>
			<br/>
			<p> 
			Christopher also founded and served as the Chief Security Advisor of Internet Security Systems, Inc. (ISS), a 
			company he created in 1994 and sold to IBM at a total value of over one billion dollars in 2006. As a world 
			renowned Internet pioneer, Christopher was one of the first to recognize the emerging need for Internet security 
			and developed the first vulnerability scanning technology. During his time at ISS, Christopher consistently lead 
			the company�s technology vision, contributed significantly to its business strategy and was instrumental in helping 
			organizations around the world safeguard critical data from the ever-growing number of information security threats. 
			During his time at ISS, Christopher testified at several U.S. Senate and House of Representative Hearings on issues 
			surrounding cyber security and was selected to co-chair the Technical Standards and Common Criteria Task Force for 
			the Department of Homeland Security National Cyber Security Summit � a private sector sponsored task force within 
			the President�s National Strategy to Secure Cyberspace.
			</p>
			<br/>
			<p> 
			A former student at Georgia Institute of Technology, Christopher decided to share his success by donating $15 million 
			in 1999 for the construction of a new building on the Georgia Tech campus �the Klaus Advanced Computing Building--representing 
			the largest personal donation from anyone of his generation according to the Chronicle of Philanthropy. Opened in 2006, 
			the building was designed to inspire students to also give back, as well as accelerate future technology innovation, 
			making a positive, direct impact to our digital lifestyle and the global online community. 
			</p>
			<br/>
			<p> 
			Appointed by Georgia Governor Sonny Perdue, Christopher currently sits on the Film, Video, and Music Advisory 
			Commission where he plays an active role in helping define legislation to support the number of films and games 
			made in Georgia. He is a game advisor to American Intercontinental University (AIU), serves on the 
			Savannah College of Art and Design (SCAD) Board of Visitors as well as the boards of VerticalOne, the Georgia 
			Game Developers Association (GGDA), Georgia Tech Advisory Board (GTAB), and the Georgia Tech College of Computing. 
			</p>
			<br/>
			<p> 
			Further demonstrating his commitment to creating more opportunities for entrepreneurs and in giving back to the 
			community, Christopher is involved at a high level in groups such as: The Metro Atlanta Chamber of Commerce, 
			Hands on Atlanta, Hands on Network, C.A.R.E., the Technology Association of Georgia and The Georgia Tech Alumni Association. 
			</p>
		

			<br/><br/>
			<div style="float:left;margin-right:10px;"><img src="~/images/avatars/exec-animesh-avatar.jpg" width="70" height="67" runat="server" id="Img8"> </div>
			<h2 style="padding-top:4px;">Animesh Saha - Vice President of Engineering</h2> 
			
			<p>
			A seasoned technology professional, Animesh has over seventeen years experience managing, directing and architecting 
			software products. At Kaneva, Animesh is responsible for leading the Engineering organization that develops Kaneva�s 
			grassroots entertainment portal, related Connected Media functionality,  massively online virtual worlds, applications 
			and services. 
			</p>
			<br/>
			<p>
			Prior to joining Kaneva, Animesh held a wide range of management and senior technical positions at Lancope, 
			Thoughtmill, Attachmate and Sybase. At Lancope, an Internet Security firm, Animesh was the Director of Product 
			Development and led the development of the award winning StealthWatch 4.0 product suite. At Thoughtmill Corporation, 
			an Engineering Services firm, Animesh served as the Chief Architect overseeing full life cycle development of 
			large commercial projects for many high technology companies including Internet Security Systems (ISS), DoubleClick, 
			MediaBin (formerly Iterated Systems), Engage Technologies, Attachmate, and XcelleNet. During this time, Animesh 
			was instrumental in helping successfully execute these companies� Internet and technology strategies.
			</p>
			<br/>
			<p>
			Prior to Thoughtmill, Animesh was on board at Attachmate, a leader in multi-host access and integration, where 
			he pioneered the development of an Intelligent Internet Proxy server. Additionally, he also worked on OpenMind, 
			Attachmate's award-winning groupware product. Prior to this, Animesh served as a Senior Consultant at Sybase, 
			working closely with top management of Fortune 500 companies. 
			</p>
			<br/>
			<p>
			Animesh holds a Masters degree in Computer Science from Clarkson University, New York and a Bachelors degree 
			in Electrical and Electronics Engineering from Indian Institute of Technology, Madras, India.
			</p>
			
										
															</div>
														</td>
													</tr>
												</table>
											</td>
											
										</tr>
									</table>
									<span class="cb"><span class="cl"></span></span>
								</div>
								</td>
								
									</td>
							</tr>
						</table>
					</td>
					<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img3"/></td>
				</tr>
				<tr>
					<td class="frBottomLeft"></td>
					<td class="frBottom"></td>
					<td class="frBottomRight"></td>
				</tr>
				<tr>
					<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" id="Img4"/></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
