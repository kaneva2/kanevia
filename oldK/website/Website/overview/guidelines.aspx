<%@ Page language="c#" Codebehind="guidelines.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.overview.guidelines" %>

<link href="../css/friends.css" rel="stylesheet" type="text/css"/>
<link href="../css/new.css" rel="stylesheet" type="text/css" />		

<table border="0" cellspacing="0" cellpadding="0" class="newcontainer" align="center">
	<tr>
		<td>
			<table  border="0" cellpadding="0" cellspacing="0" width="100%" class="newcontainerborder">
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
				<tr>
					<td class=""><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img2"/></td>
					<td valign="top" class="newdatacontainer">
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
								
									<!-- TOP STATUS BAR -->
									<div id="pageheader">
										<table cellpadding="0" cellspacing="0" border="0" width="99%">
											<tr>
												<td nowrap align="left">
													<table cellpadding="0" cellspacing="0" border="0">
														<tr>
															<td><h1>Member Guidelines</h1></td>
														</tr>
													</table>
												</td>
												
												<td align="right" valign="middle">
													<table cellpadding="0" cellspacing="0" border="0" width="690">
														<tr>
															<td class="headertout" width="175"></td>
															<td class="searchnav" width="260"></td>
															<td align="left" width="130"></td>
															<td class="searchnav" align="right" width="210" nowrap>
																
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</div>
									<!-- END TOP STATUS BAR -->	
								
								</td>
							</tr>
							<tr>
								<td width="968" align="left"  valign="top">
									<div class="module whitebg">
									<span class="ct"><span class="cl"></span></span>
									<table cellpadding="0" cellspacing="0" border="0"  width="98%">
										<tr>
											
											<td width="230" valign="top">
												<br>
												<table border="0" cellspacing="0" cellpadding="0" width="190">
													<tr>
														<td id="pagenav">
															<ul>
																<li>
																	<a class="selected" runat="server" href="~/overview/guidelines.aspx">Member Guidelines</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/rulesofconduct.aspx">Rules of Conduct</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/termsandconditions.aspx">Terms & Conditions</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/copyright.aspx">Copyright Policy</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/privacy.aspx">Privacy Policy</a> 
																</li>
																<li>
																	<a runat="server" href="~/community/safety.kaneva">Safety Center</a> 
																</li>
																<li>
																	<a runat="server" id="aHelp" target="_blank">Help Center</a> 
																</li>
																
															</ul>
														</td>
													</tr>																														
												</table>
												
											</td>
											<td valign="top" align="center" width="710">
												
												<table border="0" cellspacing="0" cellpadding="0" width="98%">
													<tr>
														<td>
															<br/>
															
															<div style="text-align:justify;padding-right:10px;">

<p>
By enabling members to interact in a powerful new way online and in a virtual world, 
Kaneva is creating a unique community of self-expression and sharing. To aid our members 
in understanding the boundaries of conduct in Kaneva, we have posted our Member Guidelines 
and our Rules of Conduct. To ensure the environment is suited for members of various ages 
and backgrounds, please follow these guidelines. 
</p>
<br/>
<p>
The Member Guidelines may be found below and are good general rules to follow. 
The Rules of Conduct are specific rules dealing with all aspects of the Kaneva service
<a runat="server" href="~/overview/rulesofconduct.aspx">and can be found here.</a>
</p>
<br/>

</p>
<br/>
<h2>DO�s</h2>

<ul>
<p>
<li><span style="font-weight:bold;">DO: Be polite and respectful.</span></li><br/>
Give Kaneva members the same consideration you would give them in person. Don�t harass, 
abuse, impersonate, or intimidate others with any communications, actions, or other methods.  
</p>

<p>
<li><span style="font-weight:bold;">DO: Agree to disagree.</span><br/>
Exchange opinions respectfully and without resorting to prohibited content. 
This applies to your comments, forum posts, and messages to members.  
</li>
</p>
	
<p>
<li><span style="font-weight:bold;">DO: Mark content, communities, and profiles as "Access Pass" if they contain material or member submissions that may be
 inappropriate for visitors under the age of 18.</span><br/> 
When you mark content as <span style="font-weight:bold;font-style:italic;">Access Pass</span>, you protect our visitors -- and give yourself greater freedom of expression.  
Only Kaneva members 18 years of age or older that have purchased an <span style="font-weight:bold;font-style:italic;">Access Pass</span> may post or 
view <span style="font-weight:bold;font-style:italic;">Access Pass</span> content. 
</li>
</p>
	
<p>
<li><span style="font-weight:bold;">DO: Take action if you come across inappropriate content.</span><br/>
Because our members express themselves in a variety of ways, you may see something that offends you.    Our tools provide 
you with a variety of responses.   Examples:
<ul>
	<li style="padding:8 0 4 0;">If you see content that you believe should be marked �Restricted,� or violates 
		<a href="~/overview/termsandconditions.aspx" runat="server">Kaneva's Terms and Conditions</a>, you can click <i>Report Abuse</i>.</li>
	<li style="padding:4 0 4 0;">If a member�s content or behavior violates our Member Guidelines or 
		<a href="~/overview/termsandconditions.aspx" runat="server">Kaneva�s Terms and Conditions</a>, click the <i>Report Abuse</i> link 
		below the member's profile picture.</li> 
	<li style="padding:4 0 4 0;">If the member's Profile is connected to you as a friend, you can <i>Remove</i> them from your Friends list.</li> 
	<li style="padding:4 0 0 0;">You can click <i>Block</i> to ignore a Member.  This will prevent their messages from reaching you.</li> 
</ul>
</li>
</p>
	
<p>
<li><span style="font-weight:bold;">DO: Make Kaneva a rich, compelling experience.</span><br/>
At Kaneva, media is a key ingredient of member expression and interaction.  Upload all your photos, 
videos and music to show-off and share (after all, Kaneva has no limits on storage), or link to
your favorite YouTube and Flash widgets.   Just make sure your content is permitted on Kaneva and 
doesn�t violate our <a href="~/overview/copyright.aspx" runat="server">copyright policy</a>, as 
prohibited or copyrighted content may be removed without warning.
</li>
</p>
	
<p>
<li><span style="font-weight:bold;">DO: Share Kaneva with others.</span><br/>
Get the word out about your profile, your communities, and the Virtual World of Kaneva.  Click 
<i>Invite a Friend</i> to invite your friends and family to check out your Profile and to join. You also can use 
<a href="~/overview/learnmore_cmp.aspx" runat="server">Kaneva�s Connected Media Module</a> 
to embed your media in other sites � MySpace, Digg and more.
</li>
</p>

</ul>

<br/>
<h2>DON�Ts</h2>

<ul>	
<p>
<li><span style="font-weight:bold;">DON�T: Post content that is not permitted.</span><br/> 
Do not post words, photos, or media that are <strong>harmful, harassing, racist or ethnically offensive, incendiary, 
or otherwise objectionable</strong>. Content of this type is subject to removal at our sole discretion and you may receive a warning. 
If you repeatedly post this type of content, your account may be deactivated.</li>
</p>

	
<p>
<li><span style="font-weight:bold;">DON�T: Post content that violates Kaneva�s 
<a href="~/overview/copyright.aspx" runat="server">Copyright Policy</a> and 
<a href="~/overview/termsandconditions.aspx" runat="server">Terms and Conditions</a>.</span><br/>
Kaneva respects the intellectual property rights of others and we require that our members do 
the same. Members may not upload, post, reproduce, link to, distribute or store material protected 
by copyright, trademark, trade secret or other proprietary right, or which would violate any right 
of publicity, right of privacy or other right of any third party, without first obtaining permission 
of the owner of such right.   If you post content that infringes these rights, we may remove the 
media or deactivate your account. </li>
</p>
	
<p>
<li><span style="font-weight:bold;">DON�T: Upload photos, video, text, or other content in public or 
private areas that violates state, federal, or local law.</span><br/>
Illegal content does not belong at Kaneva. If you post it, we will deactivate your account and 
take appropriate action that may include reporting you to the authorities.</li>
</p>
	
<p>
<li><span style="font-weight:bold;">DON�T: Use your Kaneva access to solicit or spam others.</span><br/>
This includes selling goods or services and/or posting similar or identical messages repeatedly 
to other members' media and comments, message centers, forums, or other venues. If you spam 
or solicit others, your account may be deactivated. </li>
</p>
	
<p>
<li><span style="font-weight:bold;">DON�T:  Surrender your privacy to strangers.</span><br/>
Kaneva is committed to protecting the privacy of its members.   But you play a role 
as well.   We recommend the following for all members, especially our members under the age of 18:
<ul>
	<li style="padding:8 0 4 0;">Do not post your full name, or exactly where you live.</li>
	<li style="padding:4 0 4 0;">Do not post contact information, such as your home address, or a phone number.</li>
	<li style="padding:4 0 4 0;">Do not add people as Friends you don�t personally know.</li>
	<li style="padding:4 0 0 0;">Do not give away your email address to people you're not familiar with.</li>
</ul>
</p>
	
</ul>	
	

<br/>
<h2>Kaneva's Response to Violations</h2>

<p>
Kaneva does not edit profiles, communities, or other member-created content. But we 
reserve the right to remove or block content that we determine, in our sole discretion, is inappropriate 
and/or in violation of the <a href="~/overview/legal.aspx" runat="server">Kaneva Terms of Service</a>. 
Depending on the level of offense, you may 
risk deactivation of your Kaneva account(s) and everything associated with it, including access to the 
virtual world, private messages, profile 
access, communities, blogs, media, and social connections. In some cases, Kaneva will attempt to contact 
you via email if your content requires editing or removal, or if your account is deactivated.
</p>

<br/>
<h2>Your Responsibilities as a Kaneva Member</h2>

<p>
Your use of Kaneva is subject to these guidelines and the 
<a href="~/overview/legal.aspx" runat="server">Kaneva Terms of Service</a>. Kaneva reserves 
the right to modify these guidelines at any time. If you are unsure whether your content is appropriate 
or consistent with these policies, please err on the side of caution and do not post the content on Kaneva.
</p>


<br/><br/>
<h1 style="padding-left:0px;">Kaneva Policies</h1>

<p>
Kaneva, Inc. is strongly committed to protecting the privacy of our members, and respecting the 
rights of content owners.   Please read the following to learn more about our legal policies and terms of use.
</p>

<br/>
<h2>Privacy</h2>
<p>
Kaneva provides a safe and secure environment for consumers by enforcing a strict policy of customer 
privacy. <a href="~/overview/privacy.aspx" runat="server" id="A1">Read More >></a>
</p>

<br/>
<h2>Copyright</h2>
<p>
Kaneva members remain the original copyright owners of all material provided on the community. 
Use of the material in a manner which is inconsistent with our terms and conditions is strictly prohibited.  
<a href="~/overview/copyright.aspx" runat="server" id="A2">Read More >></a>
</p>

<br/>
<h2>Terms & Conditions</h2>
<p>
Kaneva provides its service to you subject to the following Terms of Service.   <a href="~/overview/TermsAndConditions.aspx" runat="server" id="A3">Read More >></a>
</p>
	
<br/>	
														
															</div>
														</td>
													</tr>
												</table>
											</td>
											
										</tr>
									</table>
									<span class="cb"><span class="cl"></span></span>
								</div>
								</td>
								
									</td>
							</tr>
						</table>
					</td>
					<td class=""><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img3"/></td>
				</tr>
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
				<tr>
					<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" id="Img4"/></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
