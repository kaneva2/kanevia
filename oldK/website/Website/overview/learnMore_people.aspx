<%@ Page language="c#" Codebehind="learnMore_people.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.overview.learnMore_people" %>

<style type="text/css">

p {font-size:12px;font-weight:bold;line-height:15px}
</style>

<center>
<br/>
<table cellpadding="0" cellspacing="0" class="container" border="0">
	<tr>
		<td width="45%" class="mktText" style="padding:10px;" valign="top">

		<div style="font-size:16pt;font-weight:bold;">ABOUT MEMBER PROFILES</div><br/>
		
		<p>
			A Member Profile is a series of easily customized web pages all about you. 
		</p>
		
		<p>
			A next generation online space, your Kaneva Profile allows you to express yourself and share your passions like never before.
		</p>
		
		<p>
			Kaneva�s free and easy-to-use tools enable you to create a personalized, multi-media Profile to ... 
		</p>
		
		<p>
			<ul style="font-weight:bold">
			<li>Upload and showcase all of your media � videos, photos, music/audio, games, and text</li>
			<br/><br/>
			<li>Publish your own blog</li>
			<br/><br/>
			<li>Personalize your Profile by choosing from our catalog of cool themes and automated, easy to use, drag and drop widgets </li>
			<br/><br/>
			<li>Show your media through Kaneva�s <a href="learnmore_cmp.aspx">Connected Media Module</a> and use the Player the publish your media anywhere on the net</li>
			<br/><br/>
			<li>Use Kaneva�s built-in social networking platform to share your media and Profile with friends, family, colleagues and other Kaneva members</li> 
			<br/><br/>
			<li>Rave media in other Profiles and Communities and express your opinion on hundreds of forums</li>
			</ul>
		</p>
		
		<p>
			Whether you want a multimedia-rich Profile to share with your family and friends - or you're 
			ready to broadcast your story and personal talents to the world, a Kaneva Member Profile is your 
			stage -- the ultimate online canvas for self-expression and sharing. 
		</p>
		
		<br/>
		<p><a id="joinLink" runat="server">Get started now >>></a></p>

		<p><a href="~/overview/whyjoin.aspx" runat="server" id="A3">Why Join >>></a></p>

		</td>

		<td width="5%"></td>
		
		<td width="45%" class="mktText" style="padding:10px;" valign="top">
				
		<img src="../images/content/race_cars.jpg" border="0" />
		
		<div>
		
		<p>CONNECTED MEDIA MODULE</p>
		
		<p style="font-weight:normal">The next generation media player.</p>
		
		<p style="font-weight:normal">Scalable, customizable media player for showing photos, videos and audio.</p> 

		<p style="font-weight:normal">Can be easily embedded, �skinned� and sized to fit on any web page.</p>

		<p><a href="~/overview/learnmore_cmp.aspx" runat="server">Learn More >>></a></p>
 

		</div>
		
		</td>
	</tr>
</table>
<br/>
</center>