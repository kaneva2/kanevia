<%@ Page language="c#" Codebehind="whyjoin_money.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.overview.whyjoin_money" %>


<br/>

<table cellpadding="0" cellspacing="0" border="0" width="990" height="425" style="background-image:url(../images/content/make_money.jpg);">
	<tr>
		<td width="140" valign="top" class="menu" align="left" >
			<div style="padding-top:12px"><a href="whyjoin.aspx">Get Connected</a></div>
			
			<div><a href="whyjoin_people.aspx">Meet People</a></div>

			<div><a href="whyjoin_channels.aspx">Create Channels</a></div>

			<div><a href="whyjoin_money.aspx">Make Money</a></div>
		</td>
		
		<td width="430" style="padding-left:16px;padding-right:10px;" valign="top">

			<div class="header">MAKE MONEY</div>

			<div class="content">
			The world is changing. With just a few clicks, anyone can be a journalist, a photographer, a TV producer 
			or a DJ.  Not too long ago it was television and movie studios dictating the what and when of our entertainment.  
			Now, for the first time in history, the cost of creative production is so low the power is in everyone's hands 
			to play, teach, share, gain recognition and get rewarded.
			</div>

			<div class="content">
			Be part of the new face of celebrity. In addition to providing a means for achieving global visibility, Kaneva 
			is currently developing an innovative Revenue Sharing Program that monetizes the open flow and sharing power of the Internet. 
			</div>

			<div class="content">
			Earn a share in revenue when you attract others to your Channel and the media in it. We will soon have options 
			of layering in advertising with Kaneva Advertising widgets or adding pay per view content to your Channel. At 
			Kaneva, we realize that the audience is the network. We want to both empower and reward you. 
			</div>

			<div class="content">
			Stay tuned as we unveil our Revenue Sharing Program.
			</div>

			<div class="link" style="padding-top:18px">
			<a id="joinLink" runat="server">Get started now</a>
			</div>
		</td>

		<td class="bodyText" style="padding:10px;line-height:14px">
		</td>
	</tr>
</table>
