<%@ Page language="c#" Codebehind="TermsAndConditions.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.TermsAndConditions" %>

<link href="../css/friends.css" rel="stylesheet" type="text/css"/>
<link href="../css/new.css" rel="stylesheet" type="text/css" />		

<table border="0" cellspacing="0" cellpadding="0" class="newcontainer" align="center">
	<tr>
		<td>
			<table  border="0" cellpadding="0" cellspacing="0" width="100%" class="newcontainerborder">
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
				<tr>
					<td class=""><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img2"/></td>
					<td valign="top" class="newdatacontainer">
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
								
									<!-- TOP STATUS BAR -->
									<div id="pageheader">
										<table cellpadding="0" cellspacing="0" border="0" width="99%">
											<tr>
												<td nowrap align="left">
													<table cellpadding="0" cellspacing="0" border="0">
														<tr>
															<td><h1>Terms & Conditions</h1></td>
														</tr>
													</table>
												</td>
												
												<td align="right" valign="middle">
													<table cellpadding="0" cellspacing="0" border="0" width="690">
														<tr>
															<td class="headertout" width="175"></td>
															<td class="searchnav" width="260"></td>
															<td align="left" width="130"></td>
															<td class="searchnav" align="right" width="210" nowrap>
																
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</div>
									<!-- END TOP STATUS BAR -->	
								
								</td>
							</tr>
							<tr>
								<td width="968" align="left"  valign="top">
									<div class="module whitebg">
									<span class="ct"><span class="cl"></span></span>
									<table cellpadding="0" cellspacing="0" border="0"  width="98%">
										<tr>
											<td width="230" valign="top">
												<br>
												<table border="0" cellspacing="0" cellpadding="0" width="190">
													<tr>
														<td id="pagenav">
															<ul>
																<li>
																	<a runat="server" href="~/overview/guidelines.aspx">Member Guidelines</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/rulesofconduct.aspx">Rules of Conduct</a> 
																</li>
																<li>
																	<a class="selected" runat="server" href="~/overview/termsandconditions.aspx">Terms & Conditions</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/copyright.aspx">Copyright Policy</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/privacy.aspx">Privacy Policy</a> 
																</li>
																<li>
																	<a runat="server" href="~/community/safety.kaneva">Safety Center</a> 
																</li>
																<li>
																	<a runat="server" id="aHelp" target="_blank">Help Center</a> 
																</li>
																
															</ul>
														</td>
													</tr>																														
												</table>
												
											</td>
											<td valign="top" align="center" width="710">
												
												<table border="0" cellspacing="0" cellpadding="0" width="98%">
													<tr>
														<td>
															<br/>
															
															<div style="text-align:justify;padding-right:10px;">
<p>															
Welcome to Kaneva.com and related websites (individually and together, the "Site"). PLEASE READ THESE 
TERMS AND CONDITIONS OF USE ("TERMS") CAREFULLY BEFORE USING THIS SITE OR ANY APPLICATIONS OR SERVICES 
AVAILABLE FROM IT. This Site is owned and operated by Kaneva, Inc. (referred to as "Kaneva"). "You" are the 
person or entity using the Site, ordering or using Applications or described in the Site registration form. 
</p>
<br/>
<p>
By using this Site, ordering or using Applications, or clicking on a button indicating your consent, you 
agree to these Terms including but not limited to conducting this transaction electronically, and to 
disclaimers of warranties, damage and remedy exclusions and limitations, and a choice of Georgia law. 
If you do not agree to these Terms, you do not have permission to use the Site. Kaneva reserves the right, 
at its discretion, to change, modify, add, or remove portions of these Terms at any time. Notification of 
changes to these Terms will be posted on the Site or sent via e-mail. If any future changes to these 
Terms are unacceptable to you, you must discontinue use of the Site. 
</p>
<br/>
<p>
Payment terms and refund information (if applicable) for Applications and other products are provided 
in the ordering process and/or on the Help page in connection with ordering that product. 
</p>
<br/>
<p>
You may also be subject to additional terms and conditions when you use third-party content, services, 
or software (including Applications) accessed through or promoted on the Site. 
</p>
<br/>
<h2>1. KANEVA SERVICES</h2>
<p>
Kaneva is a service provider, which means, among other things, that Kaneva does not control various 
aspects of the Site.  You acknowledge that Kaneva is a service provider that may allow people to 
interact online regarding topics and content chosen by users of the Site, and that users can alter the
Site environment on a real-time basis. Kaneva generally does not regulate the content of communications
between users or users' interactions with the Site. As a result, Kaneva has very limited control, if any,
over the quality, safety, morality, legality, truthfulness or accuracy of various aspects of the Site. 
</p>
<br/>
<h2>2. REGISTRATION AND ORDERING OBLIGATIONS</h2>
<p>
In consideration of your use of the Site and/or Applications, you agree to provide true, accurate and 
complete information about yourself as prompted by the Site registration or ordering process, provided 
that in connection with registration, you do not need to use your full or true name for your member name 
(all together, the �Account�). In addition, you agree to update that Account information in order to 
maintain its truth, accuracy and completeness. Kaneva may deny you access to the Site and Applications
or reject your order in the event that your required information for your Account is untrue, inaccurate or 
incomplete. 
</p>
<br/>
<h2>3. AGE LIMITATIONS</h2>
<p>
You must be 13 years of age or older to register an Account and participate in the Site.  If you are 
less than age 18 you must have your parent or legal guardian�s permission to use the Site, as well as 
have them read and agree to the TERMS.
By accepting this agreement and these TERMS in connection with an Account, you represent, acknowledge 
and agree that (i) you are at least 18 years of age and have read and accepted these TERMS; or (ii) 
you are the parent or legal guardian of a user that is at least 13 years of age, and you have read and 
accepted these TERMS.
</p>
<br />
<h2>4. AGREEMENT TO DEAL ELECTRONICALLY</h2>
<p>
All transactions with or through the Site or Kaneva may, at Kaneva's option, be conducted electronically. 
Kaneva may keep records of any type of communication conducted via the Site. All electronic records are 
deemed sent when they are properly addressed to the recipient and the record enters an information processing 
system outside the control of the sender or the record enters a region of an information processing system 
under the recipient's control. All electronic records are received when the record enters an information 
processing system that the recipient has designated or uses for the purpose of receiving electronic records 
or information of the type sent, in a form capable of being processed by that system, and from which 
the recipient is able to retrieve the electronic record. 
</p>
<br/>
<h2>5. OBLIGATION FOR USING YOUR PASSWORD</h2>
<p>
ALL CONTENT OR INSTRUCTIONS TRANSMITTED BY OR RECEIVED FROM ANYONE PRESENTING YOUR PASSWORD ON THE SITE WILL 
BE DEEMED BINDING ON YOU. You agree that you are solely liable for all actions taken via your password, whether 
or not made with your knowledge or authority. You agree to guard your password carefully, with the full 
awareness that a failure to keep it secure will enable others to engage in transactions through the Site for 
which you will be legally responsible. If you suspect that someone may have obtained access to your password 
who is not intended to have authority to act on your behalf, please contact Kaneva immediately to authorize 
Kaneva to deny access to the Site to anyone else presenting your password. 
</p>
<br/>
<h2>6. LIMITED NON-COMMERCIAL LICENSE TO USE SITE</h2>
<p>
Kaneva hereby grants you the limited right to view and use the Site only for the purposes of viewing or 
playing content such as films or games, placing product orders or for accessing information, Applications 
and services. Kaneva reserves the right to suspend, ban, reset any and all features of your account including 
credits and rewards, or deny, in its sole discretion, your access to all or any portions of the Site 
as described in Section 24. This license is limited to personal and non-commercial uses by you. Any rights 
not expressly granted to you herein are reserved to Kaneva. Unless you have received specific written 
permission from Kaneva, you may not (a) "frame" or otherwise impose editorial comment, commercial material 
or any information or content on, or in proximity to, content displayed on the Site or (b) alter or modify 
any content on the Site. Without limiting other restrictions, you agree not to reproduce, transmit, sell, 
or otherwise exploit the Site and/or Applications for any commercial purpose. 
</p>
<br/>
<p>
The Site may allow members to upload, post, and/or distribute member submitted content, and use of the Site for 
this purpose is subject to the following conditions:
</p>
<br/>
<ul class="greenarrow">
<li>
You represent and warrant that (a) all required registration information you submit is truthful and accurate 
as described in Section 2; 
(b) you will maintain the accuracy of such information; (c) you are 13 years of age or older and agree to the 
age limitations described in Section 3;  (d) your use of Kaneva does not violate any applicable law or regulation; 
and (e) you agree to abide by the Kaneva Rules of Conduct posted on the Kaneva website. Your information may be 
deleted and your Membership may be terminated without warning if we believe that you are less than 13 years of age. 
</li>
<br/>
<li>
You understand that all member feedback, data, comments, suggestions, information, text, data, software, 
sounds, photographs, audio, audiovisual, video, artwork, graphics, messages and other materials of any 
nature ("Materials") that are transmitted to or via the Site are the sole responsibility of the person 
from which the Materials originated. This means you, and not Kaneva, are entirely responsible for the 
Materials you transmit through the Site. Further, you understand that by using the Site you may be 
exposed to Materials that are offensive, objectionable or indecent. 
</li>
<br/>
<li>
You shall not create a member name or nickname or upload to, distribute through or otherwise publish 
through the Site any Materials which are indecent, libelous, defamatory, obscene, threatening, invasive 
of privacy or publicity rights, abusive, illegal, harassing, contain expressions of hatred, bigotry, 
racism or pornography, depicts graphic or gratuitous violence, or are otherwise objectionable, or that 
would constitute or encourage a criminal offense, violate the rights of any party or violate any law.  
</li>
<br/>
<li>
Your Materials, member name and/or nickname will not disparage in any manner Kaneva, its Licensors, or their 
Applications, products, or services and sites. 
</li>
<br/>
<li>
Your Materials, member name and/or nickname shall not infringe the copyright, trademark, publicity/privacy 
right or other intellectual property right of any third party. 
</li>
<br/>
<li>
You shall not upload to, distribute through or otherwise publish through the Site any Materials that are directly 
or indirectly commercial in nature or contain any solicitation of funds, promotion, advertising or solicitation 
for goods or Site. You specifically acknowledge that soliciting other members to join or become users or members 
of any commercial online web site or other organization is expressly prohibited. 
</li>
<br/>
<li>
You shall not upload to, distribute through or otherwise publish through the Site any Materials that contain 
viruses or any other computer code, corrupt files or programs designed to interrupt, destroy or limit the 
functionality or disrupt any software, hardware, telecommunications, networks, servers or other equipment. 
</li>
</ul>
<br/>

<h2>7. LIMITED NON-COMMERCIAL LICENSE TO USE APPLICATIONS</h2>
<p>
Kaneva may offer you the ability to use certain applications, services or software, and content such as games 
and films for use therewith, either at no charge or for a fee (such applications, services, software and any 
related content provided by Kaneva or its licensors is referred to herein collectively as "Applications"). 
For example, Kaneva may provide chat areas, bulletin boards, e-mail functions, software and services such as 
Kaneva downloads that allow you to download specific content from the Sites to your computer, multi-player 
games and software, and videos and music videos. Without limiting your obligations and restrictions described 
in Section 6, your limited license to the Applications is subject to the following conditions: 
</p>
<br/>

<ul class="greenarrow">
<li>
The Applications that are made available to you are the copyrighted work of Kaneva and/or its suppliers. 
Certain Applications may be owned by third parties and distributed under third party agreements and you agree 
that Kaneva shall not be responsible for any loss or damage of any sort relating to your dealings with such 
third parties. 
</li>
<br/>
<li>
You shall provide and properly maintain at your expense a computing system and Internet connection that meets 
or exceeds <a href="~/community/CommunityPage.aspx?communityId=1118&pageId=887411" runat="server">Kaneva�s system requirements</a> 
for access and use of the Site, Applications, and services. Kaneva does not provide Internet access, and 
you are responsible for all expenses and fees associated with your computing system and Internet connection.
<li>
Unless expressly stated otherwise you are granted only a limited license to download and/or use the Application 
from a single computer for personal and non-commercial purposes. You may not make copies of or distribute the 
Application or electronically transfer the Application from one computer to another or over a network, nor may you 
separate any content from an associated Application. You may not decompile, reverse engineer, disassemble or 
otherwise reduce the Application to human perceivable form. You may not rent, lease or sublicense the Application. 
You may not create derivative works of the Application and you may not export the Application in violation of any 
U.S. or foreign law, rule or regulation. All rights in Applications not specifically granted to you in writing by 
Kaneva are reserved to Kaneva.
</li>
<br/>
<li>
Kaneva may cease support of any Application at any time in its sole discretion, as further described in Section 24. 
</li>
</ul>
<br/>
<h2>8. SERVICE FEE; ADDITIONAL FEATURE PURCHASES</h2>
<p>
(a)  All fees are stated in U.S. Dollars. WHILE YOU CAN CONTINUE TO PLAY THE FREE PORTION OF THE VIRTUAL WORLD OF 
KANEVA (�the Game�, �the Service�) ONCE YOU HAVE DOWNLOADED THE GAME WITHOUT HAVING TO PURCHASE ANY ADDITIONAL 
FEATURES, IF YOU CHOOSE TO PURCHASE ADDITIONAL FEATURES OR USE PAID ASPECTS OF THE SERVICE, FEES ARE PAYABLE IN 
ADVANCE AND ARE NOT REFUNDABLE IN WHOLE OR IN PART FOR ANY REASON WHATSOEVER, INCLUDING, WITHOUT LIMITATION, 
INTERRUPTION OR UNAVAILABILITY OF SERVICE. If you choose to pay for paid aspects and/or buy Additional Features 
online using a credit card, we will automatically charge you through your chosen payment method for the purchases,
plus any applicable taxes we are required to collect, where you authorize us to do so. All fees and purchases are 
payable in advance. YOU ARE FULLY LIABLE FOR ALL CHARGES TO YOUR ACCOUNT. You represent to Kaneva that you are 
the authorized account holder or an authorized user of the chosen method of payment used to pay for the paid aspects 
of the Service and/or Additional Features purchase.  
</p>
<br/>
<p>
(b) YOU UNDERSTAND AND AGREE THAT ANY ATTEMPT TO MAKE ANY CHARGE BACK OR OTHERWISE RECLAIM OR OBTAIN A REFUND OF OR 
A CREDIT AGAINST ANY FEES PAID WITHOUT FIRST HAVING OBTAINED KANEVA�S PRIOR WRITTEN CONSENT TO SUCH REFUND SHALL 
ENTITLE KANEVA TO DEACTIVATE YOUR ACCOUNT UNLESS AND/OR UNTIL SUCH TIME AS THE FEES ARE REPAID OR A CREDIT OR REFUND 
IS AGREED WITH KANEVA. DEACTIVATION WILL INCLUDE, NOT ONLY THE CASH AND CREDIT BALANCE TO WHICH THE REFUND OR CREDIT 
WAS OBTAINED, BUT ALSO ALL ACCESS TO THE SERVICE INCLUDING PREVIOUSLY PURCHASED ADDITIONAL FEATURES AND/OR THE FREE 
ASPECTS OF THE VIRTUAL WORLD OF KANEVA. YOU FURTHER AGREE THAT THE PAID ASPECTS AND/OR ADDITIONAL FEATURES FORM PART 
OF THE SERVICE AND ARE DELIVERED �UNSEALED� AUTOMATICALLY UPON RECEIPT OF THE FEES AND THEREFORE YOU HAVE NO 
CANCELLATION RIGHTS (SAVE FOR A WILFULL ERROR OR FAULT WITH THE PAID ASPECTS AND/OR ADDITIONAL FEATURES IF REPORTED 
IMMEDIATELY TO KANEVA).
</p>
<br/>
<p>
(c) Kaneva reserves the right to alter the fees payable for the paid aspects of the Virtual World of Kaneva 
and/or Additional Features or introduce alternative fee structures for the Service. If you do not agree to 
any such alteration, you should not purchase Additional Features and/or you should terminate your Account 
prior to the date on which the alteration takes effect, otherwise the revised amount will automatically be 
debited to your Account.  
</p>
<br/>
<h2>9. VIRTUAL WORLD CURRENCY</h2>
<p>
KANEVA "CURRENCY" IS A LIMITED LICENSE RIGHT AVAILABLE FOR PURCHASE OR FREE DISTRIBUTION AT KANEVA'S DISCRETION, 
AND IS NOT REDEEMABLE FOR MONETARY VALUE FROM KANEVA. You acknowledge that the Service presently includes a 
component of in-world fictional currency ("Credits" or "Kaneva Credits"), which constitutes a limited license 
right to use a feature of our product when, as, and if allowed by Kaneva. Kaneva may charge fees for the right 
to use Kaneva Credits, or may distribute Kaneva Credits without charge, in its sole discretion. Regardless of 
terminology used, Kaneva Credits represent a limited license right governed solely under the terms of this 
Agreement, and are not redeemable for any sum of money or monetary value from Kaneva at any time. You agree that 
Kaneva has the absolute right to manage, regulate, control, modify and/or eliminate such Currency as it sees fit 
in its sole discretion, in any general or specific case, and that Kaneva will have no liability to you based on 
its exercise of such right. 
</p>
<br/>
<h2>10. AGREEMENT TO PRICING AND BILLING</h2>
<p>
IN THE EVENT YOU CHOOSE TO USE PAID ASPECTS OF THE SERVICE, YOU AGREE TO THE POSTED PRICING AND BILLING POLICIES 
ON THE SITE. Certain aspects of the Service are provided for a fee or other charge. These fees and charges are 
described on the Site, and in the event you elect to use paid aspects of the Service, you agree to the pricing, 
payment and billing policies applicable to such fees and charges. Kaneva may add new services for additional 
fees and charges, or proactively amend fees and charges for existing services, at any time in its sole discretion. 
</p>
<br/>
<h2>11. NO REFUNDS FOR CANCELLATION OR TERMINATION</h2>
<p>
YOU MAY CANCEL YOUR ACCOUNT AT ANY TIME; HOWEVER, THERE ARE NO REFUNDS FOR CANCELLATION. Accounts may be cancelled 
by you at any time. Upon your election to cancel, your account will be cancelled within 2 business days. There 
will be no refunds for any unused credits or any prepaid fees for any portion of the Service. 
</p>
<br/>
<ul class="greenarrow">
<li>
Kaneva has the right at any time for any reason or no reason to suspend or terminate your Account, terminate this 
Agreement, and/or refuse any and all current or future use of the Service without notice or liability to you. In 
the event that Kaneva suspends or terminates your Account or this Agreement, you understand and agree that you 
shall receive no refund or exchange for any unused credits on the account, any license or fees, any content or 
data associated with your Account, or for anything else.  
</li>
<br/>
<li>
In the event an Account is suspended or terminated for your breach of this Agreement (in each case as determined 
in Kaneva's sole discretion), Kaneva may suspend or terminate the Account associated with such breach and any or 
all other Accounts held by you or your affiliates, and your breach shall be deemed to apply to all such Accounts. 
</li>
</ul>
<br/>
<h2>12. COPYRIGHTS AND TRADEMARKS</h2>
<p>
EXCEPT AS EXPRESSLY PROVIDED HEREIN BY THESE TERMS, NEITHER KANEVA, NOR ANY THIRD PARTY HAS CONFERRED UPON YOU 
BY IMPLICATION, ESTOPPEL OR OTHERWISE, ANY LICENSE OR RIGHT UNDER ANY PATENT, TRADEMARK, COPYRIGHT OR OTHER 
PROPRIETARY RIGHTS TO USE THE SITE OR ANY APPLICATIONS OFFERED ON THE SITE. NO OWNERSHIP RIGHTS ARE OR WILL BE 
ASSIGNED TO YOU. Kaneva will not tolerate copyright infringement and reserves the right to block, disable, or 
otherwise remove any content or materials uploaded to the Site as well as terminate access to the Site if you 
engage in copyright or other intellectual property infringement. The law provides for civil and criminal 
penalties for copyright and other intellectual property law infringements. Displaying, performing, storing, copying, distributing, or otherwise making available or using any content from the Site or Applications is prohibited, unless specifically authorized by Kaneva. Accordingly no such content or Applications may be used on another web site without express written permission from Kaneva. Please see Kaneva's Copyright Policy for more detailed copyright and trademark information, use limitations and for notice and procedures for making a claim of copyright infringement to Kaneva.  
</p>
<br/>
<h2>13. PRIVACY POLICY</h2>
<p>
Kaneva respects your right to privacy and understands that visitors to Kaneva need to be in control of their 
personal information. To that end Kaneva has developed a Privacy Policy, and you should review it carefully. 
Your use of the Site is your consent to the Privacy Policy. 
</p>
<br/>
<h2>14. SUBMITTING MATERIALS AND FEEDBACK</h2>
<p>
Unless you enter into a separate written agreement with Kaneva, Kaneva does not claim ownership in Materials 
you submit, however, by submitting Materials in any form to Kaneva, in addition to other provisions of the 
Terms, you automatically grant Kaneva a royalty-free, world-wide, non-exclusive, and assignable right and 
license to use, copy, reproduce, modify, adapt, publish, edit, translate, create derivative works from, 
transmit, distribute, publicly display and publicly perform such Materials for the purpose of displaying 
and promoting the Materials on any web site operated by, and in any related marketing materials produced by, 
Kaneva and its affiliates. 
</p>
<br/>
<h2>15. INDEMNITY</h2>
<p>
You agree to indemnify and hold Kaneva, its, affiliates, officers, agents, partners and employees harmless 
from any claim or demand, including reasonable attorneys fees, arising out of your content and materials, 
your use of the Site, your violation of these Terms or your violation of any third party's rights including 
such party's copyrights and trademarks. 
</p>
<br/>
<h2>16. DISCLAIMER OF WARRANTIES</h2>
<p>
EXCEPT IF EXPRESSLY PROVIDED OTHERWISE IN A WRITTEN AGREEMENT (SUCH AS A EULA) BETWEEN YOU AND KANEVA, 
THE SITE AND APPLICATIONS OFFERED AT OR THROUGH THE SITE, AND APPLICATIONS ARE PROVIDED TO YOU "AS IS" 
WITHOUT WARRANTY OF ANY KIND AND WITH ALL RISKS. KANEVA HEREBY DISCLAIMS TO THE MAXIMUM EXTENT PERMITTED 
BY LAW (A) ALL WARRANTIES EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES 
OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE; AVAILABILITY OF THE SITE AND APPLICATIONS; LACK 
OF VIRUSES, WORMS, TROJAN HORSES, OR OTHER CODE THAT MANIFESTS CONTAMINATING OR DESTRUCTIVE PROPERTIES; 
ACCURACY, COMPLETENESS, RELIABILITY, TIMELINESS, CURRENCY, OR USEFULNESS OF ANY INFORMATION ON THE SITE; 
AND (B) ANY DUTIES OF REASONABLE CARE, WORKMANLIKE EFFORT OR LACK OF NEGLIGENCE IN CONNECTION WITH THE 
SITE, APPLICATIONS, OR RELATED INFORMATION. THE ENTIRE RISK AS TO SATISFACTORY QUALITY, PERFORMANCE, 
ACCURACY AND EFFORT IN CONNECTION WITH THE SITE, APPLICATIONS, AND RELATED INFORMATION IS BORNE BY YOU. 
SOME STATES DO NOT ALLOW THE EXCLUSION OR LIMITATION OF IMPLIED WARRANTIES, SO THE ABOVE LIMITATION 
MAY NOT APPLY TO YOU.  
</p>
<br/>
<p>
IN ADDITION, KANEVA DISCLAIMS ANY WARRANTIES OF NON-INFRINGEMENT, TITLE OR QUIET ENJOYMENT IN CONNECTION 
WITH THE SITE, APPLICATIONS, AND RELATED INFORMATION. 
</p>
<br/>
<h2>17. ASSUMPTION OF RISKS</h2>
<p>
YOU ASSUME ALL RISKS THAT THE SITE, APPLICATIONS, AND RELATED INFORMATION ARE SUITABLE OR ACCURATE 
FOR YOUR NEEDS AND WILL BE UNINTERRUPTED, TIMELY, SECURE OR ERROR-FREE. ANY APPLICATIONS DOWNLOADED 
OR OTHERWISE OBTAINED THROUGH THE SITE ARE AT YOUR OWN DISCRETION AND RISK AND YOU ARE SOLELY 
RESPONSIBLE FOR ANY DAMAGE TO YOUR COMPUTER OR LOSS OF DATA. BY PARTICIPATING IN MULTI-PLAYER GAMES 
OR VISITING CHAT ROOMS YOU MAY BE EXPOSED TO RUDE, CRUDE, INDECENT, OR OTHER OFFENSIVE LANGUAGE OR 
REFERENCES. YOU AGREE THAT KANEVA SHALL NOT BE RESPONSIBLE FOR ANY LOSS OR DAMAGE OF ANY SORT 
RELATING TO YOUR DEALINGS WITH ANY THIRD PARTY ADVERTISER OR CONTENT PROVIDER ON THE SITE. 
</p>
<br/>
<h2>18. NO INCIDENTAL, CONSEQUENTIAL OR CERTAIN OTHER DAMAGES</h2>
<p>
TO THE MAXIMUM EXTENT ALLOWED BY LAW, YOU AGREE THAT NEITHER KANEVA NOR ANY OF KANEVA'S AFFILIATES 
OR AGENTS WILL BE LIABLE TO YOU AND/OR ANY OTHER PERSON FOR ANY CONSEQUENTIAL OR INCIDENTAL DAMAGES 
(INCLUDING BUT NOT LIMITED TO LOST PROFITS, LOSS OF PRIVACY OR FOR FAILURE TO MEET ANY DUTY 
INCLUDING BUT NOT LIMITED TO ANY DUTY OF GOOD FAITH, LACK OF NEGLIGENCE OR OF WORKMANLIKE EFFORT) 
OR ANY OTHER INDIRECT, SPECIAL, OR PUNITIVE DAMAGES WHATSOEVER THAT ARISE OUT OF OR ARE RELATED 
TO THE SITE, APPLICATIONS OR RELATED INFORMATION, OR TO ANY BREACH OF THESE TERMS, EVEN IF KANEVA 
HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES AND EVEN IN THE EVENT OF FAULT, 
TORT (INCLUDING NEGLIGENCE) OR STRICT OR PRODUCT LIABILITY. SOME STATES DO NOT ALLOW THE EXCLUSION 
OR LIMITATION OF LIABILITY FOR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION MAY NOT APPLY TO YOU.  
</p>
<br/>
<h2>19. LIMITATION OF LIABILITY AND EXCLUSIVE REMEDY</h2>
<p>
YOU AGREE THAT YOUR SOLE REMEDY FOR ANY BREACH OF THESE TERMS BY KANEVA OR ANY KANEVA AGENTS SHALL 
BE, AT KANEVA'S OPTION, (1) SUBSTITUTION OR REPLACEMENT OF ALL OR PART OF THE APPLICATION OR PRODUCT 
THAT GIVES RISE TO DAMAGES INCURRED BY YOU IN REASONABLE RELIANCE ON KANEVA; OR (2) REFUND OF THE 
AMOUNT THAT YOU PAID TO KANEVA. YOU AGREE THAT THE DAMAGE EXCLUSIONS IN THESE TERMS SHALL APPLY EVEN 
IF ANY REMEDY FAILS OF ITS ESSENTIAL PURPOSE. 
</p>
<br/>
<h2>20. YOUR REPRESENTATIONS AND WARRANTIES</h2>
<p> 
You represent and warrant for the benefit of Kaneva, Kaneva�s suppliers, and any third parties mentioned 
on the Site, in addition to other representations and obligations contained in these Terms, that: (a) 
you possess the legal right and ability to enter into and make the representations and warranties contained
in these Terms; (b) all required information (as described in Section 2) submitted by you to the Site is 
true and accurate; (c) you will keep your registration information current; (d) you will be responsible 
for all use of your password even if such use was conducted without your authority or permission; 
(e) you will not use the Site, Applications or Services for any purpose that is unlawful or prohibited by 
these Terms; (f) you are the owner of the Materials and they are original to you; (g) the Materials do not 
infringe any third party right, such as copyright, trademark, and publicity/privacy right; (h) the Materials 
do not constitute defamation or libel or otherwise violate the law, and (i) you agree to defend, indemnify, 
and hold Kaneva (and its employees, representative, agents, and assigns) harmless from breaches of (a) through (h). 
</p>
<br/>
<h2>21. LINKS - ADVERTISERS</h2>
<p>
The Site may contain links to third party sites that are not under the control of Kaneva and Kaneva is 
not responsible for any content on any linked site. If you access a third party site from the Site, 
then you do so at your own risk. Kaneva provides links only as a convenience and the inclusion of the 
link does not imply that Kaneva endorses or accepts any responsibility for the content on those third 
party sites. Additionally, your dealings with or participation in promotions of advertisers found on 
the Site, including payment and delivery of goods, and any other terms (such as warranties) are solely 
between you and such advertisers. You agree that Kaneva shall not be responsible for any loss or damage 
of any sort relating to your dealings with such advertisers. 
</p>
<br/>
<h2>22. INTERNATIONAL USAGE</h2>
<p>
This Site is controlled and operated by Kaneva from its offices within Georgia, United States of America. 
Kaneva makes no representation that the Site, Applications, or related information offered by Kaneva are 
appropriate or available in other locations. Those who choose to access the Site from other locations do 
so on their own initiative and are responsible for compliance with local laws, if and to the extent local 
laws are applicable.
</p>
<br/>
<h2>23. OWNERSHIP</h2>
<p>
The Site, Applications, and related information are the exclusive property of Kaneva or its suppliers. 
All rights not licensed herein are hereby reserved to Kaneva or its suppliers. 
</p>
<br/>
<h2>24. TERMINATION OR CANCELLATION OF SITE ACCESS; MODIFICATIONS TO SITE AND APPLICATIONS</h2>
<p>
If you violate these Terms, Kaneva may terminate or cancel your access rights to the Site and/or Applications 
immediately without notice. Kaneva may also block your use of the Site and/or Applications or direct you to 
cease using it. Kaneva reserves the right at any time to modify or discontinue the Site, Applications, or any 
part thereof and you agree that Kaneva shall not be liable to you or to any third party for any modification, 
suspension, or discontinuance of the Site, Applications, or any part thereof, except you may receive a pro-rata 
refund in the event such modification, suspension, or discontinuance materially affects your access to those parts 
of the Site or Applications that you have paid for. 
</p>
<br/>
<h2>25. EXCLUSIVE JURISDICTION</h2>
<p>
These Terms shall be governed by the laws of the State of Georgia without regard to its conflict of law provisions.
Any disputes arising under or related in any way to these Terms, the Site or any Application shall be litigated or 
otherwise heard in the appropriate forum in Fulton county Georgia. The parties hereto hereby consent to the 
exclusive jurisdiction of the state and federal courts sitting in Fulton County Georgia, and hereby waive any claim 
or defense that such forum is not convenient or proper, and consent to service of process by any means authorized by 
Georgia law.  
</p>
<br/>
<h2>26. ENTIRE AGREEMENT</h2>
<p>
These Terms, as amended, your registration forms, and the disclosures provided by Kaneva and the consents 
provided by you, constitute the entire agreement between you and Kaneva. If any provision of these Terms shall 
be unlawful, void, or for any reason unenforceable, then that provision shall be deemed severable from these Terms 
and shall not affect the validity and enforceability of any remaining provisions. Kaneva's failure to act with 
respect to a breach by you or others does not waive Kaneva's right to act with respect to subsequent or similar breaches. 
</p>
<br/>
<h2>27. AMENDMENTS</h2>
<p>
You agree that Kaneva may amend or modify these Terms or impose new conditions at any time by updating these 
Terms on the Site or upon notice from Kaneva to you as published through the Site or by email. Any use of 
the Site or order by you after such updating shall be deemed to constitute acceptance of such amendments, 
modifications, or new conditions. If you do not want to be bound by an amendment, you will need to terminate 
your registration, if any, and refrain from using the Site or using or ordering Applications after that date. 
No other amendments will be valid unless they are in a paper writing signed by Kaneva and by you.  
</p>
<br/>
<h2>28. NOTICES</h2>
<p>
Except as expressly stated otherwise, any notices required or allowed under these Terms shall be given to 
Kaneva by postal mail to: Kaneva, 270 Carpenter Drive, Suite 350, 
Sandy Springs, Georgia 30328, or as to a successor address that Kaneva makes available on the Site or through 
other reasonable manner. If applicable law requires that Kaneva accepts email notices (but not otherwise), 
then you may send Kaneva email notice using Kaneva's Contact Kaneva Forum. With respect to Kaneva�s notices 
to you, Kaneva may provide notice of amendments by posting them in the Site and you agree to check for 
changes. Instead or in addition, Kaneva may give notice by sending email to the email address you provide 
during registration. Notice shall be deemed given 24 hours after it is posted or an email is sent, unless 
(as to email) the sending party is notified that the email address is invalid.  
</p>
<br/>
<p>
Last Updated on March 9, 2015.
</p>
														
															</div>
														</td>
													</tr>
												</table>
											</td>
											
										</tr>
									</table>
									<span class="cb"><span class="cl"></span></span>
									</div>
								</td>
								
									
							</tr>
						</table>
					</td>
					<td class=""><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img3"/></td>
				</tr>
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
				<tr>
					<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" id="Img4"/></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
