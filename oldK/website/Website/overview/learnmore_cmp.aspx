<%@ Page language="c#" Codebehind="learnmore_cmp.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.overview.learnmore_cmp" %>

<script type="text/javascript" src="../jscript/SWFObject/swfobject.js"></script>

<style type="text/css">

p {font-size:12px;font-weight:bold;line-height:15px}
</style>

<br/>
<table cellpadding="0" cellspacing="0" class="container" border="0">
	<tr>
		<td width="45%" class="mktText" style="padding:10px;" valign="top">

		<div style="font-size:16pt;font-weight:bold">Connected Media Module: The Next Generation Media Player</div><br/>
		
		<p>
			Harness the power of the Internet with Kaneva�s Connected Media Module, a scalable, embedded 
			Flash media player for experiencing videos, photos and audio that can also be used to publish 
			your media anywhere on the net.  
		</p>
		
		<p>
			Encapsulated in the Module is all of the meta information and innovative features such as: 
		</p>

		</td>
		
		<td width="5%"></td>
		
		<td width="45%" class="mktText" style="padding:10px;" valign="top" align="center">

			<div id="o95"><div class="mkttext"><p/><p/><b style="font-size: 11px;">This computer needs Flash 8 or later to view the media.</b></p><p>To get Flash, <a href="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash">click here.</a></p></div></div><script type="text/javascript">var so = new SWFObject("../flash/promos/CMP_diag.swf", "", "400 ", "600", "8", "#ffffff"); so.addVariable("quality", "high"); so.write("o95"); </script>

		</td>
	</tr>
</table>

