<%@ Page language="c#" Codebehind="aboutLanding.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.aboutLanding" %>

<link href="../css/friends.css" rel="stylesheet" type="text/css"/>

<link href="../css/new.css" rel="stylesheet" type="text/css" />		

<table border="0" cellspacing="0" cellpadding="0"  width="990" align="center">
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" id="Img1"/></td>
	</tr>
	<tr>
		<td>
			<table  border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="frTopLeft"></td>
					<td class="frTop"></td>
					<td class="frTopRight"></td>
				</tr>
				<tr>
					<td class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img2"/></td>
					<td valign="top" class="frBgIntMembers">
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
								
									<!-- TOP STATUS BAR -->
									<div id="pageheader">
										<table cellpadding="0" cellspacing="0" border="0" width="99%">
											<tr>
												<td nowrap align="left">
													<table cellpadding="0" cellspacing="0" border="0">
														<tr>
															<td><h1>About Kaneva</h1></td>
														</tr>
													</table>
												</td>
												
												<td align="right" valign="middle">
													<table cellpadding="0" cellspacing="0" border="0" width="690">
														<tr>
															<td class="headertout" width="175"></td>
															<td class="searchnav" width="260"></td>
															<td align="left" width="130"></td>
															<td class="searchnav" align="right" width="210" nowrap>
																
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</div>
									<!-- END TOP STATUS BAR -->	
								
								</td>
							</tr>
							<tr>
								<td width="968" align="left"  valign="top">
									<div class="module whitebg">
									<span class="ct"><span class="cl"></span></span>
									<table cellpadding="0" cellspacing="0" border="0"  width="98%">
										<tr>
											
											<td width="230" valign="top">
												<br>
												<table border="0" cellspacing="0" cellpadding="0" width="190">
													<tr>
														<td id="pagenav">
															<ul>
																<li>
																	<a class="selected" runat="server" href="~/overview/aboutLanding.aspx">About Kaneva</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/vision.aspx">History</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/company.aspx">Leadership</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/ourvalues.aspx">Culture & Values</a> 
																</li>
																<li>
																	<a runat="server" href="http://www.kaneva.com/community/ShareKaneva.channel">Share Kaneva</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/advertising.aspx">Advertising & Sponsors</a> 
																</li>
																<li>
																	<a runat="server" href="~/community/news.kaneva">News / Press Center</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/contactus.aspx">Contact Us</a> 
																</li>
																
															</ul>
														</td>
													</tr>																														
												</table>
												
											</td>
											<td valign="top" align="center" width="710">
												
												<table border="0" cellspacing="0" cellpadding="0" width="98%">
													<tr>
														<td>
															<br/>
															
															<div style="text-align:justify;padding-right:10px;">
															
<h2>It's a brave, new world.</h2> 

<p>
Combining a social network and a virtual world, Kaneva brings profiles and entertainment to the 
3D realm in a modern-day, online digital world full of real friends and good times. Kaneva provides 
a whole new way -- a more human way -- to connect with friends online. 
</p>
<br />
<p>
Kaneva members create the digital version of themselves -- avatars -- and then meet up in a vibrant, 
3D world based on the modern day. Every Kaneva member gets a Kaneva City Loft -- their own 3D space -- 
that they can decorate and furnish in their unique style. You can bring your favorite videos, 
photos, music, and games, and watch them on your 3D televisions. You can invite friends to hang 
out in your 3D home or meet up in any of Kaneva�s public spaces and chat in real-time. You can 
shop for the latest fashions or home decor, chat, dance, play games, watch TV and movies, and 
come back again and again to explore and have fun in an ever evolving world full of exciting 
people, places and entertainment. 
</p>

														
															</div>
														</td>
													</tr>
												</table>
											</td>
											
										</tr>
									</table>
									<span class="cb"><span class="cl"></span></span>
								</div>
								</td>
								
									</td>
							</tr>
						</table>
					</td>
					<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img3"/></td>
				</tr>
				<tr>
					<td class="frBottomLeft"></td>
					<td class="frBottom"></td>
					<td class="frBottomRight"></td>
				</tr>
				<tr>
					<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" id="Img4"/></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
