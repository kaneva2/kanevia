<%@ Page language="c#" Codebehind="learnMore_producer.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.learnMore" %>
<%@ Register TagPrefix="Kaneva" TagName="HeaderText" Src="../usercontrols/HeaderText.ascx" %>

<script type="text/javascript" src="../jscript/SWFObject/swfobject.js"></script>
<script language="javascript" src="../jscript/prototype.js"></script>

<style type="text/css">

p {font-size:12px;font-weight:bold;line-height:15px}
</style>

<center>
<br/>
<table cellpadding="0" cellspacing="0" class="container" border="0">
	<tr>
		<td width="45%" class="mktText" style="padding:10px;" valign="top" align="left">

			<div id="o96"><div class="mkttext"><p/><p/><b style="font-size: 11px;">This computer needs Flash 8 or later to view the media.</b></p><p>To get Flash, <a href="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash">click here.</a></p></div></div><script type="text/javascript">var so = new SWFObject("../flash/promos/broadband_topics.swf", "", "260 ", "90", "8", "#ffffff"); so.addVariable("quality", "high"); so.write("o96"); </script>
			
			<p>
				Are you a professional producer� or an aspiring one? Become a Kaneva Producer and create your 
				own Community. Imagine what you can do.
			</p>
			 
			<p>
				By producing a Kaneva Community, you can tell the story of your special interest and 
				dynamically grow a global audience and fan base. 
			</p>
			 
			<p>
				You have complete creative control of your Community and you retain full content 
				ownership.  Kaneva provides you with all the easy-to-use tools, the creative freedom and social 
				networking power to create a multimedia-rich, topical Community to share with the world.  
			</p>
			
		
			<p><a id="joinLink" runat="server">Get started now >>></a></p>

			<p><a href="~/overview/learnmore_channels.aspx" runat="server" id="A2">Learn About Communities >>></a></p>

			<p><a href="~/overview/whyjoin.aspx" runat="server" id="A3">Why Join >>></a></p>

		</td>

		<td width="5%"></td>
		
		<td width="45%" class="mktText" style="padding:10px;" valign="top">
				
			<p style="color:#9cc83a;padding-top:10px">
			We provide innovative interaction capabilities and a growing viewer community to get your work seen, 
			shared, talked about ... and raved.
			</p>
			
			<div>
			
				<img src="../images/content/connected_media_tracker_dua.jpg" border="0" />
				
				<p>CONNECTED MEDIA TRACKER</p>
				
				<p style="font-weight:normal">
					See how one person can make a difference. Our tracking system helps illustrate the power 
					of six degrees of separation. As your media is being shared, our built-in social networking 
					platform lets you know who your �activists� are and how, when and where your Community 
					is being embraced.
				</p>
				
				<div style="padding-top:6px">
					<div style="float:right;padding-left:6px">
						<img src="../images/content/man_bear.jpg" border="0" />
					</div>
					
					<p>CONNECTED MEDIA MODULE</p>
					
					<p style="font-weight:normal;">
						The next generation media player. 
					</p>
					
					<p style="font-weight:normal">
						Scalable, customizable Flash media player for showing photos, videos and audio. 
					</p>
					
					<p style="font-weight:normal">
						Can be easily embedded, �skinned� and sized to fit into any website.
					</p>
					
					<p><a href="~/overview/learnmore_cmp.aspx" runat="server" id="A4">Learn More >>></a></p>
				</div>
			
			</div>
		</td>
	</tr>
</table>
<br/>
</center>