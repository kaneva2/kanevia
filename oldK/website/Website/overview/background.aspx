<%@ Page language="c#" Codebehind="background.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.background" %>
<center>
<table cellpadding="0" cellspacing="0" border="0" width="710">
<tr>
	<td align="left"><a runat="server" href="~/free-virtual-world.kaneva" ID="A4"><img border="0" style="vertical-align:middle;" runat="server" src="~/images/Kaneva_logo_150x50.jpg" ID="Img4"/></a> <span style="font-family:arial; font-size: 27px; font-weight:bold;">Corporate Information</span></td>
</tr>
<tr>
	<td align="left"><span style="font-size:6px;"><br><br></span><span style="font-size:6px;"><br><br></span>
	</td>
</tr>
<tr>
	<td>
	<span class="orangeHeader3">Kaneva Overview</span>
	</td>
</tr>
<tr>
	<td valign="top" class="bodyText">
<p>Co-founded in 2004 by Internet security expert, Christopher Klaus, and avid gamer, Greg Frame, Kaneva is a true digital entertainment marketplace where people can watch, play, create and self-publish films and
games.</p>

<p>Frustrated by the lack of distribution opportunities for the mass majority of films and
games as well as the challenges of creating games, Klaus and Frame set out to
develop a functionally-rich, user-driven web site as well as complete
technology platform for giving filmmakers and game creators a new way to
self-publish and directly distribute their work, as well as a complete
technology platform for creating online games, bringing games to market faster
at a lower cost.</p>

<p>In Latin, Kaneva translates to �canvas�. Using what is today Kaneva.com, Kaneva provides a digital canvas for
the world to share movies, documentaries, television shows, foreign films,
music videos, massively multiplayer online (MMO) games, casual games, online
art assets and more.</p>

<p><span class="orangeHeader3">Why Kaneva?</span></p>


<p>Today�s film and game industries prohibit most filmmakers and game creators from making
it big. Historically, significant financial backing and influential connections
have been necessary to open the doors to mass distribution and the opportunity
to earn a living. Even then, most filmmakers lose their creative rights and are
forced to agree to low royalty payments, giving large studios the majority of
the profits.</p>


<p>Kaneva�s unique business model is for the first time empowering filmmakers and game
creators to self-publish, distribute and sell their work to a mass audience
using the Internet. Kaneva�s state-of-the-art underlying technology and online
business services, make it easy for anyone with a passion and talent to be
successful. At the same time, Kaneva is changing the way people watch films and
play and create games.</p>

<p><span class="orangeHeader3">Watch: No More Rentals, Download-to-Own a Wide
Variety of Films</span></p>

<p>Kaneva offers immediate gratification with download-to-own, DVD quality movies that you can keep for
your personal use. This content is viewable on computers, mobile video devices
and can easily be transferred to DVD to watch on television. With collaborative searching capabilities and unique
interactive communities, viewers can quickly find the media that best maps to
their interests and choose from an ever-growing, online collection of
everything from shorts and feature length films to educational videos, trailers,
music videos and videoblogs. Kaneva�s unique community features enable viewers
to interact with filmmakers as well as their peers by chatting, writing,
reading, rating, and posting reviews.</p>

<p><span class="orangeHeader3">Play: Experience Games Online Arcade-Style</span></p>

<p>Kaneva offers gamers an arcade-style online gaming capability that is totally immersive, social and
interactive. Gamers can choose and play from a continually growing collection
of online games and take advantage of flexible payment options to play
professionally crafted and beta version games of every genre. Play anytime, anywhere games featured on Kaneva located in
special gaming communities that include forums and blogs. Gamers can also write and read
reviews of games by gamers, rate games, and interact with other gamers in a
dynamic online community designed especially for them.</p>

<p><span class="orangeHeader3">Create: Empowering Anyone with a Passion to Make
their Own Games</span></p>

<p>Kaneva provides a powerful new way for game developers to create and sell their work.
Whether they are from an established game studio, a modder or just someone who
has a passion and dream of making their own game, Kaneva empowers state-of-the-art,
multi-genre game creation faster and with richer content at a much lower cost.
Using the Kaneva Game Platform (KGP) -- the first, downloadable Massively
Multi-Player Online Game (MMOG) engine -- combined with extensive online
tutorials, active forums and a computer art asset marketplace, game creators have
easy access to all of the tools they need to quickly create your own worlds.
What�s more, once a game is developed using the platform, game creators can rely
on Kaneva�s Managed Game Services to quickly and cost-effectively distribute their
games on Kaneva.com, with the potential to earn worldwide recognition and up to
70% royalties, all while they maintain complete content control.</p>

<p>The Kaneva Studio is helping change the way game developers build Massively
Multi-player Online Games (MMOG) by testing and implementing new techniques not
only for game creation but also for minimizing the time to market and resources
needed for building games. Made up of some of the
most talented game and creative directors, integrators, animators, modelers and
texture artists in the industry, the Kaneva Studio brings multiplayer online
games and movies to life through the complete life cycle of conception and
pre-production, production and post-production. The Kaneva Studio is the interface to getting new features and requirements in the Kaneva Game Platform by gathering those requirements from partner game
and movie studios using the Kaneva Game Platform. Kaneva Studio�s
flagship MMO game, Gorilla Paintball, available for immediate play on
Kaneva.com features multiple arenas, and a unique underground city.</p>

<p><span class="orangeHeader3">Publish: Empowering Filmmakers and Game Creators to
Share and Sell Their Work</span></p>

<p>Without relying on traditional retail channels, filmmakers and game creators can now
earn significant royalties while remaining non-exclusive and maintaining their
copyrights and ownership. Through Kaneva publishing, filmmakers and game
creators can be easily and immediately connected to viewers, gaining instant
fame and recognition. By establishing their own interactive Communities on Kaneva,
filmmakers and game creators can create their own identity and promote and sell
their work while facilitating discussions with their viewers and players.
Kaneva also features a digital art assets marketplace for game creators and
filmmakers to buy and sell art assets needs for their creations.</p>

<p>Whether you come to watch, play, create or publish, Kaneva combines depth and breadth
of quality content, with the ability to self-publish, create and interact in an
engaging, community-driven arena.</p><br><br><br><br>

</td>
</tr>
</table>


</center>
