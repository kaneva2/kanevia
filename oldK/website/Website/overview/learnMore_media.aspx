<%@ Page language="c#" Codebehind="learnMore_media.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.overview.learnMore_media" %>

<style type="text/css">

p {font-size:12px;font-weight:bold;line-height:15px}
</style>

<center>
<br/>
<table cellpadding="0" cellspacing="0" class="container" border="0">
	<tr>
		<td width="45%" class="mktText" style="padding:10px;" valign="top">

		<div style="font-size:16pt;font-weight:bold">MEDIA ON KANEVA - HARNESSING THE POWER OF THE INTERNET</div><br/>
		
		<br/>
		<p>
			Kaneva Media is all about harnessing the power of the Internet to connect media, people and 
			their passions. 
		</p>
		
		<p>
			Create a <a href="learnmore_people.aspx">Profile</a> all about you or a topically-rich
			<a href="learnmore_channels.aspx">Community</a> and use Kaneva�s 
			<a href="learnmore_cmp.aspx">Connected Media Module</a> to not only publish media on your 
			Profile or Community but to place any of your media,	anywhere on the net.
		</p>
		
		<p>
			Leverage Kaneva�s built-in social networking platform for global sharing, extensive 
			friend-to-friend distribution. 
		</p>
		
		<p>
			Kaneva�s Connected Media functionality also enables you to be entertained and interact with people and media in totally 
			new ways. Experience a wide variety of media online or in our Virtual World, in thousands of Profiles and Communities and then go way beyond 
			passive viewing to share, comment, rave, search, tag and join. 
		</p>
		
		<p>
			There is no place like Kaneva, your entertainment network to get connected. 
		</p>
		
		<br/>
		<p><a id="joinNow" runat="server">Get started now >>></a></p>

		<p><a href="~/overview/whyjoin.aspx" runat="server" id="A3">Why Join >>></a></p>

		</td>

		<td width="5%"></td>
		
		<td width="45%" class="mktText" style="padding:10px;" valign="top">
				
		<div style="padding-top:20px">
		
		<img src="../images/content/connected_media_tracker.jpg" border="0" />
		
		<p>CONNECTED MEDIA TRACKER</p>
		
		<p style="font-weight:normal">
			See how one person can make a difference. Our Viral Tracking System helps illustrate the power 
			of sharing on the net. As your media is being shared, our built-in social networking infrastructure 
			lets you know who your �activists� are and how, when and where your Profile, Community, and media are being embraced. 
		</p>

		</div>
		
		</td>
	</tr>
</table>
<br/>
</center>