<%@ Page language="c#" Codebehind="privacy.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.privacy" %>

<link href="../css/friends.css" rel="stylesheet" type="text/css"/>
<link href="../css/new.css" rel="stylesheet" type="text/css" />		

<table border="0" cellspacing="0" cellpadding="0" class="newcontainer" align="center">
	<tr>
		<td>
			<table  border="0" cellpadding="0" cellspacing="0" width="100%" class="newcontainerborder">
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
				<tr>
					<td class=""><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img2"/></td>
					<td valign="top" class="newdatacontainer">
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
								
									<!-- TOP STATUS BAR -->
									<div id="pageheader">
										<table cellpadding="0" cellspacing="0" border="0" width="99%">
											<tr>
												<td nowrap align="left">
													<table cellpadding="0" cellspacing="0" border="0">
														<tr>
															<td><h1>Privacy Policy</h1></td>
														</tr>
													</table>
												</td>
												
												<td align="right" valign="middle">
													<table cellpadding="0" cellspacing="0" border="0" width="690">
														<tr>
															<td class="headertout" width="175"></td>
															<td class="searchnav" width="260"></td>
															<td align="left" width="130"></td>
															<td class="searchnav" align="right" width="210" nowrap>
																
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</div>
									<!-- END TOP STATUS BAR -->	
								
								</td>
							</tr>
							<tr>
								<td width="968" align="left"  valign="top">
									<div class="module whitebg">
									<span class="ct"><span class="cl"></span></span>
									<table cellpadding="0" cellspacing="0" border="0"  width="98%">
										<tr>
											
											<td width="230" valign="top">
												<br>
												<table border="0" cellspacing="0" cellpadding="0" width="190">
													<tr>
														<td id="pagenav">
															<ul>
																<li>
																	<a runat="server" href="~/overview/guidelines.aspx">Member Guidelines</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/rulesofconduct.aspx">Rules of Conduct</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/termsandconditions.aspx">Terms & Conditions</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/copyright.aspx">Copyright Policy</a> 
																</li>
																<li>
																	<a class="selected" runat="server" href="~/overview/privacy.aspx">Privacy Policy</a> 
																</li>
																<li>
																	<a runat="server" href="~/community/safety.kaneva">Safety Center</a> 
																</li>
																<li>
																	<a runat="server" id="aHelp" target="_blank">Help Center</a> 
																</li>

															</ul>
														</td>
													</tr>																														
												</table>
												
											</td>
											<td valign="top" align="center" width="710">
												
												<table border="0" cellspacing="0" cellpadding="0" width="98%">
													<tr>
														<td>
															<br/>
															
															<div style="text-align:justify;padding-right:10px;">


<p align="justify"><b>Kaneva is strongly committed to protecting your privacy 
and creating a strong and welcoming bond with our members.</b></p>
<br/>

<p>
This privacy policy 
explains the use of information collected through our website, applications, 
and customer service interactions. The term “personal information” as used in 
this privacy policy means any information that may be used to identify an 
individual, including, but not limited to, billing name, home or other physical 
address, email address, phone number or other contact information, at any 
location where you access Kaneva. Kaneva advises that in connection with 
registration, you do not need to use your full or true name for your member 
name. We urge everyone to take an active role in helping us protect their privacy 
by becoming aware of the policies you will encounter here.</p>
<br/>
<ul>
<li>
We collect personal information and usage statistics to sustain high-quality 
member experiences and provide excellent member service. 
</li>
<li>
Some information we request from you when you join Kaneva or participate in a 
financial transaction. Other data are gathered indirectly from website traffic, 
your computer hardware and Internet connection, and usage of Kaneva websites, 
applications, and services.
</li>
<li>
Except under certain limited circumstances set forth here and in the Kaneva 
Terms & Conditions posted at http://www.kaneva.com/overview/termsandconditions.aspx, 
Kaneva does not disclose your personal information to any third parties without 
your permission. Kaneva may disclose your personal information if required to do 
so or in the good faith belief that such action is necessary to: (a) conform to the 
edicts of the law or comply with legal process served on Kaneva, the Kaneva website 
or any Kaneva member; (b) protect and defend the rights or property of Kaneva, 
The Virtual World of Kaneva, or the members of The Virtual World of Kaneva; or 
(c) act in urgent circumstances to protect the personal safety of members of the 
Kaneva website, The Virtual World of Kaneva, or the public. In addition, if Kaneva 
should ever merge with or be acquired by another company, or file for bankruptcy, 
Kaneva may sell the information you provide to us on this site to a third party or 
share your personal information with any company with whom we merge. 
</li>
<li>
You will have the ability to update the personal data you provided to us during 
registration from the Kaneva website or by contacting us at the Support Center 
after you sign into the Kaneva web site. 
</li>
<li>
Kaneva will update this privacy policy in a timely fashion when any changes occur. 
</li>
</ul>
<br/>
<p>
The privacy policy that follows applies to the Kaneva site and other Internet-accessible 
Kaneva-branded services that incorporate this policy (although we will refer here only to 
Kaneva to make reading this policy easier). Finally, please note that this policy applies 
only to Kaneva, and not to other companies' or organizations' Websites to which we link. 
</p>
<br/>
<ul>
<li>
The Information We Collect and How We Collect It 
</li>
<li>
Information Displayed to Other Users 
</li>
<li>
Disclosing Personal Information in Profiles, Forums or in the Virtual World of Kaneva 
</li>
<li>
How We Protect Your Personal Data 
</li>
<li>
Notice to Those Under the Age of 13 
</li>
<li>
Your Consent 
</li>
<li>
How to Contact Us 
</li>
</ul>
<br/>
<h2>The Information We Collect and How We Collect It</h2>
<br/><p> 
There are different levels of information we collect, depending upon your 
relationship to Kaneva. </p>
<br/>
<ul>
<li><b>Anonymous Website Visitor:</b> If you visit our websites, we automatically 
record information related to your IP address, cookie information, and aggregate 
information on what content you access or visit. This information is collected in 
the background while you move around our website.
</li>
<li><b>Mailings:</b> If you choose to provide us personal information to subscribe 
to our announcements or mailing list, we collect information to provide delivery 
such as e-mail address, delivery preferences, and message performance statistics.
</li>
<li>
<b>Registered Kaneva Member:</b> If you choose to register as a Kaneva member on 
our website, we will track and monitor site usage and aggregate usage of website 
features such as media views, member activity, communications, and forums. This is 
in addition to the personal data collected during the registration process.
</li>
<li>
<b>Virtual World of Kaneva Member:</b> If you install or use the Virtual World of 
Kaneva software, we collect and aggregate a variety of data to monitor system and 
simulation performance, and to verify your unique identity. This includes specific 
and general information about your computer hardware and Internet connection, which 
are stored but are not personally identifiable. We track usage of customer-service 
resources to ensure high-quality interactions. This is in addition to the personal 
data and billing information collected during the registration and billing processes.
</li>
<li>
<b>Additionally, there are special situations to mention: </b>
<ul>
<li>
<b>Beta Tester:</b> If you volunteer to serve as a beta tester for pre-release software 
or content, Kaneva tracks bug reports and may monitor individual system performance in 
an effort to rigorously test pre-deployed technology.
</li>
<li>
<b>Former Member:</b> If you cease being a member for any reason, Kaneva may keep a copy 
of your registration and past activity in our database. 
</li>
<li>
<b>Response to Career Postings or Unsolicited Communications:</b> Please note that 
information Kaneva receives in reference to a career posting or through unsolicited 
communications does not fall under the privacy terms outlined in this policy. 
Information from your resume will be used solely for the purposes of evaluating your 
candidacy for employment. Other unsolicited communications, such as email, are the 
property of and may be used by Kaneva as needed to fulfill business requirements.
</li>
<li>
<b>Individuals Outside of the United States:</b> Anyone using Kaneva websites, 
applications, or services from outside of the United States should be aware that 
personal information collected on the websites, applications, and services may be 
stored and processed in the United States or any other country in which Kaneva or its 
affiliates, subsidiaries or agents maintain facilities, and by using these websites, 
applications, and services, you consent to any such transfer of information outside 
of your country.
</li>
</ul>
</ul>
<h2>Information Displayed to Other Visitors and Members</h2>
<br/><p>
Some information about your account is displayed to other visitors and 
members through your Kaneva profile and in the Virtual World of Kaneva 
virtual world, and this same information may be available on other sites 
that summarize this information, such as search engines. This information 
may include your first and last name, member name, age, location, date(s) 
of activity, interests, profile photo, whether you are currently on the 
website or in the virtual world, member rating information, community 
affiliation, and any member-generated content you have provided or added to 
your account. However, we will not display your actual name as submitted for 
financial transactions, address, or credit card or bank account numbers, 
except with your express permission or as otherwise permitted by this 
privacy policy and our terms and conditions. 
</p>
<br/>
<h2>Disclosing Personal Information in Profiles, Forums, or in the Virtual World of Kaneva</h2>
<br/>
<p>
You may choose to disclose personal information in our online forums, 
your Kaneva profile, directly to other members in public or private chat, 
or in other manners while using the Kaneva service. Please be aware that such 
information is public information, may be misused by anyone accessing Kaneva, 
and you should not expect privacy, safety, or confidentiality in these settings. 
</p>
<br/>
<h2>How We Protect Your Personal Data</h2>
<br/>
<p>
Kaneva complies with applicable laws and industry standards when transferring, 
receiving and storing visitor and member data. Access to your personal information 
is limited to those Kaneva employees who require the information to provide products 
or services to you or perform their jobs. 
</p>
<br/>
<h2>Only Those 13 or Older May Register</h2>
<br/>
<p>
Kaneva does not collect information from children under the age of 13 because only those individuals 
who are 13 years of age or older are permitted to register.  Registration for our website and/or 
Virtual World of Kaneva service requires entry of a valid and eligible Date of Birth. 
</p>
<br/>
<h2>Your Consent</h2>
<br/>
<p>
Periodically we may need to update our privacy policy to reflect changes in the types 
of information we collect, the means we use to collect information, and/or our usage 
of collected information. When this occurs, we will make reasonable efforts to alert 
you to these changes. Your use of the Kaneva websites and/or any Kaneva products or 
services noted above signifies your assent to this Kaneva Privacy Policy. 
</p>
<br/>
<h2>How to Contact Us</h2>
<br/>
<p>
Kaneva welcomes your questions and comments relating to privacy. Please direct any 
communications at http://www.kaneva.com/overview/contactUs.aspx. 
</p>
<br/>
<p>You may also contact Kaneva through US Postal Mail at:</p>
<br/>
<p>Kaneva, Inc.<br/>
270 Carpenter Drive<br/>
Sandy Springs, Georgia 30328
</p>
<br/>
<p>Last updated: March 9, 2015</p>

															</div>
														</td>
													</tr>
												</table>
											</td>
											
										</tr>
									</table>
									<span class="cb"><span class="cl"></span></span>
								</div>
								</td>
								
									</td>
							</tr>
						</table>
					</td>
					<td class=""><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img3"/></td>
				</tr>
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
				<tr>
					<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" id="Img4"/></td>
				</tr>
			</table>
		</td>
	</tr>
</table>

