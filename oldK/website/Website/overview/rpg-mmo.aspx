<%@ Page language="c#" Codebehind="rpg-mmo.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.overview.rpg_mmo" %>

<link href="../css/friends.css" rel="stylesheet" type="text/css"/>
<link href="../css/new.css" rel="stylesheet" type="text/css" />		

<table border="0" cellspacing="0" cellpadding="0"  width="990" align="center">
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" id="Img1"/></td>
	</tr>
	<tr>
		<td>
			<table  border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="frTopLeft"></td>
					<td class="frTop"></td>
					<td class="frTopRight"></td>
				</tr>
				<tr>
					<td class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img2"/></td>
					<td valign="top" class="frBgIntMembers">
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
								
									<!-- TOP STATUS BAR -->
									<div id="pageheader">
										<table cellpadding="0" cellspacing="0" border="0" width="99%">
											<tr>
												<td nowrap align="left">
													<table cellpadding="0" cellspacing="0" border="0">
														<tr>
															<td><h1>Create Your Own MMO</h1></td>
														</tr>
													</table>
												</td>
												
											</tr>
										</table>
									</div>
									<!-- END TOP STATUS BAR -->	
								
								</td>
							</tr>
							<tr>
								<td width="968" align="left"  valign="top">
									<div class="module whitebg">
									<span class="ct"><span class="cl"></span></span>
									<table cellpadding="0" cellspacing="0" border="0"  width="98%">
										<tr>
											
											<td width="230" valign="top">
												<br>
												<table border="0" cellspacing="0" cellpadding="0" width="190">
													<tr>
														<td id="pagenav">
															<ul>
																<li>
																	<a id="A1" runat="server" href="~/overview/aboutLanding.aspx">About Kaneva</a> 
																</li>
																<li>
																	<a id="A2" runat="server" href="~/overview/3-d-world.aspx">Online 3D World</a> 
																</li>
																<li>
																	<a id="A3" runat="server" href="~/overview/virtual-life.aspx">Your Virtual Life</a> 
																</li>
																<li>
																	<a id="A4" runat="server" href="~/overview/avatar.aspx">Create an Avatar</a> 
																</li>
																<li>
																	<a id="A5" runat="server" href="~/overview/virtual-games.aspx">Virtual Reality</a> 
																</li>
																<li>
																	<a id="A6" class="selected" runat="server" href="~/overview/rpg-mmo.aspx">Free MMO Game</a> 
																</li>																
																<li>
																	<a id="A7" runat="server" href="~/overview/artists.aspx">Artist Network</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/online-community.aspx" id="A10">Online Community Features</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/social-networking.aspx" id="A11">Social Networking Features</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/media-sharing.aspx" id="A12">Media Sharing Features</a> 
																</li>																
															</ul>
														</td>
													</tr>																														
												</table>
												
											</td>
											<td valign="top" align="center" width="710">
												
												<table border="0" cellspacing="0" cellpadding="0" width="98%">
													<tr>
														<td>
															<div style="text-align:justify;padding-right:10px;">
<br />
<a href="../default.aspx">Home</a> > Create Your Own MMO<br />															
<br/>
<h1 style="padding-left:0px;">Introducing Kaneva�a Free MMO like No Other</h1>
<p>
Experience Kaneva for yourself, it�s the ultimate free MMO. A virtual world where you, your media, 
and your friends come to life. Unlike most free online mmo games, with Kaneva, you live in a modern-day 
virtual world full of exciting people, places, and entertainment. It�s a whole new way to connect with 
others�a world full of real friends and good times. Jump in and play the mmog like no other.
</p>
<br/>
<p>
A free mmo, with Kaneva, you can create your own unique avatar, get an exclusive, 
FREE Kaneva City apartment, explore and meet friends in rockin' 3D hangouts, AND join 
the ultimate 3D dance game.
</p>
<br/>

<h2>Want to Create Your Own MMO?</h2>

<p>
Do you have what it takes to help create the next generation of MMO game and 3D virtual world content? 
Are you a game developer looking to take advantage of the opportunities in the new era of online gaming 
and social media? The Kaneva Game Platform is designed for end-to-end MMO game (MMOG) development for 
FPS and RPG genres. You can join the Kaneva Elite Developers group and learn how to create your own 
free 3D online MMOG. 
</p>
<br/>
<p>
<a runat="server" id="joinLink">Join Kaneva and experience the ultimate mmog</a>
</p>											
<br/>
															</div>														
														</td>
													</tr>
												</table>
											</td>
											
										</tr>
									</table>
									<span class="cb"><span class="cl"></span></span>
								</div>
								</td>
								
									</td>
							</tr>
						</table>
					</td>
					<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img3"/></td>
				</tr>
				<tr>
					<td class="frBottomLeft"></td>
					<td class="frBottom"></td>
					<td class="frBottomRight"></td>
				</tr>
				<tr>
					<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" id="Img4"/></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
