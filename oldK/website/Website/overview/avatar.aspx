<%@ Page language="c#" Codebehind="avatar.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.overview.avatar" %>

<link href="../css/friends.css" rel="stylesheet" type="text/css"/>
<link href="../css/new.css" rel="stylesheet" type="text/css" />		

<table border="0" cellspacing="0" cellpadding="0"  width="990" align="center">
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" id="Img1"/></td>
	</tr>
	<tr>
		<td>
			<table  border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="frTopLeft"></td>
					<td class="frTop"></td>
					<td class="frTopRight"></td>
				</tr>
				<tr>
					<td class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img2"/></td>
					<td valign="top" class="frBgIntMembers">
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
								
									<!-- TOP STATUS BAR -->
									<div id="pageheader">
										<table cellpadding="0" cellspacing="0" border="0" width="99%">
											<tr>
												<td nowrap align="left">
													<table cellpadding="0" cellspacing="0" border="0">
														<tr>
															<td><h1>Create an Avatar</h1></td>
														</tr>
													</table>
												</td>
												
											</tr>
										</table>
									</div>
									<!-- END TOP STATUS BAR -->	
								
								</td>
							</tr>
							<tr>
								<td width="968" align="left"  valign="top">
									<div class="module whitebg">
									<span class="ct"><span class="cl"></span></span>
									<table cellpadding="0" cellspacing="0" border="0"  width="98%">
										<tr>
											
											<td width="230" valign="top">
												<br>
												<table border="0" cellspacing="0" cellpadding="0" width="190">
													<tr>
														<td id="pagenav">
															<ul>
																<li>
																	<a id="A1" runat="server" href="~/overview/aboutLanding.aspx">About Kaneva</a> 
																</li>
																<li>
																	<a id="A2" runat="server" href="~/overview/3-d-world.aspx">Online 3D World</a> 
																</li>
																<li>
																	<a id="A3" runat="server" href="~/overview/virtual-life.aspx">Your Virtual Life</a> 
																</li>
																<li>
																	<a id="A4" class="selected" runat="server" href="~/overview/avatar.aspx">Create an Avatar</a> 
																</li>
																<li>
																	<a id="A5" runat="server" href="~/overview/virtual-games.aspx">Virtual Reality</a> 
																</li>
																<li>
																	<a id="A6" runat="server" href="~/overview/rpg-mmo.aspx">Free MMO Game</a> 
																</li>																
																<li>
																	<a id="A7" runat="server" href="~/overview/artists.aspx">Artist Network</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/online-community.aspx" id="A10">Online Community Features</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/social-networking.aspx" id="A11">Social Networking Features</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/media-sharing.aspx" id="A12">Media Sharing Features</a> 
																</li>																
															</ul>
														</td>
													</tr>																														
												</table>
												
											</td>
											<td valign="top" align="center" width="710">
												
												<table border="0" cellspacing="0" cellpadding="0" width="98%">
													<tr>
														<td>
															<div style="text-align:justify;padding-right:10px;">
<br />
<a href="../default.aspx">Home</a> > Create an Avatar<br />															
<br/>
<h1 style="padding-left:0px;">Create an Avatar in the Virtual World of Kaneva</h1>
<p>
You know you'll look good in 3D, so what are you waiting for? Using our free avatar creator, you can create 
your very own cool avatar. Make your avatar as unique and stylish as you are, and shop for the latest 3D clothes and accessories in our free virtual world. 
</p>
<br/>
<p>
Then use your avatar to explore the Virtual World of Kaneva and interact in 3D. Full of modern day people and entertainment, 
being in the Virtual World is like hanging out and going places with your friends in the real world. If you've heard about 
virtual avatar worlds before, now is your chance to experience what the buzz is about. 
</p>
<br/>

<h2>Discover the new you (in 3D) with our free avatar creator</h2>

<p>
With our free avatar creator, you can choose from over 55 million possible combinations, including body type, hairstyle, 
facial features, and skin color.  Tall, short, or curvy? You decide. Hair color and cut? It's your call.  It's an avatar 
game for Kaneva's virtual world. Use our free virtual avatar maker to create yourself in 3D. Then share your cool new Avatar with friends.
</p>
<br/>
<p>
<a runat="server" id="joinLink">Use our Free Virtual Avatar Maker to Create Yourself in 3D</a>
</p>											
<br/>
															</div>														
														</td>
													</tr>
												</table>
											</td>
											
										</tr>
									</table>
									<span class="cb"><span class="cl"></span></span>
								</div>
								</td>
								
									</td>
							</tr>
						</table>
					</td>
					<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img3"/></td>
				</tr>
				<tr>
					<td class="frBottomLeft"></td>
					<td class="frBottom"></td>
					<td class="frBottomRight"></td>
				</tr>
				<tr>
					<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" id="Img4"/></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
