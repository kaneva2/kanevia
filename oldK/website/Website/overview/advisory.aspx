<%@ Page language="c#" Codebehind="advisory.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.advisory" %>
<center>
<table cellpadding="0" cellspacing="0" border="0" width="710">
<tr>
	<td align="left"><a runat="server" href="~/free-virtual-world.kaneva" ID="A4"><img border="0" style="vertical-align:middle;" runat="server" src="~/images/Kaneva_logo_150x50.jpg" ID="Img4"/></a> <span style="font-family:arial; font-size: 27px; font-weight:bold;">Corporate Information</span></td>
</tr>
<tr>
	<td align="left"><span style="font-size:6px;"><br><br></span><span style="font-size:6px;"><br><br></span>
	</td>
</tr>
<tr>
	<td>
	<span class="orangeHeader3">Advisory Board</span>
	</td>
</tr>
<tr>
<td valign="top" class="bodyText">


<BR><BR><B>Brian Hook</B> <BR>Founder / President <BR>Pyrogon 
<BR><BR>Brian is a well known online gaming professional and has been credited 
with several blockbuster games. He was the lead programmer and technology 
architect for Everquest 2 massively multiplayer on-line role-playing game, which 
has been the benchmark for all other MMO games to date. Previously Brian was one 
of three programmers that worked on the hit games Quake 2 and Quake 3. 
<BR><BR>Brian has consulted to several video card and high-end graphics 
workstations manufacturers including nVidia, Trident and Silicon Graphics. He 
also developed the highly acclaimed Glide API for 3Dfx Voodoo processor. 
<BR><BR>In addition to game development, Brian has had Articles published in 
numerous magazines including PC Techniques, Dr. Dobb's Journal and Game 
Developer Magazine. Author of "Building a 3D Game Engine in C++" published by J. 
Wiley &amp; Sons (1996). <BR><BR><BR><BR><B>Josephine 
Leong</B><BR>Chair<BR>Interactive Design and Game Development <BR>School of Film 
and Digital Media <BR>Savannah College of Art and Design <BR><BR>Professor 
Josephine Leong is the Chair of the Interactive Design and Game Development at 
the Savannah College of Art and Design. Professor Leong holds a Master of 
Science (Intelligent Knowledge Based Systems) degree from the University of 
Essex, UK. <BR><BR>A native of Singapore, she moved to the US in 2001. Professor 
Leong has over 15 years of experience in the industry. She has taught at 
institutes of higher education, in Singapore, Australia and the US. Over the 
years she has managed a multimedia start-up, consulted for and managed projects 
for numerous companies and government statutory boards in Singapore and in 
Japan. Her areas of expertise include computer programming, systems design, 
local area networking, artificial intelligence, smart-card applications, 
electronic publishing, Internet and multimedia technologies and interactive 
entertainment. <BR><BR><BR><BR><B>Aram N. Cookson </B><BR>Interactive Design and 
Game Development <BR>School of Film and Digital Media <BR>Savannah College of 
Art and Design <BR><BR>Professor Aram Cookson has been teaching at the Savannah 
College of Art and Design for five years. Professor Cookson is one of the 
original faculty members of the Game Development major playing a leading role in 
curriculum development including environment and level design, character design, 
and game criticism. <BR><BR>Professor Cookson has a BFA in Visual Arts from 
Boston University School of Fine Arts and his MFA in Digital Arts from the 
Savannah College of Art and Design. He is a member of the Interactive Game 
Developers Association (IGDA) and is currently focused on the continued 
development of curriculum integrating narrative and game mechanics. 
<BR><BR><BR><BR><B>Heath Thompson</B><BR>Vice President of 
Engineering<BR>Internet Security Systems (NASDAQ:ISSX) <BR><BR>ISS is the 
world's leading independent security company. At ISS, Thompson is responsible 
for the delivery of all ISS products, including Proventia appliances, 
SiteProtector management, and RealSecure software products. Thompson manages the 
worldwide ISS engineering team, and since coming on board at ISS in 2003, has 
helped launch ISS' appliance initiatives and bring innovative new products to 
market, such as the first true inline intrusion prevention system from ISS, and 
best-in-class content filtering and anti-spam technology. Thompson has driven 
product quality levels to new highs, and has significantly streamlined 
engineering operations, delivering 50% more products than in previous years 
while simultaneously reducing costs by over 15%. <BR><BR>Before joining ISS, 
Thompson was President and Executive Director of the Thoughtmill Division of 
American Systems Corporation (ASC). As President of the Thoughtmill Division, 
Thompson is responsible for the delivery of high-quality, commercial strength 
custom software solutions to the customers of American Systems, and crafting the 
software services strategy for ASC. <BR><BR>Prior to his current role with ASC, 
Thompson founded Thoughtmill Corporation in 1996, and led Thoughtmill as CEO 
until its successful merger with American Systems in 2002. Thompson's 
commitment, and now ASC's as well, is for Thoughtmill to be a trusted partner to 
its clients, built upon the company's founding core values of integrity, 
quality, innovation, and business urgency. Over the past 7 years, Thompson and 
Thoughtmill have delivered on this commitment to companies such as AFLAC, TSYS, 
Global Payments, T-Mobile, Internet Security Systems, DoubleClick, IBM, 
Scientific-Atlanta, NAPA/Genuine Parts, and Six Continents Hotels. <BR><BR>Prior 
to founding Thoughtmill, Thompson was Director of Engineering with Attachmate 
Corporation, responsible for the development of several commercial products, 
including OpenMind, an enterprise collaboration solution that twice won Network 
Computing's Groupware Product of the Year. In addition, Thompson was 
Attachmate's Internet Architect, and helped determine the company's early 
Internet strategy in the mid-1990's. <BR><BR>Thompson has over 25 years 
experience in the software development industry, with both software products and 
consulting companies. Thompson has held numerous senior architect and project 
management positions throughout his career. He has a deep background in system 
architecture, application development, software process, and usability. 
<BR><BR>Outside of his duties at Thoughtmill/ASC, Thompson is active in the 
technology community. He is a member of the Georgia Tech College of Computing 
Alumni Advisory Board, and has served on a technical advisory council to former 
U.S. Senator Max Cleland. Thompson also serves on the advisory boards of many 
technology companies, including Klaus Entertainment. <BR>


</td>
</tr>
</table>
</center>
<br><br><br><br>

