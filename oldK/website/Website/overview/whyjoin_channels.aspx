<%@ Page language="c#" Codebehind="whyjoin_channels.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.overview.whyjoin_channels" %>
<asp:placeholder id="phBreadCrumb" runat="server"/>
 <style>
.menu1
{
	font-family: Verdana;
	font-size:15px;
	color:#646464;
	font-weight:bold;
	text-align:center;	
} 
.menu1  a
{
	font-family: Verdana;
	font-size:12px;
	color:#646464;
	font-weight:bold;
	text-align:center;	
}
.menu1 a:visited
{
	font-family: Verdana;
	font-size:12px;
	color:#646464;
	font-weight:bold;
	text-align:center;	
}
.menu1 a:hover
{
	font-family: Verdana;
	font-size:12px;
	color:#FFFFFF;
}
.menu1 div
{
	width:140px;
	padding: 4px 0px;
}
.content a
{
	font-family: Verdana;
	font-size:11px;
	color:#767661;
	text-decoration:underline;
} 
.content a:visited
{
	font-family: Verdana;
	font-size:11px;
	color:#767661;
	text-decoration:underline;
} 
.content a:hover
{
	font-family: Verdana;
	font-size:11px;
	color:#FFFFFF;
	text-decoration:underline;
} 
 .link1 a
{
	font-family: Verdana;
	font-size:14px;
	color:#767661;
	text-decoration:underline;
} 
.link1 a:visited
{
	font-family: Verdana;
	font-size:14px;
	color:#767661;
	text-decoration:underline;
} 
.link1 a:hover
{
	font-family: Verdana;
	font-size:14px;
	color:#FFFFFF;
	text-decoration:underline;
} 

</style>

<br/>

<table cellpadding="0" cellspacing="0" border="0" width="990" height="500" style="background-image:url(../images/content/create_channels.jpg);">
	<tr>
		<td width="140" valign="top" class="menu1" align="left" >
			<div style="padding-top:15px;"><a href="whyjoin.aspx">Explore</a></div>
			
			<div style="padding-top:12px;"><a href="whyjoin_people.aspx">Express</a></div>

			<div style="padding-top:12px;"><a href="whyjoin_channels.aspx">Start Community</a></div>
		</td>
		
		<td width="430" style="padding-left:16px;padding-right:10px;" valign="top">

			<div class="header">Start a Community</div>

			<div class="content">
			<span class="subheader">Are you a professional producer, content owner or an aspiring one?</span> 
			</div>

			<div class="content">
			Create your own easily customizable Community � a series of feature-rich, multi-media web pages 
			to showcase your content online and in our Virtual World. Whether your Community is all about outdoor 
			adventures, cooking or somewhere in between, Kaneva is your �canvas� to showcase your creative work 
			and passions and rapidly build a global audience.
			</div>

			<div class="content">
			Upload unlimited media � video, photos, music and more -- enjoy creative freedom and maintain complete 
			content control and ownership. Completely customize the look of your Community using our easy to use tools and themes. 
			</div>

			<div class="content">
			Use Kaneva�s Connected Media Module to showcase and share your media anywhere on the Net and in the Virtual 
			World of Kaneva. Use our built-in social networking functionality to share your Community with the world.
			</div>

			<div class="link1" style="padding-top:7px">
			<a href="~/overview/learnmore_producer.aspx" runat="server" id="A1">Learn More</a>
			</div>
		</td>

		<td class="bodyText" style="padding:10px;line-height:14px">
		</td>
	</tr>
</table>
