<%@ Page language="c#" Codebehind="learnMore_channels.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.overview.learnMore_channels" %>

<style type="text/css">

p {font-size:12px;font-weight:bold;line-height:15px}
div {font-size:12px;font-weight:bold;line-height:15px}
li   {font-size:12px;font-weight:bold;line-height:15px}
</style>

<center>
<br/>
<table cellpadding="0" cellspacing="0" class="container" border="0">
	<tr>
		<td width="45%" class="mktText" style="padding:10px;" valign="top">

		<div style="font-size:16pt;font-weight:bold;">ABOUT COMMUNITIES</div>
		
		<div style="padding-top:20px">
			While <a href="learnmore_people.aspx">Kaneva Profiles</a> are all about you, Kaneva�s Communities revolve around topics of 
			interest, hobbies and passions, featuring a wide variety of pre-professional and professional 
			<a href="learnmore_media.aspx">Media</a>. Join a Community with a topic that interests you. Or, if you have content 
			to share or a story to tell, it�s easy to become a Kaneva Producer and create your own.
		</div>
		
		<p>
			Kaneva�s Communities feature:
			<ul>
			<li>Unlimited media: videos, photos, music/audio, blogs and more</li>
			<br/><br/>
			<li>Complete creative freedom with customizable with drag and drop widgets</li>
			<br/><br/>
			<li>Non-exclusivity, maintain total ownership of your content</li>
			<br/><br/>
			<li>Built-in social networking platform for viral sharing, extensive friend to friend distribution, measurement and reporting</li>
			<br/><br/>
			<li>Additional interactive functionality including, membership, search, tag and rave</li>
			<br/><br/>
			<li>Includes <a href="learnmore_cmp.aspx">Connected Media Module</a> to play and publish media in your Community or anywhere on the net</li>
			</ul>
		</p>
		
		<br/>
		<p><a runat="server" id="joinLink">Get started now >>></a></p>

		<p><a href="~/overview/learnmore_producer.aspx" runat="server" id="A2">Become a Kaneva Producer >>></a></p>

		<p><a href="~/overview/whyjoin.aspx" runat="server" id="A3">Why Join >>></a></p>

		</td>

		<td width="5%"></td>
		
		<td width="45%" class="bodyText" style="padding:10px;" valign="top">
				
		<img src="../images/content/broad_band_channels.jpg" border="0" />
		
		</td>
	</tr>
</table>
<br/>
</center>