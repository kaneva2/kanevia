<%@ Page language="c#" Codebehind="legal.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.legal" %>

<link href="../css/friends.css" rel="stylesheet" type="text/css"/>
<link href="../css/new.css" rel="stylesheet" = type="text/css"/>

<table cellpadding="0" cellspacing="0" border="0" width="990" align="center">
	<tr>
		<td colspan="3"><img runat="server" src="~/images/spacer.gif" width="1" height="14" /></td>
	</tr>
	<tr colspan="3">
		<td align="center">
			<table  border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="frTopLeft"></td>
					<td class="frTop"></td>
					<td class="frTopRight"></td>
				</tr>
				<tr>
					<td bgcolor="#f1f1f2" class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
					<td valign="top" bgcolor="#f1f1f2" class="frBgIntMembers" align="center">

<table border="0" cellpadding="15" cellspacing="0" width="700">
<tr>
	<td class="mktText" style="horizontal-align:left">

<p class="mktHeader">Policies</p>

<p style="padding-bottom:10px">
Kaneva, Inc. is strongly committed to protecting the privacy of consumers, and respecting the 
rights of content owners.   Please read the following to learn more about our legal policies and terms of use.
</p>

<p class="mktSubHeader">Privacy</p>
<p>
Kaneva provides a safe and secure environment for consumers by enforcing a strict policy of customer 
privacy. <a href="~/overview/privacy.aspx" runat="server">Read More >></a>
</p>

<p class="mktSubHeader">Copyright</p>
<p>
Kaneva members remain the original copyright owners of all material provided on the community. 
Use of the material in a manner which is inconsistent with our terms and conditions is strictly prohibited.  
<a href="~/overview/copyright.aspx" runat="server" id="A1">Read More >></a>
</p>

<p class="mktSubHeader">Terms & Conditions</p>
<p>
Kaneva provides its service to you subject to the following Terms of Service.   <a href="~/overview/TermsAndConditions.aspx" runat="server" id="A2">Read More >></a>
</p>

	</td>
</tr>
</table>

					</td>
					<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
				</tr>
				<tr>
					<td class="frBottomLeft"></td>
					<td class="frBottom"></td>
					<td class="frBottomRight"></td>
				</tr>
			</table>				
		</td>
	</tr>
</table>


	  