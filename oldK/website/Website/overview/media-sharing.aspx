<%@ Page language="c#" Codebehind="media-sharing.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.overview.media_sharing" %>

<link href="../css/friends.css" rel="stylesheet" type="text/css"/>
<link href="../css/new.css" rel="stylesheet" type="text/css" />		

<table border="0" cellspacing="0" cellpadding="0"  width="990" align="center">
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" id="Img1"/></td>
	</tr>
	<tr>
		<td>
			<table  border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="frTopLeft"></td>
					<td class="frTop"></td>
					<td class="frTopRight"></td>
				</tr>
				<tr>
					<td class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img2"/></td>
					<td valign="top" class="frBgIntMembers">
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
								
									<!-- TOP STATUS BAR -->
									<div id="pageheader">
										<table cellpadding="0" cellspacing="0" border="0" width="99%">
											<tr>
												<td nowrap align="left">
													<table cellpadding="0" cellspacing="0" border="0">
														<tr>
															<td><h1>Media Sharing Features</h1></td>
														</tr>
													</table>
												</td>
												
											</tr>
										</table>
									</div>
									<!-- END TOP STATUS BAR -->	
								
								</td>
							</tr>
							<tr>
								<td width="968" align="left"  valign="top">
									<div class="module whitebg">
									<span class="ct"><span class="cl"></span></span>
									<table cellpadding="0" cellspacing="0" border="0"  width="98%">
										<tr>
											
											<td width="230" valign="top">
												<br>
												<table border="0" cellspacing="0" cellpadding="0" width="190">
													<tr>
														<td id="pagenav">
															<ul>
																<li>
																	<a id="A1" runat="server" href="~/overview/aboutLanding.aspx">About Kaneva</a> 
																</li>
																<li>
																	<a id="A2" runat="server" href="~/overview/3-d-world.aspx">Online 3D World</a> 
																</li>
																<li>
																	<a id="A3" runat="server" href="~/overview/virtual-life.aspx">Your Virtual Life</a> 
																</li>
																<li>
																	<a id="A4" runat="server" href="~/overview/avatar.aspx">Create an Avatar</a> 
																</li>
																<li>
																	<a id="A5" runat="server" href="~/overview/virtual-games.aspx">Virtual Reality</a> 
																</li>
																<li>
																	<a id="A6" runat="server" href="~/overview/rpg-mmo.aspx">Free MMO Game</a> 
																</li>																
																<li>
																	<a id="A7" runat="server" href="~/overview/artists.aspx">Artist Network</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/online-community.aspx" id="A10">Online Community Features</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/social-networking.aspx" id="A11">Social Networking Features</a> 
																</li>
																<li>
																	<a runat="server" class="selected" href="~/overview/media-sharing.aspx" id="A12">Media Sharing Features</a> 
																</li>
															</ul>
														</td>
													</tr>																														
												</table>
												
											</td>
											<td valign="top" align="center" width="710">
												
												<table border="0" cellspacing="0" cellpadding="0" width="98%">
													<tr>
														<td>
															<div style="text-align:justify;padding-right:10px;">
<br />
<a href="../default.aspx">Home</a> > Media Sharing Features<br />	
<br/>
 <h1 style="padding-left:0px;">Kaneva Media Sharing Features Include Free Video Sharing</h1>
<p>
Kaneva offers free video sharing - as well other types of media sharing - to all of its members. Simply upload 
your media - photos, videos, music, and games -- to your Kaneva Profile. Include media that represents you and 
your interests and use them to help share your passions with the world.  You can also easily start and add your 
favorite media to a special interest Community on Kaneva. Kaneva's Open Media Module lets you share your media 
by placing it anywhere on the Net.
</p>
<br/>
<p>
Take media sharing one step further, by showcasing your media in 3D in Kaneva's rapidly emerging Virtual World. 
In the <a href="~/3d-virtual-world/virtual-life.kaneva" runat="server">Virtual World of Kaneva</a>, you get a 3D home to call 
their own. Use your Media Library to feature the media in your Profile in your 3D home -- watch your favorite 
video clips or home movies on your virtual TV and show off your favorite photos in picture frames on the walls. 
</p>
<br/>
<p>
Take advantage of media sharing within Kaneva's Communities by gaining a 3D hang-out in the Virtual World. If 
you start a Kaneva Community, you can use our free video sharing capabilities to create a virtual movie theater 
where people can meet to see you world premiere your latest video clips! You can even throw a virtual party and 
invite everyone you know to mix and mingle.
</p>
<br/>

<h2>We Make Media Sharing Simple</h2>

<p>
With the photo and video sharing capabilities offered by Kaneva, you can express yourself uniquely to anyone 
who looks at your Profile, Community or virtual home. Our Open Media Player lets you display your work anywhere 
on the Net. Musicians and artists can use the media sharing capability to build a following that goes beyond 
local borders to attract a global audience. Kaneva�s free video sharing brings people together around their 
interests both on the web and in a 3D Virtual World. 
</p>
<br/>
<p>
Sign up now, get started building your Profile and take advantage of easy music, game, photo and video sharing! 
</p>
<br/>
<p>
<a id="joinLink" runat="server">Ready to get started using our free video sharing features? Join Kaneva today</a>.
</p>
												
<br/>
															</div>														
														</td>
													</tr>
												</table>
											</td>
											
										</tr>
									</table>
									<span class="cb"><span class="cl"></span></span>
								</div>
								</td>
								
									</td>
							</tr>
						</table>
					</td>
					<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img3"/></td>
				</tr>
				<tr>
					<td class="frBottomLeft"></td>
					<td class="frBottom"></td>
					<td class="frBottomRight"></td>
				</tr>
				<tr>
					<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" id="Img4"/></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
