<%@ Page language="c#" Codebehind="ourValues.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.ourValues" %>

<link href="../css/friends.css" rel="stylesheet" type="text/css"/>

<link href="../css/new.css" rel="stylesheet" type="text/css" />		

<table border="0" cellspacing="0" cellpadding="0"  width="990" align="center">
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" id="Img1"/></td>
	</tr>
	<tr>
		<td>
			<table  border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="frTopLeft"></td>
					<td class="frTop"></td>
					<td class="frTopRight"></td>
				</tr>
				<tr>
					<td class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img2"/></td>
					<td valign="top" class="frBgIntMembers">
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
								
									<!-- TOP STATUS BAR -->
									<div id="pageheader">
										<table cellpadding="0" cellspacing="0" border="0" width="99%">
											<tr>
												<td nowrap align="left">
													<table cellpadding="0" cellspacing="0" border="0">
														<tr>
															<td><h1>Culture & Values</h1></td>
														</tr>
													</table>
												</td>
												
												<td align="right" valign="middle">
													<table cellpadding="0" cellspacing="0" border="0" width="690">
														<tr>
															<td class="headertout" width="175"></td>
															<td class="searchnav" width="260"></td>
															<td align="left" width="130"></td>
															<td class="searchnav" align="right" width="210" nowrap>
																
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</div>
									<!-- END TOP STATUS BAR -->	
								
								</td>
							</tr>
							<tr>
								<td width="968" align="left"  valign="top">
									<div class="module whitebg">
									<span class="ct"><span class="cl"></span></span>
									<table cellpadding="0" cellspacing="0" border="0"  width="98%">
										<tr>
											
											<td width="230" valign="top">
												<br>
												<table border="0" cellspacing="0" cellpadding="0" width="190">
													<tr>
														<td id="pagenav">
															<ul>
																<li>
																	<a runat="server" href="~/overview/aboutLanding.aspx" id="A1">About Kaneva</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/vision.aspx" id="A2">History</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/company.aspx" id="A3">Leadership</a> 
																</li>
																<li>
																	<a class="selected" runat="server" href="~/overview/ourvalues.aspx" id="A4">Culture & Values</a> 
																</li>
																<li>
																	<a runat="server" href="~/share/default.aspx">Share Kaneva</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/advertising.aspx" id="A5">Advertising & Sponsors</a> 
																</li>
																<li>
																	<a runat="server" href="~/community/news.kaneva" id="A6">News / Press Center</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/contactus.aspx" id="A7">Contact Us</a> 
																</li>
																
															</ul>
														</td>
													</tr>																														
												</table>
												
											</td>
											<td valign="top" align="center" width="710">
												
												<table border="0" cellspacing="0" cellpadding="0" width="98%">
													<tr>
														<td>
															<br/>
															
															<div style="text-align:justify;padding-right:10px;">
		<h2>Values</h2>
		
		<p>
		Our company is united by a set of values that let us leverage our individual strengths to achieve our 
		shared vision. These values express who we are and what we believe � the positive spirit that is Kaneva. 
		They are critical to helping us achieve our mission of delivering a global interactive grassroots entertainment 
		network whereby the Kaneva community thrives and prospers. These values also reflect the energy and spirit of 
		a company that has the solid foundation to lead change as our market evolves and grows.
		</p>
		<br/>
		<h2>Team-based</h2>
		
		<p>
		We value each other as integral members of the collective Kaneva team and believe that team-based collaboration 
		produces the best results as well as happy team members. We encourage the exchange of ideas among individuals 
		within our team and believe that true innovation and creativity emerge when people from diverse backgrounds unite. 
		We encourage compassion, cooperation, and collaboration. At Kaneva, we foster an environment of mutual respect where 
		team members can thrive and feel valued.
		</p>
		<br/>
		<h2>Quality and Excellence</h2>
		
		<p>
		At Kaneva, we are committed to constant improvement of our products, services, procedures and ourselves in order to 
		deliver the best to our customers. Quality is something we incorporate throughout all our processes, keeping our 
		customers and partners in mind each step of the way. By focusing on quality, and acting with integrity, we believe 
		true excellence can be achieved and we can earn a high level of trust and confidence among our team members, customers 
		and partners.
		</p>
		<br/>
		<h2>Execution</h2>
		
		<p>
		We insist on giving our best effort in everything we undertake. We are focused on achieving and maintaining a high level 
		of productivity as well as desired results based on measurable goals and objectives and delivering on the expectations we 
		have set with our partners and customers.
		</p>
		<br/>
		<h2>Fun Place</h2>
		
		<p>
		We believe in fostering a corporate culture where working is made fun. From Kaneva movie night to an afternoon at the 
		race track, Kaneva supports excursions, parties and activities that embody the �work hard, play hard� mantra. With mutual 
		respect for each other�s creativity and freedom, we want every Kaneva team member to be passionate about their job and do 
		it the very best they can.
		</p> <br/>
		
														
															</div>
														</td>
													</tr>
												</table>
											</td>
											
										</tr>
									</table>
									<span class="cb"><span class="cl"></span></span>
								</div>
								</td>
								
									</td>
							</tr>
						</table>
					</td>
					<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img3"/></td>
				</tr>
				<tr>
					<td class="frBottomLeft"></td>
					<td class="frBottom"></td>
					<td class="frBottomRight"></td>
				</tr>
				<tr>
					<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" id="Img4"/></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
