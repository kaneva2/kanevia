<%@ Page language="c#" Codebehind="Vision.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.overview.Vision" %>

<link href="../css/friends.css" rel="stylesheet" type="text/css"/>

<link href="../css/new.css" rel="stylesheet" type="text/css" />		

<table border="0" cellspacing="0" cellpadding="0"  width="990" align="center">
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" id="Img1"/></td>
	</tr>
	<tr>
		<td>
			<table  border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="frTopLeft"></td>
					<td class="frTop"></td>
					<td class="frTopRight"></td>
				</tr>
				<tr>
					<td class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img2"/></td>
					<td valign="top" class="frBgIntMembers">
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
								
									<!-- TOP STATUS BAR -->
									<div id="pageheader">
										<table cellpadding="0" cellspacing="0" border="0" width="99%">
											<tr>
												<td nowrap align="left">
													<table cellpadding="0" cellspacing="0" border="0">
														<tr>
															<td><h1>History</h1></td>
														</tr>
													</table>
												</td>
												
												<td align="right" valign="middle">
													<table cellpadding="0" cellspacing="0" border="0" width="690">
														<tr>
															<td class="headertout" width="175"></td>
															<td class="searchnav" width="260"></td>
															<td align="left" width="130"></td>
															<td class="searchnav" align="right" width="210" nowrap>
																
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</div>
									<!-- END TOP STATUS BAR -->	
								
								</td>
							</tr>
							<tr>
								<td width="968" align="left"  valign="top">
									<div class="module whitebg">
									<span class="ct"><span class="cl"></span></span>
									<table cellpadding="0" cellspacing="0" border="0"  width="98%">
										<tr>
											
											<td width="230" valign="top">
												<br>
												<table border="0" cellspacing="0" cellpadding="0" width="190">
													<tr>
														<td id="pagenav">
															<ul>
																<li>
																	<a runat="server" href="~/overview/aboutLanding.aspx">About Kaneva</a> 
																</li>
																<li>
																	<a class="selected" runat="server" href="~/overview/vision.aspx">History</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/company.aspx">Leadership</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/ourvalues.aspx">Culture & Values</a> 
																</li>
																<li>
																	<a runat="server" href="~/share/default.aspx">Share Kaneva</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/advertising.aspx">Advertising & Sponsors</a> 
																</li>
																<li>
																	<a runat="server" href="~/community/news.kaneva">News / Press Center</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/contactus.aspx">Contact Us</a> 
																</li>
																
															</ul>
														</td>
													</tr>																														
												</table>
												
											</td>
											<td valign="top" align="center" width="710">
												
												<table border="0" cellspacing="0" cellpadding="0" width="98%">
													<tr>
														<td>
															<br/>
															
															<div style="text-align:justify;padding-right:10px;">
<h2>Pioneering the virtual world revolution.</h2>

<p>
According to Gartner, over 250 million people will be in virtual worlds by 2011. 
</p>
<br />
<p>
This is the world we live in. At Kaneva, we�re blurring the line between the online and offline world. We�re creating a place where you can live out the 3D you as an extension of the real you. Instead of a virtual, non-realistic world, Kaneva is an extensible world that brings together the best of life and enables online connections and encourages and enhances real friendships.
</p>
<br />
<p>
Kaneva, Latin for �canvas�, is a digital canvas for creativity and entertainment. We continually evolve our world so that members can connect with others, share interests, build communities and 3D spaces, and share media.
</p>
<br />
<p>
Designed for the masses, Kaneva is easy-to-use, fun, and exciting. Jump in and play!
</p>
<br /><br />

<h2>The birth of Kaneva.</h2>

<p>
A few years ago, one of the youngest and brightest Internet pioneers of our time, Christopher Klaus, was at the helm of his wildly successful brainchild, Internet Security Systems, Inc. (ISS). In 1993, long before the Internet became a household name, Chris was one of the first to recognize the need for Internet security. He founded Internet Security Systems and continued to lead the company�s technological vision. In 2006, Chris helped sell the company to IBM at a total value of more than one billion dollars. Throughout his tremendous success in the security field, Chris never let go of his early passions for gaming and entertainment.
</p>
<br />
<p>
Long before Web 2.0 became a buzz word, Chris started thinking of ways to harness the Internet�s power to fuse online communities, gaming, media sharing, social networking and immersive communication in a single forum. At the same time, Chris recognized the power and future of a 3D Internet. He envisioned user-friendly 3D hangouts for people to meet, interact, play games, watch entertainment and have a great time.
</p>
<br />
<p>
Thus, Kaneva was born. Kaneva is the first to integrate social networking, shared media, and collaborative online communities into a modern-day, immersive 3D world for the masses. Kaneva...imagine what you can do, do what you can imagine.
</p>
<br />
<p>
<div style="margin:0 40px 0 40px;font-style:italic;font-weight:bold;">�I believe virtual worlds and MMOGs (Massively multiplayer online games) 
will be the next fully immersive way to engage with each other. We are in the infancy of this exciting new industry and 
are excited about the many opportunities to innovate and bring humanity together in ways that several years ago would 
only have seemed like science fiction.�</div><br />
<div style="margin-left:40px;font-style:normal;">- Christopher Klaus, Kaneva founder and CEO</div>
</p>
<br />
															</div>
														</td>
													</tr>
												</table>
											</td>
											
										</tr>
									</table>
									<span class="cb"><span class="cl"></span></span>
								</div>
								</td>
								
									</td>
							</tr>
						</table>
					</td>
					<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img3"/></td>
				</tr>
				<tr>
					<td class="frBottomLeft"></td>
					<td class="frBottom"></td>
					<td class="frBottomRight"></td>
				</tr>
				<tr>
					<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" id="Img4"/></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
