///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Kaneva.overview
{
	/// <summary>
	/// Summary description for about_kaneva.
	/// </summary>
	public class about_kaneva : MainTemplatePage
	{
		protected System.Web.UI.HtmlControls.HtmlImage Img1;
		protected System.Web.UI.HtmlControls.HtmlImage Img2;
		protected System.Web.UI.HtmlControls.HtmlAnchor A1;
		protected System.Web.UI.HtmlControls.HtmlAnchor A2;
		protected System.Web.UI.HtmlControls.HtmlAnchor A3;
		protected System.Web.UI.HtmlControls.HtmlAnchor A4;
		protected System.Web.UI.HtmlControls.HtmlAnchor A5;
		protected System.Web.UI.HtmlControls.HtmlAnchor A6;
		protected System.Web.UI.HtmlControls.HtmlAnchor A7;
		protected System.Web.UI.HtmlControls.HtmlAnchor A8;
		protected System.Web.UI.HtmlControls.HtmlAnchor A9;
		protected System.Web.UI.HtmlControls.HtmlAnchor A10;
		protected System.Web.UI.HtmlControls.HtmlImage Img3;
		protected System.Web.UI.HtmlControls.HtmlImage Img4;
	
		protected about_kaneva () 
		{
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			// set this page's specific meta tags

			Title = "Learn More about the Kaneva Online Community";
			MetaDataDescription = "<meta name=\"description\" content=\"Kaneva is the canvas where anyone can showcase and share their passions, interests and talents with the world.  \"/>";
			//MetaDataKeywords = "<meta name=\"keywords\" content=\"online community, social network, meet people, friends, networking, games, mmo, movies, 3D, virtual world, photos, video, blog, bands, music, digital, rpg, entertainment, join groups, forums, online social networking, caneva, kaneva, kaneeva, kanneva\" />";
			//this one set to empty set at business request
			MetaDataTitle = null;
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
