<%@ Page language="c#" Codebehind="managedServices.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.managedServices" %>
<center>
<table cellpadding="0" cellspacing="0" border="0" width="710">
<tr>
	<td align="left"><a runat="server" href="~/free-virtual-world.kaneva" ID="A4"><img border="0" style="vertical-align:middle;" runat="server" src="~/images/kaneva_logo_150x50.jpg" ID="Img4"/></a> <span style="font-family:arial; font-size: 27px; font-weight:bold;">Corporate Information</span></td>
</tr>
<tr>
	<td align="left"><span style="font-size:6px;"><br><br></span>
		<span style="font-size:6px;"><br><br></span>
	</td>
</tr>
<tr>
	<td>
	<span class="orangeHeader3">Online Gaming Services</span><br><br>	
	</td>
</tr><tr>
<td class="bodyText" valign="top">
Online Gaming Services offer you comprehensive solutions for real-time gaming management including game server 
monitoring, updating, and 24/7 technical and customer support.

<BR><BR>All systems inside the data center are monitored 24/7/365 by our support engineers 
at our Network Operations Center (NOC). The NOC engineers monitor the status of 
the network, allowing the staff to take immediate action and provide critical 
real-time information to customers. 

<BR><BR>Kaneva has partnered with Rackable Systems and uses Scale Out Series Server technology for hosting 
the Kaneva site, movies and online games. The Rackable Systems servers provide a 
way to scale quickly and efficiently as demand requires providing a dense 
footprint of 92 high-performance low power/heat consumption servers per rack. 

</td></tr>
</table>
</center>