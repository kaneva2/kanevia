///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Kaneva.overview
{
	/// <summary>
	/// Summary description for anime.
	/// </summary>
	public class anime : MainTemplatePage
	{
		protected anime () 
		{
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
            joinLink.HRef = ResolveUrl(KanevaGlobals.JoinLocation);

            // set this page's specific meta tags
			Title = "Share Anime Videos and Watch Anime Online at Kaneva";
			MetaDataDescription = "<meta name=\"description\" content=\"Kaneva offers a way for fans of anime videos to watch free anime online and to connect with others who share the same interests.\" />";
			MetaDataKeywords = "<meta name=\"keywords\" content=\"anime videos, watch anime online, anime online, watch free anime online\" />";
			//this one set to empty set at business request
			MetaDataTitle = null;
		}

        protected HtmlAnchor joinLink;

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
