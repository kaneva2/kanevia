///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for about.
	/// </summary>
	public class about : NoBorderPage
	{
		protected about () 
		{
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
            string status301 = "301 Moved Permanently";
            Response.Status = status301;
            Response.AddHeader ("Location", ResolveUrl("~/overview/aboutLanding.aspx"));
            Response.End ();
            
            
            //setup header nav bar
			//HeaderNav.FirstTab = Header.eFIRST_TAB.TAKE_TOUR;

			// Put user code to initialize the page here
        //    string currentOverview = "";
        //    if (Request ["over"] != null)
        //    {
        //        currentOverview = (string) Request ["over"];
        //    }
        //    else
        //    {
        //        currentOverview = "overview_01.htm";
        //    }


        //litFrame.Text = "<center><iframe width=\"740\" height=\"1260\" src=\"../overview/" + currentOverview + "\" frameborder=\"0\"></iframe></center>";
		}
		
		protected Literal litFrame;

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
