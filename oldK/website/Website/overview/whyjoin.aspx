<%@ Page language="c#" Codebehind="whyjoin.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.overview.whyjoin" %>
<%@ Register TagPrefix="Kaneva" TagName="HeaderText" Src="../usercontrols/HeaderText.ascx" %>

<kaneva:headertext runat="server" text="Learn More About Kaneva" id="Headertext1" visible="false"/>
<asp:placeholder id="phBreadCrumb" runat="server"/>

<style>
.menu1
{
	font-family: Verdana;
	font-size:15px;
	color:#646464;
	font-weight:bold;
	text-align:center;	
} 
.menu1  a
{
	font-family: Verdana;
	font-size:12px;
	color:#646464;
	font-weight:bold;
	text-align:center;	
}
.menu1 a:visited
{
	font-family: Verdana;
	font-size:12px;
	color:#646464;
	font-weight:bold;
	text-align:center;	
}
.menu1 a:hover
{
	font-family: Verdana;
	font-size:12px;
	color:#FFFFFF;
}
.menu1 div
{
	width:140px;
	padding: 4px 0px;
}
.content a
{
	font-family: Verdana;
	font-size:11px;
	color:#767661;
	text-decoration:underline;
} 
.content a:visited
{
	font-family: Verdana;
	font-size:11px;
	color:#767661;
	text-decoration:underline;
} 
.content a:hover
{
	font-family: Verdana;
	font-size:11px;
	color:#FFFFFF;
	text-decoration:underline;
} 
 .link1 a
{
	font-family: Verdana;
	font-size:14px;
	color:#767661;
	text-decoration:underline;
} 
.link1 a:visited
{
	font-family: Verdana;
	font-size:14px;
	color:#767661;
	text-decoration:underline;
} 
.link1 a:hover
{
	font-family: Verdana;
	font-size:14px;
	color:#FFFFFF;
	text-decoration:underline;
} 

</style>
<br/>

<table cellpadding="0" cellspacing="0" border="0" width="990" height="575" style="background-image:url(../images/content/get_connected.jpg);">
	<tr>
		<td width="140" valign="top" class="menu1" align="left" >
			<div style="padding-top:15px;"><a href="whyjoin.aspx">Explore</a></div>
			
			<div style="padding-top:12px;"><a href="whyjoin_people.aspx">Express</a></div>

			<div style="padding-top:12px;"><a href="whyjoin_channels.aspx">Start Community</a></div>
		</td>
		
		<td width="430" style="padding-left:16px;padding-right:10px;" valign="top">

			<div class="header">Explore and Hang out</div>

			<div class="content">
			Explore and discover the creative energy of thousands of multi-media Communities and Profiles 
			on Kaneva either online or...in our Virtual World.  
			</div>

			<div class="content">
			Watch, rate/rave, join and contribute to the Communities that interest you.
			</div>

			<div class="content">
			Browse profiles, meet friends and interact in our online blogs and forums. 
			</div>

			<div class="content">
			You can also go �in world� -- into the 3D Virtual World of Kaneva � and experience a fully modern 
			world filled with entertainment venues where you can hang out with friends, have fun and check 
			out the latest entertainment (movies, videos, music) and people.</div>
			
			<div class="content">
			Upload your videos, photos, music/audio, and personalize 
			the look of your Profile and Communities using cool themes and innovative drag and drop widgets. Add and publish 
			your own blog. Then decide how and with whom you want to share your Profile or Community. You can also browse, 
			watch, rave, comment and contribute to thousands of other Kaneva Communities. You never know what cool 
			media you might discover and who you will meet. 
			</div>


			<div class="link1" style="padding-top:10px;text-align:left">
			<a id="joinLink" runat="server">Get Started</a>
			</div>

		</td>

		<td class="contentRight" style="padding:10px;line-height:14px">
				
			<div style="padding-top:220px;padding-left:70px;">
			<div class="contentRightTitle" style="padding-top:30px">CONNECTED MEDIA MODULE</div>
			<div class="contentRightSubTitle"">The next generation media player.</div>
			 

			<div style="padding-top:16px">
			Scalable, customizable media player for showing photos, videos and audio. 
			Can be easily embedded, �skinned� and sized to fit on any web page.
			</div> 

			<div class="link1" style="text-align:left;padding-left:90px;padding-top:22px">
			<a href="learnmore_cmp.aspx" style="letter-spacing:0px;">Learn More</a> 
			</div> 

			</div>
		</td>
	</tr>
</table>
