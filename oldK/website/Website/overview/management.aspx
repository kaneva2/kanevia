<%@ Page language="c#" Codebehind="management.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.management" %>
<center>
<table cellpadding="0" cellspacing="0" border="0" width="710">
<tr>
	<td align="left"><a runat="server" href="~/free-virtual-world.kaneva" ID="A4"><img border="0" style="vertical-align:middle;" runat="server" src="~/images/Kaneva_logo_150x50.jpg" ID="Img4"/></a> <span style="font-family:arial; font-size: 27px; font-weight:bold;">Corporate Information</span></td>
</tr>
<tr>
	<td align="left"><span style="font-size:6px;"><br><br></span><span style="font-size:6px;"><br><br></span>
	</td>
</tr>
<tr>
	<td>
	<span class="orangeHeader3">Management Team</span>
	</td>
</tr>
<tr>
<td valign="top" class="bodyText">


<BR><BR><B>Christopher W. Klaus</B> - Founder / Chief Executive 
Officer <BR><BR>Mr. Klaus is regarded as one of the world's foremost security 
experts. In 1994, he founded Internet Security Systems (NASDAQ:ISSX) and helped 
grow the company to a preeminent position in the network security industry. In 
1992, he developed ISS' first software program and flagship product, Internet 
Scanner, while attending the Georgia Institute of Technology. <BR><BR>He also 
developed the four-quadrant strategy for focusing on intrusion protection with 
security assessment and intrusion detection for both network and host. Klaus 
provides high-level security consultation to a number of global government 
organizations and Fortune 500 companies. Most recently, he was selected to 
co-chair the Technical Standards and Common Criteria Task Force for the 
Department of Homeland Security National Cyber Security Summit. The task force 
is one of five private sector sponsored task forces to address cyber security 
issues within the President's National Strategy to Secure Cyberspace and will be 
recommending metrics and action for implementation to the Department of Homeland 
Security. <BR><BR><BR><BR><B>Greg Frame </B>- Co-Founder / Chief Gaming Officer 
<BR><BR>With over fifteen years of systems management, design and development 
Mr. Frame brings a wealth of talent and experience to the Klaus Entertainment 
team. Since the infancy of the Internet in the late 80's, Mr. Frame has been 
heavily involved in designing and implementing Internet based distributed 
systems to enterprises including The Limited Stores, Inc., BellSouth Mobility, 
Lockheed Martin and Verizon Wireless. <BR><BR>Previously, Mr. Frame served at 
The Limited Stores, Inc. in Columbus, Ohio where he eventually was placed in 
charge of the Victoria's Secret division's merchandise planning, logistics and 
overseas systems. In the late 80's, Mr. Frame was recruited by Unlimited 
Solutions, Inc., a leading software vendor for the foodservice and retail 
markets. As the Lead of Research and Development, Mr. Frame was instrumental in 
designing and delivering one of the most advanced retailing systems of the time 
while serving a dual role as the primary technical marketing person for all 
sales both in the United States and Europe. <BR><BR>During this time Mr. Frame 
was also a Technical Speaker for the Microsoft Corporation in the TechEd seminar 
series throughout the United States and Europe. In late 1999 Mr. Frame founded 
Indigo Olive Software which is one of the largest mobile application software 
companies in the country processing over $100 million in sales annually. 
<BR><BR><BR><BR><B>Michael Ira Jacobson, CPA</B> - Chief Financial Officer 
<BR><BR>Michael is a seasoned executive with over twenty five years of 
experience and has assisted assisting numerous companies in strategic planning, 
financial procedures, financial statement preparation, business and marketing 
plan creation; and operation analysis and implementation. Michael has held 
senior management positions at Causelink, US Games, Squirrel, Sun America, and 
KPMG Peat Marwick. <BR><BR>While at US Games, Michael grew the company from $8 
million to $28 million in annual sales in three years and reduced manufacturing 
costs by over 35% through reengineering all facets of the business. Previously 
at Squirrel, Michael helped improve customer satisfaction, and reduced average 
service call response times by more than 50%. He managed a financial turn around 
for the service division, from annual losses to profits in excess of $500,000. 
In March 1992, Squirrel was sold to Sulcus Computer Corp. At Sun Life U.S.A, 
Michael was responsible for the supervision and review of SEC filings, and 
management and statutory financial statements for the Sun Life U.S.A. Group a 
financial institution with assets in excess of $10 billion. <BR><BR>Michael 
holds an MBA from Georgia State University and a BSBA from University of North 
Carolina at Chapel Hill. Michael is also a Certified Public Accountant. 
<BR><BR><BR><BR><B>Animesh Saha</B> - Vice President of Engineering 
<BR><BR>Animesh has over seventeen years of experience managing and architecting 
software products. At Klaus Entertainment, Animesh is responsible for the 
Engineering organization that develops the platform and supporting services for 
creating and running massive multi-player online (MMO) games. <BR><BR>Prior to 
joining Klaus Entertainment, Animesh held a wide range of management and senior 
technical positions at Lancope, Thoughtmill, Attachmate and Sybase. At Lancope, 
an Internet security firm, Animesh was the Director of Product Development and 
led the development of the award winning StealthWatch 4.0 product suite. At 
Thoughtmill Corporation, an Engineering Services firm, Animesh served as the 
Chief Architect overseeing full life cycle development of large commercial 
projects for many high technology companies including ISS, DoubleClick, MediaBin 
(formerly Iterated Systems), Engage Technologies, Attachmate, and XcelleNet. At 
Attachmate, Animesh pioneered the development of an Intelligent Internet Proxy 
server. Additionally, he also worked on OpenMind, Attachmate's award-winning 
groupware product. At Sybase, Animesh served as a senior consultant working 
closely with top management of fortune 500 companies. <BR><BR>Animesh holds a 
Masters degree in Computer Science from Clarkson University, New York and a 
Bachelors degree in Electrical and Electronics Engineering from Indian Institute 
of Technology, Madras, India. <BR><BR><BR>

</td>
</tr>
</table>
</center>
<br><br><br><br>