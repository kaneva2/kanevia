<%@ Page language="c#" Codebehind="Piracy.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.Piracy" %>
<br>
<center>
<table cellpadding="0" cellspacing="0" border="0" width="710">
<tr>
	<td align="left"><a runat="server" href="~/free-virtual-world.kaneva" ID="A4"><img border="0" style="vertical-align:middle;" runat="server" src="~/images/kaneva_logo_150x50.jpg" ID="Img4"/></a> <span style="font-family:arial; font-size: 27px; font-weight:bold;">Corporate Information</span></td>
</tr>
<tr>
	<td align="left">
	</td>
</tr>
<tr>
	<td>
	<span class="orangeHeader3">Piracy Prevention</span>	
	</td>
</tr>
<tr>
<td class="bodyText">

<p>Kaneva enforces its intellectual property rights
and the intellectual property rights of our content providers to the fullest
extent of the law (<a runat="server" href="~/overview/copyright.aspx">see copyright policy</a>). Kaneva is committed to working
closely with anti-piracy associations to combat and prevent software and online
digital piracy worldwide.</p>
<p class="orangeHeader3">What are Software and Online Digital Piracy?</p>

<p>
Software piracy is the illegal copying of
software programs. Online digital piracy typically involves distribution of
pirated programs on the Internet. Software and online digital piracy are worldwide
problems -- with estimates in the billions of dollars being lost to piracy
every year. Because software is valuable, and it is easy to create an exact
copy of a program from a single computer, software piracy is widespread. Online
digital piracy is also a major problem with the proliferation of peer-to-peer
networks making it easier than ever to distribute pirated programs online.</p>
<p>
The illegal copying of software programs is a crime, as is the illegal distribution or selling of
these programs. In the United States and many other countries, copyright
law provides for severe civil and criminal penalties for the unauthorized
reproduction or distribution of copyrighted material. Copyrighted material includes,
but is not limited to, computer programs and accompanying sounds, images and
text. </p>

</p>

<p class="orangeHeader3">Kaneva: Legal Online Digital Entertainment
Distribution</p>


<p>At Kaneva, our mission is to provide a
legitimate way to create and sell digital entertainment and foster a safe
environment where both content creators as well as consumers benefit. Although
it is unlikely that piracy will ever be eliminated, we believe that by
combining efforts with the industry�s, educating consumers, and rewarding our customers with
greater value, we can help both combat and prevent piracy. </p>

</p>

<p>Kaneva is specifically helping
combat and prevent piracy by:</p>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal style='mso-list:l0 level1 lfo1;tab-stops:list .5in'><span
     style='font-family:Arial'>providing a legal outlet and new distribution
     system for the buying and selling of digital media <o:p></o:p></span></li>
 <li class=MsoNormal style='mso-list:l0 level1 lfo1;tab-stops:list .5in'><span
     style='font-family:Arial'>dramatically reducing the development and
     distribution costs for game creators and filmmakers<o:p></o:p></span></li>
 <li class=MsoNormal style='mso-list:l0 level1 lfo1;tab-stops:list .5in'><span
     style='font-family:Arial'>providing flexible payment options including
     subscriptions that represent greater value for consumers; and<o:p></o:p></span></li>
 <li class=MsoNormal style='mso-list:l0 level1 lfo1;tab-stops:list .5in'><span
     style='font-family:Arial'>integrating effective security measures into its
     technology and services.<o:p></o:p></span></li>
</ul>


<p class="orangeHeader3">Do Your Part: Report Piracy</p>

<p>
Please join Kaneva in our piracy prevention
efforts. We encourage you to take action and report piracy. For more
information about piracy as well as guidelines on how to report piracy, please
refer to the following web sites for further guidance and information:</span></p>


<p><b>Entertainment Software</b></p>

<p class=MsoNormal><span style='font-family:Arial'><a href="http://www.esa.org/" target="_resource">www.esa.org</a><o:p></o:p></span></p>


<p><b>Business Software
Association (BSA)</b></p>

<p class=MsoNormal><span style='font-family:Arial'><a href="http://www.bsa.org/" target="_resource">www.bsa.org</a><o:p></o:p></span></p>

<p><b>Software and Industry
Information Association (SIIA)</b></p>

<p class=MsoNormal><span style='font-family:Arial'><a
href="http://www.siia.net/piracy/" target="_resource">http://www.siia.net/piracy/</a><o:p></o:p></span></p>



</td>
</tr>
</table>
</center>
<bR><bR><br>
