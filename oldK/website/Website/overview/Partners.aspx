<%@ Page language="c#" Codebehind="Partners.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.Partners" %>


<br>
<center>
<table cellpadding="0" cellspacing="0" border="0" width="710">
<tr>
	<td colspan="3" align="left"><a runat="server" href="~/free-virtual-world.kaneva" ID="A4"><img border="0" style="vertical-align:middle;" runat="server" src="~/images/kaneva_logo_150x50.jpg" ID="Img4"/></a> <span style="font-family:arial; font-size: 27px; font-weight:bold;">Corporate Information</span></td>
</tr>
<tr>
	<td align="left" colspan="3"><span style="font-size:6px;"><br><br></span><span style="font-size:6px;"><br><br></span>
	</td>
</tr>
<tr>
	<td>
	<span class="orangeHeader3">Partners Overview</span>	
	</td>
</tr>
<tr>
<td class="bodyText">
<p>
Long-term, strategic
business partnerships with technology leaders, educational institutions
creative game studios, game publishers and film distributors remain at the
center of our go-to-market strategy and we are gaining excellent support. Many
game creators and filmmakers share our passion for enabling a new way to create
and market games and films. Kaneva is in a unique position to partner with the
industry to deliver solutions that overcome traditional development and
distribution challenges and unlock the opportunities to reaching a worldwide
audience.</p>



<p>

<p>Kaneva offers a
solutions-focused partnering approach designed to expand business
opportunities, accelerate time-to-market, and create competitive differentiation.
<br>
<br>
Discover the benefits of partnering with Kaneva.</p>



<p>Kaneva Partners
Include:</p>

<ul style="line-height:18px;"><b>
 <li><a href="#GSP" class="bodyBold">Game Studio Partners</a></li>
 <li><a href="#GPP" class="bodyBold">Game Publishing Partners</a></li>
 <li><a href="#GDP" class="bodyBold">Game Distribution Partners</a></li>
 <li><a href="#FSP" class="bodyBold">Film Studio Partners</a></li>
 <li><a href="#FDP" class="bodyBold">Film Distribution Partners</a></li>
 <li><a href="#EP" class="bodyBold">Education Partners</a></li>
 <li><a href="#TP" class="bodyBold">Technology Partners</a></li>
 </b>
</ul>

</p>

<p><span class="orangeHeader3" id="#GSP" >Game Studio Partners</span></p>



<p>The core focus
for Kaneva is to dramatically change the business of digital entertainment by
empowering both large and small digital entertainment studios, providing them
with all the tools they need to bring the best products to market faster and with
less expense than ever before.</p>

<p>For game studios, partnering with Kaneva dramatically speeds the development of high end
Massively Multi-Player Online Games (MMOGs) and eases distribution and ongoing
management with comprehensives online game services. Studios maintain control
over their content and gain the opportunity to earn generous royalties.</p>

<p><a href="http://www.klausentertainment.com/partners/form_partner.php" target="_resource">Become a Kaneva Studio Partner</a></p>

<p class="orangeHeader3" id="#GPP">Game Publishing Partners</p> 
<p>
Kaneva is a one stop shop
for MMO game development and comprehensives online game services. Kaneva�s
Online Game Services enable immediate and easy online game publishing without
the burdens of setting up and managing their own costly infrastructures to
support such an effort. Services include: hosting, billing, security, availability and load balancing. Partners earn
significant royalties and the ability to remain non-exclusive. Partnerships with game publishers worldwide are critical to
Kaneva�s success in reaching both national and overseas markets. <br>
<br>
<p><a href="http://www.klausentertainment.com/partners/form_partner.php" target="_resource">Become a Game Publishing Partner</a></p>


<p class="orangeHeader3" id="GDP">Game Distribution Partners</p> 

<p>Kaneva is actively seeking international partners interested in distributing games in
their local countries. Kaneva provides opportunities to localize and bring
multi-genre MMO games developed using Kaneva�s state-of-the-art game development
platform to their local markets.</p>

<p><a href="http://www.klausentertainment.com/partners/form_partner.php" target="_resource">Become a Game Distribution Partner</a></p>


<p class="orangeHeader3" id="FSP">Film Studio Partners</p> 

<p>Kaneva provides
film studios with the ability to immediately and easily publish their films
over the Internet and the opportunity to earn generous royalties while
remaining non-exclusive. Film studios can create their own Channels to feature
their work and connect directly with fans through blogs, forums and more.</p>

<p><a href="http://www.klausentertainment.com/partners/form_partner.php" target="_resource">Become a Kaneva Film Studio Partner</a></p>


<p class="orangeHeader3" id="FDP">Film Distribution Partners</p> 

<p>Kaneva is seeking partnerships
with film distributors to provide a new mechanism for distributing and earning
revenue for their films. Kaneva is giving back filmmakers the power to market
and distribute their own movies.&nbsp; There is a growing audience for fresh,
new media content, and our online venue is closing the gap between filmmakers
and this audience.&nbsp; By creating a channel on Kaneva, filmmakers have a
digital home through which they can market and distribute their movies.&nbsp;
Filmmakers can earn substantial royalties by publishing and selling their works
on Kaneva, while maintaining a close connection with their viewers through a
personal Kaneva blog (add description here)</p>

</p>

<p><a href="http://www.klausentertainment.com/partners/form_partner.php" target="_resource">Become a Kaneva Film Distribution Partner</a></p>


<p class="orangeHeader3" id="EP">Education Partners</p> 

<p>

<p>Kaneva believes that partnering with the best academic institutions for game and film technology
research and art and design promotes the future of the Digital Entertainment
industry by empowering the brightest minds with the best tools while also
giving graduates a solid stream of opportunities. 
</p><p>
If your educational institution that currently includes or is looking to
incorporate a gaming, digital art and design or film curriculum, we would be
interested in speaking with you.
</p>

<p><a href="http://www.klausentertainment.com/partners/form_partner.php" target="_resource">Become a Kaneva Education
Partner</a></p>



<p class="orangeHeader3" id="#TP">Technology Partners </p>
<p>	<OBJECT classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
 codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0"
 WIDTH="203" HEIGHT="111" id="KE_partners" ALIGN="" VIEWASTEXT>
 <PARAM NAME=movie VALUE="KE_partners.swf"/> <PARAM NAME=quality VALUE=high/> <PARAM NAME=bgcolor VALUE=#FFFFFF/> <EMBED src="KE_partners.swf" quality=high bgcolor=#FFFFFF  WIDTH="203" HEIGHT="111" NAME="KE_partners" ALIGN=""
 TYPE="application/x-shockwave-flash" PLUGINSPAGE="http://www.macromedia.com/go/getflashplayer"></EMBED>
</OBJECT></p>
<p>
At Kaneva, we believe technology is at the heart of our mission � to empower game
creators, filmmakers and other digital media providers to create, distribute
and share their work with a worldwide audience. With a long-standing history of
technology partnerships, strategic business partnerships with technology
leaders are critical to Kaneva�s success.</p>

<p><a href="http://www.klausentertainment.com/partners/form_partner.php" target="_resource">Become a Kaneva Technology Partner</a> </p>


</td>
</tr>
</table>
</center><bR><br><br><bR><br><br><bR><br><br><bR><br><br><bR><br><br><bR><br><br><bR><br><br><bR><br><br><bR><br><br>