<%@ Page language="c#" Codebehind="social-networking.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.overview.social_networking" %>

<link href="../css/friends.css" rel="stylesheet" type="text/css"/>
<link href="../css/new.css" rel="stylesheet" type="text/css" />		

<table border="0" cellspacing="0" cellpadding="0"  width="990" align="center">
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" id="Img1"/></td>
	</tr>
	<tr>
		<td>
			<table  border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="frTopLeft"></td>
					<td class="frTop"></td>
					<td class="frTopRight"></td>
				</tr>
				<tr>
					<td class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img2"/></td>
					<td valign="top" class="frBgIntMembers">
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
								
									<!-- TOP STATUS BAR -->
									<div id="pageheader">
										<table cellpadding="0" cellspacing="0" border="0" width="99%">
											<tr>
												<td nowrap align="left">
													<table cellpadding="0" cellspacing="0" border="0">
														<tr>
															<td><h1>Social Networking Features</h1></td>
														</tr>
													</table>
												</td>
												
											</tr>
										</table>
									</div>
									<!-- END TOP STATUS BAR -->	
								
								</td>
							</tr>
							<tr>
								<td width="968" align="left"  valign="top">
									<div class="module whitebg">
									<span class="ct"><span class="cl"></span></span>
									<table cellpadding="0" cellspacing="0" border="0"  width="98%">
										<tr>
											
											<td width="230" valign="top">
												<br>
												<table border="0" cellspacing="0" cellpadding="0" width="190">
													<tr>
														<td id="pagenav">
															<ul>
																<li>
																	<a id="A1" runat="server" href="~/overview/aboutLanding.aspx">About Kaneva</a> 
																</li>
																<li>
																	<a id="A2" runat="server" href="~/overview/3-d-world.aspx">Online 3D World</a> 
																</li>
																<li>
																	<a id="A3" runat="server" href="~/overview/virtual-life.aspx">Your Virtual Life</a> 
																</li>
																<li>
																	<a id="A4" runat="server" href="~/overview/avatar.aspx">Create an Avatar</a> 
																</li>
																<li>
																	<a id="A5" runat="server" href="~/overview/virtual-games.aspx">Virtual Reality</a> 
																</li>
																<li>
																	<a id="A6" runat="server" href="~/overview/rpg-mmo.aspx">Free MMO Game</a> 
																</li>																
																<li>
																	<a id="A7" runat="server" href="~/overview/artists.aspx">Artist Network</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/online-community.aspx" id="A10">Online Community Features</a> 
																</li>
																<li>
																	<a runat="server" class="selected" href="~/overview/social-networking.aspx" id="A11">Social Networking Features</a> 
																</li>
																<li>
																	<a runat="server"  href="~/overview/media-sharing.aspx" id="A12">Media Sharing Features</a> 
																</li>
															</ul>
														</td>
													</tr>																														
												</table>
												
											</td>
											<td valign="top" align="center" width="710">
												
												<table border="0" cellspacing="0" cellpadding="0" width="98%">
													<tr>
														<td>
															<div style="text-align:justify;padding-right:10px;">
<br />
<a href="../default.aspx">Home</a> > Social Networking Features<br />	
<br/>
<h1 style="padding-left:0px;">Kaneva Takes Social Networking, Shared Media And Online Collaborative Community Into 3D</h1>
<p>
Kaneva goes beyond traditional free social networking, shared media and online community functionality. Kaneva sees 
online social networks, media sharing and online community collaboration as integrated features and extends them 
into a 3D Virtual World. Kaneva combines a <a href="~/3d-virtual-world/virtual-life.kaneva" runat="server" id="A9">3D Virtual World</a> 
with free social networking, online collaborative communities and shared media for a totally new entertainment experience.
</p>
<br/>
<p>
When you join Kaneva, you can benefit from built-in, free social networking features to create your own multi-paged, 
customized, web Profile, make friends, grow your friend list, and interact and connect with others. Our shared media 
features enable you to easily upload and share your favorite videos, photos, music and games. You can also easily 
take your favorite media with you to experience in 3D in the Virtual World of Kaneva. Easily set up your Playlist 
and show your favorite videos or home movies on your own Virtual TV. 
</p>
<br/>
<p>
On the web or in-world, you can also join or start your own online Community on Kaneva around a favorite hobby, club, 
passion or talent. Every Kaneva Community gets a 3D hang-out in the Virtual World of Kaneva.
</p>
<br/>
<p>
Experience the next big thing in entertainment when you teleport your Profile, your Media and your Communities into a 
3D Virtual World full of friends, cool places and entertainment.
</p>
<br/>

<h2>First to Tie In Online Communities, Shared Media and Online Social Networking with a 3D Virtual Entertainment World</h2>

<p>
Kaneva takes social networking, shared media and online communities into a 3-D virtual world universe. Each member 
Profile on Kaneva can have an in-world 3D home and <a href="~/community/communitypage.aspx?communityid=1118&pageId=13415" runat="server">customized Avatar</a>, 
and each Community gets a <a href="~/community/communitypage.aspx?communityid=1118&pageId=12394" runat="server">3D hang-out</a>. 
The media in the Community is made available as entertainment in the Virtual World. Friends on the Kaneva web are 
your friends in the Virtual World and all of your media in your Profile will be available in your 3D home - either 
on your virtual TV or as photos on the walls. 
</p>
<br/>
<p>
In our unified 2D web and 3D world, Kaneva introduces the next-generation in online social networking and community 
interaction. Hanging out at Kaneva takes social networking, shared media and online community features into a 3D 
environment, enabling its members to authentically connect with one another in a more human like way. Join Kaneva 
and extend your real life into 3D! 
</p>
<br/>
<p>
<a id="joinLink" runat="server">Sign-up today and use Kaneva�s social networking features to connect with others</a>.
</p>

<br/>
															</div>														
														</td>
													</tr>
												</table>
											</td>
											
										</tr>
									</table>
									<span class="cb"><span class="cl"></span></span>
								</div>
								</td>
								
									</td>
							</tr>
						</table>
					</td>
					<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img3"/></td>
				</tr>
				<tr>
					<td class="frBottomLeft"></td>
					<td class="frBottom"></td>
					<td class="frBottomRight"></td>
				</tr>
				<tr>
					<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" id="Img4"/></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
