<%@ Page language="c#" Codebehind="virtual-world.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.overview.virtual_world" %>

<link href="../css/friends.css" rel="stylesheet" type="text/css"/>

<link href="../css/new.css" rel="stylesheet" type="text/css" />		

<table border="0" cellspacing="0" cellpadding="0"  width="990" align="center">
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" /></td>
	</tr>
	<tr>
		<td>
			<table  border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="frTopLeft"></td>
					<td class="frTop"></td>
					<td class="frTopRight"></td>
				</tr>
				<tr>
					<td class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
					<td valign="top" class="frBgIntMembers">
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
								
									<!-- TOP STATUS BAR -->
									<div id="pageheader">
										<table cellpadding="0" cellspacing="0" border="0" width="99%">
											<tr>
												<td nowrap align="left">
													<table cellpadding="0" cellspacing="0" border="0">
														<tr>
															<td><h1>Our Virtual World</h1></td>
														</tr>
													</table>
												</td>
												
											</tr>
										</table>
									</div>
									<!-- END TOP STATUS BAR -->	
								
								</td>
							</tr>
							<tr>
								<td width="968" align="left"  valign="top">
									<div class="module whitebg">
									<span class="ct"><span class="cl"></span></span>
									<table cellpadding="0" cellspacing="0" border="0"  width="98%">
										<tr>
											
											<td width="230" valign="top">
												<br>
												<table border="0" cellspacing="0" cellpadding="0" width="190">
													<tr>
														<td id="pagenav">
															<ul>
																<li>
																	<a runat="server" href="~/overview/aboutLanding.aspx">About Kaneva</a> 
																</li>
																<li>
																	<a class="selected" runat="server" href="~/overview/virtual-world.aspx">Our Virtual World</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/virtual-life.aspx">Your Virtual Life</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/avatar.aspx">Create Your Avatar</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/online-community.aspx">Online Community Features</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/social-networking.aspx">Social Networking Features</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/media-sharing.aspx">Media Sharing Features</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/artists.aspx">Artist Network</a> 
																</li>
																
															</ul>
														</td>
													</tr>																														
												</table>
												
											</td>
											<td valign="top" align="center" width="710">
												
												<table border="0" cellspacing="0" cellpadding="0" width="98%">
													<tr>
														<td>
															<div style="text-align:justify;padding-right:10px;">

<br/>
<h1 style="padding-left:0px;">Experience Our Virtual World</h1>
<p>															
<a href="~/3d-virtual-world/virtual-life.kaneva" runat="server">The Virtual World of Kaneva</a> is a 3D online virtual world that 
embraces the next generation of the Internet -- three dimensional, more human-like interaction that bring people 
together around their interests.
</p>
<br/>
<p>
Unlike traditional worlds, Kaneva is a modern day, free, 3D virtual world where your Profile, Media and favorite 
online Communities are teleported into a world full of people, places and entertainment. The focal point of your 
3D life is your virtual home and favorite hang-outs, where all the videos and photos you�ve uploaded to Kaneva can 
be seen, heard and shared. 
</p>
<br/>
<h2>Your Avatar and 3D Home</h2>

<p>
We�ve made it easy and simple to create and customize <a href="~/community/communitypage.aspx?communityid=1118&pageId=13415" runat="server">your Avatar</a> 
to look just like you (or your alter ego). Create your <a href="~/community/communitypage.aspx?communityid=1118&pageId=12394" runat="server">3D home</a> 
and decorate it with your unique style. Personalize everything from wallpaper to flooring. Select from a variety 
of furniture styles -- contemporary American, Euro chic, retro. Set up your own virtual Entertainment Center in 
your apartment to play your favorite videos or home movies. Be surrounded by your favorite online photos hanging 
in virtual picture frames on your walls. Want to upgrade? You can also buy an expanding array of premium home d�cor 
items to give your crib that signature style.
</p>
<br/>

<h2>Free Virtual World Fun</h2>

<p>
The friends you make on Kaneva are the same friends you can hang-out with in the Virtual World of Kaneva. Meet up in 
your apartment or any number of <a href="~/community/communitypage.aspx?communityid=1118&pageId=772341" runat="server">3D hang-outs</a> 
and interact through live, 3D chat. You can watch (and share) videos in 3D, play games or go to virtual events together. 
Want to find out more about someone in-world? Just click on their Avatar and you can take a look at their Kaneva 
Profile. Want to hang-out with your real life buddies virtually? Invite them to join Kaneva and connect in-world. 
Invite them for a 3D chat in your apartment or other virtual meeting spot.
</p>
<br/>
<p>
In the Virtual World of Kaneva, you start off in a free virtual world with a set of basics. Want to go beyond and 
make a unique, fashion statement, be the first to show off the latest stuff or buy a virtual gift for someone? 
<a href="http://shop.kaneva.com" runat="server">Go shopping</a>  
in the virtual mall -- and use credits to buy whatever makes you tick or just browse and people watch. It�s up 
to you to completely customize your in-world experience and make it your own.
</p>
<br/>
<p>
<a id="joinLink" runat="server">Join Kaneva and jump into the Virtual World now - it�s free.</a>
</p>
<br/>

															</div>														
														</td>
													</tr>
												</table>
											</td>
											
										</tr>
									</table>
									<span class="cb"><span class="cl"></span></span>
								</div>
								</td>
								
							</tr>
						</table>
					</td>
					<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
				</tr>
				<tr>
					<td class="frBottomLeft"></td>
					<td class="frBottom"></td>
					<td class="frBottomRight"></td>
				</tr>
				<tr>
					<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" /></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
