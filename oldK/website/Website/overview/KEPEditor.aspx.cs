///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for KEPEditor.
	/// </summary>
	public class KEPEditor : MainTemplatePage
	{
		protected KEPEditor () 
		{
			Title = "Game Platform";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			Response.Redirect ("http://elite.kaneva.com");
			Response.End ();

			lblVersion.Text = System.Configuration.ConfigurationManager.AppSettings ["KGPVersion"].ToString ();
		}

		/// <summary>
		/// Click the add button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnDownload_Click (Object sender, ImageClickEventArgs e) 
		{
			if (!Request.IsAuthenticated)
			{
                Response.Redirect(GetLoginURL());
			}
			else
			{
				//string editorFileName = "KanevaGamePlatform_66.exe";
                string editorFileName = System.Configuration.ConfigurationManager.AppSettings["KGPFilename"].ToString();

				string filePath = Server.MapPath("~/download/" + editorFileName);

				if (System.IO.File.Exists (filePath))
				{
                    UsersUtility.InsertUserEditorDownload(GetUserId(), System.Configuration.ConfigurationManager.AppSettings["KGPVersion"].ToString());

					Response.Clear ();
					Response.ContentType = "application/octet-stream";
					Response.AddHeader ("Content-Length", new System.IO.FileInfo (filePath).Length.ToString());
					Response.AddHeader ("Content-Disposition", "attachment; filename=\"" + editorFileName + "\"");
					Response.Flush ();
					Response.TransmitFile (filePath);
					Response.End ();
				}
				else
				{
					ShowErrorOnStartup ("File was not found on server.");
				}
			}
		}

		protected HyperLink hlDownload, hlDownload2;
		protected Label lblVersion;

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
