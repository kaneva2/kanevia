<%@ Page language="c#" Codebehind="anime.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.overview.anime" %>

<link href="../css/friends.css" rel="stylesheet" type="text/css"/>

<link href="../css/new.css" rel="stylesheet" type="text/css" />		

<table border="0" cellspacing="0" cellpadding="0"  width="990" align="center">
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" id="Img1"/></td>
	</tr>
	<tr>
		<td>
			<table  border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="frTopLeft"></td>
					<td class="frTop"></td>
					<td class="frTopRight"></td>
				</tr>
				<tr>
					<td class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img2"/></td>
					<td valign="top" class="frBgIntMembers">
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
								
									<!-- TOP STATUS BAR -->
									<div id="pageheader">
										<table cellpadding="0" cellspacing="0" border="0" width="99%">
											<tr>
												<td nowrap align="left">
													<table cellpadding="0" cellspacing="0" border="0">
														<tr>
															<td><h1>About Kaneva</h1></td>
														</tr>
													</table>
												</td>
												
												<td align="right" valign="middle">
													<table cellpadding="0" cellspacing="0" border="0" width="690">
														<tr>
															<td class="headertout" width="175"></td>
															<td class="searchnav" width="260"></td>
															<td align="left" width="130"></td>
															<td class="searchnav" align="right" width="210" nowrap>
																
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</div>
									<!-- END TOP STATUS BAR -->	
								
								</td>
							</tr>
							<tr>
								<td width="968" align="left"  valign="top">
									<div class="module whitebg">
									<span class="ct"><span class="cl"></span></span>
									<table cellpadding="0" cellspacing="0" border="0"  width="98%">
										<tr>
											
											<td width="230" valign="top">
												<br>
												<table border="0" cellspacing="0" cellpadding="0" width="190">
													<tr>
														<td id="pagenav">
															<ul>
																<li>
																	<a runat="server" href="~/overview/about-kaneva.aspx" id="A1">About Kaneva</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/social-networking.aspx" id="A2">Social Networking</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/online-community.aspx" id="A3">Online Community</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/virtual-world.aspx" id="A4">Virtual World</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/mmorpg.aspx" id="A5">MMORPG</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/virtual-life.aspx" id="A6">Virtual Life</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/avatar.aspx" id="A7">Avatar</a> 
																</li>
																<li>
																	<a class="selected" runat="server" href="~/overview/anime.aspx" id="A8">Anime</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/artists.aspx" id="A9">Artists</a> 
																</li>
																<li>
																	<a runat="server" href="~/overview/media-sharing.aspx" id="A10">Media Sharing</a> 
																</li>
																
															</ul>
														</td>
													</tr>																														
												</table>
												
											</td>
											<td valign="top" align="center" width="710">
												
												
												
												<table border="0" cellspacing="0" cellpadding="0" width="98%">
													<tr>
														<td><br/>
														<h1>Share Anime Videos with Fans from Around the World!</h1>
														<br>
<div style="text-align:justify;padding-right:10px;">
														<p>
Are you a big fan of Anime videos, magazines, and books? Kaneva makes it easy for you to upload and watch anime 
online - in your own profile, in the profiles of other users, or our unique Anime Community. Plus, when you enter 
the Virtual World of Kaneva, you can meet other anime fans in 3D, get together to watch videos in a virtual movie 
theater or in someone's virtual apartment, and discuss your favorite characters and machinima. Build a unique 
network of anime fans - and make new friends at the same time!</p>
<br>
<p>
In October 2006, Kaneva partnered with Central Park Media, one of the leading anime distributors in the United 
States, and held a contest for fans to create trailers based on popular anime videos. The creators of the best 
trailers received all sorts of exciting prizes, and their winning videos will continue to be 
<a href="http://www.centralparkmedia.com/" target="_blank">available for viewing</a> well into the future. </p>
<br>
<p>
Join Kaneva and the Hardcore Anime Community to:<br>
<br>
<ul>
<li>Watch Anime online � including a growing collection of Anime series trailers (nearly 100) </li>
<li>Check out user-generated Anime videos and fan art </li>
<li>Stay up to date with an industry news blog </li>
<li>Take part in online contests and sweepstakes </li>
<li>Read and post on message boards </li>
</ul>
<br>
<br>
<h2>Kaneva Lets You Watch Anime Online for Free � And More!</h2>
<br>
<p>
When you join Kaneva, not only can you watch free Anime online, but you can also read the latest news about 
the world of Anime in shared blogs, chat with other fans in one of our forums, and even find out where people 
are getting together offline to share Anime videos, chat, and hang out. </p>
<br>
<p>
Kaneva is an innovative online social entertainment world where you can share your talents and find people who 
are passionate about the same interests you have. We invite you to watch Anime online while you explore all of 
the other exciting communities, Profiles and Communities that we have to offer.</p>
<br>
<p>
<a runat="server" id="joinLink">Join Kaneva today and start sharing your Anime videos with the world</a>!

														</p>
</div>														
													
														</td>
													</tr>
												</table>
												
												
											</td>
											
										</tr>
									</table>
									<span class="cb"><span class="cl"></span></span>
								</div>
								</td>
								
									</td>
							</tr>
						</table>
					</td>
					<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img3"/></td>
				</tr>
				<tr>
					<td class="frBottomLeft"></td>
					<td class="frBottom"></td>
					<td class="frBottomRight"></td>
				</tr>
				<tr>
					<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" id="Img4"/></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
