///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.overview
{
	/// <summary>
	/// Summary description for guidelines.
	/// </summary>
	public class guidelines : MainTemplatePage
	{
		protected guidelines () 
		{
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			Title = "Kaneva Member Guidelines";
			MetaDataDescription = "<meta name=\"description\" content=\"Kaneva is the only online community where your Profile, Friends, Media and favorite Communities are teleported into a modern-day 3D world where you can explore, socialize and experience entertainment in an entirely new way.\" />";
			//MetaDataKeywords = "<meta name=\"keywords\" content=\"Online virtual world, free virtual world, online virtual worlds\" />";
			//this one set to empty set at business request
			MetaDataTitle = null;

			//setup header nav bar
			HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.NONE;

            aHelp.HRef = Common.GetHelpURL(KanevaWebGlobals.CurrentUser.FirstName, KanevaWebGlobals.CurrentUser.LastName, KanevaWebGlobals.CurrentUser.Email);
		}

        protected HtmlAnchor aHelp;

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
