///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Kaneva.BusinessLayer.BusinessObjects;
using System.Collections.Generic;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for memberhome.
	/// </summary>
	public class defaultHome : BasePage
    {
        protected defaultHome() 
		{
			Title = "Home";
		}

        private void Page_Load (object sender, System.EventArgs e)
        {
            System.Web.HttpCookie aCookie;

            // Is user logged in?
            if (!Request.IsAuthenticated)
            {
				string redirectUrl = "~/free-virtual-world.kaneva";
				if (!String.IsNullOrEmpty(Request.ServerVariables["QUERY_STRING"].ToString()))
				{
					redirectUrl += "?" + Request.ServerVariables["QUERY_STRING"].ToString();
				}
				
                Response.Redirect (redirectUrl);
            }

            // Add prototype
            RegisterJavaScript("jscript/prototype-1.6.1-min.js");

            // If they have been banned, log them out!
            if (Current_User.StatusId.Equals ((int) Constants.eUSER_STATUS.LOCKED) || Current_User.StatusId.Equals ((int) Constants.eUSER_STATUS.LOCKEDVALIDATED))
            {
                for (int i = Request.Cookies.Count - 1; i != 0; i--)
                {
                    aCookie = Request.Cookies[i];
                    aCookie.Expires = DateTime.Now.AddDays (-1);
                    Response.Cookies.Add (aCookie);
                }

                FormsAuthentication.SignOut ();
                Session.Abandon ();
                Response.Redirect ("~/free-virtual-world.kaneva");
            }

            // Set Nav
            ((GenericPageTemplate) Master).HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
            ((GenericPageTemplate) Master).HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.NONE;
            ((GenericPageTemplate) Master).HeaderNav.SetNavVisible (((GenericPageTemplate) Master).HeaderNav.MyKanevaNav);
        }


        #region Declarations

        // private 
        private User Current_User = KanevaWebGlobals.CurrentUser;
        protected usercontrols.ContainerMyKaneva MKcontainerNotifications, MKcontainerPasses, MKcontainerMyTopWorlds;
        public Literal litExperimentCode;

        #endregion Declerations

        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
    }
}
