﻿<%@ Page Language="C#" MasterPageFile="~/masterpages/GenericPageTemplate.Master"  AutoEventWireup="true" CodeBehind="newWorldFame.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.newWorldFame" %>

<asp:Content ID="cntWorldFame" runat="server" contentplaceholderid="cph_Body" >

<style type="text/css">
<!--
@import url("css/worldfame.css");
-->
</style>
<script type="text/javascript">	function OnLoad() { }</script>

<div id="pageContainer">

<img class="banner" src="~/images/worldfame/worldfame_preview_banner_916x151.jpg" runat="server" width="916" height="151" />


<img class="leveling" src="~/images/worldfame/worldfame_preview_illus01_174x275.jpg" runat="server" width="174" height="275" />
<div class="levelingsys">
<h1>The World Fame Leveling System</h1>
<div>When you do certain things in the World of Kaneva, you earn Experience Points (XP). 
	Earning XP increases your World Fame Level which gives you Reward Points to buy things from 
	Shop Kaneva. You’ll also get items, animations, or other bonuses at each level. The more 
	you do, the more you earn!
	<div class="box">
		<h2>Some ways to earn World Fame:</h2>
		<div class="friends">Adding friends</div>
		<div class="travel">Travelling to Communities and Worlds</div>
		<div class="shop">Going shopping</div>
		and more!
	</div>
</div>
</div>
<div class="clearit"></div>
<div id="btmContainer">
	<div class="badgestxt">
		<h1>We've added Badges</h1>
		<div>You can now earn Badges in Kaneva. Collect them all!</div>
	</div>
	<img class="badges" src="~/images/worldfame/worldfame_preview_illus02_491x81.jpg" runat="server" width="491" height="81" />
	<div class="clearit"></div>
	<hr class="line" />
	<div class="unlocktxt">
		<h1>Unlockables</h1>
		<div>With the new World Fame system, you’ll receive new items, animations, or gain access to new features each time you level up!</div>
	</div>
	<img class="unlock" src="~/images/worldfame/worldfame_preview_illus02_491x134.jpg" runat="server" width="491" height="134" />
	<div class="clearit"></div>
	<hr class="line" style="margin-bottom:6px;" />
	<div class="footertxt">Are you a current Kanevan with questions about how the new system is different than 
	the old World Fame? <a href="http://docs.kaneva.com/mediawiki/index.php/Fame_and_Rewards" target="_blank">Click Here</a></div>
</div>

</div>

</asp:Content>