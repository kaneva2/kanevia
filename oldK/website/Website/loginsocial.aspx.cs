///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KlausEnt.KEP.Kaneva
{
    public partial class loginsocial : NoBorderPage
    {
        protected void Page_Load (object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bool matchingEmail = false;

                if (Request["accessToken"] != null)
                {
                    accessToken.Value = Request["accessToken"].ToString ();
                }

                if (Request.QueryString["match"] != null)
                {
                    matchingEmail = true;
                }

                ConfigurePageForDisplay (matchingEmail);

                string loginUrl = KanevaGlobals.SSLLogin ? "https://" : "http://";
                loginUrl += KanevaGlobals.SiteName + "/register/kaneva/registerInfo.aspx";

                lbRegister.PostBackUrl = loginUrl;
            }
        }

        private void ConfigurePageForDisplay (bool match)
        {
            if (match)
            {
                desc.InnerHtml = "<p>We found a Kaneva account registered to the same email address.</p>" +
                 "<p>Please log in with your Kaneva credentials to connect it to your Facebook account.</p>";
            }
            else
            {
                desc.InnerHtml = "<p>You do not yet have a Kaneva account connected to your Facebook account.</p>" +
                  "<p>If you have a Kaneva account, please log in with your Kaneva credentials and connect it to your Facebook account.</p>";
            }
        }
    }
}