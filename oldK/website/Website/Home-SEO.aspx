<%@ Page Language="C#" MasterPageFile="~/masterpages/home.Master" EnableViewState="False" AutoEventWireup="false" CodeBehind="Home-SEO.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.HomeSEO" %>

<asp:Content ID="cnt_seoHome" runat="server" ContentPlaceHolderID="cph_Body" >

 <STYLE type="text/css">
   h1.seo {font-size: 14px; font-family: verdana, arial, sans-serif; color: #3ec3e3; padding: 10px 8px 0 8px; margin: 0; position: relative;line-height: 16px;}
   h2.seo {	font-size: 12px;font-family: verdana, arial, sans-serif;color: #3ec3e3;padding: 0;margin: 0;line-height: 16px;}
 </STYLE>

<script type="text/javascript">
	var flashvars = {};
	var params = {};
	params.quality = "high";
	params.wmode = "transparent";
	params.allowscriptaccess = "always";
	var attributes = {};
	attributes.id = "flashArea";
	swfobject.embedSWF("http://streaming.kaneva.com/media/home_still/flashpocketPlayer.swf", "flash", "974", "425", "9.0.0", "expressInstall.swf", flashvars, params, attributes);
</script>



<div class="content_wrapper">
  <div id="flashArea">
    <div id="flash">
      <div class="getFlash"><a href="http://www.adobe.com/go/getflashplayer"><img src="conv_1-0/images/btn_flash.jpg" alt="Get Flash" /></a></div>
      <div class="startBtn"><a href="register/kaneva/registerInfo.aspx">Get Started</a></div>
    </div>
  </div>
  <div class="subFeature_area">
        <div class="subFeature">
          <h2>Experience</h2>
          <div><img src="conv_1-0/images/img_experiance.jpg" alt="Experiance Kaneva" /></div>
          <p>Kaneva is full of fun and exciting things to do. You can shop, dance, chat, play, and hang out with friends in 3D.</p>
        </div>
        <div class="subFeature">
          <h2>Interact</h2>
          <div><img src="conv_1-0/images/img_interact.jpg" alt="Experiance Kaneva" /></div>
          <p>Chat with people from around the world, attend exclusive online events, and connect with others like you!</p>
        </div>
        <div class="subFeature">
          <h2>Create</h2>
          <div><img src="conv_1-0/images/img_create.jpg" alt="Experiance Kaneva" /></div>
          <p>Decorate your FREE 3D home, design and sell your own stuff, build a community hangout, and share entertainment with your friends!</p>
        </div>
   </div>
   <div class="clear"><!-- clear the floats --></div>
    <div class="seo" id="seo1" runat="server" style="display:none">
        <!-- 3-d-world -->
        <h1 class="seo">Discover Our 3D Virtual World</h1>
	    <p>Kaneva blurs the line between the offline and online world in a 3D virtual world where the virtual you is an extension of the real you. A truly unique online experience, Kaneva brings web profiles and entertainment to life. Join the free 3D virtual world full of real friends and good times</p>	    
	    <br />
	    <h2 class="seo">An online Virtual World to Make Your Own</h2>
	    <p>In our online virtual world, you can invent your 3D avatar that�s as unique and stylish as you are. Decorate your free 3D home with your favorite photos and patterns. And with Kaneva�s 3D virtual world, you can enjoy your favorite videos, photos, and music on your 3D TV. The world�s more fun with friends! Invite yours and meet up in our online virtual world for 3D events, chats, and more!</p>
        <br /><a href="register/kaneva/registerInfo.aspx">Jump in and play! Join Kaneva�s 3-D virtual world.</a>
    </div>
    <div class="seo" id="seo2" runat="server" style="display:none">
       <!-- virtual life -->
       <h1 class="seo">With Kaneva, It�s Never Been Easier to Get a Virtual Life</h1>
	    <p>Create a virtual life in Kaneva�where the 3D you is an extension of the real you. More than a virtual life game, Kaneva�s a modern-day place where you, your media, and your friends come to life. Meet new people, chat, explore, and get the virtual life you�ve been dreaming of.</p>	    
	    <br />
	    <h2 class="seo"> What Are You Waiting for? Create a Virtual Life!</h2>
	    <p>Creating a virtual life in Kaneva opens the door to a world full of exciting people, places, and entertainment. Explore thousands of 3D hangouts and connect with people who share your interests. In your virtual life, you can bring friends to meet up for 3D events, chats, and more! With Kaneva, you can make your 3D avatar as unique and stylish as you are. Decorate your free 3D home with your favorite photos and watch YouTube videos with friends on your TV.</p>
         <br /><a href="register/kaneva/registerInfo.aspx">Get your free virtual life with Kaneva.</a>
   </div>
    <div class="seo" id="seo3" runat="server" style="display:none">
        <!-- avatar -->
        <h1 class="seo">Create an Avatar in the Virtual World of Kaneva</h1>
	    <p>You know you'll look good in 3D, so what are you waiting for? Using our free avatar creator, you can create your very own cool avatar. Make your avatar as unique and stylish as you are, and shop for the latest 3D clothes and accessories in our free virtual world.</p>	    
	    <p>Then use your avatar to explore the Virtual World of Kaneva and interact in 3D. Full of modern day people and entertainment, being in the Virtual World is like hanging out and going places with your friends in the real world. If you've heard about virtual avatar worlds before, now is your chance to experience what the buzz is about.</p>	    
	    <br />
	    <h2 class="seo">Discover the new you (in 3D) with our free avatar creator</h2>
	    <p>With our free avatar creator, you can choose from over 55 million possible combinations, including body type, hairstyle, facial features, and skin color. Tall, short, or curvy? You decide. Hair color and cut? It's your call. It's an avatar game for Kaneva's virtual world. Use our free virtual avatar maker to create yourself in 3D. Then share your cool new Avatar with friends.</p>
         <br /><a href="register/kaneva/registerInfo.aspx">Use our Free Virtual Avatar Maker to Create Yourself in.</a>
    </div>
    <div class="seo" id="seo4" runat="server" style="display:none">
        <!-- virtual-games -->
        <h1 class="seo">Why play virtual reality games when you can live a virtual life?</h1>
	    <p>Experience our free virtual world where you, your media, and your friends come to life. Unlike most virtual people games, with Kaneva, you live in a modern-day virtual world full of exciting people, places, and entertainment. It�s a whole new way to connect with others�a world full of real friends and good times. Jump in and play the virtual reality game like no other.</p>	    
	    <br />
	    <h2 class="seo">More than a virtual online game, Kaneva�s an extension of the real you</h2>
	    <p>Kaneva blurs line between real life and virtual reality games by creating a world where the 3D you is an extension of the real you. A truly unique online experience, Kaneva brings web profiles and entertainment to life in a modern-day virtual world full of real friends and good times.</p>
         <br /><a href="register/kaneva/registerInfo.aspx">Join Kaneva and experience more than a virtual reality game.</a>
    </div>
    <div class="seo" id="seo5" runat="server" style="display:none">
        <!-- rpg-mmo -->
        <h1 class="seo">Introducing Kaneva�a Free MMO like No Other</h1>
	    <p>Experience Kaneva for yourself, it�s the ultimate free MMO. A virtual world where you, your media, and your friends come to life. Unlike most free online mmo games, with Kaneva, you live in a modern-day virtual world full of exciting people, places, and entertainment. It�s a whole new way to connect with others�a world full of real friends and good times. Jump in and play the mmog like no other.</p>	    
	    <p>A free mmo, with Kaneva, you can create your own unique avatar, get an exclusive, FREE Kaneva City apartment, explore and meet friends in rockin' 3D hangouts, AND play the ultimate 3D dance game.</p>	    
	    <br />
	    <h2 class="seo">Want to Create Your Own MMO?</h2>
	    <p>Do you have what it takes to help create the next generation of MMO game and 3D virtual world content? Are you a game developer looking to take advantage of the opportunities in the new era of online gaming and social media? The Kaneva Game Platform is designed for end-to-end MMO game (MMOG) development for FPS and RPG genres. You can join the Kaneva Elite Developers group and learn how to create your own free 3D online MMOG.</p>
        <br /><a href="register/kaneva/registerInfo.aspx">Join Kaneva and experience the ultimate mmog.</a>
   </div>
    <div class="seo" id="seo6" runat="server" style="display:none">
        <!-- 3d-chat-world -->
        <h1 class="seo">Why just chat when you can chat in 3D?</h1>
	    <p>Teleport yourself into a vibrant 3D world where avatars, media and communities come to life. Make friends and meet up for games, 3D chats, and more! Find new friends that are your age, live in your area, and share your interests. Better yet, invite your friends to chat in the 3D world!</p>
	    <ul>
	        <li>Create a 3D avatar that�s as distinct and stylish as you are</li>
	        <li>Get a free, fully customizable 3D home</li>
	        <li>Watch and share your favorite videos, play games, or 3D chat with your friends!</li>
	    </ul>	    
	    <br />
	    <h2 class="seo">A whole new way to connect with friends online - It�s 3D Avatar Chat.</h2>
	    <p>It�s more than a 3D chat room, it�s a vibrant 3D world where friends, communities and entertainment come to life! Create your 3D avatar and shop for the latest fashions, emote, chat, dance, and interact with your friends in 3D!</p>
	    <p>Invite your friends over to hang out, 3D chat, while you watch YouTube videos on your 3D TV. Plus, explore thousands of 3D hangouts and have an avatar chat with people from all over the world!</p>
        <br /><a href="register/kaneva/registerInfo.aspx">Join Kaneva and Invite Your Friends to 3D Chat.</a>
    </div>
    <div class="seo" id="seo7" runat="server" style="display:none">
        <!-- on line community -->
        <h1 class="seo">Introducing Kaneva�the best collection of Online Communities</h1>
	    <p>Kaneva is an online community, incorporating social networking, forums, gaming and a 3D virtual world. A collection of free 3D communities created by its members, with Kaneva, you can create your own unique avatar, get an exclusive, FREE Kaneva City apartment, explore and meet friends in rockin' 3D hangouts made by the community.</p>	    
	    <p>Experience Kaneva for yourself, it�s the ultimate free community builder. A place where you, your community, and your friends come to life inside a 3D world. Unlike most free online communities, with Kaneva, you build a virtual meeting place, hangouts, inside in a modern-day 3D world full of exciting people, places, and entertainment. It�s a whole new way to connect with your online community�Jump in today.</p>	    
	    <br />
	    <h2 class="seo">Want to Build Your Own Online 3D Community?</h2>
	    <p>Do you have what it takes to help create the next generation of online communities? Are you a virtual world builder looking to take advantage of the opportunities in the new era of online gaming and social media? Then Kaneva is designed for you. Learn how to create your own free 3D online community.</p>
        <br /><a href="register/kaneva/registerInfo.aspx">Join Kaneva and experience the ultimate online community.</a>
     </div>
     
    <div class="seo" id="seo" runat="server" style="display:none">
      <h1 class="seo">The Virtual World of Kaneva</h1>
	    <h2 class="seo">The World of Kaneva is a free virtual world, where you can make new friends, hang out together, and just have fun.</h4>
	    <p>A digital metropolis full of cool hangouts, interesting people, and entertainment hot spots, Kaneva's free virtual world offers you an immersive 3D experience where you can play games, shop, or build your dream home.</p>
	    <p>Bring your friends, your favorite videos and music&#133; bring your imagination, and step into our free virtual world, the World of Kaneva.</p>
        <br /><a href="register/kaneva/registerInfo.aspx">Join Kaneva and experience the ultimate online community.</a>
     </div>
     
</div><br /> 
<div style="display:none;">													
	<!--Google Code for Yahoo! Conversion Page -->
	<script language="JavaScript" type="text/javascript">
	<!--
	var google_conversion_type='landing';
	var google_conversion_id=1065938872;
	var google_conversion_language="en_US";
	var google_conversion_format="1";
	var google_conversion_color="FFFFFF";
	//-->
	</script>
	<script language="JavaScript" src="http://www.googleadservices.com/pagead/conversion.js">
	</script>
	<noscript>
	<img height=1 width=1 border=0 src="http://www.googleadservices.com/pagead/conversion/1065938872/extclk?script=0">
	</noscript>	
</div> 
</asp:Content>