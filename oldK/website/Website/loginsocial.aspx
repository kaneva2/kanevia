﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="loginsocial.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.loginsocial" %>
<%@ Register TagPrefix="Kaneva" TagName="LoginFormSoc" Src="~/usercontrols/LoginFormSocial.ascx" %>

<!--[if IE 6]>
<link rel="stylesheet" href="css/new_IE.css" type="text/css" />
<![endif]-->

<link href="css/themes-join/template.css" rel="stylesheet" type="text/css">	
<link href="css/registration_flow/facebookconnect.css?v2" rel="stylesheet" type="text/css">	
<link href="css/base/base_new.css" rel="stylesheet" type="text/css">	

<br />
<table cellpadding="0" cellspacing="0" border="0" width="790" align="center">
	<tr>
		<td colspan="3"><img id="Img1" runat="server" src="~/images/spacer.gif" width="1" height="14" /></td>
	</tr>
	<tr>
		<td colspan="3">
			<table class="sociallogin" border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="frTopLeft"></td>
					<td class="frTop"></td>
					<td class="frTopRight"></td>
				</tr>
				<tr>
					<td class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" id="Img2" width="1" height="1" /></td>
					<td valign="top" class="frBgIntMembers">
						<div class="container">
							<h6>Log In</h6>
							<div id="desc" runat="server"></div>
							<p class="bold">If you do not have a Kaneva account, please signup <asp:linkbutton id="lbRegister" runat="server" class="afb">here</asp:linkbutton>.</p>
					 
							<center>
							<Kaneva:LoginFormSoc runat="server" id="loginForm"></Kaneva:LoginFormSoc>
							</center>

							<asp:hiddenfield id="accessToken" runat="server"></asp:hiddenfield>
						</div>
					</td>
					<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" id="Img4" width="1" height="1" /></td>
				</tr>
				<tr>
					<td class="frBottomLeft"></td>
					<td class="frBottom"></td>
					<td class="frBottomRight"></td>
				</tr>
			</table>				
		</td>
	</tr>
</table>
<br/><br/><br/><br/>
