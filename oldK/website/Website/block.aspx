﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="block.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.block" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<script type="text/javascript">
	parent.$j("#fbtitle").text('Block/Report');
</script>

<link href="/css/base/buttons_new.css" rel="stylesheet" type="text/css" />
<style type="text/css">
.blockcontainer {width:300px;height:152px;text-align:left;margin-top:10px;color:#333;font-family:Verdana, Geneva, sans-serif;}
#chkBlock, #chkReport {margin:18px 10px 0 0;padding:0;}
.desc {margin:4px 0 0 24px;line-height:1.1em;font-size:12px;}
.btncontainer {margin:20px auto 0 auto;display:inline-block;text-align:center;width:100%;}
.btncontainer div {margin:0 auto;display:inline-block;}
.btncontainer a {float:left;margin:0 4px;}
label {font-weight:bold;padding:0;position:relative;top:-2px;font-size:14px;padding-left:0;}
</style>

<asp:scriptmanager id="smBLock" runat="server" enablepartialrendering="true"></asp:scriptmanager>
            
<div id="divData" class="blockcontainer" runat="server">

<input type="checkbox" id="chkBlock" clientidmode="Static" runat="server" value="block" /><label>Block <asp:literal id="litUsername" runat="server"></asp:literal></label>
<div class="desc">This member will be blocked from all communications and from traveling to you.</div>

<input type="checkbox" id="chkReport" clientidmode="Static" runat="server" value="block" /><label>Submit a Report</label>
<div class="desc">Report member on web site</div>

<asp:updatepanel id="upButtons" runat="server" childrenastriggers="true" updatemode="conditional">
	<contenttemplate>

		<div class="btncontainer"><div>
		<a class="btnxsmall black" id="btnOK" onserverclick="btnOK_Click" runat="server" href="javascript:void(0);">OK</a>
		<a class="btnxsmall black" id="btnCancel" runat="server" href="javascript:CloseBlock();">Cancel</a>
		</div></div>

	</contenttemplate>
</asp:updatepanel>

</div>

<script type="text/javascript">
	function UpdateAndClose() { Close(); }
	function CloseBlock() { 
		parent.$j("#fbtitle").text('');
		parent.$j.facebox.close(); 
	}
</script>