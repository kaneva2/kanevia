///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;

namespace KlausEnt.KEP.Kaneva
{
    public partial class kanevaInvite : NoBorderPage
    {
        protected void Page_Load (object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request["kuid"] != null)
                {
                    try
                    {
                        if (Request.UrlReferrer != null)
                        {
                            if (!KanevaGlobals.SiteName.Contains (Request.UrlReferrer.Host.ToString ()) && Request.UrlReferrer.Host.ToString () != "")
                            {
                                Response.Cookies[Constants.ACQUISITION_COOKIE_NAME].Values[Constants.ACQUISITION_COOKIE_VALUE_HOST] = KanevaGlobals.Encrypt (Request.UrlReferrer.Host.ToString ());
                                Response.Cookies[Constants.ACQUISITION_COOKIE_NAME].Values[Constants.ACQUISITION_COOKIE_VALUE_ABSOLUTEURI] = KanevaGlobals.Encrypt (Request.UrlReferrer.AbsoluteUri.ToString ());
                                Response.Cookies[Constants.ACQUISITION_COOKIE_NAME].Expires = DateTime.Now.AddYears (1);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        m_logger.Error (ex.ToString ());
                    }
                }
            }
        }

        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);
    }
}