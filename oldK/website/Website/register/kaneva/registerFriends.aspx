<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="registerFriends.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.registerFriendsKaneva" %>
<%@ Register TagPrefix="Kaneva" TagName="ConnWithFriends" Src="../../usercontrols/ConnectWithFriends.ascx" %>


<!-- Google Website Optimizer Control Script -->
<!--
<script>
function utmx_section(){}function utmx(){}
(function(){var k='0875675435',d=document,l=d.location,c=d.cookie;function f(n){
if(c){var i=c.indexOf(n+'=');if(i>-1){var j=c.indexOf(';',i);return c.substring(i+n.
length+1,j<0?c.length:j)}}}var x=f('__utmx'),xx=f('__utmxx'),h=l.hash;
d.write('<sc'+'ript src="'+
'http'+(l.protocol=='https:'?'s://ssl':'://www')+'.google-analytics.com'
+'/siteopt.js?v=1&utmxkey='+k+'&utmx='+(x?x:'')+'&utmxx='+(xx?xx:'')+'&utmxtime='
+new Date().valueOf()+(h?'&utmxhash='+escape(h.substr(1)):'')+
'" type="text/javascript" charset="utf-8"></sc'+'ript>')})();
</script><script>utmx("url",'A/B');</script>
-->
<!-- End of Google Website Optimizer Control Script -->




<asp:literal id="litCSS" runat="server" />	

<link href="../../css/themes-join/template.css" rel="stylesheet" type="text/css" />	

<br />
<table cellpadding="0" cellspacing="0" border="0" width="790" align="center">
	<tr>
		<td colspan="3"><img runat="server" src="~/images/spacer.gif" width="1" height="14" id="Img12"/></td>
	</tr>
	<tr>
		<td colspan="3">
			<table  border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="frTopLeft"></td>
					<td class="frTop"></td>
					<td class="frTopRight"></td>
				</tr>
				<tr>
					<td class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" id="Img13" width="1" height="1" /></td>
					<td valign="top" class="frBgIntMembers">
						<table width="100%" border="0" cellpadding="0" cellspacing="0" >
							<tr>
								<td width="770" align="left"  valign="top">
									<table cellpadding="0" cellspacing="0" border="0"  width="100%">
										<tr>
											<td valign="top">
												<div id="templateHeader"><img runat="server" src="~/images/header_border.gif" alt="header_border.gif" width="768" height="120" border="0" id="Img14"><h1>Kaneva Join Process</h1></div>
											</td>
										</tr>
										<tr>
											<td width="790" valign="top">
												
												<table border="0" cellspacing="0" cellpadding="0" width="100%">
													<tr>
														<!-- START WIZARD CONTENT -->
														<td valign="top" align="center">

														<kaneva:ConnWithFriends runat="server" id="ucConnWithFriends" ExitUrl="register/kaneva/registerCompleted2.aspx"/>
																																																																																
														</td>
														<!-- END WIZARD CONTENT -->
													</tr>
												</table>
											
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" id="Img15" width="1" height="1" /></td>
				</tr>
				<tr>
					<td class="frBottomLeft"></td>
					<td class="frBottom"></td>
					<td class="frBottomRight"></td>
				</tr>
			</table>				
		</td>
	</tr>
</table>
<br/><br/><br/><br/>

<!-- Google Website Optimizer Tracking Script -->
<script type="text/javascript">
if(typeof(_gat)!='object')document.write('<sc'+'ript src="http'+
(document.location.protocol=='https:'?'s://ssl':'://www')+
'.google-analytics.com/ga.js"></sc'+'ript>')</script>
<script type="text/javascript">
try {
var gwoTracker=_gat._getTracker("UA-10115643-15");
gwoTracker._trackPageview("/0875675435/test");
}catch(err){}</script>
<!-- End of Google Website Optimizer Tracking Script -->

