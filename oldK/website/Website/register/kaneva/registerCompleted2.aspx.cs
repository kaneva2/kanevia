///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for registerCompleted.
	/// </summary>
    public partial class registerCompleted2 : NoBorderPage
	{
		private void Page_Load(object sender, System.EventArgs e)
		{						
			// If not a windows box, go directly to mykaneva page
			if (!Request.Browser.Platform.ToLower().Contains("win"))
			{
				Response.Redirect(ResolveUrl("~/default.aspx"));
			}

			// Is the email address validated?  If not, don't show email has been validated message
			if (!Request.IsAuthenticated ||
				KanevaWebGlobals.CurrentUser.StatusId.Equals((int)Constants.eUSER_STATUS.REGNOTVALIDATED))
			{
				h2Validated.InnerHtml = "<br/>";
			}

			if (Request.Cookies["invsent"] != null)
			{
				if (Request.Cookies["invsent"].Value == "true")
				{
					pnlInvExp.Visible = true;					
				}
			}

			if (!IsPostBack)
			{
				int community_id = 0;

				try
				{
					community_id = Convert.ToInt32(Request.QueryString["joinid"]);
				}
				catch { }

				// Get the join configuration settings
				if (community_id > 0)
				{
					DataRow drJoin = CommunityUtility.GetJoinConfig(community_id);

					if (drJoin != null)
					{						
						if (drJoin["css"] != null && drJoin["css"].ToString().Length > 0)
						{
							litCSS.Text = "<link href=\"" + drJoin["css"].ToString() + "\" rel=\"stylesheet\" type=\"text/css\">";
						}
					}

					btnGetStarted.CommandName = "communityid";
					btnGetStarted.CommandArgument = community_id.ToString();
				}

				// Get user info
				spnUserName.InnerText = KanevaWebGlobals.CurrentUser.Username;
			}

            aDownload.HRef = KanevaSetupInstallURL();
            aDownload2.HRef = KanevaSetupInstallURL();
		}

		#region Event Handlers
		protected void btnGetStarted_Click(Object sender, CommandEventArgs e)
		{

		}
		#endregion

		#region Declerations

		protected Button btnGetStarted;
		protected Literal litCSS;
        protected HtmlAnchor aDownload, aDownload2;

		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
