///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for registerCompleted.
	/// </summary>
    public class registerCompletedKaneva : NoBorderPage
	{
		private void Page_Load(object sender, System.EventArgs e)
		{
           
            // If not a windows box, go directly to mykaneva page
            if (!Request.Browser.Platform.ToLower().Contains ("win"))
            {
                Response.Redirect (ResolveUrl("~/default.aspx"));
            }
             
            if (!IsPostBack)
			{
                // Browser specific image
                if (Request.Browser.Browser.ToLower().Contains("firefox"))
                {
                    imgDownload.Src = ResolveUrl("~/images/download/firefox.png");
                }
                else if (Request.Browser.Browser.ToLower().Contains("chrome"))
                {
                    imgDownload.Src = ResolveUrl("~/images/download/chrome.png");
                }

                // Grab single sign-on session variables used to display the sso Flash content.
                string singleSignOnToken = GetUserFacade.GetSingleSignOnToken(KanevaWebGlobals.CurrentUser.UserId);
                if (!string.IsNullOrWhiteSpace(singleSignOnToken))
                {
                    string fSSOVars = "<param name=\"FlashVars\" value=\"" + singleSignOnToken + "\" />";

                    divFlashSSO.Visible = true;
                    litFlashSSOVars.Text = fSSOVars;
                    litFlashSSOVarsIE.Text = fSSOVars;
                }

                // Insert web login_log record
                GetUserFacade.UpdateLastLogin(KanevaWebGlobals.CurrentUser.UserId, Common.GetVisitorIPAddress(), Server.MachineName);

                // Start the client download.
                if (!Page.ClientScript.IsClientScriptBlockRegistered(GetType(), "downloadstartup"))
                {
                    string javascript = "<script type=\"text/javascript\">function show() { location.href = '" + KanevaSetupInstallURL() + "';} window.onload = show;</script>";
                    Page.ClientScript.RegisterStartupScript(GetType(), "downloadstartup", javascript);
                }
			}

            aDownload.HRef = KanevaSetupInstallURL();
		}


		#region Declerations

        protected HtmlImage imgDownload;
        protected HtmlContainerControl divFlashSSO;
        protected Literal litFlashSSOVars, litFlashSSOVarsIE;
        protected HtmlAnchor aDownload;

        public string FlashSSOPath
        {
            get { return ResolveUrl("~/flash/kaneva_sinfo.swf"); }
        }
		
		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
