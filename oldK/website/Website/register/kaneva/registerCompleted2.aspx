<%@ Page Language="C#" MasterPageFile="~/MASTERPAGES/Register.Master" AutoEventWireup="true" CodeBehind="registerCompleted2.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.registerCompleted2" Title="Untitled Page" %>
<%@ Register TagPrefix="kaneva" TagName="conversionTracking" Src="../../usercontrols/register/RegisterConversionTracking.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cph_Body" runat="server">
<link href="../../css/registration_flow/registerCompleted.css" rel="stylesheet" type="text/css" />

<div class="content_wrapper">
	<div id="ContentContainer">

		<img src="../../images/registration_flow/install_image_438x275.jpg" style="float:right;" />
		<h1 class="homepageHead">Congratulations <span id="spnUserName" runat="server"></span>!<br>
		You're now a member of Kaneva!</h1>
		<br />
		<br />
		<p id="h2Validated" runat="server" class="headline">Your Email address has been validated!</p>
		<p class="headline">Next, we'll install the Kaneva 3D Explorer</p>			
		<a runat="server" id="aDownload" class="nohover"
			onclick="showNav()"><img src="../../images/registration_flow/btn_install_257x41.gif"  
			border="0" align="center" /></a>

		<br style="clear:both;" />

		<div class="installHelp">

			<img src="../../images/registration_flow/guide_run_alert_176x117.gif" class="installImg">
			<img src="../../images/registration_flow/guide_ff_alert_227x96.gif" class="installImg">		

			<h2>How to Install 3D World 
			<a onclick="javascript:window.open('http://docs.kaneva.com/mediawiki/index.php/System_Requirements','Priv','width=500,height=400,resizable=yes,scrollbars=yes');false;" href="#"><span class="small">(System Requirements)</span></a></h2>
			<br />
			<p class="headline"><span class="installCount">1)</span> Click <strong>"Run"</strong> in each window that appears.</p>
			<br />
			<p class="headline"><span class="installCount">2)</span> Sign in using the email address you used to create your account.</p>
			
			<br style="clear:both;" />
			</div>
	</div>
</div>

<!-- Important -->
<kaneva:conversionTracking id="convTrack" runat="server"></kaneva:conversionTracking>

<asp:Panel ID="pnlInvExp" Visible="false" runat="server">

<!-- Google Website Optimizer Conversion Script -->
<script type="text/javascript">
if(typeof(_gat)!='object')document.write('<sc'+'ript src="http'+
(document.location.protocol=='https:'?'s://ssl':'://www')+
'.google-analytics.com/ga.js"></sc'+'ript>')</script>
<script type="text/javascript">
try {
var gwoTracker=_gat._getTracker("UA-10115643-15");
gwoTracker._trackPageview("/0875675435/goal");
}catch(err){}</script>
<!-- End of Google Website Optimizer Conversion Script -->

</asp:Panel>

</asp:Content>
