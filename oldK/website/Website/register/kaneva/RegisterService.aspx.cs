///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using System.Text.RegularExpressions;
using System.Security.Cryptography;
using System.Web.Security;
using log4net;
using KlausEnt.KEP.Kaneva.framework.widgets;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
    public partial class RegisterService : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Fields are username, firstName, lastName, gender,  countryCode, zipCode (US Only), birthMonth, birthDay, birthYear, email, password

            if (!IsPostBack)
            {
                string strUserName = "";
                string firstName = "";
                string lastName = "";
                string gender = "";
                string countryCode = "";
                string zipCode = "";
                string birthMonth = "";
                string birthDay = "";
                string birthYear = "";
                string email = "";
                string strPassword = "";


                // Returns userId on sucess


                // Do some service validations....
                if ((Request.Params["username"] == null) || (Request.Params["firstName"] == null) || (Request.Params["lastName"] == null))
                {
                    string errorStr = "<Result>\r\n  <ReturnCode>-10</ReturnCode>\r\n  <ResultDescription>username or firstName or lastName not specified</ResultDescription>\r\n</Result>";
                    Response.Write(errorStr);
                    return;
                }

                if ((Request.Params["gender"] == null) || (Request.Params["countryCode"] == null) || (Request.Params["email"] == null) || (Request.Params["password"] == null))
                {
                    string errorStr = "<Result>\r\n  <ReturnCode>-9</ReturnCode>\r\n  <ResultDescription>gender or countryCode or email or password not specified</ResultDescription>\r\n</Result>";
                    Response.Write(errorStr);
                    return;
                }

                if ((Request.Params["birthMonth"] == null) || (Request.Params["birthDay"] == null) || (Request.Params["birthYear"] == null))
                {
                    string errorStr = "<Result>\r\n  <ReturnCode>-10</ReturnCode>\r\n  <ResultDescription>birthMonth or birthDay or birthYear not specified</ResultDescription>\r\n</Result>";
                    Response.Write(errorStr);
                    return;
                }

                strUserName = Request.Params["username"].ToString();
                firstName = Request.Params["firstName"].ToString();
                lastName = Request.Params["lastName"].ToString();
                gender = Request.Params["gender"].ToString().Equals ("F") ? "F" : "M";
                countryCode = Request.Params["countryCode"].ToString();
                zipCode = "";
                birthMonth = Request.Params["birthMonth"].ToString(); ;
                birthDay = Request.Params["birthDay"].ToString();
                birthYear = Request.Params["birthYear"].ToString();
                email = Request.Params["email"].ToString();
                strPassword = Request.Params["password"].ToString();

                if (!Regex.IsMatch(strUserName, "^" + Constants.VALIDATION_REGEX_USERNAME  + "$"))
                {
                    string errorStr = "<Result>\r\n  <ReturnCode>-10</ReturnCode>\r\n  <ResultDescription>Username must be 4-20 characters, and can contain upper or lower-case letters, numbers, and underscores (_) only. No spaces.</ResultDescription>\r\n</Result>";
                    Response.Write(errorStr);
                    return;
                }

                if (!Regex.IsMatch(strPassword, "^" + Constants.VALIDATION_REGEX_PASSWORD + "$"))
                {
                    string errorStr = "<Result>\r\n  <ReturnCode>-9</ReturnCode>\r\n  <ResultDescription>Password must be 4-20 characters, and can contain upper or lower-case letters, numbers, and underscores (_) only. No spaces.</ResultDescription>\r\n</Result>";
                    Response.Write(errorStr);
                    return;
                }

                if (!Regex.IsMatch(email, Constants.VALIDATION_REGEX_EMAIL))
                {
                    string errorStr = "<Result>\r\n  <ReturnCode>-9</ReturnCode>\r\n  <ResultDescription>Invalid Email</ResultDescription>\r\n</Result>";
                    Response.Write(errorStr);
                    return;
                }


                if (countryCode.Equals ("US"))
                {

                    if (Request.Params["zipCode"] == null)
                    {
                        string errorStr = "<Result>\r\n  <ReturnCode>-8</ReturnCode>\r\n  <ResultDescription>zipCode is required for US</ResultDescription>\r\n</Result>";
                        Response.Write(errorStr);
                        return;
                    }

                    zipCode = Request.Params["zipCode"].ToString(); ;
                }

                //compile users birthday
                DateTime dtBirthDate = new DateTime();
                try
                {
                    dtBirthDate = new DateTime(Convert.ToInt32(birthYear), Convert.ToInt32(birthMonth), Convert.ToInt32(birthDay));
                }
                catch
                {
                    string errorStr = "<Result>\r\n  <ReturnCode>-6</ReturnCode>\r\n  <ResultDescription>Birthdate is an invalid date./ResultDescription>\r\n</Result>";
                    Response.Write(errorStr);
                    return;
                }

                //check to see if applicant is a minor before proceeding
                if (IsUserAMinor(dtBirthDate))
                {
                    //display message alerting user they are under aged
                    string errorStr = "<Result>\r\n  <ReturnCode>-7</ReturnCode>\r\n  <ResultDescription>We apologize but users under the age of " + KanevaGlobals.MinorCutOffAge + " are not allowed to register.</ResultDescription>\r\n</Result>";
                    Response.Write(errorStr);
                    return;
                }

                // Check to see if users IP address has been banned
                try
                {
                    DataRow drIPBan = UsersUtility.GetIPBan(Common.GetVisitorIPAddress());
                    if (drIPBan != null && drIPBan.Table.Rows.Count > 0)
                    {
                        // Now check to see if account is still banned
                        if (Convert.ToDateTime(drIPBan["ban_end_date"]) < DateTime.Now)
                        {   // Ban has expired, un-ban ip and continue
                            UsersUtility.RemoveIPBan(Common.GetVisitorIPAddress());
                        }
                        else
                        {   // Ban still active, show
                            string errorStr = "<Result>\r\n  <ReturnCode>-5</ReturnCode>\r\n  <ResultDescription>Registration failed:</b> This account has been locked by the Kaneva administrator.</ResultDescription>\r\n</Result>";
                            Response.Write(errorStr);
                            return;
                        }
                    }
                }
                catch (Exception)
                {
                    //m_logger.Error("Error checking user IP against Banned IP List: ", ex);
                }


                // Hash password
                byte[] salt = new byte[9];
                new RNGCryptoServiceProvider().GetBytes(salt);
                string hashPassword = FormsAuthentication.HashPasswordForStoringInConfigFile(UsersUtility.MakeHash(strPassword + strUserName.ToLower()) + Convert.ToBase64String(salt), "MD5");

                // Generate a unique key_code for registration purposes
                string keyCode = KanevaGlobals.GenerateUniqueString(20);

                // Create a registration Key
                string regKey = KanevaGlobals.GenerateUniqueString(50);

                // Set the default Display Name
                string displayName = "";

                // If user is under 18, set display name to first name and last initial only
                if (DateTime.Today.AddYears(-18) < dtBirthDate)
                {
                    displayName = String.Concat(Server.HtmlEncode(firstName), " ", Server.HtmlEncode(lastName).Substring(0, 1));
                }
                else
                {
                    int displayNameLen = (Server.HtmlEncode(firstName.Trim()) + Server.HtmlEncode(lastName.Trim())).Length;
                    if (displayNameLen >= 30)
                    {
                        if (displayNameLen == 30)
                        {
                            displayName = String.Concat(Server.HtmlEncode(firstName), Server.HtmlEncode(lastName));
                        }
                        else
                        {
                            displayName = String.Concat(Server.HtmlEncode(firstName) + Server.HtmlEncode(lastName)).Substring(0, 29);
                        }
                    }
                    else
                    {
                        displayName = String.Concat(Server.HtmlEncode(firstName), " ", Server.HtmlEncode(lastName));
                    }
                }

                int result = 0;
                int userId = 0;

                // Make sure it is not on the invalid user names list
                if (UsersUtility.IsReservedName(strUserName))
                {
                    result = -3;
                }
                else
                {
                    // This returns userId
                    userId = GetUserFacade.InsertUser(strUserName, hashPassword, Convert.ToBase64String(salt), 2, 1,
                        Server.HtmlEncode(firstName), Server.HtmlEncode(lastName), displayName, Server.HtmlEncode(gender), "", Server.HtmlEncode(email), dtBirthDate, keyCode,
                        Server.HtmlEncode(countryCode), Server.HtmlEncode(zipCode), regKey, Common.GetVisitorIPAddress(), (int)Kaneva.Constants.eUSER_STATUS.REGNOTVALIDATED, 0);

                    // Did we get a success?
                    if (userId > 0)
                    {
                        result = 0;
                    }
                    else
                    {
                        result = userId;
                    }
                }

                switch (result)
                {
                    case 0:
                        {

                            // Create their home page and world
                            if (CreateDefaultUserHomePage(strUserName, userId) && CreateDefaultUserHomeWorld(strUserName, userId))
                            {
                                // home page was built successfullly
                                //get default kaneva community
                                string defaultCommunityId = System.Configuration.ConfigurationManager.AppSettings["Default_Kaneva_Community_Id"];
                                if (defaultCommunityId != null && defaultCommunityId.Trim() != "")
                                {
                                    int communityId = Convert.ToInt32(defaultCommunityId); //logic to prevent convrt exceptions

                                    //auto join the user to the designated kaneva community
                                    CommunityMember member = new CommunityMember();
                                    member.CommunityId = communityId;
                                    member.UserId = userId;
                                    member.AccountTypeId = (int)CommunityMember.CommunityMemberAccountType.SUBSCRIBER;
                                    member.Newsletter = "Y";
                                    member.InvitedByUserId = 0;
                                    member.AllowAssetUploads = "N";
                                    member.AllowForumUse = "Y";
                                    member.StatusId = (int)CommunityMember.CommunityMemberStatus.ACTIVE;

                                    GetCommunityFacade.InsertCommunityMember(member);
                                }

                            }
                            else
                            {
                                // Problem creating default home page
                                GetUserFacade.CleanUpRegistrationIssue(strUserName);

                                // return success with user id
                                string errorStr = "<Result>\r\n  <ReturnCode>-4</ReturnCode>\r\n  <ResultDescription>The default home page for this user could not be created.</ResultDescription>\r\n</Result>";
                                Response.Write(errorStr);
                                return;
                            }

                            // return success with user id
                            string errorStr2 = "<Result>\r\n  <ReturnCode>0</ReturnCode>\r\n  <ResultDescription>Success</ResultDescription><User username=\"" + strUserName + "\" user_id=\"" + userId.ToString() + "\"></User>\r\n</Result>";
                            Response.Write(errorStr2);
                            return;
                        }
                    case -1:
                        {
                            string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>The Username you requested isn't available. Please select another Username.</ResultDescription>\r\n</Result>";
                            Response.Write(errorStr);
                            return;
                        }
                    case -2:
                        {
                            string errorStr = "<Result>\r\n  <ReturnCode>-2</ReturnCode>\r\n  <ResultDescription>The email address you provided is already associated with a Kaneva account. Please provide another email address and re-enter your password.</ResultDescription>\r\n</Result>";
                            Response.Write(errorStr);
                            return;
                        }
                    case -3:
                        {

                            string errorStr = "<Result>\r\n  <ReturnCode>-3</ReturnCode>\r\n  <ResultDescription>The Username already exists or is not allowed. Please change your Username.</ResultDescription>\r\n</Result>";
                            Response.Write(errorStr);
                            return;
                        }
                    case -4:
                        {
                            string errorStr = "<Result>\r\n  <ReturnCode>-4</ReturnCode>\r\n  <ResultDescription>The default home page for this user could not be created.</ResultDescription>\r\n</Result>";
                            Response.Write(errorStr);
                            return;
                        }
                }






            }

        }

        /// <summary>
		/// Creates the default home page for a newly registered user. Places a set of default
		/// widgets on the page.
		/// </summary>
		/// <returns>bool - true on success</returns>
		private bool CreateDefaultUserHomePage (string username, int userId)
		{
            if (userId.Equals(0))
            {
                return false;
            }

            try
            {
                //create a personal channel
                Community newUserPrifile = new Community();
                newUserPrifile.PlaceTypeId = 0;
                newUserPrifile.Name = username;
                newUserPrifile.NameNoSpaces = username.Replace(" ", "");
                newUserPrifile.Description = "NEW";
                newUserPrifile.CreatorId = userId;
                newUserPrifile.CreatorUsername = username;
                newUserPrifile.IsPublic = "Y";
                newUserPrifile.IsAdult = "N";
                newUserPrifile.IsPersonal = 1; //true
                newUserPrifile.CommunityTypeId = (int)CommunityType.USER;
                newUserPrifile.StatusId = (int)CommunityStatus.ACTIVE;

                int profileId = GetCommunityFacade.InsertCommunity(newUserPrifile);

                //if profile successfully created add user as member
                if (profileId > 0)
                {
                    CommunityMember member = new CommunityMember();
                    member.CommunityId = profileId;
                    member.UserId = userId;
                    member.AccountTypeId = (int)CommunityMember.CommunityMemberAccountType.OWNER;
                    member.Newsletter = "Y";
                    member.InvitedByUserId = 0;
                    member.AllowAssetUploads = "Y";
                    member.AllowForumUse = "Y";
                    member.StatusId = (int)CommunityMember.CommunityMemberStatus.ACTIVE;

                    GetCommunityFacade.InsertCommunityMember(member);
                }

                // Give each user three friends groups: 
                FriendGroup friendGroup = new FriendGroup();
                friendGroup.OwnerId = userId;

                //create Family
                friendGroup.Name = "Family";
                GetUserFacade.InsertFriendGroup(friendGroup);

                //create Coworkers
                friendGroup.Name = "Coworkers";
                GetUserFacade.InsertFriendGroup(friendGroup);

                //create Inner Circle
                friendGroup.Name = "Inner Circle";
                GetUserFacade.InsertFriendGroup(friendGroup);

                return true;
            }
            catch (Exception)
            {
                //m_logger.Error(e.ToString());
            }

            return false;
        }


        /// <summary>
        /// Creates the default home world for a newly registered user. 
        /// </summary>
        /// <returns>bool - true on success</returns>
        private bool CreateDefaultUserHomeWorld(string username, int userId)
        {
            if (userId.Equals(0))
            {
                return false;
            }

            try
            {
                string worldName = username + " World";
                int worldNameIndex = 0;
                bool invalidNameFound = GetCommunityFacade.IsCommunityNameTaken(false, worldName, 0);

                while (invalidNameFound)
                {
                    worldNameIndex++;
                    invalidNameFound = GetCommunityFacade.IsCommunityNameTaken(false, worldName + " " + worldNameIndex, 0);
                }

                //create a personal channel
                Community homeWorld = new Community();
                homeWorld.PlaceTypeId = 0;
                homeWorld.Name = worldName;
                homeWorld.NameNoSpaces = worldName.Replace(" ", "");
                homeWorld.Description = username + " Home World";
                homeWorld.CreatorId = userId;
                homeWorld.CreatorUsername = username;
                homeWorld.IsPublic = "Y";
                homeWorld.IsAdult = "N";
                homeWorld.IsPersonal = 0; //false
                homeWorld.CommunityTypeId = (int)CommunityType.HOME;
                homeWorld.StatusId = (int)CommunityStatus.ACTIVE;

                int communityId = GetCommunityFacade.InsertCommunity(homeWorld);
                bool success = (communityId > 0);

                //if profile successfully created add user as member
                if (success)
                {
                    CommunityMember member = new CommunityMember();
                    member.CommunityId = communityId;
                    member.UserId = userId;
                    member.AccountTypeId = (int)CommunityMember.CommunityMemberAccountType.OWNER;
                    member.Newsletter = "Y";
                    member.InvitedByUserId = 0;
                    member.AllowAssetUploads = "Y";
                    member.AllowForumUse = "Y";
                    member.StatusId = (int)CommunityMember.CommunityMemberStatus.ACTIVE;

                    GetCommunityFacade.InsertCommunityMember(member);

                    GetUserFacade.UpdateUserHomeCommunityId(userId, communityId);
                }

                return success;
            }
            catch (Exception)
            {
                // m_logger.Error(e.ToString());
            }

            return false;
        }

      
    }
}