///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for registerCompleted.
	/// </summary>
    public class registerCompletedKaneva_B : NoBorderPage
	{
		private void Page_Load(object sender, System.EventArgs e)
		{
            // If not a windows box, go directly to mykaneva page
            if (!Request.Browser.Platform.ToLower().Contains ("win"))
            {
                Response.Redirect (ResolveUrl("~/default.aspx"));
            }
            
            // Is the email address validated?  If not, don't show email has been validated message
            if (!Request.IsAuthenticated || 
                KanevaWebGlobals.CurrentUser.StatusId.Equals ((int) Constants.eUSER_STATUS.REGNOTVALIDATED))
            {
                h2Validated.InnerHtml = "<br/>";
            }
             
            if (!IsPostBack)
			{
				int community_id = 0;
				
				try
				{
					community_id = Convert.ToInt32 (Request.QueryString["joinid"]);
				}
				catch {}
				
				// Get the join configuration settings
				if (community_id > 0)
				{
					DataRow drJoin = CommunityUtility.GetJoinConfig (community_id);
			
					if (drJoin != null)
					{	
						if (drJoin ["page_complete_copy"] != null && drJoin ["page_complete_copy"].ToString ().Length > 0)
						{
							spnCoBrandCopy.InnerHtml = drJoin ["page_complete_copy"].ToString ();
						}

						if (drJoin ["css"] != null && drJoin ["css"].ToString ().Length > 0)
						{
							litCSS.Text = "<link href=\"" + drJoin ["css"].ToString () + "\" rel=\"stylesheet\" type=\"text/css\">";
						}
					}
				
					btnGetStarted.CommandName = "communityid";
					btnGetStarted.CommandArgument = community_id.ToString ();
				}

				// Get user info
				spnUserName.InnerText = KanevaWebGlobals.CurrentUser.Username;

                // Start the client download.
                if (!Page.ClientScript.IsClientScriptBlockRegistered(GetType(), "downloadstartup"))
                {
                    string javascript = "<script type=\"text/javascript\">function show() { location.href = '" + KanevaSetupInstallURL() + "';} window.onload = show;</script>";
                    Page.ClientScript.RegisterStartupScript(GetType(), "downloadstartup", javascript);
                }
			}

            aDownload.HRef = KanevaSetupInstallURL();
		}

		#region Event Handlers
		protected void btnGetStarted_Click (Object sender, CommandEventArgs e)
		{
			
		}
		#endregion

		#region Declerations

		protected Button	btnGetStarted;
		protected Literal	litCSS;

        protected HtmlContainerControl h2Validated;
        protected HtmlContainerControl	spnCoBrandCopy, spnUserName;

        protected HtmlAnchor aDownload;

		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}

