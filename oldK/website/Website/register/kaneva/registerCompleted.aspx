<%@ Page language="c#" Codebehind="registerCompleted.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.registerCompletedKaneva" %>

<asp:literal id="litCSS" runat="server" />	

<link href="../../css/themes-join/template.css" rel="stylesheet" type="text/css" />	

<style type="text/css">
    body {
	    background: #F6f6f6 url("../../images/landing_page/background_100x100_greyNoise.jpg") repeat left top;
    }
    a:hover{
	    background-color:#ffffff;
    }
</style>

<script language="javascript">
function showNav() {
    document.getElementById('primary').style.display='inline' ;
    document.getElementById('tertiary').style.display='inline' ;
}
</script>

<!-- Header -->
<div style="background-color:#ffffff;border-bottom:1px solid #f6f6f6;margin:0;padding:0;">
    <div style="text-align:left;width:973px;margin:0;padding:21px 0 16px 0;">
        <a id="a1" runat="server" href="~/default.aspx">
            <img id="Img2" runat="server" BORDER=0 src="~/images/download/kaneva_logo.png" width="200" height="32"/>
        </a>
    </div>
    <div id="divFlashSSO" runat="server" visible="false">
	    <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="1" height="1" id="kaneva_sinfo" align="middle">
		    <param name="movie" value="<%:FlashSSOPath %>" />
		    <param name="quality" value="high" />
		    <param name="bgcolor" value="#ffffff" />
		    <param name="play" value="true" />
		    <param name="loop" value="false" />
		    <param name="wmode" value="transparent" />
		    <param name="scale" value="showall" />
		    <param name="menu" value="true" />
		    <param name="devicefont" value="false" />
		    <param name="salign" value="" />
		    <param name="allowScriptAccess" value="sameDomain" />
            <asp:Literal ID="litFlashSSOVars" runat="server" />
		    <!--[if !IE]>-->
		    <object type="application/x-shockwave-flash" data="<%:FlashSSOPath %>" width="1" height="1">
			    <param name="movie" value="<%:FlashSSOPath %>" />
			    <param name="quality" value="high" />
			    <param name="bgcolor" value="#ffffff" />
			    <param name="play" value="true" />
			    <param name="loop" value="false" />
			    <param name="wmode" value="transparent" />
			    <param name="scale" value="showall" />
			    <param name="menu" value="true" />
			    <param name="devicefont" value="false" />
			    <param name="salign" value="" />
			    <param name="allowScriptAccess" value="sameDomain" />
                <asp:Literal ID="litFlashSSOVarsIE" runat="server" />
		    </object>
		    <!--<![endif]-->
	    </object>
    </div>
</div>

    <!-- Body Middle -->
<div style="background-color:#ffffff;border-bottom:1px solid #c4d730;margin:0;padding:0;">
    <div style="width:973px;margin:0px;padding:12px 0px 0px 0px;"> 
        <a style="padding:0px;margin:0px;" runat="server" id="aDownload">
            <img id="Img1" runat="server" BORDER=0 src="~/images/download/thanks_registration.png" />
        </a>
    </div>
</div>

<!-- Body Bottom -->
<div style="width:973px;padding:0px;margin:0px;">
    <div style="padding-top:15px;border-top:1px solid #ffffff;"></div>
    <img style="padding-top:15px" id="imgDownload" runat="server" src="~/images/download/explorer.png" border="0"/>
</div>


<!-- Google Code for pageview Conversion Page -->
<script language="JavaScript" type="text/javascript">
<!--
var google_conversion_id = 1065938872;
var google_conversion_language = "en_US";
var google_conversion_format = "1";
var google_conversion_color = "FFFFFF";
if (2.0) {
  var google_conversion_value = 2.0;
}
var google_conversion_label = "pageview";
//-->
</script>
<script language="JavaScript" src="http://www.googleadservices.com/pagead/conversion.js"> [^]
</script>
<noscript>
<img height=1 width=1 border=0 src="http://www.googleadservices.com/pagead/conversion/1065938872/?value=2.0&label=pageview&script=0"> [^]
</noscript>

<script>
if(typeof(urchinTracker)!='function')document.write('<sc'+'ript src="'+
'http'+(document.location.protocol=='https:'?'s://ssl':'://www')+
'.google-analytics.com/urchin.js'+'"></sc'+'ript>')
</script>
<script>
_uacct = 'UA-5755114-4';
urchinTracker("/3837460917/goal");
</script>

<script>
if(typeof(urchinTracker)!='function')document.write('<sc'+'ript src="'+
'http'+(document.location.protocol=='https:'?'s://ssl':'://www')+
'.google-analytics.com/urchin.js'+'"></sc'+'ript>')
</script>
<script>
try {
_uacct = 'UA-5755114-4';
urchinTracker("/3799682931/goal");
} catch (err) { }
</script>