<%@ Page language="c#" Codebehind="registerValidate.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.registerValidateKaneva" %>

<link href="../../css/themes-join/template.css" rel="stylesheet" type="text/css">	
<asp:literal id="litCSS" runat="server" />	

<table cellpadding="0" cellspacing="0" border="0" width="790" align="center">
	<tr>
		<td colspan="3"><img runat="server" src="~/images/spacer.gif" width="1" height="14" id="Img12"/></td>
	</tr>
	<tr>
		<td colspan="3">
			<table  border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="frTopLeft"></td>
					<td class="frTop"></td>
					<td class="frTopRight"></td>
				</tr>
				<tr>
					<td class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" id="Img13" width="1" height="1" /></td>
					<td valign="top" class="frBgIntMembers">
						<table width="100%" border="0" cellpadding="0" cellspacing="0" >
							<tr>
								<td width="770" align="left"  valign="top">
								
									<table cellpadding="0" cellspacing="0" border="0"  width="100%">
										<tr>
											<td valign="top">
												<div id="templateHeader"><img runat="server" src="~/images/header_border.gif" alt="header_border.gif" width="768" height="120" border="0" id="Img14"><h1>Kaneva Join Process</h1></div>
											</td>
										</tr>
										<tr>
											<td width="790" valign="top">
												
												<table border="0" cellspacing="0" cellpadding="0" width="100%">
													<tr>
														<!-- START WIZARD CONTENT -->
														<td valign="top" align="center">
															
															<table border="0" cellspacing="0" cellpadding="0" width="670" align="center" class="wizform">
																<tr>
																	<td width="670">

																		<h1>Verify your email address</h1>
																		<p style="font-size:11pt;top:-10px;position:relative;display:block;">Thanks for joining Kaneva!</p>
																		<br />	
																		<p style="font-size:12pt;">We've just sent a verification email to <b><asp:label id="lblEmail" runat="server"></asp:label></b>. 
																		Take a second to click the link inside the email message and you'll earn your first 100 Kaneva Rewards!</p>
																		<br />
																		<p>Note: If you can�t find our email, check your spam or junk mail folders to ensure the 
																		message wasn't flagged as spam. Please add <u>kaneva@kaneva.com</u> to your email program's address book.</p>
																		
																		<br />
																		
																		<p><a id="A1" style="font-size:12pt;" runat="server" href="~/register/kaneva/registerCompleted.aspx">Skip this step</a></p> 
																			
																		<div id="smallpage_spacer"></div> 
																		 
																	</td>
																</tr>
															</table>
															
														</td>
														
														<!-- END WIZARD CONTENT -->
													</tr>
												</table>
											
											</td>
											
										</tr>
										
									</table>
											
								</td>
							</tr>
							
						</table>
					</td>
					<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" id="Img15" width="1" height="1" /></td>
				</tr>
				<tr>
					<td class="frBottomLeft"></td>
					<td class="frBottom"></td>
					<td class="frBottomRight"></td>
				</tr>
			</table>				
		</td>
	</tr>
</table>
<br/><br/><br/><br/>
<div style="display:none;">
<!-- Google Code for signup Conversion Page --> <script language="JavaScript" type="text/javascript">
<!--
var google_conversion_id = 1065938872;
var google_conversion_language = "en_US"; 
var google_conversion_format = "1"; 
var google_conversion_color = "FFFFFF"; if (1.0) {
var google_conversion_value = 1.0;
}
var google_conversion_label = "signup";
//-->
</script>
<script language="JavaScript"
src="http://www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<img height=1 width=1 border=0
src="http://www.googleadservices.com/pagead/conversion/1065938872/?value=1.0
&label=signup&script=0">
</noscript>

</div>
