///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Security.Cryptography;
using System.Web.Security;
using log4net;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using UAParser;
using System.Collections.Generic;

namespace KlausEnt.KEP.Kaneva
{
    public partial class registerInfoKaneva : BasePage
    {
        private void Page_Load(object sender, System.EventArgs e)
        {
            //Set the page Meta data
            ((GenericLandingPageTemplate)Master).Title = "Join Kaneva";
            ((GenericLandingPageTemplate)Master).MetaDataDescription = "Join Kaneva today. (It’s Free!) Why join Kaneva? Kaneva is the only place where your Profile, Friends, Media and favorite Communities are teleported into a modern-day 3D world where you can explore, socialize and experience entertainment in an entirely new way.";

            if (!IsPostBack)
            {
                if (string.IsNullOrEmpty(Request.Form["postFromHome"]))
                {
                    // Special case scenario for joining from a community / co-brand
                    int community_id = 0;

                    try
                    {
                        community_id = Convert.ToInt32(Request.QueryString["joinid"]);
                    }
                    catch { }

                    // Get the join configuration settings
                    DataRow drJoin = CommunityUtility.GetJoinConfig(community_id);

                    if (drJoin != null)
                    {
                        if (drJoin["page_join_copy"] != null && drJoin["page_join_copy"].ToString().Length > 0)
                        {
                            spnCoBrandCopy.InnerHtml = drJoin["page_join_copy"].ToString();
                        }

                        if (drJoin["css"] != null && drJoin["css"].ToString().Length > 0)
                        {
                            litCSS.Text = "<link href=\"" + drJoin["css"].ToString() + "\" rel=\"stylesheet\" type=\"text/css\">";
                        }
                    }

                    // Was this user invited by a friend?  If so, put this email in the email field  
                    // and and disable it so the user can not change it for this registration
                    if (Request["i"] != null)
                    {
                        try
                        {
                            int inviteId = Convert.ToInt32(Request["i"]);
                            DataRow drInvite = UsersUtility.GetInvite(inviteId, Request["k"].ToString());

                            if (drInvite != null && Convert.ToInt32(drInvite["invite_status_id"]) == (int)Constants.eINVITE_STATUS.UNREGISTER)
                            {
                                txtEmail.Text = Server.UrlDecode(drInvite["email"].ToString());
                                txtEmail.Enabled = false;

                                if (drInvite["to_name"] != null)
                                {
                                    string[] name = drInvite["to_name"].ToString().Split(' ');

                                    if (name.Length > 0)
                                    {
                                        txtFirstName.Text = name[0];

                                        for (int i = 1; i < name.Length; i++)
                                        {
                                            if (txtLastName.Text.Length > 0) txtLastName.Text += " ";
                                            txtLastName.Text += name[i];
                                        }
                                    }
                                }

                                // update the invite to show user clicked the link in invite email
                                UsersUtility.UpdateInviteClick(inviteId);
                            }
                        }
                        catch { }
                    }
                }

                drpCountry.DataTextField = "Name";
                drpCountry.DataValueField = "CountryId";
                drpCountry.DataSource = WebCache.GetCountries();
                drpCountry.DataBind();

                drpCountry.Items.Insert(0, new ListItem("United States", Constants.COUNTRY_CODE_UNITED_STATES));

                AddBreadCrumb(new BreadCrumb("Registration - Info", GetCurrentURL(), "", 0));

                if (Request.UrlReferrer != null && Request.QueryString["contest"] != null)
                {
                    ViewState["rurl"] = Request.UrlReferrer.AbsoluteUri.ToString();
                }

                if (Request.QueryString["redeem"] != null)
                {
                    ViewState["redeem"] = true;
                }

                LoadYears();

                ShowCaptcha();

                PrimeFormValuesFromFacebook();

                txtUserName.Focus();

                // Enable fast signup
                if (Request.QueryString["KANEVA_SPEEDY_SIGNUP"] != null && Request.QueryString["KANEVA_SPEEDY_SIGNUP"].Equals("true"))
                {
                    if (Request.QueryString["username"] != null && Request.QueryString["username"] != string.Empty)
                    {
                        SpeedySignupEnabled = true;
                        PrimeFormValuesForSpeedySignup();
                    }
                }

            }

            string scriptString;

            // Set up regular expression validators
            revEmail.ValidationExpression = Constants.VALIDATION_REGEX_EMAIL;
            revPassword.ValidationExpression = Constants.VALIDATION_REGEX_PASSWORD;
            revUsername.ValidationExpression = Constants.VALIDATION_REGEX_USERNAME;

            // Script to enable/disable zip code field base on selected country
            drpCountry.Attributes.Add("onchange", "CountryCheck ();");

            scriptString = "<script language=JavaScript>\n";
            scriptString += "function CountryCheck (){\n";
            scriptString += " if (document.getElementById('" + drpCountry.ClientID + "').value == 'US')\n";
            scriptString += " {document.getElementById('" + divPostalCode.ClientID + "').style.display = 'block';\n";
            scriptString += " } else { \n";
            scriptString += "  document.getElementById('" + divPostalCode.ClientID + "').style.display = 'none';\n";
            scriptString += "}}\n";
            scriptString += "CountryCheck();</script>";

            if (!ClientScript.IsClientScriptBlockRegistered("CountryCheck"))
            {
                ClientScript.RegisterStartupScript(this.GetType(), "CountryCheck", scriptString);
            }

            bool showingSpecificPromise = false;

            // If we're posting from the signup form on the homepage we need to 
            //fill form values and attempt to process the post
            if (!string.IsNullOrEmpty(Request.Form["postFromHome"]))
            {
                PrimeFormValuesFromHomepagePost();
                RegisterNewUser();
            }
            // If we're not posting from the homepage, try to load a promise
            else if (!string.IsNullOrEmpty(Request.Params["lpage"]))
            {
                string landingPageType = Request.Params["lpage"];

                if (landingPageType.Equals("profile") && !string.IsNullOrEmpty(Request.Params["uname"]))
                {
                    LoadProfilePromise(Request.Params["uname"]);
                    showingSpecificPromise = true;
                }
                else if ((landingPageType.Equals("mediafull") || landingPageType.Equals("mediafull")) && !string.IsNullOrEmpty(Request.Params["mediatype"]))
                {
                    LoadMediaPromise(Request.Params["mediatype"]);
                    showingSpecificPromise = true;
                }
            }

            // If not showing specific promise, show default
            if (!showingSpecificPromise)
            {
                LoadDefaultPromise();
            }

            // Assume input UserName is valid until proven otherwise
            uNameValid = true;
        }

        #region Helper Methods

        /// <summary>
        /// Display a default promise if not coming from specific source.
        /// </summary>
        private void LoadDefaultPromise()
        {
            divPromiseContainer.Visible = false;
            divDefaultPromise.Visible = true;
            h1PromiseMessage.InnerText = "Join Kaneva";
        }
        /// <summary>
        /// Display a default promise if not coming from specific source.
        /// </summary>
        private void LoadMediaPromise(string mediaType)
        {
            imgPromise.Src = "~/images/landing_page/promise_types.png";
            imgPromise.Width = 129;
            imgPromise.Height = 46;

            switch (mediaType)
            {
                case "music":
                    h1PromiseMessage.InnerText = "Sign up for Kaneva and listen to the latest raved music.";
                    imgPromise.Attributes["class"] += " musicUpsell ";
                    divPromiseImageFrame.Attributes["class"] += " musicUpsell ";
                    break;
                case "video":
                    h1PromiseMessage.InnerText = "Sign up for Kaneva and get access to the most popular videos.";
                    imgPromise.Attributes["class"] += " videoUpsell ";
                    divPromiseImageFrame.Attributes["class"] += " videoUpsell ";
                    break;
                case "image":
                    h1PromiseMessage.InnerText = "Sign up for Kaneva and get access to over 100,000 photos.";
                    imgPromise.Attributes["class"] += " imageUpsell ";
                    divPromiseImageFrame.Attributes["class"] += " imageUpsell ";
                    break;
                default:
                    imgPromise.Visible = false;
                    break;
            }
        }

        /// <summary>
        /// Initialize promise container with user profile information.
        /// </summary>
        /// <param name="userName">The username of the user to pull data from.</param>
        private void LoadProfilePromise(string userName)
        {
            User user = GetUserFacade.GetUser(userName);
            string imagePath = "";
            if (user.MatureProfile && !KanevaWebGlobals.CurrentUser.HasAccessPass)
            {
                imagePath += KanevaGlobals.ImageServer;
                imagePath += user.Gender.ToUpper().Equals("M") ? "/KanevaIconMale_sm.gif" : "/KanevaIconFemale_sm.gif";
            }
            else
            {
                imagePath = GetProfileImageURL(user.ThumbnailSmallPath, "sm", user.Gender, user.FacebookSettings.UseFacebookProfilePicture, user.FacebookSettings.FacebookUserId);
            }
            imgPromise.Attributes["class"] += " profileUpsell ";
            divPromiseImageFrame.Attributes["class"] += " profileUpsell ";
            imgPromise.Src = imagePath;
            imgPromise.Width = 50;
            imgPromise.Height = 50;

            h1PromiseMessage.InnerHtml = "Sign up for Kaneva to become friends with <span class=\"userNameWrapper\">" + user.DisplayName + "</span>";
        }

        /// <summary>
        /// Validate licensing
        /// </summary>
        /// <param name="source"></param>
        /// <param name="args"></param>
        protected void LicenseValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = this.chkCopyright.Checked;
        }

        /// <summary>
        /// Populate the drpYears dropdown.
        /// </summary>
        private void LoadYears()
        {
            int startYear = Convert.ToInt16(DateTime.Now.Year) - 13;

            drpYear.Items.Add(new ListItem("> " + startYear.ToString(), (startYear + 1).ToString()));

            int currentYear = Convert.ToInt16(DateTime.Now.Year) - 13;

            for (int i = currentYear; i > 1940; i--)
            {
                drpYear.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
            drpYear.Items.Add(new ListItem("< 1940", "1940"));
            drpYear.SelectedIndex = 0;
        }

        /// <summary>
        /// Show/hide the captcha control as needed
        /// </summary>
        private void ShowCaptcha()
        {
            string visitorIP = Common.GetVisitorIPAddress();
            DataTable dtUsers = UsersUtility.GetUsersByIp(visitorIP, KanevaGlobals.CaptchaRegistrationWindow);

            if (dtUsers.Rows.Count == 0)
            {
                CaptchaControl1.Visible = false;
                captchaContainer.Visible = false;
            }
        }

        /// <summary>
        /// Check to determine if captcha is needed for this signup
        /// </summary>
        /// <returns></returns>
        private bool EnforceCaptcha()
        {
            bool enforce = true;
            string visitorIP = Common.GetVisitorIPAddress();
            DataTable dtUsers = UsersUtility.GetUsersByIp(visitorIP, KanevaGlobals.CaptchaRegistrationWindow);

            // Disable captcha if it's not necessary, of if using speedy signup on Preview or Dev
            if (dtUsers.Rows.Count == 0 ||
               (SpeedySignupEnabled && (KanevaGlobals.WokGameId == 3298 || KanevaGlobals.WokGameId == 5316)))
            {
                enforce = false;
            }
            return enforce;
        }

        /// <summary>
        /// Adds a users first time visit source information
        /// </summary>
        public void AddUserAcquisitionSourceURL(HttpRequest request, int userId)
        {
            // Get the cookie from the request object
            HttpCookie firstRefCookie = request.Cookies[Constants.ACQUISITION_COOKIE_NAME];

            // Get the values from the cookie
            string host = KanevaGlobals.Decrypt(firstRefCookie[Constants.ACQUISITION_COOKIE_VALUE_HOST]);
            string fullUrl = KanevaGlobals.Decrypt(firstRefCookie[Constants.ACQUISITION_COOKIE_VALUE_ABSOLUTEURI]);
            int sourceTypeId = (int)Constants.eACQUISITION_SOURCE_TYPE.OTHER_SITE;

            // Check to see if source if from google
            if (host.ToLower().Contains("www.google."))
            {
                // Did user find kaneva from a paid keyword?
                if (request.QueryString[Constants.ACQUISITION_QUERYSTRING_GOOGLE_PAID_KEYWORD] != null)
                {
                    sourceTypeId = (int)Constants.eACQUISITION_SOURCE_TYPE.GOOGLE_PAID_KEYWORD;
                }
                else
                {   // Must have been a regular google search
                    sourceTypeId = (int)Constants.eACQUISITION_SOURCE_TYPE.GOOGLE_SEARCH;
                }
            } // See if user came from a referral banner add on another site
            else if (request.QueryString[Constants.ACQUISITION_QUERYSTRING_REFERRAL] != null)
            {
                sourceTypeId = (int)Constants.eACQUISITION_SOURCE_TYPE.REFERRAL;
            }

            // Add source url data to database
            UserFacade userFacade = new UserFacade();
            int ret = userFacade.InsertUserSourceAcquisitionUrl(userId, fullUrl, host, sourceTypeId);

            // Check to see if insert failed without throwing an exception
            if (ret == 0)
            {
                m_logger.Error("Error adding user acquisition source info. UserId=" + userId + ". Return code=0.");
            }
        }

        /// <summary>
        /// Set form fields based on Facebook data.
        /// </summary>
        private void PrimeFormValuesFromFacebook()
        {
            if (Request["accessToken"] != null && Request["accessToken"].ToString().Length > 0)
            {
                try
                {
                    string accessToken = Request["accessToken"].ToString();
                    bool isError = false;
                    string errMsg = "";
                    FacebookUser fbuser = GetSocialFacade.GetCurrentUser(accessToken, out isError, out errMsg);

                    if (fbuser.Id > 0)
                    {
                        // Check to see if this FB account is already linked to a Kaneva account
                        // If so then we log them in with that connected account
                        if (GetUserFacade.IsFacebookUserAlreadyConnected(fbuser.Id))
                        {
                            Response.Redirect(GetLoginURL());
                        }

                        // Store access token in viewstate
                        this.FBAccessToken = accessToken;

                        txtFirstName.Text = fbuser.First_Name;
                        txtLastName.Text = fbuser.Last_Name;
                        txtEmail.Text = fbuser.Email;

                        // Hide field description labels
                        txtFirstName_lbl.Style.Add("display", "none");
                        txtLastName_lbl.Style.Add("display", "none");
                        txtEmail_lbl.Style.Add("display", "none");

                        imgFBProfilePic.Src = "https://graph.facebook.com/" + fbuser.Id + "/picture";
                        divFBProfilePic.Visible = true;
                        if (fbuser.Gender.Length > 0)
                        {
                            drpGender.SelectedValue = fbuser.Gender;
                        }

                        if (fbuser.Birthday.Length > 0)
                        {
                            try
                            {
                                DateTime dt = DateTime.Parse(fbuser.Birthday);
                                drpMonth.SelectedValue = dt.Month.ToString();
                                drpDay.SelectedValue = dt.Day.ToString();
                                drpYear.SelectedValue = dt.Year.ToString();
                            }
                            catch { }
                        }

                        imgFBSignup.Visible = false;
                        pFBSignupMessage.Visible = false;
                    }

                    // Set the the url in viewstate so the user will be directed to the find friends page after registration
                    ViewState["rurl"] = KanevaGlobals.PostFacebookRegistrationLandingPage;

                    if (Request.UrlReferrer.AbsolutePath.Contains("kanevainvite.aspx"))
                    {
                        if (Request.UrlReferrer.Query.Contains("kuid"))
                        {
                            string kuid = Request.UrlReferrer.Query.Replace("?kuid=", "");
                            if (kuid.Length > 0)
                            {
                                ViewState["FacebookInviteSentByUserId"] = kuid;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    m_logger.Error("Error trying to set up load Facebook values for registration. ", ex);

                    // Reset the values
                    txtFirstName.Text = string.Empty;
                    txtLastName.Text = string.Empty;
                    txtEmail.Text = string.Empty;
                    imgFBProfilePic.Src = string.Empty;
                    divFBProfilePic.Visible = false;
                    drpGender.SelectedValue = string.Empty;
                    drpMonth.SelectedValue = string.Empty;
                    drpDay.SelectedValue = string.Empty;
                    drpYear.SelectedValue = string.Empty;
                    imgFBSignup.Visible = true;
                    pFBSignupMessage.Visible = true;

                    // Display error
                    cvBlank.IsValid = false;
                    cvBlank.ErrorMessage = "We encountered an error retrieving your Facebook information. " +
                        " Click the Facebook button above.";
                }

                ScriptManager.RegisterStartupScript(this, GetType(), "FBCheck", "ValidateAllFields(true, true, true);", true);
            }
        }

        /// <summary>
        /// Sets form fields based on the posted values from the signup form on the homepage
        /// </summary>
        private void PrimeFormValuesFromHomepagePost()
        {
            txtUserName.Text = Request.Form[Request.Form["txtUserNameUID"]];
            txtFirstName.Text = Request.Form[Request.Form["txtFirstNameUID"]];
            txtLastName.Text = Request.Form[Request.Form["txtLastNameUID"]];
            txtEmail.Text = Request.Form[Request.Form["txtEmailUID"]];
            txtPassword.Text = Request.Form[Request.Form["txtPasswordUID"]];
            drpGender.SelectedValue = Request.Form[Request.Form["drpGenderUID"]];

            if (drpGender.SelectedValue != null && drpGender.SelectedValue != "")
            {
                if (drpGender.SelectedValue.ToLower() == "female")
                {
                    radFemale.Checked = true;
                }
                else if (drpGender.SelectedValue.ToLower() == "male")
                {
                    radMale.Checked = true;
                }
            }

            drpMonth.SelectedValue = Request.Form[Request.Form["drpMonthUID"]];
            drpDay.SelectedValue = Request.Form[Request.Form["drpDayUID"]];
            drpYear.SelectedValue = Request.Form[Request.Form["drpYearUID"]];
            drpCountry.SelectedValue = Request.Form[Request.Form["drpCountryUID"]];
            txtPostalCode.Text = Request.Form[Request.Form["txtPostalCodeUID"]];
        }

        /// <summary>
        /// Auto-fills form fields if speedy-testing
        /// </summary>
        private void PrimeFormValuesForSpeedySignup()
        {
            // Find the next available username
            string username = Request.QueryString["username"];

            if (!string.IsNullOrWhiteSpace(username))
            {
                txtUserName.Text = username;
                txtFirstName.Text = username;
                txtLastName.Text = "TestUser";
                txtEmail.Text = username + "@kaneva.com";
                radMale.Checked = true;
                drpMonth.SelectedValue = "1";
                drpDay.SelectedValue = "1";
                drpYear.SelectedValue = "1980";
                drpCountry.SelectedValue = "US";
                txtPostalCode.Text = "12345";

                ScriptManager.RegisterStartupScript(this, GetType(), "Check", "ValidateAllFields(true, true, true);", true);
            }
        }

        /// <summary>
        /// Initialize Facebook Connect
        /// </summary>
        /// <param name="userId"></param>
        private void SetUserFacebookConnectValues(int userId)
        {
            try
            {
                if (this.FBAccessToken.Length > 0 && divFBProfilePic.Visible && userId > 0)
                {
                    bool isError = false;
                    string errMsg = "";
                    FacebookUser fbuser = GetSocialFacade.GetCurrentUser(this.FBAccessToken, out isError, out errMsg);

                    if (fbuser.Id > 0)
                    {
                        GetUserFacade.ConnectUserToFacebook(userId, fbuser.Id, rbUseFBProfilePicYes.Checked, this.FBAccessToken);

                        if (rbUseFBProfilePicYes.Checked)
                        {
                            // User completed fame packet
                            try
                            {
                                GetFameFacade.RedeemPacket(KanevaWebGlobals.GetUserId(), (int)PacketId.WEB_OT_ADD_PICTURE, (int)FameTypes.World);
                            }
                            catch (Exception ex)
                            {
                                m_logger.Error("Error awarding Packet WEB_OT_ADD_PICTURE, userid=" + KanevaWebGlobals.GetUserId(), ex);
                            }
                        }
                    }
                }
            }
            catch { }
        }

        /// <summary>
        /// A/B testing code.  Still needed?
        /// </summary>
        /// <param name="userId"></param>
        private void CheckExperimentParticipation(int userId)
        {
            string abExperimentId = string.Empty;
            string abExperimentGroupId = string.Empty;

            if (Request["ABExpID"] != null && Request["ABExpGrpID"] != null)
            {
                try
                {
                    abExperimentId = Request["ABExpID"].ToString();
                    abExperimentGroupId = Request["ABExpGrpID"].ToString();
                }
                catch (Exception)
                {
                    abExperimentId = string.Empty;
                    abExperimentGroupId = string.Empty;
                }
            }

            // Assign values for the internal A/B test so we can track user groups as related to their user group
            // associated with the Google A/B test for the registration page with inline errors
            string abRegistrationExperimentId = "d1ecf071-3a05-11e5-858d-f6e07fdbcf79";
            string abRegistrationExperimentGroupId = "5b34d6ae-3a08-11e5-858d-f6e07fdbcf79";  // Group C - Variant

            if (userId > 0)
            {
                IList<Experiment> experiments;

                // Get experiments not assigned at startup, assign via URL only here
                if (!string.IsNullOrWhiteSpace(abExperimentId))
                {
                    experiments = GetExperimentFacade.GetAvailableExperimentsByUserContext(userId, Experiment.ASSIGN_ON_REQUEST, false);

                    foreach (Experiment experiment in experiments)
                    {
                        // Assign based on URL???
                        if (experiment.ExperimentId.Equals(abExperimentId) && !string.IsNullOrWhiteSpace(abExperimentGroupId))
                        {
                            GetExperimentFacade.SaveUserExperimentGroup(new UserExperimentGroup(string.Empty, userId, abExperimentGroupId, DateTime.Now, null, true, string.Empty));
                        }
                    }
                }


                // Get experiments marked as assigned at startup
                experiments = GetExperimentFacade.GetAvailableExperimentsByUserContext(userId, Experiment.ASSIGN_ON_CREATION_ONLY);

                foreach (Experiment experiment in experiments)
                {
                    // Assign based on URL???
                    if (experiment.ExperimentId.Equals(abExperimentId) && !string.IsNullOrWhiteSpace(abExperimentGroupId))
                    {
                        GetExperimentFacade.SaveUserExperimentGroup(new UserExperimentGroup(string.Empty, userId, abExperimentGroupId, DateTime.Now, null, true, string.Empty));
                    }
                    // Auto assign group based on which Google A/B group the user was in
                    else if (experiment.ExperimentId.Equals(abRegistrationExperimentId) && !string.IsNullOrWhiteSpace(abRegistrationExperimentGroupId))
                    {
                        GetExperimentFacade.SaveUserExperimentGroup(new UserExperimentGroup(string.Empty, userId, abRegistrationExperimentGroupId, DateTime.Now, null, true, string.Empty));
                    }
                    else
                    {
                        // Get random group
                        string assignedGroup = GetExperimentFacade.AssignParticipant(experiment);

                        if (!string.IsNullOrWhiteSpace(assignedGroup))
                        {
                            GetExperimentFacade.SaveUserExperimentGroup(new UserExperimentGroup(string.Empty, userId, assignedGroup, DateTime.Now, null, false, string.Empty));
                        }
                    }
                }
            }
        }

        private bool ValidateRegistrationValues()
        {
            bool isValid = true;

            // Postal Code
            if (drpCountry.SelectedValue == "US")
            {
                rftxtPostalCode.Enabled = true;
            }
            else
            {
                rftxtPostalCode.Enabled = false;
            }

            Page.Validate();

            // Check validation controls
            if (!Page.IsValid || !ValidateUserName())
            {
                isValid = false;
            }

            bool valUsername = true, valFirstname = true, valLastname = true;
            if (KanevaWebGlobals.isTextRestricted(txtUserName.Text.Trim(), Constants.eRESTRICTION_TYPE.POTTY_MOUTH, false))
            {
                username_msg.InnerText = "Do not use restricted words.";
                username_msg.Attributes.Add("class", "msg single");
                txtUserName.CssClass += " validate-err";
                txtUserName_validateimg.Style.Add("display", "block");
                txtUserName_lbl.Style.Add("display", "none");
                valUsername = false;
                isValid = false;
            }
            if (KanevaWebGlobals.isTextRestricted(txtFirstName.Text.Trim(), Constants.eRESTRICTION_TYPE.POTTY_MOUTH, false))
            {
                firstname_msg.InnerText = "Do not use restricted words.";
                txtFirstName.CssClass += " validate-err";
                txtFirstName_validateimg.Style.Add("display", "block");
                txtFirstName_lbl.Style.Add("display", "none");
                valFirstname = false;
                isValid = false;
            }
            if (KanevaWebGlobals.isTextRestricted(txtLastName.Text.Trim(), Constants.eRESTRICTION_TYPE.POTTY_MOUTH, false))
            {
                lastname_msg.InnerText = "Do not use restricted words.";
                txtLastName.CssClass += " validate-err";
                txtLastName_validateimg.Style.Add("display", "block");
                txtLastName_lbl.Style.Add("display", "none");
                valLastname = false;
                isValid = false;
            }

            //compile users birthday
            try
            {
                dtBirthDate = new DateTime(Convert.ToInt32(drpYear.SelectedValue), Convert.ToInt32(drpMonth.SelectedValue), Convert.ToInt32(drpDay.SelectedValue));

                //check to see if applicant is a minor before proceeding
                if (IsUserAMinor(dtBirthDate))
                {
                    isValid = false;
                }
            }
            catch
            {
                isValid = false;
            }

            if (EnforceCaptcha())
            {
                if (!CaptchaControl1.Validate(CaptchaCodeTextBox.Text))
                {
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "CaptchaErr", "ValidateCaptcha($j('.signupForm .captcha input[type=\"text\"]'));", true);
                    isValid = false;
                }
            }

            if (!isValid)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "UsernameErr", "ValidateAllFields(" + valUsername.ToString().ToLower() + "," + valFirstname.ToString().ToLower() + "," + valLastname.ToString().ToLower() + ");", true);
            }

            return isValid;
        }

        /// <summary>
        /// Called on form postback to process input and register a new user.
        /// Method contents were originally part of btnRegister_OnClick() and were
        /// separated so they can be used multiple places.
        /// </summary>
        private void RegisterNewUser()
        {
            cvBlank.IsValid = true;
            cvBlank.ErrorMessage = string.Empty;

            // Validate form values again, if fail then return           
            if (!ValidateRegistrationValues())
            {
                return;
            }

            // Check to see if users IP address has been banned
            try
            {
                DataRow drIPBan = UsersUtility.GetIPBan(Common.GetVisitorIPAddress());
                if (drIPBan != null && drIPBan.Table.Rows.Count > 0)
                {
                    // Now check to see if account is still banned
                    if (Convert.ToDateTime(drIPBan["ban_end_date"]) < DateTime.Now)
                    {   // Ban has expired, un-ban ip and continue
                        UsersUtility.RemoveIPBan(Common.GetVisitorIPAddress());
                    }
                    else
                    {   // Ban still active, show
                        cvBlank.IsValid = false;
                        cvBlank.ErrorMessage = "<div style=\"padding:20px 0 10px 0;\"><b>Registration failed:</b> This account has been locked by the Kaneva administrator.</div>";
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                m_logger.Error("Error checking user IP against Banned IP List: ", ex);
            }

            // If user is currently logged in, log them out before continuing, if not, 
            // user gets redirected to the wrong page and it is a dead end for them.
            if (Request.IsAuthenticated)
            {
                System.Web.HttpCookie aCookie;
                string bannerCookieName = KanevaWebGlobals.GetBannerCookieName();
                int limit = Request.Cookies.Count - 1;
                for (int i = Request.Cookies.Count - 1; i != 0; i--)
                {
                    aCookie = Request.Cookies[i];
                    if (aCookie.Name != bannerCookieName)
                    {
                        aCookie.Expires = DateTime.Now.AddDays(-1);
                        Response.Cookies.Add(aCookie);
                    }
                }

                FormsAuthentication.SignOut();
                Session.Abandon();
            }

            int source_community_id = 0;
            try
            {
                source_community_id = Convert.ToInt32(Request.QueryString["joinid"]);
            }
            catch { }

            int communityInviteId = 0;
            try
            {
                if (Request["cInv"] != null)
                {
                    communityInviteId = Convert.ToInt32(Request.QueryString["cInv"]);
                }
            }
            catch { }

            string strUserName = Server.HtmlEncode(txtUserName.Text.Trim());
            string strPassword = Server.HtmlEncode(txtPassword.Text.Trim());

            // Hash password
            byte[] salt = new byte[9];
            new RNGCryptoServiceProvider().GetBytes(salt);
            string hashPassword = FormsAuthentication.HashPasswordForStoringInConfigFile(UsersUtility.MakeHash(strPassword + strUserName.ToLower()) + Convert.ToBase64String(salt), "MD5");

            // Generate a unique key_code for registration purposes
            string keyCode = KanevaGlobals.GenerateUniqueString(20);

            // Create a registration Key
            string regKey = KanevaGlobals.GenerateUniqueString(50);

            // If they were invited from a friend, no need to send out email.
            bool bInvitedByFriend = (Request["i"] != null);
            bool bInvitedViaFacebook = (Request["fb"] != null);

            string gender = drpGender.SelectedValue;

            // Set the default Display Name
            string displayName = "";

            // If user is under 18, set display name to first name and last initial only
            if (DateTime.Today.AddYears(-18) < dtBirthDate)
            {
                displayName = String.Concat(Server.HtmlEncode(txtFirstName.Text), " ", Server.HtmlEncode(txtLastName.Text).Substring(0, 1));
            }
            else
            {
                int displayNameLen = (Server.HtmlEncode(txtFirstName.Text.Trim()) + Server.HtmlEncode(txtLastName.Text.Trim())).Length;
                if (displayNameLen >= 30)
                {
                    if (displayNameLen == 30)
                    {
                        displayName = String.Concat(Server.HtmlEncode(txtFirstName.Text), Server.HtmlEncode(txtLastName.Text));
                    }
                    else
                    {
                        displayName = String.Concat(Server.HtmlEncode(txtFirstName.Text) + Server.HtmlEncode(txtLastName.Text)).Substring(0, 29);
                    }
                }
                else
                {
                    displayName = String.Concat(Server.HtmlEncode(txtFirstName.Text), " ", Server.HtmlEncode(txtLastName.Text));
                }
            }

            int result = 0;
            int userId = 0;

            // Make sure it is not on the invalid user names list
            if (UsersUtility.IsReservedName(strUserName))
            {
                result = -3;
            }
            else
            {
                // This returns userId
                userId = GetUserFacade.InsertUser(strUserName, hashPassword, Convert.ToBase64String(salt), 2, 1,
                    Server.HtmlEncode(txtFirstName.Text), Server.HtmlEncode(txtLastName.Text), displayName, Server.HtmlEncode(gender.Substring(0,1)), "", Server.HtmlEncode(txtEmail.Text), dtBirthDate, keyCode,
                    Server.HtmlEncode(drpCountry.SelectedValue), Server.HtmlEncode(txtPostalCode.Text), regKey, Common.GetVisitorIPAddress(), (int)Kaneva.Constants.eUSER_STATUS.REGNOTVALIDATED, source_community_id);

                // Did we get a success?
                if (userId > 0)
                {
                    result = 0;

                    // If user signed up with Facebook, add Facebook values
                    SetUserFacebookConnectValues(userId);

                    // Prepare Single Sign-On
                    GetUserFacade.SetupSingleSignOn(userId, Server.HtmlEncode(txtEmail.Text), strPassword);

                    // Add user's useragent info from browser
                    try
                    {
                        Parser uaParser = Parser.GetDefault();
                        ClientInfo c = uaParser.Parse(Request.UserAgent);
                        GetUserFacade.InsertUserAgentData(userId, c.OS.ToString(), c.UserAgent.ToString(), Request.UserAgent);
                    }
                    catch (Exception ex)
                    {
                        m_logger.Error("Error adding user agent info. UserId=" + userId + ". ", ex);
                    }
                }
                else
                {
                    result = userId;
                }
            }

            switch (result)
            {
                case 0:
                    {
                        DataTable dtJoinCommunities = null;
                        DataRow drJoin = null;
                        User user = new User();

                        // Check to see if user has acquisition cookie, if so, then let's log the info
                        // about where the user originally came from
                        try
                        {
                            if (Request.Cookies[Constants.ACQUISITION_COOKIE_NAME] != null && Request.Cookies[Constants.ACQUISITION_COOKIE_NAME].Value != string.Empty)
                            {
                                // Update the database
                                AddUserAcquisitionSourceURL(Page.Request, userId);
                            }
                        }
                        catch (Exception ex)
                        {
                            m_logger.Error("Error adding user acquisition source info. UserId=" + userId + ". ", ex);
                        }

                        // Create their home page
                        if (GetUserFacade.CreateDefaultUserHomePage(strUserName, userId) && GetUserFacade.CreateDefaultUserHomeWorld(strUserName, userId))
                        {
                            // home page was built successfullly
                            //get default kaneva community
                            string defaultCommunityId = System.Configuration.ConfigurationManager.AppSettings["Default_Kaneva_Community_Id"];
                            if (defaultCommunityId != null && defaultCommunityId.Trim() != "")
                            {
                                int communityId = Convert.ToInt32(defaultCommunityId); //logic to prevent convrt exceptions

                                //auto join the user to the designated kaneva community
                                CommunityMember member = new CommunityMember();
                                member.CommunityId = communityId;
                                member.UserId = userId;
                                member.AccountTypeId = (int)CommunityMember.CommunityMemberAccountType.SUBSCRIBER;
                                member.Newsletter = "Y";
                                member.InvitedByUserId = 0;
                                member.AllowAssetUploads = "N";
                                member.AllowForumUse = "Y";
                                member.StatusId = (int)CommunityMember.CommunityMemberStatus.ACTIVE;

                                GetCommunityFacade.InsertCommunityMember(member);

                            }

                            // Are they coming from a community Invite?
                            if (communityInviteId > 0)
                            {
                                CommunityMember member = new CommunityMember();
                                member.CommunityId = communityInviteId;
                                member.UserId = userId;
                                member.AccountTypeId = (int)CommunityMember.CommunityMemberAccountType.SUBSCRIBER;
                                member.Newsletter = "Y";
                                member.InvitedByUserId = 0;
                                member.AllowAssetUploads = "N";
                                member.AllowForumUse = "Y";
                                member.StatusId = (int)CommunityMember.CommunityMemberStatus.ACTIVE;

                                GetCommunityFacade.InsertCommunityMember(member);
                            }

                            // Now check to see if there are any other communities the user needs to be joined into
                            // based on the join profile for the source community
                            if (source_community_id > 0)
                            {
                                //get other communities the new user is supposed to be auto joined to
                                dtJoinCommunities = CommunityUtility.GetJoinCommunities(source_community_id);

                                //automatically add them as active members of these communities
                                if (dtJoinCommunities != null)
                                {
                                    int join_comm_id = 0;

                                    for (int i = 0; i < dtJoinCommunities.Rows.Count; i++)
                                    {
                                        try
                                        {
                                            join_comm_id = Convert.ToInt32(dtJoinCommunities.Rows[i]["join_community_id"]);
                                            if (join_comm_id > 0)
                                            {
                                                CommunityMember member = new CommunityMember();
                                                member.CommunityId = join_comm_id;
                                                member.UserId = userId;
                                                member.AccountTypeId = (int)CommunityMember.CommunityMemberAccountType.SUBSCRIBER;
                                                member.Newsletter = "Y";
                                                member.InvitedByUserId = 0;
                                                member.AllowAssetUploads = "N";
                                                member.AllowForumUse = "Y";
                                                member.StatusId = (int)CommunityMember.CommunityMemberStatus.ACTIVE;

                                                GetCommunityFacade.InsertCommunityMember(member);
                                            }
                                        }
                                        catch
                                        { }
                                    }
                                }

                                // Get the join settings for the source community
                                drJoin = CommunityUtility.GetJoinConfig(source_community_id);

                                //if there are particular join settings set them
                                if (drJoin != null)
                                {
                                    try
                                    {
                                        // get the new user account just created
                                        user = GetUserFacade.GetUser(userId);
                                        Community newProfile = GetCommunityFacade.GetCommunity(user.CommunityId);

                                        string avatarPath = string.Empty;

                                        // check to see if there is a default theme to apply to the new users personal pages
                                        try
                                        {
                                            if (user.Gender == "F")
                                            {
                                                avatarPath = drJoin["user_profile_female_avatar_path"].ToString();
                                            }
                                            else
                                            {
                                                avatarPath = drJoin["user_profile_male_avatar_path"].ToString();
                                            }
                                        }
                                        catch { }

                                        // apply avatar if one exists for this join
                                        if (avatarPath != string.Empty && avatarPath.Length > 0)
                                        {
                                            string fileStoreHack = "";
                                            if (KanevaGlobals.FileStoreHack != "")
                                            {
                                                fileStoreHack = KanevaGlobals.FileStoreHack;
                                            }

                                            string imagePath = KanevaGlobals.ContentServerPath + System.IO.Path.DirectorySeparatorChar + avatarPath;
                                            string avatar_sm = avatarPath.Substring(0, avatarPath.IndexOf(".")) + "_sm" + avatarPath.Substring(avatarPath.IndexOf("."));
                                            string avatar_me = avatarPath.Substring(0, avatarPath.IndexOf(".")) + "_me" + avatarPath.Substring(avatarPath.IndexOf("."));
                                            string avatar_lg = avatarPath.Substring(0, avatarPath.IndexOf(".")) + "_la" + avatarPath.Substring(avatarPath.IndexOf("."));
                                            string avatar_xl = avatarPath.Substring(0, avatarPath.IndexOf(".")) + "_xl" + avatarPath.Substring(avatarPath.IndexOf("."));

                                            //update image paths
                                            newProfile.ThumbnailPath = imagePath;
                                            newProfile.ThumbnailType = string.Empty;
                                            newProfile.HasThumbnail = true; //originally was false but this logic seems to be setting a thumbnail so changing to true
                                            newProfile.ThumbnailSmallPath = avatar_sm;
                                            newProfile.ThumbnailMediumPath = avatar_me;
                                            newProfile.ThumbnailLargePath = avatar_lg;
                                            newProfile.ThumbnailXlargePath = avatar_xl;

                                            GetCommunityFacade.UpdateCommunity(newProfile);
                                        }
                                    }
                                    catch (Exception Exc)
                                    {
                                        m_logger.Error("Error in reg", Exc);
                                    }
                                }
                                //todo do we need to set a default profile for the user here??
                            }
                        }
                        else
                        {
                            // Problem creating default home page
                            GetUserFacade.CleanUpRegistrationIssue(strUserName);

                            cvBlank.IsValid = false;
                            cvBlank.ErrorMessage = "The default home page for this user could not be created.";
                            return;
                        }

                        // Send out verification email
                        if (!bInvitedByFriend && !Configuration.AutoValidateRegistrationEmail)
                        {
                            if (Request.QueryString["joinid"] != null && Request.QueryString["joinid"].ToString().Length > 0)
                            {
                                string email_body_top = "";
                                string email_body_bottom = "";
                                string subject = "";
                                int community_id = 0;

                                if (Request.QueryString["joinid"] != null)
                                {
                                    try
                                    {
                                        community_id = Convert.ToInt32(Request.QueryString["joinid"]);

                                        drJoin = CommunityUtility.GetJoinConfig(community_id);
                                    }
                                    catch { }
                                }

                                if (drJoin != null)
                                {
                                    if (drJoin["email_body_top"] != null && drJoin["email_body_top"].ToString().Length > 0)
                                    {
                                        email_body_top = drJoin["email_body_top"].ToString();
                                    }
                                    if (drJoin["email_body_bottom"] != null && drJoin["email_body_bottom"].ToString().Length > 0)
                                    {
                                        email_body_bottom = drJoin["email_body_bottom"].ToString();
                                    }
                                    if (drJoin["email_subject"] != null && drJoin["email_subject"].ToString().Length > 0)
                                    {
                                        subject = drJoin["email_subject"].ToString();
                                    }

                                    // set the return url
                                    string retURL = string.Empty;
                                    if (Request.QueryString["redeem"] != null)
                                    {
                                        retURL += "redeem";
                                    }

                                    string joinId = "&joinid=" + Request.QueryString["joinid"].ToString();

                                    // send validation email				 
                                    try
                                    {
                                        MailUtilityWeb.SendRegistrationEmailCoBrand(
                                            email_body_top,
                                            email_body_bottom,
                                            subject,
                                            Server.HtmlEncode(txtEmail.Text), strUserName, keyCode, Server.UrlEncode(retURL) + joinId);
                                    }
                                    catch { }
                                }
                            }
                            else if (Request.QueryString["redeem"] != null)
                            //if (ViewState ["redeem"] != null && Convert.ToBoolean (ViewState ["redeem"]) == true)
                            {
                                try
                                {
                                    // if we came from the redeem gift card page, make sure the link in 
                                    // the email will redirect the user back into the redeem flow
                                    MailUtilityWeb.SendRegistrationEmail(strUserName, Server.HtmlEncode(txtEmail.Text), keyCode, "redeem");
                                }
                                catch { }
                            }
                            else
                            {
                                try
                                {
                                    MailUtilityWeb.SendRegistrationEmail(strUserName, Server.HtmlEncode(txtEmail.Text), keyCode);
                                }
                                catch { }
                            }
                        }

                        // If they were invited by a friend, glue them here, and give them any free points
                        else if (bInvitedByFriend)
                        {
                            // Automatically set thier status since we know they clicked the invite email
                            GetUserFacade.UpdateUserStatus(userId, (int)Constants.eUSER_STATUS.REGVALIDATED);

                            // since invited by friend, user email is automatically validated, so award fame
                            try
                            {
                                GetFameFacade.RedeemPacket(userId, (int)PacketId.WEB_OT_EMAIL_VALIATION, (int)FameTypes.World);
                            }
                            catch (Exception ex)
                            {
                                m_logger.Error("Error awarding Packet WEB_OT_EMAIL_VALIATION, userid=" + userId, ex);
                            }

                            DataRow drInvite = UsersUtility.GetInvite(Convert.ToInt32(Request["i"]), Request["k"].ToString());

                            // Give the user the free Reward Points for this user registering
                            // Only do the following if status is UNREGISTER so they cannot do it more than once!!!
                            if (drInvite != null && Convert.ToInt32(drInvite["invite_status_id"]).Equals((int)Constants.eINVITE_STATUS.UNREGISTER))
                            {
                                int inviteSentByUserId = Convert.ToInt32(drInvite["user_id"]);

                                int channelId = drInvite["channel_id"] == DBNull.Value ? -1 :
                                    Convert.ToInt32(drInvite["channel_id"]);

                                if (channelId > 0)
                                {
                                    //if there is channelId, add user as a member to the channel
                                    CommunityMember member = new CommunityMember();
                                    member.CommunityId = channelId;
                                    member.UserId = userId;
                                    member.AccountTypeId = (int)CommunityMember.CommunityMemberAccountType.SUBSCRIBER;
                                    member.Newsletter = "N";
                                    member.InvitedByUserId = inviteSentByUserId;
                                    member.AllowAssetUploads = "N";
                                    member.AllowForumUse = "Y";
                                    member.StatusId = (int)CommunityMember.CommunityMemberStatus.ACTIVE;

                                    GetCommunityFacade.InsertCommunityMember(member);

                                    UsersUtility.InsertUserConnection(channelId, userId, inviteSentByUserId);
                                }
                                else
                                {
                                    //add user as a personal friend								
                                    int ret = GetUserFacade.AcceptFriend(userId, inviteSentByUserId);

                                    if (ret == 1)
                                    {
                                        // If you wants emails when new friends are added, send them an email
                                        if (GetUserFacade.GetUser(inviteSentByUserId).NotifyNewFriends)
                                        {
                                            try
                                            {
                                                MailUtilityWeb.SendFriendAcceptedInviteNotification(inviteSentByUserId, userId);
                                            }
                                            catch { }
                                        }
                                    }
                                }

                                UsersUtility.UpdateInvite(Convert.ToInt32(drInvite["invite_id"]), (int)Constants.eINVITE_STATUS.REGISTERED, userId);

                                // User completed fame packet
                                try
                                {
                                    GetFameFacade.RedeemPacket(inviteSentByUserId, (int)PacketId.WEB_INVITE_JOIN_SITE, (int)FameTypes.World);
                                }
                                catch (Exception ex)
                                {
                                    m_logger.Error("Error awarding Packet WEB_OT_FRIENDS_INVITE_10, userid=" + inviteSentByUserId + ", inviteId=" + drInvite["invite_id"].ToString(), ex);
                                }
                            }
                        }

                        // If we're configured to auto validate, do so
                        else if (Configuration.AutoValidateRegistrationEmail)
                        {
                            // Automatically set thier status
                            GetUserFacade.UpdateUserStatus(userId, (int)Constants.eUSER_STATUS.REGVALIDATED);

                            // User email is automatically validated, so award fame
                            try
                            {
                                GetFameFacade.RedeemPacket(userId, (int)PacketId.WEB_OT_EMAIL_VALIATION, (int)FameTypes.World);
                            }
                            catch (Exception ex)
                            {
                                m_logger.Error("Error awarding Packet WEB_OT_EMAIL_VALIATION, userid=" + userId, ex);
                            }
                        }

                        // If they were invited by a friend via facebook
                        else if (bInvitedViaFacebook)
                        {
                            // User has not been set yet so get new user
                            if (user.UserId == 0)
                            {
                                user = GetUserFacade.GetUser(userId);
                            }

                            // Verify user signed up using facebook
                            if (user.FacebookSettings.FacebookUserId > 0)
                            {
                                try
                                {
                                    if (ViewState["FacebookInviteSentByUserId"] != null && ViewState["FacebookInviteSentByUserId"].ToString() != string.Empty)
                                    {
                                        Int32 uid = Convert.ToInt32(ViewState["FacebookInviteSentByUserId"]);

                                        // kuid is the id of the kaneva user that sent he facebook invite.  Validate
                                        // this is a valid user id
                                        User inviteSentByUser = GetUserFacade.GetUser(uid);

                                        // If valid user, continue
                                        if (inviteSentByUser.UserId > 0)
                                        {
                                            //add user as a personal friend								
                                            int ret = GetUserFacade.AcceptFriend(userId, inviteSentByUser.UserId);

                                            if (ret == 1)
                                            {
                                                // If you wants emails when new friends are added, send them an email
                                                if (inviteSentByUser.NotifyNewFriends)
                                                {
                                                    try
                                                    {
                                                        MailUtilityWeb.SendFriendAcceptedInviteNotification(inviteSentByUser.UserId, userId);
                                                    }
                                                    catch { }
                                                }
                                            }

                                            // User completed fame packet
                                            try
                                            {
                                                GetFameFacade.RedeemPacket(inviteSentByUser.UserId, (int)PacketId.WEB_INVITE_JOIN_SITE, (int)FameTypes.World);
                                            }
                                            catch (Exception ex)
                                            {
                                                m_logger.Error("Error awarding Packet WEB_OT_FRIENDS_INVITE_10, userid=" + inviteSentByUser.UserId + " from Facebook invite.", ex);
                                            }
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    m_logger.Error(ex.ToString());
                                }
                            }
                        }



                        // Add default video to connected media for this user
                        try
                        {
                            int assetId = 0;
                            bool addDefaultVideo = true;

                            if (drJoin != null && drJoin["user_profile_video_id"] != null)
                            {
                                try
                                {
                                    assetId = Convert.ToInt32(drJoin["user_profile_video_id"]);

                                    if (assetId > 0)
                                    {
                                        StoreUtility.InsertAssetChannel(assetId, GetUserFacade.GetPersonalChannelId(userId));
                                        addDefaultVideo = false;
                                    }
                                }
                                catch
                                { }
                            }

                            if (addDefaultVideo)
                            {
                                assetId = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["DefaultNewUserVideoId"]);
                                if (assetId > 0)
                                {
                                    StoreUtility.InsertAssetChannel(assetId, GetUserFacade.GetPersonalChannelId(userId));
                                }
                            }
                        }
                        catch { }

                        /*
                         * REFERRAL 
                         * If this from a share kaneva update as registered.
                         * At this point the user must be registered.
                         * 
                         */
                        if (Request.Cookies["refuser"] != null)
                        {
                            try
                            {
                                int referringUserId = Convert.ToInt32(Request.Cookies["refuser"].Value);
                                int referredUserId = UsersUtility.GetUserIdFromUsername(txtUserName.Text.Trim());

                                GetMarketingFacade.AddReferral(referringUserId, referredUserId);
                            }
                            catch (Exception ex)
                            {
                                m_logger.Error(ex.ToString());
                            }
                        }

                        // Check for any internal experiment testing that may be active
                        CheckExperimentParticipation(userId);

                        // Record the user's progression through new user flow
                        GetMetricsFacade.RecordNewUserFunnelProgression(userId, "web_signup");

                        // Redirect to register complete page
                        if (ViewState["rurl"] != null)	 // This case was put here to handle registration for Contests and auto redirect back to the contest
                        {
                            // Automatically sign them in 
                            FormsAuthentication.SetAuthCookie(userId.ToString(), false);

                            Response.Redirect(ViewState["rurl"].ToString());
                        }
                        else if (Request["returnURL"] != null)
                        {
                            Response.Redirect(ResolveUrl(Request["returnURL"].ToString()));
                        }
                        else
                        {
                            // Automatically sign them in here so we know who they are
                            // for next step if they decide to import contacts from email
                            FormsAuthentication.SetAuthCookie(userId.ToString(), false);

                            string joinid = string.Empty;
                            if (Request.QueryString["joinid"] != null && Request.QueryString["joinid"].ToString().Length > 0)
                            {
                                joinid = "&joinid=" + Server.UrlEncode(Request.QueryString["joinid"].ToString());
                            }

                            string speedySignup = string.Empty;
                            if (SpeedySignupEnabled)
                            {
                                speedySignup = "&KANEVA_SPEEDY_SIGNUP=true";
                            }

                            // redirect them to validation page.  This page will decide where they should go next
                            Response.Redirect(ResolveUrl("~/register/kaneva/registerValidate.aspx?reg=1&email=" + Server.UrlEncode(txtEmail.Text.Trim()) + joinid + speedySignup));
                        }
                        break;
                    }
                case -1:
                    {
                        uNameValid = false;
                        cvBlank.IsValid = false;
                        cvBlank.ErrorMessage = "The Username you requested isn't available. Please select another Username.";
                        break;
                    }
                case -2:
                    {
                        cvBlank.IsValid = false;
                        cvBlank.ErrorMessage = "The email address you provided is already associated with a Kaneva account. Please provide another email address and re-enter your password.";
                        break;
                    }
                case -3:
                    {
                        uNameValid = false;
                        cvBlank.IsValid = false;
                        cvBlank.ErrorMessage = "The Username already exists or is not allowed. Please change your Username.";
                        break;
                    }
                case -4:
                    {
                        cvBlank.IsValid = false;
                        cvBlank.ErrorMessage = "The default home page for this user could not be created.";
                        break;
                    }
            }
        }

        /// <summary>
        /// Called to validate the username field for validity and availability.
        /// </summary>
        /// <returns>True if username is valid and available.</returns>
        private bool ValidateUserName()
        {
            bool usernameOk = true;

            if (revUsername.IsValid && rfUsername.IsValid)
            {
                if (txtUserName.Text.Trim() != string.Empty)
                {
                    if (UsersUtility.GetUserIdFromUsername(txtUserName.Text.Trim()) > 0)
                    {
                        usernameOk = false;
                        revUsername.ErrorMessage = "The Username you requested isn't available. Please select another Username.";
                    }
                }
            }
            else
            {
                usernameOk = false;
            }

            return usernameOk;
        }
        #endregion

        #region Event Handlers
        /// <summary>
        /// Called upon form submission to register a new user.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnRegister_Click(object sender, EventArgs e)
        {
            RegisterNewUser();
        }

        #endregion

        #region Properties

        private string FBAccessToken
        {
            get
            {
                try
                {
                    if (ViewState["accessToken"] != null)
                    {
                        string token = ViewState["accessToken"].ToString();
                        if (token.Length > 0)
                        {
                            return token;
                        }
                    }
                }
                catch { }
                return "";
            }
            set
            {
                ViewState["accessToken"] = value;
            }
        }

        private bool SpeedySignupEnabled
        {
            get
            {
                try
                {
                    if (ViewState["speedySignup"] != null)
                    {
                        return Convert.ToBoolean(ViewState["speedySignup"]);
                    }
                }
                catch { }
                return false;
            }
            set
            {
                ViewState["speedySignup"] = value;
            }
        }

        #endregion Properties

        #region Declerations

        protected CheckBox chkCopyright, chkBeta;
        protected HtmlContainerControl spnCheckUsername, spnCoBrandCopy;
        protected MagicAjax.UI.Controls.AjaxPanel ajpCheckUsername;
        private DateTime dtBirthDate = new DateTime();

        private bool uNameValid;

        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

    }
}