///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Kaneva
{
	public partial class registerFriendsKaneva4 : System.Web.UI.Page
    {
		string _basepath = "http://" + KanevaGlobals.SiteName + "/";//leave in as long as SEO/ search engine indexes are still directing traffic to overview/###.aspx

        protected void Page_Load (object sender, EventArgs e)
        {
            if (!Request.IsAuthenticated)
            {
                Response.Redirect ("~/default.aspx");
            }

            ucConnWithFriends.Visible = true;
			((Register)Master).CustomJavaScript = "<script type=\"text/javascript\" src=\"" + _basepath + "jscript/prototype-1.6.0.3-min.js\"></script>";
			((Register)Master).CustomJavaScript += "<script type=\"text/javascript\" src=\"" + _basepath + "jscript/kaneva.js\"></script>";
        }

        protected override void OnPreRender (EventArgs e)
        {
            // Need to user this event to get the validation table row from the Header Nav.  Can't do this code
            // in Page Load as the header nav code has not been loaded yet so any visibility settings will get
            // overriden by the header nave code behind.
            // We do this so the 'You need to validate' message and nav bar will not display during the signup process
//            HtmlTableRow trValidate = (HtmlTableRow) HeaderNav.FindControl ("trValidateFirst");
//            if (trValidate != null)
//            {
//                trValidate.Visible = false;
//            }
        }
    }
}
