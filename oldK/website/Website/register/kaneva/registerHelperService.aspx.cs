///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Xml;
using System.Xml.Serialization;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva.register.kaneva
{
    public partial class registerHelperService : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                 if (Request.Params["action"] == null)
                {
                    string errorStr = "<Result>\r\n  <ReturnCode>-1</ReturnCode>\r\n  <ResultDescription>action not specified</ResultDescription>\r\n</Result>";
                    Response.Write(errorStr);
                    return;
                }

                string actionreq = Request.Params["action"];

                if (actionreq.Equals("GetStates"))
                {
                    XmlDocument doc = new XmlDocument();
                    XmlElement rootNode = doc.CreateElement("Result");
                    doc.AppendChild(rootNode);

                    XmlElement statesNode = doc.CreateElement("States");
                    rootNode.AppendChild(statesNode);

                    IList<State> states = WebCache.GetStates();

                    foreach (State state in states)
                    {
                        // Add the packet node
                        XmlElement stateNode = doc.CreateElement("State");
                        stateNode.SetAttribute("Name", state.Name);
                        stateNode.SetAttribute("StateCode", state.StateCode); 
                        statesNode.AppendChild(stateNode);
                    }

                    XmlNode root = doc.DocumentElement;

                    XmlElement elem = doc.CreateElement("NumberRecords");
                    elem.InnerText = states.Count.ToString();
                    
                    root.InsertBefore(elem, root.FirstChild);

                    elem = doc.CreateElement("ReturnDescription");
                    elem.InnerText = "success";

                    root.InsertBefore(elem, root.FirstChild);

                    elem = doc.CreateElement("ReturnCode");
                    elem.InnerText = "0";

                    root.InsertBefore(elem, root.FirstChild);

                    Response.Write(root.OuterXml);
                }
                else if  (actionreq.Equals("GetCountries"))
                {
                    XmlDocument doc = new XmlDocument();
                    XmlElement rootNode = doc.CreateElement("Result");
                    doc.AppendChild(rootNode);

                    XmlElement statesNode = doc.CreateElement("Countries");
                    rootNode.AppendChild(statesNode);

                    IList<Country> countries = WebCache.GetCountries ();

                    foreach (Country country in countries)
                    {
                        // Add the packet node
                        XmlElement stateNode = doc.CreateElement("Country");
                        stateNode.SetAttribute("Name", country.Name);
                        stateNode.SetAttribute("CountryId", country.CountryId); 
                        statesNode.AppendChild(stateNode);
                    }

                    XmlNode root = doc.DocumentElement;

                    XmlElement elem = doc.CreateElement("NumberRecords");
                    elem.InnerText = countries.Count.ToString();
                    
                    root.InsertBefore(elem, root.FirstChild);

                    elem = doc.CreateElement("ReturnDescription");
                    elem.InnerText = "success";

                    root.InsertBefore(elem, root.FirstChild);

                    elem = doc.CreateElement("ReturnCode");
                    elem.InnerText = "0";

                    root.InsertBefore(elem, root.FirstChild);

                    Response.Write(root.OuterXml);
                }
                else if (actionreq.Equals("GetUniqueId"))
                {
                    Response.Write("<Result><ReturnDescription>success</ReturnDescription><ReturnCode>0</ReturnCode><Unique>" + System.DateTime.Now.Ticks.ToString () + "</Unique></Result>");
                }
            }


        }
    }
}