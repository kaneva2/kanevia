﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/masterpages/GenericLandingPageTemplate.Master" CodeBehind="registerInfo.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.registerInfoKaneva" %>
<%@ Register TagPrefix="cc2" Namespace="WebControlCaptcha" Assembly="WebControlCaptcha" %>
<%@ Register Assembly="BotDetect" Namespace="BotDetect.Web.UI" TagPrefix="BotDetect" %>

<asp:Content ID="cntHeaderCSS" runat="server" contentplaceholderid="cphHeadCSS" >
    <asp:literal id="litCSS" runat="server" ></asp:literal>	
    <link type="text/css" rel="Stylesheet" href="../../css/landing_page/register/register.css?v4" />
    <link type="text/css" rel="Stylesheet" href="../../css/base/buttons_new.css" />
    <link type="text/css" rel="Stylesheet" href="../../css/registration_flow/register.css?v4" />
    <link type="text/css" rel="Stylesheet" href="../../css/registration_flow/registrationform.css" />
</asp:Content>

<asp:Content ID="cntHeaderJS" runat="server" contentplaceholderid="cphHeadJS" >
    <script type="text/javascript" src="//connect.facebook.net/en_US/all.js"></script>
    <script type="text/javascript" src="../../jscript/selectBox/jquery.selectbox.min.js"></script>
    <script type="text/javascript" src="../../jscript/landing_page/register.js?v2"></script>
</asp:Content>

<asp:Content ID="cntHeader" runat="server" contentplaceholderid="cphHeader" >
    <div class="headerLinks">
        Already a Kaneva member? <span class="showSignIn">Sign In</span>
    </div>
</asp:Content>

<asp:Content id="cntBody" runat="server" contentplaceholderid="cphBody" >
    <asp:Panel ID="pnlSignupForm" runat="server" DefaultButton="btnRegister" ClientIDMode="Static">
        <asp:UpdatePanel ID="upPageMain" runat="server" UpdateMode="Conditional" CSSClass="signupForm" ChildrenAsTriggers="False">	
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnRegister" EventName="Click" />
            </Triggers>
            <ContentTemplate>							
                <div>
		            <asp:validationsummary cssclass="errBox black" id="valSum" runat="server" showmessagebox="false" showsummary="false"
			            displaymode="BulletList" enableclientscript="false" headertext="Please correct the following error(s):" forecolor="#333333"></asp:validationsummary>
		            <asp:customvalidator id="cvBlank" runat="server" display="none" enableclientscript="false"></asp:customvalidator>
	            </div>
                <div id="divPromiseContainer" class="promiseContainer" runat="server">
                    <div class="promiseImageFrame" id="divPromiseImageFrame" runat="server">
                        <img id="imgPromise" runat="server" />
                    </div>
                    <div class="promiseMessageFrame">
                        <h1 id="h1PromiseMessage" runat="server"></h1>
                    </div>
                </div>
                <div class="fbSignup">
                    <p id="pFBSignupMessage" runat="server"><span style="font-style:italic;">Save time</span> by using your Facebook account to sign up for Kaneva.</p>
                    <img src="~/images/facebook/signupfacebooksmall.gif" runat="server" clientidmode="static" id="imgFBSignup" class="imgFBSignup" width="174" height="23" />
                </div>
                <div class="signupForm">
               
                <div class="formContainer">	
                    <div id="divDefaultPromise" runat="server" visible="false">
                        <h2>Join today - it's free.</h2>
                    </div>
                    <div id="divFBProfilePic" class="fbprofilepiccontainer" runat="server" visible="false">
                        <img class="fbprofilepic" id="imgFBProfilePic" runat="server" />
                        <span class="fbpicquestion">Use your Facebook profile picture on Kaneva?</span>
                        <asp:RadioButton id="rbUseFBProfilePicYes" runat="server" checked="True" text="Yes" cssclass="fbprofilerb" GroupName="rbgFBProfilePic" /> 
                        <asp:RadioButton id="rbUseFBProfilePicNo" runat="server" text="No" cssclass="fbprofilerb" GroupName="rbgFBProfilePic" />										
                    </div>
                    <div>
                        <div id='txtUserName_lbl' class="labeltxt" runat="server" clientidmode="static">Username</div>
	                    <asp:textbox id="txtUserName" name="username" field-name="username" clientidmode="static" runat="server" maxlength="20" tabindex="5" AutoPostBack="false"></asp:textbox>
                        <i id="txtUserName_validateimg" runat="server" clientidmode="static" name="txtUserName" class="validateimg username"></i><div id="txtUserName_errmsg" runat="server" clientidmode="static" class="errmsgbox username"><div id="username_msg" runat="server" clientidmode="static" class="msg">Usernames should be 4-20 characters and not have spaces.</div></div>
                        <br class="clear" />
	                    <asp:requiredfieldvalidator id="rfUsername" runat="server" controltovalidate="txtUserName" text="" 
                            enableclientscript="false" enabled="true" errormessage="" display="none"></asp:requiredfieldvalidator>
	                    <asp:regularexpressionvalidator id="revUsername" runat="server" controltovalidate="txtUserName" text="" 
                            errormessage="Nicknames should be 4-20 characters and a combination of upper and lower-case letters, numbers, and underscores (_) only. Do not use spaces." 
                            display="none" enableclientscript="false"></asp:regularexpressionvalidator>																							
                    </div>
                    <div>
                        <div class="firstname-container">
                            <div id="txtFirstName_lbl" clientidmode="static" runat="server" class="labeltxt">First name</div>
			                <asp:textbox id="txtFirstName" runat="server" field-name="firstname" clientidmode="Static" maxlength="50" tabindex="6" cssclass="name"></asp:textbox>
                            <i id="txtFirstName_validateimg" name="txtFirstName" runat="server" clientidmode="static" class="validateimg firstname"></i>
                            <div id='txtFirstName_errmsg' class="errmsgbox short firstname"><div id="firstname_msg" runat="server" clientidmode="static" class="msg single">What is your first name?</div></div>
                        </div>
                        <div class="lastname-container">
                            <div id="txtLastName_lbl" clientidmode="static" runat="server" class="labeltxt lastname">Last name</div>
                            <asp:textbox id="txtLastName" runat="server" field-name="lastname" clientidmode="static" maxlength="50" tabindex="7" cssclass="last name"></asp:textbox>
                            <i id="txtLastName_validateimg" name="txtLastName" runat="server" clientidmode="static" class="validateimg lastname"></i>
                            <div id='txtLastName_errmsg' class="errmsgbox short lastname"><div id="lastname_msg" runat="server" class="msg single">What is your last name?</div></div>
                        </div>
                    </div>
                    <div>
                        <div id='txtEmail_lbl' runat="server" clientidmode="static" class="labeltxt">Email</div>
			            <asp:textbox id="txtEmail" clientidmode="Static" field-name="email" runat="server" maxlength="100" tabindex="8"></asp:textbox> 
                        <i name="<%=txtEmail.ClientID%>" class="validateimg email"></i><div id='<%=txtEmail.ClientID%>_errmsg' class="errmsgbox email"><div class="msg">You’ll use your email to log in each time.</div></div>
                        <asp:regularexpressionvalidator id="revEmail" runat="server" controltovalidate="txtEmail" text="" errormessage="Invalid email address."
			                display="none" enableclientscript="false"></asp:regularexpressionvalidator>
		            </div>	
                    <div>
                        <div id='<%=txtPassword.ClientID%>_lbl' class="labeltxt">New Password</div>
			            <asp:textbox id="txtPassword" clientidmode="static" field-name="password" runat="server" maxlength="20" tabindex="9" TextMode="Password"></asp:textbox>
                        <i name="<%=txtPassword.ClientID%>" class="validateimg password"></i><div id='<%=txtPassword.ClientID%>_errmsgbox' class="errmsgbox password"><div class="msg">Passwords should be 4-20 letters, numbers, and underscores (_) only.</div></div>
		                <asp:regularexpressionvalidator id="revPassword" runat="server" controltovalidate="txtPassword" text="" errormessage="Passwords should be 4-20 characters and a combination of upper and lower-case letters, numbers and underscores (_) only. Do not use spaces."
			                display="none" enableclientscript="false"></asp:regularexpressionvalidator>
                    </div>
                    <div class="zipcontainer">
                        <div class="zipcode">
                            <div id='<%=txtPostalCode.ClientID%>_lbl' class="labeltxt">Zip Code</div>
			                <asp:dropdownlist id="drpCountry" runat="server" tabindex="10" width="120" class="countrySelect"></asp:dropdownlist>
			                <span id="divPostalCode" runat="server"><asp:textbox id="txtPostalCode" field-name="postalcode" runat="server" clientidmode="Static" maxlength="25" tabindex="11" class="postalCode"></asp:textbox>
                            <i name="<%=txtPostalCode.ClientID%>" class="validateimg postalcode"></i></span><div id='<%=txtPostalCode.ClientID%>_errmsg' class="errmsgbox short postalcode"><div class="msg single">Enter your zip code.</div></div>
	                        <asp:requiredfieldvalidator id="rftxtPostalCode" runat="server" controltovalidate="txtPostalCode" text="" errormessage=""
		                        display="none" enabled="false" enableclientscript="false"></asp:requiredfieldvalidator>
                        </div>
                    </div>																							
	                <div class="birthday">
	                    <label id="lblBirthday" runat="server" for='<%=drpMonth.ClientID %>'>Birthday</label>
						<div class="clear"></div>		
	                    <asp:dropdownlist id="drpMonth" clientidmode="static" field-name="birthdate" cssclass="monthSelect" runat="server" tabindex="11">
			                <asp:listitem value="">Month</asp:listitem>
			                <asp:listitem value="1">January</asp:listitem>
			                <asp:listitem value="2">February</asp:listitem>
			                <asp:listitem value="3">March</asp:listitem>
			                <asp:listitem value="4">April</asp:listitem>
			                <asp:listitem value="5">May</asp:listitem>
			                <asp:listitem value="6">June</asp:listitem>
			                <asp:listitem value="7">July</asp:listitem>
			                <asp:listitem value="8">August</asp:listitem>
			                <asp:listitem value="9">September</asp:listitem>
			                <asp:listitem value="10">October</asp:listitem>
			                <asp:listitem value="11">November</asp:listitem>
			                <asp:listitem value="12">December</asp:listitem>
		                </asp:dropdownlist>
		                <asp:dropdownlist runat="server" clientidmode="static" field-name="birthdate" id="drpDay" cssclass="daySelect" tabindex="12">
			                <asp:listitem value="">Day</asp:listitem>
			                <asp:listitem value="1">01</asp:listitem>
			                <asp:listitem value="2">02</asp:listitem>
			                <asp:listitem value="3">03</asp:listitem>
			                <asp:listitem value="4">04</asp:listitem>
			                <asp:listitem value="5">05</asp:listitem>
			                <asp:listitem value="6">06</asp:listitem>
			                <asp:listitem value="7">07</asp:listitem>
			                <asp:listitem value="8">08</asp:listitem>
			                <asp:listitem value="9">09</asp:listitem>
			                <asp:listitem value="10">10</asp:listitem>
			                <asp:listitem value="11">11</asp:listitem>
			                <asp:listitem value="12">12</asp:listitem>
			                <asp:listitem value="13">13</asp:listitem>
			                <asp:listitem value="14">14</asp:listitem>
			                <asp:listitem value="15">15</asp:listitem>
			                <asp:listitem value="16">16</asp:listitem>
			                <asp:listitem value="17">17</asp:listitem>
			                <asp:listitem value="18">18</asp:listitem>
			                <asp:listitem value="19">19</asp:listitem>
			                <asp:listitem value="20">20</asp:listitem>
			                <asp:listitem value="21">21</asp:listitem>
			                <asp:listitem value="22">22</asp:listitem>
			                <asp:listitem value="23">23</asp:listitem>
			                <asp:listitem value="24">24</asp:listitem>
			                <asp:listitem value="25">25</asp:listitem>
			                <asp:listitem value="26">26</asp:listitem>
			                <asp:listitem value="27">27</asp:listitem>
			                <asp:listitem value="28">28</asp:listitem>
			                <asp:listitem value="29">29</asp:listitem>
			                <asp:listitem value="30">30</asp:listitem>
			                <asp:listitem value="31">31</asp:listitem>
		                </asp:dropdownlist>
		                <asp:dropdownlist runat="server" clientidmode="static" id="drpYear" field-name="birthdate" cssclass="yearSelect" tabindex="13">
			                <asp:listitem value="">Year</asp:listitem>
		                </asp:dropdownlist>	
                        <i name="birthdate" class="validateimg birthdate"></i><div id="birthdate_errmsg" class="errmsgbox birthdate"><div id="birthday_msg" class="msg" runat="server" clientidmode="static">Enter your birthday. You must be 14 or older to join.</div></div>
                    </div>	
                    <div class="gender">
                        <span style="display:none;">
			            <asp:dropdownlist id="drpGender" clientidmode="static" runat="server" class="genderSelect">
				            <asp:listitem value="">Select Sex</asp:listitem>
				            <asp:listitem value="Male">Male</asp:listitem>
				            <asp:listitem value="Female">Female</asp:listitem>
			            </asp:dropdownlist>
			            </span>
			            <div class="gender-container">
                            <div class="female"><input type="radio" id="radFemale" runat="server" clientidmode="static" name="gender" field-name="gender" value="Female" tabindex="14" /><span>Female</span></div>
			                <div class="male"><input type="radio" id="radMale" runat="server" clientidmode="static" name="gender" field-name="gender" value="Male" tabindex="15" /><span>Male</span></div>
                            <i name="gender" class="validateimg gender"></i>
                        </div>
                        <div id='gender_errmsg' class="errmsgbox gender"><div class="msg single">Please select either male or female.</div></div>
                    </div>

	                <div class="captcha-container" id="captchaContainer" runat="server" clientidmode="static">
                           <BotDetect:WebFormsCaptcha runat="server" ID="CaptchaControl1" />
                           <asp:TextBox ID="CaptchaCodeTextBox" runat="server" CssClass="captcha"/>
                        <i name="captcha" class="validateimg captcha"></i><div id="captcha_errmsg" class="errmsgbox captcha"><div id="captcha_msg" class="msg" runat="server" clientidmode="static">Unique security code does not match.</div></div>
                    </div>

                    <div class="join-container">
                        <asp:linkbutton id="btnRegister" onclick="btnRegister_Click" clientidmode="static" runat="server" alternatetext="Start Now!" tabindex="17" CssClass="joinNow"></asp:linkbutton>
                        <img class="loading" src="../../images/ajax-loader.gif" alt="Loading..." style="display:none;"/>	
                    </div>
                    <div class="terms"></div>
                
                </div>
                
                
                </div>
                <img src="../../images/landing_page/graphic_10x363_registration_verticalShadowBreak.png" width="10px" height="363px" alt="" />
            </ContentTemplate>	
        </asp:UpdatePanel>	
    </asp:Panel>
</asp:Content>

<asp:Content id="cntFooter" runat="server" contentplaceholderid="cphFooter" >
    <!--[if lte IE 8]><img class="footerRuleImage" src="../../images/landing_page/hr_917x2_footerBreak.jpg" alt="" width="923px" height="2px"/><![endif]-->
    <hr class="footerRuleTop" />
    <hr class="footerRuleBottom" />
</asp:Content>