<%@ Page language="c#" Codebehind="registerCompleted_B.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.registerCompletedKaneva_B" %>

<asp:literal id="litCSS" runat="server" />	

<link href="../../css/themes-join/template.css" rel="stylesheet" type="text/css" />	


<script language="javascript">
    function showNav() {
        document.getElementById('primary').style.display = 'inline';
        document.getElementById('tertiary').style.display = 'inline';
    }
</script>

<br />
<table cellpadding="0" cellspacing="0" border="0" width="790" align="center"  >
	<tr>
		<td colspan="3"><img runat="server" src="~/images/spacer.gif" width="1" height="14" id="Img12"/></td>
	</tr>
	<tr>
		<td colspan="3">
			<table  border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="frTopLeft"></td>
					<td class="frTop"></td>
					<td class="frTopRight"></td>
				</tr>
				<tr>
					<td class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" id="Img13" width="1" height="1" /></td>
					<td valign="top" class="frBgIntMembers">
						<table width="100%" border="0" cellpadding="0" cellspacing="0" >
							<tr>
								<td width="770" align="left"  valign="top">
								
									<table cellpadding="0" cellspacing="0" border="0"  width="100%">
										<tr>
											<td width="790" valign="top">
												
												<table border="0" cellspacing="0" cellpadding="0" width="100%">
													<tr>
														<!-- START WIZARD CONTENT -->
														<td valign="top" align="center">
															<br />
                                                            <table border="0" cellspacing="0" cellpadding="0" width="670" align="center">
																<tr>
																	<td width="670">

																		<table border="0" cellspacing="0" cellpadding="0" width="100%">
																			<tr>
																				<td width="70%" valign="top">
																					<span id="Span1"></span>
																					<h1>Congratulations <span id="spnUserName" runat="server"></span>!<br>You're now a member of Kaneva!</h1>																					
			                                                                        <span id="spnCoBrandCopy" runat="server"></span>
			                                                                        <h2 id="h2Validated" runat="server">Your Email address has been validated!</h2>
																					<p>As a Kaneva member, you can:</p>
																					<ul>
																						<li>Meet new people and hang out in the Virtual World</li>
																						<li>Go shopping for virtual clothes and accessories</li>
																						<li>Get your free, fully customizable 3D home</li>
																						<li>Open your own 3D dance club</li>
																					</ul>
																					
																					<br>
																					<center>
																					<a runat="server" id="aDownload2" class="nohover" onclick="showNav()"><img src="../../images/btn_letsgo.gif"  border="0" align="center" /></a></center>
																				   
																				</td>
																				<td width="30%" valign="top" align="right"><img id="Img1" runat="server" src="~/images/themes-join/kaneva/DLscreen_New.jpg" alt="" height="197" border="0" /></td>
																			</tr>
																			<tr><td></td></tr>
																		</table>
																		
																		<br><br>	
																			
																			
																			<table border="0" cellspacing="0" cellpadding="0" width="100%">
																				
																				<tr>
																					<td align="center">
																						<div class="module">
																						<span class="ct"><span class="cl"></span></span>
																						
																						
																						
																						<table border="0" cellspacing="0" cellpadding="0" width="95%" align="center">
																							<tr>
																								<td valign="top" width="400">
																								    	<p class="small">Your download will start automatically.</p>	
																								    	<p class="small">If it does not start, <a runat="server" id="aDownload">click here</a> to download.</p>																									
																								    	<br />
																									<h2>How to Install 3D World <a href="javascript:void(0);" onclick="document.getElementById('divSysReq').style.display='block';" style="margin-left:30px;"><span class="small">(System Requirements)</span></a></h2>
																									<p class="small">1) Click <strong>"Run"</strong> in each window that appears.</p>
																									<p class="small">2) Sign in using the email address you used to create your account.</p>																									
																									<br>
																									<table border="0" cellspacing="0" cellpadding="5">
																										<tr>
																											<td><img id="Img2" runat="server" src="~/images/rundialog1.jpg" alt="rundialog1.jpg" width="150" border="0"></td>
																											<td><img id="Img3" runat="server" src="~/images/rundialog2.jpg" alt="rundialog2.jpg" width="200" border="0"></td>
																										</tr>
																									</table>
																								</td>
																								<td valign="top">
																									<div class="module whitebg" id="divSysReq" style="display:none;">
																									<span class="ct"><span class="cl"></span></span>
																									<h2>System Requirements</h2>
																									
																									<ul class="small">
																										<li>Windows XP or Vista</li>
																										<li>515 available hard disk space</li>
																										<li>512 MB RAM</li>
																										<li>High-speed Internet connection</li>
																										<li>Nvidia 5200 series / ATI X700 series or faster graphics card</li>
																										<li>Intel or AMD 2GHz or faster processor</li>
																										<li>Internet Explorer 6.0 OR Mozilla Firefox 1.5 or later</li>
																									</ul>
																									<br>
																									<p>Need Help? <a id="A1" href="~/community/CommunityPage.aspx?communityId=1118&pageId=887411" runat="server" target="_blank">Click here for support.</a></p>
																									<br />
																									<p>Still having trouble? <a id="A2" href="~/suggestions.aspx?mode=F&category=Feedback" runat="server" target="_blank">Contact us</a></p>

																									
																									<span class="cb"><span class="cl"></span></span>
																									</div>	
																								</td>
																							</tr>
																						</table>
																						<span class="cb"><span class="cl"></span></span>
																						</div>	
																					</td>
																				</tr>
																				<tr>
																				<td>
																				<p style="margin-left: 275px"><a id="A3" href="~/default.aspx" runat="server" target="_blank">Click here </a> to explore Kaneva on the Web, where you can connect with friends, gain fame and fortune, and have fun!</p>
																				
																				</td>
																				</tr>
																			</table>
																			<br/>																		
																		 
																	</td>
																</tr>
															</table>
																																																																																
														</td>
														
														<!-- END WIZARD CONTENT -->
													</tr>
												</table>
											
											</td>
											
										</tr>
										
									</table>
											
								</td>
							</tr>
							
						</table>
					</td>
					<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" id="Img15" width="1" height="1" /></td>
				</tr>
				<tr>
					<td class="frBottomLeft"></td>
					<td class="frBottom"></td>
					<td class="frBottomRight"></td>
				</tr>
			</table>				
		</td>
	</tr>
</table>
<br/><br/><br/><br/>

<!-- Google Code for pageview Conversion Page -->
<script language="JavaScript" type="text/javascript">
<!--
    var google_conversion_id = 1065938872;
    var google_conversion_language = "en_US";
    var google_conversion_format = "1";
    var google_conversion_color = "FFFFFF";
    if (2.0) {
        var google_conversion_value = 2.0;
    }
    var google_conversion_label = "pageview";
//-->
</script>
<script language="JavaScript" src="http://www.googleadservices.com/pagead/conversion.js"> [^]
</script>
<noscript>
<img height=1 width=1 border=0 src="http://www.googleadservices.com/pagead/conversion/1065938872/?value=2.0&label=pageview&script=0"> [^]
</noscript>

<script>
    if (typeof (urchinTracker) != 'function') document.write('<sc' + 'ript src="' +
'http' + (document.location.protocol == 'https:' ? 's://ssl' : '://www') +
'.google-analytics.com/urchin.js' + '"></sc' + 'ript>')
</script>
<script>
    _uacct = 'UA-5755114-4';
    urchinTracker("/3837460917/goal");
</script>

<script>
    if (typeof (urchinTracker) != 'function') document.write('<sc' + 'ript src="' +
'http' + (document.location.protocol == 'https:' ? 's://ssl' : '://www') +
'.google-analytics.com/urchin.js' + '"></sc' + 'ript>')
</script>
<script>
    try {
        _uacct = 'UA-5755114-4';
        urchinTracker("/3799682931/goal");
    } catch (err) { }
</script>