<%@ Page Language="C#" MasterPageFile="~/masterpages/Register.Master" AutoEventWireup="true" CodeBehind="registerFriends2.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.registerFriendsKaneva2"  %>
<%@ Register TagPrefix="Kaneva" TagName="ConnWithFriends" Src="../../usercontrols/register/ConnectWithFriends2.ascx" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cph_Body" >
<div class="content_wrapper">
	<div id="ContentContainer">								
		<!-- START WIZARD CONTENT -->
		<kaneva:ConnWithFriends runat="server" id="ucConnWithFriends" ExitUrl="register/kaneva/registerCompleted2.aspx"/>
		<!-- END WIZARD CONTENT -->														
	</div>
</div>
<!-- Google Website Optimizer Tracking Script -->
<script type="text/javascript">
if(typeof(_gat)!='object')document.write('<sc'+'ript src="http'+
(document.location.protocol=='https:'?'s://ssl':'://www')+
'.google-analytics.com/ga.js"></sc'+'ript>')</script>
<script type="text/javascript">
try {
var gwoTracker=_gat._getTracker("UA-10115643-15");
gwoTracker._trackPageview("/0875675435/test");
}catch(err){}</script>
<!-- End of Google Website Optimizer Tracking Script -->
</asp:Content>