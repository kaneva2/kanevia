///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using log4net;
using System.Web.Security;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for registerValidate.
    /// 
    /// IMPORTANT:  This page still get's processed even though
    /// the user will no longer see it during the join process.
    /// 
    /// 
	/// </summary>
    public class registerValidateKaneva : MainTemplatePage
	{
		protected registerValidateKaneva () 
		{
			Title = "Join Kaneva: Email Verified.";
		}
		
		private void Page_Load(object sender, System.EventArgs e)
		{
			if (!IsPostBack)													 
			{																	 
				if (Request ["i"] != null && Request ["j"] != null)
				{
					string username = Request ["i"].ToString ();
					string keyCode = Request ["j"].ToString ();
				
					string destinationPage = string.Empty;
					if (Request ["k"] != null)
					{
						destinationPage = Request ["k"].ToString ();
					}

					if (!IsPostBack)
					{
                        
						// if the user has any status other than not validated, they should not be on this page
                        UserFacade userFacade = new UserFacade ();
                        User user = userFacade.GetUser (username);

                        if (!user.StatusId.Equals((int)Constants.eUSER_STATUS.REGNOTVALIDATED) && !user.EmailStatus.Equals((int)Constants.eEMAIL_STATUS.BOUNCED))
						{
                            Response.Redirect("~/free-virtual-world.kaneva");
						}

						//validate user
                        bool validated = false;

                        // Handles users that are re-verifying their email address after a bounce.
                        if (user.EmailStatus.Equals((int)Constants.eEMAIL_STATUS.BOUNCED) && user.KeyValue.Equals(keyCode))
                        {
                            userFacade.UpdateUserEmailStatus(user.UserId, (int)Constants.eEMAIL_STATUS.OK);
                            validated = true;
                        }

                        if (user.StatusId.Equals((int)Constants.eUSER_STATUS.REGNOTVALIDATED) && user.KeyValue.Equals (keyCode))
                        {
                            userFacade.UpdateUserStatus(user.UserId, (int)Constants.eUSER_STATUS.REGVALIDATED);
                            validated = true;
                        }

                        // They validated try to remove from blacklist
                        MailUtility.RemoveFromBlackList(user.Email);

                        // If they joined through a special community that has free wok items check here.
                        if (Request.QueryString["joinid"] != null)
                        {
                            try
                            {
                                AddWokItems(Convert.ToInt32(Request.QueryString["joinid"]), user);
                            }
                            catch { }
                        } 

						//Work around redirect for users who are coming here from the validation resend on general.aspx
						//redirects them to the general page
						if (destinationPage == "general")
						{
							LoginUser(user.Email, false, false);

                            // Award Fame Packet
                            AwardValidateEmailFame (user.UserId);

                            Response.Redirect ("~/mykaneva/general.aspx?validated=" + (validated ? "true" : "false"));
						}

						if (validated)
						{
							// Automatically sign them in here
                            FormsAuthentication.SetAuthCookie(user.UserId.ToString(), false);

							m_logger.Info ("Validate successfull username = '" + username + "', keyCode ='" + keyCode + "'");

                            // Award Fame Packet
                            AwardValidateEmailFame (user.UserId);

                            if (destinationPage == "redeem")
							{
								Response.Redirect ("~/checkout/redeemGiftCard.aspx");	
							}

                            if (Request.QueryString["joinid"] != null)
                            {
                                Response.Redirect("~/register/registerCompleted.aspx" +
                                    (Request.QueryString["joinid"] != null ? "?joinid=" + Request.QueryString["joinid"].ToString() : ""));
                            }
                            else
                            {
                               
                                Response.Redirect("~/default.aspx");
                            }
						}
						else // failed registration
						{
							//TODO: ***** WHAT TO DO IF REGISTRATION FAILS ??? *****
						
							m_logger.Info ("Validate FAILED username = '" + username + "', keyCode ='" + keyCode + "'");
						}
					}
				}
				else if (Request.QueryString ["email"] != null || Request.IsAuthenticated)
				{
                    User drUser = null;

                    if (Request.IsAuthenticated)
                    {
                        drUser = GetUserFacade.GetUser(GetUserId());
                    }
                    else
                    {
                        // Get user to see if they need the validation email sent to them
                        drUser = GetUserFacade.GetUserByEmail(Server.UrlDecode(Request["email"].ToString()));
                    }

                    if (drUser != null)
                    {
                        // added referral url check so that we do not send a duplicate validation email for 
                        // a new user that just joined. 
						// 2010-05-17 pain that this url check exists, since we change it from time to time
						// putting some kewords here to trigger searching for keywords
						// register.aspx registerInfo.aspx registration
                        if (drUser.StatusId == (int)Constants.eUSER_STATUS.REGNOTVALIDATED && 
                            Request.UrlReferrer != null && 
                            (Request.UrlReferrer.AbsoluteUri.ToLower().IndexOf("home.aspx") < 0 ||
                             Request.UrlReferrer.AbsoluteUri.ToLower().IndexOf("home_a.aspx") < 0))
                        {
                            MailUtilityWeb.SendRegistrationEmail(drUser.Username, drUser.Email, drUser.RegistrationKey);
                        }
                        // had to add this case to handle people joining that were invited by friends so they were validated in the
                        // registration step (registerinfo.aspx page) and need to be redirected to the registerfriends page
                        else if (drUser.StatusId == (int) Constants.eUSER_STATUS.REGVALIDATED &&
                            Request.QueryString["reg"] != null &&
                            Request.QueryString["reg"].ToString ().Equals("1"))
                        {
                            // don't do anything, let them fall through to the redirect at the end to the
                            // registerFriends.aspx page
                        }
                        else if (drUser.StatusId == (int) Constants.eUSER_STATUS.REGVALIDATED)
                        {
                            Response.Redirect ("~/checkout/redeemgiftcard.aspx");
                        }

                        lblEmail.Text = drUser.Email.ToString();
                    }
				}
				else 
				{
                    Response.Redirect("~/free-virtual-world.kaneva");
				}

				
				int community_id = 0;
				if (Request.QueryString ["joinid"] != null)
				{
					try
					{
						community_id = Convert.ToInt32 (Request.QueryString["joinid"]);
					}
					catch {}
				}

				// Get the join configuration settings if we have not alread retrieved them
				DataRow drJoin = CommunityUtility.GetJoinConfig (community_id);

				if (drJoin != null)
				{	
					if (drJoin ["css"] != null && drJoin ["css"].ToString ().Length > 0)
					{
						litCSS.Text = "<link href=\"" + ResolveUrl (drJoin ["css"].ToString ()) + "\" rel=\"stylesheet\" type=\"text/css\">";
					}
				}
			}

            // 2008-06-03 CHM
            // Along with the removal of required validation this page is to be skipped.
            string emailQueryString = "";
            if (Request.QueryString ["email"] != null)
            {
                emailQueryString = "?email=" + Server.UrlEncode(Request.QueryString["email"].ToString());
            }

            // If using speedy signup for testing, skip the completion page and take the user directly to myKaneva
            if (Request.QueryString["KANEVA_SPEEDY_SIGNUP"] != null && Request.QueryString["KANEVA_SPEEDY_SIGNUP"].Equals("true") && Request.IsAuthenticated)
            {
                Response.Redirect("~/default.aspx");
            }
            else
            {
                Response.Redirect("registerCompleted.aspx" + emailQueryString);
            }	
			//Response.Redirect("registerCompleted2.aspx" + emailQueryString);
			//Response.Redirect("registerFriends.aspx" + emailQueryString);

		}

		#region Helper Methods
		
        /// <summary>
		/// LoginUser
		/// </summary>
		private void LoginUser (string email, bool bPersistLogin, bool useWizard)
		{
            UserFacade userFacade = new UserFacade();
			DataRow drUser = UsersUtility.GetUserFromEmail (email);
			int userId = Convert.ToInt32 (drUser ["user_id"]);

            FormsAuthentication.SetAuthCookie(userId.ToString(), bPersistLogin);
            int loginId = userFacade.UpdateLastLogin(userId, Common.GetVisitorIPAddress(), Server.MachineName);

      // Now take their current IP and and update their cookie.
      Response.Cookies["ipcheck"].Value = Common.GetVisitorIPAddress();
      Response.Cookies["ipcheck"].Expires = DateTime.Now.AddYears(1);  

      // Set the userId in the session for keeping track of current users online
			Session ["userId"] = userId;
            Session["loginId"] = loginId;

			if(useWizard)
			{
				Response.Redirect ("~/registerPhoto.aspx?step=2");
			}
		}

        /// <summary>
        /// AddWokItems
        /// </summary>
        private static void AddWokItems (int communityId, User user)
        {
            int globalId = 0;
            int qty = 0;
            int userId = user.UserId;

            try
            {
                // get the free items
                DataTable dtFreeItems = StoreUtility.GetFreeWokItemsCommunity(communityId, user.Gender);

                for (int i = 0; i < dtFreeItems.Rows.Count; i++)
                {
                    globalId = Convert.ToInt32(dtFreeItems.Rows[i]["wok_item_id"]);
                    qty = Convert.ToInt32(dtFreeItems.Rows[i]["quantity"]);

                    UsersUtility.AddItemToUserInventory(userId, Constants.WOK_INVENTORY_TYPE_PERSONAL, globalId, qty);
                }
            }
            catch (Exception exc)
            {
                m_logger.Error("Error adding item to users inventory: ", exc);
                m_logger.Error("userId=" + userId.ToString() + " : userId=" + userId.ToString() + " : globalId=" + globalId.ToString() + " : qty=" + qty.ToString());
            }
        }

        /// <summary>
        /// AwardValidateEmailFame
        /// </summary>
        private void AwardValidateEmailFame (int userId)
        {
            try
            {
                FameFacade fameFacade = new FameFacade ();
                fameFacade.RedeemPacket (userId, (int) PacketId.WEB_OT_EMAIL_VALIATION, (int) FameTypes.World);
            }
            catch (Exception ex)
            {
                m_logger.Error ("Error awarding Packet WEB_OT_EMAIL_VALIATION, userid=" + userId, ex);
            }
        }
        
        #endregion

		#region Declerations
		
		protected HtmlInputText	txtEmail, txtPassword;
		
		protected ImageButton	btnNext;
		protected Label			lblEmail;
		protected Literal		litCSS;

		// Logger
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);
		
		#endregion
 
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
