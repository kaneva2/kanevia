<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="registerFrame.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.registerFrame" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>
<%@ Register TagPrefix="cc2" Namespace="WebControlCaptcha" Assembly="WebControlCaptcha" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head >   
    <script type="text/javascript" src="../jscript/prototype-1.6.0.3-min.js"></script>
	<link href="../css/home/home20100519.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">
var shownMsg = false;
function alertMsg(obj, focus)
{
	if (!shownMsg && focus)
	{
		obj.className = 'red bold small';
		shownMsg = true;
	}
	else
	{
		obj.className = '';
	}
}
</script>


</head>
<body>
    <form id="form1" runat="server">
    <div id="regForm">		
                                                                      



<div id="homepageRegWrapper">
																																																																				    	
<div>
	<h1 class="homepageJoin">Join Kaneva!</h1>
	<h2 class="homepageFree">It's FREE and EASY</h2>
</div>			   
<ajax:ajaxpanel id="ajpMain" runat="server">

<asp:validationsummary cssclass="errBox black" id="valSum" runat="server" showmessagebox="False" showsummary="True"
	displaymode="BulletList" style="margin-top:10px;" headertext="Please correct the following error(s):" forecolor="black"></asp:validationsummary>
<asp:customvalidator id="cvBlank" runat="server" display="None" enableclientscript="False"></asp:customvalidator>


<fieldset>
	<ol>
		<li>			
			<asp:requiredfieldvalidator id="rfUsername" runat="server" controltovalidate="txtUserName" text="*" 
				errormessage=""	display="none"></asp:requiredfieldvalidator>
			
			<asp:regularexpressionvalidator id="revUsername" runat="server" controltovalidate="txtUserName" 
				text="*" errormessage="Nicknames should be 4-20 characters and a combination of upper and lower-case 
				letters, numbers, and underscores (_) only. Do not use spaces." display="none"></asp:regularexpressionvalidator>
			
			<label for="nickName"><span id="spnUsername" runat="server">Nickname:</span></label>
			
			<asp:textbox id="txtUserName" runat="server" maxlength="20" tabindex="10" Width="200px"></asp:textbox>
			<p class="username note">This is your avatar's name in Kaneva</p>			
			
			<!--
			<ajax:ajaxpanel id="ajpCheckUsername" runat="server">
			<asp:imagebutton id="btnCheckUsername" onclick="btnCheckUsername_Click" tabindex="2" runat="server"
					width="124"
					imageurl="~/images/wizard_btn_checkavail.gif"
					alternatetext="Check Availability"
					height="21"
					border="0"
					imagealign="AbsMiddle"
					causesvalidation="False"></asp:imagebutton>&nbsp;&nbsp;&nbsp;&nbsp;
			<div>			
			<span id="spnCheckUsername" runat="server"></span>			
			</ajax:ajaxpanel>	
		    -->	
		</li>
		
		<li>
			<asp:requiredfieldvalidator id="rfFirstName" runat="server" controltovalidate="txtFirstName" text="*" errormessage=""
				display="none"></asp:requiredfieldvalidator>			
			<label for="firstName"><span id="spnFirstName" runat="server">First Name:</span></label>
			<asp:textbox id="txtFirstName" runat="server" maxlength="50" tabindex="20" Width="200px"></asp:textbox>
		</li>
		<li>
			<asp:requiredfieldvalidator id="rfLastName" runat="server" controltovalidate="txtLastName" text="*" errormessage=""
					display="none"></asp:requiredfieldvalidator>
			<label for="lastName"><span id="spnLastName" runat="server">Last Name:</span></label>
			<asp:textbox id="txtLastName" runat="server" maxlength="50" tabindex="30" Width="200px" ></asp:textbox>
		</li>
		<li>
			<asp:customvalidator id="cvGender" runat="server" controltovalidate="hidGender" text="*" errormessage="" display="none"></asp:customvalidator>  
			<label for="gender"><span id="spnGender" runat="server">Gender:</span></label>
			<asp:radiobutton groupname="rblGender" id="rbFemale" runat="server" tabindex="40"   />
			<img id="Img1" src="~/images/registration_flow/gender_female_25x49.gif" runat="server" style="padding-right:30px;" />
			<asp:radiobutton groupname="rblGender" id="rbMale" runat="server" tabIndex="41"  />
			<img id="Img2" src="~/images/registration_flow/gender_male_20x49.gif" runat="server"  />
			<asp:textbox id="hidGender" runat="server" style="display:none;"/>
			
			
		</li>
		<li>
			<asp:requiredfieldvalidator id="rfdrpCountry" runat="server" controltovalidate="drpCountry" text="*" errormessage="" tabindex="7" display="none"></asp:requiredfieldvalidator>
			<asp:requiredfieldvalidator id="rftxtPostalCode" runat="server" controltovalidate="txtPostalCode" text="*" errormessage="Zip code is a required field for U.S. residents."
				display="none" enabled="False"></asp:requiredfieldvalidator>
			<label for="country"><span id="spnCountry" runat="server">Country:</span></label>
			<asp:dropdownlist id="drpCountry" runat="server" tabindex="50" Width="200px"></asp:dropdownlist>
		</li>
		<li>
			<label for="zipCode"><span id="spnPostalCode" runat="server">ZIP Code:</span></label>
			<asp:textbox id="txtPostalCode" runat="server" maxlength="25" tabindex="60" Width="200px"></asp:textbox> 		
		</li>
		<li>
			<br />
		</li>
		<li>
			<label for="birthdate"><span id="spnBirthday" runat="server">Birthday:</span></label>
			<asp:requiredfieldvalidator id="rfMonth" runat="server" controltovalidate="drpMonth" text="*" errormessage="" display="none"></asp:requiredfieldvalidator>
			<asp:requiredfieldvalidator id="rfDay" runat="server" controltovalidate="drpDay" text="*" errormessage="" display="none"></asp:requiredfieldvalidator>
			<asp:requiredfieldvalidator id="rfYear" runat="server" controltovalidate="drpYear" text="*" errormessage=""	display="none"></asp:requiredfieldvalidator>
			<asp:dropdownlist class="insideBoxText11" id="drpMonth" runat="server" tabindex="70" Width="70px">
				<asp:listitem value="">Month</asp:listitem>
				<asp:listitem value="1">January</asp:listitem>
				<asp:listitem value="2">February</asp:listitem>
				<asp:listitem value="3">March</asp:listitem>
				<asp:listitem value="4">April</asp:listitem>
				<asp:listitem value="5">May</asp:listitem>
				<asp:listitem value="6">June</asp:listitem>
				<asp:listitem value="7">July</asp:listitem>
				<asp:listitem value="8">August</asp:listitem>
				<asp:listitem value="9">September</asp:listitem>
				<asp:listitem value="10">October</asp:listitem>
				<asp:listitem value="11">November</asp:listitem>
				<asp:listitem value="12">December</asp:listitem>
			</asp:dropdownlist>
			/
			<asp:dropdownlist runat="server" class="insideBoxText11" id="drpDay" tabindex="71">
				<asp:listitem value="">Day</asp:listitem>
				<asp:listitem value="1">01</asp:listitem>
				<asp:listitem value="2">02</asp:listitem>
				<asp:listitem value="3">03</asp:listitem>
				<asp:listitem value="4">04</asp:listitem>
				<asp:listitem value="5">05</asp:listitem>
				<asp:listitem value="6">06</asp:listitem>
				<asp:listitem value="7">07</asp:listitem>
				<asp:listitem value="8">08</asp:listitem>
				<asp:listitem value="9">09</asp:listitem>
				<asp:listitem value="10">10</asp:listitem>
				<asp:listitem value="11">11</asp:listitem>
				<asp:listitem value="12">12</asp:listitem>
				<asp:listitem value="13">13</asp:listitem>
				<asp:listitem value="14">14</asp:listitem>
				<asp:listitem value="15">15</asp:listitem>
				<asp:listitem value="16">16</asp:listitem>
				<asp:listitem value="17">17</asp:listitem>
				<asp:listitem value="18">18</asp:listitem>
				<asp:listitem value="19">19</asp:listitem>
				<asp:listitem value="20">20</asp:listitem>
				<asp:listitem value="21">21</asp:listitem>
				<asp:listitem value="22">22</asp:listitem>
				<asp:listitem value="23">23</asp:listitem>
				<asp:listitem value="24">24</asp:listitem>
				<asp:listitem value="25">25</asp:listitem>
				<asp:listitem value="26">26</asp:listitem>
				<asp:listitem value="27">27</asp:listitem>
				<asp:listitem value="28">28</asp:listitem>
				<asp:listitem value="29">29</asp:listitem>
				<asp:listitem value="30">30</asp:listitem>
				<asp:listitem value="31">31</asp:listitem>
			</asp:dropdownlist>
			/
			<asp:dropdownlist runat="server" class="insideBoxText11" id="drpYear" tabindex="72" Width="55px">
				<asp:listitem value="">Year</asp:listitem>
			</asp:dropdownlist>	
		</li>
		<li>
			<label for="email"><span id="spnEmail" runat="server">Email:</span></label>
			<asp:requiredfieldvalidator id="rfEmail" runat="server" controltovalidate="txtEmail" text="*" errormessage=""
				display="none"></asp:requiredfieldvalidator>
			<asp:regularexpressionvalidator id="revEmail" runat="server" controltovalidate="txtEmail" text="*" errormessage="Invalid email address."
				display="none" enableclientscript="True"></asp:regularexpressionvalidator>
			<asp:textbox id="txtEmail" runat="server" maxlength="100" tabindex="80" Width="200px"></asp:textbox> 						
		</li>
		<li>
			<label for="password"><span id="spnPassword" runat="server">Password:</span></label>
			<asp:requiredfieldvalidator id="rfPassword" runat="server" controltovalidate="txtPassword" text="*" errormessage=""
				display="none"></asp:requiredfieldvalidator>
			<asp:regularexpressionvalidator id="revPassword" runat="server" controltovalidate="txtPassword" text="*" errormessage="Passwords should be 4-20 characters and a combination of upper and lower-case letters, numbers, and underscores (_) only. Do not use spaces."
				display="none"></asp:regularexpressionvalidator>
			<asp:requiredfieldvalidator id="rfConfirmPassword" runat="server" controltovalidate="txtPassword" text="*" errormessage=""
				display="none"></asp:requiredfieldvalidator>
			<asp:textbox id="txtPassword" runat="server" maxlength="20" textmode="Password"	tabindex="90" Width="200px"></asp:textbox>
		</li>
		
		<!-- Promo code -->
		<!--
																											
			<span id="spnCouponCode" runat="server">Coupon Code</span>																																														
			<asp:textbox id="txtCouponCode" runat="server" maxlength="20" tabindex="100" ></asp:textbox>
		    <div class="note" style="float:left;">Optional.</div>

		-->				
	</ol>
</fieldset>
<br style="clear: both;" />
<!-- CAPTCHA -->
<ajax:AjaxPanel ID="ajpCaptcha" runat="server">
<cc2:captchacontrol id="CaptchaControl1" runat="server" text="Type the security code<br>exactly as it appears." width="250" height="60" captchlength="5" showsubmitbutton="false" />
<br style="clear: both;" />
</ajax:AjaxPanel>
<asp:imagebutton id="btnRegister" OnClientClick="setTimeout('parent.change_frame_height(document.body.offsetHeight)',500);" onclick="btnRegister_Click" runat="server" imageurl="~/images/registration_flow/btn_signup_136x41.gif" CssClass="regNextButton"
				alternatetext="Next Step" width="136" height="41" causesvalidation="False" tabindex="110"></asp:imagebutton>
	
<br style="clear: both;" />
<p class="note" >
By clicking "Next Step" you are agreeing to the <br />
<a onclick="javascript:window.open('../overview/TermsAndConditions.aspx','TandC','width=500,height=400,resizable=yes,scrollbars=yes');false;" href="#">
	Terms &amp; Conditions Agreement</a>.<br />
Kaneva does not spam and 
<a onclick="javascript:window.open('../overview/privacy.aspx','Priv','width=500,height=400,resizable=yes,scrollbars=yes');false;" href="#">
	respects your privacy.</a>					
</p>
</ajax:ajaxpanel>
	
</div>
		
    </div>
    </form>
</body>


</html>