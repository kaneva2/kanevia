///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Security.Cryptography;
using log4net;
using KlausEnt.KEP.Kaneva.framework.widgets;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{

	public partial class registerFrame : BasePage
	{
		private void Page_Load(object sender, System.EventArgs e)
		{
			Page.ClientScript.RegisterStartupScript(GetType(), "change_frame_height", "parent.change_frame_height(document.body.offsetHeight);", true);
			
			if (!IsPostBack)
			{

				// Special case scenario for joining from a community / co-brand
				int community_id = 0;

				try
				{
					community_id = Convert.ToInt32(Request.QueryString["joinid"]);
				}
				catch { }

				// Get the join configuration settings
				DataRow drJoin = CommunityUtility.GetJoinConfig(community_id);


				if (Request.Cookies["refuser"] != null)
				{
					txtCouponCode.Enabled = false;

					// Get most recent non-expired promo with type referral.
					txtCouponCode.Text = StoreUtility.GetActivePromotionCouponCodeByType(Convert.ToInt32(Constants.ePROMOTION_TYPE.REFERRAL));
				}
				else
				{
					txtCouponCode.Text = "New_User_Package";
				}

				// Was this user invited by a friend?  If so, put this email in the email field  
				// and and disable it so the user can not change it for this registration
				if (Request["i"] != null)
				{
					try
					{
						int inviteId = Convert.ToInt32(Request["i"]);
						DataRow drInvite = UsersUtility.GetInvite(inviteId, Request["k"].ToString());

						if (drInvite != null && Convert.ToInt32(drInvite["invite_status_id"]) == (int)Constants.eINVITE_STATUS.UNREGISTER)
						{
							txtEmail.Text = Server.UrlDecode(drInvite["email"].ToString());
							txtEmail.Enabled = false;

							if (drInvite["to_name"] != null)
							{
								string[] name = drInvite["to_name"].ToString().Split(' ');

								if (name.Length > 0)
								{
									txtFirstName.Text = name[0];

									for (int i = 1; i < name.Length; i++)
									{
										if (txtLastName.Text.Length > 0) txtLastName.Text += " ";
										txtLastName.Text += name[i];
									}
								}
							}

							// update the invite to show user clicked the link in invite email
							UsersUtility.UpdateInviteClick(inviteId);
						}
					}
					catch { }
				}

				drpCountry.DataTextField = "Name";
				drpCountry.DataValueField = "CountryId";
				drpCountry.DataSource = WebCache.GetCountries();
				drpCountry.DataBind();

				drpCountry.Items.Insert(0, new ListItem("United States", Constants.COUNTRY_CODE_UNITED_STATES));

				//AddBreadCrumb(new BreadCrumb("Registration - Info", GetCurrentURL(), "", 0));

				if (Request.UrlReferrer != null && Request.QueryString["contest"] != null)
				{
					ViewState["rurl"] = Request.UrlReferrer.AbsoluteUri.ToString();
				}

				if (Request.QueryString["redeem"] != null)
				{
					ViewState["redeem"] = true;
				}

				LoadYears();

				ShowCaptcha();

				txtUserName.Focus();
			}

			string scriptString;

			// Set up regular expression validators
			revEmail.ValidationExpression = Constants.VALIDATION_REGEX_EMAIL;
			revPassword.ValidationExpression = Constants.VALIDATION_REGEX_PASSWORD;
			revUsername.ValidationExpression = Constants.VALIDATION_REGEX_USERNAME;

			// Script to enable/disable zip code field base on selected country
			drpCountry.Attributes.Add("onchange", "CountryCheck ();");

			scriptString = "<script language=JavaScript>\n";
			scriptString += "function CountryCheck (){\n";
			scriptString += " if (document.getElementById('" + drpCountry.ClientID + "').value == 'US')\n";
			scriptString += " {document.getElementById('" + txtPostalCode.ClientID + "').disabled = false;\n";
			scriptString += " } else { \n";
			scriptString += "  document.getElementById('" + txtPostalCode.ClientID + "').disabled = true;\n";
			scriptString += "}}</script>";

			if (!Page.ClientScript.IsClientScriptBlockRegistered("CountryCheck"))
			{
				Page.ClientScript.RegisterStartupScript(this.GetType(), "CountryCheck", scriptString);
			}
		}

		#region Helper Methods
		protected void LicenseValidate(object source, ServerValidateEventArgs args)
		{
			args.IsValid = this.chkCopyright.Checked;
		}

		private void LoadYears()
		{
			int startYear = Convert.ToInt16(DateTime.Now.Year) - 13;

			drpYear.Items.Add(new ListItem("> " + startYear.ToString(), (startYear + 1).ToString()));

			int currentYear = Convert.ToInt16(DateTime.Now.Year) - 13;

			for (int i = currentYear; i > 1940; i--)
			{
				drpYear.Items.Add(new ListItem(i.ToString(), i.ToString()));
			}
			drpYear.Items.Add(new ListItem("< 1940", "1940"));
			drpYear.SelectedIndex = 0;
		}

		private void ShowCaptcha()
		{
			string visitorIP = Common.GetVisitorIPAddress();
			DataTable dtUsers = UsersUtility.GetUsersByIp(visitorIP, KanevaGlobals.CaptchaRegistrationWindow);

			if (dtUsers.Rows.Count == 0)
			{
				CaptchaControl1.Visible = false;
			}
			
		}

		private bool EnforceCaptcha()
		{
			bool enforce = true;
			string visitorIP = Common.GetVisitorIPAddress();
			DataTable dtUsers = UsersUtility.GetUsersByIp(visitorIP, KanevaGlobals.CaptchaRegistrationWindow);

			if (dtUsers.Rows.Count == 0)
			{
				enforce = false;
			}
			
			return enforce;
		}

		/// <summary>
		/// Creates the default home page for a newly registered user. Places a set of default
		/// widgets on the page.
		/// </summary>
		/// <returns>bool - true on success</returns>
		private bool CreateDefaultUserHomePage(string username, int userId)
		{
			if (userId.Equals(0))
			{
				return false;
			}

			try
			{
				//create a personal channel
				Community newUserPrifile = new Community();
				newUserPrifile.PlaceTypeId = 0;
				newUserPrifile.Name = username;
				newUserPrifile.NameNoSpaces = username.Replace(" ", "");
				newUserPrifile.Description = "NEW";
				newUserPrifile.CreatorId = userId;
				newUserPrifile.CreatorUsername = username;
				newUserPrifile.IsPublic = "Y";
				newUserPrifile.IsAdult = "N";
				newUserPrifile.IsPersonal = 1; //true
				newUserPrifile.CommunityTypeId = (int)CommunityType.USER;
				newUserPrifile.StatusId = (int)CommunityStatus.ACTIVE;

				int profileId = GetCommunityFacade.InsertCommunity(newUserPrifile);

				//if profile successfully created add user as member
				if (profileId > 0)
				{
					CommunityMember member = new CommunityMember();
					member.CommunityId = profileId;
					member.UserId = userId;
					member.AccountTypeId = (int)CommunityMember.CommunityMemberAccountType.OWNER;
					member.Newsletter = "Y";
					member.InvitedByUserId = 0;
					member.AllowAssetUploads = "Y";
					member.AllowForumUse = "Y";
					member.StatusId = (int)CommunityMember.CommunityMemberStatus.ACTIVE;

					GetCommunityFacade.InsertCommunityMember(member);
				}

				// Give each user three friends groups: 
				FriendGroup friendGroup = new FriendGroup();
				friendGroup.OwnerId = userId;

				//create Family
				friendGroup.Name = "Family";
				GetUserFacade.InsertFriendGroup(friendGroup);

				//create Coworkers
				friendGroup.Name = "Coworkers";
				GetUserFacade.InsertFriendGroup(friendGroup);

				//create Inner Circle
				friendGroup.Name = "Inner Circle";
				GetUserFacade.InsertFriendGroup(friendGroup);

				return true;
			}
			catch (Exception e)
			{
				m_logger.Error(e.ToString());
			}

			return false;
		}

        /// <summary>
        /// Creates the default home world for a newly registered user. 
        /// </summary>
        /// <returns>bool - true on success</returns>
        private bool CreateDefaultUserHomeWorld(string username, int userId)
        {
            if (userId.Equals(0))
            {
                return false;
            }

            try
            {
                string worldName = username + " World";
                int worldNameIndex = 0;
                bool invalidNameFound = GetCommunityFacade.IsCommunityNameTaken(false, worldName, 0);

                while (invalidNameFound)
                {
                    worldNameIndex++;
                    invalidNameFound = GetCommunityFacade.IsCommunityNameTaken(false, worldName + " " + worldNameIndex, 0);
                }

                //create a personal channel
                Community homeWorld = new Community();
                homeWorld.PlaceTypeId = 0;
                homeWorld.Name = worldName;
                homeWorld.NameNoSpaces = worldName.Replace(" ", "");
                homeWorld.Description = username + " Home World";
                homeWorld.CreatorId = userId;
                homeWorld.CreatorUsername = username;
                homeWorld.IsPublic = "Y";
                homeWorld.IsAdult = "N";
                homeWorld.IsPersonal = 0; //false
                homeWorld.CommunityTypeId = (int)CommunityType.HOME;
                homeWorld.StatusId = (int)CommunityStatus.ACTIVE;

                int communityId = GetCommunityFacade.InsertCommunity(homeWorld);
                bool success = (communityId > 0);

                //if profile successfully created add user as member
                if (success)
                {
                    CommunityMember member = new CommunityMember();
                    member.CommunityId = communityId;
                    member.UserId = userId;
                    member.AccountTypeId = (int)CommunityMember.CommunityMemberAccountType.OWNER;
                    member.Newsletter = "Y";
                    member.InvitedByUserId = 0;
                    member.AllowAssetUploads = "Y";
                    member.AllowForumUse = "Y";
                    member.StatusId = (int)CommunityMember.CommunityMemberStatus.ACTIVE;

                    GetCommunityFacade.InsertCommunityMember(member);

                    GetUserFacade.UpdateUserHomeCommunityId(userId, communityId);
                }

                return success;
            }
            catch (Exception e)
            {
                m_logger.Error(e.ToString());
            }

            return false;
        }

		/// <summary>
		/// Checks for validation errors and hilites approppriate section text
		/// </summary>
		private void ShowPageErrors()
		{
			string errClassname = "bold red";

			spnEmail.Attributes["class"] = "";

			spnBirthday.Attributes["class"] = "";
			spnUsername.Attributes["class"] = "";
			spnPassword.Attributes["class"] = "";



			// first name
			spnFirstName.Attributes["class"] = !rfFirstName.IsValid ? errClassname : "";

			// last name
			spnLastName.Attributes["class"] = !rfLastName.IsValid ? errClassname : "";

			// email
			if (!rfEmail.IsValid || !revEmail.IsValid)
			{
				spnEmail.Attributes["class"] = errClassname;
			}

			// country
			spnCountry.Attributes["class"] = !rfdrpCountry.IsValid ? errClassname : "";

			// zip code
			spnPostalCode.Attributes["class"] = !rftxtPostalCode.IsValid ? errClassname : "";

			// birthday
			if (!rfMonth.IsValid || !rfDay.IsValid || !rfYear.IsValid)
			{
				spnBirthday.Attributes["class"] = errClassname;
			}

			// gender
			if (!rbMale.Checked && !rbFemale.Checked)
			{
				cvGender.IsValid = false;
			}
			spnGender.Attributes["class"] = !cvGender.IsValid ? errClassname : "";

			// username
			if (!rfUsername.IsValid || !revUsername.IsValid)
			{
				spnUsername.Attributes["class"] = errClassname;
			}

			// password
			if (!rfPassword.IsValid || !revPassword.IsValid)
			{
				spnPassword.Attributes["class"] = errClassname;
			}
		}

		/// <summary>
		/// Adds a users first time visit source information
		/// </summary>
		public void AddUserAcquisitionSourceURL(HttpRequest request, int userId)
		{
			// Get the cookie from the request object
			HttpCookie firstRefCookie = request.Cookies[Constants.ACQUISITION_COOKIE_NAME];

			// Get the values from the cookie
			string host = KanevaGlobals.Decrypt(firstRefCookie[Constants.ACQUISITION_COOKIE_VALUE_HOST]);
			string fullUrl = KanevaGlobals.Decrypt(firstRefCookie[Constants.ACQUISITION_COOKIE_VALUE_ABSOLUTEURI]);
			int sourceTypeId = (int)Constants.eACQUISITION_SOURCE_TYPE.OTHER_SITE;

			// Check to see if source if from google
			if (host.ToLower().Contains("www.google."))
			{
				// Did user find kaneva from a paid keyword?
				if (request.QueryString[Constants.ACQUISITION_QUERYSTRING_GOOGLE_PAID_KEYWORD] != null)
				{
					sourceTypeId = (int)Constants.eACQUISITION_SOURCE_TYPE.GOOGLE_PAID_KEYWORD;
				}
				else
				{   // Must have been a regular google search
					sourceTypeId = (int)Constants.eACQUISITION_SOURCE_TYPE.GOOGLE_SEARCH;
				}
			} // See if user came from a referral banner add on another site
			else if (request.QueryString[Constants.ACQUISITION_QUERYSTRING_REFERRAL] != null)
			{
				sourceTypeId = (int)Constants.eACQUISITION_SOURCE_TYPE.REFERRAL;
			}

			// Add source url data to database
			UserFacade userFacade = new UserFacade();
			int ret = userFacade.InsertUserSourceAcquisitionUrl(userId, fullUrl, host, sourceTypeId);

			// Check to see if insert failed without throwing an exception
			if (ret == 0)
			{
				m_logger.Error("Error adding user acquisition source info. UserId=" + userId + ". Return code=0.");
			}
		}

		#endregion

		#region Event Handlers
		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnRegister_Click(object sender, ImageClickEventArgs e)
		{
			cvBlank.IsValid = true;
			cvBlank.ErrorMessage = string.Empty;

			if (drpCountry.SelectedValue == "US")
			{
				rftxtPostalCode.Enabled = true;
			}
			else
			{
				rftxtPostalCode.Enabled = false;
			}

			Page.Validate();
			if (!Page.IsValid || (!rbFemale.Checked && !rbMale.Checked))
			{
				ShowPageErrors();

				cvBlank.IsValid = false;
				cvBlank.ErrorMessage = "Some required fields are blank or invalid.";
				return;
			}

			if (EnforceCaptcha())
			{
				if ((Request["CaptchaControl1"] == null) || (!CaptchaControl1.UserValidated))
				{
					cvBlank.IsValid = false;
					cvBlank.ErrorMessage = "Unique security code did not match.";
					return;
				}
			}

			//compile users birthday
			DateTime dtBirthDate = new DateTime();
			try
			{
				dtBirthDate = new DateTime(Convert.ToInt32(drpYear.SelectedValue), Convert.ToInt32(drpMonth.SelectedValue), Convert.ToInt32(drpDay.SelectedValue));
			}
			catch
			{
				cvBlank.IsValid = false;
				cvBlank.ErrorMessage = "Birthdate is an invalid date.";
				return;
			}

			//check to see if applicant is a minor before proceeding
			if (IsUserAMinor(dtBirthDate))
			{
				//display message alerting user they are under aged
				cvBlank.IsValid = false;
				cvBlank.ErrorMessage = "We apologize but users under the age of " + KanevaGlobals.MinorCutOffAge + " are not allowed to register.";
				return;
			}

			//check to see if CouponCode is valid
			if (txtCouponCode.Text != null && txtCouponCode.Text != "" && txtCouponCode.Text != "New_User_Package")
			{
				int promoID = StoreUtility.GetActivePromotionIDByCouponCode(txtCouponCode.Text);

				spnCouponCode.Attributes["class"] = "";

				if (promoID <= 0)
				{
					spnCouponCode.Attributes["class"] = "bold red";
					//display message 
					valSum.DisplayMode = ValidationSummaryDisplayMode.SingleParagraph;

					cvBlank.IsValid = false;
					cvBlank.ErrorMessage = "The coupon code used is not a valid code.";
					ShowPageErrors();
					return;
				}
			}



			// Check to see if users IP address has been banned
			try
			{
				DataRow drIPBan = UsersUtility.GetIPBan(Common.GetVisitorIPAddress());
				if (drIPBan != null && drIPBan.Table.Rows.Count > 0)
				{
					// Now check to see if account is still banned
					if (Convert.ToDateTime(drIPBan["ban_end_date"]) < DateTime.Now)
					{   // Ban has expired, un-ban ip and continue
						UsersUtility.RemoveIPBan(Common.GetVisitorIPAddress());
					}
					else
					{   // Ban still active, show
						ShowPageErrors();

						cvBlank.IsValid = false;
						cvBlank.ErrorMessage = "<div style=\"padding:20px 0 10px 0;\"><b>Registration failed:</b> This account has been locked by the Kaneva administrator.</div>";
						return;
					}
				}
			}
			catch (Exception ex)
			{
				m_logger.Error("Error checking user IP against Banned IP List: ", ex);
			}

			// If user is currently logged in, log them out before continuing, if not, 
			// user gets redirected to the wrong page and it is a dead end for them.
			if (Request.IsAuthenticated)
			{
				System.Web.HttpCookie aCookie;
				int limit = Request.Cookies.Count - 1;
				for (int i = Request.Cookies.Count - 1; i != 0; i--)
				{
					aCookie = Request.Cookies[i];
					aCookie.Expires = DateTime.Now.AddDays(-1);
					Response.Cookies.Add(aCookie);
				}

				FormsAuthentication.SignOut();
				Session.Abandon();
			}

			int source_community_id = 0;
			try
			{
				source_community_id = Convert.ToInt32(Request.QueryString["joinid"]);
			}
			catch { }

			int communityInviteId = 0;
			try
			{
				if (Request["cInv"] != null)
				{
					communityInviteId = Convert.ToInt32(Request.QueryString["cInv"]);
				}
			}
			catch { }

			string strUserName = Server.HtmlEncode(txtUserName.Text.Trim());
			string strPassword = Server.HtmlEncode(txtPassword.Text.Trim());

			// Hash password
			byte[] salt = new byte[9];
			new RNGCryptoServiceProvider().GetBytes(salt);
			string hashPassword = FormsAuthentication.HashPasswordForStoringInConfigFile(UsersUtility.MakeHash(strPassword + strUserName.ToLower()) + Convert.ToBase64String(salt), "MD5");

			// Generate a unique key_code for registration purposes
			string keyCode = KanevaGlobals.GenerateUniqueString(20);

			// Create a registration Key
			string regKey = KanevaGlobals.GenerateUniqueString(50);

			// If they were invited from a friend, no need to send out email.
			bool bInvitedByFriend = (Request["i"] != null);

			string gender = rbMale.Checked ? "M" : "F";

			// Set the default Display Name
			string displayName = "";

			// If user is under 18, set display name to first name and last initial only
			if (DateTime.Today.AddYears(-18) < dtBirthDate)
			{
				displayName = String.Concat(Server.HtmlEncode(txtFirstName.Text), " ", Server.HtmlEncode(txtLastName.Text).Substring(0, 1));
			}
			else
			{
				int displayNameLen = (Server.HtmlEncode(txtFirstName.Text.Trim()) + Server.HtmlEncode(txtLastName.Text.Trim())).Length;
				if (displayNameLen >= 30)
				{
					if (displayNameLen == 30)
					{
						displayName = String.Concat(Server.HtmlEncode(txtFirstName.Text), Server.HtmlEncode(txtLastName.Text));
					}
					else
					{
						displayName = String.Concat(Server.HtmlEncode(txtFirstName.Text) + Server.HtmlEncode(txtLastName.Text)).Substring(0, 29);
					}
				}
				else
				{
					displayName = String.Concat(Server.HtmlEncode(txtFirstName.Text), " ", Server.HtmlEncode(txtLastName.Text));
				}
			}

			int result = 0;
			int userId = 0;

			// Make sure it is not on the invalid user names list
			if (UsersUtility.IsReservedName(strUserName))
			{
				result = -3;
			}
			else
			{
				// This returns userId
				userId = GetUserFacade.InsertUser(strUserName, hashPassword, Convert.ToBase64String(salt), 2, 1,
					Server.HtmlEncode(txtFirstName.Text), Server.HtmlEncode(txtLastName.Text), displayName, Server.HtmlEncode(gender), "", Server.HtmlEncode(txtEmail.Text), dtBirthDate, keyCode,
					Server.HtmlEncode(drpCountry.SelectedValue), Server.HtmlEncode(txtPostalCode.Text), regKey, Common.GetVisitorIPAddress(), (int)Kaneva.Constants.eUSER_STATUS.REGNOTVALIDATED, source_community_id);

				// Did we get a success?
				if (userId > 0)
				{
					result = 0;
				}
				else
				{
					result = userId;
				}
			}

			switch (result)
			{
				case 0:
					{

						DataTable dtJoinCommunities = null;
						DataRow drJoin = null;

						// Check to see if user has acquisition cookie, if so, then let's log the info
						// about where the user originally came from
						try
						{
							if (Request.Cookies[Constants.ACQUISITION_COOKIE_NAME] != null && Request.Cookies[Constants.ACQUISITION_COOKIE_NAME].Value != string.Empty)
							{
								// Update the database
								AddUserAcquisitionSourceURL(Page.Request, userId);
							}
						}
						catch (Exception ex)
						{
							m_logger.Error("Error adding user acquisition source info. UserId=" + userId + ". ", ex);
						}

						// Create their home page
						if (CreateDefaultUserHomePage(strUserName, userId) && CreateDefaultUserHomeWorld(strUserName, userId))
						{
							// home page was built successfullly
							//get default kaneva community
							string defaultCommunityId = System.Configuration.ConfigurationManager.AppSettings["Default_Kaneva_Community_Id"];
							if (defaultCommunityId != null && defaultCommunityId.Trim() != "")
							{
								int communityId = Convert.ToInt32(defaultCommunityId); //logic to prevent convrt exceptions

								//auto join the user to the designated kaneva community
								CommunityMember member = new CommunityMember();
								member.CommunityId = communityId;
								member.UserId = userId;
								member.AccountTypeId = (int)CommunityMember.CommunityMemberAccountType.SUBSCRIBER;
								member.Newsletter = "Y";
								member.InvitedByUserId = 0;
								member.AllowAssetUploads = "N";
								member.AllowForumUse = "Y";
								member.StatusId = (int)CommunityMember.CommunityMemberStatus.ACTIVE;

								GetCommunityFacade.InsertCommunityMember(member);

							}

							// Are they coming from a community Invite?
							if (communityInviteId > 0)
							{
								CommunityMember member = new CommunityMember();
								member.CommunityId = communityInviteId;
								member.UserId = userId;
								member.AccountTypeId = (int)CommunityMember.CommunityMemberAccountType.SUBSCRIBER;
								member.Newsletter = "Y";
								member.InvitedByUserId = 0;
								member.AllowAssetUploads = "N";
								member.AllowForumUse = "Y";
								member.StatusId = (int)CommunityMember.CommunityMemberStatus.ACTIVE;

								GetCommunityFacade.InsertCommunityMember(member);
							}

							// Now check to see if there are any other communities the user needs to be joined into
							// based on the join profile for the source community
							if (source_community_id > 0)
							{
								//get other communities the new user is supposed to be auto joined to
								dtJoinCommunities = CommunityUtility.GetJoinCommunities(source_community_id);

								//automatically add them as active members of these communities
								if (dtJoinCommunities != null)
								{
									int join_comm_id = 0;

									for (int i = 0; i < dtJoinCommunities.Rows.Count; i++)
									{
										try
										{
											join_comm_id = Convert.ToInt32(dtJoinCommunities.Rows[i]["join_community_id"]);
											if (join_comm_id > 0)
											{
												CommunityMember member = new CommunityMember();
												member.CommunityId = join_comm_id;
												member.UserId = userId;
												member.AccountTypeId = (int)CommunityMember.CommunityMemberAccountType.SUBSCRIBER;
												member.Newsletter = "Y";
												member.InvitedByUserId = 0;
												member.AllowAssetUploads = "N";
												member.AllowForumUse = "Y";
												member.StatusId = (int)CommunityMember.CommunityMemberStatus.ACTIVE;

												GetCommunityFacade.InsertCommunityMember(member);
											}
										}
										catch
										{ }
									}
								}

								// Get the join settings for the source community
								drJoin = CommunityUtility.GetJoinConfig(source_community_id);

								//if there are particular join settings set them
								if (drJoin != null)
								{
									try
									{
										// get the new user account just created
										User user = GetUserFacade.GetUser(userId);
										Community newProfile = GetCommunityFacade.GetCommunity(user.CommunityId);

										string avatarPath = string.Empty;

										// check to see if there is a default theme to apply to the new users personal pages
										try
										{
											if (user.Gender == "F")
											{
												avatarPath = drJoin["user_profile_female_avatar_path"].ToString();
											}
											else
											{
												avatarPath = drJoin["user_profile_male_avatar_path"].ToString();
											}
										}
										catch { }

										// apply avatar if one exists for this join
										if (avatarPath != string.Empty && avatarPath.Length > 0)
										{
											string fileStoreHack = "";
											if (KanevaGlobals.FileStoreHack != "")
											{
												fileStoreHack = KanevaGlobals.FileStoreHack;
											}

											string imagePath = KanevaGlobals.ContentServerPath + System.IO.Path.DirectorySeparatorChar + avatarPath;
											string avatar_sm = avatarPath.Substring(0, avatarPath.IndexOf(".")) + "_sm" + avatarPath.Substring(avatarPath.IndexOf("."));
											string avatar_me = avatarPath.Substring(0, avatarPath.IndexOf(".")) + "_me" + avatarPath.Substring(avatarPath.IndexOf("."));
											string avatar_lg = avatarPath.Substring(0, avatarPath.IndexOf(".")) + "_la" + avatarPath.Substring(avatarPath.IndexOf("."));
											string avatar_xl = avatarPath.Substring(0, avatarPath.IndexOf(".")) + "_xl" + avatarPath.Substring(avatarPath.IndexOf("."));

											//update image paths
											newProfile.ThumbnailPath = imagePath;
											newProfile.ThumbnailType = string.Empty;
											newProfile.HasThumbnail = true; //originally was false but this logic seems to be setting a thumbnail so changing to true
											newProfile.ThumbnailSmallPath = avatar_sm;
											newProfile.ThumbnailMediumPath = avatar_me;
											newProfile.ThumbnailLargePath = avatar_lg;
											newProfile.ThumbnailXlargePath = avatar_xl;

											GetCommunityFacade.UpdateCommunity(newProfile);
										}
									}
									catch (Exception Exc)
									{
										m_logger.Error("Error in reg", Exc);
									}
								}
								//todo do we need to set a default profile for the user here??
							}
						}
						else
						{
							// Problem creating default home page
							GetUserFacade.CleanUpRegistrationIssue(strUserName);

							cvBlank.IsValid = false;
							cvBlank.ErrorMessage = "The default home page for this user could not be created.";
							return;
						}

						// Send out verification email
						if (!bInvitedByFriend)
						{
							if (Request.QueryString["joinid"] != null && Request.QueryString["joinid"].ToString().Length > 0)
							{
								string email_body_top = "";
								string email_body_bottom = "";
								string subject = "";
								int community_id = 0;

								if (Request.QueryString["joinid"] != null)
								{
									try
									{
										community_id = Convert.ToInt32(Request.QueryString["joinid"]);

										drJoin = CommunityUtility.GetJoinConfig(community_id);
									}
									catch { }
								}

								if (drJoin != null)
								{
									if (drJoin["email_body_top"] != null && drJoin["email_body_top"].ToString().Length > 0)
									{
										email_body_top = drJoin["email_body_top"].ToString();
									}
									if (drJoin["email_body_bottom"] != null && drJoin["email_body_bottom"].ToString().Length > 0)
									{
										email_body_bottom = drJoin["email_body_bottom"].ToString();
									}
									if (drJoin["email_subject"] != null && drJoin["email_subject"].ToString().Length > 0)
									{
										subject = drJoin["email_subject"].ToString();
									}

									// set the return url
									string retURL = string.Empty;
									if (Request.QueryString["redeem"] != null)
									{
										retURL += "redeem";
									}

									string joinId = "&joinid=" + Request.QueryString["joinid"].ToString();

									// send validation email				 
									try
									{
										MailUtilityWeb.SendRegistrationEmailCoBrand(
											email_body_top,
											email_body_bottom,
											subject,
											Server.HtmlEncode(txtEmail.Text), strUserName, keyCode, Server.UrlEncode(retURL) + joinId);
									}
									catch { }
								}
							}
							else if (Request.QueryString["redeem"] != null)
							//if (ViewState ["redeem"] != null && Convert.ToBoolean (ViewState ["redeem"]) == true)
							{
								try
								{
									// if we came from the redeem gift card page, make sure the link in 
									// the email will redirect the user back into the redeem flow
									MailUtilityWeb.SendRegistrationEmail(strUserName, Server.HtmlEncode(txtEmail.Text), keyCode, "redeem");
								}
								catch { }
							}
							else
							{
								try
								{
									MailUtilityWeb.SendRegistrationEmail(strUserName, Server.HtmlEncode(txtEmail.Text), keyCode);
								}
								catch { }
							}
						}

						// If they were invited by a friend, glue them here, and give them any free points
						else if (bInvitedByFriend)
						{
							// Automatically set thier status since we know they clicked the invite email
							GetUserFacade.UpdateUserStatus(userId, (int)Constants.eUSER_STATUS.REGVALIDATED);

							// since invited by friend, user email is automatically validated, so award fame
							try
							{
								GetFameFacade.RedeemPacket(userId, (int)PacketId.WEB_OT_EMAIL_VALIATION, (int)FameTypes.World);
							}
							catch (Exception ex)
							{
								m_logger.Error("Error awarding Packet WEB_OT_EMAIL_VALIATION, userid=" + userId, ex);
							}

							DataRow drInvite = UsersUtility.GetInvite(Convert.ToInt32(Request["i"]), Request["k"].ToString());

							// Give the user the free Reward Points for this user registering
							// Only do the following if status is UNREGISTER so they cannot do it more than once!!!
							if (drInvite != null && Convert.ToInt32(drInvite["invite_status_id"]).Equals((int)Constants.eINVITE_STATUS.UNREGISTER))
							{
								int inviteSentByUserId = Convert.ToInt32(drInvite["user_id"]);

								int channelId = drInvite["channel_id"] == DBNull.Value ? -1 :
									Convert.ToInt32(drInvite["channel_id"]);

								if (channelId > 0)
								{
									//if there is channelId, add user as a member to the channel
									CommunityMember member = new CommunityMember();
									member.CommunityId = channelId;
									member.UserId = userId;
									member.AccountTypeId = (int)CommunityMember.CommunityMemberAccountType.SUBSCRIBER;
									member.Newsletter = "N";
									member.InvitedByUserId = inviteSentByUserId;
									member.AllowAssetUploads = "N";
									member.AllowForumUse = "Y";
									member.StatusId = (int)CommunityMember.CommunityMemberStatus.ACTIVE;

									GetCommunityFacade.InsertCommunityMember(member);

									UsersUtility.InsertUserConnection(channelId, userId, inviteSentByUserId);
								}
								else
								{
									//add user as a personal friend								
									int ret = GetUserFacade.AcceptFriend(userId, inviteSentByUserId);

									if (ret == 1)
									{
										// If you wants emails when new friends are added, send them an email
										if (GetUserFacade.GetUser(inviteSentByUserId).NotifyNewFriends)
										{
											try
											{
												MailUtilityWeb.SendFriendAcceptedInviteNotification(inviteSentByUserId, userId);
											}
											catch { }
										}
									}
								}

								UsersUtility.UpdateInvite(Convert.ToInt32(drInvite["invite_id"]), (int)Constants.eINVITE_STATUS.REGISTERED, userId);

								// Invite 5 friend awards
								UsersUtility.AwardInviteRegister(inviteSentByUserId, Common.GetVisitorIPAddress(), Convert.ToInt32(drInvite["invite_id"]), userId);

								// User completed fame packet
								try
								{
									GetFameFacade.RedeemPacket(inviteSentByUserId, (int)PacketId.WEB_INVITE_JOIN_SITE, (int)FameTypes.World);
								}
								catch (Exception ex)
								{
									m_logger.Error("Error awarding Packet WEB_OT_FRIENDS_INVITE_10, userid=" + inviteSentByUserId + ", inviteId=" + drInvite["invite_id"].ToString(), ex);
								}
							}
						}


						// Add default video to connected media for this user
						#region default video
						try
						{
							int assetId = 0;
							bool addDefaultVideo = true;

							if (drJoin != null && drJoin["user_profile_video_id"] != null)
							{
								try
								{
									assetId = Convert.ToInt32(drJoin["user_profile_video_id"]);

									if (assetId > 0)
									{
                                        StoreUtility.InsertAssetChannel(assetId, GetUserFacade.GetPersonalChannelId(userId));
										addDefaultVideo = false;
									}
								}
								catch
								{ }
							}

							if (addDefaultVideo)
							{
								assetId = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["DefaultNewUserVideoId"]);
								if (assetId > 0)
								{
                                    StoreUtility.InsertAssetChannel(assetId, GetUserFacade.GetPersonalChannelId(userId));
								}
							}
						}
						catch { }
						#endregion

						/*
						 * REFERRAL 
						 * If this from a share kaneva update as registered.
						 * At this point the user must be registered.						  
						 */
						#region Referral from share kaneva
						if (Request.Cookies["refuser"] != null)
						{
							try
							{
								int referringUserId = Convert.ToInt32(Request.Cookies["refuser"].Value);
								int referredUserId = UsersUtility.GetUserIdFromUsername(txtUserName.Text.Trim());

								GetMarketingFacade.AddReferral(referringUserId, referredUserId);
							}
							catch (Exception ex)
							{
								m_logger.Error(ex.ToString());
							}
						}
						#endregion

						/*
						 * COUPON CODE 
						 * Redeem coupon code                      
						 * At this point the user must be registered.						  
						 */
						#region Coupon Code
						if (txtCouponCode.Text != null && txtCouponCode.Text != "" && txtCouponCode.Text != "New_User_Package")
						{
							try
							{
								int promoID = StoreUtility.GetActivePromotionIDByCouponCode(txtCouponCode.Text);

								// Update user balances with the K-Point amounts
								Double pointBucketAmount = 0;
								Double pointBucketFreeAmount = 0;
								string pointBucketKEIPoint = Constants.CURR_KPOINT;

								DataRow drPointBucket = StoreUtility.GetPromotion(promoID);

								// No point bucket found here means something messed up somewhere
								if (drPointBucket == null)
								{
									// Mark it as item not found                               
								}
								else
								{
									pointBucketKEIPoint = drPointBucket["kei_point_id"].ToString();
									pointBucketAmount = Convert.ToDouble(drPointBucket["kei_point_amount"]);
									pointBucketFreeAmount = Convert.ToDouble(drPointBucket["free_points_awarded_amount"]);
								}
								// Any credits?
								if (pointBucketAmount > 0)
								{
									GetUserFacade.AdjustUserBalance(userId, pointBucketKEIPoint, pointBucketAmount, Constants.CASH_TT_SPECIAL);
								}
								// Any rewards?
								if (pointBucketFreeAmount > 0)
								{
									GetUserFacade.AdjustUserBalance(userId, drPointBucket["free_kei_point_id"].ToString(), pointBucketFreeAmount, Constants.CASH_TT_SPECIAL);
								}

								StoreUtility.AddWokItems(promoID, userId);
							}
							catch (Exception ex)
							{
								m_logger.Error(ex.ToString());
							}
						}
						#endregion

						// Redirect to register complete page
						if (ViewState["rurl"] != null)	 // This case was put here to handle registration for Contests and auto redirect back to the contest
						{
							// Automatically sign them in 
							FormsAuthentication.SetAuthCookie(userId.ToString(), false);

							Response.Redirect(ViewState["rurl"].ToString());
						}
						else if (Request["returnURL"] != null)
						{
							Response.Redirect(ResolveUrl(Request["returnURL"].ToString()));
						}
						else
						{
							// Automatically sign them in here so we know who they are
							// for next step if they decide to import contacts from email
							FormsAuthentication.SetAuthCookie(userId.ToString(), false);

							string joinid = string.Empty;
							if (Request.QueryString["joinid"] != null && Request.QueryString["joinid"].ToString().Length > 0)
							{
								joinid = "&joinid=" + Server.UrlEncode(Request.QueryString["joinid"].ToString());
							}

							// redirect them to validation page.  This page will decide where they should go next
							//Response.Redirect(ResolveUrl("~/register/kaneva/registerValidate.aspx?reg=1&email=" + Server.UrlEncode(txtEmail.Text.Trim()) + joinid));

							if (!Page.ClientScript.IsClientScriptBlockRegistered(GetType(), "changeParent"))
							{
								Page.ClientScript.RegisterClientScriptBlock(GetType(), "changeParent", "parent.change_parent_url('" + ResolveUrl("~/register/kaneva/registerValidate.aspx?reg=1&email=" + Server.UrlEncode(txtEmail.Text.Trim()) + joinid) + "');", true);
							}
						}
						break;
					}
				case -1:
					{
						cvBlank.IsValid = false;
						cvBlank.ErrorMessage = "?	The Nickname you requested isn't available. Please select another nickname.";
						break;
					}
				case -2:
					{
						cvBlank.IsValid = false;
						cvBlank.ErrorMessage = "The email address you provided is already associated with a Kaneva account. Please provide another email address and re-enter your password.";
						break;
					}
				case -3:
					{
						cvBlank.IsValid = false;
						cvBlank.ErrorMessage = "The Nickname already exists or is not allowed. Please change your nickname.";
						break;
					}
				case -4:
					{
						cvBlank.IsValid = false;
						cvBlank.ErrorMessage = "The default home page for this user could not be created.";
						break;
					}
			}
		}

		protected void btnCheckUsername_Click(object sender, ImageClickEventArgs e)
		{
			revUsername.Validate();

			if (revUsername.IsValid)
			{
				if (txtUserName.Text.Trim() != string.Empty)
				{
					if (UsersUtility.GetUserIdFromUsername(txtUserName.Text.Trim()) > 0)
					{
						spnCheckUsername.InnerText = "Not available";
						spnCheckUsername.Attributes.Add("class", "failure");
					}
					else
					{
						spnCheckUsername.InnerText = "Available";
						spnCheckUsername.Attributes.Add("class", "success");
					}
				}
			}
			else
			{
				spnCheckUsername.InnerText = "Nicknames must be 4-20 characters, and can contain upper or lower-case letters, numbers, and underscores (_) only. No spaces.";
				spnCheckUsername.Attributes.Add("class", "failure");
			}
		}


		#endregion

		#region Declarations

		protected PlaceHolder phBreadCrumb;	
		protected TextBox txtHomepage;
		protected TextBox txtDay, txtYear;
		protected CheckBox chkCopyright, chkBeta;
		// protected Literal litCSS;
		protected HtmlContainerControl  spnCoBrandCopy;
		protected CompareValidator cvEmail;
		protected CompareValidator cvPassword;
		private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
		#endregion
	}
}
