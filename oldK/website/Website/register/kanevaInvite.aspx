﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="kanevaInvite.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.kanevaInvite" %>
<%@ Register TagPrefix="UserControl" TagName="FacebookJS" Src="~/usercontrols/FacebookJavascriptInclude.ascx" %>

<!--[if IE 6]>
<link rel="stylesheet" href="css/new_IE.css" type="text/css" />
<![endif]-->

<link href="../css/themes-join/template.css" rel="stylesheet" type="text/css">	
<link href="../css/registration_flow/facebookconnect.css" rel="stylesheet" type="text/css">	
<usercontrol:FacebookJS runat="server"></usercontrol:FacebookJS>

<br /><br />
<table class="inviteouter" cellpadding="0" cellspacing="0" border="0" width="790" align="center">
	<tr>
		<td colspan="3" valign="top">
			<table class="invite" border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="frTopLeft"></td>
					<td class="frTop"></td>
					<td class="frTopRight"></td>
				</tr>
				<tr>
					<td class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" id="Img2" width="1" height="1" /></td>
					<td valign="top" class="frBgIntMembers">
						<h1>You've been invited to join Kaneva</h1>
						<p class="desc">Thousands of places to explore... your own 3D home for hanging out with your friends.
							Getting started is easy and free. Earn 100 Rewards for signing up!</p>

						<img onclick="FBLogin(FBAction.REGISTER);" src="~/images/facebook/FB_signup.png" runat="server" />
						<p class="link">or sign up by <a href="~/register/kaneva/registerinfo.aspx" runat="server">email</a></p>
					</td>
					<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" id="Img4" width="1" height="1" /></td>
				</tr>
				<tr>
					<td class="frBottomLeft"></td>
					<td class="frBottom"></td>
					<td class="frBottomRight"></td>
				</tr>
			</table>				
		</td>
	</tr>
</table>

