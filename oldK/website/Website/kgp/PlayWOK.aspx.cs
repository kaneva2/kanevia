///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva.kgp
{
	/// <summary>
	/// Tiny page to launch the game, or redirect to install page
	/// </summary>
	public class PlayWOK : NoBorderPage
	{
		protected System.Web.UI.WebControls.Literal imageLink, litKanevaUrl;
		protected System.Web.UI.HtmlControls.HtmlForm Form1;
		protected System.Web.UI.WebControls.Literal litPlayWok;
		protected Repeater	rptList1, latestBtmRpt, troubleRpt, latestTopRpt;

		protected PlayWOK ()
		{
			Title = "Virtual World of Kaneva Play Page";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			string requestTrackingGUID = "";
            // Request tracking
            if (Request.Params[Constants.cREQUEST_TRACKING_URL_PARAM] != null)
            {
				requestTrackingGUID = Request.Params[Constants.cREQUEST_TRACKING_URL_PARAM];
                // int iResult = GetUserFacade.AcceptTrackingRequest(trackingParam, KanevaWebGlobals.CurrentUser.UserId);
            }

			string patchUrl = KanevaGlobals.WokPatcherUrl;
			string gameName = KanevaGlobals.WokGameName;

			DataSet dsProblem = new DataSet();
			dsProblem.ReadXml(MapPath("../xml/playWOKnewsProblem.xml"));

			if (dsProblem != null)
			{
				rptList1.DataSource = dsProblem;
				rptList1.DataBind();
			}

			DataSet dsTrouble = new DataSet();
			dsTrouble.ReadXml(MapPath("../xml/playWOKnewsTrouble.xml"));

			if (dsTrouble != null)
			{
				troubleRpt.DataSource = dsTrouble;
				troubleRpt.DataBind();
			}

			DataSet dsLatestTop = new DataSet();
			dsLatestTop.ReadXml(MapPath("../xml/playWOKnewsLatestTop.xml"));

			if (dsLatestTop != null)
			{
				latestTopRpt.DataSource = dsLatestTop;
				latestTopRpt.DataBind();
			}

			// Load the news and info from the XML file
			DataSet dsLatestBtm = new DataSet();
			dsLatestBtm.ReadXml(MapPath("../xml/playWOKnewsLatestBottom.xml"));

			if (dsLatestBtm != null)
			{
				latestBtmRpt.DataSource = dsLatestBtm;
				latestBtmRpt.DataBind();
			}


			//logic for swapping out image for promotional offers
			//if promotional path is blank or null default to standard image.
			//otherwise show promotional image
			string promoWOKImagePath = System.Configuration.ConfigurationManager.AppSettings ["promoWOKImagePath"];
			if((promoWOKImagePath != null) && (promoWOKImagePath.Length > 0))
			{
				this.imageLink.Text = "<a href=" + this.ResolveUrl("~/woktest") + "><img runat=\"server\" src=\"" + promoWOKImagePath + "\" border=\"0\"></a>";
			}
			else
			{
				this.imageLink.Text = "<a href=" + this.ResolveUrl("~/woktest") + "><img runat=\"server\" src=\"../images/download-get-ready2.jpg\" border=\"0\"></a>";
			}

			string destIdStr = "0";
            string stpUrl = StpUrl.MakeUrlPrefix( KanevaGlobals.WokGameId.ToString() ); // default stp url
            string email = ""; 
            string tryOn = "";

            if (Request["email"] != null)
                email = Request["email"];

            // Check to see if this is a new url.  New url's will check to see where user is on this
            // page instead of the prior page so we have a more up-to-date location of user in world
            if (Request["new"] != null && Request["new"].Length > 1)
            {
                string userId = "";
                string commId = "";

                if (Request["userId"] != null && Request["userId"].Length > 0)
                {
                    userId = Request["userId"].ToString ();
                }
                else if (Request["commId"] != null && Request["commId"].Length > 0)
                {
                    commId = Request["commId"].ToString ();
                }

                if (userId.Length > 0)
                {
                    string inGameId = "";

                    DataTable dt = UsersUtility.GetUserLoggedInGameId (Convert.ToInt32 (userId));
                    foreach (DataRow dr in dt.Rows)
                    {
                        inGameId = dr["game_id"].ToString ();
                    }

                    // is this person at a 3d app?
                    if (inGameId == KanevaGlobals.WokGameId.ToString ())
                    {
                        User u = new UserFacade ().GetUser (Convert.ToInt32 (userId));
                        if (u.UserId > 0)
                        {
                            stpUrl += "/" + StpUrl.MakePersonUrlPath (u.Username) + "/?";
                        }
                        else
                        {
                            // shouldn't ever happen, only if caller bad
                            Response.Write ("<script type=\"text/javascript\">alert('Invalid destination for Person Url: " + userId + "' );</script>");
                            return;
                        }
                    }
                    else if (inGameId != "0")
                    {
                        // it's a 3d APP
                        stpUrl = StpUrl.MakeUrlPrefix (inGameId) + "/?" + Request.ServerVariables["QUERY_STRING"];
                        litKanevaUrl.Text = stpUrl;
                    }
                    else
                    {
                        // not logged in to WOK
                        User u = new UserFacade ().GetUser (Convert.ToInt32 (userId));
                        if (u.UserId > 0)
                        {
                            stpUrl += "/" + StpUrl.MakeAptUrlPath (u.Username) + "/?";
                        }
                        else
                        {
                            // shouldn't ever happen, only if caller bad
                            Response.Write ("<script type=\"text/javascript\">alert('Invalid destination Apt Url: " + userId + "' );</script>");
                            return;
                        }
                    }
                }
                else if (commId.Length > 0)
                {
                    Community c = new CommunityFacade ().GetCommunity (Convert.ToInt32 (commId));

                    if (c.WOK3App.GameId > 0)
                    { // this community is a 3D App
                        stpUrl = StpUrl.MakeUrlPrefix (c.WOK3App.GameId.ToString()) + "/?" + Request.ServerVariables["QUERY_STRING"];
                        litKanevaUrl.Text = stpUrl;
                    }
                    else if (c.CommunityId > 0)
                    { // this is a regular community
                        stpUrl += "/" + StpUrl.MakeCommunityUrlPath (c.Name) + "/?";
                    }
                    else
                    {
                        // shouldn't ever happen, only if caller bad
                        Response.Write ("<script type=\"text/javascript\">alert('Invalid destination for Community Url: " + commId + "' );</script>");
                        return;
                    }
                }
            }
            else  // leave for backward compatability.  Will remove once we are sure new code above is handling all cases correctly
            {
                // should always be called from generated url, so
                // only do minor scrubbing of params
                if (Request["goto"] != null && Request["goto"].Length >= 2)
                {
                    destIdStr = Request["goto"].Substring (1);

                    switch (Request["goto"][0])
                    {
                        // A/P/C old MM3D, not preferred anymore, but backward compat
                        case 'A':
                            {
                                User u = null;
                                int destId = 0;
                                if (Int32.TryParse (destIdStr, out destId) &&
                                    (u = new UserFacade ().GetUser (destId)) != null &&
                                    u.UserId != 0)
                                    stpUrl += "/" + StpUrl.MakeAptUrlPath(u.Username) + "/?";
                                else
                                {
                                    // shouldn't ever happen, only if caller bad
                                    Response.Write ("<script type=\"text/javascript\">alert('Invalid destination for A: " + destIdStr + "' );</script>");
                                    return;
                                }
                                break;
                            }
                        case 'P':
                            {
                                User u = null;
                                int destId = 0;
                                if (Int32.TryParse (destIdStr, out destId) &&
                                    (u = new UserFacade ().GetUser (destId)) != null &&
                                    u.UserId != 0)
                                    stpUrl += "/" + StpUrl.MakePersonUrlPath(u.Username) + "/?";
                                else
                                {
                                    // shouldn't ever happen, only if caller bad
                                    Response.Write ("<script type=\"text/javascript\">alert('Invalid destination for P: " + destIdStr + "' );</script>");
                                    return;
                                }
                                break;
                            }
                        case 'C':
                            {
                                int destId = 0;

                                if (Int32.TryParse (destIdStr, out destId))
                                {
                                    Community c = new CommunityFacade ().GetCommunity (destId);
                                    if (c.WOK3App.GameId > 0)
                                    {   // community belongs to a 3D App
                                        stpUrl = StpUrl.MakeUrlPrefix (c.WOK3App.GameId.ToString()) + "/?" + Request.ServerVariables["QUERY_STRING"];
                                        litKanevaUrl.Text = stpUrl;
                                    }
                                    else if (c.CommunityId != 0)
                                    {
                                        stpUrl += "/" + StpUrl.MakeCommunityUrlPath(c.Name) + "/?";
                                    }
                                }
                                else
                                {
                                    // shouldn't ever happen, only if caller bad
                                    Response.Write ("<script type=\"text/javascript\">alert('Invalid destination for C: " + destIdStr + "' );</script>");
                                    return;
                                }
                                break;
                            }
                        case 'T':
                            {
                                // in this case stpUrl defaulting to this server's game id url
                                tryOn = destIdStr;
                                if (Request["useType"] != null)
                                {
                                    tryOn += ":" + Request["useType"].ToString ();
                                }
                                else
                                {
                                    tryOn += ":0";
                                }

                                break;
                            }
                        case 'U':
                            {
                                stpUrl = StpUrl.MakeUrlPrefix (destIdStr) + "/?" + Request.ServerVariables["QUERY_STRING"];
                                //stpUrl = StpUrl.MakeUrlPrefix(destIdStr);
                                litKanevaUrl.Text = stpUrl;
                                //stpUrl += destIdStr;
                            }
                            break;
                    }
                }
            }

			
				
			litPlayWok.Text =
				"<script> " +
				"trackMetrics('http://" + KanevaGlobals.SiteName + "/requestTracking.aspx', '" + Constants.cREQUEST_TRACKING_URL_PARAM + "','" + requestTrackingGUID.ToString() + "','" + Constants.cREQUEST_TRACKING_URL_IE_PARAM + "','" + Constants.cREQUEST_TRACKING_URL_USERAGENT_PARAM + "'); " +
			    "LaunchPatcher('" + stpUrl + "', '" + tryOn + "', '" + email + "'); " +
				"</script>";

		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
