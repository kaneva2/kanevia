<%@ Page language="c#" Codebehind="PlayWOK.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.kgp.PlayWOK" %>

<script type="text/javascript" src="../jscript/prototype.js"><!-- // External JS --></script>


<script type="text/javascript" src="../jscript/PatcherDetect.js?v5"></script>

<style type="text/css">
	TD IMG { DISPLAY: block }
	.style1 { FONT-SIZE: 12px; COLOR: #ffffff; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif }
	.style1 A:active { FONT-SIZE: 12px; COLOR: #ffffff; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif }
	.style1 A:hover { FONT-SIZE: 12px; COLOR: #ffffff; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif }
	.style1 A:visited { FONT-SIZE: 12px; COLOR: #ffffff; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif }
	.style1 A:link { FONT-SIZE: 12px; COLOR: #ffffff; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif }
	UL.style1 { PADDING-RIGHT: 0px; PADDING-LEFT: 0px; PADDING-BOTTOM: 0px; MARGIN: 0px; PADDING-TOP: 0px }
	UL.style1 LI { PADDING-RIGHT: 0px; PADDING-LEFT: 0px; PADDING-BOTTOM: 10px; MARGIN: 0px; PADDING-TOP: 0px; LIST-STYLE-TYPE: disc }
	</style>

<asp:Literal runat="server" id="litPlayWok" />
<link href="../css/friends.css" rel="stylesheet" type="text/css">
	<link href="../css/new.css" rel="stylesheet" type="text/css">
		<style type="text/css">BODY { BACKGROUND: url(../images/bg_body.jpg) #ffffff repeat-x 0px 0px }
	</style>
		<table width="990" border="0" align="center" cellpadding="0" cellspacing="0">
			<tr>
				<td align="left" valign="" height="82"><img runat="server" src="~/images/home/kaneva_logo_275x45.gif" ></td>
			</tr>
			<tr>
				<td>
					<table border="0" cellpadding="0" cellspacing="0" width="100%">

						<tr>
							<td class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1"></td>
							<td valign="top" class="frBgIntMembers">
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td>
											<!-- TOP STATUS BAR -->
											<div id="pageheader">
												<table cellpadding="0" cellspacing="0" border="0" width="100%">
													<tr>
														<td nowrap align="left">
															<h1>Virtual World & Support</h1>
														</td>
													</tr>
												</table>
											</div>
											<!-- END TOP STATUS BAR -->
										</td>
									</tr>
									<tr>
										<td width="968" align="left" valign="top">
											<div class="module whitebg">
												<span class="ct">
													<span class="cl"></span>
												</span>
												<!-- temporarily disabled for quick launch -->
												<table cellpadding="0" cellspacing="0" border="0" width="98%" style="DISPLAY: none">
													<tr>
														<td valign="top" align="center">
															<br>
															<asp:Literal id="imageLink" runat="server"></asp:Literal>
															<!--<a href="http://www.kaneva.com/woktest"><img runat="server" src="~/images/download-get-ready2.jpg" border="0"></a>-->
														</td>
													</tr>
												</table>
												<table border="0" cellpadding="0" cellspacing="0" width="780">
													<tr>
														<td><img src="../images/spacer.gif" width="34" height="1" border="0" alt=""></td>
														<td><img src="../images/spacer.gif" width="200" height="1" border="0" alt=""></td>
														<td><img src="../images/spacer.gif" width="53" height="1" border="0" alt=""></td>
														<td><img src="../images/spacer.gif" width="200" height="1" border="0" alt=""></td>
														<td><img src="../images/spacer.gif" width="56" height="1" border="0" alt=""></td>
														<td><img src="../images/spacer.gif" width="200" height="1" border="0" alt=""></td>
														<td><img src="../images/spacer.gif" width="37" height="1" border="0" alt=""></td>
														<td><img src="../images/spacer.gif" width="1" height="1" border="0" alt=""></td>
													</tr>
													<tr>
														<td colspan="7"><img name="runwokblue_r1_c1" src="../images/runwok-blue_r1_c1.jpg" width="780" height="89"
																border="0" id="runwokblue_r1_c1" alt=""></td>
														<td><img src="../images/spacer.gif" width="1" height="89" border="0" alt=""></td>
													</tr>
													<tr>
														<td rowspan="7"><img name="runwokblue_r2_c1" src="../images/runwok-blue_r2_c1.jpg" width="34" height="324"
																border="0" id="runwokblue_r2_c1" alt=""></td>
														<td valign="top" background="../images/runwok-blue_r2_c2.jpg" width="200" height="81">
															<p class="style1">
																<asp:repeater id="latestTopRpt" runat="server">
																	<itemtemplate>
																		<p class="style1"><%# DataBinder.Eval (Container.DataItem, "topNews").ToString () %></p>
																	</itemtemplate>
																</asp:repeater>
															</p>
														</td>
														<td rowspan="7"><img name="runwokblue_r2_c3" src="../images/runwok-blue_r2_c3.jpg" width="53" height="324"
																border="0" id="runwokblue_r2_c3" alt=""></td>
														<td rowspan="3" valign="top" background="../images/runwok-blue_r2_c4.jpg" width="200"
															height="205">
															<ul class="style1">
																<asp:repeater id="rptList1" runat="server">
																	<itemtemplate>
																		<li><%# DataBinder.Eval (Container.DataItem, "topic").ToString () %></li>
																	</itemtemplate>
																</asp:repeater>
															</ul>
														</td>
														<td rowspan="7"><img name="runwokblue_r2_c5" src="../images/runwok-blue_r2_c5.jpg" width="56" height="324"
																border="0" id="runwokblue_r2_c5" alt=""></td>
														<td rowspan="3" valign="top" background="../images/runwok-blue_r2_c6.jpg" width="200"
															height="205">
															<asp:repeater id="troubleRpt" runat="server">
																<itemtemplate>
																	<P class="style1"><%# DataBinder.Eval (Container.DataItem, "newsItem").ToString () %></P><BR />
																</itemtemplate>
															</asp:repeater>
														</td>
														<td rowspan="7"><img name="runwokblue_r2_c7" src="../images/runwok-blue_r2_c7.jpg" width="37" height="324"
																border="0" id="runwokblue_r2_c7" alt=""></td>
														<td><img src="../images/spacer.gif" width="1" height="81" border="0" alt=""></td>
													</tr>
													<tr>
														<td><img name="runwokblue_r3_c2" src="../images/runwok-blue_r3_c2.jpg" width="200" height="20"
																border="0" id="runwokblue_r3_c2" alt=""></td>
														<td><img src="../images/spacer.gif" width="1" height="20" border="0" alt=""></td>
													</tr>
													<tr>
														<td rowspan="3" valign="top" background="../images/runwok-blue_r4_c2.jpg" width="200"
															height="162">
															<asp:repeater id="latestBtmRpt" runat="server">
																<itemtemplate>
																	<span class="style1"><%# DataBinder.Eval (Container.DataItem, "bottomNews").ToString () %></span>
																</itemtemplate>
															</asp:repeater>
														</td>
														<td><img src="../images/spacer.gif" width="1" height="104" border="0" alt=""></td>
													</tr>
													<tr>
														<td><img name="runwokblue_r5_c4" src="../images/runwok-blue_r5_c4.jpg" width="200" height="9"
																border="0" id="runwokblue_r5_c4" alt=""></td>
														<td><img name="runwokblue_r5_c6" src="../images/runwok-blue_r5_c6.jpg" width="200" height="9"
																border="0" id="runwokblue_r5_c6" alt=""></td>
														<td><img src="../images/spacer.gif" width="1" height="9" border="0" alt=""></td>
													</tr>
													<tr>
														<td rowspan="2"><a target="_blank" runat="server" href="~/community/CommunityPage.aspx?communityId=1118&amp;pageId=887411"><img name="runwokblue_r6_c4" src="../images/runwok-blue_r6_c4.jpg" width="200" height="59"
																	border="0" id="runwokblue_r6_c4" alt=""></a></td>
														<td rowspan="2"><a target="_blank" runat="server" href="~/suggestions.aspx?mode=F&amp;category=Feedback"><img name="runwokblue_r6_c6" src="../images/runwok-blue_r6_c6.jpg" width="200" height="59"
																	border="0" id="runwokblue_r6_c6" alt=""></a></td>
														<td><img src="../images/spacer.gif" width="1" height="49" border="0" alt=""></td>
													</tr>
													<tr>
														<td rowspan="2"><img name="runwokblue_r7_c2" src="../images/runwok-blue_r7_c2.jpg" width="200" height="61"
																border="0" id="runwokblue_r7_c2" alt=""></td>
														<td><img src="../images/spacer.gif" width="1" height="10" border="0" alt=""></td>
													</tr>
													<tr>
														<td><img name="runwokblue_r8_c4" src="../images/runwok-blue_r8_c4.jpg" width="200" height="51"
																border="0" id="runwokblue_r8_c4" alt=""></td>
														<td><img name="runwokblue_r8_c6" src="../images/runwok-blue_r8_c6.jpg" width="200" height="51"
																border="0" id="runwokblue_r8_c6" alt=""></td>
														<td><img src="../images/spacer.gif" width="1" height="51" border="0" alt=""></td>
													</tr>
												</table>
												<span class="cb">
													<span class="cl"></span>
												</span>
											</div>
										</td>
									</tr>
								</table>
								<A href="JavaScript:history.back(1)">Navigate Back To Previous Page</A> 
							</td>
							<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1"></td>
						</tr>

					</table>
				</td>
			</tr>
		</table>
		<!-- kurl <asp:literal id="litKanevaUrl" runat="server"></asp:literal> -->