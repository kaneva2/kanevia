<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WOKPatchRequired.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.kgp.WOKPatchRequired" %>
<%@ Register TagPrefix="Kaneva" TagName="HeaderText" Src="../usercontrols/HeaderText.ascx" %>

<Kaneva:HeaderText ID="HeaderText1" runat="server" Text="Whoops! You need to patch because..."/>
<br>
<center>
<table cellpadding="20" cellspacing="1" border="0" width="730">
	<tr>
	<td class="bodyText" align="left"><br>
	
	You may not be able to access this item because one or more of the following conditions apply:
	<br><br> 
	<ul>
		<asp:Label id="lblOther" runat="server">
			<li> New functionality has been added to Kaneva that requires the game client to be executed to apply updates. Close all browsers and enter the game for updates to be applied.</i>
		</asp:Label>
	</ul>
	
	</td>
	</tr>
</table>
</center>

