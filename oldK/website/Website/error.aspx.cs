///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using log4net;


namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for Error.
	/// </summary>
	public class Error : BasePage
    {
        #region Declarations
        private int errorReason = (int)Constants.eERROR_TYPE.GENERIC;
        protected Label profileName, communityName, threeDAppName;
        private string errorMessage = "";
        protected HtmlContainerControl genericError, checkoutClosed, pageNotFound, profileNotFound, accessDenied, communityNotFound, threeDAppNotFound, invalidAccount;
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected HtmlAnchor aShop;
        #endregion

        private void Page_Load(object sender, System.EventArgs e)
		{
			// get the error reason from parameter
            GetRequestParams();

            //configure page based on error cause
            ConfigurePage();
        }

        #region Functions

        private void GetRequestParams()
        {
            if (Request["error"] != null)
            {
                //get error reason
                try
                {
                    this.errorReason = Convert.ToInt32(Request["error"]);
                }
                catch (Exception ex)
                {
                    m_logger.Error("Error retrieving error reason", ex);
                }
            }
            if (Request["message"] != null)
            {
                //get error reason
                this.errorMessage = Request["message"].ToString();
            }
        }

        private void ConfigurePage()
        {
            //set shopping site link
            aShop.HRef = "http://" + KanevaGlobals.ShoppingSiteName;

            switch (errorReason)
            {
                case (int)Constants.eERROR_TYPE.CHECKOUT_CLOSED:
                    checkoutClosed.Visible = true;
                    break;
                case (int)Constants.eERROR_TYPE.COMMUNITY_NOT_FOUND:
                    communityNotFound.Visible = true;
                    communityName.Text = errorMessage;
                    break;
                case (int)Constants.eERROR_TYPE.THEEDAPP_NOT_FOUND:
                    threeDAppNotFound.Visible = true;
                    threeDAppName.Text = errorMessage;
                    break;
                case (int)Constants.eERROR_TYPE.PROFILE_NOT_FOUND:
                    profileNotFound.Visible = true;
                    profileName.Text = errorMessage;
                    break;
                case (int)Constants.eERROR_TYPE.WEBPAGE_NOT_FOUND:
                    pageNotFound.Visible = true;
                    break;
                case (int)Constants.eERROR_TYPE.ACCESS_DENIED:
                    accessDenied.Visible = true;
                    break;
                case (int)Constants.eERROR_TYPE.INVALID_ACCOUNT:
                    invalidAccount.Visible = true;
                    break;
                case (int)Constants.eERROR_TYPE.GENERIC:
                default:
                    genericError.Visible = true;
                    break;
            }
        }

        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
