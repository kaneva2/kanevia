<%@ Page language="c#" ValidateRequest="False" Codebehind="login.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.login" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head>
    <title>login</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">

<style type="text/css">

		
		#tblLogin {
			font-size: 11px;
			font-family: verdana, arial, sans-serif;
			color: #737373;
			}
			
		a.signTextLink:Link, a.signTextLink:Visited {
			color: #476163; 
			text-decoration:underline;
			font-weight: normal;
			margin-left: 50px;
			}
			
		a.signTextLink:hover {
			font-weight: normal;
			}
		
			
		#tblLogin table {
			font-size: 10px;
			font-family: verdana, arial, sans-serif;
			color: #8a8a8a;
			}
		
		
 	</style>
 	
  </head>
  <body MS_POSITIONING="GridLayout" topmargin="0" leftmargin="0" bottommargin="0" rightmargin="0">
    <form id="frmLogin" method="post" runat="server">
		<table id="tblLogin" width="242" cellpadding="4" cellspacing="0" border="0" height="100" >
			
			<tr align="left">
				<td align="right">E-mail:</td>
				<td colspan="2"><input name="txtUserName" id="txtUserName" runat="server" type="text" style="width:105px" maxlength="50" /></td>
			</tr>
			<tr align="left">
				<td align="right">Password:</td>
				<td colspan="2"><input type="password" maxlength="30" id="txtPassword" runat="server" onkeydown="javascript:checkEnter(event)" name="txtPassword" style="width:105px;margin-right:5px;">
				<asp:imagebutton imagealign="AbsMiddle" imageurl="./images/signIn_bt.gif" id="btnLogin" runat="server" causesvalidation="False" onclick="btnLogin_Click"></asp:imagebutton></td>
			</tr>
			<tr>
				<td align="left" colspan="3">
					<table cellpadding="0" cellspacing="0" border="0" align="left">
						<tr>
							<td ><span  title="Remember My Info"><asp:checkbox runat="server" id="chkRememberLogin" cssclass="signCheckBox" tooltip="Remember My Info"/></span></td>
							<td width="6"></td> 
							<td  valign="middle" nowrap>Remember me on this computer</td>
						</tr>
					</table> 
				</td>
			</tr>
													
			</form>																																																																																										
		</table>	
	</form>
  </body>
</html>

