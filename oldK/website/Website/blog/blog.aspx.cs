///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Text;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva.channel;
using log4net;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for blog.
	/// </summary>
	public class blog : BlogBasePage
	{
		protected blog () 
		{
			Title = "My Blogs";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			if (Request.IsAuthenticated)
			{
				if (GetRequestParams())
				{
					//check if user is allowed to edit this channel
					if (!IsUserAllowedToEdit())
					{
						RedirectToHomePage ();
					}
				}
				else
				{
					//invalid request params
					//if the user logged in, send him to his home page
					//if not, redirect to login page
					RedirectToHomePage ();
				}
			}
			else
			{
				Response.Redirect (this.GetLoginURL ());
			}
			
			//setup header nav bar
			if(_channelId == this.GetPersonalChannelId())
			{
				//user's own channel
				HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.PROFILE;
			}

			if (!IsPostBack)
			{
				int userId = GetUserId ();

                // Request tracking
                if (Request.Params[Constants.cREQUEST_TRACKING_URL_PARAM] != null)
                {
                    int iResult = GetUserFacade.AcceptTrackingRequest(Request.Params[Constants.cREQUEST_TRACKING_URL_PARAM], KanevaWebGlobals.CurrentUser.UserId);
                }

				BindData (1, "");

				LoadDropdown (userId);

				// Add bread crumb
				AddBreadCrumb (new BreadCrumb ("Blogs", GetCurrentURL(), "", 0));
			}

			// Set Nav
            HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
			HeaderNav.SetNavVisible(HeaderNav.MyKanevaNav,2);
			
			hlAddBlog.NavigateUrl = GetBlogEditLink (0, _channelId, 0);
        }


        #region Helper Methods

        private bool GetRequestParams()
		{
			bool retVal = true;
			try
			{
				if(Request ["communityId"] != null && Request ["communityId"] != "")
				{
					_channelId = Convert.ToInt32 (Request ["communityId"]);
				}

				if(_channelId <= 0)
				{
					_channelId = this.GetPersonalChannelId();
				}
			}
			catch(Exception)
			{
				//invalid numbers
				retVal = false;
			}
			return retVal;
		}

		/// <summary>
		/// returns true if current user is allowed to edit this channel
		/// </summary>
		/// <returns></returns>
		private bool IsUserAllowedToEdit()
		{
			return IsAdministrator() || CommunityUtility.IsCommunityModerator(
				_channelId, GetUserId());
        }

        /// <summary>
        /// BindData
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="filter"></param>
        private void BindData (int pageNumber, string filter)
        {
            BlogFacade blogFacade = new BlogFacade ();
            PagedList<Blog> pds = blogFacade.GetBlogs (_channelId, filter, filBlog.CurrentSort, pageNumber, filBlog.ItemsPerPages);
            blogRepeater.DataSource = pds;
            blogRepeater.DataBind ();

            pgBlogs.NumberOfPages = Math.Ceiling ((double) pds.TotalCount / filBlog.ItemsPerPages).ToString ();
            pgBlogs.DrawControl ();

            // The results
            lblSearch.Text = GetResultsText (pds.TotalCount, pageNumber, filBlog.ItemsPerPages, pds.Count, true);
        }

        /// <summary>
        /// Get Blog Details Link
        /// </summary>
        /// <param name="topicId"></param>
        /// <returns></returns>
        public string GetBlogEditLink (int topicId)
        {
            return Page.ResolveUrl ("~/blog/blogEdit.aspx?topicId=" + topicId + "&communityId=0");
        }

        /// <summary>
        /// Delete a topic
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Delete_Topic (Object sender, EventArgs e)
        {
            BlogFacade blogFacade = new BlogFacade ();

            // Make sure they have rights.
            if (!blogFacade.IsBlogOwner (GetUserId (), Convert.ToInt32 (TopicId.Value)) && !IsAdministrator ())
            {
                m_logger.Warn ("User " + GetUserId () + " from IP " + Common.GetVisitorIPAddress() + " tried to delete topic " + TopicId.Value + " but is not a site administrator");
                return;
            }

            blogFacade.DeleteBlog (Convert.ToInt32 (TopicId.Value), GetUserId ());
            pgBlogs_PageChange (this, new PageChangeEventArgs (pgBlogs.CurrentPageNumber));
        }

        /// <summary>
        /// Are they the blog owner?
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="topicId"></param>
        /// <returns></returns>
        protected bool IsBlogOwner (int userId, int topicId)
        {
            BlogFacade blogFacade = new BlogFacade ();
            return blogFacade.IsBlogOwner (userId, topicId) || IsAdministrator ();
        }

        protected void LoadDropdown (int userId)
        {
            drpActions.Items.Clear ();

            drpActions.Items.Add (CreateListItem ("Select action...", "", true));
            drpActions.Items.Add (new ListItem (Constants.DROPDOWN_SEPERATOR, ""));

            drpActions.Items.Add (new ListItem ("Delete selected blog(s)", "del"));

            drpActions.SelectedIndex = 0;

            StringBuilder sb = new StringBuilder ();
            sb.Append (" javascript: ");
            sb.Append (" if (").Append (drpActions.ClientID).Append (".value == 'del')");
            sb.Append (" if(!confirm('Are you sure you want to delete selected blog(s)?')) return false;");

            drpActions.Attributes.Add ("onChange", sb.ToString ());
        }

        #endregion


        #region Event Handlers

        /// <summary>
		/// pgBlogs_PageChange
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pgBlogs_PageChange (object sender, PageChangeEventArgs e)
		{
			BindData (e.PageNumber, filBlog.CurrentFilter);
		}

		/// <summary>
		/// Click the add button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnAddBlog_Click (Object sender, System.EventArgs e) 
		{
			Server.Transfer (GetBlogEditLink (0, 0, 0));
        }

        private void filBlog_FilterChanged (object sender, FilterChangedEventArgs e)
        {
            pgBlogs.CurrentPageNumber = 1;
            BindData (1, e.Filter);
        }

        /// <summary>
        /// Change category of asset
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void drpAction_Change (Object sender, EventArgs e)
        {
            BlogFacade blogFacade = new BlogFacade ();
            int userId = GetUserId ();
            HtmlInputHidden hidBlogId;
            CheckBox chkEdit;

            if (drpActions.SelectedValue.Equals ("del"))
            {
                // Remove the friends
                foreach (RepeaterItem riBlog in blogRepeater.Items)
                {
                    chkEdit = (CheckBox) riBlog.FindControl ("chkEdit");

                    if (chkEdit.Checked)
                    {
                        hidBlogId = (HtmlInputHidden) riBlog.FindControl ("hidBlogId");

                        // Make sure they have rights.
                        if (!blogFacade.IsBlogOwner (GetUserId (), Convert.ToInt32 (hidBlogId.Value)) && !IsAdministrator ())
                        {
                            m_logger.Warn ("User " + GetUserId () + " from IP " + Common.GetVisitorIPAddress() + " tried to delete topic " + TopicId.Value + " but is not a site administrator");
                            return;
                        }

                        blogFacade.DeleteBlog (Convert.ToInt32 (hidBlogId.Value), userId);
                    }
                }
            }

            //reset selection
            drpActions.SelectedIndex = 0;

            BindData (1, filBlog.CurrentFilter);
        }
        
        #endregion


        #region Declerations

        protected Kaneva.BlogFilter filBlog;
		protected DropDownList drpActions;
		protected System.Web.UI.HtmlControls.HtmlInputHidden TopicId;

		protected System.Web.UI.WebControls.Repeater blogRepeater;

		protected Kaneva.Pager pgBlogs;

		protected Label lblSearch;
		protected PlaceHolder phBreadCrumb;

		protected HyperLink hlAddBlog;

		private int _channelId;

		// Logger
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

        #endregion Declerations


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
			pgBlogs.PageChanged +=new PageChangeEventHandler (pgBlogs_PageChange);
			filBlog.FilterChanged	+=new FilterChangedEventHandler(filBlog_FilterChanged);
		}
		#endregion
	}
}
