///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for commentEdit.
	/// </summary>
	public class commentEdit : MainTemplatePage
	{
		/// <summary>
		/// Constructor
		/// </summary>
		protected commentEdit () 
		{
			Title = "Edit Blog Comment";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
            if (Request.IsAuthenticated)
            {
                if (GetRequestParams ())
                {
                    //check if user is allowed to edit this channel
                    if (!IsUserAllowedToEdit ())
                    {
                        RedirectToHomePage ();
                    }
                }
                else
                {
                    //invalid request params
                    //if the user logged in, send him to his home page
                    //if not, redirect to login page
                    RedirectToHomePage ();
                }
            }
            else
            {
                Response.Redirect (this.GetLoginURL ());
            }
            
            if (!IsPostBack)
			{
				int threadId = Convert.ToInt32 (Request ["threadId"]);
                BlogFacade blogFacade = new BlogFacade();
                BlogComment blogComment = blogFacade.GetBlogComment(threadId);

                txtComment.Value = Server.HtmlDecode(blogComment.BodyText);
			}

            // Set Nav
            HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
            HeaderNav.SetNavVisible (HeaderNav.MyKanevaNav, 2);
        }


        #region Helper Methods

        private bool GetRequestParams ()
        {
            bool retVal = true;
            try
            {
                if (Request["communityId"] != null && Request["communityId"] != "")
                {
                    _channelId = Convert.ToInt32 (Request["communityId"]);
                }

                if (_channelId <= 0)
                {
                    _channelId = this.GetPersonalChannelId ();
                }
            }
            catch (Exception)
            {
                //invalid numbers
                retVal = false;
            }
            return retVal;
        }

        /// <summary>
        /// returns true if current user is allowed to edit this channel
        /// </summary>
        /// <returns></returns>
        private bool IsUserAllowedToEdit ()
        {
            return IsAdministrator () || CommunityUtility.IsCommunityModerator (
                _channelId, GetUserId ());
        }


        /// <summary>
        /// NavigateToReturnURl
        /// </summary>
        private void NavigateToReturnURl ()
        {
            if (Request["returnURL"] != null)
            {
                Response.Redirect (Request["returnURL"].ToString () + "?topicId=" + Request["topicId"] + GetCommunityIdQueryString ());
            }
        }

        #endregion Helper Methods


        #region Even Handlers

        /// <summary>
		/// Cancel the edit
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnCancel_Click (object sender, EventArgs e) 
		{
			NavigateToReturnURl ();
		}

		/// <summary>
		/// Update the thread comment
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnUpdate_Click (object sender, EventArgs e) 
		{
            spnAlertMsg.Style.Add ("display", "none");

            // Make sure the page is valid
            Page.Validate ();
            if (!Page.IsValid)
			{
				return;
			}

            // Check to make sure user did not enter any "potty mouth" words
            if (KanevaWebGlobals.isTextRestricted (txtComment.Value, Constants.eRESTRICTION_TYPE.POTTY_MOUTH))
            {
                spnAlertMsg.InnerText = Constants.VALIDATION_REGEX_RESTRICTED_WORD_MESSAGE;
                spnAlertMsg.Style.Add ("display", "block");
                return;
            }

			if (IsAdministrator ())
			{
                BlogFacade blogFacade = new BlogFacade();
                blogFacade.UpdateBlogComment(Convert.ToInt32(Request["threadId"]), GetUserId(), Server.HtmlEncode(txtComment.Value), 
					Common.GetVisitorIPAddress());
			}
	
			NavigateToReturnURl ();
        }

        #endregion Event Handlers


        #region Declerations

        private int _channelId;

        protected HtmlTextArea txtComment;
        protected HtmlContainerControl spnAlertMsg;
		protected HtmlTable tblCommentEdit;

        #endregion Declerations


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
