<%@ Page language="c#" Codebehind="blog.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.blog" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../usercontrols/Pager.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="BlogFilter" Src="../usercontrols/BlogFilter.ascx" %>

<link href="../css/home.css" rel="stylesheet" type="text/css" />		
<link href="../css/kanevaSC.css" rel="stylesheet" type="text/css"/>
<link href="../css/friends.css" rel="stylesheet" type="text/css"/>
<link href="../css/kanevaText.css" type="text/css" rel="stylesheet">

<center>
</center>

<table border="0" cellpadding="0" cellspacing="0" class="newcontainer" align="center">
	<tr>
		<td>
			<table border="0" cellpadding="0" cellspacing="0" width="100%" class="newcontainerborder">
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
				<tr> 
					<td class=""><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
					<td valign="top" class="newdatacontainer">
						<table cellpadding="0" cellspacing="0" border="0" width="100%">	
							<tr>
								<td align="right" valign="middle">
									<Kaneva:BlogFilter runat="server" id="filBlog" BlogCategory="0"/>
								</td>	
								<td align="right" valign="middle">
									<asp:Label runat="server" id="lblSearch" CssClass="insideTextNoBold"/>
								</td>	
								<td align="right" valign="middle">
									<Kaneva:Pager runat="server" id="pgBlogs"/>
								</td>	
							</tr>
						</table>

									<table  border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr>
										<td class="boxInsideTopLeft"><img runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
										<td class="boxInsideTop" ><img runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
										<td  class="boxInsideTopRight"><img runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
										</tr>
										<tr class="boxInside">
										<td class="boxInsideleft" ><img runat="server" src="~/images/spacer.gif" width="1" height="4" /></td>
										<td class="boxInsideContent">		

												<table border="0" cellpadding="0" cellspacing="0" width="100%">
													<tr>
														<td width="100%">
															<table border="0" cellpadding="0" cellspacing="0">
																<tr><td colspan="3"><img runat="server" src="~/images/spacer.gif" width="1" height="10" /></td></tr>
																<tr>
																	<td><img runat="server" src="~/images/spacer.gif" width="5" height="4" /></td>
																	<td width="100%">
																		<table border="0" cellpadding="0" cellspacing="0" width="100%" >
																			<tr>
																				<td  rowspan="5" align="right"><img runat="server" src="~/images/friends/cornerTopLeft.gif" border="0" /></td>
																				<td class="borderBoxTop"></td>
																				<td  rowspan="5"><img runat="server" src="~/images/friends/corneTopRight.gif" border="0" /></td>
																			</tr>
																			<tr> 
																				<td valign="top" class="bgColorBox" align="left">
																							<table border="0" cellpadding="0" cellspacing="0"  width="930" >
																									<tr>
																									<td colspan="5"><img runat="server" src="~/images/spacer.gif" width="1" height="5" /></td></tr>
																									<tr>
																										<td><img runat="server" src="~/images/spacer.gif" width="15" height="1" /></td>
																										<td align="left"  class="insideBoxText11">Select All:</td>
																										<td><img runat="server" src="~/images/spacer.gif" width="2" height="1" /></td>
																										<td align="left"><input type="checkbox" class="formKanevaCheck" id="chkSelect" onclick="javascript:SelectAll(this,this.checked);"/></td>
																										<td><img runat="server" src="~/images/spacer.gif" width="10" height="1" /></td>
																										<td align="left" class="insideBoxText11" >Actions:</td>
																										<td align="left">
																											<asp:DropDownList ID="drpActions" CssClass="formKanevaSelect" runat="server" style="width:250px"  OnSelectedIndexChanged="drpAction_Change" AutoPostBack="True"></asp:DropDownList>																																
																										</td>
																										<td><img runat="server" src="~/images/spacer.gif" width="10" height="8" /></td>
																										<td width="405" align="right"><img runat="server" src="~/images/blog/addBlog.gif" /></td>
																										<td><img runat="server" src="~/images/spacer.gif" width="8" height="8" /></td>
																										<td width="85" align="right" ><span class="textGeneralGray12"><asp:hyperlink runat="server" ID="hlAddBlog" NavigateUrl="" Text="Add Blog Entry"/></span></td>
																										<td><img runat="server" src="~/images/spacer.gif" width="10" height="8" /></td>
																									</tr>
																									<tr><td><img runat="server" src="~/images/spacer.gif" width="1" height="1"/></td></tr>
																								</table>
																			 				</td>
																						</tr>
																					<tr>
																				<td class="borderBoxbottom"></td>
																			</tr>
																		</table>
																	<td><img runat="server" src="~/images/spacer.gif" width="5" height="4" /></td>
																</tr>
															</table>
														</td>
													</tr>															
													<tr>
														<td><img runat="server" src="~/images/spacer.gif" width="4" height="10"/></td>
													</tr>
												</table>
											</td>
											<td class="boxInsideRight"><img runat="server" src="~/images/spacer.gif" width="1" height="4"/></td>
											</tr>
											<tr>
												<td class="boxInsideBottomLeft" ><img runat="server" src="~/images/spacer.gif" width="4" height="4" /></td>
												<td class="boxInsideBottom2"><img runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
												<td class="boxInsideBottomRight"><img runat="server" src="~/images/spacer.gif" width="4" height="4" /></td>
											</tr>
										</table>
										<br />
                <asp:Repeater id="blogRepeater" runat="server" EnableViewState="True">	
					<ItemTemplate>
					
						<!-- Blog Entry Start -->
						<table  border="0" cellpadding="0" cellspacing="0" width="100%" class="boxInside">
							<tr>
								<td class="boxInsideTopLeft"><img id="Img1" runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
									<td class="boxInsideTop" ><img id="Img2" runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
									<td  class="boxInsideTopRight"><img id="Img3" runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
							</tr>
							<tr class="boxInside">
								<td class="boxInsideleft" ><img id="Img4" runat="server" src="~/images/spacer.gif" width="1" height="4" /></td>
								<td class="boxInsideContent">
									<table border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td width="10" height="13"><img id="Img5" runat="server" src="~/images/spacer.gif" width="10" height="13" /></td>
											<td valign="center"><img id="Img6" runat="server" src="~/images/icon_general.gif" border="0"/></td>
											<td width="10" ><img id="Img7" runat="server" src="~/images/spacer.gif" width="10" height="8" /></td>									
											<td align="left" width="100%" class="textGeneralGray01"><%# TruncateWithEllipsis (DataBinder.Eval(Container.DataItem, "Subject").ToString (), 45) %></td>                                
										</tr>
									</table>
								</td>
								<td class="boxInsideRight"><img id="Img8" runat="server" src="~/images/spacer.gif" width="1" height="4"/></td>
							</tr>
							<tr>
								<td class="boxInsideBottomLeft" ><img id="Img9" runat="server" src="~/images/spacer.gif" width="4" height="4" /></td>
								<td class="boxInsideBottom"><img id="Img10" runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
								<td class="boxInsideBottomRight"><img id="Img11" runat="server" src="~/images/spacer.gif" width="4" height="4" /></td>
							</tr>
						</table>
						<table  border="0" cellpadding="0" cellspacing="0" width="100%" >
								<tr>
								<td class="boxInsideTopLeft"><img id="Img12" runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
								<td class="boxInsideTop" ><img id="Img13" runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
								<td  class="boxInsideTopRight"><img id="Img14" runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
								</tr>
								<tr class="boxInside">
								<td class="boxInsideleft" ><img id="Img15" runat="server" src="~/images/spacer.gif" width="1" height="4" /></td>
								<td align="left" class="boxInsideContent">
									<table border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr>
											<td width="10"><img id="Img16" runat="server" src="~/images/spacer.gif" width="1" height="10"/></td>
										<tr>
											<td><img id="Img17" runat="server" src="~/images/spacer.gif" width="10" height="8"/></td>
											<td align="center" valign="bottom">
												<asp:checkbox runat="server" id="chkEdit"/>
												<input type="hidden" runat="server" id="hidBlogId" value='<%#DataBinder.Eval(Container.DataItem, "BlogId")%>' NAME="hidBlogId"> 	
											</td>
											<td><img id="Img18" runat="server" src="~/images/spacer.gif" width="10" height="8"/></td>
											<td align="left" valign="bottom"><font class="textGeneralGray03"><%# FormatDateTime (DataBinder.Eval(Container.DataItem, "CreatedDate")) %></font>&nbsp;<font class="bolderText"><a class="bodyText"  style="color: #5D8CCC;" href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "Username").ToString ())%>><%# DataBinder.Eval(Container.DataItem, "Username") %></a> </font>&nbsp;<font class="textGeneralGray01"> tags:&nbsp;<font class="bolderText"><%# DataBinder.Eval(Container.DataItem, "Keywords")%></font></font></td>
											<td valign="top">
												<table border="0" cellpadding="0" cellspacing="0" width="100%">
													<tr>
														<td align="right"></td>
														<td class="bolderText" valign="top" align="left"></td>
														<td width="1"><img id="Img19" runat="server" src="~/images/spacer.gif" width="1" height="8"/></td>										
														<td valign="top" align="right">comments (<font class="bolderText"><a runat="server" href='<%# GetBlogCommentsLink (Convert.ToInt32 ( DataBinder.Eval(Container.DataItem, "BlogId")), Convert.ToInt32 ( DataBinder.Eval(Container.DataItem, "CommunityId"))) %>' ID="A1"><%# DataBinder.Eval(Container.DataItem, "NumberOfComments") %></a></font>)</td>
														<td width="27"><img id="Img20" runat="server" src="~/images/spacer.gif" width="27" height="8"/></td>										
													</tr>
												</table>
											</td>										
										</tr>
										<tr>						
											<td><img id="Img21" runat="server" src="~/images/spacer.gif" width="10" height="8"/></td>
											<td align="left" valign="center">
												<img id="Img22" runat="server" src="~/images/widgets/widget_editc_bt.gif" width="22" height="22"/>
												<asp:hyperlink id="hlEdit" visible='<%#IsBlogOwner (Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "CreatedUserId")), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "BlogId")))%>' runat="server" CssClass="adminLinks" NavigateURL='<%#GetBlogEditLink ((int) DataBinder.Eval(Container.DataItem, "BlogId"))%>' ToolTip="Edit">edit</asp:hyperlink>
											</td>
											<td><img id="Img23" runat="server" src="~/images/spacer.gif" width="15" height="8"/></td>
											<td align="left" class="textGeneralGray01" width="79%">
												<table border="0" cellpadding="0" cellspacing="0" width="98%">
													<tr>
														<td><img id="Img24" runat="server" src="~/images/spacer.gif" width="1" height="13"/></td>
													</tr>										
													<tr>
														<td>
															<%# AddHrefNewWindow (DataBinder.Eval(Container.DataItem, "BodyText").ToString ()) %> &nbsp;&nbsp;<b><a runat="server" href='<%# GetBlogDetailsLink (Convert.ToInt32 ( DataBinder.Eval(Container.DataItem, "BlogId")), Convert.ToInt32 ( DataBinder.Eval(Container.DataItem, "CommunityId"))) %>' ID="A2">more<span runat="server" ID="Span1">...</span></a></b>
														</td>
													</tr>
												</table>
											</td>
										<td>&nbsp;</td></tr>
										<tr>
											<td><img id="Img25" runat="server" src="~/images/spacer.gif" width="1" height="20"/></td>
										<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
									</table>
								</td>
								<td class="boxInsideRight"><img id="Img26" runat="server" src="~/images/spacer.gif" width="1" height="4"/></td>
							</tr>
							<tr>
								<td class="boxInsideBottomLeft" ><img id="Img27" runat="server" src="~/images/spacer.gif" width="4" height="4" /></td>
								<td class="boxInsideBottom2"><img id="Img28" runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
								<td class="boxInsideBottomRight"><img id="Img29" runat="server" src="~/images/spacer.gif" width="4" height="4" /></td>
							</tr>
						</table>
						<br/>
						<!-- Blog Entry End -->

					</ItemTemplate>
					
				</asp:Repeater>
					
					
					</td>
					<td class=""><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
				</tr>
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<input type="hidden" runat="server" id="TopicId" value="0" NAME="TopicId"> 
<asp:button ID="DeleteTopic" OnClick="Delete_Topic" runat="server" Visible="false"></asp:button>
