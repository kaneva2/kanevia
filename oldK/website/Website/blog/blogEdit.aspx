<%@ Page language="c#" Codebehind="blogEdit.aspx.cs" AutoEventWireup="false" ValidateRequest="False" Inherits="KlausEnt.KEP.Kaneva.blogEdit" %>
<%@ Register TagPrefix="Kaneva" TagName="HeaderText" Src="../usercontrols/HeaderText.ascx" %>
<%@ Register TagPrefix="CE" Namespace="CuteEditor" Assembly="CuteEditor" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>

	
<link href="../css/home.css" rel="stylesheet" type="text/css" />		
<link href="../css/Kaneva.css" rel="stylesheet" type="text/css"/>
<link href="../css/kanevaSC.css" rel="stylesheet" type="text/css"/>
<link href="../css/friends.css" rel="stylesheet" type="text/css"/>
<link href="../css/kanevaBroadBand.css" rel="stylesheet" type="text/css" />		
<link href="../css/kanevaText.css" rel="stylesheet" type="text/css" />
<link href="../css/new.css" rel="stylesheet" type="text/css" />


<table width="80%"  border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table border="0" cellpadding="0" cellspacing="0" class="newcontainer">
				<tr>
					<td valign="top">
						<table  border="0" cellpadding="0" cellspacing="0" width="100%" class="newcontainerborder">
							<tr>
								<td class=""></td>
								<td class=""></td>
								<td class=""></td>
							</tr>
							<tr>
								<td class=""><img runat="server" src="~/images/spacer.gif" width="1" height="1"/></td>
								<td valign="top" align="center" class="newdatacontainer">   
								
									<div style="width:70%;">
									
									<ajax:ajaxpanel id="ajpMsg" runat="server">
									<asp:ValidationSummary ShowMessageBox="False" ShowSummary="True" cssclass="errBox black" id="valSum" 
										DisplayMode="BulletList" runat="server" HeaderText="Please correct the following errors:"/>
									
									<span id="spnAlertMsg" runat="server" class="errBox black" style="display:none;"></span>
									</ajax:ajaxpanel>
																
									</div>
								
									<table border="0" cellpadding="0" cellspacing="0" width="100%" class="boxInside" style="margin-top:6px;">
										<tr>
											<td class="boxInsideTopLeft"><img runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
											<td class="boxInsideTop" ><img runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
											<td  class="boxInsideTopRight"><img runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
										</tr>
										<tr class="boxInside">
											<td class="boxInsideleft" ><img runat="server" src="~/images/spacer.gif" width="1" height="4" /></td>
											<td class="boxInsideContent">
												<table width="100%"  border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td><img runat="server" src="~/images/spacer.gif" width="6" height="4"/></td>
														<td><img runat="server" src="~/images/spacer.gif" width="6" height="4"/></td>
														<td><img runat="server" src="~/images/spacer.gif" width="6" height="4"/></td>
													</tr>
													 <tr>
														<td><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
														<td width="100%" class="textGeneralGray01" align="left"><img runat="server" src="~/images/spacer.gif" width="2" height="1"/>Editing blog entry: <asp:label runat="server" id="lblItemName"/></td>
														<td><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
													</tr>
													<tr>
														<td><img runat="server" src="~/images/spacer.gif" width="6" height="4"/></td>
														<td><img runat="server" src="~/images/spacer.gif" width="6" height="4"/></td>
														<td><img runat="server" src="~/images/spacer.gif" width="6" height="4"/></td>
													</tr>
												</table>
											</td>
											<td class="boxInsideRight"><img runat="server" src="~/images/spacer.gif" width="1" height="4"/></td>
										</tr>
										<tr>
											<td class="boxInsideBottomLeft" ><img runat="server" src="~/images/spacer.gif" width="4" height="4" /></td>
											<td class="boxInsideBottom"><img runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
											<td class="boxInsideBottomRight"><img runat="server" src="~/images/spacer.gif" width="4" height="4" /></td>
										</tr>
									</table>
									
									<table  border="0" cellpadding="0" cellspacing="0" width="100%" >
										<tr>
											<td class="boxInsideTopLeft"><img runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
											<td class="boxInsideTop" ><img runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
											<td  class="boxInsideTopRight"><img runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
										</tr>
										<tr class="boxInside">
											<td class="boxInsideleft" ><img runat="server" src="~/images/spacer.gif" width="1" height="4" /></td>
											<td align="center" class="boxInsideContent">
												<table width="100%"  border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td width="6" height="6"><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
														<td width="6" height="6"><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
														<td width="6" height="6"><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
													</tr>
													<tr>
														<td width="6" height="6"><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
														<td width="100%">
															<table width="100%"  border="0" cellspacing="0" cellpadding="0">
																<tr>
																	<td>   
																		<table width="100%" border="0" cellpadding="0" cellspacing="0" >
																			<tr>
																				<td class="bbBoxInsideTopLeft"></td>
																				<td class="bbBoxInsideTop"></td>
																				<td class="bbBoxInsideTopRight"></td>
																			</tr>
																			<tr>
																				<td class="bbBoxInsideLeft"><img runat="server" src="~/images/spacer.gif" width="5" height="1"/></td>
																				<td class="bbBoxInsideContent" align="left">
																					<table width="100%" border="0" cellspacing="0" cellpadding="0" align="left">
																						<tr>
																							<td width="5" height="5"><img runat="server" src="~/images/spacer.gif" width="5" height="5"/></td>
																							<td width="5" height="5"><img runat="server" src="~/images/spacer.gif" width="5" height="5"/></td>
																							<td width="5" height="5"><img runat="server" src="~/images/spacer.gif" width="5" height="5"/></td>
																						</tr>
																						<tr>
																							<td width="5" height="5"><img runat="server" src="~/images/spacer.gif" width="5" height="5"/></td>
																							<td width="100%">
																								<table width="80%" border="0" cellspacing="0" cellpadding="0" align="left">
                                          <tr>
                                            <td width="8%" align="right" nowrap="nowrap" class="textGeneralGray01">Blog for:</td>
                                            <td width="1%"><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
                                            <td width="91%" class="textGeneralGray01" align="left"><asp:Label ID="lblChannel" Runat="Server"/></td>
                                          </tr>
                                          <tr>
                                            <td colspan="3"><img runat="server" src="~/images/spacer.gif" width="1" height="6"/></td>
                                          </tr>
                                          <tr>
                                            <td align="right" nowrap="nowrap" class="textGeneralGray01">Title:</td>
                                            <td><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
                                            <td align="left">
												<asp:TextBox ID="txtSubject" class="formKanevaText" style="width:350px" MaxLength="100" runat="server"/>
												<asp:RequiredFieldValidator ID="rfSubject" ControlToValidate="txtSubject" Text="*" ErrorMessage="Subject is a required field." Display="Dynamic" runat="server"/>	
											</td>
                                          </tr>
                                          <tr>
                                            <td colspan="3"><img runat="server" src="~/images/spacer.gif" width="1" height="6"/></td>
                                          </tr>
                                          <tr>
                                            <td align="right" nowrap="nowrap" class="textGeneralGray01">Tags: </td>
                                            <td><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
                                            <td class="textGeneralGray03" align="left">
												<asp:TextBox ID="txtKeywords" class="formKanevaText" style="width:400px" MaxLength="250" runat="server"/>
												<asp:RegularExpressionValidator id="regtxtKeywords" ControlToValidate="txtKeywords" Text="*" Display="Dynamic" EnableClientScript="True" runat="server"/>
                            &nbsp;&nbsp;(spaces separated list)</td>
                                          </tr>
                                          <tr>
                                            <td colspan="3" nowrap="nowrap" class="textGeneralGray01"><img runat="server" src="~/images/spacer.gif" width="1" height="4"/></td>
                                          </tr>
                                          <tr>
                                            <td nowrap="nowrap" class="textGeneralGray01">&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td class="textGeneralGray03" align="left">Tags are unique keywords that help to categorize your item to enable user searches. </td>
                                          </tr>
                                      </table></td>
                                      <td width="5" height="5"><img runat="server" src="~/images/spacer.gif" width="5" height="5"/></td>
                                    </tr>
                                    <tr>
                                      <td width="5" height="5"><img runat="server" src="~/images/spacer.gif" width="5" height="5"/></td>
                                      <td width="5" height="5"><img runat="server" src="~/images/spacer.gif" width="5" height="5"/></td>
                                      <td width="5" height="5"><img runat="server" src="~/images/spacer.gif" width="5" height="5"/></td>
                                    </tr>
                                </table></td>
                                <td class="bbBoxInsideRight"><img runat="server" src="~/images/spacer.gif" width="5" height="1"/></td>
                              </tr>
                              <tr>
                                <td class="bbBoxInsideBottomLeft"></td>
                                <td class="bbBoxInsideBottom"></td>
                                <td class="bbBoxInsideBottomRight"></td>
                              </tr>
                            </table></td>
                          </tr>
                          <tr>
                            <td>&nbsp;</td>
                          </tr>
                          <tr>
                            <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td class="textGeneralGray01" align="left"><img runat="server" src="~/images/spacer.gif" width="2" height="1"/>Blog Entry </td>
                              </tr>
                              <tr>
                                <td><span class="textGeneralGray01"><img runat="server" src="~/images/spacer.gif" width="1" height="8"/></span></td>
                              </tr>
                              <tr>
                                <td class="bbGrayLine"><img runat="server" src="~/images/spacer.gif" width="1" height="1"/></td>
                              </tr>
                              <tr>
                                <td><span class="textGeneralGray01"><img runat="server" src="~/images/spacer.gif" width="1" height="5"/></span></td>
                              </tr>
                              <tr>
                                <td align="right" class="textGeneralGray01">
									<table cellspacing="0" cellpadding="0" border="0">
										<tr>
											<td valign="middle">
												<asp:CheckBox id="chkAdvanced" runat="server" AutoPostBack="True" OnCheckedChanged="chkAdvanced_Click"/>
											</td>
											<td valign="middle">&nbsp;<asp:Label CssClass="filter2" runat="server">Show advanced buttons</asp:label></td>
										</tr>
									</table>
								</td>
                              </tr>
                              <tr>
                                <td><span class="textGeneralGray01"><img runat="server" src="~/images/spacer.gif" width="1" height="8"/></span></td>
                              </tr>
                              <tr>
                                <td align="center">
									<CE:Editor id="txtBody" BackColor="#ededed" runat="server" width="100%" Height="300px" ShowHtmlMode="True" ConfigurationPath="~/CuteSoft_Client/CuteEditor/Configuration/AutoConfigure/blog.config" AutoConfigure="None" ></CE:Editor>
                                </td>
                              </tr>
                              <tr>
                                <td align="left"><span class="textGeneralGray01"><img runat="server" src="~/images/spacer.gif" width="1" height="8"/></span></td>
                              </tr>
                              <tr>
                                <td class="bbGrayLine"><img runat="server" src="~/images/spacer.gif" width="1" height="1"/></td>
                              </tr>
                              <tr>
                                <td align="left"><span class="textGeneralGray01"><img runat="server" src="~/images/spacer.gif" width="1" height="8"/></span></td>
                              </tr>
                              <tr>
                                <td align="right"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td width="100%" align="right"><asp:button id="btnCancel" runat="Server" onClick="btnCancel_Click" class="Filter2" Text="   cancel   " CausesValidation="False"/></td>
                                    <td><span class="textGeneralGray01"><img runat="server" src="~/images/spacer.gif" width="2" height="8"/></span></td>
                                    <td width="6%" align="right">
										<ajax:ajaxpanel id="ajpSubmit" runat="server">
										<asp:button id="btnUpdate" runat="Server" onClick="btnUpdate_Click" class="Filter2" Text="   submit   "/>
										</ajax:ajaxpanel>
									</td>
                                  </tr>
                                </table></td>
                              </tr>
                            </table></td>
                          </tr>
                        </table></td>
                        <td width="6" height="6"><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
                      </tr>
                      <tr>
                        <td width="6" height="6"><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
                        <td width="6" height="6"><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
                        <td width="6" height="6"><img runat="server" src="~/images/spacer.gif" width="6" height="6"/></td>
                      </tr>
                  </table></td>
                  <td class="boxInsideRight"><img runat="server" src="~/images/spacer.gif" width="1" height="4"/></td>
                </tr>
                <tr>
                  <td class="boxInsideBottomLeft" ><img runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
                  <td class="boxInsideBottom2"><img runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
                  <td class="boxInsideBottomRight"><img runat="server" src="~/images/spacer.gif" width="4" height="4" /></td>
                </tr>
            </table></td>
            <td class=""><img runat="server" src="~/images/spacer.gif" width="1" height="1"/></td>
          </tr>
          <tr>
            <td class=""></td>
            <td class=""></td>
            <td class=""></td>
          </tr>
        </table></td>
        </tr>
    </table></td>
  </tr>
</table>