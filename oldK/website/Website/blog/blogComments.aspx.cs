///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for blogComments.
	/// </summary>
	public class blogComments : BlogBasePage
	{
		protected blogComments () 
		{
			Title = "Blog Comments";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
            BlogFacade blogFacade = new BlogFacade();

			if (!IsPostBack)
			{
                SetReturnUrl ();

                GetBlogData ();
				BindComments ();
			}

			int topicId = Convert.ToInt32 (Request ["topicId"]);
            Blog blog = blogFacade.GetBlog(topicId);
				
			hlPerm.NavigateUrl = KanevaGlobals.GeBlogUrl (topicId);
			hlPerm.Text = KanevaGlobals.GeBlogUrl (topicId);

			// If the blog is in a private community, they may not have access to it
            if (blog.CommunityId > 0)
			{
                if (!CommunityUtility.HasCommunityAccess(blog.CommunityId, GetUserId(), Request.IsAuthenticated, KanevaWebGlobals.CurrentUser.IsAdult))
				{
					spnAlertMsg.InnerText = "You do not have access to this blog!";
                    spnAlertMsg.Style.Add ("display", "block"); 
                    return;
				}
			}

			// Add the javascript for deleting comments
			string  scriptString = "<script language=\"javaScript\">\n<!--\n function DeleteComment (id) {\n";
			scriptString += "	document.all.ThreadId.value = id;\n";			// save the page number clicked to the hidden field
            scriptString += ClientScript.GetPostBackEventReference(btnDeleteComments, "") + ";\n";
			scriptString += "}\n// -->\n";
			scriptString += "</script>";

            if (!ClientScript.IsClientScriptBlockRegistered(GetType (), "DeleteComment"))
			{
                ClientScript.RegisterClientScriptBlock(GetType (), "DeleteComment", scriptString);
			}

			AddBreadCrumb (new BreadCrumb ("Blog Comments", GetCurrentURL (), "", 0));

            if (Request.IsAuthenticated)
            {
                tblPostComment.Visible = true;
            }
            else
            {
                divLoginToComment.Visible = true;
            }

            // Security check to prevent spam blogging.  If user does not have a validated
            // email address then they are not allowed to blog
            if (KanevaWebGlobals.CurrentUser.StatusId.Equals ((int) Constants.eUSER_STATUS.REGNOTVALIDATED))
            {
                spnAlertMsg.InnerText = "You must validate your email address in order to blog.";
                spnAlertMsg.Style.Add ("display", "block");
                spnAlertMsg.Style.Add ("text-alignment", "center");
                txtResponse.Disabled = true;
                btnUpdate.Enabled = false;
                btnUpdate.Visible = false;
            }

            // Set Nav
            HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
            HeaderNav.SetNavVisible (HeaderNav.MyKanevaNav, 2);
        }


        #region Helper Methods

        /// <summary>
        /// Sets the return url based on querystring param or urlreferrer property of the request object
        /// </summary>
        /// <returns></returns>
        private void SetReturnUrl ()
        {
            try
            {
                if (Request.QueryString["back"] != null && Request.QueryString["back"] == "ML")
                {
                    returnURL = ResolveUrl ("~/asset/publishedItemsNew.aspx");
                    return;
                }

                returnURL = Request.UrlReferrer.ToString ();
            }
            catch { }
        }

        /// <summary>
		/// Get the blog data
		/// </summary>
		private void GetBlogData ()
		{
            BlogFacade blogFacade = new BlogFacade();

			int topicId = Convert.ToInt32 (Request ["topicId"]);
			Blog blog = blogFacade.GetBlog (topicId);
				
			imgBlogCategory.Src = ResolveUrl ("~/images/icon_general.gif");
			lblSubject.Text = TruncateWithEllipsis (blog.Subject, 50);
			lblCreatedDate.Text = FormatDateTime (blog.CreatedDate);

			aUserName.Attributes.Add ("href", GetPersonalChannelUrl (blog.NameNoSpaces));
			aUserName.InnerText = blog.Username;

			lblBodyText.Text = AddHrefNewWindow (blog.BodyText);
			lblTags.Text = KanevaGlobals.GetNormalizedTags(blog.Keywords);
		}
		
		private void BindComments ()
		{
            BlogFacade blogFacade = new BlogFacade();
            rptComments.DataSource = blogFacade.GetBlogComments(Convert.ToInt32(Request["topicId"]), 1, 500);
			rptComments.DataBind ();
		}

		/// <summary>
		/// Return the delete javascript
		/// </summary>
		/// <returns></returns>
		public string GetDeleteCommentScript (int threadId)
		{
			return "javascript:if (confirm(\'Are you sure you want to delete this comment?\')){DeleteComment (" + threadId + ")};";
		}

		/// <summary>
		/// Get the edit thread url
		/// </summary>
		/// <param name="threadId"></param>
		/// <returns></returns>
		protected string GetEditCommentURL (int threadId)
		{
			int topicId = Convert.ToInt32 (Request ["topicId"]);
			return "javascript:window.location = ('" + ResolveUrl ("~/blog/blogCommentEdit.aspx?threadId=" + threadId + "&topicId=" + topicId + GetCommunityIdQueryString () + "&returnURL=" + Request.CurrentExecutionFilePath) + "')";
		}

		/// <summary>
		/// GetThreadEditedText
		/// </summary>
		/// <param name="updatedUser"></param>
		/// <param name="lastUpdatedDate"></param>
		/// <returns></returns>
		protected string GetThreadEditedText (object updatedUser, object lastUpdatedDate)
		{
            if (!DBNull.Value.Equals(updatedUser) && updatedUser.ToString ().Length > 0)
			{
				return "Edited by " + updatedUser.ToString () + " on " + FormatDateTime (lastUpdatedDate);
			}

			return "";
        }

        #endregion Helper Methods


        #region Event Handlers

        /// <summary>
        /// Delete a Comment
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Delete_Comments (Object sender, EventArgs e)
        {
            if (IsAdministrator ())
            {
                BlogFacade blogFacade = new BlogFacade ();
                blogFacade.DeleteBlogComment (Convert.ToInt32 (ThreadId.Value), GetUserId ());
                BindComments ();
            }
        }

        /// <summary>
        /// User's response to a blog
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnResponse_click (object sender, EventArgs e)
        {
            spnAlertMsg.Style.Add ("display", "none"); 

            if (!Request.IsAuthenticated)
            {
                Response.Redirect (this.GetLoginURL ());

                spnAlertMsg.InnerText = "You must be signed in to post a comment.";
                spnAlertMsg.Style.Add ("display", "block"); 
                return;
            }

            if (txtResponse.Value.Trim().Length == 0)
                return;

            // Check to make sure user did not enter any "potty mouth" words
            if (KanevaWebGlobals.isTextRestricted (txtResponse.Value, Constants.eRESTRICTION_TYPE.POTTY_MOUTH))
            {
                spnAlertMsg.InnerText = Constants.VALIDATION_REGEX_RESTRICTED_WORD_MESSAGE;
                spnAlertMsg.Style.Add ("display", "block"); 
                return;
            }

            BlogFacade blogFacade = new BlogFacade ();
            blogFacade.InsertBlogComment (Convert.ToInt32 (Request["topicId"]), GetUserId (), Server.HtmlEncode (txtResponse.Value), Common.GetVisitorIPAddress());

            txtResponse.Value = "";
            GetBlogData ();
            BindComments ();
        }

        private void hlBack_Click (object sender, EventArgs e)
		{
            Response.Redirect (returnURL);
        }

        #endregion Event Handlers


        #region Properties

        protected string returnURL
        {
            get
            {
                if (ViewState["ReturnUrl"] != null)
                {
                    return ViewState["ReturnUrl"].ToString ();
                }
                else
                {
                    return ResolveUrl ("~/watch/watch.kaneva");
                }
            }
            set
            {
                ViewState["ReturnUrl"] = value;
            }
        }

        #endregion Properties


        #region Declerations

        protected HtmlImage imgBlogCategory;
		protected Label lblSubject;
		protected Label lblCreatedDate, lblBodyText;
		protected HtmlAnchor aUserName;

		protected HyperLink hlPerm;
		protected Label lblTags;

		// Comments 
		protected Repeater rptComments;
		protected HtmlTextArea txtResponse;
		protected HtmlInputHidden ThreadId;
        protected Button btnDeleteComments, btnUpdate;
		protected LinkButton hlBack;
        
        protected HtmlContainerControl divLoginToComment;
        protected HtmlContainerControl spnAlertMsg;
        protected LinkButton lbLoginToComment;
        protected HtmlTable tblPostComment;

        #endregion Declerations


        #region Web Form Designer generated code

        override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
			hlBack.Click +=new EventHandler(hlBack_Click);
		}
		
        #endregion
	}
}
