<%@ Page language="c#" Codebehind="blogComments.aspx.cs" ValidateRequest="False" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.blogComments" %>
<%@ Register TagPrefix="Kaneva" TagName="HeaderText" Src="../usercontrols/HeaderText.ascx" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>


<link href="../css/home.css" rel="stylesheet" type="text/css" />		
<link href="../css/Kaneva.css" rel="stylesheet" type="text/css"/>
<link href="../css/kanevaSC.css" rel="stylesheet" type="text/css"/>
<link href="../css/friends.css" rel="stylesheet" type="text/css"/>
<link href="../css/kanevaBroadBand.css" rel="stylesheet" type="text/css" />		
<link href="../css/kanevaText.css" rel="stylesheet" type="text/css" />
<link href="../css/new.css" rel="stylesheet" type="text/css" />


<table border="0" cellpadding="0" cellspacing="0" class="newcontainer" align="center">
	<tr>
		<td>
			<table border="0" cellpadding="0" cellspacing="0" width="100%" class="newcontainerborder">
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
				<tr>
					<td class=""><img runat="server" src="~/images/spacer.gif" width="1" height="1"></td>
					<td valign="top" class="newdatacontainer">
								
						<div style="width:70%;">
						
						<ajax:ajaxpanel id="ajpMsg" runat="server">
						<asp:ValidationSummary ShowMessageBox="False" ShowSummary="True" cssclass="errBox black" id="valSum" 
							DisplayMode="BulletList" runat="server" HeaderText="Please correct the following errors:"/>
						
						<span id="spnAlertMsg" runat="server" class="errBox black" style="display:none;"></span>
						</ajax:ajaxpanel>
													
						</div>
								
						<table  border="0" cellpadding="0" cellspacing="0" width="100%" class="boxInside" style="margin-top:6px;">
							<tr>
								<td class="boxInsideTopLeft"><img runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
								<td class="boxInsideTop" ><img runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
								<td  class="boxInsideTopRight"><img runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
							</tr>
							<tr class="boxInside">
								<td class="boxInsideleft" ><img runat="server" src="~/images/spacer.gif" width="1" height="4" /></td>
								<td class="boxInsideContent">
							  		<table border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr>
											<td width="10" height="13"><img runat="server" src="~/images/spacer.gif" width="10" height="13" /></td>
											<td width="30" valign="center"><img runat="server" id="imgBlogCategory" border="0"/></td>
											<td align="left" class="textGeneralGray01"><asp:Label id="lblSubject" runat="server"/></td>                                
											<td width="60" align="right"><asp:LinkButton ID="hlBack" runat="server" >< Back</asp:LinkButton>&nbsp;&nbsp;</td>
										</tr>
									</table>
								</td>
                          		<td class="boxInsideRight"><img runat="server" src="~/images/spacer.gif" width="1" height="4"/></td>
							</tr>
                        <tr>
                          <td class="boxInsideBottomLeft" ><img runat="server" src="~/images/spacer.gif" width="4" height="4" /></td>
                          <td class="boxInsideBottom"><img runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
                          <td class="boxInsideBottomRight"><img runat="server" src="~/images/spacer.gif" width="4" height="4" /></td>
                        </tr>
					</table>
                    <table  border="0" cellpadding="0" cellspacing="0" width="100%" >
                          <tr>
                            <td class="boxInsideTopLeft"><img runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
                            <td class="boxInsideTop" ><img runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
                            <td  class="boxInsideTopRight"><img runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
                          </tr>
                          <tr class="boxInside">
                            <td class="boxInsideleft" ><img runat="server" src="~/images/spacer.gif" width="1" height="4" /></td>
                            <td align="left" class="boxInsideContent">
								<table border="0" cellpadding="0" cellspacing="0" width="100%">
									<tr>
										<td width="10"><img runat="server" src="~/images/spacer.gif" width="1" height="10"/></td>
									<tr>
										<td><img runat="server" src="~/images/spacer.gif" width="10" height="8"/></td>
										<td align="left" valign="bottom"><font class="textGeneralGray03"><asp:Label id="lblCreatedDate" runat="server"/> </font>&nbsp;<font class="bolderText"><a id="aUserName" runat="server"></a> </font>&nbsp;<font class="textGeneralGray01"> tags:&nbsp;<asp:Label CssClass="bolderText" ID="lblTags" Runat="server"/></font></td>
										<td valign="top" align="right">
											<table border="0" cellpadding="0" cellspacing="0" width="100%">
												<tr>
													<td align="right"></td>
													<td class="bolderText" valign="top" align="left"></td>
													<td><img runat="server" src="~/images/spacer.gif" width="10" height="8"/></td>
												</tr>
											</table>
										</td>										
									</tr>
									<tr>						
										<td><img runat="server" src="~/images/spacer.gif" width="10" height="8"/></td>
										<td align="left" class="textGeneralGray01">
											<table border="0" cellpadding="0" cellspacing="0" width="98%">
												<tr>
													<td><img runat="server" src="~/images/spacer.gif" width="1" height="13"/></td>
												</tr>										
												<tr>
													<td>
														<asp:Label id="lblBodyText" runat="server"/>
													</td>
												</tr>
											</table>
										</td>
								  <td>&nbsp;</td></tr>						
								  <tr>
										<td><img runat="server" src="~/images/spacer.gif" width="1" height="10"/></td>								  
								  </tr>		  
							  </table>
							</td>
                            <td class="boxInsideRight"><img runat="server" src="~/images/spacer.gif" width="1" height="4"/></td>
                          </tr>
							<tr>
							<td colspan="8" class="boxInsideContent" align="left">
									<table border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td><img runat="server" src="~/images/spacer.gif" width="1" height="20"/></td>
										</tr>
										<tr>
											<td width="15"><img runat="server" src="~/images/spacer.gif" width="15" height="8"/></td>
											<td align="left" class="textGeneralGray01">This blog's link:</td>
											<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="8"/></td>
											<td align="left" class="bolderText"><asp:HyperLink ID="hlPerm" Runat="server"/></td>
										</tr>
										<tr>
											<td><img runat="server" src="~/images/spacer.gif" width="1" height="5"/></td>
										</tr>
									</table>
							  </td>
							</tr>
							<tr>
								<td class="bbGrayLine" colspan="8"><img runat="server" src="~/images/spacer.gif" width="1" height="1"/></td>
							</tr>
							<tr>
								<td colspan="8" class="boxInsideContent">
							<table border="0" cellpadding="0" cellspacing="0" align="center">
							<tr>
								<td colspan="3"><img runat="server" src="~/images/spacer.gif" width="1" height="20"/></td>								  
							</tr>
							
							<tr>
								<td colspan="3">
									<asp:Repeater id="rptComments" runat="server">
										<ItemTemplate>
											<center>
											<table cellpadding="0" cellspacing="0" border="0" style="width:600px">
												<tr>	
													<td bgcolor="#D3D3D3" width="100%" class="blogHeadline2" valign="middle">
														<img runat="server" src="~/images/spacer.gif" width="1" height="13"/>Comment from: <a class="blogHeadline2" href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "Username").ToString ())%>><%# DataBinder.Eval(Container.DataItem, "Username") %></a><span class="white">  <%# FormatDateTime (DataBinder.Eval(Container.DataItem, "CreatedDate")) %> </span>
													</td>
													<td align="right" bgcolor="#D3D3D3"><a class="white" runat="server" visible=<%# IsAdministrator ()%> href='<%# GetEditCommentURL ((int) DataBinder.Eval(Container.DataItem, "BlogCommentId"))%>'>edit</a>&nbsp;<a class="white" runat="server" ID="btnDelete" Visible='<%# IsAdministrator ()%>' href='<%#GetDeleteCommentScript ((int) DataBinder.Eval(Container.DataItem, "BlogCommentId")) %>'>delete</a>&nbsp;&nbsp;</td>
												</tr>
											</table>
											<table cellpadding="0" cellspacing="0" border="0" width="600" class="LeftRightBorders BottomBorders">							
												<tr>
													<td colspan="2" class="blogBody02" valign="top" width="100%" >
													<%# DataBinder.Eval(Container.DataItem, "BodyText") %>
													<br><br><asp:Label runat="server" id="lblEdited" CssClass="threadEdit" Text='<%# GetThreadEditedText (DataBinder.Eval(Container.DataItem, "UpdatedUsername"), DataBinder.Eval(Container.DataItem, "LastUpdatedDate")) %>'/><br><br>
													</td>
												</tr>	
											</table>
											</center>
										</ItemTemplate>
									</asp:Repeater>										
								</td>
							<tr>
							
							<tr>
								<td>
									<div id="divLoginToComment" runat="server" style="text-align:center;width:100%" visible="false">
										<asp:linkbutton id="lbLoginToComment" runat="server" onclick="btnResponse_click">login to post comment</asp:linkbutton>
										<br /><br />
									</div>
									
									<table id="tblPostComment" runat="server" visible="false" border="0" cellpadding="0" cellspacing="0" width="100%" >
										<tr>
											<td><img runat="server" src="~/images/spacer.gif" width="1" height="1"/></td>								  
											<td class="textGeneralGray01" align="center">Your comment</td>
											<td><img runat="server" src="~/images/spacer.gif" width="1" height="1"/></td>								  
										</tr>
										<tr>
											<td colspan="3"><img runat="server" src="~/images/spacer.gif" width="1" height="10"/></td>								  
										</tr>
										<tr>
											<td><img runat="server" src="~/images/spacer.gif" width="1" height="1"/></td>								  
											<td align="center" >
												<textarea runat="server" id="txtResponse" rows="20" cols="70" class="formKanevaText"></textarea>
											</td>
											<td><img runat="server" src="~/images/spacer.gif" width="1" height="1"/></td>								  
										</tr>
										<tr>
											<td colspan="3"><img runat="server" src="~/images/spacer.gif" width="1" height="10"/></td>								  
										</tr>
										<tr>
											<td><img runat="server" src="~/images/spacer.gif" width="1" height="1"/></td>								  
											<td class="bodyText" align="center">
												<asp:button id="btnUpdate" runat="Server" onClick="btnResponse_click" class="Filter2" Text="post comment"/>
											</td>
											<td><img runat="server" src="~/images/spacer.gif" width="1" height="1"/></td>								  
										</tr>
									</table>
								</td>
							</tr>
							
							<tr>
								<td colspan="3"><img runat="server" src="~/images/spacer.gif" width="1" height="10"/></td>								  
							</tr>
						</table>								
						</td>							
							</tr>
                          <tr>
                            <td class="boxInsideBottomLeft" ><img runat="server" src="~/images/spacer.gif" width="4" height="4" /></td>
                            <td class="boxInsideBottom2"><img runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
                            <td class="boxInsideBottomRight"><img runat="server" src="~/images/spacer.gif" width="4" height="4" /></td>
                          </tr>
              </table>
		    </td>
            <td class=""><img runat="server" src="~/images/spacer.gif" width="1" height="1"></td>
          </tr>
					<tr>
                      <td class=""></td>
                      <td class=""></td>
                      <td class=""></td>
                    </tr>				  
		  </table>
		</td>
	</tr>
</table>

<input type="hidden" runat="server" id="ThreadId" value="0"> 
<asp:button ID="btnDeleteComments" OnClick="Delete_Comments" runat="server" Visible="false"></asp:button>