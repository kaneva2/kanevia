<%@ Page language="c#" Codebehind="blogCommentEdit.aspx.cs" ValidateRequest="False" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.commentEdit" %>
<%@ Register TagPrefix="Kaneva" TagName="HeaderText" Src="../usercontrols/HeaderText.ascx" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>

<link href="../css/new.css" rel="stylesheet" type="text/css" />		


<table border="0" cellspacing="0" cellpadding="0" class="newcontainer" align="center">
	<tr>
		<td>
			<table  border="0" cellpadding="0" cellspacing="0" width="100%" class="newcontainerborder">
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
				<tr>
					<td class=""><img id="Img2" runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
					<td valign="top" class="newdatacontainer">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td colspan="3">
								
									<!-- TOP STATUS BAR -->
									<div id="pageheader">
										<table cellpadding="0" cellspacing="0" border="0" width="99%">
											<tr>
												<td align="left">
													<h1>Blog Comment Edit</h1>
												</td>
											</tr>
										</table>
									</div>
									<!-- END TOP STATUS BAR -->		
								
								</td>
							</tr>
							<tr>
								<td width="968" align="left"  valign="top">
									<table border="0" cellspacing="0" cellpadding="0" width="100%">
										<tr>
											<td width="99%" align="center" valign="top">
												<div class="module">
												<span class="ct"><span class="cl"></span></span>
                                                
												<table cellpadding="10" cellspacing="0" width="90%" border="0" align="center">
                                                    <tr>
                                                        <td align="center">
								
															<div style="width:70%;margin-bottom:10px;">
															
															<ajax:ajaxpanel id="ajpMsg" runat="server">
															<asp:ValidationSummary ShowMessageBox="False" ShowSummary="True" cssclass="errBox black" id="valSum" 
																DisplayMode="BulletList" runat="server" HeaderText="Please correct the following errors:"/>
															
															<span id="spnAlertMsg" runat="server" class="errBox black" style="display:none;"></span>
															</ajax:ajaxpanel>
																						
															</div>
								
															<table runat="server" cellpadding="0" cellspacing="0" border="0" width="560" ID="tblCommentEdit">	
																<tr>
																	<td align="center" class="subHead">Edit Comment</td>
																</tr>											 
																<tr>
																	<td align="center" width="100%">
																		<textarea runat="server" id="txtComment" rows="10" cols="50"></textarea>
																		<asp:RequiredFieldValidator ID="rftxtComment" enableclientscript="false" ControlToValidate="txtComment" Text="" ErrorMessage="Comment is a required field." Display="Dynamic" runat="server"/>
																	</td>
																</tr>											  
																<tr>
																	<td align="right" width="100%"><br/>
																		<asp:button id="btnCancel" CausesValidation="False" runat="Server" onClick="btnCancel_Click" class="Filter2" Text=" cancel "/>&nbsp;
																		<ajax:ajaxpanel id="ajpSubmit" runat="server">
																		<asp:button id="btnUpdate" runat="Server" onClick="btnUpdate_Click" class="Filter2" Text=" submit "/>
																		</ajax:ajaxpanel><img runat="server" src="~/images/spacer.gif" width="67" height="10"/><br/><br/><br/><br/>
																	</td>
																</tr>
															</table>	

														</td>
													</tr>
												 </table>

                                                <span class="cb"><span class="cl"></span></span>
												</div>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				    <td class=""><img id="Img3" runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
			    </tr>
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
				
			</table>
		</td>
	</tr>
</table>
