///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using log4net;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for EditBlog.
	/// </summary>
	public class blogEdit : BlogBasePage
	{
		/// <summary>
		/// Constructor
		/// </summary>
		protected blogEdit () 
		{
			Title = "Blog Edit";
		}

		/// <summary>
		/// Page_Load
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Page_Load (object sender, System.EventArgs e)
		{
            BlogFacade blogFacade = new BlogFacade();

			if (Request.IsAuthenticated)
			{
				if (GetRequestParams())
				{
					//check if user is allowed to edit this channel
					if(!IsUserAllowedToEdit())
					{
						RedirectToHomePage ();
					}
				}
				else
				{
					//invalid request params
					//if the user logged in, send him to his home page
					//if not, redirect to login page
					RedirectToHomePage ();
				}
			}
			else
			{
				Response.Redirect(this.GetLoginURL());
			}

			// Extend the timeout for this page
			this.AddKeepAlive ();

			//setup header nav bar
            if (CommunityId == this.GetPersonalChannelId ())
			{
				//user's own channel
				HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.PROFILE;
			}
			else
			{
				//navs go to home-my channels for all channels if the user is an admin
				HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.CHANNELS;
			}

			// Set Nav
            HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
			HeaderNav.SetNavVisible(HeaderNav.MyKanevaNav,2);

            if (!IsPostBack)
            {
                SetReturnUrl ();

                HeaderNav.MyChannelsNav.ChannelId = CommunityId;

                lblChannel.Text = GetCommunityFacade.GetCommunity (CommunityId).Name;
                //**
                //                DataRow drChannel = CommunityUtility.GetCommunity(CommunityId);
                //				lblChannel.Text = drChannel["name"].ToString();


                // Get the blog data
                Blog blog = blogFacade.GetBlog (_topicId);

                //**
                //				if (IsNewBlog ())
                //				{
                //                    int newTopicId = blogFacade.InsertBlog(Convert.ToInt32(Request["communityId"]), "", GetUserId(), KanevaWebGlobals.CurrentUser.Username, Common.GetVisitorIPAddress());
                //					ViewState ["newTopicId"] = newTopicId;
                //				}

                txtSubject.Text = Server.HtmlDecode (blog.Subject);
                lblItemName.Text = txtSubject.Text;
                txtBody.Text = blog.BodyText;
                txtBody.ShowEnlargeButton = true;
                txtBody.ShowDecreaseButton = false;

                txtKeywords.Text = Server.HtmlDecode (blog.Keywords);
            }

            // Security check to prevent spam blogging.  If user does not have a validated
            // email address then they are not allowed to blog
            if (KanevaWebGlobals.CurrentUser.StatusId.Equals ((int) Constants.eUSER_STATUS.REGNOTVALIDATED) ||
                KanevaWebGlobals.CurrentUser.WokPlayerId == 0)
            {
                spnAlertMsg.InnerText = "You must validate your email address in order to blog.";
                spnAlertMsg.Style.Add ("display", "block");
                spnAlertMsg.Style.Add ("text-alignment", "center");
                txtSubject.Enabled = false;
                lblItemName.Enabled = false;
                txtBody.Enabled = false;
                txtKeywords.Enabled = false;
                btnUpdate.Enabled = false;
                btnUpdate.Visible = false;
                chkAdvanced.Enabled = false;
            }

			// Tag validator
			regtxtKeywords.ValidationExpression = Constants.VALIDATION_REGEX_TAG;
			regtxtKeywords.ErrorMessage = Constants.VALIDATION_REGEX_TAG_ERROR_MESSAGE;
        }


        #region Helper Methods

        /// <summary>
        /// Sets the return url based on querystring param or urlreferrer property of the request object
        /// </summary>
        /// <returns></returns>
        private void SetReturnUrl ()
        {
            try
            {
                if (Request.QueryString["communityId"] != null)
                {
                    returnURL = ResolveUrl ("~/blog/blog.aspx?communityId=" + Request.QueryString["communityId"].ToString());
                    return;
                }

                returnURL = Request.UrlReferrer.ToString ();
            }
            catch { }
        }

        private bool GetRequestParams ()
		{
            BlogFacade blogFacade = new BlogFacade();

			bool retVal = true;
			try
			{
				if(Request ["topicId"] != null && Request ["topicId"] != "")
				{
					//not pageId passed, get channelId and find the default page
					_topicId = Convert.ToInt32 (Request ["topicId"]);
				}

				if(_topicId > 0)
				{
					//edit
                    Blog blog = blogFacade.GetBlog(_topicId);
                    CommunityId = blog.CommunityId;
                    retVal = CommunityId > 0;
				}
				else
				{
					//new
                    CommunityId = Convert.ToInt32 (Request["communityId"]);
                    if (CommunityId <= 0)
					{
                        CommunityId = this.GetPersonalChannelId ();
					}
				}
			}
			catch(Exception)
			{
				//invalid numbers
				retVal = false;
			}
			return retVal;
		}

		/// <summary>
		/// returns true if current user is allowed to edit this channel
		/// </summary>
		/// <returns></returns>
		private bool IsUserAllowedToEdit ()
		{
            return IsAdministrator () || CommunityUtility.IsCommunityModerator (CommunityId, GetUserId ());
		}

		/// <summary>
		/// GetBlogId
		/// </summary>
		/// <returns></returns>
		private int GetBlogId ()
		{
			if (Convert.ToInt32 (Request ["topicId"]).Equals (0))
			{
				// This is an add new and was already inserted above
				return Convert.ToInt32 (ViewState ["newTopicId"]);
			}
			else
			{
				return Convert.ToInt32 (Request ["topicId"]);
			}
		}

		/// <summary>
		/// IsNewBlog
		/// </summary>
		/// <returns></returns>
		private bool IsNewBlog ()
		{
			return _topicId <= 0;
        }

        #endregion Helper Methods


        #region Event Handlers

        /// <summary>
		/// chkAdvanced_Click
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void chkAdvanced_Click (Object sender, EventArgs e)
		{
			if (chkAdvanced.Checked)
			{
				txtBody.ConfigurationPath = ResolveUrl ("~/CuteSoft_Client/CuteEditor/Configuration/AutoConfigure/blogAdvanced.config");
			}
			else
			{
				txtBody.ConfigurationPath = ResolveUrl ("~/CuteSoft_Client/CuteEditor/Configuration/AutoConfigure/blog.config");
			}

			txtBody.Focus = true;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnUpdate_Click (object sender, EventArgs e) 
		{
            BlogFacade blogFacade = new BlogFacade();
            User user = KanevaWebGlobals.CurrentUser;
            spnAlertMsg.Style.Add("display", "none");

            if (!KanevaWebGlobals.CurrentUser.StatusId.Equals ((int) Constants.eUSER_STATUS.REGNOTVALIDATED) &&
                KanevaWebGlobals.CurrentUser.HasWOKAccount)
            {
                // Check for any inject scripts
                if (KanevaWebGlobals.ContainsInjectScripts (txtBody.Text) ||
                    KanevaWebGlobals.ContainsInjectScripts (txtSubject.Text) ||
                    KanevaWebGlobals.ContainsInjectScripts (txtKeywords.Text))
                {
                    m_logger.Warn ("User " + user.UserId + " tried to script from IP " + Common.GetVisitorIPAddress());
                    spnAlertMsg.InnerText = "Your input contains invalid scripting, please remove script code and try again.";
                    return;
                }

                // Check to make sure user did not enter any "potty mouth" words
                if (KanevaWebGlobals.isTextRestricted (txtBody.Text, Constants.eRESTRICTION_TYPE.POTTY_MOUTH) ||
                    KanevaWebGlobals.isTextRestricted (txtSubject.Text, Constants.eRESTRICTION_TYPE.POTTY_MOUTH) ||
                    KanevaWebGlobals.isTextRestricted (txtKeywords.Text, Constants.eRESTRICTION_TYPE.POTTY_MOUTH))
                {
                    spnAlertMsg.InnerText = Constants.VALIDATION_REGEX_RESTRICTED_WORD_MESSAGE;
                    spnAlertMsg.Style.Add ("display", "block");
                    return;
                }

                // Server validation
                if (!Page.IsValid)
                {
                    return;
                }

                // Add to Blast if it is new
                if (IsNewBlog ())
                {
                    // Add the blog
                    GetBlogFacade.InsertBlog (CommunityId, Server.HtmlEncode (txtSubject.Text), txtBody.Text, user.UserId,
                        user.Username, Common.GetVisitorIPAddress(), Server.HtmlEncode (Common.GetNormalizedTags (txtKeywords.Text)));

                    if (user.BlastPrivacyBlogs)
                    {
                        UserFacade userFacade = new UserFacade();
                        string requestId = userFacade.InsertTrackingRequest(Constants.cREQUEST_TRACKING_REQUEST_TYPE_ID_BLOG, KanevaWebGlobals.CurrentUser.UserId, 0, 0);
                        string requestTypeSentIdBlast = userFacade.InsertTrackingTypeSent(requestId, Constants.cREQUEST_TRACKING_MESSAGE_TYPE_ID_BLAST, 0);

                        BlastFacade blastFacade = new BlastFacade ();
                        blastFacade.SendBlogBlast(user.UserId, user.Username, user.NameNoSpaces, GetBlogId(), "http://" + Configuration.SiteName + "/blog/blog.aspx?blogId=" + GetBlogId().ToString() + "&" + Constants.cREQUEST_TRACKING_URL_PARAM + "=" + requestTypeSentIdBlast);
                    }
                }
                else
                {
                    int result = blogFacade.UpdateBlog (user.UserId, GetBlogId (), Server.HtmlEncode (txtSubject.Text), txtBody.Text,
                        (int) BlogStatus.Active,
                        Server.HtmlEncode (Common.GetNormalizedTags (txtKeywords.Text)));

                    // Was it successful?
                    if (result == 1)
                    {
                        spnAlertMsg.InnerText = "Blog subject \\'" + txtSubject.Text + "\\' already exists, please change the subject.";
                        return;
                    }
                }
            }

			//NavigateToReturn ();
            Response.Redirect (returnURL);
		}

		/// <summary>
		/// Cancel click
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnCancel_Click (object sender, EventArgs e) 
		{
			Response.Redirect (returnURL);
        }

        #endregion Event Handlers


        #region Properties

        protected string returnURL
        {
            get
            {
                if (ViewState["ReturnUrl"] != null)
                {
                    return ViewState["ReturnUrl"].ToString ();
                }
                else
                {
                    return ResolveUrl ("~/blog/blog.aspx?communityId=" + CommunityId.ToString ());
                }
            }
            set                             
            {
                ViewState["ReturnUrl"] = value;
            }
        }

        private int CommunityId
        {
            get
            {
                try
                {
                    if (ViewState["CommunityId"] != null)
                    {
                        return Convert.ToInt32 (ViewState["CommunityId"]);
                    }
                    else
                    {
                        return 0;
                    }
                }
                catch
                {
                    return 0;
                }
            }
            set
            {
                ViewState["CommunityId"] = value;
            }
        }

        #endregion Properties
        

        #region Declerations

        protected HtmlTable tblBlogEdit;
        protected HtmlContainerControl spnAlertMsg;
        protected Button btnUpdate;

		protected CuteEditor.Editor txtBody;
		protected TextBox txtSubject;
		protected Label lblItemName;

		protected TextBox txtKeywords;
		protected RegularExpressionValidator regtxtKeywords;

		protected CheckBox chkAdvanced;
		protected Label lblChannel;

		private	int	_topicId;

		// Logger
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

        #endregion Declerations


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
