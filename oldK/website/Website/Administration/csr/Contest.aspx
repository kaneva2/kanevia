<%@ Register TagPrefix="Kaneva" TagName="AdminMenu" Src="../../usercontrols/AdministrativeMenu.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="activityNav" Src="activityNav.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="CSRNav" Src="csrNav.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../../usercontrols/Pager.ascx" %>
<%@ Page language="c#" SmartNavigation=true Codebehind="Contest.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.csr.Contest" %>

<center>
<Kaneva:AdminMenu ID="adminmenu" runat="server" />
<KANEVA:CSRNAV id="csrNavigation" runat="server" SubTab="ca"></KANEVA:CSRNAV><br>
	<br>
</center>

 <table id="tblUserFame" border="0" cellpadding="0" cellspacing="0" width="1000" style="background-color:#ffffff">
	<tr>
		<td style="height:20px"></td>
    </tr>
    <tr>
         <td align="center">

<table id="tblNoCSR" cellSpacing="0" cellPadding="0" border="0" runat="server">
	<tr>
		<td align="center"><BR>
			<BR>
			<BR>
			<span class="subHead">You must be a CSR to access this section.</span>
		</td>
	</tr>
</table>


<asp:panel id="pnlCSR" runat="server">
	<TABLE id="body" cellSpacing="0" cellPadding="5" width="100%" border="0">
		<TR>
			<TD vAlign="middle" align="left" colSpan="2">
				<asp:Label id="lbl_ExistingContests" runat="server" Font-Bold="True">Existing Contests</asp:Label>&nbsp;
				<asp:Label id="lbl_Error" runat="server" Font-Bold="True" ForeColor="Red" Font-Italic="True"></asp:Label></TD>
		</TR>
		<TR>
			<TD vAlign="middle" align="left" width="100%" colSpan="2">
				<asp:ListBox id="lbx_Contests" runat="server" AutoPostBack="True" Width="400px" Height="200px"></asp:ListBox></TD>
		</TR>
		<TR>
			<TD vAlign="middle" align="left" width="100%" colSpan="2">
				<asp:TextBox id="TextBox1" runat="server" Width="300px"></asp:TextBox>
				<asp:Button id="btn_ContestFind" runat="server" Text="Search" Width="100px"></asp:Button></TD>
		</TR>
		<TR>
			<TD vAlign="bottom" align="left" width="100%"><BR>
				<TABLE cellSpacing="0" cellPadding="0" align="left" border="0">
					<TR>
						<TD vAlign="bottom" align="left">
							<asp:Button id="btn_New1" runat="server" Text="NEW" CausesValidation="False" width="100px"></asp:Button></TD>
						<TD vAlign="bottom" align="left">
							<asp:Button id="btn_Delete1" runat="server" Text="DELETE CONTEST" CausesValidation="False" width="150px"
								Enabled="False"></asp:Button></TD>
						<TD vAlign="bottom" align="left">
							<asp:Button id="btn_Save1" runat="server" Text="SAVE CHANGES" width="200px"></asp:Button></TD>
					</TR>
				</TABLE>
				<BR>
			</TD>
		</TR>
		<TR>
			<TD vAlign="middle" align="left" width="100%" colSpan="2" height="20"><BR>
				<SPAN style="COLOR: #ffffff">Selected Contest Information</SPAN>
				<HR align="left" width="100%" SIZE="2">
			</TD>
		</TR>
		<TR>
			<TD vAlign="middle" align="left">
				<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
					<TR>
						<TD width="20">&nbsp;</TD>
						<TD vAlign="top" align="left" width="180">
							<asp:Label id="lbl_ContestChannel" runat="server" Font-Bold="True">Contest's Community</asp:Label></TD>
						<TD align="left">
							<asp:DropDownList id="ddl_Channels" runat="server" Width="400px"></asp:DropDownList>
							<asp:CompareValidator id="cpv_Channel" runat="server" Operator="NotEqual" ValueToCompare="-1" ControlToValidate="ddl_Channels"
								ErrorMessage="you must select a community for the contest before proceeding."></asp:CompareValidator></TD>
					</TR>
					<TR>
						<TD colSpan="2">&nbsp;</TD>
						<TD align="left">
							<asp:TextBox id="tbx_searchField" runat="server" Width="340px"></asp:TextBox>
							<asp:Button id="btn_ChannelSarch" runat="server" Text="Search"></asp:Button></TD>
					</TR>
				</TABLE>
			</TD>
		</TR>
		<TR>
			<TD vAlign="middle" align="left" width="100%" height="43"><BR>
			</TD>
		</TR>
		<TR>
			<TD vAlign="middle" align="left">
				<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
					<TR>
						<TD width="20">&nbsp;</TD>
						<TD vAlign="top" align="left" width="180">
							<asp:Label id="lbl_ContestName" runat="server" Font-Bold="True">Contest's Name</asp:Label></TD>
						<TD>
							<asp:TextBox id="tbx_ContestName" runat="server" Width="400px"></asp:TextBox>
							<asp:RequiredFieldValidator id="rfv_ContestName" runat="server" ControlToValidate="tbx_ContestName" ErrorMessage="You must enter a contest name before proceeding"></asp:RequiredFieldValidator></TD>
					</TR>
				</TABLE>
			</TD>
		</TR>
		<TR>
			<TD vAlign="middle" align="left" width="100%" height="20"><BR>
			</TD>
		</TR>
		<TR>
			<TD vAlign="middle" align="left">
				<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
					<TR>
						<TD width="20">&nbsp;</TD>
						<TD vAlign="top" width="180">
							<asp:Label id="Label1" runat="server" Font-Bold="True">Contest Terms</asp:Label></TD>
						<TD>
							<asp:TextBox id="tbx_Terms" runat="server" Width="480px" Height="70px" TextMode="MultiLine"></asp:TextBox></TD>
					</TR>
				</TABLE>
			</TD>
		</TR>
		<TR>
			<TD vAlign="middle" align="left" width="100%" height="20"><BR>
			</TD>
		</TR>
		<TR>
			<TD vAlign="middle" align="left" width="100%">
				<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
					<TR>
						<TD width="20">&nbsp;</TD>
						<TD width="400">
							<asp:Label id="lbl_UploadStart" runat="server" Font-Bold="True">Upload Start Date</asp:Label></TD>
						<TD>
							<asp:Label id="lbl_UploadEnd" runat="server" Font-Bold="True">Upload End Date</asp:Label></TD>
					</TR>
					<TR>
						<TD width="20">&nbsp;</TD>
						<TD width="400">
							<asp:Calendar id="cld_UploadStart" runat="server">
								<DayHeaderStyle Font-Bold="True" BorderColor="CornflowerBlue" BackColor="CornflowerBlue"></DayHeaderStyle>
								<SelectedDayStyle Font-Bold="True" ForeColor="Black" BackColor="Red"></SelectedDayStyle>
								<WeekendDayStyle BackColor="#C0C0FF"></WeekendDayStyle>
								<OtherMonthDayStyle Font-Size="Smaller" Font-Italic="True" BackColor="#EEEEEE"></OtherMonthDayStyle>
							</asp:Calendar></TD>
						<TD>
							<asp:Calendar id="cld_UploadEnd" runat="server">
								<DayHeaderStyle Font-Bold="True" BorderColor="CornflowerBlue" BackColor="CornflowerBlue"></DayHeaderStyle>
								<SelectedDayStyle Font-Bold="True" ForeColor="Black" BackColor="Red"></SelectedDayStyle>
								<WeekendDayStyle BackColor="#C0C0FF"></WeekendDayStyle>
								<OtherMonthDayStyle Font-Size="Smaller" Font-Italic="True" BackColor="#EEEEEE"></OtherMonthDayStyle>
							</asp:Calendar></TD>
					</TR>
				</TABLE>
			</TD>
		</TR>
		<TR>
			<TD vAlign="middle" align="left" width="100%" height="20"><BR>
			</TD>
		</TR>
		<TR>
			<TD vAlign="middle" align="left" width="100%">
				<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
					<TR>
						<TD width="20">&nbsp;</TD>
						<TD width="400">
							<asp:Label id="lbl_voteStart" runat="server" Font-Bold="True">Vote Start Date</asp:Label></TD>
						<TD>
							<asp:Label id="lbl_VoteEnd" runat="server" Font-Bold="True">Vote End Date</asp:Label></TD>
					</TR>
					<TR>
						<TD width="20">&nbsp;</TD>
						<TD width="400">
							<asp:Calendar id="cld_VoteStart" runat="server">
								<DayHeaderStyle Font-Bold="True" BorderColor="IndianRed" BackColor="IndianRed"></DayHeaderStyle>
								<SelectedDayStyle Font-Bold="True" ForeColor="Black" BackColor="Red"></SelectedDayStyle>
								<WeekendDayStyle BackColor="MistyRose"></WeekendDayStyle>
								<OtherMonthDayStyle Font-Size="Smaller" Font-Italic="True" BackColor="#EEEEEE"></OtherMonthDayStyle>
							</asp:Calendar></TD>
						<TD>
							<asp:Calendar id="cld_VoteEnd" runat="server">
								<SelectorStyle BackColor="Transparent"></SelectorStyle>
								<DayHeaderStyle Font-Bold="True" BorderColor="IndianRed" BackColor="IndianRed"></DayHeaderStyle>
								<SelectedDayStyle Font-Bold="True" ForeColor="Black" BackColor="Red"></SelectedDayStyle>
								<WeekendDayStyle BackColor="MistyRose"></WeekendDayStyle>
								<OtherMonthDayStyle Font-Size="Smaller" Font-Italic="True" BackColor="#EEEEEE"></OtherMonthDayStyle>
							</asp:Calendar></TD>
					</TR>
				</TABLE>
			</TD>
		</TR>
		<TR>
			<TD vAlign="middle" align="left" width="100%" height="20"><BR>
			</TD>
		</TR>
		<TR>
			<TD vAlign="middle" align="left">
				<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
					<TR>
						<TD width="20" height="23">&nbsp;</TD>
						<TD width="180" height="23">
							<asp:Label id="lbl_LightBoxImage" runat="server" Font-Bold="True">Light Box Contest Image</asp:Label></TD>
						<TD height="23">&nbsp;
							<asp:Label id="lbl_contestImage" runat="server"></asp:Label><INPUT class="formKanevaText" id="fil_ContestImage" type="file" onchange="UpdateFileName(document.frmMain.txtItemName1, document.frmMain.inpTorrent1.value)"
								size="60" name="Upload" runat="server">
							<asp:Button id="btn_Change1" runat="server" Text="Change" CausesValidation="False" Visible="False"></asp:Button></TD>
					</TR>
					<TR>
						<TD width="20" height="19">&nbsp;</TD>
						<TD width="180" height="19">
							<asp:Label id="lbl_YesImage" runat="server" Font-Bold="True">Yes Button Image</asp:Label></TD>
						<TD height="19">&nbsp;
							<asp:Label id="lbl_YesButton" runat="server"></asp:Label><INPUT class="formKanevaText" id="fil_YesButton" type="file" onchange="UpdateFileName(document.frmMain.txtItemName1, document.frmMain.inpTorrent1.value)"
								size="60" name="Upload" runat="server">
							<asp:Button id="btn_Change2" runat="server" Text="Change" CausesValidation="False" Visible="False"></asp:Button></TD>
					</TR>
					<TR>
						<TD width="20">&nbsp;</TD>
						<TD width="180">
							<asp:Label id="lbl_NoImage" runat="server" Font-Bold="True">No Button Image</asp:Label></TD>
						<TD>&nbsp;
							<asp:Label id="lbl_NoButton" runat="server"></asp:Label><INPUT class="formKanevaText" id="fil_NoButton" type="file" onchange="UpdateFileName(document.frmMain.txtItemName1, document.frmMain.inpTorrent1.value)"
								size="60" name="Upload" runat="server">
							<asp:Button id="btn_Change3" runat="server" Text="Change" CausesValidation="False" Visible="False"></asp:Button></TD>
					</TR>
				</TABLE>
			</TD>
		</TR>
		<TR>
			<TD vAlign="middle" align="left" width="100%" colSpan="2" height="20"><BR>
				<SPAN style="COLOR: #ffffff"></SPAN>
				<HR align="left" width="100%" SIZE="2">
			</TD>
		</TR>
		<TR>
			<TD vAlign="bottom" align="left" width="100%">
				<TABLE cellSpacing="0" cellPadding="0" align="left" border="0">
					<TR>
						<TD vAlign="bottom" align="left">
							<asp:Button id="btn_New2" runat="server" Text="NEW" CausesValidation="False" width="100px"></asp:Button></TD>
						<TD vAlign="bottom" align="left">
							<asp:Button id="btn_Delete2" runat="server" Text="DELETE CONTEST" CausesValidation="False" width="150px"
								Enabled="False"></asp:Button></TD>
						<TD vAlign="bottom" align="left">
							<asp:Button id="btn_Save2" runat="server" Text="SAVE CHANGES" width="200px"></asp:Button></TD>
					</TR>
				</TABLE>
			</TD>
		</TR>
		<TR>
			<TD class="belowFilter" vAlign="middle" align="left" width="230"></TD>
		</TR>
	</TABLE>
</asp:panel>

		</td>
	</tr>
</table>