<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="bannedIPs.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.csr.bannedIPs" %>
<%@ Register TagPrefix="Kaneva" TagName="AdminMenu" Src="../../usercontrols/AdministrativeMenu.ascx" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../../usercontrols/Pager.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="CSRNav" Src="csrNav.ascx" %>


<script type="text/javascript" language="javascript" src="../../jscript/prototype.js"></script>

<link href="../../css/new.css" rel="stylesheet" type="text/css" />
<link href="../../css/yahoo/calendar.css" type="text/css" rel="stylesheet" />   

<Kaneva:AdminMenu ID="adminmenu" runat="server" />
<Kaneva:CSRNav runat="server" id="csrNavigation" SubTab="ip"/>

<br/>

<table id="tblUserFame" border="0" cellpadding="0" cellspacing="0" width="1000" style="background-color:#ffffff">
	<tr>
        <td align="center">
			<span style="height:30px; font-size:28px; font-weight:bold">Banned IP Addresses</span><br />
			<ajax:ajaxpanel id="ajpError" runat="server">
			<asp:label runat="server" id="lblErrMessage" class="errBox black" style="width:60%;margin-top:20px;text-align:center;"></asp:label></ajax:ajaxpanel>
		</td>
    </tr>
	<tr>
		<td style="height:20px"></td>
    </tr>
    <tr>
         <td align="center">

		 
			<ajax:ajaxpanel id="ajpBannedIPs" runat="server">  
		 
				<asp:repeater id="rptBannedIPs" runat="server" onitemcommand="rptBannedIPs_ItemCommand">
				
					<headertemplate>
						<div style="border-color:DarkGray;border-width:1px;border-style:Solid;font-size:12px;width:800px;">
						<table cellpadding="4" cellspacing="0" border="0" width="100%">
							<tr style="color:Black;background-color:LightGrey;font-weight:bold;">
								<th align="left">IP Address</th>
								<th align="left">Ban Start</th>
								<th align="left">Ban End</th>
								<th></th>
							</tr>
					</headertemplate>
					
					<itemtemplate>
							<tr>
								<td><%# DataBinder.Eval(Container.DataItem, "ip_address") %></td>
								<td><%# DataBinder.Eval(Container.DataItem, "ban_start_date") %></td>
								<td><%# DataBinder.Eval(Container.DataItem, "ban_end_date") %></td>
								<td>
									<asp:LinkButton id="lbUnBanIP" runat="server" commandname="cmdUnBan" causesvalidation="false" OnClientClick="return confirm('Are you sure you want to un-ban this IP address?');">Un-Ban</asp:LinkButton>
									<input type="hidden" runat="server" id="hidIPAddress" value='<%# DataBinder.Eval(Container.DataItem, "ip_address") %>' >
								</td>			
							</tr>
					</itemtemplate>
					
					<alternatingitemtemplate>
							<tr style="background-color:#DCDCDC;">
								<td><%# DataBinder.Eval(Container.DataItem, "ip_address") %></td>
								<td><%# DataBinder.Eval(Container.DataItem, "ban_start_date") %></td>
								<td><%# DataBinder.Eval(Container.DataItem, "ban_end_date") %></td>
								<td>
									<asp:LinkButton id="lbUnBanIP" runat="server" commandname="cmdUnBan" causesvalidation="false" OnClientClick="return confirm('Are you sure you want to un-ban this IP address?');">Un-Ban</asp:LinkButton>
									<input type="hidden" runat="server" id="hidIPAddress" value='<%# DataBinder.Eval(Container.DataItem, "ip_address") %>' >
								</td>			
							</tr>
					</alternatingitemtemplate>
					
					<footertemplate>
						</table></div>
					</footertemplate>
				
				</asp:repeater>
			
				<div style="width:95%;text-align:right;margin:10px 6px 0 0;"><kaneva:pager runat="server" isajaxmode="True" onpagechanged="pgBannedIPs_PageChange" id="pgBannedIPs" maxpagestodisplay="5" shownextprevlabels="true" /></div>
				
			</ajax:ajaxpanel> 
         
			<asp:Label id="lblNoBannedIPs" visible="false" runat="server">
				<span style="font-size:16px;color:Red;font-weight:bold;margin:30px 0;width:95%;text-align:center;">No Banned IP Addresses found.</span>
			</asp:Label>

			<br /><br />		 		 			
		</td>
	</tr>
</table>
