<%@ Page language="c#" ValidateRequest="False" Codebehind="userNotes.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.CSR.userNotes" %>
<%@ Register TagPrefix="Kaneva" TagName="CSRNav" Src="csrNav.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="AdminMenu" Src="../../usercontrols/AdministrativeMenu.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../../usercontrols/Pager.ascx" %>
<%@ Register TagPrefix="CE" Namespace="CuteEditor" Assembly="CuteEditor" %>

<Kaneva:AdminMenu ID="adminmenu" runat="server" />
<asp:ValidationSummary ShowMessageBox="False" ShowSummary="True" Class="formError" id="valSum" DisplayMode="BulletList" runat="server" HeaderText="Please correct the following errors:"/>

<table runat="server" id="tblNoCSR" cellpadding="0" cellspacing="0" border="0" width="560">
	<tr><td ALIGN="CENTER">
		<BR><BR><BR>
		<span class="subHead">You must be a CSR to access this section.</span>
	</td></tr>
</table>

<asp:panel runat="server" ID="pnlCSR">

<Kaneva:CSRNav runat="server" id="csrNavigation" SubTab="us"/>
<br>


 <table id="tblUserFame" border="0" cellpadding="0" cellspacing="0" width="1000" style="background-color:#ffffff">
	<tr>
		<td style="height:20px"></td>
    </tr>
    <tr>
         <td align="center">

<br>
<table cellpadding="0" cellspacing="0" border="0" width="1000">
	<tr>
		<td align="right" class="belowFilter2" valign="middle" style="line-height:15px;">
			<asp:Label runat="server" id="lblSearch" CssClass="dateStamp"/><br><Kaneva:Pager runat="server" id="pgTop"/><br>
		</td>						
	</tr>
</table>
<center>

<asp:DataGrid runat="server" EnableViewState="False" ShowFooter="False" Width="1000" id="dgrdNotes" cellpadding="0" cellspacing="0" border="0" AutoGenerateColumns="False" AllowSorting="True" CssClass="FullBorders">  
	<HeaderStyle CssClass="lineItemColHead"/>
	<ItemStyle CssClass="lineItemEven"/>
	<AlternatingItemStyle CssClass="lineItemOdd"/>
	<Columns>
		<asp:TemplateColumn HeaderText="created date" SortExpression="created_datetime" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="Top">
			<ItemTemplate>
				<%# FormatDateTime (DataBinder.Eval(Container.DataItem, "created_datetime")) %>&nbsp;
			</ItemTemplate>
		</asp:TemplateColumn>
			<asp:TemplateColumn HeaderText="posted by" SortExpression="username" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="Top">
				<ItemTemplate>
					<%# DataBinder.Eval(Container.DataItem, "username") %>&nbsp;
				</ItemTemplate>
			</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="note" SortExpression="note" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="middle" ItemStyle-Width="80%">
			<ItemTemplate> 
				<%# DataBinder.Eval(Container.DataItem, "note") %>
			</ItemTemplate>
		</asp:TemplateColumn>		
	</Columns>
</asp:datagrid>

<br/><br/><br/>

<table cellpadding="0" cellspacing="0" border="0" width="750">
    <tr>
            <td><img runat="server" src="~/images/spacer.gif" width="20" height="1"/></td>
            <td colspan="3" class=""><b id="rounded2"><i>&nbsp;&nbsp;Add a Note<span><img border="0" runat="server" src="~/images/spacer.gif" width="710" height="1"/></span></i></b></td><td><img runat="server" src="~/images/spacer.gif" width="20" height="1"/></td>
    </tr>
     <tr>
            <td><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
            <td align="center" colspan="3" width="705" style="border-left: 1px solid #ededed;border-right: 1px solid #ededed;"><br>
                <CE:Editor id="txtNote" BackColor="#ededed" runat="server" width="100%" Height="200px" ShowHtmlMode="False" ConfigurationPath="~/CuteSoft_Client/CuteEditor/Configuration/AutoConfigure/asset.config" AutoConfigure="None" ></CE:Editor>
                <asp:RequiredFieldValidator ID="rfTeaser" ControlToValidate="txtNote" Text="*" ErrorMessage="Note is a required field." Display="Dynamic" runat="server"/>
                <br><br>
            </td>
            <td><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
    </tr>
    <tr>
            <td><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
            <td colspan="3" align="right" bgcolor="#ededed"><br>
                    <asp:button id="btnCancel" runat="Server" CausesValidation="False" onClick="btnCancel_Click" class="Filter2" Text="  cancel  "/>&nbsp;&nbsp;<asp:button id="btnUpdate" runat="Server" onClick="btnUpdate_Click" CausesValidation="False" class="Filter2" Text="  add note  "/>&nbsp;&nbsp;&nbsp;&nbsp;<br>
            </td>
            <td><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
    </tr>
    
    
</table>

</asp:panel>
			<br />
		</td>
	</tr>
</table>