<%@ Page language="c#" Codebehind="itemTransfer.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.CSR.itemTransfer" %>
<%@ Register TagPrefix="Kaneva" TagName="CSRNav" Src="csrNav.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="AdminMenu" Src="../../usercontrols/AdministrativeMenu.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../../usercontrols/Pager.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="StoreFilter" Src="../../usercontrols/StoreFilter.ascx" %>

<Kaneva:AdminMenu ID="adminmenu" runat="server" />
<asp:ValidationSummary ShowMessageBox="False" ShowSummary="True" Class="formError" id="valSum" DisplayMode="BulletList" runat="server" HeaderText="Please correct the following errors:"/>

<table runat="server" id="tblNoCSR" cellpadding="0" cellspacing="0" border="0" width="560">
	<tr><td ALIGN="CENTER">
		<BR><BR><BR>
		<span class="subHead">You must be a CSR to access this section.</span>
	</td></tr>
</table>

<asp:panel runat="server" ID="pnlCSR">

<Kaneva:CSRNav runat="server" id="csrNavigation" SubTab="it"/>
<br>

<BR>
<center>

<table cellpadding="0" cellspacing="0" border="0" width="905">
<tr>
	<td><img runat="server" src="~/images/spacer.gif" width="20" height="1"/></td>
	<td><b id="rounded2"><i><span><img border="0" runat="server" src="~/images/spacer.gif" width="865" height="8"/></span></i></b></td>
	<td><img runat="server" src="~/images/spacer.gif" width="20" height="1"/></td>
</tr>
<tr>
	<td><img runat="server" src="~/images/spacer.gif" width="20" height="1"/></td>
	<td class="bodyText" align="left" bgcolor="#ededed" style="padding-left:10px;">
	<br>
		<span class="assetTitle">Are you sure you want to make the following member the owner?</span><br><br>
		<ul>
		<span class="adminLinks"><b>
		Transfer item '<asp:label id="lblAssetName" runat="server"/>' to user </b></span><asp:TextBox ID="txtUsername" class="Filter2" style="width:300px" MaxLength="100" runat="server"/>&nbsp;<input class="Filter2" type="button" value="  select  " onclick="<% =GetPickerLink ()%>"/>
		<!--<span class="adminLinks"><b>Transfering ownership will result in the following:</b></span><br><br>
		1) Loss of any future royalties earned by this community<br>
		2) Loss of control of community<br>
		3) This action can not be reversed except by the new community owner<br>
		4) You would become a community member with only membership rights<br>-->
		</ul>
		<br>
	</td>
	<td><img runat="server" src="~/images/spacer.gif" width="20" height="1"/></td>
</tr>

<tr>
	<td><img runat="server" src="~/images/spacer.gif" width="20" height="30"/></td>
	<td class="bodyText" align="right" bgcolor="#ededed">
		<asp:button id="btnCancel" runat="Server" onClick="btnCancel_Click" class="Filter2" CausesValidation="False" Text="       cancel       "/>&nbsp;&nbsp;<asp:button id="btnUpdate" runat="Server" onClick="btnUpdate_Click" class="Filter2" CausesValidation="True" Text="  transfer ownership  "/>&nbsp;&nbsp;&nbsp;
	</td>
	<td><img runat="server" src="~/images/spacer.gif" width="20" height="30"/></td>
</tr>

<tr>
	<td><img runat="server" src="~/images/spacer.gif" width="20" height="1"/></td>
	<td><b id="rounded2bot"><i><span><img border="0" runat="server" src="~/images/spacer.gif" width="690" height="8"/></span></i></b></td>
	<td><img runat="server" src="~/images/spacer.gif" width="20" height="1"/></td>
</tr>


</table>
</center><br><br>



</asp:panel>


