<%@ Page language="c#" Codebehind="Activity_Sales.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.CSR.Activity_Sales" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../../usercontrols/Pager.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="CSRNav" Src="csrNav.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="activityNav" Src="activityNav.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="AdminMenu" Src="../../usercontrols/AdministrativeMenu.ascx" %>

<link href="../../css/csr.css" rel="stylesheet" type="text/css" />

<center>
<Kaneva:AdminMenu ID="adminmenu" runat="server" />
<Kaneva:CSRNav runat="server" id="csrNavigation" SubTab="us"/>
<br><br>
<Kaneva:activityNav runat="server" id="activityNavigation" SubTab="sales"/>
</center>
<br><br>

<table runat="server" id="tblNoCSR" cellpadding="0" cellspacing="0" border="0" width="560">
	<tr><td ALIGN="CENTER">
		<BR><BR><BR>
		<span class="subHead">You must be a CSR to access this section.</span>
	</td></tr>
</table>

<asp:panel runat="server" ID="pnlCSR">


<table cellpadding="0" cellspacing="0" border="0" width="100%">
		<tr>
			<td valign="middle" class="belowFilter" align="left" width="230">&nbsp;</td>
			<td width="100%" align="right" valign="bottom">
				<asp:Label runat="server" id="lblSearch" CssClass="showingCount"/>&nbsp;&nbsp;<Kaneva:Pager runat="server" id="pgTop"/>&nbsp;&nbsp;&nbsp;
			</td>
		</tr>
</table>

<asp:DataGrid runat="server" EnableViewState="False" ShowFooter="False" Width="100%" id="dgrdRatings" cellpadding="0" cellspacing="0" border="0" AutoGenerateColumns="False" AllowSorting="True" style="border: 1px solid #cccccc; font-family:arial">  
	<HeaderStyle CssClass="lineItemColHeadCSR"/>
	<ItemStyle CssClass="lineItemEvenCSR"/>
	<AlternatingItemStyle CssClass="lineItemOddCSR"/>
	<Columns>				
		<asp:TemplateColumn ItemStyle-CssClass="assetLink" HeaderText="item name" SortExpression="name" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="top" ItemStyle-Width="40%" ItemStyle-Wrap="true">
			<ItemTemplate>
				<a title='<%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "name").ToString ())%>' CssClass="assetLink" href='<%# GetAssetDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "asset_id")))%>'><%# TruncateWithEllipsis (Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "name").ToString ()), 40)%></a>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="# sold" SortExpression="count" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="top" ItemStyle-Width="10%" ItemStyle-Wrap="true">
			<ItemTemplate>
				<%# DataBinder.Eval(Container.DataItem, "count")%>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="royalty" SortExpression="royalty_amount" ItemStyle-Width="20%" ItemStyle-HorizontalAlign="Center">
			<ItemTemplate>
				<span class="money"><%#FormatCurrency (ConvertKPointsToDollars (Convert.ToDouble (DataBinder.Eval(Container.DataItem, "royalty_amount"))))%></span>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="current price" SortExpression="amount" ItemStyle-Width="20%" ItemStyle-HorizontalAlign="Center">
			<ItemTemplate>
				<span class="money"><%# FormatKpoints (DataBinder.Eval (Container.DataItem, "amount"), false) %></span> 
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn ItemStyle-CssClass="adminLinks" HeaderText="since" SortExpression="created_date" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="top" ItemStyle-Width="10%" ItemStyle-Wrap="false">
			<ItemTemplate>
				<%# FormatDateTime (DataBinder.Eval(Container.DataItem, "created_date"))%>
			</ItemTemplate>
		</asp:TemplateColumn>
	</Columns>
</asp:datagrid>

</asp:panel>