<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="diag.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.csr.diag" %>
<%@ Register TagPrefix="Kaneva" TagName="CSRNav" Src="csrNav.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="activityNav" Src="activityNav.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="AdminMenu" Src="../../usercontrols/AdministrativeMenu.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../../usercontrols/Pager.ascx" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>

<link href="../../css/csr.css" rel="stylesheet" type="text/css" />

<center>
<Kaneva:AdminMenu ID="adminmenu" runat="server" />
<Kaneva:CSRNav runat="server" id="csrNavigation" SubTab="us"/>
<br/><br/>
<Kaneva:activityNav runat="server" id="activityNavigation" SubTab="diagnostics"/>
</center>

<table runat="server" id="tblNoCSR" cellpadding="0" cellspacing="0" border="0" width="560">
	<tr><td align="center">
		<br/><br/><br/>
		<span class="subHead">You must be a CSR to access this section.</span>
	</td></tr>
</table>

<br /><br />

<ajax:ajaxpanel id="ajp" runat="server">
<div id="divCSR" runat="server" visible="false">

<table cellSpacing="0" cellPadding="0" width="990" border="0">
	<tbody>
	<tr>
		<td>
			<div style="width:100%;text-align:right;margin:10px 6px 0 0;">
				<asp:Label runat="server" id="lblResultCount" CssClass="showingCount"/>&nbsp;&nbsp;
				<kaneva:pager runat="server" isajaxmode="True" onpagechanged="pgDiag_PageChange" id="pgDiag" 
					maxpagestodisplay="5" shownextprevlabels="true" />&nbsp;&nbsp;&nbsp;
			</div>
	   
			<asp:DataGrid runat="server" EnableViewState="False" ShowFooter="False" Width="1500" id="dgrdDXDiag" 
				cellpadding="2" cellspacing="0" border="0" AutoGenerateColumns="False" AllowSorting="True" 
				style="border: 1px solid #cccccc; font-family:arial">  
				<HeaderStyle CssClass="lineItemColHeadCSR" horizontalalign="left"/>
				<ItemStyle CssClass="lineItemEvenCSR" horizontalalign="left" verticalalign="top"/>
				<AlternatingItemStyle CssClass="lineItemOddCSR" horizontalalign="left" verticalalign="top"/>
				<Columns>					
					
					<asp:TemplateColumn HeaderText="Date" SortExpression="diag_date" ItemStyle-Wrap="true">
						<ItemTemplate>
							<%# DataBinder.Eval (Container.DataItem, "diag_date") %> 
						</ItemTemplate>
					</asp:TemplateColumn>

					<asp:TemplateColumn HeaderText="O/S" SortExpression="operating_system" ItemStyle-Wrap="true">
						<ItemTemplate>
							<%# DataBinder.Eval(Container.DataItem, "operating_system").ToString()%>
						</ItemTemplate>
					</asp:TemplateColumn>

					<asp:TemplateColumn HeaderText="Make" SortExpression="make" ItemStyle-Wrap="true">
						<ItemTemplate>
							<%# DataBinder.Eval(Container.DataItem, "make")%>
						</ItemTemplate>
					</asp:TemplateColumn>
					
					<asp:TemplateColumn HeaderText="Model" SortExpression="model" ItemStyle-Wrap="true">
						<ItemTemplate>
							<%# DataBinder.Eval(Container.DataItem, "model")%>
						</ItemTemplate>
					</asp:TemplateColumn>
					
					<asp:TemplateColumn HeaderText="Bios" SortExpression="bios" ItemStyle-Wrap="true">
						<ItemTemplate>
							<%# DataBinder.Eval(Container.DataItem, "bios")%>
						</ItemTemplate>
					</asp:TemplateColumn>
					
					<asp:TemplateColumn HeaderText="Processor" SortExpression="proc" ItemStyle-Wrap="true">
						<ItemTemplate>
							<%# DataBinder.Eval(Container.DataItem, "proc")%>
						</ItemTemplate>
					</asp:TemplateColumn>
					
					<asp:TemplateColumn HeaderText="Memory" SortExpression="memory" ItemStyle-Wrap="true">
						<ItemTemplate>
							<%# DataBinder.Eval(Container.DataItem, "memory")%>
						</ItemTemplate>
					</asp:TemplateColumn>
					
					<asp:TemplateColumn HeaderText="DirectX Ver" SortExpression="directx_version" ItemStyle-Wrap="true">
						<ItemTemplate>
							<%# DataBinder.Eval (Container.DataItem, "directx_version")%>
						</ItemTemplate>
					</asp:TemplateColumn>
					
					<asp:TemplateColumn HeaderText="Card Name" SortExpression="card_name" ItemStyle-Wrap="true">
						<ItemTemplate>
							<%# DataBinder.Eval (Container.DataItem, "card_name")%>
						</ItemTemplate>
					</asp:TemplateColumn>
					
					<asp:TemplateColumn HeaderText="Card Make" SortExpression="card_make" ItemStyle-Wrap="true">
						<ItemTemplate>
							<%# DataBinder.Eval (Container.DataItem, "card_make")%>
						</ItemTemplate>
					</asp:TemplateColumn>
					
					<asp:TemplateColumn HeaderText="Card Chip Type" SortExpression="card_chip_type" ItemStyle-Wrap="true">
						<ItemTemplate>
							<%# DataBinder.Eval (Container.DataItem, "card_chip_type")%>
						</ItemTemplate>
					</asp:TemplateColumn>
					
					<asp:TemplateColumn HeaderText="DAC Type" SortExpression="dac_type" ItemStyle-Wrap="true">
						<ItemTemplate>
							<%# DataBinder.Eval (Container.DataItem, "dac_type")%>
						</ItemTemplate>
					</asp:TemplateColumn>
					
					<asp:TemplateColumn HeaderText="Card Memory" SortExpression="card_memory" ItemStyle-Wrap="true">
						<ItemTemplate>
							<%# DataBinder.Eval (Container.DataItem, "card_memory")%>
						</ItemTemplate>
					</asp:TemplateColumn>
					
					<asp:TemplateColumn HeaderText="Display Mode" SortExpression="display_mode" ItemStyle-Wrap="true">
						<ItemTemplate>
							<%# DataBinder.Eval (Container.DataItem, "display_mode")%>
						</ItemTemplate>
					</asp:TemplateColumn>
					
					<asp:TemplateColumn HeaderText="Download Size" SortExpression="downloaded_size" ItemStyle-Wrap="true">
						<ItemTemplate>
							<%# DataBinder.Eval (Container.DataItem, "downloaded_size")%>
						</ItemTemplate>
					</asp:TemplateColumn>
					
					<asp:TemplateColumn HeaderText="Elapsed Time" SortExpression="elapsed_time" ItemStyle-Wrap="true">
						<ItemTemplate>
							<%# DataBinder.Eval (Container.DataItem, "elapsed_time")%>
						</ItemTemplate>
					</asp:TemplateColumn>
					
					<asp:TemplateColumn HeaderText="DL Complete" SortExpression="download_completed" ItemStyle-Wrap="true">
						<ItemTemplate>
							<%# DataBinder.Eval (Container.DataItem, "download_completed")%>
						</ItemTemplate>
					</asp:TemplateColumn>
					
					<asp:TemplateColumn HeaderText="Driver Ver" SortExpression="driver_version" ItemStyle-Wrap="true">
						<ItemTemplate>
							<%# DataBinder.Eval (Container.DataItem, "driver_version")%>
						</ItemTemplate>
					</asp:TemplateColumn>
					
					<asp:TemplateColumn HeaderText="Driver Date/Size" SortExpression="driver_date_size" ItemStyle-Wrap="true">
						<ItemTemplate>
							<%# DataBinder.Eval (Container.DataItem, "driver_date_size")%>
						</ItemTemplate>
					</asp:TemplateColumn>
					
				</Columns>
			</asp:datagrid>
		
			<br /><br />
			
		</td>
	</tr>
	</tbody>
</table>

</div>
</ajax:ajaxpanel>