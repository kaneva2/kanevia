<%@ Page language="c#" Codebehind="userSearch.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.CSR.userSearch" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../../usercontrols/Pager.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="CSRNav" Src="csrNav.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="MembersFilter" Src="../../usercontrols/MembersFilter.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="AdminMenu" Src="../../usercontrols/AdministrativeMenu.ascx" %>

<Kaneva:AdminMenu ID="adminmenu" runat="server" />
<asp:ValidationSummary ShowMessageBox="False" ShowSummary="True" Class="formError" id="valSum" DisplayMode="BulletList" runat="server" HeaderText="Please correct the following errors:"/>

<table runat="server" id="tblNoCSR" cellpadding="0" cellspacing="0" border="0" width="560">
	<tr><td ALIGN="CENTER">
		<BR><BR><BR>
		<span class="subHead">You must be a CSR to access this section.</span>
	</td></tr>
</table>

<asp:panel runat="server" ID="pnlCSR">

<Kaneva:CSRNav runat="server" id="csrNavigation" SubTab="us"/>
<br>

 <table id="tblUserFame" border="0" cellpadding="0" cellspacing="0" width="1000" style="background-color:#ffffff">
	<tr>
		<td style="height:20px"></td>
    </tr>
    <tr>
         <td align="center">

 <table cellpadding="0" cellspacing="0" border="0" width="750">
    <tr>
            <td><img runat="server" src="~/images/spacer.gif" width="20" height="1"/></td>
            <td colspan="3"><b id="rounded2"><i><span><img border="0" runat="server" src="~/images/spacer.gif" width="710" height="8"/></span></i> </b></td>
            <td><img runat="server" src="~/images/spacer.gif" width="20" height="1"/></td>
    </tr>
    <tr>
            <td><img runat="server" src="~/images/spacer.gif" width="20" height="20" ID="Img1"/></td>
            <td colspan="3" width="650" style="background-color: #ededed;" align="left">&nbsp;&nbsp;&nbsp;<span class="assetLink">Search Users</span>&nbsp;<span style="color:orange;font-family:arial; font-size: 10px;">(Use single words only, search will return all matches that start with the word)</span></td>
            <td><img runat="server" src="~/images/spacer.gif" width="20" height="20" ID="Img5"/></td>
    </tr>
    <tr>
            <td><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
            <td align="right" class="bodyText" style="border-left: 1px solid #ededed;">&nbsp;<b>username:</b></td>
            <td><img runat="server" src="~/images/spacer.gif" width="5" ID="Img7"/></td>
            <td align="left" width="705" class="bodyText" style="border-right: 1px solid #ededed;">
                 <asp:TextBox ID="txtUsername" class="Filter2" style="width:300px" MaxLength="31" runat="server"/>
            </td>
            <td width="10"><img runat="server" src="~/images/spacer.gif" width="20" height="18"/></td>
    </tr>
    <tr>
            <td><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
            <td align="right" class="bodyText" bgcolor="#ededed" style="border-left: 1px solid #ededed;">&nbsp;<b>email:</b></td>
            <td bgcolor="#ededed"><img runat="server" src="~/images/spacer.gif" width="5"/></td>
            <td align="left" width="705" class="bodyText" bgcolor="#ededed" style="border-right: 1px solid #ededed;">
                    <asp:TextBox ID="txtEmail" class="Filter2" style="width:300px" MaxLength="31" runat="server"/>
            </td>
            <td width="10"><img runat="server" src="~/images/spacer.gif" width="20" height="18"/></td>
    </tr>
    <tr>
            <td><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
            <td align="right" class="bodyText" style="border-left: 1px solid #ededed;">&nbsp;<b>first name:</b></td>
            <td><img runat="server" src="~/images/spacer.gif" width="5"/></td>
            <td align="left" width="705" class="bodyText" style="border-right: 1px solid #ededed;">
                    <asp:TextBox ID="txtFirstName" class="Filter2" style="width:300px" MaxLength="31" runat="server"/>
            </td>
            <td width="10"><img runat="server" src="~/images/spacer.gif" width="20" height="18"/></td>
    </tr>
    <tr>
            <td><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
            <td align="right" bgcolor="#ededed" class="bodyText" style="border-left: 1px solid #ededed;"><b>last name:</b></td>
            <td bgcolor="#ededed"><img runat="server" src="~/images/spacer.gif" width="5"/></td>
            <td align="left" width="705" bgcolor="#ededed" class="bodyText" style="border-right: 1px solid #ededed;">
				<asp:TextBox ID="txtLastName" class="Filter2" style="width:300px" MaxLength="31" runat="server"/>  
				
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<asp:button id="btnSearch" align="right" runat="Server" class="Filter2" Text="    Search    " onClick="btnSearch_Click" CausesValidation="False"/>          
			</td>
            <td width="10"><img runat="server" src="~/images/spacer.gif" width="20" height="18"/></td>
    </tr>  
     <tr>
            <td><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
            <td align="right" class="bodyText" style="border-left: 1px solid #ededed;">&nbsp;<b>status:</b></td>
            <td><img runat="server" src="~/images/spacer.gif" width="5"/></td>
            <td align="left" width="705" class="bodyText" style="border-right: 1px solid #ededed;">
                    <asp:DropDownList class="filter2" id="drpStatus" runat="Server" style="width:200px"/>
            </td>
            <td width="10"><img runat="server" src="~/images/spacer.gif" width="20" height="18"/></td>
    </tr>
    <tr>
            <td><img runat="server" src="~/images/spacer.gif" width="20" height="1"/></td>
            <td colspan="3">
                    <b id="rounded2bot">
                    <i><span><img border="0" runat="server" src="~/images/spacer.gif" width="710" height="8"/></span></i> 
                    </b>
            </td>
            <td><img runat="server" src="~/images/spacer.gif" width="20" height="1"/></td>
    </tr>
</table>
<br>
<table cellpadding="0" cellspacing="0" border="0" width="1000">
	<tr>
		<td align="right" class="belowFilter2" valign="middle" style="line-height:15px;">
			<asp:Label runat="server" id="lblSearch" CssClass="dateStamp"/><br><Kaneva:Pager runat="server" id="pgTop"/><br><Kaneva:MembersFilter runat="server" id="filMembers" RolesEnabled="false" StatusEnabled="false" InitialItemsPerPage="30"/>
		</td>						
	</tr>
</table>
<center>

<asp:DataGrid runat="server" EnableViewState="False" ShowFooter="False" Width="1000" id="dgrdMembers" cellpadding="0" cellspacing="0" border="0" AutoGenerateColumns="False" AllowSorting="True" CssClass="FullBorders">  
	<HeaderStyle CssClass="lineItemColHead"/>
	<ItemStyle CssClass="lineItemEven"/>
	<AlternatingItemStyle CssClass="lineItemOdd"/>
	<Columns>
<asp:TemplateColumn HeaderText="info" SortExpression="" ItemStyle-Width="8%" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="Middle">
			<ItemTemplate>
				<a href=<%# GetActivityUrl (Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "user_id")))%> class="dateStamp">Activity</a><br>
				<a href=<%# GetNotesUrl (Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "user_id")))%> class="dateStamp">Notes(<%# DataBinder.Eval(Container.DataItem, "note_count") %>)</a></ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="members" SortExpression="username" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="middle" ItemStyle-Width="30%">
			<ItemTemplate>
				<a href=<%# GetDetailsUrl (Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "user_id")))%> style="COLOR: #3258ba; font-size: 12px;"><%# DataBinder.Eval(Container.DataItem, "username") %></a>&nbsp;&nbsp;
				<br/><B>(<%# DataBinder.Eval(Container.DataItem, "first_name") %>&nbsp;<%# DataBinder.Eval(Container.DataItem, "last_name") %>)</B>
				<br/><%# DataBinder.Eval(Container.DataItem, "email") %>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="# logins" SortExpression="number_of_logins" ItemStyle-Width="5%" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="Middle">
			<ItemTemplate>
				<span class="money"><%# DataBinder.Eval(Container.DataItem, "number_of_logins") %></span>&nbsp;
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="birth date" SortExpression="birth_date" ItemStyle-Width="10%" ItemStyle-ForeColor="#4863a2" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="Middle">
			<ItemTemplate>
				<span class="adminLinks"><%# FormatDate (DataBinder.Eval(Container.DataItem, "birth_date")) %></span>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="gender" SortExpression="gender" ItemStyle-Width="4%" ItemStyle-ForeColor="#4863a2" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="Middle">
			<ItemTemplate>
				<%# DataBinder.Eval(Container.DataItem, "gender") %>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="joined" SortExpression="user_id" ItemStyle-Width="12%" ItemStyle-ForeColor="#4863a2" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="Middle">
			<ItemTemplate>
				<span class="adminLinks"><%# FormatDateTime (DataBinder.Eval(Container.DataItem, "signup_date")) %></span>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="last active" SortExpression="last_login" ItemStyle-Width="12%" HeaderStyle-Wrap="False" ItemStyle-ForeColor="#4863a2" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="Middle">
			<ItemTemplate>
				<span class="adminLinks"><%# FormatDateTime (DataBinder.Eval(Container.DataItem, "last_login")) %></span>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="status" SortExpression="user_status_name" ItemStyle-Width="15%" HeaderStyle-Wrap="False" ItemStyle-ForeColor="#4863a2" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="Top">
			<ItemTemplate>
				<%# DataBinder.Eval(Container.DataItem, "user_status_name") %>				
				<BR/><asp:hyperlink id="hlBan" runat="server" visible='<%# Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "status_id")).Equals (1) || Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "status_id")).Equals (2)  %>' NavigateURL='<%# GetSuspensionUrl (Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "user_id")))%>' ToolTip="Ban" Text="Ban"/>
				<asp:hyperlink id="hlRemoveBan" runat="server" visible='<%# !Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "status_id")).Equals (1) && !Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "status_id")).Equals (2)%>' NavigateURL='<%# GetSuspensionUrl (Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "user_id")))%>' ToolTip="Remove Ban" Text="Remove Ban"/>
				
			</ItemTemplate>
		</asp:TemplateColumn>
		
	</Columns>
</asp:datagrid>

<br><br>
</center>

</asp:panel>


		</td>
	</tr>
</table>