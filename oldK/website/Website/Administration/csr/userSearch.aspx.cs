///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva;
using log4net;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.CSR
{
	/// <summary>
	/// Summary description for userSearch.
	/// </summary>
	public class userSearch : NoBorderPage
	{
		protected userSearch () 
		{
			Title = "CSR - User Search";
		}

		private void Page_Load (object sender, System.EventArgs e)
		{
			// Make sure they are a CSR
			pnlCSR.Visible = (IsCSR ());
			tblNoCSR.Visible = (!IsCSR ());
			
			if (!IsPostBack)
			{
				drpStatus.DataTextField = "name";
				drpStatus.DataValueField = "status_id";
				drpStatus.DataSource = UsersUtility.GetUserStatus ();
				drpStatus.DataBind ();
				drpStatus.Items.Insert (0, new ListItem ("All Status", "0"));
				drpStatus.SelectedValue = "0";

				// User base is getting large so having this bind on load 
                // can cause a long wait.
                // BindData (1, "");
			}

		}

		/// <summary>
		/// Pager change event
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pg_PageChange (object sender, PageChangeEventArgs e)
		{
			BindData (e.PageNumber, filMembers.CurrentFilter);
		}

		/// <summary>
		///  Bind the data
		/// </summary>
		/// <param name="pageNumber"></param>
		private void BindData (int pageNumber, string filter)
		{
			// Set the sortable columns
			SetHeaderSortText (dgrdMembers);
			string orderby = CurrentSort + " " + CurrentSortOrder;

			// Log the CSR activity
            SiteMgmtFacade siteMgmtFacade = new SiteMgmtFacade();
            siteMgmtFacade.InsertCSRLog(GetUserId(), "CSR - Searching users. Username = " + Server.HtmlEncode(txtUsername.Text) + ", Email = " + Server.HtmlEncode(txtEmail.Text) + ", First Name = " + Server.HtmlEncode(txtFirstName.Text) + ", Last Name = " + Server.HtmlEncode(txtLastName.Text), 0, 0);

			PagedDataTable pds = UsersUtility.GetUsers (Server.HtmlEncode (txtUsername.Text), Server.HtmlEncode (txtEmail.Text), Server.HtmlEncode (txtFirstName.Text), Server.HtmlEncode (txtLastName.Text), Convert.ToInt32 (drpStatus.SelectedValue), filter, orderby, pageNumber, filMembers.ItemsPerPages);
			pgTop.NumberOfPages = Math.Ceiling ((double) pds.TotalCount / filMembers.ItemsPerPages).ToString ();
			pgTop.DrawControl ();

			dgrdMembers.DataSource = pds;
			dgrdMembers.DataBind ();

			// The results
			lblSearch.Text = GetResultsText (pds.TotalCount, pageNumber, filMembers.ItemsPerPages, pds.Rows.Count);
		}

		/// <summary>
		/// filMembers_FilterChanged
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void filMembers_FilterChanged (object sender, FilterChangedEventArgs e)
		{
			pgTop.CurrentPageNumber = 1;
			BindData (1, e.Filter);
		}

		/// <summary>
		/// They clicked to sort a column
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected override void btnSort_Click (object sender, System.EventArgs e)
		{
			base.btnSort_Click (sender, e);
			BindData (pgTop.CurrentPageNumber, filMembers.CurrentFilter);
		}

		/// <summary>
		/// DEFAULT_SORT
		/// </summary>
		/// <returns></returns>
		protected override string DEFAULT_SORT
		{
			get
			{
				return "user_id";
			}
		}

		/// <summary>
		/// DEFAULT_SORT
		/// </summary>
		/// <returns></returns>
		protected override string DEFAULT_SORT_ORDER
		{
			get
			{
				return "desc";
			}
		}


		/// <summary>
		/// Search Event Handler
		/// </summary>
		protected void btnSearch_Click (object sender, EventArgs e) 
		{
			BindData (pgTop.CurrentPageNumber, filMembers.CurrentFilter);
		}

		protected string GetActivityUrl (int userId)
		{
            return ResolveUrl("~/Administration/csr/Activity_Posts.aspx?userId=" + userId);
		}

		protected string GetDetailsUrl (int userId)
		{
            return ResolveUrl("~/Administration/csr/userDetails.aspx?userId=" + userId);
		}

		protected string GetNotesUrl (int userId)
		{
            return ResolveUrl("~/Administration/csr/userNotes.aspx?userId=" + userId);
		}


        protected string GetSuspensionUrl(int userId)
        {
            return ResolveUrl("~/Administration/csr/Activity_Suspensions.aspx?userId=" + userId);
        }


		protected Kaneva.MembersFilter filMembers;
		protected Kaneva.Pager pgTop;

		protected DataGrid dgrdMembers;

		protected Label lblSearch;
		protected HtmlTable tblNoCSR;
		protected Panel pnlCSR;

		protected HtmlInputHidden txtUserId;
		protected Button btnRemoveUserBan, btnBanUser;

		protected TextBox txtUsername, txtEmail, txtFirstName, txtLastName;
		protected DropDownList drpStatus;

		// Logger
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
			pgTop.PageChanged +=new PageChangeEventHandler (pg_PageChange);
			filMembers.FilterChanged +=new FilterChangedEventHandler (filMembers_FilterChanged);
		}
		#endregion
	}
}
