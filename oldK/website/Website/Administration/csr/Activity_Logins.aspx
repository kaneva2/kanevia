<%@ Page language="c#" Codebehind="Activity_Logins.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.CSR.Activity_Logins" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../../usercontrols/Pager.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="CSRNav" Src="csrNav.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="activityNav" Src="activityNav.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="AdminMenu" Src="../../usercontrols/AdministrativeMenu.ascx" %>

<link href="../../css/csr.css" rel="stylesheet" type="text/css" />

<center>
<Kaneva:AdminMenu ID="adminmenu" runat="server" />
<Kaneva:CSRNav runat="server" id="csrNavigation" SubTab="us"/>
<br><br>
<Kaneva:activityNav runat="server" id="activityNavigation" SubTab="logins"/>
</center>
<br><br>

<table runat="server" id="tblNoCSR" cellpadding="0" cellspacing="0" border="0" width="560">
	<tr><td ALIGN="CENTER">
		<BR><BR><BR>
		<span class="subHead">You must be a CSR to access this section.</span>
	</td></tr>
</table>

<asp:panel runat="server" ID="pnlCSR">


<table cellpadding="0" cellspacing="0" border="0" width="90%">
		<tr>
			<td valign="middle" class="belowFilter" align="left" width="230">&nbsp;</td>
			<td width="100%" align="right" valign="bottom">
				<asp:Label runat="server" id="lblSearch" CssClass="showingCount"/>&nbsp;&nbsp;<Kaneva:Pager runat="server" id="pgTop"/>&nbsp;&nbsp;&nbsp;
			</td>
		</tr>
</table>

<asp:DataGrid runat="server" EnableViewState="False" ShowFooter="False" Width="90%" id="dgrdLogins" cellpadding="0" cellspacing="0" border="0" AutoGenerateColumns="False" AllowSorting="True" style="border: 1px solid #cccccc; font-family:arial">  
	<HeaderStyle CssClass="lineItemColHeadCSR"/>
	<ItemStyle CssClass="lineItemEvenCSR"/>
	<AlternatingItemStyle CssClass="lineItemOddCSR"/>
	<Columns>					
		
		<asp:TemplateColumn HeaderText="Date" SortExpression="created_date" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="top"  ItemStyle-Wrap="true">
			<ItemTemplate>
				<%# DataBinder.Eval (Container.DataItem, "created_date") %> 
			</ItemTemplate>
		</asp:TemplateColumn>

        <asp:TemplateColumn HeaderText="Action" SortExpression="action_type" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="top" ItemStyle-Wrap="true">
			<ItemTemplate>
				<%# DataBinder.Eval(Container.DataItem, "action_type").ToString()%>
			</ItemTemplate>
		</asp:TemplateColumn>

		<asp:TemplateColumn HeaderText="Time Played" SortExpression="time_played" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="top" ItemStyle-Wrap="true">
			<ItemTemplate>
				<%# DataBinder.Eval(Container.DataItem, "time_played")%>
			</ItemTemplate>
		</asp:TemplateColumn>
			
		
	</Columns>
</asp:datagrid>

</asp:panel>