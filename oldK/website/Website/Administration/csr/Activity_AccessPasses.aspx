<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Activity_AccessPasses.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.csr.Activity_AccessPasses" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../../usercontrols/Pager.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="CSRNav" Src="csrNav.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="activityNav" Src="activityNav.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="AdminMenu" Src="../../usercontrols/AdministrativeMenu.ascx" %>

<link href="../../css/csr.css" rel="stylesheet" type="text/css" />

<center>
<Kaneva:AdminMenu ID="adminmenu" runat="server" />
<Kaneva:CSRNav runat="server" id="csrNavigation" SubTab="us"/>
<br><br>
<Kaneva:activityNav runat="server" id="activityNavigation" SubTab="accesspasses"/>
</center>

<table runat="server" id="tblNoCSR" cellpadding="0" cellspacing="0" border="0" width="560">
	<tr><td ALIGN="CENTER">
		<BR><BR><BR>
		<span class="subHead">You must be a CSR to access this section.</span>
	</td></tr>
</table>

<asp:panel runat="server" ID="pnlCSR"><BR /><BR /><TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY><TR><TD><asp:Button id="btn_Add" onclick="btn_Add_Click" Text="New Access Pass" runat="server"></asp:Button> <asp:Label id="lbl_Messages" runat="server">Label</asp:Label> </TD></TR><TR><TD><asp:DataGrid style="BORDER-RIGHT: #cccccc 1px solid; BORDER-TOP: #cccccc 1px solid; BORDER-LEFT: #cccccc 1px solid; BORDER-BOTTOM: #cccccc 1px solid; FONT-FAMILY: arial" id="dgrdPasses" runat="server" OnSortCommand="dgrdPasses_SortCommand" OnEditCommand="dgrdPasses_EditCommand" OnCancelCommand="dgrdPasses_CancelCommand" PageSize="20" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" border="0" cellpadding="2" Width="100%">
<PagerStyle Mode="NumericPages"></PagerStyle>

<AlternatingItemStyle CssClass="lineItemOddCSR"></AlternatingItemStyle>

<ItemStyle CssClass="lineItemEvenCSR"></ItemStyle>

<HeaderStyle CssClass="lineItemColHeadCSR"></HeaderStyle>
<Columns>
<asp:BoundColumn DataField="pass_group_id" HeaderText="pass_group_id" SortExpression="pass_group_id" Visible="False"></asp:BoundColumn>
<asp:BoundColumn DataField="name" HeaderText="Access Pass Name" SortExpression="name" ReadOnly="True">
<HeaderStyle HorizontalAlign="Left" Font-Bold="True"></HeaderStyle>
</asp:BoundColumn>
<asp:BoundColumn DataField="created_date" HeaderText="Created Date" SortExpression="created_date" ReadOnly="True">
<HeaderStyle HorizontalAlign="Left" Font-Bold="True"></HeaderStyle>
</asp:BoundColumn>
<asp:TemplateColumn><ItemTemplate>
<asp:hyperlink id="hlEdit" runat="server" CssClass="adminLinks" NavigateURL='<%#GetAccessEditLink (Convert.ToInt32 ( DataBinder.Eval(Container.DataItem, "pass_group_id")))%>' ToolTip="Edit" Visible='True'>edit</asp:hyperlink>
</ItemTemplate>
</asp:TemplateColumn>
<asp:TemplateColumn><ItemTemplate>
<asp:hyperlink id="hlDelete" runat="server" CssClass="adminLinks" NavigateURL='<%#DeleteAccessLink (Convert.ToInt32 ( DataBinder.Eval(Container.DataItem, "pass_group_id")))%>' ToolTip="Delete" Visible='True'>delete</asp:hyperlink>
</ItemTemplate>
</asp:TemplateColumn>
</Columns>
</asp:DataGrid> </TD></TR></TBODY></TABLE></asp:panel>