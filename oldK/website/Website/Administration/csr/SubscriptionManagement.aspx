<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SubscriptionManagement.aspx.cs" ValidateRequest="false" Inherits="KlausEnt.KEP.Kaneva.csr.SubscriptionManagement" %>
<%@ Register TagPrefix="Kaneva" TagName="CSRNav" Src="csrNav.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="AdminMenu" Src="../../usercontrols/AdministrativeMenu.ascx" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>

<script type="text/javascript" src="../../jscript/yahoo/yahoo-min.js"></script>  
<script type="text/javascript" src="../../jscript/yahoo/event-min.js" ></script>   
<script type="text/javascript" src="../../jscript/yahoo/dom-min.js" ></script>   
<script type="text/javascript" src="../../jscript/yahoo/calendar-min.js"></script>

<script type="text/javascript" language="Javascript" src="../../jscript/PageLayout/colorpicker.js"></script>
<script type="text/javascript" language="javascript" src="../../jscript/prototype.js"></script>

<link href="../../css/new.css" rel="stylesheet" type="text/css" />
<link type="text/css" rel="stylesheet" href="../../css/yahoo/calendar.css">   
<link href="../../css/editWidgets.css" rel="stylesheet" type="text/css" />


<style >
	#cal4Container { display:none; position:absolute; z-index:100;}
</style>

 
<script type="text/javascript"><!--

YAHOO.namespace("example.calendar");

	function handleIntroOfferDate(type,args,obj) {
		var dates = args[0]; 
		var date = dates[0];
		var year = date[0], month = date[1], day = date[2];
		
		var txtDate4 = document.getElementById("tbx_IntroOfferDate");
		txtDate4.value = month + "-" + day + "-" + year;
		YAHOO.example.calendar.cal4.hide();
	}

	function initIntroOfferEnd() {
		// Event Calendar
		YAHOO.example.calendar.cal4 = new YAHOO.widget.Calendar ("cal4", "cal4Container", { iframe:true, zIndex:1000, mindate:"1/1/2007", title:"Choose a date:", close:true } );   
		YAHOO.example.calendar.cal4.render();
		
		// Listener to show the Event Calendar when the button is clicked   
		YAHOO.util.Event.addListener("imgIntroOfferEndDate", "click", YAHOO.example.calendar.cal4.show, YAHOO.example.calendar.cal4, true);   
		YAHOO.example.calendar.cal4.selectEvent.subscribe(handleIntroOfferDate, YAHOO.example.calendar.cal4, true);
	}

//--> </script>  

<body>


<Kaneva:AdminMenu ID="adminmenu" runat="server" />
<Kaneva:CSRNav runat="server" id="csrNavigation" SubTab="pm"/>
<br/>

 <table id="tblUserFame" border="0" cellpadding="0" cellspacing="0" width="1000" style="background-color:#ffffff">
    <tr>
        <td align="center">
			<span style="height:30px; font-size:28px; font-weight:bold">Subscription Management</span><br />
		</td>
    </tr>
	<tr>
		<td style="height:20px"></td>
    </tr>
    <tr>
		<td align="center">

			<ajax:ajaxpanel id="ajPromoList" runat="server">

				<div style="width:60%;margin-bottom:20px;">
					<asp:validationsummary cssclass="errBox black" id="valSum" runat="server" showmessagebox="False" showsummary="True"
						displaymode="BulletList" headertext="Please correct the following errors:" forecolor="black"></asp:validationsummary>
					<asp:customvalidator id="cvBlank" runat="server" display="none" enableclientscript="false"></asp:customvalidator>
				</div>
					
				<table id="tbl_AvailableSubscriptions" border="0" cellpadding="0" cellspacing="0" width="95%">
					<tr>
						 <td><asp:Label id="lbl_NoSubscriptions" visible="false" runat="server"><span style="color:Navy; size:24pt; font-weight:bold">No Subscriptions Were Found.</span> </asp:Label></td>
					</tr>
					<tr>
						 <td>
				            <div style="width:900px;text-align:left;margin:0 0 12px 0;">
								<asp:LinkButton id="lbn_NewSubscription" runat="server" causesvalidation="false" onclick="lbn_NewSubscription_Click">Add New Subscription</asp:LinkButton>
							</div>
							<asp:gridview id="dg_Subscriptions" runat="server" onsorting="dg_Subscriptions_Sorting" 
								OnRowCreated="dg_SubscriptionsmyGrid_OnRowCreated" autogeneratecolumns="False" width="980px" 
								OnPageIndexChanging="dg_Subscriptions_PageIndexChanging" OnRowDeleting="dg_Subscriptions_RowDeleting" 
								OnRowEditing="dg_Subscriptions_RowEditing" AllowSorting="True" border="0" cellpadding="4" 
								bordercolor="DarkGray" borderstyle="solid" borderwidth="1px"
								PageSize="20" AllowPaging="True">
								
								<RowStyle BackColor="White"></RowStyle>
								<HeaderStyle BackColor="LightGray" Font-Underline="True" Font-Bold="True" ForeColor="#000000" HorizontalAlign="Left"></HeaderStyle>
								<AlternatingRowStyle BackColor="Gainsboro"></AlternatingRowStyle>
								<FooterStyle BackColor="#FFffff"></FooterStyle>
								<Columns>
									<asp:BoundField HeaderText="Subscription ID" DataField="subscription_id" SortExpression="subscription_id" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
									<asp:BoundField HeaderText="Name" DataField="name" SortExpression="name" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
									<asp:BoundField HeaderText="Price" DataField="price" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
									<asp:BoundField HeaderText="Introductory Price" DataField="introductory_price" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
									<asp:BoundField HeaderText="Introductory End Date" DataField="introductory_end_date" SortExpression="introductory_end_date" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
									<asp:BoundField HeaderText="Grandfather Price" DataField="grandfather_price" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
									<asp:BoundField HeaderText="Term" DataField="term" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
									<asp:BoundField HeaderText="Days Free" DataField="days_free" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
									<asp:BoundField HeaderText="Learn More Path" DataField="learn_more_image_path" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
									<asp:BoundField HeaderText="Upsell Path" DataField="upsell_image_path" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
									<asp:BoundField HeaderText="Learn More Content" DataField="learn_more_content" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
									<asp:BoundField HeaderText="Discount Percent" DataField="discount_percent" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
									<asp:BoundField HeaderText="Keep Subscription Reasons" DataField="keep_subscription_reasons" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
									<asp:BoundField HeaderText="Monthly Allowance" DataField="monthly_allowance" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
									<asp:CommandField CausesValidation="False" ShowEditButton="True"></asp:CommandField>
									<asp:TemplateField>
										<ItemTemplate>
											<asp:LinkButton id="lbn_confirmPromoDelete" Runat="server" causesvalidation="false" OnClientClick="return confirm('Are you sure you want to delete this promotion?');" CommandName="Delete">Delete</asp:LinkButton>
										</ItemTemplate>
									</asp:TemplateField>
				        
								</Columns>
							</asp:gridview><br />
						 </td>
					</tr>
					<tr>
						 <td align="center">
							<div id="divSubscriptionsDetails" runat="server" >
                
								<h2 id="h2OfferTitle" runat="server" style="width:99%;text-align:left;"></h2><br />
								<table id="tbl_OfferDetails" border="0" cellpadding="5px" cellspacing="1" width="100%">
						            <tr>
						                <td align="right" style="width:200px">Subscription Name:</td>
						                <td align="left">
							                <asp:TextBox runat="server" id="tbx_SubscriptionName"></asp:TextBox>
							                <asp:Label runat="server" id="lbl_SubscriptionId" Visible="false" />
							                <asp:RequiredFieldValidator runat="server" id="rfv_SubscriptionName" controltovalidate="tbx_SubscriptionName" text="* Please enter a a value." errormessage="The subscription name is required." display="Dynamic"></asp:RequiredFieldValidator>
						                </td>
						            </tr>
						            <tr>
						                <td align="right" style="width:200px">Price:</td>
						                <td align="left">
							                <asp:TextBox runat="server" id="tbx_Price"></asp:TextBox>
							                <asp:RequiredFieldValidator runat="server" id="rfv_Price" controltovalidate="tbx_Price" text="* Please enter a a value." errormessage="The price is required." display="Dynamic"></asp:RequiredFieldValidator>
						                </td>
						            </tr>
						            <tr>
						                <td align="right" style="width:200px">Introductory Price:</td>
						                <td align="left">
							                <asp:TextBox runat="server" id="tbx_IntroductoryPrice"></asp:TextBox>
							                <asp:RequiredFieldValidator runat="server" id="rfv_IntroductionPrice" controltovalidate="tbx_IntroductoryPrice" text="* Please enter a a value." errormessage="The introductory price is required." display="Dynamic"></asp:RequiredFieldValidator>
						                </td>
						            </tr>
						            <tr>
						                <td align="right" style="width:200px">Introductory End Date:</td>
						                <td align="left">
							                <asp:TextBox runat="server" Enabled="false" id="tbx_IntroOfferDate"></asp:TextBox>
							                <asp:RequiredFieldValidator runat="server" id="rfv_IntroductionEndDate" controltovalidate="tbx_IntroOfferDate" text="* Please enter a a value." errormessage="The introductory end date is required." display="Dynamic"></asp:RequiredFieldValidator>
							                <img id="imgIntroOfferEndDate" name="imgIntroOfferEndDate" onload="initIntroOfferEnd();" src="../../images/blast/cal_16.gif"/> 
                                            <div id="cal4Container"></div>
						                </td>
						            </tr>
						            <tr>
						                <td align="right" style="width:200px">Grandfather Price:</td>
						                <td align="left">
							                <asp:Checkbox runat="server" id="cbx_GrandfatherPrice" Text="Grand father the introductory price?" />
						                </td>
						            </tr>
						            <tr>
						                <td align="right" style="width:200px">Trial Period (Days):</td>
						                <td align="left">
							                <asp:TextBox runat="server" id="tbx_TrialPeriod"></asp:TextBox>
							                <asp:RequiredFieldValidator runat="server" id="rfv_TrialPeriod" controltovalidate="tbx_TrialPeriod" text="* Please enter a a value." errormessage="The trial period (days) is required." display="Dynamic"></asp:RequiredFieldValidator>
						                </td>
						            </tr>
						            <tr>
						                <td align="right" style="width:200px">Subscription Period:</td>
						                <td align="left">
							                <asp:DropDownList runat="server" id="ddp_SubscriptionPeriod"></asp:DropDownList>
						                </td>
						            </tr>
						            <tr>
						                <td align="right" style="width:200px">Discount Amount(%):</td>
						                <td align="left">
							                <asp:TextBox runat="server" id="tbx_DiscountAmount"></asp:TextBox>
						                </td>
						            </tr>
						            <tr>
						                <td align="right" style="width:200px">Monthly Allowance (Credits):</td>
						                <td align="left">
							                <asp:TextBox runat="server" id="tbx_MonthlyAllowance"></asp:TextBox>
						                </td>
						            </tr>
						            <tr>
						                <td align="right" style="width:200px">Learn More Content:</td>
						                <td align="left">
							                <asp:TextBox runat="server" id="tbx_LearnMoreContent" Columns="80" Rows="10" TextMode="MultiLine"></asp:TextBox>
							                <asp:RequiredFieldValidator runat="server" id="rfv_learnMore" controltovalidate="tbx_LearnMoreContent" text="* Please enter a a value." errormessage="The learn more content is required." display="Dynamic"></asp:RequiredFieldValidator>
						                </td>
						            </tr>
						            <tr>
						                <td align="right" style="width:200px">Reasons To Keep:</td>
						                <td align="left">
							                <asp:TextBox runat="server" id="tbx_Reasons2Keep" Columns="40" Rows="6" TextMode="MultiLine"></asp:TextBox>
							                <asp:RequiredFieldValidator runat="server" id="frv_Reasons2Keep" controltovalidate="tbx_Reasons2Keep" text="* Please enter a a value." errormessage="The reasons to keep the subscription content is required." display="Dynamic"></asp:RequiredFieldValidator>
						                </td>
						            </tr>
						            <tr>
						                <td align="right" style="width:200px">Learn More Image:</td>
						                <td align="left">
							                <asp:RequiredFieldValidator runat="server" id="rfv_learnMoreImage" controltovalidate="inp_learnMoreImage" text="* Please enter a a value." errormessage="The learn more image is required." display="Dynamic"></asp:RequiredFieldValidator>
							                <input type="file" name="browseLearnMore" style="display: none;" />
							                <input class="formKanevaText" id="inp_learnMoreImage" style="WIDTH: 400px" type="text" size="43" name="inp_learnMoreImage" runat="server" /> 
							                <input type="button" onclick="browseLearnMore.click();inp_learnMoreImage.value=browseLearnMore.value;" value="Browse..."  />                         
						                </td>
						            </tr>
						            <tr>
						                <td align="right" style="width:200px">Upsell Image:</td>
						                <td align="left">
							                <asp:RequiredFieldValidator runat="server" id="rfv_UpsellImage" controltovalidate="inp_upsellImage" text="* Please enter a a value." errormessage="The upsell image is required." display="Dynamic"></asp:RequiredFieldValidator>
							                <input type="file" name="browseUpsell" style="display: none;" />
							                <input class="formKanevaText" id="inp_upsellImage" style="WIDTH: 400px" type="text" size="43" name="inp_upsellImage" runat="server" /> 
							                <input type="button" onclick="browseUpsell.click();inp_upsellImage.value=browseUpsell.value;" value="Browse..."  />                         
						                </td>
						            </tr>
			        	            <tr>
			        		            <td align="right" style="width:220px">Associated Pass Group(s):</td>
				                        <td align="left">
                                            <asp:GridView runat="server" id="dgd_passGroups" AllowPaging="false" cellpadding="2" border="1" BorderColor="Navy" autogeneratecolumns="False" width="280px">
                                                <RowStyle BackColor="White"></RowStyle>
                                                <AlternatingRowStyle BackColor="Gainsboro"></AlternatingRowStyle>
                                                <FooterStyle BackColor="#FFffff"></FooterStyle>
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemStyle Width="10px" />
                                                        <ItemTemplate>
                                                            <asp:CheckBox runat="server" id="cbx_assocPassGroup" autopostback="true" checked='<%# DataBinder.Eval(Container.DataItem, "cbx_assocPassGroup")%>' oncheckedchanged="cbx_assocPassGroup_CheckedChanged"></asp:CheckBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>                                       
                                                    <asp:TemplateField visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" id="lbl_passGroupId" text='<%# DataBinder.Eval(Container.DataItem, "pass_group_id")%>' ></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>                                       
                                                    <asp:BoundField ReadOnly="True" HeaderText="Subscription" DataField="name"></asp:BoundField>
                                                </Columns>
                                            </asp:GridView>
				                        </td>
				                    </tr>
						            
									<tr>
										<td align="right" colspan="2" style="padding-right:20px; padding-top:30px">
											<table>
												<tr>
													<td><asp:Button runat="server" id="btn_Cancel" Text="Cancel" causesvalidation="false" onclick="btn_Cancel_Click"></asp:Button></td>
													<td style="padding-left:30px"><asp:Button runat="server" id="btn_Save" Text="Submit" onclick="btn_Save_Click"></asp:Button></td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</div>
						 </td>
					</tr>
				</table>
			</ajax:ajaxpanel>
			<br />
		</td>
	</tr>
</table>


<br /><br />

</body>
