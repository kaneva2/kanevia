///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using KlausEnt.KEP.Kaneva.framework.biz.widgets;
using KlausEnt.KEP.Kaneva.framework.widgets;
using KlausEnt.KEP.Kaneva.mykaneva.widgets;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.csr
{
    public partial class Activity_Suspensions : NoBorderPage
    {
        protected Activity_Suspensions() 
		{
			Title = "CSR - Suspensions";
		}

        protected void Page_Load(object sender, EventArgs e)
        {
            // Make sure they are a CSR
            pnlCSR.Visible = (IsCSR());
            tblNoCSR.Visible = (!IsCSR());

            if (!IsPostBack)
            {
                // Log the CSR activity
                int userId = Convert.ToInt32(Request["userId"]);
                SiteMgmtFacade siteMgmtFacade = new SiteMgmtFacade();
                siteMgmtFacade.InsertCSRLog(GetUserId(), "CSR - Suspensions", 0, userId);

                // Set default dates
                txtBanStartDate.Text = DateTime.Now.ToShortDateString();
                txtBanEndDate.Text = DateTime.Now.AddDays(1).ToShortDateString();

                txtIPBanStartDate.Text = DateTime.Now.ToShortDateString ();
                txtIPBanEndDate.Text = DateTime.Now.AddDays(1).ToShortDateString ();

                InitialBind();
                BindIPBanData (userId);
            }
        }

        private void InitialBind()
        {
            currentPage = 0;

            drp_Statuses.DataTextField = "name";
            drp_Statuses.DataValueField = "status_id";
            drp_Statuses.DataSource = UsersUtility.GetUserStatus();
            drp_Statuses.DataBind();
                        
            dt = UsersUtility.GetUserSuspensions(Convert.ToInt32(Request["userId"]));           
            BindData();
        }

        private void BindData()
        {
            // Set the user's status
            lbl_UserStatus.Text = WebCache.GetStatusName(userStatusId);                     
 
            // Set the sortable columns
            string orderby = CurrentSort + " " + CurrentSortOrder;

            //set the current page
            this.dgrdBans.CurrentPageIndex = currentPage;

            dt.DefaultView.Sort = orderby;

            dgrdBans.DataSource = dt;
            dgrdBans.DataBind();
        }

        private void BindIPBanData (int userId)
        {
            divUsername.InnerText = UsersUtility.GetUserNameFromId (userId);

            DataRow drIPs = UsersUtility.GetIPsByUser (userId);

            if (drIPs == null || drIPs["ip_address"] == null) return;

            string ip = drIPs["ip_address"].ToString ();
            string last_ip = drIPs["last_ip_address"].ToString ();

            // User IP info
            chkIP.Enabled = true;
            chkLastIP.Enabled = true;
            
            chkIP.Checked = false;
            chkLastIP.Checked = false;
            
            chkIP.Text = ip;
            chkLastIP.Text = last_ip;

            lbUnBanIP.Style.Add ("display", "none");
            lbUnBanLastIP.Style.Add ("display", "none");

            lbUnBanIP.CommandArgument = ip;
            lbUnBanLastIP.CommandArgument = last_ip;

            if (UsersUtility.IsIPBanned (ip))
            {
                chkIP.Enabled = false;
                chkIP.Checked = true;
                lbUnBanIP.Style.Add ("display", "block");
            }
            if (UsersUtility.IsIPBanned (last_ip))
            {
                chkLastIP.Enabled = false;
                chkLastIP.Checked = true;
                lbUnBanLastIP.Style.Add ("display", "block");
            }

            // Related IP info
            DataTable dtRelatedIPs = UsersUtility.GetRelatedIPsByUser (userId);

            rptRelatedIPs.DataSource = dtRelatedIPs;
            rptRelatedIPs.DataBind ();
        }


        #region Helper Methods

        private void SortSwitch(string newSort)
        {
            if (CurrentSort == newSort)
            {
                if (CurrentSortOrder == "DESC")
                {
                    CurrentSortOrder = "ASC";
                }
                else
                {
                    CurrentSortOrder = "DESC";
                }
            }
            else
            {
                CurrentSort = newSort;
                CurrentSortOrder = "DESC";
            }
        }

        //private void ShowMsg (HtmlContainerControl ctrl, string msg)
        //{
        //    ctrl.InnerHtml = msg;
        //    ctrl.Style.Add ("display", "block");
        //}

        /// <summary>
        /// ShowMessage
        /// </summary>
        protected void ShowMsg (HtmlContainerControl ctrl, bool isErr, string msg)
        {
          //  messages.ForeColor = isErr ? System.Drawing.Color.DarkRed : System.Drawing.Color.DarkGreen;
            ctrl.Attributes.Add ("class", isErr ? "errBox darkRed" : "infoBox darkGreen");
            ctrl.InnerHtml = msg;
            ctrl.Style.Add ("display", "block");
        }

        #endregion


        #region Event Handlers

        protected void dgrdBans_CancelCommand (object source, DataGridCommandEventArgs e)
        {
            this.dgrdBans.EditItemIndex = -1;
            BindData ();
        }

        protected void dgrdBans_EditCommand (object source, DataGridCommandEventArgs e)
        {
            this.dgrdBans.EditItemIndex = e.Item.ItemIndex;
            BindData ();
        }

        protected void dgrdBans_PageIndexChanged (object source, DataGridPageChangedEventArgs e)
        {
            currentPage = e.NewPageIndex;
            BindData ();
        }

        protected void dgrdBans_SortCommand (object source, DataGridSortCommandEventArgs e)
        {
            SortSwitch (e.SortExpression); //sets the sort expression
            this.dgrdBans.EditItemIndex = -1; //closes any open edit boxes
            BindData ();
        }

        protected void dgrdBans_DeleteCommand (object source, DataGridCommandEventArgs e)
        {
            UserFacade userFacade = new UserFacade();
            int userId = Convert.ToInt32 (Request["userId"]);
            int banId = Convert.ToInt32 (e.Item.Cells[0].Text);

            // We need to know if the end date is in the future
            // Because if it is, the account is locked, and deleting this 
            // suspention sould unlock it.  Therefore when the end date
            // is a null value (perm ban) we give it an arbitrary future date
            // to represent that it is ongoing.

            DateTime banDateEnd = DateTime.Now.AddDays (1);

            if (e.Item.Cells[2].Text.Trim () != "&nbsp;")
            {
                banDateEnd = Convert.ToDateTime (e.Item.Cells[2].Text);
            }



            // if this suspension has an end date later than now
            // and the user is locked, unlock them.
            if (banDateEnd > DateTime.Now)
            {
                if (userStatusId == 6 || userStatusId == 7 || userStatusId == 1)
                {
                    // Set validated 
                    userFacade.UpdateUserStatus(userId, (int)Constants.eUSER_STATUS.REGVALIDATED);
                }
                else
                {
                    // set to not validated
                    userFacade.UpdateUserStatus(userId, (int)Constants.eUSER_STATUS.REGNOTVALIDATED);
                }
            }

            try
            {
                UsersUtility.DeleteSuspension (banId);
                lblDeleteMsg.Text = "Suspension Deleted. ";

                string strUserNote = "Deleted Suspension. ";
                UsersUtility.InsertUserNote (userId, GetUserId (), strUserNote);
            }
            catch (Exception exc)
            {
                lblDeleteMsg.Text = "Error Deleting Suspension: " + exc;
            }
            InitialBind ();
        }

        protected void drpBanLength_Selected (object sender, System.EventArgs e)
        {
            if (drpBanLength.SelectedValue == "Perm")
            {
                txtBanEndDate.Text = "";
            }
            else
            {
                int banLength = Convert.ToInt16 (drpBanLength.SelectedValue);
                DateTime banEnd = Convert.ToDateTime (txtBanStartDate.Text).AddDays (banLength);
                txtBanEndDate.Text = banEnd.ToShortDateString ().ToString ();
            }
        }


        protected void drpIPBanLength_Selected (object sender, EventArgs e)
        {
            if (drpIPBanLength.SelectedValue == "Perm")
            {
                txtIPBanEndDate.Text = "";
            }
            else
            {
                int ipBanLength = Convert.ToInt16 (drpIPBanLength.SelectedValue);
                DateTime ipBanEnd = Convert.ToDateTime (txtIPBanStartDate.Text).AddDays (ipBanLength);
                txtIPBanEndDate.Text = ipBanEnd.ToShortDateString ().ToString ();
            }
        }

        protected void btnAddIPBan_Click (object sender, EventArgs e)
        {
            try
            {
                int userId = Convert.ToInt32 (Request["userId"]);

                string ipBanStart = txtIPBanStartDate.Text + " " + drpIPBanStartTime.SelectedValue;
                string ipBanEnd = string.Empty;

                // Make sure at least 1 ip is selected
                //if (!chkIP.Checked && !chkLastIP.Checked)
                //{
                //    ShowMsg (spnIPBanAlertMsg, "Please select an IP Address to ban.");
                //    return;
                //}

                if (((chkIP.Enabled && !chkIP.Checked) && ((chkLastIP.Enabled && !chkLastIP.Checked) || !chkLastIP.Enabled)) ||
                     ((chkLastIP.Enabled && !chkLastIP.Checked) && ((chkIP.Enabled && !chkIP.Checked) || !chkIP.Enabled)))
                {
                    ShowMsg (spnIPBanAlertMsg, true, "Please select an IP Address to ban.");
                    return;
                }

                // Check to see if both IPs are already banned
                if (!chkIP.Enabled && !chkLastIP.Enabled)
                {
                    ShowMsg (spnIPBanAlertMsg, true, "Both IP Addresses are already banned.");
                    return;
                }

                // Check if ban length is set via drop down
                if (drpIPBanLength.SelectedValue != "0")
                {
                    int banLength = Convert.ToInt32 (drpIPBanLength.SelectedValue);

                    // check for permanent ban.  perm ban value is -1
                    if (banLength < 0)
                    {
                        ipBanEnd = DateTime.MaxValue.ToString ();
                    }
                    else
                    {
                        DateTime startDate = new DateTime ();
                        startDate = Convert.ToDateTime (ipBanStart);
                        ipBanEnd = startDate.AddDays(Convert.ToDouble(banLength)).ToString();       
                    }
                }
                else
                {   // use end date/time to set ban length
                    ipBanEnd = txtIPBanEndDate.Text.Trim ();
                    if (ipBanEnd == "")
                    {
                        ShowMsg (spnIPBanAlertMsg, true, "Please enter an End Date for IP Ban.");
                        return;
                    }
                    else
                    {
                        try
                        {
                            DateTime startDate = new DateTime ();
                            startDate = Convert.ToDateTime (ipBanStart);

                            DateTime endDate = new DateTime ();
                            endDate = Convert.ToDateTime (ipBanEnd + " " + drpIPBanEndTime.SelectedValue);

                            if (endDate <= startDate)
                            {
                                ShowMsg (spnIPBanAlertMsg, true, "Please enter an End Date/Time later than Start Date.");
                                return;
                            }
                        }
                        catch
                        {
                            ShowMsg (spnIPBanAlertMsg, true, "Please enter a valid End Date/Time.");
                            return;
                        }
                    }

                    ipBanEnd = txtIPBanEndDate.Text.Trim () + " " + drpIPBanEndTime.SelectedValue;
                }

                // is sign-up ip address checked?
                if (chkIP.Checked && chkIP.Enabled)
                {
                    try
                    {
                        // verify ip is not already banned.  will throw error if ip already banned
                        if (!UsersUtility.IsIPBanned (chkLastIP.Text))
                        {
                            // ban the ip
                            if (!UsersUtility.AddIPBan (chkIP.Text, ipBanStart, ipBanEnd).Equals (0))
                            {
                                ShowMsg (spnIPBanAlertMsg, false, "IP " + chkIP.Text + " added to ban list.");

                                string strUserNote = "IP " + chkIP.Text + " Banned. <br />";
                                if (txtIPBanReasonNotes.Text != "") { strUserNote += "Notes: " + txtIPBanReasonNotes.Text + "<br />"; }

                                // add user not explaining the ban
                                UsersUtility.InsertUserNote (userId, GetUserId (), strUserNote);
                            }
                            else
                            {
                                ShowMsg (spnIPBanAlertMsg, true, "Failed to Add Ban for IP: " + chkIP.Text + ".  AddIPBan() call failed." +
                                    "<br/>IP : " + chkLastIP.Text +
                                    "<br/>Start Date : " + ipBanStart +
                                    "<br/>End Date : " + ipBanEnd);
                            }
                        }
                    }
                    catch (Exception exc)
                    {
                        ShowMsg (spnIPBanAlertMsg, true, "Error Banning IP " + chkIP.Text + ". Please check the date fields. <br />" + exc.ToString () + "<br />");
                    }
                }

                // is last ip-address checked?
                if (chkLastIP.Checked && chkLastIP.Enabled)
                {
                    try
                    {
                        // verify the ip is not already banned.  will throw error if ip already banned
                        if (!UsersUtility.IsIPBanned (chkLastIP.Text))
                        {
                            // ban the ip
                            if (!UsersUtility.AddIPBan (chkLastIP.Text, ipBanStart, ipBanEnd).Equals (0))
                            {
                                ShowMsg (spnIPBanAlertMsg, false, "IP " + chkLastIP.Text + " added to ban list.");

                                string strUserNote = "IP " + chkLastIP.Text + " Banned. <br />";
                                if (txtIPBanReasonNotes.Text != "") { strUserNote += "Notes: " + txtIPBanReasonNotes.Text + "<br />"; }
                                
                                // add user not explaining the ban
                                UsersUtility.InsertUserNote (userId, GetUserId (), strUserNote);
                            }
                            else
                            {
                                ShowMsg (spnIPBanAlertMsg, true, "Failed to Add Ban for Last IP: " + chkLastIP.Text + ".  AddIPBan() call failed." +
                                    "<br/>IP : " + chkLastIP.Text +
                                    "<br/>Start Date : " + ipBanStart +
                                    "<br/>End Date : " + ipBanEnd);
                            }
                        }
                    }
                    catch (Exception exc)
                    {
                        ShowMsg (spnIPBanAlertMsg, true, "Error Banning Last IP " + chkLastIP.Text + ". Please check the date fields. <br />" + exc.ToString () + "<br />");
                    }
                }

                BindIPBanData (userId);
            }
            catch (Exception exc)
            {
                ShowMsg (spnIPBanAlertMsg, true, "Error Banning IP. Please check the date fields. <br />" + exc.ToString () + "<br />");
            }
        }

        protected void lbUnBanIP_Click (object sender, CommandEventArgs e)
        {
            try
            {
                string ip = e.CommandArgument.ToString ();

                if (!UsersUtility.RemoveIPBan (ip).Equals (0))
                {
                    ShowMsg (spnIPBanAlertMsg, false, "Ban removed for IP " + ip + ".");
                }

                BindIPBanData (Convert.ToInt32 (Request["userId"]));
            }
            catch (Exception exc)
            {
                ShowMsg (spnIPBanAlertMsg, true, "Error Removin ban for IP. <br />" + exc.ToString () + "<br />");
            }
        }

        protected void btn_Add_Click (object sender, EventArgs e)
        {
            UserFacade userFacade = new UserFacade();
            int userId = Convert.ToInt32 (Request["userId"]);

            try
            {
                string banStart = txtBanStartDate.Text + " " + drpBanStartTime.SelectedValue;

                // Handle permanent ban cases

                string banEnd = null;
                if (txtBanEndDate.Text != "")
                {
                    banEnd = txtBanEndDate.Text + " " + drpBanEndTime.SelectedValue;
                }


                if (!UsersUtility.AddUserSuspension (userId, banStart, banEnd).Equals (0))
                {

                    lbl_Messages.Text = "Suspension Added.";

                    string strUserNote = "User suspended. <br />";
                    strUserNote += "Reason: " + drpBanReasons.SelectedValue + "<br />";
                    if (txtBanReasonNotes.Text != "") { strUserNote += "Notes: " + txtBanReasonNotes.Text + "<br />"; }
                    UsersUtility.InsertUserNote (userId, GetUserId (), strUserNote);

                    // Update their status

                    if (userStatusId == 1)
                    {
                        userFacade.UpdateUserStatus(userId, (int)Constants.eUSER_STATUS.LOCKEDVALIDATED);
                    }
                    else if (userStatusId == 2)
                    {
                        userFacade.UpdateUserStatus(userId, (int)Constants.eUSER_STATUS.LOCKED);
                    }

                    //cancel any subscriptions they may have if it is a permanent ban
                    if (drpBanLength.SelectedValue == "Perm")
                    {
                        try
                        {
                            SubscriptionFacade subFacade = new SubscriptionFacade();

                            //see if they have any subscriptions
                            List<UserSubscription> usersubscriptions = subFacade.GetUserSubscriptions(userId);

                            if (usersubscriptions.Count > 0)
                            {
                                bool bSystemDown = false;
                                string userErrorMessage = "";

                                foreach (UserSubscription userSubscription in usersubscriptions)
                                {
                                    userSubscription.UserCancellationReason = "User has been banned";

                                    CybersourceAdaptor cybersourceAdaptor = new CybersourceAdaptor();
                                    bool succeed = cybersourceAdaptor.CancelSubscription(ref bSystemDown, ref userErrorMessage,userSubscription);
                                }

                                //tickle the wok servers
                                (new RemoteEventSender()).BroadcastItemChangeEvent(RemoteEventSender.ObjectType.PlayerPassList, RemoteEventSender.ChangeType.UpdateObject, userId);
                            }
                        }
                        catch (Exception exc)
                        {
                            lbl_Messages.Text = "Error cancelling the users subscriiption. <br />" + exc.ToString() + "<br />";
                        }

                    }
                }
                else
                {
                    lbl_Messages.Text = "Falied to Add Suspension.";
                }

            }
            catch (Exception exc)
            {
                lbl_Messages.Text = "Error Suspending User. Please check the date fields. <br />" + exc.ToString () + "<br />";
            }
            currentPage = 0;
            this.dgrdBans.EditItemIndex = 0;
            InitialBind ();
        }

        protected void btn_StatusUpdate_Click (object sender, EventArgs e)
        {
            UserFacade userFacade = new UserFacade();
            int userId = Convert.ToInt32 (Request["userId"]);

            // get old status 
            int oldUserStatusId = userStatusId;

            userFacade.UpdateUserStatus(userId, Convert.ToInt32(drp_Statuses.SelectedValue));

            // If user is deleted mark their profile home page as 'only me'
            if (drp_Statuses.SelectedValue == "7" || drp_Statuses.SelectedValue == "3")
            {
                // get the info of the correct layout_page
                int channelId = CommunityUtility.GetPersonalChannelId (userId);

                int pageId = PageUtility.GetChannelDefaultPageId (channelId);

                DataRow drPage = PageUtility.GetLayoutPage (pageId);

                string strName = drPage["name"].ToString ();

                int groupId = 0;
                if (drPage["group_id"] != DBNull.Value) { groupId = Convert.ToInt32 (drPage["group_id"]); }

                // update it to be 'only me'
                PageUtility.UpdateLayoutPage (pageId, strName, groupId, 2);
            }

            // Update User Note
            string strUserNote = "User Status Updated. <br />";
            strUserNote += "From: " + WebCache.GetStatusName (oldUserStatusId) + " To: " + WebCache.GetStatusName (userStatusId) + "<br />";
            if (txtBanReasonNotes.Text != "") { strUserNote += "Notes: " + txtStatusChangeReason.Text + "<br />"; }
            UsersUtility.InsertUserNote (userId, GetUserId (), strUserNote);

            lblStatusMsg.Text = "User Status Updated.";
            BindData ();
        }


        #endregion


        #region Properties

        /// <summary>
        /// DEFAULT_SORT
        /// </summary>
        /// <returns></returns>
        protected override string DEFAULT_SORT
        {
            get
            {
                return "ban_date_end";
            }
        }

        /// <summary>
        /// DEFAULT_SORT
        /// </summary>
        /// <returns></returns>
        protected override string DEFAULT_SORT_ORDER
        {
            get
            {
                return "DESC";
            }
        }

        protected int userStatusId
        {
            get
            {
                int userId = Convert.ToInt32 (Request["userId"]);
                UserFacade userFacade = new UserFacade ();
                User user = userFacade.GetUser (userId);
                return user.StatusId;
            }
        }

        protected string userStatusName
        {
            get
            {
                int userId = Convert.ToInt32 (Request["userId"]);
                UserFacade userFacade = new UserFacade ();
                User user = userFacade.GetUser (userId);
                return user.Ustate;
            }
        }

        private DataTable dt
        {
            get
            {
                if (Session["_permZoneRoles"] == null)
                {
                    Session["_permZoneRoles"] = new DataTable ();
                }
                return (DataTable) Session["_permZoneRoles"];
            }
            set
            {
                Session["_permZoneRoles"] = value;
            }
        }

        private int currentPage
        {
            get
            {
                if (Session["_currentPage"] == null)
                {
                    Session["_currentPage"] = 0;
                }
                return (int) Session["_currentPage"];
            }
            set
            {
                Session["_currentPage"] = value;
            }
        }

        #endregion


        #region Declerations

        protected Panel pnlCSR;
        protected Button btn_Add, btn_Save;
        protected DataGrid dgrdBans;
        protected Repeater rptRelatedIPs;
        protected CheckBox chkIP, chkLastIP;
        protected Label lbl_Messages, lbl_UserStatus, lblStatusMsg, lblDeleteMsg, lblIPBanMessages;
        protected TextBox txtBanStartDate, txtBanEndDate, txtBanReasonNotes, txtStatusChangeReason;
        protected TextBox txtIPBanStartDate, txtIPBanEndDate, txtIPBanReasonNotes;
        protected DropDownList drp_Statuses, drpBanStartTime, drpBanEndTime, drpBanLength, drpBanReasons;
        protected DropDownList drpIPBanStartTime, drpIPBanEndTime, drpIPBanLength;
        protected LinkButton lbUnBanIP, lbUnBanLastIP;

        protected HtmlTable tblNoCSR;
        protected HtmlContainerControl divUsername;
        protected HtmlContainerControl spnIPBanAlertMsg;

        #endregion
    }
}
