///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva;
using log4net;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.CSR
{
	/// <summary>
	/// Summary description for userActivity.
	/// </summary>
	public class userActivity : NoBorderPage
	{
		protected userActivity () 
		{
			Title = "CSR - User Activity";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Make sure they are a CSR
			pnlCSR.Visible = (IsCSR ());
			tblNoCSR.Visible = (!IsCSR ());

			int userId = Convert.ToInt32 (Request ["userId"]);

            UserFacade userFacade = new UserFacade();
            User user = userFacade.GetUser(userId);

			lblWho.Text = user.Username;

			hidUserId.Value = userId.ToString ();

			lblItemsSold.Text = "0";
			lblBlogs.Text = "0";
			lblPosts.Text = ForumUtility.GetUserForumTopics (userId, "", 1, 1).TotalCount.ToString ();
			lblReply.Text = ForumUtility.GetUserForumThreads (userId, "", 1, 1).TotalCount.ToString ();
			lblRatings.Text = "0";
			lblItems.Text = "0"; 
			lblChannels.Text = "0";

		}

		protected HtmlTable tblNoCSR;
		protected Panel pnlCSR;
		protected Label lblWho, lblItemsSold, lblBlogs, lblPosts, lblReply, lblRatings, lblItems, lblChannels;

		protected HtmlInputHidden hidUserId;

		// Logger
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
