///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva;
using log4net;
using System.Drawing;
using MagicAjax;
using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;
using System.Collections.Specialized;
using System.Collections.Generic;

namespace KlausEnt.KEP.Kaneva.csr
{
    public partial class PromotionManagement : NoBorderPage
    {
        #region Declarations

        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #endregion

        protected PromotionManagement() 
		{
			Title = "CSR - Promotions";
        }

        #region PageLoad

        protected void Page_Load(object sender, EventArgs e)
        {
            // Redirect if user is not admin
            if (!IsCSR ())
            {
                Response.Redirect ("~/default.aspx");
            }

            if (!IsPostBack)
            {
                //bind the promotions
                BindPromotionalData(currentPage);

                //bind pulldowns
                PopulateGenderList();
                PopulateGiftCardsList();
                PopulatePromotionTypeList();
                PopulateKEITypeLists();
                PopulatePassGroupList();
                //hide the WOK browser
                divWOKItemsSearch.Style.Add ("display", "none");
                divCommunitiesSearch.Style.Add ("display", "none");
                divPreview.Style.Add ("display", "none");
                divPromotionsDetails.Style.Add ("display", "none");
            }
        }

        #endregion

        #region Primary Functions

        /// <summary>
        /// BindWOKInventory
        /// </summary>
        private void BindWOKInventory()
        {
            WOKItems.PageSize = PageSize();
            WOKItems.PageIndex = WOKSearchCurrentPage;
            //PagedDataTable pdt = WOKStoreUtility.GetWOKItems(filter, currentPage, PageSize());
            DataTable dt = WOKStoreUtility.GetWOKItems(WOKSearchFilter);
            WOKItems.DataSource = dt;
            WOKItems.DataBind();
        }

        /// <summary>
        /// BindCommunities
        /// </summary>
        private void BindCommunities()
        {
            Communities.PageSize = PageSize();
            Communities.PageIndex = CommunitySearchCurrentPage;
            DataTable dt = CommunityUtility.GetAllPublicChannels(CommunitySearchFilter);
            Communities.DataSource = dt;
            Communities.DataBind();
        }

        /// <summary>
        /// BindData
        /// </summary>
        private void BindPromotionalData(int currentPage)
        {
            //query the database for existing promotions
            DataTable dt = StoreUtility.GetPromotionalOffers();

            // Set the sortable columns
            string orderby = CurrentSort + " " + CurrentSortOrder;

            //set the current page
            this.dg_Promotions.PageIndex = currentPage;

            //set the sort order
            if ((dt != null) && (dt.Rows.Count > 0))
            {
                dt.DefaultView.Sort = orderby;
            }

            if ((dt != null) && (dt.Rows.Count > 0))
            {
                lbl_NoPromotions.Visible = false;

                dg_Promotions.DataSource = dt;
                dg_Promotions.DataBind();
            }
            else
            {
                lbl_NoPromotions.Visible = true;
            }

        }

        private void BindWOKItemData(DataTable dt)
        {
            promoItemsDT = dt;
            dg_PromotionalItems.DataSource = dt;
            dg_PromotionalItems.DataBind();
        }

        /// <summary>
        /// BindStoreList
        /// </summary>
        private void BindSubscriptionList(List<Subscription> usedSubscriptions)
        {
            DataTable subscriptionsDT = null;

            //get all the current WOK stores
            subscriptionsDT = (new SubscriptionFacade()).GetAvailableSubscriptions();

            //create the check field column for the get all stores data result
            DataColumn checkColumn = new DataColumn();
            checkColumn.DataType = System.Type.GetType("System.Boolean");
            checkColumn.AllowDBNull = false;
            checkColumn.ColumnName = "cbx_inpromotion";
            checkColumn.DefaultValue = false;
            subscriptionsDT.Columns.Add(checkColumn);

            //mark the stores the the item is in if any
            if (usedSubscriptions.Count > 0)
            {
                foreach (Subscription subscript in usedSubscriptions)
                {
                    DataRow[] subscriptionRow = subscriptionsDT.Select("subscription_id = " + subscript.SubscriptionId.ToString());
                    if (subscriptionRow.Length > 0)
                    {
                        DataRow subscription = subscriptionRow[0];
                        subscription["cbx_inpromotion"] = true;

                        subscription2Promo.Add(subscript.SubscriptionId.ToString(), "true");

                        subscription.AcceptChanges();
                        subscriptionsDT.AcceptChanges();
                    }

                }
            }

            dgd_Subscriptions.DataSource = subscriptionsDT;
            dgd_Subscriptions.DataBind();
        }

        /// <summary>
        /// BindStoreList
        /// </summary>
        private void DeletePromotionAndRelatedData(int promotionalId)
        {
            SubscriptionFacade subFac = new SubscriptionFacade();
            
            //delete all the promotion to subscription references
            subFac.DeleteAllSubscription2PromotionByPromoID(promotionalId);

            //delete the promotional offer
            StoreUtility.DeletePromotionalOffer(promotionalId);
        }

        #endregion

        #region Helper Functions

        /// <summary>ge
        ///  
        /// </summary>
        private void PopulateGiftCardsList()
        {
            ddl_giftcards.DataSource = StoreUtility.GetGiftCards();
            ddl_giftcards.DataTextField = "description";
            ddl_giftcards.DataValueField = "gift_card_id";
            ddl_giftcards.DataBind();

            ddl_giftcards.Items.Insert(0, new ListItem("-- Select --", "0"));
        
        }

        /// <summary>
        ///  populates KEI type pulldowns
        /// </summary>
        private void PopulateKEITypeLists()
        {
            DataTable dt = StoreUtility.GetKEITypes();

            ddl_KEICreditType.DataSource = dt;
            ddl_KEICreditType.DataTextField = "description";
            ddl_KEICreditType.DataValueField = "kei_point_id";
            ddl_KEICreditType.DataBind();

            //ddl_KEICreditType.Items.Insert(0, new ListItem("-- Select --", "0"));

            ddl_FreePointType.DataSource = dt;
            ddl_FreePointType.DataTextField = "description";
            ddl_FreePointType.DataValueField = "kei_point_id";
            ddl_FreePointType.DataBind();

            //ddl_FreePointType.Items.Insert(0, new ListItem("-- Select --", "0"));

        }

        private void PopulatePassGroupList()
        {
            ddl_PassGroupType.DataSource = WOKStoreUtility.GetPassGroups();
            ddl_PassGroupType.DataTextField = "name";
            ddl_PassGroupType.DataValueField = "pass_group_id";
            ddl_PassGroupType.DataBind();

            //ddl_PassGroupType.Items.Insert(0, new ListItem("-- Select --", "0"));
        }

        private void PopulatePromotionTypeList()
        {
            ddl_PromoOfferType.DataSource = StoreUtility.GetPromotionalOfferTypes();
            ddl_PromoOfferType.DataTextField = "promotional_offer_type";
            ddl_PromoOfferType.DataValueField = "promotional_offer_type_id";
            ddl_PromoOfferType.DataBind();

            //ddl_PromoOfferType.Items.Insert(0, new ListItem("-- Select --", "0"));

        }

        /// <summary>
        /// Clears out all Promotion fields 
        /// </summary>
        private void PopulateGenderList()
        {
            ddl_gender.Items.Add(new ListItem("Both", Constants.GENDER_BOTH));
            ddl_gender.Items.Add(new ListItem("Male", Constants.GENDER_MALE));
            ddl_gender.Items.Add(new ListItem("Female", Constants.GENDER_FEMALE));
        }

        /// <summary>
        /// Clears out all Promotion fields 
        /// </summary>
        private void ClearPromotionData()
        {
            tbx_BundleTitle.Text = "";
            tbx_PromoID.Text = "";
            tbx_SubheadLine1.Text = "";
            tbx_SubheadLine2.Text = "";
            tbx_Price.Text = "";
            tbx_Credits.Text = "";
            tbx_PromotionDescr.Text = "";
            txtPromoStartDate.Text = "";
            txtPromoEndDate.Text = "";
            tbx_PromotionListHeading.Text = "";
            tbx_PromotionPackageLabel.Text = "";
            tbx_BackgroundColor.Text = "";
            tbx_HighlightColor.Text = "";
            inp_backgroundImage.Value = "";
            tbx_adRotatorPath.Text = "";
            inp_stickerImage.Value = "";
            ddl_KEICreditType.SelectedIndex = 0;
            tbx_FreeAwardPoints.Text = "";
            ddl_FreePointType.SelectedIndex = 0;
            ddl_PromoOfferType.SelectedIndex = 0;
            ddl_PassGroupType.SelectedIndex = 0;
            tbx_sku.Text = "";
            tbx_ValueOfCredits.Text = "200";
            tbx_FontColor.Text = "";
            tbx_CouponCode.Text = "";
            dg_PromotionalItems.DataSource = null;
            dg_PromotionalItems.DataBind();
            pnl_PromotionsWOKItemsAdd.Visible = false;
            cbx_UseOptions.Checked = false;
            tbx_OriginalPrice.Text = "0.0";
            tr_SubscriptionData.Visible = false;
            ClearPromotionItems();
            ClearSubscriptionItems();
        }

        /// <summary>
        /// Clears out all Promotional Items fields 
        /// </summary>
        private void ClearPromotionItems()
        {
            tbx_promoofferid.Text = "";
            tbx_promoidWOK.Text = "";
            tbx_itemquantity.Text = "";
            lbl_wokitemid.Text = "";
            lbl_itemdesciption.Text = "";
            ddl_gender.SelectedValue = Constants.GENDER_BOTH;
            WOKItemValidation.Text = "";
            selectedItemIndex.Text = "";
            ddl_giftcards.SelectedValue = "0";
            lbl_community.Text = "";
            lbl_communityId.Text = "";
            tbx_marketprice.Text = "";
            tbx_alternatedesciption.Text = "";
            descriptionRequired.Visible = false;
            itemIDRequired.Visible = false;
            quantityRequired.Visible = false;
        }

        /// <summary>
        /// Clears out all Promotional Items fields 
        /// </summary>
        private void ClearSubscriptionItems()
        {
            subscription2Promo.Clear();
        }

        private void WOKSearchClear()
        {
            tbx_WOKSearchFilter.Text = "";
            divWOKItemsSearch.Style.Add ("display", "none");
            WOKItems.DataSource = null;
            WOKItems.DataBind();
            WOKSearchFilter = null;
            WOKSearchCurrentPage = 0;
        }

        private void CommunitySearchClear()
        {
            tbx_CommunityFilter.Text = "";
            divCommunitiesSearch.Style.Add ("display", "none");
            Communities.DataSource = null;
            Communities.DataBind();
            CommunitySearchFilter = null;
            CommunitySearchCurrentPage = 0;
        }

        private void ProcessSubscriptions()
        {
            //make it visible and clear the fields before populating
            ClearSubscriptionItems();

            //initialize the needed variables
            List<Subscription> subscriptions = new List<Subscription>();
            int promotionID = Convert.ToInt32(tbx_PromoID.Text);
            Subscription subscription = new Subscription();

            //show singular subscription row
            tr_SubscriptionData.Visible = true;

            //if editing an existging items process these actions
            if (promotionID > 0)
            {
                subscriptions = (new SubscriptionFacade()).GetSubscriptionsByPromotionId((uint)promotionID);
            }

            //populate the available subscriptions
            BindSubscriptionList(subscriptions);
        }

        private void WireJavaScript()
        {
            //wire javascript for the promotional start date calendar
            //set color picker controls
            tbx_HighlightColorAppClrPickerStr.NavigateUrl = "javascript:pickColor('" + tbx_HighlightColorAppClrPickerStr.ClientID + "');";
            tbx_HighlightColor.Attributes.Add("onchange", "relateColor('" + tbx_HighlightColorAppClrPickerStr.ClientID + "',this.value);");
            if (!ClientScript.IsClientScriptBlockRegistered(GetType(), "setbgcolor"))
            {
                ClientScript.RegisterStartupScript(GetType(), "setbgcolor", "<script>relateColor('" +
                    tbx_HighlightColorAppClrPickerStr.ClientID + "',$('" + tbx_HighlightColor.ClientID + "').value);</script>");
            }

            tbx_BackgroundColorAppClrPickerStr.NavigateUrl = "javascript:pickColor('" + tbx_BackgroundColorAppClrPickerStr.ClientID + "');";
            tbx_BackgroundColor.Attributes.Add("onchange", "relateColor('" + tbx_BackgroundColorAppClrPickerStr.ClientID + "',this.value);");
            if (!ClientScript.IsClientScriptBlockRegistered(GetType(), "setbgcolor2"))
            {
                ClientScript.RegisterStartupScript(GetType(), "setbgcolor2", "<script>relateColor('" +
                    tbx_BackgroundColorAppClrPickerStr.ClientID + "',$('" + tbx_BackgroundColor.ClientID + "').value);</script>");
            }

            tbx_FontColorAppClrPickerStr.NavigateUrl = "javascript:pickColor('" + tbx_FontColorAppClrPickerStr.ClientID + "');";
            tbx_FontColor.Attributes.Add("onchange", "relateColor('" + tbx_FontColorAppClrPickerStr.ClientID + "',this.value);");
            if (!ClientScript.IsClientScriptBlockRegistered(GetType(), "setbgcolor3"))
            {
                ClientScript.RegisterStartupScript(GetType(), "setbgcolor3", "<script>relateColor('" +
                    tbx_FontColorAppClrPickerStr.ClientID + "',$('" + tbx_FontColor.ClientID + "').value);</script>");
            }			
        }

        public int PageSize()
        {
            return 10;
        }

        /// <summary>
        /// DEFAULT_SORT
        /// </summary>
        /// <returns></returns>
        protected override string DEFAULT_SORT
        {
            get
            {
                return "bundle_title";
            }
        }

        private int WOKSearchCurrentPage
        {
            get
            {
                if (ViewState["_WOKcurrentPage"] == null)
                {
                    ViewState["_WOKcurrentPage"] = 0;
                }
                return (int)ViewState["_WOKcurrentPage"];
            }
            set
            {
                ViewState["_WOKcurrentPage"] = value;
            }
        }

        private int CommunitySearchCurrentPage
        {
            get
            {
                if (ViewState["_communitycurrentPage"] == null)
                {
                    ViewState["_communitycurrentPage"] = 0;
                }
                return (int)ViewState["_communitycurrentPage"];
            }
            set
            {
                ViewState["_communitycurrentPage"] = value;
            }
        }

        private void resetViewState()
        {
            WOKSearchFilter = null;
            currentPage = 0;
            promoItemsDT = null;
            CommunitySearchFilter = null;
        }

        private NameValueCollection subscription2Promo
        {
            get
            {
                if (ViewState["_subscription2Promo"] == null)
                {
                    ViewState["_subscription2Promo"] = new NameValueCollection();
                }
                return (NameValueCollection)ViewState["_subscription2Promo"];
            }
            set
            {
                ViewState["_subscription2Promo"] = value;
            }
        }

        private string WOKSearchFilter
        {
            get
            {
                if (ViewState["_WOKSearchFilter"] == null)
                {
                    ViewState["_WOKSearchFilter"] = "";
                }
                return ViewState["_WOKSearchFilter"].ToString();
            }
            set
            {
                ViewState["_WOKSearchFilter"] = value;
            }
        }

        private string CommunitySearchFilter
        {
            get
            {
                if (ViewState["_CommunitySearchFilter"] == null)
                {
                    ViewState["_CommunitySearchFilter"] = "";
                }
                return ViewState["_CommunitySearchFilter"].ToString();
            }
            set
            {
                ViewState["_CommunitySearchFilter"] = value;
            }
        }

        private int currentPage
        {
            get
            {
                if (ViewState["_currentPage"] == null)
                {
                    ViewState["_currentPage"] = 0;
                }
                return (int)ViewState["_currentPage"];
            }
            set
            {
                ViewState["_currentPage"] = value;
            }
        }

        //allows user to switch the ordering between DESC and ASC
        //based on column clicked
        private void SortSwitch(string newSort)
        {
            if (CurrentSort == newSort)
            {
                if (CurrentSortOrder == "DESC")
                {
                    CurrentSortOrder = "ASC";
                }
                else
                {
                    CurrentSortOrder = "DESC";
                }
            }
            else
            {
                CurrentSort = newSort;
                CurrentSortOrder = "DESC";
            }
        }

        private DataTable promoItemsDT
        {
            get
            {
                if (Session["_promoItemsDT"] == null)
                {
                    Session["_promoItemsDT"] = new DataTable();
                }
                return (DataTable)Session["_promoItemsDT"];
            }
            set
            {
                Session["_promoItemsDT"] = value;
            }
        }

        /// <summary>
        /// DEFAULT_SORT
        /// </summary>
        /// <returns></returns>
        protected override string DEFAULT_SORT_ORDER
        {
            get
            {
                return "DESC";
            }
        }

        #endregion

        #region Event Handlers

        public void lbn_CommunitySearch_Click(object sender, EventArgs e)
        {
            CommunitySearchFilter = "name LIKE '" + tbx_CommunityFilter.Text + "%'";
            BindCommunities();
        }

        public void lbn_CommunitySearchCancel_Click(object sender, EventArgs e)
        {
            CommunitySearchClear();
        }

        protected void ChooseCommunity_Click(object sender, EventArgs e)
        {
            divCommunitiesSearch.Style.Add ("display", "block");
            BindCommunities();
        }

        protected void Communities_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            CommunitySearchCurrentPage = e.NewPageIndex;
            BindCommunities();
        }

        protected void Communities_SelectedIndexChanged(object sender, EventArgs e)
        {
            lbl_communityId.Text = Communities.SelectedRow.Cells[0].Text;
            lbl_community.Text = Communities.SelectedRow.Cells[1].Text;
            CommunitySearchClear();
        }

        protected void WOKItems_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            WOKSearchCurrentPage = e.NewPageIndex;
            BindWOKInventory();
        }

        protected void WOKItems_SelectedIndexChanged(object sender, EventArgs e)
        {
            lbl_wokitemid.Text = WOKItems.SelectedRow.Cells[0].Text;
            lbl_itemdesciption.Text = WOKItems.SelectedRow.Cells[1].Text;
            WOKSearchClear();
        }

        public void lbn_WOKSearch_Click(object sender, EventArgs e)
        {
           if(KanevaGlobals.IsNumeric (tbx_WOKSearchFilter.Text))
           {
               WOKSearchFilter = "global_id = " + tbx_WOKSearchFilter.Text ;
           }
           else
           {
                WOKSearchFilter = "name LIKE '%" + tbx_WOKSearchFilter.Text + "%'";
           }
            BindWOKInventory();
        }

        public void lbn_WOKSearchCancel_Click(object sender, EventArgs e)
        {
            WOKSearchClear();
        }

        protected void ChooseWOKItem_Click(object sender, EventArgs e)
        {
            divWOKItemsSearch.Style.Add ("display", "block");
            BindWOKInventory();
        }

        public void lbn_CancelPreview_Click(object sender, EventArgs e)
        {
            tbx_previewDate.Text = "";
            divPreview.Style.Add ("display", "none");
            btn_Preview.Enabled = true;
            btn_Cancel.Enabled = true;
            btn_Save.Enabled = true;
        }

        protected void btn_Preview_Click(object sender, EventArgs e)
        {
            divPreview.Style.Add ("display", "block");
            btn_Preview.Enabled = false;
            btn_Cancel.Enabled = false;
            btn_Save.Enabled = false;
        }

        protected void lbn_PreviewPromotion_Click(object sender, EventArgs e)
        {
            if (tbx_previewDate.Text != "")
            {
                tbx_previewDate.Text = "";
                divPreview.Style.Add ("display", "none");
                btn_Preview.Enabled = true;
                btn_Cancel.Enabled = true;
                btn_Save.Enabled = true;
            }
        }

        protected void cbx_inpromotion_CheckedChanged(object sender, EventArgs e)
        {
            GridViewRow ritem = (GridViewRow)((Control)sender).NamingContainer;
            string id = ((Label)ritem.FindControl("lbl_subscriptionId")).Text;
            string value = ((CheckBox)ritem.FindControl("cbx_inpromotion")).Checked.ToString();
            if (subscription2Promo[id] != null)
            {
                subscription2Promo[id] = value;
            }
            else
            {
                subscription2Promo.Add(id, value);
            }

        }

        protected void lbn_NewOffer_Click(object sender, EventArgs e)
        {
            //clear the fields
            ClearPromotionData();

            //make the details panel visible
            divPromotionsDetails.Style.Add ("display", "block");

            //set id to -1 <indicator that it is a new entry>
            tbx_PromoID.Text = "-1";

            //set sku to next highest 
            tbx_sku.Text = StoreUtility.GetNewSku();


            WireJavaScript();
        }

        protected void lbn_NewItems_Click(object sender, System.EventArgs e)
        {
            //clear the fields
            ClearPromotionItems();

            //make the details panel visible
            pnl_PromotionsWOKItemsAdd.Visible = true;

            WireJavaScript();

            //set the action text
            btn_ItemsAdd.Text = "Add";

            //set id to -1 <indicator that it is a new entry>
            tbx_promoofferid.Text = "-1";
            tbx_promoidWOK.Text = tbx_PromoID.Text;
        }

        protected void btn_Cancel_Click(object sender, System.EventArgs e)
        {
            //clear the fields
            ClearPromotionData();

            //make the details panel visible
            divPromotionsDetails.Style.Add ("display", "none");

        }

        protected void dg_Promotions_RowDeleting(object source, System.Web.UI.WebControls.GridViewDeleteEventArgs e)
        {
            //get the selected row
            GridViewRow gvr = dg_Promotions.Rows[e.RowIndex];

            //delete the record and rebind
            DeletePromotionAndRelatedData(Convert.ToInt32(gvr.Cells[0].Text));

            BindPromotionalData(currentPage);

            //clear the fields
            ClearPromotionData();

            //reset viewstate
            resetViewState();

            //make the details panel visible
            divPromotionsDetails.Style.Add ("display", "none");
        }

        protected void dg_Promotions_RowEditing(object source, System.Web.UI.WebControls.GridViewEditEventArgs e)
        {
            //clear the fields
            ClearPromotionData();

            //make the promotions details panel visible
            divPromotionsDetails.Style.Add ("display", "block");

            WireJavaScript();

           //get the promotion
            GridViewRow gvr = dg_Promotions.Rows[e.NewEditIndex];

            try
            {
                //parse dates
                DateTime startDate = Convert.ToDateTime(gvr.Cells[2].Text.Replace("&nbsp;", ""));
                DateTime endDate = Convert.ToDateTime(gvr.Cells[3].Text.Replace("&nbsp;", ""));

                //populate fields
                //replace is a work around for unexplained &nbsp; showing up figure it out later
                h2OfferTitle.InnerText = "Offer Details: " + gvr.Cells[1].Text.Replace ("&nbsp;", "");
                tbx_PromoID.Text = gvr.Cells[0].Text.Replace("&nbsp;", "");
                tbx_BundleTitle.Text = gvr.Cells[1].Text.Replace("&nbsp;", "");
                txtPromoStartDate.Text = startDate.ToShortDateString();
                txtPromoEndDate.Text = endDate.ToShortDateString();
                drpPromoStartTime.SelectedValue = startDate.ToString("HH:mm:ss");
                drpPromoEndTime.SelectedValue = endDate.ToString("HH:mm:ss");
                tbx_SubheadLine1.Text = gvr.Cells[4].Text.Replace("&nbsp;", "");
                tbx_SubheadLine2.Text = gvr.Cells[5].Text.Replace("&nbsp;", "");
                tbx_Price.Text = gvr.Cells[6].Text.Replace("&nbsp;", "");
                tbx_Credits.Text = gvr.Cells[7].Text.Replace("&nbsp;", "");
                tbx_PromotionListHeading.Text = gvr.Cells[8].Text.Replace("&nbsp;", "");
                tbx_PromotionDescr.Text = gvr.Cells[9].Text.Replace("&nbsp;", "");
                tbx_HighlightColor.Text = gvr.Cells[10].Text.Replace("&nbsp;", "");
                tbx_BackgroundColor.Text = gvr.Cells[11].Text.Replace("&nbsp;", "");
                inp_backgroundImage.Value = gvr.Cells[12].Text.Replace("&nbsp;", "");
                inp_stickerImage.Value = gvr.Cells[13].Text.Replace("&nbsp;", "");
                tbx_PromotionPackageLabel.Text = gvr.Cells[14].Text.Replace("&nbsp;", "");
                ddl_KEICreditType.SelectedValue = gvr.Cells[15].Text.Replace("&nbsp;", "");
                tbx_FreeAwardPoints.Text = gvr.Cells[16].Text.Replace("&nbsp;", "");
                ddl_FreePointType.SelectedValue = gvr.Cells[17].Text.Replace("&nbsp;", "");
                ddl_PromoOfferType.SelectedValue = gvr.Cells[18].Text.Replace("&nbsp;", "");
                ddl_PassGroupType.SelectedValue = gvr.Cells[19].Text.Replace("&nbsp;", "");
                tbx_sku.Text = gvr.Cells[20].Text.Replace("&nbsp;", "");
                tbx_CouponCode.Text = gvr.Cells[21].Text.Replace("&nbsp;", "");
                tbx_ValueOfCredits.Text = gvr.Cells[22].Text.Replace("&nbsp;", "");
                tbx_FontColor.Text = gvr.Cells[23].Text.Replace("&nbsp;", "");
                tbx_adRotatorPath.Text = gvr.Cells[26].Text.Replace("&nbsp;", "");

                //loads the original price for marketing purposes - if the original price is lower than the true price reset it here
                try
                {
                    tbx_OriginalPrice.Text = gvr.Cells[25].Text.Replace("&nbsp;", "");
                    if (Convert.ToDecimal(tbx_OriginalPrice.Text) < Convert.ToDecimal(tbx_Price.Text))
                    {
                        tbx_OriginalPrice.Text = tbx_Price.Text;
                    }
                }
                catch { }

                //make sure the business credit to dollar ratio us used. Forces the use of the standard since in past no one has set this field
                try
                {
                    double dbValueOfCredits = Convert.ToDouble(tbx_ValueOfCredits.Text != "" ? tbx_ValueOfCredits.Text : "0");
                    int valueOfCredits = Configuration.Credit2DollarRatio;

                    if (((int)dbValueOfCredits) != valueOfCredits)
                    {
                        tbx_ValueOfCredits.Text = valueOfCredits.ToString();
                    }
                }
                catch (Exception) {}

                try
                {
                    cbx_UseOptions.Checked = Convert.ToInt32(gvr.Cells[24].Text.Replace("&nbsp;", "")) == 1 ? true : false;
                }
                catch { }

                string bgColor = string.Empty;
                    
                try
                {
                    bgColor = tbx_HighlightColor.Text.Trim().Equals(string.Empty) ? "" : tbx_HighlightColor.Text;
                    tbx_HighlightColorAppClrPickerStr.Style.Add ("background-color", bgColor);
                }
                catch { }
                try
                {
                    bgColor = tbx_BackgroundColor.Text.Trim ().Equals (string.Empty) ? "" : tbx_BackgroundColor.Text;
                    tbx_BackgroundColorAppClrPickerStr.Style.Add ("background-color", bgColor);
                }
                catch { }
                try
                {
                    bgColor = tbx_FontColor.Text.Trim ().Equals (string.Empty) ? "" : tbx_FontColor.Text;
                    tbx_FontColorAppClrPickerStr.Style.Add ("background-color", bgColor);
                }
                catch { }
            }
            catch (Exception ex)
            {
                // Display err message
                cvBlank.IsValid = false;
                cvBlank.ErrorMessage = "Error displaying promotion detials: " + ex.Message;
                valSum.Style.Add ("display", "block");
                m_logger.Error ("Error displaying promotion detials. ", ex);
                return;
            }

            try
            {
                //query WOK items
                DataTable wokItems = StoreUtility.GetPromotionalOfferItems(Convert.ToInt32(tbx_PromoID.Text));
                BindWOKItemData(wokItems);
                //ensure WOK ITems edit panel intially invisible
                pnl_PromotionsWOKItemsAdd.Visible = false;
            }
            catch (Exception ex)
            {
                // Display err message
                cvBlank.IsValid = false;
                cvBlank.ErrorMessage = "Unable to retrieve WOK items: " + ex.Message;
                valSum.Style.Add ("display", "block");
                m_logger.Error ("Unable to retrieve WOK items. ", ex);
            }

            //get the subscription if there is one
            try
            {
                switch (Convert.ToInt32(ddl_PromoOfferType.SelectedValue))
                {
                    case (int)Promotion.ePROMOTION_TYPE.CREDITS:
                    case (int)Promotion.ePROMOTION_TYPE.REFERRAL:
                    case (int)Promotion.ePROMOTION_TYPE.SPECIAL:
                    case (int)Promotion.ePROMOTION_TYPE.SPECIAL_FIRST_TIME:
                    case (int)Promotion.ePROMOTION_TYPE.SPECIAL_ONE_TIME:
                        break;
                    case (int)Promotion.ePROMOTION_TYPE.SUBSCRIPTION_PASS:
                    case (int)Promotion.ePROMOTION_TYPE.MATURE_SUBSCRIPTION_PASS:
                        //show and configure the subscription fields
                        ProcessSubscriptions();
                        break;
                }
            }
            catch (Exception ex)
            {
                // Display err message
                cvBlank.IsValid = false;
                cvBlank.ErrorMessage = "Unable to retrieve subscription data: " + ex.Message;
                valSum.Style.Add("display", "block");
                m_logger.Error("Unable to retrieve subscription data. ", ex);
            }


        }

        protected void dg_Promotions_PageIndexChanging(object source, System.Web.UI.WebControls.GridViewPageEventArgs e)
        {
            currentPage = e.NewPageIndex;
            BindPromotionalData(currentPage);
        }

        protected void dg_Promotions_Sorting(Object sender, GridViewSortEventArgs e)
        {
            SortSwitch(e.SortExpression); //sets the sort expression
            BindPromotionalData(currentPage);
        }

        protected void dg_PromotionsmyGrid_OnRowCreated(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            //logic check to work around initial row creation wheere there is only one column
            if (e.Row.Cells.Count > 10)
            {
                e.Row.Cells[0].Visible = false;
                e.Row.Cells[4].Visible = false;
                e.Row.Cells[5].Visible = false;
                e.Row.Cells[7].Visible = false;
                
                e.Row.Cells[8].Visible = false;
                e.Row.Cells[10].Visible = false;
                e.Row.Cells[11].Visible = false;
                e.Row.Cells[12].Visible = false;
                e.Row.Cells[13].Visible = false;
                e.Row.Cells[14].Visible = false;
                e.Row.Cells[15].Visible = false;
                e.Row.Cells[16].Visible = false;
                e.Row.Cells[17].Visible = false;
                e.Row.Cells[18].Visible = false;
                e.Row.Cells[19].Visible = false;
                e.Row.Cells[20].Visible = false;
                e.Row.Cells[21].Visible = false;
                e.Row.Cells[22].Visible = false;
             //   e.Row.Cells[23].Visible = false;
                e.Row.Cells[24].Visible = false;
                e.Row.Cells[25].Visible = false;
                e.Row.Cells[26].Visible = false;
            }

        }

        protected void dg_PromotionalItems_RowEditing(object sender, System.Web.UI.WebControls.GridViewEditEventArgs e)
        {
            pnl_PromotionsWOKItemsAdd.Visible = true;

            DataRow gvr2 = promoItemsDT.Rows[e.NewEditIndex];
            selectedItemIndex.Text = e.NewEditIndex.ToString();

            btn_ItemsAdd.Text = "Update";

            tbx_promoofferid.Text = gvr2["promotion_offer_id"].ToString();
            tbx_promoidWOK.Text = gvr2["promotion_id"].ToString();
            tbx_itemquantity.Text = gvr2["quantity"].ToString();
            lbl_wokitemid.Text = gvr2["wok_item_id"].ToString();
            lbl_itemdesciption.Text = gvr2["item_description"].ToString();
            tbx_alternatedesciption.Text = gvr2["alternate_description"].ToString();
            ddl_gender.SelectedValue = gvr2["gender"].ToString();
            ddl_giftcards.SelectedValue = gvr2["gift_card_id"].ToString();
            lbl_communityId.Text = gvr2["community_id"].ToString();
            tbx_marketprice.Text = gvr2["market_price"].ToString();

            //this is strictly for the user may remove later for performance reasons
            if (lbl_communityId.Text != "")
            {
                try
                {
                    DataRow dr = CommunityUtility.GetCommunity(Convert.ToInt32(gvr2["community_id"].ToString()));
                    lbl_community.Text = dr["name"].ToString();
                }
                catch (Exception)
                {
                }
            }
        }

        protected void dg_PromotionalItems_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            DataTable dt = promoItemsDT;
            dt.Rows[e.RowIndex].Delete();
            BindWOKItemData(dt);
        }

        protected void btn_ItemsCancel_Click(object sender, System.EventArgs e)
        {
            //clear the fields
            ClearPromotionItems();

            WireJavaScript();

            //hide fields again
            pnl_PromotionsWOKItemsAdd.Visible = false;
        }

        protected void btn_ItemsAdd_Click(object sender, System.EventArgs e)
        {
            WireJavaScript();
            
            //set error message variable
            string error = "";
            descriptionRequired.Visible = false;
            itemIDRequired.Visible = false;
            quantityRequired.Visible = false;

            //validate the required fields
            //validate the required fields
            if (tbx_itemquantity.Text == "")
            {
                error += "Quantity is required. ";
                quantityRequired.Visible = true;
            }
            if (lbl_wokitemid.Text == "")
            {
                error += "An item is required. ";
                itemIDRequired.Visible = true;
            }
            if (lbl_itemdesciption.Text == "")
            {
                error += "Description is required. ";
                descriptionRequired.Visible = true;
            }

            if (error.Length > 0)
            {
                // Display err message
                cvBlank.IsValid = false;
                cvBlank.ErrorMessage = error;
                valSumItems.Style.Add ("display", "block");
                return;
            }

            try
            {
                //get data source
                DataTable dt = promoItemsDT;
                if (dt.Columns.Count <= 0)
                {
                    //dt = new DataTable();
                    dt.Columns.Add("promotion_offer_id");
                    dt.Columns.Add("promotion_id");
                    dt.Columns.Add("quantity");
                    dt.Columns.Add("wok_item_id");
                    dt.Columns.Add("item_description");
                    dt.Columns.Add("alternate_description");
                    dt.Columns.Add("gender");
                    dt.Columns.Add("gift_card_id");
                    dt.Columns.Add("community_id");
                    dt.Columns.Add("market_price");
                }

                if (btn_ItemsAdd.Text.ToUpper().Equals("ADD"))
                {
                    error = "Error During Add: ";

                    DataRow newRow = dt.NewRow();
                    //set default values
                    newRow["promotion_offer_id"] = Convert.ToInt32(tbx_promoofferid.Text);
                    newRow["promotion_id"] = Convert.ToInt32(tbx_promoidWOK.Text);
                    newRow["quantity"] = Convert.ToInt32(tbx_itemquantity.Text);
                    newRow["wok_item_id"] = Convert.ToInt32(lbl_wokitemid.Text);
                    newRow["item_description"] = lbl_itemdesciption.Text;
                    newRow["alternate_description"] = tbx_alternatedesciption.Text;
                    newRow["gender"] = ddl_gender.SelectedValue;
                    newRow["gift_card_id"] = ddl_giftcards.SelectedValue;
                    newRow["community_id"] = (lbl_communityId.Text.Equals("") ? "0" : lbl_communityId.Text);
                    newRow["market_price"] = (tbx_marketprice.Text.Equals("") ? "0" : tbx_marketprice.Text);

                    dt.Rows.Add(newRow);
                }
                else
                {
                    error = "Error During Update: ";

                    DataRow row = dt.Rows[Convert.ToInt32(selectedItemIndex.Text)];

                    row["promotion_offer_id"] = Convert.ToInt32(tbx_promoofferid.Text);
                    row["promotion_id"] = Convert.ToInt32(tbx_promoidWOK.Text);
                    row["quantity"] = Convert.ToInt32(tbx_itemquantity.Text);
                    row["wok_item_id"] = Convert.ToInt32(lbl_wokitemid.Text);
                    row["item_description"] = lbl_itemdesciption.Text;
                    row["alternate_description"] = tbx_alternatedesciption.Text;
                    row["gender"] = ddl_gender.SelectedValue;
                    row["gift_card_id"] = ddl_giftcards.SelectedValue;
                    row["community_id"] = (lbl_communityId.Text.Equals("") ? "0" : lbl_communityId.Text);
                    row["market_price"] = (tbx_marketprice.Text.Equals("") ? "0" : tbx_marketprice.Text);
                }
                
                BindWOKItemData(dt);
                
                //clear the fields
                ClearPromotionItems();

                //hide fields again
                pnl_PromotionsWOKItemsAdd.Visible = false;
            }
            catch (Exception ex)
            {
                // Display err message
                cvBlank.IsValid = false;
                cvBlank.ErrorMessage = error + ex.Message;
                valSumItems.Style.Add ("display", "block");
                m_logger.Error ("Promotion Management - " + error, ex);
            }
        }

        protected void tbx_Credits_Changed(object sender, System.EventArgs e)
        {
            try
            {
                //pull offered credits and value of credits
                int credsPerDollar = (int)Convert.ToDouble(tbx_ValueOfCredits.Text);
                double credits = Convert.ToDouble(tbx_Credits.Text);
                double freeCredits = 0.0;

                //this part accounts for free award credits if they are KPOINTS
                if(ddl_FreePointType.SelectedValue == "KPOINT")
                {
                    freeCredits = Convert.ToDouble(tbx_FreeAwardPoints.Text);
                    credits = credits + freeCredits;
                }

                tbx_OriginalPrice.Text = Convert.ToString(Math.Round((credits / credsPerDollar), 2));
            }
            catch(Exception)
            {
                cvBlank.ErrorMessage = "Error: please check your Price/Credit values!";
            }
        }

        protected void ddl_PromoOfferType_Changed(object sender, System.EventArgs e)
        {
            try
            {
                int selectedPromoType = Convert.ToInt32(ddl_PromoOfferType.SelectedValue);

                switch (selectedPromoType)
                {
                    case (int)Promotion.ePROMOTION_TYPE.CREDITS:
                    case (int)Promotion.ePROMOTION_TYPE.REFERRAL:
                    case (int)Promotion.ePROMOTION_TYPE.SPECIAL:
                    case (int)Promotion.ePROMOTION_TYPE.SPECIAL_FIRST_TIME:
                    case (int)Promotion.ePROMOTION_TYPE.SPECIAL_ONE_TIME:
                        //hide the subscription fields
                        tr_SubscriptionData.Visible = false;
                        ClearSubscriptionItems();
                        break;
                    case (int)Promotion.ePROMOTION_TYPE.MATURE_SUBSCRIPTION_PASS:
                    case (int)Promotion.ePROMOTION_TYPE.SUBSCRIPTION_PASS:
                        this.ProcessSubscriptions();
                        tr_SubscriptionData.Visible = true;
                        break;
                }
            }
            catch(Exception ex)
            {
                // Display err message
                cvBlank.IsValid = false;
                cvBlank.ErrorMessage = "Unable to process promotion type: " + ex.Message;
                valSum.Style.Add ("display", "block");
                m_logger.Error ("Unable to process promotion type. ", ex);

            }
        }

        protected void btn_Save_Click(object sender, System.EventArgs e)
        {
            if (!Page.IsValid)
            {
                return;
            }
            DateTime startDate;
            DateTime endDate;
            
            //grab WOK Items data source
            DataTable changes = promoItemsDT.GetChanges();
            bool errorsOccured = false;
            int promotionalID = -1;
            string skuNumber = tbx_sku.Text.Equals("") ? "0" : tbx_sku.Text;

            //validate start and end dates
            try
            {
                startDate = KanevaGlobals.UnFormatDate(txtPromoStartDate.Text + " " + drpPromoStartTime.SelectedValue);
                endDate = KanevaGlobals.UnFormatDate(txtPromoEndDate.Text + " " + drpPromoEndTime.SelectedValue);

                if (startDate.CompareTo(endDate) > 0)
                {
                    throw new Exception("Start date cannot be after the end date.");
                }
            }
            catch(Exception ex)
            {
                // Display err message
                cvBlank.IsValid = false;
                cvBlank.ErrorMessage = "Error Saving Your Promotion: " + ex.Message;
                valSum.Style.Add ("display", "block");
                return;
            }

            try
            {
                //grab all data for the promotion and update database
                promotionalID = Convert.ToInt32(tbx_PromoID.Text);
                if (promotionalID > 0)
                {
                    //promotionalID = Convert.ToInt32(this.tbx_PromoID.Text);
                    StoreUtility.UpdatePromotionalOffers(promotionalID, tbx_BundleTitle.Text, tbx_SubheadLine1.Text, tbx_SubheadLine2.Text, Convert.ToDecimal(tbx_Price.Text),
                        Convert.ToDecimal(tbx_Credits.Text), startDate, endDate, tbx_PromotionListHeading.Text, 
                        tbx_PromotionDescr.Text, tbx_HighlightColor.Text, tbx_BackgroundColor.Text, inp_backgroundImage.Value, inp_stickerImage.Value, tbx_PromotionPackageLabel.Text,
                        ddl_KEICreditType.SelectedValue, Convert.ToDecimal(tbx_FreeAwardPoints.Text), ddl_FreePointType.SelectedValue,
                        Convert.ToInt32(ddl_PromoOfferType.SelectedValue), Convert.ToInt32(ddl_PassGroupType.SelectedValue), Convert.ToInt32(skuNumber), Convert.ToDecimal(tbx_ValueOfCredits.Text),
                        tbx_FontColor.Text, tbx_CouponCode.Text, GetUserId(), cbx_UseOptions.Checked, Convert.ToDecimal(tbx_OriginalPrice.Text), tbx_adRotatorPath.Text);
                }
                else
                {
                    promotionalID = StoreUtility.InsertPromotionalOffers(tbx_BundleTitle.Text, tbx_SubheadLine1.Text, tbx_SubheadLine2.Text, Convert.ToDecimal(tbx_Price.Text),
                        Convert.ToDecimal(tbx_Credits.Text), startDate, endDate, tbx_PromotionListHeading.Text,
                        tbx_PromotionDescr.Text, tbx_HighlightColor.Text, tbx_BackgroundColor.Text, inp_backgroundImage.Value, inp_stickerImage.Value, tbx_PromotionPackageLabel.Text,
                        ddl_KEICreditType.SelectedValue, Convert.ToDecimal(tbx_FreeAwardPoints.Text), ddl_FreePointType.SelectedValue,
                        Convert.ToInt32(ddl_PromoOfferType.SelectedValue), Convert.ToInt32(ddl_PassGroupType.SelectedValue), Convert.ToInt32(skuNumber), Convert.ToDecimal(tbx_ValueOfCredits.Text),
                        tbx_FontColor.Text, tbx_CouponCode.Text, GetUserId(), cbx_UseOptions.Checked, Convert.ToDecimal(tbx_OriginalPrice.Text), tbx_adRotatorPath.Text);
                }

                if (promotionalID <= 0)
                {
                    throw new Exception();
                }

            }
            catch (Exception ex)
            {
                // Display err message
                cvBlank.IsValid = false;
                cvBlank.ErrorMessage = "Error Saving Your Promotion: " + ex.Message;
                valSum.Style.Add ("display", "block");
                return;
            }
           
            //write changes in WOK items to database
            if (changes != null)
            {
                //enumeration shouldn't cause over head issues due to low concurrent users
                foreach (DataRow dr in changes.Rows)
                {
                    //decide which database operation to do
                    switch (dr.RowState.ToString().ToUpper())
                    {
                        case "ADDED":
                            try
                            {

                                StoreUtility.InsertPromotionalOfferItems(promotionalID,
                                    Convert.ToInt32(dr["quantity"]), Convert.ToInt32(dr["wok_item_id"]),
                                    dr["item_description"].ToString(), dr["gender"].ToString(),
                                    Convert.ToInt32(dr["gift_card_id"]), Convert.ToInt32(dr["market_price"]), Convert.ToInt32(dr["community_id"]), dr["alternate_description"].ToString(), GetUserId());
                            }
                            catch (Exception ex)
                            {
                                errorsOccured = true;
                                // Display err message
                                cvBlank.IsValid = false;
                                cvBlank.ErrorMessage = "Error Saving New Promotional Items Records: " + ex.Message;
                                valSum.Style.Add ("display", "block");
                            }
                            break;
                        case "DELETED":
                            try
                            {
                                dr.RejectChanges(); //this is used to un delete the row so that we can retreive the id
                                StoreUtility.DeletePromotionalOfferItem(Convert.ToInt32(dr["promotion_offer_id"]));
                            }
                            catch (Exception ex)
                            {
                                errorsOccured = true;
                                // Display err message
                                cvBlank.IsValid = false;
                                cvBlank.ErrorMessage = "Error Deleting Record Update: " + ex.Message;
                                valSum.Style.Add ("display", "block");
                            }
                            break;
                        case "MODIFIED":
                            try
                            {
                                StoreUtility.UpdatePromotionalOfferItems(Convert.ToInt32(dr["promotion_offer_id"]),
                                    Convert.ToInt32(dr["quantity"]), Convert.ToInt32(dr["wok_item_id"]),
                                    dr["item_description"].ToString(), dr["gender"].ToString(),
                                    Convert.ToInt32(dr["gift_card_id"]), Convert.ToInt32(dr["market_price"]), Convert.ToInt32(dr["community_id"]), dr["alternate_description"].ToString(), GetUserId());
                            }
                            catch (Exception ex)
                            {
                                errorsOccured = true;
                                // Display err message
                                cvBlank.IsValid = false;
                                cvBlank.ErrorMessage = "Error Updating Record: " + ex.Message;
                                valSum.Style.Add ("display", "block");
                            }
                            
                            break;
                    }
                }
            }

            if (tr_SubscriptionData.Visible)
            {
                //delete all existing pairings if existing
                (new SubscriptionFacade()).DeleteAllSubscription2PromotionByPromoID(promotionalID);

                //add whatever is in the collection
                foreach (string key in subscription2Promo.Keys)
                {
                    int subscriptionId = Convert.ToInt32(key);
                    bool inSet = Convert.ToBoolean(subscription2Promo[key].ToLower());

                    if (inSet)
                    {
                        //write to the promotion to subscription table
                        (new SubscriptionFacade()).InsertSubscription2Promotion(promotionalID, subscriptionId);

                        //if the subscription has any mature pass types mark this promotion mature
                        DataTable passes = (new SubscriptionFacade()).GetPassGroupsAssociatedWSubscription((uint)subscriptionId);
                        foreach (DataRow row in passes.Rows)
                        {
                            switch (Convert.ToInt32(row["pass_group_id"]))
                            {
                                case (int)Constants.ePASS_TYPE.ACCESS:

                                       StoreUtility.UpdatePromotionalOffers(promotionalID, tbx_BundleTitle.Text, tbx_SubheadLine1.Text, tbx_SubheadLine2.Text, Convert.ToDecimal(tbx_Price.Text),
                                       Convert.ToDecimal(tbx_Credits.Text), startDate, endDate, tbx_PromotionListHeading.Text,
                                       tbx_PromotionDescr.Text, tbx_HighlightColor.Text, tbx_BackgroundColor.Text, inp_backgroundImage.Value, inp_stickerImage.Value, tbx_PromotionPackageLabel.Text,
                                       ddl_KEICreditType.SelectedValue, Convert.ToDecimal(tbx_FreeAwardPoints.Text), ddl_FreePointType.SelectedValue,
                                       (int)Promotion.ePROMOTION_TYPE.MATURE_SUBSCRIPTION_PASS, (int)Constants.ePASS_TYPE.ACCESS, Convert.ToInt32(skuNumber), Convert.ToDecimal(tbx_ValueOfCredits.Text),
                                       tbx_FontColor.Text, tbx_CouponCode.Text, GetUserId(), cbx_UseOptions.Checked, Convert.ToDecimal(tbx_OriginalPrice.Text), tbx_adRotatorPath.Text);

                                    break;
                            }
                        }
                    }
                }
            }

            //clear the fields
            ClearSubscriptionItems();

            //clear the fields
            ClearPromotionData();

            //reset viewstate
            resetViewState();

            //pulledsaved info from database
            BindPromotionalData(currentPage);

            //make the details panel visible
            divPromotionsDetails.Style.Add ("display", "none");

            if (!errorsOccured)
            {
//                ErrorMessages.Text = "Save successful!";               // ****************************
//                ErrorMessages.ForeColor = Color.DarkGreen;
            }



        }

        #endregion
    }
}
