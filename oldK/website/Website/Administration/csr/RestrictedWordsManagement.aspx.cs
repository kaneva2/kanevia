///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using log4net;
using MagicAjax;

namespace KlausEnt.KEP.Kaneva.csr
{
    public partial class RestrictedWordsManagement : NoBorderPage
    {
        protected RestrictedWordsManagement () 
		{
            Title = "CSR - Restricted Words Management";
        }
       
        protected void Page_Load (object sender, EventArgs e)
        {
            // Redirect if user is not admin
            if (!IsCSR ())
            {
                Response.Redirect ("~/default.aspx");
            }

            if (!IsPostBack)
            {
                //bind the promotions
                BindRestrictedData (currentPage);

                //bind pulldowns
                PopulateRestrictionTypeList ();
            }
        }


        #region Helper Methods

        /// <summary>
        /// BindRestrictedData
        /// </summary>
        private void BindRestrictedData (int currentPage)
        {
            try
            {
                lblErrMessage.Visible = false;
                
                // Set the sortable columns
                string orderby = CurrentSort + " " + CurrentSortOrder;
                int pageSize = 50;

                //query the database for existing promotions
                PagedDataTable pdt = StoreUtility.GetRestrictedWords ("", orderby, currentPage, pageSize);

                if ((pdt != null) && (pdt.TotalCount > 0))
                {
                    lblNoRestrictions.Visible = false;

                    dgWordList.DataSource = pdt;
                    dgWordList.DataBind ();

                    // Show Pager
                    pgWordList.NumberOfPages = Math.Ceiling ((double) pdt.TotalCount / pageSize).ToString ();
                    pgWordList.DrawControl ();
                }
                else
                {
                    lblNoRestrictions.Visible = true;
                }
            }
            catch (Exception ex)
            {
                lblErrMessage.Text = "Error displaying restricted words list: " + ex.Message;
                lblErrMessage.Visible = true;
            }
        }

        //allows user to switch the ordering between DESC and ASC
        //based on column clicked
        private void SortSwitch (string newSort)
        {
            if (CurrentSort == newSort)
            {
                if (CurrentSortOrder == "DESC")
                {
                    CurrentSortOrder = "ASC";
                }
                else
                {
                    CurrentSortOrder = "DESC";
                }
            }
            else
            {
                CurrentSort = newSort;
                CurrentSortOrder = "DESC";
            }
        }

        /// <summary>
        /// Populate the Restricted Type drop down list
        /// </summary>
        private void PopulateRestrictionTypeList ()
        {
            ddlRestrictionType.DataSource = StoreUtility.GetRestrictionTypes ();
            ddlRestrictionType.DataTextField = "name";
            ddlRestrictionType.DataValueField = "restriction_type_id";
            ddlRestrictionType.DataBind ();
        }

        /// <summary>
        /// Clear the Restricted Data edit fields
        /// </summary>
        private void ClearRestrictionData ()
        {
            txtWord.Text = "";
            ddlRestrictionType.SelectedIndex = 0;
            ddlMatchType.SelectedIndex = 0;
            txtReplace.Text = "";

            lblErrMessage.Visible = false;
        }

        /// <summary>
        /// Get the Restricted Word Data
        /// </summary>
        private void GetRestrictedWordDetails (GridViewRow gvr, int pageNumber)
        {
            //make the details panel visible
            AjaxCallHelper.Write ("$('divRestrictionDetails').style.display='block';");

            try
            {
                txtWord.Text = gvr.Cells[1].Text;
                SetDropDownIndex (ddlRestrictionType, gvr.Cells[3].Text); 
                SetDropDownIndex (ddlMatchType, gvr.Cells[4].Text);
                txtReplace.Text = Server.HtmlDecode(gvr.Cells[5].Text).Trim();
                txtRestrictedWordId.Text = gvr.Cells[0].Text;

                lblErrMessage.Visible = false;
            }
            catch (Exception ex)
            {
                lblErrMessage.Text = "Error displaying restricted word details: " + ex.Message;
                lblErrMessage.Visible = true;
            }
        }

        /// <summary>
        /// Used to clear the cache for the restriction type that was updated
        /// </summary>
        private void ClearCache (int restrictionType, string matchType)
        {
            // Clear the DataTable of restricted words
            WebCache.ClearCacheValue (WebCache.cRESTWORDS);
            
            // Based on the restriction type and match type, clear the correct word string from cache
            if (restrictionType == (int) Constants.eRESTRICTION_TYPE.POTTY_MOUTH)
            {
                if (matchType == Constants.eMATCH_TYPE.EXACT.ToString())
                {
                    WebCache.ClearCacheValue (WebCache.cPMEXACT);
                }
                else
                {
                    WebCache.ClearCacheValue (WebCache.cPMANY);
                }
            }
            else if (restrictionType == (int) Constants.eRESTRICTION_TYPE.RESERVED)
            {
                if (matchType == Constants.eMATCH_TYPE.EXACT.ToString())
                {
                    WebCache.ClearCacheValue (WebCache.cRESTEXACT);
                }
                else
                {
                    WebCache.ClearCacheValue (WebCache.cRESTANY);
                }
            }
        }   

        #endregion Helper Methods


        #region Event Handlers

        /// <summary>
        /// Execute when the user selects a delete link from the data grid 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgWordList_RowDeleting (object source, GridViewDeleteEventArgs e)
        {
            try
            {
                // Get the selected row
                GridViewRow gvr = dgWordList.Rows[e.RowIndex];

                // Delete the record and rebind
                StoreUtility.DeleteRestrictedWord (Convert.ToInt32 (gvr.Cells[0].Text));
                
                // Clear the cache
                ClearCache (Convert.ToInt32(gvr.Cells[3].Text), gvr.Cells[4].Text.ToUpper());

                pgWordList.CurrentPageNumber = currentPage;
                BindRestrictedData (currentPage);
            }
            catch (Exception ex)
            {
                lblErrMessage.Text = "Error deleting restricted word: " + ex.Message;
                lblErrMessage.Visible = true;
            }
        }

        /// <summary>
        /// Execute when each row of the data grid is created
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgWordList_RowCreated (object sender, GridViewRowEventArgs e)
        {

            // The GridViewCommandEventArgs class does not contain a 
            // property that indicates which row's command button was
            // clicked. To identify which row's button was clicked, use 
            // the button's CommandArgument property by setting it to the 
            // row's index.
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // Set the visibility of the restriction_type_id field to false.  The reason we do this is
                // if the column is set to visible=false in the aspx code, the data is not bound to that
                // column.  So, we bind the data to the datagrid then set the visibility of this column to false
                // when each row of the grid is created.
                e.Row.Cells[3].Visible = false;
            }

        }

        /// <summary>
        /// Execute when the user selects a header link from the data grid to sort a by a column
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgWordList_Sorting (object source, GridViewSortEventArgs e)
        {
            SortSwitch (e.SortExpression); //sets the sort expression
            BindRestrictedData (currentPage);
        }

        /// <summary>
        /// Execute when the user selects a page change link from the Pager control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void pgWordList_PageChange (object sender, PageChangeEventArgs e)
        {
            BindRestrictedData (e.PageNumber);
        }

        /// <summary>
        /// Execute when the user selects Edit link from the grid view
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgWordList_RowEditing (object source, GridViewEditEventArgs e)
        {
            //passin the selected row and populate the fields
            GetRestrictedWordDetails (dgWordList.Rows[e.NewEditIndex], 1);
        }

        /// <summary>
        /// Execute when the user selects Save button to save changes to restricted word
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void btnSave_Click (object source, EventArgs e)
        {
            try
            {
                // Make sure word text box is not empty
                if (txtWord.Text.Trim ().Length < 1)
                {
                    lblErrMessage.Text = "You must enter a word to be restricted";
                    lblErrMessage.Visible = false;
                    return;
                }


                // If ID is -1, this is a new word
                if (Convert.ToInt32 (txtRestrictedWordId.Text) < 0)
                {
                    StoreUtility.InsertRestrictedWord (txtWord.Text.Trim ().ToUpper (), txtReplace.Text.Trim (), 
                        Convert.ToInt32 (ddlRestrictionType.SelectedValue), ddlMatchType.SelectedValue);
                }
                else  // Updating existing word
                {
                    StoreUtility.UpdateRestrictedWord (Convert.ToInt32 (txtRestrictedWordId.Text), txtWord.Text.Trim ().ToUpper (),
                        txtReplace.Text.Trim (), Convert.ToInt32 (ddlRestrictionType.SelectedValue), ddlMatchType.SelectedValue);
                }

                //make the details panel hidden
                AjaxCallHelper.Write ("$('divRestrictionDetails').style.display='none';");

                currentPage = 1;
                pgWordList.CurrentPageNumber = currentPage;
                BindRestrictedData (currentPage);
            }
            catch (Exception ex)
            {
                lblErrMessage.Text = "Error saving restricted word : " + ex.Message;
                lblErrMessage.Visible = true;
            }
        }

        /// <summary>
        /// Execute when the user selects Add New Word link 
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void lbNewWord_Click (object sender, EventArgs e)
        {
            //clear the fields
            ClearRestrictionData ();

            //make the details panel visible
            AjaxCallHelper.Write ("$('divRestrictionDetails').style.display='block';");

            //set id to -1 <indicator that it is a new entry>
            txtRestrictedWordId.Text = "-1";
        }

        #endregion Event Handlers


        #region Properties

        /// <summary>
        /// Current Page Number
        /// </summary>
        /// <returns></returns>
        private int currentPage
        {
            get
            {
                if (ViewState["_currentPage"] == null)
                {
                    ViewState["_currentPage"] = 0;
                }
                return (int) ViewState["_currentPage"];
            }
            set
            {
                ViewState["_currentPage"] = value;
            }
        }

        /// <summary>
        /// DEFAULT_SORT
        /// </summary>
        /// <returns></returns>
        protected override string DEFAULT_SORT_ORDER
        {
            get
            {
                return "ASC";
            }
        }

        /// <summary>
        /// DEFAULT_SORT
        /// </summary>
        /// <returns></returns>
        protected override string DEFAULT_SORT
        {
            get
            {
                return "name, word";
            }
        }
        
        #endregion Properties


        #region Declarations

        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

        #endregion Declerations
    }
}
