<%@ Control Language="c#" AutoEventWireup="false" Codebehind="csrNav.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.csr.csrNav" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<table width="100%" CellPadding="0" CellSpacing="0" Border="0">
	<tr>
		<td bgcolor="#eeeeff" style="PADDING-RIGHT: 4px; PADDING-LEFT: 4px; FONT-SIZE: 10px; PADDING-BOTTOM: 7px; WORD-SPACING: normal; COLOR: #000000; LINE-HEIGHT: 10px; PADDING-TOP: 7px; FONT-STYLE: normal; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; LETTER-SPACING: normal; TEXT-DECORATION: none">
			&nbsp;<a href="~/free-virtual-world.kaneva" class="t4" style="CURSOR:hand" runat="server" id="aHome">Home Page</a> | 
				<a href="~/Administration/csr/userSearch.aspx" class="t4" style="CURSOR:hand" runat="server" id="aUserSearch">User Search</a> | 
				<a href="~/Administration/csr/games.aspx" class="t4" style="CURSOR:hand" runat="server" id="aGames">Games</a> | 
				<a href="~/Administration/csr/items.aspx" class="t4" style="CURSOR:hand" runat="server" id="aItems">Items</a> | 
				<a href="~/Administration/csr/Contest.aspx" class="t4" style="CURSOR:hand" runat="server" id="aContests">Contest Admin.</a> | 
				<a href="~/Administration/csr/PromotionManagement.aspx" class="t4" style="CURSOR:hand" runat="server" id="aPromotions">Promotions Mgmt</a> | 
				<a href="~/Administration/csr/SubscriptionManagement.aspx" class="t4" style="CURSOR:hand" runat="server" id="a1">Subscription Mgmt</a> | 
				<a href="~/Administration/csr/nameChanges.aspx" class="t4" style="CURSOR:hand" runat="server" id="aNameChange">Name Changes</a> |
				<a href="~/Administration/csr/bannedips.aspx" class="t4" style="CURSOR:hand" runat="server" id="aBannedIPs">Banned IPs</a> |
				<a href="~/Administration/csr/RestrictedWordsManagement.aspx" class="t4" style="CURSOR:hand" runat="server" id="aRestricted">Restricted Words</a> | 
				<a href="~/Administration/csr/RewardsManagement.aspx" class="t4" style="CURSOR:hand" runat="server" id="aRewards">Rewards Mgmt</a>
		</td>
	</tr>
</table>
