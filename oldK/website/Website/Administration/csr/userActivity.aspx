<%@ Page language="c#" Codebehind="userActivity.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.CSR.userActivity" %>
<%@ Register TagPrefix="Kaneva" TagName="CSRNav" Src="csrNav.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="AdminMenu" Src="../../usercontrols/AdministrativeMenu.ascx" %>

<Kaneva:AdminMenu ID="adminmenu" runat="server" />
<asp:ValidationSummary ShowMessageBox="False" ShowSummary="True" Class="formError" id="valSum" DisplayMode="BulletList" runat="server" HeaderText="Please correct the following errors:"/>

<table runat="server" id="tblNoCSR" cellpadding="0" cellspacing="0" border="0" width="560">
	<tr><td ALIGN="CENTER">
		<BR><BR><BR>
		<span class="subHead">You must be a CSR to access this section.</span>
	</td></tr>
</table>

<asp:panel runat="server" ID="pnlCSR">

<Kaneva:CSRNav runat="server" id="csrNavigation" SubTab="us"/>
<br>
<span class="assetLink"><asp:label runat="server" id="lblWho"/> Activity</span><br><bR>
<span  class="dateStamp">
&nbsp;&nbsp;<a runat="server" class="dateStamp" href="javascript: pageFlip('Activity_Posts.aspx')">Posts</a> (<asp:label runat="server" id="lblPosts"/>)&nbsp;&nbsp;<a runat="server" class="dateStamp" href="javascript: pageFlip('Activity_Replies.aspx')">Replies</a> (<asp:label runat="server" id="lblReply"/>)&nbsp;&nbsp;<a runat="server" class="dateStamp" href="javascript: pageFlip('Activity_Ratings.aspx')">Ratings</a> (<asp:label runat="server" id="lblRatings"/>)&nbsp;&nbsp;<a runat="server" class="dateStamp" href="javascript: pageFlip('Activity_Published.aspx')">Published Items</a> (<asp:label runat="server" id="lblItems"/>)&nbsp;&nbsp;<a runat="server" class="dateStamp" href="javascript: pageFlip('Activity_Channels.aspx')">Communities</a> (<asp:label runat="server" id="lblChannels"/>)&nbsp;&nbsp;<a runat="server" class="dateStamp" href="javascript: pageFlip('Activity_Blogs.aspx')">Blogs</a> (<asp:label runat="server" id="lblBlogs"/>)&nbsp;&nbsp;<a runat="server" class="dateStamp" href="javascript: pageFlip('Activity_Sales.aspx')">Items Sold</a> (<asp:label runat="server" id="lblItemsSold"/>)
</span><br><br>

<center>
<iframe id="activityFrame" runat="server" width="1000" height="80%" frameborder="0"></iframe>

<input type="hidden" runat="server" id="hidUserId" value="0">

</center>


<script language="javascript">
<!--
//
	function pageFlip(which)
	{
		document.getElementById("activityFrame").src = which + '?userId=' + document.getElementById("hidUserId").value;		
	}
	
	pageFlip('Activity_Posts.aspx');
	
//
-->


</script>












</asp:panel>

