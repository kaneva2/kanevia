<%@ Page language="c#" ValidateRequest="False" Codebehind="userDetails.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.CSR.userDetails" %>
<%@ Register TagPrefix="Kaneva" TagName="CSRNav" Src="csrNav.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="AdminMenu" Src="../../usercontrols/AdministrativeMenu.ascx" %>

<Kaneva:AdminMenu ID="adminmenu" runat="server" />
<asp:ValidationSummary ShowMessageBox="False" ShowSummary="True" Class="formError" id="valSum" DisplayMode="BulletList" runat="server" HeaderText="Please correct the following errors:"/>

<table runat="server" id="tblNoCSR" cellpadding="0" cellspacing="0" border="0" width="560">
	<tr>
		<td ALIGN="CENTER">
			<BR><BR><BR>
			<span class="subHead">You must be a CSR to access this section.</span>
		</td>
	</tr>
</table>

<asp:panel runat="server" ID="pnlCSR">

<Kaneva:CSRNav runat="server" id="csrNavigation" SubTab="us"/>
<br>


<table id="tblUserFame" border="0" cellpadding="0" cellspacing="0" width="1000" style="background-color:#ffffff">
	<tr>
		<td style="height:20px"></td>
    </tr>
    <tr>
         <td align="center">

			<table cellpadding="0" cellspacing="0" border="0" width="750">
				<tr>
					<td><img runat="server" src="~/images/spacer.gif" width="20" height="1" ID="Img28"/></td>
					<td><asp:ValidationSummary ShowMessageBox="False" ShowSummary="True" Class="formError" id="Validationsummary1" DisplayMode="BulletList" runat="server" HeaderText="Please correct the following errors:"/></td>
					<td><img runat="server" src="~/images/spacer.gif" width="20" height="1" ID="Img29"/></td>
				</tr>
				<tr id="trError" runat="server">
					<td><img runat="server" src="~/images/spacer.gif" width="20" height="1" ID="Img30"/></td>
					<td class="messageBox">
						<BR/>			
						NOTICE: To be able to experience Kaneva at its fullest, please validate your 
						current email address '<B><asp:Label runat="server" id="lblEmail" CssClass="datestamp"/></B>'. 
						Follow the instructions in the message Kaneva 
						sends to this address to validate your address, or click 'Send Validation' if 
						you'd like another copy of the message.
						<BR/><BR/><center><asp:button id="btnRegEmail" align="right" runat="Server" onClick="btnRegEmail_Click" class="Filter2" Text=" Send Validation " CausesValidation="False"/></center>
						<BR/><BR/><span class="dateStamp">
						Kaneva sends email from kaneva@kaneva.com. If you are using an 
						antispam filter, please check your junk mail folder or add this address so you 
						will receive the email.<BR><BR><i>If you do not receive an email from Kaneva within 
						4 hours, please contact <a runat="server" href="~/suggestions.aspx?mode=F&category=Feedback" class="showingCount" ID="A1">support</a>.</i></span>
						<BR/><BR/>
					</td>
					<td><img runat="server" src="~/images/spacer.gif" width="20" ID="Img31"/></td>
				</tr>
				<tr>
					<td colspan="3"></td>
				</tr>
				<tr>
					<td><img runat="server" src="~/images/spacer.gif" width="20" height="1" ID="Img32"/></td>
					<td>
						<b id="rounded2">
						<i><span><img border="0" runat="server" src="~/images/spacer.gif" width="709" height="8" ID="Img33"/></span></i> 
						</b>
					</td>
					<td><img runat="server" src="~/images/spacer.gif" width="20" height="1" ID="Img34"/></td>
				</tr>
			</table> 

			<table cellpadding="0" cellspacing="0" border="0" width="750" bgcolor="#ededed">							
				<tr>
					<td bgcolor="white"><img runat="server" src="~/images/spacer.gif" width="20" height="25" border="0" ID="Img1"/></td>
					<td valign="top" align="center" class="bodyText" bgcolor="#ededed">
						<img runat="server" src="~/images/spacer.gif" width="100" height="1" ID="Img2"/>
						<br>
						<img runat="server" id="imgAvatar" src="~/images/KanevaIcon01.gif" 
						border="0" style="border: 1px solid #666666;" width="64" height="64"/><br><br>
						<br><br><br><br>
					</td>
					<td><img runat="server" src="~/images/spacer.gif" width="10" ID="Img3"/></td>
					<td width="100%" valign="top">
						<table cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td colspan="3" class="bodyText">
									<div style="height:30px;width:150px;padding:10px 0 10px 0;">
									<asp:linkbutton id="lbRemovePic" runat="server" onclientclick="return confirm('Are you sure you want to clear the profile picture?');" onclick="lbRemovePic_Click">Clear profile pic</asp:linkbutton>
									</div>
									
									<b><font size="5" face="verdana"><asp:Label runat="server" id="lblUsername"/></font></b>&nbsp;&nbsp;<br />
										<a href="Activity_Posts.aspx?userId=<%= Request["userId"] %>">Activity</a> 
										<a href="userNotes.aspx?userId=<%= Request["userId"] %>">Notes</a> 
										<asp:DropDownList class="filter2" id="drpAccounts" runat="Server" autopostback="true" OnSelectedIndexChanged="drpAccounts_Change" DataTextField="username" DataValueField="user_id" Width="200px"/><br><br>
										<asp:imagebutton runat="server" ID="btnAddAccount" ImageUrl="~/images/button_addNewAccount.gif" class="Filter2" alt="Add a New Sub-Account" OnClick="btnAddAccount_Click"/> <asp:imagebutton runat="server" ID="btnDeleteAccount" ImageUrl="~/images/button_deleteThisAccount.gif" class="Filter2" alt="Delete This Account" OnClick="btnDeleteAccount_Click"/><br>
								</td>
							</tr>
							<!--
							<tr>
								<td colspan=3>                    
									<asp:button runat="server" ID="btnEraseProfilePic" Text="Erase Profile Picture" />
									<asp:button runat="server" ID="btnMarkMsgs419" Text="Erase Profile Picture" />
								</td>				
							</tr>
							-->
							<!--<tr>
								<td class="bodyBold" align="right" valign="middle" width="70"><img runat="server" src="~/images/spacer.gif" width="70" height="1"/><br>Rank:</td>
								<td width="2">&nbsp;</td>
								<td class="bodyText" align="left" valign="middle" width="100%"><asp:Label runat="server" id="lblRole"/></td>
							</tr>-->
							<tr>
								<td class="bodyBold" align="right" valign="middle" width="70">Account:</td>
								<td width="2">&nbsp;</td>
								<td class="bodyText" align="left" valign="middle" width="100%"> <B style="color: green; font-size: 14px;"><asp:Label runat="server" id="lblLicenseType"/></B>&nbsp;<!-- <asp:imagebutton id="btnChange" ImageUrl="~/images/button_change.gif" runat="Server" onClick="btnChange_Click" class="Filter2" alt="Change this Account" style="vertical-align: middle;"/> --></td>
							</tr>
							<tr>
								<td class="bodyBold" align="right" valign="middle" width="70">K-Points:</td>
								<td width="2">&nbsp;</td>
								<td class="bodyText" align="left" valign="middle" width="100%"><B style="color: green;"><asp:Label runat="server" id="lblKPoint"/></B></td>
							</tr>
							<tr>
								<td class="bodyBold" align="right" valign="middle" width="70">Cash:</td>
								<td width="2">&nbsp;</td>
								<td class="bodyText" align="left" valign="middle" width="100%"><B style="color: green;">
									<asp:Label runat="server" id="lblCash"/>
									</b>&nbsp;&nbsp;<!--<a href="#" style="color: #4863a2; font-size: 10px; font-family: arial;">how to make $$$</a>-->
								</td>
							</tr>
							<tr id="trClan" runat="server">
								<td class="bodyBold" align="right" valign="middle" width="70">Clan:</td>
								<td width="2">&nbsp;</td>
								<td class="bodyText" align="left" valign="middle" width="100%">
									<asp:Label runat="server" id="lblClan" />
									<asp:button runat="server" text="Disband" id="btnClanDisband" OnClientClick="return confirm('This action removes all members and deltes this clan. Are you sure?');" onClick="btnClanDisband_Click" />
								</td>
							</tr>									
							<tr>
								<td class="bodyBold" align="right" valign="middle" width="70">Homepage:</td>
								<td width="2">&nbsp;</td>
								<td class="bodyText" align="left" valign="middle" width="100%" nowrap="true">
									<small>http://</small><asp:TextBox ID="txtHomepage2" class="Filter2" style="width:242px" MaxLength="242" runat="server"/>
								</td>
							</tr>
							<tr>
								<td class="bodyBold" align="right" valign="middle" width="70">MSN:</td>
								<td width="2">&nbsp;</td>
								<td class="bodyText" align="left" valign="middle" width="100%">
									<asp:TextBox ID="txtMSN" class="Filter2" style="width:280px" MaxLength="280" runat="server"/>
								</td>
							</tr>
							<tr>
								<td class="bodyBold" align="right" valign="middle" width="70">ICQ:</td>
								<td width="2">&nbsp;</td>
								<td class="bodyText" align="left" valign="middle" width="100%">
									<asp:TextBox ID="txtICQ" class="Filter2" style="width:280px" MaxLength="280" runat="server"/>
								</td>
							</tr>
							<tr>
								<td class="bodyBold" align="right" valign="middle" width="70">AIM:</td>
								<td width="2">&nbsp;</td>
								<td class="bodyText" align="left" valign="middle" width="100%">
									<asp:TextBox ID="txtAIM" class="Filter2" style="width:280px" MaxLength="280" runat="server"/>
								</td>
							</tr>
							<tr>
								<td class="bodyBold" align="right" valign="middle" width="70">Yahoo:</td>
								<td width="2">&nbsp;</td>
								<td class="bodyText" align="left" valign="middle" width="100%">
									<asp:TextBox ID="txtYahoo" class="Filter2" style="width:280px" MaxLength="280" runat="server"/>
								</td>
							</tr>
							<tr>
								<td colspan="3"><br></td>
							</tr>
							<tr>
								<td class="bodyBold" align="right" valign="middle" width="70"></td>
								<td width="2">&nbsp;</td>
								<td class="bodyText" align="left" valign="middle" width="100%">
									<asp:Checkbox runat="server" id="chkShowMature"/><b> Show mature content.</b><br><br>
									<asp:Checkbox runat="server" id="chkProfileRestrict"/><b> Restrict user.</b><br><br>
								</td>
							</tr>
						</table>
					</td>
					<td></td>							
					<td valign="top" align="center" bgcolor="white">
						<img runat="server" src="~/images/spacer.gif" width="20" height="10" border="0" ID="Img7"/><br>
					</td>													
				</tr>
				<tr>
					<td valign="top" align="center" bgcolor="white">
						<img runat="server" src="~/images/spacer.gif" width="16" height="10" border="0" ID="Img8"/>
					</td>	
					<td bgcolor="#dddddd" class="blogHeadline2" align="right" width="100" valign="middle" style="background-color: #dddddd;">Tell People About Yourself:</td>
					<td bgcolor="#dddddd"><img runat="server" src="~/images/spacer.gif" width="10" height="18" ID="Img9"/></td>
					<td class="bodyText" bgcolor="#dddddd" align="left" valign="middle" height="100" colspan="2">
						<asp:TextBox ID="txtDescription" TextMode="multiline" Rows="6" cols="90" class="Filter2" MaxLength="400" runat="server"/><BR/><BR/>
					</td>
					<td valign="top" align="center" bgcolor="white">
						<img runat="server" src="~/images/spacer.gif" width="16" height="10" border="0" ID="Img10"/>
					</td>	
				</tr>
			</table>

			<table cellpadding="0" cellspacing="0" border="0" width="710" ID="Table2" style="border-left: 1px solid #ededed; border-right: 1px solid #ededed; border-bottom: 1px solid #ededed;">
				<tr>
					<td colspan="3" align="left" valign="middle" style="background-color: #FFC96C;">
						&nbsp;&nbsp;<span class="blogHeadline2"><b>Credentials<b></b></span>
					</td>
					<td colspan="2" style="background-color: #FFC96C;" width="570">&nbsp;&nbsp;</td>
				</tr>
				<tr>
					<td colspan="3" align="right" valign="middle" class="filter2" bgcolor="#dddddd">
						<font color="red">*</font> <b>Username:</b>&nbsp;
					</td>
					<td colspan="2" bgcolor="#dddddd">
						<asp:TextBox ID="txtUserName" Enabled="False" class="Filter2" style="width:300px" MaxLength="80" runat="server"/><asp:RequiredFieldValidator ID="rfUsername" ControlToValidate="txtUserName" Text="*" ErrorMessage="Username is a required field." Display="Dynamic" runat="server"/>
					</td>
				</tr>
				<tr>
					<td colspan="3" align="right" valign="middle" class="filter2" bgcolor="#dddddd">
						<font color="red">*</font> <b>Display Name:</b>&nbsp;
					</td>
					<td colspan="2" bgcolor="#dddddd">
						<asp:TextBox ID="txtDisplayName" class="Filter2" style="width:300px" MaxLength="30" runat="server"/><asp:RequiredFieldValidator ID="rfDisplayname" ControlToValidate="txtDisplayName" Text="*" ErrorMessage="Display Name is a required field." Display="Dynamic" runat="server"/>
					</td>
				</tr>
				<tr>
					<td colspan="3" align="right" valign="middle" class="filter2">
						<font color="red">*</font> <b>Email:</b>&nbsp; 
					</td>
					<td colspan="2" class="bodyText">
						<asp:TextBox ID="txtEmail" class="Filter2" style="width:300px" MaxLength="100" runat="server"/>
						<asp:RequiredFieldValidator ID="rfEmail" ControlToValidate="txtEmail" Text="*" ErrorMessage="Email is a required field." Display="Dynamic" runat="server"/>
						<asp:RegularExpressionValidator id="revEmail" ControlToValidate="txtEmail" Text="*" Display="Static" ErrorMessage="Invalid email address." EnableClientScript="True" runat="server"/><asp:Checkbox runat="server" id="chkShowEmail"/>show 
					</td>
				</tr>							
				<tr>
					<td colspan="3" align="right" valign="middle" class="filter2" bgcolor="#dddddd">
						<font color="red">*</font> <b>Confirm Email:</b>&nbsp; 
					</td>
					<td colspan="2" bgcolor="#dddddd">
						<asp:TextBox ID="txtConfirmEmail" class="Filter2" style="width:300px" MaxLength="100" runat="server"/><asp:CompareValidator id="cmpEmail" ControlToValidate="txtConfirmEmail" ControlToCompare="txtEmail" Type="String" Operator="Equal" ErrorMessage="Email and Confirm Email must match." Text="*" runat="server"/>
					</td>
				</tr>
				<tr>
					<td colspan="3" align="right" valign="middle" class="filter2">
						<b>New Password:</b>&nbsp; 
					</td>
					<td colspan="2">
						<asp:TextBox ID="txtPassword" TextMode="Password" class="Filter2" style="width:300px" MaxLength="20" runat="server"/>
						<asp:RegularExpressionValidator id="revPassword" runat="server" Text="*" ErrorMessage="Password contains illegal characters or is too short. Password must contain only letters, numbers, and underscore,<BR> and must contain at least 4 characters." Display="Dynamic" ControlToValidate="txtPassword"/>
					</td>
				</tr>	
				<tr>
					<td colspan="3" align="right" valign="middle" class="filter2" bgcolor="#dddddd">
						<b>Confirm Password:</b>&nbsp; 
					</td>
					<td colspan="2" bgcolor="#dddddd">
						<asp:TextBox ID="txtConfirmPassword" TextMode="Password" class="Filter2" style="width:300px" MaxLength="20" runat="server"/><asp:CompareValidator Text="*" id="cmpPassword" ControlToValidate="txtConfirmPassword" ControlToCompare="txtPassword" Type="String" Operator="Equal" ErrorMessage="Password and Confirm Password must match." runat="server"/>
					</td>
				</tr>
				<tr>
					<td colspan="3" align="right" valign="middle" class="filter2">
						<font color="red">*</font> <b>First Name:</b>&nbsp; 
					</td>
					<td colspan="2">
						<asp:TextBox ID="txtFirstName" class="Filter2" style="width:200px" MaxLength="50" runat="server"/><asp:RequiredFieldValidator ID="rffirstname" ControlToValidate="txtFirstName" Text="*" ErrorMessage="First name is a required field." Display="Dynamic" runat="server"/>
					</td>
				</tr>
				<tr>
					<td colspan="3" align="right" valign="middle" class="filter2" bgcolor="#dddddd">
						<font color="red">*</font> <b>Last Name:</b>&nbsp; 
					</td>
					<td colspan="2" bgcolor="#dddddd">
						<asp:TextBox ID="txtLastName" class="Filter2" style="width:200px" MaxLength="50" runat="server"/><asp:RequiredFieldValidator ID="rflastname" ControlToValidate="txtLastName" Text="*" ErrorMessage="Last name is a required field." Display="Dynamic" runat="server"/>
					</td>
				</tr>							
				<tr>
					<td colspan="3" align="right" valign="middle" class="filter2">
						<font color="red">*</font> <b>Date of Birth:</b>&nbsp;
					</td>
					<td colspan="2">
						<asp:Dropdownlist runat="server" class="Filter2" ID="drpMonth">
							<asp:ListItem  value="">select...</asp:ListItem >
							<asp:ListItem  value="1">January</asp:ListItem >
							<asp:ListItem  value="2">February</asp:ListItem >
							<asp:ListItem  value="3">March</asp:ListItem >
							<asp:ListItem  value="4">April</asp:ListItem >
							<asp:ListItem  value="5">May</asp:ListItem >
							<asp:ListItem  value="6">June</asp:ListItem >
							<asp:ListItem  value="7">July</asp:ListItem >
							<asp:ListItem  value="8">August</asp:ListItem >
							<asp:ListItem  value="9">September</asp:ListItem >
							<asp:ListItem  value="10">October</asp:ListItem >
							<asp:ListItem  value="11">November</asp:ListItem >
							<asp:ListItem  value="12">December</asp:ListItem >
						</asp:Dropdownlist><asp:RequiredFieldValidator ID="rfMonth" ControlToValidate="drpMonth" Text="*" ErrorMessage="Birth month is a required field." Display="Static" runat="server"/>
						<asp:Dropdownlist runat="server" class="Filter2" ID="drpDay" NAME="drpDay">
							<asp:ListItem  value="">select...</asp:ListItem >
							<asp:ListItem  value="1">01</asp:ListItem >
							<asp:ListItem  value="2">02</asp:ListItem >
							<asp:ListItem  value="3">03</asp:ListItem >
							<asp:ListItem  value="4">04</asp:ListItem >
							<asp:ListItem  value="5">05</asp:ListItem >
							<asp:ListItem  value="6">06</asp:ListItem >
							<asp:ListItem  value="7">07</asp:ListItem >
							<asp:ListItem  value="8">08</asp:ListItem >
							<asp:ListItem  value="9">09</asp:ListItem >
							<asp:ListItem  value="10">10</asp:ListItem >
							<asp:ListItem  value="11">11</asp:ListItem >
							<asp:ListItem  value="12">12</asp:ListItem >
							<asp:ListItem  value="13">13</asp:ListItem >
							<asp:ListItem  value="14">14</asp:ListItem >
							<asp:ListItem  value="15">15</asp:ListItem >
							<asp:ListItem  value="16">16</asp:ListItem >
							<asp:ListItem  value="17">17</asp:ListItem >
							<asp:ListItem  value="18">18</asp:ListItem >
							<asp:ListItem  value="19">19</asp:ListItem >
							<asp:ListItem  value="20">20</asp:ListItem >
							<asp:ListItem  value="21">21</asp:ListItem >
							<asp:ListItem  value="22">22</asp:ListItem >
							<asp:ListItem  value="23">23</asp:ListItem >
							<asp:ListItem  value="24">24</asp:ListItem >
							<asp:ListItem  value="25">25</asp:ListItem >
							<asp:ListItem  value="26">26</asp:ListItem >
							<asp:ListItem  value="27">27</asp:ListItem >
							<asp:ListItem  value="28">28</asp:ListItem >
							<asp:ListItem  value="29">29</asp:ListItem >
							<asp:ListItem  value="30">30</asp:ListItem >
							<asp:ListItem  value="31">31</asp:ListItem >
						</asp:Dropdownlist><asp:RequiredFieldValidator ID="rfDay" ControlToValidate="drpDay" Text="*" ErrorMessage="Birth day is a required field." Display="Static" runat="server"/>
						<asp:Dropdownlist runat="server" class="Filter2" ID="drpYear">
							<asp:ListItem  value="">select...</asp:ListItem >
							<asp:ListItem  value="1940">< 1940</asp:ListItem >
							<asp:ListItem  value="1941">1941</asp:ListItem >
							<asp:ListItem  value="1942">1942</asp:ListItem >
							<asp:ListItem  value="1943">1943</asp:ListItem >
							<asp:ListItem  value="1944">1944</asp:ListItem >
							<asp:ListItem  value="1945">1945</asp:ListItem >
							<asp:ListItem  value="1946">1946</asp:ListItem >
							<asp:ListItem  value="1947">1947</asp:ListItem >
							<asp:ListItem  value="1948">1948</asp:ListItem >
							<asp:ListItem  value="1949">1949</asp:ListItem >
							<asp:ListItem  value="1950">1950</asp:ListItem >
							<asp:ListItem  value="1951">1951</asp:ListItem >
							<asp:ListItem  value="1952">1952</asp:ListItem >
							<asp:ListItem  value="1953">1953</asp:ListItem >
							<asp:ListItem  value="1954">1954</asp:ListItem >
							<asp:ListItem  value="1955">1955</asp:ListItem >
							<asp:ListItem  value="1956">1956</asp:ListItem >
							<asp:ListItem  value="1957">1957</asp:ListItem >
							<asp:ListItem  value="1958">1958</asp:ListItem >
							<asp:ListItem  value="1959">1959</asp:ListItem >
							<asp:ListItem  value="1960">1960</asp:ListItem >
							<asp:ListItem  value="1961">1961</asp:ListItem >
							<asp:ListItem  value="1962">1962</asp:ListItem >
							<asp:ListItem  value="1963">1963</asp:ListItem >
							<asp:ListItem  value="1964">1964</asp:ListItem >
							<asp:ListItem  value="1965">1965</asp:ListItem >
							<asp:ListItem  value="1966">1966</asp:ListItem >
							<asp:ListItem  value="1967">1967</asp:ListItem >
							<asp:ListItem  value="1968">1968</asp:ListItem >
							<asp:ListItem  value="1969">1969</asp:ListItem >
							<asp:ListItem  value="1970">1970</asp:ListItem >
							<asp:ListItem  value="1971">1971</asp:ListItem >
							<asp:ListItem  value="1972">1972</asp:ListItem >
							<asp:ListItem  value="1973">1973</asp:ListItem >
							<asp:ListItem  value="1974">1974</asp:ListItem >
							<asp:ListItem  value="1975">1975</asp:ListItem >
							<asp:ListItem  value="1976">1976</asp:ListItem >
							<asp:ListItem  value="1977">1977</asp:ListItem >
							<asp:ListItem  value="1978">1978</asp:ListItem >
							<asp:ListItem  value="1979">1979</asp:ListItem >
							<asp:ListItem  value="1980">1980</asp:ListItem >
							<asp:ListItem  value="1981">1981</asp:ListItem >
							<asp:ListItem  value="1982">1982</asp:ListItem >
							<asp:ListItem  value="1983">1983</asp:ListItem >
							<asp:ListItem  value="1984">1984</asp:ListItem >
							<asp:ListItem  value="1985">1985</asp:ListItem >
							<asp:ListItem  value="1986">1986</asp:ListItem >
							<asp:ListItem  value="1987">1987</asp:ListItem >
							<asp:ListItem  value="1988">1988</asp:ListItem >
							<asp:ListItem  value="1989">1989</asp:ListItem >
							<asp:ListItem  value="1990">1990</asp:ListItem >
							<asp:ListItem  value="1991">1991</asp:ListItem >
							<asp:ListItem  value="1992">1992</asp:ListItem >
							<asp:ListItem  value="1993">1993</asp:ListItem >
							<asp:ListItem  value="1994">1994</asp:ListItem >
							<asp:ListItem  value="1995">1995</asp:ListItem >
							<asp:ListItem  value="1996">1996</asp:ListItem >
							<asp:ListItem  value="1997">1997</asp:ListItem >
							<asp:ListItem  value="1998">1998</asp:ListItem >
							<asp:ListItem  value="1999">1999</asp:ListItem >
							<asp:ListItem  value="2000">2000</asp:ListItem >
							<asp:ListItem  value="2001">2001</asp:ListItem >
							<asp:ListItem  value="2002">2002</asp:ListItem >
						</asp:Dropdownlist><asp:RequiredFieldValidator ID="rfYear" ControlToValidate="drpYear" Text="*" ErrorMessage="Birth year is a required field." Display="Static" runat="server"/>
					</td>
				</tr>
				<tr>
					<td colspan="3" align="right" valign="middle" class="filter2" bgcolor="#dddddd">
						<font color="red">*</font> <b>Gender:</b>&nbsp; 
					</td>
					<td colspan="2" bgcolor="#dddddd">
						<asp:Dropdownlist runat="server" class="Filter2" ID="drpGender">
							<asp:ListItem value="">select...</asp:ListItem >
							<asp:ListItem value="M">Male</asp:ListItem >
							<asp:ListItem  value="F">Female</asp:ListItem >
						</asp:Dropdownlist><asp:RequiredFieldValidator ID="rfdrpGender" ControlToValidate="drpGender" Text="*" ErrorMessage="Gender is a required field." Display="Dynamic" runat="server"/>
					</td>
				</tr>	
				<tr>
					<td colspan="3" align="right" valign="middle" class="filter2">
						<font color="red">*</font> <b>Postal Code:</b>&nbsp;
					</td>
					<td colspan="2">
						<asp:TextBox ID="txtPostalCode" class="Filter2" Width="100" MaxLength="25" runat="server"/><asp:RequiredFieldValidator ID="rftxtPostalCode" ControlToValidate="txtPostalCode" Text="*" ErrorMessage="Postal code is a required field." Display="Dynamic" runat="server"/>
					</td>
				</tr>
				<tr>
					<td colspan="3" align="right" valign="middle" class="filter2"  bgcolor="#dddddd">
						<font color="red">*</font> <b>Country:</b>&nbsp;
					</td>
					<td colspan="2"  bgcolor="#dddddd">
						<asp:Dropdownlist runat="server" class="Filter2" ID="drpCountry">
						</asp:Dropdownlist><asp:RequiredFieldValidator ID="rfdrpCountry" ControlToValidate="drpCountry" Text="*" ErrorMessage="Country is a required field." Display="Dynamic" runat="server"/>
					</td>
				</tr>
				<tr>
					<td colspan="8" valign="top" align="center">
						<div id="divNameChanges" runat="server"	style="display:none;">
							<asp:repeater id="rptNameChanges" runat="server">
								<headertemplate>
									<div class="filter2" style="font-weight:bold;margin:20px 0 6px 0;width:600px;text-align:left;">Name Changes:</div>
									<div style="border:solid 1px gray;width:600px;">
									<table cellpadding="5" cellspacing="0" border="0">
										<tr>
											<td width="200" class="filter2" style="font-weight:bold;background-color:#dddddd;">New Username</td>
											<td width="200" class="filter2" style="font-weight:bold;background-color:#dddddd;">Old Username</td>
											<td width="200" class="filter2" style="font-weight:bold;background-color:#dddddd;">Date Changed</td></tr>
								</headertemplate>
								<itemtemplate>
									<tr>
										<td class="filter2"><%# DataBinder.Eval(Container.DataItem, "username") %></td>
										<td class="filter2"><%# DataBinder.Eval(Container.DataItem, "username_old") %></td>
										<td class="filter2"><%# DataBinder.Eval(Container.DataItem, "date_modified") %></td>
									</tr>	
								</itemtemplate>
								<alternatingitemtemplate>
									<tr>
										<td class="filter2" style="background-color:#ffffff;"><%# DataBinder.Eval(Container.DataItem, "username") %></td>
										<td class="filter2 style="background-color:#ffffff;"><%# DataBinder.Eval(Container.DataItem, "username_old") %></td>
										<td class="filter2 style="background-color:#ffffff;"><%# DataBinder.Eval(Container.DataItem, "date_modified") %></td>
									</tr>	
								</alternatingitemtemplate>
								<footertemplate></table></div></footertemplate>
							</asp:repeater>
						</div>
						<br/>
					</td>
				</tr>
			</table>			
	
			<table cellpadding="0" cellspacing="0" border="0" width="710" style="border-left: 1px solid #ededed; border-right: 1px solid #ededed;">				
				<tr>
					<td colspan="3" align="left" valign="middle" style="background-color: #FFC96C; line-height:22px;">
					&nbsp;&nbsp;<span class="blogHeadline2"><b>Connected Media</b></span>	
					</td>
					<td colspan="2" class="bodyBold" style="background-color: #FFC96C;" valign="middle" align="left">
						<asp:Dropdownlist runat="server" class="bodyBold" ID="drpShowFavs" style="width:250px">
						</asp:Dropdownlist> can view
					</td>
				</tr>
				<tr>
					<td colspan="3" align="right" valign="top" class="filter2" bgcolor="#dddddd"><br><br>
						<b>Favorite Games:</b>&nbsp;
					</td>
					<td colspan="2" bgcolor="#dddddd">
						<asp:TextBox ID="txtGames" TextMode="multiline" Rows="6" Cols="82" class="Filter2" MaxLength="400" runat="server"/>
					</td>
				</tr>
				<tr>
					<td colspan="3" align="right" valign="top" class="filter2"><br>
						<b>Favorite Movies:</b>&nbsp;
					</td>
					<td colspan="2">
						<asp:TextBox ID="txtMovies" TextMode="multiline" Rows="6" Cols="82" class="Filter2" MaxLength="400" runat="server"/>
					</td>
				</tr>
				<tr>
					<td colspan="3" align="right" valign="top" class="filter2" bgcolor="#dddddd"><br>
						<b>Favorite Music:</b>&nbsp;
					</td>
					<td colspan="2" bgcolor="#dddddd">
						<asp:TextBox ID="txtMusic" TextMode="multiline" Rows="6" Cols="82" class="Filter2" MaxLength="400" runat="server"/>
					</td>
				</tr>
				<tr>
					<td colspan="3" align="right" valign="top" class="filter2"><br>
						<b>Favorite Books:</b>&nbsp;
					</td>
					<td colspan="2">
						<asp:TextBox ID="txtBooks" TextMode="multiline" Rows="6" Cols="82" class="Filter2" MaxLength="400" runat="server"/>
					</td>
				</tr>							
				<tr>
					<td colspan="3" align="right" valign="top" class="filter2" bgcolor="#dddddd"><br>
						<b>Favorite TV Shows:</b>&nbsp;
					</td>
					<td colspan="2" bgcolor="#dddddd">
						<asp:TextBox ID="txtTV" TextMode="multiline" Rows="6" Cols="82" class="Filter2" MaxLength="400" runat="server"/>
					</td>
				</tr>
				<tr>
					<td colspan="8" valign="top"><br></td>
				</tr>
			</table>

			<table cellpadding="0" cellspacing="0" border="0" width="710" style="border-left: 1px solid #ededed; border-right: 1px solid #ededed;">				
				<tr>
					<td colspan="3" align="left" valign="middle" height="22" class="header02" style="background-color: #FFC96C; line-height:22px;">&nbsp;&nbsp;<span class="blogHeadline2"><b>Professional</b></span></td>
					<td colspan="2" class="bodyBold" style="background-color: #FFC96C;" valign="middle" align="left">
						<asp:Dropdownlist runat="server" class="bodyBold" ID="drpShowProf" style="width:250px">
							</asp:Dropdownlist> can view	
					</td>
				</tr>
				<tr>
					<td colspan="3" align="right" valign="middle" class="filter2"> <b>Title:</b>&nbsp;</td>
					<td colspan="2">
						<asp:TextBox ID="txtTitle" Width="300" MaxLength="80" class="Filter2" runat="server"/>
					</td>
				</tr>							
				<tr>
					<td colspan="3" align="right" valign="middle" class="filter2" bgcolor="#dddddd"> <b>Position:</b>&nbsp;</td>
					<td colspan="2" bgcolor="#dddddd">
						<asp:TextBox ID="txtPosition"  style="width:300px" MaxLength="80" class="Filter2" runat="server"/>
					</td>
				</tr>
				<tr>
					<td colspan="3" align="right" valign="middle" class="filter2"> <b>Industry:</b>&nbsp;</td>
					<td colspan="2">
						<asp:TextBox ID="txtIndustry" style="width:300px" MaxLength="80" class="Filter2" runat="server"/>
					</td>
				</tr>	
				<tr>
					<td colspan="3" align="right" valign="middle" class="filter2" bgcolor="#dddddd"> <b>Company:</b>&nbsp;</td>
					<td colspan="2" bgcolor="#dddddd">
						<asp:TextBox ID="txtCompany"  style="width:300px" MaxLength="80" class="Filter2" runat="server"/>
					</td>
				</tr>
				<tr>
					<td colspan="3" align="right" valign="top" class="filter2"><br><br> <b>Job Description:</b>&nbsp;</td>
					<td colspan="2" bgcolor="#ffffff" align="left">
						<asp:TextBox ID="txtJobDesc" TextMode="multiline" Rows="6" Cols="70" class="Filter2" MaxLength="400" runat="server"/>
					</td>
				</tr>
				<tr>
					<td colspan="3" align="right" valign="top" class="filter2" bgcolor="#dddddd"><br>
						<b>Tools & Skills:</b>&nbsp;
					</td>
					<td colspan="2" bgcolor="#dddddd">
						<asp:TextBox ID="txtSkills" TextMode="multiline" Rows="6" Cols="70" class="Filter2" MaxLength="400" runat="server"/>
					</td>
				</tr>
				<tr>
					<td colspan="8" valign="top"><br></td>
				</tr>
			</table>

			<table cellpadding="0" cellspacing="0" border="0" width="710" style="border-left: 1px solid #ededed; border-right: 1px solid #ededed;">				
				<tr>
					<td colspan="3" align="left" valign="middle" class="header02" style="background-color: #FFC96C; line-height:22px;">
						&nbsp;<span class="blogHeadline2"><b>Organizations</b></span>							
					</td>
					<td colspan="2" class="bodyBold" style="background-color: #FFC96C;" valign="middle" align="left">
						<asp:Dropdownlist runat="server" class="bodyBold" ID="drpShowOrg" style="width:250px">
						</asp:Dropdownlist> can view
					</td>
				</tr>
				<tr>
					<td colspan="3" align="right" valign="top" class="filter2" bgcolor="#dddddd"><br><br>
						<b>Current Associations:</b>&nbsp;
					</td>
					<td colspan="2" bgcolor="#dddddd">
						<asp:TextBox ID="txtCurrAssoc" TextMode="multiline" Rows="6" Cols="90" class="Filter2" MaxLength="400" runat="server"/>
					</td>
				</tr>
				<tr>
					<td colspan="3" align="right" valign="top" class="filter2"><br>
						<b>Past Associations:</b>&nbsp;
					</td>
					<td colspan="2">
						<asp:TextBox ID="txtPastAssoc" TextMode="multiline" Rows="6" Cols="90" class="Filter2" MaxLength="400" runat="server"/>
					</td>
				</tr>
				<tr>
					<td colspan="8" valign="top"><br></td>
				</tr>
			</table>

			<table cellpadding="0" cellspacing="0" border="0" width="750">	
				<tr>
					<td><img runat="server" src="~/images/spacer.gif" width="20" height="20" ID="Img13"/></td>
					<td colspan="3" class="bodyBold" style="background-color: #FFC96C; padding-top:4px;padding-bottom:4px;" width="730" valign="middle">&nbsp;&nbsp;<span class="blogHeadline2"><b>Screenshots</b></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Dropdownlist runat="server" class="bodyBold" ID="drpShowSShots" width="250px">
						</asp:Dropdownlist> can view
					

					</td>
					<td><img runat="server" src="~/images/spacer.gif" width="20" height="20" ID="Img14"/></td>
				</tr>
				<tr>
					<td><img runat="server" src="~/images/spacer.gif" width="20" height="40" ID="Img23"/></td>
					<td colspan="3" align="left" class="bodyText" style="border-left: 1px solid #ededed; border-right: 1px solid #ededed; border-bottom: 1px solid #ededed;"><br>
						&nbsp;&nbsp;<input runat="server" ID="inpAvatar" type="file" size="45" NAME="inpAvatar"/><br>
						<img runat="server" src="~/images/spacer.gif" width="130" height="4" ID="Img15"/>						
						<center>
						<!-- <input runat="server" type="file" id="inp2" class="bodyText" NAME="inp2"/> -->
						</center>
						<table border="0" width="100%" CellSpacing="2" CellPadding="1">
							<tr>
								<td valign="top" align="center" width="50%"></td>
								<td valign="top" width="50%"></td>
							</tr>
							<tr>
								<td colspan="3" align="center">
									<asp:Label ID="lblSize" class="dateStamp" runat="server"></asp:label>
								</td>
							</tr>	
						</table>
					</td>
					<td><img runat="server" src="~/images/spacer.gif" width="20" height="20" ID="Img24"/></td>
				</tr>	
			</table>
			
			<table cellpadding="0" cellspacing="0" border="0" width="710" style="border-left: 1px solid #ededed; border-right: 1px solid #ededed; border-bottom: 1px solid #ededed;">				
				<tr>
					<td colspan="2" align="left" valign="middle" class="blogHeadline" style="background-color: #FFC96C; line-height: 22px;">
						<span class="blogHeadline2">&nbsp;&nbsp;<b>Public Community</b></span>&nbsp; 							
					</td>
					<td colspan="3" style="background-color: #FFC96C;" valign="top" align="left"></td>
				</tr>
				<tr>
					<td colspan="5"><br></td>
				</tr>
				<tr>
					<td colspan="2" align="right" valign="middle" class="filter2"><img runat="server" src="~/images/spacer.gif" height="1" width="130" ID="Img18"/>
						<b>Show Blog:</b>&nbsp;
					</td>
					<td colspan="3">
						<asp:Dropdownlist runat="server" class="Filter2" ID="drpShowBlog" width="250px"></asp:Dropdownlist>
					</td>
				</tr>
				<tr>
					<td colspan="2" align="right" valign="middle" class="filter2"><img runat="server" src="~/images/spacer.gif" height="1" width="130" ID="Img19"/>
						<b>Show Games:</b>&nbsp;
					</td>
					<td colspan="3">
						<asp:Dropdownlist runat="server" class="Filter2" ID="drpShowGames" width="250px"></asp:Dropdownlist>
					</td>
				</tr>
				<tr>
					<td colspan="2" align="right" valign="middle" class="filter2">
						<b>Show Bookmarks:</b>&nbsp;
					</td>
					<td colspan="3" valign="middle">
						<asp:Dropdownlist runat="server" class="Filter2" ID="drpShowBookmarks" width="250px"></asp:Dropdownlist>
					</td>
				</tr>
				<tr>
					<td colspan="5"><br></td>
				</tr>
			</table>

			<table cellpadding="0" cellspacing="0" border="0" width="710" style="border-left: 1px solid #ededed; border-right: 1px solid #ededed;">				
				<tr>
					<td colspan="5" align="left" valign="middle" class="blogHeadline2" style="line-height:22px;" bgcolor="#FFC96C">
						<span class="blogHeadline2">&nbsp;&nbsp;<b>Email Preferences</b></span>&nbsp; 							
					</td>
				</tr>
				<tr>
					<td colspan="5"><br></td>
				</tr>
				<tr>
					<td colspan="3" align="left" valign="middle" class="filter2">&nbsp; <input type="radio" id="optNotifyAll" name="optNotify" value="1" runat="server"/> Notify me for all private messages.
					</td>
				</tr>
				<tr>
					<td colspan="3" align="left" valign="middle" class="filter2">&nbsp; <input type="radio" id="optNotifyAlerts" name="optNotify" value="2" runat="server"/> Notify me for alerts only.
					</td>
				</tr>
				<tr>
					<td colspan="3" align="left" valign="middle" class="filter2">&nbsp; <input type="radio" id="optNotifyNone" name="optNotify" value="0" runat="server"/> Do not notify me.
					</td>
				</tr>
				<tr>
					<td colspan="5"><br></td>
				</tr>	
			</table>

			<table cellpadding="0" cellspacing="0" border="0" width="750">
				<tr>	
					<td><img runat="server" src="~/images/spacer.gif" width="20" height="1" ID="Img21"/></td>						
					<td align="right" bgcolor="#ededed" width="100%"><br>
						<asp:button id="btnUpdate" runat="Server" onClick="btnUpdate_Click" class="Filter2" Text="  submit  "/>&nbsp;&nbsp;&nbsp;<br><br>
					</td>
					<td><img runat="server" src="~/images/spacer.gif" width="20" height="1" ID="Img22"/></td>
				</tr>
			</table>
			
			<table cellpadding="0" cellspacing="0" border="0" width="750">
				<tr>
					<td><img runat="server" src="~/images/spacer.gif" width="20" height="1" ID="Img25"/></td>
					<td colspan="3">
						<b id="rounded2bot">
						 <i><span><img border="0" runat="server" src="~/images/spacer.gif" width="709" height="8" ID="Img26"/></span></i> 
						</b>
					</td>
					<td><img runat="server" src="~/images/spacer.gif" width="20" height="1" ID="Img27"/></td>
				</tr>
			</table>  

			<br/><br/>	

		</td>
	</tr>	
</table>

<br/><br/>
</asp:panel>
