///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva;
using log4net;
using System.Drawing;
using MagicAjax;
using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;
using System.Collections.Specialized;
using System.Collections.Generic;

namespace KlausEnt.KEP.Kaneva.csr
{
    public partial class SubscriptionManagement : NoBorderPage
    {
        #region Declarations

        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #endregion

        protected SubscriptionManagement() 
		{
			Title = "CSR - Subscriptions";
        }

        #region PageLoad

        protected void Page_Load(object sender, EventArgs e)
        {
            // Redirect if user is not admin
            if (!IsCSR ())
            {
                Response.Redirect ("~/default.aspx");
            }

            if (!IsPostBack)
            {
                //bind the Subscriptions
                BindSubscriptionsData(currentPage);

                //bind pulldown
                PopulateSubscriptionTerms();
                PopulatePassGroupList();

                divSubscriptionsDetails.Style.Add("display", "none");
            }
        }

        #endregion

        #region Primary Functions


        /// <summary>
        /// BindData
        /// </summary>
        private void BindSubscriptionsData(int currentPage)
        {
            DataTable subscriptionsDT = null;

            subscriptionsDT  = (new SubscriptionFacade()).GetAllSubscriptions();

            // Set the sortable columns
            string orderby = CurrentSort + " " + CurrentSortOrder;

            //set the current page
            this.dg_Subscriptions.PageIndex = currentPage;

            //set the sort order
            if ((subscriptionsDT != null) && (subscriptionsDT.Rows.Count > 0))
            {
                subscriptionsDT.DefaultView.Sort = orderby;

                lbl_NoSubscriptions.Visible = false;

                dg_Subscriptions.DataSource = subscriptionsDT;
                dg_Subscriptions.DataBind();
            }
            else
            {
                lbl_NoSubscriptions.Visible = true;
            }

        }

        /// <summary>
        /// BindStoreList
        /// </summary>
        private void DeleteSubscriptionsAndRelatedData(int SubscriptionsId)
        {
            SubscriptionFacade subFac = new SubscriptionFacade();

            subFac.DeleteSubscription(SubscriptionsId);
        }

        #endregion

        #region Helper Functions

        /// <summary>
        /// BindStoreList
        /// </summary>
        private void BindPassGroupList(uint SubscriptionId)
        {
            DataTable assocPassGroupDT = null;

            PopulatePassGroupList();

            //get all the current WOK stores
            DataTable passGroupsDT = PASS_GROUPS;

            //get the pass groups associated with the subscriptions
            assocPassGroupDT = (new SubscriptionFacade()).GetPassGroupsAssociatedWSubscription(SubscriptionId);

            //mark the stores the the item is in if any
            if (assocPassGroupDT.Rows.Count > 0)
            {
                foreach (DataRow row in assocPassGroupDT.Rows)
                {
                    DataRow[] passGroupRows = passGroupsDT.Select("pass_group_id = " + row["pass_group_id"].ToString());
                    if (passGroupRows.Length > 0)
                    {
                        DataRow passGroupRow = passGroupRows[0];
                        passGroupRow["cbx_assocPassGroup"] = true;

                        subscription2PassGroup.Add(row["pass_group_id"].ToString(), "true");

                        passGroupRow.AcceptChanges();
                        passGroupsDT.AcceptChanges();
                    }

                }
            }

            dgd_passGroups.DataSource = passGroupsDT;
            dgd_passGroups.DataBind();
        }


        private void PopulatePassGroupList()
        {
            DataTable passGroupsDT = WOKStoreUtility.GetPassGroups();

            //create the check field column for the get all stores data result
            DataColumn checkColumn = new DataColumn();
            checkColumn.DataType = System.Type.GetType("System.Boolean");
            checkColumn.AllowDBNull = false;
            checkColumn.ColumnName = "cbx_assocPassGroup";
            checkColumn.DefaultValue = false;
            passGroupsDT.Columns.Add(checkColumn);

            PASS_GROUPS = passGroupsDT;

            dgd_passGroups.DataSource = passGroupsDT;
            dgd_passGroups.DataBind();
        }

        private void PopulateSubscriptionTerms()
        {
            ddp_SubscriptionPeriod.DataSource = (new SubscriptionFacade()).GetSubscriptionTerms();
            ddp_SubscriptionPeriod.DataTextField = "subscription_term";
            ddp_SubscriptionPeriod.DataValueField = "subscription_term_id";
            ddp_SubscriptionPeriod.DataBind();
        }

        private void ClearPassGroupsDataTable()
        {
            foreach (DataRow row in PASS_GROUPS.Rows)
            {
                row["cbx_assocPassGroup"] = false;

                row.AcceptChanges();
                PASS_GROUPS.AcceptChanges();
            }

            dgd_passGroups.DataSource = PASS_GROUPS;
            dgd_passGroups.DataBind();
        }

        /// <summary>
        /// Clears out all Subscriptions Items fields 
        /// </summary>
        private void ClearSubscriptionItems()
        {
            subscription2PassGroup.Clear();
            ClearPassGroupsDataTable();
            tbx_SubscriptionName.Text = "";
            lbl_SubscriptionId.Text = "0";
            tbx_IntroductoryPrice.Text = "";
            tbx_IntroOfferDate.Text = "";
            cbx_GrandfatherPrice.Checked = false;
            tbx_TrialPeriod.Text = "";
            ddp_SubscriptionPeriod.SelectedIndex = 0;
            inp_learnMoreImage.Value = "";
            inp_upsellImage.Value = "";
            tbx_DiscountAmount.Text = "";
            tbx_LearnMoreContent.Text = "";
            tbx_Reasons2Keep.Text = "";
            tbx_Price.Text = "";
            tbx_MonthlyAllowance.Text = "";
        }

        private void resetViewState()
        {
            currentPage = 0;
        }

        public int PageSize()
        {
            return 10;
        }

        /// <summary>
        /// DEFAULT_SORT
        /// </summary>
        /// <returns></returns>
        protected override string DEFAULT_SORT
        {
            get
            {
                return "subscription_id";
            }
        }

        private DataTable PASS_GROUPS
        {
            get
            {
                if (Session["_passGroupsDT"] == null)
                {
                    Session["_passGroupsDT"] = new DataTable();
                }
                return (DataTable)Session["_passGroupsDT"];
            }
            set
            {
                Session["_passGroupsDT"] = value;
            }
        }

        private NameValueCollection subscription2PassGroup
        {
            get
            {
                if (ViewState["_subscription2PassGroup"] == null)
                {
                    ViewState["_subscription2PassGroup"] = new NameValueCollection();
                }
                return (NameValueCollection)ViewState["_subscription2PassGroup"];
            }
            set
            {
                ViewState["_subscription2PassGroup"] = value;
            }
        }

        private int currentPage
        {
            get
            {
                if (ViewState["_currentPage"] == null)
                {
                    ViewState["_currentPage"] = 0;
                }
                return (int)ViewState["_currentPage"];
            }
            set
            {
                ViewState["_currentPage"] = value;
            }
        }

        //allows user to switch the ordering between DESC and ASC
        //based on column clicked
        private void SortSwitch(string newSort)
        {
            if (CurrentSort == newSort)
            {
                if (CurrentSortOrder == "DESC")
                {
                    CurrentSortOrder = "ASC";
                }
                else
                {
                    CurrentSortOrder = "DESC";
                }
            }
            else
            {
                CurrentSort = newSort;
                CurrentSortOrder = "DESC";
            }
        }

        /// <summary>
        /// DEFAULT_SORT
        /// </summary>
        /// <returns></returns>
        protected override string DEFAULT_SORT_ORDER
        {
            get
            {
                return "DESC";
            }
        }

        #endregion

        #region Event Handlers

        protected void cbx_assocPassGroup_CheckedChanged(object sender, EventArgs e)
        {
            GridViewRow ritem = (GridViewRow)((Control)sender).NamingContainer;
            string id = ((Label)ritem.FindControl("lbl_passGroupId")).Text;
            string value = ((CheckBox)ritem.FindControl("cbx_assocPassGroup")).Checked.ToString();
            if (subscription2PassGroup[id] != null)
            {
                subscription2PassGroup[id] = value;
            }
            else
            {
                subscription2PassGroup.Add(id, value);
            }

        }

        protected void dg_SubscriptionsmyGrid_OnRowCreated(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            //logic check to work around initial row creation wheere there is only one column
            if (e.Row.Cells.Count > 10)
            {
                e.Row.Cells[0].Visible = true;
                e.Row.Cells[1].Visible = true;
                e.Row.Cells[2].Visible = true;
                e.Row.Cells[3].Visible = true;
                e.Row.Cells[4].Visible = true;
                e.Row.Cells[5].Visible = true;
                e.Row.Cells[6].Visible = true;
                e.Row.Cells[7].Visible = true;
                e.Row.Cells[8].Visible = false;
                e.Row.Cells[9].Visible = false;
                e.Row.Cells[10].Visible = false;
                e.Row.Cells[11].Visible = true;
                e.Row.Cells[12].Visible = false;
                e.Row.Cells[13].Visible = true;
            }

        }
        
        protected void lbn_NewSubscription_Click(object sender, EventArgs e)
        {
            //clear the fields
            ClearSubscriptionItems();

            //make the details panel visible
            divSubscriptionsDetails.Style.Add ("display", "block");

            //set id to -1 <indicator that it is a new entry>
            lbl_SubscriptionId.Text = "-1";
        }

        protected void btn_Cancel_Click(object sender, System.EventArgs e)
        {
            //clear the fields
            ClearSubscriptionItems();

            //make the details panel visible
            divSubscriptionsDetails.Style.Add ("display", "none");

        }

        protected void dg_Subscriptions_RowDeleting(object source, System.Web.UI.WebControls.GridViewDeleteEventArgs e)
        {
            //get the selected row
            GridViewRow gvr = dg_Subscriptions.Rows[e.RowIndex];

            //delete the record and rebind
            DeleteSubscriptionsAndRelatedData(Convert.ToInt32(gvr.Cells[0].Text));

            BindSubscriptionsData(currentPage);

            //clear the fields
            ClearSubscriptionItems();

            //reset viewstate
            resetViewState();

            //make the details panel visible
            divSubscriptionsDetails.Style.Add ("display", "none");
        }

        protected void dg_Subscriptions_RowEditing(object source, System.Web.UI.WebControls.GridViewEditEventArgs e)
        {
            //clear the fields
            ClearSubscriptionItems();

            //make the Subscriptions details panel visible
            divSubscriptionsDetails.Style.Add ("display", "block");

           //get the promotion
            GridViewRow gvr = dg_Subscriptions.Rows[e.NewEditIndex];

            try
            {
                //extra parsing to make sure HTML encoding is maintained and displayed correctly
                string reasons2Keep = ((((gvr.Cells[12].Text.Replace("amp;", "")).Replace("&quot;", "")).Replace("&lt;", "<")).Replace("&gt;", ">")).Replace("&nbsp;", "");
                string learnMoreContent = ((((gvr.Cells[10].Text.Replace("amp;", "")).Replace("&quot;", "")).Replace("&lt;", "<")).Replace("&gt;", ">")).Replace("&nbsp;", "");

                //populate fields
                //replace is a work around for unexplained &nbsp; showing up figure it out later
                h2OfferTitle.InnerText = "Subscription Details: " + gvr.Cells[1].Text.Replace ("&nbsp;", "");
                lbl_SubscriptionId.Text = gvr.Cells[0].Text.Replace("&nbsp;", "");
                tbx_SubscriptionName.Text = gvr.Cells[1].Text.Replace("&nbsp;", "");
                tbx_Price.Text = gvr.Cells[2].Text.Replace("&nbsp;", "");
                tbx_IntroductoryPrice.Text = gvr.Cells[3].Text.Replace("&nbsp;", "");
                tbx_IntroOfferDate.Text = Convert.ToDateTime(gvr.Cells[4].Text.Replace("&nbsp;", "")).ToString();
                cbx_GrandfatherPrice.Checked = gvr.Cells[5].Text.Replace("&nbsp;", "") == "Y" ? true : false;
                ddp_SubscriptionPeriod.SelectedValue = gvr.Cells[6].Text.Replace("&nbsp;", "");//term
                tbx_TrialPeriod.Text = gvr.Cells[7].Text.Replace("&nbsp;", "");
                inp_learnMoreImage.Value = gvr.Cells[8].Text.Replace("&nbsp;", "");
                inp_upsellImage.Value = gvr.Cells[9].Text.Replace("&nbsp;", "");
                tbx_LearnMoreContent.Text = HttpUtility.HtmlDecode(learnMoreContent);
                tbx_DiscountAmount.Text = gvr.Cells[11].Text.Replace("&nbsp;", "");
                tbx_Reasons2Keep.Text = HttpUtility.HtmlDecode(reasons2Keep);
                tbx_MonthlyAllowance.Text = gvr.Cells[13].Text.Replace("&nbsp;", "");

                //display associated pass groups
                BindPassGroupList(Convert.ToUInt32(lbl_SubscriptionId.Text));
            }
            catch (Exception ex)
            {
                // Display err message
                cvBlank.IsValid = false;
                cvBlank.ErrorMessage = "Error displaying subscription detials: " + ex.Message;
                valSum.Style.Add ("display", "block");
                m_logger.Error ("Error displaying subscription detials. ", ex);
                return;
            }

        }

        protected void dg_Subscriptions_PageIndexChanging(object source, System.Web.UI.WebControls.GridViewPageEventArgs e)
        {
            currentPage = e.NewPageIndex;
            BindSubscriptionsData(currentPage);
        }

        protected void dg_Subscriptions_Sorting(Object sender, GridViewSortEventArgs e)
        {
            SortSwitch(e.SortExpression); //sets the sort expression
            BindSubscriptionsData(currentPage);
        }

        protected void btn_Save_Click(object sender, System.EventArgs e)
        {
            if (!Page.IsValid)
            {
                return;
            }

            try
            {
                //extra parsing to make sure HTML encoding is maintained and displayed correctly
                string reasons2Keep = ((((tbx_Reasons2Keep.Text.Replace("amp;", "")).Replace("&quot;", "")).Replace("&lt;", "<")).Replace("&gt;", ">")).Replace("&nbsp;", "");
                string learnMoreContent = ((((tbx_LearnMoreContent.Text.Replace("amp;", "")).Replace("&quot;", "")).Replace("&lt;", "<")).Replace("&gt;", ">")).Replace("&nbsp;", "");

                //get the subscription id
                int subscriptionId = Convert.ToInt32(lbl_SubscriptionId.Text);

                //pull the data from fields 
                Subscription subscription = new Subscription();

                subscription.Name = tbx_SubscriptionName.Text;
                subscription.SubscriptionId = (subscriptionId > 0 ? (uint)subscriptionId : 0);
                subscription.IntroductoryPrice = Convert.ToDouble(tbx_IntroductoryPrice.Text);
                subscription.IntroductoryEndDate = Convert.ToDateTime(tbx_IntroOfferDate.Text);
                subscription.GrandfatherPrice = cbx_GrandfatherPrice.Checked;
                subscription.DaysFree = Convert.ToInt32(tbx_TrialPeriod.Text);
                subscription.Term = (Subscription.SubscriptionTerm)Convert.ToUInt32(ddp_SubscriptionPeriod.SelectedValue);
                subscription.Price = Convert.ToDouble(tbx_Price.Text);
                subscription.LearnMoreImage = inp_learnMoreImage.Value;
                subscription.UpsellImage = inp_upsellImage.Value;
                subscription.LearnMoreContent = HttpUtility.HtmlEncode(learnMoreContent);
                subscription.DiscountPercent = Convert.ToUInt32(tbx_DiscountAmount.Text);
                subscription.Reasons2Keep = HttpUtility.HtmlEncode(reasons2Keep);
                subscription.MonthlyAllowance = Convert.ToUInt32(tbx_MonthlyAllowance.Text);

                //set the subscription type based on the select promotion subscription type

                SubscriptionFacade subFac = new SubscriptionFacade();

                //save the subscription changes
                if (subscription.SubscriptionId > 0)
                {
                    //update in the subscrption table
                    (new SubscriptionFacade()).UpdateSubscription(subscription);
                }
                else
                {
                    //write to the subscrption table
                    subscription.SubscriptionId = (uint)subFac.InsertSubscription(subscription);
                }

                if (subscription.SubscriptionId  > 0)
                {
                    //delete all existing pairings if existing
                    (new SubscriptionFacade()).DeleteAllSubscription2PassGroupBySubscptID(subscription.SubscriptionId);

                    //add whatever is in the collection
                    foreach (string key in subscription2PassGroup.Keys)
                    {
                        uint passGroupId = Convert.ToUInt32(key);
                        bool inSet = Convert.ToBoolean(subscription2PassGroup[key].ToLower());

                        if (inSet)
                        {
                            //write to the promotion to subscription table
                            (new SubscriptionFacade()).InsertSubscription2PassGroup( passGroupId, subscription.SubscriptionId);
                        }
                    }
                }

                //clear the fields
                ClearSubscriptionItems();

                BindSubscriptionsData(currentPage);

                //make the details panel visible
                divSubscriptionsDetails.Style.Add("display", "none");
            }
            catch (Exception ex)
            {
                // Display err message
                cvBlank.IsValid = false;
                cvBlank.ErrorMessage = "Error saving subscription detials: " + ex.Message;
                valSum.Style.Add("display", "block");
                m_logger.Error("Error saving subscription detials. ", ex);
                return;
            }

        }

        #endregion
    }
}
