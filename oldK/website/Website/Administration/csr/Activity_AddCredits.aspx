<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Activity_AddCredits.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.csr.Activity_AddCredits" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../../usercontrols/Pager.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="CSRNav" Src="csrNav.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="activityNav" Src="activityNav.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="AdminMenu" Src="../../usercontrols/AdministrativeMenu.ascx" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>

<link href="../../css/csr.css" rel="stylesheet" type="text/css" />

<center>

<Kaneva:AdminMenu ID="adminmenu" runat="server" />
<Kaneva:CSRNav runat="server" id="csrNavigation" SubTab="us"/>
<br><br>
<Kaneva:activityNav runat="server" id="activityNavigation" SubTab="addCredits"/>


<table runat="server" id="tblNoCSR" cellpadding="0" cellspacing="0" border="0" width="560">
	<tr><td ALIGN="CENTER">
		<br/><br/><br/>
		<span class="subHead">You must be a CSR to access this section.</span>
	</td></tr>
</table>


<br/><br/>

<ajax:ajaxpanel id="ajpTransaction" runat="server">	

	<div class="errBox black" style="position:relative;display:none;width:500px;margin-top:20px;" runat="server" id="divMessages"></div>

	<br/>

<div style="background-color:White;" id="divCSR" runat="server">

	<table cellSpacing="10" cellPadding="0" width="800" border="0">
		<tbody>
			<tr style="padding-bottom: 10px;">
				<td valign="top" align="right" width="150" style="padding-right:12px;">Current Balance: </td>
				<td valign="top">
					<span style="width:100px;">Rewards:</span><asp:Label ID="lblBalanceRewards" runat="server"></asp:Label><br /> 
					<span style="width:100px;">Credits:</span><asp:label ID="lblBalanceCredits" runat="server"></asp:label>        
				</td>    
			</tr>
			<tr>
				<td valign="middle" align="right" width="150" style="padding-right:12px;">Transaction Type: </td>
				<td align="left">
																			
					<table cellpadding="0" cellspacing="0" border="0">
						<tr>
							<td style="width:200px;"><asp:DropDownList class="formKanevaText" id="drpTransactionTypes" onselectedindexchanged="dropTransactionTypes_OnChange" autopostback="true" style="width:180px" runat="server"></asp:DropDownList></td>
							<td style="width:350px;text-align:right;" valign="middle">
								<span id="spnEventName" runat="server" style="width:135px;text;text-align:right;padding-right:10px;">Survey Name:</span><asp:DropDownList class="formKanevaText" id="drpEventName" style="width:200px;" runat="server"></asp:DropDownList>
							</td>
						</tr>
					</table>
																											   
				</td>
			</tr>
			<tr>
				<td valign="middle" align="right" style="padding-right:12px;">Currency: </td>
				<td align="left">
					<asp:DropDownList class="formKanevaText" id="drpCurrency" style="width:180px" runat="server">
						<asp:ListItem Text="GPOINT (Rewards)" Value="GPOINT"></asp:ListItem>
						<asp:ListItem Text="KPOINT (Credits)" Value="KPOINT"></asp:ListItem>
					</asp:DropDownList>
				</td>
			</tr>
			<tr>
				<td valign="middle" align="right" style="padding-right:12px;">Amount: </td>
				<td align="left">
					<asp:textbox id="txtAmount" runat="server" style="width:180px" maxlength="5"></asp:textbox>	        
				</td>
			</tr>
			<tr>
				<td valign="middle" align="right"></td>
				<td align="left">
					<asp:radiobutton ID="rbCredit" runat="server" GroupName="operationType" Text="credit" Checked="true" ></asp:radiobutton>
					<asp:radiobutton ID="rbDebit" runat="server" GroupName="operationType" Text="debit" ></asp:radiobutton>                            
				</td>
			</tr>
			<tr>
				<td valign="top" align="right" style="padding-right:12px;">Note: </td>
				<td align="left">
					<asp:textbox id="txtNote" runat="server" TextMode="MultiLine" Rows="3" Columns="60"></asp:textbox><br />
					(Note will be prepeneded with the credit type and ammount.)
				</td>
			</tr>		
			<tr>
			<td align="right"></td>
			<td valign="middle"><br />
				<asp:button id="btnSave" runat="Server" onClick="btnSave_Click" CausesValidation="False" Text="Complete Transaction"/> 
				<asp:Label id=lblMessages runat="server"></asp:Label>	
			</td>
			</tr>	
		</tbody>
	</table>
</div>
</ajax:ajaxpanel>


</center>

