///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva;
using log4net;

namespace KlausEnt.KEP.Kaneva.csr
{
	/// <summary>
	/// Summary description for PlayList.
	/// </summary>
	public class PlayList : NoBorderPage
	{
		protected System.Web.UI.WebControls.Panel pnlCSR;
		protected System.Web.UI.HtmlControls.HtmlTable tblNoCSR;
		protected System.Web.UI.WebControls.Button btn_Add;
		protected System.Web.UI.WebControls.DataGrid dgrdPlayList;
		protected System.Web.UI.WebControls.Button btn_Save;
		protected System.Web.UI.WebControls.Label lbl_Messages;

		//enumeration of columns to make code more readable
		//there really should be a centralized database to code class for this
		//that makes column "names" globally available constants
		private const int FEATURED_ASSET_ID = 0;
		private const int ASSET_ID = 1;
		private const int USER_ID = 2;
		private const int START_DATETIME = 3;
		private const int END_DATETIME = 4;
		private const int SHOWN_COUNT = 5;
		private const int STATUS_ID = 6;
		private const int PLAYLIST_TYPE = 7;

		protected PlayList () 
		{
			Title = "CSR - Manage PlayList";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Make sure they are a CSR
			pnlCSR.Visible = (IsCSR ());
			tblNoCSR.Visible = (!IsCSR ());

			this.lbl_Messages.Text = "";

			if (!IsPostBack)
			{
				// Log the CSR activity
				int userId = Convert.ToInt32 (Request ["userId"]);
				UsersUtility.InsertCSRLog (GetUserId (), "CSR - Manage Playlist", 0, userId);
				InitialBind();
			}
			//table must be rebound with any page reload
		}

		private void InitialBind()
		{
			currentPage = 0;
			//dt = StoreUtility.GetFeaturedAssets ();
			dt = StoreUtility.GetFeaturedAssets ();
			BindData();
		}

		/// <summary>
		/// BindData
		/// </summary>
		private void BindData()
		{
			// Set the sortable columns
			string orderby = CurrentSort + " " + CurrentSortOrder;

			//set the current page
			this.dgrdPlayList.CurrentPageIndex = currentPage;

			dt.DefaultView.Sort = orderby;

			dgrdPlayList.DataSource = dt;
			dgrdPlayList.DataBind ();
				
		}

		//allows user to switch the ordering between DESC and ASC
		//based on column clicked
		private void SortSwitch(string newSort)
		{
			if(CurrentSort == newSort)
			{
				if(CurrentSortOrder == "DESC")
				{
					CurrentSortOrder = "ASC";
				}
				else
				{
					CurrentSortOrder = "DESC";
				}
			}
			else
			{
				CurrentSort = newSort;
				CurrentSortOrder = "DESC";
			}
		}

		/// <summary>
		/// DEFAULT_SORT
		/// </summary>
		/// <returns></returns>
		protected override string DEFAULT_SORT
		{
			get
			{
				return "start_datetime";
			}
		}

		/// <summary>
		/// DEFAULT_SORT
		/// </summary>
		/// <returns></returns>
		protected override string DEFAULT_SORT_ORDER
		{
			get
			{
				return "DESC";
			}
		}

		private DataTable dt
		{
			get
			{
				if(Session["_playList"] == null)
				{
					Session["_playList"] = new DataTable();
				}
				return (DataTable)Session["_playList"];
			}
			set
			{
				Session["_playList"] = value;
			}
		}
		

	/*	private PagedDataTable dt
		{
			get
			{
				if(ViewState["_playList"] == null)
				{
					ViewState["_playList"] = new DataTable();
				}
				return (PagedDataTable)ViewState["_playList"];
			}
			set
			{
				ViewState["_playList"] = value;
			}
		}*/

		private int currentPage
		{
			get
			{
				if(Session["_currentPage"] == null)
				{
					Session["_currentPage"] = 0;
				}
				return (int)Session["_currentPage"];
			}
			set
			{
				Session["_currentPage"] = value;
			}
		}

		private void cancelOperations()
		{
			int rowCount = dt.Rows.Count;
			if(dt.Rows[rowCount - 1].RowState.ToString() == "Added")
			{
				dt.Rows[rowCount - 1].Delete();
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btn_Add.Click += new System.EventHandler(this.btn_Add_Click);
			this.dgrdPlayList.ItemCreated += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgrdPlayList_ItemCreated);
			this.dgrdPlayList.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.dgrdPlayList_PageIndexChanged);
			this.dgrdPlayList.CancelCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgrdPlayList_CancelCommand);
			this.dgrdPlayList.EditCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgrdPlayList_EditCommand);
			this.dgrdPlayList.SortCommand += new System.Web.UI.WebControls.DataGridSortCommandEventHandler(this.dgrdPlayList_SortCommand);
			this.dgrdPlayList.UpdateCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgrdPlayList_UpdateCommand);
			this.dgrdPlayList.DeleteCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgrdPlayList_DeleteCommand);
			this.btn_Save.Click += new System.EventHandler(this.btn_Save_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void dgrdPlayList_CancelCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			//cancelOperations();
			this.dgrdPlayList.EditItemIndex = -1;
			BindData();
		}

		private void dgrdPlayList_DeleteCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			//get the selected rows featured item id
			try
			{
				//get feature_asset_id from selected datagrid
				//int selectedRowFIId = Convert.ToInt32(this.dgrdPlayList.Items[e.Item.ItemIndex].Cells[0].ToString());
				
				//logic to handle auto switching of pages when deleting
				if(this.dgrdPlayList.Items.Count == 1)
				{
					if(currentPage != 0)
					{
						currentPage = currentPage - 1;
					}
				}

				//remove row from DataTable dt
				dt.DefaultView.Delete(e.Item.ItemIndex);
				
				//bind changes
				BindData();
			}
			catch(Exception )
			{
			}
		}

		
		private void dgrdPlayList_EditCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			this.dgrdPlayList.EditItemIndex = e.Item.ItemIndex;
			BindData();
		}

		private void dgrdPlayList_ItemCreated(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			//this.dgrdPlayList.EditItemIndex = e.Item.ItemIndex;
			//BindData();
		}

		private void dgrdPlayList_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			currentPage = e.NewPageIndex;
			BindData();
		}

		private void dgrdPlayList_SortCommand(object source, System.Web.UI.WebControls.DataGridSortCommandEventArgs e)
		{
			SortSwitch(e.SortExpression); //sets the sort expression
			this.dgrdPlayList.EditItemIndex = -1; //closes any open edit boxes
			BindData();
		}

		private void dgrdPlayList_UpdateCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			//get the altered row
			DataGridItem alteredRow = e.Item;

			//takes datagrid out of the edit mode
			this.dgrdPlayList.EditItemIndex = -1;	

			//get the respective row from the DataTable
			DataRow[] dtAlteredRow = dt.Select("featured_asset_id = " + alteredRow.Cells[0].Text);
			DataRow drAlteredRow = dtAlteredRow[0];

			try
			{
				drAlteredRow[FEATURED_ASSET_ID] = alteredRow.Cells[FEATURED_ASSET_ID].Text;
				drAlteredRow[ASSET_ID]			= ((TextBox)(alteredRow.Cells[ASSET_ID]).Controls[0]).Text;
				drAlteredRow[USER_ID]			= ((TextBox)(alteredRow.Cells[USER_ID]).Controls[0]).Text;
				drAlteredRow[START_DATETIME]	= ((TextBox)(alteredRow.Cells[START_DATETIME]).Controls[0]).Text;
				drAlteredRow[END_DATETIME]		= ((TextBox)(alteredRow.Cells[END_DATETIME]).Controls[0]).Text;
				drAlteredRow[SHOWN_COUNT]		= ((TextBox)(alteredRow.Cells[SHOWN_COUNT]).Controls[0]).Text;
				drAlteredRow[STATUS_ID]			= ((TextBox)(alteredRow.Cells[STATUS_ID]).Controls[0]).Text;
				drAlteredRow[PLAYLIST_TYPE]		= ((TextBox)(alteredRow.Cells[PLAYLIST_TYPE]).Controls[0]).Text;
			}
			catch(Exception ex)
			{
				lbl_Messages.Text = "Error Processing Update: " + ex.Message;
				lbl_Messages.ForeColor = Color.DarkMagenta;
			}
					
			BindData();
		}

		private void btn_Add_Click(object sender, System.EventArgs e)
		{
			DataRow newRow = dt.NewRow();
			//set default values
			newRow[FEATURED_ASSET_ID]	= -1;
			newRow[ASSET_ID]			= -1;
			newRow[USER_ID]				= this.GetUserId();
			newRow[START_DATETIME]		= DateTime.Now.ToString();
			newRow[END_DATETIME]		= DateTime.Now.AddDays(365.0).ToString();
			newRow[SHOWN_COUNT]			= 0;
			newRow[STATUS_ID]			= Constants.eFEATURED_ASSET_STATUS.ACTIVE;
			newRow[PLAYLIST_TYPE]		= Constants.COOL_ITEM_PLAYLIST_ID_MEDIA;

			//dt.Rows.InsertAt(newRow,(dt.Rows.Count - 1));
			dt.Rows.Add(newRow);

			currentPage = 0;
			this.dgrdPlayList.EditItemIndex = 0;
			BindData();
			
		}

		private void btn_Save_Click(object sender, System.EventArgs e)
		{
			DataTable changes = dt.GetChanges();
			bool errorsOccured = false;

			if(changes != null)
			{
				//enumeration shouldn't cause over head issues due to low concurrent users
				foreach(DataRow dr in changes.Rows)
				{
					//decide which database operation to do
					switch(dr.RowState.ToString().ToUpper())
					{
						case "ADDED":
							try
							{
								StoreUtility.InsertFeaturedAsset(Convert.ToInt32(dr[ASSET_ID]),
									Convert.ToInt32(dr[USER_ID]), Convert.ToDateTime(dr[START_DATETIME]), 
									Convert.ToDateTime(dr[END_DATETIME]), Convert.ToInt32(dr[SHOWN_COUNT]), 
									Convert.ToInt32(dr[STATUS_ID]), Convert.ToInt32(dr[PLAYLIST_TYPE]));
							}
							catch(Exception ex)
							{
								errorsOccured = true;
								lbl_Messages.Text = "Error Saving New Records: " + ex.Message; 
								lbl_Messages.ForeColor = Color.DarkMagenta;
							}
							break;
						case "DELETED":
							try
							{
								dr.RejectChanges(); //this is used to un delete the row so that we can retreive the id
								StoreUtility.DeleteFeaturedAsset(Convert.ToInt32(dr[FEATURED_ASSET_ID]));
							}
							catch(Exception ex)
							{
								errorsOccured = true;
								lbl_Messages.Text = "Error Deleting Record Update: " + ex.Message;
								lbl_Messages.ForeColor = Color.DarkMagenta;
							}
							break;
						case "MODIFIED":
							try
							{
								StoreUtility.UpdateFeaturedAsset(Convert.ToInt32(dr[FEATURED_ASSET_ID]),Convert.ToInt32(dr[ASSET_ID]),
									Convert.ToInt32(dr[USER_ID]), Convert.ToDateTime(dr[START_DATETIME]), 
									Convert.ToDateTime(dr[END_DATETIME]), Convert.ToInt32(dr[SHOWN_COUNT]), 
									Convert.ToInt32(dr[STATUS_ID]), Convert.ToInt32(dr[PLAYLIST_TYPE])) ;
							}
							catch(Exception ex)
							{
								errorsOccured = true;
								lbl_Messages.Text = "Error Updating Record: " + ex.Message;
								lbl_Messages.ForeColor = Color.DarkMagenta;
							}
							break;
					}
				}
				//pulledsaved info from database
				InitialBind();
				if(!errorsOccured)
				{
					lbl_Messages.Text = "Save successful!";
					lbl_Messages.ForeColor = Color.DarkGreen;
				}

			}
		}

	}
}
