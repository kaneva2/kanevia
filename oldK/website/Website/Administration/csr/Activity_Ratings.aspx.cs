///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva;
using log4net;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.CSR
{
	/// <summary>
	/// Summary description for Activity_Ratings.
	/// </summary>
	public class Activity_Ratings : NoBorderPage
	{
		protected Activity_Ratings () 
		{
			Title = "CSR - User Ratings";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Make sure they are a CSR
			pnlCSR.Visible = (IsCSR ());
			tblNoCSR.Visible = (!IsCSR ());

			if (!IsPostBack)
			{
				// Log the CSR activity
				int userId = Convert.ToInt32 (Request ["userId"]);
                SiteMgmtFacade siteMgmtFacade = new SiteMgmtFacade();
                siteMgmtFacade.InsertCSRLog(GetUserId(), "CSR - Viewing user ratings", 0, userId);

				BindData (1, "");
			}

			// Add the javascript for deleting ratings
			string scriptString = "<script language=\"javaScript\">\n<!--\n function DeleteRating (id) {\n";
			scriptString += "   document.all." + hidRatingId.ClientID + ".value = id;\n";            
			scriptString += "	" + ClientScript.GetPostBackEventReference (btnDeleteRate, "") + ";\n";
			scriptString += "}\n// -->\n";
			scriptString += "</script>";

            if (!ClientScript.IsClientScriptBlockRegistered(GetType(), "DeleteRating"))
			{
                ClientScript.RegisterClientScriptBlock(GetType(), "DeleteRating", scriptString);
			}
		}

		/// <summary>
		/// pg_PageChange
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pg_PageChange (object sender, PageChangeEventArgs e)
		{
			BindData (e.PageNumber, "");
		}

		/// <summary>
		/// BindData
		/// </summary>
		/// <param name="pageNumber"></param>
		/// <param name="filter"></param>
		private void BindData (int pageNumber, string filter)
		{
			// Set the sortable columns
			SetHeaderSortText (dgrdRatings);
			string orderby = CurrentSort + " " + CurrentSortOrder;

			int iRatingsPerPage = 30;
			int userId = Convert.ToInt32 (Request ["userId"]);

			PagedDataTable pds = UsersUtility.GetUserRatings (userId, orderby, pageNumber, iRatingsPerPage);
			pgTop.NumberOfPages = Math.Ceiling ((double) pds.TotalCount / iRatingsPerPage).ToString ();
			pgTop.DrawControl ();

			dgrdRatings.DataSource = pds;
			dgrdRatings.DataBind ();
				
			// The results
			lblSearch.Text = GetResultsText (pds.TotalCount, pageNumber, iRatingsPerPage, pds.Rows.Count);
		}

		/// <summary>
		/// They clicked to sort a column
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected override void btnSort_Click (object sender, System.EventArgs e)
		{
			base.btnSort_Click (sender, e);
			BindData (pgTop.CurrentPageNumber, "");
		}

		/// <summary>
		/// DEFAULT_SORT
		/// </summary>
		/// <returns></returns>
		protected override string DEFAULT_SORT
		{
			get
			{
				return "created_date";
			}
		}

		/// <summary>
		/// DEFAULT_SORT
		/// </summary>
		/// <returns></returns>
		protected override string DEFAULT_SORT_ORDER
		{
			get
			{
				return "DESC";
			}
		}

		/// <summary>
		/// They clicked delete a rating
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnDeleteRating_Click (object sender, System.EventArgs e)
		{
			// Log the CSR activity
			int userId = Convert.ToInt32 (Request ["userId"]);
            SiteMgmtFacade siteMgmtFacade = new SiteMgmtFacade();
            siteMgmtFacade.InsertCSRLog(GetUserId(), "CSR - Delete user rating " + hidRatingId.Value, 0, userId);

			StoreUtility.DeleteAssetRating (Convert.ToInt32 (hidRatingId.Value));	
			BindData (pgTop.CurrentPageNumber, "");
		}

		/// <summary>
		/// Return the delete javascript
		/// </summary>
		/// <param name="assetId"></param>
		/// <returns></returns>
		public string GetDeleteScript (int assetRatingId)
		{			
			return "javascript:if (confirm(\"Are you sure you want to delete this rating?\")){DeleteRating (" + assetRatingId + ")};";
		}

		protected Kaneva.Pager pgTop;
		protected Label lblSearch;
		protected DataGrid dgrdRatings;

		// Delete rating
		protected Button btnDeleteRate;
		protected HtmlInputHidden hidRatingId;

		protected HtmlTable tblNoCSR;
		protected Panel pnlCSR;

		#region Web Form Designer generated code

		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);

			// Delete rating controls
			btnDeleteRate = new Button ();
			hidRatingId = new HtmlInputHidden ();

			btnDeleteRate.Style.Add ("DISPLAY", "none");
			btnDeleteRate.Click += new EventHandler (btnDeleteRating_Click);

			m_Form.Controls.Add (btnDeleteRate);
			m_Form.Controls.Add (hidRatingId);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
			pgTop.PageChanged +=new PageChangeEventHandler (pg_PageChange);
		}
		#endregion
	}
}