<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Activity_AddItem.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.csr.Activity_AddItem" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../../usercontrols/Pager.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="CSRNav" Src="csrNav.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="activityNav" Src="activityNav.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="AdminMenu" Src="../../usercontrols/AdministrativeMenu.ascx" %>

<link href="../../css/csr.css" rel="stylesheet" type="text/css" />

<center>
<Kaneva:AdminMenu ID="adminmenu" runat="server" />
<Kaneva:CSRNav runat="server" id="csrNavigation" SubTab="us"/>
<br><br>
<Kaneva:activityNav runat="server" id="activityNavigation" SubTab="addItems"/>
</center>

<table runat="server" id="tblNoCSR" cellpadding="0" cellspacing="0" border="0" width="560">
	<tr><td ALIGN="CENTER">
		<BR><BR><BR>
		<span class="subHead">You must be a CSR to access this section.</span>
	</td></tr>
</table>

<center>
<asp:panel runat="server" ID="pnlCSR">
<BR />
<BR />
<TABLE cellSpacing=5 cellPadding=0 width="80%" border=0>
<TBODY>
    <tr>
        <td valign="middle" align="right" width="100"><strong>Item Name: </strong></td>
	    <td align="left">
	        <asp:DropDownList class="formKanevaText" id="drpItemName" style="width:180px" runat="server"/>
	        <asp:textbox id="txtItemFilter" runat="server"></asp:textbox>
	        <asp:button id="btnItemFilter" text="Filter" runat="server" onClick="btnItemFilter_Click" /> 	        
	        <asp:LinkButton ID="lbtnLoadUgc" text="[Load Dropdown Including UGC]" runat="server" OnClick="lbtnLoadUgc_Click"></asp:LinkButton>
	    </td>
    </tr>
    <tr>
        <td valign="middle" align="right" width="100"><strong>Quantity: </strong></td>
	    <td align="left"><asp:TextBox id="txtQuantity" class="formKanevaText" style="width:150px" MaxLength="5" runat="server"/></td>
    </tr>
    <tr>
        <td valign="middle" align="right" width="100"><strong>Gift: </strong></td>
	    <td align="left"><asp:CheckBox id="chkGift" runat="server"/></td>
	</tr>
	<tr>
	    <td valign="middle" align="right" width="100"><strong>Bank: </strong></td>
        <td align="left"><asp:CheckBox id="chkBank" runat="server"/></td>
	</tr>
	<tr>
	    <td valign="top" align="right" width="100"><strong>Note: </strong></td>
        <td align="left">
            <asp:textbox id="txtNote" runat="server" TextMode="MultiLine" Rows="3" Columns="60"></asp:textbox><br />
            (Note will be prepeneded with the name of the item and quantity added)
        </td>
	</tr>		
	<tr>
	<td align="right"></td>
	<td valign="middle" width="100">
	    <asp:button id="btnSave" runat="Server" onClick="btnSave_Click" CausesValidation="False" Text="Add Item"/> 
	    <asp:Label id=lbl_Messages runat="server"></asp:Label>	
	</td>
	</tr>
	<tr>
	    <td colspan=2><hr /></td>
	</tr>
	<tr>
	    <td align="right"><strong>Package: </strong></td>
	    <td>
	        <asp:DropDownList id="drpPackageList" runat="server"></asp:DropDownList> ( Will only add items associated with the package. No credits will be added.)
	    </td>
	</tr>
    <tr>
	    <td valign="top" align="right" width="100"><strong>Note: </strong></td>
        <td align="left">
            <asp:textbox id="txtNotePackage" runat="server" TextMode="MultiLine" Rows="3" Columns="60"></asp:textbox><br />
            (Note will be prepeneded with the name of the item and quantity added)
        </td>
	</tr>	
	<tr>
	    <td></td>
	    <td >
	        <asp:button id="btnSavePackage" runat="Server" onClick="btnSavePackage_Click" CausesValidation="False" Text="Add Package"/>
	        <asp:Label id=lbl_MessagesPackage runat="server"></asp:Label>	
	    </td>
	    
	</tr>
	
</TBODY>
</TABLE>


</asp:panel>
</center>

