<%@ Control Language="c#" AutoEventWireup="false" Codebehind="activityNav.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.csr.activityNav" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<style>
a { 
	font-weight:normal;
 }
a:visited { 
	font-weight:normal;
 }
a:hover { 
	font-weight:normal;  
 }
</style>

<span class="assetLink"><asp:label runat="server" id="lblWho"/> Activity</span><br /><br />
<span  class="dateStamp">
<a runat="server" class="dateStamp" id="aDetails">Details</a>&nbsp;&nbsp;
<a runat="server" class="dateStamp" id="aNotes">Notes</a>&nbsp;&nbsp;
<a runat="server" class="dateStamp" id="aPosts">Posts</a> (<asp:label runat="server" id="lblPosts"/>)&nbsp;&nbsp;
<a runat="server" class="dateStamp" id="aReplies">Replies</a> (<asp:label runat="server" id="lblReply"/>)&nbsp;&nbsp;
<a runat="server" class="dateStamp" id="aPublished">Published Items</a> (<asp:label runat="server" id="lblItems"/>)&nbsp;&nbsp;
<a runat="server" class="dateStamp" ID="aChannels">Communities</a> (<asp:label runat="server" id="lblChannels"/>)&nbsp;&nbsp;
<a runat="server" class="dateStamp" id="aBlogs">Blogs</a> (<asp:label runat="server" id="lblBlogs"/>)&nbsp;&nbsp;
<a runat="server" class="dateStamp" ID="aAccessPasses">Access Passes</a> (<asp:label runat="server" id="lblAccessPasses"/>)&nbsp;&nbsp;
<a runat="server" class="dateStamp" id="aSales">Items Sold</a> (<asp:label runat="server" id="lblItemsSold"/>)&nbsp;&nbsp;
<br />
<a runat="server" class="dateStamp" ID="aAddItems">Add Items</a>&nbsp;&nbsp;
<a runat="server" class="dateStamp" ID="aInventory">Inventory</a>&nbsp;&nbsp;
<a runat="server" class="dateStamp" ID="aAddCredits">Add Credits</a>&nbsp;&nbsp;
<a runat="server" class="dateStamp" ID="aTransactions">Transactions</a>&nbsp;&nbsp;
<a runat="server" class="dateStamp" ID="aLoginHistory">Login History</a>&nbsp;&nbsp;
<a runat="server" class="dateStamp" ID="aZoneModeration">Zone Moderation</a>&nbsp;&nbsp;
<a runat="server" class="dateStamp" ID="aSuspensions">Suspensions</a>&nbsp;&nbsp;
<a runat="server" class="dateStamp" ID="aDiag">Diagnostics</a>&nbsp;&nbsp;

</span>