<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="nameChanges.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.csr.nameChanges" %>
<%@ Register TagPrefix="Kaneva" TagName="AdminMenu" Src="../../usercontrols/AdministrativeMenu.ascx" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../../usercontrols/Pager.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="CSRNav" Src="csrNav.ascx" %>

<script type="text/javascript" src="../../jscript/yahoo/yahoo-min.js"></script>  
<script type="text/javascript" src="../../jscript/yahoo/event-min.js" ></script>   
<script type="text/javascript" src="../../jscript/yahoo/dom-min.js" ></script>   
<script type="text/javascript" src="../../jscript/yahoo/calendar-min.js"></script>

<script type="text/javascript" language="javascript" src="../../jscript/prototype.js"></script>

<link href="../../css/new.css" rel="stylesheet" type="text/css" />
<link href="../../css/yahoo/calendar.css" type="text/css" rel="stylesheet" />   

<style >
	#cal1Container { display:none; position:absolute; z-index:100;}
</style>
 
<script type="text/javascript"><!--
 
YAHOO.namespace("example.calendar");
    
	function handleAddedDate(type,args,obj) {
		var dates = args[0]; 
		var date = dates[0];
		var year = date[0], month = date[1], day = date[2];
		
		var txtDate1 = $("txtSearch");
		txtDate1.value = year + "-" + month + "-" + day;
		YAHOO.example.calendar.cal1.hide();
	}
	
	function initAddedDate() {
		// Admin Calendar
		YAHOO.example.calendar.cal1 = new YAHOO.widget.Calendar ("cal1", "cal1Container", { iframe:true, zIndex:200, mindate:"1/1/2007", title:"Choose a date:", close:true } );   
		YAHOO.example.calendar.cal1.render();
		
		// Listener to show Admin Calendar when the button is clicked   
		YAHOO.util.Event.addListener("imgItemAddDate", "click", YAHOO.example.calendar.cal1.show, YAHOO.example.calendar.cal1, true);   
		YAHOO.example.calendar.cal1.selectEvent.subscribe(handleAddedDate, YAHOO.example.calendar.cal1, true);
	}
	
//--> </script>  


<body>
<Kaneva:AdminMenu ID="adminmenu" runat="server" />
<Kaneva:CSRNav runat="server" id="csrNavigation" SubTab="nc"/>
<br/>

 <table id="tblUserFame" border="0" cellpadding="0" cellspacing="0" width="1000" style="background-color:#ffffff">
    <tr>
        <td align="center">
			<span style="height:30px; font-size:28px; font-weight:bold">Name Changes</span><br />
			<ajax:ajaxpanel id="ajpError" runat="server">
			<asp:label runat="server" id="lblErrMessage" class="errBox black" style="width:60%;margin-top:20px;text-align:center;"></asp:label></ajax:ajaxpanel>
		</td>
    </tr>
	<tr>
		<td style="height:20px"></td>
    </tr>
    <tr>
         <td align="center">
			
			<ajax:ajaxpanel id="ajpNameChange" runat="server">  
				
				<div width="95%" style="padding: 6px 20px 6px 0px;font-size:10pt;text-align:right;">
					<b>Search by Date:</b> &nbsp;<div id="cal1Container"></div><asp:TextBox runat="server" id="txtSearch" maxlength="10" width="80"></asp:TextBox>&nbsp;
					<img id="imgItemAddDate" onload="initAddedDate();"  src="../../images/blast/cal_16.gif"/>
					<asp:Button id="btnSearch" onclick="btnSearch_Click" causesvalidation="false" runat="server" text="Search"></asp:Button>
				</div>
 
 				<div style="width:95%;margin-bottom:6px;text-align:left;"><span id="spnResultFilter" runat="server"></span></div>

				<asp:gridview id="dgNameChange" runat="server"
					OnRowEditing="dgNameChange_RowEditing" 
					autogeneratecolumns="False" 
					width="95%"
					AllowSorting="True" border="0" cellpadding="4" 
					PageSize="20" AllowPaging="False" Align="center" 
					bordercolor="DarkGray" borderstyle="solid" borderwidth="1px">
					
					<HeaderStyle BackColor="Gray" Font-Underline="True" Font-Bold="True" ForeColor="#000000" HorizontalAlign="Left"></HeaderStyle>
					<RowStyle BackColor="White"></RowStyle>
					<AlternatingRowStyle BackColor="Gainsboro"></AlternatingRowStyle>
					<FooterStyle BackColor="#FFffff"></FooterStyle>
					<Columns>
						<asp:BoundField HeaderText="User Id" DataField="user_id" ReadOnly="True" ConvertEmptyStringToNull="False" itemstyle-horizontalalign="center" itemstyle-width="120px" headerstyle-horizontalalign="center"></asp:BoundField>
						<asp:BoundField HeaderText="User Name" DataField="username" SortExpression="username" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
						<asp:BoundField HeaderText="Old User Name" DataField="username_old" SortExpression="username_old" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
						<asp:BoundField HeaderText="Date Modified" DataField="date_modified" SortExpression="date_modified" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
						<asp:CommandField CausesValidation="False" ShowEditButton="True"></asp:CommandField>        
					</Columns>
				</asp:gridview>
			
				<div style="width:95%;text-align:right;margin:10px 6px 0 0;"><kaneva:pager runat="server" isajaxmode="True" onpagechanged="pg_NameChange_PageChange" id="pgNameChange" maxpagestodisplay="5" shownextprevlabels="true" /></div>
			
			</ajax:ajaxpanel> 
			
			<asp:Label id="lblNoNameChanges" visible="false" runat="server">
				<span style="font-size:16px;color:Red;font-weight:bold;margin:30px 0;width:95%;text-align:center;">No Name Changes Were Found.</span>
			</asp:Label>


			<br /><br />
         
         </td>
    </tr>
	<tr>
		<td align="center">
 
			<ajax:ajaxpanel id="ajpNameChangeDetails" runat="server">

				<div id="divUsernameDetails" style="display:none;background-color:White;width:95%;margin-bottom:20px;" runat="server" >	 

					<div style="width:50%;margin-bottom:20px;">
						<asp:validationsummary cssclass="errBox black" id="valSum" runat="server" showmessagebox="False" showsummary="True"
							displaymode="BulletList" headertext="Please correct the following errors:" forecolor="black"></asp:validationsummary>
						<asp:customvalidator id="cvBlank" runat="server" display="None" enableclientscript="False"></asp:customvalidator>
					</div>		
					
					<asp:requiredfieldvalidator id="rfUsername" runat="server" controltovalidate="txtUsername" text="*" 
						errormessage="You must enter a nickname." display="none"></asp:requiredfieldvalidator>
					<asp:regularexpressionvalidator id="revUsername" runat="server" controltovalidate="txtUsername" text="" display="none" 
						errormessage="Nicknames should be 4-20 characters and a combination of upper and lower-case letters, numbers, and underscores (_) only. Do not use spaces."></asp:regularexpressionvalidator>
				
					<div style="width:60%;text-align:left;">
						<div style="margin-bottom:10px;">Current Username: &nbsp;&nbsp;<span id="spnUsername" runat="server" style="font-weight:bold;font-size:14px;"></span></div>
						<div style="padding-left:20px;">New Username: &nbsp;&nbsp;<asp:textbox class="biginput" id="txtUsername" runat="server" maxlength="20" tabindex="1" width="220"></asp:textbox>
							<asp:imagebutton id="btnCheckUsername" onclick="btnCheckUsername_Click" tabindex="2" runat="server"
									width="124"
									imageurl="~/images/wizard_btn_checkavail.gif"
									alternatetext="Check Availability"
									height="21"
									border="0"
									imagealign="AbsMiddle"
									causesvalidation="False"></asp:imagebutton>&nbsp;&nbsp;<span id="spnCheckUsername" runat="server"></span></div>
						
						<div style="margin-top:6px;padding-left:24px;">New password: &nbsp;&nbsp;<asp:textbox class="biginput" id="txtPassword" runat="server" maxlength="20" textmode="Password" tabindex="13" width="220"></asp:textbox>																								
						<asp:requiredfieldvalidator id="rfPassword" runat="server" controltovalidate="txtPassword" text="*" errormessage="Please enter your password"
							display="none"></asp:requiredfieldvalidator></div>
						
						<div style="margin-top:20px;width:83%;text-align:right;">
						<asp:button id="btnChange" runat="server" onclick="btnChange_Click" text="Change Name"/></div>

					</div>
				
				</div>
				
			</ajax:ajaxpanel>
			
		</td>
	</tr>
</table>



</body>
