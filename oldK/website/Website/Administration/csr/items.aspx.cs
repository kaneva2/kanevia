///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva;
using log4net;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.CSR
{
	/// <summary>
	/// Summary description for items.
	/// </summary>
	public class items : NoBorderPage
	{
		protected items () 
		{
			Title = "CSR - Items";
		}

		private void Page_Load (object sender, System.EventArgs e)
		{

			pnlCSR.Visible = (IsCSR ());
			tblNoCSR.Visible = (!IsCSR ());

			if (!IsPostBack)
			{
				// Log the CSR activity
                SiteMgmtFacade siteMgmtFacade = new SiteMgmtFacade();
                siteMgmtFacade.InsertCSRLog(GetUserId(), "CSR - Viewing all items", 0, 0);

				BindData (1, "");
			}

			// Add the javascript for deleting assets
			string scriptString = "<script language=\"javaScript\">\n<!--\n function DeleteAsset (id) {\n";
			scriptString += "   document.all." + hidAssetId.ClientID + ".value = id;\n";            
			scriptString += "	" + ClientScript.GetPostBackEventReference (btnDeleteAsset, "", false) + ";\n";
			scriptString += "}\n// -->\n";
			scriptString += "</script>";

            if (!ClientScript.IsClientScriptBlockRegistered(GetType (), "DeleteAsset"))
			{
                ClientScript.RegisterClientScriptBlock(GetType(), "DeleteAsset", scriptString);
			}
		}

		/// <summary>
		/// Delete an asset
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnDeleteAsset_Click (Object sender, EventArgs e)
		{
			StoreUtility.DeleteAsset (Convert.ToInt32 (hidAssetId.Value), GetUserId ());
			pg_PageChange (this, new PageChangeEventArgs (pgTop.CurrentPageNumber));
		}



		/// <summary>
		/// pg_PageChange
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pg_PageChange (object sender, PageChangeEventArgs e)
		{
			BindData (e.PageNumber, filStore.CurrentFilter);
		}

		/// <summary>
		/// filStore_FilterChanged
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void filStore_FilterChanged (object sender, FilterChangedEventArgs e)
		{
			pgTop.CurrentPageNumber = 1;
			BindData (1, e.Filter);
		}

		/// <summary>
		/// BindData
		/// </summary>
		/// <param name="pageNumber"></param>
		/// <param name="filter"></param>
		private void BindData (int pageNumber, string filter)
		{

			// Set the sortable columns
			SetHeaderSortText (listAsset);
			string orderby = CurrentSort + " " + CurrentSortOrder;

            PagedDataTable pds = StoreUtility.GetCSRAssets (filter, orderby, pageNumber, filStore.NumberOfPages);
			pgTop.NumberOfPages = Math.Ceiling ((double) pds.TotalCount / filStore.NumberOfPages).ToString ();
			pgTop.DrawControl ();

			listAsset.DataSource = pds;
			listAsset.DataBind ();

			// The results
			lblSearch.Text = GetResultsText (pds.TotalCount, pageNumber, filStore.NumberOfPages, pds.Rows.Count);
		}

		/// <summary>
		/// They clicked to sort a column
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected override void btnSort_Click (object sender, System.EventArgs e)
		{
			base.btnSort_Click (sender, e);
			BindData (pgTop.CurrentPageNumber, filStore.CurrentFilter);
		}

		/// <summary>
		/// DEFAULT_SORT
		/// </summary>
		/// <returns></returns>
		protected override string DEFAULT_SORT
		{
			get
			{
				return "created_date";
			}
		}

		/// <summary>
		/// DEFAULT_SORT
		/// </summary>
		/// <returns></returns>
		protected override string DEFAULT_SORT_ORDER
		{
			get
			{
				return "DESC";
			}
		}

		/// <summary>
		/// Return the delete javascript
		/// </summary>
		/// <param name="assetId"></param>
		/// <returns></returns>
		public string GetDeleteScript (int assetId)
		{			
			return "javascript:if (confirm(\"Are you sure you want to delete this asset?\")){DeleteAsset (" + assetId + ")};";
		}

		/// <summary>
		/// Get download Link
		/// </summary>
		/// <param name="assetId"></param>
		/// <returns></returns>
		public string GetDownloadLink (int assetId)
		{
			return Page.ResolveUrl ("~/Administration/csr/download.aspx?assetId=" + assetId); 
		}   

		/// <summary>
		/// Get the status link
		/// </summary>
		/// <param name="torrentStatus"></param>
		/// <returns></returns>
		protected string GetStatusLink (int torrentStatus)
		{
			return "javascript:window.open('" + ResolveUrl ("~/asset/publishStatus.aspx#" + torrentStatus + "','add','toolbar=no,width=600,height=450,menubar=no,scrollbars=yes,status=yes').focus();return false;");
		}

		/// <summary>
		/// IsDeleted
		/// </summary>
		protected bool IsDeleted (int statusId)
		{
			return (statusId.Equals ((int) Constants.eASSET_STATUS.MARKED_FOR_DELETION));
		}

		/// <summary>
		/// Get transfer Link
		/// </summary>
		public string GetTransferLink (int assetId)
		{
            return Page.ResolveUrl("~/Administration/csr/itemTransfer.aspx?assetId=" + assetId); 
		}   

		protected HtmlTable tblNoCSR;
		protected Panel pnlCSR;

		protected Kaneva.Pager pgTop;
		protected Kaneva.StoreFilter filStore;

		protected DataGrid listAsset;

		protected Label lblPrice, lblPrice2, lblType;
		protected Label lblSearch;

		protected PlaceHolder phBreadCrumb;

		// Delete asset
		protected Button btnDeleteAsset;
		protected HtmlInputHidden hidAssetId;

		// Logger
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);

			// Delete asset controls
			btnDeleteAsset = new Button ();
			hidAssetId = new HtmlInputHidden ();

			btnDeleteAsset.Style.Add ("DISPLAY", "none");
			btnDeleteAsset.Click += new EventHandler (btnDeleteAsset_Click);

			m_Form.Controls.Add (btnDeleteAsset);
			m_Form.Controls.Add (hidAssetId);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
			pgTop.PageChanged +=new PageChangeEventHandler (pg_PageChange);
			filStore.FilterChanged	+=new FilterChangedEventHandler(filStore_FilterChanged);
		}
		#endregion
	}
}
