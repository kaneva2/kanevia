///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Drawing;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Net;
using log4net;

namespace KlausEnt.KEP.Kaneva.csr
{
    public partial class Activity_AddCredits : NoBorderPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            divCSR.Visible = (IsCSR());
            tblNoCSR.Visible = (!IsCSR());

            if (!IsPostBack)
            {
                PopulateTransTypeDropList ();
                
                DisplayMessageBox (false, "", false);

                BindData ();
            }
        }

        /// <summary>
        /// Bind the data
        /// </summary>
        protected void BindData ()
        {
            Int32 userId = Convert.ToInt32(Request["userId"].ToString());
            lblBalanceRewards.Text = UsersUtility.getUserBalance(userId, "GPOINT").ToString();
            lblBalanceCredits.Text = UsersUtility.getUserBalance(userId, "KPOINT").ToString();

            spnEventName.InnerHtml = "";

            drpEventName.Items.Clear ();
            drpEventName.Enabled = false;

            drpTransactionTypes.SelectedIndex = 0;
            txtAmount.Text = "";
            txtNote.Text = "";
        }

        /// <summary>
        /// Populate the Transaction Type Drop List
        /// </summary>
        private void PopulateTransTypeDropList ()
        {
            // Set filter to only show reward event transaction types.
            string filter = "transaction_type IN (6,15,22,23,24)";
            string orderBy = "trans_desc";

            // Get the transaction types
            DataTable dtTransTypes = UsersUtility.GetTransactionTypes (filter, orderBy);

            if (dtTransTypes.Rows.Count > 0)
            {
                drpTransactionTypes.DataSource = dtTransTypes;

                // Set the text a value fields
                drpTransactionTypes.DataTextField = "trans_desc";
                drpTransactionTypes.DataValueField = "transaction_type";

                // Bind the data to the drop list
                drpTransactionTypes.DataBind ();

                // Insert a 'select...' message as first item in list
                drpTransactionTypes.Items.Insert (0, new ListItem ("select transaction type...", "0"));
                drpTransactionTypes.Enabled = true;
            }
            else
            {
                drpTransactionTypes.Items.Insert (0, new ListItem ("no transaction types found", "-1"));
            }
        }

        /// <summary>
        /// Display message box with message
        /// </summary>
        private void DisplayMessageBox (bool isError, string msg)
        {
            DisplayMessageBox (isError, msg, true);
        }
        private void DisplayMessageBox (bool isError, string msg, bool isVisible)
        {
            if (!isVisible)
            {
                divMessages.Style.Add ("display", "none");
            }
            else
            {
                string className = isError ? "errBox black" : "infoBox black";
                divMessages.Attributes.Add ("class", className);
                
                divMessages.InnerHtml = msg; 
                divMessages.Style.Add ("display", "block");
            }
        }

        /// <summary>
        /// Event fired when user clicks the Save button
        /// </summary>
        protected void btnSave_Click (object sender, EventArgs e)                   
        {
            int userId = Convert.ToInt32(Request["userId"].ToString());

            if (txtAmount.Text.Trim() != "" && KanevaGlobals.IsNumeric (txtAmount.Text))
            { 
                double amount = Convert.ToDouble (txtAmount.Text.Trim ());

                // Need to test to see if the new amount is larger than the db field
                if (amount + UsersUtility.getUserBalance(userId, drpCurrency.SelectedValue) > 1000000000)
                {
                    DisplayMessageBox(true, "The amount can not cause the balance to be more or less than 1 Billion.");
                }
                else
                {                                                                   
                    try
                    {
                        // Verify transaction type is selected
                        if (drpTransactionTypes.SelectedValue == "0")
                        {
                            DisplayMessageBox (true, "You must select a Transaction Type.");
                            return;
                        }

                        UInt16 transType = Convert.ToUInt16 (drpTransactionTypes.SelectedValue);

                        // Check to see if Event Name value is selected
                        int rewardEventId = 0;
                        try
                        {
                            if (drpEventName.Enabled && drpEventName.SelectedValue != string.Empty)
                            {
                                rewardEventId = Convert.ToInt32 (drpEventName.SelectedValue);
                            }
                        }
                        catch { }
                        
                        // If transaction tyep is a contest, event, or survey, verify 
                        // that the appropriate type event name is also selected
                        if (transType == Constants.CASH_TT_CONTEST ||
                            transType == Constants.CASH_TT_EVENT ||
                            transType == Constants.CASH_TT_SURVEY)
                        {
                            if (rewardEventId == 0)
                            {
                                DisplayMessageBox (true, "You must select an event name for this transaction.");
                                return;
                            }
                        }

                        if (rbDebit.Checked) { amount = amount * -1; }

                        int wokTransLogId = 0;
                        // Adjust the users balance with amount value
                        GetUserFacade.AdjustUserBalance(userId, drpCurrency.SelectedValue, amount, transType, ref wokTransLogId);


                        // Display Success message
                        DisplayMessageBox(false, "Transaction successfully applied to users balance.");

                        // Insert record into reward_log table
                        UsersUtility.InsertRewardLog (userId, amount, drpCurrency.SelectedValue, transType, rewardEventId, wokTransLogId);

                        // Set user note
                        string strUserNote = "Balance Modification. Trans Type: " + drpTransactionTypes.SelectedItem.Text + " Currency: " + drpCurrency.SelectedValue + " Amount: " + amount;
                        strUserNote += "<br /> Note: " + txtNote.Text;
                        UsersUtility.InsertUserNote(userId, GetUserId(), strUserNote, wokTransLogId);
                    }
                    catch (Exception exc)
                    {
                        m_logger.Error("Error in modifying user balance.", exc);
                        DisplayMessageBox (true, "Error in modifying user balance. " + exc);
                        return;
                    }
                }               
            }
            else
            {
                // Display Error
                DisplayMessageBox (true, "Amount must be a positive numeric value.");
                return;
            }

            BindData ();
        }

        /// <summary>
        /// Event fired when selection changes in the transaction type drop list.
        /// Populates the event name drop list with related transaction type data
        /// </summary>
        public void dropTransactionTypes_OnChange (object sender, EventArgs e)
        {
            try
            {
                // Disable drop list and clear all values from drop list
                drpEventName.Enabled = false;
                drpEventName.Items.Clear ();
                
                spnEventName.InnerHtml = "";
                
                DropDownList ddlTransType = (DropDownList) sender;

                if (ddlTransType != null && ddlTransType.SelectedValue != "")
                {
                    int transTypeId = Convert.ToInt32 (ddlTransType.SelectedValue);

                    if (transTypeId == Constants.CASH_TT_CONTEST ||
                        transTypeId == Constants.CASH_TT_EVENT ||
                        transTypeId == Constants.CASH_TT_SURVEY)
                    {
                        // Get the event list matching transaction type               
                        DataTable dtEvents = UsersUtility.GetKanevaEventsByTransactionType (transTypeId);

                        if (dtEvents.Rows.Count > 0)
                        {
                            drpEventName.DataSource = dtEvents;

                            // Set the text a value fields
                            drpEventName.DataTextField = "event_name";
                            drpEventName.DataValueField = "reward_event_id";

                            // Bind the data to the drop list
                            drpEventName.DataBind ();

                            // Insert a 'select...' message as first item in list
                            drpEventName.Items.Insert (0, new ListItem ("select " + ddlTransType.SelectedItem.ToString ().ToLower () + " name...", "0"));
                            drpEventName.Enabled = true;
                        }
                        else
                        {
                            drpEventName.Items.Insert (0, new ListItem ("no " + ddlTransType.SelectedItem.ToString ().ToLower () + "s found", "-1"));
                        }
                        // Update the label
                        spnEventName.InnerHtml = ddlTransType.SelectedItem + " Name:";
                    }
                }
            }
            catch (Exception)
            {
                drpEventName.Enabled = false;
                spnEventName.InnerHtml = "";
            }
        }

        #region Declerations

        protected Panel pnlCSR;
        protected HtmlTable tblNoCSR;
        protected RadioButton rbDebit, rbCredit;
        protected DropDownList drpTransactionTypes, drpEventName, drpCurrency;
        protected TextBox txtAmount, txtNote;
        protected Label lblBalanceCredits, lblBalanceRewards;
        protected HtmlContainerControl spnEventName;
        protected HtmlContainerControl divMessages, divCSR;

        private ILog m_logger = LogManager.GetLogger("Billing");

        #endregion Declerations
    }
}
