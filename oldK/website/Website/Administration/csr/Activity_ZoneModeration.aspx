<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Activity_ZoneModeration.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.csr.Activity_ZoneModeration" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../../usercontrols/Pager.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="CSRNav" Src="csrNav.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="activityNav" Src="activityNav.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="AdminMenu" Src="../../usercontrols/AdministrativeMenu.ascx" %>

<link href="../../css/csr.css" rel="stylesheet" type="text/css" />

<center>
<Kaneva:AdminMenu ID="adminmenu" runat="server" />
<Kaneva:CSRNav runat="server" id="csrNavigation" SubTab="us"/>
<br><br>
<Kaneva:activityNav runat="server" id="activityNavigation" SubTab="zonemoderation"/>
</center>

<table runat="server" id="tblNoCSR" cellpadding="0" cellspacing="0" border="0" width="560">
	<tr><td ALIGN="CENTER">
		<BR><BR><BR>
		<span class="subHead">You must be a CSR to access this section.</span>
	</td></tr>
</table>

<asp:panel runat="server" ID="pnlCSR"><BR /><BR />
<TABLE cellSpacing=0 cellPadding=0 width="90%" border=0>
<TBODY>
<TR><TD>

    <asp:Label id="lbl_Messages" runat="server" ForeColor="red" Font-Names="Arial"></asp:Label> 
    <br />
    <asp:dropdownlist ID="drpZones" runat="server" DataTextField="name" DataValueField="zone_index_plain" width="300" >
    </asp:dropdownlist>    
    <asp:dropdownlist ID="drpRoles" runat="server">
        <asp:ListItem Value="1">Owner (1)</asp:ListItem>
        <asp:ListItem Value="2">Moderator (2)</asp:ListItem>
    </asp:dropdownlist>

    <asp:Button id="btn_Add" onclick="btn_Add_Click" Text="New Role" runat="server"></asp:Button> 
    
</TD></TR> 
<TR><TD>
    <asp:DataGrid style="BORDER-RIGHT: #cccccc 1px solid; BORDER-TOP: #cccccc 1px solid; BORDER-LEFT: #cccccc 1px solid; BORDER-BOTTOM: #cccccc 1px solid; FONT-FAMILY: arial" 
    id="dgrdRoles" runat="server" OnPageIndexChanged="dgrdRoles_PageIndexChanged" OnSortCommand="dgrdRoles_SortCommand" OnEditCommand="dgrdRoles_EditCommand"  OnDeleteCommand="dgrdRoles_DeleteCommand"
    OnCancelCommand="dgrdRoles_CancelCommand"  PageSize="15" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" border="0" cellpadding="2" Width="600">
    
    <PagerStyle Mode="NumericPages"></PagerStyle>
    <AlternatingItemStyle CssClass="lineItemOddCSR"></AlternatingItemStyle>
    <ItemStyle CssClass="lineItemEvenCSR"></ItemStyle>
    <HeaderStyle CssClass="lineItemColHeadCSR"></HeaderStyle>
    
    <Columns>
    <asp:BoundColumn DataField="zone_index" HeaderText="Zone Id" SortExpression="zone_index" ReadOnly="True">
    <HeaderStyle HorizontalAlign="Left" Font-Bold="True" Width="50" ></HeaderStyle>
    </asp:BoundColumn>
    
    <asp:BoundColumn DataField="name" HeaderText="Zone Name" SortExpression="name" ReadOnly="True">
    <HeaderStyle HorizontalAlign="Left" Font-Bold="True" Width="400"></HeaderStyle>
    </asp:BoundColumn>
    
    <asp:BoundColumn DataField="role" HeaderText="Role" SortExpression="role" ReadOnly="True">
    <HeaderStyle HorizontalAlign="Left" Font-Bold="True" width="50"></HeaderStyle>
    </asp:BoundColumn>
    
    <asp:ButtonColumn ButtonType="LinkButton" Text="Delete"  CommandName="Delete" >
    <HeaderStyle HorizontalAlign="Left" Font-Bold="True" width="50"></HeaderStyle>
    </asp:ButtonColumn>
    </Columns>
    
    </asp:DataGrid> 
</TD></TR>
</TBODY></TABLE>
</asp:panel>
