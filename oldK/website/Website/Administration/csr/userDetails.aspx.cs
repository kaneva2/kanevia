///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva;
using System.Web.Security;
using System.Security.Cryptography;
using log4net;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.CSR
{
	/// <summary>
	/// Summary description for userDetails.
	/// </summary>
	public class userDetails : NoBorderPage
	{
		protected userDetails () 
		{
			Title = "CSR - User Details";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{

			// Make sure they are a CSR
			pnlCSR.Visible = (IsCSR ());
			tblNoCSR.Visible = (!IsCSR ());

			// They must be logged in
			if (!Request.IsAuthenticated)
			{
				Response.Redirect (GetLoginURL ());
				return;
			}

			if (!IsPostBack)
			{
				// Load the current user's data the first time
				int userId = Convert.ToInt32(Request["userId"]);
				LoadData (userId);
			
				lblSize.Text = "Photo width must be 740 pixels wide or less, please keep photo size under " + KanevaGlobals.FormatImageSize (KanevaGlobals.MaxUploadedScreenShotSize);
			}

			// Set up regular expression validators
			revEmail.ValidationExpression = Constants.VALIDATION_REGEX_EMAIL;
			revPassword.ValidationExpression = Constants.VALIDATION_REGEX_PASSWORD;
		}

		/// <summary>
		/// AddDropDownPermissions
		/// </summary>
		private void AddDropDownPermissions (DropDownList drpDropDown)
		{
			drpDropDown.Items.Insert (0, new ListItem ("Only I", "M"));
			drpDropDown.Items.Insert (0, new ListItem ("All friends", "A"));
			drpDropDown.Items.Insert (0, new ListItem ("Kaneva members", "K"));
			drpDropDown.Items.Insert (0, new ListItem ("Everyone", "Y"));
		}

		/// <summary>
		/// Load data to screen
		/// </summary>
		/// <param name="userId"></param>
		private void LoadData (int userId)
		{
            UserFacade userFacade = new UserFacade();
            User user = userFacade.GetUser(userId);

			DataRow drUserProfile = UsersUtility.GetUserProfile (userId);

			// Log the CSR activity
            SiteMgmtFacade siteMgmtFacade = new SiteMgmtFacade();
            siteMgmtFacade.InsertCSRLog(GetUserId(), "CSR - Viewing user details for userId = " + userId + ", username = " + user.Username, 0, userId);

			// Set up the permission dropdowns
			PagedDataTable dtFriendGroups = UsersUtility.GetFriendGroups (userId, "", "name", 1, Int32.MaxValue);

            if (UsersUtility.GetClanName(userId) != null)
            {
                lblClan.Text = UsersUtility.GetClanName(userId).ToString();
            }
            else
            {
                trClan.Visible = false;
            }

            if (user.MatureProfile)
            {
                chkProfileRestrict.Checked = true;
            }
            else
            {
                chkProfileRestrict.Checked = false;
            }


			// Is the email address validated?
            trError.Visible = (user.StatusId.Equals((int)Constants.eUSER_STATUS.REGNOTVALIDATED));
            lblEmail.Text = user.Email;

			drpShowFavs.DataTextField = "name";
			drpShowFavs.DataValueField = "friend_group_id";
			drpShowFavs.DataSource = dtFriendGroups;
			drpShowFavs.DataBind ();
			AddDropDownPermissions (drpShowFavs);

			drpShowProf.DataTextField = "name";
			drpShowProf.DataValueField = "friend_group_id";
			drpShowProf.DataSource = dtFriendGroups;
			drpShowProf.DataBind ();
			AddDropDownPermissions (drpShowProf);

			drpShowOrg.DataTextField = "name";
			drpShowOrg.DataValueField = "friend_group_id";
			drpShowOrg.DataSource = dtFriendGroups;
			drpShowOrg.DataBind ();
			AddDropDownPermissions (drpShowOrg);

			drpShowBlog.DataTextField = "name";
			drpShowBlog.DataValueField = "friend_group_id";
			drpShowBlog.DataSource = dtFriendGroups;
			drpShowBlog.DataBind ();
			AddDropDownPermissions (drpShowBlog);

			drpShowGames.DataTextField = "name";
			drpShowGames.DataValueField = "friend_group_id";
			drpShowGames.DataSource = dtFriendGroups;
			drpShowGames.DataBind ();
			AddDropDownPermissions (drpShowGames);

			drpShowBookmarks.DataTextField = "name";
			drpShowBookmarks.DataValueField = "friend_group_id";
			drpShowBookmarks.DataSource = dtFriendGroups;
			drpShowBookmarks.DataBind ();
			AddDropDownPermissions (drpShowBookmarks);

			drpShowSShots.DataTextField = "name";
			drpShowSShots.DataValueField = "friend_group_id";
			drpShowSShots.DataSource = dtFriendGroups;
			drpShowSShots.DataBind ();
			AddDropDownPermissions (drpShowSShots);

			// Set up rest of form
            lblUsername.Text = KanevaGlobals.TruncateWithEllipsis(user.Username, 19);

            txtUserName.Text = Server.HtmlDecode(user.Username);
            txtDisplayName.Text = Server.HtmlDecode (user.DisplayName);
            txtFirstName.Text = Server.HtmlDecode(user.FirstName);
            txtLastName.Text = Server.HtmlDecode(user.LastName);

            txtEmail.Text = Server.HtmlDecode(user.Email);

            chkShowMature.Checked = user.ShowMature;

            chkShowEmail.Checked = false;
            txtConfirmEmail.Text = Server.HtmlDecode(user.Email);
            txtHomepage2.Text = Server.HtmlDecode(user.Homepage);
			
            SetDropDownIndex(drpMonth, (Convert.ToDateTime (user.BirthDate)).Month.ToString());
            SetDropDownIndex(drpDay, (Convert.ToDateTime (user.BirthDate)).Day.ToString());
            SetDropDownIndex(drpYear, (Convert.ToDateTime (user.BirthDate)).Year.ToString());

			if (IsCurrentUserAdult())
			{
				chkShowMature.Attributes.Add ("onClick", "javascript: if ("+chkShowMature.ClientID+".checked){if (!confirm('I am at least 18 years old. I agree to enter this site of my own free will and for my personal entertainment only. I understand this site contains material that may be considered objectionable. I will not pass any information contained herein to Minors or to anyone who would find such photos offensive. By continuing past this point, I affirm, under penalty of perjury that I am not an agent or attorney for any government, governmental agency or branch, law enforcement agency or military agency or branch. I pledge that my interest in viewing the content of this site is for my personal entertainment only. I specifically pledge not to use my viewing experience against the publisher, provider, or designer of this site. Upon accessing this site, I hereby release, indemnify and hold harmless the providers, owners, models and creators of this site from any and all liability arising from my use of this site. By accessing this site, I am not violating any law of my locality, city, town, state, province, or nation. I subscribe to the principles of the First Amendment which holds that free adult Americans have the right to decide for themselves what they will read and view. By entering this website, you agree to the above terms and conditions.')){	"+chkShowMature.ClientID+".checked = false;}}");}
			else 
			{ 
				chkShowMature.Enabled = false; 
				chkShowMature.Checked = false;
				//drpMonth.Enabled = false;
				//drpDay.Enabled = fals;e
				//drpYear.Enabled = false;
			}

            chkProfileRestrict.Checked = false;
            if (user.MatureProfile)
            {
                chkProfileRestrict.Checked = true;
            }

 
			txtPostalCode.Text = Server.HtmlDecode (user.ZipCode);

			// Set up country dropdown
			drpCountry.DataTextField = "name";
			drpCountry.DataValueField = "countryId";
			drpCountry.DataSource = WebCache.GetCountries ();
			drpCountry.DataBind ();

			drpCountry.Items.Insert (0, new ListItem ("United States", Constants.COUNTRY_CODE_UNITED_STATES));
			drpCountry.Items.Insert (0, new ListItem ("select...", ""));
			
			SetDropDownIndex (drpCountry, Server.HtmlDecode (user.Country));
			SetDropDownIndex (drpGender, Server.HtmlDecode (user.Gender));

			imgAvatar.Src = GetProfileImageURL (user.ThumbnailSmallPath, "sm", user.Gender);

			txtDescription.Text = Server.HtmlDecode (user.Description);

			txtTV.Text = Server.HtmlDecode (drUserProfile ["favorite_tv"].ToString ());
			txtBooks.Text = Server.HtmlDecode (drUserProfile ["favorite_books"].ToString ());
			txtMusic.Text = Server.HtmlDecode (drUserProfile ["favorite_music"].ToString ());
			txtMovies.Text = Server.HtmlDecode (drUserProfile ["favorite_movies"].ToString ());
			txtGames.Text = Server.HtmlDecode (drUserProfile ["favorite_games"].ToString ());
			txtTitle.Text = Server.HtmlDecode (drUserProfile ["prof_title"].ToString ()); 
			txtPosition.Text = Server.HtmlDecode (drUserProfile ["prof_position"].ToString ()); 
			txtIndustry.Text = Server.HtmlDecode (drUserProfile ["prof_industry"].ToString ()); 
			txtCompany.Text = Server.HtmlDecode (drUserProfile ["prof_company"].ToString ());
			txtJobDesc.Text = Server.HtmlDecode (drUserProfile ["prof_job"].ToString ());
			txtSkills.Text = Server.HtmlDecode (drUserProfile ["prof_skills"].ToString ());
			txtCurrAssoc.Text = Server.HtmlDecode (drUserProfile ["org_current_assoc"].ToString ());
			txtPastAssoc.Text = Server.HtmlDecode (drUserProfile ["org_past_assoc"].ToString ());
			
			SetDropDownIndex (drpShowOrg, drUserProfile ["show_org"].ToString (), false);
			SetDropDownIndex (drpShowProf, drUserProfile ["show_prof"].ToString (), false);
			SetDropDownIndex (drpShowFavs, drUserProfile ["show_favorites"].ToString (), false);

			SetDropDownIndex (drpShowBlog, drUserProfile ["show_blog"].ToString (), false);
			SetDropDownIndex (drpShowGames, drUserProfile ["show_games"].ToString (), false);
			SetDropDownIndex (drpShowBookmarks, drUserProfile ["show_bookmarks"].ToString (), false);
			
			lblCash.Text = KanevaGlobals.FormatCurrency (UsersUtility.getUserBalance (userId, Constants.CURR_DOLLAR));
			lblKPoint.Text = KanevaGlobals.FormatKPoints (UsersUtility.getUserBalance (userId, Constants.CURR_KPOINT) + UsersUtility.getUserBalance (userId, Constants.CURR_MPOINT), true, false);
			//lblLicenseKey.Text = drUser ["registration_key"].ToString ();

			// Get the logical accounts under this user if they are a master user
			bool bAccountsVisible = false;

			// Show add and delete buttons
			// If master and it's a logical account
			btnDeleteAccount.Visible = false;
			btnAddAccount.Visible = false;

			if (!IsPostBack)
			{
				drpAccounts.Visible = bAccountsVisible;
			}

            //// License Type
            //DataRow drUserLicense = StoreUtility.GetActiveUserLicenseSubscription (userId);

            //if (drUserLicense == null || drUserLicense ["license_subscription_id"] == null)
            //{
            //    lblLicenseType.Text = "Basic";
            //}
            //else
            //{
            //    switch (Convert.ToInt32 (drUserLicense ["license_subscription_id"]))
            //    {
            //        case (int) Constants.eLICENSE_SUBSCRIPTIONS.PILOT:
            //        {
            //            lblLicenseType.Text = "Enhanced";
            //            break;
            //        }
            //        case (int) Constants.eLICENSE_SUBSCRIPTIONS.DEVELOPER:
            //        {
            //            lblLicenseType.Text = "Basic";
            //            break;
            //        }
            //        case (int) Constants.eLICENSE_SUBSCRIPTIONS.COMMERCIAL:
            //        {
            //            lblLicenseType.Text = "Commercial";
            //            break;
            //        }
            //        default:
            //        {
            //            lblLicenseType.Text = "Unknown";
            //            break;
            //        }

            //    }
				
            //}

            // Get any name changes
            PagedDataTable pdtNameChanges = UsersUtility.GetUserChanges (userId, "username <> username_old", "date_modified DESC", 1, 100);
            if (pdtNameChanges.Rows.Count > 0)
            {
                rptNameChanges.DataSource = pdtNameChanges;
                rptNameChanges.DataBind ();
                divNameChanges.Style.Add ("display", "block");
            }
        
        }

		/// <summary>
		/// btnRegEmail_Click
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnRegEmail_Click (object sender, EventArgs e) 
		{
			int userId = Convert.ToInt32(Request["userId"]);

            UserFacade userFacade = new UserFacade();
            User user = userFacade.GetUser(userId);

            MailUtilityWeb.SendRegistrationEmail(user.Username, user.Email, user.KeyValue);
			ShowErrorOnStartup ("Your validation email has been sent.", false);
		}
		
		/// <summary>
		/// Change account
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void drpAccounts_Change (Object sender, EventArgs e)
		{
			LoadData (Convert.ToInt32 (drpAccounts.SelectedValue));
		}

		/// <summary>
		/// Add a logical account
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnAddAccount_Click (object sender, ImageClickEventArgs e) 
		{
			int userId = Convert.ToInt32(Request["userId"]);
			Response.Redirect (ResolveUrl ( KanevaGlobals.JoinLocation + "?masterUserId=" + userId));
		}

		/// <summary>
		/// Delete the users account
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnDeleteAccount_Click (object sender, ImageClickEventArgs e) 
		{
            UserFacade userFacade = new UserFacade();
			int userId = Convert.ToInt32(Request["userId"]);

            userFacade.UpdateUserStatus(Convert.ToInt32(drpAccounts.SelectedValue), (int)Constants.eUSER_STATUS.DELETED);
			
			drpAccounts.Items.Remove (drpAccounts.SelectedValue);
			LoadData (userId);
		}			

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnChange_Click (object sender, ImageClickEventArgs e) 
		{
			Response.Redirect ("changeMyAccount.aspx");
		}


		/// <summary>
		/// btnUpdate_Click
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
        protected void btnClanDisband_Click(object sender, EventArgs e)
        {
            int userId = Convert.ToInt32(Request["userId"]);
            UsersUtility.ClanDisband(userId);
            LoadData(userId);
        }


        /// <summary>
        /// btnClearProfilePic_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lbRemovePic_Click (object sender, EventArgs e)
        {
            int userId = Convert.ToInt32 (Request["userId"]);
            UsersUtility.ClearUserProfilePic (userId);
            LoadData (userId);
        }


        /// <summary>
		/// btnUpdate_Clikc
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnUpdate_Click (object sender, EventArgs e) 
		{
			if (!Page.IsValid) 
			{
				return;
			}

            UserFacade userFacade = new UserFacade();
			int userId = Convert.ToInt32(Request["userId"]);

			// Log the CSR activity
            SiteMgmtFacade siteMgmtFacade = new SiteMgmtFacade();
            siteMgmtFacade.InsertCSRLog(GetUserId(), "CSR - Updating user details for userId = " + userId + ", username = " + txtUserName.Text, 0, userId);

			// Are they editing logical accounts?
			if (drpAccounts.Visible)
			{
				userId = Convert.ToInt32 (drpAccounts.SelectedValue);
			}

			// Are they changing the password?
			if (txtPassword.Text.Trim ().Length > 0)
			{
				string strPassword = Server.HtmlEncode (txtPassword.Text.Trim ());
				string strUserName = Server.HtmlEncode (txtUserName.Text.Trim ());

				// Update the password
				byte[] salt = new byte[9];
				new RNGCryptoServiceProvider().GetBytes (salt);
				string hashPassword = FormsAuthentication.HashPasswordForStoringInConfigFile (UsersUtility.MakeHash(strPassword+strUserName.ToLower()) + Convert.ToBase64String (salt), "MD5");	    

				byte[] keyvalue = new byte [10];
				new RNGCryptoServiceProvider().GetBytes (keyvalue);

                userFacade.UpdatePassword(userId, hashPassword, Convert.ToBase64String(salt));
			}

			// Did they change email? Now they must validate it.
            User user = userFacade.GetUser(userId);

            if (!Server.HtmlDecode(user.Email).ToLower().Equals(txtEmail.Text.ToLower()))
			{
				// They cannot change thier email if it is already in use
				if (userFacade.EmailExists (txtEmail.Text))
				{
					ShowErrorOnStartup ("The email address you provided is already associated with a Kaneva account. Please provide another email address.");
					return;
				}

                userFacade.UpdateUserStatus(userId, (int)Constants.eUSER_STATUS.REGNOTVALIDATED);
			}

			// Update user
            userFacade.UpdateUserIdentityCSR(userId, Server.HtmlEncode(txtFirstName.Text), Server.HtmlEncode(txtLastName.Text), Server.HtmlEncode(txtDescription.Text), 
				Server.HtmlEncode (drpGender.SelectedValue), Server.HtmlEncode (txtHomepage2.Text), Server.HtmlEncode (txtEmail.Text), chkShowEmail.Checked, chkShowMature.Checked, Server.HtmlEncode (drpCountry.SelectedValue), Server.HtmlEncode (txtPostalCode.Text),
                new DateTime (Convert.ToInt32 (drpYear.SelectedValue), Convert.ToInt32 (drpMonth.SelectedValue), Convert.ToInt32 (drpDay.SelectedValue)), Server.HtmlEncode (txtDisplayName.Text));

			// Update user profile
			UsersUtility.UpdateUserProfile (userId, drpShowFavs.SelectedValue, drpShowProf.SelectedValue, drpShowOrg.SelectedValue, 
                Server.HtmlEncode (txtGames.Text), Server.HtmlEncode (txtMovies.Text), Server.HtmlEncode (txtMusic.Text),
				Server.HtmlEncode (txtBooks.Text), Server.HtmlEncode (txtTV.Text), "", Server.HtmlEncode (txtTitle.Text), 
				Server.HtmlEncode (txtPosition.Text), Server.HtmlEncode (txtIndustry.Text), Server.HtmlEncode (txtCompany.Text), 
				Server.HtmlEncode (txtJobDesc.Text), Server.HtmlEncode (txtSkills.Text), Server.HtmlEncode (txtCurrAssoc.Text), 
                Server.HtmlEncode (txtPastAssoc.Text), drpShowBlog.SelectedValue, drpShowGames.SelectedValue, "Y", 
                drpShowBookmarks.SelectedValue);

			// Update Restriction            
            if(chkProfileRestrict.Checked)
            {
                userFacade.UpdateUserRestriction(userId, 1);
            }
            else
            {
                userFacade.UpdateUserRestriction(userId, 0);
            }             
            
            LoadData (userId);
		}

		/// <summary>
		/// Age Disclaimer
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void chkShowMature_Click () 
		{
			Response.Redirect (ResolveUrl (KanevaGlobals.JoinLocation));
        }


        #region Declerations

        protected TextBox txtUserName;
        protected TextBox txtDisplayName;
		protected TextBox txtEmail;
		protected TextBox txtConfirmEmail;
		protected TextBox txtPassword;
		protected TextBox txtConfirmPassword;
		protected TextBox txtHomepage2, txtDescription;

		protected TextBox txtFirstName;
		protected TextBox txtLastName;
		protected DropDownList drpMonth;
		protected DropDownList drpDay;
		protected DropDownList drpYear;
		protected TextBox txtPostalCode;
		protected DropDownList drpCountry, drpGender;

        protected HtmlContainerControl divNameChanges;
        protected Repeater rptNameChanges;

		protected TextBox txtYahoo, txtMSN, txtAIM, txtICQ;

		protected TextBox txtTV, txtBooks, txtMusic, txtMovies, txtGames;
 
		protected HtmlSelect MainCategory;
		protected HtmlSelect SubCategory;
		protected HtmlSelect OperatingSystem;

		protected TextBox txtTitle, txtPosition, txtIndustry, txtCompany, txtJobDesc, txtSkills, txtCurrAssoc, txtPastAssoc;

		protected Label lblUsername, lblRole, lblCash, lblKPoint;
		protected Label lblEmail;

		protected DropDownList drpShowOrg, drpShowProf, drpShowFavs;
		protected DropDownList drpShowBlog, drpShowGames, drpShowBookmarks, drpShowSShots;

		protected HtmlImage imgAvatar;
		protected HtmlInputFile inpAvatar;
		protected HtmlTable tblNoLogin;
        protected HtmlTableRow trClan;
		protected Panel pnlLoggedIn;

        protected Button btnUpdate, btnRestrict;

		protected DropDownList drpAccounts;

		protected ImageButton btnDeleteAccount, btnAddAccount, btnChange;

        protected Label lblSize, lblLicenseType, lblClan;

        protected CheckBox chkShowEmail, chkShowMature, chkProfileRestrict;
		protected PlaceHolder phBreadCrumb;
		protected RegularExpressionValidator revEmail, revPassword;

		protected HtmlInputRadioButton optNotifyAll, optNotifyAlerts, optNotifyNone;

		protected HtmlTableRow trError;

		protected HtmlTable tblNoCSR;
		protected Panel pnlCSR;

		// Logger
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

        #endregion Declerations


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
