<%@ Page language="c#" Codebehind="Activity_Transactions.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.CSR.Activity_Transactions" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../../usercontrols/Pager.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="CSRNav" Src="csrNav.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="activityNav" Src="activityNav.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="AdminMenu" Src="../../usercontrols/AdministrativeMenu.ascx" %>

<link href="../../css/csr.css" rel="stylesheet" type="text/css" />

<center>
<Kaneva:AdminMenu ID="adminmenu" runat="server" />
<Kaneva:CSRNav runat="server" id="csrNavigation" SubTab="us"/>
<br><br>
<Kaneva:activityNav runat="server" id="activityNavigation" SubTab="transactions"/>
</center>
<br><br>

<table runat="server" id="tblNoCSR" cellpadding="0" cellspacing="0" border="0" width="560">
	<tr><td ALIGN="CENTER">
		<BR><BR><BR>
		<span class="subHead">You must be a CSR to access this section.</span>
	</td></tr>
</table>

<asp:panel runat="server" ID="pnlCSR">


<table cellpadding="0" cellspacing="0" border="0" width="90%">
		<tr>
			<td valign="middle" class="belowFilter" align="left" width="230">&nbsp;</td>
			<td width="100%" align="right" valign="bottom">
				<asp:Label runat="server" id="lblSearch" CssClass="showingCount"/>&nbsp;&nbsp;<Kaneva:Pager runat="server" id="pgTop"/>&nbsp;&nbsp;&nbsp;
			</td>
		</tr>
</table>

<asp:DataGrid runat="server" EnableViewState="False" ShowFooter="False" Width="90%" id="dgrdInventory" cellpadding="0" cellspacing="0" border="0" AutoGenerateColumns="False" AllowSorting="True" style="border: 1px solid #cccccc; font-family:arial">  
	<HeaderStyle CssClass="lineItemColHeadCSR"/>
	<ItemStyle CssClass="lineItemEvenCSR"/>
	<AlternatingItemStyle CssClass="lineItemOddCSR"/>
	<Columns>					
		<asp:TemplateColumn HeaderText="Trans Id" SortExpression="transaction_id" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="top"  ItemStyle-Wrap="true">
			<ItemTemplate>
				<%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "transaction_id").ToString ())%> 
			</ItemTemplate>
		</asp:TemplateColumn>		
		<asp:TemplateColumn HeaderText="Table" SortExpression="tablename" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="top"  ItemStyle-Wrap="true">
			<ItemTemplate>
				<%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "tablename").ToString ())%> 
			</ItemTemplate>
		</asp:TemplateColumn>	
		<asp:TemplateColumn Visible="false" HeaderText="item name" SortExpression="name" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="top"  ItemStyle-Wrap="true">
			<ItemTemplate>
				<%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "name").ToString ())%> 
			</ItemTemplate>
		</asp:TemplateColumn>
		

		<asp:TemplateColumn HeaderText="Date" SortExpression="created_date" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="top"  ItemStyle-Wrap="true">
			<ItemTemplate>
				<%# Server.HtmlDecode(DataBinder.Eval(Container.DataItem, "created_date").ToString())%> 
			</ItemTemplate>
		</asp:TemplateColumn>
		
		<asp:TemplateColumn HeaderText="Currency" SortExpression="currency_type" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="top"  ItemStyle-Wrap="true">
			<ItemTemplate>
				<%# Server.HtmlDecode(DataBinder.Eval(Container.DataItem, "currency_type").ToString())%> 
			</ItemTemplate>
		</asp:TemplateColumn>
				
		<asp:TemplateColumn HeaderText="Amount" SortExpression="transaction_amount" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="top"  ItemStyle-Wrap="true">
			<ItemTemplate>
				<%# Server.HtmlDecode(DataBinder.Eval(Container.DataItem, "transaction_amount").ToString())%> 
			</ItemTemplate>
		</asp:TemplateColumn>
		
		<asp:TemplateColumn HeaderText="Balance" SortExpression="balance" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="top"  ItemStyle-Wrap="true">
			<ItemTemplate>
				<%# DataBinder.Eval(Container.DataItem, "balance")%> 
			</ItemTemplate>
		</asp:TemplateColumn>	
		
        <asp:TemplateColumn HeaderText="Qty." SortExpression="quantity" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="top" ItemStyle-Wrap="true">
			<ItemTemplate>
				<%# DataBinder.Eval(Container.DataItem, "quantity").ToString()%>
			</ItemTemplate>
		</asp:TemplateColumn>
			
        <asp:TemplateColumn HeaderText="New Qty." SortExpression="qty_new" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="top" ItemStyle-Wrap="true">
			<ItemTemplate>
				<%# DataBinder.Eval(Container.DataItem, "qty_new").ToString()%>
			</ItemTemplate>
		</asp:TemplateColumn>
		
		<asp:TemplateColumn HeaderText="Description" SortExpression="trans_desc" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="top"  ItemStyle-Wrap="true">
			<ItemTemplate>
				<%# Server.HtmlDecode(DataBinder.Eval(Container.DataItem, "trans_desc").ToString())%> 
			</ItemTemplate>
		</asp:TemplateColumn>
		
	
		
		<asp:TemplateColumn HeaderText="Item" SortExpression="item_name" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="top"  ItemStyle-Wrap="true">
			<ItemTemplate>
				<%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "item_name").ToString ())%> 
			</ItemTemplate>
		</asp:TemplateColumn>								

		<asp:TemplateColumn HeaderText="GLID" SortExpression="glid" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="top"  ItemStyle-Wrap="true">
			<ItemTemplate>
				<%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "glid").ToString ())%> 
			</ItemTemplate>
		</asp:TemplateColumn>	


		
	</Columns>
</asp:datagrid>

</asp:panel>