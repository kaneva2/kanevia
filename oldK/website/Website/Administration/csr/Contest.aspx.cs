///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva;
using log4net;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.csr
{
	/// <summary>
	/// Summary description for Contest.
	/// </summary>
	public class Contest : NoBorderPage
	{
		protected System.Web.UI.WebControls.Panel pnlCSR;
		protected System.Web.UI.WebControls.Label lbl_UploadStart;
		protected System.Web.UI.WebControls.Label lbl_UploadEnd;
		protected System.Web.UI.WebControls.Label lbl_voteStart;
		protected System.Web.UI.WebControls.Label lbl_VoteEnd;
		protected System.Web.UI.WebControls.Label lbl_LightBoxImage;
		protected System.Web.UI.WebControls.Label lbl_YesImage;
		protected System.Web.UI.WebControls.Label lbl_NoImage;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.TextBox tbx_Terms;
		protected System.Web.UI.WebControls.Label lbl_ExistingContests;
		protected System.Web.UI.WebControls.ListBox lbx_Contests;
		protected System.Web.UI.WebControls.DropDownList ddl_Channels;
		protected System.Web.UI.WebControls.TextBox tbx_searchField;
		protected System.Web.UI.WebControls.Label lbl_ContestChannel;
		protected System.Web.UI.WebControls.Label lbl_ContestName;
		protected System.Web.UI.WebControls.TextBox tbx_ContestName;
		protected System.Web.UI.WebControls.Button btn_ChannelSarch;
		protected System.Web.UI.WebControls.Button btn_ContestFind;
		protected System.Web.UI.WebControls.TextBox TextBox1;
		protected System.Web.UI.WebControls.Calendar cld_UploadStart;
		protected System.Web.UI.WebControls.Calendar cld_UploadEnd;
		protected System.Web.UI.WebControls.Calendar cld_VoteStart;
		protected System.Web.UI.WebControls.Calendar cld_VoteEnd;
		protected System.Web.UI.WebControls.Label lbl_Error;
		protected System.Web.UI.HtmlControls.HtmlInputFile fil_ContestImage;
		protected System.Web.UI.HtmlControls.HtmlInputFile fil_YesButton;
		protected System.Web.UI.HtmlControls.HtmlInputFile fil_NoButton;
		protected System.Web.UI.WebControls.Label lbl_contestImage;
		protected System.Web.UI.WebControls.Label lbl_YesButton;
		protected System.Web.UI.WebControls.Label lbl_NoButton;
		protected System.Web.UI.WebControls.Button btn_New1;
		protected System.Web.UI.WebControls.Button btn_Save1;
		protected System.Web.UI.WebControls.Button btn_New2;
		protected System.Web.UI.WebControls.Button btn_Save2;
		protected System.Web.UI.WebControls.RequiredFieldValidator rfv_ContestName;
		protected System.Web.UI.WebControls.CompareValidator cpv_Channel;
		protected System.Web.UI.WebControls.Button btn_Delete1;
		protected System.Web.UI.WebControls.Button btn_Delete2;
		protected System.Web.UI.WebControls.Button btn_Change1;
		protected System.Web.UI.WebControls.Button btn_Change2;
		protected System.Web.UI.WebControls.Button btn_Change3;
		protected System.Web.UI.HtmlControls.HtmlTable tblNoCSR;
	
		protected Contest () 
		{
			Title = "CSR - Contest Maintenance";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Make sure they are a CSR
			bool isCSR = IsCSR();

			pnlCSR.Visible = (isCSR);
			tblNoCSR.Visible = (!isCSR);

			if (!IsPostBack)
			{
				//skip this if user is not a CSR or admin
				if(isCSR)
				{
					btn_Delete1.Attributes.Add ("onclick", "javascript:if (!confirm(\"Are you sure you want to delete this contest?\")){return false;};"); 
					btn_Delete2.Attributes.Add ("onclick", "javascript:if (!confirm(\"Are you sure you want to delete this contest?\")){return false;};");

					// Log the CSR activity
					int userId = Convert.ToInt32 (Request ["userId"]);
                    SiteMgmtFacade siteMgmtFacade = new SiteMgmtFacade();
                    siteMgmtFacade.InsertCSRLog(GetUserId(), "CSR - Manage Contests", 0, userId);

					BindData ();
				}
			}
		}

		private void BindData()
		{
			lbl_Error.Visible = false;
			GetContest();
		}

		//throws exception - caught by calling function
		private void GetContest()
		{
			DataTable currentContest = null;

			try
			{
				//getall current contest
				currentContest =  StoreUtility.GetAllContests();

				//bind to listview
				lbx_Contests.DataValueField = currentContest.Columns[Constants.ContestTableColumns.ContestId].ColumnName;
				lbx_Contests.DataTextField = currentContest.Columns[Constants.ContestTableColumns.ContestName].ColumnName;
				lbx_Contests.DataSource = currentContest;
				lbx_Contests.DataBind();
				lbx_Contests.Items.Insert(0, new ListItem("- New Contest -", Constants.NOTHING_SELECTED.ToString()));


			}
			catch(Exception ex)
			{
				lbl_Error.Text = ex.Message;
				lbl_Error.Visible = true;
				this.lbl_Error.ForeColor = Color.Red;
			}

		}

		//throws exception - caught by calling function
		private void PopulateChannelsPullDown()
		{
			//get the list of all the channels
			DataTable communities = CommunityUtility.GetAllPublicChannels();
			ddl_Channels.DataValueField = communities.Columns[Constants.ChannelCommunityTableColumns.ChannelId].ColumnName;
			ddl_Channels.DataTextField = communities.Columns[Constants.ChannelCommunityTableColumns.ChannelName].ColumnName;
			ddl_Channels.DataSource = communities;
			ddl_Channels.DataBind();
			ddl_Channels.Items.Insert(0,new ListItem("- SELECT - ", Constants.NOTHING_SELECTED.ToString()));

		}

		private void GatherContestInformation(int contestId)
		{
			lbl_Error.Visible = false;

			try
			{
				PopulateChannelsPullDown();

				//get selected contest's information
				DataRow contestInfo = StoreUtility.GetContestByContestId(contestId);

				if(contestInfo != null)
				{
					//indicate current settings.
					//select channel of selected contest
					try
					{
						this.ddl_Channels.SelectedValue = contestInfo[Constants.ContestTableColumns.ChannelId].ToString();
					}
					catch(Exception)
					{
						this.ddl_Channels.SelectedValue = Constants.NOTHING_SELECTED.ToString();
					}

					//display contests name
					tbx_ContestName.Text = contestInfo[Constants.ContestTableColumns.ContestName].ToString();

					//display the contest terms
					string terms = contestInfo[Constants.ContestTableColumns.ContestTerms].ToString();
					tbx_Terms.Text = (terms == null) ? "" : terms;

					//display contest button image
					string image = contestInfo[Constants.ContestTableColumns.ContestPicture].ToString();
					lbl_contestImage.Visible = true;
					this.btn_Change1.Visible = true;
					lbl_contestImage.Text = (image == null) ? "" : image;
					fil_ContestImage.Visible = false;

					//display contest yes button image
					string yesButton = contestInfo[Constants.ContestTableColumns.YesButton].ToString();
					lbl_YesButton.Visible = true;
					lbl_YesButton.Text = (yesButton == null) ? "" : yesButton;
					fil_YesButton.Visible = false;
					this.btn_Change2.Visible = true;

					//display contest no button image
					string noButton = contestInfo[Constants.ContestTableColumns.NoButton].ToString();
					fil_NoButton.Visible = false;
					lbl_NoButton.Visible = true;
					lbl_NoButton.Text = (noButton == null) ? "" : noButton;
					this.btn_Change3.Visible = true;

					//set the calendars
					cld_UploadStart.SelectedDate = Convert.ToDateTime(contestInfo[Constants.ContestTableColumns.UploadStart]);
					cld_UploadStart.VisibleDate = Convert.ToDateTime(contestInfo[Constants.ContestTableColumns.UploadStart]);
					cld_UploadEnd.SelectedDate = Convert.ToDateTime(contestInfo[Constants.ContestTableColumns.UploadEnd]);
					cld_UploadEnd.VisibleDate = Convert.ToDateTime(contestInfo[Constants.ContestTableColumns.UploadEnd]);
					cld_VoteStart.SelectedDate = Convert.ToDateTime(contestInfo[Constants.ContestTableColumns.VoteStart]);
					cld_VoteStart.VisibleDate = Convert.ToDateTime(contestInfo[Constants.ContestTableColumns.VoteStart]);
					cld_VoteEnd.SelectedDate = Convert.ToDateTime(contestInfo[Constants.ContestTableColumns.VoteEnd]);
					cld_VoteEnd.VisibleDate = Convert.ToDateTime(contestInfo[Constants.ContestTableColumns.VoteEnd]);

				}
				else
				{
					throw new Exception("unable to retrieve contest information");
				}
			}
			catch(Exception ex)
			{
				lbl_Error.Text = ex.Message;
				lbl_Error.Visible = true;
				this.lbl_Error.ForeColor = Color.Red;
			}

		}

		private void ClearFields()
		{
			lbl_Error.Visible = false;

			//reset calendars
			cld_UploadStart.SelectedDate = DateTime.Now;
			cld_UploadStart.VisibleDate =  DateTime.Now;
			cld_UploadEnd.SelectedDate =  DateTime.Now;
			cld_UploadEnd.VisibleDate =  DateTime.Now;
			cld_VoteStart.SelectedDate =  DateTime.Now;
			cld_VoteStart.VisibleDate =  DateTime.Now;
			cld_VoteEnd.SelectedDate =  DateTime.Now;
			cld_VoteEnd.VisibleDate =  DateTime.Now;

			//reset change buttons
			this.btn_Change1.Visible = false;
			this.btn_Change2.Visible = false;
			this.btn_Change3.Visible = false;

			//reset file upload fields
			fil_ContestImage.Visible = true;
			lbl_contestImage.Visible = false;
			fil_YesButton.Visible = true;
			lbl_YesButton.Visible = false;
			fil_NoButton.Visible = true;
			lbl_NoButton.Visible = false;

			//populate drop down if necessary
			if(this.ddl_Channels.Items.Count <= 0)
			{
				PopulateChannelsPullDown();
			}

			//reset name and other fields
			this.ddl_Channels.SelectedValue = Constants.NOTHING_SELECTED.ToString();
			this.lbx_Contests.SelectedValue = Constants.NOTHING_SELECTED.ToString();
			tbx_ContestName.Text = "";
			tbx_Terms.Text = "";

			//disable delete buttons
			this.btn_Delete1.Enabled = false;
			this.btn_Delete2.Enabled = false;

		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.lbx_Contests.SelectedIndexChanged += new System.EventHandler(this.lbx_Contests_SelectedIndexChanged);
			this.btn_New1.Click += new System.EventHandler(this.btn_New1_Click);
			this.btn_Delete1.Click += new System.EventHandler(this.btn_Delete1_Click);
			this.btn_Save1.Click += new System.EventHandler(this.btn_Save1_Click);
			this.btn_Change1.Click += new System.EventHandler(this.btn_Change1_Click);
			this.btn_Change2.Click += new System.EventHandler(this.btn_Change2_Click);
			this.btn_Change3.Click += new System.EventHandler(this.btn_Change3_Click);
			this.btn_New2.Click += new System.EventHandler(this.btn_New1_Click);
			this.btn_Delete2.Click += new System.EventHandler(this.btn_Delete1_Click);
			this.btn_Save2.Click += new System.EventHandler(this.btn_Save1_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void lbx_Contests_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(Convert.ToInt32(this.lbx_Contests.SelectedValue) != -1)
			{
				GatherContestInformation(Convert.ToInt32(this.lbx_Contests.SelectedValue));

				this.btn_Delete1.Enabled = true;
				this.btn_Delete2.Enabled = true;
			}
			else
			{
				ClearFields();
			}
		}

		private void btn_New1_Click(object sender, System.EventArgs e)
		{
			ClearFields();
		}

		private void btn_Save1_Click(object sender, System.EventArgs e)
		{
			if(this.lbx_Contests.SelectedValue == Constants.NOTHING_SELECTED.ToString())
			{
				try
				{
					StoreUtility.InsertNewContest(tbx_ContestName.Text, tbx_Terms.Text, cld_UploadStart.SelectedDate, cld_UploadEnd.SelectedDate, cld_VoteStart.SelectedDate, 
						cld_VoteEnd.SelectedDate, Convert.ToInt32(this.ddl_Channels.SelectedValue), fil_ContestImage.Value, fil_YesButton.Value, fil_NoButton.Value);
					ClearFields();
					this.lbl_Error.Visible = true;
					this.lbl_Error.Text = "Contest successfully added.";
					this.lbl_Error.ForeColor = Color.Green;
					GetContest();
				}
				catch(Exception ex)
				{
					lbl_Error.Text = ex.Message;
					lbl_Error.Visible = true;
				}
			}
			else
			{
				try
				{
					//check for new or changed image paths
					string contestImage = (fil_ContestImage.Value != null && fil_ContestImage.Value != "") ? fil_ContestImage.Value : lbl_contestImage.Text;
					string yesButton = (fil_YesButton.Value != null && fil_YesButton.Value != "") ? fil_YesButton.Value : lbl_YesButton.Text;
					string noButton = (fil_NoButton.Value != null && fil_NoButton.Value != "") ? fil_NoButton.Value : lbl_NoButton.Text;

					StoreUtility.SaveContestChanges(Convert.ToInt32(this.lbx_Contests.SelectedValue), tbx_ContestName.Text, tbx_Terms.Text, cld_UploadStart.SelectedDate, cld_UploadEnd.SelectedDate, 
						cld_VoteStart.SelectedDate, cld_VoteEnd.SelectedDate, Convert.ToInt32(this.ddl_Channels.SelectedValue), contestImage, yesButton, noButton);
					ClearFields();
					this.lbl_Error.Visible = true;
					this.lbl_Error.Text = "Contest successfully updated.";
					this.lbl_Error.ForeColor = Color.Green;
					GetContest();
				}
				catch(Exception ex)
				{
					lbl_Error.Text = ex.Message;
					lbl_Error.Visible = true;
					this.lbl_Error.ForeColor = Color.Red;
				}
			}
		}

		private void btn_Delete1_Click(object sender, System.EventArgs e)
		{
			try
			{ 
				StoreUtility.DeleteCurrentContest(Convert.ToInt32(this.lbx_Contests.SelectedValue));
				this.lbl_Error.ForeColor = Color.Red;
				this.lbl_Error.Visible = true;
				this.lbl_Error.Text = "Contest successfully removed.";
				this.lbl_Error.ForeColor = Color.Green;
				GetContest();
			}
			catch(Exception ex)
			{
				lbl_Error.Text = ex.Message;
				lbl_Error.Visible = true;
			}
		}

		private void btn_Change1_Click(object sender, System.EventArgs e)
		{
			if(fil_ContestImage.Visible)
			{
				fil_ContestImage.Visible = false;
				lbl_contestImage.Visible = true;
				btn_Change1.Text = "Change";
			}
			else
			{
				fil_ContestImage.Visible = true;
				lbl_contestImage.Visible = false;
				btn_Change1.Text = "Cancel";
			}
		}

		private void btn_Change2_Click(object sender, System.EventArgs e)
		{
			if(fil_YesButton.Visible)
			{
				fil_YesButton.Visible = false;
				lbl_YesButton.Visible = true;
				btn_Change2.Text = "Change";
			}
			else
			{
				fil_YesButton.Visible = true;
				lbl_YesButton.Visible = false;
				btn_Change2.Text = "Cancel";
			}
		}

		private void btn_Change3_Click(object sender, System.EventArgs e)
		{
			if(fil_NoButton.Visible)
			{
				fil_NoButton.Visible = false;
				lbl_NoButton.Visible = true;
				btn_Change3.Text = "Change";
			}
			else
			{
				fil_NoButton.Visible = true;
				lbl_NoButton.Visible = false;
				btn_Change3.Text = "Cancel";
			}
		}
	}
}
