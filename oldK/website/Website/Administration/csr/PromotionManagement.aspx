<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PromotionManagement.aspx.cs" ValidateRequest="false" Inherits="KlausEnt.KEP.Kaneva.csr.PromotionManagement" %>
<%@ Register TagPrefix="Kaneva" TagName="CSRNav" Src="csrNav.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="AdminMenu" Src="../../usercontrols/AdministrativeMenu.ascx" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>

<script type="text/javascript" src="../../jscript/yahoo/yahoo-min.js"></script>  
<script type="text/javascript" src="../../jscript/yahoo/event-min.js" ></script>   
<script type="text/javascript" src="../../jscript/yahoo/dom-min.js" ></script>   
<script type="text/javascript" src="../../jscript/yahoo/calendar-min.js"></script>

<script type="text/javascript" language="Javascript" src="../../jscript/PageLayout/colorpicker.js"></script>
<script type="text/javascript" language="javascript" src="../../jscript/prototype.js"></script>

<link href="../../css/new.css" rel="stylesheet" type="text/css" />
<link type="text/css" rel="stylesheet" href="../../css/yahoo/calendar.css">   
<link href="../../css/editWidgets.css" rel="stylesheet" type="text/css" />


<style >
	#cal1Container { display:none; position:absolute; z-index:100;}
	#cal2Container { display:none; position:absolute; z-index:100;}
	#cal3Container { display:none; position:absolute; z-index:100;}
	div.WOKItemsStyle {position:absolute; z-order:8; display:none; border:3px solid #f7ce52; padding:5px; background-color:#ffff99; width:310px; height:350px}
	div.CommunitiesStyle {position:absolute; z-order:8; display:none; border:3px solid #f7ce52; padding:5px; background-color:#ffff99; width:310px; height:350px;}
	div.Preview {position:absolute; z-order:8; display:none; border:3px solid #f7ce52; padding:10px; background-color:#ffff99; width:210px; height:100px}
</style>

<script type="text/javascript">
<!--
    function PreviewPromotion()
    {
        var date = $("tbx_previewDate").value;
        var promoType = $("ddl_PromoOfferType").value;
        if(date != "")
        {
            if((promoType == 1) || (promoType == 6))
            {
                window.open("../../mykaneva/buySpecials.aspx?pass=true&previewDate=" + date, "_blank");
            }
            else
            {
                window.open("../../mykaneva/buySpecials.aspx?previewDate=" + date, "_blank");
            }
        }
        else
        {
            alert("Please choose your date.");
        }
    }
    //-->
 </script>
 
 
<script type="text/javascript"><!--

YAHOO.namespace("example.calendar");
    
	function handleStartDate(type,args,obj) {
		var dates = args[0]; 
		var date = dates[0];
		var year = date[0], month = date[1], day = date[2];
		
		var txtDate1 = document.getElementById("txtPromoStartDate");
		txtDate1.value = month + "-" + day + "-" + year;
		YAHOO.example.calendar.cal1.hide();
	}
	
	function handleEndDate(type,args,obj) {
		var dates = args[0]; 
		var date = dates[0];
		var year = date[0], month = date[1], day = date[2];
		
		var txtDate2 = document.getElementById("txtPromoEndDate");
		txtDate2.value = month + "-" + day + "-" + year;
		YAHOO.example.calendar.cal2.hide();
	}

	function handlePreviewDate(type,args,obj) {
		var dates = args[0]; 
		var date = dates[0];
		var year = date[0], month = date[1], day = date[2];
		
		var txtDate3 = document.getElementById("tbx_previewDate");
		txtDate3.value = month + "-" + day + "-" + year;
		YAHOO.example.calendar.cal3.hide();
	}
	
	function initPromoStart() {
		// Admin Calendar
		YAHOO.example.calendar.cal1 = new YAHOO.widget.Calendar ("cal1", "cal1Container", { iframe:true, zIndex:200, mindate:"1/1/2007", title:"Choose a date:", close:true } );   
		YAHOO.example.calendar.cal1.render();
		
		// Listener to show Admin Calendar when the button is clicked   
		YAHOO.util.Event.addListener("imgSelectStartDate", "click", YAHOO.example.calendar.cal1.show, YAHOO.example.calendar.cal1, true);   
		YAHOO.example.calendar.cal1.selectEvent.subscribe(handleStartDate, YAHOO.example.calendar.cal1, true);
	}
	
	function initPromoEnd() {
		// Event Calendar
		YAHOO.example.calendar.cal2 = new YAHOO.widget.Calendar ("cal2", "cal2Container", { iframe:true, zIndex:1000, mindate:"1/1/2007", title:"Choose a date:", close:true } );   
		YAHOO.example.calendar.cal2.render();
		
		// Listener to show the Event Calendar when the button is clicked   
		YAHOO.util.Event.addListener("imgSelectEndDate", "click", YAHOO.example.calendar.cal2.show, YAHOO.example.calendar.cal2, true);   
		YAHOO.example.calendar.cal2.selectEvent.subscribe(handleEndDate, YAHOO.example.calendar.cal2, true);
	}

	function initPreviewPromo() {
		// Event Calendar
		YAHOO.example.calendar.cal3 = new YAHOO.widget.Calendar ("cal3", "cal3Container", { iframe:true, zIndex:1000, mindate:"1/1/2007", title:"Choose a date:", close:true } );   
		YAHOO.example.calendar.cal3.render();
		
		// Listener to show the Event Calendar when the button is clicked   
		YAHOO.util.Event.addListener("imgSelectPreviewDate", "click", YAHOO.example.calendar.cal3.show, YAHOO.example.calendar.cal3, true);   
		YAHOO.example.calendar.cal3.selectEvent.subscribe(handlePreviewDate, YAHOO.example.calendar.cal3, true);
	}

//--> </script>  

<body>


<Kaneva:AdminMenu ID="adminmenu" runat="server" />
<Kaneva:CSRNav runat="server" id="csrNavigation" SubTab="pm"/>
<br/>

 <table id="tblUserFame" border="0" cellpadding="0" cellspacing="0" width="1000" style="background-color:#ffffff">
    <tr>
        <td align="center">
			<span style="height:30px; font-size:28px; font-weight:bold">Promotion Management</span><br />
		</td>
    </tr>
	<tr>
		<td style="height:20px"></td>
    </tr>
    <tr>
		<td align="center">

			<ajax:ajaxpanel id="ajPromoList" runat="server">

				<div style="width:60%;margin-bottom:20px;">
					<asp:validationsummary cssclass="errBox black" id="valSum" runat="server" showmessagebox="False" showsummary="True"
						displaymode="BulletList" headertext="Please correct the following errors:" forecolor="black"></asp:validationsummary>
					<asp:customvalidator id="cvBlank" runat="server" display="none" enableclientscript="false"></asp:customvalidator>
				</div>
					
				<table id="tbl_AvailableOffers" border="0" cellpadding="0" cellspacing="0" width="95%">
					<tr>
						 <td><asp:Label id="lbl_NoPromotions" visible="false" runat="server"><span style="color:Navy; size:24pt; font-weight:bold">No Promotions Were Found.</span> </asp:Label></td>
					</tr>
					<tr>
						 <td>
				            <div style="width:900px;text-align:left;margin:0 0 12px 0;">
								<asp:LinkButton id="lbn_NewOffer" runat="server" causesvalidation="false" onclick="lbn_NewOffer_Click">Add New Offer</asp:LinkButton>
							</div>
							<asp:gridview id="dg_Promotions" runat="server" onsorting="dg_Promotions_Sorting" 
								OnRowCreated="dg_PromotionsmyGrid_OnRowCreated" autogeneratecolumns="False" width="980px" 
								OnPageIndexChanging="dg_Promotions_PageIndexChanging" OnRowDeleting="dg_Promotions_RowDeleting" 
								OnRowEditing="dg_Promotions_RowEditing" AllowSorting="True" border="0" cellpadding="4" 
								bordercolor="DarkGray" borderstyle="solid" borderwidth="1px"
								PageSize="20" AllowPaging="True">
								
								<RowStyle BackColor="White"></RowStyle>
								<HeaderStyle BackColor="LightGray" Font-Underline="True" Font-Bold="True" ForeColor="#000000" HorizontalAlign="Left"></HeaderStyle>
								<AlternatingRowStyle BackColor="Gainsboro"></AlternatingRowStyle>
								<FooterStyle BackColor="#FFffff"></FooterStyle>
								<Columns>
									<asp:BoundField HeaderText="Promotion ID" DataField="promotion_id" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
									<asp:BoundField HeaderText="Bundle Title" DataField="bundle_title" SortExpression="bundle_title" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
									<asp:BoundField HeaderText="Start Date" DataField="promotion_start" SortExpression="promotion_start" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
									<asp:BoundField HeaderText="End Date" DataField="promotion_end" SortExpression="promotion_end" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
									<asp:BoundField HeaderText="Subheading I" DataField="bundle_subheading1" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
									<asp:BoundField HeaderText="Subheading II" DataField="bundle_subheading2" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
									<asp:BoundField HeaderText="Price (Dollars)" DataField="dollar_amount" SortExpression="dollar_amount" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
									<asp:BoundField HeaderText="Price (Credits)" DataField="kei_point_amount" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
									<asp:BoundField HeaderText="Promotion List Heading" DataField="promotion_list_heading" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
									<asp:BoundField HeaderText="Promotion Description" DataField="promotion_description" SortExpression="promotion_description" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
									<asp:BoundField HeaderText="Highlight Color" DataField="highlight_color" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
									<asp:BoundField HeaderText="Background Color" DataField="special_background_color" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
									<asp:BoundField HeaderText="Background Image" DataField="special_background_image" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
									<asp:BoundField HeaderText="Special Sticker" DataField="special_sticker_image" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
									<asp:BoundField HeaderText="Special Package Label" DataField="promotional_package_label" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
									<asp:BoundField HeaderText="KEI Point" DataField="kei_point_id" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
									<asp:BoundField HeaderText="Free Award Points" DataField="free_points_awarded_amount" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
									<asp:BoundField HeaderText="Free Point Type" DataField="free_kei_point_ID" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
									<asp:BoundField HeaderText="Promotional Type" DataField="promotional_offers_type_id" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
									<asp:BoundField HeaderText="Pass Group" DataField="wok_pass_group_id" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
									<asp:BoundField HeaderText="Sku" DataField="sku" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
									<asp:BoundField HeaderText="Coupon Code" DataField="coupon_code" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
									<asp:BoundField HeaderText="Value of Credits" DataField="value_of_credits" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
									<asp:BoundField HeaderText="Special Font Color" DataField="special_font_color" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
									<asp:BoundField HeaderText="Use Specials Options" DataField="use_display_options" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
									<asp:BoundField HeaderText="Original Price" DataField="original_price" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
									<asp:BoundField HeaderText="AdRotator Path" DataField="adrotator_xml_path" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
									<asp:CommandField CausesValidation="False" ShowEditButton="True"></asp:CommandField>
									<asp:TemplateField>
										<ItemTemplate>
											<asp:LinkButton id="lbn_confirmPromoDelete" Runat="server" causesvalidation="false" OnClientClick="return confirm('Are you sure you want to delete this promotion?');" CommandName="Delete">Delete</asp:LinkButton>
										</ItemTemplate>
									</asp:TemplateField>
				        
								</Columns>
							</asp:gridview><br />
						 </td>
					</tr>
					<tr>
						 <td align="center">
							<div id="divPromotionsDetails" runat="server" >
                
								<h2 id="h2OfferTitle" runat="server" style="width:99%;text-align:left;"></h2><br />
								<table id="tbl_OfferDetails" border="0" cellpadding="5px" cellspacing="1" width="100%">
									<tr>
										<td align="right" style="width:200px;"><label style="color: Red">*</label>Bundle Title:</td>
										<td align="left">
											<asp:TextBox runat="server" maxlength="50" id="tbx_BundleTitle" style="width:300px;" ></asp:TextBox>
											<asp:RequiredFieldValidator runat="server" id="rfvBundleTitle" controltovalidate="tbx_BundleTitle" text="* Please enter a a value." errormessage="The bundle title is required." display="Dynamic"></asp:RequiredFieldValidator>
											<asp:TextBox runat="server" maxlength="50" visible="false" id="tbx_PromoID"></asp:TextBox>
										</td>
									</tr>
									<tr>
										<td align="right" style="width:200px"><label style="color: Red">*</label>Price (Dollars):</td>
										<td align="left">
											<asp:TextBox runat="server" id="tbx_Price"></asp:TextBox>
											<asp:RequiredFieldValidator runat="server" id="rfvPrice" controltovalidate="tbx_Price" text="* Please enter a a value." errormessage="The price (dollars) is required." display="Dynamic"></asp:RequiredFieldValidator>
										</td>
									</tr>
									<tr>
										<td align="right" style="width:200px"><label style="color: Red">*</label>Original Price (Dollars):</td>
										<td align="left">
											<asp:TextBox runat="server" id="tbx_OriginalPrice" Enabled="false"></asp:TextBox>
											<asp:RequiredFieldValidator runat="server" id="rfvOriginalPrice" controltovalidate="tbx_OriginalPrice" text="* Please enter a a value." errormessage="The original price (dollars) is required." display="Dynamic"></asp:RequiredFieldValidator>
										</td>
									</tr>
									 <tr>
										<td align="right" style="width:200px"><label style="color: Red">*</label>KEI Credits:</td>
										<td align="left">
											<asp:TextBox runat="server" id="tbx_Credits" OnTextChanged="tbx_Credits_Changed" AutoPostBack="true"></asp:TextBox>
											<asp:RequiredFieldValidator runat="server" id="rfvCredits" controltovalidate="tbx_Credits" text="* Please enter a a value." errormessage="The price (credits) is required." display="Dynamic"></asp:RequiredFieldValidator>
										</td>
									 </tr>
									<tr>
										<td align="right" style="width:200px">KEI Credits Type:</td>
										<td align="left">
											<asp:DropDownList runat="server" id="ddl_KEICreditType"></asp:DropDownList>
										</td>
									</tr>
									<tr>
										<td align="right" style="width:200px"><label style="color: Red">*</label>Value of Credits:</td>
										<td align="left">
											<asp:TextBox runat="server" id="tbx_ValueOfCredits" Enabled="false"></asp:TextBox>
											<asp:RequiredFieldValidator runat="server" id="rfvVofC" controltovalidate="tbx_ValueOfCredits" text="* Please enter a a value." errormessage="The value of credits is required." display="Dynamic"></asp:RequiredFieldValidator>
										</td>
									</tr>
									<tr>
										<td align="right" style="width:200px"><label style="color: Red">*</label>Free Award Points:</td>
										<td align="left">
											<asp:TextBox runat="server" id="tbx_FreeAwardPoints" OnTextChanged="tbx_Credits_Changed" AutoPostBack="true" ></asp:TextBox>
											<asp:RequiredFieldValidator runat="server" id="rfvFAP" controltovalidate="tbx_FreeAwardPoints" text="* Please enter a a value." errormessage="The number of free points is required." display="Dynamic"></asp:RequiredFieldValidator>
										</td>
									</tr>
									<tr>
										<td align="right" style="width:200px">Free Point Type:</td>
										<td align="left">
											<asp:DropDownList runat="server" id="ddl_FreePointType" OnSelectedIndexChanged="tbx_Credits_Changed" AutoPostBack="true" ></asp:DropDownList>
										</td>
									</tr>
									<tr>
										<td align="right" style="width:200px">Promotional Offer Type:</td>
										<td align="left">
											<asp:DropDownList runat="server" id="ddl_PromoOfferType" OnSelectedIndexChanged="ddl_PromoOfferType_Changed" AutoPostBack="true"></asp:DropDownList>&nbsp;&nbsp;
											<small>Credit, Subscription, Special, One-Time Purchase, First-Time Purchase</small>
										</td>
									</tr>
									<!--<tr>
										<td align="right" style="width:200px">Pass Group Type:</td>
										<td align="left">
											<asp:DropDownList runat="server" id="ddl_PassGroupType" ></asp:DropDownList>
										</td>
									</tr>-->
									<tr>
										<td align="right" style="width:200px">Sku:</td>
										<td align="left">
											<asp:TextBox runat="server" id="tbx_sku"></asp:TextBox>
										</td>
									</tr>
									<tr>
										<td align="right" style="width:200px"><label style="color: Red">*</label>Start Date:</td>
										<td align="left">
											<asp:TextBox enabled="false" ID="txtPromoStartDate" class="formKanevaText" style="width:80px" MaxLength="10" runat="server"/>
											<img id="imgSelectStartDate" name="imgSelectStartDate" onload="initPromoStart();" src="../../images/blast/cal_16.gif"/>
											<asp:Dropdownlist runat="server" ID="drpPromoStartTime" style="width: 80px; vertical-align: top;">
												<asp:ListItem value="00:00:00">12:00 AM</asp:ListItem >
												<asp:ListItem value="00:30:00">12:30 AM</asp:ListItem >
												<asp:ListItem value="01:00:00">1:00 AM</asp:ListItem >
												<asp:ListItem value="01:30:00">1:30 AM</asp:ListItem >
												<asp:ListItem value="02:00:00">2:00 AM</asp:ListItem >
												<asp:ListItem value="02:30:00">2:30 AM</asp:ListItem >
												<asp:ListItem value="03:00:00">3:00 AM</asp:ListItem >
												<asp:ListItem value="03:30:00">3:30 AM</asp:ListItem >
												<asp:ListItem value="04:00:00">4:00 AM</asp:ListItem >
												<asp:ListItem value="04:30:00">4:30 AM</asp:ListItem >
												<asp:ListItem value="05:00:00">5:00 AM</asp:ListItem >
												<asp:ListItem value="05:30:00">5:30 AM</asp:ListItem >
												<asp:ListItem value="06:00:00">6:00 AM</asp:ListItem >
												<asp:ListItem value="06:30:00">6:30 AM</asp:ListItem >
												<asp:ListItem value="07:00:00">7:00 AM</asp:ListItem >
												<asp:ListItem value="07:30:00">7:30 AM</asp:ListItem >
												<asp:ListItem value="08:00:00">8:00 AM</asp:ListItem >
												<asp:ListItem value="08:30:00">8:30 AM</asp:ListItem >
												<asp:ListItem value="09:00:00">9:00 AM</asp:ListItem >
												<asp:ListItem value="09:30:00">9:30 AM</asp:ListItem >
												<asp:ListItem value="10:00:00">10:00 AM</asp:ListItem >
												<asp:ListItem value="10:30:00">10:30 AM</asp:ListItem >
												<asp:ListItem value="11:00:00">11:00 AM</asp:ListItem >
												<asp:ListItem value="11:30:00">11:30 AM</asp:ListItem >
												<asp:ListItem value="12:00:00">12:00 PM</asp:ListItem >
												<asp:ListItem value="12:30:00">12:30 PM</asp:ListItem >
												<asp:ListItem value="13:00:00">1:00 PM</asp:ListItem >
												<asp:ListItem value="13:30:00">1:30 PM</asp:ListItem >
												<asp:ListItem value="14:00:00">2:00 PM</asp:ListItem >
												<asp:ListItem value="14:30:00">2:30 PM</asp:ListItem >
												<asp:ListItem value="15:00:00">3:00 PM</asp:ListItem >
												<asp:ListItem value="15:30:00">3:30 PM</asp:ListItem >
												<asp:ListItem value="16:00:00">4:00 PM</asp:ListItem >
												<asp:ListItem value="16:30:00">4:30 PM</asp:ListItem >
												<asp:ListItem value="17:00:00">5:00 PM</asp:ListItem >
												<asp:ListItem value="17:30:00">5:30 PM</asp:ListItem >
												<asp:ListItem value="18:00:00">6:00 PM</asp:ListItem >
												<asp:ListItem value="18:30:00">6:30 PM</asp:ListItem >
												<asp:ListItem value="19:00:00">7:00 PM</asp:ListItem >
												<asp:ListItem value="19:30:00">7:30 PM</asp:ListItem >
												<asp:ListItem value="20:00:00" Selected="True">8:00 PM</asp:ListItem >
												<asp:ListItem value="20:30:00">8:30 PM</asp:ListItem >
												<asp:ListItem value="21:00:00">9:00 PM</asp:ListItem >
												<asp:ListItem value="21:30:00">9:30 PM</asp:ListItem >
												<asp:ListItem value="22:00:00">10:00 PM</asp:ListItem >
												<asp:ListItem value="22:30:00">10:30 PM</asp:ListItem >
												<asp:ListItem value="23:00:00">11:00 PM</asp:ListItem >
												<asp:ListItem value="23:30:00">11:30 PM</asp:ListItem >
											</asp:Dropdownlist>&nbsp;EST						
											<BR><small>MM-dd-yyyy</small><div id="cal1Container"></div>
											<asp:RequiredFieldValidator runat="server" id="rfvStartDate" controltovalidate="txtPromoStartDate" text="* Please enter a a value." errormessage="The promotion start date is required." display="Dynamic"></asp:RequiredFieldValidator>
									   </td>
									</tr>
									<tr>
										<td align="right" style="width:200px"><label style="color: Red">*</label>End Date:</td>
										<td align="left">
											<asp:TextBox enabled="false" ID="txtPromoEndDate" class="formKanevaText" style="width:80px" MaxLength="10" runat="server"/>
											<img id="imgSelectEndDate" name="imgSelectEndDate" onload="initPromoEnd();" src="../../images/blast/cal_16.gif"/> 
											<asp:Dropdownlist runat="server" ID="drpPromoEndTime" style="width: 80px; vertical-align:top;">
												<asp:ListItem value="00:00:00">12:00 AM</asp:ListItem >
												<asp:ListItem value="00:30:00">12:30 AM</asp:ListItem >
												<asp:ListItem value="01:00:00">1:00 AM</asp:ListItem >
												<asp:ListItem value="01:30:00">1:30 AM</asp:ListItem >
												<asp:ListItem value="02:00:00">2:00 AM</asp:ListItem >
												<asp:ListItem value="02:30:00">2:30 AM</asp:ListItem >
												<asp:ListItem value="03:00:00">3:00 AM</asp:ListItem >
												<asp:ListItem value="03:30:00">3:30 AM</asp:ListItem >
												<asp:ListItem value="04:00:00">4:00 AM</asp:ListItem >
												<asp:ListItem value="04:30:00">4:30 AM</asp:ListItem >
												<asp:ListItem value="05:00:00">5:00 AM</asp:ListItem >
												<asp:ListItem value="05:30:00">5:30 AM</asp:ListItem >
												<asp:ListItem value="06:00:00">6:00 AM</asp:ListItem >
												<asp:ListItem value="06:30:00">6:30 AM</asp:ListItem >
												<asp:ListItem value="07:00:00">7:00 AM</asp:ListItem >
												<asp:ListItem value="07:30:00">7:30 AM</asp:ListItem >
												<asp:ListItem value="08:00:00">8:00 AM</asp:ListItem >
												<asp:ListItem value="08:30:00">8:30 AM</asp:ListItem >
												<asp:ListItem value="09:00:00">9:00 AM</asp:ListItem >
												<asp:ListItem value="09:30:00">9:30 AM</asp:ListItem >
												<asp:ListItem value="10:00:00">10:00 AM</asp:ListItem >
												<asp:ListItem value="10:30:00">10:30 AM</asp:ListItem >
												<asp:ListItem value="11:00:00">11:00 AM</asp:ListItem >
												<asp:ListItem value="11:30:00">11:30 AM</asp:ListItem >
												<asp:ListItem value="12:00:00">12:00 PM</asp:ListItem >
												<asp:ListItem value="12:30:00">12:30 PM</asp:ListItem >
												<asp:ListItem value="13:00:00">1:00 PM</asp:ListItem >
												<asp:ListItem value="13:30:00">1:30 PM</asp:ListItem >
												<asp:ListItem value="14:00:00">2:00 PM</asp:ListItem >
												<asp:ListItem value="14:30:00">2:30 PM</asp:ListItem >
												<asp:ListItem value="15:00:00">3:00 PM</asp:ListItem >
												<asp:ListItem value="15:30:00">3:30 PM</asp:ListItem >
												<asp:ListItem value="16:00:00">4:00 PM</asp:ListItem >
												<asp:ListItem value="16:30:00">4:30 PM</asp:ListItem >
												<asp:ListItem value="17:00:00">5:00 PM</asp:ListItem >
												<asp:ListItem value="17:30:00">5:30 PM</asp:ListItem >
												<asp:ListItem value="18:00:00">6:00 PM</asp:ListItem >
												<asp:ListItem value="18:30:00">6:30 PM</asp:ListItem >
												<asp:ListItem value="19:00:00">7:00 PM</asp:ListItem >
												<asp:ListItem value="19:30:00">7:30 PM</asp:ListItem >
												<asp:ListItem value="20:00:00" Selected="True">8:00 PM</asp:ListItem >
												<asp:ListItem value="20:30:00">8:30 PM</asp:ListItem >
												<asp:ListItem value="21:00:00">9:00 PM</asp:ListItem >
												<asp:ListItem value="21:30:00">9:30 PM</asp:ListItem >
												<asp:ListItem value="22:00:00">10:00 PM</asp:ListItem >
												<asp:ListItem value="22:30:00">10:30 PM</asp:ListItem >
												<asp:ListItem value="23:00:00">11:00 PM</asp:ListItem >
												<asp:ListItem value="23:30:00">11:30 PM</asp:ListItem >
											</asp:Dropdownlist>&nbsp;EST
											<BR><small>MM-dd-yyyy</small><div id="cal2Container"></div>
											<asp:RequiredFieldValidator runat="server" id="rfvEndDate" controltovalidate="txtPromoEndDate" text="* Please enter a a value." errormessage="The promotion end date is required." display="Dynamic"></asp:RequiredFieldValidator>
										</td>
									</tr>
									<tr>
										<td align="right" style="width:200px">Coupon Code:</td>
										<td align="left">
											<asp:TextBox runat="server" id="tbx_CouponCode"></asp:TextBox>
											This is for giving away a package during the join process.
										</td>
									</tr>									
									<tr id="tr_SubscriptionData" runat="server" visible="false">
						                <td colspan="2" align="left">
						                    <table border="0" cellpadding="5px" cellspacing="1" width="100%">
						        	            <tr>
						        		            <td align="right" style="width:220px">.</td>
							                        <td align="left" style="font-size:9pt; color:Red; font-weight:bold; font-style:italic">Please choose the existing subscription(s) to use for the basis of the promotion</td>
							                    </tr>
						        	            <tr>
						        		            <td align="right" style="width:220px">Existing Subscriptions:</td>
							                        <td align="left">
                                                        <asp:GridView runat="server" id="dgd_Subscriptions" AllowPaging="false" cellpadding="2" border="1" BorderColor="Navy" autogeneratecolumns="False" width="280px">
                                                            <RowStyle BackColor="White"></RowStyle>
                                                            <AlternatingRowStyle BackColor="Gainsboro"></AlternatingRowStyle>
                                                            <FooterStyle BackColor="#FFffff"></FooterStyle>
                                                            <Columns>
                                                                <asp:TemplateField>
                                                                    <ItemStyle Width=10px />
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox runat="server" id="cbx_inpromotion" autopostback="true" checked='<%# DataBinder.Eval(Container.DataItem, "cbx_inpromotion")%>' oncheckedchanged="cbx_inpromotion_CheckedChanged"></asp:CheckBox>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>                                       
                                                                <asp:TemplateField visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label runat="server" id="lbl_subscriptionId" text='<%# DataBinder.Eval(Container.DataItem, "subscription_id")%>' ></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>                                       
                                                                <asp:BoundField ReadOnly="True" HeaderText="Subscription" DataField="name"></asp:BoundField>
                                                            </Columns>
                                                        </asp:GridView>
							                        </td>
							                    </tr>
						                    </table>
						                </td>
									 </tr>																
									 <tr>
										<td align="center" colspan="2">
											
											<div id="divCommunitiesSearch" runat="server" class="CommunitiesStyle" style="margin:-50px 0 0 450px;">
												<div style="padding-bottom:6px;"><asp:TextBox runat="server" id="tbx_CommunityFilter"></asp:TextBox>&nbsp;&nbsp;<asp:LinkButton id="lbn_CommunitySearch" causesvalidation="false" onclick="lbn_CommunitySearch_Click" runat="server">Search</asp:LinkButton></div>
												<asp:GridView runat="server" id="Communities" AllowPaging="True" cellpadding="4" PageSize="10" 
													border="0" autogeneratecolumns="False" width="300px" 
													bordercolor="DarkGray" borderstyle="solid" borderwidth="1px"
													OnPageIndexChanging="Communities_PageIndexChanging" 
													onselectedindexchanged="Communities_SelectedIndexChanged">
													<RowStyle BackColor="White"></RowStyle>
													<HeaderStyle BackColor="LightGray" Font-Underline="True" Font-Bold="True" ForeColor="#000000" HorizontalAlign="Left"></HeaderStyle>
													<AlternatingRowStyle BackColor="Gainsboro"></AlternatingRowStyle>
													<FooterStyle BackColor="#FFffff"></FooterStyle>
													<Columns>
														<asp:BoundField HeaderText="ID" visible="true" DataField="community_id"></asp:BoundField>
														<asp:BoundField ReadOnly="True" HeaderText="WOK Item" DataField="name">
															<ItemStyle Font-Italic="false"></ItemStyle>
															<HeaderStyle Font-Bold="True"></HeaderStyle>
														</asp:BoundField>
														<asp:CommandField CausesValidation="False" ShowCancelButton="False" ShowSelectButton="True"></asp:CommandField>
													</Columns>
												</asp:GridView>
												<div><br /><asp:LinkButton id="lbn_CommunitySearchCancel" causesvalidation="false" onclick="lbn_CommunitySearchCancel_Click" runat="server">Cancel</asp:LinkButton></div>
											</div>
											<div id="divWOKItemsSearch" runat="server" class="WOKItemsStyle" style="margin:-50px 0 0 20px;">
												<div style="padding-bottom:6px;"><asp:TextBox runat="server" id="tbx_WOKSearchFilter"></asp:TextBox>&nbsp;&nbsp;<asp:LinkButton id="lbn_WOKSearch" causesvalidation="false" onclick="lbn_WOKSearch_Click" runat="server">Search</asp:LinkButton></div>
												<asp:GridView runat="server" id="WOKItems" AllowPaging="True" cellpadding="4" PageSize="10" 
													border="0" autogeneratecolumns="False" width="300px"
													bordercolor="DarkGray" borderstyle="solid" borderwidth="1px" 
													OnPageIndexChanging="WOKItems_PageIndexChanging" 
													onselectedindexchanged="WOKItems_SelectedIndexChanged" >
													
													<RowStyle BackColor="White"></RowStyle>
													<HeaderStyle BackColor="LightGray" Font-Underline="True" Font-Bold="True" ForeColor="#000000" HorizontalAlign="Left"></HeaderStyle>
													<AlternatingRowStyle BackColor="Gainsboro"></AlternatingRowStyle>
													<FooterStyle BackColor="#FFffff"></FooterStyle>
													<Columns>
														<asp:BoundField HeaderText="ID" visible="true" DataField="global_id">
															<ItemStyle Width="0px"></ItemStyle>
															<HeaderStyle Width="0px"></HeaderStyle>
														</asp:BoundField>
														<asp:BoundField ReadOnly="True" HeaderText="WOK Item" DataField="name">
															<ItemStyle Font-Italic="false"></ItemStyle>
															<HeaderStyle Font-Bold="True"></HeaderStyle>
														</asp:BoundField>
														<asp:CommandField CausesValidation="False" ShowCancelButton="False" ShowSelectButton="True"></asp:CommandField>
													</Columns>
												</asp:GridView>
												<div><br /><asp:LinkButton id="lbn_WOKSearchCancel" causesvalidation="false" onclick="lbn_WOKSearchCancel_Click" runat="server">Cancel</asp:LinkButton></div>
											</div>
											<div style="width:900px;text-align:left;margin:10px 0 0 0;">
												<asp:LinkButton id="lbn_NewItems" runat="server" causesvalidation="false" onclick="lbn_NewItems_Click">Add New Items</asp:LinkButton>
											</div>
											<div style="width:60%;margin-bottom:20px;">
												<asp:validationsummary cssclass="errBox black" id="valSumItems" runat="server" showmessagebox="False" showsummary="True"
													displaymode="BulletList" headertext="Please correct the following errors:" forecolor="black"></asp:validationsummary>
											</div>
											<asp:gridview id="dg_PromotionalItems" runat="server" autogeneratecolumns="False" width="900px" 
												AllowSorting="false" border="0" cellpadding="3" AllowPaging="false" 
												OnRowDeleting="dg_PromotionalItems_RowDeleting" 
												OnRowEditing="dg_PromotionalItems_RowEditing" 
												bordercolor="DarkGray" borderstyle="solid" borderwidth="1px">
												
												<RowStyle BackColor="White"></RowStyle>
												<HeaderStyle BackColor="LightGray" Font-Underline="True" Font-Bold="True" ForeColor="#000000" HorizontalAlign="Left"></HeaderStyle>
												<AlternatingRowStyle BackColor="Gainsboro"></AlternatingRowStyle>
												<Columns>
													<asp:BoundField HeaderText="Promotion Offer ID" DataField="promotion_offer_id" ReadOnly="True" visible="false"></asp:BoundField>
													<asp:BoundField HeaderText="Promotion ID" DataField="promotion_id" ReadOnly="True" visible="false"></asp:BoundField>
													<asp:BoundField HeaderText="Quantity" DataField="quantity" ReadOnly="True" ></asp:BoundField>
													<asp:BoundField HeaderText="WOK Item ID" DataField="wok_item_id" ReadOnly="True" ></asp:BoundField>
													<asp:BoundField HeaderText="Item Description" DataField="item_description" ReadOnly="True" >
														<ItemStyle width="150px"></ItemStyle>
													</asp:BoundField>
													<asp:BoundField HeaderText="Alternate Description" DataField="alternate_description" ReadOnly="True" >
														<ItemStyle width="150px"></ItemStyle>
													</asp:BoundField>
													<asp:BoundField HeaderText="Gender" DataField="gender" ReadOnly="True" ></asp:BoundField>
													<asp:BoundField HeaderText="Gift Card" DataField="gift_card_id" ReadOnly="True" ></asp:BoundField>
													<asp:BoundField HeaderText="Community" DataField="community_id" ReadOnly="True" ></asp:BoundField>
													<asp:BoundField HeaderText="Market Price" DataField="market_price" ReadOnly="True" ></asp:BoundField>
													<asp:CommandField CausesValidation="False" ShowEditButton="True" ></asp:CommandField>
													<asp:TemplateField>
														<ItemTemplate>
															<asp:LinkButton id="lbn_confirmPromoItemDelete" causesvalidation="false" Runat="server" OnClientClick="return confirm('Are you sure you want to delete this promotional item?');" CommandName="Delete">Delete</asp:LinkButton>
														</ItemTemplate>
													</asp:TemplateField>
												</Columns>
											</asp:gridview>
											<asp:panel id="pnl_PromotionsWOKItemsAdd" runat="server" >
												<span style="border:solid 1px DarkGray;padding-top:2-px;">
												<table border="0" cellpadding="4" cellspacing="0" width="900px">
													<tr><td><img runat="server" src="~/images/spacer.gif" width="1" height="10" /></td></tr>
													<tr>
														<td colspan="4" ><u>Item Quantity</u><asp:Label runat="server" style="color:red" id="quantityRequired" visible="false">*</asp:Label></td>
														<td><u>WOK Item Id</u><asp:Label runat="server" style="color:red" id="itemIDRequired" visible="false">*</asp:Label></td>
														<td><u>Item Description</u><asp:Label runat="server" style="color:red" id="descriptionRequired" visible="false">*</asp:Label></td>
														<td><u>Alternate Description</u></td>
														<td ><u>Gender</u></td>
														<td ><u>Gift Card</u></td>
														<td ><u>Community</u></td>
														<td colspan="3"><u>MarketPrice</u></td>
													</tr>
													<tr>
														<td style="padding: 0px" ><asp:Label runat="server" id="selectedItemIndex" visible="false"></asp:Label></td>
														<td style="padding: 0px" ><asp:TextBox runat="server" maxlength="50" id="tbx_promoofferid" visible="false"></asp:TextBox></td>
														<td style="padding: 0px" ><asp:TextBox runat="server" maxlength="50" id="tbx_promoidWOK" visible="false"></asp:TextBox></td>
														<td><asp:TextBox runat="server" maxlength="50" width="40px" id="tbx_itemquantity"></asp:TextBox></td>
														<td valign="middle"><asp:Label runat="server" width="20px" maxlength="50" id="lbl_wokitemid" /><asp:LinkButton id="lbn_BrowseWoKItems" runat="server" style="padding: 4px; vertical-align:top" causesvalidation="false" onclick="ChooseWOKItem_Click">Select</asp:LinkButton></td>
														<td><asp:Label runat="server" maxlength="125" width="100px" id="lbl_itemdesciption" /></td>
														<td><asp:TextBox runat="server" maxlength="125" width="100px" id="tbx_alternatedesciption" ></asp:TextBox></td>
														<td><asp:DropDownList runat="server" maxlength="50" width="60px" id="ddl_gender"></asp:DropDownList></td>
														<td><asp:DropDownList runat="server" maxlength="50" width="100px" id="ddl_giftcards"></asp:DropDownList></td>
														<td><asp:Label runat="server" width="60px" maxlength="50" id="lbl_community" /><asp:LinkButton id="lbn_BrowseCommnities" runat="server" style="padding: 4px; vertical-align:top" causesvalidation="false" onclick="ChooseCommunity_Click">Select</asp:LinkButton><asp:Label runat="server" visible="false" id="lbl_communityId" /></td>
														<td><asp:TextBox runat="server" maxlength="50" width="40px" id="tbx_marketprice"></asp:TextBox></td>
														<td><asp:Button runat="server" id="btn_ItemsAdd" Text="Add" causesvalidation="false" onclick="btn_ItemsAdd_Click"></asp:Button></td>
														<td><asp:Button runat="server" id="btn_ItemsCancel" Text="Cancel" causesvalidation="false" onclick="btn_ItemsCancel_Click"></asp:Button><asp:Label runat="server" id="WOKItemValidation" /></td>
													</tr>
													<tr><td><img runat="server" src="~/images/spacer.gif" width="1" height="10" /></td></tr>
												</table>
												</span>
											</asp:panel>
										</td>
									</tr>
									<tr>
									    <td ><h2 style="margin:20 0 14px 30px">Specials Options Section:</h2></td>
										<td align="left">
                                            <asp:CheckBox ID="cbx_UseOptions" runat="server" Text="Use the below display options. (images are used regardless)" />
										</td>
									</tr>
									<tr>
										<td align="right" style="width:200px; color:#000066">Subhead line 1:</td>
										<td align="left">
											<asp:TextBox runat="server" maxlength="50" style="width:300px;" id="tbx_SubheadLine1"></asp:TextBox>
										</td>
									</tr>
									<tr>
										<td align="right" style="width:200px; color:#000066">Subhead line 2:</td>
										<td align="left">
											<asp:TextBox runat="server" maxlength="50" style="width:300px;" id="tbx_SubheadLine2"></asp:TextBox>
										</td>
									</tr>
									<tr>
										<td align="right" style="width:200px; color:#000066">Promotion Package Label:</td>
										<td align="left"><asp:TextBox runat="server" maxlength="30" style="width:300px;" id="tbx_PromotionPackageLabel"></asp:TextBox></td>
									</tr>
									<tr>
										<td align="right" style="width:200px; color:#000066"><label style="color: Red">*</label>Promotion Heading&nbsp;<br />(Radio Button):</td>
										<td align="left">
											<asp:TextBox runat="server" maxlength="50" style="width:300px;" id="tbx_PromotionListHeading"></asp:TextBox>
											<asp:RequiredFieldValidator runat="server" id="rfvPromoListHeading" controltovalidate="tbx_PromotionListHeading" text="* Please enter a a value." errormessage="The promotion list heading is required." display="Dynamic"></asp:RequiredFieldValidator>
										</td>
									</tr>
									 <tr>
										<td align="right" style="width:200px; color:#000066">Promotion Description:</td>
										<td align="left"><asp:TextBox runat="server" maxlength="30" style="width:300px;" id="tbx_PromotionDescr"></asp:TextBox></td>
									</tr>
									<tr>
										<td align="right" style="width:200px; color:#000066">Highlight Color:</td>
										<td align="left"><asp:TextBox runat="server" id="tbx_HighlightColor" maxlength="7" ></asp:TextBox>&nbsp<asp:hyperlink class="colorPicker" id="tbx_HighlightColorAppClrPickerStr" Runat="server" ImageUrl="..\..\images\colorpickmask.gif"></asp:hyperlink></td>
									</tr>
									<tr>
										<td align="right" style="width:200px; color:#000066">Background Color:</td>
										<td align="left"><asp:TextBox runat="server" id="tbx_BackgroundColor" maxlength="7"></asp:TextBox>&nbsp<asp:hyperlink class="colorPicker" id="tbx_BackgroundColorAppClrPickerStr" Runat="server" ImageUrl="..\..\images\colorpickmask.gif"></asp:hyperlink></td>
									</tr>
									<tr>
										<td align="right" style="width:200px; color:#000066">Special Font Color:</td>
										<td align="left"><asp:TextBox runat="server" id="tbx_FontColor" maxlength="7"></asp:TextBox>&nbsp<asp:hyperlink class="colorPicker" id="tbx_FontColorAppClrPickerStr" Runat="server" ImageUrl="..\..\images\colorpickmask.gif"></asp:hyperlink>
											<div style="float:right;width:32%;display:block;">
												<div id="divPreview" runat="server" class="Preview" style="text-align:center;">
													<div>
														<asp:TextBox runat="server" id="tbx_previewDate" enabled="false"></asp:TextBox>
														<img id="imgpreviewDate" name="imgSelectPreviewDate" onload="initPreviewPromo();" src="../../images/blast/cal_16.gif"/>
														<BR><small>MM-dd-yyyy</small><div id="cal3Container"></div>
													</div>
													<div>
														<br />
														<asp:LinkButton id="lbn_PreviewPromotion" causesvalidation="false" onclientclick="PreviewPromotion()" onclick="lbn_PreviewPromotion_Click" runat="server">Preview</asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;
														<asp:LinkButton id="lbn_CancelPreview" causesvalidation="false" onclick="lbn_CancelPreview_Click" runat="server">Cancel</asp:LinkButton>
													</div>
												</div>
											</div>
										</td>
									</tr>
									<tr>
										<td align="right" style="width:200px; color:#000066">Background Image:</td>
										<td align="left">
											<input type="file" name="browseBCKGRND" style="display: none;" />
											<input class="formKanevaText" id="inp_backgroundImage" style="WIDTH: 400px" type="text" size="43" name="inp_backgroundImage" runat="server" /> 
											<input type="button" onclick="browseBCKGRND.click();inp_backgroundImage.value=browseBCKGRND.value;" value="Browse..."  />                         
									   </td>
									</tr>
									<tr>
										<td align="right" style="width:200px; color:#000066">AdRotator Path:</td>
										<td align="left">
											<asp:TextBox runat="server" style="width:300px;" id="tbx_adRotatorPath"></asp:TextBox>                         
									   </td>
									</tr>
									<tr>
										<td align="right" style="width:200px; color:#000066">Sticker Image:</td>
										<td align="left">
											<input type="file" name="browseSTKR" style="display: none;" />
											<input class="formKanevaText" id="inp_stickerImage" style="WIDTH: 400px" type="text" size="43" name="inp_stickerImage" runat="server" /> 
											<input type="button" onclick="browseSTKR.click();inp_stickerImage.value=browseSTKR.value;" value="Browse..."  />                         
										</td>
									</tr>
									<tr>
										<td align="right" colspan="2" style="padding-right:20px; padding-top:30px">
											<table>
												<tr>
													<td><asp:Button runat="server" id="btn_Preview" Text="Preview" causesvalidation="false" onclick="btn_Preview_Click"></asp:Button></td>
													<td><asp:Button runat="server" id="btn_Cancel" Text="Cancel" causesvalidation="false" onclick="btn_Cancel_Click"></asp:Button></td>
													<td style="padding-left:30px"><asp:Button runat="server" id="btn_Save" Text="Submit" onclick="btn_Save_Click"></asp:Button></td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
            
							</div>
						 </td>
					</tr>
				</table>




			</ajax:ajaxpanel>
			<br />
		</td>
	</tr>
</table>


<br /><br />

</body>