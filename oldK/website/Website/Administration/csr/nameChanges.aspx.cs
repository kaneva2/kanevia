///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using log4net;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using System.Security.Cryptography;

namespace KlausEnt.KEP.Kaneva.csr
{
    public partial class nameChanges : NoBorderPage
    {
        protected nameChanges () 
	    {
		    Title = "CSR - User Name Changes";
		}

        protected void Page_Load (object sender, EventArgs e)
        {
            // Redirect if user is not admin
            if (!IsCSR ())
            {
                Response.Redirect ("~/default.aspx");
            }
            
            this.lblErrMessage.Text = "";

            if (!IsPostBack)
            {
                NameChangeSearchFilter = "";

                // bind the member fame data
                BindData (1);
            }

            // Set up regular expression validators
            revUsername.ValidationExpression = Constants.VALIDATION_REGEX_USERNAME;
        }

        /// <summary>
        /// BindMemberFameData
        /// </summary>
        private void BindData (int pageNumber)
        {
            try
            {
                //make the details panel visible
                divUsernameDetails.Style["display"] = "none";

                if (txtSearch.Text.Trim () == "")
                {
                    txtSearch.Text = DateTime.Now.ToString ("yyyy-MM-dd");
                }

                DateTime startDate = KanevaGlobals.UnFormatDate (txtSearch.Text + " 00:00:00");
                DateTime endDate = KanevaGlobals.UnFormatDate (txtSearch.Text + " 23:59:59");

                // Set the sortable columns
                string orderby = CurrentSort + " " + CurrentSortOrder;
                int pageSize = 60;

                NameChangeSearchFilter = " username != username_old ";

                PagedDataTable pdtNameChanges = UsersUtility.GetUserChanges (startDate, endDate, NameChangeSearchFilter, orderby, pageNumber, pageSize);
                if (pdtNameChanges != null)
                {
                    dgNameChange.DataSource = pdtNameChanges;
                    dgNameChange.DataBind ();

                    spnResultFilter.InnerText = "Showing results for: " + txtSearch.Text;

                    // Set current page
                    pgNameChange.CurrentPageNumber = pageNumber;
                    pgNameChange.NumberOfPages = Math.Ceiling ((double) pdtNameChanges.TotalCount / pageSize).ToString ();
                    pgNameChange.DrawControl ();


                    if (pdtNameChanges.TotalCount == 0)
                    {
                        spnResultFilter.InnerText = "No results found for " + txtSearch.Text + ".";
                    }
                }

                lblErrMessage.Visible = false;
            }
            catch (Exception ex)
            {
                lblErrMessage.Text = "Error: " + ex.Message;
                lblErrMessage.Visible = true;
            }
        }
        
        /// <summary>
        /// PopulateWOKItemEdit
        /// </summary>
        private void GetNameChangeDetails (GridViewRow gvr, int pageNumber)
        {
            this.UserId = Convert.ToInt32 (gvr.Cells[0].Text);

            //make the details panel visible
            divUsernameDetails.Style["display"] = "block";

            spnUsername.InnerText = gvr.Cells[1].Text;

            if (this.HilitedRowIndex > -1)
            {
                dgNameChange.Rows[this.HilitedRowIndex].BackColor = this.HilitedRowBackColor;
            }
            
            this.HilitedRowIndex = gvr.RowIndex;
            this.HilitedRowBackColor = gvr.BackColor;

            gvr.BackColor = System.Drawing.Color.Yellow;
        }

        #region Event Handlers

        protected void btnSearch_Click (object sender, EventArgs e)
        {
            string searchText = txtSearch.Text.Trim ();

            try
            {
                currentPage = 1;
                BindData (currentPage);
            }
            catch
            {
                //error not valid date
                lblErrMessage.Text = searchText + " is not a valid date.";
                lblErrMessage.Visible = true;
            }
        }
        protected void btnChange_Click (object sender, EventArgs e)
        {
            UserFacade userFacade = new UserFacade();

            try
            {
                if (!Page.IsValid)
                {
                    return;
                }

                string newUserName = txtUsername.Text.Trim ();
                string oldUserName = spnUsername.InnerText;
                int userId = this.UserId;

                // Verify username is still available
                if (UsersUtility.GetUserIdFromUsername (newUserName) > 0)
                {
                    cvBlank.IsValid = false;
                    cvBlank.ErrorMessage = "The Nickname already exists. Please change your Nickname.";
                    valSum.Style.Add ("display", "block");
                    return;
                }

                string strPassword = Server.HtmlEncode (txtPassword.Text.Trim ());

                int ret = userFacade.UpdateUserName(userId, oldUserName, newUserName);
                if (ret == 0)
                {
                    // Update the password with new username
                    byte[] salt = new byte[9];
                    new RNGCryptoServiceProvider ().GetBytes (salt);
                    string hashPassword = FormsAuthentication.HashPasswordForStoringInConfigFile (UsersUtility.MakeHash (strPassword + newUserName.ToLower ()) + Convert.ToBase64String (salt), "MD5");

                    byte[] keyvalue = new byte[10];
                    new RNGCryptoServiceProvider ().GetBytes (keyvalue);

                    // Update user record with new hashed password
                    userFacade.UpdatePassword(userId, hashPassword, Convert.ToBase64String(salt));

                    // Send user email with new username & password
                    User user = userFacade.GetUser (newUserName);

                    if (user != null)
                    {
                        try
                        {
                            // Send out link to username reset email
                            MailUtilityWeb.SendUsernameResetEmail (user.Username, strPassword, user.Email);
                        }
                        catch (Exception ex)
                        {
                            cvBlank.IsValid = false;
                            cvBlank.ErrorMessage = "An error was encountered trying to send email to " + newUserName + ". Error : " + ex;
                            valSum.Style.Add ("display", "block");
                            return;
                        }
                    }
                    else
                    {
                        cvBlank.IsValid = false;
                        cvBlank.ErrorMessage = "Could not retrieve user with username = " + newUserName + ".  The email was NOT sent to " + newUserName + ".";
                        valSum.Style.Add ("display", "block");
                        return;
                    }
                }
                else
                {
                    throw new Exception ("Failed to update user record with new name.");
                }

                // show success message***************
            }
            catch (Exception ex)
            {
                // Display err message
                cvBlank.IsValid = false;
                cvBlank.ErrorMessage = "An error was encountered while processing your request. Err: " + ex.Message;
                valSum.Style.Add ("display", "block");
                m_logger.Error ("Error saving name change: userId=" + this.UserId.ToString (), ex);
            }
        }
        protected void pg_NameChange_PageChange (object sender, PageChangeEventArgs e)
        {
            BindData (e.PageNumber);
        }
        protected void btnCheckUsername_Click (object sender, ImageClickEventArgs e)
        {
            revUsername.Validate ();

            if (revUsername.IsValid)
            {
                if (txtUsername.Text.Trim () != string.Empty)
                {
                    if (UsersUtility.GetUserIdFromUsername (txtUsername.Text.Trim ()) > 0)
                    {
                        spnCheckUsername.InnerText = "Not available";
                        spnCheckUsername.Attributes.Add ("class", "failure");
                    }
                    else
                    {
                        spnCheckUsername.InnerText = "Available";
                        spnCheckUsername.Attributes.Add ("class", "success");
                    }
                }
            }
            else
            {
                spnCheckUsername.InnerText = "Invalid name";
                spnCheckUsername.Attributes.Add ("class", "failure");
            }
        }

        protected void dgNameChange_RowEditing (object source, GridViewEditEventArgs e)
        {
            //pass the selected row and populate the fields
            GetNameChangeDetails (dgNameChange.Rows[e.NewEditIndex], 1);
        }

        #endregion

        #region Properties

        private int currentPage
        {
            get
            {
                if (ViewState["_currentPage"] == null)
                {
                    ViewState["_currentPage"] = 0;
                }
                return (int) ViewState["_currentPage"];
            }
            set
            {
                ViewState["_currentPage"] = value;
            }
        }
        private string NameChangeSearchFilter
        {
            get
            {
                if (ViewState["_NameChangeSearchFilter"] == null)
                {
                    ViewState["_NameChangeSearchFilter"] = "";
                }
                return ViewState["_NameChangeSearchFilter"].ToString ();
            }
            set
            {
                ViewState["_NameChangeSearchFilter"] = value;
            }
        }
        private int UserId
        {
            get
            {
                if (ViewState["_NameChangeUserId"] == null)
                {
                    ViewState["_NameChangeUserId"] = 0;
                }
                return Convert.ToInt32 (ViewState["_NameChangeUserId"]);
            }
            set
            {
                ViewState["_NameChangeUserId"] = value;
            }
        }
        private int HilitedRowIndex
        {
            get
            {
                if (ViewState["_HilitedRowIndex"] == null)
                {
                    ViewState["_HilitedRowIndex"] = -1;
                }
                return Convert.ToInt32 (ViewState["_HilitedRowIndex"]);
            }
            set
            {
                ViewState["_HilitedRowIndex"] = value;
            }
        }
        private System.Drawing.Color HilitedRowBackColor
        {
            get
            {
                if (ViewState["_HilitedRowBackColor"] == null)
                {
                    ViewState["_HilitedRowBackColor"] = 0;
                }
                return (System.Drawing.Color) (ViewState["_HilitedRowBackColor"]);
            }
            set
            {
                ViewState["_HilitedRowBackColor"] = value;
            }
        }
        /// <summary>
        /// DEFAULT_SORT
        /// </summary>
        /// <returns></returns>
        protected override string DEFAULT_SORT
        {
            get
            {
                return "date_modified";
            }
        }

        /// <summary>
        /// DEFAULT_SORT_ORDER
        /// </summary>
        /// <returns></returns>
        protected override string DEFAULT_SORT_ORDER
        {
            get
            {
                return "DESC";
            }
        }

        #endregion

        #region Declerations

        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

        #endregion
    }
}
