///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Drawing;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.csr
{
    public partial class AccessPassEdit : NoBorderPage
    {
        protected System.Web.UI.WebControls.Panel pnlCSR;
        protected System.Web.UI.HtmlControls.HtmlTable tblNoCSR;
        protected System.Web.UI.WebControls.DropDownList drpPassType;
        protected System.Web.UI.WebControls.TextBox txtCreatedDate;
        protected System.Web.UI.WebControls.CheckBox chkGiveItems;
        protected System.Web.UI.WebControls.Label lbl_Messages;
        protected System.Web.UI.WebControls.Label lbl_PassType;
        protected System.Web.UI.WebControls.Label lbl_CreatedDate;
        protected System.Web.UI.WebControls.Label lbl_GiveItems;
        protected System.Web.UI.WebControls.Button btnSave;

        protected void Page_Load(object sender, EventArgs e)
        {
            // Make sure they are a CSR
            pnlCSR.Visible = (IsCSR());
            tblNoCSR.Visible = (!IsCSR());

            this.lbl_Messages.Text = "";

            if (!IsPostBack)
            {
                // Log the CSR activity
                userId = Convert.ToInt32(Request["userId"]);
                SiteMgmtFacade siteMgmtFacade = new SiteMgmtFacade();
                siteMgmtFacade.InsertCSRLog(GetUserId(), "CSR - Access Pass Edit", 0, userId);

                passGroupId = Convert.ToInt32(Request["passGroupId"]);

                int deletePass = Convert.ToInt32(Request["delete"]);

                if (deletePass == 1)
                {
                    int returnValue = 0;
                    returnValue = UsersUtility.DeleteAccessPass(userId, passGroupId);

                    lbl_PassType.Visible = false;
                    lbl_CreatedDate.Visible = false;
                    lbl_GiveItems.Visible = false;
                    drpPassType.Visible = false;
                    txtCreatedDate.Visible = false;
                    chkGiveItems.Visible = false;
                    btnSave.Visible = false;
                    
                    if (returnValue == 1)
                    {
                        lbl_Messages.Text = "Access Pass Deleted";
                        lbl_Messages.ForeColor = Color.Red;
                    }
                    else
                    {
                        lbl_Messages.Text = "Error attempting to delete Access Pass";
                        lbl_Messages.ForeColor = Color.Red;
                    }
                }
                else
                {
                    if (passGroupId == -1)
                    {
                        newPass = true;
                        passGroupId = 1;
                        txtCreatedDate.Text = DateTime.Now.ToString();
                        chkGiveItems.Checked = true;
                    }
                    else
                    {
                        newPass = false;
                        DataTable dtPassGroup = UsersUtility.GetAccessPasses(userId, passGroupId);
                        if (dtPassGroup.Rows.Count > 0)
                        {
                            txtCreatedDate.Text = dtPassGroup.Rows[0]["created_date"].ToString();
                            chkGiveItems.Checked = false;
                        }
                    }

                    drpPassType.DataValueField = "pass_group_id";
                    drpPassType.DataTextField = "name";
                    drpPassType.DataSource = UsersUtility.GetAccessPasses();
                    drpPassType.DataBind();
                    SetDropDownIndex(drpPassType, passGroupId.ToString());
                }
            }
        }

        private bool newPass
        {
            get
            {
                if (Session["_newPass"] == null)
                {
                    Session["_newPass"] = false;
                }
                return (bool)Session["_newPass"];
            }
            set
            {
                Session["_newPass"] = value;
            }
        }

        private int passGroupId
        {
            get
            {
                if (Session["_passGroupId"] == null)
                {
                    Session["_passGroupId"] = -1;
                }
                return (int)Session["_passGroupId"];
            }
            set
            {
                Session["_passGroupId"] = value;
            }
        }

        private int userId
        {
            get
            {
                if (Session["_userId"] == null)
                {
                    Session["_userId"] = -1;
                }
                return (int)Session["_userId"];
            }
            set
            {
                Session["_userId"] = value;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (newPass)
            {
                int returnValue = 0;
                returnValue = UsersUtility.CreateNewAccessPass(userId, Convert.ToInt32(drpPassType.SelectedValue), txtCreatedDate.Text, chkGiveItems.Checked);

                if (returnValue == 0)
                {
                    lbl_Messages.Text = "Error Creating New Access Pass";
                    lbl_Messages.ForeColor = Color.Red;
                }
                else
                {
                    lbl_Messages.Text = "Success!";
                    lbl_Messages.ForeColor = Color.Red;

                    // Set user note
                    string strUserNote = "Added New Access Pass.<br /> Type: " + drpPassType.SelectedItem.Text + "<br /> Items: " + chkGiveItems.Checked.ToString() ;                    
                    UsersUtility.InsertUserNote(userId, GetUserId(), strUserNote);
                }
            }
            else
            {
                int returnValue = 0;
                returnValue = UsersUtility.UpdateAccessPass(userId, passGroupId, Convert.ToInt32(drpPassType.SelectedValue), txtCreatedDate.Text, chkGiveItems.Checked);

                if (returnValue != 1)
                {
                    lbl_Messages.Text = "Error Updating Access Pass";
                    lbl_Messages.ForeColor = Color.Red;
                }
                else
                {
                    lbl_Messages.Text = "Success!";
                    lbl_Messages.ForeColor = Color.Red;
                }
            }
        }
    }
}
