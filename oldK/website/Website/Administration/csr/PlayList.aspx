<%@ Page language="c#" SmartNavigation=true Codebehind="PlayList.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.csr.PlayList" %>
<%@ Register TagPrefix="Kaneva" TagName="AdminMenu" Src="../../usercontrols/AdministrativeMenu.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="activityNav" Src="activityNav.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="CSRNav" Src="csrNav.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../../usercontrols/Pager.ascx" %>

<center>
    <Kaneva:AdminMenu ID="adminmenu" runat="server" />
    <KANEVA:CSRNAV id="csrNavigation" runat="server" SubTab="pl"></KANEVA:CSRNAV><br>
	<br>
</center>


 <table id="tblUserFame" border="0" cellpadding="0" cellspacing="0" width="1000" style="background-color:#ffffff">
	<tr>
		<td style="height:20px"></td>
    </tr>
    <tr>
         <td align="center">

<table id="tblNoCSR" cellSpacing="0" cellPadding="0" border="0" runat="server">
	<tr>
		<td align="center"><BR>
			<BR>
			<BR>
			<span class="subHead">You must be a CSR to access this section.</span>
		</td>
	</tr>
</table>

<asp:panel id="pnlCSR" runat="server"><BR><BR>
<TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
  <TR>
    <TD>
<asp:Button id=btn_Add runat="server" Text="New PlayList Item"></asp:Button>
<asp:Label id=lbl_Messages runat="server">Label</asp:Label></TD></TR>
  <TR>
    <TD>
<asp:DataGrid id=dgrdPlayList style="BORDER-RIGHT: #cccccc 1px solid; BORDER-TOP: #cccccc 1px solid; BORDER-LEFT: #cccccc 1px solid; BORDER-BOTTOM: #cccccc 1px solid; FONT-FAMILY: arial" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False" border="0" cellpadding="2" PageSize="20" AllowPaging="True">
					<AlternatingItemStyle CssClass="lineItemOdd"></AlternatingItemStyle>
					<ItemStyle CssClass="lineItemEven"></ItemStyle>
					<HeaderStyle Font-Bold="True" HorizontalAlign="Left" ForeColor="Black" CssClass="lineItemColHead"></HeaderStyle>
					<Columns>
						<asp:BoundColumn Visible="False" DataField="featured_asset_id" ReadOnly="True" HeaderText="Feature Asset Id">
							<HeaderStyle Font-Bold="True" HorizontalAlign="Left"></HeaderStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="asset_id" SortExpression="asset_id" HeaderText="Asset Id">
							<HeaderStyle Font-Bold="True" HorizontalAlign="Left"></HeaderStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="user_id" SortExpression="user_id" HeaderText="User Id">
							<HeaderStyle Font-Bold="True" HorizontalAlign="Left"></HeaderStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="start_datetime" SortExpression="start_datetime" HeaderText="Start Time">
							<HeaderStyle Font-Bold="True" HorizontalAlign="Left"></HeaderStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="end_datetime" SortExpression="end_datetime" HeaderText="End Time">
							<HeaderStyle Font-Bold="True" HorizontalAlign="Left"></HeaderStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="shown_count" SortExpression="shown_count" HeaderText="Shown Count">
							<HeaderStyle Font-Bold="True" HorizontalAlign="Left"></HeaderStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="status_id" SortExpression="status_id" HeaderText="Status Id">
							<HeaderStyle Font-Bold="True" HorizontalAlign="Left"></HeaderStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="playlist_type" SortExpression="playlist_type" HeaderText="PlayList Type">
							<HeaderStyle Font-Bold="True" HorizontalAlign="Left"></HeaderStyle>
						</asp:BoundColumn>
						<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="Update" CancelText="Cancel" EditText="Edit">
							<HeaderStyle Font-Bold="True" HorizontalAlign="Left"></HeaderStyle>
						</asp:EditCommandColumn>
						<asp:ButtonColumn Text="Delete" CommandName="Delete">
							<HeaderStyle Font-Bold="True" HorizontalAlign="Left"></HeaderStyle>
						</asp:ButtonColumn>
					</Columns>
					<PagerStyle Position="TopAndBottom" Mode="NumericPages"></PagerStyle>
				</asp:DataGrid></TD></TR>
  <TR>
    <TD align=right>
<asp:Button id=btn_Save runat="server" Text="Save Changes"></asp:Button></TD></TR></TABLE>
</asp:panel>
		  <br /> 
		</td>
	</tr>
</table>