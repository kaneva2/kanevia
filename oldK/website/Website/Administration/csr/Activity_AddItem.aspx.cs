///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Drawing;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.csr
{
    public partial class Activity_AddItem : NoBorderPage
    {
        protected System.Web.UI.WebControls.Panel pnlCSR;
        protected System.Web.UI.HtmlControls.HtmlTable tblNoCSR;
        protected System.Web.UI.WebControls.DropDownList drpItemName, drpPackageList;
        protected System.Web.UI.WebControls.TextBox txtQuantity, txtNote, txtNotePackage;
        protected System.Web.UI.WebControls.CheckBox chkGift;
        protected System.Web.UI.WebControls.CheckBox chkBank;
        protected System.Web.UI.WebControls.Label lbl_ItemName;
        protected System.Web.UI.WebControls.Label lbl_Quantity;
        protected System.Web.UI.WebControls.Label lbl_Gift;
        protected System.Web.UI.WebControls.Label lbl_Personal;
        protected System.Web.UI.WebControls.Label lbl_Messages, lbl_MessagesPackage;
        protected System.Web.UI.WebControls.Button btnSave, btnItemFilter;
        protected System.Web.UI.WebControls.TextBox txtItemFilter;
        protected System.Web.UI.WebControls.LinkButton lbtnLoadUgc;

        protected void Page_Load(object sender, EventArgs e)
        {
            pnlCSR.Visible = (IsCSR());
            tblNoCSR.Visible = (!IsCSR());

            if (!IsPostBack)
            {
                userId = Convert.ToInt32(Request["userId"]);
                SiteMgmtFacade siteMgmtFacade = new SiteMgmtFacade();
                siteMgmtFacade.InsertCSRLog(GetUserId(), "CSR - Add Item", 0, userId);

                chkGift.Checked = false;
                chkBank.Checked = false;
                txtQuantity.Text = "1";

                drpItemName.DataValueField = "global_id";
                drpItemName.DataTextField = "glidName";
                drpItemName.DataSource = UsersUtility.GetWOKItems();
                drpItemName.DataBind();


                drpPackageList.DataValueField = "promotion_id";
                drpPackageList.DataTextField = "bundle_title";
                drpPackageList.DataSource = StoreUtility.GetPromotionalOffers();
                drpPackageList.DataBind();

            }

        }

        private int userId
        {
            get
            {
                if (Session["_userId"] == null)
                {
                    Session["_userId"] = -1;
                }
                return (int)Session["_userId"];
            }
            set
            {
                Session["_userId"] = value;
            }
        }


        protected void btnSave_Click (object sender, EventArgs e)
        {
            int returnValue = 0;
            returnValue = UsersUtility.AddWOKItem(userId, Convert.ToInt32(drpItemName.SelectedValue), Convert.ToInt32(txtQuantity.Text), chkGift.Checked, chkBank.Checked);

            if (returnValue == 0)
            {
                lbl_Messages.Text = "Error Adding Item(s)";
                lbl_Messages.ForeColor = Color.Red;
            }
            else
            {
                // Add note
                string strUserNote = "Added: " + drpItemName.SelectedItem.Text + " <br/>" + txtNote.Text;

                UsersUtility.InsertUserNote(userId, GetUserId(), strUserNote);

                lbl_Messages.Text = "Success!";
                lbl_Messages.ForeColor = Color.Red;
            }
        }



        protected void btnSavePackage_Click(object sender, EventArgs e)
        {
            int returnValue = 0;

            returnValue = StoreUtility.AddWokItems(Convert.ToInt32(drpPackageList.SelectedValue), userId);
          
            if (returnValue == 0)
            {
                lbl_MessagesPackage.Text = "Error Adding Item(s)";
                lbl_MessagesPackage.ForeColor = Color.Red;
            }
            else
            {
                // Add note

                string strUserNote = "Added Package: " + drpPackageList.SelectedItem.Text + " <br/>" + txtNotePackage.Text;

                UsersUtility.InsertUserNote(userId, GetUserId(), strUserNote);
                
                lbl_MessagesPackage.Text = "Success!";
                lbl_MessagesPackage.ForeColor = Color.Red;
            }
                         
        }
        
        
        
        protected void btnItemFilter_Click(object sender, EventArgs e)
        {
            drpItemName.DataValueField = "global_id";
            drpItemName.DataTextField = "glidName";
            drpItemName.DataSource = UsersUtility.GetWOKItems(txtItemFilter.Text);
            drpItemName.DataBind();
        }

        protected void lbtnLoadUgc_Click(object sender, EventArgs e)
        {
            drpItemName.DataValueField = "global_id";
            drpItemName.DataTextField = "glidName";
            drpItemName.DataSource = UsersUtility.GetWOKItemsIncludingUgc();
            drpItemName.DataBind();
        }


    }
}
