///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.csr
{
    public partial class Activity_AccessPasses : NoBorderPage
    {
        protected System.Web.UI.WebControls.Panel pnlCSR;
		protected System.Web.UI.HtmlControls.HtmlTable tblNoCSR;
        protected System.Web.UI.WebControls.Button btn_Add;
        protected System.Web.UI.WebControls.DataGrid dgrdPasses;
        protected System.Web.UI.WebControls.Button btn_Save;
        protected System.Web.UI.WebControls.Label lbl_Messages;

        protected Activity_AccessPasses () 
		{
			Title = "CSR - Access Passes";
		}

        protected void Page_Load(object sender, EventArgs e)
        {
            // Make sure they are a CSR
            pnlCSR.Visible = (IsCSR());
            tblNoCSR.Visible = (!IsCSR());

            this.lbl_Messages.Text = "";

            if (!IsPostBack)
            {
                // Log the CSR activity
                int userId = Convert.ToInt32(Request["userId"]);
                SiteMgmtFacade siteMgmtFacade = new SiteMgmtFacade();
                siteMgmtFacade.InsertCSRLog(GetUserId(), "CSR - Access Passes", 0, userId);
                InitialBind();
            }
        }

        private void InitialBind()
        {
            currentPage = 0;
            dt = UsersUtility.GetAccessPasses(Convert.ToInt32(Request["userId"]));
            BindData();
        }

        private void BindData()
        {
            // Set the sortable columns
            string orderby = CurrentSort + " " + CurrentSortOrder;

            //set the current page
            this.dgrdPasses.CurrentPageIndex = currentPage;

            dt.DefaultView.Sort = orderby;

            dgrdPasses.DataSource = dt;
            dgrdPasses.DataBind();
        }

        private void SortSwitch(string newSort)
        {
            if (CurrentSort == newSort)
            {
                if (CurrentSortOrder == "DESC")
                {
                    CurrentSortOrder = "ASC";
                }
                else
                {
                    CurrentSortOrder = "DESC";
                }
            }
            else
            {
                CurrentSort = newSort;
                CurrentSortOrder = "DESC";
            }
        }

        /// <summary>
        /// DEFAULT_SORT
        /// </summary>
        /// <returns></returns>
        protected override string DEFAULT_SORT
        {
            get
            {
                return "created_date";
            }
        }

        /// <summary>
        /// DEFAULT_SORT
        /// </summary>
        /// <returns></returns>
        protected override string DEFAULT_SORT_ORDER
        {
            get
            {
                return "DESC";
            }
        }

        private DataTable dt
        {
            get
            {
                if (Session["_accessPasses"] == null)
                {
                    Session["_accessPasses"] = new DataTable();
                }
                return (DataTable)Session["_accessPasses"];
            }
            set
            {
                Session["_accessPasses"] = value;
            }
        }

        private int currentPage
        {
            get
            {
                if (Session["_currentPage"] == null)
                {
                    Session["_currentPage"] = 0;
                }
                return (int)Session["_currentPage"];
            }
            set
            {
                Session["_currentPage"] = value;
            }
        }

        protected void dgrdPasses_CancelCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            this.dgrdPasses.EditItemIndex = -1;
            BindData();
        }

        protected void dgrdPasses_EditCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            this.dgrdPasses.EditItemIndex = e.Item.ItemIndex;
            BindData();
        }

        protected void dgrdPasses_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
        {
            currentPage = e.NewPageIndex;
            BindData();
        }

        protected void dgrdPasses_SortCommand(object source, System.Web.UI.WebControls.DataGridSortCommandEventArgs e)
        {
            SortSwitch(e.SortExpression); //sets the sort expression
            this.dgrdPasses.EditItemIndex = -1; //closes any open edit boxes
            BindData();
        }

        public string GetAccessEditLink(int passGroupId)
        {
            return Page.ResolveUrl("~/administration/csr/AccessPassEdit.aspx?userId=" + Request["userId"] + "&passGroupId=" + passGroupId);
        }

        public string DeleteAccessLink(int passGroupId)
        {
            return Page.ResolveUrl("~/administration/csr/AccessPassEdit.aspx?userId=" + Request["userId"] + "&passGroupId=" + passGroupId + "&delete=1");
        }

        protected void btn_Add_Click(object sender, System.EventArgs e)
        {
            DataRow newRow = dt.NewRow();
            //set default values
            newRow[0] = -1;
            newRow[1] = "Click edit to modify this pass";
            newRow[2] = DateTime.Now.ToString();

            //dt.Rows.InsertAt(newRow,(dt.Rows.Count - 1));
            dt.Rows.Add(newRow);

            currentPage = 0;
            this.dgrdPasses.EditItemIndex = 0;
            BindData();
        }
    }
}
