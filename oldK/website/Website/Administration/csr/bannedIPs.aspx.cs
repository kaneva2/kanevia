///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Kaneva.csr
{
    public partial class bannedIPs : NoBorderPage
    {
        protected void Page_Load (object sender, EventArgs e)
        {
            // Redirect if user is not admin
            if (!IsCSR ())
            {
                Response.Redirect ("~/default.aspx");
            }

            this.lblErrMessage.Text = "";

            if (!IsPostBack)
            {
                // bind the member fame data
                BindData (1);
            }
        }

        private void BindData (int pageNumber)
        {
            try
            {
                // Set current page
                pgBannedIPs.CurrentPageNumber = pageNumber;

                // Set the sortable columns
                string orderby = CurrentSort + " " + CurrentSortOrder;
                int pageSize = 20;

                // Get the Banned IPs
                PagedDataTable pdtBannedIPs = UsersUtility.GetBannedIPs (DateTime.MinValue, DateTime.MaxValue, "", orderby, 1, 50);


                if (pdtBannedIPs.Rows.Count > 0)
                {
                    lblNoBannedIPs.Visible = false;

                    //dgMemberFame.PageCount = userFame.TotalCount / pageSize;
                    rptBannedIPs.DataSource = pdtBannedIPs;
                    rptBannedIPs.DataBind ();

                    // Show Pager
                    pgBannedIPs.NumberOfPages = Math.Ceiling ((double) pdtBannedIPs.TotalCount / pageSize).ToString ();
                    pgBannedIPs.DrawControl ();
                }
                else
                {
                    lblNoBannedIPs.Visible = true;
                }

                lblErrMessage.Visible = false;
            }
            catch (Exception ex)
            {
                lblErrMessage.Text = "Error: " + ex.Message;
                lblErrMessage.Visible = true;
            }
        }

        //allows user to switch the ordering between DESC and ASC
        //based on column clicked
        private void SortSwitch (string newSort)
        {
            if (CurrentSort == newSort)
            {
                if (CurrentSortOrder == "DESC")
                {
                    CurrentSortOrder = "ASC";
                }
                else
                {
                    CurrentSortOrder = "DESC";
                }
            }
            else
            {
                CurrentSort = newSort;
                CurrentSortOrder = "DESC";
            }
        }

        protected void rptBannedIPs_ItemCreated (object sender, RepeaterItemEventArgs e)
        {
            //TextBox tbxSortOrder = (TextBox) e.Item.FindControl ("tbxSortOrder");
            //tbxSortOrder.AutoPostBack = true;
            //tbxSortOrder.TextChanged += new EventHandler (tbxSortOrder_TextChanged);
            //HtmlAnchor thumbnail = (HtmlAnchor) e.Item.FindControl ("thumbnail");
            //HtmlAnchor mediaTitle = (HtmlAnchor) e.Item.FindControl ("mediaTitle");

            ////create the tool summary data
            //string title = "\\<b>Name: </b>" + Convert.ToString (DataBinder.Eval (e.Item.DataItem, "name")) + "<br>";
            //string description = "\\<b>Description: </b>" + Convert.ToString (DataBinder.Eval (e.Item.DataItem, "teaser")) + "<br>";
            //string owner = "\\<b>Owner: </b>" + Convert.ToString (DataBinder.Eval (e.Item.DataItem, "username")) + "<br>";
            //string restricted = "\\<b>Rating: </b>" + (Convert.ToBoolean (DataBinder.Eval (e.Item.DataItem, "mature_profile")) ? "Restricted" : "General Audience") + "<br>";
            //string assetType = "\\<b>Type: </b>" + GetAssetTypeName (Convert.ToInt32 (DataBinder.Eval (e.Item.DataItem, "asset_type_id"))) + "<br>";
            //string _public = "\\<b>Access: </b>" + (Convert.ToString (DataBinder.Eval (e.Item.DataItem, "public")) == "Y" ? "Public" : "Private") + "<br>";
            //string fileSize = "\\<b>Size: </b>" + Math.Round ((Convert.ToInt32 (DataBinder.Eval (e.Item.DataItem, "file_size"))) / 1000.0, 2) + " Mb" + "<br>";
            //string createdOn = "\\<b>Added On: </b>" + FormatDateTime (Convert.ToDateTime (DataBinder.Eval (e.Item.DataItem, "date_added"))) + "<br>";

            //string toolTip = "\\ <h4>Asset Details</h4>" + title + description + owner + restricted + assetType + _public + fileSize + createdOn;
            //thumbnail.Attributes.Add ("onmouseover", "whiteBalloonSans.showTooltip(event,'" + KanevaGlobals.CleanJavascriptFull (toolTip) + "')");
            //mediaTitle.Attributes.Add ("onmouseover", "whiteBalloonSans.showTooltip(event,'" + KanevaGlobals.CleanJavascriptFull (toolTip) + "')");
        }
        
        protected void rptBannedIPs_ItemDataBound (object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

            }
        }

        /// <summary>
        /// rptBannedIPs_ItemCommand
        /// </summary>
        protected void rptBannedIPs_ItemCommand (object source, RepeaterCommandEventArgs e)
        {
            string command = e.CommandName;

            HtmlInputHidden hidIPAddress;

            switch (command)
            {
                case "cmdUnBan":
                    {
                        int index = e.Item.ItemIndex;
                        hidIPAddress = (HtmlInputHidden) rptBannedIPs.Items[index].FindControl ("hidIPAddress");

                        UsersUtility.RemoveIPBan (hidIPAddress.Value);
                        
                        BindData (1);
                        break;
                    }
            }
        }



        //protected void dgBannedIPs_OnRowCreated (object sender, GridViewRowEventArgs e)
        //{
        //    if (e.Row.RowType == DataControlRowType.DataRow)
        //    {
        //        LinkButton lbUnBanIP = (LinkButton) e.Row.FindControl ("lbUnBanIP");
        //        lbUnBanIP.CommandArgument = DataBinder.Eval (e.Row.DataItem, "ip_address").ToString ();
        //        lbUnBanIP.CommandName = "UnBan";
        //        lbUnBanIP.ToolTip = "Un-Ban IP " + DataBinder.Eval (e.Row.DataItem, "ip_address").ToString ();
        //    }
        //}

        /// <summary>
        /// Execute when the user selects a page change link from the Pager control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void pgBannedIPs_PageChange (object sender, PageChangeEventArgs e)
        {
            BindData (e.PageNumber);
        }


        #region Properties

        /// <summary>
        /// Banned IP Search Filter
        /// </summary>
        /// <returns></returns>
        private string BannedIPSearchFilter
        {
            get
            {
                if (ViewState["_BannedIPSearchFilter"] == null)
                {
                    ViewState["_BannedIPSearchFilter"] = "";
                }
                return ViewState["_BannedIPSearchFilter"].ToString ();
            }
            set
            {
                ViewState["_BannedIPSearchFilter"] = value;
            }
        }

        /// <summary>
        /// DEFAULT_SORT
        /// </summary>
        /// <returns></returns>
        protected override string DEFAULT_SORT
        {
            get
            {
                return "ip_address";
            }
        }
        
        /// <summary>
        /// DEFAULT_SORT_ORDER
        /// </summary>
        /// <returns></returns>
        protected override string DEFAULT_SORT_ORDER
        {
            get
            {
                return "ASC";
            }
        }

        #endregion



    }
}
