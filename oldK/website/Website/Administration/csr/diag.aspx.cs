///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.csr
{
    public partial class diag : NoBorderPage
    {
        protected diag () 
		{
			Title = "CSR - Diagnostics";
		}

        protected void Page_Load (object sender, EventArgs e)
        {
            // Make sure they are a CSR
            divCSR.Visible = (IsCSR ());
            tblNoCSR.Visible = (!IsCSR ());

            if (!IsPostBack)
            {
                // Log the CSR activity
                this.UserId = Convert.ToInt32 (Request["userId"]);
                SiteMgmtFacade siteMgmtFacade = new SiteMgmtFacade();
                siteMgmtFacade.InsertCSRLog(GetUserId(), "CSR - Diag", 0, this.UserId);

                this.CurrentPage = 0;
                BindData (this.UserId, 1);
            }
        }

        private void BindData (int userId, int pageNumber)
        {
            // Set the sortable columns
            string orderby = this.CurrentSort + " " + this.CurrentSortOrder;
            int pageSize = 25;

            //set the current page
            this.dgrdDXDiag.CurrentPageIndex = this.CurrentPage;

            PagedDataTable pdtDXDiag = UsersUtility.GetUserDXDiag (this.UserId, orderby, pageNumber, pageSize);

            dgrdDXDiag.DataSource = pdtDXDiag;
            dgrdDXDiag.DataBind ();

            pgDiag.NumberOfPages = Math.Ceiling ((double) pdtDXDiag.TotalCount / pageSize).ToString ();
            pgDiag.DrawControl ();

            lblResultCount.Text = GetResultsText (pdtDXDiag.TotalCount, pageNumber, pageSize, pdtDXDiag.Rows.Count);
        }

        protected void pgDiag_PageChange (object sender, PageChangeEventArgs e)
        {
            BindData (this.UserId, e.PageNumber);
        }

        #region Properties

        /// <summary>
        /// DEFAULT_SORT
        /// </summary>
        /// <returns></returns>
        protected override string DEFAULT_SORT
        {
            get
            {
                return "diag_date";
            }
        }

        /// <summary>
        /// DEFAULT_SORT
        /// </summary>
        /// <returns></returns>
        protected override string DEFAULT_SORT_ORDER
        {
            get
            {
                return "DESC";
            }
        }

        private int CurrentPage
        {
            get
            {
                if (ViewState["_currentPage"] == null)
                {
                    ViewState["_currentPage"] = 0;
                }
                return (int) ViewState["_currentPage"];
            }
            set
            {
                ViewState["_currentPage"] = value;
            }
        }

        private int UserId
        {
            get
            {
                if (ViewState["_userId"] == null)
                {
                    ViewState["_userId"] = Request["userId"].ToString ();
                }
                return (int) ViewState["_userId"];
            }
            set
            {
                ViewState["_userId"] = value;
            }
        }

        
#endregion

    }
}
