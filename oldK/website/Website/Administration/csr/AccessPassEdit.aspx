<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AccessPassEdit.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.csr.AccessPassEdit" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../../usercontrols/Pager.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="CSRNav" Src="csrNav.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="activityNav" Src="activityNav.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="AdminMenu" Src="../../usercontrols/AdministrativeMenu.ascx" %>

<link href="../../css/csr.css" rel="stylesheet" type="text/css" />

<center>
<Kaneva:AdminMenu ID="adminmenu" runat="server" />
<Kaneva:CSRNav runat="server" id="csrNavigation" SubTab="us"/>
<br><br>
<Kaneva:activityNav runat="server" id="activityNavigation" SubTab="accesspasses"/>
</center>

<table runat="server" id="tblNoCSR" cellpadding="0" cellspacing="0" border="0" width="560">
	<tr><td ALIGN="CENTER">
		<BR><BR><BR>
		<span class="subHead">You must be a CSR to access this section.</span>
	</td></tr>
</table>

<center>
<asp:panel runat="server" ID="pnlCSR">
<BR />
<BR />
<TABLE cellSpacing=5 cellPadding=0 width="30%" border=0>
<TBODY>
    <tr>
    <td valign="middle" align="right" width="100"><asp:Label id=lbl_PassType runat="server"><strong>Pass Type: </strong></asp:Label></td>
	<td align="left"><asp:DropDownList class="formKanevaText" id="drpPassType" style="width:130px" runat="server"/></td>
    </tr>
    <tr>
    <td valign="middle" align="right" width="100"><asp:Label id=lbl_CreatedDate runat="server"><strong>Created Date: </strong></asp:Label></td>
	<td align="left"><asp:TextBox id="txtCreatedDate" class="formKanevaText" style="width:150px" MaxLength="40" runat="server"/></td>
    </tr>
    <tr>
    <td valign="middle" align="right" width="100"><asp:Label id=lbl_GiveItems runat="server"><strong>Give Items: </strong></asp:Label></td>
	<td align="left"><asp:CheckBox id="chkGiveItems" runat="server"/></td>
	</tr>
	<tr>
	<td valign="middle" align="right" width="100"><asp:Label id=lbl_Messages runat="server">Label</asp:Label></td>
    <td align="right"><asp:button id="btnSave" runat="Server" onClick="btnSave_Click" CausesValidation="False" Text="Save"/></td>
	</tr>
</TBODY>
</TABLE>
</asp:panel>
</center>

