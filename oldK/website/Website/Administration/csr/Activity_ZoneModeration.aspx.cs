///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.csr
{
    public partial class Activity_ZoneModeration : NoBorderPage
    {
        protected System.Web.UI.WebControls.Panel pnlCSR;
		protected System.Web.UI.HtmlControls.HtmlTable tblNoCSR;
        protected System.Web.UI.WebControls.Button btn_Add;
        protected System.Web.UI.WebControls.DataGrid dgrdRoles;
        protected System.Web.UI.WebControls.Button btn_Save;
        protected System.Web.UI.WebControls.Label lbl_Messages;
        protected System.Web.UI.WebControls.DropDownList drpZones, drpRoles;

        protected Activity_ZoneModeration() 
		{
			Title = "CSR - Zone Moderation";
		}

        protected void Page_Load(object sender, EventArgs e)
        {
            // Make sure they are a CSR
            pnlCSR.Visible = (IsCSR());
            tblNoCSR.Visible = (!IsCSR());

            if (!IsPostBack)
            {
                // Log the CSR activity
                int userId = Convert.ToInt32(Request["userId"]);
                SiteMgmtFacade siteMgmtFacade = new SiteMgmtFacade();
                siteMgmtFacade.InsertCSRLog(GetUserId(), "CSR - Zone Moderation", 0, userId);
                InitialBind();               
            }
        }

        private void InitialBind()
        {
            BindZones();
            currentPage = 0;                      
            dt = UsersUtility.GetPermanentZoneRoles(Convert.ToInt32(Request["userId"]));          
            BindData();
        }

        private void BindData()
        {
            // Set the sortable columns
            string orderby = CurrentSort + " " + CurrentSortOrder;

            //set the current page
            this.dgrdRoles.CurrentPageIndex = currentPage;

            dt.DefaultView.Sort = orderby;

            dgrdRoles.DataSource = dt;
            dgrdRoles.DataBind();
        }

        /// <summary>
        /// BindZones
        /// </summary>
        private void BindZones()
        {
            DataTable dt = UsersUtility.GetPermanentZonesNotModeratedByUser(Convert.ToInt32(Request["userId"]));
            drpZones.DataSource = dt;
            drpZones.DataBind();
            
            // If there are no more zones that can  be added disable the button.
            if (dt.Rows.Count < 1)
            {
                btn_Add.Enabled = false;
                btn_Add.Text = "No Zones To Add";
            }
            else
            {
                btn_Add.Enabled = true;
                btn_Add.Text = "New Role";
            }
        }

        private void SortSwitch(string newSort)
        {
            if (CurrentSort == newSort)
            {
                if (CurrentSortOrder == "DESC")
                {
                    CurrentSortOrder = "ASC";
                }
                else
                {
                    CurrentSortOrder = "DESC";
                }
            }
            else
            {
                CurrentSort = newSort;
                CurrentSortOrder = "DESC";
            }
        }

        /// <summary>
        /// DEFAULT_SORT
        /// </summary>
        /// <returns></returns>
        protected override string DEFAULT_SORT
        {
            get
            {
                return "name";
            }
        }

        /// <summary>
        /// DEFAULT_SORT
        /// </summary>
        /// <returns></returns>
        protected override string DEFAULT_SORT_ORDER
        {
            get
            {
                return "DESC";
            }
        }

        private DataTable dt
        {
            get
            {
                if (Session["_permZoneRoles"] == null)
                {
                    Session["_permZoneRoles"] = new DataTable();
                }
                return (DataTable)Session["_permZoneRoles"];
            }
            set
            {
                Session["_permZoneRoles"] = value;
            }
        }

        private int currentPage
        {
            get
            {
                if (Session["_currentPage"] == null)
                {
                    Session["_currentPage"] = 0;
                }
                return (int)Session["_currentPage"];
            }
            set
            {
                Session["_currentPage"] = value;
            }
        }

        protected void dgrdRoles_CancelCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            this.dgrdRoles.EditItemIndex = -1;
            BindData();
        }

        protected void dgrdRoles_EditCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            this.dgrdRoles.EditItemIndex = e.Item.ItemIndex;
            BindData();
        }

        protected void dgrdRoles_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
        {
            currentPage = e.NewPageIndex;
            BindData();
        }

        protected void dgrdRoles_SortCommand(object source, System.Web.UI.WebControls.DataGridSortCommandEventArgs e)
        {
            SortSwitch(e.SortExpression); //sets the sort expression
            this.dgrdRoles.EditItemIndex = -1; //closes any open edit boxes
            BindData();
        }

        protected void dgrdRoles_DeleteCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            int userId = Convert.ToInt32(Request["userId"]);

            try
            {
                UsersUtility.DeletePermanentRole(userId, Convert.ToInt32(e.Item.Cells[0].Text));
                lbl_Messages.Text = "Role Deleted.";

                string strUserNote = "Deleted Permanent Zone Moderation Role for Zone: " + drpZones.SelectedItem.Text;
                UsersUtility.InsertUserNote(userId, GetUserId(), strUserNote);
            }
            catch (Exception exc)
            {
                lbl_Messages.Text = "Error Deleting Role: " + exc;
            }
            InitialBind();
        }

        protected void btn_Add_Click(object sender, System.EventArgs e)
        {
            int userId = Convert.ToInt32(Request["userId"]);

            try
            {
                UsersUtility.AddPermanentZoneRole(userId, Convert.ToInt32(drpZones.SelectedValue), Convert.ToInt32(drpRoles.SelectedValue));
                lbl_Messages.Text = "Role Added.";

                string strUserNote = "Added Permanent Zone Moderation Role for Zone: " + drpZones.SelectedItem.Text ;
                UsersUtility.InsertUserNote(userId, GetUserId(), strUserNote);
            }
            catch (Exception exc)
            {
                lbl_Messages.Text = "Error Adding Role" + exc.ToString();
                //m_logger.Error("Error adding role ", exc);              
            }
            currentPage = 0;
            this.dgrdRoles.EditItemIndex = 0;
            InitialBind();
        }
    }
}
