<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RestrictedWordsManagement.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.csr.RestrictedWordsManagement" %>
<%@ Register TagPrefix="Kaneva" TagName="CSRNav" Src="csrNav.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="AdminMenu" Src="../../usercontrols/AdministrativeMenu.ascx" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../../usercontrols/Pager.ascx" %>

<script type="text/javascript" language="javascript" src="../../jscript/prototype.js"></script>

<link href="../../css/new.css" rel="stylesheet" type="text/css" />

<body>

<Kaneva:AdminMenu ID="adminmenu" runat="server" />
<Kaneva:CSRNav runat="server" id="csrNavigation" SubTab="rw"/>
<br/>

 <table id="tblRestrictions" border="0" cellpadding="0" cellspacing="0" width="1000" style="background-color:#ffffff">
    <tr>
        <td align="center">
			<span style="height:30px; font-size:28px; font-weight:bold">Restricted Words Management</span><br />
			<ajax:ajaxpanel id="ajpError" runat="server">
			<asp:label runat="server" id="lblErrMessage" class="errBox black" style="width:60%;margin-top:20px;text-align:center;"></asp:label></ajax:ajaxpanel>
		</td>
    </tr>
	<tr>
		<td style="height:20px"></td>
    </tr>
    <tr>
		<td align="center">
			<ajax:ajaxpanel id="ajRestritionsList" runat="server">

				<table id="tblRestrictedList" border="0" cellpadding="0" cellspacing="0" width="95%">
					<tr>
						 <td>
				            <div style="width:900px;text-align:left;margin:0 0 12px 0;">
								<asp:LinkButton id="lbNewWord" runat="server" causesvalidation="false" onclick="lbNewWord_Click">Add New Word</asp:LinkButton>
							</div>
							
							<div id="divRestrictionDetails" runat="server" style="display:none;width:100%;height:50px;" >
								<div style="float:left;display:block;">Word:<br /><asp:textbox id="txtWord" width="200" runat="server"></asp:textbox></div>
								<div style="float:left;display:block;margin-left:20px;">Restriction Type:<br /><asp:dropdownlist id="ddlRestrictionType" runat="server"></asp:dropdownlist></div>
								<div style="float:left;display:block;margin-left:20px;">Match Type:<br /><asp:dropdownlist id="ddlMatchType" runat="server">
									<asp:listitem text="exact" value="exact"></asp:listitem>
									<asp:listitem text="anywhere" value="anywhere"></asp:listitem>
								</asp:dropdownlist></div>
								<div style="float:left;display:inline;margin-left:20px;text-align:left;">Replace With:<br /><asp:textbox id="txtReplace" width="200" runat="server"></asp:textbox></div>
								<div style="float:left;display:block;margin-left:20px;"><br /><asp:button id="btnSave" onclick="btnSave_Click" runat="server" text="Save" style="width:60px;" /><input type="button" value="Cancel" onclick="$('divRestrictionDetails').style.display='none';" /></div>
								<asp:textbox id="txtRestrictedWordId" runat="server" visible="false"></asp:textbox>
							</div>	
							
							<asp:gridview id="dgWordList" runat="server" 
								onsorting="dgWordList_Sorting" 
								OnRowDeleting="dgWordList_RowDeleting" 
								OnRowEditing="dgWordList_RowEditing"
								OnRowCreated="dgWordList_RowCreated"
								AllowSorting="True" border="0" cellpadding="4" 
								autogeneratecolumns="False" width="980px" 
								bordercolor="DarkGray" borderstyle="solid" borderwidth="1px" 
								PageSize="50" AllowPaging="False" >
								
								<RowStyle BackColor="White"></RowStyle>
								<HeaderStyle BackColor="LightGray" Font-Underline="True" Font-Bold="True" ForeColor="#000000" HorizontalAlign="Left"></HeaderStyle>
								<AlternatingRowStyle BackColor="Gainsboro"></AlternatingRowStyle>
								<FooterStyle BackColor="#FFffff"></FooterStyle>
								<Columns>
									<asp:BoundField HeaderText="Id" DataField="restricted_words_id" ReadOnly="True" ConvertEmptyStringToNull="False" visible="True" itemstyle-width="30px" ></asp:BoundField>
									<asp:BoundField HeaderText="Word" DataField="word" SortExpression="word" ReadOnly="True" ConvertEmptyStringToNull="False" itemstyle-width="250px" ></asp:BoundField>
									<asp:BoundField HeaderText="Restriction Type" DataField="name" SortExpression="name" ReadOnly="True" ConvertEmptyStringToNull="False" itemstyle-width="200px"></asp:BoundField>
									<asp:BoundField HeaderText="id" DataField="restriction_type_id" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
									<asp:BoundField HeaderText="Match Type" DataField="match_type" SortExpression="match_type" ReadOnly="True" ConvertEmptyStringToNull="False" itemstyle-width="120px"></asp:BoundField>
									<asp:BoundField HeaderText="Replace With" DataField="replace_with" ReadOnly="True" ConvertEmptyStringToNull="True" itemstyle-width="220px"></asp:BoundField>
									<asp:CommandField CausesValidation="False" ShowEditButton="True" itemstyle-width="100px"></asp:CommandField>
									<asp:TemplateField>
										<ItemTemplate>
											<asp:LinkButton id="lbnConfirmDelete" Runat="server" causesvalidation="false" OnClientClick="return confirm('Are you sure you want to delete this word?');" CommandName="Delete">Delete</asp:LinkButton>
										</ItemTemplate>
									</asp:TemplateField>
				        
								</Columns>
							</asp:gridview>
							
							<div style="width:95%;text-align:right;margin:10px 6px 0 0;"><kaneva:pager runat="server" isajaxmode="True" onpagechanged="pgWordList_PageChange" id="pgWordList" maxpagestodisplay="5" shownextprevlabels="true" /></div>
							         
							<asp:Label id="lblNoRestrictions" visible="false" runat="server">
								<span style="font-size:16px;color:Red;font-weight:bold;margin:30px 0;width:95%;text-align:center;">No Restricted Words Were Found.</span>
							</asp:Label>
				
							<br /><br />
							
						</td>
					</tr>
					<tr>
						 <td align="center">
						 </td>
					</tr>
				</table>

			</ajax:ajaxpanel>
			<br />
		</td>
	</tr>
</table>


<br /><br />




</body>
