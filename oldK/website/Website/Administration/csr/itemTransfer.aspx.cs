///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva;
using log4net;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.CSR
{
	/// <summary>
	/// Summary description for itemTransfer.
	/// </summary>
	public class itemTransfer : NoBorderPage
	{
		protected itemTransfer () 
		{
			Title = "CSR - Item Transfer";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			pnlCSR.Visible = (IsCSR ());
			tblNoCSR.Visible = (!IsCSR ());

			int assetId = Convert.ToInt32 (Request ["assetId"]);
			DataRow drAsset = StoreUtility.GetAsset (assetId);
			lblAssetName.Text = Server.HtmlDecode (drAsset ["name"].ToString ());
		}

		/// <summary>
		/// Update Click
		/// </summary>
		protected void btnUpdate_Click (object sender, EventArgs e) 
		{
			int assetId = Convert.ToInt32 (Request ["assetId"]);
			DataRow drAsset = StoreUtility.GetAsset (assetId);

			// Look up the user id via the username
			DataRow drUser = UsersUtility.GetUser (txtUsername.Text.Trim ());

			if (drUser == null)
			{
				ShowErrorOnStartup ("Invalid username, please select a valid username.");
				return;
			}

			if (IsCSR ())
			{
                SiteMgmtFacade siteMgmtFacade = new SiteMgmtFacade();
                siteMgmtFacade.InsertCSRLog(GetUserId(), "Transfer item " + Server.HtmlDecode(drAsset["name"].ToString()) + " from user " + drAsset["username"] + " to user " + drUser["username"].ToString(), assetId, 0);
				StoreUtility.TransferItem (assetId, Convert.ToInt32 (drUser ["user_id"]), drUser ["username"].ToString (), GetUserId ()); 
			}

            Response.Redirect("~/Administration/csr/items.aspx");
		}

		/// <summary>
		/// Cancel Click
		/// </summary>
		protected void btnCancel_Click (object sender, EventArgs e) 
		{
            Response.Redirect("~/Administration/csr/items.aspx");
		}

		/// <summary>
		/// Get the user picker link.
		/// </summary>
		/// <returns></returns>
		protected string GetPickerLink ()
		{
			return "javascript: window.open('" + ResolveUrl ("~/userPicker.aspx?") + 
				"','add','toolbar=no,width=580,height=360,menubar=no,scrollbars=yes');";
		}

		protected TextBox txtUsername;
		protected Label lblAssetName;

		protected HtmlTable tblNoCSR;
		protected Panel pnlCSR;

		// Logger
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
