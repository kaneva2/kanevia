<%@ Page language="c#" Codebehind="Activity_Inventory.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.CSR.Activity_Inventory" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../../usercontrols/Pager.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="CSRNav" Src="csrNav.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="activityNav" Src="activityNav.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="AdminMenu" Src="../../usercontrols/AdministrativeMenu.ascx" %>

<link href="../../css/csr.css" rel="stylesheet" type="text/css" />

<center>
<Kaneva:AdminMenu ID="adminmenu" runat="server" />
<Kaneva:CSRNav runat="server" id="csrNavigation" SubTab="us"/>
<br><br>
<Kaneva:activityNav runat="server" id="activityNavigation" SubTab="inventory"/>
</center>
<br><br>

<table runat="server" id="tblNoCSR" cellpadding="0" cellspacing="0" border="0" width="560">
	<tr><td ALIGN="CENTER">
		<BR><BR><BR>
		<span class="subHead">You must be a CSR to access this section.</span>
	</td></tr>
</table>

<asp:panel runat="server" ID="pnlCSR">


<table cellpadding="0" cellspacing="0" border="0" width="90%">
		<tr>
			<td valign="middle" class="belowFilter" align="left" width="230">&nbsp;</td>
			<td width="100%" align="right" valign="bottom">
				<asp:Label runat="server" id="lblSearch" CssClass="showingCount"/>&nbsp;&nbsp;<Kaneva:Pager runat="server" id="pgTop"/>&nbsp;&nbsp;&nbsp;
			</td>
		</tr>
</table>

<asp:DataGrid runat="server" EnableViewState="False" ShowFooter="False" Width="90%" id="dgrdInventory" cellpadding="4" cellspacing="0" border="0" AutoGenerateColumns="False" AllowSorting="True" style="border: 1px solid #cccccc; font-family:arial">  
	<HeaderStyle CssClass="lineItemColHeadCSR"/>
	<ItemStyle CssClass="lineItemEvenCSR"/>
	<AlternatingItemStyle CssClass="lineItemOddCSR"/>
	<Columns>					
		
		<asp:TemplateColumn HeaderText="item name" SortExpression="name" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="top"  ItemStyle-Wrap="true">
			<ItemTemplate>
				<%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "name").ToString ())%> 
			</ItemTemplate>
		</asp:TemplateColumn>

        <asp:TemplateColumn HeaderText="Qty." SortExpression="quantity" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="top" ItemStyle-Wrap="true">
			<ItemTemplate>
				<%# DataBinder.Eval(Container.DataItem, "quantity").ToString()%>
			</ItemTemplate>
		</asp:TemplateColumn>

		<asp:TemplateColumn HeaderText="Credits / Rewards" SortExpression="inventory_sub_type" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="top" ItemStyle-Wrap="true">
			<ItemTemplate>
				<%# creditType(DataBinder.Eval(Container.DataItem, "inventory_sub_type").ToString())%>
			</ItemTemplate>
		</asp:TemplateColumn>
		
		<asp:TemplateColumn HeaderText="On Player / In Bank" SortExpression="inventory_type" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="top" ItemStyle-Wrap="true">
			<ItemTemplate>
				<%# inventoryLocation(DataBinder.Eval(Container.DataItem, "inventory_type").ToString())%>
			</ItemTemplate>
		</asp:TemplateColumn>
		
		<asp:TemplateColumn HeaderText="Pending" SortExpression="inv_location" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="top"  ItemStyle-Wrap="true">
			<ItemTemplate>
				<%# inventoryPending(DataBinder.Eval (Container.DataItem, "inv_location").ToString ())%>
			</ItemTemplate>
		</asp:TemplateColumn>		
		
	</Columns>
</asp:datagrid>

</asp:panel>