///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva;
using log4net;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.CSR
{
	/// <summary>
	/// Summary description for userNotes.
	/// </summary>
	public class userNotes : NoBorderPage
	{
		protected userNotes () 
		{
			Title = "CSR - User Notes";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Make sure they are a CSR
			pnlCSR.Visible = (IsCSR ());
			tblNoCSR.Visible = (!IsCSR ());

			if (!IsPostBack)
			{
				int userId = Convert.ToInt32 (Request ["userId"]);

				// Log the CSR activity
                SiteMgmtFacade siteMgmtFacade = new SiteMgmtFacade();
                siteMgmtFacade.InsertCSRLog(GetUserId(), "CSR - Viewing user notes", 0, userId);

				BindData (1, "");
			}
		}

		/// <summary>
		/// Pager change event
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pg_PageChange (object sender, PageChangeEventArgs e)
		{
			BindData (e.PageNumber, "");
		}

		/// <summary>
		///  Bind the data
		/// </summary>
		/// <param name="pageNumber"></param>
		private void BindData (int pageNumber, string filter)
		{
			// Set the sortable columns
			SetHeaderSortText (dgrdNotes);
			string orderby = CurrentSort + " " + CurrentSortOrder;

			int userId = Convert.ToInt32 (Request ["userId"]);
			int notesPerPage = 10;
	
			PagedDataTable pds = UsersUtility.GetUserNotes (userId, filter, orderby, pageNumber, notesPerPage);
			pgTop.NumberOfPages = Math.Ceiling ((double) pds.TotalCount / notesPerPage).ToString ();
			pgTop.DrawControl ();

			dgrdNotes.DataSource = pds;
			dgrdNotes.DataBind ();

			// The results
			lblSearch.Text = GetResultsText (pds.TotalCount, pageNumber, notesPerPage, pds.Rows.Count);
		}

		/// <summary>
		/// They clicked to sort a column
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected override void btnSort_Click (object sender, System.EventArgs e)
		{
			base.btnSort_Click (sender, e);
			BindData (pgTop.CurrentPageNumber, "");
		}

		/// <summary>
		/// Cancel Event Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnCancel_Click (object sender, EventArgs e) 
		{
			NavigateToReturn ();
		}

		/// <summary>
		/// Update Event Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnUpdate_Click (object sender, EventArgs e) 
		{
			// Server validation
			Page.Validate ();
			if (!Page.IsValid) 
			{
				BindData (1, "");
				return;
			}

			int userId = Convert.ToInt32 (Request ["userId"]);

			// Log the CSR activity
            SiteMgmtFacade siteMgmtFacade = new SiteMgmtFacade();
            siteMgmtFacade.InsertCSRLog(GetUserId(), "CSR - Adding user note.", 0, userId);

			UsersUtility.InsertUserNote (userId, GetUserId (), txtNote.Text);

			BindData (1, "");
		}

		/// <summary>
		/// NavigateToReturn
		/// </summary>
		private void NavigateToReturn ()
		{
            Response.Redirect("~/Administration/csr/userSearch.aspx");
		}

		/// <summary>
		/// DEFAULT_SORT
		/// </summary>
		/// <returns></returns>
		protected override string DEFAULT_SORT
		{
			get
			{
				return "created_datetime";
			}
		}

		/// <summary>
		/// DEFAULT_SORT
		/// </summary>
		/// <returns></returns>
		protected override string DEFAULT_SORT_ORDER
		{
			get
			{
				return "desc";
			}
		}

		protected HtmlTable tblNoCSR;
		protected Panel pnlCSR;

		protected Kaneva.Pager pgTop;
		protected DataGrid dgrdNotes;
		protected Label lblSearch;

		protected CuteEditor.Editor txtNote;

		// Logger
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
            pgTop.PageChanged += new PageChangeEventHandler(pg_PageChange);
		}
		#endregion
	}
}
