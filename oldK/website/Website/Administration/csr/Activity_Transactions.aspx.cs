///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva;
using log4net;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.CSR
{
	/// <summary>
    /// Summary description for Activity_Transactions.
	/// </summary>
	public class Activity_Transactions : NoBorderPage
	{
        protected Activity_Transactions() 
		{
			Title = "CSR - User Sales";
		}

		private void Page_Load (object sender, System.EventArgs e)
		{
			// Make sure they are a CSR
			pnlCSR.Visible = (IsCSR ());
			tblNoCSR.Visible = (!IsCSR ());

			if (!IsPostBack)
			{
				// Log the CSR activity
				int userId = Convert.ToInt32 (Request ["userId"]);
                SiteMgmtFacade siteMgmtFacade = new SiteMgmtFacade();
                siteMgmtFacade.InsertCSRLog(GetUserId(), "CSR - Viewing user inventory", 0, userId);

				BindData (1, "created_date");
			}
		}

		/// <summary>
		/// pg_PageChange
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pg_PageChange (object sender, PageChangeEventArgs e)
		{
			BindData (e.PageNumber, "");
		}

		/// <summary>
		/// BindData
		/// </summary>
		/// <param name="pageNumber"></param>
		/// <param name="filter"></param>
		private void BindData (int pageNumber, string orderby)
		{
			
            // Set the sortable columns
            SetHeaderSortText(dgrdInventory);
			string orderbyList = CurrentSort + " " + CurrentSortOrder;

			int iItemsPerPage = 30;
			int userId = Convert.ToInt32 (Request ["userId"]);


            PagedDataTable pds = UsersUtility.GetUserTransactions(userId, orderbyList, pageNumber, iItemsPerPage);
            pgTop.NumberOfPages = Math.Ceiling ((double) pds.TotalCount / iItemsPerPage).ToString ();
			pgTop.DrawControl ();

            dgrdInventory.DataSource = pds;
            dgrdInventory.DataBind();
				
			// The results
			lblSearch.Text = GetResultsText (pds.TotalCount, pageNumber, iItemsPerPage, pds.Rows.Count);
            
		}

		/// <summary>
		/// They clicked to sort a column
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected override void btnSort_Click (object sender, System.EventArgs e)
		{
			base.btnSort_Click (sender, e);
			BindData (pgTop.CurrentPageNumber, "");
		}

		/// <summary>
		/// DEFAULT_SORT
		/// </summary>
		/// <returns></returns>
		protected override string DEFAULT_SORT
		{
			get
			{
				return "created_date";
			}
		}

		/// <summary>
		/// DEFAULT_SORT
		/// </summary>
		/// <returns></returns>
		protected override string DEFAULT_SORT_ORDER
		{
			get
			{
				return "DESC";
			}
		}


        protected string inventoryPending(string location)
        {
            if (location == "Pnd Inv")
            {
                return "Pending";
            }
            else
            {
                return "";
            }
        }



        protected string inventoryLocation(string location)
        {
            if (location == "P")
            {
                return "Player";
            }
            else if (location == "B")
            {
                return "Bank";
            }
            else
            {
                return "";
            }
        }

        protected string creditType(string type)
        {
            if (type == "512")
            {
                return "Credits";
            }
            else if (type == "256")
            {
                return "Rewards";
            }
            else
            {
                return "";
            }
        }



		protected Kaneva.Pager pgTop;
		protected Label lblSearch;
        protected DataGrid dgrdInventory;

		protected HtmlTable tblNoCSR;
		protected Panel pnlCSR;


		#region Web Form Designer generated code

		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
			pgTop.PageChanged +=new PageChangeEventHandler (pg_PageChange);
		}
		#endregion
	}
}