///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.csr
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

	/// <summary>
	///		Summary description for activityNav.
	/// </summary>
	public class activityNav : System.Web.UI.UserControl
	{

		private void Page_Load(object sender, System.EventArgs e)
		{
            BlogFacade blogFacade = new BlogFacade();

			switch (SubTab)
			{
                case "details":
                    {
                        aDetails.InnerHtml = "<B>" + aDetails.InnerText + "</B>";
                        break;
                    }
                case "notes":
                    {
                        aNotes.InnerHtml = "<B>" + aNotes.InnerText + "</B>";
                        break;
                    }
                case "posts":
				{
					aPosts.InnerHtml = "<B>" + aPosts.InnerText + "</B>";
					break;
				}
				case "replies":
				{
					aReplies.InnerHtml = "<B>" + aReplies.InnerText + "</B>";
					break;
				}
				case "published":
				{
					aPublished.InnerHtml = "<B>" + aPublished.InnerText + "</B>";
					break;
				}
				case "channels":
				{
					aChannels.InnerHtml = "<B>" + aChannels.InnerText + "</B>";
					break;
				}
				case "blogs":
				{
					aBlogs.InnerHtml = "<B>" + aBlogs.InnerText + "</B>";
					break;
				}
				case "sales":
				{
					aSales.InnerHtml = "<B>" + aSales.InnerText + "</B>";
					break;
				}
                case "accesspasses":
                {
                    aAccessPasses.InnerHtml = "<B>" + aAccessPasses.InnerText + "</B>";
                    break;
                }

                case "addItems":
                {
                    aAddItems.InnerHtml = "<B>" + aAddItems.InnerText + "</B>";
                    break;
                }
                case "addCredits":
                {
                    aAddCredits.InnerHtml = "<B>" + aAddCredits.InnerText + "</B>";
                    break;
                }
                case "inventory":
                {
                    aInventory.InnerHtml = "<B>" + aInventory.InnerText + "</B>";
                    break;
                }
                case "transactions":
                {
                    aTransactions.InnerHtml = "<B>" + aTransactions.InnerText + "</B>";
                    break;
                }
                case "logins":
                {
                    aLoginHistory.InnerHtml = "<B>" + aLoginHistory.InnerText + "</B>";
                    break;
                }
                case "zonemoderation":
                {
                    aZoneModeration.InnerHtml = "<B>" + aZoneModeration.InnerText + "</B>";
                    break;
                }
                case "suspensions":
                {
                    aSuspensions.InnerHtml = "<B>" + aSuspensions.InnerText + "</B>";
                    break;
                }
                case "diagnostics":
                {
                    aDiag.InnerHtml = "<B>" + aDiag.InnerText + "</B>";
                    break;
                }            

            }

			int userId = Convert.ToInt32 (Request ["userId"]);

            UserFacade userFacade = new UserFacade();
            User user = userFacade.GetUser(userId);
			lblWho.Text = user.Username;

            aDetails.Attributes.Add("href", "../csr/userDetails.aspx?userId=" + userId);
            aNotes.Attributes.Add("href", "../csr/userNotes.aspx?userId=" + userId);
			aPosts.Attributes.Add ("href", "../csr/Activity_Posts.aspx?userId=" + userId);
            aReplies.Attributes.Add("href", "../csr/Activity_Replies.aspx?userId=" + userId);
            aBlogs.Attributes.Add("href", "../csr/Activity_Blogs.aspx?userId=" + userId);
            aSales.Attributes.Add("href", "../csr/Activity_Sales.aspx?userId=" + userId);
            aChannels.Attributes.Add("href", "../csr/Activity_Channels.aspx?userId=" + userId);
            aPublished.Attributes.Add("href", "../csr/Activity_Published.aspx?userId=" + userId);
            aAccessPasses.Attributes.Add("href", "../csr/Activity_AccessPasses.aspx?userId=" + userId);
            aAddItems.Attributes.Add("href", "../csr/Activity_AddItem.aspx?userId=" + userId);
            aInventory.Attributes.Add("href", "../csr/Activity_Inventory.aspx?userId=" + userId);
            aAddCredits.Attributes.Add("href", "../csr/Activity_AddCredits.aspx?userId=" + userId);
            aTransactions.Attributes.Add("href", "../csr/Activity_Transactions.aspx?userId=" + userId);
            aLoginHistory.Attributes.Add("href", "../csr/Activity_Logins.aspx?userId=" + userId);
            aZoneModeration.Attributes.Add("href", "../csr/Activity_ZoneModeration.aspx?userId=" + userId);
            aSuspensions.Attributes.Add("href", "../csr/Activity_Suspensions.aspx?userId=" + userId);
            aDiag.Attributes.Add ("href", "../csr/diag.aspx?userId=" + userId);

			lblItemsSold.Text = StoreUtility.GetSoldItems (userId, "", 1, 1).TotalCount.ToString ();
            lblBlogs.Text = blogFacade.GetAllUserBlogs(userId, "", "", 1, 1).TotalCount.ToString();
			lblPosts.Text = ForumUtility.GetUserForumTopics (userId, "", 1, 1).TotalCount.ToString ();
			lblReply.Text = ForumUtility.GetUserForumThreads (userId, "", 1, 1).TotalCount.ToString ();
            lblAccessPasses.Text = UsersUtility.GetAccessPasses(userId).Rows.Count.ToString();
			lblItems.Text = StoreUtility.GetAssetsInChannel (user.CommunityId, false , true, 0, "", "", 1, 1).TotalCount.ToString (); 
			lblChannels.Text = UsersUtility.GetUserCommunities (userId, "", "", 1, 1).TotalCount.ToString ();           
		}

		/// <summary>
		/// The SubTab to display
		/// </summary>
		public string SubTab
		{
			get 
			{
				return m_SubTab;
			} 
			set
			{
				m_SubTab = value;
			}
		}

		protected string m_SubTab = "";
        protected HtmlAnchor aNotes, aDetails, aSuspensions, aPosts, aReplies, aBlogs, aSales;
        protected HtmlAnchor aChannels, aPublished, aAccessPasses, aAddItems, aInventory, aAddCredits;
        protected HtmlAnchor aTransactions, aLoginHistory, aZoneModeration, aDiag;
        protected Label lblWho, lblItemsSold, lblBlogs, lblPosts, lblReply, lblItems, lblChannels, lblAccessPasses;

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
