<%@ Page language="c#" Codebehind="items.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.CSR.items" %>
<%@ Register TagPrefix="Kaneva" TagName="CSRNav" Src="csrNav.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="AdminMenu" Src="../../usercontrols/AdministrativeMenu.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../../usercontrols/Pager.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="StoreFilter" Src="../../usercontrols/StoreFilter.ascx" %>

<Kaneva:AdminMenu ID="adminmenu" runat="server" />
<asp:ValidationSummary ShowMessageBox="False" ShowSummary="True" Class="formError" id="valSum" DisplayMode="BulletList" runat="server" HeaderText="Please correct the following errors:"/>

<table runat="server" id="tblNoCSR" cellpadding="0" cellspacing="0" border="0" width="560">
	<tr><td ALIGN="CENTER">
		<BR><BR><BR>
		<span class="subHead">You must be a CSR to access this section.</span>
	</td></tr>
</table>

<asp:panel runat="server" ID="pnlCSR">

<Kaneva:CSRNav runat="server" id="csrNavigation" SubTab="it"/>
<br>

 <table id="tblUserFame" border="0" cellpadding="0" cellspacing="0" width="1000" style="background-color:#ffffff">
	<tr>
		<td style="height:20px"></td>
    </tr>
    <tr>
         <td align="center">

<center>
<table cellpadding="0" cellspacing="0" border="0" width="730">
	<tr><td valign="middle" class="belowFilter" align="left" width="230">&nbsp;</td><td width="500" align="right" valign="bottom"><asp:Label runat="server" id="lblSearch" CssClass="showingCount"/>&nbsp;&nbsp;<Kaneva:Pager runat="server" id="pgTop"/></td></tr>
</table>
</center>

<Kaneva:StoreFilter runat="server" id="filStore" AssetType="0"/>

<table cellpadding="0" cellspacing="0" border="0" width="1000">	
	<tr>
		<td width="17"><img runat="server" src="~/images/spacer.gif" width="17" height="5" border="0" ID="Img4"/></td>
		<td colspan="9" class="FullBorders">		
			
			<asp:DataGrid EnableViewState="False" runat="server" ShowFooter="False" Width="100%" id="listAsset" cellpadding="0" cellspacing="0" border="0" AutoGenerateColumns="False" AllowSorting="True" style="border: 0px hidden white;">  
				<HeaderStyle CssClass="lineItemColHead"/>
				<ItemStyle CssClass="lineItemEven"/>
				<AlternatingItemStyle CssClass="lineItemOdd"/>
				<Columns>
					<asp:TemplateColumn HeaderText=" " ItemStyle-Width="4%" ItemStyle-VerticalAlign="top">
						<ItemTemplate>							
							<asp:hyperlink id="hlEdit" runat="server" CssClass="adminLinks" NavigateURL='<%#GetAssetEditLink (Convert.ToInt32 ( DataBinder.Eval(Container.DataItem, "asset_id")))%>' ToolTip="Edit" Visible='<%#!IsDeleted (Convert.ToInt32 ( DataBinder.Eval(Container.DataItem, "status_id")))%>'>edit</asp:hyperlink> 
							&nbsp;<asp:hyperlink id="hlDelete" CssClass="adminLinks" runat="server" NavigateURL='<%#GetDeleteScript (Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_id"))) %>' ToolTip="Delete" Visible='<%#!IsDeleted (Convert.ToInt32 ( DataBinder.Eval(Container.DataItem, "status_id")))%>'>delete</asp:hyperlink>
							<br><asp:hyperlink id="hlDownload" CssClass="adminLinks" runat="server" NavigateURL='<%#GetDownloadLink (Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_id"))) %>' ToolTip="Delete" Visible='<%#!IsDeleted (Convert.ToInt32 ( DataBinder.Eval(Container.DataItem, "status_id")))%>'>download</asp:hyperlink>
							&nbsp;<asp:hyperlink id="hlTransfer" CssClass="adminLinks" runat="server" NavigateURL='<%#GetTransferLink (Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_id"))) %>' ToolTip="Transfer" Visible='<%#!IsDeleted (Convert.ToInt32 ( DataBinder.Eval(Container.DataItem, "status_id")))%>'>transfer</asp:hyperlink>
						</ItemTemplate>
					</asp:TemplateColumn>
			
					<asp:TemplateColumn HeaderText="item name" SortExpression="a.name" ItemStyle-Width="20%" ItemStyle-VerticalAlign="top">
						<ItemTemplate>
							<a title='<%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "name").ToString ())%>' href='<%# GetAssetDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "asset_id")))%>' style="COLOR: #3258ba; font-size: 12px;"><%# TruncateWithEllipsis (Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "name").ToString ()), 30) %></a>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="category" SortExpression="asset_type_id" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center">
						<ItemTemplate>
							<%# GetAssetTypeName (Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_type_id"))) %>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="owner" SortExpression="username" ItemStyle-Width="7%" ItemStyle-HorizontalAlign="Center">
						<ItemTemplate>
							<a class="dateStamp" href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "username").ToString ())%>><%# DataBinder.Eval(Container.DataItem, "username") %></a>
						</ItemTemplate>
					</asp:TemplateColumn>		
					<asp:TemplateColumn HeaderText="size" SortExpression="file_size" ItemStyle-Width="11%" ItemStyle-HorizontalAlign="Center">
						<ItemTemplate>
							<font color="green"><%# FormatImageSize (DataBinder.Eval (Container.DataItem, "file_size")) %></font>
						</ItemTemplate>
					</asp:TemplateColumn>		
					<asp:TemplateColumn HeaderText="published" SortExpression="created_date" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Center">
						<ItemTemplate>
							<span class="adminLinks"><%# DataBinder.Eval (Container.DataItem, "created_date") %></span>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="status" SortExpression="status_id" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="top" ItemStyle-Width="10%" ItemStyle-Wrap="false">
						<ItemTemplate>
							<A runat="server" style="color: #4863a2; font-size: 11px;" HREF='#' onclick='<%# GetStatusLink (Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "publish_status_id")))%>'><%# GetStatusText (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "publish_status_id")), Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "status_id")), Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "asset_id"))) %></a>
						</ItemTemplate>
					</asp:TemplateColumn>
				</Columns>
			</asp:datagrid>
			
			
		</td>
		<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="5" border="0" ID="Img1"/></td>
	</tr>


</table>
<br>

</asp:panel>

		</td>
	</tr>
</table>