///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

namespace KlausEnt.KEP.Kaneva.csr
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

	/// <summary>
	///		Summary description for csrNav.
	/// </summary>
	public class csrNav : System.Web.UI.UserControl
	{
		private void Page_Load(object sender, System.EventArgs e)
		{
            aHome.Style.Add ("font-weight", "normal");
            aUserSearch.Style.Add ("font-weight", "normal");
            aGames.Style.Add ("font-weight", "normal");
            aItems.Style.Add ("font-weight", "normal");
            aContests.Style.Add ("font-weight", "normal");
            aPromotions.Style.Add ("font-weight", "normal");
            aNameChange.Style.Add ("font-weight", "normal");
            aBannedIPs.Style.Add ("font-weight", "normal");
            aRestricted.Style.Add ("font-weight", "normal");
            aRewards.Style.Add ("font-weight", "normal");

            switch (SubTab.ToLower())
            {
                case "us":
                    {
                        aUserSearch.Style.Add ("font-weight", "bold");
                        break;
                    }
                case "ga":
                    {
                        aGames.Style.Add ("font-weight", "bold");
                        break;
                    }
                case "it":
                    {
                        aItems.Style.Add ("font-weight", "bold");
                        break;
                    }
                case "ca":
                    {
                        aContests.Style.Add ("font-weight", "bold");
                        break;
                    }
                case "pl":
                    {
                        //aPlaylist.Style.Add ("font-weight", "bold");
                        break;
                    }
                case "pm":
                    {
                        aPromotions.Style.Add ("font-weight", "bold");
                        break;
                    }
                case "nc":
                    {
                        aNameChange.Style.Add ("font-weight", "bold");
                        break;
                    }
                case "ip":
                    {
                        aBannedIPs.Style.Add ("font-weight", "bold");
                        break;
                    }
                case "rw":
                    {
                        aRestricted.Style.Add ("font-weight", "bold");
                        break;
                    }
                case "rm":
                    {
                        aRewards.Style.Add ("font-weight", "bold");
                        break;
                    }
            }
        }


        #region Properties

        /// <summary>
		/// The SubTab to display
		/// </summary>
		public string SubTab
		{
			get 
			{
				return m_SubTab;
			} 
			set
			{
				m_SubTab = value;
			}
		}

		protected string m_SubTab = "";

        #endregion

        #region Declerations

        protected HtmlAnchor aHome, aUserSearch, aGames, aItems, aContests;
        protected HtmlAnchor  aPromotions, aNameChange, aBannedIPs, aRestricted, aRewards;

        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
