///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Collections.Specialized;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.IO;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva;
using log4net;
using System.Drawing;
using MagicAjax;
using System.Collections.Generic;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;


namespace KlausEnt.KEP.Kaneva.Catalog
{
    public partial class CategoryManagement : NoBorderPage
    {
        #region Declarations

        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private const int ALLCATEGORIES = -1;

        #endregion

        protected CategoryManagement() 
		{
			Title = "Category Management";
        }

        #region PageLoad

        protected void Page_Load(object sender, EventArgs e)
        {
            pnlADMIN.Visible = (IsAdministrator());
            tblNoADMIN.Visible = (!IsAdministrator());
            this.ErrorMessages.Text = "";

            if (!IsPostBack)
            {
                //bind the CatalogItems
                BindCategoryData(currentPage);

                //hide details panel
                pnl_CategoryDetails.Visible = false;

                //hides delete choices div
                divCascadingDeleteConfirm.Visible = false;

            }

        }

        /// <summary>
        /// CategoryId
        /// </summary>
        private uint CategoryId()
        {
            if (Request ["id"] != null)
            {
                return (uint) (Convert.ToUInt32(Request["id"]));
            }
            else
            {
                return 0;
            }
        }

        #endregion

        #region Primary Functions

        /// <summary>
        /// BindStoreList
        /// </summary>
        private void BindParentsList(int idToExclude)
        {
            string filter = null;
            List<ItemCategory> parentsDTList = null;
            DataTable parentsDT = null;

            //get instance of the shopping facade
            ShoppingFacade shoppingFacade = new ShoppingFacade();


            //filter for excluding the item being edited
            if (idToExclude > 0)
            {
                filter = " item_category_id != " + idToExclude;
            }
            //search filter logic
            if ((ParentFilter != null) && (ParentFilter != "") && (filter != null))
            {
                filter += " AND " + ParentFilter;
            }
            else if ((ParentFilter != null) && (ParentFilter != "") && (filter == null))
            {
                filter = ParentFilter;
            }
            //retrieve the potential parent categories
            parentsDTList = shoppingFacade.GetItemCategoriesByFilter(filter);

            //initialize quick reference category data for use when editing to prevent round trip DB calls
            Categories.Clear();
            foreach (ItemCategory item in parentsDTList)
            {
                Categories.Add(item.ItemCategoryId.ToString(), item.Name.ToString());
            }

            //generate the datatable
            parentsDT = CreateTableFromList(parentsDTList);

            //create the radio field column for the get all stores data result
            DataColumn radioColumn = new DataColumn();
            radioColumn.DataType = System.Type.GetType("System.Boolean");
            radioColumn.AllowDBNull = false;
            radioColumn.ColumnName = "cbx_parents";
            radioColumn.DefaultValue = false;
            parentsDT.Columns.Add(radioColumn);

            if ((ParentCategoryId != null) && (ParentCategoryId != ""))
            {
                //mark the items in this category if any
                DataRow[] parents = parentsDT.Select("category_id = " + ParentCategoryId);
                if (parents.Length > 0)
                {
                    DataRow parentCategory = parents[0];
                    parentCategory["cbx_parents"] = true;
                    parentCategory.AcceptChanges();
                    parentsDT.AcceptChanges();
                }
            }

            dgd_Parents.DataSource = parentsDT;
            dgd_Parents.DataBind();
        }

        /// <summary>
        /// BindCatalogItemsData
        /// </summary>
        private void BindCategoryData(int currentPage)
        {
            //get instance of the shopping facade
            ShoppingFacade shoppingFacade = new ShoppingFacade();

            //query the database for existing CatalogItems
            List<ItemCategory> lItemCategory = shoppingFacade.GetItemCategoriesByFilter(CategoryFilter);

            DataTable dt = CreateTableFromList(lItemCategory);

            // Set the sortable columns
            string orderby = CurrentSort + " " + CurrentSortOrder;

            if (dt.Rows.Count > 0)
            {
                //set the current page
                this.dg_Categories.PageIndex = currentPage;

                //set the sort order
                dt.DefaultView.Sort = orderby;
                
                lbl_NoCategories.Visible = false;

                dg_Categories.DataSource = dt;
                dg_Categories.DataBind();
            }
            else
            {
                lbl_NoCategories.Visible = true;
                dg_Categories.DataSource = dt;
                dg_Categories.DataBind();
            }

        }

        /// <summary>
        /// BindStoreList
        /// </summary>
        private void BindItemList(int categoryId, int currentPage)
        {
            DataTable itemsInCategory = null;
            DataTable wokItemsDT = null;
            //get instance of the shopping facade
            ShoppingFacade shoppingFacade = new ShoppingFacade();

            //get all the current WOK stores
            wokItemsDT = CreateTableFromList(shoppingFacade.GetWOKItemsList());

            //create the check field column for the get all items data result
            DataColumn checkColumn = new DataColumn();
            checkColumn.DataType = System.Type.GetType("System.Boolean");
            checkColumn.AllowDBNull = false;
            checkColumn.ColumnName = "cbx_inCategory";
            checkColumn.DefaultValue = false;
            wokItemsDT.Columns.Add(checkColumn);

            //get all the stores that this item is in
            if (categoryId > 0)
            {
                itemsInCategory = shoppingFacade.GetWOKCategoryItemsByCategoryId(categoryId);
            }

            //mark the items in this category if any
            if ((itemsInCategory != null) && (itemsInCategory.Rows.Count > 0))
            {
                foreach (DataRow dr in itemsInCategory.Rows)
                {
                    string itemId = dr["global_id"].ToString();
                    DataRow[] wokItems = wokItemsDT.Select("global_id = " + itemId);
                    if (wokItems.Length > 0)
                    {
                        DataRow wokItem = wokItems[0];
                        wokItem["cbx_inCategory"] = true;
                        wokItem.AcceptChanges();
                        wokItemsDT.AcceptChanges();
                    }

                }
            }

            //set the current page
            this.dgd_ItemsNCategories.PageIndex = currentPage;

            dgd_ItemsNCategories.DataSource = wokItemsDT;
            dgd_ItemsNCategories.DataBind(); 
        }

        #endregion

        #region Helper Functions

        private DataTable CreateTableFromList(List<ItemCategory> list)
        {
            DataTable dt = new DataTable();

            dt.Columns.Add(new DataColumn("category_id", System.Type.GetType("System.Int32")));
            dt.Columns.Add(new DataColumn("category", System.Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("description", System.Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("parent_category_id", System.Type.GetType("System.Int32")));
            dt.Columns.Add(new DataColumn("sort_order", System.Type.GetType("System.Int32")));

            foreach (ItemCategory item in list)
            {
                DataRow row = dt.NewRow();
                row["category_id"] = item.ItemCategoryId;
                row["category"] = item.Name;
                row["description"] = item.Description;
                row["parent_category_id"] = item.ParentCategoryId;
                row["sort_order"] = item.SortOrder;

                dt.Rows.Add(row);
            }
            return dt;
        }

        private DataTable CreateTableFromList(List<WOKItem> list)
        {
            DataTable dt = new DataTable();

            dt.Columns.Add(new DataColumn("global_id", System.Type.GetType("System.Int32")));
            dt.Columns.Add(new DataColumn("display_name", System.Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("name", System.Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("description", System.Type.GetType("System.String")));

            foreach (WOKItem item in list)
            {
                DataRow row = dt.NewRow();
                row["global_id"] = item.GlobalId;
                row["display_name"] = item.DisplayName;
                row["name"] = item.Name;
                row["description"] = item.Description;

                dt.Rows.Add(row);
            }
            return dt;
        }

        private void NoteItemChanges(NameValueCollection collection, string id, string value)
        {
            if (collection[id] != null)
            {
                collection.Remove(id);
            }
            else
            {
                collection.Add(id, value);
            }
        }

        /// <summary>
        /// Clears out all Promotion fields 
        /// </summary>
        private void ClearCategoryData()
        {
            tbx_CategoryName.Text = "";
            tbx_CategoryNameID.Text = "";
            rdo_topCategory1.Checked = false;
            rdo_childCategory1.Checked = false;
            tbx_ParentCategoryName.Text = "";
            tbx_ParentCategoryID.Text = "";
            tbx_CategorySearchFilter.Text = "";
            dgd_Parents.DataSource = null;
            dgd_Parents.DataBind();
            dgd_ItemsNCategories.DataSource = null;
            dgd_ItemsNCategories.DataBind();
            tbx_ParentSearchFilter.Text = null;
            ParentFilter = null;
            ParentCategoryId = null;
            tbx_catergoryDescription.Text = "";

        }

        private void resetViewState()
        {
            CategoryFilter = null;
            currentPage = 0;
            currentPageItems = 0;
            itemChanges = null;
        }

        private void setDisplayConfig(bool choice)
        {
            tblrow_childCategory.Visible = !choice;
            tblrow_parentSearch.Visible = !choice;

        }

        private void DeleteCategory(int categoryId, int parentCategoryId, bool recursiveDelete)
        {
            //get instance of the shopping facade
            ShoppingFacade shoppingFacade = new ShoppingFacade();

            //delete the category   
            if (recursiveDelete)
            {
                shoppingFacade.RecursiveDeleteParentWOKCategory(categoryId, parentCategoryId);
            }
            else
            {
                shoppingFacade.DeleteItemCategory(categoryId, parentCategoryId);
            }

            //rebind the data
            BindCategoryData(currentPage);

            //clear the fields
            ClearCategoryData();

            //reset viewstate
            resetViewState();

            //make the details panel visible
            pnl_CategoryDetails.Visible = false;
        }

        protected string LevelIndicator(string parentId)
        {
            string level = "";
            try
            {
                int id = Convert.ToInt32(parentId);
                if (id == 0)
                {
                    level = "Top Level";
                }
            }
            catch (FormatException)
            {
            }
            return level;
        }

        #endregion

        #region Properties

        public int PageSize()
        {
            return 10;
        }

        /// <summary>
        /// DEFAULT_SORT
        /// </summary>
        /// <returns></returns>
        protected override string DEFAULT_SORT
        {
            get
            {
                return "category";
            }
        }

        private string CategoryFilter
        {
            get
            {
                if (ViewState["_CategoryFilter"] == null)
                {
                    ViewState["_CategoryFilter"] = "";
                }
                return ViewState["_CategoryFilter"].ToString();
            }
            set
            {
                ViewState["_CategoryFilter"] = value;
            }
        }

        private string ParentFilter
        {
            get
            {
                if (ViewState["_ParentFilter"] == null)
                {
                    ViewState["_ParentFilter"] = "";
                }
                return ViewState["_ParentFilter"].ToString();
            }
            set
            {
                ViewState["_ParentFilter"] = value;
            }
        }

        private string ParentCategoryId
        {
            get
            {
                if (ViewState["_ParentCategoryId"] == null)
                {
                    ViewState["_ParentCategoryId"] = "";
                }
                return ViewState["_ParentCategoryId"].ToString();
            }
            set
            {
                ViewState["_ParentCategoryId"] = value;
            }
        }


        private int currentPage
        {
            get
            {
                if (ViewState["_currentPage"] == null)
                {
                    ViewState["_currentPage"] = 0;
                }
                return (int)ViewState["_currentPage"];
            }
            set
            {
                ViewState["_currentPage"] = value;
            }
        }

        private int currentPageItems
        {
            get
            {
                if (ViewState["_currentPageItems"] == null)
                {
                    ViewState["_currentPageItems"] = 0;
                }
                return (int)ViewState["_currentPageItems"];
            }
            set
            {
                ViewState["_currentPageItems"] = value;
            }
        }

        private NameValueCollection itemChanges
        {
            get
            {
                if (ViewState["_itemChanges"] == null)
                {
                    ViewState["_itemChanges"] = new NameValueCollection();
                }
                return (NameValueCollection)ViewState["_itemChanges"];
            }
            set
            {
                ViewState["_itemChanges"] = value;
            }
        }

        private NameValueCollection Categories
        {
            get
            {
                if (ViewState["_Categories"] == null)
                {
                    ViewState["_Categories"] = new NameValueCollection();
                }
                return (NameValueCollection)ViewState["_Categories"];
            }
            set
            {
                ViewState["_Categories"] = value;
            }
        }


        private int topLevelCategoryToDelete
        {
            get
            {
                if (ViewState["_topLevelId"] == null)
                {
                    ViewState["_topLevelId"] = 0;
                }
                return (int)ViewState["_topLevelId"];
            }
            set
            {
                ViewState["_topLevelId"] = value;
            }
        }


        //allows user to switch the ordering between DESC and ASC
        //based on column clicked
        private void SortSwitch(string newSort)
        {
            if (CurrentSort == newSort)
            {
                if (CurrentSortOrder == "DESC")
                {
                    CurrentSortOrder = "ASC";
                }
                else
                {
                    CurrentSortOrder = "DESC";
                }
            }
            else
            {
                CurrentSort = newSort;
                CurrentSortOrder = "DESC";
            }
        }

        /// <summary>
        /// DEFAULT_SORT
        /// </summary>
        /// <returns></returns>
        protected override string DEFAULT_SORT_ORDER
        {
            get
            {
                return "DESC";
            }
        }

        private DataTable categoriesDT
        {
            get
            {
                if (Session["_categoriesDT"] == null)
                {
                    Session["_categoriesDT"] = new DataTable();
                }
                return (DataTable)Session["_categoriesDT"];
            }
            set
            {
                Session["_categoriesDT"] = value;
            }
        }

        #endregion


        #region Event Handlers


        protected void btn_Cancel_Click(object sender, System.EventArgs e)
        {
            //clear the fields
            ClearCategoryData();

            //make the details panel visible
            pnl_CategoryDetails.Visible = false;

        }

        protected void btn_ParentSearch_Click(object sender, EventArgs e)
        {
            if (tbx_ParentSearchFilter.Text != "")
            {
                if (KanevaGlobals.IsNumeric(tbx_ParentSearchFilter.Text))
                {
                    ParentFilter = "item_category_id = " + tbx_ParentSearchFilter.Text;
                }
                else
                {
                    ParentFilter = "name LIKE '%" + tbx_ParentSearchFilter.Text + "%'";
                }
            }
            else
            {
                ParentFilter = null;
            }

            //pass in the current id to also filter by the currently selected category
            //categories can't be parents for themselves
            BindParentsList(Convert.ToInt32(tbx_CategoryNameID.Text));
        }

        protected void rdb_CategoryLevel_CheckChanged(object sender, EventArgs e)
        {
            setDisplayConfig(rdo_topCategory1.Checked);
            if (rdo_topCategory1.Checked)
            {
                tbx_ParentCategoryID.Text = "";
                tbx_ParentCategoryName.Text = "";
            }

        }

        protected void categoryParent_CheckedChanged(object sender, EventArgs e)
        {
            GridViewRow ritem = (GridViewRow)((Control)sender).NamingContainer;
            tbx_ParentCategoryName.Text = Server.HtmlDecode(ritem.Cells[2].Text);
            tbx_ParentCategoryID.Text = ((Label)ritem.FindControl("lbl_categoryId")).Text;
            ParentCategoryId = tbx_ParentCategoryID.Text;

            string strJavascript = "<script language=\"JavaScript\">ClearCategoryRButtons('" + ritem.Cells[0].Controls[1].ClientID + "');</script>";
            if (!ClientScript.IsStartupScriptRegistered(GetType(), "radioBreset"))
            {
                ClientScript.RegisterStartupScript(GetType(), "radioBreset", strJavascript);
            }
        }

        protected void cbx_items_CheckedChanged(object sender, EventArgs e)
        {
            GridViewRow ritem = (GridViewRow)((Control)sender).NamingContainer;
            NoteItemChanges(itemChanges, ((Label)ritem.FindControl("lbl_id")).Text, ((CheckBox)ritem.FindControl("cbx_items")).Checked.ToString());
        }

        public void btn_CategorySearch_Click(object sender, EventArgs e)
        {
            string categorySearchvalue = tbx_CategorySearchFilter.Text.Trim();

            if ((categorySearchvalue != "") && (KanevaGlobals.IsNumeric(categorySearchvalue)))
            {
                CategoryFilter = "item_category_id = " + tbx_CategorySearchFilter.Text;
            }
            else
            {
                if (categorySearchvalue == "")
                {
                    CategoryFilter = "";
                }
                else
                {
                    CategoryFilter = "name LIKE '%" + tbx_CategorySearchFilter.Text + "%'";
                }
            }
            BindCategoryData(currentPage);

        }

        protected void dg_Categories_RowEditing(object source, System.Web.UI.WebControls.GridViewEditEventArgs e)
        {
            //clear the fields
            ClearCategoryData();

            //make the CatalogItems details panel visible
            pnl_CategoryDetails.Visible = true;

            //get the catalog item
            GridViewRow gvr = dg_Categories.Rows[e.NewEditIndex];


            try
            {
                int parentID = 0;

                //populate fields
                //replace is a work around for unexplained &nbsp; showing up figure it out later
                tbx_CategoryNameID.Text = gvr.Cells[0].Text.Replace("&nbsp;", "");
                tbx_CategoryName.Text = Server.HtmlDecode(gvr.Cells[1].Text.Replace("&nbsp;", ""));
                tbx_ParentCategoryID.Text = gvr.Cells[3].Text.Replace("&nbsp;", "");
                tbx_catergoryDescription.Text = gvr.Cells[2].Text.Replace("&nbsp;", "");

                //get the parent category list
                ParentCategoryId = tbx_ParentCategoryID.Text;
                BindParentsList(Convert.ToInt32(tbx_CategoryNameID.Text));

                //bind the category items
                BindItemList(Convert.ToInt32(tbx_CategoryNameID.Text), currentPageItems);

                try
                {
                    parentID = Convert.ToInt32(tbx_ParentCategoryID.Text);
                }
                catch (FormatException fe)
                {
                    m_logger.Error("Category Management ", fe);
                }

                if (parentID > 0)
                {
                    rdo_topCategory1.Checked = false;
                    rdo_childCategory1.Checked = true;

                    //pullback parent category name from categories collection to prevent
                    //unnecessary  round trip to Datbase
                    tbx_ParentCategoryName.Text = Server.HtmlDecode(Categories[parentID.ToString()].ToString());
                }
                else
                {
                    rdo_topCategory1.Checked = true;
                    rdo_childCategory1.Checked = false;
                }

                //configure display
                setDisplayConfig(rdo_topCategory1.Checked);


            }
            catch (Exception ex)
            {
                ErrorMessages.ForeColor = Color.DarkRed;
                ErrorMessages.Text = "Error displaying promotion detials: " + ex.Message;
                return;
            }

        }

        protected void dg_Categories_PageIndexChanging(object source, System.Web.UI.WebControls.GridViewPageEventArgs e)
        {
            currentPage = e.NewPageIndex;
            BindCategoryData(currentPage);
        }

        protected void dgd_ItemsNCategories_PageIndexChanging(object source, System.Web.UI.WebControls.GridViewPageEventArgs e)
        {
            currentPageItems = e.NewPageIndex;
            int categoryId = -1;
            if ((tbx_CategoryNameID.Text != null) && (tbx_CategoryNameID.Text != ""))
            {
                categoryId = Convert.ToInt32(tbx_CategoryNameID.Text);
            }
            BindItemList(categoryId, currentPageItems);
        }
 
        protected void dg_Categories_Sorting(Object sender, GridViewSortEventArgs e)
        {
            SortSwitch(e.SortExpression); //sets the sort expression
            BindCategoryData(currentPage);
        }

        protected void dgd_Parents_OnRowCreated(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
        }

        protected void dgd_Parents_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            //logic check to work around initial row creation wheere there is only one column
            if (e.Row.Cells.Count >= 3)
            {
                e.Row.Cells[2].Text = Server.HtmlDecode(e.Row.Cells[2].Text);

            }
        }

        protected void dgd_ItemsNCategories_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            //logic check to work around initial row creation wheere there is only one column
            if (e.Row.Cells.Count >= 4)
            {
                e.Row.Cells[2].Text = Server.HtmlDecode(e.Row.Cells[2].Text);
                e.Row.Cells[3].Text = Server.HtmlDecode(e.Row.Cells[3].Text);
                e.Row.Cells[4].Text = Server.HtmlDecode(e.Row.Cells[4].Text);
            }
        }


        protected void dg_Categories_OnRowCreated(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            //logic check to work around initial row creation wheere there is only one column
            if (e.Row.Cells.Count >= 4)
            {
                e.Row.Cells[0].Visible = false;
                e.Row.Cells[1].Visible = true;
                e.Row.Cells[2].Visible = true;
                e.Row.Cells[3].Visible = false;
                e.Row.Cells[4].Visible = true;
            }

        }

        protected void dg_Categories_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            //logic check to work around initial row creation wheere there is only one column
            if (e.Row.Cells.Count >= 4)
            {
                e.Row.Cells[1].Text = Server.HtmlDecode(e.Row.Cells[1].Text);
            }

        }

        protected void btnCascadingDelete_Click(object sender, System.EventArgs e)
        {
            bool recursiveDelete = true;
            //parent category id is hard coded to one since this shouls only
            //be called if deleting a top level category
            DeleteCategory(topLevelCategoryToDelete, 0, recursiveDelete);
            divCascadingDeleteConfirm.Visible = false;
        }

        protected void btnNormalDelete_Click(object sender, System.EventArgs e)
        {
            bool recursiveDelete = false;
            //parent category id is hard coded to one since this shouls only
            //be called if deleting a top level category
            DeleteCategory(topLevelCategoryToDelete, 0, recursiveDelete);
            divCascadingDeleteConfirm.Visible = false;
        }

        protected void dg_Categories_RowDeleting(object source, System.Web.UI.WebControls.GridViewDeleteEventArgs e)
        {
            bool recursiveDelete = false;

            //get the selected row
            GridViewRow gvr = dg_Categories.Rows[e.RowIndex];

            //get the the category ids
            int categoryId = Convert.ToInt32(gvr.Cells[0].Text);
            int parentCategoryId = Convert.ToInt32(gvr.Cells[3].Text);

            //check to see if this is a top level deletion
            if (parentCategoryId == 0)
            {
                //store the top level category id (parent id is zero if it is top level)
                topLevelCategoryToDelete = categoryId;

                //make deletion choices visible
                divCascadingDeleteConfirm.Visible = true;
            }
            else
            {
                //this handles non-parent level deletions immediately
                DeleteCategory(categoryId, parentCategoryId, recursiveDelete);
            }

        }

        protected void lbn_NewCategory_Click(object sender, System.EventArgs e)
        {
            //clear the fields
            ClearCategoryData();

            //make the Catelog details panel visible
            pnl_CategoryDetails.Visible = true;

            //default to top category
            rdo_topCategory1.Checked = true;
            setDisplayConfig(rdo_topCategory1.Checked);

            //set id to -1 <indicator that it is a new entry>
            tbx_CategoryNameID.Text = "-1";

            //populate the parents list box
            BindParentsList(ALLCATEGORIES);
            //populate the available items / in category list
            BindItemList(-1, currentPageItems);

        }

        protected void btn_Save_Click(object sender, System.EventArgs e)
        {
            if (!Page.IsValid)
            {
                return;
            }

            ShoppingFacade shopFacade = new ShoppingFacade();

            //grab WOK Items data source
            //DataTable itemCategorychanges = wokItemsDT.GetChanges();
            bool errorsOccured = false;
            int categoryID = -1;
            int parentCategoryID = 0;
            int idcheck = -1;

            try
            {
                categoryID = Convert.ToInt32(tbx_CategoryNameID.Text);
                if (tbx_ParentCategoryID.Text != "")
                {
                    parentCategoryID = Convert.ToInt32(tbx_ParentCategoryID.Text);
                }

                //grab all data for the promotion and update database
                if (categoryID > 0)
                {
                    //idcheck = shopFacade.UpdateWOKCategory(categoryID, Server.HtmlEncode(tbx_CategoryName.Text), parentCategoryID, tbx_MarketingPath.Value, GetUserId());
                    idcheck = shopFacade.UpdateWOKCategory(categoryID, Server.HtmlEncode(tbx_CategoryName.Text), parentCategoryID, tbx_catergoryDescription.Text, GetUserId());
                }
                else
                {
                    //this insert returns the new id of the new category which is defaulted to 0
                    //which indicates failure 
                    //idcheck = shopFacade.InsertWOKCategory(Server.HtmlEncode(tbx_CategoryName.Text), parentCategoryID, tbx_MarketingPath.Value, GetUserId());
                    idcheck = shopFacade.InsertWOKCategory(Server.HtmlEncode(tbx_CategoryName.Text), parentCategoryID, tbx_catergoryDescription.Text, GetUserId());
                    categoryID = idcheck;
                }

                if (idcheck <= 0)
                {
                    errorsOccured = true;
                    throw new Exception();
                }
            }
            catch (Exception ex)
            {
                ErrorMessages.Text = "Error Saving Your Category: " + ex.Message;
                ErrorMessages.ForeColor = Color.DarkRed;
                return;
            }

            //write changes in category items to database 
            if (itemChanges.Count > 0)
            {
                foreach (string key in itemChanges.Keys)
                {
                    bool inCategory = Convert.ToBoolean(itemChanges[key].ToLower());
                    int itemId = Convert.ToInt32(key);

                    if (!inCategory)
                    {
                        //if item is unchecked (no longer in set) remove it
                        idcheck = shopFacade.DeleteCategoryFromItem(itemId, (uint)categoryID);
                    }
                    else
                    {
                        //check to see if record already exist
                        DataTable inCategoryAlready = shopFacade.GetWOKCategoryItems(itemId, categoryID);
                        if (inCategoryAlready.Rows.Count <= 0)
                        {
                            ItemCategory itemCategory = new ItemCategory();
                            itemCategory.ItemCategoryId = (uint)categoryID;

                            idcheck = shopFacade.AddItemCategoryItem(itemId, itemCategory);
                        }
                    }

                    if (idcheck <= 0)
                    {
                        errorsOccured = true;
                    }

                }
            }

            //reset viewstate
            resetViewState();

            ClearCategoryData();

            //pulledsaved info from database
            BindCategoryData(currentPage);

            //make the details panel invisible
            pnl_CategoryDetails.Visible = false;

            if (!errorsOccured)
            {
                ErrorMessages.Text = "Save successful!";
                ErrorMessages.ForeColor = Color.DarkGreen;
            }
            else
            {
                ErrorMessages.Text = "An error has occured during save!";
                ErrorMessages.ForeColor = Color.DarkRed;
            }

        }

        #endregion


    }
}
