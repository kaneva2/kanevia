<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CatalogNav.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.Administration.Catalog.CatalogNav" %>
<table width="100%" CellPadding="0" CellSpacing="0" Border="0">
	<tr>
		<td bgcolor="#eeeeff" style="PADDING-RIGHT: 4px; PADDING-LEFT: 4px; FONT-SIZE: 10px; PADDING-BOTTOM: 7px; WORD-SPACING: normal; COLOR: #000000; LINE-HEIGHT: 10px; PADDING-TOP: 7px; FONT-STYLE: normal; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; LETTER-SPACING: normal; TEXT-DECORATION: none">
			&nbsp;
			<a href="~/Administration/Catalog/CatalogAdministration.aspx" class="t4" style="CURSOR:hand" runat="server" id="aItems">Catalog Administration</a>| 
			<a href="~/Administration/Catalog/CategoryManagement.aspx" class="t4" style="CURSOR:hand" runat="server" id="aCategories">Category Management</a>
		</td>
	</tr>
</table>
