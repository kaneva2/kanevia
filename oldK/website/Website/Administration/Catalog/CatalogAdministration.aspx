<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CatalogAdministration.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.Catalog.CatalogAdministration" %>
<%@ Register TagPrefix="Kaneva" TagName="CATNav" Src="CatalogNav.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="AdminMenu" Src="../../usercontrols/AdministrativeMenu.ascx" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>

<script type="text/javascript" src="../../jscript/yahoo/yahoo-min.js"></script>  
<script type="text/javascript" src="../../jscript/yahoo/event-min.js" ></script>   
<script type="text/javascript" src="../../jscript/yahoo/dom-min.js" ></script>   
<script type="text/javascript" src="../../jscript/yahoo/calendar-min.js"></script>

<script type="text/javascript" language="Javascript" src="../../jscript/PageLayout/colorpicker.js"></script>
<script type="text/javascript" language="javascript" src="../../jscript/prototype.js"></script>

<link href="../../css/new.css" rel="stylesheet" type="text/css" />
<link type="text/css" rel="stylesheet" href="../../css/yahoo/calendar.css" />   
<link href="../../css/editWidgets.css" rel="stylesheet" type="text/css" />


<style >
	#cal1Container { display:none; position:absolute; z-index:100;}
	
	#popUpWinAddCat {
	width: 700px;
	background: #FFFFFF;
	text-align: center;
	padding: 40px 0px 10px;
	color: #990000;
	font-size: 14px;
	height: 275px;
	opacity:0.85;
	display: none;
	margin: 0px;
	border: 1px solid #990000;
	position: absolute;
	left: 100px;
	top: 350px;
	}
	
#popUpWinAddCat div {
	background: #FFFFFF;
	padding-right: 50px;
	padding-left: 50px;
	text-align: left;
	}
	
#popUpWinAddCat h1 {
	margin: 0px 0px 10px 5px;
	padding: 0px;
}
	
#popUpWinAddCat p {
	background: #FFFFFF;
	}

#popUpWinAddCat input {
	margin-top: 10px;
	width: 200px;
	}
</style>
 
 
<script type="text/javascript"><!--
 function specialCharReplace()
 {
   //replace for item text, display text, and description;
    var itemName = document.getElementById("tbx_ItemName");
    itemName.value = itemName.value.replace(/amp;/g,"")

    var itemDisplayName = document.getElementById("tbx_DisplayName");
    itemDisplayName.value = itemDisplayName.value.replace(/amp;/g,"")

    var itemDescription = document.getElementById("tbx_Description");
    itemDescription.value = itemDescription.value.replace(/amp;/g,"")

 }
 
YAHOO.namespace("example.calendar");

    
	function handleAddedDate(type,args,obj) {
		var dates = args[0]; 
		var date = dates[0];
		var year = date[0], month = date[1], day = date[2];
		
		var txtDate1 = document.getElementById("txtItemAddedDate");
		txtDate1.value = month + "-" + day + "-" + year;
		YAHOO.example.calendar.cal1.hide();
	}
	
	function initAddedDate() {
		// Admin Calendar
		YAHOO.example.calendar.cal1 = new YAHOO.widget.Calendar ("cal1", "cal1Container", { iframe:true, zIndex:200, mindate:"1/1/2007", title:"Choose a date:", close:true } );   
		YAHOO.example.calendar.cal1.render();
		
		// Listener to show Admin Calendar when the button is clicked   
		YAHOO.util.Event.addListener("imgItemAddDate", "click", YAHOO.example.calendar.cal1.show, YAHOO.example.calendar.cal1, true);   
		YAHOO.example.calendar.cal1.selectEvent.subscribe(handleAddedDate, YAHOO.example.calendar.cal1, true);
	}
	
//--> </script>  

<Kaneva:AdminMenu ID="adminmenu" runat="server" />
<asp:ValidationSummary ShowMessageBox="False" ShowSummary="True" Class="formError" id="valSum" DisplayMode="BulletList" runat="server" HeaderText="Please correct the following errors:"/>

<table runat="server" id="tblNoADMIN" cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr><td align="left"><Kaneva:CATNav ID="CATNav1" runat="server" width="100%" SubTab="us"/></td></tr>
	<tr><td ALIGN="CENTER">
		<BR><BR><BR>
		<span class="subHead">You must be an Administrator to access this section.</span>
	</td></tr>
</table>

<asp:panel runat="server" ID="pnlADMIN">

<Kaneva:CATNav runat="server" id="catNavigation" SubTab="us"/>

<br>

 <table id="tbl_AvailableItems" border="0" cellpadding="0" cellspacing="0" width="1000" style="background-color:#ffffff">
    <tr>
        <td align="center"><span style="height:30px; font-size:28px; font-weight:bold">Catalog Item Management</span><br /><asp:Label runat="server" id="ErrorMessages"></asp:Label></td>
    </tr>
   <tr>
         <td style="height:20px"></td>
    </tr>
    <tr>
         <td><asp:Label id="lbl_NoCatalogItems" visible="false" runat="server"><span style="color:Navy; size:24pt; font-weight:bold">No Catalog Items Were Found.</span> </asp:Label></td>
    </tr>
    <tr>
         <td>
            
            <table border="0" width="98%" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="100px" align="left" valign="bottom"><asp:LinkButton enabled="false" id="lbn_NewItem" runat="server" causesvalidation="false" onclick="lbn_NewItems_Click">Add New Item</asp:LinkButton></td>
                    <td align="right" style="padding: 6px 0px 6px 6px; font-size:10pt"><b>Search</b> &nbsp;<asp:TextBox runat="server" id="tbx_WOKSearchFilter"></asp:TextBox>&nbsp;<asp:Button id="btn_WOKSearch" causesvalidation="false" onclick="btn_WOKSearch_Click" runat="server" text="Search"></asp:Button></td>
                </tr>
            </table>
            
            <asp:gridview id="dg_CatalogItems" runat="server" onRowDataBound="dg_CatalogItems_RowDataBound" onsorting="dg_CatalogItems_Sorting" OnRowCreated="dg_CatalogItemsmyGrid_OnRowCreated" autogeneratecolumns="False" width="980px" OnPageIndexChanging="dg_CatalogItems_PageIndexChanging" OnRowEditing="dg_CatalogItems_RowEditing" AllowSorting="True" border="0" cellpadding="2" PageSize="5" AllowPaging="True">
                <RowStyle BackColor="White"></RowStyle>
                <HeaderStyle BackColor="LightGray" Font-Underline="True" Font-Bold="True" ForeColor="#000000" HorizontalAlign="Left"></HeaderStyle>
                <AlternatingRowStyle BackColor="Gainsboro"></AlternatingRowStyle>
                <FooterStyle BackColor="#FFffff"></FooterStyle>
                <Columns>
                    <asp:BoundField HeaderText="Item ID" DataField="global_id" SortExpression="global_id" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
                    <asp:TemplateField HeaderText="Username" SortExpression="item_creator_id">
                     <ItemTemplate> 
                        <asp:Label runat="server" text='<%#GetUserName(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "item_creator_id")))%>'></asp:Label>
                    </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="Internal Name" DataField="name" SortExpression="name" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
                    <asp:BoundField HeaderText="Display Name" DataField="display_name" SortExpression="display_name" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
                    <asp:BoundField HeaderText="Item Description" DataField="description" SortExpression="description" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
                    <asp:BoundField HeaderText="Date Added" DataField="date_added" SortExpression="date_added" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
                    <asp:BoundField HeaderText="Initial Quantity" DataField="product_amount" SortExpression="product_amount" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
                    <asp:BoundField HeaderText="Selling Price" DataField="market_cost" SortExpression="market_cost" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
                    <asp:BoundField HeaderText="Buy Back Price" DataField="selling_price" SortExpression="selling_price" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
                    <asp:BoundField HeaderText="Creator" DataField="item_creator_id" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
                    <asp:BoundField HeaderText="Currency Type" DataField="currency_type_id" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
                    <asp:BoundField HeaderText="Availability" DataField="product_availability_id" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
                    <asp:BoundField HeaderText="Exclusivity" DataField="product_exclusivity_id" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
                    <asp:BoundField HeaderText="Image Path" DataField="item_image_path" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
                    <asp:BoundField HeaderText="Active" DataField="item_active" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
                    <asp:TemplateField HeaderText="Catalog" SortExpression="item_active">
                     <ItemTemplate> 
                        <asp:Label runat="server" text='<%#DataBinder.Eval(Container.DataItem, "item_active").Equals(1)%>'></asp:Label>
                    </ItemTemplate>
                    </asp:TemplateField>
                    <asp:CommandField CausesValidation="False" ShowEditButton="True"></asp:CommandField>        
                </Columns>
            </asp:gridview>
         </td>
    </tr>
    <tr>
         <td style="height:20px"></td>
    </tr>
    <tr>
         <td>
            <asp:panel id="pnl_CatalogItemsDetails" runat="server" >
                <hr />
                <table style="font-weight:bold" id="tbl_OfferDetails" border="1" frame="below" cellpadding="5px" cellspacing="0" width="100%">
                     <tr >
                        <td align="left" colspan="4">
                            <asp:Label runat="server" id="lbl_DetailsTitle"></asp:Label>
                        </td>
                    </tr>
                     <tr >
                        <td align="left" colspan="2">
                            <asp:Image runat="server" id="itemImage" border="2px"></asp:Image>
                            <asp:dropdownlist id="dgd_drpActive" runat="server">
                                <asp:ListItem Value="0">Inactive - Not shown in catalog or user inventory</asp:ListItem>
                                <asp:ListItem Value="1">Public - Shown in catalog and inventory</asp:ListItem>
                                <asp:ListItem Value="2">Private - Not shown in catalog</asp:ListItem>
                            </asp:dropdownlist><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Inactive removes from Shopping site)
                        </td>
                        <td align="left" colspan="2">
                            <asp:button runat="server" Text="Remove from Shopping and Remove From All Inventories" causesValidation="false" onclick="btnRemoveInventory_Click"/>
                        </td>
                    </tr>
                    <ajax:ajaxpanel id="Ajaxpanel1" runat="server">                    
                    <tr >
                        <td align="right" style="width:120px"><label style="color: Red">*</label>Item Name:</td>
                        <td align="left" style="width:350px">
                            <asp:TextBox runat="server" maxlength="125" id="tbx_ItemName" width="280px"></asp:TextBox>
                            <asp:RequiredFieldValidator runat="server" id="rfvItemName" controltovalidate="tbx_ItemName" text="* Please enter a value." errormessage="The item name is required." display="Dynamic"></asp:RequiredFieldValidator>
                            <asp:TextBox runat="server" maxlength="125" visible="false" id="tbx_ItemID"></asp:TextBox>
                        </td>
                        <td align="right" style="width:150px"><label style="color: Red">*</label>Inventory Type:</td>
                        <td align="left">
                            <asp:DropDownList runat="server" id="ddl_CurrencyType" width="200px"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" style="width:120px"><label style="color: Red">*</label>Display Name:</td>
                        <td align="left" style="width:350px">
                            <asp:TextBox runat="server" id="tbx_DisplayName" width="280px"></asp:TextBox>
                            <asp:RequiredFieldValidator runat="server" id="rfvDisplayName" controltovalidate="tbx_DisplayName" text="* Please enter a value." errormessage="The display name is required." display="Dynamic"></asp:RequiredFieldValidator>
                        </td>
                         <td align="right" style="width:150px"><label style="color: Red">*</label>Access Type:</td>
                        <td align="left">
                                <div style="height:130px; width:200px; overflow:auto">
                                    <asp:GridView runat="server" id="dgd_PassGroups" AllowPaging="false" cellpadding="2" border="1" BorderColor="Navy" autogeneratecolumns="False" width="200px">
                                        <RowStyle BackColor="White"></RowStyle>
                                        <AlternatingRowStyle BackColor="Gainsboro"></AlternatingRowStyle>
                                        <FooterStyle BackColor="#FFffff"></FooterStyle>
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:CheckBox runat="server" id="cbx_inPassGroup" autopostback="true"  checked='<%# DataBinder.Eval(Container.DataItem, "cbx_inPassGroup")%>' oncheckedchanged="cbx_inPassGroup_CheckedChanged"></asp:CheckBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>                                       
                                             <asp:TemplateField visible="false">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" id="lbl_PassGroupId" Text='<%# DataBinder.Eval(Container.DataItem, "pass_group_id")%>'> </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>                                       
                                            <asp:BoundField ReadOnly="True" HeaderText="Pass Group" DataField="name"></asp:BoundField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                        </td>
                   </tr>
                     <tr>
                        <td align="right" style="width:120px"><label style="color: Red">*</label>Description:</td>
                        <td align="left" style="width:350px">
                            <asp:TextBox runat="server" id="tbx_Description" width="280px" TextMode="MultiLine" Rows="4"></asp:TextBox>
                            <asp:RequiredFieldValidator runat="server" id="rfvDescription" controltovalidate="tbx_Description" text="* Please enter a value." errormessage="The item description is required." display="Dynamic"></asp:RequiredFieldValidator>
                        </td>
                         <td align="right" style="width:150px"><label style="color: Red">*</label>Product Availability:</td>
                        <td align="left">
                            <asp:DropDownList runat="server" id="ddl_ProductAvailability" width="200px"></asp:DropDownList>
                        </td>
                    </tr>
                     <tr >
                        <td align="right" style="width:120px"><label style="color: Red">*</label>Initial Count:</td>
                        <td title="insert 0 to indicate unlimited amount" align="left" style="width:350px">
                            <asp:TextBox runat="server" id="tbx_InitialAmount" width="280px"></asp:TextBox>
                            <asp:RequiredFieldValidator runat="server" id="rfvInitialCount" controltovalidate="tbx_InitialAmount" text="* Please enter a value." errormessage="The intial amount produced is required." display="Dynamic"></asp:RequiredFieldValidator>
                        </td>
                        <td align="right" style="width:150px"><label style="color: Red">*</label>Product Exclusivity:</td>
                        <td align="left">
                            <asp:DropDownList runat="server" id="ddl_ProductExclusivity" width="200px"></asp:DropDownList>
                        </td>
                    </tr>
                   <tr>
                        <td align="right" style="width:120px"><label style="color: Red">*</label>Date Added:</td>
                        <td align="left" style="width:350px">
						    <asp:TextBox enabled="false" ID="txtItemAddedDate" class="formKanevaText" style="width:260px" MaxLength="10" runat="server"/>
						    <img id="imgItemAddDate" name="imgItemAddDate" onload="initAddedDate();" src="../../images/blast/cal_16.gif"/>						
	                        <BR><small>MM-dd-yyyy</small><div id="cal1Container"></div>
	                        <asp:RequiredFieldValidator runat="server" id="rfvStartDate" controltovalidate="txtItemAddedDate" text="* Please enter a value." errormessage="The item creation date is required." display="Dynamic"></asp:RequiredFieldValidator>
                       </td>
                        <td align="right" style="width:150px"><label style="color: Red">*</label>Selling Price:</td>
                        <td align="left">
                            <asp:TextBox runat="server" id="tbx_SellingPrice" width="100px" causesvalidation="false" autopostback="true" ontextchanged="tbx_SellingPrice_TextChanged"></asp:TextBox>
                            <asp:RequiredFieldValidator runat="server" id="rfvSellingPrice" controltovalidate="tbx_SellingPrice" text="* Please enter a value." errormessage="The selling price is required." display="Dynamic"></asp:RequiredFieldValidator>
                            <asp:TextBox runat="server" id="tbx_DesignerPrice" Enabled="False"  width="100px" causesvalidation="false" autopostback="true" ontextchanged="tbx_SellingPrice_TextChanged"></asp:TextBox>
                            <br/><small>Selling Price</small>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<small>Designer Commision</small>
                        </td>
                    </tr>
                    </ajax:ajaxpanel>
                    <tr>
                        <td align="right" style="width:120px">Categories:</td>
                        <td align="left" style="width:350px">
                            <ajax:ajaxpanel id="AjaxpanelRemoveCat" runat="server"> 
                                <ul>
                                    <asp:Repeater ID="rptCategories" runat="server">
                                        <ItemTemplate>                
                                           <li><%# DataBinder.Eval(Container.DataItem, "name")%>&nbsp;<asp:LinkButton id="lbDeleteCategory" Text='Remove' OnCommand="lbCategoryDelete_Click" runat="server" CommandName="cmdClick" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ItemCategoryId")%>'/></li>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ul>
                            </ajax:ajaxpanel>
                       </td>
                        <td align="left" colspan="2">
                            <ajax:ajaxpanel id="AjaxpanelAddCat" runat="server"> 
                                <asp:Repeater ID="rptAddCategories" runat="server">
                                    <ItemTemplate>                
                                       <asp:checkbox runat="server" id="chkSelect"></asp:checkbox>
                                       <asp:LinkButton id="lbCategory" Text='<%# DataBinder.Eval(Container.DataItem, "name")%>' OnCommand="lbCategory_Click" runat="server" CommandName="cmdClick" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ItemCategoryId")%>'/>
                                       <input type="hidden" runat="server" id="hidItemCategoryId" value='<%#DataBinder.Eval(Container.DataItem, "ItemCategoryId")%>' name="hidItemCategoryId"/>
                                       <br />
                                    </ItemTemplate>
                                </asp:Repeater>
                                
                                <asp:Button runat="server" AjaxCall="Async" id="button" Text="Add Selected Categories" OnClick="btnSaveCategory_Click" CausesValidation="False"></asp:Button><asp:button runat="server" AjaxCall="Async" text="Go To Top" OnClick="btnAddCategory_Click" CausesValidation="False"/>
                            </ajax:ajaxpanel>
                            
                        </td>
                    </tr>

                    <tr>
                        <td align="right" style="width:120px"><label style="color: Red">*</label>Item Image:</td>
                        <td align="left" style="width:350px">
                            <input class="formKanevaText" id="inp_itemImage" type="text" size="43" name="inp_itemImage" runat="server" style="WIDTH: 198px; position:absolute; z-index:3"  /> 
                            <input type="file" runat="server" name="browseTHUMB" id="browseTHUMB" onchange="inp_itemImage.value=browseTHUMB.value;" style="WIDTH: 200px; z-index:2; float:right; left:-70px; position:relative" />
                           <!-- <input type="button" onclick="browseTHUMB.click();inp_itemImage.value=browseTHUMB.value;" value="Browse..."  />   -->                     
                       </td>
                       <ajax:ajaxpanel id="Ajaxpanel2" runat="server"> 
                        <td align="right" style="width:150px"><label style="color: Red">*</label>Buy Back Price:</td>
                        <td align="left" title="select the buyback percentage">
                            <asp:TextBox runat="server" id="tbx_Price" enabled="false" width="150px"></asp:TextBox><asp:DropDownList autopostback="true" width="50px" runat="server" id="ddl_BBPrice" onselectedindexchanged="ddl_BBPrice_SelectedIndexChanged"></asp:DropDownList>
                            <asp:RequiredFieldValidator runat="server" id="rfvPrice" controltovalidate="tbx_Price" text="* Please enter a a value." errormessage="The price (dollars) is required." display="Dynamic"></asp:RequiredFieldValidator>
                        </td>
                        </ajax:ajaxpanel>
                    </tr>
                    
                    <ajax:ajaxpanel id="ajItemData3" runat="server"> 
                    <tr >
                        <td align="right" valign="top" style="width:120px" ><label style="color: Red">*</label>Creator Name:</td>
                        <td align="left" style="width:350px" valign="top" >
                            <table border="0" cellpadding="0" cellspacing="0" width="350px">
                                <tr>
                                    <td align="left" style="width:350px">
                                        <asp:TextBox runat="server" maxlength="125" id="tbx_CreatorName" enabled="False" width="280px"></asp:TextBox>
                                        <asp:RequiredFieldValidator runat="server" id="rfvCreatorName" controltovalidate="tbx_CreatorName" text="* Please enter a value." errormessage="The item name is required." display="Dynamic"></asp:RequiredFieldValidator>
                                        <asp:TextBox runat="server" maxlength="125" visible="false" id="tbx_CreatorID"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="width:350px">
                                         <b>Search</b>&nbsp;&nbsp;<asp:TextBox runat="server" id="tbx_OwnersSearchFilter"></asp:TextBox>&nbsp;&nbsp;<asp:Button id="btn_UsersSearch" causesvalidation="false" onclick="btn_UsersSearch_Click" runat="server" text="Search"></asp:Button>   
                                        
                                        <asp:GridView runat="server" id="dgd_Users" AllowPaging="True" cellpadding="2" PageSize="10" border="0" BorderColor="Navy" OnPageIndexChanging="dgd_Users_PageIndexChanging" onselectedindexchanged="dgd_Users_SelectedIndexChanged" autogeneratecolumns="False" width="280px">
                                            <RowStyle BackColor="White"></RowStyle>
                                            <HeaderStyle BackColor="Navy" Font-Underline="True" Font-Bold="True" ForeColor="WHITe"></HeaderStyle>
                                            <AlternatingRowStyle BackColor="Gainsboro"></AlternatingRowStyle>
                                            <FooterStyle BackColor="#FFffff"></FooterStyle>
                                            <Columns>
                                                <asp:BoundField HeaderText="ID" visible="true" DataField="user_id">
                                                    <ItemStyle Width="0px"></ItemStyle>
                                                    <HeaderStyle Width="0px"></HeaderStyle>
                                                </asp:BoundField>
                                                <asp:BoundField ReadOnly="True" HeaderText="WOK Item" DataField="username">
                                                    <ItemStyle Font-Italic="false"></ItemStyle>
                                                    <HeaderStyle Font-Bold="True"></HeaderStyle>
                                                </asp:BoundField>
                                                <asp:CommandField CausesValidation="False" ShowCancelButton="False" ShowSelectButton="True"></asp:CommandField>
                                            </Columns>
                                        </asp:GridView>

                                   </td>
                                </tr>
                            </table>
                        </td>
                        <td align="center" valign="top" colspan="2" >
                            <table border="0" cellpadding="0" cellspacing="0" width="480px">
                                <tr>
                                    <td align="center" style="width:240px; text-decoration:underline">Item Sets</td>
                                    <td align="center" style="width:240px; text-decoration:underline">Store Locations</td>
                                </tr>
                                
                                <tr>
                                    <td align="center" style="width:240px">
                                        <div style="height:260px; width:240px; overflow:auto">
                                            <asp:GridView runat="server" id="dgd_ItemSets" AllowPaging="false" cellpadding="2" border="1" BorderColor="Navy" autogeneratecolumns="False" width="220px">
                                                <RowStyle BackColor="White"></RowStyle>
                                                <AlternatingRowStyle BackColor="Gainsboro"></AlternatingRowStyle>
                                                <FooterStyle BackColor="#FFffff"></FooterStyle>
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:CheckBox runat="server" id="cbx_inset" autopostback="true" checked='<%# DataBinder.Eval(Container.DataItem, "cbx_inset")%>' oncheckedchanged="cbx_inset_CheckedChanged"></asp:CheckBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>                                       
                                                    <asp:TemplateField visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" id="lbl_itemSetId" text='<%# DataBinder.Eval(Container.DataItem, "item_set_id")%>' ></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>                                       
                                                    <asp:BoundField ReadOnly="True" HeaderText="Set Name" DataField="set_name"></asp:BoundField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </td>
                                    <td align="center" style="width:240px">
                                        <div style="height:260px; width:240px; overflow:auto">
                                            <asp:GridView runat="server" id="dgd_Stores" AllowPaging="false" cellpadding="2" border="1" BorderColor="Navy" autogeneratecolumns="False" width="220px">
                                                <RowStyle BackColor="White"></RowStyle>
                                                <AlternatingRowStyle BackColor="Gainsboro"></AlternatingRowStyle>
                                                <FooterStyle BackColor="#FFffff"></FooterStyle>
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:CheckBox runat="server" id="cbx_stores" autopostback="true"  checked='<%# DataBinder.Eval(Container.DataItem, "cbx_instore")%>' oncheckedchanged="cbx_stores_CheckedChanged"></asp:CheckBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>                                       
                                                     <asp:TemplateField visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" id="lbl_storeId" Text='<%# DataBinder.Eval(Container.DataItem, "store_id")%>'> </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>                                       
                                                    <asp:BoundField ReadOnly="True" HeaderText="Store Name" DataField="name"></asp:BoundField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </td>
                                </tr>
                                
                            </table>
                        </td>
                    </tr>
                     <tr>
                        <td align="center" colspan="4">
                        </td>
                    </tr>
                    </ajax:ajaxpanel>
                    <tr>
                        <td align="right" colspan="4" style="padding-right:20px; padding-top:30px">
                            <table>
                                <tr>
                                    <td><asp:Button runat="server" id="btn_Cancel" Text="Cancel" causesvalidation="false" onclick="btn_Cancel_Click"></asp:Button></td>
                                    <td style="padding-left:30px"><asp:Button runat="server" id="btn_Save" Text="Submit" onclientclick="specialCharReplace();" onclick="btn_Save_Click"></asp:Button></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </asp:panel>
         </td>
    </tr>
</table>

</asp:panel>
