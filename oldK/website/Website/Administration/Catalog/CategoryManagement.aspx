<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CategoryManagement.aspx.cs" validateRequest="false" Inherits="KlausEnt.KEP.Kaneva.Catalog.CategoryManagement" %>
<%@ Register TagPrefix="Kaneva" TagName="CATNav" Src="CatalogNav.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="AdminMenu" Src="../../usercontrols/AdministrativeMenu.ascx" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>

<script type="text/javascript">

function ClearCategoryRButtons(rdo2check)
{	   
	var el = document.frmMain.elements;
    
	//resets the radio buttons and selects the row clicked on
	for (var i=0; i < el.length; i++)
	{
		if (el[i].type.toLowerCase() == 'radio')
		{	
		    if(el[i].id == rdo2check)
		    {
		        el[i].checked = true;
		    }	    
		    else if ((el[i].id != "rdo_topCategory1") && (el[i].id != "rdo_childCategory1"))
			{
				el[i].checked = false;
			}
		}
	}
}

 function specialCharReplace()
 {
   //replace for item text, display text, and description;
    var categoryName = document.getElementById("tbx_CategoryName");
    categoryName.value = categoryName.value.replace(/amp;/g,"")

    var parentCategoryName = document.getElementById("tbx_ParentCategoryName");
    parentCategoryName.value = parentCategoryName.value.replace(/amp;/g,"")

 }
 

</script>


<Kaneva:AdminMenu ID="adminmenu" runat="server" />
<asp:ValidationSummary ShowMessageBox="False" ShowSummary="True" Class="formError" id="valSum" DisplayMode="BulletList" runat="server" HeaderText="Please correct the following errors:"/>

<table runat="server" id="tblNoADMIN" cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr><td align="left"><Kaneva:CATNav ID="CATNav1" runat="server" width="100%" SubTab="us"/></td></tr>
	<tr><td ALIGN="CENTER">
		<BR><BR><BR>
		<span class="subHead">You must be an Administrator to access this section.</span>
	</td></tr>
</table>
<asp:panel runat="server" id="pnlADMIN">

<!--<ajax:ajaxpanel id="ajCategoryManagement" runat="server">-->

<Kaneva:CATNav runat="server" id="catNavigation" SubTab="us"/>
<br>

 <table id="tbl_CurrentCategories" border="0" cellpadding="0" cellspacing="0" width="1000" style="background-color:#ffffff">
    <tr>
        <td align="center"><span style="height:30px; font-size:28px; font-weight:bold">Category Management</span><br /><asp:Label runat="server" id="ErrorMessages"></asp:Label></td>
    </tr>
   <tr>
         <td style="height:20px"></td>
    </tr>
    <tr>
         <td><asp:Label id="lbl_NoCategories" visible="false" runat="server"><span style="color:Navy; size:24pt; font-weight:bold">No Catalog Items Were Found.</span> </asp:Label></td>
    </tr>
    <tr>
         <td>
            
            <table border="0" width="98%" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="200px" align="left" valign="bottom"><asp:LinkButton id="lbn_NewCategory" runat="server" causesvalidation="false" onclick="lbn_NewCategory_Click">Add New Category</asp:LinkButton></td>
                    <td align="right" style="padding: 6px 0px 6px 6px; font-size:10pt"><b>Search</b> &nbsp;<asp:TextBox runat="server" id="tbx_CategorySearchFilter"></asp:TextBox>&nbsp;<asp:Button id="btn_CategorySearch" causesvalidation="false" onclick="btn_CategorySearch_Click" runat="server" text="Search"></asp:Button></td>
                </tr>
            </table>
            
            <ajax:ajaxpanel id="ajdgCategories" runat="server">
            <asp:gridview id="dg_Categories" runat="server" onsorting="dg_Categories_Sorting"  onRowDataBound="dg_Categories_RowDataBound" OnRowDeleting="dg_Categories_RowDeleting" OnRowCreated="dg_Categories_OnRowCreated" autogeneratecolumns="False" width="980px" OnPageIndexChanging="dg_Categories_PageIndexChanging" OnRowEditing="dg_Categories_RowEditing" AllowSorting="True" border="0" cellpadding="2" PageSize="5" AllowPaging="True">
                <RowStyle BackColor="White"></RowStyle>
                <HeaderStyle BackColor="LightGray" Font-Underline="True" Font-Bold="True" ForeColor="#000000" HorizontalAlign="Left"></HeaderStyle>
                <AlternatingRowStyle BackColor="Gainsboro"></AlternatingRowStyle>
                <FooterStyle BackColor="#FFffff"></FooterStyle>
                <Columns>
                    <asp:BoundField HeaderText="Category Id" DataField="category_id" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
                    <asp:BoundField HeaderText="Category" DataField="category" SortExpression="category" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
                    <asp:BoundField HeaderText="Description" DataField="description" SortExpression="description" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>                    
                    <asp:BoundField HeaderText="Parent Category" DataField="parent_category_id" SortExpression="parent_category_id" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
                     <asp:TemplateField>
                        <ItemTemplate>
                            <asp:Label Runat="server" causesvalidation="false" ><%# LevelIndicator(DataBinder.Eval(Container.DataItem, "parent_category_id").ToString()) %></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                   <asp:CommandField CausesValidation="False" ShowEditButton="True"></asp:CommandField>        
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton id="lbn_confirmCategoryDelete" Runat="server" causesvalidation="false" OnClientClick="return confirm('Are you sure you want to delete this category?');" CommandName="Delete">Delete</asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:gridview>
            
            
	        <div id="divCascadingDeleteConfirm" runat="server" style=" position:absolute; top:200px; left:200px; border: 4 4 4 4; border:solid 1px #000000; background-color:White; width:300px">
	            <table style="text-align:center" border="1" rules="cols" cellpadding="4" cellspacing="0">
	                <tr>
	                    <td colspan="2" style="font-size:12pt; font-weight:bold">You are about to delete a top level item!<hr /></td>
	                </tr>
	                <tr>
	                    <td valign="top">You may delete the top level and all the levels below it</td>
	                    <td  valign="top">Or just the top level</td>
	                </tr>
	                <tr>
	                    <td>
	                        <asp:button id="btnCascadingDelete" onclick="btnCascadingDelete_Click" runat="server" causesvalidation="false" text="Delete All" ></asp:button>
	                    </td>
	                    <td>
	                        <asp:button id="btnNormalDelete"  onclick="btnNormalDelete_Click" runat="server" causesvalidation="false" text="Top Level Only" ></asp:button>
	                    </td>
	                </tr>
	            </table>
	        </div>
            
            </ajax:ajaxpanel>
         </td>
    </tr>
    <tr>
         <td style="height:20px"></td>
    </tr>
    <tr>
         <td>
            <asp:panel id="pnl_CategoryDetails" runat="server" >
                <hr />
                <table style="font-weight:bold" id="tbl_OfferDetails" border="1" frame="below" cellpadding="5px" cellspacing="0px" width="100%">
                    <tr>
                         <td align="right" style="width:150px"><label style="color: Red">*</label>Category Level:</td>
                         <td align="left" style="width:300px">
                        
                            <ajax:ajaxpanel id="ajrbCategoryLevel" runat="server">
                            <asp:RadioButton id="rdo_topCategory1" autopostback="true" groupname="toplevel" text="Top Level" oncheckedchanged="rdb_CategoryLevel_CheckChanged" runat="server" />
                            <asp:RadioButton id="rdo_childCategory1" autopostback="true" groupname="toplevel" text="Child" oncheckedchanged="rdb_CategoryLevel_CheckChanged" runat="server" />
                            </ajax:ajaxpanel>
                            
                        </td>
                    </tr>
                    <tr >
                        <td align="right" style="width:150px"><label style="color: Red">*</label>Category Name:</td>
                        <td align="left" style="width:300px">
                            <asp:TextBox runat="server" maxlength="60" id="tbx_CategoryName" width="280px"></asp:TextBox>
                            <asp:RequiredFieldValidator runat="server" id="rfvCategoryName" controltovalidate="tbx_CategoryName" text="* Please enter a value." errormessage="The category name is required." display="Dynamic"></asp:RequiredFieldValidator>
                            <asp:TextBox runat="server" maxlength="125" visible="false" id="tbx_CategoryNameID"></asp:TextBox>
                        </td>
                    </tr>
                    <tr >
                        <td align="right" style="width:150px"><label style="color: Red">*</label>Category Description:</td>
                        <td align="left" style="width:300px">
                            <asp:TextBox runat="server" maxlength="60" id="tbx_catergoryDescription" width="280px"></asp:TextBox>
                            <asp:RequiredFieldValidator runat="server" id="rfvCategoryDescription" controltovalidate="tbx_catergoryDescription" text="* Please enter a value." errormessage="The category description is required." display="Dynamic"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                     <tr id="tblrow_childCategory" runat="server">
                        <td align="right" style="width:150px"><label style="color: Red">*</label>Parent Category:</td>
                        <td align="left" style="width:300px">
                            <asp:TextBox runat="server" enabled="false" maxlength="125" id="tbx_ParentCategoryName" width="280px"></asp:TextBox>
                            <asp:RequiredFieldValidator runat="server" id="rfvParentCategoryName" controltovalidate="tbx_ParentCategoryName" text="* Please enter a value." errormessage="The item name is required." display="Dynamic"></asp:RequiredFieldValidator>
                            <asp:TextBox runat="server" maxlength="125" visible="false" id="tbx_ParentCategoryID"></asp:TextBox>
                        </td>
                    </tr>
                    <tr id="tblrow_parentSearch" runat="server">
                       <td align="right" style="width:150px"></td>
                       <td>
                             <b>Search</b>&nbsp;&nbsp;<asp:TextBox runat="server" id="tbx_ParentSearchFilter"></asp:TextBox>&nbsp;&nbsp;<asp:Button id="btn_ParentSearch" causesvalidation="false" onclick="btn_ParentSearch_Click" runat="server" text="Search"></asp:Button>   
                            
                           <div style="height:260px; width:300px; overflow:auto">
                           
                           <ajax:ajaxpanel id="ajdgParentCategories" runat="server">
                           <asp:GridView runat="server" id="dgd_Parents" onRowDataBound="dgd_Parents_RowDataBound" OnRowCreated="dgd_Parents_OnRowCreated" AllowPaging="false" cellpadding="2" PageSize="10" border="0" BorderColor="Navy" autogeneratecolumns="False" width="280px">
                                <RowStyle BackColor="White"></RowStyle>
                                <HeaderStyle BackColor="Navy" Font-Underline="True" Font-Bold="True" ForeColor="WHITe"></HeaderStyle>
                                <AlternatingRowStyle BackColor="Gainsboro"></AlternatingRowStyle>
                                <FooterStyle BackColor="#FFffff"></FooterStyle>
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:RadioButton runat="server" id="rdb_categoryParent" groupname="parents" autopostback="true" oncheckedchanged="categoryParent_CheckedChanged" checked='<%# DataBinder.Eval(Container.DataItem, "cbx_parents")%>' ></asp:RadioButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>                                       
                                    <asp:TemplateField Visible="False">
                                        <ItemTemplate >
                                            <asp:Label runat="server" id="lbl_categoryId" text='<%# DataBinder.Eval(Container.DataItem, "category_id")%>' ></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>                                       
                                    <asp:BoundField ReadOnly="True" HeaderText="Category" DataField="category"></asp:BoundField>
                                </Columns>
                            </asp:GridView>
                            </ajax:ajaxpanel>
                            
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" valign="top" style="width:150px">Category Items:</td>
                        
                        <td align="left" style="padding-right:20px">
                           <div style="height:260px; width:700px; overflow:auto">
                           
                                <ajax:ajaxpanel id="ajdgCategoryItems" runat="server">
                                <asp:GridView runat="server" id="dgd_ItemsNCategories" OnPageIndexChanging="dgd_ItemsNCategories_PageIndexChanging" onRowDataBound="dgd_ItemsNCategories_RowDataBound" cellpadding="2" border="1" BorderColor="Navy" autogeneratecolumns="False" width="680px" PageSize="10" AllowPaging="True">
                                    <RowStyle BackColor="White" ></RowStyle>
                                    <AlternatingRowStyle BackColor="Gainsboro"></AlternatingRowStyle>
                                    <FooterStyle BackColor="#FFffff"></FooterStyle>
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:CheckBox runat="server" id="cbx_items" autopostback="true"  checked='<%# DataBinder.Eval(Container.DataItem, "cbx_inCategory")%>' oncheckedchanged="cbx_items_CheckedChanged"></asp:CheckBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                       
                                        <asp:TemplateField Visible="False">
                                            <ItemTemplate >
                                                <asp:Label runat="server" id="lbl_id" text='<%# DataBinder.Eval(Container.DataItem, "global_id")%>' ></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                       
                                        <asp:BoundField ReadOnly="True" HeaderText="Item Name" DataField="name"></asp:BoundField>
                                        <asp:BoundField ReadOnly="True" HeaderText="Display Name" DataField="display_name"></asp:BoundField>
                                        <asp:BoundField ReadOnly="True" HeaderText="Description" DataField="description"></asp:BoundField>
                                    </Columns>
                                </asp:GridView>
                                </ajax:ajaxpanel>
                                
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="2" style="padding-right:20px; padding-top:30px">
                        
                            <ajax:ajaxpanel id="ajbtnFormControl" runat="server">
                            <table>
                                <tr>
                                    <td><asp:Button runat="server" id="btn_Cancel" Text="Cancel" causesvalidation="false" onclick="btn_Cancel_Click"></asp:Button></td>
                                    <td style="padding-left:30px"><asp:Button runat="server" id="btn_Save" Text="Submit" onclientclick="specialCharReplace();" onclick="btn_Save_Click"></asp:Button></td>
                                </tr>
                            </table>
                            </ajax:ajaxpanel>
                            
                        </td>
                    </tr>
                </table>
            </asp:panel>
         </td>
    </tr>
</table>
<!--</ajax:ajaxpanel>-->
</asp:panel> 
