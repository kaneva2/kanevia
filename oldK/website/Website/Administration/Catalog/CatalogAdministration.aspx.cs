///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Collections.Specialized;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva;
using log4net;
using System.Drawing;
using MagicAjax;
using System.IO;

using System.Collections.Generic;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.Catalog
{
    public partial class CatalogAdministration : NoBorderPage
    {
        #region Declarations

        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private bool editItemSubmission = false;

        #endregion

        protected CatalogAdministration()
        {
            Title = "Catalog Item Management";
        }

        #region PageLoad

        protected void Page_Load(object sender, EventArgs e)
        {
            pnlADMIN.Visible = (IsAdministrator());
            tblNoADMIN.Visible = (!IsAdministrator());
            this.ErrorMessages.Text = "";

            if (!IsPostBack)
            {
                //check for  submission from another page
                GetRequestParams();

                //bind the CatalogItems
                BindCatalogItemData(currentPage);

                //bind pulldowns
                PopulateCurrencyTypeList();
                PopulateAvailabiltyList();
                PopulateExclusivityList();
                PopulateBuyBackPercentages();

                pnl_CatalogItemsDetails.Visible = false;

                if (editItemSubmission)
                {
                    //get the catalog item
                    PopulateWOKItemEdit(dg_CatalogItems.Rows[0]);
                }
            }

        }

        #endregion

        #region Primary Functions

        /// <summary>
        /// BindStoreList
        /// </summary>
        private void BindPassList(int itemId)
        {
            DataTable passTypes = null;
            DataTable itemPassGroups = null;

            //get all the current WOK pass types
            passTypes = WOKStoreUtility.GetAccessPassTypes();

            //create the check field column for the get all pass types data result
            DataColumn checkColumn = new DataColumn();
            checkColumn.DataType = System.Type.GetType("System.Boolean");
            checkColumn.AllowDBNull = false;
            checkColumn.ColumnName = "cbx_inPassGroup";
            checkColumn.DefaultValue = false;
            passTypes.Columns.Add(checkColumn);

            //get all the pass groups that this item is in
            if (itemId > 0)
            {
                itemPassGroups = WOKStoreUtility.GetItemPassGroups(itemId);
            }

            //mark the pass groups the item is in if any
            if ((itemPassGroups != null) && (itemPassGroups.Rows.Count > 0))
            {
                foreach (DataRow dr in itemPassGroups.Rows)
                {
                    string passGroupId = dr["pass_group_id"].ToString();
                    DataRow[] passGroupItems = passTypes.Select("pass_group_id = " + passGroupId);
                    if (passGroupItems.Length > 0)
                    {
                        DataRow passGroupItem = passGroupItems[0];
                        passGroupItem["cbx_inPassGroup"] = true;
                        passGroupItem.AcceptChanges();
                        passTypes.AcceptChanges();
                    }

                }
            }

            dgd_PassGroups.DataSource = passTypes;
            dgd_PassGroups.DataBind();
        }

        /// <summary>
        /// BindStoreList
        /// </summary>
        private void BindStoreList(int itemId)
        {
            DataTable itemsInStore = null;
            DataTable storesDT = null;

            //get all the current WOK stores
            storesDT = WOKStoreUtility.GetAllStores();

            //create the check field column for the get all stores data result
            DataColumn checkColumn = new DataColumn();
            checkColumn.DataType = System.Type.GetType("System.Boolean");
            checkColumn.AllowDBNull = false;
            checkColumn.ColumnName = "cbx_instore";
            checkColumn.DefaultValue = false;
            storesDT.Columns.Add(checkColumn);

            //get all the stores that this item is in
            if (itemId > 0)
            {
                itemsInStore = WOKStoreUtility.GetItemStores(itemId);
            }

            //mark the stores the the item is in if any
            if ((itemsInStore != null) && (itemsInStore.Rows.Count > 0))
            {
                foreach (DataRow dr in itemsInStore.Rows)
                {
                    string storeId = dr["store_id"].ToString();
                    DataRow[] storeItems = storesDT.Select("store_id = " + storeId);
                    if (storeItems.Length > 0)
                    {
                        DataRow storeItem = storeItems[0];
                        storeItem["cbx_instore"] = true;
                        storeItem.AcceptChanges();
                        storesDT.AcceptChanges();
                    }

                }
            }

            dgd_Stores.DataSource = storesDT;
            dgd_Stores.DataBind();
        }

        /// <summary>
        /// PopulateWOKItemEdit
        /// </summary>
        private void PopulateWOKItemEdit(GridViewRow gvr)
        {
            ShoppingFacade shoppingFacade = new ShoppingFacade();
            int globalId = Convert.ToInt32(gvr.Cells[0].Text);

            WOKItem item = shoppingFacade.GetItem(globalId);

            //clear the fields
            ClearCatalogItemData();

            //make the CatalogItems details panel visible
            pnl_CatalogItemsDetails.Visible = true;

            //load the gift sets and stores
            BindStoreList(globalId);
            BindItemSets(globalId);

            // Show the categories
            BindItemCategories(globalId);

            //show the Pass groups the items is in
            BindPassList(globalId);

            // Set up the add categories
            SetAddCategoryToTopLevel();

            try
            {

                //populate fields
                //replace is a work around for unexplained &nbsp; showing up figure it out later
                tbx_ItemID.Text = gvr.Cells[0].Text.Replace("&nbsp;", "");
                tbx_ItemName.Text = Server.HtmlDecode(gvr.Cells[2].Text.Replace("&nbsp;", ""));
                tbx_DisplayName.Text = Server.HtmlDecode(gvr.Cells[3].Text.Replace("&nbsp;", ""));
                tbx_Description.Text = Server.HtmlDecode(gvr.Cells[4].Text.Replace("&nbsp;", ""));

                //parse dates
                DateTime itemAddDate = Convert.ToDateTime(gvr.Cells[5].Text.Replace("&nbsp;", ""));
                txtItemAddedDate.Text = itemAddDate.ToShortDateString();

                tbx_InitialAmount.Text = gvr.Cells[6].Text.Replace("&nbsp;", "");
                tbx_SellingPrice.Text = gvr.Cells[7].Text.Replace("&nbsp;", "");
                tbx_DesignerPrice.Text = item.DesignerPrice.ToString();
                tbx_Price.Text = gvr.Cells[8].Text.Replace("&nbsp;", "");
                tbx_CreatorID.Text = gvr.Cells[9].Text.Replace("&nbsp;", "");

                //set the buyback pulldown
                int percentage = Convert.ToInt32(Math.Round((Convert.ToDecimal(tbx_Price.Text) / Convert.ToDecimal((tbx_SellingPrice.Text.Equals("0") ? "1" : tbx_SellingPrice.Text))) * 100));
                int remainder = percentage%5;
                if (remainder < 3)
                {
                    percentage -= remainder;
                }
                else
                {
                    percentage += (5 - remainder);
                }
                ddl_BBPrice.SelectedValue = (percentage.Equals(0) ? "50": percentage.ToString());

                //query to get the username for display
                try
                {
                    tbx_CreatorName.Text = GetUserName(Convert.ToInt32(gvr.Cells[9].Text));
                }
                catch (FormatException fe)
                {
                    m_logger.Error("Catalog Administration ", fe);
                }

                string currencyType = gvr.Cells[10].Text.Replace("&nbsp;", "");
                string availabiltyType = gvr.Cells[11].Text.Replace("&nbsp;", "");
                string exclusivityType = gvr.Cells[12].Text.Replace("&nbsp;", "");

                SetDropDownIndex (ddl_CurrencyType, item.InventoryType.ToString ());
                ddl_ProductAvailability.SelectedValue = (availabiltyType == "0" ? DEFAULT_AVAILIBILITY : availabiltyType);
                ddl_ProductExclusivity.SelectedValue = (exclusivityType == "0" ? DEFAULT_EXCLUSIVITY : exclusivityType);

                inp_itemImage.Value = gvr.Cells[13].Text.Replace("&nbsp;", "");

                //stores imagepath for change check
                ItemImage = inp_itemImage.Value;

                itemImage.ImageUrl = StoreUtility.GetItemImageURL(item.ThumbnailMediumPath, "me");
                SetDropDownIndex(dgd_drpActive, gvr.Cells[14].Text.Replace("&nbsp;", ""));

                //show display information as summary for user convenience
                lbl_DetailsTitle.Text = tbx_ItemName.Text.Replace("&nbsp;", "").Replace("&quot;", "") +
                    "(" + tbx_ItemID.Text + ") by " + tbx_CreatorName.Text + "(" + tbx_CreatorID.Text + ")";
            }
            catch (Exception ex)
            {
                ErrorMessages.ForeColor = Color.DarkRed;
                ErrorMessages.Text = "Error displaying promotion detials: " + ex.Message;
                return;
            }

        }

        /// <summary>
        /// Gets the user Name associated with the user Id
        /// </summary>
        protected string GetUserName(int userID)
        {
            string userName = "N/A";

            try
            {
                if (userID == 0)
                {
                    userName = "Kaneva";
                }
                else if (userID > 0)
                {
                    userName = UsersUtility.GetUserName(userID);
                }
            }
            catch (Exception ex)
            {
                m_logger.Error("Catalog Administration: error trying to get username from user id ", ex);
            }
            return userName;
        }

        
        /// <summary>
        /// BindItemSets
        /// </summary>
        private void BindItemSets(int itemId)
        {
            DataTable itemsSets = null;
            DataTable itemsSetDT = null;

            //get all the current WOK stores
            itemsSetDT = WOKStoreUtility.GetAllItemSets();

            //create the check field column for the get all stores data result
            DataColumn checkColumn = new DataColumn();
            checkColumn.DataType = System.Type.GetType("System.Boolean");
            checkColumn.AllowDBNull = false;
            checkColumn.ColumnName = "cbx_inset";
            checkColumn.DefaultValue = false;
            itemsSetDT.Columns.Add(checkColumn);

            //get all the stores that this item is in
            if (itemId > 0)
            {
                itemsSets = WOKStoreUtility.GetItemsSets(itemId);
            }

            //mark the sets the the item is in if any
            if ((itemsSets != null) && (itemsSets.Rows.Count > 0))
            {

                foreach (DataRow dr in itemsSets.Rows)
                {
                    string itemSetId = dr["item_set_id"].ToString();
                    DataRow[] items_Sets = itemsSetDT.Select("item_set_id = " + itemSetId);
                    if (items_Sets.Length > 0)
                    {
                        DataRow item_set = items_Sets[0];
                        item_set["cbx_inset"] = true;
                        item_set.AcceptChanges();
                        itemsSetDT.AcceptChanges();
                    }

                }
            }

            dgd_ItemSets.DataSource = itemsSetDT;
            dgd_ItemSets.DataBind();
        }

        /// <summary>
        /// BindCatalogItemsData
        /// </summary>
        private void BindCatalogItemData(int currentPage)
        {
            //query the database for existing CatalogItems
            DataTable dt = WOKStoreUtility.GetWOKItemsCatalog(WOKSearchFilter);

            // Set the sortable columns
            string orderby = CurrentSort + " " + CurrentSortOrder;

            if ((dt != null) && (dt.Rows.Count > 0))
            {
                //set the current page
                this.dg_CatalogItems.PageIndex = currentPage;

                //set the sort order
                dt.DefaultView.Sort = orderby;

                lbl_NoCatalogItems.Visible = false;

                dg_CatalogItems.DataSource = dt;
                dg_CatalogItems.DataBind();
            }
            else
            {
                lbl_NoCatalogItems.Visible = true;
            }

        }

        private void BindUserSearch()
        {
            dgd_Users.PageSize = PageSize();
            dgd_Users.PageIndex = WOKSearchCurrentPage;
            DataTable dt = UsersUtility.GetUsersList(-1, UserSearchFilter);
            dgd_Users.DataSource = dt;
            dgd_Users.DataBind();
        }


        #endregion

        #region Properties


        public int PageSize()
        {
            return 10;
        }

        /// <summary>
        /// DEFAULT_SORT
        /// </summary>
        /// <returns></returns>
        protected override string DEFAULT_SORT
        {
            get
            {
                return "name";
            }
        }

        /// <summary>
        /// DEFAULT_SORT_ORDER
        /// </summary>
        /// <returns></returns>
        protected override string DEFAULT_SORT_ORDER
        {
            get
            {
                return "ASC";
            }
        }

        private int WOKSearchCurrentPage
        {
            get
            {
                if (ViewState["_WOKcurrentPage"] == null)
                {
                    ViewState["_WOKcurrentPage"] = 0;
                }
                return (int)ViewState["_WOKcurrentPage"];
            }
            set
            {
                ViewState["_WOKcurrentPage"] = value;
            }
        }

        private void resetViewState()
        {
            WOKSearchFilter = null;
            currentPage = 0;
            itemSetChanges = null;
            storeChanges = null;
            passGroupChanges = null;
        }

        private string WOKSearchFilter
        {
            get
            {
                if (ViewState["_WOKSearchFilter"] == null)
                {
                    ViewState["_WOKSearchFilter"] = "";
                }
                return ViewState["_WOKSearchFilter"].ToString();
            }
            set
            {
                ViewState["_WOKSearchFilter"] = value;
            }
        }

        private int currentPage
        {
            get
            {
                if (ViewState["_currentPage"] == null)
                {
                    ViewState["_currentPage"] = 0;
                }
                return (int)ViewState["_currentPage"];
            }
            set
            {
                ViewState["_currentPage"] = value;
            }
        }

        private int UserSearchCurrentPage
        {
            get
            {
                if (ViewState["_UsercurrentPage"] == null)
                {
                    ViewState["_UsercurrentPage"] = 0;
                }
                return (int)ViewState["_UsercurrentPage"];
            }
            set
            {
                ViewState["_UsercurrentPage"] = value;
            }
        }

        private string UserSearchFilter
        {
            get
            {
                if (ViewState["_UserSearchFilter"] == null)
                {
                    ViewState["_UserSearchFilter"] = "";
                }
                return ViewState["_UserSearchFilter"].ToString();
            }
            set
            {
                ViewState["_UserSearchFilter"] = value;
            }
        }

        private string ItemImage
        {
            get
            {
                if (ViewState["_ItemImage"] == null)
                {
                    ViewState["_ItemImage"] = "";
                }
                return ViewState["_ItemImage"].ToString();
            }
            set
            {
                ViewState["_ItemImage"] = value;
            }
        }

        //allows user to switch the ordering between DESC and ASC
        //based on column clicked
        private void SortSwitch(string newSort)
        {
            if (CurrentSort == newSort)
            {
                if (CurrentSortOrder == "DESC")
                {
                    CurrentSortOrder = "ASC";
                }
                else
                {
                    CurrentSortOrder = "DESC";
                }
            }
            else
            {
                CurrentSort = newSort;
                CurrentSortOrder = "DESC";
            }
        }

        private NameValueCollection passGroupChanges
        {
            get
            {
                if (ViewState["_passGroupChanges"] == null)
                {
                    ViewState["_passGroupChanges"] = new NameValueCollection();
                }
                return (NameValueCollection)ViewState["_passGroupChanges"];
            }
            set
            {
                ViewState["_passGroupChanges"] = value;
            }
        }

        private NameValueCollection itemSetChanges
        {
            get
            {
                if (ViewState["_itemSetChanges"] == null)
                {
                    ViewState["_itemSetChanges"] = new NameValueCollection();
                }
                return (NameValueCollection)ViewState["_itemSetChanges"];
            }
            set
            {
                ViewState["_itemSetChanges"] = value;
            }
        }

        private NameValueCollection storeChanges
        {
            get
            {
                if (ViewState["_storeChanges"] == null)
                {
                    ViewState["_storeChanges"] = new NameValueCollection();
                }
                return (NameValueCollection)ViewState["_storeChanges"];
            }
            set
            {
                ViewState["_storeChanges"] = value;
            }
        }

        /// <summary>
        /// DEFAULT_AVAILIBILITY
        /// </summary>
        /// <returns></returns>
        protected string DEFAULT_AVAILIBILITY
        {
            get
            {
                return "2";
            }
        }

        /// <summary>
        /// DEFAULT_CURRENCYTYPE
        /// </summary>
        /// <returns></returns>
        protected string DEFAULT_CURRENCYTYPE
        {
            get
            {
                return "256";
            }
        }

        /// <summary>
        /// DEFAULT_ACCESSTYPE
        /// has been altered to set access pass type to none
        /// </summary>
        /// <returns></returns>
        protected string DEFAULT_ACCESSTYPE
        {
            get
            {
                return "0";
            }
        }

        /// <summary>
        /// DEFAULT_EXCLUSIVITY
        /// </summary>
        /// <returns></returns>
        protected string DEFAULT_EXCLUSIVITY
        {
            get
            {
                return "3";
            }
        }

        #endregion

        #region Helper Functions

        private void NoteChanges(NameValueCollection collection, string id, string value)
        {
            if (collection[id] != null)
            {
                collection.Remove(id);
            }
            else
            {
                collection.Add(id, value);
            }
        }

        private void CalculateSellBackPrice()
        {
            decimal sellPrice = 0.0m;
            try
            {
                sellPrice = Convert.ToDecimal(tbx_SellingPrice.Text);
                int percent = Convert.ToInt32(ddl_BBPrice.SelectedValue);
                tbx_Price.Text = Convert.ToString(Math.Round(sellPrice * (percent / 100.0m)));
            }
            catch (FormatException)
            {
            }
        }

        private void GetRequestParams()
        {
            try
            {
                WOKSearchFilter = "global_id = " + Request["itemId"].ToString();
                editItemSubmission = true;
            }
            catch (Exception)
            {
            }
        }

        private void PopulateAvailabiltyList()
        {
            ddl_ProductAvailability.DataSource = WOKStoreUtility.GetProductAvailability();
            ddl_ProductAvailability.DataTextField = "product_availability";
            ddl_ProductAvailability.DataValueField = "product_availability_id";
            ddl_ProductAvailability.DataBind();
        }

        private void PopulateExclusivityList()
        {
            ddl_ProductExclusivity.DataSource = WOKStoreUtility.GetProductExclusivity();
            ddl_ProductExclusivity.DataTextField = "product_exclusivity";
            ddl_ProductExclusivity.DataValueField = "product_exclusivity_id";
            ddl_ProductExclusivity.DataBind();
        }

        private void PopulateBuyBackPercentages()
        {
            for (int i = 100; i > 0; i = i - 5)
            {
                ddl_BBPrice.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }

        }

        /// <summary>
        /// Clears out all Promotion fields 
        /// </summary>
        private void PopulateCurrencyTypeList()
        {
            //ddl_CurrencyType.DataSource = WOKStoreUtility.GetCurrencyTypes();
            //ddl_CurrencyType.DataTextField = "currency_type";
            //ddl_CurrencyType.DataValueField = "currency_type_id";
            //ddl_CurrencyType.DataBind();

            // Change this per World Item Refresh PRD to Inventory Types
            ddl_CurrencyType.Items.Add(new ListItem ("Cant Be Bought", "0"));
            ddl_CurrencyType.Items.Add(new ListItem("Credits Only", "256"));
            ddl_CurrencyType.Items.Add(new ListItem("Rewards Only", "512"));
            ddl_CurrencyType.Items.Add(new ListItem("Both Credits and Rewards", "768"));
        }

        /// <summary>
        /// Clears out all Promotion fields 
        /// </summary>
        private void ClearCatalogItemData()
        {
            tbx_ItemName.Text = "";
            tbx_ItemID.Text = "";
            tbx_DisplayName.Text = "";
            tbx_Description.Text = "";
            tbx_InitialAmount.Text = "0";
            txtItemAddedDate.Text = DateTime.Now.Date.ToString("MM-dd-yyyy");
            tbx_SellingPrice.Text = "";
            tbx_DesignerPrice.Text = "";
            tbx_Price.Text = "";
            tbx_CreatorName.Text = "";
            tbx_CreatorID.Text = "";
            inp_itemImage.Value = "";
            itemImage.ImageUrl = "";
            ddl_CurrencyType.SelectedValue = DEFAULT_CURRENCYTYPE;
            ddl_ProductAvailability.SelectedValue = DEFAULT_AVAILIBILITY;
            ddl_ProductExclusivity.SelectedValue = DEFAULT_EXCLUSIVITY;
            ddl_BBPrice.SelectedValue = "50";
            //cbx_itemActive.Checked = true;
            SetDropDownIndex(dgd_drpActive, "1");
            dgd_Stores.DataSource = null;
            dgd_Stores.DataBind();
            dgd_ItemSets.DataSource = null;
            dgd_ItemSets.DataBind();
            dgd_Users.DataSource = null;
            dgd_Users.DataBind();
            dgd_PassGroups.DataSource = null;
            dgd_PassGroups.DataBind();

        }

        private void USERSearchClear()
        {
            tbx_OwnersSearchFilter.Text = "";
            dgd_Users.DataSource = null;
            dgd_Users.DataBind();
            UserSearchFilter = null;
            UserSearchCurrentPage = 0;
        }

        private void UploadWOKItemImage(int userId, int itemId)
        {
            try
            {
                //ImageHelper.UploadImageFromUser(userId, itemId, browseTHUMB, KanevaGlobals.WOKImagesServerPath,ImageHelper.WOK_IMAGE);

                // Generate the thumbnails
                HttpPostedFile fThumbnail = browseTHUMB.PostedFile;

                string thumbnailFilename = "";
                string newPath = Path.Combine(Path.Combine(KanevaGlobals.TexturePath, KanevaWebGlobals.CurrentUser.UserId.ToString()), itemId.ToString());

                if (fThumbnail != null && fThumbnail.ContentLength > 0)
                {
                    ImageHelper.UploadImageFromUser(ref thumbnailFilename, fThumbnail, newPath, KanevaGlobals.MaxUploadedImageSize);
                    string imagePath = fThumbnail.FileName.Remove(0, fThumbnail.FileName.LastIndexOf("\\") + 1);
                    string origImagePath = newPath + Path.DirectorySeparatorChar + thumbnailFilename;

                    ImageHelper.GenerateWOKItemThumbs(thumbnailFilename, origImagePath, KanevaGlobals.TexturePath, KanevaWebGlobals.CurrentUser.UserId, itemId);
                }

            }
            catch (Exception ex)
            {
                ShowErrorOnStartup(ex.Message, false);
            }
        }

        #endregion

        #region Event Handlers


        protected void btn_Cancel_Click(object sender, System.EventArgs e)
        {
            //clear the fields
            ClearCatalogItemData();

            //make the details panel visible
            pnl_CatalogItemsDetails.Visible = false;

        }

        protected void btn_UsersSearch_Click(object sender, EventArgs e)
        {
            string usersearchvalue = tbx_OwnersSearchFilter.Text.Trim();

            if ((usersearchvalue != "") && (KanevaGlobals.IsNumeric(usersearchvalue)))
            {
                UserSearchFilter = "user_id = " + tbx_OwnersSearchFilter.Text;
            }
            else
            {
                if (usersearchvalue == "")
                {
                    UserSearchFilter = "";
                }
                else
                {
                    UserSearchFilter = "username LIKE '%" + usersearchvalue + "%'";
                }
            }
            BindUserSearch();
        }

        protected void dgd_Users_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            UserSearchCurrentPage = e.NewPageIndex;
            BindUserSearch();
        }

        protected void dgd_Users_SelectedIndexChanged(object sender, EventArgs e)
        {
            tbx_CreatorID.Text = dgd_Users.SelectedRow.Cells[0].Text;
            tbx_CreatorName.Text = dgd_Users.SelectedRow.Cells[1].Text;
            //USERSearchClear();
        }

        public void btn_WOKSearch_Click(object sender, EventArgs e)
        {
            string wokSearchvalue = tbx_WOKSearchFilter.Text.Trim();

            if ((wokSearchvalue != "") && (KanevaGlobals.IsNumeric(wokSearchvalue)))
            {
                WOKSearchFilter = "global_id = " + tbx_WOKSearchFilter.Text + " OR item_creator_id = " + tbx_WOKSearchFilter.Text;
            }
            else
            {
                if (wokSearchvalue == "")
                {
                    WOKSearchFilter = "";
                }
                else
                {
                    WOKSearchFilter = "name LIKE '%" + wokSearchvalue + "%'";
                }
            }
            BindCatalogItemData(currentPage);
        }

        protected void dg_CatalogItems_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            //logic check to work around initial row creation wheere there is only one column
            if (e.Row.Cells.Count >= 4)
            {
                e.Row.Cells[2].Text = Server.HtmlDecode(e.Row.Cells[2].Text);
                e.Row.Cells[3].Text = Server.HtmlDecode(e.Row.Cells[3].Text);
                e.Row.Cells[4].Text = Server.HtmlDecode(e.Row.Cells[4].Text);
            }

        }

        protected void dg_CatalogItems_RowEditing(object source, System.Web.UI.WebControls.GridViewEditEventArgs e)
        {
            //passin the selected row and populate the fields
            PopulateWOKItemEdit(dg_CatalogItems.Rows[e.NewEditIndex]);
        }

        protected void dg_CatalogItems_PageIndexChanging(object source, System.Web.UI.WebControls.GridViewPageEventArgs e)
        {
            currentPage = e.NewPageIndex;
            BindCatalogItemData(currentPage);
        }

        protected void dg_CatalogItems_Sorting(Object sender, GridViewSortEventArgs e)
        {
            SortSwitch(e.SortExpression); //sets the sort expression
            BindCatalogItemData(currentPage);
        }

        protected void dg_CatalogItemsmyGrid_OnRowCreated(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            //logic check to work around initial row creation wheere there is only one column
            if (e.Row.Cells.Count > 10)
            {
                e.Row.Cells[0].Visible = true; //global id
                e.Row.Cells[2].Visible = false; //Internal Name
                e.Row.Cells[3].Visible = true; //Display Name
                e.Row.Cells[4].Visible = false; //Item Description
                e.Row.Cells[5].Visible = true; //Date Added
                e.Row.Cells[6].Visible = false; // Initial Quantity
                e.Row.Cells[7].Visible = true; //Selling Price
                e.Row.Cells[8].Visible = true; //Buy Back Price
                e.Row.Cells[9].Visible = false; //Creator
                e.Row.Cells[10].Visible = false; //Currency Type
                e.Row.Cells[11].Visible = false; //Availability
                e.Row.Cells[12].Visible = false; //Exclusivity
                e.Row.Cells[13].Visible = false; //Image Path
                e.Row.Cells[14].Visible = false; //Active
            }

        }


        protected void cbx_inPassGroup_CheckedChanged(object sender, EventArgs e)
        {
            GridViewRow ritem = (GridViewRow)((Control)sender).NamingContainer;
            NoteChanges(passGroupChanges, ((Label)ritem.FindControl("lbl_PassGroupId")).Text, ((CheckBox)ritem.FindControl("cbx_inPassGroup")).Checked.ToString());
        }

        protected void cbx_inset_CheckedChanged(object sender, EventArgs e)
        {
            GridViewRow ritem = (GridViewRow)((Control)sender).NamingContainer;
            NoteChanges(itemSetChanges, ((Label)ritem.FindControl("lbl_itemSetId")).Text, ((CheckBox)ritem.FindControl("cbx_inset")).Checked.ToString());
        }

        protected void tbx_SellingPrice_TextChanged(object sender, EventArgs e)
        {
            CalculateSellBackPrice();
        }

        protected void ddl_BBPrice_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            CalculateSellBackPrice();
        }

        protected void cbx_stores_CheckedChanged(object sender, EventArgs e)
        {
            GridViewRow ritem = (GridViewRow)((Control)sender).NamingContainer;
            NoteChanges(storeChanges, ((Label)ritem.FindControl("lbl_storeId")).Text, ((CheckBox)ritem.FindControl("cbx_stores")).Checked.ToString());
        }


        protected void lbn_NewItems_Click(object sender, System.EventArgs e)
        {
            //clear the fields
            ClearCatalogItemData();

            //make the CatalogItems details panel visible
            pnl_CatalogItemsDetails.Visible = true;

            //load the gift sets and stores
            BindStoreList(-1);
            BindItemSets(-1);

            //set id to -1 <indicator that it is a new entry>
            tbx_ItemID.Text = "-1";

        }

        protected void btn_Save_Click(object sender, System.EventArgs e)
        {
            if (!Page.IsValid)
            {
                return;
            }

            //grab WOK Items data source
            bool errorsOccured = false;
            int catalogItemId = 0;
            int idcheck = 0;
            
                        try
                        {
                            catalogItemId = Convert.ToInt32(tbx_ItemID.Text);

                            //deal with any special character replacement - work around for AJAX auto encoding

                            //grab all data for the promotion and update database
                            if (catalogItemId > 0)
                            {
                                ShoppingFacade shoppingFacade = new ShoppingFacade();
                                WOKItem origWokItem = shoppingFacade.GetItem(catalogItemId);

                                idcheck = WOKStoreUtility.UpdateCatalogItem(catalogItemId, Server.HtmlEncode(tbx_ItemName.Text), Server.HtmlEncode(tbx_DisplayName.Text.Trim()), inp_itemImage.Value, Server.HtmlEncode(tbx_Description.Text.Trim()), Convert.ToDateTime(txtItemAddedDate.Text),
                                 Convert.ToInt32(tbx_SellingPrice.Text), Convert.ToInt32(tbx_Price.Text), Convert.ToInt32(tbx_CreatorID.Text), Convert.ToInt32(ddl_CurrencyType.SelectedValue), Convert.ToInt32(ddl_CurrencyType.SelectedValue),
                                 Convert.ToInt32(ddl_ProductAvailability.SelectedValue), Convert.ToInt32(ddl_ProductExclusivity.SelectedValue), Convert.ToInt32(tbx_InitialAmount.Text), Convert.ToInt32(dgd_drpActive.SelectedValue), GetUserId());

                                // Now tickle WOK for the fields that are changed
                                // Item Name, Currency/Inventory Type, Description, Selling Price, 
                                if (!origWokItem.Name.Equals(Server.HtmlEncode(tbx_ItemName.Text))
                                    || !origWokItem.InventoryType.Equals(Convert.ToInt32(ddl_CurrencyType.SelectedValue))
                                    || !origWokItem.Description.Equals(Server.HtmlEncode(tbx_Description.Text))
                                    || !origWokItem.MarketCost.Equals(Convert.ToInt32(tbx_SellingPrice.Text))
                                    )
                                {
                                    SendItemTickle(RemoteEventSender.ObjectType.Item,
                                        RemoteEventSender.ChangeType.UpdateObject,
                                        catalogItemId);

                                    WOKStoreUtility.UpdateCatalogItemControlledByWeb(catalogItemId, 1);
                                }
                            }
                            else
                            {
                                idcheck = WOKStoreUtility.InsertCatalogItem(ref catalogItemId, Server.HtmlEncode(tbx_ItemName.Text), Server.HtmlEncode(tbx_DisplayName.Text), inp_itemImage.Value, Server.HtmlEncode(tbx_Description.Text), Convert.ToDateTime(txtItemAddedDate.Text),
                                 Convert.ToInt32(tbx_SellingPrice.Text), Convert.ToInt32(tbx_Price.Text), Convert.ToInt32(tbx_CreatorID.Text), Convert.ToInt32(ddl_CurrencyType.SelectedValue), 
                                 Convert.ToInt32(ddl_ProductAvailability.SelectedValue), Convert.ToInt32(ddl_ProductExclusivity.SelectedValue), Convert.ToInt32(tbx_InitialAmount.Text), GetUserId());
                            }

                            if (idcheck <= 0)
                            {
                                errorsOccured = true;
                                throw new Exception();
                            }
                        }
                        catch (Exception ex)
                        {
                            ErrorMessages.Text = "Error Saving Your Promotion: " + ex.Message;
                            ErrorMessages.ForeColor = Color.DarkRed;
                            return;
                        }

                        //write changes in pass group items to database
                        if (passGroupChanges.Count > 0)
                        {

                            foreach (string key in passGroupChanges.Keys)
                            {
                                idcheck = 1; //set to always be positive for now

                                int passGroupId = Convert.ToInt32(key);
                                bool inGroup = Convert.ToBoolean(passGroupChanges[key].ToLower());

                                //check to see if record already exist
                                DataTable inGroupAlready = WOKStoreUtility.GetItemByPassGroupID(catalogItemId, passGroupId);

                                if ((inGroupAlready.Rows.Count > 0) && (!inGroup))
                                {
                                    //if item is unchecked (no longer in group) but exist in database remove it
                                    WOKStoreUtility.DeleteFromPassGroup(passGroupId, catalogItemId, GetUserId());

                                    SendItemTickle (RemoteEventSender.ObjectType.ItemPassList,
                                        RemoteEventSender.ChangeType.DeleteObject,
                                        catalogItemId);

                                    WOKStoreUtility.UpdateCatalogItemControlledByWeb(catalogItemId, 1);
                                }
                                else if ((inGroupAlready.Rows.Count <= 0) && (inGroup))
                                {
                                    WOKStoreUtility.InsertIntoPassGroup(passGroupId, catalogItemId, GetUserId());

                                    SendItemTickle(RemoteEventSender.ObjectType.ItemPassList,
                                        RemoteEventSender.ChangeType.AddObject,
                                        catalogItemId);

                                    WOKStoreUtility.UpdateCatalogItemControlledByWeb(catalogItemId, 1);
                                }

                                if (idcheck <= 0)
                                {
                                    errorsOccured = true;
                                }
                            }
                        }

                        //write changes in items sets items to database
                        if (itemSetChanges.Count > 0)
                        {
                            //enumeration shouldn't cause over head issues due to low concurrent users
                            foreach (string key in itemSetChanges.Keys)
                            {
                                idcheck = -1;
                                int itemSetId = Convert.ToInt32(key);
                                bool inSet = Convert.ToBoolean(itemSetChanges[key].ToLower());

                                //check to see if record already exist
                                DataTable inSetAlready = WOKStoreUtility.GetItemSetByItemID(catalogItemId, itemSetId);

                                if ((inSetAlready.Rows.Count > 0) && (!inSet))
                                {
                                    //if item is unchecked (no longer in set) but exist in database remove it
                                    idcheck = WOKStoreUtility.DeleteFromItemSet(itemSetId, catalogItemId);
                                }
                                else if ((inSetAlready.Rows.Count <= 0) && (inSet))
                                {
                                    idcheck = WOKStoreUtility.InsertIntoItemSet(itemSetId, catalogItemId);
                                }

                                if (idcheck <= 0)
                                {
                                    errorsOccured = true;
                                }
                            }
                        }

                        //write changes in items sets items to database
                        if (storeChanges.Count > 0)
                        {
                            //enumeration shouldn't cause over head issues due to low concurrent users
                            foreach (string key in storeChanges.Keys)
                            {
                                idcheck = -1;
                                int storeId = Convert.ToInt32(key);
                                bool inSet = Convert.ToBoolean(storeChanges[key].ToLower());

                                //check to see if record already exist
                                DataTable inStoreAlready = WOKStoreUtility.GetItemStoreByItemIDStoreID(catalogItemId, storeId);

                                if ((inStoreAlready.Rows.Count > 0) && (!inSet))
                                {
                                    //if item is unchecked (no longer in set) but exist in database remove it
                                    idcheck = WOKStoreUtility.DeleteFromStoreInventories(storeId, catalogItemId);
                                }
                                else if ((inStoreAlready.Rows.Count <= 0) && (inSet))
                                {
                                    idcheck = WOKStoreUtility.InsertIntoStoreInventories(storeId, catalogItemId);
                                }

                                if (idcheck <= 0)
                                {
                                    errorsOccured = true;
                                }

                            }
                        }
                        
            //check to see if images need to be uploaded and saved
            if (!ItemImage.Equals(inp_itemImage.Value))
            //if (!ItemImage.Equals(browseTHUMB.Value))
            {
                UploadWOKItemImage(Convert.ToInt32(tbx_CreatorID.Text), catalogItemId);
            }

            //reset viewstate
            resetViewState();

            //pulledsaved info from database
            BindCatalogItemData(currentPage);

            //make the details panel invisible
            pnl_CatalogItemsDetails.Visible = false;

            if (!errorsOccured)
            {
                ErrorMessages.Text = "Save successful!";
                ErrorMessages.ForeColor = Color.DarkGreen;
            }
            else
            {
                ErrorMessages.Text = "An error has occured during save!";
                ErrorMessages.ForeColor = Color.DarkRed;
            }

        }

        /// <summary>
        /// SendItemTickle
        /// </summary>
        private void SendItemTickle (RemoteEventSender.ObjectType ot,
                RemoteEventSender.ChangeType ct,
                int globalId)
        {
            try
            {
                RemoteEventSender rs = new RemoteEventSender();
                rs.BroadcastItemChangeEvent(ot, ct, globalId);
            }
            catch (Exception exc)
            {
                m_logger.Error("Error tickling WOK", exc);
            }
        }

        /// <summary>
        /// BindItemCategories
        /// </summary>
        /// <param name="globalId"></param>
        protected void BindItemCategories (int globalId)
        {
            // Show the categories
            ShoppingFacade shoppingFacade = new ShoppingFacade();
            rptCategories.DataSource = shoppingFacade.GetItemCategoriesByItemId(globalId);
            rptCategories.DataBind();
        }


        /// <summary>
        /// btnRemoveInventory_Click
        /// </summary>
        protected void btnRemoveInventory_Click(object sender, EventArgs e)
        {
            int globalId = Convert.ToInt32(tbx_ItemID.Text);

            ShoppingFacade shoppingFacade = new ShoppingFacade();
            shoppingFacade.DeleteCustomItemTexture(globalId, KanevaWebGlobals.CurrentUser.UserId, true);

            SendItemTickle (RemoteEventSender.ObjectType.Item,
                RemoteEventSender.ChangeType.DeleteObject,
                globalId);

            //reset viewstate
            resetViewState();

            //pulledsaved info from database
            BindCatalogItemData(currentPage);

            //make the details panel invisible
            pnl_CatalogItemsDetails.Visible = false;

            ErrorMessages.Text = "Item removed from all inventories";
            ErrorMessages.ForeColor = Color.DarkGreen;
        }

        /// <summary>
        /// btnAddCategory_Click
        /// </summary>
        protected void btnAddCategory_Click(object sender, EventArgs e)
        {
            SetAddCategoryToTopLevel();
        }

        /// <summary>
        /// SetAddCategoryToTopLevel
        /// </summary>
        private void SetAddCategoryToTopLevel()
        {
            ShoppingFacade shoppingFacade = new ShoppingFacade();
            List<ItemCategory> lItemCategory = shoppingFacade.GetItemCategoriesByParentCategoryId((uint)0);

            rptAddCategories.DataSource = lItemCategory;
            rptAddCategories.DataBind();
        }

        /// <summary>
        /// btnSaveCategory_Click
        /// </summary>
        protected void btnSaveCategory_Click(object sender, EventArgs e)
        {
            ShoppingFacade shoppingFacade = new ShoppingFacade();

            CheckBox chkSelect;
            HtmlInputHidden hidItemCategoryId;

            int globalId = Convert.ToInt32(tbx_ItemID.Text);

            foreach (RepeaterItem dgiCategory in rptAddCategories.Items)
            {
                chkSelect = (CheckBox)dgiCategory.FindControl("chkSelect");

                if (chkSelect.Checked)
                {
                    hidItemCategoryId = (HtmlInputHidden)dgiCategory.FindControl("hidItemCategoryId");
                    shoppingFacade.AddCategoryToItem (globalId, Convert.ToUInt32(hidItemCategoryId.Value));
                }
            }

            BindItemCategories(globalId);
        }

        /// <summary>
        /// 
        /// </summary>
        protected void lbCategoryDelete_Click(object sender, CommandEventArgs e)
        {
            ShoppingFacade shoppingFacade = new ShoppingFacade();
            int globalId = Convert.ToInt32(tbx_ItemID.Text);
            shoppingFacade.DeleteCategoryFromItem(globalId, Convert.ToUInt32(e.CommandArgument));
            BindItemCategories(globalId);
        }

        /// <summary>
        /// lbCategory_Click
        /// </summary>
        protected void lbCategory_Click(object sender, CommandEventArgs e)
        {
            ShoppingFacade shoppingFacade = new ShoppingFacade();
            List<ItemCategory> lItemCategory = shoppingFacade.GetItemCategoriesByParentCategoryId(Convert.ToUInt32 (e.CommandArgument));

            if (lItemCategory.Count > 0)
            {
                rptAddCategories.DataSource = lItemCategory;
                rptAddCategories.DataBind();
            }
        }

        #endregion



    }
}
