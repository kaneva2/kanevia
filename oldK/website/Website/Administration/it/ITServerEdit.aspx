<%@ Page language="c#" Codebehind="ITServerEdit.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.ITServerEdit" %>
<%@ Register TagPrefix="Kaneva" TagName="HeaderText" Src="../../usercontrols/HeaderText.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="ITNav" Src="ITNav.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="AdminMenu" Src="../../usercontrols/AdministrativeMenu.ascx" %>

<Kaneva:AdminMenu runat="server" id="adminMenu" />
<Kaneva:ITNav runat="server" id="rnNavigation" SubTab="gl"/>

<table cellpadding="0" cellspacing="0" border="0" width="750">
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="20" height="1"/></td>
		<td colspan="3" width="710">
			<BR>
			<asp:ValidationSummary ShowMessageBox="False" ShowSummary="True" Class="formError" id="valSum" DisplayMode="BulletList" runat="server" HeaderText="Please correct the following errors:"/><br>
		</td>
		<td><img runat="server" src="~/images/spacer.gif" width="20" height="1"/></td>
	</tr>
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="20" height="1"/></td>
		<td colspan="3"><b id="rounded2"><i><span><img border="0" runat="server" src="~/images/spacer.gif" width="710" height="8"/></span></i> </b></td>
		<td><img runat="server" src="~/images/spacer.gif" width="20" height="1"/></td>
	</tr>
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
		<td width="160" style="background-color: #ededed;"></td>
		<td colspan="2" width="550" style="background-color: #ededed;"></td>
		<td><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
	</tr>
	<tr runat="server" id="trStoreAssetLink">
		<td><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
		<td colspan="3" style="background-color: #ededed;" width="710">&nbsp;&nbsp;
		</td>
		<td><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
	</tr>
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
		<td align="right" class="bodyText" style="border-left: 1px solid #ededed;">&nbsp;<b>server type:</b></td>
		<td><img runat="server" src="~/images/spacer.gif" width="5" ID="Img7"/></td>
		<td align="left" width="705" class="bodyText" style="border-right: 1px solid #ededed;">
			<asp:DropDownList class="filter2" id="drpType" runat="Server" style="width:450px"/>
			<a target="_resource" runat="server" href="http://docs.kaneva.com/bin/view/Public/KanevaFaqKgpLicensing" class="LnavText">License FAQ</a>
		</td>
		<td width="10"><img runat="server" src="~/images/spacer.gif" width="20" height="18"/></td>
	</tr>


	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
		<td align="right" class="bodyText" bgcolor="#ededed" style="border-left: 1px solid #ededed;">&nbsp;<b>server&nbsp;name:</b></td>
		<td bgcolor="#ededed"><img runat="server" src="~/images/spacer.gif" width="5"/></td>
		<td align="left" width="705" class="bodyText" bgcolor="#ededed" style="border-right: 1px solid #ededed;">
			<asp:TextBox ID="txtServerName" class="Filter2" style="width:450px" MaxLength="50" runat="server"/> 
			<asp:RequiredFieldValidator ID="rftxtServerName" ControlToValidate="txtServerName" Text="*" ErrorMessage="Server name is a required field." Display="Dynamic" runat="server"/>
		</td>
		<td width="10"><img runat="server" src="~/images/spacer.gif" width="20" height="18"/></td>
	</tr>

	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
		<td align="right" class="bodyText" style="border-left: 1px solid #ededed;">&nbsp;<b>patch URL:</b></td>
		<td><img runat="server" src="~/images/spacer.gif" width="5"/></td>
		<td align="left" width="705" class="bodyText" style="border-right: 1px solid #ededed;">
			<asp:TextBox ID="txtPatchURL" class="Filter2" style="width:450px" MaxLength="255" runat="server"/> 
		</td>
		<td width="10"><img runat="server" src="~/images/spacer.gif" width="20" height="18"/></td>
	</tr>
	<tr>
		<td><img id="Img1" runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
		<td align="right" class="bodyText" bgcolor="#ededed" style="border-left: 1px solid #ededed;">&nbsp;<b>patch URL V2:</b></td>
		<td bgcolor="#ededed"><img id="Img2" runat="server" src="~/images/spacer.gif" width="5"/></td>
		<td align="left" width="705" class="bodyText" bgcolor="#ededed" style="border-right: 1px solid #ededed;">
			<asp:TextBox ID="txtPatchURLV2" class="Filter2" style="width:450px" MaxLength="255" runat="server"/> 
		</td>
		<td width="10"><img id="Img3" runat="server" src="~/images/spacer.gif" width="20" height="18"/></td>
	</tr>
	<tr>
		<td><img id="Img4" runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
		<td align="right" class="bodyText" style="border-left: 1px solid #ededed;">&nbsp;<b>patch URL V3:</b></td>
		<td><img id="Img5" runat="server" src="~/images/spacer.gif" width="5"/></td>
		<td align="left" width="705" class="bodyText" style="border-right: 1px solid #ededed;">
			<asp:TextBox ID="txtPatchURLV3" class="Filter2" style="width:450px" MaxLength="255" runat="server"/> 
		</td>
		<td width="10"><img id="Img6" runat="server" src="~/images/spacer.gif" width="20" height="18"/></td>
	</tr>
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
		<td align="right" bgcolor="#ededed" class="bodyText" style="border-left: 1px solid #ededed;"><b>admin URL:</b></td>
		<td bgcolor="#ededed"><img runat="server" src="~/images/spacer.gif" width="5"/></td>
		<td align="left" width="705" bgcolor="#ededed" class="bodyText" style="border-right: 1px solid #ededed;">
			<asp:TextBox ID="txtAdminURL" class="Filter2" style="width:450px" MaxLength="255" runat="server"/> 
		</td>
		<td width="10"><img runat="server" src="~/images/spacer.gif" width="20" height="18"/></td>
	</tr>	
	
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
		<td align="right" class="bodyText" style="border-left: 1px solid #ededed;">&nbsp;<b>IT Game Server:</b></td>
		<td><img runat="server" src="~/images/spacer.gif" width="5"/></td>
		<td align="left" width="705" class="bodyText" style="border-right: 1px solid #ededed;">
			<asp:TextBox ID="txtITGameServer" class="Filter2" style="width:450px" MaxLength="50" runat="server"/> 
		</td>
		<td width="10"><img runat="server" src="~/images/spacer.gif" width="20" height="18"/></td>
	</tr>
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
		<td align="right" bgcolor="#ededed" class="bodyText" style="border-left: 1px solid #ededed;"><b>IT Game Binary Directory:</b></td>
		<td bgcolor="#ededed"><img runat="server" src="~/images/spacer.gif" width="5"/></td>
		<td align="left" width="705" bgcolor="#ededed" class="bodyText" style="border-right: 1px solid #ededed;">
			<asp:TextBox ID="txtITBinaryDirectory" class="Filter2" style="width:450px" MaxLength="255" runat="server"/> 
		</td>
		<td width="10"><img runat="server" src="~/images/spacer.gif" width="20" height="18"/></td>
	</tr>	
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
		<td align="right" class="bodyText" style="border-left: 1px solid #ededed;">&nbsp;<b>IT Game Directory:</b></td>
		<td><img runat="server" src="~/images/spacer.gif" width="5"/></td>
		<td align="left" width="705" class="bodyText" style="border-right: 1px solid #ededed;">
			<asp:TextBox ID="txtITGameDirectory" class="Filter2" style="width:450px" MaxLength="255" runat="server"/> 
		</td>
		<td width="10"><img runat="server" src="~/images/spacer.gif" width="20" height="18"/></td>
	</tr>
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
		<td align="right" bgcolor="#ededed" class="bodyText" style="border-left: 1px solid #ededed;"><b>Purchase Date:</b></td>
		<td bgcolor="#ededed"><img runat="server" src="~/images/spacer.gif" width="5"/></td>
		<td align="left" width="705" bgcolor="#ededed" class="bodyText" style="border-right: 1px solid #ededed;">
			<asp:Label id="lblPurchaseDate" style="color: green; font-size: 18px;" runat="server"/>
		</td>
		<td width="10"><img runat="server" src="~/images/spacer.gif" width="20" height="18"/></td>
	</tr>	
	<tr class="FullBorders">
		<td><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
		<td align="right" class="bodyText" style="border-left: 1px solid #ededed;">&nbsp;<b>server key:</b></td>
		<td><img runat="server" src="~/images/spacer.gif" width="5"/></td>
		<td align="left" width="705" style="border-right: 1px solid #ededed;">
			<asp:Label id="lblServerKey" style="color: green; font-size: 18px;" runat="server"/>
		</td>
		<td width="10"><img runat="server" src="~/images/spacer.gif" width="20" height="18"/></td>
	</tr>
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
		<td align="right" bgcolor="#ededed" class="bodyText" style="border-left: 1px solid #ededed;"><b>Server version:</b></td>
		<td bgcolor="#ededed"><img runat="server" src="~/images/spacer.gif" width="5"/></td>
		<td align="left" width="705" bgcolor="#ededed" class="bodyText" style="border-right: 1px solid #ededed;">
			<asp:TextBox ID="txtServerVersion" class="Filter2" style="width:450px" MaxLength="255" runat="server"/> 
		</td>
		<td width="10"><img runat="server" src="~/images/spacer.gif" width="20" height="18"/></td>
	</tr>	
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
		<td align="right" bgcolor="#ededed" class="bodyText" style="border-left: 1px solid #ededed;"><b>Status:</b></td>
		<td bgcolor="#ededed"><img runat="server" src="~/images/spacer.gif" width="5"/></td>
		<td align="left" width="705" bgcolor="#ededed" class="bodyText" style="border-right: 1px solid #ededed;">
			<asp:Label class="filter2" id="lblStatus" runat="server"/>
		</td>
		<td width="10"><img runat="server" src="~/images/spacer.gif" width="20" height="18"/></td>
	</tr>	
	
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
		<td colspan="3" align="right" bgcolor="#ededed"><br>
			<asp:button id="btnCancel" runat="Server" CausesValidation="False" onClick="btnCancel_Click" class="Filter2" Text="  cancel  "/>&nbsp;&nbsp;<asp:button id="Button1" runat="Server" onClick="btnUpdate_Click" class="Filter2" Text="  submit  "/>&nbsp;&nbsp;&nbsp;&nbsp;<br>
		</td>
		<td><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
	</tr>
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="20" height="1"/></td>
		<td colspan="3">
			<b id="rounded2bot">
			<i><span><img border="0" runat="server" src="~/images/spacer.gif" width="710" height="8"/></span></i> 
			</b>
		</td>
		<td><img runat="server" src="~/images/spacer.gif" width="20" height="1"/></td>
	</tr>
</table>  	
<br><br>
