<%@ Page language="c#" Codebehind="GameList.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.GameList" %>
<%@ Register TagPrefix="Kaneva" TagName="StoreFilter" Src="../../usercontrols/StoreFilter.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../../usercontrols/Pager.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="ITNav" Src="ITNav.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="AdminMenu" Src="../../usercontrols/AdministrativeMenu.ascx" %>

<style type=\"text/css\">
<!--

.lineItemColHeadgreen
{
	font-family: Verdana, Arial, Helvetica, sans-serif; 
	font-size: 11px; 
	line-height: 19px; 
	font-style: normal;
	color: black;
	text-align: center;
	vertical-align: middle;
	letter-spacing: normal;
	word-spacing: normal;
	background-color: #bbbbbb;	
}

.lineItemColHeadgreen A:link
{
	color: #ffffff;
}

.lineItemColHeadgreen A:active
{
	color: #ffffff;
}

.lineItemColHeadgreen A:visited
{
	color: #ffffff;
}

.lineItemColHeadgreen A:hover
{
	color: #000000;
}

.assetStoreStats
{
	color: #B0B0B0;
	font-family: Arial;
	font-size: 10px;
}

-->
</style>

<table runat="server" id="tblNoAdmin" cellpadding="0" cellspacing="0" border="0" width="560">
	<tr><td ALIGN="CENTER">
		<BR><BR><BR>
		<span class="subHead">You must be IT to access this section.</span>
	</td></tr>
</table>

<asp:panel runat="server" ID="pnlAdmin">

<Kaneva:ITNav runat="server" id="rnNavigation" SubTab="gl"/>

 <br />
 
 <table id="tblUserFame" border="0" cellpadding="0" cellspacing="0" width="1000" style="background-color:#ffffff">
	<tr>
		<td style="height:20px"></td>
    </tr>
    <tr>
         <td align="center">

<input type="hidden" runat="server" id="AssetId" value="0"> 
<asp:button ID="btnDeleteAsset" OnClick="Delete_Asset" runat="server" Visible="false"></asp:button>

<input type="hidden" runat="server" id="hidGameLicId" value="0"> 
<asp:button ID="btnDeleteServer" OnClick="Delete_Server" runat="server" Visible="false"></asp:button>

<asp:button ID="btnActivateServer" OnClick="Activate_Server" runat="server" Visible="false"></asp:button>

<center>
<table cellpadding="0" cellspacing="0" border="0" width="730">
	<tr><td valign="middle" class="belowFilter" align="left" width="230">&nbsp;</td><td width="500" align="right" valign="bottom"><asp:Label runat="server" id="lblSearch" CssClass="showingCount"/>&nbsp;&nbsp;<Kaneva:Pager runat="server" id="pgTop"/></td></tr>
</table>
</center>
<Kaneva:StoreFilter runat="server" id="filStore" AssetType="1"/>
<table cellpadding="0" cellspacing="0" border="0" width="750">	
	<tr>
		<td width="17"><img runat="server" src="~/images/spacer.gif" width="17" height="5" border="0" ID="Img4"/></td>
		<td colspan="9" class="FullBorders">
			<asp:DataGrid runat="server" EnableViewState="False" ShowFooter="False" Width="100%" id="dgrdAsset" cellpadding="0" cellspacing="0" border="0" AutoGenerateColumns="False" AllowSorting="True" style="border: 0px hidden white;">  
				<HeaderStyle CssClass="lineItemColHeadgreen"/>
				<ItemStyle CssClass="lineItemEven"/>
				<AlternatingItemStyle CssClass="lineItemOdd"/>
				<Columns>
					<asp:TemplateColumn Visible="false" HeaderText="  date" SortExpression="created_date" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="top" ItemStyle-Width="3%" ItemStyle-Wrap="false">
						<ItemTemplate><br><span class="adminLinks" style="font-weight:bold;"><%#FormatDateNumbersOnly (DataBinder.Eval(Container.DataItem, "created_date"))%></span>								</ItemTemplate>
					</asp:TemplateColumn>		
					<asp:TemplateColumn HeaderText="" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="top" ItemStyle-Width="25%">
						<ItemTemplate>
							<a title='<%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "name").ToString ())%>' href='<%# GetAssetDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "asset_id")))%>'><img src='<%#GetMediaImageURL (DataBinder.Eval(Container.DataItem, "thumbnail_medium_path").ToString () ,"me", Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_id")), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_type_id")), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "thumbnail_gen")))%>' border="0" style="margin: 10px;"/></a><br><asp:label id="Label3" visible='<%#IsAdministrator ()%>' runat="server"><br></asp:label>
							<asp:hyperlink CssClass="adminLinks" id="hlEdit" visible='<%#IsAdministrator ()%>' runat="server" NavigateURL='<%#GetAssetEditLink (Convert.ToInt32 ( DataBinder.Eval(Container.DataItem, "asset_id")))%>' ToolTip="Edit this Item">edit</asp:hyperlink>&nbsp;<asp:hyperlink CssClass="adminLinks" id="hlDeleteTest" runat="server" visible='<%#IsAdministrator ()%>'  NavigateURL='<%#GetDeleteScript (Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_id"))) %>' ToolTip="Delete this Item">delete</asp:hyperlink><asp:label id="Label4" visible='<%#IsAdministrator ()%>' runat="server"><br><br></asp:label>	
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="item name" SortExpression="a.name" ItemStyle-Width="75%">
						<ItemTemplate>
						<br>
							<a href='<%# GetAssetDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "asset_id")))%>' class="assetLink"><%# TruncateWithEllipsis (Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "name").ToString ()), 40) %></a>
							<span style="line-height: 7px; ">
							<br><br></span>						
							<span class="assetStoreStats">type:</span> <span class="dateStamp"><%# GetAssetTypeName (Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_type_id")))%></span>&nbsp;&nbsp;<span class="assetStoreStats">owner:</span>&nbsp;&nbsp;<a class="dateStamp" href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "username").ToString ())%>><%# DataBinder.Eval(Container.DataItem, "username") %></a>&nbsp;&nbsp;<span class="assetStoreStats">size:</span> <font color="green"> <%# FormatImageSize (DataBinder.Eval (Container.DataItem, "file_size")) %></font>
							<span class="assetStoreStats">price:</span><span style="COLOR: #459113; font-size: 12px; font-weight: bold;"><%# FormatKpoints (DataBinder.Eval (Container.DataItem, "amount")) %></span><br><br>
							
							 <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                    <asp:Repeater id="rptGameServers" runat="server" EnableViewState="True">
                                            <HeaderTemplate>
                                                <tr Class="lineItemColHead">
                                                    <td width="5%">Type</td>
                                                    <td width="40%">Server Name</td>
                                                    <td width="30%">Game Key</td>
                                                    <td width="10%"><span title="Max Users">Max</span></td>
                                                    <td width="10%">status</td>
                                                    <td width="5%"></td>
                                                </tr>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr class="LineItemEven">
                                                    <td align="center" class="bodyText"><%# GetGameType (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "license_type"))) %></td>
                                                    <td align="center" class="bodyText"><span title='<%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "server_name").ToString ()) %>'><%# TruncateWithEllipsis (Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "server_name").ToString ()), 35) %></span></td>
                                                    <td align="center" class="bodyText"><%# DataBinder.Eval (Container.DataItem, "game_key") %></td>
                                                    <td align="center" class="bodyText"><%# DataBinder.Eval (Container.DataItem, " max_server_users") %> </td>
                                                    <td align="center" class="bodyText"><A runat="server" class="adminLinks" HREF='#' onclick='<%# GetGameLicenseStatusLink (Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "status_id")))%>'><%# DataBinder.Eval (Container.DataItem, "status_name") %></a></td>
                                                    <td align="center" class="bodyText">
                                                            <asp:hyperlink CssClass="adminLinks" runat="server" NavigateURL='<%#GetServerEditLink ((int) DataBinder.Eval(Container.DataItem, "game_license_id"), (int) DataBinder.Eval(Container.DataItem, "asset_id"))%>' ToolTip="Edit">edit</asp:hyperlink><br/>
                                                            <asp:hyperlink CssClass="adminLinks" runat="server" NavigateURL='<%#GetServerDeleteScript ((int) DataBinder.Eval(Container.DataItem, "game_license_id"), (int) DataBinder.Eval(Container.DataItem, "asset_id")) %>' ToolTip="Delete">delete</asp:hyperlink>
                                                            <asp:hyperlink Visible='<%# CanActivate (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "license_type")), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "status_id")))%>' CssClass="adminLinks" runat="server" NavigateURL='<%#GetServerActivateScript ((int) DataBinder.Eval(Container.DataItem, "game_license_id"), (int) DataBinder.Eval(Container.DataItem, "asset_id")) %>' ToolTip="Activate">activate</asp:hyperlink>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <AlternatingItemTemplate>
                                                <tr class="LineItemOdd">
                                                    <td align="center" class="bodyText"><%# GetGameType (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "license_type"))) %></td>
                                                    <td align="center" class="bodyText"><span title='<%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "server_name").ToString ()) %>'><%# TruncateWithEllipsis (Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "server_name").ToString ()), 35) %></span></td>
                                                    <td align="center" class="bodyText"><%# DataBinder.Eval (Container.DataItem, "game_key") %></td>
                                                    <td align="center" class="bodyText"><%# DataBinder.Eval (Container.DataItem, " max_server_users") %> </td>
                                                    <td align="center" class="bodyText"><A runat="server" class="adminLinks" HREF='#' onclick='<%# GetGameLicenseStatusLink (Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "status_id")))%>'><%# DataBinder.Eval (Container.DataItem, "status_name") %></a></td>
                                                    <td align="center" class="bodyText">
                                                            <asp:hyperlink CssClass="adminLinks" runat="server" NavigateURL='<%#GetServerEditLink ((int) DataBinder.Eval(Container.DataItem, "game_license_id"), (int) DataBinder.Eval(Container.DataItem, "asset_id"))%>' ToolTip="Edit">edit</asp:hyperlink><br/>
                                                            <asp:hyperlink CssClass="adminLinks" runat="server" NavigateURL='<%#GetServerDeleteScript ((int) DataBinder.Eval(Container.DataItem, "game_license_id"), (int) DataBinder.Eval(Container.DataItem, "asset_id")) %>' ToolTip="Delete">delete</asp:hyperlink>
                                                            <asp:hyperlink Visible='<%# CanActivate (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "license_type")), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "status_id")))%>' CssClass="adminLinks" runat="server" NavigateURL='<%#GetServerActivateScript ((int) DataBinder.Eval(Container.DataItem, "game_license_id"), (int) DataBinder.Eval(Container.DataItem, "asset_id")) %>' ToolTip="Activate">activate</asp:hyperlink>
                                                    </td>
                                                </tr>
                                            </AlternatingItemTemplate>
                                    </asp:Repeater>
                                    
                                    <tr Class="LineItemEven">
										<td Colspan="6" align="right">&nbsp;</td>
                                    </tr>
                                    <tr Class="LineItemEven">
										<td Colspan="6" align="right">
                                              <asp:hyperlink id="hlAddServer" CssClass="adminLinks" runat="server" ToolTip="add new server">add new server</asp:hyperlink><br/>
                                        &nbsp;</td>
                                    </tr>
                                </table>
							
							
							<br/><br/>
						</ItemTemplate>
					</asp:TemplateColumn>
	
				</Columns>
			</asp:datagrid>
			
			<asp:DataList Visible="False" runat="server" EnableViewState="False" ShowFooter="False" Width="100%" id="dlAsset" cellpadding="2" cellspacing="2" border="0" RepeatColumns="3" RepeatDirection="Horizontal">
				<ItemStyle CssClass="lineItemThumb" />
				<ItemTemplate>
					<a title='<%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "name").ToString ())%>' href='<%# GetAssetDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "asset_id")))%>'><img src='<%#GetMediaImageURL (DataBinder.Eval(Container.DataItem, "thumbnail_medium_path").ToString () ,"me", Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_id")), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_type_id")), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "thumbnail_gen")))%>' style="border: 1px solid #000000;"/></a><br>
					<asp:label id="Label2" visible='<%#IsAdministrator ()%>' runat="server"><br></asp:label>
							<asp:hyperlink CssClass="adminLinks" id="Hyperlink1" visible='<%#IsAdministrator ()%>' runat="server" NavigateURL='<%#GetAssetEditLink (Convert.ToInt32 ( DataBinder.Eval(Container.DataItem, "asset_id")))%>' ToolTip="Edit this Item">edit</asp:hyperlink>&nbsp;<asp:hyperlink CssClass="adminLinks" id="Hyperlink2" runat="server" visible='<%#IsAdministrator ()%>'  NavigateURL='<%#GetDeleteScript (Convert.ToInt32 ( DataBinder.Eval(Container.DataItem, "asset_id"))) %>' ToolTip="Delete this Item">delete</asp:hyperlink>			
					<BR/>
					<a title='<%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "name").ToString ())%>' style="font-size: 12px;" href='<%# GetAssetDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "asset_id")))%>' style="COLOR: #3258ba; font-size: 12px;"><%# TruncateWithEllipsis (Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "name").ToString ()), 18) %></a><BR/>
					<font color="green" style="font-size: 12px;"><b><%# FormatKpoints (DataBinder.Eval (Container.DataItem, "amount")) %></b></font>
				</ItemTemplate>
			</asp:DataList>
			
			
			<asp:DataGrid EnableViewState="False" runat="server" ShowFooter="False" Width="100%" id="listAsset" cellpadding="0" cellspacing="0" border="0" AutoGenerateColumns="False" AllowSorting="True" style="border: 0px hidden white;">  
				<HeaderStyle CssClass="lineItemColHead"/>
				<ItemStyle CssClass="lineItemEven"/>
				<AlternatingItemStyle CssClass="lineItemOdd"/>
				<Columns>
					<asp:TemplateColumn HeaderText=" " ItemStyle-Width="4%" ItemStyle-VerticalAlign="top">
						<ItemTemplate>
						<asp:hyperlink CssClass="adminLinks" id="Hyperlink3" visible='<%#IsAdministrator ()%>' runat="server" NavigateURL='<%#GetAssetEditLink (Convert.ToInt32 ( DataBinder.Eval(Container.DataItem, "asset_id")))%>' ToolTip="Edit this Item">edit</asp:hyperlink>&nbsp;<asp:hyperlink CssClass="adminLinks" id="Hyperlink4" runat="server" visible='<%#IsAdministrator ()%>'  NavigateURL='<%#GetDeleteScript (Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_id"))) %>' ToolTip="Delete this Item">delete</asp:hyperlink>&nbsp;
						</ItemTemplate>
					</asp:TemplateColumn>		
					<asp:TemplateColumn HeaderText="item name" SortExpression="a.name" ItemStyle-Width="28%" ItemStyle-VerticalAlign="top">
						<ItemTemplate>
							<a title='<%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "name").ToString ())%>' href='<%# GetAssetDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "asset_id")))%>' style="COLOR: #3258ba; font-size: 12px;"><%# TruncateWithEllipsis (Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "name").ToString ()), 30) %></a>
						</ItemTemplate>
					</asp:TemplateColumn>
<asp:TemplateColumn HeaderText="size" SortExpression="file_size" ItemStyle-Width="11%" ItemStyle-HorizontalAlign="Center">
						<ItemTemplate>
							<font color="green"> <%# FormatImageSize (DataBinder.Eval (Container.DataItem, "file_size")) %></font>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="published" SortExpression="created_date" ItemStyle-Width="22%" ItemStyle-HorizontalAlign="Center">
						<ItemTemplate>
							<span class="adminLinks"><%# DataBinder.Eval (Container.DataItem, "created_date") %></span>
						</ItemTemplate>
					</asp:TemplateColumn>

					<asp:TemplateColumn HeaderText="k-points" SortExpression="a.amount" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center">
						<ItemTemplate>
							<span style="COLOR: #459113; font-weight: bold;"><%# FormatKpoints (DataBinder.Eval (Container.DataItem, "amount")) %></span>
						</ItemTemplate>
					</asp:TemplateColumn>
				</Columns>
			</asp:datagrid>
			
			
		</td>
		<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="5" border="0" ID="Img1"/></td>
	</tr>


</table><br><br>

</asp:panel>


		</td>
	</tr>
</table>