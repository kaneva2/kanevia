///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using log4net;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for ITServerEdit.
	/// </summary>
	public class ITServerEdit : MainTemplatePage
	{
		/// <summary>
		/// publishedGames
		/// </summary>
		protected ITServerEdit () 
		{
			Title = "IT - Server Add/Edit";
		}

		/// <summary>
		/// Page_Load
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Page_Load (object sender, System.EventArgs e)
		{
			// They must be logged in
			if (!Request.IsAuthenticated)
			{
				Response.Redirect (GetLoginURL ());
				return;
			}

			if (!IsIT ())
			{
				ShowErrorOnStartup ("Must be IT");
				return;
			}

			int gameLicenseId = Convert.ToInt32 (Request ["gli"]);
			int assetId = Convert.ToInt32 (Request ["assetId"]);

			if (!IsPostBack)
			{
				DataRow drGameLicense = null;

				// For existing licenses, get the asset id to prevent query string tampering
				if (!IsNewServer ())
				{
					drGameLicense = StoreUtility.GetGameLicense (gameLicenseId);
					assetId = Convert.ToInt32 (drGameLicense ["asset_id"]);
				}

				// Add the staging license
				drpType.Items.Add (new ListItem ("Staging License", ((int)Constants.eLICENSE_SUBSCRIPTIONS.STAGING).ToString ()));

				// Set up the form
				DataRow drDevSubscription = StoreUtility.GetLicenseSubscription ((int) Constants.eLICENSE_SUBSCRIPTIONS.DEVELOPER);
				drpType.Items.Add (new ListItem (drDevSubscription ["name"].ToString () + " (Free, Max of " + drDevSubscription ["max_server_users"].ToString () + " concurrent users)", ((int)Constants.eLICENSE_SUBSCRIPTIONS.DEVELOPER).ToString ()));
				
				// Can be a pilot only if it is not already a commercial
				if (!StoreUtility.IsCommercialGame (assetId))
				{
					DataRow drPilotSubscription = StoreUtility.GetLicenseSubscription ((int) Constants.eLICENSE_SUBSCRIPTIONS.PILOT);
					drpType.Items.Add (new ListItem (drPilotSubscription ["name"].ToString () + " (" + FormatCurrency (StoreUtility.ConvertKPointsToDollars (Convert.ToDouble (drPilotSubscription ["amount"]))) + "/month, Max of " + drPilotSubscription ["max_server_users"].ToString () + " concurrent users)", ((int)Constants.eLICENSE_SUBSCRIPTIONS.PILOT).ToString ()));
				}
				
				// Can be a commercial only if it is not a pilot
				if (!StoreUtility.IsPilotGame (assetId))
				{
					DataRow drCommSubscription = StoreUtility.GetLicenseSubscription ((int) Constants.eLICENSE_SUBSCRIPTIONS.COMMERCIAL);
					drpType.Items.Add (new ListItem (drCommSubscription ["name"].ToString () + " (" + FormatCurrency (StoreUtility.ConvertKPointsToDollars (Convert.ToDouble (drCommSubscription ["amount"]))) + "/month minimum, Max of " + drCommSubscription ["max_server_users"].ToString () + " concurrent users)", ((int)Constants.eLICENSE_SUBSCRIPTIONS.COMMERCIAL).ToString ()));
				}

				// Is it an add new?
				if (IsNewServer ())
				{
					// It is an add new
					DataRow drAsset = StoreUtility.GetAsset (assetId);
					txtServerName.Text = Server.HtmlDecode (drAsset ["name"].ToString ());		// Default to game name
					lblServerKey.Text = "[Not Created Yet]";
					lblStatus.Text = "New License";

					drpType.Enabled = true;
				}
				else
				{
					txtServerName.Text = Server.HtmlDecode (drGameLicense ["server_name"].ToString ());
					txtAdminURL.Text = Server.HtmlDecode (drGameLicense ["admin_URL"].ToString ());
					lblServerKey.Text = drGameLicense ["game_key"].ToString ();
					lblStatus.Text = drGameLicense ["status_name"].ToString ();
					lblStatus.ToolTip = drGameLicense ["status_description"].ToString ();
					txtPatchURL.Text = Server.HtmlDecode (drGameLicense ["patch_URL"].ToString ());
                    txtPatchURLV2.Text = Server.HtmlDecode(drGameLicense["patch_url_v2"].ToString());
                    txtPatchURLV3.Text = Server.HtmlDecode(drGameLicense["patch_url_v3"].ToString());

					txtITGameServer.Text = Server.HtmlDecode (drGameLicense ["IT_game_server"].ToString ());
					txtITGameDirectory.Text = Server.HtmlDecode (drGameLicense ["IT_game_directory"].ToString ());
					txtITBinaryDirectory.Text = Server.HtmlDecode (drGameLicense ["IT_game_binaries_directory"].ToString ());
					txtServerVersion.Text = Server.HtmlDecode (drGameLicense ["server_version"].ToString ());

					SetDropDownIndex (drpType, drGameLicense ["license_type"].ToString ());
					drpType.Enabled = false;
				}
			}
		}

		/// <summary>
		/// IsNewServer
		/// </summary>
		/// <returns></returns>
		private bool IsNewServer ()
		{
			int gameLiceseId = Convert.ToInt32 (Request ["gli"]);		
			return (gameLiceseId == 0);
		}


		/// <summary>
		/// Cancel Event Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnCancel_Click (object sender, EventArgs e) 
		{
            Response.Redirect("~/Administration/it/GameList.aspx?ttCat=mykaneva");
		}

		/// <summary>
		/// Update Event Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnUpdate_Click (object sender, EventArgs e) 
		{
			// Server validation
			if (!Page.IsValid) 
			{
				return;
			}

			int gameLicenseId = Convert.ToInt32 (Request ["gli"]);
			int assetId = Convert.ToInt32 (Request ["assetId"]);
			int userId = GetUserId ();

			if (IsNewServer ())
			{
				// If it is commercial, validate a credit card for $200
				if (Convert.ToInt32 (drpType.SelectedValue).Equals ((int) Constants.eLICENSE_SUBSCRIPTIONS.COMMERCIAL))
				{
					int newGameLicenseId = StoreUtility.InsertITGameLicense (assetId, Convert.ToInt32 (drpType.SelectedValue), GetUserId (), Server.HtmlEncode (txtServerName.Text), Server.HtmlEncode (txtPatchURL.Text), Server.HtmlEncode (txtPatchURLV2.Text), Server.HtmlEncode (txtPatchURLV3.Text), Constants.eGAME_LICENSE_STATUS.WAITING_ON_IT, Server.HtmlEncode (txtITGameServer.Text), Server.HtmlEncode (txtITGameDirectory.Text), Server.HtmlEncode (txtITBinaryDirectory.Text), Server.HtmlEncode (txtServerVersion.Text));
				}
				else
				{
					// DEV or Pilot go right to active
                    StoreUtility.InsertITGameLicense(assetId, Convert.ToInt32(drpType.SelectedValue), GetUserId(), Server.HtmlEncode(txtServerName.Text), Server.HtmlEncode(txtPatchURL.Text), Server.HtmlEncode(txtPatchURLV2.Text), Server.HtmlEncode(txtPatchURLV3.Text), Constants.eGAME_LICENSE_STATUS.ACTIVE, Server.HtmlEncode(txtITGameServer.Text), Server.HtmlEncode(txtITGameDirectory.Text), Server.HtmlEncode(txtITBinaryDirectory.Text), Server.HtmlEncode(txtServerVersion.Text));
				}
			}
			else
			{
                StoreUtility.UpdateITGameLicense(gameLicenseId, GetUserId(), assetId, Server.HtmlEncode(txtServerName.Text), Server.HtmlEncode(txtPatchURL.Text), Server.HtmlEncode(txtPatchURLV2.Text), Server.HtmlEncode(txtPatchURLV3.Text), Server.HtmlEncode(txtITGameServer.Text), Server.HtmlEncode(txtITGameDirectory.Text), Server.HtmlEncode(txtITBinaryDirectory.Text), Server.HtmlEncode(txtServerVersion.Text));
			}


            Response.Redirect("~/Administration/it/GameList.aspx?ttCat=mykaneva");
		}

		// Logger
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

        protected TextBox txtServerName, txtPatchURL, txtPatchURLV2, txtPatchURLV3, txtAdminURL, txtITGameServer, txtITBinaryDirectory, txtITGameDirectory, txtServerVersion;

		protected Label lblServerKey, lblStatus;

		protected DropDownList drpType;

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}