///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using log4net;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for _default.
	/// </summary>
	public class GameList : StoreBasePage
	{
		protected GameList () 
		{
			Title = "IT - Game Server Setup";
		}

		private void Page_Load (object sender, System.EventArgs e)
		{
			pnlAdmin.Visible = (IsIT ());
			tblNoAdmin.Visible = (!IsIT ());

			if (!IsPostBack)
			{
				BindData (1, "");
			}
	
			// Add the javascript for deleting servers
			string scriptString = "<script language=\"javaScript\">\n<!--\n function DeleteServer (id,assetId) {\n";
			scriptString += "   document.all.hidGameLicId.value = id;\n";
			scriptString += "   document.all.AssetId.value = assetId;\n";
            scriptString += "   " + ClientScript.GetPostBackEventReference(btnDeleteServer, "") + ";\n";                // call the __doPostBack function to post back the form and execute the PageClick event
			scriptString += "}\n// -->\n";
			scriptString += "</script>";
                
			if (!ClientScript.IsClientScriptBlockRegistered (GetType (), "DeleteServer"))
			{
                ClientScript.RegisterClientScriptBlock(GetType(), "DeleteServer", scriptString);
			}

			// Activate a commercial server
			scriptString = "<script language=\"javaScript\">\n<!--\n function ActivateServer (id,assetId) {\n";
			scriptString += "   document.all.hidGameLicId.value = id;\n";
			scriptString += "   document.all.AssetId.value = assetId;\n";
            scriptString += "   " + ClientScript.GetPostBackEventReference(btnActivateServer, "") + ";\n";                // call the __doPostBack function to post back the form and execute the PageClick event
			scriptString += "}\n// -->\n";
			scriptString += "</script>";

            if (!ClientScript.IsClientScriptBlockRegistered(GetType(), "ActivateServer"))
			{
                ClientScript.RegisterClientScriptBlock(GetType(), "ActivateServer", scriptString);
			}
		}

		/// <summary>
		/// Delete an asset
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void Delete_Asset (Object sender, EventArgs e)
		{
			if (!IsIT ())
			{
				m_logger.Warn ("User " + GetUserId () + " from IP " + Request.UserHostAddress + " tried to delete store item " + AssetId.Value + " but is not a site administrator");
				return;
			}

			StoreUtility.DeleteAsset (Convert.ToInt32 (AssetId.Value), GetUserId ());
			pg_PageChange (this, new PageChangeEventArgs (pgTop.CurrentPageNumber));
		}

		protected void Activate_Server (Object sender, EventArgs e)
		{
			if (IsIT ())
			{
				int assetId = Convert.ToInt32 (AssetId.Value);
				int gameLicenseId = Convert.ToInt32 (hidGameLicId.Value);
				StoreUtility.UpdateITGameLicense (gameLicenseId, assetId, GetUserId (), Constants.eGAME_LICENSE_STATUS.ACTIVE);
			}

			// Rebind Servers
			BindData (pgTop.CurrentPageNumber, filStore.CurrentFilter);
		}

		/// <summary>
		/// Delete a Server
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void Delete_Server (Object sender, EventArgs e)
		{
			int assetId = Convert.ToInt32 (AssetId.Value);

			// Only the owner can delete it
			if (IsIT ())
			{
				int gameLicenseId = Convert.ToInt32 (hidGameLicId.Value);

				DataRow drGameLicense = StoreUtility.GetGameLicense (gameLicenseId);

				// Is it an active commercial server?
				if (Convert.ToInt32 (drGameLicense ["license_type"]).Equals ((int) Constants.eLICENSE_SUBSCRIPTIONS.COMMERCIAL) && 
					Convert.ToInt32 (drGameLicense ["status_id"]).Equals ((int) Constants.eGAME_LICENSE_STATUS.ACTIVE))
				{
					// Notify IT to takedown server
					string ITEmail = "cbuchanan@kaneva.com";
					if (System.Configuration.ConfigurationManager.AppSettings ["ITEmail"] != null)
					{
                        ITEmail = System.Configuration.ConfigurationManager.AppSettings["ITEmail"].ToString();
					}
					MailUtility.SendEmail (KanevaGlobals.FromEmail, ITEmail, "Removed Commercial Server", "gameLicenseId = " + gameLicenseId + " is waiting on IT to take down, user deleted.", false); 

					// Mark as waiting on IT to takedown
					StoreUtility.UpdateITGameLicense (gameLicenseId ,assetId, GetUserId (), Constants.eGAME_LICENSE_STATUS.WAITING_ON_IT_TAKEDOWN);
				}
				else
				{
					// Delete the server
					StoreUtility.UpdateITGameLicense (gameLicenseId, assetId, GetUserId (), Constants.eGAME_LICENSE_STATUS.DELETED);
				}
			}

			// Rebind Servers
			BindData (pgTop.CurrentPageNumber, filStore.CurrentFilter);
		}

		private void pg_PageChange (object sender, PageChangeEventArgs e)
		{
			BindData (e.PageNumber, filStore.CurrentFilter);
		}

		private void filStore_FilterChanged(object sender, FilterChangedEventArgs e)
		{
			pgTop.CurrentPageNumber = 1;
			BindData (1, e.Filter);
		}

		private void BindData (int pageNumber, string filter)
		{
			// Set the sortable columns
			SetHeaderSortText (dgrdAsset);
			SetHeaderSortText (listAsset);
			string orderby = CurrentSort + " " + CurrentSortOrder;

			PagedDataTable pds = StoreUtility.GetItGameAssets (filter, orderby, pageNumber, filStore.NumberOfPages);
			pgTop.NumberOfPages = Math.Ceiling ((double) pds.TotalCount / filStore.NumberOfPages).ToString ();

			switch (filStore.CurrentView)
			{
				case "th":
				{
					dlAsset.Visible = true;
					dgrdAsset.Visible = false;
					listAsset.Visible = false;
					dlAsset.DataSource = pds;
					dlAsset.DataBind ();
					break;
				}

				case "li":
				{
					dlAsset.Visible = false;
					listAsset.Visible = false;
					dgrdAsset.Visible = true;
					dgrdAsset.DataSource = pds;
					dgrdAsset.DataBind ();
					break;
				}

				default: 
				{
					dlAsset.Visible = false;
					dgrdAsset.Visible = false;
					listAsset.Visible = true;
					listAsset.DataSource = pds;
					listAsset.DataBind ();
					listAsset.Columns[0].Visible = IsIT ();
					break;
				}

			}

			// The results
			lblSearch.Text = GetResultsText (pds.TotalCount, pageNumber, filStore.NumberOfPages, pds.Rows.Count);
		}

		/// <summary>
		/// They clicked to sort a column
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected override void btnSort_Click (object sender, System.EventArgs e)
		{
			base.btnSort_Click (sender, e);
			BindData (pgTop.CurrentPageNumber, filStore.CurrentFilter);
		}

		/// <summary>
		/// DEFAULT_SORT
		/// </summary>
		/// <returns></returns>
		protected override string DEFAULT_SORT
		{
			get
			{
				return "a.name";
			}
		}

		/// <summary>
		/// GetServerEditLink
		/// </summary>
		/// <param name="gameLicenseId"></param>
		/// <returns></returns>
		protected string GetServerEditLink (int gameLicenseId, int assetId)
		{
			return ResolveUrl ("~/Administration/it/ITServerEdit.aspx?gli=" + gameLicenseId + "&assetId=" + assetId);
		}

		/// <summary>
		/// Return the delete javascript
		/// </summary>
		/// <param name="TopicID"></param>
		/// <returns></returns>
		public string GetServerDeleteScript (int gameLicenseId, int assetId)
		{
			return "javascript:if (confirm(\"Are you sure you want to delete this server?\")){DeleteServer (" + gameLicenseId + "," + assetId + ")};";
		}

		/// <summary>
		/// Return the activate javascript
		/// </summary>
		public string GetServerActivateScript (int gameLicenseId, int assetId)
		{
			return "javascript:if (confirm(\"Are you sure you want to activate this server?\\nThis means the server has been set up and is ready for game play.\")){ActivateServer (" + gameLicenseId + "," + assetId + ")};";
		}

		/// <summary>
		/// CanActivate
		/// </summary>
		public bool CanActivate (int licenseType, int statusId)
		{
			return (licenseType.Equals ((int) Constants.eLICENSE_SUBSCRIPTIONS.COMMERCIAL) && statusId.Equals ((int) Constants.eGAME_LICENSE_STATUS.WAITING_ON_IT));	
		}

		/// <summary>
		/// Forum Categories databound
		/// </summary>
		/// <param name="Sender"></param>
		/// <param name="e"></param>
		protected void dgrdAsset_ItemDataBound (Object Sender, DataGridItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
			{
				// Get data for that year and region and create data set 
				Control obCtl = e.Item.FindControl ("rptGameServers");
				if (null != obCtl)
				{
					DataRowView drv = (DataRowView) e.Item.DataItem;
					int assetId = Convert.ToInt32 (drv ["asset_id"]);

					Repeater rptGameServers = (Repeater) obCtl;

					rptGameServers.DataSource = StoreUtility.GetGameLicenses (assetId, "");
					rptGameServers.DataBind ();

					HyperLink hlAddServer = (HyperLink) e.Item.FindControl ("hlAddServer");
					hlAddServer.NavigateUrl = GetServerEditLink (0, assetId);
				}
			}
		}

		protected Kaneva.Pager pgTop;
		protected Kaneva.StoreFilter filStore;
		protected DataGrid dgrdAsset;
		protected DataGrid listAsset;
		protected DataList dlAsset;

		protected HtmlInputHidden AssetId, hidGameLicId;

		protected Label lblSearch;

		protected HtmlTable tblNoAdmin;
		protected Panel pnlAdmin;

		protected Button btnDeleteServer, btnActivateServer;

		// Logger
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

		#region Web Form Designer generated code

		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
			pgTop.PageChanged +=new PageChangeEventHandler (pg_PageChange);
			filStore.FilterChanged	+=new FilterChangedEventHandler (filStore_FilterChanged);
			this.dgrdAsset.ItemDataBound +=new DataGridItemEventHandler (dgrdAsset_ItemDataBound);
		}
		#endregion

	}
}
