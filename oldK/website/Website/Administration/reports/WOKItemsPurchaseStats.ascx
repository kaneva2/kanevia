<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="WOKItemsPurchaseStats.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.WOKItemsPurchaseStats" %>
<%@ Register TagPrefix="Kaneva" TagName="DateControl"  Src="../../usercontrols/DateTimeControl.ascx" %>

<table runat="server" id="tblAccessDenied" cellpadding="0" cellspacing="0" border="0" width="560">
	<tr><td ALIGN="CENTER">
		<BR><BR><BR>
		<span class="subHead">You do not have the proper access rights to view this page.</span>
	</td></tr>
</table>

<asp:panel runat="server" ID="pnlAccGranted" >
    <table border="1" width="98%" cellpadding="0" cellspacing="0" style="background-color:white">
       <tr>
             <td><Kaneva:DateControl TimeFieldVisible="true" DateFieldVisible="true" SetDateLabel="Start Date" ID="uc_StartDate" runat="server" /></td>
             <td><Kaneva:DateControl TimeFieldVisible="true" DateFieldVisible="true" SetDateLabel="End Date" ID="uc_EndDate" runat="server" /></td>
             <td align="center" ><asp:Button ID="btn_Search" runat="server" Text="Search" onclick="btnSearch_Click" /></td>
       </tr>
       <tr>
             <td colspan="3"><asp:Label id="lbl_Messages" visible="false" runat="server"><span style="color:#ffffff; size:24pt; font-weight:bold"></span> </asp:Label></td>
        </tr>
       <tr>
             <td colspan="3"><br /><br />
                <asp:gridview id="grdvw_WOKPurchases" runat="server" onsorting="grdvw_WOKPurchases_Sorting" autogeneratecolumns="False" width="905px" OnPageIndexChanging="grdvw_WOKPurchases_PageIndexChanging" AllowSorting="True" border="0" cellpadding="2" AllowPaging="True" PageSize="500">
                    <RowStyle BackColor="White"></RowStyle>
                    <HeaderStyle BackColor="LightGray" Font-Underline="True" Font-Bold="True" ForeColor="Black" HorizontalAlign="Left"></HeaderStyle>
                    <AlternatingRowStyle BackColor="Gainsboro"></AlternatingRowStyle>
                    <FooterStyle BackColor="White"></FooterStyle>
                    <Columns>
                        <asp:BoundField HeaderText="Item ID" DataField="global_id" SortExpression="global_id" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
                        <asp:BoundField HeaderText="Name" DataField="name" SortExpression="name" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
                        <asp:BoundField HeaderText="Mark Cost" DataField="market_cost" SortExpression="market_cost" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
                        <asp:BoundField HeaderText="Quantity Sold (Total)" DataField="quantity" SortExpression="quantity" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
                        <asp:BoundField HeaderText="Quantity Sold (KPOINTS)" DataField="kpoint_quantity" SortExpression="kpoint_quantity" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
                        <asp:BoundField HeaderText="Quantity Sold (GPOINTS)" DataField="gpoint_quantity" SortExpression="gpoint_quantity" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
                    </Columns>
                </asp:gridview>
             </td>
        </tr>

    </table>
</asp:panel>
