<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="friendInvites.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.friendInvites" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../../usercontrols/Pager.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="ReportNav" Src="reportNav.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="AdminMenu" Src="../../usercontrols/AdministrativeMenu.ascx" %>

<link href="../../css/new.css" rel="stylesheet" type="text/css">
<script src="../../jscript/prototype.js" type="text/javascript"></script>
<script type="text/javascript">
function SetPopupLocation()
{
	return ((document.all?document.body.clientWidth:window.innerWidth)/2)+46; 
}
 </script>

<Kaneva:AdminMenu ID="adminmenu" runat="server" />
<Kaneva:ReportNav runat="server" id="rnNavigation" SubTab="fi"/>

<center>
<br/>	  

<div id="divExplain" style="position:absolute;top:190px;text-align:left;display:none;border:solid 1px black;background-color:white;z-order:10;width:400px;padding:10px;">
	<div style="padding:0 0 4px 0;">A x B x C = Viral Coefficient</div>
	<div style="margin:2px 0 2px 0;"><div style="float:left;display:block;height:20px;">A =</div><div> % of users inviting people<br />(# users who sent invites / # users who joined Kaneva)</div></div>
	<div style="margin:4px 0 4px 0;"><div style="float:left;display:block;height:20px;">B =</div><div> Avg. # people invited per user<br />(# invites sent / # users who sent invites)</div></div>
	<div style="margin:2px 0 2px 0;"><div style="float:left;display:block;height:20px;">C =</div><div> % people accepting the invite<br />(# accepts / # invites)</div></div>
	<div style="margin:8px 0 2px 0;">
		<div style="margin:2px 0 2px 0;">A = <span id="spnAFormula" runat="server"></span></div>
		<div style="margin:2px 0 2px 0;">B = <span id="spnBFormula" runat="server"></span></div>
		<div style="margin:2px 0 2px 0;">C = <span id="spnCFormula" runat="server"></span></div>
	</div>
	
	<div style="margin:6px 0 2px 0;">
		<span id="spnA" runat="server" style="font-weight:bold;"></span> x 
		<span id="spnB" runat="server" style="font-weight:bold;"></span> x 
		<span id="spnC" runat="server" style="font-weight:bold;"></span> = 
		<span id="spnABC" runat="server" style="font-weight:bold;"></span>
	</div>
	<span style="width:98%;text-align:right;"><a href="javascript:void(0);" onclick="javascript:$('divExplain').style.display='none';">close</a></span>
</div>

<table cellpadding="1" cellspacing="0" border="0" width="905" style="background-color:#ffffff">
	<tr>
		<td align="center">
			<br /><br />
		
			<table cellpadding="0" cellspacing="0" border="0" width="450">
				<tr>
					<td valign="middle" align="right" class="Filter2" width="25%" nowrap="true">
						Search from&nbsp;
					</td>
					<td valign="middle" align="left" class="Filter2" nowrap="true">
						<asp:TextBox ID="txtStartDate" class="Filter2" Width="100" MaxLength="10" runat="server"/> (MM-dd-yyyy) 12:00:00 AM
						<asp:RequiredFieldValidator ID="rfdtxtStartDate" ControlToValidate="txtStartDate" Text="* Start date is a required field." ErrorMessage="" Display="Dynamic" runat="server"/>
						<asp:RegularExpressionValidator id="revStartDate" ControlToValidate="txtStartDate" Text="* Start date must be in format mm-dd-yyyy." Display="Dynamic" ErrorMessage="" EnableClientScript="True" runat="server"/>
					</td>
				</tr>
				<tr>
					<td valign="middle" align="right" class="Filter2" nowrap="true">
						to&nbsp;
					</td>
					<td valign="middle" align="left" class="Filter2" nowrap="true">
						<asp:TextBox ID="txtEndDate" class="Filter2" Width="100" MaxLength="10" runat="server"/> (MM-dd-yyyy) 11:59:59 PM
						<asp:RequiredFieldValidator ID="rfdtxtEndDate" ControlToValidate="txtEndDate" Text="* End date is a required field." ErrorMessage="" Display="Dynamic" runat="server"/>
						<asp:RegularExpressionValidator id="revEndDate" ControlToValidate="txtEndDate" Text="* End date must be in format mm-dd-yyyy." Display="Dynamic" ErrorMessage="" EnableClientScript="True" runat="server"/>
					</td>
				</tr>
				<tr>
					<td colspan="2" align="right"><asp:button id="btnSearch" runat="Server" CausesValidation="True" onClick="btnSearch_Click" class="Filter2" Text=" search "/></td>
				</tr>
			</table>	
			
			<br/><br/>

			<table cellpadding="0" cellspacing="0" border="0" width="99%">
				<tr Class="lineItemColHead">
					<td width="100%" align="left">&nbsp;&nbsp;<span class="header02" style="font-size: 15px;"><b><asp:Label class="dateStamp" id="lblInvites" runat="server"/><b></b></span>
					</td>
				</tr>
			</table>	

			<table cellpadding="0" cellspacing="0" border="0" width="99%">
				<tr>
					<td style="padding-left:20px;width:70%;" valign="top" align="left">
						<div style="margin:10px 0 2px 0;"><span style="width:70px;">Sent:</span><span style="width:50px;" id="spnSentCount" runat="server"></span><span style="font-size:10px;margin-left:10px;">(Invites sent during date range)</span></div>
						<div style="margin:2px 0 2px 0;"><span style="width:70px;">Accepted:</span><span style="width:50px;" id="spnAcceptCount" runat="server"></span><span style="font-size:10px;margin-left:10px;">(Invitees joining during date range)</span></div>
						<div style="margin:2px 0 2px 0;"><span style="width:70px;">Declined:</span><span style="width:50px;" id="spnDeclineCount" runat="server"></span><span style="font-size:10px;margin-left:10px;">(Intites declined during date range)</span></div>
						<div style="margin:2px 0 10px 0;"><span style="width:70px;">Pending:</span><span style="width:50px;" id="spnPendCount" runat="server"></span><span style="font-size:10px;margin-left:10px;">(Invites sent during date range that are pending)</span>
							<div style="margin:5px 0 2px 20px;"><span style="width:50px;">Opens:</span><span style="width:50px;" id="spnOpenCount" runat="server"></span><span style="font-size:10px;margin-left:10px;">(Invites sent during date range that have been opened)</span></div>
							<div style="margin:2px 0 2px 20px;"><span style="width:50px;">Clicks:</span><span style="width:50px;" id="spnClickCount" runat="server"></span><span style="font-size:10px;margin-left:10px;">(Invites send during date range and users clicked join link)</span></div>
						</div>
					</td>
					<td valign="top" align="right">
						<div style="margin:10px 20px 2px 0;"><span style="width:130px;">Viral Coefficient:</span><span id="spnCoefficient" runat="server" style="width:40px;font-weight:bold;"></span></div>
						<a style="margin-right:20px;" href="javascript:void(0);" onclick="javascript:$('divExplain').style.left=SetPopupLocation();$('divExplain').style.display='block';">explain</a> 
					</td>
				</tr>
				<tr>
					<td colspan="2" align="right" class="belowFilter2" valign="middle" style="line-height:15px;">
						<asp:Label runat="server" id="lblSearch" CssClass="dateStamp"/><br><Kaneva:Pager runat="server" id="pgTop" onpagechanged="pg_PageChange"/>
					</td>
				</tr>
				<tr>
					 <td colspan="2" align="center">
						<asp:Label id="lblNoInvites" visible="false" runat="server">
							<span style="size:24pt; font-weight:bold">No Declined Friend Invites found for this date range.</span>
						</asp:Label>
					</td>
				</tr>
			</table>
			
          
			<asp:gridview id="dgInvites" runat="server"  
				onsorting="dgInvites_Sorting" 
				autogeneratecolumns="False" 
				width="98%" 
				AllowSorting="True" border="0" cellpadding="4" 
				AllowPaging="False" Align="center" 
				bordercolor="DarkGray" borderstyle="solid" borderwidth="1px">
				
				<HeaderStyle BackColor="LightGray" horizontalalign="left" Font-Underline="True" Font-Bold="True" ForeColor="#000000"></HeaderStyle>
				<RowStyle BackColor="White" horizontalalign="left"></RowStyle>
				<AlternatingRowStyle BackColor="Gainsboro"></AlternatingRowStyle>
				<FooterStyle BackColor="#FFffff"></FooterStyle>
				<Columns>
					<asp:BoundField HeaderText="Invite Id"  DataField="invite_id" ReadOnly="True" ConvertEmptyStringToNull="False" visible="false" ></asp:BoundField>
					<asp:BoundField HeaderText="Date Sent" DataField="invited_date" SortExpression="invited_date" dataformatstring="{0:d}" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
					<asp:BoundField HeaderText="Date Declined" DataField="decline_date" SortExpression="decline_date" dataformatstring="{0:d}" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
					<asp:BoundField HeaderText="Email" DataField="email" SortExpression="email" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
					<asp:BoundField HeaderText="Reason" DataField="name" SortExpression="name" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
					<asp:BoundField HeaderText="Comments" DataField="additional_information" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
				</Columns>
			</asp:gridview>
			         
			<br /><br />

		</td>
	</tr>
</table>

</center>
