///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Globalization;
using log4net;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.reports
{
	/// <summary>
	/// Summary description for platformDownloads.
	/// </summary>
    public class platformDownloads : NoBorderPage
	{
		protected platformDownloads () 
		{
			Title = "Admin - Platform Downloads";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			pnlAdmin.Visible = (IsAdministrator ());
			tblNoAdmin.Visible = (!IsAdministrator ());
			
			// Date validators
			revStartDate.ValidationExpression = Constants.VALIDATION_REGEX_DATE;
			revEndDate.ValidationExpression = Constants.VALIDATION_REGEX_DATE;

			if (!IsPostBack)
			{
				// Log the activity
                SiteMgmtFacade siteMgmtFacade = new SiteMgmtFacade();
                siteMgmtFacade.InsertCSRLog(GetUserId(), "Admin - Viewing Platform Downloads", 0, 0);

				txtStartDate.Text = KanevaGlobals.FormatDateNumbersOnly (KanevaGlobals.GetCurrentDateTime ());
				txtEndDate.Text = KanevaGlobals.FormatDateNumbersOnly (KanevaGlobals.GetCurrentDateTime ());
				BindData (1);
			}
		}

		/// <summary>
		/// Pager change event
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pg_PageChange (object sender, PageChangeEventArgs e)
		{
			BindData (e.PageNumber);
		}

		/// <summary>
		///  Bind the data
		/// </summary>
		/// <param name="pageNumber"></param>
		private void BindData (int pageNumber)
		{
			DateTime dtStartDate = KanevaGlobals.UnFormatDate (txtStartDate.Text + " 00:00:00");
			DateTime dtEndDate = KanevaGlobals.UnFormatDate (txtEndDate.Text + " 23:59:59");

			lblEarnings.Text = "Unique User Platform Download Report from '" + txtStartDate.Text + " 12:00 AM' to '" + txtEndDate.Text + " 11:59 PM' (Only count 1 per user)";
			lblAllPlatformDownloads.Text = "All Platform Downloads from '" + txtStartDate.Text + " 12:00 AM' to '" + txtEndDate.Text + " 11:59 PM'";

			// Set the sortable columns
			SetHeaderSortText (dgrdDownloads);
			string orderby = CurrentSort + " " + CurrentSortOrder;

			int itemsPerPage = 50;

			PagedDataTable pds = UsersUtility.GetEditorDownloads (dtStartDate, dtEndDate, orderby, pgTop.CurrentPageNumber, itemsPerPage);
			pgTop.NumberOfPages = Math.Ceiling ((double) pds.TotalCount / itemsPerPage).ToString ();
			pgTop.DrawControl ();

			dgrdDownloads.DataSource = pds;
			dgrdDownloads.DataBind ();

			// The results
			lblSearch.Text = GetResultsText (pds.TotalCount, pageNumber, itemsPerPage, pds.Rows.Count);

			// Get the asset id's of the editor's
			string editorAssetIds = "534, 616, 626";
			if (System.Configuration.ConfigurationManager.AppSettings ["EditorAssetIds"] != null)
			{
                editorAssetIds = System.Configuration.ConfigurationManager.AppSettings["EditorAssetIds"].ToString();
			}

			int exeDownloadCount = ReportUtility.GetNumberEditorDownloads (dtStartDate, "", dtEndDate);
			int torrentDownloadCount = 0;

			lblPlatformDownloads.Text = exeDownloadCount.ToString ();
			lblPlatformDownloadsTorrent.Text = torrentDownloadCount.ToString ();
			lblPlatformDownloadsTotal.Text = Convert.ToString (torrentDownloadCount + exeDownloadCount);
		}

		/// <summary>
		/// They clicked to sort a column
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected override void btnSort_Click (object sender, System.EventArgs e)
		{
			base.btnSort_Click (sender, e);
			BindData (pgTop.CurrentPageNumber);
		}

		/// <summary>
		/// DEFAULT_SORT
		/// </summary>
		/// <returns></returns>
		protected override string DEFAULT_SORT
		{
			get
			{
				return "downloaded_datetime";
			}
		}

		/// <summary>
		/// DEFAULT_SORT
		/// </summary>
		/// <returns></returns>
		protected override string DEFAULT_SORT_ORDER
		{
			get
			{
				return "desc";
			}
		}

		/// <summary>
		/// Search Event Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnSearch_Click (object sender, EventArgs e) 
		{
			BindData (1);
		}

		protected Label lblEarnings, lblAllPlatformDownloads;
		protected TextBox txtStartDate, txtEndDate;

		protected Kaneva.Pager pgTop;

		protected DataGrid dgrdDownloads;

		protected Label lblSearch;
		protected HtmlTable tblNoAdmin;
		protected Panel pnlAdmin;

		protected Label lblPlatformDownloads, lblPlatformDownloadsTorrent, lblPlatformDownloadsTotal;

		protected RegularExpressionValidator revStartDate, revEndDate;

		// Logger
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

		#region Web Form Designer generated code

		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
			pgTop.PageChanged +=new PageChangeEventHandler (pg_PageChange);
		}
		#endregion

	}
}