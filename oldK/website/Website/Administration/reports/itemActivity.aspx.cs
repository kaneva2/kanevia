///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for itemActivity.
	/// </summary>
    public class itemActivity : NoBorderPage
	{
		protected itemActivity () 
		{
			Title = "Admin - Item Activity";
		}

		private void Page_Load (object sender, System.EventArgs e)
		{
			pnlAdmin.Visible = (IsAdministrator ());
			tblNoAdmin.Visible = (!IsAdministrator ());

			// Date validators
			revStartDate.ValidationExpression = Constants.VALIDATION_REGEX_DATE;
			revEndDate.ValidationExpression = Constants.VALIDATION_REGEX_DATE;

			if (!IsPostBack)
			{
				txtStartDate.Text = KanevaGlobals.FormatDateNumbersOnly (KanevaGlobals.GetCurrentDateTime ());
				txtEndDate.Text = KanevaGlobals.FormatDateNumbersOnly (KanevaGlobals.GetCurrentDateTime ());

				BindData (1);
			}
		}

		/// <summary>
		/// Search Event Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnSearch_Click (object sender, EventArgs e) 
		{
			BindData (1);
		}

		/// <summary>
		/// Bind the data
		/// </summary>
		/// <param name="pageNumber"></param>
		private void BindData (int pageNumber)
		{
			lblEarnings.Text = "Activity Report from '" + txtStartDate.Text + " 12:00 AM' to '" + txtEndDate.Text + " 11:59 PM'";
			lblChannels.Text = "Item Report from '" + txtStartDate.Text + " 12:00 AM' to '" + txtEndDate.Text + " 11:59 PM'";

			// Log the activity
            SiteMgmtFacade siteMgmtFacade = new SiteMgmtFacade();
            siteMgmtFacade.InsertCSRLog(GetUserId(), "Admin - " + lblEarnings.Text, 0, 0);

			DateTime dtStartDate = KanevaGlobals.UnFormatDate (txtStartDate.Text + " 00:00:00");
			DateTime dtEndDate = KanevaGlobals.UnFormatDate (txtEndDate.Text + " 23:59:59");

			DataRow drAssetResults = ReportUtility.GetAssetActivity (dtStartDate, dtEndDate);
			lblAssetsCreated.Text = drAssetResults ["number_created"].ToString ();
			lblAssetsUpdated.Text = drAssetResults ["number_updated"].ToString ();
			lblAssetsDeleted.Text = drAssetResults ["number_deleted"].ToString ();

			// Channels
			SetHeaderSortText (dgrdItems);
			string orderby = CurrentSort + " " + CurrentSortOrder;

			int itemsPerPage = 50;
			PagedDataTable pds = ReportUtility.GetItems (dtStartDate, dtEndDate, orderby, pgTop.CurrentPageNumber, itemsPerPage);
			pgTop.NumberOfPages = Math.Ceiling ((double) pds.TotalCount / itemsPerPage).ToString ();
			pgTop.DrawControl ();

			dgrdItems.DataSource = pds;
			dgrdItems.DataBind ();

			// The results
			lblSearch.Text = GetResultsText (pds.TotalCount, pageNumber, itemsPerPage, pds.Rows.Count);
		}

		/// <summary>
		/// They clicked to sort a column
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected override void btnSort_Click (object sender, System.EventArgs e)
		{
			base.btnSort_Click (sender, e);
			BindData (pgTop.CurrentPageNumber);
		}

		/// <summary>
		/// Pager change event
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pg_PageChange (object sender, PageChangeEventArgs e)
		{
			BindData (e.PageNumber);
		}

		/// <summary>
		/// DEFAULT_SORT
		/// </summary>
		/// <returns></returns>
		protected override string DEFAULT_SORT
		{
			get
			{
				return "created_date";
			}
		}

		/// <summary>
		/// DEFAULT_SORT
		/// </summary>
		/// <returns></returns>
		protected override string DEFAULT_SORT_ORDER
		{
			get
			{
				return "DESC";
			}
		}

		/// <summary>
		/// Get the status link
		/// </summary>
		/// <param name="torrentStatus"></param>
		/// <returns></returns>
		protected string GetStatusLink (int torrentStatus)
		{
			return "javascript:window.open('" + ResolveUrl ("~/asset/publishStatus.aspx#" + torrentStatus + "','add','toolbar=no,width=600,height=450,menubar=no,scrollbars=yes,status=yes').focus();return false;");
		}

		/// <summary>
		/// IsDeleted
		/// </summary>
		protected bool IsDeleted (int statusId)
		{
			return (statusId.Equals ((int) Constants.eASSET_STATUS.MARKED_FOR_DELETION));
		}

		protected HtmlTable tblNoAdmin;
		protected Panel pnlAdmin;

		protected Label lblEarnings, lblChannels;
		protected TextBox txtStartDate, txtEndDate;
		protected RegularExpressionValidator revStartDate, revEndDate;

		protected Label lblAssetsCreated, lblAssetsUpdated, lblAssetsDeleted;

		// Channels
		protected Kaneva.Pager pgTop;
		protected DataGrid dgrdItems;
		protected Label lblSearch;

		#region Web Form Designer generated code

		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
			pgTop.PageChanged +=new PageChangeEventHandler (pg_PageChange);
		}
		#endregion
	}
}
