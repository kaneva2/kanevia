///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Globalization;
using log4net;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for sales.
	/// </summary>
    public class sales : NoBorderPage
	{
		protected sales () 
		{
			Title = "Admin - K-Point Sales";
		}

		private void Page_Load (object sender, System.EventArgs e)
		{
			pnlAdmin.Visible = (IsAdministrator ());
			tblNoAdmin.Visible = (!IsAdministrator ());

			// Date validators
			revStartDate.ValidationExpression = Constants.VALIDATION_REGEX_DATE;
			revEndDate.ValidationExpression = Constants.VALIDATION_REGEX_DATE;

			if (!IsPostBack)
			{
				txtStartDate.Text = KanevaGlobals.FormatDateNumbersOnly (KanevaGlobals.GetCurrentDateTime ());
				txtEndDate.Text = KanevaGlobals.FormatDateNumbersOnly (KanevaGlobals.GetCurrentDateTime ());
				BindData (1);
			}
		}

		/// <summary>
		/// Bind the data
		/// </summary>
		private void BindData (int pageNumber)
		{
			lblSales.Text = "Orders Report from '" + txtStartDate.Text + " 12:00 AM' to '" + txtEndDate.Text + " 11:59 PM'";

			// Log the activity
            SiteMgmtFacade siteMgmtFacade = new SiteMgmtFacade();
            siteMgmtFacade.InsertCSRLog(GetUserId(), "Admin - " + lblSales.Text + " page number " + pageNumber, 0, 0);

			DateTime dtStartDate = KanevaGlobals.UnFormatDate (txtStartDate.Text + " 00:00:00");
			DateTime dtEndDate = KanevaGlobals.UnFormatDate (txtEndDate.Text + " 23:59:59");

			// Set the sortable columns
			SetHeaderSortText (dgrdSales);
			string orderby = CurrentSort + " " + CurrentSortOrder;

			int itemsPerPage = 50;
			PagedDataTable pds = ReportUtility.GetKPointSales (dtStartDate, dtEndDate, orderby, pgTop.CurrentPageNumber, itemsPerPage);
			pgTop.NumberOfPages = Math.Ceiling ((double) pds.TotalCount / itemsPerPage).ToString ();
			pgTop.DrawControl ();

			dgrdSales.DataSource = pds;
			dgrdSales.DataBind ();

			// The results
			lblSearch.Text = GetResultsText (pds.TotalCount, pageNumber, itemsPerPage, pds.Rows.Count);
		}

		/// <summary>
		/// DEFAULT_SORT
		/// </summary>
		/// <returns></returns>
		protected override string DEFAULT_SORT
		{
			get
			{
				return "transaction_date";
			}
		}

		/// <summary>
		/// DEFAULT_SORT
		/// </summary>
		/// <returns></returns>
		protected override string DEFAULT_SORT_ORDER
		{
			get
			{
				return "DESC";
			}
		}

		/// <summary>
		/// Search Event Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnSearch_Click (object sender, EventArgs e) 
		{
			BindData (1);
		}

		/// <summary>
		/// They clicked to sort a column
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected override void btnSort_Click (object sender, System.EventArgs e)
		{
			base.btnSort_Click (sender, e);
			BindData (pgTop.CurrentPageNumber);
		}

		/// <summary>
		/// Pager change event
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pg_PageChange (object sender, PageChangeEventArgs e)
		{
			BindData (e.PageNumber);
		}

		protected TextBox txtStartDate, txtEndDate;
		protected RegularExpressionValidator revStartDate, revEndDate;

		protected HtmlTable tblNoAdmin;
		protected Panel pnlAdmin;

		// login grid
		protected DataGrid dgrdSales;
		protected Kaneva.Pager pgTop;
		protected Label lblSearch, lblSales;

		#region Web Form Designer generated code

		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
			pgTop.PageChanged +=new PageChangeEventHandler (pg_PageChange);
		}
		#endregion
	}
}
