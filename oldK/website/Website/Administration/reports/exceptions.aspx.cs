///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Globalization;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for exceptions.
	/// </summary>
    public class exceptions : NoBorderPage
	{
		protected exceptions () 
		{
			Title = "Admin - Exceptions";
		}

		private void Page_Load (object sender, System.EventArgs e)
		{
			pnlAdmin.Visible = (IsAdministrator ());
			tblNoAdmin.Visible = (!IsAdministrator ());

			// Date validators
			revStartDate.ValidationExpression = Constants.VALIDATION_REGEX_DATE;
			revEndDate.ValidationExpression = Constants.VALIDATION_REGEX_DATE;

			if (!IsPostBack)
			{
				lblEarnings.Text = "Exceptions Report (Click search to generate report...)";

				txtStartDate.Text = KanevaGlobals.FormatDateNumbersOnly (KanevaGlobals.GetCurrentDateTime ());
				txtEndDate.Text = KanevaGlobals.FormatDateNumbersOnly (KanevaGlobals.GetCurrentDateTime ());

				optReportAmount.Checked = true;
				txtTransactionsPerDay.Enabled = false;
				txtTransactionAmount.Text = "100.00";
				txtTransactionsPerDay.Text = "10";

				// Javascripts
				optReportAmount.Attributes.Add ("onclick", "javascript:document.frmMain." + txtTransactionAmount.ClientID + ".disabled=false;document.frmMain." + txtTransactionsPerDay.ClientID + ".disabled=true;");
				optReportPerDay.Attributes.Add ("onclick", "javascript:document.frmMain." + txtTransactionAmount.ClientID + ".disabled=true;document.frmMain." + txtTransactionsPerDay.ClientID + ".disabled=false;");
			}
			else
			{
				// Set values
				txtTransactionAmount.Enabled = optReportAmount.Checked;
				if (!txtTransactionAmount.Enabled)
				{
					txtTransactionAmount.Text = "100.00";
				}
				txtTransactionsPerDay.Enabled = optReportPerDay.Checked;
				if (!txtTransactionsPerDay.Enabled)
				{
					txtTransactionsPerDay.Text = "10";
				}
			}
		}


		/// <summary>
		/// Search Event Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnSearch_Click (object sender, EventArgs e) 
		{
			BindData ();
		}

		/// <summary>
		/// Bind the data
		/// </summary>
		/// <param name="pageNumber"></param>
		private void BindData ()
		{
			lblEarnings.Text = "Exceptions Report from '" + txtStartDate.Text + " 12:00 AM' to '" + txtEndDate.Text + " 11:59 PM'";

			// Log the activity
            SiteMgmtFacade siteMgmtFacade = new SiteMgmtFacade();
            siteMgmtFacade.InsertCSRLog(GetUserId(), "Admin - " + lblEarnings.Text, 0, 0);

			DateTime dtStartDate = KanevaGlobals.UnFormatDate (txtStartDate.Text + " 00:00:00");
			DateTime dtEndDate = KanevaGlobals.UnFormatDate (txtEndDate.Text + " 23:59:59");

			if (optReportAmount.Checked)
			{
				dgrdCreditTransactions.Visible = true;
				dgrdNumberTransactions.Visible = false;

				Double maxAmount = 0.0;
				try
				{
					maxAmount = Convert.ToDouble (txtTransactionAmount.Text);
				}
				catch (Exception) {};

				DataTable pds = ReportUtility.GetCreditTransactions (maxAmount, "ppt.amount_debited DESC", dtStartDate, dtEndDate);
				dgrdCreditTransactions.DataSource = pds;
				dgrdCreditTransactions.DataBind ();

				// The results
				lblSearch.Text = GetResultsText (pds.Rows.Count, 1, Int32.MaxValue, pds.Rows.Count);
			}
			else
			{
				dgrdCreditTransactions.Visible = false;
				dgrdNumberTransactions.Visible = true;

				int iNumberTransactions = 0;
				try
				{
					iNumberTransactions = Convert.ToInt32 (txtTransactionsPerDay.Text);
				}
				catch (Exception) {};

				DataTable dtTrans = ReportUtility.GetTransactions (iNumberTransactions, dtStartDate, dtEndDate);
				dgrdNumberTransactions.DataSource = dtTrans;
				dgrdNumberTransactions.DataBind ();

				// The results
				lblSearch.Text = GetResultsText (dtTrans.Rows.Count, 1, Int32.MaxValue, dtTrans.Rows.Count);
			}
		}

		/// <summary>
		/// GetStatus
		/// </summary>
		/// <returns></returns>
		protected string GetStatus (int transactionStatus, string statusDescription, DateTime transactionDate)
		{
			if (transactionStatus.Equals ((int) Constants.eTRANSACTION_STATUS.WAITING_VERIFICATION))
			{
				// If it is more than a day old, show it as expired
				DateTime dtCurrentTime = KanevaGlobals.GetCurrentDateTime ();
				TimeSpan tsDifference;

				if (!transactionDate.Equals (DBNull.Value))
				{
					tsDifference = dtCurrentTime - (DateTime) transactionDate;
					if (tsDifference.Days > 1)
					{
						return "<span style=\"color=red\">Expired</span>";
					}
				}
			}

			return statusDescription;
		}

		/// <summary>
		/// GetStatusLink
		/// </summary>
		/// <returns></returns>
		protected string GetStatusLink (int transactionStatus, string statusDescription, DateTime transactionDate)
		{
			if (transactionStatus.Equals ((int) Constants.eTRANSACTION_STATUS.WAITING_VERIFICATION))
			{
				// If it is more than a day old, show it as expired
				DateTime dtCurrentTime = KanevaGlobals.GetCurrentDateTime ();
				TimeSpan tsDifference;

				if (!transactionDate.Equals (DBNull.Value))
				{
					tsDifference = dtCurrentTime - (DateTime) transactionDate;
					if (tsDifference.Days > 1)
					{
						transactionStatus = (int) Constants.eTRANSACTION_STATUS.EXPIRED;
					}
				}
			}

			return "javascript:window.open('" + ResolveUrl ("~/mykaneva/transactionStatus.aspx#" + transactionStatus + "','add','toolbar=no,width=600,height=300,menubar=no,scrollbars=yes,status=yes').focus();");
		}

		protected HtmlInputRadioButton optReportAmount, optReportPerDay;

		protected TextBox txtTransactionAmount, txtTransactionsPerDay;

		protected TextBox txtStartDate, txtEndDate;
		protected RegularExpressionValidator revStartDate, revEndDate;

		protected HtmlTable tblNoAdmin;
		protected Panel pnlAdmin;

		protected Label lblSearch, lblEarnings;

		protected DataGrid dgrdCreditTransactions, dgrdNumberTransactions;

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
