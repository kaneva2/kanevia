///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Globalization;
using MagicAjax;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for KPI.
	/// </summary>
    public class KPIMedia : NoBorderPage
	{
		
		/// <summary>
		/// This is the day we started taking money.
		/// </summary>
		public string startOfTime = "2007-06-26";

		protected KPIMedia () 
		{
			Title = "Admin - Key Performance Indicators";
		}

		private void Page_Load (object sender, System.EventArgs e)
		{
			tblNoAdmin.Visible = (!IsAdministrator ());

			// Date validators
			revStartDate.ValidationExpression = Constants.VALIDATION_REGEX_DATE;
			revEndDate.ValidationExpression = Constants.VALIDATION_REGEX_DATE;

			if (!IsPostBack)
			{
				lblEarnings.Text = "Download Report (Click search to generate report...)";
				txtStartDate.Text = KanevaGlobals.FormatDateNumbersOnly (KanevaGlobals.GetCurrentDateTime ());
				txtEndDate.Text = KanevaGlobals.FormatDateNumbersOnly (KanevaGlobals.GetCurrentDateTime ());
			}
            btnSearch7.Text = DateTime.Today.AddDays(-7).ToString("MM/dd");
            btnSearch6.Text = DateTime.Today.AddDays(-6).ToString("MM/dd");
            btnSearch5.Text = DateTime.Today.AddDays(-5).ToString("MM/dd");
            btnSearch4.Text = DateTime.Today.AddDays(-4).ToString("MM/dd");
            btnSearch3.Text = DateTime.Today.AddDays(-3).ToString("MM/dd");
            btnSearch2.Text = DateTime.Today.AddDays(-2).ToString("MM/dd");
            btnSearch1.Text = DateTime.Today.AddDays(-1).ToString("MM/dd");
            
		}


		#region Helper Methods
		/// <summary>
		/// Bind the data
		/// </summary>
      private void BindData()
      {
        Trace.Write("Test Category 1", "1");

        pnlAdmin.Visible = IsAdministrator();

        lblEarnings.Text = "KPI Report from '" + txtStartDate.Text + " 12:00 AM' to '" + txtEndDate.Text + " 11:59 PM'";

        // Log the activity
        SiteMgmtFacade siteMgmtFacade = new SiteMgmtFacade();
        siteMgmtFacade.InsertCSRLog(GetUserId(), "Admin - " + lblEarnings.Text, 0, 0);

        DateTime dtStartDate = KanevaGlobals.UnFormatDate(txtStartDate.Text + " 00:00:00");
        DateTime dtEndDate = KanevaGlobals.UnFormatDate(txtEndDate.Text + " 23:59:59");

        string memberChannels = UsersUtility.GetUsers(dtStartDate, dtEndDate, "", "", 1, 1).TotalCount.ToString();

        string memberChannelsActivated = UsersUtility.GetUsers(dtStartDate, dtEndDate, "u.status_id = 1", "", 1, 1).TotalCount.ToString();

        Trace.Write("Test Category 1", "1.5");


        // Media
        int numberUploads = ReportUtility.GetContentPublished(dtStartDate, dtEndDate, "", "", 1, 1).TotalCount;
        lblConnectedMedia.Text = FormatNumber(ReportUtility.GetConnectedMedia(dtStartDate, dtEndDate) - numberUploads);
        lblMediaUploaded.Text = FormatNumber(numberUploads);
        lblVideoUploaded.Text = FormatNumber(ReportUtility.GetContentPublished(dtStartDate, dtEndDate, "a.asset_type_id = " + (int)Constants.eASSET_TYPE.VIDEO + " AND a.asset_sub_type_id = 0", "", 1, 1).TotalCount);
        lblYouTubeUploaded.Text = FormatNumber(ReportUtility.GetContentPublished(dtStartDate, dtEndDate, "a.asset_type_id = " + (int)Constants.eASSET_TYPE.VIDEO + " AND a.asset_sub_type_id = 1", "", 1, 1).TotalCount);
        lblMusicUploaded.Text = FormatNumber(ReportUtility.GetContentPublished(dtStartDate, dtEndDate, "a.asset_type_id = " + (int)Constants.eASSET_TYPE.MUSIC, "", 1, 1).TotalCount);
        lblPhotoUploaded.Text = FormatNumber(ReportUtility.GetContentPublished(dtStartDate, dtEndDate, "a.asset_type_id = " + (int)Constants.eASSET_TYPE.PICTURE, "", 1, 1).TotalCount);
        lblPatternUploaded.Text = FormatNumber(ReportUtility.GetContentPublished(dtStartDate, dtEndDate, "a.asset_type_id = " + (int)Constants.eASSET_TYPE.PATTERN, "", 1, 1).TotalCount);
        lblGameUploaded.Text = FormatNumber(ReportUtility.GetContentPublished(dtStartDate, dtEndDate, "a.asset_type_id = " + (int)Constants.eASSET_TYPE.GAME, "", 1, 1).TotalCount);
        lblTVUploaded.Text = FormatNumber(ReportUtility.GetContentPublished(dtStartDate, dtEndDate, "a.asset_type_id = " + (int)Constants.eASSET_TYPE.TV, "", 1, 1).TotalCount);
        lblWidgetsUploaded.Text = FormatNumber(ReportUtility.GetContentPublished(dtStartDate, dtEndDate, "a.asset_type_id = " + (int)Constants.eASSET_TYPE.WIDGET, "", 1, 1).TotalCount);

        Trace.Write("Test Category 1", "4");
      }



		public string RoundValue (Object obj, int dec_places)
		{
			return (System.Math.Round(Convert.ToDecimal(obj), dec_places)).ToString ();
		}
		private string FormatNumber (Object obj)
		{
			return FormatNumber (Convert.ToDouble (obj), false);
		}
		private string FormatNumber (Double dblNum)
		{
			return FormatNumber (dblNum, false);
		}
		private string FormatNumber (Double dblNum, bool includeDecimal)
		{
			if (includeDecimal)
			{
				return (dblNum).ToString ("#,##0.00");
			}
			
			return (dblNum).ToString ("#,##0");
		}

		#endregion

        #region Event Handlers
        /// <summary>
        /// Search Event Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindData();
        }
        protected void btnSearch1_Click(object sender, EventArgs e)
        {
            txtStartDate.Text = DateTime.Now.Date.AddDays(-1).Date.ToString("MM-dd-yy");
            txtEndDate.Text = DateTime.Now.Date.AddDays(-1).Date.ToString("MM-dd-yy");
            BindData();
        }
        protected void btnSearch2_Click(object sender, EventArgs e)
        {
            txtStartDate.Text = DateTime.Now.Date.AddDays(-2).Date.ToString("MM-dd-yy");
            txtEndDate.Text = DateTime.Now.Date.AddDays(-2).Date.ToString("MM-dd-yy");
            BindData();
        }
        protected void btnSearch3_Click(object sender, EventArgs e)
        {
            txtStartDate.Text = DateTime.Now.Date.AddDays(-3).Date.ToString("MM-dd-yy");
            txtEndDate.Text = DateTime.Now.Date.AddDays(-3).Date.ToString("MM-dd-yy");
            BindData();
        }
        protected void btnSearch4_Click(object sender, EventArgs e)
        {
            txtStartDate.Text = DateTime.Now.Date.AddDays(-4).Date.ToString("MM-dd-yy");
            txtEndDate.Text = DateTime.Now.Date.AddDays(-4).Date.ToString("MM-dd-yy");
            BindData();
        }
        protected void btnSearch5_Click(object sender, EventArgs e)
        {
            txtStartDate.Text = DateTime.Now.Date.AddDays(-5).Date.ToString("MM-dd-yy");
            txtEndDate.Text = DateTime.Now.Date.AddDays(-5).Date.ToString("MM-dd-yy");
            BindData();
        }
        protected void btnSearch6_Click(object sender, EventArgs e)
        {
            txtStartDate.Text = DateTime.Now.Date.AddDays(-6).Date.ToString("MM-dd-yy");
            txtEndDate.Text = DateTime.Now.Date.AddDays(-6).Date.ToString("MM-dd-yy");
            BindData();
        }
        protected void btnSearch7_Click(object sender, EventArgs e)
        {
            txtStartDate.Text = DateTime.Now.Date.AddDays(-7).Date.ToString("MM-dd-yy");
            txtEndDate.Text = DateTime.Now.Date.AddDays(-7).Date.ToString("MM-dd-yy");
            BindData();
        }
      

        #endregion

		#region Properties

		/// <summary>
		/// DEFAULT_SORT
		/// </summary>
		/// <returns></returns>
		protected override string DEFAULT_SORT
		{
			get
			{
				return "purchase_date";
			}
		}

		/// <summary>
		/// DEFAULT_SORT
		/// </summary>
		/// <returns></returns>
		protected override string DEFAULT_SORT_ORDER
		{
			get
			{
				return "DESC";
			}
		}

		
		#endregion

		#region Declarations

        protected LinkButton lbGetIncomeBreakdownK, lbGetIncomeBreakdownG, lbGetSpendingBreakdownK, lbGetSpendingBreakdownG;
        protected HtmlTable tblNoAdmin;
		protected Panel pnlAdmin;

		protected Label lblEarnings;
		protected TextBox txtStartDate, txtEndDate;
		protected RegularExpressionValidator revStartDate, revEndDate;

		// Users
		protected Label lblMembers, lblUsers, lblMemberContribs, lblActivatedEmails;
		protected Label lblWOKUsersTotal, lblAvgMinInWorld, lblAvgMinInWorldRange, lblAvgDollarsSpent;
		protected Label	lblMalePlayers, lblFemalePlayers;
		protected Label lblActiveWOKMembers, lblActiveWOKSpenders, lblInactiveWOKMembers, lblInactiveWOKSpenders, lblSemiActiveWokMembers;
		protected Label lblAvatarsCreated;

		// Channels
		protected Label lblBroadband, lblNewBroadband, lblMemberChannels;

		// Viral
		protected Label lblShares, lblRaves, lblBlogActivity, lblComments, lblFriendRequests, lblFriendsAccepted, lblDownloads;

		// Media
		protected Label lblConnectedMedia, lblMediaUploaded, lblVideoUploaded, lblYouTubeUploaded, lblMusicUploaded, lblPhotoUploaded, lblPatternUploaded, lblGameUploaded;

		// WOK Status
		protected Label lblLogins, lblUniqueLogins, lblFirstTimeLogins;
		protected Label lblNewHomes, lblNewHangouts, lblAvgHourlyConcurrentUsers, lblPeakConcurrentUsers;
		protected DataList dlAvgConcUsersPerHour;
        
        //Not used anymore 2007-09-24
        //protected Label lblAppDownloadAttempts;


		//WOK Economy
		protected Label lblCreditsTotal, lblCreditsAdded, lblRewardCreditsTotal, lblCreditsPurchased, lblCreditsSpent, lblCreditPrice, lblCreditPriceAllTime;
		protected Label lblNumPayers, lblTotalNumPayers, lblFirstTimePayers, lblTotalRevenue, lblNumberTransactions, lblNumberTransactionsAll;
        protected Label lblFurniturePlaced, lblClothingPurchased, lblAccessPasses, lblAccessPassOnly, lblWidgetsUploaded, lblTVUploaded;
		protected DataList dlGetSpendingBreakdownK, dlGetSpendingBreakdownG;
		protected DataList dlGetIncomeBreakdownK, dlGetIncomeBreakdownG;
        protected Repeater rptPayersByAmount, rptTransactionsByPayers, rptGiftCardRedemptions, rptRevenueBreakdown;
		protected Label lblNumberTransactions30Mav, lblValueTransaction30Mav, lblValuePayer30Mav, lblTransPayer30Mav, lblTransPayer, lblValueTrans;

        protected Label lblGCsold10, lblGCredeemed10, lblGCsold25, lblGCredeemed25;
        protected Repeater dlGiftCardsByState;

		protected RequiredFieldValidator rfdtxtStartDate, rfdtxtEndDate;
		protected Label		lblRewardCreditsAdded, lblRewardCreditsSpent, lblAvgActiveWokMembers;
        protected Label lblAvgActiveWOK, lblAvgActiveWOK5, lblAvgActiveWOK2, lblAvgActiveWOK1, lblAvgInactiveWOK, lblAvgActiveSpend, lblAvgInactiveSpend;
		protected System.Web.UI.HtmlControls.HtmlImage Img2;
		protected System.Web.UI.HtmlControls.HtmlImage Img3;
		protected System.Web.UI.HtmlControls.HtmlImage Img4;
		protected System.Web.UI.HtmlControls.HtmlImage Img49;
		protected System.Web.UI.HtmlControls.HtmlImage Img8;
		protected System.Web.UI.HtmlControls.HtmlImage Img14;
		protected System.Web.UI.HtmlControls.HtmlImage Img26;
		protected System.Web.UI.HtmlControls.HtmlImage Img25;
		protected System.Web.UI.HtmlControls.HtmlImage Img27;
		protected System.Web.UI.HtmlControls.HtmlImage Img5;
		protected System.Web.UI.HtmlControls.HtmlImage Img28;
		protected System.Web.UI.HtmlControls.HtmlImage Img29;
		protected System.Web.UI.HtmlControls.HtmlImage Img6;
		protected System.Web.UI.HtmlControls.HtmlImage Img7;
		protected System.Web.UI.HtmlControls.HtmlImage Img9;
		protected System.Web.UI.HtmlControls.HtmlImage Img19;
		protected System.Web.UI.HtmlControls.HtmlImage Img20;
		protected System.Web.UI.HtmlControls.HtmlImage Img10;
		protected System.Web.UI.HtmlControls.HtmlImage Img1;
		protected System.Web.UI.HtmlControls.HtmlImage Img18;
		protected System.Web.UI.HtmlControls.HtmlImage Img12;
		protected System.Web.UI.HtmlControls.HtmlImage Img13;
		protected System.Web.UI.HtmlControls.HtmlImage Img11;
		protected System.Web.UI.HtmlControls.HtmlImage Img21;
		protected System.Web.UI.HtmlControls.HtmlImage Img16;
		protected System.Web.UI.HtmlControls.HtmlImage Img22;
		protected System.Web.UI.HtmlControls.HtmlImage Img15;
		protected System.Web.UI.HtmlControls.HtmlImage Img23;
		protected System.Web.UI.HtmlControls.HtmlImage Img17;
		protected System.Web.UI.HtmlControls.HtmlImage Img24;
		protected Button	btnSearch, btnSearch1, btnSearch2, btnSearch3, btnSearch4, btnSearch5, btnSearch6, btnSearch7;

		#endregion

		#region Web Form Designer generated code

		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
