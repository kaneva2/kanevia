<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="KPINav.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.Administration.reports.KPI2.KPINav" %>
<style type="text/css">
 #kpinav a, #kpinav a:visited, #kpinav a:hover,  #kpinav a:active {
  color: #018AAA;
  font-size: 12px;
  font-color: blue;
  font-weight: normal;
  text-decoration: underline;
  background-color: white;
 }
</style>
<div id="kpinav" style="background-color:White;width:756px;">
  <a href="~/administration/reports/KPI.aspx" runat="server"  class="kpiFull">Full Report</a> |
  <a href="~/administration/reports/kpi2/KPIWokStatus.aspx" runat="server"  class="kpinavWokstatus">WoK Status</a> |
  <a href="~/administration/reports/kpi2/KPIWokEconomy.aspx" runat="server" class="kpinavWokeconomy">WoK Economy</a> |
  <a href="~/administration/reports/kpi2/KPIMembers.aspx" runat="server"    class="kpinavMembers">Members</a> |
  <a href="~/administration/reports/kpi2/KPICommunity.aspx" runat="server"  class="kpinavCommunity">Community</a> |
  <a href="~/administration/reports/kpi2/KPIMedia.aspx" runat="server"      class="kpinavMedia">Media</a>
</div>