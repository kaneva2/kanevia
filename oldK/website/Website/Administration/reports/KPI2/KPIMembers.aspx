<%@ Register TagPrefix="Kaneva" TagName="AdminMenu" Src="../../../usercontrols/AdministrativeMenu.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="ReportNav" Src="../reportNav.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../../../usercontrols/Pager.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="KPINav" Src="KPINav.ascx" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>
<%@ Page language="c#" Codebehind="KPIMembers.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.KPIMembers" %>


<script language="javascript">
  function toggleDiv(divid){
    if(document.getElementById(divid).style.display == 'none'){
      document.getElementById(divid).style.display = 'block';
    }else{
      document.getElementById(divid).style.display = 'none';
    }
  }
</script>



<LINK href="../../../css/home.css" type="text/css" rel="stylesheet">
<LINK href="../../../css/kanevaSC.css" type="text/css" rel="stylesheet">
<style type="text/css">
.hlRow {
	CURSOR: hand;
}

a.info{
    position:relative; /*this is the key*/
    z-index:24;
    
    text-decoration:none}

a.info:hover{z-index:25;}

a.info span{display: none}

a.info:hover span{ /*the span will display just on :hover state*/
    display:block;
    position:absolute;
    top:2em; left:2em; width:30em;
    padding:4px;
    border:1px solid #000000;
    background-color:#ffffff; color:#000;
    text-align: left;
    }
    

</style>
    <Kaneva:AdminMenu ID="adminmenu" runat="server" />
		<table id="tblNoAdmin" cellSpacing="0" cellPadding="0" width="560" border="0" runat="server">
			<tr>
				<td align="center"><BR>
					<BR>
					<BR>
					<span class="subHead">You must be a site administrator to access this section.</span>
				</td>
			</tr>
		</table>
		<KANEVA:REPORTNAV id="rnNavigation" runat="server" SubTab="kpi"></KANEVA:REPORTNAV>
		<style type="text/css">.kpinavMembers{  font-weight:bold !important;  }</style>
		<kaneva:KPINav id="kpiNav" runat="server"></kaneva:KPINav>
		<table width="auto" style="background-color:#FFFFFF;"><tr><td><BR>
			<table cellSpacing="0" cellPadding="0" width="750" border="0" align="center">
				<tr>
					<td class="Filter2" vAlign="middle" noWrap align="right" width="25%">Search 
						from&nbsp;
					</td>
					<td class="Filter2" vAlign="middle" noWrap align="left" width="25%"><asp:textbox class="Filter2" id="txtStartDate" runat="server" MaxLength="10" Width="100"></asp:textbox>(MM-dd-yyyy) 
						12:00:00 AM
						<asp:requiredfieldvalidator id="rfdtxtStartDate" Text="*" runat="server" Display="Dynamic" ErrorMessage="Start date is a required field."
							ControlToValidate="txtStartDate"></asp:requiredfieldvalidator><asp:regularexpressionvalidator id="revStartDate" Text="*" runat="server" Display="Static" ErrorMessage="Start date must be in format mm-dd-yyyy."
							ControlToValidate="txtStartDate" EnableClientScript="True"></asp:regularexpressionvalidator></td>
					<td></td>
					<td>
					<asp:button class="Filter2" id="btnSearch" onclick="btnSearch_Click" Text="search" runat="Server" CausesValidation="True"></asp:button> 					    					
					</td>
				</tr>
				<tr>
					<td class="Filter2" vAlign="middle" noWrap align="right">to&nbsp;
					</td>
					<td class="Filter2" vAlign="middle" noWrap align="left"><asp:textbox class="Filter2" id="txtEndDate" runat="server" MaxLength="10" Width="100"></asp:textbox>(MM-dd-yyyy) 
						11:59:59 PM
						<asp:requiredfieldvalidator id="rfdtxtEndDate" Text="*" runat="server" Display="Dynamic" ErrorMessage="End date is a required field."
							ControlToValidate="txtEndDate"></asp:requiredfieldvalidator><asp:regularexpressionvalidator id="revEndDate" Text="*" runat="server" Display="Static" ErrorMessage="End date must be in format mm-dd-yyyy."
							ControlToValidate="txtEndDate" EnableClientScript="True"></asp:regularexpressionvalidator></td>
					<td vAlign="middle" align="center" width="10%"></td>
					<td vAlign="middle" align="left" width="40%">
					    
					    <asp:button class="Filter2" id="btnSearch7" onclick="btnSearch7_Click" Text="search" runat="Server" CausesValidation="True"></asp:button>
					    <asp:button class="Filter2" id="btnSearch6" onclick="btnSearch6_Click" Text="search" runat="Server" CausesValidation="True"></asp:button>
					    <asp:button class="Filter2" id="btnSearch5" onclick="btnSearch5_Click" Text="search" runat="Server" CausesValidation="True"></asp:button>					    					    
					    <asp:button class="Filter2" id="btnSearch4" onclick="btnSearch4_Click" Text="search" runat="Server" CausesValidation="True"></asp:button>
					    <asp:button class="Filter2" id="btnSearch3" onclick="btnSearch3_Click" Text="search" runat="Server" CausesValidation="True"></asp:button>
					    <asp:button class="Filter2" id="btnSearch2" onclick="btnSearch2_Click" Text="search" runat="Server" CausesValidation="True"></asp:button>
					    <asp:button class="Filter2" id="btnSearch1" onclick="btnSearch1_Click" Text="search" runat="Server" CausesValidation="True"></asp:button> 
					    					    					    
					    </td>
				</tr>
			</table>
			<asp:panel id="pnlAdmin" runat="server" Visible="False">
				<BR>
				<table cellSpacing="0" cellPadding="0" width="905" border="0" runat="server">
					<tr class="lineItemColHead">
						<td align="left" width="100%">&nbsp;&nbsp;<b><asp:label class="dateStamp" id="lblEarnings" runat="server"></asp:label></b>
						</td>
					</tr>
				</table>
				<table class="FullBorders" cellSpacing="0" cellPadding="3" width="905" border="0">

					
					
					<tr class="sectionTitle" bgColor="#999999">
						<td class="bodyText" align="left" colSpan="3">Members</td>
					</tr>
					<tr>
						<td class="bodyText" align="right" width="348">Site Members (All Time):</td>
						<td><IMG src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblMembers" runat="server"></asp:label>&nbsp;
						</td>
					</tr>
					<tr class="lineItemOdd">
						<td class="bodyText" align="right" width="348">New Site Members:</td>
						<td><IMG src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left">
							<asp:label class="dateStamp" id="lblUsers" runat="server"></asp:label>&nbsp;
							(Of these <asp:label class="dateStamp" id="lblActivatedEmails" runat="server"></asp:label> have been activated)
						
						</td>
					</tr>

					<tr>
						<td class="bodyText" align="right" width="348">Member Contributors:</td>
						<td><IMG src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblMemberContribs" runat="server"></asp:label>&nbsp;
						</td>
					</tr>
					<tr class="lineItemOdd"> <!-- New -->
						<td class="bodyText" align="right" width="348"><b>Total Number of WOK Users (All Time):</b></td>
						<td><IMG id="Img10" src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblWOKUsersTotal" runat="server"></asp:label>&nbsp;
						</td>
					</tr>
					<tr>
						<td class="bodyText" align="right" width="348">Total Male Players (All Time):</td>
						<td><IMG id="Img1" src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblMalePlayers" runat="server"></asp:label>&nbsp;
						</td>
					</tr>
					<tr class="lineItemOdd">
						<td class="bodyText" align="right" width="348">Total Female Players (All Time):</td>
						<td><IMG id="Img18" src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblFemalePlayers" runat="server"></asp:label>&nbsp;
						</td>
					</tr>
					<tr>
						<td class="bodyText" align="right" width="348">Avg. Minutes In-World per Player 
							(All Time):</td>
						<td><IMG src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblAvgMinInWorld" runat="server"></asp:label>&nbsp;
						</td>
					</tr>
					<tr class="lineItemOdd">
						<td class="bodyText" align="right" width="348">Avg. Dollars Spent per Member:</td>
						<td><IMG id="Img12" src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblAvgDollarsSpent" runat="server"></asp:label>&nbsp;
						</td>
					</tr>
					<tr>
						<td class="bodyText" align="right" width="348">Active WOK Members (=/+10hrs) (All 
							Time):</td>
						<td><IMG id="Img13" src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblActiveWOKMembers" runat="server"></asp:label>&nbsp;
						</td>
					</tr>
					<TR class="lineItemOdd">
						<TD class="bodyText" align="right" width="348">Active WOK Members (=/+ x hrs) (Logged within 30 days):</TD>
						<TD><IMG id="Img11" src="~/images/spacer.gif" width="5" runat="server"></TD>
						<TD class="bodyText" align="left">
							<asp:label class="dateStamp" id="lblAvgActiveWOK" runat="server"></asp:label> 10hrs, 
							<asp:label class="dateStamp" id="lblAvgActiveWOK5" runat="server"></asp:label> 5hrs, 
							<asp:label class="dateStamp" id="lblAvgActiveWOK2" runat="server"></asp:label> 2hrs,
							<asp:label class="dateStamp" id="lblAvgActiveWOK1" runat="server"></asp:label> 1hrs 
							
							<a href="#" class="info">[?]<span>Number of users that have spent 10 hours or more online within the last 30 days. Note: Specified dates are 
							not used, this is from the current date.</span></a>
							</TD>
					</TR>
					<tr class="lineItemOdd">
						<td class="bodyText" align="right" width="348">Semi-Active WOK Members (2-10hrs) 
							(All Time):</td>
						<td><IMG id="Img21" src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblSemiActiveWokMembers" runat="server"></asp:label>&nbsp;
						</td>
					</tr>
					<tr>
						<td class="bodyText" align="right" width="348">Inactive WOK Members (&lt;2hrs) (All 
							Time):</td>
						<td><IMG id="Img16" src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblInactiveWOKMembers" runat="server"></asp:label>&nbsp;
						</td>
					</tr>
					<TR class="lineItemOdd">
						<TD class="bodyText" align="right" width="348">Avg. Inactive WOK Members (&lt;2hrs) 
							(All Time):</TD>
						<TD><IMG id="Img22" src="~/images/spacer.gif" width="5" runat="server"></TD>
						<TD class="bodyText" align="left">
							<asp:label class="dateStamp" id="lblAvgInactiveWOK" runat="server"></asp:label></TD>
					</TR>
					<tr class="lineItemOdd">
						<td class="bodyText" align="right" width="348">Active WOK Spenders (=/+$9.99) (All 
							Time):</td>
						<td><IMG id="Img15" src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblActiveWOKSpenders" runat="server"></asp:label>&nbsp;
						</td>
					</tr>
					<TR class="lineItemOdd">
						<TD class="bodyText" align="right" width="348">Active WOK Spenders (=/+$9.99) 
							(30 days):</TD>
						<TD><IMG id="Img23" src="~/images/spacer.gif" width="5" runat="server"></TD>
						<TD class="bodyText" align="left">
							<asp:label class="dateStamp" id="lblAvgActiveSpend" runat="server"></asp:label> 
							<a href="#" class="info">[?]<span>Number of users that have spent $9.99 or more within the 30 days proceeding the specified end date.</span></a>							
							</TD>
					</TR>
					<tr>
						<td class="bodyText" align="right" width="348">Inactive WOK Spenders (&lt;$10) (All 
							Time):</td>
						<td><IMG id="Img17" src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblInactiveWOKSpenders" runat="server"></asp:label>&nbsp;
						</td>
					</tr>
					<TR class="lineItemOdd">
						<TD class="bodyText" align="right" width="348">Avg. Inactive WOK Spenders 
							(=/+$9.99) (30 days):</TD>
						<TD><IMG id="Img24" src="~/images/spacer.gif" width="5" runat="server"></TD>
						<TD class="bodyText" align="left">
							<asp:label class="dateStamp" id="lblAvgInactiveSpend" runat="server"></asp:label></TD>
					</TR>


				</table>
				<br>
				<br></asp:panel>
			</td></tr></table>
