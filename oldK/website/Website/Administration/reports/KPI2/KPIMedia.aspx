<%@ Register TagPrefix="Kaneva" TagName="AdminMenu" Src="../../../usercontrols/AdministrativeMenu.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="ReportNav" Src="../reportNav.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../../../usercontrols/Pager.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="KPINav" Src="KPINav.ascx" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>
<%@ Page language="c#" Codebehind="KPIMedia.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.KPIMedia" %>


<script language="javascript">
  function toggleDiv(divid){
    if(document.getElementById(divid).style.display == 'none'){
      document.getElementById(divid).style.display = 'block';
    }else{
      document.getElementById(divid).style.display = 'none';
    }
  }
</script>



<LINK href="../../../css/home.css" type="text/css" rel="stylesheet">
<LINK href="../../../css/kanevaSC.css" type="text/css" rel="stylesheet">
<style type="text/css">
.hlRow {
	CURSOR: hand;
}

a.info{
    position:relative; /*this is the key*/
    z-index:24;
    
    text-decoration:none}

a.info:hover{z-index:25;}

a.info span{display: none}

a.info:hover span{ /*the span will display just on :hover state*/
    display:block;
    position:absolute;
    top:2em; left:2em; width:30em;
    padding:4px;
    border:1px solid #000000;
    background-color:#ffffff; color:#000;
    text-align: left;
    }
    

</style>
    <Kaneva:AdminMenu ID="adminmenu" runat="server" />
		<table id="tblNoAdmin" cellSpacing="0" cellPadding="0" width="560" border="0" runat="server">
			<tr>
				<td align="center"><BR>
					<BR>
					<BR>
					<span class="subHead">You must be a site administrator to access this section.</span>
				</td>
			</tr>
		</table>
		<KANEVA:REPORTNAV id="rnNavigation" runat="server" SubTab="kpi"></KANEVA:REPORTNAV>
		<style type="text/css">.kpinavMedia{  font-weight:bold !important;  }</style>
		<kaneva:KPINav id="kpiNav" runat="server"></kaneva:KPINav>
		<table width="auto" style="background-color:#FFFFFF;"><tr><td><BR>
			<table cellSpacing="0" cellPadding="0" width="750" border="0" align="center">
				<tr>
					<td class="Filter2" vAlign="middle" noWrap align="right" width="25%">Search 
						from&nbsp;
					</td>
					<td class="Filter2" vAlign="middle" noWrap align="left" width="25%"><asp:textbox class="Filter2" id="txtStartDate" runat="server" MaxLength="10" Width="100"></asp:textbox>(MM-dd-yyyy) 
						12:00:00 AM
						<asp:requiredfieldvalidator id="rfdtxtStartDate" Text="*" runat="server" Display="Dynamic" ErrorMessage="Start date is a required field."
							ControlToValidate="txtStartDate"></asp:requiredfieldvalidator><asp:regularexpressionvalidator id="revStartDate" Text="*" runat="server" Display="Static" ErrorMessage="Start date must be in format mm-dd-yyyy."
							ControlToValidate="txtStartDate" EnableClientScript="True"></asp:regularexpressionvalidator></td>
					<td></td>
					<td>
					<asp:button class="Filter2" id="btnSearch" onclick="btnSearch_Click" Text="search" runat="Server" CausesValidation="True"></asp:button> 					    					
					</td>
				</tr>
				<tr>
					<td class="Filter2" vAlign="middle" noWrap align="right">to&nbsp;
					</td>
					<td class="Filter2" vAlign="middle" noWrap align="left"><asp:textbox class="Filter2" id="txtEndDate" runat="server" MaxLength="10" Width="100"></asp:textbox>(MM-dd-yyyy) 
						11:59:59 PM
						<asp:requiredfieldvalidator id="rfdtxtEndDate" Text="*" runat="server" Display="Dynamic" ErrorMessage="End date is a required field."
							ControlToValidate="txtEndDate"></asp:requiredfieldvalidator><asp:regularexpressionvalidator id="revEndDate" Text="*" runat="server" Display="Static" ErrorMessage="End date must be in format mm-dd-yyyy."
							ControlToValidate="txtEndDate" EnableClientScript="True"></asp:regularexpressionvalidator></td>
					<td vAlign="middle" align="center" width="10%"></td>
					<td vAlign="middle" align="left" width="40%">
					    
					    <asp:button class="Filter2" id="btnSearch7" onclick="btnSearch7_Click" Text="search" runat="Server" CausesValidation="True"></asp:button>
					    <asp:button class="Filter2" id="btnSearch6" onclick="btnSearch6_Click" Text="search" runat="Server" CausesValidation="True"></asp:button>
					    <asp:button class="Filter2" id="btnSearch5" onclick="btnSearch5_Click" Text="search" runat="Server" CausesValidation="True"></asp:button>					    					    
					    <asp:button class="Filter2" id="btnSearch4" onclick="btnSearch4_Click" Text="search" runat="Server" CausesValidation="True"></asp:button>
					    <asp:button class="Filter2" id="btnSearch3" onclick="btnSearch3_Click" Text="search" runat="Server" CausesValidation="True"></asp:button>
					    <asp:button class="Filter2" id="btnSearch2" onclick="btnSearch2_Click" Text="search" runat="Server" CausesValidation="True"></asp:button>
					    <asp:button class="Filter2" id="btnSearch1" onclick="btnSearch1_Click" Text="search" runat="Server" CausesValidation="True"></asp:button> 
					    					    					    
					    </td>
				</tr>
			</table>
			<asp:panel id="pnlAdmin" runat="server" Visible="False">
				<BR>
				<table cellSpacing="0" cellPadding="0" width="905" border="0" runat="server">
					<tr class="lineItemColHead">
						<td align="left" width="100%">&nbsp;&nbsp;<b><asp:label class="dateStamp" id="lblEarnings" runat="server"></asp:label></b>
						</td>
					</tr>
				</table>
				<table class="FullBorders" cellSpacing="0" cellPadding="3" width="905" border="0">

					<tr class="sectionTitle" bgColor="#999999">
						<td class="bodyText" align="left" colSpan="3">Media</td>
					</tr>
					<tr class="lineItemOdd">
						<td class="bodyText" align="right" width="348">&nbsp;Connected Media:</td>
						<td><IMG src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblConnectedMedia" runat="server"></asp:label>&nbsp;
						</td>
					</tr>
					<tr>
						<td class="bodyText" align="right" width="348">&nbsp;Total Media Uploaded:</td>
						<td><IMG src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblMediaUploaded" runat="server"></asp:label>&nbsp;
						</td>
					</tr>
					<tr class="lineItemOdd">
						<td class="bodyText" align="right" width="348">&nbsp;Videos Uploaded:</td>
						<td><IMG src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblVideoUploaded" runat="server"></asp:label>&nbsp;
						</td>
					</tr>
					<tr>
						<td class="bodyText" align="right" width="348">&nbsp;YouTube Videos Uploaded:</td>
						<td><img src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblYouTubeUploaded" runat="server"></asp:label>&nbsp;
						</td>
					</tr>
					<tr class="lineItemOdd">
						<td class="bodyText" align="right" width="348">&nbsp;Music Uploaded:</td>
						<td><IMG src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblMusicUploaded" runat="server"></asp:label>&nbsp;
						</td>
					</tr>
					<tr>
						<td class="bodyText" align="right" width="348">&nbsp;Photos Uploaded:</td>
						<td><IMG src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblPhotoUploaded" runat="server"></asp:label>&nbsp;
						</td>
					</tr>
					<tr class="lineItemOdd">
						<td class="bodyText" align="right" width="348">&nbsp;Patterns/Textures Uploaded:</td>
						<td><img src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblPatternUploaded" runat="server"></asp:label>&nbsp;
						</td>
					</tr>
					<tr>
						<td class="bodyText" align="right" width="348">&nbsp;Games Uploaded:</td>
						<td><IMG src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblGameUploaded" runat="server"></asp:label>&nbsp;
						</td>
					</tr>
					<tr class="lineItemOdd">
						<td class="bodyText" align="right" width="348">&nbsp;TV Streams Uploaded:</td>
						<td><IMG src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblTVUploaded" runat="server"></asp:label>&nbsp;
						</td>
					</tr>
					<tr>
						<td class="bodyText" align="right" width="348">&nbsp;Widgets Uploaded:</td>
						<td><IMG src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblWidgetsUploaded" runat="server"></asp:label>&nbsp;
						</td>
					</tr>

				</table>
				<br>
				<br></asp:panel>
			</td></tr></table>
