///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Globalization;
using log4net;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for KGPFeedback.
	/// </summary>
    public class KGPFeedback : NoBorderPage
	{
		protected KGPFeedback () 
		{
			Title = "Admin - KGP Feedback";
		}

		private void Page_Load (object sender, System.EventArgs e)
		{
			pnlAdmin.Visible = (IsAdministrator ());
			tblNoAdmin.Visible = (!IsAdministrator ());

			if (!IsPostBack)
			{
				BindData (1);
			}
		}

		/// <summary>
		/// Bind the data
		/// </summary>
		private void BindData (int pageNumber)
		{
			// Log the activity
            SiteMgmtFacade siteMgmtFacade = new SiteMgmtFacade();
            siteMgmtFacade.InsertCSRLog(GetUserId(), "Admin - Viewing KGP Feedback page " + pageNumber, 0, 0);

			// Set the sortable columns
			SetHeaderSortText (dgrdFeedback);
			string orderby = CurrentSort + " " + CurrentSortOrder;

			int itemsPerPage = 50;
			PagedDataTable pds = ReportUtility.GetPlatformFeedback (orderby, pgTop.CurrentPageNumber, itemsPerPage);
			pgTop.NumberOfPages = Math.Ceiling ((double) pds.TotalCount / itemsPerPage).ToString ();
			pgTop.DrawControl ();

			dgrdFeedback.DataSource = pds;
			dgrdFeedback.DataBind ();

			// The results
			lblSearch.Text = GetResultsText (pds.TotalCount, pageNumber, itemsPerPage, pds.Rows.Count);
		}

		/// <summary>
		/// DEFAULT_SORT
		/// </summary>
		/// <returns></returns>
		protected override string DEFAULT_SORT
		{
			get
			{
				return "created_datetime";
			}
		}

		/// <summary>
		/// DEFAULT_SORT
		/// </summary>
		/// <returns></returns>
		protected override string DEFAULT_SORT_ORDER
		{
			get
			{
				return "DESC";
			}
		}

		/// <summary>
		/// They clicked to sort a column
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected override void btnSort_Click (object sender, System.EventArgs e)
		{
			base.btnSort_Click (sender, e);
			BindData (pgTop.CurrentPageNumber);
		}

		/// <summary>
		/// Pager change event
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pg_PageChange (object sender, PageChangeEventArgs e)
		{
			BindData (e.PageNumber);
		}

		/// <summary>
		/// GetDownloadLink
		/// </summary>
		protected string GetDownloadLink (int feedbackId)
		{
			return ResolveUrl ("~/Administration/reports/downloadFeedback.aspx?feedbackId=" + feedbackId);
		}

		/// <summary>
		/// GetType
		/// </summary>
		protected string GetType (int type)
		{
			if (type.Equals ((int) Constants.eFeedback.ERROR))
			{
				return "error";
			}
			else if (type.Equals ((int) Constants.eFeedback.FEEDBACK))
			{
				return "feedback";
			}
			else
			{
				return "N/A";
			}	
		}

		protected HtmlTable tblNoAdmin;
		protected Panel pnlAdmin;

		// Grid
		protected DataGrid dgrdFeedback;
		protected Kaneva.Pager pgTop;
		protected Label lblSearch;

		#region Web Form Designer generated code

		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
			pgTop.PageChanged +=new PageChangeEventHandler (pg_PageChange);
		}
		#endregion
	}
}
