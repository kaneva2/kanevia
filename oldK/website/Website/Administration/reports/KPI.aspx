<%@ Register TagPrefix="Kaneva" TagName="AdminMenu" Src="../../usercontrols/AdministrativeMenu.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="ReportNav" Src="reportNav.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../../usercontrols/Pager.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="KPINav" Src="kpi2/KPINav.ascx" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>
<%@ Page language="c#" Codebehind="KPI.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.KPI" %>


<script language="javascript">
  function toggleDiv(divid){
    if(document.getElementById(divid).style.display == 'none'){
      document.getElementById(divid).style.display = 'block';
    }else{
      document.getElementById(divid).style.display = 'none';
    }
  }
</script>



<LINK href="../../css/home.css" type="text/css" rel="stylesheet">
<LINK href="../../css/kanevaSC.css" type="text/css" rel="stylesheet">
<style type="text/css">
.hlRow {
	CURSOR: hand;
}

a.info{
    position:relative; /*this is the key*/
    z-index:24;
    
    text-decoration:none}

a.info:hover{z-index:25;}

a.info span{display: none}

a.info:hover span{ /*the span will display just on :hover state*/
    display:block;
    position:absolute;
    top:2em; left:2em; width:30em;
    padding:4px;
    border:1px solid #000000;
    background-color:#ffffff; color:#000;
    text-align: left;
    }
    

</style>
    <Kaneva:AdminMenu ID="adminmenu" runat="server" />
		<table id="tblNoAdmin" cellSpacing="0" cellPadding="0" width="560" border="0" runat="server">
			<tr>
				<td align="center"><BR>
					<BR>
					<BR>
					<span class="subHead">You must be a site administrator to access this section.</span>
				</td>
			</tr>
		</table>
		<KANEVA:REPORTNAV id="rnNavigation" runat="server" SubTab="kpi"></KANEVA:REPORTNAV>
		<style type="text/css">.kpiFull{  font-weight:bold !important;  }</style>
		<kaneva:KPINav id="kpiNav" runat="server"></kaneva:KPINav>
		<table width="auto" style="background-color:#FFFFFF;"><tr><td><BR>
			<table cellSpacing="0" cellPadding="0" width="750" border="0" align="center">
				<tr>
					<td class="Filter2" vAlign="middle" noWrap align="right" width="25%">Search 
						from&nbsp;
					</td>
					<td class="Filter2" vAlign="middle" noWrap align="left" width="25%"><asp:textbox class="Filter2" id="txtStartDate" runat="server" MaxLength="10" Width="100"></asp:textbox>(MM-dd-yyyy) 
						12:00:00 AM
						<asp:requiredfieldvalidator id="rfdtxtStartDate" Text="*" runat="server" Display="Dynamic" ErrorMessage="Start date is a required field."
							ControlToValidate="txtStartDate"></asp:requiredfieldvalidator><asp:regularexpressionvalidator id="revStartDate" Text="*" runat="server" Display="Static" ErrorMessage="Start date must be in format mm-dd-yyyy."
							ControlToValidate="txtStartDate" EnableClientScript="True"></asp:regularexpressionvalidator></td>
					<td></td>
					<td>
					<asp:button class="Filter2" id="btnSearch" onclick="btnSearch_Click" Text="search" runat="Server" CausesValidation="True"></asp:button> 					    					
					</td>
				</tr>
				<tr>
					<td class="Filter2" vAlign="middle" noWrap align="right">to&nbsp;
					</td>
					<td class="Filter2" vAlign="middle" noWrap align="left"><asp:textbox class="Filter2" id="txtEndDate" runat="server" MaxLength="10" Width="100"></asp:textbox>(MM-dd-yyyy) 
						11:59:59 PM
						<asp:requiredfieldvalidator id="rfdtxtEndDate" Text="*" runat="server" Display="Dynamic" ErrorMessage="End date is a required field."
							ControlToValidate="txtEndDate"></asp:requiredfieldvalidator><asp:regularexpressionvalidator id="revEndDate" Text="*" runat="server" Display="Static" ErrorMessage="End date must be in format mm-dd-yyyy."
							ControlToValidate="txtEndDate" EnableClientScript="True"></asp:regularexpressionvalidator></td>
					<td vAlign="middle" align="center" width="10%"></td>
					<td vAlign="middle" align="left" width="40%">
					    
					    <asp:button class="Filter2" id="btnSearch7" onclick="btnSearch7_Click" Text="search" runat="Server" CausesValidation="True"></asp:button>
					    <asp:button class="Filter2" id="btnSearch6" onclick="btnSearch6_Click" Text="search" runat="Server" CausesValidation="True"></asp:button>
					    <asp:button class="Filter2" id="btnSearch5" onclick="btnSearch5_Click" Text="search" runat="Server" CausesValidation="True"></asp:button>					    					    
					    <asp:button class="Filter2" id="btnSearch4" onclick="btnSearch4_Click" Text="search" runat="Server" CausesValidation="True"></asp:button>
					    <asp:button class="Filter2" id="btnSearch3" onclick="btnSearch3_Click" Text="search" runat="Server" CausesValidation="True"></asp:button>
					    <asp:button class="Filter2" id="btnSearch2" onclick="btnSearch2_Click" Text="search" runat="Server" CausesValidation="True"></asp:button>
					    <asp:button class="Filter2" id="btnSearch1" onclick="btnSearch1_Click" Text="search" runat="Server" CausesValidation="True"></asp:button> 
					    					    					    
					    </td>
				</tr>
			</table>
			<asp:panel id="pnlAdmin" runat="server" Visible="False">
				<BR>
				<table cellSpacing="0" cellPadding="0" width="905" border="0" runat="server">
					<tr class="lineItemColHead">
						<td align="left" width="100%">&nbsp;&nbsp;<b><asp:label class="dateStamp" id="lblEarnings" runat="server"></asp:label></b>
						</td>
					</tr>
				</table>
				<table class="FullBorders" cellSpacing="0" cellPadding="3" width="905" border="0">
					<!-- WOK -->
					<tr bgColor="#999999">
						<td class="bodyText" align="left" colSpan="3">WOK Status</td>
					</tr>
					<tr class="lineItemOdd">
						<td class="bodyText" align="right" width="348">WOK Logins:</td>
						<td><IMG id="Img2" src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblLogins" runat="server"></asp:label>&nbsp;
						</td>
					</tr>
					<tr> <!-- NEW -->
						<td class="bodyText" align="right" width="348">Unique WOK Logins:</td>
						<td><IMG id="Img3" src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblUniqueLogins" runat="server"></asp:label>&nbsp;
						</td>
					</tr>
					<tr class="lineItemOdd">
						<td class="bodyText" align="right" width="348">First Time WOK Logins:</td>
						<td><IMG id="Img4" src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblFirstTimeLogins" runat="server"></asp:label>&nbsp;
						</td>
					</tr>
					
					<tr class="lineItemOdd">
						<td class="bodyText" align="right" width="348">Avatars Created:</td>
						<td><IMG id="Img49" src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblAvatarsCreated" runat="server"></asp:label>&nbsp;
						</td>
					</tr>
										
					<tr>
						<td class="bodyText" align="right" width="348">New Homes:</td>
						<td><IMG id="Img8" src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblNewHomes" runat="server"></asp:label></td>
					</tr>
					<tr class="lineItemOdd">
						<td class="bodyText" align="right" width="348">New Hangouts:</td>
						<td><IMG src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblNewHangouts" runat="server"></asp:label></td>
					</tr>
					<tr>
						<td class="bodyText" noWrap align="right" width="348">Avg. Hourly Concurrent Users:</td>
						<td><IMG src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblAvgHourlyConcurrentUsers" runat="server"></asp:label>&nbsp;
						</td>
					</tr>
					<tr class="lineItemOdd">
						<td class="bodyText" align="right" width="348">Peak Concurrent Users:</td>
						<td><IMG id="Img14" src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblPeakConcurrentUsers" runat="server"></asp:label>&nbsp;
						</td>
					</tr>
					<tr>
						<td class="bodyText" align="right" width="348">Avg. Concurrent Users per Hour:</td>
						<td><IMG src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:datalist id="dlAvgConcUsersPerHour" runat="server" repeatdirection="Horizontal" cssclass="bodyText"
								borderwidth="0" cellspacing="0" cellpadding="0" repeatcolumns="12">
								<itemtemplate>
									<table cellpadding="0" cellspacing="0" border="0" class="bodyText">
										<tr>
											<td style="border-bottom:1px solid #000000;text-align:center;width:40px;"><%# DataBinder.Eval(Container.DataItem, "cur_hour") %></td>
										</tr>
										<tr>
											<td style="text-align:center;padding:2px 0 8px 0;"><%# RoundValue(DataBinder.Eval(Container.DataItem, "count"), 1) %></td>
										</tr>
									</table>
								</itemtemplate>
							</asp:datalist></td>
					</tr>
					<tr class="lineItemOdd">
						<td class="bodyText" align="right" width="348">Peak Concurrent Users per Hour:</td>
						<td><IMG src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left">
						    <asp:datalist id="dlPeakConcUsersPerHour" runat="server" repeatdirection="Horizontal" cssclass="bodyText"
								borderwidth="0" cellspacing="0" cellpadding="0" repeatcolumns="12">
								<itemtemplate>
									<table cellpadding="0" cellspacing="0" border="0" class="bodyText">
										<tr>
											<td style="border-bottom:1px solid #000000;text-align:center;width:40px;"><%# DataBinder.Eval(Container.DataItem, "cur_hour") %></td>
										</tr>
										<tr>
											<td style="text-align:center;padding:2px 0 8px 0;"><%# RoundValue(DataBinder.Eval(Container.DataItem, "count"), 1) %></td>
										</tr>
									</table>
								</itemtemplate>
							</asp:datalist></td>
					</tr>
					<tr class="sectionTitle" bgColor="#999999">
						<td class="bodyText" align="left" colSpan="3">WOK Economy</td>
					</tr>
					<tr class="lineItemOdd">
						<td class="bodyText" align="right" valign="top" width="348">Total WOK Economy(c):<br/>
						(rc):</td>
						<td><IMG src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp"  id="lblCreditsTotal" runat="server"></asp:label> 
						<a href="#" class="info">[?]<span>We have a snap shot of every user's balance and this is the sum of those balances on the end date 
						specifed.  If a record is not available for the end date the most recent record is shown. </span></a> 
						<br/>
						<asp:label class="dateStamp" id="lblRewardCreditsTotal" runat="server"></asp:label><br/>					

						</td>
					</tr>
					
					
					<tr id="hlRow" >
						<td class="bodyText" align="right" valign="top" width="348">Total Credits Added (c):</td>
						<td><IMG id="Img26" src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left" id="name1"><asp:label id="lblCreditsAdded" class="dateStamp" runat="server"></asp:label> 
						<a href="#" class="info">[?]<span>Sum of transactions between the specified dates where the tansaction amount was positive, 
						and the transaction was not a trade between users.</span></a>								    
							<ajax:ajaxpanel id="ajIncomeBreakdownK" runat="server">							
							[<asp:linkbutton runat="server" id="lbGetIncomeBreakdownK" onclick="lbGetIncomeBreakdownK_Click">Breakdown</asp:linkbutton>]							
							<asp:datalist id="dlGetIncomeBreakdownK" runat="server"  cssclass="bodyText" visible="false">
									<itemtemplate><%# DataBinder.Eval(Container.DataItem, "trans_desc")%>:
									<%# RoundValue(DataBinder.Eval(Container.DataItem, "debit_total"), 1) %></itemtemplate>
									<FooterTemplate><br /></FooterTemplate>													
						    </asp:datalist>						    
							</ajax:ajaxpanel>				
						</td>
					</tr>					
										
					<TR  id="hlRow">
						<TD class="bodyText" align="right"  valign="top" width="348">Total Reward Credits Added (rc):</TD>
						<TD></TD>
						<TD class="bodyText" align="left"  id="name2"><asp:label class="dateStamp" id="lblRewardCreditsAdded" runat="server"></asp:label> 								
							<ajax:ajaxpanel id="ajIncomeBreakdownG" runat="server">							
							[<asp:linkbutton runat="server" id="lbGetIncomeBreakdownG" onclick="lbGetIncomeBreakdownG_Click">Breakdown</asp:linkbutton>]							
							<asp:datalist id="dlGetIncomeBreakdownG" runat="server"  cssclass="bodyText" visible="false">
									<itemtemplate><%# DataBinder.Eval(Container.DataItem, "trans_desc")%>:
									<%# RoundValue(DataBinder.Eval(Container.DataItem, "debit_total"), 1) %></itemtemplate>
									<FooterTemplate><br /></FooterTemplate>													
						    </asp:datalist>						    
							</ajax:ajaxpanel>
												
						</TD>
					</TR>					
										
					<tr class="lineItemOdd">
						<td class="bodyText" align="right" width="348">Credits Purchased (c):</td>
						<td><IMG id="Img5" src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblCreditsPurchased" runat="server"></asp:label>&nbsp;
						</td>
					</tr>
					
					<tr id="hlRow"  >
						<td class="bodyText" align="right" valign="top" width="348">Revenue in Dollars ($):</td>
						<td><IMG  src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblTotalRevenue" runat="server"></asp:label>&nbsp;
						<% if(lblTotalRevenue.Text != "0"){ %>[<a href="javascript:;" onmousedown="toggleDiv('divRevenueBreakdown');">Breakdown</a>]<% } %>
						    <div id="divRevenueBreakdown" style="display:none"> 
							<div class="breakdowntext">
																
								    <table >
                                    <asp:Repeater runat="server" ID="rptRevenueBreakdown"  >
                                        <ItemTemplate>
                                        <tr>
                                           <td align="right" class="bodytext"><%# DataBinder.Eval(Container.DataItem, "name") %> </td>
                                           <td>&nbsp;</td>
                                           <td align="left" class="bodytext">$<%# DataBinder.Eval(Container.DataItem, "amount").ToString().Remove(DataBinder.Eval(Container.DataItem, "amount").ToString().IndexOf(".") + 3)%> </td>                                           
                                        </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    </table>
                                      
							</div>
							</div>
						
						
						</td>
					</tr>							

					<tr class="lineItemOdd">
						<td class="bodyText" align="right" width="348">Effective Credit Price ($):</td>
						<td><IMG  src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left">$1 = <asp:label class="dateStamp" id="lblCreditPrice" runat="server"></asp:label> Credits <a href="#" class="info"> 
						[?]<span>The number of credits sold divided by revenue for the period specified.</span></a> 
						
						(<asp:label class="dateStamp" id="lblCreditPriceAllTime" runat="server"></asp:label> since <% Response.Write(startOfTime); %>)
						</td>
					</tr>

					
					<tr id="hlRow">
						<td class="bodyText" align="right" valign="top" width="348">Total Credits Spent (c):</td>
						<td><IMG src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"  id="name3"><asp:label class="dateStamp" id="lblCreditsSpent" runat="server"></asp:label>&nbsp;
						    <ajax:ajaxpanel id="ajSpendingBreakdownK" runat="server">
						    [<asp:linkbutton runat="server" id="lbGetSpendingBreakdownK" onclick="lbGetSpendingBreakdownK_Click">Breakdown</asp:linkbutton>]						
								<asp:datalist id="dlGetSpendingBreakdownK" runat="server"  cssclass="bodyText" visible="false">
									<itemtemplate><%# DataBinder.Eval(Container.DataItem, "trans_desc")%>:
									<%# RoundValue(DataBinder.Eval(Container.DataItem, "debit_total"), 1) %></itemtemplate>													
									<FooterTemplate><br /></FooterTemplate>
								</asp:datalist>
						    </ajax:ajaxpanel>
																					
						</td>
					</tr>					

					
					
					<TR id="hlRow">
						<TD class="bodyText" align="right" valign="top" width="348">Total Reward Credits Spent (rc):</TD>
						<TD></TD>
						<TD class="bodyText" align="left"  id="name4"><asp:label class="dateStamp" id="lblRewardCreditsSpent" runat="server"></asp:label>
						    <ajax:ajaxpanel id="ajSpendingBreakdownG" runat="server">
						    [<asp:linkbutton runat="server" id="lbGetSpendingBreakdownG" onclick="lbGetSpendingBreakdownG_Click">Breakdown</asp:linkbutton>]
							<div class="breakdowntext">
								<asp:datalist id="dlGetSpendingBreakdownG" runat="server"  cssclass="bodyText"  visible="false">
									<itemtemplate><%# DataBinder.Eval(Container.DataItem, "trans_desc")%>:
									<%# RoundValue(DataBinder.Eval(Container.DataItem, "debit_total"), 1) %></itemtemplate>													
									<FooterTemplate><br /></FooterTemplate>
								</asp:datalist>
							</ajax:ajaxpanel>
						
						</TD>
					</TR>										

					
					<tr class="lineItemOdd">
						<td class="bodyText" align="right" valign="top" width="348">Number of Payers:</td>
						<td><IMG id="Img6" src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblNumPayers" runat="server"></asp:label> 
						<a href="#" class="info">[?]<span>Number of distinct users that have made transactions.</span></a>						
						(<asp:label class="dateStamp" id="lblTotalNumPayers" runat="server"></asp:label> since <% Response.Write(startOfTime); %>)												
						</td>
					</tr>
					
					<tr >
						<td class="bodyText" align="right" width="348">First Time Payers:</td>
						<td><IMG id="Img7" src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblFirstTimePayers" runat="server"></asp:label>&nbsp;
						</td>
					</tr>
					
					
					<tr class="lineItemOdd">
						<td class="bodyText" align="right" valign="top" width="348">Number of Transactions:</td>
						<td><IMG  src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblNumberTransactions" runat="server"></asp:label> 
						<a href="#" class="info">[?]<span>Number of verified transactions.</span></a>
						(<asp:label class="dateStamp" id="lblNumberTransactionsAll" runat="server"></asp:label> since <% Response.Write(startOfTime); %>)																		
						</td>
					</tr>					
					
					<tr >
						<td class="bodyText" align="right" valign="top" width="348">Trans/Day:</td>
						<td><IMG  src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblNumberTransactions30Mav" runat="server"></asp:label> 30 Day Mav 
						<a href="#" class="info">[?]<span>Number of verified transactions per day averaged over the 30 days preceding the end date specified.</span></a><br />													
						</td>
					</tr>
					
					
					<tr class="lineItemOdd">
						<td class="bodyText" align="right" valign="top" width="348">$/Trans:</td>
						<td><IMG  src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblValueTransaction30Mav" runat="server"></asp:label> 30 Day Mav 
						<a href="#" class="info">[?]<span>Value per transaction averaged over the 30 days preceding the end date specified.</span></a>	
						(<asp:label class="dateStamp" id="lblValueTrans" runat="server"></asp:label> since <% Response.Write(startOfTime); %>)					
						</td>
					</tr>
					
					<tr >
						<td class="bodyText" align="right" valign="top" width="348">$/Payer:</td>
						<td><IMG  src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblValuePayer30Mav" runat="server"></asp:label> 30 Day Mav
						<a href="#" class="info">[?]<span>Value per payer averaged over the 30 days preceding the end date specified.</span></a>
						[<a href="javascript:;" onmousedown="toggleDiv('divAmountPayer');">Breakdown</a>]												
						<div id="divAmountPayer" style="display:none">
						<table class="bodytext">
							<tr>
								<td>Payers&nbsp;&nbsp;</td>
								<td>Amount <a href="#" class="info">[?]<span>Payers have paid at least this amount but no more than the amount in the next row.</span></a></td></tr>
						<asp:repeater id="rptPayersByAmount" runat="server">
							<itemtemplate>
								<tr><td><%# DataBinder.Eval(Container.DataItem, "numPayers") %></td><td><%# DataBinder.Eval(Container.DataItem, "bucket") %></td></tr>					
							</itemtemplate>
						</asp:repeater>
						</table>						
						</div>												
						</td>
					</tr>					
										
					<tr class="lineItemOdd">
						<td class="bodyText" align="right" valign="top" width="348">Trans/Payer:</td>
						<td><IMG  src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblTransPayer30Mav" runat="server"></asp:label> 30 Day Mav 
						<a href="#" class="info">[?]<span>Number of transactions per payer averaged over the 30 days preceding the end date specified.</span></a>						
						(<asp:label class="dateStamp" id="lblTransPayer" runat="server"></asp:label> since <% Response.Write(startOfTime); %>)												
						[<a href="javascript:;" onmousedown="toggleDiv('divTransPayer');">Breakdown</a>]
						<div id="divTransPayer" style="display:none">
						<table class="bodytext">
							<tr><td>Payers&nbsp;&nbsp;</td><td>Trans Per Payer</td></tr>
						<asp:repeater id="rptTransactionsByPayers" runat="server">
							<itemtemplate>
								<tr><td><%# DataBinder.Eval(Container.DataItem, "numPayers") %></td><td><%# DataBinder.Eval(Container.DataItem, "bucket") %></td></tr>					
							</itemtemplate>
						</asp:repeater>
						</table>
						</div>	
											
						</td>
					</tr>
						
                    <tr>
						<td class="bodyText" align="right" width="348">Access Passes Purchased:</td>
						<td><IMG src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblAccessPasses" runat="server"></asp:label>&nbsp;
						(Pass only: <asp:label class="dateStamp" id="lblAccessPassOnly" runat="server"></asp:label> )
						</td>
                    </tr>

                    <tr>
						<td class="bodyText" align="right" valign="top" width="348">Gift Cards Redeemed:</td>
						<td><IMG src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left">
						    <table class="bodytext">
							    <tr><td>Payers&nbsp;&nbsp;</td><td>Card Type</td></tr>
						        <asp:repeater id="rptGiftCardRedemptions" runat="server">
						            <itemtemplate>
    						            <tr><td><%# DataBinder.Eval(Container.DataItem, "count") %></td><td><%# DataBinder.Eval(Container.DataItem, "description") %></td></tr>
						            </itemtemplate>						    
						        </asp:repeater>	
						    </table>											
						</td>
                    </tr>

                    <tr  >
                        <td class="bodyText"  align="right"  valign="top" >Gift Cards - Incomm Reports</td>
                        <td><IMG  src="~/images/spacer.gif" width="5" runat="server"></td>
                        <td class="bodyText">
                            $10 -   Sold: <asp:label id="lblGCsold10" runat="server"></asp:label> 
                                    Redeemed: <asp:label id="lblGCredeemed10" runat="server"></asp:label> <br />
                            $25 -   Sold: <asp:label id="lblGCsold25" runat="server"></asp:label> 
                                    Redeemed: <asp:label id="lblGCredeemed25" runat="server"></asp:label> <br />                            
                            [<a href="javascript:;" onmousedown="toggleDiv('divGCBreakdown');">Breakdown</a>]<br />
                            	<div id="divGCBreakdown"  style="display:none" >
                            	<table class="bodyText" >
								    <tr>
								        <td>State</td><td>Sold</td><td>Returned</td><td>Net Sold</td>
								    </tr>								    								
								    <asp:repeater id="dlGiftCardsByState" runat="server">
									    <itemtemplate>
									    <tr>
									        <td><%# DataBinder.Eval(Container.DataItem, "state")%></td>
									        <td><%# DataBinder.Eval(Container.DataItem, "num_sold") %></td>
									        <td><%# DataBinder.Eval(Container.DataItem, "num_returned") %></td>
									        <td><%# DataBinder.Eval(Container.DataItem, "net_sold") %></td>
									    </tr>
									    </itemtemplate>									    													
								    </asp:repeater>
								</table>
								</div>                        
                        </td>                                            
                    </tr>



					<!--<tr class="lineItemOdd" >
						<td class="bodyText" align="right" width="348">Total Furniture Placed (All Time):</td>
						<td><IMG id="Img19" src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblFurniturePlaced" runat="server"></asp:label>&nbsp;
						</td>
					</tr>
					<tr>
						<td class="bodyText" align="right" width="348">Total Clothing Purchased (All Time):</td>
						<td><IMG id="Img20" src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblClothingPurchased" runat="server"></asp:label>&nbsp;
						</td>
					</tr>-->
					
					
					
					<tr class="sectionTitle" bgColor="#999999">
						<td class="bodyText" align="left" colSpan="3">Members</td>
					</tr>
					<tr>
						<td class="bodyText" align="right" width="348">Site Members (All Time):</td>
						<td><IMG src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblMembers" runat="server"></asp:label>&nbsp;
						</td>
					</tr>
					<tr class="lineItemOdd">
						<td class="bodyText" align="right" width="348">New Site Members:</td>
						<td><IMG src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left">
							<asp:label class="dateStamp" id="lblUsers" runat="server"></asp:label>&nbsp;
							(Of these <asp:label class="dateStamp" id="lblActivatedEmails" runat="server"></asp:label> have been activated)
						
						</td>
					</tr>

					<tr>
						<td class="bodyText" align="right" width="348">Member Contributors:</td>
						<td><IMG src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblMemberContribs" runat="server"></asp:label>&nbsp;
						</td>
					</tr>
					<tr class="lineItemOdd"> <!-- New -->
						<td class="bodyText" align="right" width="348"><b>Total Number of WOK Users (All Time):</b></td>
						<td><IMG id="Img10" src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblWOKUsersTotal" runat="server"></asp:label>&nbsp;
						</td>
					</tr>
					<tr>
						<td class="bodyText" align="right" width="348">Total Male Players (All Time):</td>
						<td><IMG id="Img1" src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblMalePlayers" runat="server"></asp:label>&nbsp;
						</td>
					</tr>
					<tr class="lineItemOdd">
						<td class="bodyText" align="right" width="348">Total Female Players (All Time):</td>
						<td><IMG id="Img18" src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblFemalePlayers" runat="server"></asp:label>&nbsp;
						</td>
					</tr>
					<tr>
						<td class="bodyText" align="right" width="348">Avg. Minutes In-World per Player 
							(All Time):</td>
						<td><IMG src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblAvgMinInWorld" runat="server"></asp:label>&nbsp;
						</td>
					</tr>
					<tr class="lineItemOdd">
						<td class="bodyText" align="right" width="348">Avg. Dollars Spent per Member:</td>
						<td><IMG id="Img12" src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblAvgDollarsSpent" runat="server"></asp:label>&nbsp;
						</td>
					</tr>
					<tr>
						<td class="bodyText" align="right" width="348">Active WOK Members (=/+10hrs) (All 
							Time):</td>
						<td><IMG id="Img13" src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblActiveWOKMembers" runat="server"></asp:label>&nbsp;
						</td>
					</tr>
					<TR class="lineItemOdd">
						<TD class="bodyText" align="right" width="348">Active WOK Members (=/+ x hrs) (Logged within 30 days):</TD>
						<TD><IMG id="Img11" src="~/images/spacer.gif" width="5" runat="server"></TD>
						<TD class="bodyText" align="left">
							<asp:label class="dateStamp" id="lblAvgActiveWOK" runat="server"></asp:label> 10hrs, 
							<asp:label class="dateStamp" id="lblAvgActiveWOK5" runat="server"></asp:label> 5hrs, 
							<asp:label class="dateStamp" id="lblAvgActiveWOK2" runat="server"></asp:label> 2hrs,
							<asp:label class="dateStamp" id="lblAvgActiveWOK1" runat="server"></asp:label> 1hrs 
							
							<a href="#" class="info">[?]<span>Number of users that have spent 10 hours or more online within the last 30 days. Note: Specified dates are 
							not used, this is from the current date.</span></a>
							</TD>
					</TR>
					<tr class="lineItemOdd">
						<td class="bodyText" align="right" width="348">Semi-Active WOK Members (2-10hrs) 
							(All Time):</td>
						<td><IMG id="Img21" src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblSemiActiveWokMembers" runat="server"></asp:label>&nbsp;
						</td>
					</tr>
					<tr>
						<td class="bodyText" align="right" width="348">Inactive WOK Members (&lt;2hrs) (All 
							Time):</td>
						<td><IMG id="Img16" src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblInactiveWOKMembers" runat="server"></asp:label>&nbsp;
						</td>
					</tr>
					<TR class="lineItemOdd">
						<TD class="bodyText" align="right" width="348">Avg. Inactive WOK Members (&lt;2hrs) 
							(All Time):</TD>
						<TD><IMG id="Img22" src="~/images/spacer.gif" width="5" runat="server"></TD>
						<TD class="bodyText" align="left">
							<asp:label class="dateStamp" id="lblAvgInactiveWOK" runat="server"></asp:label></TD>
					</TR>
					<tr class="lineItemOdd">
						<td class="bodyText" align="right" width="348">Active WOK Spenders (=/+$9.99) (All 
							Time):</td>
						<td><IMG id="Img15" src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblActiveWOKSpenders" runat="server"></asp:label>&nbsp;
						</td>
					</tr>
					<TR class="lineItemOdd">
						<TD class="bodyText" align="right" width="348">Active WOK Spenders (=/+$9.99) 
							(30 days):</TD>
						<TD><IMG id="Img23" src="~/images/spacer.gif" width="5" runat="server"></TD>
						<TD class="bodyText" align="left">
							<asp:label class="dateStamp" id="lblAvgActiveSpend" runat="server"></asp:label> 
							<a href="#" class="info">[?]<span>Number of users that have spent $9.99 or more within the 30 days proceeding the specified end date.</span></a>							
							</TD>
					</TR>
					<tr>
						<td class="bodyText" align="right" width="348">Inactive WOK Spenders (&lt;$10) (All 
							Time):</td>
						<td><IMG id="Img17" src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblInactiveWOKSpenders" runat="server"></asp:label>&nbsp;
						</td>
					</tr>
					<TR class="lineItemOdd">
						<TD class="bodyText" align="right" width="348">Avg. Inactive WOK Spenders 
							(=/+$9.99) (30 days):</TD>
						<TD><IMG id="Img24" src="~/images/spacer.gif" width="5" runat="server"></TD>
						<TD class="bodyText" align="left">
							<asp:label class="dateStamp" id="lblAvgInactiveSpend" runat="server"></asp:label></TD>
					</TR>
					<tr class="sectionTitle" bgColor="#999999">
						<td class="bodyText" align="left" colSpan="3">Communities and Profiles</td>
					</tr>
					<tr>
						<td class="bodyText" align="right" width="348">&nbsp;Communities (All Time):</td>
						<td><IMG src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblBroadband" runat="server"></asp:label>&nbsp;
						</td>
					</tr>
					<tr class="lineItemOdd">
						<td class="bodyText" align="right" width="348">&nbsp;New Communities:</td>
						<td><IMG src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblNewBroadband" runat="server"></asp:label>&nbsp;
						</td>
					</tr>
					<tr>
						<td class="bodyText" align="right" width="348">&nbsp;New Profiles:</td>
						<td><IMG src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblMemberChannels" runat="server"></asp:label>&nbsp;
						</td>
					</tr>
					<tr class="sectionTitle" bgColor="#999999">
						<td class="bodyText" align="left" colSpan="3">Viral/Community</td>
					</tr>
					<tr>
						<td class="bodyText" align="right" width="348">&nbsp;Shares:</td>
						<td><IMG src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblShares" runat="server"></asp:label>&nbsp;
						</td>
					</tr>
					<tr class="lineItemOdd">
						<td class="bodyText" align="right" width="348">&nbsp;Raves:</td>
						<td><IMG src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblRaves" runat="server"></asp:label>&nbsp;
						</td>
					</tr>
					<tr>
						<td class="bodyText" align="right" width="348">&nbsp;Blog Activity:</td>
						<td><IMG src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblBlogActivity" runat="server"></asp:label>&nbsp;
						</td>
					</tr>
					<tr class="lineItemOdd">
						<td class="bodyText" align="right" width="348">&nbsp;Comments:</td>
						<td><IMG src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblComments" runat="server"></asp:label>&nbsp;
						</td>
					</tr>
					<tr>
						<td class="bodyText" align="right" width="348">&nbsp;Friend Requests:</td>
						<td><IMG src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblFriendRequests" runat="server"></asp:label>&nbsp;
						</td>
					</tr>
					<tr class="lineItemOdd">
						<td class="bodyText" align="right" width="348">&nbsp;Friends Accepted:</td>
						<td><IMG src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblFriendsAccepted" runat="server"></asp:label>&nbsp;
						</td>
					</tr>
					<!--<tr>
						<td class="bodyText" align="right" width="348">&nbsp;Downloads:</td>
						<td><IMG src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblDownloads" runat="server"></asp:label>&nbsp;
						</td>
					</tr>-->
					<tr class="sectionTitle" bgColor="#999999">
						<td class="bodyText" align="left" colSpan="3">Media</td>
					</tr>
					<tr class="lineItemOdd">
						<td class="bodyText" align="right" width="348">&nbsp;Connected Media:</td>
						<td><IMG src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblConnectedMedia" runat="server"></asp:label>&nbsp;
						</td>
					</tr>
					<tr>
						<td class="bodyText" align="right" width="348">&nbsp;Total Media Uploaded:</td>
						<td><IMG src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblMediaUploaded" runat="server"></asp:label>&nbsp;
						</td>
					</tr>
					<tr class="lineItemOdd">
						<td class="bodyText" align="right" width="348">&nbsp;Videos Uploaded:</td>
						<td><IMG src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblVideoUploaded" runat="server"></asp:label>&nbsp;
						</td>
					</tr>
					<tr>
						<td class="bodyText" align="right" width="348">&nbsp;YouTube Videos Uploaded:</td>
						<td><img src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblYouTubeUploaded" runat="server"></asp:label>&nbsp;
						</td>
					</tr>
					<tr class="lineItemOdd">
						<td class="bodyText" align="right" width="348">&nbsp;Music Uploaded:</td>
						<td><IMG src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblMusicUploaded" runat="server"></asp:label>&nbsp;
						</td>
					</tr>
					<tr>
						<td class="bodyText" align="right" width="348">&nbsp;Photos Uploaded:</td>
						<td><IMG src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblPhotoUploaded" runat="server"></asp:label>&nbsp;
						</td>
					</tr>
					<tr class="lineItemOdd">
						<td class="bodyText" align="right" width="348">&nbsp;Patterns/Textures Uploaded:</td>
						<td><img src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblPatternUploaded" runat="server"></asp:label>&nbsp;
						</td>
					</tr>
					<tr>
						<td class="bodyText" align="right" width="348">&nbsp;Games Uploaded:</td>
						<td><IMG src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblGameUploaded" runat="server"></asp:label>&nbsp;
						</td>
					</tr>
					<tr class="lineItemOdd">
						<td class="bodyText" align="right" width="348">&nbsp;TV Streams Uploaded:</td>
						<td><IMG src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblTVUploaded" runat="server"></asp:label>&nbsp;
						</td>
					</tr>
					<tr>
						<td class="bodyText" align="right" width="348">&nbsp;Widgets Uploaded:</td>
						<td><IMG src="~/images/spacer.gif" width="5" runat="server"></td>
						<td class="bodyText" align="left"><asp:label class="dateStamp" id="lblWidgetsUploaded" runat="server"></asp:label>&nbsp;
						</td>
					</tr>

				</table>
				<br>
				<br></asp:panel>
			</td></tr></table>
