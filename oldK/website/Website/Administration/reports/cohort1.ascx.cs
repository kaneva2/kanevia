///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

namespace KlausEnt.KEP.Kaneva
{
  public partial class cohort1 : System.Web.UI.UserControl
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!IsPostBack)
      {
        BindData();
      }
    }

    protected void BindData()
    {
      txtStartDate.Text = DateTime.Now.ToShortDateString();
    }

    protected void GetReport()
    {

      string interval = ddlCohortInterval.SelectedValue.ToString();

      /* For the user group */

      DateTime dtStart = Convert.ToDateTime(txtStartDate.Text);
      DateTime dtEnd = dtStart.AddDays(1);

      if (ddlCohortInterval.SelectedValue == "WEEK")
      {
        dtEnd = dtStart.AddDays(7);
      }

      if (ddlCohortInterval.SelectedValue == "MONTH")
      {
        dtEnd = dtStart.AddMonths(1);
      }


      /* For the Period */
      DateTime dtStartP = Convert.ToDateTime(txtStartDate.Text);

	  Double DaysToReport = 5;

	  DaysToReport = Convert.ToDouble(txtDaysToReport.Text);
	  
      DateTime dtEndP = dtStartP.AddDays(DaysToReport);
      
      lblStatus.Text = "Group: " + dtStart.ToString("yyyy'-'MM'-'dd") + " - ";
      lblStatus.Text += dtEnd.ToString("yyyy'-'MM'-'dd") + "<br />";

      lblStatus.Text += "Period: " + dtStartP.ToString("yyyy'-'MM'-'dd") + " - ";
      lblStatus.Text += dtEndP.ToString("yyyy'-'MM'-'dd") + "<br />";

      StringBuilder sb1 = new StringBuilder();
      sb1.Append("SELECT COUNT(DISTINCT ll.user_id ) numusers ");
      sb1.Append("FROM wok.login_log ll ");
      sb1.Append("INNER JOIN wok.game_users gu ON ll.user_id = gu.user_id ");
      sb1.Append("WHERE ll.created_date BETWEEN '" + dtStartP.ToString("yyyy'-'MM'-'dd") + "' AND '" + dtEndP.ToString("yyyy'-'MM'-'dd") + "' ");
      sb1.Append("AND gu.created_date BETWEEN '" + dtStart.ToString("yyyy'-'MM'-'dd") + "' AND '" + dtEnd.ToString("yyyy'-'MM'-'dd") + "' ");
      
      if(ddlCohortFlavor.SelectedValue=="payers")
      {
      sb1.Append("AND gu.kaneva_user_id IN ( SELECT user_id FROM kaneva.purchase_point_transactions ppt ");
      sb1.Append("WHERE ppt.transaction_status = 3 AND amount_debited > 0) ");
      }

      string sqlSelect1 = sb1.ToString();
      int startGroup = KanevaGlobals.GetDatabaseUtility().ExecuteScalar(sqlSelect1);
	  
	  lblStatus.Text += "<!-- <br /> " + sqlSelect1 + " --> ";
      lblStatus.Text += "Users in first interval: " + startGroup.ToString() + "<br />";

      if (startGroup > 0)
      {
        StringBuilder sb = new StringBuilder();
        sb.Append("SELECT COUNT(DISTINCT ll.user_id ) numusers,  ");
        sb.Append("MIN(DATE(ll.created_date)) firstday,  ");
        sb.Append("MAX(DATE(ll.created_date)) lastday, ");
        sb.Append("((COUNT(DISTINCT ll.user_id )/" + startGroup + ") * 100) retention ");
        sb.Append("FROM wok.login_log ll ");
        sb.Append("INNER JOIN wok.game_users gu ON ll.user_id = gu.user_id ");
        sb.Append("WHERE ll.created_date BETWEEN '" + dtStartP.ToString("yyyy'-'MM'-'dd") + "' AND '" + dtEndP.ToString("yyyy'-'MM'-'dd") + "' ");
        sb.Append("AND gu.created_date BETWEEN '" + dtStart.ToString("yyyy'-'MM'-'dd") + "' AND '" + dtEnd.ToString("yyyy'-'MM'-'dd") + "' ");

        if (ddlCohortFlavor.SelectedValue == "payers")
        {
          sb.Append("AND gu.kaneva_user_id IN ( SELECT user_id FROM kaneva.purchase_point_transactions ppt ");
          sb.Append("WHERE ppt.transaction_status = 3 AND amount_debited > 0) ");
        }
        sb.Append("GROUP BY " + interval + "(ll.created_date) ");

        string sqlSelect = sb.ToString();

        lblStatus.Text += "<!-- <br /> " + sqlSelect + " --> ";

        DataTable dt = KanevaGlobals.GetDatabaseUtility().GetDataTable(sqlSelect);
        dgCohortData.DataSource = dt;
        dgCohortData.DataBind();

        dgCohortData2.DataSource = dt;
        dgCohortData2.DataBind();
      }
    }

    protected void btnGetReport_Click(object sender, EventArgs e)
    {
      GetReport();
    }
  }
}