<%@ Page language="c#" Codebehind="userActivity.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.userActivity" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../../usercontrols/Pager.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="ReportNav" Src="reportNav.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="MembersFilter" Src="../../usercontrols/MembersFilter.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="AdminMenu" Src="../../usercontrols/AdministrativeMenu.ascx" %>

<Kaneva:AdminMenu ID="adminmenu" runat="server" />
<asp:ValidationSummary ShowMessageBox="True" ShowSummary="False" Class="formError" id="valSum" DisplayMode="BulletList" runat="server" HeaderText="Please correct the following errors:"/>

<table runat="server" id="tblNoAdmin" cellpadding="0" cellspacing="0" border="0" width="560">
	<tr><td ALIGN="CENTER">
		<BR><BR><BR>
		<span class="subHead">You must be a site administrator to access this section.</span>
	</td></tr>
</table>

<asp:panel runat="server" ID="pnlAdmin">

<Kaneva:ReportNav runat="server" id="rnNavigation" SubTab="ua"/>
<br>

<table runat="server" cellpadding="0" cellspacing="0" border="0" width="905">
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
			<td colspan="3" style="background-color: #ededed;" width="100%">&nbsp;&nbsp;<span class="header02" style="font-size: 15px;"><b><asp:Label class="dateStamp" id="lblActiveUsers" runat="server"/><b></b></span>
			</td>
		<td><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
	</tr>
</table>

<br>

<table cellpadding="0" cellspacing="0" border="0" width="905">
<tr>
<td align="left">
	<table cellpadding="0" cellspacing="0" border="0" width="500">
		<tr>
				<td><img runat="server" src="~/images/spacer.gif" width="20" height="1"/></td>
				<td colspan="3"><b id="rounded2"><i><span><img border="0" runat="server" src="~/images/spacer.gif" height="8"/></span></i> </b></td>
				<td><img runat="server" src="~/images/spacer.gif" width="20" height="1"/></td>
		</tr>
		<tr>
				<td><img runat="server" src="~/images/spacer.gif" width="20" height="20" ID="Img1"/></td>
				<td colspan="3" width="560" style="background-color: #ededed;" align="left">&nbsp;&nbsp;&nbsp;<span class="assetLink">Search Users</span>&nbsp;<span style="color:orange;font-family:arial; font-size: 10px;">(Use single words only, search will return all matches that start with the word)</span></td>
				<td><img runat="server" src="~/images/spacer.gif" width="20" height="20" ID="Img5"/></td>
		</tr>
		<tr>
				<td><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
				<td align="right" class="bodyText" style="border-left: 1px solid #ededed;">&nbsp;<b>username:</b></td>
				<td><img runat="server" src="~/images/spacer.gif" width="5" ID="Img7"/></td>
				<td align="left" width="300" class="bodyText" style="border-right: 1px solid #ededed;">
					<asp:TextBox ID="txtUsername" class="Filter2" style="width:300px" MaxLength="31" runat="server"/>
				</td>
				<td width="10"><img runat="server" src="~/images/spacer.gif" width="20" height="18"/></td>
		</tr>
		<tr>
				<td><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
				<td align="right" class="bodyText" bgcolor="#ededed" style="border-left: 1px solid #ededed;">&nbsp;<b>email:</b></td>
				<td bgcolor="#ededed"><img runat="server" src="~/images/spacer.gif" width="5"/></td>
				<td align="left" width="300" class="bodyText" bgcolor="#ededed" style="border-right: 1px solid #ededed;">
						<asp:TextBox ID="txtEmail" class="Filter2" style="width:300px" MaxLength="31" runat="server"/>
				</td>
				<td width="10"><img runat="server" src="~/images/spacer.gif" width="20" height="18"/></td>
		</tr>
		<tr>
				<td><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
				<td align="right" class="bodyText" style="border-left: 1px solid #ededed;">&nbsp;<b>first name:</b></td>
				<td><img runat="server" src="~/images/spacer.gif" width="5"/></td>
				<td align="left" class="bodyText" style="border-right: 1px solid #ededed;">
						<asp:TextBox ID="txtFirstName" class="Filter2" style="width:300px" MaxLength="31" runat="server"/>
				</td>
				<td width="10"><img runat="server" src="~/images/spacer.gif" width="20" height="18"/></td>
		</tr>
		<tr>
				<td><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
				<td align="right" bgcolor="#ededed" class="bodyText" style="border-left: 1px solid #ededed;"><b>last name:</b></td>
				<td bgcolor="#ededed"><img runat="server" src="~/images/spacer.gif" width="5"/></td>
				<td align="left" bgcolor="#ededed" class="bodyText" style="border-right: 1px solid #ededed;">
					<asp:TextBox ID="txtLastName" class="Filter2" style="width:300px" MaxLength="31" runat="server"/>  
				</td>
				<td width="10"><img runat="server" src="~/images/spacer.gif" width="20" height="18"/></td>
		</tr>  
		<tr>
				<td><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
				<td align="right" class="bodyText" style="border-left: 1px solid #ededed;">&nbsp;<b>status:</b></td>
				<td><img runat="server" src="~/images/spacer.gif" width="5"/></td>
				<td align="left" class="bodyText" style="border-right: 1px solid #ededed;">
					<asp:DropDownList class="filter2" id="drpStatus" runat="Server" style="width:200px"/>
						
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<asp:button id="btnSearch" align="right" runat="Server" class="Filter2" Text="    Search    " onClick="btnSearch_Click" CausesValidation="False"/>    
                    
                    <asp:button id="btnGEN" align="right" runat="Server" class="Filter2" Text="    GEN FB!!!    " onClick="btnGENFG_Click" CausesValidation="False"/>        
				</td>
				<td width="10"><img runat="server" src="~/images/spacer.gif" width="20" height="18"/></td>
		</tr>
		<tr>
				<td><img runat="server" src="~/images/spacer.gif" width="20" height="1"/></td>
				<td colspan="3">
					<b id="rounded2bot">
					<i><span><img border="0" runat="server" src="~/images/spacer.gif" height="8"/></span></i> 
					</b>
				</td>
				<td><img runat="server" src="~/images/spacer.gif" width="20" height="1"/></td>
		</tr>
	</table>
</td>
<td>
		
	<table cellpadding="0" cellspacing="0" border="0" width="400">
		<tr>
				<td><img runat="server" src="~/images/spacer.gif" width="20" height="1"/></td>
				<td colspan="5"><b id="rounded2"><i><span><img border="0" runat="server" src="~/images/spacer.gif" height="8"/></span></i> </b></td>
				<td><img runat="server" src="~/images/spacer.gif" width="20" height="1"/></td>
		</tr>
		<tr>
				<td><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
				<td colspan="5" width="560" style="background-color: #ededed;" align="left">&nbsp;&nbsp;&nbsp;<span class="assetLink">Demographics</span>&nbsp;<span style="color:orange;font-family:arial; font-size: 10px;">(All users by age group)</span></td>
				<td><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
		</tr>
		<tr>
				<td><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
				<td align="right" width="50" class="bodyText" style="border-left: 1px solid #ededed;">&nbsp;<b>0-12:</b></td>
				<td><img runat="server" src="~/images/spacer.gif" width="5"/></td>
				<td align="left" width="115" class="bodyText">
					<asp:Label runat="server" id="lblDemo1"/>
				</td>
				<td align="center" width="120" class="bodyText">
					<B>M:</B>&nbsp;<asp:Label runat="server" id="lblGenderMale1"/>
				</td>
				<td align="center" width="120" style="border-right: 1px solid #ededed;" class="bodyText">
					<B>F:</B>&nbsp;<asp:Label runat="server" id="lblGenderFemale1"/>
				</td>
				<td width="10"><img runat="server" src="~/images/spacer.gif" width="20" height="18"/></td>
		</tr>
		<tr>
				<td><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
				<td align="right" class="bodyText" bgcolor="#ededed" style="border-left: 1px solid #ededed;">&nbsp;<b>13-17:</b></td>
				<td bgcolor="#ededed"><img runat="server" src="~/images/spacer.gif" width="5"/></td>
				<td align="left" class="bodyText" bgcolor="#ededed">
					<asp:Label runat="server" id="lblDemo2"/>
				</td>
				<td align="center" class="bodyText" bgcolor="#ededed">
					<B>M:</B>&nbsp;<asp:Label runat="server" id="lblGenderMale2"/>
				</td>
				<td align="center" bgcolor="#ededed" style="border-right: 1px solid #ededed;" class="bodyText">
					<B>F:</B>&nbsp;<asp:Label runat="server" id="lblGenderFemale2"/>
				</td>
				<td width="10"><img runat="server" src="~/images/spacer.gif" width="20" height="18"/></td>
		</tr>
		<tr>
				<td><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
				<td align="right" class="bodyText" style="border-left: 1px solid #ededed;">&nbsp;<b>18-20:</b></td>
				<td><img runat="server" src="~/images/spacer.gif" width="5"/></td>
				<td align="left" class="bodyText">
					<asp:Label runat="server" id="lblDemo3"/>
				</td>
				<td align="center" class="bodyText">
					<B>M:</B>&nbsp;<asp:Label runat="server" id="lblGenderMale3"/>
				</td>
				<td align="center" style="border-right: 1px solid #ededed;" class="bodyText">
					<B>F:</B>&nbsp;<asp:Label runat="server" id="lblGenderFemale3"/>
				</td>
				<td width="10"><img runat="server" src="~/images/spacer.gif" width="20" height="18"/></td>
		</tr>
		<tr>
				<td><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
				<td align="right" bgcolor="#ededed" class="bodyText" style="border-left: 1px solid #ededed;"><b>21-24:</b></td>
				<td bgcolor="#ededed"><img runat="server" src="~/images/spacer.gif" width="5"/></td>
				<td align="left" bgcolor="#ededed" class="bodyText">
					<asp:Label runat="server" id="lblDemo4"/>
				</td>
				<td align="center" class="bodyText" bgcolor="#ededed">
					<B>M:</B>&nbsp;<asp:Label runat="server" id="lblGenderMale4"/>
				</td>
				<td align="center" bgcolor="#ededed" style="border-right: 1px solid #ededed;" class="bodyText">
					<B>F:</B>&nbsp;<asp:Label runat="server" id="lblGenderFemale4"/>
				</td>
				<td width="10"><img runat="server" src="~/images/spacer.gif" width="20" height="18"/></td>
		</tr>  
		<tr>
				<td><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
				<td align="right" class="bodyText" style="border-left: 1px solid #ededed;">&nbsp;<b>25-34:</b></td>
				<td><img runat="server" src="~/images/spacer.gif" width="5"/></td>
				<td align="left" class="bodyText">
					<asp:Label runat="server" id="lblDemo5"/>
				</td>
				<td align="center" class="bodyText">
					<B>M:</B>&nbsp;<asp:Label runat="server" id="lblGenderMale5"/>
				</td>
				<td align="center" style="border-right: 1px solid #ededed;" class="bodyText">
					<B>F:</B>&nbsp;<asp:Label runat="server" id="lblGenderFemale5"/>
				</td>
				<td width="10"><img runat="server" src="~/images/spacer.gif" width="20" height="18"/></td>
		</tr>
		<tr>
				<td><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
				<td align="right" bgcolor="#ededed" class="bodyText" style="border-left: 1px solid #ededed;"><b>35-40:</b></td>
				<td bgcolor="#ededed"><img runat="server" src="~/images/spacer.gif" width="5"/></td>
				<td align="left" bgcolor="#ededed" class="bodyText">
					<asp:Label runat="server" id="lblDemo6"/>
				</td>
				<td align="center" class="bodyText" bgcolor="#ededed">
					<B>M:</B>&nbsp;<asp:Label runat="server" id="lblGenderMale6"/>
				</td>
				<td align="center" bgcolor="#ededed" style="border-right: 1px solid #ededed;" class="bodyText">
					<B>F:</B>&nbsp;<asp:Label runat="server" id="lblGenderFemale6"/>
				</td>
				<td width="10"><img runat="server" src="~/images/spacer.gif" width="20" height="18"/></td>
		</tr>  
		<tr>
				<td><img runat="server" src="~/images/spacer.gif" width="20" height="20" id="Img2"/></td>
				<td align="right" bgcolor="#ededed" class="bodyText" style="border-left: 1px solid #ededed;"><b>41-49:</b></td>
				<td bgcolor="#ededed"><img runat="server" src="~/images/spacer.gif" width="5" id="Img3"/></td>
				<td align="left" bgcolor="#ededed" class="bodyText">
					<asp:label runat="server" id="lblDemo7"/>
				</td>
				<td align="center" class="bodyText" bgcolor="#ededed">
					<b>M:</b>&nbsp;<asp:label runat="server" id="lblGenderMale7"/>
				</td>
				<td align="center" bgcolor="#ededed" style="border-right: 1px solid #ededed;" class="bodyText">
					<b>F:</b>&nbsp;<asp:label runat="server" id="lblGenderFemale7"/>
				</td>
				<td width="10"><img runat="server" src="~/images/spacer.gif" width="20" height="18" id="Img4"/></td>
		</tr>  
		<tr>
				<td><img runat="server" src="~/images/spacer.gif" width="20" height="20" id="Img6"/></td>
				<td align="right" bgcolor="#ededed" class="bodyText" style="border-left: 1px solid #ededed;"><b>50-54:</b></td>
				<td bgcolor="#ededed"><img runat="server" src="~/images/spacer.gif" width="5" id="Img8"/></td>
				<td align="left" bgcolor="#ededed" class="bodyText">
					<asp:label runat="server" id="lblDemo8"/>
				</td>
				<td align="center" class="bodyText" bgcolor="#ededed">
					<b>M:</b>&nbsp;<asp:label runat="server" id="lblGenderMale8"/>
				</td>
				<td align="center" bgcolor="#ededed" style="border-right: 1px solid #ededed;" class="bodyText">
					<b>F:</b>&nbsp;<asp:label runat="server" id="lblGenderFemale8"/>
				</td>
				<td width="10"><img runat="server" src="~/images/spacer.gif" width="20" height="18" id="Img9"/></td>
		</tr>  
		<tr>
				<td><img runat="server" src="~/images/spacer.gif" width="20" height="20" id="Img10"/></td>
				<td align="right" bgcolor="#ededed" class="bodyText" style="border-left: 1px solid #ededed;"><b>55+:</b></td>
				<td bgcolor="#ededed"><img runat="server" src="~/images/spacer.gif" width="5" id="Img11"/></td>
				<td align="left" bgcolor="#ededed" class="bodyText">
					<asp:label runat="server" id="lblDemo9"/>
				</td>
				<td align="center" class="bodyText" bgcolor="#ededed">
					<b>M:</b>&nbsp;<asp:label runat="server" id="lblGenderMale9"/>
				</td>
				<td align="center" bgcolor="#ededed" style="border-right: 1px solid #ededed;" class="bodyText">
					<b>F:</b>&nbsp;<asp:label runat="server" id="lblGenderFemale9"/>
				</td>
				<td width="10"><img runat="server" src="~/images/spacer.gif" width="20" height="18" id="Img12"/></td>
		</tr>  
		<tr>
				<td><img runat="server" src="~/images/spacer.gif" width="20" height="1"/></td>
				<td colspan="5">
					<b id="rounded2bot"><i><span><img border="0" runat="server" src="~/images/spacer.gif" height="8"/></span></i></b>
				</td>
				<td><img runat="server" src="~/images/spacer.gif" width="20" height="1"/></td>
		</tr>
	</table>
		
</td>
</tr>
</table>
	
<br>
<table cellpadding="0" cellspacing="0" border="0" width="905" ID="Table1">
	<tr>
		<td align="right" class="belowFilter2" valign="middle" style="line-height:15px;"><asp:Label runat="server" id="lblSearch" CssClass="dateStamp"/><br><Kaneva:Pager runat="server" id="pgTop"/><br><Kaneva:MembersFilter runat="server" id="filMembers" RolesEnabled="false" StatusEnabled="false" InitialItemsPerPage="30"/></td>						
	</tr>
</table>
<center>

<asp:DataGrid runat="server" EnableViewState="False" ShowFooter="False" Width="905" id="dgrdMembers" cellpadding="0" cellspacing="0" border="0" AutoGenerateColumns="False" AllowSorting="True" CssClass="FullBorders">  
	<HeaderStyle CssClass="lineItemColHead"/>
	<ItemStyle CssClass="lineItemEven"/>
	<AlternatingItemStyle CssClass="lineItemOdd"/>
	<Columns>
		<asp:TemplateColumn HeaderText="members" SortExpression="username" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="middle" ItemStyle-Width="20%">
			<ItemTemplate>
				<a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "username").ToString ())%> style="COLOR: #3258ba; font-size: 12px;"><%# DataBinder.Eval(Container.DataItem, "username") %></a>&nbsp;&nbsp;
				<br/><B>(<%# DataBinder.Eval(Container.DataItem, "first_name") %>&nbsp;<%# DataBinder.Eval(Container.DataItem, "last_name") %>)</B>
				<br/><%# DataBinder.Eval(Container.DataItem, "email") %>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="# logins" SortExpression="number_of_logins" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="Middle">
			<ItemTemplate>
				<span class="money"><%# DataBinder.Eval(Container.DataItem, "number_of_logins") %></span>&nbsp;
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="birth date" SortExpression="birth_date" ItemStyle-Width="10%" ItemStyle-ForeColor="#4863a2" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="Middle">
			<ItemTemplate>
				<span class="adminLinks"><%# FormatDate (DataBinder.Eval(Container.DataItem, "birth_date")) %></span>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="gender" SortExpression="gender" ItemStyle-Width="4%" ItemStyle-ForeColor="#4863a2" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="Middle">
			<ItemTemplate>
				<%# DataBinder.Eval(Container.DataItem, "gender") %>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="joined" SortExpression="user_id" ItemStyle-Width="20%" ItemStyle-ForeColor="#4863a2" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="Top">
			<ItemTemplate>
				<%# FormatDateTime (DataBinder.Eval(Container.DataItem, "signup_date")) %>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="last active" SortExpression="last_login" HeaderStyle-Wrap="False" ItemStyle-ForeColor="#4863a2" ItemStyle-Width="20%" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="Top">
			<ItemTemplate>
				<%# FormatDateTime (DataBinder.Eval(Container.DataItem, "last_login")) %>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="status" SortExpression="user_status_name" HeaderStyle-Wrap="False" ItemStyle-ForeColor="#4863a2" ItemStyle-Width="20%" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="Top">
			<ItemTemplate>
				<%# DataBinder.Eval(Container.DataItem, "user_status_name") %>				
				<BR/><asp:hyperlink id="hlBan" runat="server" visible='<%# !Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "status_id")).Equals (5)%>' NavigateURL='<%# GetSuspensionUrl (Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "user_id")))%>' ToolTip="Ban" Text="Ban"/>
				<asp:hyperlink id="hlRemoveBan" runat="server" visible='<%# Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "status_id")).Equals (5)%>' NavigateURL='<%# GetSuspensionUrl (Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "user_id")))%>' ToolTip="Remove Ban" Text="Remove Ban"/>
				
			</ItemTemplate>
		</asp:TemplateColumn>
		
	</Columns>
</asp:datagrid>

<br><br>
</center>
<input type="hidden" runat="server" id="txtUserId" value='0'> 

</asp:panel>