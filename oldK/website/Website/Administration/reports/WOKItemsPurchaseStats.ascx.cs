///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.SessionState;
using System.Globalization;
using log4net;

namespace KlausEnt.KEP.Kaneva
{
    public partial class WOKItemsPurchaseStats : BaseReportingUserControl
    {
        private void Page_Load(object sender, System.EventArgs e)
        {
            //security check to determine who can see this page
            if (IsUserAuthorized())
            {
                //show user control data area
                pnlAccGranted.Visible = true;
                tblAccessDenied.Visible = false;
            }
            else
            {
                pnlAccGranted.Visible = false;
                tblAccessDenied.Visible = true;
            }
        }

        #region main functionality
        /// <summary>
        /// Bind the data
        /// </summary>
        private void BindData()
        {
            // Set the sortable columns
            string orderby = CurrentSort + " " + CurrentSortOrder;

            DataTable dt = ReportUtility.GetWOKItemsPurchasedStats(StartDate, EndDate, orderby);
            if ((dt != null) && (dt.Rows.Count > 0))
            {
                //set the current page
                grdvw_WOKPurchases.PageIndex = currentPage;

                //set the sort order
                dt.DefaultView.Sort = orderby;

                lbl_Messages.Visible = true;

                //bind the data
                grdvw_WOKPurchases.DataSource = dt;
                grdvw_WOKPurchases.DataBind();
            }
            else
            {
                lbl_Messages.Visible = true;
                lbl_Messages.Text = "No WOK Purchase Statistics Were Found.";
            }
        }

        #endregion

        #region Properties

        public int PageSize
        {
            set
            {
                this.grdvw_WOKPurchases.PageSize = value;
            }
        }

        private DateTime StartDate
        {
            get
            {
                if (ViewState["_startDate"] == null)
                {
                    ViewState["_startDate"] = DateTime.Now;
                }
                return (DateTime)ViewState["_startDate"];
            }
            set
            {
                ViewState["_startDate"] = value;
            }
        }

        private DateTime EndDate
        {
            get
            {
                if (ViewState["_endDate"] == null)
                {
                    ViewState["_endDate"] = DateTime.Now;
                }
                return (DateTime)ViewState["_endDate"];
            }
            set
            {
                ViewState["_endDate"] = value;
            }
        }


        #endregion

        #region Events

        /// <summary>
        /// Search Event Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                EndDate  = Convert.ToDateTime(this.uc_EndDate.DateFieldValue + " " + this.uc_EndDate.TimeFieldValue);
                StartDate = Convert.ToDateTime(this.uc_StartDate.DateFieldValue + " " + this.uc_StartDate.TimeFieldValue);
            }
            catch (FormatException fe) 
            {
                lbl_Messages.Visible = true;
                this.lbl_Messages.Text = "Date Range Error: " + fe.Message;
            }
            BindData();
        }

        protected void grdvw_WOKPurchases_PageIndexChanging(object source, System.Web.UI.WebControls.GridViewPageEventArgs e)
        {
            base.gridview_PageIndexChanging(source, e);
            //bind data
            BindData();
        }

        /// <summary>
        /// They clicked to sort a column
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void grdvw_WOKPurchases_Sorting(object sender, GridViewSortEventArgs e)
        {
            base.gridview_Sorting(sender, e);
            //bind data
            BindData();
        }

        #endregion

        #region Web Form Designer generated code

        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}
