///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Globalization;
using log4net;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for downloadFeedback.
	/// </summary>
	public class downloadFeedback : BasePage
	{
		private void Page_Load(object sender, System.EventArgs e)
		{
			if (!IsAdministrator ())
			{
				return;
			}

			int feedbackId = Convert.ToInt32 (Request ["feedbackId"]);

			// Log the activity
            SiteMgmtFacade siteMgmtFacade = new SiteMgmtFacade();
            siteMgmtFacade.InsertCSRLog(GetUserId(), "Admin - Downloading KPG feedback " + feedbackId, 0, 0);

			DataRow drFeedback = StoreUtility.GetFeedback (feedbackId);

			string filepath = drFeedback ["file_link"].ToString ();

			if (System.IO.File.Exists (filepath))
			{
				try
				{
					string filename = System.IO.Path.GetFileName (filepath);

					Response.Clear ();
					Response.ContentType = "application/octet-stream";
					Response.AddHeader ("Content-Length", new System.IO.FileInfo (filepath).Length.ToString());
					Response.AddHeader ("Content-Disposition", "attachment; filename=\"" + filename + "\"");
					Response.Flush ();
					Response.TransmitFile (filepath);
					Response.End ();
				}
				catch (Exception exc)
				{
					m_logger.Error ("Error downloading feedback file", exc);
				}
				finally 
				{
					Response.End ();
				}
			}
			else
			{
				Response.Write ("<B>Feedback file was not found on server!</B>");
				m_logger.Error ("Feedback file was not found on server" + filepath);
			}
		}

		// Logger
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
