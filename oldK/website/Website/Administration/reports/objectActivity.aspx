<%@ Page language="c#" Codebehind="objectActivity.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.objectActivity" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../../usercontrols/Pager.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="ReportNav" Src="reportNav.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="AdminMenu" Src="../../usercontrols/AdministrativeMenu.ascx" %>

<Kaneva:AdminMenu ID="adminmenu" runat="server" />
<asp:ValidationSummary ShowMessageBox="True" ShowSummary="False" Class="formError" id="valSum" DisplayMode="BulletList" runat="server" HeaderText="Please correct the following errors:"/>

<table runat="server" id="tblNoAdmin" cellpadding="0" cellspacing="0" border="0" width="560">
	<tr><td ALIGN="CENTER">
		<BR><BR><BR>
		<span class="subHead">You must be a site administrator to access this section.</span>
	</td></tr>
</table>

<asp:panel runat="server" ID="pnlAdmin">

<Kaneva:ReportNav runat="server" id="rnNavigation" SubTab="oa"/>
<center>
<BR>
<table cellpadding="0" cellspacing="0" border="0" width="750">
	<tr>
		<td valign="middle" align="right" class="Filter2" width="25%" nowrap="true">
			Search from&nbsp;
		</td>
		<td valign="middle" align="left" class="Filter2" width="25%" nowrap="true">
			<asp:TextBox ID="txtStartDate" class="Filter2" Width="100" MaxLength="10" runat="server"/> (MM-dd-yyyy) 12:00:00 AM
			<asp:RequiredFieldValidator ID="rfdtxtStartDate" ControlToValidate="txtStartDate" Text="*" ErrorMessage="Start date is a required field." Display="Dynamic" runat="server"/>
			<asp:RegularExpressionValidator id="revStartDate" ControlToValidate="txtStartDate" Text="*" Display="Static" ErrorMessage="Start date must be in format mm-dd-yyyy." EnableClientScript="True" runat="server"/>
		</td>
		<td width="50%" colspan="2">
		</td>
	</tr>
	<tr>
		<td valign="middle" align="right" class="Filter2" nowrap="true">
			to&nbsp;
		</td>
		<td valign="middle" align="left" class="Filter2" nowrap="true">
			<asp:TextBox ID="txtEndDate" class="Filter2" Width="100" MaxLength="10" runat="server"/> (MM-dd-yyyy) 11:59:59 PM
			<asp:RequiredFieldValidator ID="rfdtxtEndDate" ControlToValidate="txtEndDate" Text="*" ErrorMessage="End date is a required field." Display="Dynamic" runat="server"/>
			<asp:RegularExpressionValidator id="revEndDate" ControlToValidate="txtEndDate" Text="*" Display="Static" ErrorMessage="End date must be in format mm-dd-yyyy." EnableClientScript="True" runat="server"/>
		</td>
		<td width="10%" valign="middle" align="center">
		</td>
		<td width="40%" valign="middle" align="left">
			<asp:button id="btnSearch" runat="Server" CausesValidation="True" onClick="btnSearch_Click" class="Filter2" Text=" search "/>
		</td>
	</tr>
</table>	
<BR>
<table runat="server" cellpadding="0" cellspacing="0" border="0" width="905">
	<tr Class="lineItemColHead">
		<td width="100%" align="left">&nbsp;&nbsp;<span class="header02" style="font-size: 15px;"><b><asp:Label class="dateStamp" id="lblEarnings" runat="server"/><b></b></span>
		</td>
	</tr>
</table>
<table cellpadding="0" cellspacing="0" border="0" width="905"class="FullBorders">
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
		<td align="right" class="bodyText" width="30%">&nbsp;Communities Created:</td><td><img runat="server" src="~/images/spacer.gif" width="5"/></td>
		<td align="left" width="70%" class="bodyText">
			<asp:Label class="dateStamp" id="lblCommsCreated" runat="server"/>&nbsp;
		</td>
		<td width="10"><img runat="server" src="~/images/spacer.gif" width="20" height="18"/></td>
	</tr>
	<tr class="lineItemOdd">
		<td><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
		<td align="right" class="bodyText">&nbsp;Communities Updated:</td><td ><img runat="server" src="~/images/spacer.gif" width="5"/></td>
		<td align="left" width="260" class="bodyText">
			<asp:Label class="dateStamp" id="lblCommsUpdated" runat="server"/>&nbsp;
		</td>
		<td width="10"><img runat="server" src="~/images/spacer.gif" width="20" height="18"/></td>
	</tr>
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
		<td align="right" class="bodyText">&nbsp;Communities Deleted:</td><td><img runat="server" src="~/images/spacer.gif" width="5"/></td>
		<td align="left" width="260" class="bodyText">
			<asp:Label class="dateStamp" id="lblCommsDeleted" runat="server"/>&nbsp;
		</td>
		<td width="10"><img runat="server" src="~/images/spacer.gif" width="20" height="18"/></td>
	</tr>
</table>	

<br><br>
<table runat="server" cellpadding="0" cellspacing="0" border="0" width="905">
	<tr Class="lineItemColHead">
		<td width="100%" align="Left">&nbsp;&nbsp;<b><asp:Label class="dateStamp" id="lblChannels" runat="server"/></b>
		</td>
	</tr>
</table>
<table cellpadding="0" cellspacing="0" border="0" width="905">
	<tr>
		<td align="right" class="belowFilter2" valign="middle" style="line-height:15px;"><asp:Label runat="server" id="lblSearch" CssClass="dateStamp"/><br><Kaneva:Pager runat="server" id="pgTop"/><br></td>						
	</tr>
</table>
<asp:DataGrid runat="server" EnableViewState="False" ShowFooter="False" Width="905" id="dgrdChannels" cellpadding="0" cellspacing="0" border="0" AutoGenerateColumns="False" AllowSorting="True" CssClass="FullBorders">  
	<HeaderStyle CssClass="lineItemColHead"/>
	<ItemStyle CssClass="lineItemEven"/>
	<AlternatingItemStyle CssClass="lineItemOdd"/>
	<Columns>
		<asp:TemplateColumn HeaderText="name" SortExpression="name" ItemStyle-Width="30%" ItemStyle-HorizontalAlign="Left">
			<ItemTemplate>
				<a href='<%# GetBroadcastChannelUrl (DataBinder.Eval (Container.DataItem, "name_no_spaces").ToString ())%>'><%# TruncateWithEllipsis (DataBinder.Eval (Container.DataItem, "name").ToString (), 40)%></a>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="members" SortExpression="number_of_members" ItemStyle-CssClass="money" ItemStyle-Width="5%" ItemStyle-HorizontalAlign="Center">
			<ItemTemplate>
				<%# DataBinder.Eval (Container.DataItem, "number_of_members") %>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="owner" SortExpression="creator_username" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Center">
			<ItemTemplate>
				<a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "creator_username").ToString ())%> style="COLOR: #3258ba; font-size: 12px;"><%# DataBinder.Eval(Container.DataItem, "creator_username") %></a>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="created date" SortExpression="created_date" ItemStyle-ForeColor="#4863a2" ItemStyle-Width="20%" ItemStyle-HorizontalAlign="Center">
			<ItemTemplate>
				<%# FormatDateTime (DataBinder.Eval (Container.DataItem, "created_date")) %>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="updated date" SortExpression="last_edit" ItemStyle-ForeColor="#4863a2" ItemStyle-Width="20%" ItemStyle-HorizontalAlign="Center">
			<ItemTemplate>
				<%# FormatDateTime (DataBinder.Eval (Container.DataItem, "last_edit")) %>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="status" SortExpression="description" ItemStyle-CssClass="adminLinks" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center">
			<ItemTemplate>
				<%# DataBinder.Eval (Container.DataItem, "description") %>
			</ItemTemplate>
		</asp:TemplateColumn>
	</Columns>
</asp:datagrid>

<br><br>


</center>
</asp:panel>