<%@ Page language="c#" Codebehind="platformLicense.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.platformLicense" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../../usercontrols/Pager.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="ReportNav" Src="reportNav.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="AdminMenu" Src="../../usercontrols/AdministrativeMenu.ascx" %>

<Kaneva:AdminMenu ID="adminmenu" runat="server" />
<table runat="server" id="tblNoAdmin" cellpadding="0" cellspacing="0" border="0" width="560">
	<tr><td ALIGN="CENTER">
		<BR><BR><BR>
		<span class="subHead">You must be a site administrator to access this section.</span>
	</td></tr>
</table>

<asp:panel runat="server" ID="pnlAdmin">

<Kaneva:ReportNav runat="server" id="rnNavigation" SubTab="pl"/>
<center>

<br><br>	
<table cellpadding="0" cellspacing="0" border="0" width="905">
	<tr>
		<td align="right" class="belowFilter2" valign="middle" style="line-height:15px;">
			<asp:Label runat="server" id="lblSearch" CssClass="dateStamp"/><br><Kaneva:Pager runat="server" id="pgTop"/>
		</td>
	</tr>
</table>
<asp:DataGrid runat="server" EnableViewState="False" ShowFooter="False" Width="905" id="dgrdLicenses" cellpadding="0" cellspacing="0" border="0" AutoGenerateColumns="False" AllowSorting="True" CssClass="FullBorders">  
	<HeaderStyle CssClass="lineItemColHead"/>
	<ItemStyle CssClass="lineItemEven"/>
	<AlternatingItemStyle CssClass="lineItemOdd"/>
	<Columns>
		<asp:TemplateColumn HeaderText="type" SortExpression="license_type" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="middle" ItemStyle-Width="5%">
			<ItemTemplate>
				<%# GetGameType (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "license_type"))) %>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="game" SortExpression="asset_name" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="middle" ItemStyle-Width="20%">
			<ItemTemplate>
				<a class="adminLinks" title='<%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "asset_name").ToString ())%>' href='<%# GetAssetDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "asset_id")))%>'><%# TruncateWithEllipsis (Server.HtmlDecode (DataBinder.Eval(Container.DataItem, "asset_name").ToString ()), 35) %></a>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="server name" SortExpression="server_name" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="middle" ItemStyle-Width="20%">
			<ItemTemplate>
				<span title='<%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "server_name").ToString ()) %>'><%# TruncateWithEllipsis (Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "server_name").ToString ()), 35) %></span>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="key" SortExpression="game_key" ItemStyle-CssClass="adminLinks" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="middle" ItemStyle-Width="20%">
			<ItemTemplate>
				<%# DataBinder.Eval (Container.DataItem, "game_key") %>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="max users" ItemStyle-CssClass="money" SortExpression="max_server_users" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="middle" ItemStyle-Width="5%">
			<ItemTemplate>
				<%# DataBinder.Eval (Container.DataItem, "max_server_users") %> 
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="last ping" SortExpression="last_ping_datetime" ItemStyle-Width="15%" ItemStyle-ForeColor="#4863a2" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="Top">
			<ItemTemplate>
				<a href='<%#GetPingHistoryLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "engine_id")))%>' runat="server" class="adminLinks"><%# FormatDateTime (DataBinder.Eval(Container.DataItem, "last_ping_datetime"))%></a>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="status" SortExpression="status_name" ItemStyle-CssClass="adminLinks" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="middle" ItemStyle-Width="10%">
			<ItemTemplate>
				<%# DataBinder.Eval (Container.DataItem, "status_name") %>
			</ItemTemplate>
		</asp:TemplateColumn>
	</Columns>
</asp:datagrid>

<br><br>
</center>
</asp:panel>
