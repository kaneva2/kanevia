<%@ Control Language="c#" AutoEventWireup="false" Codebehind="reportNav.ascx.cs" Inherits="KanevaWeb.reports.reportNav" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>

<style type="text/css">
	#reportNav a {
		text-decoration: none;
		font-weight: normal;	
		}
	#reportNav { background: #ff6666;
		PADDING-RIGHT: 120px; 
		PADDING-LEFT: 4px; 
		FONT-SIZE: 10px; 
		PADDING-BOTTOM: 7px;  
		COLOR: #000000; 
		LINE-HEIGHT: 14px; 
		PADDING-TOP: 7px;
		}
</style>

<table width="905" CellPadding="0" CellSpacing="0" Border="0" id="reportNav">
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="4" height="1"></td>
		<td runat="server" id="tdReportTabs" >
			  <a href="~/Administration/reports/KPI.aspx" runat="server" id="aKPI">KPI</a> |
			  <a href="~/Administration/reports/cohort.aspx" runat="server" id="aCohort">Cohort</a> |
				<a href="~/Administration/reports/earnings.aspx" runat="server" id="aEarnings">earnings</a> |
				<a href="~/Administration/reports/sales.aspx" runat="server" id="aSales">orders</a> |
				<a href="~/Administration/reports/userActivity.aspx" runat="server" id="aUserActivity">user activity</a> |
				<a href="~/Administration/reports/loginAttempts.aspx" runat="server" id="aInvalidLogin">invalid logins</a> |
				<a href="~/Administration/reports/exceptions.aspx" runat="server" id="aExceptions">exceptions</a> |
				<a href="~/Administration/reports/objectActivity.aspx" runat="server" id="aObjectActivity">community activity</a> |
				<a href="~/Administration/reports/itemActivity.aspx" runat="server" id="aItemActivity">item activity</a> <br />
				<a href="~/Administration/reports/platformDownloads.aspx" runat="server" id="aPlatformDownloads">KGP downloads</a> |
				<a href="~/Administration/reports/platformLicense.aspx" runat="server" id="aPlatformLicense">KGP licenses</a> |
				<a href="~/Administration/reports/KGPFeedback.aspx" runat="server" id="aPlatformFeedback">KGP feedback</a> |
				<a href="~/Administration/AdministrationContainer.aspx" runat="server" id="a1">WOK Item Purchases</a> |
				<a href="~/Administration/reports/ViolationReports.aspx" runat="server" id="a2">Reported Violations</a> |
				<a href="~/Administration/reports/friendInvites.aspx" runat="server" id="aFriendInvites">Declined Invites</a>
		</td>
	</tr>
</table>
