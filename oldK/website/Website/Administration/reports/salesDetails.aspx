<%@ Page language="c#" Codebehind="salesDetails.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.salesDetails" %>
<%@ Register TagPrefix="Kaneva" TagName="ReportNav" Src="reportNav.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="AdminMenu" Src="../../usercontrols/AdministrativeMenu.ascx" %>

<Kaneva:AdminMenu ID="adminmenu" runat="server" />
<table runat="server" id="tblNoAdmin" cellpadding="0" cellspacing="0" border="0" width="560">
	<tr><td ALIGN="CENTER">
		<BR><BR><BR>
		<span class="subHead">You must be a site administrator to access this section.</span>
	</td></tr>
</table>

<asp:panel runat="server" ID="pnlAdmin">

<Kaneva:ReportNav runat="server" id="rnNavigation" SubTab="sa"/>
<center>
<BR>

<table cellpadding="0" cellspacing="0" border="0" width="750">
	<tr>
		<td colspan="5" style="background-color: #cccccc;">
			<b id="rounded"><li><i><span><img border="0" runat="server" src="~/images/spacer.gif" width="750" height="8"/></span></i></li></b>
		</td>
	</tr>
	<tr>
		<td colspan="5" style="background-color: #cccccc;">
			<span class="blogHeadline">&nbsp;&nbsp;Order Details</span>
		</td>
	</tr>
	<tr class="lineItemEven">
		<td><img runat="server" src="~/images/spacer.gif" width="11" height="20"/></td>
		<td align="right" class="bodyText">&nbsp;order&nbsp;id:</td>
		<td><img runat="server" src="~/images/spacer.gif" width="5"/></td>
		<td align="left" width="700" class="bodyText" >
			<b><asp:Label class="filter2" style="font-size: 12px;" id="lblOrderId" runat="server"/></b>
		</td>
		<td width="10"><img runat="server" src="~/images/spacer.gif" width="20" height="18"/></td>
	</tr>
	<tr class="lineItemOdd">
		<td><img runat="server" src="~/images/spacer.gif" width="11" height="20"/></td>
		<td align="right" class="bodyText" >&nbsp;description:</td><td><img runat="server" src="~/images/spacer.gif" width="5"/></td>
		<td align="left" width="700" class="bodyText" >
			<asp:Label class="adminLinks" id="lblDescription" runat="server"/>
		</td>
		<td width="10"><img runat="server" src="~/images/spacer.gif" width="20" height="18"/></td>
	</tr>
	<tr class="lineItemEven">
		<td><img runat="server" src="~/images/spacer.gif" width="11" height="20"/></td>
		<td align="right" class="bodyText"  nowrap="true">&nbsp;amount:</td>
		<td><img runat="server" src="~/images/spacer.gif" width="5"/></td>
		<td align="left" width="700" class="bodyText" >
			<asp:Label class="money" id="lblAmount" runat="server"/>
		</td>
		<td width="10"><img runat="server" src="~/images/spacer.gif" width="20" height="18"/></td>
	</tr>
	<tr class="lineItemOdd">
		<td><img runat="server" src="~/images/spacer.gif" width="11" height="20"/></td>
		<td align="right" class="bodyText" >&nbsp;k-Points&nbsp;received:</td><td><img runat="server" src="~/images/spacer.gif" width="5"/></td>
		<td align="left" width="700" class="bodyText" >
			<asp:Label class="money" id="lblKPoints" runat="server"/>
		</td>
		<td width="10"><img runat="server" src="~/images/spacer.gif" width="20" height="18"/></td>
	</tr>
	<tr class="lineItemEven">
		<td><img runat="server" src="~/images/spacer.gif" width="11" height="20"/></td>
		<td align="right" class="bodyText"  nowrap="true">&nbsp;payment&nbsp;method:</td>
		<td><img runat="server" src="~/images/spacer.gif" width="5"/></td>
		<td align="left" width="700" class="bodyText" >
			<asp:Label id="lblPaymentMethod" runat="server"/>
		</td>
		<td width="10"><img runat="server" src="~/images/spacer.gif" width="20" height="18"/></td>
	</tr>
	<tr class="lineItemOdd">
		<td><img runat="server" src="~/images/spacer.gif" width="11" height="20"/></td>
		<td align="right" class="bodyText" >&nbsp;status:</td><td><img runat="server" src="~/images/spacer.gif" width="5"/></td>
		<td align="left" width="700" class="bodyText" >
			<asp:Label class="filter2" id="lblStatus" runat="server"/>
			<A runat="server" id="aStatus" style="color: #4863a2; font-size: 11px;"></a>&nbsp;&nbsp;&nbsp;<asp:button id="btnRefund" runat="server" onclick="btnRefund_OnClick" text="Mark Payment as Refunded"/>
		</td>
		<td width="10"><img runat="server" src="~/images/spacer.gif" width="20" height="18"/></td>
	</tr>
	<tr class="lineItemEven">
		<td><img runat="server" src="~/images/spacer.gif" width="11" height="20"/></td>
		<td align="right" class="bodyText"  nowrap="true">&nbsp;purchase&nbsp;date:</td>
		<td><img runat="server" src="~/images/spacer.gif" width="5"/></td>
		<td align="left" width="700" class="adminLinks" >
			<asp:Label id="lblPurchaseDate" runat="server"/>
		</td>
		<td width="10"><img runat="server" src="~/images/spacer.gif" width="20" height="18"/></td>
	</tr>
	<tr class="lineItemOdd">
		<td><img runat="server" src="~/images/spacer.gif" width="11" height="20"/></td>
		<td align="right" class="bodyText" >&nbsp;username:</td><td><img runat="server" src="~/images/spacer.gif" width="5"/></td>
		<td align="left" width="700" class="bodyText" >
			<A runat="server" id="aUsername" style="color: #4863a2; font-size: 11px;"></a>
		</td>
		<td width="10"><img runat="server" src="~/images/spacer.gif" width="20" height="18"/></td>
	</tr>
	<tr class="lineItemEven">
		<td><img runat="server" src="~/images/spacer.gif" width="11" height="20"/></td>
		<td align="right" class="bodyText"  nowrap="true">&nbsp;address:</td>
		<td><img runat="server" src="~/images/spacer.gif" width="5"/></td>
		<td align="left" width="700" class="bodyText" >
			<asp:Label id="lblAddress" runat="server"/>
		</td>
		<td width="10"><img runat="server" src="~/images/spacer.gif" width="20" height="18"/></td>
	</tr>
	<tr class="lineItemOdd">
		<td><img runat="server" src="~/images/spacer.gif" width="11" height="20"/></td>
		<td align="right" class="bodyText" >&nbsp;billing info:</td>
		<td><img runat="server" src="~/images/spacer.gif" width="5"/></td>
		<td align="left" width="700" class="bodyText" >
			<asp:Label id="lblCreditInfo" runat="server"/>
		</td>
		<td width="10"><img runat="server" src="~/images/spacer.gif" width="20" height="18"/></td>
	</tr>
	<tr class="lineItemEven">
		<td><img runat="server" src="~/images/spacer.gif" width="11" height="20"/></td>
		<td align="right" class="bodyText"  nowrap="true">&nbsp;Error:</td>
		<td><img runat="server" src="~/images/spacer.gif" width="5"/></td>
		<td align="left" width="700" class="bodyText" >
			<asp:Label id="lblErrorDescription" runat="server"/>
		</td>
		<td width="10"><img runat="server" src="~/images/spacer.gif" width="20" height="18"/></td>
	</tr>

	
</table><br><br>



<br><br>
</center>
</asp:panel>

