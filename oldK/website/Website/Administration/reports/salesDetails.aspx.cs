///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Globalization;
using log4net;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for salesDetails.
	/// </summary>
    public class salesDetails : NoBorderPage
	{
		protected salesDetails () 
		{
			Title = "Admin - K-Point Sale Details";
		}

		private void Page_Load (object sender, System.EventArgs e)
		{
			pnlAdmin.Visible = (IsAdministrator ());
			tblNoAdmin.Visible = (!IsAdministrator ());

			if (!IsPostBack)
			{
				BindData ();
			}
		}

		/// <summary>
		/// Bind the data
		/// </summary>
		private void BindData ()
		{
			int pointTransactionId = Convert.ToInt32 (Request ["ptId"]);

			// Log the activity
            SiteMgmtFacade siteMgmtFacade = new SiteMgmtFacade();
            siteMgmtFacade.InsertCSRLog(GetUserId(), "Admin - Viewing Sales Details for point transaction id = " + pointTransactionId, 0, 0);

			// Get the info
			DataRow drPpt = StoreUtility.GetPurchasePointTransaction (pointTransactionId);

			int userId = Convert.ToInt32 (drPpt ["user_id"]);
			lblOrderId.Text = "KEN-" + drPpt ["order_id"].ToString ();
			lblDescription.Text = drPpt ["description"].ToString ();
			lblAmount.Text = FormatCurrency (drPpt ["amount_debited"]);
			lblKPoints.Text = FormatKpoints (drPpt ["totalPoints"], false);
			lblPaymentMethod.Text = drPpt ["name"].ToString ();
			lblPurchaseDate.Text = FormatDateTime (drPpt ["transaction_date"]);

			if (Convert.ToInt32 (drPpt ["transaction_status"]).Equals ((int) Constants.eTRANSACTION_STATUS.VERIFIED))
			{
				lblErrorDescription.Text = "Ok";
			}
			else
			{
				lblErrorDescription.Text = drPpt ["error_description"].ToString ();
			}

			aStatus.HRef = GetTransactionStatusLink (Convert.ToInt32 (drPpt ["transaction_status"]), drPpt ["status_description"].ToString (), Convert.ToDateTime (drPpt ["transaction_date"]));
			aStatus.InnerHtml = GetTransactionStatus (Convert.ToInt32 (drPpt ["transaction_status"]), drPpt ["status_description"].ToString (), Convert.ToDateTime (drPpt ["transaction_date"]));
			aUsername.HRef = GetPersonalChannelUrl (drPpt ["username"].ToString ());
			aUsername.InnerText = drPpt ["username"].ToString ();

            btnRefund.Enabled = Convert.ToInt32(drPpt["transaction_status"]).Equals((int) Constants.eTRANSACTION_STATUS.VERIFIED);

			// What is the payment method?
			int paymentMethodId = Convert.ToInt32 (drPpt ["payment_method_id"]);

			if (paymentMethodId.Equals ((int) Constants.ePAYMENT_METHODS.PAYPAL))
			{
				lblCreditInfo.Text = "Pay using PayPal";
				lblAddress.Text = "Not required for PayPal";
			}
			else
			{
				// Credit card
				if (!drPpt ["billing_information_id"].Equals (DBNull.Value))
				{
					int billingInfoId = Convert.ToInt32 (drPpt ["billing_information_id"]);
					DataRow drCreditCard = UsersUtility.GetCreditCard (userId, billingInfoId);
					lblCreditInfo.Text = drCreditCard ["name_on_card"].ToString () + "<br>" +
						drCreditCard ["card_type"].ToString () + ":" + ShowCreditCardNumber (drCreditCard ["card_number"].ToString ()) + "<br>" +
						"Exp: " + drCreditCard ["exp_month"].ToString () + "/" + drCreditCard ["exp_year"].ToString () + "<BR>";
				}
				else
				{
					lblCreditInfo.Text = "Unknown";
				}

				if (!drPpt ["address_id"].Equals (DBNull.Value))
				{
					// Address
					int addressId = Convert.ToInt32 (drPpt ["address_id"]);
					DataRow drAddress = UsersUtility.GetAddress (userId, addressId);

					if (drAddress != null)
					{
						lblAddress.Text = drAddress ["name"].ToString () + "<BR>" +
							drAddress ["address1"].ToString () + "<BR>";;
						if (!drAddress ["address2"].Equals (DBNull.Value) && drAddress ["address2"].ToString ().Length > 0)
						{
							lblAddress.Text += drAddress ["address2"].ToString () + "<BR>";
						}
						lblAddress.Text += drAddress ["city"].ToString () + "," + drAddress ["state_name"].ToString () + " " + drAddress ["zip_code"].ToString () + "<BR>" +
							drAddress ["country_name"].ToString () + "<BR>";
					}
					else
					{
						lblAddress.Text = "Address not found";
					}
				}
				else
				{
					lblAddress.Text = "Unknown";
				}
			}
		}

        protected void btnRefund_OnClick(object sender, EventArgs e)
        {
            int pointTransactionId = Convert.ToInt32(Request["ptId"]);

            // Log the activity
            SiteMgmtFacade siteMgmtFacade = new SiteMgmtFacade();
            siteMgmtFacade.InsertCSRLog(GetUserId(), "Admin - Refunding transaction id = " + pointTransactionId, 0, 0);

            // Get the info
            DataRow drPpt = StoreUtility.GetPurchasePointTransaction(pointTransactionId);

            StoreUtility.UpdatePointTransaction(pointTransactionId, (int)Constants.eTRANSACTION_STATUS.REFUNDED, "", "");

            // Update order
            StoreUtility.UpdateOrderToRefunded(Convert.ToInt32(drPpt["order_id"]));

            BindData();
        }

		protected Label lblOrderId, lblDescription, lblAmount, lblKPoints, lblPaymentMethod, lblPurchaseDate, lblCreditInfo, lblAddress, lblErrorDescription;
		protected HtmlAnchor aStatus, aUsername;

		protected HtmlTable tblNoAdmin;
		protected Panel pnlAdmin;
        protected Button btnRefund;

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
