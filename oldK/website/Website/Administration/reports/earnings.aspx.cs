///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using System.Globalization;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for earnings.
	/// </summary>
    public class earnings : NoBorderPage
	{
		protected earnings () 
		{
			Title = "Admin - Earnings";
		}

		private void Page_Load (object sender, System.EventArgs e)
		{
			pnlAdmin.Visible = (IsAdministrator ());
			tblNoAdmin.Visible = (!IsAdministrator ());

			// Date validators
			revStartDate.ValidationExpression = Constants.VALIDATION_REGEX_DATE;
			revEndDate.ValidationExpression = Constants.VALIDATION_REGEX_DATE;

			if (!IsPostBack)
			{
				lblEarnings.Text = "Earnings Report (Click search to generate report...)";
				txtStartDate.Text = KanevaGlobals.FormatDateNumbersOnly (KanevaGlobals.GetCurrentDateTime ());
				txtEndDate.Text = KanevaGlobals.FormatDateNumbersOnly (KanevaGlobals.GetCurrentDateTime ());
				BindData (1);
			}

		}

		/// <summary>
		/// Search Event Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnSearch_Click (object sender, EventArgs e) 
		{
			BindData (1);
		}

		/// <summary>
		/// Bind the data
		/// </summary>
		/// <param name="pageNumber"></param>
		private void BindData (int pageNumber)
		{
			lblEarnings.Text = "Earnings Report from '" + txtStartDate.Text + " 12:00 AM' to '" + txtEndDate.Text + " 11:59 PM'";
			lblSales.Text = "Sales Report from '" + txtStartDate.Text + " 12:00 AM' to '" + txtEndDate.Text + " 11:59 PM'";

			// Log the activity
            SiteMgmtFacade siteMgmtFacade = new SiteMgmtFacade();
            siteMgmtFacade.InsertCSRLog(GetUserId(), "Admin - " + lblEarnings.Text, 0, 0);

			DateTime dtStartDate = KanevaGlobals.UnFormatDate (txtStartDate.Text + " 00:00:00");
			DateTime dtEndDate = KanevaGlobals.UnFormatDate (txtEndDate.Text + " 23:59:59");

			// Non subscriptions (K-point purchases, buckets and custom amounts)
			DataRow drKPointPurchases = ReportUtility.GetCashEarnings (dtStartDate, dtEndDate);
			Double dKpointsSold = ReportUtility.GetKPointEarnings (dtStartDate, dtEndDate, Constants.CURR_KPOINT);
			Double dMpointsSold = ReportUtility.GetKPointEarnings (dtStartDate, dtEndDate, Constants.CURR_MPOINT);
			lblKPointsSold.Text = "<B>" + KanevaGlobals.FormatKPoints (dKpointsSold, false, false) + "</B> K-Points<BR>" +
				"<B>" + KanevaGlobals.FormatKPoints (dMpointsSold, false, false) + "</B> M-Points<BR>" + 
				"<B>" + KanevaGlobals.FormatCurrency (Convert.ToDouble (drKPointPurchases ["cash_total"])) + "</B> Dollars";
			lblKPointTransactions.Text = "In <B>" + drKPointPurchases ["count"].ToString () + "</B> transactions";

			// Subscriptions
			//DataRow drSubscriptions = ReportUtility.GetCashEarnings (dtStartDate, dtEndDate);
            //lblSubscriptionsSold.Text = "<B>" + KanevaGlobals.FormatCurrency (Convert.ToDouble (drSubscriptions ["cash_total"])) + "</B> Dollars";
            //lblSubTransactions.Text = "<B>" + drSubscriptions ["count"].ToString () + "</B> subscriptions paid<BR>" +
            //    "(There were <B>" + ReportUtility.GetNewSubscriptions (dtStartDate, dtEndDate).ToString () + "</B> new signups<BR>" +
            //    "and <B>" + ReportUtility.GetCancelledSubscriptions (dtStartDate, dtEndDate).ToString () + "</B> cancellations)";
            lblSubscriptionsSold.Text = "TBD";
            lblSubTransactions.Text = "TBD";

			// K-points spent
			DataRow drKSpent = ReportUtility.GetKPointsSpent (dtStartDate, dtEndDate);
			lblKPointsSpent.Text = "<B>" + KanevaGlobals.FormatKPoints (Convert.ToDouble (drKSpent ["amount"]), false, false) + "</B> K-Points";
			lblKPointSpentTransactions.Text = "For <B>" + drKSpent ["count"].ToString () + "</B> items<BR>" +
				"(<B>" + drKSpent ["a_count"].ToString () + "</B> assets<BR><B>" +
				drKSpent ["feat_count"].ToString () + "</B> featured assets<BR><B>" +
				drKSpent ["cms_count"].ToString () + "</B> community subscriptions<BR><B>" +
				drKSpent ["uas_count"].ToString () + "</B> game subscriptions)";

			// Royalties earned
			DataRow drRoyalties = ReportUtility.GetRoyalties (dtStartDate, dtEndDate);
			lblRoyaltyTotal.Text = "<B>" + KanevaGlobals.FormatKPoints (Convert.ToDouble (drRoyalties ["amount"]), false, false) + "</B> K-Points<BR><B>" + 
				KanevaGlobals.FormatCurrency (StoreUtility.ConvertKPointsToDollars (Convert.ToDouble (drRoyalties ["amount"]))) + "</B> Dollars";
			lblRoyaltyTransactions.Text = "For <B>" + drRoyalties ["count"].ToString () + "</B> items";

			// Outstanding Balance
			DataRow drCurrentUserBalances = ReportUtility.GetCurrentUserBalances ();
			lblBalance.Text = "<B>" + FormatKpoints (ReportUtility.GetOutstandingPointBalance (dtStartDate), false) + "</B> Points on '" + txtStartDate.Text + " 12:00 AM'<BR/>" +
				"<B>" + FormatKpoints (ReportUtility.GetOutstandingPointBalance (dtEndDate), false) + "</B> Points on '" + txtEndDate.Text + " 11:59 PM'<BR><BR>" +
				"<B>" + FormatKpoints (drCurrentUserBalances ["balance"]) + "</B> Points on " + FormatDateTime (drCurrentUserBalances ["the_date"]);

			// Sales
			SetHeaderSortText (dgrdSales);
			string orderby = CurrentSort + " " + CurrentSortOrder;

			int itemsPerPage = 50;
			PagedDataTable pds = ReportUtility.GetSales (dtStartDate, dtEndDate, orderby, pgTop.CurrentPageNumber, itemsPerPage);
			pgTop.NumberOfPages = Math.Ceiling ((double) pds.TotalCount / itemsPerPage).ToString ();
			pgTop.DrawControl ();

			dgrdSales.DataSource = pds;
			dgrdSales.DataBind ();

			// The results
			lblSearch.Text = GetResultsText (pds.TotalCount, pageNumber, itemsPerPage, pds.Rows.Count);
		}

		/// <summary>
		/// They clicked to sort a column
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected override void btnSort_Click (object sender, System.EventArgs e)
		{
			base.btnSort_Click (sender, e);
			BindData (pgTop.CurrentPageNumber);
		}

		/// <summary>
		/// Pager change event
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pg_PageChange (object sender, PageChangeEventArgs e)
		{
			BindData (e.PageNumber);
		}

		protected HtmlTable tblNoAdmin;
		protected Panel pnlAdmin;

		protected TextBox txtStartDate, txtEndDate;

		protected Label lblKPointsSold, lblKPointsSpent, lblRoyaltyTotal, lblSubscriptionsSold, lblEarnings, lblSales;
			
		protected Label lblSubTransactions, lblKPointTransactions, lblKPointSpentTransactions, lblRoyaltyTransactions, lblBalance;

		protected RegularExpressionValidator revStartDate, revEndDate;

		// Sales
		protected Kaneva.Pager pgTop;
		protected DataGrid dgrdSales;
		protected Label lblSearch;

		#region Web Form Designer generated code

		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
			pgTop.PageChanged +=new PageChangeEventHandler (pg_PageChange);
		}
		#endregion
	}
}
