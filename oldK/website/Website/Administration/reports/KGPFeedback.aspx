<%@ Page language="c#" Codebehind="KGPFeedback.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.KGPFeedback" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../../usercontrols/Pager.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="ReportNav" Src="reportNav.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="AdminMenu" Src="../../usercontrols/AdministrativeMenu.ascx" %>

<Kaneva:AdminMenu ID="adminmenu" runat="server" />
<table runat="server" id="tblNoAdmin" cellpadding="0" cellspacing="0" border="0" width="560">
	<tr><td ALIGN="CENTER">
		<BR><BR><BR>
		<span class="subHead">You must be a site administrator to access this section.</span>
	</td></tr>
</table>

<asp:panel runat="server" ID="pnlAdmin">

<Kaneva:ReportNav runat="server" id="rnNavigation" SubTab="pf"/>
<center>

<br><br>	
<table cellpadding="0" cellspacing="0" border="0" width="905">
	<tr>
		<td align="right" class="belowFilter2" valign="middle" style="line-height:15px;">
			<asp:Label runat="server" id="lblSearch" CssClass="dateStamp"/><br><Kaneva:Pager runat="server" id="pgTop"/>
		</td>
	</tr>
</table>
<asp:DataGrid runat="server" EnableViewState="False" ShowFooter="False" Width="905" id="dgrdFeedback" cellpadding="0" cellspacing="0" border="0" AutoGenerateColumns="False" AllowSorting="True" CssClass="FullBorders">  
	<HeaderStyle CssClass="lineItemColHead"/>
	<ItemStyle CssClass="lineItemEven"/>
	<AlternatingItemStyle CssClass="lineItemOdd"/>
	<Columns>
		<asp:TemplateColumn HeaderText="subject" SortExpression="subject" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="Top" ItemStyle-Width="55%">
			<ItemTemplate>
				<B><%# DataBinder.Eval(Container.DataItem, "subject") %></B><BR/>
				<%# DataBinder.Eval(Container.DataItem, "summary") %>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="download" SortExpression="feedback_id" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="middle" ItemStyle-Width="10%">
			<ItemTemplate>
				<a href='<%# GetDownloadLink (Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "feedback_id"))) %>' runat="server" class="adminLinks">download</a>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="ip address" SortExpression="IP" ItemStyle-CssClass="bodyText" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="middle" ItemStyle-Width="10%">
			<ItemTemplate>
				<%# DataBinder.Eval(Container.DataItem, "IP") %>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="date" SortExpression="created_datetime" ItemStyle-Width="20%" ItemStyle-ForeColor="#4863a2" ItemStyle-HorizontalAlign="center">
			<ItemTemplate>
				<%# FormatDateTime (DataBinder.Eval(Container.DataItem, "created_datetime")) %>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="type" SortExpression="type" ItemStyle-CssClass="adminLinks" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="middle" ItemStyle-Width="5%">
			<ItemTemplate>
				<%# GetType (Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "type"))) %>
			</ItemTemplate>
		</asp:TemplateColumn>
	</Columns>
</asp:datagrid>

<br><br>
</center>
</asp:panel>
