<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="cohort1.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.cohort1" %>
    <div>
      <h1>Cohort</h1>
      <p>Retention of users from one interval to the next.</p>      
      <table>
        <tr>
          <td valign="top">
            Report Type: <asp:DropDownList ID="ddlCohortFlavor" runat="server">
              <asp:ListItem Text="WoK Login" Value="startwok"></asp:ListItem>
              <asp:ListItem Text="Payers" Value="payers"></asp:ListItem>              
            </asp:DropDownList><br />            
            <br />            
            Start Date: <asp:TextBox ID="txtStartDate" runat="server"></asp:TextBox> <br />
            <br />
            Interval: 
            <asp:DropDownList ID="ddlCohortInterval" runat="server">
              <asp:ListItem Value="WEEK">Week</asp:ListItem>              
              <asp:ListItem Value="DATE">Day</asp:ListItem>               
            </asp:DropDownList><br />
            Days to report:  <asp:TextBox ID="txtDaysToReport" runat="server">7</asp:TextBox><br />
            <br />
            <asp:Button ID="btnGetReport" runat="server" Text="Run Report" OnClick="btnGetReport_Click" />                              
          </td>
          <td valign="top">
          <asp:Label ID="lblStatus" runat="server"></asp:Label>      
          </td>                
        </tr>      
      </table>
     
     
     <hr />                    
      <asp:GridView ID="dgCohortData" runat="server" AutoGenerateColumns="false" Width="500">                
        <RowStyle BackColor="White"></RowStyle>
			  <HeaderStyle BackColor="LightGray" Font-Underline="True" Font-Bold="True" 
			    ForeColor="#000000" HorizontalAlign="Left"></HeaderStyle>
			  <AlternatingRowStyle BackColor="Gainsboro"></AlternatingRowStyle>
			  <FooterStyle BackColor="#FFffff"></FooterStyle>
        <Columns>
        <asp:BoundField HeaderText="Users" DataField="numusers" />
        <asp:BoundField HeaderText="First Day" DataField="firstday" />
        <asp:BoundField HeaderText="Last Day" DataField="lastday" />        
        <asp:TemplateField HeaderText="Retention %">
			        <ItemTemplate>						  	
						    <%# Convert.ToDouble(DataBinder.Eval(Container.DataItem, "retention")).ToString("N") %>				  
					    </ItemTemplate>
				    </asp:TemplateField>  
        </Columns> 
      </asp:GridView>  
        
      <br />
      
      <asp:GridView ID="dgCohortData2" runat="server" AutoGenerateColumns="false" >        
        <Columns>
            <asp:TemplateField HeaderText="Retention %">
			        <ItemTemplate>						  	
						    <%# Convert.ToDouble(DataBinder.Eval(Container.DataItem, "retention")).ToString("N") %>%				  
					    </ItemTemplate>
				    </asp:TemplateField>  
        </Columns> 
      </asp:GridView>    
      
    </div>