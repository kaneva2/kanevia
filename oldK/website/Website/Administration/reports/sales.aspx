<%@ Page language="c#" Codebehind="sales.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.sales" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../../usercontrols/Pager.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="ReportNav" Src="reportNav.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="AdminMenu" Src="../../usercontrols/AdministrativeMenu.ascx" %>

<link href="../../css/new.css" rel="stylesheet" type="text/css">

<Kaneva:AdminMenu ID="adminmenu" runat="server" />
<table runat="server" id="tblNoAdmin" cellpadding="0" cellspacing="0" border="0" width="560">
	<tr><td ALIGN="CENTER">
		<BR><BR><BR>
		<span class="subHead">You must be a site administrator to access this section.</span>
	</td></tr>
</table>

<asp:panel runat="server" ID="pnlAdmin">

<Kaneva:ReportNav runat="server" id="rnNavigation" SubTab="sa"/>
<center>
<BR>
<table cellpadding="0" cellspacing="0" border="0" width="750">
	<tr>
		<td valign="middle" align="right" class="Filter2" width="25%" nowrap="true">
			Search from&nbsp;
		</td>
		<td valign="middle" align="left" class="Filter2" width="25%" nowrap="true">
			<asp:TextBox ID="txtStartDate" class="Filter2" Width="100" MaxLength="10" runat="server"/> (MM-dd-yyyy) 12:00:00 AM
			<asp:RequiredFieldValidator ID="rfdtxtStartDate" ControlToValidate="txtStartDate" Text="*" ErrorMessage="Start date is a required field." Display="Dynamic" runat="server"/>
			<asp:RegularExpressionValidator id="revStartDate" ControlToValidate="txtStartDate" Text="*" Display="Static" ErrorMessage="Start date must be in format mm-dd-yyyy." EnableClientScript="True" runat="server"/>
		</td>
		<td width="50%" colspan="2">
		</td>
	</tr>
	<tr>
		<td valign="middle" align="right" class="Filter2" nowrap="true">
			to&nbsp;
		</td>
		<td valign="middle" align="left" class="Filter2" nowrap="true">
			<asp:TextBox ID="txtEndDate" class="Filter2" Width="100" MaxLength="10" runat="server"/> (MM-dd-yyyy) 11:59:59 PM
			<asp:RequiredFieldValidator ID="rfdtxtEndDate" ControlToValidate="txtEndDate" Text="*" ErrorMessage="End date is a required field." Display="Dynamic" runat="server"/>
			<asp:RegularExpressionValidator id="revEndDate" ControlToValidate="txtEndDate" Text="*" Display="Static" ErrorMessage="End date must be in format mm-dd-yyyy." EnableClientScript="True" runat="server"/>
		</td>
		<td width="10%" valign="middle" align="center">
		</td>
		<td width="40%" valign="middle" align="left">
			<asp:button id="btnSearch" runat="Server" CausesValidation="True" onClick="btnSearch_Click" class="Filter2" Text=" search "/>
		</td>
	</tr>
</table>	
<br><br>
<table runat="server" cellpadding="0" cellspacing="0" border="0" width="905">
	<tr Class="lineItemColHead">
		<td width="100%" align="left">&nbsp;&nbsp;<span class="header02" style="font-size: 15px;"><b><asp:Label class="dateStamp" id="lblSales" runat="server"/><b></b></span>
		</td>
	</tr>
</table>	
<table cellpadding="0" cellspacing="0" border="0" width="905">
	<tr>
		<td align="right" class="belowFilter2" valign="middle" style="line-height:15px;">
			<asp:Label runat="server" id="lblSearch" CssClass="dateStamp"/><br><Kaneva:Pager runat="server" id="pgTop"/>
		</td>
	</tr>
</table>
<asp:DataGrid runat="server" EnableViewState="False" ShowFooter="False" Width="905" id="dgrdSales" cellpadding="0" cellspacing="0" border="0" AutoGenerateColumns="False" AllowSorting="True" CssClass="FullBorders">  
	<HeaderStyle CssClass="lineItemColHead"/>
	<ItemStyle CssClass="lineItemEven"/>
	<AlternatingItemStyle CssClass="lineItemOdd"/>
	<Columns>
		<asp:TemplateColumn HeaderText="user" SortExpression="com.name" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="middle" ItemStyle-Width="10%">
			<ItemTemplate>
				<div class="framesize-xsmall">
					<div class="restricted" visible="false" runat="server" id="divOwnerRestricted"></div>
					<div class="frame">
						<span class="ct"><span class="cl"></span></span>
						<div class="imgconstrain">
							<a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "name_no_spaces").ToString ())%> style="cursor: hand;">
							<img runat="server" src='<%#GetProfileImageURL (DataBinder.Eval(Container.DataItem, "thumbnail_small_path").ToString (), "sm", "M")%>' border="0"/>
							</a>	
						</div>
						<span class="cb"><span class="cl"></span></span>
					</div>
				</div>
				</div>
				<a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "name_no_spaces").ToString ())%>><%# TruncateWithEllipsis (DataBinder.Eval(Container.DataItem, "com_name").ToString (), 10) %></a>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="order id" SortExpression="ppt.point_transaction_id" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="middle" ItemStyle-Width="28%">
			<ItemTemplate>
				<a href='<%# "salesDetails.aspx?ptId=" + DataBinder.Eval(Container.DataItem, "point_transaction_id")%>' runat="server" id="hlOrder"  style="COLOR: #3258ba; font-size: 13px; font-weight: bold;">KEN-<%# DataBinder.Eval(Container.DataItem, "order_id")%></a>
				</span><BR>
				<span style="font-size:10px; color: gray;"><%# DataBinder.Eval(Container.DataItem, "ppt_description")%></span>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="amount ($)" SortExpression="amount_debited" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="middle">
			<ItemTemplate>
				<span style="color: green;"><%# FormatCurrency (DataBinder.Eval(Container.DataItem, "amount_debited"))%></span>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="k-points received" SortExpression="kpoints" ItemStyle-Width="12%" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="middle">
			<ItemTemplate>
				<span style="color: green;"><%# FormatKpoints (DataBinder.Eval(Container.DataItem, "kpoints"), false) %></span>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="payment method" SortExpression="pm.name" ItemStyle-Width="12%" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="middle">
			<ItemTemplate>
				<%# DataBinder.Eval(Container.DataItem, "name")%>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="status" SortExpression="status_description" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="middle">
			<ItemTemplate>
				<A runat="server" style="color: #4863a2; font-size: 11px;" HREF='<%# GetTransactionStatusLink (Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "transaction_status")), DataBinder.Eval(Container.DataItem, "status_description").ToString (), Convert.ToDateTime (DataBinder.Eval(Container.DataItem, "transaction_date")))%>'><%# GetTransactionStatus (Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "transaction_status")), DataBinder.Eval(Container.DataItem, "status_description").ToString (), Convert.ToDateTime (DataBinder.Eval(Container.DataItem, "transaction_date")))%></a>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="purchase date" SortExpression="transaction_date" ItemStyle-Width="18%" ItemStyle-Font-Size="8px" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="middle">
			<ItemTemplate>
				<span  style="font-size:10px; color: orange;"><%# FormatDateTime (DataBinder.Eval(Container.DataItem, "transaction_date"))%></span>
			</ItemTemplate>
		</asp:TemplateColumn>
	</Columns>
</asp:datagrid>

<br><br>
</center>
</asp:panel>
