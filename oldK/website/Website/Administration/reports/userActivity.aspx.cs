///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Globalization;
using log4net;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for userActivity.
	/// </summary>
    public class userActivity : NoBorderPage
	{
		protected userActivity () 
		{
			Title = "Admin - User Activity";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			pnlAdmin.Visible = (IsAdministrator ());
			tblNoAdmin.Visible = (!IsAdministrator ());
			
			if (!IsPostBack)
			{
				drpStatus.DataTextField = "name";
				drpStatus.DataValueField = "status_id";
				drpStatus.DataSource = UsersUtility.GetUserStatus ();
				drpStatus.DataBind ();
				drpStatus.Items.Insert (0, new ListItem ("All Status", "0"));
				drpStatus.SelectedValue = "0";

			//	BindData (1, "");

				// Demographics
				DataRow drAgeRange = ReportUtility.GetAgeRange (0, 13);  // 0-12
				lblDemo1.Text = drAgeRange ["count"].ToString ();
				lblGenderMale1.Text = drAgeRange ["mCount"].ToString ();
				lblGenderFemale1.Text = drAgeRange ["fCount"].ToString ();

				drAgeRange = ReportUtility.GetAgeRange (13, 17);  // 13-17 
				lblDemo2.Text = drAgeRange ["count"].ToString ();
				lblGenderMale2.Text = drAgeRange ["mCount"].ToString ();
				lblGenderFemale2.Text = drAgeRange ["fCount"].ToString ();

				drAgeRange = ReportUtility.GetAgeRange (18, 20);  //18-20 
				lblDemo3.Text = drAgeRange ["count"].ToString ();
				lblGenderMale3.Text = drAgeRange ["mCount"].ToString ();
				lblGenderFemale3.Text = drAgeRange ["fCount"].ToString ();

				drAgeRange = ReportUtility.GetAgeRange (21, 24);  //21-24
				lblDemo4.Text = drAgeRange ["count"].ToString ();
				lblGenderMale4.Text = drAgeRange ["mCount"].ToString ();
				lblGenderFemale4.Text = drAgeRange ["fCount"].ToString ();

				drAgeRange = ReportUtility.GetAgeRange (25, 34);  //25-34
				lblDemo5.Text = drAgeRange ["count"].ToString ();
				lblGenderMale5.Text = drAgeRange ["mCount"].ToString ();
				lblGenderFemale5.Text = drAgeRange ["fCount"].ToString ();

				drAgeRange = ReportUtility.GetAgeRange (35, 40);   //35-40
				lblDemo6.Text = drAgeRange ["count"].ToString ();
				lblGenderMale6.Text = drAgeRange ["mCount"].ToString ();
				lblGenderFemale6.Text = drAgeRange ["fCount"].ToString ();

				drAgeRange = ReportUtility.GetAgeRange (41, 49);   //41-49
				lblDemo7.Text = drAgeRange ["count"].ToString ();
				lblGenderMale7.Text = drAgeRange ["mCount"].ToString ();
				lblGenderFemale7.Text = drAgeRange ["fCount"].ToString ();
			
				drAgeRange = ReportUtility.GetAgeRange (50, 54);   //50-54
				lblDemo8.Text = drAgeRange ["count"].ToString ();
				lblGenderMale8.Text = drAgeRange ["mCount"].ToString ();
				lblGenderFemale8.Text = drAgeRange ["fCount"].ToString ();
			
				drAgeRange = ReportUtility.GetAgeRange (55, 500);   //55+
				lblDemo9.Text = drAgeRange ["count"].ToString ();
				lblGenderMale9.Text = drAgeRange ["mCount"].ToString ();
				lblGenderFemale9.Text = drAgeRange ["fCount"].ToString ();
			}

			lblActiveUsers.Text = Application [Constants.CURRENT_USER_COUNT].ToString () + " user(s) currently online server " + Server.MachineName + " (" + Request ["LOCAL_ADDR"].ToString () + ")";

			
		}


		#region Helper Methods
		/// <summary>
		///  Bind the data
		/// </summary>
		/// <param name="pageNumber"></param>
		private void BindData (int pageNumber, string filter)
		{
			// Set the sortable columns
			SetHeaderSortText (dgrdMembers);
			string orderby = CurrentSort + " " + CurrentSortOrder;

			// Log the CSR activity
            SiteMgmtFacade siteMgmtFacade = new SiteMgmtFacade();
            siteMgmtFacade.InsertCSRLog(GetUserId(), "Admin - Searching users. Username = " + Server.HtmlEncode(txtUsername.Text) + ", Email = " + Server.HtmlEncode(txtEmail.Text) + ", First Name = " + Server.HtmlEncode(txtFirstName.Text) + ", Last Name = " + Server.HtmlEncode(txtLastName.Text), 0, 0);

			PagedDataTable pds = UsersUtility.GetUsers (Server.HtmlEncode (txtUsername.Text), Server.HtmlEncode (txtEmail.Text), Server.HtmlEncode (txtFirstName.Text), Server.HtmlEncode (txtLastName.Text), Convert.ToInt32 (drpStatus.SelectedValue), filter, orderby, pageNumber, filMembers.ItemsPerPages);
			pgTop.NumberOfPages = Math.Ceiling ((double) pds.TotalCount / filMembers.ItemsPerPages).ToString ();
			pgTop.DrawControl ();

			dgrdMembers.DataSource = pds;
			dgrdMembers.DataBind ();

			// The results
			lblSearch.Text = GetResultsText (pds.TotalCount, pageNumber, filMembers.ItemsPerPages, pds.Rows.Count);
		}




        protected string GetSuspensionUrl(int userId)
        {
            return ResolveUrl("~/Administration/csr/Activity_Suspensions.aspx?userId=" + userId);
        }

		#endregion

		#region Event Handlers
		/// <summary>
		/// Pager change event
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pg_PageChange (object sender, PageChangeEventArgs e)
		{
			BindData (e.PageNumber, filMembers.CurrentFilter);
		}

		/// <summary>
		/// filMembers_FilterChanged
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void filMembers_FilterChanged (object sender, FilterChangedEventArgs e)
		{
			pgTop.CurrentPageNumber = 1;
			BindData (1, e.Filter);
		}

		/// <summary>
		/// They clicked to sort a column
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected override void btnSort_Click (object sender, System.EventArgs e)
		{
			base.btnSort_Click (sender, e);
			BindData (pgTop.CurrentPageNumber, filMembers.CurrentFilter);
		}

		/// <summary>
		/// Search Event Handler
		/// </summary>
		protected void btnSearch_Click (object sender, EventArgs e) 
		{
			BindData (1, filMembers.CurrentFilter);
		}

        protected void btnGENFG_Click(object sender, EventArgs e)
        {
            //ImageHelper.GenerateFacebookThumbnails();
        }

		#endregion

		#region Properties
		/// <summary>
		/// DEFAULT_SORT
		/// </summary>
		/// <returns></returns>
		protected override string DEFAULT_SORT
		{
			get
			{
				return "user_id";
			}
		}

		/// <summary>
		/// DEFAULT_SORT
		/// </summary>
		/// <returns></returns>
		protected override string DEFAULT_SORT_ORDER
		{
			get
			{
				return "desc";
			}
		}

		#endregion

		#region Declerations

		protected Kaneva.MembersFilter filMembers;
		protected Kaneva.Pager pgTop;

		protected DataGrid dgrdMembers;

		protected Label lblSearch;
		protected HtmlTable tblNoAdmin;
		protected Panel pnlAdmin;

		protected HtmlInputHidden txtUserId;
		protected Button btnRemoveUserBan, btnBanUser;

		protected Label lblActiveUsers;

		protected Label lblDemo1, lblDemo2, lblDemo3, lblDemo4, lblDemo5, lblDemo6, lblDemo7, lblDemo8, lblDemo9;
		protected Label lblGenderMale1, lblGenderMale2, lblGenderMale3, lblGenderMale4, lblGenderMale5;
		protected Label lblGenderMale6, lblGenderMale7, lblGenderMale8, lblGenderMale9;
		protected Label lblGenderFemale1, lblGenderFemale2, lblGenderFemale3, lblGenderFemale4, lblGenderFemale5;
		protected Label lblGenderFemale6, lblGenderFemale7, lblGenderFemale8, lblGenderFemale9;

		// Search
		protected TextBox txtUsername, txtEmail, txtFirstName, txtLastName;
		protected DropDownList drpStatus;

		// Logger
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

		#endregion

		#region Web Form Designer generated code

		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
			pgTop.PageChanged +=new PageChangeEventHandler (pg_PageChange);
			filMembers.FilterChanged +=new FilterChangedEventHandler(filMembers_FilterChanged);
		}
		#endregion

	}
}
