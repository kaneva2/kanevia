///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using log4net;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
    public partial class friendInvites : NoBorderPage
    {
        protected friendInvites ()
        {
            Title = "Admin - Declined Friend Invites";
        }

        protected void Page_Load (object sender, EventArgs e)
        {
            if (!IsAdministrator ())
            {
                Response.Redirect ("~/default.asp");
            }
            
            // Date validators
            revStartDate.ValidationExpression = Constants.VALIDATION_REGEX_DATE;
            revEndDate.ValidationExpression = Constants.VALIDATION_REGEX_DATE;

            if (!IsPostBack)
            {
                txtStartDate.Text = KanevaGlobals.FormatDateNumbersOnly (KanevaGlobals.GetCurrentDateTime ());
                txtEndDate.Text = KanevaGlobals.FormatDateNumbersOnly (KanevaGlobals.GetCurrentDateTime ());
                BindData (1);
            }
        }


        #region Helper Methods

        /// <summary>
        /// Bind the data
        /// </summary>
        private void BindData (int pageNumber)
        {
            lblInvites.Text = "Declined Invites Report from '" + txtStartDate.Text + " 12:00 AM' to '" + txtEndDate.Text + " 11:59 PM'";

            // Log the activity
            SiteMgmtFacade siteMgmtFacade = new SiteMgmtFacade();
            siteMgmtFacade.InsertCSRLog(GetUserId(), "Admin - " + lblInvites.Text + " page number " + pageNumber, 0, 0);

            DateTime dtStartDate = KanevaGlobals.UnFormatDate (txtStartDate.Text + " 00:00:00");
            DateTime dtEndDate = KanevaGlobals.UnFormatDate (txtEndDate.Text + " 23:59:59");

            GetInviteCounts (dtStartDate, dtEndDate);

            string orderby = CurrentSort + " " + CurrentSortOrder;

            int itemsPerPage = 50;
            PagedDataTable pds = ReportUtility.GetDeclinedInvites (dtStartDate, dtEndDate, orderby, pgTop.CurrentPageNumber, itemsPerPage);

            // Display the declined count
            spnDeclineCount.InnerHtml = pds.TotalCount.ToString();

            if ((pds != null) && (pds.Rows.Count > 0))
            {
                lblNoInvites.Visible = false;

                // Bind the data
                dgInvites.DataSource = pds;
                dgInvites.DataBind ();
                dgInvites.Visible = true;

                // Set current page
                pgTop.CurrentPageNumber = pageNumber;
                pgTop.NumberOfPages = Math.Ceiling ((double) pds.TotalCount / itemsPerPage).ToString ();
                pgTop.DrawControl ();
            }
            else
            {
                lblNoInvites.Visible = true;
                dgInvites.Visible = false;
            }
        }

        /// <summary>
        /// Get counts for all the invite info
        /// </summary>
        private void GetInviteCounts (DateTime start, DateTime end)
        {
            spnSentCount.InnerHtml = "0";
            spnAcceptCount.InnerHtml = "0";
            spnDeclineCount.InnerHtml = "0";
            spnPendCount.InnerHtml = "0";
            spnOpenCount.InnerHtml = "0";
            spnClickCount.InnerHtml = "0";

            int numInvites = ReportUtility.GetInvitesCount (start, end, "");

            spnSentCount.InnerHtml = numInvites.ToString ();
            spnAcceptCount.InnerHtml = ReportUtility.GetInvitesAcceptedCount (start, end).ToString ();
            spnPendCount.InnerHtml = ReportUtility.GetInvitesCount (start, end, "invite_status_id=1").ToString ();
            spnOpenCount.InnerHtml = ReportUtility.GetInvitesOpenedCount (start, end).ToString ();
            spnClickCount.InnerHtml = ReportUtility.GetInvitesJoinClickCount (start, end).ToString ();

            GetViralCoefficient (start, end, numInvites);
        }

        /// <summary>
        /// Calculate the viral coefficient value
        /// </summary>
        private void GetViralCoefficient (DateTime start, DateTime end, int invitesSent)
        {
            try
            {
                spnCoefficient.InnerHtml = "0";

                // Viral Coefficient = A x B x C
                // A - % of users inviting people
                // B - Avg. number people invited per user in A
                // C - % of accepted invites
                decimal numUsersSendingInvites = Convert.ToDecimal (ReportUtility.GetNumberUsersInvitingFriends (start, end));
                decimal invitesAccepted = Convert.ToDecimal (ReportUtility.GetInvitesAcceptedCount (start, end));
                decimal activeUserCount = Convert.ToDecimal (ReportUtility.GetNewUserCount (start, end));

                // % of users inviting people
                decimal a = activeUserCount > 0 ? numUsersSendingInvites / activeUserCount : 0;
                // Avg. number people invited per user in A
                decimal b = numUsersSendingInvites > 0 ? invitesSent / numUsersSendingInvites : 0;
                // % of accepted invites
                decimal c = invitesSent > 0 ? invitesAccepted / invitesSent : 0;

                spnCoefficient.InnerHtml = (Math.Round((a * b * c), 2)).ToString ();

                // Explanation div
                spnA.InnerHtml = Math.Round (a, 4).ToString ();
                spnB.InnerHtml = Math.Round (b, 4).ToString ();
                spnC.InnerHtml = Math.Round (c, 4).ToString ();

                spnAFormula.InnerHtml = numUsersSendingInvites.ToString () + " / " + activeUserCount.ToString () + " = " + spnA.InnerHtml;
                spnBFormula.InnerHtml = invitesSent.ToString () + " / " + numUsersSendingInvites.ToString () + " = " + spnB.InnerHtml;
                spnCFormula.InnerHtml = invitesAccepted.ToString () + " / " + invitesSent.ToString () + " = " + spnC.InnerHtml;
                
                spnABC.InnerHtml = spnCoefficient.InnerHtml;
            }
            catch
            {
                spnCoefficient.InnerHtml = "ERROR";
            }
        }

        /// <summary>
        /// Switch the sort order
        /// </summary>
        private void SortSwitch (string newSort)
        {
            if (CurrentSort == newSort)
            {
                if (CurrentSortOrder == "DESC")
                {
                    CurrentSortOrder = "ASC";
                }
                else
                {
                    CurrentSortOrder = "DESC";
                }
            }
            else
            {
                CurrentSort = newSort;
                CurrentSortOrder = "DESC";
            }
        }
       
        #endregion Helper Methods


        #region Event Handlers

        /// <summary>
        /// Click event for search button
        /// </summary>
        protected void btnSearch_Click (object sender, EventArgs e)
        {
            BindData (1);
        }

        /// <summary>
        /// Pager change event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void pg_PageChange (object sender, PageChangeEventArgs e)
        {
            BindData (e.PageNumber);
        }

        /// <summary>
        /// Sort Declined Invite Data
        /// </summary>
        protected void dgInvites_Sorting (Object sender, GridViewSortEventArgs e)
        {
            SortSwitch (e.SortExpression); //sets the sort expression
            BindData (1);
        }

        #endregion Event Handlers


        #region Properties

        /// <summary>
        /// DEFAULT_SORT
        /// </summary>
        /// <returns></returns>
        protected override string DEFAULT_SORT
        {
            get
            {
                return "decline_date";
            }
        }

        /// <summary>
        /// DEFAULT_SORT_ORDER
        /// </summary>
        /// <returns></returns>
        protected override string DEFAULT_SORT_ORDER
        {
            get
            {
                return "ASC";
            }
        }

        #endregion Properties
    }
}
