///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using System.Collections.Specialized;

namespace KlausEnt.KEP.Kaneva.Administration.reports
{
    public partial class ViolationReports : NoBorderPage
    {
        #region Declarations
            private ViolationsFacade violationFacade = new ViolationsFacade();
            private uint _currentViolationType = 0;
            private uint _currentViolationActionType = 0;
            private uint _currentViolationReportId = 0;
            private uint _wokItemId = 0;
            private uint _assetId = 0;
        #endregion

        #region Page Load

        protected void Page_Load(object sender, EventArgs e)
        {
            // Redirect if user is not admin
            if (!IsAdministrator())
            {
                Response.Redirect("~/default.aspx");
            }

            this.lblErrMessage.Text = "";

            if (!Page.IsPostBack)
            {
                //get the report data
                BindData(CURRENTPAGE);
            }
        }

        #endregion

        #region Helper Functions

        protected string GetItemDetailsURL(string globalId)
        {
            return "http://"+ KanevaGlobals.ShoppingSiteName + "/ItemDetails.aspx?gId=" + globalId;
        }

        //delegate function used in LIst FindAll search
        private bool IsViolationType(ViolationType type)
        {
            return type.ViolationTypeId == _currentViolationType;
        }

        //delegate function used in LIst FindAll search
        private bool IsViolationReport(ViolationReport report)
        {
            return report.ViolationID == _currentViolationReportId;
        }

        //delegate function used in LIst FindAll search
        private bool IsWebViolationReport(ViolationReport report)
        {
            return report.AssetID == _assetId;
        }

        //delegate function used in LIst FindAll search
        private bool IsWOKViolationReport(ViolationReport report)
        {
            return report.WokItemId == _wokItemId;
        }

        //delegate function used in LIst FindAll search
        private bool IsViolationActionType(ViolationActionType action)
        {
            return action.ViolationActionTypeId == _currentViolationActionType;
        }

        //finds the amount of reports made on this item
        private string GetNumberOfReports(uint assetID, uint wokItemId)
        {
            int numberReports = 0;

            //store values
            _wokItemId = wokItemId;
            _assetId = assetID;

            //process summary information
            if (assetID > 0)
            {
                numberReports = (VIOLATION_REPORT_DS.FindAll(IsWebViolationReport)).Count;
            }
            else
            {
                numberReports = (VIOLATION_REPORT_DS.FindAll(IsWOKViolationReport)).Count;
            }

            return numberReports.ToString();
        }

        private void BindData(int pageNumber)
        {
            try
            {
                // Set current page
                pgBottom.CurrentPageNumber = pgTop.CurrentPageNumber = pageNumber;
                
                // Set the sortable columns
                string orderby = CurrentSort + " " + CurrentSortOrder;
                int pageSize = 10;

                // Get the violation info (rolled up)
                PagedList<ViolationReport> violationList = violationFacade.GetAllViolationReportsJoined(SEARCHFILTER, orderby, pageNumber, pageSize);
                
                if ((violationList != null) && (violationList.Count > 0))
                {
                    lblNoViolations.Visible = false;

                    //dgMemberFame.PageCount = userFame.TotalCount / pageSize;
                    VIOLATION_REPORT_DS = (List<ViolationReport>)violationList.List;
                    rpt_ViolationReports.DataSource = violationList;
                    rpt_ViolationReports.DataBind();

                    // Show Pager
                    pgTop.NumberOfPages = pgBottom.NumberOfPages = Math.Ceiling((double)violationList.TotalCount / pageSize).ToString();
                    pgTop.DrawControl();
                    pgBottom.DrawControl();
                }
                else
                {
                    lblNoViolations.Visible = true;
                    rpt_ViolationReports.DataSource = violationList;
                    rpt_ViolationReports.DataBind();
                }

                lblErrMessage.Visible = false;
            }
            catch (Exception ex)
            {
                lblErrMessage.Text = "Error: " + ex.Message;
                lblErrMessage.Visible = true;
            }
        }

        #endregion

        #region Properties

        private string SEARCHFILTER
        {
            get
            {
                if (ViewState["_SearchFilter"] == null)
                {
                    ViewState["_SearchFilter"] = "";
                }
                return ViewState["_SearchFilter"].ToString();
            }
            set
            {
                ViewState["_SearchFilter"] = value;
            }
        }

        private int CURRENTPAGE
        {
            get
            {
                if (ViewState["_CurrentPage"] == null)
                {
                    ViewState["_CurrentPage"] = 1;
                }
                return (int)ViewState["_CurrentPage"];
            }
            set
            {
                ViewState["_CurrentPage"] = value;
            }
        }
        /// <summary>
        /// stores the list of violation reports
        /// </summary>
        /// <returns>int</returns>
        private List<ViolationReport> VIOLATION_REPORT_DS
        {
            get
            {
                if (ViewState["_VRDataSource"] == null)
                {
                    return null;
                }
                return (List<ViolationReport>)ViewState["_VRDataSource"];
            }
            set
            {
                ViewState["_VRDataSource"] = value;
            }
        }

        //allows user to switch the ordering between DESC and ASC
        //based on column clicked
        private void SortSwitch(string newSort)
        {
            if (CurrentSort == newSort)
            {
                if (CurrentSortOrder == "DESC")
                {
                    CurrentSortOrder = "ASC";
                }
                else
                {
                    CurrentSortOrder = "DESC";
                }
            }
            else
            {
                CurrentSort = newSort;
                CurrentSortOrder = "DESC";
            }
        }

        		/// <summary>
		/// DEFAULT_SORT
		/// </summary>
		/// <returns></returns>
		protected override string DEFAULT_SORT
		{
			get
			{
                return "report_date";
			}
		}
        
        #endregion

        #region Event Handlers

        /// <summary>
        /// Execute when the user selects a header link from the data grid to sort a by a column
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Sort_Command(Object sender, CommandEventArgs e)
        {
            SortSwitch(e.CommandArgument.ToString()); //sets the sort expression
            BindData(CURRENTPAGE);
        }

        private void pg_PageChange(object sender, PageChangeEventArgs e)
        {
            pgTop.CurrentPageNumber = e.PageNumber;
            pgTop.DrawControl();
            pgBottom.CurrentPageNumber = e.PageNumber;
            pgBottom.DrawControl();
            CURRENTPAGE = e.PageNumber;
            BindData(CURRENTPAGE);
        }

        private void rpt_ViolationReports_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                ////find all the controls in the repeater

                //pull back all the controls
                HtmlAnchor lnkItemLocation = (HtmlAnchor)e.Item.FindControl("lnk_Itemlocation");
                HtmlImage imgItemImage = (HtmlImage)e.Item.FindControl("img_ItemImage");
                HtmlAnchor lnkItemName = (HtmlAnchor)e.Item.FindControl("lnk_ItemName");
                HtmlAnchor lnkItemNo = (HtmlAnchor)e.Item.FindControl("lnk_ItemNo");
                HtmlAnchor lnkAssetNo = (HtmlAnchor)e.Item.FindControl("lnk_AssetNo");
                HtmlAnchor lnkOwnerName = (HtmlAnchor)e.Item.FindControl("lnk_OwnerName");
                HtmlAnchor lnkReportingUser = (HtmlAnchor)e.Item.FindControl("lnk_ReportingUser");
                Label lblViolationType = (Label)e.Item.FindControl("lbl_violation");
                LinkButton lnkMemberComments = (LinkButton)e.Item.FindControl("lbn_MemberComments");
                Label lblReportDate = (Label)e.Item.FindControl("lbl_ReportDate");
                DropDownList ddlViolationAction = (DropDownList)e.Item.FindControl("ddl_ViolationAction");
                LinkButton lnkCSRComment = (LinkButton)e.Item.FindControl("lbn_CSRComments");
                HtmlInputHidden hidViolationId = (HtmlInputHidden)e.Item.FindControl("hidViolationId");
                Label lblStatus = (Label)e.Item.FindControl("lbl_Status");
                TextBox tbxMemberComment = (TextBox)e.Item.FindControl("tbx_MemberComment");
                TextBox CSRNotes = (TextBox)e.Item.FindControl("tbx_CSRComment");
                Label lblSubtotal = (Label)e.Item.FindControl("tr_subTotal");
                HtmlTableRow trSubtotal = (HtmlTableRow)e.Item.FindControl("tr_reportSummary");
                Label lblLastModifier = (Label)e.Item.FindControl("lbl_LastModifier");


                //set the data for the drop down
                IList<ViolationActionType> VATSource = WebCache.GetAllViolationActionTypes();
                _currentViolationActionType = 0;
                if (!(((List<ViolationActionType>)VATSource).Exists(IsViolationActionType)))
                {
                    VATSource.Insert(0, new ViolationActionType(0, "--SELECT--"));
                }

                ddlViolationAction.DataSource = VATSource;
                ddlViolationAction.DataTextField = "ViolationActionDescription";
                ddlViolationAction.DataValueField = "ViolationActionTypeId";
                ddlViolationAction.DataBind();

                //populate the values
                try
                {
                    //used for summary violation
                    uint _currentItemIDInViolation = 0;

                    //get the user objects for the owner and reporter
                    User owner = (new UserFacade()).GetUser((int)((ViolationReport)e.Item.DataItem).OwnerId);
                    User reporter = (new UserFacade()).GetUser((int)((ViolationReport)e.Item.DataItem).ReportingUserId);
                    User modifier = (new UserFacade()).GetUser((int)((ViolationReport)e.Item.DataItem).ModifiersID);

                    //populate user data
                    lnkOwnerName.HRef = GetPersonalChannelUrl(owner.NameNoSpaces);
                    lnkOwnerName.InnerText = TruncateWithEllipsis(owner.Username, 16);

                    lnkReportingUser.HRef = GetPersonalChannelUrl(reporter.NameNoSpaces);
                    lnkReportingUser.InnerText = TruncateWithEllipsis(reporter.Username, 16);

                    lblLastModifier.Text = TruncateWithEllipsis(modifier.Username, 16);

                    //get the object for the WOK item or asset as needed and populate
                    uint assetID = ((ViolationReport)e.Item.DataItem).AssetID;
                    uint wokItemID = ((ViolationReport)e.Item.DataItem).WokItemId;
                    if (assetID > 0)
                    {
                        Asset asset = (new MediaFacade()).GetAsset((int)assetID);

                        lnkItemNo.HRef = lnkItemLocation.HRef = lnkItemName.HRef = GetAssetDetailsLink((int)assetID);
                        lnkItemNo.InnerText = ((ViolationReport)e.Item.DataItem).AssetID.ToString();
                        lnkItemName.InnerText = asset.Name;
                        imgItemImage.Src = StoreUtility.GetMediaImageURL((asset.ImagePath == null ? "" : asset.ImagePath), "sm", (int)assetID, asset.AssetTypeId, 0, this);
                        imgItemImage.Alt = asset.Name;
                    }
                    else if (wokItemID > 0)
                    {
                        WOKItem item = (new ShoppingFacade()).GetItem((int)wokItemID);

                        lnkItemNo.HRef = lnkItemLocation.HRef = lnkItemName.HRef = GetItemDetailsURL(wokItemID.ToString());
                        lnkItemNo.InnerText = ((ViolationReport)e.Item.DataItem).WokItemId.ToString();
                        lnkItemName.InnerText = item.Name;
                        imgItemImage.Src = StoreUtility.GetItemImageURL(item.ThumbnailAssetdetailsPath, "ad");
                        imgItemImage.Alt = item.Name;

                        //set current item involation for use in summary count display
                        _currentItemIDInViolation = wokItemID;
                    }

                    //set the violation type and status
                    List<ViolationType> violationTypes = (List<ViolationType>)WebCache.GetAllViolationTypes();
                    _currentViolationType = ((ViolationReport)e.Item.DataItem).ViolationTypeId;
                    lblViolationType.Text = (violationTypes.Find(IsViolationType)).ViolationTypeDescription;

                    List<ViolationActionType> violationActionTypes = (List<ViolationActionType>)WebCache.GetAllViolationActionTypes();
                    _currentViolationActionType = ((ViolationReport)e.Item.DataItem).ViolationActionTypeId;
                    lblStatus.Text = (violationActionTypes.Find(IsViolationActionType)).ViolationActionDescription;
                    if (_currentViolationActionType == (uint)ViolationActionType.eVIOLATION_ACTIONS.REMOVE)
                    {
                        ddlViolationAction.SelectedValue = _currentViolationActionType.ToString();
                        ddlViolationAction.Enabled = false;
                    }

                    //set report date
                    lblReportDate.Text = ((ViolationReport)e.Item.DataItem).ReportDate.ToShortDateString();

                    //set csr and member notes
                    string csrComment = ((ViolationReport)e.Item.DataItem).CSRNotes;
                    lnkCSRComment.Text = TruncateWithEllipsis((csrComment.Length > 0 ? csrComment : "add"), 16);
                    CSRNotes.Text = (csrComment.Length > 0 ? csrComment : "");

                    string memberComment = ((ViolationReport)e.Item.DataItem).MemberComments;
                    lnkMemberComments.Text = TruncateWithEllipsis((memberComment.Length > 0 ? memberComment : "none"), 16);
                    tbxMemberComment.Text = (memberComment.Length > 0 ? memberComment : "");

                    //set the hidden violation id value
                    hidViolationId.Value = ((ViolationReport)e.Item.DataItem).ViolationID.ToString();

                    //set the total number of reports
                    lblSubtotal.Text = GetNumberOfReports(assetID, wokItemID);

                }
                catch { }
            }
        }

        private void rpt_ViolationReports_ItemCreated(object sender, RepeaterItemEventArgs e)
        {
        }

        protected void ViolationAction_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //first get a reference to the repeater item the drop down is in
                RepeaterItem item = (RepeaterItem)((DropDownList)sender).Parent;

                //get the Drop down causing the change
                DropDownList activeDropDown = (DropDownList)item.FindControl("ddl_ViolationAction");
                uint violationActionTypeId = Convert.ToUInt32(activeDropDown.SelectedValue);

                if(violationActionTypeId > 0)
                {
                    //next get the id of the violation in that repeater item
                    HtmlInputHidden hidViolationId = (HtmlInputHidden)item.FindControl("hidViolationId");
                    _currentViolationReportId = Convert.ToUInt32(hidViolationId.Value);

                    //get the violationReport object associated with this row
                    ViolationReport activeReport = VIOLATION_REPORT_DS.Find(IsViolationReport);
                    activeReport.ViolationActionTypeId = violationActionTypeId;

                    //set the modifying user
                    activeReport.ModifiersID = (uint)KanevaWebGlobals.CurrentUser.UserId;

                    int result = 0;
                    //check to see if the selected item is an administrative request to immediately remove or mark AP
                    switch (violationActionTypeId)
                    {
                        case (uint)ViolationActionType.eVIOLATION_ACTIONS.SET_AS_AP:
                        case (uint)ViolationActionType.eVIOLATION_ACTIONS.REMOVE:
                        case (uint)ViolationActionType.eVIOLATION_ACTIONS.SET_AS_GOOD:
                            result = (new ViolationsFacade()).ProcessViolationImmediately(activeReport);
                            break;
                        default:
                            result = (new ViolationsFacade()).UpdateViolationReport(activeReport);
                            break;
                    }

                    BindData(CURRENTPAGE);

                    //check for and communicate if insert failed
                    if (result < 1)
                    {
                        lblErrMessage.Text = "Error: Violation action changes were not saved.";
                        lblErrMessage.Visible = true;
                    }

                }
            }
            catch (Exception)
            {
                lblErrMessage.Text = "Invalid date(s) has been entered.";
                lblErrMessage.Visible = true;
            }
        }

        public void CSRComment_Command(Object sender, CommandEventArgs e)
        {
            //switch (e.CommandName)
            //{
            //}
        }

        protected void dvbtn_MemberCancel_Click(object sender, EventArgs e)
        {
            try
            {
                //first get a reference to the repeater item the cancel button is in
                RepeaterItem violationReportRow = (RepeaterItem)((Button)sender).NamingContainer;

                HtmlContainerControl divMemberComments = (HtmlContainerControl)violationReportRow.FindControl("divMemberComments");
                divMemberComments.Visible = false;
            }
            catch (Exception)
            {
            }
        }

        protected void lbnMemberComments_Click(object sender, EventArgs e)
        {
            try
            {
                //first get a reference to the repeater item the link is in
                RepeaterItem violationReportRow = (RepeaterItem)((LinkButton)sender).NamingContainer;

                HtmlContainerControl divMemberComments = (HtmlContainerControl)violationReportRow.FindControl("divMemberComments");
                divMemberComments.Visible = true;
            }
            catch (Exception)
            {
            }
        }


        protected void dvbtn_CSRCancel_Click(object sender, EventArgs e)
        {
            try
            {
                //first get a reference to the repeater item the cancel button is in
                RepeaterItem violationReportRow = (RepeaterItem)((Button)sender).NamingContainer;

                HtmlContainerControl divCSRComments = (HtmlContainerControl)violationReportRow.FindControl("divCSRComments");
                divCSRComments.Visible = false;
            }
            catch (Exception)
            {
            }
        }

        protected void lbnCSRComments_Click(object sender, EventArgs e)
        {
            try
            {
                //first get a reference to the repeater item the link is in
                RepeaterItem violationReportRow = (RepeaterItem)((LinkButton)sender).NamingContainer;

                HtmlContainerControl divCSRComments = (HtmlContainerControl)violationReportRow.FindControl("divCSRComments");
                divCSRComments.Visible = true;
            }
            catch (Exception)
            {
            }
        }

        protected void lbnCSRSave_Click(object sender, EventArgs e)
        {
            try
            {
                //first get a reference to the repeater item the save button is in
                RepeaterItem violationReportRow = (RepeaterItem)((Button)sender).NamingContainer;

                //grab all the needed controls
                HtmlContainerControl divCSRComments = (HtmlContainerControl)violationReportRow.FindControl("divCSRComments");
                TextBox CSRAddNotes = (TextBox)violationReportRow.FindControl("tbx_CSRAddComment");
                HtmlInputHidden hidViolationId = (HtmlInputHidden)violationReportRow.FindControl("hidViolationId");

                //set the violation id to search for
                _currentViolationReportId = Convert.ToUInt32(hidViolationId.Value);

                //get the violationReport object associated with this row
                ViolationReport activeReport = VIOLATION_REPORT_DS.Find(IsViolationReport);
                activeReport.CSRNotes += "--- " + DateTime.Now + " -- \n" + CSRAddNotes.Text + "\n\r";

                //set the modifying user
                activeReport.ModifiersID = (uint)KanevaWebGlobals.CurrentUser.UserId;

                //save change to the Database
                int result = (new ViolationsFacade()).UpdateViolationReport(activeReport);

                //check for and communicate if insert failed
                if (result < 1)
                {
                    lblErrMessage.Text = "Error: CSR notes were not updated.";
                    lblErrMessage.Visible = true;
                }

                BindData(CURRENTPAGE);
            }
            catch (Exception)
            {
            }
        }

        ///// <summary>
        ///// BindMemberFameData
        ///// </summary>
        //protected void dgUserFame_Sorting(Object sender, GridViewSortEventArgs e)
        //{
        //    SortSwitch(e.SortExpression); //sets the sort expression
        //    BindData(1);
        //}

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string errorMessage = "";
            SEARCHFILTER = "";

            try
            {
                if (tbxItemNumSearch.Text.Trim() != "")
                {
                    errorMessage = "invalid item number";
                    uint itemNumber = Convert.ToUInt32(tbxItemNumSearch.Text);
                    SEARCHFILTER = " (vr.wok_item_id = " + itemNumber + " OR vr.asset_id = " + itemNumber + ")";
                }

                if (tbxNameSearch.Text.Trim() != "")
                {
                    if (SEARCHFILTER != "")
                    {
                        SEARCHFILTER += " AND ";
                    }

                    string itemName = tbxNameSearch.Text.Trim();
                    SEARCHFILTER += "(a.name LIKE '%" + itemName + "%' OR i.name LIKE '%" + itemName + "%')";
                }

                if (tbxOwnerSearch.Text.Trim() != "")
                {
                    if (SEARCHFILTER != "")
                    {
                        SEARCHFILTER += " AND ";
                    }

                    SEARCHFILTER += " u1.username LIKE '%" + tbxOwnerSearch.Text.Trim() + "%' ";
                }

                if (tbxRptBySearch.Text.Trim() != "")
                {
                    if (SEARCHFILTER != "")
                    {
                        SEARCHFILTER += " AND ";
                    }

                    SEARCHFILTER += " u2.username LIKE '%" + tbxRptBySearch.Text.Trim() + "%' ";
                }

                if ((txtStartDate.Text != "") && (txtEndDate.Text != ""))
                {
                    if (SEARCHFILTER != "")
                    {
                        SEARCHFILTER += " AND ";
                    }

                    errorMessage = "Invalid date(s) has been entered.";
                    DateTime sdt = Convert.ToDateTime(txtStartDate.Text.Trim());
                    DateTime edt = Convert.ToDateTime(txtEndDate.Text.Trim());

                    string startDate = sdt.ToString("yyyy-MM-dd");
                    string endDate = edt.ToString("yyyy-MM-dd");
                    SEARCHFILTER += " report_date BETWEEN '" + startDate + "' AND '" + endDate + "'";
                }
                CURRENTPAGE = 1;
                BindData(CURRENTPAGE);
            }
            catch
            {
                //error not valid date
                lblErrMessage.Text = errorMessage;
                lblErrMessage.Visible = true;
            }
        }


        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            this.rpt_ViolationReports.ItemCreated += new System.Web.UI.WebControls.RepeaterItemEventHandler(this.rpt_ViolationReports_ItemCreated);
            this.rpt_ViolationReports.ItemDataBound += new System.Web.UI.WebControls.RepeaterItemEventHandler(this.rpt_ViolationReports_ItemDataBound);
            pgTop.PageChanged += new PageChangeEventHandler(pg_PageChange);
            pgBottom.PageChanged += new PageChangeEventHandler(pg_PageChange);
        }
        
        #endregion

    }
}
