///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Globalization;
using log4net;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for platformLicense.
	/// </summary>
    public class platformLicense : NoBorderPage
	{
		protected platformLicense () 
		{
			Title = "Admin - Platform Licenses";
		}

		private void Page_Load (object sender, System.EventArgs e)
		{
			pnlAdmin.Visible = (IsAdministrator ());
			tblNoAdmin.Visible = (!IsAdministrator ());

			if (!IsPostBack)
			{
				BindData (1);
			}
		}

		/// <summary>
		/// Bind the data
		/// </summary>
		private void BindData (int pageNumber)
		{
			// Log the activity
            SiteMgmtFacade siteMgmtFacade = new SiteMgmtFacade();
            siteMgmtFacade.InsertCSRLog(GetUserId(), "Admin - Platform License Report", 0, 0);

			// Set the sortable columns
			SetHeaderSortText (dgrdLicenses);
			string orderby = CurrentSort + " " + CurrentSortOrder;

			int itemsPerPage = 50;
			PagedDataTable pds = ReportUtility.GetGameLicenses (orderby, pgTop.CurrentPageNumber, itemsPerPage);
			pgTop.NumberOfPages = Math.Ceiling ((double) pds.TotalCount / itemsPerPage).ToString ();
			pgTop.DrawControl ();

			dgrdLicenses.DataSource = pds;
			dgrdLicenses.DataBind ();

			// The results
			lblSearch.Text = GetResultsText (pds.TotalCount, pageNumber, itemsPerPage, pds.Rows.Count);
		}

		/// <summary>
		/// DEFAULT_SORT
		/// </summary>
		/// <returns></returns>
		protected override string DEFAULT_SORT
		{
			get
			{
				return "last_ping_datetime";
			}
		}

		/// <summary>
		/// DEFAULT_SORT
		/// </summary>
		/// <returns></returns>
		protected override string DEFAULT_SORT_ORDER
		{
			get
			{
				return "DESC";
			}
		}

		/// <summary>
		/// They clicked to sort a column
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected override void btnSort_Click (object sender, System.EventArgs e)
		{
			base.btnSort_Click (sender, e);
			BindData (pgTop.CurrentPageNumber);
		}

		/// <summary>
		/// Pager change event
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pg_PageChange (object sender, PageChangeEventArgs e)
		{
			BindData (e.PageNumber);
		}

		protected HtmlTable tblNoAdmin;
		protected Panel pnlAdmin;

		// login grid
		protected DataGrid dgrdLicenses;
		protected Kaneva.Pager pgTop;
		protected Label lblSearch;

		#region Web Form Designer generated code

		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
			pgTop.PageChanged +=new PageChangeEventHandler (pg_PageChange);
		}
		#endregion
	}
}
