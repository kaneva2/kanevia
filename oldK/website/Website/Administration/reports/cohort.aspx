<%@ Register TagPrefix="Kaneva" TagName="AdminMenu" Src="../../usercontrols/AdministrativeMenu.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="ReportNav" Src="reportNav.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="Cohort" Src="cohort1.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../../usercontrols/Pager.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="KPINav" Src="kpi2/KPINav.ascx" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="cohort.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.cohort" %>

<LINK href="../../css/home.css" type="text/css" rel="stylesheet">
<LINK href="../../css/kanevaSC.css" type="text/css" rel="stylesheet">

<Kaneva:AdminMenu ID="adminmenu" runat="server" />
		<table id="tblNoAdmin" cellSpacing="0" cellPadding="0" width="560" border="0" runat="server">
			<tr>
				<td align="center"><BR />
					<BR />
					<BR />
					<span class="subHead">You must be a site administrator to access this section.</span>
				</td>
			</tr>
		</table>
		<KANEVA:REPORTNAV id="rnNavigation" runat="server" SubTab="cohort"></KANEVA:REPORTNAV>
		<table width="auto" style="background-color:#FFFFFF;"><tr><td><BR>
			<table cellSpacing="0" cellPadding="0" width="750" border="0" align="center">
				<tr>
				  <td>
            <Kaneva:Cohort ID="ucCohort" runat="server" />
          </td>
        </tr>
      </table>
    </td></tr></table>
    
		