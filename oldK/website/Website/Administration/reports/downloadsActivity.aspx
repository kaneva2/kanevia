<%@ Page language="c#" Codebehind="downloadsActivity.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.downloadActivity" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../../usercontrols/Pager.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="ReportNav" Src="reportNav.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="AdminMenu" Src="../../usercontrols/AdministrativeMenu.ascx" %>

<Kaneva:AdminMenu ID="adminmenu" runat="server" />
<table runat="server" id="tblNoAdmin" cellpadding="0" cellspacing="0" border="0" width="560">
	<tr><td ALIGN="CENTER">
		<BR><BR><BR>
		<span class="subHead">You must be a site administrator to access this section.</span>
	</td></tr>
</table>

<asp:panel runat="server" ID="pnlAdmin">

<Kaneva:ReportNav runat="server" id="rnNavigation" SubTab="da"/>
<center>
<BR>
<table cellpadding="0" cellspacing="0" border="0" width="750">
	<tr>
		<td valign="middle" align="right" class="Filter2" width="25%" nowrap="true">
			Search from&nbsp;
		</td>
		<td valign="middle" align="left" class="Filter2" width="25%" nowrap="true">
			<asp:TextBox ID="txtStartDate" class="Filter2" Width="100" MaxLength="10" runat="server"/> (MM-dd-yyyy) 12:00:00 AM
			<asp:RequiredFieldValidator ID="rfdtxtStartDate" ControlToValidate="txtStartDate" Text="*" ErrorMessage="Start date is a required field." Display="Dynamic" runat="server"/>
			<asp:RegularExpressionValidator id="revStartDate" ControlToValidate="txtStartDate" Text="*" Display="Static" ErrorMessage="Start date must be in format mm-dd-yyyy." EnableClientScript="True" runat="server"/>
		</td>
		<td width="50%" colspan="2">
		</td>
	</tr>
	<tr>
		<td valign="middle" align="right" class="Filter2" nowrap="true">
			to&nbsp;
		</td>
		<td valign="middle" align="left" class="Filter2" nowrap="true">
			<asp:TextBox ID="txtEndDate" class="Filter2" Width="100" MaxLength="10" runat="server"/> (MM-dd-yyyy) 11:59:59 PM
			<asp:RequiredFieldValidator ID="rfdtxtEndDate" ControlToValidate="txtEndDate" Text="*" ErrorMessage="End date is a required field." Display="Dynamic" runat="server"/>
			<asp:RegularExpressionValidator id="revEndDate" ControlToValidate="txtEndDate" Text="*" Display="Static" ErrorMessage="End date must be in format mm-dd-yyyy." EnableClientScript="True" runat="server"/>
		</td>
		<td width="10%" valign="middle" align="center">
		</td>
		<td width="40%" valign="middle" align="left">
			<asp:button id="btnSearch" runat="Server" CausesValidation="True" onClick="btnSearch_Click" class="Filter2" Text=" search "/>
		</td>
	</tr>
</table>	
<BR>
<table runat="server" cellpadding="0" cellspacing="0" border="0" width="905">
	<tr Class="lineItemColHead">
		<td width="100%" align="Left">&nbsp;&nbsp;<b><asp:Label class="dateStamp" id="lblEarnings" runat="server"/></b>
		</td>
	</tr>
</table>
<table cellpadding="2" cellspacing="0" border="0" width="905" class="FullBorders">
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="15" height="20"/></td>
		<td align="right" class="bodyText" width="40%">&nbsp;# Content Downloaded:</td><td><img runat="server" src="~/images/spacer.gif" width="5"/></td>
		<td align="left" width="60%" class="bodyText" >
			<asp:Label class="dateStamp" id="lblDownloaded" runat="server"/>&nbsp;
		</td>
		<td width="10"><img runat="server" src="~/images/spacer.gif" width="15" height="18"/></td>
	</tr>
	<tr class="lineItemOdd">
		<td><img runat="server" src="~/images/spacer.gif" width="15" height="20"/></td>
		<td align="right" class="bodyText">&nbsp;# Content Published:</td><td  style="border-bottom: 1px solid #ededed;"><img runat="server" src="~/images/spacer.gif" width="5"/></td>
		<td align="left" width="260" class="bodyText">
			<asp:Label class="dateStamp" id="lblContentPublished" runat="server"/>&nbsp;
		</td>
		<td width="10"><img runat="server" src="~/images/spacer.gif" width="15" height="18"/></td>
	</tr>
</table>

<br><br>	
<table cellpadding="0" cellspacing="0" border="0" width="905">
	<tr>
		<td align="left" width="150">
			<asp:checkbox runat="server" id="chkShowFree" class="Filter2"/><span class="blueInfo">show free items</span>
		</td>
		<td align="right" class="belowFilter2" valign="middle" style="line-height:15px;">
			<asp:Label runat="server" id="lblSearch" CssClass="dateStamp"/><br><Kaneva:Pager runat="server" id="pgTop"/>
		</td>
	</tr>
</table>
<asp:DataGrid runat="server" EnableViewState="False" ShowFooter="False" Width="905" id="dgrdDownloads" cellpadding="0" cellspacing="0" border="0" AutoGenerateColumns="False" AllowSorting="True" CssClass="FullBorders">  
	<HeaderStyle CssClass="lineItemColHead"/>
	<ItemStyle CssClass="lineItemEven"/>
	<AlternatingItemStyle CssClass="lineItemOdd"/>
	<Columns>
		<asp:TemplateColumn HeaderText="member" SortExpression="username" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="middle" ItemStyle-Width="25%">
			<ItemTemplate>
				<a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "username").ToString ())%> style="COLOR: #3258ba; font-size: 12px;"><%# DataBinder.Eval(Container.DataItem, "username") %></a>&nbsp;-<%# DataBinder.Eval(Container.DataItem, "ip_address") %>&nbsp;
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="item" SortExpression="name" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="middle" ItemStyle-Width="25%">
			<ItemTemplate>
				<a class="adminLinks" title='<%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "name").ToString ())%>' href='<%# GetAssetDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "asset_id")))%>'><%# TruncateWithEllipsis (Server.HtmlDecode (DataBinder.Eval(Container.DataItem, "name").ToString ()), 35) %></a>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="streams" SortExpression="number_of_streams" ItemStyle-Width="6%" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="Top">
			<ItemTemplate>
				<%# DataBinder.Eval (Container.DataItem, "number_of_streams") %>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="downloads" SortExpression="number_of_downloads" ItemStyle-Width="6%" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="Top">
			<ItemTemplate>
				<%# DataBinder.Eval (Container.DataItem, "number_of_downloads") %>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="purchase date" SortExpression="purchase_date" ItemStyle-Width="18%" ItemStyle-ForeColor="#4863a2" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="Top">
			<ItemTemplate>
				<%# FormatDateTime (DataBinder.Eval(Container.DataItem, "purchase_date")) %>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="price" SortExpression="gross_point_amount" HeaderStyle-Wrap="False" ItemStyle-CssClass="money" ItemStyle-Width="20%" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="Top">
			<ItemTemplate>
				<%# FormatKpoints (DataBinder.Eval(Container.DataItem, "gross_point_amount")) %>
			</ItemTemplate>
		</asp:TemplateColumn>
	</Columns>
</asp:datagrid>

<br><br>
</center>
</asp:panel>