<%@ Page language="c#" Codebehind="exceptions.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.exceptions" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../../usercontrols/Pager.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="ReportNav" Src="reportNav.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="AdminMenu" Src="../../usercontrols/AdministrativeMenu.ascx" %>

<Kaneva:AdminMenu ID="adminmenu" runat="server" />

<asp:ValidationSummary ShowMessageBox="True" ShowSummary="False" Class="formError" id="valSum" DisplayMode="BulletList" runat="server" HeaderText="Please correct the following errors:"/>

<table runat="server" id="tblNoAdmin" cellpadding="0" cellspacing="0" border="0" width="560">
	<tr><td ALIGN="CENTER">
		<BR><BR><BR>
		<span class="subHead">You must be a site administrator to access this section.</span>
	</td></tr>
</table>

<asp:panel runat="server" ID="pnlAdmin">

<Kaneva:ReportNav runat="server" id="rnNavigation" SubTab="ex"/>
<center>
<BR>

<table cellpadding="0" cellspacing="0" border="0" width="905">
	<tr>
		<td valign="middle" align="right" class="Filter2" width="25%" nowrap="true">
			Search from&nbsp;
		</td>
		<td valign="middle" align="left" class="Filter2" width="25%" nowrap="true">
			<asp:TextBox ID="txtStartDate" class="Filter2" Width="100" MaxLength="10" runat="server"/> (MM-dd-yyyy) 12:00:00 AM
			<asp:RequiredFieldValidator ID="rfdtxtStartDate" ControlToValidate="txtStartDate" Text="*" ErrorMessage="Start date is a required field." Display="Dynamic" runat="server"/>
			<asp:RegularExpressionValidator id="revStartDate" ControlToValidate="txtStartDate" Text="*" Display="Static" ErrorMessage="Start date must be in format mm-dd-yyyy." EnableClientScript="True" runat="server"/>
		</td>
		<td width="50%" colspan="2">
		</td>
	</tr>
	<tr>
		<td valign="middle" align="right" class="Filter2" nowrap="true">
			to&nbsp;
		</td>
		<td valign="middle" align="left" class="Filter2" nowrap="true">
			<asp:TextBox ID="txtEndDate" class="Filter2" Width="100" MaxLength="10" runat="server"/> (MM-dd-yyyy) 11:59:59 PM
			<asp:RequiredFieldValidator ID="rfdtxtEndDate" ControlToValidate="txtEndDate" Text="*" ErrorMessage="End date is a required field." Display="Dynamic" runat="server"/>
			<asp:RegularExpressionValidator id="revEndDate" ControlToValidate="txtEndDate" Text="*" Display="Static" ErrorMessage="End date must be in format mm-dd-yyyy." EnableClientScript="True" runat="server"/>
		</td>
		<td width="10%" valign="middle" align="center">
		</td>
		<td width="40%" valign="middle" align="left">
			<asp:button id="btnSearch" runat="Server" CausesValidation="True" onClick="btnSearch_Click" class="Filter2" Text=" search "/>
		</td>
	</tr>
	<tr>
		<td width="100%" colspan="4" class="Filter2">
		<BR/><BR/>
		</td>
	</tr>
	<tr>
		<td width="100%" colspan="4" class="Filter2">
			<input type="radio" id="optReportAmount" name="optReport" value="amountover" runat="server"/>
			Transactions over $<asp:TextBox ID="txtTransactionAmount" class="Filter2" Width="100" MaxLength="10" runat="server"/> Dollars
		</td>
	</tr>
	<tr>
		<td width="100%" colspan="4" class="Filter2">
			<input type="radio" id="optReportPerDay" name="optReport" value="numtrans" runat="server"/>
			More than <asp:TextBox ID="txtTransactionsPerDay" class="Filter2" Width="100" MaxLength="10" runat="server"/> Transactions
		</td>
	</tr>	
</table>	
<BR>
<table runat="server" cellpadding="0" cellspacing="0" border="0" width="905">
	<tr>
		<td align="right">
			<asp:Label runat="server" id="lblSearch" CssClass="dateStamp"/>&nbsp;&nbsp;&nbsp;&nbsp;<br>
		</td>
	</tr>
	
	<tr>
		<td style="background-color: #ededed;" width="100%">&nbsp;&nbsp;<span class="header02" style="font-size: 15px;"><b><asp:Label class="dateStamp" id="lblEarnings" runat="server"/><b></b></span>
		</td>
	</tr>
</table>
<center>
<asp:DataGrid runat="server" EnableViewState="True" ShowFooter="False" Width="905" id="dgrdCreditTransactions" cellpadding="0" cellspacing="0" border="0" AutoGenerateColumns="False" AllowSorting="True" CssClass="FullBorders">  
	<HeaderStyle CssClass="lineItemColHead"/>
	<ItemStyle CssClass="lineItemEven"/>
	<AlternatingItemStyle CssClass="lineItemOdd"/>
	<Columns>
		<asp:TemplateColumn HeaderText="order id" SortExpression="ppt.point_transaction_id" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="middle" ItemStyle-Width="28%">
			<ItemTemplate>
				<B>KEN-KP-<%# DataBinder.Eval(Container.DataItem, "point_transaction_id")%></B><BR>
				<span style="font-size:10px; color: orange;"><%# DataBinder.Eval(Container.DataItem, "ppt_description")%></span>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="amount ($)" SortExpression="amount_debited" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="middle">
			<ItemTemplate>
				<span class="money" style="font-size:11px"><%# FormatCurrency (DataBinder.Eval(Container.DataItem, "amount_debited"))%></span>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="k-points received" SortExpression="kpoints" ItemStyle-Width="12%" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="middle">
			<ItemTemplate>
				<%# FormatKpoints (DataBinder.Eval(Container.DataItem, "kpoints"), false) %>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="payment method" SortExpression="pm.name" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="middle">
			<ItemTemplate>
				<%# DataBinder.Eval(Container.DataItem, "name")%>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="status" SortExpression="status_description" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="middle">
			<ItemTemplate>
				<A runat="server" style="color: #4863a2; font-size: 11px;" HREF='<%# GetStatusLink (Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "transaction_status")), DataBinder.Eval(Container.DataItem, "status_description").ToString (), Convert.ToDateTime (DataBinder.Eval(Container.DataItem, "transaction_date")))%>'><%# GetStatus (Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "transaction_status")), DataBinder.Eval(Container.DataItem, "status_description").ToString (), Convert.ToDateTime (DataBinder.Eval(Container.DataItem, "transaction_date")))%></a>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="purchase date" SortExpression="transaction_date" ItemStyle-Width="15%" ItemStyle-Font-Size="8px" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="middle" >
			<ItemTemplate>
				<span class="adminLinks" style="font-size:8px;"><%# FormatDateTime (DataBinder.Eval(Container.DataItem, "transaction_date"))%></span>
			</ItemTemplate>
		</asp:TemplateColumn>
		</Columns>
</asp:datagrid>


<asp:DataGrid runat="server" EnableViewState="False" ShowFooter="False" Width="730" id="dgrdNumberTransactions" cellpadding="0" cellspacing="0" border="0" AutoGenerateColumns="False" AllowSorting="True">  
	<HeaderStyle CssClass="lineItemColHead"/>
	<ItemStyle CssClass="lineItemEven"/>
	<AlternatingItemStyle CssClass="lineItemOdd"/>
	<Columns>
		<asp:TemplateColumn HeaderText="username" SortExpression="username" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="middle" ItemStyle-Width="28%">
			<ItemTemplate>
				<a class="bodyText"  style="color: #5D8CCC;" href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "username").ToString ())%>><%# DataBinder.Eval(Container.DataItem, "username") %></a>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="# transactions" SortExpression="transactions" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="middle" ItemStyle-Width="28%">
			<ItemTemplate>
				<%# DataBinder.Eval(Container.DataItem, "transactions")%>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="total ($)" SortExpression="total" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="middle">
			<ItemTemplate>
				<%# FormatCurrency (DataBinder.Eval(Container.DataItem, "total"))%>
			</ItemTemplate>
		</asp:TemplateColumn>
		</Columns>
</asp:datagrid>
</center>
	<br><br>
</center>
</asp:panel>