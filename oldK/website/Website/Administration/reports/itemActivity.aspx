<%@ Page language="c#" Codebehind="itemActivity.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.itemActivity" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../../usercontrols/Pager.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="ReportNav" Src="reportNav.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="AdminMenu" Src="../../usercontrols/AdministrativeMenu.ascx" %>

<Kaneva:AdminMenu ID="adminmenu" runat="server" />
<asp:ValidationSummary ShowMessageBox="True" ShowSummary="False" Class="formError" id="valSum" DisplayMode="BulletList" runat="server" HeaderText="Please correct the following errors:"/>

<table runat="server" id="tblNoAdmin" cellpadding="0" cellspacing="0" border="0" width="560">
	<tr><td ALIGN="CENTER">
		<BR><BR><BR>
		<span class="subHead">You must be a site administrator to access this section.</span>
	</td></tr>
</table>

<asp:panel runat="server" ID="pnlAdmin">

<Kaneva:ReportNav runat="server" id="rnNavigation" SubTab="ia"/>
<center>
<BR>
<table cellpadding="0" cellspacing="0" border="0" width="750">
	<tr>
		<td valign="middle" align="right" class="Filter2" width="25%" nowrap="true">
			Search from&nbsp;
		</td>
		<td valign="middle" align="left" class="Filter2" width="25%" nowrap="true">
			<asp:TextBox ID="txtStartDate" class="Filter2" Width="100" MaxLength="10" runat="server"/> (MM-dd-yyyy) 12:00:00 AM
			<asp:RequiredFieldValidator ID="rfdtxtStartDate" ControlToValidate="txtStartDate" Text="*" ErrorMessage="Start date is a required field." Display="Dynamic" runat="server"/>
			<asp:RegularExpressionValidator id="revStartDate" ControlToValidate="txtStartDate" Text="*" Display="Static" ErrorMessage="Start date must be in format mm-dd-yyyy." EnableClientScript="True" runat="server"/>
		</td>
		<td width="50%" colspan="2">
		</td>
	</tr>
	<tr>
		<td valign="middle" align="right" class="Filter2" nowrap="true">
			to&nbsp;
		</td>
		<td valign="middle" align="left" class="Filter2" nowrap="true">
			<asp:TextBox ID="txtEndDate" class="Filter2" Width="100" MaxLength="10" runat="server"/> (MM-dd-yyyy) 11:59:59 PM
			<asp:RequiredFieldValidator ID="rfdtxtEndDate" ControlToValidate="txtEndDate" Text="*" ErrorMessage="End date is a required field." Display="Dynamic" runat="server"/>
			<asp:RegularExpressionValidator id="revEndDate" ControlToValidate="txtEndDate" Text="*" Display="Static" ErrorMessage="End date must be in format mm-dd-yyyy." EnableClientScript="True" runat="server"/>
		</td>
		<td width="10%" valign="middle" align="center">
		</td>
		<td width="40%" valign="middle" align="left">
			<asp:button id="btnSearch" runat="Server" CausesValidation="True" onClick="btnSearch_Click" class="Filter2" Text=" search "/>
		</td>
	</tr>
</table>	
<BR>
<table runat="server" cellpadding="0" cellspacing="0" border="0" width="905">
	<tr Class="lineItemColHead">
		<td width="100%" align="left">&nbsp;&nbsp;<span class="header02" style="font-size: 15px;"><b><asp:Label class="dateStamp" id="lblEarnings" runat="server"/><b></b></span>
		</td>
	</tr>
</table>
<table cellpadding="0" cellspacing="0" border="0" width="905"class="FullBorders">
	<tr class="lineItemOdd">
		<td><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
		<td align="right" class="bodyText" width="30%">&nbsp;Items Created:</td><td ><img runat="server" src="~/images/spacer.gif" width="5"/></td>
		<td align="left" width="70%" class="bodyText">
			<asp:Label class="dateStamp" id="lblAssetsCreated" runat="server"/>&nbsp;
		</td>
		<td width="10"><img runat="server" src="~/images/spacer.gif" width="20" height="18"/></td>
	</tr>
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
		<td align="right" class="bodyText">&nbsp;Items Updated:</td><td><img runat="server" src="~/images/spacer.gif" width="5"/></td>
		<td align="left" class="bodyText">
			<asp:Label class="dateStamp" id="lblAssetsUpdated" runat="server"/>&nbsp;
		</td>
		<td width="10"><img runat="server" src="~/images/spacer.gif" width="20" height="18"/></td>
	</tr>
	<tr class="lineItemOdd">
		<td><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
		<td align="right" class="bodyText"  nowrap="true">&nbsp;Items Deleted:</td><td><img runat="server" src="~/images/spacer.gif" width="5"/></td>
		<td align="left" class="bodyText">
			<asp:Label class="dateStamp" id="lblAssetsDeleted" runat="server"/>&nbsp;
		</td>
		<td width="10"><img runat="server" src="~/images/spacer.gif" width="20" height="18"/></td>
	</tr>
</table>	

<br><br>
<table runat="server" cellpadding="0" cellspacing="0" border="0" width="905">
	<tr Class="lineItemColHead">
		<td width="100%" align="Left">&nbsp;&nbsp;<b><asp:Label class="dateStamp" id="lblChannels" runat="server"/></b>
		</td>
	</tr>
</table>
<table cellpadding="0" cellspacing="0" border="0" width="905">
	<tr>
		<td align="right" class="belowFilter2" valign="middle" style="line-height:15px;"><asp:Label runat="server" id="lblSearch" CssClass="dateStamp"/><br><Kaneva:Pager runat="server" id="pgTop"/><br></td>						
	</tr>
</table>
<asp:DataGrid runat="server" EnableViewState="False" ShowFooter="False" Width="905" id="dgrdItems" cellpadding="0" cellspacing="0" border="0" AutoGenerateColumns="False" AllowSorting="True" CssClass="FullBorders">  
	<HeaderStyle CssClass="lineItemColHead"/>
	<ItemStyle CssClass="lineItemEven"/>
	<AlternatingItemStyle CssClass="lineItemOdd"/>
	<Columns>
		<asp:TemplateColumn HeaderText="name" SortExpression="name" ItemStyle-Width="30%" ItemStyle-HorizontalAlign="Left">
			<ItemTemplate>
				<a href='<%# GetAssetDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "asset_id")))%>' style="COLOR: #3258ba; font-size: 10px;"><%# TruncateWithEllipsis (DataBinder.Eval (Container.DataItem, "name").ToString (), 50) %></a>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="owner" SortExpression="username" ItemStyle-Width="20%" ItemStyle-HorizontalAlign="Center">
			<ItemTemplate>
				<a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "username").ToString ())%> style="COLOR: #3258ba; font-size: 12px;"><%# DataBinder.Eval(Container.DataItem, "username") %></a>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="type" SortExpression="asset_type_id" ItemStyle-CssClass="adminLinks" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center">
			<ItemTemplate>
				<%# GetAssetTypeName (Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_type_id")))%>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="updated date" SortExpression="last_updated_date" ItemStyle-CssClass="adminLinks" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center">
			<ItemTemplate>
				<%# FormatDateTime (DataBinder.Eval (Container.DataItem, "last_updated_date")) %>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="size" SortExpression="file_size" ItemStyle-Width="5%" ItemStyle-HorizontalAlign="Center">
			<ItemTemplate>
				<font color="green"><%# FormatImageSize (DataBinder.Eval (Container.DataItem, "file_size")) %></font>
			</ItemTemplate>
		</asp:TemplateColumn>		
		<asp:TemplateColumn HeaderText="published" SortExpression="created_date" ItemStyle-CssClass="adminLinks" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center">
			<ItemTemplate>
				<%# DataBinder.Eval (Container.DataItem, "created_date") %>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="status" SortExpression="status_id" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="top" ItemStyle-Width="25%" ItemStyle-Wrap="true">
			<ItemTemplate>
				<A runat="server" style="color: #4863a2; font-size: 11px;" HREF='#' onclick='<%# GetStatusLink (Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "publish_status_id")))%>'><%# GetStatusText (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "publish_status_id")), Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "status_id")), Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "asset_id"))) %></a>
			</ItemTemplate>
		</asp:TemplateColumn>
	</Columns>
</asp:datagrid>

<br><br>


</center>
</asp:panel>
