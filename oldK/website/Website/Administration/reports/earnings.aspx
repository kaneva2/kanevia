<%@ Page language="c#" Codebehind="earnings.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.earnings" %>
<%@ Register TagPrefix="Kaneva" TagName="ReportNav" Src="reportNav.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="AdminMenu" Src="../../usercontrols/AdministrativeMenu.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../../usercontrols/Pager.ascx" %>

<Kaneva:AdminMenu ID="adminmenu" runat="server" />

<asp:ValidationSummary ShowMessageBox="True" ShowSummary="False" Class="formError" id="valSum" DisplayMode="BulletList" runat="server" HeaderText="Please correct the following errors:"/>

<table runat="server" id="tblNoAdmin" cellpadding="0" cellspacing="0" border="0" width="560">
	<tr><td ALIGN="CENTER">
		<BR><BR><BR>
		<span class="subHead">You must be a site administrator to access this section.</span>
	</td></tr>
</table>

<asp:panel runat="server" ID="pnlAdmin">

<Kaneva:ReportNav runat="server" id="rnNavigation" SubTab="ea"/>
<center>
<BR>
<table cellpadding="0" cellspacing="0" border="0" width="730">
	<tr>
		<td valign="middle" align="right" class="Filter2" width="25%" nowrap="true">
			Search from&nbsp;
		</td>
		<td valign="middle" align="left" class="Filter2" width="25%" nowrap="true">
			<asp:TextBox ID="txtStartDate" class="Filter2" Width="100" MaxLength="10" runat="server"/> (MM-dd-yyyy) 12:00:00 AM
			<asp:RequiredFieldValidator ID="rfdtxtStartDate" ControlToValidate="txtStartDate" Text="*" ErrorMessage="Start date is a required field." Display="Dynamic" runat="server"/>
			<asp:RegularExpressionValidator id="revStartDate" ControlToValidate="txtStartDate" Text="*" Display="Static" ErrorMessage="Start date must be in format mm-dd-yyyy." EnableClientScript="True" runat="server"/>
		</td>
		<td width="50%" colspan="2">
		</td>
	</tr>
	<tr>
		<td valign="middle" align="right" class="Filter2" nowrap="true">
			to&nbsp;
		</td>
		<td valign="middle" align="left" class="Filter2" nowrap="true">
			<asp:TextBox ID="txtEndDate" class="Filter2" Width="100" MaxLength="10" runat="server"/> (MM-dd-yyyy) 11:59:59 PM
			<asp:RequiredFieldValidator ID="rfdtxtEndDate" ControlToValidate="txtEndDate" Text="*" ErrorMessage="End date is a required field." Display="Dynamic" runat="server"/>
			<asp:RegularExpressionValidator id="revEndDate" ControlToValidate="txtEndDate" Text="*" Display="Static" ErrorMessage="End date must be in format mm-dd-yyyy." EnableClientScript="True" runat="server"/>
		</td>
		<td width="10%" valign="middle" align="center">
		</td>
		<td width="40%" valign="middle" align="left">
			<asp:button id="btnSearch" runat="Server" CausesValidation="True" onClick="btnSearch_Click" class="Filter2" Text=" search "/>
		</td>
	</tr>
</table>	
<BR>
<table runat="server" cellpadding="0" cellspacing="0" border="0" width="905">
	<tr Class="lineItemColHead">
		<td width="100%" align="Left">&nbsp;&nbsp;<span class="header02" style="font-size: 15px;"><b><asp:Label class="dateStamp" id="lblEarnings" runat="server"/><b></b></span>
		</td>
	</tr>
</table>

<table cellpadding="2" cellspacing="0" border="0" width="905" class="FullBorders">
	<tr class="lineItemEven">
		<td><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
		<td align="right" class="bodyText">&nbsp;License Subscription Sales:</td><td><img runat="server" src="~/images/spacer.gif" width="5"/></td>
		<td align="left" class="bodyText">
			<asp:Label class="filter2" id="lblSubscriptionsSold" runat="server"/>&nbsp;
		</td>
		<td align="left" class="bodyText">
			<asp:Label class="filter2" id="lblSubTransactions" runat="server"/>&nbsp;
		</td>
		<td width="10"><img runat="server" src="~/images/spacer.gif" width="20" height="18"/></td>
	</tr>
	<tr class="lineItemOdd">
		<td><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
		<td align="right" width="200" class="bodyText">&nbsp;K-Points Sales:</td><td ><img runat="server" src="~/images/spacer.gif" width="5"/></td>
		<td align="left" class="bodyText" >
			<asp:Label class="filter2" id="lblKPointsSold" runat="server"/>&nbsp;
		</td>
		<td align="left" class="bodyText" >
			<asp:Label class="filter2" id="lblKPointTransactions" runat="server"/>&nbsp;
		</td>
		<td width="10"><img runat="server" src="~/images/spacer.gif" width="20" height="18"/></td>
	</tr>
	<tr class="lineItemEven">
		<td><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
		<td align="right" class="bodyText">&nbsp;Total K-Points Spent:</td><td><img runat="server" src="~/images/spacer.gif" width="5"/></td>
		<td align="left" class="bodyText">
			<asp:Label class="filter2" id="lblKPointsSpent" runat="server"/>&nbsp;
		</td>
		<td align="left" class="bodyText">
			<asp:Label class="filter2" id="lblKPointSpentTransactions" runat="server"/>&nbsp;
		</td>
		<td width="10"><img runat="server" src="~/images/spacer.gif" width="20" height="18"/></td>
	</tr>
	<tr class="lineItemOdd">
		<td><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
		<td align="right" class="bodyText" nowrap="true">&nbsp;Total User Royalties Earned:</td><td ><img runat="server" src="~/images/spacer.gif" width="5"/></td>
		<td align="left" class="bodyText" >
			<asp:Label class="filter2" id="lblRoyaltyTotal" runat="server"/>&nbsp;
		</td>
		<td align="left" class="bodyText">
			<asp:Label class="filter2" id="lblRoyaltyTransactions" runat="server"/>&nbsp;
		</td>
		<td width="10"><img runat="server" src="~/images/spacer.gif" width="20" height="18"/></td>
	</tr>
	<tr class="lineItemEven">
		<td><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
		<td align="right" class="bodyText" nowrap="true">&nbsp;Outstanding balances:</td><td><img runat="server" src="~/images/spacer.gif" width="5"/></td>
		<td colspan="2" align="left" class="bodyText">
			<asp:Label class="filter2" id="lblBalance" runat="server"/>&nbsp;
		</td>
		<td width="10"><img runat="server" src="~/images/spacer.gif" width="20" height="18"/></td>
	</tr>
</table>	
<br><br>														

<table runat="server" cellpadding="0" cellspacing="0" border="0" width="905">
	<tr Class="lineItemColHead">
		<td width="100%" align="Left">&nbsp;&nbsp;<b><asp:Label class="dateStamp" id="lblSales" runat="server"/></b>
		</td>
	</tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="905">
	<tr>
		<td align="right" class="belowFilter2" valign="middle" style="line-height:15px;"><asp:Label runat="server" id="lblSearch" CssClass="dateStamp"/><br><Kaneva:Pager runat="server" id="pgTop"/><br></td>						
	</tr>
</table>
<asp:DataGrid runat="server" EnableViewState="False" ShowFooter="False" Width="905" id="dgrdSales" cellpadding="0" cellspacing="0" border="0" AutoGenerateColumns="False" AllowSorting="True" CssClass="FullBorders">  
	<HeaderStyle CssClass="lineItemColHead"/>
	<ItemStyle CssClass="lineItemEven"/>
	<AlternatingItemStyle CssClass="lineItemOdd"/>
	<Columns>
		<asp:TemplateColumn HeaderText="item name" SortExpression="a.name" ItemStyle-Width="40%" ItemStyle-VerticalAlign="top">
			<ItemTemplate>
				<a href='<%# GetAssetDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "asset_id")))%>' style="COLOR: #3258ba; font-size: 10px;"><%# TruncateWithEllipsis (DataBinder.Eval (Container.DataItem, "name").ToString (), 50) %></a>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="streams" SortExpression="number_of_streams" ItemStyle-Width="8%" ItemStyle-HorizontalAlign="Center">
			<ItemTemplate>
				<B><%# DataBinder.Eval (Container.DataItem, "number_of_streams") %></b>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="downloads" SortExpression="count" ItemStyle-Width="7%" ItemStyle-HorizontalAlign="Center">
			<ItemTemplate>
				<B><%# DataBinder.Eval (Container.DataItem, "count") %></b>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="amount sold" SortExpression="point_amount" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Center">
			<ItemTemplate>
				<span class="money"><%#FormatCurrency (ConvertKPointsToDollars (Convert.ToDouble (DataBinder.Eval(Container.DataItem, "point_amount"))))%></span>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="royalty" SortExpression="royalty_amount" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Center">
			<ItemTemplate>
				<span class="money"><%#FormatCurrency (ConvertKPointsToDollars (Convert.ToDouble (DataBinder.Eval(Container.DataItem, "royalty_amount"))))%></span>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="current price" SortExpression="a.amount" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Center">
			<ItemTemplate>
				<span class="money"><%# FormatKpoints (DataBinder.Eval (Container.DataItem, "amount"), false) %></span> 
			</ItemTemplate>
		</asp:TemplateColumn>
	</Columns>
</asp:datagrid>

<br><br>
</center>
</asp:panel>
