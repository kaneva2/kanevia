///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

namespace KanevaWeb.reports
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

	/// <summary>
	///		Summary description for reportNav.
	/// </summary>
	public class reportNav : System.Web.UI.UserControl
	{

		private void Page_Load(object sender, System.EventArgs e)
		{
			switch (SubTab)
			{
				case "ea":
				{
					aEarnings.InnerHtml = "<B>" + aEarnings.InnerText + "</B>";
					break;
				}
				case "sa":
				{
					aSales.InnerHtml = "<B>" + aSales.InnerText + "</B>";
					break;
				}
				case "da":
				{
					aDownloads.InnerHtml = "<B>" + aDownloads.InnerText + "</B>";
					break;
				}
				case "ex":
				{
					aExceptions.InnerHtml = "<B>" + aExceptions.InnerText + "</B>";
					break;
				}
				case "oa":
				{
					aObjectActivity.InnerHtml = "<B>" + aObjectActivity.InnerText + "</B>";
					break;
				}
				case "ia":
				{
					aItemActivity.InnerHtml = "<B>" + aItemActivity.InnerText + "</B>";
					break;
				}
				case "ua":
				{
					aUserActivity.InnerHtml = "<B>" + aUserActivity.InnerText + "</B>";
					break;
				}
				case "pd":
				{
					aPlatformDownloads.InnerHtml = "<B>" + aPlatformDownloads.InnerText + "</B>";
					break;
				}
				case "il":
				{
					aInvalidLogin.InnerHtml = "<B>" + aInvalidLogin.InnerText + "</B>";
					break;
				}
				case "pl":
				{
					aPlatformLicense.InnerHtml = "<B>" + aPlatformLicense.InnerText + "</B>";
					break;
				}
				case "pf":
				{
					aPlatformFeedback.InnerHtml = "<B>" + aPlatformFeedback.InnerText + "</B>";
					break;
				}
				case "kpi":
				{
					aKPI.InnerHtml = "<B>" + aKPI.InnerText + "</B>";
					break;
				}
        case "cohort":
        {
          aCohort.InnerHtml = "<B>" + aCohort.InnerText + "</B>";
          break;
        }
        case "fi":
        {
            aFriendInvites.InnerHtml = "<B>" + aFriendInvites.InnerText + "</B>";
            break;
        }
      }
		}

		/// <summary>
		/// The SubTab to display
		/// </summary>
		public string SubTab
		{
			get 
			{
				return m_SubTab;
			} 
			set
			{
				m_SubTab = value;
			}
		}

		protected string m_SubTab = "";

		protected HtmlAnchor aEarnings, aSales, aExceptions, aDownloads, aUserActivity, 
			aObjectActivity, aItemActivity, aPlatformDownloads, aInvalidLogin, aPlatformLicense, aPlatformFeedback, aKPI,aCohort, aFriendInvites;
		protected System.Web.UI.HtmlControls.HtmlTableCell tdReportTabs;

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
