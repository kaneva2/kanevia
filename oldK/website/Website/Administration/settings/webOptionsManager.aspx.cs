///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva.Administration
{
    public partial class webOptionsManager: NoBorderPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Redirect if user is not admin
            if (!IsAdministrator())
            {
                Response.Redirect("~/default.aspx");
            }

            if (!IsPostBack)
            {
                IList<string> allGroups = Configuration.GetAvailableWebOptionGroups();
                if( allGroups!=null )
                {
                    ddlGroup.DataSource = allGroups;
                    ddlGroup.DataBind();
                }

                ddlGroup.Items.Insert(0, new ListItem("--Select--", ""));
                ddlGroup_SelectedIndexChanged(null, null);

                BindData(1);
                LeaveEditMode();
            }
            else
            {
                int pageNumber = Convert.ToInt32(hidPageId.Value);
                BindData(pageNumber);
            }
        }

        protected void gvWebOptions_RowEditing(object source, GridViewEditEventArgs e)
        {
            //passin the selected row and populate the fields
            EditRow(e.NewEditIndex, gvWebOptions.Rows[e.NewEditIndex], 1);
        }

        protected void pgWebOptions_PageChange(object sender, PageChangeEventArgs e)
        {
            BindData(e.PageNumber);
        }

        protected void ddlGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(ddlGroup.SelectedValue!="")
            {
                IList<WebOption> availOpts = Configuration.GetAvailableWebOptions(ddlGroup.SelectedValue);
                ddlKey.DataSource = availOpts;
                ddlKey.DataTextField = "Key";
                ddlKey.DataValueField = "Key";
                ddlKey.DataBind();
                ddlKey.Items.Insert(0, new ListItem("--Select--", ""));
                ddlKey.Enabled = true;

                ddlHiddenDesc.DataSource = availOpts;
                ddlHiddenDesc.DataTextField = "Description";
                ddlHiddenDesc.DataValueField = "Key";
                ddlHiddenDesc.DataBind();
                ddlHiddenDesc.Items.Insert(0, new ListItem("", ""));

                lblValueHelp.Text = "";
            }
            else
            {
                ddlKey.Items.Clear();
                ddlKey.Enabled = false;
            }
        }

        protected void ddlKey_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlHiddenDesc.SelectedValue = ddlKey.SelectedValue;
            lblValueHelp.Text = ddlHiddenDesc.SelectedItem.Text;

            if (ddlKey.SelectedValue != "")
            {
                txtRole.Enabled = true;
                txtValue.Enabled = true;
                btnSave.Enabled = true;
            }
            else
            {
                txtRole.Enabled = false;
                txtValue.Enabled = false;
                btnSave.Enabled = false;
            }
        }

        /// <summary>
        /// BindMemberFameData
        /// </summary>
        private void BindData(int pageNumber)
        {
            hidPageId.Value = Convert.ToString(pageNumber);
            try
            {
                PagedList<WebOption> webOptionsList = (PagedList<WebOption>)Configuration.GetWebOptions(pageNumber, pageSize);
                if (webOptionsList != null)
                {
                    gvWebOptions.DataSource = webOptionsList;
                    gvWebOptions.DataBind();

                    // Set current page
                    pgWebOptions.CurrentPageNumber = pageNumber;
                    pgWebOptions.NumberOfPages = Math.Ceiling((double)webOptionsList.TotalCount / pageSize).ToString();
                    pgWebOptions.DrawControl();
                }
                else
                {
                    // no records found
                    // todo...
                }
            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// AddRow
        /// </summary>
        protected void AddRow()
        {
            ddlGroup.Enabled = true;
            ddlGroup.SelectedValue = "";
            ddlGroup_SelectedIndexChanged(null, null);

            hidOrigRole.Value = "-1";

            txtRole.Enabled = false;
            txtValue.Enabled = false;
            btnSave.Enabled = false;
            btnRemove.Enabled = false;
            btnCancel.Enabled = true;
            divEditPanel.Visible = true;
        }

        /// <summary>
        /// EditRow
        /// </summary>
        private void EditRow(int index, GridViewRow gvr, int pageNumber)
        {
            string group = gvr.Cells[0].Text;
            string key = gvr.Cells[1].Text;
            int role = 0;
            try
            {
                role = Convert.ToInt32(gvr.Cells[2].Text);
            }
            catch (System.Exception)
            {
                ShowErrorOnStartup("Internal error - casting failed on role");
                return;
            }
            string helpMsg = gvr.Cells[3].Text;

            divEditPanel.Visible = true;

            ddlGroup.SelectedValue = gvr.Cells[0].Text;
            ddlGroup_SelectedIndexChanged(null, null);
            ddlKey.SelectedValue = gvr.Cells[1].Text;
            hidOrigRole.Value = txtRole.Text = Convert.ToString(role);
            lblValueHelp.Text = helpMsg;

            WebOption opt = Configuration.GetWebOption(group, key, role);
            if (opt == null || opt.Value==null)
            {
                lblValueComment.Text = "(NULL)";
                txtValue.Text = "";
            }
            else if (opt.Value=="~*INFO*~")
            {
                ShowErrorOnStartup("Cannot edit an information row", false);
                LeaveEditMode();
            }
            else if (opt.Value == "")
            {
                lblValueComment.Text = "(Empty String)";
                txtValue.Text = "";
            }
            else
            {
                lblValueComment.Text = "";
                txtValue.Text = opt.Value;
            }

            ddlGroup.Enabled = true;
            ddlKey.Enabled = true;
            txtRole.Enabled = true;
            txtValue.Enabled = true;
            btnSave.Enabled = true;
            btnRemove.Enabled = true;
            btnCancel.Enabled = true;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (hidPageId.Value == null || hidPageId.Value == "")
                return;

            int origRole = Convert.ToInt32(hidOrigRole.Value);   // No try-catch: guard by type conversion in EditRow()
            int newRole = 0;

            try
            {
                txtRole.Text.Trim();
                newRole = Convert.ToInt32(txtRole.Text);
            }
            catch (System.Exception)
            {
                newRole = -1;
            }

            if (newRole < 0)
            {
                ShowErrorOnStartup("Role priority must be a non-negative integer");
                return;
            }

            if (newRole != origRole && origRole>=0)
            {
                WebOption opt = Configuration.GetWebOption(ddlGroup.SelectedValue, ddlKey.SelectedValue, newRole);
                if (opt != null)
                {
                    ShowErrorOnStartup("Settings for " + ddlGroup.SelectedValue + "." + ddlKey.SelectedValue + ", role " + newRole + " already exists");
                    return;
                }
            }

            Configuration.SetWebOption(ddlGroup.SelectedValue, ddlKey.SelectedValue, newRole, txtValue.Text, lblValueHelp.Text.Replace("&nbsp;", ""), origRole);
            int pageNumber = Convert.ToInt32(hidPageId.Value);
            BindData(pageNumber);
            LeaveEditMode();
        }

        protected void btnRemove_Click(object sender, EventArgs e)
        {
            if (hidPageId.Value == null || hidPageId.Value == "")
                return;

            int role = Convert.ToInt32(hidOrigRole.Value);     // No try-catch: guard by type conversion in EditRow()

            Configuration.SetWebOption(ddlGroup.SelectedValue, ddlKey.SelectedValue, role, null, null);
            int pageNumber = Convert.ToInt32(hidPageId.Value);
            BindData(pageNumber);
            LeaveEditMode();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            LeaveEditMode();
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            AddRow();
        }

        protected void LeaveEditMode()
        {
            divEditPanel.Visible = false;
            ddlGroup.SelectedValue = "";
            ddlKey.Items.Clear();
            txtValue.Text = "";
            ddlGroup.Enabled = false;
            ddlKey.Enabled = false;
            txtRole.Enabled = false;
            txtValue.Enabled = false;
            btnSave.Enabled = false;
            btnRemove.Enabled = false;
            btnCancel.Enabled = false;
            lblValueComment.Text = "";
        }

        protected GridView gvWebOptions;
        protected Pager pgWebOptions;
        protected int pageSize = 25;
    }
}
