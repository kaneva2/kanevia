<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="webOptionsManager.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.Administration.webOptionsManager" %>
<%@ Register TagPrefix="Kaneva" TagName="AdminMenu" Src="../../usercontrols/AdministrativeMenu.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../../usercontrols/Pager.ascx" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<Kaneva:AdminMenu ID="adminmenu" runat="server" />
<div>
&nbsp;
</div>
<div>
	<ajax:ajaxpanel id="ajpWebOptionsManager" runat="server">  
		
		<asp:gridview id="gvWebOptions" runat="server"
			OnRowEditing="gvWebOptions_RowEditing" 
			autogeneratecolumns="False" 
			width="1020px"
			AllowSorting="True" border="0" cellpadding="4" 
			PageSize="20" AllowPaging="False" Align="center" 
			bordercolor="DarkGray" borderstyle="solid" borderwidth="1px">
			
			<HeaderStyle BackColor="Gray" Font-Underline="True" Font-Bold="True" ForeColor="#000000" HorizontalAlign="Left"></HeaderStyle>
			<RowStyle BackColor="White" Font-Size="10"></RowStyle>
			<AlternatingRowStyle BackColor="Gainsboro"></AlternatingRowStyle>
			<FooterStyle BackColor="#FFffff"></FooterStyle>
			<Columns>
				<asp:BoundField HeaderText="Group" DataField="Group" ReadOnly="True" ConvertEmptyStringToNull="False" itemstyle-horizontalalign="center" itemstyle-width="60px" headerstyle-horizontalalign="center"></asp:BoundField>
				<asp:BoundField HeaderText="Key" DataField="Key" ReadOnly="True" ConvertEmptyStringToNull="False" itemstyle-width="160px" ></asp:BoundField>
				<asp:BoundField HeaderText="Role" DataField="Role" ReadOnly="False" ConvertEmptyStringToNull="False" itemstyle-width="60px" ></asp:BoundField>
				<asp:BoundField HeaderText="Help / Semantics" DataField="Description" ReadOnly="False" ConvertEmptyStringToNull="False" itemstyle-width="560px" ></asp:BoundField>
				<asp:BoundField HeaderText="Value" DataField="Value" ReadOnly="False" ConvertEmptyStringToNull="False" itemstyle-width="80px" ></asp:BoundField>
				<asp:CommandField CausesValidation="False" ShowEditButton="True" itemstyle-width="40px" ></asp:CommandField>        
			</Columns>
		</asp:gridview>
	
		<div style="width:1020px;text-align:right;margin:10px 6px 0 0;text-align:center;"><kaneva:pager runat="server" isajaxmode="True" onpagechanged="pgWebOptions_PageChange" id="pgWebOptions" maxpagestodisplay="5" shownextprevlabels="true" /></div>

        <div style="width:1020px;text-align:right;margin:10px 6px 0 0;text-align:right;">
            <asp:button runat="server" id="btnAdd" width="180" text="Add New Entry" onclick="btnAdd_Click" />
        </div>
	
	    <div id="divEditPanel" runat="server" style="width:720px;font-size:10pt;text-align:center;">
	        <table style="width:100%;background-color:gray" cellspacing="1" cellpadding="4">
	          <tr style="background-color:white;">
	            <td><b>Group</b></td>
	            <td><asp:DropDownList runat="server" id="ddlGroup" width="100" AutoPostBack="true" OnSelectedIndexChanged="ddlGroup_SelectedIndexChanged"></asp:DropDownList></td>
	          </tr>
	          <tr style="background-color:white">
	            <td><b>Key</b></td>
	            <td>
	              <asp:DropDownList runat="server" id="ddlKey" width="300" AutoPostBack="true" OnSelectedIndexChanged="ddlKey_SelectedIndexChanged"></asp:DropDownList>
	              <asp:DropDownList runat="server" id="ddlHiddenDesc" visible="false"></asp:DropDownList>
	            </td>
	          </tr>
	          <tr style="background-color:white">
	            <td><b>Role</b></td>
	            <td>
	              <asp:TextBox runat="server" id="txtRole" maxlength="40" width="50"></asp:TextBox>
  	              <asp:HiddenField runat="server" id="hidOrigRole"></asp:HiddenField>
  	              <div style="font-size:8pt;font-family:Arial;color:#666666;">Enter 0 for all roles.</div>
	            </td>
	          </tr>
	          <tr style="background-color:white">
	            <td><b>Value</b></td>
	            <td>
	                <asp:TextBox runat="server" id="txtValue" maxlength="250" width="100"></asp:TextBox>
                    <asp:label runat="server" id="lblValueComment"></asp:label>
                    <br />
                    <div style="font-size:8pt;font-family:Arial;color:#666666;"><asp:label runat="server" id="lblValueHelp"></asp:label></div>
	            </td>
	          </tr>
	          <tr style="background-color:white">
	            <td><b>&nbsp;</b></td>
	            <td>
	                <asp:hiddenfield runat="server" id="hidPageId"></asp:hiddenfield>
	                <asp:button runat="server" id="btnSave" width="80" text="Save" onclick="btnSave_Click"></asp:button>
                    <asp:button runat="server" id="btnRemove" width="80" text="Remove" onclick="btnRemove_Click"></asp:button>	                
                    <asp:button runat="server" id="btnCancel" width="80" text="Cancel" onclick="btnCancel_Click"></asp:button>	
	            </td>
	          </tr>
	        </table>
	    </div>
	
	</ajax:ajaxpanel> 
	
	<asp:Label id="lblNoRecords" visible="false" runat="server">
		<span style="font-size:16px;color:Red;font-weight:bold;margin:30px 0;width:95%;text-align:center;">No web options were found.</span>
	</asp:Label>
    
</div>
