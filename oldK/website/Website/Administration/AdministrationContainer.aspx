<%@ Page language="c#" Codebehind="KPI.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.AdministrationContainer" %>
<%@ Register TagPrefix="Kaneva" TagName="AdminMenu" Src="../usercontrols/AdministrativeMenu.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="ReportNav" Src="reports/reportNav.ascx" %>
 
<table id="tbl_container" cellSpacing="0" cellPadding="0" width="100%" border="0" runat="server">
    <tr>
        <td colspan="2" align="left">
            <!-- Global navigation -->
            <Kaneva:AdminMenu ID="adminmenu" runat="server" />
            <KANEVA:REPORTNAV id="rnNavigation" runat="server" SubTab="kpi"></KANEVA:REPORTNAV>
        </td>
    </tr>
    <tr>
        <td style="width:2px">
            <!-- repeater for local nav -->
            &nbsp;
        </td>
        <td align="left" style="width:910px">
            <!-- access denied panel -->
            <asp:panel runat="server" ID="pnlNoAdmin"><span class="subHead">You must be a site administrator to access this section.</span></asp:panel> 
            <asp:panel runat="server" ID="pnlAdmin">
                <!-- for error messages -->
                <asp:Label runat="server" id="Messages" visible="false"></asp:Label>
                <!-- dynamically loaded user control -->       
                <asp:PlaceHolder runat="server" id="ucShell"></asp:PlaceHolder>
            </asp:panel> 
        </td>
    </tr>
</table>