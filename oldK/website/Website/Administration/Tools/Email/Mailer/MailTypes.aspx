<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MailTypes.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.Administration.Mailer.MailTypes" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
	<title>Email Types</title>   
	<style type="text/css" media="screen">@import url( AdminMailer.css );</style>       
</head>
<body>

<form id="form1" runat="server">

<div id="dvMailerNav" runat="server">
	<a href="Mailer.aspx">Mailer</a> > 
	<a href="MailTypes.aspx">Email Types</a>
</div>
<br />
<asp:LinkButton ID="lbListTypes" runat="server" Text="List Types" OnClick="btnListTypes_Click"></asp:LinkButton>
<asp:LinkButton ID="lbNewType" runat="server" Text="New" OnClick="btnNewType_Click"></asp:LinkButton>
<br />
<br />

<div id="dvTypeEdit" runat="server">

	<asp:TextBox ID="txtTypeId" runat="server" Visible="false" ></asp:TextBox>  
	
	<table>
		<tr>
			<td>

				<table>
					<tr>
						<td>Name</td>
						<td><asp:TextBox ID="txtTypeName" runat="server" Columns="50" ></asp:TextBox></td>
						<td></td>
					</tr>
					<tr>
						<td>Description</td>
						<td><asp:TextBox ID="txtDescription" runat="server"  Columns="50" > ></asp:TextBox></td>
						<td></td>
					</tr>
					<tr>
						<td>Tier</td>
						<td>
							<asp:DropDownList ID="ddlTier" runat="server">
								<asp:ListItem Value="1" Text="1" />
								<asp:ListItem Value="2" Text="2" />
								<asp:ListItem Value="3" Text="3" />
								<asp:ListItem Value="4" Text="4" />      
							</asp:DropDownList>
						</td>
						<td></td>
					</tr>
					<tr>
						<td>Enabled</td>
						<td><asp:CheckBox ID="ckEnabled" runat="server" /></td>
					</tr>
					<tr>
						<td>Intervals</td>
						<td>
							Que <asp:TextBox ID="txtIntervalQue" runat="server" Columns="5">0</asp:TextBox>
							Chew <asp:TextBox ID="txtIntervalChew" runat="server"  Columns="5">0</asp:TextBox>
						</td>
						<td>0 or 1, for non-scheduled emails (pm's, comments) use que = 0 </td>
					</tr>
					<tr>
						<td>Query</td>
						<td><asp:TextBox ID="txtTypeQuery" runat="server" TextMode="multiLine" Columns="40"></asp:TextBox></td>
						<td>Stored proc for queuer</td>
					</tr>
					<tr>
						<td></td>
						<td>
							<asp:Button ID="btnSaveType" Text="Save" OnClick="btnSaveType_Click" runat="server" /> 
							<asp:Button ID="btnCancelEdit" Text="Cancel" OnClick="btnCancelEdit_Click" runat="server" />
						</td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td><asp:Label ID="txtSaveStatus" runat="server"></asp:Label></td>
						<td></td>
					</tr>	
				</table>

			
			</td>
			<td>
				<!-- Help Text -->
			</td>
		</tr>
	</table>
</div>	

<div id="dvTypes" runat="server">
	<asp:Label ID="txtGVStatus" runat="server"></asp:Label>
	<asp:GridView ID="gvEmailTypes" runat="server"  
	OnRowDeleting="gvEmailTypes_Delete" OnRowEditing="gvEmailTypes_Edit" AutoGenerateColumns="false">

	<RowStyle BackColor="White"></RowStyle>
	<HeaderStyle BackColor="LightGray" Font-Underline="True" Font-Bold="True" 
	ForeColor="#000000" HorizontalAlign="Left"></HeaderStyle>
	<AlternatingRowStyle BackColor="Gainsboro"></AlternatingRowStyle>
	<FooterStyle BackColor="#FFffff"></FooterStyle>
	<Columns>
	<asp:TemplateField>
	<ItemTemplate>
	<asp:LinkButton id="lbnConfirmDelete" Runat="server" causesvalidation="false" OnClientClick="return confirm('Are you sure you want to delete this type?');" CommandName="Delete">Delete</asp:LinkButton>
	<asp:LinkButton id="lbnEdit" Runat="server" causesvalidation="false" CommandName="Edit">Edit</asp:LinkButton>
	</ItemTemplate>
	</asp:TemplateField>  
	<asp:BoundField DataField="email_type_id" HeaderText="Type Id" />    
	<asp:BoundField DataField="type_name" HeaderText="Name" />

	<asp:TemplateField HeaderText="Enabled">
	<ItemTemplate>						  	
	<%#Convert.ToInt32(DataBinder.Eval(Container.DataItem, "enabled")).Equals(1)%>				  
	</ItemTemplate>
	</asp:TemplateField>  				 

	<asp:BoundField DataField="date_created" HeaderText="Created" DataFormatString="{0:d}" />
	<asp:BoundField DataField="description" HeaderText="Description" />
	<asp:BoundField DataField="tier" HeaderText="Tier" />
	<asp:TemplateField>
	<ItemTemplate>
	<a href="templates.aspx?type=<%#DataBinder.Eval(Container.DataItem, "email_type_id")%>">Templates</a>						  				  
	</ItemTemplate>
	</asp:TemplateField>  
	</Columns> 
	</asp:GridView>   
</div> 


</form>
</body>
</html>
