<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Mailer.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.Administration.Mailer.Mailer" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
  <title>Mailer</title>
    
  <style type="text/css" media="screen">@import url( AdminMailer.css );</style>
        
</head>
<body>
    <form id="form1" runat="server">
    <div id="dvMailerNav" runat="server">
      <a href="Mailer.aspx">Mailer</a>
    </div>
    <div>
      <p>Warning, take extreme caution when using this tool.</p>
      <ul>
        <li><a href="MailTypes.aspx">Types</a></li>
        <li><a href="MailerLog.aspx">Mailer Log</a></li>
        <li><a href="EmailTesting.aspx">Email Testing</a></li>
      </ul>
    </div>
    </form>
</body>
</html>
