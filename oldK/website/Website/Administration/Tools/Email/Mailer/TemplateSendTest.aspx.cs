using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva.Mailer;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.Administration.Mailer
{
	public partial class TemplateSendTest : BasePage
	{
		/* This page uses function calls that 
		* could potentially become out of date 
		* or have data that is incorrect in 
		* other environments.
		*/		

		#region default values
		// To User
		public int toUserId;
		public string toEmail;
		public string toName;

		// From User
		public int fromUserId;

		public string additionalmessage;
		public string strSubject, messageText;
		public int messageId;
		string commentType;

		// For community
		public int communityId;
		public string communityName;

		public int inviteID;
		public string keyCode;
		#endregion

		
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				dvSendTestForm.Visible = false;

				// To User
				toUserId = 11881; // mullins
				toEmail = "cmullins@kaneva.com";
				toName = "Chris Mullins";

				// From User
				fromUserId = 11901; // mchristopher 

				additionalmessage = "This is the additional Message";
				strSubject = "";
				messageText = "";
				messageId = 1;
				commentType = "Profile";

				// For community
				communityId = 19441;
				communityName = "cm test aa";

				inviteID = 1231;
				keyCode = "fiGPGrBf7dgwQy6JF4Qj";

				setValues();
			}
			else
			{
				getFormValues();
			}
		}


		protected void setValues()
		{
			txtToUserId.Text = toUserId.ToString();
			txtToEmail.Text = toEmail.ToString();
			txtToName.Text = toName.ToString();
			txtFromUserId.Text = fromUserId.ToString();
			txtAdditionalMessage.Text = additionalmessage.ToString();
			txtStrSubject.Text = strSubject.ToString();
			txtMessageId.Text = messageId.ToString();
			txtCommentType.Text = commentType.ToString();
			txtCommunityId.Text = communityId.ToString();
			txtCommunityName.Text = communityName.ToString();
			txtInviteId.Text = inviteID.ToString();
			txtKeyCode.Text = keyCode.ToString();

			UserFacade userFacade = new UserFacade();
			User userFrom = userFacade.GetUser(fromUserId);

			lblFromUser.Text = userFrom.Email;
			lblToUser.Text = toEmail;
		}

		protected void getFormValues()
		{
			toUserId = Convert.ToInt32(txtToUserId.Text);
			toEmail = txtToEmail.Text;
			toName = txtToName.Text;
			fromUserId = Convert.ToInt32(txtFromUserId.Text);
			additionalmessage = txtAdditionalMessage.Text;
			strSubject = txtStrSubject.Text;
			messageId = Convert.ToInt32(txtMessageId.Text);
			commentType = txtCommentType.Text;
			communityId = Convert.ToInt32(txtCommunityId.Text);
			communityName = txtCommunityName.Text;
			inviteID = Convert.ToInt32(txtInviteId.Text);
			keyCode = txtKeyCode.Text;
		}

		protected void showFieldsToggle_Click(object sender, EventArgs e)
		{
			if (dvSendTestForm.Visible)
			{
				dvSendTestForm.Visible = false;
				lbShowFields.Text = "Show Fields";
			}
			else
			{
				dvSendTestForm.Visible = true;
				lbShowFields.Text = "Hide Fields";
			}
		}

		protected void btnUpdate_Click(object sender, EventArgs e)
		{
			getFormValues();
			setValues();
		}

		protected void btnCancel_Click(object sender, EventArgs e)
		{
			Response.Redirect("TemplateSendTest.aspx");
		}

		#region Email Send Functions
		protected void btnSendTest_Click(object sender, EventArgs e)
		{
		try
		{
		int typeId = EmailTypeUtility.GetTypeIdByName("FieldTest");
		TemplateSherpa.DressAndSendTemplate(typeId, 0, toEmail, inviteID, fromUserId, toUserId, keyCode, 0, additionalmessage, strSubject, 0);
		UpdateStatus("FieldTest: Sent");
		}
		catch (Exception ex)
		{
		UpdateStatus("Error SendTest: " + ex.ToString());
		}
		}

		protected void btnSendInvitation_Click(object sender, EventArgs e)
		{      
		try
		{                                    
		System.Web.UI.Page pageHolder = new System.Web.UI.Page();

		//SendInvitation(int userId, string toEmail, string toName, string additionalmessage, int inviteID, string keyCode, System.Web.UI.Page page)
		MailUtilityWeb.SendInvitation(fromUserId, toEmail, toName, additionalmessage, inviteID, keyCode, pageHolder);
		UpdateStatus("SendInvitation: Sent");
		}
		catch (Exception ex)
		{
		UpdateStatus("Error SendInvitation: " + ex.ToString());
		}
		}

		protected void btnSendComment_Click(object sender, EventArgs e)
		{      
			try
			{                
				UserFacade userFacade = new UserFacade();
				User userTo = userFacade.GetUser(fromUserId);

				// SendCommentEmail(string toEmail, User userTo, int fromUserId, string commentType)
				MailUtilityWeb.SendCommentEmail(toEmail, userTo, fromUserId, commentType);
				UpdateStatus("SendCommentEmail: Sent" + " Comment Type: " + commentType);
			}
			catch (Exception ex)
			{
				UpdateStatus("Error SendCommentEmail: " + ex.ToString());
			}
		}

		protected void btnSendPrivateMessageNotificationEmail_Click(object sender, EventArgs e)
		{
		try
		{        
		// SendPrivateMessageNotificationEmail(string toEmail, string strSubject, int messageId, int toUserId, int fromUserId)
		MailUtilityWeb.SendPrivateMessageNotificationEmail(toEmail, strSubject, messageId, messageText, toUserId, fromUserId);
		UpdateStatus("SendPrivateMessageNotificationEmail: Sent");
		}
		catch (Exception ex)
		{
		UpdateStatus("Error SendPrivateMessageNotificationEmail: " + ex.ToString());
		}
		}

		// Mega Rave
		protected void btnSendMegaRaveNotification_Click(object sender, EventArgs e)
		{
		try
		{
		UserFacade userFacade = new UserFacade();
		User userTo = userFacade.GetUser(fromUserId);

		// SendMegaRaveNotification(string toEmail, User userTo, int fromUserId)
		MailUtilityWeb.SendMegaRaveNotification(toEmail, userTo, fromUserId);
		UpdateStatus("SendMegaRaveNotification: Sent");
		}
		catch (Exception ex)
		{
		UpdateStatus("Error SendMegaRaveNotification: " + ex.ToString());
		}
		}

		// friend-request
		protected void btnSendFriendRequestEmail_Click(object sender, EventArgs e)
		{
		try
		{
		// SendFriendRequestEmail(int userId, int friendId)
		MailUtilityWeb.SendFriendRequestEmail(fromUserId, toUserId);
		UpdateStatus("SendFriendRequestEmail: Sent");
		}
		catch (Exception ex)
		{
		UpdateStatus("Error btnSendFriendRequestEmail: " + ex.ToString());
		}
		}

		// invite-accepted
		protected void btnSendFriendAcceptedInvite_Click(object sender, EventArgs e)
		{
		try
		{
		// SendFriendAcceptedInviteNotification(int userToId, int userIdNewMember)
		MailUtilityWeb.SendFriendAcceptedInviteNotification(fromUserId, toUserId);
		UpdateStatus("SendFriendAcceptedInvite: Sent");
		}
		catch (Exception ex)
		{
		UpdateStatus("Error btnSendFriendAcceptedInvite: " + ex.ToString());
		}
		}

		// invite-community
		protected void btnSendCommunityInvite_Click(object sender, EventArgs e)
		{
		try
		{
		// SendCommunityInvitation(int userId, int communityId, string communityName, string firstName, string lastName, 
		// string toEmail, string toName, string additionalmessage, int inviteID, string keyCode)
		MailUtilityWeb.SendCommunityInvitation(fromUserId, communityId, communityName, "",
		"", toEmail, toName, additionalmessage, inviteID, keyCode);
		UpdateStatus("SendCommunityInvite: Sent");
		}
		catch (Exception ex)
		{
		UpdateStatus("Error btnSendCommunityInvite: " + ex.ToString());
		}
		}

		// joins-community
		// Notification that a user has joined a community you own.
		protected void btnSendCommunityJoinNotification_Click(object sender, EventArgs e)
		{
		try
		{
		MailUtilityWeb.SendCommunityJoinNotification(toEmail, fromUserId, toUserId, communityName, communityId);
		UpdateStatus("SendCommunityJoinNotification: Sent");
		}
		catch (Exception ex)
		{
		UpdateStatus("Error SendCommunityJoinNotification: " + ex.ToString());
		}
		}

		// community-join-request
		// a request to join a community you own.
		protected void btnSendCommunityRequest_Click(object sender, EventArgs e)
		{
		try
		{

		MailUtilityWeb.SendCommunityRequest(toEmail, fromUserId, toUserId, communityName, communityId);
		UpdateStatus("SendCommunityRequest: Sent");
		}
		catch (Exception ex)
		{
		UpdateStatus("Error SendCommunityRequest: " + ex.ToString());
		}
		}

		// community-join-request-accepted
		// when your request to join a community has been accepted
		protected void btnSendCommunityAccepted_Click(object sender, EventArgs e)
		{
			try
			{
				MailUtilityWeb.SendCommunityAccepted(toEmail, fromUserId, toUserId, communityName, communityId);
				UpdateStatus("SendCommunityAccepted: Sent");
			}
			catch (Exception ex)
			{
				UpdateStatus("Error SendCommunityAccepted: " + ex.ToString());
			}

		}
		#endregion

		protected void UpdateStatus(string message)
		{
		lblStatus.Text = lblStatus.Text.Insert(0, DateTime.Now.ToString() + " " + message + "<br/>");
		}

	}
}
