<%@ Page Language="C#" validateRequest="false" AutoEventWireup="true" CodeBehind="TemplateSendTest.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.Administration.Mailer.TemplateSendTest" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Email Templates - Send Test</title>    
    <style type="text/css" media="screen">@import url( AdminMailer.css );</style>
</head>
<body>
    <form id="form1" runat="server">
    
    <!-- Main Nav -->
    <div id="dvMailerNav" runat="server">
      <a href="Mailer.aspx">Mailer</a> > 
      <a href="MailTypes.aspx">Email Types</a> >
      <a href="Templates.aspx">Templates</a> 
    </div>


    <div id="dvContent">    
      <div id="dvRside">
          <asp:Label ID="lblStatus" runat="server"></asp:Label>      
      </div>
      <div id="dvLside">
      
      You are sending TO <asp:Label ID="lblToUser" runat="server"></asp:Label>
      FROM <asp:Label ID="lblFromUser" runat="server"></asp:Label>. 
      (<asp:LinkButton id="lbShowFields" runat="server" Text="Show Fields" OnClick="showFieldsToggle_Click"></asp:LinkButton>)
      
      <br />
      <br />
      
      <div id="dvSendTestForm" runat="server" style="border:solid 1px grey; width:800px; background-color: #cccccc;">
		 <table>
			<tr>
				<td>toUserId</td>
				<td><asp:TextBox ID="txtToUserId" runat="server" Columns="50"></asp:TextBox></td>
				<td>Recipient's User Id (kaneva.users)</td>
			</tr>
			<tr>
				<td>toEmail</td>
				<td><asp:TextBox ID="txtToEmail" runat="server" Columns="50"></asp:TextBox></td>
				<td>Recipient's Email Address</td>
			</tr>
			<tr>
				<td>toName</td>
				<td><asp:TextBox ID="txtToName" runat="server" Columns="50"></asp:TextBox></td>
				<td>Recipients Name</td>
			</tr>
			<tr>
				<td>fromUserId</td>
				<td><asp:TextBox ID="txtFromUserId" runat="server" Columns="50"></asp:TextBox></td>
				<td>Sender's User Id</td>
			</tr>
			<tr>
				<td>additionalmessage</td>
				<td><asp:TextBox ID="txtAdditionalMessage" runat="server" Columns="50"></asp:TextBox></td>
				<td>Some emails have a field which allows a custom message. Other email templates may use this field for dynamic content.</td>
				
			</tr>
			<tr>
				<td>strSubject</td>
				<td><asp:TextBox ID="txtStrSubject" runat="server" Columns="50"></asp:TextBox></td>
				<td>Some emails may have additional content in the subject.</td>
			</tr>
			<tr>
				<td>messageId</td>
				<td><asp:TextBox ID="txtMessageId" runat="server" Columns="50"></asp:TextBox></td>
				<td>If the email is a notification about a message this is the id of the messge.</td>
			</tr>
			<tr>
				<td>commentType</td>
				<td><asp:TextBox ID="txtCommentType" runat="server" Columns="50"></asp:TextBox></td>
				<td>This is the message type</td>
			</tr>
			<tr>
				<td>communityId</td>
				<td><asp:TextBox ID="txtCommunityId" runat="server" Columns="50"></asp:TextBox></td>
				<td>If the email is regarding a community this holds the community Id.</td>
			</tr>
			<tr>
				<td>communityName</td>
				<td><asp:TextBox ID="txtCommunityName" runat="server" Columns="50"></asp:TextBox></td>
				<td>If the email is regarding a community this holds the community name.</td>
			</tr>
			<tr>
				<td>inviteID</td>
				<td><asp:TextBox ID="txtInviteId" runat="server" Columns="50"></asp:TextBox></td>
				<td>For invite emails this is the invite id (kaneva.invites)</td>
			</tr>
			<tr>
				<td>keyCode</td>
				<td><asp:TextBox ID="txtKeyCode" runat="server" Columns="50"></asp:TextBox></td>
				<td>Invites have a keycode that is used in links.</td>
			</tr>
			<tr>
				<td></td>
				<td>
					<asp:button ID="btnUpdate" runat="server" Text="Update" OnClick="btnUpdate_Click" /> 
					<asp:button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
				</td>				
			</tr>
	      
		 </table>
      </div>

      <table>
      
        <tr>
          <td><asp:Button ID="Button7" Text="Send" runat="server" OnClick="btnSendTest_Click" /></td>
          <td>btnSendTest</td>
        </tr>
      
        <tr>
          <td><asp:Button ID="Button1" Text="Send" runat="server" OnClick="btnSendInvitation_Click" /></td>
          <td>SendInvitation</td>
        </tr>
        
        <tr>
          <td><asp:Button ID="Button2" Text="Send" runat="server" OnClick="btnSendComment_Click" /></td>
          <td>SendCommentEmail</td>
        </tr>
      
        <tr>
          <td><asp:Button ID="Button3" Text="Send" runat="server" OnClick="btnSendPrivateMessageNotificationEmail_Click" /></td>
          <td>SendPrivateMessageNotificationEmail</td>
        </tr>

        <tr>
          <td><asp:Button ID="Button4" Text="Send" runat="server" OnClick="btnSendMegaRaveNotification_Click" /></td>
          <td>SendMegaRaveNotification</td>
        </tr>
        
        <tr>
          <td><asp:Button ID="Button5" Text="Send" runat="server" OnClick="btnSendFriendRequestEmail_Click" /></td>
          <td>SendFriendRequestEmail</td>
        </tr>
        
        <tr>
          <td><asp:Button ID="Button6" Text="Send" runat="server" OnClick="btnSendFriendAcceptedInvite_Click" /></td>
          <td>SendFriendAcceptedInvite</td>
        </tr>
                        
        <tr>
          <td><asp:Button ID="Button8" Text="Send" runat="server" OnClick="btnSendCommunityInvite_Click" /></td>
          <td>SendCommunityInvite</td>
        </tr>
        
        <tr>
          <td><asp:Button ID="Button9" Text="Send" runat="server" OnClick="btnSendCommunityJoinNotification_Click" /></td>
          <td>SendCommunityJoinNotification</td>
        </tr>
        
        <tr>
          <td><asp:Button ID="Button10" Text="Send" runat="server" OnClick="btnSendCommunityRequest_Click" /></td>
          <td>SendCommunityRequest</td>
        </tr>
        
        <tr>
          <td><asp:Button ID="Button11" Text="Send" runat="server" OnClick="btnSendCommunityAccepted_Click" /></td>
          <td>SendCommunityAccepted</td>
        </tr>
        
        <!--
        <tr>
          <td><asp:Button ID="Button12" Text="Send" runat="server" OnClick="btnSendPrivateMessageNotificationEmail_Click" /></td>
          <td>btnSendPrivateMessageNotificationEmail</td>
        </tr>
        
        <tr>
          <td><asp:Button ID="Button13" Text="Send" runat="server" OnClick="btnSendPrivateMessageNotificationEmail_Click" /></td>
          <td>btnSendPrivateMessageNotificationEmail</td>
        </tr>
        -->
      </table>
      
      </div>   
    </div>    
    
    </form>
</body>
</html>