using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva.Mailer;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva.Administration.Mailer
{
  public partial class MailerLog : BasePage
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      // Redirect if user is not admin
      if (!IsCSR())
      {
        Response.Redirect("~/default.aspx");
      }

      if (!IsPostBack)
      {

        BindData();
      }
    }

    protected void BindData()
    {
      string sqlSelect = "SELECT * FROM kes_log ORDER BY kes_log_id DESC LIMIT 40";
      
      DataTable dt = KanevaGlobals.GetDatabaseUtilityEmailViral().GetDataTable(sqlSelect);
      gvLog.DataSource = dt;
      gvLog.DataBind();

    }
  }
}
