<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EmailTesting.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.Administration.Mailer.EmailTesting" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Email Testing</title>
    <style type="text/css" media="screen">@import url( AdminMailer.css );</style>  
</head>
<body>
    <form id="form1" runat="server">
    
    <div id="dvMailerNav" runat="server">
      <a href="Mailer.aspx">Mailer</a> > 
      <a href="EmailTesting.aspx">Email Testing</a>
    </div>
    
    <div id="dvLsideNav">
    <h3>Tests</h3>
      <ul>
        <li><asp:LinkButton Text="Que Email" runat="server" CommandArgument="dvQueEmail" CommandName="Click" OnCommand="ShowDiv_Command"/></li>
        <li><asp:LinkButton Text="Run Chewer" runat="server" CommandArgument="dvRunChewer" CommandName="Click" OnCommand="ShowDiv_Command"/></li>
        <li><asp:LinkButton Text="Parse List" runat="server" CommandArgument="dvParseList" CommandName="Click" OnCommand="ShowDiv_Command"/></li>
        <li><asp:LinkButton Text="Send Email" runat="server" CommandArgument="dvSendTemplate" CommandName="Click" OnCommand="ShowDiv_Command"/></li>        
        <li><asp:LinkButton Text="Link Encrypt" runat="server" CommandArgument="dvLinkEncrypt" CommandName="Click" OnCommand="ShowDiv_Command"/></li>
        <li><asp:LinkButton Text="Weighted Random" runat="server" CommandArgument="dvWeightedRandom" CommandName="Click" OnCommand="ShowDiv_Command"/></li>
        <li><asp:LinkButton Text="Invite Info" runat="server" CommandArgument="dvUserInviteInfo" CommandName="Click" OnCommand="ShowDiv_Command"/></li>
      </ul>
    <h3>Tools</h3>
      <ul>
        <li><asp:LinkButton Text="Purge Que" runat="server" CommandArgument="dvPurgeQueue" CommandName="Click" OnCommand="ShowDiv_Command" /></li>     
        <li><asp:LinkButton Text="Purge Log" runat="server" CommandArgument="dvPurgeLog" CommandName="Click" OnCommand="ShowDiv_Command"/></li>
      </ul>  
    </div>
    
     <div id="dvRside">Status:<br />
      <asp:label ID="lblStatus" runat="server"></asp:label>    
    </div> 
             
    <div id="dvContent" >
      
      <div id="dvIntro" runat="server">
        <h3>Tests and Tools</h3>
      </div>      
      
       <div id="dvQueEmail" runat="server">
        <h3>Que Email</h3>
        <p>Queuer.QueueEmail(typeId, email_template_id, userId, toEmail, fromEmail, inviteID);</p>
      
        <table>
        <tr><td>typeId</td><td><asp:TextBox ID="txtTypeIdQE" runat="server"></asp:TextBox></td></tr>
        <tr><td>email_template_id</td><td><asp:TextBox ID="txtEmailTemplateIdQE" runat="server"></asp:TextBox></td></tr>
        <tr><td>userId</td><td><asp:TextBox ID="txtUserIdQE" runat="server"></asp:TextBox></td></tr>
        <tr><td>toEmail</td><td><asp:TextBox ID="txtToEmailQE" runat="server"></asp:TextBox></td></tr>
        <tr><td>fromEmail</td><td><asp:TextBox ID="txtFromEmailQE" runat="server"></asp:TextBox></td></tr>
        <tr><td>inviteID</td><td><asp:TextBox ID="txtInviteIdQE" runat="server"></asp:TextBox></td></tr>
        </table>
        <asp:Button ID="btnQueEmail" Text="QueueEmail" OnClick="btnQueEmail_click" runat="server" /> -> 
        <asp:LinkButton Text="Run Chewer" runat="server" CommandArgument="dvRunChewer" CommandName="Click" OnCommand="ShowDiv_Command"/>
      </div>   
      
                          
      <div id="dvRunChewer" runat="server">
        <h3>Run Chewer</h3>
        <p>Chewer.Chew()</p>
      
        <table>
        <tr><td></td><td></td></tr>
        <tr><td></td><td></td></tr>
        </table>
        <asp:Button ID="btnChew" Text="Chew" OnClick="btnChew_click" runat="server" />
      </div>             
                              
      <div id="dvParseList" runat="server">
        <h3>Parse List</h3>
        <p>DressAndSendTemplate(DataTable dtMailingList)</p>
       
        <table>        
        <tr><td>email_queue_id</td><td><asp:TextBox ID="txtEmailQueueId" runat="server">0</asp:TextBox></td></tr>
        <tr><td>email_type_id</td><td><asp:TextBox ID="txtEmailTypeId" runat="server">1</asp:TextBox></td></tr>
        <tr><td>email_template_id</td><td><asp:TextBox ID="txtEmailTemplateId" runat="server">10</asp:TextBox></td></tr>
        <tr><td>email_to</td><td><asp:TextBox ID="txtEmailToTS" runat="server">cmullins@kaneva.com</asp:TextBox></td></tr>
        <tr><td>invite_id</td><td><asp:TextBox ID="txtInviteId" runat="server">0</asp:TextBox></td></tr>
        <tr><td>Number to send</td><td><asp:TextBox ID="txtIterationsPL" runat="server">1</asp:TextBox></td></tr>
        </table>
        <asp:Button ID="btnParseList" Text="DressAndSendTemplate" OnClick="btnParseList_click" runat="server" /> -> LogEmailSend -> SendEmail
      </div>
                              
      <div id="dvSendTemplate" runat="server">  
        <h3>Send Email</h3>                    
        <p>MailUtility.SendEmail(emailFrom, emailTo, subject, body, ishtml, cc, tier);</p>
        
        <table>        
        <tr><td>emailFrom</td><td><asp:TextBox ID="txtEmailFrom" runat="server">kaneva@kaneva.com</asp:TextBox></td></tr> 
        <tr><td>emailTo</td><td><asp:TextBox ID="txtEmailTo" runat="server">cmullins@kaneva.com</asp:TextBox></td></tr>
        <tr><td>subject</td><td><asp:TextBox ID="txtSubject" runat="server">subject</asp:TextBox></td></tr>
        <tr><td>body</td><td><asp:TextBox ID="txtBody" runat="server">body</asp:TextBox></td></tr>
        <tr><td>ishtml</td><td><asp:TextBox ID="txtIsHtml" runat="server">true</asp:TextBox></td></tr>
        <tr><td>cc</td><td><asp:TextBox ID="txtCC" runat="server">false</asp:TextBox></td></tr>
        <tr><td>tier</td><td><asp:TextBox ID="txtTier" runat="server">4</asp:TextBox></td></tr>
        </table>                     
        <asp:Button ID="btnSend" Text="SendEmail" OnClick="btnSend_Click" runat="server" />                               
      </div>

      <div id="dvWeightedRandom" runat="server">
        <h3>Weighted Random</h3>
        <p></p>
      
        <table>
        <tr><td>Iterations</td><td><asp:TextBox ID="txtIterationsWR" runat="server"></asp:TextBox></td></tr>    
        <tr><td>Email Type</td><td><asp:TextBox ID="txtEmailTypeIdWR" runat="server">1</asp:TextBox></td></tr>    
        <tr><td></td><td></td></tr>    
        </table>
        <asp:Button ID="btnWeightedRandom" Text="Run Test" OnClick="btnWeightedRandom_Click" runat="server" />
        
        <asp:Literal ID="litWeightedResult" runat="server"></asp:Literal>
        
      </div>
      
      <div id="dvLinkEncrypt" runat="server">
        <h3>Link Encrypt</h3>
        <p>xxxxx(xxx,xxx)</p>
      
        <table>
        <tr><td>Link</td><td><asp:TextBox ID="txtLink" Columns="120" runat="server"></asp:TextBox></td></tr>        
        <tr><td>Encoded</td><td><asp:TextBox ID="txtEncoded" Columns="120" runat="server"></asp:TextBox></td></tr>
        </table>
        <asp:Button ID="btnEncryptLink" Text="Encrypt Link" OnClick="btnEncryptLink_Click" runat="server" />
        <asp:Button ID="btnDecryptLink" Text="Decrypt Link" OnClick="btnDecryptLink_Click" runat="server" />
        <br />
        <asp:Literal ID="litEncryptLinkResults" runat="server"></asp:Literal>
        
      </div>
         
      <div id="dvPurgeLog" runat="server">
        <h3>Purge Log</h3>
        <p>DeleteEmailSendLog(DateTime dateSent)</p>
      
        <table>
        <tr><td>Date Sent</td><td><asp:TextBox ID="txtDateSentPL" runat="server"></asp:TextBox></td></tr>        
        </table>
        <asp:Button ID="btnPurgeSendLog" Text="DeleteEmailSendLog" OnClick="btnPurgeSendLog_Click" runat="server" />
      </div>
      
      
      <div id="dvPurgeQueue" runat="server">
        <h3>Purge Queue</h3>
        <p>DeleteQueue(DateTime dateQueued)</p>
      
        <table>
        <tr><td>Date Queued</td><td><asp:TextBox ID="txtDateQueuedPQ" runat="server"></asp:TextBox></td></tr>        
        </table>
        <asp:Button ID="btnPurgeQueue" Text="DeleteQueue" OnClick="btnPurgeQueue_Click" runat="server" />
      </div>
      

      <div id="dvUserInviteInfo" runat="server">
        <h3>Invite Info</h3>
        <p>UsersUtility.GetInvite(inviteId) -> userFacade.GetUser(Convert.ToInt32(drIvite["user_id"])</p>
      
        <table>
        <tr><td>Invite Id</td><td><asp:TextBox ID="txtInviteIdUII" runat="server"></asp:TextBox></td></tr>        
        </table>
        <asp:Button ID="btnUserInviteInfo" Text="Get User Info" OnClick="btnUserInviteInfo_Click" runat="server" />
        <br />
        <asp:Literal ID="litUserInfo" runat="server"></asp:Literal>
      </div>
      
            
      
      
      <!-- Not finished with these tests yet. 
                  
      <div id="Div2">
        <h3>Process Type</h3>
        <p>InviteReminderChewByType(int limit, string typeName)</p>
      
        <table>
        <tr><td></td><td></td></tr>        
        </table>
      
      </div>
            
      <div id="Div1">
        <h3>Get List</h3>
        <p>Chew calls template parser and feeds it a list</p>
        <p>TemplateSherpa.DressAndSendTemplate(GetQueueInviteReminder(limit, typeName));</p>
      
        <table>
        <tr><td></td><td></td></tr>        
        </table>
      
      </div> 
      
      -->
      
      
         
    </div>

    </form>
</body>
</html>