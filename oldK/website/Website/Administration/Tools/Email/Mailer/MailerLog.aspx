<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MailerLog.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.Administration.Mailer.MailerLog" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Mailer Log</title>
    <style type="text/css" media="screen">@import url( AdminMailer.css );</style>  
    <meta http-equiv="refresh" content="5" />
</head>
<body>
    <form id="form1" runat="server">
    
    <div id="dvMailerNav" runat="server">
      <a href="Mailer.aspx">Mailer</a> > 
      <a href="MailerLog.aspx">Mailer Log</a>
    </div>
    
    
    <div id="dvLog">        
      <asp:GridView ID="gvLog" runat="server" AutoGenerateColumns="true">        
        <RowStyle BackColor="White"></RowStyle>
			  <HeaderStyle BackColor="LightGray" Font-Underline="True" Font-Bold="True" 
			    ForeColor="#000000" HorizontalAlign="Left"></HeaderStyle>
			  <AlternatingRowStyle BackColor="Gainsboro"></AlternatingRowStyle>
			  <FooterStyle BackColor="#FFffff"></FooterStyle>
        <Columns></Columns> 
      </asp:GridView>      
    </div>

    </form>
</body>
</html>
