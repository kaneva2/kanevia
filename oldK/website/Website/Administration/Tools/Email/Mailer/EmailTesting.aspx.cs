using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva.Mailer;


using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva.Administration.Mailer
{
  public partial class EmailTesting : BasePage
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      // Redirect if user is not admin
      if (!IsCSR())
      {
        Response.Redirect("~/default.aspx");
      }
      
      if (!IsPostBack)
      {
        BindData();
      }
    }

    protected void BindData()
    {
      ShowDiv("dvIntro");
      
      //BindSendTypes();
      txtDateSentPL.Text = txtDateQueuedPQ.Text = DateTime.Now.ToString();
    }

    protected void btnChew_click(object sender, EventArgs e)
    {
      Chewer.Chew();
      UpdateStatus("Run Chewer");
    }

    protected void btnQueEmail_click(object sender, EventArgs e)
    {
      try
      {
        int typeId = Convert.ToInt16(txtTypeIdQE.Text);
        int emailTemplateId = Convert.ToInt16(txtEmailTemplateIdQE.Text);
        int userId = Convert.ToInt16(txtUserIdQE.Text);
        int inviteId = Convert.ToInt16(txtInviteId.Text);

        Queuer.QueueEmail(typeId, emailTemplateId, userId, txtToEmailQE.Text, txtFromEmailQE.Text, inviteId);
        UpdateStatus("Que email_template_id:" + emailTemplateId);
      }
      catch
      {
        UpdateStatus("Failure: Que email_template_id");
      }
    }

    protected void btnParseList_click(object sender, EventArgs e)
    {
      DataTable dt = MailingLists.GetEmailList("SELECT " + 
        Convert.ToInt32(txtEmailQueueId.Text) + " as email_queue_id," + 
        Convert.ToInt32(txtEmailTypeId.Text) + " as email_type_id," +
        Convert.ToInt32(txtEmailTemplateId.Text) + " as email_template_id, " + 
        "'" + txtEmailToTS.Text + "' as email_to , " + 
        Convert.ToInt32(txtInviteId.Text) +  " as invite_id");

      int numIterations = Convert.ToInt16(txtIterationsPL.Text);
      
      int result = 0;

      int i = 0;
      for (i = 0; i < numIterations; i++)
      {                
        result = TemplateSherpa.DressAndSendTemplate(dt);
        if (result == -1)
        {
          break;
        }        
      } 
      UpdateStatus("DressAndSendTemplate " + txtEmailTemplateId.Text + ". Result: " + result.ToString());
    }

    protected void btnSend_Click(object sender, EventArgs e)
    {
      MailUtility.SendEmail(txtEmailFrom.Text, txtEmailTo.Text, txtSubject.Text, txtBody.Text, Convert.ToBoolean(txtIsHtml.Text),
        Convert.ToBoolean(txtCC.Text), Convert.ToInt16(txtTier.Text));

      UpdateStatus("Email Sent.");
    }

    protected void btnPurgeSendLog_Click(object sender, EventArgs e)
    {
      DateTime dateSent = Convert.ToDateTime(txtDateSentPL.Text);
      int results = Logger.DeleteEmailSendLog(dateSent);

      UpdateStatus("Deleted " + results  + " entries from email_send_log sent after  " + txtDateSentPL.Text);
    }

    protected void btnPurgeQueue_Click(object sender, EventArgs e)
    {
      DateTime dateQueued = Convert.ToDateTime(txtDateQueuedPQ.Text);
      int results = Logger.DeleteQueue(dateQueued);

      UpdateStatus("Deleted " + results + " entries from email_queue queued after  " + txtDateQueuedPQ.Text);
    }

    protected void btnEncryptLink_Click(object sender, EventArgs e)
    {
      StringBuilder sb = new StringBuilder();

      string strInput = txtLink.Text;

      sb.Append("<table>");

      sb.Append("<tr><td>Raw link</td><td>" + strInput + "</td></tr>");

      string newLocation = System.Web.HttpUtility.UrlEncode(KanevaGlobals.Encrypt(strInput));
      txtEncoded.Text = newLocation;

      sb.Append("<tr><td>Encrpyted link</td><td>" + newLocation + "</td></tr>");

      string redirectLink = KanevaGlobals.Decrypt(Server.UrlDecode(newLocation));

      sb.Append("<tr><td>Redirect link</td><td>" + redirectLink + "</td></tr>");

      sb.Append("</table>");
      litEncryptLinkResults.Text = sb.ToString();

      UpdateStatus("Encrypt Link");
    }

    protected void btnDecryptLink_Click(object sender, EventArgs e)
    {
      StringBuilder sb = new StringBuilder();

      string strInput = txtEncoded.Text;

      sb.Append("<table>");

      sb.Append("<tr><td>Raw link</td><td>" + strInput + "</td></tr>");

      string redirectLink = KanevaGlobals.Decrypt(Server.UrlDecode(strInput).ToString());


      sb.Append("<tr><td>Decrpyted link</td><td>" + redirectLink + "</td></tr>");

      string newLocation = System.Web.HttpUtility.UrlEncode(KanevaGlobals.Encrypt(redirectLink.ToString()));

      sb.Append("<tr><td>Redirect link</td><td>" + newLocation + "</td></tr>");

      sb.Append("</table>");
      litEncryptLinkResults.Text = sb.ToString();

      UpdateStatus("Decrypt Link");
    }

    protected void btnWeightedRandom_Click(object sender, EventArgs e)
    {

		int typeId = Convert.ToInt16(txtEmailTypeIdWR.Text);

		int templateToUse = EmailTemplateUtility.GetTemplateNextRandomId(typeId);

		lblStatus.Text = lblStatus.Text.Insert(0, "Random Template: " + templateToUse + "<br />");

		litWeightedResult.Text = templateToUse.ToString();

		/*
		StringBuilder sb = new StringBuilder();
		sb.Append("<hr /><h3>Results</h3>");

		int typeId = Convert.ToInt16(txtEmailTypeIdWR.Text);

		DataTable dt = EmailTemplateUtility.GetTemplatesByTypeId(typeId, 1);

		//get the total of weights
		int weightTotal = 0;
		for (int i = 0; i < dt.Rows.Count; i++)
		{
		weightTotal += Convert.ToInt16(dt.Rows[i]["rotation_weight"]);
		}

		sb.Append("Weight Total: " + weightTotal + "<br />");


		Random random = new Random();
		int num = random.Next(0, weightTotal);

		sb.Append("Random: " + num + "<br />");

		int templateToUse = 0;
		int thisWeight = 0;

		for (int i = 0; i < dt.Rows.Count; i++)
		{
		thisWeight += Convert.ToInt16(dt.Rows[i]["rotation_weight"]);
		if (thisWeight <= num)
		{
		  sb.Append("Template Weight: " + thisWeight + " < " + num + " Go Fish!" + "<br />");
		}
		else
		{
		  templateToUse = Convert.ToInt16(dt.Rows[i]["email_template_id"]);
		  break;
		}
		}

		sb.Append("Template: " + templateToUse + "<br />");

		lblStatus.Text = lblStatus.Text.Insert(0, "Random Template: " + templateToUse + "<br />");

		litWeightedResult.Text = sb.ToString();
		*/

      /*  Query to test distribution
       
         SELECT COUNT(email_id), l.email_template_id, rotation_weight, COUNT(email_id)/rotation_weight FROM email_send_log l
         INNER JOIN email_template t ON l.email_template_id = t.email_template_id
         WHERE date_sent > '2009-06-23' 
         GROUP BY email_template_id; 
       
       */
    }

    protected void btnUserInviteInfo_Click(object sender, EventArgs e)
    {
      StringBuilder sb = new StringBuilder();
      int inviteId = Convert.ToInt32(txtInviteIdUII.Text.ToString());

      DataRow drInvite = UsersUtility.GetInvite(inviteId);
      if (drInvite != null)
      {
        string keyCode = drInvite["key_value"].ToString();

        UserFacade userFacade = new UserFacade();
        User user = userFacade.GetUser(Convert.ToInt32(drInvite["user_id"]));

        sb.Append("#userfullname#" + user.FirstName + " " + user.LastName);
        sb.Append("<br />");
        sb.Append("#username#" + user.Username);
        sb.Append("<br />");
        sb.Append("#userlink#" + KanevaGlobals.GetPersonalChannelUrl(user.NameNoSpaces));
        sb.Append("<br />");
        sb.Append("#joinurl#" + KanevaGlobals.SiteName + KanevaGlobals.JoinLocation.Substring(1) + "?i=" + inviteId + "&k=" + keyCode + "&CMP=" + " omnitureCode " + "_Yes");
        sb.Append("<br />");
        sb.Append("#declineurl#" + KanevaGlobals.SiteName + "/declineInvite.aspx?i=" + inviteId + "&k=" + keyCode + "&CMP=" + " omnitureCode " + "_No");
        sb.Append("<br />");
        sb.Append("#userfirstname#" + user.FirstName);
        sb.Append("<br />");
        sb.Append("#additionalmessage#" + " template.UserMessage ");

        litUserInfo.Text = sb.ToString();
      }

    }



    // Load Data
    protected void BindSendTypes()
    {
      /*
      //get email types for edit dropdown
      DataTable dtTypes = EmailTypeUtility.GetEmailTypes();
      ddlTypes.DataSource = dtTypes;
      ddlTypes.DataBind();
      ddlTypes.Items.Insert(0, new ListItem("----", "0"));
      */
    }

    protected void ShowDiv_Command(object sender, CommandEventArgs e)
    {
      ShowDiv(e.CommandArgument.ToString());      
    }

    protected void ShowDiv(string divName)
    {
      try
      {
        dvParseList.Visible = false;
        dvSendTemplate.Visible = false;
        dvPurgeLog.Visible = false;
        dvIntro.Visible = false;
        dvPurgeQueue.Visible = false;
        dvWeightedRandom.Visible = false;
        dvRunChewer.Visible = false;
        dvQueEmail.Visible = false;
        dvLinkEncrypt.Visible = false;
        dvUserInviteInfo.Visible = false;

        FindControl(divName).Visible = true;
      }
      catch
      {
        UpdateStatus("ShowDiv Error: " + divName);
      }
    }

    protected void UpdateStatus(string message)
    {
      lblStatus.Text = lblStatus.Text.Insert(0, DateTime.Now.ToString() + " " + message + "<br/>");
    }
  }
}
