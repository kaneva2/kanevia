using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva.Mailer;


namespace KlausEnt.KEP.Kaneva.Administration.Mailer
{
  public partial class TemplatePreview : BasePage
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      // Redirect if user is not admin
      if (!IsCSR())
      {
        Response.Redirect("~/default.aspx");
      }
      
      int tmplId = Convert.ToInt16(Request["tmplId"]);
      int inviteId = 0; //Convert.ToInt16(Request["inviteId"]);
      string emailTo = "test@kaneva.com"; // Request["emailTo"].ToString();
      int emailId = 0;

      DataRow drTemplate = EmailTemplateUtility.GetTemplateById(tmplId);

      Template template = new Template();

      template.Footer = drTemplate["footer_html"].ToString();
      template.Header = drTemplate["header_html"].ToString();
	 

      string body = TemplateSherpa.ReplaceBot(template, drTemplate["template_html"].ToString(), inviteId, 0, 0, "", emailTo, emailId, "", 0, 0);

      Response.Write(body);
    }
  }
}
