<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EmailBlacklist.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.csr.EmailBlacklistManagement" %>
<%@ Register TagPrefix="Kaneva" TagName="CSRNav" Src="../../csr/csrNav.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="AdminMenu" Src="../../../usercontrols/AdministrativeMenu.ascx" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../../../usercontrols/Pager.ascx" %>

<script type="text/javascript" language="javascript" src="../../../jscript/prototype.js"></script>

<link href="../../../css/new.css" rel="stylesheet" type="text/css" />

<body>

<Kaneva:AdminMenu ID="adminmenu" runat="server" />
<Kaneva:CSRNav runat="server" id="csrNavigation" />

<br/>
<br/>
<br/>
<br/>

Blacklist is now populated automatically via back end tool and unsubscribe.


 <table id="tblRestrictions" runat="server" Visible="False" border="0" cellpadding="0" cellspacing="0" width="1000" style="background-color:#ffffff">
    <tr>
        <td align="center">
			    <span style="height:30px; font-size:28px; font-weight:bold">Blacklist Management</span><br />
			    <ajax:ajaxpanel id="ajpError" runat="server">
			    <asp:label runat="server" id="lblErrMessage" class="errBox black" 
			      style="width:60%;margin-top:20px;text-align:center;"></asp:label></ajax:ajaxpanel>
		    </td>
    </tr>
	  <tr>
		  <td style="height:20px"></td>
    </tr>
    <tr>
		<td align="center">
			<ajax:ajaxpanel id="ajBlacklist" runat="server">

				<table id="tblBlacklist" border="0" cellpadding="0" cellspacing="0" width="95%">
					<tr>
						 <td>
				            <div style="width:900px;text-align:left;margin:0 0 12px 0;">
								<asp:LinkButton id="lbNewPattern" runat="server" causesvalidation="false" onclick="lbNewPattern_Click">Add New Pattern</asp:LinkButton>
							</div>
							
							<div id="divPatternDetails" runat="server" style="display:none;width:100%;height:50px;" >
								<div style="float:left;display:block;">Pattern:<br /><asp:textbox id="txtPattern" width="200" runat="server"></asp:textbox></div>

								<div style="float:left;display:inline;margin-left:20px;text-align:left;">
								  Note:<br /><asp:textbox id="txtNote" width="200" runat="server"></asp:textbox></div>
								<div style="float:left;display:block;margin-left:20px;"><br /><asp:button id="btnSave" onclick="btnSave_Click" runat="server" text="Save" style="width:60px;" /><input type="button" value="Cancel" onclick="$('divPatternDetails').style.display='none';" /></div>
								<asp:textbox id="txtPatternId" runat="server" visible="false"></asp:textbox>
							</div>	
							
							<asp:gridview id="dgBlacklist" runat="server" 
								onsorting="dgBlacklist_Sorting" 
								OnRowDeleting="dgBlacklist_RowDeleting" 
								OnRowEditing="dgBlacklist_RowEditing"
								OnRowCreated="dgBlacklist_RowCreated"
								AllowSorting="True" border="0" cellpadding="4" 
								autogeneratecolumns="False" width="980px" 
								bordercolor="DarkGray" borderstyle="solid" borderwidth="1px" 
								PageSize="50" AllowPaging="False" >
								
								<RowStyle BackColor="White"></RowStyle>
								<HeaderStyle BackColor="LightGray" Font-Underline="True" Font-Bold="True" ForeColor="#000000" HorizontalAlign="Left"></HeaderStyle>
								<AlternatingRowStyle BackColor="Gainsboro"></AlternatingRowStyle>
								<FooterStyle BackColor="#FFffff"></FooterStyle>
								<Columns>
									<asp:BoundField HeaderText="Id" DataField="blacklist_id" ReadOnly="True" ConvertEmptyStringToNull="False" visible="True" itemstyle-width="30px" ></asp:BoundField>
									<asp:BoundField HeaderText="Pattern" DataField="pattern" SortExpression="pattern" ReadOnly="True" ConvertEmptyStringToNull="False" itemstyle-width="250px" ></asp:BoundField>
									<asp:BoundField HeaderText="Note Type" DataField="note" SortExpression="name" ReadOnly="True" ConvertEmptyStringToNull="False" itemstyle-width="200px"></asp:BoundField>
									<asp:BoundField HeaderText="Created Date" DataField="created_date" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
									<asp:BoundField HeaderText="" DataField="created_date" ReadOnly="True" ConvertEmptyStringToNull="False" ></asp:BoundField>
									<asp:CommandField CausesValidation="False" ShowEditButton="True" itemstyle-width="100px"></asp:CommandField>
									<asp:TemplateField>
										<ItemTemplate>
											<asp:LinkButton id="lbnConfirmDelete" Runat="server" causesvalidation="false" OnClientClick="return confirm('Are you sure you want to delete this pattern?');" CommandName="Delete">Delete</asp:LinkButton>
										</ItemTemplate>
									</asp:TemplateField>
				        
								</Columns>
							</asp:gridview>
							
							<div style="width:95%;text-align:right;margin:10px 6px 0 0;"><kaneva:pager runat="server" isajaxmode="True" onpagechanged="pgBlacklist_PageChange" id="pgBlacklist" maxpagestodisplay="5" shownextprevlabels="true" /></div>
							         
							<asp:Label id="lblNoBlacklist" visible="false" runat="server">
								<span style="font-size:16px;color:Red;font-weight:bold;margin:30px 0;width:95%;text-align:center;">Nothing found in Blacklist.</span>
							</asp:Label>
				
							<br /><br />
							
						</td>
					</tr>
					<tr>
						 <td align="center">
						 </td>
					</tr>
				</table>

			</ajax:ajaxpanel>
			<br />
		</td>
	</tr>
</table>


<br /><br />




</body>
