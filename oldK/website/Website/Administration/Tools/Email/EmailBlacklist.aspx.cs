using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using log4net;
using MagicAjax;

namespace KlausEnt.KEP.Kaneva.csr
{
    public partial class EmailBlacklistManagement : NoBorderPage
    {
        protected EmailBlacklistManagement () 
		    {
            Title = "CSR - Email Blacklist Managment";
        }
       
        protected void Page_Load (object sender, EventArgs e)
        {
            // Redirect if user is not admin
            if (!IsCSR ())
            {
                Response.Redirect ("~/default.aspx");
            }

            if (!IsPostBack)
            {
                //bind the blacklist
                BindBlacklistData(currentPage);
            }
        }


        #region Helper Methods

        /// <summary>
        /// BindBlacklistData
        /// </summary>
        private void BindBlacklistData (int currentPage)
        {
            //try
            //{
            //    lblErrMessage.Visible = false;
                
            //    // Set the sortable columns
            //    string orderby = CurrentSort + " " + CurrentSortOrder;
            //    int pageSize = 50;

            //    //query the database for existing promotions
            //    PagedDataTable pdt = MailUtility.GetEmailBlacklist("", orderby, currentPage, pageSize);

            //    if ((pdt != null) && (pdt.TotalCount > 0))
            //    {
            //        lblNoBlacklist.Visible = false;

            //        dgBlacklist.DataSource = pdt;
            //        dgBlacklist.DataBind();

            //        // Show Pager
            //        pgBlacklist.NumberOfPages = Math.Ceiling ((double) pdt.TotalCount / pageSize).ToString ();
            //        pgBlacklist.DrawControl ();
            //    }
            //    else
            //    {
            //        lblNoBlacklist.Visible = true;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    lblErrMessage.Text = "Error displaying Blacklist: " + ex.Message;
            //    lblErrMessage.Visible = true;
            //}
        }

        //allows user to switch the ordering between DESC and ASC
        //based on column clicked
        private void SortSwitch (string newSort)
        {
            if (CurrentSort == newSort)
            {
                if (CurrentSortOrder == "DESC")
                {
                    CurrentSortOrder = "ASC";
                }
                else
                {
                    CurrentSortOrder = "DESC";
                }
            }
            else
            {
                CurrentSort = newSort;
                CurrentSortOrder = "DESC";
            }
        }



        /// <summary>
        /// Clear the Restricted Data edit fields
        /// </summary>
        private void ClearPatternData ()
        {
            txtPattern.Text = "";            
            txtNote.Text = "";

            lblErrMessage.Visible = false;
        }

        /// <summary>
        /// Get the Restricted Word Data
        /// </summary>
        private void GetPatternDetails (GridViewRow gvr, int pageNumber)
        {
            //make the details panel visible
            AjaxCallHelper.Write ("$('divPatternDetails').style.display='block';");

            try
            {
                txtPattern.Text = gvr.Cells[1].Text;
                txtNote.Text = Server.HtmlDecode(gvr.Cells[2].Text).Trim();
                txtPatternId.Text = gvr.Cells[0].Text;

                lblErrMessage.Visible = false;
            }
            catch (Exception ex)
            {
                lblErrMessage.Text = "Error displaying pattern details: " + ex.Message;
                lblErrMessage.Visible = true;
            }
        }
        /*
        /// <summary>
        /// Used to clear the cache for the restriction type that was updated
        /// </summary>
        private void ClearCache (int restrictionType, string matchType)
        {
            // Clear the DataTable of restricted words
            WebCache.ClearCacheValue (WebCache.cRESTWORDS);
            
            // Based on the restriction type and match type, clear the correct word string from cache
            if (restrictionType == (int) Constants.eRESTRICTION_TYPE.POTTY_MOUTH)
            {
                if (matchType == Constants.eMATCH_TYPE.EXACT.ToString())
                {
                    WebCache.ClearCacheValue (WebCache.cPMEXACT);
                }
                else
                {
                    WebCache.ClearCacheValue (WebCache.cPMANY);
                }
            }
            else if (restrictionType == (int) Constants.eRESTRICTION_TYPE.RESERVED)
            {
                if (matchType == Constants.eMATCH_TYPE.EXACT.ToString())
                {
                    WebCache.ClearCacheValue (WebCache.cRESTEXACT);
                }
                else
                {
                    WebCache.ClearCacheValue (WebCache.cRESTANY);
                }
            }
        }   
        */

        #endregion Helper Methods


        #region Event Handlers

        /// <summary>
        /// Execute when the user selects a delete link from the data grid 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgBlacklist_RowDeleting (object source, GridViewDeleteEventArgs e)
        {
            //try
            //{
            //    // Get the selected row
            //    GridViewRow gvr = dgBlacklist.Rows[e.RowIndex];

            //    // Delete the record and rebind
            //    MailUtility.DeleteBlacklistPattern (Convert.ToInt32 (gvr.Cells[0].Text));
                
            //    // TO DO !!!!!!!!!!!!!
            //    // Clear the cache
            //    //ClearCache (Convert.ToInt32(gvr.Cells[3].Text), gvr.Cells[4].Text.ToUpper());

            //    pgBlacklist.CurrentPageNumber = currentPage;
            //    BindBlacklistData (currentPage);
            //}
            //catch (Exception ex)
            //{
            //    lblErrMessage.Text = "Error deleting pattern: " + ex.Message;
            //    lblErrMessage.Visible = true;
            //}
        }

        /// <summary>
        /// Execute when each row of the data grid is created
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgBlacklist_RowCreated (object sender, GridViewRowEventArgs e)
        {

            // The GridViewCommandEventArgs class does not contain a 
            // property that indicates which row's command button was
            // clicked. To identify which row's button was clicked, use 
            // the button's CommandArgument property by setting it to the 
            // row's index.
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // Set the visibility of the restriction_type_id field to false.  The reason we do this is
                // if the column is set to visible=false in the aspx code, the data is not bound to that
                // column.  So, we bind the data to the datagrid then set the visibility of this column to false
                // when each row of the grid is created.
                e.Row.Cells[3].Visible = false;
            }

        }

        /// <summary>
        /// Execute when the user selects a header link from the data grid to sort a by a column
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgBlacklist_Sorting (object source, GridViewSortEventArgs e)
        {
            SortSwitch (e.SortExpression); //sets the sort expression
            BindBlacklistData(currentPage);
        }

        /// <summary>
        /// Execute when the user selects a page change link from the Pager control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void pgBlacklist_PageChange (object sender, PageChangeEventArgs e)
        {
            BindBlacklistData(e.PageNumber);
        }

        /// <summary>
        /// Execute when the user selects Edit link from the grid view
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgBlacklist_RowEditing (object source, GridViewEditEventArgs e)
        {
            //passin the selected row and populate the fields
            GetPatternDetails(dgBlacklist.Rows[e.NewEditIndex], 1);
        }

        /// <summary>
        /// Execute when the user selects Save button to save changes to restricted word
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void btnSave_Click (object source, EventArgs e)
        {
            //try
            //{
            //    // Make sure word text box is not empty
            //    if (txtPattern.Text.Trim ().Length < 1)
            //    {
            //        lblErrMessage.Text = "You must enter a pattern to be blocked";
            //        lblErrMessage.Visible = false;
            //        return;
            //    }


            //    // If ID is -1, this is a new word
            //    if (Convert.ToInt32 (txtPatternId.Text) < 0)
            //    {
            //        MailUtility.InsertBlacklistPattern (txtPattern.Text.Trim (), txtNote.Text.Trim ());
            //    }
            //    else  // Updating existing word
            //    {
            //        MailUtility.UpdateBlacklistPattern (Convert.ToInt32 (txtPatternId.Text), txtPattern.Text.Trim (),
            //            txtNote.Text.Trim ());
            //    }

            //    //make the details panel hidden
            //    AjaxCallHelper.Write ("$('divPatternDetails').style.display='none';");

            //    currentPage = 1;
            //    pgBlacklist.CurrentPageNumber = currentPage;
            //    BindBlacklistData (currentPage);
            //}
            //catch (Exception ex)
            //{
            //    lblErrMessage.Text = "Error saving pattern : " + ex.Message;
            //    lblErrMessage.Visible = true;
            //}
        }

        /// <summary>
        /// Execute when the user selects Add New Word link 
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void lbNewPattern_Click (object sender, EventArgs e)
        {
            //clear the fields
            ClearPatternData ();

            //make the details panel visible
            AjaxCallHelper.Write ("$('divPatternDetails').style.display='block';");

            //set id to -1 <indicator that it is a new entry>
            txtPatternId.Text = "-1";
        }

        #endregion Event Handlers


        #region Properties

        /// <summary>
        /// Current Page Number
        /// </summary>
        /// <returns></returns>
        private int currentPage
        {
            get
            {
                if (ViewState["_currentPage"] == null)
                {
                    ViewState["_currentPage"] = 0;
                }
                return (int) ViewState["_currentPage"];
            }
            set
            {
                ViewState["_currentPage"] = value;
            }
        }

        /// <summary>
        /// DEFAULT_SORT
        /// </summary>
        /// <returns></returns>
        protected override string DEFAULT_SORT_ORDER
        {
            get
            {
                return "ASC";
            }
        }

        /// <summary>
        /// DEFAULT_SORT
        /// </summary>
        /// <returns></returns>
        protected override string DEFAULT_SORT
        {
            get
            {
                return " pattern ";
            }
        }
        
        #endregion Properties


        #region Declarations

        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

        #endregion Declarations
    }
}
