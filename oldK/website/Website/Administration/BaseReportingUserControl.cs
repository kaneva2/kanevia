///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Kaneva
{
    public class BaseReportingUserControl : System.Web.UI.UserControl
    {
        // Sorting
        protected Button btnSort;
        protected HtmlInputHidden hidSortColumn;
        private int l_securityLevel = 0;

        public BaseReportingUserControl(): base()
		{
		}

        /// <summary>
        /// Return the current user id
        /// </summary>
        /// <returns></returns>
        protected int GetUserId()
        {
            return KanevaWebGlobals.GetUserId();
        }

        protected int UsersAccessLevel()
        {
            return 1;
        }

        public int SecurityLevel
        {
            get
            {
                return l_securityLevel;
            }
            set
            {
                l_securityLevel = value;
            }
        }

        /// <summary>
        /// Get the results text
        /// </summary>
        /// <returns></returns>
        public string GetResultsText(int TotalCount, int pageNumber, int resultsPerPage, int currentPageCount)
        {
            return KanevaGlobals.GetResultsText(TotalCount, pageNumber, resultsPerPage, currentPageCount, true);
        }

        /// <summary>
        /// PassRequired
        /// </summary>
        public bool IsUserAuthorized()
        {
            bool authorized = false;

            //compare user access level to pages required access level
            switch (SecurityLevel)
            {
                case (int)Constants.eACCESS_TYPE.ACCESS_PASS:
                    authorized = true;
                    break;
                case (int)Constants.eACCESS_TYPE.GENERAL:
                    authorized = true;
                    break;
                default:
                    authorized = true;
                    break;
            }

            return authorized;
        }

        /// <summary>
        /// Current page of datagrid
        /// </summary>
        public int currentPage
        {
            get
            {
                if (ViewState["_currentPage"] == null)
                {
                    ViewState["_currentPage"] = DEFAULT_PAGE;
                }
                return (int)ViewState["_currentPage"];
            }
            set
            {
                ViewState["_currentPage"] = value;
            }
        }

        /// <summary>
        /// Current sort expression
        /// </summary>
        public string CurrentSort
        {
            get
            {
                if (ViewState["cs"] == null)
                {
                    return DEFAULT_SORT;
                }
                else
                {
                    return ViewState["cs"].ToString();
                }
            }
            set
            {
                ViewState["cs"] = value;
            }
        }

        /// <summary>
        /// Current sort order
        /// </summary>
        public string CurrentSortOrder
        {
            get
            {
                if (ViewState["cso"] == null)
                {
                    return DEFAULT_SORT_ORDER;
                }
                else
                {
                    return ViewState["cso"].ToString();
                }
            }
            set
            {
                ViewState["cso"] = value;
            }
        }

        //overridable paging event for gridview
        protected virtual void gridview_PageIndexChanging(object source, System.Web.UI.WebControls.GridViewPageEventArgs e)
        {
            currentPage = e.NewPageIndex;
        }

        //overridable sorting event for gridview
        protected virtual void gridview_Sorting(Object sender, GridViewSortEventArgs e)
        {
            SortSwitch(e.SortExpression); //sets the sort expression
        }

        //function to switch the sort order given a new sort command - for gridview
        private void SortSwitch(string newSort)
        {
            if (CurrentSort == newSort)
            {
                if (CurrentSortOrder == "DESC")
                {
                    CurrentSortOrder = "ASC";
                }
                else
                {
                    CurrentSortOrder = "DESC";
                }
            }
            else
            {
                CurrentSort = newSort;
                CurrentSortOrder = "DESC";
            }
        }

        /// <summary>
        /// They clicked to sort a column
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void btnSort_Click(object sender, System.EventArgs e)
        {
            // Set the sort order
            if (CurrentSortOrder == "DESC")
            {
                CurrentSortOrder = "ASC";
            }
            else if (CurrentSortOrder == "ASC")
            {
                CurrentSortOrder = "DESC";
            }
            else
            {
                CurrentSortOrder = "ASC";
            }

            // Set the sort column
            if (!CurrentSort.Equals(hidSortColumn.Value))
            {
                // Changing sort expression, so set to ASC
                CurrentSortOrder = "ASC";
                CurrentSort = hidSortColumn.Value;
            }
        }

        /// <summary>
        /// Set Header Sort column Text
        /// </summary>
        protected void SetHeaderSortText(DataGrid dgrdToSort)
        {
            DataGridColumn dgrdColumn;
            string strippedHeader;

            // Which arrow to use?
            string arrowImage = "arrow_sort_up.gif";
            if (CurrentSortOrder.Equals("DESC"))
            {
                arrowImage = "arrow_sort.gif";
            }

            // Loop through all sortable columns
            for (int i = 0; i < dgrdToSort.Columns.Count; i++)
            {
                dgrdColumn = dgrdToSort.Columns[i];

                // Is it a sortable column?
                if (dgrdColumn.SortExpression.Length > 0 && dgrdToSort.AllowSorting)
                {
                    strippedHeader = dgrdColumn.HeaderText;

                    if (dgrdToSort.EnableViewState)
                    {
                        strippedHeader = strippedHeader.Replace("<img src=" + ResolveUrl("~/images/arrow_sort.gif") + " border=0/>", "");
                        strippedHeader = strippedHeader.Replace("<img src=" + ResolveUrl("~/images/arrow_sort_up.gif") + " border=0/>", "");
                    }

                    // Is this column the current sorted?
                    if (CurrentSort.Equals(dgrdColumn.SortExpression))
                    {
                        dgrdColumn.HeaderText = "<a href='#' onclick='javascript:document.frmMain." + hidSortColumn.ClientID + ".value=\"" + dgrdColumn.SortExpression + "\";javascript:document.frmMain." + btnSort.ClientID + ".click();'>" + strippedHeader + "<img src=" + ResolveUrl("~/images/" + arrowImage) + " border=0/></a>";
                    }
                    else
                    {
                        dgrdColumn.HeaderText = "<a href='#' onclick='javascript:document.frmMain." + hidSortColumn.ClientID + ".value=\"" + dgrdColumn.SortExpression + "\";javascript:document.frmMain." + btnSort.ClientID + ".click();'>" + strippedHeader + "</a>";
                    }
                }
            }
        }

        /// <summary>
        /// DEFAULT_SORT
        /// </summary>
        /// <returns></returns>
        protected virtual int DEFAULT_PAGE
        {
            get
            {
                return 1;
            }
        }

        /// <summary>
        /// DEFAULT_SORT
        /// </summary>
        /// <returns></returns>
        protected virtual string DEFAULT_SORT
        {
            get
            {
                return "name";
            }
        }

        /// <summary>
        /// DEFAULT_SORT_ORDER
        /// </summary>
        /// <returns></returns>
        protected virtual string DEFAULT_SORT_ORDER
        {
            get
            {
                return "ASC";
            }
        }

    }
}
