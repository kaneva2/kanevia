///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using MagicAjax;
using KlausEnt.KEP.Kaneva.framework.widgets;
using KlausEnt.KEP.Kaneva.mykaneva.widgets;
using KlausEnt.KEP.Kaneva.usercontrols;
using Kaneva.BusinessLayer.Facade;


namespace KlausEnt.KEP.Kaneva.channel
{
	/// <summary>
	/// Summary description for contestPlayer.
	/// </summary>
	public class contestPlayer : System.Web.UI.Page
	{
		private const bool NOT_VALIDATED = false;
		protected System.Web.UI.WebControls.Button btnRegEmail;
		private const bool VALIDATED = true;
		protected System.Web.UI.WebControls.Label lbl_DescVotes;
		protected System.Web.UI.WebControls.Label lbl_DescArtist;
		protected System.Web.UI.WebControls.Label Label7;
		protected System.Web.UI.WebControls.Label lblName_gen;
		protected System.Web.UI.WebControls.Label lbl_DescArtist_gen;
		protected System.Web.UI.WebControls.Label lblArtist_gen;
		protected System.Web.UI.WebControls.Label lbl_DescVotes_gen;
		protected System.Web.UI.WebControls.Label lblVotes_gen;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Label lblUsername_gen;
		protected System.Web.UI.HtmlControls.HtmlTableCell rightPanel;
		protected uc_AssetDetails ucAssetDetails;
		protected System.Web.UI.HtmlControls.HtmlImage IMG1;

		private void Page_Load(object sender, System.EventArgs e)
		{
			int templateId = -1;
			DataRow drAsset = null;
			//litStream.Text = StoreUtility.GetAssetEmbedLink (Convert.ToInt32 (Request ["assetId"]), 499, 498, Page, false, true, true);
			//set parent specific settings for user control
			ucAssetDetails.ShowAssetToolBar = false;
			ucAssetDetails.UCParent = this.Page;
			ucAssetDetails.MediaHeight = 200;
			ucAssetDetails.MediaWidth = 450;
			ucAssetDetails.AllowPictureDetail = false;

			if (!IsPostBack)
			{

				//get te Request parameters
				int assetId = Convert.ToInt32 (Request ["assetId"]);
				int contestId = Convert.ToInt32 (Request ["contestId"]);

				//if the contest id is -1 it is not a contest request it is channel request (added for new widgets use)
				if(contestId == -1)
				{
					try
					{
						//collapse right hand side
						this.rightPanel.Visible = false;

						//set table visibility
						this.tbl_contest.Visible = false;
						this.tbl_general.Visible = true;

						//set labels and values
						drAsset = StoreUtility.GetAssetForLightBox (assetId);
						lblArtist_gen.Text = KanevaGlobals.TruncateWithEllipsis (drAsset ["short_description"].ToString (), 40);
						lblName_gen.Text = KanevaGlobals.TruncateWithEllipsis (drAsset ["name"].ToString (), 40);
						lblVotes_gen.Text = drAsset ["number_of_diggs"].ToString ();
						lblUsername_gen.Text = KanevaGlobals.TruncateWithEllipsis (drAsset ["owner_username"].ToString (), 40);

					}
					catch(Exception ex)
					{
						string error = ex.Message;
						lblVoteStatus.Text = "error loading asset information";
						return;
					}
				}
				else
				{
					try
					{
						//expand the right hand side
						this.rightPanel.Visible = true;

						//set table visibility
						this.tbl_contest.Visible = true;
						this.tbl_general.Visible = false;
						
						//retreive asset information
						drAsset = StoreUtility.GetAssetInContest (contestId, assetId);
						lblName.Text = KanevaGlobals.TruncateWithEllipsis (drAsset ["name"].ToString (), 40);
						lblArtist.Text = KanevaGlobals.TruncateWithEllipsis (drAsset ["artist_name"].ToString (), 40);
						lblVotes.Text = drAsset ["number_of_votes"].ToString ();
						lblUsername.Text = KanevaGlobals.TruncateWithEllipsis (drAsset ["owner_username"].ToString (), 40);
					}
					catch(Exception ex)
					{
						string error = ex.Message;
						lblVoteStatus.Text = "error loading asset information";
						return;
					}

					//retreive contest information
					DataRow drContest = StoreUtility.GetContestByContestId (contestId);

					//set the images and template if any
					templateId = Convert.ToInt32 (drContest ["template_id"]);
					if (drContest["contest_picture"] != null&& drContest["contest_picture"] != System.DBNull.Value)
					{
						this.imgContestPicture.ImageUrl =  drContest["contest_picture"].ToString();
						this.imgContestPicture.ImageAlign = ImageAlign.Middle;
					}
					if(drContest["vote_yes_image"] != null && drContest["vote_yes_image"] != System.DBNull.Value)
					{
						this.btnVote.ImageUrl = drContest["vote_yes_image"].ToString();
						this.btnVote.ImageAlign = ImageAlign.Middle;
					}
					if(drContest["vote_no_image"] != null && drContest["vote_no_image"] != System.DBNull.Value)
					{
						this.btnVoteNo.ImageUrl = drContest["vote_no_image"].ToString();
						this.btnVoteNo.ImageAlign = ImageAlign.Middle;
					}

					
					//if no user info (user is not logged in) skip the contest info and set up
					if(Request.IsAuthenticated)
					{
                        if (KanevaWebGlobals.CurrentUser.StatusId.Equals((int)Constants.eUSER_STATUS.REGVALIDATED))
						{
							ValidationNeeded(VALIDATED);
						}
						else
						{
							ValidationNeeded(NOT_VALIDATED);
							return;//stops processing if user not validated
						}

						if (Convert.ToInt32 (drContest ["vote_ended"]).Equals (1))
						{
							tblVoting.Visible = false;
							lblVoteStatus.Text = "Voting ended on " + KanevaGlobals.FormatDateTime (Convert.ToDateTime (drContest ["vote_end_date"]));;
						}
						else if (Convert.ToInt32 (drContest ["vote_started"]).Equals (1)) 
						{
							tblVoting.Visible = true;
						}
						else
						{
							tblVoting.Visible = false;
							lblVoteStatus.Text = "Voting starts " + KanevaGlobals.FormatDateTime (Convert.ToDateTime (drContest ["vote_start_date"]));
						}

						//check for voting abuse by this ip
						if(StoreUtility.VotingAbuseCheck(Common.GetVisitorIPAddress(), assetId, contestId) >= 5)
						{
							lblVoteStatus.Text = "This entry has already received its allocated votes from your IP address.";
							tblVoting.Visible = false;
						}
						else
						{
							// Make sure they have not already voted
							DataRow drVote = StoreUtility.GetVote (KanevaWebGlobals.GetUserId (), assetId, contestId);

							if (drVote != null)
							{
								lblVoteStatus.Text = "You�ve already voted for this song. Sample another!";
								tblVoting.Visible = false;
							}
						}
					}
					else
					{
						if (!Request.IsAuthenticated)
						{
							lblVoteStatus.Text = "You must be signed in to vote on this item";
							this.tblVoting.Visible = false;

						}
					}

					// Load theme
					LoadTheme (templateId);

				}
			}
		}

		private void ValidationNeeded(bool validated)
		{
			if(validated)
			{
				tblValidateNotice.Visible = false;
				imgContestPicture.Visible = true;
				tblVoting.Visible = true;
			}
			else
			{
				tblValidateNotice.Visible = true;
				imgContestPicture.Visible = false;
				tblVoting.Visible = false;
			}
		}

		/// <summary>
		/// Handle a vote click
		/// </summary>
		protected void btnVoteNo_Click (object sender, ImageClickEventArgs e)
		{
			if (!Request.IsAuthenticated)
			{
				lblVoteStatus.Text = "You must be signed in to vote on this item";
				return;
			}

			int assetId = Convert.ToInt32 (Request ["assetId"]);
			int contestId = Convert.ToInt32 (Request ["contestId"]);

			int iVoteResult = StoreUtility.InsertVote (KanevaWebGlobals.GetUserId (), assetId, contestId, false, Common.GetVisitorIPAddress());
			
			if (iVoteResult.Equals (1))
			{
				// You have already voted
				lblVoteStatus.Text = "You have already voted on this item.";
			}
			else if (iVoteResult.Equals (3))
			{
				lblVoteStatus.Text = "This is not a valid voting date.";
			}
			else if (iVoteResult.Equals (4))
			{
				lblVoteStatus.Text = "This entry has already received its allocated votes from your IP address.";
			}
			else
			{
				lblVoteStatus.Text = "Thank you for participating.";
			}

			tblVoting.Visible = false;
		}

		/// <summary>
		/// Handle a vote click
		/// </summary>
		protected void btnVoteYes_Click (object sender, ImageClickEventArgs e)
		{
			if (!Request.IsAuthenticated)
			{
				lblVoteStatus.Text = "You must be signed in to vote on this item";
				return;
			}

			int assetId = Convert.ToInt32 (Request ["assetId"]);
			int contestId = Convert.ToInt32 (Request ["contestId"]);

			if( StoreUtility.InsertDigg (KanevaWebGlobals.GetUserId (), assetId).Equals (1))
			{
				//if (MagicAjaxContext.Current.IsAjaxCallForPage (Page))
				//{
				//	AjaxCallHelper.WriteAlert ("You have already raved this item.");
				//}
			}

			int iVoteResult = StoreUtility.InsertVote (KanevaWebGlobals.GetUserId (), assetId, contestId, true, Common.GetVisitorIPAddress());
			
			if (iVoteResult.Equals (1))
			{
				// You have already voted
				lblVoteStatus.Text = "You have already voted on this item.";
			}
			else if (iVoteResult.Equals (3))
			{
				lblVoteStatus.Text = "This is not a valid voting date.";
			}
			else if (iVoteResult.Equals (4))
			{
				lblVoteStatus.Text = "This entry has already received its allocated votes from your IP address.";
			}
			else
			{
				try
				{
					lblVotes.Text = Convert.ToString (Convert.ToInt32 (lblVotes.Text) + 1);
				}
				catch (Exception){}

				lblVoteStatus.Text = "Thank you for your vote.";
			}

			tblVoting.Visible = false;
		}

		/// <summary>
		/// LoadTheme
		/// </summary>
		/// <param name="templateId"></param>
		private void LoadTheme (int templateId)
		{
			const int defaultTemplate = 1;
			DataRow drStandardTemplate;

			// Get the correct standard template (theme)
			if (templateId.Equals (0))
			{
				// They have not selected a theme
				drStandardTemplate = WidgetUtility.GetStandardTemplate (defaultTemplate);
				LoadStyleSheet (drStandardTemplate);
			}
			else
			{
				// Load any custimizations
				DataRow drTemplate = WidgetUtility.GetTemplate (templateId);

				if (drTemplate != null)
				{
					drStandardTemplate = WidgetUtility.GetStandardTemplate (Convert.ToInt32 (drTemplate ["standard_template_id"]));
					LoadStyleSheet (drStandardTemplate);

					Literal litCustomStyle = new Literal ();
					litCustomStyle.Text = "<style type=\"text/css\">\n<!--\n";

					// Base font
					if (!drTemplate ["base_font"].Equals (DBNull.Value))
					{
                        DataRow drBaseFont = WebCache.GetFont(Convert.ToInt32(drTemplate["base_font"]));

						if (drBaseFont != null)
						{
							if (!drBaseFont ["name"].Equals (DBNull.Value))
							{
								litCustomStyle.Text += "#chanPage .widgetText {font-family:" + drBaseFont ["name"].ToString () + ";}\n";
							}
							if (!drBaseFont ["rgb"].Equals (DBNull.Value))
							{
								litCustomStyle.Text += "#chanPage .widgetText {color:" + drBaseFont ["rgb"].ToString () + ";}\n";
								litCustomStyle.Text += "#chanPage .widgetTextBold {color:" + drBaseFont ["rgb"].ToString () + ";}\n";
							}
							if (!drBaseFont ["pixel_size"].Equals (DBNull.Value))
							{
								litCustomStyle.Text += "#chanPage .widgetText {font-size:" + drBaseFont ["pixel_size"].ToString () + "px;}\n";
							}
						}
					}

					// Base Link
					if (!drTemplate ["base_link"].Equals (DBNull.Value))
					{
                        DataRow drBaseLink = WebCache.GetFont(Convert.ToInt32(drTemplate["base_link"]));

						if (drBaseLink != null)
						{
							if (!drBaseLink ["rgb"].Equals (DBNull.Value))
							{
								litCustomStyle.Text += "#chanPage A:link{color:" + drBaseLink ["rgb"].ToString () + ";}\n";
							}
						}
					}

					// Base visited
					if (!drTemplate ["base_visited"].Equals (DBNull.Value))
					{
                        DataRow drBaseVisited = WebCache.GetFont(Convert.ToInt32(drTemplate["base_visited"]));

						if (drBaseVisited != null)
						{
							if (!drBaseVisited ["rgb"].Equals (DBNull.Value))
							{
								litCustomStyle.Text += "#chanPage A:visited{color:" + drBaseVisited ["rgb"].ToString () + ";}\n";
							}
						}
					}

					// Base hover
					if (!drTemplate ["base_hover"].Equals (DBNull.Value))
					{
                        DataRow drBaseHover = WebCache.GetFont(Convert.ToInt32(drTemplate["base_hover"]));

						if (drBaseHover != null)
						{
							if (!drBaseHover ["rgb"].Equals (DBNull.Value))
							{
								litCustomStyle.Text += "#chanPage A:hover{color:" + drBaseHover ["rgb"].ToString () + ";}\n";
							}
						}
					}

					// Background color
					if (!drTemplate ["background_rgb"].Equals (DBNull.Value))
					{
						litCustomStyle.Text += "body{background-color:" + drTemplate ["background_rgb"].ToString () + ";}\n";
					}

					// Background picture
					if (!drTemplate ["background_picture"].Equals (DBNull.Value))
					{
						litCustomStyle.Text += "body{background-image:url(" + drTemplate ["background_picture"].ToString () + ");}\n";
					}

					// Background repeat
					if (!drTemplate ["repeat_html"].Equals (DBNull.Value))
					{
						litCustomStyle.Text += drTemplate ["repeat_html"].ToString () + "\n";
					}

					// Background orientation
					if (!drTemplate ["background_orientation"].Equals (DBNull.Value))
					{
						
					}

					// Background fixed
					if (!drTemplate ["background_fixed"].Equals (DBNull.Value))
					{
						
					}

					// Widget Settings

					// Outer Border
					if (!drTemplate ["module_border_rgb"].Equals (DBNull.Value))
					{
						litCustomStyle.Text += "#chanPage .outerFrame * {BACKGROUND:" +drTemplate ["module_border_rgb"].ToString () + ";border-right:1px solid " +drTemplate ["module_border_rgb"].ToString () + ";border-left:1px solid " +drTemplate ["module_border_rgb"].ToString () + ";}\n";
						litCustomStyle.Text += "#chanPage .outerFrame_content {BACKGROUND:" +drTemplate ["module_border_rgb"].ToString () + ";}\n";
					}

					if (!drTemplate ["module_border_width"].Equals (DBNull.Value))
					{
						//litCustomStyle.Text += "#chanHeaderZone .widgetTable {border-width:" +drTemplate ["module_border_width"].ToString () + ";}\n";
						//litCustomStyle.Text += "#chanFooterZone .widgetTable {border-width:" +drTemplate ["module_border_width"].ToString () + ";}\n";
						//litCustomStyle.Text += "#chanBodyZone .widgetTable {border-width:" +drTemplate ["module_border_width"].ToString () + ";}\n";
						//litCustomStyle.Text += "#chanColumnZone .widgetTable {border-width:" +drTemplate ["module_border_width"].ToString () + ";}\n";
					}

					if (!drTemplate ["module_border_style"].Equals (DBNull.Value))
					{
						litCustomStyle.Text += "#chanHeaderZone .innerFrame_content {border-style:" +drTemplate ["module_border_style"].ToString () + ";}\n";
						litCustomStyle.Text += "#chanFooterZone .innerFrame_content {border-style:" +drTemplate ["module_border_style"].ToString () + ";}\n";
						litCustomStyle.Text += "#chanBodyZone .innerFrame_content {border-style:" +drTemplate ["module_border_style"].ToString () + ";}\n";
						litCustomStyle.Text += "#chanColumnZone .innerFrame_content {border-style:" +drTemplate ["module_border_style"].ToString () + ";}\n";
					}

					// Widget Header
					if (!drTemplate ["module_header_rgb"].Equals (DBNull.Value))
					{
						litCustomStyle.Text += "#widgetHeader .innerFrameTitle_content {BACKGROUND:" +drTemplate ["module_header_rgb"].ToString () + ";}\n";
						litCustomStyle.Text += "#widgetHeader .innerFrame * {BACKGROUND:" +drTemplate ["module_header_rgb"].ToString () + ";border-right:1px solid " +drTemplate ["module_header_rgb"].ToString () + ";border-left:1px solid " +drTemplate ["module_header_rgb"].ToString () + ";}\n";
					}

					// Widget Body
					if (!drTemplate ["module_background_rgb"].Equals (DBNull.Value))
					{
						litCustomStyle.Text += "#widgetBody .innerFrame_content {BACKGROUND:" +drTemplate ["module_background_rgb"].ToString () + ";}\n";
						litCustomStyle.Text += "#widgetBody .innerFrame * {BACKGROUND:" +drTemplate ["module_background_rgb"].ToString () + ";;border-right:1px solid " +drTemplate ["module_background_rgb"].ToString () + ";border-left:1px solid " +drTemplate ["module_background_rgb"].ToString () + ";}\n";
					}

					// Widget Body Picture
					if (!drTemplate ["module_picture"].Equals (DBNull.Value))
					{
						litCustomStyle.Text += "#chanHeaderZone .innerFrame_content {background:url(" +drTemplate ["module_picture"].ToString () + ");}\n";
						litCustomStyle.Text += "#chanFooterZone .innerFrame_content {background:url(" +drTemplate ["module_picture"].ToString () + ");}\n";
						litCustomStyle.Text += "#chanBodyZone .innerFrame_content {background:url(" +drTemplate ["module_picture"].ToString () + ");}\n";
						litCustomStyle.Text += "#chanColumnZone .innerFrame_content {background:url(" +drTemplate ["module_picture"].ToString () + ");}\n";
					}

					// Opacity
					if (!drTemplate ["opacity"].Equals (DBNull.Value))
					{
						litCustomStyle.Text += ".outerFrame_content{filter:alpha(opacity=" +drTemplate ["opacity"].ToString () + ")-moz-opacity:." +drTemplate ["opacity"].ToString () + ";opacity:." +drTemplate ["opacity"].ToString () + ";}\n";
					}

					// Custom CSS
					if (!drTemplate ["custom_css"].Equals (DBNull.Value))
					{
						litCustomStyle.Text += Server.HtmlDecode (drTemplate ["custom_css"].ToString ()) + "\n";
					}

					litCustomStyle.Text += "-->\n</style>";
					phStyleCustomizes.Controls.Add (litCustomStyle);
				}				
			}				
		}

		private void LoadStyleSheet (DataRow drStandardTemplate)
		{
			if (drStandardTemplate != null)
			{
				Literal litStyleSheet = new Literal ();
				litStyleSheet.Text = "<LINK href=\"" + drStandardTemplate ["style_sheet"].ToString () + "\" type=\"text/css\" rel=\"stylesheet\">";

				phStyleSheet.Controls.Clear ();
				phStyleSheet.Controls.Add (litStyleSheet);
			}
		}

		/// <summary>
		/// Show an error message on startup
		/// </summary>
		/// <param name="errorMessage"></param>
		protected void ShowErrorOnStartup (string errorMessage, bool bShowPleaseMessage)
		{
			string scriptString = "<script language=JavaScript>";
			if (bShowPleaseMessage)
			{
				scriptString += "alert ('Please correct the following errors:\\n\\n" + errorMessage + "');";
			}
			else
			{
				scriptString += "alert ('" + errorMessage + "');";
			}
			scriptString += "</script>";

			if (!ClientScript.IsClientScriptBlockRegistered (GetType (), "ShowError"))
			{
                ClientScript.RegisterStartupScript(GetType (),"ShowError", scriptString);
			}
		}

		protected ImageButton btnVote;
		protected Label lblName, lblArtist, lblVoteStatus, lblVotes, lblUsername, lblValidationMessage;
		protected HtmlTable tblVoting, tblValidateNotice;
		protected HtmlTable tbl_general, tbl_contest;
		protected HtmlTableCell picturePanel;

		protected PlaceHolder phStyleSheet, phStyleCustomizes;
		protected System.Web.UI.WebControls.ImageButton btnVoteNo;
		protected System.Web.UI.WebControls.Image imgContestPicture;
		protected MagicAjax.UI.Controls.AjaxPanel ajContest;

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnRegEmail.Click += new System.EventHandler(this.btnRegEmail_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void btnRegEmail_Click(object sender, System.EventArgs e)
		{
            MailUtilityWeb.SendRegistrationEmail(KanevaWebGlobals.CurrentUser.Username, KanevaWebGlobals.CurrentUser.Email, KanevaWebGlobals.CurrentUser.KeyValue);
			ShowErrorOnStartup ("Your validation email has been sent.", false);
            this.lblValidationMessage.Text = "<br> <br>Your validation email has been sent. Please check your email account <b>" + KanevaWebGlobals.CurrentUser.Email + " </b> to complete this process and return to vote.";
			tblValidateNotice.Visible = false;
			imgContestPicture.Visible = false;
			lblValidationMessage.Visible = true;
			//Response.Redirect(Request.UrlReferrer.AbsoluteUri);
		}
	}
}
