<%@ Page language="c#" validateRequest="false" Codebehind="channelPage.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.channel.channelPage" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>
<%@ Register TagPrefix="Kaneva" TagName="ProfilePanel" Src="~/usercontrols/ProfilePanel.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="CommDescriptionPanel" Src="~/usercontrols/CommunityProfilePanel.ascx" %>

<script type="text/javascript" src="../jscript/SWFObject/swfobject.js"></script>
<script type="text/javascript" src="../jscript/rollover.js"></script>

<script type="text/javascript" src="../jscript/highslide/highslide.js"></script>
<script type="text/javascript" src="../jscript/highslide/easing_equations.js"></script>
<script type="text/javascript" src="../jscript/highslide/highslide_cfg_profile.js"></script>

<link href="../css/kanevaSC.css" rel="stylesheet" type="text/css" />
<link href="../css/PageLayout/channel_base.css" type="text/css" rel="stylesheet">
<link href="../css/friends.css" type="text/css" rel="stylesheet">
<link href="../css/new.css" type="text/css" rel="stylesheet">
<link href="../css/mykaneva.css" rel="stylesheet" type="text/css">
<link href="../css/newcontrolpanel.css" type="text/css" rel="stylesheet">

<asp:PlaceHolder Runat="server" ID="phStyleSheet" />
<asp:PlaceHolder Runat="server" ID="phStyleCustomizes" />

	<div style="WIDTH:1000px" align="center">
		<!--  Channel content section -->
		<span style="line-height:10px;"><br/></span>
		<table border="0" cellpadding="0" align="center" cellspacing="0" style="WIDTH: 990px; HEIGHT: 100%" id="chanContent">
			<tr>
				<td valign="top" align="left">
					
					
					<div id="divAccessMessage" runat="server" visible="false">
						<table border="0" cellspacing="0" cellpadding="0" width="100%" align="center">
							<tr>
								<td valign="top" align="center">
									<table  border="0" cellpadding="0" cellspacing="0" width="75%">
										<tr>
											<td class="frTopLeft"></td>
											<td class="frTop"></td>
											<td class="frTopRight"></td>
										</tr>
										<tr>
											<td class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img2"/></td>
											<td valign="top" class="frBgIntMembers">
		
												<table cellpadding="0" cellspacing="0" border="0" width="100%">
													<tr>
														<td valign="top" align="center">
															<div class="module whitebg">
																<span class="ct"><span class="cl"></span></span>
																	<h2 id="h2Title" runat="server"></h2>
																	<table cellpadding="6" cellspacing="0" border="0" width="95%">
																	    <tr>
																	        <td rowspan="3" valign="top">    
																	            <kaneva:ProfilePanel id="userProfile" runat="server" visible="false"></kaneva:ProfilePanel> 
																	            <kaneva:CommDescriptionPanel id="communityDesciption" runat="server" visible="false"></kaneva:CommDescriptionPanel> 
																	        </td>
																			<td align="left" valign="top" id="tdActionText" runat="server" style="padding: 0px 20px 20px 20px; width:50%">
																			</td>
																	    </tr>
																		<tr>
																			<td align="center" valign="top" id="tdButton" runat="server">
																				<asp:button id="btnAction" runat="server"></asp:button>
																				<asp:button id="AddFriendThread" onclick="Add_Friend" runat="server" visible="false"></asp:button>
																				<asp:button id="JoinChannelThread" onclick="Join_Channel" runat="server" visible="false"></asp:button>
																			</td>
																		</tr>
																		<tr>
																		    <td align="left" valign="top" id="tdActionText2" runat="server" style="padding: 20px 20px 20px 20px; width:50%">
																		    </td> 
																		</tr>
																		<tr><td colspan="2" align="left" valign="top" id="tdPolicyText" runat="server"></td></tr>
																	</table>
																<span class="cb"><span class="cl"></span></span>
															</div>
														</td>
													</tr>
												</table>
												
											</td>
											<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img3"/></td>
										</tr>
										<tr>
											<td class="frBottomLeft"></td>
											<td class="frBottom"></td>
											<td class="frBottomRight"></td>
										</tr>
										<tr>
											<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" id="Img4"/></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</div>
					
					
					<div id="chanPage" style="Z-INDEX: 1">
						<table cellpadding="0" cellspacing="0" width="100%" border="0">
							<tr>
								<td colspan="3">
									<div id="chanHeaderZone" class="layoutHeader">
										<!-- begin header zone -->
										<asp:PlaceHolder Runat="server" ID="placeHolderHeader" />
										<!-- end header zone -->
									</div>
								</td>
							</tr>
							<tr>
								<td valign="top" height="100%" id="chanBodyZone" runat="server">
									<asp:PlaceHolder Runat="server" ID="placeHolderColumnTop" />
									<asp:PlaceHolder Runat="server" ID="placeHolderBody" />
								</td>
								<td id="mwZoneBorder" width="10">
									&nbsp;&nbsp;
								</td>
								
								<td valign="top" height="100%" id="chanColumnZone" align="right" runat="server">
									
									<asp:PlaceHolder Runat="server" ID="placeHolderColumn" />
								</td>
							</tr>
							<tr>
								<td colspan="3">
									<div id="chanFooterZone" class="layoutHeader">
										<!-- begin footer zone -->
										<asp:PlaceHolder Runat="server" ID="placeHolderFooter" />
										<!-- end footer zone -->
									</div>
								</td>
							</tr>
						</table>
					</div>
				</td>
			</tr>
		</table>
	</div>

<style type="text/css">
* {
    font-family: Verdana, Helvetica;
    font-size: 10pt;
}
.highslide-html {
    background-color: white;
}
.highslide-html-blur {
}
.highslide-html-content {
	position: absolute;
    display: none;
}
.highslide-loading {
    display: block;
	color: black;
	font-size: 8pt;
	font-family: sans-serif;
	font-weight: bold;
    text-decoration: none;
	padding: 2px;
	border: 1px solid black;
    background-color: white;
    
    padding-left: 22px;
    background-image: url(../images/highslide/loader.white.gif);
    background-repeat: no-repeat;
    background-position: 3px 1px;
}
a.highslide-credits,
a.highslide-credits i {
    padding: 2px;
    color: silver;
    text-decoration: none;
	font-size: 10px;
}
a.highslide-credits:hover,
a.highslide-credits:hover i {
    color: white;
    background-color: gray;
}


/* Styles for the popup */
.highslide-wrapper {
	background-color: white;
}
.highslide-wrapper .highslide-html-content {
    width: 400px;
    padding: 5px;
}
.highslide-wrapper .highslide-header div {
}
.highslide-wrapper .highslide-header ul {
	margin: 0;
	padding: 0;
	text-align: right;
}
.highslide-wrapper .highslide-header ul li {
	display: inline;
	padding-left: 1em;
}
.highslide-wrapper .highslide-header ul li.highslide-previous, .highslide-wrapper .highslide-header ul li.highslide-next {
	display: none;
}
.highslide-wrapper .highslide-header a {
	font-weight: bold;
	color: gray;
	text-transform: uppercase;
	text-decoration: none;
}
.highslide-wrapper .highslide-header a:hover {
	color: black;
}
.highslide-wrapper .highslide-header .highslide-move a {
	cursor: move;
}
.highslide-wrapper .highslide-footer {
	height: 11px;
}
.highslide-wrapper .highslide-footer .highslide-resize {
	float: right;
	height: 11px;
	width: 11px;
	background: url(../images/highslide/resize.gif);
}
.highslide-wrapper .highslide-body {
}
.highslide-move {
    cursor: move;
}
.highslide-resize {
    cursor: nw-resize;
}

/* These must be the last of the Highslide rules */
.highslide-display-block {
    display: block;
}
.highslide-display-none {
    display: none;
}
</style>

