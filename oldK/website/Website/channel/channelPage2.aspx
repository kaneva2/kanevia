<%@ Page Language="C#" MasterPageFile="~/masterpages/ProfilePageTemplate.Master" AutoEventWireup="false" CodeBehind="channelPage2.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.channel.channelPage2" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>
<%@ Register TagPrefix="Kaneva" TagName="ProfilePanel" Src="~/usercontrols/ProfilePanel.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="CommDescriptionPanel" Src="~/usercontrols/CommunityProfilePanel.ascx" %>

<asp:Content ID="cnt_buySpecials" runat="server" contentplaceholderid="cph_Body" >


<script type="text/javascript"><!--

document.observe("dom:loaded", PassesInit('test parameters'));

function PassesInit (message)
{
    alert(message);
    
    var ajaxRequest = new Ajax.Request('usercontrols/doodad/PassesData.aspx', {
        method:       'get', 
        parameters:   '', 
        asynchronous: true,
        evalJS:true,
        evalScripts:true,
        onComplete:   showPasses,
        onFailure: errorPasses
    });
}


//--> </script> 
        

    <asp:PlaceHolder Runat="server" ID="phStyleSheet" />
    <asp:PlaceHolder Runat="server" ID="phStyleCustomizes" />

	    <div style="WIDTH:1000px" align="center">
		    <!--  Channel content section -->
		    <span style="line-height:10px;"><br/></span>
		    <table border="0" cellpadding="0" align="center" cellspacing="0" style="WIDTH: 990px; HEIGHT: 100%" id="chanContent">
			    <tr>
				    <td valign="top" align="left">
   					
					    <div id="divAccessMessage" runat="server" visible="false">
						    <table border="0" cellspacing="0" cellpadding="0" width="100%" align="center">
							    <tr>
								    <td valign="top" align="center">
									    <table  border="0" cellpadding="0" cellspacing="0" width="75%">
										    <tr>
											    <td class="frTopLeft"></td>
											    <td class="frTop"></td>
											    <td class="frTopRight"></td>
										    </tr>
										    <tr>
											    <td class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img2"/></td>
											    <td valign="top" class="frBgIntMembers">
    		
												    <table cellpadding="0" cellspacing="0" border="0" width="100%">
													    <tr>
														    <td valign="top" align="center">
															    <div class="module whitebg">
																    <span class="ct"><span class="cl"></span></span>
																	    <h2 id="h2Title" runat="server"></h2>
																	    <table cellpadding="6" cellspacing="0" border="0" width="95%">
																	        <tr>
																	            <td rowspan="3" valign="top">    
																	                <kaneva:ProfilePanel id="userProfile" runat="server" visible="false"></kaneva:ProfilePanel> 
																	                <kaneva:CommDescriptionPanel id="communityDesciption" runat="server" visible="false"></kaneva:CommDescriptionPanel> 
																	            </td>
																			    <td align="left" valign="top" id="tdActionText" runat="server" style="padding: 0px 20px 20px 20px; width:50%">
																			    </td>
																	        </tr>
																		    <tr>
																			    <td align="center" valign="top" id="tdButton" runat="server">
																				    <asp:button id="btnAction" runat="server"></asp:button>
																				    <asp:button id="AddFriendThread" onclick="Add_Friend" runat="server" visible="false"></asp:button>
																				    <asp:button id="JoinChannelThread" onclick="Join_Channel" runat="server" visible="false"></asp:button>
																			    </td>
																		    </tr>
																		    <tr>
																		        <td align="left" valign="top" id="tdActionText2" runat="server" style="padding: 20px 20px 20px 20px; width:50%">
																		        </td> 
																		    </tr>
																		    <tr><td colspan="2" align="left" valign="top" id="tdPolicyText" runat="server"></td></tr>
																	    </table>
																    <span class="cb"><span class="cl"></span></span>
															    </div>
														    </td>
													    </tr>
												    </table>
    												
											    </td>
											    <td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img3"/></td>
										    </tr>
										    <tr>
											    <td class="frBottomLeft"></td>
											    <td class="frBottom"></td>
											    <td class="frBottomRight"></td>
										    </tr>
										    <tr>
											    <td><img runat="server" src="~/images/spacer.gif" width="1" height="14" id="Img4"/></td>
										    </tr>
									    </table>
								    </td>
							    </tr>
						    </table>
					    </div>
					    
                    <ajax:ajaxpanel id="ajpPageLayout" runat="server">
                    
					    <div id="div_PageLayout" runat="server" visible="false" style="background-color:#ffffff; color:#000000; border: solid 2 #000000; margin-bottom:20px">
						    <asp:label id="pgLayoutMessages" runat="server" />
						    <table cellpadding="2" cellspacing="0" width="100%" border="1">
						        <tr>
						            <td valign="top" style="width:180px">
						                <!-- page management -->
						                <b>Page Management</b><br />
						                <table cellpadding="4" cellspacing="0" width="100%" border="0">
						                    <tr>
						                        <td align="center"><asp:DropDownList ID="ddl_AvailablePages" style="width:100%" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddl_AvailablePages_SelectedIndexChanged"/></td>
						                    </tr>
						                    <tr>
						                        <td><asp:textbox id="txtPageName" style="width:150px" runat="server" name="tbName" type="text" cssclass="formKanevaText" maxlength="50"/></td>
						                    </tr>
						                    <tr>
						                        <td>
                                                    <asp:Button ID="btnAdd" Width="70px" runat="server" Text="Add Page" OnClick="btnAdd_Click" />
                                                    <asp:Button ID="btnDelete" Width="90px" runat="server" Text="Delete Page" OnClick="btnDelete_Click" />
                                                </td>
						                    </tr>
						                </table>
						            </td>
						            <td valign="top" style="width:120px">
						                <!-- page security -->
						                <b>Security</b><br />
						                <table cellpadding="4" cellspacing="0" width="100%" border="0">
						                    <tr>
						                        <td>
						                            <asp:DropDownList id="ddlPageAccess" runat="server" style="width:100%" name="ddlAccess" class="Filter2" AutoPostBack="true" OnSelectedIndexChanged="ddlPageAccess_SelectedIndexChanged" />
												</td>
						                    </tr>
						                    <tr>
						                        <td>
						                            <asp:dropdownlist style="width:100%" id="ddlNewGroup" runat="server" name="ddlNewGroup" class="Filter2" Enabled="false" />
												</td>
						                    </tr>
						                </table>
						            </td>
						            <td valign="top" >
						                <!-- add widgets -->
						                <b>Add Widgets</b><br />
                                        <asp:DataList ID="dl_WidgetPanels" runat="server" RepeatColumns="4" RepeatDirection="Horizontal" RepeatLayout="Table">
                                            <HeaderTemplate>
                                                <table cellpadding="2" cellspacing="0" width="100%" border="0">
                                                    <tr>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                        <td style="width:140px">
                                                            <input id="hd_ModuleId" type="hidden" runat="server" value='<%#DataBinder.Eval(Container.DataItem, "module_group_id")%>' />
                                                            <i><asp:Label ID="lbl_widgetGroupName" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "title")%>'></asp:Label></i>
                                                            <asp:dropdownlist style="width:110px" id="ddl_widgetPanel" runat="server" name="ddlNewGroup" class="Filter2" />
                                                            <asp:LinkButton id="lbnAddWidget" class="nohover" title="Add selected widget to page." OnClick="lbnAddWidget_Click" runat="server" CausesValidation="False">
																<img src="../images/btnico_add.gif" title="Add selected widget to page." height="16" width="16" border="0" />
															</asp:LinkButton>
                                                        </td>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                    </tr>
                                                </table>
                                            </FooterTemplate>
                                        </asp:DataList>		
						            </td>
						            <td valign="top" style="width:70px">
						                <!-- save page -->
						                <table cellpadding="4" cellspacing="0" width="100%" border="0">
						                    <tr>
						                        <td>
                                                    <asp:button id="btnSaveTop" runat="server" name="btnSaveTop" style="CURSOR:hand" onclick="btnSaveWidgets_Click"  text="Save" width="70px" height="22px"/>												
                                                </td>
						                    </tr>
						                </table>
						            </td>
						        </tr>
						    </table>
                        </div>    					
    				</ajax:ajaxpanel>
				    <div id="draggable_demo" style="background-color:White; width:100%; margin: 10px 10px 10px 10px">
                        <div id="draggable_demo_1" class="draggable" style="margin:0" >This is a reorderable paragraph. <a href="#" onclick="return false">move</a></div>
                        <div id="draggable_demo_2"  class="draggable" style="margin:0" >This is another reorderable paragraph. <a href="#" onclick="return false">move</a></div>
                        <div  id="draggable_demo_3" class="draggable" style="margin:0" >This is yet another reorderable paragraph. <a href="#" onclick="return false">move</a></div>
                        <div  id="draggable_demo_4" class="draggable" style="margin:0" >And can you believe it? Another reorderable paragraph. <a href="#" onclick="return false">move</a></div>
                    </div>
    				
					    <div id="chanPage" style="Z-INDEX: 1">
						    <table cellpadding="0" cellspacing="0" width="100%" border="0">
							    <tr>
								    <td colspan="3">
									    <div id="chanHeaderZone" class="layoutHeader">
										    <!-- begin header zone -->
										    <asp:PlaceHolder Runat="server" ID="placeHolderHeader" />
										    <!-- end header zone -->
									    </div>
								    </td>
							    </tr>
							    <tr>
								    <td valign="top" height="100%" id="chanBodyZone" runat="server">
								        <div id="chanBodyZone2" class="layoutHeader">
									        <asp:PlaceHolder Runat="server" ID="placeHolderColumnTop" />
									        <asp:PlaceHolder Runat="server" ID="placeHolderBody" />
									    </div>
								    </td>
								    <td id="mwZoneBorder" width="10">
									    &nbsp;&nbsp;
								    </td>
    								
								    <td valign="top" height="100%" id="chanColumnZone" align="right" runat="server">
								        <div id="chanBodyZone3" class="layoutHeader">
									        <asp:PlaceHolder Runat="server" ID="placeHolderColumn" />
									    </div>
								    </td>
							    </tr>
							    <tr>
								    <td colspan="3">
									    <div id="chanFooterZone" class="layoutHeader">
										    <!-- begin footer zone -->
										    <asp:PlaceHolder Runat="server" ID="placeHolderFooter" />
										    <!-- end footer zone -->
									    </div>
								    </td>
							    </tr>
						    </table>
					    </div>
				    </td>
			    </tr>
		    </table>
	    </div>
	    
    <script type="text/javascript">
        Sortable.create("chanHeaderZone", {elements:$$('#chanHeaderZone div'), handles:$$('#chanHeaderZone a')});
        Sortable.create("chanBodyZone2", {elements:$$('#chanBodyZone2 div'), handles:$$('#chanBodyZone2 a')});
        Sortable.create("chanBodyZone3", {elements:$$('#chanBodyZone3 div'), handles:$$('#chanBodyZone3 a')});
        Sortable.create("chanFooterZone", {elements:$$('#chanFooterZone div'), handles:$$('#chanFooterZone a')});
      
        new Draggable('draggable_demo_1', { revert: false, handles:$$('#draggable_demo_1 a') });
        new Draggable('draggable_demo_2', { revert: false, handles:$$('#draggable_demo_2 a') });
        new Draggable('draggable_demo_3', { revert: false, handles:$$('#draggable_demo_3 a') });
        new Draggable('draggable_demo_4', { revert: false, handles:$$('#draggable_demo_4 a') });
        
        Droppables.add('chanHeaderZone', { 
        accept: 'draggable',
        hoverclass: 'hover',
        onDrop: function(){alert('released');}
        });
      
        Droppables.add('chanBodyZone2', { 
        accept: 'draggable',
        hoverclass: 'hover',
        onDrop: function(){alert('released');}
        });
      
        Droppables.add('chanBodyZone3', { 
        accept: 'draggable',
        hoverclass: 'hover',
        onDrop: function(){alert('released');}
        });
      
        Droppables.add('chanFooterZone', { 
        accept: 'draggable',
        hoverclass: 'hover',
        onDrop: function(){alert('released');}
        });
      
    </script>
    
    <style type="text/css">
    
    div#chanHeaderZone.hover 
    {
        border: 5px dashed #aaa;
        background-color:red; 
    }
    div#chanBodyZone2.hover 
    {
        border: 5px dashed #aaa;
        background-color:white; 
    }
    div#chanBodyZone3.hover 
    {
        border: 5px dashed #aaa;
        background-color:green; 
    }

    div#chanFooterZone.hover 
    {
        border: 5px dashed #aaa;
        background-color:blue; 
    }
    
    * {
        font-family: Verdana, Helvetica;
        font-size: 10pt;
    }
    .highslide-html {
        background-color: white;
    }
    .highslide-html-blur {
    }
    .highslide-html-content {
	    position: absolute;
        display: none;
    }
    .highslide-loading {
        display: block;
	    color: black;
	    font-size: 8pt;
	    font-family: sans-serif;
	    font-weight: bold;
        text-decoration: none;
	    padding: 2px;
	    border: 1px solid black;
        background-color: white;
        
        padding-left: 22px;
        background-image: url(../images/highslide/loader.white.gif);
        background-repeat: no-repeat;
        background-position: 3px 1px;
    }
    a.highslide-credits,
    a.highslide-credits i {
        padding: 2px;
        color: silver;
        text-decoration: none;
	    font-size: 10px;
    }
    a.highslide-credits:hover,
    a.highslide-credits:hover i {
        color: white;
        background-color: gray;
    }


    /* Styles for the popup */
    .highslide-wrapper {
	    background-color: white;
    }
    .highslide-wrapper .highslide-html-content {
        width: 400px;
        padding: 5px;
    }
    .highslide-wrapper .highslide-header div {
    }
    .highslide-wrapper .highslide-header ul {
	    margin: 0;
	    padding: 0;
	    text-align: right;
    }
    .highslide-wrapper .highslide-header ul li {
	    display: inline;
	    padding-left: 1em;
    }
    .highslide-wrapper .highslide-header ul li.highslide-previous, .highslide-wrapper .highslide-header ul li.highslide-next {
	    display: none;
    }
    .highslide-wrapper .highslide-header a {
	    font-weight: bold;
	    color: gray;
	    text-transform: uppercase;
	    text-decoration: none;
    }
    .highslide-wrapper .highslide-header a:hover {
	    color: black;
    }
    .highslide-wrapper .highslide-header .highslide-move a {
	    cursor: move;
    }
    .highslide-wrapper .highslide-footer {
	    height: 11px;
    }
    .highslide-wrapper .highslide-footer .highslide-resize {
	    float: right;
	    height: 11px;
	    width: 11px;
	    background: url(../images/highslide/resize.gif);
    }
    .highslide-wrapper .highslide-body {
    }
    .highslide-move {
        cursor: move;
    }
    .highslide-resize {
        cursor: nw-resize;
    }

    /* These must be the last of the Highslide rules */
    .highslide-display-block {
        display: block;
    }
    .highslide-display-none {
        display: none;
    }
    </style>

</asp:Content>