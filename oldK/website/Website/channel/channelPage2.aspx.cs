///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Collections.Specialized;
using System.Data;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using KlausEnt.KEP.Kaneva.framework.biz.widgets;
using KlausEnt.KEP.Kaneva.framework.widgets;
using KlausEnt.KEP.Kaneva.mykaneva.widgets;
using MagicAjax;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.channel
{
    /// <summary>
    /// Summary description for channelPage.
    /// </summary>
    public partial class channelPage2 : BasePage
    {

        #region Declerations
        protected PlaceHolder placeHolderHeader;
        protected PlaceHolder placeHolderFooter;
        protected PlaceHolder placeHolderColumnTop;
        protected PlaceHolder placeHolderColumn;
        protected PlaceHolder placeHolderBody;
        protected PlaceHolder phStyleSheet, phStyleCustomizes;
        protected HtmlGenericControl divStaticUrls;
        protected Button AddFriendThread, JoinChannelThread, btnAction;
        protected HtmlTableCell chanBodyZone, chanColumnZone;
        protected HtmlTableCell tdActionText, tdActionText2, tdButton, tdPolicyText;
        protected HtmlContainerControl h2Title, divAccessMessage;
        protected Kaneva.usercontrols.ProfilePanel userProfile;
        protected Kaneva.usercontrols.CommunityProfilePanel communityDesciption;
        private int _channelId;
        private int _channelOwnerId;
        private int _pageId;
        private string _channelName, _page_name = "";
        private bool _canEdit = false;
        private bool _isDefault = false;
        private bool _isPersonal = true;
        private int _num_control_panels = 0;

        private IDictionary _widgets;
        #endregion

        #region Page Load

        private void Page_Load(object sender, EventArgs e)
        {
            _widgets = new Hashtable();

            if (!GetRequestParams())
            {
                //invalid params
                if (Request.IsAuthenticated)
                {
                    RedirectToHomePage();
                }
                else
                {
                    Response.Redirect(this.GetLoginURL());
                }
            }

            // Get the community
            DataRow drChannel = CommunityUtility.GetCommunity(_channelId);


            if (drChannel["status_id"] == DBNull.Value)
            {
                //Community probably doesn't exist becaue status_id is null so display error.
                h2Title.InnerText = "Channel Not Found";
                tdActionText.InnerHtml = "The requested channel does not exist or has been deleted.";
                btnAction.Visible = false;
                divAccessMessage.Visible = true;

                return;
            }

            _isPersonal = CommunityUtility.IsChannelPersonal(drChannel);
            _isDefault = (_pageId == PageUtility.GetChannelDefaultPageId(_channelId));
            _channelOwnerId = Convert.ToInt32(drChannel["creator_id"]);
            _channelName = drChannel["name"].ToString();
            _canEdit = CommunityUtility.IsCommunityModerator(_channelId, GetUserId());

            //set the non AP message panels values
            communityDesciption.PageID = _pageId;
            userProfile.ProfileOwnersID = _channelOwnerId;
            userProfile.ChannelID = _channelId;

            if (_isPersonal)
            {
                //configure the visibilty of the no AP messaging
                communityDesciption.Visible = false;
                userProfile.Visible = true;

                // Add the javascript for adding friend
                string scriptString = "<script language=\"javaScript\">\n<!--\n function AddFriend() {\n";
                scriptString += "	" + ClientScript.GetPostBackEventReference(AddFriendThread, "") + ";\n";
                scriptString += "}\n// -->\n";
                scriptString += "</script>";

                if (!ClientScript.IsClientScriptBlockRegistered(GetType(), "AddFriend"))
                {
                    ClientScript.RegisterClientScriptBlock(GetType(), "AddFriend", scriptString);
                }
            }
            else
            {
                //configure the visibilty of the no AP messaging
                communityDesciption.Visible = true;
                userProfile.Visible = false;

                // Add the javascript for joining channel
                string scriptString = "<script language=\"javaScript\">\n<!--\n function JoinChannel() {\n";
                scriptString += "	" + ClientScript.GetPostBackEventReference(JoinChannelThread, "") + ";\n";
                scriptString += "}\n// -->\n";
                scriptString += "</script>";

                if (!ClientScript.IsClientScriptBlockRegistered(GetType(), "JoinChannel"))
                {
                    ClientScript.RegisterClientScriptBlock(GetType(), "JoinChannel", scriptString);
                }
            }

            SetNav(_isPersonal);
            BindData(drChannel);

            //set all meta data
            if (_isPersonal)
            {
                // Build Title
                string pageNameToShow = "";
                if (!_isDefault)
                {
                    pageNameToShow = _page_name;
                }
                ((IndexPageTemplate)Master).Title = MetaDataGenerator.GenerateTitle(_channelName + " - Kaneva Profile", pageNameToShow, (int)Constants.eWEB_PAGE_TYPE.PROFILE);

                // Build Keywords
                NameValueCollection tagList = new NameValueCollection();
                if (drChannel != null)
                {
                    tagList.Add("location", drChannel["location"].ToString());
                }
                else
                {
                    tagList.Add("location", "n/a");
                }
                ((IndexPageTemplate)Master).MetaDataKeywords = MetaDataGenerator.GenerateMetaKeywords(_channelName, tagList, (int)Constants.eWEB_PAGE_TYPE.COMMUNITY);

                //clears collection
                tagList.Clear();

                //repopulates
                if (drChannel != null)
                {
                    tagList.Add("age", drChannel["age"].ToString());
                    tagList.Add("gender", drChannel["gender"].ToString());
                    tagList.Add("location", drChannel["location"].ToString());
                    tagList.Add("community1", "");
                    tagList.Add("community2", "");
                    tagList.Add("community3", "");
                    tagList.Add("friends", drChannel["friends"].ToString());
                    tagList.Add("raves", drChannel["number_of_diggs"].ToString());
                }
                else
                {
                    tagList.Add("age", "0");
                    tagList.Add("gender", "n/a");
                    tagList.Add("country", "n/a");
                    tagList.Add("location", "n/a");
                    tagList.Add("community1", "n/a");
                    tagList.Add("community2", "n/a");
                    tagList.Add("community3", "n/a");
                    tagList.Add("friends", "n/a");
                    tagList.Add("raves", "n/a");
                }
                ((IndexPageTemplate)Master).MetaDataDescription = MetaDataGenerator.GenerateMetaDescription(_channelName, tagList, (int)Constants.eWEB_PAGE_TYPE.PROFILE);
            }
            else
            {
                NameValueCollection tagList = new NameValueCollection();
                tagList.Add("Videos", "Videos");
                tagList.Add("Photos", "Photos");
                tagList.Add("Music", "Music");
                tagList.Add("Blogs", "Blogs");
                tagList.Add("Forums", "Forums");

                Title = MetaDataGenerator.GenerateTitle(_channelName + " - Kaneva Community", "", (int)Constants.eWEB_PAGE_TYPE.COMMUNITY);
                ((IndexPageTemplate)Master).MetaDataDescription = MetaDataGenerator.GenerateMetaDescription(_channelName, tagList, (int)Constants.eWEB_PAGE_TYPE.COMMUNITY);
                ((IndexPageTemplate)Master).MetaDataKeywords = MetaDataGenerator.GenerateMetaKeywords(_channelName, tagList, (int)Constants.eWEB_PAGE_TYPE.COMMUNITY);
            }


            //populate all page layout management data
            if (!IsPostBack)
            {
                //only called if page is in edit mode
                GetAvailablePages();
                GetAvailableWidgets();
                GetPageSecurity();
                SetPageLayout();
            }

        }

        #endregion

        #region Profile Display Functions & Events

        /// <summary>
        /// BindData
        /// </summary>
        private void BindData(DataRow drChannel)
        {

            bool show_page = true;

            // only users who choose to display mature material can see this page if the owner
            // has marked their profile as containing mature material
            //ignore this for the owner or community moderator

            bool blockMature = false;
            if (!KanevaWebGlobals.CurrentUser.HasAccessPass && !_channelOwnerId.Equals(GetUserId()))
            {
                //check if channel is mature
                if (_isPersonal)
                {
                    blockMature = (Convert.ToInt32(drChannel["mature_profile"]).Equals(1));
                }
                else
                {
                    blockMature = drChannel["is_adult"].ToString().Equals("Y");
                }
            }

            // Does it require user to be over 21?
            bool blockOver21 = false;
            if (!KanevaWebGlobals.CurrentUser.Over21)
            {
                blockOver21 = drChannel["over_21_required"].ToString().Equals("Y");
            }

            string community_guidelines = "<br/><br/><u><b>Kaneva Community Guidelines</b></u><br/>" +
                "Kaneva defines restricted content as anything that contains strong language, depictions of nonsexual nudity, violence " +
                "or substance abuse.  When you mark content as Restricted, you protect our children and visitors -- and give yourself greater " +
                "freedom of expression.  Only Kaneva members 18 years of age or older may post or view restricted content.";

            string privacy_settings = "<br/><br/><u><b>Privacy Settings for Kaneva Members</b></u><br/>" +
                "Kaneva members have the option to set their media, profiles, and communities to \"friends only\" or \"private\". " +
                "When the item is set to friends only, then only friends within the member's private " +
                "network are allowed to view detailed information such as personal interests and friends. Items set to Private are only viewable by the member. These settings are " +
                "part of a broad effort at Kaneva to help protect the privacy of our members and provide a safe " +
                "environment for all members to connect online.";

            if (blockOver21)
            {
                show_page = false;
                string page_type = _isPersonal ? "profile" : "community";

                h2Title.InnerText = "If you want to enjoy this Community ...";
                tdActionText.InnerHtml = "You must be age 21 or older*.<br/><br/><br/><span class=\"note\">* Kaneva members can restrict access to their Communities if their content is tailored for specific audiences.</span>";

                btnAction.Visible = false;

                divAccessMessage.Visible = true;
            }

            if (blockMature)
            {
                show_page = false;
                string page_type = _isPersonal ? "profile" : "community";

                if (!Request.IsAuthenticated)
                {
                    h2Title.InnerText = "This Item is Restricted";

                    tdActionText.InnerHtml = "The " + page_type + " you are trying to access has been set to \"Restricted\" by its " +
                        "owner and may contain material inappropriate for anyone under the age of 18. Please log in to verify your right " +
                        "to access this page.";

                    tdPolicyText.InnerHtml = community_guidelines;
                    tdPolicyText.Visible = false;

                    btnAction.Text = " Sign in ";
                    btnAction.Attributes.Add("onclick", "location.href='" + GetLoginURL() + "';return false;");

                    divAccessMessage.Visible = true;
                }
                else if (KanevaWebGlobals.CurrentUser.IsAdult)   // user is 18 or over
                {
                    h2Title.InnerText = "This " + page_type + " can only be seen by people with an Access Pass";

                    tdActionText.InnerHtml = "The " + page_type + " has taken advantage of our Access Pass subscription " +
                        "and has set this " + page_type + " so that it can only be seen by other people with Access Pass. " +
                        "<br/><br/>What are you waiting for? Get Yours now. <br />";
                    tdActionText2.InnerHtml = "Kaneva's Access Pass includes: <br /><ul><li style=\"line-height:11px; font-size:11px; list-style-type:disc\">Access to restricted (18 and over) clubs and hangouts</li>" +
                        "<li style=\"line-height:11px; font-size:11px; list-style-type:disc\">Exclusive clothing and accessories for restricted areas</li><li style=\"line-height:11px; font-size:11px; list-style-type:disc\">Special privileges only Access Pass members enjoy</li>" +
                        "<li style=\"line-height:11px; font-size:11px; list-style-type:disc\">Access to private events</li><li style=\"line-height:11px; font-size:11px; list-style-type:disc\">And more!</li><br />";

                    tdPolicyText.InnerHtml = community_guidelines;
                    tdPolicyText.Visible = false;

                    btnAction.Text = "Purchase Access Pass";
                    btnAction.Attributes.Add("onclick", "javacript:location.href='" + ResolveUrl("~/mykaneva/passDetails.aspx?pass=true&passID=" + KanevaGlobals.AccessPassGroupID) + "';return false;");

                    divAccessMessage.Visible = true;
                }
                else   // user is under 18 or age unknown
                {
                    h2Title.InnerText = "This Item is Restricted";

                    tdActionText.InnerHtml = "The " + page_type + " you are trying to access has been set to \"Restricted\" by its " +
                        "owner and may contain material inappropriate for anyone under the age of 18.<br/><br/>" +
                        "NOTE:  If you believe you are receiving this page in error, please contact support via the Support Page.";

                    tdPolicyText.InnerHtml = community_guidelines;
                    tdPolicyText.Visible = false;

                    btnAction.Text = " Go Back ";
                    btnAction.Attributes.Add("onclick", "javacript:history.back();return false;");

                    divAccessMessage.Visible = true;
                }
            }
            else
            {
                // if not the owner/admin, determine visibility permissions
                if (!_canEdit)
                {
                    DataRow drLayoutPage = PageUtility.GetLayoutPage(PageId);
                    if (drLayoutPage != null)
                    {
                        //pull page name for SEO
                        _page_name = drLayoutPage["name"].ToString();

                        int access_id = Convert.ToInt32(drLayoutPage["access_id"].ToString());
                        int group_id = drLayoutPage["group_id"] != DBNull.Value ? Convert.ToInt32(drLayoutPage["group_id"].ToString()) : 0;
                        switch (access_id)
                        {
                            // public
                            case (int)Constants.ePAGE_ACCESS.PUBLIC:
                                break;

                            // friends
                            case (int)Constants.ePAGE_ACCESS.FRIENDS:

                                // if user is not logged in, show message and link to login
                                if (!Request.IsAuthenticated)
                                {
                                    show_page = false;

                                    h2Title.InnerText = "This Member?s Profile is Viewable by Friends Only";

                                    tdActionText.InnerHtml = "This member has set their profile to be viewable by \"friends only\".  If you " +
                                        "are on this member's friends list, sign into Kaneva and you will be granted access to this profile. " +
                                        "If you would like to be their friend, sign into Kaneva and you will be given the opportunity to send this " +
                                        "member a Friend Request.  Once approved by the member, you will be given access to their profile.";

                                    tdPolicyText.InnerHtml = privacy_settings;

                                    btnAction.Text = " Sign in ";
                                    btnAction.Attributes.Add("onclick", "location.href='" + GetLoginURL() + "';return false;");

                                    divAccessMessage.Visible = true;
                                }
                                else
                                {
                                    // if any friend can view this page and this user is a friend
                                    if (group_id == 0)
                                    {
                                        show_page = UsersUtility.IsFriend(ChannelOwnerId, GetUserId());

                                        if (!show_page)	  // Not a Friend
                                        {
                                            DataRow drRequest = UsersUtility.GetFriendRequest(ChannelOwnerId, GetUserId());

                                            if (drRequest != null) // existing friend request
                                            {
                                                h2Title.InnerText = "This Member?s Profile is Viewable by Friends Only";

                                                tdActionText.InnerHtml = "This member has set their profile to be viewable by \"friends only\". " +
                                                    "Once approved by the member, you will be given access to their profile.<br/><br/>" +
                                                    "<span class=\"alertmessage\"><b>A Friend Request has been sent to this member.</b></span>";

                                                tdPolicyText.InnerHtml = privacy_settings;

                                                btnAction.Text = " Go Back ";
                                                btnAction.Attributes.Add("onclick", "javacript:history.go(" + (IsPostBack ? "-2" : "-1") + ");return false;");
                                            }
                                            else
                                            {
                                                h2Title.InnerText = "This Member?s Profile is Viewable by Friends Only";

                                                tdActionText.InnerText = "This member has set their profile to be viewable by \"friends only\".  If " +
                                                    "you would like to be their friend, you can send a Friend Request to this member.  Once you are approved " +
                                                    "as a friend by the member, you will be given access to their profile.";

                                                tdPolicyText.InnerHtml = privacy_settings;

                                                btnAction.Text = "Friend Request";
                                                btnAction.Attributes.Add("onclick", "javascript:if (confirm('Send a friend request to this member?') ) { AddFriend(); } else { return false; }");
                                            }

                                            divAccessMessage.Visible = true;
                                        }
                                    }
                                    else
                                    {
                                        // check if user is part of friends group
                                        try
                                        {
                                            show_page = UsersUtility.IsFriendInGroup(group_id, GetUserId());

                                            if (!show_page)
                                            {
                                                h2Title.InnerText = "This page is Viewable by a specific Friends Group";

                                                tdActionText.InnerHtml = "Send a message to your friend and let them know you cannot see the " +
                                                    "page. Ask them to put you in the appropriate friend group.";

                                                tdPolicyText.InnerHtml = privacy_settings;

                                                btnAction.Text = "Send Message";
                                                btnAction.Attributes.Add("onclick", "location.href='" + ResolveUrl("~/myKaneva/newMessage.aspx?userId=" + _channelOwnerId.ToString()) + "';return false;");

                                                divAccessMessage.Visible = true;
                                            }
                                        }
                                        catch (Exception)
                                        {
                                            show_page = false;
                                            if (!show_page)
                                            {
                                                h2Title.InnerText = "This Member?s Profile is Viewable by Friends Only";

                                                tdActionText.InnerText = "This member has set their profile to be viewable by \"friends only\".  If " +
                                                    "you would like to be a friend, you can send a Friend Request to this member.  Once approved " +
                                                    "by the member, you will be given access to their profile.";

                                                tdPolicyText.InnerHtml = privacy_settings;

                                                btnAction.Text = "Friend Request";
                                                btnAction.Attributes.Add("onclick", "javascript:if (confirm('Send a friend request to this user?') ) { AddFriend(); } else { return false; }");

                                                divAccessMessage.Visible = true;
                                            }
                                        }
                                    }
                                }
                                break;

                            // Members Only
                            case (int)Constants.ePAGE_ACCESS.MEMBERS:

                                // Testing to see if the user's membership is pending.
                                CommunityFacade communityFacade = new CommunityFacade();
                                CommunityMember communityMember = communityFacade.GetCommunityMember(_channelId, GetUserId());

                                if (communityMember.StatusId == (UInt32)CommunityMember.CommunityMemberStatus.PENDING)
                                {
                                    h2Title.InnerText = "Community is Viewable by Members Only";

                                    tdActionText.InnerHtml = "The community you are trying to access has been set to \"Members Only\" " +
                                        "by its owner and is currently not available for public viewing.";

                                    tdPolicyText.InnerHtml = string.Empty;

                                    tdPolicyText.InnerHtml = privacy_settings;

                                    btnAction.Text = "Membership Pending";
                                    btnAction.Enabled = false;

                                    divAccessMessage.Visible = true;
                                    return;
                                }

                                // if user is not logged in, show message and link to login
                                if (!Request.IsAuthenticated)
                                {
                                    show_page = false;		   // Members Only page, user not logged in

                                    h2Title.InnerText = "Community is Viewable by Members Only";

                                    tdActionText.InnerHtml = "This community is set to be viewable by \"members only\".  If you are a member " +
                                        "of this community, sign into Kaneva and you will be granted access to this community.  If you are " +
                                        "not a member, join Kaneva and you will be given the opportunity to join this community.";

                                    tdPolicyText.InnerHtml = community_guidelines;

                                    btnAction.Text = " Sign in ";
                                    btnAction.Attributes.Add("onclick", "location.href='" + GetLoginURL() + "';return false;");

                                    divAccessMessage.Visible = true;
                                }
                                else
                                {
                                    // if any member can view this page and this user is a member
                                    if (group_id == 0)
                                    {
                                        show_page = CommunityUtility.IsActiveCommunityMember(_channelId, GetUserId());

                                        if (!show_page)	// Not a community member
                                        {
                                            h2Title.InnerText = "Community is Viewable by Members Only";

                                            tdActionText.InnerHtml = "The community you are trying to access has been set to \"Members Only\" " +
                                                "by its owner and is currently not available for public viewing.";

                                            tdPolicyText.InnerHtml = string.Empty;

                                            tdPolicyText.InnerHtml = privacy_settings;

                                            btnAction.Text = "Request Membership";
                                            btnAction.Attributes.Add("onclick", "javascript:if (confirm('Join this community?') ) { JoinChannel(); return false; } else { return false; }");

                                            divAccessMessage.Visible = true;
                                        }
                                    }
                                    else
                                    {

                                        // check if user is part of member group
                                        try
                                        {
                                            show_page = CommunityUtility.IsMemberInGroup(group_id, GetUserId());
                                            if (!show_page)
                                            {
                                                h2Title.InnerText = "This page is Viewable by a Specific Member Group";

                                                tdActionText.InnerHtml = "Send a message to the Community Owner and let them know you cannot " +
                                                    "see the page. Ask them to put you in the appropriate member group.";

                                                tdPolicyText.InnerHtml = privacy_settings;

                                                btnAction.Text = "Send Message";
                                                btnAction.Attributes.Add("onclick", "location.href='" + ResolveUrl("~/myKaneva/newMessage.aspx?userId=" + _channelOwnerId.ToString()) + "';return false;");

                                                divAccessMessage.Visible = true;
                                            }
                                        }
                                        catch (Exception)
                                        {
                                            show_page = false;
                                            if (!show_page)
                                            {
                                                h2Title.InnerText = "Community is Viewable by Members Only";

                                                tdActionText.InnerHtml = "The community you are trying to access has been set to \"Members Only\" " +
                                                    "by its owner and is currently not available for public viewing.";

                                                tdPolicyText.InnerHtml = string.Empty;

                                                btnAction.Text = "Request Membership";
                                                btnAction.Attributes.Add("onclick", "javascript:if (confirm('Join this community?') ) { JoinChannel(); return false; } else { return false; }");

                                                divAccessMessage.Visible = true;
                                            }
                                        }
                                    }
                                }
                                break;

                            // private
                            case (int)Constants.ePAGE_ACCESS.PRIVATE:
                                show_page = false;
                                string page_type = _isPersonal ? "profile" : "community";

                                h2Title.InnerText = "This Item is Private and Viewable by the Owner only";

                                tdActionText.InnerText = "The " + page_type + " you are trying to access has been set to \"Private\" by its owner " +
                                    "and is currently not available for public viewing.";

                                tdPolicyText.InnerHtml = privacy_settings;

                                btnAction.Attributes.Add("onclick", "javascript:history.back();return false;");
                                btnAction.Text = "  Go Back  ";

                                divAccessMessage.Visible = true;
                                break;

                            default:
                                break;
                        }
                    }
                }
            }


            if (show_page)
            {
                double left_percentage = PageUtility.GetLayoutPageLeftPercentage(PageId);


                int left_width = (int)(left_percentage * (double)Constants.eZONE_WIDTHS.ZONE_HEADER + 0.5);
                int right_width = (int)Constants.eZONE_WIDTHS.ZONE_HEADER - left_width;

                LoadWidgetControls(left_width, right_width);

                chanBodyZone.Attributes.Add("style", "margin:auto; width:" + left_width + "px");
                chanColumnZone.Attributes.Add("style", "margin:auto; width:" + right_width + "px");


                // Load theme
                int templateId = Convert.ToInt32(drChannel["template_id"]);
                LoadTheme(templateId);

                // Update my profile views
                if (GetUserId() != _channelOwnerId)
                {
                    //only do this when the page is viewed by other users
                    CommunityFacade communityFacade = new CommunityFacade();
                    communityFacade.UpdateChannelViews(GetUserId(), _channelId, KanevaWebGlobals.CurrentUser.BrowseAnonymously, Request.UserHostAddress);
                }

                // Add a bread crumb
                //ResetBreadCrumb();
                AddBreadCrumb(new BreadCrumb("community: " + _channelName, GetCurrentURL(), "", 0));
            }
        }

        // Page Nav
        #region Page Nav

        private void SetChannelProperties()
        {
            DataRow drChannel = CommunityUtility.GetCommunity(_channelId);
            if (drChannel != null)
            {
                ChannelName = drChannel["name"].ToString();
            }
        }

        public string ChannelName
        {
            get
            {
                if (ViewState["ChannelName"] != null)
                {
                    return ViewState["ChannelName"].ToString();
                }
                else
                {
                    return "";
                }
            }
            set
            {
                ViewState["ChannelName"] = value;
            }
        }

        #endregion

        #region Helper Methods
        private void SetNav(bool isPersonal)
        {
            bool isAdmin = false;
            bool isMyProfile = false;

            //setup header nav bar
            if (_channelId == this.GetPersonalChannelId())
            {
                isMyProfile = true;
            }
            else
            {
                if (IsAdministrator())
                {
                    isAdmin = true;
                }
            }

            if ((isAdmin && isPersonal) || isMyProfile)
            {
                ((IndexPageTemplate)Master).HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
                ((IndexPageTemplate)Master).HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.PROFILE;
                ((IndexPageTemplate)Master).HeaderNav.MyChannelsNav.PageId = PageId;
                ((IndexPageTemplate)Master).HeaderNav.MyChannelsNav.ChannelId = _channelId;
                ((IndexPageTemplate)Master).HeaderNav.MyChannelsNav.ActiveTab = NavMyChannels.TAB.EDIT;
                ((IndexPageTemplate)Master).HeaderNav.SetNavVisible(((IndexPageTemplate)Master).HeaderNav.MyKanevaNav, 2);
                ((IndexPageTemplate)Master).HeaderNav.SetNavVisible(((IndexPageTemplate)Master).HeaderNav.MyChannelsNav);
            }
            else if ((isAdmin && !isPersonal) || (CommunityUtility.IsCommunityModerator(_channelId, GetUserId()) && !isPersonal))
            {
                ((IndexPageTemplate)Master).HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
                ((IndexPageTemplate)Master).HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.CHANNELS;
                ((IndexPageTemplate)Master).HeaderNav.MyChannelsNav.PageId = PageId;
                ((IndexPageTemplate)Master).HeaderNav.MyChannelsNav.ChannelId = _channelId;
                ((IndexPageTemplate)Master).HeaderNav.MyChannelsNav.ActiveTab = NavMyChannels.TAB.EDIT;
                ((IndexPageTemplate)Master).HeaderNav.SetNavVisible(((IndexPageTemplate)Master).HeaderNav.MyKanevaNav, 2);
                ((IndexPageTemplate)Master).HeaderNav.SetNavVisible(((IndexPageTemplate)Master).HeaderNav.MyChannelsNav);
            }
            else if (!isPersonal)
            {
                if (Request.QueryString["communityID"] == "1118")
                {
                    ((IndexPageTemplate)Master).HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.PLAY;
                }
                else
                {
                    ((IndexPageTemplate)Master).HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.CONNECT;
                }
            }
            else
            {
                ((IndexPageTemplate)Master).HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.CONNECT;
            }

        }

        /// <summary>
        /// GetRequestParams
        /// </summary>
        private bool GetRequestParams()
        {
            bool retVal = true;
            try
            {
                if (Request["pageId"] == null || Request["pageId"] == "" || Request["pageId"] == "0")
                {
                    if (Request["communityId"] != null)
                    {
                        //get channelId and find the default page
                        _channelId = Convert.ToInt32(Request["communityId"]);
                        if (CommunityUtility.IsCommunityValid(_channelId))
                        {
                            _pageId = PageUtility.GetChannelDefaultPageId(_channelId);
                        }
                        else
                        {
                            retVal = false;
                        }
                    }
                    else
                    {
                        //no channel id passed, get personal channelId if user logged in
                        if (Request.IsAuthenticated)
                        {
                            _channelId = GetPersonalChannelId();
                            _pageId = PageUtility.GetChannelDefaultPageId(_channelId);
                        }
                        else
                        {
                            retVal = false;
                        }
                    }
                }
                else
                {
                    if (Request["pageId"] == "-1")
                    {
                        _channelId = Convert.ToInt32(Request["communityId"]);
                        CreateNewPage();
                    }
                    else
                    {
                        _pageId = Convert.ToInt32(Request["pageId"]);
                        DataRow drChannel = PageUtility.GetChannelByPageId(_pageId);
                        _channelId = Convert.ToInt32(drChannel["community_id"]);
                    }
                }

                // setup for page nav
                SetChannelProperties();
            }
            catch (Exception)
            {
                //invalid numbers
                retVal = false;
            }
            return retVal;
        }

        private void LoadWidgetControls(int left_width, int right_width)
        {
            placeHolderHeader.Controls.Clear();
            placeHolderColumnTop.Controls.Clear();
            placeHolderColumn.Controls.Clear();
            placeHolderBody.Controls.Clear();
            placeHolderFooter.Controls.Clear();

            LoadWidgets();

            foreach (DictionaryEntry entry in _widgets)
            {
                int key = Int32.Parse(entry.Key.ToString());
                ArrayList list = (ArrayList)entry.Value;
                switch (key)
                {
                    case (int)Constants.eMODULE_ZONE.HEADER:
                        LoadZoneWidgets(placeHolderHeader, list, (int)Constants.eZONE_WIDTHS.ZONE_HEADER, Constants.eMODULE_ZONE.HEADER);
                        break;

                    // footer has same width as header
                    case (int)Constants.eMODULE_ZONE.FOOTER:
                        LoadZoneWidgets(placeHolderFooter, list, (int)Constants.eZONE_WIDTHS.ZONE_HEADER, Constants.eMODULE_ZONE.FOOTER);
                        break;

                    case (int)Constants.eMODULE_ZONE.BODY:  // left
                        LoadZoneWidgets(placeHolderBody, list, left_width, Constants.eMODULE_ZONE.BODY);
                        break;

                    case (int)Constants.eMODULE_ZONE.BODY_TOP: // top right
                        LoadZoneWidgets(placeHolderColumnTop, list, left_width, Constants.eMODULE_ZONE.BODY_TOP);
                        break;

                    case (int)Constants.eMODULE_ZONE.COLUMN: // right
                        LoadZoneWidgets(placeHolderColumn, list, right_width, Constants.eMODULE_ZONE.COLUMN);
                        break;

                    default:
                        break;
                }
            }
        }

        /// <summary>
        /// LoadTheme
        /// </summary>
        /// <param name="templateId"></param>
        private void LoadTheme(int templateId)
        {
            const int defaultTemplate = 1;
            DataRow drStandardTemplate;

            // Get the correct standard template (theme)
            if (templateId.Equals(0))
            {
                // They have not selected a theme
                drStandardTemplate = WidgetUtility.GetStandardTemplate(defaultTemplate);
                LoadStyleSheet(drStandardTemplate);
            }
            else
            {
                // Load any custimizations
                DataRow drTemplate = WidgetUtility.GetTemplate(templateId);

                if (drTemplate != null)
                {
                    drStandardTemplate = WidgetUtility.GetStandardTemplate(Convert.ToInt32(drTemplate["standard_template_id"]));
                    LoadStyleSheet(drStandardTemplate);

                    Literal litCustomStyle = new Literal();
                    litCustomStyle.Text = "<style type=\"text/css\">\n<!--\n";

                    // Base font
                    if (!drTemplate["base_font"].Equals(DBNull.Value))
                    {
                        DataRow drBaseFont = WebCache.GetFont(Convert.ToInt32(drTemplate["base_font"]));

                        if (drBaseFont != null)
                        {
                            if (!drBaseFont["name"].Equals(DBNull.Value))
                            {
                                litCustomStyle.Text += "#chanPage .widgetText {font-family:" + drBaseFont["name"].ToString() + ";}\n";
                            }
                            if (!drBaseFont["rgb"].Equals(DBNull.Value))
                            {
                                litCustomStyle.Text += "#chanPage .widgetText {color:" + drBaseFont["rgb"].ToString() + ";}\n";
                                litCustomStyle.Text += "#chanPage .widgetTextBold {color:" + drBaseFont["rgb"].ToString() + ";}\n";
                            }
                            if (!drBaseFont["pixel_size"].Equals(DBNull.Value))
                            {
                                litCustomStyle.Text += "#chanPage .widgetText {font-size:" + drBaseFont["pixel_size"].ToString() + "px;}\n";
                            }
                        }
                    }

                    // Base Link
                    if (!drTemplate["base_link"].Equals(DBNull.Value))
                    {
                        DataRow drBaseLink = WebCache.GetFont(Convert.ToInt32(drTemplate["base_link"]));

                        if (drBaseLink != null)
                        {
                            if (!drBaseLink["rgb"].Equals(DBNull.Value))
                            {
                                litCustomStyle.Text += "#chanPage A:link{color:" + drBaseLink["rgb"].ToString() + ";}\n";
                            }
                        }
                    }

                    // Base visited
                    if (!drTemplate["base_visited"].Equals(DBNull.Value))
                    {
                        DataRow drBaseVisited = WebCache.GetFont(Convert.ToInt32(drTemplate["base_visited"]));

                        if (drBaseVisited != null)
                        {
                            if (!drBaseVisited["rgb"].Equals(DBNull.Value))
                            {
                                litCustomStyle.Text += "#chanPage A:visited{color:" + drBaseVisited["rgb"].ToString() + ";}\n";
                            }
                        }
                    }

                    // Base hover
                    if (!drTemplate["base_hover"].Equals(DBNull.Value))
                    {
                        DataRow drBaseHover = WebCache.GetFont(Convert.ToInt32(drTemplate["base_hover"]));

                        if (drBaseHover != null)
                        {
                            if (!drBaseHover["rgb"].Equals(DBNull.Value))
                            {
                                litCustomStyle.Text += "#chanPage A:hover{color:" + drBaseHover["rgb"].ToString() + ";}\n";
                            }
                        }
                    }

                    // Background color
                    if (!drTemplate["background_rgb"].Equals(DBNull.Value))
                    {
                        litCustomStyle.Text += "body{background-color:" + drTemplate["background_rgb"].ToString() + ";}\n";
                    }

                    // Background picture
                    if (!drTemplate["background_picture"].Equals(DBNull.Value))
                    {
                        litCustomStyle.Text += "body{background-image:url(" + drTemplate["background_picture"].ToString() + ");}\n";
                    }

                    // Background repeat
                    if (!drTemplate["repeat_html"].Equals(DBNull.Value))
                    {
                        litCustomStyle.Text += drTemplate["repeat_html"].ToString() + "\n";
                    }

                    // Background orientation
                    if (!drTemplate["background_orientation"].Equals(DBNull.Value))
                    {

                    }

                    // Background fixed
                    if (!drTemplate["background_fixed"].Equals(DBNull.Value))
                    {

                    }

                    // Widget Settings

                    // Outer Border
                    if (!drTemplate["module_border_rgb"].Equals(DBNull.Value))
                    {
                        litCustomStyle.Text += "#chanPage .outerFrame * {BACKGROUND:" + drTemplate["module_border_rgb"].ToString() + ";border-right:1px solid " + drTemplate["module_border_rgb"].ToString() + ";border-left:1px solid " + drTemplate["module_border_rgb"].ToString() + ";}\n";
                        litCustomStyle.Text += "#chanPage .outerFrame_content {BACKGROUND:" + drTemplate["module_border_rgb"].ToString() + ";}\n";
                    }

                    if (!drTemplate["module_border_width"].Equals(DBNull.Value))
                    {
                        if (drTemplate["module_border_width"].ToString() != "0px")
                        {
                            litCustomStyle.Text += "#chanPage .outerFrame * {display:block;}";

                            string topWidth, sideWidth;

                            switch (drTemplate["module_border_width"].ToString())
                            {
                                case "0px":
                                    topWidth = "0px";
                                    sideWidth = "0px";
                                    break;
                                case "1px":
                                    topWidth = "1px";
                                    sideWidth = "5px";
                                    break;
                                case "2px":
                                    topWidth = "2px";
                                    sideWidth = "6px";
                                    break;
                                case "3px":
                                    topWidth = "3px";
                                    sideWidth = "7px";
                                    break;
                                case "4px":
                                    topWidth = "4px";
                                    sideWidth = "8px";
                                    break;
                                case "5px":
                                    topWidth = "5px";
                                    sideWidth = "10px";
                                    break;
                                default:
                                    topWidth = "4px";
                                    sideWidth = "8px";
                                    break;
                            }

                            litCustomStyle.Text += "#chanPage .outerFrame_content {padding:" + topWidth + " " + sideWidth +
                                ";background:" + drTemplate["module_border_rgb"].ToString() + ";border-width=0px;}";
                        }
                        else
                        {
                            litCustomStyle.Text += "#chanPage .outerFrame * {Display: None;}";
                            litCustomStyle.Text += "#chanPage .outerFrame_content {padding:0px;background:transparent;border-width=0px;}";
                        }
                    }

                    if (!drTemplate["module_border_style"].Equals(DBNull.Value))
                    {
                        litCustomStyle.Text += "#chanHeaderZone .innerFrame_content {border-style:" + drTemplate["module_border_style"].ToString() + ";}\n";
                        litCustomStyle.Text += "#chanFooterZone .innerFrame_content {border-style:" + drTemplate["module_border_style"].ToString() + ";}\n";
                        litCustomStyle.Text += "#chanBodyZone .innerFrame_content {border-style:" + drTemplate["module_border_style"].ToString() + ";}\n";
                        litCustomStyle.Text += "#chanColumnZone .innerFrame_content {border-style:" + drTemplate["module_border_style"].ToString() + ";}\n";
                    }

                    // Widget Header
                    if (!drTemplate["module_header_rgb"].Equals(DBNull.Value))
                    {
                        litCustomStyle.Text += "#widgetHeader .innerFrameTitle_content {BACKGROUND:" + drTemplate["module_header_rgb"].ToString() + ";}\n";
                        litCustomStyle.Text += "#widgetHeader .innerFrame * {BACKGROUND:" + drTemplate["module_header_rgb"].ToString() + ";border-right:1px solid " + drTemplate["module_header_rgb"].ToString() + ";border-left:1px solid " + drTemplate["module_header_rgb"].ToString() + ";}\n";
                    }

                    // Widget Body
                    if (!drTemplate["module_background_rgb"].Equals(DBNull.Value))
                    {
                        litCustomStyle.Text += "#widgetBody .innerFrame_content {BACKGROUND:" + drTemplate["module_background_rgb"].ToString() + ";}\n";
                        litCustomStyle.Text += "#widgetBody .innerFrame * {BACKGROUND:" + drTemplate["module_background_rgb"].ToString() + ";;border-right:1px solid " + drTemplate["module_background_rgb"].ToString() + ";border-left:1px solid " + drTemplate["module_background_rgb"].ToString() + ";}\n";
                    }

                    // Widget Body Picture
                    if (!drTemplate["module_picture"].Equals(DBNull.Value))
                    {
                        litCustomStyle.Text += "#chanHeaderZone .innerFrame_content {background:url(" + drTemplate["module_picture"].ToString() + ");}\n";
                        litCustomStyle.Text += "#chanFooterZone .innerFrame_content {background:url(" + drTemplate["module_picture"].ToString() + ");}\n";
                        litCustomStyle.Text += "#chanBodyZone .innerFrame_content {background:url(" + drTemplate["module_picture"].ToString() + ");}\n";
                        litCustomStyle.Text += "#chanColumnZone .innerFrame_content {background:url(" + drTemplate["module_picture"].ToString() + ");}\n";
                    }

                    // Opacity
                    if (!drTemplate["opacity"].Equals(DBNull.Value))
                    {
                        litCustomStyle.Text += "#pageOpacity{filter:alpha(opacity=" + drTemplate["opacity"].ToString() + ")-moz-opacity:." + drTemplate["opacity"].ToString() + ";opacity:." + drTemplate["opacity"].ToString() + ";}\n";
                    }

                    // Custom CSS
                    if (!drTemplate["custom_css"].Equals(DBNull.Value))
                    {
                        string strCustomStyle = Server.HtmlDecode(drTemplate["custom_css"].ToString()) + "\n";
                        litCustomStyle.Text += strCustomStyle;

                        // Check for Standards Mode Flag In 
                        if (strCustomStyle.Contains("standards=true"))
                        {
                            //StandardsMode = true;
                        }
                    }

                    litCustomStyle.Text += "-->\n</style>";
                    phStyleCustomizes.Controls.Add(litCustomStyle);
                }
            }
        }

        private void LoadStyleSheet(DataRow drStandardTemplate)
        {
            if (drStandardTemplate != null)
            {
                string _styleSheets = "<LINK href=\"" + drStandardTemplate["style_sheet"].ToString() + "\" type=\"text/css\" rel=\"stylesheet\">";
                ((IndexPageTemplate)Master).CustomCSS = _styleSheets;

                //Literal litStyleSheet = new Literal();
                //litStyleSheet.Text = _styleSheets;

                //phStyleSheet.Controls.Clear();
                //phStyleSheet.Controls.Add(litStyleSheet);

            }
        }

        private void LoadZoneWidgets(PlaceHolder zone, ArrayList widgets, int width, Constants.eMODULE_ZONE moduleZone)
        {
            widgets.Sort(); //sort by sequence
            foreach (BasePageModule module in widgets)
            {
                ModuleViewBaseControl control = null;
                switch (module.ModuleId)
                {
                    case (int)Constants.eMODULE_TYPE.TITLE_TEXT:
                        control = (ModuleViewBaseControl)LoadControl("../mykaneva/widgets/ModuleTitleView.ascx");
                        zone.Controls.Add(control);
                        break;

                    case (int)Constants.eMODULE_TYPE.BLOGS:
                        control = (ModuleViewBaseControl)LoadControl("../mykaneva/widgets/ModuleBlogView.ascx");
                        zone.Controls.Add(control);
                        break;

                    case (int)Constants.eMODULE_TYPE.PROFILE:
                        control = (ModuleViewBaseControl)LoadControl("../mykaneva/widgets/ModuleProfileView.ascx");
                        zone.Controls.Add(control);
                        break;

                    case (int)Constants.eMODULE_TYPE.FAME_PANEL:
                        control = (ModuleViewBaseControl)LoadControl("../mykaneva/widgets/ModuleFameView.ascx");
                        zone.Controls.Add(control);
                        break;

                    case (int)Constants.eMODULE_TYPE.FRIENDS:
                        control = (ModuleViewBaseControl)LoadControl("../mykaneva/widgets/ModuleFriendsView.ascx");
                        zone.Controls.Add(control);
                        break;

                    case (int)Constants.eMODULE_TYPE.CHANNELS:
                        control = (ModuleViewBaseControl)LoadControl("../mykaneva/widgets/ModuleChannelView.ascx");
                        ((ModuleChannelView)control).UserId = this.GetUserId();
                        zone.Controls.Add(control);
                        break;

                    case (int)Constants.eMODULE_TYPE.MENU:
                        control = (ModuleViewBaseControl)LoadControl("../mykaneva/widgets/ModuleMenuView.ascx");
                        zone.Controls.Add(control);
                        break;

                    case (int)Constants.eMODULE_TYPE.COMMENTS:
                        control = (ModuleViewBaseControl)LoadControl("../mykaneva/widgets/ModuleCommentsView.ascx");
                        zone.Controls.Add(control);
                        break;

                    case (int)Constants.eMODULE_TYPE.USER_UPLOAD:
                        control = (ModuleViewBaseControl)LoadControl("../mykaneva/widgets/ModuleUserUploadView.ascx");
                        zone.Controls.Add(control);
                        break;

                    case (int)Constants.eMODULE_TYPE.HTML_CONTENT:
                        control = (ModuleViewBaseControl)LoadControl("../mykaneva/widgets/ModuleHtmlView.ascx");
                        zone.Controls.Add(control);
                        break;

                    case (int)Constants.eMODULE_TYPE.HTML_CONTENT_BASIC:
                        control = (ModuleViewBaseControl)LoadControl("../mykaneva/widgets/ModuleHtml2View.ascx");
                        zone.Controls.Add(control);
                        break;

                    case (int)Constants.eMODULE_TYPE.SYSTEM_STATS:
                        control = (ModuleViewBaseControl)LoadControl("../mykaneva/widgets/ModuleStatsView.ascx");
                        zone.Controls.Add(control);
                        break;

                    case (int)Constants.eMODULE_TYPE.MY_COUNTER:
                        control = (ModuleViewBaseControl)LoadControl("../mykaneva/widgets/ModuleCounterView.ascx");
                        zone.Controls.Add(control);
                        break;

                    case (int)Constants.eMODULE_TYPE.MY_PICTURE:
                        control = (ModuleViewBaseControl)LoadControl("../mykaneva/widgets/ModuleMyPictureView.ascx");
                        zone.Controls.Add(control);
                        break;

                    case (int)Constants.eMODULE_TYPE.NEW_PEOPLE:
                        control = (ModuleViewBaseControl)LoadControl("../mykaneva/widgets/ModuleNewPeopleView.ascx");
                        zone.Controls.Add(control);
                        break;

                    case (int)Constants.eMODULE_TYPE.MULTIPLE_PICTURES:
                        control = (ModuleViewBaseControl)LoadControl("../mykaneva/widgets/ModuleMultiplePicturesView.ascx");
                        zone.Controls.Add(control);
                        break;

                    case (int)Constants.eMODULE_TYPE.SLIDE_SHOW:
                        control = (ModuleViewBaseControl)LoadControl("../mykaneva/widgets/ModuleSlideShowView.ascx");
                        zone.Controls.Add(control);
                        break;

                    case (int)Constants.eMODULE_TYPE.GAMES_PLAYER:
                        control = (ModuleViewBaseControl)LoadControl("../mykaneva/widgets/ModuleStoreView.ascx");
                        zone.Controls.Add(control);
                        break;

                    case (int)Constants.eMODULE_TYPE.MUSIC_PLAYER:
                        control = (ModuleViewBaseControl)LoadControl("../mykaneva/widgets/ModuleStoreView.ascx");
                        zone.Controls.Add(control);
                        break;

                    case (int)Constants.eMODULE_TYPE.VIDEO_PLAYER:
                        control = (ModuleViewBaseControl)LoadControl("../mykaneva/widgets/ModuleStoreView.ascx");
                        zone.Controls.Add(control);
                        break;

                    case (int)Constants.eMODULE_TYPE.SINGLE_PICTURE:
                        control = (ModuleViewBaseControl)LoadControl("../mykaneva/widgets/ModuleSinglePictureView.ascx");
                        zone.Controls.Add(control);
                        break;

                    case (int)Constants.eMODULE_TYPE.CONTROL_PANEL:
                        control = (ModuleViewBaseControl)LoadControl("../mykaneva/widgets/ModuleControlPanelView.ascx");
                        zone.Controls.Add(control);
                        break;

                    case (int)Constants.eMODULE_TYPE.MIXED_MEDIA:
                        control = (ModuleViewBaseControl)LoadControl("../mykaneva/widgets/ModuleMixedMediaView.ascx");
                        zone.Controls.Add(control);
                        break;

                    case (int)Constants.eMODULE_TYPE.CHANNEL_OWNER:
                        control = (ModuleViewBaseControl)LoadControl("../mykaneva/widgets/ModuleMyPictureView.ascx");
                        zone.Controls.Add(control);
                        break;

                    case (int)Constants.eMODULE_TYPE.CHANNEL_MEMBERS:
                        control = (ModuleViewBaseControl)LoadControl("../mykaneva/widgets/ModuleChannelMembersView.ascx");
                        zone.Controls.Add(control);
                        break;

                    case (int)Constants.eMODULE_TYPE.CHANNEL_EVENTS:
                        control = (ModuleViewBaseControl)LoadControl("../mykaneva/widgets/ModuleEventsView.ascx");
                        zone.Controls.Add(control);
                        break;

                    case (int)Constants.eMODULE_TYPE.CHANNEL_FORUM:
                        control = (ModuleViewBaseControl)LoadControl("../mykaneva/widgets/ModuleChannelForumView.ascx");
                        zone.Controls.Add(control);
                        break;

                    case (int)Constants.eMODULE_TYPE.CHANNEL_CONTROL_PANEL:
                        control = (ModuleViewBaseControl)LoadControl("../mykaneva/widgets/ModuleChannelControlPanelView.ascx");
                        zone.Controls.Add(control);
                        break;

                    case (int)Constants.eMODULE_TYPE.CHANNEL_DESCRIPTION:
                        control = (ModuleViewBaseControl)LoadControl("../mykaneva/widgets/ModuleChannelDescriptionView.ascx");
                        zone.Controls.Add(control);
                        break;

                    case (int)Constants.eMODULE_TYPE.OMM_VIDEO_PLAYER:
                        control = (ModuleViewBaseControl)LoadControl("../mykaneva/widgets/ModuleMediaView.ascx");
                        zone.Controls.Add(control);
                        break;

                    case (int)Constants.eMODULE_TYPE.OMM_VIDEO_PLAYLIST:
                        control = (ModuleViewBaseControl)LoadControl("../mykaneva/widgets/ModuleMediaPlaylistView.ascx");
                        zone.Controls.Add(control);
                        break;

                    case (int)Constants.eMODULE_TYPE.OMM_MUSIC_PLAYER:
                        control = (ModuleViewBaseControl)LoadControl("../mykaneva/widgets/ModuleMediaView.ascx");
                        zone.Controls.Add(control);
                        break;

                    case (int)Constants.eMODULE_TYPE.OMM_MUSIC_PLAYLIST:
                        control = (ModuleViewBaseControl)LoadControl("../mykaneva/widgets/ModuleMediaPlaylistView.ascx");
                        zone.Controls.Add(control);
                        break;

                    case (int)Constants.eMODULE_TYPE.TOP_CONTRIBUTORS:
                        control = (ModuleViewBaseControl)LoadControl("../mykaneva/widgets/ModuleChannelTopContributorsView.ascx");
                        zone.Controls.Add(control);
                        break;

                    case (int)Constants.eMODULE_TYPE.CONTEST_UPLOAD:
                        control = (ModuleViewBaseControl)LoadControl("../mykaneva/widgets/ModuleContestUploadView.ascx");
                        zone.Controls.Add(control);
                        break;

                    case (int)Constants.eMODULE_TYPE.CONTEST_SUBMISSIONS:
                        control = (ModuleViewBaseControl)LoadControl("../mykaneva/widgets/ModuleContestSubmissionsView.ascx");
                        zone.Controls.Add(control);
                        break;

                    case (int)Constants.eMODULE_TYPE.CONTEST_GET_RAVED:
                        control = (ModuleViewBaseControl)LoadControl("../mykaneva/widgets/ModuleContestGetRavedView.ascx");
                        zone.Controls.Add(control);
                        break;

                    case (int)Constants.eMODULE_TYPE.USER_GIFTS:
                        control = (ModuleViewBaseControl)LoadControl("../mykaneva/widgets/ModuleGiftListView.ascx");
                        zone.Controls.Add(control);
                        break;

                    case (int)Constants.eMODULE_TYPE.CONTEST_PROFILE_HEADER:
                        control = (ModuleViewBaseControl)LoadControl("../mykaneva/widgets/ModuleContestProfileHeaderView.ascx");
                        zone.Controls.Add(control);
                        break;

                    case (int)Constants.eMODULE_TYPE.COMMUNITY_TOP_BANNER:
                        control = (ModuleViewBaseControl)LoadControl("../mykaneva/widgets/ModuleCommunityTopBannerView.ascx");
                        zone.Controls.Add(control);
                        break;

                    case (int)Constants.eMODULE_TYPE.LEADER_BOARD:
                        control = (ModuleViewBaseControl)LoadControl("../mykaneva/widgets/ModuleLeaderBoardView.ascx");
                        zone.Controls.Add(control);
                        break;

                    case (int)Constants.eMODULE_TYPE.KANEVA_LEADER_BOARD:
                        control = (ModuleViewBaseControl)LoadControl("../mykaneva/widgets/ModuleKanevaLeaderBoardView.ascx");
                        zone.Controls.Add(control);
                        break;

                    case (int)Constants.eMODULE_TYPE.HOT_NEW_STUFF:
                        control = (ModuleViewBaseControl)LoadControl("../mykaneva/widgets/ModuleHotNewStuffView.ascx");
                        zone.Controls.Add(control);
                        break;

                    case (int)Constants.eMODULE_TYPE.BILLBOARD:
                        control = (ModuleViewBaseControl)LoadControl("../mykaneva/widgets/ModuleBillBoardView.ascx");
                        zone.Controls.Add(control);
                        break;

                    case (int)Constants.eMODULE_TYPE.NEWEST_MEDIA:
                        control = (ModuleViewBaseControl)LoadControl("../mykaneva/widgets/ModuleNewestMediaView.ascx");
                        zone.Controls.Add(control);
                        break;

                    case (int)Constants.eMODULE_TYPE.CONTEST_TOP_RESULTS:
                        control = (ModuleViewBaseControl)LoadControl("../mykaneva/widgets/ModuleContestTopVoteGettersView.ascx");
                        zone.Controls.Add(control);
                        break;

                    case (int)Constants.eMODULE_TYPE.PROFILE_PORTAL:
                        control = (ModuleViewBaseControl)LoadControl("../mykaneva/widgets/ModuleProfilePortalView.ascx");
                        zone.Controls.Add(control);
                        break;

                    case (int)Constants.eMODULE_TYPE.CHANNEL_PORTAL:
                        control = (ModuleViewBaseControl)LoadControl("../mykaneva/widgets/ModuleChannelPortalView.ascx");
                        zone.Controls.Add(control);
                        break;

                    case (int)Constants.eMODULE_TYPE.MOST_VIEWED:
                        control = (ModuleViewBaseControl)LoadControl("../mykaneva/widgets/ModuleMostViewedView.ascx");
                        zone.Controls.Add(control);
                        break;

                    case (int)Constants.eMODULE_TYPE.INTERESTS:
                        control = (ModuleViewBaseControl)LoadControl("../mykaneva/widgets/ModuleInterestsView.ascx");
                        zone.Controls.Add(control);
                        break;

                    case (int)Constants.eMODULE_TYPE.PERSONAL:
                        control = (ModuleViewBaseControl)LoadControl("../mykaneva/widgets/ModulePersonalView.ascx");
                        zone.Controls.Add(control);
                        break;

                    default:
                        break;
                }
                if (control != null)
                {
                    control.ChannelId = _channelId;
                    control.InstanceId = module.Id;
                    control.ShowEdit = _canEdit;
                    control.ProfileOwnerId = _channelOwnerId;
                    control.PageId = PageId;
                    control.ModuleId = module.ModuleId;
                    control.ModuleDeleted += new ModuleViewBaseControl.ModuleDeletedEventHandler(this.HandleModuleDeleted);
                    control.TotalWidth = width;
                    control.Zone = moduleZone;

                    // set delete button
                    switch (module.ModuleId)
                    {
                        case (int)Constants.eMODULE_TYPE.CONTROL_PANEL:
                        case (int)Constants.eMODULE_TYPE.CHANNEL_CONTROL_PANEL:
                            _num_control_panels++;
                            control.ShowDelete = !_isDefault || (_isDefault && _num_control_panels > 1);
                            break;

                        default:
                            control.ShowDelete = true;
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// load all widgets into a dictionary, the key is the zone id and the value is a list of all
        /// widgets in that zone
        /// </summary>
        private void LoadWidgets()
        {
            _widgets.Clear();
            DataTable dtModules = PageUtility.GetPageLayoutModules(PageId);
            foreach (DataRow drModule in dtModules.Rows)
            {
                int modulePageId = Int32.Parse(drModule["module_page_id"].ToString());
                int moduleId = Int32.Parse(drModule["module_id"].ToString());
                int zoneId = Int32.Parse(drModule["zone_id"].ToString());
                int id = Int32.Parse(drModule["id"].ToString());
                int sequence = Int32.Parse(drModule["sequence"].ToString());
                BasePageModule basePageModule = new BasePageModule();
                basePageModule.Id = id;
                basePageModule.ModulePageId = modulePageId;
                basePageModule.ModuleId = moduleId;
                basePageModule.ZoneId = zoneId;
                basePageModule.Sequence = sequence;
                basePageModule.PageId = PageId;

                if (!_widgets.Contains(basePageModule.ZoneId))
                {
                    _widgets.Add(basePageModule.ZoneId, new ArrayList());
                }
                ((ArrayList)_widgets[basePageModule.ZoneId]).Add(basePageModule);
            }
        }

        private void AddFriend()
        {
            if (Request.IsAuthenticated)
            {
                // Add them as a friend
                if (UsersUtility.InsertFriendRequest(GetUserId(), ChannelOwnerId).Equals(0))
                {
                    MailUtilityWeb.SendFriendRequestEmail(GetUserId(), ChannelOwnerId);

                    //					// They were added, send Index tools a message
                    //					Literal litIndexTools = new Literal ();
                    //					litIndexTools.Text = "<script language=\"Javascript\">var ACTION='04';var DOCUMENTNAME='Friend Request';</script>";
                    //					phIndexTools.Controls.Add (litIndexTools);
                }
            }
        }

        private void JoinChannel()
        {
            Response.Redirect(ResolveUrl("~/community/commJoin.aspx?communityId=" + _channelId + "&join=Y"));
        }

        #endregion

        #region Event Handlers
        protected void Add_Friend(Object sender, EventArgs e)
        {
            AddFriend();
        }
        protected void Join_Channel(Object sender, EventArgs e)
        {
            JoinChannel();
        }

        private void HandleModuleDeleted(object sender, ModuleDeletedEventArgs e)
        {
            // simulate 'refresh'
            Response.Redirect(Request.Url.AbsoluteUri);
        }

        #endregion

        #region Properties
        public int ChannelOwnerId
        {
            get { return _channelOwnerId; }
            set { _channelOwnerId = value; }
        }

        public int PageId
        {
            get { return _pageId; }
            set { _pageId = value; }
        }
        #endregion

        #endregion

        #region Layout Function & Events Section


        #region Lay Functions

        private void SaveAll()
        {
            try
            {
                //save the security settings
                SaveSecuritySettings();
            }
            catch(Exception)
            {
                pgLayoutMessages.ForeColor = System.Drawing.Color.DarkRed;
                pgLayoutMessages.Text = "Error! Some or all of your changes failed.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('pgLayoutMessages', 5000);");
            }
        }

        private void SaveSecuritySettings()
        {
            // update layout page 
            PageUtility.UpdateLayoutPage(_pageId, txtPageName.Text, Convert.ToInt32(ddlNewGroup.SelectedValue), Convert.ToInt32(ddlPageAccess.SelectedValue));
        }

        private void GetAvailablePages()
        {
            //get the available pages
            DataTable dtLayoutPages = PageUtility.GetLayoutPages(_channelId);

            // populate the available pages
            ddl_AvailablePages.DataSource = dtLayoutPages;
            ddl_AvailablePages.DataValueField = "page_id";
            ddl_AvailablePages.DataTextField = "name";
            ddl_AvailablePages.SelectedValue = _pageId.ToString();
            ddl_AvailablePages.DataBind();
        }

        private void SetAvailablePageSecurityGroups()
        {
            //this is kind of hacky. we are using the same control to store options that come from two diff 
            //sources
            if (_isPersonal)
            {
                DataTable dtGroups = UsersUtility.GetFriendGroups(_channelOwnerId, "", "name", 1, Int32.MaxValue);
                ddlNewGroup.DataSource = dtGroups;
                ddlNewGroup.DataValueField = "friend_group_id";
                ddlNewGroup.DataTextField = "name";
                ddlNewGroup.DataBind();
            }
            else
            {
                DataTable dtGroups = CommunityUtility.GetMemberGroups(_channelId, "", "name", 1, Int32.MaxValue);
                ddlNewGroup.DataSource = dtGroups;
                ddlNewGroup.DataValueField = "id";
                ddlNewGroup.DataTextField = "name";
                ddlNewGroup.DataBind();
            }

            // add the option to select all to the list of security options
            ddlNewGroup.Items.Insert(0, new ListItem("All", "0"));
            ddlNewGroup.SelectedIndex = 0;
        }

        private void SetAvailablePageSecurityLevels()
        {
            ddlPageAccess.Items.Clear();
            ddlPageAccess.Items.Add(new ListItem("Public", ((int)Constants.ePAGE_ACCESS.PUBLIC).ToString()));
            if (_isPersonal)
            {
                ddlPageAccess.Items.Add(new ListItem("Friends Only", ((int)Constants.ePAGE_ACCESS.FRIENDS).ToString()));
            }
            else
            {
                ddlPageAccess.Items.Add(new ListItem("Members Only", ((int)Constants.ePAGE_ACCESS.MEMBERS).ToString()));
            }
            ddlPageAccess.Items.Add(new ListItem("Private", ((int)Constants.ePAGE_ACCESS.PRIVATE).ToString()));
            ddlPageAccess.Attributes.Add("onclick", "page_detail.ChangePermission()");
        }

        private void GetPageSecurity()
        {
            //set up the available page security groups (friends/or members)
            SetAvailablePageSecurityGroups();

            //set up the available page security levels
            SetAvailablePageSecurityLevels();

            //get the selected pages layout
            DataRow drLayoutPage = PageUtility.GetLayoutPage(_pageId);

            //if the page layout is found
            if (drLayoutPage != null)
            {
                //set the current pages name
                txtPageName.Text = drLayoutPage["name"].ToString();

                //disable delete button if this is the home page
                btnDelete.Enabled = !drLayoutPage["home_page"].ToString().Equals("1");

                //set delete button warnings
                if (_isDefault)
                {
                    btnDelete.Attributes.Add("onclick", "javascript:if (!confirm(\"You can not delete the profile home page.\")){return false;};");
                }
                else
                {
                    btnDelete.Attributes.Add("onclick", "javascript:if (!confirm(\"Are you sure you want to delete this page?\")){return false;};");
                }

                //get and set the access level of the page
                int access_id = Convert.ToInt32(drLayoutPage["access_id"].ToString());
                try
                {
                    ddlPageAccess.SelectedValue = access_id.ToString();
                }
                catch (Exception)
                {
                }

                bool disabled = (access_id == (int)Constants.ePAGE_ACCESS.PUBLIC) || (access_id == (int)Constants.ePAGE_ACCESS.PRIVATE);

                //get and set the group / person allowed if the access level is set to member group/friends group
                string strGroupId = drLayoutPage["group_id"] == DBNull.Value ? "0" : drLayoutPage["group_id"].ToString();
                try
                {
                    ddlNewGroup.SelectedValue = strGroupId;
                    ToggleAccessGroupEnabled(ddlPageAccess.SelectedValue);
                }
                catch (Exception)
                {
                }
            }
        }

        /// <summary>
        /// get all the available widges 
        /// </summary>
        private void GetAvailableWidgets()
        {
            if (_isPersonal)
            {
                dl_WidgetPanels.DataSource = WebCache.GetLayoutModuleGroups(
                    (int)Constants.eMODULE_GROUP_VISIBLE.PERSON, "");
                dl_WidgetPanels.DataBind();
            }
            else
            {
                // 7 is admin only
                string filter = " AND module_group_id <> 7 ";
                if (IsAdministrator())
                {
                    filter = "";
                }

                dl_WidgetPanels.DataSource = WebCache.GetLayoutModuleGroups(
                    (int)Constants.eMODULE_GROUP_VISIBLE.CHANNEL, filter);
                dl_WidgetPanels.DataBind();
            }
        }

        private void CreateNewPage()
        {
            //create a new page using default settings
            int pageId = PageUtility.AddLayoutPage(false, _channelId, "New Page",
                0,
                (int)Constants.ePAGE_ACCESS.PUBLIC, 0);

            if (_channelId == this.GetPersonalChannelId())
            {
                DataRow drTitle = WidgetUtility.GetDefaultModuleTitle(_channelId);
                if (drTitle != null)
                {
                    //copy the title widget from the home page if there is one
                    int module_page_id = WidgetUtility.InsertLayoutModuleTitle(drTitle["text"].ToString(),
                        drTitle["show_menu"].ToString().Equals("1"), drTitle["banner_path"].ToString());

                    if (module_page_id != -1)
                        PageUtility.AddLayoutPageModule(module_page_id, pageId, (int)Constants.eMODULE_TYPE.TITLE_TEXT
                            , (int)Constants.eMODULE_ZONE.HEADER, 1);
                }
            }
            else
            {
                DataRow drCCPanel = WidgetUtility.GetDefaultModuleChannelControlPanel(_channelId);
                if (drCCPanel != null)
                {
                    //copy the title widget from the home page if there is one
                    int module_page_id = WidgetUtility.InsertLayoutModuleChannelControlPanel(drCCPanel["title"].ToString(),
                        drCCPanel["show_menu"].ToString().Equals("1"), drCCPanel["banner_path"].ToString());

                    if (module_page_id != -1)
                        PageUtility.AddLayoutPageModule(module_page_id, pageId, (int)Constants.eMODULE_TYPE.CHANNEL_CONTROL_PANEL
                            , (int)Constants.eMODULE_ZONE.HEADER, 1);
                }
            }

            //redirect to the new page
            Response.Redirect(ResolveUrl("~/channelPage2.aspx?communityId=" + this._channelId + "&pageId=" + pageId));
        }

        private void ToggleAccessGroupEnabled(string pageAccess)
        {
            if ((pageAccess == ((int)Constants.ePAGE_ACCESS.MEMBERS).ToString()) || (pageAccess == ((int)Constants.ePAGE_ACCESS.FRIENDS).ToString()))
            {
                ddlNewGroup.Enabled = true;
            }
            else
            {
                ddlNewGroup.Enabled = false;
                ddlNewGroup.SelectedValue = "0";
            }
        }

        protected void SetPageLayout()
        {
            //StringBuilder msg = new StringBuilder();

            //msg.Append("\nvar widget_page;\n");
            //msg.Append("widget_page = new WidgetPage($('relativeContainer'), WidgetManager, $('lyrGhostModuleBox'), $('lyrModuleSeparator'));\n");
            //msg.Append("widget_page.widget_edit_template = \"window.open('ModuleEditor.aspx?modid=%widget_id%&pageId=" + _pageId + "&fromEdit=true', 'ModuleEditor', 'width=750,height=680,scrollbars=1,resizable=1,status=1,toolbar=0');\"\n");
            //msg.Append("addLoadEvent(InitPage);\n\n");

            //RegisterScripts.RegisterStartupScript(Page, "NewWidgetPage", msg.ToString(), true, 1);

            //// generate zones

            //// wide column (body)
            //HtmlGenericControl divLeftLayoutTop = new HtmlGenericControl("div");
            //divLeftLayoutTop.Style["text-align"] = "center";
            //divLeftLayoutTop.Attributes.Add("align", "center");
            //if (!(_isDefault && _isPersonal))
            //{
            //    divLeftLayoutTop.Style["display"] = "hidden";
            //}

            //HtmlGenericControl divLeftTopChild = new HtmlGenericControl("div");
            //divLeftTopChild.Style["width"] = "100%";
            //divLeftTopChild.Style["text-align"] = "center";
            //divLeftTopChild.Attributes.Add("class", "darkBoldVerdana12");

            //HtmlGenericControl divLeftLayout = new HtmlGenericControl("div");
            //divLeftLayout.Style["text-align"] = "center";
            //divLeftLayout.Attributes.Add("align", "center");

            //HtmlGenericControl divLeftChild = new HtmlGenericControl("div");
            //divLeftChild.Style["width"] = "100%";
            //divLeftChild.Style["text-align"] = "center";
            //divLeftChild.Attributes.Add("class", "darkBoldVerdana12");

            //HtmlGenericControl divRightLayout = new HtmlGenericControl("div");
            //divRightLayout.Style["text-align"] = "center";
            //divRightLayout.Attributes.Add("align", "center");

            //HtmlGenericControl divRightChild = new HtmlGenericControl("div");
            //divRightChild.Style["width"] = "100%";
            //divRightChild.Style["text-align"] = "center";
            //divRightChild.Attributes.Add("class", "darkBoldVerdana12");



            //LiteralControl lBreak = new LiteralControl("<br/>");
            //LiteralControl rBreak = new LiteralControl("<br/>");

            ////divLeftLayout.Attributes.Add("class","columnZone");
            //divLeftLayout.ID = "area_2";
            //divLeftLayout.Style["width"] = "347px";
            ////divLeftChild.InnerText			= "left";

            ////divRightLayout.Attributes.Add("class","columnZone");
            //divRightLayout.ID = "area_3";
            //divRightLayout.Style["width"] = "347px";
            ////divRightChild.InnerText			= "right";

            ////divRightLayoutTop.Attributes.Add("class","columnZone");
            //divLeftLayoutTop.ID = "area_6";
            //divLeftLayoutTop.Style["width"] = "347px";
            ////divRightTopChild.InnerText			= "right";

            //divLeftLayout.Controls.Add(divLeftChild);
            //divLeftLayout.Controls.Add(lBreak);

            //divRightLayout.Controls.Add(divRightChild);
            //divRightLayout.Controls.Add(rBreak);

            //divLeftLayoutTop.Controls.Add(divLeftTopChild);
            //divLeftLayoutTop.Controls.Add(lBreak);

            //tdContentLeft.Controls.Add(divLeftLayoutTop);
            //tdContentLeft.Controls.Add(divLeftLayout);
            //tdContentRight.Controls.Add(divRightLayout);

            //// Add the javascript for changing pages
            //string scriptString = "<script language=\"javaScript\">\n<!--\n function ChangePage (id) {\n";
            //scriptString += "	document.all." + hfChangePageID.ClientID + ".value = id;\n";			// save the page number clicked to the hidden field
            //scriptString += "   widget_page.GetSaveString('hfSaveString');\n";
            //scriptString += "	" + ClientScript.GetPostBackEventReference(hbChangePage, "") + ";\n";		// call the __doPostBack function to post back the form and execute the PageClick event
            //scriptString += "}\n// -->\n";
            //scriptString += "</script>";

            //if (!ClientScript.IsClientScriptBlockRegistered(GetType(), "ChangePage"))
            //{
            //    ClientScript.RegisterClientScriptBlock(GetType(), "ChangePage", scriptString);
            //}

            //// ensure adding a page button saves the layout
            //btnCreate.Attributes.Add("onclick", "widget_page.GetSaveString('hfSaveString');"
            //    + " PManager.SavePositions('" + hfPositions.ClientID + "'); "
            //    + ClientScript.GetPostBackEventReference(btnCreate, "") + ";return false;");


        }

        #endregion

        #region Layout Events

        protected void dl_WidgetPanels_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                //get the needed controls
                DropDownList ddlwidgetList = (DropDownList)e.Item.FindControl("ddl_widgetPanel");
                HtmlInputHidden hdModuleId = (HtmlInputHidden)e.Item.FindControl("hd_ModuleId");

                //Label widgetGroup = (Label)e.Item.FindControl("lbl_widgetGroupName");

                //widgetGroup.Text = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "title"));

                DataTable widgetList = new DataTable();

                //populate the dynamically created drop downs
                //check for profile versus community and retieve the available widget types/groups
                if (_isPersonal)
                {
                    widgetList = WebCache.GetLayoutModules(Convert.ToInt32(hdModuleId.Value), (int)Constants.eMODULE_VISIBLE.PERSON);
                }
                else
                {
                    widgetList = WebCache.GetLayoutModules(Convert.ToInt32(hdModuleId.Value), (int)Constants.eMODULE_VISIBLE.CHANNEL);
                }

                //bind the data do the datalist
                ddlwidgetList.DataSource = widgetList;
                ddlwidgetList.DataValueField = "module_id";
                ddlwidgetList.DataTextField = "title";
                ddlwidgetList.DataBind();

                ddlwidgetList.Items.Insert(0, new ListItem("-SELECT-", "0"));
                ddlwidgetList.SelectedIndex = 0;

            }
        }

        protected void lbnAddWidget_Click(object sender, EventArgs e)
        {
            try
            {
                //first get a reference to the datalist item the link is in
                DataListItem widgetControlsRow = (DataListItem)((LinkButton)sender).NamingContainer;

                //gets the drop down corresponding to the button that was clicked
                DropDownList ddlWidgetPanel = (DropDownList)widgetControlsRow.FindControl("ddl_widgetPanel");

                //gets the id of the widget being selected
                int widgetId = Convert.ToInt32(ddlWidgetPanel.SelectedValue);
                if (widgetId > 0)
                {
                    ///code to add new widget goes here
                }
                else
                {
                    pgLayoutMessages.ForeColor = System.Drawing.Color.DarkRed;
                    pgLayoutMessages.Text = "Please select your desired widget.";
                    MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('pgLayoutMessages', 5000);");
                }
            }
            catch (Exception)
            {
                pgLayoutMessages.ForeColor = System.Drawing.Color.DarkRed;
                pgLayoutMessages.Text = "Unable to process your request.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('pgLayoutMessages', 5000);");
            }
        }

        /// <summary>
        /// Change Pages
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddl_AvailablePages_SelectedIndexChanged(Object sender, EventArgs e)
        {
            // save layout
            SaveAll();

            Response.Redirect("channelPage2.aspx?communityId=" + _channelId + "&pageId=" + ddl_AvailablePages.SelectedValue);
        }

        /// <summary>
        /// Change Pages
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlPageAccess_SelectedIndexChanged(Object sender, EventArgs e)
        {
            ToggleAccessGroupEnabled(ddlPageAccess.SelectedValue);        
        }

        protected void btnDelete_Click(object sender, System.EventArgs e)
        {
            // Make sure this is a page that allows deletion
            if (!_isDefault)
            {
                PageUtility.DeleteLayoutPage(_pageId);
                Response.Redirect(ResolveUrl("~/channelPage2.aspx?communityId=" + this._channelId));
            }
        }

        protected void btnAdd_Click(object sender, System.EventArgs e)
        {
            SaveAll ();

            //create the new page
            CreateNewPage();
        }

        // save the layout -- remove any pages marked for deletion
        // and update the sequence of pages along with the default page
        protected void btnSaveWidgets_Click(object sender, EventArgs e)
        {
            SaveAll ();
        }

        #endregion

        #endregion

        #region Web Form Designer generated code

        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new EventHandler(this.Page_Load);
            this.dl_WidgetPanels.ItemDataBound += new DataListItemEventHandler(dl_WidgetPanels_ItemDataBound);
        }

        #endregion
    }
}
