<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>
<%@ Page language="c#" Codebehind="contestPlayer.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.channel.contestPlayer" %>
<%@ Register TagPrefix="UserControl" TagName="uc_AssetDetails" Src="../usercontrols/uc_AssetDetails.ascx" %>
<HTML>
	<LINK href="../css/kanevaSC.css" type="text/css" rel="stylesheet">
		<LINK href="../css/PageLayout/channel_base.css" type="text/css" rel="stylesheet">
			<body>
				<form id="frmPopUp" runat="server">
					<asp:placeholder id="phStyleSheet" Runat="server"></asp:placeholder><asp:placeholder id="phStyleCustomizes" Runat="server"></asp:placeholder>
					<div id="chanPage">
						<div id="widgetBody">
							<div class="innerFrame_content" align="center">
								<table height="100%" cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
									<tr>
										<!--<td width="500"><asp:literal id="litStream" runat="server"></asp:literal></td>-->
										<td><usercontrol:uc_assetdetails id="ucAssetDetails" runat="server"></usercontrol:uc_assetdetails></td>
										<td id="rightPanel" vAlign="top" runat="server"><ajax:ajaxpanel id="ajContest" runat="server">
												<TABLE id="tbl_contest" cellSpacing="0" cellPadding="8" width="250" border="0" runat="server">
													<TR>
														<TD class="widgetContestPicture">
															<TABLE id="tblValidateNotice" height="100%" cellSpacing="0" cellPadding="0" width="100%"
																align="left" bgColor="#ffffff" runat="server" visible="false">
																<TR>
																	<TD align="left"><B>NOTICE:</B><BR>
																		<BR>
																		You must validate your current email address before casting your vote. You only 
																		have to do this once to continue participating in this competition and all 
																		other Kaneva events.
																	</TD>
																</TR>
																<TR>
																	<TD vAlign="top" align="center" height="10">&nbsp;</TD>
																</TR>
																<TR>
																	<TD vAlign="top" align="center">
																		<asp:button class="Filter2" id="btnRegEmail" runat="Server" CausesValidation="False" Text=" Send Validation "
																			align="right"></asp:button></TD>
																</TR>
																<TR>
																	<TD vAlign="top" align="center" height="10">&nbsp;
																	</TD>
																</TR>
															</TABLE>
															<asp:Image id="imgContestPicture" runat="server" ImageAlign="Middle" EnableViewState="False"
																Width="100%" Height="100%" BorderColor="Transparent" BackColor="Transparent"></asp:Image>
															<asp:Label id="lblValidationMessage" style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px" runat="server"
																Width="100%" Height="100%" BackColor="#ffffff" Visible="False"></asp:Label></TD>
													</TR>
													<TR>
														<TD>
															<TABLE id="tblVoting" cellSpacing="0" cellPadding="8" width="250" border="0" runat="server">
																<TR>
																	<TD align="right" width="125">
																		<asp:imagebutton id="btnVote" style="CURSOR: hand" onclick="btnVoteYes_Click" runat="server" imageurl="~/images/contests/btn_yes.gif"
																			CommandName="Yes" border="0"></asp:imagebutton></TD>
																	<TD align="left" width="125">
																		<asp:imagebutton id="btnVoteNo" style="CURSOR: hand" onclick="btnVoteNo_Click" runat="server" imageurl="~/images/contests/btn_no.gif"
																			CommandName="No" border="0"></asp:imagebutton></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD>
															<P>
																<asp:label id="lblName" runat="server"></asp:label></P>
															<P>
																<asp:label id="lbl_DescArtist" runat="server">Artist</asp:label>:
																<asp:label id="lblArtist" runat="server"></asp:label></P>
															<P>
																<asp:label id="lbl_DescVotes" runat="server">Yes Votes</asp:label>:
																<asp:label id="lblVotes" runat="server"></asp:label></P>
															<P>
																<asp:label id="Label7" runat="server">Uploaded by</asp:label>:
																<asp:label id="lblUsername" runat="server"></asp:label></P>
														</TD>
													</TR>
													<TR>
														<TD>
															<asp:label id="lblVoteStatus" runat="server" CssClass="widgetText" Font-Bold="True"></asp:label><BR>
															<BR>
														</TD>
													</TR>
													<TR>
														<TD align="right"><A onclick="self.parent.TB_remove();return false" href="#"><IMG id="IMG1" src="~/images/contests/btn_close.gif" border="0" runat="server">
																<DIV></DIV>
															</A>
														</TD>
													</TR>
												</TABLE>
												<TABLE id="tbl_general" cellSpacing="0" cellPadding="8" width="250" border="0" runat="server">
													<TR>
														<TD class="widgetText">
															<P>
																<asp:label id="lblName_gen" runat="server" Font-Size="12pt"></asp:label></P>
															<P>
																<asp:label id="lbl_DescArtist_gen" runat="server" Font-Size="12pt">Description</asp:label>:
																<asp:label id="lblArtist_gen" runat="server" Font-Size="12pt"></asp:label></P>
															<P>
																<asp:label id="lbl_DescVotes_gen" runat="server" Font-Size="12pt">Number of Raves</asp:label>:
																<asp:label id="lblVotes_gen" runat="server" Font-Size="12pt"></asp:label></P>
															<P>
																<asp:label id="Label1" runat="server" Font-Size="12pt">Uploaded by</asp:label>:
																<asp:label id="lblUsername_gen" runat="server" Font-Size="12pt"></asp:label></P>
														</TD>
													</TR>
												</TABLE>
											</ajax:ajaxpanel></td>
									</tr>
								</table>
							</div>
						</div>
					</div>
				</form>
			</body>
</HTML>
