///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using log4net;
using MagicAjax;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;


namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for vworld.
	/// </summary>
	public class vworld : StoreBasePage
	{
		protected vworld () 
		{
		}

        private void Page_Load(object sender, System.EventArgs e)
        {
            // This page is no longer valid. Redirect all users to My Kaneva
            Response.Redirect("~/default.aspx");
            
            if (Request.IsAuthenticated)
            {
                AwardFamePoints ();
            }

            // if going to old world fame page, redirect to new world fame.
            if (Request.QueryString["ss"] == "world-of-kaneva-fame" || Request.QueryString["ss"] == "fame-how-to")
            {
                Response.Redirect ("~/newworldfame.aspx");
            }

            //initialize the page 
            InitializePage();
        }

        #region Primary Functions

        private void InitializePage()
        {
            //setup header nav bar
            HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.PLAY;

            Title = "Virtual World of Kaneva. Create Avatars, 3D Spaces and Explore.";
            MetaDataTitle = "<meta name=\"title\" content=\"Kaneva. A Unique Online Community and 3D Social Network.\" />";
            MetaDataDescription = "<meta name=\"description\" content=\"Kaneva�s free virtual world lets you customize your Avatar and your surroundings for a 3D online virtual world experience that is truly unique.  \"/>";
            MetaDataKeywords = "<meta name=\"keywords\" content=\"online community, social network, meet people, friends, networking, games, mmo, movies, 3D, virtual world, sharing photos, video, blog, bands, music, rate pics, digital, rpg, entertainment, join groups, forums, online social networking, caneva, kaneeva, kanneva, kineva\" />";

            // Is user logged in?
            if (Request.IsAuthenticated)
            {
                imgRegister.Alt = "Edit Your Profile";
                imgRegister.Src = ResolveUrl("~/images/header/indexpages/blue-install.png");
                aRegister.HRef = ResolveUrl("~/community/install3d.kaneva");

                // Has user logged into wok?
                if (KanevaWebGlobals.CurrentUser.HasWOKAccount)
                {
                    // Show the wok user heading text
                    /* TODO */
                    arHeader.AdvertisementFile = ResolveUrl("~/xml/virtual-world_headertxt_wokuser.xml");
                }
                else
                {
                    // Show the non-wok user heading text
                    /* TODO */
                    arHeader.AdvertisementFile = ResolveUrl("~/xml/virtual-world_headertxt_notwokuser.xml");

                    imgRegister.Src = ResolveUrl("~/images/header/indexpages/blue-install.png");
                    aRegister.HRef = ResolveUrl("~/community/install3d.kaneva");
                }
            }
            else
            {
                // Show the not logged in heading text
                /* TODO */
                arHeader.AdvertisementFile = ResolveUrl("~/xml/community_headertxt_notloggedin.xml");

                imgRegister.Src = ResolveUrl("~/images/header/indexpages/blue-install.png");
                aRegister.HRef = ResolveUrl("~/community/install3d.kaneva");
            }

            // Bring in the content
            if (Request.QueryString["contentpath"] != null)
            {
                litContent.Text = readFile("../3d-virtual-world/content-files/" + Request.QueryString["contentpath"]);
            }
            else
            {
                litContent.Text = readFile("../3d-virtual-world/content-files/whats-new.html");
            }
        }

        private String readFile(string path)
        {
            string pageContent = (string)Global.Cache()[path];
            if (pageContent == null || pageContent.Length.Equals(0))
            {
                try
                {
                    String result;
                    StreamReader sr = File.OpenText(Server.MapPath(path));
                    {
                        Title = sr.ReadLine();
                        Title = Title.Substring(Title.IndexOf(":") + 1).Trim();

                        MetaDataTitle = sr.ReadLine();
                        MetaDataTitle = MetaDataTitle.Substring(MetaDataTitle.IndexOf(":") + 1).Trim();

                        MetaDataDescription = sr.ReadLine();
                        MetaDataDescription = MetaDataDescription.Substring(MetaDataDescription.IndexOf(":") + 1).Trim();

                        MetaDataKeywords = sr.ReadLine();
                        MetaDataKeywords = MetaDataKeywords.Substring(MetaDataKeywords.IndexOf(":") + 1).Trim();

                        sr.ReadLine();
                        
                        result = sr.ReadToEnd();
                        // Close and clean up the StreamReader
                        sr.Close();
                    }
                    pageContent = result;
                }
                catch (Exception exc)
                {
                    m_logger.Error("Error retrieving file, fname =" + path, exc);
                    pageContent = "";
                }
                // Add to the cache                
                Global.Cache().Insert(path, pageContent, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
            }
            return pageContent;
        }

        private void AwardFamePoints ()
        {
            try
            {
                if (Request.QueryString["ss"] == "world-of-kaneva-fame")
                {
                    FameFacade fameFacade = new FameFacade ();
                    fameFacade.RedeemPacket (KanevaWebGlobals.CurrentUser.UserId, (int) PacketId.WEB_OT_HELP, (int) FameTypes.World);
                }
            }
            catch (Exception ex)
            {
                m_logger.Error ("Error awarding Packet WEB_OT_HELP, userid=" + KanevaWebGlobals.CurrentUser.UserId, ex);
            }
        }

        #endregion Primary Functions


        #region Event Handlers

        protected void AdCreated(Object source, AdCreatedEventArgs e)
        {
            if (e.AdProperties["HeaderText"].ToString() != "")
            {

                litHeaderText.Text = e.AdProperties["HeaderText"].ToString();
                //litHeaderText.Text = "test";
                arHeader.Visible = false;
            }
        }

        #endregion


        #region Properties

        /// <summary>
        /// OwnerId
        /// </summary>
        private int OwnerId 
        {
            get
            {
                if (ViewState["ownerid"] == null)
                {
                    ViewState["ownerid"] = -1;
                }
                return (int)ViewState["ownerid"];
            }
            set
            {
                ViewState["ownerid"] = value;
            }
        }

        # endregion


        #region Declarations

        protected AdRotator arHeader;
        protected Literal litHeaderText, litContent;
        protected HtmlImage imgRegister;
        protected HtmlAnchor aRegister;

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #endregion


        #region Web Form Designer generated code

        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion
    }
}
