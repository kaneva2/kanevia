///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using log4net;
using MagicAjax;

namespace KlausEnt.KEP.Kaneva
{
    public partial class starsDirectory : StoreBasePage
    {
        #region Declarations

        protected Kaneva.Pager pgTop, pgBottom;
        protected Kaneva.SearchFilter filterTop;
        protected Kaneva.usercontrols.TagCloud ucTagCloud;

        protected HyperLink hlChangeProfile;
        protected Button btnSearch, btnRestrictAsset;
        protected TextBox txtKeywords;
        protected LinkButton lbThumbTop, lbDetailTop;
        protected LinkButton liBrowse_Population, lbBrowse_Raves, lbBrowse_Visits, lbBrowse_Adds, lbBrowse_Newest;
        protected LinkButton lbTime_Today, lbTime_Week, lbTime_Month, lbTime_All, lbRestrict;
        protected LinkButton lbType_Videos, lbType_Photos, lbType_Music, lbType_Games, lbType_Patterns, lbType_Widgets, lbType_TV;
        protected CheckBox chkPhotoRequired;
        protected Label lblResultsBottom, lblShowRestricted;
        protected Label lblCount;
        protected Literal litAdTest, litPng, litJSOpenConst;

        protected AdRotator arHeader;
        protected Literal litHeaderText;
        protected HtmlImage imgRegister;
        protected HtmlAnchor aRegister;

        protected PlaceHolder phBreadCrumb;
        protected DataList dlSTARsDetail, dlSTARsThumb;
        protected Repeater rptCategories;

        protected HtmlContainerControl divNoResults, divRestricted;
        protected HtmlContainerControl liBrowse_Relevance, liBrowse_Raves, liBrowse_Visits, liBrowse_Adds, liBrowse_Newest;
        protected HtmlContainerControl liTime_Today, liTime_Week, liTime_Month, liTime_All;
        protected HtmlContainerControl liType_Videos, liType_Photos, liType_Music, liType_Games, liType_Patterns, liType_TV, liType_Widgets;
        protected HtmlContainerControl spnSearchFilterDesc, spnResultsTitle, spnTagCloudTitle, spnFindTitle;
        protected HtmlContainerControl ifrmTagCloud;
        protected HtmlContainerControl liBrowseLabel;
        protected HtmlContainerControl ulTime;
        protected HtmlContainerControl spnHotTip, spnInstruction;
        protected HtmlContainerControl d1, d2, d3;	 //Hot Tips div tags
        protected HtmlInputHidden AssetId;

        private string cCSS_CLASS_SELECTED = "selected";

        private const string cORDER_BY_DEFAULT = Constants.SEARCH_ORDERBY_CURRENT_POPULATION;
        private const int cNEW_WITHIN_DEFAULT = (int)Constants.eSEARCH_TIME_FRAME.WEEK;

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #endregion

        private void Page_Load(object sender, System.EventArgs e)
        {
            if (!IsPostBack)
            {
                InitializeSearch(true);

            }

            txtKeywords.Attributes.Add("onkeydown", "CheckKey()");

            // Add the javascript for deleting comments
            string scriptString = "<script language=\"javaScript\">\n<!--\n function CheckKey () {\n";
            scriptString += "if (event.keyCode == 13)\n";
            scriptString += "{\n";
            scriptString += ClientScript.GetPostBackEventReference(btnSearch, "") + ";\n";
            scriptString += "}}\n// -->\n";
            scriptString += "</script>";

            if (!ClientScript.IsClientScriptBlockRegistered(GetType(), "CheckKey"))
            {
                ClientScript.RegisterClientScriptBlock(GetType(), "CheckKey", scriptString);
            }

            //// Get total number of uploads
            //lblCount.Text = WebCache.GetTotalAssets().ToString("N0") + " Uploads";
            //litPng.Text = ResolveUrl ("~/images/mainheader_statsbg.png");

            // Is user logged in?
            if (Request.IsAuthenticated)
            {
                if (IsCurrentUserAdult())
                {
                    hlChangeProfile.NavigateUrl = ResolveUrl("~/mykaneva/settings.aspx");
                    lblShowRestricted.Text = KanevaWebGlobals.CurrentUser.ShowMature ? "Yes" : "No";
                }
                else
                {
                    divRestricted.Visible = false;
                }

                imgRegister.Alt = "Edit Your Profile";
                imgRegister.Src = ResolveUrl("~/images/header/indexpages/burgundy-add.png");
                aRegister.HRef = ResolveUrl("~/mykaneva/upload.aspx");

                // Has user logged into wok?
                if (KanevaWebGlobals.CurrentUser.HasWOKAccount)
                {
                    // Show the wok user heading text
                    /* TODO */
                    arHeader.AdvertisementFile = ResolveUrl("~/xml/entertainment_headertxt_wokuser.xml");
                }
                else
                {
                    // Show the non-wok user heading text
                    /* TODO */
                    arHeader.AdvertisementFile = ResolveUrl("~/xml/entertainment_headertxt_notwokuser.xml");

                    imgRegister.Src = ResolveUrl("~/images/header/indexpages/burgundy-add.png");
                    aRegister.HRef = ResolveUrl("~/mykaneva/upload.aspx");
                }
            }
            else
            {
                hlChangeProfile.NavigateUrl = ResolveUrl("~/loginsecure.aspx");

                // Show the not logged in heading text
                /* TODO */
                arHeader.AdvertisementFile = ResolveUrl("~/xml/entertainment_headertxt_notloggedin.xml");

                imgRegister.Src = ResolveUrl("~/images/header/indexpages/burgundy-add.png");
                aRegister.HRef = ResolveUrl("~/mykaneva/upload.aspx");
            }

            // Show correct advertisements
            AdvertisementSettings Settings = (AdvertisementSettings)Application["Advertisement"];

            // Set mode of Google Ads
            if (Settings.Mode.Equals(AdvertisingMode.Live))
            {
                litAdTest.Visible = false;
            }
            else if (Settings.Mode.Equals(AdvertisingMode.Testing))
            {
                litAdTest.Text = "google_adtest = \"on\"";
            }
        }

        #region Primary Functions

        /// <summary>
        /// BindData
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="filter"></param>
        private void BindData(int pageNumber, string filter)
        {
            ////// How many results do we display?
            ////int pgSize = filterTop.NumberOfPages;

            ////// Set current page
            ////pgTop.CurrentPageNumber = pageNumber;
            ////pgBottom.CurrentPageNumber = pageNumber;

            ////// Clear existing results
            ////dlSTARsThumb.Visible = false;
            ////dlSTARsDetail.Visible = false;

            ////divNoResults.Visible = false;

            ////string search_type_name = GetSearchTypeName();

            ////spnResultsTitle.InnerText = search_type_name;
            ////spnSearchFilterDesc.InnerText = GetSearchFilterSummary();

            ////spnTagCloudTitle.InnerText = (search_type_name.EndsWith("s") ? search_type_name.Remove(search_type_name.Length - 1, 1) : search_type_name);

            ////if (Is_Detail_View)
            ////{
            ////    lbThumbTop.CssClass = "";
            ////    lbDetailTop.Enabled = false;
            ////    lbDetailTop.CssClass = "selected";
            ////    lbDetailTop.Enabled = true;

            ////    pgSize = 10;
            ////}
            ////else
            ////{
            ////    lbDetailTop.CssClass = "";
            ////    lbThumbTop.Enabled = false;
            ////    lbThumbTop.CssClass = "selected";
            ////    lbThumbTop.Enabled = true;

            ////    pgSize = 20;
            ////}

            ////SearchSTARs(filter, pageNumber, pgSize);

            ////// Set the cloud type in the iframe source url
            //////ifrmTagCloud.Attributes.Add("src", ResolveUrl("~/usercontrols/TagCloud.aspx?tag=" + STARs_Type_Id.ToString()));

            ////// Show Pager
            ////pgTop.DrawControl();
            ////pgBottom.DrawControl();

            // Log the search
            //UsersUtility.LogSearch(GetUserId(), Search_String, STARs_Type_Id);
        }
        /// <summary>
        /// SearchSTARs
        /// </summary>
        /// <param name="searchString"></param>
        /// <param name="filter"></param>
        /// <param name="pgNumber"></param>
        /// <param name="pgSize"></param>
        private void SearchSTARs(string filter, int pgNumber, int pgSize)
        {
            string media_filter = filter;
            string category = string.Empty;

            // set which media we can see.  Public or Public and Kaneva Members only
            if (media_filter.Length > 0)
            {
                media_filter += " AND ";
            }

            PagedDataTable pds = null;

            if (Category_Id > 0)
            {
                category = Category_Id.ToString();
            }

            if (Request.IsAuthenticated)
            {
                media_filter += " (sa.permission=1 OR sa.permission=4) ";
            }
            else
            {
                media_filter += " sa.permission=1";
            }

            // Get the data
            //pds = StoreUtility.SearchAssetsNew(KanevaWebGlobals.CurrentUser.ShowMature, STARs_Type_Id,
            //    GetUserId(), false, chkPhotoRequired.Checked, Server.HtmlEncode(Search_String),
            //    category, New_Within, "", "", 0, media_filter, SetOrderByValue(), pgNumber, pgSize);

            pds = new PagedDataTable();

            // this is used to limit the number of pages to 5 when browsing
            if (Search_String.Length > 0)
            {
                pgTop.NumberOfPages = Math.Ceiling((double)pds.TotalCount / pgSize).ToString();
                pgBottom.NumberOfPages = Math.Ceiling((double)pds.TotalCount / pgSize).ToString();

                // Set up browse area
                ulTime.Visible = false;
                liBrowseLabel.InnerText = "Sort by";
                liBrowse_Relevance.Visible = true;
            }
            else
            {
                if (Math.Ceiling((double)pds.TotalCount / pgSize) < 5 || IsAdministrator())
                {
                    pgTop.NumberOfPages = Math.Ceiling((double)pds.TotalCount / pgSize).ToString();
                    pgBottom.NumberOfPages = Math.Ceiling((double)pds.TotalCount / pgSize).ToString();
                }
                else
                {
                    pgTop.NumberOfPages = "5";
                    pgBottom.NumberOfPages = "5";
                }

                // Set up browse area
                ulTime.Visible = true;
                liBrowseLabel.InnerText = "Browse";
                liBrowse_Relevance.Visible = false;
            }


            // Display thumbnail or detail results?
            if (Is_Detail_View && pds.TotalCount > 0)
            {
                dlSTARsDetail.Visible = true;
                dlSTARsDetail.DataSource = pds;
                dlSTARsDetail.DataBind();
            }
            else if (pds.TotalCount > 0)
            {
                dlSTARsThumb.Visible = true;
                dlSTARsThumb.DataSource = pds;
                dlSTARsThumb.DataBind();
            }
            else
            {
                ShowNoResults();
            }

            ShowResultsCount(pds.TotalCount, pgTop.CurrentPageNumber, filterTop.NumberOfPages, pds.Rows.Count);
        }

        #endregion

        #region Helper Methods

        /// <summary>
        /// ShowResultsCount
        /// </summary>
        /// <param name="total_count"></param>
        /// <param name="page_num"></param>
        /// <param name="count_per_page"></param>
        /// <param name="current_page_count"></param>
        private void ShowResultsCount(int total_count, int page_num, int count_per_page, int current_page_count)
        {
            // Display the showing #-# of ### count
            string results = string.Empty;

            if (total_count > 0)
            {
                results = "Results " + KanevaGlobals.GetResultsText(total_count, page_num, count_per_page,
                    current_page_count, true, "%START%-%END% of %TOTAL%");
            }

            lblResultsBottom.Text = results;
        }
        /// <summary>
        /// ShowNoResults
        /// </summary>
        private void ShowNoResults()
        {
            ////divNoResults.InnerHtml = "<div class='noresults'><br/>Your search did not find any matching " + GetSearchTypeName() + ".</div>";
            ////divNoResults.Visible = true;
        }
        /// <summary>
        /// SetOrderByValue
        /// </summary>
        private string SetOrderByValue()
        {
            // How they are sorted
            if (Order_By.Equals(Constants.SEARCH_ORDERBY_RELEVANCE))
            {
                // Most Relevant
                CurrentSort = "";
            }
            else if (Order_By.Equals(Constants.SEARCH_ORDERBY_NEWEST))
            {
                // Newest
                CurrentSort = "_dt_created";
            }
            else if (Order_By.Equals(Constants.SEARCH_ORDERBY_RAVES))
            {
                // Most raves
                CurrentSort = "_raves";
            }
            else if (Order_By.Equals(Constants.SEARCH_ORDERBY_ADDS))
            {
                // Most members
                CurrentSort = "_shared";
            }
            else // Views
            {
                CurrentSort = "_views";
            }

            // Set sort and order by
            return CurrentSort;
        }

        /// <summary>
        /// GetStarsCategories
        /// </summary>
        private void GetStarsCategories()
        {
            DataTable dtCategories = null; // GetGameFacade.GetGameCategories(GameToEditsID);
            
            DataRow drAll = dtCategories.NewRow();
            drAll["category_id"] = "0";
            drAll["name"] = "All";
            drAll["description"] = "All Categories";
            drAll["asset_type_id"] = "0";

            dtCategories.Rows.InsertAt(drAll, 0);

            rptCategories.DataSource = dtCategories;
            rptCategories.DataBind();
        }

        ///// <summary>
        ///// SetHotTipType
        ///// </summary>
        //private void SetHotTipType()
        //{
        //    switch (STARs_Type_Id)
        //    {
        //        case (int)Constants.eASSET_TYPE.TV:
        //        case (int)Constants.eASSET_TYPE.WIDGET:
        //        case (int)Constants.eASSET_TYPE.VIDEO:
        //            d1.Style.Add("display", "block");
        //            litJSOpenConst.Text = "d1";
        //            break;
        //        case (int)Constants.eASSET_TYPE.PATTERN:
        //        case (int)Constants.eASSET_TYPE.PICTURE:
        //            d2.Style.Add("display", "block");
        //            litJSOpenConst.Text = "d2";
        //            break;
        //        case (int)Constants.eASSET_TYPE.MUSIC:
        //        case (int)Constants.eASSET_TYPE.GAME:
        //            d3.Style.Add("display", "block");
        //            litJSOpenConst.Text = "d3";
        //            break;
        //        default:
        //            d1.Style.Add("display", "block");
        //            litJSOpenConst.Text = "d1";
        //            break;
        //    }
        //}

        /// <summary>
        /// SetOrderByType
        /// </summary>
        private void SetOrderByType()
        {
            try
            {
                if (Order_By == string.Empty) // Set order by if not already in viewstate
                {
                    if (Request[Constants.QUERY_STRING_ORDERBY] != null && Request[Constants.QUERY_STRING_ORDERBY] != string.Empty)
                    {
                        switch (Server.UrlDecode(Request[Constants.QUERY_STRING_ORDERBY].ToString().ToLower()))
                        {
                            case Constants.SEARCH_ORDERBY_CURRENT_POPULATION:
                            case Constants.SEARCH_ORDERBY_RAVES:
                            case Constants.SEARCH_ORDERBY_VISITS:
                                Order_By = Server.UrlDecode(Request[Constants.QUERY_STRING_ORDERBY].ToString());
                                break;
                            default:
                                Order_By = cORDER_BY_DEFAULT;
                                break;
                        }
                    }
                    else
                    {
                        Order_By = cORDER_BY_DEFAULT;
                    }
                }
            }
            catch (Exception)
            {
                Order_By = cORDER_BY_DEFAULT;
            }
        }

        /// <summary>
        /// SetWithinTimeFrameType
        /// </summary>
        private void SetWithinTimeFrameType()
        {
            try
            {
                if (New_Within < 0) // Get the type from the radio button selected
                {
                    if (Request[Constants.QUERY_STRING_WITHIN] != null && Request[Constants.QUERY_STRING_WITHIN] != string.Empty)
                    {
                        switch (Convert.ToInt32(Request[Constants.QUERY_STRING_WITHIN]))
                        {
                            case 0:
                            case 1:
                            case 7:
                            case 30:
                                New_Within = Convert.ToInt32(Request[Constants.QUERY_STRING_WITHIN]);
                                break;
                            default:
                                New_Within = cNEW_WITHIN_DEFAULT;
                                break;
                        }
                    }
                    else
                    {
                        New_Within = cNEW_WITHIN_DEFAULT;   //all time
                    }
                }
            }
            catch (Exception)
            {
                New_Within = cNEW_WITHIN_DEFAULT;	 //all time
            }
        }

        /// <summary>
        /// SetSearchKeywords
        /// </summary>
        private void SetSearchKeywords()
        {
            try
            {
                if (Search_String == string.Empty)
                {
                    if (Request[Constants.QUERY_STRING_KEYWORD] != null && Request[Constants.QUERY_STRING_KEYWORD] != string.Empty)
                    {
                        Search_String = Server.UrlDecode(Request[Constants.QUERY_STRING_KEYWORD].ToString());

                        Order_By = Constants.SEARCH_ORDERBY_RELEVANCE;
                        New_Within = 0;

                        txtKeywords.Text = Search_String;
                        txtKeywords.Style.Add("color", "#535353");
                    }
                    else
                    {
                        Search_String = string.Empty;
                    }
                }
            }
            catch (Exception)
            {
                Search_String = string.Empty;
            }
        }


        /// <summary>
        /// SetPopularityLinks
        /// </summary>
        private void SetPopularityLinks()
        {
            liBrowse_Population.Attributes.Add("class", "");
            liBrowse_Raves.Attributes.Add("class", "");
            liBrowse_Visits.Attributes.Add("class", "");

            switch (Order_By)
            {
                case Constants.SEARCH_ORDERBY_CURRENT_POPULATION:
                    liBrowse_Population.Attributes.Add("class", cCSS_CLASS_SELECTED);
                    break;
                case Constants.SEARCH_ORDERBY_RAVES:
                    liBrowse_Raves.Attributes.Add("class", cCSS_CLASS_SELECTED);
                    break;
                case Constants.SEARCH_ORDERBY_VISITS:
                    liBrowse_Visits.Attributes.Add("class", cCSS_CLASS_SELECTED);
                    break;
            }
        }

        /// <summary>
        /// SetTimeLinks
        /// </summary>
        private void SetTimeLinks()
        {
            //reset the list item classes
            liTime_Today.Attributes.Add("class", "");
            liTime_Week.Attributes.Add("class", "");
            liTime_Month.Attributes.Add("class", "");
            liTime_All.Attributes.Add("class", "");

            //set the new selected item's class
            switch (New_Within)
            {
                case (int)Constants.eSEARCH_TIME_FRAME.TODAY:
                    liTime_Today.Attributes.Add("class", cCSS_CLASS_SELECTED);
                    break;
                case (int)Constants.eSEARCH_TIME_FRAME.WEEK:
                    liTime_Week.Attributes.Add("class", cCSS_CLASS_SELECTED);
                    break;
                case (int)Constants.eSEARCH_TIME_FRAME.MONTH:
                    liTime_Month.Attributes.Add("class", cCSS_CLASS_SELECTED);
                    break;
                case (int)Constants.eSEARCH_TIME_FRAME.ALL_TIME:
                    liTime_All.Attributes.Add("class", cCSS_CLASS_SELECTED);
                    break;
            }
        }

        /// <summary>
        /// InitializeNewSearch
        /// </summary>
        private void InitializeSearch(bool Set_Keyword)
        {
            if (!IsPostBack)
            {
                Is_Detail_View = false;
            }

            // Set Search keyword
            if (Set_Keyword)
            {
                SetSearchKeywords();
            }

            // Set OrderBy Type
            SetOrderByType();

            // Set Within Type
            SetWithinTimeFrameType();

            // Get the Category list
            GetStarsCategories();

            //set advertisement
            //SetHotTipType();

            // These functions set the class to indicate to the user
            // what options are currently set
            SetPopularityLinks();
            SetTimeLinks();

            BindData(1, "");
        }
        /// <summary>
        /// GetSearchFilterSummary
        /// </summary>
        private string GetSearchFilterSummary()
        {
            string browse_txt = string.Empty;
            string time_txt = string.Empty;

            switch (Order_By)
            {
                case Constants.SEARCH_ORDERBY_RAVES:
                    browse_txt = "Most Raves";
                    break;
                case Constants.SEARCH_ORDERBY_VIEWS:
                    browse_txt = "Most Views";
                    break;
                case Constants.SEARCH_ORDERBY_ADDS:
                    browse_txt = "Most Adds";
                    break;
                case Constants.SEARCH_ORDERBY_NEWEST:
                    browse_txt = "Newest";
                    break;
                default:
                    browse_txt = "Best Match";
                    break;
            }

            switch (New_Within)
            {
                case (int)Constants.eSEARCH_TIME_FRAME.TODAY:
                    time_txt = "Today";
                    break;
                case (int)Constants.eSEARCH_TIME_FRAME.WEEK:
                    time_txt = "This Week";
                    break;
                case (int)Constants.eSEARCH_TIME_FRAME.MONTH:
                    time_txt = "This Month";
                    break;
                case (int)Constants.eSEARCH_TIME_FRAME.ALL_TIME:
                    time_txt = "All Time";
                    break;
            }

            return browse_txt + " - " + time_txt;
        }

        protected void AdCreated(Object source, AdCreatedEventArgs e)
        {
            if (e.AdProperties["HeaderText"].ToString() != "")
            {

                litHeaderText.Text = e.AdProperties["HeaderText"].ToString();
                //litHeaderText.Text = "test";
                arHeader.Visible = false;
            }
        }

        #endregion

        #region Event Handlers
        /// <summary>
        /// Execute when the user clicks the the Search button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (KanevaWebGlobals.ContainsInjectScripts(txtKeywords.Text))
            {
                m_logger.Warn("User " + KanevaWebGlobals.GetUserId() + " tried to script from IP " + Request.UserHostAddress);
                ShowErrorOnStartup("Your input contains invalid scripting, please remove script code and try again.", true);
                return;
            }

            //Search_String = txtKeywords.Text.ToLower().Trim() != cTEXTBOX_INSTRUCTION ? txtKeywords.Text : "";
            Search_String = txtKeywords.Text.Trim();

            Order_By = Constants.SEARCH_ORDERBY_NEWEST;
            if (Search_String.Length > 0)
            {
                Order_By = Constants.SEARCH_ORDERBY_RELEVANCE;
            }

            New_Within = (int)Constants.eSEARCH_TIME_FRAME.ALL_TIME;
            Category_Id = 0;

            lblResultsBottom.Visible = true;
            if (Search_String.Length == 0)
            {
                lblResultsBottom.Visible = false;
                New_Within = 30;
            }

            InitializeSearch(false);
        }

        /// <summary>
        /// Execute when the user clicks the Browse type link button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lbBrowse_Click(object sender, CommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case Constants.SEARCH_ORDERBY_VISITS:
                case Constants.SEARCH_ORDERBY_CURRENT_POPULATION:
                    Order_By = e.CommandName;
                    break;
                case Constants.SEARCH_ORDERBY_RAVES:
                default:
                    Order_By = Constants.SEARCH_ORDERBY_RAVES;
                    break;
            }

            SetPopularityLinks();

            BindData(1, filterTop.CurrentFilter);
        }
        /// <summary>
        /// Execute when the user clicks the Time range link button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lbTime_Click(object sender, CommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "today":
                    New_Within = (int)Constants.eSEARCH_TIME_FRAME.TODAY;
                    break;
                case "week":
                    New_Within = (int)Constants.eSEARCH_TIME_FRAME.WEEK;
                    break;
                case "month":
                    New_Within = (int)Constants.eSEARCH_TIME_FRAME.MONTH;
                    break;
                case "alltime":
                    New_Within = (int)Constants.eSEARCH_TIME_FRAME.ALL_TIME;
                    break;
            }

            SetTimeLinks();

            BindData(1, filterTop.CurrentFilter);
        }

        /// <summary>
        /// rptCategories_ItemDataBound
        /// </summary>
        private void rptCategories_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton lbCategory = (LinkButton)e.Item.FindControl("lbCategory");
                lbCategory.CommandArgument = DataBinder.Eval(e.Item.DataItem, "category_id").ToString();
                lbCategory.CommandName = DataBinder.Eval(e.Item.DataItem, "name").ToString();
                lbCategory.Text = DataBinder.Eval(e.Item.DataItem, "name").ToString();
                lbCategory.ToolTip = "View items in the " + DataBinder.Eval(e.Item.DataItem, "name").ToString() + " category";

                HtmlContainerControl liCategory = (HtmlContainerControl)e.Item.FindControl("liCategory");

                // select All as the default
                if (lbCategory.CommandArgument == "0" && liCategory != null)
                {
                    liCategory.Attributes.Add("class", cCSS_CLASS_SELECTED);
                }
            }
        }
        /// <summary>
        /// rptAssets_ItemCommand
        /// </summary>
        private void rptCategories_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            string command = e.CommandName;
            string cmdarg = e.CommandArgument.ToString();

            Category_Id = Convert.ToInt32(e.CommandArgument);

            LinkButton lbCat = (LinkButton)e.Item.FindControl("lbCategory");

            Repeater rptCategories = (Repeater)e.Item.Parent;
            if (rptCategories != null)
            {
                for (int i = 0; i < rptCategories.Items.Count; i++)
                {
                    if (rptCategories.Items[i].ItemType == ListItemType.Item || rptCategories.Items[i].ItemType == ListItemType.AlternatingItem)
                    {
                        HtmlContainerControl liCategory = (HtmlContainerControl)rptCategories.Items[i].FindControl("liCategory");
                        liCategory.Attributes.Add("class", "");
                    }
                }
            }

            if (lbCat != null)
            {
                Category_Id = Convert.ToInt32(lbCat.CommandArgument);

                ((HtmlContainerControl)lbCat.Parent).Attributes.Add("class", cCSS_CLASS_SELECTED);
            }

            BindData(1, filterTop.CurrentFilter);
        }

        /// <summary>
        /// Delete an asset
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Delete_Asset(Object sender, EventArgs e)
        {
            if (IsAdministrator() || IsCSR())
            {
                StoreUtility.DeleteAsset(Convert.ToInt32(AssetId.Value), GetUserId());
                pg_PageChange(this, new PageChangeEventArgs(pgTop.CurrentPageNumber));
                //BindData (1, filterTop.CurrentFilter);
            }
        }

        /// <summary>
        /// Restrict an asset
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnRestrictAsset_Click(Object sender, EventArgs e)
        {
            if (IsAdministrator() || IsCSR())
            {
                StoreUtility.UpdateAssetRestriction(Convert.ToInt32(AssetId.Value), true);
                //pg_PageChange (this, new PageChangeEventArgs (pgTop.CurrentPageNumber));
                BindData(pgTop.CurrentPageNumber, filterTop.CurrentFilter);
            }
        }

        /// <summary>
        /// Execute when the user clicks the the view type link button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lbViewAs_Click(object sender, EventArgs e)
        {
            if (((LinkButton)sender).Attributes["View"].ToString().ToLower() == "thumb")
            {
                Is_Detail_View = false;
            }
            else
            {
                Is_Detail_View = true;
            }

            BindData(1, filterTop.CurrentFilter);
        }

        #endregion

        #region Pager & Filter control events
        /// <summary>
        /// Execute when the user selects a page change link from the Pager control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pg_PageChange(object sender, PageChangeEventArgs e)
        {
            BindData(e.PageNumber, filterTop.CurrentFilter);
        }
        /// <summary>
        /// Execute when the user selects an item from the Store Filter
        /// Items to display drop down list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FilterChanged(object sender, FilterChangedEventArgs e)
        {
            BindData(1, e.Filter);
        }
        #endregion

        #region Properties


        private int STARs_Type_Id
        {
            set
            {
                ViewState["Star_Type_Id"] = value;
            }
            get
            {
                if (ViewState["Star_Type_Id"] == null)
                {
                    return -1;
                }
                else
                {
                    return Convert.ToInt32(ViewState["Star_Type_Id"]);
                }
            }
        }

        /// <summary>
        /// New_Within
        /// </summary>
        private int New_Within
        {
            set
            {
                ViewState["New_Within"] = value;
            }
            get
            {
                if (ViewState["New_Within"] == null)
                {
                    return -1;
                }
                else
                {
                    return Convert.ToInt32(ViewState["New_Within"]);
                }
            }
        }
        /// <summary>
        /// Search_String
        /// </summary>
        private string Search_String
        {
            set
            {
                ViewState["Search_String"] = value.Trim();
            }
            get
            {
                if (ViewState["Search_String"] == null)
                {
                    return string.Empty; //cORDER_BY_DEFAULT;
                }
                else
                {
                    return ViewState["Search_String"].ToString();
                }
            }
        }

        /// <summary>
        /// orderBy
        /// </summary>
        private string Order_By
        {
            set
            {
                ViewState["Order_By"] = value;
            }
            get
            {
                if (ViewState["Order_By"] == null)
                {
                    return string.Empty; //cORDER_BY_DEFAULT;
                }
                else
                {
                    return ViewState["Order_By"].ToString();
                }
            }
        }

        /// <summary>
        /// categoryId
        /// </summary>
        private int Category_Id
        {
            set
            {
                ViewState["Category_Id"] = value;
            }
            get
            {
                if (ViewState["Category_Id"] == null)
                {
                    return 0;   // All
                }
                else
                {
                    return Convert.ToInt32(ViewState["Category_Id"]);
                }
            }
        }

        /// <summary>
        /// isDetailView
        /// </summary>
        private bool Is_Detail_View
        {
            set
            {
                ViewState["Is_Detail_View"] = value;
            }
            get
            {
                if (ViewState["Is_Detail_View"] == null)
                {
                    return false;
                }
                else
                {
                    return Convert.ToBoolean(ViewState["Is_Detail_View"]);
                }
            }
        }
        # endregion

        #region Web Form Designer generated code

        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            rptCategories.ItemCommand += new RepeaterCommandEventHandler(rptCategories_ItemCommand);
            rptCategories.ItemDataBound += new RepeaterItemEventHandler(rptCategories_ItemDataBound);
            pgTop.PageChanged += new PageChangeEventHandler(pg_PageChange);
            pgBottom.PageChanged += new PageChangeEventHandler(pg_PageChange);
            filterTop.FilterChanged += new FilterChangedEventHandler(FilterChanged);
        }
        #endregion
    }
}
