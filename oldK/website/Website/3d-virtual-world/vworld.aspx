<%@ Page language="c#" EnableViewState="True" Codebehind="vworld.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.vworld" %>
<%@ Register TagPrefix="Kaneva" TagName="SearchFilter" Src="../usercontrols/SearchFilter.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../usercontrols/Pager.ascx" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>
<%@ Register TagPrefix="Kaneva" TagName="VWNav" Src="../usercontrols/VirtualWorld-Nav.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="Kaching" Src="../usercontrols/VirtualWorld-Kaching.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="NewsList" Src="../usercontrols/NewsList.ascx" %>

<link href="../css/new.css" type="text/css" rel="stylesheet">
<link href="../css/shadow.css" type="text/css" rel="stylesheet">
<!--[if IE]> <style type="text/css">@import "../css/new_IE.css";</style> <![endif]-->
<script src="../jscript/prototype.js" type="text/javascript"></script>
<script src="../jscript/scriptaculous/scriptaculous.js" type="text/javascript"></script>
<table cellpadding="0" cellspacing="0" border="0" class="newcontainer" align="center">
<tr>
<td colspan="3">
<table  border="0" cellpadding="0" cellspacing="0" width="100%" class="newcontainerborder">
<tr>
<td class=""></td>
<td class=""></td>
<td class=""></td>
</tr>
<tr>
<td class=""><img id="Img2" runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
<td valign="top" class="newdatacontainer">
<table width="100%" border="0" cellpadding="0" cellspacing="0" >
<tr colspan="2">											
<td id="virtualworld">

    <div id="mainHeader">
        <table width="930" border="0" cellpadding="0" cellspacing="0" id="headerContent">
        <tr>
        <td  align="left" width="702">
        <h1>World of Kaneva</h1>				                                   
        <asp:adrotator id="arHeader" runat="server" OnAdCreated="AdCreated"  />
        <h2><asp:literal id="litHeaderText" runat="server"></asp:literal></h2>				                                   
        </td>
        <td width="44"></td>
        <td width="216" valign="bottom" align="right"><a id="aRegister" runat="server" class="nohover"><img runat="server" src="~/images/header/indexpages/green-start.png" id="imgRegister" alt="Start a Community" width="215" height="65" border="0" class="png" /></a><br>                
        <h3><span class="stat"><asp:label id="lblCount" runat="server" /></span><img id="Img6" runat="server" src="~/images/spacer.gif" width="14" height="8"></h3></td>
        </tr>
        </table>                                        
    </div>

</td>
</tr>
<tr>
<td width="968" align="left"  valign="top">
<div id="promotionBanner">
	<table border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td><h1>Shop now for hundreds of new items -- cool clothes, stylish furniture and more!</h1></td>
			<td><a href="http://www.kaneva.com/myKaneva/buycredits.aspx" target="_parent"><img src="http://images.kaneva.com/media/communities/kaneva/virtualworld/button_addcredits.gif" border="0" class="nohover"></a></td>
		</tr>
	</table>
</div>
    <table border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td valign="top" width="217">
                <Kaneva:VWNav ID="VWNav" runat="server" />
            </td>
            <td width="10">&nbsp;</td>
            <td valign="top">
            <div style="width:730px; overflow:hidden;">
            <!-- Begin Main Content -->
                <asp:Literal ID="litContent" runat="server"></asp:Literal>
                
                <Kaneva:Kaching ID="Kaching" runat="server"></Kaneva:Kaching>
                
            <!-- End Main Content -->                                                   
            </div>                
            </td>    
        </tr>									
    </table>
</td>
</tr>
</table>
</td>
<td class=""><img id="Img9" runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
</tr>
<tr>
<td class=""></td>
<td class=""></td>
<td class=""></td>
</tr>
</table>				
</td>
</tr>
</table>