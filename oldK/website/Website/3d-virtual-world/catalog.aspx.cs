///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using log4net;
using MagicAjax;

namespace KlausEnt.KEP.Kaneva
{
    /// <summary>
    /// Summary description for vworld.
    /// </summary>
    public class catalog : StoreBasePage
    {
        protected catalog()
        {
        }

        private void Page_Load(object sender, System.EventArgs e)
        {
            //initialize the page 
            InitializePage();
        }



        #region Primary Functions

        private void InitializePage()
        {

            txtKeywords.Attributes.Add("onkeydown", "CheckKey()");

            // Add the javascript for deleting comments
            string scriptString = "<script language=\"javaScript\">\n<!--\n function CheckKey () {\n";
            scriptString += "if (event.keyCode == 13)\n";
            scriptString += "{\n";
            scriptString += ClientScript.GetPostBackEventReference(btnSearch, "") + ";\n";
            scriptString += "}}\n// -->\n";
            scriptString += "</script>";

            if (!ClientScript.IsClientScriptBlockRegistered(GetType(), "CheckKey"))
            {
                ClientScript.RegisterClientScriptBlock(GetType(), "CheckKey", scriptString);
            }

            //setup header nav bar
            HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.VIRTUAL_WORLD;

            //set the meta tags
            Title = "Virtual World of Kaneva. Create Avatars, 3D Spaces and Explore.";
            MetaDataTitle = "<meta name=\"title\" content=\"Kaneva. A Unique Online Community and 3D Social Network.\" />";
            MetaDataDescription = "<meta name=\"description\" content=\"Kaneva�s free virtual world lets you customize your Avatar and your surroundings for a 3D online virtual world experience that is truly unique.  \"/>";
            MetaDataKeywords = "<meta name=\"keywords\" content=\"online community, social network, meet people, friends, networking, games, mmo, movies, 3D, virtual world, sharing photos, video, blog, bands, music, rate pics, digital, rpg, entertainment, join groups, forums, online social networking, caneva, kaneeva, kanneva, kineva\" />";


            // Is user logged in?
            if (Request.IsAuthenticated)
            {

                imgRegister.Alt = "Edit Your Profile";
                imgRegister.Src = ResolveUrl("~/images/header/indexpages/blue-install.png");
                aRegister.HRef = ResolveUrl("~/channel/channelPage.aspx?communityId=1118&pageId=2047101");

                // Has user logged into wok?
                if (KanevaWebGlobals.CurrentUser.HasWOKAccount)
                {
                    // Show the wok user heading text
                    /* TODO */
                    arHeader.AdvertisementFile = ResolveUrl("~/xml/virtual-world_headertxt_wokuser.xml");
                }
                else
                {
                    // Show the non-wok user heading text
                    /* TODO */
                    arHeader.AdvertisementFile = ResolveUrl("~/xml/virtual-world_headertxt_notwokuser.xml");

                    imgRegister.Src = ResolveUrl("~/images/header/indexpages/blue-install.png");
                    aRegister.HRef = ResolveUrl("~/channel/channelPage.aspx?communityId=1118&pageId=2047101");
                }
            }
            else
            {
                //hlChangeProfile.NavigateUrl = ResolveUrl("~/loginsecure.aspx");

                // Show the not logged in heading text
                /* TODO */
                arHeader.AdvertisementFile = ResolveUrl("~/xml/community_headertxt_notloggedin.xml");

                imgRegister.Src = ResolveUrl("~/images/header/indexpages/blue-install.png");
                aRegister.HRef = ResolveUrl("~/channel/channelPage.aspx?communityId=1118&pageId=2047101x");
            }




            // Show correct advertisements
            AdvertisementSettings Settings = (AdvertisementSettings)Application["Advertisement"];

            // Set mode of Google Ads
            if (Settings.Mode.Equals(AdvertisingMode.Live))
            {
                litAdTest.Visible = false;
            }
            else if (Settings.Mode.Equals(AdvertisingMode.Testing))
            {
                litAdTest.Text = "google_adtest = \"on\"";
            }

            //get any parameters for configuration
            if (GetRequestParams() || divWOKCAtalogItemsResults.Visible)
            {
                //initialize the search results
                BindData(1);
            }

        }

        /// <summary>
        /// BindData
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="filter"></param>
        private void BindData(int pageNumber)
        {
            // How many results do we display?
            int pgSize = filterTop.NumberOfPages;

            //show catalog div
            divWOKCAtalogItemsResults.Visible = true;

            // Set current page
            pgTop.CurrentPageNumber = pageNumber;
            pgBottom.CurrentPageNumber = pageNumber;

            // Clear existing results
            dlAssetsThumb.Visible = false;
            dlAssetsDetail.Visible = false;

            divNoResults.Visible = false;

            spnResultsTitle.InnerText = Server.HtmlDecode(CurrentCategoryName);
            spnSearchFilterDesc.InnerText = CatalogSearchString;

            if (Is_Detail_View)
            {
                lbThumbTop.CssClass = "";
                lbDetailTop.Enabled = false;
                lbDetailTop.CssClass = "selected";
                lbDetailTop.Enabled = true;

                pgSize = 10;
            }
            else
            {
                lbDetailTop.CssClass = "";
                lbThumbTop.Enabled = false;
                lbThumbTop.CssClass = "selected";
                lbThumbTop.Enabled = true;

                pgSize = 20;
            }

            SearchWOKItems(pageNumber, pgSize);

            // Show Pager
            pgTop.DrawControl();
            pgBottom.DrawControl();

            // Log the search
            //UsersUtility.LogSearch(GetUserId(), CatalogSearchString, Category_Id);
        }

        /// <summary>
        /// SearchAssets
        /// </summary>
        /// <param name="searchString"></param>
        /// <param name="filter"></param>
        /// <param name="pgNumber"></param>
        /// <param name="pgSize"></param>
        private void SearchWOKItems(int pgNumber, int pgSize)
        {
            PagedDataTable pds = null;

            pds = StoreUtility.SearchWOKCatalogItems(Server.HtmlEncode(CatalogSearchString), WOKCategory_Ids, 0, OwnerId,
                CurrentCategoryId, pgNumber, pgSize);

            // this is used to limit the number of pages to 5 when browsing
            if (CatalogSearchString.Length > 0)
            {
                pgTop.NumberOfPages = Math.Ceiling((double)pds.TotalCount / pgSize).ToString();
                pgBottom.NumberOfPages = Math.Ceiling((double)pds.TotalCount / pgSize).ToString();
            }
            else
            {
                if (Math.Ceiling((double)pds.TotalCount / pgSize) < 5 || IsAdministrator())
                {
                    pgTop.NumberOfPages = Math.Ceiling((double)pds.TotalCount / pgSize).ToString();
                    pgBottom.NumberOfPages = Math.Ceiling((double)pds.TotalCount / pgSize).ToString();
                }
                else
                {
                    pgTop.NumberOfPages = "5";
                    pgBottom.NumberOfPages = "5";
                }

            }

            // Display thumbnail or detail results?
            if (Is_Detail_View && pds.TotalCount > 0)
            {
                dlAssetsDetail.Visible = true;
                dlAssetsDetail.DataSource = pds;
                dlAssetsDetail.DataBind();
            }
            else if (pds.TotalCount > 0)
            {
                dlAssetsThumb.Visible = true;
                dlAssetsThumb.DataSource = pds;
                dlAssetsThumb.DataBind();
            }
            else
            {
                ShowNoResults();
            }

            ShowResultsCount(pds.TotalCount, pgTop.CurrentPageNumber, filterTop.NumberOfPages, pds.Rows.Count);
        }

        /// <summary>
        /// ShowResultsCount
        /// </summary>
        /// <param name="total_count"></param>
        /// <param name="page_num"></param>
        /// <param name="count_per_page"></param>
        /// <param name="current_page_count"></param>
        private void ShowResultsCount(int total_count, int page_num, int count_per_page, int current_page_count)
        {
            // Display the showing #-# of ### count
            string results = string.Empty;

            if (total_count > 0)
            {
                results = "Results " + KanevaGlobals.GetResultsText(total_count, page_num, count_per_page,
                    current_page_count, true, "%START%-%END% of %TOTAL%");
            }

            lblResultsBottom.Text = results;
        }
        /// <summary>
        /// ShowNoResults
        /// </summary>
        private void ShowNoResults()
        {
            divNoResults.InnerHtml = "<div class='noresults'><br/>Your search did not find any items under " + GetSearchFilterSummary() + ".</div>";
            divNoResults.Visible = true;
        }

        /// <summary>
        /// GetSearchFilterSummary
        /// </summary>
        private string GetSearchFilterSummary()
        {
            return CurrentCategoryName + ((CatalogSearchString.Length > 0) ? (" - " + CatalogSearchString) : "");
        }

        /// <summary>
        /// DisplayCategoryMerchandising
        /// </summary>
        private void DisplayCategoryMarketing(int categoryId)
        {
            string marketingPath = CategoryMarketing[categoryId.ToString()];
            if (marketingPath != null && marketingPath.Length > 0)
            {
                divPromotions.Visible = true;
                divPromotions.InnerHtml = marketingPath;
            }
            else
            {
                divPromotions.Visible = false;
                divPromotions.InnerHtml = "";
            }
        }

        private void PopulateCatalogNodes(TreeNode node, int categoryId)
        {
            switch (node.Depth)
            {
                case 0:
                    CategoryMarketing = new NameValueCollection();
                    GetTopLevelsParents(node, categoryId);
                    break;
                default:
                    GetParentsChildren(node, categoryId);
                    break;
            }
        }

        private void GetTopLevelsParents(TreeNode node, int parentId)
        {
            //clear out any previous children to prevent double loading
            node.ChildNodes.Clear();

            //get toplevel catalog categories - zero is the parent id of toplevel categories
            DataTable dtCategories = StoreUtility.GetItemCategoriesByParentCategoryId(parentId);
            //generate and bind top level categories
            foreach (DataRow row in dtCategories.Rows)
            {
                TreeNode newNode = new TreeNode(row["category"].ToString(), node.Value.ToString() + "-" + row["category_id"].ToString());
                newNode.ToolTip = "View " + row["category"].ToString();

                //check each one to see if it has any children
                int category_id = -1;
                try
                {
                    category_id = Convert.ToInt32(row["category_id"]);
                }
                catch (FormatException)
                { }
                if (StoreUtility.HasChildCategories(category_id))
                {
                    newNode.SelectAction = TreeNodeSelectAction.SelectExpand;
                    newNode.PopulateOnDemand = true;
                }
                else
                {
                    newNode.SelectAction = TreeNodeSelectAction.Select;
                    newNode.PopulateOnDemand = false;
                }

                node.ChildNodes.Add(newNode);

                //store merchandising path
                CategoryMarketing.Add(row["category_id"].ToString(), row["marketing_path"].ToString());
            }
            //pull back all catalog items
            CatalogSearchString = "";
        }

        private void GetParentsChildren(TreeNode node, int parentId)
        {
            node.ChildNodes.Clear();

            //get the subcatagories
            DataTable dtsubCategories = StoreUtility.GetItemCategoriesByParentCategoryId(parentId);
            foreach (DataRow row in dtsubCategories.Rows)
            {
                TreeNode newNode = new TreeNode(row["category"].ToString(), node.Value.ToString() + "-" + row["category_id"].ToString());
                newNode.PopulateOnDemand = false;
                newNode.ToolTip = "View " + row["category"].ToString();

                //check each one to see if it has any children
                int category_id = -1;
                try
                {
                    category_id = Convert.ToInt32(row["category_id"]);
                }
                catch (FormatException)
                { }
                if (StoreUtility.HasChildCategories(category_id))
                {
                    newNode.SelectAction = TreeNodeSelectAction.SelectExpand;
                    newNode.PopulateOnDemand = true;
                }
                else
                {
                    newNode.SelectAction = TreeNodeSelectAction.Select;
                    newNode.PopulateOnDemand = false;
                }
                node.ChildNodes.Add(newNode);
            }
        }

        private void MenuChanged(TreeNode Node)
        {
            ResetMenuControlledDivAreas();
            if (Node.Depth == 0)
            {
                //reset all filter values
                OwnerId = -1;
                WOKCategory_Ids = null;
            }

            //gather and assign the toplevel menu and current category
            string[] directLevels = Node.Value.Split('-');
            SelectedTopLevelMenu = directLevels[0];
            CurrentCategoryId = Convert.ToInt32(directLevels[directLevels.Length - 1]);
            //set the display value - must be set before the databind
            CurrentCategoryName = Node.Text;

            switch (SelectedTopLevelMenu)
            {
                case "0":
                    divMenuChoiceDisplay.Visible = true;
                    break;
                case "1":
                    divWOKCAtalogItemsResults.Visible = true;
                    if ((CurrentCategoryNode == null) || (!CurrentCategoryNode.Equals(Node)))
                    {
                        PopulateCatalogNodes(Node, CurrentCategoryId);
                        BindData(1);
                        //display merchandising if any
                    }
                    DisplayCategoryMarketing(CurrentCategoryId);
                    break;
                case "2":
                    divMenuChoiceDisplay.Visible = true;
                    break;
                case "3":
                    divMenuChoiceDisplay.Visible = true;
                    break;
                case "4":
                    divMenuChoiceDisplay.Visible = true;
                    break;
                case "5":
                    divMenuChoiceDisplay.Visible = true;
                    break;
            }
            //store the currently selected node
            //must be stored after the check
            CurrentCategoryNode = Node;

            //disable the newly selected node to prevent select actions
            /* Node.SelectAction = TreeNodeSelectAction.Expand;

             //restore the select action of the previous node if any
             if ((CurrentCategoryNode != null) && (!CurrentCategoryNode.Equals(Node)))
             {
                 CurrentCategoryNode.SelectAction = TreeNodeSelectAction.SelectExpand;
             }*/


        }

        private void GetLeaves(TreeNode node)
        {
        }

        #endregion

        #region Helper Functions

        private bool GetRequestParams()
        {
            bool retVal = false;
            if (Request["catId"] != null)
            {
                WOKCategory_Ids = Request["catId"].ToString();
                retVal = true;
            }
            try
            {
                if (Request["typeId"] != null)
                {
                    OwnerId = Convert.ToInt32(Request["typeId"]);
                    retVal = true;
                }
            }
            catch (FormatException)
            {
            }
            return retVal;
        }

        private void ResetViewStateVariables()
        {
            CatalogSearchString = "";
            OwnerId = -1;
            CurrentCategoryId = 0;
            WOKCategory_Ids = "";
        }

        private void ResetMenuControlledDivAreas()
        {
            divWOKCAtalogItemsResults.Visible = false;
            divMenuChoiceDisplay.Visible = false;
        }

        protected void AdCreated(Object source, AdCreatedEventArgs e)
        {
            if (e.AdProperties["HeaderText"].ToString() != "")
            {

                litHeaderText.Text = e.AdProperties["HeaderText"].ToString();
                //litHeaderText.Text = "test";
                arHeader.Visible = false;
            }
        }

        #endregion


        #region Event Handlers
        /// <summary>
        /// Execute when the user clicks the the Search button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (KanevaWebGlobals.ContainsInjectScripts(txtKeywords.Text))
            {
                m_logger.Warn("User " + KanevaWebGlobals.GetUserId() + " tried to script from IP " + Request.UserHostAddress);
                ShowErrorOnStartup("Your input contains invalid scripting, please remove script code and try again.", true);
                return;
            }

            CatalogSearchString = txtKeywords.Text.Trim();

            //search for items
            BindData(1);
        }

        /// <summary>
        /// Execute when the user clicks the the tree view parent nodes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GetMenu(object sender, TreeNodeEventArgs e)
        {
            MenuChanged(e.Node);
        }

        protected void GetMenuItems_Select(object sender, EventArgs e)
        {
            TreeNode selectNode = vWorldMenu.SelectedNode;
            MenuChanged(selectNode);
        }


        /// <summary>
        /// Execute when the user clicks the the view type link button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lbViewAs_Click(object sender, EventArgs e)
        {
            if (((LinkButton)sender).Attributes["View"].ToString().ToLower() == "thumb")
            {
                Is_Detail_View = false;
            }
            else
            {
                Is_Detail_View = true;
            }

            BindData(1);
        }



        #endregion

        #region Pager & Filter control events
        /// <summary>
        /// Execute when the user selects a page change link from the Pager control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pg_PageChange(object sender, PageChangeEventArgs e)
        {
            BindData(e.PageNumber);
        }
        /// <summary>
        /// Execute when the user selects an item from the Store Filter
        /// Items to display drop down list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FilterChanged(object sender, FilterChangedEventArgs e)
        {
            CatalogSearchString = e.Filter;
            BindData(1);
        }

        #endregion


        #region Properties

        /// <summary>
        /// OwnerId
        /// </summary>
        private int OwnerId
        {
            get
            {
                if (ViewState["ownerid"] == null)
                {
                    ViewState["ownerid"] = -1;
                }
                return (int)ViewState["ownerid"];
            }
            set
            {
                ViewState["ownerid"] = value;
            }
        }

        /// <summary>
        /// CurrentCategoryId
        /// </summary>
        private int CurrentCategoryId
        {
            set
            {
                ViewState["CurrentCategoryId"] = value;
            }
            get
            {
                if (ViewState["CurrentCategoryId"] == null)
                {
                    return 0;   // All
                }
                else
                {
                    return Convert.ToInt32(ViewState["CurrentCategoryId"]);
                }
            }
        }


        /// <summary>
        /// CurrentCategoryName
        /// </summary>
        private string CurrentCategoryName
        {
            set
            {
                ViewState["CurrentCategoryName"] = value;
            }
            get
            {
                if (ViewState["CurrentCategoryName"] == null)
                {
                    return "Shopping";
                }
                else
                {
                    return ViewState["CurrentCategoryName"].ToString();
                }
            }
        }

        /// <summary>
        /// SelectedTopLevelMenu
        /// </summary>
        private string SelectedTopLevelMenu
        {
            set
            {
                ViewState["SelectedTopLevelMenu"] = value;
            }
            get
            {
                if (ViewState["SelectedTopLevelMenu"] == null)
                {
                    return "0";
                }
                else
                {
                    return ViewState["SelectedTopLevelMenu"].ToString();
                }
            }
        }


        /// <summary>
        /// CurrentCategoryNode
        /// </summary>
        private TreeNode CurrentCategoryNode
        {
            set
            {
                Session["CurrentCategoryNode"] = value;
            }
            get
            {
                if (Session["CurrentCategoryNode"] == null)
                {
                    return null;
                }
                else
                {
                    return (TreeNode)Session["CurrentCategoryNode"];
                }
            }
        }

        /// <summary>
        /// CurrentCategoryNode
        /// </summary>
        private NameValueCollection CategoryMarketing
        {
            set
            {
                Session["CatMarketing"] = value;
            }
            get
            {
                if (Session["CatMarketing"] == null)
                {
                    return new NameValueCollection();
                }
                else
                {
                    return (NameValueCollection)Session["CatMarketing"];
                }
            }
        }

        /// <summary>
        /// WOKCategory_Ids
        /// </summary>
        private string WOKCategory_Ids
        {
            set
            {
                ViewState["WOKCategory_Ids"] = value;
            }
            get
            {
                if (ViewState["WOKCategory_Ids"] == null)
                {
                    return "";
                }
                else
                {
                    return ViewState["WOKCategory_Ids"].ToString();
                }
            }
        }

        /// <summary>
        /// CatalogSearchString
        /// </summary>
        private string CatalogSearchString
        {
            set
            {
                ViewState["CatalogSearchString"] = value.Trim();
            }
            get
            {
                if (ViewState["CatalogSearchString"] == null)
                {
                    return string.Empty; //cORDER_BY_DEFAULT;
                }
                else
                {
                    return ViewState["CatalogSearchString"].ToString();
                }
            }
        }

        /// <summary>
        /// isDetailView
        /// </summary>
        private bool Is_Detail_View
        {
            set
            {
                ViewState["Is_Detail_View"] = value;
            }
            get
            {
                if (ViewState["Is_Detail_View"] == null)
                {
                    return false;
                }
                else
                {
                    return Convert.ToBoolean(ViewState["Is_Detail_View"]);
                }
            }
        }

        # endregion


        #region Declarations

        protected Kaneva.Pager pgTop, pgBottom;
        protected Kaneva.SearchFilter filterTop;
        protected Kaneva.usercontrols.TagCloud ucTagCloud;

        protected HyperLink hlChangeProfile;
        protected Button btnSearch;
        protected TextBox txtKeywords;
        protected LinkButton lbThumbTop, lbDetailTop;
        protected LinkButton lbBrowse_Relevance, lbBrowse_Raves, lbBrowse_Views, lbBrowse_Adds, lbBrowse_Newest;
        protected LinkButton lbTime_Today, lbTime_Week, lbTime_Month, lbTime_All;
        protected LinkButton lbType_Videos, lbType_Photos, lbType_Music, lbType_Games, lbType_Patterns, lbType_Widgets, lbType_TV;
        protected CheckBox chkPhotoRequired;
        protected Label lblResultsBottom, lblShowRestricted;
        protected Label lblCount;
        protected Literal litAdTest, litPng, litJSOpenConst;

        protected AdRotator arHeader;
        protected Literal litHeaderText;
        protected HtmlImage imgRegister;
        protected HtmlAnchor aRegister;

        protected PlaceHolder phBreadCrumb;
        protected DataList dlAssetsDetail, dlAssetsThumb;
        protected Repeater rptCategories, rptSubCategories;

        protected HtmlContainerControl divNoResults, divRestricted, divPromotions, divMenuChoiceDisplay, divWOKCAtalogItemsResults;
        protected HtmlContainerControl liBrowse_Relevance, liBrowse_Raves, liBrowse_Views, liBrowse_Adds, liBrowse_Newest;
        protected HtmlContainerControl liTime_Today, liTime_Week, liTime_Month, liTime_All;
        protected HtmlContainerControl liType_Videos, liType_Photos, liType_Music, liType_Games, liType_Patterns, liType_TV, liType_Widgets;
        protected HtmlContainerControl spnSearchFilterDesc, spnResultsTitle, spnFindTitle;
        protected HtmlContainerControl ifrmTagCloud;
        protected HtmlContainerControl liBrowseLabel;
        protected HtmlContainerControl ulTime, shoppingMenu, shoppingSubMenu;
        protected HtmlContainerControl spnHotTip, spnInstruction;
        protected HtmlContainerControl d1, d2, d3;	 //Hot Tips div tags
        protected HtmlInputHidden AssetId;
        protected TreeView vWorldMenu;

        private const int GET_DATA = 2;
        private const int GET_MENU = 1;
        private const string cORDER_BY_DEFAULT = Constants.SEARCH_ORDERBY_VIEWS;
        private const int cNEW_WITHIN_DEFAULT = (int)Constants.eSEARCH_TIME_FRAME.WEEK;
        private const int cSEARCH_TYPE_DEFAULT = (int)Constants.eASSET_TYPE.VIDEO;

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #endregion


        #region Web Form Designer generated code

        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            pgTop.PageChanged += new PageChangeEventHandler(pg_PageChange);
            pgBottom.PageChanged += new PageChangeEventHandler(pg_PageChange);
            filterTop.FilterChanged += new FilterChangedEventHandler(FilterChanged);
        }
        #endregion
    }
}
