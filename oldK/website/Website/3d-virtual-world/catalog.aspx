<%@ Page language="c#" EnableViewState="True" Codebehind="vworld.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.catalog" %>
<%@ Register TagPrefix="Kaneva" TagName="SearchFilter" Src="../usercontrols/SearchFilter.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../usercontrols/Pager.ascx" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>

<link href="../css/new.css" type="text/css" rel="stylesheet">
<link href="../css/shadow.css" type="text/css" rel="stylesheet">

<!--[if IE]> <style type="text/css">@import "../css/new_IE.css";</style> <![endif]-->

<script src="../jscript/prototype.js" type="text/javascript"></script>
<script src="../jscript/scriptaculous/scriptaculous.js" type="text/javascript"></script>

<table cellpadding="0" cellspacing="0" border="0" width="990" align="center">
	<tr>
		<td colspan="3"><img id="Img1" runat="server" src="~/images/spacer.gif" width="1" height="14" /></td>
	</tr>
	<tr>
		<td colspan="3">
			<table  border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="frTopLeft"></td>
					<td class="frTop"></td>
					<td class="frTopRight"></td>
				</tr>
				<tr>
					<td bgcolor="#f1f1f2" class="frBorderLeft"><img id="Img2" runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
					<td valign="top" bgcolor="#f1f1f2" class="frBgIntMembers">
						<table width="100%" border="0" cellpadding="0" cellspacing="0" >
							<tr colspan="2">
								<td id="virtualworld">
									<div id="mainHeader">
										<img id="Img3" runat="server" src="~/images/header/indexpages/header-virtualworld4.jpg" alt="Kaneva Avatar" border="0" class="png">
										<table cellpadding="0" cellspacing="0" border="0" id="headerContent" width="80%">
                                            <tr>
                                                <td  align="left">
                                                <h1>Virtual World</h1>
                                                <asp:adrotator id="arHeader" runat="server" OnAdCreated="AdCreated"  />
                                                <h2><asp:literal id="litHeaderText" runat="server"></asp:literal></h2>				                                   
                                                </td>
                                                <td width="44"></td>
                                                <td width="216" valign="bottom" align="right"><a id="aRegister" runat="server" class="nohover"><img runat="server" src="~/images/header/indexpages/green-start.png" id="imgRegister" alt="Start a Community" width="215" height="65" border="0" class="png" /></a><br>                
                                                <h3><span class="stat"><asp:label id="lblCount" runat="server" /></span><img id="Img4" runat="server" src="~/images/spacer.gif" width="14" height="8"></h3></td>
                                                </td>
                                            </tr>
                                        </table>

									</div>
								</td>
							</tr>
							<tr>
								<td width="968" align="left"  valign="top">
									
									<table cellpadding="0" cellspacing="0" border="0" width="100%">
										<tr>
											<td width="783" valign="top">
											
												<table border="0" cellspacing="0" cellpadding="0" width="100%">
													<tr>
														<td align="center">
												
															<div class="module">
																<span class="ct"><span class="cl"></span></span>
																
																
																<span class="cb"><span class="cl"></span></span>
															</div>
														</td>
													</tr>
												</table>
												
												<table border="0" cellspacing="0" cellpadding="0" width="100%" >
													<tr>
														<td width="180" align="left" valign="top">
															<div class="module whitebg" style="text-align:left">
																<span class="ct"><span class="cl"></span></span>
								<!-- BEGIN SIDE MENU SECTION --------------------------------------------------------------------->				
																    <ajax:ajaxpanel id="Ajaxpanel1" runat="server">
                                                                        <asp:TreeView ExpandDepth="0" id="vWorldMenu" runat="server" 
															                OnTreeNodePopulate="GetMenu" 
															                PopulateNodesFromClient="false"
                                                                            ExpandImageUrl="~/images/arrow_pointright.gif"
                                                                            CollapseImageUrl="~/images/arrow_pointdown.gif" 
                                                                            LeafNodeImageUrl="" 
                                                                            NodeIndent="10"
                                                                            OnSelectedNodeChanged="GetMenuItems_Select">
                                                                            <SelectedNodeStyle Font-Names="verdana, arial, sans-serif" Font-Size="9pt" ForeColor="#ef7e03" BackColor="#CCCCCC" />
                                                                            <NodeStyle Font-Names="verdana, arial, sans-serif" Font-Size="9pt" ForeColor="#747474" HorizontalPadding="0px" />
                                                                            <RootNodeStyle Font-Names="verdana, arial, sans-serif" Font-Size="9pt" Font-Bold="false" ForeColor="#747474" />
                                                                            <Nodes>
                                                                              <asp:TreeNode Text="What's New" SelectAction="Select" PopulateOnDemand="false" tooltip="View What's New On Kaneva" value="0-0" />
                                                                              <asp:TreeNode Text="Shopping" SelectAction="SelectExpand" PopulateOnDemand="true" tooltip="View Virtual World Items" value="1-0" />
                                                                              <asp:TreeNode Text="Member Homes" SelectAction="Select" PopulateOnDemand="false" tooltip="View Kaneva's Top Homes" value="2-0" />
                                                                              <asp:TreeNode Text="Member Hangouts" SelectAction="Select" PopulateOnDemand="false" tooltip="View kaneva's Top Hangouts" value="3-0" />
                                                                              <asp:TreeNode Text="Showcase" SelectAction="Select" PopulateOnDemand="false" tooltip="View Showcased Items" value="4-0" />
                                                                              <asp:TreeNode Text="Special Features" SelectAction="SelectExpand" PopulateOnDemand="false" tooltip="View Special Features" value="5-0" >
                                                                                <asp:TreeNode Text="Access Pass" SelectAction="Select" PopulateOnDemand="false" tooltip="Learn About The Kaneva Access Pass" value="5-accesspass" />
                                                                                <asp:TreeNode Text="Cover Charge" SelectAction="Select" PopulateOnDemand="false" tooltip="Learn About Cover Charges" value="5-covercharge" />
                                                                                <asp:TreeNode Text="Dance Party 3D" SelectAction="Select" PopulateOnDemand="false" tooltip="Learn About Dance Party 3D" value="5-danceparty" />
                                                                                <asp:TreeNode Text="Emotes & Animations" SelectAction="Select" PopulateOnDemand="false" tooltip="Bring Your Avatar To Life!" value="5-emotes" />
                                                                                <asp:TreeNode Text="Flash Widgets" SelectAction="Select" PopulateOnDemand="false" tooltip="Learn About Our Animates Flash Widgets" value="5-flash" />
                                                                                <asp:TreeNode Text="TV Channels" SelectAction="Select" PopulateOnDemand="false" tooltip="Watch Streaming TV In World1" value="5-tv" />
                                                                                <asp:TreeNode Text="Texture Patterns" SelectAction="Select" PopulateOnDemand="false" tooltip="Pattern Your World" value="5-textures" />
                                                                              </asp:TreeNode>                                                                            
                                                                            </Nodes>
                                                                          </asp:TreeView>																   
                                                                      </ajax:ajaxpanel>
								<!-- END SIDE MENU SECTION --------------------------------------------------------------------->				
																<span class="cb"><span class="cl"></span></span>
															</div>
															
														</td>
														<td valign="top" align="center">
															<div id="divMenuChoiceDisplay" runat="server" visible="false" style="width:99%" >
															</div>
														
															<ajax:ajaxpanel id="Ajaxpanel2" runat="server">															
															<!-- CATALOG SHOPPING SECTION -->
															<div id="divWOKCAtalogItemsResults" runat="server" visible="false" style="width:99%" >
															    
															    <!-- CATALOG HEADER SECTION -->
															    <ajax:ajaxpanel id="ajpSearch1" runat="server">
																
																<!-- basic search -->
																<table cellpadding="5" cellspacing="0" border="0" width="92%" align="center">
																	<tr>
																		
																		<td width="100px"><h4>Find <span id="spnFindTitle" runat="server">Items</span></h4></td>
																		<td width="305px" align="left">
																		    <asp:textbox id="txtKeywords" class="formKanevaText" style="width:300px;" maxlength="100" runat="server"></asp:textbox>
																			<span id="spnSearchInst" runat="server"></span>
																		</td>
																		<td align="left"><asp:button runat="server" id="btnSearch" text="Search" onclick="btnSearch_Click"/></td>
																	</tr>
													                <tr><td colspan="3" style="height:10px"></td></tr>
																</table>
																
																</ajax:ajaxpanel>

															    <table border="0" cellspacing="0" cellpadding="0" width="92%" class="nopadding">
                                                                    <tr>
                                                                        <td width="300" rowspan="2" valign="top"><h1><span id="spnResultsTitle" runat="server">Product Items</span></h1></td>
                                                                        <td height="16" class="searchnav" align="right"><kaneva:searchfilter visible="false" runat="server" id="filterTop" loc="top" hidethumbview="true" showfilterlists="false" assetsperpagelist="12,24,40" /> Showing as: 
                                                                    	    <asp:linkbutton id="lbThumbTop" runat="server" view="Thumb"  onclick="lbViewAs_Click">Thumbnails</asp:linkbutton>&nbsp;| 
																		    <asp:linkbutton id="lbDetailTop" runat="server" view="Detail" onclick="lbViewAs_Click">Details</asp:linkbutton>
																	    </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td height="16">
																		    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                <tr>
                                                                                    <td class="searchnav" align="right" valign="bottom" nowrap>
                                                                                        <kaneva:pager runat="server" isajaxmode="True" id="pgTop" maxpagestodisplay="5" shownextprevlabels="true" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2"><h2><span id="spnSearchFilterDesc" runat="server"></span></h2></td>
                                                                    </tr>
                                                                </table>
                                                                <!-- END CATALOG HEADER SECTION -->
															
															    <!-- BEGIN promotional section -->
															    <div id="divPromotions" runat="server" visible="false" style="width:99%" ></div>
															    <!-- END promotional section -->
																		
															    <!-- MEDIA THUMB VIEW -->
															    <asp:datalist visible="False" runat="server" enableviewstate="False" width="99%" showfooter="False" id="dlAssetsThumb"
																    cellpadding="0" cellspacing="0" border="0" itemstyle-width="25%" itemstyle-horizontalalign="Center" repeatcolumns="4" repeatdirection="Horizontal" cssclass="thumb_table">
																    <itemtemplate>
																	    <div class="framesize-medium">
																		    <div id="Div1" class="passrequired" runat="server" visible='<%# PassRequired (Convert.ToInt32 ( DataBinder.Eval(Container.DataItem, "access_type_id")))%>'></div>
																		    <div class="frame">
																			    <span class="ct"><span class="cl"></span></span>
																			    <div class="imgconstrain">
																				    <a title='View <%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "display_name").ToString ())%>' href='<%# GetWOKDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "global_id")))%>'>
																					    <img border="0" src='<%#GetWOKItemImageURL (DataBinder.Eval(Container.DataItem, "item_image_path").ToString () ,"me")%>' border="0"/>
																				    </a>
																			    </div>	
																			    <span class="cb"><span class="cl"></span></span>
																		    </div>
																	    </div>
																	    <p><a title='View <%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "display_name").ToString ())%>' href='<%# GetWOKDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "global_id")))%>'>
																		    <%# TruncateWithEllipsis (Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "display_name").ToString ()), 16) %></a></p>
																	    <p>Owner:<a href='<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "ownername_nospaces").ToString ()) %>' title="<%# Server.HtmlDecode(DataBinder.Eval(Container.DataItem, "ownername").ToString()) %>"><%# TruncateWithEllipsis (DataBinder.Eval(Container.DataItem, "ownername").ToString(), 10) %></a></p>
																	    <p class="small">Price: <%# DataBinder.Eval (Container.DataItem, "market_cost") %> | <%# DataBinder.Eval(Container.DataItem, "product_availability")%></p>
																	    <p id="P1" class="small" runat="server" visible="<%#IsAdministrator () || IsCSR ()%>">
																		    <asp:hyperlink visible='<%#IsAdministrator ()%>' runat="server" navigateurl='<%#GetWOKEditLink (Convert.ToInt32 ( DataBinder.Eval(Container.DataItem, "global_id")))%>' id="Hyperlink1" name="Hyperlink1">edit</asp:hyperlink>&nbsp;
																	    </p>
																    </itemtemplate>
															    </asp:datalist>
															    <!-- END MEDIA THUMB VIEW -->


															    <!-- MEDIA DETAIL VIEW -->
															    <asp:datalist visible="true" runat="server" enableviewstate="False" showfooter="False" width="590" id="dlAssetsDetail" style="margin-top:10px;"
																    cellpadding="5" cellspacing="0" border="0" itemstyle-horizontalalign="Center" repeatcolumns="2" repeatdirection="Horizontal" cssclass="detail_table">
																    <itemtemplate>	
																	    <div class="module whitebg">
																		    <span class="ct"><span class="cl"></span></span>
																		    <table cellpadding="0" cellspacing="0" border="0" width="280">
																			    <tr>
																				    <td width="85" valign="top" align="center">
																					    <div class="framesize-small">
																						    <div id="Div2" class="restricted" runat="server" visible='<%# PassRequired( Convert.ToInt32(DataBinder.Eval(Container.DataItem, "access_type_id")) )%>'></div>
																						    <div class="frame">
																							    <span class="ct"><span class="cl"></span></span>
																							    <div class="imgconstrain">
																								    <a title='View <%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "display_name").ToString ())%>' href='<%# GetWOKDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "global_id")))%>'>
																								    <img border="0" src='<%#GetWOKItemImageURL (DataBinder.Eval(Container.DataItem, "item_image_path").ToString () ,"sm")%>' border="0"/></a>
																							    </div>	
																							    <span class="cb"><span class="cl"></span></span>
																						    </div>
																					    </div>
																				    </td>																		 
																				    <td align="left" valign="top" width="264" style="padding-bottom:0px;">
																					    <p><a href='<%# GetWOKDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "global_id")))%>' title='View <%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "display_name").ToString ())%>'><%# TruncateWithEllipsis (Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "display_name").ToString ()), 20) %></a></p>
																					    <div style="height:56px;">
																					    <p><%# TruncateWithEllipsis (Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "description").ToString ()), 85) %></p>
																					    </div>
																				    </td>
																			    </tr>
																			    <tr>
																				    <td colspan="2" style="padding:0 5px 0 10px;">
																					    <table cellpadding="0" cellspacing="0" width="100%" border="0" class="stats">
																						    <tr>
																							    <td align="left" width="36%" style="padding-top:0px;">
																								    <p class="small">Price: <%# (Convert.ToDouble(DataBinder.Eval(Container.DataItem, "market_cost"))).ToString("N0")%></p>
																								    <p class="small"> <%# (DataBinder.Eval(Container.DataItem, "product_availability")).ToString()%></p>
																							    </td>
																							    <td>
																								    <table cellpadding="0" cellspacing="0" width="100%" border="0" class="stats">
																									    <tr>
																										    <td align="right" width="85%">
																											    <p class="small">Owner:</p>
																											    <p class="small"><a href='<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "ownername_nospaces").ToString ()) %>' title="<%# Server.HtmlDecode(DataBinder.Eval(Container.DataItem, "ownername").ToString()) %>"><%# TruncateWithEllipsis (Server.HtmlDecode(DataBinder.Eval(Container.DataItem, "ownername").ToString()), 15) %></a></p>
																										    </td>
																										    <td>
																										        <a href='<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "ownername_nospaces").ToString ()) %>' style="cursor:hand;" title="<%# Server.HtmlDecode(DataBinder.Eval(Container.DataItem, "ownername").ToString()) %>">
																											    <img id="Img5" runat="server" src='<%#GetProfileImageURL (DataBinder.Eval(Container.DataItem, "thumbnail_medium_path").ToString (), "me", "M")%>' width="30" height="30" border="0" hspace="5"/></a>
																										    </td>
																									    </tr>
																								    </table>
																							    </td>	
																						    </tr>
																					    </table>	
																				    </td>
																			    </tr>
																		    </table>
																		    <span class="cb"><span class="cl"></span></span>
																	    </div>
    																	
																    </itemtemplate>
															    </asp:datalist>
															
															    <!-- END MEDIA DETAIL VIEW -->
    													 
                                                                <!-- div for no results found -->
															    <div id="divNoResults" runat="server" visible="false"></div>
    						
    															
															    <div class="formspacer"></div>
    															
															    <!-- CATALOG FOOTER SECTION -->
															    <table cellpadding="0" cellspacing="0" border="0" width="92%">
																    <tr>
																	    <td align="left">
																		    <asp:label id="lblResultsBottom" runat="server" visible="false"></asp:label>
																	    </td>
    																	
																	    <td class="searchnav" align="right" valign="bottom" nowrap>
																		    <kaneva:pager runat="server" isajaxmode="True" id="pgBottom" maxpagestodisplay="5" shownextprevlabels="true" />
																	    </td>
																    </tr>
															    </table>
															    <!-- END CATALOG FOOTER SECTION -->
    									
															    <div class="formspacer"></div>
															
															</div>
															<!-- END CATALOG SHOPPING SECTION --> 	
																													
															</ajax:ajaxpanel>

														</td>
													</tr>
												</table>
												
											</td>
											
											<td width="10"></td>
											
											<td width="175" align="right" valign="top">
												<div class="module whitebg" id="hotTips">
													<span class="ct"><span class="cl"></span></span>
														<h2>Quick Tips</h2>
													
                                                          <ul>
															<li>
																<a href="javascript:void(0);" onClick="ToggleHottip ('d1')">Show Off Your Stuff</a>
																<div id="d1" style="display:block;" class="content" runat="server">
																	<img id="Img6" src="~/images/hottip_youtube.png" runat="server" alt="Add YouTube Videos" width="90" height="44" border="0" class="png" />
																	
																	<p>Share your favorite videos with your friends in your 3D home.</p>
                                                                    <div style="margin-left:10px;">
																		<p style="text-indent:-10px;">&bull; In the World of Kaneva, right-click on your TV, and click <b>Choose Media</b>.</p>
																		<p style="text-indent:-10px;">&bull; Choose your favorite playlist from <b>My Playlists</b>, or pick a playlist from the <b>Entertainment</b> tab.</p>
																		<p style="text-indent:-10px;">&bull; Invite your friends over to enjoy the show!</p>
                                                                    </div>
																</div>
																
															</li>
															<li>
																<a href="javascript:void(0);" onClick="ToggleHottip ('d2')">Add Photos & Patterns</a>
																<div id="d2" style="display:none;" class="content" runat="server">
																	<img id="Img7" src="~/images/hottip_patterns.png" runat="server" alt="hottip_patterns.png" width="100" height="40" border="0" class="png" />
																	<p>Make your 3D Home your own . . . you can now decorate furniture, walls and floors with your favorite 
																	photo or pattern. To convert a photo: Simply edit the photo and change the �Item Type� to �Pattern�.</p>
																</div>
																
															</li>
															<li class="last">
																<a href="javascript:void(0);" onClick="ToggleHottip ('d3')">Add and Share Media</a>
																<div id="d3" style="display:none;" class="content" runat="server">
																	<img id="Img8" src="~/images/hottip_add.png" runat="server" alt="hottip_add.png" width="140" height="40" border="0" class="png" />
																	<p>Check out the latest and most-raved videos, photos, music, and games. See something you like? Add it to your own media library by clicking �Add to my library�.</p>
																</div>
																
															</li>
														</ul>
														
														
														
													<span class="cb"><span class="cl"></span></span>
												</div>
												
												<div class="module whitebg">
													<span class="ct"><span class="cl"></span></span>
														<table id="_ctl97_tblGoogleAdds" cellpadding="0" cellspacing="0">
															<tr>
																<td align="center" style="padding-bottom:5px">
																<div id="goog">
																<script type="text/javascript"><!--
																	google_ad_client = "pub-7044624796952740";
																	//google_ad_client = "pub-googleIamTesting";
																	google_ad_width = 160;
																	google_ad_height = 600;
																	google_ad_format = "160x600_as";
																	google_ad_type = "text";
																	//2006-09-26: Search Results
																	google_ad_channel ="5505473784";
																	google_color_border = "FFFFFF";
																	google_color_bg = "FFFFFF";
																	google_color_link = "018AAA";
																	google_color_text = "333333";
																	google_color_url = "666666";
																	<asp:Literal runat="server" id="litAdTest"/>;
																	//--></script>
																	<script type="text/javascript"
																	src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
																</script>
																</div>
																</td>
															</tr>
														</table>
														
													<span class="cb"><span class="cl"></span></span>
												</div>
											</td>
										</tr>
									</table>
		
								</td>
							</tr>
						</table>
					</td>
					<td class="frBorderRight"><img id="Img9" runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
				</tr>
				<tr>
					<td class="frBottomLeft"></td>
					<td class="frBottom"></td>
					<td class="frBottomRight"></td>
				</tr>
			</table>				
		</td>
	</tr>
</table>
				
<script type="text/javascript">
var open = '<asp:literal id="litJSOpenConst" runat="server"/>';

function ToggleHottip (element)
{
	if (open == 'd1' && element != 'd1')
	{
		Effect.toggle('d1','slide' , {duration:.3});
		Effect.toggle(element, 'slide', {duration:.3});
	}
	else if (open == 'd2' && element != 'd2')
	{
		Effect.toggle('d2','slide' , {duration:.3});
		Effect.toggle(element, 'slide', {duration:.3});
	}
	else if (open == 'd3' && element != 'd3')
	{
		Effect.toggle('d3','slide' , {duration:.3});
		Effect.toggle(element, 'slide', {duration:.3});
	}
	
	open = element; 

	
	
	return false;
}
</script>