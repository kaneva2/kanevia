<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="starsDirectory.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.starsDirectory" %>
<%@ Register TagPrefix="Kaneva" TagName="SearchFilter" Src="../usercontrols/SearchFilter.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../usercontrols/Pager.ascx" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>

<link href="../css/new.css" type="text/css" rel="stylesheet">
<link href="../css/shadow.css" type="text/css" rel="stylesheet">
<!--[if IE]> <style type="text/css">@import "../css/new_IE.css";</style> <![endif]-->

<script src="../jscript/prototype.js" type="text/javascript"></script>
<script src="../jscript/scriptaculous/scriptaculous.js" type="text/javascript"></script>

<table cellpadding="0" cellspacing="0" border="0" width="990" align="center">
	<tr>
		<td colspan="3"><img id="Img1" runat="server" src="~/images/spacer.gif" width="1" height="14" /></td>
	</tr>
	<tr>
		<td colspan="3">
			<table  border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="frTopLeft"></td>
					<td class="frTop"></td>
					<td class="frTopRight"></td>
				</tr>
				<tr>
					<td bgcolor="#f1f1f2" class="frBorderLeft"><img id="Img2" runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
					<td valign="top" bgcolor="#f1f1f2" class="frBgIntMembers">
						<table width="100%" border="0" cellpadding="0" cellspacing="0" >
							<tr colspan="2">
								<td id="entertainment">
                                    <div id="mainHeader">
	                                    <table width="930" border="0" cellpadding="0" cellspacing="0" id="headerContent">
		                                    <tr>
			                                    <td  align="left" width="702">
				                                    <h1>STARs</h1>
				                                    <asp:adrotator id="arHeader" runat="server" OnAdCreated="AdCreated"  />
												    <h2><asp:literal id="litHeaderText" runat="server"></asp:literal></h2>
				                                   
		                                      </td>
			                                    <td width="44"></td>
		                                      <td width="216" valign="bottom" align="right"><a id="aRegister" runat="server" class="nohover"><img runat="server" src="~/images/header/indexpages/burgundy-add.png" id="imgRegister" width="215" height="65" border="0" class="png" /></a><br>
			                                    <h4>Now Playing:<img id="Img3" runat="server" src="~/images/spacer.gif" width="14" height="8"></h4>
			                                    <h3><span class="stat"><asp:label id="lblCount" runat="server" /></span><img id="Img4" runat="server" src="~/images/spacer.gif" width="14" height="8"></h3></td>
		                                    </tr>
                                      </table>
                                    </div>      
								</td>
							</tr>
							<tr>
								<td width="968" align="left"  valign="top">
									
									<table cellpadding="0" cellspacing="0" border="0" width="100%">
										<tr>
											<td width="783" valign="top">
											
												<table border="0" cellspacing="0" cellpadding="0" width="100%">
													<tr>
														<td align="center">
												
															<div class="module">
																<span class="ct"><span class="cl"></span></span>
																
																<ajax:ajaxpanel id="ajpSearch1" runat="server">
																
																<!-- basic search -->
																<table cellpadding="0" cellspacing="0" border="0" width="98%" align="center">
																	<tr>
																		
																		<td valign="top" width="380">
																			
																			<table cellpadding="0" cellspacing="0" border="0">
																				
																				<tr>
																					<td>
																						<table cellpadding="5" cellspacing="0" border="0">
																							<tr>
																								<td width="122"><h4>Find <span id="spnFindTitle" runat="server">Videos</span></h4></td>
																								<td><asp:textbox id="txtKeywords" class="formKanevaText" style="width:250px;" maxlength="50" runat="server"></asp:textbox>
																									<span id="spnSearchInst" runat="server"></span></td>
																							</tr>
																						</table>
																					</td>
																				</tr>
																			</table>
																			
																		</td>
																		<td width="90" align="center"><asp:button runat="server" id="btnSearch" text="Search" onclick="btnSearch_Click"/></td>
																		<td valign="top" class="small">
																			<div id="divRestricted" runat="server">Show Restricted Content: <asp:label id="lblShowRestricted" runat="server">No</asp:label>
																			<asp:hyperlink id="hlChangeProfile" runat="server">Change Profile Setting</asp:hyperlink></div>
																			<asp:checkbox id="chkPhotoRequired" runat="server" checked />Must Contain Thumbnail Photo<br/>

																		</td>
																	</tr>
													
																</table>
																
																</ajax:ajaxpanel>
																
																<span class="cb"><span class="cl"></span></span>
															</div>
														</td>
													</tr>
												</table>
												
												
												<table border="0" cellspacing="0" cellpadding="0" width="100%">
													<tr>
														<td width="180" valign="top">
															<div class="module whitebg">
																<span class="ct"><span class="cl"></span></span>
																
																<ajax:ajaxpanel id="Ajaxpanel1" runat="server">
																
																<div id="sortlinks">																	
																													
																	<ul>
																		<li class="header">Categories</li>
																		<asp:repeater id="rptCategories" runat="server">
																		<itemtemplate>
																			<li id="liCategory" runat="server"><asp:linkbutton id="lbCategory" runat="server" /></li>		
																		</itemtemplate>		   
																		</asp:repeater>
																	</ul>
                                                					<ul id="ulTime" runat="server">
																		<li class="header">Time</li>
																		<li id="liTime_Today" runat="server"><asp:linkbutton text="Today" id="lbTime_Today" commandname="today" oncommand="lbTime_Click" tooltip="View communities added today" runat="server" /></li>
																		<li id="liTime_Week" runat="server"><asp:linkbutton text="This Week" id="lbTime_Week" commandname="week" oncommand="lbTime_Click" tooltip="View communities added within the past week" runat="server" /></li>
																		<li id="liTime_Month" runat="server"><asp:linkbutton text="This Month" id="lbTime_Month" commandname="month" oncommand="lbTime_Click" tooltip="View communities added within the past month" runat="server" /></li>
																		<li id="liTime_All" runat="server"><asp:linkbutton text="All Time" id="lbTime_All" commandname="alltime" oncommand="lbTime_Click" tooltip="View all communities" runat="server" /></li>
																	</ul>
																	
																	<ul>
																		<li class="header" id="liBrowseLabel" runat="server">Popularity</li>
																		<li id="liBrowse_Population" runat="server"><asp:linkbutton text="Relevance" id="lbBrowse_Population" commandname="population" oncommand="lbBrowse_Click" tooltip="View results by current population" runat="server" /></li>
																		<li id="liBrowse_Raves" runat="server"><asp:linkbutton text="Most Raves" id="lbBrowse_Raves" commandname="raves" oncommand="lbBrowse_Click" tooltip="View results by number of raves" runat="server" /></li>
																		<li id="liBrowse_Visits" runat="server"><asp:linkbutton text="Most Views" id="lbBrowse_Visits" commandname="visits" oncommand="lbBrowse_Click" tooltip="View results by number of visits" runat="server" /></li>
																	</ul>
																	
																</div>
																
																</ajax:ajaxpanel>
																
																<span class="cb"><span class="cl"></span></span>
															</div>
															
														</td>
														<td valign="top" align="center">
														
															<ajax:ajaxpanel id="Ajaxpanel2" runat="server">
															
															<table border="0" cellspacing="0" cellpadding="0" width="92%" class="nopadding">
                                                                <tr>
                                                                    <td width="170" rowspan="2"><h1><span id="spnResultsTitle" runat="server">STARs</span></h1></td>
                                                                    <td height="16" class="searchnav" align="right"><kaneva:searchfilter visible="false" runat="server" id="filterTop" loc="top" hidethumbview="true" showfilterlists="false" assetsperpagelist="12,24,40" /> Showing as: 
                                                                    	<asp:linkbutton id="lbThumbTop" runat="server" view="Thumb"  onclick="lbViewAs_Click">Thumbnails</asp:linkbutton>&nbsp;| 
																		<asp:linkbutton id="lbDetailTop" runat="server" view="Detail" onclick="lbViewAs_Click">Details</asp:linkbutton>
																	</td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="16">
																		<table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                            <tr>
                                                                                <td class="searchnav" align="right" valign="bottom" nowrap>
                                                                                    <kaneva:pager runat="server" isajaxmode="True" id="pgTop" maxpagestodisplay="5" shownextprevlabels="true" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2"><h2><span id="spnSearchFilterDesc" runat="server"></span></h2></td>
                                                                </tr>
                                                            </table>
																																											  
															<!-- MEDIA THUMB VIEW -->
															<asp:datalist visible="False" runat="server" enableviewstate="True" 
																width="99%" showfooter="False" id="dlSTARsThumb"
																cellpadding="0" cellspacing="0" border="0" itemstyle-width="25%" 
																itemstyle-horizontalalign="Center" repeatcolumns="4" 
																repeatdirection="Horizontal" cssclass="thumb_table">
																
																<itemtemplate>
																																											
																	<div class="framesize-medium">
																		<div id="Div1" class="restricted" runat="server" visible='<%# IsMature (Convert.ToInt32 ( DataBinder.Eval(Container.DataItem, "asset_rating_id")))%>'></div>
																		<div class="frame">
																			<span class="ct"><span class="cl"></span></span>
																			<div class="imgconstrain">
																				<a title='View <%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "name").ToString ())%>' href='<%# GetAssetDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "asset_id")))%>'>
																					<img border="0" src='<%#GetMediaImageURL (DataBinder.Eval(Container.DataItem, "thumbnail_medium_path").ToString () ,"me", Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_id")), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_type_id")), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "thumbnail_gen")))%>' border="0"/>
																				</a>
																			</div>	
																			<span class="cb"><span class="cl"></span></span>
																		</div>
																	</div>
																	<p><a title='View <%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "name").ToString ())%>' href='<%# GetAssetDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "asset_id")))%>'>
																		<%# TruncateWithEllipsis (Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "name").ToString ()), 16) %></a></p>
																	<p>Owner:<a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "owner_name_no_spaces").ToString ()) %> title="<%# Server.HtmlDecode(DataBinder.Eval(Container.DataItem, "owner_username").ToString()) %>"><%# TruncateWithEllipsis (DataBinder.Eval(Container.DataItem, "owner_username").ToString(), 10) %></a></p>
																	<p class="small">Views: <%# DataBinder.Eval (Container.DataItem, "number_of_downloads") %> | Raves: <%# DataBinder.Eval (Container.DataItem, "number_of_diggs") %></p>
																	<p id="P1" class="small" runat="server" visible="<%#IsAdministrator () || IsCSR ()%>">																 
																		<asp:hyperlink visible='<%#IsAdministrator () || IsCSR ()%>' runat="server" navigateurl='<%#GetAssetEditLink (Convert.ToInt32 ( DataBinder.Eval(Container.DataItem, "asset_id")))%>' id="Hyperlink1" name="Hyperlink1">edit</asp:hyperlink>&nbsp;
																		<asp:hyperlink visible='<%#IsAdministrator () || IsCSR ()%>' runat="server" navigateurl='<%#GetDeleteScript ((int) DataBinder.Eval(Container.DataItem, "asset_id")) %>' id="Hyperlink2" name="Hyperlink2">delete</asp:hyperlink>&nbsp;
																		<asp:hyperlink visible="<%#IsAdministrator () || IsCSR ()%>" runat="server" navigateurl='<%# "javascript:RestrictAsset(" + DataBinder.Eval (Container.DataItem, "asset_id").ToString () + ");" %>' id="lnkRestrict">restrict</asp:hyperlink>
																	</p>																																			 
																																					
																</itemtemplate>																					
															</asp:datalist>
															<!-- END MEDIA THUMB VIEW -->


															<!-- MEDIA DETAIL VIEW -->
															<asp:datalist visible="true" runat="server" enableviewstate="False" showfooter="False" width="590" id="dlSTARsDetail" style="margin-top:10px;"
																cellpadding="5" cellspacing="0" border="0" itemstyle-horizontalalign="Center" repeatcolumns="2" repeatdirection="Horizontal" cssclass="detail_table">
																
																<itemtemplate>	
															 		
																	<div class="module whitebg">
																		<span class="ct"><span class="cl"></span></span>
																		<table cellpadding="0" cellspacing="0" border="0" width="280">
																			<tr>
																				<td width="85" valign="top" align="center">
																					<div class="framesize-small">
																						<div id="Div2" class="restricted" runat="server" visible='<%# IsMature( Convert.ToInt32(DataBinder.Eval(Container.DataItem, "asset_rating_id")) )%>'></div>
																						<div class="frame">
																							<span class="ct"><span class="cl"></span></span>
																							<div class="imgconstrain">
																								<a title='View <%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "name").ToString ())%>' href='<%# GetAssetDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "asset_id")))%>'>
																								<img border="0" src='<%#GetMediaImageURL (DataBinder.Eval(Container.DataItem, "thumbnail_small_path").ToString () ,"sm", Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_id")), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_type_id")), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "thumbnail_gen")))%>' border="0"/></a>
																							</div>	
																							<span class="cb"><span class="cl"></span></span>
																						</div>
																					</div>
																				</td>																		 
																				<td align="left" valign="top" width="264" style="padding-bottom:0px;">
																					<p><a href='<%# GetAssetDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "asset_id")))%>' title='View <%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "name").ToString ())%>'><%# TruncateWithEllipsis (Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "name").ToString ()), 20) %></a></p>
																					<div style="height:56px;">
																					<p><%# TruncateWithEllipsis (Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "teaser").ToString ()), 85) %></p>
																					</div>
																				</td>
																			</tr>
																			<tr>
																				<td colspan="2" style="padding:0 5px 0 10px;">
																					<table cellpadding="0" cellspacing="0" width="100%" border="0" class="stats">
																						<tr>
																							<td align="left" width="36%" style="padding-top:0px;">
																								<p class="small">Raves: <%# (Convert.ToDouble(DataBinder.Eval (Container.DataItem, "number_of_diggs"))).ToString("N0") %></p>
																								<p class="small">Views: <%# (Convert.ToDouble(DataBinder.Eval (Container.DataItem, "number_of_downloads"))).ToString("N0") %></p>
																							</td>
																							<td>
																								<table cellpadding="0" cellspacing="0" width="100%" border="0" class="stats">
																									<tr>
																										<td align="right" width="85%">
																											<p class="small">Owner:</p>
																											<p class="small"><a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "owner_name_no_spaces").ToString ()) %> title="<%# Server.HtmlDecode(DataBinder.Eval(Container.DataItem, "owner_username").ToString()) %>"><%# TruncateWithEllipsis (Server.HtmlDecode(DataBinder.Eval(Container.DataItem, "owner_username").ToString()), 15) %></a></p>
																										</td>
																										<td>
																											<a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "owner_name_no_spaces").ToString ()) %> style="cursor:hand;" title="<%# Server.HtmlDecode(DataBinder.Eval(Container.DataItem, "owner_username").ToString()) %>">
																											<img id="Img5" runat="server" src='<%#GetProfileImageURL (DataBinder.Eval(Container.DataItem, "owner_thumbnail_small_path").ToString (), "me", "M")%>' width="30" height="30" border="0" hspace="5"/></a>
																										</td>
																									</tr>
																								</table>
																							</td>	
																						</tr>
																					</table>	
																				</td>
																			</tr>
																		</table>
																		<span class="cb"><span class="cl"></span></span>
																	</div>
																	
																</itemtemplate>
															</asp:datalist>
															<!-- END MEDIA DETAIL VIEW -->
													 

															<div id="divNoResults" runat="server" visible="false"></div>
						
															
															<div class="formspacer"></div>
															
															<table cellpadding="0" cellspacing="0" border="0" width="92%">
																<tr>
																	<td align="left">
																		<asp:label id="lblResultsBottom" runat="server" visible="false"></asp:label>
																	</td>
																	
																	<td class="searchnav" align="right" valign="bottom" nowrap>
																		<kaneva:pager runat="server" isajaxmode="True" id="pgBottom" maxpagestodisplay="5" shownextprevlabels="true" />
																	</td>
																</tr>
															</table>
									
															<div class="formspacer"></div>
															
															<!-- TAG CLOUD -->
															<!--<table border="0" cellspacing="0" cellpadding="0" width="95%">
																<tr>
																	<td>
																		<div class="module whitebg">
																			<span class="ct"><span class="cl"></span></span>
																			<h2>Popular <span id="spnTagCloudTitle" runat="server"></span> Tags</h2>
																			<iframe id="ifrmTagCloud" runat="server" src="../usercontrols/TagCloud.aspx?tag=" scrolling="no" height="110" width="99%" frameborder="0"></iframe>	   
																			<span class="cb"><span class="cl"></span></span>
																		</div>
																	</td>
																</tr>
															</table>-->
															<!-- END TAG CLOUD -->
															
															</ajax:ajaxpanel>

														</td>
													</tr>
												</table>
												
											</td>
											
											<td width="10"></td>
											
											<td width="175" align="right" valign="top">
												<div class="module whitebg" id="hotTips">
													<span class="ct"><span class="cl"></span></span>
														<h2>Quick Tips</h2>
													
                                                          <ul>
															<li>
																<a href="javascript:void(0);" onClick="ToggleHottip ('d1')">Show Off Your Stuff</a>
																<div id="d1" style="display:block;" class="content" runat="server">
																	<img id="Img6" src="~/images/hottip_youtube.png" runat="server" alt="Add YouTube Videos" width="90" height="44" border="0" class="png" />
																	
																	<p>Share your favorite videos with your friends in your 3D home.</p>
                                                                    <div style="margin-left:10px;">
																		<p style="text-indent:-10px;">&bull; In the World of Kaneva, right-click on your TV, and click <b>Choose Media</b>.</p>
																		<p style="text-indent:-10px;">&bull; Choose your favorite playlist from <b>My Playlists</b>, or pick a playlist from the <b>Entertainment</b> tab.</p>
																		<p style="text-indent:-10px;">&bull; Invite your friends over to enjoy the show!</p>
                                                                    </div>
																</div>
																
															</li>
															<li>
																<a href="javascript:void(0);" onClick="ToggleHottip ('d2')">Add Photos & Patterns</a>
																<div id="d2" style="display:none;" class="content" runat="server">
																	<img id="Img7" src="~/images/hottip_patterns.png" runat="server" alt="hottip_patterns.png" width="100" height="40" border="0" class="png" />
																	<p>Make your 3D Home your own . . . you can now decorate furniture, walls and floors with your favorite 
																	photo or pattern. To convert a photo: Simply edit the photo and change the &quot;Item Type&quot; to &quot;Pattern&quot;.</p>
																</div>
																
															</li>
															<li class="last">
																<a href="javascript:void(0);" onClick="ToggleHottip ('d3')">Add and Share Media</a>
																<div id="d3" style="display:none;" class="content" runat="server">
																	<img id="Img8" src="~/images/hottip_add.png" runat="server" alt="hottip_add.png" width="140" height="40" border="0" class="png" />
																	<p>Check out the latest and most-raved videos, photos, music, and games. See something you like? Add it to your own media library by clicking &quot;Add to my library&quot;.</p>
																</div>
																
															</li>
														</ul>
														
														
														
													<span class="cb"><span class="cl"></span></span>
												</div>												
												
												<div class="module whitebg">
													<span class="ct"><span class="cl"></span></span>
														<table id="_ctl97_tblGoogleAdds" cellpadding="0" cellspacing="0">
															<tr>
																<td align="center" style="padding-bottom:5px">
																<div id="goog">
																<script type="text/javascript"><!--
																	google_ad_client = "pub-7044624796952740";
																	//google_ad_client = "pub-googleIamTesting";
																	google_ad_width = 160;
																	google_ad_height = 600;
																	google_ad_format = "160x600_as";
																	google_ad_type = "text";
																	//2006-09-26: Search Results
																	google_ad_channel ="5505473784";
																	google_color_border = "FFFFFF";
																	google_color_bg = "FFFFFF";
																	google_color_link = "018AAA";
																	google_color_text = "333333";
																	google_color_url = "666666";
																	<asp:Literal runat="server" id="litAdTest"/>;
																	//--></script>
																	<script type="text/javascript"
																	src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
																</script>
																</div>
																</td>
															</tr>
														</table>
														
													<span class="cb"><span class="cl"></span></span>
												</div>
											</td>
										</tr>
									</table>
		
								</td>
							</tr>
						</table>
					</td>
					<td class="frBorderRight"><img id="Img9" runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
				</tr>
				<tr>
					<td class="frBottomLeft"></td>
					<td class="frBottom"></td>
					<td class="frBottomRight"></td>
				</tr>
			</table>				
		</td>
	</tr>
</table>

<input type="hidden" runat="server" id="AssetId" value="0" name="AssetId">
<asp:button id="DeleteAsset" onclick="Delete_Asset" runat="server" visible="false"></asp:button>

<ajax:ajaxpanel ID="Ajaxpanel3" runat="server">
<asp:button id="btnRestrictAsset" onclick="btnRestrictAsset_Click" runat="server" style="display:none;"></asp:button>
</ajax:ajaxpanel>
				
<script type="text/javascript">
var open = '<asp:literal id="litJSOpenConst" runat="server"/>';

function ToggleHottip (element)
{
	if (open == 'd1' && element != 'd1')
	{
		Effect.toggle('d1','slide' , {duration:.3});
		Effect.toggle(element, 'slide', {duration:.3});
	}
	else if (open == 'd2' && element != 'd2')
	{
		Effect.toggle('d2','slide' , {duration:.3});
		Effect.toggle(element, 'slide', {duration:.3});
	}
	else if (open == 'd3' && element != 'd3')
	{
		Effect.toggle('d3','slide' , {duration:.3});
		Effect.toggle(element, 'slide', {duration:.3});
	}
	
	open = element; 

	
	
	return false;
}
</script>