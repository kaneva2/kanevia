///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections;
using KlausEnt.KEP.Kaneva.framework.kgp;
using MagicAjax;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for commDrillDown.
	/// </summary>
	public class storeCatalog : SortedBasePage
	{
		protected storeCatalog () 
		{
			Title = "WOK Stores";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			// They must be logged in
			if (!Request.IsAuthenticated)
			{
				Response.Redirect (GetLoginURL ());
				return;
			}

			if (!IsPostBack)
			{
				//setup header nav bar
				HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.VIRTUAL_WORLD;

				BindData(1);
			}
			else
			{

			}
		}

		/// <summary>
		/// GetCommunityEditLink
		/// </summary>
		protected string GetCommunityEditLink (int communityId)
		{
			return ResolveUrl ("~/community/commEdit.aspx?communityId=" + communityId);
		}

		/// <summary>
		/// pg_PageChange
		/// </summary>
		private void pg_PageChange (object sender, PageChangeEventArgs e)
		{
			pgTop.CurrentPageNumber = e.PageNumber;
			pgTop.DrawControl ();
			pgBottom.CurrentPageNumber = e.PageNumber;
			pgBottom.DrawControl ();
			BindStoreData (e.PageNumber);
		}

		protected string GetAssetImageURL( string globalId )
		{
			// hmmm where, how, when
			return "";
		}

		/// <summary>
		/// Click the search button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void imgSearch_Click (object sender, ImageClickEventArgs e) 
		{
			pgTop.CurrentPageNumber = 1;
			pgBottom.CurrentPageNumber = 1;
			BindStoreData (1);
		}

		/// <summary>
		///  Bind the default data
		/// </summary>
		private void BindDefaultData()
		{
			dgrdCatalog.Visible = false;
			
			pgTop.NumberOfPages = "0";
			pgBottom.NumberOfPages = "0";
			pgTop.DrawControl ();
			pgBottom.DrawControl ();
		}

		/// <summary>
		///  Bind the data
		/// </summary>
		/// <param name="pageNumber"></param>
		private void BindData (int pageNumber)
		{

			DataTable dt = WOKStoreUtility.GetStores();
			ddlStores.DataSource = dt;
			ddlStores.DataTextField = "name";
			ddlStores.DataValueField = "store_id";
			ddlStores.DataBind();
			if ( dt.Rows.Count > 0 )
			{
				ddlStores.SelectedIndex = 0;
				BindStoreData( pageNumber );
			}
			else
				ddlStores.SelectedIndex = -1;

		}

		private void BindStoreData( int pageNumber )
		{
			CurrentSort = "name";
			CurrentSortOrder = "ASC";

			// Set the sortable columns
			SetHeaderSortText (dgrdCatalog);
			string orderby = CurrentSort + " " + CurrentSortOrder;


			// Set up search
			int pgSize = 4;

			PagedDataTable pds = WOKStoreUtility.GetStoreContents( Convert.ToInt32( ddlStores.SelectedValue ), pageNumber, pgSize );

			/*
			PagedDataTable pds = WOKSStoreUtility.GiftCatalog( orderby, pageNumber, pgSize);
			*/
		
			pgTop.NumberOfPages = Math.Ceiling ((double) pds.TotalCount / pgSize).ToString ();
			pgTop.DrawControl ();

			pgBottom.NumberOfPages = Math.Ceiling ((double) pds.TotalCount / pgSize).ToString ();
			pgBottom.DrawControl ();

			dgrdCatalog.DataSource = pds;
			dgrdCatalog.DataBind ();
			dgrdCatalog.Visible = true;
			tblSearchResults.Visible = true;

			// The results
			lblSearch.Text = GetResultsText (pds.TotalCount, pageNumber, pgSize, pds.Rows.Count);
		}

		protected string GetStoreItemToolTip( string id, string name )
		{
			return "This is " + name + " with id of " + id;
		}

		# region Event Handlers

		protected void StoreChanged(Object sender, EventArgs e)
		{
			BindStoreData( 1 );
		}

		protected void dgrdCatalog_ItemCommand (object sender, DataGridCommandEventArgs  e)
		{
			// to nothing when selected for now
		}

		#endregion

		/// <summary>
		/// DEFAULT_SORT
		/// </summary>
		/// <returns></returns>
		protected override string DEFAULT_SORT
		{
			get
			{
				return "name";
			}
		}

		protected string GetPrice( string price )
		{
			return price + Constants.CURR_KPOINT_SYMBOL_HTML;
		}

		protected Kaneva.Pager pgTop, pgBottom;

		protected DataGrid dgrdCatalog;

		protected Label lblSearch;

		protected Label lblChannelTags;
		protected HyperLink hlMature;
		protected LinkButton lbPopularMore;

		protected DropDownList ddlStores;

		// Search
		protected CheckBox chkRestricted;
		protected TextBox txtKeywords, txtZipcode;
		protected HtmlAnchor aTooltip;
		protected HtmlGenericControl sRestictedText;
		protected HtmlTable tblSearchResults;

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
			pgTop.PageChanged +=new PageChangeEventHandler (pg_PageChange);
			pgBottom.PageChanged +=new PageChangeEventHandler (pg_PageChange);
		}
		#endregion
	}
}
