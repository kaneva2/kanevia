///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;

namespace KlausEnt.KEP.Kaneva
{
	public class KanevaTray: System.Web.UI.Page
	{
		private static Dictionary<string, string> gotoUrls = staticInit();

		private static Dictionary<string, string> staticInit()
		{
			Dictionary<string, string> map = new Dictionary<string,string>();
			map.Add("home", "~");
			map.Add("register", "~/register/kaneva/registerInfo.aspx");
			map.Add("buycredits", "~/mykaneva/buyCreditPackages.aspx");
			map.Add("shop", "http://" + KanevaGlobals.ShoppingSiteName);
			map.Add("games", "~/watch/watch.kaneva?type=1");
			map.Add("communities", "~/bookmarks/channelTags.aspx");
			map.Add("friends", "~/mykaneva/friendsGroups.aspx");
			map.Add("profile", "~/channel/{0}.people");
			return map;
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (Request.QueryString["goto"] != null)
			{
				string link = Request.QueryString["goto"].ToLower();
				if (gotoUrls.ContainsKey(link))
				{
					string url = gotoUrls[link];
					bool resolved = true;

					int argIdx = 0;
					string argToken = "{" + argIdx + "}";

					while (url.Contains( argToken ))
					{
						string value = Request.QueryString["arg" + argIdx];
						if (value == null)
						{
							resolved = false;
							break;
						}

						url = url.Replace(argToken, value);
						argIdx++;
						argToken = "{" + argIdx + "}";
					}

					if( resolved )
					{
						Response.Redirect( url );
					}
				}
			}

			Response.Redirect(gotoUrls["home"]);
		}
	}
}