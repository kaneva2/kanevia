<%@ Page language="c#" MasterPageFile="~/MASTERPAGES/GenericLandingPageTemplate.Master" CodeBehind="loginSecure.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.loginSecure" %>
<%@ Register TagPrefix="Kaneva" TagName="HeaderText" Src="usercontrols/HeaderText.ascx" %>
<%@ PreviousPageType VirtualPath="~/Home.aspx" %>

<asp:Content ID="cntHeaderCSS" runat="server" contentplaceholderid="cphHeadCSS" >
    <link href="css/sign-in/signIn.css" rel="stylesheet" type="text/css" /> 
	<link href="css/base/buttons_new.css" rel="stylesheet" type="text/css" />  
</asp:Content>

<asp:Content id="cntBody" runat="server" contentplaceholderid="cphBody" >
    <div class="content" id="divLoginInfo">       
        <div>
		    <asp:validationsummary cssclass="errBox black" id="valSum" runat="server" showmessagebox="False" showsummary="True"
			    displaymode="BulletList" headertext="Please correct the following error(s):" forecolor="#333333"></asp:validationsummary>
		    <asp:customvalidator id="cvBlank" runat="server" display="None" enableclientscript="False"></asp:customvalidator>
	    </div>
	    
		<asp:Panel id="pnlLoginForm" DefaultButton="btnLogin" CssClass="signinForm" runat="server"> 
		    <h1 class="title">Sign In to Kaneva</h1>
		    
			<div>
				<label id="lblEmail" runat="server" for='<%=txtUserName.ClientID %>'>Email</label>
				<input type="text" maxlength="100" id="txtUserName" runat="server" name="email" tabindex="1"/>
			</div>
			
			<div class="lostpw"><a runat="server" id="aLostPassword">Forgot your password?</a></div>
			
			<div>
				<label id="lblPassword" runat="server" for='<%=txtPassword.ClientID %>'>Password</label>
				<input name="password" type="password" maxlength="30" id="txtPassword" runat="server" tabindex="2" /> 
			</div>
			
			<div class="login">
				<asp:checkbox runat="server" id="chkRememberLogin" tooltip="Remember Password" text="Remember me" />
			</div>
				
			<asp:linkbutton id="btnLogin" runat="server" cssclass="btnsmall orange" causesvalidation="false" onclick="btnLogin_Click" tabindex="3" text="Sign In"></asp:linkbutton>                 
				
			<div class="fbSignin">
				<p id="pFBSignupMessage" runat="server"><span style="font-style:italic;">Save time</span> by using your Facebook account to login to Kaneva.</p>
		    	<a href="#" class="fbsignin" onclick="FBLogin(FBAction.LOGIN);"><img id="Img2" src="~/images/facebook/signinfacebooksmall.gif" width="174" height="23" runat="server" /></a>
			</div>
		
		
		</asp:Panel>
		<img src="~/images/landing_page/graphic_10x363_registration_verticalShadowBreak.png" runat="server" width="10" height="363" alt="" />

	    <div class="signupForm">                                           
		    <h1 class="title">Not A Member?</h1>
		    <p><a class="kansignup" runat="server" id="aRegister"><img src="~/images/facebook/kaneva_signup.png" runat="server" /></a></p>
		    <p><a href="#" class="fbsignup" onclick="FBLogin(FBAction.REGISTER);"><img id="Img1" src="~/images/facebook/FB_signup.png" runat="server" /></a></p>            
	    </div>
    </div>
</asp:Content>

<asp:Content id="cntFooter" runat="server" contentplaceholderid="cphFooter" >
    <!--[if lte IE 8]><img class="footerRuleImage" src="../../images/landing_page/hr_917x2_footerBreak.jpg" alt="" width="923px" height="2px"/><![endif]-->
    <hr class="footerRuleTop" />
    <hr class="footerRuleBottom" />
</asp:Content>