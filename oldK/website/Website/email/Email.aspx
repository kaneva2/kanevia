<%@ Page language="c#" Codebehind="Email.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.email.Email" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<link href="http://www.kaneva.com/css/kaneva.css" rel="stylesheet" type="text/css" />
<link href="http://www.kaneva.com/css/kanevasc.css" rel="stylesheet" type="text/css" />
</head>
<body>

<div style="width:900px;padding-top:20px;">


<!-- Header -->
<div id="header">
	<table id="TopNav" width="100%" cellspacing="0" cellpadding="0" border="0">
		<tr>
			<td width="5" rowspan="2"><img border="0" src="http://www.kaneva.com/images/head/leftCornerTopHome.gif"></td>
			<td class="topBoxHome"></td>
			<td rowspan="2" align="right"><img border="0" src="http://www.kaneva.com/images/head/rightCornerTopHome.gif"></td>
		</tr>
		<tr>
			<td class="bgcolorConetentHome"><img src="http://www.kaneva.com/images/spacer.gif" height="4" width="1" border="0"></td>
		</tr>
		<tr>
			<td class="leftBoxHome"></td>
			<td class="bgcolorConetentHome"></td>
			<td class="rightBoxHome"></td>
		</tr>
		<tr class="bgcolorConetentHome">
			<td class="leftBoxHome"></td>
			<td valign="middle">
				<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" id="Table1">
					<tr>
						<td align="left" valign="middle" class="bgKanevaLogo"><a href="http://www.kaneva.com/free-virtual-world.kaneva?communcations1=homepage"><img src="http://www.kaneva.com/images/head/logok.gif" border="0"></a></td>
					</tr>
				</table>
			</td>
			<td class="rightBoxHome"></td>
		</tr>
		<tr>
			<td class="leftBoxHome"></td>
			<td class="bgcolorConetentHome"></td>
			<td class="rightBoxHome"></td>
		</tr>
		<tr>
			<td width="5" rowspan="2" align="left"><img border="0" src="http://www.kaneva.com/images/head/leftCornerBottomHome.gif"></td>
			<td class="bgcolorConetentHome" height="4"></td>
			<td rowspan="2" align="right"><img border="0" src="http://www.kaneva.com/images/head/rightCornerBottomHome.gif"></td>
		</tr>
		<tr>
			<td class="bottomBoxHome"></td>
		</tr>
	</table>
</div>


<!-- Message Body -->
<div id="body" style="padding-top:10px;width=905px;">
	<table width="100%" cellspacing="0" cellpadding="0" border="0">
		<tr>
			<td width="5" rowspan="2"><img border="0" src="http://www.kaneva.com/images/head/leftCornerTopHome.gif"></td>
			<td class="topBoxHome"></td>
			<td rowspan="2" align="right"><img border="0" src="http://www.kaneva.com/images/head/rightCornerTopHome.gif"></td>
		</tr>
		<tr>
			<td class="leftBoxHome"></td>
			<td></td>
			<td class="rightBoxHome"></td>
		</tr>
		<tr>
			<td class="leftBoxHome"></td>
			<td valign="middle" class="insideBoxText11">
				<div style="padding:10px;" id="divBody" runat="server">
				
				</div>
			</td>
			<td class="rightBoxHome"></td>
		</tr>
		<tr>
			<td width="5" rowspan="2" align="left"><img border="0" src="http://www.kaneva.com/images/head/leftCornerBottomHome.gif"></td>
			<td height="4"></td>
			<td rowspan="2" align="right"><img border="0" src="http://www.kaneva.com/images/head/rightCornerBottomHome.gif"></td>
		</tr>
		<tr>
			<td class="bottomBoxHome"></td>
		</tr>
	</table>
</div>


<!-- Footer -->
<div id="footer" style="padding-top:10px;width=905px;">
	<table width="100%" cellspacing="0" cellpadding="0" border="0">
		<tr>
			<td width="5" rowspan="2"><img border="0" src="http://www.kaneva.com/images/head/leftCornerTopHome.gif"></td>
			<td class="topBoxHome"></td>
			<td rowspan="2" align="right"><img border="0" src="http://www.kaneva.com/images/head/rightCornerTopHome.gif"></td>
		</tr>
		<tr>
			<td class="leftBoxHome"></td>
			<td></td>
			<td class="rightBoxHome"></td>
		</tr>
		<tr>
			<td class="leftBoxHome"></td>
			<td valign="middle" class="insideBoxText11" style="font-weight:bold;">
				<div style="padding:5px 10px 10px 10px;">About Kaneva</div>
				<div style="padding:0 10px;">
					Kaneva is the grassroots entertainment network where you can showcase and share your passions,
					interests, and talents with the world.  View and upload videos, photos, comments, and more.
				</div>
				<div style="padding:10px 10px 5px 10px;">
					Everyone at Kaneva cares about your privacy.  If you don't want to receive emails like this
					sent to this email address in the future, click here to change your Account Settings.
				</div>		
			</td>
			<td class="rightBoxHome"></td>
		</tr>
		<tr>
			<td width="5" rowspan="2" align="left"><img border="0" src="http://www.kaneva.com/images/head/leftCornerBottomHome.gif"></td>
			<td height="4"></td>
			<td rowspan="2" align="right"><img border="0" src="http://www.kaneva.com/images/head/rightCornerBottomHome.gif"></td>
		</tr>
		<tr>
			<td class="bottomBoxHome"></td>
		</tr>
	</table>
</div>

<div id="copyright" class="insideBoxText10" style="text-align:left;padding:26px 0 0 18px;">&#64; 2006-2007 Kaneva. All rights reserved worldwide.</div>

</div>
