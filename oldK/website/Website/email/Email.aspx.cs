///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Text;

namespace KlausEnt.KEP.Kaneva.email
{
	/// <summary>
	/// Summary description for Email.
	/// </summary>
	public class Email : NoBorderPage
	{
		private void Page_Load(object sender, System.EventArgs e)
		{
			string msgId = string.Empty;
			string msgBody = string.Empty;

			if (Request.QueryString["msgId"] != null && Request.QueryString["msgId"] != string.Empty)
			{
				try { msgId = Request.QueryString["msgId"].ToString(); } 
				catch {}
						
				if (msgId != string.Empty)
				{
					string path = Server.MapPath("~/email/" + msgId + ".txt");

					if (File.Exists(path))
					{
						// Open the stream and read it back.
						using (FileStream fs = File.OpenRead(path)) 
						{
							StreamReader sr = new StreamReader(fs);
							msgBody = sr.ReadToEnd();

							sr.Close();
							fs.Close();
						}
					}
				}
			}

			if (msgId == string.Empty)
			{
				msgBody = "<b>Title goes here</b><br><br><br>Then some text for the message";
			}
			
			divBody.InnerHtml = msgBody;
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	
	protected HtmlContainerControl	divBody;

	}
}
