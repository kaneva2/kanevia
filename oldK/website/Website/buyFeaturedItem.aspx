<%@ Page language="c#" Codebehind="buyFeaturedItem.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.buyFeaturedItem" %>

<br>
<table width="750" cellpadding="12" cellspacing="0" border="0">
	<tr>
		<td width="100%" class="bodyText" valign="top"><span class="orangeHeader">Create a Kaneva Featured Item!</span><br>
		<br><br>		
		Do you want more visibility for your Kaneva store items? By choosing to feature your items for 500 k-points, we will create for you a callout advertisement (see right) that will appear within
		the 'Home' plus 'Watch', 'Play' or 'Create' site sections based upon the object type.<br><br>
		Features give more visibility into your store offerings, resulting in potentially greater item sales. All ads run for one (1) full month after purchase date and
		cannot be terminated before the full month expires. Click 'next' to purchase...
		<br><br>
		<b style="color:red">* Requirements:</b>
		<span class="dateStamp" style="line-height: 16px;">
		<ul><b>
<b style="color:red">*</b> Must be appropriate for all ages<BR>
<b style="color:red">*</b> No Promotional or advertising materials<BR>
<b style="color:red">*</b> Item and its description must be in good taste<BR>
<b style="color:red">*</b> Your item must be published in a public channel<BR>
<b style="color:red">*</b> Must be immediately viewable or usable by your audience
		</b>
		</ul>
	
		</span>


		<br>
		<img runat="server" src="~/images/spacer.gif" width="180" height="5" border="0"/>
		<asp:button id="btnCancel" runat="Server" CausesValidation="False" onClick="btnCancel_Click" class="Filter2" Text="  cancel  "/>&nbsp;<asp:button id="btnUpdate" runat="Server" onClick="btnUpdate_Click" class="Filter2" Text="  next  "/>
		</td>	
		<td class="dateStamp" align="center" width="130" valign="top"><img runat="server" src="~/images/egFeat.gif" ID="Img1"/><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sample feature</td>
	</tr>
	<tr>
		<td class="bodyText" align="center"></td>	
	</tr>
</table><br><br>