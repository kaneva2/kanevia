///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.IO;
using System.Text;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for rssFeed.
	/// </summary>
	public class rssFeed : BasePage
	{
		private void Page_Load(object sender, System.EventArgs e)
		{
			if (IsPostBack)
			{
				return;
			}

            BlogFacade blogFacade = new BlogFacade();
            PagedList<Blog> plBlogs;

			int rssFeedId = Convert.ToInt32 (Request ["rssFeedId"]);
			string title = "";
			string mainLink = "http://" + KanevaGlobals.SiteName;

			// Create the new datatable
			DataTable dtRSSItems = new DataTable ("item");
			dtRSSItems.Columns.Add (new DataColumn ("title", System.Type.GetType("System.String") ));
			dtRSSItems.Columns.Add (new DataColumn ("description", System.Type.GetType("System.String") ));
			dtRSSItems.Columns.Add (new DataColumn ("link", System.Type.GetType("System.String") ));
			dtRSSItems.Columns.Add (new DataColumn ("author", System.Type.GetType("System.String") ));
			dtRSSItems.Columns.Add (new DataColumn ("pubDate", System.Type.GetType("System.DateTime") ));
			dtRSSItems.Columns.Add (new DataColumn ("category", System.Type.GetType("System.String") ));

			DataRow drNewItem;

			switch (rssFeedId)
			{
				case (int) Constants.eRSS_FEEDS.KANEVA_NEWS:
				{
					title = "Kaneva";
                    plBlogs = blogFacade.GetBlogs(0, "", DEFAULT_BLOG_SORT, 1, Int32.MaxValue);
					break;
				}
				case (int) Constants.eRSS_FEEDS.COMMUNITY_NEWS:
				{
					int channelId = 0;
					if (Request ["channelId"] != null)
					{
						channelId = Convert.ToInt32 (Request ["channelId"]);

						// Get the channel
						DataRow drChannel = CommunityUtility.GetCommunity (channelId);

						// Make sure the channel is not private
						// NOTE : For now don't allow private channels RSS until we get RSS authentication
						if (!CommunityUtility.IsKanevaMainCommunity (channelId) && !CommunityUtility.IsCommunityPublic (drChannel))
						{
							Response.Write ("RSS to members-only community blogs is not allowed");
							return;
						}

						title = drChannel ["name"].ToString ();
                        plBlogs = blogFacade.GetBlogs(channelId, "", DEFAULT_BLOG_SORT, 1, Int32.MaxValue);

						mainLink = "http://" + KanevaGlobals.SiteName + "/community/" + drChannel ["name_no_spaces"].ToString () + ".blogs";
					}
					else
					{
						// Main community news
						title = "Kaneva Community";
                        plBlogs = blogFacade.GetBlogs(0, "", DEFAULT_BLOG_SORT, 1, Int32.MaxValue);
					}
					
					break;
				}
				case (int) Constants.eRSS_FEEDS.DEV_NEWS:
				{
					title = "Kaneva Developer";
                    plBlogs = blogFacade.GetBlogs(0, "", DEFAULT_BLOG_SORT, 1, Int32.MaxValue);
					break;
				}
				case (int) Constants.eRSS_FEEDS.GAME_NEWS:
				{
					title = "Kaneva Game";
                    plBlogs = blogFacade.GetBlogs(0, "", DEFAULT_BLOG_SORT, 1, Int32.MaxValue);
					break;
				}
				case (int) Constants.eRSS_FEEDS.VIDEO_NEWS:
				{
					title = "Kaneva Video";
                    plBlogs = blogFacade.GetBlogs(0, "", DEFAULT_BLOG_SORT, 1, Int32.MaxValue);
					break;
				}
				case (int) Constants.eRSS_FEEDS.FORUM_POSTS:
				{
					int channelId = 0;
					if (Request ["channelId"] != null)
					{
						channelId = Convert.ToInt32 (Request ["channelId"]);
					}

					// Get the channel
					DataRow drChannel = CommunityUtility.GetCommunity (channelId);

					// Make sure the channel is not private
					// NOTE : For now don't allow private channels RSS until we get RSS authentication
					if (!CommunityUtility.IsKanevaMainCommunity (channelId) && !CommunityUtility.IsCommunityPublic (drChannel))
					{
						Response.Write ("RSS to members-only community forums is not allowed.");
						return;
					}

                    //DataTable dtForumThreads = ForumUtility.GetRssForumThreads (channelId);
					
                    //for (int j = 0; j < dtForumThreads.Rows.Count; j++)
                    //{
                    //    drNewItem = dtRSSItems.NewRow ();
                    //    drNewItem ["title"] = Server.HtmlDecode (dtForumThreads.Rows [j]["subject"].ToString ());
                    //    drNewItem ["description"] = TruncateWithEllipsis (dtForumThreads.Rows [j]["body_text"].ToString (), 250);
                    //    drNewItem ["link"] = "http://" + KanevaGlobals.SiteName + "/forum/forumThreads.aspx?topicId=" + dtForumThreads.Rows [j]["topic_id"].ToString ();
                    //    drNewItem ["author"] = dtForumThreads.Rows [j]["username"].ToString ();
                    //    drNewItem ["pubDate"] = dtForumThreads.Rows [j]["pubDate"];
                    //    drNewItem ["category"] = "Forum Post";
                    //    dtRSSItems.Rows.Add (drNewItem);
                    //}

					string feedName = drChannel ["name"].ToString ();
					if (channelId.Equals ((int) Constants.eCOMMUNITIES.CREATE))
					{
						feedName = "Kaneva Developer";
						mainLink = "http://elite.kaneva.com";
					}
					else
					{
						mainLink = "http://" + KanevaGlobals.SiteName + "/community/" + drChannel ["name_no_spaces"].ToString () + ".forums";
					}

					SendXML (dtRSSItems, feedName + " Forums", mainLink, feedName + " Forum Posts");
					return;
				}
				case (int) Constants.eRSS_FEEDS.STORE_ITEMS:
				{
					int channelId = 0;
					if (Request ["channelId"] != null)
					{
						channelId = Convert.ToInt32 (Request ["channelId"]);
					}

					// Get the channel
					DataRow drChannel = CommunityUtility.GetCommunity (channelId);

					// Make sure the channel is not private
					// NOTE : For now don't allow private channels RSS until we get RSS authentication
					if (!CommunityUtility.IsKanevaMainCommunity (channelId) && !CommunityUtility.IsCommunityPublic (drChannel))
					{
						Response.Write ("RSS to members-only community forums is not allowed.");
						return;
					}

					PagedDataTable pdtAssets = StoreUtility.GetAssetsInChannel (channelId, true, false, 0, "", "a.created_date", 1, Int32.MaxValue);

					for (int j = 0; j < pdtAssets.Rows.Count; j++)
					{
						drNewItem = dtRSSItems.NewRow ();
						drNewItem ["title"] = Server.HtmlDecode (pdtAssets.Rows [j]["name"].ToString ());
						drNewItem ["description"] = TruncateWithEllipsis (Server.HtmlDecode (pdtAssets.Rows [j]["teaser"].ToString ()), 250);
						drNewItem ["link"] = "http://" + KanevaGlobals.SiteName + "/community/" + pdtAssets.Rows [j]["asset_id"].ToString () + ".storeItem";
						drNewItem ["author"] = pdtAssets.Rows [j]["username"].ToString ();
						drNewItem ["pubDate"] = pdtAssets.Rows [j]["created_date"];
						drNewItem ["category"] = "Store Item";
						dtRSSItems.Rows.Add (drNewItem);
					}

					mainLink = "http://" + KanevaGlobals.SiteName + "/community/" + drChannel ["name_no_spaces"].ToString () + ".nowplaying";

					SendXML (dtRSSItems, drChannel ["name"].ToString () + " Store Items", mainLink, drChannel ["name"].ToString () + " Store Items");
					return;
				}
				case (int) Constants.eRSS_FEEDS.WHOLE_CHANNEL:
				{
					int channelId = 0;
					if (Request ["channelId"] != null)
					{
						channelId = Convert.ToInt32 (Request ["channelId"]);
					}

					// Get the channel
					DataRow drChannel = CommunityUtility.GetCommunity (channelId);

					// Make sure the channel is not private
					// NOTE : For now don't allow private channels RSS until we get RSS authentication
					if (!CommunityUtility.IsKanevaMainCommunity (channelId) && !CommunityUtility.IsCommunityPublic (drChannel))
					{
						Response.Write ("RSS to members-only community forums is not allowed.");
						return;
					}

					// Blogs

                    PagedList<Blog> plChannelBlogs = blogFacade.GetBlogs(channelId, "", DEFAULT_BLOG_SORT, 1, Int32.MaxValue);
                    foreach (Blog blog in plChannelBlogs)
					{
						drNewItem = dtRSSItems.NewRow ();
                        drNewItem["title"] = Server.HtmlDecode(blog.Subject) + " (Blog)";
						//drNewItem ["description"] = dtChannelBlogs.Rows [i]["teaser"].ToString ();
                        drNewItem["link"] = "http://" + KanevaGlobals.SiteName + "/blog/blogComments.aspx?topicId=" + blog.BlogId.ToString() + "&communityId=" + blog.CommunityId.ToString();
                        drNewItem["author"] = blog.Username;
                        drNewItem["pubDate"] = blog.CreatedDate;
						drNewItem ["category"] = "Blog Post";
						dtRSSItems.Rows.Add (drNewItem);
					}

					// Store Items
					PagedDataTable pdtAssets = StoreUtility.GetAssetsInChannel (channelId, true, false, 0, "", "a.created_date", 1, Int32.MaxValue);

					for (int j = 0; j < pdtAssets.Rows.Count; j++)
					{
						drNewItem = dtRSSItems.NewRow ();
						drNewItem ["title"] = Server.HtmlDecode (pdtAssets.Rows [j]["name"].ToString ()) + " (Store Item)";
						drNewItem ["description"] = TruncateWithEllipsis (Server.HtmlDecode (pdtAssets.Rows [j]["teaser"].ToString ()), 250);
						drNewItem ["link"] = "http://" + KanevaGlobals.SiteName + "/community/" + pdtAssets.Rows [j]["asset_id"].ToString () + ".storeItem";
						drNewItem ["author"] = pdtAssets.Rows [j]["username"].ToString ();
						drNewItem ["pubDate"] = pdtAssets.Rows [j]["created_date"];
						drNewItem ["category"] = "Store Item";
						dtRSSItems.Rows.Add (drNewItem);
					}

                    //// Forums
                    //DataTable dtForumThreads = ForumUtility.GetRssForumThreads (channelId);
					
                    //for (int j = 0; j < dtForumThreads.Rows.Count; j++)
                    //{
                    //    drNewItem = dtRSSItems.NewRow ();
                    //    drNewItem ["title"] = Server.HtmlDecode (dtForumThreads.Rows [j]["subject"].ToString ()) + " (Forum Post)";
                    //    drNewItem ["description"] = TruncateWithEllipsis (dtForumThreads.Rows [j]["body_text"].ToString (), 250);
                    //    drNewItem ["link"] = "http://" + KanevaGlobals.SiteName + "/forum/forumThreads.aspx?topicId=" + dtForumThreads.Rows [j]["topic_id"].ToString ();
                    //    drNewItem ["author"] = dtForumThreads.Rows [j]["username"].ToString ();
                    //    drNewItem ["pubDate"] = dtForumThreads.Rows [j]["pubDate"];
                    //    drNewItem ["category"] = "Forum Post";
                    //    dtRSSItems.Rows.Add (drNewItem);
                    //}

					//mainLink = "http://" + KanevaGlobals.SiteName + "/channel/" + drChannel ["name_no_spaces"].ToString () + ".channel";
					mainLink = KanevaGlobals.GetBroadcastChannelUrl (drChannel ["name_no_spaces"].ToString ());

					SendXML (dtRSSItems, drChannel ["name"].ToString (), mainLink, TruncateWithEllipsis (drChannel ["description"].ToString (), 100));
					return;
				}
				default:
				{
                    plBlogs = blogFacade.GetBlogs(0, "",  DEFAULT_BLOG_SORT, 1, Int32.MaxValue);
					break;
				}
			}

            foreach (Blog blog in plBlogs)
            {
                drNewItem = dtRSSItems.NewRow();
                //				drNewItem ["title"] = "![CDATA[" + dtBlogs.Rows [i]["subject"].ToString () + "]]";
                //				drNewItem ["description"] = "![CDATA[" + dtBlogs.Rows [i]["teaser"].ToString () + "]]";
                drNewItem["title"] = Server.HtmlDecode(blog.Subject);
                //	drNewItem ["description"] = dtBlogs.Rows [i]["teaser"].ToString ();
                drNewItem["link"] = "http://" + KanevaGlobals.SiteName + "/blog/blogComments.aspx?topicId=" + blog.BlogId + "&communityId=" + blog.CommunityId.ToString();
                drNewItem["author"] = blog.Username;
                drNewItem["pubDate"] = blog.CreatedDate;
                drNewItem["category"] = "Blog Post";
                dtRSSItems.Rows.Add(drNewItem);
            }

			
			SendXML (dtRSSItems, title + " Blog", mainLink, title + " Blog");
		}

//		/// <summary>
//		/// Send out the XML
//		/// </summary>
//		/// <param name="dtRSSItems"></param>
//		private void SendXML (DataTable dtRSSItems, string feedName)
//		{
//			string strStartRSS = "<?xml version=\"1.0\"?><rss version=\"2.0\"><channel><title>" + feedName + "</title><link>http://" + KanevaGlobals.SiteName + "</link>";
//			string strEndRSS = "</channel></rss>";
//
//			DataSet dsRSS = new DataSet ("removethis");
//			dsRSS.Tables.Add (dtRSSItems);
//
//			Response.Clear ();
//			Response.ContentType = "text/xml";
//			Response.Write (strStartRSS);
//			dsRSS.WriteXml (Response.OutputStream, System.Data.XmlWriteMode.IgnoreSchema);
//			Response.Write (strEndRSS);
//			Response.End ();
//		}

		/// <summary>
		/// SendXML
		/// </summary>
		/// <param name="dtRSSItems"></param>
		/// <param name="feedName"></param>
		private void SendXML (DataTable dtRSSItems, string feedName, string link, string feedDescription)
		{
			// Set the content-type
			Response.ContentType = "text/xml";
			Response.ContentEncoding = Encoding.UTF8;

			// Use an XmlTextWriter to write the XML data to a string...
			StringWriter sw = new StringWriter();
			XmlTextWriter writer = new XmlTextWriter (sw);

			// write out 
			writer.WriteStartElement("rss");
			writer.WriteAttributeString("version", "2.0");

			// write out 
			writer.WriteStartElement("channel");

			// write out -level elements
			writer.WriteElementString("title", feedName);
			writer.WriteElementString("link", link);
			writer.WriteElementString("description", feedDescription);
			//writer.WriteElementString("ttl", "5");

			// write out all elements
			for (int j = 0; j < dtRSSItems.Rows.Count; j++)
			{
				writer.WriteStartElement("item");

				writer.WriteElementString("title", dtRSSItems.Rows [j]["title"].ToString());
				writer.WriteElementString("link", dtRSSItems.Rows [j]["link"].ToString());
				writer.WriteElementString("author", dtRSSItems.Rows [j]["author"].ToString());
				writer.WriteElementString("description", dtRSSItems.Rows [j]["description"].ToString()); 
				writer.WriteElementString("pubDate", ((DateTime) dtRSSItems.Rows[j]["pubDate"]).ToString("r"));
				writer.WriteElementString("category", dtRSSItems.Rows [j]["category"].ToString()); 
				writer.WriteEndElement();
			}

			// write out 
			writer.WriteEndElement();

			// write out 
			writer.WriteEndElement();

			writer.Close();

			// write out the cached value
			Response.Write (sw.ToString());
		}

		private const string DEFAULT_BLOG_SORT = "b.created_date desc"; 

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
