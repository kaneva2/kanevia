///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using log4net;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
    public partial class declineInvite : MainTemplatePage
    {
        protected void Page_Load (object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request["i"] == null)
                {
                    // added i=null to url as a way to easily debug what code path is causing user to be redirected
                    // so when use is on default page and has i=null in querystring then they were redirected from this page
                    Response.Redirect ("~/default.aspx?i=null");
                }
                else
                {
                    DataRow drInvite = UsersUtility.GetInvite (Convert.ToInt32 (Request["i"]), Request["k"].ToString ());

                    if (drInvite == null || Convert.ToInt32 (drInvite["invite_status_id"]) != (int) Constants.eINVITE_STATUS.UNREGISTER)
                    {
                        // log reason user was redirected
                        m_logger.Info ("User redirected from declineInvite page because invite not valid.");

                        Response.Redirect ("~/default.aspx");
                    }

                    try
                    {
                        // set join button url with invite information so inviter will get credit
                        aRegister.HRef = ResolveUrl(KanevaGlobals.JoinLocation) + "?i=" + drInvite["invite_id"].ToString () + "&k=" + drInvite["key_value"].ToString ();
                    }
                    catch
                    {   // if error, set to register page w/o invite info
						aRegister.HRef = ResolveUrl(KanevaGlobals.JoinLocation);
                    }

                    // load the decline reasons into drop list
                    BindDeclineReasons ();
                }
            }
        }


        /// <summary>
        /// BindDeclineReasons
        /// </summary>
        private void BindDeclineReasons ()
        {
            DataTable dtReasons = WebCache.GetInviteDeclineReasons ();

            if (dtReasons != null)
            {
                ddlDeclineReason.DataTextField = "name";
                ddlDeclineReason.DataValueField = "invite_decline_reason_id";

                ddlDeclineReason.DataSource = dtReasons;
                ddlDeclineReason.DataBind ();
            }
            else
            {
                ddlDeclineReason.Items.Add (new ListItem ("No reason given", "1"));
                ddlDeclineReason.SelectedIndex = ddlDeclineReason.Items.Count - 1;
            }
        }


        #region Event Handlers

        /// <summary>
        /// btnSubmit_Click
        /// </summary>
        protected void btnSubmit_Click (object sender, EventArgs e)
        {
            try
            {
                int inviteId = Convert.ToInt32 (Request["i"]);

                if (inviteId > 0)
                {
                    string declineText = "";

                    if (KanevaWebGlobals.ContainsInjectScripts (txtDeclineInfo.Text))
                    {
                        m_logger.Warn ("User associated with Friend Invite (invite_id=" + inviteId.ToString () + ") tried to script from IP " + Common.GetVisitorIPAddress());
                    }
                    else
                    {
                        declineText = txtDeclineInfo.Text.Trim ();
                    }
				
                    // Insert the decline reason
                    UsersUtility.InsertInviteDecline (inviteId, Convert.ToInt32 (ddlDeclineReason.SelectedValue), declineText);
                }

                tblDecline.Visible = false;
                divThankYou.Style.Add ("display", "block");
            }
            catch (Exception ex)
            {
                m_logger.Error ("Error saving invite decline reason. ", ex);
            }
        }

        /// <summary>
        /// lbNoThanks_Click
        /// </summary>
        protected void lbNoThanks_Click (object sender, EventArgs e)
        {
            try
            {
                int inviteId = Convert.ToInt32 (Request["i"]);

                // Update invite to show declined
                if (UsersUtility.UpdateInvite (inviteId, (int) Constants.eINVITE_STATUS.DECLINED) > 0)
                {
                    // hide no thanks button
                    lbNoThanks.Style.Add ("display", "none");

                    // show may we ask why section
                    divNoThanks.Style.Add ("display", "block");
                }
            }
            catch (Exception ex)
            {
                m_logger.Error ("Error updating invite status to declined. ", ex);
            }
        }

        #endregion Event Handlers


        #region Declerations

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

        #endregion Declerations
    }

}
