///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for buyFeaturedItem.
	/// </summary>
	public class buyFeaturedItem : MainTemplatePage
	{
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Is the asset already featured?
			int assetId = Convert.ToInt32 (Request["assetId"]);

			if (StoreUtility.IsAssetFeatured (assetId))
			{
				ShowErrorOnStartup ("This asset is already featured, please select a different asset to feature.");
				btnUpdate.Enabled = false;
				return;
			}
		}


		/// <summary>
		/// Cancel Event Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnCancel_Click (object sender, EventArgs e) 
		{
			NavigateToReturn ();
		}

		/// <summary>
		/// Update Event Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnUpdate_Click (object sender, EventArgs e) 
		{
			string queryString = "";
			if (Request ["communityId"] != null)
			{
				queryString = "&communityId=" + Request ["communityId"].ToString ();
			}

			Response.Redirect (ResolveUrl("~/checkout/kPointSelection.aspx?feat=Y&assetId=" + Request["assetId"] + queryString));
		}


		/// <summary>
		/// NavigateToReturn
		/// </summary>
		private void NavigateToReturn ()
		{
			
		}

		protected Button btnUpdate;

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
