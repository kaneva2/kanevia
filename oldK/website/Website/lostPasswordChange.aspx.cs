///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using System.Security.Cryptography;
using System.Web.Security;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for lostPasswordChange.
	/// </summary>
	public class lostPasswordChange : BasePage
	{
		protected lostPasswordChange () 
		{
			Title = "Change Password";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
            // User is already logged in. If they want to change their password they need to do it via Account Settings->Account Information
            if (Request.IsAuthenticated)
            {
                RedirectToHomePage();
            }

            // If no key code pass via querystring, link is invalid so redirect to Forgot Password page
            if (Request ["j"] == null || Request ["j"].ToString() == "")
            {
                Response.Redirect("~/lostpassword.aspx");    
            }

            revPassword.ValidationExpression = Constants.VALIDATION_REGEX_PASSWORD;
            revEmail.ValidationExpression = Constants.VALIDATION_REGEX_EMAIL;
		}

		/// <summary>
		/// btnPassword_Click
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnChangePassword_Click (object sender, EventArgs e) 
		{
            // Verify key code exists. We will validate with email later
            if (Request["j"] == null || Request["j"].ToString() == "")
            {
                Response.Redirect("~/lostpassword.aspx");
            }

            // Validate page. If not valid, return errors
            Page.Validate();
            if (!Page.IsValid)
            {
                return;
            }

            string strEmail = Server.HtmlEncode(txtEmail.Text.Trim());
			string strPassword = Server.HtmlEncode (txtPassword.Text.Trim ());
			string strUserName = "";
			string keyCode = Request ["j"].ToString ();
            
			// Make sure this user can be looked up by username and keycode.
			DataRow drUser = UsersUtility.GetUser (strEmail, keyCode);

			// If key does not match email address, display message and have user resend email from Forgot Password page
            if (drUser == null)
			{
                GetUserFacade.InsertUserLoginIssue(0, Common.GetVisitorIPAddress(), "Password change attempt failure. The supplied email or web link was not valid.", "CHANGE_PASSWORD", "WEB");
				cvBlank.ErrorMessage = "This password reset link is no longer valid. <a href='lostpassword.aspx'>Reset Your Password</a> to resend your Password Reset email.";
                cvBlank.IsValid = false; 
                return;
			}

			strUserName = drUser ["username"].ToString ();

			// Hash the new password
			byte[] salt = new byte[9];
			new RNGCryptoServiceProvider().GetBytes (salt);
			string hashPassword = FormsAuthentication.HashPasswordForStoringInConfigFile (UsersUtility.MakeHash(strPassword+strUserName.ToLower()) + Convert.ToBase64String (salt), "MD5");

            GetUserFacade.UpdatePassword(Convert.ToInt32(drUser["user_id"].ToString()), hashPassword, Convert.ToBase64String(salt));
			
			string scriptString = "<script language=JavaScript>";
			scriptString += "alert ('Your password has been successfully changed. You can now sign in with your new password.');";
			scriptString += "window.location = 'loginSecure.aspx?logretURL=default.aspx'";
			scriptString += "</script>";

			if (!ClientScript.IsClientScriptBlockRegistered (GetType (),"ShowSuccess"))
			{
                ClientScript.RegisterStartupScript(GetType(), "ShowSuccess", scriptString);
			}
		}

        protected TextBox txtEmail;
		protected TextBox txtPassword;
		protected TextBox txtConfirmPassword;
        protected RequiredFieldValidator rfUsername, rfPassword, rfConfirmPassword;
        protected RegularExpressionValidator revEmail, revPassword;
        protected CompareValidator cmpPassword;
        protected CustomValidator cvBlank;
        protected ValidationSummary valSum;

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
