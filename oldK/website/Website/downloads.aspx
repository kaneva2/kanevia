<%@ Page language="c#" Codebehind="downloads.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.downloads" %>
<%@ Register TagPrefix="Kaneva" TagName="HeaderText" Src="usercontrols/HeaderText.ascx" %>
<Kaneva:HeaderText runat="server" Text="Download Essential Kaneva Utilities"/>

<br>
<center>
	<table cellpadding="5" cellspacing="0" border="0" width="900" ID="Table2" class="FullBorders">
	<tr class="lineItemColHead">
	<td></td>
	<td valign="middle" width="90%">kaneva utilities</td>
	<td valign="middle" width="10%">actions</td>
	</tr>	
	<!--<tr>
	<td valign="middle" class="lineItemEven" width="40%"><a href="#" style="COLOR: #3258ba; font-size: 12px;">KANEVA Editor</a><font color="green"> - .zip 12mb</font>&nbsp;&nbsp;<br>
	Design, create, and publish your own games with our revolutionary 3D game development platform. Supports MMO, FPS, RPG, and RTS game types.<br><br>
	</td>
	<td valign="middle" class="lineItemEven" width="10%" align="center"><input type="image" src="images/button_get.gif" onclick="javascript: window.location = ('community_store_buy.htm');"></td>
	</tr>	-->
	<tr>
		<td class="lineItemOdd"><img runat="server" src="~/images/prodShot_KML.jpg" ID="Img1"/></td>
		<td valign="middle" class="lineItemOdd" width="40%"><a runat="server" id="aKMLSetup" href="~/download/KanevaMediaLauncherSetup.exe" class="assetLink">Kaneva Media Launcher</a><font color="green"> - .exe</font><br><span class="adminLinks"><asp:Label id="lblVersion" runat="server"/></span><br>
		Kaneva Media Launcher is a media manager that facilitates downloading of content on your desktop. <a runat="server" href="~/overview/Iris.aspx">more...</a><br><br>
		</td>
		<td valign="middle" class="lineItemOdd" width="10%" align="center"><asp:hyperlink runat="server" id="hlKMLSetup" ImageURL="~/images/button_WEform.gif"/></td>
	</tr>	
	<tr>
		<td><img runat="server" src="~/images/prodShot_KMP.jpg" ID="Img2"/></td>
		<td valign="middle" class="lineItemEven" width="40%"><a runat="server" id="aKMPWinSetup" class="assetLink">Kaneva Media Publisher</a><font color="green"> - .exe</font><br><span class="adminLinks"><asp:Label id="lblKMPVersion" runat="server"/></span><br>
		Kaneva Media Publisher is a light-weight application that facilitates 
publishing of content from your Windows PC. <a runat="server" href="~/asset/kanevaMediaPublisher.aspx" ID="A1">more...</a><br><br>
		</td>
		<td valign="middle" class="lineItemEven" width="10%" align="center"><asp:hyperlink runat="server" id="hlKMPWinSetup" ImageURL="~/images/button_WEform.gif"/></td>
	</tr>	
	<tr>
		<td class="lineItemOdd"><img runat="server" src="~/images/prodShot_KMP.jpg" ID="Img3"/></td>
		<td valign="middle" class="lineItemOdd" width="40%"><a runat="server" id="aKMPMacSetup" class="assetLink">Kaneva Media Publisher</a><font color="green"> - .pkg</font><br><span class="adminLinks"><asp:Label id="lblKMPMacVersion" runat="server"/></span><br>
		Kaneva Media Publisher for Macintosh (MAC) is a light-weight application that facilitates publishing of content from your MAC. <a runat="server" href="~/asset/kanevaMediaPublisher.aspx" ID="A2">more...</a><br><br>
		</td>
		<td valign="middle" class="lineItemOdd" width="10%" align="center"><asp:hyperlink runat="server" id="hlKMPMacSetup" ImageURL="~/images/button_WEform.gif"/></td>
	</tr>	
	<tr>
		<td><img runat="server" src="~/images/prodShot_KGP.jpg" ID="Img4"/></td>
		<td valign="middle" class="lineItemEven" width="40%"><asp:linkButton runat="server" OnClick="btnDownload2_Click" id="hlKGPSetup" class="assetLink">Kaneva Game Platform</asp:linkButton ><font color="green"> - .exe</font><br><span class="adminLinks"><asp:Label id="lblKGPVersion" runat="server"/></span><br>
		Kaneva Game Platform allows for the creation of MMO, RPG, and FPS games. <a runat="server" href="~/overview/KEPEditor.aspx?ttCat=create&subtab=myg" ID="A4">more...</a><br><br>
		</td>
		<td valign="middle" class="lineItemEven" width="10%" align="center"><asp:imagebutton runat="server" ID="btnDownload" ImageUrl="~/images/button_WEform.gif" class="Filter2" alt="Download Now" OnClick="btnDownload_Click"/></td>
	</tr>	
<!--	<tr>
	<td valign="middle" class="lineItemEven" width="40%"><a href="#" style="COLOR: #3258ba; font-size: 12px;">KANEVA FAQ</a><font color="green"> - .pdf 2.3mb</font><br>
	A consolidated list of Frequently Asked Questions...updated frequently.<br><br>
	</td>
	<td valign="middle" class="lineItemEven" width="10%" align="center"><input type="image" src="images/button_get.gif" onclick="javascript: window.location = ('community_store_buy.htm');"></td>
	</tr>-->
	</table>
</center>
<br><br>