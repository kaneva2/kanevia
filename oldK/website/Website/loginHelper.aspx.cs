///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using System.Web.Security;
using System.Security.Principal;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for login.
	/// </summary>
	public class loginHelper :Page
	{
		private void Page_Load(object sender, System.EventArgs e)
		{
			string scriptString;

			if (Request ["action"] != null)
			{
				switch (Request ["action"].ToString ())
				{
					case ("reg"):
						scriptString = "<script language=JavaScript>";
						scriptString += "parent.location = ('" + ResolveUrl (KanevaGlobals.JoinLocation) + "');";
						scriptString += "</script>";

                        if (!ClientScript.IsClientScriptBlockRegistered(GetType (), "Navigate"))
						{
                            ClientScript.RegisterStartupScript(GetType(), "Navigate", scriptString);
						}
						break;

					case ("myK"):
						scriptString = "<script language=JavaScript>";
						scriptString += "parent.location = ('" + ResolveUrl ("~/mykaneva/mykaneva.kaneva") + "');";
						scriptString += "</script>";

                        if (!ClientScript.IsClientScriptBlockRegistered(GetType(), "Navigate"))
						{
                            ClientScript.RegisterStartupScript(GetType(), "Navigate", scriptString);
						}
						break;

					case ("login"):
					{

                        string url = ResolveUrl("~/free-virtual-world.kaneva");

						scriptString = "<script language=JavaScript>";
						scriptString += "parent.location.replace(\"";
						scriptString += url;
						scriptString += "\");</script>";

                        if (!ClientScript.IsClientScriptBlockRegistered(GetType(), "Navigate"))
						{
                            ClientScript.RegisterStartupScript(GetType(), "Navigate", scriptString);
						}
						break;
					}

					case ("join"):
					{

						string url = ResolveUrl (KanevaGlobals.JoinLocation);

						scriptString = "<script language=JavaScript>";
						scriptString += "parent.location.replace(\"";
						scriptString += url;
						scriptString += "\");</script>";

                        if (!ClientScript.IsClientScriptBlockRegistered(GetType(), "Join"))
						{
                            ClientScript.RegisterStartupScript(GetType(), "Join", scriptString);
						}
						break;
					}

					case ("logout"):
						scriptString = "<script language=JavaScript>";
						scriptString += "parent.location = ('" + ResolveUrl ("~/default.aspx?ttCat=home&subtab=feat") + "')\n";
						scriptString += "</script>";

                        if (!ClientScript.IsClientScriptBlockRegistered(GetType(), "Navigate"))
						{
                            ClientScript.RegisterStartupScript(GetType(), "Navigate", scriptString);
						}
						break;

					case ("cart"):
						scriptString = "<script language=JavaScript>";
						scriptString += "parent.location = ('" + ResolveUrl ("~/bookmarks/bookmarks.aspx?ttCat=mykaneva&subtab=bkm&cart=Y") + "')\n";
						scriptString += "</script>";

                        if (!ClientScript.IsClientScriptBlockRegistered(GetType(), "Navigate"))
						{
                            ClientScript.RegisterStartupScript(GetType(), "Navigate", scriptString);
						}
						break;

					case ("lost"):
						scriptString = "<script language=JavaScript>";
						scriptString += "parent.location = ('" + ResolveUrl ("~/lostPassword.aspx") + "')\n";
						scriptString += "</script>";

                        if (!ClientScript.IsClientScriptBlockRegistered(GetType(), "Navigate"))
						{
                            ClientScript.RegisterStartupScript(GetType(), "Navigate", scriptString);
						}
						break;

					case ("admin"):
						scriptString = "<script language=JavaScript>";
						scriptString += "parent.location = ('" + ResolveUrl ("~/reports/KPI.aspx") + "')\n";
						scriptString += "</script>";

                        if (!ClientScript.IsClientScriptBlockRegistered(GetType(), "Navigate"))
						{
                            ClientScript.RegisterStartupScript(GetType(), "Navigate", scriptString);
						}
						break;

					case ("buy"):
						scriptString = "<script language=JavaScript>";
						scriptString += "parent.location = ('" + ResolveUrl ("~/buyKPoints.aspx") + "')\n";
						scriptString += "</script>";

                        if (!ClientScript.IsClientScriptBlockRegistered(GetType(), "Navigate"))
						{
                            ClientScript.RegisterStartupScript(GetType(), "Navigate", scriptString);
						}
						break;
				}
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
