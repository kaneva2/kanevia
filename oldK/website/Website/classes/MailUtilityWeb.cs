///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Data;
using System.IO;
using System.Drawing;
using System.Web.Hosting;
using log4net;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for MailUtilityWeb.
	/// </summary>
	public class MailUtilityWeb
	{
		public MailUtilityWeb()
		{
		}

    #region Database Templates
    // Converting emails to the new method
    // Where templates are stored in the database
    // and sending is done through the templatesherpa.

    // DressAndSendTemplate(emailTypeId, emailTemplateId, emailTo, inviteId, fromUserId, toUserId, keyCode, emailQueId, userMessage, addToSubject)

    /// <summary>
    /// SendInvitation
    /// When no from name is specified user username
    /// </summary>
    public static void SendInvitation(int userId, string toEmail, string toName, string additionalmessage, int inviteID, string keyCode, System.Web.UI.Page page)
    {
      int typeId = Mailer.EmailTypeUtility.GetTypeIdByName("Invitation");
      Mailer.TemplateSherpa.DressAndSendTemplate(typeId, 0, toEmail, inviteID, userId, 0, keyCode, 0, additionalmessage, "", 0);
    }

    /// <summary>
    /// SendCommentEmail
    /// </summary>
    public static void SendCommentEmail(string toEmail, User userTo, int fromUserId, string commentType)
    {
		int typeId = Mailer.EmailTypeUtility.GetTypeIdByName("CommentNotification");
		
		// use addToSubject field for the comment type
	     // used later to determine the correct link in the email
		Mailer.TemplateSherpa.DressAndSendTemplate(typeId, 0, toEmail, 0, fromUserId, userTo.UserId, "", 0, "", commentType, 0);
    }

	/// <summary>
	/// SendBlastCommentNotificationEmail
	/// </summary>
	public static void SendBlastCommentNotificationEmail(string toEmail, string blastText, string blastLocation, int toUserId, int fromUserId)
	{
		// store subject and message as overload fields		
		int overloadId = 0;
		overloadId = Mailer.Queuer.AddOverloadField(overloadId, "#BlastText#", blastText);
		overloadId = Mailer.Queuer.AddOverloadField(overloadId, "#BlastLocation#", blastLocation);

		//if overloadid did not work don't send the email.
		if (overloadId != 0)
		{
			int typeId = Mailer.EmailTypeUtility.GetTypeIdByName("BlastCommentNotification");

			Mailer.TemplateSherpa.DressAndSendTemplate(typeId, 0, toEmail, 0, fromUserId, toUserId, "", 0, "", "", 0, overloadId);
		}
	} 


    /// <summary>
    /// SendPrivateMessageNotificationEmail
    /// </summary>
    public static void SendPrivateMessageNotificationEmail(string toEmail, string strSubject, int messageId, string messageText, int toUserId, int fromUserId)
    {
      // store subject and message as overload fields		
		int overloadId = 0;
		overloadId = Mailer.Queuer.AddOverloadField(overloadId, "#PMSubject#", strSubject);
		overloadId = Mailer.Queuer.AddOverloadField(overloadId, "#PMMessageText#", messageText);

		//if overloadid did not work don't send the email.
		if (overloadId != 0)
		{
			int typeId = Mailer.EmailTypeUtility.GetTypeIdByName("PrivateMessage");
			string strMessageId = messageId.ToString();
			Mailer.TemplateSherpa.DressAndSendTemplate(typeId, 0, toEmail, 0, fromUserId, toUserId, "", 0, strMessageId, "", 0, overloadId);
		}			  
    }

	/// <summary>
	/// SendBlastNotificationEmail
	/// </summary>
    private static void SendBlastToProfileNotificationEmail (string toEmail, string blastText, int toUserId, int fromUserId)
	{
		// store subject and message as overload fields		
		int overloadId = 0;
		overloadId = Mailer.Queuer.AddOverloadField(overloadId, "#BlastText#", blastText);

		//if overloadid did not work don't send the email.
		if (overloadId != 0)
		{
			int typeId = Mailer.EmailTypeUtility.GetTypeIdByName("ProfileBlastNotification");
			
			Mailer.TemplateSherpa.DressAndSendTemplate(typeId, 0, toEmail, 0, fromUserId, toUserId, "", 0, "", "", 0, overloadId);
		}
	}

    private static void SendWorldOwnerBlastNotificationEmail (string toEmail, string blastText, int toUserId, int fromUserId)
    {
        // store subject and message as overload fields		
        int overloadId = 0;
        overloadId = Mailer.Queuer.AddOverloadField (overloadId, "#BlastText#", blastText);

        //if overloadid did not work don't send the email.
        if (overloadId != 0)
        {
            int typeId = Mailer.EmailTypeUtility.GetTypeIdByName ("WorldOwnerBlastNotification");

            Mailer.TemplateSherpa.DressAndSendTemplate (typeId, 0, toEmail, 0, fromUserId, toUserId, "", 0, "", "", 0, overloadId);
        }
    }

    public static void SendBlastNotificationEmail (bool isProfile, string toEmail, string blastText, int toUserId, int fromUserId)
    {
        SendBlastNotificationEmail (isProfile, toEmail, blastText, toUserId, fromUserId, 0);
    }
    public static void SendBlastNotificationEmail (bool isProfile, string toEmail, string blastText, int toUserId, int fromUserId, int communityId)
    {
        if (isProfile)
        {
            SendBlastToProfileNotificationEmail (toEmail, blastText, toUserId, fromUserId);
        }
        else
        {                                      
            // if test mode, if from community id > 0
            // override toEmail and toUserId fields from web config
            if (KanevaGlobals.WorldBlastTestMode)
            {
                if (KanevaGlobals.WorldBlastTestMode_CommunityId == communityId)
                {
                    SendWorldOwnerBlastNotificationEmail (toEmail, blastText, toUserId, fromUserId);
                }
            }
            else
            {
                SendWorldOwnerBlastNotificationEmail (toEmail, blastText, toUserId, fromUserId);
            }
        }
    }
  
    /// <summary>
    /// SendMegaRaveNotification
    /// </summary>
    /// <returns></returns>
    public static void SendMegaRaveNotification(string toEmail, User userTo, int fromUserId)
    {
      int typeId = Mailer.EmailTypeUtility.GetTypeIdByName("MegaRave");
      Mailer.TemplateSherpa.DressAndSendTemplate(typeId, 0, toEmail, 0, fromUserId, userTo.UserId, "", 0, "", "", 0);
    }

    /// <summary>
    /// SendFriendRequestEmail
    /// </summary>
    public static int SendFriendRequestEmail(int userId, int friendId)
    {
      // Send out a friend request Email
      UserFacade userFacade = new UserFacade();
      User userFriend = userFacade.GetUser(friendId);

      if (Convert.ToInt32(userFriend.NotifyFriendRequests).Equals(1))
      {        
        User userFrom = userFacade.GetUser(userId);
        int typeId = Mailer.EmailTypeUtility.GetTypeIdByName("FriendRequest");
        Mailer.TemplateSherpa.DressAndSendTemplate(typeId, 0, userFriend.Email, 0, userFrom.UserId, userFriend.UserId, "", 0, "", "", 0);       
      }

      return 1;
    }

 
    /// <summary>
    /// SendNewFriendNotification
    /// </summary>
    public static void SendNewFriendNotification(int userIdTo, int newFriendUserId)
    {
      UserFacade userFacade = new UserFacade();
      User userNewFriend = userFacade.GetUser(newFriendUserId);
      User userTo = userFacade.GetUser(userIdTo);

      int typeId = Mailer.EmailTypeUtility.GetTypeIdByName("NewFriendNotification");
      Mailer.TemplateSherpa.DressAndSendTemplate(typeId, 0, userTo.Email, 0, userNewFriend.UserId, userTo.UserId, "", 0, "", "", 0);  
    }

    /// <summary>
    /// SendFriendAcceptedInviteNotification
    /// </summary>
    /// <param name="userToId"></param>
    /// <param name="userIdNewMember"></param>
    public static void SendFriendAcceptedInviteNotification(int userToId, int userIdNewMember)
    {
      UserFacade userFacade = new UserFacade();
      User userNewFriend = userFacade.GetUser(userIdNewMember);
      User userTo = userFacade.GetUser(userToId);

      int typeId = Mailer.EmailTypeUtility.GetTypeIdByName("FriendAcceptedInvite");
      Mailer.TemplateSherpa.DressAndSendTemplate(typeId, 0, userTo.Email, 0, userNewFriend.UserId, userTo.UserId, "", 0, "", "", 0);       
    }

    /// <summary>
    /// SendCommunityInvitation
    /// When no from name is specified user username
    /// </summary>
    public static void SendCommunityInvitation(int userId, int communityId, string communityName, string toEmail, string toName,
      string additionalmessage, int inviteID, string keyCode)
    {
      UserFacade userFacade = new UserFacade();
      User user = userFacade.GetUser(userId);

      SendCommunityInvitation(userId, communityId, communityName, user.FirstName, user.LastName, toEmail, toName, 
        additionalmessage, inviteID, keyCode);
    }

    /// <summary>
    /// SendCommunityInvitation
    /// </summary>
    public static void SendCommunityInvitation(int userId, int communityId, string communityName, string firstName, string lastName, 
      string toEmail, string toName, string additionalmessage, int inviteID, string keyCode)
    {
      int typeId = Mailer.EmailTypeUtility.GetTypeIdByName("CommunityInvite");
      Mailer.TemplateSherpa.DressAndSendTemplate(typeId, 0, toEmail, inviteID, userId, 0, keyCode, 0, additionalmessage, communityName, communityId);      
    }


    /// <summary>
    /// SendCommunityJoinNotification
    /// </summary>
    public static void SendCommunityJoinNotification(string toEmail, int joiningUserId, int commOwnerUserId,string communityName, int communityId)
    {
      int typeId = Mailer.EmailTypeUtility.GetTypeIdByName("CommunityJoinNotification");
      Mailer.TemplateSherpa.DressAndSendTemplate(typeId, 0, toEmail, 0, joiningUserId, commOwnerUserId, "", 0, "", communityName, communityId);
    }

    /// <summary>
    /// SendCommunityRequest
    /// </summary>
    public static void SendCommunityRequest(string toEmail, int requestingUserId, int commOwnerUserId, string communityName, int communityId)
    {
      int typeId = Mailer.EmailTypeUtility.GetTypeIdByName("CommunityRequest");
      Mailer.TemplateSherpa.DressAndSendTemplate(typeId, 0, toEmail, 0, requestingUserId, commOwnerUserId, "", 0, "", communityName, communityId);
    }


    /// <summary>
    /// SendCommunityAccepted
    /// </summary>
    public static void SendCommunityAccepted(string toEmail, int newUserId, int commOwnerUserId, string communityName, int communityId)
    {
      int typeId = Mailer.EmailTypeUtility.GetTypeIdByName("CommunityAccepted");
      Mailer.TemplateSherpa.DressAndSendTemplate(typeId, 0, toEmail, 0, newUserId, commOwnerUserId, "", 0, "", communityName, communityId);
    }

    /// <summary>
    /// SendEventInvitationEmail
    /// </summary>
    public static void SendEventEmail(Event evt, string emailTypeName, string toEmail, int toUserId, int fromUserId, string userMessage, string trackingMetric)
    {
        // calculate formatted event date & time
        string ampm = evt.StartTime.ToString("tt").ToLower();
        string eventDateTime = evt.StartTime.ToString("dddd, MMMM ") + evt.StartTime.Day + " at " +
            string.Format("{0:h:mm}{1}", evt.StartTime, ampm) + " EST";

        // grab the event url
        string eventUrl = "http://" + KanevaGlobals.SiteName + "/mykaneva/events/eventDetail.aspx?event=" + evt.EventId;

        // store subject and message as overload fields		
        int overloadId = 0; 
        overloadId = Mailer.Queuer.AddOverloadField(overloadId, "#EventName#", evt.Title);
        overloadId = Mailer.Queuer.AddOverloadField(overloadId, "#EventDetails#", 
            (KanevaGlobals.TruncateWithEllipsis(evt.Details, 50) + "<br/>" + eventDateTime + "<br/>" + evt.Location));
        overloadId = Mailer.Queuer.AddOverloadField(overloadId, "#EventLocation#", evt.Location);
        overloadId = Mailer.Queuer.AddOverloadField(overloadId, "#EventUrl#", eventUrl); 

        //if overloadid did not work don't send the email.
        if (overloadId != 0)
        {
            int typeId = Mailer.EmailTypeUtility.GetTypeIdByName(emailTypeName);
            Mailer.TemplateSherpa.DressAndSendTemplate(typeId, 0, toEmail, 0, fromUserId, toUserId, "", 0, userMessage, "", 0, overloadId, trackingMetric);
        }
    }

    #endregion

    private static string getStandardHeader()
    {
        string header = (string)Global.Cache()["mailHeader"];
        if (header == null || header.Length.Equals(0))
        {
            DataRow dr = Mailer.EmailTemplateUtility.GetTemplateByName("Header");
            header = dr["template_html"].ToString();                                
            Global.Cache().Insert("mailHeader", header, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
        }
        return header;
    }

    private static string getStandardFooter()
    {
        string footer = (string)Global.Cache()["mailFooter"];

        if (footer == null || footer.Length.Equals(0))
        {
          DataRow dr = Mailer.EmailTemplateUtility.GetTemplateByName("Footer");
          footer = dr["template_html"].ToString();                                
          Global.Cache().Insert("mailFooter", footer, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
        }
        return footer;
    }


        /// <summary>
        /// Send an email out for buying Kaneva Credits
        /// </summary>
        public static void SendKpointPurchaseEmail(int userId, int orderId)
        {
            UserFacade userFacade = new UserFacade();
            User user = userFacade.GetUser(userId);
            try
            {                                                         
                if (orderId > 0)
                {
                    DataRow drOrder = StoreUtility.GetOrder(orderId);

                    if (drOrder["point_transaction_id"] != DBNull.Value)
                    {
                        DataRow drPurchasePoints = StoreUtility.GetPurchasePointTransaction(Convert.ToInt32(drOrder["point_transaction_id"]));                    
                                          
                        string description = drPurchasePoints["description"].ToString();
                        string amount_debited = KanevaGlobals.FormatCurrency(Convert.ToDouble(drPurchasePoints["amount_debited"])).ToString();
                        string totalPoints = KanevaGlobals.FormatKPoints(Convert.ToDouble(drPurchasePoints["totalPoints"])).ToString();
                        string transaction_date = KanevaGlobals.FormatDateTime(Convert.ToDateTime(drPurchasePoints["transaction_date"])).ToString();

                        //SendEmail(KanevaGlobals.FromEmail, user.Email, "Kaneva Credit Order Confirmation", strMessage, true, false, 1);
                
                        string header = getStandardHeader();
                        string footer = getStandardFooter();

                        string subject = (string)Global.Cache()["mailPurchaseEmailSubject"];
                        string message = (string)Global.Cache()["mailPurchaseEmailBody"];

                        if (subject == null || subject.Length.Equals(0))
                        {
                            using (StreamReader sr = File.OpenText(System.Configuration.ConfigurationManager.AppSettings["EmailTemplatePath"].ToString() + "purchase.txt"))
                            {
                                DataRow dr = Mailer.EmailTemplateUtility.GetTemplateByName("Footer");  
                                
                              subject = sr.ReadLine();
                                subject = subject.Substring(subject.IndexOf(":") + 1).Trim();
                                sr.ReadLine();
                                message = sr.ReadToEnd();
                                Global.Cache().Insert("mailPurchaseEmailSubject", subject, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
                                Global.Cache().Insert("mailPurchaseEmailBody", message, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
                                sr.Close();
                            }
                        }

                        message = message.Replace("#header#", header);
                        message = message.Replace("#footer#", footer);
                        message = message.Replace("#orderid#", Convert.ToString(orderId));
                        message = message.Replace("#description#", description);
                        message = message.Replace("#amount_debited#", amount_debited);
                        message = message.Replace("#totalPoints#", totalPoints);
                        message = message.Replace("#transaction_date#", transaction_date);

                        message = message.Replace("#username#", user.Username);
                        message = message.Replace("#sitename#", KanevaGlobals.SiteName);

                        message = message.Replace("#toemail#", user.Email);

                        subject = subject.Replace("#username#", user.Username);

                        SendEmail(KanevaGlobals.FromEmail, user.Email, subject, message, true, true, 1);
                    }
                }
            }
            catch (Exception exc)
            {
                m_logger.Error("Error sending system email, SendPurchaseEmail, username = " + user.Username + ", orderId = " + orderId , exc);
            }

        }

        /// <summary>
        /// Send an email out for buying Kaneva Subscription
        /// </summary>
        public static void SendSubscriptionPurchaseEmail(int userId, int orderId, string interval)
        {
            UserFacade userFacade = new UserFacade();
            User user = userFacade.GetUser(userId);
            try
            {
                if (orderId > 0)
                {
                    DataRow drOrder = StoreUtility.GetOrder(orderId);

                    if (drOrder["point_transaction_id"] != DBNull.Value)
                    {
                        DataRow drPurchasePoints = StoreUtility.GetPurchasePointTransaction(Convert.ToInt32(drOrder["point_transaction_id"]));

                        string description = drPurchasePoints["description"].ToString();
                        string amount_debited = KanevaGlobals.FormatCurrency(Convert.ToDouble(drPurchasePoints["amount_debited"])).ToString();
                        string totalPoints = KanevaGlobals.FormatKPoints(Convert.ToDouble(drPurchasePoints["totalPoints"])).ToString();
                        string transaction_date = KanevaGlobals.FormatDateTime(Convert.ToDateTime(drPurchasePoints["transaction_date"])).ToString();

                        //SendEmail(KanevaGlobals.FromEmail, user.Email, "Kaneva Credit Order Confirmation", strMessage, true, false, 1);

                        string header = getStandardHeader();
                        string footer = getStandardFooter();

                        string subject = (string)Global.Cache()["mailPurchaseAPEmailSubject"];
                        string message = (string)Global.Cache()["mailPurchaseAPEmailBody"];

                        if (subject == null || subject.Length.Equals(0))
                        {
                            using (StreamReader sr = File.OpenText(System.Configuration.ConfigurationManager.AppSettings["EmailTemplatePath"].ToString() + "purchase-ap.txt"))
                            {
                                subject = sr.ReadLine();
                                subject = subject.Substring(subject.IndexOf(":") + 1).Trim();
                                sr.ReadLine();
                                message = sr.ReadToEnd();
                                Global.Cache().Insert("mailRegistrationAPEmailSubject", subject, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
                                Global.Cache().Insert("mailRegistrationAPEmailBody", message, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
                                sr.Close();
                            }
                        }
						
                        message = message.Replace("#header#", header);
                        message = message.Replace("#footer#", footer);
                        message = message.Replace("#orderid#", Convert.ToString(orderId));
                        message = message.Replace("#transaction_date#", transaction_date);
                        message = message.Replace("#description#", description);
                        message = message.Replace("#bill#", amount_debited);
						message = message.Replace("#billInterval#", interval);
                        message = message.Replace("#username#", user.Username);
                        message = message.Replace("#sitename#", KanevaGlobals.SiteName);
                        message = message.Replace("#toemail#", user.Email);
                        subject = subject.Replace("#username#", user.Username);

                        SendEmail(KanevaGlobals.FromEmail, user.Email, subject, message, true, true, 1);
                    }
                }
            }
            catch (Exception exc)
            {
                m_logger.Error("Error sending system email, SendPurchaseAPEmail, username = " + user.Username + ", orderId = " + orderId, exc);
            }
        }

        /// <summary>
        /// Send an email out for buying Kaneva Subscription
        /// </summary>
        public static void SendVIPSubscriptionPurchaseEmail(int userId, int orderId, string interval)
        {
            UserFacade userFacade = new UserFacade();
            User user = userFacade.GetUser(userId);
            try
            {
                if (orderId > 0)
                {
                    DataRow drOrder = StoreUtility.GetOrder(orderId);

                    if (drOrder["point_transaction_id"] != DBNull.Value)
                    {
                        DataRow drPurchasePoints = StoreUtility.GetPurchasePointTransaction(Convert.ToInt32(drOrder["point_transaction_id"]));

                        string description = drPurchasePoints["description"].ToString();
                        string amount_debited = KanevaGlobals.FormatCurrency(Convert.ToDouble(drPurchasePoints["amount_debited"])).ToString();
                        string totalPoints = KanevaGlobals.FormatKPoints(Convert.ToDouble(drPurchasePoints["totalPoints"])).ToString();
                        string transaction_date = KanevaGlobals.FormatDateTime(Convert.ToDateTime(drPurchasePoints["transaction_date"])).ToString();

                        //SendEmail(KanevaGlobals.FromEmail, user.Email, "Kaneva Credit Order Confirmation", strMessage, true, false, 1);

                        string header = getStandardHeader();
                        string footer = getStandardFooter();

                        string subject = (string)Global.Cache()["mailPurchaseVIPEmailSubject"];
                        string message = (string)Global.Cache()["mailPurchaseVIPEmailBody"];

                        if (subject == null || subject.Length.Equals(0))
                        {
                            using (StreamReader sr = File.OpenText(System.Configuration.ConfigurationManager.AppSettings["EmailTemplatePath"].ToString() + "purchase-vip.txt"))
                            {
                                subject = sr.ReadLine();
                                subject = subject.Substring(subject.IndexOf(":") + 1).Trim();
                                sr.ReadLine();
                                message = sr.ReadToEnd();
                                Global.Cache().Insert("mailRegistrationVIPEmailSubject", subject, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
                                Global.Cache().Insert("mailRegistrationVIPEmailBody", message, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
                                sr.Close();
                            }
                        }

                        message = message.Replace("#header#", header);
                        message = message.Replace("#footer#", footer);
                        message = message.Replace("#orderid#", Convert.ToString(orderId));
                        message = message.Replace("#transaction_date#", transaction_date);
                        message = message.Replace("#description#", description);
                        message = message.Replace("#bill#", amount_debited);
						message = message.Replace("#billInterval#", interval);
                        message = message.Replace("#credits_given#", totalPoints);

                        message = message.Replace("#username#", user.Username);
                        message = message.Replace("#sitename#", KanevaGlobals.SiteName);
                        message = message.Replace("#toemail#", user.Email);
                        subject = subject.Replace("#username#", user.Username);

                        SendEmail(KanevaGlobals.FromEmail, user.Email, subject, message, true, true, 1);
                    }
                }
            }
            catch (Exception exc)
            {
                m_logger.Error("Error sending system email, SendPurchaseAPEmail, username = " + user.Username + ", orderId = " + orderId, exc);
            }
        }

        /// <summary>
        /// Send an email out for buying Kaneva Subscription
        /// </summary>
        public static void SendAPVIPSubscriptionPurchaseEmail(int userId, int orderId)
        {
            UserFacade userFacade = new UserFacade();
            User user = userFacade.GetUser(userId);
            try
            {
                if (orderId > 0)
                {
                    DataRow drOrder = StoreUtility.GetOrder(orderId);

                    if (drOrder["point_transaction_id"] != DBNull.Value)
                    {
                        DataRow drPurchasePoints = StoreUtility.GetPurchasePointTransaction(Convert.ToInt32(drOrder["point_transaction_id"]));

                        string description = drPurchasePoints["description"].ToString();
                        string amount_debited = KanevaGlobals.FormatCurrency(Convert.ToDouble(drPurchasePoints["amount_debited"])).ToString();
                        string totalPoints = KanevaGlobals.FormatKPoints(Convert.ToDouble(drPurchasePoints["totalPoints"])).ToString();
                        string transaction_date = KanevaGlobals.FormatDateTime(Convert.ToDateTime(drPurchasePoints["transaction_date"])).ToString();

                        //SendEmail(KanevaGlobals.FromEmail, user.Email, "Kaneva Credit Order Confirmation", strMessage, true, false, 1);

                        string header = getStandardHeader();
                        string footer = getStandardFooter();

                        string subject = (string)Global.Cache()["mailPurchaseAPVIPEmailSubject"];
                        string message = (string)Global.Cache()["mailPurchaseAPVIPEmailBody"];

                        if (subject == null || subject.Length.Equals(0))
                        {
                            using (StreamReader sr = File.OpenText(System.Configuration.ConfigurationManager.AppSettings["EmailTemplatePath"].ToString() + "purchase-ap-vip.txt"))
                            {
                                subject = sr.ReadLine();
                                subject = subject.Substring(subject.IndexOf(":") + 1).Trim();
                                sr.ReadLine();
                                message = sr.ReadToEnd();
                                Global.Cache().Insert("mailRegistrationAPVIPEmailSubject", subject, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
                                Global.Cache().Insert("mailRegistrationAPVIPEmailBody", message, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
                                sr.Close();
                            }
                        }

                        message = message.Replace("#header#", header);
                        message = message.Replace("#footer#", footer);
                        message = message.Replace("#orderid#", Convert.ToString(orderId));
                        message = message.Replace("#transaction_date#", transaction_date);
                        message = message.Replace("#description#", description);
                        message = message.Replace("#bill#", amount_debited);
                        message = message.Replace("#credits_given#", totalPoints);

                        message = message.Replace("#username#", user.Username);
                        message = message.Replace("#sitename#", KanevaGlobals.SiteName);
                        message = message.Replace("#toemail#", user.Email);
                        subject = subject.Replace("#username#", user.Username);

                        SendEmail(KanevaGlobals.FromEmail, user.Email, subject, message, true, true, 1);
                    }
                }
            }
            catch (Exception exc)
            {
                m_logger.Error("Error sending system email, SendPurchaseAPEmail, username = " + user.Username + ", orderId = " + orderId, exc);
            }
        }

        /// <summary>
        /// SendRegistrationEmail
        /// </summary>
        /// <param name="username"></param>
        /// <param name="userId"></param>
        /// <param name="activationKey"></param>
        public static void SendRegistrationEmail(string username, string email, string activationKey)
        {
            SendRegistrationEmail(username, email, activationKey, "signup");
        }

        /// <summary>
        /// SendRegistrationEmail
        /// </summary>
        /// <param name="username"></param>
        /// <param name="userId"></param>
        /// <param name="activationKey"></param>
        /// <param name="returnPage"></param>
        public static void SendRegistrationEmail(string username, string email, string activationKey, string returnPage)
        {
            try
            {
                string header = getStandardHeader();
                string footer = getStandardFooter();

                string subject = (string)Global.Cache()["mailRegistrationEmailSubject"];
                string message = (string)Global.Cache()["mailRegistrationEmailBody"];

                if (subject == null || subject.Length.Equals(0))
                {
                    using (StreamReader sr = File.OpenText(System.Configuration.ConfigurationManager.AppSettings["EmailTemplatePath"].ToString() + "activate-account.txt"))
                    {
                        subject = sr.ReadLine();
                        subject = subject.Substring(subject.IndexOf(":") + 1).Trim();
                        sr.ReadLine();
                        message = sr.ReadToEnd();
                        Global.Cache().Insert("mailRegistrationEmailSubject", subject, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
                        Global.Cache().Insert("mailRegistrationEmailBody", message, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
                        sr.Close();
                    }
                }

                message = message.Replace("#header#", header);
                message = message.Replace("#footer#", footer);
                message = message.Replace("#username#", username);
                message = message.Replace("#sitename#", KanevaGlobals.SiteName);
                message = message.Replace("#activationkey#", activationKey);
                message = message.Replace("#returnpage#", returnPage);
                message = message.Replace("#toemail#", email);

                subject = subject.Replace("#username#", username);

                SendEmail("myaccount@kaneva.com", email, subject, message, true, true, 1);
            }
            catch (Exception exc)
            {
                m_logger.Error("Error sending system email, SendRegistrationEmail, username =" + username, exc);                
            }
        }

        /// <summary>
        /// SendVerificationEmail
        /// </summary>
        /// <param name="username"></param>
        /// <param name="userId"></param>
        /// <param name="activationKey"></param>
        /// <param name="returnPage"></param>
        public static void SendVerificationEmail (string username, string email, string activationKey, string returnPage)
        {
            try
            {
                string header = getStandardHeader ();
                string footer = getStandardFooter ();

                string subject = (string) Global.Cache ()["mailVerificationEmailSubject"];
                string message = (string) Global.Cache ()["mailVerificationEmailBody"];

                if (subject == null || subject.Length.Equals (0))
                {
                    using (StreamReader sr = File.OpenText (System.Configuration.ConfigurationManager.AppSettings["EmailTemplatePath"].ToString () + "validate-email.txt"))
                    {
                        subject = sr.ReadLine ();
                        subject = subject.Substring (subject.IndexOf (":") + 1).Trim ();
                        sr.ReadLine ();
                        message = sr.ReadToEnd ();
                        Global.Cache ().Insert ("mailVerificationEmailSubject", subject, null, DateTime.Now.AddMinutes (30), System.Web.Caching.Cache.NoSlidingExpiration);
                        Global.Cache ().Insert ("mailVerificationEmailBody", message, null, DateTime.Now.AddMinutes (30), System.Web.Caching.Cache.NoSlidingExpiration);
                        sr.Close ();
                    }
                }

                message = message.Replace ("#header#", header);
                message = message.Replace ("#footer#", footer);
                message = message.Replace ("#username#", username);
                message = message.Replace ("#sitename#", KanevaGlobals.SiteName);
                message = message.Replace ("#activationkey#", activationKey);
                message = message.Replace ("#returnpage#", returnPage);
                message = message.Replace ("#toemail#", email);

                subject = subject.Replace ("#username#", username);

                SendEmail ("myaccount@kaneva.com", email, subject, message, true, true, 1);
            }
            catch (Exception exc)
            {
                m_logger.Error ("Error sending system email, SendVerificationEmail, username =" + username, exc);
            }
        }

        /// <summary>
        /// SendRegistrationEmailCoBrand
        /// </summary>
        /// <param name="username"></param>
        /// <param name="userId"></param>
        /// <param name="activationKey"></param>
        /// <param name="returnPage"></param>
        public static void SendRegistrationEmailCoBrand(string body_top, string body_bottom, string subject, string email, string username, string activationKey, string returnPage)
        {
            try
            {
                string header = getStandardHeader();
                string footer = getStandardFooter();

                string message = (string)Global.Cache()["mailRegistrationEmailCoBrandBody"];

                if (message == null || message.Length.Equals(0))
                {
                    using (StreamReader sr = File.OpenText(System.Configuration.ConfigurationManager.AppSettings["EmailTemplatePath"].ToString() + "activate-account-cobrand.txt"))
                    {
                        sr.ReadLine();
                        sr.ReadLine();
                        message = sr.ReadToEnd();
                        Global.Cache().Insert("mailRegistrationEmailCoBrandBody", message, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
                        sr.Close();
                    }

                }

                message = message.Replace("#header#", header);
                message = message.Replace("#footer#", footer);
                message = message.Replace("#username#", username);
                message = message.Replace("#sitename#", KanevaGlobals.SiteName);
                message = message.Replace("#activationkey#", activationKey);
                message = message.Replace("#returnpage#", returnPage);
                message = message.Replace("#toemail#", email);
                message = message.Replace("#body_top#", body_top);
                message = message.Replace("#body_bottom#", body_bottom);

                subject = subject.Replace("#username#", username);

                SendEmail("myaccount@kaneva.com", email, subject, message, true, true, 1);
            }
            catch (Exception exc)
            {
                m_logger.Error("Error sending system email, SendRegistrationEmailCoBrand, username =" + username, exc);
            }
        }

        /// <summary>
        /// SendEmailChange
        /// </summary>
        /// <param name="username"></param>
        /// <param name="userId"></param>
        /// <param name="activationKey"></param>
        /// <param name="returnPage"></param>
        public static void SendEmailChange(string username, string email, string activationKey, string returnPage)
        {
            try
            {
                string header = getStandardHeader();
                string footer = getStandardFooter();

                string subject = (string)Global.Cache()["mailEmailChangeSubject"];
                string message = (string)Global.Cache()["mailEmailChangeBody"];

                if (subject == null || subject.Length.Equals(0))
                {
                    using (StreamReader sr = File.OpenText(System.Configuration.ConfigurationManager.AppSettings["EmailTemplatePath"].ToString() + "email-change.txt"))
                    {
                        subject = sr.ReadLine();
                        subject = subject.Substring(subject.IndexOf(":") + 1).Trim();
                        sr.ReadLine();
                        message = sr.ReadToEnd();
                        Global.Cache().Insert("mailEmailChangeSubject", subject, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
                        Global.Cache().Insert("mailEmailChangeBody", message, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
                        sr.Close();
                    }
                }

                message = message.Replace("#header#", header);
                message = message.Replace("#footer#", footer);
                message = message.Replace("#username#", username);
                message = message.Replace("#sitename#", KanevaGlobals.SiteName);
                message = message.Replace("#activationkey#", activationKey);
                message = message.Replace("#returnpage#", returnPage);
                message = message.Replace("#toemail#", email);

                subject = subject.Replace("#username#", username);

                SendEmail(KanevaGlobals.FromEmail, email, subject, message, true, true, 1);
            }
            catch (Exception exc)
            {
                m_logger.Error("Error sending system email, SendEmailChange, username =" + username, exc);
            }
        }

        /// <summary>
        /// SendPasswordRequest
        /// </summary>
        /// <param name="username"></param>
        /// <param name="userId"></param>
        /// <param name="activationKey"></param>
        /// <param name="returnPage"></param>
        public static void SendPasswordRequest(string username, string email, string keyvalue)
        {
            try
            {
                string header = getStandardHeader();
                string footer = getStandardFooter();

                string subject = (string)Global.Cache()["mailPasswordRequestSubject"];
                string message = (string)Global.Cache()["mailPasswordRequest"];

                if (message == null || message.Length.Equals(0))
                {
                    using (StreamReader sr = File.OpenText(System.Configuration.ConfigurationManager.AppSettings["EmailTemplatePath"].ToString() + "forgot-password.txt"))
                    {
                        subject = sr.ReadLine();
                        subject = subject.Substring(subject.IndexOf(":") + 1).Trim();
                        sr.ReadLine();
                        message = sr.ReadToEnd();
                        Global.Cache().Insert("mailPasswordRequestSubject", subject, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
                        Global.Cache().Insert("mailPasswordRequest", message, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
                        sr.Close();
                    }


                }

                message = message.Replace("#header#", header);
                message = message.Replace("#footer#", footer);
                message = message.Replace("#username#", username);
                message = message.Replace("#sitename#", KanevaGlobals.SiteName);
                message = message.Replace("#key_value#", keyvalue);
                message = message.Replace("#toemail#", email);

                SendEmail("myaccount@kaneva.com", email, subject, message, true, true, 1);
            }
            catch (Exception exc)
            {
                m_logger.Error("Error sending system email, username =" + username, exc);
            }
        }

        /// <summary>
        /// SendUsernameResetEmail
        /// </summary>
        /// <param name="username"></param>
        /// <param name="userId"></param>
        /// <param name="activationKey"></param>
        /// <param name="returnPage"></param>
        public static void SendUsernameResetEmail (string username, string password, string email)
        {
            try
            {
                string header = getStandardHeader ();
                string footer = getStandardFooter ();

                string subject = (string) Global.Cache ()["mailUsernameResetSubject"];
                string message = (string) Global.Cache ()["mailUsernameReset"];

                if (message == null || message.Length.Equals (0))
                {
                    using (StreamReader sr = File.OpenText (System.Configuration.ConfigurationManager.AppSettings["EmailTemplatePath"].ToString () + "username-reset.txt"))
                    {
                        subject = sr.ReadLine ();
                        subject = subject.Substring (subject.IndexOf (":") + 1).Trim ();
                        sr.ReadLine ();
                        message = sr.ReadToEnd ();
                        Global.Cache ().Insert ("mailUsernameResetSubject", subject, null, DateTime.Now.AddMinutes (30), System.Web.Caching.Cache.NoSlidingExpiration);
                        Global.Cache ().Insert ("mailUsernameReset", message, null, DateTime.Now.AddMinutes (30), System.Web.Caching.Cache.NoSlidingExpiration);
                        sr.Close ();
                    }
                }

                message = message.Replace ("#header#", header);
                message = message.Replace ("#footer#", footer);
                message = message.Replace ("#temp_username#", username);
                message = message.Replace ("#temp_password#", password);
                message = message.Replace ("#sitename#", KanevaGlobals.SiteName);

                SendEmail (KanevaGlobals.FromSupportEmail, email, subject, message, true, true, 1);
            }
            catch (Exception exc)
            {
                m_logger.Error ("Error sending system email, username =" + username, exc);
            }
        }

        /// <summary>
        /// ShareAssetWithFriend
        /// </summary>
        /// <param name="username"></param>
        /// <param name="userId"></param>
        /// <param name="activationKey"></param>
        public static void ShareAssetWithFriend(int userId, string recipient, int assetId, string sender, string message, DataRow drAsset, string assetType, System.Web.UI.Page page)
        {
            string senderURL = sender;
            string assetLink = "http://" + KanevaGlobals.SiteName + "/asset/" + assetId + ".storeItem";

            if (userId > 0)
            {
                UserFacade userFacade = new UserFacade();
                User user = userFacade.GetUser(userId);

                sender = user.Username;
                senderURL = "member <a target='_resource' href='" + KanevaGlobals.GetPersonalChannelUrl(user.NameNoSpaces) + "' >" + sender + "</a>";
            }

            string subject = sender + " has sent you a Kaneva " + assetType;
            string strMessage = "<html><body bgcolor='#ffffff' topmargin='0' leftmargin='0'>";
            strMessage += "<br> Kaneva " + senderURL + " has shared the " + assetType + " <a target='_resource' href='" + assetLink + "'>" + drAsset["name"].ToString() + "</a> with you.";

            strMessage += "<br><br>" + assetType + " link: ";
            strMessage += "<br> <a target='_resource' href='" + assetLink + "'>" + assetLink + "</a>";

            // Attach optional personal message?
            if (message.Trim().Length > 0)
            {
                strMessage += "<br>\r\nPersonal message from " + sender + ":";
                strMessage += "<br><br>" + message;
            }

            strMessage += "<br><br>";
            strMessage += "<br><span style='font-size:11px;'>This email was sent by Kaneva, 270 Carpenter Drive, Sandy Springs, Georgia, 30328 USA.</span>";

            strMessage += "</body></html>";
            SendEmail(KanevaGlobals.FromEmail, recipient, subject, strMessage, true, true, 2);
        }

        /// <summary>
        /// ShareChannelWithFriend
        /// </summary>
        /// <param name="username"></param>
        /// <param name="userId"></param>
        /// <param name="activationKey"></param>
        public static void ShareChannelWithFriend(int userId, string recipient, int channelId, string sender, string additionalmessage, System.Web.UI.Page page)
        {
            string header = getStandardHeader();
            string footer = getStandardFooter();

            string subject = (string)Global.Cache()["mailShareChannelSubject"];
            string message = (string)Global.Cache()["mailShareChannel"];

            if (message == null || message.Length.Equals(0))
            {
                using (StreamReader sr = File.OpenText(System.Configuration.ConfigurationManager.AppSettings["EmailTemplatePath"].ToString() + "tell-others-community.txt"))
                {
                    subject = sr.ReadLine();
                    subject = subject.Substring(subject.IndexOf(":") + 1).Trim();
                    sr.ReadLine();
                    message = sr.ReadToEnd();
                    Global.Cache().Insert("mailShareChannelSubject", subject, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
                    Global.Cache().Insert("mailShareChannel", message, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
                    sr.Close();
                }
            }

            DataRow drChannel = CommunityUtility.GetCommunity(channelId);
            string username = sender; // username of the person sending the email
            string communitylink = KanevaGlobals.GetBroadcastChannelUrl(drChannel["name_no_spaces"].ToString()); // link of the profile being shared
            string communityimage = CommunityUtility.GetBroadcastChannelImageURL(drChannel["thumbnail_small_path"].ToString(), "sm");
            string communityname = drChannel["name"].ToString();
            string description = drChannel["description"].ToString();
            string userlink = KanevaGlobals.SiteName;

            if (userId > 0)
            {
                UserFacade userFacade = new UserFacade();
                User user = userFacade.GetUser(userId);

                username = user.Username;
                userlink = KanevaGlobals.GetPersonalChannelUrl(user.NameNoSpaces);
            }

            message = message.Replace("#header#", header);
            message = message.Replace("#footer#", footer);

            message = message.Replace("#username#", username);
            message = message.Replace("#userlink#", userlink);
            message = message.Replace("#communitylink#", communitylink);
            message = message.Replace("#communityimage#", communityimage);
            message = message.Replace("#communityname#", communityname);
            message = message.Replace("#description#", description);
            message = message.Replace("#sitename#", KanevaGlobals.SiteName);
            message = message.Replace("#additionalmessage#", additionalmessage);
            message = message.Replace("#toemail#", recipient);

            subject = subject.Replace("#username#", username);

            SendEmail(KanevaGlobals.FromEmail, recipient, subject, message, true, true, 2);
        }

        /// <summary>
        /// ShareProfileWithFriend
        /// </summary>
        public static void ShareProfileWithFriend(int userId, string recipient, int profileId, string sender, string additionalmessage, System.Web.UI.Page page)
        {
            string header = getStandardHeader();
            string footer = getStandardFooter();

            string subject = (string)Global.Cache()["mailShareProfileSubject"];
            string message = (string)Global.Cache()["mailShareProfile"];

            if (message == null || message.Length.Equals(0))
            {
                using (StreamReader sr = File.OpenText(System.Configuration.ConfigurationManager.AppSettings["EmailTemplatePath"].ToString() + "tell-others-profile.txt"))
                {
                    subject = sr.ReadLine();
                    subject = subject.Substring(subject.IndexOf(":") + 1).Trim();
                    sr.ReadLine();
                    message = sr.ReadToEnd();
                    Global.Cache().Insert("mailShareProfileSubject", subject, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
                    Global.Cache().Insert("mailShareProfile", message, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
                    sr.Close();
                }

            }

            UserFacade userFacade = new UserFacade();
            User userChannel = userFacade.GetUser(profileId);

            string username = sender; // username of the person sending the email
            string profilelink = KanevaGlobals.GetPersonalChannelUrl(userChannel.NameNoSpaces); // link of the profile being shared
            string profileimage = UsersUtility.GetProfileImageURL(userChannel.ThumbnailSmallPath, "sm", userChannel.Gender);
            string profileusername = userChannel.Username;
            string location = userChannel.Location;
            string userlink = KanevaGlobals.SiteName;

            if (userId > 0)
            {
                User user = userFacade.GetUser(userId);

                username = user.Username;
                userlink = KanevaGlobals.GetPersonalChannelUrl(user.NameNoSpaces);
            }
          
            message = message.Replace("#header#", header);
            message = message.Replace("#footer#", footer);

            message = message.Replace("#username#", username);
            message = message.Replace("#userlink#", userlink);
            message = message.Replace("#profilelink#", profilelink);
            message = message.Replace("#profileimage#", profileimage);
            message = message.Replace("#profileusername#", profileusername);
            message = message.Replace("#location#", location); 
            message = message.Replace("#sitename#", KanevaGlobals.SiteName);
            message = message.Replace("#additionalmessage#", additionalmessage);
            message = message.Replace("#toemail#", recipient);

            subject = subject.Replace("#username#", username);

            SendEmail(KanevaGlobals.FromEmail, recipient, subject, message, true, true, 2);
        }

       
        public static void SendEmail(MailAddress maFrom, MailAddress maTo, string subject, string body, bool isHtml, bool bSendBCCToKaneva, int tier)
        {
            MailUtility.SendEmail(maFrom, maTo, subject, body, isHtml, bSendBCCToKaneva, tier);
        }
    
        /// <summary>
        /// Sends a single email. 
        /// </summary>
        public static void SendEmail(string from, string to, string subject, string body, bool isHtml, bool bSendBCCToKaneva, int tier)
        {
          MailUtility.SendEmail(from, to, subject, body, isHtml, bSendBCCToKaneva, tier);
        }


		// Logger
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

	}
}
