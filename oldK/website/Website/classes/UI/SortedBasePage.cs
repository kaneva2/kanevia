///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for SortedBasePage.
	/// </summary>
	public class SortedBasePage : MainTemplatePage
	{
		public SortedBasePage()
		{
		}

		/// <summary>
		/// OnInit
		/// </summary>
		/// <param name="e"></param>
		protected override void OnInit (System.EventArgs e) 
		{
			base.OnInit (e);

			// Add the sorting controls
			btnSort = new Button ();
			hidSortColumn = new HtmlInputHidden ();

			btnSort.Style.Add ("DISPLAY", "none");
			btnSort.Click += new EventHandler(btnSort_Click);

			m_Form.Controls.Add (btnSort);
			m_Form.Controls.Add (hidSortColumn);
		}

		/// <summary>
		/// Current sort expression
		/// </summary>
		public string CurrentSort
		{
			get 
			{
				if (ViewState ["cs"] == null)
				{
					return DEFAULT_SORT;
				}
				else
				{
					return ViewState ["cs"].ToString ();
				}
			} 
			set
			{
				ViewState ["cs"] = value;
			}
		}

		/// <summary>
		/// Current sort order
		/// </summary>
		public string CurrentSortOrder
		{
			get 
			{
				if (ViewState ["cso"] == null)
				{
					return DEFAULT_SORT_ORDER;
				}
				else
				{
					return ViewState ["cso"].ToString ();
				}
			} 
			set
			{
				ViewState ["cso"] = value;
			}
		}

		/// <summary>
		/// They clicked to sort a column
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected virtual void btnSort_Click (object sender, System.EventArgs e)
		{
			// Set the sort order
			if (CurrentSortOrder == "DESC")
			{
				CurrentSortOrder = "ASC";
			}
			else if (CurrentSortOrder == "ASC")
			{
				CurrentSortOrder = "DESC";
			}
			else 
			{
				CurrentSortOrder = "ASC";
			}

			// Set the sort column
			if (!CurrentSort.Equals (hidSortColumn.Value))
			{
				// Changing sort expression, so set to ASC
				CurrentSortOrder = "ASC";
				CurrentSort = hidSortColumn.Value;
			}
		}

		/// <summary>
		/// Set Header Sort column Text
		/// </summary>
		protected void SetHeaderSortText (DataGrid dgrdToSort)
		{
			DataGridColumn dgrdColumn;
			string strippedHeader;

			// Which arrow to use?
			string arrowImage = "arrow_sort_up.gif";
			if (CurrentSortOrder.Equals ("DESC"))
			{
				arrowImage = "arrow_sort.gif";
			}

			// Loop through all sortable columns
			for (int i = 0; i < dgrdToSort.Columns.Count; i ++)
			{
				dgrdColumn = dgrdToSort.Columns [i];

				// Is it a sortable column?
				if (dgrdColumn.SortExpression.Length > 0 && dgrdToSort.AllowSorting)
				{
					strippedHeader = dgrdColumn.HeaderText;

					if (dgrdToSort.EnableViewState)
					{
						strippedHeader = strippedHeader.Replace ("<img src=" + ResolveUrl ("~/images/arrow_sort.gif") + " border=0/>", "");
						strippedHeader = strippedHeader.Replace ("<img src=" + ResolveUrl ("~/images/arrow_sort_up.gif") + " border=0/>", "");
					}

					// Is this column the current sorted?
					if (CurrentSort.Equals (dgrdColumn.SortExpression))
					{
						dgrdColumn.HeaderText = "<a href='#' onclick='javascript:document.frmMain." + hidSortColumn.ClientID + ".value=\"" + dgrdColumn.SortExpression + "\";javascript:document.frmMain." + btnSort.ClientID + ".click();'>" + strippedHeader + "<img src=" + ResolveUrl ("~/images/" + arrowImage) + " border=0/></a>";
					}
					else
					{
						dgrdColumn.HeaderText = "<a href='#' onclick='javascript:document.frmMain." + hidSortColumn.ClientID + ".value=\"" + dgrdColumn.SortExpression + "\";javascript:document.frmMain." + btnSort.ClientID + ".click();'>" + strippedHeader + "</a>";
					}
				}
			}
		}



		/// <summary>
		/// DEFAULT_SORT
		/// </summary>
		/// <returns></returns>
		protected virtual string DEFAULT_SORT
		{
			get
			{
				return "a.name";
			}
		}

		/// <summary>
		/// DEFAULT_SORT_ORDER
		/// </summary>
		/// <returns></returns>
		protected virtual string DEFAULT_SORT_ORDER
		{
			get
			{
				return "ASC";
			}
		}

		// Sorting
		protected Button btnSort;
		protected HtmlInputHidden hidSortColumn;
	}
}
