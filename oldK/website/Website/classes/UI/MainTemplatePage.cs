///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Diagnostics;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for MainTemplatePage.
	/// </summary>
	public class MainTemplatePage : BasePage
	{
		/// <summary>
		/// Constructor
		/// </summary>
		public MainTemplatePage ()
		{
			m_Form = new HtmlForm ();
			m_Form.ID = "frmMain";
			m_Form.Enctype = "multipart/form-data";

			m_ControlBin = new ControlCollection (this);
		}

        /// <summary>
        /// OnPreRender
        /// </summary>
        protected override void OnPreRender(EventArgs e) 
        { 
            base.OnPreRender(e);

            //if (IsAdministrator())
            //{
                // Page timing
                Stopwatch stopwatch = (Stopwatch)this.Context.Items["Stopwatch"];
                stopwatch.Stop();

                TimeSpan ts = stopwatch.Elapsed;
                string elapsedTime = String.Format("({0}ms)", ts.TotalMilliseconds);

                Label lblLoadTime = (Label)_footer.FindControl("lblLoadTime");
                lblLoadTime.Text = elapsedTime;
            //}
        }

		/// <summary>
		/// OnInit
		/// </summary>
		/// <param name="e"></param>
		protected override void OnInit (System.EventArgs e) 
		{
			base.OnInit (e);

            //if (IsAdministrator())
            //{
                // Page timing
                Stopwatch stopwatch = new Stopwatch();
                this.Context.Items["Stopwatch"] = stopwatch;
                stopwatch.Start();
            //}

		//	StartHtml ucStartHtml = (StartHtml) LoadControl ("~/usercontrols/StartHtml.ascx");
			UC_StartHTML = (StartHtml) LoadControl ("~/usercontrols/StartHtml.ascx");

			this.Controls.AddAt (0, ucStartHtml);

			_header = (Header) LoadControl ("~/usercontrols/Header.ascx");
			m_Form.Controls.Add (_header);

			// Start the main formatting table
			Literal litMainTableStart = new Literal ();
            litMainTableStart.ID = "litMainTableStart";
			litMainTableStart.Text = "<TABLE id=\"tblMain\" cellSpacing=\"0\" cellPadding=\"0\" border=\"0\" width=\"1002\" ><TR><TD valign=\"top\" id=\"tdLeftNav\">";		
			m_Form.Controls.Add (litMainTableStart);

			//m_Form.Controls.Add (LoadControl ("~/usercontrols/LeftPanel.ascx"));

			// Start a new column
			Literal litMainTableNewColumn = new Literal ();
            litMainTableNewColumn.ID = "litMainTableNewColumn";
            litMainTableNewColumn.Text = "</TD><!-- Begin Center ---><TD id=\"tdMainCenterStage\" valign=\"top\">";
			m_Form.Controls.Add (litMainTableNewColumn);

			// Start the main template
			Literal litMainBodyStart = new Literal ();
			litMainBodyStart.ID = "litMainBodyStart";
            litMainBodyStart.Text = "<!-- Start main body --><table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\"><tr><TD valign=\"top\"><center>";
			m_Form.Controls.Add (litMainBodyStart);

			// Add the filter bar
			m_Form.Controls.Add (LoadControl (Filter));

			// Add the main content
			for(int i = 0; i < m_ControlBin.Count; i++)
			{
				m_Form.Controls.Add (m_ControlBin [i]);
			}

			Literal litMainTableClose = new Literal ();
            litMainTableClose.ID = "litMainTableClose";
            litMainTableClose.Text = "</center></TD></TR></TABLE></TD></TR></TABLE>";
			m_Form.Controls.Add (litMainTableClose);

			_footer = (Footer2) LoadControl ("~/usercontrols/Footer2.ascx");
			m_Form.Controls.Add (_footer);

            // Check to see if there is a script manager on page, if not then add it
            if (ScriptManager.GetCurrent (Page) == null)
            {
                ScriptManager sMgr = new ScriptManager ();
                this.Controls.AddAt (0, sMgr);
            }
 
			this.Controls.Add (m_Form);
			this.Controls.Add (LoadControl ("~/usercontrols/EndHtml.ascx"));
		}

		/// <summary>
		/// The page title property
		/// </summary>
		protected new string Title
		{
			set
			{
				UC_StartHTML.Title = value;
			}
			get 
			{
				return UC_StartHTML.Title;
			}
		}

    /// <summary>
    /// Standards Mode Property
    /// </summary>
    protected bool StandardsMode
    {
        set
        {
            UC_StartHTML.StandardsMode = value;
        }
        get
        {
            return UC_StartHTML.StandardsMode;
        }
    }

    /// <summary>
    /// Standards Mode Property
    /// </summary>
    protected string StyleSheets
    {
      set
      {
        UC_StartHTML.StyleSheets = value;
      }
      get
      {
        return UC_StartHTML.StyleSheets;
      }
    }

		/// <summary>
		/// Instance of the usercontrol StartHTML
		/// </summary>
		protected StartHtml UC_StartHTML 
		{
			set
			{
				ucStartHtml = value;
			}
			get 
			{
				return ucStartHtml;
			}
		}

		/// <summary>
		/// sets or gets meta data keyword html
		/// </summary>
		public string MetaDataKeywords 
		{
			set
			{
				if (value != string.Empty)
				{
					UC_StartHTML.MetaDataKeywords = value;
				}
			}
			get
			{
				return UC_StartHTML.MetaDataKeywords;
			}
		}

		/// <summary>
		/// sets or gets meta data description html
		/// </summary>
		public string MetaDataDescription 
		{
			set
			{
				if (value != string.Empty)
				{
					UC_StartHTML.MetaDataDescription = value;
				}
			}
			get
			{
				return UC_StartHTML.MetaDataDescription;
			}
		}

		/// <summary>
		/// sets or gets meta data title html
		/// </summary>
		public string MetaDataTitle 
		{
			set
			{
				if (value != string.Empty)
				{
					UC_StartHTML.MetaDataTitle = value;
				}
			}
			get
			{
				return UC_StartHTML.MetaDataTitle;
			}
		}

        /// <summary>
        /// sets or gets web analytics experiment code
        /// </summary>
        public string WebAnalyticsExperimentCode
        {
            set
            {
                if (value != string.Empty)
                {
                    UC_StartHTML.WebAnalyticsExperimentCode = value;
                }
            }
            get
            {
                return UC_StartHTML.WebAnalyticsExperimentCode;
            }
        }

		protected override void AddParsedSubObject (object obj) 
		{			
			// Add the control to our holding bin collection
			m_ControlBin.Add ((System.Web.UI.Control) obj);
		}

		/// <summary>
		/// Filter Property
		/// </summary>
		public string Filter
		{
			set
			{
				m_Filter = value;
			}
			get
			{
				return m_Filter;
			}
		}

		/// <summary>
		/// Form Property
		/// </summary>
		public HtmlForm FormMain
		{
			get
			{
				return m_Form;
			}
		}

		public Header HeaderNav
		{
			get { return _header; }
			set { _header = value; }
		}

		public Footer2 FooterNav
		{
			get { return _footer; }
			set { _footer = value; }
		}

		public StartHtml StartHTML
		{
			get { return ucStartHtml; }
			set { ucStartHtml = value; }
		}

		Header _header;
		Footer2 _footer;

		StartHtml ucStartHtml = new StartHtml();

		/// <summary>
		/// Standard form for the page
		/// </summary>
		protected HtmlForm m_Form;

		/// <summary>
		/// Temporary control collection to hold parsed controls.
		/// </summary>
		protected ControlCollection m_ControlBin;

		protected string m_Filter = "~/usercontrols/Filter.ascx";

		protected string m_Title = "";
	}
}
