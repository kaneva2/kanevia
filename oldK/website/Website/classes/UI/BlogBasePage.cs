///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Collections;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for BlogBasePage.
	/// </summary>
	public class BlogBasePage : MainTemplatePage
	{
		protected BlogBasePage () 
		{
		}

		protected override void OnInit (System.EventArgs e) 
		{
			base.OnInit (e);
			this.Load += new System.EventHandler(this.Page_Load);
		}

		private void Page_Load (object sender, System.EventArgs e)
		{
			// Add the javascript for deleting topics
			string scriptString = "<script language=\"javaScript\">\n<!--\n function DeleteTopic (id) {\n";
			scriptString += "	document.all.TopicId.value = id;\n";			// save the page number clicked to the hidden field
			scriptString += "	__doPostBack('DeleteTopic','');\n";				// call the __doPostBack function to post back the form and execute the PageClick event
			scriptString += "}\n// -->\n";
			scriptString += "</script>";

            if (!ClientScript.IsClientScriptBlockRegistered(GetType (), "DeleteFeature"))
			{
                ClientScript.RegisterClientScriptBlock(GetType(), "DeleteFeature", scriptString);
			}
		}

		/// <summary>
		/// Is the blog image visible
		/// </summary>
		/// <param name="imageLength"></param>
		/// <returns></returns>
		public bool IsImageVisible (int imageLength)
		{
			return (imageLength > 0);
		}

		/// <summary>
		/// Get Blog Details Link
		/// </summary>
		/// <param name="topicId"></param>
		/// <returns></returns>
		public string GetBlogEditLink (int topicId, int communityId, int defaultBlogCategory)
		{
			return Page.ResolveUrl ("~/blog/blogEdit.aspx?topicId=" + topicId + "&communityId=" + communityId); 
		}	

		/// <summary>
		/// Return the delete javascript
		/// </summary>
		/// <param name="TopicID"></param>
		/// <returns></returns>
		public string GetDeleteScript (int TopicID)
		{
			return "javascript:if (confirm(\"Are you sure you want to delete this blog?\")){DeleteTopic (" + TopicID + ")};";
		}

		/// <summary>
		/// Get Blog Comments Link
		/// </summary>
		/// <param name="topicId"></param>
		/// <returns></returns>
		public string GetBlogCommentsLink (int topicId, int communityId)
		{
			return Page.ResolveUrl ("~/blog/blogComments.aspx?topicId=" + topicId+ "&communityId=" + communityId);  
		}

		/// <summary>
		/// GetHeaderBackground
		/// </summary>
		/// <returns></returns>
		public string GetHeaderBackground ()
		{
			return Page.ResolveUrl ("~/images/main_headlinebar_bg.gif");
		}
	}
}
