///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Collections;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for StoreBasePage.
	/// </summary>
	public class StoreBasePage : SortedBasePage
	{
		public StoreBasePage()
		{
		}

		protected override void OnInit (System.EventArgs e) 
		{
			base.OnInit (e);
			this.Load += new System.EventHandler (this.Page_Load);
		}

		private void Page_Load (object sender, System.EventArgs e)
		{
			// Add the javascript for deleting assets
			string scriptString = "<script language=\"javaScript\">\n<!--\n function DeleteAsset (id) {\n";
			scriptString += "   document.getElementById(\"AssetId\").value = id;\n";            // save the page number clicked to the hidden field
			scriptString += "   __doPostBack('DeleteAsset','');\n";             // call the __doPostBack function to post back the form and execute the PageClick event
			scriptString += "}\n// -->\n";
			scriptString += "</script>";

            if (!ClientScript.IsClientScriptBlockRegistered(GetType(), "DeleteAsset"))
			{
                ClientScript.RegisterClientScriptBlock(GetType(), "DeleteAsset", scriptString);
			}
		}

		/// <summary>
		/// Return the delete javascript
		/// </summary>
		/// <param name="assetId"></param>
		/// <returns></returns>
		public string GetDeleteScript (int assetId)
		{
			return GetDeleteScript (assetId, false);
		}

		/// <summary>
		/// Return the delete javascript
		/// </summary>
		/// <param name="assetId"></param>
		/// <returns></returns>
		public string GetDeleteScript (int assetId, bool removeOnly)
		{
			if (removeOnly)
			{
				return "javascript:if (confirm(\"Are you sure you want to remove this item from the channel?\")){DeleteAsset (" + assetId + ")};";
			}
			else
			{
				return "javascript:if (confirm(\"Are you sure you want to delete this item?\")){DeleteAsset (" + assetId + ")};";
			}
		}

        /// <summary>
        /// Get the action image
        /// </summary>
        /// <param name="amount"></param>
        /// <returns></returns>
        public string GetActionImage (DataRowView drAsset)
        {
            return GetActionImage (drAsset.Row);
        }

        /// <summary>
        /// Get the action image
        /// </summary>
        /// <param name="amount"></param>
        /// <returns></returns>
        public string GetActionImage (DataRow drAsset)
        {
            //Double dAssetAmount = Convert.ToDouble (drAsset ["amount"]);
            int assetTypeId = Convert.ToInt32 (drAsset ["asset_type_id"]);

            // watch
            if (assetTypeId.Equals ((int) Constants.eASSET_TYPE.VIDEO))
            {
                return ResolveUrl ("~/images/button_watch_secure.gif");
            }

            // play
            if (assetTypeId.Equals ((int) Constants.eASSET_TYPE.GAME))
            {
                return ResolveUrl ("~/images/button_play.gif");
            }

            // Must be a create
            return ResolveUrl ("~/images/button_get_secure.gif");
        }

		protected string getFileExtension (int folder, string extension)
		{
			if (folder.Equals(0))
			{
				return System.IO.Path.GetExtension (extension).ToUpper ();
			}
			return "folder";		
		}

    }
}
