///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for ServerBasePage.
	/// </summary>
	public class ServerBasePage : SortedBasePage
	{
		public ServerBasePage ()
		{
		}

		/// <summary>
		/// Get the current server uptime
		/// </summary>
		/// <param name="dtStartDate"></param>
		/// <returns></returns>
		public string GetDateTimeDifference (object dtStartDate, Int64 elapsedSeconds, int statusId)
		{
			// If status is running and pinging in last 7 minutes
			if (IsOnline (elapsedSeconds, statusId))
			{
				return "<span class=\"adminLinks\">" + KanevaGlobals.GetDateTimeDifference (dtStartDate) + "</span>";
			}
			else
			{
				return "<span class=\"offline\">offline</span>";
			}
		}

		/// <summary>
		/// IsOnline
		/// </summary>
		private bool IsOnline (Int64 elapsedSeconds, int statusId)
		{
			// If status is running and pinging in last 7 minutes
			return (statusId.Equals ((int) Constants.eGAME_SERVER_STATUS.RUNNING) && (elapsedSeconds < (7*60)));
		}

		public string GetPlayLauncher (int assetId, string gameName, string gameExe, string patchServer, string ipAddress, int serverPort)
		{
			return "KEPPatcher.Play(" + assetId + ", '" + KanevaGlobals.CleanJavascript (gameName) + "', '" + KanevaGlobals.CleanJavascript (gameExe) + "', '" + KanevaGlobals.CleanJavascript (patchServer) + "', '" + KanevaGlobals.CleanJavascript (ipAddress) + ":" + serverPort + "' );";
		}

		/// <summary>
		/// DEFAULT_SORT
		/// </summary>
		/// <returns></returns>
		protected override string DEFAULT_SORT
		{
			get
			{
				return "last_ping_datetime";
			}
		}

		/// <summary>
		/// DEFAULT_SORT
		/// </summary>
		/// <returns></returns>
		protected override string DEFAULT_SORT_ORDER
		{
			get
			{
				return "DESC";
			}
		}

	}
}
