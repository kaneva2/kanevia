///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for NoBorderPage.
	/// </summary>
	public class NoLeftNavPage : BasePage
	{
		public NoLeftNavPage()
		{
			m_Form = new HtmlForm ();
			m_Form.ID = "frmMain";
			m_Form.Enctype = "multipart/form-data";

			m_ControlBin = new ControlCollection (this);
		}

		/// <summary>
		/// OnInit
		/// </summary>
		/// <param name="e"></param>
		protected override void OnInit (System.EventArgs e) 
		{
			base.OnInit (e);

			StartHtml ucStartHtml = (StartHtml) LoadControl ("~/usercontrols/StartHtml.ascx");
			ucStartHtml.Title = m_Title;
			this.Controls.AddAt (0, ucStartHtml);

			m_Form.Controls.Add (LoadControl ("~/usercontrols/Header.ascx"));

			// Add the main content
			for(int i = 0; i < m_ControlBin.Count; i++)
			{
				m_Form.Controls.Add (m_ControlBin [i]);
			}

			this.Controls.Add (m_Form);
			this.Controls.Add (LoadControl ("~/usercontrols/EndHtml.ascx"));

			// Add the sorting controls
			btnSort = new Button ();
			hidSortColumn = new HtmlInputHidden ();

			btnSort.Style.Add ("DISPLAY", "none");
			btnSort.Click += new EventHandler(btnSort_Click);

			m_Form.Controls.Add (btnSort);
			m_Form.Controls.Add (hidSortColumn);
		}

		/// <summary>
		/// The page title property
		/// </summary>
        protected new string Title 
		{
			set
			{
				m_Title = value;
			}
			get 
			{
				return m_Title;
			}
		}

		/// <summary>
		/// AddParsedSubObject
		/// </summary>
		/// <param name="obj"></param>
		protected override void AddParsedSubObject (object obj) 
		{			
			// Add the control to form collection
			m_ControlBin.Add ((System.Web.UI.Control) obj);
		}

		/// <summary>
		/// Form Property
		/// </summary>
		public HtmlForm FormMain
		{
			get
			{
				return m_Form;
			}
		}

		/// <summary>
		/// Current sort expression
		/// </summary>
		public string CurrentSort
		{
			get 
			{
				if (ViewState ["cs"] == null)
				{
					return DEFAULT_SORT;
				}
				else
				{
					return ViewState ["cs"].ToString ();
				}
			} 
			set
			{
				ViewState ["cs"] = value;
			}
		}

		/// <summary>
		/// Current sort order
		/// </summary>
		public string CurrentSortOrder
		{
			get 
			{
				if (ViewState ["cso"] == null)
				{
					return DEFAULT_SORT_ORDER;
				}
				else
				{
					return ViewState ["cso"].ToString ();
				}
			} 
			set
			{
				ViewState ["cso"] = value;
			}
		}

		/// <summary>
		/// They clicked to sort a column
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected virtual void btnSort_Click (object sender, System.EventArgs e)
		{
			// Set the sort order
			if (CurrentSortOrder == "DESC")
			{
				CurrentSortOrder = "ASC";
			}
			else if (CurrentSortOrder == "ASC")
			{
				CurrentSortOrder = "DESC";
			}
			else 
			{
				CurrentSortOrder = "ASC";
			}

			// Set the sort column
			if (!CurrentSort.Equals (hidSortColumn.Value))
			{
				// Changing sort expression, so set to ASC
				CurrentSortOrder = "ASC";
				CurrentSort = hidSortColumn.Value;
			}
		}

		/// <summary>
		/// Set Header Sort column Text
		/// </summary>
		protected void SetHeaderSortText (DataGrid dgrdToSort)
		{
			DataGridColumn dgrdColumn;
			string strippedHeader;

			// Which arrow to use?
			string arrowImage = "arrow_sort_up.gif";
			if (CurrentSortOrder.Equals ("DESC"))
			{
				arrowImage = "arrow_sort.gif";
			}

			// Loop through all sortable columns
			for (int i = 0; i < dgrdToSort.Columns.Count; i ++)
			{
				dgrdColumn = dgrdToSort.Columns [i];

				// Is it a sortable column?
				if (dgrdColumn.SortExpression.Length > 0 && dgrdToSort.AllowSorting)
				{
					strippedHeader = dgrdColumn.HeaderText;

					if (dgrdToSort.EnableViewState)
					{
						strippedHeader = strippedHeader.Replace ("<img src=" + ResolveUrl ("~/images/arrow_sort.gif") + " border=0/>", "");
						strippedHeader = strippedHeader.Replace ("<img src=" + ResolveUrl ("~/images/arrow_sort_up.gif") + " border=0/>", "");
					}

					// Is this column the current sorted?
					if (CurrentSort.Equals (dgrdColumn.SortExpression))
					{
						dgrdColumn.HeaderText = "<a href='#' onclick='javascript:document.frmMain." + hidSortColumn.ClientID + ".value=\"" + dgrdColumn.SortExpression + "\";javascript:document.frmMain." + btnSort.ClientID + ".click();'>" + strippedHeader + "<img src=" + ResolveUrl ("~/images/" + arrowImage) + " border=0/></a>";
					}
					else
					{
						dgrdColumn.HeaderText = "<a href='#' onclick='javascript:document.frmMain." + hidSortColumn.ClientID + ".value=\"" + dgrdColumn.SortExpression + "\";javascript:document.frmMain." + btnSort.ClientID + ".click();'>" + strippedHeader + "</a>";
					}
				}
			}
		}

		/// <summary>
		/// DEFAULT_SORT
		/// </summary>
		/// <returns></returns>
		protected virtual string DEFAULT_SORT
		{
			get
			{
				return "a.name";
			}
		}

		/// <summary>
		/// DEFAULT_SORT_ORDER
		/// </summary>
		/// <returns></returns>
		protected virtual string DEFAULT_SORT_ORDER
		{
			get
			{
				return "ASC";
			}
		}

		// Sorting
		protected Button btnSort;
		protected HtmlInputHidden hidSortColumn;

		/// <summary>
		/// Temporary control collection to hold parsed controls.
		/// </summary>
		protected ControlCollection m_ControlBin;

		/// <summary>
		/// Standard form for the page
		/// </summary>
		protected HtmlForm m_Form;

		protected string m_Title = "";

	}
}