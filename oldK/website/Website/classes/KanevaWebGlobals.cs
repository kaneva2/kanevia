///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using System.Reflection;
using System.Resources;
using System.Globalization;
using System.Xml;
using CuteEditor;
using System.Data;
using System.Text.RegularExpressions;
using System.Collections.Specialized;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for KanevaWebGlobals.
	/// </summary>
	public class KanevaWebGlobals
	{
		private static ResourceManager resmgr = new ResourceManager ("KlausEnt.KEP.Kaneva.Kaneva", Assembly.GetExecutingAssembly());

		/// <summary>
		/// KanevaWebGlobals
		/// </summary>
		public KanevaWebGlobals ()
		{
		}

        // ***********************************************
        // Security Functions
        // ***********************************************
        #region Security Functions

        /// <summary>
        /// Return the users privilege settings. All privileges available and their corresponding 
        /// access levels.
        /// </summary>
        /// <returns></returns>
        public static int CheckUserAccess(int privilegeId)
        {
            int accessLevel = (int)SitePrivilege.eACCESS_LEVEL.ACCESS_NONE;
            try
            {
                //users privileges
                NameValueCollection privileges = CurrentUser.SitePrivileges;

                //find their access level for the privilege being checked
                accessLevel = Convert.ToInt32(privileges[privilegeId.ToString()]);
            }
            catch { }

            return accessLevel;
        }

        #endregion

        /// <summary>
        /// GetJoinLink
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string GetJoinLink(Page page)
        {
            return page.ResolveUrl(KanevaGlobals.JoinLocation);
        }



		/// <summary>
		/// GetResourceString
		/// </summary>
		/// <param name="name"></param>
		/// <returns></returns>
		public static string GetResourceString (string name)
		{
			return resmgr.GetString (name, System.Globalization.CultureInfo.CurrentCulture);
		}

		/// <summary>
		/// GetResourceString
		/// </summary>
		public static string GetResourceString (string name, params string [] args)
		{
			string [] newParams = new string [args.Length];
			
			for (int i = 0; i < args.Length; i++)
			{
				newParams [i] = GetResourceString (args [i]);
			}

			return String.Format (GetResourceString (name), newParams);
		}

		/// <summary>
		/// Get the user info from the current logged in user
		/// </summary>
		/// <returns></returns>
		public static User CurrentUser
		{
            get
            {
                User user = (User)HttpContext.Current.Items["User"];
                return user;
            }
		}

		/// <summary>
        /// Get whether the user has a mature ("access") pass
		/// </summary>
		/// <returns></returns>
		public static bool UserHasMaturePass ()
		{
			if (HttpContext.Current.Request.IsAuthenticated)
			{
				User userInfo = (User) HttpContext.Current.Items ["User"];
                return userInfo.HasAccessPass; ;
			}
			else
			{
				return false;
			}
		}	

		/// <summary>
		/// Get the user id from the current logged in user
		/// </summary>
		/// <returns></returns>
		public static int GetUserId ()
		{
			if (HttpContext.Current.Request.IsAuthenticated)
			{
				User userInfo = (User) HttpContext.Current.Items ["User"];
				return userInfo.UserId;
			}
			else
			{
				return 0;
			}
		}	

		/// <summary>
		/// Get the user id from the current logged in user
		/// </summary>
		/// <returns></returns>
		public static string GetUserState (string ustate)
		{
			switch (ustate)
			{
				case Constants.ONLINE_USTATE_OFF:
					return "";
				case Constants.ONLINE_USTATE_ON:		// logged into web site
					return "on-web";
				case Constants.ONLINE_USTATE_INWORLD:	// logged into WOK
					return "in-world";
				case Constants.ONLINE_USTATE_ONINWORLD:	// logged into web site and WOK
					return "in-world";
				default:
					return "";
			}
		}	

		/// <summary>
		/// Is the user in the Virtual World?
		/// </summary>
		/// <param name="ustate"></param>
		/// <returns></returns>
		public static bool IsInWorld (string ustate)
		{
			return (ustate.Equals (Constants.ONLINE_USTATE_INWORLD) || ustate.Equals (Constants.ONLINE_USTATE_ONINWORLD));
		}

		/// <summary>
		/// RemoveScripts
		/// </summary>
		/// <param name="text"></param>
		/// <returns></returns>
		public static bool ContainsInjectScripts (string text)
		{
			bool bInvalid = false;

			try
			{
				XmlDocument doc = EditorUtility.ConvertToXmlDocument (text);
				bInvalid = ContainsScriptsRecursive (doc.DocumentElement);
			}
			catch (Exception) {}

			return bInvalid;
		}

		/// <summary>
		/// RemoveScriptsRecursive
		/// </summary>
		/// <param name="element"></param>
		private static bool ContainsScriptsRecursive (XmlElement element)
		{
			if (ShouldRemove (element))
			{
				return true;
			}
			foreach(XmlElement child in element.SelectNodes("*"))
			{
				return (ContainsScriptsRecursive (child));
			}

			return false;
		}
		
		/// <summary>
		/// GetAssetTypeName
		/// </summary>
		public static string GetAssetTypeName (int assetTypeId)   
		{
			string typeName = "Media";

			switch (assetTypeId)
			{
				case (int) Constants.eASSET_TYPE.GAME:
					typeName = "Game";
					break;

				case (int) Constants.eASSET_TYPE.VIDEO:
					typeName = "Video";
					break;

				case (int) Constants.eASSET_TYPE.MUSIC:
					typeName = "Audio";
					break;

				case (int) Constants.eASSET_TYPE.PICTURE:
					typeName = "Photo";
					break;

				case (int) Constants.eASSET_TYPE.PATTERN:
					typeName = "Pattern";
					break;

				case (int) Constants.eASSET_TYPE.WIDGET:
					typeName = "Widget";
					break;

				case (int) Constants.eASSET_TYPE.TV:
					typeName = "Streaming TV";
					break;
			}

			return typeName;
		}

		/// <summary>
		/// ShouldRemove
		/// </summary>
		/// <param name="element"></param>
		/// <returns></returns>
		private static bool ShouldRemove (XmlElement element)
		{
			string name=element.LocalName.ToLower();
			//check the element name
			switch(name)
			{
				case "link"://maybe link to another css that contains scripts(behavior,expression)
				case "script":
					return true;
			}
			//check the attribute
			foreach(XmlAttribute attr in element.Attributes)
			{
				string attrname=attr.LocalName.ToLower();
				//<img onclick=....
				if(attrname.StartsWith("on"))
					return true;
				string val=attr.Value.ToLower();
				//<a href="javascript:scriptcode()"..
				if(val.IndexOf("script")!=-1)
					return true;
				//<a style="behavior:url(http://another/code.htc)"
				if(attrname=="style")
				{
					if(val.IndexOf("behavior")!=-1)
						return true;
					if(val.IndexOf("expression")!=-1)
						return true;
				}
    
			}
			return false;
        }

        #region Parsing Embed Code

        /// <summary>
        /// GetVideoId - embed code to look for kaneva assetId 
        /// throws an error for the calling function to handle for user message display
        /// </summary>
        /// <param name="fullString"></param>
        /// <param name="strFindString"></param>
        /// <param name="endingString"></param>
        /// <returns>int</returns>
        public static int GetVideoId(string fullString, string strFindString, string endingString)
        {
            string strFindResult = "";
            int iStartIndexOfFindString = fullString.IndexOf(strFindString, 0, StringComparison.CurrentCultureIgnoreCase);

            if (iStartIndexOfFindString > -1)
            {
                int iEndIndexOfFindString = fullString.IndexOf(endingString, iStartIndexOfFindString + strFindString.Length);

                if (iEndIndexOfFindString > -1)
                {
                    strFindResult = fullString.Substring(iStartIndexOfFindString + strFindString.Length, iEndIndexOfFindString - (iStartIndexOfFindString + strFindString.Length));
                }
            }  // end of height resize

            return Convert.ToInt32(strFindResult);
        }

        /// <summary>
        /// ParseYouTubeEmbedCode - parses youtube embed code to return only the movie url
        /// throws an error for the calling function to handle for user message display
        /// </summary>
        /// <param name="flashWidgetEmbedCode"></param>
        /// <returns>string</returns>
        public static string ParseYouTubeEmbedCode(string youTubeEmbedCode)
        {
            string assetId = "";
            string errorMessage = null;

            if ((youTubeEmbedCode.Length > 0) && ((youTubeEmbedCode.ToUpper()).IndexOf("YOUTUBE") >= 0))
            {
                int endIndexOfassetId = -1;
                //check for you tube video marker
                int startIndexOfassetId = youTubeEmbedCode.IndexOf("v/");

                //if marker not found look for URL tag
                if (startIndexOfassetId < 0)
                {
                    startIndexOfassetId = youTubeEmbedCode.IndexOf("v=");
                    //if no URL tag found display error
                    if (startIndexOfassetId < 0)
                    {
                        startIndexOfassetId = youTubeEmbedCode.IndexOf("embed/");

                        if (startIndexOfassetId < 0)
                        {
                            errorMessage = "This is not a valid YouTube embed code.";
                        }
                        else
                        {
                            endIndexOfassetId = youTubeEmbedCode.IndexOf("\"", startIndexOfassetId);
                            //if end index not found display error
                            if (endIndexOfassetId < 0)
                            {
                                errorMessage = "This is not a valid YouTube embed code.";
                            }
                            if (errorMessage == null)
                            {
                                //pull out assetID from embed code
                                assetId = youTubeEmbedCode.Substring(startIndexOfassetId + 6, endIndexOfassetId - (startIndexOfassetId + 6));
                            }
                        }
                    }
                    else
                    {
                        //if URL tag found pull out the assetID
                        assetId = youTubeEmbedCode.Substring(startIndexOfassetId + 2);
                    }


                    
                }
                //if embed tag found pull out assetID
                else
                {
                    //find end of asset id in embed string with end index
                    endIndexOfassetId = youTubeEmbedCode.IndexOf("\"", startIndexOfassetId);
                    //if end index not found display error
                    if (endIndexOfassetId < 0)
                    {
                        errorMessage = "This is not a valid YouTube embed code.";
                    }
                    if (errorMessage == null)
                    {
                        //pull out assetID from embed code
                        assetId = youTubeEmbedCode.Substring(startIndexOfassetId + 2, endIndexOfassetId - (startIndexOfassetId + 2));
                    }
                }

            }
            //no valid embed string provided display error
            else
            {
                errorMessage = "This is a not valid YouTube embed code.";
            }

            if (errorMessage == null)
            {
                //added logic to account for YouTube url string Changes
                //modify as necessary
                int amplocation = assetId.IndexOf("&");
                if (amplocation >= 0)
                {
                    assetId = assetId.Substring(0, amplocation);
                }

                amplocation = assetId.IndexOf("?");
                if (amplocation >= 0)
                {
                    assetId = assetId.Substring(0, amplocation);
                }

                amplocation = assetId.IndexOf("http");
                if (amplocation >= 0)
                {
                    assetId = assetId.Substring(0, amplocation);
                }
            }
            //notify calling function that something went awry
            else
            {
                throw new Exception(errorMessage);
            }

            return assetId;
        }

        /// <summary>
        /// ParseKanevaApprovedEmbedCode - parses embed code to return only KANEVA approved embed/object tags and remove any
        /// extra and potentially harmful code. Currently only supports YouTube and Kaneva processed movies
        /// throws an error for the calling function to handle for user message display
        /// </summary>
        /// <param name="flashWidgetEmbedCode"></param>
        /// <returns>string</returns>
        public static string ParseKanevaApprovedEmbedCode(string providedEmbedCode, string kanevaIdToFind, string kanevaIdEnd)
        {
            string strippedEmbedCode = null;
            int assetId = -1;

            //process to see if it is a youtube movie first
            try
            {
                strippedEmbedCode = ParseYouTubeEmbedCode(providedEmbedCode);
            }
            catch (Exception) 
            {
            }


            //if no Youtube value present check it to see if it is a kaneva movie
            if (strippedEmbedCode == null)
            {
                try
                {
                    assetId = GetVideoId(providedEmbedCode, kanevaIdToFind, kanevaIdEnd);
                }
                catch (FormatException)
                {
                    throw new Exception("This is a not valid embed code.");
                }
            }

            strippedEmbedCode = ParseEmbedCode(providedEmbedCode);


            return strippedEmbedCode;
        }


        /// <summary>
        /// ParseEmbedCode - parses embed code to return only the embed/object tags and remove any
        /// extra and potentially harmful code
        /// throws an error for the calling function to handle for user message display
        /// </summary>
        /// <param name="flashWidgetEmbedCode"></param>
        /// <returns>string</returns>
        public static string ParseEmbedCode(string provideEmbedCode)
        {
            string errorMessage = null;

            //remove all spaces around equal marks and quotes to make processing easier
            provideEmbedCode = provideEmbedCode.Replace(" =", "=");
            provideEmbedCode = provideEmbedCode.Replace("= ", "=");
            //provideEmbedCode = provideEmbedCode.Replace("\"", "");
            provideEmbedCode = provideEmbedCode.Replace("  ", " ");
            provideEmbedCode = provideEmbedCode.Replace("\n", "");
            provideEmbedCode = provideEmbedCode.Replace("\r", "");

            string strippedEmbedCode = provideEmbedCode;
            if (strippedEmbedCode.Length > 0)
            {
                int embedindex = (strippedEmbedCode.ToUpper()).IndexOf("<EMBED");
                int objectindex = (strippedEmbedCode.ToUpper()).IndexOf("<OBJECT");
                if (embedindex >= 0)
                {
                    //strip code down to the embed tag
                    strippedEmbedCode = strippedEmbedCode.Substring(embedindex);

                    //find end of embed tag value always start at beginning of new substring
                    int endEmbedTag = (strippedEmbedCode.ToUpper()).IndexOf("</EMBED>");
                    if (endEmbedTag > 0)
                    {
                        //remove the rest of the code
                        strippedEmbedCode = strippedEmbedCode.Substring(0, endEmbedTag + 8);
                    }
                }
                else if (objectindex >= 0)
                {
                    //strip code down to the object tag
                    strippedEmbedCode = strippedEmbedCode.Substring(objectindex);

                    //find ending object tag
                    int objectendindex = (strippedEmbedCode.ToUpper()).IndexOf("</OBJECT>");
                    if (objectendindex > 0)
                    {
                        //remove the rest of the code
                        strippedEmbedCode = strippedEmbedCode.Substring(0, objectendindex + 9);
                    }
                }
                else
                {
                    errorMessage = "This is not a valid embed code .";
                }
            }
            else
            {
                errorMessage = "This is not a valid embed code .";
            }

            //notify calling function that something went awry
            if (errorMessage != null)
            {
                throw new Exception(errorMessage);
            }

            return strippedEmbedCode;
        }

        /// <summary>
        /// ParseFlashGameEmbedCode - parses flash game embed code to return only the movie url
        /// throws an error for the calling function to handle for user message display
        /// </summary>
        /// <param name="flashWidgetEmbedCode"></param>
        /// <returns>string</returns>
        public static string ParseFlashGameEmbedCode(string flashGameEmbedCode)
        {
            string strippedOutURL = "";
            string errorMessage = null;

            //remove all spaces around equal marks and quotes to make processing easier
            flashGameEmbedCode = flashGameEmbedCode.Replace(" =", "=");
            flashGameEmbedCode = flashGameEmbedCode.Replace("= ", "=");
            flashGameEmbedCode = flashGameEmbedCode.Replace("\"", "");
            flashGameEmbedCode = flashGameEmbedCode.Replace("  ", " ");
            flashGameEmbedCode = flashGameEmbedCode.Replace("\n", "");
            flashGameEmbedCode = flashGameEmbedCode.Replace("\r", "");

            string flashGameLink = flashGameEmbedCode;
            int endingChar = -1;
            string parameters = "?";

            if (flashGameLink.Length > 0)
            {
                int embedindex = (flashGameLink.ToUpper()).IndexOf("<EMBED");
                int objectindex = (flashGameLink.ToUpper()).IndexOf("<OBJECT");
                if (embedindex >= 0)
                {
                    int srcindex = (flashGameLink.ToUpper()).IndexOf("SRC=", embedindex);
                    if (srcindex >= 0)
                    {
                        //getsubtring starting at src=
                        flashGameLink = flashGameLink.Substring(srcindex);

                        //find end of src value; always start at beginning of new substring
                        endingChar = flashGameLink.IndexOf(" ", 0);
                    }

                    //logic to create parameter string for URL
                    //find end of embed tag value always start at beginning of new substring
                    int endEmbedTag = flashGameLink.IndexOf(">", 0);
                    if (endEmbedTag > 0 && endingChar > 0)
                    {
                        //append the "name=" value word as the parameter name
                        parameters += flashGameLink.Substring((endingChar + 1), (endEmbedTag - endingChar));
                        parameters = parameters.Replace(" />", "");
                        parameters = parameters.Replace(" ", "&");
                        parameters = parameters.Replace("&&", "&");
                    }
                }
                else if (objectindex >= 0)
                {
                    int movieindex = (flashGameLink.ToUpper()).IndexOf("NAME=MOVIE");
                    if (movieindex >= 0)
                    {
                        //find the value for the movie param
                        movieindex = (flashGameLink.ToUpper()).IndexOf("VALUE=", movieindex);

                        if (movieindex >= 0)
                        {
                            //get substring starting at value=
                            flashGameLink = flashGameLink.Substring(movieindex);

                            //find end of src value always start at beginning of new substring
                            endingChar = (flashGameLink.IndexOf(">", 0)) + 1;

                            //logic to create parameter string for URL
                            int paramIndex = 0;
                            int valueIndex = 0;
                            int endParaTag = 0;
                            int startindex = movieindex;
                            while (paramIndex >= 0)
                            {
                                //try and find the next parameter tag
                                paramIndex = (flashGameEmbedCode.ToUpper()).IndexOf("PARAM NAME=", startindex);
                                if (paramIndex >= 0)
                                {
                                    //advance index to end of position beyond PARAM NAME=
                                    paramIndex += 11;

                                    //find the value tag
                                    valueIndex = (flashGameEmbedCode.ToUpper()).IndexOf("VALUE=", paramIndex);

                                    if (valueIndex > 0)
                                    {
                                        //if more than one parameter in URL append the ampersand
                                        if (parameters.Length > 2)
                                        {
                                            parameters += "&";
                                        }
                                        //append the "name=" value word as the parameter name (subtract the extra one from valueIndex
                                        //to account for space at end of parameter name
                                        parameters += flashGameEmbedCode.Substring(paramIndex, (valueIndex - paramIndex));

                                        //advance index to end beyond value=
                                        valueIndex += 6;

                                        //find end of parameter tag
                                        endParaTag = ((flashGameEmbedCode.ToUpper()).IndexOf(">", valueIndex)) + 1;

                                        if (endParaTag > 0)
                                        {
                                            //append the "name=" value word as the parameter name
                                            parameters += "=" + flashGameEmbedCode.Substring(valueIndex, (endParaTag - valueIndex));
                                            //advance paraindex to end of current tag to continue search
                                            startindex = endParaTag;
                                        }
                                        //if no end para teg found tags are malformed exit out of loop
                                        else
                                        {
                                            paramIndex = -1;
                                        }

                                    }
                                    //if no value index found tags are malformed exit out of loop
                                    else
                                    {
                                        paramIndex = -1;
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    errorMessage = "This is not a valid embed code .";
                }

                //temporary logic to check for a full url with an swf extention
                int httpindex = (flashGameLink.ToUpper()).IndexOf("HTTP://");
                int swfIndex = (flashGameLink.ToUpper()).IndexOf(".SWF");
                if ((httpindex >= 0) && (swfIndex >= 0) && (httpindex < swfIndex))
                {
                    //save widget link
                    if (endingChar < httpindex)
                    {
                        endingChar = flashGameLink.Length;
                    }

                    strippedOutURL = flashGameLink.Substring(httpindex, (endingChar - httpindex));

                    if (parameters.Length > 1)
                    {
                        //extra logic to remove "/" and "." in data - it cause issues on media detials content extension
                        //this will create invalid links for pluginspace and application type but shouldnt affect widget use
                        parameters = parameters.Replace("/", "");
                        parameters = parameters.Replace(".", "");
                        parameters = parameters.Replace(":", "");

                        strippedOutURL = strippedOutURL + parameters;
                    }

                    //extra processing to remove any stray ending tags
                    strippedOutURL = strippedOutURL.Replace(" ", "");
                    strippedOutURL = strippedOutURL.Replace("/>", "");
                    strippedOutURL = strippedOutURL.Replace(">", "");
                }
                else
                {
                    errorMessage = "This is not a valid link.";
                }
            }
            //no valid url string provided display error
            else
            {
                errorMessage = "This is not a valid link.";
            }

            //notify calling function that something went awry
            if (errorMessage != null)
            {
                throw new Exception(errorMessage);
            }

            return strippedOutURL;
        }

        /// <summary>
        /// ParseTVEmbedCode - parses tv embed code to return only the movie url
        /// throws an error for the calling function to handle for user message display
        /// </summary>
        /// <param name="flashWidgetEmbedCode"></param>
        /// <returns>string</returns>
        public static string ParseTVEmbedCode(string streamnTVLink)
        {
            string strippedOutURL = "";
            string errorMessage = null;
            
            if (streamnTVLink.Length > 0)
            {
                // Per Jeff P, redo livestream to hard coded embed and add autoplay...
                if (streamnTVLink.ToLower().Contains("cdn.livestream.com"))
                {
                    string liveStreamEmbed = "";

                    //<object width="560" height="340" id="lsplayer" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000">
                    //<embed name="lsplayer" wmode="transparent" src="http://cdn.livestream.com/grid/LSPlayer.swf?channel=discgolfplanet&autoPlay=true">
                    //</embed></object>

                    int LiveStreamhttpindex = (streamnTVLink.ToUpper()).IndexOf("HTTP://");
                    if (LiveStreamhttpindex > 0)
                    {
                        //extra check to make sure parameters are gathered by finding end of string by "
                        int LiveStreamEndingChar = streamnTVLink.IndexOf("\"", LiveStreamhttpindex);
                        //for case where there are no quotes just the straight URL and parameters
                        if (LiveStreamEndingChar > 0)
                        {
                            liveStreamEmbed = streamnTVLink.Substring(LiveStreamhttpindex, (LiveStreamEndingChar - LiveStreamhttpindex));

                            liveStreamEmbed = liveStreamEmbed.Replace("autoPlay=false", "autoPlay=true");

                            liveStreamEmbed =   "<object width=\"560\" height=\"340\" id=\"lsplayer\" classid=\"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000\">" +
                                "<embed name=\"lsplayer\" wmode=\"transparent\" src=\"" + liveStreamEmbed + "\">" +
                                 "</embed></object>";

                            return liveStreamEmbed;
                        }
                    }

                  
                }




                int embedindex = (streamnTVLink.ToUpper()).IndexOf("<EMBED");
                int objectindex = (streamnTVLink.ToUpper()).IndexOf("<OBJECT");
                if (embedindex >= 0)
                {
                    int srcindex = (streamnTVLink.ToUpper()).IndexOf("SRC=", embedindex);
                    if (srcindex >= 0)
                    {
                        streamnTVLink = streamnTVLink.Substring(srcindex);
                    }
                }
                else if (objectindex >= 0)
                {
                    int movieindex = (streamnTVLink.ToUpper()).IndexOf("NAME=\"MOVIE\"");
                    if (movieindex >= 0)
                    {
                        streamnTVLink = streamnTVLink.Substring(movieindex);
                    }
                }
                else
                {
                    errorMessage = "This is not a valid embed code .";
                }

                if (errorMessage == null)
                {
                    //temporary logic to check for a full url with an asx extention
                    int httpindex = (streamnTVLink.ToUpper()).IndexOf("HTTP://");
                    int asxIndex = (streamnTVLink.ToUpper()).IndexOf(".ASX");
                    int mmsIndex = (streamnTVLink.ToUpper()).IndexOf("MMS");
                    if ((httpindex >= 0) && (asxIndex >= 0) && (httpindex < asxIndex))
                    {
                        //extra check to make sure parameters are gathered by finding end of string by "
                        int endingChar = streamnTVLink.IndexOf("\"", httpindex);
                        //for case where there are no quotes just the straight URL and parameters
                        if (endingChar < 0)
                        {
                            endingChar = streamnTVLink.Length;
                        }

                        strippedOutURL = streamnTVLink.Substring(httpindex, (endingChar - httpindex));
                    }
                    else if (mmsIndex >= 0)
                    {
                        int endingChar = streamnTVLink.IndexOf("\"", mmsIndex);
                        //for case where there are no quotes just the straight URL and parameters
                        if (endingChar < 0)
                        {
                            endingChar = streamnTVLink.Length;
                        }
                        //save streaming tv link
                        strippedOutURL = streamnTVLink.Substring(mmsIndex, (endingChar - mmsIndex));
                    }
                    else
                    {
                        errorMessage = "This is not a valid streaming TV link.";
                    }
                }
            }
            //no valid url string provided display error
            else
            {
                errorMessage = "This is not a valid streaming TV link.";
            }
            //notify calling function that something went awry
            if (errorMessage != null)
            {
                throw new Exception(errorMessage);
            }

            return strippedOutURL;
        }

        /// <summary>
        /// ParseWidgetEmbedCode - parses flash embed code to return only the movie url
        /// throws an error for the calling function to handle for user message display
        /// </summary>
        /// <param name="flashWidgetEmbedCode"></param>
        /// <returns>string</returns>
        public static string ParseWidgetEmbedCode(string flashWidgetEmbedCode)
        {
            string strippedOutURL = "";
            string errorMessage = null;

            //remove all spaces around equal marks and quotes to make processing easier
            flashWidgetEmbedCode = flashWidgetEmbedCode.Replace(" =", "=");
            flashWidgetEmbedCode = flashWidgetEmbedCode.Replace("= ", "=");
            flashWidgetEmbedCode = flashWidgetEmbedCode.Replace("\"", "");
            flashWidgetEmbedCode = flashWidgetEmbedCode.Replace("  ", " ");
            flashWidgetEmbedCode = flashWidgetEmbedCode.Replace("\n", "");
            flashWidgetEmbedCode = flashWidgetEmbedCode.Replace("\r", "");

            string flashWidgetLink = flashWidgetEmbedCode;
            int endingChar = -1;
            string parameters = "?";

            if (flashWidgetLink.Length > 0)
            {
                int embedindex = (flashWidgetLink.ToUpper()).IndexOf("<EMBED");
                int objectindex = (flashWidgetLink.ToUpper()).IndexOf("<OBJECT");
                if (embedindex >= 0)
                {
                    int srcindex = (flashWidgetLink.ToUpper()).IndexOf("SRC=", embedindex);
                    if (srcindex >= 0)
                    {
                        //getsubtring starting at src=
                        flashWidgetLink = flashWidgetLink.Substring(srcindex);

                        //find end of src value; always start at beginning of new substring
                        endingChar = flashWidgetLink.IndexOf(" ", 0);
                    }

                    //logic to create parameter string for URL
                    //find end of embed tag value always start at beginning of new substring
                    int endEmbedTag = flashWidgetLink.IndexOf(">", 0);
                    if (endEmbedTag > 0 && endingChar > 0)
                    {
                        //append the "name=" value word as the parameter name
                        parameters += flashWidgetLink.Substring((endingChar + 1), (endEmbedTag - endingChar));
                        parameters = parameters.Replace(" />", "");
                        parameters = parameters.Replace(" ", "&");
                        parameters = parameters.Replace("&&", "&");
                    }

                }
                else if (objectindex >= 0)
                {
                    int movieindex = (flashWidgetLink.ToUpper()).IndexOf("NAME=MOVIE");
                    if (movieindex >= 0)
                    {
                        //find the value for the movie param
                        movieindex = (flashWidgetLink.ToUpper()).IndexOf("VALUE=", movieindex);

                        if (movieindex >= 0)
                        {
                            //get substring starting at value=
                            flashWidgetLink = flashWidgetLink.Substring(movieindex);

                            //find end of src value always start at beginning of new substring
                            endingChar = (flashWidgetLink.IndexOf(">", 0)) + 1;

                            //logic to create parameter string for URL
                            int paramIndex = 0;
                            int valueIndex = 0;
                            int endParaTag = 0;
                            int startindex = movieindex;
                            while (paramIndex >= 0)
                            {
                                //try and find the next parameter tag
                                paramIndex = (flashWidgetEmbedCode.ToUpper()).IndexOf("PARAM NAME=", startindex);
                                if (paramIndex >= 0)
                                {
                                    //advance index to end of position beyond PARAM NAME=
                                    paramIndex += 11;

                                    //find the value tag
                                    valueIndex = (flashWidgetEmbedCode.ToUpper()).IndexOf("VALUE=", paramIndex);

                                    if (valueIndex > 0)
                                    {
                                        //if more than one parameter in URL append the ampersand
                                        if (parameters.Length > 2)
                                        {
                                            parameters += "&";
                                        }
                                        //append the "name=" value word as the parameter name (subtract the extra one from valueIndex
                                        //to account for space at end of parameter name
                                        parameters += flashWidgetEmbedCode.Substring(paramIndex, (valueIndex - paramIndex));

                                        //advance index to end beyond value=
                                        valueIndex += 6;

                                        //find end of parameter tag
                                        endParaTag = ((flashWidgetEmbedCode.ToUpper()).IndexOf(">", valueIndex)) + 1;

                                        if (endParaTag > 0)
                                        {
                                            //append the "name=" value word as the parameter name
                                            parameters += "=" + flashWidgetEmbedCode.Substring(valueIndex, (endParaTag - valueIndex));
                                            //advance paraindex to end of current tag to continue search
                                            startindex = endParaTag;
                                        }
                                        //if no end para teg found tags are malformed exit out of loop
                                        else
                                        {
                                            paramIndex = -1;
                                        }

                                    }
                                    //if no value index found tags are malformed exit out of loop
                                    else
                                    {
                                        paramIndex = -1;
                                    }

                                }
                            }
                        }
                    }

                }
                else
                {
                    errorMessage = "This is not a valid embed code .";
                }

                if (errorMessage == null)
                {
                    //temporary logic to check for a full url with an swf extention
                    int httpindex = (flashWidgetLink.ToUpper()).IndexOf("HTTP://");
                    int swfIndex = (flashWidgetLink.ToUpper()).IndexOf(".SWF");
                    if ((httpindex >= 0) && (swfIndex >= 0) && (httpindex < swfIndex))
                    {
                        //save widget link
                        if (endingChar < httpindex)
                        {
                            endingChar = flashWidgetLink.Length;
                        }

                        strippedOutURL = flashWidgetLink.Substring(httpindex, (endingChar - httpindex));

                        if (parameters.Length > 1)
                        {
                            //extra logic to remove "/" and "." in data - it cause issues on media detials content extension
                            //this will create invalid links for pluginspace and application type but shouldnt affect widget use
                            parameters = parameters.Replace("/", "");
                            parameters = parameters.Replace(".", "");
                            parameters = parameters.Replace(":", "");

                            strippedOutURL = strippedOutURL + parameters;
                        }

                        //extra processing to remove any stray ending tags
                        strippedOutURL = strippedOutURL.Replace(" ", "");
                        strippedOutURL = strippedOutURL.Replace("/>", "");
                        strippedOutURL = strippedOutURL.Replace(">", "");
                    }
                    else
                    {
                        errorMessage = "This is not a valid link.";
                    }
                }
            }
            //no valid url string provided display error
            else
            {
                errorMessage = "This is not a valid link.";
            }

            //notify calling function that something went awry
            if (errorMessage != null)
            {
                throw new Exception(errorMessage);
            }

            return strippedOutURL;
        }

        #endregion

        public static bool isTextRestricted (string text, Constants.eRESTRICTION_TYPE restriction_type)
        {
            return isTextRestricted (text, restriction_type, false);
        }   

        /// <summary>
        /// isTextRestricted
        /// </summary>
        /// <param name="text"></param>
        /// <param name="restriction_type"></param>
        /// <param name="replace"></param>
        /// <returns>boolean</returns>
        public static bool isTextRestricted (string text, Constants.eRESTRICTION_TYPE restriction_type, bool replace)
        {
            try
            {
                string pattern = "";
                text = text.ToUpper ();
                Regex regx = null; 

                // Only check potty mouth words if potty mouth or all type is requested
                if (restriction_type == Constants.eRESTRICTION_TYPE.POTTY_MOUTH || 
                    restriction_type == Constants.eRESTRICTION_TYPE.ALL)
                {
                    // Add potty mouth exact match word list to the REGEX expression
                    if (WebCache.GetRestrictedWordsList (Constants.eRESTRICTION_TYPE.POTTY_MOUTH, Constants.eMATCH_TYPE.EXACT).Length > 0)
                    {
                        pattern = @"\b(" + WebCache.GetRestrictedWordsList (Constants.eRESTRICTION_TYPE.POTTY_MOUTH, Constants.eMATCH_TYPE.EXACT) + ")" + @"\b";
                    }

                    // If there are potty mouth match anywhere words, add them to the REGEX expression
                    if (WebCache.GetRestrictedWordsList (Constants.eRESTRICTION_TYPE.POTTY_MOUTH, Constants.eMATCH_TYPE.ANYWHERE).Length > 0)
                    {
                        if (pattern.Length > 0) pattern += "|";
                        pattern += "(" + WebCache.GetRestrictedWordsList (Constants.eRESTRICTION_TYPE.POTTY_MOUTH, Constants.eMATCH_TYPE.ANYWHERE) + ")";
                    }
                    
                    // create the Regex obj
                    regx = new Regex (pattern);

                    // If we have a match, return true.  No need to continue.
                    if (regx.IsMatch (text, 0))
                    {                                                                        
                        return true;                                                         
                    }
                }


                // Only check reserved words if reserved or all type is requested              
                if (restriction_type == Constants.eRESTRICTION_TYPE.RESERVED || 
                    restriction_type == Constants.eRESTRICTION_TYPE.ALL)
                {
                    // Add reserved exact match word list to the REGEX expression
                    if (WebCache.GetRestrictedWordsList (Constants.eRESTRICTION_TYPE.RESERVED, Constants.eMATCH_TYPE.EXACT).Length > 0)
                    {
                        pattern = @"\b(" + WebCache.GetRestrictedWordsList (Constants.eRESTRICTION_TYPE.RESERVED, Constants.eMATCH_TYPE.EXACT) + ")" + @"\b";
                    }

                    // If there are reserved match anywhere words, add them to the REGEX expression
                    if (WebCache.GetRestrictedWordsList (Constants.eRESTRICTION_TYPE.RESERVED, Constants.eMATCH_TYPE.ANYWHERE).Length > 0)
                    {
                        if (pattern.Length > 0) pattern += "|";
                        pattern += "(" + WebCache.GetRestrictedWordsList (Constants.eRESTRICTION_TYPE.RESERVED, Constants.eMATCH_TYPE.ANYWHERE) + ")";
                    }

                    // create the Regex obj
                    regx = new Regex (pattern);

                    // return result of match
                    return regx.IsMatch (text, 0);
                }
            }
            catch (Exception)
            {

            }
            return false;
        }

        public static string GetBannerCookieName ()
        {
            try
            {
                if (System.Configuration.ConfigurationManager.AppSettings["Banner_CookieName"] != null &&
                    System.Configuration.ConfigurationManager.AppSettings["Banner_CookieName"].ToString ().Trim ().Length > 0)
                {
                    return System.Configuration.ConfigurationManager.AppSettings["Banner_CookieName"].ToString ();
                }
            }
            catch { }

            return "";
        }
    }
}
