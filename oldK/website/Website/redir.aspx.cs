///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva.Mailer;
using log4net;


namespace KlausEnt.KEP.Kaneva
{
  public partial class redir : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      string newLocation = KanevaGlobals.SiteName;
      
      //handle a url parameter
      if (Request["u"] != null)
      {
        try
        {                    
          string encUrl = KanevaGlobals.Decrypt(Request["u"].ToString());          
          newLocation = encUrl;                                                  
        }
        catch
        {
        }
      }

      if (Request["t"] == "1")
      {
        Response.Write(Request["u"].ToString() + "<br />");
        Response.Write(KanevaGlobals.Decrypt(Request["u"].ToString()) + "<br />");
        //Response.Write(KanevaGlobals.Decrypt(Server.UrlDecode( Request["u"].ToString())) + "<br />");     
        Response.Write(newLocation + "<br />"); //test mode
      }
      else
      {
        redirectPerm(newLocation);
      }

      if (Request["e"] != null)
      {
        try
        {
          //Log this link click

          int eid = Convert.ToInt32(Request["e"]);          

          Logger.LogEmailClick(eid);          
          Logger.LogEmailOpen(eid);

          recordClick(eid, newLocation);
        }
        catch (Exception exc)
        {
          m_logger.Error("Error Recording Click: ", exc);
        }
      }
      
    }

    protected void recordClick(int eid, string newLocation)
    {
      int user_id = KanevaWebGlobals.GetUserId();         
      
      if (Request.QueryString["uid"] != null &&
              Request.QueryString["uid"] != "" &&
              Convert.ToInt32 (Request.QueryString["uid"]) > 0)
      {
       user_id = Convert.ToInt32(Request.QueryString["uid"]);
      }
                  
      string ipaddress = Request.ServerVariables["REMOTE_ADDR"];
      string useragent = Request.ServerVariables["HTTP_USER_AGENT"];
    
      MailUtility.recordClick(user_id, eid, newLocation, ipaddress, useragent);
      
    }

    protected void redirectPerm(string newLocation)
    {
      // wil redirect to the decrypted or default url
      Response.Status = "301 Moved Permanently";
      Response.AddHeader("Location", "http://" + newLocation);
    }


    // Logger
    private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

  }
}
