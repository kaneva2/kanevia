///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for userPicker.
	/// </summary>
	public class userPicker : System.Web.UI.Page
	{
		private void Page_Load(object sender, System.EventArgs e)
		{

			if (!IsPostBack)
			{
				BindData (1, "");
			}
		}

		private void pg_PageChange (object sender, PageChangeEventArgs e)
		{
			BindData (e.PageNumber, filMembers.CurrentFilter);
		}

		private void BindData (int pageNumber, string filter)
		{
			// Set the sortable columns
			SetHeaderSortText (dgrdAsset);
			string orderby = CurrentSort + " " + CurrentSortOrder;

			PagedDataTable pds = UsersUtility.GetUsers ((int) Constants.eUSER_STATUS.REGVALIDATED, true, filter, orderby, pageNumber, filMembers.ItemsPerPages);
			pgTop.NumberOfPages = Math.Ceiling ((double) pds.TotalCount / filMembers.ItemsPerPages).ToString ();

			dgrdAsset.DataSource = pds;
			dgrdAsset.DataBind ();

			// The results
			lblSearch.Text = KanevaGlobals.GetResultsText (pds.TotalCount, pageNumber, filMembers.ItemsPerPages, pds.Rows.Count);
		}

		/// <summary>
		/// filMembers_FilterChanged
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void filMembers_FilterChanged (object sender, FilterChangedEventArgs e)
		{
			pgTop.CurrentPageNumber = 1;
			BindData (1, e.Filter);
		}

		public string TruncateWithEllipsis (string text, int length)
		{
			return KanevaGlobals.TruncateWithEllipsis (text, length);
		}

		/// <summary>
		/// Get the javacript to select a user
		/// </summary>
		/// <returns></returns>
		protected string GetSelectJavascript (string username)
		{
			return "javascript:window.opener.document.frmMain.txtUsername.value='" + KanevaGlobals.CleanJavascript (username) + "';window.close();";
		}

		/// <summary>
		/// Current sort expression
		/// </summary>
		public string CurrentSort
		{
			get 
			{
				if (ViewState ["cs"] == null)
				{
					return DEFAULT_SORT;
				}
				else
				{
					return ViewState ["cs"].ToString ();
				}
			} 
			set
			{
				ViewState ["cs"] = value;
			}
		}

		/// <summary>
		/// Current sort order
		/// </summary>
		public string CurrentSortOrder
		{
			get 
			{
				if (ViewState ["cso"] == null)
				{
					return DEFAULT_SORT_ORDER;
				}
				else
				{
					return ViewState ["cso"].ToString ();
				}
			} 
			set
			{
				ViewState ["cso"] = value;
			}
		}

		/// <summary>
		/// They clicked to sort a column
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected virtual void btnSort_Click (object sender, System.EventArgs e)
		{
			// Set the sort order
			if (CurrentSortOrder == "DESC")
			{
				CurrentSortOrder = "ASC";
			}
			else if (CurrentSortOrder == "ASC")
			{
				CurrentSortOrder = "DESC";
			}
			else 
			{
				CurrentSortOrder = "ASC";
			}

			// Set the sort column
			if (!CurrentSort.Equals (hidSortColumn.Value))
			{
				// Changing sort expression, so set to ASC
				CurrentSortOrder = "ASC";
				CurrentSort = hidSortColumn.Value;
			}

			BindData (pgTop.CurrentPageNumber, filMembers.CurrentFilter);
		}

		/// <summary>
		/// Set Header Sort column Text
		/// </summary>
		protected void SetHeaderSortText (DataGrid dgrdToSort)
		{
			DataGridColumn dgrdColumn;

			// Which arrow to use?
			string arrowImage = "arrow_sort_up.gif";
			if (CurrentSortOrder.Equals ("DESC"))
			{
				arrowImage = "arrow_sort.gif";
			}

			// Loop through all sortable columns
			for (int i = 0; i < dgrdToSort.Columns.Count; i ++)
			{
				dgrdColumn = dgrdToSort.Columns [i];

				// Is it a sortable column?
				if (dgrdColumn.SortExpression.Length > 0)
				{
					// Is this column the current sorted?
					if (CurrentSort.Equals (dgrdColumn.SortExpression))
					{
						dgrdColumn.HeaderText = "<a href='#' onclick='javascript:document.frmMain." + hidSortColumn.ClientID + ".value=\"" + dgrdColumn.SortExpression + "\";javascript:document.frmMain." + btnSort.ClientID + ".click();'>" + dgrdColumn.HeaderText + "<img src=" + ResolveUrl ("~/images/" + arrowImage) + " border=0/></a>";
					}
					else
					{
						dgrdColumn.HeaderText = "<a href='#' onclick='javascript:document.frmMain." + hidSortColumn.ClientID + ".value=\"" + dgrdColumn.SortExpression + "\";javascript:document.frmMain." + btnSort.ClientID + ".click();'>" + dgrdColumn.HeaderText + "</a>";
					}
				}
			}
		}

		/// <summary>
		/// DEFAULT_SORT
		/// </summary>
		/// <returns></returns>
		protected virtual string DEFAULT_SORT
		{
			get
			{
				return "username";
			}
		}

		/// <summary>
		/// DEFAULT_SORT_ORDER
		/// </summary>
		/// <returns></returns>
		protected virtual string DEFAULT_SORT_ORDER
		{
			get
			{
				return "ASC";
			}
		}

		// Sorting
		protected Button btnSort;
		protected HtmlInputHidden hidSortColumn;

		protected Kaneva.Pager pgTop;

		protected DataGrid dgrdAsset;

		protected HtmlInputHidden AssetId;

		protected Label lblSearch;

		protected Kaneva.MembersFilter filMembers;

		#region Web Form Designer generated code

		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
			pgTop.PageChanged +=new PageChangeEventHandler (pg_PageChange);
			filMembers.FilterChanged +=new FilterChangedEventHandler(filMembers_FilterChanged);
		}
		#endregion
	}
}
