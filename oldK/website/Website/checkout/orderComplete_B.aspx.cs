///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using log4net;
using Kaneva.BusinessLayer.Facade;


namespace KlausEnt.KEP.Kaneva
{
    public partial class orderComplete_B : MainTemplatePage
    {
        protected orderComplete_B () 
		{
			Title = "Order Complete";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
            if (!KanevaGlobals.EnableCheckout_B)
            {
                if (!IsAdministrator ())
                {
                    Response.Redirect("~/error.aspx?error=" + (int)Constants.eERROR_TYPE.CHECKOUT_CLOSED);
                }
            }

            VerifyUserAccess();
            VerifyOrder();

            int orderId = 0;
            if (Request ["orderId"] != null)
            {
                orderId = Convert.ToInt32 (Request ["orderId"]);
            }

            if (orderId > 0)
            {
                DataRow drOrder = StoreUtility.GetOrder (orderId);

                if (drOrder ["point_transaction_id"] != DBNull.Value)
                {
                    DataRow drPurchasePoints = StoreUtility.GetPurchasePointTransaction (Convert.ToInt32 (drOrder ["point_transaction_id"]));

                    lblOrderNumber.Text = drPurchasePoints ["order_id"].ToString ();
                    lblDescription.Text = drPurchasePoints ["description"].ToString ();
                    lblAmountPaid.Text = FormatCurrency (drPurchasePoints ["amount_debited"]);
                    lblKPoints.Text = FormatKpoints (drPurchasePoints ["totalPoints"]);
                    lblDate.Text = FormatDateTime (drPurchasePoints ["transaction_date"]);

                    if (Convert.ToInt32(drPurchasePoints["payment_method_id"]) == (int) Constants.ePAYMENT_METHODS.PAYPAL)            
                    {                        
                        trBillAddress.Visible = false;

                        lblName.Text = "Paypal";
                    }
                    else    // credit card
                    {
                        DataRow drBillingInfo = StoreUtility.GetOrderBillingInfo (Convert.ToInt32 (drPurchasePoints["order_billing_information_id"]));

                        lblName.Text = drBillingInfo["name_on_card"].ToString ();
                        lblCardNumber.Text = "xxxx-xxxx-xxxx-" + KanevaGlobals.Decrypt (drBillingInfo["card_number"].ToString ());
                        lblCardType.Text = drBillingInfo["card_type"].ToString ();

                        DataRow drAddress = UsersUtility.GetAddress (GetUserId (), Convert.ToInt32 (drPurchasePoints["address_id"]));

                        lblAddress1.Text = drAddress["address1"].ToString ();
                        lblAddress2.Text = drAddress["address2"].ToString ();
                        lblCity.Text = drAddress["city"].ToString ();
                        lblState.Text = drAddress["state_code"].ToString ();
                        lblZip.Text = drAddress["zip_code"].ToString ();
                        lblCountry.Text = drAddress["country_name"].ToString ();
                    }
                }

				// Set Nav
                HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;

                aShop.HRef = "http://" + KanevaGlobals.ShoppingSiteName;

                aGoInWorld.HRef = KanevaSetupInstallURL();
            }
		}

		
		#region Helper Methods

        /// <summary>
        /// Verify user has access to view page
        /// </summary>
        private void VerifyUserAccess ()
		{
			// They must be logged in
			if (!Request.IsAuthenticated)
			{
				Response.Redirect (GetLoginURL ());
			}
		}

        /// <summary>
        /// Verify user is owner of order
        /// </summary>
        private void VerifyOrder ()
		{
			if (Request ["orderId"] == null)
			{
				Response.Redirect ( "~/mykaneva/managecredits.aspx" );
			}

			try
			{
				int orderId = Convert.ToInt32 (Request ["orderId"]);			
				VerifyOrder (StoreUtility.GetOrder (orderId, KanevaWebGlobals.CurrentUser.UserId));
			}
			catch (Exception ex)
			{
				string x = ex.Message;
				Response.Redirect ( "~/mykaneva/managecredits.aspx" );
			}
		}
		private void VerifyOrder (DataRow drOrder)
		{
			try				
			{
				// Verify user is owner of this order		  
				if (drOrder == null)
				{
					// User may be trying to view someone else's order
					m_logger.Warn ("User " + KanevaWebGlobals.CurrentUser.UserId + " tried to view orderId " + Request ["orderId"].ToString () + " from IP " + Common.GetVisitorIPAddress());
					Response.Redirect ( "~/mykaneva/managecredits.aspx" );
				}

				if (Convert.ToInt32(drOrder["transaction_status_id"]) != (int) Constants.eORDER_STATUS.COMPLETED) 
                {
                    // paypal orders may not have been updated in our db from paypal yet.  if not the order will be
                    // in a pending state until we receive the info back from the paypal site.
                    if (Convert.ToInt32 (drOrder["transaction_status_id"]) != (int) Constants.eORDER_STATUS.PENDING_PURCHASE &&
                        StoreUtility.GetOrderPaymentMethod (Convert.ToInt32(drOrder["order_id"]), Convert.ToInt32 (drOrder["point_transaction_id"])) != (int) Constants.ePAYMENT_METHODS.PAYPAL)
                    {
                        m_logger.Warn ("User " + KanevaWebGlobals.CurrentUser.UserId + " tried to view orderId " + Request["orderId"].ToString () + " from IP " + Common.GetVisitorIPAddress() + ".  Order does not have a status of COMPLETED.");
                        Response.Redirect ("~/mykaneva/managecredits.aspx");
                    }
				}
			}
			catch
			{
				Response.Redirect ( "~/mykaneva/managecredits.aspx" );
			}
		}
		
		#endregion
		

		#region Declerations

		// Logger
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);
       

		#endregion Declerations
    }
}
