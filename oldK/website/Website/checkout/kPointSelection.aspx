<%@ Page language="c#" Codebehind="kPointSelection.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.kPointSelection" %>
<%@ Register TagPrefix="Kaneva" Namespace="KlausEnt.KEP.Kaneva.WebControls" Assembly="Kaneva.WebControls" %>
<%@ Register TagPrefix="Kaneva" TagName="HeaderText" Src="../usercontrols/HeaderText.ascx" %>

<Kaneva:HeaderText runat="server" Text="Kaneva Checkout" />

<center>
<br>
<table runat="server" cellpadding="0" cellspacing="0" border="0" width="730">
	<tr><td>
		<asp:ValidationSummary ShowMessageBox="False" ShowSummary="True" Class="formError" id="valSum" DisplayMode="BulletList" runat="server" HeaderText="Please correct the following errors:"/>
	</td></tr>
</table>
	
<div ID="divLoading">
	<table BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="100%" HEIGHT="300px">
		<tr><th CLASS="loadingText" ID="divLoadingText">Loading...</th></tr>
	</table>
</div>

<div id="divData" style="DISPLAY:none">
<center>

<table runat="server" cellpadding="0" cellspacing="0" border="0" width="730">
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="1" height="200"/></td>
		<td height="200" class="dateStamp" id="tdLoginBG" align="left" width="730" valign="top"><br>
			<span class="orangeHeader2"><asp:Label runat="server" id="lblHeading"/></span><br><br>

<asp:Panel ID="pnlNotRegistered" Visible="False" runat="server">
	<table runat="server" cellpadding="0" cellspacing="0" border="0" width="730">
		<tr id="trError" runat="server">
				<td><img runat="server" src="~/images/spacer.gif" width="20" height="1"/></td>
				<td class="messageBox">
					<BR/>			
					NOTICE: For security reasons, purchasing requires your current email address '<B><asp:Label runat="server" id="lblEmail" CssClass="datestamp"/></B>' to be validated. Please send a validation email by clicking
					the 'Send Validation' button. Kaneva will send you an email to verify your email address. After you have followed the instructions in the email, please press 'Continue'.
					<BR><BR/><BR/>
					<center><asp:button id="btnRegEmail" align="right" runat="Server" onClick="btnRegEmail_Click" class="Filter2" Text=" Send Validation " CausesValidation="False"/>
						&nbsp;&nbsp;<asp:button id="btnContinue" align="right" runat="Server" class="Filter2" Text="    Continue    " CausesValidation="False"/>
					</center>
					
					<BR><BR/><BR/><span class="dateStamp">
					Kaneva sends email from kaneva@kaneva.com. If you are using an 
					antispam filter, please check your junk mail folder or add this address so you 
					will receive the email.<BR><BR><i>If you do not receive an email from Kaneva within 
					4 hours, please contact <a runat="server" href="~/suggestions.aspx?mode=F&category=Feedback" class="showingCount" ID="A1">support</a>.</i></span>
					<BR/><BR/>
				</td>
				<td><img runat="server" src="~/images/spacer.gif" width="20"/></td>
			</tr>
	</table>

</asp:Panel>


<table runat="server" id="tblLogin" cellpadding="0" cellspacing="0" border="0" width="730">
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="10" height="5" border="0"/></td>
		<td runat="server" id="tdLogin" valign="top" align="center">
			<img runat="server" src="~/images/login_top.gif"/><br>  
			<table ID="Table1" runat="server" cellpadding="0" cellspacing="1" border="0" width="730" bgcolor="#CCD9E8">
				<tr>
					<td align="right" valign="top">
					<img runat="server" src="~/images/spacer.gif" width="260" height="1"/><br>
						<center class="assetTitle">
						<span style="padding-left: 70px;">
							Member Sign In<br>
							<span class="bodyText" style="font-weight: normal;padding-left: 70px;">Existing members sign in here.</span></span></center>
						<br>
						email &nbsp; <input type="text" size="30" maxlength="50" ID="txtUserName" runat="server" NAME="Text1">
						<br>
						&nbsp;password &nbsp; <input type="password" size="30" maxlength="80" ID="txtPassword" runat="server" onkeydown="javascript:checkEnter(event)" NAME="Password1"><br>
						<asp:checkbox runat="server" id="chkRememberLogin" class="bodyText" ToolTip="Remember Password"/>&nbsp;Remember my sign in info <br><br><center style="padding-left: 80px;">
						
						<asp:ImageButton id="imgLogin" runat="server" CausesValidation="False" AlternateText="Sign in to Kaneva" ImageUrl="~/images/button_signin.gif" OnClick="imgLogin_Click"/><br>
						<a runat="server" id="aLostPassword" class="adminLinks" style="vertical-align:top; cursor:hand" href="~/lostPassword.aspx"><u>lost id or password?</u></a></center>
						<br>
						<center>
							<table runat="server" id="tblFree" Visible="false" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td><img runat="server" src="~/images/spacer.gif" width="60" height="2"/></td>
									<td class="dateStamp" align="center">
									<center style="padding-left: 20px;">
										<b>OR: </b>&nbsp;continue to download FREE content without registration<br><br><asp:ImageButton id="imgFreeContent" runat="server" CausesValidation="False" OnClick="btnFree_click" ImageUrl="~/images/button_WEform.gif" /></center><br><br>
									</td>
								</tr>
							</table>
						</center>
					</td>
					<td width="100%" valign="top" align="right">
						<center class="assetTitle">New Member Registration
							<br><span class="money" style="font-size: 25px; line-height:30px;">FREE & Easy!</span>
							<br><br><img runat="server" src="~/images/regpeeps.jpg" border="0"/><br><br>
							<a runat="server" id="aRegister" class="t4" ><img runat="server" border="0" src="~/images/button_reg.gif" alt="Join Kaneva"/></a>
						</center><br>
						<img runat="server" src="~/images/spacer.gif" width="400" height="1" border="0">
					</td>
					<td width="100%">&nbsp;</td>
				</tr>
			</table>
			<img runat="server" src="~/images/login_bottom.gif"/>
		</td>
	</tr>
</table>

<asp:Panel ID="pnlPurchase" runat="server">
<center>

	<table cellpadding="2" cellspacing="0" border="0" width="730">
		<tr>
			<td class="header02" align="left" bgcolor="#999999" width="80%">Item
			</td>
			<td class="header02" bgcolor="#999999" align="center">Price</td>
		</tr>
		<tr ID="trImmediatePurchase" runat="Server">
			<td class="bodyText" align="left" valign="middle">
				
				<table cellpadding="0" cellspacing="0" border="0" width="100%">							
					<tr>
						<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="5" border="0"/></td>
						<td class="blogBody02" valign="top" width="100%">
							<asp:Label runat="server" id="lblAssetName2" CssClass="assetLink" style="font-size:17px;"/><br><br>
							type: 	<asp:Label id="lblType" runat="server" class="bodyText"/>	<br>
							owner: <asp:Label id="lblUserName" runat="server" class="dateStamp"/>&nbsp;&nbsp;
							published:&nbsp;<asp:Label id="lblCreatedDate" runat="server" CssClass="adminLinks"/></span>&nbsp; 
							<br><br>	
							<asp:Label id="lblTeaser" runat="server"/>	
						</td>	
						<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="5" border="0"/></td>								
					</tr>	
				</table>
				
			</td>
			<td class="bodyText" align="right" nowrap>
				<asp:Label CssClass="Money" runat="server" id="lblAssetAmount"/><asp:DropDownList class="filter2" id="drpSingleSubscription" style="width:250px" runat="Server" OnSelectedIndexChanged="drpSubscription_Change" AutoPostBack="True"/><img runat="server" src="~/images/spacer.gif" width="10" height="10" border="0"/>
				
				<br><br><a runat="server" id="aRecurringHelp" target="_resource" href="http://docs.kaneva.com/bin/view/Public/KanevaComPurchaseGame" class="LnavText">How do recurring subscriptions work?</a><img runat="server" src="~/images/spacer.gif" width="10" height="10" border="0"/>
			</td>
		</tr>
		<tr>							
			<td align="right" class="dateStamp" style="border-top: 1px solid #bbbbbb;"><B>Total:</B></td>
			<td class="bodyText" align="right" style="border-top: 1px solid #bbbbbb;">
				<asp:Label CssClass="Money" runat="server" id="lblOrderTotal"/><img runat="server" src="~/images/spacer.gif" width="10" height="10" border="0"/>
			</td>
		</tr>
		</table>
		</center>
		
		<br/><br/><br/>
		
		<center>
		<table cellpadding="2" cellspacing="0" border="0" width="730">
			<tr>
				<td colspan="2" class="header02" bgcolor="#999999">K-Point Selection&nbsp;&nbsp;<a href="http://docs.kaneva.com/bin/view/public/KanevaFaqKpoints" target="_resource" class="header02">K-Point FAQ</a>
				</td>
			</tr>
			<tr>
				<td align="left" valign="middle" class="bodyText" Width="50%">
					<span style="color: red;"><asp:Label runat="server" id="lblInsufficientPoints">*Insufficient K-Points to make purchase.<BR/>Buy more K-Points&nbsp;</asp:Label><i><asp:Label id="lblMinimumKPoints" runat="server">(500 minimum):</asp:label> </i></span><asp:Label runat="server" id="lblBuyPoints">Would you like to buy additional K-Points? (<i>500 minimum</i>)</asp:Label>
				</td>
				<td class="bodyText" align="left" Width="50%">
					<asp:RadioButton id="rdoNoThanks" AutoPostBack="False" GroupName="optKPoints" runat="server"/><asp:Label id="lblNoThanks" runat="server">No Thanks<br/></asp:label>
					<asp:RadioButton id="rdoPackage" AutoPostBack="False" GroupName="optKPoints" runat="server"/>Package Deal <asp:DropDownList class="filter2" Width="250px" id="drpKPoints" runat="Server" AutoPostBack="False"/>
				</td>
			</tr>
			<tr>
				<td align="right" valign="middle" class="bodyText">
					
				</td>
				<td class="bodyText" align="left">
					<asp:RadioButton id="rdoCustom" AutoPostBack="False" GroupName="optKPoints" runat="server"/>Custom Amount <asp:textbox class="filter2" runat="server" id="txtKPoints" runat="Server" width="200px" maxlength="7"/>
					<asp:RangeValidator id="rngtxtKPoints" ControlToValidate="txtKPoints" MinimumValue="0" MaximumValue="1000000" Type="Integer" EnableClientScript="true" ErrorMessage="Custom amount must be at least 500, up to a maximum of 1000000 (1 million)." Text="*" Display="Dynamic" runat="server"/>
					<asp:RequiredFieldValidator ID="rfdtxtKPoints" ControlToValidate="txtKPoints" Text="*" ErrorMessage="Amount is a required field." Display="Dynamic" runat="server"/>
				</td>
			</tr>
		</table>
	
		
		<table cellpadding="2" cellspacing="0" border="0" width="730">
			<tr>
				<td colspan="3" bgcolor="#ededed" align="right"><br>
				<asp:button id="btnContinueShopping" runat="Server" CausesValidation="False" onClick="btnContinueShopping_Click" class="Filter2" Text="     cancel     "/>&nbsp;<asp:button id="btnMakePurchase" runat="Server" CausesValidation="False" onClick="btnMakePurchase_Click" class="Filter2" Text="   continue   "/><img runat="server" src="~/images/spacer.gif" width="10" height="10" ID="Img1"/><br>
				</td>
			</tr>
		</table>
			
			
	</center>	
</asp:Panel>

	</td>
	</tr>
</table>
</center>

</div><br><br>