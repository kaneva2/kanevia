///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for orderCompleteFeaturedItem.
	/// </summary>
	public class orderCompleteFeaturedItem : MainTemplatePage
	{
		protected orderCompleteFeaturedItem () 
		{
			Title = "Featured Item Order Complete";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			if (Request ["assetId"] != null)
			{
				int assetId = Convert.ToInt32 (Request ["assetId"]);

				DataRow drAsset = StoreUtility.GetAsset (assetId);
				lblFeatured.Text = drAsset ["name"].ToString ();
			}
		}

		protected Label lblFeatured;

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
