<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="orderComplete_B.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.orderComplete_B" %>

<link href="../css/new.css" rel="stylesheet" type="text/css">
<link href="../css/shadow.css" rel="stylesheet" type="text/css">
<link href="../css/temp_purchaseFlow.css" rel="stylesheet" type="text/css">

<table border="0" cellspacing="0" cellpadding="0"  width="990" align="center">
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" id="Img1" width="1" height="14" /></td>
	</tr>
	<tr>
		<td>
			<!-- new div: link to manage account settings -->
			<table  border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="frTopLeft"></td>
					<td class="frTop"></td>
					<td class="frTopRight"></td>
				</tr>
				<tr>
					<td class="frBorderLeft"><img id="Img2" runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
					<td valign="top" class="frBgIntMembers">
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="968" align="left"  valign="top">
								
									<table cellpadding="0" cellspacing="0" border="0"  width="100%">
										<tr>
											<td width="968" valign="top" align="center">

												<div class="progressBar">
                                                <!-- WIDTH OF ALL SPANS SHOULD ADD UP TO 958 PX -->
                                                    <ul class="receipt">
                                                        <li id="liSelect" class=""><h3>Select Package</h3></li>
                                                        <li id="liPayment" class=""><h3>Payment</h3></li>
                                                        <li id="liReceipt" class=""><h3>Receipt</h3></li>
                                                    </ul>			
                                                </div>

											</td>
										</tr>
										<tr>
											<td width="970" valign="top">
												<table border="0" cellspacing="0" cellpadding="0" width="100%">
													<tr>
														<!-- START WIZARD CONTENT -->
														<td valign="top" align="center">
															<table border="0" cellspacing="0" cellpadding="0">
																<tr>
																	<td width="670">
																		<div class="module whitebg fullpage">
																			<span class="ct"><span class="cl"></span></span>
																			<table border="0" cellspacing="0" cellpadding="0"  align="center" class="wizform">
																				<tr>
																					<td align="left">
																						<table id="Table11" width="640" cellspacing="0" cellpadding="0" border="0">
																							<tr>
																								<td colspan="3">
																									<h1>View Receipt</h1>
																									<p align="left" style="padding-left:20px;">A confirmation receipt was sent to you by email.</p>
																									<table border="0" cellspacing="0" cellpadding="0" width="94%" align="center" class="data nopadding">
																										<tr>
																											<td colspan="2" style="padding: 8px 0 8px 5px;"><h2>Completed Order Summary</h2></td>
																										</tr>
																										<tr>
																											<td align="left" width="35%">Order Number</td>
																											<td align="left" width="65%"><asp:label id="lblOrderNumber" runat="server"/></td>
																										</tr>
																										<tr>
																											<td align="left">Description</td>
																											<td align="left"><asp:label id="lblDescription" runat="server"/></td>
																										</tr>
																										<tr>
																											<td align="left">Amount Paid</td>
																											<td align="left"><asp:label id="lblAmountPaid" runat="server"/></td>
																										</tr>
																										<tr>
																											<td align="left" class="bold">Credits Received</td>
																											<td align="left" class="bold"><asp:label id="lblKPoints" runat="server"/></td>
																										</tr>
																										<tr>
																											<td align="left">Transaction Date</td>
																											<td align="left"><asp:label id="lblDate" runat="server"/></td>
																										</tr>
																										<tr>
																											<td align="left">Payment Method</td>
																											<td align="left">
																												<table border="0" cellspacing="0" cellpadding="0" align="left" width="100%">
																													<tr>
																														<td align="left"><asp:label id="lblName" runat="server"/></td>
																													</tr>
																													<tr>
																														<td align="left"><asp:label id="lblCardType" runat="server"/></td>
																													</tr>
																													<tr>
																														<td align="left"><asp:label id="lblCardNumber" runat="server"/></td>
																													</tr>
																												</table>
																											</td>
																										</tr>
																										<tr id="trBillAddress" runat="server">
																											<td align="left">Billing Address</td>
																											<td align="left">
																												<table border="0" cellspacing="0" cellpadding="0" align="left" width="100%">
																													<tr>
																														<td align="left"><asp:label id="lblAddress1" runat="server"/>
																															<asp:label id="lblAddress2" runat="server"/>
																														</td>
																													</tr>
																													<tr>
																														<td align="left"><asp:label id="lblCity" runat="server"/>, <asp:label id="lblState" runat="server"/> <asp:label id="lblZip" runat="server"/></td>
																													</tr>
																													<tr>
																														<td align="left"><asp:label id="lblCountry" runat="server"/></td>
																													</tr>
																												</table>
																											</td>
																										</tr>
																									</table>
																									<br /><br />
																									<table border="0" cellspacing="0" cellpadding="0" width="94%">
																										<tr>
																											<td align="right">
																												<a href="" id="aShop" runat="server" class="btnGetStuff"><span>Get Cool 3D Stuff</span></a>
																												<a href="" runat="server" id="aGoInWorld">Go Into 3D</a>
	                                                                                                       	</td>
																										</tr>
																									</table>
																								</td>
																							</tr>
																						</table>
																					</td>
																				</tr>
																			</table>
																		
																			<span class="cb"><span class="cl"></span></span>
																		</div>
																	</td>
																</tr>
															</table>
														</td>
														
														<!-- END WIZARD CONTENT -->
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" id="Img5" width="1" height="1" /></td>
				</tr>
				<tr>
					<td class="frBottomLeft"></td>
					<td class="frBottom"></td>
					<td class="frBottomRight"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>