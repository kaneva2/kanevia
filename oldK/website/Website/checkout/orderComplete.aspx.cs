///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using log4net;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using System.Collections.Generic;

using KlausEnt.KEP.Kaneva.usercontrols;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for orderComplete.
	/// </summary>
	public class orderComplete : MainTemplatePage
	{
		protected orderComplete () 
		{
			Title = "Order Complete";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			VerifyUserAccess();
			VerifyOrder();

			int orderId = 0;
			if (Request ["orderId"] != null)
			{
				orderId = Convert.ToInt32 (Request ["orderId"]);
			}

			if (orderId > 0)
			{
				DataRow drOrder = StoreUtility.GetOrder (orderId);

				if (drOrder ["point_transaction_id"] != DBNull.Value)
				{
					DataRow drPurchasePoints = StoreUtility.GetPurchasePointTransaction (Convert.ToInt32 (drOrder ["point_transaction_id"]));
                    Promotion promotion = (new PromotionsFacade()).GetPromotionsByPromoId(Convert.ToInt32(drOrder["point_bucket_id"]));

                    string freeCredits = FormatCreditDisplay(promotion.FreePointsAwardedAmount.ToString(), promotion.FreeKeiPointID);
                    string paidCredits = FormatCreditDisplay(promotion.KeiPointAmount.ToString(), promotion.KeiPointId);

					lblOrderNumber.Text = drPurchasePoints ["order_id"].ToString ();
					lblDescription.Text = drPurchasePoints ["description"].ToString ();
					lblAmountPaid.Text = FormatCurrency (drPurchasePoints ["amount_debited"]);
                    lblKPoints.Text = paidCredits + (freeCredits.Length > 0 ? (" " + freeCredits) : "");
					lblDate.Text = FormatDateTime (drPurchasePoints ["transaction_date"]);

                    if (Convert.ToInt32(drPurchasePoints["payment_method_id"]) == (int) Constants.ePAYMENT_METHODS.PAYPAL)            
                    {                        
                        trBillAddress.Visible = false;

                        lblName.Text = "Paypal";

                        litOrderCity.Text = "";
                        litOrderState.Text = "";
                    }
                    else    // credit card
                    {
                        DataRow drBillingInfo = StoreUtility.GetOrderBillingInfo (Convert.ToInt32 (drPurchasePoints["order_billing_information_id"]));

                        lblName.Text = drBillingInfo["name_on_card"].ToString ();
                        lblCardNumber.Text = "xxxx-xxxx-xxxx-" + KanevaGlobals.Decrypt (drBillingInfo["card_number"].ToString ());
                        lblCardType.Text = drBillingInfo["card_type"].ToString ();

                        DataRow drAddress = UsersUtility.GetAddress (GetUserId (), Convert.ToInt32 (drPurchasePoints["address_id"]));

                        lblAddress1.Text = drAddress["address1"].ToString ();
                        lblAddress2.Text = drAddress["address2"].ToString ();
                        lblCity.Text = drAddress["city"].ToString ();
                        lblState.Text = drAddress["state_code"].ToString ();
                        lblZip.Text = drAddress["zip_code"].ToString ();
                        lblCountry.Text = drAddress["country_name"].ToString ();

                        litOrderCity.Text = drAddress["city"].ToString ();
                        litOrderState.Text = drAddress["state_code"].ToString();
                    }

                    // Add Google Ecommerce Tracking here
                    // Google Analytics 
                    litGAAcct.Text = KanevaGlobals.GoogleAnalyticsAccount;
                    litOrderId1.Text = orderId.ToString ();
                    litOrderId2.Text = orderId.ToString ();
                    litOrderTotal.Text = drPurchasePoints ["amount_debited"].ToString ();
                    litOrderCountry.Text = KanevaWebGlobals.CurrentUser.Country;
                    litItemSKU.Text = promotion.PromotionId.ToString ();
                    litItemName.Text = drPurchasePoints ["description"].ToString ().Replace ("\"", "").Replace ("'","");
                    litItemCategory.Text = promotion.PromotionalOffersTypeId.ToString ();
                    litItemPrice.Text = drPurchasePoints ["amount_debited"].ToString ();

                    // -- TEMP CODE for VIP/AP A/B test
                    // This code will write out the correcte Google Optimizer code based on whethere
                    // the user purchased VIP or AP.   
                    litGoogleOptimizer.Text = "";
                    if (promotion.PromotionId == KanevaGlobals.AccessPassPromotionID)   // AP
                    {
                        litGoogleOptimizer.Text = "	var _gaq = _gaq || [];" +
	                        "_gaq.push(['gwo._setAccount', 'UA-5755114-4']);" +
                            "_gaq.push(['gwo._trackPageview', '/3204893232/goal']);" +
	                        "(function () {" +
		                    "    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;" +
		                    "    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';" +
		                    "    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);" +
	                        "})();";   
                    }
                    else if (promotion.PromotionId == KanevaGlobals.VIPPassPromotionID)  // VIP
                    {
                        litGoogleOptimizer.Text = "	var _gaq = _gaq || [];" +
	                        "_gaq.push(['gwo._setAccount', 'UA-5755114-4']);" +
                            "_gaq.push(['gwo._trackPageview', '/2557627495/goal']);" +
	                        "(function () {" +
		                    "    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;" +
		                    "    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';" +
		                    "    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);" +
	                        "})();";   
                    }
				}

				// Set Nav
                HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;

				//setup wizard nav bar
				ucWizardNavBar.ActiveStep = CheckoutWizardNavBar.STEP.STEP_4;
			}
		}

		
		#region Helper Methods
		
		private void VerifyUserAccess()
		{
			// They must be logged in
			if (!Request.IsAuthenticated)
			{
				Response.Redirect (GetLoginURL ());
			}
		}

        private string FormatCreditDisplay(string amount, string currency)
        {
            string display = "";
            try
            {

                //pull out the credits given
                double creditAmount = Convert.ToDouble(amount);

                if (creditAmount > 0.0)
                {
                    switch (currency)
                    {
                        case Constants.CURR_KPOINT:
                            display += creditAmount.ToString("#,##0") + " credits";
                            break;
                        case Constants.CURR_GPOINT:
                            display += creditAmount.ToString("#,##0") + " rewards";
                            break;
                    }
                }
            }
            catch (Exception) { }
            return display;
        }

		private void VerifyOrder ()
		{
			if (Request ["orderId"] == null)
			{
				Response.Redirect ( "~/mykaneva/managecredits.aspx" );
			}

			try
			{
				int orderId = Convert.ToInt32 (Request ["orderId"]);			
				VerifyOrder (StoreUtility.GetOrder (orderId, GetUserId()));
			}
			catch (Exception ex)
			{
				string x = ex.Message;
				Response.Redirect ( "~/mykaneva/managecredits.aspx" );
			}
		}

		private void VerifyOrder (DataRow drOrder)
		{
			try				
			{
				// Verify user is owner of this order		  
				if (drOrder == null)
				{
					// User may be trying to view someone else's order
					m_logger.Warn ("User " + GetUserId () + " tried to view orderId " + Request ["orderId"].ToString () + " from IP " + Common.GetVisitorIPAddress());
					Response.Redirect ( "~/mykaneva/managecredits.aspx" );
				}

				if (Convert.ToInt32(drOrder["transaction_status_id"]) != (int) Constants.eORDER_STATUS.COMPLETED) 
                {
                    // paypal orders may not have been updated in our db from paypal yet.  if not the order will be
                    // in a pending state until we receive the info back from the paypal site.
                    if (Convert.ToInt32 (drOrder["transaction_status_id"]) != (int) Constants.eORDER_STATUS.PENDING_PURCHASE &&
                        StoreUtility.GetOrderPaymentMethod (Convert.ToInt32(drOrder["order_id"]), Convert.ToInt32 (drOrder["point_transaction_id"])) != (int) Constants.ePAYMENT_METHODS.PAYPAL)
                    {
                        m_logger.Warn ("User " + GetUserId () + " tried to view orderId " + Request["orderId"].ToString () + " from IP " + Common.GetVisitorIPAddress() + ".  Order does not have a status of COMPLETED.");
                        Response.Redirect ("~/mykaneva/managecredits.aspx");
                    }
				}
			}
			catch
			{
				Response.Redirect ( "~/mykaneva/managecredits.aspx" );
			}
		}
		
		#endregion
		
		#region Decleraions

		protected Label lblOrderNumber, lblDescription, lblAmountPaid, lblKPoints, lblDate;
		protected Label lblName, lblCardNumber, lblCardType;
		protected Label lblAddress1, lblAddress2, lblCity, lblState, lblZip, lblCountry;

        protected HtmlTableRow trBillAddress;

		protected CheckoutWizardNavBar	ucWizardNavBar;
		
        // Google Tracking 
        protected Literal litGAAcct, litOrderId1, litOrderId2, litOrderTotal;
        protected Literal litOrderCity, litOrderState, litOrderCountry;
        protected Literal litItemSKU, litItemName, litItemCategory, litItemPrice;

        protected Literal litGoogleOptimizer;


		// Logger
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
