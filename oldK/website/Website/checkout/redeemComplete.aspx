<%@ Page language="c#" Codebehind="redeemComplete.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.redeemComplete" %>

<link href="../css/themes-join/template.css" rel="stylesheet" type="text/css">	
<link href="../css/themes-join/join_dp3d.css" rel="stylesheet" type="text/css">	

<table cellpadding="0" cellspacing="0" border="0" width="790" align="center">
	<tr>
		<td colspan="3"><img runat="server" src="~/images/spacer.gif" width="1" height="14" id="Img12"/></td>
	</tr>
	<tr>
		<td colspan="3">
			<table  border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="frTopLeft"></td>
					<td class="frTop"></td>
					<td class="frTopRight"></td>
				</tr>
				<tr>
					<td class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" id="Img13" width="1" height="1" /></td>
					<td valign="top" class="frBgIntMembers">
						<table width="100%" border="0" cellpadding="0" cellspacing="0" >
							<tr>
								<td width="770" align="left"  valign="top">
								
									<table cellpadding="0" cellspacing="0" border="0"  width="100%">
										<tr>
											<td valign="top">
												<div id="templateHeader"><img runat="server" src="~/images/header_border.gif" alt="header_border.gif" width="768" height="120" border="0" id="Img14"><h1>Kaneva Join Process</h1></div>
											</td>
										</tr>
										<tr>
											<td width="790" valign="top">
												
												<table border="0" cellspacing="0" cellpadding="0" width="100%">
													<tr>
														<!-- START WIZARD CONTENT -->
														<td valign="top" align="center">
															
															
															
                                                            <table border="0" cellspacing="0" cellpadding="0" width="670" align="center">
																<tr>
																	<td width="670">

																		<table border="0" cellspacing="0" cellpadding="0" width="100%">
																			<tr>
																				<td width="70%" valign="top">
																					<span id="Span1"></span>
																					<h1>Congratulations <span id="spnUserName" runat="server"></span>!</h1>
																					<h2>You're Dance Party 3D Game card has been redeemed.</h2><br />
																					<h2>You now have <span id="spnCreditTotal" runat="server"></span> additional credits and bonus dance gear.</h2><a href="~/mykaneva/managecredits.aspx" runat="server">Check your credits.</a>
			                                                                        <br /><br />
			                                                                        <p>Go shopping:</p>
																					<ul>
																						<li>Get clothes and accessories for your avatar</li>
																						<li>Open a 3D dance club</li>
																						<li>Get furniture and accessories to decorate your 3D home</li>
																					</ul>
																					
																					<br>
																					<center>
																					<a href="" runat="server" id="aGoInWorld" class="nohover"><img runat="server" src="~/images/btn_letsgo.gif" width="134" height="40" border="0" align="center" /></a></center>
																				</td>
																				<td width="30%" valign="top" align="right"><img runat="server" src="~/images/male_avatar.jpg" alt="male_avatar.jpg" height="250" border="0"></td>
																			</tr>
																		</table>
																		
																		<br><br>	
																			
																			
																			<table border="0" cellspacing="0" cellpadding="0" width="100%">
																				
																				<tr>
																					<td align="center">
																						<div class="module">
																						<span class="ct"><span class="cl"></span></span>
																						
																						
																						
																						<table border="0" cellspacing="0" cellpadding="0" width="95%" align="center">
																							<tr>
																								<td valign="top" width="400">
																									<h2>How to Install</h2>
																									<p class="small">1) Click the <strong>Let's Go</strong> button above. 2) Click <strong>Run</strong> in each window that appears. 3) Sign in using the email address you used to create your account.</p>
																									<br>
																									<table border="0" cellspacing="0" cellpadding="5">
																										<tr>
																											<td><img runat="server" src="~/images/rundialog1.jpg" alt="rundialog1.jpg" width="150" border="0"></td>
																											<td><img runat="server" src="~/images/rundialog2.jpg" alt="rundialog2.jpg" width="200" border="0"></td>
																										</tr>
																									</table>
																								</td>
																								<td valign="top">
																									<div class="module whitebg">
																									<span class="ct"><span class="cl"></span></span>
																									<h2>System Requirements</h2>
																									
																									<ul class="small">
																										<li>Windows XP or Vista</li>
																										<li>515 MB available hard disk space</li>
																										<li>512 MB RAM</li>
																										<li>High-speed Internet connection</li>
																										<li>Nvidia 5200 series / ATI X700 series or faster graphics card</li>
																										<li>Intel or AMD 2GHz or faster processor</li>
																										<li>Internet Explorer 6.0 OR Mozilla Firefox 1.5 or later</li>
																									</ul>
																									<br>
																									<p>Need Help? <a id="A1" href="~/community/CommunityPage.aspx?communityId=1118&pageId=887411" runat="server" target="_blank">Click here for support.</a></p>
																									<br />
																									<p>Still having trouble? <a id="A2" href="~/suggestions.aspx?mode=F&category=Feedback" runat="server" target="_blank">Contact us</a></p>
																									
																									<span class="cb"><span class="cl"></span></span>
																									</div>	
																								</td>
																							</tr>
																						</table>
																						<span class="cb"><span class="cl"></span></span>
																						</div>	
																					</td>
																				</tr>
																				
																			</table>
																			<br/>																		
																		 
																	</td>
																</tr>
															</table>
																																																																																
														</td>
														
														<!-- END WIZARD CONTENT -->
													</tr>
												</table>
											
											</td>
											
										</tr>
										
									</table>
											
								</td>
							</tr>
							
						</table>
					</td>
					<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" id="Img15" width="1" height="1" /></td>
				</tr>
				<tr>
					<td class="frBottomLeft"></td>
					<td class="frBottom"></td>
					<td class="frBottomRight"></td>
				</tr>
			</table>				
		</td>
	</tr>
</table>
<br/><br/><br/><br/>
