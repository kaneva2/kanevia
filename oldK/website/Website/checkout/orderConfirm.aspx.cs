///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using System.Web.Security;
using System.Security.Principal;
using System.Security.Cryptography;
using log4net;

using KlausEnt.KEP.Kaneva.usercontrols;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using System.Collections.Generic;


namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for orderConfirm.
	/// </summary>
	public class orderConfirm : MainTemplatePage
	{
		/// <summary>
		/// Page_Load
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Page_Load (object sender, System.EventArgs e)
		{
			Title = "Make Purchase";

            //determines if the promotion is a subscription based purchase
            promotionIsSubscription = IsSubscription();
			
            VerifyUserAccess();
            if (!KanevaWebGlobals.CurrentUser.HasWOKAccount)
            {
                Response.Redirect ("~/error.aspx?error=" + (int) Constants.eERROR_TYPE.INVALID_ACCOUNT);
            }

			// Clear any previous errors
			lblErrors.Text = "";

			Response.Expires = 0;
			Response.CacheControl = "no-cache";

            LoadCCYears ();

			if (!IsPostBack)
			{
				DisplayScreen ();
			}

			// Set up purchase button																																																								  
			btnMakePurchase.CausesValidation = false;

            btnMakePurchase.Attributes.Add ("onclick", "makePurchase();"); 
            // litPostBackRef.Text = ClientScript.GetPostBackEventReference (this.btnMakePurchase, "", false);
			
            
            imgMakePurchase.Attributes.Add ("onclick", "document.getElementById('" + btnMakePurchase.ClientID + "').click();");


			string orderId = Request["orderId"].ToString();

			aChangeAddress.HRef = ResolveUrl ("~/checkout/billinginfo.aspx?orderId=" + orderId);
            aPopUpEnterAddress.HRef = ResolveUrl ("~/checkout/billinginfo.aspx?orderId=" + orderId);
            aEditOrder.HRef = ResolveUrl ("~/mykaneva/buyCredits.aspx?orderId=" + orderId);

			// Set Nav
            HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
			
			//setup wizard nav bar
			ucWizardNavBar.ActiveStep = CheckoutWizardNavBar.STEP.STEP_3;
            string retUrl = "~/mykaneva/buycredits.aspx?orderId=" + Request["orderId"].ToString();
            if (promotionIsSubscription)
            {
                retUrl = "~/mykaneva/buySpecials.aspx?pass=true&orderId=" + Request["orderId"].ToString();
            }

            ucWizardNavBar.StepOneHref = retUrl;
            ucWizardNavBar.StepTwoHref = "~/checkout/billinginfo.aspx?orderId=" + orderId;

            if (KanevaGlobals.EnablePaypal || IsAdministrator ())
            {
                trPaypal.Visible = true;
            }
		}

		
		#region Helper Methods

		/// <summary>
		/// DisplayScreen
		/// </summary>
		private void DisplayScreen ()
		{
			int userId = GetUserId ();

			if (Request ["orderId"] == null)
			{
				Response.Redirect ("~/mykaneva/managecredits.aspx");
			}

			try
			{
				// Get the order
				int orderId = Convert.ToInt32 (Request ["orderId"]);				  
				DataRow drOrder = StoreUtility.GetOrder (orderId, GetUserId());

				VerifyOrder (drOrder);

				int purchaseType = Convert.ToInt32 (drOrder ["purchase_type"]);

				// Where any K-Points purchased?
				int pointTransactionId = 0;
				Double dCurrentPointAmountPurchased = 0.0;              

				// Get the billing info
				pointTransactionId = Convert.ToInt32 (drOrder ["point_transaction_id"]);
				DataRow drPpt = StoreUtility.GetPurchasePointTransaction (pointTransactionId);

				spnPrice.InnerText = Convert.ToDouble(drPpt["amount_debited"]).ToString ();
				spnTotal.InnerText = spnPrice.InnerText;

                spnItemDescription.InnerText = drPpt["description"].ToString();
				dCurrentPointAmountPurchased = Convert.ToDouble (drPpt ["totalPoints"]);

				// What is the payment method?
				int paymentMethodId = Convert.ToInt32 (drPpt ["payment_method_id"]);

				if (paymentMethodId.Equals ((int) Constants.ePAYMENT_METHODS.PAYPAL))
				{
                    divLegal.Visible = true;
                    radPmtPP.Checked = true;
                    tblcc.Style["display"] = "none";
                    spnAddress2.InnerText = "Address not required for Paypal";
				}
				else
				{
                    //configure for  pass if needed
                    if (promotionIsSubscription)
                    {
                        radPmtPP.Disabled = true;
                        trPaypal.Style["display"] = "none";

                        //get subscription data and promotion data
                        try
                        {
                            //get promotion Id
                            int promotionID = Convert.ToInt32(drPpt["point_bucket_id"]);

                            //get related promotion
                            Promotion associatePromotion = (new PromotionsFacade()).GetPromotionsByPromoId(promotionID);

                            //get related subscription and subscription info
                            Subscription subscript = (new SubscriptionFacade()).GetSubscriptionByPromotionId((uint)promotionID);
                            bool userHadThisBefore = (new SubscriptionFacade()).HasUserHadThisPromotionBefore((uint)promotionID, (uint)GetUserId());


                            //display various legal text based on different conditions
                            if (userHadThisBefore || subscript.DaysFree <= 0)
                            {
                                divPassessLegal2NoTrial.Visible = true;
                                divPassessLegalNoTrial.Visible = true;
                                divPassessLegal.Visible = false;
                                divPassessLegal2.Visible = false;
                                lblSUBPriceNT.Text = subscript.Price.ToString();
                                lblBillingPeriodNT.Text = subscript.Term.ToString();
                            }
                            else
                            {
                                divPassessLegal.Visible = true;
                                divPassessLegal2.Visible = true;
                                divPassessLegal2NoTrial.Visible = false;
                                divPassessLegalNoTrial.Visible = false;

                                lblFreePeriod.Text = subscript.DaysFree.ToString();
                                lblSUBPrice.Text = subscript.Price.ToString();
                                lblBillingPeriod.Text = subscript.Term.ToString();
                            }
                            divAllowance.Visible = (subscript.MonthlyAllowance > 0);
                            divFreeRewards.Visible = (associatePromotion.FreePointsAwardedAmount > 0.0m);
                            divFreeCredits.Visible = (associatePromotion.KeiPointAmount > 0.0m);
                        }
                        catch (Exception) { }

                    }
                    radPmtCC.Checked = true;
                    tblpp.Style["display"] = "none";
                }

                string noAddressScript = ";$('PopUpAddr').style.display='block';$('imgMakePurchase').style.display='none';";  //$('btnMakePurchase').disabled=true;";

                // Address
                if (drPpt["address_id"] != System.DBNull.Value)
                {
                    int addressId = Convert.ToInt32 (drPpt["address_id"]);
                    DataRow drAddress = UsersUtility.GetAddress (userId, addressId);

                    if (drAddress != null)
                    {
                        spnFullName.InnerText = drAddress["name"].ToString ();
                        spnAddress1.InnerText = drAddress["address1"].ToString ();
                        spnAddress2.InnerText = drAddress["address2"].ToString ();
                        spnCity.InnerText = drAddress["city"].ToString () + (drAddress["state_name"].ToString ().Length > 0 ? ", " : "");
                        spnState.InnerText = drAddress["state_name"].ToString ();
                        spnZip.InnerText = drAddress["zip_code"].ToString ();
                        spnCountry.InnerText = drAddress["country_name"].ToString ();
                        spnPhone.InnerText = drAddress["phone_number"].ToString ();

                        aChangeAddress.InnerText = "Change Address";
                        noAddressScript = "";
                    }
                }


                radPmtCC.Attributes.Add ("onclick", "javascript:$('tblcc').style.display='block';$('tblpp').style.display='none';" + noAddressScript);

				// Show rest of labels
				Double dUserKPointTotal = GetUserPointTotal ();
				spnCreditBalance.InnerText = Convert.ToUInt64(dUserKPointTotal).ToString("N0");
				spnCreditPurchasing.InnerText = Convert.ToUInt64(dCurrentPointAmountPurchased).ToString("N0");
				spnNewCreditBalance.InnerText = Convert.ToUInt64(dUserKPointTotal + dCurrentPointAmountPurchased).ToString("N0");

				drpMonth.SelectedIndex = DateTime.Now.Month-1;
			}
			catch (Exception exc)
			{
				m_logger.Error ("Error loading order information for order " + Request ["orderId"].ToString(), exc);
				ShowMessage ("There was a problem loading the order	information.  Please try reloading this page or creating a new order.");
				return;
			}
		}

		/// <summary>
		/// GetUserPointTotal
		/// </summary>
		/// <returns></returns>
		private Double GetUserPointTotal ()
		{
			return UsersUtility.GetUserPointTotal (GetUserId ());
		}

		/// <summary>
		/// MakeCyberSourcePayment
		/// </summary>
		/// <param name="drOrder"></param>
		/// <param name="drPpt"></param>
		/// <returns></returns>
		private bool MakeCyberSourcePayment (DataRow drOrder, DataRow drPpt, string ccNumEncrypt, string ccSecCodeEncrypt, ref string userErrorMessage, ref bool userHadThisBefore)
		{
            UserFacade userFacade = new UserFacade();
			bool bSystemDown = false;
            bool processSuccesful = false;
            string subscriptionId = "";
            int orderId = Convert.ToInt32(drOrder["order_id"]);

			CybersourceAdaptor cybersourceAdaptor = new CybersourceAdaptor ();

            // Set up for Fraud Checks
            int userId = Convert.ToInt32(drOrder["user_id"]);
            FraudDetect fraudDetect = userFacade.GetFraudDetect(userId);

            string ccNumTempLast4 = txtCardNumber.Text.Trim();
            if (ccNumTempLast4.Length > 4)
            {
                ccNumTempLast4 = ccNumTempLast4.Substring(ccNumTempLast4.Length - 4, 4);
            }

            TransactionData td = new TransactionData(
                Common.GetVisitorIPAddress(),
                Convert.ToDouble(drPpt["amount_debited"]),
                ccNumTempLast4,
                DateTime.Now,
                false);



            if (promotionIsSubscription)
            {
                //get the subscription that goes along with the access pass being purchased
                int promotionID = Convert.ToInt32(drPpt["point_bucket_id"]);
                SubscriptionFacade target = new SubscriptionFacade();

                //Subscription relatedSubscription = target.GetSubscriptionByPromotionId((uint)promotionID);
                List<Subscription> relatedSubscriptions = target.GetSubscriptionsByPromotionId((uint)promotionID);

                //check to see if this user has already purchsed this subscription before. If so do not give the free trial period or free items
                //to them. Attempt to prevent gaming.
                userHadThisBefore = target.HasUserHadThisPromotionBefore((uint)promotionID, (uint)GetUserId());

                //process all subscriptions under the promotion
                foreach (Subscription relatedSubscription in relatedSubscriptions)
                {
                    //create new user subscription
                    UserSubscription userSubscription = new UserSubscription();

                    // Set up initial Dates
                    DateTime dtNow = KanevaGlobals.GetCurrentDateTime ();
                    DateTime dtTodayEOD = new DateTime(dtNow.Year, dtNow.Month, dtNow.Day, 23, 59, 59);

                    //set the subscription term free period - none if they have had this subscription before
                    if (userHadThisBefore)
                    {
                        // They do not get a free trial for this case
                        relatedSubscription.DaysFree = (int)Subscription.SubscriptionTerm.None;
                        userSubscription.EndDate = target.GetNextEndDate(dtNow, dtNow, relatedSubscription.Term);
                        userSubscription.FreeTrialEndDate = dtNow;
                    }
                    else
                    {
                        // They get a free trial if there is one
                        if (relatedSubscription.DaysFree > 0)
                        {
                            userSubscription.EndDate = dtTodayEOD.AddDays(relatedSubscription.DaysFree);
                        }
                        else
                        {
                            userSubscription.EndDate = target.GetNextEndDate(dtNow, dtNow, relatedSubscription.Term);
                        }

                        userSubscription.FreeTrialEndDate = dtTodayEOD.AddDays(relatedSubscription.DaysFree);
                    }

                    //insert record into database
                    userSubscription.AutoRenew = true;
                    userSubscription.PromotionId = (uint)promotionID;
                    userSubscription.BillingType = (int)Constants.ePAYMENT_METHODS.KANEVA_CYBERSOURCE;
                    userSubscription.CancelledDate = DateTime.Now;
                    userSubscription.InitalTerm = relatedSubscription.Term;
                    userSubscription.Price = (double)relatedSubscription.Price;
                    userSubscription.PurchaseDate = DateTime.Now;
                    userSubscription.RenewalTerm = relatedSubscription.Term;
                    userSubscription.StatusId = Subscription.SubscriptionStatus.Pending;
                    userSubscription.SubscriptionId = relatedSubscription.SubscriptionId;
                    userSubscription.SubscriptionIdentifier = "-1";
                    userSubscription.UserCancelled = false;
                    userSubscription.UserId = Convert.ToInt32(drOrder["user_id"]);
                    userSubscription.MontlyAllowance = relatedSubscription.MonthlyAllowance;
                    userSubscription.DiscountPercent = relatedSubscription.DiscountPercent;

                    uint usersubscriptionid = target.InsertUserSubscription(userSubscription);

                    // Associate it with an order
                    target.InsertUserSubscriptionOrder(usersubscriptionid, orderId);

                    //if successfully entered into database set up cyber source and benefits
                    if (usersubscriptionid > 0)
                    {
                        //process cyber source						
						//processSuccesful = cybersourceAdaptor.CreateSubscription(drOrder, drPpt, ccNumEncrypt, ccSecCodeEncrypt, Common.GetVisitorIPAddress(), ref bSystemDown, ref userErrorMessage, ref subscriptionId, 0, usersubscriptionid, (Subscription.SubscriptionTerm)cybersourceAdaptor.GetSubcriptionConstantsFromDays(relatedSubscription.DaysFree), relatedSubscription.Term);
                        int orderbillingInfoId = Convert.ToInt32(drPpt["order_billing_information_id"]);

                        DataRow drOrderBillingInfo = StoreUtility.GetOrderBillingInfo(orderbillingInfoId);

                        BillingInfo billinfo = new BillingInfo();
                        billinfo.Address1 = drOrderBillingInfo["address1"].ToString();
                        billinfo.Address2 = drOrderBillingInfo["address2"].ToString();
                        billinfo.CardExpirationMonth = drOrderBillingInfo["exp_month"].ToString();;
                        billinfo.CardExpirationYear = drOrderBillingInfo["exp_year"].ToString();;
                        //billinfo.CardNumber;
                        billinfo.CardNumberEncrypt = ccNumEncrypt;
                        //billinfo.CardNumberLast4;
                        billinfo.CardSecurityCodeEncrypt = ccSecCodeEncrypt;
                        billinfo.CardType = drOrderBillingInfo["card_type"].ToString();
                        billinfo.City = drOrderBillingInfo["city"].ToString();
                        billinfo.Country = drOrderBillingInfo["country_id"].ToString();
                        //billinfo.CybersourceCardTypeNumber = ;
                        billinfo.FullName = drOrderBillingInfo["name_on_card"].ToString();
                        billinfo.PhoneNumber = drOrderBillingInfo["phone_number"].ToString();
                        billinfo.PostalCode = drOrderBillingInfo["zip_code"].ToString();
                        billinfo.State = drOrderBillingInfo["state_code"].ToString();
                        //billinfo.SubscriptionId = ;


                        // Fraud Check first...
                        if (CheckFraudLimit(userId, fraudDetect, ref userErrorMessage))
                        {
                            processSuccesful = cybersourceAdaptor.CreateBillingSubscription(userSubscription.UserId, usersubscriptionid, billinfo, Common.GetVisitorIPAddress(), ref bSystemDown, ref userErrorMessage, ref subscriptionId);

                            if (processSuccesful)
                            {
                                fraudDetect.SuccessCount++;
                                td.Success = true;


                            }
                            else
                            {
                                fraudDetect.FailureCount++;
                            }
                        }

                        // Save some memory only store last 10, after 5 they are all kaneva error message anyway
                        if (fraudDetect.TansactionList.Count < 10)
                        {
                            fraudDetect.TansactionList.Add(td);
                        }
                        userFacade.SaveFraudDetect(userId, fraudDetect);

                        // Send out Warning email?
                        // Warning for to many successes
                        if (processSuccesful && fraudDetect.SuccessCount == 3)
                        {
                            // Send email here
                            SendFraudAlert(userId, "Fraud Alert Warning " + KanevaGlobals.SiteName + " - 3 Successful Purchases", fraudDetect);
                        }

                        

                        //if cyber source successful adjust their balance and tickle server
                        if (processSuccesful)
                        {
                            // Bill the user
                            processSuccesful = cybersourceAdaptor.ProcessPaymentUsingSubscription (drOrder, drPpt, subscriptionId, Common.GetVisitorIPAddress(), ref userErrorMessage, ref bSystemDown, true);

                            if (processSuccesful)
                            {
                                //update the user subscription to have the cybersource id
                                userSubscription.SubscriptionIdentifier = subscriptionId;
                                userSubscription.UserSubscriptionId = usersubscriptionid;
                                userSubscription.StatusId = Subscription.SubscriptionStatus.Active;
                                target.UpdateUserSubscription(userSubscription);

                                //check to see if any credits are to be given with this promotion
                                Promotion associatePromotion = (new PromotionsFacade()).GetPromotionsByPromoId(promotionID);

                                //adjust the users balance if necessary
                                if (associatePromotion.KeiPointAmount > 0.0m)
                                {
                                    int success = (new UserFacade()).AdjustUserBalance(userSubscription.UserId, associatePromotion.KeiPointId, Convert.ToDouble(associatePromotion.KeiPointAmount), (ushort)Constants.CASH_TT_SUBSCRIPTIONS);
                                }
                                if (associatePromotion.FreePointsAwardedAmount > 0.0m)
                                {
                                    int success = (new UserFacade()).AdjustUserBalance(userSubscription.UserId, associatePromotion.FreeKeiPointID, Convert.ToDouble(associatePromotion.FreePointsAwardedAmount), (ushort)Constants.CASH_TT_SUBSCRIPTIONS);
                                }

                                // Give the montly Allowance now
                                if (userSubscription.MontlyAllowance > 0)
                                {
                                    (new UserFacade()).AdjustUserBalance(userSubscription.UserId, Constants.CURR_KPOINT, (double)userSubscription.MontlyAllowance, Constants.CASH_TT_SUBSCRIPTIONS);
                                    //UsersUtility.AdjustUserBalance(userSubscription.UserId, Constants.CURR_KPOINT, (double)userSubscription.MontlyAllowance, Constants.CASH_TT_SUBSCRIPTIONS);
                                    m_logger.Info("Awarded monthly allowance " + usersubscriptionid.ToString() + " " + userSubscription.MontlyAllowance.ToString());
                                }

                                // Complete order
                                StoreUtility.UpdateOrderToComplete(orderId, userSubscription.UserId);

                                //get all the pass groups associated with the subscription
                                DataRow[] matchingPassGroups = WebCache.GetPassGroupsToSubscription().Select("subscription_id = " + relatedSubscription.SubscriptionId);

                                //process for each pass group associated with the subscription
                                for (int i = 0; i < matchingPassGroups.Length; i++)
                                {
                                    //get the row and the pass group from that row
                                    DataRow drMPG = matchingPassGroups[i];
                                    int passGroupId = Convert.ToInt32(drMPG["pass_group_id"]);

                                    //does extra features and emailing based on pass type
                                    if (passGroupId == (int)KanevaGlobals.VipPassGroupID)
                                    {
                                        //if this fails keep processing
                                        try
                                        {
                                            (new UserFacade()).ChangePlayerNameColor(GetUserId(), (int)Constants.ePLAYER_NAME_COLORS.VIP);
                                            (new FameFacade()).RedeemPacket(GetUserId(), (int)PacketId.BUY_VIP_PASS, (int)FameTypes.World);
                                        }
                                        catch (Exception) { }
									    string billInterval = (userSubscription.RenewalTerm).ToString();
                                        MailUtilityWeb.SendVIPSubscriptionPurchaseEmail(GetUserId(), orderId, billInterval);
                                    }
                                    else if (passGroupId == (int)KanevaGlobals.AccessPassGroupID)
                                    {
                                        //if this fails keep processing
                                        try
                                        {
                                            // Set show mature flag per Jim W email - "I suggest that the show_mature column be set to 1 for everyone who has AP, 
                                            // then everything else will work.  That way when it comes back, we?ll be ready."
                                            userFacade.UpdateUser(userSubscription.UserId, 1);
                                        }
                                        catch (Exception) { }
									    string billInterval = (userSubscription.RenewalTerm).ToString();
                                        MailUtilityWeb.SendSubscriptionPurchaseEmail(GetUserId(), orderId, billInterval);
                                    }

                                    //add the user to the pass group table
                                    UsersUtility.AddUserToPassGroup(GetUserId(), passGroupId);
                                }


                                // Dev note, the below assumes this is a pass subscription change
                                //tickle the wok servers
                                try
                                {
                                    (new RemoteEventSender()).BroadcastItemChangeEvent(RemoteEventSender.ObjectType.PlayerPassList, RemoteEventSender.ChangeType.UpdateObject, userSubscription.UserId);
                                }
                                catch (Exception ex)
                                {
                                    m_logger.Error("Error while trying to tickle server", ex);
                                }
                            }
                        }
                    }
                }

            }
            else
            {
                // Fraud Check first...
                if (CheckFraudLimit(userId, fraudDetect, ref userErrorMessage))
                {
                    processSuccesful = cybersourceAdaptor.ProcessPayment(drOrder, drPpt, ccNumEncrypt, ccSecCodeEncrypt, Common.GetVisitorIPAddress(), ref bSystemDown, ref userErrorMessage);

                    if (processSuccesful)
                    {
                        fraudDetect.SuccessCount++;
                        td.Success = true;
                    }
                    else
                    {
                        fraudDetect.FailureCount++;
                    }
                }

                // Save some memory only store last 10, after 5 they are all kaneva error message anyway
                if (fraudDetect.TansactionList.Count < 10)
                {
                    fraudDetect.TansactionList.Add(td);
                }
                userFacade.SaveFraudDetect (userId, fraudDetect);

                // Send out Warning email?
                // Warning for to many successes
                if (processSuccesful && fraudDetect.SuccessCount == 3)
                {
                    // Send email here
                    SendFraudAlert(userId, "Fraud Alert Warning " + KanevaGlobals.SiteName + " - 3 Successful Purchases", fraudDetect);
                }

            }
            
            return processSuccesful;
		}

        /// <summary>
        /// CheckFraudLimit
        /// </summary>
        public bool CheckFraudLimit(int userId, FraudDetect fraudDetect, ref string userErrorMessage)
        {
            UserFacade userFacade = new UserFacade();

            int maxAttempts = 5;

            // Are they over the limit?
            if ((fraudDetect.SuccessCount + fraudDetect.FailureCount) >= maxAttempts)
            {
                // Send email if 5 attempts
                if ((fraudDetect.SuccessCount + fraudDetect.FailureCount) == maxAttempts)
                {
                    // Send email here
                    SendFraudAlert(userId, "Fraud Alert " + KanevaGlobals.SiteName + " - Max Daily Purchases", fraudDetect);
                }

                // Increase Failure attempt
                fraudDetect.FailureCount++;

                userErrorMessage = "There was a problem processing your order. Please contact Kaneva Support <a href=\"http://support.kaneva.com\">http://support.kaneva.com</a>";
                return false;
            }

            return true;
        }

        /// <summary>
        /// SendFraudAlert
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="subject"></param>
        /// <param name="fraudDetect"></param>
        private void SendFraudAlert(int userId, string subject, FraudDetect fraudDetect)
        {
            UserFacade userFacade = new UserFacade();
            User user = userFacade.GetUser(userId);

            FameFacade fameFacade = new FameFacade();
            UserFame userFame = fameFacade.GetUserFame(userId, (int)FameTypes.World);

            string body = "Real Name (Username) = " + user.DisplayName + " (" + user.Username + ")<br/>" +
                "Email = " + user.Email + "<br/>" + 
                "User Id = " + user.UserId.ToString () + "<br/><br/>" +
                "Signup Date = " + KanevaGlobals.FormatDateTime (user.SignupDate) + "<br/>" +
                "Fame Level = " + userFame.CurrentLevel.LevelNumber + "<br/>" +
                "<b>Transactions</b><br/><ul>";

            foreach (TransactionData transactionData in fraudDetect.TansactionList)
            {
                body += "<li>" +
                    KanevaGlobals.FormatCurrency (transactionData.Amount) +
                    " with card ending in " + transactionData.Last4Digits + 
                    " from " + transactionData.IPAddress + 
                    " at " + KanevaGlobals.FormatDateTime (transactionData.TransDate) + " was a " + (transactionData.Success ? "Success" : "Failure") + "</li>";
            }

            body += "</ul><br/><br/><b>Possible Related Acccounts</b><br/>";

            // Show related Accounts
            // Related IP info
            DataTable dtRelatedIPs = UsersUtility.GetRelatedIPsByUser(userId);

            for (int i = 0; i < dtRelatedIPs.Rows.Count; i++)
            {
                body += dtRelatedIPs.Rows[i]["username"].ToString ()  + " from IP " + dtRelatedIPs.Rows[i]["last_ip_address"].ToString () +"<br/>";
            }


            MailUtilityWeb.SendEmail("fraudalerts@kaneva.com", "fraudalerts@kaneva.com",subject, body, true, false, 3);
        }


		/// <summary>
		/// MakePayPalPayment
		/// </summary>
		/// <param name="drOrder"></param>
		/// <param name="drPpt"></param>
		/// <returns></returns>
		private bool MakePayPalPayment (int orderId, DataRow drPpt)
		{
			try
			{
				// Mark it as pending
				StoreUtility.UpdateOrderToPending (orderId);			

				// Are they puchasing a bucket or custom amount?
				if (drPpt ["point_bucket_id"] != null && drPpt ["point_bucket_id"].ToString ().Length > 0)
				{
					// Send them to PayPal
					int pointBucketId = Convert.ToInt32 (drPpt ["point_bucket_id"]);
					Response.Redirect (ResolveUrl ("~/billing/makePurchase.aspx?pb=" + pointBucketId.ToString () + "&pOID=" + orderId), false);
				}
				else
				{
					// Send them to PayPal
					Double amount = Convert.ToDouble (drPpt ["totalPoints"]);
					Response.Redirect (ResolveUrl ("~/billing/makePurchase.aspx?kpAmount=" + amount.ToString () + "&pOID=" + orderId));
				}

                return true;
			}
			catch (Exception exc)
			{
				m_logger.Error ("Error sending user to PayPal", exc);
                return false;
            }
		}

        /// <summary>
        /// GetFeaturedItemPrice
        /// </summary>
        /// <returns></returns>
        private Double GetFeaturedItemPrice()
        {
            // Free for an admin
            if (IsAdministrator())
            {
                return 0;
            }
            else
            {
                return Constants.C_FEATURED_ASSET_PRICE;
            }
        }

        private Subscription GetSubscriptionByOrderID()
        {
            Subscription subscription = new Subscription();

            try
            {
                DataRow drOrder = StoreUtility.GetOrder(Convert.ToInt32(Request["orderId"]), GetUserId());
                int promotionID = Convert.ToInt32(drOrder["point_bucket_id"]);
                subscription = (new SubscriptionFacade()).GetSubscriptionByPromotionId((uint)promotionID);
            }
            catch (Exception)
            {
            }

            return subscription;
        }

        /// <summary>
        /// Is subscription - helper function to check and see if the order is for a subscription
        /// </summary>
        /// <returns></returns>
        private bool IsSubscription()
        {
            bool isSubscription = false;
 
            // Free for an admin
            if (Request.QueryString["pass"] != null && Request.QueryString["pass"].ToString() == "true")
            {
                isSubscription = true;
            }
            else
            {
                //check for a subscription object to make sure user isnt removing the value from the URL
                Subscription subscript = GetSubscriptionByOrderID();

                if (subscript.SubscriptionId > 0)
                {
                    isSubscription = true;
                }
            }

            return isSubscription;
        }

        /// <summary>
		/// ShowMessage
		/// </summary>
		/// <param name="strMessage"></param>
		private void ShowMessage (string strMessage)
		{
			lblErrors.Text = strMessage;
		}

        /// <summary>
        /// VerifyUserAccess
        /// </summary>
        private void VerifyUserAccess ()
		{
			// They must be logged in
			if (!Request.IsAuthenticated)
			{
				Response.Redirect (GetLoginURL ());
			}

            // if access pass purchase, must be 18 or older
            if (promotionIsSubscription)
            {
                if (!VerifyMaturePassEligibility())
                {
                    m_logger.Warn("User " + GetUserId() + " tried to process orderId " + Request["orderId"].ToString() + " from IP " + Common.GetVisitorIPAddress() + ".  Order is for Adult Pass and user is UNDER 18.");
                    Response.Redirect("~/mykaneva/underage.aspx");
                }
            }
		}

        private bool VerifyMaturePassEligibility()
        {
            bool showMaturePasses = true;

            SubscriptionFacade subFac = new SubscriptionFacade();

            //get the Pass in question
            Subscription subscript = GetSubscriptionByOrderID();

            //get all the pass groups associated with the subscription
            DataTable passGroups = subFac.GetPassGroupsAssociatedWSubscription(subscript.SubscriptionId);

            //process for each pass group associated with the subscription
            foreach (DataRow row in passGroups.Rows)
            {
                int passGroupId = Convert.ToInt32(row["pass_group_id"]);
                if ((passGroupId == (int)KanevaGlobals.AccessPassGroupID) && (!KanevaWebGlobals.CurrentUser.IsAdult))
                {
                    showMaturePasses = false;
                }
            }

            return showMaturePasses;
        }

        /// <summary>
        /// VerifyOrder
        /// </summary>
        private void VerifyOrder ()
		{
			if (Request ["orderId"] == null)
			{
				Response.Redirect ( "~/mykaneva/managecredits.aspx" );
			}

			try
			{
				int orderId = Convert.ToInt32 (Request ["orderId"]);			
				VerifyOrder (StoreUtility.GetOrder (orderId, GetUserId()));
			}
			catch
			{
				Response.Redirect ( "~/mykaneva/managecredits.aspx" );
			}
		}
        /// <summary>
        /// VerifyOrder
        /// </summary>
        private void VerifyOrder (DataRow drOrder)
		{
            bool err = false;

            try				
			{
				// Verify user is owner of this order		  
				if (drOrder == null)
				{
					// User may be trying to view someone else's order
					m_logger.Warn ("User " + GetUserId () + " tried to view orderId " + Request ["orderId"].ToString () + " from IP " + Common.GetVisitorIPAddress());
					err = true;
				}

				if (Convert.ToInt32(drOrder["transaction_status_id"]) != (int) Constants.eORDER_STATUS.CHECKOUT)
				{
					m_logger.Warn ("User " + GetUserId () + " tried to view orderId " + Request ["orderId"].ToString () + " from IP " + Common.GetVisitorIPAddress() + ".  Order does not have a status of CART.");
					err = true;
				}
			}
			catch
			{
                err = true;	
			}

            if (err)
                Response.Redirect("~/mykaneva/managecredits.aspx");
		}

        /// <summary>
        /// AddPaymentDetails
        /// </summary>
        private void AddPaymentDetails (string ccNumLastFourDigits)
		{
			int orderId = Convert.ToInt32 (Request ["orderId"]);			
			DataRow drOrder = StoreUtility.GetOrder (orderId);

			int pointTransactionId = Convert.ToInt32 (drOrder ["point_transaction_id"]);

			// Update the payment method
			//StoreUtility.UpdatePointTransaction (pointTransactionId, Constants.ePAYMENT_METHODS.KANEVA_CYBERSOURCE);

			// Add the new credit card
			int billingInfoId = UsersUtility.InsertBillingInfo (KanevaWebGlobals.CurrentUser.UserId, 
				Constants.eBILLING_INFO_TYPE.PROFILE,"", 
				Server.HtmlEncode (txtNameOnCard.Text), 
				KanevaGlobals.Encrypt (Server.HtmlEncode (ccNumLastFourDigits)), 
				Server.HtmlEncode (drpCardType.SelectedItem.Text), 
				Server.HtmlEncode (drpMonth.SelectedValue), "", 
				Server.HtmlEncode (drpYear.SelectedValue), 0);

			// Update purchase transaction
			StoreUtility.UpdatePointTransactionBillingInfoId (pointTransactionId, billingInfoId);
		}

        private void LoadCCYears ()
        {
            int currYear = DateTime.Now.Year;

            for (int i = currYear; i < currYear + 12; i++)
            {
                drpYear.Items.Add (new ListItem (i.ToString (), i.ToString ()));
            }
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// The make purchase event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnMakePurchase_Click (object sender, EventArgs e) 
		{			
			VerifyUserAccess();
            if (!KanevaWebGlobals.CurrentUser.HasWOKAccount)
            {
                Response.Redirect ("~/error.aspx?error=" + (int) Constants.eERROR_TYPE.INVALID_ACCOUNT);
            }

            VerifyOrder ();

 			try
			{
                // Get the order
                int orderId = Convert.ToInt32 (Request["orderId"]);
                DataRow drOrder = StoreUtility.GetOrder (orderId);
                int pointTransactionId = Convert.ToInt32 (drOrder ["point_transaction_id"]);

                // Get the payment type
                if (radPmtCC.Checked)
                {
                    Page.Validate ();
                    if (!Page.IsValid)
                    {
                        return;
                    }

                    AddPaymentDetails (txtCardNumber.Text.Substring (txtCardNumber.Text.Length - 4));
                }
                else if (radPmtPP.Checked && !IsSubscription())
                {
                    StoreUtility.UpdatePointTransaction(pointTransactionId, Constants.ePAYMENT_METHODS.PAYPAL);
                }
                else
                {
                    ShowMessage("Please correct the following errors:<br><br>Subscriptions cannot be purchased with Pay Pal.");
                    return;
                }
			
				int userId = GetUserId ();
				string userErrorMessage = "";

				int orderBillingInformationId = 0;
				int billingInformationId = 0; 
				int addressId = 0;
                bool userHadThisBefore = false;
	
                // They are purchasing credit
				DataRow drPpt = StoreUtility.GetPurchasePointTransaction (pointTransactionId);

				// Is it PayPal?
				int paymentMethodId = Convert.ToInt32 (drPpt ["payment_method_id"]);

				if (paymentMethodId.Equals ((int) Constants.ePAYMENT_METHODS.PAYPAL) && !IsSubscription())
				{
					// PayPal
					if (!MakePayPalPayment (orderId, drPpt))
					{
						ShowMessage ("Please correct the following errors:<br><br>Error processing order for PayPal");
						return;
					}
				}
				else if (paymentMethodId.Equals ((int) Constants.ePAYMENT_METHODS.KANEVA_CYBERSOURCE))
				{
                    string ccNumEncrypt = KanevaGlobals.Encrypt (txtCardNumber.Text);
                    string ccSecCodeEncrypt = KanevaGlobals.Encrypt (txtSecurityCode.Text);
                   

//                    AddPaymentDetails (txtCardNumber.Text.Substring (txtCardNumber.Text.Length - 4));
																	   
					// Did we already copy the billing information over to history?
					if (!drPpt ["order_billing_information_id"].Equals (DBNull.Value))
					{
						orderBillingInformationId = Convert.ToInt32 (drPpt ["order_billing_information_id"]);
					}

					// Payment info and address are required here for Cybersource transactions				   
					if (drPpt ["address_id"].Equals (DBNull.Value) || drPpt ["billing_information_id"].Equals (DBNull.Value))
					{
						ShowMessage ("Please correct the following errors:<br><br>Credit card and billing address are required to continue this purchase");
						return;
					}

					billingInformationId = Convert.ToInt32 (drPpt ["billing_information_id"]); 
					addressId = Convert.ToInt32 (drPpt ["address_id"]);

					// Copy payment/billing address to order history
					orderBillingInformationId = StoreUtility.CopyBillingInfoToOrder (orderBillingInformationId, billingInformationId, addressId);

					// Update the purchase_transaction order_billing_id
					StoreUtility.UpdatePointTransactionOrderBillingInfoId (pointTransactionId, orderBillingInformationId);

					// Get it again to update the orderBillingInformationId to be used in the next call
					drPpt = StoreUtility.GetPurchasePointTransaction (pointTransactionId);

					// CyberSource
					if (!MakeCyberSourcePayment (drOrder, drPpt, ccNumEncrypt, ccSecCodeEncrypt, ref userErrorMessage, ref userHadThisBefore))
					{
						// Failure
						ShowMessage ("Please correct the following errors:<br><br>" + userErrorMessage);
						return;
					}

                    // if there are wok items included in the order, add
                    // them to the users inventory
                    if(!userHadThisBefore)
                    {
                        StoreUtility.AddOrderItemsToUserInventory (orderId, Convert.ToInt32 (drOrder["point_bucket_id"]), userId, KanevaWebGlobals.CurrentUser.Gender);
                    }

                    //handle emails to send
                    //if (pointTransactionId == (int)KanevaGlobals.VipPassGroupID)
                    //{
                    //    MailUtilityWeb.SendVIPSubscriptionPurchaseEmail(userId, orderId);
                    //}
                    //else if (pointTransactionId == (int)KanevaGlobals.AccessPassGroupID)
                    //{
                    //    MailUtilityWeb.SendSubscriptionPurchaseEmail(userId, orderId);
                    //}
                    //else

                    //send normal email if it is not a pass type
                    if (Request.QueryString["pass"] == null)
                    {
                        // Successfull k-point purchase here
                        MailUtilityWeb.SendKpointPurchaseEmail(userId, orderId);
                    }

                    Response.Redirect (ResolveUrl ("~/checkout/orderComplete.aspx?orderId=" + orderId), false);
                }																						 
				else
				{
					ShowMessage ("<br/>Please correct the following errors:<br/><br/>Invalid Payment Method specified!");
					return;
				}
			}
			catch (Exception exc)
			{
                m_logger.Error("Error trying to process purchase order: ", exc);
                ShowMessage("<br/>An error occurred while trying to process your order.  Please retry your request.");
			}
		}
		
		#endregion

		#region Declerations

        protected Label lblErrors, lblFreePeriod, lblSUBPrice, lblBillingPeriod, lblSUBPriceNT, lblBillingPeriodNT;
		protected Label			lblKPointAfterPurchase;
        protected Literal litPostBackRef;
        private bool promotionIsSubscription = false;

		protected Button		btnMakePurchase;
		protected TextBox		txtNameOnCard, txtCardNumber, txtSecurityCode;
		protected DropDownList	drpMonth, drpYear;

		protected HtmlImage				imgMakePurchase;
        protected HtmlAnchor            aChangeAddress, aEditOrder, aPopUpEnterAddress;
        protected HtmlTable             tblpp, tblcc;
        protected HtmlTableRow trPaypal;
		protected HtmlContainerControl	spnFullName, spnAddress1, spnAddress2, spnCity, spnState, spnZip, spnCountry, spnPhone;
		protected HtmlContainerControl	spnPrice, spnTax, spnTotal, spnItemDescription;
        protected HtmlContainerControl spnCreditBalance, spnCreditPurchasing, spnNewCreditBalance, divPassessLegal2NoTrial, divPassessLegalNoTrial, divPassessLegal, divPassessLegal2, divLegal;
        protected HtmlInputRadioButton radPmtCC, radPmtPP;
        protected HtmlContainerControl divAllowance, divFreeRewards, divFreeCredits;

		protected KlausEnt.KEP.Kaneva.WebControls.CardTypesListBox drpCardType;

		protected CheckoutWizardNavBar ucWizardNavBar;

		// Logger
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
