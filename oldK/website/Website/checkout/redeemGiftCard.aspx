<%@ Page language="c#" Codebehind="redeemGiftCard.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.redeemGiftCard" %>


<link href="../css/themes-join/template.css" rel="stylesheet" type="text/css">	
<link href="../css/themes-join/join_dp3d.css" rel="stylesheet" type="text/css">	


<table cellpadding="0" cellspacing="0" border="0" width="790" align="center">
	<tr>
		<td colspan="3"><img runat="server" src="~/images/spacer.gif" width="1" height="14" id="Img12"/></td>
	</tr>
	<tr>
		<td colspan="3">
			<table  border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="frTopLeft"></td>
					<td class="frTop"></td>
					<td class="frTopRight"></td>
				</tr>
				<tr>
					<td class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" id="Img13" width="1" height="1" /></td>
					<td valign="top" class="frBgIntMembers">
						<table width="100%" border="0" cellpadding="0" cellspacing="0" >
							<tr>
								<td width="770" align="left"  valign="top">
								
									<table cellpadding="0" cellspacing="0" border="0"  width="100%">
										<tr>
											<td valign="top">
												<div id="templateHeader"><img runat="server" src="~/images/header_border.gif" alt="header_border.gif" width="768" height="120" border="0" id="Img14"><h1>Kaneva Join Process</h1></div>
											</td>
										</tr>
										<tr>
											<td width="790" valign="top">
												
												<table border="0" cellspacing="0" cellpadding="0" width="100%">
													<tr>
														<!-- START WIZARD CONTENT -->
														<td valign="top" align="center">
															
															<table border="0" cellspacing="0" cellpadding="0" width="670" align="center" class="wizform">
																<tr>
																	<td width="670">
																	
																		<h2 style="margin-left:0px;">Redeem your Dance Party 3D Game Card</h2>
                                                                        <div id="divValidated" runat="server" visible="false" style="margin-left:0px;"><span >Your Email address has been validated!</span></div>
																		<table runat="server" cellpadding="0" cellspacing="0" border="0" width="100%">
																			<tr><td>
																				<asp:ValidationSummary ShowMessageBox="False" ShowSummary="True" Class="formError" id="valSum" DisplayMode="BulletList" runat="server" HeaderText="Please correct the following errors:"/>
																				<asp:label cssclass="alertmessage bold" id="lblErrors" runat="server"/>
																			</td></tr>
																		</table>

																		<div ID="divLoading">
																			<table BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="100%" HEIGHT="260px">
																				<tr><th CLASS="loadingText" ID="divLoadingText">Loading...</th></tr>
																			</table>
																		</div>

																		<div id="divData" style="DISPLAY:none">
																		<br/><br/>
																		<table cellSpacing="0" cellPadding="0" style="width:99%" border="0">
																		<tr>
																			<td valign="top">
																				To redeem your card:<br/><br/>
																				<ol>
																					<li>Redeem prepaid card by scratching off the designated grey area on the back of the card to find your unique code underneath.</li><br/><br/>
																					<li>Then submit the 10 digit pin.</li><br/>
																				</ol>
																			</td>
																			<td width="80"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
																			<td valign="top" width="240">
																				<img runat="server" src="~/images/checkout/Card_Back_50percent.jpg" id="Img1"/><br/><br/>
																				
																				<table cellSpacing="0" cellPadding="0" border="0">
																					<tr>
																						<td>
																							<asp:TextBox ID="txtPinNumber" style="width:200px" MaxLength="40" runat="server"/>
																							<asp:RequiredFieldValidator ID="rftxtPinNumber" ControlToValidate="txtPinNumber" Text="*" ErrorMessage="Pin Number is a required field." Display="Dynamic" runat="server"/>
																						</td>
																					</tr>
																					<tr>
																						<td class="note" colspan="2">Type the 10-digit PIN exactly as it appears.</td>
																					</tr>
																				</table>																				
																				
																				<br/><br/>
																				<asp:button id="btnRedeemCard" runat="Server" CausesValidation="False" onClick="btnRedeemCard_Click" class="Filter2" Text="   redeem   "/>
																				<br/><br/>
																			</td>
																		</tr>
																		</table>
																		</div>


																		 
																	</td>
																</tr>
															</table>
															
														</td>
														
														<!-- END WIZARD CONTENT -->
													</tr>
												</table>
											
											</td>
											
										</tr>
										
									</table>
											
								</td>
							</tr>
							
						</table>
					</td>
					<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" id="Img15" width="1" height="1" /></td>
				</tr>
				<tr>
					<td class="frBottomLeft"></td>
					<td class="frBottom"></td>
					<td class="frBottomRight"></td>
				</tr>
			</table>				
		</td>
	</tr>
</table>
<br/><br/><br/><br/>
