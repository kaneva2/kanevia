///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for launchIris.
	/// </summary>
	public class launchIris : MainTemplatePage
	{
		protected launchIris () 
		{
			Title = "Item Download Ready";
		}

		private void Page_Load (object sender, System.EventArgs e)
		{
			int assetId = 0;
			string assetTypeId = "0";
			string free = "0";

			int orderId = 0;
			if (Request ["orderId"] != null)
			{
				orderId = Convert.ToInt32 (Request ["orderId"]);
			}
			
			if (Request ["assetId"] != null)
			{
				assetId = Convert.ToInt32 (Request ["assetId"]);

				DataRow drAsset = StoreUtility.GetAsset (assetId);
				assetTypeId = drAsset ["asset_type_id"].ToString ();

				// Is it a game?
				if (StoreUtility.IsKanevaGame (drAsset))
				{
					// Show the download to the launcher
					Response.Redirect (ResolveUrl ("~/checkout/launchGame.aspx?assetId=" + assetId));
					return;
				}

				// Mark it as free?
				if (Convert.ToDouble (drAsset ["amount"]).Equals (0.0))
				{
					if (Request ["free"] != null)
					{
						free = Request ["free"].ToString ();
					}
				}
				else
				{
					// Get download stats
					if (Request.IsAuthenticated)
					{
						tblStats.Visible = true;
						try
						{
							DataRow drOrder = StoreUtility.GetActiveAssetOrderRecord (KanevaWebGlobals.GetUserId (), assetId);

							int numberOfDownloadsAllowed = Convert.ToInt32 (drOrder ["number_of_downloads_allowed"]);
							int numberOfDownloads = Convert.ToInt32 (drOrder ["number_of_downloads"]);

							lblDownloadUntil.Text = KanevaGlobals.FormatDateTime (Convert.ToDateTime (drOrder ["download_end_date"]));
							lblAllowedDownloads.Text = numberOfDownloadsAllowed.ToString (); 
							lblDownloadsRemaining.Text = (numberOfDownloadsAllowed - numberOfDownloads).ToString ();
						}
						catch (Exception)
						{
							tblStats.Visible = false;
						}
					}
				}

////				// Show the torrent link
////				if (drAsset ["torrent_id"] != DBNull.Value)
////				{
////					DataRow drTorrent = StoreUtility.GetTorrent (Convert.ToInt32 (drAsset ["torrent_id"]));
////
////					if (drTorrent != null)
////					{
////						// Show link to torrent
////						aBitTorrent.HRef = ResolveUrl ("~/checkout/downloadTorrent.aspx?orderId=" + orderId + "&assetId=" + assetId);
////						//aBitTorrent.InnerText = drTorrent ["torrent_name"].ToString ();
////					}
////				}

				// Set up KML Link
				lblVersion.Text = System.Configuration.ConfigurationManager.AppSettings ["KMLVersion"].ToString ();

                string KMLFilename = System.Configuration.ConfigurationManager.AppSettings["KMLFilename"].ToString();
				string filePath = Server.MapPath("~/download/" + KMLFilename);

				if (System.IO.File.Exists (filePath))
				{
					aInstallIris.HRef = ResolveUrl ("~/download/" + KMLFilename);
				}
				else
				{
					aInstallIris.HRef = "javascript:alert('File was not found on server.');";
				}
			}

			//Literal litLaunch = new Literal ();
			//litLaunch.Text = "parent.iframeTest.location.replace('kaneva:assetId=" + assetId + "&assetTypeId=" + assetTypeId + "&free=" + free + "');\n" +
			//	"location.replace('launchIrisResults.aspx?orderId=" + orderId + "&assetId=" + assetId + "');";
			//plLaunch.Controls.Add (litLaunch);

			aLaunchIris.HRef = "kaneva:assetId=" + assetId + "&assetTypeId=" + assetTypeId + "&free=" + free;
		}

		/// <summary>
		/// Continue Shopping click
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnContinue_Click (object sender, EventArgs e)
		{
			
		}

		protected PlaceHolder plLaunch;
		protected HtmlAnchor aLaunchIris;
		protected HtmlAnchor aBitTorrent;

		protected Label lblDownloadUntil, lblAllowedDownloads, lblDownloadsRemaining;

		protected HtmlTable tblStats;
		protected HtmlAnchor aInstallIris;
		protected Label lblVersion;

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
