<%@ Page language="c#" Codebehind="billingInfo.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.billingInfo" %>
<%@ Register TagPrefix="Kaneva" TagName="WizardNavBar" Src="../usercontrols/checkoutwizardnavbar.ascx" %>

<script src="../jscript/prototype.js" type="text/javascript"></script>

<script type="text/javascript">
function CountryOnChange (val)
{
	if (val != '')
	{
		if (val=='US' || val=='CA')
		{
			$('drpState').style.display='block';
			$('txtState').style.display='none';
		}
		else
		{
			$('drpState').style.display='none';
			$('txtState').style.display='block';
		}
	}
}
</script>

<link href="../css/new.css" rel="stylesheet" type="text/css">
<link href="../css/shadow.css" rel="stylesheet" type="text/css">


<table border="0" cellspacing="0" cellpadding="0" class="newcontainer" align="center">
	<tr>
		<td>
			<table  border="0" cellpadding="0" cellspacing="0" width="100%"class="newcontainerborder">
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
				<tr>
					<td class=""><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
					<td valign="top" class="newdatacontainer">
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="968" align="left"  valign="top">
									
								
									<table cellpadding="0" cellspacing="0" border="0"  width="100%">
										<tr>
											<td width="968" valign="top">
												
												<kaneva:wizardnavbar runat="server" id="ucWizardNavBar"/>
												
											</td>
										</tr>
										<tr>
											<td width="970" valign="top">
											
												<table id="tblSelectAddress" visible="false" runat="server" border="0" cellspacing="0" cellpadding="0" width="100%">
													<tr>
														<td valign="top" align="center">
															
															<table border="0" cellspacing="0" cellpadding="0">
																<tr>
																	<td width="670">
																		
																		<div class="module whitebg fullpage">
																		<span class="ct"><span class="cl"></span></span>
																		<h1>Select Billing Address</h1>

																				
																		<asp:datalist runat="server" enableviewstate="True" showfooter="False" width="97%" id="dlAddress"
																			cellpadding="6" cellspacing="0" border="0" repeatcolumns="2" repeatdirection="Horizontal" 
																			horizontalalign="Center" cssclass="wizform nopadding">
																			
																			<itemtemplate>	   
																				
																				<table cellspacing="0" width="316" cellpadding="1" align="left" border="0" class="unselected" id="tblAddress<%# GetAddressNum(true) %>">
																					<tr>
																						<td rowspan="8" width="10" class="radiobg"><input name="rbAddress" id="<%# GetAddressNum(false) %>" onclick="SelectAddress(this.id);" type="radio" value='<%# DataBinder.Eval(Container.DataItem, "address_id")%>' /></td>
																						<td rowspan="8" width="5"></td>
																					</tr>
																					<tr><td><%# DataBinder.Eval (Container.DataItem, "name")%></td></tr>
																					<tr><td><%# DataBinder.Eval (Container.DataItem, "address1")%></td></tr>
																					<tr><td><%# DataBinder.Eval (Container.DataItem, "address2")%></td></tr>
																					<tr>
																						<td align="left">
																							<table border="0" cellspacing="0" cellpadding="0" align="left" style="margin:0 0;">
																								<tr>
																									<td colspan="2"><%# DataBinder.Eval (Container.DataItem, "city")%></td>
																									<td width="20"></td>
																									<td colspan="2"><%# DataBinder.Eval (Container.DataItem, "state_name")%></td>
																									<td width="20"></td>
																									<td><%# DataBinder.Eval (Container.DataItem, "zip_code")%></td>
																								</tr>
																							</table>
																						</td>
																					</tr>
																					<tr><td><%# DataBinder.Eval (Container.DataItem, "country_name")%></td></tr>
																				</table>
																				
																			
																			</itemtemplate>
																			
																		</asp:datalist>
																		
																		<table border="0" cellspacing="0" cellpadding="5" width="97%">
																			<tr>
																				<td class="alertmessage"><strong><span id="spnMessage" runat="server" /></strong></td>
																				<td align="right" width="150">
																					<asp:imagebutton id="btnContinue" runat="Server" causesvalidation="False" onclick="btnContinue_Click" 
																						alternatetext="Continue" width="135" height="41" border="0" imageurl="~/images/wizard_btn_continue.gif" />
																				</td>
																			</tr>
																		</table>
																		<span class="cb"><span class="cl"></span></span>
																		</div>
																	
																	</td>
																</tr>
															</table>
															
														</td>
													</tr>
												</table>
											
												<table border="0" cellspacing="0" cellpadding="0" width="100%">
													<tr>
														<!-- START WIZARD CONTENT -->
														<td valign="top" align="center">
															
															<table border="0" cellspacing="0" cellpadding="0">
																<tr>
																	<td width="670">
																		
																		<div class="module fullpage">
																		<span class="ct"><span class="cl"></span></span>
																		<h1>Add Billing Address</h1>
																		<div style="width:180px; position:absolute; margin-left:240px; left:240px; top:12px; float:right;" id="divPaypalTop" runat="server" visible="false">
																			<img style="float:left;margin:3px 5px 0 0" src="~/images/paypal_small.gif" width="69" height="22" runat="server" />
																			<span style="font-weight:bold;float:right">Using Paypal?<br /><asp:linkbutton runat="server" causesvalidation="false"  id="lbPaypal" onclick="lbPaypal_Click">Skip this step >></asp:linkbutton></span></div>
																		
																		<p align="left" id="pDesc" runat="server" style="padding-top:20px; padding-left:14px;text-align:left;">Billing address must match billing address of credit card to be used.</p>
																		<asp:validationsummary cssclass="errBox" id="valSum" runat="server" showmessagebox="False" showsummary="True"
																			displaymode="BulletList" style="margin-top:10px;width:600px;" headertext="Please correct the following error(s):" forecolor="black"></asp:validationsummary>
																			<asp:customvalidator id="cvBlank" runat="server" display="None" enableclientscript="False"></asp:customvalidator>
																		<br />			  
                                                                        
																		<table border="0" cellspacing="0" cellpadding="0" width="460" align="center" class="wizform">
																			<tr>																												  
																				<td align="left">
																					<table id="Table11" cellspacing="0" cellpadding="0" width="470" align="center" border="0">					  
																						<tr>
																							<td>Full Name*<br>
																							<asp:textbox id="txtFullName" class="biginput" style="width:455" maxlength="100" runat="server"/>
																							<asp:requiredfieldvalidator id="rfUsername" controltovalidate="txtFullName" text="" errormessage="Full Name is a required field." display="None" runat="server"/>
																							</td>
																						</tr>
																						
																						<tr><td class="formspacer"></td></tr>
																						
																						<tr>
																							<td>Address 1*<br>
																							<asp:textbox id="txtAddress1" class="biginput" style="width:455px" maxlength="100" runat="server"/>
																							<asp:requiredfieldvalidator id="rfEmail" controltovalidate="txtAddress1" text="" errormessage="Address1 is a required field." display="None" runat="server"/>
																							</td>
																						</tr>
																						
																						<tr><td class="formspacer"></td></tr>
																						
																						<tr>
																							<td>Address 2<br>
																							<asp:textbox id="txtAddress2" class="biginput" style="width:455px" maxlength="80" runat="server"/>
																							</td>
																						</tr>
																						
																						<tr><td class="formspacer"></td></tr>
																						
																						<tr>
																							<td>City*<br>
																								<asp:textbox id="txtCity" class="biginput" style="width:200px;" maxlength="100" runat="server"/>
																								<asp:requiredfieldvalidator id="rftxtCity" controltovalidate="txtCity" text="" errormessage="City is a required field." display="None" runat="server"/>
																							</td>
																						</tr>				
																						
																						<tr><td class="formspacer"></td></tr>
																						
																						<tr>
																							<td>State/Province/Region**<br>
																								<asp:textbox runat="server" class="biginput" id="txtState" style="width:200px;display:none;" maxlength="100"></asp:textbox>
																								<asp:dropdownlist runat="server" class="biginput" id="drpState" style="width:120px;margin-bottom:2px;"></asp:dropdownlist>
																								<asp:requiredfieldvalidator id="rfdrpState" controltovalidate="drpState" text="" errormessage="State is a required field when country is United States or Canada." display="None" runat="server"/>
																								<asp:requiredfieldvalidator id="rftxtState" controltovalidate="txtState" text="" errormessage="State/Province/Region is a required field." display="None" runat="server"/>
																							</td>
																						</tr>				
																						
																						<tr><td class="formspacer"></td></tr>
																						
																						<tr>
																							<td>Postal Code/ZIP*<br>
																								<asp:textbox id="txtPostalCode" class="biginput" style="width:120px;" size="9" maxlength="25" runat="server"/>
																								<asp:requiredfieldvalidator id="rftxtPostalCode" controltovalidate="txtPostalCode" text="" errormessage="Postal code is a required field." display="None" runat="server"/><img src="images/spacer.gif" height="1" width="15" />
																							</td>
																						</tr>
																						<tr><td class="formspacer"></td></tr>
																						
																						<tr>
																							<td>Country*<br>
																								<asp:dropdownlist runat="server" class="biginput" id="drpCountry" style="width:300px;" onchange="CountryOnChange(this.options[this.selectedIndex].value);"></asp:dropdownlist>
																								<asp:requiredfieldvalidator id="rfdrpCountry" controltovalidate="drpCountry" text="" errormessage="Country is a required field." display="None" runat="server"/>
																							</td>
																						</tr>
																						<tr><td class="formspacer"></td></tr>
																						<tr>
																							<td>Phone Number*<br>
																								<asp:textbox id="txtPhoneNumber" class="biginput" style="width:200px" maxlength="15" runat="server"/>
																								<asp:requiredfieldvalidator id="rftxtPhoneNumber" controltovalidate="txtPhoneNumber" text="" errormessage="Phone Number is a required field." display="None" runat="server"/>
																							</td>
																						</tr>
																						
																						<tr><td class="formspacer"></td></tr>
																						<tr>
																							<td class="note">* Indicates Required Field.<br>** Indicates required fields for United States and Canada only.</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																		</table>
																		
																		<br><br>
																		
																		<table border="0" cellspacing="0" cellpadding="5" width="97%">
																			<tr>
																				<td align="right">
																					<div id="divPaypalBot" runat="server" visible="false">
																					<span style="font-weight:bold;">Using Paypal?&nbsp;&nbsp;</span><br />
																					<asp:linkbutton runat="server" causesvalidation="false"  id="lbPaypal_Bottom" onclick="lbPaypal_Click">Skip this step >></asp:linkbutton></div>
																				</td>
																				<td align="right" width="250">					   
																					<asp:imagebutton id="btnSaveContinue" runat="Server" causesvalidation="False" onclick="btnSaveContinue_Click" 
																						alternatetext="Save and Continue" width="230" height="41" border="0" imageurl="~/images/wizard_btn_savecontinue.gif" />
																				</td>								  
																			</tr>
																		</table>
																		<span class="cb"><span class="cl"></span></span>
																		</div>
																	
																	</td>
																</tr>
															</table>
															
														</td>
														
														<!-- END WIZARD CONTENT -->
													</tr>
												</table>
											
											</td>
											
										</tr>
										
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td class=""><img runat="server" src="~/images/spacer.gif" id="Img5" width="1" height="1" /></td>
				</tr>
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
				
			</table>
		</td>
	</tr>
</table>
<script language="javascript">

var tblCount = <asp:literal id="litAddressCount" runat="server" />;
var tblArray;

if (tblCount > 0)
{
	tblArray = new Array(tblCount);
	
	for (var i=0; i<tblArray.length; i++)
	{
		tblArray[i] = document.getElementById('tblAddress'+(i+1));
	}
	
	var tbl = document.getElementById('tblAddress1');
	if (tbl != null)
	{
		tbl.className = 'selected';
		document.getElementById('1').checked = true;
	}
}

function SelectAddress(j)
{
	for (var i=0; i<tblArray.length; i++)
	{
		tblArray[i].className = 'unselected';
	}
	tblArray[j-1].className = 'selected';
}

function IsIE ()
{
	return ((navigator.userAgent.indexOf('MSIE') != -1) && (navigator.userAgent.indexOf('Win') != -1));
}

if (IsIE)
{
	if ($('divPaypalTop'))
		$('divPaypalTop').style.left = '240 px';
}
else
{
	if ($('divPaypalTop'))
		$('divPaypalTop').style.left = '480 px';
}
</script>

		