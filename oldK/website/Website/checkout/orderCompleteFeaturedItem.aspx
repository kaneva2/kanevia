<%@ Page language="c#" Codebehind="orderCompleteFeaturedItem.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.orderCompleteFeaturedItem" %>
<BR><BR><BR><BR>
<table cellpadding="5" cellspacing="0" border="0" width="730" ID="Table1">
	<tr>
		<td>
			<center>
				<table cellpadding="0" cellspacing="20" border="0" width="700">
					<tr>
						<td class="bodyText" align="center">
							<b style="font-size: 20px;line-height:23px;">Featured Item Order Complete!</b><br/><br>
							Thank you for your order, '<asp:label id="lblFeatured" runat="server"/>' will now be listed in the featured items rotation.<br><br>
						</td>
					</tr>
				</table>
			</center>
		</td>
	</tr>
</table>
	