<%@ Page language="c#" Codebehind="launchIris.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.launchIris" %>
<%@ Register TagPrefix="Kaneva" TagName="HeaderText" Src="../usercontrols/HeaderText.ascx" %>

<Kaneva:HeaderText runat="server" Text="Item Ready for Download!"/>

<script>

function checkBrowser (sbrowser)
{
//possible strings 'konqueror''safari''omniweb''opera''webtv''icab''msie''compatible'
	return navigator.userAgent.toLowerCase ().indexOf (sbrowser) + 1;
}

function OnLoad()
{
	var divWindows3	= document.getElementById ('divWindows3');
	var divIrisTest = document.getElementById ('divIrisTest');
	var divTorrent = document.getElementById ('divTorrent');

	// If we are not on windows, don't even attemp to launch Iris, Iris is windows only.
	if (navigator.platform.indexOf ("Win") > -1)
	{		
		// Are we IE on XP SP1?
		//if (checkBrowser ('msie') && navigator.userAgent.toLowerCase().indexOf("windows nt 5.1")>-1 && navigator.appMinorVersion.toLowerCase().indexOf("sp1")>-1)
		//{
			//alert ('IE XP SP1 detected, this browser fails trying to launch unknown protocols.');
			// Show the message below
			divWindows3.style.display = '';
		//}
		//else
		//{
		//	<asp:PlaceHolder id="plLaunch" runat="server"/>
		//}
	}
	else
	{
		// NOT WINDOWS
		divWindows3.style.display = 'none';
	}
	
	divIrisTest.style.display = 'none';
	divTorrent.style.display = '';
}
</script>

	
	<table cellpadding="0" cellspacing="10" border="0" width="720" style="border-top: 1px solid #cccccc;border-bottom: 1px solid #cccccc">
		<tr>
			<td class="bodyText" HEIGHT="460px" VAlign="Top">
				<table runat="server" id="tblStats" Visible="False" width="700" cellpadding="0" cellspacing="5" border="0">
					<tr>
						<td class="dateStamp" align="left" width="35%">
							<b>Start downloading before:</b>
						</td>
						<td class="dateStamp" align="left" width="65%">
							<asp:label id="lblDownloadUntil" runat="server"/>
						</td>
					</tr>
					<tr>
						<td class="dateStamp" align="left" width="35%">
							<b>Number of allowed downloads:</b>
						</td>
						<td class="dateStamp" align="left" width="65%">
							<asp:label id="lblAllowedDownloads" runat="server"/>
						</td>
					</tr>
					<tr>
						<td class="dateStamp" align="left" width="35%">
							<b>Number of downloads remaining:</b>
						</td>
						<td class="dateStamp" align="left" width="65%">
							<asp:label id="lblDownloadsRemaining" runat="server"/>
						</td>
					</tr>
				</table>
				<BR/>
				<div ID="divWindows3" style="DISPLAY:none">
					<span class="loadingText" style="color:black;">&nbsp;&nbsp;<i>Choose the download that is best for you:</i></span><br><br>
					<table width="700" cellpadding="0" cellspacing="10" border="0">
						<tr>
							<td class="dateStamp" width="100%" align="left">
								&nbsp;<BR/>
								<span style="COLOR: gray; font-size: 18px; font-weight: bold;">Easy Download: </span><a runat="server" id="aLaunchIris" style="COLOR: #3258ba; font-size: 18px;">Get Item Using Kaneva Media Launcher</a>&nbsp;&nbsp;&nbsp;<br/>
								<img runat="server" src="~/images/spacer.gif" width="1" height="15"/><span style="COLOR: red; font-size: 12px;font-weight: bold;">*Requires Kaneva Media Launcher <asp:Label id="lblVersion" runat="server"/>. <a runat="server" id="aInstallIris" target="_blank" style="COLOR: red; font-size: 12px;font-weight: bold;">Install now...</a>
								<BR/>
							</td>
						</tr>
						<tr>
							<td class="bodyText" width="100%" align="left">
								About Kaneva Media Launcher<br/><br/>
								<li>The easy way to get media to your Windows computer.<br/>
								<li>Easily and reliably receive and manage content you have downloaded and purchased.<br/>
								<li>Runs in the system tray.<br/>
								<li>Features secure transfers, error correction, and the auto-resume if the connection is lost.<br/>
								<li>Questions? Get more details from the <a href="http://docs.kaneva.com/bin/view/Public/KanevaIrisUserGuide" class="LnavText" target="_resource">Kaneva Media Launcher User Guide</a>.<br/>
								
							</td>
						</tr>
					</table><br><br>
				</div>
				<div ID="divTorrent" style="DISPLAY:none">
					<table width="700" cellpadding="0" cellspacing="10" border="0">
						<tr>
							<td class="datestamp" align="left" width="100%">
								&nbsp;<BR/>
								<span style="COLOR: gray; font-size: 18px; font-weight: bold;">Power Download: </span><a runat="server" id="aBitTorrent" style="COLOR: #3258ba; font-size: 18px;">Get Item Using BitTorrent</a>&nbsp;&nbsp;&nbsp;<br/>
								<img runat="server" src="~/images/spacer.gif" width="1" height="15"/><span style="COLOR: red; font-size: 12px;font-weight: bold;">*Requires BitTorrent client. We recommend the <a href="http://azureus.sourceforge.net/index.php" style="COLOR: red;" target="_resource">Azureus client</a></span><br/>
								<br/>
							</td>
						</tr>
						<tr>
							<td colspan="2" class="bodyText" width="100%" align="left">
								About BitTorrent<br/><br/>
								<li>Use your favorite BitTorrent client software.<br/>
								<li>Use any operating system (such as Windows, Mac OS, and Linux).<br/>
								<li>Questions? You can learn more from our <a href="http://docs.kaneva.com/bin/view/Public/KanevaBitTorrentIntro" class="LnavText" target="_resource">Kaneva BitTorrent Overview</a>.<br/>
								<li>Note: For pay content you must be signed into the web site to download.<br/>
							</td>
						</tr>
						<tr>
							<td colspan="2" class="dateStamp" width="100%" align="center">
								<br/><br/>
								<a href="http://docs.kaneva.com/bin/view/Public/KanevaFaqWatchingMedia#KanevaFaqProblems" class="LnavText" target="_resource">Problems viewing video or opening or finding your item?</a>
							</td>
						</tr>
					</table><br><br>
				</div>
				<div ID="divIrisTest">
					<table BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="100%" HEIGHT="298px">
						<tr><th CLASS="loadingText" ID="divLoadingText">Please wait. Preparing your download options...</th></tr>
					</table>
				</div>
			</td>
		</tr>
	</table>
	
	<table width="700" cellpadding="0" cellspacing="10" border="0">
		<tr>
			<td width="100%" Align="right">
				<asp:button id="btnContinue" runat="Server" CausesValidation="False" onClick="btnContinue_Click" class="Filter2" Text=" continue browsing "/>
			</td>
		</tr>
	</table>
