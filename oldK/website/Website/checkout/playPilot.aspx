<%@ Page language="c#" Codebehind="playPilot.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.playPilot" %>
<br><br>
<table runat="server" id="tblNoLogin" cellpadding="0" cellspacing="0" border="0" width="560">
	<tr><td ALIGN="CENTER">
		<BR><BR><BR>
		<span class="subHead">You must be registered and signed in to access this page.</span>
	</td></tr>
</table>

<asp:panel runat="server" ID="pnlLoggedIn">

	<table cellpadding="10" cellspacing="0" border="0" width="560" style="border-bottom: 2px solid #404040;border-top: 2px solid #404040;">
	<tr>
	<td width="50%" class="bodyText">
	<span class="loadingText" style="color:black;">Please Select Your Play Option!</span><br><br>
	As a basic user of KANEVA, when choosing to play pilot games
	you can either:<br>
	<p>
	<span class="dateStamp">
	1. Pay 100 K-points per day to play this game<br><br>
	2. Buy an Enhanced KANEVA<br>&nbsp;&nbsp;&nbsp;&nbsp;subscription for $10/month, which<br>&nbsp;&nbsp;&nbsp;&nbsp;allows
	you to play ALL pilot<br>&nbsp;&nbsp;&nbsp;&nbsp;games for FREE!
	</span>
	</p>
	</ul>
	Please make your selection to the right!<br><br>
	</td>

	<td width="50%">
	<a runat="server" id="aOneDay" href="#"><img runat="server" src="~/images/button_pilot_1day.gif" border="0"/></a><br>
	<a runat="server" id="aPurchasePilot" href="~/billing/makePurchase.aspx?sub=pilot"><img runat="server" src="~/images/button_pilot_enh.gif" border="0"/></a>
	</td>
	<td></td>

	</tr>
	</table>

</asp:panel>