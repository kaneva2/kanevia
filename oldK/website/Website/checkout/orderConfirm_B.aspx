<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="orderConfirm_B.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.orderConfirm_B" %>
<%@ Register TagPrefix="Kaneva" Namespace="KlausEnt.KEP.Kaneva.WebControls" Assembly="Kaneva.WebControls" %>

<script type="text/javascript" src="../jscript/prototype.js"></script>

<link href="../css/friends.css" rel="stylesheet" type="text/css"/>
<link href="../css/new.css" rel="stylesheet" type="text/css">
<link href="../css/shadow.css" rel="stylesheet" type="text/css">
<link href="../css/temp_purchaseFlow.css" rel="stylesheet" type="text/css">

<div id="divLoading">
	<table border="0" cellspacing="0" cellpadding="0" width="500" align="center" style="margin:50px 0 260px 0;">
		<tr>
			<td>
				<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100">
					<tr>
						<td class="frTopLeft"></td>
						<td class="frTop"></td>
						<td class="frTopRight"></td>
					</tr>
					<tr>
						<td class="frBorderLeft"><img id="Img1" runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
						<td valign="top" align="center" class="frBgIntMembers">
							<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100">
								<tr><th class="frBgIntMembers" id="divLoadingText">Loading...<div style="display:none;"><img src="../images/progress_bar.gif" ></div></th></tr>
							</table>
						</td>
						<td class="frBorderRight"><img id="Img2" runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
					</tr>
					<tr>
						<td class="frBottomLeft"></td>
						<td class="frBottom"></td>
						<td class="frBottomRight"></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</div>

<div id="divData" style="display:none;">

<table border="0" cellspacing="0" cellpadding="0"  width="990" align="center">
	<tr>
		<td><img id="Img3" runat="server" src="~/images/spacer.gif" width="1" height="14" /></td>
	</tr>
	<tr>
		<td>
			<!-- new div: link to manage account settings -->
			<table  border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="frTopLeft"></td>
					<td class="frTop"></td>
					<td class="frTopRight"></td>
				</tr>
				<tr>
					<td class="frBorderLeft"><img id="Img4" runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
					<td valign="top" class="frBgIntMembers">
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="968" align="left"  valign="top">
									<table cellpadding="0" cellspacing="0" border="0"  width="100%">
										<tr>
											<td width="968" valign="top" align="center">
												
                                                <div class="progressBar">
                                                <!-- WIDTH OF ALL SPANS SHOULD ADD UP TO 958 PX -->
                                                    <ul class="payment">
                                                        <li id="liSelect" class=""><h3>Select Package</h3></li>
                                                        <li id="liPayment" class=""><h3>Payment</h3></li>
                                                        <li id="liReceipt" class=""><h3>Receipt</h3></li>
                                                    </ul>			
                                                </div>
                                                                                                
											</td>
										</tr>
										<tr>
											<td width="970" valign="top">
												<table border="0" cellspacing="0" cellpadding="0" width="100%">
													<tr>
														<!-- START WIZARD CONTENT -->
														<td valign="top" align="center">
															<table border="0" cellspacing="0" cellpadding="0">
																<tr>
																	<td width="670">
																		<div class="module whitebg">
																		<span class="ct"><span class="cl"></span></span>
																				
																		<table border="0" cellspacing="0" cellpadding="0"  align="center" class="wizform">
																			<tr>
																				<td align="left">
																					<table id="Table11" width="640" cellspacing="0" cellpadding="0" border="0">
																						<tr>
																							<td colspan="3">
																								<h1>Make Purchase</h1>
																																																		   
																								<div style="padding:0px 10px 10px 10px;margin-top:-10px;">
																									<asp:validationsummary cssclass="errBox" id="valSum" runat="server" showmessagebox="False" showsummary="True"
																										displaymode="BulletList" style="margin-top:10px;" headertext="Please correct the following error(s):" forecolor="black"></asp:validationsummary>
																									<asp:customvalidator id="cvBlank" runat="server" display="None" enableclientscript="False"></asp:customvalidator>
																								</div>																								   
																								
																								<table border="0" cellspacing="0" cellpadding="0" width="97%" align="center" class="data white nopadding" id="ordersummary">
																									<tr>
																										<td colspan="3" style="padding: 8px 0 8px 5px;"><h2>Order Summary <span class="hlink"><a runat="server" id="aEditOrder">edit order</a></span></h2></td>
																									</tr>
																									<tr class="altrow">
																										<td align="left" class="bold">Description</td>
																										<td align="right" class="bold" width="60">Price</td>
																									</tr>
																									<tr>
																										<td align="left"><span id="spnItemDescription" runat="server"/></td>
																										<td align="right">$<span id="spnPrice" runat="server"/></td>
																									</tr>
																									<tr>
																										<td align="right">Tax</td>
																										<td align="right"><span id="spnTax" runat="server">0.00</span></td>
																									</tr>
																									<tr class="altrow">
																										<td align="right" class="bold">Total</td>
																										<td align="right" class="bold">$<span id="spnTotal" runat="server" /></td>
																									</tr>
																								</table>
																								<br />
																							</td>
																						</tr>
																						<tr>
																							<td valign="top" width="260" align="left">
																								<div class="module">
																									<span class="ct"><span class="cl"></span></span>
																									<h2>Billing Address</h2>
																									<p class="note">Billing address must match billing address of credit card used.</p>
																									<table cellspacing="0" cellpadding="1" border="0" width="88%">
																										<tr><td><span id="spnFullName" runat="server"/></td></tr>
																										<tr><td><span id="spnAddress1" runat="server"/></td></tr>
																										<tr><td><span id="spnAddress2" runat="server"/></td></tr>
																										<tr>
																											<td align="left"><span id="spnCity" runat="server"/>&nbsp;
																												<span id="spnState" runat="server"/>&nbsp;&nbsp;&nbsp;<span id="spnZip" runat="server"/>
																											</td>
																										</tr>
																										<tr><td><span id="spnCountry" runat="server"/></td></tr>
																										<tr><td><span id="spnPhone" runat="server"/></td></tr>
																										<tr>
																											<td><br /><a runat="server" id="aChangeAddress">Add Address</a></td>
																										</tr>
																										<tr><td><img id="Img5" runat="server" src="~/images/spacer.gif" width="228" height="1" /></td></tr>
																									</table>
																									<span class="cb"><span class="cl"></span></span>
																								</div>
																								<br />
																								<div>
																									Your Credit Balance Summary:<br /><br />
																									<table cellpadding="0" cellspacing="4" border="0" width="98%">
																										<tr>
																											<td>Current Balance:</td>
																											<td align="right"><span id="spnCreditBalance" runat="server" /></td>
																										</tr>
																										<tr>
																											<td>Amount Purchasing:</td>
																											<td align="right" style="border-bottom:1px solid #000000;"><span id="spnCreditPurchasing" runat="server" /></td>
																										</tr>
																										<tr>
																											<td>Balance After Purchase:</td>
																											<td align="right"><span id="spnNewCreditBalance" runat="server" /></td>
																										</tr>
																									</table>
																								</div>
																							</td>
																							<td width="15"><img id="Img6" runat="server" src="~/images/spacer.gif" width="15" height="1" /></td>
																							<td align="right" valign="top" width="370">
																								<div class="module">
																									<span class="ct"><span class="cl"></span></span>
																									
                                                                                                    <!-- !r: revised area -->
                                                                                                    <!-- there is a bunch of revised css that need to be dumped into one of teh existing css. most of teh existing css are from new.css, put them there? Please add the comment: New purchase flow 021009 -->
                                                                                                    <h2>Payment Method</h2>
																									<table class="creditCard">
																										<tr>
																											<td>
																												<div id="divCreditCard" runat="server">
																													<table border="0" cellpadding="10" cellspacing="0" align="left">
																														<tr>
																															<td>Card Type: </td>
																															<td><div id="divCardType" runat="server"></div></td>
																														</tr>
																														<tr>
																															<td>Card Number: </td>
																															<td><div id="divCardNumber" runat="server"></div></td>
																														</tr>
																														<tr>
																															<td>Card Expiration: </td>
																															<td><div id="divCardExpiration" runat="server"></div></td>
																														</tr>
																													</table>	
																												</div>
																												<div id="divPaypal" runat="server">
																													<!-- PayPal Logo -->
																													<table border="0" cellpadding="10" cellspacing="0" align="center">
																														<tr><td align="center"></td></tr>
																														<tr><td align="center"><a href="#" onclick="javascript:window.open('https://www.paypal.com/us/cgi-bin/webscr?cmd=xpt/cps/popup/OLCWhatIsPayPal-outside','olcwhatispaypal','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=400, height=350');"><img src="https://www.paypal.com/en_US/i/bnr/bnr_nowAccepting_150x60.gif" border="0" alt="Additional Options"></a></td></tr>
																													</table>
																													<!-- PayPal Logo -->
																												</div>
																											</td>
                                                                                                        </tr>
                                                                                                        <tr>
																											<td align="center">
																												<a id="aUseDiffCC" runat="server" href="javascript:void(0);" >Use Different Credit Card</a>
																												<a id="aPayWithCC" runat="server" href="javascript:void(0);" onclick="$('radPmtCC').click();">Pay With Credit Card</a>
																											</td>
                                                                                                        </tr>
                                                                                                        <tr>
																											<td align="center">
																												<a id="aPaypal" runat="server" href="javascript:void(0);" onclick="$('radPmtPP').click();">Pay with Paypal</a></td>
                                                                                                        </tr>
                                                                                                        <tr>
																											<td>
																												Clicking Place Your Order will submit your order for processing. Do not click the back button while order is in process. Note: All sales are final.
																											</td>
                                                                                                        </tr>
                                                                                                        <tr>
																											<td align="center">
																												<asp:linkbutton id="lbPlaceOrder" onclick="lbPlaceOrder_Click" class="placeOrder" runat="server">Place Order</asp:linkbutton>
																											</td>		
																										</tr>
																									</table>
                                                                                                    <!-- !r: End of revised area -->
                                                                                                    <div style="display:none;">
																										<input type="radio" runat="server" onclick="SetPmtMethod(true);" id="radPmtCC" name="paymentType" value="cc" checked />
																										<input type="radio" runat="server" onclick="SetPmtMethod(false);" id="radPmtPP" name="paymentType" value="pp" />
																									</div>
																									<span class="cb"><span class="cl"></span></span>
																								</div>
																							</td>
																						</tr>
																					</table>
																					
																					<script type="text/javascript">
																					function makePurchase ()
																					{
																						if ($('radPmtCC').checked)
																						{
																							if (typeof(Page_ClientValidate) == 'function') 
																								if (!Page_ClientValidate()){return false;};
																							
																							this.disabled = true;
																							ShowLoading(true,'Please wait while we process your order...<br/><img src=\"../images/progress_bar.gif\">');
																							<asp:literal id="litPostBackRef" runat="server" />;
																						}
																					}
																					function SetPmtMethod (isCC)
																					{
																						var hasCC = <asp:literal id="litHasCC" runat="server"/>;
																						if (isCC)
																						{
																							$('divCreditCard').style.display='block';
																							$('divPaypal').style.display='none';
																							$('aPaypal').style.display='block';
																							
																							if (hasCC)
																							{
																								$('aUseDiffCC').style.display='block';
																								$('aPayWithCC').style.display='none';
																							}
																							else
																							{
																								$('aUseDiffCC').style.display='none';
																								$('aPayWithCC').style.display='block';
																							}
																						}
																						else
																						{
																							$('divCreditCard').style.display='none';
																							$('divPaypal').style.display='block';
																							$('aPaypal').style.display='none';
																							
																							$('aUseDiffCC').style.display='none';
																							$('aPayWithCC').style.display='block';
																						}	
																					}
																					</script>

																				</td>
																			</tr>
																		</table>
																		
																		<span class="cb"><span class="cl"></span></span>
																		</div>
																	</td>
																</tr>
															</table>
														</td>
														<!-- END WIZARD CONTENT -->
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td class="frBorderRight"><img id="Img7" runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
				</tr>
				<tr>
					<td class="frBottomLeft"></td>
					<td class="frBottom"></td>
					<td class="frBottomRight"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>

</div>
