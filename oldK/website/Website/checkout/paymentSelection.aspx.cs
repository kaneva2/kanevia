///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using log4net;
using Kaneva.BusinessLayer.Facade;


namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for paymentSelection.
	/// </summary>
	public class paymentSelection : MainTemplatePage
	{

		protected paymentSelection () 
		{
			Title = "Payment Selection";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			VerifyUserAccess ();
			VerifyOrder ();

			int userId = GetUserId ();

			if (!IsPostBack)
			{
				// Get this user's current credit cards
				DataTable dtCards = UsersUtility.GetCreditCards (userId, Constants.eBILLING_INFO_TYPE.PROFILE);

				rptCards.DataSource = dtCards;
				rptCards.DataBind ();

				rptCards.Visible = dtCards.Rows.Count > 0;
			}
			
			// Disable Billing?
			bool bDisableNewBilling = false;
			if (System.Configuration.ConfigurationManager.AppSettings ["DisableNewBilling"] != null)
			{
                bDisableNewBilling = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["DisableNewBilling"]);
			}

			if (bDisableNewBilling && !IsAdministrator ())
			{
				tblBillingDown.Visible = false;
				tblForm.Visible = true;
				return;
			}											

			tblBillingDown.Visible = false;
			tblForm.Visible = true;

			// Keep bread crumb at the top
			AddBreadCrumb (new BreadCrumb ("Payment", GetCurrentURL(), "", 0));

			Response.Expires = 0;
			Response.CacheControl = "no-cache";

			// Hide paypal?
			bool bHidePaypal = true;

            if (System.Configuration.ConfigurationManager.AppSettings["HidePaypal"] != null)
			{
                bHidePaypal = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["HidePaypal"]);
			}

			if (Request ["hidPP"] != null)
			{
				bHidePaypal = true;
			}

			if (bHidePaypal)
			{
				trPaypal1.Visible = false;
				trPaypal2.Visible = false;
			}

			// Set Nav
            HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB._UPDATE_;
			HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.ACCOUNT;
			HeaderNav.MyAccountNav.ActiveTab = NavAccount.TAB.CREDITS;
			HeaderNav.SetNavVisible(HeaderNav.MyKanevaNav,2);
			HeaderNav.SetNavVisible(HeaderNav.MyAccountNav);
		}
		
		#region Helper Methods
		
		private void VerifyUserAccess()
		{
			// They must be logged in
			if (!Request.IsAuthenticated)
			{
				Response.Redirect (GetLoginURL ());
			}
		}

		private void VerifyOrder ()
		{
			if (Request ["orderId"] == null)
			{
				Response.Redirect ( "~/mykaneva/managecredits.aspx" );
			}

			int orderId = Convert.ToInt32 (Request ["orderId"]);			
			VerifyOrder (StoreUtility.GetOrder (orderId));
		}
		private void VerifyOrder (DataRow drOrder)
		{
			if (Request ["orderId"] != null)
			{
				try
				{
					// Verify user is owner of this order
					if (drOrder == null)
					{
						// User may be trying to view someone else's order
						m_logger.Warn ("User " + GetUserId () + " tried to view orderId " + Request ["orderId"].ToString () + " from IP " + Common.GetVisitorIPAddress());
						Response.Redirect ( "~/mykaneva/managecredits.aspx" );
					}
				}
				catch
				{
					Response.Redirect ( "~/mykaneva/managecredits.aspx" );
				}
			}
			else
			{	  
				Response.Redirect ( "~/mykaneva/managecredits.aspx" );
			}
		}

		
		#endregion

		#region Event Handlers
		/// <summary>
		/// imgPayPal_Click
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void imgPayPal_Click (object sender, ImageClickEventArgs e) 
		{
			int orderId = Convert.ToInt32 (Session ["orderId"]);			  //TODO*****
			DataRow drOrder = StoreUtility.GetOrder (orderId);

			int pointTransactionId = Convert.ToInt32 (drOrder ["point_transaction_id"]);
			StoreUtility.UpdatePointTransaction (pointTransactionId, Constants.ePAYMENT_METHODS.PAYPAL);

			Response.Redirect ("~/checkout/orderConfirm.aspx");
		}

		/// <summary>
		/// btnAddCard_Click
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnAddCard_Click (object sender, EventArgs e)
		{
			VerifyUserAccess();

			if (!Page.IsValid) 
			{
				return;
			}

			if (Request ["orderId"] != null)
			{
				int orderId = Convert.ToInt32 (Request ["orderId"]);			
				DataRow drOrder = StoreUtility.GetOrder (orderId, GetUserId ());

				VerifyOrder(drOrder);

				int pointTransactionId = Convert.ToInt32 (drOrder ["point_transaction_id"]);

				// Update the payment method
				// StoreUtility.UpdatePointTransaction (pointTransactionId, Constants.ePAYMENT_METHODS.KANEVA_CYBERSOURCE);

				// Add the new credit card
				int billingInfoId = UsersUtility.InsertBillingInfo (GetUserId (), Constants.eBILLING_INFO_TYPE.PROFILE,"", Server.HtmlEncode (txtNameOnCard.Text), KanevaGlobals.Encrypt (Server.HtmlEncode (txtCardNumber.Text)), Server.HtmlEncode (drpCardType.SelectedItem.Text), Server.HtmlEncode (drpMonth.SelectedValue), "", Server.HtmlEncode (drpYear.SelectedValue), 0);

				// Update purchase transaction
				StoreUtility.UpdatePointTransactionBillingInfoId (pointTransactionId, billingInfoId);

				Response.Redirect ("~/checkout/billingInfo.aspx?orderId=" + orderId.ToString());
			}
			else
			{
				Response.Redirect ("~/mykaneva/managecredits.aspx");
			}
		}

		/// <summary>
		/// Select this address
		/// </summary>
		/// <param name="source"></param>
		/// <param name="e"></param>
		private void rptCards_ItemCommand (object source, RepeaterCommandEventArgs e)
		{
			string command = e.CommandName;							  //TODO**** need to combine this code with the code above in the
																	  //		 addcard_click event handlers since we will not be storing card #s
			HtmlInputHidden hidBillId;

			switch (command)
			{
				case "cmdSelect":
				{
					VerifyUserAccess();

					int index = e.Item.ItemIndex;
					hidBillId = (HtmlInputHidden) rptCards.Items [index].FindControl ("hidBillId");

					if (Request["orderId"] != null)
					{
						try
						{
							int orderId = Convert.ToInt32 (Request["orderId"]);					
							DataRow drOrder = StoreUtility.GetOrder (orderId, GetUserId());

							// Verify current order # belongs to user
							VerifyOrder(drOrder);

							int pointTransactionId = Convert.ToInt32 (drOrder ["point_transaction_id"]);

							// Update the payment method
							// StoreUtility.UpdatePointTransaction (pointTransactionId, Constants.ePAYMENT_METHODS.KANEVA_CYBERSOURCE);
					
							// Update the billing info
							StoreUtility.UpdatePointTransactionBillingInfoId (pointTransactionId, Convert.ToInt32 (hidBillId.Value));

							// Update the exp date
							DropDownList drpMonthChange = (DropDownList) e.Item.FindControl ("drpMonthChange");
							DropDownList drpYearChange = (DropDownList) e.Item.FindControl ("drpYearChange");
							UsersUtility.UpdateBillingInfo (GetUserId (), Convert.ToInt32 (hidBillId.Value), drpMonthChange.SelectedValue, drpYearChange.SelectedValue);

							Response.Redirect ("~/checkout/billingInfo.aspx?orderId=" + orderId.ToString());
							break;
						}
						catch 
						{
							//TODO*****
						}
					}
					else
					{
						Response.Redirect ("~/mykaneva/managecredits.aspx");
					}

					break;
				}
			}
		}

		/// <summary>
		/// rptCards_ItemDataBound
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void rptCards_ItemDataBound (object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
			{
				DataRowView drv = (DataRowView) e.Item.DataItem;
				string expYear = drv ["exp_year"].ToString ();

				DropDownList drpYearChange = (DropDownList) e.Item.FindControl ("drpYearChange");
				SetDropDownIndex (drpYearChange, expYear, true);
			}
		}

		#endregion
		
		#region Declerations
		protected PlaceHolder	phBreadCrumb;
		protected Repeater		rptCards;

		// New card info
		protected TextBox		txtCardNumber, txtNameOnCard;
		protected DropDownList	drpMonth, drpYear;
		protected KlausEnt.KEP.Kaneva.WebControls.CardTypesListBox drpCardType;

		protected HtmlTableRow trPaypal1, trPaypal2;

		protected HtmlTable tblBillingDown, tblForm;

		// Logger
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
			this.rptCards.ItemCommand +=new RepeaterCommandEventHandler (rptCards_ItemCommand);
			this.rptCards.ItemDataBound	+=new RepeaterItemEventHandler(rptCards_ItemDataBound);
		}
		#endregion

	}
}
