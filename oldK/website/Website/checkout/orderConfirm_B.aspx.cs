///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Kaneva.BusinessLayer.BusinessObjects;
using log4net;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
    public partial class orderConfirm_B : MainTemplatePage
    {
        private void Page_Load (object sender, System.EventArgs e)
        {
            if (!KanevaGlobals.EnableCheckout_B)
            {
                if (!IsAdministrator ())
                {
                    Response.Redirect("~/error.aspx?error=" + (int)Constants.eERROR_TYPE.CHECKOUT_CLOSED);
                }
            }

            // need to have an order to view this page
            if (Request["orderId"] == null || Request["orderId"].ToString ().Length == 0)
            {
                Response.Redirect ("~/mykaneva/buycredits.aspx");
            }

            Title = "Make Purchase";

            VerifyUserAccess ();

            Response.Expires = 0;
            Response.CacheControl = "no-cache";

            if (!IsPostBack)
            {
                DisplayScreen ();
            }

            // Set up purchase button																																																								  
            lbPlaceOrder.CausesValidation = false;
            lbPlaceOrder.Attributes.Add ("onclick", "makePurchase();");

            
            string orderId = Request["orderId"].ToString ();

            aChangeAddress.HRef = ResolveUrl ("~/checkout/billinginfo_B.aspx?orderId=" + orderId);
            aEditOrder.HRef = ResolveUrl ("~/mykaneva/buyCredits_B.aspx?orderId=" + orderId);

            // Set Nav
            HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
        }


        #region Helper Methods

        /// <summary>
        /// DisplayScreen
        /// </summary>
        private void DisplayScreen ()
        {
            if (Request["orderId"] == null)
            {
                Response.Redirect ("~/mykaneva/buycredits_B.aspx");
            }

            try
            {
                // Get the order
                int orderId = Convert.ToInt32 (Request["orderId"]);
                DataRow drOrder = StoreUtility.GetOrder (orderId, KanevaWebGlobals.CurrentUser.UserId);

                VerifyOrder (drOrder);

                int purchaseType = Convert.ToInt32 (drOrder["purchase_type"]);

                // Were any K-Points purchased?
                int pointTransactionId = 0;
                Double dCurrentPointAmountPurchased = 0.0;


                // Get the billing info and display purchase summary
                pointTransactionId = Convert.ToInt32 (drOrder["point_transaction_id"]);
                DataRow drPpt = StoreUtility.GetPurchasePointTransaction (pointTransactionId);

                spnPrice.InnerText = Convert.ToDouble (drPpt["amount_debited"]).ToString ();
                spnTotal.InnerText = spnPrice.InnerText;

                spnItemDescription.InnerText = drPpt["description"].ToString ();
                dCurrentPointAmountPurchased = Convert.ToDouble (drPpt["totalPoints"]);


                // What is the payment method?
                int paymentMethodId = Convert.ToInt32 (drPpt["payment_method_id"]);

                // Set UI based on payment type of order
                if (paymentMethodId.Equals ((int) Constants.ePAYMENT_METHODS.PAYPAL))
                {
                    // Set the type radio button
                    radPmtPP.Checked = true;
                    
                    // show the paypal payment div 
                    divPaypal.Style.Add ("display", "block");
                    divCreditCard.Style.Add ("display", "none");
                    
                    // Add line to address section
                    spnAddress2.InnerText = "Address not required for Paypal";
                    
                    // Configure which links to show
                    aPaypal.Style.Add ("display", "none");
                    aUseDiffCC.Style.Add ("display", "none");
                    aPayWithCC.Style.Add ("display", "block");
                }
                else  // Paying with Credit Card
                {
                    // Set the type radio button
                    radPmtCC.Checked = true;

                    // show the paypal payment div
                    divCreditCard.Style.Add ("display", "block");
                    divPaypal.Style.Add ("display", "none");

                    // Configure which links to show
                    aPaypal.Style.Add ("display", "block");
                    aUseDiffCC.Style.Add ("display", "block");
                    aPayWithCC.Style.Add ("display", "none");
                }

                // Address
                if (drPpt["address_id"] != System.DBNull.Value)
                {
                    int addressId = Convert.ToInt32 (drPpt["address_id"]);
                    DataRow drAddress = UsersUtility.GetAddress (KanevaWebGlobals.CurrentUser.UserId, addressId);

                    if (drAddress != null)
                    {
                        spnFullName.InnerText = Server.HtmlDecode (drAddress["name"].ToString ());
                        spnAddress1.InnerText = Server.HtmlDecode (drAddress["address1"].ToString ());
                        spnAddress2.InnerText = Server.HtmlDecode (drAddress["address2"].ToString ());
                        spnCity.InnerText = Server.HtmlDecode (drAddress["city"].ToString ()) + (Server.HtmlDecode (drAddress["state_name"].ToString ()).Length > 0 ? ", " : ""); 
                        spnState.InnerText = Server.HtmlDecode (drAddress["state_name"].ToString ());
                        spnZip.InnerText = Server.HtmlDecode (drAddress["zip_code"].ToString ());
                        spnCountry.InnerText = Server.HtmlDecode (drAddress["country_name"].ToString ());

                        if (spnCountry.InnerText.ToLower () == "united states" && Server.HtmlDecode (drAddress["phone_number"].ToString ()).Length == 10)
                        {
                            string phNum = Server.HtmlDecode (drAddress["phone_number"].ToString ());
                            spnPhone.InnerText = phNum.Substring (0, 3) + "." + phNum.Substring (3, 3) + "." + phNum.Substring (6, 4);
                        }
                        else
                        {
                            spnPhone.InnerText = Server.HtmlDecode (drAddress["phone_number"].ToString ());
                        }

                        aChangeAddress.InnerText = "Change Address";
                    }
                }

                // this value is used in the javascript method SetPmtMethod
                litHasCC.Text = "false";

                // Get users billing subscription id
                string billSubId = UsersUtility.GetUserBillingSubscriptionId (KanevaWebGlobals.CurrentUser.UserId);

                // does user have a billing subscription id?
                if (billSubId.Length > 0)
                {
                    // user has subscription id, so let's get the info from cybersource
                    CybersourceAdaptor cybersourceAdaptor = new CybersourceAdaptor ();
                    BillingInfo billInfo = new BillingInfo ();
                    string userErrorMessage = "";
                    bool bSystemDown = false;
                    if (!cybersourceAdaptor.GetBillingSubscription (KanevaWebGlobals.CurrentUser.UserId,
                        billSubId, ref bSystemDown, ref userErrorMessage, ref billInfo))
                    {
                        if (bSystemDown)
                        {
                            m_logger.Error ("Error loading subscription information from Cybersource.  System is down.  UserId = " + KanevaWebGlobals.CurrentUser.UserId.ToString());
                            ShowErrorMessage ("There was a problem loading your billing information.  The sytem is currently unavailable.  Please try again later.", false);
                            return;
                        }
                        else
                        {
                            m_logger.Error ("Error loading subscription information from Cybersource " + userErrorMessage + ".  UserId = " + KanevaWebGlobals.CurrentUser.UserId.ToString ());
                            ShowErrorMessage ("There was a problem loading your billing information.  Please try reloading this page.", false);
                            return;
                        }
                    }

                    // Is there any data stored or has it been cleared by the user?
                    if (billInfo.CardNumber.Length > 0 && billInfo.CardType.Length > 0)
                    {
                        // Subscription has data, so we are going to use this data for display
                        divCardType.InnerHtml = billInfo.CardType;
                        divCardNumber.InnerHtml = "..." + billInfo.CardNumberLast4;
                        divCardExpiration.InnerHtml = billInfo.CardExpirationMonth + "/" + billInfo.CardExpirationYear;

                        aUseDiffCC.HRef = ResolveUrl ("~/checkout/billingInfo_B.aspx?orderId=" + orderId.ToString () + "&showcc=&#cc");
                        litHasCC.Text = "true";
                    }
                    else // User has an existing subscription but there is no stored credit card info, 
                    // so redirect to billing info page if they choose Use Credit Card link
                    {
                        aPayWithCC.HRef = ResolveUrl ("~/checkout/billingInfo_B.aspx?orderId=" + orderId.ToString ());
                        aPayWithCC.Attributes.Add ("onclick", "javascript:void(0);");
                    }
                }
                else
                {
                    aPayWithCC.HRef = ResolveUrl ("~/checkout/billingInfo_B.aspx?orderId=" + orderId.ToString ());
                    aPayWithCC.Attributes.Add ("onclick", "javascript:void(0);");
                }

                // Show rest of labels
                Double dUserKPointTotal = GetUserPointTotal ();
                spnCreditBalance.InnerText = Convert.ToUInt64 (dUserKPointTotal).ToString ("N0");
                spnCreditPurchasing.InnerText = Convert.ToUInt64 (dCurrentPointAmountPurchased).ToString ("N0");
                spnNewCreditBalance.InnerText = Convert.ToUInt64 (dUserKPointTotal + dCurrentPointAmountPurchased).ToString ("N0");
            }
            catch (Exception exc)
            {
                m_logger.Error ("Error loading order information for order " + Request["orderId"].ToString (), exc);
                ShowErrorMessage ("There was a problem loading the order information.  Please try reloading this page or creating a new order.", false);
                return;
            }
        }

        /// <summary>
        /// VerifyUserAccess
        /// </summary>
        private void VerifyUserAccess ()
        {
            // They must be logged in
            if (!Request.IsAuthenticated)
            {
                Response.Redirect (GetLoginURL ());
            }

            // if access pass purchase, must be 18 or older
            if (Request.QueryString["pass"] != null &&
                Request.QueryString["pass"].ToString () == "true" &&
                !KanevaWebGlobals.CurrentUser.IsAdult)
            {
                Response.Redirect ("~/mykaneva/underage.aspx");
            }
        }

        /// <summary>
        /// VerifyOrder
        /// </summary>
        private void VerifyOrder ()
        {
            if (Request["orderId"] == null)
            {
                Response.Redirect ("~/mykaneva/managecredits.aspx");
            }

            try
            {
                int orderId = Convert.ToInt32 (Request["orderId"]);
                VerifyOrder (StoreUtility.GetOrder (orderId, GetUserId ()));
            }
            catch
            {
                Response.Redirect ("~/mykaneva/managecredits.aspx");
            }
        }
        /// <summary>
        /// VerifyOrder
        /// </summary>
        private void VerifyOrder (DataRow drOrder)
        {
            bool err = false;

            try
            {
                // Verify user is owner of this order		  
                if (drOrder == null)
                {
                    // User may be trying to view someone else's order
                    m_logger.Warn ("User " + GetUserId () + " tried to view orderId " + Request["orderId"].ToString () + " from IP " + Common.GetVisitorIPAddress());
                    err = true;
                }

                if (Convert.ToInt32 (drOrder["transaction_status_id"]) != (int) Constants.eORDER_STATUS.CHECKOUT)
                {
                    m_logger.Warn ("User " + GetUserId () + " tried to view orderId " + Request["orderId"].ToString () + " from IP " + Common.GetVisitorIPAddress() + ".  Order does not have a status of CART.");
                    err = true;
                }

                // Check to see if user is trying to purchase an access pass
                DataRow drPointBucket = StoreUtility.GetPromotion (Convert.ToInt32 (drOrder["point_bucket_id"]));
                if (Convert.ToInt32 (drPointBucket["wok_pass_group_id"]) > 0)
                {
                    // verify user is old enough to purchase an access pass
                    if (!KanevaWebGlobals.CurrentUser.IsAdult)
                    {
                        m_logger.Warn ("User " + GetUserId () + " tried to process orderId " + Request["orderId"].ToString () + " from IP " + Common.GetVisitorIPAddress() + ".  Order is for Adult Pass and user is UNDER 18.");
                        err = true;
                    }
                }
            }
            catch
            {
                err = true;
            }

            if (err)
                Response.Redirect ("~/mykaneva/managecredits.aspx");
        }

        /// <summary>
        /// GetUserPointTotal
        /// </summary>
        /// <returns></returns>
        private Double GetUserPointTotal ()
        {
            return UsersUtility.GetUserPointTotal (KanevaWebGlobals.CurrentUser.UserId);
        }

        /// <summary>
        /// MakePayPalPayment
        /// </summary>
        /// <param name="drOrder"></param>
        /// <param name="drPpt"></param>
        /// <returns></returns>
        private bool MakePayPalPayment (int orderId, DataRow drPpt)
        {
            try
            {
                // Mark it as pending
                StoreUtility.UpdateOrderToPending (orderId);

                // Are they puchasing a bucket or custom amount?
                if (drPpt["point_bucket_id"] != null && drPpt["point_bucket_id"].ToString ().Length > 0)
                {
                    // Send them to PayPal
                    int pointBucketId = Convert.ToInt32 (drPpt["point_bucket_id"]);
                    Response.Redirect (ResolveUrl ("~/billing/makePurchase.aspx?pb=" + pointBucketId.ToString () + "&pOID=" + orderId), false);
                }
                else
                {
                    // Send them to PayPal
                    Double amount = Convert.ToDouble (drPpt["totalPoints"]);
                    Response.Redirect (ResolveUrl ("~/billing/makePurchase.aspx?kpAmount=" + amount.ToString () + "&pOID=" + orderId));
                }

                return true;
            }
            catch (Exception exc)
            {
                m_logger.Error ("Error sending user to PayPal", exc);
                return false;
            }
        }

        /// <summary>
        /// MakeCyberSourcePayment
        /// </summary>
        /// <param name="drOrder"></param>
        /// <param name="drPpt"></param>
        /// <returns></returns>
        private bool MakeCyberSourcePayment (DataRow drOrder, DataRow drPpt, string subscriptionId, ref string userErrorMessage)
        {
            bool bSystemDown = false;
            CybersourceAdaptor cybersourceAdaptor = new CybersourceAdaptor ();
            return cybersourceAdaptor.ProcessPaymentUsingSubscription (drOrder, drPpt, subscriptionId, Common.GetVisitorIPAddress(), ref userErrorMessage, ref bSystemDown, true);
        }

        /// <summary>
        /// Format and display error message
        /// </summary>
        private void ShowErrorMessage (string err)
        {
            ShowErrorMessage (err, true);
        }
        private void ShowErrorMessage (string err, bool showAsList)
        {
            if (showAsList)
            {
                valSum.DisplayMode = ValidationSummaryDisplayMode.List;
                valSum.HeaderText = "Please correct the following error(s):<br/>";
                valSum.Style.Add ("text-align", "left");
            }
            else
            {
                valSum.DisplayMode = ValidationSummaryDisplayMode.SingleParagraph;
                valSum.HeaderText = "";
                valSum.Style.Add ("text-align", "center");
            }

            cvBlank.IsValid = false;
            cvBlank.ErrorMessage = err;
        }
        
        #endregion Helper Methods


        #region Event Handlers

        /// <summary>
        /// Click event for the place order linkbutton
        /// </summary>
        protected void lbPlaceOrder_Click (object sender, EventArgs e)
        {
            VerifyUserAccess ();
            VerifyOrder ();

            try
            {
                // Get the order
                int orderId = Convert.ToInt32 (Request["orderId"]);
                DataRow drOrder = StoreUtility.GetOrder (orderId);
                int pointTransactionId = Convert.ToInt32 (drOrder["point_transaction_id"]);

                // Get the payment type
                if (radPmtCC.Checked)
                {
                    StoreUtility.UpdatePointTransaction (pointTransactionId, Constants.ePAYMENT_METHODS.KANEVA_CYBERSOURCE);
                }
                else if (radPmtPP.Checked)
                {
                    StoreUtility.UpdatePointTransaction (pointTransactionId, Constants.ePAYMENT_METHODS.PAYPAL);
                }

                string userErrorMessage = "";

                int orderBillingInformationId = 0;
                int billingInformationId = 0;
                int addressId = 0;

                // They are purchasing credit
                DataRow drPpt = StoreUtility.GetPurchasePointTransaction (pointTransactionId);

                // Is it PayPal?
                int paymentMethodId = Convert.ToInt32 (drPpt["payment_method_id"]);

                if (paymentMethodId.Equals ((int) Constants.ePAYMENT_METHODS.PAYPAL))
                {
                    // PayPal
                    if (!MakePayPalPayment (orderId, drPpt))
                    {
                        ShowErrorMessage ("Error processing order for PayPal");
                        return;
                    }
                }
                else if (paymentMethodId.Equals ((int) Constants.ePAYMENT_METHODS.KANEVA_CYBERSOURCE))
                {
                    // Did we already copy the billing information over to history?
                    if (!drPpt["order_billing_information_id"].Equals (DBNull.Value))
                    {
                        orderBillingInformationId = Convert.ToInt32 (drPpt["order_billing_information_id"]);
                    }

                    billingInformationId = Convert.ToInt32 (drPpt["billing_information_id"]);
                    addressId = Convert.ToInt32 (drPpt["address_id"]);

                    // Copy payment/billing address to order history
                    orderBillingInformationId = StoreUtility.CopyBillingInfoToOrder (orderBillingInformationId, billingInformationId, addressId);

                    // Update the purchase_transaction order_billing_id
                    StoreUtility.UpdatePointTransactionOrderBillingInfoId (pointTransactionId, orderBillingInformationId);

                    // Get it again to update the orderBillingInformationId to be used in the next call
                    drPpt = StoreUtility.GetPurchasePointTransaction (pointTransactionId);

                    // Get users billing subscription id
                    string billSubId = UsersUtility.GetUserBillingSubscriptionId (KanevaWebGlobals.CurrentUser.UserId);

                    // CyberSource
                    if (!MakeCyberSourcePayment (drOrder, drPpt, billSubId, ref userErrorMessage))
                    {
                        // Check err and see if we need to redirect user back to billing info page
                        // the string we are checking against is the default err message for non credit
                        // card and address related problems. If it's genereic, display error on this page.
                        if (userErrorMessage != "" && userErrorMessage.IndexOf ("problem processing your payment.") > 0)
                        {
                            // display error on this page
                            ShowErrorMessage (userErrorMessage, false);
                            return;
                        }
                        else
                        {
                            // redirect back to billing info page and display err
                            Response.Redirect ("~/checkout/billingInfo_B.aspx?orderId=" + orderId.ToString() + "&err=" + Server.UrlEncode(Server.HtmlEncode(userErrorMessage)));
                        }
                        
                        ShowErrorMessage ("An error occurred while trying to process your order.  Please retry your request.", false);
                        return;
                    }

                    // if there are wok items included in the order, add
                    // them to the users inventory
                    StoreUtility.AddOrderItemsToUserInventory (orderId, Convert.ToInt32 (drOrder["point_bucket_id"]), KanevaWebGlobals.CurrentUser.UserId, KanevaWebGlobals.CurrentUser.Gender);

                    // Successfull k-point purchase here
                    MailUtilityWeb.SendKpointPurchaseEmail (KanevaWebGlobals.CurrentUser.UserId, orderId);

                    Response.Redirect (ResolveUrl ("~/checkout/orderComplete_B.aspx?orderId=" + orderId), false);
                }
                else
                {
                    ShowErrorMessage ("Invalid Payment Method specified!");
                    return;
                }
            }
            catch (Exception exc)
            {
                m_logger.Error ("Error trying to process purchase order: ", exc);
                ShowErrorMessage ("An error occurred while trying to process your order.  Please retry your request.", false);
            }
        }

        #endregion Event Handlers


        #region Declerations

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

        #endregion Declerations

    }
}
