<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="billingInfo_B.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.billingInfo_B" %>
<%@ Register TagPrefix="Kaneva" Namespace="KlausEnt.KEP.Kaneva.WebControls" Assembly="Kaneva.WebControls" %>

<script src="../jscript/prototype.js" type="text/javascript"></script>
<script type="text/javascript">
function CountryOnChange (val)
{
	if (val != '')
	{
		if (val=='US' || val=='CA')
		{
			$('drpState').style.display='block';
			$('txtState').style.display='none';
		}
		else
		{
			$('drpState').style.display='none';
			$('txtState').style.display='block';
		}
	}
}
</script>

<link href="../css/new.css" rel="stylesheet" type="text/css">
<link href="../css/shadow.css" rel="stylesheet" type="text/css">
<link href="../css/temp_purchaseFlow.css" rel="stylesheet" type="text/css">

<table border="0" cellspacing="0" cellpadding="0" width="990" align="center">
	<tr>
		<td><img id="Img1" runat="server" src="~/images/spacer.gif" width="1" height="14" /></td>
	</tr>
	<tr>
		<td>
        	<!-- new div: link to manage account settings -->
			<table  border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="frTopLeft"></td>
					<td class="frTop"></td>
					<td class="frTopRight"></td>
				</tr>
				<tr>
					<td class="frBorderLeft"><img id="Img2" runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
					<td valign="top" class="frBgIntMembers">
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="968" align="left"  valign="top">
									<table cellpadding="0" cellspacing="0" border="0" width="100%">
										<tr>
											<td width="968" valign="top" align="center">
												<div class="progressBar" id="divProgressBar" runat="server">
													<!-- WIDTH OF ALL SPANS SHOULD ADD UP TO 958 PX -->
													<ul class="payment">
														<li id="liSelect" class="">
															<h3>Select Package</h3>
														</li>
														<li id="liPayment" class="">
															<h3>Payment</h3>
														</li>
														<li id="liReceipt" class="">
															<h3>Receipt</h3>
														</li>
													</ul>
												</div>
											</td>
										</tr>
										<tr>
											<td width="970" valign="top">
												<table border="0" cellspacing="0" cellpadding="0" width="100%">
													<tr>
														<!-- START WIZARD CONTENT -->
														<td valign="top" align="center">
															<table border="0" cellspacing="0" cellpadding="0">
																<tr>
																	<td width="670">
																		<div class="module fullpage">
																			<span class="ct"><span class="cl"></span></span>
	                                                                        
																				<!-- there is a bunch of revised css that need to be dumped into one of the existing css. most of the existing css are from new.css; put them there? Please add the comment: New purchase flow 021009 -->
																				<!-- !r: start the revisions here -->
																				<h1 class="sellBillingAddy">Billing &amp; Payment Information</h1>
																				<div class="payPalTop" id="divPaypalTop" runat="server" style="display:none;">
																					<img id="Img3" style="float:left;margin:3px 5px 0 0;" src="~/images/paypal_small.gif" width="69" height="22" runat="server" />
																					<span style="font-weight:bold;">Using Paypal?</span><br />
                                                      								<asp:linkbutton id="lbPaypalTop" runat="server" causesvalidation="false" onclick="lbPaypal_Click">Skip this step >></asp:linkbutton>
	                                                    						</div>
		                                                    					
																				<p id="pDesc" runat="server" class="billingCopy clearboth">Billing address must match billing address of credit card to be used.</p>
																		
																				<table border="0" cellspacing="0" cellpadding="0" width="460" align="center" class="wizform">
																					<tr>																												  
																						<td align="center">		  
																							<asp:validationsummary cssclass="errBox" id="valSum" runat="server" showmessagebox="False" showsummary="True"
																								displaymode="BulletList" style="margin-top:10px;width:400px;" headertext="Please correct the following error(s):" forecolor="black"></asp:validationsummary>
																								<asp:customvalidator id="cvBlank" runat="server" display="None" enableclientscript="False"></asp:customvalidator>
																							<br />			  
	                                                                                
																							<table id="Table11" class="billingData">																								  
																								<tr>
																									<td style="width:475px;">Full Name*<br />
																										<asp:textbox id="txtFullName" class="biginput" style="width:420px" maxlength="100" runat="server"/>
																										<asp:requiredfieldvalidator id="rfUsername" controltovalidate="txtFullName" text="<br>Full Name is a required field" errormessage="" display="Dynamic" runat="server"/>
																									</td>
																									<td class="note" style="width:180px;">&nbsp;&nbsp;* Indicates Required Field.<br />
																										** Indicates required fields for United States and Canada only.</td>														  
																								</tr>
																								<tr>
																									<td>Address 1*<br />
																										<asp:textbox id="txtAddress1" class="biginput" style="width:420px" maxlength="100" runat="server"/>
																										<asp:requiredfieldvalidator id="rfvAddress1" controltovalidate="txtAddress1" text="<br>Address1 is a required field" errormessage="" display="Dynamic" runat="server"/>
																									</td>
																									<td><!-- empty --></td>
																								</tr>
																								<tr>
																									<td>Address 2<br />
																										<asp:textbox id="txtAddress2" class="biginput" style="width:420px" maxlength="80" runat="server"/>
																									</td>
																									<td><!-- empty --></td>
																								</tr>
																								<tr>
																									<td>City*<br />
																										<asp:textbox id="txtCity" class="biginput" style="width:200px" maxlength="100" runat="server"/>
																										<asp:requiredfieldvalidator id="rftxtCity" controltovalidate="txtCity" text="<br>City is a required field" errormessage="" display="Dynamic" runat="server"/>
																									</td>
																									<td><!-- empty --></td>
																								</tr>
																								<tr>
																									<td>State/Province/Region**<br />
																										<asp:textbox runat="server" class="biginput" id="txtState" style="width:200px;display:none;" maxlength="100"></asp:textbox>
																										<asp:requiredfieldvalidator id="rftxtState" controltovalidate="txtState" text="" errormessage="State/Province/Region is a required field." display="None" runat="server"/>
																										<asp:dropdownlist id="drpState" runat="server" class="biginput" style="width:120px;margin-bottom:2px;"></asp:dropdownlist>
																										<asp:requiredfieldvalidator id="rfdrpState" controltovalidate="drpState" text="State is a required field when country is United States or Canada<br>" errormessage="" display="Dynamic" runat="server"/>
																									</td>
																									<td><!-- empty --></td>
																								</tr>			
																								<tr>
																									<td>Postal Code/ZIP*<br />
																										<asp:textbox id="txtPostalCode" class="biginput" style="width:120px" size="9" maxlength="25" runat="server"/>
																										<asp:requiredfieldvalidator id="rftxtPostalCode" controltovalidate="txtPostalCode" text="<br>Postal Code/ZIP is a required field" errormessage="" display="Dynamic" runat="server"/>
																									<td><!-- empty --></td>
																								</tr>
																								<tr>
																									<td>Country*<br />
																										<asp:dropdownlist id="drpCountry" runat="server" class="biginput" style="width:300px;" onchange="CountryOnChange(this.options[this.selectedIndex].value);"></asp:dropdownlist>
																										<asp:requiredfieldvalidator id="rfdrpCountry" controltovalidate="drpCountry" text="<br>Country is a required field" errormessage="" display="Dynamic" runat="server"/>
																									</td>
																									<td><!-- empty --></td>
																								</tr>
																								<tr>
																									<td>Phone Number*<br />
																										<asp:textbox id="txtPhoneNumber" class="biginput" style="width:200px" maxlength="15" runat="server"/>
																										<asp:requiredfieldvalidator id="rftxtPhoneNumber" controltovalidate="txtPhoneNumber" text="<br>Phone Number is a required field" errormessage="" display="Dynamic" runat="server"/>
																									</td>
																									<td><!-- empty --></td>
																								</tr>
																								<tr>
																									<td colspan="2">
																										<input type="text" id="txtEditCard" value="false" runat="server" style="display:none;" />
																										<table id="tblCardView" runat="server" class="" style="padding:0px;margin:0px;display:block;width:400px;" border="0">
																											<tr>
																												<td style="width:120px;padding:0px'" valign="top">Type: </td>
																												<td style="padding:0px;" valign="top"><span id="spnCardType" runat="server"></span></td>
																												<td style="text-align:right;padding:0px;"><a href="javascript:void(0);" onclick="$('txtEditCard').value='true';$('tblCardView').style.display='none';$('tblCardEdit').style.display='block';">Edit Credit Card</a></td>
																											</tr>
																											<tr>
																												<td style="width:120px;padding:0px'" valign="top">Card Number: </td>
																												<td style="padding:0px;" valign="top"><span id="spnCardNumber" runat="server"></span></td>
																												<td style="text-align:right;padding:0px;"></td>
																											</tr>
																											<tr>
																												<td style="width:120px;padding:0px'" valign="top">Expiration: </td>
																												<td style="padding:0px;" valign="top"><span id="spnCardExpiration" runat="server"></span></td>
																												<td style="text-align:right;padding:0px;"></td>
																											</tr>
																										</table>
																										<a name="cc"></a>
																										<table id="tblCardEdit" runat="server" class="billingData" style="margin:0px;display:none;width:500px;" border="0">
																											<tr>
																												<td colspan="2">Card Type*<br />
																													<kaneva:cardtypeslistbox class="biginput" id="drpCardType" runat="server" style="width:200px" size="9" rows="1">
																														<asp:listitem value="CardTypes Here">CardTypes Here</asp:listitem>
																													</kaneva:cardtypeslistbox>
																												</td>
																											</tr>
																											<tr>
																												<td colspan="2">Card Number*<br /> 
																													<asp:textbox id="txtCardNumber" class="biginput" style="width:255px" maxlength="16" runat="server" enableviewstate="false" /><br />
																													<kaneva:creditcardvalidator id="ccvNumber" controltovalidate="txtCardNumber" errormessage="" display="Dynamic" text="Please enter a valid credit card number for the card type selected<br>" runat="server" cardtypeslistbox="drpCardType" />
																													<asp:requiredfieldvalidator id="rfvCardNumber" controltovalidate="txtCardNumber" text="Card Number is a required field" errormessage="" display="Dynamic" runat="server"/>
																													<div class="note">Please do not enter spaces.<br />
																													For security purposes Kaneva does NOT store credit card numbers.</div></td>
																											</tr>
																											
																											<tr style="display:none;">
																												<td colspan="2">Security Code* <br />
																												<!-- Keep this code - Prod Man. may decide to add back later
																													<asp:textbox id="txtSecurityCode" class="biginput" style="width:100px" maxlength="4"/> 
                                                                                          							<a onmouseover="this.style.cursor='pointer'" onfocus="this.blur();" onclick="$('PopUp').style.display='block';return false;" href="javascript:void(0);">What is this?</a>
																													<div id="PopUp">
																														<div style="display:block; border: 3px solid #f7ce52; padding: 10px 10px 0 10px; background-color: #FFFFFF;">
																															<table border="0" cellspacing="0" cellpadding="3">
																																<tr>
																																	<td><img runat="server" src="~/images/mini_amex.gif" id="Img4" alt="American Express displays the 4 digit number on the front right of the card." width="118" height="73" border="0" /></td>
																																	<td><img runat="server" src="~/images/mini_discover.gif" id="Img6" alt="Discover displays the 3 digit number on the back of the card." width="118" height="73" border="0" /></td>
																																</tr>
																																<tr>
																																	<td><img runat="server" src="~/images/mini_mc.gif" id="Img7" alt="MasterCard displays the 3 digit number on the back of the card." width="118" height="73" border="0" /></td>
																																	<td><img runat="server" src="~/images/mini_visa.gif" id="Img8" alt="Visa displays the 3 digit number on the back of the card." width="118" height="73" border="0" /></td>
																																</tr>
																																<tr>
																																	<td colspan="2">Please enter your 3 or 4 digit Card ID Number</td>
																																</tr>
																																<tr>
																																	<td colspan="2"><div style="text-align: right;"><a href="javascript:void(0);" onmouseover='' onfocus='this.blur();' onClick="$('PopUp').style.display='none';return false;" >Close</a></div></td>
																																</tr>                                                 
																															</table>
																														</div>
																													</div><br />
																													<asp:requiredfieldvalidator id="rftxtSecurityCode" controltovalidate="txtSecurityCode" text="Security Code is a required field" errormessage="" display="Dynamic"/>
																												-->
																												</td>
																											</tr>
																											
																											<tr>
																												<td colspan="2">Expiration Date*<br />
																													<asp:dropdownlist class="biginput" id="drpMonth" runat="Server" style="width:150px">
																														<asp:listitem value="01">(01) January</asp:listitem>
																														<asp:listitem value="02">(02) February</asp:listitem>
																														<asp:listitem value="03">(03) March</asp:listitem>
																														<asp:listitem value="04">(04) April</asp:listitem>
																														<asp:listitem value="05">(05) May</asp:listitem>
																														<asp:listitem value="06">(06) June</asp:listitem>
																														<asp:listitem value="07">(07) July</asp:listitem>
																														<asp:listitem value="08">(08) August</asp:listitem>
																														<asp:listitem value="09">(09) September</asp:listitem>
																														<asp:listitem value="10">(10) October</asp:listitem>
																														<asp:listitem value="11">(11) November</asp:listitem>
																														<asp:listitem value="12">(12) December</asp:listitem>
																													</asp:dropdownlist>
																																					  
																													<asp:dropdownlist class="biginput" runat="server" id="drpYear" style="width:100px"> 
																														<asp:listitem value="2010">2010</asp:listitem>
																														<asp:listitem value="2011">2011</asp:listitem>
																														<asp:listitem value="2012">2012</asp:listitem>
																														<asp:listitem value="2013">2013</asp:listitem>
																														<asp:listitem value="2014">2014</asp:listitem>
																														<asp:listitem value="2015">2015</asp:listitem>
																														<asp:listitem value="2016">2016</asp:listitem>
																														<asp:listitem value="2017">2017</asp:listitem>
																														<asp:listitem value="2018">2018</asp:listitem>
																														<asp:listitem value="2019">2019</asp:listitem>
																														<asp:listitem value="2020">2020</asp:listitem>
																													</asp:dropdownlist>
																												</td>
																											</tr>
																										</table>
																									</td>
																								</tr>		
																								<tr>
																									<td>
																										<div class="saveBtnArea">
																											<asp:linkbutton id="lbSave" causesvalidation="false" runat="server" onclick="lbSaveContinue_Click" class="saveBtn">Save &amp; Continue</asp:linkbutton>
																											<asp:linkbutton id="lbSaveEdit" causesvalidation="false" runat="server" onclick="lbSaveEdit_Click" class="saveBtn">Save &amp; Continue</asp:linkbutton>
																										</div>
																										<div class="clearInfo" id="divClearInfo" runat="server" style="display:none;"><asp:linkbutton id="lbClearInfo" runat="server" onclick="lbClearInfo_Click" causesvalidation="false">Clear Saved Information</asp:linkbutton></div>
																									</td>
																									<td align="right"><div id="divPaypalBtm" class="payPalR" runat="server" style="display:none;"> <img src="~/images/paypal_small.gif" runat="server" style="float:left; margin:3px 5px 0 0;" width="69" height="22" /> <span style="font-weight:bold;">Using Paypal?</span><br />
																										<asp:linkbutton runat="server" causesvalidation="false" id="lbPaypalBtm" onclick="lbPaypal_Click">Skip this step >></asp:linkbutton></div>
																									</td>
																								</tr>
																							</table>
																						</td>
																					</tr>
																				</table>
																				<br />
																			
																			<span class="cb"><span class="cl"></span></span>
																		</div>
																	</td>
																</tr>
															</table>
														</td>
														<!-- END WIZARD CONTENT -->
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" id="Img5" width="1" height="1" /></td>
				</tr>
				<tr>
					<td class="frBottomLeft"></td>
					<td class="frBottom"></td>
					<td class="frBottomRight"></td>
				</tr>
				
			</table>
		</td>
	</tr>
</table>


		