<%@ Page language="c#" Codebehind="paymentSelection.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.paymentSelection" %>
<%@ Register TagPrefix="Kaneva" TagName="HeaderText" Src="../usercontrols/HeaderText.ascx" %>
<%@ Register TagPrefix="Kaneva" Namespace="KlausEnt.KEP.Kaneva.WebControls" Assembly="Kaneva.WebControls" %>

<Kaneva:HeaderText runat="server" Text="Payment Method" />
<asp:PlaceHolder id="phBreadCrumb" runat="server"/>

<br>

<table runat="server" cellpadding="0" cellspacing="0" border="0" width="710">
	<tr><td>
		<asp:ValidationSummary ShowMessageBox="False" ShowSummary="True" Class="formError" id="valSum" DisplayMode="BulletList" runat="server" HeaderText="Please correct the following errors:"/>
	</td></tr>
</table>
<center>

<table id="tblBillingDown" runat="server" cellpadding="0" cellspacing="0" border="0" width="710">
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="1" height="200"/></td>
		<td height="200" class="dateStamp" align="left" width="710" valign="top"><br>
			<span class="orangeHeader2">We are sorry, our billing system is currently down for maintenence. Please try again at a later time.</span><br><br>
		</td>
	</tr>
</table>

<table id="tblForm" runat="server" cellpadding="0" cellspacing="0" border="0" width="710">
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="1" height="200"/></td>
		<td height="200" class="dateStamp" id="tdLoginBG" align="left" width="710" valign="top"><br>
			<span class="orangeHeader2">Please Select a Payment Method</span><br><br>
			
			<table cellpadding="0" cellspacing="0" border="0" width="710">
				
				<asp:Repeater id="rptCards" runat="server" EnableViewState="True">
					<HeaderTemplate>
						<tr Class="heading02">
							<td colspan="7" align="Left">
								<img runat="server" src="~/images/spacer.gif" width="1" height="10"/>Pay using existing credit cards
							</td>
						</tr>
						<tr Class="lineItemColHead">
							<td><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
							<td width="10%">Action</td>
							<td>Payment Method</td>
							<td>Credit Card Number</td>
							<td>Expiration Date</td>
							<td>Name on Card</td>
							<td width="10"><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
						</tr>
					</HeaderTemplate>
					<ItemTemplate>
						<tr class="bodyText">
							<td><img runat="server" src="~/images/spacer.gif" width="20" height="40"/></td>
							<td>								
								<asp:ImageButton id="imgSelect" CommandName="cmdSelect" AlternateText="Select this Card" runat="server" ImageUrl="~/images/button_select.gif" CausesValidation="False"/>
							</td>
							<td>	
								<%# DataBinder.Eval(Container.DataItem, "card_type") %>
								
								<input type="hidden" runat="server" id="hidBillId" value='<%# DataBinder.Eval(Container.DataItem, "billing_information_id")%>'>
							</td>
							<td><%# ShowCreditCardNumber (DataBinder.Eval(Container.DataItem, "card_number").ToString ()) %></td>
							<td>
								<asp:DropDownList class="filter2" id="drpMonthChange" runat="Server" style="width:80px" SelectedValue='<%# DataBinder.Eval(Container.DataItem, "exp_month")%>'>
									<asp:ListItem Value="01">01 (Jan)</asp:ListItem>
									<asp:ListItem Value="02">02 (Feb)</asp:ListItem>
									<asp:ListItem Value="03">03 (Mar)</asp:ListItem>
									<asp:ListItem Value="04">04 (Apr)</asp:ListItem>
									<asp:ListItem Value="05">05 (May)</asp:ListItem>
									<asp:ListItem Value="06">06 (Jun)</asp:ListItem>
									<asp:ListItem Value="07">07 (Jul)</asp:ListItem>
									<asp:ListItem Value="08">08 (Aug)</asp:ListItem>
									<asp:ListItem Value="09">09 (Sep)</asp:ListItem>
									<asp:ListItem Value="10">10 (Oct)</asp:ListItem>
									<asp:ListItem Value="11">11 (Nov)</asp:ListItem>
									<asp:ListItem Value="12">12 (Dec)</asp:ListItem>
								</asp:DropDownList>
								&nbsp;<asp:DropDownList class="filter2" runat="server" id="drpYearChange" style="width:60px">
										<asp:ListItem Value="2007">2007</asp:ListItem>
										<asp:ListItem Value="2008">2008</asp:ListItem>
										<asp:ListItem Value="2009">2009</asp:ListItem>
										<asp:ListItem Value="2010">2010</asp:ListItem>
										<asp:ListItem Value="2011">2011</asp:ListItem>
										<asp:ListItem Value="2012">2012</asp:ListItem>
										<asp:ListItem Value="2013">2013</asp:ListItem>
										<asp:ListItem Value="2014">2014</asp:ListItem>
										<asp:ListItem Value="2015">2015</asp:ListItem>
										<asp:ListItem Value="2016">2016</asp:ListItem>
										<asp:ListItem Value="2017">2017</asp:ListItem>
										<asp:ListItem Value="2018">2018</asp:ListItem>
										<asp:ListItem Value="2019">2019</asp:ListItem>
										<asp:ListItem Value="2020">2020</asp:ListItem>
									</asp:DropDownList> 
							</td>
							<td>
								<%# DataBinder.Eval(Container.DataItem, "name_on_card") %>
							</td>
							<td width="10"><img runat="server" src="~/images/spacer.gif" width="20" height="30"/></td>
						</tr>
					</ItemTemplate>
					<FooterTemplate>
						
					</FooterTemplate>
				</asp:Repeater>
				
				<!-- PayPal -->
				<tr Class="heading02" runat="server" id="trPaypal1">
					<td colspan="7" align="Left"><img runat="server" src="~/images/spacer.gif" width="1" height="40"/>Pay using PayPal</td>
				</tr>
				<tr class="lineItemEven" runat="server" id="trPaypal2">
					<td><img runat="server" src="~/images/spacer.gif" width="20" height="40"/></td>
					<td>								
						<asp:ImageButton id="imgPayPal" AlternateText="Select PayPal" runat="server" ImageUrl="~/images/button_select.gif" CausesValidation="False" OnClick="imgPayPal_Click"/>
					</td>
					<td colspan="4">
						&nbsp;<B>Pay using PayPal</B><br/>
						&nbsp;(You will be redirected to the PayPal secure website)<br/><br/>
					</td>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="20" height="40"/></td>
				</tr>
				
				<!-- New Credit Card -->
				<tr Class="heading02">
					<td colspan="7" align="Left"><img runat="server" src="~/images/spacer.gif" width="1" height="40"/>Pay with a new credit card</td>
				</tr>
				
				<tr>
					<td colspan="7">
					<table cellpadding="0" cellspacing="0" border="0" width="710" class="FullBorders">
						<tr>
							<td align="right" valign="middle" class="filter2" bgcolor="#ededed"><br>
							<font color="red">*</font> <b>Name On Card:</b>&nbsp;
							</td>
							<td bgcolor="#ededed"><br>
								<asp:TextBox ID="txtNameOnCard" class="Filter2" style="width:350px" MaxLength="100" runat="server"/>
								<asp:RequiredFieldValidator ID="rftxtNameOnCard" ControlToValidate="txtNameOnCard" Text="*" ErrorMessage="Name on Card is a required field." Display="Dynamic" runat="server"/>
							</td>
						</tr>
						<tr>
							<td align="right" valign="middle" class="filter2">
							<font color="red">*</font> <b>Card Type:</b>&nbsp; 
							</td>
							<td colspan="3">
								<Kaneva:CardTypesListBox class="filter2" id="drpCardType" runat="server" style="width:125px" Height="80px" Rows="1">
									<asp:ListItem Value="CardTypes Here">CardTypes Here</asp:ListItem>
								</Kaneva:CardTypesListBox>							
							</td>
						</tr>		
						<tr>
							<td align="right" valign="middle" class="filter2" bgcolor="#ededed">
							<font color="red">*</font> <b>Card Number:</b>&nbsp;
							</td>
							<td bgcolor="#ededed">
								<asp:TextBox ID="txtCardNumber" class="Filter2" style="width:150px" MaxLength="16" runat="server"/>
								<Kaneva:CreditCardValidator Id="ccvNumber" ControlToValidate="txtCardNumber" ErrorMessage="Please enter a valid credit card number" Display="none" runat="server" CardTypesListBox="drpCardType" />	
							</td>
						</tr>
						<tr>
							<td align="right" valign="middle" class="filter2">
							<font color="red">*</font> <b>Expiration Date:</b>&nbsp; 
							</td>
							<td>
								<asp:DropDownList class="filter2" id="drpMonth" runat="Server" style="width:80px">
									<asp:ListItem Value="01">01 (Jan)</asp:ListItem>
									<asp:ListItem Value="02">02 (Feb)</asp:ListItem>
									<asp:ListItem Value="03">03 (Mar)</asp:ListItem>
									<asp:ListItem Value="04">04 (Apr)</asp:ListItem>
									<asp:ListItem Value="05">05 (May)</asp:ListItem>
									<asp:ListItem Value="06">06 (Jun)</asp:ListItem>
									<asp:ListItem Value="07">07 (Jul)</asp:ListItem>
									<asp:ListItem Value="08">08 (Aug)</asp:ListItem>
									<asp:ListItem Value="09">09 (Sep)</asp:ListItem>
									<asp:ListItem Value="10">10 (Oct)</asp:ListItem>
									<asp:ListItem Value="11">11 (Nov)</asp:ListItem>
									<asp:ListItem Value="12">12 (Dec)</asp:ListItem>
								</asp:DropDownList>
								&nbsp;<asp:DropDownList class="filter2" runat="server" id="drpYear" style="width:60px"> 
									<asp:ListItem Value="2007">2007</asp:ListItem>
									<asp:ListItem Value="2008">2008</asp:ListItem>
									<asp:ListItem Value="2009">2009</asp:ListItem>
									<asp:ListItem Value="2010">2010</asp:ListItem>
									<asp:ListItem Value="2011">2011</asp:ListItem>
									<asp:ListItem Value="2012">2012</asp:ListItem>
									<asp:ListItem Value="2013">2013</asp:ListItem>
									<asp:ListItem Value="2014">2014</asp:ListItem>
									<asp:ListItem Value="2015">2015</asp:ListItem>
									<asp:ListItem Value="2016">2016</asp:ListItem>
									<asp:ListItem Value="2017">2017</asp:ListItem>
									<asp:ListItem Value="2018">2018</asp:ListItem>
									<asp:ListItem Value="2019">2019</asp:ListItem>
									<asp:ListItem Value="2020">2020</asp:ListItem>
								</asp:DropDownList>
							</td>
						</tr>
						<tr>
							<td colspan="4"><br>
							</td>
						</tr>	
						</table>
					</td>
				</tr>
				
				<tr>
					<td colspan="7" align="right"><br>
						<asp:button id="btnAddCard" runat="Server" CausesValidation="True" onClick="btnAddCard_Click" class="Filter2" Text="   add a new card   "/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</td>
				</tr>
				
			</table>
			
			
			
		</td>	
	</tr>
</table>
</center>
