///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections;

using System.Web.Security;
using System.Security.Principal;
using System.Security.Cryptography;
using log4net;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for kPointSelection.
	/// </summary>
	public class kPointSelection : StoreBasePage
	{
		protected kPointSelection () 
		{
			Title = "Purchase Item";
		}

		/// <summary>
		/// Page_Load
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Page_Load (object sender, System.EventArgs e)
		{
            aRegister.HRef = ResolveUrl(KanevaGlobals.JoinLocation);
            
            Response.Expires = 0;
			Response.CacheControl = "no-cache";

			// Make sure they have access to this asset before continueing
			// 1) It must be published (status is active)
			// 2) If in a private channel, they must be a member
			// 3) If it is mature content, they must be loggin in and of age

			int assetId = GetAssetId ();
			DataRow drAsset = StoreUtility.GetAsset (assetId);

			if (drAsset == null || drAsset ["asset_id"].Equals (DBNull.Value) || drAsset ["status_id"].Equals ((int) Constants.eASSET_STATUS.MARKED_FOR_DELETION) || drAsset ["status_id"].Equals ((int) Constants.eASSET_STATUS.DELETED))
			{
				// Couldn't find the asset as published
				Response.Redirect (ResolveUrl ("~/asset/notAvailable.aspx?M=NA"));
			}
			else
			{
				// Is it mature?
				if (IsMature (Convert.ToInt32 (drAsset ["asset_rating_id"])))
				{
					if (!IsCurrentUserAdult (false))
					{
						// The user is not an adult
						Response.Redirect (ResolveUrl ("~/asset/notAvailable.aspx?M=ADU"));
					}
				}
			}

			if (!IsPostBack)
			{
				// Update the user IP address if signed to handle case where auto login is true
				// Mantis tracking 651 (http://qa3/tracking/view.php?id=651) 
				if (Request.IsAuthenticated)
				{
					UsersUtility.UpdateLastIP (GetUserId (), Request.UserHostAddress);
				}

				lblHeading.Text = "Purchase Item";
				lblMinimumKPoints.Text = "(" + FormatKpoints (MiniumKPointPurchase ()) + " minimum)";

				trImmediatePurchase.Visible = true;
				DisplayScreen ();
				DisplayAsset (drAsset);
			}

			// Update the visibilities
			UpdateVisibility (Request.IsAuthenticated);

			// Set up option javascripts
			rdoNoThanks.Attributes.Add ("onclick", "javascript:document.frmMain." + txtKPoints.ClientID + ".disabled=true;document.frmMain." + drpKPoints.ClientID + ".disabled=true;ValidatorEnable(document.getElementById ('" + rngtxtKPoints.ClientID + "'),false);ValidatorEnable(document.getElementById ('" + rfdtxtKPoints.ClientID + "'),false);");
			rdoCustom.Attributes.Add ("onclick", "javascript:document.frmMain." + txtKPoints.ClientID + ".disabled=false;document.frmMain." + drpKPoints.ClientID + ".disabled=true;ValidatorEnable(document.getElementById ('" + rngtxtKPoints.ClientID + "'),true);ValidatorEnable(document.getElementById ('" + rfdtxtKPoints.ClientID + "'),true);");
			rdoPackage.Attributes.Add ("onclick", "javascript:document.frmMain." + txtKPoints.ClientID + ".disabled=true;document.frmMain." + drpKPoints.ClientID + ".disabled=false;ValidatorEnable(document.getElementById ('" + rngtxtKPoints.ClientID + "'),false);ValidatorEnable(document.getElementById ('" + rfdtxtKPoints.ClientID + "'),false);");

			// Set up purchase button
			btnMakePurchase.CausesValidation = false;
			btnMakePurchase.Attributes.Add ("onclick", "javascript: if (typeof(Page_ClientValidate) == 'function') if (!Page_ClientValidate()){return false;};this.value='  Please wait...  ';this.disabled = true;ShowLoading(true,'Please wait. Your selection is being processed...');" + ClientScript.GetPostBackEventReference(this.btnMakePurchase, "", false));

			// Enter to log in
			string strJavascript = "<script language=\"JavaScript\">" +
				"function checkEnter(event){\n" +
				"if ((event.which && event.which == 13) || (event.keyCode && event.keyCode == 13))\n" +
				"{event.returnValue=false;event.cancel=true;" + ClientScript.GetPostBackEventReference  (this.imgLogin, "", false) + ";return false;}\n else \n{return true;}" +
				"}</script>";

            if (!ClientScript.IsClientScriptBlockRegistered(GetType (), "checkEnter"))
			{
                ClientScript.RegisterClientScriptBlock(GetType(), "checkEnter", strJavascript);	
			}
		}

		/// <summary>
		/// DisplayAsset
		/// </summary>
		private void DisplayAsset (DataRow drAsset)
		{
			int assetId = GetAssetId ();

			lblCreatedDate.Text = FormatDateTime (drAsset ["created_date"]);
			lblUserName.Text = drAsset ["username"].ToString ();
			lblType.Text = GetAssetTypeName (Convert.ToInt32 (drAsset ["asset_type_id"]));

			lblTeaser.Text = TruncateWithEllipsis (drAsset ["teaser"].ToString (), 200);
		}

		/// <summary>
  		/// Are we in Featured Item mode?
		/// </summary>
		private bool IsFeaturedItemMode
		{
			get
			{
				// Are they adding to cart
				if (Request ["feat"] != null)
				{
					return Request ["feat"].ToString ().Equals ("Y");
				}
				return false;
			}
		}

		/// <summary>
		/// Are we in bug 1 day pilot license mode?
		/// </summary>
		private bool IsBuying1DayPilotMode
		{
			get
			{
				// Are they adding to cart
				if (Request ["mode"] != null)
				{
					return Request ["mode"].ToString ().Equals ("1D");
				}
				return false;
			}
		}

		/// <summary>
		/// It is a buy now!
		/// </summary>
		private void DisplayScreen ()
		{
			int assetId = GetAssetId ();
			DataRow drAsset = StoreUtility.GetAsset (assetId);
			bool isGameSubscription = StoreUtility.IsGameSubscription (drAsset);
			Double dAssetAmount = 0.0;
			string itemName = "";
			int userId = GetUserId ();

			// Is it a featured item purchase?
			if (IsFeaturedItemMode)
			{
				dAssetAmount = GetFeaturedItemPrice ();
				itemName = "Feature '" + Server.HtmlDecode (drAsset ["name"].ToString ()) + "' for " + Constants.C_FEATURED_ASSET_MONTHS.ToString () + " month";

				// Don't Show the subscription dropdown
				drpSingleSubscription.Visible = false;
				aRecurringHelp.Visible = false;
				lblAssetAmount.Visible = true;
			}
				// Is it a game and buying a one day pilot game?
			else if (IsBuying1DayPilotMode)
			{
				dAssetAmount = Constants.C_PILOT_GAME_ONE_DAY_PRICE;
				itemName = "Purchase '" + Server.HtmlDecode (drAsset ["name"].ToString ()) + "' for " + Constants.C_PILOT_GAME_DAYS.ToString () + " day";

				// Don't Show the subscription dropdown
				drpSingleSubscription.Visible = false;
				aRecurringHelp.Visible = false;
				lblAssetAmount.Visible = true;
			}
				// Is it a game subscription or an asset purchase?
			else if (isGameSubscription)
			{
				// If the user has an active subscription to this game, forward them on to launch iris
				if (StoreUtility.HasActiveUserAssetSubscription (assetId, userId) || StoreUtility.IsAssetOwner (userId, assetId))
				{
					RedirectToDownload (assetId, 0);
				}

				// Is it a free game?
				dAssetAmount = Convert.ToDouble (drAsset ["amount"]);
				itemName = Server.HtmlDecode (drAsset ["name"].ToString ());

				// Get the subscriptions
				DataTable dtAssetSubs = StoreUtility.GetAssetSubscriptions (assetId, true);
				//System.Collections.Hashtable htSubs = GetSubscriptions (dtAssetSubs);

				drpSingleSubscription.DataTextField = "display";
				drpSingleSubscription.DataValueField = "asset_subscription_id";
				drpSingleSubscription.DataSource = dtAssetSubs;
				drpSingleSubscription.DataBind ();

				// Are there subscription to a pay game?
				if (dtAssetSubs.Rows.Count > 0 && dAssetAmount > 0.0)
				{
					// Show the subscription dropdown
					drpSingleSubscription.Visible = true;
					aRecurringHelp.Visible = true;
					lblAssetAmount.Visible = false;

					// Set the dropdown to the first one
					SetDropDownIndex (drpSingleSubscription, dtAssetSubs.Rows [0]["asset_subscription_id"].ToString ());
					dAssetAmount = Convert.ToDouble (dtAssetSubs.Rows [0]["amount"]);
				}
				else
				{
					// Do not show the subscription dropdown
					drpSingleSubscription.Visible = false;
					lblAssetAmount.Visible = true;
					lblAssetAmount.Text = "0";
				}
			}
			else
			{
				// Purchasing an asset

				// Do they already have an existing completed order for this asset?
				// And still allowed to download it?
				if (StoreUtility.IsUserAllowedToDownload (userId, assetId))
				{
					// Stream it?
					if (StoreUtility.IsAssetStreamable (drAsset))
					{
						RedirectToStream (assetId);
					}

					RedirectToDownload (assetId, 0);
				}

				dAssetAmount = Convert.ToDouble (drAsset ["amount"]);
				itemName = Server.HtmlDecode (drAsset ["name"].ToString ());

				// Don't Show the subscription dropdown
				drpSingleSubscription.Visible = false;
				aRecurringHelp.Visible = false;
				lblAssetAmount.Visible = true;
			}

			// If it is free go right to the asset download
			if (dAssetAmount.Equals (0.0) && !IsFeaturedItemMode)
			{
				// Change the heading
				lblHeading.Text = "Free Item Download";

				// Make sure they are at least registered and logged in
				if (Request.IsAuthenticated)
				{
					// If it is a pilot game, forward them on if they have a pilot license,
					// otherwise forward them to playPilot so they can buy 1 day or subscription
					if (StoreUtility.IsPilotGame (assetId) && !StoreUtility.HasActivePilotLicense (userId))
					{

						// Configurable if a user can play pilot games for free or needs a pilot subscrition
						if (System.Configuration.ConfigurationManager.AppSettings ["FreeToPlayPilot"] != null &&
                            Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["FreeToPlayPilot"]))
						{
							// It is free to play pilot games if FreeToPlayPilot is true
						}
						else
						{
							// It's pilot, If the user has an active 1 day subscription to this game, forward them on to launch iris
							if (StoreUtility.HasActiveUserAssetSubscription (assetId, userId))
							{
								RedirectToDownload (assetId, 0);
							}
							else
							{
								Response.Redirect ("~/checkout/playPilot.aspx?assetId=" + assetId);
							}
						}
					}
				}
				else
				{
					// Show just the login or register
					tblLogin.Visible = true;

					// Only if it is not a game allow them to get without registering (Or if the asset has require login to 1)
					//if (!StoreUtility.IsPilotGame (assetId))
					if (!StoreUtility.IsKanevaGame (drAsset) && !Convert.ToInt32 (drAsset ["require_login"]).Equals (1))
					{
						tblFree.Visible = true;
						btnFree_click (null, null);
					}
					
					return;
				}

				// Purchase the single asset now and redirect
				bool bMonthlyGameSub = false;
				PurchaseAsset (userId, (int) Constants.eORDER_STATUS.COMPLETED, ref bMonthlyGameSub);
			}

			lblAssetName2.Text = itemName;
			lblAssetAmount.Text = KanevaGlobals.FormatKPoints (dAssetAmount);

			UpdateScreen (dAssetAmount);
		}

		/// <summary>
		/// Update the K-Point Dropdown
		/// </summary>
		/// <param name="dOrderAmount"></param>
		private void UpdateScreen (Double dOrderAmount)
		{
			int assetId = GetAssetId ();
			int userId = GetUserId ();

			lblOrderTotal.Text = KanevaGlobals.FormatKPoints (dOrderAmount, true, false);

			Double dUserKPointTotal = GetUserPointTotal ();
			Double dKPointRemainder = dUserKPointTotal - dOrderAmount;
			
			Double dKpointsShortage = 0;
			Double dMinimunCustomAmount = MiniumKPointPurchase ();

			bool bNeedToPurchaseKPoints = (dKPointRemainder < 0);

			rdoNoThanks.Visible = !bNeedToPurchaseKPoints;
			lblNoThanks.Visible = !bNeedToPurchaseKPoints;

			if (bNeedToPurchaseKPoints)
			{
				dKpointsShortage = Math.Abs (dKPointRemainder);

				// Set minumum for the custom amount
				if (dKpointsShortage > dMinimunCustomAmount)
				{
					dMinimunCustomAmount = dKpointsShortage;
				}
			}
				
			// Set up KPoint dropdown
			DataTable dtPointBuckets;
			if (dKpointsShortage > 0)
			{
                dtPointBuckets = StoreUtility.GetActivePromotions(Constants.CURR_KPOINT, dKpointsShortage);
			}
			else
			{
                dtPointBuckets = StoreUtility.GetActivePromotions(Constants.CURR_KPOINT, 0);
			}

			// Always set these (the total may have changed)
			rngtxtKPoints.MinimumValue = dMinimunCustomAmount.ToString ();
			rngtxtKPoints.ErrorMessage = "Custom amount must be at least " + dMinimunCustomAmount.ToString () + ", up to a maximum of 1000000 (1 million).";

			rngtxtKPoints.Enabled = rdoCustom.Checked;

			// Set some initial values
			if (!IsPostBack)
			{
				txtKPoints.Text = dMinimunCustomAmount.ToString ();
			}
			else
			{
				// Set postback values
				txtKPoints.Enabled = rdoCustom.Checked;
				txtKPoints.Enabled = rdoCustom.Checked;
				drpKPoints.Enabled = rdoPackage.Checked;

				// Fix amount due to post back disabled issue
				if (txtKPoints.Text.Length == 0)
				{
					txtKPoints.Text = dMinimunCustomAmount.ToString ();;
				}
			}

			// If we didn't get any point buckets, make them purchase a custom amount
			if (dtPointBuckets.Rows.Count == 0)
			{
				// Making them purchase a custom amount here...
				DataRow drCustom = dtPointBuckets.NewRow ();
                drCustom["promotion_description"] = KanevaGlobals.FormatKPoints(dKpointsShortage, false, false) + " (" + KanevaGlobals.FormatCurrency(dKpointsShortage / 100) + ")";
                drCustom["promotion_id"] = "999999";
				drCustom ["kei_point_amount"] = dKpointsShortage;
				drCustom ["dollar_amount"] = dKpointsShortage / 100;
				drCustom ["free_points_awarded_amount"] = 0;
				dtPointBuckets.Rows.Add (drCustom);

				if (!IsPostBack)
				{
					// Default the radio
					if (bNeedToPurchaseKPoints)
					{
						// Default to Custom
						rdoCustom.Checked = true;	
					}
					else
					{
						// Default to "No Thanks"
						rdoNoThanks.Checked = true;
					}
					
					rdoPackage.Enabled = false;
					drpKPoints.Enabled = false;
				}
				else
				{
					// Handle case if they changed game subscriptions and "No Thanks" appeared or disappeared
					if (bNeedToPurchaseKPoints)
					{
						if (rdoNoThanks.Checked)
						{
							rdoCustom.Checked = true;
							txtKPoints.Enabled = true;

//							rdoPackage.Enabled = false;
//							drpKPoints.Enabled = false;
						}
					}
				}
			}
			else
			{
				// Here we can allow a point bucket
				if (!IsPostBack)
				{
					if (bNeedToPurchaseKPoints)
					{
						// Default to point bucket
						rdoPackage.Checked = true;
					}
					else
					{
						// Default to "No Thanks"
						rdoNoThanks.Checked = true;
						drpKPoints.Enabled = false;
					}
					
					txtKPoints.Enabled = false;
				}
				else
				{
					// Handle case if they changed game subscriptions and "No Thanks" appeared or disappeared
					if (bNeedToPurchaseKPoints)
					{
						if (rdoNoThanks.Checked)
						{
							rdoPackage.Checked = true;
							rdoPackage.Enabled = true;
							drpKPoints.Enabled = true;
							txtKPoints.Enabled = false;
						}
					}
				}
			}

			string origValue = drpKPoints.SelectedValue;

			drpKPoints.Items.Clear ();
            drpKPoints.DataTextField = "promotion_description";
            drpKPoints.DataValueField = "promotion_id";
			drpKPoints.DataSource = dtPointBuckets;
			drpKPoints.DataBind ();

			if (IsPostBack)
			{
				SetDropDownIndex (drpKPoints, origValue, false);
			}

			// Is a credit card purchase required?
			if (!bNeedToPurchaseKPoints)
			{
				// Add a "Please Select a Package"
				drpKPoints.Items.Insert (0, new ListItem ("Please Select a Package...", "0"));				
			}

			// See if they are coming from the change button from order confirm (or breadcrumb)
			// this would mean they already have an existing order and point trasaction id
			if (!IsPostBack)
			{
				// Note, per Curtis, only do this if coming from order confirm...
				// XXX
				DataRow drOrder = StoreUtility.GetOrder (assetId, userId, Constants.eORDER_STATUS.CHECKOUT);
				if (drOrder != null && drOrder ["point_transaction_id"] != DBNull.Value)
				{
					// They already tried to order this item
					int pointTransactionId = Convert.ToInt32 (drOrder ["point_transaction_id"]);
					DataRow drPurchasePointTransaction = StoreUtility.GetPurchasePointTransaction (pointTransactionId);

					// Where they purchasing k-points?
					if (drPurchasePointTransaction != null)
					{
						// Was it a point bucket purchase?
                        if (drPurchasePointTransaction["promotion_id"] != DBNull.Value)
						{
                            SetDropDownIndex(drpKPoints, drPurchasePointTransaction["promotion_id"].ToString(), false);
							rdoPackage.Checked = true;
							drpKPoints.Enabled = true;
							txtKPoints.Enabled = false;
						}
						else
						{
							txtKPoints.Text = drPurchasePointTransaction ["totalPoints"].ToString ();
							rdoCustom.Checked = true;
							drpKPoints.Enabled = false;
							txtKPoints.Enabled = true;
						}
					}
				}
			}

			// Buy Points label visibility
			lblBuyPoints.Visible = (!bNeedToPurchaseKPoints);
			
			lblInsufficientPoints.Visible = (bNeedToPurchaseKPoints);
			lblMinimumKPoints.Visible = (bNeedToPurchaseKPoints);
		}


		/// <summary>
		/// GetUserPointTotal
		/// </summary>
		/// <returns></returns>
		private Double GetUserPointTotal ()
		{
			return UsersUtility.GetUserPointTotal (GetUserId ());
		}

		/// <summary>
		/// They changed the dropdown
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void drpSubscription_Change (Object sender, EventArgs e)
		{
			DataRow drCommSubscription = StoreUtility.GetAssetSubscription (Convert.ToInt32 (drpSingleSubscription.SelectedValue));
			Double dOrderAmount = Convert.ToDouble (drCommSubscription ["amount"]);
			UpdateScreen (dOrderAmount);
		}

		/// <summary>
		/// Show/Hide stuff based on login
		/// </summary>
		/// <param name="loggedIn"></param>
		private void UpdateVisibility (bool loggedIn)
		{
			// Not free from here down, make sure thier email is validated
			if (loggedIn && KanevaWebGlobals.CurrentUser.StatusId.Equals ((int) Constants.eUSER_STATUS.REGNOTVALIDATED))
			{
				lblEmail.Text = KanevaWebGlobals.CurrentUser.Email;
				pnlNotRegistered.Visible = true;
				tblLogin.Visible = false;
				pnlPurchase.Visible = false;
			}
			else
			{
				pnlNotRegistered.Visible = false;
				tblLogin.Visible = (!loggedIn);
				pnlPurchase.Visible = loggedIn;
			}
		}

		
		/// <summary>
		/// btnRegEmail_Click
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnRegEmail_Click (object sender, EventArgs e) 
		{
            MailUtilityWeb.SendRegistrationEmail(KanevaWebGlobals.CurrentUser.Username, KanevaWebGlobals.CurrentUser.Email, KanevaWebGlobals.CurrentUser.KeyValue);
			ShowErrorOnStartup ("Your validation email has been sent.", false);
		}

		/// <summary>
		/// Continue Shopping click
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnContinueShopping_Click (object sender, EventArgs e)
		{
			
		}


		/// <summary>
		/// The make purchase event handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnMakePurchase_Click (object sender, EventArgs e) 
		{		
			// Handle firefox problem here with ValidatorEnable javascript
			if (rdoNoThanks.Checked)
			{
				rngtxtKPoints.Enabled = false;
				rfdtxtKPoints.Enabled = false;
				txtKPoints.Enabled = false;
				drpKPoints.Enabled = false;
			}
			else if (rdoCustom.Checked)
			{
				rngtxtKPoints.Enabled = true;
				rfdtxtKPoints.Enabled = true;
				txtKPoints.Enabled = true;
				drpKPoints.Enabled = false;
			}
			else if (rdoPackage.Checked)
			{
				rngtxtKPoints.Enabled = false;
				rfdtxtKPoints.Enabled = false;
				txtKPoints.Enabled = false;
				drpKPoints.Enabled = true;
			}

			Page.Validate ();
			if (!Page.IsValid) 
			{
				return;
			}

			int assetId = GetAssetId ();
			int transactionId = 0;
			string itemName = "";
			
			// Get the order total
			Double dOrderTotal = GetCurrentOrderTotal ();

			// Get the user balances
			int userId = GetUserId ();
			Double dMpointBalance = UsersUtility.getUserBalance (userId, Constants.CURR_MPOINT);
			Double dKpointBalance = UsersUtility.getUserBalance (userId, Constants.CURR_KPOINT);
			Double dTotalPointBalance = dMpointBalance + dKpointBalance;

			// First update the screen
			UpdateScreen (dOrderTotal);

			string strNotEnoughKPoints = "Not enough K-Points to purchase this item! Please buy more K-Points.";

			// Attempting to make a purchase

			// Validate amounts
			int selectedKPointBucket = Convert.ToInt32 (drpKPoints.SelectedValue);
			int orderId = 0;

			// Create an order for the asset
			bool bMonthlyGameSub = false;
			orderId = PurchaseAsset (userId, (int) Constants.eORDER_STATUS.CHECKOUT, ref bMonthlyGameSub);

			// Did they want to buy KPoints package?
			if (selectedKPointBucket > 0 && rdoPackage.Checked)
			{
                DataRow drPointBucket = StoreUtility.GetPromotion(selectedKPointBucket);

				// No point bucket found here means we didn't have one big enough for this purchase
				// Custom amount
				if (drPointBucket == null)
				{
					// Validate and Charge credit card here
					ShowErrorOnStartup ("Error with order, please select a valid point bucket.");
					return;
				}
				else
				{
					// *** VALIDATE *** they are purchasing a big enough point bucket here
					if (dOrderTotal > (dTotalPointBalance + Convert.ToDouble (drPointBucket ["kei_point_amount"]) + Convert.ToDouble (drPointBucket ["free_points_awarded_amount"])))
					{
						ShowErrorOnStartup (strNotEnoughKPoints);
						return;
					}

					// Point Bucket item name
                    itemName = Server.HtmlEncode(drPointBucket["promotion_description"].ToString());

					// Record the transaction in the database, marked as checkout
					transactionId = StoreUtility.PurchasePoints (userId, (int) Constants.eTRANSACTION_STATUS.CHECKOUT, itemName, 0, (int) Constants.ePAYMENT_METHODS.KANEVA_CYBERSOURCE, 
						Convert.ToDouble (drPointBucket ["dollar_amount"]), Convert.ToDouble (drPointBucket ["kei_point_amount"]), Convert.ToDouble (drPointBucket ["free_points_awarded_amount"]), 0, Request.UserHostAddress);

					// Record the point bucket purchased
					StoreUtility.UpdatePointTransactionPointBucket (transactionId, Convert.ToInt32 (drPointBucket ["promotion_id"]));

					// Set the order id on the point purchase transaction
					StoreUtility.UpdatePointTransaction (transactionId, orderId);
					StoreUtility.UpdateOrderPointTranasactionId (orderId, transactionId);

					// Send them to select a payment method
					RedirectToPaymentSelection (assetId, orderId);
				}
			}
			else if (rdoCustom.Checked)
			{
				// Purchasing a custom amount
				Double kPointsCustomAmount = Convert.ToDouble (txtKPoints.Text);

				// Minimum of 1000 K-Points!!!
				if (kPointsCustomAmount < MiniumKPointPurchase ())
				{
					ShowErrorOnStartup ("Custom amount must be at least " + MiniumKPointPurchase ().ToString ());
					return;
				}

				// *** VALIDATE *** they are purchasing enough here
				if (dOrderTotal > (dTotalPointBalance + kPointsCustomAmount))
				{
					ShowErrorOnStartup (strNotEnoughKPoints);
					return;
				}

				// Custom point amount item name
				itemName = Server.HtmlEncode (KanevaGlobals.FormatKPoints (kPointsCustomAmount));

				// Record the transaction in the database, marked as pending
				transactionId = StoreUtility.PurchasePoints (userId, (int) Constants.eTRANSACTION_STATUS.CHECKOUT, itemName, 0, (int) Constants.ePAYMENT_METHODS.KANEVA_CYBERSOURCE, 
					StoreUtility.ConvertKPointsToDollars (kPointsCustomAmount), kPointsCustomAmount, 0, 0, Request.UserHostAddress);

				// Set the order id on the point purchase transaction
				StoreUtility.UpdatePointTransaction (transactionId, orderId);
				StoreUtility.UpdateOrderPointTranasactionId (orderId, transactionId);

				// Send them to select a payment method
				RedirectToPaymentSelection (assetId, orderId);
			}
			else if (bMonthlyGameSub)
			{
				// This means they are purchasing a montly game sub but no additional k-points
					
				// NO thanks case. Force credit card for monthly games
				itemName = "0 Kp - Monthly Game Subscription";

				// Record the transaction in the database, marked as pending
				transactionId = StoreUtility.PurchasePoints (userId, (int) Constants.eTRANSACTION_STATUS.CHECKOUT, itemName, 0, (int) Constants.ePAYMENT_METHODS.NOCHARGE, 
					0.0, 0.0, 0.0, 0, Request.UserHostAddress);

				// Set the order id on the point purchase transaction
				StoreUtility.UpdatePointTransaction (transactionId, orderId);
				StoreUtility.UpdateOrderPointTranasactionId (orderId, transactionId);

				// If monthly redirect to payment selection
				RedirectToPaymentSelection (assetId, orderId);
			}
			
			// They are not purchasing k-points from here down
			// Either 'No Thanks' or left on 'Please select a point bucket'
			StoreUtility.UpdateOrderPointTranasactionId (orderId, 0);

			// *** VALIDATE *** A final check
			if (dOrderTotal > (dTotalPointBalance))
			{
				ShowErrorOnStartup (strNotEnoughKPoints);
				return;
			}

			// *****************************************************************************
			// Purchasing item(s) and not buying additional K-points, we have enough
			// so go right to confirm
			// *****************************************************************************
			RedirectToConfirmOrder (assetId, orderId);
		}

		/// <summary>
		/// Purchase an asset now
		/// </summary>
		private int PurchaseAsset (int userId, int orderStatusId, ref bool bMonthlyGameSub)
		{
			// Is it marked as completed?
			bool bOrderComplete = (orderStatusId.Equals ((int) Constants.eORDER_STATUS.COMPLETED));

			int orderId = 0;
			int assetId = GetAssetId ();
			int orderItemId = 0;

			// See if they already have an order id for this asset (We should just reuse it)
			DataRow drOrder = StoreUtility.GetOrder (assetId, userId, Constants.eORDER_STATUS.CHECKOUT);

			if (drOrder == null)
			{
				// Create a new order
				orderId = StoreUtility.CreateOrder (userId, Request.UserHostAddress, orderStatusId);
				
			}
			else
			{
				// They already tried to order this item
				orderId = Convert.ToInt32 (drOrder ["order_id"]);

				// This caused a bug in royalties calculation, make sure we update the royalties (and order price)
				// with the below function InsertOrderItem
//				PagedDataTable pdtItems = StoreUtility.GetOrderItems (orderId, "");
//				orderItemId = Convert.ToInt32 (pdtItems.Rows [0]["order_item_id"]);
			}

			drOrder = StoreUtility.GetOrder (orderId);
			orderItemId = StoreUtility.InsertOrderItem (drOrder, assetId, 1, GenerateToken (orderId));

			// Add the featured item purchase if needed
			if (IsFeaturedItemMode)
			{
				// Create the featured
				Constants.eFEATURED_ASSET_STATUS faStatus = (bOrderComplete)? Constants.eFEATURED_ASSET_STATUS.ACTIVE : Constants.eFEATURED_ASSET_STATUS.PENDING;
				int featuredAssetId = StoreUtility.InsertFeaturedAsset (userId, assetId, 1, faStatus);

				// Set the purchase type
				StoreUtility.SetPurchaseType (orderId, Constants.ePURCHASE_TYPE.FEATURED_ITEM);

				StoreUtility.UpdateOrderItemFeatured (featuredAssetId, GetFeaturedItemPrice (), orderItemId);
				if (bOrderComplete)
				{
					// Finalize the purchase
					StoreUtility.PurchaseOrderNow (userId, orderId, Request.UserHostAddress);
					MailUtility.SendPurchaseEmail (userId, orderId, assetId, true);
					RedirectToDownload (assetId, orderId);
				}
			}
				// Is it a pilot game purchase?
			else if (IsBuying1DayPilotMode)
			{
				// Set the purchase type
				StoreUtility.SetPurchaseType (orderId, Constants.ePURCHASE_TYPE.DAY_PILOT_LICENSE);

				// Update the order item
				StoreUtility.UpdateOrderItemPilotOneDay (Constants.C_PILOT_GAME_ONE_DAY_PRICE, orderItemId, assetId);
				if (bOrderComplete)
				{
					// Finalize the purchase
					StoreUtility.PurchaseOrderNow (userId, orderId, Request.UserHostAddress);
					MailUtility.SendPurchaseEmail (userId, orderId, assetId, false);
					MailUtility.SendOwnerEmail (orderId, assetId);
					RedirectToDownload (assetId, orderId);
				}
			}
				// Add the user subscription if needed
			else if (StoreUtility.IsGameSubscription (assetId))
			{
				// Set the purchase type
				StoreUtility.SetPurchaseType (orderId, Constants.ePURCHASE_TYPE.GAME_SUBSCRIPTION);

				StoreUtility.UpdateOrderItemAssetSubscription (assetId, Convert.ToInt32 (drpSingleSubscription.SelectedValue), orderItemId);

				// Check for monthly, we need to capture a credit card for recurring payments
				int assetSubscriptionId = Convert.ToInt32 (drpSingleSubscription.SelectedValue);
				DataRow drAssetSub = StoreUtility.GetAssetSubscription (assetSubscriptionId);
				if (Convert.ToInt32 (drAssetSub ["length_of_subscription"]).Equals ((int) Constants.eASSET_SUBSCRIPTION_LENGTH.MONTHLY))
				{
					bMonthlyGameSub = true;
				}

				if (bOrderComplete)
				{
					// Finalize the purchase
					StoreUtility.PurchaseOrderNow (userId, orderId, Request.UserHostAddress);
					MailUtility.SendPurchaseEmail (userId, orderId, assetId, false);
					MailUtility.SendOwnerEmail (orderId, assetId);
					RedirectToDownload (assetId, orderId);
				}
			}
			else
			{
				StoreUtility.UpdateOrderItemAsset (assetId, orderItemId);

				// Must be an asset purchase
				StoreUtility.SetPurchaseType (orderId, Constants.ePURCHASE_TYPE.ASSET);
			}

			// Finalize the purchase?
			if (bOrderComplete)
			{
				StoreUtility.PurchaseOrderNow (userId, orderId, Request.UserHostAddress);
			
				MailUtility.SendPurchaseEmail (userId, orderId, assetId, false);
				MailUtility.SendOwnerEmail (orderId, assetId);

				// Stream it?
				if (StoreUtility.IsAssetStreamable (assetId))
				{
					RedirectToStream (assetId);
				}

				RedirectToDownload (assetId, orderId);	
			}

			return orderId;
		}

		/// <summary>
		/// Register link
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnRegister_click (object sender, EventArgs e) 
		{
            Response.Redirect(ResolveUrl(KanevaGlobals.JoinLocation));
		}

		/// <summary>
		/// They clicked no register but want to download a free item
		/// </summary>
		protected void btnFree_click (object sender,  System.Web.UI.ImageClickEventArgs e)
		{
			// Save it to orders, but no user is associated with it!
			int orderId = StoreUtility.CreateOrder (GetUserId (), Request.UserHostAddress, (int) Constants.eORDER_STATUS.COMPLETED);
			DataRow drOrder = StoreUtility.GetOrder (orderId);
			int assetId = GetAssetId ();

			// Add the item
			int orderItemId = StoreUtility.InsertOrderItem (drOrder, assetId, 1, GenerateToken (orderId));

			StoreUtility.UpdateFreeOrder (orderId, assetId);

			// Stream it?
			if (StoreUtility.IsAssetStreamable (assetId))
			{
				RedirectToStream (assetId, true);
			}

			RedirectToDownload (assetId, orderId, true);
		}
		

		/// <summary>
		/// The login click event handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnLogin_click (object sender, EventArgs e) 
		{
			Double dOrderTotal = 0.0;
			int userId = 0;

			int assetId = GetAssetId ();
			DataRow drAsset = StoreUtility.GetAsset (assetId);
			bool isGameSubscription = StoreUtility.IsGameSubscription (drAsset);

			dOrderTotal = Convert.ToDouble (drAsset ["amount"]);

			// Try to log in here
			if (Login (txtUserName.Value, txtPassword.Value, ref userId))
			{
				if (dOrderTotal.Equals (0.0))
				{
					// It is free so go right to the asset download
					/// Purchase the single asset (buy now)
					bool bMonthlyGameSub = false;
					PurchaseAsset (userId, (int) Constants.eORDER_STATUS.COMPLETED, ref bMonthlyGameSub);
				}
				else
				{
					// Stay on same page
					Response.Redirect (Request.Url.AbsoluteUri);
				}
			}
		}

		/// <summary>
		/// Redirect to download screen
		/// </summary>
		/// <param name="assetId"></param>
		private void RedirectToPaymentSelection (int assetId, int orderId)
		{
			Session ["assetId"] = assetId;
			Session ["orderId"] = orderId;

			if (IsFeaturedItemMode)
			{
				Session ["feat"] = "Y";
			}
			else
			{
				Session ["feat"] = null;
			}

			Response.Redirect (ResolveUrl ("~/checkout/paymentSelection.aspx"));
		}

		/// <summary>
		/// Redirect to download screen
		/// </summary>
		/// <param name="assetId"></param>
		private void RedirectToConfirmOrder (int assetId, int orderId)
		{
			Session ["assetId"] = assetId;
			Session ["orderId"] = orderId;

			if (IsFeaturedItemMode)
			{
				Session ["feat"] = "Y";
			}
			else
			{
				Session ["feat"] = null;
			}

			Response.Redirect (ResolveUrl ("~/checkout/orderConfirm.aspx"));
		}


		/// <summary>
		/// Redirect to download screen
		/// </summary>
		/// <param name="assetId"></param>
		private void RedirectToDownload (int assetId, int orderId)
		{
			RedirectToDownload (assetId, orderId, false);
		}

		/// <summary>
		/// Redirect to download screen
		/// </summary>
		/// <param name="assetId"></param>
		private void RedirectToDownload (int assetId, int orderId, bool noRegistrationForFree)
		{
			// If it is a featured item, don't launch iris.
			if (IsFeaturedItemMode)
			{
				Response.Redirect (ResolveUrl ("~/checkout/orderCompleteFeaturedItem.aspx?assetId=" + assetId + GetCommunityIdQueryString ()));
			}

			string free = (noRegistrationForFree) ? "1": "0";
			Response.Redirect (ResolveUrl ("~/checkout/launchIris.aspx?orderId=" + orderId + "&assetId=" + assetId + "&free=" + free));
		}

		/// <summary>
		/// RedirectToStream
		/// </summary>
		/// <param name="assetId"></param>
		private void RedirectToStream (int assetId)
		{
			RedirectToStream (assetId, false);
		}

		/// <summary>
		/// RedirectToStream
		/// </summary>
		/// <param name="assetId"></param>
		private void RedirectToStream (int assetId, bool noRegistrationForFree)
		{
			//string free = (noRegistrationForFree) ? "1": "0";
			//Response.Redirect (ResolveUrl ("~/checkout/stream.aspx?assetId=" + assetId + "&free=" + free));
			Response.Redirect (ResolveUrl ("~/asset/" + assetId + ".storeItem"));
		}

		/// <summary>
		/// Login the user
		/// </summary>
		/// <param name="username"></param>
		/// <param name="password"></param>
		/// <returns></returns>
		private bool Login (string email, string password, ref int user_id)
		{
			// May want to persist login info later.
			bool bPersistLogin = false;
			int roleMembership = 0;

			//			Kaneva.Authorization.KEPAuth wsKepAuth = new Kaneva.Authorization.KEPAuth ();
			//			wsKepAuth.Timeout = -1;
			//			int validLogin = wsKepAuth.authorize ( userName, password, 0, ref roleMembership);

			int validLogin = UsersUtility.Authorize (email, password, 0, ref roleMembership, Request.UserHostAddress, true);

			string results = "";

			switch (validLogin)       
			{       
				case 0:  
					results = "Not authenticated.";
					break; 
				case (int) Constants.eLOGIN_RESULTS.NOT_VALIDATED:
					LoginUser (email, bPersistLogin);
					break;   
				case (int) Constants.eLOGIN_RESULTS.SUCCESS:  
					LoginUser (email, bPersistLogin);
					break;   
				case (int) Constants.eLOGIN_RESULTS.USER_NOT_FOUND: 
					UsersUtility.InsertUserLoginIssue (0, Request.UserHostAddress, "Email '" + email + "' not found");
					results = "Email '" + email + "' was not found.";
					break;                  
				case (int) Constants.eLOGIN_RESULTS.INVALID_PASSWORD:   
				{
					UsersUtility.InsertUserLoginIssue (0, Request.UserHostAddress, "Invalid password for username '" + email + "'");
					m_logger.Warn ("Failed sign in (invalid password) for email " + email + " from IP " + Request.UserHostAddress);
					results = "Invalid password.";
					break;
				}
				case (int) Constants.eLOGIN_RESULTS.NO_GAME_ACCESS:   
					results = "No access to this game.";
					break;       
				case (int) Constants.eLOGIN_RESULTS.ACCOUNT_DELETED:
					results = "This account has been deleted.";
					break;    
				case (int) Constants.eLOGIN_RESULTS.ACCOUNT_LOCKED:
				{
					UsersUtility.InsertUserLoginIssue (0, Request.UserHostAddress, "Locked account email '" + email + "' tried to sign in");
					m_logger.Warn ("Locked account " + email + " tried to sign in from IP " + Request.UserHostAddress);
					results = "This account has been locked by the Kaneva administrator";
					break; 
				}
				default:            
					results = "Not authenticated.";           
					break;      
			}

			// Did they fail login?
			if (results.Length > 0)
			{
				// Show an alert
				string strScript = "<script language=JavaScript>";
				strScript += "alert(\"Login Failed: " + results + "\");";
				strScript += "</script>";

				if (!ClientScript.IsStartupScriptRegistered (GetType (), "invalidLogin"))
				{
                    ClientScript.RegisterStartupScript(GetType(), "invalidLogin", strScript);
				}

				return false;
			}

			return true;
		}

		/// <summary>
		/// LoginUser
		/// </summary>
		private void LoginUser (string email, bool bPersistLogin)
		{
			DataRow drUser = UsersUtility.GetUserFromEmail (email);
			int userId = Convert.ToInt32 (drUser ["user_id"]);
			FormsAuthentication.SetAuthCookie (drUser ["userName"].ToString (), bPersistLogin);

			UsersUtility.UpdateLastLogin (userId, Request.UserHostAddress);

			// Set the userId in the session for keeping track of current users online
			Session ["userId"] = userId;
		}

		/// <summary>
		/// Returns the user id who should recieve royalties
		/// </summary>
		/// <param name="assetId"></param>
		/// <returns></returns>
		private int GetUserIdToRecieveRoyalties (int assetId)
		{
			return GetUserIdToRecieveRoyalties (StoreUtility.GetAsset (assetId));
		}	

		/// <summary>
		/// Returns the user id who should recieve royalties
		/// </summary>
		/// <param name="assetId"></param>
		/// <returns></returns>
		private int GetUserIdToRecieveRoyalties (DataRow drAsset)
		{
			return Convert.ToInt32 (drAsset ["owner_id"]);
		}

		/// <summary>
		/// Get the current order total
		/// </summary>
		/// <returns></returns>
		private Double GetCurrentOrderTotal ()
		{
			// One click buy mode
			int assetId = GetAssetId ();
			DataRow drAsset = StoreUtility.GetAsset (assetId);
			bool isGameSubscription = StoreUtility.IsGameSubscription (drAsset);

			if (IsFeaturedItemMode)
			{
				return GetFeaturedItemPrice ();
			}
			else if (IsBuying1DayPilotMode)
			{
				return Constants.C_PILOT_GAME_ONE_DAY_PRICE;
			}
			else if (isGameSubscription)
			{
				// Get the amount for the selected subscription
				int assetSubscriptionId = Convert.ToInt32 (drpSingleSubscription.SelectedValue);
				DataRow drAssetSubscription = StoreUtility.GetAssetSubscription (assetSubscriptionId);
				return Convert.ToDouble (drAssetSubscription ["amount"]);
			}
			else
			{
				return Convert.ToDouble (drAsset ["amount"]);
			} 
		}

		/// <summary>
		/// GetAssetId
		/// </summary>
		/// <returns></returns>
		public int GetAssetId ()
		{
			if (Request ["change"] != null && Request ["change"].Equals ("Y"))
			{
				return Convert.ToInt32 (Session ["assetId"]);
			}

			return Convert.ToInt32 (Request ["assetId"]);
		}

		/// <summary>
		/// IsGameSubscription
		/// </summary>
		/// <param name="assetId"></param>
		/// <returns></returns>
		public bool IsGameSubscription (int assetId)
		{
			return StoreUtility.IsGameSubscription (assetId);
		}

		/// <summary>
		/// IsGameSubscription
		/// </summary>
		/// <param name="assetId"></param>
		/// <returns></returns>
		public bool IsGameSubscription (DataRow drAsset)
		{
			return StoreUtility.IsGameSubscription (drAsset);
		}

		/// <summary>
		/// GetFeaturedItemPrice
		/// </summary>
		/// <returns></returns>
		private Double GetFeaturedItemPrice ()
		{
			// Free for an admin
			if (IsAdministrator ())
			{
				return 0;
			}
			else
			{
				return Constants.C_FEATURED_ASSET_PRICE;
			}
		}


		protected void imgReg_Click (object sender, System.Web.UI.ImageClickEventArgs e) 
		{
            Response.Redirect(ResolveUrl(KanevaGlobals.JoinLocation));
		}


		/// <summary>
		/// The login click event handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void imgLogin_Click (object sender, System.Web.UI.ImageClickEventArgs e) 
		{
			Double dOrderTotal = 0.0;
			int userId = 0;

			int assetId = GetAssetId ();
			DataRow drAsset = StoreUtility.GetAsset (assetId);
			bool isGameSubscription = StoreUtility.IsGameSubscription (drAsset);

			dOrderTotal = Convert.ToDouble (drAsset ["amount"]);

			// Try to log in here
			if (Login (txtUserName.Value, txtPassword.Value, ref userId))
			{
				if (dOrderTotal.Equals (0.0))
				{
					// It is free so go right to the asset download
					/// Purchase the single asset (buy now)
					bool bMonthlyGameSub = false;
					PurchaseAsset (userId, (int) Constants.eORDER_STATUS.COMPLETED, ref bMonthlyGameSub);
				}
				else
				{
					// Stay on same page
					Response.Redirect (Request.Url.AbsoluteUri);
				}
			}

		}

		/// <summary>
		/// MiniumKPointPurchase allowed
		/// </summary>
		/// <returns></returns>
		private Double MiniumKPointPurchase ()
		{
			return StoreUtility.ConvertDollarsToKPoints (KanevaGlobals.MinimumCreditCardTransactionAmount);
		}

        protected HtmlAnchor aRegister;
		protected Label lblHeading, lblAssetName2, lblAssetAmount;
		protected Label lblBuyPoints, lblInsufficientPoints, lblOrderTotal;

		protected HtmlTableCell tdLoginBG;
		protected HtmlTable tblLogin, tblFree;
		protected HtmlTableRow trImmediatePurchase;

		protected Panel pnlPurchase, pnlNotRegistered;

		protected Button btnMakePurchase;

		protected DropDownList drpKPoints;
		protected TextBox txtKPoints;

		protected RadioButton rdoPackage, rdoCustom, rdoNoThanks;
		protected Label lblNoThanks, lblMinimumKPoints;

		protected HtmlAnchor aRecurringHelp;

		// Validators
		protected RangeValidator rngtxtKPoints;
		protected RequiredFieldValidator rfdtxtKPoints;

		// Login
		protected HtmlInputText txtUserName;
		protected HtmlInputText txtPassword;

		protected ImageButton imgLogin, imgReg, imgFreeContent;

		protected CheckBox chkRememberLogin;

		protected DropDownList drpSingleSubscription;

		protected Label lblEmail;

		// The asset details
		protected Label lblType, lblCreatedDate, lblTeaser, lblUserName;

		// Logger
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
