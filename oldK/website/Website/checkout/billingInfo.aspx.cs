///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using log4net;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;
using KlausEnt.KEP.Kaneva.usercontrols;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for billingInfo.
	/// </summary>
	public class billingInfo : MainTemplatePage
	{
		protected billingInfo () 
		{
			Title = "Billing Information";
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Page_Load (object sender, System.EventArgs e)
		{
            if (!KanevaGlobals.EnableCheckout)
            {
                if (!IsAdministrator())
                {
                    Response.Redirect("~/error.aspx?error=" + (int)Constants.eERROR_TYPE.CHECKOUT_CLOSED);
                }
			}

			Response.Expires = 0;
			Response.CacheControl = "no-cache";

			int userId = GetUserId ();

            if (!IsPostBack)
            {
                VerifyUserAccess ();
                if (!KanevaWebGlobals.CurrentUser.HasWOKAccount)
                {
                    Response.Redirect ("~/error.aspx?error=" + (int) Constants.eERROR_TYPE.INVALID_ACCOUNT);
                }

                VerifyOrder ();

                DataTable dtAddresses = UsersUtility.GetAddresses (userId);
                litAddressCount.Text = "0";

                if (dtAddresses.Rows.Count > 0)
                {
                    dlAddress.DataSource = dtAddresses;
                    dlAddress.DataBind ();

                    tblSelectAddress.Visible = true;

                    // used for Javascript variable
                    litAddressCount.Text = dtAddresses.Rows.Count.ToString ();
                }

                SetCountryAndState ();
            }
            else
            {
                if (!KanevaWebGlobals.CurrentUser.HasWOKAccount)
                {
                    Response.Redirect ("~/error.aspx?error=" + (int) Constants.eERROR_TYPE.INVALID_ACCOUNT);
                }

                if (drpCountry.SelectedValue.ToUpper () == "US" ||
                    drpCountry.SelectedValue.ToUpper () == "CA" ||
                    drpCountry.SelectedValue.ToUpper () == "")
                {
                    // hide state text box used for international and show drop down
                    drpState.Style.Add ("display", "block");
                    txtState.Style.Add ("display", "none");
                }
                else
                {
                    // hide state drop down and show state text box used for international
                    drpState.Style.Add ("display", "none");
                    txtState.Style.Add ("display", "block");
                }
            }

			// Set Nav
            HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;

			//setup wizard nav bar
			ucWizardNavBar.ActiveStep = CheckoutWizardNavBar.STEP.STEP_2;

            string retUrl = "~/mykaneva/buycredits.aspx?orderId=" + Request["orderId"].ToString();

            ucWizardNavBar.StepOneHref = retUrl;

            if (KanevaGlobals.EnablePaypal || IsAdministrator())
            {
                divPaypalTop.Visible = true;
                divPaypalBot.Visible = true;
                pDesc.Style.Add ("margin-top", "-20px");
            }

            //disable paypal for pass purchases
            if (Request.QueryString["pass"] != null && Request.QueryString["pass"].ToString() == "true")
            {
                divPaypalTop.Visible = false;
                divPaypalBot.Visible = false;
            }
        }
		
		
		#region Helper Methods
		
		private void VerifyUserAccess()
		{
			// They must be logged in
			if (!Request.IsAuthenticated)
			{
				Response.Redirect (GetLoginURL ());
			}
		}

		private void VerifyOrder ()
		{
			if (Request ["orderId"] == null)
			{
				Response.Redirect ( "~/mykaneva/managecredits.aspx" );
			}

			try
			{
				int orderId = Convert.ToInt32 (Request ["orderId"]);			
				VerifyOrder (StoreUtility.GetOrder (orderId, GetUserId()));
			}
			catch
			{
				Response.Redirect ( "~/mykaneva/managecredits.aspx" );
			}
		}
		private void VerifyOrder (DataRow drOrder)
		{
			try				
			{
				// Verify user is owner of this order		  
				if (drOrder == null)
				{
					// User may be trying to view someone else's order
					m_logger.Warn ("User " + GetUserId () + " tried to view orderId " + Request ["orderId"].ToString () + " from IP " + Common.GetVisitorIPAddress());
					Response.Redirect ( "~/mykaneva/managecredits.aspx" );
				}

				if (Convert.ToInt32(drOrder["transaction_status_id"]) != (int) Constants.eORDER_STATUS.CHECKOUT)
				{
					m_logger.Warn ("User " + GetUserId () + " tried to view orderId " + Request ["orderId"].ToString () + " from IP " + Common.GetVisitorIPAddress() + ".  Order does not have a status of CART.");
					Response.Redirect ( "~/mykaneva/managecredits.aspx" );
				}
			}
			catch
			{
				Response.Redirect ( "~/mykaneva/managecredits.aspx" );
			}
		}

		protected string GetAddressNum(bool increment)
		{
			if (increment)
			{
				TableCount++;
			}
			return TableCount.ToString();
		}

        private void SetCountryAndState ()
        {
            string country = KanevaWebGlobals.CurrentUser.Country;

            // if us or canada, get the state
            if (country.ToUpper() == "US" || country.ToUpper() == "CA")
            {
            }

            // Countries
            drpCountry.DataTextField = "Name";
            drpCountry.DataValueField = "CountryId";
            drpCountry.DataSource = WebCache.GetCountries ();
            drpCountry.DataBind ();

            drpCountry.Items.Insert (0, new ListItem ("United States", Constants.COUNTRY_CODE_UNITED_STATES));
            drpCountry.Items.Insert (0, new ListItem ("select...", ""));

            // States
            drpState.DataTextField = "StateCode";
            drpState.DataValueField = "StateCode";
            drpState.DataSource = WebCache.GetStates ();
            drpState.DataBind ();
            drpState.Items.Insert (0, new ListItem ("select...", ""));

            // Set the country
            SetDropDownIndex (drpCountry, country);

            // If users profile is set to US or Canada
            if (drpCountry.SelectedValue == "US" || drpCountry.SelectedValue == "CA")
            {
                // hide state text box used for international and show drop down
                drpState.Style.Add ("display", "block");
                txtState.Style.Add ("display", "none");
            }
            else
            {
                // hide state drop down and show state text box used for international
                drpState.Style.Add ("display", "none");
                txtState.Style.Add ("display", "block");
            }
        }
		#endregion

		#region Event Handlers
		/// <summary>
		/// The continue event handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnSaveContinue_Click (object sender, ImageClickEventArgs e) 
		{			
			VerifyUserAccess();

			if (Request["orderId"] != null)
			{
				try
				{
                    string state = "";

                    // State is only required for US and Canada
					if (drpCountry.SelectedValue.Equals (Constants.COUNTRY_CODE_UNITED_STATES) || drpCountry.SelectedValue.Equals (Constants.COUNTRY_CODE_CANADA))
					{
						rfdrpState.Enabled = true;
						rftxtPostalCode.Enabled = true;
                        rftxtState.Enabled = false;
                        state = drpState.SelectedValue;
					}
					else
					{
						rfdrpState.Enabled = false;
						rftxtPostalCode.Enabled = true;
                        rftxtState.Enabled = false;
                        state = txtState.Text.Trim ();
					}

					Page.Validate ();

					if (!Page.IsValid) 
					{
						return;
					}

					// Get the order
					int orderId = Convert.ToInt32 (Request ["orderId"]);			
					DataRow drOrder = StoreUtility.GetOrder (orderId, GetUserId());

					VerifyOrder(drOrder);

					// Insert the new billing address
					int addressId = UsersUtility.InsertAddress (GetUserId (), Server.HtmlEncode (txtFullName.Text), Server.HtmlEncode (txtAddress1.Text), 
						Server.HtmlEncode (txtAddress2.Text), Server.HtmlEncode (txtCity.Text), Server.HtmlEncode (state),
						Server.HtmlEncode (txtPostalCode.Text), Server.HtmlEncode (txtPhoneNumber.Text), Server.HtmlEncode (drpCountry.SelectedValue));
			
					// Save the address to the order
					StoreUtility.UpdatePointTransactionAddressId (Convert.ToInt32 (drOrder ["point_transaction_id"]), addressId);

                    int transactionId = Convert.ToInt32 (drOrder["point_transaction_id"]);
                    StoreUtility.UpdatePointTransaction (transactionId, Constants.ePAYMENT_METHODS.KANEVA_CYBERSOURCE);

                    Response.Redirect("~/checkout/orderConfirm.aspx?orderId=" + orderId.ToString() + "&pass=" + (Request.QueryString["pass"] != null ? Request.QueryString["pass"].ToString() : ""), false);
				}
				catch (Exception exc)
				{
					m_logger.Error ("Error saving billing address to users order: UserId=" + GetUserId().ToString () + " : ", exc);

					spnMessage.InnerText = "An error occurred while trying to save your address.";
					return;
				}
			}
			else
			{
				Response.Redirect ("~/mykaneva/managecredits.aspx");
			}
		}

		protected void btnContinue_Click (object sender, ImageClickEventArgs e)
		{
			VerifyUserAccess();

			int addressId = 0;
			if (Request ["rbAddress"] != null)
			{
				if (KanevaGlobals.IsNumeric (Request ["rbAddress"].ToString ()))
				{
					addressId = Convert.ToInt32 (Server.HtmlEncode (Request ["rbAddress"].ToString ()));
				}
			}
			
			if (addressId == 0)
			{
				spnMessage.InnerText = "Please select an existing address or enter a new address.";
				return;
			}

			if (Request["orderId"] != null)
			{
				try
				{
					// Get the order
					int orderId = Convert.ToInt32 (Request ["orderId"]);			
					DataRow drOrder = StoreUtility.GetOrder (orderId, GetUserId());

					// Verify current order # belongs to user
					VerifyOrder(drOrder);

					// Save the address to the order
					StoreUtility.UpdatePointTransactionAddressId (Convert.ToInt32 (drOrder ["point_transaction_id"]), addressId);

                    int transactionId = Convert.ToInt32 (drOrder["point_transaction_id"]);
                    StoreUtility.UpdatePointTransaction (transactionId, Constants.ePAYMENT_METHODS.KANEVA_CYBERSOURCE);

                    Response.Redirect("~/checkout/orderConfirm.aspx?orderId=" + orderId.ToString() + "&pass=" + (Request.QueryString["pass"] != null ? Request.QueryString["pass"].ToString() : ""), false);
				}
				catch (Exception exc)
				{
					m_logger.Error ("Error adding billing address to users order: UserId=" + GetUserId().ToString () + " : ", exc);

					spnMessage.InnerText = "An error occurred while trying to save your address.";
					return;
				}
			}
			else
			{
				Response.Redirect ("~/mykaneva/managecredits.aspx");
			}	
		}

        protected void lbPaypal_Click (object sender, EventArgs e)
        {
            VerifyUserAccess ();

            if (Request["orderId"] != null)
            {
                int orderId = 0;

                try
                {
                    // Get the order
                    orderId = Convert.ToInt32 (Request["orderId"]);
                    DataRow drOrder = StoreUtility.GetOrder (orderId);
                    int pointTransactionId = Convert.ToInt32 (drOrder ["point_transaction_id"]);

                    StoreUtility.UpdatePointTransaction (pointTransactionId, Constants.ePAYMENT_METHODS.PAYPAL);
                    
                    Response.Redirect ("~/checkout/orderConfirm.aspx?orderId=" + orderId.ToString () + "&pass=" + (Request.QueryString["pass"] != null ? Request.QueryString["pass"].ToString () : ""), false);
                }
                catch (Exception exc)
                {
                    m_logger.Error ("Error updating payment type: UserId=" + KanevaWebGlobals.CurrentUser.UserId.ToString () + " : OrderId=" + orderId.ToString() + " : ", exc);

                    spnMessage.InnerText = "An error occurred while trying to process your order.";
                    return;
                }
            }
            else
            {
                Response.Redirect ("~/mykaneva/managecredits.aspx");
            }
        }

        #endregion
		
		#region Properties

		public int TableCount
		{
			get { return m_table_count; }
			set { m_table_count = value; }
		}

		#endregion

		#region Declerations

		private int m_table_count = 0;

		protected DataList					dlAddress;
		protected DropDownList				drpState, drpCountry;
		protected TextBox					txtFullName, txtAddress1, txtAddress2, txtCity, txtPostalCode, txtPhoneNumber, txtState;
        protected RequiredFieldValidator rfdrpState, rftxtPostalCode, rftxtState;
		protected Literal					litAddressCount;
        protected LinkButton                lbPaypal;

		protected HtmlTable					tblSelectAddress;
		protected HtmlContainerControl		spnMessage;
        protected HtmlContainerControl divPaypalTop, divPaypalBot;
        protected HtmlContainerControl pDesc;
		
		protected CheckoutWizardNavBar		ucWizardNavBar;

		// Logger
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
