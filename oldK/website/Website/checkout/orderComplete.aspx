<%@ Page language="c#" Codebehind="orderComplete.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.orderComplete" %>
<%@ Register TagPrefix="Kaneva" TagName="WizardNavBar" Src="../usercontrols/checkoutwizardnavbar.ascx" %>

<link href="../css/new.css" rel="stylesheet" type="text/css">
<link href="../css/shadow.css" rel="stylesheet" type="text/css">


<!-- Google Website Optimizer Tracking Script PREVIEW -->
<script type="text/javascript">
<asp:literal id="litGoogleOptimizer" runat="server"></asp:literal>
</script>
<!-- End of Google Website Optimizer Tracking Script -->



<table border="0" cellspacing="0" cellpadding="0" class="newcontainer" align="center">
	<tr>
		<td>
			<table  border="0" cellpadding="0" cellspacing="0" width="100%" class="newcontainerborder">
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
				<tr>
					<td class=""><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
					<td valign="top" class="newdatacontainer">
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="968" align="left"  valign="top">
								
									<table cellpadding="0" cellspacing="0" border="0"  width="100%">
										<tr>
											<td width="968" valign="top">

												<kaneva:wizardnavbar runat="server" id="ucWizardNavBar"/>

											</td>
										</tr>
										<tr>
											<td width="970" valign="top">
											
												<table border="0" cellspacing="0" cellpadding="0" width="100%">
													<tr>
														<!-- START WIZARD CONTENT -->
														<td valign="top" align="center">
															
															<table border="0" cellspacing="0" cellpadding="0">
																<tr>
																	<td width="670">
																		
																		<div class="module whitebg fullpage">
																		<span class="ct"><span class="cl"></span></span>
																				
																		<table border="0" cellspacing="0" cellpadding="0"  align="center" class="wizform">
																			<tr>
																				<td align="left">
																					
																					<table id="Table11" width="640" cellspacing="0" cellpadding="0" border="0">
																						<tr>
																							<td colspan="3">
																								<h1>View Receipt</h1>
																								<p align="left" style="padding-left:20px;">A confirmation receipt was sent to you by email.</p>
																								<table border="0" cellspacing="0" cellpadding="0" width="94%" align="center" class="data nopadding">
																									<tr>
																										<td colspan="2" style="padding: 8px 0 8px 5px;"><h2>Completed Order Summary</h2></td>
																									</tr>
																									<tr>
																										<td align="left" width="35%">Order Number</td>
																										<td align="left" width="65%"><asp:label id="lblOrderNumber" runat="server"/></td>
																									</tr>
																									<tr>
																										<td align="left">Description</td>
																										<td align="left"><asp:label id="lblDescription" runat="server"/></td>
																									</tr>
																									<tr>
																										<td align="left">Amount Paid</td>
																										<td align="left"><asp:label id="lblAmountPaid" runat="server"/></td>
																									</tr>
																									<tr>
																										<td align="left" class="bold">Credits Received</td>
																										<td align="left" class="bold"><asp:label id="lblKPoints" runat="server"/></td>
																									</tr>
																									<tr>
																										<td align="left">Transaction Date</td>
																										<td align="left"><asp:label id="lblDate" runat="server"/></td>
																									</tr>
																									<tr>
																										<td align="left">Payment Method</td>
																										<td align="left">
																											<table border="0" cellspacing="0" cellpadding="0" align="left" width="100%">
																												<tr>
																													<td align="left"><asp:label id="lblName" runat="server"/></td>
																												</tr>
																												<tr>
																													<td align="left"><asp:label id="lblCardType" runat="server"/></td>
																												</tr>
																												<tr>
																													<td align="left"><asp:label id="lblCardNumber" runat="server"/></td>
																												</tr>
																											</table>
																										</td>
																									</tr>
																									<tr id="trBillAddress" runat="server">
																										<td align="left">Billing Address</td>
																										<td align="left">
																											<table border="0" cellspacing="0" cellpadding="0" align="left" width="100%">
																												<tr>
																													<td align="left"><asp:label id="lblAddress1" runat="server"/>
																														<asp:label id="lblAddress2" runat="server"/></td>
																												</tr>
																												<tr>
																													<td align="left"><asp:label id="lblCity" runat="server"/>, <asp:label id="lblState" runat="server"/> <asp:label id="lblZip" runat="server"/></td>
																												</tr>
																												<tr>
																													<td align="left"><asp:label id="lblCountry" runat="server"/></td>
																												</tr>
																											</table>
																										</td>
																									</tr>
																								</table>
																								<br><br>
																								<table border="0" cellspacing="0" cellpadding="0" width="94%">
																									<tr>
																										<td align="right"><a href="~/kgp/playwok.aspx" runat="server"><img runat="server" src="~/images/wizard_btn_goinworld.gif" alt="Go In World" width="180" height="41" border="0"></a><br><br><a href="~/mykaneva/managecredits.aspx" runat="server">Back to Manage Credits</a></td>
																									</tr>
																								</table>
																							
																							</td>
																						</tr>
																					</table>
																					
																				</td>
																			</tr>
																		</table>
																		
																		<span class="cb"><span class="cl"></span></span>
																		</div>
																	</td>
																</tr>
															</table>
															
														</td>
														
														<!-- END WIZARD CONTENT -->
													</tr>
												</table>
											
											</td>
											
										</tr>
										
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td class=""><img runat="server" src="~/images/spacer.gif" id="Img5" width="1" height="1" /></td>
				</tr>
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
				
			</table>
		</td>
	</tr>
</table>

<script type="text/javascript"> 
try
{
  var _gaq = _gaq || []; 
  _gaq.push(['_setAccount', '<asp:literal id="litGAAcct" runat="server"></asp:literal>']); 
  _gaq.push(['_trackPageview']); 
  _gaq.push(['_addTrans', 
    '<asp:literal id="litOrderId1" runat="server"></asp:literal>',           // order ID - required 
    'Kaneva',  // affiliation or store name 
    '<asp:literal id="litOrderTotal" runat="server"></asp:literal>',          // total - required 
    '0',              // tax 
    '0',              // shipping 
    '<asp:literal id="litOrderCity" runat="server"></asp:literal>',       // city 
    '<asp:literal id="litOrderState" runat="server"></asp:literal>',     // state or province 
    '<asp:literal id="litOrderCountry" runat="server"></asp:literal>'             // country 
  ]); 
 
   // add item might be called for every item in the shopping cart 
   // where your ecommerce engine loops through each item in the cart and 
   // prints out _addItem for each  
  _gaq.push(['_addItem', 
    '<asp:literal id="litOrderId2" runat="server"></asp:literal>',           // order ID - required 
    '<asp:literal id="litItemSKU" runat="server"></asp:literal>',           // SKU/code 
    '<asp:literal id="litItemName" runat="server"></asp:literal>',        // product name 
    '<asp:literal id="litItemCategory" runat="server"></asp:literal>',   // category or variation 
    '<asp:literal id="litItemPrice" runat="server"></asp:literal>',          // unit price - required 
    '1'               // quantity - required 
  ]); 
  _gaq.push(['_trackTrans']); //submits transaction to the Analytics servers 
}
catch(err){}
</script> 

