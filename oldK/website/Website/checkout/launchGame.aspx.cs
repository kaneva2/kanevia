///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using log4net;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for launchGame.
	/// </summary>
	public class launchGame : MainTemplatePage
	{
		protected launchGame () 
		{
			Title = "Game Download Ready";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			int assetId = Convert.ToInt32 (Request ["assetId"]);

			DataRow drAsset = StoreUtility.GetAsset (assetId);
			Double amount = Convert.ToDouble (drAsset ["amount"]);

			lblGameName.Text = TruncateWithEllipsis (drAsset ["name"].ToString (), 30);
            lblVersion.Text = System.Configuration.ConfigurationManager.AppSettings["KGLVersion"].ToString(); ;

            string gameLauncherExe = System.Configuration.ConfigurationManager.AppSettings["KGLFilename"].ToString();
			string filePath = Server.MapPath ("~/download/launcher/" + assetId.ToString () + "/" + gameLauncherExe);

			if (!System.IO.File.Exists (filePath))
			{
				aGameLauncher.HRef = "javascript:alert('Game launcher was not found on server.');";
				return;
			}

			// if it is not free, they must have an active subscription
			if (Request.IsAuthenticated && ((amount.Equals (0.0)) || StoreUtility.HasActiveUserAssetSubscription (assetId, GetUserId ())))
			{
				// Update the number_of_downloads for this game
				//xxx


				aGameLauncher.HRef = ResolveUrl ("~/download/launcher/" + assetId.ToString () + "/" + gameLauncherExe);
			}
			else
			{
				// Don't have an active subscription
				m_logger.Warn ("User " + GetUserId () + " is not allowed to download this asset " + assetId + ". From IP " + Request.UserHostAddress);
				ShowErrorOnStartup ("You do not have an active subscription to this game! Please purchase the game first.");
			}
		}

		protected HtmlAnchor aGameLauncher;

		protected Label lblGameName;
		protected Label lblVersion;

		// Logger
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
