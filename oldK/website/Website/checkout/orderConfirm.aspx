<%@ Page language="c#" Codebehind="orderConfirm.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.orderConfirm" enableViewState="True"%>
<%@ Register TagPrefix="Kaneva" Namespace="KlausEnt.KEP.Kaneva.WebControls" Assembly="Kaneva.WebControls" %>
<%@ Register TagPrefix="Kaneva" TagName="WizardNavBar" Src="../usercontrols/checkoutwizardnavbar.ascx" %>

<script type="text/javascript" src="../jscript/prototype.js"></script>

<link href="../css/friends.css" rel="stylesheet" type="text/css"/>
<link href="../css/new.css" rel="stylesheet" type="text/css">
<link href="../css/shadow.css" rel="stylesheet" type="text/css">

<div id="divLoading">
	<table border="0" cellspacing="0" cellpadding="0" width="500" align="center" style="margin:50px 0 260px 0;">
		<tr>
			<td>
				<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100">
					<tr>
						<td class="frTopLeft"></td>
						<td class="frTop"></td>
						<td class="frTopRight"></td>
					</tr>
					<tr>
						<td class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
						<td valign="top" align="center" class="frBgIntMembers">
							<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100">
								<tr><th class="frBgIntMembers" id="divLoadingText">Loading...<div style="display:none;"><img src="../images/progress_bar.gif" ></div></th></tr>
							</table>
						</td>
						<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
					</tr>
					<tr>
						<td class="frBottomLeft"></td>
						<td class="frBottom"></td>
						<td class="frBottomRight"></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</div>


<div id="divData" style="DISPLAY:none">

<table border="0" cellspacing="0" cellpadding="0" class="newcontainer" align="center">
	<tr>
		<td>
			<table  border="0" cellpadding="0" cellspacing="0" width="100%" class="newcontainerborder">
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
				<tr>
					<td class=""><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
					<td valign="top" class="newdatacontainer">
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="968" align="left"  valign="top">
								
									<table cellpadding="0" cellspacing="0" border="0"  width="100%">
										<tr>
											<td width="968" valign="top">
												
												<kaneva:wizardnavbar runat="server" id="ucWizardNavBar"/>
												
											</td>
										</tr>
										<tr>
											<td width="970" valign="top">
											
												<table border="0" cellspacing="0" cellpadding="0" width="100%">
													<tr>
														<!-- START WIZARD CONTENT -->
														<td valign="top" align="center">
															
															<table border="0" cellspacing="0" cellpadding="0">
																<tr>
																	<td width="670">
																		
																		<div class="module whitebg">
																		<span class="ct"><span class="cl"></span></span>
																				
																		<table border="0" cellspacing="0" cellpadding="0"  align="center" class="wizform">
																			<tr>
																				<td align="left">
																					
																					<table id="Table11" width="640" cellspacing="0" cellpadding="0" border="0">
																						<tr>
																							<td colspan="3">
																								<h1>Make Purchase</h1>
																																																		   
																								<div style="padding:0px 10px 10px 10px;margin-top:-10px;">
																								<asp:validationsummary showmessagebox="False" showsummary="True" cssclass="alertmessage bold" enableclientscript="True"  id="valSum" displaymode="List" runat="server" headertext="<br/>Please correct the following errors:<br/>"/>
																								<asp:label cssclass="alertmessage bold" id="lblErrors" runat="server"/>
																								</div>																								   
																								
																								<table border="0" cellspacing="0" cellpadding="0" width="97%" align="center" class="data white nopadding" id="ordersummary">
																									<tr>
																										<td colspan="3" style="padding: 8px 0 8px 5px;"><h2>Order Summary <span class="hlink"><a runat="server" id="aEditOrder">edit order</a></span></h2></td>
																									</tr>
																									<tr class="altrow">
																										<td align="left" class="bold">Description</td>
																										<td align="right" class="bold" width="60">Price</td>
																									</tr>
																									<tr>
																										<td align="left"><span id="spnItemDescription" runat="server"/></td>
																										<td align="right">$<span id="spnPrice" runat="server"/></td>
																									</tr>
																									<tr>
																										<td align="right">Tax</td>
																										<td align="right"><span id="spnTax" runat="server">0.00</span></td>
																									</tr>
																									<tr class="altrow">
																										<td align="right" class="bold">Total</td>
																										<td align="right" class="bold">$<span id="spnTotal" runat="server" /></td>
																									</tr>
																								</table>
																								<br>
																							
																							</td>
																						</tr>
																						<tr>
																							<td valign="top" width="260" align="left">
																								<div class="module">
																								<span class="ct"><span class="cl"></span></span>
																								<h2>Billing Address</h2>
																								<p class="note">Billing address must match billing address of credit card used.</p>
																								<table cellspacing="0" cellpadding="1" border="0" width="88%">
																									<tr><td><span id="spnFullName" runat="server"/></td></tr>
																									<tr><td><span id="spnAddress1" runat="server"/></td></tr>
																									<tr><td><span id="spnAddress2" runat="server"/></td></tr>
																									<tr>
																										<td align="left"><span id="spnCity" runat="server"/>&nbsp;
																											<span id="spnState" runat="server"/>&nbsp;&nbsp;&nbsp;<span id="spnZip" runat="server"/>
																										</td>
																									</tr>
																									<tr><td><span id="spnCountry" runat="server"/></td></tr>
																									<tr><td><span id="spnPhone" runat="server"/></td></tr>
																									<tr>
																										<td><br><a runat="server" id="aChangeAddress">Add Address</a></td>
																									</tr>
																									<tr><td><img id="Img1" runat="server" src="~/images/spacer.gif" width="228" height="1" /></td></tr>
																								</table>
																								<span class="cb"><span class="cl"></span></span>
																								</div>
																								
																								<br/>
																								<div>
																								Your Credit Balance Summary:<br/><br/>
																								<table cellpadding="0" cellspacing="4" border="0" width="98%">
																									<tr>
																										<td>Current Balance:</td>
																										<td align="right"><span id="spnCreditBalance" runat="server" /></td>
																									</tr>
																									<tr>
																										<td>Amount Purchasing:</td>
																										<td align="right" style="border-bottom:1px solid #000000;"><span id="spnCreditPurchasing" runat="server" /></td>
																									</tr>
																									<tr>
																										<td>Balance After Purchase:</td>
																										<td align="right"><span id="spnNewCreditBalance" runat="server" /></td>
																									</tr>
																								</table>
																								
																								</div>
																								
																							</td>
																							
																							<td width="15"><img runat="server" src="~/images/spacer.gif" width="15" height="1" /></td>
																							
																							<td align="right" valign="top" width="370">
																								<div class="module">
																									<span class="ct"><span class="cl"></span></span>
																									<h2>Payment Method</h2>
																									<table border="0" cellspacing="0" cellpadding="0" width="92%" align="center" class="wizform">
																										<tr>
																											<td align="center">
																												
																												<table cellspacing="0" cellpadding="4" align="center" border="0">
																													<tr>
																														<td>
																															<div id="PopUpAddr" style="display:none;position:absolute;left:84px;bottom:470px;margin:0;padding:15px 15px 5px 15px;background:transparent url(../images/popup_point.gif) no-repeat bottom left;text-align:justify;font-size:12px;">
																																<div style="display:block;border:3px solid #f7ce52;padding:10px 10px 10px 10px;background-color:#FFFFFF;width:260px">
																																	<table border="0" cellspacing="0" cellpadding="3" width="250">
																																		<tr>
																																			<td>Using a credit card requires a matching address.</td>
																																		</tr>
																																		<tr>
																																			<td><div style="text-align: right;"><a runat="server" id="aPopUpEnterAddress">Enter an address now</a></div></td>
																																		</tr>												 
																																	</table>
																																</div>
																															</div>
																																																					 
																															<table cellspacing="0" cellpadding="2" align="left" border="0">
																																<tr>
																																	<td><input type="radio" runat="server" onclick="javascript:$('tblcc').style.display='block';$('tblpp').style.display='none';" id="radPmtCC" name="paymentType" value="cc" checked />Credit Card</td>
																																	<td style="padding-left:10px;"><img runat="server" src="~/images/credit_card_icons.gif" alt="Kaneva accepts American Express, Discover/NOVUS, MasterCard, and Visa."  height="20" border="0"></td>
																																</tr>
																																<tr id="trPaypal" runat="server" visible="false">
																																	<td><input type="radio" runat="server" onclick="javascript:$('tblcc').style.display='none';$('tblpp').style.display='block';$('imgMakePurchase').style.display='block';$('PopUpAddr').style.display='none';" id="radPmtPP" name="paymentType" value="pp" />Paypal</td>
																																	<td style="padding-left:10px;"><a href="#" onclick="javascript:window.open('https://www.paypal.com/us/cgi-bin/webscr?cmd=xpt/cps/popup/OLCWhatIsPayPal-outside','olcwhatispaypal','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=400, height=350');"><img  src="https://www.paypal.com/en_US/i/logo/PayPal_mark_37x23.gif" border="0" alt="Acceptance Mark"></a></td>
																																</tr>
																															</table>
																														</td>
																													</tr>
																													<tr><td align="center" valign="top"><hr style="color:#000000;height:1px;width:98%;" /></td></tr>
																													<tr>
																														<td>
																														    <div id="divPassessLegal2" visible="false" runat="server" style="border: solid 1 #000000; line-height:16px; margin-bottom:10px; padding:4px">
																														        We need a credit card to set up your account. Your card will not be charged until your trial period ends.
																														    </div>
																														    <div id="divPassessLegal2NoTrial" visible="false" runat="server" style="border: solid 1 #000000; line-height:16px; margin-bottom:10px; padding:4px">
																														        We need a credit card to set up your account.
																														    </div>
																															<table id="tblcc" cellpadding="4" cellspacing="0" border="0" runat="server">
																																<tr>
																																	<td>Name as it appears on card&nbsp;<asp:requiredfieldvalidator id="rftxtNameOnCard" controltovalidate="txtNameOnCard" text="*" errormessage="Name as it appears on card is a required field" display="Dynamic" runat="server"/><br>
																																	<asp:textbox id="txtNameOnCard" class="biginput" style="width:300px" maxlength="100" runat="server" tabindex="1" /></td>
																																</tr>
																																<tr>
																																	<td>Card Type<br>
																																		<table border="0" cellspacing="0" cellpadding="0" width="100%">						
																																			<tr>
																																				<td width="160">
																																					<kaneva:cardtypeslistbox class="biginput" id="drpCardType" runat="server" style="width:150px" rows="1" tabindex="2">
																																						<asp:listitem value="CardTypes Here">CardTypes Here</asp:listitem>
																																					</kaneva:cardtypeslistbox>
																																				</td>
																																			</tr>
																																		</table>
																																	</td>																	
																																</tr>
																																<tr>
																																	<td>Card Number&nbsp;<kaneva:creditcardvalidator id="ccvNumber" controltovalidate="txtCardNumber" errormessage="Please enter a valid credit card number for the card type selected" display="Dynamic" text="*" runat="server" cardtypeslistbox="drpCardType" /><asp:requiredfieldvalidator id="rfvCardNumber" controltovalidate="txtCardNumber" text="*" errormessage="Card Number is a required field" display="Dynamic" runat="server"/><br>
																																	<asp:textbox id="txtCardNumber" class="biginput" style="width:255px" maxlength="16" runat="server" enableviewstate="false" tabindex="3" /><br/><span class="note">Please do not enter spaces.<br/>For security purposes Kaneva does NOT store credit card numbers.</span></td>
																																</tr>
																																<tr>																		 
																																	<td>Security Code&nbsp;<asp:requiredfieldvalidator id="rftxtSecurityCode" controltovalidate="txtSecurityCode" text="*" errormessage="Security Code is a required field" display="Dynamic" runat="server"/><br>
																																		<asp:textbox id="txtSecurityCode" class="biginput" style="width:100px" maxlength="4" runat="server" tabindex="4"/> 
																																		&nbsp;&nbsp;<a onmouseover="this.style.cursor='pointer'" onfocus="this.blur();" onclick="document.getElementById('PopUp').style.display = 'block';  return false;" href="#">What is this?</a>
																																		<div id="PopUp" style="display: none; position: absolute; left: 190px; bottom: 110px; margin: 0; padding: 15px 15px 5px 15px; background: transparent url(../images/popup_point.gif) no-repeat bottom left; text-align: justify; font-size: 12px; width: auto;">
																																			<div style="display:block; border: 3px solid #f7ce52; padding: 10px 10px 0 10px; background-color: #FFFFFF;">
																																				<table border="0" cellspacing="0" cellpadding="3">
																																					<tr>
																																						<td><img src="~/images/mini_amex.gif" runat="server" alt="American Express displays the 4 digit number on the front right of the card." width="118" height="73" border="0"></td>
																																						<td><img src="~/images/mini_discover.gif" runat="server" alt="Discover displays the 3 digit number on the back of the card." width="118" height="73" border="0"></td>
																																					</tr>
																																					<tr>
																																						<td><img src="~/images/mini_mc.gif" runat="server" alt="MasterCard displays the 3 digit number on the back of the card." width="118" height="73" border="0"></td>
																																						<td><img src="~/images/mini_visa.gif" runat="server" alt="Visa displays the 3 digit number on the back of the card." width="118" height="73" border="0"></td>
																																					</tr>
																																					<tr>
																																						<td colspan="2">Please enter your 3 or 4 digit Card ID Number</td>
																																					</tr>
																																					
																																					<tr>
																																						<td colspan="2"><div style="text-align: right;"><a href="#" onmouseover='' onfocus='this.blur();' onclick="document.getElementById('PopUp').style.display = 'none';  return false; " >Close</a></div></td>
																																					</tr>												 
																																				</table>
																																			</div>
																																		</div>
																																	
																																	</td>
																																</tr>
																																<tr>
																																	<td>Expiration Date<br>
																																		<asp:dropdownlist class="biginput" id="drpMonth" runat="Server" style="width:150px" tabindex="5">
																																			<asp:listitem value="01">(01) January</asp:listitem>
																																			<asp:listitem value="02">(02) February</asp:listitem>
																																			<asp:listitem value="03">(03) March</asp:listitem>
																																			<asp:listitem value="04">(04) April</asp:listitem>
																																			<asp:listitem value="05">(05) May</asp:listitem>
																																			<asp:listitem value="06">(06) June</asp:listitem>
																																			<asp:listitem value="07">(07) July</asp:listitem>
																																			<asp:listitem value="08">(08) August</asp:listitem>
																																			<asp:listitem value="09">(09) September</asp:listitem>
																																			<asp:listitem value="10">(10) October</asp:listitem>
																																			<asp:listitem value="11">(11) November</asp:listitem>
																																			<asp:listitem value="12">(12) December</asp:listitem>
																																		</asp:dropdownlist>
																																		&nbsp;&nbsp;
																																		<asp:dropdownlist class="biginput" runat="server" id="drpYear" style="width:100px" tabindex="6"></asp:dropdownlist>
																																	</td>
																																</tr>
																																<tr>
																																	<td class="note">
																																	    <div style="border: solid 1 #000000; background-color:#eeffee; line-height:16px; margin-bottom:10px; padding:4px">
																																	        <div id="divPassessLegal" visible="false" runat="server">
																														                        You will not be charged during the trial period. <br />
																														                        If you do not cancel within <ASP:label id="lblFreePeriod" style="color:#444444" runat="server" /> days of your sign-up date, you will be charged US $ <span style="color:444444"><ASP:label id="lblSUBPrice" runat="server" /> <ASP:label id="lblBillingPeriod" runat="server" /></span>. <br />
																														                    </div>																								 
																																	        <div id="divPassessLegalNoTrial" visible="false" runat="server">
																														                        You will be charged charged US $ <span style="color:444444"><ASP:label id="lblSUBPriceNT" runat="server" /> <ASP:label id="lblBillingPeriodNT" runat="server" /></span>. <br />
																														                    </div>
																														                    <div id="divAllowance" visible="false" runat="server" >
																														                        All credits issued under this pass may take up to 6 days from purchase to process.
																														                    </div>
																														                    <div id="divFreeRewards" visible="false" runat="server" >
																														                        Sign-up bonus rewards will post to your account upon order completion.
																														                    </div>
																														                    <div id="divFreeCredits" visible="false" runat="server" >
																														                        Sign-up bonus credits will post to your account upon order completion.
																														                    </div>
																														                    By ordering, you agree to the Terms of Service.
																														                </div>																								 
                                                                                                                                        <div id="divLegal" runat="server" visible="false" >
																																	        Clicking the Make Purchase button will submit your order for processing, do not click the back button while order is in process. <strong>Note: All Sales Final.</strong>
                                                                                                                                        </div>
																																</tr>
																															</table>
																															<table id="tblpp" cellpadding="0" cellspacing="0" border="0" align="center" runat="server">
																																<tr>
																																	<td align="center">
																																		<!-- PayPal Logo -->
																																		<table border="0" cellpadding="10" cellspacing="0" align="center">
																																			<tr><td align="center"></td></tr>
																																			<tr><td align="center"><a href="#" onclick="javascript:window.open('https://www.paypal.com/us/cgi-bin/webscr?cmd=xpt/cps/popup/OLCWhatIsPayPal-outside','olcwhatispaypal','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=400, height=350');"><img  src="https://www.paypal.com/en_US/i/bnr/bnr_nowAccepting_150x60.gif" border="0" alt="Additional Options"></a></td></tr>
																																		</table>
																																		<!-- PayPal Logo -->
																																		<img id="Img2" runat="server" src="~/images/spacer.gif" width="310" height="1" />
																																	</td>
																																</tr>
																																<tr><td class="note">You will be asked to log into your Paypal account to complete your purchase.</td></tr>
																															</table>
																														</td>
																													</tr>
																													<tr>														 
																														<td align="right">
																															<div><img src="~/images/wizard_btn_makepurchase.gif" runat="server" id="imgMakePurchase" style="cursor:hand"></div>
																														
																															<asp:button id="btnMakePurchase" runat="Server" style="display:none;width:1px;height:1px;" causesvalidation="False" onclick="btnMakePurchase_Click" class="Filter2" tabindex="7"/>
																														</td>
																													</tr>														  
																													
																												</table>
																												
																												<script type="text/javascript">
																												function makePurchase ()
																												{
																													if ($('radPmtCC').checked)
																													{
																														if (typeof(Page_ClientValidate) == 'function') 
																															if (!Page_ClientValidate()){return false;};
																														
																														this.disabled = true;
																														ShowLoading(true,'Please wait while we process your order...<br/><img src=\"../images/progress_bar.gif\">');
																														<asp:literal id="litPostBackRef" runat="server" />;
																													}
																												}
																												</script>
																												
																											</td>
																										</tr>
																									</table>
																									<span class="cb"><span class="cl"></span></span>
																								</div>
																							</td>
																							
																						</tr>
																					</table>
																					
																				</td>
																			</tr>
																		</table>
																		
																		<span class="cb"><span class="cl"></span></span>
																		</div>
																	</td>
																</tr>
															</table>
															
														</td>
														
														<!-- END WIZARD CONTENT -->
													</tr>
												</table>
											
											</td>
											
										</tr>
										
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td class=""><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
				</tr>
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
				
			</table>
		</td>
	</tr>
</table>

</div>

<script language="javascript">
function IsIE ()
{
	return ((navigator.userAgent.indexOf('MSIE') != -1) && (navigator.userAgent.indexOf('Win') != -1));
}

if (IsIE)
{
	$('PopUpAddr').style.left = '24 px';
	$('PopUpAddr').style.bottom = '410 px';
}
else
{
	$('PopUpAddr').style.left = '84 px';
	$('PopUpAddr').style.bottom = '470 px';
}
</script>
