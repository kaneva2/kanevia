///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for redeemComplete.
	/// </summary>
	public class redeemComplete : NoBorderPage
	{
		private void Page_Load(object sender, System.EventArgs e)
		{
            if (!Request.IsAuthenticated)
            {
                Response.Redirect("~/redeemLogin.aspx");
            }

            try
            {
                // Get user info
                spnUserName.InnerText = KanevaWebGlobals.CurrentUser.Username;

                spnCreditTotal.InnerText = Convert.ToUInt64(StoreUtility.GetLastRedeemedGiftCardCreditAmount(GetUserId()).ToString()).ToString("N0");
            }
            catch { }

            aGoInWorld.HRef = KanevaSetupInstallURL();
        }

		#region Event Handlers
		protected void btnGetStarted_Click (Object sender, CommandEventArgs e)
		{
			
		}
		#endregion

		#region Declerations

		protected Button btnGetStarted;

        protected HtmlContainerControl spnUserName, spnCreditTotal;

        protected HtmlAnchor aGoInWorld;

		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
