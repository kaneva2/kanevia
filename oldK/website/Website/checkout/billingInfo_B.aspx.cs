///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;
using System.Security.Principal;
using System.Security.Cryptography;
using log4net;


namespace KlausEnt.KEP.Kaneva
{
    public partial class billingInfo_B : MainTemplatePage
    {
        protected void Page_Load (object sender, EventArgs e)
        {
            if (!KanevaGlobals.EnableCheckout_B)
            {
                if (!IsAdministrator ())
                {
                    Response.Redirect("~/error.aspx?error=" + (int)Constants.eERROR_TYPE.CHECKOUT_CLOSED);
                }
            }

            Response.Expires = 0;
            Response.CacheControl = "no-cache";

            if (!IsPostBack)
            {
                // Must be logged in to see this page
                VerifyUserAccess ();

                bool bIsCheckout = false;
                DataRow drOrder = null;
                int orderId = 0;

                // This page is dual purpose.  User can use it update their billing info at any time, or this
                // page can be part of the checkout flow for an order.  If it is part of the order flow,
                // then we need to verify there is an order and the user has access to the order
                VerifyOrder (ref orderId, ref drOrder);

                if (orderId == 0 || drOrder == null)
                {
                    // could not get order record.  not is checkout flow
                    bIsCheckout = false;
                }
                else
                {
                    bIsCheckout = true;
                }
                

                // Create a new (empty) billing info object
                BillingInfo billInfo = new BillingInfo ();

                string billSubId = UsersUtility.GetUserBillingSubscriptionId (KanevaWebGlobals.CurrentUser.UserId);

                // Check users account to see if user has saved credit card and address info
                if (billSubId.Length > 0)
                {
                    // Get the subscription info from cybersource
                    CybersourceAdaptor cybersourceAdaptor = new CybersourceAdaptor ();

                    string userErrorMessage = "";
                    bool bSystemDown = false;
                    if (!cybersourceAdaptor.GetBillingSubscription (KanevaWebGlobals.CurrentUser.UserId,
                        billSubId, ref bSystemDown, ref userErrorMessage, ref billInfo))
                    {
                        if (bSystemDown)
                        {
                            m_logger.Warn ("Could not update Cybersource Billing Profile for User " + KanevaWebGlobals.CurrentUser.UserId + " because system is unavailable");
                            ShowErrorMessage ("Information not saved.  System is currently unavailable.  Please try again later.", false);
                            return;
                        }
                        else  // error
                        {
                            m_logger.Warn ("Error updating User " + KanevaWebGlobals.CurrentUser.UserId + " Cybersource Billing Profile. Err: " + userErrorMessage);
                            ShowErrorMessage (userErrorMessage);
                            return;
                        }
                    }

                    // If we are returning from order confirm page, then we need to show page bacause user has selected to see
                    // billing info or we are returning because of an error thrown while trying to process the order that
                    // user needs to correct on this page.

                    // If we are coming from the buycredits page, then keep processing and move along to order confirm page
                    if (Request.UrlReferrer != null &&
                        Request.UrlReferrer.ToString ().ToLower ().IndexOf ("/mykaneva/buycredits_b.aspx") > 0)
                    {
                        // check to see if this is part of a checkout process, and if the subscription info from
                        // cybersource actually has data in it (user could have previously cleared this data)
                        if (bIsCheckout && billInfo.CardNumber.Length > 0 && billInfo.FullName.Length > 0)
                        {
                            // process the order for cc purchase and move on to order confirm page so user does not see this page

                            // Get the id of last address used by user.  should match what is in cybersource subscription
                            int addressId = UsersUtility.GetLastAddressId (KanevaWebGlobals.CurrentUser.UserId);

                            int pointTransactionId = Convert.ToInt32 (drOrder["point_transaction_id"]);

                            // Save the address to the order
                            StoreUtility.UpdatePointTransactionAddressId (pointTransactionId, addressId);

                            // Save the payment type
                            StoreUtility.UpdatePointTransaction (pointTransactionId, Constants.ePAYMENT_METHODS.KANEVA_CYBERSOURCE);

                            // Add the credit card info to the order
                            AddPaymentDetails (billInfo, drOrder);

                            Response.Redirect ("~/checkout/orderConfirm_B.aspx?orderId=" + orderId.ToString () + "&pass=" + (Request.QueryString["pass"] != null ? Request.QueryString["pass"].ToString () : ""), false);
                        }
                    }
                }

                // Countries
                GetCountries ();

                // States
                GetStates ();

                // Show fields and data
                PopulateFields (billInfo);

                if (Request["err"] != null && Request["err"].ToString ().Length > 0)
                {
                    ShowErrorMessage (Server.UrlDecode (Server.HtmlDecode (Request["err"].ToString ())));
                }

                // If we are coming here to just edit the billing info, then
                if (!bIsCheckout)
                {
                    ShowProgressBar (false);
                    ShowPaypalLinks (false);

                    // Enable appropriate save button
                    lbSaveEdit.Visible = true;
                    lbSave.Visible = false;

                    // if they don't already have cc info stored, show the edit table 
                    if (billSubId.Length == 0)
                    {
                        ShowCreditCardEditFields (true);
                        ShowClearInfoButton (false);
                    }
                    else
                    {
                        ShowClearInfoButton (true);
                    }

                    // Set Nav
                    HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
                    HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.ACCOUNT;
                    HeaderNav.MyAccountNav.ActiveTab = NavAccount.TAB.CREDITS;
                    HeaderNav.SetNavVisible (HeaderNav.MyKanevaNav, 2);
                    HeaderNav.SetNavVisible (HeaderNav.MyAccountNav);
                }
                else
                {
                    ShowClearInfoButton (false);
                    ShowProgressBar (true);

                    // Enable appropriate save button
                    lbSaveEdit.Visible = false;
                    lbSave.Visible = true;

                    // if there is not a credit card number, then show the card edit table
                    if (billInfo.CardNumber.Length == 0 || Request["showcc"] != null)
                    {
                        ShowCreditCardEditFields (true);
                    }
                    else
                    {
                        ShowCreditCardEditFields (false);
                    }

                    // show the paypal links?
                    if (KanevaGlobals.EnablePaypal || IsAdministrator ())
                    {
                        ShowPaypalLinks (true);
                    }

                    // Set Nav
                    HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
                }
            }
            else
            {
                // if isPostBack, do we need to display the edit credit card table
                // or just display the view only credit card information table
                try
                {
                    if (Convert.ToBoolean(txtEditCard.Value) == true)
                    {
                        ShowCreditCardEditFields (true);
                    }
                }
                catch {}
            }
        }


        #region Helper Methods

        /// <summary>
        /// Populate the fields on the page with current data
        /// </summary>
        private void PopulateFields (BillingInfo billInfo)
        {
            string country = "";

            // Populate the edit fields
            txtFullName.Text = Server.HtmlDecode (billInfo.FullName);
            txtAddress1.Text = Server.HtmlDecode (billInfo.Address1);
            txtAddress2.Text = Server.HtmlDecode (billInfo.Address2);
            txtCity.Text = Server.HtmlDecode (billInfo.City);
            txtPostalCode.Text = Server.HtmlDecode (billInfo.PostalCode);
            txtPhoneNumber.Text = Server.HtmlDecode (billInfo.PhoneNumber);
            //txtSecurityCode.Text = "";
            
            if (billInfo.State.Length > 0) SetDropDownIndex (drpState, Server.HtmlDecode (billInfo.State));
            if (billInfo.Country.Length > 0)
            {
                SetDropDownIndex (drpCountry, Server.HtmlDecode (billInfo.Country));
                country = billInfo.Country;
            }
            else
            {
                SetDropDownIndex (drpCountry, KanevaWebGlobals.CurrentUser.Country);
                country = KanevaWebGlobals.CurrentUser.Country;
            }
            if (billInfo.CardExpirationMonth.Length > 0) SetDropDownIndex (drpMonth, Server.HtmlDecode (billInfo.CardExpirationMonth));
            if (billInfo.CardExpirationYear.Length > 0) SetDropDownIndex (drpYear, Server.HtmlDecode (billInfo.CardExpirationYear));

            // we don't have the full credit card number, only a partial is returned from cybersource.  If we entered
            // that number into the field the user would have to update that field anyway, so we just blank out the
            // credit card number field so it is obvious to the user they must re-enter the number
            txtCardNumber.Text = string.Empty;

            if (billInfo.CardType.Length > 0)
            {
                try
                {
                    drpCardType.SelectItemByName = Server.HtmlDecode (billInfo.CardType);
                }
                catch { }
            }

            // Populate the read-only credit card fields
            spnCardType.InnerHtml = Server.HtmlDecode (billInfo.CardType);
            spnCardNumber.InnerHtml = "..." + Server.HtmlDecode (billInfo.CardNumberLast4);
            spnCardExpiration.InnerHtml = Server.HtmlDecode (billInfo.CardExpirationMonth) + "/" + Server.HtmlDecode (billInfo.CardExpirationYear);
        
            // show hide appropriate state field based on country
            if (country == "US" || country == "CA" || country == "")
            {
                drpState.Style.Add ("display", "block");
                rfdrpState.Enabled = true;

                txtState.Style.Add ("display", "none");
            }
            else
            {
                drpState.Style.Add ("display", "none");
                rfdrpState.Enabled = false;

                txtState.Style.Add ("display", "block");
            }
        }

        /// <summary>
        /// Adds a new subscription to cybersource
        /// </summary>
        private bool AddCybersourceSubscription (BillingInfo billingInfo, ref bool bSystemDown, ref string userErrorMessage)
        {
            try
            {
                string billingSubscriptionId = "";

                CybersourceAdaptor cybersourceAdaptor = new CybersourceAdaptor ();
                if (!cybersourceAdaptor.CreateBillingSubscription (KanevaWebGlobals.CurrentUser.UserId, 0, billingInfo,
                    Common.GetVisitorIPAddress(), ref bSystemDown, ref userErrorMessage, ref billingSubscriptionId))
                {
                    return false;
                }

                if (billingSubscriptionId.Length > 0)
                {
                    // update billingProfileId in users record
                    UsersUtility.AddBillingSubscriptionId (KanevaWebGlobals.CurrentUser.UserId, billingSubscriptionId);
                    
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Updates an existing subscription to cybersource
        /// </summary>
        private bool UpdateCybersourceSubscription (BillingInfo billingInfo, bool includeCardInfo, ref bool bSystemDown, ref string userErrorMessage)
        {
            try
            {
                if (!includeCardInfo)
                {
                    billingInfo.CardType = string.Empty;
                }
                
                CybersourceAdaptor cybersourceAdaptor = new CybersourceAdaptor ();
                if (!cybersourceAdaptor.UpdateBillingProfile (KanevaWebGlobals.CurrentUser.UserId, 0, billingInfo, 
                    Common.GetVisitorIPAddress(), ref bSystemDown, ref userErrorMessage))
                {
                    return false;
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Get info from form fields and popluate BillingInfo object
        /// </summary>
        private BillingInfo GetBillingInfo (string billingSubscriptionId)
        {
            string ccNumEncrypt = KanevaGlobals.Encrypt (txtCardNumber.Text);
            //string ccSecCodeEncrypt = KanevaGlobals.Encrypt (txtSecurityCode.Text);

            // create billing info object
            BillingInfo billingInfo = new BillingInfo ();
            billingInfo.FullName = txtFullName.Text.Trim ();
            billingInfo.Address1 = txtAddress1.Text.Trim ();
            billingInfo.Address2 = txtAddress2.Text.Trim ();
            billingInfo.City = txtCity.Text.Trim ();

            if (drpCountry.SelectedValue == "US" || drpCountry.SelectedValue == "CA")
            {
                billingInfo.State = drpState.SelectedValue;
            }
            else
            {
                billingInfo.State = txtState.Text.Trim();
            }
            billingInfo.PostalCode = txtPostalCode.Text.Trim ();
            billingInfo.Country = drpCountry.SelectedValue;
            billingInfo.PhoneNumber = txtPhoneNumber.Text.Trim ();
            billingInfo.CardType = drpCardType.SelectedItem.Text;
            billingInfo.CardNumberEncrypt = ccNumEncrypt;
            //billingInfo.CardSecurityCodeEncrypt = ccSecCodeEncrypt;
            billingInfo.CardExpirationMonth = drpMonth.SelectedValue;
            billingInfo.CardExpirationYear = drpYear.SelectedValue;
            billingInfo.SubscriptionId = billingSubscriptionId;

            return billingInfo;
        }

        /// <summary>
        /// Populate the BillingInfo object with "void" data used when we clear users info on cybersource
        /// </summary>
        private BillingInfo GetClearProfileBillingInfo (string billingSubscriptionId)
        {
            string ccNumEncrypt = KanevaGlobals.Encrypt ("4111111111111111");
            //string ccSecCodeEncrypt = KanevaGlobals.Encrypt ("1111");

            // create billing info object
            BillingInfo billingInfo = new BillingInfo ();
            billingInfo.FullName = "Kaneva Cancel";
            billingInfo.Address1 = "Void";
            billingInfo.Address2 = "";
            billingInfo.City = "Void";
            billingInfo.State = "GA";
            billingInfo.PostalCode = "00000";
            billingInfo.Country = "US";
            billingInfo.PhoneNumber = "0000000000";
            billingInfo.CardType = "Visa";
            billingInfo.CardNumberEncrypt = ccNumEncrypt;
            //billingInfo.CardSecurityCodeEncrypt = ccSecCodeEncrypt;
            billingInfo.CardExpirationMonth = DateTime.Now.Month.ToString ();
            billingInfo.CardExpirationYear = DateTime.Now.Year.ToString ();
            billingInfo.SubscriptionId = billingSubscriptionId;

            return billingInfo;
        }

        /// <summary>
        /// Get the list of countries
        /// </summary>
        private void GetCountries ()
        {				
            drpCountry.DataTextField = "Name";
            drpCountry.DataValueField = "CountryId";
            drpCountry.DataSource = WebCache.GetCountries ();
            drpCountry.DataBind ();

            drpCountry.Items.Insert (0, new ListItem ("United States", Constants.COUNTRY_CODE_UNITED_STATES));
            drpCountry.Items.Insert (0, new ListItem ("select...", ""));
        }

        /// <summary>
        /// Get the list of states
        /// </summary>
        private void GetStates ()
        {
            drpState.DataTextField = "StateCode";
            drpState.DataValueField = "StateCode";
            drpState.DataSource = WebCache.GetStates ();
            drpState.DataBind ();
            drpState.Items.Insert (0, new ListItem ("select...", ""));
        }

        /// <summary>
        /// Verify users access to page
        /// </summary>
        private void VerifyUserAccess ()
        {
            // They must be logged in
            if (!Request.IsAuthenticated)
            {
                Response.Redirect (GetLoginURL ());
            }
        }

        /// <summary>
        /// Verify user is owner of to the order
        /// </summary>
        private void VerifyOrder (ref int orderId, ref DataRow drOrder)
        {
            if (Request["orderId"] == null)
            {
                orderId = 0;
            }

            try
            {
                orderId = Convert.ToInt32 (Request["orderId"]);
                drOrder = StoreUtility.GetOrder (orderId, KanevaWebGlobals.CurrentUser.UserId);
                if (!VerifyOrder (drOrder))
                {
                    orderId = 0;
                }
            }
            catch
            {
                orderId = 0;
            }
        }
        private void VerifyOrder ()
        {
            if (Request["orderId"] == null)
            {
                Response.Redirect ("~/mykaneva/managecredits.aspx");
            }

            try
            {
                int orderId = Convert.ToInt32 (Request["orderId"]);
                if (!VerifyOrder (StoreUtility.GetOrder (orderId, KanevaWebGlobals.CurrentUser.UserId)))
                {
                    Response.Redirect ("~/mykaneva/managecredits.aspx");
                }
            }
            catch
            {
                Response.Redirect ("~/mykaneva/managecredits.aspx");
            }
        }
        private bool VerifyOrder (DataRow drOrder)
        {
            try
            {
                // Verify user is owner of this order		  
                if (drOrder == null)
                {
                    // User may be trying to view someone else's order
                    m_logger.Warn ("User " + KanevaWebGlobals.CurrentUser.UserId + " tried to view orderId " + Request["orderId"].ToString () + " from IP " + Common.GetVisitorIPAddress());
                    return false;
                }

                if (Convert.ToInt32 (drOrder["transaction_status_id"]) != (int) Constants.eORDER_STATUS.CHECKOUT)
                {
                    m_logger.Warn ("User " + KanevaWebGlobals.CurrentUser.UserId + " tried to view orderId " + Request["orderId"].ToString () + " from IP " + Common.GetVisitorIPAddress() + ".  Order does not have a status of CART.");
                    return false;
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// AddPaymentDetails
        /// </summary>
        private void AddPaymentDetails (string ccNumLastFourDigits)
        {
            int orderId = Convert.ToInt32 (Request["orderId"]);
            DataRow drOrder = StoreUtility.GetOrder (orderId);

            int pointTransactionId = Convert.ToInt32 (drOrder["point_transaction_id"]);

            // Add the new credit card
            int billingInfoId = UsersUtility.InsertBillingInfo (KanevaWebGlobals.CurrentUser.UserId,
                Constants.eBILLING_INFO_TYPE.PROFILE, "",
                Server.HtmlEncode (txtFullName.Text),
                KanevaGlobals.Encrypt (Server.HtmlEncode (ccNumLastFourDigits)),
                Server.HtmlEncode (drpCardType.SelectedItem.Text),
                Server.HtmlEncode (drpMonth.SelectedValue), "",
                Server.HtmlEncode (drpYear.SelectedValue), 0);

            // Update purchase transaction
            StoreUtility.UpdatePointTransactionBillingInfoId (pointTransactionId, billingInfoId);
        }
        private void AddPaymentDetails (BillingInfo billInfo, DataRow drOrder)
        {
            int pointTransactionId = Convert.ToInt32 (drOrder["point_transaction_id"]);

            // Add the new credit card
            int billingInfoId = UsersUtility.InsertBillingInfo (KanevaWebGlobals.CurrentUser.UserId,
                Constants.eBILLING_INFO_TYPE.PROFILE, "",
                Server.HtmlEncode (billInfo.FullName),
                KanevaGlobals.Encrypt (billInfo.CardNumberLast4),
                Server.HtmlEncode (billInfo.CardType),
                Server.HtmlEncode (billInfo.CardExpirationMonth), "",
                Server.HtmlEncode (billInfo.CardExpirationYear), 0);

            // Update purchase transaction
            StoreUtility.UpdatePointTransactionBillingInfoId (pointTransactionId, billingInfoId);
        }

        /// <summary>
        /// Format and display the error message
        /// </summary>
        private void ShowErrorMessage (string err)
        {
            ShowErrorMessage (err, true);
        }
        private void ShowErrorMessage (string err, bool showAsList)
        {
            if (showAsList)
            {
                valSum.DisplayMode = ValidationSummaryDisplayMode.List;
                valSum.HeaderText = "Please correct the following error(s):<br/>";
                valSum.Style.Add ("text-align", "left");
            }
            else
            {
                valSum.DisplayMode = ValidationSummaryDisplayMode.SingleParagraph;
                valSum.HeaderText = "";
                valSum.Style.Add ("text-align", "center");
            }

            cvBlank.IsValid = false;
            cvBlank.ErrorMessage = err;
        }

        /// <summary>
        /// Show/hide the credit card edit fields
        /// </summary>
        private void ShowCreditCardEditFields (bool showEditFields)
        {
            if (showEditFields)
            {
                tblCardView.Style.Add ("display", "none");
                tblCardEdit.Style.Add ("display", "block");
                txtEditCard.Value = "true";
            }
            else
            {
                tblCardView.Style.Add ("display", "block");
                tblCardEdit.Style.Add ("display", "none");
                txtEditCard.Value = "false";
            }
        }

        /// <summary>
        /// Show/hide the paypal links
        /// </summary>
        private void ShowPaypalLinks (bool showPaypal)
        {
            if (showPaypal)
            {
                divPaypalTop.Style.Add ("display", "block");
                divPaypalBtm.Style.Add ("display", "block");
            }
            else
            {
                divPaypalTop.Style.Add ("display", "none");
                divPaypalBtm.Style.Add ("display", "none");
            }
        }

        /// <summary>
        /// Show/hide the clear info button
        /// </summary>
        private void ShowClearInfoButton (bool showBtn)
        {
            if (showBtn)
            {
                divClearInfo.Style.Add ("display", "block");
            }
            else
            {
                divClearInfo.Style.Add ("display", "none");
            }
        }

        /// <summary>
        /// Show/hide the purchase progress bar
        /// </summary>
        private void ShowProgressBar (bool showBar)
        {
            if (showBar)
            {
                divProgressBar.Style.Add ("display", "block");
            }
            else
            {
                divProgressBar.Style.Add ("display", "none");
            }
        }
        
        #endregion Helper Methods


        #region Event Handlers

        /// <summary>
        /// Click event for the paypal link buttons
        /// </summary>
        protected void lbPaypal_Click (object sender, EventArgs e)
        {
            cvBlank.IsValid = true;
            cvBlank.ErrorMessage = string.Empty;
            
            VerifyUserAccess ();

            if (Request["orderId"] != null)
            {
                int orderId = 0;

                try
                {
                    // Get the order
                    orderId = Convert.ToInt32 (Request["orderId"]);
                    DataRow drOrder = StoreUtility.GetOrder (orderId);
                    int pointTransactionId = Convert.ToInt32 (drOrder["point_transaction_id"]);

                    StoreUtility.UpdatePointTransaction (pointTransactionId, Constants.ePAYMENT_METHODS.PAYPAL);

                    Response.Redirect ("~/checkout/orderConfirm_B.aspx?orderId=" + orderId.ToString () + "&pass=" + (Request.QueryString["pass"] != null ? Request.QueryString["pass"].ToString () : ""), false);
                }
                catch (Exception exc)
                {
                    m_logger.Error ("Error updating payment type: UserId=" + KanevaWebGlobals.CurrentUser.UserId.ToString () + " : OrderId=" + orderId.ToString () + " : ", exc);

                    cvBlank.IsValid = false;
                    cvBlank.ErrorMessage = "An error occurred while trying to process your order.";
                    return;
                }
            }
            else
            {
                Response.Redirect ("~/mykaneva/managecredits.aspx");
            }
        }

        /// <summary>
        /// Click event for the Save Edit linkbutton
        /// </summary>
        protected void lbSaveEdit_Click (object sender, EventArgs e)
        {
            try
            {
                bool includeCardInfo = false;

                // State is only required for US and Canada
                if (drpCountry.SelectedValue.Equals (Constants.COUNTRY_CODE_UNITED_STATES) || 
                    drpCountry.SelectedValue.Equals (Constants.COUNTRY_CODE_CANADA))
                {
                    rfdrpState.Enabled = true;
                    rftxtPostalCode.Enabled = true;
                    rftxtState.Enabled = false;

                    // Make sure correct control is visible
                    drpState.Style.Add ("display", "block");
                    txtState.Style.Add ("display", "none");
                }
                else
                {
                    rfdrpState.Enabled = false;
                    rftxtPostalCode.Enabled = true;
                    rftxtState.Enabled = false;

                    // Make sure correct control is visible
                    drpState.Style.Add ("display", "none");
                    txtState.Style.Add ("display", "block");
                }

                // check if user is updating credit card information
                try
                {   // if user is not trying to edit card info, disable card validators
                    includeCardInfo = Convert.ToBoolean (txtEditCard.Value);

                    if (!includeCardInfo)
                    {
                        rfvCardNumber.Enabled = false;
                        ccvNumber.Enabled = false;
                        //rftxtSecurityCode.Enabled = false;
                    }
                    else
                    {
                        rfvCardNumber.Enabled = true;
                        ccvNumber.Enabled = true;
                        //rftxtSecurityCode.Enabled = true;
                    }
                }
                catch { }

                Page.Validate ();

                if (!Page.IsValid)
                {
                    valSum.ShowSummary = false;
                    return;
                }

                // check to make sure no one is trying any injections through input fields
                if (KanevaWebGlobals.ContainsInjectScripts (txtFullName.Text) || KanevaWebGlobals.ContainsInjectScripts (txtAddress1.Text) ||
                    KanevaWebGlobals.ContainsInjectScripts (txtAddress2.Text) || KanevaWebGlobals.ContainsInjectScripts (txtCity.Text) ||
                    KanevaWebGlobals.ContainsInjectScripts (txtPostalCode.Text) || KanevaWebGlobals.ContainsInjectScripts (txtPhoneNumber.Text) ||
                    KanevaWebGlobals.ContainsInjectScripts (txtState.Text))
                {
                    m_logger.Warn ("User " + KanevaWebGlobals.CurrentUser.UserId + " tried to script from IP " + Common.GetVisitorIPAddress());
                    ShowErrorMessage ("Your input contains invalid scripting, please remove script code and try again.");
                    return;
                }

                // If user already has cybersource subscription, then update existing info
                int addressId = 0;
                bool bSystemDown = false;
                string userErrorMessage = "";

                string billSubId = UsersUtility.GetUserBillingSubscriptionId (KanevaWebGlobals.CurrentUser.UserId);

                BillingInfo billInfo = GetBillingInfo (billSubId);

                // If they have an existing subscription, then update that subscription
                if (billSubId.Length > 0)
                {
                    if (!UpdateCybersourceSubscription (billInfo, includeCardInfo, ref bSystemDown, ref userErrorMessage))
                    {
                        if (bSystemDown)
                        {
                            m_logger.Warn ("Could not update Cybersource Billing Profile for User " + KanevaWebGlobals.CurrentUser.UserId + " because system is unavailable");
                            ShowErrorMessage ("Information not saved.  System is currently unavailable.  Please try again later.", false);
                            return;
                        }
                        else  // error
                        {
                            m_logger.Warn ("Error updating User " + KanevaWebGlobals.CurrentUser.UserId + " Cybersource Billing Profile. Err: " + userErrorMessage);
                            ShowErrorMessage (userErrorMessage);
                            return;
                        }
                    }

                    addressId = UsersUtility.GetLastAddressId (KanevaWebGlobals.CurrentUser.UserId);
                    UsersUtility.UpdateAddress (KanevaWebGlobals.CurrentUser.UserId, addressId, Server.HtmlEncode (billInfo.FullName),
                        Server.HtmlEncode (billInfo.Address1), Server.HtmlEncode (billInfo.Address2), Server.HtmlEncode (billInfo.City),
                        Server.HtmlEncode (billInfo.State), Server.HtmlEncode (billInfo.PostalCode), Server.HtmlEncode (billInfo.PhoneNumber),
                        Server.HtmlEncode (billInfo.Country));
                }
                else // else create a new cybersource subscription for user
                {
                    if (!AddCybersourceSubscription (billInfo, ref bSystemDown, ref userErrorMessage))
                    {
                        if (bSystemDown)
                        {
                            m_logger.Warn ("Could not create Cybersource Billing Profile for User " + KanevaWebGlobals.CurrentUser.UserId + " because system is unavailable");
                            ShowErrorMessage ("Information not saved.  System is currently unavailable.  Please try again later.", false);
                            return;
                        }
                        else  // error
                        {
                            m_logger.Warn ("Error creating Cybersource Billing Profile for User " + KanevaWebGlobals.CurrentUser.UserId + ". Err: " + userErrorMessage);
                            ShowErrorMessage (userErrorMessage);
                            return;
                        }
                    }

                    // Insert the new billing address
                    addressId = UsersUtility.InsertAddress (KanevaWebGlobals.CurrentUser.UserId, Server.HtmlEncode (billInfo.FullName), 
                        Server.HtmlEncode (billInfo.Address1), Server.HtmlEncode (billInfo.Address2), Server.HtmlEncode (billInfo.City), 
                        Server.HtmlEncode (billInfo.State), Server.HtmlEncode (billInfo.PostalCode), Server.HtmlEncode (billInfo.PhoneNumber), 
                        Server.HtmlEncode (billInfo.Country));
                }

                Response.Redirect ("~/mykaneva/billingInfoSummary.aspx", false);
            }
            catch (Exception exc)
            {
                m_logger.Error ("Error saving billing information to users order: UserId=" + KanevaWebGlobals.CurrentUser.UserId.ToString () + " : ", exc);
                ShowErrorMessage ("An error occurred while trying to save your address.");
                return;
            }
        }

        /// <summary>
        /// Click event for the Save and Continue linkbutton
        /// </summary>
        protected void lbSaveContinue_Click (object sender, EventArgs e)
        {
            VerifyUserAccess ();

            if (Request["orderId"] != null)
            {
                try
                {
                    bool includeCardInfo = true;

                    // State is only required for US and Canada
                    if (drpCountry.SelectedValue.Equals (Constants.COUNTRY_CODE_UNITED_STATES) ||
                        drpCountry.SelectedValue.Equals (Constants.COUNTRY_CODE_CANADA))
                    {
                        rfdrpState.Enabled = true;
                        rftxtPostalCode.Enabled = true;
                        rftxtState.Enabled = false;

                        // Make sure correct control is visible
                        drpState.Style.Add ("display", "block");
                        txtState.Style.Add ("display", "none");
                    }
                    else
                    {
                        rfdrpState.Enabled = false;
                        rftxtPostalCode.Enabled = true;
                        rftxtState.Enabled = false;

                        // Make sure correct control is visible
                        drpState.Style.Add ("display", "none");
                        txtState.Style.Add ("display", "block");
                    }

                    // check if user is updating credit card information
                    try
                    {   // if user is not trying to edit card info, disable card validators
                        includeCardInfo = Convert.ToBoolean (txtEditCard.Value);

                        if (!includeCardInfo)
                        {
                            rfvCardNumber.Enabled = false;
                            ccvNumber.Enabled = false;
                            //rftxtSecurityCode.Enabled = false;
                        }
                        else
                        {
                            rfvCardNumber.Enabled = true;
                            ccvNumber.Enabled = true;
                            //rftxtSecurityCode.Enabled = true;
                        }
                    }
                    catch { }
                    
                    Page.Validate ();

                    if (!Page.IsValid)
                    {
                        valSum.ShowSummary = false;
                        return;
                    }

                    // Get the order
                    int orderId = Convert.ToInt32 (Request["orderId"]);
                    DataRow drOrder = StoreUtility.GetOrder (orderId, KanevaWebGlobals.CurrentUser.UserId);

                    VerifyOrder (drOrder);

                    // check to make sure no one is trying any injections through input fields
                    if (KanevaWebGlobals.ContainsInjectScripts (txtFullName.Text) || KanevaWebGlobals.ContainsInjectScripts (txtAddress1.Text) ||
                        KanevaWebGlobals.ContainsInjectScripts (txtAddress2.Text) || KanevaWebGlobals.ContainsInjectScripts (txtCity.Text) ||
                        KanevaWebGlobals.ContainsInjectScripts (txtPostalCode.Text) || KanevaWebGlobals.ContainsInjectScripts (txtPhoneNumber.Text) ||
                        KanevaWebGlobals.ContainsInjectScripts (txtState.Text))
                    {
                        m_logger.Warn ("User " + KanevaWebGlobals.CurrentUser.UserId + " tried to script from IP " + Common.GetVisitorIPAddress());
                        ShowErrorMessage ("Your input contains invalid scripting, please remove script code and try again.");
                        return;
                    }

                    // If user already has cybersource subscription, then update existing info
                    int addressId = 0;
                    bool bSystemDown = false;
                    string userErrorMessage = "";

                    // Get users billing subscription id
                    string billSubId = UsersUtility.GetUserBillingSubscriptionId (KanevaWebGlobals.CurrentUser.UserId);

                    BillingInfo billInfo = GetBillingInfo(billSubId);

                    if (billSubId.Length > 0)
                    {
                        if (!UpdateCybersourceSubscription (billInfo, includeCardInfo, ref bSystemDown, ref userErrorMessage))
                        {
                            if (bSystemDown)
                            {
                                m_logger.Warn ("Could not update Cybersource Billing Profile for User " + KanevaWebGlobals.CurrentUser.UserId.ToString () + " because system is unavailable");
                                ShowErrorMessage ("Information not saved.  System is currently unavailable.  Please try again later.", false);
                                return;
                            }
                            else  // error
                            {
                                m_logger.Warn ("Error updating User " + KanevaWebGlobals.CurrentUser.UserId.ToString () + " Cybersource Billing Profile. Err: " + userErrorMessage);
                                ShowErrorMessage (userErrorMessage);
                                return;
                            }
                        }
 
                        addressId = UsersUtility.GetLastAddressId (KanevaWebGlobals.CurrentUser.UserId);
                        UsersUtility.UpdateAddress (KanevaWebGlobals.CurrentUser.UserId, addressId, Server.HtmlEncode (billInfo.FullName),
                            Server.HtmlEncode (billInfo.Address1), Server.HtmlEncode (billInfo.Address2), Server.HtmlEncode (billInfo.City),
                            Server.HtmlEncode (billInfo.State), Server.HtmlEncode (billInfo.PostalCode), Server.HtmlEncode (billInfo.PhoneNumber),
                            Server.HtmlEncode (billInfo.Country));
                    }
                    else // else create a new cybersource subscription for user
                    {
                        if (!AddCybersourceSubscription (billInfo, ref bSystemDown, ref userErrorMessage))
                        {
                            if (bSystemDown)
                            {
                                m_logger.Warn ("Could not create Cybersource Billing Profile for User " + KanevaWebGlobals.CurrentUser.UserId.ToString () + " because system is unavailable");
                                ShowErrorMessage ("Information not saved.  System is currently unavailable.  Please try again later.", false);
                                return;
                            }
                            else  // error
                            {
                                m_logger.Warn ("Error creating Cybersource Billing Profile for User " + KanevaWebGlobals.CurrentUser.UserId.ToString () + ". Err: " + userErrorMessage);
                                ShowErrorMessage (userErrorMessage);
                                return;
                            }
                        }

                        // Insert the new billing address
                        addressId = UsersUtility.InsertAddress (KanevaWebGlobals.CurrentUser.UserId, Server.HtmlEncode (billInfo.FullName), 
                            Server.HtmlEncode (billInfo.Address1), Server.HtmlEncode (billInfo.Address2), Server.HtmlEncode (billInfo.City), 
                            Server.HtmlEncode (billInfo.State), Server.HtmlEncode (billInfo.PostalCode), Server.HtmlEncode (billInfo.PhoneNumber), 
                            Server.HtmlEncode (billInfo.Country));
                    }

                    int pointTransactionId = Convert.ToInt32 (drOrder["point_transaction_id"]);
                    
                    // Save the address to the order
                    StoreUtility.UpdatePointTransactionAddressId (pointTransactionId, addressId);
                    
                    // Save the payment type
                    StoreUtility.UpdatePointTransaction (pointTransactionId, Constants.ePAYMENT_METHODS.KANEVA_CYBERSOURCE);

                    // Add the credit card info to the order if we are saving credit card.  If card has already
                    // been saved and we are just updating address, then skip this step
                    if (billInfo.CardType != string.Empty)
                    {
                        AddPaymentDetails (billInfo.CardNumberLast4); 
                    }

                    Response.Redirect ("~/checkout/orderConfirm_B.aspx?orderId=" + orderId.ToString () + "&pass=" + (Request.QueryString["pass"] != null ? Request.QueryString["pass"].ToString () : ""), false);
                }
                catch (Exception exc)
                {
                    m_logger.Error ("Error saving billing information to users order: UserId=" + KanevaWebGlobals.CurrentUser.UserId.ToString () + " : ", exc);
                    ShowErrorMessage ("An error occurred while trying to save your address.");
                    return;
                }
            }
            else
            {
                Response.Redirect ("~/mykaneva/managecredits.aspx");
            }
        }

        /// <summary>
        /// Click event for the Clear Info linkbutton
        /// </summary>
        protected void lbClearInfo_Click (object sender, EventArgs e)
        {
            bool bSystemDown = false;
            string userErrorMessage = "";

            // Get users billing subscription id
            string billSubId = UsersUtility.GetUserBillingSubscriptionId (KanevaWebGlobals.CurrentUser.UserId);
            
            // Fill the object with bogus info so we can get the users info out of the subscription.
            // We do this because we can't just clear the info, we have to set it to something, so 
            // we set it to bogus info then clear the subscription id form the users profile
            BillingInfo billInfo = GetClearProfileBillingInfo (billSubId);

            // if they have an existing profile, then update that profile
            if (billInfo.SubscriptionId.Length > 0)
            {
                if (!UpdateCybersourceSubscription (billInfo, true, ref bSystemDown, ref userErrorMessage))
                {
                    if (bSystemDown)
                    {
                        m_logger.Warn ("Could not update Cybersource Billing Profile for User " + KanevaWebGlobals.CurrentUser.UserId + " because system is unavailable");
                        ShowErrorMessage ("Information not saved.  System is currently unavailable.  Please try again later.", false);
                        return;
                    }
                    else  // error
                    {
                        m_logger.Warn ("Error updating User " + KanevaWebGlobals.CurrentUser.UserId + " Cybersource Billing Profile. Err: " + userErrorMessage);
                        ShowErrorMessage (userErrorMessage);
                        return;
                    }
                }

                 // now that the info is gone from cybersource, lets clear the subscription id
                try
                {
                    if (UsersUtility.ClearBillingSubscriptionId (KanevaWebGlobals.CurrentUser.UserId, billSubId) < 1)         
                    {
                        // Setup info box to display error message.
                        cvBlank.ErrorMessage = "Billing information was not cleared.  Please try again.";
                        cvBlank.IsValid = false;
                        valSum.CssClass = "errBox";
                        valSum.DisplayMode = ValidationSummaryDisplayMode.SingleParagraph;
                        valSum.HeaderText = "";
                        valSum.Style.Add ("text-align", "center");
                    }
                }
                catch (Exception ex)
                {
                    m_logger.Warn ("Error clearing subscription data for User " + KanevaWebGlobals.CurrentUser.UserId + ".  Subscription Id = " + billSubId + ". Err: " + ex.ToString ());
                    ShowErrorMessage (userErrorMessage);
                    return;
                }

                // Get blank info since data has just been cleared
                billInfo = new BillingInfo ();
                PopulateFields (billInfo);

                // Setup info box to display success message.
                cvBlank.ErrorMessage = "Billing information successfully cleared.";
                cvBlank.IsValid = false;
                valSum.CssClass = "infoBox";
                valSum.DisplayMode = ValidationSummaryDisplayMode.SingleParagraph;
                valSum.HeaderText = "";
                valSum.Style.Add ("text-align", "center");

                // Hide the clear button
                ShowClearInfoButton (false);
            }
        }

        #endregion Event Handlers


        #region Declerations

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

        #endregion Declerations
    }
}
