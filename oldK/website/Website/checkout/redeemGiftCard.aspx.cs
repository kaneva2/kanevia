///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using log4net;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for redeemGiftCard.
	/// </summary>
	public class redeemGiftCard : NoBorderPage
	{
		private void Page_Load(object sender, System.EventArgs e)
		{
			Response.Expires = 0;
			Response.CacheControl = "no-cache";

			if (!Request.IsAuthenticated)
			{
				Response.Redirect("~/redeemlogin.aspx");
			}

            if (Request.QueryString["show"] == "valid")
            {
                divValidated.Visible = true;
            }
                

			// Clear any previous errors
			lblErrors.Text = "";

			// Set up purchase button
			btnRedeemCard.CausesValidation = false;
			btnRedeemCard.Attributes.Add ("onclick", "javascript: if (typeof(Page_ClientValidate) == 'function') if (!Page_ClientValidate()){return false;};this.value='  Please wait...  ';this.disabled = true;ShowLoading(true,'Please wait. Your card is being processed...');" + ClientScript.GetPostBackEventReference(this.btnRedeemCard, "", false));
		}

		
		#region Helper Methods
		/// <summary>
		/// ShowMessage
		/// </summary>
		/// <param name="strMessage"></param>
		private void ShowMessage (string strMessage)
		{
			lblErrors.Text = strMessage;
		}
		#endregion

		#region Event Handlers
		/// <summary>
		/// The make purchase event handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnRedeemCard_Click (object sender, EventArgs e) 
		{		
			string errorMsg = "";
			if (!InCommGiftCard.RedeemGiftCard (GetUserId (), txtPinNumber.Text.Trim (), Common.GetVisitorIPAddress(), ref errorMsg))
			{
				// Failure
				ShowMessage ("Please correct the following errors:<br><br>" + errorMsg);
				return;
			}

			Response.Redirect (ResolveUrl ("~/checkout/redeemComplete.aspx"));
		}

		#endregion
		
		#region Declerations
		protected Button	btnRedeemCard;
		protected TextBox	txtPinNumber;
		protected Label		lblErrors;
        protected HtmlContainerControl divValidated;
		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
