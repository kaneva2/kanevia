<%@ Page language="c#" Codebehind="launchGame.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.launchGame" %>
<%@ Register TagPrefix="Kaneva" TagName="HeaderText" Src="../usercontrols/HeaderText.ascx" %>

<Kaneva:HeaderText runat="server" Text="Item Ready for Download!"/>

<script>

function checkBrowser (sbrowser)
{
//possible strings 'konqueror''safari''omniweb''opera''webtv''icab''msie''compatible'
	return navigator.userAgent.toLowerCase ().indexOf (sbrowser) + 1;
}

function OnLoad()
{
	// If we are not on windows, don't even attemp to launch Iris, Iris is windows only.
	if (navigator.platform.indexOf ("Win") > -1)
	{		
		
	}
	else
	{
	
	}
}
</script>

	
	<table cellpadding="0" cellspacing="10" border="0" width="720" style="border-top: 1px solid #cccccc;border-bottom: 1px solid #cccccc">
		<tr>
			<td class="bodyText">
				<br/><br/>				
				<table width="700" cellpadding="0" cellspacing="10" border="0">
					<tr>
						<td class="bodyText" width="100%" align="left">
							&nbsp;<BR/>
							<a runat="server" id="aGameLauncher" style="COLOR: #3258ba; font-size: 18px;">Get '<asp:Label id="lblGameName" runat="server"/>' Using Kaneva Game Launcher</a>
							<BR/><span class="dateStamp">Kaneva Game Launcher <asp:Label id="lblVersion" runat="server"/></span>
							<BR/>&nbsp;
						</td>
					</tr>
					<tr>
							<td class="bodyText" width="100%" align="left">
								About Kaneva Game Launcher<br/><br/>
								<li>The easy way to get games on your Windows XP/2000 computer.<br/>
								<li>Conveniently receive and update your Powered by Kaneva Games.<br/>
								<li>Runs in the background when you start your game.<br/>
								<li>Never again lose game CDs or use outdated versions.<br/>
								<li>Questions? Get more details from the <a href="http://docs.kaneva.com/bin/view/Public/KanevaGameLauncherHome" class="LnavText" target="_resource">Kaneva Game Launcher User Guide</a>.<br/>
							</td>
						</tr>
				</table><br>
			
				<br><br>
				Note: The game launcher and game play require Windows XP/2000.
			</td>
		</tr>
	</table>
	