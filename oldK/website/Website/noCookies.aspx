<%@ Page language="c#" Codebehind="noCookies.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.noCookies" %>
<%@ Register TagPrefix="Kaneva" TagName="HeaderText" Src="usercontrols/HeaderText.ascx" %>
<Kaneva:HeaderText runat="server" Text="Cookies must be allowed"/>
	
<CENTER>
<table cellpadding="0" cellspacing="1" border="0" width="700" ID="Table2">
	<tr>
	<td class="bodyText" align="left"><br><br>
	<b style="font-size: 14px;">You are seeing this page because your internet browser currently does not allow cookies. 
		www.kaneva.com requires an Internet browser setting that allows cookies.</B>
		<BR><BR>
		Cookies are small text files stored on your computer that tell 
		www.kaneva.com when you're signed in. To learn how to allow cookies, see online help in your web browser.
		<BR><BR>
		Once you have set your browser to allow cookies, close it, open a new browser 
		window and return to <A href="http://www.kaneva.com/">http://www.kaneva.com</A> to continue your digital entertainment experience.<br><br><br>

	</td>
	</tr>
</table>
</CENTER>