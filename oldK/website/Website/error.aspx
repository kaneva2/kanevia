<%@ Page language="c#" MasterPageFile="~/masterpages/GenericPageTemplate.Master" Codebehind="error.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.Error" %>

<asp:Content ID="cntProfileHead" runat="server" contentplaceholderid="cph_HeadData" >
    <meta name="title" content="Kaneva. The Online Community and Social Network." />
    <meta name="description" content="Kaneva is the first to combine social networking and a free 3D virtual world. It's a whole new way to connect with friends. Have fun in a vibrant virtual world full of people, cool places and fun games. Create your avatar and get a free 3D home today!"/>
    <meta name="keywords" content="free virtual world, virtual world, virtual worlds, 3D world, online avatar community, mmo game, virtual reality, caneva, kaniva" />
    <script type="text/javascript" src="jscript/error/error.js"></script>
    <link href="css/base/base.css" type="text/css" rel="stylesheet" />
    <link href="css/base/footer.css" type="text/css" rel="stylesheet" />
    <link href="css/error/Error.css" type="text/css" rel="stylesheet" />
    <NOSCRIPT><META HTTP-EQUIV="refresh" CONTENT="1" URL="noJavascript.aspx" /></NOSCRIPT>
</asp:Content>

<asp:Content id="cntErrorMessage" runat="server" contentplaceholderid="cph_Body" >
	<div id="genericError" class="errorContainer"  runat="server" visible="false">
        <div class="errorWrapper">
            <h4 class="errorHeading">Application Error</h4>
            <p class="infoBlock">
                We are sorry, but Kaneva experienced a problem completing your request. <br />
                You can:
            </p>
            <ul class="errorOptions">
                <li>Use your back button and retry your request</li>
                <li>Return to the <a href="free-virtual-world.kaneva">home page</a></li>
            </ul>
        </div>
	</div>
	<div id="checkoutClosed" class="errorContainer"  runat="server" visible="false">
        <div class="errorWrapper">
            <h4 class="errorHeading">Credit Purchasing Error</h4>
            <p class="infoBlock">
	            Sorry but credit purchasing is not available at this time.<br />
            </p>
        </div>
	</div>
	<div id="pageNotFound" class="errorContainer"  runat="server" visible="false">
        <div class="errorWrapper">
            <h4 class="errorHeading">Page Not Found</h4>
            <p class="infoBlock">
                The page you requested doesn't exist.
            </p>
            <ul class="errorOptions">
                <li>If you are looking for someone, try <a href="people/people.kaneva">Find Friends</a></li>
                <li>If you are looking for something to do, try <a href="community/channel.kaneva">Find a Community</a></li>
                <li>If you are looking for someone, try <a id="aShop" runat="server" target="_blank">Shop Kaneva</a></li>
            </ul>
        </div>
	</div>
	<div id="profileNotFound" class="errorContainer"  runat="server" visible="false">
        <div class="errorWrapper">
            <h4 class="errorHeading">Member Profile <asp:label ID="profileName" runat="server" /> Not Found</h4>
            <p class="infoBlock">
                Please try again, or you can try to <a href="people/people.kaneva">Find a Friend</a>.
            </p>
        </div>
	</div>
	<div id="communityNotFound" class="errorContainer"  runat="server" visible="false">
        <div class="errorWrapper">
            <h4 class="errorHeading"><asp:label ID="communityName" runat="server" /> Community Not Found</h4>
            <p class="infoBlock">
                Please try again, or you can try to <a href="community/channel.kaneva">Find a Community</a>.
            </p>
        </div>
	</div>
	<div id="threeDAppNotFound" class="errorContainer"  runat="server" visible="false">
        <div class="errorWrapper">
            <h4 class="errorHeading">World <asp:label ID="threeDAppName" runat="server" /> Not Found</h4>
            <p class="infoBlock">
                Please try again, or you can try to <a href="community/channel.kaneva">Find a World</a>.
            </p>
        </div>
	</div>
	<div id="accessDenied" class="errorContainer"  runat="server" visible="false">
        <div class="errorWrapper">
            <h4 class="errorHeading">You do not have permission to access this page.</h4>
        </div>
	</div>
	<div id="invalidAccount" class="errorContainer"  runat="server" visible="false">
        <div class="errorWrapper">
            <h4 class="errorHeading">Account Not Valid</h4>
            <p class="infoBlock">
                Your account is not valid for making purchases.
				<p>If you feel you have received this message
				in error please <a href="http://support.kaneva.com" target="_blank">Contact Support</a>.</p>
            </p>
        </div>
	</div>
	
</asp:Content>

