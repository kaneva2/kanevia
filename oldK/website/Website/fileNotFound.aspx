<%@ Page language="c#" Codebehind="fileNotFound.aspx.cs" MasterPageFile="~/masterpages/IndexPageTemplate.Master" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.fileNotFound" %>


<asp:Content id="cph_Body" runat="server" contentplaceholderid="cph_Body" >
	<link href="css/base/base_new.css" type="text/css" rel="stylesheet" />
	<link href="/css/events.css?v6" type="text/css" rel="stylesheet" />

	<style type="text/css">
		#wrapper {border:1px solid #ccc;border-top:none;background-color:#fff;width:1004px;margin:0 auto;height:auto !important;height:100%;min-height:100%;padding-bottom:0;clear:both;display:inline-block;}
		.PageContainer {background-color:#fff;width:1004px;height:100%;border-top:solid 1px #fff;}
		#formContainer {width:780px;height:400px;background-color:white;text-align:left;padding:10px 0px 20px 0;margin:20px auto 0 auto;}
		#formContainer h1 {width:250px;margin-bottom:30px;font-weight:bold;}
		#formContainer p {margin-bottom:30px;line-height:1.2em;}
		.solo {margin:70px auto 0 auto;}
	</style>
	
	<div class="PageContainer"> 
	
		<div id="formContainer"> 

			<span id="spnUrl" runat="server" visible="false">
			<h1>File Not Found</h1>

			<p>The page you are looking for could have been 
				removed, had its name changed, or is temporarily unavailable. Please review the 
				URL and make sure that it is spelled correctly.</p>

			<p>Requested URL is '<asp:Label runat="server" id="lblURL"/>'</p>
			</span>

			<span id="spnProfile" runat="server" visible="false">
				<h1 class="solo">Profile Not Found</h1>
			</span>

		</div>

	</div>

</asp:Content>