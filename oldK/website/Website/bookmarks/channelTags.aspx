<%@ Page language="c#" Codebehind="channelTags.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.channelTags" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../usercontrols/Pager.ascx" %>


<link href="../css/home.css" rel="stylesheet" type="text/css" />		
<link href="../css/kanevaSC.css" rel="stylesheet" type="text/css"/>
<link href="../css/friends.css" rel="stylesheet" type="text/css"/>
<link href="../css/kanevaText.css" type="text/css" rel="stylesheet">
<link href="../css/new.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../jscript/dw_tooltip/dw_event.js" ></script>
<script language="javascript" src="../jscript/dw_tooltip/dw_viewport.js" ></script>
<script language="javascript" src="../jscript/dw_tooltip/dw_tooltip.js" ></script>


<script type="text/javascript">
window.onload = ttINIT;

function ttINIT ()
{
	Tooltip.init();
	OnLoad();
}

/***********************************************
* Switch Content script- � Dynamic Drive (www.dynamicdrive.com)
* This notice must stay intact for legal use. Last updated April 2nd, 2005.
* Visit http://www.dynamicdrive.com/ for full source code
***********************************************/

var enablepersist="off"		//Enable saving state of content structure using session cookies? (on/off)
var collapseprevious="no"	//Collapse previously open content when opening present? (yes/no)

var contractsymbol='- '		//HTML for contract symbol. For image, use: <img src="whatever.gif">
var expandsymbol='+ '		//HTML for expand symbol.


if (document.getElementById){
document.write('<style type="text/css">')
document.write('.switchcontent{display:none;}')
document.write('</style>')
}

function getElementbyClass(rootobj, classname){
var temparray=new Array()
var inc=0
var rootlength=rootobj.length
for (i=0; i<rootlength; i++){
if (rootobj[i].className==classname)
temparray[inc++]=rootobj[i]
}
return temparray
}

function sweeptoggle(ec){
var thestate=(ec=="expand")? "block" : "none"
var inc=0
while (ccollect[inc]){
ccollect[inc].style.display=thestate
inc++
}
revivestatus()
}


function contractcontent(omit){
var inc=0
while (ccollect[inc]){
if (ccollect[inc].id!=omit)
ccollect[inc].style.display="none"
inc++
}
}

function expandcontent(curobj, cid){
var spantags=curobj.getElementsByTagName("SPAN")
var showstateobj=getElementbyClass(spantags, "showstate")
if (ccollect.length>0){
if (collapseprevious=="yes")
contractcontent(cid)
document.getElementById(cid).style.display=(document.getElementById(cid).style.display!="block")? "block" : "none"
if (showstateobj.length>0){ //if "showstate" span exists in header
if (collapseprevious=="no")
showstateobj[0].innerHTML=(document.getElementById(cid).style.display=="block")? contractsymbol : expandsymbol
else
revivestatus()
}
}
}

function revivecontent(){
contractcontent("omitnothing")
selectedItem=getselectedItem()
selectedComponents=selectedItem.split("|")
for (i=0; i<selectedComponents.length-1; i++)
document.getElementById(selectedComponents[i]).style.display="block"
}

function revivestatus(){
var inc=0
while (statecollect[inc]){
if (ccollect[inc].style.display=="block")
statecollect[inc].innerHTML=contractsymbol
else
statecollect[inc].innerHTML=expandsymbol
inc++
}
}

function get_cookie(Name) { 
var search = Name + "="
var returnvalue = "";
if (document.cookie.length > 0) {
offset = document.cookie.indexOf(search)
if (offset != -1) { 
offset += search.length
end = document.cookie.indexOf(";", offset);
if (end == -1) end = document.cookie.length;
returnvalue=unescape(document.cookie.substring(offset, end))
}
}
return returnvalue;
}

function getselectedItem(){
if (get_cookie(window.location.pathname) != ""){
selectedItem=get_cookie(window.location.pathname)
return selectedItem
}
else
return ""
}

function saveswitchstate(){
var inc=0, selectedItem=""
while (ccollect[inc]){
if (ccollect[inc].style.display=="block")
selectedItem+=ccollect[inc].id+"|"
inc++
}

document.cookie=window.location.pathname+"="+selectedItem
}

function do_onload(){
uniqueidn=window.location.pathname+"firsttimeload"
var alltags=document.all? document.all : document.getElementsByTagName("*")
ccollect=getElementbyClass(alltags, "switchcontent")
statecollect=getElementbyClass(alltags, "showstate")
if (enablepersist=="on" && ccollect.length>0){
document.cookie=(get_cookie(uniqueidn)=="")? uniqueidn+"=1" : uniqueidn+"=0" 
firsttimeload=(get_cookie(uniqueidn)==1)? 1 : 0 //check if this is 1st page load
if (!firsttimeload)
revivecontent()
}
if (ccollect.length>0 && statecollect.length>0)
revivestatus()
}

if (window.addEventListener)
window.addEventListener("load", do_onload, false)
else if (window.attachEvent)
window.attachEvent("onload", do_onload)
else if (document.getElementById)
window.onload=do_onload

if (enablepersist=="on" && document.getElementById)
window.onunload=saveswitchstate

</script>

<asp:ValidationSummary ShowMessageBox="False" ShowSummary="True" Class="formError" id="valSum" DisplayMode="BulletList" runat="server" HeaderText="Please correct the following errors:"/>

<table border="0" cellspacing="0" cellpadding="0" class="newcontainer" align="center">
	<tr>
		<td>
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="newcontainerborder">
	<tr>
		<td width="6" class=""></td>
		<td colspan="3" class=""></td>
		<td width="7" class=""></td>
	</tr>
	<tr> 
		<td rowspan="3"></td>
		<td width="10" rowspan="3" valign="top" class=""></td>
		<td valign="middle" class="">
			<br /><br />
			<table width="100%"  border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="200"><h1>My Worlds</h1></td>
					<td width="100" align="left"></td>
					<td align="center">&nbsp;</td>
					<td align="right">Go To: <a id="A4" runat="server" href="~/community/channel.kaneva">All Worlds</a> |<a id="A2" runat="server" href="~/mykaneva/messagecenter.aspx?mc=cr">World Requests</a> | <a id="A1" runat="server" href="~/mykaneva/messagecenter.aspx?mc=ci">World Invites</a> | <a id="A3" runat="server" href="~/community/StartWorld.aspx">Create a World</a> </td>
				</tr>
			</table>
						
		</td>
		<td width="10" rowspan="3" valign="top" class=""></td>
		<td rowspan="3" class=""><img runat="server" src="~/images/spacer.gif" width="1" height="1"></td>
		</tr>
	<tr>
			<td valign="top" class="">
					<!-- ACTION MENU -->
			<!-- END ACTION MENU -->
			
			 <br />
			<!-- DISPLAY OPTIONS -->
			<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
				<tr>
					<td class="intBorderB1TopLeft"></td>
					<td class="intBorderB1Top"></td>
					<td class="intBorderB1TopRight"></td>
				</tr>
				<tr>
					<td class="intBorderB1BorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1"></td>
					<td valign="top" class="intBorderB1BgInt1">
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="right">
									<asp:label runat="server" id="lblSearch" cssclass="insideTextNoBold" style="margin-right:15px;"/>
									<Kaneva:Pager runat="server" id="pgTop"/>
								</td>
							</tr>
						</table>
					</td>
					<td class="intBorderB1BorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1"></td>
				</tr>
				<tr>
					<td class="intBorderB1BottomLeft"></td>
					<td class="intBorderB1Bottom"></td>
					<td class="intBorderB1BottomRight"></td>
				</tr>
			</table>
			<!-- END DISPLAY OPTIONS -->
			
			<!-- IMAGE SPACER -->
			<table width="100%"  border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="intBorderBgInt1"><img runat="server" src="~/images/spacer.gif" width="1" height="2"></td>
				</tr>
			</table>
			<!-- END IMAGE SPACER -->
			
			<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
				<tr>
					<td height="7" class="intBorderCTopLeft"></td>
					<td colspan="10" class="intBorderCTop"></td>
					<td class="intBorderCTopRight"></td>
				</tr>
				<tr>
					<td height="20" class="intBorderCBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
					<td valign="middle" align="center" class="intBorderCBgInt1" width="100"><span class="content"><a>Membership Status</a></span></td>
					<td width="1" class="sepTit1"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
					<td class="intBorderCBgInt1" align="center"><span class="content"><a>World Name and Description</a></span></td>
					<td width="1" class="sepTit1"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
					<td valign="middle" class="intBorderCBgInt1" align="center"><span class="content"><a>Actions</a></span></td>			
					<td valign="middle" class="intBorderCBgInt1" align="left"></td>
					<td width="1" class="sepTit1"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>			  
					<td width="130" valign="middle" class="intBorderCBgInt1" align="center"><span class="content"><a>World Email</a></span> 
					<asp:image id="imgTT" runat="server" imageurl="~/images/button_tooltip.gif" onmouseover="doTooltip(event,'As it happens - Receive individual emails as activity happens <br><br>Daily Digests - Receive all notifications in daily email <br><br>None - I do not wish to receive any emails for this World')" onmouseout="hideTip();"/></td>
					<td width="1" class="sepTit1"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>			   
					<td valign="middle" class="intBorderCBgInt1" align="center" width="110"><span class="content"><a>Stats</a></span></td>
					<td class="intBorderCBorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
				</tr>
				<tr>
					<td height="7" class="intBorderCBottomLeft"></td>
					<td colspan="10" class="intBorderCBottom"></td>
					<td class="intBorderCBottomRight"></td>
				</tr>
				
				<asp:Repeater runat="server" id="rptChannels">  
					<ItemTemplate>
						<tr class="intColor1">
							<td height="125" class="intBorderC1BorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="110" ID="Img1"/></td>
							<td align="center" class="intColor1"> 
								<div style="padding:16px 10px 0 10px;"> 
									<strong><asp:label id="lblMemberType" runat="server"/></strong>
									<input type="hidden" runat="server" id="hidChannelId" value='<%#DataBinder.Eval(Container.DataItem, "community_id")%>' NAME="hidChannelId">
								</div>
							</td>
							<td class="sepInt1"><img runat="server" src="~/images/spacer.gif" width="1" height="1" ID="Img2"/></td>
							
							<td align="left" class="intColor1" valign="top">
							
							<div style="padding:12px 5px 0 5px;height:1px;">
							
								<table width="100%" border="0" cellpadding="0" cellspacing="5">
									<tr>
										<td rowspan="4" width="100" valign="top">
							
											<!--image holder-->
											<div class="framesize-small">
												<div class="restricted" runat="server" visible='<%# (DataBinder.Eval(Container.DataItem, "is_adult").Equals ("Y")) %>'></div>
												<div class="over21" runat="server" visible='<%# (DataBinder.Eval(Container.DataItem, "over_21_required").Equals ("Y")) %>'></div>
												<div class="frame">
													<span class="ct"><span class="cl"></span></span>
													<div class="imgconstrain">
														<a href='<%# GetBroadcastChannelUrl (DataBinder.Eval (Container.DataItem, "name_no_spaces").ToString ())%>' style="cursor: hand;">
															<img runat="server" src='<%# GetBroadcastChannelImageURL (DataBinder.Eval(Container.DataItem, "thumbnail_small_path").ToString (), "sm") %>' border="0" id="Img20"/>
														</a>
													</div>
													<span class="cb"><span class="cl"></span></span>
												</div>
											</div>
											<!--end image holder-->
										</td>
									</tr>
									<tr>
									  <td><span class="content"><a href='<%# GetBroadcastChannelUrl (DataBinder.Eval (Container.DataItem, "name_no_spaces").ToString ())%>'><%# TruncateWithEllipsis (DataBinder.Eval (Container.DataItem, "name").ToString (), 40)%></a></span></td>
									  <td><span class="insideTextNoBold">Owner: <a class="dateStamp" href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "username").ToString ())%>><%# DataBinder.Eval(Container.DataItem, "username") %></a></span></td>
									  <td align="right"><span class="insideTextNoBold">Access: <strong><%# GetChannelStatus (DataBinder.Eval (Container.DataItem, "is_public")) %></strong></span></td>
									</tr>
									<tr>
										<td colspan="3"><span class="insideBoxText11"><%# DataBinder.Eval(Container.DataItem, "description") %></span></td>
									</tr>
									<tr>
										<td colspan="3"><span class="insideTextNoBold">Tags: <asp:label id="Label1" runat="server" text='<%# GetChannelTags (DataBinder.Eval(Container.DataItem, "keywords").ToString ())%>'/></span></td>
									</tr>
								</table>
								
							</div>
							
							</td>
							<td class="sepInt1"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
							<td valign="top" class="intColor1" align="center">
								<div id="divmessage" style="padding:12px 10px 0 10px;">
									<asp:imagebutton id="btnView" commandname="cmdView" alternatetext="View World" imageurl="~/images/button_view.gif" runat="server" border="0" />
									<asp:imagebutton id="btnViewEdit" commandname="cmdEdit" alternatetext="Edit World" imageurl="~/images/button_editCommunity.gif" runat="server" border="0" />
									<asp:imagebutton id="btnDelete" commandname="cmdDeleteChannel" alternatetext="Delete World" imageurl="~/images/button_delete_lg.gif" runat="server" border="0" />
									<asp:imagebutton id="btnQuit" commandname="cmdQuitChannel" alternatetext="Quit World" imageurl="~/images/button_quit_lg.gif" runat="server" border="0" />
								</div>
							</td>									  
							
							
							<td valign="middle" class="intColor1" align="left">	</td>
							<td valign="top" class="sepInt1"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
							<td valign="top" class="intColor1" width="130">

								<div id="divNotification" runat="server" style="padding:12px 10px 0 10px;">
									<div id="divNotifySummary" runat="server"></div><br><br>
									<span id="spnNotify" runat="server" style="cursor:hand; cursor:pointer"><span class="showstate" style="width: 10px; display: block; float: left;"></span><a href="javascript:void(0);">Change Setting</a></span>
									<br><br>
									<div id="divNotifyOptions" runat="server" class="switchcontent" style="text-align:left;">
										<asp:radiobuttonlist id="rblNotify" runat="server" visible="False">
											<asp:listitem value="1">AS it happens</asp:listitem>
											<asp:listitem value="2">Daily Digest</asp:listitem>
											<asp:listitem value="3">Weekly Digest</asp:listitem>
											<asp:listitem value="0">None</asp:listitem>
										</asp:radiobuttonlist>
										<br><br>
										
										<div align="center"><asp:imagebutton id="btnSaveNotify" runat="server" commandname="cmdSaveNotifyOption" alternatetext="Save" imageurl="~/images/button_save.gif" border="0" /></div>
									</div>
								</div>
								
							</td>
							<td valign="top" class="sepInt1"><img runat="server" src="~/images/spacer.gif" width="1" height="1"/></td>
							<td valign="top" class="intColor1" align="center">
								<div style="padding:10px;text-align:left;">
								<span class="insideTextNoBold">Raves: <%# DataBinder.Eval (Container.DataItem, "number_of_diggs") %></span><br/>
								<span class="insideTextNoBold">Views: <%# DataBinder.Eval (Container.DataItem, "number_of_views") %></span><br/>
								<span class="insideTextNoBold">Members: <%# DataBinder.Eval (Container.DataItem, "number_of_members") %></span>
								<br/>
								<img runat="server" src="~/images/spacer.gif" width="15" height="28" ID="Img7"><%# GetChannelContentType (DataBinder.Eval (Container.DataItem, "is_adult").ToString()) %>
								</div>
							</td>
							<td class="intBorderC1BorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1" ID="Img17"/></td>
			 			</tr>
					</ItemTemplate>
					<AlternatingItemTemplate>
						<tr class="intColor2">
							<td height="125" class="intBorderC1BorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="110" id="Img3"/></td>
							<td align="center" class="intColor2"> 
								<div style="padding:16px 10px 0 10px;"> 
									<strong><asp:label id="lblMemberType" runat="server"/></strong>
									<input type="hidden" runat="server" id="hidChannelId" value='<%#DataBinder.Eval(Container.DataItem, "community_id")%>' name="hidChannelId">
								</div>
							</td>
							<td class="sepInt1"><img runat="server" src="~/images/spacer.gif" width="1" height="1"/></td>
							
							<td align="left" class="intColor2" valign="top">
							
							<div style="padding:12px 5px 0 5px;height:1px;">
							
								<table width="100%" border="0" cellpadding="0" cellspacing="5">
									<tr>
										<td rowspan="4" width="100" valign="top">
							
											<!--image holder-->
											<div class="framesize-small">
											<div class="restricted" runat="server" visible='<%# (DataBinder.Eval(Container.DataItem, "is_adult").Equals ("Y")) %>'></div>
												<div class="over21" runat="server" visible='<%# (DataBinder.Eval(Container.DataItem, "over_21_required").Equals ("Y")) %>'></div>
												<div class="frame">
													<span class="ct"><span class="cl"></span></span>
													<div class="imgconstrain">
														<a href='<%# GetBroadcastChannelUrl (DataBinder.Eval (Container.DataItem, "name_no_spaces").ToString ())%>' style="cursor: hand;">
															<img runat="server" src='<%# GetBroadcastChannelImageURL (DataBinder.Eval(Container.DataItem, "thumbnail_small_path").ToString (), "sm") %>' border="0" id="Img5"/>
														</a>
													</div>
													<span class="cb"><span class="cl"></span></span>
												</div>
											</div>
											<!--end image holder-->
										</td>
									</tr>
									<tr>
									  <td><span class="content"><a href='<%# GetBroadcastChannelUrl (DataBinder.Eval (Container.DataItem, "name_no_spaces").ToString ())%>'><%# TruncateWithEllipsis (DataBinder.Eval (Container.DataItem, "name").ToString (), 40)%></a></span></td>
									  <td><span class="insideTextNoBold">Owner: <a class="dateStamp" href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "username").ToString ())%>><%# DataBinder.Eval(Container.DataItem, "username") %></a></span></td>
									  <td align="right"><span class="insideTextNoBold">Access: <strong><%# GetChannelStatus (DataBinder.Eval (Container.DataItem, "is_public")) %></strong></span></td>
									</tr>
									<tr>
										<td colspan="3"><span class="insideBoxText11"><%# DataBinder.Eval(Container.DataItem, "description") %></span></td>
									</tr>
									<tr>
										<td colspan="3"><span class="insideTextNoBold">Tags: <asp:label id="Label3" runat="server" text='<%# GetChannelTags (DataBinder.Eval(Container.DataItem, "keywords").ToString ())%>'/></span></td>
									</tr>
								</table>
								
							</div>
							
							</td>
							<td class="sepInt1"><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img6"/></td>
							<td valign="top" class="intColor2" align="center">
								<div style="padding:12px 10px 0 10px;">
									<asp:imagebutton id="btnView" commandname="cmdView" alternatetext="View World" imageurl="~/images/button_view.gif" runat="server" border="0" />
									<asp:imagebutton id="btnViewEdit" commandname="cmdEdit" alternatetext="Edit World" imageurl="~/images/button_editCommunity.gif" runat="server" border="0" />
									<asp:imagebutton id="btnDelete" commandname="cmdDeleteChannel" alternatetext="Delete World" imageurl="~/images/button_delete_lg.gif" runat="server" border="0" />
									<asp:imagebutton id="btnQuit" commandname="cmdQuitChannel" alternatetext="Quit World" imageurl="~/images/button_quit_lg.gif" runat="server" border="0" />
								</div>
							</td>									  
							
							
							<td valign="middle" class="intColor2" align="left">	</td>
							<td valign="top" class="sepInt1"><img runat="server" src="~/images/spacer.gif" width="1" height="1"/></td>
							<td valign="top" class="intColor2" width="130">
							
								<div id="divNotification" runat="server" style="padding:12px 10px 0 10px;">
									<div id="divNotifySummary" runat="server"></div><br><br>
									<span id="spnNotify" runat="server" style="cursor:hand; cursor:pointer"><span class="showstate" style="width: 10px; display: block; float: left;"></span><a href="javascript:void(0);">Change Setting</a></span>
									<br><br>
									<div id="divNotifyOptions" runat="server" class="switchcontent" style="text-align:left;">
										<asp:radiobuttonlist id="rblNotify" runat="server" visible="False">
											<asp:listitem value="1">AS it happens</asp:listitem>
											<asp:listitem value="2">Daily Digest</asp:listitem>
											<asp:listitem value="3">Weekly Digest</asp:listitem>
											<asp:listitem value="0">None</asp:listitem>
										</asp:radiobuttonlist>
										<br><br>
										
										<div align="center"><asp:imagebutton id="btnSaveNotify" runat="server" commandname="cmdSaveNotifyOption" alternatetext="Save" imageurl="~/images/button_save.gif" border="0" /></div>
									</div>
								</div>
								
							</td>
							<td valign="top" class="sepInt1"><img runat="server" src="~/images/spacer.gif" width="1" height="1"/></td>
							<td valign="top" class="intColor2" align="center">
								<div style="padding:10px;text-align:left;">
								<span class="insideTextNoBold">Raves: <%# DataBinder.Eval (Container.DataItem, "number_of_diggs") %></span><br/>
								<span class="insideTextNoBold">Views: <%# DataBinder.Eval (Container.DataItem, "number_of_views") %></span><br/>
								<span class="insideTextNoBold">Members: <%# DataBinder.Eval (Container.DataItem, "number_of_members") %></span>
								<br/>
								<img runat="server" src="~/images/spacer.gif" width="15" height="28" id="Img12"><%# GetChannelContentType (DataBinder.Eval (Container.DataItem, "is_adult").ToString()) %>
								</div>
							</td>
							<td class="intBorderC1BorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img13"/></td>
			 			</tr>
					</AlternatingItemTemplate>
				</asp:Repeater>
				
				<tr>
					<td height="7" class="intBorderC1BottomLeft"></td>
					<td colspan="10" class="intBorderC1Bottom"></td>
					<td class="intBorderC1BottomRight"></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" bgcolor="#f1f1f2"><img runat="server" src="~/images/spacer.gif" width="1" height="10"></td>
	</tr>
	<tr>
		<td class=""></td>
		<td colspan="3" class=""></td>
		<td class=""></td>
	</tr>
</table>
		
		</td>
	</tr>
</table>
<!-- Info text displayed in tool bar -->
<input id="hdInfoText" runat="server" type="hidden" name="hdInfoText"
	value="Grow your network of Kaneva friends and manage your Friends list by placing everyone in distinct Groups (e.g. Guys and Dolls, Friends and Family)."/>

