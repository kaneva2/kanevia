///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Data;
using System.Text;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for channelTags.
	/// </summary>
	public class channelTags : SortedBasePage
    {
        #region Declarations
        protected Kaneva.Pager pgTop;
        protected PlaceHolder phBreadCrumb;
        protected Label lblSearch;

        protected Repeater rptChannels;
        private CommunityFacade communityFacade = new CommunityFacade();
        #endregion

        protected channelTags () 
		{
			Title = "My Worlds";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			// They must be logged in
			if (!Request.IsAuthenticated)
			{
				Response.Redirect (GetLoginURL ());
				return;
			}

			if (!IsPostBack)
			{

				int userId = GetUserId ();

				BindCommunities (1, userId);
			}
			
			HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
			HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.CHANNELS;
			HeaderNav.SetNavVisible(HeaderNav.MyKanevaNav,2);
		}

		/// <summary>
		/// Bind the data
		/// </summary>
		/// <param name="pageNumber"></param>
		private void BindCommunities (int pageNumber, int userId)
		{
			// Set the sortable columns
			string orderby = "account_type_id, " + CurrentSort + " " + CurrentSortOrder;

			PagedDataTable pds = UsersUtility.GetUserCommunities (userId, orderby, "community_type_id=2", pageNumber, KanevaGlobals.CommunitiesPerPage);

			rptChannels.DataSource = pds;
			rptChannels.DataBind ();
			

			pgTop.NumberOfPages = Math.Ceiling ((double) pds.TotalCount / KanevaGlobals.CommunitiesPerPage).ToString ();
			pgTop.DrawControl ();

			// The results
			lblSearch.Text = GetResultsText (pds.TotalCount, pageNumber, KanevaGlobals.CommunitiesPerPage, pds.Rows.Count);

			if (pds.TotalCount < 1)
			{
				//TODO show msg in the page
				//rnNavigation.NoDataMsg = "You belong to 0 communities.";
				//rnNavigation.NoDataMsg_Visible = true;
			}
		}

		/// <summary>
		/// Get the community status
		/// </summary>
		/// <param name="isPublic"></param>
		/// <returns></returns>
		protected string GetStatus (object isPublic)
		{
			if (isPublic == null)
			{
				return "";
			}
			return CommunityUtility.GetStatus (isPublic.ToString ());
		}

		/// <summary>
		/// Page Change
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pgTop_PageChange (object sender, PageChangeEventArgs e)
		{
			BindCommunities (e.PageNumber, GetUserId ());
		}

		/// <summary>
		/// They clicked to sort a column
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected override void btnSort_Click (object sender, System.EventArgs e)
		{
			base.btnSort_Click (sender, e);
			BindCommunities (pgTop.CurrentPageNumber, GetUserId ());
		}

		/// <summary>
		/// DEFAULT_SORT
		/// </summary>
		/// <returns></returns>
		protected override string DEFAULT_SORT
		{
			get
			{
				return "name";
			}
		}

		/// <summary>
		/// rptAssets_ItemCommand
		/// </summary>
		private void rptChannels_ItemCommand (object source, RepeaterCommandEventArgs e)
		{
			string command = e.CommandName;
			int userId = GetUserId();
			int index = e.Item.ItemIndex;

			HtmlInputHidden hidChannelId = (HtmlInputHidden) rptChannels.Items [index].FindControl ("hidChannelId");
			int communityId = Convert.ToInt32 (hidChannelId.Value);

			switch (command)
			{
				case "cmdView":
				{
					DataRow drChannel = CommunityUtility.GetCommunity (communityId);
					Response.Redirect (GetBroadcastChannelUrl (drChannel ["name_no_spaces"].ToString ()));
					break;
				}
            case "cmdEdit":
                {
                    Response.Redirect("~/community/CommunityEdit.aspx?communityId=" + communityId.ToString());
                    break;
                }
				case "cmdDeleteChannel":
				{
                    if (IsAdministrator() || communityFacade.IsCommunityOwner(communityId, userId))
                    {
                        // Don't chagne this to Facade until everything is implemented there!!!
                        // Facade did not implement deleteing members, 
                        //      removing assets and adjusting counts, 
                        //      set last update so it would be removed from search, 
                        //      call correct SP so wok items get taken care of.
                        CommunityUtility.DeleteCommunity(communityId, userId);
                    }

					break;
				}
				case "cmdQuitChannel":
				{
                    ////copied from commJoin.aspx
                    //if (CommunityUtility.IsPayCommunity (communityId))
                    //{
                    //    // Get the current active subscription
                    //    DataRow drCommunityMemberSubscription = CommunityUtility.GetActiveCommunityMemberSubscription (communityId, userId);
                    //    if (drCommunityMemberSubscription != null)
                    //    {
                    //        StoreUtility.UpdateCommunityMemberSubscription (userId, Convert.ToInt32 (drCommunityMemberSubscription ["community_member_subscription_id"]), 0, (int) Constants.eCOMMUNITY_MEMBER_SUBSCRIPTION_STATUS.QUIT, false);
                    //    }
                    //}

					// They are trying to quit
					GetCommunityFacade.UpdateCommunityMember (communityId, userId, (UInt32) CommunityMember.CommunityMemberStatus.DELETED);
					break;																			
				}
				case "cmdSaveNotifyOption":
				{
					RadioButtonList rblNotify = (RadioButtonList) rptChannels.Items[index].FindControl ("rblNotify");
					int notify_type = 0;

					switch ( Convert.ToInt32(rblNotify.SelectedValue) )
					{
						case (int) Constants.eCOMMUNITY_NOTIFICATIONS.INDIVIDUAL_EMAIL:
							notify_type = (int) Constants.eCOMMUNITY_NOTIFICATIONS.INDIVIDUAL_EMAIL;
							break;
					
						case (int) Constants.eCOMMUNITY_NOTIFICATIONS.DAILY_EMAILS:
							notify_type = (int) Constants.eCOMMUNITY_NOTIFICATIONS.DAILY_EMAILS;
							break;
					
						case (int) Constants.eCOMMUNITY_NOTIFICATIONS.WEEKLY_EMAILS:
							notify_type = (int) Constants.eCOMMUNITY_NOTIFICATIONS.WEEKLY_EMAILS;
							break;
					
						default:
							notify_type = (int) Constants.eCOMMUNITY_NOTIFICATIONS.OFF;
							break;
					}

					CommunityUtility.UpdateCommunityNotifications(communityId, userId, notify_type);
					break;
				}
			}

			BindCommunities (pgTop.CurrentPageNumber, userId);
		}
		/// <summary>
		/// Change category of asset
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void rptChannels_ItemDataBound (object sender, RepeaterItemEventArgs e)
		{
			if(e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				Label lblMemberType = (Label) e.Item.FindControl("lblMemberType");
				
				ImageButton btnView = (ImageButton) e.Item.FindControl("btnView");
				ImageButton btnViewEdit = (ImageButton) e.Item.FindControl("btnViewEdit");
				ImageButton btnDelete = (ImageButton)  e.Item.FindControl("btnDelete");
				ImageButton btnQuit = (ImageButton)  e.Item.FindControl("btnQuit");
				
				HtmlContainerControl divNotifySummary = (HtmlContainerControl) e.Item.FindControl("divNotifySummary");
				HtmlContainerControl divNotification  = (HtmlContainerControl) e.Item.FindControl("divNotification");
				HtmlContainerControl divNotifyOptions = (HtmlContainerControl) e.Item.FindControl("divNotifyOptions");
				HtmlContainerControl spnNotify = (HtmlContainerControl) e.Item.FindControl("spnNotify");

				switch ( Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "account_type_id")) )
				{
					case (int) CommunityMember.CommunityMemberAccountType.OWNER:
					{
						lblMemberType.Text = "Owner";
						
						RadioButtonList rblNotify = (RadioButtonList) e.Item.FindControl("rblNotify");
						if (rblNotify != null)
						{
							rblNotify.SelectedValue = DataBinder.Eval(e.Item.DataItem, "notifications").ToString();
							rblNotify.Visible = true;

							divNotifySummary.InnerHtml = GetNotificationSummary(Convert.ToInt32(rblNotify.SelectedValue));
							divNotifySummary.Visible = true;
						}
						
						btnView.Visible = true;
						btnViewEdit.Visible = true;
						btnDelete.Visible = true;
						btnQuit.Visible = false;
						
						//changed to allow owners to change notification pref
						divNotification.Visible = true;
						spnNotify.Attributes.Add("onclick", "expandcontent(this, '" + divNotifyOptions.ClientID + "')") ;

						btnDelete.Attributes.Add("onclick", GetDeleteScript());
						break;
					}
					case (int) CommunityMember.CommunityMemberAccountType.MODERATOR:
					{
						lblMemberType.Text = "Moderator";
						
						RadioButtonList rblNotify = (RadioButtonList) e.Item.FindControl("rblNotify");
						if (rblNotify != null)
						{
							rblNotify.SelectedValue = DataBinder.Eval(e.Item.DataItem, "notifications").ToString();
							rblNotify.Visible = true;

							divNotifySummary.InnerHtml = GetNotificationSummary(Convert.ToInt32(rblNotify.SelectedValue));
							divNotifySummary.Visible = true;
						}
						
						btnView.Visible = true;
						btnViewEdit.Visible = true;
						btnDelete.Visible = false;
						btnQuit.Visible = true;

						//changed to allow owners to change notification pref
						divNotification.Visible = true;
						spnNotify.Attributes.Add("onclick", "expandcontent(this, '" + divNotifyOptions.ClientID + "')") ;


						btnQuit.Attributes.Add("onclick", GetQuitScript());
						break;
					}
					case (int) CommunityMember.CommunityMemberAccountType.SUBSCRIBER:
					{
						lblMemberType.Text = "Belong to";
						
						RadioButtonList rblNotify = (RadioButtonList) e.Item.FindControl("rblNotify");
						if (rblNotify != null)
						{
							rblNotify.SelectedValue = DataBinder.Eval(e.Item.DataItem, "notifications").ToString();
							rblNotify.Visible = true;

							divNotifySummary.InnerHtml = GetNotificationSummary(Convert.ToInt32(rblNotify.SelectedValue));
							divNotifySummary.Visible = true;
						}
						
						btnView.Visible = true;
						btnViewEdit.Visible = false;
						btnDelete.Visible = false;
						btnQuit.Visible = true;
						
						spnNotify.Attributes.Add("onclick", "expandcontent(this, '" + divNotifyOptions.ClientID + "')") ;
						btnQuit.Attributes.Add("onclick", GetQuitScript());
						break;
					}
				}
			}
		}

		private string GetNotificationSummary(int val)
		{
			string notify = string.Empty;

			switch (val)
			{
				case (int) Constants.eCOMMUNITY_NOTIFICATIONS.DAILY_EMAILS:
				{
					notify = "Daily";
					break;
				}
				case (int) Constants.eCOMMUNITY_NOTIFICATIONS.WEEKLY_EMAILS:
				{
					notify = "Weekly";
					break;
				}
				case (int) Constants.eCOMMUNITY_NOTIFICATIONS.INDIVIDUAL_EMAIL:
				{
					notify = "As they happen";
					break;
				}
				default:
				{
					notify = "Never";
					break;
				}
			}

			return "I want World updates: <strong>" + notify + "</strong>";
		}
		private string GetDeleteScript()
		{
			StringBuilder sbDelete = new StringBuilder() ;
			sbDelete.Append(" javascript: ");
			sbDelete.Append(@" if(!confirm('Are you sure you want to delete this World? \r");
			sbDelete.Append(@"This action will delete all forums, blogs, and everything \r");
			sbDelete.Append("associated with this World.')) return false;");
			return sbDelete.ToString();
		}
		private string GetQuitScript()
		{
			StringBuilder sbQuit = new StringBuilder() ;
			sbQuit.Append(" javascript: ");
			sbQuit.Append(" if(!confirm('Are you sure you want to quit selected Worlds?')) return false;");
			return sbQuit.ToString();
		}

		#region Web Form Designer generated code

		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
			pgTop.PageChanged +=new PageChangeEventHandler (pgTop_PageChange);
			this.rptChannels.ItemDataBound +=new RepeaterItemEventHandler (rptChannels_ItemDataBound);
			rptChannels.ItemCommand += new RepeaterCommandEventHandler (rptChannels_ItemCommand);
		}
		#endregion

		
	}
}
