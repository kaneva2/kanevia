﻿///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KlausEnt.KEP.Kaneva
{
    public partial class block : NoBorderPage
    {
        protected void Page_Load (object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request["blockUserId"] != null)
                {
                    _BlockUserId = Convert.ToInt32 (Request["blockUserId"]);
                }

                if (Request["blockUsername"] != null)
                {
                    _BlockUsername = Request["blockUsername"].ToString ();
                }

                litUsername.Text = _BlockUsername;
            }
        }

        protected void btnOK_Click (object sender, EventArgs e)
        {
            // Can't block self
            if (KanevaWebGlobals.CurrentUser.UserId != _BlockUserId)
            {
                int ret = 0;
                string strJS = "";

                if (chkBlock.Checked || chkReport.Checked)
                {
                    // Block this user
                    if (chkBlock.Checked)
                    {
                        if (_BlockUserId > 0 && KanevaWebGlobals.CurrentUser.UserId != _BlockUserId)
                        {
                            ret = GetUserFacade.BlockUser (KanevaWebGlobals.CurrentUser.UserId, _BlockUserId, (int) Constants.eUSER_BLOCK_SOURCE.WEB);
                            if (ret > 0)
                            {
                                strJS = "javascript:parent.setBlockText();";
                            }
                        }
                    }
                    
                    if (chkReport.Checked)
                    {
                        strJS = "javascript:parent.location='http://" + KanevaGlobals.SiteName + "//suggestions.aspx?mode=WB&category=KANEVA%20Web%20Site';";
                    }

             //       MagicAjax.AjaxCallHelper.Write ("javascript:parent.$j.facebox.close();" + strJS);
                    ScriptManager.RegisterClientScriptBlock (upButtons, upButtons.GetType (), "callback", strJS, true);
                }
            }
        }

        private int _BlockUserId
        {
            get
            {
                if (ViewState["blockUserId"] != null)
                {
                    return Convert.ToInt32 (ViewState["blockUserId"]);
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                ViewState["blockUserId"] = value;
            }
        }
        private string _BlockUsername
        {
            get
            {
                if (ViewState["blockUsername"] != null)
                {
                    return ViewState["blockUsername"].ToString();
                }
                else
                {
                    return GetUserFacade.GetUserName(_BlockUserId);
                }
            }
            set
            {
                ViewState["blockUsername"] = value;
            }
        }

    }
}