<%@ Page language="c#" Codebehind="userPicker.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.userPicker" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="usercontrols/Pager.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="MembersFilter" Src="usercontrols/MembersFilter.ascx" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head>
    <title>KANEVA User Selector</title>
    <link rel="stylesheet" href="css/Kaneva.css" type="text/css" />
    <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
  </head>
  <body MS_POSITIONING="GridLayout" topmargin="0" leftmargin="0">
	
    <form id="frmMain" method="post" runat="server">
<center>
	<table cellpadding="0" cellspacing="0" border="0" width="560" class="FullBorders">
		<tr>
			<td valign="middle" class="belowFilter" colspan="2" width="268" height="23" align="left">&nbsp;</td><td colspan="5" width="292" height="23" align="right" class="belowFilter2" valign="middle"><br><br><asp:Label runat="server" id="lblSearch" CssClass="assetCommunity"/><br><br><Kaneva:Pager runat="server" id="pgTop"/><br><br></td>						
		</tr>
		<tr>
			<td colspan="9">
				<Kaneva:MembersFilter runat="server" id="filMembers" RolesEnabled="false" StatusEnabled="false" InitialItemsPerPage="30"/>
				<asp:DataGrid EnableViewState="False" runat="server" ShowFooter="False" Width="100%" id="dgrdAsset" cellpadding="0" cellspacing="0" border="0" AutoGenerateColumns="False" AllowSorting="True">  
					<HeaderStyle CssClass="lineItemColHead"/>
					<ItemStyle CssClass="lineItemEven"/>
					<AlternatingItemStyle CssClass="lineItemOdd"/>
					<Columns>
						
						<asp:TemplateColumn HeaderText="username" SortExpression="username" ItemStyle-Width="42%" >
							<ItemTemplate>
								<%# TruncateWithEllipsis (DataBinder.Eval (Container.DataItem, "username").ToString (), 50) %>
							</ItemTemplate>
						</asp:TemplateColumn>
						
						<asp:TemplateColumn HeaderText="actions" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center" >
							<ItemTemplate>
								<asp:hyperlink id="hlBuy" runat="server" ImageURL="~/images/button_select.gif" NavigateURL='<%# GetSelectJavascript (Convert.ToString (DataBinder.Eval (Container.DataItem, "username"))) %>' ToolTip="Accept"/>
							</ItemTemplate>
						</asp:TemplateColumn>
		
					</Columns>
				</asp:datagrid>
			</td>
		</tr>
		<tr>
			<td><img runat="server" src="~/images/spacer.gif" width="3"></td>
		</tr>

	</table>

</center>
<input type="hidden" runat="server" id="hidSortColumn" value="0"> 
<asp:button ID="btnSort" OnClick="btnSort_Click" runat="server" style="DISPLAY:none"></asp:button>

    </form>
	
  </body>
</html>
