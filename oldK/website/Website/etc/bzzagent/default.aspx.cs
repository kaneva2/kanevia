///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using System.Web.Security;
using System.Security.Principal;
using log4net;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for bzzAgentLanding.
	/// </summary>
    public class bzzAgentLanding : MainTemplatePage
	{
		protected bzzAgentLanding () 
		{
		}

		private void Page_Load (object sender, System.EventArgs e)
		{

			//set the meta tags
			Title = "Kaneva BzzAgents.";
			MetaDataDescription = "<meta name=\"description\" content=\"Already a member? Log in to Kaneva. Where your Profile, Friends, Media and favorite Communities are teleported into a modern-day 3D world where you can explore, socialize and experience entertainment in an entirely new way.\"/>";


            //aRegister.HRef = KanevaWebGlobals.GetJoinLink(Page);
            aRegister.HRef = ResolveUrl(KanevaGlobals.JoinLocation) + "?joinId=" + System.Configuration.ConfigurationManager.AppSettings["BzzAgent_Community_ID"];

			if (!IsPostBack)
			{

				if (Request.UrlReferrer != null)
				{
					ViewState ["rurl"] = Request.UrlReferrer.AbsoluteUri.ToString ();
				}

			}

			
		}



		// Logger
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

        protected HtmlAnchor aRegister;

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
