﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using log4net;
using Kaneva.BusinessLayer.Facade;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace KlausEnt.KEP.Kaneva
{
    public partial class ParaturePassThrough : System.Web.UI.Page
    {
        /// <summary>
        /// parameter name to denote the ping fed instance id configured for your instance.
        /// </summary>
        private const string instanceIdParamName = "ping.instanceId";

        /// <summary>
        /// instance ID that is configured to accept data from you.
        /// </summary>
        private const string securePassThroughPingFedInstanceID = "kanevaSecPass";

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected void Page_Load(object sender, EventArgs e)
        {

            //NOTE if the authorization fails using my self signed certificate. you might need to add it to your local certificate store.

            //valid API related CSR user account to your department.
            //string sessEmail = "systemuser@kaneva.com"; // 1mzrfj5mcglw2un4
            string SessEmail = "systemuser@kaneva.com";

            // Send required user informations. you could pass custom information about the user like phone number in the json object inline with your previous passthrough implementation.
            JToken refId = GetSecurePassThroughRefID(SessEmail, KanevaWebGlobals.CurrentUser.FirstName, KanevaWebGlobals.CurrentUser.LastName, "password", KanevaWebGlobals.CurrentUser.Email, KanevaWebGlobals.CurrentUser.UserId.ToString (), KanevaWebGlobals.CurrentUser.Username, 5433);

            //Secure PassThrough URL
            var portalURL = Configuration.ParaturePassThroughPortalURL; // "http://s3-sandbox.parature.com/ics/support/security2.asp";
            
            //this page adds details to its DOM to do a client side POST to security2.asp, which would get the user authenticated to the support portal based on the refID and instance ID.
            //refID is short lived. so you cannot cache it.

            // Trigger a client form POST with refID and instanceID, to the security2.asp
            this.Form.Action = portalURL;
            var sb = new StringBuilder();
            sb.Append(string.Format(@"<input type=""hidden"" name=""refID"" value=""{0}"" />", refId));
            sb.Append(string.Format(@"<input type=""hidden"" name=""instanceID"" value=""{0}"" />", securePassThroughPingFedInstanceID));
            sb.Append("<script type='text/javascript'>document.forms[0].submit();</script>");
            securePassThroughDetails.InnerHtml = sb.ToString();

        }

        /// <summary>
        /// method which communicates to the sso-mutual-auth.parature.com
        /// </summary>
        /// <param name="sessEmail">system user id</param>
        /// <param name="firstName">first name</param>
        /// <param name="lastName">last name</param>
        /// <param name="password">user password, send empty if you plan not to update</param>
        /// <param name="emailID">users email id</param>
        /// <param name="userID">userid</param>
        /// <param name="accountName">account user is associated with</param>
        /// <param name="departmentID">your parature instance department id</param>
        /// <returns></returns>
        public string GetSecurePassThroughRefID(string sessEmail, string firstName, string lastName, string password, string emailID, string userID, string accountName, int departmentID)
        {
            // Send  user data to the Parature pass through DropOff Url where you will drop off information about the user to be authenticated.
            var dropOffURL = Configuration.ParatureDropOffURL;

            var token = string.Empty;
            var collection = new X509Certificate2Collection();

            try
            {
                // Add our Cert
                collection.Import(Server.MapPath("support_kaneva_com.cer"));
            }
            catch (Exception e1)
            {
                m_logger.Error ("Error reading CERT", e1);
                securePassThroughDetails.InnerHtml = "Fatal Error Reading Cert";
                Response.End();
            }

            try
            {
                var requestToPing = (HttpWebRequest)WebRequest.Create(dropOffURL);
                requestToPing.Method = "POST";
                requestToPing.PreAuthenticate = true;

                //requestToPing.ClientCertificates.Add(collection[0]);
                foreach (var cert in collection)
                {
                    requestToPing.ClientCertificates.Add(cert);
                }

                requestToPing.Headers.Add(instanceIdParamName, securePassThroughPingFedInstanceID);

                //Construct the JSON payload with required information
                var payload = new StringBuilder();

                payload.Append("{");

                payload.Append(string.Format("\"subject\": \"{0}\",", securePassThroughPingFedInstanceID));
                payload.Append("\"payload\":");

                payload.Append("{");
                payload.Append("\"sessEmail\": \"" + sessEmail + "\",");
                payload.Append("\"cFname\": \"" + firstName + "\",");
                payload.Append("\"cLname\": \"" + lastName + "\",");
                payload.Append("\"cEmail\": \"" + emailID + "\",");
                //payload.Append("\"cPassword\": \"" + password + "\",");
                payload.Append("\"cStatus\": \"REGISTERED\",");
                payload.Append("\"cUname\" : \"" + userID + "\",");
                payload.Append("\"cTou\": \"1\","); // 1 is accepting terms and conditions
                payload.Append("\"amName\": \"" + accountName + "\",");

                //“cSlaName” : “<md5 hash>”
                // This appears to work properly. The JSON I’m using (note that the SlaName is an md5 hash of “Member”):
                payload.Append("\"cSlaName\": \"858ba4765e53c712ef672a9570474b1d\",");

                ////string sla = "\"Sla\": {\"ID\":\"4857\"},";
                ////payload.Append(sla);

             //   payload.Append("\"Sla\":{\"id\"=\"4857\"},");

                payload.Append("\"deptID\": \"" + departmentID.ToString() + "\"");
                // payload.Append("\"customKeyValuesAllowed\" : \"CustomFieldValue\""); // Note: you can pass custom values to update Customer fields after configuring them in the back end. you can ignore if you dont need these custom values. 
                payload.Append("}");

                payload.Append("}");

                // TLS Only
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls;

                using (var sr = new StreamWriter(requestToPing.GetRequestStream()))
                {
                    sr.Write(payload.ToString());
                }

                // read response from Parature and grab the REF parameter
                var responseFromPing = requestToPing.GetResponse();
                JToken refId = "0";
                if (responseFromPing != null)
                {
                    using (var reader = new StreamReader(responseFromPing.GetResponseStream()))
                    {
                        JObject.Parse(reader.ReadToEnd()).TryGetValue("REF", out refId);
                    }
                }

                if (refId != null)
                {
                    token = refId.ToString();
                }
                return token;
            }
            catch (Exception e2)
            {
                m_logger.Error("Error Authenticating to Parature", e2);
                securePassThroughDetails.InnerHtml = "Error Auth to Parature";
                Response.End();
            }

            return token;
        }
    }
}