///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Kaneva.flash.omm
{
	/// <summary>
	/// Summary description for FullScreen.
	/// </summary>
	public class FullScreen : System.Web.UI.Page
	{
		private void Page_Load(object sender, System.EventArgs e)
		{
			if (!IsPostBack)
			{
				Literal litStream = new	 Literal ();
				string equalSign = "%3d";
				string questionMark = "%3f";
				string amp = "%26";

				string mediaURL = "/flash/omm/OMM.swf" +
					"?startMe=" + "/services/omm/request.aspx" + questionMark + Constants.QS_TYPE + equalSign;

				mediaURL += Constants.GET_FULL_METADATA + amp + Constants.QS_ASSETID + equalSign + Convert.ToInt32 (Request ["assetId"]);;

				litStream.Text = playerScript_start + mediaURL + playerScript_end;
				phStream.Controls.Add (litStream);	
			}
		}


		string playerScript_start = " <div id=\"flashcontent\"> This text is replaced by the Flash movie. </div> " + 
			"<script type=\"text/javascript\">var so = new SWFObject(";

		string playerScript_end = ", \"OMM\", \"100%\", \"100%\", \"8\", \"#999999\");" + 
			"so.addVariable(\"fullScreen\", getQueryParamValue(\"fullScreen\"));" +
			"so.addVariable(\"autoStart\", getQueryParamValue(\"autoStart\"));" +
			"so.addVariable(\"detailMode\", getQueryParamValue(\"detailMode\"));" +
			"so.write(\"flashcontent\");</script>";

		protected PlaceHolder	phStream;

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
