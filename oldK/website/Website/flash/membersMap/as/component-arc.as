﻿#initclip 1

_ARROW_LEVEL = 50000;
_MASK_OFFSET = 500;

function FArc()
{
	this.xst;
	this.yst;
	this.xen;
	this.yen;
	this.total = _root._DEGREE_FRAMES;
	this.step;
	this.level;
}

FArc.prototype = new MovieClip();

Object.registerClass("FArc", FArc);

FArc.prototype.drawArc = function(from, to, step) {

	this.xst = from.x;
	this.yst = from.y;
	this.xen = to.x;
	this.yen = to.y;
	this.level = step;

	this.dist = this.lineDist(this.xst, this.yst, this.xen, this.yen);
	
	var mp = this.getMidpoint(this.xst, this.yst, this.xen, this.yen);
	var thet = this.getSla(this.xst, this.yst, this.xen, this.yen);
	var fin = this.getFinalPoint(mp.x, mp.y, thet, this.dist/2);	
	
	this.square = this.createEmptyMovieClip('square' + this.level,_ARROW_LEVEL + _MASK_OFFSET + this.level);
	
	if (this.dist < 10) {
		this.drawCircle(10,this.xst, this.yst);		
	} else {
		thist.dist += 10 + random(10)
		this.drawLine(this.xst, this.yst, this.xen, this.yen, fin.x, fin.y);
		this.mp = mp;
		this.thet = thet;
		
		this.callLoop();
	}
	
}



FArc.prototype.drawCircle = function(r, x, y) {	
	this.c = this.createEmptyMovieClip("c" + this.level, _ARROW_LEVEL + this.level);
	
		
	a = Math.tan(22.5 * Math.PI/180);
	a = 1;
	var angleSt = (this.level + 3) * 45;
	
	with (this.c) {
		moveTo(x, y);
	}

	this.circle_center = this.getFinalPoint(x, y, angleSt * Math.PI / 180, r);
	this.circle_angle_off = angleSt;
	this.circle_angle = 0;
	this.circle_radius = r;
	this.circle_x = this.xst;
	this.circle_y = this.yst;
	
	this.onEnterFrame = function() {
    	if (this.circle_angle <= 360) {
			with (this.c){
				lineStyle(0,0xCC0099,50);
				var nextPt = this.getFinalPoint(this.circle_center.x,
												this.circle_center.y,
												(this.circle_angle + this.circle_angle_off - 180) * Math.PI / 180, 
												this.circle_radius);

				curveTo(this.circle_x, this.circle_y, nextPt.x, nextPt.y);
				this.circle_x = nextPt.x;
				this.circle_y = nextPt.y;
				this.circle_angle += 360 / this.total;
			}

		} else {
			delete this.onEnterFrame;
		}
	}


}



FArc.prototype.callLoop = function() {

	
	this.maskStep  = 0;
	this.onEnterframe = function() {
		this.maskStep++;
		if (this.maskStep <= this.total) {
			this.drawMask();
		} else {
			delete this.onEnterFrame;
			this.maskStep = 0;
		}
	}
}

FArc.prototype.drawMask = function() {	
	
	var top = this.getFinalPoint(this.xst, this.yst, this.thet, this.dist)
	
	var swing = this.getMaskPoint(this.mp.x, this.mp.y, this.thet, this.dist / 2
								  , this.maskStep
								  , this.total);
	
	with(this.square) {
		clear();
		moveTo(this.mp.x,this.mp.y);
		lineStyle(0,0x000066,100);
		beginFill(0x000088)
		lineTo(this.xst, this.yst);
		lineTo(top.x, top.y);
		lineTo(swing.x, swing.y);					
		endFill();
	}				
//	this.line.setMask (this.square)
}


FArc.prototype.getMaskPoint = function(x, y, th, radius, step, total)
{
	var xf;
	var yf;
	if (this.xst > this.xen ) {
		var angle;
		if(this.yst > this.yen) {
			var angle = Math.PI / 2 + th;		
			angle = angle + Math.PI / 2; 
			angle = angle - (step * Math.PI / total);						
		}
		else {
			var angle = th;
			angle = angle - Math.PI / 1; 
			angle = angle - (step * Math.PI / total);			
		}
		xf = x + (-1* radius * Math.cos(angle));
		yf = y + (-1* radius * Math.sin(angle));
	} else {
		var angle = Math.PI / 2 + th;
		angle = angle - Math.PI / 2; 
		angle = angle + (step * Math.PI / total);
		xf = x + (-1 * radius * Math.cos(angle));	
		yf = y + (-1 * radius * Math.sin(angle));		
	}
	
	var fp = {
		x: xf,
		y: yf
		}
		
	return fp;


	
}

FArc.prototype.getFinalPoint = function(x, y, th, radius) {
	var xf = x + (-1 * radius * Math.cos(Math.PI / 2 + th));	
	var yf = y + (-1 * radius * Math.sin(Math.PI / 2 + th));		
	
	var fp = {
		x: xf,
		y: yf
		}
		
	return fp;
}

FArc.prototype.drawLine = function(xs, ys, xe, ye, cx, cy)
{
	
	this.line = this.createEmptyMovieClip( "line" + this.level, _ARROW_LEVEL + this.level);
	
	this.line._alpha = 40;
	
	this.line.setMask(this.square)
	
	with ( this.line) {		
	  lineStyle(2,0xCC0099,80);
	  moveTo(xs,ys);
	  curveTo(cx, cy, xe,ye);	  
	}
	
}

FArc.prototype.getMidpoint = function(xs, ys, xe, ye)
{	
	var xm = (xs + xe)/2;
	var ym = (ys + ye)/2;

	var po = {
		x: xm,
		y: ym
	};
	
	return po;
			
}

FArc.prototype.lineDist = function(xs, ys, xe, ye)
{
	var finalx = ye - ys;
	var finaly = xe - xs;
	
	var final = Math.sqrt((Math.pow(finaly, 2)) + (Math.pow(finalx, 2)));
	return final; 
	
}

FArc.prototype.getSla = function(xs, ys, xe, ye)
{
	var xdiff = xe - xs;
	var ydiff = ye - ys;
	
	var slope = ydiff/xdiff;
	
	var theta = Math.atan(slope);
	
	return theta;
}


#endinitclip 
