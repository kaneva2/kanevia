﻿#initclip 1


_BUTTON_LEVEL = 30000;

function FButtons() {
	this.buttons = new Array();
	this.degreeArray = new Array("th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th", "th");
	//variables that check the state of the left and right buttons on the main timeline within the
	//"buttons" movie clip
	

}

FButtons.prototype = new MovieClip();

Object.registerClass("FButtons", FButtons);

FButtons.prototype.setDegrees = function(nDegrees, startName) {	
	
	this.window.targ = this.window._x;
	mainMovieTimeLine.num_ButtonPos = this.window.targ;
	//trace("mainMovieTimeLine.num_ButtonPos = " + mainMovieTimeLine.num_ButtonPos);
	var num = nDegrees;
	var h = 0;
	var off = -50;
	this.attached = num;
	//trace("num = " + num);
	
	for (i = 0; i <= num; i++) {
		var b = this.window.attachMovie("adv", "box" + i, i + _BUTTON_LEVEL);
		//trace("box'i' = box" + i);
		b._x = i * b._width;
		//trace(b._width + " " + b._x);
		b._y = this.window._y;
		b._alpha = 60;
		b.n = i;

		if(i == 0) {
			b.degree = startName; //startName or 'Start';
		}
		else {
			var suff;
			if(i >= 10 && i <= 20) {
				suff = "th";
			}
			else {
				suff = this.degreeArray[(i % 10)];
			}
	
			b.degree = i + suff;
		}
		
//		if (i % 2) {
//			my_color = new Color(b.boxBG);
//			my_color.setRGB(0xF0F0F0);			
//		}
	}
	//trace(this.window._length);
}

FButtons.prototype.moveAlong = function() {         
    //trace(mainMovieTimeLine.bool_isHitLeft);
	//mainMovieTimeLine.bool_isHitLeft = true;
	//trace(mainMovieTimeLine.bool_isHitLeft);
	var t = this.window;
	//trace("t.targ0 = " + t.targ);
	if ((!mainMovieTimeLine.bool_isHitLeft && !mainMovieTimeLine.bool_buttonWasHitBefore) || (!mainMovieTimeLine.bool_isHitRight && !mainMovieTimeLine.bool_buttonWasHitBefore)){
		if(mainMovieTimeLine.num_currentDegree != mainMovieTimeLine.degreeArray.length-1){
			t.targ -= 50;
			mainMovieTimeLine.num_PreviousDegree = mainMovieTimeLine.num_CurrentDegree;
			mainMovieTimeLine.num_CurrentDegree += 1;
		}
		
		//trace("Value of button mainMovieTimeLine.num_CurrentDegree = " + mainMovieTimeLine.num_CurrentDegree);
		//trace("NOT LEFT or RIGHT and No Button = " + t.targ);
		
	}
	else if ((mainMovieTimeLine.bool_isHitLeft && !mainMovieTimeLine.bool_buttonWasHitBefore) || (mainMovieTimeLine.bool_isHitRight && !mainMovieTimeLine.bool_buttonWasHitBefore)) {
		//trace("Left or Right and no Button hit before = " + t.targ);
		mainMovieTimeLine.bool_buttonWasHitBefore = true;
	}
	else{
		//t.targ -= 50;
	}
	/*
	else if (mainMovieTimeLine.bool_isHitRight) && !mainMovieTimeLine.bool_buttonWasHitBefore) {
		if(!mainMoveTimeLine.bool_buttonWasHitBefore){
			mainMoveTimeLine.bool_buttonWasHitBefore = true;
		}
		t.targ -= 50;
			trace("t.targ3 = " + t.targ);
		//mainMovieTimeLine.bool_buttonWasHitBefore = true;		
	}
	else{
		//t.targ = t.targ;
		trace("t.targ4 = " + t.targ);
	}
	trace("t.targ5 = " + t.targ);
	*/
	t.onEnterFrame = function() {
		if(mainMovieTimeLine.bool_buttonWasHitBefore){
			if(mainMovieTimeLine.bool_isHitRight){
				//this.targ -= 75;
				//this._x += (this.targ - this._x)/7;
				this._x = mainMovieTimeLine.num_ButtonPos - mainMovieTimeLine.num_CurrentDegree * 50;
				//trace("mainMovieTimeLine.num_CurrentDegree = " + mainMovieTimeLine.num_CurrentDegree);
				//this.targ += 100;
				fl_coords = this._x;

				mainMovieTimeLine.bool_isHitRight = false;
			}
			else if(mainMovieTimeLine.bool_isHitLeft){
				//this.targ += 75;
				//this._x += (this.targ - this._x)/7;
				this._x = mainMovieTimeLine.num_ButtonPos - mainMovieTimeLine.num_CurrentDegree * 50;
				//trace("mainMovieTimeLine.num_CurrentDegree = " + mainMovieTimeLine.num_CurrentDegree);
				//trace("mainMovieTimeLine.num_ButtonPos = "  + mainMovieTimeLine.num_ButtonPos + "mainMovieTimeLine.num_CurrentDegree = " + mainMovieTimeLine.num_CurrentDegree);
				
				//this.targ -= 100;
				fl_coords = this._x;
				mainMovieTimeLine.bool_isHitLeft = false;
			}
			else{
				//do nothing
			}
		}
		else{
			this._x += (this.targ - this._x)/7;
			fl_coords = this._x;
			
		}
		//trace(fl_coords);
	}
		
		//if(!mainMovieTimeLine.bool_buttonWasHitBefore){
			//this._x += (this.targ - this._x)/7;
			//trace(this._x);
		//}
}//end of moveAlong
				


FButtons.prototype.resetMe = function() {
	trace("reset called");
	this.blank["box" + 1].removeMovieClip();
	
	this.blank.targ = 0;
	this.off = 0;
	i = 0;
	h = 0;
}

FButtons.prototype.resetPos = function() {
	for (h = 0; h <= this.attached; h++) {		
		this.blank["box" + h]._x = this.blank["box" + h]._x - 50;
	}
}

 
#endinitclip