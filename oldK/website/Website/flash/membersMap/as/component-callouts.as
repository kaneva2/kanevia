﻿#initclip 1

_CALL_LEVEL = 6000;
_CALL_LINE_LEVEL = 5000;


var zipCoords = _root.zipCoords;

function FCallouts() {	
	this.nCalls = 0;
	this.interval = 0;
	this.currNode = 0;
}

FCallouts.prototype = new MovieClip();

Object.registerClass("FCallouts", FCallouts);



FCallouts.prototype.drawCall = function(node, delay) {
	this.currNode = node;
	var inttime =  0;
	if(delay) {
		inttime = 5;//(intervalTime() - 3*intervalDelay()/4);
	}
	this.interval = setInterval(FCalloutsIntervalBounce, 
							inttime,
							this);

}

function FCalloutsIntervalBounce(callouts) {
	callouts.drawCurr();
}

FCallouts.prototype.clearInterval = function() {
	if(this.interval) {
		clearInterval(this.interval);
		this.interval = 0;

	}
}


FCallouts.prototype.drawCurr = function() {
	this.clearInterval();
	var node = this.currNode;
	var ca = this.attachMovie("callout", "call" + _CALL_LEVEL + this.nCalls, _CALL_LEVEL + this.nCalls);
	ca.pointer = this;
	
	if (node.x < 30) {
		ca._x = Math.floor(node.x / 2);
	}	
	else if (node.x > 515) {
		ca._x = node.x - 5;
	} else {
		ca._x = node.x - 30;
		var lineOrigx = node.x;
		var frX = (node.x - 30);
	}
	
	ca.call_tex.text = node.nom + (node.via != 0 ? " (" + node.via + ")" : "");
	ca.call_tex.border = false;
	ca.back._width = 2 * ca.call_tex.textWidth * 1.15 + 5;	
	ca.call_tex._width = ca.back._width;
	ca._y = node.y - 20;
	ca._alpha = 80;
	
	ca.step = this.nCalls;
	
	var frY = ca._y;
	var frX = ca._x;
	ca.lineOrigx = node.x;
	ca.lineOrigy = node.y;
	
	var li = this.attachMovie("call_line", 
							  "caLine" + _CALL_LINE_LEVEL + this.nCalls, 
							  _CALL_LINE_LEVEL + this.nCalls);
	
	ca.line = li;

	li._x =  frX;
	li._y = frY;
			
	li._xscale = ca.lineOrigx - frX;
	li._yscale = ca.lineOrigy - frY;
	ca.callouts = this;
	
	ca.onEnterFrame = function() {
		var discall = 0; 

		var collision = 0;
		for (k = 0; k < this.step; k++) {
			var oc = this.callouts["call" + _CALL_LEVEL + k];
			var dis = distanceCalc(this._x, this._y, oc._x, oc._y);
			
			if(dis < 35) {
				collision = 1;
				this.moveMe(dis, 
							this.disX(this._x, oc._x),
							this.disY(this._y, oc._y));
			}
		}		
		// if all have gone
		if (!collision) {
			delete this.onEnterFrame;
		}		
	}
	
	ca.moveMe = function(di, d1, d2) {	
		if (di == 0) {				
			di = 3;
			d1 = 0;
			d2 = -5;
		}
		this.speed=(80-di)/40;		
		this._x+=this.speed*d1/di;		
		this._y+=this.speed*d2/di;
		if (this._y < 10) {
			this._y += 1;
		}
		if (this._y > 390) {
			this._y -= 1;
		}
		
		var frX = (this._x);
		var frY = (this._y);
		
		this.line._x =  frX;
		this.line._y = frY;
		
		this.line._xscale = this.lineOrigx - frX;
		this.line._yscale = this.lineOrigy - frY;		
	}
	
	ca.disX = function(d1, d2) {
		var disX = d1 - d2;
		return disX;
	}
	
	ca.disY = function(d1, d2) {
		var disY = d1 - d2;
		return disY;
	}

	this.nCalls++;

}


function distanceCalc(xp1, yp1, xp2, yp2)
{
	var finalx = xp2 - xp1;
	
	var finaly = yp2 - yp1;
	
	
	var final = Math.sqrt(Math.pow(finalx, 2) + Math.pow(finaly, 2));
	return final; 
}


#endinitclip