﻿#initclip 1


function curtain() {
	this.scale = 0;
	this.targ = 0;

	this.points = 0;
}

curtain.prototype = new MovieClip();

Object.registerClass("curtain", curtain);

curtain.prototype.animateSize = function(target) {
	this.targ = target;
	
	trace("animating: " + target);
	
	this.onEnterFrame = function() {		
		this.scale += (this.targ - this.scale)/3;
		this._xscale = this.scale * 100;
		this._yscale = this.scale * 100;
		if(Math.abs(this.targ - this.scale) < .001) {
			delete this.onEnterFrame;
			if(this.targ == 0) {
				this.points.curtainClosed();
				this.removeMovieClip();
			}
			else {
				this._xscale = this.targ * 100;
				this._yscale = this.targ * 100;
			}
		}
	}
}


#endinitclip 
