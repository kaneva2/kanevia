/** register_marker
 * registers markers from the map.
 * when there are three, derives a transformation
 * and places a bunch of extra gray markers in the same
 * coordinate space.
 *
 * @param   movieclip   m   a movie clip with properties x, y or latitude, longitude
 *                          (y, x override lat, long respectively if provided)
 */
function register_marker(m)
{
    if(m.x == undefined) { m.x = m.longitude; }
    if(m.y == undefined) { m.y = m.latitude; }
    trace('registered: '+m+', x/y: '+m.x+', '+m.y);
    
    if(this.markers == undefined) {
        this.markers = new Array();
    }
    
    this.markers.push(m);
    m.label.text = m.x + ', ' + m.y;
    m._visible = false;
    
    if(this.markers.length == 3) {
        m._parent.transforms = _parent.derive_transforms(this.markers);
        draw_grid(m._parent);
    }
}

/** draw_grid
 * places a bunch of extra gray markers in the same
 * coordinate space.
 *
 * @param   movieclip   mc  movie clip where markers should be attached,
 *                          and transforms object lives
 */
function draw_grid(mc)
{
    return; // do nothing.

    var d = 101;
    
    for(var y = 20; y <= 60; y += 5) {
        for(var x = -130; x <= -60; x += 5) {

            mc.attachMovie('marker', 'marker'+d, d);
            mc['marker'+d]._alpha = 35;
            mc['marker'+d]._x = mc.transforms.in2outx(x, y);
            mc['marker'+d]._y = mc.transforms.in2outy(x, y);
            mc['marker'+d].label.text = x + ', ' + y;

            d++;
        }
    }
}