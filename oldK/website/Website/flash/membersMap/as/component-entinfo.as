﻿#initclip 1


function entInfo() {
	this.buttonActions();
	this.cl_one.gotoAndStop(20);
	this.cl_two.gotoAndStop(20);
	this.cl_three.gotoAndStop(20);
	
	this.fname.tabIndex = 1;
	this.lname.tabIndex = 2;
	this.zip.tabIndex = 3;
	this.submit_button.tabIndex = 4;

	Selection.setFocus(this.fname);
	_root.focusManager.defaultPushButton = this.submit_button;
	
	this.scale = 0;
	this.targ = 0;

	this.doLastName = 1;
	this.fname = "";
	
	this.points = 0;
	
}

entInfo.prototype = new MovieClip();

Object.registerClass("entInfo", entInfo);

entInfo.prototype.go = function() {
	if(this.doLastName != 1) {
		
		
	}

	this.animateSize();
}

entInfo.prototype.animateSize = function() {
	this.onEnterFrame = function() {		
		this.scale += (this.targ - this.scale)/3;
		this._xscale = this.scale;
		this._yscale = this.scale;
		if(Math.abs(this.targ - this.scale) < .001) {
			trace("Yo entinfo");
			delete this.onEnterFrame;
			if(this.targ == 0) {
				trace("I'm done");
				this.removeMovieClip();
			}
		}
	}
}

entInfo.prototype.buttonActions = function() {
	this.submit_button.pointer = this;
	
	this.submit_button.onRelease = function() {		
		if(this.pointer.validate() == 1) {
			this.pointer.points.onInputEntered(this.pointer.fname, this.pointer.lname, this.pointer.zip);
			this.pointer.targ = 0;
			this.pointer.animateSize();
		}
	}
}

entInfo.prototype.validate = function() {
	var valid = 1; 
		
	if((this.fname == NULL) || this.fname.length == 0) {
		this.callErr(1);
		valid = 0;		
	}  else {
		this.cl_one.gotoAndStop(20);
	}
	
	if((this.lname == NULL) || this.lname.length == 0) {
		this.callErr(2);
		valid = 0;		
	} else {
		this.cl_two.gotoAndStop(20);
	}
	
	if((this.zip == NULL) || this.zip.length != 5  ) {
		this.callErr(3, valid);
		valid = 0;
	}  else {
		this.cl_three.gotoAndStop(20);
	}
	
	return valid;
}

entInfo.prototype.callErr = function(er, showErr) {	
	if(er == 1 || er == 2) {
		this.errTex = "We need your first and last name for the petition.";
	}

	if((er == 3) && (showErr == 1)) {
		this.errTex = "We need your 5-digit zip code to show you on this map.";
	}
		
	switch(er) {
		case 1: er = 1;
		this.cl_one.gotoAndStop(21);

		case 2: er = 2;
		this.cl_two.gotoAndStop(21);
		
		case 3: er = 3;
		this.cl_three.gotoAndStop(21);
	}
}

#endinitclip 
