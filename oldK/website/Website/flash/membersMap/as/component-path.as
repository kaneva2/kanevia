﻿#initclip 1

_ARC_LEVEL =4000;
_CALL_LEVEL = 6000;
_CALL_LINE_LEVEL = 5000;
_STAR_LEVEL = 50000;

var zipCoords = _root.zipCoords;

function FPath() {	
	this.path = 0;
	this.pathStep = 1;
	this.points = 0;
	this.allPoints = 0;
}

FPath.prototype = new MovieClip();

Object.registerClass("FPath", FPath);


FPath.prototype.setPath = function(path) {
	this.path = path;
}

FPath.prototype.setPoints = function(points, allPoints) {
	this.points = points;
	this.allPoints = allPoints;
}

FPath.prototype.setCallouts = function(callouts) {
	this.callouts = callouts;
}


FPath.prototype.go = function() {
	this.drawHomeNode(this.path[0]);
	this.callouts.drawCall(this.path[0], 0);
	this.interval = setInterval(FPathIntervalBounce, 0, this);
}

function FPathIntervalBounce(path) {	
	path.drawPathStep();
}

FPath.prototype.clearInterval = function() {
	if(this.interval) {
		clearInterval(this.interval);
		this.interval = 0;
	}
}

FPath.prototype.drawHomeNode = function(node) {
	var pt = this.attachMovie("star", "star" + node.zip, _STAR_LEVEL + node.zip);
	pt._x = node.x;
	pt._y = node.y;
}

FPath.prototype.drawPathStep = function() {
	this.clearInterval();
	if(this.pathStep < 0 || this.pathStep >= this.path.length)  {
//		trace(this + " no more path steps to draw");
		return;
	}
	var to = this.path[this.pathStep];
	var node = to;
	if(this.pathStep == this.path.length - 1 && node.z == 0) {
		this.whereAreYou();
		return;
	}
	if(node.z != 0 && node.x == 0 && node.y == 0) {
		var badZip = node.z;
		var newZip = findClosestZip(node.z, zipCoords);
		node.z = newZip;
		var coord = _root.zipToPos(newZip);
		node.x = coord.x;
		node.y = coord.y;
	}
	
	var from = this.path[this.pathStep - 1];
	var arc = attachMovie("FArc", "arc" + this.pathStep, this.pathStep+ _ARC_LEVEL);
	arc.drawArc(from, to, this.pathStep);
	to.n = 0;
	this.pathStep++;
	if(this.pathStep == this.path.length) {
		this.points.drawZip(to, 1);
	}
	else {
		this.allPoints.drawZip(to, 1);		
	}

	this.callouts.drawCall(to, 1);

	if(this.pathStep < this.path.length) {
		var inttime = intervalTime();
		this.interval = setInterval(FPathIntervalBounce, inttime, this);
		trace("this.interval = " + this.interval);
	}

}

FPath.prototype.whereAreYou = function() {
	var sc = 100;
	var ent = attachMovie("entInfo", "entInfo", 8000);
	ent.points = this;
	ent._x = 265;
	ent._y = 100;
	ent._xscale = 0;
	ent._yscale = 0;
	ent.targ = sc;
		
	ent.go();
	
	this.ent = ent;
}

#endinitclip