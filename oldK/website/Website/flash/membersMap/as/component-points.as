﻿#initclip 1
mainMovieTimeLine.points = this;
_POINT_LEVEL = 20000;
_ALL_POINT_LEVEL = 10000;
var zipCoords = _root.zipCoords;
var ptType = "Joe";
var levelOff;
var alpha;
function FPoints() {
	this.zipDegrees = 0;
	this.degreeStep = 1;
	this.degreeOffset = 0;
	this.zips = new Array();
	for (var i = 0; i<1000; i++) {
		this.zips[i] = 0;
	}
	this.all = 1;
	this.buttons = 0;
	this.total = 0;
}
FPoints.prototype = new MovieClip();
Object.registerClass("FPoints", FPoints);
FPoints.prototype.setDegreeOffset = function(off) {
	this.degreeOffset = off;
};
FPoints.prototype.traceTest = function() {
	trace("Here I am");
	trace("Here I am 2");
};
FPoints.prototype.setAll = function(all) {
	this.all = all;
};
FPoints.prototype.setButtons = function(buttons) {
	this.buttons = buttons;
};
FPoints.prototype.setZipDegrees = function(zipDegrees) {
	var tempDegrees = zipDegrees.length;
	trace(this+" setZipDegrees with "+zipDegrees.length+" degrees");
	this.zipDegrees = zipDegrees;
	i = 0;
	j = 0;
	for (i=0; i<this.zipDegrees.length; i++) {
		mainMovieTimeLine.degreeArray.push(zipDegrees[i]);
		//mainMovieTimeLine.totalArray.push(ts[i]);
		/*for(j = 0; j < this.zipDegrees[i].length); j++){
		mainMovieTimeLine.degreeArray.push(this.zipDegrees
		}*/
	}
	trace("Value of mainMovieTimeLine.degreeArray[1][0].y = "+mainMovieTimeLine.degreeArray[1][0].y);
	//this will set up variables to store the information for each degree into its own variable
	//this functionality was added in order to allow the user to click back and forth through
	//the levels of separation and have the program appriately draw the correct information
	//for each level
};
FPoints.prototype.go = function() {
	trace("Entering go in Comp - points");
	this.interval = setInterval(FPointsIntervalBounce, 0, this);
	trace("Interval = "+this.interval);
};
function FPointsIntervalBounce(points) {
	points.clearInterval();
	var inttime = intervalTime();
	points.interval = setInterval(FPointsIntervalBounce, inttime, points);
	points.drawDegreeStep();
}
FPoints.prototype.clearInterval = function() {
	if (this.interval) {
		//trace("This interval = " + this.interval);
		clearInterval(this.interval);
		this.interval = 0;
	}
};
//////////////////////////////////////////
// draw dots, separated from other drawing functions above because it contains the
// main functionality of the movie
///////////////////////////////////////////
FPoints.prototype.drawDegreeStep = function() {
	if (!mainMovieTimeLine.bool_buttonWasHitBefore) {
		if (this.degreeOffset>0) {
			trace(this+" not drawing, degree offset: "+this.degreeOffset);
			this.degreeOffset--;
			return;
		}
		var degree = mainMovieTimeLine.num_CurrentDegree+1;
		//this.degreeStep;
		updateAfterEvent();
		//trace("degree = " + degree);
		if (degree<0 || degree>=this.zipDegrees.length) {
			trace(this+" no more degrees to draw");
			this.clearInterval();
			//return;
		}
		if (this.all) {
			trace("this.all = "+this.all);
			if (mainMovieTimeLine.num_CurrentDegree<mainMovieTimeLine.degreeArray.length) {
				this.buttons.moveAlong();
			}
			//trace("Yo mama");     
		}
		//trace(this + " about to draw degree " + degree);     
		var zd = this.zipDegrees[degree];
		if (!zd) {
			//do something while still loading
			//		trace("Degree " + degree + " not yet loaded");
			return;
		}
		//	trace(this + " degree has " + zd.length + " zips");     
		for (var i = 0; i<zd.length; i++) {
			this.drawZip(zd[i], 1);
			this.total += zd[i].n;
			updateAfterEvent();
		}
		if (this.all) {
			var numtot = new Number(this.total);
			setTotal(degree, numTot);
		}
		//trace("this.degreeStep = " + this.degreeStep);     
		this.degreeStep++;
	}
};
FPoints.prototype.drawZip = function(zip, animate) {
	if (zip.x<=0 || zip.y<=0) {
		return;
	}
	if (!this.zips[zip.z]) {
		this.zips[zip.z] = newZipRec(zip);
	}
	this.zips[zip.z] = incrementZipRec(this.zips[zip.z], zip.n);
	var zr = this.zips[zip.z];
	var newPt = 0;
	if (!zr.obj) {

		if (this.all) {
			ptType = "FPointAnimAll";
			levelOff = _ALL_POINT_LEVEL;
			alpha = 50;
		} else {
			ptType = "FPointAnim";
			levelOff = _POINT_LEVEL;
			alpha = 50;
		}
		//var pt = this.attachMovie(ptType, ptType + zr.zip, levelOff + zr.zip);
		trace(ptType+zr.zip);
		mainMovieTimeLine.pt = this.attachMovie(ptType, ptType+zr.zip, levelOff+zr.zip);
		trace("ptType+zr.zip = "+ptType+zr.zip);
		mainMovieTimeLine.pt._x = zr.x;
		mainMovieTimeLine.pt._y = zr.y;
		mainMovieTimeLine.pt._alpha = alpha;
		////zr.obj = mainMovieTimeLine.pt;
		if (!animate) {
			mainMovieTimeLine.pt.gotoAndPlay(30);
			trace("Does this even happen??");
		}
		newPt = 1;
	}
};
//This function will assume the responsibility of drawing and undrawing points on the map once a button has been hit
FPoints.prototype.maintainPoints = function(inDirection) {
	trace("Entering maintainPoints(inDirection)");
	//we do not need to do anything if we're at the root degree (num_CurrentDegree == 0)
	//if (mainMovieTimeLine.num_CurrentDegree != 0) {
		i = 0;
		if (inDirection == 0) {
			for (var property in _root.allpoints) {
				if (typeof _root.allpoints[property] == "movieclip") {
					for (i=0; i< mainMovieTimeLine.degreeArray[mainMovieTimeLine.num_CurrentDegree].length; i++) {
						var tempName = ptType+mainMovieTimeLine.degreeArray[mainMovieTimeLine.num_CurrentDegree][i].z;
						trace("tempName = "+_root.allpoints[property]._name);
						if (_root.allpoints[property]._name == tempName) {
							trace("deleting = "+_root.allpoints[property]._name);
							_root.allpoints[property].removeMovieClip();
						}
					}
					//tempMovieArray.push(_root.allpoints[property]);
					//_root.allpoints[property].removeMovieClip();
					trace("hey = "+_root.allpoints[property]._name);
				}
				//end if (typeof _root.allpoints[property] == "movieclip")  
			}
			//end for (var property in _root.allpoints)
		}
		//end if (inDirection == 0)

		//if inDirection == 1, we need to add points to the map(allpoints) 		
		else if (inDirection == 1) {
			for(i = 0; i < mainMovieTimeLine.degreeArray[mainMovieTimeLine.num_CurrentDegree + 1].length; i++){
				tempName = ptType+mainMovieTimeLine.degreeArray[mainMovieTimeLine.num_CurrentDegree + 1][i].z;
				trace("Right Button... adding " + tempName);
				mainMovieTimeLine.pt = this.attachMovie(ptType, tempName, levelOff+mainMovieTimeLine.degreeArray[mainMovieTimeLine.num_CurrentDegree + 1][i].z);
				mainMovieTimeLine.pt._x = mainMovieTimeLine.degreeArray[mainMovieTimeLine.num_CurrentDegree + 1][i].x;
				trace(mainMovieTimeLine.pt._x);
				mainMovieTimeLine.pt._y = mainMovieTimeLine.degreeArray[mainMovieTimeLine.num_CurrentDegree + 1][i].y;
				trace(mainMovieTimeLine.pt._y);				
				mainMovieTimeLine.pt._alpha = alpha;
				/*
				mainMovieTimeLine.pt._x = zr.x;
				mainMovieTimeLine.pt._y = zr.y;
				mainMovieTimeLine.pt._alpha = alpha;
				*/
			}
		} else {
			trace("Error: maintainPoints(inDirection)");
		}
		
	//}
	/*
	//if previous degree - current degree > 0, we need to undraw the points on the previous degree
	//This step happens when the user has pressed the left button and changed to the lesser degree
	if (mainMovieTimeLine.num_PreviousDegree - mainMovieTimeLine.um_CurrentDegree > 0){
	mainMovieTimeLine.pt.removeMovieClip(
	}
	//if previous degree - current degree < 0, we need to add points on the map to represent the current degree
	//This step happens when the user has pressed the right button and changed to the next degree
	else if (mainMovieTimeLine.num_PreviousDegree - mainMovieTimeLine.um_CurrentDegree < 0){
	
	}
	
	i = 0;
	j = 0;
	k = 0;
	for(i = 1; i < mainMovieTimeLine.degreeArray.length; i ++){
	for(j = 0; j < mainMovieTimeLine.degreeArray[i].length; j++){
	var tempMovieArrayList = ptType + mainMovieTimeLine.degreeArray[i][j].z;
	}
	}
	/*
	
	i = 0;
	j = 0;
	for(i = 1; i < mainMovieTimeLine.degreeArray.length; i ++){
	for(j = 0; j < mainMovieTimeLine.degreeArray[i].length; j++){
	//this is how the movie clips are added
	
	trace("Trying to remove " + tempName);
	trace(_root.allpoints.tempName.removeMovieClip());
	//mainMovieTimeLine.pt.removeMovieClip(); //tempName.
	//_root.allPoints.removeMovieClip;								// = this.attachMovie(ptType, ptType+zr.zip, levelOff+zr.zip);
	
	}
	}
	//mainMovieTimeLine.pt.degreeArray[1][0].removeMovieClip();
	//mainMovieTimeLine.pt.degreeArray[1][1].removeMovieClip();
	//mainMovieTimeLine.pt.degreeArray[1][2].removeMovieClip();
	*/ 
}
function newZipRec(zip) {
	res = {zip:zip.z, x:zip.x, y:zip.y, n:0, obj:0};
	return res;
}
function incrementZipRec(zr, n) {
	zr.n += n;
	return zr;
}
function resizePoint(pt, n) {
	if (1) {
		mainMovieTimeLine.pt.diam = (Math.log(2+n)*2)+5;
		mainMovieTimeLine.pt._visible = true;
	} else {
		mainMovieTimeLine.pt._visible = false;
	}
	mainMovieTimeLine.pt._width = mainMovieTimeLine.pt.diam;
	mainMovieTimeLine.pt._height = mainMovieTimeLine.pt.diam;
}
#endinitclip
