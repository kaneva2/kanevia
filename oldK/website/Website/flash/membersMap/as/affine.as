﻿/** linear_solution
 * solves a system of linear equations:
 *   z1 = (a * x1) + (b + y1) + c
 *   z2 = (a * x2) + (b + y2) + c
 *   z3 = (a * x3) + (b + y3) + c
 *
 * x1 - z3 are the known values.
 * a, b, c are the unknowns to be solved.
 * generates a function which will return z for any x, y.
 * ask mike about pages 78-79 of his notebook for more info.
 *
 *  @param  array       knowns  array of known parameters.
 *                              assumes that elements
 *                              x1, x2, x3, y1, y2, y3, z1, z2, z3
 *                              are all numeric.
 *
 *  @return function    returns a function that takes two arguments
 *                      x & y and returns the corresponding z, based
 *                      on a solution from the above knowns.
 */
function linear_solution(knowns)
{
    var unknowns = new Object();
    
    unknowns.a = (((knowns.z2 - knowns.z3) * (knowns.y1 - knowns.y2)) - ((knowns.z1 - knowns.z2) * (knowns.y2 - knowns.y3))) /
                 (((knowns.x2 - knowns.x3) * (knowns.y1 - knowns.y2)) - ((knowns.x1 - knowns.x2) * (knowns.y2 - knowns.y3)));
    
    unknowns.b = (((knowns.z2 - knowns.z3) * (knowns.x1 - knowns.x2)) - ((knowns.z1 - knowns.z2) * (knowns.x2 - knowns.x3))) /
                 (((knowns.y2 - knowns.y3) * (knowns.x1 - knowns.x2)) - ((knowns.y1 - knowns.y2) * (knowns.x2 - knowns.x3)));
               
    unknowns.c = knowns.z1 - (knowns.x1 * unknowns.a) - (knowns.y1 * unknowns.b);
    /*
    trace('--> linear_solution:');
    trace('    x1, y1, z1: '+knowns.x1+', '+knowns.y1+', '+knowns.z1);
    trace('    x2, y2, z2: '+knowns.x2+', '+knowns.y2+', '+knowns.z2);
    trace('    x3, y3, z3: '+knowns.x3+', '+knowns.y3+', '+knowns.z3);
    trace('    a, b, c: '+unknowns.a+', '+unknowns.b+', '+unknowns.c);
    */
    var z1 = (unknowns.a * knowns.x1) + (unknowns.b * knowns.y1) + unknowns.c;
    //trace('z1 vs. knowns.z1: '+z1+', '+knowns.z1);
    if(Math.abs(z1 - knowns.z1) > 0.00001) { trace('failed on z1'); return false; }
    
    var z2 = (unknowns.a * knowns.x2) + (unknowns.b * knowns.y2) + unknowns.c;
    //trace('z2 vs. knowns.z2: '+z2+', '+knowns.z2);
    if(Math.abs(z2 - knowns.z2) > 0.00001) { trace('failed on z2'); return false; }
    
    var z3 = (unknowns.a * knowns.x3) + (unknowns.b * knowns.y3) + unknowns.c;
    //trace('z3 vs. knowns.z3: '+z3+', '+knowns.z3);
    if(Math.abs(z3 - knowns.z3) > 0.00001) { trace('failed on z3'); return false; }
    
    var f = function(x, y) {
        //trace('anon function a, b, c: '+unknowns.a+', '+unknowns.b+', '+unknowns.c);
        return (unknowns.a * x) + (unknowns.b * y) + unknowns.c;
    };

    return f;    
}

/** derive_transforms
 * generates a set of 2D transformation functions.
 * given a set of three well-chosen points (i.e., not on a
 * line or in one place), each with properties y & x
 * and placed at point _x, _y, returns a set
 * of four functions useful for performing linear
 * transformation from input (x/y or lon/lat) coordinates
 * to output (_x/_y) coordinates, and back.
 *
 *  @param  array   markers     array of three movie clips.
 *                              each must have input properties x, y defined.
 *
 *  @return array   array of functions:
 *                  in2outx(x, y) returns output x for given input coords
 *                  in2outy(x, y) returns output y for given input coords
 *                  out2iny(x, y) returns input y for given output coords
 *                  out2inx(x, y) returns input x for given output coords
 */
function derive_transforms(markers)
{
    var knowns = new Object();

    knowns.in2outx = new Array();
    knowns.in2outy = new Array();
    knowns.out2iny = new Array();
    knowns.out2inx = new Array();

    var i = 1;
    
    for(var m in markers) {
        var marker = markers[m];
        
        //trace(marker+', in x/y: '+marker.x+'/'+marker.y+' <--> out _x/_y: '+marker._x+'/'+marker._y);

        knowns.in2outx['x'+i] = marker.x;
        knowns.in2outx['y'+i] = marker.y;
        knowns.in2outx['z'+i] = marker._x;

        knowns.in2outy['x'+i] = marker.x;
        knowns.in2outy['y'+i] = marker.y;
        knowns.in2outy['z'+i] = marker._y;

        knowns.out2iny['x'+i] = marker._x;
        knowns.out2iny['y'+i] = marker._y;
        knowns.out2iny['z'+i] = marker.y;

        knowns.out2inx['x'+i] = marker._x;
        knowns.out2inx['y'+i] = marker._y;
        knowns.out2inx['z'+i] = marker.x;
        
        i += 1;
    }
    
    var transforms = new Object();

    transforms.in2outx = linear_solution(knowns.in2outx);
    transforms.in2outy = linear_solution(knowns.in2outy);
    transforms.out2iny = linear_solution(knowns.out2iny);
    transforms.out2inx = linear_solution(knowns.out2inx);
    
    return transforms;
}
