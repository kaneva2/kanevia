﻿stop();

_root._DEGREE_FRAMES = 30;
_root._FPS = 30;

var lv;

legend_node._visible = false;
legend_invite._visible = false;
_root._total = total;
if(_root.dataPath == undefined) {
/**/
	_root.pathEncoded = 1;
	_root.node_depth = 2;
	_root.p0z = 770;
	_root.p0n = "Kaneva";
	_root.p1z = 310;
	_root.p1n = "Hollywood";
/**/	
//	node = "f02f74ee0177f672b4d44fcc4016059c";
	node = new String(node);
	node = 0;
	//host = "file://C:\test.aspx.aspx";
    host = "http://kanevadev/services/track/memberMapStats.aspx";
	if(node) {
		_root.dataPath = host; //host + "/zip_data/" + node.substring(0,2) + "/" + node;
		_root.allDataPath = host; //host + "/zip_data/all";
	}
	else {
		//_root.dataPath = host; // + "/zip_data/all";
		//_root.dataPath = "file://C:\\test.aspx"
		_root.dataPath = "http://kanevadev/services/track/memberMapStats.aspx?communityId=919"
	}
}

// load the map geometry movie into specially-prepard empty clip called "map_geometry"
if(_root.geo_base == undefined) {
    _root.geo_base = '';
}

_root['map_geometry'].loadMovie(_root.geo_base+'map-geometry.swf');
_root['map_geometry']._x = _root['map_geometry']._y = 0;

// wait until map geometry has finished loading
_root.onEnterFrame = function() {
	if(this['map_geometry']._width > 0 && (this['map_geometry'].getBytesLoaded() == this['map_geometry'].getBytesTotal())) {

        // don't repeat this again
		this.onEnterFrame = null;
        
        this.loadZips();
        
        this._mapData = {
            isRoot: (this.allDataPath == undefined ? 1 : 0),
            
            dataLoaded: 0,
            subtreeHeight: 0,
            path: 0,
            zipDegrees: 0,
        
            allDataLoaded: 0,
            allTreeHeight: 0,
            allZipDegrees: 0
        };
        
        this.loadData(this.dataPath, this.allDataPath);
	}
};

function go() {
	path.setPath(_root._mapData.path);
	if(_root._mapData.isRoot) {
		//trace("_root._mapData.zipDegrees = " + _root._mapData.zipDegrees);
		allpoints.setZipDegrees(_root._mapData.zipDegrees);		
		buttons.setDegrees(_root._mapData.subtreeHeight, _root._mapData.path[0].nom);
	}
	else{
		points.setDegreeOffset(_root._mapData.path.length - 1);
		points.setAll(0);
		points.setZipDegrees(_root._mapData.zipDegrees);
		allpoints.setZipDegrees(_root._mapData.allZipDegrees);		
		buttons.setDegrees(_root._mapData.allTreeHeight, _root._mapData.path[0].nom);
	}

	path.setPoints(points, allPoints);
	path.setCallouts(callouts);
	path.go();
	points.go();
	allpoints.setButtons(buttons);
	allpoints.go();
}

function intervalDelay() {
	return 1000 * _root._DEGREE_FRAMES / _root._FPS;	
}

function intervalTime() {
	return intervalDelay() + 1000 * _root._DEGREE_FRAMES / _root._FPS;
}



////////////////////// LOADING / PARSING //////////////////

function loadData(dataPath, allDataPath) {
	var lv1 = new LoadVars();
	lv = lv1;
	lv.onLoad = function(success) {
		if(success == true) {
			onDataLoad(this, 0);
		}
		else {
			if(!isAll) {
				onDataLoadFail(0);
			}
		}
	}
	
	lv.load(dataPath);
	
	if((allDataPath != undefined)) {
		legend_node._visible = true;
		legend_invite._visible = true;
		var lvAll = new LoadVars();
		lvAll.onLoad = function(success) {
			if(success == true) {
				onDataLoad(this, 1);
			}
			else {
				if(!isAll) {
					onDataLoadFail(1);
				}
			}
		}
		
		lvAll.load(allDataPath);
	}
	else {
		_root._mapData.isRoot = 1;
	}
}

function onDataLoadFail(isAll) {
	trace("FAILED, isAll?" + isAll);
	if(isAll) {
		_root._mapData.allDataLoaded = -1;
	}
	else {
		_root._mapData.dataLoaded = -1;		
	}
	
	checkReady();
	
}

function onDataLoad(lv, isAll) {
	if(! isAll) {
		_root._mapData.subtreeHeight = Math.floor(lv.subtree_height);
		_root._mapData.path = parsePath(lv);
		//trace(_root._mapData.subtreeHeight);
		_root._mapData.zipDegrees = parseZipDegrees(lv, _root._mapData.subtreeHeight);
		_root._mapData.dataLoaded = 1;
	}
	else {
		_root._mapData.allTreeHeight = Math.floor(lv.subtree_height);
		_root._mapData.allZipDegrees = parseZipDegrees(lv, _root._mapData.allTreeHeight);
		_root._mapData.allDataLoaded = 1;
	}

	_root._total_stats = parseTotalStats(lv);
	
	checkReady();
}

function checkReady() {
//	trace("am i ready?" + );
	var ready = 0;
	if(_root._mapData.isRoot == 1) {
		if(_root._mapData.dataLoaded == 1) {
			ready = 1;
		}
	}
	else {
		if(_root._mapData.dataLoaded == -1) {
				if(_root.pathEncoded == 1) {
					_root._mapData.subtreeHeight = 0;
					_root._mapData.path = parsePath(_root);
					_root._mapData.zipDegrees = new Array();
					_root._mapData.dataLoaded = 1;
				}
		}
		if(_root._mapData.allDataLoaded == -1
		   && _root._mapData.dataLoaded == 1) {
			subPathForAllZip();
		}
		   
		if(_root._mapData.allDataLoaded == 1
		   && _root._mapData.dataLoaded == 1) {
				ready = 1;
		}
	}
		
	if(ready) {
		//trace("Entering go");
		go();
	}
}

function subPathForAllZip() {
	var res = new Array();
	var path = _root._mapData.path;
	for(var i = 0; i < path.length; i++) {
		res[i] = new Array();
		res[i][0] = path[i];
	}
	_root._mapData.allZipDegrees = res;
	_root._mapData.allTreeHeight = path.length - 1;
	_root._mapData.allDataLoaded = 1;
	
}

function parsePath(data) {
	
	n = Math.floor(data.node_depth);

	var res = new Array();
	var i;
	
	for(var i = 0; i < n + 1; i++) {
		
		res[i] = {  z: Math.floor(data["p" + i + "z"]),
				x: 0,
				y: 0,
				nom: data["p" + i + "n"],
				via: 0,
				nVia: 0
				};
		var coord = _root.zipToPos(res[i].z)
		if(coord) {
			res[i].x = coord.x;
			res[i].y = coord.y;
		}
	}
	
	var i = 0;
	var res2 = res.concat();
	while(i < res2.length - 1) {
		via = 0;
		var j = i + 1;
		var nVia = 0;
		while(j < res2.length - 1 && res2[j].z == 0) {
			if(via == 0) {
				via = "via ";
			}
			else {
				via += ", "; 
			}
			via +=  res2[j].nom;
			res2.splice(j, 1);
			nVia ++;			
		}
		res2[i+1].via = via;
		res2[i+1].nVia = nVia;
		i++;
	}
	
	return res2;
}

function parseTotalStats(data) {

	var ts = new Array();
	ts[0] = 0;
	mainMovieTimeLine.totalArray[0] = 0;
	
	if(data.total_stat_degrees != undefined) {
		for(var i = 1; i <= data.total_stat_degrees; i++) {
			ts[i] = ts[i - 1] + Math.floor(data["ts" + i]);
			//trace(ts[i]);
			mainMovieTimeLine.totalArray.push(ts[i]);
			//trace("mainMovieTimeLine.totalArray[" + i + "] = " + mainMovieTimeLine.totalArray[i]);
		}
	}	

	//trace("mainMovieTimeLin.totalArray.length = " + mainMovieTimeLine.totalArray.length);
	return ts;

}

function parseZipDegrees (data, nDegrees) {
	var zd = new Array();
	zd[0] = 0;
	for(var i = 1; i <= nDegrees; i++) {
		var z = parseZipDegree(Math.floor(data["nZips" + i]), i, data);
		zd[i] = z;
	}
	return zd;
}


function parseZipDegree(n, degree, data) {
	var res = new Array();
	var i;
	
	for(i = 0; i < n; i++) {
		var ind = "z" + degree + "_" + i;
		res[i] = {  z: Math.floor(data[ind+ "z"]),
				x: 0,
				y: 0,
				n: Math.floor(data[ind + "n"])			
		};
		var coord = _root.zipToPos(res[i].z)
		if(coord) {
			res[i].x = coord.x;
			res[i].y = coord.y;
		}
	}
	
	return res;
}


function setTotal(degree, n) {
	// check for global degree totals for this degree

	var val = n;

	if(_root._total_stats != undefined) {
		if(_root._total_stats.length > degree) {
			val = _root._total_stats[degree];
		}
	}
	if(!mainMovieTimeLine.bool_buttonWasHitBefore){
		_root._total.text = "Total: " + val.toString(0);
	}
	
	trace("val = " + val.toString(0));
}

