///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using KlausEnt.KEP.Kaneva.framework.widgets;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.migration
{
  public partial class migrateProfile : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!UsersUtility.IsUserAdministrator())
      {
        divForm.Visible = false;
        
        Response.Write("Access Denied");
        return;
      }

    }
    
    public void UpdateProfileLimit_Click(object sender, System.EventArgs e)
    {
      int limit = Convert.ToInt32(txtLimit.Text);
      DataTable dt = GetUsersNoFame(limit);
      DataRow userRow;
      //int user_id = Convert.ToInt32(txtUserId.Text);
      int user_id = 0;
      for (int ii = 0; ii < dt.Rows.Count; ii++)
      {
        userRow = dt.Rows[ii];        
        user_id = (int)userRow["user_id"];
        UpdateUserProfile(user_id);
      }
    }
    
    public void UpdateProfile_Click(object sender, System.EventArgs e)
    {      
      int user_id = Convert.ToInt32(txtUserId.Text);      
      UpdateUserProfile(user_id);      
    }

    private void UpdateUserProfile(int user_id)
    {
      bool success_code = true;
      if ((user_id != 1) && (user_id != 16) && (user_id != 17) && (user_id != 0))
      {

        UserFacade userFacade = new UserFacade();
        User user = userFacade.GetUser(user_id);

        if (!UpdateSequence((int)user.CommunityId))
        {
          success_code = false;
        }

        if (!CreateDefaultUserHomePage(user.UserId, user.DisplayName, user.CommunityId))
        {
          success_code = false;
        }
      }
      if (success_code == false)
      {
        lblInsertNewHome.Text += "<br/>Update FAILED";
      }
      else
      {
        lblInsertNewHome.Text += "<br/>Update SUCCEEDED";
      }
    }

    // Update page sequence and demote current home page
    private bool UpdateSequence(int channelId)
    {


        DataTable dt = PageUtility.GetLayoutPages(channelId);

        DataRow pageRow;

        for (int ii = 0; ii < dt.Rows.Count; ii++)
        {
          pageRow = dt.Rows[ii];

          int page_id = (!pageRow["page_id"].Equals(null)) ? (int)pageRow["page_id"] : 0;
          string page_name = (!pageRow["name"].Equals(null)) ? pageRow["name"].ToString() : "";

          int group_id = 0;
          if (pageRow["group_id"].Equals(null))
          {
            group_id = (int)pageRow["group_id"];
          }

          
          int access_id = (!pageRow["access_id"].Equals(null)) ? (int)pageRow["access_id"] : 0;
          int sequence = (!pageRow["sequence"].Equals(null)) ? (int)pageRow["sequence"] : 0;
          

          if(pageRow["name"].ToString() == "Home")
          {
            PageUtility.UpdateLayoutPage(page_id, "Old Home", group_id, access_id);
          }
          PageUtility.UpdateLayoutPageSequenceAndDefault(page_id, sequence + 1, false);          
        }

        return true;
                  
    }

    /// <summary>
    /// Creates the default home page for a newly registered user. Places a set of default
    /// widgets on the page.
    /// </summary>
    /// <returns>bool - true on success</returns>
    private bool CreateDefaultUserHomePage(int user_id, string displayName, int communityId)
    {


      if (user_id.Equals(0))
      {
        return false;
      }
      

      string username = displayName;

      try
      {
        string module_title = "";
     
        // default home page
        int page_id = AddLayoutPage(communityId, "Home", 0, 0, 1);

        if (page_id == 0)
        {
          return false;
        }


        // Create default set of widgets for new user

        // -- WIDGETS IN HEADER -- //

        // NAV WIDGET 
        module_title = WebCache.GetModuleTitle((int)Constants.eMODULE_TYPE.TITLE_TEXT);
        int title_panel_id = WidgetUtility.InsertLayoutModuleTitle(username, true, null);
        if (title_panel_id == 0)
        {
          return false;
        }
        PageUtility.AddLayoutPageModule(title_panel_id, page_id, (int)Constants.eMODULE_TYPE.TITLE_TEXT, (int)Constants.eMODULE_ZONE.HEADER, 1);


        // -- WIDGETS IN LEFT COLUMN --//

        // CONTROL PANEL MODULE
        //module_title = WebCache.GetModuleTitle( (int)Constants.eMODULE_TYPE.CONTROL_PANEL);
        int control_panel_id = WidgetUtility.InsertLayoutModuleControlPanel(username);
        if (control_panel_id == 0)
        {
          return false;
        }
        PageUtility.AddLayoutPageModule(control_panel_id, page_id, (int)Constants.eMODULE_TYPE.CONTROL_PANEL, (int)Constants.eMODULE_ZONE.BODY_TOP, 1);

        // Fame Panel MODULE
        module_title = WebCache.GetModuleTitle((int)Constants.eMODULE_TYPE.FAME_PANEL);
        int profile_module_id = WidgetUtility.InsertLayoutModuleFame(module_title);
        if (profile_module_id == 0)
        {
          return false;
        }
        PageUtility.AddLayoutPageModule(profile_module_id, page_id, (int)Constants.eMODULE_TYPE.FAME_PANEL, (int)Constants.eMODULE_ZONE.BODY_TOP, 2);

        // PERSONAL MODULE
        module_title = WebCache.GetModuleTitle((int)Constants.eMODULE_TYPE.PERSONAL);
        int personal_module_id = WidgetUtility.InsertLayoutModulePersonal(module_title);
        if (personal_module_id == 0)
        {
          return false;
        }
        PageUtility.AddLayoutPageModule(personal_module_id, page_id, (int)Constants.eMODULE_TYPE.PERSONAL, (int)Constants.eMODULE_ZONE.BODY_TOP, 3);

        // FRIENDS MODULE
        module_title = WebCache.GetModuleTitle((int)Constants.eMODULE_TYPE.FRIENDS);
        int friend_module_id = WidgetUtility.InsertLayoutModuleFriends(module_title, Constants.DEFAULT_FRIEND_ENTRIES_PER_PAGE);
        if (friend_module_id == 0)
        {
          return false;
        }
        PageUtility.AddLayoutPageModule(friend_module_id, page_id, (int)Constants.eMODULE_TYPE.FRIENDS, (int)Constants.eMODULE_ZONE.BODY_TOP, 4);

        // MULTIPLE PICTURES MODULE
        module_title = WebCache.GetModuleTitle((int)Constants.eMODULE_TYPE.MULTIPLE_PICTURES);
        int multiple_pictures_module_id = WidgetUtility.InsertLayoutModuleMultiplePictures(module_title, Constants.DEFAULT_MULTIPLE_PICTURES_PICTURES_PER_PAGE, Constants.DEFAULT_MULTIPLE_PICTURES_SHOW_IMAGE_TITLE, -1, (int)Constants.ePICTURE_SIZE.MEDIUM);
        if (multiple_pictures_module_id == 0)
        {
          return false;
        }
        PageUtility.AddLayoutPageModule(multiple_pictures_module_id, page_id, (int)Constants.eMODULE_TYPE.MULTIPLE_PICTURES, (int)Constants.eMODULE_ZONE.BODY, 5);

        // CHANNEL LIST
        module_title = WebCache.GetModuleTitle((int)Constants.eMODULE_TYPE.CHANNELS);
        int channel_module_id = WidgetUtility.InsertLayoutModuleChannels(module_title);
        if (channel_module_id == 0)
        {
          return false;
        }
        PageUtility.AddLayoutPageModule(channel_module_id, page_id, (int)Constants.eMODULE_TYPE.CHANNELS, (int)Constants.eMODULE_ZONE.BODY, 6);


        // -- WIDGETS IN RIGHT COLUMN -- //

        // PROFILE (INTERESTS) MODULE
        module_title = WebCache.GetModuleTitle((int)Constants.eMODULE_TYPE.INTERESTS);
        int fame_module_id = WidgetUtility.InsertLayoutModuleInterests(module_title);
        if (fame_module_id == 0)
        {
          return false;
        }
        PageUtility.AddLayoutPageModule(profile_module_id, page_id, (int)Constants.eMODULE_TYPE.INTERESTS, (int)Constants.eMODULE_ZONE.COLUMN, 1);

        // BLOG MODULE
        module_title = WebCache.GetModuleTitle((int)Constants.eMODULE_TYPE.BLOGS);
        int blog_module_id = WidgetUtility.InsertLayoutModuleBlogs(module_title);
        if (blog_module_id == 0)
        {
          return false;
        }
        PageUtility.AddLayoutPageModule(blog_module_id, page_id, (int)Constants.eMODULE_TYPE.BLOGS, (int)Constants.eMODULE_ZONE.COLUMN, 2);

        // VIDEO PLAYER
        module_title = WebCache.GetModuleTitle((int)Constants.eMODULE_TYPE.OMM_VIDEO_PLAYER);
        int video_player_module_id = WidgetUtility.InsertLayoutModuleOmmMedia((int)Constants.eMODULE_TYPE.OMM_VIDEO_PLAYER, module_title);
        if (video_player_module_id == 0)
        {
          return false;
        }
        PageUtility.AddLayoutPageModule(video_player_module_id, page_id, (int)Constants.eMODULE_TYPE.OMM_VIDEO_PLAYER, (int)Constants.eMODULE_ZONE.COLUMN, 3);

        // VIDEO Catalog
        module_title = WebCache.GetModuleTitle((int)Constants.eMODULE_TYPE.VIDEO_PLAYER);
        int video_module_id = WidgetUtility.InsertLayoutModuleStores((int)Constants.eMODULE_TYPE.VIDEO_PLAYER, module_title);
        if (video_module_id == 0)
        {
          return false;
        }
        PageUtility.AddLayoutPageModule(video_module_id, page_id, (int)Constants.eMODULE_TYPE.VIDEO_PLAYER, (int)Constants.eMODULE_ZONE.COLUMN, 4);


        // COMMENTS MODULE
        module_title = WebCache.GetModuleTitle((int)Constants.eMODULE_TYPE.COMMENTS);
        int guestbook_module_id = WidgetUtility.InsertLayoutModuleComments(module_title, Constants.DEFAULT_COMMENT_MAX_PER_PAGE, Constants.DEFAULT_COMMENT_SHOW_DATE_TIME, Constants.DEFAULT_COMMENT_SHOW_PROFILE_PIC, Constants.DEFAULT_COMMENT_AUTHOR_ALIGNMENT, 1, 0);
        if (guestbook_module_id == 0)
        {
          return false;
        }
        PageUtility.AddLayoutPageModule(guestbook_module_id, page_id, (int)Constants.eMODULE_TYPE.COMMENTS, (int)Constants.eMODULE_ZONE.COLUMN, 5);


        return true;
      }
      catch 
      {

      }

      return false;
    }

    public static int AddLayoutPage(int channel_id, string name, int group_id, int access_id, int home_page)
    {
      DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();

      string sqlSelect = "INSERT INTO layout_pages " +
        "(name, group_id, access_id, home_page, channel_id, sequence) " +
        " VALUES " +
        "( @name, @group_id, @access_id, @home_page, @channel_id, 0)";

      Hashtable parameters = new Hashtable();
      parameters.Add("@name", name);

      if (group_id != 0)
        parameters.Add("@group_id", group_id);
      else
        parameters.Add("@group_id", DBNull.Value);

      if (channel_id != 0)
        parameters.Add("@channel_id", channel_id);
      else
        parameters.Add("@channel_id", DBNull.Value);

      parameters.Add("@access_id", access_id);
      parameters.Add("@home_page", home_page);

      int pageId = 0;
      dbUtility.ExecuteIdentityInsert(sqlSelect, parameters, ref pageId);

      
      return pageId;
    }

    /// <summary>
    /// get simple user list based on a user ID and filter
    /// </summary>
    /// <param name="itemID"></param>
    /// <param name="filter"></param>
    public static DataTable GetUsersNoFame(int limit)
    {
      string sql = "";

      DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility();
      Hashtable theParams = new Hashtable();

      //attach primary key to columns and attach items table to columns to form SQL string
      sql = " SELECT * FROM users WHERE user_id not in ( " + 
            " SELECT creator_id FROM layout_page_modules pm " + 
            " inner join layout_pages p on p.page_id = pm.page_id " + 
            " inner join communities c on c.community_id = p.channel_id " + 
            " WHERE pm.module_id = 56) LIMIT @limit ";

      theParams.Add("@limit", limit);

      return dbUtility.GetDataTable(sql, theParams);

    }  


  }
}
