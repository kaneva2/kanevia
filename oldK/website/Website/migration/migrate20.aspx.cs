///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using log4net;
using KlausEnt.KEP.Kaneva.framework.widgets;

namespace KlausEnt.KEP.Kaneva.migration
{
	/// <summary>
	/// Summary description for migrate20.
	/// </summary>
	public class migrate20 : BasePage
	{
		protected System.Web.UI.WebControls.Button ChannelPages;
		protected System.Web.UI.WebControls.Button MultipleImages;
		protected System.Web.UI.WebControls.Button PeopleHomePage;
		protected System.Web.UI.WebControls.Button ChannelThumbsGenerate, GamesThumbsGenerate, PhotoThumbsGenerate, PhotoThumbsGenerate2, PhotoThumbsGenerateFunky, MusicThumbsGenerate, UserThumbsGenerate, VideoThumbsGenerate;
		protected System.Web.UI.WebControls.Button ContestProfileHeaderBannerGenerate, TitleBannerGenerate;

		protected Label lblSettings, lblImageServer;

		protected TextBox txtStartId, txtEndId;
		// Logger
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);


		private void Page_Load(object sender, System.EventArgs e)
		{
			lblSettings.Text = "KanevaGlobals.ContentServerPath = " + KanevaGlobals.ContentServerPath + "<br/>" +
                "Upload Repository = " + System.Configuration.ConfigurationManager.AppSettings["uploadRepository"].ToString();

			lblImageServer.Text = KanevaGlobals.ImageServer;
		}

		#region Web Form Designer generated code

		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.PeopleHomePage.Click += new System.EventHandler(this.PeopleHomePage_Click);
			this.ChannelPages.Click += new System.EventHandler(this.ChannelPages_Click);
			this.MultipleImages.Click += new System.EventHandler(this.MultipleImages_Click);

			this.ChannelThumbsGenerate.Click += new System.EventHandler(this.ChannelThumbsGenerate_Click);
			this.GamesThumbsGenerate.Click += new System.EventHandler(this.GamesThumbsGenerate_Click);
			this.VideoThumbsGenerate.Click += new System.EventHandler(this.VideoThumbsGenerate_Click);

			this.PhotoThumbsGenerate.Click += new System.EventHandler(this.PhotoThumbsGenerate_Click);
			this.PhotoThumbsGenerate2.Click += new System.EventHandler(this.PhotoThumbsGenerate2_Click);
			this.UserThumbsGenerate.Click += new System.EventHandler(this.UserThumbsGenerate_Click);

			this.MusicThumbsGenerate.Click += new System.EventHandler(this.MusicThumbsGenerate_Click);

			this.ContestProfileHeaderBannerGenerate.Click += new System.EventHandler(this.ContestProfileHeaderBannerGenerate_Click);
			this.TitleBannerGenerate.Click += new System.EventHandler(this.TitleBannerGenerate_Click);

			this.Load += new System.EventHandler(this.Page_Load);

		}

		#endregion


		/// <summary>
		/// Used to generate ContestProfileHeader Banners
		/// </summary>
		private void ContestProfileHeaderBannerGenerate_Click (object sender, System.EventArgs e)
		{
			string contentRepository = "";
			if (KanevaGlobals.ContentServerPath != null)
			{
				contentRepository = KanevaGlobals.ContentServerPath;
			}
			else
			{
				Response.Write ( "Migration FAILED - Could not obtain ContentServerPath");
			}

			if (!IsAdministrator ())
			{
				Response.Write( "Access Denied");
				return;
			}

			DataTable dt = CommunityUtility.GetContestProfileHeaderBanners ();

			DataRow drRow;

			try
			{
				for (int ii = 0; ii < dt.Rows.Count; ii++)
				{
					drRow = dt.Rows[ii];

					DataRow drChannel = CommunityUtility.GetCommunity ((int) drRow ["community_id"]);

					if (drChannel != null && !drChannel ["creator_id"].Equals (DBNull.Value))
					{
						int userId = Convert.ToInt32 (drChannel ["creator_id"]);

						int modulePageId = Convert.ToInt32 (drRow ["module_page_id"]);
						string filename = "hb_" + modulePageId + ".gif";

						try
						{
							System.IO.MemoryStream msImage = new System.IO.MemoryStream ((byte []) drRow ["banner"]);
							string newPath = Path.Combine (contentRepository, userId.ToString());
							ImageHelper.WriteImage (newPath, filename, msImage);

							// Save the image at the specified sizes
							WidgetUtility.UpdateChannelBanner (modulePageId, userId.ToString () + "/" + filename, "layout_module_contest_profile_header");
						}
						catch (Exception exc)
						{
							m_logger.Error ("FAILURE modulePageId = " + modulePageId, exc);
							Response.Write( "FAILURE " + exc.ToString () + "modulePageId = " + modulePageId);
						}
					}
				}
			}
			catch (Exception exc)
			{
				Response.Write( "Generation FAILED with catch exception - " + exc);
				return;
			}

			Response.Write( "Profile Header Banner Generation SUCCEEDED");
		}

		/// <summary>
		/// Used to generate Title BannerS
		/// </summary>
		private void TitleBannerGenerate_Click (object sender, System.EventArgs e)
		{
			string contentRepository = "";
			if (KanevaGlobals.ContentServerPath != null)
			{
				contentRepository = KanevaGlobals.ContentServerPath;
			}
			else
			{
				Response.Write ( "Migration FAILED - Could not obtain ContentServerPath");
			}

			if (!IsAdministrator ())
			{
				Response.Write( "Access Denied");
				return;
			}

			DataTable dt = CommunityUtility.GetTitleBanners ();

			DataRow drRow;

			try
			{
				for (int ii = 0; ii < dt.Rows.Count; ii++)
				{
					drRow = dt.Rows[ii];

					DataRow drChannel = CommunityUtility.GetCommunity ((int) drRow ["community_id"]);

					if (drChannel != null && !drChannel ["creator_id"].Equals (DBNull.Value))
					{
						int userId = Convert.ToInt32 (drChannel ["creator_id"]);

						int modulePageId = Convert.ToInt32 (drRow ["module_page_id"]);
						string filename = "tb_" + modulePageId + ".gif";

						try
						{
							System.IO.MemoryStream msImage = new System.IO.MemoryStream ((byte []) drRow ["banner"]);
							string newPath = Path.Combine (contentRepository, userId.ToString());
							ImageHelper.WriteImage (newPath, filename, msImage);

							// Save the image at the specified sizes
							WidgetUtility.UpdateChannelBanner (modulePageId, userId.ToString () + "/" + filename, "layout_module_title_text");
						}
						catch (Exception exc)
						{
							m_logger.Error ("FAILURE modulePageId = " + modulePageId, exc);
							Response.Write( "FAILURE " + exc.ToString () + "modulePageId = " + modulePageId);
						}
					}
				}
			}
			catch (Exception exc)
			{
				Response.Write( "Generation FAILED with catch exception - " + exc);
				return;
			}

			Response.Write( "Title Banner Generation SUCCEEDED");
		}

		/// <summary>
		/// Used to generate channel thumbnails
		/// </summary>
		private void ChannelThumbsGenerate_Click (object sender, System.EventArgs e)
		{
			string contentRepository = "";
			if (KanevaGlobals.ContentServerPath != null)
			{
				contentRepository = KanevaGlobals.ContentServerPath;
			}
			else
			{
				Response.Write ( "Migration FAILED - Could not obtain ContentServerPath");
			}

			if (!IsAdministrator ())
			{
				Response.Write( "Access Denied");
				return;
			}

			System.Drawing.Image imgOriginal = null;

			DataTable dt = CommunityUtility.GetChannelImagesGenThumbs (0);

			DataRow userRow;

			try
			{
				for (int ii = 0; ii < dt.Rows.Count; ii++)
				{
					userRow = dt.Rows[ii];

					int userId = (int) userRow ["creator_id"];
					int communityId = (int) userRow ["community_id"];

					string imagePath = userRow ["thumbnail_path"].ToString ();
					string imageType = userRow ["thumbnail_type"].ToString ();

					string filename = Path.GetFileName (imagePath);
					filename = StoreUtility.CleanImageFilename (filename);

					// Is there an issue?
					if (userRow ["thumbnail_path"].Equals (DBNull.Value) || imagePath.ToString ().Length.Equals (0) || !System.IO.File.Exists (imagePath))
					{
						Response.Write( "Thumbnail not found for channel " + communityId + " thumbnail_path = '" + imagePath + "'<br>");
					}
					else
					{

						try
						{
							//Create an image object from a file on disk
							imgOriginal = System.Drawing.Image.FromFile (imagePath);

							// Save the image at the specified sizes
							
							// Small
							ImageHelper.SaveChannelThumbnail (imgOriginal, (int) Constants.CHANNEL_THUMB_SMALL_HEIGHT, (int) Constants.CHANNEL_THUMB_SMALL_WIDTH, "_sm", "thumbnail_small_path",
								filename, imagePath, contentRepository, userId, communityId);

							// Medium
							ImageHelper.SaveChannelThumbnail (imgOriginal, (int) Constants.CHANNEL_THUMB_MEDIUM_HEIGHT, (int) Constants.CHANNEL_THUMB_MEDIUM_WIDTH, "_me", "thumbnail_medium_path",
								filename, imagePath, contentRepository, userId, communityId);

							// Large
							ImageHelper.SaveChannelThumbnail (imgOriginal, (int) Constants.CHANNEL_THUMB_LARGE_HEIGHT, (int) Constants.CHANNEL_THUMB_LARGE_WIDTH, "_la", "thumbnail_large_path",
								filename, imagePath, contentRepository, userId, communityId);
//
//							// X-Large
//							ImageHelper.SaveChannelThumbnail (imgOriginal, (int) Constants.CHANNEL_THUMB_XLARGE_HEIGHT, (int) Constants.CHANNEL_THUMB_XLARGE_WIDTH, "_xl", "thumbnail_xlarge_path",
//								filename, imagePath, contentRepository, userId, communityId);

							if (imgOriginal != null)
							{
								imgOriginal.Dispose();
							}
						}
						catch (Exception exc)
						{
							m_logger.Error ("Error reading image path is " + imagePath, exc);
							Response.Write( "FAILURE " + exc.ToString () + "----imagePath failed = " + imagePath);
							if (imgOriginal != null)
							{
								imgOriginal.Dispose();
							}
						}
					}
				}
			}
			catch (Exception exc)
			{
				Response.Write( "Generation FAILED with catch exception - " + exc);
				if (imgOriginal != null)
				{
					imgOriginal.Dispose();
				}
				return;
			}

			if (imgOriginal != null)
			{
				imgOriginal.Dispose();
			}

			Response.Write( "Channel Thumbs Generation SUCCEEDED");
		}

		/// <summary>
		/// Used to generate Game thumbnails
		/// </summary>
		private void GamesThumbsGenerate_Click (object sender, System.EventArgs e)
		{
			DataTable dt = StoreUtility.GetAssetsGenThumbs (Convert.ToInt32 (txtStartId.Text), Convert.ToInt32 (txtEndId.Text), (int) Constants.eASSET_TYPE.GAME);

			if (GenerateMediaThumbs (dt))
			{
				Response.Write( "Games Thumbs Generation SUCCEEDED");
			}
			else
			{
				Response.Write( "Games Thumbs Generation Failed");
			}
		}

		private void VideoThumbsGenerate_Click (object sender, System.EventArgs e)
		{
			DataTable dt = StoreUtility.GetAssetsGenThumbs (Convert.ToInt32 (txtStartId.Text), Convert.ToInt32 (txtEndId.Text), (int) Constants.eASSET_TYPE.VIDEO);

			if (GenerateMediaThumbs (dt))
			{
				Response.Write( "Video Thumbs Generation SUCCEEDED");
			}
			else
			{
				Response.Write( "Video Thumbs Generation Failed");
			}
		}

		/// <summary>
		/// Used to generate Music thumbnails
		/// </summary>
		private void MusicThumbsGenerate_Click (object sender, System.EventArgs e)
		{
			DataTable dt = StoreUtility.GetAssetsGenThumbs (Convert.ToInt32 (txtStartId.Text), Convert.ToInt32 (txtEndId.Text), (int) Constants.eASSET_TYPE.MUSIC);

			if (GenerateMediaThumbs (dt))
			{
				Response.Write( "Music Thumbs Generation SUCCEEDED");
			}
			else
			{
				Response.Write( "Music Thumbs Generation Failed");
			}
		}

		/// <summary>
		/// GenerateMediaThumbs
		/// </summary>
		private bool GenerateMediaThumbs (DataTable dt)
		{
			if (!IsAdministrator ())
			{
				Response.Write( "Access Denied");
				return false;
			}

			return ImageHelper.GenerateMediaThumbs (dt);
		}

		/// <summary>
		/// Used to generate Photo thumbnails
		/// </summary>
		private void PhotoThumbsGenerate_Click (object sender, System.EventArgs e)
		{
			DataTable dt = StoreUtility.GetAssetsGenThumbs (Convert.ToInt32 (txtStartId.Text), Convert.ToInt32 (txtEndId.Text), (int) Constants.eASSET_TYPE.PICTURE);

			if (GeneratePhotoThumbsSML (dt))
			{
				Response.Write( "Photo Thumbs Generation SUCCEEDED");
			}
			else
			{
				Response.Write( "Photo Thumbs Generation Failed");
			}
		}

		/// <summary>
		/// Used to generate Photo thumbnails
		/// </summary>
		private void PhotoThumbsGenerate2_Click (object sender, System.EventArgs e)
		{
			DataTable dt = StoreUtility.GetAssetsGenThumbs (Convert.ToInt32 (txtStartId.Text), Convert.ToInt32 (txtEndId.Text), (int) Constants.eASSET_TYPE.PICTURE);

			if (GeneratePhotoThumbsLXLAD (dt))
			{
				Response.Write( "Photo Thumbs Generation SUCCEEDED");
			}
			else
			{
				Response.Write( "Photo Thumbs Generation Failed");
			}
		}

		/// <summary>
		/// GeneratePhotoThumbs
		/// </summary>
		private bool GeneratePhotoThumbsLXLAD (DataTable dt)
		{
			string contentRepository = "";
			if (KanevaGlobals.ContentServerPath != null)
			{
				contentRepository = KanevaGlobals.ContentServerPath;
			}
			else
			{
				Response.Write ( "Migration FAILED - Could not obtain ContentServerPath");
			}

			if (!IsAdministrator ())
			{
				Response.Write( "Access Denied");
				return false;
			}

			System.Drawing.Image imgOriginal = null;

			DataRow userRow;

			try
			{
				for (int ii = 0; ii < dt.Rows.Count; ii++)
				{
					userRow = dt.Rows[ii];

					int userId = (int) userRow ["owner_id"];
					int assetId = (int) userRow ["asset_id"];
					int communityId = (int) userRow ["community_id"];

					string imagePath = userRow ["target_dir"].ToString().Replace("/","\\") + "\\" + userRow ["content_extension"].ToString();
	                
					// Make sure it is a valid image
					if (StoreUtility.IsImageContent (userRow ["content_extension"].ToString()) && System.IO.File.Exists (imagePath))
					{
						// Looks good so far
					}
					else
					{
						//imagePath = Request.PhysicalApplicationPath + DEFAULT_ASSET_IMAGE;
					}


					// Clean the filename
					string filename = Path.GetFileName (imagePath);
					filename = StoreUtility.CleanImageFilename (filename);
					
					// Is there an issue?
					if (userRow ["target_dir"].Equals (DBNull.Value) || imagePath.ToString ().Length.Equals (0) || !System.IO.File.Exists (imagePath))
					{
						Response.Write( "Photo not found for photo " + assetId + " thumbnail_path = '" + imagePath + "'<br>");
					}
					else
					{

						try
						{
							//Create an image object from a file on disk
							imgOriginal = System.Drawing.Image.FromFile (imagePath);

							// Save the image at the specified sizes

							// Save the original path
							StoreUtility.UpdateAssetThumb (assetId, userId.ToString () + "/" + assetId.ToString() + "/" + filename, "image_full_path");
							
							// Large
							ImageHelper.SaveAssetThumbnail (imgOriginal, (int) Constants.PHOTO_THUMB_LARGE_HEIGHT, (int) Constants.PHOTO_THUMB_LARGE_WIDTH, "_la", "thumbnail_large_path",
								filename, imagePath, contentRepository, userId, assetId);

							// XL
							ImageHelper.SaveAssetThumbnail (imgOriginal, (int) Constants.PEOPLE_THUMB_XLARGE_HEIGHT, (int) Constants.PHOTO_THUMB_XLARGE_WIDTH, "_xl", "thumbnail_xlarge_path",
								filename, imagePath, contentRepository, userId, assetId);

							// Asset Details
							ImageHelper.SaveAssetThumbnail (imgOriginal, (int) Constants.PHOTO_THUMB_ASSETDETAILS_HEIGHT, (int) Constants.PHOTO_THUMB_ASSETDETAILS_WIDTH, "_ad", "thumbnail_assetdetails_path",
								filename, imagePath, contentRepository, userId, assetId);

							if (imgOriginal != null)
							{
								imgOriginal.Dispose();
							}
						}
						catch (Exception exc)
						{
							m_logger.Error ("Error reading image path is " + imagePath, exc);
							Response.Write( "FAILURE " + exc.ToString () + "----imagePath failed = " + imagePath);
							if (imgOriginal != null)
							{
								imgOriginal.Dispose();
							}
						}
					}
				}
			}
			catch (Exception exc)
			{
				Response.Write( "Generation FAILED with catch exception - " + exc);
				if (imgOriginal != null)
				{
					imgOriginal.Dispose();
				}
				return false;
			}

			if (imgOriginal != null)
			{
				imgOriginal.Dispose();
			}

			return true;
		}

		/// <summary>
		/// GeneratePhotoThumbs
		/// </summary>
		private bool GeneratePhotoThumbsSML (DataTable dt)
		{
			string contentRepository = "";
			if (KanevaGlobals.ContentServerPath != null)
			{
				contentRepository = KanevaGlobals.ContentServerPath;
			}
			else
			{
				Response.Write ( "Migration FAILED - Could not obtain ContentServerPath");
			}

			if (!IsAdministrator ())
			{
				Response.Write( "Access Denied");
				return false;
			}

			System.Drawing.Image imgOriginal = null;

			DataRow userRow;

			try
			{
				for (int ii = 0; ii < dt.Rows.Count; ii++)
				{
					userRow = dt.Rows[ii];

					int userId = (int) userRow ["owner_id"];
					int assetId = (int) userRow ["asset_id"];
					int communityId = (int) userRow ["community_id"];

					string imagePath = userRow ["target_dir"].ToString().Replace("/","\\") + "\\" + userRow ["content_extension"].ToString();
	                
					// Make sure it is a valid image
					if (StoreUtility.IsImageContent (userRow ["content_extension"].ToString()) && System.IO.File.Exists (imagePath))
					{
						// Looks good so far
					}
					else
					{
						//imagePath = Request.PhysicalApplicationPath + DEFAULT_ASSET_IMAGE;
					}


					// Clean the filename
					string filename = Path.GetFileName (imagePath);
					filename = StoreUtility.CleanImageFilename (filename);
					
					// Is there an issue?
					if (userRow ["target_dir"].Equals (DBNull.Value) || imagePath.ToString ().Length.Equals (0) || !System.IO.File.Exists (imagePath))
					{
						Response.Write( "Photo not found for photo " + assetId + " thumbnail_path = '" + imagePath + "'<br>");
					}
					else
					{

						try
						{
							//Create an image object from a file on disk
							imgOriginal = System.Drawing.Image.FromFile (imagePath);

							// Save the image at the specified sizes

							// Save the original path
							StoreUtility.UpdateAssetThumb (assetId, userId.ToString () + "/" + assetId.ToString() + "/" + filename, "image_full_path");
							
							// Small
							ImageHelper.SaveAssetThumbnail (imgOriginal, (int) Constants.PHOTO_THUMB_SMALL_HEIGHT, (int) Constants.PHOTO_THUMB_SMALL_WIDTH, "_sm", "thumbnail_small_path",
								filename, imagePath, contentRepository, userId, assetId);

							// Medium
							ImageHelper.SaveAssetThumbnail (imgOriginal, (int) Constants.PHOTO_THUMB_MEDIUM_HEIGHT, (int) Constants.PHOTO_THUMB_MEDIUM_WIDTH, "_me", "thumbnail_medium_path",
								filename, imagePath, contentRepository, userId, assetId);

							// Large
							ImageHelper.SaveAssetThumbnail (imgOriginal, (int) Constants.PHOTO_THUMB_LARGE_HEIGHT, (int) Constants.PHOTO_THUMB_LARGE_WIDTH, "_la", "thumbnail_large_path",
								filename, imagePath, contentRepository, userId, assetId);

							if (imgOriginal != null)
							{
								imgOriginal.Dispose();
							}
						}
						catch (Exception exc)
						{
							m_logger.Error ("Error reading image path is " + imagePath, exc);
							Response.Write( "FAILURE " + exc.ToString () + "----imagePath failed = " + imagePath);
							if (imgOriginal != null)
							{
								imgOriginal.Dispose();
							}
						}
					}
				}
			}
			catch (Exception exc)
			{
				Response.Write( "Generation FAILED with catch exception - " + exc);
				if (imgOriginal != null)
				{
					imgOriginal.Dispose();
				}
				return false;
			}

			if (imgOriginal != null)
			{
				imgOriginal.Dispose();
			}

			return true;
		}
		

		/// <summary>
		/// Used to generate User thumbnails
		/// </summary>
		private void UserThumbsGenerate_Click (object sender, System.EventArgs e)
		{
			string contentRepository = "";
			if (KanevaGlobals.ContentServerPath != null)
			{
				contentRepository = KanevaGlobals.ContentServerPath;
			}
			else
			{
				Response.Write ( "Migration FAILED - Could not obtain ContentServerPath");
			}

			if (!IsAdministrator ())
			{
				Response.Write( "Access Denied");
				return;
			}

			System.Drawing.Image imgOriginal = null;

			DataTable dt = CommunityUtility.GetChannelImagesGenThumbs (1);

			DataRow userRow;

			try
			{
				for (int ii = 0; ii < dt.Rows.Count; ii++)
				{
					userRow = dt.Rows[ii];

					int userId = (int) userRow ["creator_id"];
					int communityId = (int) userRow ["community_id"];

					string imagePath = userRow ["thumbnail_path"].ToString ();
					string imageType = userRow ["thumbnail_type"].ToString ();

					string filename = Path.GetFileName (imagePath);
					
					// Is there an issue?
					if (userRow ["thumbnail_path"].Equals (DBNull.Value) || imagePath.ToString ().Length.Equals (0) || !System.IO.File.Exists (imagePath))
					{
						Response.Write( "Thumbnail not found for channel " + communityId + " thumbnail_path = '" + imagePath + "'<br>");
					}
					else
					{

						try
						{
							//Create an image object from a file on disk
							imgOriginal = System.Drawing.Image.FromFile (imagePath);

							// Save the image at the specified sizes
							
							// Small
							ImageHelper.SaveChannelThumbnail (imgOriginal, (int) Constants.PEOPLE_THUMB_SMALL_HEIGHT, (int) Constants.PEOPLE_THUMB_SMALL_WIDTH, "_sm", "thumbnail_small_path",
								filename, imagePath, contentRepository, userId, communityId);

							// Medium
							ImageHelper.SaveChannelThumbnail (imgOriginal, (int) Constants.PEOPLE_THUMB_MEDIUM_HEIGHT, (int) Constants.PEOPLE_THUMB_MEDIUM_WIDTH, "_me", "thumbnail_medium_path",
								filename, imagePath, contentRepository, userId, communityId);

							// Large
							ImageHelper.SaveChannelThumbnail (imgOriginal, (int) Constants.PEOPLE_THUMB_LARGE_HEIGHT, (int) Constants.PEOPLE_THUMB_LARGE_WIDTH, "_la", "thumbnail_large_path",
								filename, imagePath, contentRepository, userId, communityId);

							// X-Large
							ImageHelper.SaveChannelThumbnail (imgOriginal, (int) Constants.PEOPLE_THUMB_XLARGE_HEIGHT, (int) Constants.PEOPLE_THUMB_XLARGE_WIDTH, "_xl", "thumbnail_xlarge_path",
								filename, imagePath, contentRepository, userId, communityId);

							if (imgOriginal != null)
							{
								imgOriginal.Dispose();
							}
						}
						catch (Exception exc)
						{
							m_logger.Error ("Error reading image path is " + imagePath, exc);
							Response.Write( "FAILURE " + exc.ToString () + "----imagePath failed = " + imagePath);
							if (imgOriginal != null)
							{
								imgOriginal.Dispose();
							}
						}
					}
				}
			}
			catch (Exception exc)
			{
				Response.Write( "Generation FAILED with catch exception - " + exc);
				if (imgOriginal != null)
				{
					imgOriginal.Dispose();
				}
				return;
			}

			if (imgOriginal != null)
			{
				imgOriginal.Dispose();
			}

			Response.Write( "User Thumbs Generation SUCCEEDED");
		}

		private void PeopleHomePage_Click(object sender, System.EventArgs e)
		{
			Response.Write( "No Longer Needed");

//			DataTable dt = UsersUtility.GetUsersList();
//
//			bool success_code = true;
//
//			DataRow userRow;
//
//			for (int ii = 0; ii < dt.Rows.Count; ii++)
//			{
//				userRow = dt.Rows[ii];
//
//				int user_id = (int) userRow["user_id"];
//
////				if ( (user_id != 1) && (user_id != 16) && (user_id != 17) )
////				{
//					if ( ! UsersUtility.CreateDefaultUserHomePage(null, user_id) )
//					{
//						success_code = false;
//						break;
//					}
////				}
//			}
//
//			if ( success_code == false)
//			{
//				Response.Write( "Migration FAILED");
//			}
//			else
//			{
//				Response.Write( "Migration SUCCEEDED");
//			}
		}

		private void ChannelPages_Click(object sender, System.EventArgs e)
		{
			bool success_code = true;

			if (!IsAdministrator ())
			{
				Response.Write( "Access Denied");
				return;
			}

			DataTable dt = null; // CommunityUtility.GetAllCommunityTabs();

			DataRow communityRow;

			string module_title = "";

			int initial_community_id = 0;

			bool hasNowPlaying = false;
			bool hasSummary = false;
			bool hasBlog = false;
			bool hasForums = false;
			bool hasMembers = false;
			bool hasGameServers = false;
			bool hasGallery = false;

			int standard_home = 0;
			int custom_home_page = 0;

			for (int ii = 0; ii < dt.Rows.Count;)
			{
				string home_page = null;

				communityRow = dt.Rows[ii];

				int community_id = (int) communityRow["community_id"];

				standard_home = 0;

				if ( initial_community_id != community_id)
				{
					GetTabsForCommunity( dt, ii, community_id,
										 ref hasNowPlaying,
										 ref hasSummary,
										 ref hasBlog,
										 ref hasForums,
										 ref hasMembers,
										 ref hasGameServers,
										 ref hasGallery);

					home_page = (string) communityRow["tab_name"];

					if ( (home_page == "Now Playing!") ||
						 (home_page == "Summary") ||
						 (home_page == "Blog") ||
						 (home_page == "Forums") ||
						 (home_page == "Members") ||
						 (home_page == "Game Servers") ||
						 (home_page == "Gallery") )
					{
						standard_home = 1;
					}
					else
					{
						//custom_home_page = 1;
						standard_home = 1; // Currently, don't assign their custom page
										   // as 'the' home page. Re-enable if this changes.
					}

					initial_community_id = community_id;

					int page_id = PageUtility.AddLayoutPage (community_id, "Home", 0, 0, standard_home);

					if ( page_id == 0)
					{
						success_code = false;
						break;
					}

					// Always insert module_title_text

					//module_title = WebCache.GetModuleTitle( (int)Constants.eMODULE_TYPE.TITLE_TEXT);
					module_title = WidgetUtility.GetDefaultTitle(page_id);

					int title_text_id = WidgetUtility.InsertLayoutModuleTitle(module_title, true, "");

					if ( title_text_id == 0)
					{
						success_code = false;
						break;
					}

					PageUtility.AddLayoutPageModule(title_text_id, page_id, (int) Constants.eMODULE_TYPE.TITLE_TEXT, (int) Constants.eMODULE_ZONE.HEADER, 1);

					// Always insert module_channel_description

					module_title = WebCache.GetModuleTitle( (int)Constants.eMODULE_TYPE.CHANNEL_DESCRIPTION);

					int channel_description_id = WidgetUtility.InsertLayoutModuleChannelDescription(module_title, Constants.DEFAULT_CHANNEL_NUM_MODERATORS_TO_SHOW, Constants.DEFAULT_CHANNEL_SHOW_MODERATORS);

					if ( channel_description_id == 0)
					{
						success_code = false;
						break;
					}

					int module_sequence = 1;

					PageUtility.AddLayoutPageModule(channel_description_id, page_id, (int) Constants.eMODULE_TYPE.CHANNEL_DESCRIPTION, (int) Constants.eMODULE_ZONE.BODY, module_sequence);

					module_sequence++;

					if (hasMembers)
					{
						// Members module

						module_title = WebCache.GetModuleTitle( (int)Constants.eMODULE_TYPE.CHANNEL_MEMBERS);

						int channel_members_id = WidgetUtility.InsertLayoutModuleChannelMembers(module_title);

						if ( channel_members_id == 0)
						{
							success_code = false;
							break;
						}

						PageUtility.AddLayoutPageModule(channel_members_id, page_id, (int) Constants.eMODULE_TYPE.CHANNEL_MEMBERS, (int) Constants.eMODULE_ZONE.BODY, module_sequence);

						module_sequence++;
					}

					module_title = WebCache.GetModuleTitle( (int)Constants.eMODULE_TYPE.MULTIPLE_PICTURES );

					int multiple_pictures_module_id = WidgetUtility.InsertLayoutModuleMultiplePictures( module_title, Constants.DEFAULT_MULTIPLE_PICTURES_PICTURES_PER_PAGE, Constants.DEFAULT_MULTIPLE_PICTURES_SHOW_IMAGE_TITLE, -1, (int)Constants.ePICTURE_SIZE.MEDIUM);

					if ( multiple_pictures_module_id == 0)
					{
						success_code = false;
						break;
					}

					PageUtility.AddLayoutPageModule(multiple_pictures_module_id, page_id, (int) Constants.eMODULE_TYPE.MULTIPLE_PICTURES, (int) Constants.eMODULE_ZONE.BODY, module_sequence);

					module_sequence++;

					// Always insert module_events

					module_title = WebCache.GetModuleTitle( (int)Constants.eMODULE_TYPE.CHANNEL_EVENTS);

					int event_id = WidgetUtility.InsertLayoutModuleEvents(module_title);

					if ( event_id == 0)
					{
						success_code = false;
						break;
					}

					PageUtility.AddLayoutPageModule(event_id, page_id, (int) Constants.eMODULE_TYPE.CHANNEL_EVENTS, (int) Constants.eMODULE_ZONE.BODY, module_sequence);

					module_sequence++;

					// Always insert module_counter

					module_title = WebCache.GetModuleTitle( (int)Constants.eMODULE_TYPE.MY_COUNTER);

					int counter_id = WidgetUtility.InsertLayoutModuleCounter(module_title);

					if ( counter_id == 0)
					{
						success_code = false;
						break;
					}

					PageUtility.AddLayoutPageModule(counter_id, page_id, (int) Constants.eMODULE_TYPE.MY_COUNTER, (int) Constants.eMODULE_ZONE.BODY, module_sequence);

					module_sequence = 1; // switching to right column

					// Always insert module_control_panel

					module_title = WebCache.GetModuleTitle( (int)Constants.eMODULE_TYPE.CHANNEL_CONTROL_PANEL);

					int control_panel_id = WidgetUtility.InsertLayoutModuleChannelControlPanel(module_title);

					if ( control_panel_id == 0)
					{
						success_code = false;
						break;
					}

					PageUtility.AddLayoutPageModule(control_panel_id, page_id, (int) Constants.eMODULE_TYPE.CHANNEL_CONTROL_PANEL, (int) Constants.eMODULE_ZONE.COLUMN, module_sequence);

					module_sequence++;

					if ( hasBlog)
					{
						module_title = WebCache.GetModuleTitle( (int)Constants.eMODULE_TYPE.BLOGS );

						int blog_module_id = WidgetUtility.InsertLayoutModuleBlogs(module_title);

						if ( blog_module_id == 0)
						{
							success_code = false;
							break;
						}

						PageUtility.AddLayoutPageModule(blog_module_id, page_id, (int) Constants.eMODULE_TYPE.BLOGS, (int) Constants.eMODULE_ZONE.COLUMN, module_sequence);

						module_sequence++;
					}

					if ( hasNowPlaying)
					{
						module_title = WebCache.GetModuleTitle( (int)Constants.eMODULE_TYPE.MUSIC_PLAYER);

						// Music
						int music_module_id = WidgetUtility.InsertLayoutModuleStores ( (int) Constants.eMODULE_TYPE.MUSIC_PLAYER, module_title);

						if ( music_module_id == 0)
						{
							success_code = false;
							break;
						}

						PageUtility.AddLayoutPageModule(music_module_id, page_id, (int) Constants.eMODULE_TYPE.MUSIC_PLAYER, (int) Constants.eMODULE_ZONE.COLUMN, module_sequence);

						module_sequence++;

						// Games
						module_title = WebCache.GetModuleTitle( (int)Constants.eMODULE_TYPE.GAMES_PLAYER);

						int games_module_id = WidgetUtility.InsertLayoutModuleStores ( (int) Constants.eMODULE_TYPE.GAMES_PLAYER, module_title);

						if ( games_module_id == 0)
						{
							success_code = false;
							break;
						}

						PageUtility.AddLayoutPageModule(games_module_id, page_id, (int) Constants.eMODULE_TYPE.GAMES_PLAYER, (int) Constants.eMODULE_ZONE.COLUMN, module_sequence);

						module_sequence++;

						// Video
						module_title = WebCache.GetModuleTitle( (int)Constants.eMODULE_TYPE.VIDEO_PLAYER);

						int video_module_id = WidgetUtility.InsertLayoutModuleStores ( (int) Constants.eMODULE_TYPE.VIDEO_PLAYER, module_title);

						if ( video_module_id == 0)
						{
							success_code = false;
							break;
						}

						PageUtility.AddLayoutPageModule(video_module_id, page_id, (int) Constants.eMODULE_TYPE.VIDEO_PLAYER, (int) Constants.eMODULE_ZONE.COLUMN, module_sequence);

						module_sequence++;
					}

					if ( hasForums)
					{
						module_title = WebCache.GetModuleTitle( (int)Constants.eMODULE_TYPE.CHANNEL_FORUM);

						int forum_module_id = WidgetUtility.InsertLayoutModuleChannelForum(module_title);

						if ( forum_module_id == 0)
						{
							success_code = false;
							break;
						}

						PageUtility.AddLayoutPageModule(forum_module_id, page_id, (int) Constants.eMODULE_TYPE.CHANNEL_FORUM, (int) Constants.eMODULE_ZONE.COLUMN, module_sequence);

						module_sequence++;
					}
				}
				else
				{
					if ( (ii % 50) == 0)
					{
						string buf = string.Format("Migrating record {0} of {1}", ii+1, dt.Rows.Count);
						System.Diagnostics.Trace.WriteLine(buf);
					}

					// Custom page

					ii++;

					int editable = Convert.ToInt32(communityRow["editable"]);
					string tab_name = (string) communityRow["tab_name"];
					string html_text = (string) communityRow["html_text"].ToString();

					if ( (editable == 1) && (html_text != null) )
					{
						// Tabs sql were requested in ascending tab order

						int page_id = PageUtility.AddLayoutPage (community_id, tab_name, 0, 0, custom_home_page);

						if ( page_id == 0)
						{
							success_code = false;
							break;
						}

						custom_home_page = 0;

						int menu_id = WidgetUtility.InsertLayoutModuleMenu();

						if ( menu_id == 0)
						{
							success_code = false;
							break;
						}

						PageUtility.AddLayoutPageModule(menu_id, page_id, (int) Constants.eMODULE_TYPE.MENU, (int) Constants.eMODULE_ZONE.HEADER, 1);

						module_title = WebCache.GetModuleTitle( (int)Constants.eMODULE_TYPE.HTML_CONTENT );

						int html_text_id = WidgetUtility.InsertLayoutModuleHtml(module_title, html_text);

						if ( html_text_id == 0)
						{
							success_code = false;
							break;
						}

						PageUtility.AddLayoutPageModule(html_text_id, page_id, (int) Constants.eMODULE_TYPE.HTML_CONTENT, (int) Constants.eMODULE_ZONE.BODY, 1);
					}
				}
			}

			if ( success_code == false)
			{
				Response.Write( "Migration FAILED");
			}
			else
			{
				Response.Write( "Migration SUCCEEDED");
			}

		}


		private void GetTabsForCommunity( DataTable dt, int ii, int initial_community_id,
										  ref bool hasNowPlaying,
										  ref bool hasSummary,
										  ref bool hasBlog,
										  ref bool hasForums,
										  ref bool hasMembers,
										  ref bool hasGameServers,
										  ref bool hasGallery)
		{
			int community_id = 0;

			hasNowPlaying = false;
			hasSummary = false;
			hasBlog = false;
			hasForums = false;
			hasMembers = false;
			hasGameServers = false;
			hasGallery = false;

			DataRow communityRow;

			if ( ii < dt.Rows.Count) // safety
			{
				communityRow = dt.Rows[ii];

				community_id = (int) communityRow["community_id"];

				// Loop through all the rows for this community id to
				// determine what tabs are enabled.

				while (community_id == initial_community_id)
				{
					string tab_name = (string) communityRow["tab_name"];

					if ( tab_name == "Now Playing!")
					{
						hasNowPlaying = true;
					}
					else if ( tab_name == "Summary")
					{
						hasSummary = true;
					}
					else if ( tab_name == "Blog")
					{
						hasBlog = true;
					}
					else if ( tab_name == "Forums")
					{
						hasForums = true;
					}
					else if ( tab_name == "Members")
					{
						hasMembers = true;
					}
					else if ( tab_name == "Game Servers")
					{
						hasGameServers = true;
					}
					else if ( tab_name == "Gallery")
					{
						hasGallery = true;
					}

					ii++;

					if ( ii < dt.Rows.Count)
					{
						communityRow = dt.Rows[ii];

						community_id = (int) communityRow["community_id"];
					}
					else
					{
						break;
					}
				}
			}
		}

		/// <summary>
		/// UserImages_Click
		/// </summary>
		private void UserImages_Click (object sender, System.EventArgs e)
		{
			string uploadRepository = "";
			if (KanevaGlobals.ContentServerPath != null)
			{
				uploadRepository = KanevaGlobals.ContentServerPath;
			}
			else
			{
				Response.Write( "Migration FAILED - Could not obtain ContentServerPath");
			}

			DataTable dt = UsersUtility.GetUsersToMigrateImages ();

			DataRow userRow;

			try
			{
				for (int ii = 0; ii < dt.Rows.Count; ii++)
				{
					userRow = dt.Rows[ii];

					int userId = (int) userRow["user_id"];
					int communityId = (int) userRow["community_id"];

					if (!userRow["avatar_path"].Equals (DBNull.Value))
					{
						string imagePath = (string) userRow["avatar_path"];
						string imageType = userRow["avatar_type"].ToString ();

						string filename = Path.GetFileName (imagePath);
						string itemName = filename;

						if (! userRow.IsNull("avatar") )
						{
							System.IO.MemoryStream msImage = new System.IO.MemoryStream ((byte []) userRow ["avatar"]);

							// Correct some file issues
							if (filename.StartsWith ("KanevaIcon01.gif") || filename.StartsWith ("display") || filename.StartsWith (".aspx") || filename.Length.Equals (0))
							{
								byte [] bAvatar = (byte []) userRow ["avatar"];

								if (bAvatar.Length > 0 && bAvatar [0].Equals ((byte) 255))
								{
									filename = "avatar.jpg";
									imageType = "image/jpeg";
								}
								else
								{
									filename = "avatar.gif";
									imageType = "image/gif";
								}
							}
							else
							{
								if (filename.EndsWith (".gif"))
								{
									imageType = "image/gif";
								}
								else if (filename.EndsWith (".jpg"))
								{
									imageType = "image/jpeg";
								}
							}


							string newPath = Path.Combine (uploadRepository, userId.ToString());

							try
							{
								ImageHelper.WriteImage (newPath, filename, msImage);
								//UsersUtility.UpdateUserAvatar (userId, newPath + Path.DirectorySeparatorChar + filename, imageType);
								CommunityUtility.UpdateCommunity (communityId, newPath + Path.DirectorySeparatorChar + filename, imageType);
							}
							catch (System.ArgumentException ioexc)
							{
								m_logger.Error ("Error migrating user image " + filename + ", " + ioexc.ToString ());
							}
							catch (System.IO.IOException ioexc2)
							{
								m_logger.Error ("Error migrating user image " + filename + ", " + ioexc2.ToString ());
							}
						}
					}
				}
			}
			catch (Exception exc)
			{
				Response.Write( "Migration FAILED with catch exception - " + exc);
				return;
			}

			Response.Write( "Member Channel Photo Migration SUCCEEDED");
		}

		/// <summary>
		/// create new asset_upload and asset record
		/// </summary>
		/// <param name="user_id"></param>
		private string CreateNewRecord (int communityId, int user_id, string itemName, string fileName, int assetTypeId, int categoryId, string tags, long size, string uploadRepository)
		{
			//create a new asset
			string newItemName = itemName;
			int index = 2;
			string newPath = "";

			// Make the item name unique
			while (StoreUtility.AssetNameAlreadyExists (newItemName, 0))
			{
				newItemName = itemName + " [" + index + "]";
				index ++;
			}

			// Create a new asset record
			int assetId = StoreUtility.InsertAsset (assetTypeId, newItemName, user_id, UsersUtility.GetUserNameFromId (user_id), (int) Constants.ePUBLISH_STATUS.UPLOADED, 0, (int) Constants.eASSET_RATING.GENERAL, 0, "");

			if (assetId > 0)
			{
				// Add it to the community
				if (communityId > 0)
				{
					StoreUtility.InsertAssetChannel (assetId, communityId);
				}

				int assetUploadId = StoreUtility.InsertAssetUpload(user_id, fileName, uploadRepository, size, "", (int) Constants.eDS_INVENTORYTYPE.ASSETS, assetId);

				newPath = Path.Combine (Path.Combine(uploadRepository, user_id.ToString()), assetId.ToString());
				StoreUtility.UpdateAssetUploadPath (assetUploadId, newPath);

				StoreUtility.UpdateAssetUploadStatus (assetId, (int) Constants.ePUBLISH_STATUS.UPLOADED, Request.UserHostAddress);

//				// Update the tags
//				if ( tags != null && tags.Length > 0)
//				{
//					int orderItemId = StoreUtility.InsertTagItem (user_id, Request.UserHostAddress, assetId, 1, "");
//					StoreUtility.UpdateOrderItemTag (user_id, orderItemId,  Server.HtmlEncode (tags));
//				}
			}

			return newPath;
		}

		private void MultipleImages_Click(object sender, System.EventArgs e)
		{
			bool success_code = true;
			string module_title = "";

			string sqlSelect = "SELECT page_id from layout_pages where home_page=1 and name='Home' and channel_id is not null and channel_id <> 0 order by page_id";

			DataTable dt = KanevaGlobals.GetDatabaseUtility ().GetDataTable (sqlSelect);

			DataRow pageRow;

			for (int ii = 0; ii < dt.Rows.Count; ii++)
			{
				pageRow = dt.Rows[ii];

				int pageId = (int) pageRow["page_id"];

			    string sqlSelect2 = "SELECT sequence from layout_page_modules where page_id=" + pageId + " and zone_id=2 order by sequence";

				DataTable dt2 = KanevaGlobals.GetDatabaseUtility ().GetDataTable (sqlSelect2);

				DataRow layoutPageRow;

				int module_sequence = 0;
				int saved_module_sequence = 1; //always have a 1

				for (int jj = 0; jj < dt2.Rows.Count; jj++)
				{
					layoutPageRow = dt2.Rows[jj];
					module_sequence = (int) layoutPageRow["sequence"];
					if (module_sequence > saved_module_sequence+1)
					{
						module_sequence--; // use this slot
						break;
					}
					else
					{
						saved_module_sequence = module_sequence;
					}
				}

				// Should be 2 or 3

				module_title = WebCache.GetModuleTitle( (int)Constants.eMODULE_TYPE.MULTIPLE_PICTURES );

				int multiple_pictures_module_id = WidgetUtility.InsertLayoutModuleMultiplePictures( module_title, Constants.DEFAULT_MULTIPLE_PICTURES_PICTURES_PER_PAGE, Constants.DEFAULT_MULTIPLE_PICTURES_SHOW_IMAGE_TITLE, -1, (int)Constants.ePICTURE_SIZE.MEDIUM);

				if ( multiple_pictures_module_id == 0)
				{
					success_code = false;
					break;
				}

				PageUtility.AddLayoutPageModule(multiple_pictures_module_id, pageId, (int) Constants.eMODULE_TYPE.MULTIPLE_PICTURES, (int) Constants.eMODULE_ZONE.BODY, module_sequence);

				if ( (ii % 25) == 0)
				{
					string buf = string.Format("Migrating record {0} of {1}", ii+1, dt.Rows.Count);
					System.Diagnostics.Trace.WriteLine(buf);
				}

			}

			if ( success_code == false)
			{
				Response.Write( "Migration FAILED");
			}
			else
			{
				Response.Write( "Migration SUCCEEDED");
			}
		}
	}
}
