<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="migrateProfile.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.migration.migrateProfile" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div id="divForm" runat="server">
    <p>Insert specific User's Id to update:<br />
      <asp:TextBox ID="txtUserId" runat="server"></asp:TextBox>
      <asp:Button ID="btnUpdateProfile" OnClick="UpdateProfile_Click"
       runat="server" Text="Update Profile" />
    </p>
    
    <p>Insert number of profiles to update:<br />
      <asp:TextBox ID="txtLimit" runat="server"></asp:TextBox>
      <asp:Button ID="btnUpdateManyProfiles" OnClick="UpdateProfileLimit_Click"
       runat="server" Text="Update Profiles" />
    </p>
    
    <p>
      <asp:Label ID="lblUpdateSequence" runat="server" ></asp:Label>
      <asp:Label ID="lblInsertNewHome" runat="server"></asp:Label>
    </p>
    </div>
    </form>
</body>
</html>
