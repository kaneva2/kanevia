<%@ Page language="c#" Codebehind="migrate20.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.migration.migrate20" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>migrate20</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
		

		<table width="990" align="center" cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td>
					<b>These are used to move users to the new database format and create default member channels. (Run 1,2,4)</b>
				</td>
			</tr>
			<tr>
				<td>
		
					<asp:Button id="PeopleHomePage"
						runat="server" Text="Migrate - Make default home page for users" CommandName="Migrate_Click"
						Width="583" Height="34" Enabled="False"></asp:Button>
					<asp:Button id="ChannelPages" Enabled="False"
						runat="server" Text="Migrate - Construct pages for channels" Width="583px" Height="34px"></asp:Button>
					<asp:Button id="CommunityImages" Enabled="False"
						runat="server" Text="Migrate - Community Images" Width="583px" Height="34px"></asp:Button>
					<asp:Button id="MultipleImages" Enabled="False"
						runat="server" Text="Migrate - Construct multiple photos module" Width="584px"></asp:Button>
				</td>
			</tr>
			<tr>
				<td>
					<br/><br/>
					<b>These are used to move images from database to the file system</b>
				</td>
			</tr>
			<tr>
				<td>
					<asp:Label runat="server" id="lblSettings"/><br/>
					<asp:Button id="AssetImages" Enabled="False"
						runat="server" Text="Migrate - Asset Images" Width="583px" Height="34px"></asp:Button>
						
					<asp:Button id="UserImages" Enabled="False"
						runat="server" Text="Migrate - Member Channel Photos" Width="583px" Height="34px"></asp:Button>
						
					<asp:Button id="ChannelThumbs" Enabled="False"
						runat="server" Text="Migrate - Broadcast Channel Thumbnails" Width="583px" Height="34px"></asp:Button>
						
					<asp:Button id="UserScreenshots" Enabled="False"
						runat="server" Text="Migrate - User Screenshots (user_images table)" Width="583px" Height="34px"></asp:Button>
				</td>
			</tr>
			
			<tr>
				<td>
					<br/><br/>
					<b>These are used to create thumbnail images</b>
				</td>
			</tr>
			<tr>
				<td>
					Image Server : <asp:Label id="lblImageServer" runat="server"/>
					<br/>
					<asp:Button id="UserThumbsGenerate"
						runat="server" Text="Generate User Thumbnails" Width="583px" Height="34px"></asp:Button>
						
					<asp:Button id="PhotoThumbsGenerate"
						runat="server" Text="Generate Photo Thumbnails (S,M,L)" Width="583px" Height="34px"></asp:Button>From asset Id <asp:TextBox id="txtStartId" runat="server" value="0" Width="50"/> to <asp:TextBox id="txtEndId" runat="server" value="50000" Width="50"/>
					
					<asp:Button id="PhotoThumbsGenerate2"
						runat="server" Text="Generate Photo Thumbnails (L,XL, ADetails)" Width="583px" Height="34px"></asp:Button>

					<asp:Button id="ChannelThumbsGenerate"
						runat="server" Text="Generate Channel Thumbnails" Width="583px" Height="34px"></asp:Button>
						
					<asp:Button id="MusicThumbsGenerate"
						runat="server" Text="Generate Music Thumbnails" Width="583px" Height="34px"></asp:Button>
						
					<asp:Button id="GamesThumbsGenerate"
						runat="server" Text="Generate Games Thumbnails" Width="583px" Height="34px"></asp:Button>
						
					<asp:Button id="VideoThumbsGenerate"
						runat="server" Text="Generate Video Thumbnails" Width="583px" Height="34px"></asp:Button>
					
					<asp:Button id="ContestProfileHeaderBannerGenerate"
						runat="server" Text="Generate Contest Profile Header Banner" Width="583px" Height="34px"></asp:Button>
						
					<asp:Button id="TitleBannerGenerate"
						runat="server" Text="Generate Title Banner" Width="583px" Height="34px"></asp:Button>							
				</td>
			</tr>
		</form>
	</body>
</HTML>
