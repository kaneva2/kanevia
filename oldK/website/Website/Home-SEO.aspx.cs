///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
    public partial class HomeSEO : System.Web.UI.Page
    {
        #region Declarations
        private int _configuration = 0;
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            //sets a custom CSS for this page
            ((homePage)Master).CustomCSS = "<link href=\"signIn_1-1/css/home.css\" rel=\"stylesheet\" type=\"text/css\" />";
            ((homePage)Master).CustomJavaScript = "<script type=\"text/javascript\" src=\"jscript/prototype-1.6.0.3.js\"></script>" + 
                "<script type=\"text/javascript\" src=\"signIn_1-1/js/swfobj2_0/swfobject.js\"><!-- External JS //--></script>";
            
            //configure the page for SEO optimization
            GetRequestParams();
            switch (_configuration)
            {
                case 1: //3D Virtual world
                    this.seo1.Attributes.Add("style", "display:block");
                    ((homePage)Master).Title = "Experience Kaneva�s Virtual People Games";
                    ((homePage)Master).MetaDataTitle = "<meta name=\"title\" content=\"Experience Kaneva�s Online 3D World\" />";
                    ((homePage)Master).MetaDataKeywords = "<meta name=\"keywords\" content=\"world, Online virtual world, 3-d virtual world, online worlds\" />";
                    ((homePage)Master).MetaDataDescription = "<meta name=\"description\" content=\"Kaneva�s 3d virtual world lets you customize your 3D avatar, decorate your free 3D home, and it provides an online virtual world experience like no other.\"/>";
                    break;
                case 2: //Virtual Life
                    this.seo2.Attributes.Add("style", "display:block");
                    ((homePage)Master).Title = "Kaneva | A Virtual Life";
                    ((homePage)Master).MetaDataTitle = "<meta name=\"title\" content=\"Kaneva | A Virtual Life\" />";
                    ((homePage)Master).MetaDataKeywords = "<meta name=\"keywords\" content=\"virtual life, create a virtual life, virtual life games, free virtual life games\" />";
                    ((homePage)Master).MetaDataDescription = "<meta name=\"description\" content=\"Create a virtual life. Chat with your friends and meet new people who share your interests.  \"/>";
                    break;
                case 3://Avatar
                    this.seo3.Attributes.Add("style", "display:block");
                    ((homePage)Master).Title = "Create Your Own Free Kaneva Avatar";
                    ((homePage)Master).MetaDataTitle = "<meta name=\"title\" content=\"Create Your Own Free Kaneva Avatar\" />";
                    ((homePage)Master).MetaDataKeywords = "<meta name=\"keywords\" content=\"create an avatar, free avatar creator, avatar worlds, avatar games, cool avatar\" />";
                    ((homePage)Master).MetaDataDescription = "<meta name=\"description\" content=\"Easily create an avatar that�s as distinct and stylish as you are.\"/>";
                    break;
                case 4://Virtual Games
                    this.seo4.Attributes.Add("style", "display:block");
                    ((homePage)Master).Title = "Experience Kaneva�s Virtual People Games";
                    ((homePage)Master).MetaDataTitle = "<meta name=\"title\" content=\"Experience Kaneva�s Virtual People Games\" />";
                    ((homePage)Master).MetaDataKeywords = "<meta name=\"keywords\" content=\"virtual games, virtual reality games, virtual online games, virtual people games\" />";
                    ((homePage)Master).MetaDataDescription = "<meta name=\"description\" content=\"More than a virtual reality game, you can experience an entire virtual world full of exciting people, places, and entertainment.\"/>";
                    break;
                case 5:// Create Your MMO Landing page
                    this.seo5.Attributes.Add("style", "display:block");
                    ((homePage)Master).Title = "Kaneva | Build Your Own MMO";
                    ((homePage)Master).MetaDataTitle = "<meta name=\"title\" content=\"Kaneva | Build Your Own MMO\" />";
                    ((homePage)Master).MetaDataKeywords = "<meta name=\"keywords\" content=\"play rpg games, free mmo, free online rpg, mmog\" />";
                    ((homePage)Master).MetaDataDescription = "<meta name=\"description\" content=\"The ultimate mmog, Kaneva is the hottest new trend in online social entertainment.\"/>";
                    break;
                case 6:// 3D chat Landing
                    this.seo6.Attributes.Add("style", "display:block");
                    ((homePage)Master).Title = "Kaneva�s 3D Animated Chat";
                    ((homePage)Master).MetaDataTitle = "<meta name=\"title\" content=\"Kaneva�s 3D Animated Chat\" />";
                    ((homePage)Master).MetaDataKeywords = "<meta name=\"keywords\" content=\"avatar chat, 3d chat world, chat room, animated chat, emote chat\" />";
                    ((homePage)Master).MetaDataDescription = "<meta name=\"description\" content=\"Dress up your Avatar and Chat with friends in 3D. Free!\"/>";
                    break;
                case 7:// online community
                    this.seo7.Attributes.Add("style", "display:block");
                    ((homePage)Master).Title = "Kaneva  | The Online Virtual Community";
                    ((homePage)Master).MetaDataTitle = "<meta name=\"title\" content=\"Kaneva  | The Online Virtual Community\" />";
                    ((homePage)Master).MetaDataKeywords = "<meta name=\"keywords\" content=\"best online community, online community, online virtual community, 3d community, create online community\" />";
                    ((homePage)Master).MetaDataDescription = "<meta name=\"description\" content=\"\"/>";
                    break;
                default:
                    this.seo.Attributes.Add("style", "display:block");
                    ((homePage)Master).Title = "Kaneva. Imagine What You Can Do.";
                    ((homePage)Master).MetaDataTitle = "<meta name=\"title\" content=\"Kaneva. A Unique Online Community and 3D Social Network.\" />";
                    ((homePage)Master).MetaDataKeywords = "<meta name=\"keywords\" content=\"free virtual world, virtual world, virtual worlds, 3D world, online avatar community, mmo game, virtual reality, caneva, kaniva\" />";
                    ((homePage)Master).MetaDataDescription = "<meta name=\"description\" content=\"Kaneva is the first to combine social networking and a free 3D virtual world. It's a whole new way to connect with friends. Have fun in a vibrant virtual world full of people, cool places and fun games. Create your avatar and get a free 3D home today!  \"/>";
                    break;
            }


            String strTestDNS;
            strTestDNS = Request.Url.Host + Request.Path;
            strTestDNS = strTestDNS.ToLower ();

            int communityId = 0;
            string commName = "";

            if (strTestDNS.EndsWith (".kaneva.com/default.aspx"))
            {
                strTestDNS = strTestDNS.Replace ("/default.aspx", "");
            }

            // Make sure they are trying to get to the home page
            // Test for DNS per channel
            if (strTestDNS.EndsWith (".kaneva.com"))
            {
                // Assume Channel Name is the first part before .kaneva.com
                commName = System.IO.Path.GetFileNameWithoutExtension (System.IO.Path.GetFileNameWithoutExtension (strTestDNS));
                // regular channel
                communityId = CommunityUtility.GetCommunityIdFromName (commName, false);

                if (communityId > 0)
                {
                    Response.Redirect (KanevaGlobals.GetBroadcastChannelUrl (commName));
                    // Issue with RewritePath, need to wait for IIS 7.0. (http://www.codecomments.com/archive289-2005-5-497785.html)
                    //Context.RewritePath ("~/community/CommInInfo.aspx", Context.Request.PathInfo, "ttCat=inCommunity&subtab=inf&communityId=" + communityId);
                    return;
                }
            }

        }

        #region Helper Functions

        //get all possible parameters
        private void GetRequestParams()
        {
            //check to see if there is a valid usercontrol id in URL
            try
            {
                _configuration = Convert.ToInt32(Request["seo"].ToString());
            }
            catch (Exception)
            {
            }
        }

        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
    }
}
