///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using System.Web.Security;
using System.Security.Principal;
using log4net;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for login.
	/// </summary>
	public class login : System.Web.UI.Page
	{
		#region Variables

		protected HtmlInputText	txtUserName;
		protected HtmlInputText	txtPassword;
		protected ImageButton	btnLogin;
		protected CheckBox		chkRememberLogin;

		// Logger
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

		#endregion

		private void Page_Load(object sender, System.EventArgs e)
		{
			if (!IsPostBack)
			{
				// Check to see if cookies are enabled, If they don't have cookies enabled, 
				// they cannot log in, redirect to noCookies page. 
				//
				// NOTE : Response.cookies and Request.cookies share same collection, 
				// so read the actual headers to see if cookies are enabled.
				string sCookieHeader = Page.Request.Headers["Cookie"];
				if ((null == sCookieHeader) || (sCookieHeader.IndexOf ("ASP.NET_SessionId").Equals (null)))
				{
					Response.Redirect (ResolveUrl ("~/noCookies.aspx"));
				}
			}

			string strJavascript = "<script language=\"JavaScript\">" +
				"function checkEnter(event){\n" +
				"if ((event.which && event.which == 13) || (event.keyCode && event.keyCode == 13))\n" +
                "{" + ClientScript.GetPostBackEventReference(this.btnLogin, "") + ";return false;}\n else \n{return true;}" +
				"}</script>";

            if (!ClientScript.IsClientScriptBlockRegistered(GetType (), "checkEnter"))
			{
                ClientScript.RegisterClientScriptBlock(GetType(), "checkEnter", strJavascript);	
			}
		}

		/// <summary>
		/// The login click event handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnLogin_Click (object sender, ImageClickEventArgs e) 
		{
			// Try to log in here
            UserFacade userFacade = new UserFacade();
			int roleMembership = 0;
			string email = Server.HtmlEncode (txtUserName.Value);
			string password = Server.HtmlEncode (txtPassword.Value);

			// May want to persist login info later.
			bool bPersistLogin = chkRememberLogin.Checked;

			int validLogin = UsersUtility.Authorize (email, password, 0, ref roleMembership, Common.GetVisitorIPAddress(), true);

			string results = "";

			switch (validLogin)       
			{       
				case 0:  
					results = "Not authenticated.";
					break; 
				case (int) Constants.eLOGIN_RESULTS.NOT_VALIDATED:
					LoginUser (email, bPersistLogin);
					//					results = "You must first validate your account.";
					//					SetFocus (txtUserName);
					break;    
				case (int) Constants.eLOGIN_RESULTS.SUCCESS:  
					LoginUser (email, bPersistLogin);
					break;   
				case (int) Constants.eLOGIN_RESULTS.USER_NOT_FOUND:
                    userFacade.InsertUserLoginIssue(0, Common.GetVisitorIPAddress(), "Email " + email + " not found", "INVALID_EMAIL", "WEB");
					results = "Email '" + email + "' was not found.";
					SetFocus (txtUserName);
					break;                  
				case (int) Constants.eLOGIN_RESULTS.INVALID_PASSWORD:  
				{
                    userFacade.InsertUserLoginIssue(0, Common.GetVisitorIPAddress(), "Invalid password for email " + email, "INVALID_PASSWORD", "WEB");
					m_logger.Warn ("Failed login (invalid password) for email '" + email + "' from IP " + Common.GetVisitorIPAddress());
                    userFacade.UpdateFailedLogins(email);
					results = "Invalid password.";
					SetFocus (txtPassword);
					break;
				}
				case (int) Constants.eLOGIN_RESULTS.NO_GAME_ACCESS:   
					results = "No access to this game.";
					break;      
				case (int) Constants.eLOGIN_RESULTS.ACCOUNT_DELETED:
					results = "This account has been deleted.";
					SetFocus (txtUserName);
					break;    
				case (int) Constants.eLOGIN_RESULTS.ACCOUNT_LOCKED:
				{
                    userFacade.InsertUserLoginIssue(0, Common.GetVisitorIPAddress(), "Locked account username '" + email + "' tried to log in", "BANNED", "WEB");
					m_logger.Warn ("Locked account " + email + " tried to log in from IP " + Common.GetVisitorIPAddress());
					results = "This account has been locked by the Kaneva administrator";
					SetFocus (txtUserName);
					break; 
				}
				default:            
					results = "Not authenticated.";           
					break;      
			}

			// Did they fail login?
			if (results.Length > 0)
			{
				// Show an alert
				string strScript = "<script language=JavaScript>";
				strScript += "alert(\"Login Failed: " + results + "\");";
				strScript += "</script>";

                if (!ClientScript.IsStartupScriptRegistered(GetType(), "invalidLogin"))
				{
                    ClientScript.RegisterStartupScript(GetType(), "invalidLogin", strScript);
				}
			}
		}

		/// <summary>
		/// LoginUser
		/// </summary>
		private void LoginUser (string email, bool bPersistLogin)
		{
            UserFacade userFacade = new UserFacade();
			DataRow drUser = UsersUtility.GetUserFromEmail (email);
			int userId = Convert.ToInt32 (drUser ["user_id"]);

			FormsAuthentication.SetAuthCookie (drUser ["userName"].ToString (), bPersistLogin);
            int loginId = userFacade.UpdateLastLogin(userId, Common.GetVisitorIPAddress(), Server.MachineName);

      // Now take their current IP and and update their cookie.
      Response.Cookies["ipcheck"].Value = Common.GetVisitorIPAddress();  
      
      // Set the userId in the session for keeping track of current users online
			Session ["userId"] = userId;
            Session["loginId"] = loginId;

			string url = ResolveUrl ("~/loginhelper.aspx?action=login");

			Response.Clear();
			Response.AddHeader("Refresh", string.Concat ("0;URL=", url));
			Response.Write ("<script language=\"javascript\">window.location.replace(\"");
			Response.Write (url);
			Response.Write ("\");</script>");
			Response.End ();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		
	}
}
