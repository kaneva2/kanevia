///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using log4net;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for commercialLicenseAuth.
	/// </summary>
	public class commercialLicenseAuth : MainTemplatePage
	{
		
		protected commercialLicenseAuth () 
		{
			Title = "Confirm Commercial License Information";
		}
		/// <summary>
		/// Page_Load
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Page_Load (object sender, System.EventArgs e)
		{
			// They must be logged in
			if (!Request.IsAuthenticated)
			{
				Response.Redirect (GetLoginURL ());
				return;
			}

			// Clear any previous errors
			lblErrors.Text = "";
			trError.Visible = false;

			Response.Expires = 0;
			Response.CacheControl = "no-cache";

			if (!IsPostBack)
			{
				lblHeading.Text = "Confirm your information below and click 'Authorize Purchase'";
				DisplayScreen ();
			}

			// Set up purchase button
			btnMakePurchase.CausesValidation = false;
			btnMakePurchase.Attributes.Add ("onclick", "javascript: if (typeof(Page_ClientValidate) == 'function') if (!Page_ClientValidate()){return false;};this.value='  Please wait...  ';this.disabled = true;ShowLoading(true,'Please wait. Your order is being submitted...');" + ClientScript.GetPostBackEventReference(this.btnMakePurchase, "", false));
		}

		/// <summary>
		/// DisplayScreen
		/// </summary>
		private void DisplayScreen ()
		{
			int userId = GetUserId ();

			// Get the order
			int orderId = Convert.ToInt32 (Session ["orderId"]);
			DataRow drOrder = StoreUtility.GetOrder (orderId);

			int purchaseType = Convert.ToInt32 (drOrder ["purchase_type"]);

			// Where any K-Points purchased?
			int pointTransactionId = 0;

			// Get the billing info
			pointTransactionId = Convert.ToInt32 (drOrder ["point_transaction_id"]);
			DataRow drPpt = StoreUtility.GetPurchasePointTransaction (pointTransactionId);

			// Show the credit card and billing address
			trBillingInfo.Visible = true;

			// What is the payment method?
			int paymentMethodId = Convert.ToInt32 (drPpt ["payment_method_id"]);

			// Credit card
			int billingInfoId = Convert.ToInt32 (drPpt ["billing_information_id"]);
			DataRow drCreditCard = UsersUtility.GetCreditCard (userId, billingInfoId);
			lblCreditInfo.Text = drCreditCard ["name_on_card"].ToString () + "<br>" +
				drCreditCard ["card_type"].ToString () + ":" + ShowCreditCardNumber (drCreditCard ["card_number"].ToString ()) + "<br>" +
				"Exp: " + drCreditCard ["exp_month"].ToString () + "/" + drCreditCard ["exp_year"].ToString () + "<BR>";

			// Address
			int addressId = Convert.ToInt32 (drPpt ["address_id"]);
			DataRow drAddress = UsersUtility.GetAddress (userId, addressId);
			lblAddress.Text = drAddress ["name"].ToString () + "<BR>" +
				drAddress ["address1"].ToString () + "<BR>";;
			if (!drAddress ["address2"].Equals (DBNull.Value) && drAddress ["address2"].ToString ().Length > 0)
			{
				lblAddress.Text += drAddress ["address2"].ToString () + "<BR>";
			}
			lblAddress.Text += drAddress ["city"].ToString () + "," + drAddress ["state_name"].ToString () + " " + drAddress ["zip_code"].ToString () + "<BR>" +
				drAddress ["country_name"].ToString () + "<BR>";

		}


		/// <summary>
		/// The make purchase event handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnMakePurchase_Click (object sender, EventArgs e) 
		{			
			int orderId = Convert.ToInt32 (Session ["orderId"]);
			int assetId = 0;
			int userId = GetUserId ();
			string userErrorMessage = "";

			DataRow drOrder = StoreUtility.GetOrder (orderId);
			int purchaseType = Convert.ToInt32	(drOrder ["purchase_type"]);

			// They are purchasing k-Points :)
			int pointTransactionId = Convert.ToInt32 (drOrder ["point_transaction_id"]);
			DataRow drPpt = StoreUtility.GetPurchasePointTransaction (pointTransactionId);

			// Is it PayPal?
			int paymentMethodId = Convert.ToInt32 (drPpt ["payment_method_id"]);

			int orderBillingInformationId = 0;
			int billingInformationId = 0; 
			int addressId = 0;
			
			// Did we already copy the billing information over to history?
			if (!drPpt ["order_billing_information_id"].Equals (DBNull.Value))
			{
				orderBillingInformationId = Convert.ToInt32 (drPpt ["order_billing_information_id"]);
			}

			// Payment info and address are required here for Cybersource transactions
			if (drPpt ["address_id"].Equals (DBNull.Value) || drPpt ["billing_information_id"].Equals (DBNull.Value))
			{
				ShowMessage ("Please correct the following errors:<br><br>Credit card and billing address are required to continue this purchase");
				return;
			}

			billingInformationId = Convert.ToInt32 (drPpt ["billing_information_id"]); 
			addressId = Convert.ToInt32 (drPpt ["address_id"]);

			// Copy payment/billing address to order history
			orderBillingInformationId = StoreUtility.CopyBillingInfoToOrder (orderBillingInformationId, billingInformationId, addressId);

			// Update the purchase_transaction order_billing_id
			StoreUtility.UpdatePointTransactionOrderBillingInfoId (pointTransactionId, orderBillingInformationId);

			// Get it again to update the orderBillingInformationId to be used in the next call
			drPpt = StoreUtility.GetPurchasePointTransaction (pointTransactionId);

			PagedDataTable pdtOrderItems = StoreUtility.GetOrderItems (orderId, "");
			DataRow drOrderItem = pdtOrderItems.Rows [0];

			int gameLicenseId = Convert.ToInt32 (drOrderItem ["game_license_id"]);
			assetId = Convert.ToInt32 (drOrderItem ["asset_id"]);

			// CyberSource
			if (!MakeCyberSourcePayment (drOrder, drPpt, ref userErrorMessage))
			{
				// Update status to failed auth
				StoreUtility.UpdateGameLicense (gameLicenseId, assetId, userId, Constants.eGAME_LICENSE_STATUS.FAILED_PAYMENT_AUTHORIZATION);

				// Failure
				ShowMessage ("Please correct the following errors:<br><br>" + userErrorMessage);
				return;
			}
			else
			{
				// Success
				StoreUtility.UpdateOrderPurchaseDate (orderId);

				StoreUtility.UpdateGameLicense (gameLicenseId, assetId, userId, Constants.eGAME_LICENSE_STATUS.WAITING_ON_IT);

				string ITEmail = "cbuchanan@kaneva.com";

                if (System.Configuration.ConfigurationManager.AppSettings["ITEmail"] != null)
				{
                    ITEmail = System.Configuration.ConfigurationManager.AppSettings["ITEmail"].ToString();
				}

				// Send an email to IT
				MailUtility.SendEmail (KanevaGlobals.FromEmail, ITEmail, "New Commercial Server", "gameLicenseId = " + gameLicenseId + " is waiting on IT setup", false); 

				// Send an email to the purchaser
				MailUtility.SendAuthorizeGameServer (userId, orderId, Convert.ToDouble (drPpt ["amount_debited"]), assetId); 
			}
		
			RedirectToDownload (orderId, assetId, purchaseType);
		}

		/// <summary>
		/// Redirect to download screen
		/// </summary>
		/// <param name="assetId"></param>
		private void RedirectToDownload (int orderId, int assetId, int purchaseType)
		{
			Response.Redirect (ResolveUrl ("~/asset/publishedGames.aspx"));
		}

		/// <summary>
		/// MakeCyberSourcePayment
		/// </summary>
		/// <param name="drOrder"></param>
		/// <param name="drPpt"></param>
		/// <returns></returns>
		private bool MakeCyberSourcePayment (DataRow drOrder, DataRow drPpt, ref string userErrorMessage)
		{
			//bool bSystemDown = false;
			CybersourceAdaptor cybersourceAdaptor = new CybersourceAdaptor ();
			//return cybersourceAdaptor.AuthorizePayment (drOrder, drPpt, Request.UserHostAddress, ref bSystemDown, ref userErrorMessage);
			return false;
		}   // *** Will need to update to pass cc # if we use this page again ***

		/// <summary>
		/// ShowMessage
		/// </summary>
		/// <param name="strMessage"></param>
		private void ShowMessage (string strMessage)
		{
			lblErrors.Text = strMessage;
			trError.Visible = true;
		}

		protected Button btnMakePurchase;

		protected HtmlTableRow trBillingInfo, trError;

		// Credit/Address info
		protected Label lblCreditInfo, lblAddress;

		protected Label lblHeading, lblErrors;

		// Logger
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

		#region Web Form Designer generated code

		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
