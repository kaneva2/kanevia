<%@ Page language="c#" Codebehind="commercialLicenseAuth.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.commercialLicenseAuth" %>

<%@ Register TagPrefix="Kaneva" TagName="HeaderText" Src="../usercontrols/HeaderText.ascx" %>
<%@ Register TagPrefix="Kaneva" Namespace="KlausEnt.KEP.Kaneva.WebControls" Assembly="Kaneva.WebControls" %>

<Kaneva:HeaderText runat="server" Text="Authorization" />
<asp:PlaceHolder id="phBreadCrumb" runat="server"/>
			
<asp:ValidationSummary ShowMessageBox="True" ShowSummary="False" Class="formError" id="valSum" DisplayMode="BulletList" runat="server" HeaderText="Please correct the following errors:"/>

<div ID="divLoading">
	<table BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="100%" HEIGHT="300px">
		<tr><th CLASS="loadingText" ID="divLoadingText">Loading...</th></tr>
	</table>
</div>

<div id="divData" style="DISPLAY:none">
<center>
<table runat="server" cellpadding="0" cellspacing="0" border="0" width="730">
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="1" height="200"/></td>
		<td height="200" class="dateStamp" id="tdLoginBG" align="left" width="730" valign="top"><br>
			<span class="orangeHeader2"><asp:Label runat="server" id="lblHeading"/></span><br><br>
	<center>
	<table cellpadding="2" cellspacing="0" border="0" width="730">
		<tr id="trError" runat="server">
			<td class="messageBox">
				<BR>
				<asp:Label CssClass="formErrorLarge" id="lblErrors" runat="server"/>
				<BR>
			</td>
		</tr>
		<tr>
			<td class="bodyText" align="left" valign="middle">
				<br>
				Kaneva will need to Authorize your request. Your card will be authorized for the amount of $200.00. Your
				card will not be charged at this time, you will not be charged anything until your commercial server has been
				installed.<br><br>
			</td>
		</tr>
		</table>
		</center>
		<BR>
		<BR>
		<center>
		<table cellpadding="2" cellspacing="0" border="0" width="730">
			<tr>
				<td colspan="4" class="header02" bgcolor="#999999">Payment Summary
				</td>
			</tr>
			<tr id="trBillingInfo" runat="server">	
				<td><img runat="server" src="~/images/spacer.gif" width="10" height="10" border="0"/></td>							
				<td align="left" valign="top" class="bodyText">
					<span class="dateStamp"><B>Payment Method:</B></span>
					<br/>
					<asp:Label id="lblCreditInfo" runat="server"></asp:Label>
					<a CssClass="mainCats" runat="server" href="~/checkout/paymentSelection.aspx"><img runat="server" src="~/images/button_change.gif" border="0"></a>
					<BR><BR>
				</td>
				<td align="left" valign="top" class="bodyText">
					<span class="dateStamp"><B>Billing Address:</B></span><br>
					<asp:Label id="lblAddress" runat="server">
					</asp:Label>
					<a CssClass="mainCats" runat="server" href="~/checkout/billingInfo.aspx"><img runat="server" src="~/images/button_change.gif" border="0"></a>
				</td>
				<td><img runat="server" src="~/images/spacer.gif" width="10" height="10" border="0"/></td>
			</tr>
			<tr>
				<td colspan="4" bgcolor="#ededed" align="right"><br>
					<asp:button id="btnMakePurchase" runat="Server" CausesValidation="False" onClick="btnMakePurchase_Click" class="Filter2" Text=" authorize purchase "/><img runat="server" src="~/images/spacer.gif" width="10" height="10" ID="Img1"/><br>
				</td>
			</tr>
			<tr>
				<td colspan="4">			
				</td>
			</tr>

		</table>
</center>	

	</td>
	</tr>


</table>
</center>

</div><br><br>
