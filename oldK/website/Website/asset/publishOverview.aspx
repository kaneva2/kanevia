<%@ Page language="c#" Codebehind="publishOverview.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.publishOverview" %>
<%@ Register TagPrefix="Kaneva" TagName="HeaderText" Src="../usercontrols/HeaderText.ascx" %>

<Kaneva:HeaderText runat="server" Text="Get published with Kaneva!"/>

<center>
<table cellSpacing="0" cellPadding="0" border="0" width="750">
<tr>
	<td align="center">
	<a runat="server" href="~/asset/kanevaMediaPublisher.aspx?ttCat=publish&subtab=pea"><img runat="server" src="~/images/PublishVS.gif" border="0"/></a>
	</td>
</tr>
</table><br>
<table cellSpacing="0" cellPadding="0" border="0" width="710" class="bodyText">
<tr>
	<td><span class="bodyBold" style="font-size:18px;line-height:22px;">Why get published with Kaneva?</span><br><br>
	</td>
</tr>
<tr>
	<td align="left">
	<span style="color:#FF7200; font-size:18px;">1. Democratizing online entertainment for everyone.</span><br>
	<span style="padding-left:26px;">Kaneva is opening the door to all filmmakers and game developers providing an 
accessible, immediate<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; outlet for their work.</span><br><br>
	</td>
</tr>
<tr>
	<td align="left">
	<span style="color:#FF7200; font-size:18px;">2. Get directly connected to a rapidly growing, global audience.</span><br>
	<span style="padding-left:26px;">Anyone can create a community to reach their large audience or micro-audiences<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; around the world. Communities provide for communities to grow and participate around your media and<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; interactive content.</span><br><br>
	</td>
</tr>
<tr>
	<td align="left">
	<span style="color:#FF7200; font-size:18px;">3. Publish items on Kaneva immediately with a few easy steps.</span><br>
	<span style="padding-left:26px;">With Kaneva, there are simple and easy steps to get your items published and available to the world.</span><br><br>
	</td>
</tr>
<tr>
	<td align="left">
	<span style="color:#FF7200; font-size:18px;">4. Maintain all of your rights for items published and sold on Kaneva.</span><br>
	<span style="padding-left:26px;">All items published through Kaneva allow their creators to keep 100% of their rights and is completely non-exclusive.</span><br><br>
	</td>
</tr>
<tr>
	<td align="left">
	<span style="color:#FF7200; font-size:18px;">5. Free, unlimited uploads and storage of content.</span><br>
	<span style="padding-left:26px;">Communities and unlimited uploads are free to all Kaneva members.</span><br><br>
	</td>
</tr>
<tr>
	<td align="left">
	<span style="color:#FF7200; font-size:18px;">6. Sell items for the price you set and keep half or more of the money earned.</span><br>
	<span style="padding-left:26px;">Sell your items for the price you set, and earn substantial royalties on your sales.</span><br><br>
	</td>
</tr>
<tr>
	<td align="left">
	<span style="color:#FF7200; font-size:18px;">7. We are your one-stop-shop.</span><br>
	<span style="padding-left:26px;">Kaneva goes beyond online publishing to provide the business services, support 
and enabling technology needed to<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; reach your goals.</span><br><br>
	</td>
</tr>
<tr>
	<td align="right">
		<a runat="server" href="~/asset/publishValue.aspx" class="bodyBold">Learn More</a><br><br>
	</td>
</tr>
<tr>
	<td><span class="bodyBold" style="font-size:18px;line-height:22px;">How do you get published on Kaneva?</span><br><br>
	</td>
</tr>
<tr>
	<td align="left">
	<span style="color:#FF7200; font-size:18px;">1. Join or Sign In</span><br>
	<span style="padding-left:26px;"><a runat="server" id="joinLink">Sign up</a> to be a registered Kaneva member or if you have already registered, <a runat="server" href="~/loginSecure.aspx" ID="A1">sign in</a></span>.<br><br>
	</td>
</tr>
<tr>
	<td align="left">
	<span style="color:#FF7200; font-size:18px;">2. Upload</span><br>
	<span style="padding-left:26px;">Upload your content for free in just 3 easy <a runat="server" href="~/asset/kanevaMediaPublisher.aspx?ttCat=publish&subtab=pea">steps</a>.</span> <br><br>
	</td>
</tr>
<tr>
	<td align="left">
	<span style="color:#FF7200; font-size:18px;">3. Set up your Community</span><br>
	<span style="padding-left:26px;">List your item by quickly creating your own <a runat="server" href="~/community/channel.kaneva">Community</a>, describe and tag your item, determine price and approve to<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; publish and start selling immediately.</span> <br><br>
	</td>
</tr>
<tr>
	<td align="left">
	<span style="color:#FF7200; font-size:18px;">4. Get Paid</span><br>
	<span style="padding-left:26px;">Receive payment each time your items are sold!</span> <br><br>
	</td>
</tr>
</table>

<br><br><br>
</center>