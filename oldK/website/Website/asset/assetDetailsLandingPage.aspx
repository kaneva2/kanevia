<%@ Page language="c#" MasterPageFile="~/MASTERPAGES/GenericLandingPageTemplate.Master" Codebehind="assetDetailsLandingPage.aspx.cs" AutoEventWireup="false" validateRequest="false" Inherits="KlausEnt.KEP.Kaneva.assetDetailsLandingPage" %>
<%@ Register TagPrefix="UserControl" TagName="RelatedItemsView" Src="../usercontrols/RelatedItemsView.ascx" %>
<%@ Register TagPrefix="UserControl" TagName="AssetComments" Src="../usercontrols/Comments.ascx" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>
<%@ Register TagPrefix="UserControl" TagName="LightBox" Src="../usercontrols/LightBox.ascx" %>

<asp:Content ID="cntHeaderCSS" runat="server" contentplaceholderid="cphHeadCSS" >
    <link href="../css/new.css" rel="stylesheet" type="text/css"/>
    <link href="../css/landing_page/media/details.css" rel="stylesheet" type="text/css"/>
</asp:Content>

<asp:Content ID="cntHeaderJS" runat="server" contentplaceholderid="cphHeadJS" >
    <script type="text/javascript" src="../jscript/kaneva.js"></script>
    <script type="text/javascript" src="../jscript/SWFObject/swfobject.js"></script>
    <usercontrol:lightbox runat="server" id="ucLightBox" />
    <script type="text/javascript">
        function assetFullScreen(url, name) {
            window.open(url, "test", "toolbar=no,location=no,status=no,resizable=yes,width=1024,height=768,titlebar=yes,menubar=no,scrollbars=no");
        }
    </script>  
    <script type="text/javascript">
        $j(document).ready(function () {
            popFacebox = function () {
                $j.facebox({ iframe: '../buyRave.aspx?assetId=<asp:literal id="litAssetId" runat="server"></asp:literal>' }, '', 360);
            };
        });
        var raveCountCtrlName = '<asp:literal id="litCtrlRaveCount" runat="server"></asp:literal>'; 
    </script>
</asp:Content>

<asp:Content ID="cntHeader" runat="server" contentplaceholderid="cphHeader" >
    <div class="headerLinks">
        <a id="aJoinToday" runat="server" href="javascript:void(0);">Join Today</a>&nbsp;|&nbsp;<span class="showSignIn">Sign In</span>
    </div>
</asp:Content>

<asp:Content id="cntBody" runat="server" contentplaceholderid="cphBody" >
<!-- ACCESS MESSAGE -->
<div id="divAccessMessage" class="accessMessageWrapper" runat="server" visible="false">
	<div class="accessMessageImage">
        <h2 id="h2Title" runat="server"></h2>
	    <img src="../images/access_pass.png" />
    </div>
    <div class="accessMessageText">
	    <p id="pActionText" runat="server"></p>
	    <asp:button id="btnAction" runat="server"></asp:button>
	    <p id="pPolicyText" runat="server"></p>		
    </div>					
</div>
													
<div id="divDetails" runat="server" visible="false" class="assetPageWrapper">
	
	<!-- ABOUT MEDIA -->
    <div class="leftColumn">
        <h1 id="h1AssetTitle" runat="server" class="assetTitle"></h1>
        <div class="module">

            <!-- MEDIA-PLAYER / IMAGE PLACEHOLDER -->
            <div class="mediaPlayer">
	            <asp:placeholder runat="server" id="phStream"/>
	        </div>
		
            <!-- TOOLBAR -->														
	        <ajax:ajaxpanel id="ajpActionBar" runat="server">	
                <div class="mediaToolbar">														
		            <div runat="server" id="divNewWindow" >																			  
			            <ul id="mediaFullScreen">
                            <li id="full"></li>
			            </ul>
		            </div>																			  																							   
		            <ul id="mediaNav">													    
			            <li id="rave" runat="server" class="rave"><asp:linkbutton id="lbRave" visible="true" runat="server" tooltip="Rave this Item"></asp:linkbutton></li>
			            <li id="share"><asp:linkbutton id="lb_link_1" visible="true" runat="server" tooltip="E-mail this, post to del.icio.us, etc."></asp:linkbutton></li>
			            <li id="fullscreen"><asp:linkbutton id="lbFullScreen"  visible="true" runat="server" tooltip="Play In Fullscreen Mode!"></asp:linkbutton></li>
			            <li id="report"><asp:linkbutton id="lbReport" visible="true" runat="server" tooltip="Report Abuse"></asp:linkbutton></li>
		            </ul>
                </div>	
            </ajax:ajaxpanel>														
																														
		    <!-- OWNER AVATAR -->
            <div class="detailContainer">
                <div class="subColumn">
		            <div class="framesize-small">
			            <div class="restricted" visible="false" runat="server" id="divRestricted"></div>
				        <div class="frame">
					        <span class="ct"><span class="cl"></span></span>							
						        <div class="imgconstrain">
							        <img runat="server" id="imgAssetThumbnail" src="~/images/KanevaIcon01.gif" border="0" />
                                </div>																																				
					        <span class="cb"><span class="cl"></span></span>
				        </div>
			        </div>
                </div>
                <div class="subColumn">
			        <h2><asp:label id="lblName" runat="server"/></h2>
                    <asp:label id="lblDescription" runat="server"/>
                </div>
            </div>

            <!-- Instructions -->
		    <div runat="server" id="divInstructions" class="detailContainer" visible="false">
			    <span class="subColumnLabel"><strong>Instructions:</strong></span>
                <div class="subColumn">
			        <div id="divInstructionText" runat="server" class="fakeTextBox"></div>
                </div>
		    </div>

            <!-- Media Stats -->
		    <div class="detailContainer">
			    <span class="subColumnLabel"><strong>Media stats:</strong></span>
			    <ul class="subColumn">
				    <li>
					    Views: <asp:label id="lblNumViews" runat="server" /><br/>
					    Raves: <ajax:ajaxpanel id="ajpRaves" runat="server"><asp:label id="lblRaves" runat="server" text="0"/></ajax:ajaxpanel><br/>
					    Added: <asp:label id="lblNumTimesAdded" runat="server" /><br/>
					    Comments: <asp:label id="lblNumComments" runat="server" /><br/>
					    <asp:label id="lblCompanyName" runat="server" /><br/>
					    <span id="spnRuntime" runat="server" visible="false">Runtime: <asp:label id="lblRuntime" runat="server"/><br/></span>
					    Uploaded: <asp:label id="lblCreatedDate" runat="server"/>
                    </li>
			    </ul>
		    </div>	

            <!-- Shared By -->
            <div class="detailContainer">
                <span class="subColumnLabel"><strong>Shared by:</strong></span>
                <div class="subColumn">
                    <a id="aUserName" runat="server" style="line-height:16px;"></a><br />
				    <div class="framesize-xsmall">
					    <div class="restricted" visible="false" runat="server" id="divOwnerRestricted"></div>
					    <div class="frame">
						    <span class="ct"><span class="cl"></span></span>
						    <div class="imgconstrain">
							    <a class="bodytext" id="aUserName3" border="0" runat="server"><img runat="server" id="imgAvatar" src="~/images/KanevaIcon01.gif" border="0" /></a>
                            </div>
						    <span class="cb"><span class="cl"></span></span>
					    </div>
				    </div>
			    </div>
		    </div>	 

            <!-- Share This -->
		    <div class="detailContainer">
			    <span class="subColumnLabel"><strong>Share this:</strong></span>
                <div class="subColumn">
			        <div id="divShare" runat="server" class="fakeTextBox"></div>
                    <br />
			        <span class="note">Use this when you want a hyperlink. Good for email and when you can't embed the media player.</span>
		        </div>
            </div>
		    
            <!-- Photo Embed -->
		    <div class="detailContainer">
			    <div class="subColumnLabel"><strong><div id="divEmbedded" runat="server">Embed this:</div></strong></div>
                <div class="subColumn">
			        <div id="divEmbed" class="fakeTextBox" runat="server"></div>
                    <br />
			        <span class="note">Add this media to your Kaneva profile, community, web site or blog.</span>
		        </div>
            </div>
																	
		    <!-- Photo URL -->														
		    <div id="divPhotoURL" runat="server" class="detailContainer" visible="false">
			    <span class="subColumnLabel"><strong>Photo URL:</strong></span>
                <div class="subColumn">
			        <div id="divRawEmbed" class="fakeTextBox" runat="server"></div>
                    <br />
			        <span class="note">Use this photo url for backgrounds.</span>
                </div>
		    </div>
																	
            <!-- Related By Topic -->	
		    <div class="detailContainer">
			    <span class="subColumnLabel"><strong>Related <span id="spnRelatedType" runat="server">videos</span> by topic:</strong></span>
			    <div class="subColumn"><asp:label id="lblRelatedByTopic" runat="server"/></div>
		    </div>

            <!-- Added By -->	
		    <div class="detailContainer">
			    <span class="subColumnLabel"><strong>Added by:</strong></span>
                <div class="subColumn">
				    <asp:label id="lblAddedBy" runat="server"/> added this to their library.
				    <div>
					    <asp:label id="lblChannels" runat="server"/>
					    <span id="spnAdders" runat="server" class="addedBy"></span>
				    </div>
                </div>
		    </div>

            <!-- License -->
		    <div class="detailContainer" style="margin-bottom:0;">
			    <span class="subColumnLabel"><strong>License:</strong></span>
			    <div class="subColumn"><asp:Label id="lblLicense" runat="server"></asp:Label></div>
		    </div>
        </div>
    </div>

    <div class="rightColumn">

        <!--START UPSELL-->
        <div class="promiseContainer">		
            <h2 id="h2UpsellPromise" runat="server"></h2>											
		    <div id="divUpsellImageWrapper" runat="server" visible="false" class="upsellImageWrapper">
                <div id="divUpsellImage" runat="server" class="upsellImage"></div>
            </div>
            
            <a id="aJoinNow" runat="server" class="joinNow"></a>
            <a id="aItsFree" runat="server">-It's Free!</a>										
	    </div>	
        <!--END UPSELL-->
														
	    <!--START RELATED MEDIA-->
	    <div id="related_media">													
		    <usercontrol:relateditemsview runat="server" id="ucRelatedItems" />												
	    </div>																
	    <!--END RELATED MEDIA-->
																									
        <!--START COMMENTS-->
	        <div class="module whitebg">
		        <usercontrol:assetcomments runat="server" id="ucComments" />
	        </div>
        <!--END COMMENTS-->

    </div>
</div>
</asp:Content>	
																
<asp:Content id="cntFooter" runat="server" contentplaceholderid="cphFooter" >
    <!--[if lte IE 8]><img class="footerRuleImage" src="../../images/landing_page/hr_917x2_footerBreak.jpg" alt="" width="923px" height="2px"/><![endif]-->
    <hr class="footerRuleTop" />
    <hr class="footerRuleBottom" />
</asp:Content>