///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Collections.Specialized;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using MagicAjax;
using log4net;

using KlausEnt.KEP.Kaneva.usercontrols;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for assetDetails.
	/// </summary>
	public class assetDetails : StoreBasePage
	{
		/// <summary>
		/// Page_Load
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Page_Load(object sender, System.EventArgs e)
		{
            // If we're not authenticated we need to be on the other asset page
            if (Request.IsAuthenticated)
            {
                if (Request["assetId"] == null)
                {
                    Response.Redirect("~/free-virtual-world.kaneva");
                }

                DataRow drAsset = null;

                try
                {
                    int assetId = Convert.ToInt32(Request["assetId"]);
                    drAsset = StoreUtility.GetAsset(assetId, false, GetUserId());
                }
                catch (Exception ex)
                {
                    m_logger.Error("Error loading asset details page: ", ex);
                    Response.Redirect("~/free-virtual-world.kaneva");
                }

                if (drAsset == null || drAsset["asset_id"].Equals(DBNull.Value) || drAsset["status_id"].Equals((int)Constants.eASSET_STATUS.MARKED_FOR_DELETION) || drAsset["status_id"].Equals((int)Constants.eASSET_STATUS.DELETED))
                {
                    Response.Redirect(ResolveUrl("~/asset/notAvailable.aspx?M=NA"));
                }

                AssetOwnerId = Convert.ToInt32(drAsset["user_id"]);
                int assetTypeId = Convert.ToInt32(drAsset["asset_type_id"]);
                string assetType = KanevaWebGlobals.GetAssetTypeName(assetTypeId);
                if ((int)Constants.eASSET_TYPE.VIDEO == assetTypeId)
                {
                    spnRuntime.Visible = true;
                }

                bool show_page = true;
                bool blockMature = false;

                // Are they the owner or admin?
                if (IsAdministrator())
                {
                    _canEdit = true;

                    if (AssetOwnerId.Equals(KanevaWebGlobals.CurrentUser.UserId))
                    {
                        HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
                        HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.MEDIA_UPLOADS;
                        HeaderNav.SetNavVisible(HeaderNav.MyKanevaNav, 2);
                    }
                    else
                    {
                        HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.PLAY;
                    }
                }
                if (AssetOwnerId.Equals(GetUserId()))
                {
                    _canEdit = true;

                    HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
                    HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.MEDIA_UPLOADS;
                    HeaderNav.SetNavVisible(HeaderNav.MyKanevaNav, 2);
                }
                else
                {
                    HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.PLAY;
                }

                //assumes that all minors are set to don't show mature
                if (!KanevaWebGlobals.CurrentUser.HasAccessPass && IsMature(Convert.ToInt32(drAsset["asset_rating_id"])) && !_canEdit)
                {
                    blockMature = true;
                }

                // They are not the asset owner
                if (!AssetOwnerId.Equals(GetUserId()))
                {
                    // if item is to be blocked due to mature settings enter here
                    if (blockMature)
                    {
                        show_page = false;
                        if (!Request.IsAuthenticated)
                        {
                            h2Title.InnerText = "This " + assetType + " is Restricted";

                            tdActionText.InnerHtml = "The " + assetType + " you are trying to access has been set to \"Restricted\" by its " +
                            "owner and may contain material inappropriate for anyone under the age of 18. Please log in to verify your right " +
                            "to access this page.";

                            tdPolicyText.InnerHtml = COMMUNITY_GUIDELINES;
                            tdPolicyText.Visible = false;

                            btnAction.Text = " Sign in ";
                            btnAction.Attributes.Add("onclick", "location.href='" + GetLoginURL() + "';return false;");

                            divAccessMessage.Visible = true;
                        }
                        else if (KanevaWebGlobals.CurrentUser.IsAdult)   // user is 18 or over
                        {

                            h2Title.InnerText = "This " + assetType + " can only be seen by people with an Access Pass";

                            tdActionText.InnerHtml = "The " + assetType + " has taken advantage of our Access Pass subscription " +
                                "and has set this " + assetType + " so that it can only be seen by other people with Access Pass. " +
                                "<br/><br/>What are you waiting for? Get Yours now. <br />";
                            tdActionText2.InnerHtml = "Kaneva's Access Pass includes: <br /><ul><li style=\"line-height:11px; font-size:11px; list-style-type:disc\">Access to restricted (18 and over) clubs and hangouts</li>" +
                                    "<li style=\"line-height:11px; font-size:11px; list-style-type:disc\">Exclusive clothing and accessories for restricted areas</li><li style=\"line-height:11px; font-size:11px; list-style-type:disc\">Special privileges only Access Pass members enjoy</li>" +
                                    "<li style=\"line-height:11px; font-size:11px; list-style-type:disc\">Access to private events</li><li style=\"line-height:11px; font-size:11px; list-style-type:disc\">And more!</li><br />";

                            tdPolicyText.InnerHtml = COMMUNITY_GUIDELINES;
                            tdPolicyText.Visible = false;


                            btnAction.Text = "Purchase Access Pass";
                            btnAction.Attributes.Add("onclick", "javacript:location.href='" + ResolveUrl("~/mykaneva/passDetails.aspx?pass=true&passID=" + KanevaGlobals.AccessPassGroupID) + "';return false;");

                            divAccessMessage.Visible = true;
                        }
                        else   // user is under 18 or age unknown
                        {
                            h2Title.InnerText = "This " + assetType + " is Restricted";

                            tdActionText.InnerHtml = "The " + assetType + " you are trying to access has been set to \"Restricted\" by its " +
                                "owner and may contain material inappropriate for anyone under the age of 18.<br/><br/>" +
                                "NOTE:  If you believe you are receiving this page in error, please contact support via the Support Page.";

                            tdPolicyText.InnerHtml = COMMUNITY_GUIDELINES;
                            tdPolicyText.Visible = false;

                            btnAction.Text = " Go Back ";
                            btnAction.Attributes.Add("onclick", "javacript:history.back();return false;");

                            divAccessMessage.Visible = true;
                        }
                    }
                    else //else check to see if the item is marked as private for people without editing rights
                    {
                        // if not the owner/admin, determine editing permissions
                        if (!_canEdit)
                        {
                            int group_id = drAsset["permission_group"] != DBNull.Value ? Convert.ToInt32(drAsset["permission_group"].ToString()) : 0;

                            switch (Convert.ToInt32(drAsset["permission"]))
                            {
                                // public
                                case (int)Constants.eASSET_PERMISSION.PUBLIC:
                                    break;

                                // private
                                case (int)Constants.eASSET_PERMISSION.PRIVATE:
                                    show_page = false;

                                    h2Title.InnerText = "This " + assetType + " is Private and Viewable by the Owner only";

                                    tdActionText.InnerText = "The " + assetType + " you are trying to access has been set to \"Private\" by its owner " +
                                        "and is currently not available for public viewing.";

                                    tdPolicyText.InnerHtml = PRIVACY_SETTINGS;

                                    btnAction.Attributes.Add("onclick", "javascript:history.back();return false;");
                                    btnAction.Text = "  Go Back  ";

                                    divAccessMessage.Visible = true;
                                    break;

                                default:
                                    break;
                            }
                        }
                    }
                }

                if (show_page)
                {
                    divDetails.Visible = true;
                    BindData(drAsset);
                }

                // Show asset name
                h1AssetTitle.InnerText = Server.HtmlDecode(drAsset["name"].ToString());

                // set all meta data
                string _mediaName = "";
                string _mediaDescription = "";
                NameValueCollection tagList = new NameValueCollection();

                if (drAsset != null)
                {
                    _mediaName = drAsset["name"].ToString();
                    _mediaDescription = drAsset["teaser"].ToString();
                    string[] tags = (drAsset["keywords"].ToString()).Split(' ');

                    //default media name as one of the tags
                    tagList.Add(_mediaName, _mediaName);

                    for (int i = 0; i < tags.Length; i++)
                    {
                        tagList.Add(tags[i], tags[i]);
                    }
                }

                Title = MetaDataGenerator.GenerateTitle(_mediaName, "", (int)Constants.eWEB_PAGE_TYPE.MEDIA) + " - Kaneva " + assetType;
                MetaDataDescription = MetaDataGenerator.GenerateMetaDescription(_mediaDescription, tagList, (int)Constants.eWEB_PAGE_TYPE.MEDIA);
                MetaDataKeywords = MetaDataGenerator.GenerateMetaKeywords(_mediaName, tagList, (int)Constants.eWEB_PAGE_TYPE.MEDIA);


                // Set the elements for sharing
                shareLink.Text = txtShare.Text;
                shareMailto.NavigateUrl = "mailto:?subject=You%27ve%20received%20media%20shared%20from%20Kaneva%21&body=" + txtShare.Text;

                lb_link_1.Attributes.Add("onclick", "akst_share('1', '" + txtShare.Text + "', '" + (this.Title).Replace("'", "&#39;") + "'); return false; ");

                // Set which tabs are shown and selected.
                if (!Request.IsAuthenticated)
                {
                    akst_tab1.Attributes.Add("style", "display:none;");
                    akst_tab2.Attributes.Add("style", "display:none;");
                    akst_tab3.Attributes.Add("class", "selected");

                    litNotLoggedShareStyle.Text = "<style>" +
                      "  #akst_email { display: block; } " +
                      "  #akst_blast { display: none; } " +
                      "  </style>";
                }
                else
                {
                    akst_tab1.Attributes.Add("class", "selected");
                }

                // Show correct advertisements
                AdvertisementSettings Settings = (AdvertisementSettings)Application["Advertisement"];

                // Set mode of Google Ads
                if (Settings.Mode.Equals(AdvertisingMode.Live))
                {
                    litAdTest.Visible = false;
                }
                else if (Settings.Mode.Equals(AdvertisingMode.Testing))
                {
                    litAdTest.Text = "google_adtest = \"on\"";
                }
            }
            else
            {
                // Issue a 301 redirect instead of a 302 to preserve SEO.
                Response.Status = "301 Moved Permanently";
                Response.AddHeader("Location", ResolveUrl("~/asset/assetDetailsLandingPage.aspx?" + Request.QueryString));
                Response.End();
            }
		}


		# region Helper Methods
		/// <summary>
		/// Bind the page data
		/// </summary>
		private void BindData (DataRow drAsset)
		{
			int assetTypeId = Convert.ToInt32 (drAsset ["asset_type_id"]);
			int assetId = Convert.ToInt32 (drAsset ["asset_id"]);
			int userId = GetUserId ();
			int ownerUserId = Convert.ToInt32 (drAsset ["user_id"]);

			string assetName = Server.HtmlDecode (drAsset ["name"].ToString ());
			
			
			// -- ACTION BAR SECTION --
			// ------------------------

            // Share
            lb_link_1.Attributes.Add ("href",GetShareLink(assetId));
            

            //Report Abuse
            lbReport.Attributes.Add("onclick", "javascript: window.open('" + ResolveUrl("~/suggestions.aspx?mode=WB&category=KANEVA%20Web%20Site&rurl=" + Server.UrlEncode(Request.Url.ToString())) + "','add','toolbar=no,menubar=no,scrollbars=yes');");
			
			// Favorites
			
			//user cant add his own assets to his library
			lbFave.Enabled = (userId != ownerUserId);
			if (!lbFave.Enabled)
			{
				//lbFave.Text = GetAssetAddedToLibraryLink();
                add.Attributes.Add("class", "addedToLibrary");
                
                
			}
			else
			{
				if (Request.IsAuthenticated)
				{
                    PagedDataTable pdtAsset = StoreUtility.GetConnectedMedia(KanevaWebGlobals.CurrentUser.CommunityId, userId, "a.asset_id=" + assetId, "", 1, 1);
				
					if (pdtAsset.Rows.Count > 0)
					{
						//lbFave.Text = GetAssetAddedToLibraryLink();
                        add.Attributes.Add("class", "addedToLibrary");
						lbFave.Enabled = false;
					}
                    else
                    {
                        add.Attributes.Add("class", "add");
                    }
				}
			}

            // RAVE 
            if (!IsPostBack)
            {
                // Which rave graphic do we show?	 
                RaveFacade raveFacade = new RaveFacade ();
                if (!raveFacade.IsAssetRaveFree(userId, assetId, RaveType.eRAVE_TYPE.SINGLE))
                {
                    rave.Attributes.Add ("class", "raveplus");
                }
                else
                {
                    rave.Attributes.Add ("class", "rave");
                }
                litAssetId.Text = assetId.ToString ();
                litCtrlRaveCount.Text = lblRaves.ClientID;
            }

			// show the asset	
			StreamAsset (drAsset);


			// -- ITEM DETAILS SECTION --
			// --------------------------

			// Is it mature?	  
			divRestricted.Visible = IsMature (Convert.ToInt32 (drAsset ["asset_rating_id"]));
			
			// asset name
			lblName.Text = TruncateWithEllipsis (assetName, 45);

			// asset image		
			imgAssetThumbnail.Src = GetMediaImageURL (drAsset ["thumbnail_small_path"].ToString	(), "sm", assetId, assetTypeId, Convert.ToInt32 (drAsset ["thumbnail_gen"])); 

			// description
			lblDescription.Text = drAsset ["teaser"].ToString ();

			// stats
			lblRuntime.Text = FormatSecondsAsTime (drAsset["run_time_seconds"]);
			lblCreatedDate.Text = FormatDate (drAsset ["created_date"]) + " <span class=\"note\">(" + FormatDateTimeSpan(drAsset ["created_date"]) + ")</span>";
			lblRaves.Text = drAsset ["number_of_diggs"].ToString ();
			lblNumViews.Text = drAsset ["number_of_downloads"].ToString ();
			
			//**NOTE** There is a bug in the number_of_channels count.  This value is being set in the
			// BuildTopChannelList() method until the trigger is fixed to correctly keep the channel count
			lblNumTimesAdded.Text = drAsset ["number_of_channels"].ToString () + " time" + (Convert.ToInt32(drAsset ["number_of_channels"]) == 1 ? "" : "s");
			
            //instructions if any
            if((drAsset["instructions"] != null) && (drAsset["instructions"].ToString() != ""))
            {
                trInstructions.Visible = true;
                txtInstructions.Text = drAsset["instructions"].ToString();
            }
			
            //company name if any
            if((drAsset["company_name"] != null) && (drAsset["company_name"].ToString() != ""))
            {
                lblCompanyName.Text = "Company Name: " + drAsset["company_name"].ToString();
            }

			lblNumComments.Text = drAsset ["number_of_comments"].ToString ();

			// owner
            UserFacade userFacade = new UserFacade();
            User userOwner = userFacade.GetUser(ownerUserId);

            imgAvatar.Src = GetProfileImageURL(userOwner.ThumbnailSquarePath, "sq", userOwner.Gender, userOwner.FacebookSettings.UseFacebookProfilePicture, userOwner.FacebookSettings.FacebookUserId);
            imgAvatar.Alt = userOwner.Username;

            divOwnerRestricted.Visible = userOwner.MatureProfile;

            aUserName.Attributes.Add("href", GetPersonalChannelUrl(userOwner.NameNoSpaces));
            aUserName.InnerText = userOwner.Username;

			/**** Additional Info Section - Owner Information ****/
			// Username and URL 
            aUserName3.Attributes.Add("href", GetPersonalChannelUrl(userOwner.NameNoSpaces));

            // Show edit link?
			if (IsAdministrator() || IsCSR () || ( Convert.ToInt32 (drAsset ["owner_id"]).Equals (KanevaWebGlobals.CurrentUser.UserId) ))
			{
				aEdit.HRef = "~/asset/assetEdit.aspx?assetId=" + drAsset ["asset_id"].ToString ();
				aEdit.Visible = true;
			}

			// share link
			txtShare.Attributes.Add ("onclick", "this.select();");
			txtShare.Text = Server.HtmlDecode("http://" + Request.Url.Host + GetAssetDetailsLink (assetId));

            //set the full screen option
            lbFullScreen.Attributes.Add("onclick", "assetFullScreen('http://" + KanevaGlobals.SiteName + "/asset/assetDetailsFullScreen.aspx?assetId=" + assetId + "','" + assetName + " (Full SCreen)')");
            lbFullScreen.Attributes.Add("href", txtShare.Text + "?autoPlay=false");
        
            
			// embed link
			txtEmbed.Attributes.Add ("onclick", "this.select();");
			txtEmbed.Text = m_embedText;	

			// raw embed link
			txtRawEmbed.Attributes.Add ("onclick", "this.select();");

			// related items
			lblRelatedByTopic.Text = GetAssetTags (drAsset ["keywords"].ToString (), assetTypeId);
			lblRelatedByTopic.Text += GetAssetTags (drAsset ["categories"].ToString(), assetTypeId);

			string asset_name = KanevaWebGlobals.GetAssetTypeName(assetTypeId).ToLower ();
			spnRelatedType.InnerText = asset_name == "audio" ? "music" : asset_name+"s";

			// sharing communities
			BuildTopChannelList (drAsset, ownerUserId);

			// license info
			GetCopyrightInfo (drAsset, lblLicense, lblLicenseURL);

			// additional info
			string det_desc = AddHrefNewWindow (drAsset ["body_text"].ToString ().Trim() );
			lblBodyText.Text = det_desc;
			trDetailDesc.Visible = Convert.ToBoolean (RemoveHTML (det_desc).Length > 0 ? "true" : "false");

			// show additional attribute data
			BindAttributes (assetId);


			// -- ADS SECTION --
			// -----------------


			// -- RELATED ITEMS SECTION --
			// ---------------------------
			ucRelatedItems.AssetTypeId = assetTypeId;
			ucRelatedItems.AssetId = assetId;
			ucRelatedItems.ItemsPerPage = 5;
			ucRelatedItems.AssetOwnerId = ownerUserId;


			// -- COMMENTS SECTION --
			// ----------------------
			ucComments.AssetId = assetId;
			ucComments.AssetOwnerId = AssetOwnerId;
							 

			/*****************************/
			/**  WHAT TO DO WITH THESE  **/
			
			// Image Caption
			string caption = drAsset ["image_caption"].ToString ();
			if (caption != "")
			{
				lblImageCaption.Text = caption;
				lblImageCaption.Visible = true;
			}

			/** END WHAT TO DO SECTION  **/
			/*****************************/

		}
		
		/// <summary>
		/// BindAttributes
		/// </summary>
		private void BindAttributes (int assetId)   //ok
		{
			// Attributes
			DataTable dtAttributes = StoreUtility.GetAssetAttributeValues (assetId);

			if (dtAttributes.Rows.Count > 0)
			{
				rptAttributes.Visible = true;
				rptAttributes.DataSource = dtAttributes;
				rptAttributes.DataBind ();
			}
			else
			{
				rptAttributes.Visible = false;
				if (!trDetailDesc.Visible)
				{
					trAttributes.Visible = false;
				}
			}
		}

		/// <summary>
		/// StreamAsset
		/// </summary>
		/// <param name="drAsset"></param>
		private void StreamAsset(DataRow drAsset)	//ok	
		{
            bool autoPlay = false;
            MediaFacade mediaFacade = new MediaFacade();

			Response.Expires = 0;
			Response.CacheControl = "no-cache";

			int assetId = Convert.ToInt32 (drAsset["asset_id"]);
            lb_link_1.Attributes.Add("href", GetShareLink(assetId));

            //logic check to prevent play back on main page for when the user clicks the view in LArge screen;
            if ((Request["autoPlay"] == null) || ((Request["autoPlay"] != "false")))
            {
                autoPlay = true;
            }

			// Streamable asset?
			if (!StoreUtility.IsAssetStreamable (drAsset) && !StoreUtility.IsYouTubeContent(drAsset) &&
                !StoreUtility.IsStreamingTVContent (drAsset) && !StoreUtility.IsFlashWidget (drAsset) && !StoreUtility.IsFlashContent (drAsset))
			{
				ShowErrorOnStartup ("File is being converted, the process could take several minutes, please try again later.");
				return;
			}	
			
			if (Convert.ToInt32(drAsset["publish_status_id"]) != (int) Constants.ePUBLISH_STATUS.PUBLISH_COMPLETE )
			{
				ShowErrorOnStartup ("File is being published, the process could take several minutes, please try again later.");
				return;
			}

			// Set up links
			string name = Server.HtmlDecode (drAsset ["name"].ToString ());
			
			// The URL to the content on the streaming server
            string contentURL = Configuration.StreamingServer + "/" + Configuration.StreamingFile + "?tId=" + assetId;
            if (Configuration.StreamIIS)
            {
                contentURL = Configuration.StreamingServer + "/" + drAsset["media_path"].ToString();
            }

			int height = 378; 
			int width = 450; 

			string dimensions = " height=\"" + height + "\" width=\"" + width + "\" ";

			// Is it playable with Kaneva OMM?
			if (StoreUtility.IsOMMContent (drAsset))
			{
				m_embedText = litStream.Text = GetAssetEmbedLink (assetId, height, width, true ); // pass in true for fullURL only since required for embed link
				//litStream.Text = GetAssetEmbedLink (assetId, height, width, false ); 
                litStream.Text = StoreUtility.GetAssetEmbedLink(assetId, height, width, Page, false, false, autoPlay);
			}
			else if (StoreUtility.IsQuickTimeContent (drAsset))
			{
				// Don't do this for OMM, OMM takes care of that
				// Update the media views
                mediaFacade.UpdateMediaViews (assetId, Common.GetVisitorIPAddress(), GetUserId ());

				if (StoreUtility.IsAudioContent (drAsset))
				{
					dimensions = "height=\"16\" width=\"" + width.ToString () + "\"";

					// Use Quicktime for these audio
					m_embedText = litStream.Text = "<object classid=\"clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B\" codebase=\"http://www.apple.com/qtactivex/qtplugin.cab\" " + dimensions + ">" +
						"<param name=\"src\" value=\"" + contentURL + "\">" +
                        "<param name=\"autoplay\" value=\"" + autoPlay + "\">" +
						"<param name=\"controller\" value=\"true\">" +
                        "<embed " + dimensions + " src=\"" + contentURL + "\" pluginspage=\"http://www.apple.com/quicktime/download/\" type=\"video/quicktime\" controller=\"true\" autoplay=" + autoPlay + ">" +
						"</object>";
				}
				else
				{
					// Use Quicktime for these videos, set scale to ASPECT
					m_embedText = litStream.Text = "<object classid=\"clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B\" codebase=\"http://www.apple.com/qtactivex/qtplugin.cab\" " + dimensions + ">" +
						"<param name=\"src\" value=\"" + contentURL + "\">" +
                        "<param name=\"autoplay\" value=\"" + autoPlay + "\">" +
						"<param name=\"controller\" value=\"true\">" +
						"<PARAM NAME=\"scale\" VALUE=\"ASPECT\">" +
                        "<embed scale=\"ASPECT\" src=\"" + contentURL + "\" pluginspage=\"http://www.apple.com/quicktime/download/\" type=\"video/quicktime\" controller=\"true\" autoplay=" + autoPlay + ">" +
						"</object>";
				}
			}
			else if (StoreUtility.IsFlashContent (drAsset))
			{
				// Don't do this for OMM, OMM takes care of that
				// Update the media views
                mediaFacade.UpdateMediaViews (assetId, Common.GetVisitorIPAddress(), GetUserId ());

				if (drAsset ["asset_type_id"].Equals ((int) Constants.eASSET_TYPE.WIDGET) ||
                    (drAsset["asset_type_id"].Equals ((int) Constants.eASSET_TYPE.GAME) && drAsset["media_path"].ToString() == ""))
				{
					contentURL = drAsset["asset_offsite_id"].ToString();
				}

				m_embedText = litStream.Text = "<OBJECT classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000' " +
					" codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0' " +
					" id=\"swfStream\" " + dimensions + ">" +
					" <param name='movie' value=\"" + contentURL + "\">" +
					" <param name='quality' value=\"high\">" +
					" <param name='loop' value=\"false\">" +
                    " <PARAM NAME='AutoStart' VALUE=\"" + autoPlay + "\">" +
                    " <param name='wmode' value='opaque' />" +
					" <EMBED src=\"" + contentURL + "\" quality='high' " + dimensions +
                    " loop=\"false\" autostart=\"" + autoPlay + "\" type='application/x-shockwave-flash'" +
					" pluginspage='http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash'>" +
					" </EMBED> " +
					"</OBJECT>";
			}
			else if (StoreUtility.IsImageContent (drAsset))
			{
                //hide the pop up window
                trNewWindow.Visible = false;

				// Don't do this for OMM, OMM takes care of that
				// Update the media views
                mediaFacade.UpdateMediaViews (assetId, Common.GetVisitorIPAddress(), GetUserId ());

				// It is an image
				litStream.Text = "<div style=\"width: " + width.ToString () + "px; background-color: #FFFFFF; overflow: hidden; border: 1px solid #e0e0e0;\">" +
					"<a href=\"" + ResolveUrl("~/mykaneva/PictureDetail.aspx") + "?assetId=" + assetId.ToString() + "\" border=\"0\">" +
					"<img src=\"" + GetPhotoImageURL (drAsset ["thumbnail_assetdetails_path"].ToString (), "la") + "\" border=\"0\" alt=\"" + name + "\" title=\"" + name + "\"/></a></div>";

				m_embedText = "<a href=\"http://" + Request.Url.Host + GetAssetDetailsLink (assetId) + "\" target=\"resource\" border=\"0\"><img src='" + GetPhotoImageURL (drAsset ["image_full_path"].ToString (), "la") + "'/></a>";
				divEmbedded.InnerText = "Photo Embed URL:";

				txtRawEmbed.Text = GetPhotoImageURL (drAsset ["image_full_path"].ToString (), "la");
				trPhotoURL.Visible = true;
			}
			else if (StoreUtility.IsYouTubeContent (drAsset))
			{                 
				height = 356;
				width = 450;

				// Update the media views
                mediaFacade.UpdateMediaViews (assetId, Common.GetVisitorIPAddress(), GetUserId ());

                //autostart
                m_embedText = StoreUtility.GetYouTubeEmbedLink(drAsset["asset_offsite_id"].ToString(), height, width, true, autoPlay);
                litStream.Text = StoreUtility.GetYouTubeEmbedLink(drAsset["asset_offsite_id"].ToString(), height, width, false, autoPlay);
				phStream.Controls.Add (litStream);
			}
			else if (StoreUtility.IsStreamingTVContent (drAsset))
			{                 
				//height = 356;
				//width = 450;

				// Update the media views
                mediaFacade.UpdateMediaViews (assetId, Common.GetVisitorIPAddress(), GetUserId ());

				if (StoreUtility.IsAudioContent (drAsset))
				{
					dimensions = "height=\"100\" width=\"" + width.ToString () + "\"";
				}

				string tvURL = drAsset["asset_offsite_id"].ToString();

				// Use Media player
				m_embedText = litStream.Text = "<OBJECT id=\"mpStream\"" + dimensions +
					" CLASSID=\"CLSID:6BF52A52-394A-11d3-B153-00C04F79FAA6\"" +
					" standby='Loading Microsoft Windows Media Player components...'" +
					" type=\"application/x-oleobject\">" +
					" <PARAM NAME=\"URL\" VALUE=\"" + tvURL + "\">" +
					" <PARAM NAME=\"ShowStatusBar\" VALUE=\"True\">" +
					" <PARAM NAME=\"EnableContextMenu\" VALUE=\"False\">" +
                    " <PARAM NAME=\"AutoStart\" VALUE=\"" + autoPlay + "\">" +
					" <PARAM name=\"PlayCount\" value=\"1\">" +
					"<embed " + dimensions + " type=\"application/x-mplayer2\" pluginspage=\"http://www.microsoft.com/Windows/MediaPlayer/\"" +
					" name=\"mediaplayer1\" showstatusbar=\"1\" EnableContextMenu=\"false\" autostart=\"" + autoPlay + "\" transparentstart=\"1\" loop=\"0\" controller=\"true\" src=\"" + tvURL + "\"></embed>" +
					"</OBJECT>";
			}
			else
			{
				// Don't do this for OMM, OMM takes care of that
				// Update the media views
                mediaFacade.UpdateMediaViews(assetId, Common.GetVisitorIPAddress(), GetUserId());

				if (StoreUtility.IsAudioContent (drAsset))
				{
					dimensions = "height=\"100\" width=\"" + width.ToString () + "\"";
				}

				// Use Media player
				m_embedText = litStream.Text = "<OBJECT id=\"mpStream\"" + dimensions +
					" CLASSID=\"CLSID:6BF52A52-394A-11d3-B153-00C04F79FAA6\"" +
					" standby='Loading Microsoft Windows Media Player components...'" +
					" type=\"application/x-oleobject\">" +
					" <PARAM NAME=\"URL\" VALUE=\"" + contentURL + "\">" +
					" <PARAM NAME=\"ShowStatusBar\" VALUE=\"True\">" +
					" <PARAM NAME=\"EnableContextMenu\" VALUE=\"False\">" +
                    " <PARAM NAME=\"AutoStart\" VALUE=\"" + autoPlay + "\">" +
					" <PARAM name=\"PlayCount\" value=\"1\">" +
					"<embed " + dimensions + " type=\"application/x-mplayer2\" pluginspage=\"http://www.microsoft.com/Windows/MediaPlayer/\"" +
					" name=\"mediaplayer1\" showstatusbar=\"1\" EnableContextMenu=\"false\" autostart=\"" + autoPlay + "\" transparentstart=\"1\" loop=\"0\" controller=\"true\" src=\"" + contentURL + "\"></embed>" +
					"</OBJECT>";
			}
		   
			phStream.Controls.Add (litStream);	
		}

		/// <summary>
		/// Returns the number of channels this asset belongs to
		/// </summary>
		private string GetChannelCount (int numberOfChannels)
		{
			return numberOfChannels.ToString () + " channel" + (numberOfChannels == 1 ? string.Empty : "s");
		}

		/// <summary>
		/// BuildTopChannelList
		/// </summary>
		private void BuildTopChannelList (DataRow drAsset, int ownerUserId)
		{
			PagedDataTable pdtChannels = StoreUtility.GetChannelsSharingAsset (Convert.ToInt32 (drAsset["asset_id"]), KanevaWebGlobals.CurrentUser.HasAccessPass, false, "thumbnail_small_path <> '' ", 1, 33);

			string strChannels = "";
			int cnt = 0;

			for (int i = 0; i < pdtChannels.Rows.Count; i ++)
			{
				if (!pdtChannels.Rows [i]["community_id"].Equals (DBNull.Value) && !pdtChannels.Rows [i]["name"].Equals (DBNull.Value))
				{
					if ( (pdtChannels.Rows [i]["is_adult"]).ToString () == "Y" )
					{
						strChannels += "<span class=\"restricted\"></span>";
					}
					
					if ( Convert.ToBoolean (pdtChannels.Rows [i]["is_personal"]) )
					{
						if (! Convert.ToInt32 (pdtChannels.Rows [i]["creator_id"]).Equals (ownerUserId))
						{
							strChannels += "<a href=\"" + GetPersonalChannelUrl (pdtChannels.Rows [i]["name_no_spaces"].ToString ()) + 
								"\" title=\"" + pdtChannels.Rows [i]["name"].ToString () + "\"><img src=\"" +
                                GetProfileImageURL (pdtChannels.Rows[i]["thumbnail_small_path"].ToString (), "sq", "M",
                                Convert.ToBoolean (pdtChannels.Rows[i]["use_facebook_profile_picture"]), Convert.ToUInt64 (pdtChannels.Rows[i]["fb_user_id"])) + 
								"\" border=\"0\" width=\"30\" height=\"23\" style=\"margin:1px;\" /></a>";
						
							cnt++;
						}
					}		  
					else
					{
						strChannels += "<a href=\"" + GetBroadcastChannelUrl (pdtChannels.Rows [i]["name_no_spaces"].ToString ()) + "\" title=\"" + pdtChannels.Rows [i]["name"].ToString () + "\"><img src=\"" + GetBroadcastChannelImageURL (pdtChannels.Rows [i]["thumbnail_small_path"].ToString (), "sm") + "\" border=\"0\" width=\"30\" height=\"23\" style=\"margin:1px;\" /></a>";
						cnt++;
					}
				}
			}
			
			spnAdders.InnerHtml = strChannels;
			int tot_count = cnt < 34 ? cnt : pdtChannels.TotalCount - 1;	 //subtract 1 for the owner

			lblAddedBy.Text = tot_count.ToString () + (tot_count == 1 ? " Member/Community has " : " Members/Communities have ");
			
			//** This is temporary.  Need to remove when fix but to number_added_channels count
			// in assets_stats table and trigger
			lblNumTimesAdded.Text = tot_count.ToString () + " time" + (tot_count == 1 ? "" : "s");
		}

		/// <summary>
		/// Add the tag to the tag cart
		/// </summary>
		private void AddMediaToFavorites ()
		{
			if (!Request.IsAuthenticated)
			{
				AjaxCallHelper.WriteAlert ("You must be signed in to perform this action.");
				return;
			}

			if (Request ["assetId"] != null)
			{
				int asset_id = Convert.ToInt32 (Request ["assetId"]);
				int user_id = GetUserId();

				StoreUtility.AddTag( user_id, KanevaWebGlobals.CurrentUser.CommunityId, asset_id, Common.GetVisitorIPAddress() );
			}

			//lbFave.Text = GetAssetAddedToLibraryLink();
            add.Attributes.Add("class", "addedToLibrary");
			lbFave.Enabled = false;
		}
		
		private string GetAssetAddedToLibraryLink()
		{
			return  "<img src=\"" + ResolveUrl("~/images/add_16_disabled.gif") + "\" runat=\"server\" width=\"16\" height=\"16\" border=\"0\"> Added to my library";
		}

        private void AddFriend ()
        {
            if (Request.IsAuthenticated)
            {
                // Add them as a friend
                int ret = GetUserFacade.InsertFriendRequest(GetUserId(), pmToUserId,
                    Page.Request.CurrentExecutionFilePath, Global.RequestRabbitMQChannel());

                if (ret.Equals (1))	// already a friend
                {
                    AjaxCallHelper.WriteAlert ("This member is already your friend.");
                }
                else if (ret.Equals (2))  // pending friend request
                {
                    AjaxCallHelper.WriteAlert ("You have already sent this member a friend request.");
                }
                else
                {
                    // send request email
                    MailUtilityWeb.SendFriendRequestEmail (GetUserId (), pmToUserId);
                    spnPMAlertMsg.InnerHtml = "A friend request was sent to user " + UsersUtility.GetUserNameFromId(pmToUserId) + ".";
                }
            }
        }

        #endregion


		# region Event Handlers
		
		/// <summary>
		/// Handle an asset rave
		/// </summary>
		protected void lbRave_Click (Object sender, EventArgs e) 
		{
            if (!Request.IsAuthenticated)
            {
                Response.Redirect (GetLoginURL ());
            }

            RaveFacade raveFacade = new RaveFacade ();
            int assetId = Convert.ToInt32(Request["assetId"]);

            if (!raveFacade.IsAssetRaveFree (KanevaWebGlobals.CurrentUser.UserId, assetId,
                RaveType.eRAVE_TYPE.SINGLE) && !IsAdministrator ())
            {

                lbRave.CssClass = "raveplus";

                // this rave is not free, show buy rave dialog
                AjaxCallHelper.Write ("popFacebox();");

				// GA Event
				Page.ClientScript.RegisterClientScriptBlock(GetType(), "gaRaveEvent", "callGAEvent('Media', 'RavePlus', 'RavePlusClick');", true);
            }
            else  // first time rave
            {
                // first rave is free.  Insert a normal rave.
                raveFacade.RaveAsset (KanevaWebGlobals.CurrentUser.UserId, assetId, RaveType.eRAVE_TYPE.SINGLE, "");

				// GA Event
				Page.ClientScript.RegisterClientScriptBlock(GetType(), "gaRaveEvent", "callGAEvent('Media', 'Rave', 'RaveClick');", true);
            }

            rave.Attributes.Add ("class", "raveplus");
		}

		/// <summary>
		/// Handle an asset add to favorites list
		/// </summary>
		protected void lbFave_Click (Object sender, EventArgs e)
		{
			AddMediaToFavorites ();
		}

        /// <summary>
		/// Share with a blast
		/// </summary>
		protected void btnBlast_Click (object sender, EventArgs e)
		{
			if (Request.IsAuthenticated)
			{
				// Check for any inject scripts
				if (KanevaWebGlobals.ContainsInjectScripts (txtBlast.Text))
				{					
					m_logger.Warn ("User " + KanevaWebGlobals.GetUserId () + " tried to script from IP " + Common.GetVisitorIPAddress());
					ShowErrorOnStartup ("Your input contains invalid scripting, please remove script code and try again.", true);
					return;
				}

				if (txtBlast.Text.Length == 0)
				{					
					AjaxCallHelper.WriteAlert ("Please enter a blast.  You can not submit a blank blast.");
					return;
				}

				if (txtBlast.Text.Length > KanevaGlobals.MaxCommentLength)		  
				{					
					AjaxCallHelper.WriteAlert ("Your blast is too long.  Blasts must be " + KanevaGlobals.MaxCommentLength.ToString () + " characters or less");
					return;
				}

                BlastFacade blastFacade = new BlastFacade();
                if (blastFacade.SendLinkBlast(GetUserId(), 0, KanevaWebGlobals.CurrentUser.Username, KanevaWebGlobals.CurrentUser.NameNoSpaces, txtBlastTitle.Text, txtShare.Text, txtBlast.Text, "", KanevaWebGlobals.CurrentUser.ThumbnailSmallPath).Equals(1))                            
				{
					spnBlastAlertMsg.InnerText = "Blast was sent.";
                    btnBlast.Visible = false;
				}
				else
				{
					spnBlastAlertMsg.InnerText = "Failure sending Blast.";
				}

				MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('spnAlertMsg', 5000);");
			}

			txtBlast.Text = "";						
		}

        /// <summary>
        /// Share with a private message
        /// </summary>
        protected void btnPMSend_Click (object sender, EventArgs e)
        {
            int toUserId = 0;
            string strSubject = Server.HtmlEncode (txtPrivateMsgSubject.Text.Trim ());

            // Default the subject
            if (strSubject.Length.Equals (0))
            {
                strSubject = "No subject";
            }

            // Look up the user id via the username
            toUserId = UsersUtility.GetUserIdFromUsername (txtPrivateMsgTo.Text.Trim ());
            pmToUserId = toUserId;

            if (txtPrivateMsgTo.Text.Trim () == "" || toUserId == 0)
            {
                spnPMAlertMsg.InnerText = "Please provide a valid Kaneva username.";
                return;
            }

            // Send the message
            int retCode = SendMessage (toUserId, strSubject);
            if (retCode.Equals (-99))
            {
                spnPMAlertMsg.InnerText = "Message not sent. This member has blocked you from sending messages to them.";
                return;
            }
            else if (retCode.Equals (Constants.ERR_USER_EXCEEDED_NON_FRIEND_MESSAGE_COUNT))
            {   // user has exceeded number of sent non-friend messages for today
                spnPMAlertMsg.InnerHtml = "You have exceeded the maximum daily number of private messages sent " +
                   " to non-friends (" + KanevaGlobals.MaxNumberNonFriendMessagesPerDay.ToString () +
                   "). In order to send this message, the person will need to add you as a friend." +
                   "<br/><br/><a href=\"javascript:void(0);\" " +
                   " onclick=\"$('" + lbAddFriend.ClientID + "').click();" + "\">Become a Friend</a>";
                return;
            }
            else
            {
                spnPMAlertMsg.InnerText = "Message Sent!";
                MagicAjax.AjaxCallHelper.Write ("ShowConfirmMsg('spnPMAlertMsg', 4000);");
                // If this is an asset share log it
                if (Request["assetId"] != null)
                {
                    MediaFacade mediaFacade = new MediaFacade();
                    mediaFacade.ShareAsset(Convert.ToInt32(Request["assetId"]),
                        GetUserId (), Common.GetVisitorIPAddress(), null, toUserId);
                }
            }
        }

        /// <summary>
        /// Send Message
        /// </summary>
        /// <param name="toId"></param>
        private int SendMessage(int toId, string strSubject)
        {
            // confirm the user can send a pm to a non-friend (user has not exceeded daily max)
            if (!IsAdministrator ())
            {
                if (UsersUtility.ConfirmCanSendNonFriendPM (GetUserId (), toId).Equals (Constants.ERR_USER_EXCEEDED_NON_FRIEND_MESSAGE_COUNT))
                {
                    return Constants.ERR_USER_EXCEEDED_NON_FRIEND_MESSAGE_COUNT;
                }
            }

            string strUrl = "<a href=\"" + txtShare.Text + "\">" + txtShare.Text + "</a><br />";

            // Insert a private message
            UserFacade userFacade = new UserFacade();
            Message message = new Message(0, GetUserId(), toId, strSubject,
                strUrl + txtPrivateMsgBody.Text, new DateTime(), 0, (int)Constants.eMESSAGE_TYPE.PRIVATE_MESSAGE, 0, "U", "S");

            return userFacade.InsertMessage(message);
        }

        protected void lbAddFriend_Click (object sender, EventArgs e)
        {
            // Must be logged in
            if (!Request.IsAuthenticated)
            {
                AjaxCallHelper.WriteAlert ("Please sign in to add this user as a friend");
                return;
            }

            // Can't friend yourself
            if (GetUserId ().Equals (pmToUserId))
            {
                AjaxCallHelper.WriteAlert ("You cannot add yourself as a friend.");
                return;
            }

            AddFriend ();
        }
        
        #endregion

		#region Properties

        public int AssetOwnerId
        {
            get { return _assetOwnerId; }
            set { _assetOwnerId = value; }
        }

        private int pmToUserId
        {
            get
            {
                if (ViewState["pmToUserId"] == null)
                {
                    ViewState["pmToUserId"] = 0;
                }
                return (int) ViewState["pmToUserId"];
            }
            set
            {
                ViewState["pmToUserId"] = value;
            }
        }

        #endregion

		#region Declerations

		protected Button		JoinChannelThread;
		protected ImageButton	btnAvatar;
		protected LinkButton	lbCat1, lbCat2, lbCat3;
        protected LinkButton lbRave, lbFave, lbReport;
        protected LinkButton lbAddFriend;
        protected HtmlGenericControl ifrm;
        protected LinkButton btnAction;

        protected Label lblName, lblCompanyName;
		protected Label		lblCreatedDate;
		protected Label		lblImageCaption;
		protected Label		lblDescription, lblBodyText, lblLicense, lblLicenseURL;
		protected Label		lblRelatedByTopic, lblChannels;
		protected Label		lblRuntime, lblNumViews, lblNumTimesAdded, lblNumComments;
		protected Label		lblOwnerViews, lblOwnerFavs, lblOwnerFriends;
		protected Label		lblRaves, lblAddedBy;

		protected HyperLink		hlTag, hlScreenShots, hlFeature;

        protected TextBox txtShare, txtEmbed, txtRawEmbed, txtInstructions;	  
		protected Repeater		rptAttributes;
		protected PlaceHolder	phStream, phChannels;
		protected Literal		litStream = new Literal ();
		protected Literal		litAdTest;
        protected Literal litAssetId, litCtrlRaveCount;
		
		protected HtmlImage		imgAvatar, imgRestricted, imgAssetThumbnail;
		protected HtmlAnchor	aUserName, aUserName3, aEdit;
        protected HtmlTableRow trPhotoURL, trDetailDesc, trDescription, trAttributes, trInstructions, trNewWindow;
        protected HtmlTableCell tdActionText, tdButton, tdPolicyText, tdActionText2;

		protected HtmlGenericControl	divEmbedded, divRestricted, divOwnerRestricted;
		protected HtmlContainerControl	spnRuntime, spnAdders, spnRelatedType;
		protected HtmlContainerControl	h1AssetTitle;
		protected HtmlContainerControl	h2Title, divAccessMessage, divDetails;
        protected HtmlContainerControl rave;
        protected HtmlContainerControl add;

		// user controls
		protected RelatedItemsView	ucRelatedItems;
		protected Comments			ucComments;

		protected string m_embedText = string.Empty;
		private	string m_tag = string.Empty;
		private	bool _canEdit;
		private int	_assetOwnerId = 0;

		private const string COMMUNITY_GUIDELINES = "<br/><br/><u><b>Kaneva Community Guidelines</b></u><br/>" +
			"Kaneva defines restricted content as anything that contains strong language, or depictions of nudity, violence " +
			"or substance abuse.  When you mark content as Restricted, you protect our visitors -- and give youself greater " +
			"freedom of expression.  Only Kaneva members 18 years of age or older may post or view Restricted content.";

		private const string PRIVACY_SETTINGS = "<br/><br/><u><b>Privacy Settings for Kaneva Members</b></u><br/>" +
			"Kaneva members have the option to set their profiles and communities to private, only firends within their private " +
			"network are allowed to view detailed information such as personal interests and friends. These settings are " +
			"part of a broader effort to Kaneva to help protect the privacy of our community members and provide a safe " +
			"environment for all members to connect online.";

		// Logger
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);


        // Share Media 
        protected TextBox txtBlast, shareLink;
        protected HtmlContainerControl spnBlastAlertMsg, spnPMAlertMsg;
        protected Button btnBlast, btnPMSend;
        protected TextBox txtPrivateMsgTo, txtPrivateMsgSubject, txtPrivateMsgBody, txtBlastTitle;
        protected HyperLink shareMailto;
        //protected HtmlAnchor akst_link_1;
        protected LinkButton lbFullScreen, lb_link_1;
        protected HtmlGenericControl akst_tab1, akst_tab2, akst_tab3, akst_tab4;
        protected Literal litNotLoggedShareStyle;
       

		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
