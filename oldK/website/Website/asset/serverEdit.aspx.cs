///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using log4net;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for serverEdit.
	/// </summary>
	public class serverEdit : MainTemplatePage
	{
		/// <summary>
		/// publishedGames
		/// </summary>
		protected serverEdit () 
		{
			Title = "Server Add/Edit";
		}

		/// <summary>
		/// Page_Load
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Page_Load (object sender, System.EventArgs e)
		{
			// They must be logged in
			if (!Request.IsAuthenticated)
			{
				Response.Redirect (GetLoginURL ());
				return;
			}

			int gameLicenseId = Convert.ToInt32 (Request ["gli"]);
			int assetId = Convert.ToInt32 (Request ["assetId"]);

			if (!IsPostBack)
			{
				DataRow drGameLicense = null;

				// For existing licenses, get the asset id to prevent query string tampering
				if (!IsNewServer ())
				{
					drGameLicense = StoreUtility.GetGameLicense (gameLicenseId);
					assetId = Convert.ToInt32 (drGameLicense ["asset_id"]);
				}

				// Set up the form
				DataRow drDevSubscription = StoreUtility.GetLicenseSubscription ((int) Constants.eLICENSE_SUBSCRIPTIONS.DEVELOPER);
				drpType.Items.Add (new ListItem (drDevSubscription ["name"].ToString () + " (Free, Max of " + drDevSubscription ["max_server_users"].ToString () + " concurrent users)", ((int)Constants.eLICENSE_SUBSCRIPTIONS.DEVELOPER).ToString ()));

				// Can be a pilot only if it is not already a commercial
				if (!StoreUtility.IsCommercialGame (assetId))
				{
					DataRow drPilotSubscription = StoreUtility.GetLicenseSubscription ((int) Constants.eLICENSE_SUBSCRIPTIONS.PILOT);
					drpType.Items.Add (new ListItem (drPilotSubscription ["name"].ToString () + " (" + FormatCurrency (StoreUtility.ConvertKPointsToDollars (Convert.ToDouble (drPilotSubscription ["amount"]))) + "/month, Max of " + drPilotSubscription ["max_server_users"].ToString () + " concurrent users)", ((int)Constants.eLICENSE_SUBSCRIPTIONS.PILOT).ToString ()));
				}
				
				// Can be a commercial only if it is not a pilot
				if (!StoreUtility.IsPilotGame (assetId))
				{
					DataRow drCommSubscription = StoreUtility.GetLicenseSubscription ((int) Constants.eLICENSE_SUBSCRIPTIONS.COMMERCIAL);
					drpType.Items.Add (new ListItem (drCommSubscription ["name"].ToString () + " (" + FormatCurrency (StoreUtility.ConvertKPointsToDollars (Convert.ToDouble (drCommSubscription ["amount"]))) + "/month minimum, Max of " + drCommSubscription ["max_server_users"].ToString () + " concurrent users)", ((int)Constants.eLICENSE_SUBSCRIPTIONS.COMMERCIAL).ToString ()));
				}

				// Is it an add new?
				if (IsNewServer ())
				{
					// It is an add new
					DataRow drAsset = StoreUtility.GetAsset (assetId);
					txtServerName.Text = Server.HtmlDecode (drAsset ["name"].ToString ());		// Default to game name
					lblServerKey.Text = "[Not Created Yet]";
					lblStatus.Text = "New License";

					drpType.Enabled = true;
				}
				else
				{
					txtServerName.Text = Server.HtmlDecode (drGameLicense ["server_name"].ToString ());
					
					
					if (Convert.ToInt32 (drGameLicense ["license_type"]).Equals ((int) Constants.eLICENSE_SUBSCRIPTIONS.COMMERCIAL))
					{
						// Don't allow to see or edit patch on commercial games.
						lblPatchURL.Visible = true;
						txtPatchURL.Visible = false;
					}
					else
					{
						txtPatchURL.Visible = true;
					}

					lblAdminURL.Text = Server.HtmlDecode (drGameLicense ["admin_URL"].ToString ());
					lblServerKey.Text = drGameLicense ["game_key"].ToString ();
					lblStatus.Text = drGameLicense ["status_name"].ToString ();
					lblStatus.ToolTip = drGameLicense ["status_description"].ToString ();
					txtPatchURL.Text = Server.HtmlDecode (drGameLicense ["patch_URL"].ToString ());

					SetDropDownIndex (drpType, drGameLicense ["license_type"].ToString ());
					drpType.Enabled = false;
				}
			}
		}

		/// <summary>
		/// IsNewServer
		/// </summary>
		/// <returns></returns>
		private bool IsNewServer ()
		{
			int gameLiceseId = Convert.ToInt32 (Request ["gli"]);		
			return (gameLiceseId == 0);
		}


		/// <summary>
		/// Cancel Event Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnCancel_Click (object sender, EventArgs e) 
		{
			int assetId = Convert.ToInt32 (Request ["assetId"]);
			Response.Redirect (GetAssetEditLink (assetId));
		}

		/// <summary>
		/// Update Event Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnUpdate_Click (object sender, EventArgs e) 
		{
			// Server validation
			if (!Page.IsValid) 
			{
				return;
			}

			int gameLicenseId = Convert.ToInt32 (Request ["gli"]);
			int assetId = Convert.ToInt32 (Request ["assetId"]);
			int userId = GetUserId ();
			int orderId = 0;

			if (IsNewServer ())
			{
				// If it is commercial, validate a credit card for $200
				if (Convert.ToInt32 (drpType.SelectedValue).Equals ((int) Constants.eLICENSE_SUBSCRIPTIONS.COMMERCIAL))
				{
					int newGameLicenseId = StoreUtility.InsertGameLicense (assetId, Convert.ToInt32 (drpType.SelectedValue), GetUserId (), Server.HtmlEncode (txtServerName.Text), Server.HtmlEncode (txtPatchURL.Text), Constants.eGAME_LICENSE_STATUS.PENDING_AUTH);

					// Create an auth order for this license
					orderId = CreateAuthOrder (userId, newGameLicenseId, assetId);

					Session ["assetId"] = -1;
					Session ["orderId"] = orderId;
					Session ["feat"] = null;

					Response.Redirect (ResolveUrl ("~/checkout/paymentSelection.aspx?hidPP=Y"));
				}
				else
				{
					// DEV or Pilot go right to active
					StoreUtility.InsertGameLicense (assetId, Convert.ToInt32 (drpType.SelectedValue), GetUserId (), Server.HtmlEncode (txtServerName.Text), Server.HtmlEncode (txtPatchURL.Text), Constants.eGAME_LICENSE_STATUS.ACTIVE);
				}
			}
			else
			{
				StoreUtility.UpdateGameLicense (gameLicenseId, assetId, GetUserId (), Server.HtmlEncode (txtServerName.Text), Server.HtmlEncode (txtPatchURL.Text));

				// See if they have completed Auth yet
				if (Convert.ToInt32 (drpType.SelectedValue).Equals ((int) Constants.eLICENSE_SUBSCRIPTIONS.COMMERCIAL))
				{
					DataRow drGameLicense = StoreUtility.GetGameLicense (gameLicenseId);
					if (drGameLicense ["status_id"].Equals ((int) Constants.eGAME_LICENSE_STATUS.PENDING_AUTH))
					{
						// Create an auth order for this license
						orderId = CreateAuthOrder (userId, gameLicenseId, assetId);

						Session ["assetId"] = -1;
						Session ["orderId"] = orderId;
						Session ["feat"] = null;
						Response.Redirect (ResolveUrl ("~/checkout/paymentSelection.aspx?hidPP=Y"));
					}
				}
			}

			Response.Redirect (GetAssetEditLink (assetId));
		}

		/// <summary>
		/// CreateAuthOrder
		/// </summary>
		private int CreateAuthOrder (int userId, int newGameLicenseId, int assetId)
		{
			// Need this for the price
			DataRow drCommSubscription = StoreUtility.GetLicenseSubscription ((int) Constants.eLICENSE_SUBSCRIPTIONS.COMMERCIAL);

			// Create the order for auth
			int orderId = StoreUtility.CreateOrder (userId, Request.UserHostAddress, (int) Constants.eORDER_STATUS.CHECKOUT);
			StoreUtility.SetPurchaseType (orderId, Constants.ePURCHASE_TYPE.COMMERCIAL_SERVER_AUTH);

			// Add the item
			DataRow drOrder = StoreUtility.GetOrder (orderId);
			int orderItemId = StoreUtility.InsertOrderItem (drOrder, assetId, 1, "");
			StoreUtility.UpdateOrderItemGameLicenseAuth (newGameLicenseId, orderItemId);

			// Record the transaction in the database, marked as checkout
			int transactionId = StoreUtility.PurchasePoints (userId, (int) Constants.eTRANSACTION_STATUS.CHECKOUT, "Commercial Server Authorization", 0, (int) Constants.ePAYMENT_METHODS.KANEVA_CYBERSOURCE, 
				StoreUtility.ConvertKPointsToDollars (Convert.ToDouble (drCommSubscription ["amount"])), 0, 0, 0, Request.UserHostAddress);

			// Set the order id on the point purchase transaction
			StoreUtility.UpdatePointTransaction (transactionId, orderId);
			StoreUtility.UpdateOrderPointTranasactionId (orderId, transactionId);

			return orderId;
		}

		// Logger
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

		protected TextBox txtServerName, txtPatchURL;

		protected Label lblAdminURL, lblServerKey, lblStatus, lblPatchURL;

		protected DropDownList drpType;

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
