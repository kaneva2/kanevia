///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Collections.Specialized;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using MagicAjax;
using log4net;

using KlausEnt.KEP.Kaneva.usercontrols;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva
{
    /// <summary>
    /// Summary description for itemDetails.
    /// </summary>
    public partial class wokItemDetails : StoreBasePage
    {
        #region Declerations
        protected Button JoinChannelThread, btnAction;
        protected ImageButton btnAvatar;
        protected LinkButton lbCat1, lbCat2, lbCat3;
        protected LinkButton lbRave, lbShoppingList, lbReport;

        protected Label lblName;
        protected Label lblCreatedDate;
        protected Label lblImageCaption;
        protected Label lblDescription, lblBodyText, lblLicenseURL;
        protected Label lblRelatedByTopic, lblChannels;
        protected Label lblRuntime, lblNumViews, lblNumTimesAdded;
        protected Label lblOwnerViews, lblOwnerFavs, lblOwnerFriends;
        protected Label lblRaves, lblAddedBy, lblprice, lblAccess, lblAvailability, lblExclusivity;

        protected HyperLink hlTag, hlScreenShots, hlFeature;
        protected HyperLink hlShare;

        protected TextBox txtShare, txtEmbed, txtRawEmbed;
        protected Repeater rptAttributes;
        protected PlaceHolder phStream, phChannels;
        protected Literal litStream = new Literal();
        protected Literal litAdTest;

        protected HtmlImage imgAvatar, imgRestricted, imgWOKItemThumbnail;
        protected HtmlAnchor aUserName, aUserName3, aEdit;
        protected HtmlTableRow trPhotoURL, trDetailDesc, trDescription, trAttributes;
        protected HtmlTableCell tdActionText, tdButton, tdPolicyText;

        protected HtmlGenericControl divEmbedded, divPassRequired, divOwnerRestricted;
        protected HtmlContainerControl spnRuntime, spnAdders, spnRelatedType;
        protected HtmlContainerControl h1AssetTitle;
        protected HtmlContainerControl h2Title, divAccessMessage, divDetails;

        // user controls
        protected WOKRelatedItemsView ucRelatedItems;
        protected WOKItemComments ucReviews;

        protected string m_embedText = string.Empty;
        private string m_tag = string.Empty;
        private int _assetOwnerId = 0;

        private const string COMMUNITY_GUIDELINES = "<br/><br/><u><b>Kaneva Community Guidelines</b></u><br/>" +
            "Kaneva defines restricted content as anything that contains strong language, or depictions of nudity, violence " +
            "or substance abuse.  When you mark content as Restricted, you protect our visitors -- and give youself greater " +
            "freedom of expression.  Only Kaneva members 18 years of age or older may post or view Restricted content.";

        private const string PRIVACY_SETTINGS = "<br/><br/><u><b>Privacy Settings for Kaneva Members</b></u><br/>" +
            "Kaneva members have the option to set their profiles and communities to private, only firends within their private " +
            "network are allowed to view detailed information such as personal interests and friends. These settings are " +
            "part of a broader effort to Kaneva to help protect the privacy of our community members and provide a safe " +
            "environment for all members to connect online.";

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        // Share Media 
        protected TextBox txtBlast, shareLink;
        protected HtmlContainerControl spnBlastAlertMsg, spnPMAlertMsg;
        protected Button btnBlast, btnPMSend;
        protected TextBox txtPrivateMsgTo, txtPrivateMsgSubject, txtPrivateMsgBody, txtBlastTitle;
        protected HyperLink shareMailto;
        protected HtmlAnchor akst_link_1;
        protected HtmlGenericControl akst_tab1, akst_tab2, akst_tab3, akst_tab4;
        protected Literal litNotLoggedShareStyle, litStore, litSets;


        #endregion

        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Page_Load(object sender, System.EventArgs e)
        {
            InitializeWOKDetailsPage();
        }


        # region Helper Methods

        /// <summary>
        /// Initializes the page data
        /// </summary>
        private void InitializeWOKDetailsPage()
        {
            //highlights correct menu item
            HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.VIRTUAL_WORLD;

            if (Request["itemId"] == null)
            {
                Response.Redirect("~/free-virtual-world.kaneva");
            }

            DataRow drItem = null;

            try
            {
                int itemId = Convert.ToInt32(Request["itemId"]);
                drItem = StoreUtility.GetWOKItem(itemId);
                if ((drItem == null) || (drItem.ItemArray.Length <= 0))
                {
                    Response.Redirect("~/free-virtual-world.kaneva");
                }

            }
            catch (Exception ex)
            {
                m_logger.Error("Error loading WOK item details page: ", ex);
                Response.Redirect("~/free-virtual-world.kaneva");
            }

            ItemOwnerId = Convert.ToInt32(drItem["item_creator_id"]);

            divDetails.Visible = true;
            BindData(drItem);

            // Show asset name
            h1AssetTitle.InnerText = Server.HtmlDecode(drItem["display_name"].ToString());

            // set all meta data
            string _mediaName = "";
            string _mediaDescription = "";
            NameValueCollection tagList = new NameValueCollection();

            if (drItem != null)
            {
                _mediaName = drItem["display_name"].ToString();
                _mediaDescription = drItem["description"].ToString();

                //string[] tags = (drItem["keywords"].ToString()).Split(' ');
                ////default media name as one of the tags
                //tagList.Add(_mediaName, _mediaName);

                //for (int i = 0; i < tags.Length; i++)
                //{
                //    tagList.Add(tags[i], tags[i]);
                //}
            }

            Title = MetaDataGenerator.GenerateTitle(_mediaName, "", (int)Constants.eWEB_PAGE_TYPE.MEDIA) + " - Kaneva ";
            MetaDataDescription = MetaDataGenerator.GenerateMetaDescription(_mediaDescription, tagList, (int)Constants.eWEB_PAGE_TYPE.MEDIA);
            MetaDataKeywords = MetaDataGenerator.GenerateMetaKeywords(_mediaName, tagList, (int)Constants.eWEB_PAGE_TYPE.MEDIA);


            // Set the elements for sharing
            shareLink.Text = txtShare.Text;
            shareMailto.NavigateUrl = "mailto:?subject=You%27ve%20received%20media%20shared%20from%20Kaneva%21&body=" + txtShare.Text;

            akst_link_1.Attributes.Add("onclick", "akst_share('1', '" + txtShare.Text + "', '" + Title.ToString() + "'); return false; ");

            // Set which tabs are shown and selected.
            if (!Request.IsAuthenticated)
            {
                akst_tab1.Attributes.Add("style", "display:none;");
                akst_tab2.Attributes.Add("style", "display:none;");
                akst_tab3.Attributes.Add("class", "selected");

                litNotLoggedShareStyle.Text = "<style>" +
                  "  #akst_email { display: block; } " +
                  "  #akst_blast { display: none; } " +
                  "  </style>";
            }
            else
            {
                akst_tab1.Attributes.Add("class", "selected");
            }


            // Show correct advertisements
            AdvertisementSettings Settings = (AdvertisementSettings)Application["Advertisement"];

            // Set mode of Google Ads
            if (Settings.Mode.Equals(AdvertisingMode.Live))
            {
                litAdTest.Visible = false;
            }
            else if (Settings.Mode.Equals(AdvertisingMode.Testing))
            {
                litAdTest.Text = "google_adtest = \"on\"";
            }
        }


        /// <summary>
        /// Bind the page data
        /// </summary>
        private void BindData(DataRow drItem)
        {
            try
            {
                int itemId = Convert.ToInt32(drItem["global_id"]);
                int userId = GetUserId();
                int itemCreatorId = Convert.ToInt32(drItem["item_creator_id"]);
                string itemName = Server.HtmlDecode(drItem["display_name"].ToString());

                // -- ACTION BAR SECTION --
                // ------------------------

                // Share
                //hlShare.NavigateUrl = GetWOKShareLink(itemId);

                // Shopping List


                // Raves	 
                DataRow drRave = StoreUtility.GetWOKDig(userId, itemId);
                if (drRave != null && !IsAdministrator())
                {
                    lbRave.Text = "<img src=\"" + ResolveUrl("~/images/raveit_bt_disabled.gif") + "\" runat=\"server\" width=\"16\" height=\"16\" border=\"0\"> Raved";
                    lbRave.Enabled = false;
                    lbRave.ToolTip = "You Raved this Item";
                }

                // show the asset	
                StreamAsset(drItem);

                // -- ITEM DETAILS SECTION --
                // --------------------------

                // Is a pass required	  
                divPassRequired.Visible = PassRequired(Convert.ToInt32(drItem["access_type_id"]));

                // item name
                lblName.Text = TruncateWithEllipsis(itemName, 45);

                // item image		
                imgWOKItemThumbnail.Src = GetWOKItemImageURL(drItem["item_image_path"].ToString(), "sm");

                // description
                lblDescription.Text = drItem["description"].ToString();

                // stats
                lblCreatedDate.Text = FormatDate(drItem["date_added"]) + " <span class=\"note\">(" + FormatDateTimeSpan(drItem["date_added"]) + ")</span>";
                lblprice.Text = drItem["market_cost"].ToString();
                lblAccess.Text = drItem["access_type"].ToString();
                lblAvailability.Text = drItem["product_availability"].ToString();
                lblExclusivity.Text = drItem["product_exclusivity"].ToString();
                lblcredit.Text = drItem["currency_type"].ToString();
                lblRaves.Text = drItem["raves_count"].ToString();
                lblReviews.Text = drItem["comments_count"].ToString();
                lblNumViews.Text = drItem["views_count"].ToString();

                try
                {
                    string stores = "";
                    DataTable dtResults = StoreUtility.GetWOKItemStores(itemId);
                    foreach (DataRow row in dtResults.Rows)
                    {
                        stores += "<a href=\"#\">" + row["name"].ToString() + "</a>" + " ";
                    }
                    litStore.Text = stores;
                }
                catch (FormatException)
                {
                    litStore.Text = "";
                }

                try
                {
                    string sets = "";
                    DataTable dtResults = StoreUtility.GetWOKItemsSets(itemId);
                    foreach (DataRow row in dtResults.Rows)
                    {
                        sets += "<a href=\"#\">" + row["set_name"].ToString() + "</a>" + " ";
                    }
                    litSets.Text = sets;
                }
                catch (FormatException)
                {
                    litSets.Text = "";
                }

                // owner - if owner = zero use Kaneva defaults
                if (itemCreatorId > 0)
                {
                    UserFacade userFacade = new UserFacade();
                    User userOwner = userFacade.GetUser(itemCreatorId);

                    imgAvatar.Src = GetProfileImageURL(userOwner.ThumbnailSmallPath, "sm", userOwner.Gender);
                    imgAvatar.Alt = userOwner.Username;

                    aUserName.Attributes.Add("href", GetPersonalChannelUrl(userOwner.NameNoSpaces));
                    aUserName.InnerText = userOwner.Username;

                    /**** Additional Info Section - Owner Information ****/
                    // Username and URL 
                    aUserName3.Attributes.Add("href", GetPersonalChannelUrl(userOwner.NameNoSpaces));
                }
                else
                {
                    //imgAvatar.Src = "";
                    imgAvatar.Alt = "Kaneva";

                    aUserName.Attributes.Add("href", ResolveUrl("~/3d-virtual-world/virtual-life.kaneva"));
                    aUserName.InnerText = "Kaneva";

                }

                // Show edit link?
                if (IsAdministrator() || IsCSR() || (itemCreatorId.Equals(KanevaWebGlobals.CurrentUser.UserId)))
                {
                    aEdit.HRef = GetWOKEditLink(itemId);
                    aEdit.Visible = true;
                }

                // share link
                txtShare.Attributes.Add("onclick", "this.select();");
                txtShare.Text = Server.HtmlDecode("http://" + Request.Url.Host + GetWOKDetailsLink(itemId));


                // embed link
                txtEmbed.Attributes.Add("onclick", "this.select();");
                txtEmbed.Text = m_embedText;

                // raw embed link
                txtRawEmbed.Attributes.Add("onclick", "this.select();");

                // related items
                //lblRelatedByTopic.Text = GetAssetTags(drItem["keywords"].ToString(), assetTypeId);
                //lblRelatedByTopic.Text += GetAssetTags(drItem["categories"].ToString(), assetTypeId);

                //string asset_name = KanevaWebGlobals.GetAssetTypeName(assetTypeId).ToLower();
                //spnRelatedType.InnerText = asset_name == "audio" ? "music" : asset_name + "s";

                // additional info
                string det_desc = AddHrefNewWindow(drItem["description"].ToString().Trim());
                lblBodyText.Text = det_desc;
                trDetailDesc.Visible = Convert.ToBoolean(RemoveHTML(det_desc).Length > 0 ? "true" : "false");


                // -- ADS SECTION --
                // -----------------


                // -- OTHER MEDIA FROM OWNER SECTION --
                // ------------------------------------
                bool showMature = (KanevaWebGlobals.CurrentUser.IsAdult ? true : false);

                //string filter = "";
                //string orderby = "a.created_date DESC";

                //PagedDataTable pdtOwnersOtherMedia = StoreUtility.GetAssetsInChannel(
                //    CommunityUtility.GetPersonalChannelId(itemCreatorId), true, showMature,
                //    assetTypeId, filter, orderby, 1, 1);

                // -- RELATED ITEMS SECTION --
                // ---------------------------
                ucRelatedItems.WOKItem = drItem;
                ucRelatedItems.ItemsPerPage = 5;

                //// -- COMMENTS SECTION --
                //// ----------------------
                ucReviews.WOKItem = drItem;
            }
            catch (Exception)
            {
            }

        }


        /// <summary>
        /// StreamAsset
        /// </summary>
        /// <param name="drItem"></param>
        private void StreamAsset(DataRow drItem)	//ok	
        {
            Response.Expires = 0;
            Response.CacheControl = "no-cache";

            int itemId = Convert.ToInt32(drItem["global_id"]);
            //hlShare.NavigateUrl = GetWOKShareLink(itemId);

            // Set up links
            string name = Server.HtmlDecode(drItem["display_name"].ToString());

            int height = 378;
            int width = 450;
            string contentURL = drItem["item_image_path"].ToString();
            string dimensions = "height=\"" + height + "\" width=\"" + width + "\"";

            if (StoreUtility.IsFlashContent_Catalog(contentURL))
            {
                // Don't do this for OMM, OMM takes care of that
                // Update the media views
                StoreUtility.UpdateWOKItemViews(itemId, Request.UserHostAddress, GetUserId());

                m_embedText = litStream.Text = "<OBJECT classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000' " +
                    " codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0' " +
                    " id=\"swfStream\"" + dimensions + ">" +
                    " <param name='movie' value=\"" + contentURL + "\">" +
                    " <param name='quality' value=\"high\">" +
                    " <param name='loop' value=\"false\">" +
                    " <EMBED src=\"" + contentURL + "\" quality='high'" + dimensions +
                    " loop=\"false\" type='application/x-shockwave-flash'" +
                    " pluginspage='http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash'>" +
                    " </EMBED> " +
                    "</OBJECT>";
            }
            else if (StoreUtility.IsImageContent_Catalog(contentURL))
            {
                // Don't do this for OMM, OMM takes care of that
                // Update the media views
                StoreUtility.UpdateWOKItemViews(itemId, Request.UserHostAddress, GetUserId());

                // It is an image
                litStream.Text = "<div style=\"width: " + width.ToString() + "px; background-color: #FFFFFF; overflow: hidden; border: 1px solid #e0e0e0;\">" +
                    "<a href=\"" + ResolveUrl("~/mykaneva/PictureDetail.aspx") + "?itemId=" + itemId.ToString() + "\" border=\"0\">" +
                    "<img src=\"" + GetWOKItemImageURL(contentURL, "la") + "\" border=\"0\" alt=\"" + name + "\" title=\"" + name + "\"/></a></div>";

                m_embedText = "<a href=\"http://" + Request.Url.Host + GetWOKDetailsLink(itemId) + "\" target=\"resource\" border=\"0\"><img src='" + GetWOKItemImageURL(contentURL, "la") + "'/></a>";
                divEmbedded.InnerText = "Photo Embed URL:";

                txtRawEmbed.Text = GetWOKItemImageURL(contentURL, "la");
                trPhotoURL.Visible = true;
            }

            phStream.Controls.Add(litStream);
        }

        /// <summary>
        /// Add the item to the users cart
        /// </summary>
        private void AddItemToCart()
        {
            if (!Request.IsAuthenticated)
            {
                AjaxCallHelper.WriteAlert("You must be signed in to perform this action.");
                return;
            }

            if (Request["itemId"] != null)
            {
            }
        }

        protected void RaveAsset()
        {
            if (!Request.IsAuthenticated)
            {
                AjaxCallHelper.WriteAlert("You must be signed in to rave this item.");
                return;
            }

            int itemId = Convert.ToInt32(Request["itemId"]);

            if (!StoreUtility.InsertWOKDigg(GetUserId(), itemId, Request.UserHostAddress).Equals(1))
            {
                lblRaves.Text = Convert.ToString(Convert.ToInt32(lblRaves.Text) + 1);
            }

            //disables buttons if user is not an admin
            if (!this.IsAdministrator())
            {
                lbRave.Text = "<img src=\"" + ResolveUrl("~/images/icons/bb_raves.gif") + "\" runat=\"server\" width=\"16\" height=\"16\" border=\"0\"> Raved";
                lbRave.Enabled = false;
                lbRave.ToolTip = "You Raved this Item";
            }
        }

        private string GetAssetAddedToLibraryLink()
        {
            return "<img src=\"" + ResolveUrl("~/images/add_16_disabled.gif") + "\" runat=\"server\" width=\"16\" height=\"16\" border=\"0\"> Added to my library";
        }
        #endregion

        # region Event Handlers

        /// <summary>
        /// Handle an asset rave
        /// </summary>
        protected void lbRave_Click(Object sender, EventArgs e)
        {
            RaveAsset();
        }

        /// <summary>
        /// Handle an asset add to favorites list
        /// </summary>
        protected void lbShoppingList_Click(Object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Share with a blast
        /// </summary>
        protected void btnBlast_Click(object sender, EventArgs e)
        {
            if (Request.IsAuthenticated)
            {
                // Check for any inject scripts
                if (KanevaWebGlobals.ContainsInjectScripts(txtBlast.Text))
                {
                    m_logger.Warn("User " + KanevaWebGlobals.GetUserId() + " tried to script from IP " + Request.UserHostAddress);
                    ShowErrorOnStartup("Your input contains invalid scripting, please remove script code and try again.", true);
                    return;
                }

                if (txtBlast.Text.Length == 0)
                {
                    AjaxCallHelper.WriteAlert("Please enter a blast.  You can not submit a blank blast.");
                    return;
                }

                if (txtBlast.Text.Length > KanevaGlobals.MaxCommentLength)
                {
                    AjaxCallHelper.WriteAlert("Your blast is too long.  Blasts must be " + KanevaGlobals.MaxCommentLength.ToString() + " characters or less");
                    return;
                }

                BlastFacade blastFacade = new BlastFacade();
                if (blastFacade.SendLinkBlast(GetUserId(), 0, KanevaWebGlobals.CurrentUser.Username, KanevaWebGlobals.CurrentUser.NameNoSpaces, txtBlastTitle.Text, txtShare.Text, txtBlast.Text, "", KanevaWebGlobals.CurrentUser.ThumbnailSmallPath).Equals(1))
                {
                    spnBlastAlertMsg.InnerText = "Blast was sent.";
                    btnBlast.Visible = false;
                }
                else
                {
                    spnBlastAlertMsg.InnerText = "Failure sending Blast.";
                }

                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('spnAlertMsg', 5000);");
            }

            txtBlast.Text = "";
        }

        /// <summary>
        /// Share with a private message
        /// </summary>
        protected void btnPMSend_Click(object sender, EventArgs e)
        {

            int toUserId = 0;
            string strSubject = Server.HtmlEncode(txtPrivateMsgSubject.Text.Trim());

            // Default the subject
            if (strSubject.Length.Equals(0))
            {
                strSubject = "No subject";
            }

            // Look up the user id via the username
            toUserId = UsersUtility.GetUserIdFromUsername(txtPrivateMsgTo.Text.Trim());

            if (txtPrivateMsgTo.Text.Trim() == "" || toUserId == 0)
            {
                spnPMAlertMsg.InnerText = "Please provide a valid Kaneva username.";
                return;
            }


            // Send the message
            if (SendMessage(toUserId, strSubject).Equals(-99))
            {
                spnPMAlertMsg.InnerText = "Message not sent. This member has blocked you from sending messages to them.";
                return;
            }
            else
            {
                spnPMAlertMsg.InnerText = "Message Sent!";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('spnPMAlertMsg', 4000);");
                // If this is an asset share log it
                if (Request["itemId"] != null)
                {
                    StoreUtility.ShareAsset(Convert.ToInt32(Request["itemId"]),
                        GetUserId(), Request.UserHostAddress, null, toUserId);
                }
            }
        }

        /// <summary>
        /// Send Message
        /// </summary>
        /// <param name="toId"></param>
        private int SendMessage(int toId, string strSubject)
        {
            string strUrl = "<a href=\"" + txtShare.Text + "\">" + txtShare.Text + "</a><br />";

            // Insert a private message
            UserFacade userFacade = new UserFacade();
            Message message = new Message(0, GetUserId(), (int)Constants.eMESSAGE_STATUS.NEW, toId, (int)Constants.eMESSAGE_STATUS.NEW, strSubject,
                strUrl + txtPrivateMsgBody.Text, new DateTime(), 0, (int)Constants.eMESSAGE_TYPE.PRIVATE_MESSAGE, 0, true, true);

            return userFacade.InsertMessage(message);
        }


        #endregion

        #region Properties
        public int ItemOwnerId
        {
            get { return _assetOwnerId; }
            set { _assetOwnerId = value; }
        }
        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion
    }
}
