///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva.channel;

namespace KlausEnt.KEP.Kaneva.asset
{
	/// <summary>
	/// Summary description for assetSubmit.
	/// </summary>
	public class assetsSubmit : StoreBasePage
	{
		/// <summary>
		/// Constructor
		/// </summary>
		protected assetsSubmit () 
		{
			Title = "Media Submit";
		}

		/// <summary>
		/// Page_Load
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Page_Load (object sender, System.EventArgs e)
		{
			if(Request.IsAuthenticated)
			{
				if(!GetRequestParams())
				{
					//invalid request params
					RedirectToHomePage ();
				}
			}
			else
			{
				Response.Redirect (this.GetLoginURL ());
			}

			// Extend the timeout for this page
			this.AddKeepAlive ();

			//setup header nav bar
			if (_channelId == this.GetPersonalChannelId ())
			{
				//user's own channel
				HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.MEDIA_UPLOADS;
//				HeaderNav.MyMediaUploadsNav.ActiveTab = NavMediaUploads.TAB.LIBRARY;
//				HeaderNav.MyMediaUploadsNav.CommunityId = _channelId;
			//	HeaderNav.MyMediaUploadsNav.UserId = _channelOwnerId;
				
//				HeaderNav.SetNavVisible(HeaderNav.MyMediaUploadsNav);
			}
			else
			{
				//navs go to home-my channels for all channels if the user is an admin
				HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.CHANNELS;
//				HeaderNav.MyChannelsNav.ActiveTab = NavMyChannels.TAB.MEDIA_UPLOADS;
//				HeaderNav.MyChannelsNav.ChannelId = _channelId;
//				
//				HeaderNav.SetNavVisible(HeaderNav.MyChannelsNav);
			}

			// Set Nav
            HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB._UPDATE_;
			HeaderNav.SetNavVisible(HeaderNav.MyKanevaNav,2);

			hlShowMedia.NavigateUrl = ResolveUrl ("~/asset/publishedItemsNew.aspx?communityId=" + _channelId);

			if (!IsPostBack)
			{
				BindData(1);
			}

			AddBreadCrumb (new BreadCrumb ("Blog Edit", GetCurrentURL (), "", 0));
		}

		private bool GetRequestParams()
		{
			bool retVal = false;
			try
			{
				if(Request ["communityId"] != null)
				{
					//get channelId and find the default page
					_channelId = Convert.ToInt32 (Request ["communityId"]);
				}
				else
				{
					_channelId = GetPersonalChannelId ();
				}

				_assetIds = Request ["aIds"].ToString ();
				retVal = _assetIds.Length > 0;
			}
			catch(Exception){}
			
			return retVal;
		}

        private void BindData (int pageNumber)
		{
			DataTable dtAssets = StoreUtility.GetAssets (_channelId, _assetIds);
			repeaterAssets.DataSource = dtAssets;
			repeaterAssets.DataBind ();

            PagedDataTable pdtChannels = CommunityUtility.GetPublishChannels (GetUserId (), "name", pageNumber, _PGSIZE);

            rptChannels.DataSource = pdtChannels;
            rptChannels.DataBind ();
               
            // populate top pager
            pgTop.NumberOfPages = Math.Ceiling ((double) pdtChannels.TotalCount / _PGSIZE).ToString ();
            pgTop.CurrentPageNumber = pageNumber;
            pgTop.DrawControl ();

            // populate bottom pager
            pgBottom.NumberOfPages = Math.Ceiling ((double) pdtChannels.TotalCount / _PGSIZE).ToString ();
            pgBottom.CurrentPageNumber = pageNumber;
            pgBottom.DrawControl ();

            // The results
            lblResults_Top.Text = lblResults_Top.Text = GetResultsText (pdtChannels.TotalCount, pageNumber, _PGSIZE, pdtChannels.Rows.Count);
            lblResults_Bottom.Text = lblResults_Top.Text;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnUpdate_Click (object sender, EventArgs e) 
		{
			CheckBox chkEdit;
			HtmlInputHidden hidChannelId, hidAssetId;

			// Server validation
			if (Page.IsValid) 
			{
				foreach (RepeaterItem rptChannel in rptChannels.Items)
				{
					chkEdit = (CheckBox) rptChannel.FindControl ("chkEdit");

					if (chkEdit.Checked)
					{
						hidChannelId = (HtmlInputHidden) rptChannel.FindControl ("hidChannelId");

						int channelId = Convert.ToInt32 (hidChannelId.Value);

						// Moderators are allowed to upload
						if (CommunityUtility.IsCommunityModerator (channelId, GetUserId ()))
						{
							foreach (RepeaterItem rpiAsset in repeaterAssets.Items)
							{
								hidAssetId = (HtmlInputHidden) rpiAsset.FindControl ("hidAssetId");
								StoreUtility.InsertAssetChannel (Convert.ToInt32 (hidAssetId.Value), channelId);
							}
						}
						else
						{
							// Does the channel allow uploads?
							if (CommunityUtility.IsChannelPublishable (channelId))
							{
								foreach (RepeaterItem rpiAsset in repeaterAssets.Items)
								{
									hidAssetId = (HtmlInputHidden) rpiAsset.FindControl ("hidAssetId");
									StoreUtility.InsertAssetChannel (Convert.ToInt32 (hidAssetId.Value), channelId);
								}
							}
						}
					}
				}
			}
			Response.Redirect (ResolveUrl ("~/asset/publishedItemsNew.aspx?communityId=" + this._channelId));
		}

		/// <summary>
		/// Cancel click
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnCancel_Click (object sender, EventArgs e) 
		{
			Response.Redirect (ResolveUrl ("~/asset/publishedItemsNew.aspx?communityId=" + this._channelId));
		}

        /// <summary>
        /// Click event for Pager
        /// </summary>
        protected void pg_PageChange (object sender, PageChangeEventArgs e)
        {
            // get the inventory items
            BindData (e.PageNumber);
        }
		
		protected Repeater repeaterAssets, rptChannels;

		protected CheckBox chkAdvanced;
		protected Label lblChannel;
		protected HyperLink hlShowMedia;
        protected Label lblResults_Top, lblResults_Bottom;
		
		private string _assetIds;
		private int		_channelId;

        protected Pager pgTop, pgBottom;

        const int _PGSIZE = 20;


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}