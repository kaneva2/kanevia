///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva.channel;
using KlausEnt.KEP.Kaneva.framework.widgets;
using KlausEnt.KEP.Kaneva.mykaneva;
using log4net;
using System.Threading;
using MagicAjax;

using System.Collections.Generic;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
    /// <summary>
    /// Summary description for publishedItemsNew.
    /// </summary>
    public class publishedItemsNew : StoreBasePage
    {
        #region Declarations

        protected Kaneva.Pager pgTop, pgBottom;
        protected Kaneva.StoreFilter filStore, pageSort;
        protected PlaceHolder phBreadCrumb;
        protected Repeater rptAssets, rptPlayList;
        protected HtmlInputHidden AssetId, hdnPlayListId;
        protected ImageButton imgbtnAddGroup;
        protected Button btnCancelG, btnSaveG;
        protected HtmlInputText inpAG;
        protected HtmlGenericControl spnGN, spnUpload;
        protected HtmlAnchor aShowGroups, aUpload;
        protected LinkButton btnSearch;

        // STAR
        protected LinkButton lnkStarDirectory, lnkWOKCatalog;

        // Filter
        protected DropDownList drpCategory, drpMonth, drpAuthor, drpView, drpPages;
        protected Label lblSearch, lblSearch2, messages, lblSelectedMediaList;
        protected DropDownList ddlChannels;

        protected LinkButton lnkRemoveFromPlayList, lnkDeletePlayList, lbnSaveSortOrder, lbnSaveSortOrder2;

        private const int PLAYLIST_MODE = 1;
        private const int SMARTLIST_MODE = 2;

        protected LinkButton hlAll;
        protected LinkButton hlVideo;
        protected LinkButton hlPhoto;
        protected LinkButton hlMusic;
        protected LinkButton hlGames;
        protected LinkButton hlPatterns;
        protected LinkButton hlConnectedMedia;
        protected LinkButton hlTV;
        protected LinkButton hlWidgets;

        protected Literal ltrAllCount;
        protected Literal ltrVideoCount;
        protected Literal ltrPhotoCount;
        protected Literal ltrMusicCount;
        protected Literal ltrGamesCount;
        protected Literal ltrPatternCount;
        protected Literal ltrTVCount;
        protected Literal ltrWidgetsCount;
        protected Literal ltrConnectedMediaCount, litSaveSortButtonImage, litSaveSortButtonImage2;

        protected HtmlTableCell tdFilterByChannel, tdOrderHeading, tdOrder;
        protected HtmlTableRow trPlayListvalues, trPlayListAdd, trEditPlaylist;
        protected HtmlContainerControl pnlConnectedMedia;
        protected System.Web.UI.HtmlControls.HtmlGenericControl all, videos, tv, photos, music, widgets, games, patterns, shared;
        protected HtmlImage imgMediaLibrary;
        protected Label lblChannelName, playListTitle, playListDescription, shuffleText;
        protected System.Web.UI.HtmlControls.HtmlGenericControl spanMediaLibrary, spanMyMediaLibrary;

        private int _channelId;
        private int _channelOwnerId;
        private bool _isPersonalChannel;
        private bool _userCanEdit;
        protected System.Web.UI.WebControls.Button DeleteAsset;
        protected MagicAjax.UI.Controls.AjaxPanel ajMediaLibrary;
        protected System.Web.UI.HtmlControls.HtmlImage Img1;
        protected System.Web.UI.HtmlControls.HtmlImage Img2;
        protected System.Web.UI.HtmlControls.HtmlImage Img4;
        protected System.Web.UI.HtmlControls.HtmlImage Img5;
        protected System.Web.UI.HtmlControls.HtmlImage Img6;
        protected System.Web.UI.WebControls.DropDownList Dropdownlist1;
        protected System.Web.UI.WebControls.DropDownList ddlFiles2PlayList;
        protected System.Web.UI.WebControls.DropDownList ddlEditMultiples;
        protected System.Web.UI.WebControls.LinkButton lnkAddChannel, lnkNewPlaylist;
        protected System.Web.UI.WebControls.LinkButton lnkDeleteMedia, editPlayList;
        protected System.Web.UI.WebControls.LinkButton lbnEditMultiple, lbnFiles2PlayList;
        private string _channelName;
        PagedList<AssetGroup> pdtAssetGroups;
        protected System.Web.UI.HtmlControls.HtmlAnchor A1;
        protected System.Web.UI.HtmlControls.HtmlImage Img3;
        protected System.Web.UI.HtmlControls.HtmlGenericControl Ul1;
        protected System.Web.UI.WebControls.Button Button1, btnCancel, btnSavePlayList;
        protected System.Web.UI.WebControls.TextBox TextBox1, tbxPlayListName, tbxPlayListDescription, txtKeywords;
        protected System.Web.UI.HtmlControls.HtmlGenericControl playlistnav;
        protected System.Web.UI.HtmlControls.HtmlImage Img7;
        protected RadioButtonList rblShuffle;

        public int ChannelId
        {
            get { return _channelId; }
            set { _channelId = value; }
        }

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #endregion

        protected publishedItemsNew()
        {
            Title = "My Published Items";
        }

        #region PageLoad

        private void Page_Load(object sender, System.EventArgs e)
        {
            if (Request.IsAuthenticated)
            {
                if (!GetRequestParams())
                {
                    RedirectToHomePage();
                }
                else
                {
                    _userCanEdit = IsUserAllowedToEdit();
                    //check if user is allowed to edit this channel
                    if (!_userCanEdit)
                    {
                        RedirectToHomePage();
                    }
                }
            }
            else
            {
                //must log in to do this
                Response.Redirect(this.GetLoginURL());
            }

            //setup header nav bar
            if (_channelId == this.GetPersonalChannelId())
            {
                //user's own channel
                HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.MEDIA_UPLOADS;
            }
            else
            {
                //navs go to home-my channels for all channels if the user is an admin
                HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.CHANNELS;
            }


            // Set Nav
            HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
            HeaderNav.SetNavVisible(HeaderNav.MyKanevaNav, 2);

            //if this is from a personal channel, upload the file as the channel owner
            //otherwise upload as the logged in user
            aUpload.HRef = "~/mykaneva/upload.aspx?communityId=" + _channelId + "&userId=" +
                (_isPersonalChannel ? this._channelOwnerId : GetUserId());

            if (!string.IsNullOrWhiteSpace(FromObjPlacementId) &&
                    !string.IsNullOrWhiteSpace(FromZoneInstanceId) &&
                    !string.IsNullOrWhiteSpace(FromZoneType))
            {
                aUpload.HRef += "&FromGameplayMode=" + FromGameplayMode +
                "&FromObjPlacementId=" + FromObjPlacementId +
                "&FromZoneInstanceId=" + FromZoneInstanceId +
                "&FromZoneType=" + FromZoneType;
            }

            spanMyMediaLibrary.Visible = _isPersonalChannel;
            spanMediaLibrary.Visible = !_isPersonalChannel;
            pnlConnectedMedia.Visible = _isPersonalChannel;

            if (!_isPersonalChannel)
            {
                lblChannelName.Text = _channelName;
            }

            if (!IsPostBack)
            {
                // Set the category on the filter
                pgTop.CurrentPageNumber = 1;
                pgBottom.CurrentPageNumber = 1;

                lblSelectedMediaList.Text = "All";

                BindPlayList();
                BindMediaData(1);

                //NOTE: must come after playlist bind
                LoadDropdowns();

                //set the number to display per page to the max
                //set alert box to delete button
                StringBuilder sb = new StringBuilder();
                sb.Append(" javascript: ");
                sb.Append(" if(!confirm('Are you sure you want to delete the selected item(s) from your media library? \\n This cannot be undone.')) return false;");
                lnkDeleteMedia.Attributes.Add("onClick", sb.ToString());

                //set alert box to remove button
                StringBuilder sb2 = new StringBuilder();
                sb2.Append(" javascript: ");
                sb2.Append(" if(!confirm('Are you sure you want to remove the selected item(s) from your playlist? \\n These items will not be deleted from your library.')) return false;");
                lnkRemoveFromPlayList.Attributes.Add("onClick", sb2.ToString());

                //set alert box to remove button
                StringBuilder sb3 = new StringBuilder();
                sb3.Append(" javascript: ");
                sb3.Append(" if(!confirm('Are you sure you want to delete the current playlist? \\n This cannot be undone.')) return false;");
                lnkDeletePlayList.Attributes.Add("onClick", sb3.ToString());

                //filter by channels that have assets owned by a person
                tdFilterByChannel.Visible = this._isPersonalChannel;
                pnlConnectedMedia.Visible = this._isPersonalChannel;

            }

            //ResetBreadCrumb ();
            AddBreadCrumb(new BreadCrumb("My Published Items", GetCurrentURL(), "", 0));

            //clear message
            messages.Text = "";

        }
        #endregion

        #region Initial Populations (Pulldowns)

        //populates the add media to playlist drop down
        private void Load_AM2PL_DropDown()
        {
            //clear list to prevent duplication
            ddlFiles2PlayList.Items.Clear();

            //if any playlist found add them to the dropdown
            if (pdtAssetGroups.Count > 0)
            {
                ddlFiles2PlayList.Enabled = true;
                lbnFiles2PlayList.Enabled = true;

                ddlFiles2PlayList.Items.Add(new ListItem("Add to playlist:", ""));
                for (int i = 0; i < pdtAssetGroups.Count; i++)
                {
                    ddlFiles2PlayList.Items.Add(new ListItem(pdtAssetGroups[i].Name + " (" + pdtAssetGroups[i].AssetCount.ToString() + ")", pdtAssetGroups[i].AssetGroupId.ToString()));
                }
                if (ddlFiles2PlayList.Items.Count > 0)
                {
                    this.ddlFiles2PlayList.SelectedIndex = 0;
                }
            }
            else
            {
                ddlFiles2PlayList.Enabled = false;
                lbnFiles2PlayList.Enabled = false;
            }
        }

        //populates the edit multiple media drop down
        private void Load_EMM_DropDown()
        {

            if (this._isPersonalChannel)
            {
                //disable multi edit and batch edit in broadband channel
                ddlEditMultiples.Items.Add(new ListItem("Edit multiple Items:", ""));
                ddlEditMultiples.Items.Add(new ListItem("Multi-edit selected item(s)", "me1"));
                ddlEditMultiples.Items.Add(new ListItem("Batch edit selected item(s)", "be2"));
                ddlEditMultiples.Enabled = true;
                lbnEditMultiple.Enabled = true;
            }
            else
            {
                ddlEditMultiples.Enabled = false;
                lbnEditMultiple.Enabled = false;
            }
        }

        //populates the Filter By Community drop down
        private void Load_FBC_Dropdown()
        {
            if (_isPersonalChannel)
            {
                CommunityFacade communityFacade = new CommunityFacade();
                ddlChannels.Items.Clear();

                PagedList<Community> dlCommunity = communityFacade.GetUserCommunities(KanevaWebGlobals.CurrentUser.UserId, "", "place_type_id<99", 1, 50);

                if (dlCommunity.Count > 0)
                {
                    ddlChannels.Enabled = true;

                    ddlChannels.DataSource = dlCommunity;
                    ddlChannels.DataTextField = "Name";
                    ddlChannels.DataValueField = "CommunityId";
                    ddlChannels.DataBind();

                    ddlChannels.Items.Insert(0, new ListItem("All", "0"));
                }
                else
                {
                    ddlChannels.Enabled = false;
                }
            }
            else
            {
                ddlChannels.Enabled = false;
            }
        }

        //convenience function
        protected void LoadDropdowns()
        {
            Load_FBC_Dropdown();
            Load_EMM_DropDown();
            Load_AM2PL_DropDown();
        }

        #endregion

        #region Attributes/Properties

        /// <summary>
        /// FromGameplayMode
        /// </summary>
        private string FromGameplayMode
        {
            set
            {
                ViewState["FromGameplayMode"] = value.Trim();
            }
            get
            {
                if (ViewState["FromGameplayMode"] == null)
                {
                    ViewState["FromGameplayMode"] = (Request.Params["FromGameplayMode"] ?? string.Empty);
                }

                return ViewState["FromGameplayMode"].ToString();
            }
        }

        /// <summary>
        /// FromObjPlacementId
        /// </summary>
        private string FromObjPlacementId
        {
            set
            {
                ViewState["FromObjPlacementId"] = value.Trim();
            }
            get
            {
                if (ViewState["FromObjPlacementId"] == null)
                {
                    ViewState["FromObjPlacementId"] = (Request.Params["FromObjPlacementId"] ?? string.Empty);
                }

                return ViewState["FromObjPlacementId"].ToString();
            }
        }

        /// <summary>
        /// FromZoneInstanceId
        /// </summary>
        private string FromZoneInstanceId
        {
            set
            {
                ViewState["FromZoneInstanceId"] = value.Trim();
            }
            get
            {
                if (ViewState["FromZoneInstanceId"] == null)
                {
                    ViewState["FromZoneInstanceId"] = (Request.Params["FromZoneInstanceId"] ?? string.Empty);
                }

                return ViewState["FromZoneInstanceId"].ToString();
            }
        }

        /// <summary>
        /// FromZoneType
        /// </summary>
        private string FromZoneType
        {
            set
            {
                ViewState["FromZoneType"] = value.Trim();
            }
            get
            {
                if (ViewState["FromZoneType"] == null)
                {
                    ViewState["FromZoneType"] = (Request.Params["FromZoneType"] ?? string.Empty);
                }

                return ViewState["FromZoneType"].ToString();
            }
        }

        private bool FromFrameworkEnabled
        {
            get
            {
                return !string.IsNullOrWhiteSpace(FromGameplayMode);
            }
        }

        /// <summary>
        /// SearchString
        /// </summary>
        private string SearchString
        {
            set
            {
                ViewState["SearchString"] = value.Trim();
            }
            get
            {
                if (ViewState["SearchString"] == null)
                {
                    return string.Empty; //cORDER_BY_DEFAULT;
                }
                else
                {
                    return ViewState["SearchString"].ToString();
                }
            }
        }

        protected int ORDER_COUNT
        {
            get
            {
                if (ViewState["countOrder"] == null)
                {
                    ViewState["countOrder"] = ((pgTop.CurrentPageNumber - 1) * filStore.NumberOfPages) + 1;
                }
                return (int)ViewState["countOrder"];
            }
            set
            {
                ViewState["countOrder"] = value;
            }
        }

        /// <summary>
        /// Current page number
        /// </summary>
        protected NameValueCollection ASSET_ORDERING
        {
            get
            {
                if (ViewState["assetOrder"] == null)
                {
                    ViewState["assetOrder"] = new NameValueCollection();
                }
                return (NameValueCollection)ViewState["assetOrder"];
            }
            set
            {
                ViewState["assetOrder"] = value;
            }
        }

        protected NameValueCollection ASSET_ORDERING_CHANGES
        {
            get
            {
                if (ViewState["assetOrderChanges"] == null)
                {
                    ViewState["assetOrderChanges"] = new NameValueCollection();
                }
                return (NameValueCollection)ViewState["assetOrderChanges"];
            }
            set
            {
                ViewState["assetOrderChanges"] = value;
            }
        }

        /// <summary>
        /// Current page configuration
        /// </summary>
        protected int CURRENT_PAGE_CONFIGURATION
        {
            get
            {
                if (ViewState["currentConfig"] == null)
                {
                    ViewState["currentConfig"] = SMARTLIST_MODE;
                    return SMARTLIST_MODE;
                }
                else
                {
                    return (int)ViewState["currentConfig"];
                }
            }
            set
            {
                ViewState["currentConfig"] = value;
            }
        }

        /// <summary>
        /// Current filtering
        /// </summary>
        protected string CURRENT_FILTERING
        {
            get
            {
                if (ViewState["returnFilter"] == null)
                {
                    return "";
                }
                else
                {
                    return ViewState["returnFilter"].ToString();
                }
            }
            set
            {
                ViewState["returnFilter"] = value;
            }
        }

        /// <summary>
        /// Current community filtering
        /// </summary>
        protected string CURRENT_COMMUNITY_FILTERING
        {
            get
            {
                if (ViewState["communityFilter"] == null)
                {
                    return "";
                }
                else
                {
                    return ViewState["communityFilter"].ToString();
                }
            }
            set
            {
                ViewState["communityFilter"] = value;
            }
        }

        /// <summary>
        /// DEFAULT_SORT
        /// </summary>
        /// <returns></returns>
        protected override string DEFAULT_SORT
        {
            get
            {
                if (!IsPostBack && Request[Constants.PARAM_SORT] != null)
                {
                    return Request[Constants.PARAM_SORT].ToString();
                }
                else
                {
                    return "date_added";
                }
            }
        }

        private string SelectedPlayList
        {
            get
            {
                if (ViewState["PlayListId"] == null)
                {
                    return "-1";
                }
                else
                {
                    return ViewState["PlayListId"].ToString();
                }
            }
            set
            {
                ViewState["PlayListId"] = value;
            }
        }


        /// <summary>
        /// GetGroupType
        /// </summary>
        private string SelectedSmartList
        {
            get
            {
                if ((ViewState["assetTypeId"] == null) || (ViewState["assetTypeId"].ToString() == ""))
                {
                    return ((int)Constants.eASSET_TYPE.ALL).ToString();
                }
                else
                {
                    return ViewState["assetTypeId"].ToString();
                }
            }
            set
            {
                ViewState["assetTypeId"] = value;
            }
        }

        /// <summary>
        /// DEFAULT_SORT_ORDER
        /// </summary>
        /// <returns></returns>
        protected override string DEFAULT_SORT_ORDER
        {
            get
            {
                if (!IsPostBack && Request[Constants.PARAM_SORT_ORDER] != null)
                {
                    return Request[Constants.PARAM_SORT_ORDER].ToString();
                }
                else
                {
                    return "DESC";
                }
            }
        }

        protected string MEDIALIBRARY_SORT
        {
            get
            {
                return "sort_order asc, date_added desc";
            }
        }


        #endregion

        #region Helper Functions

        private void LoadAssetOrdering(string assetId, TextBox sortField, HtmlInputHidden hidSortOrig)
        {
            //assumes that assets are in order by sort_order then date created
            //get the start number for the search
            int startNumber = ((pgTop.CurrentPageNumber - 1) * filStore.NumberOfPages) + 1;

            //search for the next available order
            //bool found = false; 
            //ORDER_COUNT = startNumber;
            short index = 1;

            //while((ORDER_COUNT < (startNumber + filStore.NumberOfPages)) && (!found))
            {
                if (ASSET_ORDERING[ORDER_COUNT.ToString()] == null)
                {
                    ASSET_ORDERING.Add(ORDER_COUNT.ToString(), assetId);
                }
                else
                {
                    ASSET_ORDERING[ORDER_COUNT.ToString()] = assetId;
                }
                //found = true;
                sortField.Text = ORDER_COUNT.ToString();
                sortField.TabIndex = index;
                hidSortOrig.Value = sortField.Text;
                ORDER_COUNT++;
                index++;
            }

            /*ASSET_ORDERING.Add(Convert.ToString(ASSET_ORDERING.Count + 1),assetId);
				sortField.Text = ASSET_ORDERING.Count.ToString();
				sortField.TabIndex = (short)ASSET_ORDERING.Count;
				hidSortOrig.Value = sortField.Text;*/
        }

        private void ClearPlayListSelections()
        {
            if (rptPlayList != null)
            {
                for (int i = 0; i < rptPlayList.Items.Count; i++)
                {
                    if (rptPlayList.Items[i].ItemType == ListItemType.Item || rptPlayList.Items[i].ItemType == ListItemType.AlternatingItem)
                    {
                        HtmlGenericControl liCategory = (HtmlGenericControl)rptPlayList.Items[i].FindControl("Li1");
                        liCategory.Attributes.Remove("class");
                    }
                }
            }
        }

        private void StoreSortOrderChange(string sortOrder, string data)
        {
            NameValueCollection temp = ASSET_ORDERING_CHANGES;
            string orderFound = null;

            //check to see if that order posistion has already been marked for change
            try
            {
                orderFound = temp[sortOrder];
            }
            catch (Exception)
            {
            }
            //if you find itreplace it otherwise add it
            if (orderFound != null)
            {
                temp[sortOrder] = data;
            }
            else
            {
                temp.Add(sortOrder, data);
            }

        }


        /// <summary>
        /// sorts assets using three arrays
        /// </summary>
        /// <param name="asset_id"></param>
        /// <param name="sortOrder"></param>
        private void SortAssets()
        {
            NameValueCollection originalOrder = ASSET_ORDERING;
            NameValueCollection changes = ASSET_ORDERING_CHANGES;
            NameValueCollection newOrder = new NameValueCollection();
            int size = originalOrder.Count;

            //insert changes in new order list
            foreach (string newPosition in changes)
            {
                string origPosition = changes[newPosition];
                string assetId = originalOrder[origPosition];

                //add to new list
                newOrder.Add(newPosition, assetId);
                //remove from orignal
                originalOrder.Remove(origPosition);
            }

            //allocate remaining original values
            foreach (string originalPosition in originalOrder)
            {
                //try its orginal position
                string found = null;
                try
                {
                    //forward search limit search to original array size
                    found = newOrder[originalPosition];
                    int ogP = Convert.ToInt32(originalPosition);
                    while ((found != null) && (ogP < size))
                    {
                        ogP++;
                        found = newOrder[ogP.ToString()];
                    }

                    //backward search if needed
                    if (found != null)
                    {
                        while ((found != null) && (ogP > 0))
                        {
                            ogP--;
                            found = newOrder[ogP.ToString()];
                        }
                    }

                    if (found == null)
                    {
                        newOrder.Add(ogP.ToString(), originalOrder[originalPosition]);
                    }

                }
                catch (Exception)
                {
                }
            }

            //Update view state
            ASSET_ORDERING = newOrder;
        }


        private void ClearSmartListClasses()
        {
            all.Attributes.Remove("class");
            videos.Attributes.Remove("class");
            music.Attributes.Remove("class");
            photos.Attributes.Remove("class");
            patterns.Attributes.Remove("class");
            games.Attributes.Remove("class");
            shared.Attributes.Remove("class");
            tv.Attributes.Remove("class");
            widgets.Attributes.Remove("class");
        }


        public bool ShowOrderField()
        {
            bool show = true;
            if (Convert.ToInt32(SelectedPlayList) > 0)
            {
                show = true;
            }
            else
            {
                //highlight selected media type and set order display
                switch (Convert.ToInt32(SelectedSmartList))
                {
                    case (int)Constants.eASSET_TYPE.WIDGET:
                    case (int)Constants.eASSET_TYPE.TV:
                    case (int)Constants.eASSET_TYPE.VIDEO:
                    case (int)Constants.eASSET_TYPE.MUSIC:
                    case (int)Constants.eASSET_TYPE.PICTURE:
                    case (int)Constants.eASSET_TYPE.PATTERN:
                    case (int)Constants.eASSET_TYPE.GAME:
                        show = true;
                        break;
                    case (int)Constants.eASSET_TYPE.ALL:
                    default: //using this to populate shared since it is not a media type
                        show = false;
                        break;
                }

            }
            return show;
        }

        protected int GetChannelId()
        {
            return _channelId;
        }

        private void SavePlayList()
        {
            //check for required name
            if (tbxPlayListName.Text.Length == 0)
            {
                AjaxCallHelper.WriteAlert("You must enter a name for your playlist.");
                return;
            }

            // Check for any inject scripts
            if ((KanevaWebGlobals.ContainsInjectScripts(tbxPlayListName.Text)) || (KanevaWebGlobals.ContainsInjectScripts(tbxPlayListDescription.Text)))
            {
                m_logger.Warn("User " + KanevaWebGlobals.GetUserId() + " tried to script from IP " + Common.GetVisitorIPAddress());
                ShowErrorOnStartup("Your input contains invalid scripting, please remove script code and try again.", true);
                return;
            }

            //get playlist id 
            int playListId = hdnPlayListId.Value != "" ? Convert.ToInt32(hdnPlayListId.Value) : -1;

            //update or insert based on id
            if (playListId > 0)
            {
                //give edit area the new values
                playListTitle.Text = tbxPlayListName.Text;
                playListDescription.Text = tbxPlayListDescription.Text;
                shuffleText.Text = "Shuffle:" + rblShuffle.SelectedItem.Text;

                //update playlist data
                StoreUtility.UpdateAssetGroup(_channelId, playListId, Server.HtmlEncode(tbxPlayListName.Text), Server.HtmlEncode(tbxPlayListDescription.Text), int.Parse(rblShuffle.SelectedItem.Value));

                // DRF - ED-7522 - Web should tickle servers on playlist changes
                (new RemoteEventSender()).BroadcastItemChangeEvent(RemoteEventSender.ObjectType.MediaPlaylist, RemoteEventSender.ChangeType.UpdateObject, playListId);

                // Record metrics 
                if (!string.IsNullOrWhiteSpace(FromObjPlacementId) &&
                    !string.IsNullOrWhiteSpace(FromZoneInstanceId) &&
                    !string.IsNullOrWhiteSpace(FromZoneType))
                {
                    Dictionary<string, string> metricData = new Dictionary<string, string>
                    {
                        {"interaction", "modifiedPlaylistOnWeb" },
                        {"username", KanevaWebGlobals.CurrentUser.Username},
                        {"objPlacementId", FromObjPlacementId},
                        {"frameworkEnabled", FromFrameworkEnabled.ToString()},
                        {"playerModeActive", (FromGameplayMode.Equals("player", StringComparison.InvariantCultureIgnoreCase)).ToString()},
                        {"zoneInstanceId", FromZoneInstanceId},
                        {"zoneType", FromZoneType},
                    };
                    (new MetricsFacade()).InsertGenericMetricsLog("media_player_interaction_log", metricData, Global.RequestRabbitMQChannel());
                }
            }
            else
            {
                GetMediaFacade.InsertAssetGroup(_channelId, Server.HtmlEncode(tbxPlayListName.Text), Server.HtmlEncode(tbxPlayListDescription.Text), int.Parse(rblShuffle.SelectedItem.Value));

                // Record metrics 
                if (!string.IsNullOrWhiteSpace(FromObjPlacementId) &&
                    !string.IsNullOrWhiteSpace(FromZoneInstanceId) &&
                    !string.IsNullOrWhiteSpace(FromZoneType))
                {
                    Dictionary<string, string> metricData = new Dictionary<string, string>
                    {
                        {"interaction", "createdPlaylistOnWeb" },
                        {"username", KanevaWebGlobals.CurrentUser.Username},
                        {"objPlacementId", FromObjPlacementId},
                        {"frameworkEnabled", FromFrameworkEnabled.ToString()},
                        {"playerModeActive", (FromGameplayMode.Equals("player", StringComparison.InvariantCultureIgnoreCase)).ToString()},
                        {"zoneInstanceId", FromZoneInstanceId},
                        {"zoneType", FromZoneType},
                    };
                    (new MetricsFacade()).InsertGenericMetricsLog("media_player_interaction_log", metricData, Global.RequestRabbitMQChannel());
                }
            }

            //clear needed fields
            tbxPlayListName.Text = "";
            tbxPlayListDescription.Text = "";
            trPlayListAdd.Visible = true;
            trPlayListvalues.Visible = false;
            rblShuffle.SelectedIndex = 0;

            //update playlist
            BindPlayList();
            Load_AM2PL_DropDown();

            //message to user
            messages.ForeColor = System.Drawing.Color.DarkGreen;
            messages.Text = "Your play list changes were successfully saved.";
            MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('messages', 5000);");
        }

        private void SetPageConfig(int config)
        {

            if (this._isPersonalChannel)
            {
                ddlEditMultiples.Enabled = true;
                lbnEditMultiple.Enabled = true;
            }


            if (config == PLAYLIST_MODE)
            {
                //configure action buttons
                lnkRemoveFromPlayList.Visible = true;
                lnkDeletePlayList.Visible = true;
                lnkDeleteMedia.Visible = false;
                ddlFiles2PlayList.Visible = false;
                lbnFiles2PlayList.Visible = false;

                //make sort order buttons visible
                lbnSaveSortOrder.Visible = lbnSaveSortOrder2.Visible = true;

                //reset the smart list media type
                SelectedSmartList = null;

                //reset the filter area
                ResetFilters();

            }
            else if (config == SMARTLIST_MODE)
            {
                //equivalent to resetting field - precautionary action
                hdnPlayListId.Value = "-1";

                //configure action buttons
                lnkRemoveFromPlayList.Visible = false;
                lnkDeletePlayList.Visible = false;
                lnkDeleteMedia.Visible = true;
                ddlFiles2PlayList.Visible = true;
                lbnFiles2PlayList.Visible = true;

                //override the playlist
                SelectedPlayList = null;

                //hide the playlist information
                trEditPlaylist.Visible = false;

                //hide new playlist fields if visible
                CancelNewPlayList();
            }
        }

        private void SetPlayListEditArea(RepeaterCommandEventArgs e)
        {
            //display the edit area
            trEditPlaylist.Visible = true;

            //get the playlist info for the edit area
            playListTitle.Text = ((HtmlInputHidden)e.Item.FindControl("hdnPlayListName")).Value;
            playListDescription.Text = " - " + ((HtmlInputHidden)e.Item.FindControl("hdnPlayListDescription")).Value;
            hdnPlayListId.Value = e.CommandArgument.ToString();
            lblSelectedMediaList.Text = "PlayList: " + ((HtmlInputHidden)e.Item.FindControl("hdnPlayListName")).Value;

            shuffleText.Text = "Shuffle:" + ((((HtmlInputHidden)e.Item.FindControl("hdnShuffle")).Value.Equals("0")) ? "Off" : "On");
        }

        private void CancelNewPlayList()
        {
            trPlayListAdd.Visible = true;
            trPlayListvalues.Visible = false;
        }

        private void ResetFilters()
        {
            if (ddlChannels.Items.Count > 0)
            {
                this.ddlChannels.SelectedIndex = 0;
            }
            CURRENT_COMMUNITY_FILTERING = "";
            DropDownList curentFilter = (DropDownList)pageSort.FindControl("drpSort");
            if (curentFilter.Items.Count > 0)
            {
                curentFilter.SelectedIndex = 0;
            }
            CURRENT_FILTERING = "";
        }

        public string GetPlayListCounts(string counts)
        {
            return "(" + counts + ")";
        }

        private bool GetRequestParams()
        {
            bool retVal = true;
            try
            {
                if (Request["communityId"] != null)
                {
                    //get channelId and find the default page
                    _channelId = Convert.ToInt32(Request["communityId"]);
                    DataRow drChannel = CommunityUtility.GetCommunity(_channelId);
                    _isPersonalChannel = drChannel["is_personal"].ToString().Equals("1");
                    _channelName = drChannel["name"].ToString();
                    _channelOwnerId = Convert.ToInt32(drChannel["creator_id"]);
                }
                else
                {
                    _channelId = GetPersonalChannelId();
                    _channelOwnerId = GetUserId();
                    _isPersonalChannel = true;
                }
            }
            catch (Exception)
            {
                //invalid numbers
                retVal = false;
            }
            return retVal;
        }

        /// <summary>
        /// returns true if current user is allowed to edit this channel
        /// </summary>
        /// <returns></returns>
        private bool IsUserAllowedToEdit()
        {
            return IsAdministrator() || CommunityUtility.IsCommunityModerator(
                _channelId, GetUserId());
        }

        /// <summary>
        /// Get a string
        /// </summary>
        public string FormatString(Object dtColumn, int statusId)
        {
            return FormatString(dtColumn, statusId, "");
        }

        /// <summary>
        /// Get a string
        /// </summary>
        public string FormatString(Object dtColumn, int statusId, string notSetText)
        {
            if (statusId.Equals((int)Constants.eASSET_STATUS.NEW))
                return "<span class=\"offline\">" + notSetText + "</span>";

            return dtColumn.ToString();
        }

        /// <summary>
        /// GetAmount
        /// </summary>
        /// <param name="dtAmount"></param>
        /// <returns></returns>
        protected string GetAmount(Object dtAmount, int statusId)
        {
            //			if (statusId.Equals ((int) Constants.eASSET_STATUS.NEW))
            //				return FormatString (dtAmount, statusId, "No Price");

            return "<span class=\"money\">" + FormatKpoints(dtAmount) + "</span>";
        }

        /// <summary>
        /// Get the status link
        /// </summary>
        protected string GetStatusLink(int torrentStatus)
        {
            return "javascript:window.open('" + ResolveUrl("~/asset/publishStatus.aspx#" + torrentStatus + "','add','toolbar=no,width=600,height=450,menubar=no,scrollbars=yes,status=yes').focus();return false;");
        }

        /// <summary>
        /// IsDeleted
        /// </summary>
        protected bool IsDeleted(int statusId)
        {
            return (statusId.Equals((int)Constants.eASSET_STATUS.MARKED_FOR_DELETION));
        }

        #endregion

        #region Primary Functions


        /// <summary>
        /// adds selected files to selected playlist
        /// </summary>
        private void AddFiles2PlayList()
        {
            try
            {
                string selectedValue = ddlFiles2PlayList.SelectedValue;
                // Add the friends to a group
                if ((selectedValue == "") || (selectedValue == null))
                {
                    messages.ForeColor = System.Drawing.Color.DarkRed;
                    messages.Text = "No playlist was selected.";
                    MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('messages', 5000);");
                    return;
                }
                else if (KanevaGlobals.IsNumeric(selectedValue))
                {
                    int assetGroupId = Convert.ToInt32(ddlFiles2PlayList.SelectedValue);
                    InsertAssetsInGroup(assetGroupId);
                }
            }
            catch (Exception ex)
            {
                m_logger.Error("Media Library error adding media to playlist.", ex);
                messages.ForeColor = System.Drawing.Color.DarkRed;
                messages.Text = "Submission Error: Sorry we were unable to add media to your playlist.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('messages', 5000);");
                return;
            }


            //rebind to get the new information
            Load_AM2PL_DropDown();
        }

        /// <summary>
        /// process the edit multiple commands
        /// </summary>
        private void EditMultipleItems()
        {
            if (ddlEditMultiples.SelectedValue.Equals("me1"))
            {
                try
                {
                    MultiEdit();
                }
                catch (Exception ex)
                {
                    m_logger.Error("Media Library error during multiedit submission.", ex);
                    messages.ForeColor = System.Drawing.Color.DarkRed;
                    messages.Text = "Edit Error: Sorry we were unable to process your multi edit request.";
                    MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('messages', 5000);");
                    return;
                }

            }
            else if (ddlEditMultiples.SelectedValue.Equals("be2"))
            {
                try
                {
                    BatchEdit();
                }
                catch (Exception ex)
                {
                    m_logger.Error("Media Library error during batch edit request.", ex);
                    messages.ForeColor = System.Drawing.Color.DarkRed;
                    messages.Text = "Loading Error: Sorry we were unable to process your batch edit request.";
                    MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('messages', 5000);");
                    return;
                }

            }
            else
            {
                messages.ForeColor = System.Drawing.Color.DarkRed;
                messages.Text = "Please select an editing option.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('messages', 5000);");
            }
            if (ddlEditMultiples.Items.Count > 0)
            {
                this.ddlEditMultiples.SelectedIndex = 0;
            }
        }

        /// <summary>
        /// sets the command values and names for the SmartNav menu
        /// </summary>
        private void SetSmartNav()
        {
            // Get the media counts
            DataRow drMediaCount = StoreUtility.GetChannelMediaCounts(_channelId);

            string sumTotal = (Convert.ToUInt32(drMediaCount["sum_video"]) +
                    Convert.ToUInt32(drMediaCount["sum_picture"]) +
                    Convert.ToUInt32(drMediaCount["sum_music"]) +
                    Convert.ToUInt32(drMediaCount["sum_game"]) +
                    Convert.ToUInt32(drMediaCount["sum_pattern"]) +
                    Convert.ToUInt32(drMediaCount["sum_widget"]) +
                    Convert.ToUInt32(drMediaCount["sum_tv"])).ToString();

            ltrAllCount.Text = "(" + sumTotal + ")";
            ltrVideoCount.Text = "(" + drMediaCount["sum_video"].ToString() + ")";
            ltrPhotoCount.Text = "(" + drMediaCount["sum_picture"].ToString() + ")";
            ltrMusicCount.Text = "(" + drMediaCount["sum_music"].ToString() + ")";
            ltrGamesCount.Text = "(" + drMediaCount["sum_game"].ToString() + ")";
            ltrPatternCount.Text = "(" + drMediaCount["sum_pattern"].ToString() + ")";
            ltrWidgetsCount.Text = "(" + drMediaCount["sum_widget"].ToString() + ")";
            ltrTVCount.Text = "(" + drMediaCount["sum_tv"].ToString() + ")";

            if (_isPersonalChannel)
            {
                //get the shared items count
                //string connectedCount = StoreUtility.GetConnectedMediaCount(_channelId).ToString();
                ltrConnectedMediaCount.Text = "(" + drMediaCount["sum_share"] + ")";
                //add the shared items count into te all count so they will match up
                //ltrAllCount.Text = "(" + drMediaCount["sum_total"].ToString() + ")";
            }

            //unselect any highlighted smartlist
            ClearSmartListClasses();

            //highlight selected media type and set order display
            //don't highlight anything if using a user playlist
            if (Convert.ToInt32(SelectedPlayList) <= 0)
            {
                switch (Convert.ToInt32(SelectedSmartList))
                {
                    case (int)Constants.eASSET_TYPE.ALL:
                        this.all.Attributes.Add("class", "selected");
                        lbnSaveSortOrder.Visible = lbnSaveSortOrder2.Visible = false;
                        break;
                    case (int)Constants.eASSET_TYPE.VIDEO:
                        this.videos.Attributes.Add("class", "selected");
                        lbnSaveSortOrder.Visible = lbnSaveSortOrder2.Visible = true;
                        break;
                    case (int)Constants.eASSET_TYPE.MUSIC:
                        this.music.Attributes.Add("class", "selected");
                        lbnSaveSortOrder.Visible = lbnSaveSortOrder2.Visible = true;
                        break;
                    case (int)Constants.eASSET_TYPE.PICTURE:
                        this.photos.Attributes.Add("class", "selected");
                        lbnSaveSortOrder.Visible = lbnSaveSortOrder2.Visible = true;
                        break;
                    case (int)Constants.eASSET_TYPE.PATTERN:
                        this.patterns.Attributes.Add("class", "selected");
                        lbnSaveSortOrder.Visible = lbnSaveSortOrder2.Visible = true;

                        //temporary "feature announcement"
                        if (ltrPatternCount.Text == "(0)")
                        {
                            messages.Text = "Change photos to patterns (for use in the virtual world) by clicking 'Edit.'";
                            messages.ForeColor = System.Drawing.Color.Blue;
                        }

                        break;
                    case (int)Constants.eASSET_TYPE.GAME:
                        this.games.Attributes.Add("class", "selected");
                        lbnSaveSortOrder.Visible = lbnSaveSortOrder2.Visible = true;
                        break;
                    case (int)Constants.eASSET_TYPE.TV:
                        this.tv.Attributes.Add("class", "selected");
                        lbnSaveSortOrder.Visible = lbnSaveSortOrder2.Visible = true;
                        break;
                    case (int)Constants.eASSET_TYPE.WIDGET:
                        this.widgets.Attributes.Add("class", "selected");
                        lbnSaveSortOrder.Visible = lbnSaveSortOrder2.Visible = true;
                        break;
                    default: //using this to populate shared since it is not a media type
                        this.shared.Attributes.Add("class", "selected");
                        lbnSaveSortOrder.Visible = lbnSaveSortOrder2.Visible = false;
                        if (!IsAdministrator())
                        {
                            ddlEditMultiples.Enabled = false;
                            lbnEditMultiple.Enabled = false;
                        }
                        break;
                }
            }

        }

        /// <summary>
        /// Delete selected items
        /// </summary>
        protected void DeleteChecked()
        {
            CheckBox chkEdit;
            HtmlInputHidden hidAssetId;
            int deletionCount = 0;

            foreach (RepeaterItem dgiAsset in rptAssets.Items)
            {
                chkEdit = (CheckBox)dgiAsset.FindControl("chkEdit");

                if (chkEdit.Checked)
                {
                    hidAssetId = (HtmlInputHidden)dgiAsset.FindControl("hidAssetId");
                    RemoveAsset(Convert.ToInt32(hidAssetId.Value));
                    deletionCount++;
                }
            }

            //message to user
            if (deletionCount == 0)
            {
                messages.ForeColor = System.Drawing.Color.DarkRed;
                messages.Text = "No items were selected.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('messages', 5000);");
            }
            else
            {
                BindMediaData(pgTop.CurrentPageNumber);
                //rebind for counts
                BindPlayList();
                Load_AM2PL_DropDown();
                messages.ForeColor = System.Drawing.Color.DarkGreen;
                messages.Text = deletionCount + " item" + (deletionCount > 1 ? "s were " : " was ") + "deleted from your library.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('messages', 5000);");
            }
        }

        private void RemoveAsset(int assetId)
        {
            //if the is a personal channel and user is the owner, then delete this asset
            //remove it from the channel otherwise
            //if(this._isPersonalChannel && (IsAdministrator() || StoreUtility.IsAssetOwner(GetUserId(), assetId)))
            if (this._isPersonalChannel && (StoreUtility.IsAssetOwner(GetUserId(), assetId)))
            {
                StoreUtility.DeleteAsset(assetId, GetUserId(), false);
                //check to see if there are any playlist with this item in them and remove from the playlist(asset_group_asset) as well
                GetMediaFacade.RemoveAssetFromAllGroups(assetId);
            }
            else
            {
                StoreUtility.RemoveAssetFromChannel(GetUserId(), assetId, this._channelId);
                //check to see if there are any playlist with this item in them and remove from the playlist(asset_group_asset) as well
                GetMediaFacade.RemoveAssetFromGroupsByChannel(_channelId, assetId);
            }

        }

        /// <summary>
        /// Multi Edit
        /// </summary>
        protected void MultiEdit()
        {
            CheckBox chkEdit;
            HtmlInputHidden hidAssetId;
            string assetIds = "";


            foreach (RepeaterItem dgiAsset in rptAssets.Items)
            {
                chkEdit = (CheckBox)dgiAsset.FindControl("chkEdit");

                if (chkEdit.Checked)
                {
                    hidAssetId = (HtmlInputHidden)dgiAsset.FindControl("hidAssetId");
                    if (assetIds.Length > 0)
                    {
                        assetIds += ",";
                    }
                    assetIds += hidAssetId.Value;
                }
            }

            if (assetIds.Length == 0)
            {
                messages.ForeColor = System.Drawing.Color.DarkRed;
                messages.Text = "No items were selected.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('messages', 5000);");
                return;
            }

            Response.Redirect(ResolveUrl("~/asset/batchEdit.aspx?assetIds=" + assetIds));
        }

        /// <summary>
        /// Batch Edit
        /// </summary>
        protected void BatchEdit()
        {
            CheckBox chkEdit;
            HtmlInputHidden hidAssetId;
            string assetIds = "";

            foreach (RepeaterItem dgiAsset in rptAssets.Items)
            {
                chkEdit = (CheckBox)dgiAsset.FindControl("chkEdit");

                if (chkEdit.Checked)
                {
                    hidAssetId = (HtmlInputHidden)dgiAsset.FindControl("hidAssetId");
                    if (assetIds.Length > 0)
                    {
                        assetIds += ",";
                    }
                    assetIds += hidAssetId.Value;
                }
            }

            if (assetIds.Length == 0)
            {
                messages.ForeColor = System.Drawing.Color.DarkRed;
                messages.Text = "No items were selected.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('messages', 5000);");
                return;
            }

            //set parameter indicating when to show not show type change field on batchOps page
            if (SelectedPlayList == "-1")
            {
                Response.Redirect(ResolveUrl("~/asset/batchOperations.aspx?assetIds=" + assetIds + "&assetType=" + Convert.ToInt32(SelectedSmartList)));
            }
            else
            {
                Response.Redirect(ResolveUrl("~/asset/batchOperations.aspx?assetIds=" + assetIds + "&assetType=" + ((int)Constants.eASSET_TYPE.ALL)));
            }
        }

        protected void SubmitToChannels()
        {
            CheckBox chkEdit;
            HtmlInputHidden hidAssetId;
            string assetIds = "";

            foreach (RepeaterItem dgiAsset in rptAssets.Items)
            {
                chkEdit = (CheckBox)dgiAsset.FindControl("chkEdit");

                if (chkEdit.Checked)
                {
                    hidAssetId = (HtmlInputHidden)dgiAsset.FindControl("hidAssetId");

                    // Add a comma?
                    if (assetIds.Length > 0)
                    {
                        assetIds += ",";
                    }
                    assetIds += hidAssetId.Value;
                }
            }


            if (assetIds.Length > 0)
            {
                Response.Redirect(ResolveUrl("~/asset/assetsSubmit.aspx?communityId=" + this._channelId + "&aIds=" + assetIds));
            }
            else
            {
                messages.ForeColor = System.Drawing.Color.DarkRed;
                messages.Text = "No items were selected.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('messages', 5000);");
            }

        }

        /// <summary>
        /// InsertAssetsInGroup
        /// </summary>
        protected void InsertAssetsInGroup(int assetGroupId)
        {
            CheckBox chkEdit;
            HtmlInputHidden hidAssetId;
            int userId = GetUserId();
            int addedMedia = 0;
            int assetId = 0;

            foreach (RepeaterItem dgiAsset in rptAssets.Items)
            {
                chkEdit = (CheckBox)dgiAsset.FindControl("chkEdit");

                if (chkEdit.Checked)
                {
                    try
                    {
                        assetId = Convert.ToInt32(((HtmlInputHidden)dgiAsset.FindControl("hidAssetId")).Value);
                        GetMediaFacade.InsertAssetInGroup(_channelId, assetGroupId, assetId);
                        GetMediaFacade.MoveAssetToBottomOfGroup(assetGroupId, assetId);
                        addedMedia++;
                        chkEdit.Checked = false;
                    }
                    catch { }
                }
            }

            //rebind playlist to get new counts
            BindPlayList();

            //message to user
            if (addedMedia == 0)
            {
                messages.ForeColor = System.Drawing.Color.DarkRed;
                messages.Text = "No items were selected.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('messages', 5000);");
                return;
            }
            else
            {
                // DRF - ED-7522 - Web should tickle servers on playlist changes
                int playListId = assetGroupId;
                (new RemoteEventSender()).BroadcastItemChangeEvent(RemoteEventSender.ObjectType.MediaPlaylist, RemoteEventSender.ChangeType.UpdateObject, playListId);

                //message to user
                messages.ForeColor = System.Drawing.Color.DarkGreen;
                messages.Text = addedMedia + " item" + (addedMedia > 1 ? "s were " : " was ") + "added to the selected playlist.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('messages', 5000);");
            }
        }

        private void BindPlayList()
        {
            //pull back all the play list
            pdtAssetGroups = GetMediaFacade.GetAssetGroups(_channelId, "", "", 1, Int32.MaxValue);

            //populate the users play list
            rptPlayList.DataSource = pdtAssetGroups;
            rptPlayList.DataBind();
        }

        private void BindMediaData(int pageNumber)
        {
            //sets page configuration
            ORDER_COUNT = ((pageNumber - 1) * filStore.NumberOfPages) + 1;
            SetPageConfig(CURRENT_PAGE_CONFIGURATION);

            SetSmartNav();

            this.tdOrderHeading.Visible = ShowOrderField();

            // Set the sortable columns
            //SetHeaderSortText (rptAssets);
            //string orderby = CurrentSort + " " + CurrentSortOrder;
            string orderby = MEDIALIBRARY_SORT;

            //reset the sortItem array
            if (pageNumber == 1)
            {
                ASSET_ORDERING = null;
                ASSET_ORDERING_CHANGES = null;
                lbnSaveSortOrder.Enabled = lbnSaveSortOrder2.Enabled = false;
                litSaveSortButtonImage.Text = litSaveSortButtonImage2.Text = "<img src=\"../images/button_savesort_x.gif\" border=\"0\">";

            }

            //get the appropriate media assets.
            PagedDataTable pds = null;
            MediaFacade mediaFacade = new MediaFacade();

            //set all filters
            NameValueCollection filter = new NameValueCollection();


            //is personal channel or community
            //if(_isPersonalChannel && channelFilterId > 0)
            if (_isPersonalChannel)
            {
                switch (Convert.ToInt32(SelectedSmartList))
                {
                    case (int)Constants.eASSET_TYPE.ALL:
                        break;
                    case (int)Constants.eASSET_TYPE.VIDEO:
                    case (int)Constants.eASSET_TYPE.MUSIC:
                    case (int)Constants.eASSET_TYPE.PICTURE:
                    case (int)Constants.eASSET_TYPE.PATTERN:
                    case (int)Constants.eASSET_TYPE.TV:
                    case (int)Constants.eASSET_TYPE.WIDGET:
                    case (int)Constants.eASSET_TYPE.GAME:
                        //filter.Add("a.owner_id", " = " + _channelOwnerId); 
                        break;
                    default: //using this to populate shared since it is not a media type
                        filter.Add("owner_id", " <> " + _channelOwnerId);
                        break;
                }
            }

            string communityFilterID = CURRENT_COMMUNITY_FILTERING;
            int fltChannelId = 0;
            if ((communityFilterID != "") && (communityFilterID != "0"))
            {
                //filter.Add("ac.channel_id", " = " + communityFilterID);
                try
                {
                    fltChannelId = Convert.ToInt32(communityFilterID);
                }
                catch (Exception ex)
                {
                    m_logger.Error("Media Library error during community flilter conversion.", ex);
                }
            }

            string titleFilterID = CURRENT_FILTERING;
            if ((titleFilterID != "") && (titleFilterID.ToUpper() != "ALL"))
            {
                filter.Add("", titleFilterID);
            }



            //return assets in user's play list
            if ((SearchString != null) && (SearchString != ""))
            {
                //pds = StoreUtility.SearchMediaLibrary(Convert.ToInt32(SelectedPlayList), _channelId, Convert.ToInt32(SelectedSmartList), fltChannelId, _channelOwnerId, false, true, true, orderby, filter, SearchString, pageNumber, filStore.NumberOfPages);

                DataTable dt = null;

                if (Convert.ToInt32(SelectedPlayList) > 0)
                {
                    dt = StoreUtility.GetAssetsInGroup(Convert.ToInt32(SelectedPlayList), _channelId, Convert.ToInt32(SelectedSmartList), "", "", 1, Int32.MaxValue);
                }
                else
                {
                    dt = StoreUtility.GetAssetsInChannel(_channelId, true, true, Convert.ToInt32(SelectedSmartList), "", "", 1, Int32.MaxValue);
                }

                List<int> iAssetList = new List<int>(dt.Rows.Count);

                //string assetClause = "";
                if (dt.Rows.Count > 0)
                {
                    // assetClause = " IN (";
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        iAssetList.Add((int)Convert.ToInt32(dt.Rows[i]["asset_id"]));
                    }
                }

                PagedList<Asset> plAssetVideo = mediaFacade.SearchMedia(KanevaWebGlobals.CurrentUser.HasAccessPass, Convert.ToInt32(SelectedSmartList),
                true, false, Server.HtmlEncode(SearchString),
                "", 0, "", pageNumber, filStore.NumberOfPages, false, iAssetList);

                pds = new PagedDataTable();
                pds.TotalCount = (int)plAssetVideo.TotalCount;
                pds.Columns.Add(new DataColumn("asset_id"));
                pds.Columns.Add(new DataColumn("name"));
                pds.Columns.Add(new DataColumn("file_size"));
                pds.Columns.Add(new DataColumn("public"));
                pds.Columns.Add(new DataColumn("asset_rating_id"));
                pds.Columns.Add(new DataColumn("mature"));
                pds.Columns.Add(new DataColumn("asset_type_id"));
                pds.Columns.Add(new DataColumn("publish_status_id"));
                pds.Columns.Add(new DataColumn("owner_id"));
                pds.Columns.Add(new DataColumn("username"));
                pds.Columns.Add(new DataColumn("teaser"));
                pds.Columns.Add(new DataColumn("date_added", System.Type.GetType("System.DateTime")));
                pds.Columns.Add(new DataColumn("status_id"));
                pds.Columns.Add(new DataColumn("keywords"));
                pds.Columns.Add(new DataColumn("thumbnail_gen"));
                pds.Columns.Add(new DataColumn("thumbnail_small_path"));
                pds.Columns.Add(new DataColumn("category1_id"));
                pds.Columns.Add(new DataColumn("category2_id"));
                pds.Columns.Add(new DataColumn("category3_id"));
                pds.Columns.Add(new DataColumn("mature_profile"));

                DataRow drAsset;

                foreach (Asset asset in plAssetVideo)
                {

                    drAsset = pds.NewRow();
                    drAsset["asset_id"] = asset.AssetId;
                    drAsset["name"] = asset.Name;
                    drAsset["file_size"] = asset.FileSize;
                    drAsset["public"] = asset.Public;
                    drAsset["asset_rating_id"] = asset.AssetRatingId;
                    drAsset["mature"] = asset.Mature.Equals("Y") ? 1 : 0;
                    drAsset["asset_type_id"] = asset.AssetTypeId;
                    drAsset["publish_status_id"] = asset.PublishStatusId;
                    drAsset["owner_id"] = asset.OwnerId;
                    drAsset["username"] = asset.OwnerUsername;
                    drAsset["teaser"] = asset.Teaser;
                    drAsset["date_added"] = asset.CreatedDate;
                    drAsset["status_id"] = asset.StatusId;
                    drAsset["keywords"] = asset.Keywords;
                    drAsset["thumbnail_gen"] = asset.ThumbnailGen;
                    drAsset["thumbnail_small_path"] = asset.ThumbnailSmallPath;
                    drAsset["category1_id"] = asset.Category1Id;
                    drAsset["category2_id"] = asset.Category2Id;
                    drAsset["category3_id"] = asset.Category3Id;
                    drAsset["mature_profile"] = asset.Mature.Equals("Y") ? 1 : 0;
                    pds.Rows.Add(drAsset);
                }
            }
            else
            {
                pds = StoreUtility.GetMediaLibraryAssets(Convert.ToInt32(SelectedPlayList), _channelId, Convert.ToInt32(SelectedSmartList), fltChannelId, _channelOwnerId, false, true, true, orderby, filter, pageNumber, filStore.NumberOfPages);
            }

            pgTop.NumberOfPages = Math.Ceiling((double)pds.TotalCount / filStore.NumberOfPages).ToString();
            pgTop.DrawControl();

            pgBottom.NumberOfPages = Math.Ceiling((double)pds.TotalCount / filStore.NumberOfPages).ToString();
            pgBottom.DrawControl();

            rptAssets.DataSource = pds;
            rptAssets.DataBind();

            //reset after each search so as not to affect other smartlist searches
            SearchString = "";

            // The results
            lblSearch2.Text = lblSearch.Text = GetResultsText(pds.TotalCount, pageNumber, filStore.NumberOfPages, pds.Rows.Count);

        }

        #endregion

        #region Event Handlers

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (KanevaWebGlobals.ContainsInjectScripts(txtKeywords.Text))
            {
                m_logger.Warn("User " + KanevaWebGlobals.GetUserId() + " tried to script from IP " + Common.GetVisitorIPAddress());
                ShowErrorOnStartup("Your input contains invalid scripting, please remove script code and try again.", true);
                return;
            }

            SearchString = txtKeywords.Text;

            //search for items
            BindMediaData(1);
        }

        private void lbnSaveSortOrder_Click(object sender, System.EventArgs e)
        {
            if (ASSET_ORDERING_CHANGES.Count > 0)
            {
                //sort the assets
                SortAssets();

                //select the correct update
                switch (CURRENT_PAGE_CONFIGURATION)
                {
                    case PLAYLIST_MODE:
                        //update asset group asset table
                        int assetGroupId = Convert.ToInt32(SelectedPlayList);
                        foreach (string sortPosition in ASSET_ORDERING)
                        {
                            int assetId = Convert.ToInt32(ASSET_ORDERING[sortPosition]);
                            int sortOrder = Convert.ToInt32(sortPosition);
                            GetMediaFacade.UpdateAssetGroupAssetSortOrder(assetId, sortOrder, assetGroupId);
                        }
                        break;
                    case SMARTLIST_MODE:
                        //update assets table
                        foreach (string sortPosition in ASSET_ORDERING)
                        {
                            int assetId = Convert.ToInt32(ASSET_ORDERING[sortPosition]);
                            int sortOrder = Convert.ToInt32(sortPosition);
                            StoreUtility.UpdateAssetSortOrder(assetId, sortOrder);
                        }
                        break;
                }

                //reload to update list
                BindMediaData(pgTop.CurrentPageNumber);
            }
        }

        private void lnkDeleteMedia_Click(object sender, System.EventArgs e)
        {
            //calls function to delete all checked media
            try
            {
                DeleteChecked();
            }
            catch (Exception ex)
            {
                m_logger.Error("Media Library error during media deletion.", ex);
                messages.ForeColor = System.Drawing.Color.DarkRed;
                messages.Text = "Deletion Error: Sorry we were unable to delete your media.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('messages', 5000);");
            }

        }

        private void lnkAddChannel_Click(object sender, System.EventArgs e)
        {
            //calls function to add selected media to a channel
            try
            {
                SubmitToChannels();
            }
            catch (Exception ex)
            {
                m_logger.Error("Media Library error submitting media to Channels", ex);
                messages.ForeColor = System.Drawing.Color.DarkRed;
                messages.Text = "Error: Sorry we were unable to submit the media to the community(ies).";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('messages', 5000);");
            }

        }

        /// <summary>
        /// lnkStarDirectory_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void lnkStarDirectory_Click(object sender, System.EventArgs e)
        {
            if (IsAdministrator())
            {
                Response.Redirect("~/3d-virtual-world/starsDirectory.aspx");
            }
        }

        /// <summary>
        /// lnkWOKCatalog_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void lnkWOKCatalog_Click(object sender, System.EventArgs e)
        {
            if (IsAdministrator())
            {
                Response.Redirect("~/3d-virtual-world/catalog.aspx");
            }
        }

        /// <summary>
        /// change channel filter
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ddlChannels_SelectedIndexChanged(object sender, EventArgs e)
        {
            CURRENT_COMMUNITY_FILTERING = ddlChannels.SelectedValue;
            pgTop.CurrentPageNumber = 1;
            pgBottom.CurrentPageNumber = 1;
            this.BindMediaData(1);
        }

        /// <summary>
        /// Delete an asset
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Delete_Asset(Object sender, EventArgs e)
        {
            int userId = GetUserId();
            int assetId = 0;
            try
            {
                assetId = Convert.ToInt32(AssetId.Value);
            }
            catch (FormatException ex)
            {
                m_logger.Error("Media Library error deleting media.", ex);
                messages.ForeColor = System.Drawing.Color.DarkRed;
                messages.Text = "Submission Error: Sorry we were unable to delete your media.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('messages', 5000);");
                return;
            }

            if (!StoreUtility.IsAssetOwner(userId, assetId))
            {
                m_logger.Warn("User " + userId + " from IP " + Common.GetVisitorIPAddress() + " tried to delete store item " + AssetId.Value + " but is not the asset owner");
                return;
            }

            StoreUtility.DeleteAsset(assetId, userId);
            //check to see if there are any playlist with this item in them and remove from the playlist(asset_group_asset) as well
            GetMediaFacade.RemoveAssetFromAllGroups(assetId);
            pg_PageChange(this, new PageChangeEventArgs(pgTop.CurrentPageNumber));
        }

        /// <summary>
        /// rptAssets_ItemCommand
        /// </summary>
        private void rptPlayList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            //disable the visual clue that you can search for media
            btnSearch.Enabled = false;
            txtKeywords.Enabled = false;

            CURRENT_PAGE_CONFIGURATION = PLAYLIST_MODE;

            //set the current playlist
            SelectedPlayList = e.CommandArgument.ToString();

            //clear out the selected items
            ClearPlayListSelections();

            //reset page to page 1 for cases where there are not enough assets for multiple pages
            pgTop.CurrentPageNumber = 1;
            pgBottom.CurrentPageNumber = 1;

            //highlight the selected playlist
            HtmlGenericControl list = (HtmlGenericControl)e.Item.FindControl("Li1");
            list.Attributes.Add("class", "selected");

            //filter and display playlist media
            BindMediaData(1);

            //enable the edit feature for that play List
            try
            {
                SetPlayListEditArea(e);
            }
            catch (Exception ex)
            {
                m_logger.Error("Media Library error setting playlist edit area", ex);
                messages.ForeColor = System.Drawing.Color.DarkRed;
                messages.Text = "Error: Sorry we were unable to get the edit information.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('messages', 5000);");
                return;
            }

        }

        /// <summary>
        /// rptAssets_ItemCommand
        /// </summary>
        private void rptAssets_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            string command = e.CommandName;

            HtmlInputHidden hidAssetId;

            switch (command)
            {
                case "cmdEdit":
                    {
                        int index = e.Item.ItemIndex;
                        hidAssetId = (HtmlInputHidden)rptAssets.Items[index].FindControl("hidAssetId");

                        Response.Redirect(GetAssetEditLink(Convert.ToInt32(hidAssetId.Value)) + "&back=ML");
                        break;
                    }
            }
        }

        private void rptAssets_ItemCreated(object sender, RepeaterItemEventArgs e)
        {
            TextBox tbxSortOrder = (TextBox)e.Item.FindControl("tbxSortOrder");
            tbxSortOrder.AutoPostBack = true;
            tbxSortOrder.TextChanged += new EventHandler(tbxSortOrder_TextChanged);
            HtmlAnchor thumbnail = (HtmlAnchor)e.Item.FindControl("thumbnail");
            HtmlAnchor mediaTitle = (HtmlAnchor)e.Item.FindControl("mediaTitle");

            //create the tool summary data
            string title = "\\<b>Name: </b>" + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "name")) + "<br>";
            string description = "\\<b>Description: </b>" + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "teaser")) + "<br>";
            string owner = "\\<b>Owner: </b>" + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "username")) + "<br>";
            string restricted = "\\<b>Rating: </b>" + (Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "mature_profile")).Equals(1) ? "Restricted" : "General Audience") + "<br>";
            string assetType = "\\<b>Type: </b>" + GetAssetTypeName(Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "asset_type_id"))) + "<br>";
            string _public = "\\<b>Access: </b>" + (Convert.ToString(DataBinder.Eval(e.Item.DataItem, "public")) == "Y" ? "Public" : "Private") + "<br>";
            string fileSize = "\\<b>Size: </b>" + Math.Round((Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "file_size"))) / 1000.0, 2) + " Mb" + "<br>";
            string createdOn = "\\<b>Added On: </b>" + FormatDateTime(Convert.ToDateTime(DataBinder.Eval(e.Item.DataItem, "date_added"))) + "<br>";

            string toolTip = "\\ <h4>Asset Details</h4>" + title + description + owner + restricted + assetType + _public + fileSize + createdOn;
            thumbnail.Attributes.Add("onmouseover", "whiteBalloonSans.showTooltip(event,'" + KanevaGlobals.CleanJavascriptFull(toolTip) + "')");
            mediaTitle.Attributes.Add("onmouseover", "whiteBalloonSans.showTooltip(event,'" + KanevaGlobals.CleanJavascriptFull(toolTip) + "')");
        }

        private void rptAssets_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton lnkEdit = (LinkButton)e.Item.FindControl("lnkEdit");
                CheckBox chkEdit = (CheckBox)e.Item.FindControl("chkEdit");
                Label lblMessage = (Label)e.Item.FindControl("lblMessage");
                TextBox tbxSortOrder = (TextBox)e.Item.FindControl("tbxSortOrder");
                HtmlInputHidden hdnSrtOrig = (HtmlInputHidden)e.Item.FindControl("hidSortOrig");

                int statusId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "status_id"));
                int ownerId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "owner_id"));
                string teaser = DataBinder.Eval(e.Item.DataItem, "teaser") == null ? "" : DataBinder.Eval(e.Item.DataItem, "teaser").ToString();

                //if no order has been assigned for some reason assign an order
                //this logic assumes and requires ordering by sort order
                LoadAssetOrdering(DataBinder.Eval(e.Item.DataItem, "asset_id").ToString(), tbxSortOrder, hdnSrtOrig);


                //only asset owner and admin can edit the asset
                bool canEditAsset = ((ownerId == GetUserId()) || IsAdministrator());

                //owner, channel mods and admin can manage the assets
                bool canManageAsset = canEditAsset || _userCanEdit;

                if (ownerId != GetUserId() && !this.IsAdministrator())
                {
                    //its a connected media
                    //0002543: from RF - MyMediaLibrary formatting issues under Firefox 
                    //lblMessage.Text = "Connected media from " + username;
                }
                else if (IsDeleted(statusId))
                {
                    canEditAsset = false;
                    canManageAsset = false;
                    lblMessage.Text = "Deleted";
                }

                lnkEdit.Visible = canEditAsset;
                chkEdit.Visible = canManageAsset;
                lblMessage.Visible = !canEditAsset;

                HtmlAnchor hlStatus = (HtmlAnchor)e.Item.FindControl("hlStatus");
                HtmlAnchor hlAddData = (HtmlAnchor)e.Item.FindControl("hlAddData");

                if (ownerId != GetUserId())
                {
                    hlStatus.Attributes.Add("onclick",
                        "javascript:window.open('" + ResolveUrl("~/asset/publishStatus.aspx#ConnectedMedia','add','toolbar=no,width=600,height=450,menubar=no,scrollbars=yes,status=yes').focus();return false;"));
                    hlStatus.InnerHtml = "<span style=\"color:blue\">Shared Media</span>";

                }
                else
                {
                    hlStatus.Attributes.Add("onclick", GetStatusLink(Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "publish_status_id"))));
                    hlStatus.InnerHtml = GetStatusText(Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "publish_status_id")),
                        Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "status_id")), Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "asset_id")));

                    // Show add data?
                    int m_category1 = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "category1_id"));
                    int m_category2 = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "category2_id"));
                    int m_category3 = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "category3_id"));
                    string keywords = DataBinder.Eval(e.Item.DataItem, "keywords") == null ? "" : DataBinder.Eval(e.Item.DataItem, "keywords").ToString();

                    if (keywords.Length.Equals(0) || teaser.Length.Equals(0)
                        || (m_category1.Equals(0) && m_category2.Equals(0) && m_category3.Equals(0)))
                    {
                        hlAddData.InnerHtml = ": Add details";
                        hlAddData.HRef = GetAssetEditLink(Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "asset_id")));
                    }
                }

            }
        }

        protected void SmartNav_Command(Object sender, CommandEventArgs e)
        {
            //re-enable the visual clue that you can search for media
            btnSearch.Enabled = true;
            txtKeywords.Enabled = true;

            CURRENT_PAGE_CONFIGURATION = SMARTLIST_MODE;
            //reset page to page 1 for cases where there are not enough assets for multiple pages
            pgTop.CurrentPageNumber = 1;
            pgBottom.CurrentPageNumber = 1;

            if (ddlFiles2PlayList.Items.Count > 0)
            {
                this.ddlFiles2PlayList.SelectedIndex = 0;
            }
            if (ddlEditMultiples.Items.Count > 0)
            {
                this.ddlEditMultiples.SelectedIndex = 0;
            }

            //clear out the selected items
            ClearPlayListSelections();

            //implemented as a work around for the code assigning which seems to be causing drawing issues
            switch (e.CommandName)
            {
                case "video":
                    SelectedSmartList = ((int)Constants.eASSET_TYPE.VIDEO).ToString();
                    lblSelectedMediaList.Text = "All: Videos";
                    break;
                case "picture":
                    SelectedSmartList = ((int)Constants.eASSET_TYPE.PICTURE).ToString();
                    lblSelectedMediaList.Text = "All: Photos";
                    break;
                case "music":
                    SelectedSmartList = ((int)Constants.eASSET_TYPE.MUSIC).ToString();
                    lblSelectedMediaList.Text = "All: Music";
                    break;
                case "game":
                    SelectedSmartList = ((int)Constants.eASSET_TYPE.GAME).ToString();
                    lblSelectedMediaList.Text = "All: Games";
                    break;
                case "pattern":
                    SelectedSmartList = ((int)Constants.eASSET_TYPE.PATTERN).ToString();
                    lblSelectedMediaList.Text = "All: Patterns";
                    break;
                case "tv":
                    SelectedSmartList = ((int)Constants.eASSET_TYPE.TV).ToString();
                    lblSelectedMediaList.Text = "All: TV Channels";
                    break;
                case "widgets":
                    SelectedSmartList = ((int)Constants.eASSET_TYPE.WIDGET).ToString();
                    lblSelectedMediaList.Text = "All: Flash Widgets";
                    break;
                case "shared":
                    SelectedSmartList = "-1";
                    lblSelectedMediaList.Text = "All: Shared Media";
                    break;
                default://all
                    SelectedSmartList = ((int)Constants.eASSET_TYPE.ALL).ToString();
                    lblSelectedMediaList.Text = "All";
                    break;
            }

            //filter and display smartlist media
            BindMediaData(1);
        }

        private void pg_PageChange(object sender, PageChangeEventArgs e)
        {
            pgTop.CurrentPageNumber = e.PageNumber;
            pgTop.DrawControl();
            pgBottom.CurrentPageNumber = e.PageNumber;
            pgBottom.DrawControl();
            BindMediaData(e.PageNumber);
        }

        private void filStore_FilterChanged(object sender, FilterChangedEventArgs e)
        {
            pgTop.CurrentPageNumber = 1;
            pgBottom.CurrentPageNumber = 1;
            BindMediaData(1);
        }

        private void pageSort_FilterChanged(object sender, FilterChangedEventArgs e)
        {
            pgTop.CurrentPageNumber = 1;
            pgBottom.CurrentPageNumber = 1;
            CURRENT_FILTERING = e.Filter;
            BindMediaData(1);
        }

        /// <summary>
        /// Cancels out of new playlist creation display
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancel_Click(object sender, System.EventArgs e)
        {
            CancelNewPlayList();
        }

        /// <summary>
        /// saves changes to playlist
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSavePlayList_Click(object sender, System.EventArgs e)
        {
            try
            {
                SavePlayList();
            }
            catch (Exception ex)
            {
                m_logger.Error("Media Library error saving users playlist changes", ex);
                messages.ForeColor = System.Drawing.Color.DarkRed;
                messages.Text = "Error: Sorry we were unable to save your playlist changes.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('messages', 5000);");
                return;
            }
        }

        /// <summary>
        /// displays fields for creating a new playlist
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkNewPlaylist_Click(object sender, System.EventArgs e)
        {
            tbxPlayListName.Text = "PlayList 1";
            tbxPlayListName.Text = "";
            tbxPlayListDescription.Text = "";
            hdnPlayListId.Value = "-1";
            trPlayListAdd.Visible = false;
            trPlayListvalues.Visible = true;
            rblShuffle.SelectedIndex = 0;
        }

        protected void editPlayList_Click(object sender, System.EventArgs e)
        {
            //make add playlist area visible
            trPlayListAdd.Visible = false;
            trPlayListvalues.Visible = true;

            //populate fields with playlist information
            tbxPlayListName.Text = playListTitle.Text;
            tbxPlayListDescription.Text = playListDescription.Text;

            if (shuffleText.Text.Equals("Shuffle:" + "On"))
            {
                rblShuffle.SelectedIndex = 1;
            }
            else
            {
                rblShuffle.SelectedIndex = 0;
            }

        }

        protected void lnkRemoveFromPlayList_Click(object sender, System.EventArgs e)
        {
            CheckBox chkEdit;
            HtmlInputHidden hidAssetId;
            int deletedItems = 0;

            if ((hdnPlayListId.Value != null) && (hdnPlayListId.Value != "-1"))
            {
                int playListId = Convert.ToInt32(hdnPlayListId.Value);

                foreach (RepeaterItem dgiAsset in rptAssets.Items)
                {
                    chkEdit = (CheckBox)dgiAsset.FindControl("chkEdit");

                    if (chkEdit.Checked)
                    {
                        hidAssetId = (HtmlInputHidden)dgiAsset.FindControl("hidAssetId");
                        GetMediaFacade.RemoveAssetFromGroup(ChannelId, playListId, Convert.ToInt32(hidAssetId.Value));
                        deletedItems++;
                    }
                }

                //message to user
                if (deletedItems == 0)
                {
                    messages.ForeColor = System.Drawing.Color.DarkRed;
                    messages.Text = "No items were selected.";
                    MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('messages', 5000);");
                }
                else
                {
                    // DRF - ED-7522 - Web should tickle servers on playlist changes
                    (new RemoteEventSender()).BroadcastItemChangeEvent(RemoteEventSender.ObjectType.MediaPlaylist, RemoteEventSender.ChangeType.UpdateObject, playListId);

                    //rebind for counts
                    BindMediaData(pgTop.CurrentPageNumber);
                    BindPlayList();
                    Load_AM2PL_DropDown();

                    //message to user
                    messages.ForeColor = System.Drawing.Color.DarkGreen;
                    messages.Text = deletedItems + " item" + (deletedItems > 1 ? "s were " : " was ") + "deleted from this playlist.";
                    MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('messages', 5000);");
                }
            }
        }

        protected void lnkDeletePlayList_Click(object sender, System.EventArgs e)
        {
            if ((hdnPlayListId.Value != null) && (hdnPlayListId.Value != "-1"))
            {
                int playListId = Convert.ToInt32(hdnPlayListId.Value);
                GetMediaFacade.DeleteAssetGroup(_channelId, playListId);

                // DRF - ED-7522 - Web should tickle servers on playlist changes
                (new RemoteEventSender()).BroadcastItemChangeEvent(RemoteEventSender.ObjectType.MediaPlaylist, RemoteEventSender.ChangeType.DeleteObject, playListId);

                trEditPlaylist.Visible = false;
                BindPlayList();
                Load_AM2PL_DropDown();
                BindMediaData(pgTop.CurrentPageNumber);

                //message to user
                messages.ForeColor = System.Drawing.Color.DarkGreen;
                messages.Text = "You have successfully deleted the selected playlist.";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('messages', 5000);");
            }
        }

        private void lbnEditMultiple_Click(object sender, System.EventArgs e)
        {
            EditMultipleItems();
        }

        private void lbnFiles2PlayList_Click(object sender, System.EventArgs e)
        {
            AddFiles2PlayList();
        }

        private void tbxSortOrder_TextChanged(object sender, System.EventArgs e)
        {

            RepeaterItem ritem = (RepeaterItem)((Control)sender).NamingContainer;
            TextBox tbx = (TextBox)ritem.FindControl("tbxSortOrder");
            HtmlInputHidden hdnAssetId = (HtmlInputHidden)ritem.FindControl("hidAssetId");
            HtmlInputHidden hdnSrtOrg = (HtmlInputHidden)ritem.FindControl("hidSortOrig");

            string sortOrderValue = tbx.Text;
            string assetId = hdnAssetId.Value;
            string origSortOrder = hdnSrtOrg.Value;

            //security check
            // Check for any inject scripts
            if (KanevaWebGlobals.ContainsInjectScripts(sortOrderValue))
            {
                m_logger.Warn("User " + KanevaWebGlobals.GetUserId() + " tried to script from IP " + Common.GetVisitorIPAddress());
                ShowErrorOnStartup("Your input contains invalid scripting, please remove script code and try again.", true);
                return;
            }

            //check to see if it is a number
            try
            {
                int sortValue = Convert.ToInt32(sortOrderValue);
                if (sortValue < 1)
                {
                    MagicAjax.AjaxCallHelper.WriteAlert("Sorting values must be numbers greater that 0.");
                    tbx.Text = origSortOrder;
                    return;
                }
            }
            catch (FormatException)
            {
                MagicAjax.AjaxCallHelper.WriteAlert("Sorting values must be numbers greater that 0.");
                tbx.Text = origSortOrder;
                return;
            }
            catch (Exception ex)
            {
                MagicAjax.AjaxCallHelper.WriteAlert("Your ordering number is too large.");
                tbx.Text = origSortOrder;
                m_logger.Error("Media Library error during sort order alteration", ex);
                return;
            }

            StoreSortOrderChange(sortOrderValue, origSortOrder);
            lbnSaveSortOrder.Enabled = lbnSaveSortOrder2.Enabled = true;
            litSaveSortButtonImage.Text = litSaveSortButtonImage2.Text = "<img src=\"../images/button_savesort.gif\" border=\"0\">";

        }

        #endregion

        //placing copy here since they keep disappearing
        /*pgTop.PageChanged +=new PageChangeEventHandler (pg_PageChange);
		pgBottom.PageChanged +=new PageChangeEventHandler (pg_PageChange);
		filStore.FilterChanged	+=new FilterChangedEventHandler(filStore_FilterChanged);
		pageSort.FilterChanged +=new FilterChangedEventHandler(pageSort_FilterChanged);*/


        #region Web Form Designer generated code

        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ddlChannels.SelectedIndexChanged += new System.EventHandler(this.ddlChannels_SelectedIndexChanged);
            this.editPlayList.Click += new System.EventHandler(this.editPlayList_Click);
            this.rptPlayList.ItemCommand += new System.Web.UI.WebControls.RepeaterCommandEventHandler(this.rptPlayList_ItemCommand);
            this.lnkNewPlaylist.Click += new System.EventHandler(this.lnkNewPlaylist_Click);
            this.btnSavePlayList.Click += new System.EventHandler(this.btnSavePlayList_Click);
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            this.lbnSaveSortOrder.Click += new System.EventHandler(this.lbnSaveSortOrder_Click);
            this.lbnSaveSortOrder2.Click += new System.EventHandler(this.lbnSaveSortOrder_Click);
            this.lnkDeleteMedia.Click += new System.EventHandler(this.lnkDeleteMedia_Click);
            this.lnkRemoveFromPlayList.Click += new System.EventHandler(this.lnkRemoveFromPlayList_Click);
            this.lnkDeletePlayList.Click += new System.EventHandler(this.lnkDeletePlayList_Click);
            this.lnkAddChannel.Click += new System.EventHandler(this.lnkAddChannel_Click);
            this.rptAssets.ItemCreated += new System.Web.UI.WebControls.RepeaterItemEventHandler(this.rptAssets_ItemCreated);
            this.rptAssets.ItemDataBound += new System.Web.UI.WebControls.RepeaterItemEventHandler(this.rptAssets_ItemDataBound);
            this.rptAssets.ItemCommand += new System.Web.UI.WebControls.RepeaterCommandEventHandler(this.rptAssets_ItemCommand);
            this.Load += new System.EventHandler(this.Page_Load);
            pgTop.PageChanged += new PageChangeEventHandler(pg_PageChange);
            pgBottom.PageChanged += new PageChangeEventHandler(pg_PageChange);
            filStore.FilterChanged += new FilterChangedEventHandler(filStore_FilterChanged);
            pageSort.FilterChanged += new FilterChangedEventHandler(pageSort_FilterChanged);
            this.lbnEditMultiple.Click += new System.EventHandler(this.lbnEditMultiple_Click);
            this.lbnFiles2PlayList.Click += new System.EventHandler(this.lbnFiles2PlayList_Click);
        }
        #endregion

    }
}
