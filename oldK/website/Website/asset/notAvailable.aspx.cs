///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for notAvailable.
	/// </summary>
	public class notAvailable : BasePage
	{
		protected notAvailable () 
		{
			Title = "Whoops! You can't access this item because...";
		}

		private void Page_Load (object sender, System.EventArgs e)
		{
            joinLink.HRef = ResolveUrl(KanevaGlobals.JoinLocation);

            if (Request ["M"] != null)
			{
				string mode = Request ["M"].ToString ();

				// Adult content
				if (mode.Equals ("ADU"))
				{
					lblAdult.Visible = !Request.IsAuthenticated;
					lblAdultYoung.Visible = Request.IsAuthenticated;
					lblPrivate.Visible = false;
					lblOther.Visible = false;
				}
				// Private
				else if (mode.Equals ("PR"))
				{
					lblAdult.Visible = false;
					lblAdultYoung.Visible = false;
					lblPrivate.Visible = true;
					lblOther.Visible = false;
				}
				// Other
				else if (mode.Equals ("NA"))
				{
					lblAdult.Visible = false;
					lblAdultYoung.Visible = false;
					lblPrivate.Visible = false;
					lblOther.Visible = true;
				}
			}
		}

		protected void btnOK_Click (object sender, EventArgs e) 
		{
            Response.Redirect("~/free-virtual-world.kaneva");
		}

		protected Label lblAdult, lblAdultYoung, lblPrivate, lblOther;
        protected HtmlAnchor joinLink;

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
