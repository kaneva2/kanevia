<%@ Page language="c#" Codebehind="iconUpload.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.iconUpload" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
        <HEAD>
                <title>Upload Icon</title>
                <link rel="stylesheet" href="../css/Kaneva.css" type="text/css">
                <meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
                <meta name="CODE_LANGUAGE" Content="C#">
                <meta name="vs_defaultClientScript" content="JavaScript">
                <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
        </HEAD>
        <body OnLoad="self.focus(); document.forms.frmMain.btnUpload.disabled=true;" topmargin="0"
                leftmargin="0">
                <table width="100%" border="0" align="center" cellpadding="1" cellspacing="0" background="../images/avatarUpload.jpg">
                        <form runat="server" method="post" enctype="multipart/form-data" name="frmMain" id="frmMain">
                                <TBODY>
                                        <tr>
                                                <td width="100%">
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="1">
                                                                <tr>
                                                                        <td width="20%" align="right" class="bodyText" style="COLOR:black">Icon:&nbsp;&nbsp;</td>
                                                                        <td width="80%"><input runat="server" class="Filter2" ID="inpIcon" type="file" size="45" onFocus="document.forms.frmMain.btnUpload.disabled=false;"
                                                                                        onClick="document.forms.frmMain.btnUpload.disabled=false;">
                                                                        </td>
                                                                </tr>
                                                                <tr align="center">
                                                                        <td colspan="2" class="dateStamp" align="center">Icons must be 32 x 32 and have a 
                                                                                .ico extension<br>
                                                                                Maximum file size is 200KB</td>
                                                                </tr>
                                                        </table>
                                                </td>
                                        </tr>
                                        <tr align="right">
                                                <td><br>
                                                        <asp:button id="btnUpload" runat="Server" onClick="btnUpload_Click" class="Filter2" Text=" Upload " />&nbsp;<input type="button" class="Filter2" name="cancel" value=" Cancel " onClick="window.close()">&nbsp;&nbsp;&nbsp;<br>
                                                        <br>
                                                </td>
                                        </tr>
                        </form>
                        </TBODY>
                </table>
        </body>
</HTML>
