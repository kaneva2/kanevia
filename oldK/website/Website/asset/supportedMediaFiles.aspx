<%@ Page language="c#" Codebehind="supportedMediaFiles.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.asset.supportedMediaFiles" %>


<table cellpadding="6" cellspacing="0" border="0" style="width:400px;margin-top:40px;border:1px solid #ccc;background:#fff;">						
	<tr>
		<td valign="middle" align="center" width="150px" style="padding:2px 0;border-bottom:1px solid #ccc;" >Media Type</td>
		<td valign="middle" align="center" style="padding:2px 0;border-bottom:1px solid #ccc;">Supported File Format</td>
	</tr>
	<tr>
		<td valign="top">Picture</td>
		<td valign="middle" align="left" >
		.JPG .GIF
		</td>
	</tr>
	<tr>
		<td valign="top">Game</td>
		<td valign="middle" align="left" >
		.SWF
		</td>
	</tr>
</table>	
