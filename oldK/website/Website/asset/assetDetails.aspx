<%@ Page language="c#" Codebehind="assetDetails.aspx.cs" AutoEventWireup="false" validateRequest="false" Inherits="KlausEnt.KEP.Kaneva.assetDetails" %>
<%@ Register TagPrefix="UserControl" TagName="RelatedItemsView" Src="../usercontrols/RelatedItemsView.ascx" %>
<%@ Register TagPrefix="UserControl" TagName="AssetComments" Src="../usercontrols/Comments.ascx" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>
<%@ Register TagPrefix="UserControl" TagName="LightBox" Src="../usercontrols/LightBox.ascx" %>

<usercontrol:lightbox runat="server" id="ucLightBox" />

<script type="text/javascript">
     function assetFullScreen(url,name)
     {
        window.open(url,"test","toolbar=no,location=no,status=no,resizable=yes,width=1024,height=768,titlebar=yes,menubar=no,scrollbars=no");
     }
</script>  

<script type="text/javascript">    
var $j = jQuery.noConflict();
    
$j(document).ready(function(){				
	popFacebox=function(){
		$j.facebox({ iframe: '../buyRave.aspx?assetId=<asp:literal id="litAssetId" runat="server"></asp:literal>' }, '', 360);
	};	
});
	
var raveCountCtrlName = '<asp:literal id="litCtrlRaveCount" runat="server"></asp:literal>'; 

</script>

<div id="divIFrame" style="display:none;">
	<a href="javascript:" onclick="$j.facebox.close()" id="aCloseFacebox">close</a><br /><br />
	<iframe frameborder="0" height="160" id="ifrm" width="360" src="%20" scrolling="no"></iframe>
</div>
	

<link href="../css/new.css" rel="stylesheet" type="text/css"/>
<link href="../css/media-share.css" rel="stylesheet" type="text/css"/>
<link href="../css/base/buttons_new.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
	a.btnmedium {padding-top:7px;height:21px;}
	.framesize-square {width:50px;;height:50px;}
</style>

<div id="divAccessMessage" runat="server" visible="false">
	<table border="0" cellpadding="0" cellspacing="0" class="newcontainer" align="center">
		<tr>
			<td valign="top">
				<table border="0" cellpadding="0" cellspacing="0" width="100%" class="newcontainerborder">
					<tr>
						<td class=""></td>
						<td class=""></td>
						<td class=""></td>
					</tr>
					<tr>
						<td class=""><img runat="server" src="~/images/spacer.gif" width="1" height="1"/></td>
						<td valign="top" class="newdatacontainer">
										
							<table cellpadding="0" cellspacing="0" border="0" width="70%" style="margin:0 auto;">
								<tr>
									<td valign="top" style="text-align:left;">
												
										<h1 id="h2Title" runat="server" style="border:font-size:19px;"></h1>
										<table cellpadding="6" cellspacing="0" border="0" width="95%">
											<tr>
												<td rowspan="3" valign="top">    
												    <img src="../images/access_pass.png" border="0" />
												</td>
												<td align="left" valign="top" id="tdActionText" runat="server" style="padding:30px 0 0 0;font-size:12px;line-height:16px; width:50%">
												</td>
											</tr>
											<tr>
												<td align="center" valign="top" id="tdButton" runat="server">
													<asp:linkbutton class="btnmedium grey" id="btnAction" runat="server"></asp:linkbutton>
												</td>
											</tr>
											<tr>
												<td align="left" valign="top" id="tdActionText2" runat="server" style="padding: 20px 20px 20px 20px; width:50%">
												</td> 
											</tr>
											<tr><td colspan="2" align="left" valign="top" id="tdPolicyText" runat="server"></td></tr>
										</table>
									</td>
								</tr>
							</table>
							
						</td>
						<td class=""><img runat="server" src="~/images/spacer.gif" width="1" height="1"/></td>
					</tr>
					<tr>
						<td class=""></td>
						<td class=""></td>
						<td class=""></td>
					</tr>
					<tr>
						<td><img runat="server" src="~/images/spacer.gif" width="1" height="14"/></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</div>
													

<div id="divDetails" runat="server" visible="false">
<table border="0" cellspacing="0" cellpadding="0"  class="newcontainer" align="center">
	<tr>
		<td>
			<table  border="0" cellpadding="0" cellspacing="0" width="100%" class="newcontainerborder">
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
				<tr>
					<td class=""><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
					<td valign="top" class="newdatacontainer">
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							
							<tr>
								<td width="968" align="left"  valign="top"> 
									<div id="pageheader"><h1 id="h1AssetTitle" runat="server"></h1></div>

									<table width="100%" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td width="486" valign="top" align="center">
											
												<table width="100%"  border="0" cellpadding="0" cellspacing="0">
													
													<!-- ABOUT MEDIA -->
													<tr>
														<td align="center">
															<div class="module">
																<span class="ct"><span class="cl"></span></span>
 																<p class="formspacer_small" />
																<div style="width:450px;background-color:#FFFFFF;overflow:hidden;border:1px solid #e0e0e0;margin:0 auto;">
																<!-- PLAYER / IMAGE -->
																<asp:placeholder runat="server" id="phStream"/>
																</div>
																
																<ajax:ajaxpanel id="ajpActionBar" runat="server">																
																<!-- TOOLBAR -->
																<table border="0" cellspacing="0" cellpadding="0" width="450" align="center">
																	<tr runat="server" id="trNewWindow" >																			  
																		<td align="center">
																		    <ul id="mediaFullScreen">
                                                                                <li id="full">
                                                                                       
                                                                                       
																		               
																		        </li>
																		     </ul>
																		</td>
																	</tr>																			  
																	<tr>																			  
																		<td>																								   
																			<ul id="mediaNav" style="padding-left:8px;">
																			    
																				<li id="rave" runat="server"><asp:linkbutton id="lbRave" onclick="lbRave_Click" visible="true" runat="server" tooltip="Rave this Item"></asp:linkbutton></li>
																				<li id="add" runat="server"><asp:linkbutton id="lbFave" onclick="lbFave_Click" visible="true" runat="server" tooltip="Add to my library"></asp:linkbutton></li>
																				<li id="share"><asp:linkbutton id="lb_link_1" visible="true" runat="server" tooltip="E-mail this, post to del.icio.us, etc."></asp:linkbutton></li>
																				<li id="fullscreen"><asp:linkbutton id="lbFullScreen"  visible="true" runat="server" tooltip="Play In Fullscreen Mode!"></asp:linkbutton></li>
																				<li id="report"><asp:linkbutton id="lbReport" visible="true" runat="server" tooltip="Report Abuse"></asp:linkbutton></li>
																			</ul>
																		</td>
																	</tr>																			  
																</table>
																
																<asp:linkbutton id="lbAddFriend" runat="server" onclick="lbAddFriend_Click" style="display:none;"></asp:linkbutton>
																
																</ajax:ajaxpanel>																
																<span style="float:right;margin-right:20px;"><a id="aEdit" runat="server" style="color:red;" visible="False">Edit this item</a></span>
																
																<table width="97%" align="center"  border="0" cellspacing="0" cellpadding="5" class="nopadding">
																	
																	<!-- OWNER AVATAR -->
																	<tr> 
																		<td width="80" align="left">
																			<div class="framesize-small">
																			<div class="restricted" visible="false" runat="server" id="divRestricted"></div>
																				<div class="">
																					<div class="imgconstrain">
																						<img runat="server" id="imgAssetThumbnail" src="~/images/KanevaIcon01.gif" border="0" /></div>																																				
																				</div>
																			</div>
																		</td>
																		<td valign="top"><h2><asp:label id="lblName" runat="server"/></h2><asp:label id="lblDescription" runat="server"/></td>
																	</tr>
																	<tr>
																		<td class="formspacer_small"></td>
																	</tr>
																	<tr runat="server" id="trInstructions" visible="false">
																		<td width="80" valign="top"><strong>Instructions:</strong></td>
																		<td colspan="2"><asp:textbox style="width: 355px;" cssclass="formKanevaText" textmode="MultiLine" id="txtInstructions" runat="server" columns="60" rows="4" readonly="True"></asp:textbox><br>
																		</td>
																	</tr>
																	<tr>
																		<td class="formspacer_small"></td>
																	</tr>
																	<tr>
																		<td width="80" valign="top" align="left"><strong>Media stats:</strong></td>
																		<td>
																			<table border="0" cellspacing="0" cellpadding="0" width="97%">
																				<tr>
																					<td width="200" align="left">
																						Views: <asp:label id="lblNumViews" runat="server" /><br/>
																						Raves: <ajax:ajaxpanel id="ajpRaves" runat="server"><asp:label id="lblRaves" runat="server" text="0"/></ajax:ajaxpanel><br/>
																						Added: <asp:label id="lblNumTimesAdded" runat="server" /><br/>
																						Comments: <asp:label id="lblNumComments" runat="server" /><br/>
																						<asp:label id="lblCompanyName" runat="server" /><br/>
																						<span id="spnRuntime" runat="server" visible="false">Runtime: <asp:label id="lblRuntime" runat="server"/><br/></span>
																					</td>
																					<td align="right">Shared by: <a id="aUserName" runat="server"></a></td>
																					<td align="right" width="60">
																						
																						<div class="framesize-square">
																							<div class="restricted" visible="false" runat="server" id="divOwnerRestricted"></div>
																							<div class="">
																								<div class="imgconstrain">
																									<a class="bodytext" id="aUserName3" border="0" runat="server"><img runat="server" id="imgAvatar" src="~/images/KanevaIcon01.gif" border="0" /></a></div>
																							</div>
																						</div>
																					</td>
																				</tr>
																				<tr>
																					<td colspan="3" align="left">Uploaded: <asp:label id="lblCreatedDate" runat="server"/></td>
																				</tr>
																			</table>
																		</td>
																	</tr>		 
																	<tr>
																		<td class="formspacer_small"></td>
																	</tr>
																	<tr>
																		<td width="80" valign="top" align="left"><strong>Share this:</strong></td>
																		<td colspan="2" align="left"><asp:textbox style="width: 355px;" cssclass="formKanevaText" id="txtShare" runat="server" columns="60" readonly="True"></asp:textbox><br>
																			<span class="note">Use this when you want a hyperlink. Good for email and when you can't embed the media player.</span>
																		</td>
																	</tr>
																	<tr>
																		<td class="formspacer_small"></td>
																	</tr>
																	<tr>
																		<td width="80" valign="top" align="left"><strong><div id="divEmbedded" runat="server">Embed this:</div></strong></td>
																		<td colspan="2" align="left"><asp:textbox style="width: 355px;" id="txtEmbed" cssclass="formKanevaText" runat="server" columns="60" readonly="True"></asp:textbox><br>
																			<span class="note">Add this media to your Kaneva profile, community, web site or blog.</span>
																		</td>
																	</tr>
																	
																	
																	<tr id="trPhotoURL" runat="server" visible="false">
																		<td width="80" valign="top" align="left"><strong><div id="divPhotoUrl">Photo URL:</div></strong></td>
																		<td colspan="2" align="left"><asp:textbox style="width: 355px;" id="txtRawEmbed" cssclass="formKanevaText" runat="server" columns="60" readonly="True"></asp:textbox><br>
																			<span class="note">Use this photo url for backgrounds.</span>
																		</td>
																	</tr>
																	
																	<tr>
																		<td class="formspacer_small"></td>
																	</tr>
																	<tr>
																		<td width="80" align="left"><strong>Related <span id="spnRelatedType" runat="server">videos</span> by topic:</strong></td>
																		<td class="textGeneralGray01" colspan="2" align="left">
																			<asp:label id="lblRelatedByTopic" runat="server"/>
																		</td>
																	</tr>
																	<tr>
																		<td class="formspacer_small"></td>
																	</tr>
																	<tr>
																		<td width="80" valign="top" align="left"><strong>Added by:</strong></td>
																		<td align="left">
																			
																			<table border="0" cellspacing="0" cellpadding="0" width="">
																				<tr>
																					<td><asp:label id="lblAddedBy" runat="server"/> added this to their library</td>
																				</tr>
																				<tr>
																					<td width="360">
																						<asp:label id="lblChannels" runat="server"/>
																						<span id="spnAdders" runat="server"></span>
																				</tr>
																			</table>
																		</td>
																	</tr>
																	<tr>
																		<td class="formspacer_small"></td>
																	</tr>
																	<tr>
																		<td width="80" align="left"><strong>License:</strong></td>
																		<td class="textGeneralGray01" colspan="2" align="left"><asp:Label id="lblLicense" runat="server"></asp:Label></td>
																	</tr>
																	
																</table>
																<span class="cb"><span class="cl"></span></span>
															</div>
														</td>
													</tr>
													
													<tr>
														<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" /></td>
													</tr>
													
													
												</table>
											</td>
											<td width="14"><img runat="server" src="~/images/spacer.gif" width="14" height="1" /></td>
											
											<td width="468" valign="top">
												<table width="100%"  border="0" cellpadding="0" cellspacing="0" id="contentRight">
													<tr>
														<td>
														
															<!--START AD PROMO 1-->
															<!--<div id="promo1" class="fullbanner">
																<a href="~/community/CommunityPage.aspx?communityId=1118&pageId=772341" runat="server"><img src="~/images/promo_kaneva.gif" runat="server" border="0"/></a>
															</div>-->
															
															<!--START AD PROMO 2-->
															<!--<div id="promo2" class="fullbanner">
																<table id="_ctl51_tblGoogleAdds" cellpadding="0" cellspacing="0" border="0" width="468">
																	<tr>
																		<td align="center">
																		<script type="text/javascript"><!--
																			google_ad_client = "pub-7044624796952740"; google_ad_width = 468; google_ad_height = 60; google_ad_format = "468x60_as"; google_ad_type = "text"; google_ad_channel ="7248390549";google_color_border = "FFFFFF";google_color_bg = "FFFFFF";google_color_link = "018AAA";google_color_text = "737373";google_color_url = "018AAA";<asp:Literal runat="server" id="litAdTest"/>; //--><!--</script> 
																			<script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
																			</script>
																		</td>
																	</tr>
																</table>
															</div>-->
														
															<!--START RELATED MEDIA-->
															<div id="related_media">
																	
																<usercontrol:relateditemsview runat="server" id="ucRelatedItems" />
															
															</div>																
														<br>
														
														<!--END RELATED MEDIA-->
													
													
														<!--START COMMENTS-->
															<div class="module whitebg">
																<span class="ct"><span class="cl"></span></span>
																					
																	<usercontrol:assetcomments runat="server" id="ucComments" />

																<span class="cb"><span class="cl"></span></span>
															</div>
														<!--END COMMENTS-->
															
														</td>
													</tr>
													
													<tr>
														<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" /></td>
													</tr>
													
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td class=""><img runat="server" src="~/images/spacer.gif" id="Img5" width="1" height="1" /></td>
				</tr>
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
				
			</table>
		</td>
	</tr>
</table>						
</div>


										
<asp:label id="lblOwnerViews" runat="server"/> 
<asp:label id="lblOwnerFavs" runat="server"/>  
<asp:label id="lblOwnerFriends" runat="server"/>

<asp:label runat="server" id="lblImageCaption" cssclass="dateStamp" style="FONT-WEIGHT: bold" visible="false"/>

<table>																	
	<tr id="trAttributes" runat="server" visible="false">
		<td align="center">
			<div class="module whitebg">
				<span class="ct"><span class="cl"></span></span>
				<h2>Additional Information</h2>
					<!-- Additional Info -->
					<table width="97%"  border="0" cellspacing="0" cellpadding="5">
						<tr id="trDetailDesc" runat="server" visible="false">
							<td width="100"><strong>Detail Description:</strong></td>
							<td class="textGeneralGray01"><asp:label id="lblBodyText" runat="server"/></td>
						</tr>
																	
						<asp:repeater runat="server" id="rptAttributes" >
							<itemtemplate>
								<tr>
									<td width="100"><strong><%# DataBinder.Eval (Container.DataItem, "attribute_name") %>:</strong></td>
									<td class="textGeneralGray01"><%# DataBinder.Eval (Container.DataItem, "value_name").ToString ().Replace ("\n", "<br/>") %></td>
								</tr>	
							</itemtemplate>	
						</asp:repeater>

					</table>
				
				<span class="cb"><span class="cl"></span></span>
			</div>
		</td>
	</tr>
</table>


											
<!-- Share This BEGIN -->
<asp:literal runat="server" id="litNotLoggedShareStyle"></asp:literal>

	<div id="akst_form">
		<a href="javascript:void($('akst_form').style.display='none');" class="akst_close">Close</a>
		<ul class="tabs">
			
			<li id="akst_tab1" onclick="akst_share_tab('1');" runat="server">Blast</li>
			<li id="akst_tab2" onclick="akst_share_tab('2');" runat="server">Private</li>			
			<li id="akst_tab3" onclick="akst_share_tab('3');" runat="server">E-mail</li>
			<li id="akst_tab4" onclick="akst_share_tab('4');" runat="server">Social Web</li>						
		</ul>

		<div class="clear"></div>
		
		<div id="akst_blast">
		  <fieldset>
		  <ajax:ajaxpanel id="ajpShareBlast" runat="server">		  
		  <ul>
		    <li>Blast this to your friends!</li>
		    <li>Link Title: <asp:textbox id="txtBlastTitle" runat="server" width="265"></asp:textbox></li>
		    <li>Your Message (Link will automatically be inserted):</li>	    
		    <li><asp:textbox TextMode="multiline" id="txtBlast" runat="server" width="325" height="80"></asp:textbox></li>
		    <li><asp:button id="btnBlast" onClick="btnBlast_Click" Text="Blast It!" runat="server" />
		        <h2 class="alertmessage"><span id="spnBlastAlertMsg" runat="server"></span></h2>
		    </li>		  
		  </ul>	
		  </ajax:ajaxpanel>	  		 
		  </fieldset>
		</div>		
		
	    <div id="akst_private">
		  <fieldset>	
		  <ajax:ajaxpanel id="ajpSharePM" runat="server">	  
		  <ul>
		  <li>Private Message To Kaneva Member:</li>
		    <li><asp:textbox  id="txtPrivateMsgTo" runat="server" width="325"></asp:textbox></li>
		    <li>Subject:</li>
		    <li><asp:textbox  id="txtPrivateMsgSubject" runat="server" width="325"></asp:textbox></li>
		    <li>Your Message (Link will automatically be inserted):</li>
		    <li><asp:textbox TextMode="multiline" id="txtPrivateMsgBody" runat="server" width="325" height="50"></asp:textbox></li>
		    <li><asp:button id="btnPMSend" onClick="btnPMSend_Click" Text="Send" runat="server" />
		     <h2 class="alertmessage"><span id="spnPMAlertMsg" runat="server"></span></h2>
		    </li>		  
		  </ul>	
		  </ajax:ajaxpanel>	  		 
		  </fieldset>
		</div>
		
		<div id="akst_email">			
				<fieldset>
					<legend>E-mail It</legend>
					<ul>
						<li>
							<label>1. Copy and paste this link into an email or instant message:</label>
							<asp:textbox id="shareLink" runat="server" class="akst_text" width="325" onclick="this.select();"></asp:textbox>							
						</li>
						<li>
							<label>2. Send this media using your computer's email application:</label>
							<asp:hyperlink id="shareMailto" runat="server" NavigateUrl="" text="Email Media" ></asp:hyperlink>
						</li>					
					</ul>
					<input type="hidden" name="akst_action" value="send_mail" />
					<input type="hidden" name="akst_post_id" id="akst_post_id" value="" />
				</fieldset>			
		</div>		
				
		<div id="akst_social">
			<ul>
				<li><a href="#" id="akst_delicious" target="_blank">del.icio.us</a></li>
				<li><a href="#" id="akst_digg" target="_blank">Digg</a></li>
				<li><a href="#" id="akst_furl" target="_blank">Furl</a></li>
				<li><a href="#" id="akst_netscape" target="_blank">Netscape</a></li>
				<li><a href="#" id="akst_yahoo_myweb" target="_blank">Yahoo! My Web</a></li>
				<li><a href="#" id="akst_stumbleupon" target="_blank">StumbleUpon</a></li>
				<li><a href="#" id="akst_google_bmarks" target="_blank">Google Bookmarks</a></li>
				<li><a href="#" id="akst_technorati" target="_blank">Technorati</a></li>
				<li><a href="#" id="akst_blinklist" target="_blank">BlinkList</a></li>
				<li><a href="#" id="akst_newsvine" target="_blank">Newsvine</a></li>
				<li><a href="#" id="akst_magnolia" target="_blank">ma.gnolia</a></li>
				<li><a href="#" id="akst_reddit" target="_blank">reddit</a></li>
				<li><a href="#" id="akst_windows_live" target="_blank">Windows Live</a></li>
				<li><a href="#" id="akst_tailrank" target="_blank">Tailrank</a></li>
			</ul>
			<div class="clear"></div>
		</div>
			
	</div>
<!-- Share This END -->
																	
