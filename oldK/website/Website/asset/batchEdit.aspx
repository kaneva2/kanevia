<%@ Page language="c#" Codebehind="batchEdit.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.batchEdit" %>
<%@ Register TagPrefix="Kaneva" TagName="HeaderText" Src="../usercontrols/HeaderText.ascx" %>

<link href="../css/home.css" rel="stylesheet" type="text/css" />		
<link href="../css/kanevaSC.css" rel="stylesheet" type="text/css"/>
<link href="../css/friends.css" rel="stylesheet" type="text/css"/>
<link href="../css/kanevaText.css" type="text/css" rel="stylesheet">
<link href="../css/kanevaBroadBand.css" rel="stylesheet" type="text/css" />
<link href="../css/new.css" rel="stylesheet" type="text/css" />

<script language="javascript" src="../jscript/rollover.js"></script>

<div ID="divLoading">
	<table BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="100%" HEIGHT="450px">
		<tr><th CLASS="loadingText" ID="divLoadingText">Loading...</th></tr>
	</table>
</div>	
<div id="divData" style="DISPLAY:none" class="container">

	<table cellpadding="0" cellspacing="0" border="0" width="100%">
		<tr>
			<td colspan="3" align="center">
				<asp:ValidationSummary ShowMessageBox="False" ShowSummary="True" Class="formError" id="valSum" DisplayMode="BulletList" runat="server" HeaderText="Please correct the following errors:"/><br>
			</td>
		</tr>
	</table>
	<table border="0" cellspacing="0" cellpadding="0"  width="990" align="center">
		<tr>
			<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" /></td>
		</tr>
		<tr>
			<td>
				<table  border="0" cellpadding="0" cellspacing="0" width="100%">
					<tr>
						<td class="frTopLeft"></td>
						<td class="frTop"></td>
						<td class="frTopRight"></td>
					</tr>
					<tr>
						<td class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
						<td valign="top" class="frBgIntMembers">
							<table width="100%"  border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td>
										<!-- TOP STATUS BAR -->
										<div id="pageheader">
											<table cellpadding="0" cellspacing="0" border="0" width="99%">
												<tr>
													<td nowrap="nowrap" align="left">
														<table cellpadding="0" cellspacing="0" border="0">
															<tr>
																<td><h1>Multi-Edit Media</h1></td>
																<td width="20px">&nbsp;</td>
																<td><asp:hyperlink title="return to Previous location" runat="server" ID="hlShowMedia" Text="<< Back" CausesValidation="False"/></td>
															</tr>
														</table>
													</td>
													
													<td align="right" valign="middle">
														<table cellpadding="0" cellspacing="0" border="0" width="690">
															<tr>
																<td class="headertout" width="175"></td>
																<td class="searchnav" width="260"></td>
																<td align="left" width="130"></td>
																<td class="searchnav" align="right" width="210" nowrap>
																</td>
															</tr>
														</table>
													</td>
												</tr>
											</table>
										</div>
										<!-- END TOP STATUS BAR -->	
									</td>
								</tr>
								<tr>
									<td width="968">
										<div class="module whitebg">
										<span class="ct"><span class="cl"></span></span>
										<h2>Editing Media</h2>
										<table width="99%" cellspacing="0" cellpadding="5" border="0" class="data">
										<asp:Repeater id="rptAssets" runat="server" EnableViewState="True">
											<ItemTemplate>
												<tr>
													<td>
														<table cellpadding="0" cellspacing="0" border="0" width="100%" class="nopadding">
															<tr>
																<td align="center" width="36">
																	<div class="framesize-small">
																	<div class="frame">
																		<span class="ct"><span class="cl"></span></span>							
																			<div class="imgconstrain">
																				<img src='<%#GetMediaImageURL (DataBinder.Eval(Container.DataItem, "thumbnail_medium_path").ToString () ,"me", Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_id")), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_type_id")), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "thumbnail_gen")))%>' alt="" border="0">
																			</div>																																				
																		<span class="cb"><span class="cl"></span></span>
																	</div>
																	</div>																					
																</td>
															</tr>
														</table>
													</td>
													<td valign="middle" align="left">
														<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
															<tr>
																<td align="right">Name :<img runat="server" src="~/images/spacer.gif" width="10" height="1" ID="Img1"/></td>
																<td align="left">
																	<asp:TextBox ID="txtItemName" Text='<%#Server.HtmlDecode (DataBinder.Eval(Container.DataItem, "name").ToString ())%>' class="formKanevaText" style="width:380px" MaxLength="100" runat="server"/>
																	<asp:RequiredFieldValidator ID="rfItemName" ControlToValidate="txtItemName" Text="*" ErrorMessage="Item name is a required field." Display="Dynamic" runat="server"/>
																	<input type="hidden" runat="server" id="hidAssetId" value='<%#DataBinder.Eval(Container.DataItem, "asset_id")%>' NAME="hidAssetId"> 
																</td>
															</tr>
															<tr>
																<td  align="right" valign="top"> Short Description :<img runat="server" src="~/images/spacer.gif" width="10" height="1" ID="Img2"/></td>
																<td align="left">
																	<asp:TextBox ID="txtTeaser" Text='<%#Server.HtmlDecode (DataBinder.Eval(Container.DataItem, "teaser").ToString ())%>' TextMode="multiline" Rows="3" class="formKanevaText"  style="width:380px" MaxLength="100" runat="server"/>
																</td>
															</tr>
															<tr>
																<td align="right">Tags :<img runat="server" src="~/images/spacer.gif" width="10" height="1" ID="Img3"/></td>
																<td align="left">
																	<asp:TextBox ID="txtTags" Text='<%#Server.HtmlDecode (DataBinder.Eval(Container.DataItem, "keywords").ToString ())%>' class="formKanevaText" style="width:380px" MaxLength="100" runat="server"/>
																	<asp:RequiredFieldValidator ID="rftxtTags" ControlToValidate="txtTags" Text="*" ErrorMessage="Tags is a required field." Display="Dynamic" runat="server"/>
																</td>
															</tr>
														</table>
													</td>
												</tr>
											</ItemTemplate>
											<AlternatingItemTemplate>
												<tr class="altrow">
													<td>
														<table cellpadding="0" cellspacing="0" border="0" width="100%" class="nopadding">
															<tr>
																<td align="center" width="36">
																	<div class="framesize-small">
																	<div class="frame">
																		<span class="ct"><span class="cl"></span></span>							
																			<div class="imgconstrain">
																				<img src='<%#GetMediaImageURL (DataBinder.Eval(Container.DataItem, "thumbnail_medium_path").ToString () ,"me", Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_id")), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_type_id")), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "thumbnail_gen")))%>' alt="" border="0">
																			</div>																																				
																		<span class="cb"><span class="cl"></span></span>
																	</div>
																	</div>																					
																</td>
															</tr>
														</table>
													</td>
													<td valign="middle" align="left">
														<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
															<tr>
																<td align="right">Name :<img runat="server" src="~/images/spacer.gif" width="10" height="1" ID="Img4"/></td>
																<td align="left">
																	<asp:TextBox ID="txtItemName" Text='<%#Server.HtmlDecode (DataBinder.Eval(Container.DataItem, "name").ToString ())%>' class="formKanevaText" style="width:380px" MaxLength="100" runat="server"/>
																	<asp:RequiredFieldValidator ID="rfItemName" ControlToValidate="txtItemName" Text="*" ErrorMessage="Item name is a required field." Display="Dynamic" runat="server"/>
																	<input type="hidden" runat="server" id="hidAssetId" value='<%#DataBinder.Eval(Container.DataItem, "asset_id")%>' NAME="hidAssetId"> 
																</td>
															</tr>
															<tr>
																<td  align="right" valign="top"> Short Description :<img runat="server" src="~/images/spacer.gif" width="10" height="1" ID="Img5"/></td>
																<td align="left">
																	<asp:TextBox ID="txtTeaser" Text='<%#Server.HtmlDecode (DataBinder.Eval(Container.DataItem, "teaser").ToString ())%>' TextMode="multiline" Rows="3" class="formKanevaText"  style="width:380px" MaxLength="100" runat="server"/>
																</td>
															</tr>
															<tr>
																<td align="right">Tags :<img runat="server" src="~/images/spacer.gif" width="10" height="1" ID="Img6"/></td>
																<td align="left">
																	<asp:TextBox ID="txtTags" Text='<%#Server.HtmlDecode (DataBinder.Eval(Container.DataItem, "keywords").ToString ())%>' class="formKanevaText" style="width:380px" MaxLength="100" runat="server"/>
																	<asp:RequiredFieldValidator ID="rftxtTags" ControlToValidate="txtTags" Text="*" ErrorMessage="Tags is a required field." Display="Dynamic" runat="server"/>
																</td>
															</tr>
														</table>
													</td>
												</tr>											
											</AlternatingItemTemplate>
										</asp:Repeater>
										</table>
										<br>
										<table border="0" cellspacing="0" cellpadding="0" width="95%" align="center">
											<tr>
											<td align="left">
												<span class="note">NOTE: You can only edit media you own.</span>
											</td>
											<td align="right">
												<asp:button id="btnUpdate" runat="Server" onClick="btnUpdate_Click" class="Filter2" Text="  submit  "/>
											</td>
										</tr>
										
										</table>
										
									<span class="cb"><span class="cl"></span></span>
									</div>
									
									</td>
								</tr>
									
							</table>

						</div>
								
						</td>
						<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
					</tr>
					<tr>
						<td class="frBottomLeft"></td>
						<td class="frBottom"></td>
						<td class="frBottomRight"></td>
					</tr>
					<tr>
						<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" /></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>