///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using MagicAjax;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva
{
    public partial class assetDetailsFullScreen : NoBorderPage
    {
        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Page_Load(object sender, System.EventArgs e)
        {
            DataRow drAsset = null;

            try
            {
                
                //if no id was passed the execption catch will handle it
                int assetId = Convert.ToInt32(Request["assetId"]);

                //get the asset
                drAsset = StoreUtility.GetAsset(assetId, false, GetUserId());

                if (drAsset == null || drAsset["asset_id"].Equals(DBNull.Value) || drAsset["status_id"].Equals((int)Constants.eASSET_STATUS.MARKED_FOR_DELETION) || drAsset["status_id"].Equals((int)Constants.eASSET_STATUS.DELETED))
                {
                    throw new Exception("asset is deleted, marked for deletion, or unretrievable");
                }
                else
                {
                    //check to see if it is mature and shouldnt be shown
                    bool isMature = drAsset["mature"].ToString().Equals("Y");
                    if (isMature && !KanevaWebGlobals.CurrentUser.HasAccessPass)
                    {
                        Response.Redirect(ResolveUrl("~/asset/assetDetails.aspx?assetId=" + assetId));
                    }

                    AssetOwnerId = Convert.ToInt32(drAsset["user_id"]);
                    int assetTypeId = Convert.ToInt32(drAsset["asset_type_id"]);
                    string assetType = KanevaWebGlobals.GetAssetTypeName(assetTypeId);

                    //bind the information 
                    BindData(drAsset);

                    // Show asset name
                    h1AssetTitle.InnerText = Server.HtmlDecode(drAsset["name"].ToString());

                    // Set the elements for sharing
                    shareLink.Text = txtShare.Text;
                    shareMailto.NavigateUrl = "mailto:?subject=You%27ve%20received%20media%20shared%20from%20Kaneva%21&body=" + txtShare.Text;

                    lb_link_1.Attributes.Add("onclick", "akst_share('1', '" + txtShare.Text + "', '" + (this.Title).Replace("'", "&#39;") + "', 325,150); return false; ");

                    // Set which tabs are shown and selected.
                    if (!Request.IsAuthenticated)
                    {
                        akst_tab1.Attributes.Add("style", "display:none;");
                        akst_tab2.Attributes.Add("style", "display:none;");
                        akst_tab3.Attributes.Add("class", "selected");

                        litNotLoggedShareStyle.Text = "<style>" +
                          "  #akst_email { display: block; } " +
                          "  #akst_blast { display: none; } " +
                          "  </style>";
                    }
                    else
                    {
                        akst_tab1.Attributes.Add("class", "selected");
                    }
                }

                //make the asset visible
                divDetailsMissing.Visible = false;
                divDetails.Visible = true;

            }
            catch (Exception ex)
            {
                m_logger.Error("Error loading asset details page: ", ex);
                divDetailsMissing.Visible = true;
                divDetails.Visible = false;
            }
        }


        # region Helper Methods
        /// <summary>
        /// Bind the page data
        /// </summary>
        private void BindData(DataRow drAsset)
        {
            int assetTypeId = Convert.ToInt32(drAsset["asset_type_id"]);
            int assetId = Convert.ToInt32(drAsset["asset_id"]);
            int userId = GetUserId();
            int ownerUserId = Convert.ToInt32(drAsset["user_id"]);

            string assetName = Server.HtmlDecode(drAsset["name"].ToString());
            this.Title = assetName + " (FullScreen)";

            // -- ACTION BAR SECTION --
            // ------------------------

            // Share
            lb_link_1.Attributes.Add("href", GetShareLink(assetId));

            //Report Abuse
            lbReport.Attributes.Add("onclick", "javascript: window.open('" + ResolveUrl("~/suggestions.aspx?mode=WB&category=KANEVA%20Web%20Site&rurl=" + Server.UrlEncode(Request.Url.ToString())) + "','add','toolbar=no,menubar=no,scrollbars=yes');");

            // Favorites

            //user cant add his own assets to his library
            lbFave.Enabled = (userId != ownerUserId);
            if (!lbFave.Enabled)
            {
                //lbFave.Text = GetAssetAddedToLibraryLink();
                add.Attributes.Add("class", "addedToLibrary");
            }
            else
            {
                if (Request.IsAuthenticated)
                {
                    PagedDataTable pdtAsset = StoreUtility.GetConnectedMedia(KanevaWebGlobals.CurrentUser.CommunityId, userId, "a.asset_id=" + assetId, "", 1, 1);

                    if (pdtAsset.Rows.Count > 0)
                    {
                        //lbFave.Text = GetAssetAddedToLibraryLink();
                        add.Attributes.Add("class", "addedToLibrary");
                        lbFave.Enabled = false;
                    }
                    else
                    {
                        add.Attributes.Add("class", "add");
                    }
                }
            }


            // Raves	 
            if (!IsPostBack)
            {
                // Which rave graphic do we show?	 
                RaveFacade raveFacade = new RaveFacade ();
                if (!raveFacade.IsAssetRaveFree (userId, assetId, RaveType.eRAVE_TYPE.SINGLE))
                {
                    rave.Attributes.Add ("class", "raveplus");
                }
                else
                {
                    rave.Attributes.Add ("class", "rave");
                }
                litAssetId.Text = assetId.ToString ();
            }

            // show the asset	
            StreamAsset(drAsset);


            // -- ITEM DETAILS SECTION --
            // --------------------------

            // game play instructions
            //txtA_instructions.Text = drAsset["teaser"].ToString();
            //instructions if any
            if ((drAsset["instructions"] != null) && (drAsset["instructions"].ToString() != ""))
            {
                trInstructions1.Visible = trInstructions.Visible = true;
                txtInstructions.Text = drAsset["instructions"].ToString();
            }
			

            txtShare.Attributes.Add("onclick", "this.select();");
            txtShare.Text = Server.HtmlDecode("http://" + Request.Url.Host + GetAssetDetailsLink(assetId));

            //Display the upsell if necessary
            if (!Request.IsAuthenticated)
            {
                string regURL = ResolveUrl("~/register/kaneva/registerInfo.aspx") + "?lpage=mediafull&mediatype=";
                string upsellMessage = "";
                string upsellImgClass = "";
                bool showUpsellImg = true;

                switch(assetTypeId)
                {
                    case (int) Constants.eASSET_TYPE.MUSIC:
                        upsellMessage = "Join Kaneva and listen to the latest raved music.";
                        upsellImgClass += " musicUpsell "; 
                        regURL += "music";
                        break;
                    case (int) Constants.eASSET_TYPE.PICTURE:
                    case (int) Constants.eASSET_TYPE.PATTERN:
                        upsellMessage = "Join Kaneva and get access to over 100,000 photos.";
                        upsellImgClass += " imageUpsell "; 
                        regURL += "image";
                        break;
                    case (int) Constants.eASSET_TYPE.VIDEO:
                        upsellMessage = "Join Kaneva and get access to the most popular videos.";
                        upsellImgClass += " videoUpsell "; 
                        regURL += "video";
                        break;
                    default:
                        upsellMessage = "Join today - it's free.";
                        showUpsellImg = false;
                        break;
                }

                liJoinNow.Visible = true;
                litLandingPageCSS.Text = "<link href=\"../css/landing_page/media/fullScreen.css?v2\" rel=\"stylesheet\" type=\"text/css\"/>";
                h1UpsellPromise.InnerText = upsellMessage;
                aJoinNow.HRef = regURL;
                aItsFree.HRef = regURL;
                h1AssetTitle.Attributes["data-regurl"] = regURL;

                // Update existing buttons to link directly to registration
                lb_link_1.Attributes.Remove("href");
                lb_link_1.Attributes.Add("href", regURL);
                lbReport.Attributes.Remove("onclick");
                lbReport.Attributes.Add("href", regURL);
                lbRave.Attributes.Remove("onclick");
                lbRave.Attributes.Add("href", regURL);

                if (showUpsellImg)
                {
                    divUpsellImageWrapper.Visible = true;
                    divUpsellImage.Attributes["class"] += upsellImgClass;
                    lbRave.Style.Add("margin-left", "50px");
                }
                else
                {
                    lbRave.Style.Add("margin-left", "90px");
                }
            }
        }

        /// <summary>
        /// StreamAsset
        /// </summary>
        /// <param name="drAsset"></param>
        private void StreamAsset(DataRow drAsset)	//ok	
        {
            MediaFacade mediaFacade = new MediaFacade();
            Response.Expires = 0;
            Response.CacheControl = "no-cache";

            int assetId = Convert.ToInt32(drAsset["asset_id"]);
            lb_link_1.Attributes.Add("href", GetShareLink(assetId));

            // Streamable asset?
            if (!StoreUtility.IsAssetStreamable(drAsset) && !StoreUtility.IsYouTubeContent(drAsset) &&
                !StoreUtility.IsStreamingTVContent(drAsset) && !StoreUtility.IsFlashWidget(drAsset) && !StoreUtility.IsFlashContent(drAsset))
            {
                ShowErrorOnStartup("File is being converted, the process could take several minutes, please try again later.");
                return;
            }

            if (Convert.ToInt32(drAsset["publish_status_id"]) != (int)Constants.ePUBLISH_STATUS.PUBLISH_COMPLETE)
            {
                ShowErrorOnStartup("File is being published, the process could take several minutes, please try again later.");
                return;
            }

            // Set up links
            string name = Server.HtmlDecode(drAsset["name"].ToString());

            // The URL to the content on the streaming server
            string contentURL = Configuration.StreamingServer + "/" + Configuration.StreamingFile + "?tId=" + assetId;
            if (Configuration.StreamIIS)
            {
                contentURL = Configuration.StreamingServer + "/" + drAsset["media_path"].ToString();
            }

            int height = 588;
            int width = 700;

            string dimensions = "height=\"" + height + "\" width=\"" + width + "\"";
            string dimensions2 = "height: " + height + "px; width: " + width + "px;";
            //string dimensions2 = "width: " + width + "px;";
            string autoplay = "true";

            // Is it playable with Kaneva OMM?
            if (StoreUtility.IsOMMContent(drAsset))
            {
                m_embedText = litStream.Text = GetAssetEmbedLink(assetId, height, width, true); // pass in true for fullURL only since required for embed link
                //litStream.Text = GetAssetEmbedLink (assetId, height, width, false ); 
                litStream.Text = StoreUtility.GetAssetEmbedLink(assetId, height, width, Page, false, false, true);
            }
            else if (StoreUtility.IsQuickTimeContent(drAsset))
            {
                // Don't do this for OMM, OMM takes care of that
                // Update the media views
                mediaFacade.UpdateMediaViews(assetId, Common.GetVisitorIPAddress(), GetUserId());

                if (StoreUtility.IsAudioContent(drAsset))
                {
                    // Use Quicktime for these audio
                    m_embedText = litStream.Text = "<object classid=\"clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B\" codebase=\"http://www.apple.com/qtactivex/qtplugin.cab\" " + dimensions + ">" +
                        "<param name=\"src\" value=\"" + contentURL + "\">" +
                        "<param name=\"autoplay\" value=\"" + autoplay + "\">" +
                        "<param name=\"controller\" value=\"true\">" +
                        "<embed " + dimensions + " src=\"" + contentURL + "\" pluginspage=\"http://www.apple.com/quicktime/download/\" type=\"video/quicktime\" controller=\"true\" autoplay=\"true\">" +
                        "</object>";
                }
                else
                {
                    // Use Quicktime for these videos, set scale to ASPECT
                    m_embedText = litStream.Text = "<object classid=\"clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B\" codebase=\"http://www.apple.com/qtactivex/qtplugin.cab\" " + dimensions + ">" +
                        "<param name=\"src\" value=\"" + contentURL + "\">" +
                        "<param name=\"autoplay\" value=\"" + autoplay + "\">" +
                        "<param name=\"controller\" value=\"true\">" +
                        "<PARAM NAME=\"scale\" VALUE=\"ASPECT\">" +
                        "<embed scale=\"ASPECT\" src=\"" + contentURL + "\" pluginspage=\"http://www.apple.com/quicktime/download/\" type=\"video/quicktime\" controller=\"true\" autoplay=\"true\">" +
                        "</object>";
                }
            }
            else if (StoreUtility.IsFlashContent(drAsset))
            {
                // Don't do this for OMM, OMM takes care of that
                // Update the media views
                mediaFacade.UpdateMediaViews(assetId, Common.GetVisitorIPAddress(), GetUserId());

                if (drAsset["asset_type_id"].Equals((int)Constants.eASSET_TYPE.WIDGET) ||
                    (drAsset["asset_type_id"].Equals((int)Constants.eASSET_TYPE.GAME) && drAsset["media_path"].ToString() == ""))
                {
                    contentURL = drAsset["asset_offsite_id"].ToString();
                }

                m_embedText = litStream.Text = "<OBJECT classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000' " +
                    " codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0' " +
                    " id=\"swfStream\" " + dimensions + ">" +
                    " <param name='movie' value=\"" + contentURL + "\">" +
                    " <param name='quality' value=\"high\">" +
                    " <param name='loop' value=\"false\">" +
                    " <EMBED src=\"" + contentURL + "\" quality='high'" + dimensions +
                    " loop=\"false\" type='application/x-shockwave-flash'" +
                    " pluginspage='http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash'>" +
                    " </EMBED> " +
                    "</OBJECT>";
            }
            else if (StoreUtility.IsImageContent(drAsset))
            {
                // Don't do this for OMM, OMM takes care of that
                // Update the media views
                mediaFacade.UpdateMediaViews(assetId, Common.GetVisitorIPAddress(), GetUserId());

                // It is an image
                if (!Request.IsAuthenticated)
                {
                    string regURL = ResolveUrl("~/register/kaneva/registerInfo.aspx") + "?lpage=mediafull&mediatype=image";

                    litStream.Text = "<div style=\"" + dimensions2 + " background-color: #FFFFFF; border: 1px solid #e0e0e0; margin:0 auto;\">" +
                        "<a href=\"" + regURL + "\"><img src=\"" + GetPhotoImageURL(drAsset["image_full_path"].ToString(), "la") + "\" border=\"0\" alt=\"" + name + "\" title=\"" + name + "\"/></a></div>";
                }
                else
                {
                    litStream.Text = "<div style=\"" + dimensions2 + " background-color: #FFFFFF; border: 1px solid #e0e0e0; margin:0 auto;\">" +
                        "<img src=\"" + GetPhotoImageURL(drAsset["image_full_path"].ToString(), "la") + "\" border=\"0\" alt=\"" + name + "\" title=\"" + name + "\"/></div>";
                }
                m_embedText = "<a href=\"http://" + Request.Url.Host + GetAssetDetailsLink(assetId) + "\" target=\"resource\" border=\"0\"><img src='" + GetPhotoImageURL(drAsset["image_full_path"].ToString(), "la") + "'/></a>";
            }
            else if (StoreUtility.IsYouTubeContent(drAsset))
            {
                // Update the media views
                mediaFacade.UpdateMediaViews(assetId, Common.GetVisitorIPAddress(), GetUserId());

                //Literal litStream = new Literal ();																				 //autostart
                m_embedText = StoreUtility.GetYouTubeEmbedLink(drAsset["asset_offsite_id"].ToString(), height, width, true, false);
                litStream.Text = StoreUtility.GetYouTubeEmbedLink(drAsset["asset_offsite_id"].ToString(), height, width, false, true);
                phStream.Controls.Add(litStream);
            }
            else if (StoreUtility.IsStreamingTVContent(drAsset))
            {
                // Update the media views
                mediaFacade.UpdateMediaViews(assetId, Common.GetVisitorIPAddress(), GetUserId());

                autoplay = "true";
                string tvURL = drAsset["asset_offsite_id"].ToString();

                // Use Media player
                m_embedText = litStream.Text = "<OBJECT id=\"mpStream\"" + dimensions +
                    " CLASSID=\"CLSID:6BF52A52-394A-11d3-B153-00C04F79FAA6\"" +
                    " standby='Loading Microsoft Windows Media Player components...'" +
                    " type=\"application/x-oleobject\">" +
                    " <PARAM NAME=\"URL\" VALUE=\"" + tvURL + "\">" +
                    " <PARAM NAME=\"ShowStatusBar\" VALUE=\"True\">" +
                    " <PARAM NAME=\"EnableContextMenu\" VALUE=\"False\">" +
                    " <PARAM NAME=\"AutoStart\" VALUE=\"" + autoplay + "\">" +
                    " <PARAM name=\"PlayCount\" value=\"1\">" +
                    "<embed " + dimensions + " type=\"application/x-mplayer2\" pluginspage=\"http://www.microsoft.com/Windows/MediaPlayer/\"" +
                    " name=\"mediaplayer1\" showstatusbar=\"1\" EnableContextMenu=\"false\" autostart=\"true\" transparentstart=\"1\" loop=\"0\" controller=\"true\" src=\"" + tvURL + "\"></embed>" +
                    "</OBJECT>";
            }
            else
            {
                // Don't do this for OMM, OMM takes care of that
                // Update the media views
                mediaFacade.UpdateMediaViews(assetId, Common.GetVisitorIPAddress(), GetUserId());

                // Use Media player
                m_embedText = litStream.Text = "<OBJECT id=\"mpStream\"" + dimensions +
                    " CLASSID=\"CLSID:6BF52A52-394A-11d3-B153-00C04F79FAA6\"" +
                    " standby='Loading Microsoft Windows Media Player components...'" +
                    " type=\"application/x-oleobject\">" +
                    " <PARAM NAME=\"URL\" VALUE=\"" + contentURL + "\">" +
                    " <PARAM NAME=\"ShowStatusBar\" VALUE=\"True\">" +
                    " <PARAM NAME=\"EnableContextMenu\" VALUE=\"False\">" +
                    " <PARAM NAME=\"AutoStart\" VALUE=\"" + autoplay + "\">" +
                    " <PARAM name=\"PlayCount\" value=\"1\">" +
                    "<embed " + dimensions + " type=\"application/x-mplayer2\" pluginspage=\"http://www.microsoft.com/Windows/MediaPlayer/\"" +
                    " name=\"mediaplayer1\" showstatusbar=\"1\" EnableContextMenu=\"false\" autostart=\"true\" transparentstart=\"1\" loop=\"0\" controller=\"true\" src=\"" + contentURL + "\"></embed>" +
                    "</OBJECT>";
            }

            phStream.Controls.Add(litStream);
        }

        /// <summary>
        /// Add the tag to the tag cart
        /// </summary>
        private void AddMediaToFavorites()
        {
            if (!Request.IsAuthenticated)
            {
                AjaxCallHelper.WriteAlert("You must be signed in to perform this action.");
                return;
            }

            if (Request["assetId"] != null)
            {
                int asset_id = Convert.ToInt32(Request["assetId"]);
                StoreUtility.AddTag(KanevaWebGlobals.CurrentUser.UserId, KanevaWebGlobals.CurrentUser.CommunityId, asset_id, Common.GetVisitorIPAddress());
            }

            //lbFave.Text = GetAssetAddedToLibraryLink();
            add.Attributes.Add("class", "addedToLibrary");
            lbFave.Enabled = false;
        }
        protected void RaveAsset()
        {
            if (!Request.IsAuthenticated)
            {
                AjaxCallHelper.WriteAlert ("You must be signed in to rave this item.");
                return;
            }

            RaveFacade raveFacade = new RaveFacade ();
            int assetId = Convert.ToInt32 (Request["assetId"]);

            if (!raveFacade.IsAssetRaveFree (KanevaWebGlobals.CurrentUser.UserId, assetId,
                RaveType.eRAVE_TYPE.SINGLE) && !IsAdministrator ())
            {

                lbRave.CssClass = "raveplus";

                // this rave is not free, show buy rave dialog
                AjaxCallHelper.Write ("popFacebox();");

				// GA Event
				Page.ClientScript.RegisterClientScriptBlock(GetType(), "gaRaveEvent", "callGAEvent('Media', 'RavePlus', 'RavePlusClick');", true);
            }
            else  // first time rave
            {
                // first rave is free.  Insert a normal rave.
                raveFacade.RaveAsset (KanevaWebGlobals.CurrentUser.UserId, assetId, RaveType.eRAVE_TYPE.SINGLE, "");

				// GA Event
				Page.ClientScript.RegisterClientScriptBlock(GetType(), "gaRaveEvent", "callGAEvent('Media', 'Rave', 'RaveClick');", true);				
            }

            rave.Attributes.Add ("class", "raveplus");
        }

        private string GetAssetAddedToLibraryLink()
        {
            return "<img src=\"" + ResolveUrl("~/images/add_16_disabled.gif") + "\" runat=\"server\" width=\"16\" height=\"16\" border=\"0\"> Added to my library";
        }

        private void AddFriend()
        {
            if (Request.IsAuthenticated)
            {
                // Add them as a friend
                int ret = GetUserFacade.InsertFriendRequest(GetUserId(), pmToUserId,
                    Page.Request.CurrentExecutionFilePath, Global.RequestRabbitMQChannel());

                if (ret.Equals(1))	// already a friend
                {
                    AjaxCallHelper.WriteAlert("This member is already your friend.");
                }
                else if (ret.Equals(2))  // pending friend request
                {
                    AjaxCallHelper.WriteAlert("You have already sent this member a friend request.");
                }
                else
                {
                    // send request email
                    MailUtilityWeb.SendFriendRequestEmail(GetUserId(), pmToUserId);
                    spnPMAlertMsg.InnerHtml = "A friend request was sent to user " + UsersUtility.GetUserNameFromId(pmToUserId) + ".";
                }
            }
        }

        #endregion


        # region Event Handlers

        /// <summary>
        /// Handle an asset rave
        /// </summary>
        protected void lbRave_Click(Object sender, EventArgs e)
        {
            if (!Request.IsAuthenticated)
            {
                Response.Redirect(aJoinNow.HRef);
            }else
            {
                RaveAsset();
            }
        }

        /// <summary>
        /// Handle an asset add to favorites list
        /// </summary>
        protected void lbFave_Click(Object sender, EventArgs e)
        {
            if (!Request.IsAuthenticated)
            {
                Response.Redirect(aJoinNow.HRef);
            }
            else
            {
                AddMediaToFavorites();
            }
        }

        /// <summary>
        /// Share with a blast
        /// </summary>
        protected void btnBlast_Click(object sender, EventArgs e)
        {
            if (Request.IsAuthenticated)
            {
                // Check for any inject scripts
                if (KanevaWebGlobals.ContainsInjectScripts(txtBlast.Text))
                {
                    m_logger.Warn("User " + KanevaWebGlobals.GetUserId() + " tried to script from IP " + Common.GetVisitorIPAddress());
                    ShowErrorOnStartup("Your input contains invalid scripting, please remove script code and try again.", true);
                    return;
                }

                if (txtBlast.Text.Length == 0)
                {
                    AjaxCallHelper.WriteAlert("Please enter a blast.  You can not submit a blank blast.");
                    return;
                }

                if (txtBlast.Text.Length > KanevaGlobals.MaxCommentLength)
                {
                    AjaxCallHelper.WriteAlert("Your blast is too long.  Blasts must be " + KanevaGlobals.MaxCommentLength.ToString() + " characters or less");
                    return;
                }

                BlastFacade blastFacade = new BlastFacade();
                if (blastFacade.SendLinkBlast(GetUserId(), 0, KanevaWebGlobals.CurrentUser.Username, KanevaWebGlobals.CurrentUser.NameNoSpaces, txtBlastTitle.Text, txtShare.Text, txtBlast.Text, "", KanevaWebGlobals.CurrentUser.ThumbnailSmallPath).Equals(1))
                {
                    spnBlastAlertMsg.InnerText = "Blast was sent.";
                    btnBlast.Visible = false;
                }
                else
                {
                    spnBlastAlertMsg.InnerText = "Failure sending Blast.";
                }

                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('spnAlertMsg', 5000);");
            }

            txtBlast.Text = "";
        }

        /// <summary>
        /// Share with a private message
        /// </summary>
        protected void btnPMSend_Click(object sender, EventArgs e)
        {
            int toUserId = 0;
            string strSubject = Server.HtmlEncode(txtPrivateMsgSubject.Text.Trim());

            // Default the subject
            if (strSubject.Length.Equals(0))
            {
                strSubject = "No subject";
            }

            // Look up the user id via the username
            toUserId = UsersUtility.GetUserIdFromUsername(txtPrivateMsgTo.Text.Trim());
            pmToUserId = toUserId;

            if (txtPrivateMsgTo.Text.Trim() == "" || toUserId == 0)
            {
                spnPMAlertMsg.InnerText = "Please provide a valid Kaneva username.";
                return;
            }

            // Send the message
            int retCode = SendMessage(toUserId, strSubject);
            if (retCode.Equals(-99))
            {
                spnPMAlertMsg.InnerText = "Message not sent. This member has blocked you from sending messages to them.";
                return;
            }
            else if (retCode.Equals(Constants.ERR_USER_EXCEEDED_NON_FRIEND_MESSAGE_COUNT))
            {   // user has exceeded number of sent non-friend messages for today
                spnPMAlertMsg.InnerHtml = "You have exceeded the maximum daily number of private messages sent " +
                   " to non-friends (" + KanevaGlobals.MaxNumberNonFriendMessagesPerDay.ToString() +
                   "). In order to send this message, the person will need to add you as a friend." +
                   "<br/><br/><a href=\"javascript:void(0);\" " +
                   " onclick=\"$('" + lbAddFriend.ClientID + "').click();" + "\">Become a Friend</a>";
                return;
            }
            else
            {
                spnPMAlertMsg.InnerText = "Message Sent!";
                MagicAjax.AjaxCallHelper.Write("ShowConfirmMsg('spnPMAlertMsg', 4000);");
                // If this is an asset share log it
                if (Request["assetId"] != null)
                {
                    MediaFacade mediaFacade = new MediaFacade();
                    mediaFacade.ShareAsset(Convert.ToInt32(Request["assetId"]),
                        GetUserId(), Common.GetVisitorIPAddress(), null, toUserId);
                }
            }
        }

        /// <summary>
        /// Send Message
        /// </summary>
        /// <param name="toId"></param>
        private int SendMessage(int toId, string strSubject)
        {
            // confirm the user can send a pm to a non-friend (user has not exceeded daily max)
            if (!IsAdministrator())
            {
                if (UsersUtility.ConfirmCanSendNonFriendPM(GetUserId(), toId).Equals(Constants.ERR_USER_EXCEEDED_NON_FRIEND_MESSAGE_COUNT))
                {
                    return Constants.ERR_USER_EXCEEDED_NON_FRIEND_MESSAGE_COUNT;
                }
            }

            string strUrl = "<a href=\"" + txtShare.Text + "\">" + txtShare.Text + "</a><br />";

            // Insert a private message
            UserFacade userFacade = new UserFacade();
            Message message = new Message(0, GetUserId(), toId, strSubject,
                strUrl + txtPrivateMsgBody.Text, new DateTime(), 0, (int)Constants.eMESSAGE_TYPE.PRIVATE_MESSAGE, 0, "U", "S");

            return userFacade.InsertMessage(message);
        }

        protected void lbAddFriend_Click(object sender, EventArgs e)
        {
            // Must be logged in
            if (!Request.IsAuthenticated)
            {
                AjaxCallHelper.WriteAlert("Please sign in to add this user as a friend");
                return;
            }

            // Can't friend yourself
            if (GetUserId().Equals(pmToUserId))
            {
                AjaxCallHelper.WriteAlert("You cannot add yourself as a friend.");
                return;
            }

            AddFriend();
        }

        #endregion

        #region Properties

        public int AssetOwnerId
        {
            get { return _assetOwnerId; }
            set { _assetOwnerId = value; }
        }

        private int pmToUserId
        {
            get
            {
                if (ViewState["pmToUserId"] == null)
                {
                    ViewState["pmToUserId"] = 0;
                }
                return (int)ViewState["pmToUserId"];
            }
            set
            {
                ViewState["pmToUserId"] = value;
            }
        }

        #endregion

        #region Declerations

        protected Literal litStream = new Literal();
        protected string m_embedText = string.Empty;
        private int _assetOwnerId = 0;
        

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion
    }
}
