<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="assetDetailsFullScreen.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.assetDetailsFullScreen" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>
<%@ Register TagPrefix="UserControl" TagName="LightBox" Src="../usercontrols/LightBox.ascx" %>

<usercontrol:lightbox runat="server" id="ucLightBox" />

<script type="text/javascript" src="../jscript/SWFObject/swfobject.js"></script>
<script type="text/javascript" src="../jscript/prototype.js"></script>
<script type="text/javascript" src="../jscript/media-share.js"></script>

<link href="../css/new.css" rel="stylesheet" type="text/css"/>
<link href="../css/media-share.css" rel="stylesheet" type="text/css"/>
<asp:Literal id="litLandingPageCSS" runat="server" />

<script type="text/javascript">    
var $j = jQuery.noConflict();

$j(document).ready(function () {
    popFacebox = function () {
        $j.facebox({ iframe: '../buyRave.aspx?assetId=<asp:literal id="litAssetId" runat="server"></asp:literal>' }, '', 360);
    };

    var regurl = $j('#h1AssetTitle').attr("data-regurl");
    if (typeof regurl != 'undefined' && regurl.length > 0) {
        $j('#h1AssetTitle').click(function () {
            window.location.href = regurl;
        });
        $j('#h1AssetTitle').addClass('pseudoLink');
    }
});


	
var raveCountCtrlName = '<asp:literal id="litCtrlRaveCount" runat="server"></asp:literal>'; 

</script>
											
<div id="divDetailsMissing" runat="server" visible="false">
</div>
<div id="divDetails" runat="server" visible="false" style="position:relative; z-index:1">
	<table  border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color:White; height:100%">
		<!-- ABOUT MEDIA -->
		<tr>
			<td align="center" colspan="3">
	            <div class="module" style="height:100%;margin:0 auto;width:702px;background-image:none;border-style:none;">
		            <!--<span class="ct"><span class="cl"></span></span>-->
		            <h1 id="h1AssetTitle" style="padding:0;color:#333333;font-weight:normal;margin:5px 0 10px 0;cursor:default;" runat="server"></h1>
		            <div style="width:100%;background-color:#ffffff;overflow:hidden;border:0px solid #e0e0e0;margin:0 auto;">
		                <!-- PLAYER / IMAGE -->
		                <asp:placeholder runat="server" id="phStream"/>
		            </div>
																
		            <ajax:ajaxpanel id="ajpActionBar" runat="server">																
		            <!-- TOOLBAR -->
		            <table border="0" cellspacing="0" cellpadding="0" width="700px" align="center">
			            <tr>																			  
				            <td>
					            <ul id="mediaNav" style="margin:4px 0 10px 0px; text-align:center; width:100%">
					                
						            <li id="rave" runat="server"><asp:linkbutton id="lbRave" onclick="lbRave_Click" visible="true" runat="server" style="margin-left:130px" tooltip="Rave this Item"></asp:linkbutton></li>
                                    <li id="add" runat="server"><asp:linkbutton id="lbFave" onclick="lbFave_Click" visible="true" runat="server" tooltip="Add to my library"></asp:linkbutton></li>
									<li id="share"><asp:linkbutton id="lb_link_1" visible="true" runat="server" tooltip="E-mail this, post to del.icio.us, etc."></asp:linkbutton></li>
									<li id="report"><asp:linkbutton id="lbReport" visible="true" runat="server" tooltip="Report Abuse"></asp:linkbutton></li>
									<li id="liJoinNow" runat="server" visible="false" class="promiseContainer">
                                        <div id="divUpsellImageWrapper" runat="server" visible="false" class="upsellImageWrapper">
                                            <div id="divUpsellImage" runat="server"  class="upsellImage"></div>
                                        </div>
                                        <h2 id="h1UpsellPromise" runat="server"></h2>
                                        <a id="aJoinNow" runat="server" class="joinNow"></a>
                                        <a id="aItsFree" runat="server">-It's Free!</a>
                                    </li>											
					            </ul>
				            </td>
			            </tr>																			  
		            </table>				
					
		            <!-- Instructions -->
		            <table style="width:700px; height:100px;" width="700px" border="0" cellspacing="0" cellpadding="0" align="center">
						<tr runat="server" id="trInstructions1" visible="false">
							<td valign="bottom"><strong>Instructions:</strong></td>
						</tr>
						<tr runat="server" id="trInstructions" visible="false">
							<td valign="top"><asp:textbox style="width:100%;" cssclass="formKanevaText" textmode="MultiLine" id="txtInstructions" runat="server" columns="60" rows="4" readonly="True"></asp:textbox><br>
							</td>
						</tr>		            
					</table>
					<asp:linkbutton id="lbAddFriend" runat="server" onclick="lbAddFriend_Click" style="display:none;"></asp:linkbutton>
		            </ajax:ajaxpanel>
					<!--<span class="cb"><span class="cl"></span></span>-->
		        </div>
		    </td>
	    </tr>
    </table>
    
 <asp:textbox style="width: 355px;" cssclass="formKanevaText" id="txtShare" runat="server" columns="60" visible="false" Readonly="True" />
   
<!-- Share This BEGIN -->
<asp:literal runat="server" id="litNotLoggedShareStyle"></asp:literal>

	<div id="akst_form" >
		<a href="javascript:void($('akst_form').style.display='none');" class="akst_close">Close</a>
		<ul class="tabs">
			
			<li id="akst_tab1" onclick="akst_share_tab('1');" runat="server">Blast</li>
			<li id="akst_tab2" onclick="akst_share_tab('2');" runat="server">Private</li>			
			<li id="akst_tab3" onclick="akst_share_tab('3');" runat="server">E-mail</li>
			<li id="akst_tab4" onclick="akst_share_tab('4');" runat="server">Social Web</li>						
		</ul>

		<div class="clear"></div>
		
		<div id="akst_blast">
		  <fieldset>
		  <ajax:ajaxpanel id="ajpShareBlast" runat="server">		  
		  <ul>
		    <li>Blast this to your friends!</li>
		    <li>Link Title: <asp:textbox id="txtBlastTitle" runat="server" width="265"></asp:textbox></li>
		    <li>Your Message (Link will automatically be inserted):</li>	    
		    <li><asp:textbox TextMode="multiline" id="txtBlast" runat="server" width="325" height="80"></asp:textbox></li>
		    <li><asp:button id="btnBlast" onClick="btnBlast_Click" Text="Blast It!" runat="server" />
		        <h2 class="alertmessage"><span id="spnBlastAlertMsg" runat="server"></span></h2>
		    </li>		  
		  </ul>	
		  </ajax:ajaxpanel>	  		 
		  </fieldset>
		</div>		
		
	    <div id="akst_private">
		  <fieldset>	
		  <ajax:ajaxpanel id="ajpSharePM" runat="server">	  
		  <ul>
		  <li>Private Message To Kaneva Member:</li>
		    <li><asp:textbox  id="txtPrivateMsgTo" runat="server" width="325"></asp:textbox></li>
		    <li>Subject:</li>
		    <li><asp:textbox  id="txtPrivateMsgSubject" runat="server" width="325"></asp:textbox></li>
		    <li>Your Message (Link will automatically be inserted):</li>
		    <li><asp:textbox TextMode="multiline" id="txtPrivateMsgBody" runat="server" width="325" height="50"></asp:textbox></li>
		    <li><asp:button id="btnPMSend" onClick="btnPMSend_Click" Text="Send" runat="server" />
		     <h2 class="alertmessage"><span id="spnPMAlertMsg" runat="server"></span></h2>
		    </li>		  
		  </ul>	
		  </ajax:ajaxpanel>	  		 
		  </fieldset>
		</div>
		
		<div id="akst_email">			
				<fieldset>
					<legend>E-mail It</legend>
					<ul>
						<li>
							<label>1. Copy and paste this link into an email or instant message:</label>
							<asp:textbox id="shareLink" runat="server" class="akst_text" width="325" onclick="this.select();"></asp:textbox>							
						</li>
						<li>
							<label>2. Send this media using your computer's email application:</label>
							<asp:hyperlink id="shareMailto" runat="server" NavigateUrl="" text="Email Media" ></asp:hyperlink>
						</li>					
					</ul>
					<input type="hidden" name="akst_action" value="send_mail" />
					<input type="hidden" name="akst_post_id" id="akst_post_id" value="" />
				</fieldset>			
		</div>		
				
		<div id="akst_social">
			<ul>
				<li><a href="#" id="akst_delicious" target="_blank">del.icio.us</a></li>
				<li><a href="#" id="akst_digg" target="_blank">Digg</a></li>
				<li><a href="#" id="akst_furl" target="_blank">Furl</a></li>
				<li><a href="#" id="akst_netscape" target="_blank">Netscape</a></li>
				<li><a href="#" id="akst_yahoo_myweb" target="_blank">Yahoo! My Web</a></li>
				<li><a href="#" id="akst_stumbleupon" target="_blank">StumbleUpon</a></li>
				<li><a href="#" id="akst_google_bmarks" target="_blank">Google Bookmarks</a></li>
				<li><a href="#" id="akst_technorati" target="_blank">Technorati</a></li>
				<li><a href="#" id="akst_blinklist" target="_blank">BlinkList</a></li>
				<li><a href="#" id="akst_newsvine" target="_blank">Newsvine</a></li>
				<li><a href="#" id="akst_magnolia" target="_blank">ma.gnolia</a></li>
				<li><a href="#" id="akst_reddit" target="_blank">reddit</a></li>
				<li><a href="#" id="akst_windows_live" target="_blank">Windows Live</a></li>
				<li><a href="#" id="akst_tailrank" target="_blank">Tailrank</a></li>
			</ul>
			<div class="clear"></div>
		</div>
			
	</div>
<!-- Share This END -->    						
</div>

																	