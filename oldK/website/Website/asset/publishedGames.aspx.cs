///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using log4net;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for publishedGames.
	/// </summary>
	public class publishedGames : StoreBasePage
	{
		/// <summary>
		/// publishedGames
		/// </summary>
		protected publishedGames () 
		{
			Title = "My Published Games";
		}

		/// <summary>
		/// Page_Load
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Page_Load (object sender, System.EventArgs e)
		{
            string status301 = "301 Moved Permanently";
            Response.Status = status301;
            Response.AddHeader ("Location", "http://support.kaneva.com/ics/support/default.asp?deptID=5433");
            Response.End ();
            
            // They must be logged in
            //if (!Request.IsAuthenticated)
            //{
            //    Response.Redirect (GetLoginURL ());
            //    return;
            //}

			
            //if (!IsPostBack)
            //{
            //    // Set the category on the filter.
            //    BindData (1, "");
            //}
		}

		/// <summary>
		/// Click the add button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnAddAsset_Click (Object sender, ImageClickEventArgs e) 
		{
			Server.Transfer (GetAssetEditLink (0));
		}

		/// <summary>
		/// Delete an asset
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void Delete_Asset (Object sender, EventArgs e)
		{
			int userId = GetUserId ();
			int assetId = Convert.ToInt32 (AssetId.Value);

			if (!StoreUtility.IsAssetOwner (userId, assetId))
			{
				m_logger.Warn ("User " + userId + " from IP " + Common.GetVisitorIPAddress() + " tried to delete store item " + AssetId.Value + " but is not the asset owner");
				return;
			}

			StoreUtility.DeleteAsset (assetId, userId);
			pg_PageChange (this, new PageChangeEventArgs (pgTop.CurrentPageNumber));
		}

		private void pg_PageChange (object sender, PageChangeEventArgs e)
		{
			BindData (e.PageNumber, filStore.CurrentFilter);
		}

		private void filStore_FilterChanged(object sender, FilterChangedEventArgs e)
		{
			pgTop.CurrentPageNumber = 1;
			BindData (1, e.Filter);
		}

		private void BindData (int pageNumber, string filter)
		{
			// Set the sortable columns
			SetHeaderSortText (dgrdAsset);
			SetHeaderSortText (listAsset);
			string orderby = CurrentSort + " " + CurrentSortOrder;

            //PagedDataTable pds = StoreUtility.GetPublishedGames (GetUserId (), filter, orderby, pageNumber, filStore.NumberOfPages);
            //pgTop.NumberOfPages = Math.Ceiling ((double) pds.TotalCount / filStore.NumberOfPages).ToString ();
            //pgTop.DrawControl ();

            //dgrdAsset.DataSource = pds;
            //dgrdAsset.DataBind ();			


            //switch (filStore.CurrentView)
            //{

            //    case "li":
            //    {
            //        listAsset.Visible = false;
            //        dgrdAsset.Visible = true;
            //        dgrdAsset.DataSource = pds;
            //        dgrdAsset.DataBind ();
            //        break;
            //    }

            //    default: 
            //    {
            //        dgrdAsset.Visible = false;
            //        listAsset.Visible = true;
            //        listAsset.DataSource = pds;
            //        listAsset.DataBind ();
            //        listAsset.Columns[0].Visible = IsAdministrator();
            //        break;
            //    }

			//}

				
            //// The results
            //lblSearch.Text = GetResultsText (pds.TotalCount, pageNumber, filStore.NumberOfPages, pds.Rows.Count);
		}

		/// <summary>
		/// They clicked to sort a column
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected override void btnSort_Click (object sender, System.EventArgs e)
		{
			base.btnSort_Click (sender, e);
			BindData (pgTop.CurrentPageNumber, filStore.CurrentFilter);
		}

		/// <summary>
		/// DEFAULT_SORT
		/// </summary>
		/// <returns></returns>
		protected override string DEFAULT_SORT
		{
			get
			{
				return "created_date";
			}
		}

		/// <summary>
		/// DEFAULT_SORT_ORDER
		/// </summary>
		/// <returns></returns>
		protected override string DEFAULT_SORT_ORDER
		{
			get
			{
				return "DESC";
			}
		}

		/// <summary>
		/// Get the status link
		/// </summary>
		/// <param name="torrentStatus"></param>
		/// <returns></returns>
		protected string GetStatusLink (int statusId)
		{
			return "javascript:window.open('" + ResolveUrl ("~/asset/gamePublishStatus.aspx#" + statusId + "','add','toolbar=no,width=600,height=250,menubar=no,scrollbars=yes,status=yes').focus();return false;");
		}

		/// <summary>
		/// GetStatusText
		/// </summary>
		/// <param name="statusId"></param>
		protected string GetStatusText (int statusId)
		{
			string strText = "";

			switch (statusId)
			{
				case ((int) Constants.eASSET_STATUS.MARKED_FOR_DELETION):
				{
					strText = "Scheduled for deletion";
					break;
				}
				case ((int) Constants.eASSET_STATUS.NEW):
				{
					strText = "<font color=\"green\">Active</font>";
					break;
				}
				case ((int) Constants.eASSET_STATUS.ACTIVE):
				{
					return "<font color=\"green\">Active</font>";
				}
				case -199192:
				{
					strText = "Unknown -";
					break;
				}
				default:
				{
					strText = "Unknown " + statusId;
					break;
				}
			}

			return "<font color=\"red\">" + strText + "</font>";
		}

		/// <summary>
		/// IsDeleted
		/// </summary>
		protected bool IsDeleted (int statusId)
		{
			return (statusId.Equals ((int) Constants.eASSET_STATUS.MARKED_FOR_DELETION));
		}

		/// <summary>
		/// GetLauncherLink
		/// </summary>
		protected string GetLauncherLink (int assetId)
		{
            string gameLauncherExe = System.Configuration.ConfigurationManager.AppSettings["KGLFilename"].ToString();
			string filePath = Server.MapPath ("~/download/launcher/" + assetId.ToString () + "/" + gameLauncherExe);

			if (!System.IO.File.Exists (filePath))
			{
				return "javascript:alert('Game launcher was not found on server.');";
			}

			return ResolveUrl ("~/download/launcher/" + assetId.ToString () + "/" + gameLauncherExe);
		}

		protected Kaneva.Pager pgTop;
		protected Kaneva.PublishedItemsFilter filStore;
		protected DataGrid dgrdAsset;
		protected DataGrid listAsset;
		protected HtmlInputHidden AssetId;

		// Filter
		protected DropDownList drpCategory, drpMonth, drpAuthor, drpView, drpPages;

		protected Label lblSearch;

		// Logger
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
			pgTop.PageChanged +=new PageChangeEventHandler (pg_PageChange);
			filStore.FilterChanged	+=new FilterChangedEventHandler(filStore_FilterChanged);
		}
		#endregion
	}
}
