<%@ Page language="c#" Codebehind="publishValue.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.publishValue" %>
<%@ Register TagPrefix="Kaneva" TagName="HeaderText" Src="../usercontrols/HeaderText.ascx" %>
<Kaneva:HeaderText runat="server" Text="The Value of Kaneva Publishing" ID="Headertext1"/>

<br>
<center>
<table cellpadding="0" cellspacing="0" border="0" width="710">
<tr>
	<td class="bodyText" style="padding-left:15px;" colspan="2" with="50%">
<span class="orangeHeader3">The Value of Kaneva Publishing</span><br><br>

<p>Kaneva is ushering in a new world of online publishing that removes many traditional barriers to entry, giving filmmakers and game creators more opportunities to be successful.</p>

<p>By partnering with Kaneva, you can publish, distribute, sell and market your work, build and connect with a rapidly growing fan base and earn worldwide recognition � all while maintaining your rights and control as well as gaining the opportunity to earn significant royalties. You pay no up front fees and keep half of all money earned each time your media is sold.</p>

<p>Kaneva takes care of all the behind the scenes technological and management work. By harnessing the power of the Internet and utilizing state-of-the-art online publishing technology, Kaneva makes it easier than ever to publish and distribute your digital media online. And we are building the world�s best online destination for watching films and playing games with the addition of many new registered members each month.</p> 

<p>In the traditional publishing realm, filmmakers and game developers have been faced with extremely slim chances of making an impact on the industry, and an even slimmer one of capturing a significant share of the public�s attention. If you happen to break through and get published, it is usually with the loss of rights and control, low and unfair royalty splits and limited, short-term visibility. As a result, many creative talents and stories have been stifled or hidden from the opportunity for public consumption. Kaneva is a place where creativity can reign and where entertainment consumers can have a strong voice and truly dictate the popularity and success of games and films.</p> 



<img runat="server" src="~/images/PublishVS2.gif" border="0"/><br><br>



<p>In addition to online publishing, Kaneva provides extensive business services, marketing, support, community and enabling technology including:</p>
<br>
</td>
</tr>
<tr>
<td class="bodyText" style="padding-left:15px;" with="350" valign="top">

<b>Business Services</b><br>
<ul>
	<li>Sales tracking</li>		
	<li>Payment processing</li>			
	<li>Hosting (Kaneva Platform-built MMO games)</li>	
	<li>Billing</li>	
	<li>Security</li>	
</ul>

<b>Marketing</b><br>
<ul>
	<li>Community creation</li>			
	<li>Online marketing</li>	 		
	<li>Contests</li>				
	<li>Customized searching</li>	
	<li>PR, advertising and promotions</li>	
	<li>Online film and game festivals</li>	
</ul>	

</td>
<td class="bodyText" style="padding-left:15px;" with="350" valign="top">

<b>Support</b><br>
<ul>
	<li>Online tutorials and videos</li>	
	<li>Extensive documentation</li>	
<li>Art asset marketplace</li>
<li>Developers� and filmmaker�s forums</li>	
<li>Customer support forums</li>	
</ul>

<b>Community</b><br>
<ul>
	<li>Fast growing membership</li>	
	<li>Active forums and blogs</li>	
</ul>
<b>Enabling Technology</b><br>
<ul>
	<li>Kaneva Media Launcher/Kaneva Game Launcher for online publishing</li>	
	<li>Kaneva Media Launcher and BitTorrent program support for fast downloading of high quality digital media</li>	
	<li>Kaneva Game Platform, first freely downloadable MMO game engine</li>	
	<li>Support for new, flexible formats (Mini movies, online game and TV pilots)</li>	
</ul>	
	</td>
</tr>
<tr>
	<td class="bodyText" style="padding-left:15px;" colspan="2">Thanks to Kaneva, you now have a low-risk, accessible and immediate global outlet for connecting with fans online. Join Kaneva and start sharing your creative work with the world today. <br><br><br><br></td>
</tr>
</table>
</center>