///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Specialized;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using log4net;

using KlausEnt.KEP.Kaneva.usercontrols;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;
using System.Text.RegularExpressions;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for assetDetails.
	/// </summary>
	public class assetDetailsLandingPage : BasePage
	{
		/// <summary>
		/// Page_Load
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Page_Load(object sender, System.EventArgs e)
		{
            if (!IsPostBack)
            {
                // If we're authenticated we need to be on the other asset page
                if (Request.IsAuthenticated)
                {
                    // Issue a 301 redirect instead of a 302 to preserve SEO. Honestly, this shouldn't make any difference
                    // as search engines are likely not authenticated, but may as well.
                    Response.Status = "301 Moved Permanently";
                    Response.AddHeader("Location", ResolveUrl("~/asset/assetDetails.aspx?" + Request.QueryString));
                    Response.End();
                }
                else
                {
                    DataRow drAsset = null;
                    bool show_page = true;

                    // Try grabbing the requested asset, if not available redirect to error or home
                    if (Request["assetId"] == null)
                    {
                        Response.Redirect("~/free-virtual-world.kaneva");
                    }

                    try
                    {
                        int assetId = Convert.ToInt32(Request["assetId"]);
                        drAsset = StoreUtility.GetAsset(assetId, false, GetUserId());
                    }
                    catch (Exception ex)
                    {
                        m_logger.Error("Error loading asset details page: ", ex);
                        Response.Redirect("~/free-virtual-world.kaneva");
                    }

                    if (drAsset == null || drAsset["asset_id"].Equals(DBNull.Value) || drAsset["status_id"].Equals((int)Constants.eASSET_STATUS.MARKED_FOR_DELETION) || drAsset["status_id"].Equals((int)Constants.eASSET_STATUS.DELETED))
                    {
                        Response.Redirect(ResolveUrl("~/asset/notAvailable.aspx?M=NA"));
                    }

                    // Grab out asset info from the data row
                    AssetOwnerId = Convert.ToInt32(drAsset["user_id"]);
                    int assetTypeId = Convert.ToInt32(drAsset["asset_type_id"]);
                    string assetType = KanevaWebGlobals.GetAssetTypeName(assetTypeId);
                    if ((int)Constants.eASSET_TYPE.VIDEO == assetTypeId)
                    {
                        spnRuntime.Visible = true;
                    }

                    // If item is mature, block the page (we're not logged in, thus can't have an access pass)
                    if (IsMature(Convert.ToInt32(drAsset["asset_rating_id"])))
                    {
                        show_page = false;
                        h2Title.InnerText = "This " + assetType + " is Restricted";

                        pActionText.InnerHtml = "The " + assetType + " you are trying to access has been set to \"Restricted\" by its " +
                        "owner and may contain material inappropriate for anyone under the age of 18. Please log in to verify your right " +
                        "to access this page.";

                        pPolicyText.InnerHtml = COMMUNITY_GUIDELINES;
                        pPolicyText.Visible = false;

                        btnAction.Text = " Sign in ";
                        btnAction.Attributes.Add("onclick", "location.href='" + GetLoginURL() + "';return false;");

                        divAccessMessage.Visible = true;
                    }
                    //else check to see if the item is marked as private for people without editing rights
                    else
                    {
                        int group_id = drAsset["permission_group"] != DBNull.Value ? Convert.ToInt32(drAsset["permission_group"].ToString()) : 0;

                        switch (Convert.ToInt32(drAsset["permission"]))
                        {
                            // public
                            case (int)Constants.eASSET_PERMISSION.PUBLIC:
                                break;

                            // private
                            case (int)Constants.eASSET_PERMISSION.PRIVATE:
                                show_page = false;

                                h2Title.InnerText = "This " + assetType + " is Private and Viewable by the Owner only";

                                pActionText.InnerText = "The " + assetType + " you are trying to access has been set to \"Private\" by its owner " +
                                    "and is currently not available for public viewing.";

                                pPolicyText.InnerHtml = PRIVACY_SETTINGS;

                                btnAction.Attributes.Add("onclick", "javascript:history.back();return false;");
                                btnAction.Text = "  Go Back  ";

                                divAccessMessage.Visible = true;
                                break;

                            default:
                                break;
                        }
                    }

                    // If we're able to show the page, bind the asset data
                    if (show_page)
                    {
                        divDetails.Visible = true;
                        BindData(drAsset);
                    }

                    // Show asset name
                    h1AssetTitle.InnerText = Server.HtmlDecode(drAsset["name"].ToString());

                    // set all meta data
                    string _mediaName = "";
                    string _mediaDescription = "";
                    NameValueCollection tagList = new NameValueCollection();

                    if (drAsset != null)
                    {
                        _mediaName = drAsset["name"].ToString();
                        _mediaDescription = drAsset["teaser"].ToString();
                        string[] tags = (drAsset["keywords"].ToString()).Split(' ');

                        //default media name as one of the tags
                        tagList.Add(_mediaName, _mediaName);

                        for (int i = 0; i < tags.Length; i++)
                        {
                            tagList.Add(tags[i], tags[i]);
                        }
                    }

                    ((GenericLandingPageTemplate)Master).Title = MetaDataGenerator.GenerateTitle(_mediaName, "", (int)Constants.eWEB_PAGE_TYPE.MEDIA) + " - Kaneva " + assetType;
                    ((GenericLandingPageTemplate)Master).MetaDataDescription = MetaDataGenerator.GenerateMetaDescription(_mediaDescription, tagList, (int)Constants.eWEB_PAGE_TYPE.MEDIA, true);
                    ((GenericLandingPageTemplate)Master).MetaDataKeywords = MetaDataGenerator.GenerateMetaKeywords(_mediaName, tagList, (int)Constants.eWEB_PAGE_TYPE.MEDIA, true);
                }
            }
		}


		# region Helper Methods
		/// <summary>
		/// Bind the page data
		/// </summary>
		private void BindData (DataRow drAsset)
		{
			int assetTypeId = Convert.ToInt32 (drAsset ["asset_type_id"]);
			int assetId = Convert.ToInt32 (drAsset ["asset_id"]);
			int userId = GetUserId ();
			int ownerUserId = Convert.ToInt32 (drAsset ["user_id"]);
            string assetName = Server.HtmlDecode (drAsset ["name"].ToString ());

            // -- Upsell Section --
            // ------------------------
            string regURL = ResolveUrl("~/register/kaneva/registerInfo.aspx") + "?lpage=mediafull&mediatype=";
            string upsellMessage = "";
            string upsellImgClass = "";
            bool showUpsellImg = true;

            switch (assetTypeId)
            {
                case (int)Constants.eASSET_TYPE.MUSIC:
                    upsellMessage = "Join Kaneva and listen to the latest raved music.";
                    upsellImgClass += " musicUpsell ";
                    regURL += "music";
                    break;
                case (int)Constants.eASSET_TYPE.PICTURE:
                case (int)Constants.eASSET_TYPE.PATTERN:
                    upsellMessage = "Join Kaneva and get access to over 100,000 photos.";
                    upsellImgClass += " imageUpsell ";
                    regURL += "image";
                    break;
                case (int)Constants.eASSET_TYPE.VIDEO:
                    upsellMessage = "Join Kaneva and get access to the most popular videos.";
                    upsellImgClass += " videoUpsell ";
                    regURL += "video";
                    break;
                default:
                    upsellMessage = "Join today - it's free.";
                    showUpsellImg = false;
                    break;
            }

            // Configure upsell area
            h2UpsellPromise.InnerText = upsellMessage;
            aJoinNow.HRef = regURL;
            aItsFree.HRef = regURL;
            aJoinToday.HRef = regURL;
            
            if (showUpsellImg)
            {
                divUpsellImageWrapper.Visible = true;
                divUpsellImage.Attributes["class"] += upsellImgClass;
            }
			
			// -- ACTION BAR SECTION --
			// ------------------------
            lbRave.Attributes.Add("href", regURL);
            lb_link_1.Attributes.Add("href", regURL);
            lbReport.Attributes.Add("href", regURL);

			StreamAsset (drAsset);


			// -- ITEM DETAILS SECTION --
			// --------------------------

			// Is it mature?	  
			divRestricted.Visible = IsMature (Convert.ToInt32 (drAsset ["asset_rating_id"]));
			
			// asset name
			lblName.Text = TruncateWithEllipsis (assetName, 45);

			// asset image		
			imgAssetThumbnail.Src = GetMediaImageURL (drAsset ["thumbnail_small_path"].ToString	(), "sm", assetId, assetTypeId, Convert.ToInt32 (drAsset ["thumbnail_gen"])); 

			// description
			lblDescription.Text = drAsset ["teaser"].ToString ();

			// stats
			lblRuntime.Text = FormatSecondsAsTime (drAsset["run_time_seconds"]);
			lblCreatedDate.Text = FormatDate (drAsset ["created_date"]) + " <span class=\"note\">(" + FormatDateTimeSpan(drAsset ["created_date"]) + ")</span>";
			lblRaves.Text = drAsset ["number_of_diggs"].ToString ();
			lblNumViews.Text = drAsset ["number_of_downloads"].ToString ();
			
			//**NOTE** There is a bug in the number_of_channels count.  This value is being set in the
			// BuildTopChannelList() method until the trigger is fixed to correctly keep the channel count
			lblNumTimesAdded.Text = drAsset ["number_of_channels"].ToString () + " time" + (Convert.ToInt32(drAsset ["number_of_channels"]) == 1 ? "" : "s");
			
            //instructions if any
            if((drAsset["instructions"] != null) && (drAsset["instructions"].ToString() != ""))
            {
                divInstructions.Visible = true;
                divInstructionText.InnerText = drAsset["instructions"].ToString();
            }
			
            //company name if any
            if((drAsset["company_name"] != null) && (drAsset["company_name"].ToString() != ""))
            {
                lblCompanyName.Text = "Company Name: " + drAsset["company_name"].ToString();
            }

			lblNumComments.Text = drAsset ["number_of_comments"].ToString ();

			// owner info
            UserFacade userFacade = new UserFacade();
            User userOwner = userFacade.GetUser(ownerUserId);

            imgAvatar.Src = GetProfileImageURL(userOwner.ThumbnailSmallPath, "sm", userOwner.Gender, userOwner.FacebookSettings.UseFacebookProfilePicture, userOwner.FacebookSettings.FacebookUserId);
            imgAvatar.Alt = userOwner.Username;

            divOwnerRestricted.Visible = userOwner.MatureProfile;

            aUserName.Attributes.Add("href", GetPersonalChannelUrl(userOwner.NameNoSpaces));
            aUserName.InnerText = userOwner.Username;

            // -- ADDITIONAL INFO SECTION --
            // --------------------------
			// Username and URL 
            aUserName3.Attributes.Add("href", GetPersonalChannelUrl(userOwner.NameNoSpaces));

			// share link
			divShare.InnerText = Server.HtmlDecode("http://" + Request.Url.Host + GetAssetDetailsLink (assetId));

            //set the full screen option
            lbFullScreen.Attributes.Add("onclick", "assetFullScreen('http://" + KanevaGlobals.SiteName + "/asset/assetDetailsFullScreen.aspx?assetId=" + assetId + "','" + assetName + " (Full SCreen)')");
            lbFullScreen.Attributes.Add("href", divShare.InnerText + "?autoPlay=false");
        
			// embed link
			divEmbed.InnerText = m_embedText;	

			// related items
			lblRelatedByTopic.Text = GetAssetTags (drAsset ["keywords"].ToString (), assetTypeId);
			lblRelatedByTopic.Text += GetAssetTags (drAsset ["categories"].ToString(), assetTypeId);

            lblRelatedByTopic.Text = Regex.Replace(lblRelatedByTopic.Text, hrefRegex, "href=\"" + regURL + "\"");

			string asset_name = KanevaWebGlobals.GetAssetTypeName(assetTypeId).ToLower ();
			spnRelatedType.InnerText = asset_name == "audio" ? "music" : asset_name+"s";

			// sharing communities
			BuildTopChannelList (drAsset, ownerUserId);

			// license info
			GetCopyrightInfo (drAsset, lblLicense, lblLicenseURL);

			// -- RELATED ITEMS SECTION --
			// ---------------------------
			ucRelatedItems.AssetTypeId = assetTypeId;
			ucRelatedItems.AssetId = assetId;
			ucRelatedItems.ItemsPerPage = 5;
			ucRelatedItems.AssetOwnerId = ownerUserId;


			// -- COMMENTS SECTION --
			// ----------------------
			ucComments.AssetId = assetId;
			ucComments.AssetOwnerId = AssetOwnerId;
            ucComments.RegURL = regURL;
							 
		}
		
		/// <summary>
		/// StreamAsset
		/// </summary>
		/// <param name="drAsset"></param>
		private void StreamAsset(DataRow drAsset)
		{
            bool autoPlay = false;
            MediaFacade mediaFacade = new MediaFacade();

			Response.Expires = 0;
			Response.CacheControl = "no-cache";

			int assetId = Convert.ToInt32 (drAsset["asset_id"]);

            //logic check to prevent play back on main page for when the user clicks the view in large screen;
            if ((Request["autoPlay"] == null) || ((Request["autoPlay"] != "false")))
            {
                autoPlay = true;
            }

			// Streamable asset?
			if (!StoreUtility.IsAssetStreamable (drAsset) && !StoreUtility.IsYouTubeContent(drAsset) &&
                !StoreUtility.IsStreamingTVContent (drAsset) && !StoreUtility.IsFlashWidget (drAsset) && !StoreUtility.IsFlashContent (drAsset))
			{
				ShowErrorOnStartup ("File is being converted, the process could take several minutes, please try again later.");
				return;
			}	
			
			if (Convert.ToInt32(drAsset["publish_status_id"]) != (int) Constants.ePUBLISH_STATUS.PUBLISH_COMPLETE )
			{
				ShowErrorOnStartup ("File is being published, the process could take several minutes, please try again later.");
				return;
			}

			// Set up links
			string name = Server.HtmlDecode (drAsset ["name"].ToString ());
			
			// The URL to the content on the streaming server
            string contentURL = Configuration.StreamingServer + "/" + Configuration.StreamingFile + "?tId=" + assetId;
            if (Configuration.StreamIIS)
            {
                contentURL = Configuration.StreamingServer + "/" + drAsset["media_path"].ToString();
            }

			int height = 378; 
			int width = 450; 

			string dimensions = " height=\"" + height + "\" width=\"" + width + "\" ";

			// Is it playable with Kaneva OMM?
			if (StoreUtility.IsOMMContent (drAsset))
			{
				m_embedText = litStream.Text = GetAssetEmbedLink (assetId, height, width, true ); // pass in true for fullURL only since required for embed link
				litStream.Text = StoreUtility.GetAssetEmbedLink(assetId, height, width, Page, false, false, autoPlay);
			}
			else if (StoreUtility.IsQuickTimeContent (drAsset))
			{
				// Don't do this for OMM, OMM takes care of that
				// Update the media views
                mediaFacade.UpdateMediaViews (assetId, Common.GetVisitorIPAddress(), GetUserId ());

				if (StoreUtility.IsAudioContent (drAsset))
				{
					dimensions = "height=\"16\" width=\"" + width.ToString () + "\"";

					// Use Quicktime for these audio
					m_embedText = litStream.Text = "<object classid=\"clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B\" codebase=\"http://www.apple.com/qtactivex/qtplugin.cab\" " + dimensions + ">" +
						"<param name=\"src\" value=\"" + contentURL + "\">" +
                        "<param name=\"autoplay\" value=\"" + autoPlay + "\">" +
						"<param name=\"controller\" value=\"true\">" +
                        "<embed " + dimensions + " src=\"" + contentURL + "\" pluginspage=\"http://www.apple.com/quicktime/download/\" type=\"video/quicktime\" controller=\"true\" autoplay=" + autoPlay + ">" +
						"</object>";
				}
				else
				{
					// Use Quicktime for these videos, set scale to ASPECT
					m_embedText = litStream.Text = "<object classid=\"clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B\" codebase=\"http://www.apple.com/qtactivex/qtplugin.cab\" " + dimensions + ">" +
						"<param name=\"src\" value=\"" + contentURL + "\">" +
                        "<param name=\"autoplay\" value=\"" + autoPlay + "\">" +
						"<param name=\"controller\" value=\"true\">" +
						"<PARAM NAME=\"scale\" VALUE=\"ASPECT\">" +
                        "<embed scale=\"ASPECT\" src=\"" + contentURL + "\" pluginspage=\"http://www.apple.com/quicktime/download/\" type=\"video/quicktime\" controller=\"true\" autoplay=" + autoPlay + ">" +
						"</object>";
				}
			}
			else if (StoreUtility.IsFlashContent (drAsset))
			{
				// Don't do this for OMM, OMM takes care of that
				// Update the media views
                mediaFacade.UpdateMediaViews (assetId, Common.GetVisitorIPAddress(), GetUserId ());

				if (drAsset ["asset_type_id"].Equals ((int) Constants.eASSET_TYPE.WIDGET) ||
                    (drAsset["asset_type_id"].Equals ((int) Constants.eASSET_TYPE.GAME) && drAsset["media_path"].ToString() == ""))
				{
					contentURL = drAsset["asset_offsite_id"].ToString();
				}

				m_embedText = litStream.Text = "<OBJECT classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000' " +
					" codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0' " +
					" id=\"swfStream\" " + dimensions + ">" +
					" <param name='movie' value=\"" + contentURL + "\">" +
					" <param name='quality' value=\"high\">" +
					" <param name='loop' value=\"false\">" +
                    " <PARAM NAME='AutoStart' VALUE=\"" + autoPlay + "\">" +
					" <EMBED src=\"" + contentURL + "\" quality='high' " + dimensions +
                    " loop=\"false\" autostart=\"" + autoPlay + "\" type='application/x-shockwave-flash'" +
					" pluginspage='http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash'>" +
					" </EMBED> " +
					"</OBJECT>";
			}
			else if (StoreUtility.IsImageContent (drAsset))
			{
                //hide the pop up window
                divNewWindow.Visible = false;

				// Don't do this for OMM, OMM takes care of that
				// Update the media views
                mediaFacade.UpdateMediaViews (assetId, Common.GetVisitorIPAddress(), GetUserId ());

				// It is an image
				litStream.Text = "<div style=\"width: " + width.ToString () + "px; background-color: #FFFFFF; overflow: hidden; border: 1px solid #e0e0e0;\">" +
                    "<a href=\"" + ResolveUrl("~/mykaneva/PictureDetail.aspx") + "?assetId=" + assetId.ToString() + "\" border=\"0\">" +
                    "<img src=\"" + GetPhotoImageURL(drAsset["thumbnail_assetdetails_path"].ToString(), "la") + "\" border=\"0\" alt=\"" + name + "\" title=\"" + name + "\"/></a></div>";

				m_embedText = "<a href=\"http://" + Request.Url.Host + GetAssetDetailsLink (assetId) + "\" target=\"resource\" border=\"0\"><img src='" + GetPhotoImageURL (drAsset ["image_full_path"].ToString (), "la") + "'/></a>";
				divEmbedded.InnerText = "Photo Embed URL:";

				divRawEmbed.InnerText = GetPhotoImageURL (drAsset ["image_full_path"].ToString (), "la");
				divPhotoURL.Visible = true;
			}
			else if (StoreUtility.IsYouTubeContent (drAsset))
			{                 
				height = 356;
				width = 450;

				// Update the media views
                mediaFacade.UpdateMediaViews (assetId, Common.GetVisitorIPAddress(), GetUserId ());

                //autostart
                m_embedText = StoreUtility.GetYouTubeEmbedLink(drAsset["asset_offsite_id"].ToString(), height, width, true, autoPlay);
                litStream.Text = StoreUtility.GetYouTubeEmbedLink(drAsset["asset_offsite_id"].ToString(), height, width, false, autoPlay);
				phStream.Controls.Add (litStream);
			}
			else if (StoreUtility.IsStreamingTVContent (drAsset))
			{                 
				// Update the media views
                mediaFacade.UpdateMediaViews (assetId, Common.GetVisitorIPAddress(), GetUserId ());

				if (StoreUtility.IsAudioContent (drAsset))
				{
					dimensions = "height=\"100\" width=\"" + width.ToString () + "\"";
				}

				string tvURL = drAsset["asset_offsite_id"].ToString();

				// Use Media player
				m_embedText = litStream.Text = "<OBJECT id=\"mpStream\"" + dimensions +
					" CLASSID=\"CLSID:6BF52A52-394A-11d3-B153-00C04F79FAA6\"" +
					" standby='Loading Microsoft Windows Media Player components...'" +
					" type=\"application/x-oleobject\">" +
					" <PARAM NAME=\"URL\" VALUE=\"" + tvURL + "\">" +
					" <PARAM NAME=\"ShowStatusBar\" VALUE=\"True\">" +
					" <PARAM NAME=\"EnableContextMenu\" VALUE=\"False\">" +
                    " <PARAM NAME=\"AutoStart\" VALUE=\"" + autoPlay + "\">" +
					" <PARAM name=\"PlayCount\" value=\"1\">" +
					"<embed " + dimensions + " type=\"application/x-mplayer2\" pluginspage=\"http://www.microsoft.com/Windows/MediaPlayer/\"" +
					" name=\"mediaplayer1\" showstatusbar=\"1\" EnableContextMenu=\"false\" autostart=\"" + autoPlay + "\" transparentstart=\"1\" loop=\"0\" controller=\"true\" src=\"" + tvURL + "\"></embed>" +
					"</OBJECT>";
			}
			else
			{
				// Don't do this for OMM, OMM takes care of that
				// Update the media views
                mediaFacade.UpdateMediaViews(assetId, Common.GetVisitorIPAddress(), GetUserId());

				if (StoreUtility.IsAudioContent (drAsset))
				{
					dimensions = "height=\"100\" width=\"" + width.ToString () + "\"";
				}

				// Use Media player
				m_embedText = litStream.Text = "<OBJECT id=\"mpStream\"" + dimensions +
					" CLASSID=\"CLSID:6BF52A52-394A-11d3-B153-00C04F79FAA6\"" +
					" standby='Loading Microsoft Windows Media Player components...'" +
					" type=\"application/x-oleobject\">" +
					" <PARAM NAME=\"URL\" VALUE=\"" + contentURL + "\">" +
					" <PARAM NAME=\"ShowStatusBar\" VALUE=\"True\">" +
					" <PARAM NAME=\"EnableContextMenu\" VALUE=\"False\">" +
                    " <PARAM NAME=\"AutoStart\" VALUE=\"" + autoPlay + "\">" +
					" <PARAM name=\"PlayCount\" value=\"1\">" +
					"<embed " + dimensions + " type=\"application/x-mplayer2\" pluginspage=\"http://www.microsoft.com/Windows/MediaPlayer/\"" +
					" name=\"mediaplayer1\" showstatusbar=\"1\" EnableContextMenu=\"false\" autostart=\"" + autoPlay + "\" transparentstart=\"1\" loop=\"0\" controller=\"true\" src=\"" + contentURL + "\"></embed>" +
					"</OBJECT>";
			}
		   
			phStream.Controls.Add (litStream);	
		}

		/// <summary>
		/// Returns the number of channels this asset belongs to
		/// </summary>
		private string GetChannelCount (int numberOfChannels)
		{
			return numberOfChannels.ToString () + " channel" + (numberOfChannels == 1 ? string.Empty : "s");
		}

		/// <summary>
		/// BuildTopChannelList
		/// </summary>
		private void BuildTopChannelList (DataRow drAsset, int ownerUserId)
		{
			PagedDataTable pdtChannels = StoreUtility.GetChannelsSharingAsset (Convert.ToInt32 (drAsset["asset_id"]), KanevaWebGlobals.CurrentUser.HasAccessPass, false, "thumbnail_small_path <> '' ", 1, 33);

			string strChannels = "";
			int cnt = 0;

			for (int i = 0; i < pdtChannels.Rows.Count; i ++)
			{
				if (!pdtChannels.Rows [i]["community_id"].Equals (DBNull.Value) && !pdtChannels.Rows [i]["name"].Equals (DBNull.Value))
				{
					if ( (pdtChannels.Rows [i]["is_adult"]).ToString () == "Y" )
					{
						strChannels += "<span class=\"restricted\"></span>";
					}
					
					if ( Convert.ToBoolean (pdtChannels.Rows [i]["is_personal"]) )
					{
						if (! Convert.ToInt32 (pdtChannels.Rows [i]["creator_id"]).Equals (ownerUserId))
						{
							strChannels += "<a href=\"" + GetPersonalChannelUrl (pdtChannels.Rows [i]["name_no_spaces"].ToString ()) + 
								"\" title=\"" + pdtChannels.Rows [i]["name"].ToString () + "\"><img src=\"" +
                                GetProfileImageURL (pdtChannels.Rows[i]["thumbnail_small_path"].ToString (), "sq", "M",
                                Convert.ToBoolean (pdtChannels.Rows[i]["use_facebook_profile_picture"]), Convert.ToUInt64 (pdtChannels.Rows[i]["fb_user_id"])) + 
								"\" border=\"0\" width=\"30\" height=\"23\" style=\"margin:1px;\" /></a>";
						
							cnt++;
						}
					}		  
					else
					{
						strChannels += "<a href=\"" + GetBroadcastChannelUrl (pdtChannels.Rows [i]["name_no_spaces"].ToString ()) + "\" title=\"" + pdtChannels.Rows [i]["name"].ToString () + "\"><img src=\"" + GetBroadcastChannelImageURL (pdtChannels.Rows [i]["thumbnail_small_path"].ToString (), "sm") + "\" border=\"0\" width=\"30\" height=\"23\" style=\"margin:1px;\" /></a>";
						cnt++;
					}
				}
			}
			
			spnAdders.InnerHtml = strChannels;
			int tot_count = cnt < 34 ? cnt : pdtChannels.TotalCount - 1;	 //subtract 1 for the owner

			lblAddedBy.Text = tot_count.ToString () + (tot_count == 1 ? " Member/Community has " : " Members/Communities have ");
			
			//** This is temporary.  Need to remove when fix but to number_added_channels count
			// in assets_stats table and trigger
			lblNumTimesAdded.Text = tot_count.ToString () + " time" + (tot_count == 1 ? "" : "s");
		}
        #endregion

		#region Properties

        public int AssetOwnerId
        {
            get { return _assetOwnerId; }
            set { _assetOwnerId = value; }
        }

        private int pmToUserId
        {
            get
            {
                if (ViewState["pmToUserId"] == null)
                {
                    ViewState["pmToUserId"] = 0;
                }
                return (int) ViewState["pmToUserId"];
            }
            set
            {
                ViewState["pmToUserId"] = value;
            }
        }

        #endregion

		#region Declerations

		protected Button		JoinChannelThread, btnAction;
		protected ImageButton	btnAvatar;
		protected LinkButton	lbCat1, lbCat2, lbCat3;
        protected LinkButton lbRave, lbReport;
        protected HtmlGenericControl ifrm;

        protected Label lblName, lblCompanyName;
		protected Label		lblCreatedDate;
		protected Label		lblDescription, lblBodyText, lblLicense, lblLicenseURL;
		protected Label		lblRelatedByTopic, lblChannels;
		protected Label		lblRuntime, lblNumViews, lblNumTimesAdded, lblNumComments;
		protected Label		lblRaves, lblAddedBy;

		protected HyperLink		hlTag, hlScreenShots, hlFeature;
        protected HtmlContainerControl divShare, divEmbed, divRawEmbed, divInstructionText; 
		protected Repeater		rptAttributes;
		protected PlaceHolder	phStream, phChannels;
		protected Literal		litStream = new Literal ();
		protected Literal		litAdTest;
        protected Literal litAssetId, litCtrlRaveCount;
		
		protected HtmlImage		imgAvatar, imgRestricted, imgAssetThumbnail;
		protected HtmlAnchor	aUserName, aUserName3;
        protected HtmlTableRow  trDetailDesc, trDescription;
        protected HtmlContainerControl pActionText, pPolicyText;

        protected HtmlContainerControl divNewWindow, divInstructions, divPhotoURL;

		protected HtmlGenericControl	divEmbedded, divRestricted, divOwnerRestricted;
		protected HtmlContainerControl	spnRuntime, spnAdders, spnRelatedType;
		protected HtmlContainerControl	h1AssetTitle;
		protected HtmlContainerControl	h2Title, divAccessMessage, divDetails;
        protected HtmlContainerControl rave;
        protected HtmlContainerControl add;
        protected HtmlContainerControl h2UpsellPromise, divUpsellImage, divUpsellImageWrapper;
        protected HtmlAnchor aJoinNow, aItsFree, aJoinToday;

		// user controls
		protected RelatedItemsView	ucRelatedItems;
		protected Comments			ucComments;

		protected string m_embedText = string.Empty;
		private	string m_tag = string.Empty;
		private int	_assetOwnerId = 0;

		private const string COMMUNITY_GUIDELINES = "<br/><br/><u><b>Kaneva Community Guidelines</b></u><br/>" +
			"Kaneva defines restricted content as anything that contains strong language, or depictions of nudity, violence " +
			"or substance abuse.  When you mark content as Restricted, you protect our visitors -- and give youself greater " +
			"freedom of expression.  Only Kaneva members 18 years of age or older may post or view Restricted content.";

		private const string PRIVACY_SETTINGS = "<br/><br/><u><b>Privacy Settings for Kaneva Members</b></u><br/>" +
			"Kaneva members have the option to set their profiles and communities to private, only firends within their private " +
			"network are allowed to view detailed information such as personal interests and friends. These settings are " +
			"part of a broader effort to Kaneva to help protect the privacy of our community members and provide a safe " +
			"environment for all members to connect online.";

        // Regular expression to match href attributes of anchor tags.
        private const string hrefRegex = "href=\"[^\"]*\"";

		// Logger
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);


        // Share Media 
        protected TextBox txtBlast, shareLink;
        protected HtmlContainerControl spnBlastAlertMsg, spnPMAlertMsg;
        protected TextBox txtPrivateMsgTo, txtPrivateMsgSubject, txtPrivateMsgBody, txtBlastTitle;
        protected LinkButton lbFullScreen, lb_link_1;
       
		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
