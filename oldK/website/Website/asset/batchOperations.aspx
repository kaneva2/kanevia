<%@ Page language="c#" Codebehind="batchOperations.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.batchOperations" %>
<%@ Register TagPrefix="Kaneva" TagName="HeaderText" Src="../usercontrols/HeaderText.ascx" %>

<link href="../css/home.css" rel="stylesheet" type="text/css" />		
<link href="../css/kanevaSC.css" rel="stylesheet" type="text/css"/>
<link href="../css/friends.css" rel="stylesheet" type="text/css"/>
<link href="../css/kanevaText.css" type="text/css" rel="stylesheet">
<link href="../css/kanevaBroadBand.css" rel="stylesheet" type="text/css" />
<link href="../css/new.css" rel="stylesheet" type="text/css" />

<script language="javascript" src="../jscript/rollover.js"></script>
<script language="javascript" src="../jscript/prototype.js"></script>

<script language="Javascript" type="text/javascript">
<!--
var selectedCategories = new Array()
function categorySelected(obj)
{  
  if(obj.checked)
  {    
    //a new checkbox is selected, pop the first selection if 3 are already selected
    if(selectedCategories.length == 3)
    {
      firstEle = selectedCategories.shift();
      firstEle.checked = false;
    }
    selectedCategories.push(obj);  
  }else
  {
    var index = -1;
    //remove it from the array if it exists
    for(var i = 0; i < selectedCategories.length; i++)
    {
      if(selectedCategories[i] == obj)
      {
        index = i;
        break;          
      }
    }
    if(index >= 0)
    {
      //and rearrange the list
      for(var i = index; i < selectedCategories.length -1; i++)
      {
        selectedCategories[i] = selectedCategories[i+1];
      }
      selectedCategories.pop();
    }
  }
}
function showAdvanced()
{
	$('tblMoreOptions').style.display="";
	$('advancedLink').style.display="none"; 
	$('lessLink').style.display="";
}
function showLess()
{
	$('tblMoreOptions').style.display="none";
	$('advancedLink').style.display=""; 
	$('lessLink').style.display="none";	
}
//--></script>

<div ID="divLoading">
	<table BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="100%" HEIGHT="500px">
		<tr><th CLASS="loadingText" ID="divLoadingText">Loading...</th></tr>
	</table>
</div>	
<div id="divData" style="DISPLAY:none" class="container">
	
	<table cellpadding="0" cellspacing="0" border="0" width="100%">
		<tr>
			<td align="center">
				<asp:ValidationSummary ShowMessageBox="False" ShowSummary="True" Class="formError" id="valSum" DisplayMode="BulletList" runat="server" HeaderText="Please correct the following errors:"/><br>
			</td>
		</tr>
	</table>

	<table width="990" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td><img runat="server" src="~/images/spacer.gif" width="1" height="14"></td>
		</tr>
		<tr>
			<td>
				<table  border="0" cellpadding="0" cellspacing="0" width="100%">
					<tr>
						<td width="6" class="frTopLeft"></td>
						<td class="frTop"></td>
						<td width="7" class="frTopRight"></td>
					</tr>
					<tr> 
						<td class="frBorderLeft">
							<img runat="server" src="~/images/spacer.gif" width="1" height="1">
						</td>
						<td valign="top" class="frBgIntMembers">
							<table width="100%"  border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td>
										<!-- TOP STATUS BAR -->
										<div id="pageheader">
											<table cellpadding="0" cellspacing="0" border="0" width="99%">
												<tr>
													<td nowrap align="left">
														<table cellpadding="0" cellspacing="0" border="0">
															<tr>
																<td align="left" nowrap>
																	<h1>Batch-Edit Media</h1>
																</td>
																<td width="10px">&nbsp;</td>
																<td nowrap>
																	<asp:hyperlink title="return to Previous location" runat="server" ID="hlShowMedia" Text="<< Back" CausesValidation="False"/>
																</td>
															</tr>
														</table>
													</td>
													
													<td align="right" valign="middle">
														<table cellpadding="0" cellspacing="0" border="0" width="690">
															<tr>
																<td class="headertout" width="175"></td>
																<td class="searchnav" width="260"></td>
																<td align="left" width="130"></td>
																<td class="searchnav" align="right" width="210" nowrap>
																</td>
															</tr>
														</table>
													</td>
												</tr>
											</table>
										</div>
										<!-- END TOP STATUS BAR -->	
									
									</td>
								</tr>
								<tr>
									<td width="968">
									
									<div class="module whitebg">
										<span class="ct"><span class="cl"></span></span>
										
										<h2>Apply to all selected items:</h2>
									
										<br>
									
										<table width="95%" border="0" cellspacing="0" cellpadding="0">
											
											<tr>
												<td width="5" height="5"><img runat="server" src="~/images/spacer.gif" width="5" height="5" /></td>
												<td>
													<table width="98%" border="0" cellspacing="0" cellpadding="0" >
												
														<!--DWLayoutTable-->
														<tr>
															<td height="18" width="178" align="right" nowrap="nowrap" class="insideBoxText11">Type:</td>
															<td width="20"><img runat="server" src="~/images/spacer.gif" height="6" ID="Img2"/></td>
															<td align="left" class="textGeneralGray01" width="100%">
																<asp:DropDownList class="formKanevaText" id="drpType" OnSelectedIndexChanged="drpType_Change" AutoPostBack="true" style="width:220px" runat="Server"/>
																<asp:Label id="notice" runat="server" />
															</td>
														</tr>
														<tr>
															<td height="6" colspan="3"><img runat="server" src="~/images/spacer.gif" width="1" height="6" ID="Img3"/></td>
														</tr>
														<tr>
															<td height="18" width="178" align="right" nowrap="nowrap" class="insideBoxText11">Access:</td>
															<td width="20"><img runat="server" src="~/images/spacer.gif" width="22" height="6" /></td>
															<td align="left" class="textGeneralGray01" width="100%">
																<asp:DropDownList class="formKanevaText" id="drpPermission" style="width:130px" runat="Server"/>&nbsp;
																<asp:RequiredFieldValidator ID="rfdrpPermission" ControlToValidate="drpPermission" Text="*" ErrorMessage="Permissions is a required field." Display="Dynamic" runat="server"/>
															</td>
														</tr>
														<tr>
															<td height="6" colspan="3"><img runat="server" src="~/images/spacer.gif" width="1" height="6" /></td>
														</tr>
														<tr>
															<td height="24" width="178" align="right" nowrap="nowrap" class="insideBoxText11">Restricted content:</td>
															<td width="20"><img runat="server" src="~/images/spacer.gif" width="6" height="6" /></td>
															<td valign="middle" align="left" class="blueinfo" width="100%"><asp:CheckBox id="chkMature" runat="server"/><img runat="server" src="~/images/spacer.gif" width="5" height="6" />You should set this option if this item may not be appropriate for Kaneva members who are under the age of 18</td>
														</tr>
														<tr>
															<td height="6" colspan="3"><img runat="server" src="~/images/spacer.gif" width="1" height="6" /></td>
														</tr>
														<!--<tr>
															<td ><img runat="server" src="~/images/spacer.gif" width="6" height="6" /></td>
															<td    align="right"  class="insideBoxText11">Category:</td>	
															<td ><img runat="server" src="~/images/spacer.gif" width="6" height="6" /></td>
															<td colspan="5" align="left">
										  						<asp:DataList runat="server" EnableViewState="True" ShowFooter="False" Width="100%" id="dlCategories"
																	cellpadding="2" cellspacing="2" border="0" RepeatColumns="4" RepeatDirection="Horizontal">
																	<ItemStyle CssClass="insideBoxText11"/>
																	<ItemTemplate>
																		<asp:CheckBox id="chkCategory" runat="server" onclick="javascript:categorySelected(this)"/><%# DataBinder.Eval (Container.DataItem, "name")%>
																		<input type="hidden" runat="server" id="hidCatId" value='<%#DataBinder.Eval(Container.DataItem, "category_id")%>' NAME="hidCatId"> 
																	</ItemTemplate>
																</asp:DataList>															
															</td>
														</tr>
														<tr>
															<td height="6" colspan="8"><img runat="server" src="~/images/spacer.gif" width="1" height="6" ID="Img1"/></td>
														</tr>
														<tr>
															<td height="13" colspan="2" align="right" nowrap="nowrap" class="insideBoxText11"></td>
															<td><img runat="server" src="~/images/spacer.gif" width="6" height="6" ID="Img4"/></td>
															<td colspan="5" align="left" class="textGeneralGray03">You may choose up to 3 categories</td>
														</tr>-->
														<tr>
															<td height="6" colspan="3"><img runat="server" src="~/images/spacer.gif" width="1" height="6" ID="Img5"/></td>
														</tr>
														
														
														
														
																								  
														<tr>
															<td colspan="3" align="left">
															    <a href="#" onclick="showAdvanced()" id="advancedLink" style="font-size: 12px;">Show more options</a> 
                                                                <a href="#" onclick="showLess()" id="lessLink" style="display:none;font-size: 12px;">Show less options</a>
                                                                <br />
                                                                <br />	
															</td>
														</tr>										  
                                                        <tr>
                                                            <td align="left" colspan="3">
                                                                <table cellSpacing="0" cellPadding="0" border="0" id="tblMoreOptions" style="display:none;">
														            <tr>
															            <td width="44" height="53" align="right" nowrap="nowrap" class="insideBoxText11"><input type="radio" id="optPersonal" name="optLicense" value="P" runat="server"/></td>
															            <td width="133" align="right" nowrap="nowrap" class="insideBoxText11">Copyright - <br/>
															            Personal Usage Only:</td>
															            <td><img runat="server" src="~/images/spacer.gif" width="20" height="6" ID="Img6"/></td>
															            <td align="left" colspan="4">
																            <asp:TextBox ID="txtPerAdditional" class="formKanevaText" style="width:500px" TextMode="multiline" Rows="4" runat="server"/>
															            </td>
														            </tr>
														            <tr>
															            <td height="6" colspan="6"><img runat="server" src="~/images/spacer.gif" width="1" height="6" ID="Img7"/></td>
														            </tr>
														            <tr>
															            <td height="53" align="right" nowrap="nowrap" class="insideBoxText11"><input type="radio" id="optCommercial" name="optLicense" value="I" runat="server"/></td>
															            <td align="right" nowrap="nowrap" class="insideBoxText11">Copyright -&nbsp;<br />
																            Commercial Use:&nbsp;</td>
															            <td><img runat="server" src="~/images/spacer.gif" width="20" height="6" ID="Img8"/></td>
															            <td align="left" class="textGeneralGray03" colspan="4">
																            <asp:TextBox ID="txtCommAdditional" class="formKanevaText" style="width:500px" TextMode="multiline" Rows="4" runat="server"/>
															            </td>
														            </tr>
														            <tr>
															            <td height="6" colspan="6"><img runat="server" src="~/images/spacer.gif" width="1" height="6" ID="Img9"/></td>
														            </tr>												
														            <tr>
															            <td height="22" align="right" nowrap="nowrap" class="insideBoxText11">
																            <input type="radio" id="optCC" name="optLicense" value="C" runat="server"/>
															            </td>
															            <td align="right" class="insideBoxText11"><a target="_resource" runat="server" href="http://creativecommons.org" ID="A2">CreativeCommons</a></td>
															            <td><img runat="server" src="~/images/spacer.gif" width="20" height="6" ID="Img10"/></td>
															            <td align="left" valign="middle">
																            <asp:DropDownList CssClass="formKanevaText" id="drpCCLicense" runat="Server" Width="250px">
																	            <asp:ListItem  value="http://creativecommons.org/licenses/by/2.5/">Attribution</asp:ListItem >
																	            <asp:ListItem  value="http://creativecommons.org/licenses/by-nd/2.5/">Attribution-NoDerivs</asp:ListItem >
																	            <asp:ListItem  value="http://creativecommons.org/licenses/by-nc-nd/2.5/">Attribution-NonCommercial-NoDerivs</asp:ListItem >
																	            <asp:ListItem  value="http://creativecommons.org/licenses/by-nc/2.5/">Attribution-NonCommercial</asp:ListItem >
																	            <asp:ListItem  value="http://creativecommons.org/licenses/by-nc-sa/2.5/">Attribution-NonCommercial-ShareAlike</asp:ListItem >
																	            <asp:ListItem  value="http://creativecommons.org/licenses/by-sa/2.5/">Attribution-ShareAlike</asp:ListItem >
																            </asp:DropDownList>&nbsp;
															            </td>
															            <td valign="top"><!--DWLayoutEmptyCell-->&nbsp;</td>
															            <td width="483" align="left" valign="middle" class="insideBoxText11" ><a target="_resource" runat="server" href="http://creativecommons.org/license/?partner=kaneva&exit_url=http://www.kaneva.com&partner_icon_url=http://www.kaneva.com/images/klogo.gif" ID="A4">Selection Wizard...</a>&nbsp;<a target="_resource" runat="server" href="http://creativecommons.org/licenses/" ID="A3">What do these mean?</a> </td>
														            </tr>
														            <tr>
															            <td height="6" colspan="6"><img runat="server" src="~/images/spacer.gif" width="1" height="6" ID="Img11"/></td>
														            </tr>
														            <tr>
															            <td height="22" align="right" nowrap="nowrap" class="insideBoxText11"><input type="radio" id="optPublic" name="optLicense" value="D" runat="server"/></td>
															            <td height="22" align="right" nowrap="nowrap" class="insideBoxText11">Public Domain:</td>
															            <td><img runat="server" src="~/images/spacer.gif" width="20" height="6" ID="Img12"/></td>
															            <td align="left" class="textGeneralGray03" colspan="3">
																            <asp:TextBox ID="txtPD" class="formKanevaText" style="width:500px" MaxLength="50" runat="server"/>
															            </td>
														            </tr>
														            <tr>
															            <td height="6" colspan="6"><img runat="server" src="~/images/spacer.gif" width="1" height="6" ID="Img13"/></td>
														            </tr>												
														            <tr><td height="24" align="right" nowrap="nowrap" class="insideBoxText11"><input type="radio" id="optOther" name="optLicense" value="O" runat="server"/></td>
															            <td height="24" align="right" nowrap="nowrap" class="insideBoxText11">Other:</td>
															            <td><img runat="server" src="~/images/spacer.gif" width="20" height="6" ID="Img14"/></td>
															            <td align="left" valign="top" colspan="3"><span class="textGeneralGray03">
																            <asp:TextBox ID="txtLicenseName" class="formKanevaText" style="width:500px" MaxLength="50" runat="server"/>
															            </span></td>
														            </tr>
														            <tr>
															            <td height="6" colspan="6"><img runat="server" src="~/images/spacer.gif" width="1" height="6" ID="Img15"/></td>
														            </tr>
														            <tr>
															            <td height="22" colspan="2" align="right" nowrap class="insideBoxText11"> More details http://</td>
															            <td><img runat="server" src="~/images/spacer.gif" width="20" height="6" ID="Img16"/></td>
															            <td align="left" class="textGeneralGray03" colspan="3">
																            <asp:TextBox ID="txtLicenseURL" class="formKanevaText" style="width:500px" MaxLength="100" runat="server"/>
															            </td>
														            </tr>	
														            <tr>
															            <td class="underline" colspan="6"><img runat="server" src="~/images/spacer.gif" width="1" height="15" ID="Img17"/></td>
														            </tr>																				  
														            <tr>
															            <td height="6" colspan="6"><img runat="server" src="~/images/spacer.gif" width="1" height="10" ID="Img18"/></td>
														            </tr>
														          </table>
														        </td>
														     </tr>
		
													<tr>
														<td colspan="3" width="100%" align="right">
															<table border="0" align="right">
																<tr>
																	<td nowrap>
																		<asp:Label runat="server" class="note" ID="Label1" NAME="Label1">Note: Only media you own will be affected</asp:Label>													
																	</td>
																	<td>
																		<asp:ImageButton id="imgUpdate" runat="server" ImageUrl="~/images/buttons/btnApplySelected.gif" AlternateText="Update Selected" OnClick="imgUpdate_Click" border="0"/>
																	</td>
																</tr>
															</table>
														</td>
													</tr>										  									
													<tr>
														<td height="4" colspan="3" nowrap class="textGeneralGray01"><img runat="server" src="~/images/spacer.gif" width="1" height="4" /></td>
													</tr>
															  
												</table>
												</td>
											</tr>
										</table>
	                                
									<span class="cb"><span class="cl"></span></span>
									</div>
									
									</td>
								</tr>
									
							</table>

						</div>
								
						</td>
						<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
					</tr>
					<tr>
						<td class="frBottomLeft"></td>
						<td class="frBottom"></td>
						<td class="frBottomRight"></td>
					</tr>
					<tr>
						<td colspan="3"><img runat="server" src="~/images/spacer.gif" width="1" height="14" /></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	</div>



