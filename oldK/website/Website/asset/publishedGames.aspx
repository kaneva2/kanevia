<%@ Page language="c#" Codebehind="publishedGames.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.publishedGames" %>
<%@ Register TagPrefix="Kaneva" TagName="PublishedItemsFilter" Src="../usercontrols/PublishedItemsFilter.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../usercontrols/Pager.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="HeaderText" Src="../usercontrols/HeaderText.ascx" %>

<Kaneva:HeaderText runat="server" Text="My Published Games"/>

<input type="hidden" runat="server" id="AssetId" value="0"> 
<asp:button ID="DeleteAsset" OnClick="Delete_Asset" runat="server" Visible="false"></asp:button>

<center>
<table cellpadding="2" cellspacing="0" border="0" width="730" ID="Table1">
	<tr>
		<td width="200" >&nbsp;&nbsp;<asp:imagebutton runat="server" ID="btnAddGame" ImageUrl="~/images/button_addGame.gif" class="Filter2" alt="Add New Game" OnClick="btnAddAsset_Click" style="vertical-align: middle;"/></td>
		<td align="right" class="belowFilter2" valign="middle" width="500"><br><asp:Label runat="server" id="lblSearch" CssClass="assetCommunity"/><br><Kaneva:Pager runat="server" id="pgTop"/></td>			
	</tr>
</table>
</center>
<Kaneva:PublishedItemsFilter runat="server" id="filStore"/>

<table cellpadding="0" cellspacing="0" border="0" width="750">
	<tr>
		<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="5" border="0"/></td>
		<td colspan="9" class="FullBorders">
		
		
		
		<asp:DataGrid EnableViewState="False" runat="server" ShowFooter="False" Width="100%" id="dgrdAsset" cellpadding="0" cellspacing="0" border="0" AutoGenerateColumns="False" AllowSorting="True" style="border: 0px hidden white;">  
				<HeaderStyle CssClass="lineItemColHead"/>
				<ItemStyle CssClass="lineItemEven"/>
				<AlternatingItemStyle CssClass="lineItemOdd"/>
				<Columns>
				
					<asp:TemplateColumn HeaderText="" ItemStyle-Width="30%" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="top">
						<ItemTemplate>
							<img border="0" style="margin: 10px;" src='<%#GetMediaImageURL (DataBinder.Eval(Container.DataItem, "thumbnail_medium_path").ToString () ,"me", Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_id")), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_type_id")), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "thumbnail_gen")))%>' border="0"/>
							<br><asp:hyperlink id="Hyperlink1" runat="server" CssClass="adminLinks" NavigateURL='<%#GetAssetEditLink (Convert.ToInt32 ( DataBinder.Eval(Container.DataItem, "asset_id")))%>' ToolTip="Edit" Visible='<%#!IsDeleted (Convert.ToInt32 ( DataBinder.Eval(Container.DataItem, "status_id")))%>'>edit</asp:hyperlink> <asp:hyperlink id="Hyperlink2" CssClass="adminLinks" runat="server" NavigateURL='<%#GetDeleteScript (Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_id"))) %>' ToolTip="Delete" Visible='<%#!IsDeleted (Convert.ToInt32 ( DataBinder.Eval(Container.DataItem, "status_id")))%>'>delete</asp:hyperlink>
							<br><a runat="server" class="adminLinks" href='<%# GetLauncherLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "asset_id")))%>' Visible='<%#!IsDeleted (Convert.ToInt32 ( DataBinder.Eval(Container.DataItem, "status_id")))%>'>download launcher</a>
						
						
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="item name" SortExpression="current_name" ItemStyle-Width="55%" ItemStyle-VerticalAlign="top">
						<ItemTemplate>
							<br>
							<span class="assetLink"><%# TruncateWithEllipsis (Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "current_name").ToString ()), 40) %></span>
							<span style="line-height: 7px; "><br><br></span>
							
							<span class="assetStoreStats">type:</span> <%# GetAssetTypeName(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "asset_type_id")))%><span style="line-height: 7px;"><br></span>	&nbsp;&nbsp;<span class="assetStoreStats">owner:</span>&nbsp;&nbsp;<a class="dateStamp" href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "username").ToString ())%>><%# DataBinder.Eval(Container.DataItem, "username") %></a>&nbsp;&nbsp;<span class="assetStoreStats">size:</span> <font color="green"> <%# FormatImageSize (DataBinder.Eval (Container.DataItem, "file_size")) %></font><br><span class="assetStoreStats">Added on:</span><span class="adminLinks">  <%# FormatDateTime (DataBinder.Eval (Container.DataItem, "created_date")) %></span>
							<span style="line-height: 7px;"><br><br></span>
							
							<%# TruncateWithEllipsis (DataBinder.Eval (Container.DataItem, "teaser").ToString (), 200) %>
							<span style="line-height: 7px;"><br><br></span>
							
							<a href='<%# GetAssetDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "asset_id")))%>' Visible='<%#!IsDeleted (Convert.ToInt32 ( DataBinder.Eval(Container.DataItem, "status_id")))%>'><img runat="server" src="~/images/button_details.gif" border="0" style="vertical-align: middle" /></a>
							<span style="line-height: 7px;"><br><br></span>
							
		
							<BR>
							
							status: <A runat="server" HREF='#' onclick='<%# GetStatusLink (Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "status_id")))%>'><%# GetStatusText (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "status_id"))) %></a>
							<br><br> 
					
						</ItemTemplate>
					</asp:TemplateColumn>					
					<asp:TemplateColumn HeaderText="price" SortExpression="a.amount" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Center">
						<ItemTemplate>
							<span style="COLOR: #459113; font-size: 12px; font-weight: bold;"><%# FormatKpoints (DataBinder.Eval (Container.DataItem, "amount"))%></span>
						</ItemTemplate>
					</asp:TemplateColumn>	
				</Columns>
			</asp:datagrid>
		
			<asp:DataGrid EnableViewState="False" runat="server" ShowFooter="False" Width="100%" id="listAsset" cellpadding="0" cellspacing="0" border="0" AutoGenerateColumns="False" AllowSorting="True" style="border: 0px hidden white;">  
				<HeaderStyle CssClass="lineItemColHead"/>
				<ItemStyle CssClass="lineItemEven"/>
				<AlternatingItemStyle CssClass="lineItemOdd"/>
				<Columns>
							
					<asp:TemplateColumn HeaderText="" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="top">
						<ItemTemplate><img border="0" style="margin: 10px;" src='<%#GetMediaImageURL (DataBinder.Eval(Container.DataItem, "thumbnail_medium_path").ToString () ,"me", Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_id")), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_type_id")), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "thumbnail_gen")))%>' border="0"/>
							<br><asp:hyperlink id="hlEdit" runat="server" CssClass="adminLinks" NavigateURL='<%#GetAssetEditLink (Convert.ToInt32 ( DataBinder.Eval(Container.DataItem, "asset_id")))%>' ToolTip="Edit" Visible='<%#!IsDeleted (Convert.ToInt32 ( DataBinder.Eval(Container.DataItem, "status_id")))%>'>edit</asp:hyperlink> <asp:hyperlink id="hlDeleteTest" CssClass="adminLinks" runat="server" NavigateURL='<%#GetDeleteScript (Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_id"))) %>' ToolTip="Delete" Visible='<%#!IsDeleted (Convert.ToInt32 ( DataBinder.Eval(Container.DataItem, "status_id")))%>'>delete</asp:hyperlink>
							<br><a runat="server" class="adminLinks" href='<%# GetLauncherLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "asset_id")))%>' Visible='<%#!IsDeleted (Convert.ToInt32 ( DataBinder.Eval(Container.DataItem, "status_id")))%>'>download launcher</a><br><br>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="item name" SortExpression="current_name" ItemStyle-Width="20%" ItemStyle-VerticalAlign="middle">
						<ItemTemplate>
							<span class="assetLink"><%# TruncateWithEllipsis (Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "current_name").ToString ()), 40) %></span><br>	
	
						</ItemTemplate>
					</asp:TemplateColumn>
					
					<asp:TemplateColumn HeaderText="type" SortExpression="asset_type_id" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center">
						<ItemTemplate>						
							<%# GetAssetTypeName(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "asset_type_id")))%>					
						</ItemTemplate>
					</asp:TemplateColumn>	
					<asp:TemplateColumn HeaderText="size" SortExpression="file_size" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center">
						<ItemTemplate>						
							<font color="green"> <%# FormatImageSize (DataBinder.Eval (Container.DataItem, "file_size")) %></font>				
						</ItemTemplate>
					</asp:TemplateColumn>	
					
					<asp:TemplateColumn HeaderText="published" SortExpression="created_date" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center">
						<ItemTemplate>						
							<span class="adminLinks"><%# FormatDateTime (DataBinder.Eval (Container.DataItem, "created_date")) %></span>						
						</ItemTemplate>
					</asp:TemplateColumn>						
					<asp:TemplateColumn HeaderText="status" ItemStyle-Width="25%" ItemStyle-HorizontalAlign="Center">
						<ItemTemplate>
							status:<br><A runat="server" style="color: #4863a2; font-size: 11px;" HREF='#' onclick='<%# GetStatusLink (Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "status_id")))%>'><%# GetStatusText (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "status_id"))) %></a>
						</ItemTemplate>
					</asp:TemplateColumn>	
				</Columns>
			</asp:datagrid>
					
		</td>		
		<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="5" border="0" ID="Img1"/></td>
	</tr>
</table>
<br><br>