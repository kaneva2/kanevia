<%@ Page language="c#" MasterPageFile="~/masterpages/GenericLandingPageTemplate.Master" Codebehind="notAvailable.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.notAvailable" %>

<asp:Content ID="cntCommunityMetaData" runat="server" contentplaceholderid="cphHeader" >
    <NOSCRIPT><META HTTP-EQUIV="refresh" CONTENT="1" URL="noJavascript.aspx" /></NOSCRIPT>
    <link href="../css/new.css?v1" type="text/css" rel="stylesheet">
    <link href="../css/base/base_new1.css" type="text/css" rel="stylesheet" />
    <link href="../css/base/buttons_new.css?v1" type="text/css" rel="stylesheet">
    <style>
        .pageBody {padding:0 0 20px 0;background:#fff;border:1px solid #ccc;border-top:none;}
        .datacontainer {width:700px;margin:0 auto;padding-top:50px;}
        p {margin:20px 0 0 0;line-height:16px;display:block;}
        h6 {font-size:22px;font-family:verdana, arial, sans-serif;padding:0;margin:0;font-weight:bold;}
        .btnmedium {margin:40px auto;}
        a:link {font-size:12px;}
        ul {margin-bottom:40px;}
        li {margin:20px 0;}
    </style>
</asp:Content>


<asp:Content ID="cntKanevaCommunityBody" runat="server" contentplaceholderid="cphBody">
    <div class="datacontainer">
        <h6>Item Not Available</h6>
    	<p>You may not be able to access this item because one or more of the following conditions apply:</p>
	             
	    <ul>
		    <asp:Label id="lblAdult" runat="server">
			    <li> You�re accessing restricted content without first logging into Kaneva.<br />
			        <i> Use your browser�s Back button to go to the sign in page and sign in, or <a runat="server" id="joinLink" >join Kaneva now!</a></i></li>
		    </asp:Label>
		    <asp:Label id="lblAdultYoung" runat="server">
			    <li> You�re accessing restricted content and you are not of age.<br />
			        <i>There�s lots of other great things to browse at Kaneva, or wait until you�re old enough.</i></li>
		    </asp:Label>
		    <asp:Label id="lblPrivate" runat="server">
			    <li> This is a members-only community and you are not one of its members.<br />
			        <i> You must first request to join the community and get approval from the community owner or moderator.</i></li>
		    </asp:Label>
		    <asp:Label id="lblOther" runat="server">
			    <li> The item is discontinued or no longer active.<br />
			        <i>Check out the community for a possible update.</i></li>

			    <li> The web address (URL) to the item is not correct.<br>
			        <i>Verify that you are using the correct link, or directly go to the Kaneva community for help.</i></li>
		    </asp:Label>
	    </ul>
	
	    <p>NOTE: If you believe you are receiving this page in error, please contact support via the <a runat="server" href="~/suggestions.aspx?mode=F&category=Feedback" class="showingCount" ID="A1">Support Page</a></p>
	
		<asp:LinkButton class="btnmedium grey" id="btnOK" runat="server" text="Back" causesvalidation="false" onClick="btnOK_Click"></asp:LinkButton>
    </div>
</asp:Content>