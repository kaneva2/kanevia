///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva.channel;
using log4net;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for batchEdit.
	/// </summary>
	public class batchEdit : StoreBasePage
	{
		/// <summary>
		/// Constructor
		/// </summary>
		protected batchEdit () 
		{
			Title = "Batch Item Edit";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			// They have to at least be logged in to view this one
			if (Request.IsAuthenticated)
			{
				if(!GetRequestParams())
				{
					//invalid request params
					RedirectToHomePage ();
				}
			}
			else
			{
				Response.Redirect (this.GetLoginURL ());
			}

			// Set nav
            HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
//			HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.MEDIA_UPLOADS;
//			HeaderNav.MyMediaUploadsNav.ActiveTab = NavMediaUploads.TAB.LIBRARY;
//			HeaderNav.SetNavVisible(HeaderNav.MyMediaUploadsNav);
			HeaderNav.SetNavVisible(HeaderNav.MyKanevaNav,2);


			if (!IsPostBack)
			{
				hlShowMedia.NavigateUrl = ResolveUrl ("~/asset/publishedItemsNew.aspx?communityId=" + _channelId);
				DataTable dtAssets = StoreUtility.GetAssetsBatch (_assetIds, GetUserId ());

				rptAssets.DataSource = dtAssets;
				rptAssets.DataBind ();
			}

			// Bread crumb
			AddBreadCrumb (new BreadCrumb ("Batch Edit", GetCurrentURL (), "", 0));
		}



		private bool GetRequestParams()
		{
			bool retVal = false;
			try
			{
				if(Request ["communityId"] != null)
				{
					//get channelId and find the default page
					_channelId = Convert.ToInt32 (Request ["communityId"]);
				}
				else
				{
					_channelId = GetPersonalChannelId ();
				}

				_assetIds = Request ["assetIds"].ToString ();
				retVal = _assetIds.Length > 0;
			}
			catch(Exception){}
			
			return retVal;
		}

		/// <summary>
		/// Update Event Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnUpdate_Click (object sender, EventArgs e) 
		{
			// Server validation
			if (!Page.IsValid) 
			{
				return;
			}

			HtmlInputHidden hidAssetId;
			TextBox txtItemName;
			TextBox txtTeaser;
			TextBox txtTags;
			int assetId = 0;
			DataRow drAsset;

			int userId = GetUserId ();

			foreach (RepeaterItem rpiAsset in rptAssets.Items)
			{
				hidAssetId = (HtmlInputHidden) rpiAsset.FindControl ("hidAssetId");
				txtItemName = (TextBox) rpiAsset.FindControl ("txtItemName");
				txtTeaser = (TextBox) rpiAsset.FindControl ("txtTeaser");
				txtTags = (TextBox) rpiAsset.FindControl ("txtTags");
	
				assetId = Convert.ToInt32 (hidAssetId.Value);

				// Check for any inject scripts in text fields
				if ((KanevaWebGlobals.ContainsInjectScripts (txtItemName.Text)) || (KanevaWebGlobals.ContainsInjectScripts (txtTeaser.Text)) || (KanevaWebGlobals.ContainsInjectScripts (txtTags.Text)))
				{
					m_logger.Warn ("User " + KanevaWebGlobals.GetUserId () + " tried to script from IP " + Common.GetVisitorIPAddress());
					ShowErrorOnStartup ("Your input contains invalid scripting, please remove script code and try again.", true);
					return;
				}

				//setting amount to 0...all assets are free
				if (StoreUtility.UpdateAsset (userId, assetId, Server.HtmlEncode (txtItemName.Text), 
					Server.HtmlEncode (txtTeaser.Text), 0, (int) Constants.eASSET_STATUS.ACTIVE) == 1)
				{
					ShowErrorOnStartup ("A store item name \\'" + txtItemName.Text + "\\' already exists, please change the item name.");
					return;
				}

				// Update the tags
				drAsset = StoreUtility.GetAsset (assetId);
				StoreUtility.UpdateAssetTags (assetId, Server.HtmlEncode (txtTags.Text));
			}

			Response.Redirect (GetMyPublishedItemURL ());
		}

		protected HyperLink hlShowMedia;
		protected Repeater rptAssets;
		private string _assetIds;
		private int		_channelId;
		protected HtmlAnchor aBatchOperations;
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
