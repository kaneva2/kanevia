<%@ Page language="c#" Codebehind="addAssetToChannel.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.addAssetToChannel" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../usercontrols/Pager.ascx" %>

<link href="../css/home.css" rel="stylesheet" type="text/css" />		
<link href="../css/kanevaSC.css" rel="stylesheet" type="text/css"/>
<link href="../css/friends.css" rel="stylesheet" type="text/css"/>
<link href="../css/kanevaText.css" type="text/css" rel="stylesheet">
<link href="../css/new.css" rel="stylesheet" type="text/css">

<table width="100%"  border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="1" height="15"></td>
	</tr>
</table>
<table width="990" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td width="6" class="frTopLeft"></td>
		<td colspan="3" class="frTop"></td>
		<td width="7" class="frTopRight"></td>
	</tr>
	<tr> 
		<td rowspan="4" bgcolor="#f1f1f2" class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1"></td>
		<td width="10" rowspan="4" valign="top" bgcolor="#f1f1f2" class="frBgInt1">
			
		</td>
		<td height="50" valign="middle" class="frBgInt1">
			<table border="0" width="100%">
				<tr>
					<td align="left">
						<img id="imgMediaLibrary" runat="server" src="~/images/titles/add_to_channel.gif">
					</td>
					<td align="right">
						<asp:button id="btnAddToChannels1" runat="Server" CausesValidation="False" onClick="btnAddChannels_Click" class="Filter2" Text="  Add to Selected Communities  "/>
					</td>
				</tr>
			</table>
		</td>
		<td width="10" rowspan="4" valign="top" bgcolor="#f1f1f2" class="frBgInt1"></td>
		<td rowspan="4" class="frBorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1"></td>
	</tr>
	<tr>
		<td align="right" bgcolor="#f1f1f2" style="padding:0 4px 10px 0;">
			<asp:label runat="server" id="lblResults_Top" cssclass="insideTextNoBold" /><br />
			<kaneva:pager runat="server" isajaxmode="True" id="pgTop" onpagechanged="pg_PageChange" maxpagestodisplay="5" shownextprevlabels="true" />
		</td>
	</tr>
	<tr>
		<td valign="top" bgcolor="#f1f1f2" class="frBgInt1">
					
			
			<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
				<tr>
					<td height="7" class="intBorderCTopLeft"></td>
					<td colspan="10" class="intBorderCTop"></td>
					<td class="intBorderCTopRight"></td>
				</tr>
				<tr>
					<td height="20" class="intBorderCBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1"></td>
					<td valign="middle" align="center" class="intBorderCBgInt1"><input type="checkbox" class="formKanevaCheck" id="chkSelect" onclick="javascript:SelectAll(this,this.checked);"/></td>
					<td width="1" class="sepTit1"><img runat="server" src="~/images/spacer.gif" width="1" height="1"></td>
					<td width="1"  class="intBorderCBorderLeft"></td>
					<td width="1" class="sepTit1"><img runat="server" src="~/images/spacer.gif" width="1" height="1"></td>
					<td valign="top" class="intBorderCBgInt1" width="24">&nbsp;</td>			
					<td valign="middle" class="intBorderCBgInt1" align="left"><span class="content"><a>Community Name</a></span></td>
					<td width="1" class="sepTit1"><img runat="server" src="~/images/spacer.gif" width="1" height="1"></td>
					<td  valign="middle" class="intBorderCBgInt1" align="left"><img runat="server" src="~/images/spacer.gif" width="15" height="8"><span class="content"><a>Members</a></span></td>
					<td width="1" class="sepTit1"><img runat="server" src="~/images/spacer.gif" width="1" height="1"></td>
					<td valign="middle" class="intBorderCBgInt1" align="left"><img runat="server" src="~/images/spacer.gif" width="15" height="1"><span class="content"><a>Access</a></span></td>
					<td class="intBorderCBorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1"></td>
				</tr>
				<tr>
					<td height="7" class="intBorderCBottomLeft"></td>
					<td colspan="10" class="intBorderCBottom"></td>
					<td class="intBorderCBottomRight"></td>
				</tr>
				
				<asp:Repeater runat="server" id="rptChannels">  
					<ItemTemplate>
						<tr class="intColor1">
							<td height="125" class="intBorderC1BorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="110" ID="Img1"/></td>
							<td align="center" class="intColor1"><asp:checkbox runat="server" id="chkEdit" style="horizontal-align: right;" />
										<input type="hidden" runat="server" id="hidChannelId" value='<%#DataBinder.Eval(Container.DataItem, "community_id")%>' NAME="hidChannelId"></td>
							<td class="sepInt1"><img runat="server" src="~/images/spacer.gif" width="1" height="1" ID="Img2"/></td>
							<td align="center" class="intColor1">
							<!--image holder-->
													
								<div class="framesize-small">
									<div class="frame">
										<span class="ct"><span class="cl"></span></span>
										<div class="imgconstrain">
											<a href='<%# GetBroadcastChannelUrl (DataBinder.Eval (Container.DataItem, "name_no_spaces").ToString ())%>' style="cursor: hand;">
												<img runat="server" src='<%# GetBroadcastChannelImageURL (DataBinder.Eval(Container.DataItem, "thumbnail_small_path").ToString (), "sm") %>' border="0"/>
											</a>
										</div>
										<span class="cb"><span class="cl"></span></span>
									</div>
								</div>
										
							<!--end image holder-->			    
							</td>
							<td class="sepInt1"><img runat="server" src="~/images/spacer.gif" width="1" height="1" ID="Img12"/></td>
							<td valign="top" class="intColor1"><br /></td>
							<td valign="middle" class="intColor1" align="left">
								<span class="content"><a href='<%# GetBroadcastChannelUrl (DataBinder.Eval (Container.DataItem, "name_no_spaces").ToString ())%>'><%# TruncateWithEllipsis (DataBinder.Eval (Container.DataItem, "name").ToString (), 40)%></a></span>
								&nbsp;&nbsp;&nbsp;<span class="insideTextNoBold">Owner: <a class="dateStamp" href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "creator_username").ToString ())%>><%# DataBinder.Eval(Container.DataItem, "username") %></a></span><span class="insideBoxText11"><br /><br /><%# DataBinder.Eval(Container.DataItem, "description") %></span><br /><br /><span class="insideTextNoBold">Tags: <asp:label id="tags" runat="server" Text='<%# GetChannelTags (DataBinder.Eval(Container.DataItem, "keywords").ToString ())%>'/></span></td>
							<td valign="top" class="sepInt1"><img runat="server" src="~/images/spacer.gif" width="1" height="1" ID="Img13"/></td>
							<td valign="top" class="intColor1" align="left"><img runat="server" src="~/images/spacer.gif" width="15" height="28" ID="Img14"><span class="insideTextNoBold"><%# DataBinder.Eval (Container.DataItem, "number_of_members") %></span></td>
							<td valign="top" class="sepInt1"><img runat="server" src="~/images/spacer.gif" width="1" height="1" ID="Img15"/></td>
							<td valign="top" class="intColor1" align="left"><img runat="server" src="~/images/spacer.gif" width="15" height="28" ID="Img16"><span class="insideTextNoBold"><%# GetChannelStatus (DataBinder.Eval (Container.DataItem, "is_public")) %><br>
										<img runat="server" src="~/images/spacer.gif" width="15" height="28" ID="Img7"><%# GetChannelContentType (DataBinder.Eval (Container.DataItem, "is_adult").ToString()) %></span></td>
							<td class="intBorderC1BorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1" ID="Img17"/></td>
			 			</tr>
					</ItemTemplate>
					<AlternatingItemTemplate>
						<tr class="intColor2">
							<td height="125" class="intBorderC1BorderLeftColor1"><img runat="server" src="~/images/spacer.gif" width="1" height="110" ID="Img18"/></td>
							<td align="center" class="intColor2"><asp:checkbox runat="server" id="chkEdit" style="horizontal-align: right;" />
										<input type="hidden" runat="server" id="hidChannelId" value='<%#DataBinder.Eval(Container.DataItem, "community_id")%>' NAME="hidChannelId"></td>
							<td class="sepInt1"><img runat="server" src="~/images/spacer.gif" width="1" height="1" ID="Img19"/></td>
							<td align="center" class="intColor2">
							<!--image holder-->
													
								<div class="framesize-small">
									<div class="frame">
										<span class="ct"><span class="cl"></span></span>
										<div class="imgconstrain">
											<a href='<%# GetBroadcastChannelUrl (DataBinder.Eval (Container.DataItem, "name_no_spaces").ToString ())%>' style="cursor: hand;">
												<img runat="server" src='<%# GetBroadcastChannelImageURL (DataBinder.Eval(Container.DataItem, "thumbnail_small_path").ToString (), "sm") %>' border="0"/>
											</a>
										</div>
										<span class="cb"><span class="cl"></span></span>
									</div>
								</div>
										
									
							<!--end image holder-->			    
							</td>
							<td class="sepInt1"><img runat="server" src="~/images/spacer.gif" width="1" height="1" ID="Img28"/></td>
							<td valign="top" class="intColor2"><br /></td>
							<td valign="middle" class="intColor2" align="left">
								<span class="content"><a href='<%# GetBroadcastChannelUrl (DataBinder.Eval (Container.DataItem, "name_no_spaces").ToString ())%>'><%# TruncateWithEllipsis (DataBinder.Eval (Container.DataItem, "name").ToString (), 40)%></a></span>
								&nbsp;&nbsp;&nbsp;<span class="insideTextNoBold">Owner: <a class="dateStamp" href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "creator_username").ToString ())%>><%# DataBinder.Eval(Container.DataItem, "username") %></a></span><span class="insideBoxText11"><br /><br /><%# DataBinder.Eval(Container.DataItem, "description") %></span><br /><br /><span class="insideTextNoBold">Tags: <asp:label id="Label1" runat="server" Text='<%# GetChannelTags (DataBinder.Eval(Container.DataItem, "keywords").ToString ())%>'/></span></td>
							<td valign="top" class="sepInt1"><img runat="server" src="~/images/spacer.gif" width="1" height="1" ID="Img29"/></td>
							<td valign="top" class="intColor2" align="left"><img runat="server" src="~/images/spacer.gif" width="15" height="28" ID="Img30"><span class="insideTextNoBold"><%# DataBinder.Eval (Container.DataItem, "number_of_members") %></span></td>
							<td valign="top" class="sepInt1"><img runat="server" src="~/images/spacer.gif" width="1" height="1" ID="Img31"/></td>
							<td valign="top" class="intColor2" align="left"><img runat="server" src="~/images/spacer.gif" width="15" height="28" ID="Img32"><span class="insideTextNoBold"><%# GetChannelStatus (DataBinder.Eval (Container.DataItem, "is_public")) %><br>
										<img runat="server" src="~/images/spacer.gif" width="15" height="28" ID="Img33"><%# GetChannelContentType (DataBinder.Eval (Container.DataItem, "is_adult").ToString()) %></span></td>
							<td class="intBorderC1BorderRightColor2"><img runat="server" src="~/images/spacer.gif" width="1" height="1" ID="Img34"/></td>
			 			</tr>
					</AlternatingItemTemplate>
				</asp:Repeater>
				
				<tr style="padding: 2px 0 6px 0;">
					<td height="15" class="intBorderC1BorderLeft"><img id="Img3" runat="server" src="~/images/spacer.gif" width="1" height="1"/></td>
					<td align="right" colspan="10" class="intColor1">
						<asp:label runat="server" id="lblResults_Bottom" cssclass="insideTextNoBold" /><br />
						<kaneva:pager runat="server" isajaxmode="True" id="pgBottom" onpagechanged="pg_PageChange" maxpagestodisplay="5" shownextprevlabels="true" />
					</td>
					<td class="intBorderC1BorderRight"><img id="Img4" runat="server" src="~/images/spacer.gif" width="1" height="1"/></td>
				</tr>
				<tr>
					<td height="15" class="intBorderC1BorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1"/></td>
					<td colspan="5" class="intColor1" align="left">
						<span class="blueInfo">NOTE: You can only add items to a community you are a moderator or owner of, or belong to if the community allows it.</span>
					</td>
					<td colspan="5" class="intColor1" align="right">
						<asp:button id="btnAddToChannels" runat="Server" CausesValidation="False" onClick="btnAddChannels_Click" class="Filter2" Text="  Add to Selected Communities  "/>
					
					</td>
					<td class="intBorderC1BorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1"/></td>
				</tr>
				
				<tr>
					<td height="7" class="intBorderC1BottomLeft"></td>
					<td colspan="10" class="intBorderC1Bottom"></td>
					<td class="intBorderC1BottomRight"></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" bgcolor="#f1f1f2"><img runat="server" src="~/images/spacer.gif" width="1" height="10"></td>
	</tr>
	<tr>
		<td class="frBottomLeft"></td>
		<td colspan="3" class="frBottom"></td>
		<td class="frBottomRight"></td>
	</tr>
</table>