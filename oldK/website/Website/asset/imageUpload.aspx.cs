///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using log4net;

namespace KlausEnt.KEP.Kaneva
{
    /// <summary>
    /// Summary description for imageUpload.
    /// </summary>
    public class imageUpload : BasePage
    {
        private void Page_Load(object sender, System.EventArgs e)
        {
            // Put user code to initialize the page here
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnUpload_Click(object sender, EventArgs e)
        {
            //string url = "";

            // They must be logged in
            if (!Request.IsAuthenticated)
            {
                Response.Redirect(GetLoginURL());
                return;
            }

            // Files are now stored in content repository
            string contentRepository = "";
            if (KanevaGlobals.ContentServerPath != null)
            {
                contentRepository = KanevaGlobals.ContentServerPath;
            }
            else
            {
                ShowErrorOnStartup("No Content server path (content repository)", false);
                return;
                //Response.Write("No Content server path (content repository)");
            }

            //declare the file name of the image
            string filename = null;

            //create PostedFile
            HttpPostedFile File = inpThumbnail.PostedFile;

            try
            {
                //generate newpath
                int assetId = Convert.ToInt32(Request["assetId"]);
                string newPath = Path.Combine(Path.Combine(contentRepository, KanevaWebGlobals.GetUserId().ToString()), assetId.ToString());

                //upload image from client to image repository
                ImageHelper.UploadImageFromUser(ref filename, File, newPath, KanevaGlobals.MaxUploadedImageSize);

                //generate content type and image path
                //string imagePath = newPath + Path.DirectorySeparatorChar + filename;
                string imagePath = File.FileName.Remove(0, File.FileName.LastIndexOf("\\") + 1);
                string origImagePath = newPath + Path.DirectorySeparatorChar + filename;
                string contentType = File.ContentType;

                StoreUtility.UpdateAssetImage(Convert.ToInt32(Request["assetId"]), origImagePath, contentType);

                //create any and all thumbnails needed
                ImageHelper.GenerateAllAssetThumbnails(filename, origImagePath, imagePath, contentRepository, GetUserId(), assetId);

                 //If successfull, send javascript to update mykaneva page, close the window
                if (!Page.ClientScript.IsClientScriptBlockRegistered(GetType(), "startup"))
                {
                    //string javascript = "<script language=JavaScript>window.opener.location.reload();window.close();</script>";
                    string javascript = "<script language=JavaScript>window.close();</script>";
                    Page.ClientScript.RegisterStartupScript(GetType(), "startup", javascript);
                }

            }
            catch (Exception ex)
            {
                ShowErrorOnStartup(ex.Message, false);
            }

        }

        protected HtmlInputFile inpThumbnail;

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #region Web Form Designer generated code

        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}

