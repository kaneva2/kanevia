///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.IO;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva.channel;
using log4net;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
    /// <summary>
    /// Summary description for assetEdit.
    /// </summary>
    public class assetEdit : StoreBasePage
    {
        /// <summary>
        /// Constructor
        /// </summary>
        protected assetEdit () 
        {
            Title = "Item Edit"; 
        }

        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Page_Load (object sender, System.EventArgs e)
        {
			if(Request.IsAuthenticated)
			{
				if(GetRequestParams())
				{
					//check if user is allowed to edit this channel
					if (!IsUserAllowedToEdit())
					{
						RedirectToHomePage ();
					}
				}
				else
				{
					//invalid request params
					//if the user logged in, send him to his home page
					//if not, redirect to login page
					RedirectToHomePage ();
				}
			}
			else
			{
				Response.Redirect(this.GetLoginURL());
			}

            // Extend the timeout for this page
            this.AddKeepAlive ();

			// Enable Max Lengths of TextAreas
			txtTeaser.Attributes.Add ("MaxLength", "1000");
			txtPerAdditional.Attributes.Add ("MaxLength", "255");
			txtCommAdditional.Attributes.Add ("MaxLength", "255");
            if (!ClientScript.IsClientScriptBlockRegistered(GetType(), "taMaxLength"))
			{
				string strScript = "<script language=JavaScript> addEvent(window, \"load\", textAreasInit);</script>";
                ClientScript.RegisterClientScriptBlock(GetType(), "taMaxLength", strScript);
			}
            
            DataRow drAsset = StoreUtility.GetAsset (_assetId);

            if (drAsset["status_id"] != DBNull.Value && 
                (Convert.ToInt32(drAsset["status_id"]) != 2 && Convert.ToInt32(drAsset["status_id"]) != 4)
                )
            {
                // Are they the asset owner?
                bool bIsAssetOwner = StoreUtility.IsAssetOwner(GetUserId(), _assetId);

                DeleteAsset.Visible = IsAdministrator();

                //setup header nav bar
                if (bIsAssetOwner)
                {
                    //user's own channel
                    HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
                    HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.MEDIA_UPLOADS;
                }
                else
                {
                    //navs go to home-my channels for all channels if the user is an admin
                    HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.PLAY;
                }

                // Set Nav
                HeaderNav.SetNavVisible(HeaderNav.MyKanevaNav, 2);

                // This code sets the channel id to the owner of the asset if the current user is an administrator
                // and selected the edit link from the asset details page.  Without this, the menu would not
                // display the correct channel for the current item being edited.
                if (IsAdministrator())
                {
                    HeaderNav.MyChannelsNav.ChannelId = GetUserFacade.GetPersonalChannelId(Convert.ToInt32(drAsset["owner_id"]));
                }

                string assetName = Server.HtmlDecode(drAsset["name"].ToString());

                // Only show thumb if not a picture
                if (_assetId > 0)
                {
                    ShowThumbnailInfo(!Convert.ToInt32(drAsset["asset_type_id"]).Equals((int)Constants.eASSET_TYPE.PICTURE));
                }

                // Bread crumb
                AddBreadCrumb(new BreadCrumb("Item Edit: " + TruncateWithEllipsis(assetName, 18), GetCurrentURL(), "", 0));

                if (!IsPostBack)
                {
                    SetReturnUrl();

                    // Check permissions
                    if (!HasAssetUpdatePrivileges(drAsset, GetUserId()))
                    {
                        ShowErrorOnStartup("You must be asset owner to modify this asset.");
                        return;
                    }

                    // Get the asset type id
                    int assetTypeId = (int)Constants.eASSET_TYPE.VIDEO;
                    if (!drAsset["asset_type_id"].Equals(DBNull.Value))
                    {
                        assetTypeId = Convert.ToInt32(drAsset["asset_type_id"]);
                    }

                    txtItemName.Text = assetName;
                    lblItemName.Text = assetName;
                    lblCreatedBy.Text = drAsset["username"].ToString();

                    txtTeaser.Text = Server.HtmlDecode(drAsset["teaser"].ToString());
                    txtBody.Text = Server.HtmlDecode(drAsset["body_text"].ToString());

                    txtImageCaption.Text = Server.HtmlDecode(drAsset["image_caption"].ToString());

                    imgPreview.Src = GetMediaImageURL(drAsset["thumbnail_small_path"].ToString(), "sm", _assetId, assetTypeId, Convert.ToInt32(drAsset["thumbnail_gen"]));
                    imgPreview2.Src = GetMediaImageURL(drAsset["thumbnail_medium_path"].ToString(), "me", _assetId, assetTypeId, Convert.ToInt32(drAsset["thumbnail_gen"]));
                    btnUploadImage.Attributes.Add("onclick", GetUploadAssetImageLink(GetAssetId()));

                    // Set up the asset type dropdown
                    drpType.DataTextField = "description";
                    drpType.DataValueField = "asset_type_id";
                    string assetTypeFilter = "";

                    switch (assetTypeId)
                    {
                        case (int)Constants.eASSET_TYPE.TV:
                        case (int)Constants.eASSET_TYPE.WIDGET:
                        case (int)Constants.eASSET_TYPE.MUSIC:
                        case (int)Constants.eASSET_TYPE.VIDEO:
                            assetTypeFilter = "asset_type_id = " + assetTypeId;
                            drpType.Visible = false;
                            lblItemType.Visible = true;
                            break;
                        case (int)Constants.eASSET_TYPE.GAME:
                            assetTypeFilter = "asset_type_id IN (" + (int)Constants.eASSET_TYPE.VIDEO + ", " + (int)Constants.eASSET_TYPE.GAME + ")";
                            drpType.Visible = true;
                            lblItemType.Visible = false;
                            tblGameInstructions.Visible = true;
                            txtGameInstructions.Text = Server.HtmlDecode(drAsset["instructions"].ToString());
                            trCompanyName.Visible = true;
                            txtCompanyName.Text = Server.HtmlDecode(drAsset["company_name"].ToString());
                            break;
                        case (int)Constants.eASSET_TYPE.PICTURE:
                        case (int)Constants.eASSET_TYPE.PATTERN:
                            //trAssetType.Visible = true; 
                            assetTypeFilter = "asset_type_id IN (" + (int)Constants.eASSET_TYPE.PICTURE + ", " + (int)Constants.eASSET_TYPE.PATTERN + ")";
                            drpType.Visible = true;
                            lblItemType.Visible = false;
                            break;
                    }
                    drpType.DataSource = WebCache.GetAssetTypes(assetTypeFilter);
                    drpType.DataBind();

                    //configure media type drop down
                    SetDropDownIndex(drpType, assetTypeId.ToString());
                    lblItemType.Text = drpType.SelectedItem.Text;

                    // Permissions
                    drpPermission.DataValueField = "permission_id";
                    drpPermission.DataTextField = "name";
                    drpPermission.DataSource = WebCache.GetAssetPermissions();
                    drpPermission.DataBind();
                    SetDropDownIndex(drpPermission, drAsset["permission"].ToString());

                    // Channels
                    BindAssetChannels(_assetId);

                    // Amount of asset
                    Double dollarAmount = ConvertKPointsToDollars(Convert.ToDouble(drAsset["amount"]));

                    // Set the correct copyright option
                    GetCopyrightInfo(drAsset, lblLicense, lblLicenseURL);

                    switch (drAsset["license_type"].ToString())
                    {
                        case "D":
                            {
                                optPublic.Checked = true;
                                break;
                            }
                        case "C":
                            {
                                SetDropDownIndex(drpCCLicense, drAsset["license_cc"].ToString());
                                optCC.Checked = true;
                                break;
                            }
                        case "P":
                            {
                                txtPerAdditional.Text = Server.HtmlDecode(drAsset["license_additional"].ToString());
                                optPersonal.Checked = true;
                                break;
                            }
                        case "I":
                            {
                                txtCommAdditional.Text = Server.HtmlDecode(drAsset["license_additional"].ToString());
                                optCommercial.Checked = true;
                                break;
                            }
                        case "O":
                            {
                                optOther.Checked = true;
                                txtLicenseName.Text = Server.HtmlDecode(drAsset["license_name"].ToString());
                                txtLicenseURL.Text = Server.HtmlDecode(drAsset["license_URL"].ToString());
                                break;
                            }
                    }

                    // The owner's tags
                    bool bHasTags = false;

                    if (drAsset["keywords"] != null)
                    {
                        txtKeywords.Text = drAsset["keywords"].ToString();
                        if (drAsset["keywords"].ToString().Trim().Length > 0)
                        {
                            bHasTags = true;
                        }
                    }

                    // if user is a minor, can't set restricted flag
                    if (!IsCurrentUserAdult(false))
                    {
                        chkMature.Enabled = false;
                        spnMatureDesc.Attributes.Add("class", "note");
                    }

                    // Is it mature?
                    if (IsMature(Convert.ToInt32(drAsset["asset_rating_id"])))
                    {
                        chkMature.Checked = true;

                        if (!IsAdministrator())
                        {
                            chkMature.Enabled = false;
                            spnMatureDesc.Attributes.Add("class", "note");
                        }
                    }

                    // Category
                    m_category1 = Convert.ToInt32(drAsset["category1_id"]);
                    m_category2 = Convert.ToInt32(drAsset["category2_id"]);
                    m_category3 = Convert.ToInt32(drAsset["category3_id"]);
                    PopulateCategory(assetTypeId);

                    // Asset size
                    lblAssetSize.ForeColor = System.Drawing.Color.Green;
                    lblAssetSize.Text = FormatImageSize(drAsset["file_size"]);

                    spnAlertMsg.Style.Add("display", "none");
                    // Show the missing message if any data is missing
                    // Data is tags, categories, or short description
                    if (!bHasTags || drAsset["teaser"].ToString().Length.Equals(0)
                        || (m_category1.Equals(0) && m_category2.Equals(0) && m_category3.Equals(0)))
                    {
                        spnAlertMsg.Style.Add("display", "block");

                        spnAlertMsg.InnerHtml = KanevaWebGlobals.GetResourceString("suggestAssetFields") +
                            "<ul style=\"padding-top:0;padding-bottom:0;margin:0;\">";

                        if (!bHasTags)
                        {
                            spnAlertMsg.InnerHtml += "<li>Tags";
                        }
                        if (m_category1.Equals(0) && m_category2.Equals(0) && m_category3.Equals(0))
                        {
                            spnAlertMsg.InnerHtml += "<li>Categories";
                        }

                        if (drAsset["teaser"].ToString().Length.Equals(0))
                        {
                            spnAlertMsg.InnerHtml += "<li>Short Description";
                        }

                        spnAlertMsg.InnerHtml += "</ul>";

                    }
                }

                // Tag validator
                regtxtKeywords.ValidationExpression = Constants.VALIDATION_REGEX_TAG;
                regtxtKeywords.ErrorMessage = Constants.VALIDATION_REGEX_TAG_ERROR_MESSAGE;

                // Add the javascript for removing item from channel
                string scriptString = "<script language=\"javaScript\">\n<!--\n function RemoveChannel (id) {\n";
                scriptString += "   $('" + hidChannelId.ClientID + "').value = id;\n";
                scriptString += ClientScript.GetPostBackEventReference(btnRemoveChannel, "") + "\n";
                scriptString += "}\n// -->\n";
                scriptString += "</script>";

                if (!ClientScript.IsClientScriptBlockRegistered(GetType(), "RemoveChannel"))
                {
                    ClientScript.RegisterClientScriptBlock(GetType(), "RemoveChannel", scriptString);
                }

                //set the back button
                back.NavigateUrl = returnURL;
            }
            else
            {
                Response.Redirect("~/asset/publishedItemsNew.aspx");
            }
        }


        #region Helper Methods

        /// <summary>
        /// Sets the return url based on querystring param or urlreferrer property of the request object
        /// </summary>
        /// <returns></returns>
        private void SetReturnUrl ()
        {
            try
            {
                if (Request.QueryString["back"] != null && Request.QueryString["back"] == "ML")
                {
                    returnURL = ResolveUrl ("~/asset/publishedItemsNew.aspx");
                    return;
                }

                returnURL = Request.UrlReferrer.ToString ();
            }
            catch { }
        }

        /// <summary>
        /// returns true if asset id is retrieved from querystring
        /// </summary>
        /// <returns></returns>
        private bool GetRequestParams ()
		{
			bool retVal = true;
			try
			{
				_assetId = Convert.ToInt32 (Request ["assetId"]);
			}
			catch(Exception)
			{
				//invalid numbers
				retVal = false;
			}
			return retVal;
		}

		/// <summary>
		/// returns true if current user is allowed to edit this channel
		/// </summary>
		/// <returns></returns>
		private bool IsUserAllowedToEdit()
		{
			return IsAdministrator() || IsCSR () || StoreUtility.IsAssetOwner(GetUserId(), _assetId);
		}

		/// <summary>
		/// Is the category checked?
		/// </summary>
		/// <param name="catId"></param>
		/// <returns></returns>
		protected bool GetCatChecked (int catId)
		{
			return (m_category1.Equals (catId) || m_category2.Equals (catId) || m_category3.Equals (catId));
		}

        /// <summary>
        /// GetUploadAssetImageLink
        /// </summary>
        /// <returns></returns>
        private string GetUploadAssetImageLink (int assetId)
        {
            return "javascript: window.open('" + ResolveUrl ("~/asset/imageUpload.aspx?assetId=" + assetId) + "','add','toolbar=no,width=550,height=200,menubar=no,scrollbars=no');";
        }

        /// <summary>
        /// GetAssetId
        /// </summary>
        /// <returns></returns>
        private int GetAssetId ()
        {
            if (_assetId <= 0)
            {
                // This is an add new and was already inserted above
                return Convert.ToInt32 (ViewState ["newAssetId"]);
            }
            else
            {
                return _assetId;
            }
        }

        /// <summary>
        /// Get a subscription for a given length
        /// </summary>
        /// <param name="dtCommunitySubscription"></param>
        /// <param name="lengthOfSubscription"></param>
        /// <returns></returns>
        private DataRow GetSubscription (DataTable dtAssetSubscription, int lengthOfSubscription)
        {
            DataRow [] drResult = dtAssetSubscription.Select ("length_of_subscription = " + lengthOfSubscription);
            if (drResult.Length > 0)
            {
                return drResult [0];
            }
            else
            {
                return null;
            }
        }

		/// <summary>
		/// Return the delete javascript
		/// </summary>
		/// <param name="TopicID"></param>
		/// <returns></returns>
		public string GetChannelRemoveScript (int communityId, int isPersonal)
		{
			if (isPersonal.Equals (1))
			{
				return "javascript: alert(\"You cannot remove this item from a profile.\")";
			}
			return "javascript:if (confirm(\"Are you sure you want to remove this item from the community?\")){RemoveChannel (" + communityId + ")};";
		}

        /// <summary>
        /// Delete an asset
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Delete_Asset(Object sender, EventArgs e)
        {
            if (IsAdministrator())
            {
                StoreUtility.DeleteAsset(_assetId, GetUserId());
                Response.Redirect(returnURL);
            }
        }

        /// <summary>
        /// Intended to write the output of the NSIS make process to a debug
        /// log in case an error occurs. This debug log can later be inspected
        /// to determine the cause of the error.
        /// </summary>
        /// <param name="output">the output of the NSIS make</param>
        /// <param name="assetId">the asset id that the make was being attempted on. Will construct the log path based on this id</param>
        protected void WriteNSIDebugLog(string output, int assetId)
        {
            string log = Server.MapPath ("~/download/launcher/" + assetId.ToString () + "/NSISMake.log");

            FileStream fs = new FileStream(log, FileMode.Create, FileAccess.Write);
            BinaryWriter w = new BinaryWriter(fs);

            w.Write(output);

            w.Close();
            fs.Close();
        }

        /// <summary>
        /// Compares two files for binary equality. Currently used for
        /// icon compare.
        /// </summary>
        /// <param name="file1">first file to compare</param>
        /// <param name="file2">second file to compare</param>
        /// <returns>true=equal, false=not equal</returns>
        protected bool IconCompare(string file1, string file2)
        {
            FileStream fs1 = new FileStream(file1, FileMode.Open);
            FileStream fs2 = new FileStream(file2, FileMode.Open);
            int file1byte, file2byte;
  
            if (fs1.Length != fs2.Length)
            {
                fs1.Close();
                fs2.Close();
                return false;
            }

            do
            {
                file1byte = fs1.ReadByte();
                file2byte = fs2.ReadByte();
            } while ((file1byte == file2byte) && (file1byte != -1));

            fs1.Close();
            fs2.Close() ;

            return ((file1byte - file2byte) == 0);
        }

        /// <summary>
        /// Display inline message box to the user
        /// </summary>
        /// <param name="msg">message to be displayed</param>
        /// <param name="isErr">boolean noting if this message is an error or not</param>
        private void ShowMessage (string msg)
        {
            ShowMessage (msg, false);
        }
        private void ShowMessage (string msg, bool isErr)
        {
            string cssClass = isErr ? "errBox black" : "infoBox black";

            spnAlertMsg.InnerHtml = msg;
            spnAlertMsg.Attributes.Add ("class", cssClass);
            spnAlertMsg.Style.Add ("display", "block");
        }

        /// <summary>
        /// UpdateAsset
        /// </summary>
        /// <returns></returns>
        protected bool UpdateAsset ()
        {
			int assetId = GetAssetId ();
			DataRow drAsset = StoreUtility.GetAsset (assetId);
			int assetType = Convert.ToInt32 (drpType.SelectedValue);
            
			// Check for any inject scripts in large text fields
			if ((KanevaWebGlobals.ContainsInjectScripts (txtBody.Text)) || (KanevaWebGlobals.ContainsInjectScripts (txtTeaser.Text)))
			{
				m_logger.Warn ("User " + KanevaWebGlobals.GetUserId () + " tried to script from IP " + Common.GetVisitorIPAddress());
				ShowMessage ("Your input contains invalid scripting, please remove script code and try again.", true);
				return false;
			}

			// Check for any inject scripts in smaller text fields
			if ((KanevaWebGlobals.ContainsInjectScripts (txtItemName.Text)) || (KanevaWebGlobals.ContainsInjectScripts (this.txtKeywords.Text)))
			{
				m_logger.Warn ("User " + KanevaWebGlobals.GetUserId () + " tried to script from IP " + Common.GetVisitorIPAddress());
				ShowMessage ("Your input contains invalid scripting, please remove script code and try again.", true);
				return false;
			}

            // Server validation
			Page.Validate ();
            if (!Page.IsValid) 
            {
                return false;
            }

            // Check to make sure user did not enter any "potty mouth" words
            if (KanevaWebGlobals.isTextRestricted (txtItemName.Text, Constants.eRESTRICTION_TYPE.POTTY_MOUTH) ||
                KanevaWebGlobals.isTextRestricted (txtBody.Text, Constants.eRESTRICTION_TYPE.POTTY_MOUTH) ||
                KanevaWebGlobals.isTextRestricted (txtTeaser.Text, Constants.eRESTRICTION_TYPE.POTTY_MOUTH) ||
                KanevaWebGlobals.isTextRestricted (txtKeywords.Text, Constants.eRESTRICTION_TYPE.POTTY_MOUTH) ||
                KanevaWebGlobals.isTextRestricted (txtImageCaption.Text, Constants.eRESTRICTION_TYPE.POTTY_MOUTH))
            {
                spnAlertMsg.InnerText = Constants.VALIDATION_REGEX_RESTRICTED_WORD_MESSAGE;
                spnAlertMsg.Attributes.Add ("class", "errBox black");
                spnAlertMsg.Style.Add ("display", "block");
                return false;
            }

            // These fields are very rarely used if used at all.  No need to do check on them if they are empty
            if (txtPerAdditional.Text.Trim () != string.Empty ||
                txtCommAdditional.Text.Trim () != string.Empty ||
                txtLicenseName.Text.Trim () != string.Empty)
            {
                if (KanevaWebGlobals.isTextRestricted (txtPerAdditional.Text, Constants.eRESTRICTION_TYPE.POTTY_MOUTH) ||
                    KanevaWebGlobals.isTextRestricted (txtCommAdditional.Text, Constants.eRESTRICTION_TYPE.POTTY_MOUTH) ||
                    KanevaWebGlobals.isTextRestricted (txtLicenseName.Text, Constants.eRESTRICTION_TYPE.POTTY_MOUTH))
                {
                    spnAlertMsg.InnerText = Constants.VALIDATION_REGEX_RESTRICTED_WORD_MESSAGE;
                    spnAlertMsg.Attributes.Add ("class", "errBox black");
                    spnAlertMsg.Style.Add ("display", "block");
                    return false;
                }
            }

            // if the game only fields are visible, then check them
            if (tblGameInstructions.Visible)
            {
                if (KanevaWebGlobals.isTextRestricted (txtCompanyName.Text, Constants.eRESTRICTION_TYPE.POTTY_MOUTH) ||
                    KanevaWebGlobals.isTextRestricted (txtGameInstructions.Text, Constants.eRESTRICTION_TYPE.POTTY_MOUTH))
                {
                    spnAlertMsg.InnerText = Constants.VALIDATION_REGEX_RESTRICTED_WORD_MESSAGE;
                    spnAlertMsg.Attributes.Add ("class", "errBox black");
                    spnAlertMsg.Style.Add ("display", "block");
                    return false;
                }
            }

            // Make sure they have rights to update it
            if (!HasAssetUpdatePrivileges (drAsset, GetUserId ()))
            {
                ShowErrorOnStartup ("You do not have rights to modify this asset");
                return false;
            }

            string licenseType = "P";
            string licenseName = "";
            string licenseAdditional = "";
            
            // Set the correct option
            if (optPublic.Checked)
            {
                licenseType = "D";
            }
            else if (optCC.Checked)
            {
                licenseType = "C";
            }
            else if (optPersonal.Checked)
            {
                licenseType = "P";
                licenseAdditional = Server.HtmlEncode (txtPerAdditional.Text);
            }
            else if (optCommercial.Checked)
            {
                licenseType = "I";
                licenseAdditional = Server.HtmlEncode (txtCommAdditional.Text);
            }
            else if (optOther.Checked)
            {
                licenseType = "O";
                licenseName = Server.HtmlEncode (txtLicenseName.Text);
            }

            string itemName = Server.HtmlEncode (txtItemName.Text.Trim ());

			int isMature = (int) Constants.eASSET_RATING.GENERAL;
			if (chkMature.Checked)
			{
				isMature = (int) Constants.eASSET_RATING.MATURE;
			}

			// Permissions
			int permission = Convert.ToInt32 (Server.HtmlEncode (drpPermission.SelectedValue));
			int permissionGroup = 0;

			// Categories
			int category1Id = 0;
			int category2Id = 0;
			int category3Id = 0;
			CheckBox chkCategory;
			HtmlInputHidden hidCatId;

			int checkedCount = 0;
			foreach (DataListItem dliCategory in dlCategories.Items)
			{
				chkCategory = (CheckBox) dliCategory.FindControl ("chkCategory");

				if (chkCategory.Checked)
				{
					hidCatId = (HtmlInputHidden) dliCategory.FindControl ("hidCatId");
					
					if (category1Id.Equals (0))
					{
						category1Id = Convert.ToInt32 (hidCatId.Value);
					}
					else if (category2Id.Equals (0))
					{
						category2Id = Convert.ToInt32 (hidCatId.Value);
					}
					else if (category3Id.Equals (0))
					{
						category3Id = Convert.ToInt32 (hidCatId.Value);
					}

					checkedCount++;
				}
			}

			if( checkedCount == 0 && (assetType == (int)Constants.eASSET_TYPE.PATTERN))
			{
				cvDlCategories.IsValid = false;
				cvDlCategories.Visible = true;
				return false;
			}

            // If asset is a game, then we set these two additional fields
            string companyName = "";
            string gameInstructions = "";
            if (assetType == (int) Constants.eASSET_TYPE.GAME)
            {
                companyName = Server.HtmlEncode (txtCompanyName.Text);
                gameInstructions = Server.HtmlEncode (txtGameInstructions.Text);
            }

            int result = StoreUtility.UpdateAsset (GetUserId (), assetId, assetType, itemName,
                Server.HtmlEncode (txtBody.Text), "", Server.HtmlEncode (txtTeaser.Text), category1Id, category2Id, category3Id, 0.0, Constants.CURR_KPOINT, Server.HtmlEncode (txtImageCaption.Text),  
                isMature, licenseName, Server.HtmlEncode (drpCCLicense.SelectedValue), Server.HtmlEncode (txtLicenseURL.Text), licenseType, licenseAdditional,
                permission, permissionGroup, gameInstructions, companyName);


            // Update the tags
			int ownerId = Convert.ToInt32 (drAsset ["owner_id"]);
			StoreUtility.UpdateAssetTags (assetId, Server.HtmlEncode (KanevaGlobals.GetNormalizedTags(txtKeywords.Text)));

            // Was it successful?
            if (result == 1)
            {
                ShowMessage ("A store item name \\'" + txtItemName.Text + "\\' already exists, please change the item name.", true);
                return false;
            }

            if (assetType.Equals ((int) Constants.eASSET_TYPE.GAME))
            {
                // Update the price to 0, it is not a commercial game
                StoreUtility.UpdateAsset (ownerId, assetId, itemName, Server.HtmlEncode (txtTeaser.Text), 0.0);
            }

            // Save Attributes
            SaveAttributes (assetId);

            if (!IsAdministrator () && chkMature.Checked)
            {
                chkMature.Enabled = false;
                spnMatureDesc.Attributes.Add ("class", "note");
            }

            return true;
        }

        /// <summary>
        /// NavigateToReturn
        /// </summary>
        private void NavigateToReturn ()
        {
            NavigateBackToBreadCrumb (1);
        }

        /// <summary>
        /// Save any attributes
        /// </summary>
        /// <param name="assetId"></param>
        private void SaveAttributes (int assetId)
        {
            // First delete the existing
            StoreUtility.DeleteAssetAttributes (assetId);
            int selectedValue = 0;
            
            // Now Save the new
            for (int i = 0; i < dgrdAttributes.Items.Count; i++)
            {
                DropDownList drpAttributeValue = (DropDownList) dgrdAttributes.Items [i].FindControl ("drpAttributeValue");
                TextBox txtAttributeValue = (TextBox) dgrdAttributes.Items [i].FindControl ("txtAttributeValue");
                TextBox txtAttributeValueMultiline = (TextBox) dgrdAttributes.Items [i].FindControl ("txtAttributeValueMultiline");

                HtmlInputHidden hidAttributeId = (HtmlInputHidden) dgrdAttributes.Items [i].FindControl ("hidAttributeId");
                HtmlInputHidden hidAttributeType = (HtmlInputHidden) dgrdAttributes.Items [i].FindControl ("hidAttributeType");
                    
                // Save this attribute
                if (drpAttributeValue.SelectedValue.Length > 0)
                {
                    selectedValue = Convert.ToInt32 (drpAttributeValue.SelectedValue);
                }
                else
                {
                    selectedValue = 0;
                }

                string textValue = txtAttributeValue.Text;
                // is it a multiline?
                if (hidAttributeType.Value.Equals ("3"))
                {
                    textValue = txtAttributeValueMultiline.Text;
                }

                StoreUtility.InsertAssetAttributeValue (assetId, Convert.ToInt32 (hidAttributeId.Value), selectedValue, Server.HtmlEncode (textValue));
            }
        }

        /// <summary>
        /// Populate the categories
        /// </summary>
        /// <param name="assetTypeId"></param>
        private void PopulateCategory (int assetTypeId)
        {
			dlCategories.DataSource = WebCache.GetAssetCategories (assetTypeId);
			dlCategories.DataBind ();
        }

        /// <summary>
        /// Populate the attributes depending on the category
        /// </summary>
        /// <param name="assetTypeId"></param>
        private void PopluateAttributes (int categoryId)
        {
            DataTable dtAttributes = StoreUtility.GetAttributes (categoryId, "name");

            if (dtAttributes.Rows.Count > 0)
            {
                //trAttributes.Visible = true;
                dgrdAttributes.DataSource = dtAttributes;
                dgrdAttributes.DataBind ();
            }
            else
            {
                //trAttributes.Visible = false;
                dgrdAttributes.DataSource = null;
                dgrdAttributes.DataBind ();
            }
        }

        /// <summary>
        /// GetAttributeValuesList
        /// </summary>
        /// <param name="attributeId"></param>
        /// <returns></returns>
        protected DataTable GetAttributeValuesList (int attributeId)
        {
            return StoreUtility.GetAttributeValues (attributeId, "name");
        }

        /// <summary>
        /// GetAssetAttributeValue
        /// </summary>
        /// <returns></returns>
        protected string GetAssetAttributeValue (int attributeId)
        {
            // Get the current value for this attritbute
            int assetId = GetAssetId ();
            DataTable dtAssetAttributeValues = StoreUtility.GetAssetAttributeValues (assetId);

            DataRow [] drResult = dtAssetAttributeValues.Select ("attribute_id = " + attributeId);
            
            if (drResult.Length > 0)
            {
                return Server.HtmlDecode (drResult [0]["attribute_value"].ToString ());
            }

            return "";
        }

        /// <summary>
        /// GetAssetAttributeValueIndex
        /// </summary>
        /// <param name="attributeId"></param>
        /// <returns></returns>
        protected int GetAssetAttributeValueIndex (int attributeId)
        {
            int currentValue = 0;

            // Get the current value for this attritbute
            int assetId = GetAssetId ();
            DataTable dtAssetAttributeValues = StoreUtility.GetAssetAttributeValues (assetId);
            DataRow [] drResult = dtAssetAttributeValues.Select ("attribute_id = " + attributeId);

            if (drResult.Length > 0)
            {
                if (drResult [0]["category_attribute_value_id"] != DBNull.Value)
                {
                    currentValue = Convert.ToInt32 (drResult [0]["category_attribute_value_id"]);
                }
            }

            DataTable dtAttributeValues = StoreUtility.GetAttributeValues (attributeId, "name");
            for (int i = 0; i < dtAttributeValues.Rows.Count; i++)
            {
                if (dtAttributeValues.Rows [i]["category_attribute_value_id"].Equals (currentValue))
                {
                    return i;
                }
            }

            return 0;
        }

        /// <summary>
        /// IsVisible
        /// </summary>
        /// <param name="attributeId"></param>
        /// <param name="type"></param>
        /// <param name="uiType"></param>
        /// <returns></returns>
        protected bool IsVisible (int attributeId, int type, int uiType)
        {
            if (type.Equals (uiType))
            {
                return true;
            }

            return false;
            //DataTable dtAttributeValues = StoreUtility.GetAttributeValues (attributeId, "name");
            //return (dtAttributeValues.Rows.Count == 0);
        }

		/// <summary>
		/// BindAssetChannels
		/// </summary>
		private void BindAssetChannels (int assetId)
		{
			rptChannels.DataSource = StoreUtility.GetChannelsSharingAsset (assetId, true, true, "", 1, 50);
			rptChannels.DataBind ();
		}

        /// <summary>
        /// HasAssetUpdatePrivileges
        /// </summary>
        private bool HasAssetUpdatePrivileges (DataRow drAsset, int userId)
        {
            int assetId = Convert.ToInt32 (drAsset ["asset_id"]);
            bool bIsAssetOwner = StoreUtility.IsAssetOwner (userId, assetId);

            // Now it could be a new asset not yet in a community
            if (!bIsAssetOwner)
            {
                // Not editing a community asset, make sure they have administrator access  
                if (!(UsersUtility.IsUserAdministrator () || UsersUtility.IsUserCSR ()))
                {
                    return false;
                }
            }

            return true;
        }

		/// <summary>
		/// ShowThumbnailInfo
		/// </summary>
		private void ShowThumbnailInfo (bool bShow)
		{
            tblThumb.Visible = bShow;
        }

        #endregion Helper Methods


        #region Event Handlers

        /// <summary>
        /// lnkAddChannel_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkAddChannel_Click (Object sender, EventArgs e)
        {
            // Save the changes to the asset
            if (!UpdateAsset ())
            {
                return;
            }

            // Only the owner can delete it
            if (!StoreUtility.IsAssetOwner (GetUserId (), _assetId))
            {
                ShowMessage ("You must be the item owner to add this item to a community.", true);
                return;
            }

            Response.Redirect (ResolveUrl ("~/asset/addAssetToChannel.aspx?assetId=" + _assetId));
        }

        /// <summary>
        /// btnClearThumb_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnClearThumb_Click (object sender, EventArgs e)
        {
            int assetId = GetAssetId ();
            StoreUtility.DeleteAssetThumbs (assetId);
        }

        /// <summary>
        /// btnClearIcon_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnClearIcon_Click (object sender, EventArgs e)
        {
            int assetId = GetAssetId ();
            StoreUtility.UpdateGameIconImage (assetId, null, "", "");
        }

        /// <summary>
        /// Update Event Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnUpdate_Click (object sender, EventArgs e)
        {
            // Save the changes to the asset
            if (!UpdateAsset ())
            {
                return;
            }

            ShowMessage ("Changes saved successfully.");
        }

        /// <summary>
        /// Cancel Button Event Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancel_Click (object sender, EventArgs e)
        {
            Response.Redirect (returnURL);
        }

        /// <summary>
        /// Change type of asset
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void drpType_Change (Object sender, EventArgs e)
        {
            PopulateCategory (Convert.ToInt32 (drpType.SelectedValue));
            PopluateAttributes (0);
        }

        /// <summary>
        /// chkAdvanced_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void chkAdvanced_Click (Object sender, EventArgs e)
        {
            if (chkAdvanced.Checked)
            {
                txtBody.ConfigurationPath = ResolveUrl ("~/CuteSoft_Client/CuteEditor/Configuration/AutoConfigure/assetAdvanced.config");
            }
            else
            {
                txtBody.ConfigurationPath = ResolveUrl ("~/CuteSoft_Client/CuteEditor/Configuration/AutoConfigure/asset.config");
            }

            txtBody.Focus = true;
        }

        /// <summary>
        /// Remove from channel
        /// </summary>
        protected void btnRemoveChannel_Click (Object sender, EventArgs e)
        {
            // Only the owner can delete it
            if (StoreUtility.IsAssetOwner (GetUserId (), _assetId))
            {
                int channelId = Convert.ToInt32 (hidChannelId.Value);

                StoreUtility.RemoveAssetFromChannel (GetUserId (), _assetId, channelId);
            }

            BindAssetChannels (_assetId);
        }

        /// <summary>
        /// dgrdAttributes_ItemDataBound
        /// </summary>
        private void dgrdAttributes_ItemDataBound (object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                // Get data for that year and region and create data set 
                TextBox txtAttributeValueMultiline = (TextBox) e.Item.FindControl ("txtAttributeValueMultiline");
                if (null != txtAttributeValueMultiline)
                {
                    txtAttributeValueMultiline.Attributes.Add ("MaxLength", "255");
                }
            }
        }

        private void cvDlCategories_ServerValidate (object source, ServerValidateEventArgs args)
        {
            bool valid = false;
            //must select at least one category
            foreach (DataListItem dliCategory in dlCategories.Items)
            {
                CheckBox chkCategory = (CheckBox) dliCategory.FindControl ("chkCategory");

                if (chkCategory.Checked)
                {
                    valid = true;
                    break;
                }
            }
            args.IsValid = valid;
        }

        #endregion Event Handlers


        #region Properties

        protected string returnURL
        {
            get 
            {
                if (ViewState["ReturnUrl"] != null)
                {
                    return ViewState["ReturnUrl"].ToString ();
                }
                else
                {
                    return ResolveUrl ("~/watch/watch.kaneva");
                }
            }
            set 
            {
                ViewState["ReturnUrl"] = value;
            }
        }

        #endregion Properties


        #region Declerations

        // License stuff
        protected HtmlInputRadioButton optPublic, optCC, optPersonal, optCommercial, optOther;
        protected DropDownList drpCCLicense;
        protected TextBox txtPerAdditional, txtCommAdditional, txtLicenseName, txtLicenseURL;
        protected HtmlInputFile inpAvatar;
        protected Label lblAssetSize;
		protected Literal lblItemType;

        protected TextBox txtTeaser;
        protected CuteEditor.Editor txtBody;
        protected TextBox txtItemName, txtKeywords, txtImageCaption;
        protected Label lblCreatedBy, lblPrice, lblAssetId;

        protected CheckBox chkSticky;

        protected HtmlImage imgPreview, imgPreview2, imgPreview3;

        protected DropDownList drpType, drpPermission;
		protected CheckBox chkMature;

        protected Label lblItemName;
        protected HtmlContainerControl spnAlertMsg, spnMatureDesc;

        protected HtmlInputButton btnUpload, btnUploadImage, btnUploadIcon;
        protected Button btnClearThumb, btnClearIcon, DeleteAsset;

        protected Button btnDeleteScreenshot;
        protected HtmlInputHidden GameLicId;

        protected HtmlInputRadioButton optPayGame, optFreeGame;

        protected Repeater dgrdAttributes;

        protected CheckBox chkAdvanced;

		protected HyperLink back;

		// Categories
		protected DataList dlCategories;
		protected int m_category1, m_category2, m_category3;

		// Thumbnail
		protected HtmlTableRow trAssetType;
        protected HtmlTable tblThumb;

        // Game info
		protected RegularExpressionValidator regtxtKeywords;
        protected HtmlTable tblGameInstructions;
        protected HtmlTableRow trCompanyName;
        protected TextBox txtGameInstructions, txtCompanyName;

		// Channels
		protected Repeater rptChannels;
		protected Button btnRemoveChannel;
		protected HtmlInputHidden hidChannelId;

		// License Info
		protected Label lblLicense, lblLicenseURL;

		protected CustomValidator cvDlCategories;

		// Logger
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);
		protected ValidationSummary valSum;
		protected RequiredFieldValidator rfdrpType;
		protected RequiredFieldValidator rfItemName;
		protected RequiredFieldValidator rfdrpPermission;
		protected RangeValidator rngAmount;
		protected RequiredFieldValidator rftxtKeywords;
		protected LinkButton lnkAddChannel;
		protected Button btnUpdate, btnCancel;

		private int _assetId;

        #endregion Declerations


        #region Web Form Designer generated code

        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
        
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {    
			this.cvDlCategories.ServerValidate += new System.Web.UI.WebControls.ServerValidateEventHandler(this.cvDlCategories_ServerValidate);
			this.dgrdAttributes.ItemDataBound += new System.Web.UI.WebControls.RepeaterItemEventHandler(this.dgrdAttributes_ItemDataBound);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
