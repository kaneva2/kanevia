///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for publishOverview.
	/// </summary>
	public class publishOverview : MainTemplatePage
	{
		protected publishOverview () 
		{
			Title = "Publish Overview";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
            string status301 = "301 Moved Permanently";
            Response.Status = status301;
            Response.AddHeader ("Location", "http://support.kaneva.com/ics/support/default.asp?deptID=5433");
            Response.End ();

            //joinLink.HRef = ResolveUrl(KanevaGlobals.JoinLocation);
			// Put user code to initialize the page here
		}

        protected HtmlAnchor joinLink;

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion


	}
}
