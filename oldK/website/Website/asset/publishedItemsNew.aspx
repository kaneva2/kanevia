<%@ Page language="c#" Codebehind="publishedItemsNew.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.publishedItemsNew" %>
<%@ Register TagPrefix="Kaneva" TagName="StoreFilter" Src="../usercontrols/StoreFilter.ascx" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../usercontrols/Pager.ascx" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>

<LINK href="../css/home.css" type="text/css" rel="stylesheet">
<link href="../css/shadow.css" rel="stylesheet" type="text/css">
<link href="../css/new.css" rel="stylesheet" type="text/css">
<LINK href="../css/kanevaText.css" type="text/css" rel="stylesheet">
<LINK href="../css/kanevaSC.css" type="text/css" rel="stylesheet">
<LINK href="../css/base/buttons_new.css" type="text/css" rel="stylesheet"> 
<script type="text/javascript" src="../jscript/xEvent.js"></script>
<script type="text/javascript" src="../jscript/balloon.js"></script>
<script src="../jscript/yahoo-dom-event.js"></script> 
<script src="../jscript/prototype.js" type="text/javascript" language="javascript"></script>
 
<script type="text/JavaScript">
	function changeBG(obj) {
		obj.style.background = "url('../images/media/tabsMedia.gif')";
	}
	function changeBG2(objeto) {
		objeto.style.background = "url('../images/media/tabsMedia_on.gif')";
	}
	function changeBGB(objeto) {
		objeto.style.background = "url('../images/media/tabsMedia2.gif')";
	}
	function changeBGB2(objeto) {
		objeto.style.background = "url('../images/media/tabsMedia2_on.gif')";
	}
				
	function CheckAll() 
	{
		Select_All(document.getElementById("cbxSelectAll").checked);
	}

	// white balloon with mostly default configuration
	// (see http://www.wormbase.org/wiki/index.php/Balloon_Tooltips)
	var whiteBalloon    = new Balloon;
	whiteBalloon.balloonTextSize  = '100%';

	// white ballon with some custom config:
	var whiteBalloonSans  = new Balloon;
	whiteBalloonSans.upLeftConnector    = '../images/balloons/balloonbottom_sans.png';
	whiteBalloonSans.upRightConnector   = '../images/balloons/balloonbottom_sans.png';
	whiteBalloonSans.downLeftConnector  = '../images/balloons/balloontop_sans.png';
	whiteBalloonSans.downRightConnector = '../images/balloons/balloontop_sans.png';
	whiteBalloonSans.upBalloon          = '../images/balloons/balloon_up_top.png';
	whiteBalloonSans.downBalloon        = '../images/balloons/balloon_down_bottom.png';
	whiteBalloonSans.paddingConnector = '22px';
					   
</script>
<style>
	img { behavior: url(../css/iepngfix.htc); }
	a.btnlarge {font-size:13px;padding-top:6px;}
</style>

<div id="divLoading">
	<table height="450" cellSpacing="0" cellPadding="0" width="100%" border="0" id="Table1">
		<tr>
			<th class="loadingText" id="divLoadingText">
				Loading...</th></tr>
	</table>
</div>
<div id="divData" style="DISPLAY: none">
	<ajax:ajaxpanel id="ajMediaLibrary" runat="server">
		<!-- Main Body structure table -->
		<TABLE cellSpacing="0" cellPadding="0" width="990" align="center" border="0">
			<TR>
				<TD>
					<TABLE class="newcontainerborder" cellSpacing="0" cellPadding="0" width="1004" border="0" style="background:#fff;border:1px solid #ccc;border-top:none;">
						<TR>
							<TD class=""></TD>
							<TD class=""></TD>
							<TD class=""></TD>
						</TR>
						<TR>
							<TD class=""><IMG height="1" runat="server" src="~/images/spacer.gif" width="1"></TD>
							<TD class="" vAlign="top" style="padding:20px 12px 40px 12px;">
								<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0"> 
								<!-- BEGIN TOP STATUS BAR -->
									<TR>
										<TD>
											<DIV id="pageheader">
												<TABLE cellSpacing="0" cellPadding="0" width="99%" border="0">
													<TR>
														<TD align="left">
															<SPAN id="spanMediaLibrary" runat="server">
																<asp:Label id="lblChannelName" runat="server"></asp:Label>
																<H1>Media Library</H1>
															</SPAN>
															<SPAN id="spanMyMediaLibrary" runat="server">
																<H1>My Media Library</H1>
															</SPAN>
														</TD>
														<TD vAlign="middle" align="right">
															<SPAN id="ajpTopStatusBar$RBS_Holder">
																<SPAN id="ajpTopStatusBar" AjaxCall="async">
																	<TABLE cellSpacing="0" cellPadding="0" width="700" border="0">
																		<TR> 
																			<!-- Begin All Sort Filter --->
																			<TD align="left">
																				<TABLE id="filterTop_tblFilter" cellSpacing="2" cellPadding="2" width="100%" border="0">
																					<TR>
																						<TD align="left">
																							<Kaneva:StoreFilter id="pageSort" runat="server" AssetType="0" HideAlphaNumericsPullDown="false" HideAlphaNumerics="true"
																								HideItemsPerPagePullDown="true" HideThumbView="True" IsAjaxMode="True"></Kaneva:StoreFilter></TD>
																					</TR>
																				</TABLE>
																			</TD> 
																			<!-- End All Sort Filter ---> 
																			<!-- Begin Community Sort Filter --->
																			<TD id="tdFilterByChannel" align="center" runat="server">
																				<TABLE id="filterTop_tblFilter" cellSpacing="0" cellPadding="0" border="0">
																					<TR>
																						<TD align="center"><SPAN class="insideTextNoBold">Community:</SPAN>
																							<asp:DropDownList id="ddlChannels" class="formKanevaSelect" style="width:160px;" runat="server" AutoPostBack="True"></asp:DropDownList></TD>
																					</TR>
																				</TABLE>
																			</TD> 
																			<!-- End Community Sort Filter ---> 
																			<!-- Begin Show Filter --->
																			<TD align="left">
																				<TABLE id="filterTop_tblFilter" cellSpacing="0" cellPadding="0" width="100%" border="0">
																					<TR>
																						<TD align="center">
																							<Kaneva:StoreFilter id="filStore" runat="server" AssetType="0" HideAlphaNumerics="true" HideThumbView="True"
																								IsAjaxMode="True">
																							</Kaneva:StoreFilter>
																						</TD>
																					</TR>
																				</TABLE>
																			</TD> 
																			<!-- End Show Filter ---> 
																			<!-- Begin page results Filter --->
																			<TD class="searchnav" noWrap align="right" width="168">
																				<asp:label id="lblSearch" runat="server" cssclass="insideTextNoBold"></asp:label><BR>
																				<Kaneva:Pager id="pgTop" MaxPagesToDisplay="5" runat="server" IsAjaxMode="True">
																				</Kaneva:Pager>
																			</TD> 
																			<!-- End page results Filter --->
																		</TR>
																	</TABLE>
																</SPAN>
															</SPAN>
														</TD>
													</TR>
												</TABLE>
											</DIV>
										</TD>
									</TR> 
									<!-- END TOP STATUS BAR --> 
									<!-- BEGIN MAIN BODY -->
									<TR>
										<TD vAlign="top" align="left" width="968">
											<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
												<TR> <!-- Start Side Panel Controls -->
													<TD vAlign="top" align="center" width="246">
														<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
															<TR>
																<TD align="center">
																	<!-- smart nav div-->
																	<DIV class="module">
																																											
																		<SPAN class="ct">
																			<SPAN class="cl"></SPAN>
																		</SPAN><h2>Library</h2>
																		<UL id="librarynav">
																			<LI id="all" runat="server">
																				&nbsp;<asp:LinkButton id="hlAll" runat="server" CommandName="all" OnCommand="SmartNav_Command"><span class="navicon"></span>All<span><asp:Literal ID="ltrAllCount" runat="server" /></span></asp:LinkButton>
																			</LI>
																			<LI id="videos" runat="server">
																				&nbsp;<asp:LinkButton id="hlVideo" runat="server" CommandName="video" OnCommand="SmartNav_Command"><span class="navicon"></span>Videos<span><asp:Literal ID="ltrVideoCount" runat="server" /></span></asp:LinkButton>
																			</LI>
																			<LI id="tv" runat="server">
																				&nbsp;<asp:LinkButton id="hlTV" runat="server" CommandName="tv" OnCommand="SmartNav_Command"><span class="navicon"></span>TV Channels<span><asp:Literal ID="ltrTVCount" runat="server" /></span></asp:LinkButton>
																			</LI>
																			<LI id="photos" runat="server">
																				&nbsp;<asp:LinkButton id="hlPhoto" runat="server" CommandName="picture" OnCommand="SmartNav_Command"><span class="navicon"></span>Photos<span><asp:Literal ID="ltrPhotoCount" runat="server" /></span></asp:LinkButton>
																			</LI>
																			<LI id="music" runat="server">
																				&nbsp;<asp:LinkButton id="hlMusic" runat="server" CommandName="music" OnCommand="SmartNav_Command"><span class="navicon"></span>Music<span><asp:Literal ID="ltrMusicCount" runat="server" /></span></asp:LinkButton>
																			</LI>
																			<LI id="widgets" runat="server">
																				&nbsp;<asp:LinkButton id="hlWidgets" runat="server" CommandName="widgets" OnCommand="SmartNav_Command"><span class="navicon"></span>Flash Widgets<span><asp:Literal ID="ltrWidgetsCount" runat="server" /></span></asp:LinkButton>
																			</LI>
																			<LI id="games" runat="server">
																				&nbsp;<asp:LinkButton id="hlGames" runat="server" CommandName="game" OnCommand="SmartNav_Command"><span class="navicon"></span>Games<span><asp:Literal ID="ltrGamesCount" runat="server" /></span></asp:LinkButton>
																			</LI>
																			<LI id="patterns" runat="server">
																				&nbsp;<asp:LinkButton id="hlPatterns" runat="server" CommandName="pattern" OnCommand="SmartNav_Command"><span class="navicon"></span>Patterns<span><asp:Literal ID="ltrPatternCount" runat="server" /></span></asp:LinkButton>
																			</LI>
																								
																			<LI id="shared" runat="server">
																				<SPAN id="pnlConnectedMedia" runat="server">&nbsp;<asp:LinkButton id="hlConnectedMedia" runat="server" CommandName="shared" OnCommand="SmartNav_Command"><span class="navicon"></span>Shared Media<span><asp:Literal ID="ltrConnectedMediaCount" runat="server" /></span></asp:LinkButton></SPAN>
																			</LI>
																								
																		</UL>
																		<SPAN class="cb">
																			<SPAN class="cl"></SPAN>
																		</SPAN>
																						
																	</DIV> 
																	<!-- upload div-->
																	<DIV class="module whitebg">
																		<SPAN class="ct">
																			<SPAN class="cl"></SPAN>
																		</SPAN>
																		<h2>Browse All Media</h2>
																		<ul id="browsenav">
																			<LI id="photos1" runat="server">
																				&nbsp;<a href="~/watch/watch.kaneva?type=5" id="aPhoto1" runat="server" ><span class="navicon"></span>Photos<span></span></a>
																			</LI>
																			<LI id="videos1" runat="server">
																				&nbsp;<a href="~/watch/watch.kaneva?type=2" id="aVideo1" runat="server"><span class="navicon"></span>Videos<span></span></a>
																			</LI>
																		</ul>		
																		<SPAN class="cb">
																			<SPAN class="cl"></SPAN>
																		</SPAN>
																	</DIV> 
																	<!-- user playlist div-->
																	<DIV class="module playlistbox">
																		<SPAN class="ct">
																			<SPAN class="cl"></SPAN>
																		</SPAN><h2>My Playlists</h2>
																		<TABLE cellSpacing="0" cellPadding="0" width="99%" border="0">
																			<TR id="trPlayListAdd" runat="server">
																				<TD align="center">
																					<asp:LinkButton id="lnkNewPlaylist" class="btnlarge grey" runat="server" CommandName="cmdAddChannel" CausesValidation="False" Text="Create a New Playlist"></asp:LinkButton>
																				</TD>
																			</TR>																								
																			<TR id="trEditPlaylist" runat="server">
																				<TD align="middle">
																					<input type="hidden" runat="server" id="hdnPlayListId" NAME="hdnPlayListId">
																					<h3><span><asp:Label id="playListTitle" runat="server" /></span><asp:Label id="playListDescription" style="font-weight: normal" runat="server" /> <br/><asp:Label id="shuffleText" style="font-weight: normal" runat="server" /><asp:LinkButton ID="editPlayList" runat="server" Text="Edit" style="padding-left:10px; font-weight: normal" /></h3>
																				</TD>
																			</TR>

																			<tr><td height="10px">&nbsp;</td></tr>

																			<TR id="trPlayListvalues" runat="server">
																				<TD align="center">
																					<table border="0" cellpadding="0" cellspacing="0" width="100%">
																						<tr>
																							<td align="right">
																								<span style="vertical-align: middle;" class="bodyText" id="spnGN" visible="false">Playlist Name: </span>
																							<TD>
																								<asp:TextBox id="tbxPlayListName" style="font-family: verdana" width="150px" runat="server"></asp:TextBox></TD>
																							</TD>
																						</tr>
																						<tr>
																							<td align="right">
																								<span style="vertical-align: middle;" class="bodyText" id="Span1" visible="false">Description: </span>
																							<TD>
																								<asp:TextBox id="tbxPlayListDescription" style="font-family: verdana" width="150px" runat="server" MaxLength="100" TextMode="MultiLine"></asp:TextBox></TD>
																							</TD>
																						</tr>
                                                                                        <tr>
																							<td align="right">
																								<span style="vertical-align: middle;" class="bodyText" id="Span2" visible="false">Shuffle: </span>
																							<TD>
																								    <asp:RadioButtonList id="rblShuffle" runat="server" RepeatDirection="Horizontal">
																										<asp:ListItem title="Off" Value="0">Off</asp:ListItem>
                                                                                                        <asp:ListItem title="On" Value="1">On</asp:ListItem>
																									</asp:RadioButtonList></TD>
																							</TD>
																						</tr>
																						<tr>
																							<td>
																								&nbsp;
																							</TD>
																							<TD height="40px">
																								<asp:Button id="btnSavePlayList" runat="server" Text="Save"></asp:Button>
																								<asp:Button id="btnCancel" runat="server" Text="Cancel"></asp:Button>
																							</TD>
																						</tr>
																					</table>
																				</td>
																			</TR>																								
																			<TR>
																				<TD colSpan="2">
																					<UL id="playlistnav" runat="server">
																						<asp:Repeater id="rptPlayList" runat="server">
																							<ItemTemplate>
																								<LI id="Li1" runat="server">
																									&nbsp;
																									<asp:LinkButton ID="lbnPlayListItem" CommandName="getPlayList" CausesValidation="False" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "AssetGroupId").ToString() %>' runat="server">
																										<span class="navicon"></span><%#TruncateWithEllipsis(Server.HtmlDecode (DataBinder.Eval(Container.DataItem, "Name").ToString()), 22)%>
																										<span>
																											<asp:Label ID="Label1" runat="server" text='<%# GetPlayListCounts(DataBinder.Eval(Container.DataItem, "AssetCount").ToString())%>' />
																											<input type="hidden" runat="server" id="hdnPlayListDescription" value='<%#Server.HtmlDecode (DataBinder.Eval(Container.DataItem, "Description").ToString())%>' NAME="hdnPlayListDescription">
																											<input type="hidden" runat="server" id="hdnPlayListName" value='<%#Server.HtmlDecode (DataBinder.Eval(Container.DataItem, "Name").ToString())%>' NAME="hdnPlayListName">
                                                                                                            <input type="hidden" runat="server" id="hdnShuffle" value='<%#Server.HtmlDecode (DataBinder.Eval(Container.DataItem, "Shuffle").ToString())%>' NAME="hdnShuffle">
																										</span>
																									</asp:LinkButton>  
																								</LI>
																							</ItemTemplate>
																						</asp:Repeater>
																					</UL>
																				</TD>
																			</TR>
																							
																		</TABLE>
																		<SPAN class="cb">
																			<SPAN class="cl"></SPAN>
																		</SPAN>
																	</DIV>
																</TD>
															</TR>
														</TABLE>
													</TD> <!-- end Side Panel Controls -->
													<TD width="14">
														<IMG height="1" runat="server" src="~/images/spacer.gif" width="14">
													</TD> <!-- BEGIN MEDIA DISPLAY AREA -->
													<TD vAlign="top" width="708">
														<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
															<TR>
																<TD><!--START RIGHTCOL-->
																	<DIV class="module whitebg">
																		<SPAN class="ct">
																			<SPAN class="cl"></SPAN>
																		</SPAN>
																		<table cellSpacing="0" cellPadding="0" width="98%" border="0">
																			<tr style="height:36px">
																				<td align="left" valign="top">
																					<h2><asp:label runat="server" id="lblSelectedMediaList" /></h2>
																					<asp:label style="position:relative; top:-6" runat="server" id="messages" visible="true" />
																				</td>
																				<td align="right" valign="top"><A id="aUpload" class="btnlarge orange" runat="server" title="Upload or add items to Media Library." >Add Media to Library</A></td>
																			</tr>
																		</table>
																							
																		<TABLE id="controlbox" cellSpacing="0" cellPadding="0" width="99%" border="0"> 
																			<!-- Begin Media management controls -->
																			<TR>
																				<!-- delete selected media (button) -->
																				<TD align="center">
																					<TABLE id="filterTop_tblFilter" cellSpacing="0" cellPadding="0" width="100%" border="0">
																						<TR> 
																							<TD align="left">																													
																								<asp:LinkButton class="button" id="lnkDeleteMedia" title="Delete selected items." runat="server" CausesValidation="False" Text="Delete">Delete
																									<img src="../images/btnico_delete.gif" title="Delete selected items." width="24" height="24" border="0">
																								</asp:LinkButton>
																								<asp:LinkButton class="button" id="lnkRemoveFromPlayList" title="Remove selected items from a playlist." runat="server" CausesValidation="False" Text="Remove">Remove
																									<img src="../images/btnico_delete.gif" title="Remove selected items from a playlist." width="24" height="24" border="0">
																								</asp:LinkButton>
																							</TD> 
																						</tr>
																					</table> 
																				</td>
																				<!-- add selected items to playlist -->
																				<TD align="center">
																					<TABLE id="filterTop_tblFilter" cellSpacing="0" cellPadding="0" width="100%" border="0">
																						<TR>
																							<TD align="left">
																								<asp:DropDownList id="ddlFiles2PlayList" title="Add selected items to a playlist." width="150px" style="margin-top:3px;" runat="server" AutoPostBack="False"></asp:DropDownList>
																								<asp:LinkButton class="nohover" id="lbnFiles2PlayList" title="Add selected items to a playlist." runat="server" CausesValidation="False">
																									<img src="../images/btnico_add.gif" width="24" title="Add selected items to a playlist." height="24" border="0" />
																								</asp:LinkButton>
																							</TD>
																							<td align="left">
																								<asp:LinkButton class="button" id="lnkDeletePlayList" title="Delete current playlist." runat="server" CausesValidation="False" Text="Delete This PlayList">Delete This PlayList
																									<img href="#" src="../images/btnico_delete.gif" title="Delete current playlist." width="24" height="24" border="0">
																								</asp:LinkButton>
																							</td>
																						</TR>
																					</TABLE>
																				</TD> 
																				<!-- edit multiple items --->
																				<TD align="center">
																					<TABLE id="filterTop_tblFilter" cellSpacing="0" cellPadding="0" width="100%" border="0">
																						<TR>
																							<TD align="left">
																								<asp:DropDownList id="ddlEditMultiples" title="Edit selected items." width="200px" style="margin-top:3px;" runat="server" AutoPostBack="False" />
																								<asp:LinkButton class="nohover" id="lbnEditMultiple" title="Edit selected items." runat="server" CausesValidation="False">
																									<img src="../images/btnico_go.gif" width="24" title="Edit selected items." height="24" border="0" />
																								</asp:LinkButton>
																							</td>
																						</TR>
																					</TABLE>
																				</TD>
																				<!-- add to community -->
																				<TD align="center">
																					<TABLE id="filterTop_tblFilter" cellSpacing="0" cellPadding="0" width="100%" border="0">
																						<tr>
																							<td align="left">
																								<asp:LinkButton class="button" id="lnkAddChannel" title="Add selected items to communities." runat="server" CommandName="cmdAddChannel" CausesValidation="False" Text="Add to Community">Add to Community
																									<img src="../images/btnico_add.gif" width="24" title="Add selected items to communities." height="24" border="0">
																								</asp:LinkButton>
																							</td>
																						</tr>
																					</table>
																				</TD>
																			</TR>
																		</TABLE>
																		<!-- END Media management controls -->
																		<!-- Begin Media display region -->
																		<table width="99%" cellspacing="0" cellpadding="0" border="0" class="data">
																			<tr>
																				<td colspan="6" height="20px">
																					<table width="99%" cellspacing="0" cellpadding="0" border="0" height="34px">
																						<tr>
																							<td align="left" valign="top">
																                                <!-- basic search -->
																                                <table cellpadding="0" cellspacing="0" border="0" width="92%">
																	                                <tr>
																		                                <td width="80px"><h4>Find <span id="spnFindTitle" runat="server">Media</span></h4></td>
																		                                <td width="305px" align="left">
																		                                    <asp:textbox id="txtKeywords" class="formKanevaText" style="width:300px;" maxlength="100" runat="server"></asp:textbox>
																			                                <span id="spnSearchInst" runat="server"></span>
																		                                </td>
																		                                <td align="left"><asp:linkbutton runat="server" class="btnxsmall grey" id="btnSearch" text="Search" onclick="btnSearch_Click"></asp:linkbutton></td>
																	                                </tr>
																                                </table>
																							</td>
																							<td align="right" width="100px" height="24px">
																								<asp:LinkButton class="button" id="lbnSaveSortOrder" runat="server" CausesValidation="False" AlternateText="Save Sort Order">
																									<asp:Literal ID="litSaveSortButtonImage" runat="server" />
																								</asp:LinkButton>&nbsp;
																							</td>
																						</tr>
																					</table> 
																				</td>
																			</tr>
																							
																			<tr>
																				<TD width="15" align="left" bgcolor="#fdfdfc">
																					<asp:checkbox id="cbxSelectAll" onclick="CheckAll();" CssClass="Filter2" runat="server" />
																				</TD>
																				<td runat="server" id="tdOrderHeading" align="left" bgcolor="#fdfdfc" width="20px">Order</td>
																				<td align="left" bgcolor="#fdfdfc">Name</td>
																				<td align="left" bgcolor="#fdfdfc">Type</td>
																				<td align="left" bgcolor="#fdfdfc" width="225">Details</td>
																				<td align="left" bgcolor="#fdfdfc"></td>
																			</tr>
																			<asp:Repeater id="rptAssets" runat="server">
																				<ItemTemplate>
																					<tr>
																						<td align="center">
																							<span style="horizontal-align: right;">
																								<asp:checkbox id="chkEdit" CssClass="Filter2" runat="server" Visible='<%#!IsDeleted (Convert.ToInt32 ( DataBinder.Eval(Container.DataItem, "status_id")))%>'/>
																								<input type="hidden" runat="server" id="hidAssetId" value='<%#DataBinder.Eval(Container.DataItem, "asset_id")%>' NAME="hidAssetId">
																							</span>
																						</td>
																						<td align="center" runat="server" id="tdOrder" Visible='<%# ShowOrderField() %>'>
																							<span style="horizontal-align: middle;">
																								<asp:TextBox id="tbxSortOrder" Visible='<%# ShowOrderField() %>' width="30px" runat="server"></asp:TextBox>
																								<input type="hidden" runat="server" id="hidSortOrig" NAME="hidSortOrig">
																							</span>
																						</td>
																						<td>
																							<!--image holder-->
																							<table border="0" cellpadding="0" cellspacing="0" width="100%" class="nopadding">
																								<tr>
																									<td align="right" width="36">
																										<div class="framesize-tiny">
																											<div class="frame">
																												<div class="restricted" runat="server" visible='<%# Convert.ToInt32(DataBinder.Eval(Container.DataItem, "mature_profile")).Equals(1) %>' ID="Div2"></div>
																												<span class="ct">
																													<span class="cl"></span>
																												</span>
																												<div class="imgconstrain">
																													<a runat="server" id="thumbnail" href='<%# GetAssetDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "asset_id")))%>' >
																														<img src='<%#GetMediaImageURL (DataBinder.Eval(Container.DataItem, "thumbnail_small_path").ToString () ,"sm", Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_id")), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_type_id")), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "thumbnail_gen")))%>' border="0"/>
																													</a>
																												</div>
																												<span class="cb">
																													<span class="cl"></span>
																												</span>
																											</div>
																										</div>
																									</td>
																									<td>
																										<span class="insideTextNoBold"></span>&nbsp;<a runat="server" id="mediaTitle" style="font-size:110%; text-decoration:none" href='<%# GetAssetDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "asset_id")))%>'><%# TruncateWithEllipsis (Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "name").ToString ()), 30) %></a>
																									</td>
																								</tr>
																							</table>
																							<asp:Label ID="lblMessage" runat="server" />
																							<!--end image holder-->
																						</td>
																						<td align="center">
																							<span style="horizontal-align: right;">
																								<img border="0" alt='<%#GetAssetTypeName(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "asset_type_id")))%>' title='<%#GetAssetTypeName(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "asset_type_id")))%>' src='<%#GetAssetTypeIcon(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "asset_type_id")))%>' border="0"/>
																							</span>
																							</span>
																						</td>
																						<td valign="middle" align="left">
																							<span class="insideTextNoBold">Status:&nbsp;<A runat="server" HREF='#' id="hlStatus" />&nbsp;<A runat="server" HREF='#' style="color:red" id="hlAddData" /></span><span class="insideTextNoBold">
																								<br />Added on: <%# FormatDateTime (DataBinder.Eval (Container.DataItem, "date_added")) %></span>
																								<br />
																						</td>
																						<td align="center">
																							<asp:LinkButton id="lnkEdit" Text="Edit" ToolTip="Edit" CommandName="cmdEdit" runat="server" style="text-decoration:underline;" />
																						</td>
																					</tr>
																				</ItemTemplate>
																				<AlternatingItemTemplate>
																					<tr class="altrow">
																						<td align="center" >
																							<span style="horizontal-align: right;">
																								<asp:checkbox id="chkEdit" CssClass="Filter2" runat="server" Visible='<%#!IsDeleted (Convert.ToInt32 ( DataBinder.Eval(Container.DataItem, "status_id")))%>'/>
																								<input type="hidden" runat="server" id="hidAssetId" value='<%#DataBinder.Eval(Container.DataItem, "asset_id")%>' NAME="hidAssetId">
																							</span>
																						</td>
																						<td align="center" runat="server" id="tdOrder" Visible='<%# ShowOrderField() %>'>
																							<span style="horizontal-align: right;">
																								<asp:TextBox id="tbxSortOrder" width="30px" Visible='<%# ShowOrderField() %>' runat="server"></asp:TextBox>
																								<input type="hidden" runat="server" id="hidSortOrig" NAME="hidSortOrig">
																							</span>
																						</td>
																						<td>
																							<!--image holder-->
																							<table border="0" cellpadding="0" cellspacing="0" width="100%" class="nopadding">
																								<tr>
																									<td align="right" width="36">
																										<div class="framesize-tiny">
																											<div class="frame">
																												<div class="restricted" runat="server" visible='<%# Convert.ToInt32(DataBinder.Eval(Container.DataItem, "mature_profile")).Equals(1) %>' ID="Div1"></div>
																												<span class="ct">
																													<span class="cl"></span>
																												</span>
																												<div class="imgconstrain">
																													<a runat="server" id="thumbnail" href='<%# GetAssetDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "asset_id")))%>'>
																														<img src='<%#GetMediaImageURL (DataBinder.Eval(Container.DataItem, "thumbnail_small_path").ToString () ,"sm", Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_id")), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_type_id")), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "thumbnail_gen")))%>' border="0"/>
																													</a>
																												</div>
																												<span class="cb">
																													<span class="cl"></span>
																												</span>
																											</div>
																										</div>
																									</td>
																									<td>
																										<span class="insideTextNoBold"></span>&nbsp;<a runat="server" id="mediaTitle" style="font-size:110%; text-decoration:none" href='<%# GetAssetDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "asset_id")))%>'><%# TruncateWithEllipsis (Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "name").ToString ()), 30) %></a>
																									</td>
																								</tr>
																							</table>
																							<asp:Label ID="lblMessage" runat="server" />
																							<!--end image holder-->
																						</td>
																						<td align="center" >
																							<span style="horizontal-align: right;">
																								<img border="0" alt='<%#GetAssetTypeName(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "asset_type_id")))%>' title='<%#GetAssetTypeName(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "asset_type_id")))%>' src='<%#GetAssetTypeIcon(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "asset_type_id")))%>' border="0"/>
																							</span>
																							</span>
																						</td>
																						<td valign="middle" align="left">
																							<span class="insideTextNoBold">Status:&nbsp;<A runat="server" HREF='#' id="hlStatus" />&nbsp;<A runat="server" HREF='#' style="color:red" id="hlAddData" /></span><span class="insideTextNoBold">
																								<br />Added on: <%# FormatDateTime (DataBinder.Eval (Container.DataItem, "date_added")) %></span>
																								<br />
																						</td>
																						<td align="center">
																							<asp:LinkButton id="lnkEdit" Text="Edit" ToolTip="Edit" CommandName="cmdEdit" runat="server" style="text-decoration:underline;" />
																						</td>
																					</tr>
																				</AlternatingItemTemplate>
																			</asp:Repeater>
																								
																			<tr>
																				<td colspan="6" height="20px">
																					<table width="99%" cellspacing="0" cellpadding="0" border="0" height="34px">
																						<tr>
																							<td><asp:label runat="server" id="Label2" style="padding-left: 2px" /></td>
																							<td align="right" width="100px" height="24px">
																								<asp:LinkButton class="button" id="lbnSaveSortOrder2" runat="server" CausesValidation="False" AlternateText="Save Sort Order">
																									<asp:Literal ID="litSaveSortButtonImage2" runat="server" />
																								</asp:LinkButton>&nbsp;
																							</td>
																						</tr>
																					</table> 
																				</td>
																			</tr>
																																																
																			</TABLE>
																		<SPAN class="cb">
																			<SPAN class="cl"></SPAN>
																		</SPAN>
																	</DIV> 
																<!--END RIGHTCOL-->
																</TD>
															</TR>
														</TABLE>
													</TD> 
													<!-- END MEDIA DISPLAY AREA -->
												</TR>
											</TABLE>
										</TD>
									</TR> 
														
									<TR>
										<TD vAlign="top" align="right"><IMG height="10" runat="server" src="~/images/spacer.gif" width="1">
											<asp:label id="lblSearch2" runat="server" cssclass="insideTextNoBold"></asp:label><BR>
											<Kaneva:Pager id="pgBottom" MaxPagesToDisplay="5" runat="server" IsAjaxMode="True"></Kaneva:Pager><!-- Hidden button to handle the click event when user clicks on a page link--></TD>
									</TR>
								</TABLE>
											
							</TD>
							<TD class=""><IMG id="Img5" height="1" runat="server" src="~/images/spacer.gif" width="1"></TD>
						</TR>
						<TR>
							<TD class=""></TD>
							<TD class=""></TD>
							<TD class=""></TD>
						</TR>
					</TABLE>
				</TD>
			</TR>
		</TABLE>
		<!-- END MAIN BODY -->
		<INPUT id="AssetId" type="hidden" value="0" runat="server" NAME="AssetId">
		<asp:button id="DeleteAsset" onclick="Delete_Asset" runat="server" Visible="false"></asp:button>

	</ajax:ajaxpanel>
</div>
