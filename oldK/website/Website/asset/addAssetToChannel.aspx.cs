///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.IO;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using log4net;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for addAssetToChannel.
	/// </summary>
	public class addAssetToChannel : MainTemplatePage
	{
		/// <summary>
		/// Constructor
		/// </summary>
		protected addAssetToChannel () 
		{
			Title = "Add item to communities"; 
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			if (!IsPostBack)
			{
                BindData (1);
			}
		}

        private void BindData (int pageNumber)
        {
            PagedDataTable pdtChannels = CommunityUtility.GetPublishChannels (GetUserId (), "name", pageNumber, _PGSIZE);
            
            rptChannels.DataSource = pdtChannels;
            rptChannels.DataBind ();

            // populate top pager
            pgTop.NumberOfPages = Math.Ceiling ((double) pdtChannels.TotalCount / _PGSIZE).ToString ();
            pgTop.CurrentPageNumber = pageNumber;
            pgTop.DrawControl ();

            // populate bottom pager
            pgBottom.NumberOfPages = pgTop.NumberOfPages;
            pgBottom.CurrentPageNumber = pageNumber;
            pgBottom.DrawControl ();

            // The results
            lblResults_Top.Text = lblResults_Top.Text = GetResultsText (pdtChannels.TotalCount, pageNumber, _PGSIZE, pdtChannels.Rows.Count);
            lblResults_Bottom.Text = lblResults_Top.Text;
        }

		/// <summary>
		/// AddToChannel
		/// </summary>
		protected void btnAddChannels_Click (Object sender, EventArgs e) 
		{
			CheckBox chkEdit;
			HtmlInputHidden hidChannelId;

			int assetId = Convert.ToInt32 (Request ["assetId"]);

			foreach (RepeaterItem rptChannel in rptChannels.Items)
			{
				chkEdit = (CheckBox) rptChannel.FindControl ("chkEdit");

				if (chkEdit.Checked)
				{
					hidChannelId = (HtmlInputHidden) rptChannel.FindControl ("hidChannelId");
					int channelId = Convert.ToInt32 (hidChannelId.Value);

					// Moderators are allowed to upload
					if(CommunityUtility.IsCommunityModerator (channelId, GetUserId()))
					{
						StoreUtility.InsertAssetChannel (assetId, channelId);
					}
					else
					{
						// Does the channel allow uploads?
						if (CommunityUtility.IsChannelPublishable (channelId))
						{
							StoreUtility.InsertAssetChannel (assetId, channelId);
						}
					}
				}
			}

			NavigateBackToBreadCrumb (1);	
		}

        /// <summary>
        /// Click event for Pager
        /// </summary>
        protected void pg_PageChange (object sender, PageChangeEventArgs e)
        {
            // get the inventory items
            BindData (e.PageNumber);
        }

		protected Repeater rptChannels;

        protected Label lblResults_Top, lblResults_Bottom;
        protected Pager pgTop, pgBottom;

        const int _PGSIZE = 20;



		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
