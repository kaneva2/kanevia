<%@ Page language="c#" Codebehind="imageUpload.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.imageUpload" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>		
		<meta name="description" content="KANEVA - The Canvas for Digital Entertainment">
		<meta name="keywords" content="games, movies, 3D, art, digital, entertainment, videos, shorts, trailers, rpg, mmo, fps, rts, gaming, gamers, machinima, computer, game">
		<link href="../css/Kaneva.css" rel="stylesheet" type="text/css"/>
		<link href="../css/home.css" rel="stylesheet" type="text/css" />
		<link href="../css/kanevaSC.css" rel="stylesheet" type="text/css" />
		<link href="../css/friends.css" rel="stylesheet" type="text/css">
		<link href="../css/kanevaBroadBand.css" rel="stylesheet" type="text/css" />
		<script type="text/JavaScript" src="../jscript/kaneva.js"></script>
		<script type="text/JavaScript" src="../jscript/rollover.js"></script>
		<script type="text/javascript">
			function resize(oBody){
				var iMySize = document.getElementById(oBody).scrollHeight;
				var leftC;
				var rightC;
				var compC;
				
				if(document.getElementById("leftColumn") != null && document.getElementById("rightColumn") != null) {
					leftC = document.getElementById("leftColumn").scrollHeight;
					rightC = document.getElementById("rightColumn").scrollHeight;
					
					compC = leftC - rightC;
					if(compC > 0){
						document.getElementById("hSpacer").style.height = (compC) + "px";
					}
				}
			}
		</script>
		<link rel="icon" href="http://www.kaneva.com/favicon.ico" type="image/x-icon" />
		<link rel="shortcut icon" href="http://www.kaneva.com/favicon.ico" type="image/x-icon" />
</head>
	<body OnLoad="self.focus(); document.forms.frmMain.btnUpload.disabled=true;" topmargin="0" leftmargin="0">
	<table border="0" cellpadding="0" cellspacing="0"  align="center">
	<form runat="server" method="post" enctype="multipart/form-data" name="frmMain" id="frmMain">
		<tr>
			<td valign="top">
				<!---CONTEN BG BORDER--->
				<table border="0" cellpadding="0" cellspacing="0" width="550" >
					<tr>
						<td class="frTopLeft"></td>
						<td class="frTop"></td>
						<td class="frTopRight"></td>
					</tr>
					<tr> 
						<td class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1"></td>
						<td valign="top" class="frBgInt1" align="left">
							<table border="0" cellpadding="0" cellspacing="0" >
								<tr><td colspan="3"><img runat="server" src="~/images/spacer.gif" width="1" height="5"></td></tr>
								<!--CONTENT-->
								<tr>
									<td><img runat="server" src="~/images/spacer.gif" width="4" height="1"></td>
									<td>
										<table  border="0" cellpadding="0" cellspacing="0" class="boxInside">
										  <tr>
											<td class="boxInsideTopLeft"><img runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
											<td class="boxInsideTop" ><img runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
											<td  class="boxInsideTopRight"><img runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
										  </tr>
										  <tr class="boxInside">
											<td class="boxInsideleft" ><img runat="server" src="~/images/spacer.gif" width="1" height="4" /></td>
											<td class="boxInsideContent" align="center">
												<table border="0" cellpadding="0" cellspacing="0" width="510" >
													<tr><td colspan="3"><img runat="server" src="~/images/spacer.gif" width="1" height="5"/></td></tr>
													<tr>
														<td><img runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
														<td   align="left" >
															<!--form-->
															<table width="500" border="0" cellpadding="0" cellspacing="0" class="bbBoxInside">
																<tr>
																  <td class="bbBoxInsideTopLeft"></td>
																  <td class="bbBoxInsideTop"></td>
																  <td class="bbBoxInsideTopRight"></td>
																</tr>
																<tr>
																	  <td height="97" class="bbBoxInsideLeft"><img runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
																  <td class="bbBoxInsideContent" align="center">
																   <table border="0" cellpadding="0" cellspacing="0" align="center" width="500" >
                                                                    <tr>
                                                                      <td colspan="3"><img runat="server" src="~/images/spacer.gif" width="1" height="10"/></td>
                                                                    </tr>
                                                                    <tr>
                                                                      <td ><img runat="server" src="~/images/spacer.gif" width="20" height="1"/></td>
                                                                      <td align="left" colspan="2"><img runat="server" src="~/images/Logok.gif" /></td>
                                                                    </tr>
																	<tr><td colspan="3"><img runat="server" src="~/images/spacer.gif" width="1" height="10"/></td></tr>
																	<tr>
																		<td><img runat="server" src="~/images/spacer.gif" width="20" height="1"/></td>
																		<td align="left" colspan="2"><img runat="server" src="~/images/uploadImageTitle.gif" /></td>
																	</tr>
																	<tr><td colspan="3"><img runat="server" src="~/images/spacer.gif" width="1" height="10"/></td></tr>																	
                                                                    <tr>
                                                                      <td ><img runat="server" src="~/images/spacer.gif" width="20" height="1"/></td>
                                                                      <td align="left" valign="top">
																	  	<table border="0" cellpadding="0" cellspacing="0">
																			<tr>
																				<td align="left" class="insideBoxText11" valign="top">Photo:</td>
																				 <td ><img runat="server" src="~/images/spacer.gif" width="10" height="1"/></td>
																				 <td><input runat="server" class="formKanevaText" ID="inpThumbnail" type="file" size="45" onFocus="document.forms.frmMain.btnUpload.disabled=false;" onClick="document.forms.frmMain.btnUpload.disabled=false;" /></td>
																				 <td ><img runat="server" src="~/images/spacer.gif" width="10" height="1"/></td>
		                                                                      	 <td  align="left">
																					<table border="0" cellpadding="0" cellspacing="0">
																						<tr>
																							<td align="left"></td>
																							<td><img runat="server" src="~/images/spacer.gif" width="7" height="1"/></td>
																							<td valign="middle"></td>
																						</tr>
																					</table>
																				</td>
																				</tr>
																				<tr><td><img runat="server" src="~/images/spacer.gif" width="1" height="5"/></td></tr>
		            	                                                        <tr>
    		            	                                                      <td   colspan="2"><img runat="server" src="~/images/spacer.gif" width="1" height="1"/></td>
            		        	                                                  <td  align="left" colspan="4">
																				  	<table border="0" cellpadding="0" cellspacing="0">
																						<tr>
																							<td class="inside" align="left" width="350"> Photos must be of the jpg, jpeg,gif and have a maximum file of 200KB</td>
																						</tr>
																					</table>
																				 </td>
                    		    	                                            </tr>
																			</table>
																		</td>
                    	                                                  <td ><img runat="server" src="~/images/spacer.gif" width="7" height="1"/></td>
		                                                               </tr>
        	                                                            <tr>
            	                                                          <td colspan="6" ><img runat="server" src="~/images/spacer.gif" width="1" height="5"/></td>
                	                                                    </tr>
        	                                                            <tr>
            	                                                          <td colspan="6"  class="underline" ><img runat="server" src="~/images/spacer.gif" width="1" height="10"/></td>
                	                                                    </tr>
        	                                                            <tr>
            	                                                          <td colspan="6" ><img runat="server" src="~/images/spacer.gif" width="1" height="5"/></td>
                	                                                    </tr>
                    	                                                <tr>
	                                                                      <td  colspan="6" align="right">
																		  	<table border="0" cellpadding="0" cellspacing="0">
	    	                                                                 <tr>	    	                                                                 
    	                                                                        <td><asp:button id="btnUpload" runat="Server" onClick="btnUpload_Click" class="Filter2" Text=" Upload "/></td>
        	                                                                    <td ><img runat="server" src="~/images/spacer.gif" width="5" height="1"/></td>
            	                                                                <td><input type="button" class="Filter2" name="cancel" value=" Cancel " onClick="window.close()"></td>
																				<td ><img runat="server" src="~/images/spacer.gif" width="12" height="8"/></td>
                                                                    	      </tr>
	                                                                      </table>
																		 </td>
                                                                    </tr>
                                                                    <tr>
                                                                      <td colspan="6" ><img runat="server" src="~/images/spacer.gif" width="1" height="5"/></td>
                                                                    </tr>
                                                                  </table>
																 </td>
																	  <td class="bbBoxInsideRight"><img runat="server" src="~/images/spacer.gif" width="5" height="1"/></td>
															  </tr>
																	<tr>
																	  <td class="bbBoxInsideBottomLeft"></td>
																	  <td class="bbBoxInsideBottom"></td>
																	  <td class="bbBoxInsideBottomRight"></td>
																</tr>
														  </table>
															<!--End form-->															
														</td>
														<td><img runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
													</tr>
													<tr><td colspan="3"><img runat="server" src="~/images/spacer.gif" width="1" height="5"></td></tr>
												</table>
											</td>
											<td class="boxInsideRight"><img runat="server" src="~/images/spacer.gif" width="1" height="4"/></td>
										  </tr>
										  <tr>
											<td class="boxInsideBottomLeft" ><img runat="server" src="~/images/spacer.gif" width="4" height="4" /></td>
											<td class="boxInsideBottom"><img runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
											<td class="boxInsideBottomRight"><img runat="server" src="~/images/spacer.gif" width="4" height="4" /></td>
										  </tr>
										</table>
									</td>
									<td><img runat="server" src="~/images/spacer.gif" width="5" height="1"></td>
								</tr>
								<tr><td colspan="3"><img runat="server" src="~/images/spacer.gif" width="1" height="5"></td></tr>
								<!--END CONTENT-->
								
							</table>
						</td>
						<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1"></td>
					</tr>
					<tr>
						<td class="frBottomLeft"></td>
						<td class="frBottom"></td>
						<td class="frBottomRight"></td>
					</tr>
				</table>
			<!---CONTEN BG BORDER--->
			</td>
		</tr>	
</form>
</table>
</body>
</html>