<%@ Page language="c#" Codebehind="assetsSubmit.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.asset.assetsSubmit" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../usercontrols/Pager.ascx" %>

<link href="../css/home.css" rel="stylesheet" type="text/css" />		
<link href="../css/kanevaSC.css" rel="stylesheet" type="text/css"/>
<link href="../css/friends.css" rel="stylesheet" type="text/css"/>
<link href="../css/kanevaText.css" type="text/css" rel="stylesheet">
<link href="../css/new.css" rel="stylesheet" type="text/css">


<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td>
			<asp:ValidationSummary ShowMessageBox="False" ShowSummary="True" Class="formError" id="valSum" DisplayMode="BulletList" runat="server" HeaderText="Please correct the following errors:"/>
		</td>
		</td>
	</tr>
</table>
<table width="990" align="center" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" ID="Img3"></td>
	</tr>
	<tr>
		<td>
			<table  border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="frTopLeft"></td>
					<td class="frTop"></td>
					<td class="frTopRight"></td>
				</tr>
				<tr>
					<td class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
					<td valign="top" class="frBgIntMembers">
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
								
									<!-- TOP STATUS BAR -->
									<div id="pageheader">
										<table cellpadding="0" cellspacing="0" border="0" width="99%">
											<tr>
												<td nowrap="nowrap" align="left" width="280px">
													<table cellpadding="0" cellspacing="0" border="0">
														<tr>
															<td><h1>Add to Community</h1></td>
															<td width="20px">&nbsp;</td>
															<td><asp:hyperlink runat="server" title="return to Previous location" ID="hlShowMedia" Text="<< Back" CausesValidation="False"/>
															</td>
														</tr>
													</table>
												</td>
												<td align="left">
													<table cellspacing="0" cellpadding="0" border="0" width="100%">
														<tr>
															<td width="80px" nowrap align="right">Asset(s):</td>
															<td align="left" style="padding-left: 6px"> 
																<asp:Repeater id="repeaterAssets" runat="server">			             
																	<ItemTemplate>
																		<a href='<%# GetAssetDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "asset_id")))%>'>
																		<%# DataBinder.Eval (Container.DataItem, "name")%>
																		</a>&nbsp;&nbsp;&nbsp;
																		<input type="hidden" runat="server" id="hidAssetId" value='<%#DataBinder.Eval(Container.DataItem, "asset_id")%>' NAME="hidAssetId">
																	</ItemTemplate>			             
																</asp:Repeater>
															</td>
														</tr>
													</table>												
												</td>
												
											</tr>
										</table>
									</div>
									<!-- END TOP STATUS BAR -->	
								
								</td>
							</tr>
							<tr>
								<td width="968">
									<div class="module whitebg">
									<span class="ct"><span class="cl"></span></span>
									
									<table cellpadding="0" cellspacing="0" border="0" width="99%">
										<tr>
											<td align="left"><h2>My Communities</h2></td>
											<td align="right">
												<asp:label runat="server" id="lblResults_Top" cssclass="insideTextNoBold" /><br />
												<kaneva:pager runat="server" isajaxmode="True" id="pgTop" onpagechanged="pg_PageChange" maxpagestodisplay="5" shownextprevlabels="true" />
											</td>
										</tr>
									</table>
									
									<table width="99%" cellspacing="0" cellpadding="5" border="0" class="data">
										<tr bgcolor="#fdfdfc">
											<td align="left" colspan="2" class="">
												Select: <a href="javascript:void(0);" onclick="Select_All(true);">All</a> | <a href="javascript:void(0);" onclick="Select_All(false);">None</a>
											</td>
											<td class="bold">Community Name</td>
											<td class="bold">Members</td>
											<td class="bold" align="center">Access</td>
										</tr>
										<asp:Repeater runat="server" id="rptChannels">  
											<ItemTemplate>
												<tr>
													<td align="center" width="15">
														<asp:checkbox runat="server" id="chkEdit" style="horizontal-align: right;" />
														<input type="hidden" runat="server" id="hidChannelId" value='<%#DataBinder.Eval(Container.DataItem, "community_id")%>' NAME="hidChannelId">
													</td>
													<td>
														<!--image holder-->
														<table cellpadding="0" cellspacing="0" border="0" width="100%" class="nopadding">
															<tr>
																<td align="right" width="36">
																	<div class="framesize-small">
																	<div class="frame">
																		<span class="ct"><span class="cl"></span></span>
																		<div class="imgconstrain">
																			<a href='<%# GetBroadcastChannelUrl (DataBinder.Eval (Container.DataItem, "name_no_spaces").ToString ())%>' style="cursor: hand;">
																				<img runat="server" src='<%# GetBroadcastChannelImageURL (DataBinder.Eval(Container.DataItem, "thumbnail_small_path").ToString (), "sm") %>' border="0" ID="Img4"/>
																			</a>
																		</div>
																		<span class="cb"><span class="cl"></span></span>
																	</div>
																	</div>
																</td>
															</tr>
														</table>
														<!--end image holder-->			    
													</td>
													<td valign="middle" align="left">
														<table border="0" cellspacing="0" cellpadding="0" width="99%">
															<tr>
																<td>
																	<span >
																		<a href='<%# GetBroadcastChannelUrl (DataBinder.Eval (Container.DataItem, "name_no_spaces").ToString ())%>'><%# TruncateWithEllipsis (DataBinder.Eval (Container.DataItem, "name").ToString (), 40)%></a>&nbsp;&nbsp;&nbsp;Owner: <a class="dateStamp" href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "creator_username").ToString ())%>><%# DataBinder.Eval(Container.DataItem, "username") %></a></span><span class="insideBoxText11"><br /><br /></span></td>
																	</span>
																</td>
															</tr>
															<tr>
																<td><span ><%# DataBinder.Eval(Container.DataItem, "description") %></span></td>
															</tr>
															<tr>
																<td><span >Tags: <asp:label id="tags" runat="server" Text='<%# GetChannelTags (DataBinder.Eval(Container.DataItem, "keywords").ToString ())%>'/></span></td>
															</tr>
														</table>
													</td>
													<td align="center"><span><%# DataBinder.Eval (Container.DataItem, "number_of_members") %></span></td>
													<td align="center">
														<span><%# GetChannelContentType (DataBinder.Eval (Container.DataItem, "is_adult").ToString()) %><br><%# GetChannelStatus (DataBinder.Eval (Container.DataItem, "is_public")) %></span>
													</td>
												</tr>
											</ItemTemplate>
											<AlternatingItemTemplate>
												<tr class="altrow">
													<td align="center" width="15">
														<asp:checkbox runat="server" id="chkEdit" style="horizontal-align: right;" />
														<input type="hidden" runat="server" id="hidChannelId" value='<%#DataBinder.Eval(Container.DataItem, "community_id")%>' NAME="hidChannelId">
													</td>
													<td>
													<!--image holder-->
														<table cellpadding="0" cellspacing="0" border="0" width="100%" class="nopadding">
															<tr>
																<td align="right" width="36">
																	<div class="framesize-small">
																	<div class="frame">
																		<span class="ct"><span class="cl"></span></span>
																		<div class="imgconstrain">
																			<a href='<%# GetBroadcastChannelUrl (DataBinder.Eval (Container.DataItem, "name_no_spaces").ToString ())%>' style="cursor: hand;">
																				<img runat="server" src='<%# GetBroadcastChannelImageURL (DataBinder.Eval(Container.DataItem, "thumbnail_small_path").ToString (), "sm") %>' border="0" ID="Img5"/>
																			</a>
																		</div>
																		<span class="cb"><span class="cl"></span></span>
																	</div>
																	</div>
																</td>
															</tr>
														</table>
														<!--end image holder-->			    
														</td>
														<td valign="middle" align="left">
															<table border="0" cellspacing="0" cellpadding="0" width="99%">
																<tr>
																	<td>
																		<span >
																			<a href='<%# GetBroadcastChannelUrl (DataBinder.Eval (Container.DataItem, "name_no_spaces").ToString ())%>'><%# TruncateWithEllipsis (DataBinder.Eval (Container.DataItem, "name").ToString (), 40)%></a>&nbsp;&nbsp;&nbsp;Owner: <a class="dateStamp" href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "creator_username").ToString ())%>><%# DataBinder.Eval(Container.DataItem, "username") %></a></span><span class="insideBoxText11"><br /><br /></span></td>
																		</span>
																	</td>
																</tr>
																<tr>
																	<td><span ><%# DataBinder.Eval(Container.DataItem, "description") %></span></td>
																</tr>
																<tr>
																	<td><span >Tags: <asp:label id="tags" runat="server" Text='<%# GetChannelTags (DataBinder.Eval(Container.DataItem, "keywords").ToString ())%>'/></span></td>
																</tr>
															</table>
														</td>
														<td align="center"><span><%# DataBinder.Eval (Container.DataItem, "number_of_members") %></span></td>
														<td align="center">
															<span><%# GetChannelContentType (DataBinder.Eval (Container.DataItem, "is_adult").ToString()) %><br><%# GetChannelStatus (DataBinder.Eval (Container.DataItem, "is_public")) %></span>
														</td>
													</tr>
												</AlternatingItemTemplate>
										</asp:Repeater>
									</table>
									<br/>
									<table cellpadding="0" cellspacing="0" border="0" width="99%">
										<tr>
											<td align="right">
												<asp:label runat="server" id="lblResults_Bottom" cssclass="insideTextNoBold" /><br />
												<kaneva:pager runat="server" isajaxmode="True" id="pgBottom" onpagechanged="pg_PageChange" maxpagestodisplay="5" shownextprevlabels="true" />
											</td>
										</tr>
									</table>
									<br />
									<table border="0" cellspacing="0" cellpadding="0" width="95%" align="center">
										<tr>
											<td align="left">
												<span class="note">NOTE: You can only add items to a community you are a moderator or owner of, or belong to if the community allows it.</span>
											</td>
											<td align="right" width="80px">
												<asp:button class="biginput" id="btnAddToChannels1" runat="Server" CausesValidation="True" onClick="btnUpdate_Click" Text="    Add to Selected Communities    "/>
											</td>	
										</tr>
									</table>
								<span class="cb"><span class="cl"></span></span>
								</div>
								</td>
							</tr>
						</table>
					</td>
					<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
				</tr>
				<tr>
					<td class="frBottomLeft"></td>
					<td class="frBottom"></td>
					<td class="frBottomRight"></td>
				</tr>
				<tr>
					<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" /></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
