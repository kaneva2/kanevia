///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Kaneva
{
    /// <summary>
    /// Summary description for imageUpload.
    /// </summary>
    public class iconUpload : System.Web.UI.Page
    {
        private void Page_Load(object sender, System.EventArgs e)
        {
            // Put user code to initialize the page here
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnUpload_Click (object sender, EventArgs e) 
        {
            byte[] bIcon = new byte [0];
            string contentType = "";
            string url = "";

            if (inpIcon.PostedFile != null && inpIcon.PostedFile.ContentLength > 0)
            {
                //To create a PostedFile
                HttpPostedFile File = inpIcon.PostedFile;

                // Make sure the image is not too big
                if (File.ContentLength > KanevaGlobals.MaxUploadedImageSize)
                {
                    ShowErrorOnStartup ("This Icon is too large, please select an image smaller than " + KanevaGlobals.FormatImageSize (KanevaGlobals.MaxUploadedImageSize) + ".");
                    return;
                }

                // Make sure it is a valid file type
                string strExtensionUpperCase = System.IO.Path.GetExtension (File.FileName).ToUpper ();
                if (!strExtensionUpperCase.Equals (".ICO"))
                {
                    ShowErrorOnStartup ("Image must be a .ico");
                    return;
                }

                //Create byte Array with file len
                bIcon = new Byte [File.ContentLength];

                //force the control to load data in array
                File.InputStream.Read (bIcon, 0, File.ContentLength);

                System.IO.MemoryStream iconImageMem = null;
                System.Drawing.Icon theIco = null;

                try
                {
                    iconImageMem = new System.IO.MemoryStream(bIcon);
                    theIco = new System.Drawing.Icon(iconImageMem);

                    if ( (theIco.Height != 32) || (theIco.Width != 32) )
                    {
                        ShowErrorOnStartup ("Image must be a .ico, size 32 x 32");
                        theIco.Dispose();
                        return;
                    }

                    theIco.Dispose();
                }
                catch (System.ArgumentException)
                {
                    ShowErrorOnStartup ("Image must be a .ico, size 32 x 32");
                    return;
                }

                string imagePath = File.FileName.Remove (0, File.FileName.LastIndexOf ("\\" ) + 1);
                contentType = File.ContentType;

                StoreUtility.UpdateGameIconImage (Convert.ToInt32 (Request ["assetId"]), bIcon, imagePath, contentType);
                url = GetGameIconURL (Convert.ToInt32 (Request ["assetId"]));
            }

            // If successfull, send javascript to update mykaneva page, close the window
            if (!ClientScript.IsClientScriptBlockRegistered(GetType (), "startup"))
            {
                string javascript = "<script language=JavaScript>window.opener.frmMain.imgPreview3.src='" + url + "';window.close()</script>";
                ClientScript.RegisterStartupScript(GetType(), "startup", javascript);
            }
        }

        /// <summary>
        /// Get the asset image URL
        /// </summary>
        /// <param name="assetId"></param>
        /// <returns></returns>
        public string GetGameIconURL (int assetId)
        {
            return Page.ResolveUrl ("~/displayImage.aspx?gameId=" + assetId + "&sdi=Y&Resize=N&MaxWidth=32&MaxHeight=32");
        }

        /// <summary>
        /// Show an error message on startup
        /// </summary>
        /// <param name="errorMessage"></param>
        protected void ShowErrorOnStartup (string errorMessage)
        {
            string scriptString = "<script language=JavaScript>";
            scriptString += "alert ('Please correct the following errors:\\n\\n" + errorMessage + "');";
            scriptString += "</script>";

            if (!ClientScript.IsClientScriptBlockRegistered(GetType(), "ShowError"))
            {
                ClientScript.RegisterStartupScript(GetType(), "ShowError", scriptString);
            }
        }

        protected System.Web.UI.WebControls.Button btnUpload;

        protected HtmlInputFile inpIcon;


        #region Web Form Designer generated code

        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
        
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {    
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion
    }
}

