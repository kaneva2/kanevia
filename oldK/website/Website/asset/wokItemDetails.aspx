<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="wokItemDetails.aspx.cs" validateRequest="false" Inherits="KlausEnt.KEP.Kaneva.wokItemDetails" %>
<%@ Register TagPrefix="UserControl" TagName="RelatedItemsView" Src="../usercontrols/WOKRelatedItemsView.ascx" %>
<%@ Register TagPrefix="UserControl" TagName="ItemCommment" Src="../usercontrols/WOKItemComments.ascx" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>

<script type="text/javascript" src="../jscript/SWFObject/swfobject.js"></script>
<script type="text/javascript" src="../jscript/prototype.js"></script>
<script type="text/javascript" src="../jscript/media-share.js"></script>

<link href="../css/new.css" rel="stylesheet" type="text/css"/>
<link href="../css/media-share.css" rel="stylesheet" type="text/css"/>

<span style="line-height:10px;"><br/></span>
<div id="divAccessMessage" runat="server" visible="false">
	<table border="0" cellspacing="0" cellpadding="0" width="100%" align="center">
		<tr>
			<td valign="top" align="center">
				<table  border="0" cellpadding="0" cellspacing="0" width="75%">
					<tr>
						<td class="frTopLeft"></td>
						<td class="frTop"></td>
						<td class="frTopRight"></td>
					</tr>
					<tr>
						<td class="frBorderLeft"><img id="Img1" runat="server" src="~/images/spacer.gif" width="1" height="1"/></td>
						<td valign="top" class="frBgIntMembers">
										
							<table cellpadding="0" cellspacing="0" border="0" width="100%">
								<tr>
									<td valign="top" align="center">
										<div class="module whitebg">
											<span class="ct"><span class="cl"></span></span>
												<h2 id="h2Title" runat="server"></h2>
												<br/>
												<table cellpadding="6" cellspacing="0" border="0" width="95%">
													<tr>
														<td width="80%" align="left" valign="top" id="tdActionText" runat="server"></td>
														<td width="20%" align="right" valign="top" id="tdButton" runat="server">
															<asp:button id="btnAction" runat="server"></asp:button>
														</td>
													</tr>
													<tr><td colspan="2" align="left" valign="top" id="tdPolicyText" runat="server"></td></tr>
												</table>
											<span class="cb"><span class="cl"></span></span>
										</div>
									</td>
								</tr>
							</table>
							
						</td>
						<td class="frBorderRight"><img id="Img2" runat="server" src="~/images/spacer.gif" width="1" height="1"/></td>
					</tr>
					<tr>
						<td class="frBottomLeft"></td>
						<td class="frBottom"></td>
						<td class="frBottomRight"></td>
					</tr>
					<tr>
						<td><img id="Img3" runat="server" src="~/images/spacer.gif" width="1" height="14"/></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</div>
													

<div id="divDetails" runat="server" visible="false">
<table border="0" cellspacing="0" cellpadding="0"  width="990" align="center">
	<tr>
		<td><img runat="server" src="~/images/spacer.gif" id="Img4" width="1" height="14" /></td>
	</tr>
	<tr>
		<td>
			<table  border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="frTopLeft"></td>
					<td class="frTop"></td>
					<td class="frTopRight"></td>
				</tr>
				<tr>
					<td class="frBorderLeft"><img id="Img5" runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
					<td valign="top" class="frBgIntMembers">
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							
							<tr>
								<td width="968" align="left"  valign="top"> 

									<div id="pageheader"><h1 id="h1AssetTitle" runat="server"></h1></div>

									<table width="100%" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td width="486" valign="top" align="center">
											
												<table width="100%"  border="0" cellpadding="0" cellspacing="0">
													
													<!-- ABOUT MEDIA -->
													<tr>
														<td align="center">
															<div class="module">
																<span class="ct"><span class="cl"></span></span>
 																<p class="formspacer_small" />
																<div style="width:450px;background-color:#FFFFFF;overflow:hidden;border:1px solid #e0e0e0;margin:0 auto;">
																<!-- PLAYER / IMAGE -->
																<asp:placeholder runat="server" id="phStream"/>
																</div>
																
																<ajax:ajaxpanel id="ajpActionBar" runat="server">																
																<!-- TOOLBAR -->
																<table border="0" cellspacing="0" cellpadding="0" width="450" align="center">
																	<tr>																			  
																		<td>
																			<ul id="mediaNav">
																				
																				<li id="rave"><asp:linkbutton id="lbRave" onclick="lbRave_Click" visible="true" runat="server" tooltip="Rave this Item"><img id="Img6" src="~/images/raveit_bt.gif" runat="server" alt="raveit_bt.gif" width="16" height="16" border="0">Rave this</asp:linkbutton></li>
																				<li id="add"><asp:linkbutton id="lbShoppingList" onclick=" lbShoppingList_Click" visible="true" runat="server" tooltip="Add to my shopping list"><img id="Img7" src="~/images/add_16.gif" runat="server" alt="add_16.gif" width="16" height="16" border="0">Add to Shopping List</asp:linkbutton></li>																			 
																				<li id="share">
																				
																				    <!--
																				    <asp:hyperlink id="hlShare" visible="true" runat="server" target="_blank" tooltip="Share this Item" 
																				    title="E-mail this, post to del.icio.us, etc." >
																				    <img src="~/images/icon_share.gif" runat="server" alt="icon_share.gif" width="16" height="16" border="0"> 
																				    Share this</asp:hyperlink>
																				    -->

																				    <a href="#"  title="E-mail this, post to del.icio.us, etc." id="akst_link_1" class="akst_share_link" rel="nofollow" runat="server">
																				    <img id="Img9" src="~/images/icon_share.gif" runat="server" alt="icon_share.gif" width="16" height="16" border="0">Share This</a>
																				</li>
																			</ul>
																		</td>
																	</tr>																			  
																</table>
										
																</ajax:ajaxpanel>																
																<span style="width:90%;text-align:right;"><a id="aEdit" runat="server" style="color:red;" visible="False">Edit this item</a></span>
																<table width="97%" align="center"  border="0" cellspacing="0" cellpadding="5" class="nopadding">
																	
																	<!-- OWNER AVATAR -->
																	<tr> 
																		<td width="80px" align="left">
																			<div class="framesize-small">
																			<div class="passrequired" visible="false" runat="server" id="divPassRequired"></div>
																				<div class="frame">
																					<span class="ct"><span class="cl"></span></span>							
																						<div class="imgconstrain">
																							<img runat="server" id="imgWOKItemThumbnail" src="~/images/KanevaIcon01.gif" border="0" /></div>																																				
																					<span class="cb"><span class="cl"></span></span>
																				</div>
																			</div>
																		</td>
																		<td valign="top" colspan="3" ><h2 style="padding-bottom:4px"><asp:label id="lblName" runat="server"/></h2><asp:label id="lblDescription" runat="server"/></td>
																	</tr>
																	<tr>
																		<td class="formspacer_small" colspan="4"></td>
																	</tr>
																	<tr>
																		<td width="80px" valign="top">
																		    <strong>Price:</strong>
																		</td>
																		<td colspan="3" valign="top">
																		    <asp:label id="lblprice" runat="server" />&nbsp;
																		    <asp:label id="lblcredit" runat="server" />
																		</td>
																	</tr>		 
																	<tr>
																		<td width="80px" valign="top">
																		    <strong>Store:</strong>
																	    </td>
																		<td colspan="3">
																		    <asp:literal id="litStore" runat="server" />
																		</td>
																	</tr>		 
																	<tr>
																		<td width="80px" valign="top"><strong>AccessType:</strong></td>
																		<td colspan="3" valign="top" ><asp:label id="lblAccess" runat="server" /></td>
																	</tr>		 
																	<tr>
																		<td width="80px" valign="top"><strong>Availability:</strong></td>
																		<td colspan="3" valign="top"><asp:label id="lblAvailability" runat="server" /></td>
																	</tr>		 
																	<tr >
																		<td width="80px" valign="top"><strong>Sets:</strong></td>
																		<td valign="top" colspan="3"><asp:literal id="litSets" runat="server" /></td>
																	</tr>		 
																	<tr>
																		<td width="80px" valign="top"><strong>Exclusivity:</strong></td>
																		<td valign="top" colspan="3"><asp:label id="lblExclusivity" runat="server" /></td>
																	</tr>		 
																	<tr>
																		<td class="formspacer_small"></td>
																	</tr>
																	<tr>
																		<td width="80px" valign="top"><strong>Item stats:</strong></td>
																		<td valign="top" width="500">
																			<table border="0" cellspacing="0" cellpadding="0" width="97%">
																				<tr>
																					<td>
																						Views: <asp:label id="lblNumViews" runat="server" /><br/>
																						Raves: <ajax:ajaxpanel id="Ajaxpanel1" runat="server"><asp:label id="lblRaves" runat="server" text="0"/></ajax:ajaxpanel><br/>
																						Reviews: <asp:label id="lblReviews" runat="server" /><br/>
																					    Uploaded: <asp:label id="lblCreatedDate" runat="server"/>
																					</td>
																				</tr>
																			</table>
																		</td>
																		<td valign="top" align="right" >Shared by: <a id="aUserName" runat="server"></a></td>
                                                                        <td valign="top" align="right" >
																			<div class="framesize-xsmall">
																				<div class="restricted" visible="false" runat="server" id="div2"></div>
																					<div class="frame">
																						<span class="ct"><span class="cl"></span></span>
																							<div class="imgconstrain">
																								<a class="bodytext" id="aUserName3" runat="server"><img runat="server" id="imgAvatar" src="~/images/KanevaIcon01.gif" border="0" /></a>
																							</div>
																							<span class="cb"><span class="cl"></span></span>
																					</div>
																			</div>
																		</td>
																	</tr>		 
																	<tr>
																		<td class="formspacer_small" colspan="4"><hr /></td>
																	</tr>
																	<tr>
																		<td width="80px" valign="top"><strong>Share this:</strong></td>
																		<td colspan="3"><asp:textbox style="width: 355px;" cssclass="formKanevaText" id="txtShare" runat="server" columns="60" readonly="True"></asp:textbox><br />
																			<span class="note">Use this when you want a hyperlink. Good for email and when you can't embed the media player.</span>
																		</td>
																	</tr>
																	<tr>
																		<td class="formspacer_small"></td>
																	</tr>
																	<tr>
																		<td width="80px" valign="top"><strong><div id="divEmbedded" runat="server">Embed this:</div></strong></td>
																		<td colspan="3"><asp:textbox style="width: 355px;" id="txtEmbed" cssclass="formKanevaText" runat="server" columns="60" readonly="True"></asp:textbox><br />
																			<span class="note">Add this media to your Kaneva profile, community, web site or blog.</span>
																		</td>
																	</tr>
																	
																	
																	<tr id="trPhotoURL" runat="server" visible="false">
																		<td width="80px" valign="top"><strong><div id="divPhotoUrl">Photo URL:</div></strong></td>
																		<td colspan="3"><asp:textbox style="width: 355px;" id="txtRawEmbed" cssclass="formKanevaText" runat="server" columns="60" readonly="True"></asp:textbox><br />
																			<span class="note">Use this photo url for backgrounds.</span>
																		</td>
																	</tr>
																	
																	<tr>
																		<td class="formspacer_small"></td>
																	</tr>
																	<!--<tr>
																		<td width="80"><strong>Related <span id="spnRelatedType" runat="server">videos</span> by topic:</strong></td>
																		<td class="textGeneralGray01" colspan="2">
																			<asp:label id="lblRelatedByTopic" runat="server"/>
																		</td>
																	</tr>
																	<tr>
																		<td class="formspacer_small"></td>
																	</tr>
																	<tr>
																		<td class="formspacer_small"></td>
																	</tr>-->
																</table>
																<span class="cb"><span class="cl"></span></span>
															</div>
														</td>
													</tr>
													
													<tr>
														<td><img id="Img10" runat="server" src="~/images/spacer.gif" width="1" height="14" /></td>
													</tr>
													
													
												</table>
											</td>
											<td width="14px"><img id="Img11" runat="server" src="~/images/spacer.gif" width="14" height="1" /></td>
											
											<td width="468px" valign="top">
												<table width="100%"  border="0" cellpadding="0" cellspacing="0" id="contentRight">
													<tr>
														<td>
														
															<!--START AD PROMO 1-->
															<div id="promo1" class="fullbanner">
																<a id="A1" href="~/channel/channelPage.aspx?communityId=1118&pageId=772341" runat="server"><img id="Img12" src="~/images/promo_kaneva.gif" runat="server" border="0"/></a>
															</div>
															
															<!--START AD PROMO 2-->
															<div id="promo2" class="fullbanner">
																<table id="_ctl51_tblGoogleAdds" cellpadding="0" cellspacing="0" border="0" width="468px">
																	<tr>
																		<td align="center">
																		<script type="text/javascript"><!--
																			google_ad_client = "pub-7044624796952740"; google_ad_width = 468; google_ad_height = 60; google_ad_format = "468x60_as"; google_ad_type = "text"; google_ad_channel ="7248390549";google_color_border = "FFFFFF";google_color_bg = "FFFFFF";google_color_link = "018AAA";google_color_text = "737373";google_color_url = "018AAA";<asp:Literal runat="server" id="litAdTest"/>; //--></script> 
																			<script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
																			</script>
																		</td>
																	</tr>
																</table>
															</div>
														
															<!--START RELATED MEDIA-->
															<div id="related_media">
																	
																<usercontrol:relateditemsview runat="server" id="ucRelatedItems" />
															
															</div>																
														<br />
														
														<!--END RELATED MEDIA-->
													
													
														<!--START COMMENTS-->
															<div class="module whitebg">
																<span class="ct"><span class="cl"></span></span>
																					
																	<usercontrol:ItemCommment runat="server" id="ucReviews" />

																<span class="cb"><span class="cl"></span></span>
															</div>
														<!--END COMMENTS-->
															
														</td>
													</tr>
													
													<tr>
														<td><img id="Img13" runat="server" src="~/images/spacer.gif" width="1" height="14" /></td>
													</tr>
													
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" id="Img14" width="1" height="1" /></td>
				</tr>
				<tr>
					<td class="frBottomLeft"></td>
					<td class="frBottom"></td>
					<td class="frBottomRight"></td>
				</tr>
				
			</table>
		</td>
	</tr>
</table>						
</div>


										
<asp:label id="lblOwnerViews" runat="server"/> 
<asp:label id="lblOwnerFavs" runat="server"/>  
<asp:label id="lblOwnerFriends" runat="server"/>

<asp:label runat="server" id="lblImageCaption" cssclass="dateStamp" style="FONT-WEIGHT: bold" visible="false"/>

<table>																	
	<tr id="trAttributes" runat="server" visible="false">
		<td align="center">
			<div class="module whitebg">
				<span class="ct"><span class="cl"></span></span>
				<h2>Additional Information</h2>
					<!-- Additional Info -->
					<table width="97%"  border="0" cellspacing="0" cellpadding="5">
						<tr id="trDetailDesc" runat="server" visible="false">
							<td width="100"><strong>Detail Description:</strong></td>
							<td class="textGeneralGray01"><asp:label id="lblBodyText" runat="server"/></td>
						</tr>
																	
						<asp:repeater runat="server" id="rptAttributes" >
							<itemtemplate>
								<tr>
									<td width="100"><strong><%# DataBinder.Eval (Container.DataItem, "attribute_name") %>:</strong></td>
									<td class="textGeneralGray01"><%# DataBinder.Eval (Container.DataItem, "value_name").ToString ().Replace ("\n", "<br/>") %></td>
								</tr>	
							</itemtemplate>	
						</asp:repeater>

					</table>
				
				<span class="cb"><span class="cl"></span></span>
			</div>
		</td>
	</tr>
</table>


											
<!-- Share This BEGIN -->
<asp:literal runat="server" id="litNotLoggedShareStyle"></asp:literal>

	<div id="akst_form">
		<a href="javascript:void($('akst_form').style.display='none');" class="akst_close">Close</a>
		<ul class="tabs">
			
			<li id="akst_tab1" onclick="akst_share_tab('1');" runat="server">Blast</li>
			<li id="akst_tab2" onclick="akst_share_tab('2');" runat="server">Private</li>			
			<li id="akst_tab3" onclick="akst_share_tab('3');" runat="server">E-mail</li>
			<li id="akst_tab4" onclick="akst_share_tab('4');" runat="server">Social Web</li>						
		</ul>

		<div class="clear"></div>
		
		<div id="akst_blast">
		  <fieldset>
		  <ajax:ajaxpanel id="ajpShareBlast" runat="server">		  
		  <ul>
		    <li>Blast this to your friends!</li>
		    <li>Link Title: <asp:textbox id="txtBlastTitle" runat="server" width="265"></asp:textbox></li>
		    <li>Your Message (Link will automatically be inserted):</li>	    
		    <li><asp:textbox TextMode="multiline" id="txtBlast" runat="server" width="325" height="80"></asp:textbox></li>
		    <li><asp:button id="btnBlast" onClick="btnBlast_Click" Text="Blast It!" runat="server" />
		        <h2 class="alertmessage"><span id="spnBlastAlertMsg" runat="server"></span></h2>
		    </li>		  
		  </ul>	
		  </ajax:ajaxpanel>	  		 
		  </fieldset>
		</div>		
		
	    <div id="akst_private">
		  <fieldset>	
		  <ajax:ajaxpanel id="ajpSharePM" runat="server">	  
		  <ul>
		  <li>Private Message To Kaneva Member:</li>
		    <li><asp:textbox  id="txtPrivateMsgTo" runat="server" width="325"></asp:textbox></li>
		    <li>Subject:</li>
		    <li><asp:textbox  id="txtPrivateMsgSubject" runat="server" width="325"></asp:textbox></li>
		    <li>Your Message (Link will automatically be inserted):</li>
		    <li><asp:textbox TextMode="multiline" id="txtPrivateMsgBody" runat="server" width="325" height="50"></asp:textbox></li>
		    <li><asp:button id="btnPMSend" onClick="btnPMSend_Click" Text="Send" runat="server" />
		     <h2 class="alertmessage"><span id="spnPMAlertMsg" runat="server"></span></h2>
		    </li>		  
		  </ul>	
		  </ajax:ajaxpanel>	  		 
		  </fieldset>
		</div>
		
		<div id="akst_email">			
				<fieldset>
					<legend>E-mail It</legend>
					<ul>
						<li>
							<label>1. Copy and paste this link into an email or instant message:</label>
							<asp:textbox id="shareLink" runat="server" class="akst_text" width="325" onclick="this.select();"></asp:textbox>							
						</li>
						<li>
							<label>2. Send this media using your computer's email application:</label>
							<asp:hyperlink id="shareMailto" runat="server" NavigateUrl="" text="Email Media" ></asp:hyperlink>
						</li>					
					</ul>
					<input type="hidden" name="akst_action" value="send_mail" />
					<input type="hidden" name="akst_post_id" id="akst_post_id" value="" />
				</fieldset>			
		</div>		
				
		<div id="akst_social">
			<ul>
				<li><a href="#" id="akst_delicious" target="_blank">del.icio.us</a></li>
				<li><a href="#" id="akst_digg" target="_blank">Digg</a></li>
				<li><a href="#" id="akst_furl" target="_blank">Furl</a></li>
				<li><a href="#" id="akst_netscape" target="_blank">Netscape</a></li>
				<li><a href="#" id="akst_yahoo_myweb" target="_blank">Yahoo! My Web</a></li>
				<li><a href="#" id="akst_stumbleupon" target="_blank">StumbleUpon</a></li>
				<li><a href="#" id="akst_google_bmarks" target="_blank">Google Bookmarks</a></li>
				<li><a href="#" id="akst_technorati" target="_blank">Technorati</a></li>
				<li><a href="#" id="akst_blinklist" target="_blank">BlinkList</a></li>
				<li><a href="#" id="akst_newsvine" target="_blank">Newsvine</a></li>
				<li><a href="#" id="akst_magnolia" target="_blank">ma.gnolia</a></li>
				<li><a href="#" id="akst_reddit" target="_blank">reddit</a></li>
				<li><a href="#" id="akst_windows_live" target="_blank">Windows Live</a></li>
				<li><a href="#" id="akst_tailrank" target="_blank">Tailrank</a></li>
			</ul>
			<div class="clear"></div>
		</div>
			
	</div>
<!-- Share This END -->