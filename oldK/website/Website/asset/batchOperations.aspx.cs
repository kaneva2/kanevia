///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using log4net;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for batchOperations.
	/// </summary>
	public class batchOperations : StoreBasePage
	{
		/// <summary>
		/// Constructor
		/// </summary>
		protected batchOperations () 
		{
			Title = "Batch Item Operations";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{

			// They have to at least be logged in to view this one
			if (Request.IsAuthenticated)
			{
				if(!GetRequestParams())
				{
					//invalid request params
					RedirectToHomePage ();
				}
			}
			else
			{
				Response.Redirect (this.GetLoginURL ());
			}

			if (!IsPostBack)
			{
				hlShowMedia.NavigateUrl = ResolveUrl ("~/asset/publishedItemsNew.aspx?communityId=" + _channelId);
				DataTable dtAssets = StoreUtility.GetAssetsBatch (_assetIds, GetUserId ());

				//dlAssets.DataSource = dtAssets;
				//dlAssets.DataBind ();

				LoadDropdowns ();
			}


			// Set nav
            HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
//			HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.MEDIA_UPLOADS;
//			HeaderNav.MyMediaUploadsNav.ActiveTab = NavMediaUploads.TAB.LIBRARY;
			HeaderNav.SetNavVisible(HeaderNav.MyKanevaNav,2);
			//HeaderNav.SetNavVisible(HeaderNav.MyMediaUploadsNav);
		}

		private void LoadDropdowns ()
		{	
			// Permissions
			drpPermission.DataValueField = "permission_id";
			drpPermission.DataTextField = "name";
            drpPermission.DataSource = WebCache.GetAssetPermissions();
			drpPermission.DataBind ();
			drpPermission.Items.Insert (0, new ListItem ("No Change", "0"));

			//Item type pull down display logic
			ItemTypeDDownLoad();
		}

		private void ItemTypeDDownLoad()
		{
			// Set up the asset type dropdown
                
			drpType.DataTextField = "description";
			drpType.DataValueField = "asset_type_id";
			string assetTypeFilter = "";
			int assetTypeId = ((_assetType != "") && (_assetType != null)) ? Convert.ToInt32(_assetType): -1;
			switch(assetTypeId)
			{
				case (int)Constants.eASSET_TYPE.PICTURE:
				case (int)Constants.eASSET_TYPE.PATTERN:
					//trAssetType.Visible = true; 
					assetTypeFilter = "asset_type_id IN (" + (int)Constants.eASSET_TYPE.PICTURE + ", " + (int)Constants.eASSET_TYPE.PATTERN + ")";
					drpType.Visible = true;
					drpType.DataSource = WebCache.GetAssetTypes (assetTypeFilter);
					drpType.DataBind ();
					//configure media type drop down
					SetDropDownIndex (drpType, _assetType);
					notice.Visible = false;
					break;
				case (int)Constants.eASSET_TYPE.MUSIC:
				case (int)Constants.eASSET_TYPE.VIDEO:
				case (int)Constants.eASSET_TYPE.GAME:
				case (int)Constants.eASSET_TYPE.WIDGET:
				case (int)Constants.eASSET_TYPE.TV:
				default:
					drpType.Visible = false;
					notice.Visible = true;
					notice.Text = "Type can only be changed for photos to patterns and vice versa, while viewing �All Photos� or �All Patterns.�";
					break;
			}

		}

		/// <summary>
		/// Update Event Handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void imgUpdate_Click (object sender, ImageClickEventArgs e) 
		{
//			CheckBox chkEdit;
//			HtmlInputHidden hidAssetId;
			//string assetIds = "";
			string assetIds = Request ["assetIds"].ToString ();

			// Server validation
			if (!Page.IsValid) 
			{
				return;
			}

			if (assetIds.Length == 0)
			{
				return;
			}

			// Amount

			// Ratings
			int isMature = (int) Constants.eASSET_RATING.GENERAL;
			if (chkMature.Checked)
			{
				isMature = (int) Constants.eASSET_RATING.MATURE;
			}

			string licenseType = "";
			string licenseName = "";
			string licenseAdditional = "";
			string licenseCC = "";
			string licenseURL = "";
			
			// Check for any inject scripts in large text fields
			if ((KanevaWebGlobals.ContainsInjectScripts (txtPerAdditional.Text)) || (KanevaWebGlobals.ContainsInjectScripts (txtCommAdditional.Text)))
			{
				m_logger.Warn ("User " + KanevaWebGlobals.GetUserId () + " tried to script from IP " + Common.GetVisitorIPAddress());
				ShowErrorOnStartup ("Your input contains invalid scripting, please remove script code and try again.", true);
				return;
			}

			// Check for any inject scripts in smaller text fields
			if ((KanevaWebGlobals.ContainsInjectScripts (txtLicenseName.Text)) || (KanevaWebGlobals.ContainsInjectScripts (this.txtLicenseURL.Text)))
			{
				m_logger.Warn ("User " + KanevaWebGlobals.GetUserId () + " tried to script from IP " + Common.GetVisitorIPAddress());
				ShowErrorOnStartup ("Your input contains invalid scripting, please remove script code and try again.", true);
				return;
			}

			// Set the correct option
			if (optPublic.Checked)
			{
				licenseType = "D";
			}
			else if (optCC.Checked)
			{
				licenseType = "C";
				licenseCC = Server.HtmlEncode (drpCCLicense.SelectedValue);
			}
			else if (optPersonal.Checked)
			{
				licenseType = "P";
				licenseAdditional = Server.HtmlEncode (txtPerAdditional.Text);
			}
			else if (optCommercial.Checked)
			{
				licenseType = "I";
				licenseAdditional = Server.HtmlEncode (txtCommAdditional.Text);
			}
			else if (optOther.Checked)
			{
				licenseType = "O";
				licenseName = Server.HtmlEncode (txtLicenseName.Text);
				licenseURL = Server.HtmlEncode (txtLicenseURL.Text);
			}

			// Categories
			int category1Id = 0;
			int category2Id = 0;
			int category3Id = 0;
			CheckBox chkCategory;
			HtmlInputHidden hidCatId;

			// Permissions
			int permission = Convert.ToInt32 (Server.HtmlEncode (drpPermission.SelectedValue));
			int permissionGroup = 0;

			foreach (DataListItem dliCategory in dlCategories.Items)
			{
				chkCategory = (CheckBox) dliCategory.FindControl ("chkCategory");

				if (chkCategory.Checked)
				{
					hidCatId = (HtmlInputHidden) dliCategory.FindControl ("hidCatId");
					
					if (category1Id.Equals (0))
					{
						category1Id = Convert.ToInt32 (hidCatId.Value);
					}
					else if (category2Id.Equals (0))
					{
						category2Id = Convert.ToInt32 (hidCatId.Value);
					}
					else if (category3Id.Equals (0))
					{
						category3Id = Convert.ToInt32 (hidCatId.Value);
					}
				}
			}

			int assetType = 0;
			if(drpType.Visible)
			{
				assetType = Convert.ToInt32(drpType.SelectedValue);
			}

			// set amount to 0, all assets are now free
			if(drpPermission.SelectedValue == "0")
			{
				StoreUtility.UpdateAssetBatch (assetIds, GetUserId (),  GetUserId (), assetType,
					category1Id, category2Id, category3Id, 0, isMature, licenseType, licenseName, 
					licenseAdditional, licenseCC, licenseURL);
			}
			else
			{
				StoreUtility.UpdateAssetBatch (assetIds, GetUserId (),  GetUserId (), assetType,
					category1Id, category2Id, category3Id, 0, isMature, licenseType, licenseName, 
					licenseAdditional, licenseCC, licenseURL, permission, permissionGroup);
			}

			Response.Redirect (GetMyPublishedItemURL ());
		}



		/// <summary>
		/// Change type of asset - no longer used
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void drpType_Change (Object sender, EventArgs e)
		{
			if (drpType.SelectedValue.Equals ("0"))
			{
				dlCategories.DataSource = null;
				dlCategories.DataBind();
			}
			else
			{
				PopulateCategory (Convert.ToInt32 (drpType.SelectedValue));
			}
		}

		private bool GetRequestParams()
		{
			bool retVal = false;
			try
			{
				if(Request ["communityId"] != null)
				{
					//get channelId and find the default page
					_channelId = Convert.ToInt32 (Request ["communityId"]);
				}
				else
				{
					_channelId = GetPersonalChannelId ();
				}

				_assetIds = Request ["assetIds"].ToString ();
				_assetType = Request ["assetType"].ToString ();
				retVal = _assetIds.Length > 0;
			}
			catch(Exception){}
			
			return retVal;
		}

		/// <summary>
		/// Populate the categories
		/// </summary>
		/// <param name="assetTypeId"></param>
		private void PopulateCategory (int assetTypeId)
		{
			dlCategories.DataSource = WebCache.GetAssetCategories (assetTypeId);
			dlCategories.DataBind ();
		}

		protected DataList dlAssets;
		protected CheckBox chkMature;

		protected HtmlAnchor aMultiEdit;

		protected DropDownList drpType, drpLicense;
		protected TextBox txtTeaser;

		protected ImageButton imgUpdate;
		protected Label notice;
		protected ImageButton btnDelete;
		protected HyperLink hlShowMedia;
		private string _assetIds, _assetType;
		private int		_channelId;

		// License stuff
		protected HtmlInputRadioButton optPublic, optCC, optPersonal, optCommercial, optOther;
		protected DropDownList drpCCLicense, drpPermission;
		protected TextBox txtPerAdditional, txtCommAdditional, txtLicenseName, txtLicenseURL;
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

		protected DataList dlCategories;

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
