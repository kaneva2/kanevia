<%@ Page language="c#" Codebehind="assetEdit.aspx.cs" ValidateRequest="False" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.assetEdit" %>
<%@ Register TagPrefix="Kaneva" TagName="HeaderText" Src="../usercontrols/HeaderText.ascx" %>
<%@ Register TagPrefix="CE" Namespace="CuteEditor" Assembly="CuteEditor" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>

<link href="../css/home.css" rel="stylesheet" type="text/css" />		
<link href="../css/kanevaSC.css" rel="stylesheet" type="text/css"/>
<link href="../css/friends.css" rel="stylesheet" type="text/css"/>
<link href="../css/kanevaBroadBand.css" type="text/css" rel="stylesheet">
<link href="../css/new.css" rel="stylesheet" type="text/css" />		

<script language="javascript" src="../jscript/prototype.js"></script>
<script type="text/javascript"><!--
var selectedCategories = new Array()
function categorySelected(obj)
{  
  if(obj.checked)
  {    
    //a new checkbox is selected, pop the first selection if 3 are already selected
    if(selectedCategories.length == 3)
    {
      firstEle = selectedCategories.shift();
      firstEle.checked = false;
    }
    selectedCategories.push(obj);  
  }else
  {
    var index = -1;
    //remove it from the array if it exists
    for(var i = 0; i < selectedCategories.length; i++)
    {
      if(selectedCategories[i] == obj)
      {
        index = i;
        break;          
      }
    }
    if(index >= 0)
    {
      //and rearrange the list
      for(var i = index; i < selectedCategories.length -1; i++)
      {
        selectedCategories[i] = selectedCategories[i+1];
      }
      selectedCategories.pop();
    }
  }
}
//--></script>


<table border="0" cellpadding="0" cellspacing="0" class="newcontainer" align="center">
	<tr>
		<td>
			<table border="0" cellpadding="0" cellspacing="0" width="100%"class="newcontainerborder">
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
				<tr> 
					<td class=""><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
					<td valign="top" class="newdatacontainer">
						<table border="0" cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<td>
									<!-- TOP STATUS BAR -->
									<div id="pageheader">
										<table cellpadding="0" cellspacing="0" border="0" width="100%">
											<tr>
												<td nowrap="nowrap" align="left">
													<table cellpadding="0" cellspacing="0" border="0">
														<tr>
															<td><h1>Edit Media</h1></td>
															<td width="20px">&nbsp;</td>
															<td><asp:hyperlink runat="server" visible="true" id="back" title="return to Previous location" runat="server"><< Back</asp:hyperlink></td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td align="center">
													<table cellpadding="0" cellspacing="0" border="0" width="70%">
														<tr>
															<td align="center">
																<ajax:ajaxpanel ID="ajpMessage" runat="server">
																
																<asp:ValidationSummary ShowMessageBox="False" style="margin-top:10px;" ShowSummary="True" cssclass="errBox black" id="valSum" DisplayMode="BulletList" runat="server" HeaderText="Please correct the following errors:"/>	
																<span id="spnAlertMsg" runat="server" class="errBox black" style="display:none;margin-top:10px;"></span>
																
																</ajax:ajaxpanel>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</div>
									<!-- END TOP STATUS BAR -->	
								</td>
							</tr>		
							<tr>
								<td width="968">
									<!--BASIC INFO -->
									<table  border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr>
											<td align="center">
												<div class="module">
													<span class="ct"><span class="cl"></span></span>
													<h2>Basic Information</h2>
													<table border="0" cellpadding="0" cellspacing="0" width="95%" align="center">
														<tr>
															<td colspan="3"><h2><asp:label runat="server" id="lblItemName" style="padding-left: 0px" class="assetTitle"/></h2></td>
														</tr>
														<tr>																
															<td width="130" align="left">
																<div class="framesize-medium">
																	<div class="frame">
																		<span class="ct"><span class="cl"></span></span>
																			<div class="imgconstrain">
																				<img runat="server" id="imgPreview2" border="0"/>
																			</div>
																		<span class="cb"><span class="cl"></span></span>
																	</div>
																</div>
															</td>
															<td width="200" valign="top"><br />
																<b>Owner:</b> <asp:Label class="filter2" id="lblCreatedBy" runat="server"/> <br /><br />
																<b>Size:</b> <asp:label runat="server" id="lblAssetSize" class="Filter2"/>
																<br>
															</td>																
															<td>
																<table  border="0" cellpadding="0" cellspacing="0" width="95%">
																	<tr>
																		<td align="right" width="100"><h4>Item Type</h4></td>
																		<td colspan="2" align="left">
																			<img runat="server" src="~/images/spacer.gif" width="10" height="1" ID="Img2"/>
																			<asp:Literal id="lblItemType" runat="server" />
																			<asp:DropDownList AjaxCall="Async" class="formKanevaText" id="drpType" runat="Server" OnSelectedIndexChanged="drpType_Change" AutoPostBack="true" style="width:300px"/> 
																			<asp:RequiredFieldValidator ID="rfdrpType" ControlToValidate="drpType" Text="*" ErrorMessage="Item type is a required field." Display="Dynamic" runat="server"/>
																		</td>
																	</tr>
																	<tr>
																		<td colspan="3"><img src="~/images/spacer.gif" width="1" height="10" /></td>
																	</tr>
																	<tr>
																		<td align="right" width="100"><h4>Item Name</h4></td>
																		<td colspan="2" align="left">
																			<img runat="server" src="~/images/spacer.gif" width="10" height="1" ID="Img10"/>
																			<asp:TextBox ID="txtItemName" class="formKanevaText" style="width:300px" MaxLength="40" runat="server"/>
																			<asp:RequiredFieldValidator ID="rfItemName" ControlToValidate="txtItemName" Text="*" ErrorMessage="Item name is a required field." Display="Dynamic" runat="server"/>
																			<b><asp:Label runat="server" id="Label2"/></b>
																		</td>
																	</tr>
																	<tr>
																		<td colspan="3"><img src="~/images/spacer.gif" width="1" height="10" /></td>
																	</tr>
																	<tr>
																		<td align="right" width="100"><h4>Access</h4></td>
																		<td colspan="2" align="left">
																			<img src="~/images/spacer.gif" width="10" height="1" />
																			<asp:DropDownList class="formKanevaText" id="drpPermission" style="width:130px" runat="Server"/>
																			<asp:RequiredFieldValidator ID="rfdrpPermission" ControlToValidate="drpPermission" Text="*" ErrorMessage="Permissions is a required field." Display="Dynamic" runat="server"/>
                                                                            <asp:Button id="DeleteAsset" OnClick="Delete_Asset" Visible="false" Text="Admin Delete" runat="server" />
																		</td>
																	</tr>
																</table>
															</td>													
														</tr>
													</table>
													<span class="cb"><span class="cl"></span></span>
												</div>
											</td>
										</tr>
									</table>													
									<!-- CATEGORIZATION -->	
									<table border="0" cellspacing="0" cellpadding="0" width="100%">
										<tr>
											<td>
												<div class="module">
													<span class="ct"><span class="cl"></span></span>
													<h2>Categorization</h2>
													<table border="0" cellpadding="0" cellspacing="0" width="95%">
														<tr>
															<td align="right" width="100" class="insideBoxText11"><h4>Restricted Content</h4></td>
															<td colspan="2" align="left">
																<img runat="server" src="~/images/spacer.gif" width="10" height="1" ID="Img7"/>
																<asp:CheckBox id="chkMature" runat="server"/> 
																<span class="red" id="spnMatureDesc" runat="server">You should set this option if this item may not be appropriate for Kaneva members who are under the age of 18</span> 																					
															</td>
														</tr>
														<tr>
															<td colspan="3"><img src="~/images/spacer.gif" width="1" height="20" /></td>
														</tr>
														<tr>
															<td align="right" width="100" valign="top"><h4>Tags</h4></td>
															<td colspan="2" align="left">
																<img runat="server" src="~/images/spacer.gif" width="10" height="1" ID="Img3"/>
																<asp:TextBox ID="txtKeywords" class="formKanevaText" style="width:400px" MaxLength="250" runat="server"/>
																<asp:RequiredFieldValidator ID="rftxtKeywords" ControlToValidate="txtKeywords" Text="*" ErrorMessage="Tags is a required field." Display="Dynamic" runat="server" Enabled="False"/>
																<asp:RegularExpressionValidator id="regtxtKeywords" ControlToValidate="txtKeywords" Text="*" Display="Dynamic" EnableClientScript="True" runat="server"/>
																<span class="note">(use spaces between words)<br><img runat="server" src="~/images/spacer.gif" width="10" height="1" ID="Img8"/>Tags are unique keywords that help to categorize your item to enable user searches. Use spaces between words.</span>
															</td>
														</tr>
														<tr>
															<td colspan="3"><img src="~/images/spacer.gif" width="1" height="20" /></td>
														</tr>
														<tr>
															<td align="right" width="100" valign="top"><h4>Category</h4></td>
															<td colspan="2" align="left">
																<ajax:ajaxpanel ID="AjaxpanelAsset" runat="server">
																<asp:DataList runat="server" EnableViewState="True" ShowFooter="False" Width="100%" id="dlCategories"
																	cellpadding="0" cellspacing="0" border="0" style="margin-left: 10px;" RepeatColumns="4" RepeatDirection="Horizontal">
																	<ItemStyle CssClass="insideBoxText11" HorizontalAlign="left"/>
																	<ItemTemplate>
																		<asp:CheckBox id="chkCategory" runat="server" Checked='<%#GetCatChecked (Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "category_id")))%>' onclick="javascript:categorySelected(this)"/><%# DataBinder.Eval (Container.DataItem, "name")%>
																		<input type="hidden" runat="server" id="hidCatId" value='<%#DataBinder.Eval(Container.DataItem, "category_id")%>' NAME="hidCatId"> 
																	</ItemTemplate>
																</asp:DataList>
																</ajax:ajaxpanel>                       
																<asp:CustomValidator id="cvDlCategories" Text="*" Display="Dynamic" ErrorMessage="You must select at least one category." runat="server" Enabled="False"/>
																<img runat="server" src="~/images/spacer.gif" width="10" height="1" ID="Img9"/><br>
																<span class="blueInfo">You may choose up to 3 categories</span>
															</td>
														</tr>
														<tr>
															<td colspan="3"><img src="~/images/spacer.gif" width="1" height="20" /></td>
														</tr>
														<tr id="trCompanyName" runat="server" visible="false">
															<td align="right" width="100" valign="top"><h4>Company Name</h4></td>
															<td align="left" valign="middle">
																<img runat="server" src="~/images/spacer.gif" width="10" height="1" ID="Img5"/>
																<asp:TextBox ID="txtCompanyName" class="formKanevaText" style="width:400px" MaxLength="100" runat="server"/>
															</td>
														</tr>
													</table>
													<span class="cb"><span class="cl"></span></span>
												</div>
											</td>
										</tr>
									</table>
									<!-- DESCRIPTION -->	
									<table border="0" cellspacing="0" cellpadding="0" width="100%">
										<tr>
											<td>
												<div class="module">
													<span class="ct"><span class="cl"></span></span>
													<h2>Description</h2><br>
													<table border="0" cellpadding="0" cellspacing="0" align="center" width="95%">
														<tr>
															<td colspan="3">
																<table cellpadding="0" cellspacing="0" border="0" width="100%" id="tblThumb" runat="server">
																	<tr class="insideBoxText11">
																		<td rowspan="3" align="center" width="90px" VAlign="Top" class="dateStamp">
																			<table height="55px" cellpadding="0" cellspacing="0" border="0" style="border: 1px solid #ededed;">
																					<tr><td valign="middle" align="center"><img runat="server" id="imgPreview" border="0"/></td></tr>
																			</table>photo preview<br><br>
																		</td>
																	</tr>														
																	<tr class="insideBoxText11">
																		<td align="right" width="60px" class="insideBoxText11">Photo</td>
																		<td><img runat="server" src="~/images/spacer.gif" width="5" ID="Img24"/></td>
																		<td align="left">
																			<input type="button" class="Filter2" runat="server" id="btnUploadImage" value=" upload " NAME="btnUploadImage"/>&nbsp;<asp:button id="btnClearThumb" runat="Server" CausesValidation="False" onClick="btnClearThumb_Click" class="Filter2" Text="  clear  "/><i class="dateStamp"> must be .gif or .jpg 150 x 150 max and under 200k</i>
																		</td>
																	</tr>
																	<tr class="insideBoxText11">
																		<td align="right" valign="top" width="60px" class="insideBoxText11">Caption</td>
																		<td><img runat="server" src="~/images/spacer.gif" width="5" /></td>
																		<td align="left" valign="top">
																			<asp:TextBox ID="txtImageCaption" class="formKanevaText" style="width:350px" MaxLength="100" runat="server"/>          
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
														<tr>
															<td align="left" valign="top">
																<!-- SHORT DESCRIPTION -->	
																<h4>Short Description</h4><br>
																<asp:TextBox ID="txtTeaser" TextMode="multiline" Rows="10" class="formKanevaText"  style="width:230px" MaxLength="500" runat="server"/>
															</td>
															<td width="20"></td>
															<td align="left">
																<!-- DETAILED DESCRIPTION -->	
																<h4>Detailed Description</h4>
																<asp:CheckBox AjaxCall="None" id="chkAdvanced" runat="server" AutoPostBack="True" OnCheckedChanged="chkAdvanced_Click"/>
																<asp:Label CssClass="filter2" runat="server" ID="Label3">Show advanced buttons</asp:label><br>
																<CE:Editor id="txtBody" BackColor="#ededed" runat="server" MaxTextLength="2000" width="100%" Height="200px" ShowHtmlMode="True" ConfigurationPath="~/CuteSoft_Client/CuteEditor/Configuration/AutoConfigure/asset.config" AutoConfigure="None" ></CE:Editor>
															</td>
														</tr>
													</table>
													<span class="cb"><span class="cl"></span></span>
												</div>
											</td>
										</tr>
									</table>
									<!-- GAME INSTRUCTIONS -->	
									<table border="0" cellspacing="0" cellpadding="0" width="100%" id="tblGameInstructions" runat="server" visible="false">
										<tr>
											<td>
												<div class="module">
													<span class="ct"><span class="cl"></span></span>
													<h2>Game Instructions</h2>
													<table border="0" cellpadding="0" cellspacing="0" align="center" width="95%">
														<tr>
															<td>
																<asp:TextBox ID="txtGameInstructions" TextMode="multiline" Rows="5" class="formKanevaText" style="width:100%" MaxLength="5000" runat="server"/>
															</td>
														</tr>
													</table>
													<span class="cb"><span class="cl"></span></span>
												</div>
											</td>
										</tr>
									</table>
									<!-- THIS ITEM IS SHOWN ON THE FOLLOWING PROFILES AND COMMUNITIES -->	
									<table border="0" cellspacing="0" cellpadding="0" width="100%">
										<tr>
											<td>
												<div class="module whitebg">
													<span class="ct"><span class="cl"></span></span>
													
													<h2>This item is shown in the following profiles and communities</h2>
													<ajax:ajaxpanel ID="Ajaxpanel2" runat="server">
													<table cellpadding="0" cellspacing="0" border="0" width="95%">
														<asp:Repeater id="rptChannels" runat="server" EnableViewState="True">
															<HeaderTemplate>
																<tr Class="lineItemColHead">
																	<td width="5%"></td>
																	<td width="55%" align="left">Name</td>
																	<td width="20%">Members</td>
																	<td width="20%"></td>
																</tr>
															</HeaderTemplate>
															<ItemTemplate>
																<tr class="LineItemEven">
																	<td>
																		<a href='<%# GetBroadcastChannelUrl (DataBinder.Eval (Container.DataItem, "name_no_spaces").ToString ())%>' style="cursor: hand;">
																			<img runat="server" src='<%# GetBroadcastChannelImageURL (DataBinder.Eval(Container.DataItem, "thumbnail_small_path").ToString (), "sm") %>' width="30" border="0" ID="Img4"/>
																		</a>
																	</td>
																	<td align="left" class="bodyText"><a href='<%# GetBroadcastChannelUrl (DataBinder.Eval (Container.DataItem, "name_no_spaces").ToString ())%>'><%# TruncateWithEllipsis (DataBinder.Eval (Container.DataItem, "name").ToString (), 40)%></a></td>
																	<td align="center" class="bodyText"><%# DataBinder.Eval(Container.DataItem, "number_of_members") %></td>
																	<td align="center" class="bodyText">
																		<asp:hyperlink CssClass="adminLinks" runat="server" NavigateURL='<%#GetChannelRemoveScript (Convert.ToInt32 ( DataBinder.Eval(Container.DataItem, "community_id")), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "is_personal"))) %>' ToolTip="Remove" ID="Hyperlink1">remove</asp:hyperlink>
																	</td>
																</tr>
															</ItemTemplate>
															<AlternatingItemTemplate>
																<tr class="LineItemOdd">
																	<td>
																		<a href='<%# GetBroadcastChannelUrl (DataBinder.Eval (Container.DataItem, "name_no_spaces").ToString ())%>' style="cursor: hand;">
																			<img runat="server" src='<%# GetBroadcastChannelImageURL (DataBinder.Eval(Container.DataItem, "thumbnail_small_path").ToString (), "sm") %>' width="30" border="0" ID="Img11"/>
																		</a>
																	</td>
																	<td align="left" class="bodyText"><a href='<%# GetBroadcastChannelUrl (DataBinder.Eval (Container.DataItem, "name_no_spaces").ToString ())%>'><%# TruncateWithEllipsis (DataBinder.Eval (Container.DataItem, "name").ToString (), 40)%></a></td>
																	<td align="center" class="bodyText"><%# DataBinder.Eval(Container.DataItem, "number_of_members") %></td>
																	<td align="center" class="bodyText">
																		<asp:hyperlink CssClass="adminLinks" runat="server" NavigateURL='<%#GetChannelRemoveScript (Convert.ToInt32 ( DataBinder.Eval(Container.DataItem, "community_id")), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "is_personal"))) %>' ToolTip="Remove" ID="Hyperlink2">remove</asp:hyperlink>
																	</td>
																</tr>
															</AlternatingItemTemplate>							
														</asp:Repeater>	
														</table>				
													</ajax:ajaxpanel>
													<table cellpadding="0" cellspacing="0" border="0" width="95%">
														<tr Class="LineItemEven">
															<td align="right">&nbsp;</td>
														</tr>
														<tr Class="LineItemEven">
															<td align="right">
																<asp:LinkButton id="lnkAddChannel" OnClick="lnkAddChannel_Click" CausesValidation="False" CssClass="adminLinks" runat="server" Text="add to a community"/>
															&nbsp;</td>
														</tr>
													</table>													
													<span class="cb"><span class="cl"></span></span>
												</div>
											</td>
										</tr>
									</table>
									<!-- COPYRIGHT PROTECTION -->	
									<table border="0" cellspacing="0" cellpadding="0" width="100%">
										<tr>
											<td>
												<div class="module whitebg">
													<span class="ct"><span class="cl"></span></span>
													<h2>Copyright Protection</h2>
														<table border="0" cellpadding="0" cellspacing="0" align="center" width="95%">
															<tr>
																<td colspan="3"><img src="~/images/spacer.gif" width="1" height="5" /></td>
															</tr>
															<tr>
																<td id="tdShowCopyright" colspan="3" align="left" >                    
																	<table cellpadding="2" cellspacing="0" border="0" style="width:100%">
																		<tr>
																			<td valign="top" align="right">&nbsp;<b>copyright:</b></td>
																			<td><img src="~/images/spacer.gif" width="5" /></td>
																			<td valign="top" align="left">
																				<asp:Label id="lblLicense" runat="server"/>&nbsp;<asp:Label id="lblLicenseURL" runat="server"/>
																			</td>
																			<td valign="top" align="right">
																				<A href="javascript:void(0);" runat="server" id="aBrowse" onclick="Element.hide('tdShowCopyright');Element.show('tdEditCopyright');return(false);" class="adminLinks">change copyright</A>
																			</td>
																		</tr>
																	</table>
																	<br/><br/>
																</td>
															</tr>
															<tr>
																<td id="tdEditCopyright" colspan="3" align="left" style="display:none;">                    
																	<table cellpadding="0" cellspacing="0" border="0" style="width=95%">
																		<tr>
																			<td><img runat="server" src="~/images/spacer.gif" width="20" ID="Img12"/></td>
																			<td align="left" valign="top" class="bodyText">
																				<br><BR><a target="_resource" runat="server" href="http://docs.kaneva.com/bin/view/Public/KanevaFaqEditThisAssetDescriptions#FaqLicense" class="LnavText" ID="A1">Need help chosing a license?</a><BR><BR>
																				
																				<input type="radio" id="optPersonal" checked name="optLicense" value="P" runat="server"/>&nbsp;Copyright - Personal Usage Only<BR>
																						<img runat="server" src="~/images/spacer.gif" width="40" height="20" ID="Img13"/>
																								<asp:TextBox ID="txtPerAdditional" class="formKanevaText" style="width:500px" TextMode="multiline" Rows="4" runat="server"/><BR><BR>           
																				<input type="radio" id="optCommercial" name="optLicense" value="I" runat="server"/>&nbsp;Copyright - May be Used for Commercial Use.<BR>
																						<img runat="server" src="~/images/spacer.gif" width="40" height="20" ID="Img14"/>
																								<asp:TextBox ID="txtCommAdditional" class="formKanevaText" style="width:500px" TextMode="multiline" Rows="4" runat="server"/><BR><BR>          
																				<input type="radio" id="optCC" name="optLicense" value="C" runat="server"/>&nbsp;<a target="_resource" runat="server" href="http://creativecommons.org" ID="A2">Creative Commons</a><BR>
																						<img runat="server" src="~/images/spacer.gif" width="40" height="20" ID="Img15"/>
																						<asp:DropDownList class="formKanevaText" id="drpCCLicense" runat="Server" Width="250px">
																								<asp:ListItem  value="http://creativecommons.org/licenses/by/2.5/">Attribution</asp:ListItem >
																								<asp:ListItem  value="http://creativecommons.org/licenses/by-nd/2.5/">Attribution-NoDerivs</asp:ListItem >
																								<asp:ListItem  value="http://creativecommons.org/licenses/by-nc-nd/2.5/">Attribution-NonCommercial-NoDerivs</asp:ListItem >
																								<asp:ListItem  value="http://creativecommons.org/licenses/by-nc/2.5/">Attribution-NonCommercial</asp:ListItem >
																								<asp:ListItem  value="http://creativecommons.org/licenses/by-nc-sa/2.5/">Attribution-NonCommercial-ShareAlike</asp:ListItem >
																								<asp:ListItem  value="http://creativecommons.org/licenses/by-sa/2.5/">Attribution-ShareAlike</asp:ListItem >
																						</asp:DropDownList>
																						<a target="_resource" runat="server" href="http://creativecommons.org/licenses/" class="LnavText" ID="A3">What do these mean?</a>
																						<a target="_resource" runat="server" href="http://creativecommons.org/license/?partner=kaneva&exit_url=http://www.kaneva.com&partner_icon_url=http://www.kaneva.com/images/klogo.gif" class="LnavText" ID="A4">Selection Wizard...</a>
																						<br/>
																						<img runat="server" src="~/images/spacer.gif" width="40" height="10" ID="Img16"/><span style="color:orange;font-family:arial; font-size: 12px;">* Note: If your Creative Common license type is not found here, please use the 'Other' fields below</span>
																						
																						<BR>    
																				<input type="radio" id="optPublic" name="optLicense" value="D" runat="server"/>&nbsp;Public Domain (Entire copyright is granted to the public, all use is permitted)<BR>
																				<input type="radio" id="optOther" name="optLicense" value="O" runat="server"/>&nbsp;Other<BR>
																				<img runat="server" src="~/images/spacer.gif" width="40" height="20" ID="Img17"/>
																						name:<asp:TextBox ID="txtLicenseName" class="formKanevaText" style="width:500px" MaxLength="50" runat="server"/><BR>
																				<img runat="server" src="~/images/spacer.gif" width="40" height="20" ID="Img18"/>
																						more details http://<asp:TextBox ID="txtLicenseURL" class="formKanevaText" style="width:500px" MaxLength="100" runat="server"/><BR><BR>
																				</td>
																			<td align="right" valign="top">
																				<!--<button type="button" class="Filter2" onclick="Element.hide('tdEditCopyright');Element.show('tdShowCopyright');">&nbsp;&nbsp;&nbsp;hide&nbsp;&nbsp;&nbsp;</button> -->
																			</td>
																		</tr>
																	</table>	                         
																</td>
															</tr>
															
															<tr>
																<td colspan="3"><img src="~/images/spacer.gif" width="1" height="5" /></td>
															</tr>
															 <tr>
																<td colspan="3" align="right" class="underline"><img src="~/images/spacer.gif" width="5" height="10" /></td>
                                                            </tr>
                                                            <tr>
																<td colspan="3" align="right"><img src="~/images/spacer.gif" width="5" height="10" /></td>
                                                            </tr>
															 <tr>
																<td align="left" >
																	<a runat="server" class="dateStamp" href="http://docs.kaneva.com/bin/view/Public/KanevaFaqEditThisAssetDescriptions" target="_resource" ID="A7">Need help with item fields?</a>
																	<br>&nbsp;<font color="red">*</font>Indicates required fields.
																</td>
																<td colspan="2" align="right">
																	<asp:button id="btnCancel" runat="Server" onclick="btnCancel_Click" CausesValidation="False" class="Filter2" Text="  cancel  "/>&nbsp;&nbsp;<asp:button id="btnUpdate" runat="Server" onClick="btnUpdate_Click" CausesValidation="False" class="Filter2" Text="  submit  "/>
																</td>
                                                            </tr>
														</table>
													<span class="cb"><span class="cl"></span></span>
												</div>
											
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td class=""><img src="~/images/spacer.gif" runat="server" width="1" height="1" /></td>
				</tr>
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<ajax:ajaxpanel ID="Ajaxpanel1" runat="server">
                
        <asp:Repeater id="dgrdAttributes" runat="server" EnableViewState="True">
            <ItemTemplate>
                <tr class="lineItemOdd">
                    <td align="right" class="bodyText" style="border-left: 1px solid #ededed;">
						&nbsp;<span title='<%# DataBinder.Eval (Container.DataItem, "description") %>'><%# DataBinder.Eval (Container.DataItem, "name") %></span>:
                    </td>
                    <td><img runat="server" src="~/images/spacer.gif" width="5" ID="Img19"/></td>
                    <td align="left" width="705" class="bodyText" style="border-right: 1px solid #ededed;">
                        <asp:DropDownList Visible='<%# IsVisible (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "attribute_id")), Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "type")), 2)%>' class="filter2" id="drpAttributeValue" DataSource='<%# GetAttributeValuesList (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "attribute_id")))%>' DataTextField="name" DataValueField="category_attribute_value_id" SelectedIndex='<%# GetAssetAttributeValueIndex (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "attribute_id")))%>' runat="Server" style="width:350px"/>
                        <asp:TextBox Visible='<%# IsVisible (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "attribute_id")), Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "type")), 1)%>' ID="txtAttributeValue" class="Filter2" style="width:350px" MaxLength="255" runat="server" Text='<%# GetAssetAttributeValue (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "attribute_id")))%>'/>
                        <asp:TextBox Visible='<%# IsVisible (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "attribute_id")), Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "type")), 3)%>' ID="txtAttributeValueMultiline" class="Filter2" style="width:500px" TextMode="multiline" Rows="8" MaxLength="255" runat="server" Text='<%# GetAssetAttributeValue (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "attribute_id")))%>'/>
                        <input type="hidden" runat="server" id="hidAttributeId" value='<%#DataBinder.Eval (Container.DataItem, "attribute_id")%>' NAME="hidAttributeId">
                        <input type="hidden" runat="server" id="hidAttributeType" value='<%#DataBinder.Eval (Container.DataItem, "type")%>' NAME="hidAttributeType">
                    </td>
                </tr>
            </ItemTemplate>
            <AlternatingItemTemplate>
                <tr class="lineItemEven">
                    <td align="right" class="bodyText" style="border-left: 1px solid #ededed;">
						&nbsp;<span title='<%# DataBinder.Eval (Container.DataItem, "description") %>'><%# DataBinder.Eval (Container.DataItem, "name")%></span>:
                    </td>
                    <td><img runat="server" src="~/images/spacer.gif" width="5" ID="Img20"/></td>
                    <td align="left" width="705" class="bodyText" style="border-right: 1px solid #ededed;">
                        <asp:DropDownList Visible='<%# IsVisible (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "attribute_id")), Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "type")), 2)%>' class="filter2" id="Dropdownlist2" DataSource='<%# GetAttributeValuesList (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "attribute_id")))%>' DataTextField="name" DataValueField="category_attribute_value_id" SelectedIndex='<%# GetAssetAttributeValueIndex (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "attribute_id")))%>' runat="Server" style="width:350px"/>
                        <asp:TextBox Visible='<%# IsVisible (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "attribute_id")), Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "type")), 1)%>' ID="Textbox6" class="Filter2" style="width:350px" MaxLength="255" runat="server" Text='<%# GetAssetAttributeValue (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "attribute_id")))%>'/>
                        <asp:TextBox Visible='<%# IsVisible (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "attribute_id")), Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "type")), 3)%>' ID="Textbox7" class="Filter2" Wrap="False" style="width:500px;" TextMode="multiline" Rows="8" MaxLength="255" runat="server" Text='<%# GetAssetAttributeValue (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "attribute_id")))%>'/>
                        <input type="hidden" runat="server" id="Hidden1" value='<%#DataBinder.Eval (Container.DataItem, "attribute_id")%>' NAME="Hidden1">
                        <input type="hidden" runat="server" id="Hidden2" value='<%#DataBinder.Eval (Container.DataItem, "type")%>' NAME="Hidden2">
                    </td>
                </tr>           
            </AlternatingItemTemplate>
        </asp:Repeater>
                

</ajax:ajaxpanel>  
        
        
<input type="hidden" runat="server" id="GameLicId" value="0" NAME="GameLicId"> 

<input type="hidden" runat="server" id="hidChannelId" value="0" NAME="hidChannelId"> 
<asp:button ID="btnRemoveChannel" OnClick="btnRemoveChannel_Click" runat="server" Visible="false"></asp:button>