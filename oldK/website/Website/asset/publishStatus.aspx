<%@ Page language="c#" Codebehind="publishStatus.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.publishStatus" %>
<table cellpadding="0" cellspacing="0" border="0" width="560" ID="Table1">
	<tr>
	<td valign="middle" class="mykan1" width="125" height="23" align="left">&nbsp;</td><td valign="middle" class="mykan2" width="218" height="23" align="left">&nbsp;</td><td width="217" height="23" align="right" class="mykan3" valign="middle">&nbsp;&nbsp;&nbsp;&nbsp;</td>						
	</tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="560">						
	<tr class="lineItemColHead">
		<td valign="middle" align="center" width="40%" style="border-left: 1px solid #666666;">status</td>
		<td valign="middle" align="center" width="60%" style="border-right: 1px solid #666666;">description</td>
	</tr>
	<tr>
		<td valign="top" class="lineItemEven" width="30%" style="border-left: 1px solid #666666;"><a name='ConnectedMedia'>Shared media</a></td>
		<td valign="middle" class="lineItemEven" width="70%" align="left" style="border-right: 1px solid #666666;">This is an item another member owns and you've added to your Shared Media collection because you think it's cool. You can share it with members and friends in your profile and communities, but you can't edit its description. </td>
	</tr>

	<asp:Repeater id="rptTransactionStatus" runat="server">
		<ItemTemplate>
			<tr>
				<td valign="top" class="lineItemOdd" width="30%" style="border-left: 1px solid #666666;"><a name='<%# DataBinder.Eval(Container.DataItem, "status_id")%>'><%# DataBinder.Eval(Container.DataItem, "name")%></a></td>
				<td valign="middle" class="lineItemOdd" width="70%" align="left" style="border-right: 1px solid #666666;"><%# DataBinder.Eval(Container.DataItem, "description")%></td>
			</tr>
		</ItemTemplate>
		<AlternatingItemTemplate>
			<tr>
				<td valign="top" class="lineItemEven" width="30%" style="border-left: 1px solid #666666;"><a name='<%# DataBinder.Eval(Container.DataItem, "status_id")%>'><%# DataBinder.Eval(Container.DataItem, "name")%></a></td>
				<td valign="middle" class="lineItemEven" width="70%" align="left" style="border-right: 1px solid #666666;"><%# DataBinder.Eval(Container.DataItem, "description")%></td>
			</tr>
		</AlternatingItemTemplate>
	</asp:Repeater>	
	<tr class="lineItemColHead">
		<td colspan="100%" style="border-bottom: 1px solid #666666;">&nbsp;</td>
	</tr>		
</table>	