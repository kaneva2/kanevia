<%@ Page language="c#" Codebehind="redeemLogin.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.redeemLogin" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>


<link href="css/new.css" rel="stylesheet" type="text/css">	

<link href="css/themes-join/template.css" rel="stylesheet" type="text/css">	
<link href="css/themes-join/join_dp3d.css" rel="stylesheet" type="text/css">	


<table cellpadding="0" cellspacing="0" border="0" width="790" align="center">
	<tr>
		<td colspan="3"><img runat="server" src="~/images/spacer.gif" width="1" height="14" id="Img12"/></td>
	</tr>
	<tr>
		<td colspan="3">
			<table  border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="frTopLeft"></td>
					<td class="frTop"></td>
					<td class="frTopRight"></td>
				</tr>
				<tr>
					<td class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" id="Img13" width="1" height="1" /></td>
					<td valign="top" class="frBgIntMembers">
						<table width="100%" border="0" cellpadding="0" cellspacing="0" >
							<tr>
								<td width="770" align="left"  valign="top">
								
									<table cellpadding="0" cellspacing="0" border="0"  width="100%">
										<tr>
											<td valign="top">
												<div id="templateHeader"><img runat="server" src="~/images/header_border.gif" alt="header_border.gif" width="768" height="120" border="0" id="Img14"><h1>Kaneva Join Process</h1></div>
											</td>
										</tr>
										<tr>
											<td width="790" valign="top">
												
												<table border="0" cellspacing="0" cellpadding="0" width="100%">
													<tr>
														<!-- START WIZARD CONTENT -->
														<td valign="top" align="center">
															
															<table border="0" cellspacing="0" cellpadding="0" width="670" align="center" class="wizform">
																<tr>
																	<td width="670">

																	<h1>Welcome to Dance Party 3D!</h1>
																	<h2>Available exclusively in the Virtual World of Kaneva</h2>
																	
																	<p>If you're already a Kaneva member, sign in below using your email address and password.  If you're new to Kaneva, click the "Become a Member" button.</p>
																																		
																	
																	<ajax:ajaxpanel id="ajpError" runat="server">
																	    <h2 class="alertmessage"><span id="spnAlertMsg" runat="server"></span></h2>
																	</ajax:ajaxpanel><br />
																	
																	 <table border="0" cellspacing="0" cellpadding="0" width="670">
																			<tr>
																				<td width="300">
																			
																					<div class="module">
																					<span class="ct"><span class="cl"></span></span>
																					
																					<h2>Already a Member of Kaneva?</h2>
																					<h3>Login below to redeem your card</h3>
																					
																					<br>
																						<table width="220" border="0" cellpadding="0" cellspacing="0" align="center" class="wizform nopadding">
																							<tr>
																								<td height="19">Email<br><input type="text" style="width: 220px;" class="biginput" maxlength="100" id="txtUserName" runat="server" name="txtUserName"></td>
																							</tr>
																							<tr>
																								<td height="15"></td>
																							</tr>
																							<tr>
																								<td>Password<br><input type="password" style="width: 220px;" class="biginput" maxlength="30" id="txtPassword" runat="server" onkeydown="javascript:checkEnter(event)" name="txtPassword"></td>
																							</tr>
																							<tr>
																								<td height="10"></td>
																							</tr>
																							<tr>
																								<td height="10"><asp:checkbox runat="server" id="chkRememberLogin" tooltip="Remember Password"/> Remember my login info</td>
																							</tr>
																							<tr>
																								<td height="10"></td>
																							</tr>
																							<tr>
																								<td valign="middle"><a runat="server" id="aLostPassword" style="vertical-align:top;">Forgot password?</a></td>
																							</tr>
																							<tr>
																								<td height="15"></td>
																							</tr>
																							<tr>
																								<td align="center"><asp:imagebutton id="imgLogin" runat="server" causesvalidation="False" alternatetext="Sign in to Kaneva" 
																								imageurl="~/images/btn_signin.gif" onclick="imgLogin_Click" width="128" height="25" border="0" /></td>
																							</tr>
																							
																							
																					</table>
																					<span class="cb"><span class="cl"></span></span>
																					</div>	
																				
																				</td>
																				<td width="70"></td>
																				<td valign="top">
																				
																					<div class="module">
																					<span class="ct"><span class="cl"></span></span>
																					<h2>Not a Member yet?</h2>
																					<h3>Join and Redeem your card now</h3>
																					
																					<br><br>
																						<table width="300" border="0" cellpadding="0" cellspacing="0" class="wizform nopadding">
																							<tr>
																								<td align="center"><a runat="server" id="aRegister"><img src="images/btn_becomemember.gif" width="160" height="25" border="0" /></a></td>
																							</tr>
																						</table>
																					<span class="cb"><span class="cl"></span></span>
																					</div>	
																				
																				</td>
																			</tr>
																		</table>
																		
																					
																		 
																	</td>
																</tr>
															</table>
															
														</td>
														
														<!-- END WIZARD CONTENT -->
													</tr>
												</table>
											
											</td>
											
										</tr>
										
									</table>
											
								</td>
							</tr>
							
						</table>
					</td>
					<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" id="Img15" width="1" height="1" /></td>
				</tr>
				<tr>
					<td class="frBottomLeft"></td>
					<td class="frBottom"></td>
					<td class="frBottomRight"></td>
				</tr>
			</table>				
		</td>
	</tr>
</table>
<br/><br/><br/><br/>

