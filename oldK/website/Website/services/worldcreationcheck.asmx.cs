///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System.Web.Services;
using Kaneva.BusinessLayer.Facade;
using Newtonsoft.Json;

namespace KlausEnt.KEP.Kaneva.services
{
    /// <summary>
    /// Summary description for worldcreationcheck
    /// </summary>
    [WebService (Namespace = "http://www.kaneva.com/")]
    [WebServiceBinding (ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem (false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class worldcreationcheck : System.Web.Services.WebService
    {
        [WebMethod]
        public string HasWorldCreationCompleted (string token)
        {
            var results = (new Common()).CheckAsyncTaskCompleteKey(token);
            if (!string.IsNullOrWhiteSpace(results.TaskKey))
                return JsonConvert.SerializeObject(results);
            return string.Empty;
        }
    }
}
