///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Kaneva
{
    public partial class AJAXadrotator : System.Web.UI.Page
    {

        #region Declerations

        private string _adRotatorGroup;

        #endregion

        #region Page Load

        protected void Page_Load(object sender, EventArgs e)
        {
            GetRequestParams();
            ProcessAdRequest();
        }

        #endregion

        #region Helper Functions

        private void GetRequestParams()
        {
            if (Request["ad"] != null || Request["ad"] != "")
            {
                //get the ad group name to determine which add randmoizer should be processed
                _adRotatorGroup = Convert.ToString(Request["ad"]);
            }
        }

        private int VIP_STEP
        {
            get
            {
                if (Session["vip_step"] == null)
                {
                    Session["vip_step"] = 0;
                }
                return (int)Session["vip_step"];
            }
            set
            {
                Session["vip_step"] = value;
            }
        }

        private int AP_STEP
        {
            get
            {
                if (Session["ap_step"] == null)
                {
                    Session["ap_step"] = 0;
                }
                return (int)Session["ap_step"];
            }
            set
            {
                Session["ap_step"] = value;
            }
        }

        #endregion

        #region Functions

        private void ProcessAdRequest()
        {
            string image = "<img src=\"";
            Random imageNumber = new Random();

            switch (_adRotatorGroup.ToLower())
            {
                case "island":
                    switch (imageNumber.Next(0, 8))
                    {
                        case 0:
                            image += ""; 
                            break;
                        case 1:
                            image += "";
                            break;
                        case 2:
                            image += "";
                            break;
                        case 3:
                            image += "";
                            break;
                        case 4:
                            image += "";
                            break;
                        case 5:
                            image += "";
                            break;
                    }
                    break;
                case "vip":
                    //switch (imageNumber.Next(0, 7))
                    switch (VIP_STEP)
                    {
                        case 0:
                            image += "../mykaneva/img/VIP_0000_frame01.jpg\" alt=\"VIP Pass\" ";
                            break;
                        case 1:
                            image += "../mykaneva/img/VIP_0001_frame02.jpg\" alt=\"VIP Pass\" ";
                            break;
                        case 2:
                            image += "../mykaneva/img/VIP_0002_frame03.jpg\" alt=\"VIP Pass\" ";
                            break;
                        case 3:
                            image += "../mykaneva/img/VIP_0003_frame04.jpg\" alt=\"VIP Pass\" ";
                            break;
                        case 4:
                            image += "../mykaneva/img/VIP_0004_frame05.jpg\" alt=\"VIP Pass\" ";
                            break;
                        case 5:
                            image += "../mykaneva/img/VIP_0005_frame06.jpg\" alt=\"VIP Pass\" ";
                            break;
                        case 6:
                            image += "../mykaneva/img/VIP_0006_frame07.jpg\" alt=\"VIP Pass\" ";
                            break;
                        case 7:
                            image += "../mykaneva/img/VIP_0007_frame08.jpg\" alt=\"VIP Pass\" ";
                            break;
                    }
                    //set the next image to show
                    VIP_STEP = ((VIP_STEP + 1) / 7 > 0 ? 0 : VIP_STEP + 1);
                    break;
                case "ap":
                    //switch (imageNumber.Next(0, 8))
                    switch (AP_STEP)
                    {
                        case 0:
                            image += "../mykaneva/img/AP_0000_frame01.jpg\" alt=\"Access Pass\" ";
                            break;
                        case 1:
                            image += "../mykaneva/img/AP_0001_frame02.jpg\" alt=\"Access Pass\" ";
                            break;
                        case 2:
                            image += "../mykaneva/img/AP_0002_frame03.jpg\" alt=\"Access Pass\" ";
                            break;
                        case 3:
                            image += "../mykaneva/img/AP_0003_frame04.jpg\" alt=\"Access Pass\" ";
                            break;
                        case 4:
                            image += "../mykaneva/img/AP_0004_frame06.jpg\" alt=\"Access Pass\" ";
                            break;
                        case 5:
                            image += "../mykaneva/img/AP_0005_frame07.jpg\" alt=\"Access Pass\" ";
                            break;
                        case 6:
                            image += "../mykaneva/img/AP_0006_frame08.jpg\" alt=\"Access Pass\" ";
                            break;
                        case 7:
                            image += "../mykaneva/img/AP_0007_frame09.jpg\" alt=\"Access Pass\" ";
                            break;
                        case 8:
                            image += "../mykaneva/img/AP_0008_frame10.jpg\" alt=\"Access Pass\" ";
                            break;
                    }
                    //set the next image to show

                    AP_STEP = ((AP_STEP + 1) / 8 > 0 ? 0 : AP_STEP + 1);
                    break;
            }

            //return the image to the calling page
            Response.Write(image + "\" style=\"top:-20px\" />");

        }

        #endregion


    }
}
