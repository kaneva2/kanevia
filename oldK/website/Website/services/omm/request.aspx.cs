///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections;
using log4net;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
	// TODO:
	/*  
		NOTE ABOUT ADVERTISING

		See all the TODO:s in this file.  The idea is that a context is set
		when the request is being built, then in calls to 
		addXml, prepareXmlResponse, and closeXmlResponse will have calls
		into the ad server to insert ad widgets

		NOTE ABOUT EMBEDDED

		If a playlist is embedded, which you can't do today, we'll need to 
		know that and *not* check for mature even if you are *not* logged in.  
		This allows mature content in a playlist to be displayed outside of Kaneva
	*/
	
	/// <summary>
	/// Summary description for request.
	/// </summary>
	public class request : BasePage
	{
		// 7/06 added hack to figure out embedded, see checkAssetId()
		private bool m_isEmbedded = false; 
		
		//===============================================================================
		// data likely to change
		public const Int32 DEFAULT_TICKER_DISPLAY_TIME = 10; // seconds
		public const Int32 ERROR_MESSAGE_DISPLAY_TIME  = 30;
		public const Int32 MAX_PLAYLIST_SIZE = 20;

		// from assetdetails.aspx.cs
		public const Int32 	DEFAULT_SHARE_HEIGHT 	= 390;
		public const Int32 	DEFAULT_SHARE_WIDTH 	= 331; // updated 8/1/06
		private const int 	THUMB_WIDTH 			= Constants.PICTURE_DIMENSIONS_SMALL;
		private const int 	THUMB_HEIGHT 			= Constants.PICTURE_DIMENSIONS_SMALL;

		static private string REPORT_CATEGORY 		= "KANEVA Web Site";

		// messages send to clients
		static private string MSG_ADDED_TO_LIB 		= "Added to your media library.";
		static private string MSG_ALREADY_IN_LIB	= "Already added to your media library.";
		static private string MSG_ALREADY_DUG		= "You�ve already raved this item.";
		static private string MSG_DUGG				= "Item raved!";		
		static private string MSG_REPORT_ABUSE 		= "Thanks! Your comments have been submitted.";
		static private string REPORT_SUMMARY		= "Abuse reported via OMM";
		static private string ENTER_REPORT_DETAILS	= "Please enter report details.";
		static private string MSG_LOGIN				= "Click to logon to Kaneva";
		static private string MSG_YOUR_LIB			= "Your media library";
		static private string MSG_ALREADY_OWNED		= "You own this item.";
		static private string [] ERR_MSGS = new string[6] { "Unknown Error: ", "Asset not found or insufficient rights.", "You must be signed in to", "Invalid query string.", "An exception occurred:", "Media rated for members over 18 years old." };
		// strings appended to error messages 
		static private string ADDL_ERRMSG_GETPLAYLISTS	= "get the playlists";
		static private string ADDL_ERRMSG_RAVE_ITEM		= "rave the item";
		static private string ADDL_ERRMSG_ADDMEDIA		= "add to your media library";

		// OMM widget (not Kaneva widget) info
		static private string TICKER_WIDGET_URI = "~/flash/omm/Ticker.swf";
		static private int    TICKER_WIDGET_ID  = 5;

		//===============================================================================

		// boolean values in XML
		private const string TRUE_VALUE			= "true";
		private const string FALSE_VALUE		= "false";

		// abuse types for query string
		public const string REPORT_COPYRIGHT_VIOLATION 	= "c";
		public const string REPORT_ILLEGAL 				= "i";
		public const string REPORT_MATURE_VIOLATION		= "m";
		public const string REPORT_OTHER				= "o";
		
		// ids of data in the asset_attribute_values table
		// there are two release dates, we only pass out one
		private const int RELEASE_DATE_A = 32;
		private const int RELEASE_DATE_B = 34;
		private const int AUTHORS		= 43;
		private const int ALBUM			= 35;
		//  authors (43), release date (32 and 34), album (35)
		private const string ATTR_LIST = " 32, 34, 35, 43 "; // list inserted into SQL

		// data types in database
		private const int VIDEO_TYPE = 2;
		private const int MUSIC_TYPE = 4;
		private const int IMAGE_TYPE = 5;

		private const int GENERAL_ERROR_ID = 0;
		private const int NOT_LOGGED_ON_ERROR_ID = 99;

		private const int LOG_MSG_TYPE = 99; 

		private const int NO_XML				= 0;
		private const int CONFIG_XML			= 1;
		private const int PLAYLIST_WIDGET_XML	= 2;
		private const int SINGLE_WIDGET_XML		= 4;

		private	string	m_XmlResponse;

		// format it prettier if debugging
#if DEBUG
		static private string dbgCrLf = "\r\n";
		static private string dbgIndent = "  ";

#if TESTING		
		static private string dtd = "<!DOCTYPE omm-resp SYSTEM \"http://localhost/Flash/OMM/src/web/dtd/kaneva.omm.response.v1.3.dtd\">" + dbgCrLf;
#else
		static private string dtd = ""; 
#endif

#else
		static private string dbgCrLf = "";
		static private string dbgIndent = "";
		static private string dtd = "";
#endif

		// mapped to strings in makeErrorXml
		private enum eOMMError 
		{
			ASSET_NOT_FOUND 	= 1,
			INVALID_AUTH 		= 2,
			INVALID_ARGS 		= 3,
			EXCEPTION			= 4,
			ASSET_RESTRICTED	= 5
		}

		// Logger
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);
		
		/// <summary>
		/// class used to make a list of ratings and their descriptions
		/// </summary>
		private class RatingInfo
		{
			public RatingInfo( bool m, string s ) { isMature = m; name = s; }
			public bool 	isMature = true;
			public string	name = "";
				
		};
		
		/// <summary>
		/// class for passing in multiple ticker messages to function for building XML
		/// </summary>
		private class TickerInfo
		{
			public TickerInfo( string m, Int32 dt, Int32 ti, string u ) { msg = m; displayTime = dt; typeId = ti; url = u; }
			public string msg;
			public Int32 displayTime;
			public Int32 typeId;
			public string url;
		}

		/// <summary>
		/// build complete error XML 
		/// </summary>
		/// <param name="error"></param>
		/// <param name="addlMsg"></param>
		private void setErrorXml( eOMMError error, string addlMsg  )
		{
			prepareXmlResponse(0,0,0,NO_XML);
			addXml( makeErrorXml( error, addlMsg ), error == eOMMError.INVALID_AUTH ? NOT_LOGGED_ON_ERROR_ID : GENERAL_ERROR_ID );
			closeXmlResponse();
		}

		/// <summary>
		/// print to debugger and log file
		/// </summary>
		/// <param name="msg"></param>
		private void debugPrint( string msg )
		{
			//System.Diagnostics.Debug.WriteLine( "OMM Debug: " + msg );
			//m_logger.Debug( msg );
		}
		
		/// <summary>
		/// wrapper function to check for embedding and get the asset id
		/// this uses the hack in StoreUtility that sets the id to a 
		/// negative number if embedded
		/// </summary>
		/// <returns></returns>
		private Int32 checkAssetId()
		{
			Int32 ret = Convert.ToInt32( Request[Constants.QS_ASSETID] );
			m_isEmbedded = ret < 0; 
			if ( ret < 0 )
				ret = -ret;

			return ret;
		}

		/// <summary>
		/// main line of this page
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Page_Load(object sender, System.EventArgs e)
		{
			Response.Expires = 0;
			Response.CacheControl = "no-cache";

			// default to invalid arg
			setErrorXml( eOMMError.INVALID_ARGS, null );
			
			try
			{
				// They don't need to be logged in
				if ( Request[Constants.QS_TYPE] != null)
				{
					Int32 typeId = Convert.ToInt32 (Request [Constants.QS_TYPE]);
					int userId = GetUserId ();

					if ( typeId < LOG_MSG_TYPE )
					{
						debugPrint( "Request of type " + typeId );
					}

					switch ( typeId )
					{
						case Constants.GET_FULL_METADATA:
						case Constants.GET_STATIC_METADATA:
						case Constants.GET_DYNAMIC_METADATA:
							if ( Request[Constants.QS_ASSETID] != null )
							{ 
								getMetaData( userId, typeId, checkAssetId() );
							}
							break;

						case Constants.GET_PLAYLIST_LIST:
							if (!Request.IsAuthenticated)
							{
								setErrorXml( eOMMError.INVALID_AUTH, ADDL_ERRMSG_GETPLAYLISTS  );
							}
							else
							{
								getPlayListList( userId );
							}
							break;
						
						case Constants.GET_PLAYLIST:
							if ( Request[Constants.QS_PLAYLISTID] != null )
							{
								Int32 playListId = Convert.ToInt32( Request[Constants.QS_PLAYLISTID] );
								if ( playListId == Constants.COOL_ITEM_PLAYLIST_ID || playListId == Constants.COOL_ITEM_PLAYLIST_ID_HOME ||
									playListId == Constants.COOL_ITEM_PLAYLIST_ID_MEMBER_CHANNEL || playListId == Constants.COOL_ITEM_PLAYLIST_ID_BROADBAND_CHANNEL ||
									playListId == Constants.COOL_ITEM_PLAYLIST_ID_MEDIA)
								{
									getPlayList( userId, playListId, Request[Constants.QS_FROM_GAME] != null  );
								}
								else if ( playListId == Constants.USER_ALL_VIDEO_PLAYLIST_ID)
								{
									if ( Request[Constants.QS_USERID] != null)
									{
										getPlayList (Convert.ToInt32 (Request[Constants.QS_USERID]), Constants.USER_ALL_VIDEO_PLAYLIST_ID, Request[Constants.QS_FROM_GAME] != null);
									}
								}
								else if ( playListId == Constants.USER_ALL_MUSIC_PLAYLIST_ID)
								{
									if ( Request[Constants.QS_USERID] != null)
									{
										getPlayList (Convert.ToInt32 (Request[Constants.QS_USERID]), Constants.USER_ALL_MUSIC_PLAYLIST_ID, Request[Constants.QS_FROM_GAME] != null);
									}
								}
								else
								{
									getPlayList( userId, playListId, Request[Constants.QS_FROM_GAME] != null );
								}
							}
							break;
						
						case Constants.ADD_RAVE:
							if (!Request.IsAuthenticated)
							{
								setErrorXml( eOMMError.INVALID_AUTH, ADDL_ERRMSG_RAVE_ITEM );
							}
							else if ( Request[Constants.QS_ASSETID] != null )
							{
								addRave( userId, checkAssetId() );
							}
							break;
						
						case Constants.ADD_TO_FAVORITES:
							if (!Request.IsAuthenticated)
							{
								setErrorXml( eOMMError.INVALID_AUTH, ADDL_ERRMSG_ADDMEDIA  );
							}
							else if ( Request[Constants.QS_ASSETID] != null )
							{
								addToFavorites( userId, checkAssetId() );
							}
							break;

						case Constants.GET_SHARE_DATA:
							if ( Request[Constants.QS_ASSETID] != null )
							{
								getShareData( userId, checkAssetId() );
							}
							break;

						case Constants.REPORT_ITEM:
							if ( Request[Constants.QS_ASSETID] != null && 
								Request[Constants.QS_REPORT_EMAIL] != null && 
								Request[Constants.QS_REPORT_TYPE] != null && 
								Request[Constants.QS_REPORT_COMMENT] != null )
							{
								reportAbuse( userId, checkAssetId(), Request[Constants.QS_REPORT_EMAIL],
									Request[Constants.QS_REPORT_TYPE], Request[Constants.QS_REPORT_COMMENT] );
							}
							break;
								 
						case Constants.VIEW_ITEM:
							if ( Request[Constants.QS_ASSETID] != null )
							{
								MediaFacade mediaFacade = new MediaFacade();
                                mediaFacade.UpdateMediaViews(checkAssetId(), Common.GetVisitorIPAddress(), userId);
								prepareXmlResponse( Constants.VIEW_ITEM, userId, 0, 0 );
								closeXmlResponse();
							}
							break;

							// default just prints out the message, if there is one, then returns error
						default:
							if ( Request["msg"] != null )
							{
								debugPrint( Request["msg"] );
							}
							break;
					}
				}
			}
			catch ( Exception ex )
			{
				setErrorXml( eOMMError.EXCEPTION, "" ); // don't send exception to Flash ex.ToString() );
				m_logger.Error ("Error generating XML", ex);
			}

			Response.Write( m_XmlResponse );
		}

		/// <summary>
		/// make XML for the ticker
		/// </summary>
		/// <param name="info">array of ticker info objects</param>
		/// <returns></returns>
		private string makeTickerMessageXml( TickerInfo [] info  )
		{
			string xml = "";

			if ( info == null || info.Length == 0 )
				return xml;

			xml += "<widgets>" + dbgCrLf;

			string baseUrl = Request.Url.GetLeftPart( System.UriPartial.Authority ) + ResolveUrl("~");
			xml += "<widget id=\""+ TICKER_WIDGET_ID + "\" uri=\"" + TICKER_WIDGET_URI.Replace( "~", baseUrl ) + "\">" + dbgCrLf;

			/*	
			<method name="add">
				<arg>55224</arg>
				<arg>Click here to be a Kaneva Beta Tester!</arg>
				<arg>http://beta.kaneva.com</arg>
			</method>
			*/
			for ( int i = 0; i < info.Length; i++ )
			{
				xml += "<method name='add'>" + dbgCrLf;
					xml += "<arg>" + info[i].typeId + "</arg>" + dbgCrLf;
					xml += "<arg>" + Server.HtmlEncode( info[i].msg ) + "</arg>" + dbgCrLf;
					xml += "<arg>" + info[i].displayTime + "</arg>" + dbgCrLf;
					if ( info[i].url != null )
						xml += "<arg>" + Server.HtmlEncode( info[i].url ) + "</arg>" + dbgCrLf;
				xml += "</method>" + dbgCrLf;
			}
			xml += "</widget>" + dbgCrLf;
			xml += "</widgets>" + dbgCrLf;

			return xml;

		}

		/// <summary>
		/// called when they click Add
		/// </summary>
		/// <param name="userId"></param>
		/// <param name="assetId"></param>
		private void addToFavorites( int userId, Int32 assetId )
		{
			string xml = "";

			prepareXmlResponse( Constants.ADD_TO_FAVORITES, userId, assetId, NO_XML );

			if ( StoreUtility.IsAssetOwner (userId, assetId) )
			{
				xml += "<error/>" + dbgCrLf;
				xml += makeTickerMessageXml( new TickerInfo[2] { new TickerInfo( MSG_ALREADY_OWNED, DEFAULT_TICKER_DISPLAY_TIME, Constants.ADD_TO_FAVORITES, null ),
																   new TickerInfo( MSG_YOUR_LIB, DEFAULT_TICKER_DISPLAY_TIME, Constants.ADD_TO_FAVORITES+100, Server.HtmlEncode(Request.Url.GetLeftPart( System.UriPartial.Authority ) + ResolveUrl ("~/mykaneva/mediaFavorites.aspx?userId=" + userId ))) } );
			}
			else
			{

				bool inLib = StoreUtility.IsItemInConnectedMedia (userId, assetId);

				if ( inLib )
				{
					xml += "<error/>" + dbgCrLf;
					xml += makeTickerMessageXml( new TickerInfo[2] { new TickerInfo( MSG_ALREADY_IN_LIB, DEFAULT_TICKER_DISPLAY_TIME, Constants.ADD_TO_FAVORITES, null ),
																	   new TickerInfo( MSG_YOUR_LIB, DEFAULT_TICKER_DISPLAY_TIME, Constants.ADD_TO_FAVORITES+100, Server.HtmlEncode(Request.Url.GetLeftPart( System.UriPartial.Authority ) + ResolveUrl ("~/mykaneva/mediaFavorites.aspx?userId=" + userId ))) } );
				}
				else
				{
					try
					{
						xml = "<confirm>" + dbgCrLf;
					
						// return code doesn't tell us much, just that order id made/found
						bool ret = StoreUtility.AddTag( userId, KanevaWebGlobals.CurrentUser.CommunityId, assetId, Common.GetVisitorIPAddress() );
					
						xml += "</confirm>" + dbgCrLf;
						xml += makeTickerMessageXml( new TickerInfo[2] { new TickerInfo( MSG_ADDED_TO_LIB, DEFAULT_TICKER_DISPLAY_TIME, Constants.ADD_TO_FAVORITES, null ),
																		   new TickerInfo( MSG_YOUR_LIB, DEFAULT_TICKER_DISPLAY_TIME, Constants.ADD_TO_FAVORITES+100, Server.HtmlEncode(Request.Url.GetLeftPart( System.UriPartial.Authority ) + ResolveUrl ("~/mykaneva/mediaFavorites.aspx?userId=" + userId ))) } );
					}
					catch( Exception e )
					{
						xml = makeErrorXml( eOMMError.ASSET_NOT_FOUND, null ); 
						m_logger.Error( "Exception in requeset.addToFavorites", e );
					}
				
				}
			}

			addXml( xml, assetId );
			
			closeXmlResponse();
		}

		/// <summary>
		///  called when they click Rave It
		/// </summary>
		/// <param name="userId"></param>
		/// <param name="assetId"></param>
		private void addRave( int userId, Int32 assetId )
		{
			string xml = "";

			prepareXmlResponse( Constants.ADD_RAVE, userId, assetId, NO_XML );

			DataRow drDigg = StoreUtility.GetDigg( userId, assetId );
			bool alreadyDugg = (drDigg != null);

			if ( alreadyDugg )
			{
				xml += "<error/>" + dbgCrLf;
				xml += makeTickerMessageXml( new TickerInfo[1] { new TickerInfo( MSG_ALREADY_DUG, DEFAULT_TICKER_DISPLAY_TIME, Constants.ADD_RAVE, null ) } );
			}
			else
			{
				try
				{
					xml = "<confirm>" + dbgCrLf;

					// return code either ok, already logged on, or already Raveed
					// so we really don't care
					StoreUtility.InsertDigg( userId, assetId );
					
					xml += "</confirm>" + dbgCrLf;
					xml += makeTickerMessageXml( new TickerInfo[1] { new TickerInfo( MSG_DUGG, DEFAULT_TICKER_DISPLAY_TIME, Constants.ADD_RAVE, null ) } );
				}
				catch( Exception e )
				{
					xml = makeErrorXml( eOMMError.ASSET_NOT_FOUND, null ); 
					m_logger.Error( "Exception in requeset.addRave", e );
				}
			}

			addXml( xml, assetId );
			
			closeXmlResponse();
		}

		/// <summary>
		/// called when they click Report Abuse, and submit the "form"
		/// </summary>
		/// <param name="userId"></param>
		/// <param name="assetId"></param>
		/// <param name="email"></param>
		/// <param name="reportType"></param>
		/// <param name="comment"></param>
		private void reportAbuse( int userId, Int32 assetId , string email,
			string reportType, string comment )
		{
			string xml;

			prepareXmlResponse( Constants.REPORT_ITEM, userId, assetId, NO_XML );

			try
			{
				// Build the Desscription
				string strChecked = "";

				if (reportType == REPORT_COPYRIGHT_VIOLATION )
				{
					strChecked += "\n\tViolation of Copyright Content";
				}
				else if (reportType == REPORT_ILLEGAL )
				{
					strChecked += "\n\tViolation of Inappropriate Content";
				}
				else if (reportType == REPORT_MATURE_VIOLATION)
				{
					strChecked += "\n\tMature Content";
				}
				else if (reportType == REPORT_OTHER )
				{
					strChecked += "\n\tOther problems";
				}

				// Any checked?
				if (strChecked.Length == 0 || reportType.Length == 0 )
				{
					xml = makeErrorXml( eOMMError.INVALID_ARGS, ENTER_REPORT_DETAILS );
				}
				else
				{
					User user = (User) HttpContext.Current.Items ["User"];

					string strComments = "User comment = '" + comment + "'\n" +
						"Items checked = " + strChecked + "\n" +
						"Submitted by username = '" + user.Username + "'\n" +
						"Linked from 'OMM/request.aspx.cs?assetId=" + assetId.ToString() + "'\n" +
						"Asset link " + Request.Url.GetLeftPart( System.UriPartial.Authority ) + StoreUtility.GetAssetDetailsLink( assetId, this ) + "\n" +
						"Kaneva Web Version reported from '" + KanevaGlobals.Version () + "'";

					if (Request.IsAuthenticated)
					{
						strComments += "\nEmail = " + user.Email;
					}
					else
					{
						strComments += "\nEmail = " + email;
					}

					int issueId = StoreUtility.AddMantisIssue( REPORT_CATEGORY, REPORT_SUMMARY, strComments );

					xml = "<confirm/>" + dbgCrLf;
					xml += makeTickerMessageXml(  new TickerInfo[1] { new TickerInfo( MSG_REPORT_ABUSE, DEFAULT_TICKER_DISPLAY_TIME, Constants.REPORT_ITEM, null ) } );
				}
				
			}
			catch( Exception e )
			{
				xml = makeErrorXml( eOMMError.ASSET_NOT_FOUND, null ); 
				m_logger.Error( "Exception in requeset.reportAbuse", e );
			}

			addXml( xml, assetId );
			
			closeXmlResponse();
		}
		
		/// <summary>
		/// return the URL and HTML for embedding the asset 
		/// </summary>
		/// <param name="userId"></param>
		/// <param name="assetId"></param>
		private void getShareData( int userId, Int32 assetId )
		{
			string xml;

			prepareXmlResponse( Constants.GET_SHARE_DATA, userId, assetId, NO_XML );

			try
			{
				xml = "<confirm>" + dbgCrLf;

				string detailsLink = Request.Url.GetLeftPart( System.UriPartial.Authority ) + StoreUtility.GetAssetDetailsLink( assetId, this ); // need full URL
				string embedLink = StoreUtility.GetAssetEmbedLink( assetId, DEFAULT_SHARE_HEIGHT, DEFAULT_SHARE_WIDTH, this, false, true );

				xml += "<share-data>" + dbgCrLf;

				xml += "<details-url>" + Server.HtmlEncode(detailsLink) + "</details-url>" + dbgCrLf;
				xml += "<embed-info>" +  Server.HtmlEncode(embedLink) + "</embed-info>" + dbgCrLf;
				
				xml += "</share-data>" + dbgCrLf;
				
				xml += "</confirm>" + dbgCrLf;
			}
			catch( Exception e )
			{
				xml = makeErrorXml( eOMMError.ASSET_NOT_FOUND, null ); 
				m_logger.Error( "Exception in requeset.getShareData", e );
			}
			
			addXml( xml, assetId );
			
			closeXmlResponse();
			
		}

		/// <summary>
		/// get metadata for a single asset
		/// </summary>
		/// <param name="userId"></param>
		/// <param name="mdType"></param>
		/// <param name="assetId"></param>
		private void getMetaData( int userId, Int32 mdType, Int32 assetId )
		{
			string xml = "";
			string userThumb = "";

			prepareXmlResponse( mdType, userId, assetId, mdType == Constants.GET_FULL_METADATA ? (CONFIG_XML | SINGLE_WIDGET_XML) : NO_XML );

			xml = "<confirm>" + dbgCrLf;

			try
			{
				string columns = ""; // what columns do we want
				string extraTable = "";
				string extraWhere = "";
				
				if ( mdType == Constants.GET_FULL_METADATA || 
					mdType == Constants.GET_STATIC_METADATA )
				{
					extraTable = " INNER JOIN communities_personal com ON a.owner_id = com.creator_id ";
					columns = " a.mature, a.name, a.asset_type_id, a.asset_sub_type_id, a.owner_username, a.file_size, a.run_time_seconds, a.teaser, a.thumbnail_small_path, a.media_path, " +
						" com.thumbnail_small_path as owner_thumbnail_small_path ";
					extraWhere = " " +
					StoreUtility.SQLCommon_GetPublishedAssetsSQL ();
				}
				
				if ( mdType == Constants.GET_FULL_METADATA )
				{
					columns += ", ";
				}
				
				if ( mdType == Constants.GET_FULL_METADATA || 
					mdType == Constants.GET_DYNAMIC_METADATA )
				{
					// TODO: no shares column
					columns += " a.keywords, a.amount, " +
						" ass.number_of_downloads, ass.number_of_shares, ass.number_of_comments, ass.number_of_diggs, ass.number_of_channels ";
				}

				string sql = "SELECT " + columns + 
					" FROM `assets` a INNER JOIN assets_stats ass ON a.asset_id = ass.asset_id " + extraTable + 
					" WHERE a.asset_id = " + assetId.ToString() + extraWhere;
				
				DataRow dr = KanevaGlobals.GetDatabaseUtility().GetDataRow( sql, false );
				if ( dr == null )
				{
					xml = "";
				}
				else
				{

					if ( !m_isEmbedded && !KanevaWebGlobals.CurrentUser.HasAccessPass && 
						StoreUtility.IsMatureRating ( dr["mature"].ToString ()) )
					{
						xml = makeErrorXml( eOMMError.ASSET_RESTRICTED, null ); 
					}
					else
					{

						if ( mdType == Constants.GET_FULL_METADATA || 
							mdType == Constants.GET_STATIC_METADATA )
						{

							userThumb = "";
							if (dr.Table.Columns.Contains("owner_thumbnail_small_path")) 
							{
								userThumb = dr ["owner_thumbnail_small_path"].ToString ();
							}

							xml += "<media ";
							if ( !encodeStaticMediaAttributes( userId, ref xml, assetId, mdType, 
								Convert.ToInt32(dr["asset_type_id"]),
								Convert.ToInt32(dr["asset_sub_type_id"]),
								dr["name"].ToString (), 
								dr["owner_username"].ToString () , 
								Convert.ToInt64( dr["file_size"]), 
								Convert.ToInt32( dr["run_time_seconds"] ),
								dr["teaser"].ToString (),
								"Not Used", 
								StoreUtility.IsMatureRating (dr ["mature"].ToString ()), 
								false,
								dr ["thumbnail_small_path"].ToString (), 
								userThumb,
                                dr["media_path"].ToString ()))
							{
								xml = "";
							}
						}
					
						if ( mdType == Constants.GET_FULL_METADATA || 
							mdType == Constants.GET_DYNAMIC_METADATA )
						{
							if ( mdType == Constants.GET_DYNAMIC_METADATA )
								xml += "<media id=\"" + assetId + "\" >" + dbgCrLf;

							xml += encodeStats( assetId, ref dr );

						}
					}
				}
				
				if ( xml.Length ==  0 )
				{
					xml = makeErrorXml( eOMMError.ASSET_NOT_FOUND, null ); 
				}
				else
				{
					xml += "</media>" + dbgCrLf;
					xml += "</confirm>" + dbgCrLf;
				}
			}			
			catch( Exception e )
			{
				xml = makeErrorXml( eOMMError.ASSET_NOT_FOUND, null ); 
				m_logger.Error( "Exception in requeset.getMetaData", e );
			}
			
			addXml( xml, assetId );
			
			closeXmlResponse();
		}

		/// <summary>
		/// helper to encode the asset's stats into the XML
		/// </summary>
		/// <param name="assetId"></param>
		/// <param name="dr"></param>
		/// <returns></returns>
		private string encodeStats( int assetId, ref DataRow dr )
		{			
			return "<stats cost=\"" + Convert.ToInt32(dr["amount"]) + "\" " +
				"shares=\"" + Convert.ToInt32(dr["number_of_shares"]) + "\" " +
				"channelCount=\"" + Convert.ToInt32 (dr ["number_of_channels"]) + "\" " +
				"thumbsup=\"" + Convert.ToInt32(dr["number_of_diggs"]) + "\" " +
				"views=\"" + Convert.ToInt32(dr["number_of_downloads"]) + "\" />" + dbgCrLf + 
				"<comments total=\"" + Convert.ToInt32(dr["number_of_comments"]) + "\" />" + dbgCrLf + 
				"<tags>" + Server.HtmlEncode(dr["keywords"].ToString()) + "</tags>" + dbgCrLf;
		}
		
		/// <summary>
		/// format an error element
		/// </summary>
		/// <param name="errorNo"></param>
		/// <param name="msg"></param>
		private string makeErrorXml( eOMMError errorNo, string addlMsg )
		{
			string msg;

			if ( (int)errorNo >= 0 && (int)errorNo < ERR_MSGS.Length )
					msg = ERR_MSGS[(int)errorNo];
			else 
				msg = ERR_MSGS[0] + errorNo;
			
			string xml = "<error/>" + dbgCrLf; 
			if ( addlMsg != null )
				msg +=  " " + addlMsg;

			if ( errorNo == eOMMError.INVALID_AUTH )
				xml += makeTickerMessageXml(  new TickerInfo[2] { new TickerInfo( msg, ERROR_MESSAGE_DISPLAY_TIME, 0, null ), 
																  new TickerInfo( MSG_LOGIN, ERROR_MESSAGE_DISPLAY_TIME, 0,
																	   Request.Url.GetLeftPart( System.UriPartial.Authority ) + GetLoginURL() ) } );
			else
				xml += makeTickerMessageXml(  new TickerInfo[1] { new TickerInfo( msg, ERROR_MESSAGE_DISPLAY_TIME, 0, null ) } );

			return xml;
		}

		/// <summary>
		/// get the list of playlists for this user
		/// </summary>
		/// <param name="userId"></param>
		private void getPlayListList( int userId )
		{
			string xml = "";

			prepareXmlResponse( Constants.GET_PLAYLIST_LIST, userId, 0, NO_XML );

			try
			{
				// add the playlist element
				xml += "<playlists>" + dbgCrLf;;
			

				string sql;
				sql = "SELECT ag.channel_id, ag.asset_group_id, ag.name, c.creator_id " +
					"  FROM asset_groups ag, communities_personal c " + 
					" WHERE c.creator_id = " + userId.ToString() +
					"   AND c.community_id  = ag.channel_id";

				DataTable dt = KanevaGlobals.GetDatabaseUtility().GetDataTable( sql );
				if ( dt != null  )
				{
					int rowCount = dt.Rows.Count;

					// get each item in the playlist and add xml for it
					for ( int i = 0; i < rowCount; i++)
					{
						DataRow row = dt.Rows[i];

						// create a media node
						xml += "<playlistname id=\"" + Convert.ToInt32(row["asset_group_id"]) + "\" " +
							"name=\"" + Server.HtmlEncode( row["name"].ToString() ) + "\" />" + dbgCrLf;;
					}
				}

				xml += "</playlists>" + dbgCrLf;;
			
			}			
			catch( Exception e )
			{
				xml = makeErrorXml( eOMMError.ASSET_NOT_FOUND, null ); 
				m_logger.Error( "Exception in requeset.getShareData", e );
			}
			
			addXml( xml, 0 );	
			
			closeXmlResponse();
		}

		/// <summary>
		/// encode the common stuff for static media element
		/// </summary>
		private bool encodeStaticMediaAttributes( int userId, ref string xml, int assetId, int mdType,
			int assetTypeId,
			int assetSubTypeId,
			string name,
			string ownerName,
			Int64 fileSize,
			Int32 runTime,
			string shortDesc,
			string ratingName,
			bool isMature, 
			bool fromGame,
			string mediaThumb,
			string userThumb,
            string mediaPath)
		{
			bool ret = true;

			Hashtable attrs = getAssetsAttributes( assetId );

			// create a media node
			string lengthStr = String.Format( "{0:D}:{1:D2}:{2:D2}", runTime / 3600, runTime / 60 % 60, runTime % 60 );
			
			string released="Unknown";
			if ( attrs.Contains(RELEASE_DATE_A) )
				released = (string)attrs[RELEASE_DATE_A];

			// *******************************************************
			// Tell them after the fact for performance purposes
			// *******************************************************
			// Make sure they have not already digged
			//DataRow drDigg = StoreUtility.GetDigg( userId, assetId );
			string alreadyDugg = FALSE_VALUE; //(drDigg != null) ? TRUE_VALUE : FALSE_VALUE;
			string inLib = FALSE_VALUE; // StoreUtility.IsItemInConnectedMedia ( userId, assetId) ? TRUE_VALUE : FALSE_VALUE;

            string url = Server.HtmlEncode(Configuration.StreamingServer + "/" + Configuration.StreamingFile + "?tId=" + assetId);
            if (Configuration.StreamIIS)
            {
                url = Server.HtmlEncode(Configuration.StreamingServer + "/" + mediaPath);
            }

			// add the attributes
			if ( fromGame )
			{
				string subType = "";

				xml += "id=\"" + assetId + "\" " + 
					"secs=\"" + runTime.ToString() + "\" " +
					"size=\"" + fileSize + "\" " + 
					"thumb=\"" + Server.HtmlEncode (GetVideoImageURL (mediaThumb, "sm")) + "\" " + 
					"type=\"" + assetTypeId + "\" " +
					subType +
					"raved='" + alreadyDugg + "' " + 
					"url=\"" + url + "\" " + 
					">" + dbgCrLf;
			}
			else
			{
				xml += "id=\"" + assetId + "\" " + 
					"length=\"" + lengthStr + "\" " +
					"released=\"" + released + "\" " + 
					"size=\"" + fileSize + "\" " + 
					"thumb=\"" + Server.HtmlEncode (GetVideoImageURL (mediaThumb, "sm")) + "\" " + 
					"userThumb=\"" + Server.HtmlEncode (GetProfileImageURL (userThumb, "sm", "M")) + "\" " + 
					"type=\"" + assetTypeId+ "\" " +
					"user=\"" + Server.HtmlEncode(ownerName) + "\" " + 
					"userUrl=\"" + Server.HtmlEncode(GetPersonalChannelUrl(ownerName.Replace(" ","") )) + "\" " +
					"url=\"" + url + "\" " + 
					"mature=\"" + (string)(isMature ? TRUE_VALUE : FALSE_VALUE) + "\" " + 
					"rating=\"" + Server.HtmlEncode(ratingName) + "\" " +
					"raved='" + alreadyDugg + "' " + 
					"inlib='" + inLib + "' " + 
					">" + dbgCrLf;

				// put in a desc node, album and author are optional
				xml += dbgIndent + "<desc ";
				if ( attrs.Contains(ALBUM) )
					xml += "album=\"" + Server.HtmlEncode((string)attrs[ALBUM]) + "\" ";
				if ( attrs.Contains(AUTHORS) )
					xml += "author=\"" + Server.HtmlEncode((string)attrs[AUTHORS]) + "\" ";
				xml += "title=\"" + Server.HtmlEncode(name);
				if ( shortDesc == null || shortDesc.Length == 0 )
				{
					xml += "\"/>" + dbgCrLf;
				}
				else
				{
					xml += "\">" + Server.HtmlEncode(shortDesc) + "</desc>" + dbgCrLf;
				}
			}

			return ret;
							
		}

		/// <summary>
		///  for the game, figure out when playlist started playing so the client
		///  can figure out when to play what video
		/// </summary>
		/// <param name="userId"></param>
		/// <param name="playListId"></param>
		/// <param name="totalSeconds"></param>
		/// <returns></returns>
		private Int32 calcStartTime( int userId, Int32 playListId, Int32 totalSeconds, DateTime lastUpdate )
		{
			TimeSpan diff = DateTime.Now - lastUpdate;
			return (Int32)(diff.TotalSeconds % totalSeconds);
		}

		/// <summary>
		/// get the media in the play list
		/// </summary>
		/// <param name="userId"></param>
		/// <param name="playListId"></param>
		private void getPlayList( int userId, Int32 playListId, bool fromGame )
		{
			string xml;
			string userThumb = "";

			prepareXmlResponse( Constants.GET_PLAYLIST, userId, playListId, fromGame ? CONFIG_XML : PLAYLIST_WIDGET_XML | CONFIG_XML );

			// add the playlist element
			m_XmlResponse += "<playlist>";
			
			// Use Dataview for sorting purposes
			DataTable dtAssets = null;
			DateTime lastUpdate = new DateTime( 2006, 1, 1 ); // arbtrary start state for all but user playlist

			if (playListId == Constants.USER_ALL_VIDEO_PLAYLIST_ID)
			{
				// No list, get all
                dtAssets = StoreUtility.GetAccessibleOMMAssetsInChannel(UsersUtility.GetCommunityIdFromUserId(userId), 0, KanevaWebGlobals.CurrentUser.HasAccessPass, (int)Constants.eASSET_TYPE.VIDEO, GetUserId(), "", "", 1, MAX_PLAYLIST_SIZE);
			}
			else if (playListId == Constants.USER_ALL_MUSIC_PLAYLIST_ID)
			{
				// No list, get all
                dtAssets = StoreUtility.GetAccessibleOMMAssetsInChannel(UsersUtility.GetCommunityIdFromUserId(userId), 0, KanevaWebGlobals.CurrentUser.HasAccessPass, (int)Constants.eASSET_TYPE.MUSIC, GetUserId(), "", "", 1, MAX_PLAYLIST_SIZE);
			}
			else if ( playListId == Constants.COOL_ITEM_PLAYLIST_ID )
			{
				dtAssets = StoreUtility.GetCoolAssets( (int)Constants.eASSET_TYPE.ALL, // assetType
					" a.asset_type_id in (" + ((int)Constants.eASSET_TYPE.VIDEO).ToString() + "," + ((int)Constants.eASSET_TYPE.MUSIC).ToString() + " ) ", // filter,
					1, 8 );
			}
			else if ( playListId == Constants.COOL_ITEM_PLAYLIST_ID_HOME )
			{
				dtAssets = StoreUtility.GetCoolAssets( (int)Constants.eASSET_TYPE.ALL, // assetType
					" fa.playlist_type = " + (int) Constants.COOL_ITEM_PLAYLIST_ID_HOME + " AND a.asset_type_id in (" + ((int)Constants.eASSET_TYPE.VIDEO).ToString() + "," + ((int)Constants.eASSET_TYPE.MUSIC).ToString() + " ) ", // filter,
					1, 8);
			}
			else if ( playListId == Constants.COOL_ITEM_PLAYLIST_ID_MEMBER_CHANNEL )
			{
				dtAssets = StoreUtility.GetCoolAssets( (int)Constants.eASSET_TYPE.ALL, // assetType
					" fa.playlist_type = " + (int) Constants.COOL_ITEM_PLAYLIST_ID_MEMBER_CHANNEL + " AND a.asset_type_id in (" + ((int)Constants.eASSET_TYPE.VIDEO).ToString() + "," + ((int)Constants.eASSET_TYPE.MUSIC).ToString() + " ) ", // filter,
					1, 8 );
			}
			else if ( playListId == Constants.COOL_ITEM_PLAYLIST_ID_BROADBAND_CHANNEL )
			{
				dtAssets = StoreUtility.GetCoolAssets( (int)Constants.eASSET_TYPE.ALL, // assetType
					" fa.playlist_type = " + (int) Constants.COOL_ITEM_PLAYLIST_ID_BROADBAND_CHANNEL + " AND a.asset_type_id in (" + ((int)Constants.eASSET_TYPE.VIDEO).ToString() + "," + ((int)Constants.eASSET_TYPE.MUSIC).ToString() + " ) ", // filter,
					1, 8 );
			}
			else if ( playListId == Constants.COOL_ITEM_PLAYLIST_ID_MEDIA )
			{
//				dt = StoreUtility.GetCoolAssets( (int)Constants.eASSET_TYPE.ALL, // assetType
//					" fa.playlist_type = " + (int) Constants.COOL_ITEM_PLAYLIST_ID_MEDIA + " AND a.asset_type_id in (" + ((int)Constants.eASSET_TYPE.VIDEO).ToString() + "," + ((int)Constants.eASSET_TYPE.MUSIC).ToString() + " ) ", // filter,
//					1, 8 );

				// This is the last one in use, go to cache
				dtAssets = WebCache.GetMediaPageOMMList (8, GetRandomCacheKey ());
			}
			else // a specific playlist
			{
				string sqlSelect = "SELECT channel_id, last_updated " +
					" FROM asset_groups " +
					" WHERE asset_group_id = @assetGroupId";

				Hashtable parameters = new Hashtable ();
				parameters.Add ("@assetGroupId", playListId);
				DataRow row = KanevaGlobals.GetDatabaseUtility().GetDataRow(sqlSelect, parameters, false );
				if ( row != null )
				{
					int channelId = Convert.ToInt32(row["channel_id"]);

                    dtAssets = StoreUtility.GetAccessibleOMMAssetsInChannel(channelId, playListId, KanevaWebGlobals.CurrentUser.HasAccessPass, 0, GetUserId(), "", "", 1, MAX_PLAYLIST_SIZE);
					lastUpdate = Convert.ToDateTime( row["last_updated"] );
				}
			}


			if ( dtAssets != null  )
			{
				int rowCount = dtAssets.Rows.Count;
				int totalSeconds = 0;

				// get each item in the playlist and add xml for it
				for ( int i = 0; i < rowCount; i++)
				{
					DataRow row = dtAssets.Rows [i];
					int assetId = Convert.ToInt32 (row ["asset_id"]);

					xml = "<media ";

					string ratingName = "General";
					bool isMature = false;
					Int32 seconds = Convert.ToInt32( row ["run_time_seconds"] );
					
					if (StoreUtility.IsMatureRating (row ["mature"].ToString ()))
					{
						ratingName = "Restricted";
						isMature = true;
					}

					// Per Jim Email, never send YouTube Videos
					if (Convert.ToInt32 (row ["asset_sub_type_id"]).Equals ((int) Constants.eASSET_SUBTYPE.YOUTUBE))
					{
						continue; // skip it
					}

                    if (!m_isEmbedded && !KanevaWebGlobals.CurrentUser.HasAccessPass && isMature)
						continue; // skip it

					// 11/06 now allow zero-length videos
					if ( fromGame && (Convert.ToInt32 (row ["asset_type_id"]) != VIDEO_TYPE && 
						              Convert.ToInt32 (row ["asset_type_id"]) != MUSIC_TYPE) ) // if from game no zero-lens, and only videoes
						continue;

					// Set the user thumb
					userThumb = "";
					if (dtAssets.Columns.Contains("owner_thumbnail_small_path")) 
					{
						userThumb = row ["owner_thumbnail_small_path"].ToString ();
					}

					if ( !encodeStaticMediaAttributes( userId, ref xml, assetId, Constants.GET_PLAYLIST, 
						Convert.ToInt32 (row ["asset_type_id"]), 
						Convert.ToInt32 (row ["asset_sub_type_id"]), 
						(string)row ["name"], 
						(string)row ["owner_username"], 
						Convert.ToInt64( row ["file_size"] ), 
						seconds,
						row ["teaser"].ToString(),
						ratingName, 
						isMature,
						fromGame,
						row ["thumbnail_small_path"].ToString (),
						userThumb,
                        row["media_path"].ToString()
						)) 
					{
						continue; // TODO: what to do if one isn't there
					}
					totalSeconds += seconds;
					if ( !fromGame )
					{
						xml += encodeStats( assetId, ref row);
					}
					
					xml += "</media>" + dbgCrLf;

					addXml( xml, assetId );
				}

				if ( fromGame )
				{
					addXml( "<startOffset>" + calcStartTime( userId, playListId, totalSeconds, lastUpdate ) + "</startOffset>" + dbgCrLf, 0 );
				}
			}

			m_XmlResponse += "</playlist>";
			closeXmlResponse();
		}

		/// <summary>
		/// GetRandomCacheKey
		/// </summary>
		/// <returns></returns>
		private string GetRandomCacheKey ()
		{
			Random RandomClass = new Random ();
			return RandomClass.Next (1, 4).ToString ();
		}

		/// <summary>
		/// get the list of attributes for a given assset into a hash table
		/// </summary>
		/// <param name="assetId"></param>
		/// <returns></returns>
		private Hashtable getAssetsAttributes( int assetId )
		{
			// These are not even used anymore?
			return new Hashtable ();
//			//  authors (43), length (runtime) (27), release date (32 and 34), album (35)
//			string sql = "SELECT attribute_id, attribute_value " +
//				"  FROM asset_attribute_values " +
//				" WHERE attribute_id in ( " + ATTR_LIST + " ) " +
//				"   AND length(attribute_value) > 0 " +
//				"   AND asset_id = " + assetId.ToString();
//
//			Hashtable attrs = new Hashtable();
//			DataTable dt = Global.GetDatabaseUtility().GetDataTable( sql );
//			if ( dt != null  )
//			{
//				int rowCount = dt.Rows.Count;
//
//				for ( int i = 0; i < rowCount; i++)
//				{
//					DataRow row = dt.Rows[i];
//					int attrType = (int)row["attribute_id"];
//					if ( attrType == RELEASE_DATE_B )
//						attrType = RELEASE_DATE_A;
//
//					attrs.Add( attrType, (string)row["attribute_value"] );
//				}
//			}
//			return attrs;
		}
		
		/// <summary>
		/// append the XML to the response XML.  This gives you a chance to 
		/// add pre/post ads
		/// </summary>
		/// <param name="xml"></param>
		/// <param name="assetId"></param>
		private void addXml( string xml, Int32 assetId )
		{
			// TODO: call to advertising object to add pre-show see comment at top
			// for this item

			m_XmlResponse += xml;

			// TODO: call to advertising object to add post-show see comment at top
			// for this item
		}

		/// <summary>
		/// set the context on the ad engine, called when preparing the XML response
		/// so the ad engine has context
		/// </summary>
		/// <param name="type"></param>
		/// <param name="userId"></param>
		/// <param name="id"></param>
		private void setAdvertisingContext( Int32 type, int userId, Int32 id )
		{
			// TODO: call the advertising object to have it prepare  see comment at top
			// objects to prepend
		}

		/// <summary>
		/// initialize the XML response to the caller (OMM)
		/// </summary>
		/// <param name="type"></param>
		/// <param name="userId"></param>
		/// <param name="id"></param>
		/// <param name="extraXml"></param>
		private void prepareXmlResponse( Int32 type, int userId, Int32 id, Int32 extraXml )
		{
			setAdvertisingContext( type, userId, id );
				
			// TODO: call to advertising object to add pre-item-show see comment at top
			
			m_XmlResponse = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + dbgCrLf + dtd + "<omm-resp id='0' >" + dbgCrLf;
			
			// TODO: call to advertising object to add post-item-show see comment at top

			if ( (extraXml & CONFIG_XML) != 0 )
				addXml( "config.xml" );

			if ( (extraXml & PLAYLIST_WIDGET_XML) != 0 )
				addXml( "playlistWidgets.xml" );

			if ( (extraXml & SINGLE_WIDGET_XML) != 0 )
				addXml( "singleWidgets.xml" );
		}

		/// <summary>
		/// add XML from a file, updating any URLs on the way
		/// </summary>
		/// <param name="fname"></param>
		void addXml ( string fname )
		{
			string extra = (string) Global.Cache () [fname];

			if (extra == null || extra.Length.Equals (0))
			{
				string basePath = Request.PhysicalApplicationPath;
				System.IO.StreamReader sr = null;

				try
				{
					sr = new System.IO.StreamReader( basePath + @"flash\omm\" + fname);
					extra = sr.ReadToEnd();
					extra = extra.Replace( "~", "http://" + KanevaGlobals.SiteName );
				}
				catch (Exception exc)
				{
					m_logger.Error ("Error in OMM code, fname =" + fname, exc);
					extra = "";
				}
				finally 
				{
					if (sr != null)
					{
						sr.Close ();
					}
				}

				// Add to the cache
				Global.Cache ().Insert (fname, extra, null, DateTime.Now.AddMinutes (30), System.Web.Caching.Cache.NoSlidingExpiration);
			}
		
			m_XmlResponse += extra;
		}

		/// <summary>
		/// finish off the XML response, giving the ad engine a chance to put more in
		/// </summary>
		private void closeXmlResponse()
		{
			// TODO: call to advertising object to add post-show see comment at top
			// objects to append
			m_XmlResponse += "</omm-resp>" + dbgCrLf;
		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
