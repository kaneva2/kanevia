///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using CloudSponge;
using Newtonsoft.Json;
using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;
using log4net;

namespace KlausEnt.KEP.Kaneva.services
{
    /// <summary>
    /// Summary description for csimporter
    /// </summary>
    [WebService (Namespace = "http://www.kaneva.com/")]
    [WebServiceBinding (ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem (false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class csimporter : System.Web.Services.WebService
    {
        [WebMethod]
        public string Consent (string service)
        {
            try
            {
                if (KanevaWebGlobals.CurrentUser.UserId > 0)
                {
                    if (service.Trim () != "")
                    {
                        var api = new Api (KanevaGlobals.CloudSpongeDomainKey, KanevaGlobals.CloudSpongePassword);
                        var consent = api.Consent (SetServiceType (service));
                        return JsonConvert.SerializeObject (consent);
                    }
                }
            }
            catch { }
            return "";
        }

        [WebMethod]
        public string ImportStatus (int importId)
        {
            try
            {
                if (KanevaWebGlobals.CurrentUser.UserId > 0)
                {
                    if (importId > 0)
                    {
                        var api = new Api (KanevaGlobals.CloudSpongeDomainKey, KanevaGlobals.CloudSpongePassword);
                        var events = api.Events (importId);
                        return JsonConvert.SerializeObject (events);
                    }
                }
            }
            catch { }
            return "";
        }

        [WebMethod]
        public string Contacts (int importId)
        {
            try
            {
                if (KanevaWebGlobals.CurrentUser.UserId > 0)
                {
                    if (importId > 0)
                    {
                        var api = new Api (KanevaGlobals.CloudSpongeDomainKey, KanevaGlobals.CloudSpongePassword);
                        var contacts = api.Contacts (importId);

                        // Pull out all the email address properties
                        var emails = from items in contacts.Contacts select items.EmailAddresses;

                        // Get all emails and put into a string list
                        List<string> listEmails = new List<string> ();
                        foreach (var email in emails)
                        {
                            foreach (var e in email)
                            {
                                // Some contacts may not have email addresses. Do not include those.
                                if (email.FirstOrDefault () != "")
                                {
                                    listEmails.Add (email.FirstOrDefault ());
                                }
                            }
                        }

                        // Get any users that are already members
                        PagedList<User> users = new UserFacade ().GetUsersByEmail (listEmails);

                        var emailContacts = contacts.Contacts.ToList ();

                        // For each member, set the userid value and put them at top of list
                        foreach (User u in users)
                        {
                            // Find the user in contact list
                            var c = emailContacts.First (a => a.EmailAddresses.FirstOrDefault () == u.Email);
                            c.UserId = u.UserId;
                            c.ThumbnailPath = u.ThumbnailSquarePath;
                            c.UseFacebookProfilePicture = u.FacebookSettings.UseFacebookProfilePicture;
                            c.FacebookUserId = u.FacebookSettings.FacebookUserId;
                            emailContacts.Remove (c);
                            emailContacts.Insert (0, c);
                        }

                        return JsonConvert.SerializeObject (emailContacts);
                    }
                }
            }
            catch { }
            return "";
        }

        private ContactSource SetServiceType (string service)
        {
            switch (service.ToLower())
            {
                case "yahoo":
                    return ContactSource.Yahoo;
                case "gmail":
                    return ContactSource.Gmail;
                case "windowslive":
                    return ContactSource.WindowsLive;
                case "aol":
                    return ContactSource.AOL;
                case "plaxo":
                    return ContactSource.Plaxo;
                case "outlook":
                    return ContactSource.Outlook;
//                case "mail.ru":
//                    return ContactSource.
                case "addressbook":
                    return ContactSource.AddressBook;
                default:
                    return ContactSource.Uknown;
            }
        }

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

    }
}
