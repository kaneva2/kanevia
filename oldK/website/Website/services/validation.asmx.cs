///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;
using log4net;
using Newtonsoft.Json;

namespace KlausEnt.KEP.Kaneva.services
{
    /// <summary>
    /// Summary description for validation
    /// </summary>
    [WebService (Namespace = "http://www.kaneva.com/")]
    [WebServiceBinding (ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem (false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class validation : System.Web.Services.WebService
    {
        [WebMethod]
        public string Username (string username)
        {
            UserFacade userFacade = new UserFacade ();
            ValidUsername vu = new ValidUsername ();
            vu.Suggestions = new List<string> ();
            vu.Restricted = false;

            // Check to see if name contains restricted words
            if (this.IsPottyMouth(username))
            {
                vu.Restricted = true;
            }
            else
            {
                // Check to see if name is taken
                if (userFacade.UsernameExists(username))
                {
                    vu.Available = false;

                    // Username is unavailable, create suggestions and verify they are available
                    if (username.Length > 17)
                    {
                        username = username.Remove(17);
                    }

                    string num = string.Empty;
                    int numSuggestions = 0;
                    for (int i = 1; i <= 20; i++)
                    {
                        num = i < 10 ? "00" + i : "0" + i;

                        if (!userFacade.UsernameExists(username + num))
                        {
                            // Add username to suggestion list
                            vu.Suggestions.Add(username + num);

                            // Increment number of recommendations
                            numSuggestions++;
                            if (numSuggestions > 4)
                            {
                                break;
                            }
                        }
                    }
                }
                else
                {
                    vu.Available = true;
                    vu.Suggestions.Add(username);
                }
            }

            return JsonConvert.SerializeObject (vu);
        }

        [WebMethod]
        public string EmailAvailable (string email)
        {
            return JsonConvert.ToString((!new UserFacade().EmailExists(email)));
        }

        [WebMethod]
        public string IsRestricted(string text)
        {
            // Check to see if name contains restricted words
            return JsonConvert.ToString(IsPottyMouth(text));
        }

        [Serializable]
        public class ValidUsername
        {
            public ValidUsername (){}

            public bool Available { get; set; }
            public bool Restricted { get; set; } 
            public List<string> Suggestions { get; set; }
        }

        private bool IsPottyMouth(string txt)
        {
            if (KanevaWebGlobals.isTextRestricted(txt, Constants.eRESTRICTION_TYPE.POTTY_MOUTH, false))
            {
                return true;
            }
            return false;
        }
    }
}
