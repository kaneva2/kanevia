///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
    public partial class requestTracking : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //clears local cache so data is always new
                Response.ContentType = "text/plain";
                Response.CacheControl = "no-cache";
                Response.AddHeader("Pragma", "no-cache");
                Response.Expires = -1;

                string userAgent = "";
                string isIE = "";
                char ieBrowser = 'N'; 

                if (Request.Params[Constants.cREQUEST_TRACKING_URL_PARAM] != null)
                {
                    userAgent = Request.Params["UserAgent"];
                }

                if (Request.Params["IsIE"] != null)
                {
                    isIE = Request.Params["IsIE"];
                }

                if (isIE == "true")
                {
                   ieBrowser = 'Y';
                }

                // Request tracking
                if (Request.Params[Constants.cREQUEST_TRACKING_URL_PARAM] != null)
                {
                    UserFacade userFacade = new UserFacade();
                    int iResult = userFacade.AcceptTrackingRequest(Request.Params[Constants.cREQUEST_TRACKING_URL_PARAM], KanevaWebGlobals.CurrentUser.UserId, 0, ieBrowser, userAgent);
                }
            }
        }
    }
}