///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Kaneva.BusinessLayer.BusinessObjects;
using System.Collections.Generic;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for memberhome.
	/// </summary>
	public class defaultHomeB : BasePage
    {
        protected defaultHomeB() 
		{
			Title = "Home";
		}

        private void Page_Load (object sender, System.EventArgs e)
        {
            System.Web.HttpCookie aCookie;

            // Is user logged in?
            if (!Request.IsAuthenticated)
            {
				string redirectUrl = "~/free-virtual-world.kaneva";
				if (!String.IsNullOrEmpty(Request.ServerVariables["QUERY_STRING"].ToString()))
				{
					redirectUrl += "?" + Request.ServerVariables["QUERY_STRING"].ToString();
				}
				
                Response.Redirect (redirectUrl);
            }

            // Add prototype
            RegisterJavaScript("jscript/prototype-1.6.1-min.js");

            // If they have been banned, log them out!
            if (Current_User.StatusId.Equals ((int) Constants.eUSER_STATUS.LOCKED) || Current_User.StatusId.Equals ((int) Constants.eUSER_STATUS.LOCKEDVALIDATED))
            {
                for (int i = Request.Cookies.Count - 1; i != 0; i--)
                {
                    aCookie = Request.Cookies[i];
                    aCookie.Expires = DateTime.Now.AddDays (-1);
                    Response.Cookies.Add (aCookie);
                }

                FormsAuthentication.SignOut ();
                Session.Abandon ();
                Response.Redirect ("~/free-virtual-world.kaneva");
            }

            if (!IsPostBack)
            {
                // Has the user been in world?
                if (Current_User.WokPlayerId == 0)
                {
                    // show the reminder!
                    dvDownloadClientCon.Visible = true;
                    dvDownloadClient.Style.Add ("background-image", "images/mykaneva/announcements/my_kaneva_install_bug_520x200.jpg");
                    btnDownloadClose.CommandName = "client";

                    // check for the cookie showing they've already closed the window.			
                    if (Request.Cookies["downloadReminder"] != null)
                    {
                        if (Request.Cookies["downloadReminder"].Value == "false")
                        {
                            // don't show the reminder!
                            dvDownloadClientCon.Visible = false;
                        }
                    }
                }
                else  // Display the current communication banner
                {
                    dvDownloadClientCon.Visible = true;
                    aLearnMore.Visible = false;

                    // check for the cookie showing they've already closed the window.			
                    if (Request.Cookies[Banner.CookieName] != null)
                    {
                        if (Request.Cookies[Banner.CookieName].Value == "false")
                        {
                            // don't show the reminder!
                            dvDownloadClientCon.Visible = false;
                            aLearnMore.Visible = false;
                        }
                    }
                    else
                    {
                        if (Banner.LearnMoreLink != "")
                        {
                            aLearnMore.Visible = true;
                            aLearnMore.HRef = Banner.LearnMoreLink;
                        }

                        if (Banner.Image.Length > 0)
                        {
                            dvDownloadClient.Style.Add ("background-image", ResolveUrl ("~" + Banner.Image));
                        }
                        else
                        {
                            dvDownloadClientCon.Visible = false;
                        }
                    }

                    btnDownloadClose.CommandName = "mkbanner";
                    aDownload.Visible = false;
                }
            }

            // Set Nav
            ((GenericPageTemplate) Master).HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
            ((GenericPageTemplate) Master).HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.NONE;
            ((GenericPageTemplate) Master).HeaderNav.SetNavVisible (((GenericPageTemplate) Master).HeaderNav.MyKanevaNav);
        }

        public void btnDownloadClientClose_Click(Object sender, CommandEventArgs e) 
		{
            if (e.CommandName == "client")
            {
                Response.Cookies["downloadReminder"].Value = "false";
                Response.Cookies["downloadReminder"].Expires = DateTime.Now.AddDays (1);
                dvDownloadClientCon.Visible = false;
                Page.ClientScript.RegisterClientScriptBlock (GetType (), "gaDownloadEvent", "callGAEvent('MyKaneva', 'DownloadAd', 'Close');", true);
            }
            else // e.CommandName == "fame"
            {
                int x = Request.Cookies.Count;
                Response.Cookies[Banner.CookieName].Value = "false";
                Response.Cookies[Banner.CookieName].Expires = DateTime.Now.AddDays (Convert.ToInt32(Banner.DaysUntilExpire));
                dvDownloadClientCon.Visible = false;
                x = Request.Cookies.Count;
            }
		}

        #region Declarations

        // private 
        private User Current_User = KanevaWebGlobals.CurrentUser;
		public HtmlContainerControl dvDownloadClientCon, dvDownloadClient;
        public LinkButton btnDownloadClose;
        public HtmlAnchor aDownload, aLearnMore;
        protected usercontrols.ContainerMyKaneva MKcontainerNotifications, MKcontainerPasses;

        #endregion Declerations

        public  static class Banner
        {
            public static string CookieName
            {
                get
                {
                    try
                    {
                        if (System.Configuration.ConfigurationManager.AppSettings["Banner_CookieName"] != null)
                        {
                            return System.Configuration.ConfigurationManager.AppSettings["Banner_CookieName"].ToString ();
                        }
                        else
                        {
                            return "";
                        }
                    }
                    catch { return ""; }
                }
            }
            public static string LearnMoreLink
            {
                get
                {
                    try
                    {
                        if (System.Configuration.ConfigurationManager.AppSettings["Banner_LearnMoreLink"] != null)
                        {
                            return System.Configuration.ConfigurationManager.AppSettings["Banner_LearnMoreLink"].ToString ();
                        }
                        else
                        {
                            return "";
                        }
                    }
                    catch { return ""; }
                }
            }
            public static string Image
            {
                get
                {
                    try
                    {
                        if (System.Configuration.ConfigurationManager.AppSettings["Banner_Image"] != null)
                        {
                            return System.Configuration.ConfigurationManager.AppSettings["Banner_Image"].ToString ();
                        }
                        else
                        {
                            return "";
                        }
                    }
                    catch { return ""; }
                }
            }
            public static int DaysUntilExpire
            {
                get
                {
                    try
                    {
                        if (System.Configuration.ConfigurationManager.AppSettings["Banner_DaysUntilExpire"] != null &&
                            System.Configuration.ConfigurationManager.AppSettings["Banner_DaysUntilExpire"].ToString ().Trim ().Length > 0)
                        {
                            return Convert.ToInt32 (System.Configuration.ConfigurationManager.AppSettings["Banner_DaysUntilExpire"]);
                        }
                        else
                        {
                            return 0;
                        }
                    }
                    catch { return 0; }
                }
            }
        }


        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
    }
}
