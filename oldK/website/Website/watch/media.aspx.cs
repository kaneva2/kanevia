///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using log4net;
using MagicAjax;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for media.
	/// </summary>
	public class media : StoreBasePage
	{
        #region Declarations

        protected Kaneva.Pager pgTop, pgBottom;
        protected Kaneva.usercontrols.TagCloud ucTagCloud;

        protected HyperLink hlChangeProfile;
        protected Button btnRestrictAsset;
        protected TextBox txtKeywords;
        protected LinkButton lbThumbTop, lbDetailTop;
        protected LinkButton lbBrowse_Relevance, lbBrowse_Raves, lbBrowse_Views, lbBrowse_Adds, lbBrowse_Newest;
        protected LinkButton lbTime_Today, lbTime_Week, lbTime_Month, lbTime_All, lbRestrict;
        protected LinkButton lbType_Videos, lbType_Photos, lbType_Music, lbType_Games, lbType_Patterns, lbType_Widgets, lbType_TV;
        protected CheckBox chkPhotoRequired;
        protected Label lblResultsBottom, lblShowRestricted;
        protected Literal litAdTest, litPng, litJSOpenConst;
        protected HtmlAnchor lnk_AccesPass;
        protected CheckBox cbx_AccessPass;
        protected AdRotator arHeader;
        protected Literal litHeaderText;
        protected HtmlAnchor aRegister;
        protected LinkButton btnSearch;

        protected PlaceHolder phBreadCrumb;
        protected DataList dlAssetsDetail, dlAssetsThumb;
        protected Repeater rptCategories;

        protected HtmlContainerControl divNoResults, divRestricted, div_AccessPassFilter;
        protected HtmlContainerControl liBrowse_Relevance, liBrowse_Raves, liBrowse_Views, liBrowse_Adds, liBrowse_Newest;
        protected HtmlContainerControl liTime_Today, liTime_Week, liTime_Month, liTime_All;
        protected HtmlContainerControl liType_Videos, liType_Photos, liType_Music, liType_Games, liType_Patterns, liType_TV, liType_Widgets;
        protected HtmlContainerControl spnSearchFilterDesc, spnResultsTitle, spnTagCloudTitle, spnFindTitle;
        protected HtmlContainerControl ifrmTagCloud;
        protected HtmlContainerControl liBrowseLabel;
        protected HtmlContainerControl ulTime;
        protected HtmlContainerControl spnHotTip, spnInstruction, spanRegister;
        protected HtmlContainerControl d1, d2, d3;	 //Hot Tips div tags
        protected HtmlInputHidden AssetId;

        private string cCSS_CLASS_SELECTED = "selected";

        private const string cORDER_BY_DEFAULT = Constants.SEARCH_ORDERBY_RAVES;
        private const int cNEW_WITHIN_DEFAULT = (int)Constants.eSEARCH_TIME_FRAME.WEEK;
        private const int cSEARCH_TYPE_DEFAULT = (int)Constants.eASSET_TYPE.VIDEO;

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #endregion

		protected media () 
		{
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			if (!IsPostBack)
			{
				InitializeSearch (true);

			}

			txtKeywords.Attributes.Add ("onkeydown", "CheckKey()");

			// Add the javascript for deleting comments
			string scriptString = "<script language=\"javaScript\">\n<!--\n function CheckKey () {\n";
			scriptString +="if (event.keyCode == 13)\n";
			scriptString += "{\n";
            scriptString += ClientScript.GetPostBackEventReference(btnSearch, "") + ";\n";				
			scriptString += "}}\n// -->\n";
			scriptString += "</script>";

            if (!ClientScript.IsClientScriptBlockRegistered(GetType (), "CheckKey"))
			{
                ClientScript.RegisterClientScriptBlock(GetType(), "CheckKey", scriptString);
			}


			// Setup Browse button client side scripting		
			lbType_Videos.Attributes.Add ("onclick", "ToggleHottip ('d1')");
			lbType_Photos.Attributes.Add ("onclick", "ToggleHottip ('d2')");
			lbType_Patterns.Attributes.Add ("onclick", "ToggleHottip ('d2')");
			lbType_Music.Attributes.Add ("onclick", "ToggleHottip ('d3')");
			lbType_Games.Attributes.Add ("onclick", "ToggleHottip ('d3')"); 
			lbType_Widgets.Attributes.Add ("onclick", "ToggleHottip ('d3')"); 
			lbType_TV.Attributes.Add ("onclick", "ToggleHottip ('d3')"); 


			//setup header nav bar
			HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.NONE;

			//set the meta tags
			Title = "Kaneva Entertainment. Upload, Watch, Listen and Play";
			MetaDataTitle = "<meta name=\"title\" content=\"Kaneva. A Unique Online Community and 3D Social Network.\" />";
			MetaDataDescription = "<meta name=\"description\" content=\"Upload your favorite media � videos, photos, music and games. Show it off on the Web and in the 3D Virtual World of Kaneva on your own virtual TV.  \"/>";
			MetaDataKeywords = "<meta name=\"keywords\" content=\"online community, social network, meet people, friends, networking, games, mmo, movies, 3D, virtual world, sharing photos, video, blog, bands, music, rate pics, digital, rpg, entertainment, join groups, forums, online social networking, caneva, kaneeva, kanneva, kineva\" />";


            //set the access pass filter toggle disable and invisible then process
            cbx_AccessPass.Enabled = div_AccessPassFilter.Visible = false;

            // Is user logged in?
			if (Request.IsAuthenticated)
			{
                //if (IsCurrentUserAdult())
                //to have the ability to toggle the restricted setting user must have access pass
                //can not have access pass if not an adult
                if (UserHasMaturePass())
                {
					hlChangeProfile.NavigateUrl = ResolveUrl("~/mykaneva/settings.aspx");
                    lblShowRestricted.Text = KanevaWebGlobals.CurrentUser.HasAccessPass ? "Yes" : "No";
				}
				else
				{
					divRestricted.Visible = false;
				}

                //configure the access pass only filter
                if (KanevaWebGlobals.CurrentUser.IsAdult)
                {
                    //set the Access Pass link value
                    lnk_AccesPass.HRef = GetAccessPassLink();

                    //disable/enable show access pass only based on users access pass status
                    cbx_AccessPass.Enabled = KanevaWebGlobals.CurrentUser.HasAccessPass;
                    div_AccessPassFilter.Visible = true;
                }

                aRegister.HRef = ResolveUrl("~/mykaneva/upload.aspx");
			}
			else
			{
                hlChangeProfile.NavigateUrl = GetLoginURL();
                aRegister.HRef = ResolveUrl("~/mykaneva/upload.aspx");
			}

			// Show correct advertisements
			AdvertisementSettings Settings = (AdvertisementSettings) Application ["Advertisement"];

			// Set mode of Google Ads
			if (Settings.Mode.Equals (AdvertisingMode.Live))
			{
				litAdTest.Visible = false;
			}
			else if (Settings.Mode.Equals (AdvertisingMode.Testing))
			{
				litAdTest.Text = "google_adtest = \"on\"";
			}

            if (AllowedToEdit())
            {
                // Add the javascript for deleting assets
                scriptString = "<script language=\"javaScript\">\n<!--\n function RestrictAsset (id) {\n";
                scriptString += "   $(\"AssetId\").value = id;\n";            // save the asset id clicked to the hidden field
                //scriptString += "   " + ClientScript.GetPostBackEventReference(btnRestrictAsset, "") + ";\n";             // call the __doPostBack function to post back the form and execute the PageClick event
                scriptString += "   $('" + btnRestrictAsset.ClientID + "').click();\n";             // call the __doPostBack function to post back the form and execute the PageClick event
                scriptString += "}\n// -->\n";
                scriptString += "</script>";

                if (!ClientScript.IsClientScriptBlockRegistered (GetType (), "RestrictAsset"))
                {
                    ClientScript.RegisterClientScriptBlock (GetType (), "RestrictAsset", scriptString);
                }
            }
        }

		
		
		#region Helper Methods

        protected bool AllowedToEdit()
        {
            bool allowEditing = false;

            //check security level
            switch (KanevaWebGlobals.CheckUserAccess((int)SitePrivilege.ePRIVILEGE.USER_DATA_ADMIN))
            {
                case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_FULL:
                    allowEditing = true;
                    break;
                case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_READ:
                    break;
                case (int)SitePrivilege.eACCESS_LEVEL.ACCESS_NONE:
                    break;
                default:
                    break;
            }
            return allowEditing;
        }

		/// <summary>
		/// BindData
		/// </summary>
		/// <param name="pageNumber"></param>
		private void BindData (int pageNumber)
		{
			// How many results do we display?
			int pgSize = 20;					

			// Set current page
			pgTop.CurrentPageNumber = pageNumber;
			pgBottom.CurrentPageNumber = pageNumber;

			// Clear existing results
			dlAssetsThumb.Visible  = false;
			dlAssetsDetail.Visible = false;

			divNoResults.Visible = false;
			
			string search_type_name = GetSearchTypeName();

			spnResultsTitle.InnerText = search_type_name;
			spnSearchFilterDesc.InnerText = GetSearchFilterSummary();

			spnTagCloudTitle.InnerText = (search_type_name.EndsWith ("s") ? search_type_name.Remove (search_type_name.Length-1, 1) : search_type_name);

			if (Is_Detail_View)
			{
				lbThumbTop.CssClass = "";
				lbDetailTop.Enabled = false;
				lbDetailTop.CssClass = "selected";
				lbDetailTop.Enabled = true;

				pgSize = 10;
			}
			else
			{
				lbDetailTop.CssClass = "";
				lbThumbTop.Enabled = false;
				lbThumbTop.CssClass = "selected";
				lbThumbTop.Enabled = true;

				pgSize = 20;
			}
			
			SearchAssets (pageNumber, pgSize);

			// Set the cloud type in the iframe source url
			ifrmTagCloud.Attributes.Add ("src", ResolveUrl("~/usercontrols/TagCloud.aspx?tag=" + Asset_Type_Id.ToString ()));

			// Show Pager
			pgTop.DrawControl ();
			pgBottom.DrawControl ();

            // set the upload media link
            aRegister.HRef = ResolveUrl ("~/mykaneva/upload.aspx?type=" + Asset_Type_Id.ToString ());

            // Log the search
            UserFacade userFacade = new UserFacade();
            userFacade.LogSearch(GetUserId(), Search_String, Asset_Type_Id);	
		}
		/// <summary>
		/// SearchAssets
		/// </summary>
		/// <param name="searchString"></param>
		/// <param name="filter"></param>
		/// <param name="pgNumber"></param>
		/// <param name="pgSize"></param>
		private void SearchAssets(int pgNumber, int pgSize)
		{
            MediaFacade mediaFacade = new MediaFacade();
			string category = string.Empty;
            PagedList<Asset> plAsset = new PagedList<Asset>();

            //// set which media we can see.  Public or Public and Kaneva Members only
            //if (media_filter.Length > 0)
            //{
            //    media_filter += " AND ";
            //}

            //PagedDataTable pds = null;

			if (Category_Id > 0)
			{
				category = Category_Id.ToString ();
			}

			// Get the data
            if (Search_String.Length > 0)
            {
                //pds = StoreUtility.SearchAssets (KanevaWebGlobals.CurrentUser.HasAccessPass, Asset_Type_Id,
                //    GetUserId(), false, chkPhotoRequired.Checked, Server.HtmlEncode(Search_String),
                //    category, New_Within, media_filter, SetOrderByValue(), pgNumber, pgSize, ShowOnlyAccessPass);

                plAsset = mediaFacade.SearchMedia(KanevaWebGlobals.CurrentUser.HasAccessPass, Asset_Type_Id,
                    false, chkPhotoRequired.Checked, Server.HtmlEncode(Search_String),
                    category, 0, SetOrderByValue(), pgNumber, pgSize, ShowOnlyAccessPass, null);
            }
            else
            {
                //////string strCountry = "";

                //////// If they are logged in filter by country if not searching
                //////if (Request.IsAuthenticated && Search_String.Length.Equals(0))
                //////{
                //////    // Put this in per AS 4/10
                //////    // strCountry = KanevaWebGlobals.CurrentUser.Country;
                //////    // Put this back per AS 4/24.09
                //////    // Is user in the list of countries we populate?

                //////    // And they have the preference
                //////    DataRow[] drCountry = WebCache.GetCountriesSummarized().Select("country = '" + KanevaWebGlobals.CurrentUser.Country + "'");
                //////    if (drCountry.Length > 0 && KanevaWebGlobals.CurrentUser.Preference.FilterByCountry)
                //////    {
                //////        strCountry = drCountry[0]["country"].ToString();
                //////    }
                //////}

                if (New_Within.Equals((int)Constants.eSEARCH_TIME_FRAME.ALL_TIME) || Order_By.Equals(Constants.SEARCH_ORDERBY_NEWEST))
                {
                    plAsset = mediaFacade.SearchMedia(KanevaWebGlobals.CurrentUser.HasAccessPass, Asset_Type_Id,
                        false, chkPhotoRequired.Checked, Server.HtmlEncode(Search_String),
                        category, 0, SetOrderByValue(), pgNumber, pgSize, ShowOnlyAccessPass, null);
                }
                else
                {
                    // Use specialized tables
                    // What table?
                    string sphinxIndex = "";

                    // How they are sorted
                    if (Order_By.Equals(Constants.SEARCH_ORDERBY_RAVES))
                    {
                        // Most raves
                        sphinxIndex = "asset_raves_srch_idx";
                    }
                    else if (Order_By.Equals(Constants.SEARCH_ORDERBY_ADDS))
                    {
                        // Most members
                        sphinxIndex = "asset_shares_srch_idx";
                    }
                    else // Views
                    {
                        sphinxIndex = "asset_views_srch_idx";
                    }

                    plAsset = mediaFacade.BrowseMedia(sphinxIndex, KanevaWebGlobals.CurrentUser.HasAccessPass, Asset_Type_Id,
                        false, chkPhotoRequired.Checked, category, New_Within, SetOrderByValue(), pgNumber, pgSize, ShowOnlyAccessPass);

                }
                //// Use the new performance searching, and new algorithm for Games, Flash, Widgets, Patterns, and TV Channels
                //pds = WebCache.SearchAssetsPerformance(KanevaWebGlobals.CurrentUser.HasAccessPass, Asset_Type_Id,
                //    chkPhotoRequired.Checked, category, New_Within, strCountry, media_filter, SetOrderByValue(), pgNumber, pgSize, ShowOnlyAccessPass);
            }
			
            // this is used to limit the number of pages to 5 when browsing
            if (Search_String.Length > 0)
            {
                pgTop.NumberOfPages = Math.Ceiling ((double) plAsset.TotalCount / pgSize).ToString ();
                pgBottom.NumberOfPages = Math.Ceiling ((double) plAsset.TotalCount / pgSize).ToString ();
				
                // Set up browse area
                ulTime.Visible = false;
                liBrowseLabel.InnerText = "Sort by";
                liBrowse_Relevance.Visible = true;
            }
            else
            {
            //if (Math.Ceiling((double)plAsset.TotalCount / pgSize) < 5 || AllowedToEdit())
            //    {
                    pgTop.NumberOfPages = Math.Ceiling((double)plAsset.TotalCount / pgSize).ToString();
                    pgBottom.NumberOfPages = Math.Ceiling((double)plAsset.TotalCount / pgSize).ToString();
                //}
                //else
                //{	
                //    pgTop.NumberOfPages = "5";
                //    pgBottom.NumberOfPages = "5";
                //}

				// Set up browse area
				ulTime.Visible = true;
				liBrowseLabel.InnerText = "Browse";
				liBrowse_Relevance.Visible = false;
			}


			// Display thumbnail or detail results?
                if (Is_Detail_View && plAsset.TotalCount > 0)
			{
				dlAssetsDetail.Visible = true;
                dlAssetsDetail.DataSource = plAsset;
				dlAssetsDetail.DataBind ();
			}
                else if (plAsset.TotalCount > 0)		
			{
				dlAssetsThumb.Visible = true;
                dlAssetsThumb.DataSource = plAsset;
				dlAssetsThumb.DataBind ();
			}
			else
			{
				ShowNoResults();
			}

                ShowResultsCount((int) plAsset.TotalCount, pgTop.CurrentPageNumber, 20, plAsset.Count);
		}
		
		/// <summary>
		/// ShowResultsCount
		/// </summary>
		/// <param name="total_count"></param>
		/// <param name="page_num"></param>
		/// <param name="count_per_page"></param>
		/// <param name="current_page_count"></param>
		private void ShowResultsCount(int total_count, int page_num,  int count_per_page, int current_page_count)
		{
			// Display the showing #-# of ### count
			string results = string.Empty;

			if (total_count > 0)
			{
				results = "Results " + KanevaGlobals.GetResultsText(total_count, page_num, count_per_page, 
					current_page_count, true, "%START%-%END% of %TOTAL%");
			}
			
			lblResultsBottom.Text = results;
		}
		/// <summary>
		/// ShowNoResults
		/// </summary>
		private void ShowNoResults()
		{
			divNoResults.InnerHtml = "<div class='noresults'><br/>Your search did not find any matching " + GetSearchTypeName() + ".</div>";
			divNoResults.Visible = true;
		}
		/// <summary>
		/// SetOrderByValue
		/// </summary>
		private string SetOrderByValue()
		{
			// How they are sorted
			if (Order_By.Equals (Constants.SEARCH_ORDERBY_RELEVANCE))
			{
				// Most Relevant
				CurrentSort = "";	
			}
			else if (Order_By.Equals (Constants.SEARCH_ORDERBY_NEWEST))
			{
				// Newest
                CurrentSort = "date_created DESC";
			}
			else if (Order_By.Equals (Constants.SEARCH_ORDERBY_RAVES))
			{
				// Most raves
				CurrentSort = "raves DESC";
			}
			else if (Order_By.Equals (Constants.SEARCH_ORDERBY_ADDS))
			{
				// Most members
                CurrentSort = "shares DESC";
			}
			else // Views
			{
                CurrentSort = "views DESC";
			}

			// Set sort and order by
			return CurrentSort;
		}

		/// <summary>
		/// GetAssetCategories
		/// </summary>
		private void GetAssetCategories(int asset_type_id)
		{
			DataTable dtCategories = StoreUtility.GetAssetCategories (Asset_Type_Id, "name");
			
			DataRow drAll = dtCategories.NewRow ();			
			drAll ["category_id"] = "0";
			drAll ["name"] = "All";
			drAll ["description"] = "All Categories";
			drAll ["asset_type_id"] = "0";

			dtCategories.Rows.InsertAt (drAll,0);

			rptCategories.DataSource = dtCategories;
			rptCategories.DataBind();
		}
		/// <summary>
		/// SetSearchType
		/// </summary>
		private void SetSearchType()
		{
			try
			{
				if (Search_Type < 0) // Set order by if not already in viewstate
				{
					if (Request[Constants.QUERY_STRING_SEARCH_TYPE] != null && Request[Constants.QUERY_STRING_SEARCH_TYPE] != string.Empty)
					{
						switch ( Convert.ToInt32(Request[Constants.QUERY_STRING_SEARCH_TYPE]) )
						{
							case (int) Constants.eASSET_TYPE.TV:
							case (int) Constants.eASSET_TYPE.WIDGET:
							case (int) Constants.eASSET_TYPE.VIDEO:
							case (int) Constants.eASSET_TYPE.MUSIC:
							case (int) Constants.eASSET_TYPE.PICTURE:
							case (int) Constants.eASSET_TYPE.GAME:
							case (int) Constants.eASSET_TYPE.PATTERN:
								Search_Type = Convert.ToInt32(Request[Constants.QUERY_STRING_SEARCH_TYPE]);
								Asset_Type_Id = Search_Type;
								break;
							default:
								Search_Type = cSEARCH_TYPE_DEFAULT;
								break;
						}
					}
					else
					{
						Search_Type = cSEARCH_TYPE_DEFAULT;
					}
				}
			}
			catch (Exception)
			{
				Search_Type = cSEARCH_TYPE_DEFAULT;
			}
		}
		/// <summary>
		/// SetHotTipType
		/// </summary>
		private void SetHotTipType()
		{
			switch ( Asset_Type_Id )
			{
				case (int) Constants.eASSET_TYPE.TV:
				case (int) Constants.eASSET_TYPE.WIDGET:
				case (int) Constants.eASSET_TYPE.VIDEO:
					d1.Style.Add ("display","block");
					litJSOpenConst.Text = "d1";
					break;
				case (int) Constants.eASSET_TYPE.PATTERN:
				case (int) Constants.eASSET_TYPE.PICTURE:
					d2.Style.Add ("display","block");
					litJSOpenConst.Text = "d2";
					break;
				case (int) Constants.eASSET_TYPE.MUSIC:
				case (int) Constants.eASSET_TYPE.GAME:
					d3.Style.Add ("display","block");
					litJSOpenConst.Text = "d3";
					break;
				default:
					d1.Style.Add ("display","block");
					litJSOpenConst.Text = "d1";
					break;
			}
		}
		/// <summary>
		/// SetOrderByType
		/// </summary>
		private void SetOrderByType()
		{
			try
			{
				if (Order_By == string.Empty) // Set order by if not already in viewstate
				{
					if (Request[Constants.QUERY_STRING_ORDERBY] != null && Request[Constants.QUERY_STRING_ORDERBY] != string.Empty)
					{
						switch ( Server.UrlDecode(Request[Constants.QUERY_STRING_ORDERBY].ToString ().ToLower ()) )
						{
							case Constants.SEARCH_ORDERBY_RELEVANCE:
							case Constants.SEARCH_ORDERBY_RAVES:	
							case Constants.SEARCH_ORDERBY_VIEWS:
							case Constants.SEARCH_ORDERBY_ADDS:
							case Constants.SEARCH_ORDERBY_NEWEST:
								Order_By = Server.UrlDecode(Request[Constants.QUERY_STRING_ORDERBY].ToString());
								break;
							default:
								Order_By = cORDER_BY_DEFAULT;
								break;
						}
					}
					else
					{
						Order_By = cORDER_BY_DEFAULT;
					}
				}
			}
			catch (Exception)
			{
				Order_By = cORDER_BY_DEFAULT;
			}
		}

		/// <summary>
		/// SetWithinTimeFrameType
		/// </summary>
		private void SetWithinTimeFrameType()
		{
			try
			{
				if (New_Within < 0) // Get the type from the radio button selected
				{
					if (Request[Constants.QUERY_STRING_WITHIN] != null && Request[Constants.QUERY_STRING_WITHIN] != string.Empty)
					{
						switch (Convert.ToInt32(Request[Constants.QUERY_STRING_WITHIN]))
						{
							case 0:
							case 1:
							case 7:
							case 30:
								New_Within = Convert.ToInt32(Request[Constants.QUERY_STRING_WITHIN]);
								break;
							default:
								New_Within = cNEW_WITHIN_DEFAULT;
								break;
						}
					}
					else
					{
						New_Within = cNEW_WITHIN_DEFAULT;   //all time
					}
				}
			}
			catch (Exception)
			{
				New_Within = cNEW_WITHIN_DEFAULT;	 //all time
			}
		}
		
		/// <summary>
		/// SetSearchKeywords
		/// </summary>
		private void SetSearchKeywords()
		{
			try
			{
				if (Search_String == string.Empty) 
				{
					if (Request[Constants.QUERY_STRING_KEYWORD] != null && Request[Constants.QUERY_STRING_KEYWORD] != string.Empty)
					{
						Search_String = Server.UrlDecode(Request[Constants.QUERY_STRING_KEYWORD].ToString());
						
						Order_By = Constants.SEARCH_ORDERBY_RELEVANCE;
						New_Within = 0;

						txtKeywords.Text = Search_String;
						txtKeywords.Style.Add ("color", "#535353");
					}
					else
					{
						Search_String = string.Empty;
					}
				}
			}
			catch (Exception)
			{
				Search_String = string.Empty;	 
			}
		}
		/// <summary>
		/// SetBrowseLinks
		/// </summary>
		private void SetBrowseLinks()
		{
			liBrowse_Relevance.Attributes.Add ("class", "");
			liBrowse_Raves.Attributes.Add ("class", "");
			liBrowse_Views.Attributes.Add ("class", "");
			liBrowse_Adds.Attributes.Add ("class", "");
			liBrowse_Newest.Attributes.Add ("class", "");

			switch (Order_By)
			{
				case Constants.SEARCH_ORDERBY_RELEVANCE:
					liBrowse_Relevance.Attributes.Add ("class", cCSS_CLASS_SELECTED);
					break;
				case Constants.SEARCH_ORDERBY_RAVES:
					liBrowse_Raves.Attributes.Add ("class", cCSS_CLASS_SELECTED);
					break;
				case Constants.SEARCH_ORDERBY_VIEWS:
					liBrowse_Views.Attributes.Add ("class", cCSS_CLASS_SELECTED);
					break;
				case Constants.SEARCH_ORDERBY_ADDS:
					liBrowse_Adds.Attributes.Add ("class", cCSS_CLASS_SELECTED); 
					break;
				case Constants.SEARCH_ORDERBY_NEWEST:
					liBrowse_Newest.Attributes.Add ("class", cCSS_CLASS_SELECTED);
					break;
			}
		}
		/// <summary>
		/// SetTimeLinks
		/// </summary>
		private void SetTimeLinks()
		{
			liTime_Today.Attributes.Add ("class", "");
			liTime_Week.Attributes.Add ("class", "");
			liTime_Month.Attributes.Add ("class", "");
			liTime_All.Attributes.Add ("class", "");

			switch (New_Within)
			{
				case (int) Constants.eSEARCH_TIME_FRAME.TODAY:
					liTime_Today.Attributes.Add ("class", cCSS_CLASS_SELECTED);
					break;
				case (int) Constants.eSEARCH_TIME_FRAME.WEEK:
					liTime_Week.Attributes.Add ("class", cCSS_CLASS_SELECTED);
					break;
				case (int) Constants.eSEARCH_TIME_FRAME.MONTH:
					liTime_Month.Attributes.Add ("class", cCSS_CLASS_SELECTED);
					break;
				case (int) Constants.eSEARCH_TIME_FRAME.ALL_TIME:
					liTime_All.Attributes.Add ("class", cCSS_CLASS_SELECTED);
					break;
			}
		}
		/// <summary>
		/// SetTypeLinks
		/// </summary>
		private void SetTypeLinks()
		{
			liType_Videos.Attributes.Add ("class", "");
			liType_Photos.Attributes.Add ("class", "");
			liType_Music.Attributes.Add ("class", "");
			liType_Games.Attributes.Add ("class", "");
			liType_Patterns.Attributes.Add ("class", "");
			liType_TV.Attributes.Add ("class", "");
			liType_Widgets.Attributes.Add ("class", "");

			switch (Asset_Type_Id)
			{
				case (int) Constants.eASSET_TYPE.VIDEO:
					liType_Videos.Attributes.Add ("class", cCSS_CLASS_SELECTED);
					break;
				case (int) Constants.eASSET_TYPE.PICTURE:
					liType_Photos.Attributes.Add ("class", cCSS_CLASS_SELECTED);
					break;
				case (int) Constants.eASSET_TYPE.MUSIC:
					liType_Music.Attributes.Add ("class", cCSS_CLASS_SELECTED);
					break;
				case (int) Constants.eASSET_TYPE.GAME:
					liType_Games.Attributes.Add ("class", cCSS_CLASS_SELECTED);
					break;
				case (int) Constants.eASSET_TYPE.PATTERN:
					liType_Patterns.Attributes.Add ("class", cCSS_CLASS_SELECTED);
					break;
				case (int) Constants.eASSET_TYPE.TV:
					liType_TV.Attributes.Add ("class", cCSS_CLASS_SELECTED);
					break;
				case (int) Constants.eASSET_TYPE.WIDGET:
					liType_Widgets.Attributes.Add ("class", cCSS_CLASS_SELECTED);
					break;
			}

			GetAssetCategories (Asset_Type_Id);
		}
		/// <summary>
		/// InitializeNewSearch
		/// </summary>
		private void InitializeSearch (bool Set_Keyword)
		{
			if (!IsPostBack)
			{
				Is_Detail_View = false;	
			}
			
			// Set Search Type
			SetSearchType ();
			
			// Set Search keyword
			if (Set_Keyword)
			{
				SetSearchKeywords();
			}

			// Set OrderBy Type
			SetOrderByType();
			
			// Set Within Type
			SetWithinTimeFrameType();
			
			// Get the Category list
			GetAssetCategories (Asset_Type_Id);

			SetHotTipType();

			// Set selected Find links
			SetTypeLinks();
			SetBrowseLinks();
			SetTimeLinks();

			BindData(1);
		}
		/// <summary>
		/// GetSearchFilterSummary
		/// </summary>
		private string GetSearchFilterSummary()
		{
			string browse_txt = string.Empty;
			string time_txt = string.Empty;

			switch (Order_By)
			{
				case Constants.SEARCH_ORDERBY_RAVES:
					browse_txt = "Most Raves";
					break;
				case Constants.SEARCH_ORDERBY_VIEWS:
					browse_txt = "Most Views";
					break;
				case Constants.SEARCH_ORDERBY_ADDS:
					browse_txt = "Most Adds"; 
					break;
				case Constants.SEARCH_ORDERBY_NEWEST:
					browse_txt = "Newest";
					break;
				default:
					browse_txt = "Best Match";
					break;
			}
			
			switch (New_Within)
			{
				case (int) Constants.eSEARCH_TIME_FRAME.TODAY:
					time_txt = "Today";
					break;
				case (int) Constants.eSEARCH_TIME_FRAME.WEEK:
					time_txt = "This Week";
					break;
				case (int) Constants.eSEARCH_TIME_FRAME.MONTH:
					time_txt = "This Month";
					break;
				case (int) Constants.eSEARCH_TIME_FRAME.ALL_TIME:
					time_txt = "All Time";
					break;
			}

			return browse_txt + " - " + time_txt;
		}
		/// <summary>
		/// SetTypeLinks
		/// </summary>
		private string GetSearchTypeName()
		{
			switch (Asset_Type_Id)
			{
				case (int) Constants.eASSET_TYPE.WIDGET:
					return "Flash Widgets";
				case (int) Constants.eASSET_TYPE.TV:
					return "TV Channels";
				case (int) Constants.eASSET_TYPE.VIDEO:
					return "Videos";
				case (int) Constants.eASSET_TYPE.PICTURE:
					return "Photos";
				case (int) Constants.eASSET_TYPE.MUSIC:
					return "Music";
				case (int) Constants.eASSET_TYPE.GAME:
					return "Games";
				case (int) Constants.eASSET_TYPE.PATTERN:
					return "Patterns";
				default:
					return "Videos";
			}
		}

        protected void AdCreated(Object source, AdCreatedEventArgs e)
        {
            if (e.AdProperties["HeaderText"].ToString() != "")
            {

                litHeaderText.Text = e.AdProperties["HeaderText"].ToString();
                //litHeaderText.Text = "test";
                arHeader.Visible = false;
            }
        }

		#endregion


		#region Event Handlers

        protected void APCheckChanged(object sender, EventArgs e)
        {
            ShowOnlyAccessPass = cbx_AccessPass.Checked;
            BindData(1);
        }

        /// <summary>
		/// Execute when the user clicks the the Search button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnSearch_Click (object sender, EventArgs e)			  
		{
			if (KanevaWebGlobals.ContainsInjectScripts (txtKeywords.Text))
			{
				m_logger.Warn ("User " + KanevaWebGlobals.GetUserId () + " tried to script from IP " + Common.GetVisitorIPAddress());
				ShowErrorOnStartup ("Your input contains invalid scripting, please remove script code and try again.", true);
				return;
			}			
			
			//Search_String = txtKeywords.Text.ToLower().Trim() != cTEXTBOX_INSTRUCTION ? txtKeywords.Text : "";
			Search_String = txtKeywords.Text.Trim();
			
			Order_By = Constants.SEARCH_ORDERBY_NEWEST;
			if (Search_String.Length > 0)
			{
				Order_By = Constants.SEARCH_ORDERBY_RELEVANCE;
			}

			New_Within = (int) Constants.eSEARCH_TIME_FRAME.ALL_TIME;
			Category_Id = 0;
			
			lblResultsBottom.Visible = true;
			if (Search_String.Length == 0)
			{
				lblResultsBottom.Visible = false;
				New_Within = 30;
			}

			InitializeSearch (false);
		}
		/// <summary>
		/// Execute when the user clicks the the view type link button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void lbViewAs_Click (object sender, EventArgs e) 
		{
			if (((LinkButton)sender).Attributes["View"].ToString().ToLower() == "thumb")
			{
				Is_Detail_View = false;
			}
			else
			{
				Is_Detail_View = true;												  
			}

			BindData(1);
		}
		/// <summary>
		/// Execute when the user clicks the Browse type link button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void lbBrowse_Click (object sender, CommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case Constants.SEARCH_ORDERBY_RAVES:
				case Constants.SEARCH_ORDERBY_VIEWS:
				case Constants.SEARCH_ORDERBY_ADDS:
				case Constants.SEARCH_ORDERBY_NEWEST:
				case Constants.SEARCH_ORDERBY_RELEVANCE:
					Order_By = e.CommandName;
					break;
				default:
					Order_By = Constants.SEARCH_ORDERBY_NEWEST;
					break;
			}
    
			SetBrowseLinks();

			BindData (1);
		}
		/// <summary>
		/// Execute when the user clicks the Time range link button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void lbTime_Click (object sender, CommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "today":
					New_Within = (int) Constants.eSEARCH_TIME_FRAME.TODAY;
					break;
				case "week":
					New_Within = (int) Constants.eSEARCH_TIME_FRAME.WEEK;
					break;
				case "month":
					New_Within = (int) Constants.eSEARCH_TIME_FRAME.MONTH;
					break;
				case "alltime":
					New_Within = (int) Constants.eSEARCH_TIME_FRAME.ALL_TIME;
					break;
			}
    
			SetTimeLinks();

			BindData (1);
		}
		/// <summary>
		/// rptCategories_ItemDataBound
		/// </summary>
		protected void lbType_Click (object sender, CommandEventArgs e)
		{
			switch (e.CommandName.ToLower ())
			{
				case "videos":
					Asset_Type_Id = (int) Constants.eASSET_TYPE.VIDEO;
					break;
				case "photos":
					Asset_Type_Id = (int) Constants.eASSET_TYPE.PICTURE;
					break;
				case "music":
					Asset_Type_Id = (int) Constants.eASSET_TYPE.MUSIC;
					break;
				case "games":
					Asset_Type_Id = (int) Constants.eASSET_TYPE.GAME;
					break;
				case "patterns":
					Asset_Type_Id = (int) Constants.eASSET_TYPE.PATTERN;
					break;
				case "tv":
					Asset_Type_Id = (int) Constants.eASSET_TYPE.TV;
					break;
				case "widgets":
					Asset_Type_Id = (int) Constants.eASSET_TYPE.WIDGET;
					break;
			}

			SetTypeLinks ();

			spnFindTitle.InnerText = e.CommandName;

			// Get the categories for the selected type
			GetAssetCategories (Asset_Type_Id);
			Category_Id = 0;

			BindData (1);
		}
		/// <summary>
		/// rptCategories_ItemDataBound
		/// </summary>
		private void rptCategories_ItemDataBound (object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				LinkButton lbCategory = (LinkButton) e.Item.FindControl ("lbCategory");
				lbCategory.CommandArgument = DataBinder.Eval (e.Item.DataItem, "category_id").ToString ();
				lbCategory.CommandName = DataBinder.Eval (e.Item.DataItem, "name").ToString ();
				lbCategory.Text = DataBinder.Eval (e.Item.DataItem, "name").ToString ();
				lbCategory.ToolTip = "View items in the " + DataBinder.Eval (e.Item.DataItem, "name").ToString () + " category";

				HtmlContainerControl liCategory = (HtmlContainerControl) e.Item.FindControl ("liCategory");
				
				// select All as the default
				if (lbCategory.CommandArgument == "0" && liCategory != null)
				{
					liCategory.Attributes.Add ("class", cCSS_CLASS_SELECTED);
				}
			}
		}
		/// <summary>
		/// rptAssets_ItemCommand
		/// </summary>
		private void rptCategories_ItemCommand (object source, RepeaterCommandEventArgs e)
		{
			string command = e.CommandName;
			string cmdarg = e.CommandArgument.ToString ();

			Category_Id = Convert.ToInt32(e.CommandArgument);

			LinkButton lbCat = (LinkButton) e.Item.FindControl ("lbCategory");

			Repeater rptCategories = (Repeater) e.Item.Parent;
			if (rptCategories != null)
			{
				for (int i = 0; i < rptCategories.Items.Count; i++)
				{
					if (rptCategories.Items[i].ItemType == ListItemType.Item || rptCategories.Items[i].ItemType == ListItemType.AlternatingItem)
					{
						HtmlContainerControl liCategory = (HtmlContainerControl) rptCategories.Items[i].FindControl("liCategory");
						liCategory.Attributes.Add ("class", "");
					}
				}
			}

			if (lbCat != null)
			{
				Category_Id = Convert.ToInt32(lbCat.CommandArgument);
				
				((HtmlContainerControl)lbCat.Parent).Attributes.Add ("class", cCSS_CLASS_SELECTED);
			}

			BindData (1);
		}

		/// <summary>
		/// Delete an asset
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void Delete_Asset (Object sender, EventArgs e)
		{
			if (AllowedToEdit())
			{
				StoreUtility.DeleteAsset (Convert.ToInt32 (AssetId.Value), GetUserId ());
				pg_PageChange (this, new PageChangeEventArgs (pgTop.CurrentPageNumber));
				//BindData (1);
			}
		}

        /// <summary>
        /// Restrict an asset
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnRestrictAsset_Click (Object sender, EventArgs e)
        {
            if (AllowedToEdit())
            {
                StoreUtility.UpdateAssetRestriction (Convert.ToInt32 (AssetId.Value), true);
                //pg_PageChange (this, new PageChangeEventArgs (pgTop.CurrentPageNumber));
                BindData (pgTop.CurrentPageNumber);
            }
        }
        

#region Pager & Filter control events
		/// <summary>
		/// Execute when the user selects a page change link from the Pager control
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void pg_PageChange (object sender, PageChangeEventArgs e)
		{
			BindData (e.PageNumber);
		}

		#endregion
		#endregion


		#region Properties

        /// <summary>
        /// indicator to show only access pass or not
        /// </summary>
        private bool ShowOnlyAccessPass
        {
            get
            {
                if (ViewState["showOnlyAP"] == null)
                {
                    return false;
                }
                else
                {
                    return (bool)ViewState["showOnlyAP"];
                }
            }
            set
            {
                ViewState["showOnlyAP"] = value;
            }
        }
        
        /// <summary>
		/// Asset_Type_Id
		/// </summary>
		private int Asset_Type_Id
		{
			set
			{
				ViewState ["Asset_Type_Id"] = value;
			}
			get
			{
				if (ViewState ["Asset_Type_Id"] == null)
				{
					return (int) Constants.eASSET_TYPE.VIDEO;
				}
				else
				{
					return Convert.ToInt32 (ViewState ["Asset_Type_Id"]);
				}
			}
		}
		/// <summary>
		/// Search_Type
		/// </summary>
		private int Search_Type
		{
			set
			{
				ViewState ["Search_Type"] = value;
			}
			get
			{
				if (ViewState ["Search_Type"] == null)
				{
					return -1;
				}
				else
				{
					return Convert.ToInt32 (ViewState ["Search_Type"]);
				}
			}
		}
		/// <summary>
		/// New_Within
		/// </summary>
		private int New_Within
		{
			set
			{
				ViewState ["New_Within"] = value;
			}
			get
			{
				if (ViewState ["New_Within"] == null)
				{
					return -1;
				}
				else
				{
					return Convert.ToInt32 (ViewState ["New_Within"]);
				}
			}
		}
		/// <summary>
		/// Search_String
		/// </summary>
		private string Search_String
		{
			set
			{
				ViewState ["Search_String"] = value.Trim();
			}
			get
			{
				if (ViewState ["Search_String"] == null)
				{
					return string.Empty; //cORDER_BY_DEFAULT;
				}
				else
				{
					return ViewState ["Search_String"].ToString();
				}
			}
		}

		/// <summary>
		/// orderBy
		/// </summary>
		private string Order_By
		{
			set
			{
				ViewState ["Order_By"] = value;
			}
			get
			{
				if (ViewState ["Order_By"] == null)
				{
					return string.Empty; //cORDER_BY_DEFAULT;
				}
				else
				{
					return ViewState ["Order_By"].ToString();
				}
			}
		}

		/// <summary>
		/// categoryId
		/// </summary>
		private int Category_Id
		{
			set
			{
				ViewState ["Category_Id"] = value;
			}
			get
			{
				if (ViewState ["Category_Id"] == null)
				{
					return 0;   // All
				}
				else
				{
					return Convert.ToInt32(ViewState ["Category_Id"]);
				}
			}
		}

		/// <summary>
		/// isDetailView
		/// </summary>
		private bool Is_Detail_View
		{
			set
			{
				ViewState ["Is_Detail_View"] = value;
			}
			get
			{
				if (ViewState ["Is_Detail_View"] == null)
				{
					return false;
				}
				else
				{
					return Convert.ToBoolean (ViewState ["Is_Detail_View"]);
				}
			}
		}
		# endregion

	
		#region Web Form Designer generated code

		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
			rptCategories.ItemCommand += new RepeaterCommandEventHandler (rptCategories_ItemCommand);
			rptCategories.ItemDataBound += new RepeaterItemEventHandler (rptCategories_ItemDataBound);
            pgTop.PageChanged += new PageChangeEventHandler (pg_PageChange);
			pgBottom.PageChanged +=new PageChangeEventHandler (pg_PageChange);
        }
		#endregion
	}
}
