<%@ Page language="c#" Codebehind="sitemap.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.sitemap" %>

<link href="css/friends.css" rel="stylesheet" type="text/css"/>
<link href="css/new.css" rel="stylesheet" type="text/css" />		


<table border="0" cellspacing="0" cellpadding="0" class="newcontainer" align="center">
	<tr>
		<td>
			<table  border="0" cellpadding="0" cellspacing="0" width="100%" class="newcontainerborder">
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
				<tr>
					<td class=""><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
					<td valign="top" class="newdatacontainer">
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
								
									<!-- TOP STATUS BAR -->
									<div id="pageheader">
										<table cellpadding="0" cellspacing="0" border="0" width="99%">
											<tr>
												<td nowrap align="left">
													<table cellpadding="0" cellspacing="0" border="0">
														<tr>
															<td><h1>Site Map</h1></td>
														</tr>
													</table>
												</td>
												
												<td align="right" valign="middle">
													<table cellpadding="0" cellspacing="0" border="0" width="690">
														<tr>
															<td class="headertout" width="175"></td>
															<td class="searchnav" width="260"></td>
															<td align="left" width="130"></td>
															<td class="searchnav" align="right" width="210" nowrap>
																
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</div>
									<!-- END TOP STATUS BAR -->	
								
								</td>
							</tr>
							<tr>
								<td width="968" align="left"  valign="top">
									<div class="module whitebg">
									<span class="ct"><span class="cl"></span></span>
									<table cellpadding="8" cellspacing="0" border="0" width="98%">
										<tr>
											
											<td bgcolor="#f9f9fa" valign="top" width="35%" style="padding-left:20px;"><a runat="server" href="~/">Home</a></td>
											<td bgcolor="#f9f9fa" valign="top" width="65%">Kaneva is an exciting new kind of free online community.</td>
											
										</tr>
										<tr>
											
											<td bgcolor="#f9f9fa" valign="top" width="35%" style="padding-left:20px;"><a id="A2" runat="server" href="~/community/install3d.kaneva">Install 3D World</a></td>
											<td bgcolor="#f9f9fa" valign="top" width="65%">Download and install the 3D Virtual World of Kaneva.</td>
											
										</tr>
										<tr>
											
											<td valign="top" style="padding-left:20px;"><a runat="server" href="http://blog.kaneva.com/?page_id=2">About Kaneva</a></td>
											<td valign="top">Learn more about Kaneva including our team and mantra.</td>
											
										</tr>
										
										<tr>
											
											<td bgcolor="#f9f9fa" valign="top" style="padding-left:20px;"><a runat="server" href="~/share/default.aspx">Share Kaneva</a></td>
											<td bgcolor="#f9f9fa" valign="top">If you have a profile, blog or web site we want you to link to us.</td>
											
										</tr>
																				
										<tr>
											
											<td bgcolor="#f9f9fa" valign="top" style="padding-left:20px;"><a runat="server" href="http://ideas.kaneva.com">Kaneva Ideas</a></td>
											<td bgcolor="#f9f9fa" valign="top">Share your ideas with the Kaneva team, offer comments and suggestions.</td>
											
										</tr>
										<!--<tr>
											
											<td valign="top" style="padding-left:20px;"><a runat="server" href="~/keepitreal">Sponsorship</a></td>
											<td valign="top">Want to build awareness around your brand, company or product?</td>
											
										</tr>-->
										<tr>
											
											<td bgcolor="#f9f9fa"valign="top" style="padding-left:20px;"><a runat="server" href="~/community/Careers.kaneva">Careers</a></td>
											<td bgcolor="#f9f9fa" valign="top">Inquire about the latest job opportunities at Kaneva.</td>
											
										</tr>
										<tr>
											
											<td valign="top" style="padding-left:20px;"><a runat="server" id="aHelp" href="http://blog.kaneva.com/?page_id=10">Contact Us</a></td>
											<td valign="top">We love hearing from you. Tell us what you think.</td>
											
										</tr>
										<tr>
											
											<td bgcolor="#f9f9fa" valign="top" style="padding-left:20px;"><a runat="server" href="http://support.kaneva.com">Support Center</a></td>
											<td bgcolor="#f9f9fa" valign="top">Tutorials, tips and tricks, frequently asked questions and more.</td>
											
										</tr>
										<tr>
											
											<td valign="top" style="padding-left:20px;"><a runat="server" href="http://forums.kaneva.com/">Member Forums</a></td>
											<td valign="top">Interact with the Kaneva support team and other members.</td>
											
										</tr>
										<tr>
											
											<td bgcolor="#f9f9fa"valign="top" style="padding-left:20px;"><a runat="server" href="~/overview/guidelines.aspx">Member Guidelines</a></td>
											<td bgcolor="#f9f9fa" valign="top">Our community guidelines. Above all please be polite and respectful. </td>
											
										</tr>
										<tr>
											
											<td valign="top" style="padding-left:20px;"><a runat="server" href="~/community/safety.kaneva">Parental Resources</a></td>
											<td valign="top">Find out about our suggested safety tips and resources for.</td>
											
										</tr>
										<tr>
											
											<td bgcolor="#f9f9fa"valign="top" style="padding-left:20px;"><a runat="server" href="~/3d-virtual-world/virtual-life.kaneva">3D Kaneva</a></td>
											<td bgcolor="#f9f9fa" valign="top">All the resources you�ll need for virtual life inside Kaneva.</td>
											
										</tr>

										<tr>
											
											<td valign="top" style="padding-left:20px;"><a runat="server" href="~/community/news.kaneva">Press Center</a></td>
											<td valign="top">News from the blogosphere, press releases, screen shots and more.</td>
											
										</tr>
										<tr>
											
											<td bgcolor="#f9f9fa"valign="top" style="padding-left:20px;"><a runat="server" href="http://blog.kaneva.com">Official Blog</a></td>
											<td bgcolor="#f9f9fa" valign="top">Read and discuss ideas related to Kaneva and the virtual world industry.</td>
											
										</tr>
										<tr>
											
											<td valign="top" style="padding-left:20px;"><a id="A4" runat="server" href="~/community/channel.kaneva">Community Search</a></td>
											<td valign="top">Start a community or view the latest raved communities this week.</td>
											
										</tr>
										<tr>
											
											<td bgcolor="#f9f9fa"valign="top" style="padding-left:20px;"><a id="A5" runat="server" href="~/people/people.kaneva">People Search</a></td>
											<td bgcolor="#f9f9fa" valign="top">Search Kaneva for friends. Connect with new people around interests.</td>
											
										</tr>
										<tr>
											
											<td valign="top" style="padding-left:20px;"><a id="A6" runat="server" href="~/watch/watch.kaneva">Media Search</a></td>
											<td valign="top">The latest viral videos, funny videos� photos, music, and games too.</td>
											
										</tr>
									</table>
									<span class="cb"><span class="cl"></span></span>
								</div>
								</td>
								
									</td>
							</tr>
						</table>
					</td>
					<td class=""><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
				</tr>
				<tr>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
				<tr>
					<td><img runat="server" src="~/images/spacer.gif" width="1" height="14" /></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
