///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web.Caching;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using KlausEnt.KEP.Kaneva.framework.widgets;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for Cache.
	/// </summary>
	public class WebCache
	{
		public WebCache ()
		{
		}

        /// <summary>
        /// Get Countries
        /// </summary>
        public static DataTable GetCountriesSummarized()
        {
            // Build a unique cache key
            string cacheKey = "wcSummaryCountries";
            DataTable dtCountries = (DataTable)Global.Cache()[cacheKey];

            if (dtCountries == null)
            {
                // Add to the cache
                dtCountries = KanevaGlobals.GetDatabaseUtilityStats().GetDataTable("SELECT * FROM report_kaneva.countries_to_summarize c");
                Global.Cache().Insert(cacheKey, dtCountries, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return dtCountries;
        }

        /// <summary>
        /// GetRestrictedWords
        /// </summary>
        public static DataTable GetRestrictedWords ()
        {
            DataTable dtRestrictedWords = (DataTable) Global.Cache () [cRESTWORDS];
            if (dtRestrictedWords == null)
            {
                // Add to the cache
                dtRestrictedWords = StoreUtility.GetRestrictedWords ();
                Global.Cache ().Insert (cRESTWORDS, dtRestrictedWords, null, DateTime.Now.AddMinutes (15), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return dtRestrictedWords;
        }

        /// <summary>
        /// GetRestrictedWordsList - This method caches and gets a specified string of words based on the restricion type
        /// and the match type.  The string is generated with | (pipe) seperator which is the format used by the REGEX object
        /// for the regular expression matching.
        /// </summary>
        public static string GetRestrictedWordsList (Constants.eRESTRICTION_TYPE rest_type, Constants.eMATCH_TYPE match_type)
        {
            string cacheID = string.Empty;

            // Set the cacheID so we can see if string is already cached
            if (rest_type == Constants.eRESTRICTION_TYPE.POTTY_MOUTH)
            {
                cacheID = (match_type == Constants.eMATCH_TYPE.EXACT ? cPMEXACT : cPMANY);
            }
            else if (rest_type == Constants.eRESTRICTION_TYPE.RESERVED)
            {
                cacheID = (match_type == Constants.eMATCH_TYPE.EXACT ? cRESTEXACT : cRESTANY);
            }

            // Try to get string from cache
            string strWordList = (string) Global.Cache ()[cacheID];
            if (strWordList == null)
            {
                strWordList = string.Empty;

                // Set the match type param for the query string
                string matchTypeClause = "match_type=" + (match_type == Constants.eMATCH_TYPE.EXACT ? "'exact'" : "'anywhere'"); 
                
                // Set the restriction type param for the query string
                string restTypeClause = "restriction_type_id=" + (rest_type == Constants.eRESTRICTION_TYPE.RESERVED ? 
                    (int)Constants.eRESTRICTION_TYPE.RESERVED : (int)Constants.eRESTRICTION_TYPE.POTTY_MOUTH);

                // Get the word list
                DataRow[] drRestWords = WebCache.GetRestrictedWords ().Select (restTypeClause + " AND " + matchTypeClause);
                if (drRestWords.Length > 0)
                {
                    // Let's build the list of words we need to search for
                    for (int i = 0; i < drRestWords.Length; i++)
                    {
                        strWordList += drRestWords[i]["word"].ToString () + "|";
                    }

                    // Remove the trailing pipe |
                    if (strWordList.EndsWith ("|"))
                    {
                        strWordList = strWordList.Substring (0, strWordList.Length - 1);
                    }
                }                        

                // Add to the cache
                Global.Cache ().Insert (cacheID, strWordList, null, DateTime.Now.AddMinutes (15), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return strWordList;
        }
        
        /// <summary>
        /// Get a specific country
        /// </summary>
        /// <returns></returns>
        public static Country GetCountry (string countryId)
        {
            IList <Country> dtCountries = GetCountries ();
            Country _country = null;

            foreach (Country c in dtCountries)
            {
                if (c.CountryId == countryId)
                {
                    _country = c;
                    break;
                }
            }

            return _country;
        }

        /// <summary>
        /// GetAllViolationTypes
        /// </summary>
        public static IList<ViolationType> GetAllViolationTypes()
        {
            string cacheViolationTypeName = "violationTypes";

            // Load countries
            IList<ViolationType> dtViolationTypes = (IList<ViolationType>)Global.Cache()[cacheViolationTypeName];

            if (dtViolationTypes == null)
            {
                // Get and add to cache
                ViolationsFacade violationFacade = new ViolationsFacade();
                dtViolationTypes = violationFacade.GetAllViolationTypes();
                Global.Cache().Insert(cacheViolationTypeName, dtViolationTypes, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return dtViolationTypes;
        }

        /// <summary>
        /// GetAllViolationActionTypes
        /// </summary>
        public static IList<ViolationActionType> GetAllViolationActionTypes()
        {
            string cacheViolationActionTypeName = "violationActionTypes";

            // Load countries
            IList<ViolationActionType> dtViolationActionTypes = (IList<ViolationActionType>)Global.Cache()[cacheViolationActionTypeName];

            if (dtViolationActionTypes == null)
            {
                // Get and add to cache
                ViolationsFacade violationFacade = new ViolationsFacade();
                dtViolationActionTypes = violationFacade.GetAllViolationActionTypes();
                Global.Cache().Insert(cacheViolationActionTypeName, dtViolationActionTypes, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return dtViolationActionTypes;
        }

        /// <summary>
        /// GetPassGroupsToSubscription
        /// </summary>
        public static DataTable GetPassGroupsToSubscription()
        {
            string cachePassGroup2SubscriptName = "passGroup2Subscription";

            // Load table
            DataTable dtPassGroupsToSubscription = (DataTable)Global.Cache()[cachePassGroup2SubscriptName];

            if (dtPassGroupsToSubscription == null)
            {
                // Get and add to cache
                SubscriptionFacade subscriptionFacade = new SubscriptionFacade();
                dtPassGroupsToSubscription = subscriptionFacade.GetPassGroupsToSubscription();
                Global.Cache().Insert(cachePassGroup2SubscriptName, dtPassGroupsToSubscription, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return dtPassGroupsToSubscription;
        }

        /// <summary>
        /// GetCountries
        /// </summary>
        public static IList<Country> GetCountries()
        {
            string cacheItemsName = "countries";

            // Load countries
            IList<Country> dtCountries = (IList<Country>)Global.Cache()[cacheItemsName];

            if (dtCountries == null)
            {
                // Get and add to cache
                UserFacade userFacade = new UserFacade();
                dtCountries = userFacade.GetCountries();
                Global.Cache().Insert(cacheItemsName, dtCountries, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return dtCountries;
        }

        /// <summary>
        /// GetStates
        /// </summary>
        public static IList<State> GetStates()
        {
            string cacheItemsName = "states";

            // Load states
            IList<State> dtStates = (IList<State>)Global.Cache()[cacheItemsName];

            if (dtStates == null)
            {
                // Get and add to cache
                UserFacade userFacade = new UserFacade();
                dtStates = userFacade.GetStates();
                Global.Cache().Insert(cacheItemsName, dtStates, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return dtStates;
        }

        /// <summary>
        /// GetAssetTypes
        /// </summary>
        public static DataTable GetAssetTypes()
        {
            string cacheAssetTypes = "assetTypes";

            // Load asset perms
            DataTable dtAssetTypes = (DataTable)Global.Cache()[cacheAssetTypes];
            if (dtAssetTypes == null)
            {
                // Add the community list to the cache
                dtAssetTypes = StoreUtility.GetAssetTypes(); ;
                Global.Cache().Insert(cacheAssetTypes, dtAssetTypes, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return dtAssetTypes;
        }

        public static DataRow GetZipCode (string zip_code)
        {
            DataTable dtZipCodes = GetZipCodes ();
            DataRow[] drZipCode = dtZipCodes.Select("zip_code='" + zip_code + "'");

            return drZipCode[0];
        }

        /// <summary>
        /// GetZipCodes
        /// </summary>
        public static DataTable GetZipCodes ()
        {
            string cacheZipCodes = "zipCodes";

            // Load asset perms
            DataTable dtZipCodes = (DataTable) Global.Cache ()[cacheZipCodes];
            if (dtZipCodes == null)
            {
                // Add the community list to the cache
                dtZipCodes = UsersUtility.GetZipCodes ();
                Global.Cache ().Insert (cacheZipCodes, dtZipCodes, null, DateTime.Now.AddMinutes (30), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return dtZipCodes;
        }

        /// <summary>
        /// GetWOKAvatarTemplates
        /// </summary>
        public static DataTable GetWOKAvatarTemplates (string gender)
        {
            // Get Female avatars
            if (gender.ToLower () == "f")
            {
                string cacheWOKFemaleAvatars = "WOKFemaleAvatars";

                // Load asset perms
                DataTable dtWOKFemaleAvatars = (DataTable) Global.Cache ()[cacheWOKFemaleAvatars];
                if (dtWOKFemaleAvatars == null)
                {
                    // Add the community list to the cache
                    dtWOKFemaleAvatars = UsersUtility.GetWOKAvatarTemplates (gender);
                    Global.Cache ().Insert (cacheWOKFemaleAvatars, dtWOKFemaleAvatars, null, DateTime.Now.AddMinutes (30), System.Web.Caching.Cache.NoSlidingExpiration);
                }

                return dtWOKFemaleAvatars;
            }
            else  // Get Male avatars
            {
                string cacheWOKMaleAvatars = "WOKMaleAvatars";

                // Load asset perms
                DataTable dtWOKMaleAvatars = (DataTable) Global.Cache ()[cacheWOKMaleAvatars];
                if (dtWOKMaleAvatars == null)
                {
                    // Add the community list to the cache
                    dtWOKMaleAvatars = UsersUtility.GetWOKAvatarTemplates (gender);
                    Global.Cache ().Insert (cacheWOKMaleAvatars, dtWOKMaleAvatars, null, DateTime.Now.AddMinutes (30), System.Web.Caching.Cache.NoSlidingExpiration);
                }

                return dtWOKMaleAvatars;
            }
        }

        /// <summary>
        /// Get the Asset Types
        /// </summary>
        /// <returns></returns>
        public static DataView GetAssetTypes (string filter)
        {
            return new DataView (GetAssetTypes(), filter, "name", DataViewRowState.CurrentRows);
        }

        /// <summary>
        /// GetAssetPermissions
        /// </summary>
        public static DataTable GetAssetPermissions()
        {
            string cacheItemsName = "assetPerms";

            // Load asset perms
            DataTable dtAssetPerms = (DataTable)Global.Cache()[cacheItemsName];
            if (dtAssetPerms == null)
            {
                // Add the community list to the cache
                dtAssetPerms = StoreUtility.GetAssetPermissions(); ;
                Global.Cache().Insert(cacheItemsName, dtAssetPerms, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return dtAssetPerms;
        }

		/// <summary>
		/// GetAssetCategories
		/// </summary>
		public static DataTable GetAssetCategories (int assetTypeId)
		{
			DataTable dtAssetCategories = null;
			Cache cache = Global.Cache ();
			int cacheTime = 15;

			switch (assetTypeId)
			{
				case (int) Constants.eASSET_TYPE.ASSET:
				{
					dtAssetCategories = (DataTable) cache [cASSET_CATEGORIES];
					if (dtAssetCategories == null)
					{
						dtAssetCategories = StoreUtility.GetAssetCategories (assetTypeId, "name ASC");
						cache.Insert (cASSET_CATEGORIES, dtAssetCategories, null, DateTime.Now.AddMinutes (cacheTime), System.Web.Caching.Cache.NoSlidingExpiration);
					}
					break;
				}
				case (int) Constants.eASSET_TYPE.GAME:
				{
					dtAssetCategories = (DataTable) cache [cGAME_CATEGORIES];
					if (dtAssetCategories == null)
					{
						dtAssetCategories = StoreUtility.GetAssetCategories (assetTypeId, "name ASC");
						cache.Insert (cGAME_CATEGORIES, dtAssetCategories, null, DateTime.Now.AddMinutes (cacheTime), System.Web.Caching.Cache.NoSlidingExpiration);
					}
					break;
				}
				case (int) Constants.eASSET_TYPE.VIDEO:
				{
					dtAssetCategories = (DataTable) cache [cMEDIA_CATEGORIES];
					if (dtAssetCategories == null)
					{
						dtAssetCategories = StoreUtility.GetAssetCategories (assetTypeId, "name ASC");
						cache.Insert (cMEDIA_CATEGORIES, dtAssetCategories, null, DateTime.Now.AddMinutes (cacheTime), System.Web.Caching.Cache.NoSlidingExpiration);
					}
					break;
				}
				case (int) Constants.eASSET_TYPE.MUSIC:
				{
					dtAssetCategories = (DataTable) cache [cMUSIC_CATEGORIES];
					if (dtAssetCategories == null)
					{
						dtAssetCategories = StoreUtility.GetAssetCategories (assetTypeId, "name ASC");
						cache.Insert (cMUSIC_CATEGORIES, dtAssetCategories, null, DateTime.Now.AddMinutes (cacheTime), System.Web.Caching.Cache.NoSlidingExpiration);
					}
					break;
				}
				case (int) Constants.eASSET_TYPE.PICTURE:
				{
					dtAssetCategories = (DataTable) cache [cPHOTO_CATEGORIES];
					if (dtAssetCategories == null)
					{
						dtAssetCategories = StoreUtility.GetAssetCategories (assetTypeId, "name ASC");
						cache.Insert (cPHOTO_CATEGORIES, dtAssetCategories, null, DateTime.Now.AddMinutes (cacheTime), System.Web.Caching.Cache.NoSlidingExpiration);
					}
					break;
				}
				case (int) Constants.eASSET_TYPE.PATTERN:
				{
					dtAssetCategories = (DataTable) cache [cPATTERN_CATEGORIES];
					if (dtAssetCategories == null)
					{
						dtAssetCategories = StoreUtility.GetAssetCategories (assetTypeId, "name ASC");
						cache.Insert (cPATTERN_CATEGORIES, dtAssetCategories, null, DateTime.Now.AddMinutes (cacheTime), System.Web.Caching.Cache.NoSlidingExpiration);
					}
					break;
				}
				case (int) Constants.eASSET_TYPE.TV:
				{
					dtAssetCategories = (DataTable) cache [cTV_CATEGORIES];
					if (dtAssetCategories == null)
					{
						dtAssetCategories = StoreUtility.GetAssetCategories (assetTypeId, "name ASC");
						cache.Insert (cTV_CATEGORIES, dtAssetCategories, null, DateTime.Now.AddMinutes (cacheTime), System.Web.Caching.Cache.NoSlidingExpiration);
					}
					break;
				}
				case (int) Constants.eASSET_TYPE.WIDGET:
				{
					dtAssetCategories = (DataTable) cache [cWIDGET_CATEGORIES];
					if (dtAssetCategories == null)
					{
						dtAssetCategories = StoreUtility.GetAssetCategories (assetTypeId, "name ASC");
                        cache.Insert(cWIDGET_CATEGORIES, dtAssetCategories, null, DateTime.Now.AddMinutes(cacheTime), System.Web.Caching.Cache.NoSlidingExpiration);
					}
					break;
				}
				default:
				{
					throw new Exception ("This media type (assetTypeId) is not supported in the cache");
				}
			}			


			return dtAssetCategories;
		}

		/// <summary>
		/// GetMediaPageOMMList
		/// </summary>
		public static DataTable GetMediaPageOMMList (int pageSize, string cacheKey)
		{
			PagedDataTable dtAssets = null;
			Cache cache = Global.Cache ();

			string cacheString = "MediaPageOMMList" + cacheKey;
			
			dtAssets = (PagedDataTable) cache [cacheString];
			if (dtAssets == null)
			{
				dtAssets = StoreUtility.GetCoolAssets( (int)Constants.eASSET_TYPE.ALL,
					" fa.playlist_type = " + (int) Constants.COOL_ITEM_PLAYLIST_ID_MEDIA + " AND a.playlistable = 'Y' ", 1, pageSize);

				cache.Insert (cacheString, dtAssets, null, DateTime.Now.AddMinutes (5), System.Web.Caching.Cache.NoSlidingExpiration);
			}

			return dtAssets;
		}
		
        ///// <summary>
        ///// GetNewPeople
        ///// </summary>
        //public static DataTable GetNewPeople (bool showMature)
        //{
        //    DataTable dtUsers = null;
        //    Cache cache = Global.Cache ();
        //    int pageSize = 4;
        //    string mature = (showMature) ? "M" : "";

        //    string cacheString = "newUsers" + mature + pageSize;
			
        //    dtUsers = (DataTable) cache [cacheString];
        //    if (dtUsers == null)
        //    {				
        //        dtUsers = UsersUtility.GetNewPeople (pageSize);
        //        cache.Insert (cacheString, dtUsers, null, DateTime.Now.AddMinutes (15), System.Web.Caching.Cache.NoSlidingExpiration);
        //    }

        //    return dtUsers;
        //}

		/// <summary>
		/// GetModuleTitle
		/// </summary>
		public static string GetModuleTitle ( int module_id )
		{
			DataTable dtModules = (DataTable) Global.Cache () [cMODULE_TITLES];
			if (dtModules == null)
			{
				// Add to the cache
                dtModules = GetLayoutModules();
				Global.Cache ().Insert (cMODULE_TITLES, dtModules, null, DateTime.Now.AddMinutes (15), System.Web.Caching.Cache.NoSlidingExpiration);
			}

			DataRow []dr = dtModules.Select("module_id = " + module_id);
			if ( dr.Length > 0 )
				return dr[0]["title"].ToString();
			else
				return "";
		}

        /// <summary>
        /// GetStatusName
        /// </summary>
        public static string GetStatusName(int status_id)
        {
            DataTable dtStatuses = (DataTable)Global.Cache()[cSTATUS_NAMES];
            if (dtStatuses == null)
            {
                // Add to the cache
                dtStatuses = UsersUtility.GetUserStatus();
                Global.Cache().Insert(cSTATUS_NAMES, dtStatuses, null, DateTime.Now.AddMinutes(15), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            DataRow[] dr = dtStatuses.Select("status_id = " + status_id);
            if (dr.Length > 0)
                return dr[0]["name"].ToString();
            else
                return "";
        }

		/// <summary>
		/// GetTotalAssets
		/// </summary>
		public static double GetTotalAssets ()
		{
			if ( Global.Cache () [cTOTAL_ASSETS] == null )
			{
				// add to cache
				DataRow drMediaCounts = StoreUtility.GetTotalMediaCounts ();
				double total_assets = Convert.ToDouble (drMediaCounts ["sum_total"]);
				Global.Cache ().Insert (cTOTAL_ASSETS, total_assets, null, DateTime.Now.AddMinutes (5), System.Web.Caching.Cache.NoSlidingExpiration);
			}

			return (double) Global.Cache () [cTOTAL_ASSETS];
		}

		/// <summary>
		/// GetTotalPersonalChannels
		/// </summary>
		public static UInt64 GetTotalPersonalChannels()
		{
			if ( Global.Cache () [cTOTAL_PERSONAL_CHANNELS] == null )
			{
				// add to cache
				UInt64 total_channels = UsersUtility.GetUsersCount ();
				Global.Cache ().Insert (cTOTAL_PERSONAL_CHANNELS, total_channels, null, DateTime.Now.AddMinutes (5), System.Web.Caching.Cache.NoSlidingExpiration);
			}

			return (UInt64) Global.Cache () [cTOTAL_PERSONAL_CHANNELS];
		}

		/// <summary>
		/// GetTotalBroadcastChannels
		/// </summary>
		public static double GetTotalBroadcastChannels ()
		{
			if ( Global.Cache () [cTOTAL_BROADCAST_CHANNELS] == null )
			{
				double total_channels = CommunityUtility.GetChannelCount (); 
				Global.Cache ().Insert (cTOTAL_BROADCAST_CHANNELS, total_channels, null, DateTime.Now.AddMinutes (5), System.Web.Caching.Cache.NoSlidingExpiration);
			}

			return (double) Global.Cache () [cTOTAL_BROADCAST_CHANNELS];
		}

		/// <summary>
		/// GetWOKHangoutCount
		/// </summary>
		public static double GetWOKHangoutCount ()
		{
			if ( Global.Cache () [cTOTAL_HANGOUTS] == null )
			{
				double total_channels = CommunityUtility.GetWOKHangoutCount (); 
				Global.Cache ().Insert (cTOTAL_HANGOUTS, total_channels, null, DateTime.Now.AddMinutes (15), System.Web.Caching.Cache.NoSlidingExpiration);
			}

			return (double) Global.Cache () [cTOTAL_HANGOUTS];
		}

		/// <summary>
		/// GetWOKPopulation
		/// </summary>
		public static double GetWOKPopulation ()
		{
			if ( Global.Cache () [cWOK_POPULATION] == null )
			{
				double total_users = ReportUtility.GetTotalWokUsers (DateTime.Now.AddDays (-1)); 
				Global.Cache ().Insert (cWOK_POPULATION, total_users, null, DateTime.Now.AddMinutes (15), System.Web.Caching.Cache.NoSlidingExpiration);
			}

			return (double) Global.Cache () [cWOK_POPULATION];
		}

		/// <summary>
		/// GetLeaderBoard_User2User
		/// </summary>
		public static PagedDataTable GetLeaderBoard_User2User (int NumberOfResults, int TimePeriodInDays)
		{
			PagedDataTable dtLbUser2User = null;
			Cache cache = Global.Cache ();

			string cacheString = "dtLbUser2User" + NumberOfResults + TimePeriodInDays;
			
			dtLbUser2User = (PagedDataTable) cache [cacheString];
			if (dtLbUser2User == null)
			{
				dtLbUser2User = StoreUtility.GetLeaderBoard_User2User (NumberOfResults, TimePeriodInDays, null, 1); 
				cache.Insert (cacheString, dtLbUser2User, null, DateTime.Now.AddMinutes (15), System.Web.Caching.Cache.NoSlidingExpiration);
			}

			return dtLbUser2User;
		}

        /// <summary>
        /// Used to clear the cache value for a specified key string
        /// </summary>
        public static int ClearCacheValue (string cacheKeyString)
        {
            Global.Cache ().Remove (cacheKeyString);

            if (Global.Cache () [cacheKeyString] == null)
            {
                return 0;
            }
            else
            {
                return -1;
            }
        }

        /// <summary>
        /// GetLeaderboardDay
        /// </summary>
        public static DataTable GetLeaderboardDay(string sortOrder, int numResults)
        {
            KachingFacade kachingFacade = new KachingFacade();
            DataTable dtLeaderboard = null;
            Cache cache = Global.Cache();

            string cacheString = "dtKgLbD" + numResults + "S" + sortOrder;

            dtLeaderboard = (DataTable)cache[cacheString];
            if (dtLeaderboard == null)
            {
                dtLeaderboard = kachingFacade.GetLeaderboardDay(sortOrder, numResults);
                cache.Insert(cacheString, dtLeaderboard, null, DateTime.Now.AddMinutes(60), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return dtLeaderboard;
        }

        public static DataTable GetLeaderboardWeek(string sortOrder, int numResults)
        {
              KachingFacade kachingFacade = new KachingFacade();
            DataTable dtLeaderboard = null;
            Cache cache = Global.Cache();

            string cacheString = "dtKgLbW" + numResults + "S" + sortOrder;

            dtLeaderboard = (DataTable)cache[cacheString];
            if (dtLeaderboard == null)
            {
                dtLeaderboard = kachingFacade.GetLeaderboardWeek(sortOrder, numResults);
                cache.Insert(cacheString, dtLeaderboard, null, DateTime.Now.AddMinutes(60), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return dtLeaderboard;
        }

        public static DataTable GetLeaderboardAllTime(string sortOrder, int numResults)
        {
            KachingFacade kachingFacade = new KachingFacade();
            DataTable dtLeaderboard = null;
            Cache cache = Global.Cache();

            string cacheString = "dtKgLbAT" + numResults + "S" + sortOrder;

            dtLeaderboard = (DataTable)cache[cacheString];
            if (dtLeaderboard == null)
            {
                dtLeaderboard = kachingFacade.GetLeaderboardAllTime(sortOrder, numResults);
                cache.Insert(cacheString, dtLeaderboard, null, DateTime.Now.AddMinutes(60), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return dtLeaderboard;
        }

        /// <summary>
        /// GetInviteDeclineReasons
        /// </summary>
        public static DataTable GetInviteDeclineReasons ()
        {
            DataTable dtDeclineReasons = (DataTable) Global.Cache ()[cDECLINE_REASONS];
            if (dtDeclineReasons == null)
            {
                // Add the reasons list to the cache
                dtDeclineReasons = UsersUtility.GetInviteDeclineReasons ();
                Global.Cache ().Insert (cDECLINE_REASONS, dtDeclineReasons, null, DateTime.Now.AddMinutes (30), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return dtDeclineReasons;
        }

        /// <summary>
        /// SearchAssetsPerformance
        /// </summary>
        public static PagedDataTable SearchAssetsPerformance(bool bGetMature, int assetTypeId, bool bThumbnailRequired,
            string categories, int pastDays, string country, string filter, string orderBy, int pageNumber, int pageSize, bool onlyAccessPass)
        {
            PagedDataTable pdtSearchResults = null;
            Cache cache = Global.Cache();

            string cacheString = "pdtESs" + bGetMature + "-" + assetTypeId + "-" + bThumbnailRequired + "-" +
                categories + "-" + pastDays + "-" + country + "-" + filter + "-" + orderBy + "-" + pageNumber + "-" + pageSize + "-" + onlyAccessPass;

            pdtSearchResults = (PagedDataTable)cache[cacheString];
            if (pdtSearchResults == null)
            {
                pdtSearchResults = StoreUtility.SearchAssetsPerformance(bGetMature, assetTypeId, bThumbnailRequired,
                    categories, pastDays, country, filter, orderBy, pageNumber, pageSize, onlyAccessPass);
                cache.Insert(cacheString, pdtSearchResults, null, DateTime.Now.AddMinutes(5), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return pdtSearchResults;
        }

        /// <summary>
        /// GetAllRaveTypes
        /// </summary>
        public static IList<RaveType> GetAllRaveTypes ()
        {
            string cacheItemsName = "raveTypes";

            // Load countries
            IList<RaveType> dtRaveTypes = (IList<RaveType>) Global.Cache ()[cacheItemsName];

            if (dtRaveTypes == null)
            {
                // Get and add to cache
                RaveFacade raveFacade = new RaveFacade ();
                dtRaveTypes = raveFacade.GetRaveTypes ();
                Global.Cache ().Insert (cacheItemsName, dtRaveTypes, null, DateTime.Now.AddMinutes (30), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return dtRaveTypes;
        }

        /// <summary>
        /// GetFont
        /// </summary>
        public static DataRow GetFont(int fontId)
        {
            DataRow drFont = (DataRow)Global.Cache()[cFONT + fontId];
            if (drFont == null)
            {
                // Add the fonts to the cache
                drFont = WidgetUtility.GetFontNonCached(fontId);
                Global.Cache().Insert(cFONT + fontId, drFont, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return drFont;
        }


        /// <summary>
        /// GetFonts
        /// </summary>
        public static DataTable GetFonts()
        {
            DataTable dtFonts = (DataTable)Global.Cache()[cFONTS];
            if (dtFonts == null)
            {
                // Add the fonts to the cache
                dtFonts = WidgetUtility.GetFontsNonCached();
                Global.Cache().Insert(cFONTS, dtFonts, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return dtFonts;
        }

        /// <summary>
        /// GetLayoutModuleGroups
        /// </summary>
        /// <param name="visibleFlag"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        public static DataTable GetLayoutModuleGroups (int visibleFlag, string filter)
        {
            DataTable dtLayoutModuleGroups = (DataTable)Global.Cache()[cLAYOUT_MODULE_GROUPS + visibleFlag.ToString() + filter.ToString()];
            if (dtLayoutModuleGroups == null)
            {
                // Add the fonts to the cache
                dtLayoutModuleGroups = PageUtility.GetLayoutModuleGroupsNonCached(visibleFlag, filter);
                Global.Cache().Insert(cLAYOUT_MODULE_GROUPS + visibleFlag.ToString() + filter.ToString(), dtLayoutModuleGroups, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return dtLayoutModuleGroups;
        }

        /// <summary>
        /// GetLayoutModules
        /// </summary>
        /// <returns></returns>
        public static DataTable GetLayoutModules ()
        {
            DataTable dtLayoutModules = (DataTable)Global.Cache()[cLAYOUT_MODULES];
            if (dtLayoutModules == null)
            {
                // Add the fonts to the cache
                dtLayoutModules = PageUtility.GetLayoutModulesNonCached();
                Global.Cache().Insert(cLAYOUT_MODULES, dtLayoutModules, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return dtLayoutModules;
        }

        /// <summary>
        /// GetLayoutModules
        /// </summary>
        public static DataTable GetLayoutModules(int module_group_id, int visibleFlag)
        {
            DataTable dtLayoutModules = (DataTable)Global.Cache()[cLAYOUT_MODULES + module_group_id.ToString () + visibleFlag.ToString()];
            if (dtLayoutModules == null)
            {
                // Add the fonts to the cache
                dtLayoutModules = PageUtility.GetLayoutModulesNonCached(module_group_id, visibleFlag);
                Global.Cache().Insert(cLAYOUT_MODULES + module_group_id.ToString() + visibleFlag.ToString(), dtLayoutModules, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return dtLayoutModules;
        }

        /// <summary>
        /// GetLayoutPageAccesses
        /// </summary>
        /// <returns></returns>
        public static DataTable GetLayoutPageAccesses ()
        {
            DataTable dtLayoutPageAccesses = (DataTable)Global.Cache()[cLAYOUT_PAGE_ACCESS];
            if (dtLayoutPageAccesses == null)
            {
                // Add the fonts to the cache
                dtLayoutPageAccesses = PageUtility.GetLayoutPageAccessesNonCached();
                Global.Cache().Insert(cLAYOUT_PAGE_ACCESS, dtLayoutPageAccesses, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return dtLayoutPageAccesses;
        }

        /// <summary>
        /// GetLayoutZones
        /// </summary>
        /// <returns></returns>
        public static DataTable GetLayoutZones()
        {
            DataTable dtLayoutZones = (DataTable)Global.Cache()[cLAYOUT_ZONES];
            if (dtLayoutZones == null)
            {
                // Add the fonts to the cache
                dtLayoutZones = PageUtility.GetLayoutZonesNonCached();
                Global.Cache().Insert(cLAYOUT_ZONES, dtLayoutZones, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return dtLayoutZones;
        }

        /// <summary>
        /// GetBgRepeatModes
        /// </summary>
        /// <returns></returns>
        public static DataTable GetBgRepeatModes()
        {
            DataTable dtBbRepeatModes = (DataTable)Global.Cache()[cLAYOUT_BACKGROUND_REPEAT_MODES];
            if (dtBbRepeatModes == null)
            {
                // Add the fonts to the cache
                dtBbRepeatModes = WidgetUtility.GetBgRepeatModesNonCached();
                Global.Cache().Insert(cLAYOUT_BACKGROUND_REPEAT_MODES, dtBbRepeatModes, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return dtBbRepeatModes;
        }

        /// <summary>
        /// GetBgRepeatMode
        /// </summary>
        /// <param name="modeId"></param>
        /// <returns></returns>
        public static DataRow GetBgRepeatMode(int modeId)
        {
            DataRow drBgRepeatMode = (DataRow)Global.Cache()[cLAYOUT_BACKGROUND_REPEAT_MODES + modeId];
            if (drBgRepeatMode == null)
            {
                // Add the fonts to the cache
                drBgRepeatMode = WidgetUtility.GetBgRepeatModeNonCached(modeId);
                Global.Cache().Insert(cLAYOUT_BACKGROUND_REPEAT_MODES + modeId, drBgRepeatMode, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return drBgRepeatMode;
        }
        

        /// <summary>
        /// GetRaveType
        /// </summary>
        public static RaveType GetRaveType (RaveType.eRAVE_TYPE eRT)
        {
            string cacheItemsName = "raveType" + eRT.ToString();

            // Load countries
            RaveType raveType = (RaveType) Global.Cache ()[cacheItemsName];

            if (raveType == null)
            {
                // Get and add to cache
                RaveFacade raveFacade = new RaveFacade ();
                raveType = raveFacade.GetRaveType (Convert.ToUInt32 (eRT));
                Global.Cache ().Insert (cacheItemsName, raveType, null, DateTime.Now.AddMinutes (30), System.Web.Caching.Cache.NoSlidingExpiration);
            }

            return raveType;
        }

        private const string cKACHING_LEADER = "commCategories";

		private const string cCOMMUNITY_CATEGORIES = "commCategories";

		private const string cMEDIA_CATEGORIES = "medCats";
		private const string cASSET_CATEGORIES = "assCats";
		private const string cGAME_CATEGORIES = "gamCats";
		private const string cMUSIC_CATEGORIES = "musicCats";
		private const string cPHOTO_CATEGORIES = "photoCats";
		private const string cPATTERN_CATEGORIES = "patternCats";
		private const string cTV_CATEGORIES = "tvCats";
		private const string cWIDGET_CATEGORIES = "widgetCats";

		private const string cALL_SUB_CATEGORIES = "allSubCats";

		// Featured item cache constants
		public const string FEATURED_ITEMS = "featItems";
		public const string FEATURED_ITEMS_WATCH = "fiW";
		public const string FEATURED_ITEMS_PLAY = "fiP";
		public const string FEATURED_ITEMS_CREATE = "fiC";

        private const string cSMUT = "POTTY_MOUTH";

        public const string cRESTWORDS = "RESTRICTED_WORDS";    // Used for DataTable of restricted words
        public const string cPMEXACT = "POTTY_MOUTH_EXACT";     // Used for Potty Mouth Exact Match word list
        public const string cPMANY = "POTTY_MOUTH_ANYWHERE";    // Used for Potty Mouth Anywhere Match word list
        public const string cRESTEXACT = "RESERVED_EXACT";      // Used for Reserved Exact Match word list
        public const string cRESTANY = "RESERVED_ANYWHERE";     // Used for Reserved Anywhere Match word list

		private const string cMODULE_TITLES = "MODULE_TITLES";
        private const string cSTATUS_NAMES = "STATUS_NAMES";

		private const string cTOTAL_ASSETS = "TOTAL_ASSETS";
		private const string cTOTAL_PERSONAL_CHANNELS = "TOTAL_PERSONAL_CHANNELS";
		private const string cTOTAL_BROADCAST_CHANNELS = "TOTAL_BROADCAST_CHANNELS";
		private const string cTOTAL_HANGOUTS = "TOTAL_HANGOUTS";
		private const string cWOK_POPULATION = "WOK_POPULATION";

        private const string cDECLINE_REASONS = "INVITE_DECLINE_REASONS";

        private const string cFONTS = "kaneva.layout_fonts";
        private const string cFONT = "kaneva.layout_template_font";
        private const string cLAYOUT_MODULE_GROUPS = "kaneva.layout_module_groups";
        private const string cLAYOUT_MODULES = "kaneva.layout_modules";
        private const string cLAYOUT_PAGE_ACCESS = "kaneva.layout_page_access";
        private const string cLAYOUT_ZONES = "kaneva.layout_zones";
        private const string cLAYOUT_BACKGROUND_REPEAT_MODES = "kaneva.background_repeat_modes";
	}
}
