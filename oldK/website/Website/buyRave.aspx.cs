///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
    public partial class buyRave : NoBorderPage
    {
        private uint _currentRaveType = 0;

        // G-POINT --> Rewards
        // K-POINT --> Credits

        protected void Page_Load (object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // Get the item Id
                if (Request["assetId"] != null)
                {
                    _AssetId = Convert.ToInt32 (Request["assetId"]);
                }

                if (Request["communityId"] != null)
                {
                    _CommunityId = Convert.ToInt32 (Request["communityId"]);
                }

                if (Request["globalId"] != null)
                {
                    _GlobalId = Convert.ToInt32 (Request["globalId"]);
                }

                _RaveProfileName = (Request["raveProfileName"] ?? "").ToString();

                // Get the type of rave
                if (Request["type"] != null && Request["type"].ToString () == "mega")
                {
                    _RaveType = new RaveFacade ().GetRaveType (Convert.ToUInt32 (RaveType.eRAVE_TYPE.MEGA));
                    _IsMegaRave = true;
                }
                else
                {
                    _RaveType = new RaveFacade ().GetRaveType (Convert.ToUInt32 (RaveType.eRAVE_TYPE.SINGLE));
                    _IsMegaRave = false;
                }

                // Does user have Always Purchase with set?
                if (KanevaWebGlobals.CurrentUser.Preference.AlwaysPurchaseWithCurrencyType == Constants.CURR_REWARDS ||
                    KanevaWebGlobals.CurrentUser.Preference.AlwaysPurchaseWithCurrencyType == Constants.CURR_CREDITS
                    )
                {
                    BuyRave (false, _IsMegaRave, KanevaWebGlobals.CurrentUser.Preference.AlwaysPurchaseWithCurrencyType);
                }
                else
                {
                    if (!CanUserAffordRave (Constants.CURR_REWARDS, true))
                    {
                        //ShowPage (PageType.BuyMoreCredits);
                        DisplayBuyCreditsPage (false);
                    }
                    else
                    {
                        if (_IsMegaRave)
                        {
                            ShowPage (PageType.BuyMegaRave);
                        }
                        else
                        {
                            ShowPage (PageType.BuyRave);
                        }
                    }
                }

                aSettings.HRef = "http://" + KanevaGlobals.SiteName + "/myKaneva/settings.aspx";
                litJS.Text = "<script type=\"text/javascript\" src=\"" + ResolveUrl ("~/jscript/jquery/jquery-1.8.2.min.js") + "\">";
            }
        }


        #region Helper Methods

        private bool CanUserAffordRave (string currency)
        {
            return CanUserAffordRave (currency, false);
        }
        private bool CanUserAffordRave (string currency, bool checkRewAndCred)
        {
            UserBalances ub = new UserFacade ().GetUserBalances (KanevaWebGlobals.CurrentUser.UserId);
            if (Constants.CURR_REWARDS == currency) // Rewards
            {
                if (Convert.ToUInt32 (RaveType.eRAVE_TYPE.SINGLE) == _RaveType.RaveTypeId)  // Single Rave
                {
                    if (ub.Rewards < _RaveType.PriceSingleRave)
                    {   // Not enough Rewards to buy Rave
                        _HasEnoughRewards = false;
                        if (checkRewAndCred)
                        {   // See if user has enough Credits to buy Rave
                            if (ub.Credits >= _RaveType.PriceSingleRave)
                            {
                                _HasEnoughCredits = true;
                                return true;
                            }
                            _HasEnoughCredits = false;
                        }
                        return false;
                    }
                    _HasEnoughRewards = true;
                    return true;
                }
                else // MegaRave
                {
                    if (ub.Rewards < _RaveType.PriceMegaRave)
                    {   // Not enough Rewards to buy Mega Rave
                        _HasEnoughRewards = false;
                        if (checkRewAndCred)
                        {   // See if user has enough Credits to buy Mega Rave
                            if (ub.Credits >= _RaveType.PriceMegaRave)
                            {
                                _HasEnoughCredits = true;
                                return true;
                            }
                            _HasEnoughCredits = false;
                        }
                        return false;
                    }
                    _HasEnoughRewards = true;
                    return true;
                }
            }
            else if (Constants.CURR_CREDITS == currency) // Credits
            {
                if (Convert.ToUInt32 (RaveType.eRAVE_TYPE.SINGLE) == _RaveType.RaveTypeId)  // Single Rave
                {
                    if (ub.Credits < _RaveType.PriceSingleRave)
                    {   // Not enough Credits to buy Rave
                        _HasEnoughCredits = false;
                        if (checkRewAndCred)
                        {   // See if user has enough Rewards to buy Rave
                            if (ub.Rewards >= _RaveType.PriceSingleRave)
                            {
                                _HasEnoughRewards = true;
                                return true;
                            }
                            _HasEnoughRewards = false;
                        }
                        return false;
                    }
                    _HasEnoughCredits = true;
                    return true;
                }
                else // MegaRave
                {
                    if (ub.Credits < _RaveType.PriceMegaRave)
                    {   // Not enough Credits to buy Mega Rave
                        _HasEnoughCredits = false;
                        if (checkRewAndCred)
                        {   // See if user has enough Rewards to buy Mega Rave
                            if (ub.Rewards >= _RaveType.PriceMegaRave)
                            {
                                _HasEnoughRewards = true;
                                return true;
                            }
                            _HasEnoughRewards = false;
                        }
                        return false;
                    }
                    _HasEnoughCredits = true;
                    return true;
                }
            }

            return false;
        }

        private void DisplayBuyRavePage (bool isMegaRave)
        {
            if (isMegaRave)
            {
                spnMegaRaveValue.InnerText = _RaveType.MegaRaveValue.ToString ();
                
                divBuyMegaRave.Style.Add ("display", "block");                    
            }
            else
            {
                divBuyRave.Style.Add ("display", "block");
            }

            SetButtonState ();
        }
        private void DisplaySuccessPage ()
        {
            divSuccess.Style.Add ("display", "block");

            try
            {
                if (IsPostBack)
                {
                    ScriptManager.RegisterStartupScript (this, GetType (), "SetHeight", _JSVariables + "SetHeight(" + FaceboxHeight.DEFAULT + ");", true);
                }
            }
            catch { }
        }

        private void DisplayBuyCreditsPage ()
        {
            DisplayBuyCreditsPage (true);
        }
        private void DisplayBuyCreditsPage (bool setHeight)
        {
            divBuyCredits.Style.Add ("display", "block");

            try
            {
                if (IsPostBack)
                {
                    if (setHeight) { 
                        ScriptManager.RegisterStartupScript (this, GetType (), "SetHeight1", "SetHeight(" + FaceboxHeight.DEFAULT + ");", true);
                    }
                }
            }
            catch { }
        }

        private void DisplayUseOtherCurrencyPage (string newCurrency, bool canUserBuyWithNewCurrency)
        {
            // Not enough Rewards to purchase Rave
            if (newCurrency == Constants.CURR_CREDITS)
            {
                spnCurrentCurrency.InnerText = "Rewards";
                spnNewCurrency.InnerText = "Credits";
                btnBuyWith_1.Text = "Buy with Credits";
                btnBuyWith_1.CommandName = "purchase";
                btnBuyWith_1.CommandArgument = Constants.CURR_CREDITS;

                if (canUserBuyWithNewCurrency)
                {
                    btnBuyWith_2.Text = "Learn how to earn Rewards";
                    btnBuyWith_2.CommandName = "navigate";
                    btnBuyWith_2.CommandArgument = "learnhow";
                }
                else
                {
                    btnBuyWith_2.Style.Add ("display", "none");
                }
            }
            else  // Not enough Credits to purchase Rave
            {
                spnCurrentCurrency.InnerText = "Credits";
                spnNewCurrency.InnerText = "Rewards";

                if (canUserBuyWithNewCurrency)
                {
                    btnBuyWith_1.Text = "Buy with Rewards";
                    btnBuyWith_1.CommandName = "purchase";
                    btnBuyWith_1.CommandArgument = Constants.CURR_REWARDS;
                }
                else
                {
                    btnBuyWith_1.Style.Add ("display", "none");
                }

                btnBuyWith_2.Text = "Buy Credits";
                btnBuyWith_2.CommandName = "navigate";
                btnBuyWith_2.CommandArgument = "buycredits";
            }

            divUseOtherCurrency.Style.Add ("display", "block");
            try
            {
                if (IsPostBack)
                {
                    ScriptManager.RegisterStartupScript (this, GetType (), "SetHeight1", "SetHeight(" + FaceboxHeight.DEFAULT + ");", true);
                }
            }
            catch { }
            btnBuyWith_1.Focus ();
        }

        private void ShowPage (PageType displayPage)
        {
            ShowPage (displayPage, string.Empty, false);
        }
        private void ShowPage (PageType displayPage, string newCurrency, bool canUserBuyWithNewCurrency)
        {
            HideAllPages ();

            switch (displayPage)
            {
                case PageType.BuyMoreCredits:
                    DisplayBuyCreditsPage ();
                    return;
                case PageType.BuyRave:
                    DisplayBuyRavePage (false);
                    return;
                case PageType.BuyMegaRave:
                    DisplayBuyRavePage (true);
                    return;
                case PageType.PurchaseWithOtherCurrency:
                    if ((newCurrency == Constants.CURR_CREDITS && !canUserBuyWithNewCurrency) ||
                        (newCurrency == Constants.CURR_REWARDS && !canUserBuyWithNewCurrency))
                    {
                        DisplayBuyCreditsPage ();
                    }
                    else
                    {
                        DisplayUseOtherCurrencyPage (newCurrency, canUserBuyWithNewCurrency);
                    }
                    return;
                case PageType.Success:
                    DisplaySuccessPage ();
                    return;
            }
        }

        private void HideAllPages ()
        {
            divBuyCredits.Style.Add ("display", "none");
            divBuyRave.Style.Add ("display", "none");
            divBuyMegaRave.Style.Add ("display", "none");
            divSuccess.Style.Add ("display", "none");
            divUseOtherCurrency.Style.Add ("display", "none");
        }

        protected void BuyRaveWithAlwaysPurchase_Click (object sender, CommandEventArgs e)
        {
            BuyRave (true, false, (cbRewards.Checked ? Constants.CURR_REWARDS : Constants.CURR_CREDITS));
        }

        protected void BuyRave (bool isAlwaysPurchaseWith, bool isMegaRave, string currency)
        {
            // G-POINT --> Rewards
            // K-POINT --> Credits

            // Confirm user has enough rewards/credits to purchase rave
            UserBalances ub = new UserFacade ().GetUserBalances (KanevaWebGlobals.CurrentUser.UserId);

            // Get the details for the rave type
            List<RaveType> raveTypes = (List<RaveType>) WebCache.GetAllRaveTypes ();

            // Set the rave type
            if (isMegaRave)
            {
                _currentRaveType = Convert.ToUInt32 (RaveType.eRAVE_TYPE.MEGA);
            }
            else
            {
                _currentRaveType = Convert.ToUInt32 (RaveType.eRAVE_TYPE.SINGLE);
            }


            // Save always purchase with selection.  Need to put this here so it will save the selection
            // even if the user does not have enough Rewards/Credits currently to purchase the rave
            if (isAlwaysPurchaseWith)
            {
                // Set the users preferences 
                Preferences preferences = new Preferences (
                    KanevaWebGlobals.CurrentUser.UserId,
                    currency, KanevaWebGlobals.CurrentUser.Preference.FilterByCountry
                    );

                new UserFacade ().UpdateUserPreferences (preferences);
            }


            if (currency == Constants.CURR_CREDITS)   // Credits
            {
                if (!CanUserAffordRave (Constants.CURR_CREDITS))
                {
                    // Display message about using G-POINTS (Rewards)
                    ShowPage (PageType.PurchaseWithOtherCurrency, Constants.CURR_REWARDS, CanUserAffordRave (Constants.CURR_REWARDS));
                    return;
                }
            }
            else  // Rewards
            {
                if (!CanUserAffordRave (Constants.CURR_REWARDS))
                {
                    // Display message about using G-POINTS (Rewards)
                    ShowPage (PageType.PurchaseWithOtherCurrency, Constants.CURR_CREDITS, CanUserAffordRave (Constants.CURR_CREDITS));
                    return;
                }
            }

            RaveFacade raveFacade = new RaveFacade ();

            if (_IsAsset)
            {
                // User has enough Rewards/Credits.  Purchase the rave
                raveFacade.RaveAsset (KanevaWebGlobals.CurrentUser.UserId, Convert.ToInt32 (Request["assetId"]),
                    GetEnumRaveType (), currency);
            }
            else
            {
                if (_CommunityId > 0)
                {
                    // Rave the community or profile
                    int raveSuccess = raveFacade.RaveCommunity (KanevaWebGlobals.CurrentUser.UserId, _CommunityId, _RaveType, currency);

                    if (raveSuccess == 0 && !string.IsNullOrWhiteSpace(_RaveProfileName))
                    {
                        Dictionary<string, string> metricData = new Dictionary<string, string> 
                        {
                            {"username", KanevaWebGlobals.CurrentUser.Username},
                            {"source", Page.Request.CurrentExecutionFilePath},
                            {"target", _RaveProfileName}
                        };
                        (new MetricsFacade()).InsertGenericMetricsLog("player_rave_log", metricData, Global.RequestRabbitMQChannel());
                    }
                }
                else // UGC Rave
                {
                    // Rave the UGC item
                    raveFacade.RaveUGCItem (KanevaWebGlobals.CurrentUser.UserId, _GlobalId, GetEnumRaveType (), currency);
                }
            }

            CreateParentUpdateJavascript (KanevaWebGlobals.CurrentUser.UserId, _CommunityId, _AssetId, _GlobalId);

            // Show success page
            ShowPage (PageType.Success);
        }

        private void CreateParentUpdateJavascript (int userId, int communityId, int assetId, int globalId)
        {
            UserBalances ub = new UserFacade ().GetUserBalances (userId);
            string raves = "0";

            if (communityId > 0)
            {
                raves = (new CommunityFacade ().GetCommunity (communityId).Stats.NumberOfDiggs).ToString ("#,###");
            }
            else if (assetId > 0) // asset
            {
                raves = (new MediaFacade ().GetAsset (assetId).Stats.NumberOfDiggs).ToString ("#,###");
            }
            else // UGC
            {
                raves = (new ShoppingFacade ().GetItem (globalId).NumberOfRaves).ToString ("#,###");
            }

            string credits = ub.Credits > 0 ? ub.Credits.ToString ("#,###") : "0";
            string rewards = ub.Rewards > 0 ? ub.Rewards.ToString ("#,###") : "0";

            aClose.HRef = "javascript:amtHasChanged=true;Update('" + raves + "','" +
                credits + "','" + rewards + "');";

            // Set JS values used if facebox closed with Close (X) button
            _JSVariables = "amtHasChanged=true;totRaves='" + raves + "';totCredits='" + credits +
                "';totRewards='" + rewards + "';";

            litCloseBtnClientId.Text = aClose.ClientID;

            aClose.Attributes.Add ("onfocus", _JSVariables);
        }

        private RaveType.eRAVE_TYPE GetEnumRaveType ()
        {
            if (_RaveType.RaveTypeId == Convert.ToUInt32 (RaveType.eRAVE_TYPE.MEGA))
            {
                return RaveType.eRAVE_TYPE.MEGA;
            }
            else
            {
                return RaveType.eRAVE_TYPE.SINGLE;
            }
        }

        //delegate function used in LIst FindAll search
        private bool IsRaveType (RaveType raveType)
        {
            return raveType.RaveTypeId == _currentRaveType;
        }

        private void SetButtonState ()
        {
            // Set the button states
            if (_HasEnoughCredits)
            {
                btnCredits.Enabled = true;
                spnCreditsBtn.Attributes.Add ("class", "payment_btn");
                cbCredits.Enabled = true;
            }
            else
            {
                btnCredits.Enabled = false;
                spnCreditsBtn.Attributes.Add ("class", "payment_btn_disable");
                cbCredits.Enabled = false;
            }

            if (_HasEnoughRewards)
            {
                btnRewards.Enabled = true;
                spnRewardsBtn.Attributes.Add ("class", "payment_btn");
                cbRewards.Enabled = true;
            }
            else
            {
                btnRewards.Enabled = false;
                spnRewardsBtn.Attributes.Add ("class", "payment_btn_disable");
                cbRewards.Enabled = false;
            }
        }

        #endregion Helper Methods


        #region Event Handlers

        protected void Buy_Click (object sender, CommandEventArgs e)
        {
            BuyRave (false, _IsMegaRave, (e.CommandArgument.ToString ().Equals ("rewards") ? Constants.CURR_REWARDS : Constants.CURR_CREDITS));
        }

        protected void btnBuyWith_Click (object sender, CommandEventArgs e)
        {
            string cmdArg = e.CommandArgument.ToString ();
            string cmdName = e.CommandName.ToString ();

            if (cmdName.Equals ("navigate"))
            {
                if (cmdArg.Equals ("buycredits"))
                {
                    ScriptManager.RegisterStartupScript (this, GetType (), "Close", "javascript:parent.$j.facebox.close();javascript:parent.location='http://" + KanevaGlobals.SiteName + "//mykaneva/buycredits.aspx';", true);
                }
                else if (cmdArg.Equals ("learnhow"))
                {
                    ScriptManager.RegisterStartupScript (this, GetType (), "Close", "javascript:parent.$j.facebox.close();javascript:parent.location='http://" + KanevaGlobals.SiteName + "//3d-virtual-world/fame-how-to.kaneva';", true);
                }
            }
            else if (cmdName.Equals ("purchase"))
            {
                BuyRave (false, _IsMegaRave, cmdArg);
            }
        }

        protected void btnBuyXtraCredits_Click (object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript (this, GetType (), "Close", "javascript:parent.$j.facebox.close();javascript:parent.location='http://" + KanevaGlobals.SiteName + "//mykaneva/buycredits.aspx';", true);
        }

        // Event fired when one of the always pay with check boxes are selected
        protected void AlwaysPayWith (object sender, EventArgs e)
        {
            bool isChecked = false;

            if (((CheckBox) sender) == cbCredits)
            {
                if (cbCredits.Checked)
                {
                    cbRewards.Enabled = false;
                    isChecked = true;

                    if (btnRewards.Enabled)
                    {
                        cbRewards.Enabled = false;
                    }
                }
            }
            else if (((CheckBox) sender) == cbRewards)
            {
                if (cbRewards.Checked)
                {
                    cbCredits.Enabled = false;
                    isChecked = true;

                    if (btnCredits.Enabled)
                    {
                        cbCredits.Enabled = false;
                    }
                }
            }


            if (isChecked)
            {
                btnCredits.Enabled = false;
                btnRewards.Enabled = false;
                spnCreditsBtn.Attributes.Add ("class", "payment_btn_disable");
                spnRewardsBtn.Attributes.Add ("class", "payment_btn_disable");
                divXtraTxt.Style.Add ("display", "block");
                ScriptManager.RegisterStartupScript (this, GetType (), "SetHeight", "SetHeight(" + FaceboxHeight.ALWAYS_BUY_WITH + ");", true);
            }
            else
            {
                SetButtonState ();
                
                divXtraTxt.Style.Add ("display", "none");
                ScriptManager.RegisterStartupScript (this, GetType (), "SetHeight", "SetHeight(" + FaceboxHeight.DEFAULT + ");", true);
            }
        }

        protected void btnCancel_Click (object sender, EventArgs e)
        {
            cbCredits.Checked = false;
            cbRewards.Checked = false;

            SetButtonState ();

            divXtraTxt.Style.Add ("display", "none");
            ScriptManager.RegisterStartupScript (this, GetType (), "SetHeight", "SetHeight(" + FaceboxHeight.DEFAULT + ");", true);
        }

        #endregion Event Handlers


        #region Properties

        private string _JSVariables = "";

        private bool _IsAsset
        {
            get
            {
                if (ViewState["isAsset"] != null)
                {
                    return Convert.ToBoolean (ViewState["isAsset"]);
                }
                else
                {
                    return false;
                }
            }
            set
            {
                ViewState["isAsset"] = value;
            }
        }
        private bool _IsMegaRave
        {
            get
            {
                if (ViewState["isMegaRave"] != null)
                {
                    return Convert.ToBoolean (ViewState["isMegaRave"]);
                }
                else
                {
                    return false;
                }
            }
            set
            {
                ViewState["isMegaRave"] = value;
            }
        }
        private bool _HasEnoughCredits
        {
            get
            {
                if (ViewState["enoughCredits"] != null)
                {
                    return Convert.ToBoolean (ViewState["enoughCredits"]);
                }
                else
                {
                    bool canAfford = CanUserAffordRave (Constants.CURR_CREDITS);
                    ViewState["enoughCredits"] = canAfford;
                    return canAfford;
                }
            }
            set
            {
                ViewState["enoughCredits"] = value;
            }
        }
        private bool _HasEnoughRewards
        {
            get
            {
                if (ViewState["enoughRewards"] != null)
                {
                    return Convert.ToBoolean (ViewState["enoughRewards"]);
                }
                else
                {
                    bool canAfford = CanUserAffordRave (Constants.CURR_REWARDS);
                    ViewState["enoughRewards"] = canAfford;
                    return canAfford;
                }
            }
            set
            {
                ViewState["enoughRewards"] = value;
            }
        }
        private int _AssetId
        {
            get
            {
                if (ViewState["raveAId"] != null)
                {
                    return Convert.ToInt32 (ViewState["raveAId"]);
                }
                else
                {
                    return -1;
                }
            }
            set
            {
                ViewState["raveAId"] = value;
                if (value > 0)
                {
                    _IsAsset = true;
                }
            }
        }
        private int _CommunityId
        {
            get
            {
                if (ViewState["raveCId"] != null)
                {
                    return Convert.ToInt32 (ViewState["raveCId"]);
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                ViewState["raveCId"] = value;
            }
        }
        private int _GlobalId
        {
            get
            {
                if (ViewState["raveGId"] != null)
                {
                    return Convert.ToInt32 (ViewState["raveGId"]);
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                ViewState["raveGId"] = value;
            }
        }
        private RaveType _RaveType
        {
            get
            {
                if (ViewState["RaveType"] != null)
                {
                    return (RaveType) ViewState["RaveType"];
                }
                else
                {
                    return null;
                }
            }
            set
            {
                ViewState["RaveType"] = value;
            }
        }
        private string _RaveProfileName
        {
            get
            {
                if (ViewState["RaveProfileName"] != null)
                {
                    return ViewState["RaveProfileName"].ToString();
                }
                else
                {
                    return null;
                }
            }
            set
            {
                ViewState["RaveProfileName"] = value;
            }
        }
        private enum PageType
        {
            BuyRave,
            BuyMegaRave,
            PurchaseWithOtherCurrency,
            BuyMoreCredits,
            Success
        }

        private class FaceboxHeight
        {
            public const int DEFAULT = 200;
            public const int ALWAYS_BUY_WITH = 345;
        }

        #endregion Properties
    }
}
