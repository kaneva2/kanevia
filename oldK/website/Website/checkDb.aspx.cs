///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for checkDb.
	/// </summary>
	public class checkDb : System.Web.UI.Page
	{
		private void Page_Load (object sender, System.EventArgs e)
		{
			StringBuilder output = new StringBuilder ();
			DataTable dtUsers = UsersUtility.CheckDb ();

			if (dtUsers != null)
			{
				for (int i = 0; i < dtUsers.Rows.Count; i++)
				{
					output.Append (dtUsers.Rows [i]["username"].ToString () + "<BR>");
				}
			}
			else
			{
				output.Append ("No results from db");
			}

			litUsernames.Text = output.ToString ();
		}

		protected Literal litUsernames;

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
