<%@ Page language="c#" MasterPageFile="~/masterpages/GenericPageTemplate.Master" EnableViewState="True" Codebehind="defaultB.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.defaultHomeB" %>
<%@ Register TagPrefix="Kaneva" TagName="MKContainer" Src="usercontrols/doodad/ContainerMyKaneva.ascx" %>
<%@ Register TagPrefix="UserControl" TagName="ContestWidget" Src="usercontrols/ContestWidget.ascx" %>
<%@ Register TagPrefix="UserControl" TagName="FacebookJS" Src="usercontrols/FacebookJavascriptInclude.ascx" %>

<asp:Content ID="cntProfileHead" runat="server" contentplaceholderid="cph_HeadData" >
    
	<script type="text/javascript" src="jscript/base/base.js"></script>
    <NOSCRIPT><META HTTP-EQUIV="refresh" CONTENT="1" URL="noJavascript.aspx" /></NOSCRIPT>
    <link href="css/myKaneva_NEW.css?v1" type="text/css" rel="stylesheet" />
	<usercontrol:FacebookJS runat="server"></usercontrol:FacebookJS>

</asp:Content>

<asp:Content id="cntBody" runat="server" contentplaceholderid="cph_Body" >
<div id="fb-root"></div>

<!-- this is needed to overrided this setting in topnav.css -->
<style type="text/css">#ddmenu div{top:62px;}</style>

<div id="stage" style="text-align:left;">

	<!-- Left Column -->
	<div id="leftRail">
		
		<kaneva:mkcontainer id="MKcontainerProfile" runat="server" ControlToLoad="usercontrols/doodad/UserProfile.ascx" />
		<kaneva:mkcontainer id="MKcontainerMessageSummary" runat="server" ControlToLoad="usercontrols/doodad/MessageSummary.ascx" />
		<kaneva:mkcontainer id="MKcontainerNotifications" runat="server" ControlToLoad="usercontrols/doodad/Notifications.ascx" />

	</div>
											 
	<!-- Center Column -->
	<div id="centerRail">
		<div id="dvDownloadClientCon" runat="server" visible="false">
		<div id="dvDownloadClient" runat="server" style="background-image:url(images/mykaneva/announcements/my_kaneva_install_bug_520x200.jpg);
			background-repeat:no-repeat;width:520px;height:200px;">						
			<asp:linkbutton id="btnDownloadClose" oncommand="btnDownloadClientClose_Click" commandname="btnDownloadClientClose_Click" runat="server"><div id="downloadClientClose" style="width:100px;height:20px;position:relative;left:410px;cursor:hand;"></div></asp:linkbutton>
			<a id="aDownload" runat="server" href="community/install3d.kaneva" onclick="callGAEvent('MyKaneva', 'DownloadAd', 'Click')"><div id="downloadClientButton" style="width:250px;height:50px;position:relative;left:250px;top:120px;"></div></a>		
			<a id="aLearnMore" runat="server" visible="false" style="display:block;position:relative;float:left;top:158px;left:160px;width:190px;height:20px;outline:none;"></a>
		</div>	
		</div>	
		<kaneva:mkcontainer id="MKcontainerBlastsActive" runat="server" ControlToLoad="mykaneva/blast/BlastsActive.ascx" />
												 
	</div>
	
	<!-- Right Column -->
	<div id="rightRail">
	
		<kaneva:mkcontainer id="MKcontainerFriendInviter" runat="server" ControlToLoad="usercontrols/doodad/FriendInviter.ascx" />
		<usercontrol:ContestWidget runat="server" MyContest="true" IncludeJavascript="true"></usercontrol:ContestWidget>
		<kaneva:mkcontainer id="MKcontainerEventsList" runat="server" ControlToLoad="usercontrols/doodad/EventsList.ascx" />
		<kaneva:mkcontainer id="MKcontainerPasses" runat="server" ControlToLoad="usercontrols/doodad/Passes.ascx" />
	
	</div>

</div>

</asp:Content>

