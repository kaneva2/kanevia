<%@ Page language="c#" MasterPageFile="~/masterpages/GenericLandingPageTemplate.Master" Codebehind="lostPassword.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.lostPassword" %>
<%@ Register TagPrefix="Kaneva" TagName="HeaderText" Src="usercontrols/HeaderText.ascx" %>


<asp:Content ID="cntCommunityMetaData" runat="server" contentplaceholderid="cphHeader" >
    <NOSCRIPT><META HTTP-EQUIV="refresh" CONTENT="1" URL="noJavascript.aspx" /></NOSCRIPT>
    <link href="css/base/base_new.css" type="text/css" rel="stylesheet" />

<style>
.pageBody {padding:50px 0 20px 0;background:#fff;border:1px solid #ccc;border-top:none;height:400px;}
.datacontainer {width:700px;margin:0 auto;}
p {margin:40px 0;line-height:16px;display:block;}
a.btnmedium {display:inline-block;margin-left:10px;}
div.msg {height:24px;color:#ee2d2a;}
div.msg span {color:#333;font-weight:bold;}
</style>

</asp:Content>


<asp:Content ID="cntKanevaCommunityBody" runat="server" contentplaceholderid="cphBody" >

<div class="datacontainer">
<h6>Forgot your password?</h6>

<p style="margin-bottom:20px;">If you forgot your password, enter your sign-in email and click "Send Now". We will 
send you an email with a link to easily reset your password.</p>

									
<p style="margin:0;">
<div class="msg" id="spnMsg" runat="server"></div>
Email address <input type="text" runat="server" id="txtUserName" class="Filter2" size="30" maxlength="80"/>
<asp:linkbutton id="btnPassword" align="right" runat="Server" onClick="btnPassword_Click" class="btnmedium orange" Text="Send Now" CausesValidation="True"></asp:linkbutton>
</p>
																		
<p>Having other problems with signing in? Please contact us at 
<a target="_blank" href="http://support.kaneva.com">http://support.kaneva.com</a>.</p>

</div>
																		

</asp:Content>