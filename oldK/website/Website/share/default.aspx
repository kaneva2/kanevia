<%@ Page Language="c#" Codebehind="aboutLanding.aspx.cs" AutoEventWireup="false"
    Inherits="KlausEnt.KEP.Kaneva.aboutLanding" %>

<link href="../css/shadow.css" type="text/css" rel="stylesheet">
<link href="../css/new.css" rel="stylesheet" type="text/css" />

<table border="0" cellspacing="0" cellpadding="0" class="newcontainer" align="center">
    <tr>
        <td>
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="newcontainerborder">
                <tr>
                    <td class="">
                    </td>
                    <td class="">
                    </td>
                    <td class="">
                    </td>
                </tr>
                <tr>
                    <td class="">
                        <img runat="server" src="~/images/spacer.gif" width="1" height="1" id="Img2" /></td>
                    <td valign="top" class="newdatacontainer">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <!-- TOP STATUS BAR -->
                                    <div id="pageheader">
                                        <table cellpadding="0" cellspacing="0" border="0" width="99%">
                                            <tr>
                                                <td nowrap align="left">
                                                    <table cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <td>
                                                                <h1>
                                                                    Share Kaneva with the World</h1>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td align="right" valign="middle">
                                                    <table cellpadding="0" cellspacing="0" border="0" width="590">
                                                        <tr>
                                                            <td class="headertout" width="175">
                                                            </td>
                                                            <td class="searchnav" width="260">
                                                            </td>
                                                            <td align="left" width="130">
                                                            </td>
                                                            <td class="searchnav" align="right" width="210" nowrap>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <!-- END TOP STATUS BAR -->
                                </td>
                            </tr>
                            <tr>
                                <td width="968" align="left" valign="top">
                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                        <tr>
                                            <td width="968" valign="top">
                                                <div class="module fullpage">
                                                    <span class="ct"><span class="cl"></span></span>
                                                    <table border="0" cellspacing="0" cellpadding="0" width="99%">
                                                        <tr>
                                                            <!-- START WIZARD CONTENT -->
                                                            <td valign="top" align="center">
                                                                <table border="0" cellspacing="0" cellpadding="0" class="nopadding">
                                                                    <tr>
                                                                        <td width="318" valign="top">
                                                                            <h2>
                                                                                It's Easy</h2>
                                                                            <p>
                                                                                Choose the appropriate link. Copy the source code. Paste it into your web page or
                                                                                email.</p>
                                                                            <hr>
                                                                            <br>
                                                                            <h2>
                                                                                It's Safe</h2>
                                                                            <p>
                                                                                The links are standard HTML and contain no scripts. You can also modify the style
                                                                                to fit your needs.</p>
                                                                            <hr>
                                                                            <br>
                                                                            <h2>
                                                                                It's Helpful</h2>
                                                                            <p>
                                                                                Friends and visitors will appreciate your thoughtfulness (and possibly shower you
                                                                                with love) when you provide them with quick, easy access to Kaneva.</p>
                                                                            <hr>
                                                                            <br>
                                                                            <h2>
                                                                                How it Works</h2>
                                                                            <p>
                                                                                If you have a profile, blog or web site we want you to link to us.</p>
                                                                            <hr>
                                                                            <br>
                                                                        </td>
                                                                        <td width="20">
                                                                        </td>
                                                                        <td width="600" valign="top" align="center">
                                                                            <div class="module whitebg">
                                                                                <span class="ct"><span class="cl"></span></span>
                                                                                <center>
                                                                                    <table border="0" cellspacing="0" cellpadding="5" width="80%" align="center" class="">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <img src="http://images.kaneva.com/media/banners/kaneva_468x60.gif" alt="kaneva_468x60.gif"
                                                                                                    width="468" height="60" border="0">
                                                                                                <textarea style="width: 468px;" rows="2" cols="40" id="468" onclick="document.getElementById('468').select()"><a href="http://www.kaneva.com?AFC=SK&size=468"><img border="0" alt="Meet Me 3D at Kaneva.com" title="Meet Me 3D at Kaneva.com" src="http://images.kaneva.com/media/banners/kaneva_468x60.gif"/></a></textarea><br>
                                                                                                Full Banner 468x60
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="formspacer_small">
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <img src="http://images.kaneva.com/media/banners/kaneva_400x75.gif" alt="kaneva_400x75.gif"
                                                                                                    width="400" height="75" border="0">
                                                                                                <textarea style="width: 468px;" rows="2" cols="40" id="400" onclick="document.getElementById('400').select()"><a href="http://www.kaneva.com?AFC=SK&size=400"><img border="0" alt="Meet Me 3D at Kaneva.com" title="Meet Me 3D at Kaneva.com" src="http://images.kaneva.com/media/banners/kaneva_400x75.gif"/></a></textarea><br>
                                                                                                MySpace Comment Banner 400x75
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="formspacer_small">
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <img src="http://images.kaneva.com/media/banners/kaneva_234x60.gif" alt="kaneva_234x60.gif"
                                                                                                    width="234" height="60" border="0">
                                                                                                <textarea style="width: 468px;" rows="2" cols="40" id="234" onclick="document.getElementById('234').select()"><a href="http://www.kaneva.com?AFC=SK&size=234"><img border="0" alt="Meet Me 3D at Kaneva.com" title="Meet Me 3D at Kaneva.com" src="http://images.kaneva.com/media/banners/kaneva_234x60.gif"/></a></textarea><br>
                                                                                                Half Banner 234x60
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="formspacer_small">
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <img src="http://images.kaneva.com/media/banners/kaneva_88x31.gif" alt="kaneva_88x31.gif"
                                                                                                    width="88" height="31" border="0">
                                                                                                <textarea style="width: 468px;" rows="2" cols="40" id="88" onclick="document.getElementById('88').select()"><a href="http://www.kaneva.com?AFC=SK&size=88"><img border="0" alt="Meet Me 3D at Kaneva.com" title="Meet Me 3D at Kaneva.com" src="http://images.kaneva.com/media/banners/kaneva_88x31.gif"/></a></textarea><br>
                                                                                                Button 88x31
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="formspacer_small">
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <img src="http://images.kaneva.com/media/banners/kaneva_80x15.gif" alt="kaneva_80x15.gif"
                                                                                                    width="80" height="15" border="0"><br>
                                                                                                <textarea style="width: 468px;" rows="2" cols="40" id="clink" onclick="document.getElementById('clink').select()"><a href="http://www.kaneva.com?AFC=SK&size=c"><img border="0" alt="Meet Me 3D at Kaneva.com" title="Meet Me 3D at Kaneva.com" src="http://images.kaneva.com/media/banners/kaneva_80x15.gif"/></a></textarea><br>
                                                                                                Chicklet
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="formspacer_small">
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <a href="http://www.kaneva.com?AFC=SK&size=t">Meet Me 3D at Kaneva.com</a><br />
                                                                                                <textarea style="width: 468px;" rows="2" cols="40" id="textlink" onclick="document.getElementById('textlink').select()"><a href="http://www.kaneva.com?AFC=SK&size=t">Meet Me 3D at Kaneva.com</a></textarea><br>
                                                                                                Text link only
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </center>
                                                                                <span class="cb"><span class="cl"></span></span>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <!-- END WIZARD CONTENT -->
                                                        </tr>
                                                    </table>
                                                    <span class="cb"><span class="cl"></span></span>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td class="">
                        <img src="http://www.kaneva.com/images/spacer.gif" id="Img5" width="1" height="1" /></td>
                </tr>
                <tr>
                    <td class="">
                    </td>
                    <td class="">
                    </td>
                    <td class="">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

