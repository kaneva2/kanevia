﻿<%@ Page Language="C#" MasterPageFile="~/masterpages/home.Master" EnableViewState="False" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.Home" %>

<asp:Content ID="cnt_mainHome" runat="server" ContentPlaceHolderID="cph_MainContent" >
<script>
    var $j = jQuery.noConflict();
</script>
    <div class="welcomeMessage">
        <h1>Create Your Own World</h1>
        <h2>Build and explore virtual worlds with your friends.</h2>
    </div>
    <asp:Panel id="pnlSignupForm" runat="server" DefaultButton="btnRegister" CSSClass="signupForm">
        <div class="inner">

        <h2>Join today - it's free.</h2>
        
	    <div class="formContainer">	
            <div>
                <div id='txtUserName_lbl' class="labeltxt" runat="server" clientidmode="static">Username</div>
	            <asp:textbox id="txtUserName" name="username" field-name="username" clientidmode="static" runat="server" maxlength="20" tabindex="5" AutoPostBack="false"></asp:textbox>
                <i id="txtUserName_validateimg" runat="server" clientidmode="static" name="txtUserName" class="validateimg username"></i><div id="txtUserName_errmsg" runat="server" clientidmode="static" class="errmsgbox username"><div id="username_msg" runat="server" clientidmode="static" class="msg">Usernames should be 4-20 characters and not have spaces.</div></div>
                <br class="clear" />
	            <asp:requiredfieldvalidator id="rfUsername" runat="server" controltovalidate="txtUserName" text="" 
                    enableclientscript="false" enabled="true" errormessage="" display="none"></asp:requiredfieldvalidator>
	            <asp:regularexpressionvalidator id="revUsername" runat="server" controltovalidate="txtUserName" text="" 
                    errormessage="Usernames should be 4-20 characters and a combination of upper and lower-case letters, numbers, and underscores (_) only. Do not use spaces." 
                    display="none" enableclientscript="false"></asp:regularexpressionvalidator>																							
            </div>
            <div>
                <div class="firstname-container">
                    <div id="txtFirstName_lbl" clientidmode="static" runat="server" class="labeltxt">First name</div>
			        <asp:textbox id="txtFirstName" runat="server" field-name="firstname" clientidmode="Static" maxlength="50" tabindex="6" cssclass="name"></asp:textbox>
                    <i id="txtFirstName_validateimg" name="txtFirstName" runat="server" clientidmode="static" class="validateimg firstname"></i>
                    <div id='txtFirstName_errmsg' class="errmsgbox short firstname"><div id="firstname_msg" runat="server" clientidmode="static" class="msg single">What is your first name?</div></div>
                </div>
                <div class="lastname-container">
                    <div id="txtLastName_lbl" clientidmode="static" runat="server" class="labeltxt lastname">Last name</div>
                    <asp:textbox id="txtLastName" runat="server" field-name="lastname" clientidmode="static" maxlength="50" tabindex="7" cssclass="last name"></asp:textbox>
                    <i id="txtLastName_validateimg" name="txtLastName" runat="server" clientidmode="static" class="validateimg lastname"></i>
                    <div id='txtLastName_errmsg' class="errmsgbox short lastname"><div id="lastname_msg" runat="server" class="msg single">What is your last name?</div></div>
                </div>
            </div>
            <div class="clear"></div>
            <div>
                <div id='txtEmail_lbl' runat="server" clientidmode="static" class="labeltxt">Email</div>
			    <asp:textbox id="txtEmail" clientidmode="Static" field-name="email" runat="server" maxlength="100" tabindex="8"></asp:textbox> 
                <i name="<%=txtEmail.ClientID%>" class="validateimg email"></i><div id='<%=txtEmail.ClientID%>_errmsg' class="errmsgbox email"><div class="msg">You’ll use your email to log in each time.</div></div>
                <asp:regularexpressionvalidator id="revEmail" runat="server" controltovalidate="txtEmail" text="" errormessage="Invalid email address."
			        display="none" enableclientscript="false"></asp:regularexpressionvalidator>
            </div>
            <div>
                <div id='<%=txtPassword.ClientID%>_lbl' class="labeltxt">New Password</div>
			    <asp:textbox id="txtPassword" clientidmode="static" field-name="password" runat="server" maxlength="20" tabindex="9" TextMode="Password"></asp:textbox>
                <i name="<%=txtPassword.ClientID%>" class="validateimg password"></i><div id='<%=txtPassword.ClientID%>_errmsgbox' class="errmsgbox password"><div class="msg">Passwords should be 4-20 letters, numbers, and underscores (_) only.</div></div>
		        <asp:regularexpressionvalidator id="revPassword" runat="server" controltovalidate="txtPassword" text="" errormessage="Passwords should be 4-20 characters and a combination of upper and lower-case letters, numbers and underscores (_) only. Do not use spaces."
			        display="none" enableclientscript="false"></asp:regularexpressionvalidator>
            </div>
            <div class="country-zip">
                <div class="country-container">
			        <asp:dropdownlist id="drpCountry" runat="server" tabindex="10" width="120" class="countrySelect"></asp:dropdownlist>
                </div>
                <div class="zip-container">
                    <div id='<%=txtPostalCode.ClientID%>_lbl' class="labeltxt">Zip Code</div>
			        <span id="divPostalCode" clientidmode="static" runat="server"><asp:textbox id="txtPostalCode" field-name="postalcode" runat="server" clientidmode="Static" maxlength="25" tabindex="11" class="postalCode"></asp:textbox>
                    <i name="<%=txtPostalCode.ClientID%>" class="validateimg postalcode"></i></span><div id='<%=txtPostalCode.ClientID%>_errmsg' class="errmsgbox short postalcode"><div class="msg single">Enter your zip code.</div></div>
	                <asp:requiredfieldvalidator id="rftxtPostalCode" runat="server" controltovalidate="txtPostalCode" text="" errormessage=""
		                display="none" enabled="false" enableclientscript="false"></asp:requiredfieldvalidator>
                </div>
            <div class="clear"></div>																							
            </div>
            <div class="birthday">
	            <label id="lblBirthday" runat="server" for='<%=drpMonth.ClientID %>'>Birthday</label>
				<div class="clear"></div>		
	            <asp:dropdownlist id="drpMonth" clientidmode="static" field-name="birthdate" cssclass="monthSelect" runat="server" tabindex="11">
				    <asp:listitem value="">Month</asp:listitem>
				    <asp:listitem value="1">January</asp:listitem>
				    <asp:listitem value="2">February</asp:listitem>
				    <asp:listitem value="3">March</asp:listitem>
				    <asp:listitem value="4">April</asp:listitem>
				    <asp:listitem value="5">May</asp:listitem>
				    <asp:listitem value="6">June</asp:listitem>
				    <asp:listitem value="7">July</asp:listitem>
				    <asp:listitem value="8">August</asp:listitem>
				    <asp:listitem value="9">September</asp:listitem>
				    <asp:listitem value="10">October</asp:listitem>
				    <asp:listitem value="11">November</asp:listitem>
				    <asp:listitem value="12">December</asp:listitem>
			    </asp:dropdownlist>
		        <asp:dropdownlist runat="server" clientidmode="static" field-name="birthdate" id="drpDay" cssclass="daySelect" tabindex="12">
				    <asp:listitem value="">Day</asp:listitem>
				    <asp:listitem value="1">01</asp:listitem>
				    <asp:listitem value="2">02</asp:listitem>
				    <asp:listitem value="3">03</asp:listitem>
				    <asp:listitem value="4">04</asp:listitem>
				    <asp:listitem value="5">05</asp:listitem>
				    <asp:listitem value="6">06</asp:listitem>
				    <asp:listitem value="7">07</asp:listitem>
				    <asp:listitem value="8">08</asp:listitem>
				    <asp:listitem value="9">09</asp:listitem>
				    <asp:listitem value="10">10</asp:listitem>
				    <asp:listitem value="11">11</asp:listitem>
				    <asp:listitem value="12">12</asp:listitem>
				    <asp:listitem value="13">13</asp:listitem>
				    <asp:listitem value="14">14</asp:listitem>
				    <asp:listitem value="15">15</asp:listitem>
				    <asp:listitem value="16">16</asp:listitem>
				    <asp:listitem value="17">17</asp:listitem>
				    <asp:listitem value="18">18</asp:listitem>
				    <asp:listitem value="19">19</asp:listitem>
				    <asp:listitem value="20">20</asp:listitem>
				    <asp:listitem value="21">21</asp:listitem>
				    <asp:listitem value="22">22</asp:listitem>
				    <asp:listitem value="23">23</asp:listitem>
				    <asp:listitem value="24">24</asp:listitem>
				    <asp:listitem value="25">25</asp:listitem>
				    <asp:listitem value="26">26</asp:listitem>
				    <asp:listitem value="27">27</asp:listitem>
				    <asp:listitem value="28">28</asp:listitem>
				    <asp:listitem value="29">29</asp:listitem>
				    <asp:listitem value="30">30</asp:listitem>
				    <asp:listitem value="31">31</asp:listitem>
			    </asp:dropdownlist>
		        <asp:dropdownlist runat="server" clientidmode="static" id="drpYear" field-name="birthdate" cssclass="yearSelect" tabindex="13">
				    <asp:listitem value="">Year</asp:listitem>
			    </asp:dropdownlist>
                <i name="birthdate" class="validateimg birthdate"></i><div id="birthdate_errmsg" class="errmsgbox birthdate"><div id="birthday_msg" class="msg" runat="server" clientidmode="static">Enter your birthday. You must be 14 or older to join.</div></div>
            </div>
            <div class="gender">
                <span style="display:none;">
			    <asp:dropdownlist id="drpGender" clientidmode="static" runat="server" class="genderSelect">
				    <asp:listitem value="">Select Sex</asp:listitem>
				    <asp:listitem value="Male">Male</asp:listitem>
				    <asp:listitem value="Female">Female</asp:listitem>
			    </asp:dropdownlist>
			    </span>
			    <div class="gender-container">
                    <div class="female"><input type="radio" id="radFemale" runat="server" clientidmode="static" name="gender" field-name="gender" value="Female" tabindex="14" /><span>Female</span></div>
			        <div class="male"><input type="radio" id="radMale" runat="server" clientidmode="static" name="gender" field-name="gender" value="Male" tabindex="15" /><span>Male</span></div>
                    <i name="gender" class="validateimg gender"></i>
                </div>
                <div id='gender_errmsg' class="errmsgbox gender"><div class="msg single">Please select either male or female.</div></div>
            </div>
            <div class="clear"></div>
            <div class="join-container">
                <asp:linkbutton id="btnRegister" clientidmode="static" runat="server" alternatetext="Start Now!" tabindex="17" CssClass="joinNow"></asp:linkbutton>
                <img class="loading" src="../../images/ajax-loader.gif" alt="Loading..." style="display:none;"/>	
            </div>

    	</div>
    
        </div>   

        <input type="hidden" id="txtUserNameUID" name="txtUserNameUID" value='<%=txtUserName.UniqueID%>' />
        <input type="hidden" id="txtFirstNameUID" name="txtFirstNameUID" value='<%=txtFirstName.UniqueID%>' />
        <input type="hidden" id="txtLastNameUID" name="txtLastNameUID" value='<%=txtLastName.UniqueID%>' />
        <input type="hidden" id="txtEmailUID" name="txtEmailUID" value='<%=txtEmail.UniqueID%>' />
        <input type="hidden" id="txtPasswordUID" name="txtPasswordUID" value='<%=txtPassword.UniqueID%>' />
        <input type="hidden" id="drpGenderUID" name="drpGenderUID" value='<%=drpGender.UniqueID%>' />
        <input type="hidden" id="drpDayUID" name="drpDayUID" value='<%=drpDay.UniqueID%>' />
        <input type="hidden" id="drpMonthUID" name="drpMonthUID" value='<%=drpMonth.UniqueID%>' />
        <input type="hidden" id="drpYearUID" name="drpYearUID" value='<%=drpYear.UniqueID%>' />
        <input type="hidden" id="drpCountryUID" name="drpCountryUID" value='<%=drpCountry.UniqueID%>' />
        <input type="hidden" id="txtPostalCodeUID" name="txtPostalCodeUID" value='<%=txtPostalCode.UniqueID%>' />
        <input type="hidden" id="postFromHome" name="postFromHome" value="1" />
    </asp:Panel>
   <div class="clear"><!-- clear the floats --></div>
</asp:Content>
<asp:Content ID="cnt_seoHome" runat="server" ContentPlaceHolderID="cph_SEO" >
    <div class="seo" id="seo1" runat="server" style="display:none">
        <!-- 3-d-world -->
        <h1 class="seo">Discover Our 3D Virtual World</h1>
	    <p>Kaneva blurs the line between the offline and online world in a 3D virtual world where the virtual you is an extension of the real you. A truly unique online experience, Kaneva brings web profiles and entertainment to life. Join the free 3D virtual world full of real friends and good times</p>	    
	    <br />
	    <h2 class="seo">An online Virtual World to Make Your Own</h2>
	    <p>In our online virtual world, you can invent your 3D avatar that’s as unique and stylish as you are. Decorate your free 3D home with your favorite photos and patterns. And with Kaneva’s 3D virtual world, you can enjoy your favorite videos, photos, and music on your 3D TV. The world’s more fun with friends! Invite yours and meet up in our online virtual world for 3D events, chats, and more!</p>
        <br /><a href="register/kaneva/registerInfo.aspx">Jump in and play! Join Kaneva’s 3-D virtual world.</a>
    </div>
    <div class="seo" id="seo2" runat="server" style="display:none">
        <!-- virtual life -->
        <h1 class="seo">With Kaneva, It’s Never Been Easier to Get a Virtual Life</h1>
	    <p>Create a virtual life in Kaneva—where the 3D you is an extension of the real you. More than a virtual life game, Kaneva’s a modern-day place where you, your media, and your friends come to life. Meet new people, chat, explore, and get the virtual life you’ve been dreaming of.</p>	    
	    <br />
	    <h2 class="seo"> What Are You Waiting for? Create a Virtual Life!</h2>
	    <p>Creating a virtual life in Kaneva opens the door to a world full of exciting people, places, and entertainment. Explore thousands of 3D hangouts and connect with people who share your interests. In your virtual life, you can bring friends to meet up for 3D events, chats, and more! With Kaneva, you can make your 3D avatar as unique and stylish as you are. Decorate your free 3D home with your favorite photos and watch YouTube videos with friends on your TV.</p>
            <br /><a href="register/kaneva/registerInfo.aspx">Get your free virtual life with Kaneva.</a>
    </div>
    <div class="seo" id="seo3" runat="server" style="display:none">
        <!-- avatar -->
        <h1 class="seo">Create an Avatar in the Virtual World of Kaneva</h1>
	    <p>You know you'll look good in 3D, so what are you waiting for? Using our free avatar creator, you can create your very own cool avatar. Make your avatar as unique and stylish as you are, and shop for the latest 3D clothes and accessories in our free virtual world.</p>	    
	    <p>Then use your avatar to explore the Virtual World of Kaneva and interact in 3D. Full of modern day people and entertainment, being in the Virtual World is like hanging out and going places with your friends in the real world. If you've heard about virtual avatar worlds before, now is your chance to experience what the buzz is about.</p>	    
	    <br />
	    <h2 class="seo">Discover the new you (in 3D) with our free avatar creator</h2>
	    <p>With our free avatar creator, you can choose from over 55 million possible combinations, including body type, hairstyle, facial features, and skin color. Tall, short, or curvy? You decide. Hair color and cut? It's your call. It's an avatar game for Kaneva's virtual world. Use our free virtual avatar maker to create yourself in 3D. Then share your cool new Avatar with friends.</p>
            <br /><a href="register/kaneva/registerInfo.aspx">Use our Free Virtual Avatar Maker to Create Yourself in.</a>
    </div>
    <div class="seo" id="seo4" runat="server" style="display:none">
        <!-- virtual-games -->
        <h1 class="seo">Why play virtual reality games when you can live a virtual life?</h1>
	    <p>Experience our free virtual world where you, your media, and your friends come to life. Unlike most virtual people games, with Kaneva, you live in a modern-day virtual world full of exciting people, places, and entertainment. It’s a whole new way to connect with others—a world full of real friends and good times. Jump in and play the virtual reality game like no other.</p>	    
	    <br />
	    <h2 class="seo">More than a virtual online game, Kaneva’s an extension of the real you</h2>
	    <p>Kaneva blurs line between real life and virtual reality games by creating a world where the 3D you is an extension of the real you. A truly unique online experience, Kaneva brings web profiles and entertainment to life in a modern-day virtual world full of real friends and good times.</p>
            <br /><a href="register/kaneva/registerInfo.aspx">Join Kaneva and experience more than a virtual reality game.</a>
    </div>
    <div class="seo" id="seo5" runat="server" style="display:none">
        <!-- rpg-mmo -->
        <h1 class="seo">Introducing Kaneva—a Free MMO like No Other</h1>
	    <p>Experience Kaneva for yourself, it’s the ultimate free MMO. A virtual world where you, your media, and your friends come to life. Unlike most free online mmo games, with Kaneva, you live in a modern-day virtual world full of exciting people, places, and entertainment. It’s a whole new way to connect with others—a world full of real friends and good times. Jump in and play the mmog like no other.</p>	    
	    <p>A free mmo, with Kaneva, you can create your own unique avatar, get an exclusive, FREE Kaneva City apartment, explore and meet friends in rockin' 3D hangouts, AND play the ultimate 3D dance game.</p>	    
	    <br />
	    <h2 class="seo">Want to Create Your Own MMO?</h2>
	    <p>Do you have what it takes to help create the next generation of MMO game and 3D virtual world content? Are you a game developer looking to take advantage of the opportunities in the new era of online gaming and social media? The Kaneva Game Platform is designed for end-to-end MMO game (MMOG) development for FPS and RPG genres. You can join the Kaneva Elite Developers group and learn how to create your own free 3D online MMOG.</p>
        <br /><a href="register/kaneva/registerInfo.aspx">Join Kaneva and experience the ultimate mmog.</a>
    </div>
    <div class="seo" id="seo6" runat="server" style="display:none">
        <!-- 3d-chat-world -->
        <h1 class="seo">Why just chat when you can chat in 3D?</h1>
	    <p>Teleport yourself into a vibrant 3D world where avatars, media and communities come to life. Make friends and meet up for games, 3D chats, and more! Find new friends that are your age, live in your area, and share your interests. Better yet, invite your friends to chat in the 3D world!</p>
	    <ul>
	        <li>Create a 3D avatar that’s as distinct and stylish as you are</li>
	        <li>Get a free, fully customizable 3D home</li>
	        <li>Watch and share your favorite videos, play games, or 3D chat with your friends!</li>
	    </ul>	    
	    <br />
	    <h2 class="seo">A whole new way to connect with friends online - It’s 3D Avatar Chat.</h2>
	    <p>It’s more than a 3D chat room, it’s a vibrant 3D world where friends, communities and entertainment come to life! Create your 3D avatar and shop for the latest fashions, emote, chat, dance, and interact with your friends in 3D!</p>
	    <p>Invite your friends over to hang out, 3D chat, while you watch YouTube videos on your 3D TV. Plus, explore thousands of 3D hangouts and have an avatar chat with people from all over the world!</p>
        <br /><a href="register/kaneva/registerInfo.aspx">Join Kaneva and Invite Your Friends to 3D Chat.</a>
    </div>
    <div class="seo" id="seo7" runat="server" style="display:none">
        <!-- on line community -->
        <h1 class="seo">Introducing Kaneva—the best collection of Online Communities</h1>
	    <p>Kaneva is an online community, incorporating social networking, forums, gaming and a 3D virtual world. A collection of free 3D communities created by its members, with Kaneva, you can create your own unique avatar, get an exclusive, FREE Kaneva City apartment, explore and meet friends in rockin' 3D hangouts made by the community.</p>	    
	    <p>Experience Kaneva for yourself, it’s the ultimate free community builder. A place where you, your community, and your friends come to life inside a 3D world. Unlike most free online communities, with Kaneva, you build a virtual meeting place, hangouts, inside in a modern-day 3D world full of exciting people, places, and entertainment. It’s a whole new way to connect with your online community—Jump in today.</p>	    
	    <br />
	    <h2 class="seo">Want to Build Your Own Online 3D Community?</h2>
	    <p>Do you have what it takes to help create the next generation of online communities? Are you a virtual world builder looking to take advantage of the opportunities in the new era of online gaming and social media? Then Kaneva is designed for you. Learn how to create your own free 3D online community.</p>
        <br /><a href="register/kaneva/registerInfo.aspx">Join Kaneva and experience the ultimate online community.</a>
    </div>
    <div class="seo" id="seo8" runat="server" style="display:none">
        <!-- artists -->
        <h1 class="seo">Tap into our Growing Artist Network</h1>
	    <p>For an up-and-coming band, performer, DJ or artist, finding an artist network in which you can easily showcase your talent and get noticed can be difficult. You want to find an online artist community that brings people together all over the world and provides a stage for you to share your talent and earn recognition. At Kaneva, we don’t mean that hypothetically we mean it literally! In the <a id="A1" href="~/3d-virtual-world/virtual-life.kaneva" runat="server">3D Virtual World of Kaneva</a>, your music, movies, artwork or other creative expressions can be featured in virtual theaters or other public hang-outs.</p>	    
	    <br />
	    <h2 class="seo">Build a Connected Audience with Our Online Artist Community</h2>
	    <p>Kaneva is about enabling artists to easily get involved in our artist network by creating their own online Communities to showcase their work and build a global audience. Plug into our online artist community by starting a Community on Kaneva and then extend that Community to 3D for a real life experience. Every Community on Kaneva gets a <a id="A2" href="~/community/communitypage.aspx?communityid=1118&pageId=12394" runat="server">3D hangout</a> -- theater, media room or other virtual meeting place.</p>
        <br />
	    <p>As an artist, you can be part of our online artist community to showcase your talent and interact with your audience through forums, blogs and in-world 3D chat. Once in-world, you can even put on a virtual event - at which you play your own music, show your videos or other artistic creations. You can also earn "raves" or votes that will help you climb to the top of the Kaneva site Leaderboards and get you noticed in our artist network.</p>
        <br />
	    <p>Getting exposure for your art and talent is the name of the game in the crowded entertainment industry. By joining Kaneva, you gain access to an artist network full of possibilities and all the tools you need to upload your media, build and customize an online and 3D Community that matches your style and features all of your best work. Use our unique online artist community to make connections with other musicians, photographers, film makers, other artists, existing fans and entertainment enthusiasts. Get enough "raves" from Kaneva members and you and your talent will rise to the top and get noticed.</p>
        <br />
        <a href="register/kaneva/registerInfo.aspx">Join Kaneva and experience the ultimate online community.</a>
    </div>
    <div class="seo" id="seo9" runat="server" style="display:none">
        <!-- media sharing -->
        <h1 class="seo">Kaneva Media Sharing Features Include Free Video Sharing</h1>
	    <p>Kaneva offers free video sharing - as well other types of media sharing - to all of its members. Simply upload your media - photos, videos, music, and games -- to your Kaneva Profile. Include media that represents you and your interests and use them to help share your passions with the world.  You can also easily start and add your favorite media to a special interest Community on Kaneva. Kaneva's Open Media Module lets you share your media by placing it anywhere on the Net.</p>	    
	    <br />
	    <p>Take media sharing one step further, by showcasing your media in 3D in Kaneva's rapidly emerging Virtual World. In the <a id="A3" href="~/3d-virtual-world/virtual-life.kaneva" runat="server">Virtual World of Kaneva</a>, you get a 3D home to call their own. Use your Media Library to feature the media in your Profile in your 3D home -- watch your favorite video clips or home movies on your virtual TV and show off your favorite photos in picture frames on the walls.</p>	    
	    <br />
	    <p>Take advantage of media sharing within Kaneva's Communities by gaining a 3D hang-out in the Virtual World. If you start a Kaneva Community, you can use our free video sharing capabilities to create a virtual movie theater where people can meet to see you world premiere your latest video clips! You can even throw a virtual party and invite everyone you know to mix and mingle.</p>	    
	    <br />
	    <h2 class="seo">We Make Media Sharing Simple</h2>
	    <p>With the photo and video sharing capabilities offered by Kaneva, you can express yourself uniquely to anyone who looks at your Profile, Community or virtual home. Our Open Media Player lets you display your work anywhere on the Net. Musicians and artists can use the media sharing capability to build a following that goes beyond local borders to attract a global audience. Kaneva’s free video sharing brings people together around their interests both on the web and in a 3D Virtual World.</p>
        <br />
	    <p>Sign up now, get started building your Profile and take advantage of easy music, game, photo and video sharing!</p>
        <br />
        <a href="register/kaneva/registerInfo.aspx">Join Kaneva and experience the ultimate online community.</a>
    </div>
    <div class="seo" id="seo" runat="server" style="display:none">
        <br />
        <h1 class="seo">Create Your Own World</h1>
        <br />
	    <p>
            Kaneva allows millions of people to explore, interact and create their own worlds. You can make a virtual
            world or a 3D MMO Game World. Explore a large number of Worlds made by our community and have fun with friends. Bring your imagination and
            step into your own World on Kaneva.
        </p>
        <br /><a href="register/kaneva/registerInfo.aspx">Join Kaneva</a>
    </div>
    <div style="display:none;">													
	    <!--Google Code for Yahoo! Conversion Page -->
	    <script language="JavaScript" type="text/javascript">
	    <!--
	    	var google_conversion_type = 'landing';
	    	var google_conversion_id = 1065938872;
	    	var google_conversion_language = "en_US";
	    	var google_conversion_format = "1";
	    	var google_conversion_color = "FFFFFF";
	    //-->
	    </script>
	    <script language="JavaScript" src="//www.googleadservices.com/pagead/conversion.js">
	    </script>
	    <noscript>
	    <img height=1 width=1 border=0 src="//www.googleadservices.com/pagead/conversion/1065938872/extclk?script=0">
	    </noscript>	
    </div> 
</asp:Content>
