///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for downloads.
	/// </summary>
	public class downloads : BlogBasePage
	{
		protected downloads () 
		{
			Title = "Downloads";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here

			// Media Launcher
            lblVersion.Text = System.Configuration.ConfigurationManager.AppSettings["KMLVersion"].ToString();

            string filename = System.Configuration.ConfigurationManager.AppSettings["KMLFilename"].ToString();
			string filePath = Server.MapPath("~/download/" + filename);

			if (System.IO.File.Exists (filePath))
			{
				aKMLSetup.HRef = ResolveUrl ("~/download/" + filename);
				hlKMLSetup.NavigateUrl = ResolveUrl ("~/download/" + filename);
			}
			else
			{
				aKMLSetup.HRef = "javascript:alert('File was not found on server.');";
				hlKMLSetup.NavigateUrl = "javascript:alert('File was not found on server.');";
			}

			// 

			// Media Publisher Win
            lblKMPVersion.Text = System.Configuration.ConfigurationManager.AppSettings["KMPVersion_Win"].ToString();
            filename = System.Configuration.ConfigurationManager.AppSettings["KMPFilename_Win"].ToString();
			filePath = Server.MapPath("~/download/" + filename);

			if (System.IO.File.Exists (filePath))
			{
				aKMPWinSetup.HRef = ResolveUrl ("~/download/" + filename);
				hlKMPWinSetup.NavigateUrl = ResolveUrl ("~/download/" + filename);
			}
			else
			{
				aKMPWinSetup.HRef = "javascript:alert('File was not found on server.');";
				hlKMPWinSetup.NavigateUrl = "javascript:alert('File was not found on server.');";
			}


			// Media Publisher Mac
            lblKMPMacVersion.Text = System.Configuration.ConfigurationManager.AppSettings["KMPVersion_Mac"].ToString();
            filename = System.Configuration.ConfigurationManager.AppSettings["KMPFilename_Mac"].ToString();
			filePath = Server.MapPath("~/download/" + filename);

			if (System.IO.File.Exists (filePath))
			{
				aKMPMacSetup.HRef = ResolveUrl ("~/download/" + filename);
				hlKMPMacSetup.NavigateUrl = ResolveUrl ("~/download/" + filename);
			}
			else
			{
				aKMPMacSetup.HRef = "javascript:alert('File was not found on server.');";
				hlKMPMacSetup.NavigateUrl = "javascript:alert('File was not found on server.');";
			}

			// Game Platform
            lblKGPVersion.Text = System.Configuration.ConfigurationManager.AppSettings["KGPVersion"].ToString();

		}
		

		/// <summary>
		/// Click the add button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnDownload_Click (Object sender, ImageClickEventArgs e) 
		{
			getKGPLink();
		}

		protected void btnDownload2_Click (object sender, EventArgs e) 
		{
			getKGPLink();
		}


		protected void getKGPLink ()
		{

			if (!Request.IsAuthenticated)
			{
                Response.Redirect(GetLoginURL());
			}
			else
			{
				//string editorFileName = "KanevaGamePlatform_66.exe";
                string editorFileName = System.Configuration.ConfigurationManager.AppSettings["KGPFilename"].ToString();

				string filePath = Server.MapPath("~/download/" + editorFileName);

				if (System.IO.File.Exists (filePath))
				{
                    UsersUtility.InsertUserEditorDownload(GetUserId(), System.Configuration.ConfigurationManager.AppSettings["KGPVersion"].ToString());

					Response.Clear ();
					Response.ContentType = "application/octet-stream";
					Response.AddHeader ("Content-Length", new System.IO.FileInfo (filePath).Length.ToString());
					Response.AddHeader ("Content-Disposition", "attachment; filename=\"" + editorFileName + "\"");
					Response.Flush ();
					Response.TransmitFile (filePath);
					Response.End ();
				}
				else
				{
					ShowErrorOnStartup ("File was not found on server.");
				}
			}

		}

		protected HtmlAnchor aKMLSetup, aKMPWinSetup, aKMPMacSetup, aKGPSetup;
		protected HyperLink hlKMLSetup, hlKMPWinSetup, hlKMPMacSetup;
		protected Label lblVersion, lblKMPVersion, lblKMPMacVersion, lblKGPVersion;
		protected LinkButton hlKGPSetup;

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
