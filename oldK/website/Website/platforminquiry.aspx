<%@ Page language="c#" Codebehind="platforminquiry.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.platforminquiry1" %>
<link href="css/shadow.css" rel="stylesheet" type="text/css">
<link href="css/new.css" rel="stylesheet" type="text/css">

<table cellpadding="0" cellspacing="0" border="0" width="990" align="center">
	<tr>
		<td colspan="3"><img src="~/images/spacer.gif" id="Img1" width="1" height="14" /></td>
	</tr>
	<tr>
		<td colspan="3">
			<table  border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="frTopLeft"></td>
					<td class="frTop"></td>
					<td class="frTopRight"></td>
				</tr>
				<tr>
					<td class="frBorderLeft"><img src="~/images/spacer.gif" id="Img2" width="1" height="1" /></td>
					<td valign="top" class="frBgIntMembers">
						<table width="100%" border="0" cellpadding="0" cellspacing="0" >
							<tr>
								<td width="968" align="left" valign="top">
									<table cellpadding="0" cellspacing="0" border="0" width="100%">
										<tr>
											<td width="970" valign="top" align="center">
												<table border="0" cellspacing="0" cellpadding="0" width="99%">
													<tr>
														<!-- START CONTENT -->
														<td valign="top" align="center">
															<table border="0" cellspacing="0" cellpadding="0">
																<tr>
																	<td width="670">
																		
																		<div class="module fullpage">
																		<span class="ct"><span class="cl"></span></span>
																		<h1>KGP Commercial License Inquiry</h1><br>
																		<h3>Want to license the Kaneva Game Platform for your commercial endeavors?<br>
																		Please use this form to provide us with your information, and a Kaneva Business 
																		Representative will contact you shortly.<br></h3>
																		<br>
																		<table border="0" cellspacing="0" cellpadding="0" width="470" align="center">
																			<tr>
																				<td align="center">
																					<asp:validationsummary class="formError" id="valSum" runat="server" showmessagebox="False" showsummary="True"
																						displaymode="BulletList" headertext="Please correct the errors indicated by the asterisks."></asp:validationsummary>
																					<asp:customvalidator id="cvBlank" runat="server" display="None" enableclientscript="False"></asp:customvalidator>
																					<br>
																					<table id="Table11" cellSpacing="0" cellPadding="0" width="470" align="center" border="0" class="wizform">
																						<tr >
																							<td colspan="2"><h4>Tell us about yourself</h4><br></td>
																						</tr>
																						<tr>
																							<td>First Name
																								<asp:requiredfieldvalidator id="rffirstname" runat="server" controltovalidate="txtFirstName" text="*" errormessage=""
																									display="Dynamic"></asp:requiredfieldvalidator><br>
																								<asp:textbox class="biginput" id="txtFirstName" runat="server" maxlength="50" tabindex="1" style="Width: 220px;"></asp:textbox></td>
																							<td>Last Name
																								<asp:requiredfieldvalidator id="rflastname" runat="server" controltovalidate="txtLastName" text="*" errormessage=""
																									display="Dynamic"></asp:requiredfieldvalidator><br>
																								<asp:textbox class="biginput" id="txtLastName" runat="server" maxlength="50" tabindex="2" style="Width: 220px;"></asp:textbox></td>
																						</tr>
																						<tr>
																							<td class="note" vAlign="top" colspan="2">Kaneva never displays your name publicly.</td>
																						</tr>
																						<tr><td class="formspacer" colspan="2"></td></tr>
																						
																						<tr>
																							<td>Email
																								<asp:requiredfieldvalidator id="rfEmail" runat="server" controltovalidate="txtEmail" text="*" errormessage=""
																									display="Dynamic"></asp:requiredfieldvalidator><br>
																								<asp:textbox class="biginput" id="txtEmail" runat="server" maxlength="100" tabindex="3" style="Width: 220px;"></asp:textbox></td>
																							<td>Phone Number
																								<asp:requiredfieldvalidator id="rfPhone" runat="server" controltovalidate="txtPhone" text="*" errormessage=""
																									display="Dynamic"></asp:requiredfieldvalidator><br>
																								<asp:textbox class="biginput" id="txtPhone" runat="server" maxlength="50" tabindex="4" style="Width: 220px;"></asp:textbox></td>
																						</tr>
																						<tr>
																							<td class="note">Kaneva does not spam and <A onclick="javascript:window.open('./overview/privacy.aspx','Priv','width=500,height=400,resizable=yes,scrollbars=yes');false;" href="#">cares about your privacy.</A></td>
																							<td class="note">Include area or country code.</td>
																						</tr>
																						<tr><td class="formspacer" colspan="2"></td></tr>
																						
																						<tr>
																							<td colspan="2"><h4>Tell us about your Organization</h4><br></td>
																						</tr>

																						<tr>
																							<td colspan="2">Company Name
																								<asp:requiredfieldvalidator id="rfCompanyName" runat="server" controltovalidate="txtCompanyName" text="*" errormessage=""
																									display="Dynamic"></asp:requiredfieldvalidator><br>
																								<asp:textbox class="biginput" id="txtCompanyName" runat="server" maxlength="200" tabindex="5" style="Width: 440px;"></asp:textbox></td>
																						</tr>
																						<tr><td class="formspacer" colspan="2"></td></tr>
																						
																						<tr>
																							<td colspan="2">Mailing Address (Line 1)
																								<asp:requiredfieldvalidator id="rfCompanyAddress1" runat="server" controltovalidate="txtCompanyAddress1" text="*" errormessage=""
																									display="Dynamic"></asp:requiredfieldvalidator><br>
																								<asp:textbox class="biginput" id="txtCompanyAddress1" runat="server" maxlength="200" tabindex="6" style="Width: 440px;"></asp:textbox></td>
																						</tr>
																						<tr>
																							<td colspan="2">Mailing Address (Line 2)<br>
																								<asp:textbox class="biginput" id="txtCompanyAddress2" runat="server" maxlength="200" tabindex="7" style="Width: 440px;"></asp:textbox></td>
																						</tr>	
																						<tr>
																							<td class="note" colspan="2">Optional, if extra space is needed.</td>
																						</tr>
																						<tr><td class="formspacer" colspan="2"></td></tr>

																						<tr>
																							<td>City
																								<asp:requiredfieldvalidator id="rfCity" runat="server" controltovalidate="txtCity" text="*" errormessage=""
																									display="Dynamic"></asp:requiredfieldvalidator><br>
																								<asp:textbox class="biginput" id="txtCity" runat="server" maxlength="50" tabindex="8" style="Width: 220px;"></asp:textbox></td>
																							<td>State or Province
																								<asp:requiredfieldvalidator id="rfState" runat="server" controltovalidate="txtState" text="*" errormessage=""
																									display="Dynamic"></asp:requiredfieldvalidator><br>
																								<asp:textbox class="biginput" id="txtState" runat="server" maxlength="50" tabindex="9" style="Width: 220px;"></asp:textbox></td>
																						</tr>
																						<tr><td class="formspacer" colspan="2"></td></tr>
																							<td>Country
																								<asp:requiredfieldvalidator id="rfCountry" runat="server" controltovalidate="drpCountry" text="*" errormessage=""
																									display="Static"></asp:requiredfieldvalidator><br>
																								<asp:dropdownlist class="formKanevaText" id="drpCountry" runat="server" tabindex="10" style="Width: 220px;"></asp:dropdownlist></td>
																							<td>Zip or Post Code
																								<asp:requiredfieldvalidator id="rfPostalCode" runat="server" controltovalidate="txtPostalCode" text="*" errormessage="Zip code is a required field for U.S. residents."
																									display="Dynamic" enabled="False"></asp:requiredfieldvalidator><br>
																								<asp:textbox class="biginput" id="txtPostalCode" runat="server" maxlength="25" tabindex="11" style="Width: 100px;"></asp:textbox><img runat="server" src="~/images/spacer.gif" height="1" width="15">
																						</tr>
																						<tr><td class="formspacer" colspan="2"></td></tr>
																					
																						<tr>
																							<td colspan="2">Website URL
																								<asp:requiredfieldvalidator id="rfWebsiteUrl" runat="server" controltovalidate="txtWebsiteUrl" text="*" errormessage=""
																									display="Dynamic"></asp:requiredfieldvalidator><br>
																								<asp:textbox class="biginput" id="txtWebsiteUrl" runat="server" maxlength="200" tabindex="12" style="Width: 440px;"></asp:textbox></td>
																						</tr>
																						<tr><td class="formspacer" colspan="2"></td></tr>
																					
																						<tr>
																							<td>Annual Revenue
																								<asp:requiredfieldvalidator id="rfAnnualRevenue" runat="server" controltovalidate="txtAnnualRevenue" text="*" errormessage=""
																									display="Dynamic"></asp:requiredfieldvalidator><br>
																								<asp:textbox class="biginput" id="txtAnnualRevenue" runat="server" maxlength="50" tabindex="13" style="Width: 110px;"></asp:textbox></td>
																								
																							<td>Company History
																								<asp:requiredfieldvalidator id="rfHistory" runat="server" controltovalidate="drpHistory" text="*" errormessage="" 
																									display="Static"></asp:requiredfieldvalidator><br>
																								<asp:dropdownlist class="insideBoxText11" id="drpHistory" runat="server" tabindex="14">
																									<asp:listitem value="">Please select...</asp:listitem>
																									<asp:listitem value="Experienced MMOG Studio">Experienced MMOG Studio</asp:listitem>
																									<asp:listitem value="Startup MMOG Studio">Startup MMOG Studio</asp:listitem>
																									<asp:listitem value="Independent Consultant">Independent Consultant</asp:listitem>
																									<asp:listitem value="Publisher">Publisher</asp:listitem>
																									<asp:listitem value="In-House (Publisher) Studio">In-House (Publisher) Studio</asp:listitem>
																									<asp:listitem value="Private Venture Capital">Private Venture Capital</asp:listitem>
																								</asp:dropdownlist>
																							</td>
																						</tr>
																						<tr>
																							<td class="note">Estimate in US Dollars.</td>
																						</tr>

																						<tr><td class="formspacer" colspan="2"></td></tr>
																						<tr>
																							<td colspan="2"><h4>Tell us about your project</h4><br></td>
																						</tr>

																						<tr>
																							<td colspan="2">Project Name
																								<asp:requiredfieldvalidator id="rfProjectName" runat="server" controltovalidate="txtProjectName" text="*" errormessage=""
																									display="Dynamic"></asp:requiredfieldvalidator><br>
																								<asp:textbox class="biginput formKanevaText" id="txtProjectName" runat="server" maxlength="400" tabindex="15" style="Width: 440px;"></asp:textbox></td>
																						</tr>
																						<tr><td class="formspacer" colspan="2"></td></tr>

																						<tr>
																							<td colspan="2">Describe your Project
																								<asp:requiredfieldvalidator id="rfProjectDescription" runat="server" controltovalidate="txtProjectDescription" text="*" errormessage=""
																									display="Dynamic"></asp:requiredfieldvalidator><br>
																								<asp:textbox class="biginput" id="txtProjectDescription" runat="server" maxlength="4000" tabindex="16" style="Width: 440px; Height=100px;" TextMode="Multiline"></asp:textbox></td>
																						</tr>
																						<tr>
																							<td colspan="2" class="note">Include as much information as is publicly available.</td>
																						</tr>
																						<tr><td class="formspacer" colspan="2"></td></tr>
																						
																						<tr>
																							<td>Project Start Date
																								<asp:requiredfieldvalidator id="rfStartDate" runat="server" controltovalidate="drpStartDate" text="*" errormessage="" 
																									display="Static"></asp:requiredfieldvalidator><br>
																								<asp:dropdownlist class="insideBoxText11" id="drpStartDate" runat="server" tabindex="17">
																									<asp:listitem value="">Please select...</asp:listitem>
																									<asp:listitem value="Now">Now</asp:listitem>
																									<asp:listitem value="1 Month">1 Month</asp:listitem>
																									<asp:listitem value="3 Months">3 Months</asp:listitem>
																									<asp:listitem value="6 Months">6 Months</asp:listitem>
																									<asp:listitem value="More than 6 Months">More than 6 Months</asp:listitem>
																									<asp:listitem value="Don't Know">Don't Know</asp:listitem>
																								</asp:dropdownlist>
																							</td>
																							<td>Client Platform
																								<asp:requiredfieldvalidator id="rfClientPlatform" runat="server" controltovalidate="drpClientPlatform" text="*" errormessage="" 
																									display="Static"></asp:requiredfieldvalidator><br>
																								<asp:dropdownlist class="insideBoxText11" id="drpClientPlatform" runat="server" tabindex="18">
																									<asp:listitem value="">Please select...</asp:listitem>
																									<asp:listitem value="PC Low Spec">PC Low Spec</asp:listitem>
																									<asp:listitem value="PC Current">PC Current</asp:listitem>
																									<asp:listitem value="PC Next Gen">PC Next Gen</asp:listitem>
																									<asp:listitem value="Other">Other</asp:listitem>
																								</asp:dropdownlist>
																							</td>
																						</tr>
																						<tr><td class="formspacer" colspan="2"></td></tr>
																				
																						<tr>
																							<td>Project Budget
																								<asp:requiredfieldvalidator id="rfBudget" runat="server" controltovalidate="txtBudget" text="*" errormessage=""
																									display="Dynamic"></asp:requiredfieldvalidator><br>
																								<asp:textbox class="biginput" id="txtBudget" runat="server" maxlength="50" tabindex="19" style="Width: 110px;"></asp:textbox></td>

																							<td>Project Status
																								<asp:requiredfieldvalidator id="rfProjectStatus" runat="server" controltovalidate="drpProjectStatus" text="*" errormessage="" 
																									display="Static"></asp:requiredfieldvalidator><br>
																								<asp:dropdownlist class="insideBoxText11" id="drpProjectStatus" runat="server" tabindex="20">
																									<asp:listitem value="">Please select...</asp:listitem>
																									<asp:listitem value="Planning">Planning</asp:listitem>
																									<asp:listitem value="In Development">In Development</asp:listitem>
																								</asp:dropdownlist>
																							</td>
																						</tr>
																						<tr>
																							<td class="note">Estimate in US Dollars.</td>
																						</tr>
																						<tr><td class="formspacer" colspan="2"></td></tr>
																						
																						<tr>
																							<td>Current Team Size
																								<asp:requiredfieldvalidator id="CurrentMembers" runat="server" controltovalidate="txtCurrentMembers" text="*" errormessage=""
																									display="Dynamic"></asp:requiredfieldvalidator><br>
																								<asp:textbox class="biginput" id="txtCurrentMembers" runat="server" maxlength="50" tabindex="21" style="Width: 110px;"></asp:textbox></td>

																							<td>Projected Team Size
																								<asp:requiredfieldvalidator id="rfProjectedMembers" runat="server" controltovalidate="txtProjectedMembers" text="*" errormessage=""
																									display="Dynamic"></asp:requiredfieldvalidator><br>
																								<asp:textbox class="biginput" id="txtProjectedMembers" runat="server" maxlength="50" tabindex="22" style="Width: 110px;"></asp:textbox></td>
																						</tr>
																						<tr>
																							<td colspan="2" class="note" vAlign="top">Please include employees and managers.</td>
																						</tr>
																						<tr><td class="formspacer" colspan="2"></td></tr>

																					</table>
																				</td>
																			</tr>
																		</table>
																		
																		<br><br>
																		
																		<table border="0" cellspacing="0" cellpadding="5" width="99%">
																			<tr>
																				<td align="right"><asp:imagebutton id="btnSubmit" onclick="btnSubmit_Click" runat="server" imageurl="~/images/buttons/sendMessage.gif"
																					alternatetext="Send inquiry" width="107" height="26" causesvalidation="False" tabindex="23"></asp:imagebutton></td>
																			</tr>
																		</table>
																		<span class="cb"><span class="cl"></span></span>
																		</div>
																	
																	</td>
																</tr>
															</table>
															
														</td>
														
														<!-- END CONTENT -->
													</tr>
												</table>
											
											</td>
											
										</tr>
										
									</table>
											
								</td>
							</tr>
							
						</table>
					</td>
					<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1"></td>
				</tr>
				<tr>
					<td class="frBottomLeft"></td>
					<td class="frBottom"></td>
					<td class="frBottomRight"></td>
				</tr>
			</table>				
		</td>
	</tr>
</table>
