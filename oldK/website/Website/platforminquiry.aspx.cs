///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Web;
using System.Web.Mail;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Net;
using log4net;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for platforminquiry1.
	/// </summary>
	public class platforminquiry1 : MainTemplatePage
	{
		protected platforminquiry1 () 
		{
		}

		private void Page_Load (object sender, System.EventArgs e)
		{
			//set meta data
			Title = "Kaneva Game Platform Inquiry Form";
			MetaDataDescription = "<meta name=\"description\" content=\"Want to license the Kaneva Game Platform for your commercial endeavors? Please use this form to provide us with your information. \" />";

			//setup header nav bar
			HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.NONE;

			// Add a nice message since this action may take a moment to submit the issue.
			btnSubmit.Attributes.Add ("onClick", "if (typeof(Page_ClientValidate) == 'function') {if (Page_ClientValidate()){ShowLoading(true,'Please wait. Your inquiry is being submitted...');};}");

			// Show email?
			// trEmail.Visible = !Request.IsAuthenticated;

			// Save the referrer URL
			if (!IsPostBack)
			{
				// Default it to feedback.
				string mode = "F";

				drpCountry.DataTextField = "Name";
				drpCountry.DataValueField = "CountryId";
				drpCountry.DataSource = WebCache.GetCountries ();
				drpCountry.DataBind ();

				drpCountry.Items.Insert (0, new ListItem ("United States", Constants.COUNTRY_CODE_UNITED_STATES));
				drpCountry.Items.Insert (0, new ListItem ("select...", ""));

				if (Request.UrlReferrer != null)
				{
					ViewState ["rurl"] = Request.UrlReferrer.AbsoluteUri.ToString ();
				}
				else
				{
					if (Request.QueryString["rurl"] != null && Request.QueryString["rurl"].ToString() != string.Empty)
					{
						ViewState ["rurl"] = Server.UrlDecode(Request.QueryString["rurl"].ToString());
					}
					else
					{
						ViewState ["rurl"] = "Unknown";
					}
				}

				// Check the mode
				if (Request ["mode"] != null)
				{
					mode = Request ["mode"].ToString ();
				}
			}
		}

		#region Helper Methods
		/// <summary>
		/// Reset the form
		/// </summary>
		private void ResetForm ()
		{
			txtFirstName.Text = "";
			txtLastName.Text = "";
			txtCompanyName.Text = "";
			txtCompanyAddress1.Text = "";
			txtCompanyAddress2.Text = "";
			txtCity.Text = "";
			txtState.Text = "";
			txtEmail.Text = "";
			txtPhone.Text = "";
			txtPostalCode.Text = "";
			txtPhone.Text = "";
			txtAnnualRevenue.Text = "";
			txtBudget.Text = "";
			txtCurrentMembers.Text = "";
			txtProjectedMembers.Text = "";
			txtWebsiteUrl.Text = "";
			txtProjectName.Text = "";
			txtProjectDescription.Text = "";
			drpCountry.SelectedValue = "";
			drpHistory.SelectedValue = "";
			drpStartDate.SelectedValue = "";
			drpClientPlatform.SelectedValue = "";
			drpProjectStatus.SelectedValue = "";

		}

		#endregion

		/// <summary>
		/// btnSubmit_Click
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnSubmit_Click (object sender, ImageClickEventArgs e)
		{
			// Server validation
			Page.Validate ();
			if (!Page.IsValid) 
			{
				return;
			}

			// Build the Description
			string strComments = "KGP Commercial License Inquiry Form<br><br>" +
				"Contact information<br><br>" +
				"Name:                " + txtFirstName.Text + " " + txtLastName.Text + "<br>" +
				"Email:               " + txtEmail.Text + "<br>" +
				"Phone:               " + txtPhone.Text + "<br>" +
				"<br><br>Company information<br><br>" +
				"Company Name:        " + txtCompanyName.Text + "<br>" +
				"Company Address:     " + txtCompanyAddress1.Text + "<br>" +
				"                     " + txtCompanyAddress2.Text + "<br>" +
				"City, State:         " + txtCity.Text + ", " + txtState.Text + "<br>" +
				"Postcode, Country:   " + txtPostalCode.Text + ", " + drpCountry.SelectedValue + "<br><br>" +
				"Website URL:         " + txtWebsiteUrl.Text + "<br>" +
				"Annual Revenue:      " + txtAnnualRevenue.Text + "<br>" +
				"History:             " + drpHistory.SelectedValue + "<br>" +
				"<br><br>Project information<br><br>" +
                "Project Name:        " + txtProjectName.Text + "<br>" +
				"Project Description: " + txtProjectDescription.Text + "<br>" +
				"Start Date:          " + drpStartDate.SelectedValue + "<br>" +
				"Client Platform:     " + drpClientPlatform.SelectedValue + "<br>" +
				"Project Budget:      " + txtBudget.Text + "<br>" +
				"Project Status:      " + drpProjectStatus.SelectedValue + "<br>" +
				"Current Members:     " + txtCurrentMembers.Text + "<br>" +
				"Projected Members:   " + txtProjectedMembers.Text + "<br><br>" +
				"Linked from: " + ViewState ["rurl"].ToString () + "<br>" +
				"Kaneva Web Version reported from: " + KanevaGlobals.Version () + "<br>";

			MailUtility.SendEmail (KanevaGlobals.FromEmail, "eliteadmin@kaneva.com", "KGP Partner Inquiry", strComments, true, false, 2);

			// It was a success.
			ResetForm ();
			string scriptString = "<script language=JavaScript>";
			scriptString += "alert ('Thank you for contacting us about the Kaneva Game Platform commercial license. We will review your information and contact you with more details.');";
			scriptString += "</script>";

            if (!ClientScript.IsClientScriptBlockRegistered(GetType (), "Thankyou"))
			{
                ClientScript.RegisterStartupScript(GetType(), "Thankyou", scriptString);
			}
		}

		
		#region Declarations

		protected ImageButton btnSubmit;
		protected DropDownList drpHistory, drpCountry, drpStartDate, drpClientPlatform, drpProjectStatus;
		protected Label	lblTitle;

		protected TextBox txtFirstName, txtLastName, txtEmail, txtPhone;
		protected TextBox txtCompanyName, txtCompanyAddress1, txtCompanyAddress2, txtCity, txtState, txtPostalCode;
		protected TextBox txtAnnualRevenue, txtWebsiteUrl;
		protected TextBox txtProjectName, txtProjectDescription, txtBudget, txtCurrentMembers, txtProjectedMembers;

		// Logger
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
