///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva.framework.widgets;
using log4net;

namespace KlausEnt.KEP.Kaneva
{
    /// <summary>
    /// Summary description for displayImage.
    /// </summary>
    public class displayImage : System.Web.UI.Page
    {
        private void Page_Load (object sender, System.EventArgs e)
        {
            DataRow drImageData = null;
            System.Drawing.Image imgOriginal = null;

            int MaxWidth = 0;
            int MaxHeight = 0;
            int NewWidth = 0;
            int NewHeight = 0;

            bool bResize = false;

            string imageType = "image/gif";

            // Did they want it resized?
            if (Request ["Resize"] != null)
            {
                bResize = (Request ["Resize"].ToString ().Equals ("Y"))? true: false;
            }

            // Get the max width passed in
            if (Request ["MaxWidth"] != null)
            {
                MaxWidth = Convert.ToInt32 (Request ["MaxWidth"]);
            }

            // Get the max Height passed in
            if (Request ["MaxHeight"] != null)
            {
                MaxHeight = Convert.ToInt32 (Request ["MaxHeight"]);
            }

            if (Request ["assetId"] != null)
            {
                // It is an asset image
                drImageData = StoreUtility.GetAssetImage (Convert.ToInt32 (Request ["assetId"]));
                
				// Special case for pictures
				if (Convert.ToInt32 (drImageData ["asset_type_id"]).Equals ((int) Constants.eASSET_TYPE.PICTURE))
				{   
					string imagePath = drImageData["target_dir"].ToString().Replace("/","\\") + "\\" + drImageData["content_extension"].ToString();
	                
					// Make sure it is a valid image
					if (StoreUtility.IsImageContent (drImageData["content_extension"].ToString()) && System.IO.File.Exists (imagePath))
					{
						// Looks good so far
					}
					else
					{
						imagePath = Request.PhysicalApplicationPath + DEFAULT_ASSET_IMAGE;
					}

					if (!GetImageFromFile (ref imgOriginal, bResize, MaxWidth, MaxHeight, imagePath, Convert.ToInt32 (Request ["assetId"])))
					{
						return;
					}

					// output the actual image
					if (System.IO.Path.GetExtension (imagePath).ToUpper ().Equals (".GIF"))
					{
						imageType = "image/gif";
					}
					else
					{
						imageType = "image/jpeg";
					}
				}
				else
				{
					// Show either the uploaded thumbnail or the default asset image
					string imagePath = drImageData ["image_path"].ToString ();
	                
					// Show default image?
					if (drImageData ["image_path"].Equals (DBNull.Value) || imagePath.ToString ().Length.Equals (0) || !System.IO.File.Exists (imagePath))
					{
						if (!Convert.ToInt32 (drImageData ["asset_type_id"]).Equals ((int) Constants.eASSET_TYPE.MUSIC))
						{
							imagePath = Request.PhysicalApplicationPath + DEFAULT_ASSET_IMAGE;
						}
						else
						{
							imagePath = Request.PhysicalApplicationPath + DEFAULT_AUDIO_IMAGE;
						}

						
					}

					if (!GetImageFromFile (ref imgOriginal, bResize, MaxWidth, MaxHeight, imagePath))
					{
						return;
					}

					imageType = drImageData ["image_type"].ToString ();
				}
            }
            else if (Request ["gameId"] != null)
            {
                drImageData = StoreUtility.GetGameIconImage (Convert.ToInt32 (Request ["gameId"]));

                Response.ContentType = "image/x-icon";

                if  (!GetImage (drImageData, ref imgOriginal, bResize, MaxWidth, MaxHeight, "icon_image_data"))
                {
                    return;
                }

                imgOriginal.Dispose();

                return;
            }
            else
            {
				m_logger.Error ("No supported image type passed in");

                // No type passed in
                return;
            }
            
            // Max height, Max Width
            NewWidth = imgOriginal.Width;
            NewHeight = imgOriginal.Height;

            if (MaxWidth > 0 && NewWidth > MaxWidth)
            {
                // Resize to max width
                NewWidth = MaxWidth;
                NewHeight = imgOriginal.Height * NewWidth / imgOriginal.Width;      
            }

            if (MaxHeight > 0 && NewHeight > MaxHeight)
            {
                // Resize
                NewHeight = MaxHeight;
                NewWidth = imgOriginal.Width * NewHeight / imgOriginal.Height;
            }

            // Shoot out the image
            if (imageType.ToUpper ().Equals ("IMAGE/GIF"))
            {
				Response.ContentType = "image/gif";
				System.Drawing.Image imgOutput = imgOriginal.GetThumbnailImage (NewWidth, NewHeight, null, new System.IntPtr ());
				imgOutput.Save (Response.OutputStream, ImageFormat.Jpeg); 
				imgOutput.Dispose ();

////				Bitmap loBMP = new Bitmap (Request.PhysicalApplicationPath + DEFAULT_MEMBER_CHANNEL_IMAGE_MALE);
////				ImageFormat loFormat = loBMP.RawFormat;
////				
////				System.Drawing.Bitmap bmpOut = null;
////				bmpOut = new Bitmap (NewWidth, NewHeight);
////
////				Graphics g = Graphics.FromImage (bmpOut);
////				g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
////				g.FillRectangle ( Brushes.White, 0, 0 , NewWidth, NewHeight);
////				g.DrawImage (loBMP, 0 ,0 , NewWidth, NewHeight);
////				loBMP.Dispose ();
////
////				bmpOut.Save (Response.OutputStream, ImageFormat.Gif); 
////				bmpOut.Dispose ();

				

//				Response.ContentType = "image/gif";
//				//System.Drawing.Image imgOutput = imgOriginal.GetThumbnailImage (NewWidth, NewHeight, null, new System.IntPtr ());
//
//				Bitmap imgOutput = new Bitmap (NewWidth, NewHeight);
//				Graphics g = Graphics.FromImage (imgOutput);
//				g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
//				g.FillRectangle( Brushes.White,0,0,NewWidth, NewHeight);
//				g.DrawImage (imgOriginal, 0, 0, NewWidth, NewHeight);
//
//				imgOutput.Save (Response.OutputStream, ImageFormat.Gif); 
//				imgOutput.Dispose ();
            }
            else if (imageType.ToUpper ().Equals ("IMAGE/PNG"))
			{
                Response.ContentType = "image/jpeg";
				System.Drawing.Image imgOutput = imgOriginal.GetThumbnailImage (NewWidth, NewHeight, null, new System.IntPtr ());
				imgOutput.Save (Response.OutputStream, ImageFormat.Jpeg); 
				imgOutput.Dispose ();
			}
            else
            {
				Bitmap imgOutput = new Bitmap( imgOriginal, NewWidth, NewHeight );
                Response.ContentType = "image/jpeg";
				imgOutput.Save (Response.OutputStream, imgOriginal.RawFormat); 
				imgOutput.Dispose ();
            }

            imgOriginal.Dispose ();
        }

		/// <summary>
		/// GetImageFromFile
		/// </summary>
		private bool GetImageFromFile (ref System.Drawing.Image imgOriginal, bool bResize, int MaxWidth, int MaxHeight, string imagePath)
		{
			return GetImageFromFile (ref imgOriginal, bResize, MaxWidth, MaxHeight, imagePath, 0);
		}

		/// <summary>
		/// GetImageFromFile
		/// </summary>
		private bool GetImageFromFile (ref System.Drawing.Image imgOriginal, bool bResize, int MaxWidth, int MaxHeight, string imagePath, int assetId)
		{
			return GetImageFromFile (ref imgOriginal, bResize, MaxWidth, MaxHeight, imagePath, assetId, "" );
		}

		/// <summary>
		/// GetImageFromFile
		/// </summary>
		private bool GetImageFromFile (ref System.Drawing.Image imgOriginal, bool bResize, int MaxWidth, int MaxHeight, string imagePath, int assetId, string overrideType )
		{
			try
			{
				imgOriginal = System.Drawing.Image.FromFile (imagePath);
			}
			catch (Exception exc)
			{
				m_logger.Error ("Error reading Photo for asset " + assetId.ToString () + ", path is " + imagePath, exc);
				imgOriginal = System.Drawing.Image.FromFile (Request.PhysicalApplicationPath + DEFAULT_ASSET_IMAGE);
			}

			// Shortcut if we don't have to resize send it out now
			if ( !bResize || ( (MaxWidth > 0 && MaxWidth >= imgOriginal.Width) && (MaxHeight > 0 && MaxHeight >= imgOriginal.Height) ) )
			{
				// output the actual image
				if ( overrideType.Equals("GIF") || ( overrideType.Length == 0 && System.IO.Path.GetExtension (imagePath).ToUpper ().Equals (".GIF") ) )
				{
					imgOriginal.Save (Response.OutputStream, System.Drawing.Imaging.ImageFormat.Gif);
				}
				else
				{
					imgOriginal.Save (Response.OutputStream, System.Drawing.Imaging.ImageFormat.Jpeg);
				}

				imgOriginal.Dispose();
				return false;
			}

			return true;
		}

		/// <summary>
		/// Get the image
		/// </summary>
		/// <returns></returns>
		private bool GetImage (DataRow drImageData, ref System.Drawing.Image imgOriginal, bool bResize, int MaxWidth, int MaxHeight, string dbImageColName)
		{
			bool used_default = false;
			return GetImage (drImageData, ref imgOriginal, bResize, MaxWidth, MaxHeight, dbImageColName, ref used_default);
		}

        /// <summary>
        /// Get the image
        /// </summary>
        /// <returns></returns>
        private bool GetImage (DataRow drImageData, ref System.Drawing.Image imgOriginal, bool bResize, int MaxWidth, int MaxHeight, string dbImageColName, ref bool used_default)
        {
            System.IO.MemoryStream msOriginal = null;

            if (drImageData == null || drImageData [dbImageColName].Equals (DBNull.Value))
            {
				string imagePath = "";

                if (Request ["sd"] != null)
                {
                    if (Request ["sd"].Equals ("Y"))
                    {
                       imagePath = DEFAULT_ASSET_IMAGE;
                    }
                }
                else if (Request ["sdi"] != null)
                {
                    if (Request ["sdi"].Equals ("Y"))
                    {
                        imagePath = "\\images\\KGL.ico";
                    }
                }

				if (imagePath != "")
				{
					try
					{
						imgOriginal = System.Drawing.Image.FromFile(Request.PhysicalApplicationPath + imagePath);
						used_default = true;
					}
					catch (Exception exc)
					{
						m_logger.Error ("Error reading Photo, path is " + imagePath, exc);
					}

					if ( !bResize || ( (MaxWidth > 0 && MaxWidth >= imgOriginal.Width) && (MaxHeight > 0 && MaxHeight >= imgOriginal.Height) ) )
					{
						// output the actual image
						if (System.IO.Path.GetExtension (imagePath).ToUpper ().Equals (".GIF"))
						{
							imgOriginal.Save (Response.OutputStream, System.Drawing.Imaging.ImageFormat.Gif);
						}
						else
						{
							imgOriginal.Save (Response.OutputStream, System.Drawing.Imaging.ImageFormat.Jpeg);
						}

						imgOriginal.Dispose();
						return false;
					}
					else
						return true;
				}

                return false;
            }
                
            // Do we need to resize?
            if (!bResize)
            {
                Response.BinaryWrite ((byte []) drImageData [dbImageColName]);
                return false;
            }

            msOriginal = new System.IO.MemoryStream ((byte []) drImageData [dbImageColName]);
            imgOriginal = System.Drawing.Image.FromStream (msOriginal);

            // Is a resize even needed?
            if ((MaxWidth > 0 && imgOriginal.Width <= MaxWidth) && (MaxHeight > 0 && imgOriginal.Height <= MaxHeight))
            {
                Response.BinaryWrite ((byte []) drImageData [dbImageColName]);
                return false;
            }

            return true;
        }
    
        /// <summary>
        /// Thumbnail creation
        /// </summary>
        /// <returns></returns>
        public bool ThumbnailCallBack ()
        {
            return false;
        }

        private void ResizeImage ()
        {
            
        }

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

		private const string DEFAULT_ASSET_IMAGE = "\\images\\KanevaIcon01.gif";
		private const string DEFAULT_MEMBER_CHANNEL_IMAGE_MALE = "\\images\\KanevaIconMale.GIF";
		private const string DEFAULT_MEMBER_CHANNEL_IMAGE_FEMALE = "\\images\\KanevaIconFemale.GIF";
		private const string DEFAULT_MEMBER_CHANNEL_IMAGE_NOTSET = "\\images\\KanevaIconUnknownGender.gif";
		private const string DEFAULT_BROADCAST_CHANNEL_IMAGE = "\\images\\KanevaIconBroadband.gif";
		private const string DEFAULT_AUDIO_IMAGE = "\\images\\KanevaIconAudio.gif";

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
        
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {    
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}
