<%@ Page language="c#" MasterPageFile="~/masterpages/GenericPageTemplate.Master" EnableViewState="True" Codebehind="default.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.defaultHome" %>
<%@ Register TagPrefix="Kaneva" TagName="MKContainer" Src="usercontrols/doodad/ContainerMyKaneva.ascx" %>
<%@ Register TagPrefix="UserControl" TagName="FacebookJS" Src="usercontrols/FacebookJavascriptInclude.ascx" %>
<%@ Register TagPrefix="UserControl" TagName="AccountComplete" Src="usercontrols/AccountCompleteness.ascx" %>

<asp:Content ID="cntProfileHead" runat="server" contentplaceholderid="cph_HeadData" >
    
	<script type="text/javascript" src="jscript/base/base.js"></script>
    <NOSCRIPT><META HTTP-EQUIV="refresh" CONTENT="1" URL="noJavascript.aspx" /></NOSCRIPT>
    <link href="css/myKaneva_NEW.css?v8" type="text/css" rel="stylesheet" />
	<usercontrol:FacebookJS runat="server"></usercontrol:FacebookJS>

</asp:Content>

<asp:Content id="cntBody" runat="server" contentplaceholderid="cph_Body" >


<div id="fb-root"></div>

	<!-- Left Column -->
	<div id="leftRail">
		
		<kaneva:mkcontainer id="MKcontainerProfile" runat="server" ControlToLoad="usercontrols/doodad/UserProfile.ascx" />
		<kaneva:mkcontainer id="MKcontainerMessageSummary" runat="server" ControlToLoad="usercontrols/doodad/MessageSummary.ascx" />
		<kaneva:mkcontainer id="MKcontainerMyTopWorlds" runat="server" ControlToLoad="usercontrols/doodad/MyTopWorlds.ascx" />

	</div>
											 
	<!-- Center Column -->
	<div id="centerRail">
		
        <kaneva:mkcontainer id="MKcontainerTopWorlds" runat="server" ControlToLoad="usercontrols/doodad/TopWorlds.ascx" />
		<kaneva:mkcontainer id="MKcontainerAccountComplete" runat="server" ControlToLoad="usercontrols/AccountCompleteness.ascx" />
		<kaneva:mkcontainer id="MKcontainerBlastsActive" runat="server" ControlToLoad="mykaneva/blast/BlastsActive.ascx" />
												 
	</div>
	
	<!-- Right Column -->
	<div id="rightRail">
	
		<kaneva:mkcontainer id="MKcontainerFriendInviter" runat="server" ControlToLoad="usercontrols/doodad/FriendInviter.ascx" />
		<kaneva:mkcontainer id="MKcontainerEventsList" runat="server" ControlToLoad="usercontrols/doodad/EventsList.ascx" />
		<kaneva:mkcontainer id="MKcontainerPasses" runat="server" ControlToLoad="usercontrols/doodad/Passes.ascx" />
		<kaneva:mkcontainer id="MKcontainerNotifications" runat="server" ControlToLoad="usercontrols/doodad/Notifications.ascx" />
	
	</div>
	<script type="text/javascript" src="jscript/scriptaculous/scriptaculous.js?load=effects,builder"></script>

</asp:Content>

