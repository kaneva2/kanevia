///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KlausEnt.KEP.Kaneva
{
    public partial class download : NoBorderPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // If not a windows box, go directly to mykaneva page
            if (!Request.Browser.Platform.ToLower().Contains("win"))
            {
                Response.Redirect(ResolveUrl("~/default.aspx"));
            }

            if (Request.Browser.Browser.ToLower().Contains("firefox"))
            {
                imgDownload.Src = ResolveUrl("~/images/download/firefox.png");
            }
            else if (Request.Browser.Browser.ToLower().Contains("chrome"))
            {
                imgDownload.Src = ResolveUrl("~/images/download/chrome.png");
            }
        }
    }
}