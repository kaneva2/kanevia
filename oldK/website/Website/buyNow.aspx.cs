///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using log4net;

namespace KlausEnt.KEP.Kaneva
{
    public partial class buyNow : NoBorderPage
    {
        #region Page Load

        protected void Page_Load (object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // Get values from querystring
                GetRequestParams ();
                
                ConfigureBuy ();

                litJS.Text = "<script type=\"text/javascript\" src=\"" + ResolveUrl ("~/jscript/jquery/jquery-1.8.2.min.js") + "\">";
            }
        }

        #endregion Page Load

        #region Helper Methods

        private void ConfigureBuy ()
        {
            string result = null;

            try
            {
                if (GlobalId > 0)
                {
                    WOKItem item = GetShoppingFacade.GetItem (GlobalId);
                    bool enoughRewards, enoughCredits;
       
                    if (item.GlobalId > 0)
                    {
                        GetUserAbilityToBuy (Convert.ToInt32(item.DesignerPrice), KanevaWebGlobals.CurrentUser.Balances, out enoughRewards, out enoughCredits);

                        // Does user have Always Purchase with set?
                        // Only check always purchase with credits since that's the only way to purchase premium items
                        if (KanevaWebGlobals.CurrentUser.Preference.AlwaysPurchaseWithCurrencyType == Constants.CURR_CREDITS)
                        {
                            // Set to always use credits since premium items can only be bought with credits.
                            result = BuyPremiumItem (Constants.CURR_CREDITS, item, 1);
                            if (result == null)
                            {
                                ShowPage (PageType.Success);
                                return;
                            }
                        }
                        else
                        {
                            if (!enoughCredits)
                            {
                                DisplayBuyCreditsPage (true);
                                return;
                            }
                            else
                            {
                                divItemCost.InnerText = item.DesignerPrice + " credits";
                                btnBuy.CommandArgument = item.GlobalId.ToString();
                            }
                        }
                    }
                    else
                    {
                        // There was a problem retrieving the wok item
                        m_logger.Warn ("Error in BuyNow.  The WOKItem with globalId=" + GlobalId.ToString() + " was not successfully retrieved from db.");
                        result = "We could not retrieve item you want to buy.  Please close dialog and try your purchase again.";
                    }
                }
                else
                {
                    // Not a valid global id
                    m_logger.Warn ("Error in BuyNow.  The global id was not retrieved from querystring.");
                    result = "Invalid Item Id.";
                }
            }
            catch (Exception ex)
            {
                m_logger.Warn ("Error in BuyNow trying to purchase Premium Item.", ex);
                result = "We experienced a problem trying to purchase this item.  Please close dialog and try your purchase again.";
            }
        
            if (result != null)
            {
                divBuyNow.InnerText = result;
            }

            ShowPage (PageType.BuyNow);
        }

        private void CreateParentUpdateJavascript ()
        {
            string credits = KanevaWebGlobals.CurrentUser.Balances.Credits > 0 ? KanevaWebGlobals.CurrentUser.Balances.Credits.ToString ("#,###") : "0";
            string rewards = KanevaWebGlobals.CurrentUser.Balances.Rewards > 0 ? KanevaWebGlobals.CurrentUser.Balances.Rewards.ToString ("#,###") : "0";

            aClose.HRef = "javascript:amtHasChanged=true;Update('" + credits + "','" + rewards + "');";

            // Set JS values used if facebox closed with Close (X) button
            _JSVariables = "amtHasChanged=true;totCredits='" + credits + "';totRewards='" + rewards + "';";

            litCloseBtnClientId.Text = aClose.ClientID;

            aClose.Attributes.Add ("onfocus", _JSVariables);
        }

        private void GetRequestParams ()
        {
            try
            {
                if (Request["communityId"] != null)
                {
                    CommunityId = Convert.ToInt32 (Request["communityId"]);
                }
            }
            catch { }

            try
            {
                if (Request["globalId"] != null)
                {
                    GlobalId = Convert.ToInt32 (Request["globalId"]);
                }
            }
            catch { }
        }

        protected string BuyPremiumItem (string creditType, WOKItem item, int quantity)
        {
            //  Premium items can only be purchased with credits ... for now
            if (creditType.Equals (Constants.CURR_CREDITS))
            {
                if (quantity > 0)
                {
                    int successfulPayment = -1;

                    // Make sure the item is available for purchase
                    if (!item.ItemActive.Equals ((int) WOKItem.ItemActiveStates.Deleted))
                    {
                        uint purchasePrice = 0;
                        if (item.ApprovalStatus == WOKItem.ItemApprovalStatus.Approved)
                        {
                            purchasePrice = item.DesignerPrice;
                        }

                        // There is no kaneva price for Premium items. There is only the price that
                        // the designer adds onto the time.  Also, the order total and designer
                        // commission will always be the same.  Kaneva takes it's 30% when the 3D App
                        // developer redeems the credits that were used to purchase the Premium Items
                        UInt32 iOrderTotal = Convert.ToUInt32 (purchasePrice * quantity);

                        if (Convert.ToUInt64 (KanevaWebGlobals.CurrentUser.Balances.Credits) >= Convert.ToUInt64 (iOrderTotal))
                        {
                            try
                            {
                                successfulPayment = GetUserFacade.AdjustUserBalanceWithDetails (KanevaWebGlobals.CurrentUser.UserId, 
                                    Constants.CURR_CREDITS, -Convert.ToDouble (iOrderTotal), Constants.CASH_TT_PREMIUM_ITEM, item.GlobalId, quantity);
                            }
                            catch (Exception)
                            {
                                return "Not enough credits to purchase this item";
                            }

                            if (successfulPayment == 0)
                            {
                                GetUserFacade.AddItemToGameInventoriesSync (item.GameId, KanevaWebGlobals.CurrentUser.UserId,
                                    Constants.WOK_INVENTORY_TYPE_PERSONAL, item.GlobalId, quantity, 256);
                         
                                // Add it to their history
                                if (GetShoppingFacade.AddItemPurchase (item.GlobalId, (int) ItemPurchase.eItemTxnType.PURCHASE,
                                    KanevaWebGlobals.CurrentUser.UserId, item.ItemCreatorId, Convert.ToInt32 (iOrderTotal),
                                    iOrderTotal, Common.GetVisitorIPAddress(), Constants.CURR_CREDITS, quantity, 0) == 0)
                                {
                                    return "An error occurred adding the item you purchased.";
                                }
                                else
                                {
                                    return null;
                                }
                            }
                            else
                            {
                                return "Unknown error occurred when attempting to adjust the user's balance";
                            }
                        }
                        else
                        {
                            return "Not enough credits to purchase this item";
                        }
                    }
                    else
                    {
                        return "This item is not available for purchase";
                    }
                }
                else
                {
                    return "Quantity must be greater than 0";
                }
            }
            else
            {
                return "This item can only be purchased with Credits";
            }
        }

        private void DisplayBuyCreditsPage (bool setHeight)
        {
            divBuyCredits.Style.Add ("display", "block");

            try
            {
                if (IsPostBack)
                {
                    if (setHeight) 
                    { 
                        ScriptManager.RegisterStartupScript (this, GetType (), "SetHeight", _JSVariables + "SetHeight(" + FaceboxHeight.DEFAULT + ");", true);
                    }
                }
            }
            catch { }
        }

        private void GetUserAbilityToBuy (int itemCost, UserBalances ub, out bool canBuyWithRewards, out bool canBuyWithCredits)
        {
            if (ub.Rewards < itemCost)
            {   // Not enough Rewards
                canBuyWithRewards = false;
            }
            else
            { 
                canBuyWithRewards = true;
            }

            if (ub.Credits < itemCost)
            {   // Not enough Credits
                canBuyWithCredits = false;
            }
            else
            {
                canBuyWithCredits = true;
            }
        }

        private void ShowPage (PageType displayPage)
        {
            HideAllPages ();

            switch (displayPage)
            {
                case PageType.BuyNow:
                    divBuyNow.Style.Add ("display","block");
                    return;
                case PageType.BuyCredits:
                    divBuyCredits.Style.Add ("display", "block");
                    return;
                case PageType.Success:
                    divSuccess.Style.Add ("display", "block"); 
                    return;
                case PageType.Error:
                    divError.Style.Add ("display", "block");
                    return;
            }
        }

        private void HideAllPages ()
        {
            divBuyCredits.Style.Add ("display", "none");
            divBuyNow.Style.Add ("display", "none");
            divSuccess.Style.Add ("display", "none");
        }

        #endregion Helper Methods

        #region Event Handlers

        protected void btnBuy_Click (object sender, CommandEventArgs e)
        {
            string result = null;

            if (!Request.IsAuthenticated)
            {
                Response.Redirect (GetLoginURL ());
            }

            if (e.CommandArgument != null && e.CommandArgument.ToString () != "")
            {
                GlobalId = Convert.ToInt32 (e.CommandArgument);   
            }

            if (GlobalId > 0)      //** Do we need to confirm item is in 3dapp and user belongs to 3dapp?
            {
                WOKItem item = GetShoppingFacade.GetItem (GlobalId);
                result = BuyPremiumItem (Constants.CURR_CREDITS, item, 1);
            }
            else
            {
                result = "We encountered a problem trying to find the item you wish to purchase.  Please close dialog and try your purchase again.";
            }

            if (result != null)
            {
                errText.InnerText = result;

                ShowPage (PageType.Error);
            }
            else
            {
                CreateParentUpdateJavascript ();

                ShowPage (PageType.Success);
            }
        }

        protected void btnBuyXtraCredits_Click (object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript (this, GetType (), "Close", "javascript:parent.$j.facebox.close();javascript:parent.location='http://" + KanevaGlobals.SiteName + "//mykaneva/buycredits.aspx';", true);
        }

        #endregion Event Handlers

        #region Properties

        private int GlobalId
        {
            get
            {
                if (ViewState["globalId"] != null)
                {
                    return Convert.ToInt32 (ViewState["globalId"]);
                }
                else
                {
                    return -1;
                }
            }
            set
            {
                ViewState["globalId"] = value;
            }
        }
        private int CommunityId
        {
            get
            {
                if (ViewState["communityId"] != null)
                {
                    return Convert.ToInt32 (ViewState["communityId"]);
                }
                else
                {
                    return -1;
                }
            }
            set
            {
                ViewState["communityId"] = value;
            }
        }
        private int GameId
        {
            get
            {
                if (ViewState["gameId"] != null)
                {
                    return Convert.ToInt32 (ViewState["gameId"]);
                }
                else
                {
                    return -1;
                }
            }
            set
            {
                ViewState["gameId"] = value;
            }
        }

        #endregion Properties

        #region Declerations

        private class FaceboxHeight
        {
            public const int DEFAULT = 200;
            public const int ALWAYS_BUY_WITH = 345;
        }
        private enum PageType
        {
            BuyNow,
            BuyCredits,
            Success,
            Error
        }

        private string _JSVariables = "";
        
        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

        #endregion Declerations
    }
}
