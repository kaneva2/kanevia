///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
	/// <summary>
	/// Summary description for lostPassword.
	/// </summary>
	public class lostPassword : BasePage
	{
		protected lostPassword () 
		{
			Title = "Lost Password";
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
            spnMsg.InnerText = "";
        }

		/// <summary>
		/// btnPassword_Click
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnPassword_Click (object sender, EventArgs e) 
		{
			if (!Page.IsValid) 
			{
				spnMsg.InnerText = "Some required fields are blank.";
				return;
			}

            // Get this user's email and username
			User user = GetUserFacade.GetUserByEmail (Server.HtmlEncode (txtUserName.Value));

            if (user.UserId > 0)
			{
				// Send out link to change password here
				// Send out an email here!!!

                MailUtilityWeb.SendPasswordRequest(user.Username, user.Email, user.KeyValue);

                GetUserFacade.InsertUserLoginIssue (user.UserId, Common.GetVisitorIPAddress(), "User '" + user.Username + "' requested a password email.", "FORGOT_PASSWORD", "WEB");

				spnMsg.InnerHtml = "<span>A message has been sent to your email address on file with Kaneva.</span>";
			}
			else
			{
				spnMsg.InnerText = "No username exists for this email address. Please enter the email address you provided for this account.";
			}			
		}

		protected HtmlInputText txtUserName;
        protected HtmlContainerControl spnMsg;

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
