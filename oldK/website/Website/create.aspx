<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="create.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.create" %>

<style type="text/css">
<!--
@import url("css/new.css");
@import url("css/create.css");
-->
</style>
										

<div id="wrapper">
	<div id="topCap"><img id="Img1" runat="server" src="~/images/create/topCap.gif" border="0" /></div><!-- end topCap-->
	<div id="stage">
		<div id="leadCopy">
			<h3>What do you want to Create?</h3>
			<p>With Kaneva, anything is possible. You can be a fashion designer, game producer, event planner, and more! Imagine what you can do� do what you imagine!</p> 
		</div><!-- end leadCopy-->
		<div id="content">
			<div id="games">
				<h4><a runat="server" href="~/watch/watch.kaneva">GAMES</a></h4>
				<p>Make your own flash games and then upload them to play with others! You can even design your own arcade machines!<br /><br />
				<a runat="server" href="~/watch/watch.kaneva">Click here</a> to see what others have created.</p>
			</div><!-- end games -->
			<div id="fashion">
				<h4><a href="http://shop.kaneva.com/">FASHION & FURNITURE</a></h4>
				<p>Design and sell your own clothes, furniture, and accessories. Promote your one-of- a-kind designs to a worldwide audience!<br /><br />
				Make your own at <a href="http://shop.kaneva.com/">shop.kaneva.com</a>!</p>
			</div><!-- end fashion -->
			<div id="communities">
				<h4><a runat="server" href="~/community/channel.kaneva">COMMUNITIES</a></h4>
				<p>Share your passions, find your cause, and meet new people�create a Kaneva Community. Then build and decorate your <strong>FREE</strong> 3D community hangout!<br /><br />
				<a runat="server" href="~/community/channel.kaneva">Click here</a> to get started!</p>
			</div><!-- end communities -->
		</div><!-- end content-->
		<div id="ads">
			<a runat="server" href="~/community/channel.kaneva" class="nohover"><img runat="server" src="~/images/create/imagecreatead.gif" border="0" /></a>
		</div><!-- end ads -->
		<div id="blog">
			<a href="http://blog.kaneva.com/"><img runat="server" src="~/images/create/adBlog.gif" border="0" /></a>
		</div><!-- end blog-->
	</div><!-- end stage -->
	<div id="bottomCap"></div><!-- end bottomCap -->
</div><!-- end wrapper -->

