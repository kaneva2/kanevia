<%@ Page language="c#" Codebehind="contactUsMain.aspx.cs" AutoEventWireup="false" Inherits="KlausEnt.KEP.Kaneva.contactUsMain" %>
<%@ Register TagPrefix="Kaneva" TagName="HeaderText" Src="usercontrols/HeaderText.ascx" %>
<Kaneva:HeaderText runat="server" Text="Contact Us"/>

<table cellpadding="10" cellspacing="0" border="0" width="750">
<tr>
	<td>
		<br>
		<table cellpadding="5" cellspacing="0" border="0" width="730">
		<tr>
			<td colspan="5" align="center" class="assetLink">
			<img runat="server" src="~/images/phonechick.jpg"/><br><br>
			Thank your for your interest in Kaneva.<br>Please Let us know which issue matches your interest or concern:<br><br></td>
		</tr>
	
			<tr>
				<td width="20">&nbsp;</td>
				<td valign="top" class="showingCount" style="vertical-align:top;" width="40%">
					<span class="orangeHeader2">1. </span><a runat="server" href="~/suggestions.aspx?mode=WB&category=KANEVA%20Web%20Site" class="bodyText" ID="A1">A product or web page contains problems </a><br><br>
					<span class="orangeHeader2">2. </span><a runat="server" href="~/suggestions.aspx?mode=WB&category=KANEVA%20Web%20Site" class="bodyText" ID="A2">Request help with your Kaneva experience</a><br><br>
					<span class="orangeHeader2">3. </span><a runat="server" href="suggestions.aspx?mode=F&category=Feedback&subtab=sug" class="bodyText" ID="A3">Suggest a new idea or feature</a><br><br>
					<span class="orangeHeader2">4. </span><a runat="server" href="~/overview/copyright.aspx" class="bodyText">Let us know of a legal matter</a><br>
		
				</td>
				<td width="20">&nbsp;</td>
				<td valign="top" class="showingCount" style="vertical-align:top;" width="40%">
									<span class="orangeHeader2">5. </span><a runat="server" href="~/suggestions.aspx?mode=B" class="bodyText" ID="A5">Let us know of a business matter</a><br><br>	
					<span class="orangeHeader2">6. </span><a runat="server" href="~/suggestions.aspx?mode=F&category=Feedback&subtab=sug" class="bodyText" ID="A6">Send us praise</a><br><br>
					<span class="orangeHeader2">7. </span><a runat="server" href="~/community/Careers.kaneva" class="bodyText" ID="A7">Careers at Kaneva</a><br><br>
<span class="orangeHeader2">8. </span><a runat="server" href="~/overview/partners.aspx" class="bodyText" ID="A4">Kaneva Partnerships</a><br>
							
				</td>				
			</tr>		
			<tr>
				<td colspan="5" align="center"><br><hr width="90%" noshade size="1px" style="color: #cccccc;"></td>
			</tr>
			</table>
		</td>
	</tr>
</table>	