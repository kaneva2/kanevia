<%@ Page language="c#" MasterPageFile="~/masterpages/GenericLandingPageTemplate.Master" Codebehind="lostPasswordChange.aspx.cs" EnableViewState="False" AutoEventWireup="true" Inherits="KlausEnt.KEP.Kaneva.lostPasswordChange" %>


<asp:Content ID="cntCommunityMetaData" runat="server" contentplaceholderid="cphHeader" >
    <NOSCRIPT><META HTTP-EQUIV="refresh" CONTENT="1" URL="noJavascript.aspx" /></NOSCRIPT>
    <link href="css/new.css?v1" type="text/css" rel="stylesheet">
    <link href="css/base/base_new1.css" type="text/css" rel="stylesheet" />
    <link href="css/base/buttons_new.css?v1" type="text/css" rel="stylesheet">
    
    <style>
    .pageBody {padding:0 0 20px 0;background:#fff;border:1px solid #ccc;border-top:none;}
    .datacontainer {width:700px;margin:0 auto;padding-top:50px;}
    p {margin:20px 0 0 0;line-height:16px;display:block;}
    div.msg {height:24px;color:#ee2d2a;}
    div.msg span {color:#333;font-weight:bold;}
    h6 {font-size:22px;font-family:verdana, arial, sans-serif;padding:0;margin:0;font-weight:bold;}
    .btnlarge {margin:30px 12px 0;}
    .reset {margin:30px auto 80px auto;}
    .errBox {width:930px;border:none;padding-left:30px;}
    a:link {font-size:12px;}
    </style>

</asp:Content>


<asp:Content ID="cntKanevaCommunityBody" runat="server" contentplaceholderid="cphBody" >
    
    <asp:validationsummary cssclass="errBox black" id="valSum" runat="server" showmessagebox="False" showsummary="true"
	    displaymode="BulletList" headertext="Please correct the following error(s):" forecolor="black"></asp:validationsummary>
    <asp:customvalidator id="cvBlank" runat="server" display="None" enableclientscript="False"></asp:customvalidator>

    <div class="datacontainer">
        <h6>Reset Password</h6>
        <p>Please enter in your e-mail and your new password in the boxes below.</p>

        <asp:Panel ID="Panel1" runat="server" DefaultButton="lbChangePassword">
        <table cellpadding="5" cellspacing="0" border="0" width="380" class="wizform reset">
		    <tbody>
            <tr>
		        <td align="right">Email:</td>
		        <td valign="middle">
			        <asp:TextBox ID="txtEmail" cssclass="biginput"  Width="220" MaxLength="80" runat="server"/>
			        <asp:RequiredFieldValidator ID="rfUsername" enableclientscript="false" ControlToValidate="txtEmail" Text="*" ErrorMessage="Email is required." Display="static" runat="server"/>
		            <asp:regularexpressionvalidator id="revEmail" controltovalidate="txtEmail" text="*" display="Static" errormessage="Invalid email address." enableclientscript="false" runat="server" />
                </td>
            </tr>
		    <tr>
		        <td align="right">New Password:</td>
		        <td>
			        <asp:TextBox ID="txtPassword" TextMode="Password" cssclass="biginput" Width="220" MaxLength="20" runat="server"/>
			        <asp:RequiredFieldValidator ID="rfPassword" enableclientscript="false" ControlToValidate="txtPassword" Text="*" ErrorMessage="Password is a required field." Display="Dynamic" runat="server"/>
			        <asp:regularexpressionvalidator id="revPassword" runat="server" controltovalidate="txtPassword" text="" errormessage="Passwords should be 4-20 characters and a combination of upper and lower-case letters, numbers, and underscores (_) only. Do not use spaces."
			            display="none" enableclientscript="false"></asp:regularexpressionvalidator>
		        </td>
            </tr>
		    <tr>
		        <td align="right">Confirm Password:</td>
		        <td>
        			<asp:TextBox ID="txtConfirmPassword" TextMode="Password" cssclass="biginput" Width="220" MaxLength="20" runat="server"></asp:TextBox>
			        <asp:RequiredFieldValidator ID="rfConfirmPassword" ControlToValidate="txtConfirmPassword" Text="*" ErrorMessage="Confirm Password is a required field." Display="Dynamic" runat="server"/>
                    <asp:CompareValidator Text="*" id="cmpPassword" ControlToValidate="txtConfirmPassword" ControlToCompare="txtPassword" Type="String" Operator="Equal" ErrorMessage="Passwords do not match." runat="server"/>
		        </td>
            </tr>
		    <tr>
		        <td></td>
		        <td align="right">
		            <asp:LinkButton class="btnlarge grey" id="lbChangePassword" runat="server" text="Change Password" causesvalidation="false" onClick="btnChangePassword_Click"></asp:LinkButton>
                </td>
            </tr>
            </tbody>
        </table>
        </asp:Panel>

    </div>
</asp:Content>