///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Kaneva.BusinessLayer.BusinessObjects;


namespace KlausEnt.KEP.Kaneva
{
    public partial class DefaultWebForm : StoreBasePage
    {
        protected DefaultWebForm () 
		{
			Title = "Home";
		}

        private void Page_Load (object sender, System.EventArgs e)
        {
            System.Web.HttpCookie aCookie;

            // Is user logged in?
            if (!Request.IsAuthenticated)
            {
                Response.Redirect ("~/free-virtual-world.kaneva");
            }

            // If they have been banned, log them out!
            if (Current_User.StatusId.Equals ((int) Constants.eUSER_STATUS.LOCKED) || Current_User.StatusId.Equals ((int) Constants.eUSER_STATUS.LOCKEDVALIDATED))
            {
                for (int i = Request.Cookies.Count - 1; i != 0; i--)
                {
                    aCookie = Request.Cookies[i];
                    aCookie.Expires = DateTime.Now.AddDays (-1);
                    Response.Cookies.Add (aCookie);
                }

                FormsAuthentication.SignOut ();
                Session.Abandon ();
                Response.Redirect ("~/free-virtual-world.kaneva");
            }

            // Set Nav
            HeaderNav.ActiveTab = KlausEnt.KEP.Kaneva.Header.TAB.HOME;
            HeaderNav.MyKanevaNav.ActiveTab = NavMyKaneva.TAB.NONE;
            HeaderNav.SetNavVisible (HeaderNav.MyKanevaNav);

            // If not loaded, load prototype

        }


        #region Declerations

        // private 
        private User Current_User = KanevaWebGlobals.CurrentUser;

        #endregion Declerations
    }
}
