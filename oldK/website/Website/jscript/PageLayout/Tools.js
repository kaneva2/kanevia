

// add an event handler, not replace one
function addLoadEvent(func)
{
	var oldonload = window.onload;
	if (typeof (window.onload) != 'function')
	{
		window.onload = func;
	}
	else
	{
		window.onload = function()
		{
			oldonload();
			func();
		}
	}
}

function addUnLoadEvent(func)
{
	var oldonunload = window.onunload;
	if (typeof (window.onunload) != 'function')
	{
		window.onunload = func;
	}
	else
	{
		window.onunload = function()
		{
			oldonunload();
			func();
		}
	}
}



function SwapNodes(src, dest)
{
	if (src.swapNode)
	{
		return src.swapNode(dest);
	}
	else
	{
		var objSrcParent	= src.parentNode;
		var objDestParent	= dest.parentNode;
		var nextSrcNode		= src.nextSibling;
		var nextDestNode	= dest.nextSibling;
		objDestParent.insertBefore(objSrcParent.removeChild( src ), nextDestNode);
		objSrcParent.insertBefore(objDestParent.removeChild( dest ), nextSrcNode);
	}
}

function GenUniqueID()
{
	return document.uniqueID ? document.uniqueID : "rand_"+Math.round(Math.random()*10000000000);
}
