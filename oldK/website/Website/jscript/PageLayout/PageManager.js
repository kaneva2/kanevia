﻿// JScript File

function PageManager(defStoreEl, delStoreEl, pageNumberEl, selectAllEl, actionEl)
{
    this.defStoreEl = $(defStoreEl);
    this.delStoreEl = $(delStoreEl);
    this.pageNumberEl = $(pageNumberEl);
    this.selectAllEl = $(selectAllEl);
    this.actionEl = $(actionEl);
    this.pages = new Array();
}

PageManager.prototype.SelectAll = PageManager_SelectAll;
PageManager.prototype.GetSelected = PageManager_GetSelected;
PageManager.prototype.DoAction = PageManager_DoAction;
PageManager.prototype.ShowError = PageManager_ShowError;
PageManager.prototype.DeletePages = PageManager_DeletePages;
PageManager.prototype.DeletePage = PageManager_DeletePage;
PageManager.prototype.OpenEditor = PageManager_OpenEditor;
PageManager.prototype.CloseEditor = PageManager_CloseEditor;
PageManager.prototype.Validate = function() {};
PageManager.prototype.ChangePermission = PageManager_ChangePermission;
PageManager.prototype.ChangePermissionNew = PageManager_ChangePermissionNew;
PageManager.prototype.AddPage = PageManager_AddPage;
PageManager.prototype.UpdateView = PageManager_UpdateView;
PageManager.prototype.SavePositions = PageManager_SavePositions;
PageManager.prototype.DownPage = PageManager_DownPage;
PageManager.prototype.UpPage = PageManager_UpPage;
PageManager.prototype.IndexOfPageByItemId = PageManager_IndexOfPageByItemId;
PageManager.prototype.IndexOfPageByPageId = PageManager_IndexOfPageByPageId;
PageManager.prototype.SwapPages = PageManager_SwapPages;

function Page(pageID, itemID)
{
    this.pageID = pageID;
    this.itemID = itemID;
}

function PageManager_AddPage(pageID, itemID)
{
    this.pages.push(new Page(pageID, itemID));
}

function PageManager_UpdateView()
{
    var i;
    for(i = 0; i < this.pages.length; i++)
    {
        $(this.pages[i].itemID+"_imgUpArrow").style.display = (i == 0 || i == 1) ? "none" : "";
        $(this.pages[i].itemID+"_imgDownArrow").style.display = ( (i == 0) || (i == this.pages.length-1) ) ? "none" : "";
        $(this.pages[i].itemID+"_tdProperties").style.backgroundColor = i%2 ? "#FAFAFA" : "#FFFFFF";
    }
}

function PageManager_SavePositions(hfPositions)
{
    var i;
    hfPositions = $(hfPositions)
    hfPositions.value = "";
    for(i = 0; i < this.pages.length; i++)
    {
        hfPositions.value += this.pages[i].pageID + "_";
    }
}

function PageManager_SelectAll(check)
{
	var i; var f;
	var j; var c;
	f = document.getElementsByTagName( "input" );
	
	for(j = 0; j < f.length; ++j)
	{
		c = f[j];
		if( c.type.toLowerCase() == 'checkbox' )
		{
			if (c.getAttribute("pageId") != null)
            {
                c.checked = check;
		    }
		}
	}
}
function PageManager_GetSelected()
{
	var result = new Array();
	var f = document.getElementsByTagName( "input" );
	var j;
	for(j = 0; j < f.length; ++j)
	{
		if( f[j].type.toLowerCase() == 'checkbox' )
		{
			if (f[j].getAttribute("pageId") && f[j].checked)
            {
                result.push(f[j]);
		    }
		}
	}
	return result;
}

function PageManager_DoAction(action)
{
    switch(action)
    {
       case "delete":
            this.DeletePages(this.GetSelected());
            break;
    }   
    this.SelectAll(false);
    this.selectAllEl.checked = false;
    this.actionEl.selectedIndex = 0;
}
function PageManager_ShowError(error)
{
    alert(error);
}

function PageManager_DeletePages(pagesControls)
{
    var i;
    for(i=0; i < pagesControls.length; i++)
    {
        if (this.defStoreEl.value == pagesControls[i].getAttribute("pageId"))
        {
            this.ShowError("Can't delete default page.");
            return;
        }
    }
    for(i=0; i < pagesControls.length; i++)
    {
        this.delStoreEl.value += pagesControls[i].getAttribute("pageId") + "_";
        Element.hide(pagesControls[i].getAttribute("containerId"));
        this.DeletePage(this.IndexOfPageByPageId(pagesControls[i].getAttribute("pageId")));
    }
    this.pageNumberEl.innerHTML = parseInt(this.pageNumberEl.innerHTML) - i;
//    this.UpdateView();
}
function PageManager_DeletePage(index)
{
    for(var n=index; n<this.pages.length; n++)
    {
        this.pages[n] = this.pages[n+1];
    }
    this.pages.length--;
}
function PageManager_OpenEditor(pageId, itemId, title, access, groupId)
{
    this.CloseEditor();
    var accessDDL = $(itemId + "_ddlAccess");
    var groupDDL = $(itemId + "_ddlGroup");
    $(itemId + "_tbName").value = title;
    accessDDL.value = access;
    groupDDL.value = groupId;
    
    // 0 = public, 2 = private
    $(itemId + "_lbGroup").disabled = groupDDL.disabled = ( accessDDL.value == 0 || accessDDL.value == 2 );

    this.currentEditPanel = $(itemId + "_tdEditPanel");
    this.currentPropertiesPanel = $(itemId + "_tdProperties");
    
    Element.toggle($(itemId + "_tdEditPanel"), $(itemId + "_tdProperties"));
}
function PageManager_CloseEditor()
{
    if (this.currentEditPanel)
        Element.hide(this.currentEditPanel);
    if (this.currentPropertiesPanel)    
        Element.show(this.currentPropertiesPanel);
    this.currentEditPanel = null;
    this.currentPropertiesPanel = null;
}
function PageManager_ChangePermission(itemId)
{
    var accessDDL = $(itemId + "_ddlAccess");
    $(itemId+"_lbGroup").disabled = $(itemId+"_ddlGroup").disabled = (accessDDL.value == 0 || accessDDL.value == 2);
    if (accessDDL.value == 0 || accessDDL.value == 2)
        $(itemId+"_ddlGroup").value = 0;
}

function PageManager_ChangePermissionNew()
{
    var accessDDL = $("ddlNewAccess");
    $("lbNewGroup").disabled = $("ddlNewGroup").disabled = (accessDDL.value == 0 || accessDDL.value == 2);
    if (accessDDL.value == 0 || accessDDL.value == 2)
        $("ddlNewGroup").value = 0;
}

function PageManager_IndexOfPageByItemId(itemId)
{
    var i;
    for (i = 0; i < this.pages.length ;i++)
    {
        if (this.pages[i].itemID == itemId)
            return i;
    }
    return -1;
}
function PageManager_IndexOfPageByPageId(pageId)
{
    var i;
    for (i = 0; i < this.pages.length ;i++)
    {
        if (this.pages[i].pageID == pageId)
            return i;
    }
    return -1;
}

function PageManager_SwapPages(i1, i2)
{
    var temp = this.pages[i1];
    this.pages[i1] = this.pages[i2];
    this.pages[i2] = temp;
    SwapNodes($(this.pages[i2].itemID+"_trPageRow"), $(this.pages[i1].itemID+"_trPageRow"));
    this.UpdateView();
}

function PageManager_DownPage(itemId)
{
    var pageIndex = this.IndexOfPageByItemId(itemId);
    if (pageIndex == -1) return;
    if (pageIndex < this.pages.length - 1)
        this.SwapPages(pageIndex, pageIndex+1);
}
function PageManager_UpPage(itemId)
{
    var pageIndex = this.IndexOfPageByItemId(itemId);
    if (pageIndex == -1) return;
    
    var temp;
    if (pageIndex > 0)
        this.SwapPages(pageIndex, pageIndex-1);
}
