
// Color Picker Script from Flooble.com
// For more information, visit
//	http://www.flooble.com/scripts/colorpicker.php
// Copyright 2003 Animus Pactum Consulting inc.
//---------------------------------------------------------
     var appendedStr = "AppClrPickerStr"; //hack prefix input field with this gives us the hyperlink
     var perline = 9;
     var divSet = false;
     var curId;
     var colorLevels = Array('0', '3', '6', '9', 'C', 'F');
     var colorArray = Array();
     var ie = false;
     var nocolor = 'none';
	 if (document.all) { ie = true; nocolor = ''; }
	 function getObj(id) {
		if (ie) { return document.all[id]; }
		else {	return document.getElementById(id);	}
	 }

     function addColor(r, g, b) {
     	var red = colorLevels[r];
     	var green = colorLevels[g];
     	var blue = colorLevels[b];
     	addColorValue(red, green, blue);
     }

     function addColorValue(r, g, b) {
     	colorArray[colorArray.length] = '#' + r + r + g + g + b + b;
     }

     function setColor(color) {
     	var link = getObj(curId);
     	var field = getObj(getFieldFromId(curId));
     	var picker = getObj('colorpicker');
     	field.value = color;
     	if (color == 'none') {
	     	link.style.background = nocolor;
	     	link.style.color = nocolor;
	     	color = nocolor;
     	} else {
	     	link.style.background = color;
	     	link.style.color = color;
	    }
	    SetVisibilitySelects("visible",0,0);
     	picker.style.display = 'none';
	    eval(getObj(getFieldFromId(curId)).title);
     }

     function setDiv() {
     	if (!document.createElement) { return; }
        var elemDiv = document.createElement('div');
        if (typeof(elemDiv.innerHTML) != 'string') { return; }
        genColors();
        elemDiv.id = 'colorpicker';
	    elemDiv.style.position = 'absolute';
        elemDiv.style.display = 'none';
        elemDiv.style.border = '#000000 1px solid';
        elemDiv.style.background = '#FFFFFF';
        elemDiv.style.zIndex = 5000;
        //getObj('temp').style.visibility = 'hidden';
        elemDiv.innerHTML = '<span style="font-family:Verdana; font-size:11px;">'
        	+  '(<a href="javascript:setColor(\'\');">None</a>)<br>'
        	+ getColorTable()
        	+ '</span>';

        document.body.appendChild(elemDiv);
        divSet = true;
     }

     function pickColor(id) {
     	if (!divSet) { setDiv(); }
     	var picker = getObj('colorpicker');
		if (id == curId && picker.style.display == 'block') {
			picker.style.display = 'none';
			SetVisibilitySelects("visible",0,0);
			return;
		}
     	curId = id;
     	var thelink = getObj(id);
     	var top,left;
     	top = getAbsoluteOffsetTop(thelink) + 20;
     	left = getAbsoluteOffsetLeft(thelink);
     	picker.style.top = top;
     	picker.style.left = left;
     	SetVisibilitySelects("hidden",top,left);
	    picker.style.display = 'block';
	    
     }

     function setDivMP(obj) 
     {
     if (!document.createElement) { return; }
        var elemDiv = document.createElement('div');
        if (typeof(elemDiv.innerHTML) != 'string') { return; }
        genColors();
        
        elemDiv.id = 'colorpicker';
	    elemDiv.style.position = 'absolute';
        elemDiv.style.display = 'none';
        elemDiv.style.border = '#000000 1px solid';
        elemDiv.style.background = '#FFFFFF';
        elemDiv.style.zIndex = 5000;
        //getObj('temp').style.visibility = 'hidden';
        elemDiv.innerHTML = '<span style="font-family:Verdana; font-size:11px;">'
        	+  '(<a href="javascript:setColor(\'\');">None</a>)<br>'
        	+ getColorTableMP()
        	+ '</span>';

        obj.parentNode.appendChild(elemDiv);
        divSet = true;
     }


     function pickColorMP(id) 
     {
     	if (!divSet) { setDivMP(getObj(id)); }
     	var picker = getObj('colorpicker');
		if (id == curId && picker.style.display == 'block') 
		{
			picker.style.display = 'none';
			SetVisibilitySelects("visible",0,0);
			return;
		}
     	curId = id;
     	var thelink = getObj(id);
     	var top,left;
     	top = getAbsoluteOffsetTop(thelink) + 20;
     	left = getAbsoluteOffsetLeft(thelink);
     	picker.style.top = top;
     	picker.style.left = left;
     	SetVisibilitySelects("hidden",top,left);
	    picker.style.display = 'block';
	    
     }
     /*
        Due to windowed Elements (select) always drawn on top of non-window (DIVs, all others)
        Need to hide selects
     */
     function SetVisibilitySelects(visibility,picktop,pickleft)
     {
        var selects = document.getElementsByTagName("select");
        for( i = 0; i < selects.length; i++)
        {
            /* only hide selects close by */
            if (visibility == "hidden") 
            {
                s_top = getAbsoluteOffsetTop(selects[i]);
                s_left = getAbsoluteOffsetLeft(selects[i]);
                if ((s_top >= picktop) && (s_top <= (picktop + 120) )) // && (s_left >= pickleft) && (s_left <= (pickleft + 150)))
                {
                    selects[i].style.visibility = visibility;
                }
            }
            else
            {
                selects[i].style.visibility = visibility;
            }
                
             
            
        }
     }

     function genColors() {
        addColorValue('0','0','0');
        addColorValue('3','3','3');
        addColorValue('6','6','6');
        addColorValue('8','8','8');
        addColorValue('9','9','9');
        addColorValue('A','A','A');
        addColorValue('C','C','C');
        addColorValue('E','E','E');
        addColorValue('F','F','F');

        for (a = 1; a < colorLevels.length; a++)
			addColor(0,0,a);
        for (a = 1; a < colorLevels.length - 1; a++)
			addColor(a,a,5);

        for (a = 1; a < colorLevels.length; a++)
			addColor(0,a,0);
        for (a = 1; a < colorLevels.length - 1; a++)
			addColor(a,5,a);

        for (a = 1; a < colorLevels.length; a++)
			addColor(a,0,0);
        for (a = 1; a < colorLevels.length - 1; a++)
			addColor(5,a,a);


        for (a = 1; a < colorLevels.length; a++)
			addColor(a,a,0);
        for (a = 1; a < colorLevels.length - 1; a++)
			addColor(5,5,a);

        for (a = 1; a < colorLevels.length; a++)
			addColor(0,a,a);
        for (a = 1; a < colorLevels.length - 1; a++)
			addColor(a,5,5);

        for (a = 1; a < colorLevels.length; a++)
			addColor(a,0,a);
        for (a = 1; a < colorLevels.length - 1; a++)
			addColor(5,a,5);

       	return colorArray;
     }
     function getColorTable() {
         var colors = colorArray;
      	 var tableCode = '';
         tableCode += '<table border="0" cellspacing="1" cellpadding="1">';
         for (i = 0; i < colors.length; i++) {
              if (i % perline == 0) { tableCode += '<tr>'; }
              tableCode += '<td bgcolor="#000000"><a style="outline: 1px solid #000000; color: '
              	  + colors[i] + '; background: ' + colors[i] + ';font-size: 10px;" title="'
              	  + colors[i] + '" href="javascript:setColor(\'' + colors[i] + '\');">&nbsp;&nbsp;&nbsp;</a></td>';
              if (i % perline == perline - 1) { tableCode += '</tr>'; }
         }
         if (i % perline != 0) { tableCode += '</tr>'; }
         tableCode += '</table>';
      	 return tableCode;
     }
     
     function getColorTableMP() {
         var colors = colorArray;
      	 var tableCode = '';
         tableCode += '<table border="0" cellspacing="1" cellpadding="1">';
         for (i = 0; i < colors.length; i++) {
              if (i % perline == 0) { tableCode += '<tr style="line-height:12px">'; }
              tableCode += '<td bgcolor="#000000"><a style="outline: 1px solid #000000; color: '
              	  + colors[i] + '; background: ' + colors[i] + ';font-size: 10px;" title="'
              	  + colors[i] + '" href="javascript:setColor(\'' + colors[i] + '\');">&nbsp;&nbsp;&nbsp;</a></td>';
              if (i % perline == perline - 1) { tableCode += '</tr>'; }
         }
         if (i % perline != 0) { tableCode += '</tr>'; }
         tableCode += '</table>';
      	 return tableCode;
     }
     
     function getFieldFromId(id)
     {
        var fieldname;
        //fieldname = id.substring(1,id.length);
        fieldname = id.replace(appendedStr,"");
        return fieldname;
     }
     function relateColor(id, color) {
     	var link = getObj(id);
     	if (color == '' || color == 'inherit') {
	     	link.style.background = nocolor;
	     	link.style.color = nocolor;
	     	color = nocolor;
     	} else {
	     	link.style.background = color;
	     	link.style.color = color;
	    }
	    eval(getObj(getFieldFromId(id)).title);
     }
     function getAbsoluteOffsetTop(obj) {
     	var top = obj.offsetTop;
     	var parent = obj.offsetParent;
     	while (parent != document.body && parent != null) {
     		top += parent.offsetTop;
     		parent = parent.offsetParent;
     	}
     	return top;
     }

     function getAbsoluteOffsetLeft(obj) {
     	var left = obj.offsetLeft;
     	var parent = obj.offsetParent;
     	while (parent != document.body && parent != null) {
     		left += parent.offsetLeft;
     		parent = parent.offsetParent;
     	}
     	return left;
     }



