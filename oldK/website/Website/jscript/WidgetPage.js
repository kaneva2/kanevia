 
function WidgetPage( parent_doc, widgetManager, placement_indicator, spacer )
{
  this.parent_doc						= parent_doc;
  this.widgetManager					= widgetManager;
  
  this.left								= 0;
  this.top								= 0;
  this.width							= 0;
  this.use_spacer						= false;
  
  this.areas							= new Array();
  this.deletion_area					= null;
  
  this.spacer							= new Spacer(spacer);
  
  this.placement_indicator				= new Division(placement_indicator);
  this.placement_indicator_left_offset	= 0;
  this.placement_indicator_top_offset	= 0;
  
  // accessors
  this.GetArea							= WidgetPageGetArea;
  this.GetWidget						= WidgetPageGetWidget;
  this.GetSaveString					= WidgetPageGetSaveString;
  
  // methods
  this.AddArea							= WidgetPageAddArea;
  this.AddDeletionArea					= WidgetPageAddDeletionArea;
  this.AddWidgetInstance				= WidgetPageAddWidgetInstance;
  this.AddWidget						= WidgetPageAddWidget;
  this.RemoveWidget						= WidgetPageRemoveWidget;
  this.EditWidget						= WidgetPageEditWidget; 
  this.FindAreaContains					= WidgetPageFindAreaContains;
  this.Update							= WidgetPageUpdate;
  this.UpdateWidth						= WidgetPageUpdateWidth;
  
  
  this.StartDraggingWidget				= WidgetPageStartDraggingWidget;
  this.DraggingWidget					= WidgetPageDraggingWidget;
  this.StopDraggingWidget				= WidgetPageStopDraggingWidget;
  
  this.OnMouseMove						= WidgetPageOnMouseMove;
  this.OnMouseClick						= WidgetPageOnMouseClick;
}


function WidgetPageGetSaveString( hidden_field_id )
{
    var area, widget;
    value		= "";
    var count	= 0;
    
    for ( i = 0; i < this.areas.length; i++ )
    {
        area = this.areas[i];
        
        value += area.name + ":";
        count = 0;
        
        for ( j = 0; j < area.widgets.length; j++ )
        {
            widget = area.widgets[j];
            value += widget.widget_id + ",";
            count++;
        }
        
        if (count > 0)
        {
            value = value.substr( 0, value.length - 1);
        }
        
        value += ";";
    }
    
    $(hidden_field_id).value = value;
}

function WidgetPageUpdate()
{
    for (var i=0; i < this.areas.length; i++)
    {
        this.areas[i].UpdateBoundaries();
        this.areas[i].UpdateWidgets();
    }
}

function WidgetPageGetWidget( widget_id )
{
    var widget;
    
    for ( var i=0; i < this.areas.length; i++ )
    {
        widget = this.areas[i].GetWidget( widget_id );
        if (widget != null)
            return widget;
    }
    
    return null;
}

function WidgetPageRemoveWidget( widget_id )
{
    var widget = this.GetWidget( widget_id );
    widget.area.RemoveWidget( widget );
    widget.SetVisible(false);
    
    this.deletion_area.InsertWidget( widget ,0 );
    
    this.Update();
}


function WidgetPageAddArea( div, name, width, height, locked)
{
	var area = new Area( this, div, name, width, height, locked );

	this.areas[this.areas.length] = area;

	area.SetPosition( this.left + this.width, this.top );
	area.UpdateBoundaries();

	this.width += area.GetWidth();

	return area;
}

function WidgetPageAddDeletionArea( div, name, width, height )
{
    this.deletion_area = this.AddArea( div, name, width, height, false );
     
    return this.deletion_area;
}


// invoked by area
function WidgetPageUpdateWidth()
{
	this.width = 0;
	
	for ( var i=0; i < this.areas.length; i++ )
	{
		this.areas[i].SetPosition( this.left + this.width, this.top );
		this.width += this.areas[i].GetWidth();
	}
}

// find the area that contains the given window coordinates
function WidgetPageFindAreaContains( x, y )
{
	for( var i=0; i < this.areas.length; i++)
	{
		area = this.areas[i];
		if (area.Contains(x,y))
			return area;
	}
	
	return this.areas[this.areas.length - 1];
}

function WidgetPageGetArea(index)
{
	if ( index >= 0 && index <= this.areas.length-1 )
		return this.areas[index];
	else
		return 0;
}

function WidgetPageStartDraggingWidget( widget, x, y )
{
	if ( this.drag_widget != null )
		return;
		
	this.drag_widget = widget;
  
	this.placement_indicator_left_offset	= x - widget.GetLeft();
	this.placement_indicator_top_offset		= (y + document.body.scrollTop) - widget.GetTop();
	this.placement_indicator.SetPosition(widget.GetLeft(), widget.GetTop() );
	this.placement_indicator.SetDimensions(widget.GetWidth(), widget.GetHeight());
	this.placement_indicator.SetVisible(true);
	this.placement_indicator.SetCursor("move");
  
	MouseClickEventHandler.RegisterObserver(null, this);
	MouseMoveEventHandler.RegisterObserver(null, this);
	
	this.DraggingWidget( x, y );
}

// left and top in client coords
function WidgetPageDraggingWidget(left, top)
{
	var offset_top	= top + document.body.scrollTop;
	
	var area = this.FindAreaContains( left, offset_top );
	
	if ( area == this.deletion_area )
		return;
		
	if ( area.locked == true)
	    return;
 
	
	this.spacer.SetVisible(true);
    
	var row = area.FindRowContains( offset_top );
	if( area != this.target_area || row != this.target_row )
	{
		if ( this.target_area )
		{
			// take out spacer from any previous area
			if ( this.use_spacer ) 
			{
				this.target_area.RemoveWidget( this.spacer ); 
				this.use_spacer = false;
			}

			if ( area == this.target_area )
				row = area.FindRowContains( offset_top );
		}
		
		this.target_area	= area;
		this.target_row	= row;
		
		// insert spacer into new zone at row position
		area.InsertWidget( this.spacer, row );
		this.Update();
		this.use_spacer = true;
	}

	this.placement_indicator.SetPosition(left - this.placement_indicator_left_offset, offset_top - this.placement_indicator_top_offset);
}

function WidgetPageStopDraggingWidget()
{
	MouseClickEventHandler.UnRegisterObserver(null, this);
	MouseMoveEventHandler.UnRegisterObserver(null, this);

	this.placement_indicator.SetVisible(false);
	this.spacer.SetVisible(false);
	this.target_area = null;
	this.target_row = null;

	var target_area = this.spacer.area;
	if (target_area == this.deletion_area)
	{
		this.drag_widget = null;
		return;
	}

	var target_row = this.spacer.GetRow();

	if (this.use_spacer) 
	{  
		target_area.RemoveWidget( this.spacer );
		this.use_spacer = false;
	}
 
	if ( target_area == this.drag_widget.area && target_row > this.drag_widget.GetRow() )
		target_row--;
		
	if ( target_area != this.drag_widget.area || target_row != this.drag_widget.GetRow() )
	{
		if (target_row != null)
		{
			var widget = this.drag_widget;
			// take out from previous row
			if ( this.drag_widget.area )
			{
				this.drag_widget.area.RemoveWidget( this.drag_widget );
			}
			// now put in new area
			target_area.InsertWidget( widget, target_row );
		}
	}
	
	this.drag_widget = null;
	
	// update areas
	this.Update();
}

function WidgetPageOnMouseMove(e)
{
	if ( this.drag_widget != null )
		this.DraggingWidget( e.clientX, e.clientY )
}


function WidgetPageOnMouseClick(e)
{
	if ( this.drag_widget != null && e.type == "mouseup" )
		this.StopDraggingWidget();
}

function WidgetPageEditWidget( widget_id )
{
    var widget = this.GetWidget( widget_id );
    
    var script = this.widget_edit_template ? this.widget_edit_template.replace("%widget_id%", widget_id).replace("area", widget.area.name +  "_" + widget.GetRow()) : "";
    if (script != "")
    {
        this.edit_widget = widget;
        eval(script);
    }
}

function WidgetPageAddWidgetInstance( widget_id )
{
    if ( this.edit_widget )
    {
        var area	= this.edit_widget.area;
        var row		= this.edit_widget.GetRow();
        var widget	= this.widgetManager.CreateWidget( this.edit_widget.widget_type, this.parent_doc, widget_id, this, 0, 1);
            
        area.InsertWidget( widget, row );
        area.RemoveWidget( this.edit_widget );
        this.edit_widget.SetVisible(false);
        
        this.Update();
    }
}

function WidgetPageAddWidget( widget_type )
{
	var widget = this.widgetManager.CreateWidget( widget_type, this.parent_doc, "new_" + widget_type + "_area_" + GenUniqueID(), this, 0, 1);

	// insert widget into top position of header zone
	// unless it's locked then use the top of the right hand column
	
	if( this.areas[0].locked == false )
	    this.areas[0].InsertWidget( widget, 0 );
	else
	    this.areas[2].InsertWidget( widget, 0 );
	
	

    // update all areas
    this.Update();
}


