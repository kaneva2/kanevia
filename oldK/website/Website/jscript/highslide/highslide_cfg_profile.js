hs.graphicsDir = '../images/highslide/';
hs.outlineType = 'rounded-white';
hs.outlineWhileAnimating = true;
hs.easing = 'linearTween';
hs.expandDuration = 700;
hs.restoreDuration = 700;
hs.showCredits = false;

hs.fadeInOut = false;


// Added Overrides

// Add event to close expander when clicking outside of expander window
hs.addEventListener(document, 'click', 
	function(e)  { 
		// normalize 
		if (!e) e = window.event; 
		if (e.button > 1) return true; 
		if (!e.target) e.target = e.srcElement; 
    
		// check if it is part of an expander 
		if (!hs.getExpander(e.target)) { 
			// close the currently focused expander 
			hs.close(); 
		}   
	});

hs.cleanUpRefresh = function (funcName) {
	eval(funcName);
};