﻿/* Javascript Document: Registration Landing Page 11/1/2012 */
var $j = jQuery.noConflict();
$j(document).ready(function () {

    // Pretty selects
    $j('.signupForm select').selectBox('create');

    // Facebook registration
    $j(document).on('click', '.imgFBSignup', function () { FBLogin(FBAction.REGISTER); });
    
    // Reg button hiding
    $j('#pnlSignupForm').on('click', '.joinNow', function (event) {
        $j('.joinNow').hide();
        $j('.loading').show();
    });

    // Username wrapping
    $j('.promiseContainer h1 .userNameWrapper').each(function () {
        var curText = $j(this).text();

        if (curText.length > 30) {
            $j(this).html('<br />' + curText);
            $j('.promiseContainer').addClass('promiseContainerLarge');
        }
    });

    // Validation via ajax for availability of username    
    $j('#txtUserName').blur(ValidateUsername);
    $j('#txtEmail').blur(ValidateEmail);
    $j('.signupForm input[type="password"]').blur(function () {
        ValidatePW();
    });

    // Bind input fields for validation
    $j('.signupForm input[type="text"]:not(#txtEmail,#txtUserName),select').blur(function () {
   //     if (ValidateField($j(this))) {
   //         IsRestricted($j(this)); 
        //     }
        ValidateField($j(this));
    });

    // Country
    $j('.countrySelect').change(function () {
        CheckCountry();
    });

    // Birthdate
    $j('a.monthSelect, a.daySelect, a.yearSelect').change(function () {
        ValidateBirthday(); 
    });
    $j('a.monthSelect, a.daySelect, a.yearSelect').focusout(function () {
        ValidateBirthday(); 
    });
    // Show error message dialog for birthday
    $j('select').focus(function () {
        // If error, then display popup
        if ($j('.validateimg.' + $j(this).attr('field-name')).css('display') == 'block') {
            // Hide validation error icon
            $j('.validateimg.' + $j(this).attr('field-name')).hide();
            $j('.errmsgbox.' + $j(this).attr('field-name')).css('display', 'block');
        }
        $j(this).next('a').removeClass('validate-err');
    });
    $j('.birthday a').click(function () {
        $j(this).prev('select').focus(); 
    });

    // Gender
    $j('input[field-name="gender"]').blur(function () {
        ValidateGender($j(this));
    });
    $j('input[field-name="gender"]').click(function () {
        if ($j("#radFemale").is(":checked") || $j("#radMale").is(":checked")) {
            $j("div.female,div.male").removeClass('validate-err');
            $j('.validateimg.' + $j(this).attr('field-name')).hide();
        }
        $j('#drpGender').val($j('input[field-name="gender"]:checked').val());
    });
    // Show error message dialog for gender
    $j('input[field-name="gender"]').focus('input', function () {
        if ($j('.validateimg.' + $j(this).attr('field-name')).css('display') == 'block') {
            // Hide validation error icon
            $j('.validateimg.' + $j(this).attr('field-name')).hide();
            $j('.errmsgbox.' + $j(this).attr('field-name')).css('display', 'block');
            $j("div.female,div.male").removeClass('validate-err');
        }
    });

    // Captcha
    $j('.signupForm .captcha input[type="text"]').blur(function () {
        ValidateCaptcha($j(this));
    });

    // Show error message dialog for text fields 
    $j('.signupForm input[type="text"],.signupForm input[type="password"]').focus(function () {
        // If err, then display popup
        if ($j('.validateimg.' + $j(this).attr('field-name')).css('display') == 'block') {
            // Hide validation error icon
            $j('.validateimg.' + $j(this).attr('field-name')).hide();
            $j('.errmsgbox.' + $j(this).attr('field-name')).css('display', 'block');
        } else if (($j(this).attr('field-name') == undefined || $j(this).attr('field-name') == false) && $j('.validateimg.captcha').css('display') == 'block') {
            $j('.validateimg.captcha').hide();
            $j('.errmsgbox.captcha').css('display', 'block');
        }
    });

    // Show/hide field labels if user types in field
    $j('.signupForm input[type="text"],.signupForm input[type="password"]').bind('input', function () {
        if ($j(this).val().trim().length > 0) {
            $j('#' + $j(this).attr('id') + '_lbl').hide();
        } else {
            $j('#' + $j(this).attr('id') + '_lbl').show();
        }
    });

    // Click field label, then hide and set focus in field
    $j('.signupForm div .labeltxt').click(function () {
        $j('#' + $j(this).attr('id').replace('_lbl', '')).focus();
    });

    // Bind click event to the validation icon
    $j("i").click(function () {
        if ($j(this).attr('name') == 'birthdate') {
            $j('select[field-name=birthdate]').each(function () {
                if ($j(this).val().trim().length == 0) {
                    $j(this).next('a').focus(); 
                    return false;
                }
            });
        } else if ($j(this).attr('name') == 'captcha') {
            $j('div.captcha input[type="text"]').focus();
        } else if ($j(this).attr('name') == 'gender') {
            $j("#radFemale").focus();
        } else {
            $j('#' + $j(this).attr('name')).focus();
        }
    });

    // Bind Register button click 
    $j('#btnRegister').click(function () {
        var submit = true;
        // Username
        if (!ValidateUsername()) {
            submit = false;
            $j("#txtUserName").focus();
        }
        // Firstname
        if (!ValidateField($j('#txtFirstName'))) {
            if (submit) {
                $j("#txtFirstName").focus();
            }
            submit = false;
        }
        // Lastname
        if (!ValidateField($j('#txtLastName'))) {
            if (submit) {
                $j("#txtLastName").focus();
            }
            submit = false;
        }
        // Email
        if (!ValidateEmail()) {
            if (submit) {
                $j("#txtEmail").focus();
            }
            submit = false;
        }
        // Password
        if (!ValidatePW()) {
            if (submit) {
                $j("#txtPassword").focus();
            }
            submit = false;
        }
        // Postal Code
        if (!ValidateField($j('#txtPostalCode'))) {
            if (submit) {
                $j("#txtPostalCode").focus();
            }
            submit = false;
        }
        var dateValid = true;
        // Birthday
        if (!ValidateField($j('#drpMonth'))) {
            if (submit) {
                dateValid = false;
            }
            submit = false;
        }
        if (!ValidateField($j('#drpDay'))) {
            if (submit) {
                dateValid = false;
            }
            submit = false;
        }
        if (!ValidateField($j('#drpYear'))) {
            if (submit) {
                dateValid = false;
            }
            submit = false;
        }
        if (!dateValid) {
            $j('.errmsgbox.birthdate').css('display', 'block');
        }
        // Gender
        if (!ValidateGender($j('input[field-name="gender"]'))) {
            if (submit) {
                $j('input[field-name="gender"]').focus();
            }
            submit = false;
        }
        if ($j('.captcha').css('display') == 'block') {
            if (!ValidateCaptcha($j('.captcha input[type="text"]'))) submit = false;
        }

        if (!submit) {
            return false;
        }
    });

    GetDefaultErrorMsgs();

    /* 
    This code is used to rebind the onchange event handler after the
    Ajax call is made back to the page to  */
    try {
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function () {

            // Validation via ajax for availability of username    
            $j('#txtUserName').blur(ValidateUsername);
            $j('#txtEmail').blur(ValidateEmail);
            $j('.signupForm input[type="password"]').blur(function () {
                ValidatePW();
            });

            // Click field label, then hide and set focus in field
            $j('.signupForm div .labeltxt').click(function () {
                $j('#' + $j(this).attr('id').replace('_lbl', '')).focus();
            });

            // Birthdate
            $j('a.monthSelect, a.daySelect, a.yearSelect').change(function () {
                ValidateBirthday();
            });
            $j('a.monthSelect, a.daySelect, a.yearSelect').focusout(function () {
                ValidateBirthday();
            });
            // Show error message dialog for birthday
            $j('select').focus(function () {
                // If error, then display popup
                if ($j('.validateimg.' + $j(this).attr('field-name')).css('display') == 'block') {
                    // Hide validation error icon
                    $j('.validateimg.' + $j(this).attr('field-name')).hide();
                    $j('.errmsgbox.' + $j(this).attr('field-name')).css('display', 'block');
                }
                $j(this).next('a').removeClass('validate-err');
            });
            $j('.birthday a').click(function () {
                $j(this).prev('select').focus();
            });

            // Gender
            $j('.signupForm input[type="radio"]').blur(function () {
                ValidateGender($j(this));
            });
            $j('.signupForm input[type="radio"]').click(function () {
                if ($j("#radFemale").is(":checked") || $j("#radMale").is(":checked")) {
                    $j("div.female,div.male").removeClass('validate-err');
                    $j('.validateimg.' + $j(this).attr('field-name')).hide();
                }
            });
            // Show error message dialog for gender
            $j('input[type = "radio"]').focus('input', function () {
                if ($j('.validateimg.' + $j(this).attr('field-name')).css('display') == 'block') {
                    // Hide validation error icon
                    $j('.validateimg.' + $j(this).attr('field-name')).hide();
                    $j('.errmsgbox.' + $j(this).attr('field-name')).css('display', 'block');
                    $j("div.female,div.male").removeClass('validate-err');
                }
            });
            // Gender click event
            $j("input[name='gender']").click(function () {
                $j('#drpGender').val($j("input[name='gender']:checked").val());
            });

            // Captcha
            $j('.signupForm .captcha input[type="text"]').blur(function () {
                ValidateCaptcha($j(this));
            });

            // Show error message dialog for text fields 
            $j('.signupForm input[type="text"],.signupForm input[type="password"]').focus(function () {
                // If err, then display popup
                if ($j('.validateimg.' + $j(this).attr('field-name')).css('display') == 'block') {
                    // Hide validation error icon
                    $j('.validateimg.' + $j(this).attr('field-name')).hide();
                    $j('.errmsgbox.' + $j(this).attr('field-name')).css('display', 'block');
                } else if (($j(this).attr('field-name') == undefined || $j(this).attr('field-name') == false) && $j('.validateimg.captcha').css('display') == 'block') {
                    $j('.validateimg.captcha').hide();
                    $j('.errmsgbox.captcha').css('display', 'block');
                }
            });

            // Show/hide field labels if user types in field
            $j('.signupForm input[type="text"],.signupForm input[type="password"]').bind('input', function () {
                if ($j(this).val().trim().length > 0) {
                    $j('#' + $j(this).attr('id') + '_lbl').hide();
                } else {
                    $j('#' + $j(this).attr('id') + '_lbl').show();
                }
            });

            // Bind input fields for validation
            $j('.signupForm input[type="text"]:not(#txtEmail,#txtUserName),.signupForm input[type="password"],select').blur(function () {
                ValidateField($j(this));
            });

            // Bind click event to the validation icon
            $j("i").click(function () {
                if ($j(this).attr('name') == 'birthdate') {
                    $j('select[field-name=birthdate]').each(function () {
                        if ($j(this).val().trim().length == 0) {
                            $j(this).next('a').focus();
                            return false;
                        }
                    });
                } else if ($j(this).attr('name') == 'captcha') {
                    $j('div.captcha input[type="text"]').focus();
                } else if ($j(this).attr('name') == 'gender') {
                    $j("#radFemale").focus();
                } else {
                    $j('#' + $j(this).attr('name')).focus();
                }
            });

            $j('.signupForm select').selectBox('create');
            CountryCheck();

            // Bind Register button click 
            $j('#btnRegister').click(function () {
                var submit = true;

                // Username
                if (!ValidateUsername()) {
                    submit = false;
                    $j("#txtUserName").focus();
                }
                // Firstname
                if (!ValidateField($j('#txtFirstName'))) {
                    if (submit) {
                        $j("#txtFirstName").focus();
                    }
                    submit = false;
                }
                // Lastname
                if (!ValidateField($j('#txtLastName'))) {
                    if (submit) {
                        $j("#txtLastName").focus();
                    }
                    submit = false;
                }
                // Email
                if (!ValidateEmail()) {
                    if (submit) {
                        $j("#txtEmail").focus();
                    }
                    submit = false;
                }
                // Password
                if (!ValidatePW()) {
                    if (submit) {
                        $j("#txtPassword").focus();
                    }
                    submit = false;
                }
                // Postal Code
                if (!ValidateField($j('#txtPostalCode'))) {
                    if (submit) {
                        $j("#txtPostalCode").focus();
                    }
                    submit = false;
                }
                var dateValid = true;
                // Birthday
                if (!ValidateField($j('#drpMonth'))) {
                    if (submit) {
                        dateValid = false;
                    }
                    submit = false;
                }
                if (!ValidateField($j('#drpDay'))) {
                    if (submit) {
                        dateValid = false;
                    }
                    submit = false;
                }
                if (!ValidateField($j('#drpYear'))) {
                    if (submit) {
                        dateValid = false;
                    }
                    submit = false;
                }
                if (!dateValid) {
                    $j('.errmsgbox.birthdate').css('display', 'block');
                }
                // Gender
                if (!ValidateGender($j('input[field-name="gender"]'))) {
                    if (submit) {
                        $j('input[field-name="gender"]').focus();
                    }
                    submit = false;
                }
                if ($j('.captcha').css('display') == 'block') {
                    if (!ValidateCaptcha($j('.captcha input[type="text"]'))) submit = false;
                }

                if (!submit) {
                    return false;
                }
            });
        });
    } catch (err){}
});

var emailRegex = /^[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$/i;
var usernameRegex = /^[a-zA-Z0-9_]{4,20}$/;
var pwRegex = /^[a-zA-Z0-9_]{4,20}$/;

function setUsername(obj) {
    $j("#txtUserName").focus().val(obj.text()).removeClass("validate-err").next('i').hide();
    $j("#txtUserName_lbl").hide(); 
    $j("#txtUserName_errmsg").hide();
}

function ValidateUsername() {
    var retval = true;
    var fld = $j('#txtUserName');
    if (fld.val().trim().length > 0) {
        $j('#' + fld.attr('id') + '_lbl').hide();
    }
    if (!fld.val().match(usernameRegex)) {
        fld.addClass('validate-err');
        $j('.validateimg.' + fld.attr('field-name')).css('display', 'block');
        $j('.errmsgbox.' + fld.attr('field-name') + ' div.msg').removeClass('single');
        ResetErrorMsg(fld);
        retval = false;
    } else {
        var ctrl = fld;
        $j.ajax({
            type: "POST",
            url: root + "services/validation.asmx/Username",
            data: '{"username":"' + fld.val() + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json"
        }).done(function (data) {
            var obj = $j.parseJSON(data.d);
            if (!obj) {
                return false;
            }
            if (obj.Restricted == true) {
                ctrl.addClass('validate-err');
                $j('.validateimg.' + ctrl.attr('field-name')).css('display', 'block');
                $j('.errmsgbox.' + ctrl.attr('field-name') + ' div.msg').text("Do not use restricted words.");
                $j('.errmsgbox.' + ctrl.attr('field-name') + ' div.msg').addClass('single');
                HideSuggestions();
                retval = false;
            } else {
                if (obj.Available == false) {
                    var names = "";
                    for (var i = 0; i < obj.Suggestions.length; i++) {
                        names = names + "<a href='javascript:void(0);' onclick='setUsername($j(this));'>" + obj.Suggestions[i] + "</a> | ";
                    }
                    if ($j('#suggestions').length) {
                        $j('#suggestions').html("Suggestions: " + names.replace(/\|([^\|]*)$/, '$1')).slideDown();
                    }
                    $j('.errmsgbox.' + ctrl.attr('field-name') + ' div.msg').addClass('single');
                    $j('.errmsgbox.' + ctrl.attr('field-name') + ' div.msg').text('This username is not available.');
                    $j('.errmsgbox.' + ctrl.attr('field-name')).css('display', 'block');
                    ctrl.addClass('validate-err');
                    return false;
                } else {
                    ctrl.removeClass('validate-err');
                    $j('.validateimg.' + ctrl.attr('field-name')).hide();
                 //   HideSuggestions();
                }
            }
        });
    }
    $j('.errmsgbox.' + fld.attr('field-name')).hide();
    return retval;
}

function HideSuggestions() {
    if ($j('#suggestions').length) {
        $j('#suggestions').slideUp(800, function () {
            $j('#suggestions').html("");
        });
    }
}
function IsRestricted(fld) {
    $j.ajax({
        type: "POST",
        url: root + "services/validation.asmx/IsRestricted",
        data: '{"text":"' + fld.val() + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json"
    }).done(function (data) {
        var obj = $j.parseJSON(data.d);
        if (!obj) {
            return false;
        }
        if (obj == true) {
            fld.addClass('validate-err');
            $j('.validateimg.' + fld.attr('field-name')).css('display', 'block');
            $j('.errmsgbox.' + fld.attr('field-name') + ' div.msg').text("Do not use restricted words.");
            $j('.errmsgbox.' + fld.attr('field-name') + ' div.msg').addClass('short-multiline');
            return false;
        }
        return true;
    });
}

// Validation via ajax for availability of email
function ValidateEmail() {
    var iserr = false;
    var txtemail = $j('#txtEmail');
    if (txtemail.val().trim().length > 0) {
        $j('#' + txtemail.attr('id') + '_lbl').hide();
    }
    if (!txtemail.val().match(emailRegex)) {
        txtemail.addClass('validate-err');
        $j('.validateimg.' + txtemail.attr('field-name')).css('display', 'block');
        $j('.errmsgbox.' + txtemail.attr('field-name') + ' div.msg').removeClass('single');
        ResetErrorMsg(txtemail);
        iserr = true;
    } else {
        var ctrl = txtemail;
        $j.ajax({
            type: "POST",
            url: root + "services/validation.asmx/EmailAvailable",
            data: '{"email":"' + txtemail.val() + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json"
        }).done(function (data) {
            var obj = $j.parseJSON(data.d);
            if (obj == null) {
                return;
            }
            if (obj == false) {
                ctrl.addClass('validate-err');
                $j('.validateimg.' + ctrl.attr('field-name')).css('display', 'block');
                $j('.errmsgbox.' + ctrl.attr('field-name') + ' div.msg').text("This email is already registered.");
                $j('.errmsgbox.' + ctrl.attr('field-name') + ' div.msg').addClass('single');
                iserr = true;
            } else {
                ctrl.removeClass('validate-err');
                $j('.validateimg.' + ctrl.attr('field-name')).hide();
            }
        });
    }
    $j('.errmsgbox.' + txtemail.attr('field-name')).hide();
    return !iserr;
}

function ValidatePW() {
    var iserr = false;
    var txtpw = $j('#txtPassword');
    if (txtpw.val().trim().length > 0) {
        $j('#' + txtpw.attr('id') + '_lbl').hide();
    }
    if (!txtpw.val().match(usernameRegex)) {
        txtpw.addClass('validate-err');
        $j('.validateimg.' + txtpw.attr('field-name')).css('display', 'block');
        iserr = true;
    } else {
        txtpw.removeClass('validate-err');
    }
    $j('.errmsgbox.' + txtpw.attr('field-name')).hide();
    return !iserr;
}

function CheckCountry() {
    if ($j('.countrySelect').val() == 'US') {
        $j('#divPostalCode').css('display', 'block');
        if ($j('#txtPostalCode').val().trim().length == 0) {
            $j('#txtPostalCode_lbl').css('display', 'block');
        }
    } else {
        $j('#divPostalCode').css('display', 'none');
        $j('#txtPostalCode_lbl').css('display', 'none');
    }
}

function ValidateField(fld) {
    var iserr = false;
    if (fld.val().trim().length == 0) {
        if (fld.attr('field-name') == 'birthdate') {
            fld.next('a').addClass('validate-err');
        } else if (fld.attr('field-name') == 'postalcode') {
            if (fld.is(':visible')) {
                fld.addClass('validate-err');
            }
            else {
                return !iserr;
            }
        } else { // This case handles first and last name
            ResetErrorMsg(fld);
            fld.addClass('validate-err');
            $j('.errmsgbox.' + fld.attr('field-name') + ' div.msg').removeClass('short-multiline');
        }
        $j('.validateimg.' + fld.attr('field-name')).css('display', 'block');
        iserr = true;
    } else {
        if (fld.attr('field-name') == 'birthdate') {
            fld.next('a').removeClass('validate-err');
        } else if (fld.attr('field-name') == 'firstname' || fld.attr('field-name') == 'lastname') {
            if (IsRestricted(fld)) {
                iserr = true;
            }
            fld.removeClass('validate-err');
        }
        else {
            fld.removeClass('validate-err');
        }
        $j('.validateimg.' + fld.attr('field-name')).hide();
        $j('#' + fld.attr('id') + '_lbl').hide();
    }
    $j('.errmsgbox.' + fld.attr('field-name')).hide();
    return !iserr;
}

function ValidateBirthday() {
    var isvalid = true; 
    $j('select[field-name=birthdate]').each(function () {
        if ($j(this).val().trim().length == 0) {
            $j(this).next('a').addClass('validate-err');
            $j('.validateimg.birthdate').css('display', 'block');
            isvalid = false;
        } else {
            $j(this).next('a').removeClass('validate-err');
        }
    });
    $j('.errmsgbox.birthdate').hide();
}

function ValidateGender(fld) {
    var iserr = false;
    if ($j("#radFemale").is(":checked") || $j("#radMale").is(":checked")) {
        $j("div.female,div.male").removeClass('validate-err');
        $j('.validateimg.' + fld.attr('field-name')).hide();
    } else {
        $j("div.female,div.male").addClass('validate-err');
        $j('.validateimg.' + fld.attr('field-name')).css('display', 'block');
        iserr = true;
    }
    $j('.errmsgbox.' + fld.attr('field-name')).hide();
    return !iserr;
}

function ValidateCaptcha(fld) {
    var iserr = false;
    if (fld.val().trim().length == 0) {
        fld.addClass('validate-err');
        $j('.validateimg.captcha').css('display', 'block');
        iserr = true;
    } else {
        fld.removeClass('validate-err');
        $j('.validateimg.captcha').hide();
    }
    $j('.errmsgbox.captcha').hide();
    return !iserr;
}

function ValidateAllFields(doUsername, doFirstname, doLastname)
{
    if (doUsername) {
        ValidateUsername();
    }
    if (doFirstname) {
        ValidateField($j('#txtFirstName'));
    }
    if (doLastname) {
        ValidateField($j('#txtLastName'));
    }
    ValidateEmail();
    ValidatePW();
    ValidateField($j('#txtPostalCode'));
    ValidateField($j('#drpMonth'));
    ValidateField($j('#drpDay'));
    ValidateField($j('#drpYear'));
    ValidateGender($j("#radFemale"));
}

function ResetErrorMsg(fld) {
    $j('.errmsgbox.' + fld.attr('field-name') + ' div.msg').text(GetErrorMsg(fld));
}

var defaultMsgs = new Array();
function GetDefaultErrorMsgs() {
    $j('.signupForm input[type="text"]').each(function () {
        defaultMsgs.push({ field: $j(this).attr('field-name'), msg: $j('.errmsgbox.' + $j(this).attr('field-name') + ' div.msg').text() });
    });
}
function GetErrorMsg(fld) {
    var err = '';
    $j.each(defaultMsgs, function (key, obj) {
        if (obj.field == fld.attr('field-name')) {
            err = obj.msg;
            return;
        }
    });
    return err;
}