﻿/* Javascript Document: Profile Landing Pages 10/30/2012 */
/* NOTE: '$j' alias for jQuery defined in landing_page/base.js */

$j(document).ready(function () {

    // Page links
    $j('.profilePreview').click(function () {
        var regLink = $j('.hiRegisterLink').val();
        if (regLink.length > 0) {
            window.location = regLink;
        } else {
            window.location = "../register/kaneva/registerInfo.aspx";
        }
    });
    $j('.profileListItem').click(function () {
        var newURL = $j(this).attr('data-profilelink');
        window.location = newURL;
    });

    // Header text scaling
    var uNameCharCount = $j('.profilePreview h1').text().length;
    if (uNameCharCount <= 20) {
        $j('.profilePreview h1').css('font-size', '19px');
    } else if (uNameCharCount < 37) {
        $j('.profilePreview h1').css('font-size', '16px');
    } else {
        $j('.profilePreview h1').css('font-size', '12px');
    }

    // Username wrapping
    $j('.profilePreview .userName').each(function () {
        var curText = $j(this).text();
        if (curText.length > 30) {
            $j(this).html('<br />' + curText);
        }
    });

    // Dropshadow scaling
    var profilePreviewHeight = $j('.profilePreview').outerHeight();
    var profilePreviewShadowHeight = profilePreviewHeight - 2;
    $j('.profilePreviewShadow').css('height', profilePreviewShadowHeight);
});

// Image positioning
$j(window).load(function () {
    $j('.profilePreview .profilePhoto img').each(function () {
        var picCenterX = Math.round($j(this).width() / 2);
        var picCenterY = Math.round($j(this).height() / 2);
        var containerX = Math.round($j('.profilePreview .profilePhoto').width() / 2);
        var containerY = Math.round($j('.profilePreview .profilePhoto').height() / 2);

        $j(this).css('left', containerX - picCenterX);
        $j(this).css('top', containerY - picCenterY);
    });
});