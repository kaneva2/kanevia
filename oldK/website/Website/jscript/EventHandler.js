function EventObserver( subject, observer )
{
	this.subject	= subject;
	this.observer	= observer;
}
 
function EventHandler()
{
	this.observerCollection	= new Array();

	this.RegisterObserver	= EventHandlerRegisterObserver;
	this.UnRegisterObserver	= EventHandlerUnRegisterObserver;
	this.NotifyObservers	= EventHandlerNotifyObservers;
}

function EventHandlerRegisterObserver( subject, observer )
{
	this.observerCollection[ this.observerCollection.length ] = new EventObserver(subject, observer);
	this.RegisterEvent(subject);
}

function EventHandlerUnRegisterObserver( subject, observer )
{
	for ( var i=0, moveUp=false; i < this.observerCollection.length; i++ )
	{
		if ( moveUp )
			this.observerCollection[i-1] = this.observerCollection[i];
			
		if ( (this.observerCollection[i].subject == subject) && (this.observerCollection[i].observer == observer) )
			moveUp = true;
	}
	
	this.observerCollection.length--;
}

function EventHandlerNotifyObservers( e )
{
	for( var i=0; i < this.observerCollection.length; i++ )
	{
		if ( this.observerCollection[i].subject == null || this.observerCollection[i].subject == e.target)
		{
			this.NotifyObserverEvent( this.observerCollection[i].observer, e );
		}
	}
}

var MouseClickEventHandler						= new EventHandler();
MouseClickEventHandler.NotifyObserverEvent		= NotifyObserverMouseClickEvent;
MouseClickEventHandler.RegisterEvent			= RegisterMouseClick;

var MouseMoveEventHandler						= new EventHandler();
MouseMoveEventHandler.NotifyObserverEvent		= NotifyObserverMouseMoveEvent;
MouseMoveEventHandler.RegisterEvent				= RegisterMouseMove;

var DocumentResizeEventHandler					= new EventHandler();
DocumentResizeEventHandler.NotifyObserverEvent	= NotifyObserverDocumentResizeEvent;
DocumentResizeEventHandler.RegisterEvent		= RegisterDocumentResize;

function OnMouseClick(e)
{
	if ( !e )
		e = event;
		
	if ( !e.target )
		e.target = e.srcElement;
	
	MouseClickEventHandler.NotifyObservers(e);
}

function NotifyObserverMouseClickEvent( observer, e )
{
	observer.OnMouseClick(e);
}

function RegisterMouseClick( subject )
{
	if ( !subject )
		subject = document;
		
	subject.onmouseclick	= OnMouseClick;
	subject.onmousedown		= OnMouseClick;
	subject.onmouseup		= OnMouseClick;
}


function OnMouseMove(e)
{
	if ( !e )
		e = event;
		
	if (!e.target)
		e.target = e.srcElement;
		
	MouseMoveEventHandler.NotifyObservers(e);
}

function NotifyObserverMouseMoveEvent( observer, e )
{
	observer.OnMouseMove(e);
}

function RegisterMouseMove( subject )
{
	if (!subject)
		subject = document;

	subject.onmousemove = OnMouseMove;
}

function OnResizeEnd(e)
{
    if (!e)
		e = event;
		
    if (!e.target)
		e.target = e.srcElement;
		
    DocumentResizeEventHandler.NotifyObservers(e);
}

function NotifyObserverDocumentResizeEvent( observer, e )
{
    observer.OnResizeEnd(e);
}

function RegisterDocumentResize()
{
    window.onresizeend = OnResizeEnd;
}