var checkWorldCreation;

function Create3Dapp() {
    ShowLoading(true, 'Please wait while your World is being created, this may take a few minutes...<br/><img src="../images/progress_bar.gif">');
}

function CheckWorldCreation(token, timeout, retryLimit, retryCount) {
    jQuery.ajax({
        type: "POST",
        url: root + "services/worldcreationcheck.asmx/HasWorldCreationCompleted",
        data: '{"token":"' + token + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json"
    }).done(function (data) {
        var hf = document.getElementById("hfAsyncPollingResponder");
        if (!data.d && retryCount < retryLimit) {
            // Try again if we're still waiting.
            checkWorldCreation = setTimeout(function () { CheckWorldCreation(token, timeout, retryLimit, retryCount + 1) }, timeout);
        } else if (data.d) {
            var obj = JSON.parse(data.d);
            if (obj["Status"] === "Success") {
                hf.value = "{\"Status\":\"Success\", \"Message\":\"" + obj["Message"] + "\"}";
                __doPostBack("hfAsyncPollingResponder");
            } else {
                hf.value = "{\"Status\":\"Error\", \"Message\":\"" + obj["Message"] + "\"}";
                __doPostBack("hfAsyncPollingResponder");
            }
        } else {
            hf.value = "{\"Status\":\"Error\", \"Message\":\"Unable to create world. Please try again later.\"}";
            __doPostBack("hfAsyncPollingResponder");
        }
    });
}