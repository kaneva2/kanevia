document.write("<script type=\"text/javascript\" src=\"../jscript/base/Base.js\"></script>");

function InitWidgetData (dataPage, dataContainer, errorContainer, parameters, pageCount)
{	
    InitUpdater(dataPage,dataContainer,errorContainer,'get',parameters,true,true,true,Insertion.None, pageCount);
}

function CheckAllEvents(checkSelector, container)
{
    var divContainer = $(container);

    for(j=0; j < divContainer.length; j++)
    {
        var inputs = divContainer[j].getElementsByTagName('input');
                
        for(i=0; i < inputs.length; i++)
        {
            if(inputs[i].type == "checkbox")
            {
                inputs[i].checked = checkSelector.checked;
            }
        }
    }
}

function DeleteEvents(container)
{
    var itemsChecked = '?values&';
    var getHiddenInput = false;
    var divContainer = $(container);

    for(j=0; j < divContainer.length; j++)
    {
        var inputs = divContainer[j].getElementsByTagName('input');
                
        for(i=0; i < inputs.length; i++)
        {
            if(inputs[i].type == "checkbox")
            {
                if(inputs[i].checked)
                {
                    getHiddenInput = true;
                }
            }
            if((inputs[i].type == "hidden") && getHiddenInput)
            {
                getHiddenInput = false;
                itemsChecked += (inputs[i].value + "|");
            }
        }
    }
    location.href = itemsChecked;
}

function RemovePlayer(id,cid) {
	var ajaxRequest = new Ajax.Request('/mykaneva/widgets/' + 'LeaderboardData.aspx',
	{
	    method: 'get',
	    parameters: 'del=true&userId=' + id + '&communityId=' + cid,
	    asynchronous: true,
	    evalJS: true,
	    evalScripts: true,
	    onComplete: showLeaderboard,  
	    onFailure: PageLoadError
	});
}

function showLeaderboard(xmlHttpRequest, responseHeader) {
    xmlHttpRequest.responseText.evalScripts();
    $('leaderboard').innerHTML = xmlHttpRequest.responseText;
}

function PageLoadError(error) {
    var val = error.responseText;
    alert(val);
}

function GameFame(appCommunityId, communityId) {
    AjaxReqInit('communityId=' + communityId + '&appCommunityId=' + appCommunityId, '/kaneva2010/mykaneva/widgets/BadgesData.aspx');
}

function ProfileFame(communityId) {
    AjaxReqInit('communityId=' + communityId, '/kaneva2010/mykaneva/widgets/ProfileFameData.aspx');
}

