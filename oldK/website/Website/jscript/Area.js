// a zone contains widgets -- when dragging, a spacer may be inserted

var HeightBetweenRows = 20;
 
function Area(page, div, name, width, height, locked)
{
    this.base				= Division;
    this.base(div);	
	this.page				= page;
	this.name				= name;
	this.widgets			= new Array();
	this.locked             = locked;
		
	this.top				= 0;
	this.width				= width;
	this.height				= height;
	this.initialHeight		= height;
	
	this.baseGetLeft		= DivisionGetLeft;
	this.offsetTop			= div.offsetParent.offsetTop;
	this.left				= this.baseGetLeft();
	this.left				= div.offsetParent.offsetLeft;
	this._SetDimensions		= DivisionSetDimensions;
	this._SetDimensions( this.width, this.height );
	
	this.absolute_top;
	this.absolute_left;
	this.absolute_bottom;
	this.absolute_right;
	
	// accessors
	this.GetWidget			= AreaGetWidget;
	this.GetWidth			= AreaGetWidth;
	
	// modifiers
	this.SetWidth			= AreaSetWidth;
	this.SetPosition		= AreaSetPosition;
	
	// methods
	this.UpdateWidgets		= AreaUpdateWidgets;
	this.UpdateBoundaries	= AreaUpdateBoundaries;
	this.FindRow			= AreaFindRow;
	this.FindRowContains	= AreaFindRowContains;
	this.Contains			= AreaContains;
	
	this.AddWidget			= AreaAddWidget;
	this.InsertWidget		= AreaInsertWidget;
	this.RemoveWidget		= AreaRemoveWidget;
}

function AreaGetWidget( widget_id )
{
    for ( var i=0; i < this.widgets.length; i++)
    {
        if ( this.widgets[i].widget_id == widget_id )
            return this.widgets[i];
    }
    
    return null;
}

function AreaGetWidth()
{
	return this.width;
}

function AreaSetWidth( newWidth )
{
	this.width = newWidth;
	this.page.UpdateWidth();
}

function AreaSetPosition(x, y)
{
	this.top = y;
	this.UpdateWidgets();
}

// update positions and ensure left and right zones maintain the same height
function AreaUpdateWidgets()
{
	if ( this.page.deleteZone == this )
		return;
	
	var y_offset		= this.absolute_top + HeightBetweenRows;
	var total_height	= 0;

	var midpoint		= this.absolute_left + (this.absolute_right - this.absolute_left) / 2;
	
	for ( var i=0; i < this.widgets.length; i++ )
	{
		this.widgets[i].SetPosition((midpoint - (this.widgets[i].GetWidth() / 2)), y_offset);
		y_offset		+= this.widgets[i].GetHeight() + HeightBetweenRows;
		total_height	+= this.widgets[i].GetHeight() + HeightBetweenRows;
	}
	
	total_height += HeightBetweenRows;
  
	this.height = total_height > this.initialHeight ? total_height : this.initialHeight ;
  
	if ( this.page.areas[1] != null && this.page.areas[2] != null && this.page.areas[5] != null && (this == this.page.areas[1] || this == this.page.areas[2] || this == this.page.areas[5]) )
	{
		
		if ( this.page.areas[1].widgets.length != this.page.areas[2].widgets.length )
		{
			var left_column = this.page.areas[1].widgets.length + this.page.areas[5].widgets.length;
			var left_column_height = this.page.areas[1].height + this.page.areas[5].height;
			//var longest_zone = left_column > this.page.areas[2].widgets.length ? this.page.areas[1] : this.page.areas[2];
			var longest_zone = left_column_height > this.page.areas[2].height ? this.page.areas[1] : this.page.areas[2];
			if (longest_zone != this )
			{				
				if( this == this.page.areas[2])
				{				
				  this.height = left_column_height;
				}
				if( this == this.page.areas[1])
				{			
				  this.height = longest_zone.height - this.page.areas[5].height;;
				}
			}
		}
	}
  
	this._SetDimensions( this.width, this.height );
}

function AreaUpdateBoundaries()
{
    var left	= 0;
    var top		= 0;
    var zone	= this.div;
    
    while( zone != null)
    {
        top		+= zone.offsetTop;
        left	+= zone.offsetLeft;
        zone	= zone.offsetParent;
    }
    
    this.absolute_top	= top;
    this.absolute_left	= left;
    this.absolute_right	= left + this.div.clientWidth;
    this.absolute_bottom	= top + this.div.clientHeight;
}

// find the row for a widget (or spacer)
function AreaFindRow( widget )
{
	for ( var i=0; i < this.widgets.length; i++ )
	{
		if ( this.widgets[i] == widget)
			return i;
	}
	
	return null;
}

// find the row number that contains the passed in y-coordinate
function AreaFindRowContains(y)
{
	var y_offset = this.absolute_top + HeightBetweenRows;
	
	for ( var i=0; i < this.widgets.length; i++ )
	{
		y_offset += this.widgets[i].GetHeight() + HeightBetweenRows;
		if ( y_offset > y )
		{
			// Round to row number above or below this item
			if ( y_offset - this.widgets[i].GetHeight() / 2 > y )
				return i;
			else
				return i+1;
		}
	}
	
	return this.widgets.length;
}

function AreaContains(x,y)
{
    if ( x >= this.absolute_left && x <= this.absolute_right && y >= this.absolute_top && y <= this.absolute_bottom )
		return true;
    
    return false;
}

// add an item ( widget or spacer )
function AreaAddWidget( widget )
{
	this.InsertWidget( widget, this.widgets.length );
}

// insert an item at the given row number, shifting existing widgets down.
function AreaInsertWidget( widget, row )
{
    if (row == null)
        return;
        
	for ( var i=this.widgets.length; i > row; i-- )
	{
		this.widgets[i] = this.widgets[i-1];
	}
	
	this.widgets[row] = widget;
	widget.area = this;
	this.UpdateWidgets();
}

// remove an item from the zone, moving existing widgets up.
function AreaRemoveWidget( widget )
{

	var found = false;
  
	for ( var i=0; i < this.widgets.length; i++)
	{
		if (found)
			this.widgets[i-1] = this.widgets[i];

		if ( this.widgets[i] == widget )
			found = true;
	}
	
	if (found)
	{
		this.widgets.length--;
		this.UpdateWidgets();
	}
	
	return found;
}








