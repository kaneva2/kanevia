﻿var rootpath = "";

window.fbAsyncInit = function () {
    FB.init({
        appId: fbappid,
        version: 'v2.0',
        channelUrl: 'http://www.kaneva.com/facebook/channel.html',
        status: true,
        cookie: true,
        oauth: true,
        xfbml: true
    }); // closes FB.init

    // If connecting on load, connect
    if (fbConnectOnLoad == "True") {
        FBLogin(FBAction.CONNECT);
    }
};

(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) { return; }
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
} (document, 'script', 'facebook-jssdk'));

function FBLogin(action) {
    FB.login(function (response) {
        if (response.authResponse) {
            SubmitFBAuth(response.authResponse.accessToken, action);
        } else {
            //User cancelled login or did not fully authorize
        }
    }, { scope: 'publish_actions, email, user_friends' });
}

function SubmitFBAuth(token, action) {
    var form = document.createElement("form");
    form.setAttribute("method", 'post');
    if (action == FBAction.LOGIN) {
        form.setAttribute("action", hosturl + '/loginSecure.aspx');
    }
    else if (action == FBAction.REGISTER) {
        form.setAttribute("action", hosturl + '/register/kaneva/registerInfo.aspx' + (urlparams.length > 0 ? urlparams : ""));
    }
    else {
        form.setAttribute("action", rootpath + '/mykaneva/widgets/facebookactions.aspx?ca=' + action.value);
    }
    var field = document.createElement("input");
    field.setAttribute("type", "hidden");
    field.setAttribute("name", 'accessToken');
    field.setAttribute("value", token);
    form.appendChild(field);

    document.body.appendChild(form);
    form.submit();
}
var FBAction = {
    CONNECT : {value: 0, name: "connect"},
    DISCONN : {value: 1, name: "disconnect"},
    LOGIN   : {value: 2, name: "login"},
    REGISTER: {value: 3, name: "reg"}
};
function SendMsg(toid, fromid) {
    FB.ui({
        method: 'send',
        display: 'popup', //'iframe',
        name: 'Join Kaneva, Imagine What You Can Do',
        to: toid,
        link: 'http://www.kaneva.com/register/kanevainvite.aspx?kuid=' + fromid,
        picture: 'http://images.kaneva.com/kaneva3dlogo.gif',
        description: 'Kaneva\'s free virtual world offers you an immersive 3D ' +
			' experience where you can play games, shop, or build your ' +
			' dream home.  Bring your friends, your favorite videos and ' +
			' music... bring your imagination, and step into our free virtual world, the World of Kaneva.'
    });
}
function AddFriend(uId, bId) {
    var img_loading = $j("#imgloading" + uId);
    var btn = $j("#" + bId);
    btn.hide();
    img_loading.show();
    $j.ajax({
        type: "GET",
        url: "widgets/InviteFriend.aspx",
        data: "&uId=" + uId,
        cache: false,
        success: function (txt) {
            try {
                img_loading.hide();
                if (btn.attr("src")) {
                    btn.attr("src", rootpath + "/images/facebook/requested.png");
                }
                btn.removeAttr("onclick");
                btn.attr("disabled", true);
                btn.addClass("disabled");
                btn.show();
            } catch (err) { }
        },
        error: function (xhr, status, error) {
            img_loading.hide();
            if (btn.attr("src")) {
                btn.attr("src", rootpath + "/images/facebook/requested.png");
            }
            btn.removeAttr("onclick");
            btn.attr("disabled", true);
            btn.addClass("disabled");
            btn.show();
        }
    });
}


