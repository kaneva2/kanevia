document.write("<script type=\"text/javascript\" src=\"../jscript/base/Base-min.js\"></script>");
document.write("<script type=\"text/javascript\" src=\"../jscript/interest.js\"></script>");

function addSchool (t)
{
	for (var i=2; i<=5; i++)
	{	
		if ($(pageNamingPrefix + 'trSchool' +i).className == 'hide')
		{
			$(pageNamingPrefix + 'trSchool'+i).className = '';
			if (i==5)
				t.className = 'hide';
			
			break;
		}
	}	
}

function SetPageNamePrefix(prefix)
{
    pageNamingPrefix = prefix;
}

function confirmDelete ()
{
	return confirm('Are you sure you want to remove this school? You will lose all the data currently stored about it.');
}


// An XHR DataSource
// For schools
var svrSchoolList = "./interests/getSchoolList.aspx";
var myDataSource2 = new YAHOO.widget.DS_XHR (svrSchoolList, [";", ","]);

//added due to master page appending ids to name
var namePrefix = "";

myDataSource2.responseType = YAHOO.widget.DS_XHR.TYPE_FLAT;
myDataSource2.scriptQueryParam = "sch";      

var myAutoComp2 = new YAHOO.widget.AutoComplete('<%=lnkAddSchool.NamingContainer.ClientID%>' + "_txtSchool1","myContainer_s1", myDataSource2);   
myAutoComp2.minQueryLength = 2;
myAutoComp2.typeAhead = false;
myAutoComp2.useShadow = true; 
myAutoComp2.allowBrowserAutocomplete = false;
//myAutoComp.maxResultsDisplayed = 20;