 
function Widget( page, div, widget_type )
{
	var cont = $(div);

	this.base = Division;
	this.base(cont);
	
	cont.style.visibility = "visible";
	if (cont.style.zIndex <= page.placement_indicator.div.style.zIndex)
		cont.style.zIndex = page.placement_indicator.div.style.zIndex + 1;

	this.widget_id		= cont.id;
	this.widget_type	= widget_type;
	this.page			= page;
	this.area			= null;
	
	this.OnMouseClick	= WidgetOnMouseClick;
	this.GetRow			= WidgetGetRow;
	
	var internalDIVs	= div.getElementsByTagName("DIV");
	var internalSpans	= div.getElementsByTagName("td");
	this.oTitleBar		= new Division(internalDIVs.item(0));
	this.oTitleBarCell1	= internalSpans.item(0);
	this.oTitleBarCell2	= internalSpans.item(1);
	this.oTitleBarCell3	= internalSpans.item(2);
	MouseClickEventHandler.RegisterObserver(this.oTitleBar.div, this);
	MouseClickEventHandler.RegisterObserver(this.oTitleBarCell1, this);
	MouseClickEventHandler.RegisterObserver(this.oTitleBarCell2, this);
	MouseClickEventHandler.RegisterObserver(this.oTitleBarCell3, this);

    this.hlEdit			= new Division($("hl" + this.widget_id + "Edit"));
    this.hlDelete		= new Division($("hl" + this.widget_id + "Delete"));
}

function WidgetOnMouseClick(e)
{
	if(this.area.locked != 1)
	{	    
	    if ( "mousedown" == e.type )
	    {
		    if ( this.oTitleBar.div == e.target || this.oTitleBarCell1 == e.target  || this.oTitleBarCell2 == e.target  || this.oTitleBarCell3 == e.target )
		    {
			    this.page.StartDraggingWidget(this, e.clientX, e.clientY);
		    }
	    }
	}
}

function WidgetGetRow()
{
	if( this.area == null)
		return null;
	
	return this.area.FindRow(this);
}


function Spacer(div)
{
  this.base = Division;
  this.base(div);
  this.area = null;
  
  this.GetRow = SpacerGetRow;
}

// return the row for the area the spacer is in
function SpacerGetRow()
{
	if( this.area == null)
		return null;
		
	return this.area.FindRow(this);
}
