// Copyright 2006-2007 javascript-array.com

var timeout	= 5; //500;
var closetimer	= 0;
var ddmenuitem	= 0;

// open hidden layer
function _mopen(id)
{	
	// cancel close timer		
	mcancelclosetime();			

	// close old layer
	if(ddmenuitem) ddmenuitem.style.visibility = 'hidden';

	// get new layer and show it
	ddmenuitem = document.getElementById(id);	
	ddmenuitem.style.visibility = 'visible';		  
}

function mopen(id, navBtnId)
{	
	// cancel close timer
	mcancelclosetime();				

	// close old layer
	if(ddmenuitem) ddmenuitem.style.visibility = 'hidden';		 
									
	// get new layer and show it	  
	ddmenuitem = document.getElementById(crtl_id+id);				
	ddmenuitem.style.visibility = 'visible';	   

	// get the current mouse over button
	var menuBtn = document.getElementById(crtl_id+navBtnId);  
	// save the classname so we can reset it back
	classname = menuBtn.className;
	
	// get the nav buttons that have menus associated with them
	var navHomeBtn = document.getElementById(crtl_id+'menuHome');
	var navConnectBtn = document.getElementById(crtl_id+'menuConnect');
	var navPlayBtn = document.getElementById(crtl_id+'menuPlay');
}
// close showed layer
function mclose()
{					  
	if(ddmenuitem) 
	{
		ddmenuitem.style.visibility = 'hidden';	  	  
		// reset the class name back to original value
		if(ddmenuitem.name)
		{								  		
			document.getElementById(crtl_id+ddmenuitem.name).className=classname;  
		}
	}																
}

// go close timer
function mclosetime()
{
	closetimer = window.setTimeout(mclose, timeout);
}

// cancel close timer
function mcancelclosetime()
{
	if(closetimer)
	{
		window.clearTimeout(closetimer);
		closetimer = null;
	}
}

// close layer when click-out
document.onclick = mclose; 
