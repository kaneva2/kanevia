var popup = null,
closedTimer = null
statusTimer = null,
import_id = null,
importer = null,
abort = false,
state = null,
processing = false;
service = "";

$j(document).ready(function () {
    $j('.checkall').change(function () {
        SelectAll($j(this).prop('checked'));
    });
    $j('#btnSendInvites').click(function () {
        $j('#sendinginvites').show();
        $j(this).hide();
    });
});

/* 
This code is used to rebind the onchange event handler after the
Ajax call is made back to the page to display a different step in
the process.  Without this code the Select All checkbox on Step 3 will not work */
var prm = Sys.WebForms.PageRequestManager.getInstance();
prm.add_endRequest(function () {
    // re-bind your jQuery events here
    $j(".checkall").change(function () {
        SelectAll($j(this).prop('checked'));
    });
    $j('.email-provider').click(function (ev) {
        ConnectToService(ev, $j(this).attr('id'));
    });
    $j('#btnSendInvites').click(function () {
        $j('#sendinginvites').css('display','inline-block');
        $j(this).hide();
    });
});

function SelectAll(checkit) {
	var theForm = document.forms[0];
	for (var i = 0; i < theForm.elements.length; i++) {
	    if (theForm[i].type == 'checkbox' && !theForm[i].disabled)
	    { theForm[i].checked = checkit; }
	}
}

$j('.email-provider').click(function (ev) {
	ConnectToService(ev, $j(this).attr('id'));
});

function ConnectToService(ev, service) {
	ev.preventDefault();
	cleanUp();
	processing = true;
	    
	$j.colorbox({
	    html: '<p>Importing Contacts...<br/>You may need to disable your pop-up blocker.<br/><br/><img src="'
				+ $j('#sitename').val() + 'images/ajax-loader.gif'
				+ '" alt="" class="invite-loader"/></p>',
	    scrolling: false,
	    initialWidth: 100,
	    initialHeight: 100,
	    width: 400
	});

	if (service == 'gmail' || service == 'yahoo' || service == 'windowslive' || service == 'aol') {
	    popup = window.open('', "login", "height=420,width=600,location=no,toolbar=no,resizable=no,status=no,menubar=no");
	    popup.document.body.innerHTML = "Loading, please wait ...";
	}

	$j.ajax({
	    type: "POST",
	    url: $j('#sitename').val() + "services/csimporter.asmx/Consent",
	    data: '{"service":"' + service + '"}',
	    contentType: "application/json; charset=utf-8",
	    dataType: "json"
	}).done(function (data) {
	    var obj = $j.parseJSON(data.d);
	    if (!obj) {
	        $j('.label').text("We encountered an error trying to connect to " + service);
	        cleanUp();
	        return;
	    } 
	    import_id = obj.ImportId;
	    popup.location = obj.Url;
	    popup.focus();
	    closedTimer = setInterval(checkPopup, 1000);
	    abort = false; 
	    setTimeout(getContacts(service), 3000);
	});
}

var objContacts;
function getContacts(srvc) {
    if (!abort) {
	    $j.ajax({
	        type: "POST",
	        url: $j('#sitename').val() + "services/csimporter.asmx/ImportStatus",
	        data: '{"importId":"' + import_id + '"}',
	        contentType: "application/json; charset=utf-8",
	        dataType: "json"
	    }).done(function (data) {
	        var obj = $j.parseJSON(data.d);
	        if (!obj) {
	            $j('.label').text("We encountered an error retrieving your contacts");
	            cleanUp();
	            return;
	        }
	        state = obj.State;
	        if (obj.IsComplete === true && obj.IsError === false) {
	            import_id = obj.ImportId;
	            document.location = $j('#sitename').val() + "mykaneva/inviteFriend.aspx?id=" + import_id + "&service=" + srvc;
	        } else {
	            setTimeout(getContacts(srvc), 2000);
	        }
	    });
	}
}

function cleanUp() {
	if ($j.colorbox) { $j.colorbox.close(); }
	if (popup && !popup.closed) {
		popup.close();
	}
	clearInterval(closedTimer);
	clearInterval(statusTimer);
	popup = null;
	closedTimer = null;
	statusTimer = null;
	import_id = null;
	state = null;
	importer = null;
	processing = false;
}

function abortImport() {
	abort = true;
	if (importer) {
		importer.abort();
	}
	cleanUp();
}

function checkPopup() {
	if (!popup || popup.closed) {
		popup = null;
		clearInterval(closedTimer);
		closedTimer = null;
		statusTimer = setInterval(checkStatus, 2000);
	}
}

function checkStatus() {
	if (state === null || state === 'inprogress') {
		abortImport();
	}
}
    
function GetParameterValues(param) {  
    var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');  
    for (var i = 0; i < url.length; i++) {  
        var urlparam = url[i].split('=');  
        if (urlparam[0] == param) {  
            return urlparam[1];  
        }  
    }
    return null;  
}

function showMessage(msg, err) {
    $j('#feedback p').text(msg);
    if (msg == '') {
        $j('#feedback').hide();
    } else {
        if (err) {
            $j('#feedback').addClass('err');
        }
        $j('#feedback').show();
    }
}

function ScrollToTop() {
    $j('html, body').animate({ scrollTop: 0 }, 'slow');
}
 