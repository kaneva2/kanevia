﻿document.write("<script type=\"text/javascript\" src=\"../../jscript/base/Base.js\"></script>");

function BlastCommInit(eId, ebId) {   
    var ajaxRequest = new Ajax.Request(commentCtrlPath + 'EventCommentsData.aspx', {
        method: 'get',
        parameters: 'cpp=2&eId=' + eId + '&ebId=' + ebId,
        asynchronous: true,
        evalJS: true,
        evalScripts: true,
        onComplete: eval('showComments' + eId + ebId),
        onFailure: BlastCommError
    });
}

function ViewAllComments(eId, ebId, viewLinkDiv) {
    if ($(viewLinkDiv) != null) { $(viewLinkDiv).style.display = 'none'; };
    var ajaxRequest = new Ajax.Request(commentCtrlPath + 'EventCommentsData.aspx',
    {
        method: 'get',
        parameters: 'cpp=1000&eId=' + eId + '&ebId=' + ebId,
        asynchronous: true,
        evalJS: true,
        evalScripts: true,
        onComplete: eval('showComments' + eId + ebId),
        onFailure: BlastCommError
    });
}

function BlastCommError(error) {
    var val = error.responseText;
    alert(val);
}

// 
function AddComment(eId, ebId, txtbx, btn, div) {
    if (txtbx.value.length > 0 && txtbx.value != defText) {
        var ajaxRequest = new Ajax.Request(commentCtrlPath + 'EventCommentsData.aspx',
		{
		    method: 'post',
		    parameters: 'cpp=1000&add=true&eId=' + eId + '&ebId=' + encodeURIComponent(ebId.escapeHTML()) + '&cmt=' + encodeURIComponent(txtbx.value.escapeHTML()),
		    asynchronous: true,
		    evalJS: true,
		    evalScripts: true,
		    onComplete: eval('showComments' + eId + ebId),
		    onFailure: BlastCommError
		});
        CollapseAddComment(txtbx, btn, div);
    }
}

function ShowAddComment(txt, btnId) {    
    if (txt.value == defText) {
        txt.value = '';
        txt.rows = 2;
    }
    $(btnId).style.display = 'block';
}

function ResizeTextArea(txt) {
    if (txt.value.length > 140) {
        txt.value = txt.value.substring(0, 140);
    }
    else {
        if (txt.rows < 3) {
            var cnt = Math.ceil(txt.value.length / 32)
            if (cnt > txt.rows) {
                txt.rows++;
            }
        }
    }
}

function CollapseAddComment(txt, btn, div) {
    try {
        txt.value = defText;
        txt.rows = 1;
        btn.style.display = 'none';
        div.style.display = 'none';
    } catch (err) { }
}

function ResetAddComment(txt) {
    if (txt.value.blank()) {
        txt.className = 'textInput';
        txt.value = defText;
    }
}

var defBlastText = 'Blast to this event...';
var defText = 'Add a Comment...';

function DeleteComment(eId,ebId,cmtId) {
    var ajaxRequest = new Ajax.Request(commentCtrlPath + 'EventCommentsData.aspx',
    {
        method: 'get',
        parameters: 'del=true&cpp=1000&eId=' + eId + '&ebId=' + ebId + '&cmtId=' + cmtId,
        asynchronous: true,
        evalJS: true,
        evalScripts: true,
        onComplete: eval('showComments' + eId + ebId),
        onFailure: BlastCommError
    });
}

function ShowAddBlast(txt, btnId) {
    if (txt.value == defBlastText) {
        txt.value = '';
        txt.rows = 2;
    }
    $(btnId).style.display = 'block';
}
function ResetAddBlast(txt) {
    if (txt.value.blank()) {
        txt.className = 'textInput';
        txt.value = defBlastText;
    }
}
