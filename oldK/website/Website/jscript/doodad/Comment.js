var defBlastText = 'Blast to this event...';
var defText = 'Add a Comment...';

var type = {
    BLAST: "blast",
    CONTEST: "contest",
    EVENT: "event"
}

function Args(args) {
    this.type = (args.type === type.BLAST || args.type === type.CONTEST || args.type === type.EVENT) ? args.type : eval(args.type);
    this.parentId = args.entryId ? args.entryId : args.contestId ? args.contestId : args.eventId;
    this.childId = args.contestEntryId ? args.contestEntryId : args.eventBlastId ? args.eventBlastId : "";
    this.viewall = args.viewAllContainer ? args.viewAllContainer : null;
    this.commentId = args.commentId ? args.commentId : null;
    this.txt = args.txt ? args.txt : null;
    this.btn = args.btn ? args.btn : null;
    this.div = args.div ? args.div : null;
}

function ShowComments(params, data) {
    $j('#pComments' + params.parentId + params.childId).html(data);
}

// the params input is different depending on which control is calling it. 
// BLAST   - {type: ,entryId: }
// CONTEST - {type: ,contestId: ,contestEntryId: } 
// EVENT   - {type: ,eventId: ,eventBlastId: }
// The different parameters are handled in the creation to the Args object
function BlastCommInit(params) {
    var args = new Args(params);

    if (args.type === type.BLAST) {
        args.dataParam = "cpp=2&eId=" + args.parentId;
    } else if (args.type === type.CONTEST) {
        args.dataParam = "cpp=2&cId=" + args.parentId + "&eId=" + args.childId;
    } else if (args.type === type.EVENT) {
        args.dataParam = "cpp=2&eId=" + args.parentId + "&ebId=" + args.childId;
    }

    $j.ajax({
        type: "GET",
        url: commentCtrlPath + args.type + 'CommentsData.aspx',
        data: args.dataParam,
        contentType: "application/text; charset=utf-8",
        dataType: "text"
    }).done(function (data) {
        ShowComments(args, data);
    }).fail(function () {
        // error
    });
}

// the params input is different depending on which control is calling it. 
// BLAST   - {type: ,entryId: ,viewAllContainer: }
// CONTEST - {type: ,contestId: ,contestEntryId: ,viewAllContainer: } 
// EVENT   - {type: ,eventId: ,eventBlastId: ,viewAllContainer: }
// The different parameters are handled in the creation to the Args object
function ViewAllComments(params) {
    var args = new Args(params);

    if (args.type === type.BLAST) {
        args.dataParam = "cpp=1000&eId=" + args.parentId;
    } else if (args.type === type.CONTEST) {
        args.dataParam = "cpp=1000&cId=" + args.parentId + "&eId=" + args.childId;
    } else if (args.type === type.EVENT) {
        args.dataParam = "cpp=1000&eId=" + args.parentId + "&ebId=" + args.childId;
    }

    if (args.viewall != null) {
        args.viewall.hide();
    };

    $j.ajax({
        type: "GET",
        url: commentCtrlPath + args.type + "CommentsData.aspx",
        data: args.dataParam,
        contentType: "application/text; charset=utf-8",
        dataType: "text"
    }).done(function (data) {
        ShowComments(args, data);
    }).fail(function () {
        // error 
    });
}

// the params input is different depending on which control is calling it. 
// BLAST   - {type: ,entryId: ,txt: ,btn: ,div: }
// CONTEST - {type: ,contestId: ,contestEntryId: ,txt: ,btn: ,div: } 
// EVENT   - {type: ,eventId: ,eventBlastId: ,txt: ,btn: ,div: }
// The different parameters are handled in the creation to the Args object
function AddComment(params) {
    var args = new Args(params);
    args.dataParam = "cpp=1000&add=true&";

    if (args.txt.val().length > 0 && args.txt.val() != defText) {
        if (args.type === type.BLAST) {
            args.dataParam += "eId=" + args.parentId + "&cmt=" + encodeURIComponent(args.txt.val().escapeHTML());
        } else if (args.type === type.CONTEST) {
            args.dataParam += "cId=" + args.parentId + "&eId=" + args.childId + "&cmt=" + encodeURIComponent(args.txt.val().escapeHTML());
        } else if (args.type === type.EVENT) {
            args.dataParam += "eId=" + args.parentId + "&ebId=" + encodeURIComponent(args.childId.escapeHTML()) + "&cmt=" + encodeURIComponent(args.txt.val().escapeHTML());
        }

        $j.ajax({
            type: "GET",
            url: commentCtrlPath + args.type + 'CommentsData.aspx',
            data: args.dataParam,
            contentType: "application/text; charset=utf-8",
            dataType: "text"
        }).done(function (data) {
            ShowComments(args, data);
        }).fail(function () {
            // error
        });

        CollapseAddComment(args);
    }
}

// the params input is different depending on which control is calling it. 
// BLAST   - {type: ,entryId: ,commentId: }
// CONTEST - {type: ,contestId: ,contestEntryId: ,commentId: } 
// EVENT   - {type: ,eventId: ,eventBlastId: ,commentId: }
// The different parameters are handled in the creation to the Args object
function DeleteComment(params) {
    var args = new Args(params);
    args.dataParam = "cpp=1000&del=true&";

    if (args.type === type.BLAST) {
        args.dataParam += "eId=" + args.parentId;
        $j('#div' + args.parentId).hide();
    } else if (args.type === type.CONTEST) {
        args.dataParam += "cId=" + args.parentId + "&eId=" + args.childId + "&cmtId=" + args.commentId;
    } else if (args.type === type.EVENT) {
        args.dataParam += "eId=" + args.parentId + "&ebId=" + args.childId + "&cmtId=" + args.commentId;
    }

    $j.ajax({
        type: "GET",
        url: commentCtrlPath + args.type + "CommentsData.aspx",
        data: args.dataParam,
        contentType: "application/text; charset=utf-8",
        dataType: "text"
    }).done(function (data) {
        ShowComments(args, data);
    }).fail(function () {
        // error
    });
}

function ShowAddComment(txt, btn, txtMsg) {
    if (txt.val() == txtMsg) {
        txt.val("");
        txt.attr("rows", "2");
    }
    btn.css("display", "block");
}

function ResizeTextArea(txt) {
    if (txt.val().length > 140) {
        txt.val(txt.val().substring(0, 140));
    } else {
        if (txt.attr("rows") < 3) {
            var cnt = Math.ceil(txt.val().length / 32)
            if (cnt > txt.attr("rows")) {
                txt.attr("rows", cnt);
            }
        }
    }
}

function CollapseAddComment(args) {
    try {
        args.txt.val(defText);
        args.txt.attr("rows", "1");
        args.btn.hide();
        args.div.hide();
    } catch (err) { }
}

function ResetAddComment(txt, txtMsg) {
    if (txt.val().length == 0) {
        txt.addClass("textInput");
        txt.val(txtMsg);
    }
}


var $j = jQuery.noConflict();
$j(document).ready(function () {
    $j('body').on('focus', '.txtComment', function () {
        ShowAddComment($j(this), $j(this).parent().find('a'), defText);
    })
    .on('blur', '.txtComment', function () {
        ResetAddComment($j(this),defText);
    })
    .on('keyup', '.txtComment', function () {
        ResizeTextArea($j(this));
    });
});
