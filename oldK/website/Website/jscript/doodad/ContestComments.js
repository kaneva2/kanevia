﻿/*  Working via jQuery.  Need to convert remaining methods
var $j = jQuery.noConflict();

function BlastCommInit(cId, eId) {
    $j.ajax({
        type: "GET",
        url: commentCtrlPath + "ContestCommentsData.aspx",
        data: "cpp=2&cId=" + cId + "&eId=" + eId,
        cache: false,
        success: function (data) {
            $j("#pComments" + cId + eId).html(data);
        },
        error: function (error) {
            var val = error.responseText;
            alert();
        }
    });
}
*/

function BlastCommInit(cId,eId) {
    var ajaxRequest = new Ajax.Request(commentCtrlPath + 'ContestCommentsData.aspx', {
        method: 'get',
        parameters: 'cpp=2&cId=' + cId + '&eId=' + eId,
        asynchronous: true,
        evalJS: true,
        evalScripts: true,
        onComplete: eval('showComments' + cId + eId),
        onFailure: BlastCommError
    });
}

function ViewAllComments(cId, eId, viewLinkDiv) {
    if ($(viewLinkDiv) != null) { $(viewLinkDiv).style.display = 'none'; };
    var ajaxRequest = new Ajax.Request(commentCtrlPath + 'ContestCommentsData.aspx',
    {
        method: 'get',
        parameters: 'cpp=1000&cId=' + cId + '&eId=' + eId,
        asynchronous: true,
        evalJS: true,
        evalScripts: true,
        onComplete: eval('showComments' + cId + eId),
        onFailure: BlastCommError
    });
}

function BlastCommError(error) {
    var val = error.responseText;
    alert(val);
}

// 
function AddComment(cId, eId, txtbx, btn, div) {
    if (txtbx.value.length > 0) {
        var ajaxRequest = new Ajax.Request(commentCtrlPath + 'ContestCommentsData.aspx',
		{
		    method: 'post',
		    parameters: 'cpp=1000&add=true&cId=' + cId + '&eId=' + eId + '&cmt=' + encodeURIComponent(txtbx.value.escapeHTML()),
		    asynchronous: true,
		    evalJS: true,
		    evalScripts: true,
		    onComplete: eval('showComments' + cId + eId),
		    onFailure: BlastCommError
		});
        CollapseAddComment(txtbx, btn, div);
    }
}

function ShowAddComment(txt, btnId) {
    if (txt.value == defText) {
        txt.value = '';
        txt.rows = 2;
    }
    $(btnId).style.display = 'block';
}

function ResizeTextArea(txt) {
    if (txt.value.length > 140) {
        txt.value = txt.value.substring(0, 140);
    }
    else {
        if (txt.rows < 3) {
            var cnt = Math.ceil(txt.value.length / 32)
            if (cnt > txt.rows) {
                txt.rows++;
            }
        }
    }
}

function CollapseAddComment(txt, btn, div) {
    try {
        txt.value = defText;
        txt.rows = 1;
        btn.style.display = 'none';
        div.style.display = 'none';
    } catch (err) { }
}

function ResetAddComment(txt) {
    if (txt.value.blank()) {
        txt.className = 'textInput';
        txt.value = defText;
    }
}

var defText = 'Add a Comment...';

function DeleteComment(cId,eId,cmtId) {
    var ajaxRequest = new Ajax.Request(commentCtrlPath + 'ContestCommentsData.aspx',
    {
        method: 'get',
        parameters: 'del=true&cpp=1000&cId=' + cId + '&eId=' + eId + '&cmtId=' + cmtId,
        asynchronous: true,
        evalJS: true,
        evalScripts: true,
        onComplete: eval('showComments' + cId + eId),
        onFailure: BlastCommError
    });
}
