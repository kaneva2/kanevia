
function Division(div)
{
	this.div = div;

	// accessors
	this.GetTop			= DivisionGetTop;
	this.GetLeft		= DivisionGetLeft;
	this.GetWidth		= DivisionGetWidth;
	this.GetHeight		= DivisionGetHeight;

	// modifiers
	this.SetVisible		= DivisionSetVisible;
	this.SetDimensions	= DivisionSetDimensions;
	this.SetPosition	= DivisionSetPosition;
	this.SetCursor		= DivisionSetCursor;

	this.isIE = false;
	if ( this.div.style.posWidth )
		this.isIE = true;
}

function DivisionGetTop()
{
	return parseInt(this.div.offsetTop);
}

function DivisionGetLeft()
{
	return parseInt(this.div.offsetLeft);
}

function DivisionGetWidth()
{
	if ( this.isIE )
		return this.div.style.posWidth;
	else
		return parseInt(this.div.style.width);
}

function DivisionGetHeight()
{
	if ( this.isIE )
		return this.div.style.posHeight;
	else
		return parseInt(this.div.style.height);
}

function DivisionSetVisible( visible )
{
    this.div.style.display = visible ? "" : "none";
}

function DivisionSetDimensions( width, height )
{
	if ( this.isIE )
	{
		this.div.style.posWidth		= width;
		this.div.style.posHeight	= height;
	}
	else
	{
		this.div.style.width		= (width > 0 ? width : 0);
		this.div.style.height		= (height > 0 ? height : 0);
	}
}

function DivisionSetPosition( x, y )
{
	if( this.isIE )
	{
		this.div.style.posLeft	= x;
		this.div.style.posTop	= y;
	}
	else
	{
		this.div.style.left		= (x > 0 ? x : 0);
		this.div.style.top		= (y > 0 ? y : 0);
	}
}

function DivisionSetCursor( cursor )
{
	this.div.style.cursor = cursor;
}
