 
function WidgetManager( widget_base_template, widget_template )
{
	this.html_widget_base_template	= $(widget_base_template).innerHTML.replace("<!--", "").replace("-->", "");
    this.html_widget_template		= $(widget_template).innerHTML.replace("<!--", "").replace("-->", "");
    
    this.titleList				= new Object();
    this.descriptionList		= new Object();
    this.iconUrlList			= new Object();
}

WidgetManager.prototype.AddWidgetDescription	= WidgetManager_AddWidgetDescription;
WidgetManager.prototype.CreateBaseWidget		= WidgetManager_CreateBaseWidget;
WidgetManager.prototype.CreateWidget			= WidgetManager_CreateWidget;

function WidgetManager_AddWidgetDescription(type, title, description, iconUrl)
{
    this.iconUrlList[type]		= iconUrl;
    this.titleList[type]		= title;
    this.descriptionList[type]	= description;
}

function WidgetManager_CreateBaseWidget( type, widget_parent )
{
    var parent = $(widget_parent);
    if (parent)
    {
        var html_base_widget	= new String(this.html_widget_base_template);
        html_base_widget		= html_base_widget.replace(/%widget_title%/g, this.titleList[type]).replace(/%widget_icon_url%/g, this.iconUrlList[type]).replace(/%widget_type%/g, type).replace(/%widget_description%/g, this.descriptionList[type]);

        var div					= document.createElement("div");
        div.innerHTML			= html_base_widget;
        
        parent.appendChild(div);
    }
}

function WidgetManager_CreateWidget(type, widget_parent, widget_id, page, bEdit, bDelete, locked)
{
    var parent = $(widget_parent);
    if (parent)
    {
        if (!widget_id)
            widget_id = GenUniqueID();
            
        var html_widget = new String(this.html_widget_template);
        html_widget		= html_widget.replace(/%widget_id%/g, widget_id).replace(/%widget_title%/g, this.titleList[type]).replace(/%widget_icon_url%/g, this.iconUrlList[type]).replace(/%widget_description%/g, this.descriptionList[type]);
        
        var move = "titleBarMove";
        if(locked == true) 
        {
            move = "";
        }    
        html_widget     = html_widget.replace(/%move%/g, move)
        
        var div			= document.createElement("div");
        div.innerHTML	= html_widget;
        parent.appendChild(div);
            
        var widget		= new Widget(page, $(widget_id), type);
        widget.hlEdit.SetVisible(bEdit);
		widget.hlDelete.SetVisible(bDelete);
        
        return widget;
    }
}

