

// TreeView script for AdvancedWebControls.TreeView
// Written by Eilon Lipton (t-eilonl)


// Parameters:
//  object tree             - The TreeView object
//  object obj              - The event's source (an anchor tag)
//  string nodeid           - The ID of the node to perform the operation on
//  object expandedState    - The ExpandedState hidden form field
function TreeViewShowHide(tree, obj, nodeid, expandedState) {
    if (nodeid.children[0].style.display == 'none') {
        // Expand the node
        
        // Change main image to Minus icon
        obj.children[0].src = tree.expandedNodeImageUrl;

        // Set expandedState hidden field to reflect change
        index = parseInt(obj.expandedStateIndex);
        expandedState.value =  expandedState.value.substring(0, index) + 'c' + expandedState.value.slice(index + 1);

        // Set image URL
        if (obj.parentNode.children.length > 1)
            obj.parentNode.children[1].src = obj.expandedImageUrl;

        // Show children
        for (i = 0; i < nodeid.children.length; i++) {
            nodeid.children[i].style.display = '';
        }
    }
    else {
        // Collapse the node

        // Change main image to Plus icon
        obj.children[0].src = tree.collapsedNodeImageUrl;

        // Set expandedState hidden field to reflect change
        index = parseInt(obj.expandedStateIndex);
        expandedState.value =  expandedState.value.substring(0, index) + 'e' + expandedState.value.slice(index + 1);

        // Set image URL
        if (obj.parentNode.children.length > 1)
            obj.parentNode.children[1].src = obj.imageUrl;

        // Hide children
        for (i = 0; i < nodeid.children.length; i++) {
            nodeid.children[i].style.display = 'none';
        }
    }
}

/*
Styles:

TV.NodeStyle
TV.DisabledNodeStyle
TN.NodeStyle

TV.HoverNodeStyle
TV.SelectedNodeStyle
*/

// NOTE: This function is for testing purposes only and will be deleted eventually
function ShowExpandedState() {
    alert('Expanded state 7: ' + document.ctrl2.TreeView1_expandedstate.value);
}
