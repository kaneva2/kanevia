/* This code is based off the the lightbox implementation by Created By: Chris Campbell
	@ http://particletree.com.  This code has been modified as needed for the Kaneva
	specific implementation needs.
*/

/*-------------------------------GLOBAL VARIABLES------------------------------------*/

var detect = navigator.userAgent.toLowerCase();
var OS,browser,version,total,thestring;

var	yPos = 0;
var	xPos = 0;

/*-----------------------------------------------------------------------------------------------*/

Event.observe(window, 'load', getBrowserInfo, false);
Event.observe(window, 'unload', Event.unloadCache, false);

//Browser detect script origionally created by Peter Paul Koch at http://www.quirksmode.org/
function getBrowserInfo() 
{
	if (checkIt('konqueror'))
	{
		browser = "Konqueror";
		OS = "Linux";
	}
	else if (checkIt('safari')) browser 	= "Safari"
	else if (checkIt('omniweb')) browser	= "OmniWeb"
	else if (checkIt('opera')) browser 		= "Opera"
	else if (checkIt('webtv')) browser 		= "WebTV";
	else if (checkIt('icab')) browser 		= "iCab"
	else if (checkIt('msie')) browser 		= "Internet Explorer"
	else if (!checkIt('compatible')) 
	{
		browser = "Netscape Navigator"
		version = detect.charAt(8);
	}
	else browser = "An unknown browser";

	if (!version) version = detect.charAt(place + thestring.length);

	if (!OS) 
	{
		if (checkIt('linux')) OS 		= "Linux";
		else if (checkIt('x11')) OS 	= "Unix";
		else if (checkIt('mac')) OS 	= "Mac"
		else if (checkIt('win')) OS 	= "Windows"
		else OS 						= "an unknown operating system";
	}
}

function checkIt(string) 
{
	place = detect.indexOf(string) + 1;
	thestring = string;
	return place;
}

/*-----------------------------------------------------------------------------------------------*/

// Turn everything on - mainly the IE fixes
function activateLB ()
{							 
	if (browser == 'Internet Explorer')
	{	 
		getScroll();						 
		prepareIE('100%', 'hidden');		 
		setScroll(0,0); 					 
		hideSelects('hidden');				 
	}	  
	displayLB('block');
}

function deactivateLB ()
{
	if (browser == "Internet Explorer")
	{
		setScroll(0,yPos);
		prepareIE('auto', 'auto');
		hideSelects('visible');				 
	}
	displayLB('none');
}
		
// Ie requires height to 100% and overflow hidden or else 
// you can scroll down past the lightbox
function prepareIE(height, overflow)
{
	bod = document.getElementsByTagName('body')[0];
	bod.style.height = height;
	bod.style.overflow = overflow;

	htm = document.getElementsByTagName('html')[0];
	htm.style.height = height;
	htm.style.overflow = overflow; 
}
	
// In IE, select elements hover on top of the lightbox
function hideSelects(visibility)
{
	selects = document.getElementsByTagName('select');
	for(i = 0; i < selects.length; i++) {
		selects[i].style.visibility = visibility;
	}
}
	
// Taken from lightbox implementation found at http://www.huddletogether.com/projects/lightbox/
function getScroll()
{
	if (self.pageYOffset) {
		yPos = self.pageYOffset;
	} else if (document.documentElement && document.documentElement.scrollTop){
		yPos = document.documentElement.scrollTop; 
	} else if (document.body) {
		yPos = document.body.scrollTop;
	}
}
	
function setScroll(x, y)
{
	window.scrollTo(x, y); 
}
	
function displayLB (display)
{   
	if ($('overlay') == null) {addOverlayMarkup();}		
	$('overlay').style.display = display;
	$('lightbox').style.display = display;
}

// Add in markup necessary to make this work. Need to add the 
// Overlay div that holds the shadow but it has to be added
// to the body tag element so it will expand to 100% of the window.
function addOverlayMarkup() 
{   
	bod 		= document.getElementsByTagName('body')[0];
	overlay 	= document.createElement('div');
	overlay.id	= 'overlay';
	bod.appendChild(overlay);
}

