﻿document.write("<script type=\"text/javascript\" src=\"../jscript/base/Base.js\"></script>");

function InitWidgetData(dataPage, dataContainer, errorContainer, parameters, pageCount) {
    InitUpdater(dataPage, dataContainer, errorContainer, 'get', parameters, true, true, true, Insertion.None, pageCount);
}

function Follow(cid, id) {
    var txtSpan = $j("#"+id);
    $j.ajax({
        type: "GET",
        url: "../mykaneva/widgets/ContestActions.aspx",
        data: "&cId=" + cid + "&ca=0&a=" + txtSpan.text(),
        cache: false,
        success: function (txt) {
            try {
                if (txt.length > 0) {
                    txtSpan.toggleClass("unfollow");
                    txtSpan.toggleClass("follow");
                    txtSpan.html(txt);
                }
            } catch (err) { }
        },
        error: function (xhr, status, error) {
            alert(error);
        }
    });
}

function Vote(cid, ceId, id, btnId) {
    var vote_count = $j("#"+id);
    var img_loading = $j("#imgloading" + ceId);
    var btn = $j("#" + btnId);
    btn.hide();
    img_loading.show();
    $j.ajax({
        type: "GET",
        url: "../mykaneva/widgets/ContestActions.aspx",
        data: "&cId=" + cid + "&ceId=" + ceId + "&ca=1",
        success: function (txt) {
            try {
                if (txt.length > 0) {
                    vote_count.html(txt);
                    var votecnt = $j("#votecount");
                    votecnt.html(parseInt(votecnt.html()) + 1);
                }       
                img_loading.hide();
                btn.attr("class", "voted");
                btn.attr("src", "/images/contests/voted.gif");
                btn.attr('disabled', 'disabled');
                btn.show();
            } catch (err) { }
        },
        error: function (xhr, status, error) {
            img_loading.hide();
            btn.attr("class", "voted");
            btn.attr("src", "/images/contests/voted.gif");
            btn.attr('disabled', 'disabled');
            btn.show();
        }
    });
}
