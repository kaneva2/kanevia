/**
 * Convert a single file-input element into a 'multiple' input list
 *
 * Usage:
 *
 *   1. Create a file input element (no name)
 *      eg. <input type="file" id="first_file_element">
 *
 *   2. Create a DIV for the output to be written to
 *      eg. <div id="files_list"></div>
 *
 *   3. Instantiate a MultiSelector object, passing in the DIV and an (optional) maximum number of files
 *      eg. var multi_selector = new MultiSelector( document.getElementById( 'files_list' ), 3 );
 *
 *   4. Add the first element
 *      eg. multi_selector.addElement( document.getElementById( 'first_file_element' ) );
 *
 *   5. That's it.
 *
 *   You might (will) want to play around with the addListRow() method to make the output prettier.
 *
 *   You might also want to change the line 
 *       element.name = 'file_' + this.count;
 *   ...to a naming convention that makes more sense to you.
 * 
 * Licence:
 *   Use this however/wherever you like, just don't blame me if it breaks anything.
 *
 * Credit:
 *   If you're nice, you'll leave this bit:
 *  
 *   Class by Stickman -- http://www.the-stickman.com
 *      with thanks to:
 *      [for Safari fixes]
 *         Luis Torrefranca -- http://www.law.pitt.edu
 *         and
 *         Shawn Parker & John Pennypacker -- http://www.fuzzycoconut.com
 *      [for duplicate name bug]
 *         'neal'
 * 
 *   Hacked beyond Recognition by kaneva
 */
 
function MultiSelector( list_target, max ){

	// Where to write the list
	this.list_target = list_target;
	// How many elements?
	this.count = 0;
	// How many elements?
	this.id = 0;
	// Is there a maximum?
	if( max ){
		this.max = max;
	} else {
		this.max = -1;
	};
	this.elements = new Array(max); 

	/**
	 * Add a new file input element
	 */
	this.addElement = function (element) {

	    // Make sure it's a file input element
	    if (element.tagName == 'INPUT' && element.type == 'file') {

	        // Element name -- what number am I?
	        element.name = 'file_' + this.id++;

	        // Add reference to this object
	        element.multi_selector = this;

	        // New file input
	        var new_element = document.createElement('input');
	        new_element.type = 'file';
	        new_element.id = 'file_' + this.id;

	        // Style the object
	        new_element.size = "43";
	        new_element.className = 'formKanevaText';

	        // What to do when a file is selected	   
	        new_element.onchange = function () {
	            EnableAddButton(true);
	            SetFields(this);
	        }

	        // Don't allow editing of file by typing
	        new_element.onkeydown = function () {
	            this.blur();
	        }

	        // Add new element
	        element.parentNode.insertBefore(new_element, element);

	        // Update list - add new row to upload queue table
	        element.multi_selector.addListRow(element);


	        // Hide this: we can't use display:none because Safari doesn't like it
	        element.style.position = 'absolute';
	        element.style.left = '-1000px';

	        // Add all properties for this file
	        element.Title = document.getElementById('txtTitle').value;
	        element.Description = document.getElementById('txtDescription').value; ;
	        element.Category = $j('#ddlCategories option:selected').val();

	        /*altered to allow for user widget tags*/
	        var tags = document.getElementById('txtTags').value
	        var chanlOwnerTags = document.getElementById('tbx_ChnlOwnr_Tags')
	        if (chanlOwnerTags != null) {
	            tags += " " + chanlOwnerTags.value;
	        }

	        var temp = document.getElementById('tbx_TagField1');
	        if (temp != null) {
	            tags += " " + temp.value;
	        }
	        temp = document.getElementById('tbx_TagField2');
	        if (temp != null) {
	            tags += " " + temp.value;
	        }
	        temp = document.getElementById('tbx_TagField3');
	        if (temp != null) {
	            tags += " " + temp.value;
	        }
	        temp = document.getElementById('tbx_TagField4');
	        if (temp != null) {
	            tags += " " + temp.value;
	        }
	        temp = document.getElementById('tbx_TagField5');
	        if (temp != null) {
	            tags += " " + temp.value;
	        }
	        temp = document.getElementById('tbx_TagField6');
	        if (temp != null) {
	            tags += " " + temp.value;
	        }

	        element.Tags = tags;
	        element.IsRestricted = document.getElementById('chkRestricted').checked ? 1 : 0;

	        if ($j('[name="rdoPattern"] input') != null) {
	            element.Pattern = $j('[name="rdoPattern"] input').is(':checked') ? 1 : 0;
	        }
	        else {
	            element.Pattern = 0;
	        }

	        if ($j('#radPrivate').is(':checked')) {
	            element.Access = $j('#radPrivate').val();
	        }
	        else {
	            element.Access = $j('#radPublic').val();
	        }

	        element.Active = true;


	        // If we've reached maximum number, disable input element
	        if (this.max != -1 && this.count >= this.max - 1) {
	            new_element.disabled = true;
	        };

	        // Add element to elements array
	        this.elements[this.count] = element;

	        // File element counter
	        this.count++;
	        // Most recent element
	        this.current_element = element;

	    } else {
	        // This can only be applied to file input elements!
	        alert('Error: not a file input element');
	    };
	};

	/**
	 * Add a new row to the list of files
	 */
	this.addListRow = function( element ){

		// Add new row element
		var curr_row_num = multi_selector.count+2
		
		var files_table = document.getElementById( 'files_table' );
		var new_row = files_table.rows[curr_row_num];
		var cell_num  = new_row.cells[0];
		var cell_name = new_row.cells[1];
		var cell_type = new_row.cells[2];
		var cell_del  = new_row.cells[3];
		
		
		// Delete button
		var new_row_button = document.createElement( 'input' );
		new_row_button.type = 'image';   
		new_row_button.src = '../images/button_xdelete.gif';   
		
		new_row_button.value = 'Delete';

		// References
		new_row.element = element;

		// Delete function
		new_row_button.onclick = function()
		{	   
			// Set the active state to false
			this.parentNode.parentNode.element.Active = false;
			
			// Remove element from form
			this.parentNode.parentNode.element.parentNode.removeChild( this.parentNode.parentNode.element );
		
			// Decrement counter
			this.parentNode.parentNode.element.multi_selector.count--;
			
			var del_index = this.parentNode.parentNode.element.multi_selector.count;
			
			// Find the deleted element and remove from element array
			for (var i=0; i<multi_selector.count; i++)
			{
				if (multi_selector.elements[i] && !multi_selector.elements[i].Active)
				{
					multi_selector.elements.splice(i,1);
					del_index = i;
					break;
				}
			}

			// Get upload media table 
			var files_table = document.getElementById('files_table');
			
			// Re-number the rows as needed
			for (var i=del_index; i<5; i++)
			{	
					files_table.rows[i+2].cells[0].innerHTML = i;
			}			
			
			// Remove this row from the table
			this.parentNode.parentNode.parentNode.removeChild( this.parentNode.parentNode );

			// Create a new row to replace the deleted row
			var new_row = document.createElement( 'tr' );
			var cell_num  = document.createElement ( 'td' );  
			var cell_name = document.createElement ( 'td' );
			var cell_type = document.createElement ( 'td' );
			var cell_del  = document.createElement ( 'td' );

			// Add row number & set properties on cell
			cell_num.innerHTML = '5';	
			cell_num.style.textAlign = 'center';		
			cell_num.className = 'note';
					
			// Add file extension properties to cell
			cell_type.style.textAlign = 'center';

			// Add delete button properties to cell
			cell_del.style.textAlign = 'center';

			// add the cells to the new row
			new_row.appendChild( cell_num  );  
			new_row.appendChild( cell_name );
			new_row.appendChild( cell_type );
			new_row.appendChild( cell_del  );

			var tbody = this.parentNode.parentNode.element.multi_selector.list_target.getElementsByTagName("tbody")[0];
			tbody.appendChild( new_row );

			// Re-enable input element (if it's disabled)
			var x = 'file_'+multi_selector.id;
			document.getElementById(x).disabled = false;

			if (multi_selector.count == 0)
			{
				EnableAgreementCheckBox(false);
				agreementChanged(false);
			}
			
			// Appease Safari
			//    without it Safari wants to reload the browser window
			//    which nixes your already queued uploads
			return false;
		};

		// Set values of cells
		if (document.all)
		{	
			cell_num.innerText = multi_selector.count+1
			cell_name.innerText = GetFileName(element.value);
			cell_type.innerText = GetFileExtension(element.value).toLowerCase();
		}
		else
		{	
			cell_num.textContent = multi_selector.count+1
			cell_name.textContent = GetFileName(element.value);
			cell_type.textContent = GetFileExtension(element.value).toLowerCase();
		}
		cell_del.appendChild( new_row_button );

	};

};


function SetKeyword(t)
{
	var txtTags = document.getElementById('txtTags');
	
	if (t.selectedIndex > 0)
	{
		if (Trim(txtTags.value) == '')
		{
			document.getElementById('txtTags').value = t.options[t.selectedIndex].text.replace('&', '');
		}
	}
}

function SetTitle(t){
	// Set the title to the file name
	var txt_title = document.getElementById('txtTitle');
	txt_title.value = GetFileName(t.value);
}

function SetFileName(t)
{
	document.getElementById('hidFile').value = t.value;
}


function AddFile ()
{					 
	var ms = multi_selector;
	var obj =  document.getElementById( 'file_' + ms.id );

	// Verify all required fields have values						
	if (Trim(obj.value) != '')
	{	
		
		ms.addElement( obj );
			
		// Clear all fields	  
		ClearAllFields();
		ClearCategories ();	
	}
		
	// Disable Add button		 
	EnableAddButton(false);
	EnableAgreementCheckBox(true);
	
	return false;
}

function EnableAgreementCheckBox(bEnable)
{
	document.getElementById('cbAgreement').disabled = !bEnable;
	if (!bEnable) document.getElementById('cbAgreement').checked = false;
}
 
function EnableAddButton(bEnable) {
                                                                
	var imgAdd = document.getElementById('imgAdd');
	
	if (bEnable)
	{
		imgAdd.src = imgAddBtn[0].src;
	}
	else
	{	 
		imgAdd.src = imgAddBtn[1].src;		
	}

    isAddBtnDisabled = !bEnable;
}

function EnablePatternChkBx(bEnable)
{	
	var cxbxRow = document.getElementById('rwPattern');
	cxbxRow.Visible = bEnable;
	
}

function ClearAllFields()
{
	// Optimize to only traverse the DOM once
	allTexts = $("txtTitle", "txtDescription", "txtTags");
	for(i = 0; i < allTexts.length; i++) 
	{
		allTexts [i].value = '';
	}
	
	// Reset Access
	document.getElementById('radPublic').checked = true;
	
	// Reset Restricted
	document.getElementById('chkRestricted').checked = false;
}

function ClearCategories ()
{	
	var ddl_cat = document.getElementById('ddlCategories');
	
	while (ddl_cat.firstChild) {
		ddl_cat.removeChild(ddl_cat.firstChild);
	}
}

function UploadFiles()
{
	if ( multi_selector.count > 0 )
	{
		ShowLoading(true,'Please wait for the upload to complete...<br/><img src="../images/progress_bar.gif">');

		var x = 1;
		// Get each file input that has been added	
		for (var i=0; i<multi_selector.count; i++)
		{	
			if (multi_selector.elements[i] && multi_selector.elements[i].Active)
			{
				// Create the form fields for all the relevant values for each file
				CreateFormFields(multi_selector.elements[i], x++);
			}
		}
		
		document.getElementById('hidFileCount').value = multi_selector.count;
					
		// All fields have been created, now submit form
		document.forms[0].submit();
	}
	else
		alert('Add a file to upload.');
}

function CreateFormFields(elem, i)
{
	// Create field for Title
	var hidTitle = document.createElement( 'input' );
	hidTitle.type = 'hidden';
	hidTitle.name = 'hidTitle' + i;
	hidTitle.value = elem.Title;
	document.forms[0].appendChild( hidTitle );
	
	// Create field for Description
	var hidDesc = document.createElement( 'input' );
	hidDesc.type = 'hidden';
	hidDesc.name = 'hidDescription' + i;
	hidDesc.value = elem.Description;
	document.forms[0].appendChild( hidDesc );
	
	// Create field for Category
	var hidCategory = document.createElement( 'input' );
	hidCategory.type = 'hidden';
	hidCategory.name = 'hidCategory' + i;
	hidCategory.value = elem.Category;
	document.forms[0].appendChild( hidCategory );

	// Create Field for Tags
	var hidTags = document.createElement( 'input' );
	hidTags.type = 'hidden';
	hidTags.name = 'hidTags' + i;
	hidTags.value = elem.Tags;
	document.forms[0].appendChild( hidTags );

	// Create Field for Access
	var hidAccess = document.createElement( 'input' );
	hidAccess.type = 'hidden';
	hidAccess.name = 'hidAccess' + i;
	hidAccess.value = elem.Access;
	document.forms[0].appendChild( hidAccess );

	// Create Field for Restricted
	var hidRestricted = document.createElement( 'input' );
	hidRestricted.type = 'hidden';
	hidRestricted.name = 'hidRestricted' + i;
	hidRestricted.value = elem.IsRestricted;
	document.forms[0].appendChild( hidRestricted );
	
	// Create Field for storing if the file is a pattern or not
	var hidPattern = document.createElement( 'input' );
	hidPattern.type = 'hidden';
	hidPattern.name = 'hidPattern' + i;
	hidPattern.value = elem.Pattern;
	document.forms[0].appendChild( hidPattern );
}

function GetFileName (filename)
{
    var name;
    
    if (filename.indexOf('/') > -1) 
    {
		name = filename.substring(0, filename.lastIndexOf('.')).substring(filename.lastIndexOf('/')+1,filename.length);
		  
    }
    else
    {
		name = filename.substring(0, filename.lastIndexOf('.')).substring(filename.lastIndexOf('\\')+1,filename.length);
    }
    
    if (name.length > 20)
	{
		name = name.substr(0,20)+'...';
	}
	
	return name;
}

function GetFileExtension (filename)
{
	var fileExtension;
	
	//check to see if this is a pattern or a regular image
	if (($j('[name="rdoPattern"] input') != null) && ($j('[name="rdoPattern"] input').is(':checked')))
	{
		fileExtension = filename.substring( (filename.lastIndexOf('.')),filename.length ) + '-p';
	}
	else
	{
		fileExtension = filename.substring( (filename.lastIndexOf('.')),filename.length );
	}

	return fileExtension;
}

function Trim(s) 
{
	while (s.substring(0,1) == ' ')
	{
		s = s.substring(1, s.length);
	}
	while (s.substring(s.length-1, s.length) == ' ')
	{
		s = s.substring(0,s.length-1);
	}
	return s;
}

