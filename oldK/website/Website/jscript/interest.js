//global variables
var visibleId = 0;
var pageNamingPrefix = ""; /*need in case the asp page is appending prefixes to the client ids - calling page should assign the prefix value as needed */

// An XHR DataSource
var myServer = "./interests/getInterestList.aspx";
var myDataSource = new YAHOO.widget.DS_XHR (myServer, [";", ","]);

function showInput(t, ic_id)
{
    if (visibleId > 0)
    {
        $j('#imgNew_' + visibleId).show();
        $j('#spnAdd_' + visibleId).hide();
    }
    
    visibleId = ic_id;
    
    // reset the message area
    $j('#tdMsg_' + ic_id).addClass('info').text('(start typing to see suggestions)');
    
    t.hide();
    $j('#spnAdd_' + ic_id).show();
    $j('#txt_' + ic_id).focus();

    myDataSource.responseType = YAHOO.widget.DS_XHR.TYPE_FLAT;
    myDataSource.scriptQueryParam = "intr";  
    myDataSource.scriptQueryAppend = 'cat='+ic_id;	  

    var myAutoComp = new YAHOO.widget.AutoComplete('txt_'+ic_id,'myContainer_'+ic_id, myDataSource);   
    myAutoComp.minQueryLength = 2;
    myAutoComp.typeAhead = false;
    myAutoComp.useShadow = true; 
    myAutoComp.allowBrowserAutocomplete = false;
}

function hideInput (ic_id)
{   
    $j('#spnAdd_' + ic_id).hide();
    $j('#imgNew_' + ic_id).show();
    $j('#txt_' + ic_id).val('');
    visibleId = 0;
}

function addInterest (ic_id)
{							   
    var strInterest_old = $('txt_' + ic_id).value.replace(/\s{1,}/g, " ").strip();
    var strInterest = $j.trim($j('#txt_' + ic_id).val().replace(/\s{1,}/g, " "));

    if (strInterest.length < 1) return;
	
	var bSingle = true;
	
    // if items seperated by comma, display multi item lightbox
	if (strInterest.indexOf(',') > -1)
    {	
		var isEmpty = true;
		var temp = new Array();		 
		temp = strInterest.split(',');	 
				
		$j('#spnSingle').html(strInterest);
		$j('#spnMultiple').html('');

		for (var i=0; i<temp.length; i++)
		{
			if (temp[i].strip().length > 0)
			{
				isEmpty = false;
				$j('#spnMultiple').append(temp[i] + '<br/>');
			}	
		}
		
		if (isEmpty)
		{
		    $j('#txt_' + ic_id).val('').focus();
			return;	
		}
		
		bSingle = false;
		
		// show the lightbox
		activateLB();
    }
        
    hideInput (ic_id);

    $j('#' + pageNamingPrefix + 'hidIc_id').val(ic_id);
    $j('#' + pageNamingPrefix + 'hidInterest').val(strInterest);
	
	if (bSingle)
	{
	    $(addBtnId).click();
	}   
}

function displayInterest (interest, id, ic_id)
{
    // hide the lightbox
	deactivateLB();				
									 
    var txt = '';
    
    // Get the interest array
    var arrInterests = ReplaceAll(interest,'@~^~@','\'').split('|^|');
																			
    // Get the ids array			   
    var arrIds = id.split('|');
														   
	// Create UI element for each new interest
	for (var i=0; i<arrIds.length; i++)
    {
		txt = txt + ' <span style="white-space:no-wrap" id="spnInt_'+arrIds[i]+'">' +
			' <a href="javascript:void(0);" title="Remove ' + arrInterests[i] + '" class="nohover"><img border="0" ' +
			' onclick="removeInterest('+arrIds[i]+','+ic_id+')" src="../images/del_sm.gif" /></a> ' +
			'<a class="interestName" id="aInt_'+arrIds[i]+'" href="../people/people.kaneva?int=' + arrInterests[i].replace(/\s{1,}/g,"+") +'" >' + arrInterests[i] +'</a> ' +
			' <span>| </span></span> ';
	}
												  
	$j('#divIC_' + ic_id).append(txt);
        
	resetVals();
        
    for (var i=0; i<arrIds.length; i++)
    {							   
        $j('#aInt_' + arrIds[i]).effect("pulsate", {times:2}, 3000);
	}
        
    return false;
}

function resetVals()
{   
    $j('#' + pageNamingPrefix + 'hidIc_id').val(0);
    $j('#' + pageNamingPrefix + 'hidInterest').val('');
    $j('#' + pageNamingPrefix + 'hidInterestId').val(0);
}

function removeInterest (id, ic_id)
{                                
    $j('#' + pageNamingPrefix + 'hidIc_id').val(ic_id);
    $j('#' + pageNamingPrefix + 'hidInterestId').val(id);
    $(remBtnId).click();
}

function removeItem (id, ic_id)
{
    $j('#spnInt_' + id).hide();
}

function checkKey (e, ic_id) 
{	
    // check for enter key
	if (checkForEnterKey (e))
	{                      
		addInterest($('imgAdd_'+ic_id), ic_id);
	} 
}

function checkLen (obj, ic_id)//
{
	if (obj.value.length > 45)
	{
	    $j('#tdMsg_' + ic_id).addClass('alertmessage').html('Must be between 1 and 50 characters long.');
		if (obj.value.length > 50) {obj.value = obj.value.substr(0,50);}
	}
	else if (obj.value.length > 5) 
	{							 
	    $j('#tdMsg_' + ic_id).removeClass('alertmessage').html('');
    }
}


