// PageDetail.js

function PageDetail()
{
}

PageDetail.prototype.ChangePermission = PageDetailChangePermission;

function PageDetailChangePermission()
{
    var accessDDL = $("ddlPageAccess");
    $("lbPageGroup").disabled = $("ddlPageGroup").disabled = (accessDDL.value == 0 || accessDDL.value == 2);
    if (accessDDL.value == 0 || accessDDL.value == 2)
        $("ddlPageGroup").value = 0;
}