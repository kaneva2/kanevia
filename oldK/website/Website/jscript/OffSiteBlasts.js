function Timer(count)
{
	document.getElementById("lb_time").innerHTML = count + (count > 1 ? " seconds" : " second");
	count = count - 1;
	if(count < 0)
	{
		CloseWindow();
	}
	else
	{
		var t = setTimeout("Timer(" + count + ")", 1000);
	}
}

function copyImg(img)
{
	var photoURL = document.getElementById("txtPhotoURL");
	photoURL.value = img;
}

/*function PullVideos(offSiteContent)
{
	var videos = offSiteContent.embeds;
	var sourceMovies = document.getElementById("sourceMovies");
	var YouTubeVideoLink = offSiteContent.getElementsByName("video_link");
	var YouTubeVideoEmbed = offSiteContent.getElementsByName("embed_code");

    if(YouTubeVideoEmbed.length > 0)//specialty case youtube
    {
		sourceMovies.value = YouTubeVideoEmbed[0].value;
    }
	else if(YouTubeVideoLink.length > 0)//specialty case youtube
	{
		sourceMovies.value = YouTubeVideoLink[0].value;
	}
	else if(videos.length > 0)
	{
		sourceMovies.value = videos[0].src;
	}
}*/

function PullVideos()
{
	var sourceMovies = document.getElementById("sourceMovies");
	var videos = sourceMovies.value.split("|");
	var movieTag = "";
	if(videos.length > 0)
	{
	    for( var i=0; i < videos.length; i++)
	    {
		    var tempID = videos[i].src;	
		    var movieID = "BLASTMOV_" + i;	
		    if(((i + 1)%4 == 0) && (i != 0))
		    {
			    movieTag = movieTag + "<a href=\"#\" onclick=\"copyImg('" + tempID + "')\"><img src=\"" + tempID + "\" id=\"" + movieID + "\" border=\"1\" width=\"100px\" height=\"100px\" /></a><br>";
		    }
		    else
		    {
			    movieTag = movieTag + "<a href=\"#\" onclick=\"copyImg('" + tempID + "')\"><img src=\"" + tempID + "\" id=\"" + movieID + "\" border=\"1\" width=\"100px\" height=\"100px\" /></a>&nbsp;";
		    }
	    }
	    sourceMovies.value = videos[0];
	}
	
	return sourceMovies.value; //movieTag
}

function PullImages()
{
	var sourceImages = document.getElementById("sourceImages");
	//pulls in ALL image and embed values

	var images = sourceImages.value.split("|");		
	var imgtag = "";
	for( var i=0; i < images.length; i++)
	{
		var tempID = images[i];
		var imageID = "BLASTIMG_" + i;	
		if(((i + 1)%4 == 0) && (i != 0))
		{
			imgtag = imgtag + "<a href=\"#\" onclick=\"copyImg('" + tempID + "')\"><img src=\"" + tempID + "\" id=\"" + imageID + "\" border=\"1\" width=\"100px\" height=\"100px\" /></a><br>";
		}
		else
		{
			imgtag = imgtag + "<a href=\"#\" onclick=\"copyImg('" + tempID + "')\"><img src=\"" + tempID + "\" id=\"" + imageID + "\" border=\"1\" width=\"100px\" height=\"100px\" /></a>&nbsp;";
		}
	}
	
	return imgtag;

}

/*function PullImages(offSiteContent)
{
	var sourceImages = document.getElementById("sourceImages");
	//pulls in ALL image and embed values
	//alert("image field retreived");
	var images = offSiteContent.images;
	var imgtag = "";
	//alert(images.length);
	for( var i=0; i < images.length; i++)
	{
		var tempID = images[i].src;	
		if(((i + 1)%4 == 0) && (i != 0))
		{
			imgtag = imgtag + "<a href=\"#\" onclick=\"copyImg('" + tempID + "')\"><img src=\"" + images[i].src + "\" id=\"" + tempID + "\" border=\"1\" width=\"100px\" height=\"100px\" /></a><br>";
		}
		else
		{
			imgtag = imgtag + "<a href=\"#\" onclick=\"copyImg('" + tempID + "')\"><img src=\"" + images[i].src + "\" id=\"" + tempID + "\" border=\"1\" width=\"100px\" height=\"100px\" /></a>&nbsp;";
		}
	}
	//alert("images processed");
	sourceImages.value = imgtag;
	//alert("images stored");

}*/

/*function GetIframeObject()
{
	//gather the iframe/offsite webpage object
	var iframe=document.getElementById("offsiteContent");
	
	var offSiteContent;
	try
	{
		offSiteContent = iframe.contentWindow.document;
	}
	catch(z)
	{
		offSiteContent = iframe.contentDocument;
	}
	return offSiteContent;
}*/



/*function ReadSourceDOM()
{		
	//gathers all pictures on page
	PullImages();
	
	//gathers all videos on page
	PullVideos();
	
	//call display for intial setup/load
	GetOffsiteBlastInfo();

}*/

function GetOffsiteBlastInfo()
{	
    //used to handle this functions call after blast submission
    if(document.getElementById("blastSuccess") != null)
    {
        return;
    }
   
    //check to see if log in is required
    if(document.getElementById("loginRow") != null)
    {
        return;
    }
    //display loading message
    var loading = document.getElementById("loading");
	loading.style.display = "block";

	//gather the URL sent values
	var sourceURL = document.getElementById("sourceURL");
	var selectedText = document.getElementById("sourceSelectedText");
	var sourceTitle = document.getElementById("sourceTitle");

	//get image and movie storage devices
	var sourceImages = document.getElementById("sourceImages");
	var sourceMovies = document.getElementById("sourceMovies");
    
	//read users display choice
	var option = document.getElementById("tabChoice");

	//display user's choice
	switch(option.value)
	{
		case "images":
			//displays all images on source page
			var imageDiv = document.getElementById("photoList");			
			//imageDiv.innerHTML = sourceImages.value;
			imageDiv.innerHTML = PullImages();
			
			break
		case "videos":
			//displays all videos on page
			var videoLink = document.getElementById("txtVideoEmbed");
			videoLink.value = sourceMovies.value;
			videoLink.value = PullVideos();

			break
		case "links":
			//gathers link (pages name and URL)
			var LinkName = document.getElementById("txtLinkName");
			var LinkURL = document.getElementById("txtLinkURL");
			
			LinkURL.value = sourceURL.value;
			LinkName.value = sourceTitle.value;
			break
		//default:
		case "text":
			//gathers (highlighted) text in parent body
			var text = document.getElementById("txtBlast");
			if((selectedText.value != "") && (selectedText.value != null))
			{
				text.value = selectedText.value;
			}
			break
	}
	    
	//hide loading screen
	loading.style.display = "none";
}
			
