///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Kaneva
{
    public partial class GenericLandingPageTemplate : System.Web.UI.MasterPage
    {
        #region Declarations

        private string m_Title = "Kaneva - Imagine What You Can Do";
        private string m_MetaTitle = "";
        private string m_Description = "";
        private string m_Keywords = "";

        #endregion

        #region Page Load

        private void Page_Load(object sender, System.EventArgs e)
        {
            // populates title bar
            if (m_Title.Length > 0)
            {
                litTitle.Text = m_Title;
            }

            // populates meta data description
            if (m_MetaTitle.Length > 0)
            {
                litMetaDataTitle.Text = "<meta name=\"title\" content=\"" + m_MetaTitle + "\" />\n";
            }
            // populates meta data description
            if (m_Description.Length > 0)
            {
                litMetaDataDescription.Text = "<meta name=\"description\" content=\"" + m_Description + "\" />\n";
            }
            // populates meta data keywords
            if (m_Keywords.Length > 0)
            {
                litMetaDataKeywords.Text = "<meta name=\"keywords\" content=\"" + m_Keywords + "\" />\n";
            }

            // set the css
            litMasterCSS.Text = "<link href=\"" + ResolveUrl("~/css/base/base.css") + "\" type=\"text/css\" rel=\"stylesheet\" />\n" +
                                "<link href=\"" + ResolveUrl("~/css/base/footer.css") + "\" type=\"text/css\" rel=\"stylesheet\" />\n" +
                                "<link href=\"" + ResolveUrl("~/css/landing_page/landingPageBase.css") + "\" type=\"text/css\" rel=\"stylesheet\" />\n" +
                                "<!--[if lte IE 8]><link href=\"" + ResolveUrl("~/css/landing_page/browserFixBase.css") + "\" type=\"text/css\" rel=\"stylesheet\" /><![endif]-->\n";

            // set the js
            litMasterJS.Text =  "<script type=\"text/javascript\" src=\"" + ResolveUrl("~/jscript/jquery/jquery-1.8.2.min.js") + "\"></script>\n" + 
                                "<script type=\"text/javascript\" src=\"" + ResolveUrl("~/jscript/landing_page/base.js") + "\"></script>\n" +
                                "<script language=\"JavaScript\" type=\"text/javascript\">var root=\"" + ResolveUrl ("~/") + "\"</script>";
        }
        #endregion

        #region Attribute Functions

        /// <summary>
        /// The page title property
        /// </summary>
        public string Title
        {
            set
            {
                if (value != string.Empty)
                {
                    m_Title = value;
                }
            }
            get
            {
                return m_Title;
            }
        }

        /// <summary>
        /// The page meta title property
        /// </summary>
        public string MetaDataTitle
        {
            set
            {
                if (value != string.Empty)
                {
                    m_MetaTitle = value;
                }
            }
            get
            {
                return m_MetaTitle;
            }
        }

        /// <summary>
        /// The page description meta tag property
        /// </summary>
        public string MetaDataDescription
        {
            set
            {
                if (value != string.Empty)
                {
                    m_Description = value;
                }
            }
            get
            {
                return m_Description;
            }
        }

        /// <summary>
        /// The page keywords meta tag property
        /// </summary>
        public string MetaDataKeywords
        {
            set
            {
                if (value != string.Empty)
                {
                    m_Keywords = value;
                }
            }
            get
            {
                return m_Keywords;
            }
        }

        #endregion
    }
}
