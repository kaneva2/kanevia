///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Kaneva
{
    public partial class ProfilePageTemplate : System.Web.UI.MasterPage
    {
        #region Declarations

        private string m_Title = "Kaneva - Imagine What You Can Do";

        #endregion

        #region Page Load

        private void Page_Load(object sender, System.EventArgs e)
        {
            // populates title bar
            if (m_Title.Length > 0)
            {
                litTitle.Text = m_Title;
            }

            litJavascripts.Text = "<script language=\"JavaScript\" type=\"text/javascript\">var root=\"" + ResolveUrl ("~/") + "\"</script>";

        }
        #endregion

        #region Attribute Functions

        public Header HeaderNav
        {
            get
            {
                return navDefault;
            }
        }

        /// <summary>
        /// The page title property
        /// </summary>
        public string Title
        {
            set
            {
                if (value != string.Empty)
                {
                    m_Title = value;
                }
            }
            get
            {
                return m_Title;
            }
        }

        public Footer2 Footer
        {
            get
            {
                return ftrDefault;
            }
        }

        #endregion

    }
}
