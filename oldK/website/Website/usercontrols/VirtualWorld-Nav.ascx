<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VirtualWorld-Nav.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.usercontrols.VirtualWorld_Nav" %>

<div class="module whitebg">
	<span class="ct"><span class="cl"></span></span>
				
<div id="browseMenu_" style="display:none;">

    <!-- What's New -->
    <div  class="menutitle <%if(Request.QueryString["s"] == "1"){ %> caton <%}%>"><a href="virtual-life.kaneva" target="_parent">What's New</a></div>

	<!-- Fame & Fortune -->
    <div class="menutitle <%if(Request.QueryString["s"] == "7"){ %> caton <%}%>"><a href="world-of-kaneva-fame.kaneva" target="_parent">
        
        <% if (Request.QueryString["s"] != "7"){ %><img src="http://www.kaneva.com/images/arrow_pointright.gif" border="0"  /><%}
        else {%> <img src="http://www.kaneva.com/images/arrow_pointdown.gif"  border="0"  /> <% } %>
        
        Fame & Fortune</a>
    </div>
    <div id="sortlinks" <% if (Request.QueryString["s"] != "7") { %>style="display:none;" <%} %>  >
	    <ul>
		    <li <%if(Request.QueryString["ss"] == "world-of-kaneva-fame"){ %> class="selected" <%}%> ><a href="world-of-kaneva-fame.kaneva" target="_parent">World of Kaneva Fame</a></li>
		    <li <%if(Request.QueryString["ss"] == "dp3d-dance-fame"){ %> class="selected" <%}%>><a href="dp3d-dance-fame.kaneva" target="_parent">Dance Fame</a></li>		    
		    <li <%if(Request.QueryString["ss"] == "fame-how-to"){ %> class="selected" <%}%>><a href="fame-how-to.kaneva" target="_parent">Fame Guide</a></li>
	    </ul>
    </div>

    <!-- Shopping -->
    <div class="menutitle <%if(Request.QueryString["s"] == "2"){ %> caton <%}%>" ><a href="<%= ShoppingSiteName() %>" target="_blank">Shopping</a>
    </div>

    <!-- Member Homes -->
    <div class="menutitle <%if(Request.QueryString["s"] == "3"){ %> caton <%}%>"><a href="3-d-homes.kaneva" target="_parent">Member Homes</a></div>

    <!-- Member Hangouts -->
    <div class="menutitle <%if(Request.QueryString["s"] == "4"){ %> caton <%}%>"><a href="online-avatar-community.kaneva" target="_parent">Member Hangouts</a></div>


    <!-- Places to go -->
    <div class="menutitle <%if(Request.QueryString["s"] == "5"){ %> caton <%}%>"><a href="world-of-kaneva.kaneva" target="_parent">
        
        <% if (Request.QueryString["s"] != "5"){ %><img src="http://www.kaneva.com/images/arrow_pointright.gif" border="0"  /><%}
        else {%> <img src="http://www.kaneva.com/images/arrow_pointdown.gif"  border="0"  /> <% } %>
        
        Places To Go</a>
    </div>
    <div id="sortlinks" <% if (Request.QueryString["s"] != "5") { %>style="display:none;" <%} %>  >
	    <ul>
		    <li <%if(Request.QueryString["ss"] == "kaneva-city"){ %> class="selected" <%}%> ><a href="kaneva-city.kaneva" target="_parent">Kaneva City</a></li>
		    <li <%if(Request.QueryString["ss"] == "underground"){ %> class="selected" <%}%>><a href="underground-kaneva-club.kaneva" target="_parent">Underground</a></li>
		    <li <%if(Request.QueryString["ss"] == "plaza"){ %> class="selected" <%}%>><a href="kaneva-plaza.kaneva" target="_parent">Kaneva Plaza</a></li>
		    <li <%if(Request.QueryString["ss"] == "third-dimension"){ %> class="selected" <%}%>><a href="third-dimension-kaneva-dance.kaneva" target="_parent">Third Dimension</a></li>
		    <li <%if(Request.QueryString["ss"] == "kaneva-help"){ %> class="selected" <%}%>><a href="kaneva-help-center.kaneva" target="_parent">Help Center</a></li>
		    <li <%if(Request.QueryString["ss"] == "tbs-headquarters"){ %> class="selected" <%}%>><a href="tbs-headquarters.kaneva" target="_parent">TBS Headquarters</a></li>
		    <li <%if(Request.QueryString["ss"] == "tnt-lot"){ %> class="selected" <%}%>><a href="tnt-backlot-sets.kaneva" target="_parent">The TNT Lot</a></li>
	    </ul>
    </div>

    <!-- Special Features -->
    <div class="menutitle <%if(Request.QueryString["s"] == "6"){ %> caton <%}%>"><a href="special-features.kaneva" target="_parent">
    
         <% if (Request.QueryString["s"] != "6" && Request.QueryString["s"] != "1"){ %><img src="http://www.kaneva.com/images/arrow_pointright.gif"  border="0"  /><%}
        else {%> <img src="http://www.kaneva.com/images/arrow_pointdown.gif"  border="0"  /> <% } %>
        
    
     Special Features</a>
    </div>
    <div id="sortlinks" <% if (Request.QueryString["s"] != "6" && Request.QueryString["s"] != "1") { %>style="display:none;" <%} %>>
        <ul>
            <li <%if(Request.QueryString["ss"] == "access-pass"){ %> class="selected" <%}%>><a href="adult-chat-rooms.kaneva" target="_parent">Access Pass</a></li>
            <li <%if(Request.QueryString["ss"] == "cover-charge"){ %> class="selected" <%}%>><a href="virtual-dj.kaneva" target="_parent">Cover Charge</a></li>
            <li <%if(Request.QueryString["ss"] == "dance-party"){ %> class="selected" <%}%>><a href="virtual-dance-game.kaneva" target="_parent">Dance Party 3D</a></li>
            <li <%if(Request.QueryString["ss"] == "emotes-animations"){ %> class="selected" <%}%>><a href="animated-avatars.kaneva" target="_parent">Emotes & Animations</a></li>
            <li <%if(Request.QueryString["ss"] == "flash-widgets"){ %> class="selected" <%}%>><a href="flash-widgets.kaneva" target="_parent">Flash Widgets</a></li>
            <li <%if(Request.QueryString["ss"] == "ka-ching"){ %> class="selected" <%}%>><a href="kaneva-ka-ching.kaneva" target="_parent">Ka-ching!</a></li>
            <li <%if(Request.QueryString["ss"] == "tv-channels"){ %> class="selected" <%}%>><a href="tv-online.kaneva" target="_parent">TV Channels</a></li>
            <li <%if(Request.QueryString["ss"] == "texture-patterns"){ %> class="selected" <%}%>><a href="create-3-d-objects.kaneva" target="_parent">Texture Patterns</a></li>
            <!-- <li <%if(Request.QueryString["ss"] == "artist"){ %> class="selected" <%}%>><a href="artists.kaneva" target="_parent">Artist Network</a></li> -->
        </ul>
    </div>
    
</div>

<div id="tertlinks" style="display:none;">
    <ul>
        <li><a href="http://forums.kaneva.com/" target="_new">Forums</a></li>
        <!--<li><a href="http://www.kaneva.com/channel/KanevaEliteDevelopers.channel" target="_parent">Elite Developers</a></li>-->
        <li><a runat="server" id="aSupport" href="#" target="_parent">Support Center</a></li>
        <li><a href="http://blog.kaneva.com" target="_parent">Kaneva Blog</a></li>
        <li><a href="http://ideas.kaneva.com" target="_parent">Kaneva Ideas</a></li>
    </ul>
</div>


<div id="browseMenu" >

    <!-- What's New -->
    <div class="menutitle">In 3D</div>

    <!-- Places to go -->
    <div class="menutitle <%if(Request.QueryString["s"] == "5"){ %> caton <%}%>"><a href="world-of-kaneva.kaneva" target="_parent">
        
        <% if (Request.QueryString["s"] != "5"){ %><img src="http://www.kaneva.com/images/arrow_pointright.gif" border="0"  /><%}
        else {%> <img src="http://www.kaneva.com/images/arrow_pointdown.gif"  border="0"  /> <% } %>
        
        Places To Go</a>
    </div>
    <div id="sortlinks" <% if (Request.QueryString["s"] != "5") { %>style="display:none;" <%} %>  >
	    <ul>
		    <li <%if(Request.QueryString["ss"] == "kaneva-city"){ %> class="selected" <%}%> ><a href="kaneva-city.kaneva" target="_parent">Kaneva City</a></li>
		    <li <%if(Request.QueryString["ss"] == "underground"){ %> class="selected" <%}%>><a href="underground-kaneva-club.kaneva" target="_parent">Underground</a></li>
		    <li <%if(Request.QueryString["ss"] == "plaza"){ %> class="selected" <%}%>><a href="kaneva-plaza.kaneva" target="_parent">Kaneva Plaza</a></li>
		    <li <%if(Request.QueryString["ss"] == "third-dimension"){ %> class="selected" <%}%>><a href="third-dimension-kaneva-dance.kaneva" target="_parent">Third Dimension</a></li>
	    </ul>
    </div>
	
    <!-- Special Features -->
    <div class="menutitle <%if(Request.QueryString["s"] == "6"){ %> caton <%}%>"><a href="special-features.kaneva" target="_parent">
    
         <% if (Request.QueryString["s"] != "6" && Request.QueryString["s"] != "1"){ %><img src="http://www.kaneva.com/images/arrow_pointright.gif"  border="0"  /><%}
        else {%> <img src="http://www.kaneva.com/images/arrow_pointdown.gif"  border="0"  /> <% } %>
        
    
     Special Features</a>
    </div>
    <div id="sortlinks" <% if (Request.QueryString["s"] != "6" && Request.QueryString["s"] != "1") { %>style="display:none;" <%} %>>
        <ul>
            <li <%if(Request.QueryString["ss"] == "access-pass"){ %> class="selected" <%}%>><a href="adult-chat-rooms.kaneva" target="_parent">Access Pass</a></li>
            <li <%if(Request.QueryString["ss"] == "cover-charge"){ %> class="selected" <%}%>><a href="virtual-dj.kaneva" target="_parent">Cover Charge</a></li>
            <li <%if(Request.QueryString["ss"] == "dance-party"){ %> class="selected" <%}%>><a href="virtual-dance-game.kaneva" target="_parent">Dance Party 3D</a></li>
            <li <%if(Request.QueryString["ss"] == "emotes-animations"){ %> class="selected" <%}%>><a href="animated-avatars.kaneva" target="_parent">Emotes & Animations</a></li>
            <li <%if(Request.QueryString["ss"] == "flash-widgets"){ %> class="selected" <%}%>><a href="flash-widgets.kaneva" target="_parent">Flash Widgets</a></li>
            <li <%if(Request.QueryString["ss"] == "ka-ching"){ %> class="selected" <%}%>><a href="kaneva-ka-ching.kaneva" target="_parent">Ka-ching!</a></li>
            <li <%if(Request.QueryString["ss"] == "tv-channels"){ %> class="selected" <%}%>><a href="tv-online.kaneva" target="_parent">TV Channels</a></li>
            <li <%if(Request.QueryString["ss"] == "texture-patterns"){ %> class="selected" <%}%>><a href="create-3-d-objects.kaneva" target="_parent">Texture Patterns</a></li>
        </ul>
    </div>

</div>


	<span class="cb"><span class="cl"></span></span>
</div>

