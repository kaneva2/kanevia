﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Activities.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.usercontrols.Activities" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../usercontrols/Pager.ascx" %>

<asp:repeater runat="server" id="rptActivities"  onitemdatabound="rptNotifications_ItemDataBound">  
	<headertemplate>
		<div class="header">
			<div class="icon"></div>
			<div class="subject">Activity</div>
		</div>
	</headertemplate>
	<itemtemplate>
		<div class="row" runat="server">												
			<div class="icon" id="divIcon" runat="server"></div>			
			<div class="subject" id="divSubject" runat="server"></div>
			<div class="date" id="divDate" runat="server"></div>
			<div class="details" id="divDetails" runat="server"></div>
		</div>														 
	</itemtemplate>
	<footertemplate></footertemplate>
</asp:repeater>

<script type="text/javascript">
	function ViewDetails(obj, anchor) {
		if (obj.style.display == 'none') {
			obj.style.display = 'block';
			anchor.innerText = 'hide details';
		}
		else {
			obj.style.display = 'none';
			anchor.innerText = 'show details';
		}
	}
</script>
