<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdministrativeMenu.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.usercontrols.AdministrativeMenu" %>
    <table cellpadding="0" cellspacing="0" width="100%" border="0" id="tblAdminMenu" runat="server" visible="false">
	    <tr>
	        <td id="tdAdminMenu" align="center" class="tdAdminMenu">
	            <div id="adminNavHolder">
	                <div id="adminMenu">
	                    <ul>
			                <li id="liUserName" runat="server"></li>
			                <li id="navReports" runat="server"></li>
			                <li id="navCSR" runat="server"></li>
			                <li id="navCatalog" runat="server"></li>
			                <li id="navFame" runat="server"></li>
			                <li id="navEmail" runat="server"></li>
			                <li id="navSettings" runat="server"></li>
			            </ul>
			        </div>
			    </div>
	        </td>
	    </tr>
	</table>	