<%@ Control Language="c#" AutoEventWireup="false" Codebehind="Leaderboard.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.usercontrols.Leaderboard" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="Pager.ascx" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>
<ajax:ajaxpanel id="Ajaxpanel1" runat="server">
<table style="WIDTH: 100%" cellspacing="0" cellpadding="0" border="0" bgcolor="white">
	<tr>
		<td style="WIDTH: 100%" valign="top">
			<asp:label id="lbl_Noresults" runat="server" visible="False" backcolor="White" bordercolor="Blue"
				font-size="Large">No results were found.</asp:label>
			
					
			<asp:datagrid id="dgd_leaders" cssclass="leaderBoard" runat="server" font-size="Smaller" width="100%" showheader="False"
				borderwidth="0px" autogeneratecolumns="False" cellpadding="0" height="100%" horizontalalign="Center" GridLines="None">
				
				<footerstyle backcolor="White"></footerstyle>
				
				<alternatingitemstyle wrap="False" cssclass="altrow"></alternatingitemstyle>
				
				<itemstyle wrap="False" ></itemstyle>
				
				<headerstyle font-size="10pt" font-bold="True" wrap="False" horizontalalign="Center"></headerstyle>
							
				<columns>
					<asp:templatecolumn visible="True">
						<headerstyle wrap="False" horizontalalign="Center" width="60px"></headerstyle>
						<itemstyle wrap="False" horizontalalign="Center" width="60px" verticalalign="Middle"></itemstyle>
						<itemtemplate>
							<span class="rank">Rank</span><br>
							<span class="ranknum"><%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "rank").ToString ())%></span>
						</itemtemplate>
					</asp:templatecolumn>
					
					<asp:templatecolumn visible="False">
						<headerstyle width="60px"></headerstyle>
						<itemstyle cssclass="profilepic" wrap="False" horizontalalign="Center" width="60px" verticalalign="Middle"></itemstyle>
						<itemtemplate>
							<a title="<%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "name").ToString ())%>" href="<%# GetUrlPrsnlBroadAsset (DataBinder.Eval(Container.DataItem, "name_no_spaces").ToString (), URLType )%>" style="cursor:hand;">
								<img width="30px" alt="<%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "name").ToString ())%>" border="0" src="<%# GetPhotoImageURL(DataBinder.Eval(Container.DataItem, "thumbnail_small_path").ToString () ,"sm") %>" />
							</a>
						</itemtemplate>
					</asp:templatecolumn>
					
					<asp:templatecolumn visible="true">
						<headerstyle  cssclass="profile" width="55px"></headerstyle>
						<itemstyle cssclass="profile" wrap="False" horizontalalign="Center" width="55px" verticalalign="Middle"></itemstyle>
						<itemtemplate>
							<p><a href="<%# GetUrlPrsnlBroadAsset (DataBinder.Eval(Container.DataItem, "name_no_spaces").ToString (), URLType )%>" style="cursor:hand;"><%# TruncateWithEllipsis (DataBinder.Eval (Container.DataItem, "name").ToString(),18) %></a></p>
						</itemtemplate>
					</asp:templatecolumn>
					
					<asp:templatecolumn visible="False">
						<headerstyle  cssclass="profile" width="55px"></headerstyle>
						<itemstyle cssclass="profile" wrap="False" horizontalalign="Center" width="55px" verticalalign="Middle"></itemstyle>
						<headertemplate><span class="profile"><img src="../images/icons/leaderboard/image_16.gif" width="16" height="16" title="Profile: Change Profile Pic" /></span></headertemplate>
						<itemtemplate>
							<span class="profile"><%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "profile_picture").ToString ())%></span>
						</itemtemplate>
					</asp:templatecolumn>
					
					<asp:templatecolumn visible="false">
						<headerstyle  cssclass="profile" width="55px"></headerstyle>
						<itemstyle cssclass="profile" wrap="False" horizontalalign="Center" width="55px" verticalalign="Middle"></itemstyle>
						<headertemplate><span class="profile"><img src="../images/icons/leaderboard/paint_16.gif" width="16" height="16" title="Profile: Pimped Profile"></span></headertemplate>
						<itemtemplate>
							<span class="profile"><%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "page_pimped").ToString ())%></span>
						</itemtemplate>
					</asp:templatecolumn>																			
					
					<asp:templatecolumn visible="false">
						<headerstyle  cssclass="profile" width="55px"></headerstyle>
						<itemstyle cssclass="profile" wrap="False" horizontalalign="Center" width="55px" verticalalign="Middle"></itemstyle>
						<headertemplate><span class="profile"><img src="../images/icons/leaderboard/raveit_bt.gif" width="18" height="18" title="Profile: Earn Raves"></span></headertemplate>
						<itemtemplate>
							<span class="profile"><%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "raves_your_profile").ToString ())%></span>
						</itemtemplate>
					</asp:templatecolumn>
					
					<asp:templatecolumn visible="false">
						<headerstyle  cssclass="friends" width="55px"></headerstyle>
						<itemstyle cssclass="friends" wrap="False" horizontalalign="Center" width="55px" verticalalign="Middle"></itemstyle>
						<headertemplate><span class="friends"><img src="../images/icons/leaderboard/invite-friend.gif" width="16" height="16" title="Friends: Email Invites"></span></headertemplate>
						<itemtemplate>
							<span class="friends"><%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "people_invited").ToString ())%></span>
						</itemtemplate>
					</asp:templatecolumn>
					
					<asp:templatecolumn visible="false">
						<headerstyle  cssclass="friends" width="55px"></headerstyle>
						<itemstyle cssclass="friends" wrap="False" horizontalalign="Center" width="55px" verticalalign="Middle"></itemstyle>
						<headertemplate><span class="friends"><img src="../images/icons/leaderboard/add-friend.gif"  width="16" height="16" title="Friends: Accept Requests"></span></headertemplate>
						<itemtemplate>
							<span class="friends"><%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "friend_accepts").ToString ())%></span>
						</itemtemplate>
					</asp:templatecolumn>
					
					<asp:templatecolumn visible="false">
						<headerstyle  cssclass="channel" width="55px"></headerstyle>
						<itemstyle cssclass="channel" wrap="False" horizontalalign="Center" width="55px" verticalalign="Middle"></itemstyle>
						<headertemplate><span class="channel"><img src="../images/icons/leaderboard/join-wok.gif" width="16" height="16" title="Community: Join WOK"></span></headertemplate>
						<itemtemplate>
							<span class="channel"><%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "join_wok").ToString ())%></span>
						</itemtemplate>
					</asp:templatecolumn>
					
					<asp:templatecolumn visible="false">
						<headerstyle  cssclass="channel" width="55px"></headerstyle>
						<itemstyle cssclass="channel" wrap="False" horizontalalign="Center" width="55px" verticalalign="Middle"></itemstyle>
						<headertemplate><span class="channel"><img src="../images/icons/leaderboard/start-channel.gif"  width="16" height="16" title="Community: Create Community"></span></headertemplate>
						<itemtemplate>
							<span class="channel"><%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "channels_created").ToString ())%></span>
						</itemtemplate>
					</asp:templatecolumn>
					
					<asp:templatecolumn visible="false">
						<headerstyle  cssclass="channel" width="55px"></headerstyle>
						<itemstyle cssclass="channel" wrap="False" horizontalalign="Center" width="55px" verticalalign="Middle"></itemstyle>
						<headertemplate><span class="channel"><img src="../images/icons/leaderboard/accept-friends.gif"  width="16" height="16" title="Community: Accept Members"></span></headertemplate>
						<itemtemplate>
							<span class="channel"><%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "channel_members").ToString ())%></span>
						</itemtemplate>
					</asp:templatecolumn>
					
					<asp:templatecolumn visible="false">
						<headerstyle  cssclass="channel" width="55px"></headerstyle>
						<itemstyle cssclass="channel" wrap="False" horizontalalign="Center" width="55px" verticalalign="Middle"></itemstyle>
						<headertemplate><span class="channel"><img src="../images/icons/leaderboard/raveit_bt.gif"  width="18" height="18" title="Community: Earn Raves"></span></headertemplate>
						<itemtemplate>
							<span class="channel"><%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "raves_channel").ToString ())%></span>
						</itemtemplate>
					</asp:templatecolumn>
					
					<asp:templatecolumn visible="false">
						<headerstyle  cssclass="media" width="55px"></headerstyle>
						<itemstyle cssclass="media" wrap="False" horizontalalign="Center" width="55px" verticalalign="Middle"></itemstyle>
						<headertemplate><span class="media"><img src="../images/icons/leaderboard/camera_16.gif"  width="16" height="16" title="Media: Upload Photos"></span></headertemplate>
						<itemtemplate>
							<span class="media"><%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "photos_uploaded").ToString ())%></span>
						</itemtemplate>
					</asp:templatecolumn>
					
					<asp:templatecolumn visible="false">
						<headerstyle  cssclass="media" width="55px"></headerstyle>
						<itemstyle cssclass="media" wrap="False" horizontalalign="Center" width="55px" verticalalign="Middle"></itemstyle>
						<headertemplate><span class="media"><img src="../images/icons/leaderboard/video_16.gif"  width="16" height="16" title="Media: Upload Video"></span></headertemplate>
						<itemtemplate>
							<span class="media"><%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "video_uploaded").ToString ())%></span>
						</itemtemplate>
					</asp:templatecolumn>
					
					<asp:templatecolumn visible="false">
						<headerstyle  cssclass="media" width="55px"></headerstyle>
						<itemstyle cssclass="media" wrap="False" horizontalalign="Center" width="55px" verticalalign="Middle"></itemstyle>
						<headertemplate><span class="media"><img src="../images/icons/leaderboard/music_16.gif"  width="16" height="16" title="Media: Upload Music"></span></headertemplate>
						<itemtemplate>
							<span class="media"><%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "music_uploaded").ToString ())%></span>
						</itemtemplate>
					</asp:templatecolumn>
					
					<asp:templatecolumn visible="false">
						<headerstyle  cssclass="media" width="55px"></headerstyle>
						<itemstyle cssclass="media" wrap="False" horizontalalign="Center" width="55px" verticalalign="Middle"></itemstyle>
						<headertemplate><span class="media"><img src="../images/icons/leaderboard/raveit_bt.gif"  width="16" height="16" title="Media: Earn Raves"></span></headertemplate>
						<itemtemplate>
							<span class="media"><%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "raves_media").ToString ())%></span>
						</itemtemplate>
					</asp:templatecolumn>
					
					<asp:boundcolumn datafield="total_points" readonly="True" headertext="Total Points">
						<headerstyle cssclass="totalpoints" horizontalalign="Center" width="60px"></headerstyle>
						<itemstyle cssclass="totalpoints" font-bold="True" wrap="False" horizontalalign="Center" width="60px" verticalalign="Middle"></itemstyle>
					</asp:boundcolumn>
					
					<asp:boundcolumn visible="False" datafield="user_id" headertext="User Id">
						<headerstyle width="0px"></headerstyle>
					</asp:boundcolumn>
				
				</columns>
			
			</asp:datagrid>
		</td>
	</tr>
	
	<tr class="myrow" id="trMyRow" runat="server" visible="false">
		<td> 
			<asp:datagrid id="dgd_UserInfo" cssclass="leaderBoard" runat="server" 
				width="100%" borderwidth="0px" autogeneratecolumns="False" cellpadding="0" height="100%"
				horizontalalign="Center" allowpaging="True" showheader="False" PageSize="1" GridLines="None">
				
				<itemstyle wrap="False"></itemstyle>
				
				<columns>
				
					<asp:templatecolumn visible="True">
						<itemstyle wrap="False" horizontalalign="Center" width="60px" verticalalign="Middle"></itemstyle>
						<itemtemplate>
							<span class="rank">Rank</span><br>
							<span class="ranknum"><%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "rank").ToString ())%></span>
						</itemtemplate>
					</asp:templatecolumn>
					
					<asp:templatecolumn visible="False">
						<headerstyle width="60px"></headerstyle>
						<itemstyle wrap="False" horizontalalign="Center" width="60px" verticalalign="Middle"></itemstyle>
						<itemtemplate>
							<a title='<%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "name").ToString ())%>' href='<%#  GetUrlPrsnlBroadAsset (DataBinder.Eval(Container.DataItem, "name_no_spaces").ToString (), URLType )%>' style="cursor:hand;">
								<img width="30px" alt='<%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "name").ToString ())%>' border="0" src='<%# GetPhotoImageURL(DataBinder.Eval(Container.DataItem, "thumbnail_small_path").ToString () ,"sm") %>' border="0" />
							</a>
						</itemtemplate>
					</asp:templatecolumn>
					
					<asp:templatecolumn visible="true">
						<itemstyle cssclass="profile" wrap="False" horizontalalign="Center" width="55px" verticalalign="Middle"></itemstyle>
						<itemtemplate>
							<p><a href=<%# GetUrlPrsnlBroadAsset (DataBinder.Eval(Container.DataItem, "name_no_spaces").ToString (),URLType )%> style="cursor:hand;"><%# TruncateWithEllipsis (DataBinder.Eval (Container.DataItem, "name").ToString(),18) %></a></p>
						</itemtemplate>
					</asp:templatecolumn>
					
					<asp:boundcolumn visible="False" datafield="profile_picture" readonly="True" headertext="Profile Picture Changed">
						<itemstyle cssclass="profile" wrap="False" horizontalalign="Center" width="55px" verticalalign="Middle"></itemstyle>
					</asp:boundcolumn>
					<asp:boundcolumn visible="False" datafield="page_pimped" readonly="True" headertext="Page Pimped">
						<itemstyle cssclass="profile" wrap="False" horizontalalign="Center" width="55px" verticalalign="Middle"></itemstyle>
					</asp:boundcolumn>
					<asp:boundcolumn visible="False" datafield="raves_your_profile" readonly="True" headertext="Raves Your Profile">
						<itemstyle cssclass="profile" wrap="False" horizontalalign="Center" width="55px" verticalalign="Middle"></itemstyle>
					</asp:boundcolumn>
					<asp:boundcolumn visible="False" datafield="people_invited" readonly="True" headertext="People You've Invited">
						<itemstyle cssclass="friends" wrap="False" horizontalalign="Center" width="55px" verticalalign="Middle"></itemstyle>
					</asp:boundcolumn>
					<asp:boundcolumn visible="False" datafield="friend_accepts" readonly="True" headertext="Friend Accepts">
						<itemstyle cssclass="friends" wrap="False" horizontalalign="Center" width="55px" verticalalign="Middle"></itemstyle>
					</asp:boundcolumn>
					<asp:boundcolumn visible="False" datafield="join_wok" readonly="True" headertext="WOK Memeber">
						<itemstyle cssclass="channel" wrap="False" horizontalalign="Center" width="55px" verticalalign="Middle"></itemstyle>
					</asp:boundcolumn>
					<asp:boundcolumn visible="False" datafield="channels_created" readonly="True" headertext="Communities You've Created">
						<itemstyle cssclass="channel"  wrap="False" horizontalalign="Center" width="55px" verticalalign="Middle"></itemstyle>
					</asp:boundcolumn>
					<asp:boundcolumn visible="False" datafield="channel_members" readonly="True" headertext="Members In Your Community">
						<itemstyle cssclass="channel" wrap="False" horizontalalign="Center" width="55px" verticalalign="Middle"></itemstyle>
					</asp:boundcolumn>
					<asp:boundcolumn visible="False" datafield="raves_channel" readonly="True" headertext="Raves (Your Community)">
						<itemstyle cssclass="channel" wrap="False" horizontalalign="Center" width="55px" verticalalign="Middle"></itemstyle>
					</asp:boundcolumn>
					<asp:boundcolumn visible="False" datafield="photos_uploaded" readonly="True" headertext="Photos Uploaded">
						<itemstyle cssclass="media" wrap="False" horizontalalign="Center" width="55px" verticalalign="Middle"></itemstyle>
					</asp:boundcolumn>
					<asp:boundcolumn visible="False" datafield="video_uploaded" readonly="True" headertext="Videos Uploaded">
						<itemstyle cssclass="media" wrap="False" horizontalalign="Center" width="55px" verticalalign="Middle"></itemstyle>
					</asp:boundcolumn>
					<asp:boundcolumn visible="False" datafield="music_uploaded" readonly="True" headertext="Music Uploaded">
						<itemstyle cssclass="media" wrap="False" horizontalalign="Center" width="55px" verticalalign="Middle"></itemstyle>
					</asp:boundcolumn>
					<asp:boundcolumn visible="False" datafield="raves_media" readonly="True" headertext="Raves (Your Media)">
						<itemstyle cssclass="media" wrap="False" horizontalalign="Center" width="55px" verticalalign="Middle"></itemstyle>
					</asp:boundcolumn>
					<asp:boundcolumn datafield="total_points" readonly="True" headertext="Total Points">
						<itemstyle cssclass="totalpoints" font-bold="True" wrap="False" horizontalalign="Center" width="60px" verticalalign="Middle"></itemstyle>
					</asp:boundcolumn>
					<asp:boundcolumn visible="False" datafield="user_id" headertext="User Id">
						<itemstyle font-bold="True" horizontalalign="Center" width="0px" verticalalign="Middle"></itemstyle>
					</asp:boundcolumn>
					
				</columns>
			</asp:datagrid>
			
		</td>
	</tr>
	<TR>
		<TD >
			<TABLE runat=server id="pager2" cellSpacing="0" cellPadding="0" width="100%" border="0" style="BORDER-TOP: #cccccc 1px dashed; BORDER-BOTTOM: #cccccc 1px dashed">
				<TR>
					<td>
						<kaneva:pager id="pgBottom" runat="server" isajaxmode="True"></kaneva:pager>
					</td>
				</TR>
			</TABLE>
		</TD>
	</TR>
	
</table>
</ajax:ajaxpanel>