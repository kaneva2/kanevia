///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.usercontrols
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

	using System.Collections;
	using System.Web.SessionState;
	using MagicAjax;
	using System.Web.UI;

	/// <summary>
	///		Summary description for uc_AssetDetails.
	/// </summary>
	public class uc_AssetDetails : BaseUserControl
	{
		protected System.Web.UI.WebControls.Label lblImageCaption;
		protected System.Web.UI.WebControls.PlaceHolder phStream;
		protected System.Web.UI.WebControls.ImageButton btnVote;
		protected System.Web.UI.WebControls.ImageButton hlFave;
		protected System.Web.UI.WebControls.ImageButton hlShare;
		protected System.Web.UI.WebControls.ImageButton hlComments;
		protected System.Web.UI.WebControls.ImageButton hlViews;
		protected System.Web.UI.WebControls.LinkButton hlVote;
		protected System.Web.UI.WebControls.Label lblVotes;
		protected System.Web.UI.WebControls.LinkButton hlFave2;
		protected System.Web.UI.WebControls.LinkButton hlShare2;
		protected System.Web.UI.WebControls.LinkButton hlComments2;
		protected System.Web.UI.WebControls.LinkButton hlViews2;
		protected HtmlImage imgRestricted;
		protected Label lblSubject;
		protected Label lblCreatedDate;
		protected Label lblTeaser, lblBodyText, lblLicense, lblLicenseURL;
		protected Label lblCategories, lblTags, lblChannels;
		protected Label lblRuntime, lblNumViews, lblNumShares, lblNumComments;
		protected Label lblOwnerViews, lblOwnerFavs, lblOwnerFriends;
		protected Label lblDownloadUntil, lblAllowedDownloads, lblDownloadsRemaining;
		protected Literal litStream = new Literal ();
		protected HtmlTable tblStats;
		protected string m_embedText = string.Empty;
		protected bool showToolBar = false;
		protected bool allowPictureDetail = false;
		protected Page parent = null;
		protected HtmlTableRow trWidgetButtonBar;
		protected int height = 175; //430;
		protected int width = 398; //500;




		private DataRow drAsset = null;
		private int assetId = -1;
		protected MagicAjax.UI.Controls.AjaxPanel Ajaxpanel1;
		private int userId = -1;

		private void Page_Load(object sender, System.EventArgs e)
		{
			if(!Page.IsPostBack)
			{
				GenerateAssetDetails();
			}
		}


		public void GenerateAssetDetails()
		{
			//get asset id from the request object
			assetId = Convert.ToInt32 (Request ["assetId"]);

			try
			{
				//pullback all asset information
				AssetData = StoreUtility.GetAsset (assetId, false, KanevaWebGlobals.GetUserId());
			}
			catch(Exception)
			{
			}

			//VALIDATE asset Data record before proceeding
			//if the query failed or the asset is no longer available redirect to a message landing page
			if (AssetData == null || AssetId().Equals (DBNull.Value) || AssetStatus().Equals ((int) Constants.eASSET_STATUS.MARKED_FOR_DELETION) || AssetStatus().Equals ((int) Constants.eASSET_STATUS.DELETED))
			{
				// Couldn't find the asset as published
				Response.Redirect (ResolveUrl ("~/asset/notAvailable.aspx?M=NA"));
			}


			//set assetAttributes for use

			//if private redirect to message landing page
			if (AssetPermissions().Equals ((int) Constants.eASSET_PERMISSION.PRIVATE))
			{
				// item marked private
				Response.Redirect (ResolveUrl ("~/asset/notAvailable.aspx?M=PR"));
			}
			else
			{
				// Is it mature?
				if (IsMature (AssetRatingId()))
				{
					if (!KanevaWebGlobals.CurrentUser.HasAccessPass)
					{
						// The user either can not or chooses not to view adult material
						Response.Redirect (ResolveUrl ("~/asset/notAvailable.aspx?M=ADU"));
					}
				}
			}
			
//			// Is it mature?
//			imgRestricted.Visible = IsMature (AssetRatingId());
//		
			//get current user's id
			userId = KanevaWebGlobals.GetUserId ();

			if (AssetCaption() != "")
			{
				lblImageCaption.Text = AssetCaption();
				lblImageCaption.Visible = true;
			}
									
			/**** Control Panel Tools ****/
			// Share (tell other users)
			hlShare.CommandArgument = hlShare2.CommandArgument = GetShareLink (assetId);

			// Favorites (my channel)
			//hlFave.CommandArgument = hlFave2.CommandArgument = GetFavoriteLink (assetId);
			hlFave2.Text = GetChannelCount (AssetChannelCount());

			//user cant add his own assets to his library
			hlFave.Enabled = (userId != AssetOwnerId());
			hlFave2.Enabled = (userId != AssetOwnerId());
			
			// Comments
			hlComments.CommandArgument = hlComments2.CommandArgument = GetAssetDetailsLink(AssetId());
			
			// Raves
			lblVotes.Text = AssetNumberOfDiggs();
			btnVote.ImageUrl = StoreUtility.GetRaveIcon(AssetDiggId(), Page );
			btnVote.ToolTip = GetRaveToolTip ();

			// Views
			hlViews.CommandArgument = hlViews2.CommandArgument = Request.Url.ToString();

			lblNumViews.Text = AssetNumberOfViews() + " Views";
			lblNumShares.Text = AssetNumberOfShares() + " Shares";
			lblNumComments.Text = AssetNumberOfComments() + " Comments";
			

			// if item is free or user has already purchased it
			if (!IsPayItem (AssetData))
			{
                //if (!StoreUtility.IsKanevaGame (AssetData))
                //{
                //    StreamAsset(AssetData);

                //    // Embeded URL 
                //    AssetEmbedCode = m_embedText;	

                //}
                //else
                //{
					// show screen shot or trailer
					litStream.Text = "<img  src=\"" + GetMediaImageURL (AssetData ["thumbnail_large_path"].ToString (), "la", assetId, Convert.ToInt32(AssetData ["asset_type_id"])) + "\" border=\"0\" />";
					litStream.Text += "&nbsp;&nbsp;&nbsp;<a id=\"hlBuy\" title=\"Purchase This Item!\" class=\"adminLinks\" href=\"" + ResolveUrl("~/checkout/kPointSelection.aspx?assetId=" + assetId) + "\"><img title=\"Play This Game!\" src=\"../images/button_play.gif\" alt=\"\" border=\"0\" /></a>";
					phStream.Controls.Add (litStream);
				//}

			
				// Get download stats
				if (Request.IsAuthenticated && Convert.ToDouble (AssetData ["amount"]) > 0)
				{
					try
					{
						DataRow drOrder = StoreUtility.GetActiveAssetOrderRecord (KanevaWebGlobals.GetUserId (), assetId);

						int numberOfDownloadsAllowed = Convert.ToInt32 (drOrder ["number_of_downloads_allowed"]);
						int numberOfDownloads = Convert.ToInt32 (drOrder ["number_of_downloads"]);

						lblDownloadUntil.Text = KanevaGlobals.FormatDateTime (Convert.ToDateTime (drOrder ["download_end_date"]));
						lblAllowedDownloads.Text = numberOfDownloadsAllowed.ToString (); 
						lblDownloadsRemaining.Text = (numberOfDownloadsAllowed - numberOfDownloads).ToString ();
						
						tblStats.Visible = true;
					}
					catch (Exception)
					{
						tblStats.Visible = false;
					}
				}
			}
			else
			{
				// show screen shot or trailer
				litStream.Text = "<img  src=\"" + GetMediaImageURL (AssetData ["thumbnail_large_path"].ToString (), "la", assetId, Convert.ToInt32(AssetData ["asset_type_id"])) + "\" border=\"0\" />";
				litStream.Text += "&nbsp;&nbsp;&nbsp;<a id=\"hlBuy\" title=\"Purchase This Item!\" class=\"adminLinks\" href=\"" + ResolveUrl("~/checkout/kPointSelection.aspx?assetId=" + assetId) + "\"><img title=\"Purchase This Item!\" src=\"../images/button_purchase.gif\" alt=\"\" border=\"0\" /></a>";
				phStream.Controls.Add (litStream);
			}

//			// Load the attribute data
//			LoadData (AssetData);

		}



		# region AssetFunctions

		/// <summary>
		/// Returns the number of channels this asset belongs to
		/// </summary>
		public string GetChannelCount (int numberOfChannels)
		{
			string plural = numberOfChannels == 1 ? string.Empty : "s";
			return numberOfChannels.ToString () + " channel" + plural;
		}

		/// <summary>
		/// GetShareLink
		/// </summary>
		public string GetShareLink (int assetId)
		{ 
			return ResolveUrl("~/mykaneva/newMessage.aspx?assetId=" + assetId + "&URL=" + GetAssetDetailsLink (assetId));
		}

		/// <summary>
		/// Is the current session user a site administrator?
		/// </summary>
		/// <returns></returns>
		public bool IsAdministrator ()
		{
			return UsersUtility.IsUserAdministrator ();
		}

		/// <summary>
		/// Get Asset Details Link
		/// </summary>
		/// <param name="assetId"></param>
		/// <returns></returns>
		public string GetAssetEmbedLink (int assetId, int height, int width, bool fullUrlRequired )
		{
			return StoreUtility.GetAssetEmbedLink (assetId, height, width, Page, false, fullUrlRequired );
		}

		/// <summary>
		/// GetRaveToolTip
		/// </summary>
		public string GetRaveToolTip ()
		{
			return KanevaWebGlobals.GetResourceString ("raveInfo", "channel", "rave");
		}

		# endregion


		# region AssetAttributes


		public Page UCParent
		{
			get
			{
				return parent;
			}
			set
			{
				parent = value;
			}
		}

		public DataRow AssetData
		{
			get
			{
				return drAsset;
			}
			set
			{
				drAsset = value;
			}
		}

		public bool ShowAssetToolBar
		{
			get
			{
				return showToolBar;
			}
			set
			{
				showToolBar = value;
			}
		}

		public int AssetChannelCount()
		{
			return Convert.ToInt32 (AssetData ["number_of_channels"]);
		}

		public string AssetCategories()
		{
			return AssetData["categories"].ToString();
		}

		public string AssetEmbedCode
		{
			get
			{
				return m_embedText;
			}
			set
			{
				m_embedText = value;
			}
		}


		public int MediaHeight
		{
			get
			{
				return height;
			}
			set
			{
				if(value > 0)
				{
					height = value;
				}
			}
		}

		public int MediaWidth
		{
			get
			{
				return width;
			}
			set
			{
				if(value > 0)
				{
					width = value;
				}
			}
		}
 
		public bool AllowPictureDetail
		{
			get
			{
				return allowPictureDetail;
			}
			set
			{
				allowPictureDetail = value;
			}
		}

		public string AssetNumberOfComments()
		{
			return AssetData ["number_of_comments"].ToString();
		}

		public string AssetNumberOfViews()
		{
			return AssetData ["number_of_downloads"].ToString ();
		}

		public string AssetNumberOfDiggs()
		{
			return AssetData ["number_of_diggs"].ToString ();
		}

		public string AssetNumberOfShares()
		{
			return AssetData ["number_of_shares"].ToString ();
		}

		public int AssetDiggId()
		{
			return Convert.ToInt32(AssetData ["digg_id"]);
		}

		public string AssetCaption()
		{
			return AssetData ["image_caption"].ToString();
		}

		public int AssetStatus()
		{
			return Convert.ToInt32(AssetData ["status_id"]);
		}

		public int AssetId()
		{
			return Convert.ToInt32(AssetData ["asset_id"]);
		}

		public int AssetPermissions()
		{
			return Convert.ToInt32(AssetData ["permission"]);
		}

		public int AssetRatingId()
		{
			return Convert.ToInt32 (AssetData ["asset_rating_id"]);
		}

		public int AssetTypeId()
		{
			return Convert.ToInt32 (AssetData ["asset_type_id"]);
		}

		public double AssetPrice()
		{
			return Convert.ToDouble (AssetData ["amount"]);
		}

		public string AssetName()
		{
			return AssetData ["name"].ToString ();
		}

		public string AssetNameDecoded()
		{
			return Server.HtmlDecode (AssetName());
		}

		public int AssetOwnerId()
		{
			return Convert.ToInt32(AssetData ["user_id"]);
		}

		public int AssetOwnerChannelId()
		{
            return GetUserFacade.GetPersonalChannelId(AssetOwnerId());
		}

		public string AssetTeaser()
		{
			return AssetData ["teaser"].ToString ();
		}

		public string AssetBodyText()
		{
			return AssetData ["body_text"].ToString ();
		}

		public int AssetPublishStatusId()
		{
			return Convert.ToInt32(AssetData["publish_status_id"]);
		}

		# endregion


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnVote.Click += new System.Web.UI.ImageClickEventHandler(this.btnVote_Click);
			this.hlFave.Command += new System.Web.UI.WebControls.CommandEventHandler(this.hlFave2_Command);
			this.hlShare.Command += new System.Web.UI.WebControls.CommandEventHandler(this.hlShare2_Command);
			this.hlComments.Command += new System.Web.UI.WebControls.CommandEventHandler(this.hlComments2_Command);
			this.hlViews.Command += new System.Web.UI.WebControls.CommandEventHandler(this.hlViews2_Command);
			this.hlVote.Click += new System.EventHandler(this.btnVote_Click);
			this.hlFave2.Command += new System.Web.UI.WebControls.CommandEventHandler(this.hlFave2_Command);
			this.hlShare2.Command += new System.Web.UI.WebControls.CommandEventHandler(this.hlShare2_Command);
			this.hlComments2.Command += new System.Web.UI.WebControls.CommandEventHandler(this.hlComments2_Command);
			this.hlViews2.Command += new System.Web.UI.WebControls.CommandEventHandler(this.hlViews2_Command);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion


		# region Methods

		/// <summary>
		/// It is a buy now!
		/// </summary>
		private bool IsPayItem (DataRow AssetData)
		{
			int assetId = GetAssetId ();
			bool isPayItem = true;

			// bool isGameSubscription = StoreUtility.IsGameSubscription (AssetData);
			Double dAssetAmount = Convert.ToDouble (AssetData ["amount"]);

			// If it is free go right to the asset download
			if (dAssetAmount.Equals (0.0))
			{
				isPayItem = false;

				// Make sure they are at least registered and logged in
				if (!Request.IsAuthenticated)
				{
					// Only if it is not a game allow them to get without registering (Or if the asset has require login to 1)
					if (!Convert.ToInt32 (AssetData ["require_login"]).Equals (1))
					{
						isPayItem = false;
					}
				}
			}
			else
			{
				// Do they already have an existing completed order for this asset?
				// And still allowed to download it?
				int userId = KanevaWebGlobals.GetUserId ();
				if (StoreUtility.IsUserAllowedToDownload (userId, assetId))
				{
					return false;
				}
			}

			return isPayItem;
		}

		/// <summary>
		/// GetAssetId
		/// </summary>
		public int GetAssetId ()
		{
			return Convert.ToInt32 (Request ["assetId"]);
		}

		/// <summary>
		/// StreamAsset
		/// </summary>
		/// <param name="AssetData"></param>
		private void StreamAsset(DataRow AssetData)		
		{
			Response.Expires = 0;
			Response.CacheControl = "no-cache";

			int assetId = Convert.ToInt32 (AssetData["asset_id"]);
			hlShare.CommandArgument = GetShareLink (assetId);

			// Streamable asset?
			if (!StoreUtility.IsAssetStreamable (AssetData))
			{
				ShowErrorOnStartup ("File is being converted, the process could take several minutes, please try again later.");
				return;
			}	
			
			if (Convert.ToInt32(AssetData["publish_status_id"]) != (int) Constants.ePUBLISH_STATUS.PUBLISH_COMPLETE )
			{
				ShowErrorOnStartup ("File is being published, the process could take several minutes, please try again later.");
				return;
			}

			// Set up links
			string name = Server.HtmlDecode (AssetData ["name"].ToString ());
			
			// The URL to the content on the streaming server
            string contentURL = Configuration.StreamingServer + "/" + Configuration.StreamingFile + "?tId=" + assetId;

            if (Configuration.StreamIIS)
            {
                contentURL = Configuration.StreamingServer + "/" + AssetData["media_path"].ToString();
            }
            
            // For pay content, make sure they have access
			if (Convert.ToDouble (AssetData ["amount"]) > 0.0)
			{
				// If they are logged in, update the correct Order Item
				DataRow drOrderRecord = StoreUtility.GetActiveAssetOrderRecord (KanevaWebGlobals.GetUserId (), assetId);

				if (drOrderRecord == null)
				{
					if (Request.IsAuthenticated)
					{
						ShowErrorOnStartup ("You currently do not have access to this item!");
					}
					else
					{
						ShowErrorOnStartup ("You currently do not have access to this item! Please sign in.");
					}
					return;
				}

				contentURL += "&token=" + drOrderRecord ["token"].ToString ();
			}

			//string dimensions = "height=\"" + height + "\" width=\"" + width + "\"";
			string dimensions = "height=\"" + MediaHeight + "\" width=\"" + MediaWidth + "\"";
			string autoplay = "False";

			// Is it playable with Kaneva OMM?
			if (StoreUtility.IsOMMContent (AssetData))
			{
				m_embedText = litStream.Text = GetAssetEmbedLink (assetId, height, width, true ); // pass in true for fullURL only since required for embed link
				litStream.Text = GetAssetEmbedLink (assetId, height, width, false ); 

				trWidgetButtonBar.Visible = this.showToolBar;
			}
			else if (StoreUtility.IsQuickTimeContent (AssetData))
			{
				// Don't do this for OMM, OMM takes care of that
				// Update the media views
                MediaFacade mediaFacade = new MediaFacade();
                mediaFacade.UpdateMediaViews(assetId, Common.GetVisitorIPAddress(), KanevaWebGlobals.GetUserId());

				if (StoreUtility.IsAudioContent (AssetData))
				{
					dimensions = "height=\"16\" width=\"498\"";

					// Use Quicktime for these audio
					m_embedText = litStream.Text = "<object classid=\"clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B\" codebase=\"http://www.apple.com/qtactivex/qtplugin.cab\" " + dimensions + ">" +
						"<param name=\"src\" value=\"" + contentURL + "\">" +
						"<param name=\"autoplay\" value=\"" + autoplay + "\">" +
						"<param name=\"controller\" value=\"true\">" +
						"<embed " + dimensions + " src=\"" + contentURL + "\" pluginspage=\"http://www.apple.com/quicktime/download/\" type=\"video/quicktime\" controller=\"true\" autoplay=\"true\">" +
						"</object>";
				}
				else
				{
					// Use Quicktime for these videos, set scale to ASPECT
					m_embedText = litStream.Text = "<object classid=\"clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B\" codebase=\"http://www.apple.com/qtactivex/qtplugin.cab\" " + dimensions + ">" +
						"<param name=\"src\" value=\"" + contentURL + "\">" +
						"<param name=\"autoplay\" value=\"" + autoplay + "\">" +
						"<param name=\"controller\" value=\"true\">" +
						"<PARAM NAME=\"scale\" VALUE=\"ASPECT\">" +
						"<embed scale=\"ASPECT\" src=\"" + contentURL + "\" pluginspage=\"http://www.apple.com/quicktime/download/\" type=\"video/quicktime\" controller=\"true\" autoplay=\"true\">" +
						"</object>";
				}
			}
			else if (StoreUtility.IsFlashContent (AssetData))
			{
				// Don't do this for OMM, OMM takes care of that
				// Update the media views
				MediaFacade mediaFacade = new MediaFacade();
                mediaFacade.UpdateMediaViews (assetId, Common.GetVisitorIPAddress(), KanevaWebGlobals.GetUserId ());

				m_embedText = litStream.Text = "<OBJECT classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000' " +
					" codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0' " +
					" id=\"swfStream\"" + dimensions + ">" +
					" <param name='movie' value=\"" + contentURL + "\">" +
					" <param name='quality' value=\"high\">" +
					" <param name='loop' value=\"false\">" +
					" <EMBED src=\"" + contentURL + "\" quality='high'" + dimensions +
					" loop=\"false\" type='application/x-shockwave-flash'" +
					" pluginspage='http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash'>" +
					" </EMBED> " +
					"</OBJECT>";
			}
			else if (StoreUtility.IsImageContent (AssetData))
			{
				// Don't do this for OMM, OMM takes care of that
				// Update the media views
				MediaFacade mediaFacade = new MediaFacade();
                mediaFacade.UpdateMediaViews (assetId, Common.GetVisitorIPAddress(), KanevaWebGlobals.GetUserId ());

				// It is an image
				string litstring = "<div style=\"width: " + MediaWidth + "px; height: " + MediaHeight + "px; background-color: #FFFFFF; overflow: hidden; border: 1px solid #e0e0e0;\">";
				if(AllowPictureDetail)
				{
					litstring += "<a href=\"" + ResolveUrl("~/mykaneva/PictureDetail.aspx") + "?assetId=" + assetId.ToString() + "\" border=\"0\">";
				}

				litstring += "<img src=\"" + GetPhotoImageURL (AssetData ["thumbnail_assetdetails_path"].ToString (), "la") + "\" border=\"0\" alt=\"" + name + "\" title=\"" + name + "\"/></a></div>";
				litStream.Text = litstring;

//				litStream.Text = "<div style=\"width: 498px; width: 498px; background-color: #FFFFFF; overflow: hidden; border: 1px solid #e0e0e0;\">" +
//					"<a href=\"" + ResolveUrl("~/mykaneva/PictureDetail.aspx") + "?assetId=" + assetId.ToString() + "\" border=\"0\">" +
//					"<img src=\"" + GetPhotoImageURL (AssetData ["thumbnail_assetdetails_path"].ToString (), "la") + "\" border=\"0\" alt=\"" + name + "\" title=\"" + name + "\"/></a></div>";

				m_embedText = "<a href=\"http://" + Request.Url.Host + GetAssetDetailsLink (assetId) + "\" target=\"resource\" border=\"0\"><img src='" + GetPhotoImageURL (AssetData ["image_full_path"].ToString (), "la") + "'/></a>";
//				divEmbedded.InnerText = "Photo Embed URL:";
//
//				tbRawEmbed.Text = GetPhotoImageURL (AssetData ["image_full_path"].ToString (), "la");
//				trPhotoURL.Visible = true;
			}
			else
			{
				// Don't do this for OMM, OMM takes care of that
				// Update the media views
				MediaFacade mediaFacade = new MediaFacade();
                mediaFacade.UpdateMediaViews (assetId, Common.GetVisitorIPAddress(), KanevaWebGlobals.GetUserId ());

				if (StoreUtility.IsAudioContent (AssetData))
				{
					dimensions = "height=\"100\" width=\"498\"";
				}

				// Use Media player
				m_embedText = litStream.Text = "<OBJECT id=\"mpStream\"" + dimensions +
					" CLASSID=\"CLSID:6BF52A52-394A-11d3-B153-00C04F79FAA6\"" +
					" standby='Loading Microsoft Windows Media Player components...'" +
					" type=\"application/x-oleobject\">" +
					" <PARAM NAME=\"URL\" VALUE=\"" + contentURL + "\">" +
					" <PARAM NAME=\"ShowStatusBar\" VALUE=\"True\">" +
					" <PARAM NAME=\"EnableContextMenu\" VALUE=\"False\">" +
					" <PARAM NAME=\"AutoStart\" VALUE=\"" + autoplay + "\">" +
					" <PARAM name=\"PlayCount\" value=\"1\">" +
					"<embed " + dimensions + " type=\"application/x-mplayer2\" pluginspage=\"http://www.microsoft.com/Windows/MediaPlayer/\"" +
					" name=\"mediaplayer1\" showstatusbar=\"1\" EnableContextMenu=\"false\" autostart=\"true\" transparentstart=\"1\" loop=\"0\" controller=\"true\" src=\"" + contentURL + "\"></embed>" +
					"</OBJECT>";
			}
		   
			phStream.Controls.Add (litStream);	
		}

		private void VoteOnAsset()
		{
			if (!Request.IsAuthenticated)
			{
				AjaxCallHelper.WriteAlert ("You must be signed in to rave this item.");
				return;
			}

			int assetId = Convert.ToInt32 (Request ["assetId"]);
			if( StoreUtility.InsertDigg (KanevaWebGlobals.GetUserId (), assetId).Equals (1))
			{
				AjaxCallHelper.WriteAlert ("You have already raved this item.");
			}
			else
			{
				lblVotes.Text = Convert.ToString (Convert.ToInt32 (lblVotes.Text) + 1);
			}

			//disables buttons if user is not an admin
			if(!IsAdministrator())
			{
				btnVote.Enabled = false;
				lblVotes.Enabled = false;
				lblVotes.Style.Add("cursor","default");

				btnVote.ImageUrl = StoreUtility.GetRaveIcon( 1, Page );
				btnVote.Enabled = false;
				btnVote.Style.Add("cursor","default");
			}

		}

		private void RedirectURL(string url)
		{
			if(UCParent != null)
			{
				AjaxCallHelper.WriteLine("top.parent.window.location.href = '" + url + "';");
			}
			else
			{
				Response.Redirect(url);
			}
		}

		#endregion

		# region Event Handlers

		protected void lbCat_Click(object sender, CommandEventArgs e)
		{
			string scriptString = "<script language=JavaScript>window.open(" + ResolveUrl ("~/watch/media.aspx?cat=" + Server.HtmlEncode(e.CommandArgument.ToString ())) + ");";
			scriptString += "</script>";

			if (!Page.ClientScript.IsClientScriptBlockRegistered (Page.GetType (), "Clear"))
			{
                Page.ClientScript.RegisterStartupScript(Page.GetType (), "Clear", scriptString);
			}

			//Response.Redirect(ResolveUrl ("~/watch/media.aspx?cat=" + Server.HtmlEncode(e.CommandArgument.ToString ())) );
		}
		protected void btnSendMessage_Click(object sender, CommandEventArgs e)
		{
			string scriptString = "<script language=JavaScript>window.open(" + ResolveUrl ("~/myKaneva/newMessage.aspx?userId=" + e.CommandArgument) + ");";
			scriptString += "</script>";

            if (!Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "Clear"))
			{
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "Clear", scriptString);
			}

			//Response.Redirect(ResolveUrl ("~/myKaneva/newMessage.aspx?userId=" + e.CommandArgument));
		}

		/// <summary>
		/// Handle a profile vote
		/// </summary>
		protected void btnVote_Click (object sender, ImageClickEventArgs e)
		{
			VoteOnAsset();
		}
		private void btnVote_Click(object sender, System.EventArgs e)
		{
			VoteOnAsset();
		}

		private void hlFave2_Command(object sender, System.Web.UI.WebControls.CommandEventArgs e)
		{
			RedirectURL(e.CommandArgument.ToString());
		}

		private void hlShare2_Command(object sender, System.Web.UI.WebControls.CommandEventArgs e)
		{
			RedirectURL(e.CommandArgument.ToString());  	
		}

		private void hlComments2_Command(object sender, System.Web.UI.WebControls.CommandEventArgs e)
		{
			RedirectURL(e.CommandArgument.ToString());
		
		}

		//not to be implemented yet
		private void hlViews2_Command(object sender, System.Web.UI.WebControls.CommandEventArgs e)
		{
			//RedirectURL(e.CommandArgument.ToString());		
		}


		#endregion


	}
}
