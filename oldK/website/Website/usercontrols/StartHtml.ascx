<%@ Control Language="c#" AutoEventWireup="false" Codebehind="StartHtml.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.StartHtml" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Register TagPrefix="Kaneva" TagName="WebAnalytics" Src="~/usercontrols/WebAnalytics.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<asp:Literal runat="server" id="litHtmlHeadTag" />
  <HEAD>
    <asp:Literal runat=server id="litWebAnalyticsExperimentCode"/>
 	<title><asp:Literal runat=server id="litTitle"/></title> 
	<asp:Literal runat=server id="litMetaDataTitle"/>
	<asp:Literal runat=server id="litMetaDataDescription"/>
	<asp:Literal runat=server id="litMetaDataKeywords"/>
	<asp:Literal runat=server id="litStyleSheet"/>
	<asp:Literal runat=server id="litJavascriptDetect"/>
	<asp:Literal runat=server id="litJavascripts"/>
	<link rel="icon" href="http://www.kaneva.com/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="http://www.kaneva.com/favicon.ico" type="image/x-icon" />
  </HEAD>
	<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" onLoad="javascript:OnLoad();">
	

  <Kaneva:WebAnalytics id="WebAnalytics" runat="server"></Kaneva:webAnalytics>

	
	<center style="vertical-align: top;">