<%@ Control Language="c#" AutoEventWireup="false" Codebehind="MembersFilter.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.MembersFilter" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<table cellpadding="0" cellspacing="0" border="0" width="540" ID="Table1">
<tr>
<td class="Filter">filter by:&nbsp;	
					
<asp:DropdownList runat="server" Width="110px" class="Filter2" ID="drpMembers" AutoPostBack="True" OnSelectedIndexChanged="drpMembers_Change">															
	<asp:ListItem value="">member...</asp:ListItem>
	<asp:ListItem value=" ">----------</asp:ListItem>
	<asp:ListItem value="a">A</asp:ListItem>
	<asp:ListItem value="b">B</asp:ListItem>
	<asp:ListItem value="c">C</asp:ListItem>
	<asp:ListItem value="d">D</asp:ListItem>
	<asp:ListItem value="e">E</asp:ListItem>
	<asp:ListItem value="f">F</asp:ListItem>
	<asp:ListItem value="g">G</asp:ListItem>
	<asp:ListItem value="h">h</asp:ListItem>
	<asp:ListItem value="i">I</asp:ListItem>
	<asp:ListItem value="j">J</asp:ListItem>
	<asp:ListItem value="k">K</asp:ListItem>
	<asp:ListItem value="l">L</asp:ListItem>
	<asp:ListItem value="m">M</asp:ListItem>
	<asp:ListItem value="n">N</asp:ListItem>
	<asp:ListItem value="o">O</asp:ListItem>
	<asp:ListItem value="p">P</asp:ListItem>
	<asp:ListItem value="q">Q</asp:ListItem>
	<asp:ListItem value="r">R</asp:ListItem>
	<asp:ListItem value="s">S</asp:ListItem>
	<asp:ListItem value="t">T</asp:ListItem>
	<asp:ListItem value="u">U</asp:ListItem>
	<asp:ListItem value="v">V</asp:ListItem>
	<asp:ListItem value="w">W</asp:ListItem>
	<asp:ListItem value="x">X</asp:ListItem>
	<asp:ListItem value="y">Y</asp:ListItem>
	<asp:ListItem value="z">Z</asp:ListItem>
	<asp:ListItem value="1">1</asp:ListItem>
	<asp:ListItem value="2">2</asp:ListItem>
	<asp:ListItem value="3">3</asp:ListItem>
	<asp:ListItem value="4">4</asp:ListItem>
	<asp:ListItem value="5">5</asp:ListItem>
	<asp:ListItem value="6">6</asp:ListItem>
	<asp:ListItem value="7">7</asp:ListItem>
	<asp:ListItem value="8">8</asp:ListItem>
	<asp:ListItem value="9">9</asp:ListItem>
	<asp:ListItem value="0">0</asp:ListItem>
</asp:DropdownList>
<asp:DropdownList runat="server" size="1" class="Filter2" ID="drpRole" AutoPostBack="True" OnSelectedIndexChanged="drpRole_Change">											
</asp:DropdownList>
<asp:DropdownList runat="server" size="1" class="Filter2" ID="drpStatus" AutoPostBack="True" OnSelectedIndexChanged="drpStatus_Change">
				
</asp:DropdownList><img runat="server" src="~/images/spacer.gif" width="112" height="10" border="0"/>

<asp:DropdownList runat="server" size="1" class="Filter2" ID="drpPages" AutoPostBack="True" OnSelectedIndexChanged="drpPages_Change">	
</asp:DropdownList>

</td>
</tr>	
</table>