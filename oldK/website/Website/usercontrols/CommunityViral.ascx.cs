///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.usercontrols
{
    public partial class CommunityViral : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindFriends("");
            }
        }

        /// <summary>
        /// lbSkip_Click
        /// </summary>
        protected void lbSkip_Click(object sender, CommandEventArgs e)
        {
            DataRow drChannel = CommunityUtility.GetCommunity(_communityId);
            Response.Redirect(GetBroadcastChannelUrl(drChannel["name_no_spaces"].ToString()));
        }

        /// <summary>
        /// lbSearch_Click
        /// </summary>
        protected void lbSearch_Click(object sender, CommandEventArgs e)
        {
            BindFriends(Server.HtmlEncode(txtSearch.Text.Trim()));
        }

        /// <summary>
        /// lbSendInviteMore_Click
        /// </summary>
        protected void lbSendInviteMore_Click(object sender, CommandEventArgs e)
        {
            // Send out PM's to all selected
            SendInvitePMs();

            // Invite more via email
            Response.Redirect("~/mykaneva/inviteFriend.aspx");
        }

        /// <summary>
        /// lbSend_Click
        /// </summary>
        protected void lbSend_Click(object sender, CommandEventArgs e)
        {
            // Send out PM's to all selected
            SendInvitePMs();

            DataRow drChannel = CommunityUtility.GetCommunity(_communityId);
            Response.Redirect(GetBroadcastChannelUrl(drChannel["name_no_spaces"].ToString()));
        }

        private void SendInvitePMs()
        {
            DataRow drChannel = CommunityUtility.GetCommunity(_communityId);
            UserFacade userFacade = new UserFacade();

            string senderName = KanevaWebGlobals.CurrentUser.Username;

            int recipientId = 0;
            User recipient = new User();
            string strMessage = "";

            CheckBox chkEdit;
            HtmlInputHidden hidFriendId;

            // send invite or friend request to each contact that is checked
            // if email does not belong to Kaneva account, send invite
            // if email does belong to Kaneva account, send friend request
            foreach (RepeaterItem riFriend in rptFriends.Items)
            {
                chkEdit = (CheckBox)riFriend.FindControl("chkEdit");

                if (chkEdit.Checked)
                {
                    hidFriendId = (HtmlInputHidden)riFriend.FindControl("hidFriendId");
                    recipientId = Convert.ToInt32(hidFriendId.Value);
                    recipient = userFacade.GetUser(recipientId);

                    strMessage = "Hi " + recipient.Username + ",<BR><BR>" +
                        senderName + " invited you to join the Kaneva World " + drChannel["name"].ToString() + "." + ",<BR><BR>" +
                        "To see more details and confirm this World invitation, follow the link below:" + "<BR>" +
                        "<a href='" + ResolveUrl("~/community/commJoin.aspx?communityId=" + _communityId + "&join=Y") + "'>" + ResolveUrl("~/community/commJoin.aspx?communityId=" + _communityId + "&join=Y") + "</a>" + "<BR><BR>" +
                        "Thanks,<BR>" +
                        drChannel ["name"].ToString ();

                    // Insert the message
                    Message message = new Message(0, KanevaWebGlobals.GetUserId(), 
                        recipientId, 
                        senderName + " invited you to join the World " + drChannel["name"].ToString(),
                        strMessage, new DateTime (), 0, (int) Constants.eMESSAGE_TYPE.COMMUNITY_INVITES, _communityId, "U", "S");

                    userFacade.InsertMessage(message);
                }
            }

        }

        /// <summary>
        /// BindFriends
        /// </summary>
        private void BindFriends(string searchString)
        {
            string strSearchFilter = "";

            if (searchString.Length > 0)
            {
                strSearchFilter = " u.username = '" + searchString + "'";
            }

            rptFriends.DataSource = GetUserFacade.GetFriends(KanevaWebGlobals.GetUserId(), strSearchFilter, "", 1, 50);
            rptFriends.DataBind();
        }

        public void ShowUI()
        {
            BindFriends("");
        }

        public int CommunityId
        {
            get { return _communityId; }
            set { _communityId = value; }
        } 

        public int _communityId;
    }
}