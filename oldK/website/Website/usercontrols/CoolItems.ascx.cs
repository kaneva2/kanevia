///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

namespace KlausEnt.KEP.Kaneva.usercontrols
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

	using KlausEnt.KEP.Kaneva;

	/// <summary>
	///		Summary description for CoolItems.
	/// </summary>
	public class CoolItems : System.Web.UI.UserControl
	{

		private void Page_Load(object sender, System.EventArgs e)
		{
			if (!IsPostBack)
			{
				Literal litStream = new	 Literal ();
				litStream.Text = StoreUtility.GetAssetEmbedLink ( m_playlistType, m_height, m_width, this, true, false );
				phStream.Controls.Add (litStream);	
			}
		}

		/// <summary>
		/// The type of playlist
		/// </summary>
		public int PlaylistType
		{
			set 
			{
				m_playlistType = value;
			} 
			get
			{
				return m_playlistType;
			}
		}

		/// <summary>
		/// The height of the player
		/// </summary>
		public int PlaylistHeight
		{
			set 
			{
				m_height = value;
			} 
			get
			{
				return m_height;
			}
		}
		public int PlayerWidth
		{
			set 
			{
				m_width = value;
			} 
			get
			{
				return m_width;
			}
		}

		protected PlaceHolder	phStream;
		int m_playlistType = (int) Constants.COOL_ITEM_PLAYLIST_ID;
		int m_height = 1250;
		int m_width = 300;

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
