<%@ Control Language="c#" AutoEventWireup="false" Codebehind="Profile.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.usercontrols.Profile" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>

<asp:PlaceHolder Runat="server" ID="phStyleCustomizes" /> 

<table border="0" cellpadding="0"  cellspacing="0" align="left" width="100%" >							
	<tr>
		<td class="frTopLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
		<td class="frTop"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
		<td class="frTopRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
	</tr>
	<tr>
		<td class="frBorderLeft"><img runat="server" src="~/images/spacer.gif" width="6" height="1" /></td>
		<td valign="top" class="frBgInt1" align="center" width="100%">
			<table cellpadding="0" cellspacing="0" align="left" width="100%">
				<tr>
					<td colspan="3"><img runat="server" src="~/images/spacer.gif" width="1" height="6" /></td>
				</tr>										
				<tr>
					<td><img runat="server" src="~/images/spacer.gif" width="10" height="1" /></td>
					<td class="accountTitle" colspan="2" align="left">
						Personal
					</td>
				</tr>
				<tr>
					<td colspan="3"><img runat="server" src="~/images/spacer.gif" width="1" height="10" /></td>
				</tr>													
				<tr>
					<td><img runat="server" src="~/images/spacer.gif" width="10" height="1" /></td>
					<td>
						<table border="0" cellpadding="0" cellspacing="0" width="100%" >

							<tr>
								<td class="boxInsideTopLeft"><img runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
								<td class="boxInsideTop" ><img runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
								<td  class="boxInsideTopRight"><img runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
							</tr>
							<tr class="boxInside">
								<td class="boxInsideleft" ><img runat="server" src="~/images/spacer.gif" width="1" height="4" /></td>
								<td class="boxInsideContent" height="250">
									<table cellpadding="0" cellspacing="5" align="left">
									<!--form content-->
																																<tr>
												<td align="right" valign="middle" class="accountText" width="150">
													Relationship:&nbsp;
												</td>
												<td align="left">
													<asp:Dropdownlist runat="server" class="formKanevaSelect" ID="drpRelationship" style="width:150px">
														<asp:ListItem  value="">No Answer</asp:ListItem>
														<asp:ListItem  value="Single">Single</asp:ListItem>
														<asp:ListItem  value="In a relationship">In a relationship</asp:ListItem>
														<asp:ListItem  value="Married">Married</asp:ListItem>
														<asp:ListItem  value="Divorced">Divorced</asp:ListItem>
													</asp:Dropdownlist>
												</td>
											</tr>
											<tr id="trSexOrientation" runat="server" visible="false">
												<td align="right" valign="middle" class="accountText">
													Sexual&nbsp;&nbsp;<br>Orientation:&nbsp;
												</td>
												<td align="left">
													<asp:Dropdownlist runat="server" class="formKanevaSelect" ID="drpOrientation" style="width:150px">
														<asp:ListItem  value="">No Answer</asp:ListItem>
														<asp:ListItem  value="Straight">Straight</asp:ListItem>
														<asp:ListItem  value="Gay/lesbian">Gay/lesbian</asp:ListItem>
														<asp:ListItem  value="Bi-sexual">Bi-sexual</asp:ListItem>
														<asp:ListItem  value="Not sure">Not sure</asp:ListItem>
													</asp:Dropdownlist>
												</td>
											</tr>	
											<tr>
												<td align="right" valign="middle" class="accountText">
													Religion:&nbsp;
												</td>
												<td align="left">
													<asp:Dropdownlist runat="server" class="formKanevaSelect" ID="drpReligion" style="width:150px">
														<asp:ListItem  value="">No answer</asp:ListItem>
														<asp:ListItem  value="Agnostic">Agnostic</asp:ListItem>
														<asp:ListItem  value="Atheist">Atheist</asp:ListItem>
														<asp:ListItem  value="Buddhist">Buddhist</asp:ListItem>
														<asp:ListItem  value="Catholic">Catholic</asp:ListItem>
														<asp:ListItem  value="Christian (Other)">Christian (Other)</asp:ListItem>
														<asp:ListItem  value="Hindu">Hindu</asp:ListItem>
														<asp:ListItem  value="Jewish">Jewish</asp:ListItem>
														<asp:ListItem  value="Mormon">Mormon</asp:ListItem>
														<asp:ListItem  value="Muslim">Muslim</asp:ListItem>
														<asp:ListItem  value="Other">Other</asp:ListItem>
														<asp:ListItem  value="Protestant">Protestant</asp:ListItem>
														<asp:ListItem  value="Scientologist">Scientologist</asp:ListItem>
														<asp:ListItem  value="Taoist">Taoist</asp:ListItem>
														<asp:ListItem  value="Wiccan">Wiccan</asp:ListItem>
													</asp:Dropdownlist>		
												</td>
											</tr>
											<tr>
												<td align="right" valign="middle" class="accountText">
													Ethnicity:&nbsp;
												</td>
												<td align="left">
												<asp:Dropdownlist runat="server" class="formKanevaSelect" ID="drpEthnicity" style="width:150px">
													<asp:ListItem  value="">No answer</asp:ListItem>
													<asp:ListItem  value="Asian">Asian</asp:ListItem>
													<asp:ListItem  value="Black">Black</asp:ListItem>
													<asp:ListItem  value="East Indian">East Indian</asp:ListItem>
													<asp:ListItem  value="European">European</asp:ListItem>
													<asp:ListItem  value="Latino/Hispanic">Latino/Hispanic</asp:ListItem>
													<asp:ListItem  value="Middle Eastern">Middle Eastern</asp:ListItem>
													<asp:ListItem  value="Mixed">Mixed</asp:ListItem>
													<asp:ListItem  value="Native American">Native American</asp:ListItem>
													<asp:ListItem  value="Other">Other</asp:ListItem>
													<asp:ListItem  value="Pacific Rim">Pacific Rim</asp:ListItem>
													<asp:ListItem  value="White">White</asp:ListItem>
												</asp:Dropdownlist>
												</td>
											</tr>	
											<tr>
												<td align="right" valign="middle" class="accountText">
													Children:&nbsp;
												</td>
												<td align="left">
													<asp:Dropdownlist runat="server" class="formKanevaSelect" ID="drpChildren" style="width:150px">
														<asp:ListItem  value="">No answer</asp:ListItem>
														<asp:ListItem  value="Don't want any">Don't want any</asp:ListItem>
														<asp:ListItem  value="One day">One day</asp:ListItem>
														<asp:ListItem  value="Undecided">Undecided</asp:ListItem>
														<asp:ListItem  value="I am a parent">I am a parent</asp:ListItem>
													</asp:Dropdownlist>
												</td>
											</tr>	
											<tr>
												<td align="right" valign="middle" class="accountText">
													Education:&nbsp;
												</td>
												<td align="left">
													<asp:Dropdownlist runat="server" class="formKanevaSelect" ID="drpEducation" style="width:150px">
														<asp:ListItem  value="">No answer</asp:ListItem>
														<asp:ListItem  value="Dropout">Dropout</asp:ListItem>
														<asp:ListItem  value="High School">High School</asp:ListItem>
														<asp:ListItem  value="Some college">Some college</asp:ListItem>
														<asp:ListItem  value="In college">In college</asp:ListItem>
														<asp:ListItem  value="Bachelors Degree">Bachelors Degree</asp:ListItem>
														<asp:ListItem  value="Masters Degree">Masters Degree</asp:ListItem>
														<asp:ListItem  value="Doctorate">Doctorate</asp:ListItem>
													</asp:Dropdownlist>
												</td>
											</tr>
											<tr>
												<td  align="right" valign="middle" class="accountText">
													Income:&nbsp;
												</td>
												<td align="left">
													<asp:Dropdownlist runat="server" class="formKanevaSelect" ID="drpIncome" style="width:210px">
														<asp:ListItem  value="">No answer</asp:ListItem>
														<asp:ListItem  value="1">< $20,000</asp:ListItem>
														<asp:ListItem  value="2">Between $20,000 and $30,000</asp:ListItem>
														<asp:ListItem  value="3">Between $30,000 and $40,000</asp:ListItem>
														<asp:ListItem  value="4">Between $40,000 and $50,000</asp:ListItem>
														<asp:ListItem  value="5">Between $60,000 and $80,000</asp:ListItem>
														<asp:ListItem  value="6">Between $80,000 and $100,000</asp:ListItem>
														<asp:ListItem  value="7">Between $100,000 and $150,000</asp:ListItem>
														<asp:ListItem  value="8">$150,000+</asp:ListItem>
													</asp:Dropdownlist>
												</td>
											</tr>	
											<tr>
												<td  align="right" valign="middle" class="accountText">
													Height:&nbsp;
											
												</td>
												<td class="accountText" align="left">
													<asp:Dropdownlist runat="server" class="formKanevaSelect" ID="drpHeightFeet">
														<asp:ListItem  value="0">-</asp:ListItem>
														<asp:ListItem  value="3">3</asp:ListItem>
														<asp:ListItem  value="4">4</asp:ListItem>
														<asp:ListItem  value="5">5</asp:ListItem>
														<asp:ListItem  value="6">6</asp:ListItem>
														<asp:ListItem  value="7">7</asp:ListItem>
													</asp:Dropdownlist> Feet
													<asp:Dropdownlist runat="server" class="formKanevaSelect" ID="drpHeightInches">
														<asp:ListItem  value="-1">-</asp:ListItem>
														<asp:ListItem  value="0">0</asp:ListItem>
														<asp:ListItem  value="1">1</asp:ListItem>
														<asp:ListItem  value="2">2</asp:ListItem>
														<asp:ListItem  value="3">3</asp:ListItem>
														<asp:ListItem  value="4">4</asp:ListItem>
														<asp:ListItem  value="5">5</asp:ListItem>
														<asp:ListItem  value="6">6</asp:ListItem>
														<asp:ListItem  value="7">7</asp:ListItem>
														<asp:ListItem  value="8">8</asp:ListItem>
														<asp:ListItem  value="9">9</asp:ListItem>
														<asp:ListItem  value="10">10</asp:ListItem>
														<asp:ListItem  value="11">11</asp:ListItem>
													</asp:Dropdownlist> Inches 
												</td>
											</tr>
											
											<tr id="trSmoking" runat="server" visible="false">
												<td  align="right" valign="middle" class="accountText">
													Smoking:&nbsp;
												</td>
												<td align="left">
													<asp:Dropdownlist runat="server" class="formKanevaSelect" ID="drpSmoking" style="width:150px">
														<asp:ListItem  value="">No Answer</asp:ListItem>
														<asp:ListItem  value="Yes">Yes</asp:ListItem>
														<asp:ListItem  value="No">No</asp:ListItem>
													</asp:Dropdownlist>
												</td>
											</tr>	
											<tr id="trDrinking" runat="server" visible="false">
												<td align="right" valign="middle" class="accountText">
													Drinking:&nbsp;
												</td>
												<td align="left">
											
													<asp:Dropdownlist runat="server" class="formKanevaSelect" ID="drpDrinking" style="width:150px">
														<asp:ListItem  value="">No Answer</asp:ListItem>
														<asp:ListItem  value="Yes">Yes</asp:ListItem>
														<asp:ListItem  value="No">No</asp:ListItem>
													</asp:Dropdownlist>
												</td>
											</tr>
													
									
									<!-- end form-->
									</table>
								</td>
								<td class="boxInsideRight"><img runat="server" src="~/images/spacer.gif" width="1" height="4"/></td>
							</tr>

							<tr>
								<td class="boxInsideBottomLeft" ><img runat="server" src="~/images/spacer.gif" width="4" height="4" /></td>
								<td class="boxInsideBottom2"><img runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
								<td class="boxInsideBottomRight"><img runat="server" src="~/images/spacer.gif" width="4" height="4" /></td>
							</tr>
							<tr><td colspan="3"><img runat="server" src="~/images/spacer.gif" width="1" height="10" /></td></tr>
						</table>																		
					</td>
					<td><img runat="server" src="~/images/spacer.gif" width="10" height="1" /></td>
				</tr>
			</table>
		<td class="frBorderRight"><img runat="server" src="~/images/spacer.gif" width="6" height="1" /></td>
	</tr>
	<tr>
		<td class="frBottomLeft"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
		<td class="frBottom"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
		<td class="frBottomRight"><img runat="server" src="~/images/spacer.gif" width="1" height="1" /></td>
	</tr>
</table>
