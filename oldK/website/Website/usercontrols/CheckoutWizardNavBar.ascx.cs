///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

namespace KlausEnt.KEP.Kaneva
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

	/// <summary>
	///		Summary description for CheckoutWizardNavBar.
	/// </summary>
	public class CheckoutWizardNavBar : KlausEnt.KEP.Kaneva.usercontrols.BaseUserControl
	{
		private void Page_Load(object sender, System.EventArgs e)
		{
			if (!IsPostBack)
			{
				BindSteps();
			}
		}

		#region Helper Methods
		public void BindSteps()
		{
			liOne.Attributes.Add("class", ClassIncomplete);
			liTwo.Attributes.Add("class", ClassIncomplete);
			liThree.Attributes.Add("class", ClassIncomplete);
			liFour.Attributes.Add("class", ClassIncomplete);

			// Set selected tab
			switch (ActiveStep)
			{
				case STEP.STEP_1:
					liOne.Attributes.Add("class", ClassActive);
					break;

				case STEP.STEP_2:
					liOne.Attributes.Add("class", ClassComplete);
					liTwo.Attributes.Add("class", ClassActive);
					break;

				case STEP.STEP_3:
					liOne.Attributes.Add("class", ClassComplete);
					liTwo.Attributes.Add("class", ClassComplete);
					liThree.Attributes.Add("class", ClassActive);
					break;

				case STEP.STEP_4:
					liOne.Attributes.Add("class", ClassComplete);
					liTwo.Attributes.Add("class", ClassComplete);
					liThree.Attributes.Add("class", ClassComplete);
					liFour.Attributes.Add("class", ClassActive);
					break;
			}

			aOne.HRef = "";
			aTwo.HRef = "";
			aThree.HRef = "";
			aFour.HRef = "";
			
			aOne.Style.Add("cursor","default");
			aTwo.Style.Add("cursor","default");
			aThree.Style.Add("cursor","default");
			aFour.Style.Add("cursor","default");

			if (StepOneHref != null && StepOneHref != string.Empty)
			{
				aOne.HRef = StepOneHref;
				aOne.Style.Add("cursor","pointer");
			}
			if (StepTwoHref != null && StepTwoHref != string.Empty)
			{
				aTwo.HRef = StepTwoHref;
				aTwo.Style.Add("cursor","pointer");
			}
			if (StepThreeHref != null && StepThreeHref != string.Empty)
			{
				aThree.HRef = StepThreeHref;
				aThree.Style.Add("cursor","pointer");
			}
			if (StepFourHref != null && StepFourHref != string.Empty)
			{
				aFour.HRef = StepFourHref;
				aFour.Style.Add("cursor","pointer");
			}
		}
		#endregion

		#region Properties

		/// <summary>
		/// The current step 
		/// </summary>
		public STEP ActiveStep
		{
			get 
			{
				return m_ActiveStep;
			} 
			set
			{
				m_ActiveStep = value;
			}
		}
	
		public string ClassIncomplete
		{
			get 
			{
				return m_class_incomplete;
			} 
			set
			{
				m_class_incomplete = value;
			}
		}
			
		public string ClassActive
		{
			get 
			{
				return m_class_active;
			} 
			set
			{
				m_class_active = value;
			}
		}
			
		public string ClassComplete
		{
			get 
			{
				return m_class_complete;
			} 
			set
			{
				m_class_complete = value;
			}
		}
			

		public string StepOneHref
		{
			get 
			{
				return m_one_href;
			} 
			set
			{
				m_one_href = value;
			}
		}
		public string StepTwoHref
		{
			get 
			{
				return m_two_href;
			} 
			set
			{
				m_two_href = value;
			}
		}
		public string StepThreeHref
		{
			get 
			{
				return m_three_href;
			} 
			set
			{
				m_three_href = value;
			}
		}
		public string StepFourHref
		{
			get 
			{
				return m_four_href;
			} 
			set
			{
				m_four_href = value;
			}
		}

		#endregion
		
		#region Declerations

		private string	m_class_incomplete	= "incomplete";
		private string	m_class_active		= "active";
		private string	m_class_complete	= "completed";

		private string m_one_href, m_two_href, m_three_href, m_four_href = string.Empty;

		private STEP m_ActiveStep = STEP.NONE;

		protected HtmlGenericControl	liOne, liTwo, liThree, liFour;
		protected HtmlAnchor			aOne, aTwo, aThree, aFour;

		public enum STEP
		{
			STEP_1,
			STEP_2,
			STEP_3,
			STEP_4,
			NONE
		}
		
		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
