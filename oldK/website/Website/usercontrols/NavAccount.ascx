<%@ Control Language="c#" AutoEventWireup="false" Codebehind="NavAccount.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.NavAccount" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>


<ul class="secondary_nav">
	<li id="navGeneral" runat="server" class="general"><a id="aGeneral" runat="server">General</a></li>
	<li id="navPreferences" runat="server" class="prefs"><a id="aPreferences" runat="server">Preferences</a></li>
	<li id="navBilling" runat="server" class="billing"><a id="aBilling" runat="server">Billing</a></li>
	<li id="navTransactions" runat="server" class="trans"><a id="aTransactions" runat="server">Transactions</a></li>
	<li id="navInventory" runat="server" class="inventory"><a id="aInventory" runat="server">Inventory</a></li>
</ul>
