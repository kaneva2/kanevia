<%@ Control Language="c#" AutoEventWireup="false" Codebehind="BiggestGainersChannel.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.usercontrols.BiggestGainersChannel" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ OutputCache Duration="300" VaryByParam="none" %>


<table cellpadding="7" cellspacing="0" width="95%" border="0" class="greenbox">
	<tr>
		<td width="55">
			<div class="framesize-xsmall">
				<div class="frame">
					<span class="ct"><span class="cl"></span></span>
					<img src="images/test_pic_small.jpg" alt="test_pic.jpg" width="45" border="0">
					<span class="cb"><span class="cl"></span></span>
				</div>
			</div>
		</td>
		<td>
			<p><strong>Members Added</strong></p>
			<p class="small"><asp:label runat="server" id="lblMembersAdded" />World of Kaneva: 25</p>
		</td>
	</tr>
	<tr>
		<td width="55">
			<div class="framesize-xsmall">
				<div class="frame">
					<span class="ct"><span class="cl"></span></span>
					<img src="images/test_pic_small.jpg" alt="test_pic.jpg" width="45" border="0">
					<span class="cb"><span class="cl"></span></span>
				</div>
			</div>
		</td>
		<td>
			<p><strong>Videos Uploaded</strong></p>
			<p class="small"><asp:label runat="server" id="lblVideosUploaded" />UGTV: 125</p>
		</td>
	</tr>
	<tr>
		<td width="55">
			<div class="framesize-xsmall">
				<div class="frame">
					<span class="ct"><span class="cl"></span></span>
					<img src="images/test_pic_small.jpg" alt="test_pic.jpg" width="45" border="0">
					<span class="cb"><span class="cl"></span></span>
				</div>
			</div>
		</td>
		<td>
			<p><strong>Comments Posted</strong></p>
			<p class="small"><asp:label runat="server" id="lblCommentsPosted" />Hardcore Anime: 25</p>
		</td>
	</tr>
	
</table>
