///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Kaneva.usercontrols
{
	/// <summary>
	/// Summary description for TagCloud.
	/// </summary>
	public class TagCloud1 : NoBorderPage
	{
		private void Page_Load(object sender, System.EventArgs e)
		{
			if (!IsPostBack)
			{
				try
				{
					if (ucTagCloud != null)
					{
						int cloud_type_id = (int) Constants.eASSET_TYPE.VIDEO;
					
						if (Request.QueryString ["tag"] != null && Request.QueryString ["tag"] != string.Empty)
						{
							cloud_type_id = Convert.ToInt32 (Request.QueryString ["tag"]);

							switch (cloud_type_id)
							{
								case (int) Constants.eASSET_TYPE.VIDEO:
								case (int) Constants.eASSET_TYPE.PICTURE:
								case (int) Constants.eASSET_TYPE.MUSIC:
								case (int) Constants.eASSET_TYPE.GAME:
								case (int) Constants.eASSET_TYPE.PATTERN:
									ucTagCloud.TargetPage = "watch/media.aspx";
									break;
							}
						}
			
						ucTagCloud.CloudType = cloud_type_id;
					}
				}
				catch {}
			}
		}

		#region Declerations

		protected Kaneva.usercontrols.TagCloud	ucTagCloud;

		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
