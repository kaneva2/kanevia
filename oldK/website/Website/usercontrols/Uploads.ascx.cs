///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

namespace KlausEnt.KEP.Kaneva.usercontrols
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

	/// <summary>
	///		Summary description for Uploads.
	/// </summary>
	public class Uploads : System.Web.UI.UserControl
	{

		private void Page_Load(object sender, System.EventArgs e)
		{
			GetMediaCounts ();
		}

		private void GetMediaCounts ()
		{
			// Get the counts
//			lblVideoCount.Text = StoreUtility.CountAssets ((int) Constants.eASSET_TYPE.VIDEO).ToString ();
//			lblPhotoCount.Text = StoreUtility.CountAssets ((int) Constants.eASSET_TYPE.PICTURE).ToString ();
//			lblMusicCount.Text = StoreUtility.CountAssets ((int) Constants.eASSET_TYPE.MUSIC).ToString ();
//			lblGamesCount.Text = StoreUtility.CountAssets ((int) Constants.eASSET_TYPE.GAME).ToString ();

			// Get the media counts
			DataRow drMediaCount = StoreUtility.GetTotalMediaCounts ();
			lblVideoCount.Text = drMediaCount ["sum_video"].ToString ();
			lblPhotoCount.Text = drMediaCount ["sum_picture"].ToString ();
			lblMusicCount.Text =  drMediaCount ["sum_music"].ToString ();
			lblGamesCount.Text = drMediaCount ["sum_game"].ToString ();
		}

		protected Label lblVideoCount, lblPhotoCount, lblMusicCount, lblGamesCount;

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
