///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.usercontrols
{
    public partial class Activities : BaseUserControl
    {
        Kaneva.mykaneva.MessageCenter mc = new Kaneva.mykaneva.MessageCenter ();

        protected void Page_Load (object sender, EventArgs e)
        {
        }

        /// <summary>
        /// BindPassiveBlasts
        /// </summary>
        /// <param name="pageNumber"></param>
        public int BindNotifications (int pageNumber, bool myActivities)
        {
            try
            {
                global::Kaneva.BusinessLayer.BusinessObjects.User Current_User = KanevaWebGlobals.CurrentUser;

                //user imposed filters on passive blast
                string userVisibilityFilter = "";
                if (Current_User != null)
                {
                    if (Current_User.BlastShowBlogs)
                    {
                        userVisibilityFilter += (int) BlastTypes.BLOG + ",";
                    }
                    if (Current_User.BlastShowComments)
                    {
                        userVisibilityFilter += (int) BlastTypes.MEDIA_COMMENT + ",";
                    }
                    if (Current_User.BlastShowForums)
                    {
                        userVisibilityFilter += (int) BlastTypes.FORUM_POST + ",";
                    }
                    if (Current_User.BlastShowJoinCommunity)
                    {
                        userVisibilityFilter += (int) BlastTypes.JOIN_COMMUNITY + ",";
                    }
                    if (Current_User.BlastShowProfiles)
                    {
                        userVisibilityFilter += (int) BlastTypes.PROFILE_UPDATE + ",";
                    }
                    if (Current_User.BlastShowRave3dHangout)
                    {
                        userVisibilityFilter += (int) BlastTypes.RAVE_3D_HANGOUT + ",";
                    }
                    if (Current_User.BlastShowRave3dHome)
                    {
                        userVisibilityFilter += (int) BlastTypes.RAVE_3D_HOME + ",";
                    }
                    if (Current_User.BlastShowSendGift)
                    {
                        userVisibilityFilter += (int) BlastTypes.SEND_GIFT + ",";
                    }
                    if (Current_User.BlastShowRave3dApp)
                    {
                        userVisibilityFilter += (int)BlastTypes.RAVE_3D_APP + ",";
                    }
                    if (Current_User.BlastShowCameraScreenshot)
                    {
                        userVisibilityFilter += (int)BlastTypes.CAMERA_SCREENSHOT + ",";
                    }

                    userVisibilityFilter += (int) BlastTypes.FAME + ",";

                    if (userVisibilityFilter.Length > 0)
                    {
                        userVisibilityFilter = userVisibilityFilter.Substring (0, (userVisibilityFilter.Length - 1));
                    }
                }

                int pageSize = 20;

                IList<Blast> plPassiveBlasts = null;

                if (myActivities)
                {
                    userVisibilityFilter += "," + (int) BlastTypes.FAME_LEVELUP; 
                    plPassiveBlasts = GetBlastFacade.GetMyPassiveBlasts (KanevaWebGlobals.CurrentUser.UserId, pageSize, userVisibilityFilter, pageNumber);
                }
                else
                {
                    plPassiveBlasts = GetBlastFacade.GetPassiveBlasts (KanevaWebGlobals.CurrentUser.UserId, pageSize, userVisibilityFilter, pageNumber);
                }

                if (plPassiveBlasts.Count > 0)
                {
                    //Blasts
                    rptActivities.DataSource = plPassiveBlasts;
                    rptActivities.DataBind ();
                }
                else
                {
                    string msg = "There are currently no activity items to display.";
                    string callout = ""; //"Would you like to <a href=\"" + ResolveUrl ("~/people/people.kaneva") + "\">Find Friends</a> or " +
                    //             " view <a href=\"javascript:$('btnFriendInvites').click();\">Friend Requests</a>?";
                    mc.ShowNoResults (msg, callout);
                }

                return plPassiveBlasts.Count;

                // Set up pager
                //litJS.Text = "<script type=\"text/javascript\">setPager(" + MaxBlasts.ToString() + "," + plPassiveBlasts.TotalCount.ToString() + ");</script>";
                // }
            }
            catch { return 0; }
        }

        /// <summary>
        /// Repeater rptBlasts ItemDataBound Event Handler
        /// </summary>
        protected void rptNotifications_ItemDataBound (Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                try
                {
                    bool isLevelUp = false;
                    string className = "";
                    Blast blast = (Blast) e.Item.DataItem;

                    switch (blast.BlastType)
                    {
                        case (int) BlastTypes.BLOG:
                            className = "blog";
                            break;

                        case (int) BlastTypes.MEDIA_COMMENT:
                            className = "comment";
                            break;

                        case (int) BlastTypes.FORUM_POST:
                            className = "forum";
                            break;

                        case (int) BlastTypes.JOIN_COMMUNITY:
                            className = "community";
                            break;

                        case (int) BlastTypes.PROFILE_UPDATE:
                            className = "profile";
                            break;

                        case (int) BlastTypes.RAVE_3D_HANGOUT:
                            className = "rave";
                            break;

                        case (int) BlastTypes.RAVE_3D_HOME:
                            className = "rave";
                            break;

                        case (int) BlastTypes.SEND_GIFT:
                            className = "gift";
                            break;

                        case (int)BlastTypes.RAVE_3D_APP:
                            className = "apps3d";
                            break;

                        case (int)BlastTypes.CAMERA_SCREENSHOT:
                            className = "apps3d";
                            break;

                        case (int) BlastTypes.FAME:
                            className = "fame";
                            break;

                        case (int) BlastTypes.FAME_LEVELUP:
                            isLevelUp = true;
                            className = "fame";
                            break;
                    }

                    HtmlContainerControl icon = (HtmlContainerControl) e.Item.FindControl ("divIcon");
                    icon.Attributes.Add ("class", "icon " + className);
                    
                    HtmlContainerControl subject = (HtmlContainerControl) e.Item.FindControl ("divSubject");
                    subject.InnerHtml += blast.Subj;
                    if (isLevelUp)
                    {
                        subject.InnerHtml += "<a id=\"a" + blast.EntryId + "\" class=\"lvluplink\" href=\"javascript:void(0);\" onclick=\"ViewDetails($('" + blast.EntryId + "'), $('a" + blast.EntryId + "'));\">show details</a>";
                    }
                    
                    HtmlContainerControl date = (HtmlContainerControl) e.Item.FindControl ("divDate");
                    date.InnerHtml = FormatDateTimeSpan (blast.DateCreated, blast.CurrentTime);
                    
                    if (isLevelUp)
                    {
                        HtmlContainerControl details = (HtmlContainerControl) e.Item.FindControl ("divDetails");
                        details.InnerHtml = "<div id=\"" + blast.EntryId + "\" style=\"display:none;\">" + blast.DiaryEntry + "</div>";
                    }
                }
                catch {}
            }
        }


        private int GetUserId ()
        {
            return KanevaWebGlobals.GetUserId ();
        }
    }
}