<%@ Control Language="c#" AutoEventWireup="false" Codebehind="uc_AssetDetails.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.usercontrols.uc_AssetDetails" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>
<script type="text/javascript" src="..\jscript\SWFObject\swfobject.js"></script>
<table id="ucAssetDetailTable" border="0" height="100%" cellSpacing="0" cellPadding="0"
	width="100%" align="center">
	<!-- PLAYER / IMAGE -->
	<tr>
		<td align="center">
			<div class="module whitebg"><span class="ct"><span class="cl"></span></span><asp:label id="lblImageCaption" style="FONT-WEIGHT: bold" runat="server" cssclass="dateStamp"
					visible="false"></asp:label><asp:placeholder id="phStream" runat="server"></asp:placeholder><span class="cb"><span class="cl"></span></span></div>
		</td>
	</tr>
	<!-- TOOLBAR -->
	<tr id="trWidgetButtonBar" style="MARGIN-TOP: 14px" runat="server">
		<td align="center">
			<div class="module whitebg"><span class="ct"><span class="cl"></span></span>
				<ajax:ajaxpanel id="Ajaxpanel1" runat="server">
					<TABLE cellSpacing="0" cellPadding="0" width="95%" border="0">
						<TR>
							<TD vAlign="bottom" align="center" width="20%">
								<asp:imagebutton id="btnVote" style="CURSOR: hand" cssclass="widgetText9" runat="server" imageurl="~/images/widgets/widget_raveit_bt.gif"
									width="18" height="18"></asp:imagebutton></TD>
							<TD vAlign="bottom" align="center" width="20%">
								<asp:imagebutton id="hlFave" cssclass="widgetText9" runat="server" imageurl="~/images/widgets/widget_addc_bt.gif"
									width="25" height="19" tooltip="Add to my library"></asp:imagebutton></TD>
							<TD vAlign="bottom" align="center" width="20%">
								<asp:imagebutton id="hlShare" runat="server" imageurl="~/images/widgets/widget_shares_bt.gif" width="22"
									height="22" tooltip="Share this item!"></asp:imagebutton></TD>
							<TD vAlign="bottom" align="center" width="20%">
								<asp:imagebutton id="hlComments" runat="server" imageurl="~/images/widgets/widget_commet_bt.gif"
									width="21" height="21" tooltip="Comment on this Item!"></asp:imagebutton></TD>
							<TD vAlign="bottom" align="center" width="20%">
								<asp:imagebutton id="hlViews" runat="server" imageurl="~/images/widgets/widget_viewc_bt.gif" width="22"
									height="16" tooltip="View this Item" target="_resource"></asp:imagebutton></TD>
						</TR>
						<TR>
							<TD colSpan="9"><IMG height="2" src="~/images/spacer.gif" width="1" border="0" runat="server"></TD>
						</TR>
						<TR class="widgetText9">
							<TD align="center">
								<asp:linkbutton id="hlVote" visible="true" runat="server" tooltip="Rave this item">
									<asp:label id="lblVotes" runat="server" text="0"></asp:label>&nbsp;Raves</asp:linkbutton></TD>
							<TD align="center">
								<asp:linkbutton id="hlFave2" visible="true" runat="server" tooltip="Add to my library"></asp:linkbutton></TD>
							<TD align="center">
								<asp:linkbutton id="hlShare2" visible="true" runat="server" tooltip="Share this item">
							<asp:label id="lblNumShares" runat="server" /></a></asp:linkbutton></TD>
							<TD align="center">
								<asp:linkbutton id="hlComments2" visible="true" runat="server" tooltip="Comment on this Item!">
									<asp:label id="lblNumComments" runat="server" />
								</asp:linkbutton></TD>
							<TD align="center">
								<asp:linkbutton id="hlViews2" visible="true" runat="server" tooltip="View this Item">
									<asp:label id="lblNumViews" runat="server" />
								</asp:linkbutton></TD>
						</TR>
					</TABLE>
				</ajax:ajaxpanel>
				<span class="cb"><span class="cl"></span></span>
			</div>
		</td>
	</tr>
	<tr>
		<td><IMG height="14" src="~/images/spacer.gif" width="1" runat="server"></td>
	</tr>
</table>
