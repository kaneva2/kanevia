<%@ Control Language="c#" AutoEventWireup="false" Codebehind="NewPeople.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.usercontrols.NewPeople" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ OutputCache Duration="300" VaryByParam="none" %>


<table cellpadding="10" cellspacing="0" border="0" width="98%">
	<tr>
		<td>	

			<asp:datalist visible="true" runat="server" enableviewstate="False" showfooter="False" width="100%" id="dlNewPeople"
				cellpadding="0" cellspacing="0" itemstyle-horizontalalign="Center" repeatcolumns="4" repeatdirection="Horizontal" horizontalalign="Center">
				
				<itemtemplate>

					<div class="framesize-medium">
						<div class="frame">
							<span class="ct"><span class="cl"></span></span>
							<div class="imgconstrain">
								<a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "name_no_spaces").ToString ())%> style="cursor: hand;">
									<img runat="server" src='<%#GetProfileImageURL (DataBinder.Eval(Container.DataItem, "thumbnail_medium_path").ToString (), "me", DataBinder.Eval(Container.DataItem, "gender").ToString ())%>' border="0"/>
								</a>
							</div>
							<p><a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "name_no_spaces").ToString ()) %>><%# TruncateWithEllipsis (Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "username").ToString ()), 11) %></a></p>
							<p class="location"><%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "location").ToString ())%></a></p>
							<span class="cb"><span class="cl"></span></span>
						</div>
					</div>

				</itemtemplate>
				
			</asp:datalist>	

		</td>
	</tr>
</table>



											
																			




																	
									