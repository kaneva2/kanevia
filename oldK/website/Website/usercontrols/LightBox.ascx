<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LightBox.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.usercontrols.LightBox" %>

<script type="text/javascript">
	var $j = jQuery.noConflict();
	function showNotAvailable() {
		$j.facebox({ div: "#notavail" });
		$j('#fbtitle').text("World Access");
		return false;
	}
</script>
<script language="JavaScript" type="text/javascript" src="<%= ResolveUrl("~/jscript/facebox/jquery.facebox.js") %>"></script>
<link href="<%= ResolveUrl("~/jscript/facebox/jquery.facebox.css") %>" rel="stylesheet" type="text/css" />


<div id="divFacebox" runat="server" visible="true">



</div>

<div id="notavail" class="kdialog" style="display:none;">
	<img class="icon" src="<%= ResolveUrl("~/images/facebox/world_icon.jpg") %>" />
	<div>This World in not available.</div>
	<hr />
	<p><a class="btnxsmall black" id="btnOK" onclick="$j(document).trigger('close.facebox');" href="javascript:void(0);">OK</a></p>
</div>													  
