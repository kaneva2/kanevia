﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FacebookJavascriptInclude.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.usercontrols.FacebookJavascriptInclude" %>

<script type="text/javascript">
var fbappid = <%= GetFacebookAppId() %>;
var hosturl = "<%= GetHostUrl() %>";
var urlparams = "<%= GetUrlParams() %>";
var fbConnectOnLoad = "<%= GetConnectOnLoad() %>";

</script>
<asp:literal id="litFBJS" runat="server"></asp:literal>