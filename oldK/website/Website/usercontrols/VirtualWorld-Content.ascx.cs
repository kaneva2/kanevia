///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.IO;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using log4net;


namespace KlausEnt.KEP.Kaneva.usercontrols
{
    public partial class VirtualWorld_Content : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["contentpath"] != null)
            {
                litContent.Text = readFile("../3d-virtual-world/content-files/" + Request.QueryString["contentpath"]);
            }
            else
            {
                litContent.Text = readFile("../3d-virtual-world/content-files/whats-new.html");
            }
        }
        
        private String readFile(string path)
        {
            string pageContent = (string)Global.Cache()[path];
            if (pageContent == null || pageContent.Length.Equals(0))
            {                                
                try
                {
                    String result;
                    StreamReader sr = File.OpenText(Server.MapPath(path) );
                    {
                        result = sr.ReadToEnd();
                        // Close and clean up the StreamReader
                        sr.Close();
                    }
                    pageContent = result;
                }
                catch (Exception exc)
                {
                    m_logger.Error("Error retrieving file, fname =" + path, exc);
                    pageContent = "";
                }
                // Add to the cache
                Global.Cache().Insert(path, pageContent, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
            }           
            return pageContent;
        }


        protected Literal litContent;
        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

    }
}