<%@ Control Language="c#" AutoEventWireup="false" Codebehind="ProfileInterests.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.usercontrols.ProfileInterests" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Register TagPrefix="CE" Namespace="CuteEditor" Assembly="CuteEditor" %>

<script language="javascript" src="../jscript/prototype.js"></script>

<table border="0" cellpadding="0" cellspacing="0" width="100%" >
	<tr>
		<td class="boxInsideTopLeft"><img runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
		<td class="boxInsideTop" ><img runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
		<td  class="boxInsideTopRight"><img runat="server" src="~/images/spacer.gif" width="4" height="4"/></td>
	</tr>
	<tr class="boxInside">
		<td class="boxInsideleft" ><img runat="server" src="~/images/spacer.gif" width="1" height="4" /></td>
		<td class="boxInsideContent">
			<!--CONTENT-->
			<table cellpadding="0" cellspacing="0" width="100%" border="0" align="center" >
				<tr>
					<td colspan="7"><img runat="server" src="~/images/spacer.gif" width="1" height="15" /></td>
				</tr>
				
				<!-- MY TITLE -->
				<tr>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" /></td>
					<td class="accountText" align="right" valign="top" width="150">My Title:</td>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" /></td>		
					<td align="left" width="707">
						<CE:Editor id="txtTitle" ShowDecreaseButton="false" ShowEnlargeButton="False" runat="server" MaxTextLength="1000" width="100%" Height="150px" ConfigurationPath="~/CuteSoft_Client/CuteEditor/Configuration/AutoConfigure/profile.config" AutoConfigure="None" ></CE:Editor>
					</td>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" /></td>
					<td valign="top" width="100" align="left"></td>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" /></td>
				</tr>
				<tr>
					<td colspan="7"><img runat="server" src="~/images/spacer.gif" width="1" height="15" /></td>
				</tr>
				
				<!-- ABOUT ME -->
				<tr>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" /></td>
					<td class="accountText" align="right" valign="top" width="150">About me:</td>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" /></td>		
					<td align="left" width="707">
						<CE:Editor id="txtAboutMe" ShowDecreaseButton="false" ShowEnlargeButton="False" BackColor="#ededed" runat="server" MaxTextLength="1000" width="100%" Height="150px" ConfigurationPath="~/CuteSoft_Client/CuteEditor/Configuration/AutoConfigure/profile.config" AutoConfigure="None" ></CE:Editor>
					</td>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" /></td>
					<td valign="top" width="100" align="left"></td>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" /></td>
				</tr>
				<tr>
					<td colspan="7"><img runat="server" src="~/images/spacer.gif" width="1" height="15" /></td>
				</tr>
				
				<!-- HOBBIES -->			
				<tr>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img1"/></td>
					<td class="accountText" align="right" valign="top" width="150">Hobbies:</td>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img2"/></td>		
					<td align="left" width="707">
						<ce:editor id="txtHobbies" showdecreasebutton="false" showenlargebutton="False" backcolor="#ededed" runat="server" maxtextlength="1000" width="100%" height="150px" configurationpath="~/CuteSoft_Client/CuteEditor/Configuration/AutoConfigure/profile.config" autoconfigure="None" ></ce:editor>
					</td>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img3"/></td>
					<td valign="top" width="100" align="left"></td>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img4"/></td>
				</tr>
				<tr>
					<td colspan="7"><img runat="server" src="~/images/spacer.gif" width="1" height="15" id="Img5"/></td>
				</tr>																																									

				<!-- ASSOCIATIONS/CLUBS -->
				<tr>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img51"/></td>
					<td class="accountText" align="right" valign="top" width="150">Associations/Clubs:</td>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img52"/></td>		
					<td align="left" width="707">
						<ce:editor id="txtClubs" showdecreasebutton="false" showenlargebutton="False" runat="server" maxtextlength="1000" width="100%" height="150px" configurationpath="~/CuteSoft_Client/CuteEditor/Configuration/AutoConfigure/profile.config" autoconfigure="None" ></ce:editor>
					</td>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img53"/></td>
					<td valign="top" width="100" align="left"></td>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img54"/></td>
				</tr>
				<tr>
					<td colspan="7"><img runat="server" src="~/images/spacer.gif" width="1" height="15" id="Img55"/></td>
				</tr>

				<!-- FAVORITE MOVIES -->
				<tr>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img31"/></td>
					<td class="accountText" align="right" valign="top" width="150">Favorite Movies:</td>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img32"/></td>		
					<td align="left" width="707">
						<ce:editor id="txtMovies" showdecreasebutton="false" showenlargebutton="False" runat="server" maxtextlength="1000" width="100%" height="150px" configurationpath="~/CuteSoft_Client/CuteEditor/Configuration/AutoConfigure/profile.config" autoconfigure="None" ></ce:editor>
					</td>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img33"/></td>
					<td valign="top" width="100" align="left"></td>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img34"/></td>
				</tr>
				<tr>
					<td colspan="7"><img runat="server" src="~/images/spacer.gif" width="1" height="15" id="Img35"/></td>
				</tr>
				
				<!-- FAVORITE TV SHOWS -->
				<tr>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img71"/></td>
					<td class="accountText" align="right" valign="top" width="150">Favorite TV Shows:</td>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img72"/></td>		
					<td align="left" width="707">
						<ce:editor id="txtTV" showdecreasebutton="false" showenlargebutton="False" backcolor="#ededed" runat="server" maxtextlength="1000" width="100%" height="150px" configurationpath="~/CuteSoft_Client/CuteEditor/Configuration/AutoConfigure/profile.config" autoconfigure="None" ></ce:editor>
					</td>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img73"/></td>
					<td valign="top" width="100" align="left"></td>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img74"/></td>
				</tr>																																																																																																							
				<tr>
					<td colspan="7"><img runat="server" src="~/images/spacer.gif" width="1" height="15" id="Img75"/></td>
				</tr>
				
				<!-- FAVORITE MUSIC -->
				<tr>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img36"/></td>
					<td class="accountText" align="right" valign="top" width="150">Favorite Music:</td>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img37"/></td>		
					<td align="left" width="707">
						<ce:editor id="txtMusic" showdecreasebutton="false" showenlargebutton="False" backcolor="#ededed" runat="server" maxtextlength="1000" width="100%" height="150px" configurationpath="~/CuteSoft_Client/CuteEditor/Configuration/AutoConfigure/profile.config" autoconfigure="None" ></ce:editor>
					</td>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img38"/></td>
					<td valign="top" width="100" align="left"></td>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img39"/></td>
				</tr>
				<tr>
					<td colspan="7"><img runat="server" src="~/images/spacer.gif" width="1" height="15" id="Img40"/></td>
				</tr>
				
				<!-- FAVORITE SPORTS/TEAMS -->
				<tr>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img6"/></td>
					<td class="accountText" align="right" valign="top" width="150">Favorite Sports/Teams:</td>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img7"/></td>		
					<td align="left" width="707">
						<ce:editor id="txtSports" showdecreasebutton="false" showenlargebutton="False" backcolor="#ededed" runat="server" maxtextlength="1000" width="100%" height="150px" configurationpath="~/CuteSoft_Client/CuteEditor/Configuration/AutoConfigure/profile.config" autoconfigure="None" ></ce:editor>
					</td>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img8"/></td>
					<td valign="top" width="100" align="left"></td>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img9"/></td>
				</tr>
				<tr>
					<td colspan="7"><img runat="server" src="~/images/spacer.gif" width="1" height="15" id="Img10"/></td>
				</tr>																																									

				<!-- FAVORITE GAMES -->
				<tr>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img41"/></td>
					<td class="accountText" align="right" valign="top" width="150">Favorite Games:</td>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img42"/></td>		
					<td align="left" width="707">
						<ce:editor id="txtGames" showdecreasebutton="false" showenlargebutton="False" backcolor="#ededed" runat="server" maxtextlength="1000" width="100%" height="150px" configurationpath="~/CuteSoft_Client/CuteEditor/Configuration/AutoConfigure/profile.config" autoconfigure="None" ></ce:editor>
					</td>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img43"/></td>
					<td valign="top" width="100" align="left"></td>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img44"/></td>
				</tr>
				<tr>
					<td colspan="7"><img runat="server" src="~/images/spacer.gif" width="1" height="15" id="Img45"/></td>
				</tr>																																									
				
				<!-- FAVORITE BOOKS -->
				<tr>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img46"/></td>
					<td class="accountText" align="right" valign="top" width="150">Favorite Books:</td>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img47"/></td>		
					<td align="left" width="707">
						<ce:editor id="txtBooks" showdecreasebutton="false" showenlargebutton="False" runat="server" maxtextlength="1000" width="100%" height="150px" configurationpath="~/CuteSoft_Client/CuteEditor/Configuration/AutoConfigure/profile.config" autoconfigure="None" ></ce:editor>
					</td>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img48"/></td>
					<td valign="top" width="100" align="left"></td>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img49"/></td>
				</tr>
				<tr>
					<td colspan="7"><img runat="server" src="~/images/spacer.gif" width="1" height="15" id="Img50"/></td>
				</tr>			
				
				<!-- FAVORITE ARTISTS -->
				<tr>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img26"/></td>
					<td class="accountText" align="right" valign="top" width="150">Favorite Artists:</td>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img27"/></td>		
					<td align="left" width="707">
						<ce:editor id="txtArtists" showdecreasebutton="false" showenlargebutton="False" backcolor="#ededed" runat="server" maxtextlength="1000" width="100%" height="150px" configurationpath="~/CuteSoft_Client/CuteEditor/Configuration/AutoConfigure/profile.config" autoconfigure="None" ></ce:editor>
					</td>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img28"/></td>
					<td valign="top" width="100" align="left"></td>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img29"/></td>
				</tr>
				<tr>
					<td colspan="7"><img runat="server" src="~/images/spacer.gif" width="1" height="15" id="Img30"/></td>
				</tr>																																									

				<!-- FAVORITE CARS -->
				<tr>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img21"/></td>
					<td class="accountText" align="right" valign="top" width="150">Favorite Cars:</td>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img22"/></td>		
					<td align="left" width="707">
						<ce:editor id="txtCars" showdecreasebutton="false" showenlargebutton="False" backcolor="#ededed" runat="server" maxtextlength="1000" width="100%" height="150px" configurationpath="~/CuteSoft_Client/CuteEditor/Configuration/AutoConfigure/profile.config" autoconfigure="None" ></ce:editor>
					</td>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img23"/></td>
					<td valign="top" width="100" align="left"></td>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img24"/></td>
				</tr>
				<tr>
					<td colspan="7"><img runat="server" src="~/images/spacer.gif" width="1" height="15" id="Img25"/></td>
				</tr>																																									

				<!-- FAVORITE GADGETS -->
				<tr>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img11"/></td>
					<td class="accountText" align="right" valign="top" width="150">Favorite Gadgets:</td>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img12"/></td>		
					<td align="left" width="707">
						<ce:editor id="txtGadgets" showdecreasebutton="false" showenlargebutton="False" backcolor="#ededed" runat="server" maxtextlength="1000" width="100%" height="150px" configurationpath="~/CuteSoft_Client/CuteEditor/Configuration/AutoConfigure/profile.config" autoconfigure="None" ></ce:editor>
					</td>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img13"/></td>
					<td valign="top" width="100" align="left"></td>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img14"/></td>
				</tr>
				<tr>
					<td colspan="7"><img runat="server" src="~/images/spacer.gif" width="1" height="15" id="Img15"/></td>
				</tr>																																									

				<!-- FAVORITE FASHION -->
				<tr>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img16"/></td>
					<td class="accountText" align="right" valign="top" width="150">Favorite Fashion:</td>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img17"/></td>		
					<td align="left" width="707">
						<ce:editor id="txtFashion" showdecreasebutton="false" showenlargebutton="False" backcolor="#ededed" runat="server" maxtextlength="1000" width="100%" height="150px" configurationpath="~/CuteSoft_Client/CuteEditor/Configuration/AutoConfigure/profile.config" autoconfigure="None" ></ce:editor>
					</td>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img18"/></td>
					<td valign="top" width="100" align="left"></td>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img19"/></td>
				</tr>
				<tr>
					<td colspan="7"><img runat="server" src="~/images/spacer.gif" width="1" height="15" id="Img20"/></td>
				</tr>																																									

				<!-- FAVORITE BRANDS -->
				<tr>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img66"/></td>
					<td class="accountText" align="right" valign="top" width="150">Favorite Brands:</td>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img67"/></td>		
					<td align="left" width="707">
						<ce:editor id="txtBrands" showdecreasebutton="false" showenlargebutton="False" backcolor="#ededed" runat="server" maxtextlength="1000" width="100%" height="150px" configurationpath="~/CuteSoft_Client/CuteEditor/Configuration/AutoConfigure/profile.config" autoconfigure="None" ></ce:editor>
					</td>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img68"/></td>
					<td valign="top" width="100" align="left"></td>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img69"/></td>
				</tr>
				<tr>
					<td colspan="7"><img runat="server" src="~/images/spacer.gif" width="1" height="15" id="Img70"/></td>
				</tr>																																									

				<!-- MY ROLE MODEL(S) -->
				<tr>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img56"/></td>
					<td class="accountText" align="right" valign="top" width="150">My Role Model(s):</td>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img57"/></td>		
					<td align="left" width="707">
						<ce:editor id="txtRoleModel" showdecreasebutton="false" showenlargebutton="False" runat="server" maxtextlength="1000" width="100%" height="150px" configurationpath="~/CuteSoft_Client/CuteEditor/Configuration/AutoConfigure/profile.config" autoconfigure="None" ></ce:editor>
					</td>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img58"/></td>
					<td valign="top" width="100" align="left"></td>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img59"/></td>
				</tr>
				<tr>
					<td colspan="7"><img runat="server" src="~/images/spacer.gif" width="1" height="15" id="Img60"/></td>
				</tr>

				<!-- I'D LIKE TO MEET -->
				<tr>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" /></td>
					<td class="accountText" align="right" valign="top" width="150">I'd like to meet:</td>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" /></td>		
					<td align="left" width="707">
						<CE:Editor id="txtLikeToMeet" ShowDecreaseButton="false" ShowEnlargeButton="False" runat="server" MaxTextLength="1000" width="100%" Height="150px" ConfigurationPath="~/CuteSoft_Client/CuteEditor/Configuration/AutoConfigure/profile.config" AutoConfigure="None" ></CE:Editor>
					</td>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" /></td>
					<td valign="top" width="100" align="left"></td>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" /></td>
				</tr>
				<tr>
					<td colspan="7"><img runat="server" src="~/images/spacer.gif" width="1" height="15" /></td>
				</tr>
				
				<!-- FAVORITE QUOTES -->
				<tr>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img61"/></td>
					<td class="accountText" align="right" valign="top" width="150">Favorite Quotes:</td>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img62"/></td>		
					<td align="left" width="707">
						<ce:editor id="txtQuotes" showdecreasebutton="false" showenlargebutton="False" runat="server" maxtextlength="1000" width="100%" height="150px" configurationpath="~/CuteSoft_Client/CuteEditor/Configuration/AutoConfigure/profile.config" autoconfigure="None" ></ce:editor>
					</td>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img63"/></td>
					<td valign="top" width="100" align="left"></td>
					<td width="10"><img runat="server" src="~/images/spacer.gif" width="10" height="1" id="Img64"/></td>
				</tr>
				<tr>
					<td colspan="7"><img runat="server" src="~/images/spacer.gif" width="1" height="15" id="Img65"/></td>
				</tr>
			</table>
				
											
			<!-- end content-->	

		</td>
		<td class="boxInsideRight"><img runat="server" src="~/images/spacer.gif" width="1" height="4"/></td>
	</tr>
	<tr>
		<td class="boxInsideBottomLeft" ><img runat="server" src="~/images/spacer.gif" width="4" height="4" /></td>
		<td class="boxInsideBottom2"><img runat="server" src="~/images/spacer.gif" width="4" height="1"/></td>
		<td class="boxInsideBottomRight"><img runat="server" src="~/images/spacer.gif" width="4" height="4" /></td>
	</tr>
</table>		
