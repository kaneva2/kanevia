<%@ Control Language="c#" AutoEventWireup="false" Codebehind="KanevaToday.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.usercontrols.KanevaToday" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ OutputCache Duration="300" VaryByParam="none" %>


<table cellpadding="1" cellspacing="0" border="0">
	<tr>
		<td class="signText"><asp:label id="lblNumPersonalChannels" runat="server" /></td><td width="10"></td><td class="signTextBold">
		<a class="signTextLink" href="~/people/people.kaneva" runat="server" id="A1">Profiles</a></td>
	</tr>
	<tr>
		<td class="signText"><asp:label id="lblNumBroadcastChannels" runat="server" /></td><td width="10"></td><td class="signTextBold">
		<a class="signTextLink" href="~/community/channel.kaneva" runat="server" id="A2">Communities</a></td>
	</tr>
	<tr>
		<td class="signText"><asp:label id="lblNumMedia" runat="server" /></td><td width="10"></td><td class="signTextBold">
		<a class="signTextLink" href="~/watch/watch.kaneva" runat="server" id="A3">Media</a></td>
	</tr>																										
</table>
