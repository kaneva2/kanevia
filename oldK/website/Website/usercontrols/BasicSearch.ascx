<%@ Control Language="c#" AutoEventWireup="false" Codebehind="BasicSearch.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.usercontrols.BasicSearch" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>

<h2>Search Kaneva<span class="hlink"><asp:linkbutton runat="server" id="lbAdvSearch" onclick="AdvSearch_Click">advanced search</asp:linkbutton></span></h2>
<table cellpadding="0" cellspacing="0" border="0" width="99%">
	<tr>
		<td valign="top" class="searchbox">
			<table cellpadding="0" cellspacing="0" border="0" class="searchboxnav">
				<tr>
					<td colspan="2">
						<ul>
							<li style="width:86px;"><asp:linkbutton id="lbChannels" runat="server" href="javascript:void(0);">Communities</asp:linkbutton></li> 
							<li style="width:50px;"><asp:linkbutton id="lbPeople" runat="server" href="javascript:void(0);">People</asp:linkbutton></li> 
							<li style="width:50px;"><asp:linkbutton id="lbVideos" runat="server" href="javascript:void(0);">Videos</asp:linkbutton></li>
							<li style="width:48px;"><asp:linkbutton id="lbPhotos" runat="server" href="javascript:void(0);">Photos</asp:linkbutton></li> 
							<li style="width:40px;"><asp:linkbutton id="lbMusic" runat="server" href="javascript:void(0);">Music</asp:linkbutton></li>  
							<li style="width:48px;"><asp:linkbutton id="lbGames" runat="server" href="javascript:void(0);">Games</asp:linkbutton></li>
						</ul>
					</td>
				</tr>
				<tr>
					<td>
						<table cellpadding="5" cellspacing="0" border="0" class="selected">
							<tr>
								<td><input runat="server" name="txtKeywords" type="text" maxlength="40" id="txtKeywords" class="formKanevaText" style="width:250px" /></td>
								<td><asp:imagebutton id="imgSearch" runat="server" imageurl="~/images/btn_search.gif" alternatetext="Search" onclick="imgSearch_Click" width="52" height="22" border="0" /></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<br/>
			<asp:checkbox id="chkPhotoRequired" runat="server" checked />Must Contain Thumbnail Photo<br/><br/>
			<div id="divRestricted" runat="server">
			Show Restricted Content: <asp:label id="lblShowRestricted" runat="server">No</asp:label><br/>
			<asp:hyperlink id="hlChangeProfile" runat="server">Change Profile Setting</asp:hyperlink></div>
		</td>
		<td valign="top">
			<table cellpadding="7" cellspacing="0" border="0" width="230">
				<tr>
					<td valign="top">
						New Within:<br>
						<asp:radiobuttonlist id="rblNewWithin" runat="server">
							<asp:listitem value="1">Last 24 Hours</asp:listitem><asp:listitem value="7">Last 7 Days</asp:listitem>
							<asp:listitem value="30">Last 30 Days</asp:listitem>
							<asp:listitem value="0" selected>All Time</asp:listitem>
						</asp:radiobuttonlist>
					</td>
					<td valign="top">
						Sort by:<br>
						<asp:radiobuttonlist id="rblOrderBy" runat="server">
							<asp:listitem value="raves" selected>Most Raves</asp:listitem>
							<asp:listitem value="views">Most Views</asp:listitem>
							<asp:listitem value="newest">Newest</asp:listitem>
						</asp:radiobuttonlist>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<input type="hidden" id="hidSearchType" value="" runat="server" name="hidSearchType"/>

<script language="javascript">
	var lb = new Array();
	
	function Init()
	{
		for (var i=0; i<6; i++)	{ lb[i] = new Object; }
		
		lb[0] = document.getElementById("<%= lbPeople.ClientID %>");
		lb[1] = document.getElementById("<%= lbChannels.ClientID %>");
		lb[2] = document.getElementById("<%= lbVideos.ClientID %>");
		lb[3] = document.getElementById("<%= lbPhotos.ClientID %>");
		lb[4] = document.getElementById("<%= lbMusic.ClientID %>");
		lb[5] = document.getElementById("<%= lbGames.ClientID %>");
	}
	
	function setHilite(t)
	{
		if (!lb[0]) Init();
		
		for (var i=0; i<lb.length; i++)
		{
			lb[i].className = '';
		}
		
		t.className = 'selected';
		var val;
		switch(t.id)
		{
			case "<%= lbPeople.ClientID %>":
				val = -1;
				break;
			case "<%= lbChannels.ClientID %>":
				val = -2;
				break;
			case "<%= lbVideos.ClientID %>":
				val = 2;
				break
			case "<%= lbPhotos.ClientID %>":
				val = 5;
				break;
			case "<%= lbMusic.ClientID %>":
				val = 4;
				break;
			case "<%= lbGames.ClientID %>":
				val = 1;
				break;
		}
		
		document.getElementById("<%= hidSearchType.ClientID %>").value = val;
		
		return false;	
	}


</script>
													