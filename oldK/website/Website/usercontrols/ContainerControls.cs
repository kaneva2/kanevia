///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Kaneva.usercontrols
{
    public abstract class ContainerControls : System.Web.UI.UserControl
    {

        #region Declarations

        private string controlToLoad = "";
        private string ajaxDataPage = "";
        private string ajaxDataPageParams = "";
        private string editButtonLink = "";
        private string title = "";
        private bool hideHeader = false;

        #endregion

        #region Functions

        protected abstract void LoadControl();

        #endregion

        #region Attributes

        //common to all inheriting classes
        public string ControlToLoad
        {
            set
            {
                this.controlToLoad = value;
            }
            get
            {
                return this.controlToLoad;
            }
        }

        public string AjaxDataPage
        {
            set
            {
                this.ajaxDataPage = ResolveUrl("~" + value);
            }
            get
            {
                return this.ajaxDataPage;
            }
        }

        public string AjaxDataPageParams
        {
            set
            {
                this.ajaxDataPageParams = value;
            }
            get
            {
                return this.ajaxDataPageParams;
            }
        }

        public string EditButtonLink
        {
            set
            {
                this.editButtonLink = ResolveUrl ("~" + value);
            }
            get
            {
                return this.editButtonLink;
            }
        }

        public string Title
        {
            set
            {
                this.title = value;
            }
            get
            {
                return this.title;
            }
        }

        public bool HideHeader
        {
            set
            {
                this.hideHeader = value;
            }
            get
            {
                return this.hideHeader;
            }
        }
        
        #endregion

    }
}
