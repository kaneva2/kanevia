///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KlausEnt.KEP.Kaneva.usercontrols
{
    public partial class FacebookJavascriptInclude : BaseUserControl
    {
        protected void Page_Load (object sender, EventArgs e)
        {
            litFBJS.Text = "<script type=\"text/javascript\" src=\"" + ResolveUrl("~/jscript/facebook.js?v6") + "\"></script>";
        }

        public string GetFacebookAppId ()
        {
            return KanevaGlobals.FacebookAppId;
        }

        public string GetHostUrl ()
        {
            return (KanevaGlobals.SSLLogin ? "https://" : "http://") + KanevaGlobals.SiteName;
        }

        public string GetUrlParams()
        {
            return Request.Url.Query;
        }

        public string GetConnectOnLoad()
        {
            return (Request.Params["fbautoconnect"] != null && Request.Params["fbautoconnect"] == "true").ToString();
        }
    }
}