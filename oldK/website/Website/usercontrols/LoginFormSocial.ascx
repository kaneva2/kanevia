﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LoginFormSocial.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.usercontrols.LoginFormSocial" %>

<div class="signIn">
	<ul>
        <li class="inputField"><label>Email</label></li>            
        <li><input type="text" class="start" maxlength="100" id="txtUserName" runat="server" value="" TabIndex="1"  ></li>                                
    </ul>          
    <ul>    
        <li class="inputField"><label>Password</label></li>
		<li><input type="password" class="start" maxlength="30" id="txtPassword" runat="server" TabIndex="2"></li>
    </ul>          
    <ul class="btnHolder">    
        <li class="button">               
            <asp:Button id="loginSubmit" class="next" runat="server" TabIndex="5" text="  Next Step  " />
        </li>
	</ul>
	<p class="lostpw">Lost your Password? Click <a runat="server" id="aLostPassword" class="afb">here</a>.</p>     

</div>
	               
<asp:checkbox visible="false" runat="server" id="chkRememberLogin" tooltip="Remember Password" TabIndex="4" CssClass="chkRemember" text="Keep me signed in" />
            
 

