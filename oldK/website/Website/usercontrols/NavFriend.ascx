<%@ Control Language="c#" AutoEventWireup="false" Codebehind="NavFriend.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.NavFriend" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>


<div id="thirdaryNav">
	<ul>
		<li id="navGroups" runat="server"><a id="aGroups" runat="server">Friends Groups</a></li>
		<li id="navRequests" runat="server"><a id="aRequests" runat="server">Requests</a></li>
		<li id="navReqSent" runat="server"><a id="aReqSent" runat="server">Requests Sent</a></li>
		<li id="navInvite" runat="server"><a id="aInvite" runat="server">Invite A Friend</a></li>
		<li id="navManageInvites" runat="server"><a id="aManageInvites" runat="server">Manage Invites</a></li>
	</ul>        
</div>