///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System.Collections.Generic;

using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva.usercontrols
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	
	using KlausEnt.KEP.Kaneva;
	using MagicAjax;
	using log4net;

	/// <summary>
	///		Summary description for Comments.
	/// </summary>
	public class Comments : BaseUserControl
	{
		private void Page_Load(object sender, System.EventArgs e)
		{
			if (!IsPostBack)
			{
				if (AssetId > 0)
				{
					BindData(1);
				}
			}											 

            /*
            // Add the javascript for deleting comments
			string scriptString = "<script language=\"javaScript\">\n<!--\n function DeleteComment (id, owner_id) {\n";
			scriptString += "	$('" + hidCommentID.ClientID + "').value = id;\n";											// save the page number clicked to the hidden field
			scriptString += "	$('" + hidCommentOwnerID.ClientID + "').value = owner_id;\n";								// save the owner id of the comment to the hidden field
			scriptString += "	" + MagicAjax.AjaxCallHelper.GetAjaxCallEventReference(DeleteThread) + ";\n";				// call the __doPostBack function to post back the form and execute the PageClick event
			scriptString += "}\n// -->\n";
			scriptString += "</script>";											  

			if (!Page.ClientScript.IsClientScriptBlockRegistered (Page.GetType (), "DeleteComment"))
			{
                Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "DeleteComment", scriptString);
			}
*/
			litControlId.Text = this.ClientID.ToString ();

			litMaxLen.Text = KanevaGlobals.MaxCommentLength.ToString ();
		}


		#region Helper Methods
		/// <summary>
		/// Bind the data
		/// </summary>
		/// <param name="pageNumber"></param>
		private void BindData (int pageNumber)
		{
			if ( Request.IsAuthenticated )
			{
                if (!CurrentUserIsBlocked)
                {
                    lnkAddComment.InnerText = "Post A Comment";
                    lnkAddComment.Attributes["onclick"] = "ShowCommentBox(0, 0,this);return false;";
                    lnkAddCommentImg.Attributes["onclick"] = "javascript: $('" + lnkAddComment.ClientID + "').click(); return false;";
                }
                else
                {
                    lnkAddComment.Visible = false;
                    lnkAddCommentImg.Visible = false;
                }
			}										
			else
			{
				// if user not logged in
				lnkAddComment.InnerText = "Login To Post A Comment";
                // These need to point to the registration page, per Registration Landing pages project changes
                lnkAddComment.HRef = RegURL;
                lnkAddCommentImg.HRef = RegURL;
			}

			BindComments (pageNumber);
		}

        /// <summary>
        /// BindComments
        /// </summary>
		private void BindComments (int pageNumber)
		{
            CommentFacade commentFacade = new CommentFacade();
            int numberOfComments = 0;
            int numberOfCommentsShowing = 0;

            // Displaying a custom message based on the type
            DataRow drAsset = null;
            try
            {
                int assetId = Convert.ToInt32(Request["assetId"]);
                drAsset = StoreUtility.GetAsset(assetId, false, GetUserId());

                int assetTypeId = Convert.ToInt32(drAsset["asset_type_id"]);
                string assetType = KanevaWebGlobals.GetAssetTypeName(assetTypeId);
                nocomments.Text = "Be the first to leave a comment about this " + assetType.ToLower() + "!";

                numberOfComments = Convert.ToInt32 (drAsset ["number_of_comments"]);
            }
            catch (Exception ex)
            {
                m_logger.Error("Error loading comments control: ", ex);
                Response.Redirect("~/free-virtual-world.kaneva");
            }
			
			if (AssetId > 0)
			{                                    
				// if commenting on an asset
                IList<Comment> comments = commentFacade.GetAssetComments(AssetId, pageNumber, commentsPerPage);
                numberOfCommentsShowing = comments.Count;

                rptComments.DataSource = comments;
				rptComments.DataBind();

                pgTop.NumberOfPages = Math.Ceiling((double)numberOfComments / commentsPerPage).ToString();
				pgTop.DrawControl();
			}

			// The results
            if (numberOfComments == 0)
			{
				lblSearch.Visible = false;
				tblFooter.Visible = false;
				divCallOut.Visible = true;
			}
			else
			{
				lblSearch.Visible = true;
				tblFooter.Visible = true;
				divCallOut.Visible = false;
                lblSearch.Text = GetResultsText(numberOfComments, pageNumber, commentsPerPage, numberOfCommentsShowing);
			}
			
		}

		public bool CanUserDeleteComment ()
		{
			if ( this.IsUserAdministrator () || IsAssetOwner )
				return true;
			else
				return false;
		}

		/// <summary>
		/// Get the results text
		/// </summary>
		/// <returns></returns>
		public string GetResultsText (int TotalCount, int pageNumber, int resultsPerPage, int currentPageCount)
		{
			if ( TotalCount > 0 )
				return KanevaGlobals.GetResultsText (TotalCount, pageNumber, resultsPerPage, currentPageCount, true);
			else
				return "Be the first to leave a comment!";
		}

		/// <summary>
		/// GetThreadEditedText
		/// </summary>
		/// <param name="updatedUser"></param>
		/// <param name="lastUpdatedDate"></param>
		/// <returns></returns>
		protected string GetThreadEditedText (string updatedUser, object lastUpdatedDate, object currentDate)
		{
            if (!updatedUser.Length.Equals (0))
			{
				return "Edited by " + updatedUser.ToString () + " " + FormatDateTimeSpan (lastUpdatedDate, currentDate);
			}

			return "";
		}

		/// <summary>
		/// Return the delete javascript
		/// </summary>
		/// <returns></returns>
		public string GetDeleteCommentScript (int commentID, int commentOwnerID)
		{
			return "javascript:if (confirm('Are you sure you want to delete this comment????')){DeleteComment (" + commentID + ", " + commentOwnerID + ")};";
		}

		/// <summary>
		/// Return the edit javascript
		/// </summary>
		/// <returns></returns>
        public string GetEditCommentScript (int user_id, int commentID, string comment)
        {
            return "javascript:EditComment(" + user_id + "," + commentID + ",'" + KanevaGlobals.CleanJavascript (comment) + "');";
        }

		protected string GetReplyLinkScript (int comment_id, int parent_id)
		{
			if (Request.IsAuthenticated)
			{
				return "href=\"javascript:void(0);\" onclick=\"ShowCommentBox("+comment_id+","+parent_id+",this);return false;\"";
			}
			else
			{
                return "href=\"" + RegURL + "\"";
			}
		}

		protected int GetUserId()
		{
			return UserId;
		}
		
		protected string GetUserState (string user_status)
		{
			return KanevaWebGlobals.GetUserState (user_status);
		}

       public string ShowEditButton ()
        {
            if (IsUserAdministrator () || AssetOwnerId == GetUserId () || !CurrentUserIsBlocked)
            {
                return "inline";
            }
            return "none";
        }
        #endregion

		#region Event Handlers

		/// <summary>
		/// Delete a Comment
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void Delete_Comments (Object sender, EventArgs e)
		{
            CommentFacade commentFacade = new CommentFacade();
            Comment comment = commentFacade.GetComment(Convert.ToInt32(hidCommentID.Value));

            // Admin, asset owner, or comment owner
			if (IsUserAdministrator() || IsAssetOwner || comment.UserId == UserId)
			{
				if ( Convert.ToInt32(hidCommentID.Value) > 0)
				{
                    commentFacade.DeleteComment(comment);
					pgTop.CurrentPageNumber = 1;
					BindComments(1);
				}
			}
		}
		/// <summary>
		/// User's response to a comment
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void ReplyComment_Click (object sender, EventArgs e)
		{
			if (Request.IsAuthenticated)
			{
				// Check for any inject scripts
				if (KanevaWebGlobals.ContainsInjectScripts (hidCommentText.Value))
				{
					m_logger.Warn ("User " + KanevaWebGlobals.GetUserId () + " tried to script from IP " + Common.GetVisitorIPAddress());
					ShowErrorOnStartup ("Your input contains invalid scripting, please remove script code and try again.", true);
					return;
				}

                // Check to make sure user did not enter any "potty mouth" words
                if (KanevaWebGlobals.isTextRestricted (hidCommentText.Value, Constants.eRESTRICTION_TYPE.POTTY_MOUTH))
                {
                    AjaxCallHelper.WriteAlert (Constants.VALIDATION_REGEX_RESTRICTED_WORD_MESSAGE);
                    return;
                }

                if (hidCommentText.Value.Length == 0)
				{
					AjaxCallHelper.WriteAlert ("Please enter a comment.  You can not submit a blank comment.");
					return;
				}

				if (hidCommentText.Value.Length > KanevaGlobals.MaxCommentLength)		  
				{
					AjaxCallHelper.WriteAlert ("Your comment is too long.  Comments must be " + KanevaGlobals.MaxCommentLength.ToString () + " characters or less");
					return;
				}

				if (AssetId > 0)
				{
                    User user = KanevaWebGlobals.CurrentUser;

                    DataRow drAsset = StoreUtility.GetAsset(AssetId);

                    // Make sure the inserter is not blocked from this profile
                    UserFacade userFacade = new UserFacade();
                    if (userFacade.IsUserBlocked(Convert.ToInt32(drAsset["owner_id"]), user.UserId))
                    {
                        AjaxCallHelper.WriteAlert("You are currently blocked from adding comments to this item!");
                        return;
                    }

                    // Make sure the user has been into woK at least once
                    if (!KanevaWebGlobals.CurrentUser.HasWOKAccount)
                    {
                        AjaxCallHelper.WriteAlert("Please login to the Kaneva client to enable adding comments!");
                        return;
                    }

                    Comment comment = new Comment(0, user.UserId, AssetId, 0, Convert.ToInt32(hidParentCommentID.Value), Convert.ToInt32(hidCommentID.Value), Server.HtmlEncode(hidCommentText.Value), 0, Common.GetVisitorIPAddress(),
                        new DateTime(), new DateTime(), 0, (int)Constants.eFORUM_STATUS.ACTIVE);

                    CommentFacade commentFacade = new CommentFacade();
                    if (!commentFacade.InsertComment(comment).Equals(0))
                    {
                        AjaxCallHelper.WriteAlert("Error inserting comment.");
                        return;
                    }

					// Blast it
					//gets the users privacy settings for friend blast
					if(user.BlastPrivacyComments)
					{
                        BlastFacade blastFacade = new BlastFacade();
                        blastFacade.SendCommentBlast(UserId, user.Username, user.NameNoSpaces, AssetId, drAsset["name"].ToString());
					}
				}
			}

			hidCommentText.Value = "";
			pgTop.CurrentPageNumber = 1;
			BindComments(1);
		}
		/// <summary>
		/// updating to a comment
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void EditComments_Click (object sender, EventArgs e) 
		{
            CommentFacade commentFacade = new CommentFacade();

			if (Request.IsAuthenticated)
			{
				// Check for any inject scripts
				if (KanevaWebGlobals.ContainsInjectScripts (hidCommentText.Value))
				{
					m_logger.Warn ("User " + KanevaWebGlobals.GetUserId () + " tried to script from IP " + Common.GetVisitorIPAddress());
					ShowErrorOnStartup ("Your input contains invalid scripting, please remove script code and try again.", true);
					return;
				}

                // Check to make sure user did not enter any "potty mouth" words
                if (KanevaWebGlobals.isTextRestricted (hidCommentText.Value, Constants.eRESTRICTION_TYPE.POTTY_MOUTH))
                {
                    AjaxCallHelper.WriteAlert (Constants.VALIDATION_REGEX_RESTRICTED_WORD_MESSAGE);
                    return;
                }

				//if (txtComment.Value.Length == 0)
				if (hidCommentText.Value.Length == 0)
				{
					AjaxCallHelper.WriteAlert ("Please enter a comment.  You can not submit a blank comment.");
					return;
				}

				if (hidCommentText.Value.Length > KanevaGlobals.MaxCommentLength)		  
				{
					AjaxCallHelper.WriteAlert ("Your comment is too long.  Comments must be " + KanevaGlobals.MaxCommentLength.ToString () + " characters or less");
					return;
				}

				int user_id = Convert.ToInt32(hidUserID.Value);

				if (IsUserAdministrator() || user_id == KanevaWebGlobals.GetUserId ())
				{
					if (Convert.ToInt32(hidCommentID.Value) > 0)
					{
                        Comment comment = new Comment();
                        comment.CommentId = Convert.ToInt32(hidCommentID.Value);
                        comment.CommentText = Server.HtmlEncode(hidCommentText.Value);
                        comment.LastUpdatedUserId = user_id;

                        commentFacade.UpdateComment(comment);

						BindComments(pgTop.CurrentPageNumber);
					}
				}
			}
			else
			{
				BindComments(1);
			}
		}

		/// <summary>
		/// Page Change
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void pg_PageChange (object sender, PageChangeEventArgs e)
		{
			BindData(e.PageNumber);
		}

		#endregion

		#region Properties
		private int UserId
		{
			get
			{
				if (m_user_id == 0)
				{
					m_user_id = KanevaWebGlobals.GetUserId ();
				}

				return m_user_id;
			}
		}
        public string RegURL
        {
            get 
            {
                if (string.IsNullOrEmpty(m_reg_url))
                {
                    m_reg_url = GetLoginURL();
                }
                return m_reg_url; 
            }
            set { m_reg_url = value; }
        }
		public int AssetId
		{
			get 
			{ 
				if ( ViewState["AssetId"] != null )
					return (int)ViewState["AssetId"];
				else
					return -1;
			}
			set 
			{ 
				ViewState["AssetId"] = value;
			}
		}
        private bool CurrentUserIsBlocked
        {
            get
            {
                if (ViewState["IsBlocked"] == null)
                {
                    ViewState["IsBlocked"] = GetUserFacade.IsUserBlocked (AssetOwnerId, KanevaWebGlobals.CurrentUser.UserId);
                }
                return (bool) ViewState["IsBlocked"];
            }
        }
		public int AssetOwnerId
		{
			get 
			{
				return m_asset_owner_id;
			}
			set
			{
				m_asset_owner_id = value;
			}
		}

		/// <summary>
		/// Is the current session user the asset owner?
		/// </summary>
		/// <returns></returns>
		public bool IsAssetOwner 
		{
			get
			{
				if (!m_is_asset_owner.Equals (-1))
				{
					return m_is_asset_owner.Equals (1);
				}
				else
				{
					if (user_id == 0)
					{
						user_id = KanevaWebGlobals.GetUserId();
					}

					bool isOwner = StoreUtility.IsAssetOwner (user_id, AssetId);
					
					m_is_asset_owner = isOwner ? 1 : 0;
					return isOwner;
				}
			}
			set
			{
				m_is_asset_owner = Convert.ToInt32(value);
			}
		}
		
		#endregion

		#region Declerations
		protected	Kaneva.Pager	pgTop;

		protected	Repeater		rptComments;

		protected	LinkButton		lbEdit, lbDelete;

		protected	Button			DeleteThread;

		protected	Label			lblSearch, CaptchMessage;
		protected	Label			lblNumComments;
		protected	Label			nocomments;

		protected	Literal			litControlId;
		protected	Literal			litMaxLen;

		protected	HtmlTable		tblEdit, tblFooter;
		protected	HtmlAnchor		lnkAddComment, lnkAddCommentImg;
		protected	HtmlInputHidden hidCommentID, hidCommentOwnerID, hidCommentText;
		protected	HtmlInputHidden hidParentCommentID, hidReplyToCommentID;
		protected	HtmlInputHidden hidUserID;												// used for editing comment
		protected	HtmlTextArea	txtComment;												// used for editing a comment

		protected	HtmlContainerControl divCallOut, divView; 

		private		int		user_id = 0;

		private		int		m_user_id = 0;
		private		int		m_asset_owner_id = -1;
		private		int		m_is_asset_owner = -1;
		private		int		commentsPerPage = 10;
        private     string  m_reg_url = string.Empty;

		// Logger
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
			pgTop.PageChanged +=new PageChangeEventHandler (pg_PageChange);
		}
		#endregion
	}
}
