///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using Kaneva.BusinessLayer.Facade;


namespace KlausEnt.KEP.Kaneva
{
    using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

    public class Footer2 : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblVersionAuth.Text = lblVersion.Text = KanevaGlobals.Version();

            aHelpAuth.HRef = aHelp.HRef = Common.GetHelpURL(KanevaWebGlobals.CurrentUser.FirstName, KanevaWebGlobals.CurrentUser.LastName, KanevaWebGlobals.CurrentUser.Email);
            aHelpNonAuth.HRef = Common.GetHelpURL(KanevaWebGlobals.CurrentUser.FirstName, KanevaWebGlobals.CurrentUser.LastName, KanevaWebGlobals.CurrentUser.Email);

            if (Request.IsAuthenticated)
            {
                footerAuth.Visible = true;
                footerNotAuth.Visible = false;
                footerNotAuth_Original.Visible = false;

                if (UseBlockFooter)
                {
                    footerAuth.Attributes.Add("class", "footerAuth footerBlock");
                }
                else
                {
                    //                 footerAuth.Attributes.Add ("class", "footerAuth");
                }
            }
            else
            {
                footerAuth.Visible = false;
                if (UseOriginalLinks)
                {
                    footerNotAuth_Original.Visible = true;
                    footerNotAuth.Visible = false;
                }
                else
                {
                    footerNotAuth_Original.Visible = false;
                    footerNotAuth.Visible = true;
                }
            }
        }

        public bool UseOriginalLinks
        {
            set
            {
                m_Original = value;
            }
            get
            {
                return m_Original;
            }
        }

        public bool UseBlockFooter
        {
            set
            {
                m_UseBlock = value;
            }
            get
            {
                return m_UseBlock;
            }

        }

        protected override void OnPreRender (EventArgs e)
        {
            lblLoadTimeAuth.Text = lblLoadTime.Text;
        }

        private bool m_Original = false;
        private bool m_UseBlock = false;

        protected Label lblVersion, lblLoadTime;
        protected Label lblVersionAuth, lblLoadTimeAuth;
        protected HtmlContainerControl footerAuth, footerNotAuth, footerNotAuth_Original;
        protected HtmlAnchor aHelpAuth, aHelp, aHelpNonAuth; 


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion
    }
}