///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KlausEnt.KEP.Kaneva.usercontrols
{
    public partial class ColorBox : BaseUserControl
    {
        protected void Page_Load (object sender, EventArgs e)
        {
            litStyle.Text = "<link href=\"" + ResolveUrl ("~/jscript/colorbox/colorbox.css") + "\" rel=\"stylesheet\" type=\"text/css\" />";

            litScript.Text = "" +
                "<script type=\"text/javascript\" src=\"" + ResolveUrl("~/jscript/colorbox/jquery-1.7.1.min.js")  + "\"></script>" +
                "<script type=\"text/javascript\" src=\"" + ResolveUrl("~/jscript/colorbox/jquery.colorbox.js")  + "\"></script>";

        }
    }
}