///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva.usercontrols
{
    public partial class ContestWidget : BaseUserControl
    {
        private bool _MyContest = false;
        private bool _TopPrizes = false;
        private bool _HotContests = false;
        private bool _IncludeJS = false;

        protected void Page_Load (object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (MyContest)
                {
                    ShowMyContests ();
                }
                else if (TopPrizes)
                {
                    ShowTopPrizes ();
                }
                else if (HotContests)
                {
                    ShowHotContests ();
                }
            }

            if (IncludeJavascript)
            {
                if (!Page.ClientScript.IsClientScriptBlockRegistered (GetType (), "contestwidgetjs"))
                {
                    Page.ClientScript.RegisterClientScriptBlock (GetType (), "contestwidgetjs", "function ShowDetails(id){location.href='" + ResolveUrl("~/mykaneva/contests.aspx") + "?contestId='+id;}", true);
                }
            }
        }

        private void ShowMyContests ()
        {
            title.InnerText = "My Contests";

            // Get contests user has created
            PagedList<Contest> mycontests = GetContestsUserCreated (3);
            if (mycontests.Count < 3)
            {
                // Get contests user has entered
                PagedList<Contest> entered = GetContestsUserEntered (3);
                if (entered.Count > 0)
                {
                    foreach (Contest ce in entered)
                    {
                        bool match = false;
                        foreach (Contest c1 in mycontests)
                        {
                            if (ce.ContestId == c1.ContestId)
                            {
                                match = true;
                                break;
                            }
                        }

                        if (!match)
                        {
                            mycontests.Add (ce);
                        }
                    }
                }
            }

            if (mycontests.Count < 3)
            {
                // Get contests user is following   
                PagedList<Contest> following = GetContestsFollowing (3);
                if (following.Count > 0)
                {
                    foreach (Contest cf in following)
                    {
                        bool match = false;
                        foreach (Contest c1 in mycontests)
                        {
                            if (cf.ContestId == c1.ContestId)
                            {
                                match = true;
                                break;
                            }
                        }

                        if (!match && mycontests.Count < 3)
                        {
                            mycontests.Add (cf);
                        }
                    }
                }
            }

            if (mycontests.Count < 3)
            {
                // Get contests user has voted on   
                PagedList<Contest> voted = GetContestsVotedOn (3);
                if (voted.Count > 0)
                {
                    foreach (Contest cv in voted)
                    {
                        bool match = false;
                        foreach (Contest c1 in mycontests)
                        {
                            if (cv.ContestId == c1.ContestId)
                            {
                                match = true;
                                break;
                            }
                        }

                        if (!match && mycontests.Count < 3)
                        {
                            mycontests.Add (cv);
                        }
                    }
                }
            }
            
            if (mycontests.Count > 0)
            {
                rptWidgetContests.DataSource = mycontests;
                rptWidgetContests.DataBind ();
            }
            else
            {
                divNoData.InnerText = "";
                contest_widget.Visible = false;
            }
        }

        private void ShowTopPrizes ()
        {
            title.InnerText = "Top Prizes";
            string filter = "status = " + (int) eCONTEST_STATUS.Active + " AND archived = 0 AND end_date > NOW()";
            
            PagedList<Contest> contests = GetContestFacade.GetContests (0, "prize_amount DESC, end_date", filter, 1, 3);

            if (contests.TotalCount > 0)
            {
                rptWidgetContests.DataSource = contests;
                rptWidgetContests.DataBind ();
            }
            else
            {
                divNoData.InnerText = "";
                divNoData.Visible = true;
            }
        }

        private void ShowHotContests ()
        {
            title.InnerText = "Hot Contests";
            string filter = "status = " + (int) eCONTEST_STATUS.Active + " AND archived = 0 AND end_date > NOW()";
                                                                                          
            PagedList<Contest> contests = GetContestFacade.GetContests (0, "num_entries DESC, end_date ASC", filter, 1, 3);

            if (contests.TotalCount > 0)
            {
                rptWidgetContests.DataSource = contests;
                rptWidgetContests.DataBind ();
            }
            else
            {
                divNoData.InnerText = "";
                divNoData.Visible = true;
            }
        }

        private PagedList<Contest> GetContestsUserCreated (int pageSize)
        {
            string filter = "status > " + (int) eCONTEST_STATUS.Inactive + " AND archived = 0 AND end_date > NOW()";
            return GetContestFacade.GetContests (KanevaWebGlobals.CurrentUser.UserId, "", filter, 1, pageSize);
        }

        private PagedList<Contest> GetContestsUserEntered (int pageSize)
        {
            string filter = "status = " + (int) eCONTEST_STATUS.Active + " AND archived = 0 AND end_date > NOW()";
            return GetContestFacade.GetContestsEntered (KanevaWebGlobals.CurrentUser.UserId, "", filter, 1, pageSize);
        }

        private PagedList<Contest> GetContestsFollowing (int pageSize)
        {
            string filter = "status = " + (int) eCONTEST_STATUS.Active + " AND archived = 0 AND end_date > NOW()";
            return GetContestFacade.GetContestsFollowing (KanevaWebGlobals.CurrentUser.UserId, "", filter, 1, pageSize); 
        }

        private PagedList<Contest> GetContestsVotedOn (int pageSize)
        {
            string filter = "status = " + (int) eCONTEST_STATUS.Active + " AND archived = 0 AND end_date > NOW()";
            return GetContestFacade.GetContestsVotedOn (KanevaWebGlobals.CurrentUser.UserId, "", filter, 1, pageSize);
        }

        protected void rptWidgetContests_ItemDataBound (Object Sender, RepeaterItemEventArgs e)
        {
            if ((e.Item.ItemType == ListItemType.Item) || (e.Item.ItemType == ListItemType.AlternatingItem))
            {
                HtmlImage imgFollow = ((HtmlImage) e.Item.FindControl ("imgFollow"));

                Contest c = (Contest) e.Item.DataItem;

                // Time Remaining style
                string timeCSS = "remaining";
                if (c.TimeSpanRemaining.Days < 1)
                {
                    timeCSS += " alert";
                }
                ((HtmlGenericControl) e.Item.FindControl ("divTimeRemaining")).Attributes.Add ("class", timeCSS);

                if (GetContestFacade.IsUserFollowingContest (c.ContestId, KanevaWebGlobals.CurrentUser.UserId))
                {
                    imgFollow.Visible = true;
                }
            }
        }



        public bool MyContest
        {
            set { _MyContest = value; }
            get { return _MyContest; }
        }
        public bool TopPrizes
        {
            set { _TopPrizes = value; }
            get { return _TopPrizes; }
        }
        public bool HotContests
        {
            set { _HotContests = value; }
            get { return _HotContests; }
        }
        public bool IncludeJavascript
        {
            set { _IncludeJS = value; }
            get { return _IncludeJS; }
        }
    }
}