///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

namespace KlausEnt.KEP.Kaneva.usercontrols
{
	using System;
	using System.Collections;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

	/// <summary>
	///		Summary description for RelatedItemsView.
	/// </summary>
	public class RelatedItemsView : BaseUserControl
	{
		private void Page_Load(object sender, System.EventArgs e)
		{
			if (!IsPostBack)
			{
				BindData ();
			}
		}

		
		#region Helper Methods
		/// <summary>
		/// Bind the data
		/// </summary>
		/// <param name="pageNumber"></param>
		private void BindData ()
		{
			if (AssetId > 0)
			{
				lbMore.CssClass = "";
				lbRelated.CssClass = "";
				
				dlRelated.Visible = false;
				dlMore.Visible = false;
				
				PagedDataTable pdtRelated = StoreUtility.GetRelatedItems (this.AssetId, KanevaWebGlobals.CurrentUser.HasAccessPass, "", "", 1, this.ItemsPerPage);
				
				PagedDataTable pdtMore = StoreUtility.GetSharedAssetsInChannel (KanevaWebGlobals.CurrentUser.HasAccessPass,
					GetUserPersonalCommunityId(this.AssetOwnerId), this.AssetOwnerId, this.AssetTypeId,
					"a.permission=1", "", 1, ItemsPerPage);
				
				int count = 0;

				// display the related items 
				if (this.IsRelatedItemsSearch && pdtRelated.TotalCount > 0)
				{
					lbRelated.CssClass = "selected";

					dlRelated.DataSource = pdtRelated;
					dlRelated.DataBind ();
					dlRelated.Visible = true;

					// Show the results
					if (pdtRelated.TotalCount > ItemsPerPage)														  
					{
						count = ItemsPerPage;
						DataRow drAsset = StoreUtility.GetAsset (AssetId);
						
						aAllResults.InnerText = "See More " + this.AssetTypeName + "s";
						aAllResults.HRef = GetRelatedItemsMoreLink ( drAsset ["keywords"].ToString () );
						aAllResults.Visible = true;
					}
					else
					{
						count = pdtRelated.TotalCount;
					}
																												
					spnShowing.InnerHtml = "Showing 1 - " + count.ToString() + " of " + pdtRelated.TotalCount.ToString();
					
					if (pdtMore.TotalCount == 0)
					{
						liMore.Visible = false;
						lbRelated.Attributes["onclick"] = "javascript:return false;";
						lbRelated.Attributes["onmouseover"] = "javascript:this.style.cursor='default';";
					}
				}
				else if (pdtMore.TotalCount > 0)	// display the more items
				{
					lbMore.CssClass = "selected";

					dlMore.DataSource = pdtMore;
					dlMore.DataBind ();
					dlMore.Visible = true;

					// Show the results										  
					if (pdtMore.TotalCount > ItemsPerPage)														  
					{
						//FUTURE -- Add back when we get new search browse working so can view media for a specific user
						
						count = ItemsPerPage;
					//	DataRow drAsset = StoreUtility.GetAsset (AssetId);		 
						
					//	aAllResults.InnerText = "See This Users " + this.AssetTypeName + "s";
					//	aAllResults.HRef = GetRelatedItemsMoreLink ( drAsset ["keywords"].ToString () );

						aAllResults.Visible = false;
					}
					else
					{
						count = pdtMore.TotalCount;

						aAllResults.Visible = false;
					}
																												
					spnShowing.InnerHtml = "Showing 1 - " + count.ToString() + " of " + pdtMore.TotalCount.ToString();

					if (pdtRelated.TotalCount == 0)
					{
						liRelated.Visible = false;
						lbMore.Attributes["onclick"] = "javascript:return false;";
						lbMore.Attributes["onmouseover"] = "javascript:this.style.cursor='default';";
					}
				}
				else	// no related items or more items
				{
					divContainer.Visible = false;
				}
			}
		}

		/// <summary>
		/// Get the See More link for Related Items
		/// </summary>
		protected string GetRelatedItemsMoreLink (string strKeywords)
		{
			return ResolveUrl ("~/watch/watch.kaneva?type=" + AssetTypeId.ToString () + "&kwd=" + Server.UrlEncode (strKeywords.TrimEnd()) + "cool+new+stuff&ob=raves&wi=0&ph=True");
		}

		/// <summary>
		/// Get users personal channel id
		/// </summary>
		private int GetUserPersonalCommunityId (int userId)
		{
            return GetUserFacade.GetPersonalChannelId(userId);
		}
		#endregion

		#region Event Handlers
		/// <summary>
		/// Event fired when one of the tabs is clicked
		/// </summary>
		protected void RelatedTab_Changed (object sender, CommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "related":
					IsRelatedItemsSearch = true;
					break;
				case "more":
					IsRelatedItemsSearch = false;
					break;
			}

			BindData ();
		}
		#endregion

		#region Properties

		private string AssetTypeName
		{
			get { return KanevaWebGlobals.GetAssetTypeName(_asset_type_id); }
		}

		private bool IsRelatedItemsSearch
		{
			get { return _is_related_items; }
			set { _is_related_items = value; }
		}

		public int AssetTypeId
		{
			get { return _asset_type_id; }
			set { _asset_type_id = value; }
		}

		public int ItemsPerPage
		{
			get { return _items_per_page; }
			set { _items_per_page = value; }
		}

		public int AssetId
		{
			get { return _assetId; }
			set { _assetId = value; }
		}

		public int AssetOwnerId
		{
			get { return _asset_owner_id; }
			set { _asset_owner_id = value; }
		}

		#endregion

		#region Declerations
		
		private int		_items_per_page = 5;
		private int		_assetId;
		private int		_asset_type_id = 0;
		private int		_asset_owner_id = 0;
		private bool	_is_related_items = true;
		
		protected	HtmlAnchor				aAllResults, aMore, aRelated;
		protected	HtmlContainerControl	spnTitle, spnShowing;
		protected	HtmlContainerControl	divContainer;
		protected	HtmlGenericControl		liRelated, liMore;
		protected	DataList				dlRelated, dlMore;
		protected	LinkButton				lbRelated, lbMore;
		
		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	
	
	}
}
