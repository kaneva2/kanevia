///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

namespace KlausEnt.KEP.Kaneva.usercontrols
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

	/// <summary>
	///		Summary description for TagCloud.
	/// </summary>
	public class TagCloud : BaseUserControl
	{
		private void Page_Load(object sender, System.EventArgs e)
		{
			if (!IsPostBack)
			{
				BindData ();
			}
		}
		
		
		#region Helper Methods
		/// <summary>
		/// BindData
		/// </summary>
		protected void BindData ()
		{
			string strKeywords = "";
			string fontSize = FontSizeSmallest;	
			string search_type;
			
			// Get the correct list of tags
			DataTable dtTags = StoreUtility.GetPopularTags (m_type_id, 1, DisplayCount);

			DataColumn column = new DataColumn(); 
			column.DataType = System.Type.GetType("System.String"); 
			column.AllowDBNull = false; 
			column.ColumnName = "font_size"; 
			column.DefaultValue = fontSize;
 
			// Add column to hold the font size
			dtTags.Columns.Add(column);

			// get the appropriate font-size for each tag
			if (dtTags.Rows.Count > 0)
			{
				for (int i = 0; i < dtTags.Rows.Count; i ++)
				{
					dtTags.Rows[i]["font_size"] = GetFontStyle(i/CountPerFontSize);
				}
			}
			
			// Put list in Dataview in alphabetical order
			DataView dvTags = new DataView (dtTags, "", "keyword", DataViewRowState.CurrentRows);
			
			// Display the tag list
			for (int i = 0; i < dvTags.Count; i ++)
			{
				fontSize = dvTags [i]["font_size"].ToString ();
					
				search_type = (CloudType == -1 ? "-2" : dvTags [i]["asset_type_id"].ToString ());

				if (!Request.IsAuthenticated)
				{
					if (dvTags [i]["keyword"].ToString () == "adults" || dvTags [i]["keyword"].ToString () == "adult" || dvTags [i]["keyword"].ToString () == "mature")
					{
                        strKeywords += "&nbsp;<a href=\"" + GetLoginURL() + "\"" + fontSize + ">" + dvTags[i]["keyword"].ToString() + "</a>&nbsp; ";
					}
					else
					{
						strKeywords += "&nbsp;<a href=\"" + ResolveUrl ("~/"+TargetPage+"?" + 
							Constants.QUERY_STRING_KEYWORD+"=" + Server.UrlEncode(dvTags [i]["keyword"].ToString ())) + 
							"&" + Constants.QUERY_STRING_SEARCH_TYPE + "=" + search_type + "\"" + 
							fontSize + " target=\"_parent\">" + dvTags [i]["keyword"].ToString () + "</a>&nbsp; ";
					}
				}
				else
				{
                    if (!KanevaWebGlobals.CurrentUser.HasAccessPass)
					{
						if (dvTags [i]["keyword"].ToString () == "adults" || dvTags [i]["keyword"].ToString () == "adult" || dvTags [i]["keyword"].ToString () == "mature")
						{																														
							strKeywords += "&nbsp;<a href=\"" + ResolveUrl ("javascript: alert('Viewing restricted content is disabled in your community.');") + "\"" + fontSize + ">" + dvTags [i]["keyword"].ToString () + "</a>&nbsp; ";
						}
						else
						{
							strKeywords += "&nbsp;<a href=\"" + ResolveUrl ("~/"+TargetPage+"?" + 
								Constants.QUERY_STRING_KEYWORD + "=" + Server.UrlEncode(dvTags [i]["keyword"].ToString ())) + 
								"&" + Constants.QUERY_STRING_SEARCH_TYPE + "=" + search_type + "\"" + 
								fontSize + " target=\"_parent\">" + dvTags [i]["keyword"].ToString () + "</a>&nbsp; ";
						}
					}
					else
					{
						strKeywords += "&nbsp;<a href=\"" + ResolveUrl ("~/"+TargetPage+"?" + 
							Constants.QUERY_STRING_KEYWORD + "=" + Server.UrlEncode(dvTags [i]["keyword"].ToString ())) + 
							"&" + Constants.QUERY_STRING_SEARCH_TYPE + "=" + search_type + "\"" + 
							fontSize + " target=\"_parent\">" + dvTags [i]["keyword"].ToString () + "</a>&nbsp; ";
					}
				}
			}

			divTags.InnerHtml = strKeywords;

			if (CSSFileName != string.Empty)
			{
				divTags.Attributes.Add("class", CSSFileName);
			}
		}
		/// <summary>
		/// GetTagFontSize
		/// </summary>
		/// <param name="tagCount"></param>
		/// <returns></returns>
		protected string GetFontStyle (int indx)
		{
			string font = string.Empty;
			bool isClass = false;

			if (indx == 0) //largest count
			{
				if (!FontLargestClassName.Equals(string.Empty))
				{
					font = FontLargestClassName;
					isClass = true;
				}
				else
				{
					font = FontSizeLargest;
				}
			}

			else if (indx == 1)
			{
				if (!FontLargeClassName.Equals(string.Empty))
				{
					font = FontLargeClassName;
					isClass = true;
				}
				else
				{
					font = FontSizeLarge;
				}
			}

			else if (indx == 2)
			{
				if (!FontMediumClassName.Equals(string.Empty))
				{
					font = FontMediumClassName;
					isClass = true;
				}
				else
				{
					font = FontSizeMedium;
				}
			}

			else if (indx == 3)
			{
				if (!FontSmallClassName.Equals(string.Empty))
				{
					font = FontSmallClassName;
					isClass = true;
				}
				else
				{
					return FontSizeSmall;
				}
			}

			else 
			{
				if (!FontSmallestClassName.Equals(string.Empty))
				{
					font = FontSmallestClassName;
					isClass = true;
				}
				else
				{
					return FontSizeSmallest;
				}
			}

			if (isClass)
				font = " class=\"" +font + "\"";
			else
				font = " style=\"font-size:" + font + "px;\"";

			return font;
		}

		
		public void BindCloud ()
		{
			BindData ();
		}
		#endregion

		#region Properties

		#region Font Sizes
		public string FontSizeSmallest
		{
			get { return m_font_smallest; }
			set { m_font_smallest = value; }
		}
		public string FontSizeSmall
		{
			get { return m_font_small; }
			set { m_font_small = value; }
		}
		public string FontSizeMedium
		{
			get { return m_font_medium; }
			set { m_font_medium = value; }
		}
		public string FontSizeLarge
		{
			get { return m_font_large; }
			set { m_font_large = value; }
		}
		public string FontSizeLargest
		{
			get { return m_font_largest; }
			set { m_font_largest = value; }
		}
		# endregion
		
		#region Font Styles
		public string FontSmallestClassName
		{
			get { return m_font_smallest_class; }
			set { m_font_smallest_class = value; }
		}
		public string FontSmallClassName
		{
			get { return m_font_small_class; }
			set { m_font_small_class = value; }
		}
		public string FontMediumClassName
		{
			get { return m_font_medium_class; }
			set { m_font_medium_class = value; }
		}
		public string FontLargeClassName
		{
			get { return m_font_large_class; }
			set { m_font_large_class = value; }
		}
		public string FontLargestClassName
		{
			get { return m_font_largest_class; }
			set { m_font_largest_class = value; }
		}
		# endregion

		// CSS File
		public string CSSFileName
		{
			get { return m_css_name; }
			set { m_css_name = value; }
		}

		public int CloudType 
		{
			get { return m_type_id; }
			set 
			{ 
				switch (Convert.ToInt32(value))
				{
					case (int)Constants.eCLOUD_TYPE.COMMUNITY:
						m_type_id = (int)Constants.eCLOUD_TYPE.COMMUNITY;
						break;

					case (int)Constants.eCLOUD_TYPE.VIDEO:
						m_type_id = (int)Constants.eCLOUD_TYPE.VIDEO;
						break;

					case (int)Constants.eCLOUD_TYPE.PHOTO:
						m_type_id = (int)Constants.eCLOUD_TYPE.PHOTO;
						break;

					case (int)Constants.eCLOUD_TYPE.MUSIC:
						m_type_id = (int)Constants.eCLOUD_TYPE.MUSIC;
						break;

					case (int)Constants.eCLOUD_TYPE.GAME:
						m_type_id = (int)Constants.eCLOUD_TYPE.GAME;
						break;
					
					case (int)Constants.eCLOUD_TYPE.PATTERN:
						m_type_id = (int)Constants.eCLOUD_TYPE.PATTERN;
						break;
				}

				m_type_id = value; 
			}
		}
		
		public string TargetPage
		{
			get 
			{ 
				if (m_target_page == string.Empty)
				{
					switch (m_type_id)
					{
						case (int)Constants.eCLOUD_TYPE.COMMUNITY:
							m_target_page = "community/commdrilldown.aspx";
							break;

						case (int)Constants.eCLOUD_TYPE.VIDEO:
						case (int)Constants.eCLOUD_TYPE.PHOTO:
						case (int)Constants.eCLOUD_TYPE.MUSIC:
						case (int)Constants.eCLOUD_TYPE.GAME:
						case (int)Constants.eCLOUD_TYPE.PATTERN:
							m_target_page = "watch/media.aspx";
							break;
					}
				}

				return m_target_page; 
			}
			set { m_target_page = value.Trim(); }
		}
		
		public int DisplayCount
		{
			get { return m_display_count; }
			set { m_display_count = value; }
		}
		
		public int CountPerFontSize
		{
			get { return m_count_per_size; }
			set { m_count_per_size = value; }
		}
		
		#endregion

		#region Declerations

		protected HtmlContainerControl divTags;
		
		private int m_display_count = 25;
		private int m_count_per_size = 5;
		private int m_type_id = 0;

		private string m_target_page = string.Empty;
		
		private string m_css_name = string.Empty;
		
		private string m_font_largest  = "22";
		private string m_font_large	   = "20";
		private string m_font_medium   = "18";
		private string m_font_small    = "15";
		private string m_font_smallest = "12";

		private string m_font_largest_class  = string.Empty;
		private string m_font_large_class	 = string.Empty;
		private string m_font_medium_class   = string.Empty;
		private string m_font_small_class    = string.Empty;
		private string m_font_smallest_class = string.Empty;

		#endregion


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
