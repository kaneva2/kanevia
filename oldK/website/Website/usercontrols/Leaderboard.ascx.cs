///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

namespace KlausEnt.KEP.Kaneva.usercontrols
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

	/// <summary>
	///		Summary description for Leaderboard.
	/// </summary>
	public class Leaderboard : BaseUserControl
	{
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			if (!IsPostBack)
			{
				BindData (1);
			}
		}

		/// <summary>
		/// BindData
		/// </summary>
		private void BindData (int pageNumber)
		{
			lbl_Noresults.Visible = false;
			
			PagedDataTable pdtTopPerformers = null;
			PagedDataTable currentPerformer = null;

			int userId = KanevaWebGlobals.GetUserId();

			try
			{
				try
				{
					//chose appropriate query and execute it
					switch(CompareBy)
					{
						case (int) Constants.eLEADER_BOARD_QUERIES.MEDIA_IN_CHANNEL:
							//								pdtTopPerformers = StoreUtility.GetLeaderBoard_MediaNChannel( this.ChannelId, NumberOfResults, startDate, endDate);
							//								if(channelId != null)
							//								{
							//								}
							break;
						
						case (int) Constants.eLEADER_BOARD_QUERIES.USERS_IN_CHANNEL:
							//sets URL type
							URLType = (int)Constants.eURL_TYPE.PERSONAL;

							pdtTopPerformers = StoreUtility.GetLeaderBoard_UsersNChannel (ChannelId, NumberOfResults, TimePeriodInDays, null, 1);
							if (userId != 0)				
							{
								currentPerformer = StoreUtility.GetLeaderBoard_UsersNChannel (ChannelId, NumberOfResults, TimePeriodInDays, userId.ToString(), 1);
								this.dgd_UserInfo.Visible = true;
							}
							break;
						
						case (int) Constants.eLEADER_BOARD_QUERIES.CHANNEL_TO_CHANNEL:
							//sets URL type
							URLType = (int)Constants.eURL_TYPE.BROADBAND;

							pdtTopPerformers = StoreUtility.GetLeaderBoard_Channel2Channel (NumberOfResults, TimePeriodInDays, null, 1);
							break;
						
						case (int) Constants.eLEADER_BOARD_QUERIES.USERS_TO_USERS:
						default:
							//sets URL type
							URLType =(int)Constants.eURL_TYPE.PERSONAL;

							pdtTopPerformers = WebCache.GetLeaderBoard_User2User (NumberOfResults, TimePeriodInDays);

							if (userId != 0)
							{
								currentPerformer = StoreUtility.GetLeaderBoard_User2User (NumberOfResults, TimePeriodInDays, userId.ToString(), 1);
							}
							break;
					}
				}
				catch(Exception ex)
				{
					lbl_Error.Text = ex.Message;
				}

				if ( (pdtTopPerformers != null) && (pdtTopPerformers.Rows.Count > 0) )
				{
					///Note: these column names should not be hard coded throughout the code. they 
					///should be centralized as a constant in a database class. Current structure and
					///time constraints do not allow for this right now - Alonzo 11-1-2006

					//bind the results to the datagrid and configure
					PagedDataSource pdsTopPerformers = new PagedDataSource();
					pdsTopPerformers.DataSource = pdtTopPerformers.DefaultView;
					pdsTopPerformers.AllowPaging = true;

					// Set the number of items you wish to display per page
					pdsTopPerformers.PageSize = ResultsPerPage;							
					pdsTopPerformers.CurrentPageIndex = pageNumber - 1;

					dgd_leaders.DataSource = pdsTopPerformers;
					dgd_leaders.DataBind();

					dgd_leaders.Columns[1].Visible = ShowUserImage;
					dgd_leaders.Columns[2].Visible = ShowUserName;
					dgd_leaders.Columns[3].Visible = ShowPimpProfile;
					dgd_leaders.Columns[4].Visible = ShowPimpPage;
					dgd_leaders.Columns[5].Visible = ShowRavesYourProfile;
					dgd_leaders.Columns[6].Visible = ShowPeopleInvited;
					dgd_leaders.Columns[7].Visible = ShowFriendAccepts;
					dgd_leaders.Columns[8].Visible = ShowJoinWOK;
					dgd_leaders.Columns[9].Visible = ShowChannelsCreated;
					dgd_leaders.Columns[10].Visible = ShowChannelMembers;
					dgd_leaders.Columns[11].Visible = ShowRavesChannel;
					dgd_leaders.Columns[12].Visible = ShowPhotosUploaded;
					dgd_leaders.Columns[13].Visible = ShowVideosUploaded;
					dgd_leaders.Columns[14].Visible = ShowMusicUploaded;
					dgd_leaders.Columns[15].Visible = ShowRavesMedia;
					dgd_leaders.Columns[16].Visible = ShowUserPoints;

					//this.pgTop.Visible = true;
					this.pgBottom.Visible = true;
						
					//pgTop.NumberOfPages = Math.Ceiling ((double) pdsTopPerformers.DataSourceCount / ResultsPerPage).ToString ();
					//pgTop.DrawControl ();

					pgBottom.NumberOfPages = Math.Ceiling ((double) pdsTopPerformers.DataSourceCount / ResultsPerPage).ToString ();
					pgBottom.DrawControl ();

					if(Convert.ToInt32(pgBottom.NumberOfPages) < 2)
					{
						this.pager2.Visible = false;
					}

					//if user logged in bind user info and display
					if ((userId != 0) && (currentPerformer.Rows.Count > 0))
					{
						this.dgd_UserInfo.Visible = true;
						trMyRow.Visible = true;

						this.dgd_UserInfo.DataSource = currentPerformer; 
						this.dgd_UserInfo.DataBind();

						dgd_UserInfo.Columns[1].Visible = ShowUserImage;
						dgd_UserInfo.Columns[2].Visible = ShowUserName;
						dgd_UserInfo.Columns[3].Visible = ShowPimpProfile;
						dgd_UserInfo.Columns[4].Visible = ShowPimpPage;
						dgd_UserInfo.Columns[5].Visible = ShowRavesYourProfile;
						dgd_UserInfo.Columns[6].Visible = ShowPeopleInvited;
						dgd_UserInfo.Columns[7].Visible = ShowFriendAccepts;
						dgd_UserInfo.Columns[8].Visible = ShowJoinWOK;
						dgd_UserInfo.Columns[9].Visible = ShowChannelsCreated;
						dgd_UserInfo.Columns[10].Visible = ShowChannelMembers;
						dgd_UserInfo.Columns[11].Visible = ShowRavesChannel;
						dgd_UserInfo.Columns[12].Visible = ShowPhotosUploaded;
						dgd_UserInfo.Columns[13].Visible = ShowVideosUploaded;
						dgd_UserInfo.Columns[14].Visible = ShowMusicUploaded;
						dgd_UserInfo.Columns[15].Visible = ShowRavesMedia;
						dgd_UserInfo.Columns[16].Visible = ShowUserPoints;
					}
				}
				else
				{
					lbl_Noresults.Visible = true;
				}
			}
			catch (Exception ex)
			{
				lbl_Error.Text = ex.Message;
			}
		}

		#region Helper Methods
		/// <summary>
		/// FormatDateTimeSpan
		/// </summary>
		/// <returns></returns>
		protected string FormatDateTimeSpan (Object dtDate)
		{
			return KanevaGlobals.FormatDateTimeSpan(dtDate);
		}

		protected int URLType
		{
			set
			{
				_URLType = value;
			}
			get
			{
				return _URLType;
			}
		}

		#endregion
		
		#region Event Handlers
		private void dgd_leaders_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			int userId = KanevaWebGlobals.GetUserId();
			if (userId != 0)
			{
				if (e.Item.Cells[17].Text == userId.ToString())
				{
					if (HilitedRowClass != string.Empty)
					{
						e.Item.CssClass = HilitedRowClass;
					}
					else
					{
						e.Item.BackColor = Color.Yellow;
						e.Item.ForeColor = Color.Black;
					}
				}
			}
		}


		/// <summary>
		/// Page Change
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void pager_PageChange (object sender, PageChangeEventArgs e)
		{
			//pgTop.CurrentPageNumber = e.PageNumber;
			//pgTop.DrawControl ();
			pgBottom.CurrentPageNumber = e.PageNumber;
			pgBottom.DrawControl ();

			BindData (e.PageNumber);
			//InitThickBox ();
		}

		#endregion
		
		#region Properties
		public bool ShowUserImage
		{
			get { return _show_user_image; }
			set { _show_user_image = value; }
		}
		public bool ShowUserName
		{
			get { return _show_user_name; }
			set { _show_user_name = value; }
		}
		public bool ShowPimpProfile
		{
			get { return _show_pimp_profile; }
			set { _show_pimp_profile = value; }
		}
		public bool ShowPimpPage
		{
			get { return _show_pimp_page; }
			set { _show_pimp_page = value; }
		}
		public bool ShowRavesYourProfile
		{
			get { return _show_raves_your_profile; }
			set { _show_raves_your_profile = value; }
		}
		public bool ShowPeopleInvited
		{
			get { return _show_people_invited; }
			set { _show_people_invited = value; }
		}
		public bool ShowFriendAccepts
		{
			get { return _show_friend_accepts; }
			set { _show_friend_accepts = value; }
		}
		public bool ShowJoinWOK
		{
			get { return _show_join_wok; }
			set { _show_join_wok = value; }
		}
		public bool ShowChannelsCreated
		{
			get { return _show_channels_created; }
			set { _show_channels_created = value; }
		}
		public bool ShowChannelMembers
		{
			get { return _show_channel_members; }
			set { _show_channel_members = value; }
		}
		public bool ShowRavesChannel
		{
			get { return _show_raves_channel; }
			set { _show_raves_channel = value; }
		}
		public bool ShowPhotosUploaded
		{
			get { return _show_photos_uploaded; }
			set { _show_photos_uploaded = value; }
		}
		public bool ShowMusicUploaded
		{
			get { return _show_music_uploaded; }
			set { _show_music_uploaded = value; }
		}
		public bool ShowVideosUploaded
		{
			get { return _show_video_uploaded; }
			set { _show_video_uploaded = value; }
		}
		public bool ShowRavesMedia
		{
			get { return _show_raves_media; }
			set { _show_raves_media = value; }
		}
		public bool ShowUserPoints
		{
			get { return _show_user_points; }
			set { _show_user_points = value; }
		}
		public int NumberOfResults
		{
			get { return _num_results; }
			set { _num_results = value; }
		}

		public int ResultsPerPage
		{
			get { return _leadersPerPage; }
			set { _leadersPerPage = value; }
		}

		public int TimePeriodInDays
		{
			get { return _period_in_days; }
			set { _period_in_days = value; }
		}
		public int ChannelId
		{
			get { return _channel_id; }
			set { _channel_id = value; }
		}
		public int CompareBy
		{
			get { return _compare_by; }
			set 
			{ 
				switch (value)
				{
					case (int) Constants.eLEADER_BOARD_QUERIES.CHANNEL_TO_CHANNEL:
					case (int) Constants.eLEADER_BOARD_QUERIES.MEDIA_IN_CHANNEL:
					case (int) Constants.eLEADER_BOARD_QUERIES.USERS_IN_CHANNEL:
						_compare_by = value;
						break;
					case (int) Constants.eLEADER_BOARD_QUERIES.USERS_TO_USERS:
					default:
						_compare_by = (int) Constants.eLEADER_BOARD_QUERIES.USERS_TO_USERS;
						break;
				}
			}
		}

		public string HilitedRowClass
		{
			get { return _row_css; }
			set { _row_css = value; }
		}
		#endregion

		#region Declerations
		protected	HtmlTable pager2, pager1;
		private bool _show_user_name = false;
		private bool _show_user_image = false;
		private bool _show_pimp_profile = false;
		private bool _show_pimp_page = false;
		private bool _show_raves_your_profile = false;
		private bool _show_people_invited = false;
		private bool _show_friend_accepts = false;
		private bool _show_join_wok = false;
		private bool _show_channels_created = false;
		private bool _show_channel_members = false;
		private bool _show_raves_channel = false;
		private bool _show_photos_uploaded = false;
		private bool _show_video_uploaded = false;
		private bool _show_music_uploaded = false;
		private bool _show_raves_media = false;
		private bool _show_user_points = false;
		private int  _num_results = 10;
		private int	 _compare_by = -1;
		private int  _period_in_days = 10;
		private int  _channel_id = 0;
		private int _leadersPerPage = 10; 

		private string _row_css = string.Empty;
		protected Kaneva.Pager	pgTop, pgBottom;

		private int _URLType;
		
		protected Label			lbl_Error;
		protected Label			lbl_Noresults;
		protected DataGrid		dgd_leaders, dgd_UserInfo;

		protected HtmlTableRow	trMyRow;

		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
			this.dgd_leaders.ItemDataBound += new DataGridItemEventHandler(dgd_leaders_ItemDataBound);
			//this.pgTop.PageChanged +=new PageChangeEventHandler (pager_PageChange);
			this.pgBottom.PageChanged +=new PageChangeEventHandler (pager_PageChange);
		}
		#endregion
	}
}
