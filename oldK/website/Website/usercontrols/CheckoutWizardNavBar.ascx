<%@ Control Language="c#" AutoEventWireup="false" Codebehind="CheckoutWizardNavBar.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.CheckoutWizardNavBar" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>

<div id="wizard">
<!-- WIDTH OF ALL SPANS SHOULD ADD UP TO 958 PX -->
	<ul id="wizardProgress">
		<li id="startcap"></li>
		<li id="liOne" runat="server"><span id="one" style="width: 244px;"><a runat="server" id="aOne" class="nohover"><h3>Choose Option</h3></span></a></li>
		<li id="liTwo" runat="server"><span id="two" style="width: 239px;"><a runat="server" id="aTwo" class="nohover"><h3>Select Address</h3></span></a></li>
		<li id="liThree" runat="server"><span id="three" style="width: 238px;"><a runat="server" id="aThree" class="nohover"><h3>Make Purchase</h3></a></span></li>
		<li id="liFour" runat="server"><span id="four" class="last" style="width: 237px;"><a runat="server" id="aFour" class="nohover"><h3>View Receipt</h3></a></span></li>
	</ul>			
</div>