///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using Improsys.ContactImporter;
using System.Text.RegularExpressions;

using log4net;

namespace KlausEnt.KEP.Kaneva.usercontrols
{
    public partial class ConnectWithFriends : BaseUserControl
    {
        protected void Page_Load (object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    txtEmail.Focus ();
                    
                    if (Request["email"] != null && Request["email"].ToString ().Length > 0)
                    {
                        CheckEmailDomain (Server.UrlDecode(Request["email"].ToString ()).ToLower ());
                    }
                }
                catch { }

                // Set up regular expression validators
                revEmail.ValidationExpression = Constants.VALIDATION_REGEX_EMAIL;

                // Set client id values for javascript
                divStep1_ClientId = divStep1.ClientID;
                divStep2_ClientId = divStep2.ClientID;
                txtEmail_ClientId = txtEmail.ClientID;

                divEnterEmails.Visible = (GetCommunityId() > 0);

				Response.Cookies["inviteCompleted"].Value = "false";
				Response.Cookies["inviteCompleted"].Expires = DateTime.Now.AddDays(60);
            }
        }


        #region Helper Methods

        /// <summary>
        /// Get email domain from email string
        /// </summary>
        /// <returns></returns>
        private void CheckEmailDomain (string email)
        {
            // Parse just the domain
            // First array itme is username, second is domain
            char[] delimiter = { '@' };
            string[] arrEmail = email.Split (delimiter);

            if (IsValidEmailDomain (arrEmail[1]))
            {
                string btnName = "";
                txtEmail.Text = email;
                txtPW.Focus ();
            
                switch (arrEmail[1])
                {
                    case "gmail.com":
                        btnName = "gmail";
                        break;
                    case "yahoo.com":
                        btnName = "yahoo";
                        break;
                    case "hotmail.com":
                        btnName = "hotmail";
                        break;
                    case "aol.com":
                        btnName = "aol";
                        break;
                    default:
                        btnName = "more";
                        break;
                }

                Page.ClientScript.RegisterStartupScript (this.GetType (), "SetSelected", "SetSelected($('" + btnName + "'));", true);

                if (btnName == "more")
                {
                    Page.ClientScript.RegisterStartupScript (this.GetType (), "SetMoreEmail", "SetMoreEmail('" + arrEmail[1] + "');", true);
                }
            }
        }

        /// <summary>
        /// Check if email domain is supported
        /// </summary>
        /// <returns></returns>
        private bool IsValidEmailDomain (string domain)
        {
            switch (domain.ToLower ())
            {
                case "gmail.com":
                case "yahoo.com":
                case "hotmail.com":
                case "aol.com":
                case "live.com":
				case "live.nl":
				case "msn.com":
				case "rediff.com":
				case "myspace.com":
				case "web.de":
			//	case "mail.com":          // commented out because not working in contact importer code as of 2/4/09
			//	case "mail.ru":
				case "plaxo.com":
				case "linkedin.com":
				case "163.com":
				case "sina.com":
				case "bol.com.br":
			//	case "mynet.com":
				case "indiatimes.com":
				case "126.com":
				case "abv.bg":
				case "wp.pl":
				case "rambler.ru":
			//	case "interia.pl":
				case "lycos.com":
			//	case "libero.it":
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Shows only one div section
        /// </summary>
        /// <param name="div"></param>
        /// <returns></returns>
        private void ShowDiv (HtmlContainerControl div)
        {
            divStep1.Style.Add ("display", "none");
            divStep2.Style.Add ("display", "none");
            divStep3.Style.Add ("display", "none");

            div.Style.Add ("display", "block");
        }

        /// <summary>
        /// Send out an email
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <returns></returns>
        private void SendInvite (int currentUserId, string toEmailAddress, string toName)
        {
            // Generate the unique key code
            string keyCode = KanevaGlobals.GenerateUniqueString (20);

            // Are there to be any K-Points awarded?
            Double awardAmount = 0.0;
            string keiPointIdAward = ""; 

            DataRow drInvitePointAward = UsersUtility.GetInvitePointAwards ();
            if (drInvitePointAward != null)
            {
                awardAmount = Convert.ToDouble (drInvitePointAward["point_award_amount"]);
                keiPointIdAward = drInvitePointAward["kei_point_id"].ToString ();
            }

            // Save the invites to the invites table
            int inviteID = UsersUtility.InsertInvite(currentUserId, toEmailAddress, toName, keyCode, awardAmount, keiPointIdAward);
            if (inviteID == 0)
            {
                ShowErrorOnStartup ("Error inserting invite.");
                return;
            }

            // Is it an invite from a community?
            if (GetCommunityId() > 0)
            {
                DataRow drChannel = CommunityUtility.GetCommunity(GetCommunityId());
                MailUtilityWeb.SendCommunityInvitation(currentUserId, GetCommunityId(), drChannel["name"].ToString(), toEmailAddress, toName, "", inviteID, keyCode);
            }
            else
            {
                MailUtilityWeb.SendInvitation(currentUserId, toEmailAddress, toName, "", inviteID, keyCode, Page);
            }
        }

        /// <summary>
        /// Redirects to the exit url
        /// </summary>
        /// <returns></returns>
        private void ExitConnWithFriends ()
        {
			Response.Cookies["inviteCompleted"].Value = "true";
			Response.Cookies["inviteCompleted"].Expires = DateTime.Now.AddDays(60);	
			
			Response.Redirect ("~/" + ExitUrl);
        }

        /// <summary>
        /// Display the imported contacts
        /// </summary>
        /// <returns></returns>
        private void DisplayContacts (ArrayList contacts, bool isErr, string emailDomain)
        {
            if (isErr)
            {
                // Error retrieving contacts from users email account
                divContactsErr.Style.Add ("display", "block");
                divImportedContacts.Style.Add ("display", "none");
                divNoContacts.Style.Add ("display", "none");
                spnEmailProvider.InnerHtml = emailDomain;
            }
            else
            {
                if (contacts.Count > 0)
                {
                    rptContacts.DataSource = contacts;
                    rptContacts.DataBind ();

                    divImportedContacts.Style.Add ("display", "block");
                    divNoContacts.Style.Add ("display", "none");
                    divContactsErr.Style.Add ("display", "none");
                }
                else
                {
                    // No contacts found so show no contacts found message
                    divImportedContacts.Style.Add ("display", "none");
                    divNoContacts.Style.Add ("display", "block");
                    divContactsErr.Style.Add ("display", "none");
                }
            }

            // Show imported contacts div
            ShowDiv (divStep3);
        }

        /// <summary>
        /// Convert string array into an ArrayList 
        /// </summary>
        /// <returns></returns>
        private void PutContactsIntoArrayList (string[] nameArray, string[] emailArray, ref ArrayList arlstContacts)
        {
            for (int i = 0; i < nameArray.Length; i++)
            {
                arlstContacts.Add (new Contact (nameArray[i], emailArray[i]));
            }
        }


        #endregion Helper Methods


        #region Event Handlers

        /// <summary>
        /// Click event fired for btnSubmitContacts
        /// </summary>
        /// <returns></returns>
        protected void btnGetAddressBook_Click (object sender, ImageClickEventArgs e)
        {
            // validate the email and password fields
            valSum.DisplayMode = ValidationSummaryDisplayMode.BulletList;
            valSum.HeaderText = "Please correct the following error(s):";

            Page.Validate ();
            if (!Page.IsValid)
            {
                if (!rfvEmail.IsValid || !revEmail.IsValid)
                {
                    lblEmail.Style.Add ("color", "red");
                }
                if (!rfvPassword.IsValid)
                {
                    lblPassword.Style.Add ("color", "red");
                }

                return;
            }

            lblEmail.Style.Add ("color", "");
            lblPassword.Style.Add ("color", "");

            // now check to make sure we support the email domain entered
            char[] delimiter = { '@' };
            string[] arrEmail = txtEmail.Text.Split (delimiter);

            if (!IsValidEmailDomain (arrEmail[1]))
            {
                // show err msg  
                cvBlank.ErrorMessage = "<b>Unable to Import Address Book</b><br/><br/>" +
                    "Unfortunately, we cannot import address books from " + arrEmail[1] + ".<br/>" + 
                    "You can invite friends later by entering email addresses, importing another address book, or importing a .csv file.";

                valSum.DisplayMode = ValidationSummaryDisplayMode.SingleParagraph;
                valSum.HeaderText = "";

                cvBlank.IsValid = false;
                return;
            }

            bool isErr = false;

            try
            {
                ci = new ContactImporter (arrEmail[0], txtPW.Text, arrEmail[1]);
                ci.login ();
                valid = ci.logged_in;
            }
            catch 
            {
                isErr = true;
            }

            if (valid)
            {
                try
                {
                    ci.getcontacts ();

                    this.nameArray = ci.nameArray;
                    this.emailArray = ci.emailArray;
                }
                catch (Exception) 
                {
                    isErr = true;
                }

                // send invite or friend request to all contacts retrieved
                if (chkInviteEveryone.Checked && emailArray.Length > 0)
                {
                    UserFacade userFacade = new UserFacade ();

                    for (int i = 0; i < emailArray.Length; i++)
                    {
                        try
                        {

                            if (emailArray[i] != null && emailArray[i].Length > 0)
                            {
                                // Get contacts that are already Kaneva members
                                User user = userFacade.GetUserByEmail (emailArray[i]);
                                if (user.UserId > 0)
                                {
                                    // send friend request to contacts that already belong to kaneva
                                    userFacade.InsertFriendRequest(KanevaWebGlobals.CurrentUser.UserId, user.UserId,
                                        Page.Request.CurrentExecutionFilePath, Global.RequestRabbitMQChannel());
                                }
                                else
                                {
                                    // Check to make sure this email is not already part of an invite from this user.
                                    DataRow drUser = UsersUtility.GetInvite (emailArray[i], KanevaWebGlobals.CurrentUser.UserId);
                                    if ((drUser != null) && (Convert.ToDateTime (drUser["reinvite_date"]).AddDays (14) > DateTime.Now))
                                    {
                                        // Do nothing
                                    }
                                    else
                                    {
                                        // Send invite
                                        SendInvite(KanevaWebGlobals.CurrentUser.UserId, emailArray[i], nameArray[i]);
                                    }
                                }
                            }
                        }
                        catch { }
                    }

					// Record that they've sent invites
					Response.Cookies["invsent"].Value = "true";
					Response.Cookies["invsent"].Expires = DateTime.Now.AddHours(4);

                    // After sending invites and friend requests, exit control
                    ExitConnWithFriends ();
                }
            }
            else
            {
                // display message that login failed
                cvBlank.ErrorMessage = "We encountered a problem retrieving your contacts.  Please verify your email and password and try again.";
                cvBlank.IsValid = false;
                return;
            }

            ArrayList arlstContacts = new ArrayList ();
            PutContactsIntoArrayList (nameArray, emailArray, ref arlstContacts);

            // Display the imported contact list
            DisplayContacts (arlstContacts, isErr, arrEmail[1]);
        }

        /// <summary>
        /// Click event fired for btnSendInvites
        /// </summary>
        /// <returns></returns>
        protected void btnSendInvites_Click (object sender, ImageClickEventArgs e)                
        {
            ViewState["tempUserID"] = KanevaWebGlobals.CurrentUser.UserId;
            PageAsyncTask patSendInvite = new PageAsyncTask(BeginAsyncSendInvites, EndAsyncSendInvites, TimeoutSendInvites, "tskSendInvite", false);
            Page.RegisterAsyncTask(patSendInvite);
            Page.ExecuteRegisteredAsyncTasks();

            ExitConnWithFriends();
        }

        IAsyncResult BeginAsyncSendInvites(Object src, EventArgs args, AsyncCallback cb, Object state)
        {
            _dlgtSendInvites = new AsyncTaskDelegate(SendInvites);
            IAsyncResult result = _dlgtSendInvites.BeginInvoke(cb, state);
            return result;
        }

        void TimeoutSendInvites(IAsyncResult ar)  
        {
            m_logger.Error("Timeout sending email");
        }

        void EndAsyncSendInvites (IAsyncResult ar)
        {
            _dlgtSendInvites.EndInvoke(ar);
            m_logger.Debug("Done Sending");
        }


        public void SendInvites ()
        {
            CheckBox chkEdit;
            UserFacade userFacade = new UserFacade ();

            int currentUserId = 0;
            
            if (ViewState["tempUserID"] != null)
            {
                currentUserId = (int) ViewState["tempUserID"];
            }


			// Record that they've sent invites
			Response.Cookies["invsent"].Value = "true";
			Response.Cookies["invsent"].Expires = DateTime.Now.AddHours(4);

            // send invite or friend request to each contact that is checked
            // if email does not belong to Kaneva account, send invite
            // if email does belong to Kaneva account, send friend request
            foreach (RepeaterItem contact in rptContacts.Items)
            {
                try
                {
                    chkEdit = (CheckBox) contact.FindControl ("chkEdit");

                    if (chkEdit.Checked)
                    {
                        HtmlContainerControl divContactName = (HtmlContainerControl) contact.FindControl ("divContactName");
                        HtmlContainerControl divContactEmail = (HtmlContainerControl) contact.FindControl ("divContactEmail");

                        if (divContactEmail != null)
                        {
                            string contactEmail = divContactEmail.InnerText.Trim ();

                            if (contactEmail.Length > 0)
                            {
                                // Get contacts that are already Kaneva members
                                User user = userFacade.GetUserByEmail (contactEmail);
                                if (user.UserId > 0)
                                {
                                    // send friend request to contacts that already belong to kaneva
                                    userFacade.InsertFriendRequest(currentUserId, user.UserId,
                                        Page.Request.CurrentExecutionFilePath, Global.RequestRabbitMQChannel());
                                }
                                else
                                {
                                    
                                    DataRow drUser = null;

                                    try
                                    {
                                        // check if there is an invite for this user already 
                                        drUser = UsersUtility.GetInvite(contactEmail, currentUserId);                                   
                                    }
                                    catch (Exception exc)
                                    {
                                        m_logger.Warn("Email log 1", exc);
                                    }

                                    if ((drUser != null) && (Convert.ToDateTime(drUser["reinvite_date"]).AddDays(14) > DateTime.Now))
                                    {
                                        // Do nothing
                                        m_logger.Info("Already invited by this user");
                                    }
                                    else
                                    {
                                        if (divContactName != null)
                                        {
                                            // send invite
                                            SendInvite(currentUserId, contactEmail, divContactName.InnerText.Trim());
                                        }
                                    }
                                    

                                }
                            }
                        }
                    }
                }
                catch (Exception exc2 )
                {
                    m_logger.Warn("Email log 2", exc2);
                }
            }
        }

        /// <summary>
        /// Click event called to exit user control
        /// </summary>
        /// <returns></returns>
        protected void Exit_Click (object sender, EventArgs e)
        {
            ExitConnWithFriends ();
        }

        /// <summary>
        /// Click event fired for linkbutton lbSkipInviteFriends
        /// </summary>
        /// <returns></returns>
        protected void lbSkipInvites_Click (object sender, EventArgs e)
        {
            ExitConnWithFriends ();
        }

        /// <summary>
        /// lbEnterEmail_Click
        /// </summary>
        protected void lbEnterEmail_Click(object sender, CommandEventArgs e)
        {
            Response.Redirect("~/community/viral/CommunityInviteWithMessage.aspx?communityId=" + GetCommunityId());
        }

        /// <summary>
        /// GetCommunityId
        /// </summary>
        /// <returns></returns>
        private int GetCommunityId()
        {
            if (Request["communityId"] != null)
            {
                return Convert.ToInt32(Request["communityId"]);
            }

            return 0;
        }

        #endregion Event Handlers


        #region Properties

        /// <summary>
        /// Internal class used to hold contact info
        /// </summary>
        /// <returns></returns>
        protected class Contact
        {
            private string _name = "";
            private string _email = "";

            public Contact (string name, string email)
            {
                this._name = name;
                this._email = email;
            }

            public string Name
            {
                get { return _name; }
                set { _name = value; }
            }

            public string Email
            {
                get { return _email; }
                set { _email = value; }
            }
        }

        /// <summary>
        /// Public classed used to set the exit url (
        /// </summary>
        /// <returns></returns>
        public string ExitUrl
        {
            get { return _exitUrl; }
            set { _exitUrl = value; }
        }

        public string divStep1_ClientId
        {
            get { return _step1Id; }
            set { _step1Id = value; }
        }

        public string divStep2_ClientId
        {
            get { return _step2Id; }
            set { _step2Id = value; }
        }

        public string txtEmail_ClientId
        {
            get { return _txtEmail; }
            set { _txtEmail = value; }
        }
        
        #endregion Properties


        #region Declerations

        private string _exitUrl = "";
        private string _step1Id = "";
        private string _step2Id = "";
        private string _txtEmail = "";
        protected string postBackStr = "";

        public string[] nameArray = new string[0];
        public string[] emailArray = new string[0];
        public string[] emArray = new string[0];

        public String strHidden = "";
        public String primKey = "";
        public bool valid = false;
        
        ContactImporter ci = null;

        private AsyncTaskDelegate _dlgtSendInvites;
        protected delegate void AsyncTaskDelegate();

        /// <summary>
        /// Logger
        /// </summary>
        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #endregion Declerations
    }
}
