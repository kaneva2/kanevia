///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using log4net;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.usercontrols
{
    public partial class VirtualWorld_Kaching : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["contentpath"] != null)
            {                                               
                // determine which page to show
                if (Request["contentpath"].EndsWith("kaneva-ka-ching.html"))
                {
                    divAbout.Visible = true;
                    
                    // If it's the user's first time here we play the flash!
                    if (Request.Cookies["KachingFlash"] == null)
                    {            
                        litFlashWrite.Text = "so.write(\"flash1\");";
                        Response.Cookies["KachingFlash"].Value = "True";
                        Response.Cookies["KachingFlash"].Expires = DateTime.Now.AddMonths(3);
                    }

                    if (!Page.IsPostBack)
                    {                        
                        BindLeaderboard(defaultSortOrder);
                        lbSortWinsB.CssClass = "selected";
                    }
                }
                else if (Request["contentpath"].EndsWith("play-ka-ching.html"))
                {
                    divPlay.Visible = true;
                }
                else if (Request["contentpath"].EndsWith("ka-ching-leaderboard.html"))
                {
                    divLeaderboard.Visible = true;
                    if (!Page.IsPostBack)
                    {
                        TimeRangeTab("Today");
                        BindLeaderboard(defaultSortOrder);
                        // show that the initial results are sorted by wins
                        lbSortWinsA.CssClass = "selected";
                          
                    }
                }
            }
        }

        private void BindLeaderboard(string sortOrder)
        {
            try
            {                
                // number of results to return
                int numResults = 10;
                if (divLeaderboard.Visible){ numResults = 25; }

                DateTime dtStartDate = DateTime.Now.Date;
                DateTime dtEndDate = DateTime.Now.Date.AddDays(1);
                if (ViewState["StartDate"] != null)
                {
                    dtStartDate = Convert.ToDateTime(ViewState["StartDate"]).Date;
                }         
      
                string strTimeInterval = "D";
                if (ViewState["Time"] != null)
                {
                    strTimeInterval = ViewState["Time"].ToString ();
                }
                
                // Get the Leaders
                KachingFacade kachingFacade = new KachingFacade();
                DataTable dtLeaderboard = null;

                if (strTimeInterval.Equals ("D"))
                {
                    dtLeaderboard = WebCache.GetLeaderboardDay(sortOrder, numResults);
                }
                else if (strTimeInterval.Equals ("W"))
                {
                    dtLeaderboard = WebCache.GetLeaderboardWeek(sortOrder, numResults);
                }
                else
                {
                    dtLeaderboard = WebCache.GetLeaderboardAllTime(sortOrder, numResults);
                }

                if (Request.IsAuthenticated)
                {
                    try
                    {
                        DataTable dtUserStats = kachingFacade.GetUserStats(dtStartDate, dtEndDate, KanevaWebGlobals.CurrentUser.UserId);
                        rptUserStats.DataSource = dtUserStats;
                        rptUserStats.DataBind();
                    }
                    catch (Exception ex)
                    {
                        m_logger.Error(ex.ToString());
                    }
                    
                }


                if ((dtLeaderboard != null) && (dtLeaderboard.Rows.Count > 0))
                {

                    // Only if we haven't done before (it may be a cached version)
                    if (!dtLeaderboard.Columns.Contains("rank"))
                    {
                        // Add column to hold the rank
                        DataColumn column = new DataColumn();
                        column = new DataColumn();
                        column.DataType = System.Type.GetType("System.String");
                        column.AllowDBNull = false;
                        column.ColumnName = "rank";
                        column.DefaultValue = 0;
                        dtLeaderboard.Columns.Add(column);


                        // get the appropriate rank for each player
                        int iRank = 1;

                        for (int i = 0; i < dtLeaderboard.Rows.Count; i++)
                        {
                            dtLeaderboard.Rows[i]["rank"] = iRank;
                            iRank++;
                        }
                    }

                    if (divAbout.Visible)
                    {
                        rptLeadersBasic.DataSource = dtLeaderboard;
                        rptLeadersBasic.DataBind();
                    }
                    else if (divLeaderboard.Visible)
                    {
                        rptLeadersAdv.DataSource = dtLeaderboard;
                        rptLeadersAdv.DataBind();
                        rptLeadersAdv.Visible = true;
                    }



                    // make sure the error messages don't show.
                    ulErrMsgA.Visible = false;
                    ulErrMsgB.Visible = false;                    
                }
                else
                {
                    litErrMsgA.Text = "No results to display.";
                    ulErrMsgA.Visible = true;
                    litErrMsgB.Text = "No results.";
                    ulErrMsgB.Visible = true;
                    rptLeadersAdv.Visible = false;
                }                
            }
            catch (Exception ex)
            {
                m_logger.Error(ex.ToString());
            }
        }

        protected string HiLiteUser(int userId)
        {
            if (Request.IsAuthenticated)
            {
                if (KanevaWebGlobals.CurrentUser.UserId == userId)
                {
                    rptUserStats.Visible = false;
                    return "ka_lb_me";                    
                }
                else
                {
                    return "";
                }
            }
            else
            {
                return "";
            }
        }


        void SortData(string SortExpression)
        {
            /* code for reversing sort order, however we always want DESC
             * 
            
            if (ViewState["SortOrder"] == null)
            {
                ViewState["SortOrder"] = " DESC";
            }
            else if (ViewState["SortOrder"].ToString() == " DESC")
            {
                ViewState["SortOrder"] = " ASC";
            }
            else
            {
                ViewState["SortOrder"] = " DESC";
            } 
            */
            ViewState["SortOrder"] = " DESC";

            string sortOrder = " ORDER BY " + SortExpression.ToString() + " " + ViewState["SortOrder"];
            
            BindLeaderboard(sortOrder);
        }

        string sortField;

        protected void ClearSortHilite()
        {
            lbSortChecksA.CssClass = "";
            lbSortBouncesA.CssClass = "";
            lbSortRatioA.CssClass = "";
            lbSortRatioB.CssClass = "";
            lbSortWinsA.CssClass = "";
            lbSortCoinsA.CssClass = "";
            lbSortWinsB.CssClass = "";
            lbSortCoinsB.CssClass = "";
        }

        protected void SortRatioClick(object sender, EventArgs e)
        {
            ClearSortHilite();
            lbSortRatioA.CssClass = "selected";
            lbSortRatioB.CssClass = "selected";            
            sortField = "KillsToDeathsRatio";
            ViewState["sortField"] = sortField.ToString();
            SortData(sortField);
        }

        protected void SortWinsClick(object sender, EventArgs e)
        {
            ClearSortHilite();
            lbSortWinsA.CssClass = "selected";
            lbSortWinsB.CssClass = "selected";    
            sortField = "wins";
            ViewState["sortField"] = sortField.ToString();
            SortData(sortField);
        }

        protected void SortCoinsClick(object sender, EventArgs e)
        {
            ClearSortHilite();
            lbSortCoinsA.CssClass = "selected";
            lbSortCoinsB.CssClass = "selected";    
            sortField = "coins";
            ViewState["sortField"] = sortField.ToString();
            SortData(sortField);
        }

        protected void SortChecksClick(object sender, EventArgs e)
        {
            ClearSortHilite();
            lbSortChecksA.CssClass = "selected";
            sortField = "kills";
            ViewState["sortField"] = sortField.ToString();
            SortData(sortField);
        }

        protected void SortBouncesClick(object sender, EventArgs e)
        {
            ClearSortHilite();
            lbSortBouncesA.CssClass = "selected";           
            sortField = "deaths";
            ViewState["sortField"] = sortField.ToString();
            SortData(sortField);
        }

        protected void TodayClick(object sender, EventArgs e)
        {
            ViewState["Time"] = "D";
            ViewState["StartDate"] = null;
            TimeRangeTab("Today");            
            if (ViewState["sortField"] != null)
            {
                SortData(ViewState["sortField"].ToString());
            }
            else
            {
                BindLeaderboard(defaultSortOrder);
                ClearSortHilite();
                lbSortWinsA.CssClass = "selected";
                lbSortWinsB.CssClass = "selected";
            }
        }

        protected void AllTimeClick(object sender, EventArgs e)
        {
            ViewState["Time"] = "AT";
            ViewState["StartDate"] = "1970-01-01";
            if (ViewState["sortField"] != null)
            {
                SortData(ViewState["sortField"].ToString());                
            }
            else
            {
                BindLeaderboard(defaultSortOrder);
                ClearSortHilite();
                lbSortWinsA.CssClass = "selected";
                lbSortWinsB.CssClass = "selected"; 
            }
            TimeRangeTab("AllTime");
 
        }

        protected void SevenDaysClick(object sender, EventArgs e)
        {
            ViewState["Time"] = "W";
            ViewState["StartDate"] = DateTime.Now.Date.AddDays(-7);
            TimeRangeTab("7Days");
            if (ViewState["sortField"] != null)
            {
                SortData(ViewState["sortField"].ToString());
            }
            else
            {
                BindLeaderboard(defaultSortOrder);
                ClearSortHilite();
                lbSortWinsA.CssClass = "selected";
                lbSortWinsB.CssClass = "selected";
            }

        }

        protected void TimeRangeTab(string tabName)
        {
            liToday.Attributes.Add("class", "none");
            li7Days.Attributes.Add("class", "none");
            liAllTime.Attributes.Add("class", "none"); 

            switch (tabName)
            {               
                case "Today":
                    {
                        liToday.Attributes.Add("class", "selected");
                        break;
                    }
                case "7Days":
                    {
                        li7Days.Attributes.Add("class", "selected");
                        break;
                    }
                case "AllTime":
                    {
                        liAllTime.Attributes.Add("class", "selected");
                        break;
                    }
            }
        }

        protected void PlayFlash(object sender, ImageClickEventArgs e)
        {
            //Response.Cookies["KachingFlash"].Expires = DateTime.Now.AddDays(-1);
            litFlashWrite.Text = "so.write(\"flash1\");";
        }

        #region Declarations

        string defaultSortOrder = " ORDER BY wins DESC, KillsToDeathsRatio DESC, coins DESC ";
        
        protected HtmlContainerControl divAbout, divPlay, divLeaderboard, divRepeater;
        protected Literal litFlashWrite, litErrMsgA, litErrMsgB;
        protected Repeater rptLeadersBasic, rptLeadersAdv, rptUserStats;
        protected HtmlGenericControl liToday, liAllTime, li7Days, ulErrMsgA, ulErrMsgB;
        protected LinkButton lbSortChecksA, lbSortBouncesA, lbSortRatioA, lbSortRatioB, lbSortWinsA, lbSortCoinsA, lbSortWinsB, lbSortCoinsB;

        private static readonly ILog m_logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #endregion 
    }


}