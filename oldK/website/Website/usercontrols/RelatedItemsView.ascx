<%@ Control Language="c#" AutoEventWireup="false" Codebehind="RelatedItemsView.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.usercontrols.RelatedItemsView" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../usercontrols/Pager.ascx" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>

<div id="divContainer" runat="server">

<ajax:ajaxpanel id="Ajaxpanel1" runat="server">

<div id="related_header">
	<ul class="related_tabs">
		<li style="width:86px;" id="liRelated" runat="server"><asp:linkbutton id="lbRelated" runat="server" commandname="related" oncommand="RelatedTab_Changed">Related</asp:linkbutton></li> 
		<li style="width:170px;" id="liMore" runat="server"><asp:linkbutton id="lbMore" runat="server" commandname="more" oncommand="RelatedTab_Changed">More from this member</asp:linkbutton></li> 
	</ul>
</div>

<div id="related_content">

<table width="96%"  border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
													
		<asp:datalist runat="server" enableviewstate="True" showfooter="False" width="100%" id="dlRelated" cellpadding="0" cellspacing="0" border="0" repeatcolumns="1" repeatdirection="Horizontal">
			<itemstyle horizontalalign="left" verticalalign="top" />
			
			<itemtemplate>

			<table width="100%" border="0" cellspacing="0" cellpadding="0">	
				<tr>
					<td align="left">
						
						<div class="framesize-small">
							<div class="restricted" style="display:<%# IsMature (Convert.ToInt32 ( DataBinder.Eval(Container.DataItem, "asset_rating_id"))) ? "block" : "none" %>"></div>
							<div class="frame">
								<span class="ct"><span class="cl"></span></span>							
									<div class="imgconstrain">
										<a title='<%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "name").ToString ())%>' href='<%# GetAssetDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "asset_id")))%>'>
											<img border="0" src='<%#GetMediaImageURL (DataBinder.Eval(Container.DataItem, "thumbnail_medium_path").ToString () ,"me", Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_id")), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_type_id")), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "thumbnail_gen")))%>' border="0"/>
										</a>
									</div>																																				
								<span class="cb"><span class="cl"></span></span>
							</div>
						</div>
								
					</td>
					<td width="1"><img runat="server" src="~/images/spacer.gif" border="0" width="10" height="8" id="Img1"/></td>
					<td align="left" width="100%">
						
						<a title='<%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "name").ToString ())%>' href='<%# GetAssetDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "asset_id")))%>'><%# TruncateWithEllipsis (Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "name").ToString ()), 100) %></a>
						<br>
						Shared by: <a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "name_no_spaces").ToString ())%>><%# DataBinder.Eval(Container.DataItem, "username") %></a>
						<br>
						Views: <%# DataBinder.Eval(Container.DataItem, "number_of_downloads") %> | Raves: <%# DataBinder.Eval(Container.DataItem, "number_of_diggs") %>
								
					</td>
				</tr>
			</table>
					
			<hr />			
				
			</itemtemplate>
		</asp:datalist>
		
		
		<asp:datalist runat="server" enableviewstate="True" showfooter="False" width="100%" id="dlMore" cellpadding="0" cellspacing="0" border="0" repeatcolumns="1" repeatdirection="Horizontal">
			<itemstyle horizontalalign="left" verticalalign="top" />
			
			<itemtemplate>

			<table width="100%" border="0" cellspacing="0" cellpadding="0">	
				<tr>
					<td align="left">
						
						<div class="framesize-small">
							<div class="restricted" style="display:<%# IsMature (Convert.ToInt32 ( DataBinder.Eval(Container.DataItem, "asset_rating_id"))) ? "block" : "none" %>"></div>
							<div class="frame">
								<span class="ct"><span class="cl"></span></span>							
									<div class="imgconstrain">
										<a title='<%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "name").ToString ())%>' href='<%# GetAssetDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "asset_id")))%>'>
											<img border="0" src='<%#GetMediaImageURL (DataBinder.Eval(Container.DataItem, "thumbnail_medium_path").ToString () ,"me", Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_id")), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "asset_type_id")), Convert.ToInt32 (DataBinder.Eval(Container.DataItem, "thumbnail_gen")))%>' border="0"/>
										</a>
									</div>																																				
								<span class="cb"><span class="cl"></span></span>
							</div>
						</div>
								
					</td>
					<td width="1"><img runat="server" src="~/images/spacer.gif" border="0" width="10" height="8" id="Img2"/></td>
					<td align="left" width="100%">
						
						<a title='<%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "name").ToString ())%>' href='<%# GetAssetDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "asset_id")))%>'><%# TruncateWithEllipsis (Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "name").ToString ()), 100) %></a>
						<br/>
						Views: <%# DataBinder.Eval(Container.DataItem, "number_of_downloads") %> | Raves: <%# DataBinder.Eval(Container.DataItem, "number_of_diggs") %>
								
					</td>
				</tr>
			</table>
					
			<hr />			
				
			</itemtemplate>
		</asp:datalist>


		</td>
	</tr>
	<tr runat="server" visible="false">
		<td>
			<table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0">
				<tr>
					<td width="7" class="frTopLeft"><img runat="server" src="~/images/spacer.gif" width="6" height="6" /></td>
					<td width="50%" class="frTop"><img runat="server" src="~/images/spacer.gif" width="6" height="6" /></td>
					<td width="6" class="frTopRight"><img runat="server" src="~/images/spacer.gif" width="6" height="6" /></td>
				</tr>
				<tr align="left" class="intColor1">
					<td class="intBorderC1BorderLeft1">&nbsp;</td>
					<td width="100%" class="intColor1">
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="3" height="3"><img runat="server" src="~/images/spacer.gif" width="3" height="3" /></td>
								<td width="3" height="3"><img runat="server" src="~/images/spacer.gif" width="3" height="3" /></td>
								<td width="3" height="3"><img runat="server" src="~/images/spacer.gif" width="3" height="3" /></td>
							</tr>
							<tr>
								<td width="3" height="3"><img runat="server" src="~/images/spacer.gif" width="3" height="3" /></td>
								<td width="100%" align="right"><span class="textGeneralGray03">Please encourage the owner to categorize this media.</span></td>
								<td width="3" height="3"><img runat="server" src="~/images/spacer.gif" width="3" height="3" /></td>
							</tr>
							<tr>
								<td width="3" height="3"><img runat="server" src="~/images/spacer.gif" width="3" height="3" /></td>
								<td width="3" height="3"><img runat="server" src="~/images/spacer.gif" width="3" height="3" /></td>
								<td width="3" height="3"><img runat="server" src="~/images/spacer.gif" width="3" height="3" /></td>
							</tr>
						</table>
					</td>
					<td class="intBorderC1BorderRight1">&nbsp;</td>
				</tr>
				<tr align="left" >
					<td class="intBorderC1BottomLeft"><img runat="server" src="~/images/spacer.gif" width="7" height="7" /></td>
					<td class="intBorderC1Bottom"><img runat="server" src="~/images/spacer.gif" width="7" height="7" /></td>
					<td class="intBorderC1BottomRight"><img runat="server" src="~/images/spacer.gif" width="7" height="7" /></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
				
					
</div>
<div id="related_footer"><span id="spnShowing" runat="server"/><a id="aAllResults" runat="server">See More Media</a></div>
																													
</ajax:ajaxpanel>

</div>