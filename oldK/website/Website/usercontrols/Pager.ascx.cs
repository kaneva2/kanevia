///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

namespace KlausEnt.KEP.Kaneva
{
	using System;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

	public delegate void PageChangeEventHandler (object sender, PageChangeEventArgs e);

	/// <summary>
	/// PageChangeEventArgs
	/// </summary>
	public class PageChangeEventArgs: EventArgs
	{
		/// <summary>
		/// PageChangeEventArgs
		/// </summary>
		/// <param name="newPageNumber"></param>
		public PageChangeEventArgs (int newPageNumber)
		{
			m_pageNumber = newPageNumber;
		}

		/// <summary>
		/// PageNumber
		/// </summary>
		public int PageNumber
		{
			get
			{
				return m_pageNumber;
			}
		}

		/// <summary>
		/// Page Number
		/// </summary>
		private int m_pageNumber;
	}

	/// <summary>
	///		Summary description for Pager.
	/// </summary>
	public class Pager : System.Web.UI.UserControl
	{
		/// <summary>
		/// Page Load
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Page_Load(object sender, System.EventArgs e)
		{
			if ( ShowPagesDisplay )
			{
				// Add the javascript for changing pages
				string scriptString = "<script language=\"javaScript\">\n<!--\n function ChangePage" + this.ClientID + " (id) {\n";
				scriptString += " document.getElementById('" + hidPageNumber.ClientID + "').value = id;\n";

				if (IsAjaxMode)
				{
					scriptString += MagicAjax.AjaxCallHelper.GetAjaxCallEventReference (PageClick) + ";\n";
				}
				else
				{
					scriptString += Page.ClientScript.GetPostBackEventReference (PageClick, "", false) + ";\n";
				}

				scriptString += "}\n// -->\n";
				scriptString += "</script>";

                if (!Page.ClientScript.IsClientScriptBlockRegistered(GetType (), "ChangePage" + this.ClientID))
				{
                    Page.ClientScript.RegisterClientScriptBlock(GetType (), "ChangePage" + this.ClientID, scriptString);
				}
			}

			DrawControl ();
		}


		#region Helper Methods
		/// <summary>
		/// Draw the control
		/// </summary>
		public void DrawControl ()
		{
			int pageNumber = CurrentPageNumber;
			int totalPages = int.Parse (NumberOfPages);

			if ( ShowNextPrevLabels )
			{
				PrevPage.Text = "Prev";
				NextPage.Text = "Next";
			}
            if (PrevBtnText != "")
            {
                PrevPage.Text = this.PrevBtnText;
            }
            if (NextBtnText != "")
            {
                NextPage.Text = this.NextBtnText;
            }

			if ( ShowPagesDisplay )
			{
				// Display the page links
				PagesDisplay.Text = "";

				// Logic to only show 10 per page.
				int iStart = 1;
				int iEnd = totalPages;
				if (totalPages > MaxPagesToDisplay)
				{
					// Are we more than half way? If so, start at new place
					if (pageNumber > MaxPagesToDisplay / 2)
					{
						// Are we near the end?
						if (pageNumber > (totalPages - MaxPagesToDisplay / 2 - 1))
						{
							// Near the end
							iStart = totalPages - MaxPagesToDisplay + 1;
							iEnd = totalPages;
						}
						else
						{
							iStart = pageNumber - MaxPagesToDisplay / 2;
							double n = (double)MaxPagesToDisplay / 2;
							iEnd = (int)Math.Ceiling(pageNumber + n - 1);
						}
					}
					else
					{
						iEnd = MaxPagesToDisplay;
					}
				}

				for (int i = iStart; i <= iEnd; i++)
				{
                    if (pageNumber != i)
                    {
                        PagesDisplay.Text += "<a class=\"pager\" href=\"javascript:ChangePage" + this.ClientID + "(" + i + ")\">" + i + "</a> ";
                    }
                    else
                    {
                        if (this.ShowBracketAroundCurrentPage)
                        {
                            PagesDisplay.Text += "<span class=\"pager\">[" + i + "]</span> ";
                        }
                        else
                        {
                            PagesDisplay.Text += "<span class=\"pager\">" + i + "</span> ";
                        }
                    }
				}

				if (totalPages > iEnd)
					PagesDisplay.Text += "<span class=\"pager\">...</span> ";
			}
    
			// Enable/disable the links to navigate through the pages
			// FirstPage.Enabled = (pageNumber != 1);
			// LastPage.Enabled = (pageNumber != totalPages);

			if ( ShowDisabledPageLinks )
			{
				PrevPage.Enabled = (pageNumber != 1);
				NextPage.Enabled = (pageNumber != totalPages);
			}
			else
			{
				PrevPage.Visible = (pageNumber != 1);
				NextPage.Visible = (pageNumber != totalPages) && (totalPages > 1);
			}

            if (this.PrevNextPageBtnClass != "")
            {
                PrevPage.CssClass = PrevNextPageBtnClass;
                NextPage.CssClass = PrevNextPageBtnClass;
            }
		}

		/// <summary>
		/// Update the control visibility
		/// </summary>
		/// <param name="bShow"></param>
		private void UpdateVisibility (bool bShow)
		{
			PrevPage.Visible = bShow;
			NextPage.Visible = bShow;


			PagesDisplay.Visible = bShow && ShowPagesDisplay;
			sep1.Visible = false; //bShow && ShowPagesDisplay;
			sep2.Visible = false; //bShow && ShowPagesDisplay;
		}

		#endregion
		

		#region Event Handlers
		/// <summary>
		/// execute when user clicks on the page link
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void Page_Click (object sender, System.EventArgs e)
		{
			CurrentPageNumber = int.Parse (hidPageNumber.Value);

			// rebind data
			DrawControl ();

			// Raise the event
			PageChanged (this, new PageChangeEventArgs (int.Parse (hidPageNumber.Value)));
		}

		/// <summary>
		/// execute when user clicks on the next/prev/first/last button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void Page_Changed (object sender, CommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "FirstPage":
					hidPageNumber.Value = "1";
					break;
				case "PrevPage":
					hidPageNumber.Value = (int.Parse(hidPageNumber.Value) -1).ToString();
					break;
				case "NextPage":
					hidPageNumber.Value = (int.Parse(hidPageNumber.Value) +1).ToString();
					break;
				case "LastPage":
					hidPageNumber.Value = NumberOfPages;
					break;
			}
    
			// rebind data
			CurrentPageNumber = int.Parse (hidPageNumber.Value);
			DrawControl ();
			PageChanged (this, new PageChangeEventArgs (int.Parse (hidPageNumber.Value)));
		}

		#endregion
		

		#region Properties
		public bool ShowNextPrevLabels
		{
			get
			{
				if ( ViewState["ShowNextPrevLabels_" + this.ClientID] != null )
					return (bool)ViewState["ShowNextPrevLabels_" + this.ClientID];
				else
					return false;
			}
			set
			{
				ViewState["ShowNextPrevLabels_" + this.ClientID] = value;
			}
		}

		public bool ShowPagesDisplay
		{
			get
			{
				if ( ViewState["ShowPagesDisplay_" + this.ClientID] != null )
					return (bool)ViewState["ShowPagesDisplay_" + this.ClientID];
				else
					return true;
			}
			set
			{
				ViewState["ShowPagesDisplay_" + this.ClientID] = value;
			}
		}

		public bool ShowDisabledPageLinks
		{
			get
			{
				//if ( ViewState["ShowDisabledPageLinks_" + this.ClientID] != null )
				//	return (bool)ViewState["ShowDisabledPageLinks_" + this.ClientID];
				//else
				//	return true;

				// Added this to make a global change for all pager instances
				return false;
			}
			set
			{
				ViewState["ShowDisabledPageLinks_" + this.ClientID] = value;
			}
		}

		public int MaxPagesToDisplay
		{
			get
			{
				if ( ViewState["MaxPagesToDisplay_" + this.ClientID] != null )
					return (int)ViewState["MaxPagesToDisplay_" + this.ClientID];
				else
					return MAXPAGESDRAWN;
			}
			set
			{
				ViewState["MaxPagesToDisplay_" + this.ClientID] = value;
			}
		}
		
		/// <summary>
		/// Set the number of pages
		/// </summary>
		public string NumberOfPages
		{
			set 
			{
				ViewState [this.ClientID + "hidPages"] = value;
				UpdateVisibility ((Convert.ToInt32 (value)) > 1);
			} 
			get
			{
				if (ViewState [this.ClientID + "hidPages"] != null)
				{
					return ViewState [this.ClientID + "hidPages"].ToString ();
				}
				else
				{
					return "0";
				}
			}
		}
		
		/// <summary>
		/// Get/Set the current page
		/// </summary>
		public int CurrentPageNumber
		{
			set 
			{
				ViewState [this.ClientID + "pn"] = value;
				hidPageNumber.Value = value.ToString ();
			} 
			get
			{
				if (ViewState [this.ClientID + "pn"] != null)
				{
					return Convert.ToInt32 (ViewState [this.ClientID + "pn"]);
				}
				else
				{
					return 1;
				}
			}
		}

		// Is the pager in ajax mode?
		public bool IsAjaxMode
		{
			set 
			{
				m_AjaxMode = Convert.ToBoolean (value);
			} 
			get
			{
				return m_AjaxMode;
			}
		}


        public string PrevBtnText
        {
            set
            {
                ViewState[this.ClientID + "prevBtnText"] = value;
//                UpdateVisibility ((Convert.ToInt32 (value)) > 1);
            }
            get
            {
                if (ViewState[this.ClientID + "prevBtnText"] != null)
                {
                    return ViewState[this.ClientID + "prevBtnText"].ToString ();
                }
                else
                {
                    return "";
                }
            }
        }

        public string NextBtnText
        {
            set
            {
                ViewState[this.ClientID + "nextBtnText"] = value;
                //                UpdateVisibility ((Convert.ToInt32 (value)) > 1);
            }
            get
            {
                if (ViewState[this.ClientID + "nextBtnText"] != null)
                {
                    return ViewState[this.ClientID + "nextBtnText"].ToString ();
                }
                else
                {
                    return "";
                }
            }
        }

        public string PrevNextPageBtnClass
        {
            set
            {
                ViewState[this.ClientID + "prevNextPageBtnClass"] = value;
            }
            get
            {
                if (ViewState[this.ClientID + "prevNextPageBtnClass"] != null)
                {
                    return ViewState[this.ClientID + "prevNextPageBtnClass"].ToString ();
                }
                else
                {
                    return "";
                }
            }
        }
        public bool ShowBracketAroundCurrentPage
		{
			get
			{
				if ( ViewState["showBrackets_" + this.ClientID] != null )
					return (bool)ViewState["showBrackets_" + this.ClientID];
				else
					return true;
			}
			set
			{
				ViewState["showBrackets_" + this.ClientID] = value;
			}
		}

        #endregion
		
		
		#region Declerations

		protected LinkButton	PrevPage, NextPage;
		protected Label			PagesDisplay;
		protected Label			sep1, sep2;
		protected Button		PageClick;

		protected HtmlInputHidden hidPageNumber;

		protected bool m_AjaxMode = false;

		private const int MAXPAGESDRAWN = 10;

		public event PageChangeEventHandler PageChanged;

		#endregion


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
