<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ProfilePanel.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.usercontrols.ProfilePanel" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>
<%@ Register TagPrefix="UserControl" TagName="LightBox" Src="../usercontrols/LightBox.ascx" %>

<usercontrol:lightbox runat="server" id="ucLightBox" />
<script type="text/javascript">    
	var $j = jQuery.noConflict();	  
	var communityId = <asp:literal id="litCommunityId" runat="server"></asp:literal>;    
	function GetCommunityId () { return itemId; }
	$j(document).ready(function()
	{				
		popFacebox=function()
		{
			$j.facebox({ iframe:'../buyRave.aspx?communityId=' + communityId + '&type=single' });
 		}
 		popFaceboxMega=function()
		{
			$j.facebox({ iframe:'../buyRave.aspx?communityId=' + communityId + '&type=mega' });
 		}	
	});
</script>
	
	
<LINK href="../css/meetme3d.css" type="text/css" rel="stylesheet">
<ajax:ajaxpanel id="Ajaxpanel1" runat="server">
	<TABLE id="widgetContent" cellSpacing="0" cellPadding="0" width="100%" border="0">
		<TR>
			<TD id="pageOpacity" width="100%">
				<DIV id="widgetBorder"><B class="outerFrame" id="outerFrame"><!-- style="display:none;"--><B class="outerFrame1"><B></B></B><B class="outerFrame2"><B></B></B><B class="outerFrame3"></B><B class="outerFrame4"></B><B class="outerFrame5"></B></B>
					<DIV class="outerFrame_content" id="outerFrame_content"><!-- style="padding:0px;background:transparent;border-width=0px;"-->  <!-- Your Content Goes Here -->
						<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
							<TR>
								<TD vAlign="top" height="100%">
									<DIV id="widgetBody"><B class="innerFrame"><B class="innerFrame1"><B></B></B><B class="innerFrame2"><B></B></B><B class="innerFrame3"></B><B class="innerFrame4"></B><B class="innerFrame5"></B></B>
										<DIV class="innerFrame_content"><!-- Your Content Goes Here -->
											<TABLE id="controlpanel" cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
												<TR vAlign="bottom">
													<TD align="center">
														<DIV class="container" id="divContainer">
															<DIV class="float" id="controlSection1" style="PADDING-BOTTOM: 10px" runat="server" width="98%">
																<TABLE cellSpacing="0" cellPadding="10" width="280" align="center" border="0"> <!--STARTS AN ELEMENT--->
																	<TR> <!--thumb section--->
																		<TD vAlign="top" align="left" width="100">
																			<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																				<TR>
																					<TD vAlign="top">
																						<DIV class="framesize-medium">
																							<DIV class="frame noborder"><SPAN class="ct"><SPAN class="cl"></SPAN></SPAN>
																								<DIV class="imgconstrain" ><A id="aAvatar" runat="server" class="nohover highslide slow"><IMG id="imgAvatar" src="~/images/KanevaIcon01.gif" border="0" runat="server"></A>
																								</DIV>
																									<TABLE cellSpacing="0" cellPadding="0" salign="center" border="0" width="100%">
																										<TR>
																											<TD class="widgetText" align="left" >
																												<span><asp:literal id="litOnline" runat="server"></asp:literal></span>
																											</TD>
																										</TR>
																									</TABLE>
																								<SPAN class="cb"><SPAN class="cl"></SPAN></SPAN>
																							</DIV>
																						</DIV>
																					</TD>
																				</TR>
																			</TABLE>
																		</TD> <!--end thumb section-->
																		<TD vAlign="top"><IMG height="1" src="http://www.kaneva.com/images/spacer.gif" width="10" border="0"></TD>
																		<TD vAlign="top">
																			<TABLE cellSpacing="0" cellPadding="0" width="100%" align="left" border="0">
																				<TR>
																					<TD class="widgetTextBold" noWrap align="left">
																						<asp:label id="lblDisplayName" runat="server"></asp:label></TD>
																				</TR>
																				<TR>
																					<TD class="widgetText" noWrap align="left">
																						<asp:label id="lblUserName" runat="server"></asp:label></TD>
																				</TR>
																				<TR>
																					<TD class="widgetText small" align="left">
																						<asp:label id="lblLocation" runat="server"></asp:label></TD>
																				</TR>
																				<TR>
																					<TD class="widgetText small" align="left">
																						<asp:label id="lblGender" runat="server"></asp:label></TD>
																				</TR>
																				<TR>
																					<TD class="widgetText small" align="left">
																						<asp:label id="lblAge" runat="server"></asp:label></TD>
																				</TR>
																				<TR>
																					<TD noWrap><A class="widgetText" id="aPictures" style="TEXT-DECORATION: underline" runat="server">See 
																							More Photos</A></TD>
																				</TR>
																				<TR>
																					<TD noWrap><br /><asp:literal id="litHotLinkLocation" runat="server" /><strong><asp:literal id="litDisplayText" runat="server" /></strong></a>
																					</TD>
																				</TR>
																			</TABLE>
																		</TD>
																	</TR> <!--END ONE ELEMENT--></TABLE>
															</DIV>
															<DIV class="float" id="controlSection2" style="PADDING-BOTTOM: 10px; WIDTH: 98%" runat="server">
																<TABLE cellSpacing="0" cellPadding="0" width="280" align="center" border="0"> <!--STARTS AN ELEMENT--->
																	<TR>
																		<TD vAlign="top">
																			
																		</TD>
																		<TD vAlign="top">
																			<TABLE cellSpacing="0" cellPadding="0" align="left" border="0">
																				<!--
																				<TR>
																					<TD class="widgetText small" align="left" width="100">Member since:</TD>
																					<TD class="widgetText small" align="left">
																						<asp:label id="lblMemberSince" runat="server"></asp:label></TD>
																				</TR>
																				-->
																				<TR>
																					<TD class="widgetText small" align="left">Last login:</TD>
																					<TD class="widgetText small" align="left" style="padding-right: 10px;">
																						<asp:label id="lblLastLogin" style="padding-left:4px; font-size:10px" runat="server"></asp:label></TD>																				
																					<TD class="widgetText small" align="left">Last update:</TD>
																					<TD class="widgetText small" align="left">
																						<asp:label id="lblLastUpdate" style="padding-left:4px; font-size:10px" runat="server"></asp:label></TD>
																				</TR>																																																												
																			</TABLE>																																																																												
																		</TD>
																	</TR> <!--END ONE ELEMENT--></TABLE>
																	<TABLE cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
																				
																				<TR>
																					<TD class="raveTD" align="center" >
																				     <ajax:ajaxpanel id="Ajaxpanel2" runat="server">
																					    <asp:linkbutton id="btnVote" runat="server" oncommand="btnVote_Click" commandargument="normal"></asp:linkbutton>
                                                                                     </ajax:ajaxpanel>
																			        </TD>
																			        <td>	
																				        <div class="MegaRaveBtn">
																					    <asp:linkbutton id="lbMegaRave" runat="server" oncommand="btnVote_Click" commandargument="megarave"></asp:linkbutton>
																				        </div>
																			        </td>
																				</TR>
																				<!--
																				<TR>
																					<TD class="widgetText9" align="center">
																						<asp:label id="lblVotes" runat="server" text="0"></asp:label></TD>
																				</TR>
																				-->
																	</TABLE>
															</DIV>
															<DIV class="float" id="controlSection3" style="PADDING-BOTTOM: 0px; VERTICAL-ALIGN: top; HEIGHT: 60px"
																runat="server">
																<TABLE height="100%" cellSpacing="0" cellPadding="0" width="280" align="center" border="0">
																	<TR>
																		<TD vAlign="top" height="60">
																			<TABLE cellSpacing="0" cellPadding="0" width="280" align="left" border="0">
																				<TR>
																					<TD vAlign="bottom" align="center" >
																					    <div class="addToFriend">
																						<asp:linkbutton id="btnAddFriend" runat="server">
																							</asp:linkbutton></div></TD>
																					
																					<TD vAlign="bottom" align="center">
																					<div class="SendMsg">
																						<asp:linkbutton id="btnSendMessage" runat="server">
																							</asp:linkbutton></div></TD>
																					
																					<TD vAlign="bottom" align="center">
																					<div class="MeetMe3D">
																						<asp:linkbutton id="btnMeetMe3D" runat="server">
																							</asp:linkbutton></div></TD>
																					
																					<TD vAlign="bottom" align="center">
																					<div class="TellOthers">
																						<asp:linkbutton id="btnTellOthers" onclick="btnTellOthers_Click" runat="server">
																							</asp:linkbutton></div></TD>
																					
																					<TD vAlign="bottom" align="center">
																					<div class="ReportAbuse">
																						<asp:linkbutton id="btnReportAbuse" onclick="btnReportAbuse_Click" runat="server">
																							</asp:linkbutton></div></TD>
																					
																					<TD class="blockTD" vAlign="bottom" align="center">
																					
																						<asp:linkbutton id="btnBlockUser" onclick="btnBlockUser_Click" runat="server">
																							</asp:linkbutton></TD>
																				</TR>
																			</TABLE>
																		</TD>
																	</TR>
																</TABLE>
															</DIV>
														</DIV>
													</TD>
												</TR>
												<TR>
													<TD>
														<DIV class="widgetTextBold" id="divUrl" style="PADDING-LEFT: 10px" align="left" runat="server">
															<asp:hyperlink class="widgetLinkBt" id="hlPerm" runat="server"></asp:hyperlink></DIV>
													</TD>
												</TR>
											</TABLE>
										</DIV>
										<B class="innerFrame"><B class="innerFrame5"></B><B class="innerFrame4"></B><B class="innerFrame3">
											</B><B class="innerFrame2"><B></B></B><B class="innerFrame1"><B></B></B></B>
									</DIV>
								</TD>
							</TR>
						</TABLE>
					</DIV>
					<B class="outerFrame"><!-- style="display:none;" --><B class="outerFrame5"></B><B class="outerFrame4"></B><B class="outerFrame3"></B><B class="outerFrame2"><B></B></B><B class="outerFrame1"><B></B></B></B></DIV>
			</TD>
		</TR> <!---END CHANNEL LIST---></TABLE>
	<BR>
</ajax:ajaxpanel>
