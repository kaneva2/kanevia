///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

namespace KlausEnt.KEP.Kaneva
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	using KlausEnt.KEP.Kaneva.usercontrols;

	/// <summary>
	///		Summary description for NavAccount.
	/// </summary>
	public class NavAccount : BaseUserControl
	{

		private void Page_Load(object sender, System.EventArgs e)
		{
			if (!IsPostBack)
			{
				//BindTabs();
			}
		
			aGeneral.HRef = ResolveUrl ("~/myKaneva/general.aspx");
			aPreferences.HRef = ResolveUrl ("~/myKaneva/settings.aspx");
			aBilling.HRef = ResolveUrl ("~/myKaneva/billingInfoSummary.aspx");
            aTransactions.HRef = ResolveUrl ("~/myKaneva/transactions.aspx");
            aInventory.HRef = ResolveUrl ("~/myKaneva/inventory.aspx");

			if (!KanevaGlobals.EnableCheckout)
			{
				if (!UsersUtility.IsUserAdministrator ())
				{
					navBilling.Visible = false;
				}
			}
		}
		
		public void BindTabs()
		{
			string css_selected = "selected";

			// Set selected tab
			if ( ActiveTab.Equals(TAB.GENERAL) )
			{
				navGeneral.Attributes.Add("class",css_selected);
			}
			else if ( ActiveTab.Equals(TAB.PREFERENCES) )
			{
				navPreferences.Attributes.Add("class",css_selected);
			}
			else if ( ActiveTab.Equals(TAB.CREDITS) )
			{
				navBilling.Attributes.Add("class",css_selected);
			}
            else if (ActiveTab.Equals (TAB.TRANSACTIONS))
            {
                navTransactions.Attributes.Add ("class", css_selected);
            }
            else if (ActiveTab.Equals (TAB.INVENTORY))
            {
                navInventory.Attributes.Add ("class", css_selected);
            }
        }

		/// <summary>
		/// The SubTab to display
		/// </summary>
		public TAB ActiveTab
		{
			get 
			{
				return m_ActiveTab;
			} 
			set
			{
				m_ActiveTab = value;
			}
		}

		/// <summary>
		/// this really should be stored in channelNav
		/// </summary>
		public enum TAB
		{
			GENERAL,
			PREFERENCES,
			STATS,
			CREDITS,
            TRANSACTIONS,
            INVENTORY,
			NONE
		}

		
		protected TAB m_ActiveTab = TAB.GENERAL;

		protected HtmlAnchor			aGeneral, aPreferences, aStats, aBilling, aTransactions, aInventory;
		protected HtmlGenericControl	navGeneral, navPreferences, navBilling, navTransactions, navInventory;

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
