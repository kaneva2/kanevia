<%@ Control Language="c#" AutoEventWireup="false" Codebehind="Footer.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.Footer" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<br/>
<!--<table runat="server" Visible="False" id="tblGoogleAdds" cellpadding="0" cellspacing="0" border="0" width="905">
	<tr>
	<td align="center" style="padding-bottom:5px">
		<script type="text/javascript"><!--
			google_ad_client = "pub-7044624796952740"; google_ad_width = 728; google_ad_height = 90; google_ad_format = "728x90_as"; google_ad_type = "text"; google_ad_channel ="7248390549";google_color_border = "FFFFFF";google_color_bg = "FFFFFF";google_color_link = "018AAA";google_color_text = "737373";google_color_url = "018AAA"; <asp:Literal runat="server" id="litAdTest"/>; //--><!--</script> 
			<script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
			</script> 
		</td>
	</tr>
</table>-->

<div id="footer">
    
	<table id="footerNotAuth" runat="server" cellpadding="0" cellspacing="0" border="0" >
		<tr>
			<td align="center">
				<table border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td align="center" valign="middle" nowrap><a href="http://blog.kaneva.com?page_id=12" class="footerLink" href="~/">About</a> </td>
						<td valign="middle" align="center" width="14" class="footerText">&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;</td>
						<td align="center" valign="middle" nowrap><a runat="server" class="footerLink" href="~/community/install3d.kaneva">Download</a> </td>
						<td valign="middle" align="center" width="14" class="footerText">&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;</td>
						<td align="center" valign="middle" nowrap><a id="A1" class="footerLink" runat="server" href="~/community/safety.kaneva">Safety</a></td>  
						<td valign="middle" align="center" width="14" class="footerText">&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;</td>
						<td align="center" valign="middle" nowrap><a id="A2" class="footerLink" runat="server" href="~/community/Careers.kaneva">Careers</a></td>  
						<td valign="middle" align="center" width="14" class="footerText">&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;</td>
						<td align="center" valign="middle" nowrap><a class="footerLink" href="http://developer.kaneva.com">Developers</a></td>   
						<td valign="middle" align="center" width="14" class="footerText">&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;</td>
						<td align="center" valign="middle" nowrap><a class="footerLink" runat="server" id="aHelp" href="http://support.kaneva.com/ics/support/default.asp?deptID=5433">Help</a></td>  
					</tr>
				</table>
			</td>
		</tr>
		<tr><td><img id="Img3" src="~/images/spacer.gif" border="0" runat="server" height="10"/></td></tr>
		<tr>
			<td align="center">
				<table border="0" cellpadding="0" cellspacing="0">
					<tr><td colspan="13" align="center" class="footerText">� 2006-<%= DateTime.Now.ToString("yyyy") %> Kaneva</td></tr>
				</table>
				<span style="visibility:hidden;"><asp:Label id="lblVersion" runat="server"/>&nbsp;<asp:Label id="lblLoadTime" runat="server"/></span>		
			</td>
		</tr>
	</table>

	<div class="footerAuth" id="footerAuth" runat="server">    

	<div>
	
	<div class="footerSection">
		<div class="footerSecTitle">You</div>
		<div><a runat="server" href="~/mykaneva/buycredits.aspx">Buy Credits</a></div>
		<div><a runat="server" href="http://shop.kaneva.com/MySales.aspx">My Store</a></div>
		<div><a runat="server" href="~/myKaneva/transactions.aspx">My Transactions</a></div>
		<div><a runat="server" href="~/myKaneva/inventory.aspx">My Inventory</a></div>
		<div><a runat="server" href="~/mykaneva/upload.aspx">Upload Media</a></div>
		<div><a runat="server" href="~/3d-virtual-world/world-of-kaneva-fame.kaneva">Earn Fame</a></div>
	</div>

	<div class="footerSection">
		<div class="footerSecTitle">Explore</div>
		<div><a runat="server" href="http://shop.kaneva.com/">Shop</a></div>
		<div><a runat="server" href="~/watch/watch.kaneva?type=2">Videos</a></div>
		<div><a runat="server" href="~/watch/watch.kaneva?type=5">Photos</a></div>
		<div><a runat="server" href="~/watch/watch.kaneva?type=1">Games</a></div>
        <div><a runat="server" href="~/community/channel.kaneva?3dapps=">Worlds</a></div>
	</div>

	<div class="footerSection">
		<div class="footerSecTitle">Help</div>
		<div><a runat="server" href="http://forums.kaneva.com/">Forums</a></div>
		<div><a runat="server" id="aHelpAuth" href="http://support.kaneva.com/ics/support/default.asp?deptID=5433">Support</a></div>
		<div><a runat="server" href="~/share/default.aspx">Spread the Word</a></div>
		<div><a runat="server" href="~/sitemap.aspx">Site Map</a></div>
		<div><a id="A8" runat="server" href="http://blog.kaneva.com/?page_id=10">Contact Us</a></div>
	</div>

	<div class="footerSection last">
		<div class="footerSecTitle">Do More</div>
		<div><a runat="server" href="~/community/install3d.kaneva">Download 3D Kaneva</a></div>
		<div><a runat="server" href="http://developer.kaneva.com">Developers</a></div>
	</div>

	</div>

	<div class="footerSectionHorizontal">
		<span class="footerSecTitle">Learn About Us:</span>
		<div><a runat="server" href="http://blog.kaneva.com/?page_id=2">About Kaneva</a><span>|</span><a runat="server" href="http://blog.kaneva.com">Blog</a><span>|</span><a runat="server" href="~/community/safety.kaneva">Parental Resources</a><span>|</span><a runat="server" href="~/overview/privacy.aspx">Privacy</a><span>|</span><a runat="server" href="~/overview/copyright.aspx">Copyright</a><span>|</span><a runat="server" href="~/community/Careers.kaneva">Jobs</a></div>
	</div>

	<div class="footerVersion">� 2006-<%= DateTime.Now.ToString("yyyy") %> Kaneva<br />
		<span style="visibility:hidden;"><asp:Label id="lblVersionAuth" runat="server"/>&nbsp;<asp:Label id="lblLoadTimeAuth" runat="server"/></span></div>
	
	</div>

</div>

