///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

namespace KlausEnt.KEP.Kaneva
{
	using System;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

	/// <summary>
	///		Summary description for BlogFilter.
	/// </summary>
	public class BlogFilter : System.Web.UI.UserControl
	{

		private void Page_Load(object sender, System.EventArgs e)
		{
			if (!IsPostBack)
			{
				SetupFilter ();
			}

			drpCategory.Enabled = ShowCategory;
		}

		private void SetupFilter ()
		{
			// Category
			if (ShowCategory)
			{
                //drpCategory.DataTextField = "name";
                //drpCategory.DataValueField = "blog_category_id";
                //drpCategory.DataSource = BlogUtility.GetBlogCategories ();
                //drpCategory.DataBind ();
				drpCategory.Items.Insert (0, new ListItem ("category...", ""));
				drpCategory.Items.Insert (1, new ListItem ("----------", " "));
			}
			else
			{
				drpCategory.Items.Insert (0, new ListItem ("category...", ""));
				drpCategory.Items.Insert (1, new ListItem ("----------", " "));
			}

			// SubCategory

			// Date
			DateTime dtCurrent = KanevaGlobals.GetCurrentDateTime ();
			drpMonth.Items.Add (new ListItem ("month...", ""));
			drpMonth.Items.Add (new ListItem ("----------", " "));
			drpMonth.Items.Add (new ListItem (GetSearchDate (dtCurrent, 0), "0"));
			drpMonth.Items.Add (new ListItem (GetSearchDate (dtCurrent, 1), "1"));
			drpMonth.Items.Add (new ListItem (GetSearchDate (dtCurrent, 2), "2"));
			drpMonth.Items.Add (new ListItem (GetSearchDate (dtCurrent, 3), "3"));
			drpMonth.Items.Add (new ListItem (GetSearchDate (dtCurrent, 4), "4"));
			drpMonth.Items.Add (new ListItem (GetSearchDate (dtCurrent, 5), "5"));
			drpMonth.Items.Add (new ListItem (GetSearchDate (dtCurrent, 6), "6"));
			drpMonth.Items.Add (new ListItem (GetSearchDate (dtCurrent, 7), "7"));
			drpMonth.Items.Add (new ListItem (GetSearchDate (dtCurrent, 8), "8"));
			drpMonth.Items.Add (new ListItem (GetSearchDate (dtCurrent, 9), "9"));
			drpMonth.Items.Add (new ListItem (GetSearchDate (dtCurrent, 10), "10"));
			drpMonth.Items.Add (new ListItem (GetSearchDate (dtCurrent, 11), "11"));
			drpMonth.Items.Add (new ListItem (GetSearchDate (dtCurrent, 12), "12"));

			// View
			drpView.Items.Add (new ListItem ("view...", ""));
			drpView.Items.Add (new ListItem ("----------", " "));
			drpView.Items.Add (new ListItem ("line items", "li"));
			drpView.Items.Add (new ListItem ("small thumbs", "st"));
			
			// Pages
			// *********** NOTE *************************************************************************
			// IF YOU CHANGE THESE, MAKE SURE YOU HAVE ONE EQUAL TO THE CURRENT SETTING OF BlogsPerPage 
			// ****************************************************************************************** 
			drpPages.Items.Add (new ListItem ("3", "3"));
			drpPages.Items.Add (new ListItem ("5", "5"));
			drpPages.Items.Add (new ListItem ("10", "10"));
			drpPages.Items.Add (new ListItem ("20", "20"));
			drpPages.Items.Add (new ListItem ("30", "30"));
			drpPages.Items.Add (new ListItem ("50", "50"));
			drpPages.Items.Add (new ListItem ("100", "100"));

			try
			{
				drpPages.SelectedValue = ItemsPerPages.ToString ();
			}
			catch (Exception)
			{}
		}

		private string GetSearchDate (DateTime dtDate,int monthDifference)
		{
			System.Globalization.GregorianCalendar cal = new System.Globalization.GregorianCalendar ();
			return cal.AddMonths (dtDate, -monthDifference).ToString ("MM/yy");
		}

		/// <summary>
		/// Change Category
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void drpCategory_Change (Object sender, EventArgs e)
		{
			drpMonth.SelectedIndex = 0;
			drpTitle.SelectedIndex = 0;
			CurrentSort = DEFAULT_BLOG_SORT;

			if (drpCategory.SelectedValue.Trim ().Length > 0)
			{
				CurrentFilter = ""; // t.blog_category_id = " + Convert.ToInt32 (Server.HtmlEncode (drpCategory.SelectedValue));
			}
			else
			{
				CurrentFilter = "";
			}

			FilterChanged (this, new FilterChangedEventArgs (CurrentFilter, CurrentSort));
		}

		/// <summary>
		/// Change Month
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void drpMonth_Change (Object sender, EventArgs e)
		{
			drpCategory.SelectedIndex = 0;
			drpTitle.SelectedIndex = 0;
			CurrentSort = DEFAULT_BLOG_SORT;

			if (drpMonth.SelectedValue.Trim ().Length > 0)
			{
				DatabaseUtility dbUtility = KanevaGlobals.GetDatabaseUtility ();
				DateTime dtCurrent = KanevaGlobals.GetCurrentDateTime ();
				System.Globalization.GregorianCalendar cal = new System.Globalization.GregorianCalendar ();
				DateTime dtSearch = cal.AddMonths (dtCurrent, -Convert.ToInt32 (drpMonth.SelectedValue));

				int iDaysInMonth = DateTime.DaysInMonth (dtSearch.Year, dtSearch.Month);
				string startDate = dbUtility.FormatDate (new DateTime (dtSearch.Year, dtSearch.Month, 1, 0, 0, 0));
				string endDate = dbUtility.FormatDate (new DateTime (dtSearch.Year, dtSearch.Month, iDaysInMonth, 23, 59, 59));

				CurrentFilter = " t.created_date >= '" + startDate + "' AND t.created_date <= '" + endDate + "'";
			}
			else
			{
				CurrentFilter = "";
			}

			FilterChanged (this, new FilterChangedEventArgs (CurrentFilter, CurrentSort));
		}

		/// <summary>
		/// Change Title Sort
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>

		protected void drpTitle_Change (Object sender, EventArgs e)
		{
			drpCategory.SelectedIndex = 0;
			drpMonth.SelectedIndex = 0;

			if (drpTitle.SelectedValue.Trim ().Length > 0)
			{
				// Make sure it is one of the valid letter to protect against SQL injection
				string filter = drpTitle.SelectedValue.Substring (0,1);
				System.Text.RegularExpressions.Regex regexSingleLetter = new System.Text.RegularExpressions.Regex ("[a-zA-Z]");
				if (regexSingleLetter.IsMatch (filter))
				{
					CurrentFilter = " t.subject like '" + Server.HtmlEncode (filter) + "%'";
				}
			}
			else
			{
				CurrentFilter = "";
			}

			FilterChanged (this, new FilterChangedEventArgs (CurrentFilter, CurrentSort));
		}


		/// <summary>
		/// Change View
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void drpView_Change (Object sender, EventArgs e)
		{
			CurrentSort = DEFAULT_BLOG_SORT;

			FilterChanged (this, new FilterChangedEventArgs (CurrentFilter, CurrentSort));
		}

		/// <summary>
		/// Change Pages
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void drpPages_Change (Object sender, EventArgs e)
		{
			FilterChanged (this, new FilterChangedEventArgs (CurrentFilter, CurrentSort));
		}

		/// <summary>
		/// The current filter
		/// </summary>
		public string CurrentFilter
		{
			get 
			{
				if (ViewState ["filter"] != null)
				{
					return ViewState ["filter"].ToString ();
				}
				else
				{
					return "";
				}
			} 
			set
			{
				ViewState ["filter"] = value;
			}
		}

		/// <summary>
		/// The current Sort
		/// </summary>
		public string CurrentSort
		{
			get 
			{
				if (ViewState ["sort"] != null)
				{
					return ViewState ["sort"].ToString ();
				}
				else
				{
					return DEFAULT_BLOG_SORT;
				}
			} 
			set
			{
				ViewState ["sort"] = value;
			}
		}


		/// <summary>
		/// The current Number of Items to show per Pages
		/// </summary>
		public int ItemsPerPages
		{
			get 
			{
				if (drpPages.SelectedValue.Length > 0 && IsPostBack)
				{
					Response.Cookies ["bipp"].Value = drpPages.SelectedValue;
					Response.Cookies ["bipp"].Expires = DateTime.Now.AddYears (1);
					return Convert.ToInt32 (drpPages.SelectedValue);
				}
				else
				{
					// Read it from the cookie?
					if (Request.Cookies ["bipp"] == null)
					{
						return KanevaGlobals.BlogsPerPage;
					}
					else
					{
						return Convert.ToInt32 (Request.Cookies ["bipp"].Value);
					}
				}
			} 
		}

		/// <summary>
		/// The current BlogCategory
		/// </summary>
		public int BlogCategory
		{
			get 
			{
				if (ViewState ["BlogCategory"] != null)
				{
					return Convert.ToInt32 (ViewState ["BlogCategory"]);
				}
				else
				{
					return (int) Constants.eBLOG_CATEGORY.ALL;
				}
			} 
			set
			{
				ViewState ["BlogCategory"] = value;
			}
		}

		/// <summary>
		/// The current ShowCategory
		/// </summary>
		public bool ShowCategory
		{
			get 
			{
				if (ViewState ["ShowCategory"] != null)
				{
					return Convert.ToBoolean (ViewState ["ShowCategory"]);
				}
				else
				{
					return false;
				}
			} 
			set
			{
				ViewState ["ShowCategory"] = value;
			}
		}

		public event FilterChangedEventHandler FilterChanged;

		protected DropDownList drpCategory, drpMonth, drpTitle, drpView, drpPages;

		private const string DEFAULT_BLOG_SORT = "b.created_date desc";

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
