<%@ Control Language="c#" AutoEventWireup="false" Codebehind="Header.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.Header" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Register TagPrefix="Kaneva" Namespace="KlausEnt.KEP.Kaneva.WebControls" Assembly="Kaneva.WebControls" %>
<%@ Register TagPrefix="Kaneva" TagName="LoginForm" Src="~/usercontrols/LoginForm.ascx" %>
<%@ Register TagPrefix="UserControl" TagName="FacebookJS" Src="~/usercontrols/FacebookJavascriptInclude.ascx" %>
<%@ Register TagPrefix="UserControl" TagName="LightBox" Src="~/usercontrols/LightBox.ascx" %>

<asp:literal id="litCSS" runat="server"></asp:literal>
<asp:literal id="litJS" runat="server"></asp:literal>
<asp:literal id="litRaveVars" runat="server"></asp:literal>

<usercontrol:FacebookJS ID="FacebookJScript" runat="server"></usercontrol:FacebookJS>

<span id="spnAuthenticated" runat="server" visible="false">
<script type="text/javascript">
	try {
		function GetFortune() { __doPostBack("GetUserFortune", ""); }
	} catch (err) { }

	var $j = jQuery.noConflict();
	function ToggleDD(menu, btn) {
		menu.toggle();
		btn.toggleClass("selected", menu.is(":visible"));
	}

	$j(document).ready(function() {
		$j("html").click(function () {
			if ($j("#profiledd").is(":visible")) {
				$j("#profiledd").toggle();
				$j("#profile_selector").toggleClass("selected");
			}
			if ($j("#fortunedd").is(":visible")) {
				$j("#fortunedd").toggle();
				$j("#fortune_selector").toggleClass("selected");
			}
		});

		$j("#profiledd").click(function (e) {
			e.stopPropagation();
		});
		$j("#profile_selector").click(function (e) {
			e.stopPropagation();
			ToggleDD($j('#profiledd'), $j('#profile_selector'));
		});

		$j("#fortune_selector").click(function (e) {
			e.stopPropagation();
			ToggleDD($j('#fortunedd'), $j('#fortune_selector'));
		});
	});
</script>
<div class="nav_wrapper">
	<ul class="menu_wrapper">
		<li class="topmenu">
			<ul>
				<li class="logo"><a href="~/default.aspx" runat="server"></a></li>
				<li class="fortune"><div><a href="#" id="fortune_selector"></a></div>
					<div id="fortunedd" class="fortunedd_container">
						<ul>
							<li class="fortune_header">Your Balance</li>
							<li class="credits">Credits: <span id="spnCreditsCount_Header" runat="server" clientidmode="static">0</span> <a href="~/mykaneva/buyCreditPackages.aspx" runat="server"></a></li>
							<li class="horiz_divide"></li>
							<li class="rewards">Rewards: <span id="spnRewardsCount_Header" runat="server" clientidmode="static">0</span></li>
						</ul>
					</div>	
				</li>
				<li class="inbox"><a href="~/mykaneva/mailbox.aspx" runat="server"><div class="newmsg" id="divMsgCount" runat="server">0</div></a></li>
				<li class="world_inbox"><a id="A2" href="~/mykaneva/messagecenter.aspx?mc=ar" runat="server"><div class="newmsg" id="divWorldReqCount" runat="server">0</div></a></li>
			</ul>
		</li>
		<li class="botmenu">
			<ul class="menu_text">
				<li class="explore"><a href="~/community/channel.kaneva?3dapps=" runat="server" id="aFind3DApps">Explore Worlds</a></li>
                <li class="shop"><a href="" runat="server" id="aShop">Shop Now</a></li>
				<li class="create"><a href="~/community/StartWorld.aspx" runat="server" id="aMake3DApp">Create Your World</a></li>
				<li class="profile"><a href="javascript:void(0);" id="profile_selector" class="unselected"><img id="Img3" runat="server" src="~/images/header/down_arrow.gif" /></a>
						
				</li>
				<li class="home"><a href="~/default.aspx" runat="server" id="a7">Home</a></li>
				<li class="divide">
					<ul id="profiledd" runat="server" clientidmode="static">
						<li><a href="~/community/ProfilePage.aspx" runat="server" id="aMyProfile">My Profile</a></li>
						<li><a href="~/mykaneva/messagecenter.aspx" runat="server" id="a1">Messages</a></li>
						<li class="horiz_divide"></li>
						<li><a href="~/mykaneva/my3dapps.aspx" runat="server">My Worlds</a></li>
						<li><a href="~/community/channel.kaneva?3dapps=" runat="server">Explore Worlds</a></li>
						<li><a href="~/community/StartWorld.aspx" runat="server">Create World</a></li>
						<li class="horiz_divide"></li>
						<li><a href="~/mykaneva/inviteFriend.aspx" runat="server">Find/Invite Friends</a></li>
						<li><a href="~/people/people.kaneva" runat="server">Search People</a></li>
						<li class="horiz_divide"></li>
						<li><a href="" id="aShopDD" runat="server">Shopping</a></li>
						<li><a href="~/mykaneva/buyCreditPackages.aspx" runat="server">Get Credits</a></li>
						<li><a href="~/mykaneva/contests.aspx" runat="server">Contests</a></li>
						<li class="horiz_divide"></li>
						<li><a href="~/mykaneva/general.aspx" runat="server">Account Settings</a></li>
						<li class="horiz_divide"></li>
                        <asp:literal id="litAdminLinks" runat="server" />
                        <li><asp:linkbutton id="lnkLogout" onclick="lnkLogout_Click" runat="server" text="Log Out" causesvalidation="false"></asp:linkbutton></li>
					</ul>
				</li>
				<li class="user"><a href="~/community/ProfilePage.aspx" runat="server" id="aMyProfile2">Hi, <span id="spnUsername" runat="server"></span></a></li>
			</ul>
		</li>
		<li class="subnav"><asp:placeholder runat="server" id="phNavLvl3" /></li>
		<li class="validate" id="validateContainer" runat="server">
			<asp:updatepanel id="upResend" runat="server" childrenastriggers="true" updatemode="conditional">
			<contenttemplate>
			<p id="msg" runat="server"></p>
			<div class="resendcontainer"><span class="resendspan" id="spnResend" runat="server"><asp:linkbutton id="btnResend" onclick="btnResend_Click" runat="server" CssClass="btnlarge black resend" text="Resend Confirmation" /></span><a class="email" href="~/mykaneva/general.aspx" runat="server">Update Email Address</a></div>
			</contenttemplate>
			</asp:updatepanel>
		</li>
	</ul>
</div>
<div id="divFlashSSO" runat="server" visible="false">
	<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="1" height="1" id="kaneva_sinfo" align="middle">
		<param name="movie" value="<%:FlashSSOPath %>" />
		<param name="quality" value="high" />
		<param name="bgcolor" value="#ffffff" />
		<param name="play" value="true" />
		<param name="loop" value="false" />
		<param name="wmode" value="transparent" />
		<param name="scale" value="showall" />
		<param name="menu" value="true" />
		<param name="devicefont" value="false" />
		<param name="salign" value="" />
		<param name="allowScriptAccess" value="sameDomain" />
        <asp:Literal ID="litFlashSSOVars" runat="server" />
		<!--[if !IE]>-->
		<object type="application/x-shockwave-flash" data="<%:FlashSSOPath %>" width="1" height="1">
			<param name="movie" value="<%:FlashSSOPath %>" />
			<param name="quality" value="high" />
			<param name="bgcolor" value="#ffffff" />
			<param name="play" value="true" />
			<param name="loop" value="false" />
			<param name="wmode" value="transparent" />
			<param name="scale" value="showall" />
			<param name="menu" value="true" />
			<param name="devicefont" value="false" />
			<param name="salign" value="" />
			<param name="allowScriptAccess" value="sameDomain" />
            <asp:Literal ID="litFlashSSOVarsIE" runat="server" />
		</object>
		<!--<![endif]-->
	</object>
</div>
</span>

<span id="spnNotAuthenticated" runat="server" visible="false">
	<div class="nav_wrapper_noauth">
		<div class="pageHeader">
			<div class="logo"><a href="~/default.aspx" runat="server" id="aLogoLink"></a></div>  
			<Kaneva:LoginForm runat="server" id="loginForm"></Kaneva:LoginForm>  
			<div class="headerLinks">
				<a id="aJoinToday" runat="server" href="">Join Today</a>&nbsp;|&nbsp;<span class="showSignIn">Sign In</span>
			</div>
			<div id="fb-root"></div>
		</div>
	</div>
</span>
<usercontrol:lightbox runat="server" id="ucLightBox" />

<style type="text/css"><asp:literal id="litStyle" runat="server"></asp:literal></style>

<table cellpadding="0" cellspacing="0" border="0" id="navContainer">
	<tr>
		<td align="center">
			<asp:literal id="litXtraJS" runat="server"></asp:literal>
