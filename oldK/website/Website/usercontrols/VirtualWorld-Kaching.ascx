<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VirtualWorld-Kaching.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.usercontrols.VirtualWorld_Kaching" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>

<div id="divAbout" visible="false" runat="Server">
    <link rel="stylesheet" type="text/css" href="../css/kaching.css" />
    <div class="mainContent" >    
          <div class="kaMastHead">

            <div id="flash1"><asp:ImageButton ImageUrl="../images/vw-ka-ching/ka_mastHead.jpg"
                     AlternateText="Ka-ching! Collect Dodge Shoot" runat="server" OnClick="PlayFlash" /></div>
            
            <script type="text/javascript">
            var so = new SWFObject("http://images.kaneva.com/media/communities/kaneva/virtualworld/ka-ching/swf/kaLogoAnimation.swf", "MediaPlayer", "717 ", "210", "9", "#ffffff"); 
            so.addParam("quality", "high"); 
            <asp:literal id="litFlashWrite" runat="server" />
            </script>
            
          </div>
          
          <div class="kaNav">
            <ul>
            <li><a href="kaneva-ka-ching.kaneva" class="kaSelected">About Ka-ching!</a></li>
            <li class="kaSeperator">&nbsp;<!-- separator --></li>
            <li><a href="play-ka-ching.kaneva" class="">How to Play</a></li>
            <li class="kaSeperator">&nbsp;<!-- separator --></li>
            <li><a href="ka-ching-leaderboard.kaneva" class="">Advanced Leaderboard</a></li>
            </ul>
          </div>
          
          <div class="kaContent">
            <div class="kaPlayer">
            <div class="kaTextCol">
            <div class="kaSlideShow">
            <div id="flash2"><img src="../images/vw-ka-ching/ka_fpo_flashPocket.jpg" alt="A screen shot of Ka-Ching in action!" /></div>
            <script type="text/javascript">
            var so = new SWFObject("../flash/kaching/kaChingGallery.swf", "MediaPlayer", "366 ", "211", "9", "#ffffff"); 
            so.addParam("quality", "high"); 
            so.write("flash2");
            </script>
            </div>


            <h2>Ka-ching! is a FREE multi-player,<br /> 
            third person, 3D shooting game played exclusively inside the 3D World of Kaneva.</h2>
            <h2>The Goal</h2>
            <p>Team up with your friends to find, collect, and deposit the most coins (Ka-ching!), dodge foam darts, and check your opponents (send them back to the starting point) by firing foam darts.</p> 
            <h2>The Teams</h2>
            <p>Up to 8 players are divided into two teams-red and blue. Assemble a team with your Kaneva friends, or join one in progress.
            <br />
            (Note: There must be at least three players on each team to start a game.)</p>
            <h2>The Game</h2>
            <p>From the Ka-ching! lobby, walk up to the Arena Manager (the Avatar behind the counter) and press the TAB key.</p>
            <ul>
            <li>To join a game in progress: Click <span class="bold">Join Game</span>.</li>
            <li>To assemble your own team: Click Create a <span class="bold">New Game</span>. (If you assemble your own team, choose either The Office or The Octagon arena.) </li>
            </ul>
            <p>The game starts once all players have clicked <span class="bold">Ready.</span></p>
            <p>&nbsp;</p>
            </div>
           
            <div class="ka_ml_wrapper"> <ajax:ajaxpanel id="ajpSearch1" runat="server">           
            <div class="ka_ml_head">
                         
            <ul>
            <li class="playerName">NICKNAME</li>
            <li class="ka_ml_seperator">&nbsp;</li>
            <li class="frag"><asp:linkbutton ID="lbSortRatioB" runat="server" Text="Best Ratio" OnClick="SortRatioClick"></asp:linkbutton></li>
            <li class="ka_ml_seperator">&nbsp;</li>
            <li class="wins"><asp:linkbutton ID="lbSortWinsB" runat="server" Text="Wins" OnClick="SortWinsClick"></asp:linkbutton></li>
            <li class="ka_ml_seperator">&nbsp;</li>
            <li class="coins"><asp:linkbutton ID="lbSortCoinsB" runat="server" Text="Coins" OnClick="SortCoinsClick"></asp:linkbutton></li>
            </ul>
            
            <div class="clear"><!-- clear the floats --></div>
            </div>
            <div class="ka_ml_body">   
                       <ul id="ulErrMsgB" runat="server" visible="false" >
                            <li class="playerName"><asp:Literal ID="litErrMsgB" runat="server"></asp:Literal></li>
                            <li class="frag"></li>
                            <li class="wins"></li>
                            <li class="coins"></li>
                        </ul>                                       
                <asp:Repeater ID="rptLeadersBasic" runat="server">            
                    <ItemTemplate>
                        <ul>
                            <li class="playerName"><a href="<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "name").ToString () )%>"><%# DataBinder.Eval(Container.DataItem, "name") %></a></li>
                            <li class="frag"><%# DataBinder.Eval(Container.DataItem, "ratio") %></li>
                            <li class="wins"><%# DataBinder.Eval(Container.DataItem, "wins") %></li>
                            <li class="coins"><%# DataBinder.Eval(Container.DataItem, "coins") %></li>
                        </ul>                                            
                    </ItemTemplate>                        
                </asp:Repeater>
            
                      
            <div class="clear"><!-- clear the floats --></div>
            <div class="ka_ml_foot"><a href="ka-ching-leaderboard.kaneva"><img src="../images/vw-ka-ching/ka_ml_clickTo.jpg" alt="Click here to view the Ka-ching! Advanced Leaderboard" id="ka_ml_clickTo" onmouseover="return setImage(this.id, 'on')" onmouseout="return setImage(this.id, 'off')" /></a></div>
            
            </div>       </ajax:ajaxpanel>      
            </div>
             
            <div class="clear"><!-- clear the floats --></div>
            </div>
          </div>
    </div>
</div>

<div id="divPlay" visible="false" runat="server">
    <link rel="stylesheet" type="text/css" href="../css/kaching.css" />
    <div class="mainContent">
        <div class="kaMastHead">
          <div><img src="../images/vw-ka-ching/ka_mastHead.jpg" alt="Ka-ching! Collect Dodge Shoot" /></div>
        </div>
        <div class="kaNav">
          <ul>
            <li><a href="kaneva-ka-ching.kaneva" class="">About Ka-ching!</a></li>
            <li class="kaSeperator">&nbsp;</li>
            <li><a href="play-ka-ching.kaneva" class="kaSelected">How to Play</a></li>
            <li class="kaSeperator">&nbsp;</li>
            <li><a href="ka-ching-leaderboard.kaneva" class="">Advanced Leaderboard</a></li>
          </ul>
        </div>
        <div class="kaContent">
          <div class="kaP2_top">
            <h2>How to play</h2>
            <div class="kaIcon"><img src="../images/vw-ka-ching/ka_icon_collect.png" alt="Collect" /></div>
		    <div class="kaPlayTxt"><span class="bold">Collect:</span> Move throughout the arena and collect coins. Once you have three or more coins, you can make your way to the Drop Zone&mdash;where you can deposit coins to be added to your team's total. The team who deposits 50 coins first, wins. Ka-ching! </div>
            <div class="clear"><!-- Clear the floats --></div>
            <div class="kaIcon"><img src="../images/vw-ka-ching/ka_icon_dodge.png" alt="Dodge" /></div>
            <div class="kaPlayTxt"><span class="bold">Dodge:</span> Watch out! Once you have three coins, you become a moving target for your opponents. Dodge foam darts fired by the opposition. If you get hit, your energy goes down. When your energy is gone, you're bounced back to the starting point and you lose all of your coins.</div>
            <div class="clear"><!-- Clear the floats --></div>
            <div class="kaIcon"><img src="../images/vw-ka-ching/ka_icon_shoot.png" alt="Shoot" /></div>
		    <div class="kaPlayTxt"><span class="bold">Shoot:</span> Aim and fire foam darts at your opponents to keep them from collecting and depositing coins. Look for other weapons throughout the arena. They'll do more damage and help you check your opponents faster&mdash;sending them back to the starting point.</div>
            <div class="clear"><!-- Clear the floats --></div>
            <div class="kaIcon"><img src="../images/vw-ka-ching/ka_icon_check.png" alt="Check" /></div>
            <div class="kaPlayTxt"><span class="bold">Check:</span> When you hit another player, their energy goes down. When their energy is gone, they're bounced back to the starting point and they lose all of their coins. </div>
            <div class="clear"><!-- Clear the floats --></div>
          </div>
          <div><img src="../images/vw-ka-ching/ka_seperatorRed.jpg" alt="Ka-ching!" /></div>
          <div class="kaP2_btm">
            <div class="kaCol1">
              <h2>The Key Controls</h2>
              <ul>
                <li>To move: Use the WASD keys.
                   <ul>
                     <li>W = move forward;</li>
                     <li>S = move backward;</li>
                     <li>A = strafe to the left;</li>
                     <li>D = strafe to the r.ight.</li>
                   </ul></li>
                <li>To change viewpoint and direction: Move your mouse up or down.</li>
                <li>To shoot: Left-click your mouse button.</li>
                <li>To change weapons: Press 1, 2, or 3.</li>
                <li>To reload: Right-click your mouse button.</li>
                <li>To go to the main menu: Press ESC.</li>
              </ul>
              <h2>The Arenas</h2>
              <p>There are two different arenas to test your foam-shooting skills:</p>
              <ul>
                <li>The Office: This fast-paced, action-packed arena is sure to be a stress-reliever. Watch out for the cubicles&mdash;you never know where an opponent is hiding.</li>
                <li>The Octagon: Intense action is lurking around every corner in this post-apocalyptic arena. Use the Jump pads to leap to the upper levels&mdash;find hidden coins and stun your opponents.</li>
              </ul>
              <h2>The Weapons</h2>
              <p>There are three different weapons you can use to check your opponents:</p>
              <ul>
                <li>The Stinger: A rapid-fire, long-range, low-damage weapon.</li>
                <li>The Shotgun: A slow-fire, medium-range, medium-damage weapon.</li>
                <li>The Sucker Punch: A slow-fire, long-range, high-damage weapon.</li>
              </ul>
              <h2>World Fame&mdash;Coming Soon! </h2>
              <!--<p>Play Ka-ching! to earn Kaneva World Fame.  The more you play, the more Rewards and points you earn.
                Check out the Ka-ching! leaderboard to see the top players and watch your fame grow!</p>--></div>
            <div class="kaCol2">
              <div><img src="../images/vw-ka-ching/ka_ss_office.jpg" alt="A Screen Shot of the Office in KaChing!" /></div>
              <div><img src="../images/vw-ka-ching/ka_ss_ruin.jpg" alt="A Screen Shot of the Octagon in KaChing!" /></div>
            </div>
            <div class="clear"><!-- clear the floats --></div>
          </div>
          <div class="kaBlkSep"><img src="../images/vw-ka-ching/ka_seperatorBlk.jpg" alt="Ka-ching!" /></div>
        </div>
  <!-- end main content div -->
  </div>

</div>

<div id="divLeaderboard" visible="false" runat="server">

<div class="mainContent">
    <link rel="stylesheet" type="text/css" href="../css/kaching.css" />
    <div class="kaMastHead">
      <div><img src="../images/vw-ka-ching/ka_mastHead.jpg" alt="Ka-ching! Collect Dodge Shoot" /></div>
    </div>
    <div class="kaNav">
      <ul>
        <li><a href="kaneva-ka-ching.kaneva" class="">About Ka-ching!</a></li>
        <li class="kaSeperator">&nbsp;<!-- separator --></li>
        <li><a href="play-ka-ching.kaneva" class="">How to Play</a></li>
        <li class="kaSeperator">&nbsp;<!-- separator --></li>
        <li><a href="ka-ching-leaderboard.kaneva" class="kaSelected">Advanced Leaderboard</a></li>
      </ul>
    </div>
    <div class="kaContent">
 	  <ajax:AjaxPanel ID="ajp2" runat="server">
 	  <div class="ka_lb">
        <div class="ka_lb_tabs">
          <ul>
            <li id="liToday" runat="server" class=""><asp:linkbutton ID="Linkbutton9" runat="server" Text="Today's Totals" OnClick="TodayClick" /></a></li>
            <li id="li7Days" runat="server"  class=""><asp:linkbutton ID="Linkbutton10" runat="server" Text="Last 7 Days" OnClick="SevenDaysClick" /></a></li>
            <li id="liAllTime" runat="server"  class=""><asp:linkbutton ID="Linkbutton11" runat="server" Text="All Time" OnClick="AllTimeClick" /></a></li>
          </ul>
        </div>
        <div class="ka_lb_head">
          <ul>
            <li class="playerName">NICKNAME</li>
            <li class="ka_lb_seperator">&nbsp;</li>
            <li class="checks"><asp:linkbutton ID="lbSortChecksA" runat="server" Text="CHECKS" OnClick="SortChecksClick" /></li>
            <li class="ka_lb_seperator">&nbsp;</li>
            <li class="bounces"><asp:linkbutton ID="lbSortBouncesA" runat="server" Text="BOUNCES" OnClick="SortBouncesClick" /></li>
            <li class="ka_lb_seperator">&nbsp;</li>
            <li class="frag"><asp:linkbutton ID="lbSortRatioA" runat="server" Text="BEST RATIO" OnClick="SortRatioClick" /></li>
            <li class="ka_lb_seperator">&nbsp;</li>
            <li class="wins"><asp:linkbutton ID="lbSortWinsA" runat="server" Text="WINS" OnClick="SortWinsClick" /></li>
            <li class="ka_lb_seperator">&nbsp;</li>
            <li class="coins"><asp:linkbutton ID="lbSortCoinsA" runat="server" Text="COINS" OnClick="SortCoinsClick" /></li>
          </ul>
        </div>
        <div class="ka_lb_body" id="ka_lb_today">
                <ul id="ulErrMsgA" runat="server" visible="false" >
                    <li class="rank"></li>
                    <li class="playerName"><asp:Literal ID="litErrMsgA" runat="server"></asp:Literal></li>
                    <li class="checks"></li>
                    <li class="bounces"></li>
                    <li class="frag"></li>
                    <li class="wins"></li>
                    <li class="coins"></li>
                </ul>
          
          <asp:Repeater ID="rptLeadersAdv" runat="server">            
                <ItemTemplate>
                    <ul class="<%# HiLiteUser(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "kaneva_user_id"))) %>">
                        <li class="rank"><%# DataBinder.Eval(Container.DataItem, "rank") %></li>
                        <li class="playerName"><a href="<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "name").ToString () )%>"><%# DataBinder.Eval(Container.DataItem, "name") %></a></li>
                        <li class="checks"><%# DataBinder.Eval(Container.DataItem, "kills") %></li>
                        <li class="bounces"><%# DataBinder.Eval(Container.DataItem, "deaths") %></li>
                        <li class="frag"><%# DataBinder.Eval(Container.DataItem, "ratio") %> (<%# DataBinder.Eval(Container.DataItem, "KillsToDeathsRatio").ToString().Substring(0,4)%>)</li>
                        <li class="wins"><%# DataBinder.Eval(Container.DataItem, "wins") %></li>
                        <li class="coins"><%# DataBinder.Eval(Container.DataItem, "coins") %></li>
                    </ul>                                            
                </ItemTemplate>                        
          </asp:Repeater>   
          <asp:Repeater ID="rptUserStats" runat="server">            
                <ItemTemplate>
                    <ul class="ka_lb_me">
                        <li class="rank"></li>
                        <li class="playerName"><a href="<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "name").ToString () )%>"><%# DataBinder.Eval(Container.DataItem, "name") %></a></li>
                        <li class="checks"><%# DataBinder.Eval(Container.DataItem, "kills") %></li>
                        <li class="bounces"><%# DataBinder.Eval(Container.DataItem, "deaths") %></li>
                        <li class="frag"><%# DataBinder.Eval(Container.DataItem, "ratio") %> (<%# DataBinder.Eval(Container.DataItem, "KillsToDeathsRatio").ToString().Substring(0,4)%>)</li>
                        <li class="wins"><%# DataBinder.Eval(Container.DataItem, "wins") %></li>
                        <li class="coins"><%# DataBinder.Eval(Container.DataItem, "coins") %></li>
                    </ul>                                            
                </ItemTemplate>                        
          </asp:Repeater>                                                                           
        </div>
      </div>
      </ajax:AjaxPanel>
    </div>                                
</div>
</div>