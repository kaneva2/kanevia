///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

	/// <summary>
	///		Summary description for Footer.
	/// </summary>
	public class Footer : System.Web.UI.UserControl
	{
		private void Page_Load(object sender, System.EventArgs e)
		{
			lblVersionAuth.Text = lblVersion.Text = KanevaGlobals.Version ();

			// Show correct advertisements
			AdvertisementSettings Settings = (AdvertisementSettings) Application ["Advertisement"];
			if (Settings != null)
			{
				if (Settings.Mode.Equals (AdvertisingMode.Off))
				{
					tblGoogleAdds.Visible = false;
				}
				else
				{
					// Intialize
					bool MatchFound = false;
					bool ShowAd = false;
					bool HasQueryParams = false;

					// Get the relative file path of the current request from the application root
					string RelativeFilePath = Request.Url.AbsolutePath.Remove (0, Request.ApplicationPath.Length).ToLower();
					if (!RelativeFilePath.StartsWith("/"))
					{
						// Add a leading "/"
						RelativeFilePath = "/" + RelativeFilePath;
					}

					// Get the relative directory of the current request by removing the last segment of the RelativeFilePath
					string RelativeDirectory = RelativeFilePath.Substring (0, RelativeFilePath.LastIndexOf('/') + 1);

					// Determine if there is a matching file path for the current request
					int i = Settings.Files.IndexOf (RelativeFilePath);
					if (i >= 0)
					{
						MatchFound = true;
						ShowAd = Settings.Files [i].ShowAd;
						HasQueryParams = Settings.Files [i].QueryParam == "" ? false : true;
					}

					// Try to find a matching directory path, if no file was found
					i = 0;
					while (!MatchFound && i < Settings.Directories.Count)
					{
						if (Settings.Directories[i].Recurse)
						{
							// Match the beginning of the directory if recursion is allowed
							MatchFound = (RelativeDirectory.StartsWith (Settings.Directories [i].Path));
						}
						else
						{
							// Match the entire directory
							MatchFound = (RelativeDirectory == Settings.Directories [i].Path);
						}

						if (MatchFound)
						{
							ShowAd = Settings.Directories [i].ShowAd;
						}
						i++;
					}

					// Test for match for a ShowAd
					if (MatchFound && ShowAd.Equals (false))
					{
						// Test for query string params in url
						if (Request.Url.Query.Length > 0 && HasQueryParams)
						{
							tblGoogleAdds.Visible = true;

							for (int j=0; j<Settings.Files.Count; j++)
							{
								// Test if config setting has query params
								if (Settings.Files [j].QueryParam != string.Empty && Settings.Files [j].QueryParam != string.Empty)
								{
									// Test to see if params match those in querystring
									if (Request.Url.Query.ToLower ().IndexOf (Settings.Files [j].QueryParam + "=" + Settings.Files [j].QueryParamValue) > -1)
									{
										tblGoogleAdds.Visible = false;
										break;
									}
								}
							}
						}
						else
						{
							tblGoogleAdds.Visible = false;
						}
					}
					else
					{
						if (Settings.Mode.Equals (AdvertisingMode.Live))
						{
							litAdTest.Visible = false;
							tblGoogleAdds.Visible = true;
						}
						else if (Settings.Mode.Equals (AdvertisingMode.Testing))
						{
							litAdTest.Text = "google_adtest = \"on\"";
							tblGoogleAdds.Visible = true;
						}
						else
						{
							tblGoogleAdds.Visible = false;
						}
					}
				}
			}
			else
			{
				// No setting turn it off
				tblGoogleAdds.Visible = false;
			}

            aHelpAuth.HRef = aHelp.HRef = Common.GetHelpURL (KanevaWebGlobals.CurrentUser.FirstName, KanevaWebGlobals.CurrentUser.LastName, KanevaWebGlobals.CurrentUser.Email);
            
            if (Request.IsAuthenticated)
            {
                footerAuth.Visible = true;
                footerNotAuth.Visible = false;
            }
            else
            {
                footerAuth.Visible = false;
                footerNotAuth.Visible = true;
            }
        }


        #region Declerations

        protected override void OnPreRender (EventArgs e)
        {
            lblLoadTimeAuth.Text = lblLoadTime.Text;
        }

		protected HtmlAnchor hlMyKaneva;
        protected HtmlAnchor aHelp, aHelpAuth;

        protected Label lblVersion, lblLoadTime;
        protected Label lblVersionAuth, lblLoadTimeAuth;
		protected HtmlTable tblGoogleAdds;

		protected Literal litAdTest;

        protected HtmlTable footerNotAuth;
        protected HtmlContainerControl footerAuth;

        #endregion Declerations

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
