﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AccountCompleteness.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.usercontrols.AccountCompleteness" %>

<style type="text/css">

.containerbox {width:100%;height:162px;border:1px solid #ccc;margin:8px auto 16px auto;
-webkit-box-shadow: 0px 0px 8px 3px #dedede;
box-shadow: 0px 0px 6px 2px #dedede;
text-align:center;}

.experttitle {font-size:20px;margin:16px auto 18px auto;}
.pctbar {width:463px;height:15px;background:url(images/mykaneva/progressbar/greyBG_463x15.png) repeat-x 0 0;margin:13px auto 8px auto;display:inline-block;position:relative;top:0;left:0;z-index:1;}
.pctcomplete {width:0;height:13px;background:url(images/mykaneva/progressbar/orangeBar_13x13.png) repeat-x 0 0;margin:0;padding:0;border:1px solid #b45e10;position:relative;top:0;left:0;z-index:5;}
.pctmsg {font-size:11px;color:#999;}
.pctmsg span {font-weight:bold;}
.itemText {float:left;margin:30px 0 0 30px;font-weight:bold;width:370px;text-align:left;}
.itemText a:link, .itemText a:hover, .itemText a:visited {font-weight:bold;}
.btnholder {float:right;}
a.btnSkip {margin:24px 30px 0 0;font-weight:bold;padding-top:5px;padding-left:2px;}
a.btnClose {margin:28px 30px 0 0;color:#999;text-decoration:underline;display:inline-block;}
</style>
<script type="text/javascript">
	function Item(label, idx) {
		this.label = label;
		this.index = idx;
	}

	// Create an array of friends.
	var items = [];
	<asp:literal id="litScript" runat="server"></asp:literal>
	var curIndx = 0;

	function showNextChecklistItem() {
		curIndx ++
		if (curIndx == items.length) {
			curIndx = 0;
		}
	   document.getElementById("itemText").innerHTML = items[curIndx].label;
	}
		   
</script>

<div class="containerbox" id="chklistContainer">

	<div class="experttitle" id="expertTitle" runat="server">Become a Kaneva Expert</div>
	<img src="images/mykaneva/progressbar/wisp_border_462x1.png" />
	
	<div class="barcontainer">
		<div class="pctbar"><div class="pctcomplete" id="pctcomplete" runat="server"></div></div>
		<div class="pctmsg">You are <span id="pctmsg" runat="server"></span> complete.</div>
	</div>

	<div class="itemText" id="itemText" runat="server" clientidmode="static"></div>
	<div class="btnholder"><a class="btntiny grey btnSkip" id="btnSkip" runat="server" href="javascript:void(0);" onclick='showNextChecklistItem();'>Skip</a></div>

</div>