///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

namespace KlausEnt.KEP.Kaneva
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

	/// <summary>
	///		Summary description for NavMessages.
	/// </summary>
	public class NavMessages : System.Web.UI.UserControl
	{

		private void Page_Load(object sender, System.EventArgs e)
		{
			if (!IsPostBack)
			{
				BindTabs();
			}
		
			aInbox.HRef = ResolveUrl ("~/mykaneva/mailbox.aspx");
			aBulletins.HRef = ResolveUrl ("~/mykaneva/mailboxBulletin.aspx");
			aChannelReq.HRef = ResolveUrl ("~/mykaneva/mailboxMemberRequests.aspx");
			aTrash.HRef = ResolveUrl ("~/mykaneva/mailboxDeleted.aspx");

			navTrash.Visible = false;

			navChannelReq.Visible = true;
		}
		
		public void BindTabs()
		{
			string css_selected = "selected";

			// Set selected tab
			if ( ActiveTab.Equals(TAB.INBOX) )
			{
				navInbox.Attributes.Add("class",css_selected);
			}
			else if ( ActiveTab.Equals(TAB.BULLETINS) )
			{
				navBulletins.Attributes.Add("class",css_selected);
			}
			else if ( ActiveTab.Equals(TAB.CHANNEL_REQ) )
			{
				navChannelReq.Attributes.Add("class",css_selected);
			}
			else if ( ActiveTab.Equals(TAB.TRASH) )
			{
				navTrash.Attributes.Add("class",css_selected);
			}
		}

		/// <summary>
		/// The SubTab to display
		/// </summary>
		public TAB ActiveTab
		{
			get 
			{
				return m_ActiveTab;
			} 
			set
			{
				m_ActiveTab = value;
			}
		}

		/// <summary>
		/// this really should be stored in channelNav
		/// </summary>
		public enum TAB
		{
			INBOX,
			BULLETINS,
			CHANNEL_REQ,
			TRASH,
			NONE
		}

		protected TAB m_ActiveTab = TAB.INBOX;

		protected HtmlAnchor			aInbox, aBulletins, aChannelReq, aTrash;
		protected HtmlGenericControl	navInbox, navBulletins, navChannelReq, navTrash;
		

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
