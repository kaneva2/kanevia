<%@ Control Language="c#" AutoEventWireup="false" Codebehind="License.ascx.cs" Inherits="KanevaWeb.usercontrols.License" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>

<table cellpadding="0" cellspacing="0" border="0" width="600">
<tr>
	<td><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
	<td style="background-color: #ededed;" width="600">
						<center>
						<span class="blogHeadline2">KANEVA TERMS AND CONDITIONS LICENSE AGREEMENT</span>
						</center>
	</td>
	<td><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
</tr>
<tr>
	<td></td>
	<td>
<textarea name="txtTermsAndCond" rows="15" cols="110" class="Filter2" runat="server" EnableViewState="False">
Welcome to the KANEVA.COM and related websites (individually and together, the "Site"). PLEASE READ THESE TERMS AND CONDITIONS OF USE ("TERMS") CAREFULLY BEFORE USING THIS SITE OR ANY APPLICATIONS OR SERVICES AVAILABLE FROM IT. This Site is owned and operated by Klaus Entertainment, Inc. (referred to as "KEI"). "You" are the person or entity using the Site, ordering or using Applications or described in the Site registration form.

By using this Site, ordering or using Applications, or clicking on a button indicating your consent, you agree to these Terms including but not limited to conducting this transaction electronically, and to disclaimers of warranties, damage and remedy exclusions and limitations, and a choice of Georgia law. If you do not agree to these Terms, you do not have permission to use the Site. KEI reserves the right, at its discretion, to change, modify, add, or remove portions of these Terms at any time. Notification of changes to these Terms will be posted on the Site or sent via e-mail. If any future changes to these Terms are unacceptable to you, you must discontinue use of the Site.

Payment terms and refund information (if applicable) for Applications and other products are provided in the ordering process and/or on the Help page in connection with ordering that product.

You may also be subject to additional terms and conditions when you use third-party content, services, or software (including Applications) accessed through or promoted on the Site.

1. REGISTRATION AND ORDERING OBLIGATIONS
In consideration of your use of the Site and/or Applications, you agree to provide true, accurate and complete information about yourself as prompted by the Site registration or ordering process, provided that in connection with registration, you do not need to, and should not, use your full or true name for your username. In addition, you agree to update that information in order to maintain its truth, accuracy and completeness. KEI may deny you access to the Site or reject your order in the event that your information is untrue, inaccurate or incomplete. 


2. AGREEMENT TO DEAL ELECTRONICALLY
All transactions with or through the Site or KEI may, at KANEVA's option, be conducted electronically. KEI may keep records of any type of communication conducted via the Site. All electronic records are deemed sent when they are properly addressed to the recipient and the record enters an information processing system outside the control of the sender or the record enters a region of an information processing system under the recipient's control. All electronic records are received when the record enters an information processing system that the recipient has designated or uses for the purpose of receiving electronic records or information of the type sent, in a form capable of being processed by that system, and from which the recipient is able to retrieve the electronic record.

3. OBLIGATION FOR USING YOUR PASSWORD
ALL CONTENT OR INSTRUCTIONS TRANSMITTED BY OR RECEIVED FROM ANYONE PRESENTING YOUR PASSWORD ON THE SITE WILL BE DEEMED BINDING ON YOU. You agree that you are solely liable for all actions taken via your password, whether or not made with your knowledge or authority. You agree to guard your password carefully, with the full awareness that a failure to keep it secure will enable others to engage in transactions through the Site for which you will be legally responsible. If you suspect that someone may have obtained access to your password who is not intended to have authority to act on your behalf, please contact KEI immediately to authorize KEI to deny access to the Site to anyone else presenting your password. 

4. LIMITED NON-COMMERCIAL LICENSE TO USE SITE
KEI hereby grants you the limited right to view and use the Site only for the purposes of viewing or playing content such as films or games, placing product orders or for accessing information, Applications and services. KEI reserves the right to suspend or deny, in its sole discretion, your access to all or any portion of the Site as described in Section 18. This license is limited to personal and non-commercial uses by you. Any rights not expressly granted to you herein are reserved to KEI. No portion of this Site is targeted to children, and any minor should seek consent of his or her legal guardian before using this site. Unless you have received specific written permission from KEI, you may not (a) "frame" or otherwise impose editorial comment, commercial material or any information or content on, or in proximity to, content displayed on the Site or (b) alter or modify any content on the Site. Without limiting other restrictions, you agree not to reproduce, transmit, sell, or otherwise exploit the Site and/or Applications for any commercial purpose.

The Site may allow users to upload, post, and/or distribute user submitted content, and use of the Site for this purpose is subject to the following conditions:
�	You understand that all user feedback, data, comments, suggestions, information, text, data, software, sounds, photographs, audio, audiovisual, video, artwork, graphics, messages and other materials of any nature ("Materials") that are transmitted to or via the Site are the sole responsibility of the person from which the Materials originated. This means you, and not KEI, are entirely responsible for the Materials you transmit through the Site. Further, you understand that by using the Site you may be exposed to Materials that are offensive, objectionable or indecent. 
�	You shall not create a user name or screen name or upload to, distribute through or otherwise publish through the Site any Materials which are indecent, libelous, defamatory, obscene, threatening, invasive of privacy or publicity rights, abusive, illegal, harassing, contain expressions of hatred, bigotry, racism or pornography, or are otherwise objectionable, or that would constitute or encourage a criminal offense, violate the rights of any party or violate any law. 
�	Your Materials, user name and/or screen name will not disparage in any manner KEI, its Licensors, or their Applications, products, or services and sites. 
�	Your Materials shall not infringe the copyright, trademark, publicity/privacy right or other intellectual property right of any third party. 
�	You shall not upload to, distribute through or otherwise publish through the Site any Materials that are directly or indirectly commercial in nature or contain any solicitation of funds, promotion, advertising or solicitation for goods or Site. You specifically acknowledge that soliciting other users to join or become users or members of any commercial online web site or other organization is expressly prohibited. 
�	You shall not upload to, distribute through or otherwise publish through the Site any Materials that contain viruses or any other computer code, corrupt files or programs designed to interrupt, destroy or limit the functionality or disrupt any software, hardware, telecommunications, networks, servers or other equipment. 

You acknowledge that KEI does not pre-screen any Materials posted by you or other users, but that KEI and its designees shall have the right (but not the obligation) in their sole discretion to refuse or remove any Materials. Without limitation, KEI and its designees shall have the right to remove any Materials that violate the Terms or is otherwise objectionable, as well as terminate your access to the Site. KEI may establish practices and limits concerning the use of the Site and Applications, including the maximum disk space that will be allotted to your use. You agree that KEI has no liability or responsibility for the storage or deletion of any Materials that you submit or post. KEI reserves the right to change these general practices and limits at any time in its sole discretion, with or without notice.

5. LIMITED NON-COMMERCIAL LICENSE TO USE APPLICATIONS 
KEI may offer you the ability to use certain applications, services or software, and content such as games and films for use therewith, either at no charge or for a fee (such applications, services, software and any related content provided by KEI or its licensors is referred to herein collectively as "Applications"). For example, KEI may provide chat areas, bulletin boards, e-mail functions, software and services such as KANEVA downloads that allow you to download specific content from the Sites to your computer, multi-player games and software, and videos and music videos. Without limiting your obligations and restrictions described in Section 4, your limited license to the Applications is subject to the following conditions:
�	The Applications that are made available to you are the copyrighted work of KEI and/or its suppliers. Certain Applications may be owned by third parties and distributed under third party agreements and you agree that KEI shall not be responsible for any loss or damage of any sort relating to your dealings with such third parties.
�	Unless expressly stated otherwise you are granted only a limited license to download and/or use the Application from a single computer for personal and non-commercial purposes. You may not make copies of or distribute the Application or electronically transfer the Application from one computer to another or over a network, nor may you separate any content from an associated Application. You may not decompile, reverse engineer, disassemble or otherwise reduce the Application to human perceivable form. You may not rent, lease or sublicense the Application. You may not create derivative works of the Application and you may not export the Application in violation of any U.S. or foreign law, rule or regulation. All rights in Applications not specifically granted to you in writing by KEI are reserved to KEI.
�	KEI may cease support of any Application at any time in its sole discretion, as further described in Section 18. 

6. COPYRIGHTS AND TRADEMARKS
EXCEPT AS EXPRESSLY PROVIDED HEREIN BY THESE TERMS, NEITHER KEI, NOR ANY THIRD PARTY HAS CONFERRED UPON YOU BY IMPLICATION, ESTOPPEL OR OTHERWISE, ANY LICENSE OR RIGHT UNDER ANY PATENT, TRADEMARK, COPYRIGHT OR OTHER PROPRIETARY RIGHTS TO USE THE SITE OR ANY APPLICATIONS OFFERED ON THE SITE. NO OWNERSHIP RIGHTS ARE OR WILL BE ASSIGNED TO YOU.
KEI will not tolerate copyright infringement and reserves the right to block, disable, or otherwise remove any content or materials uploaded to the Site as well as terminate access to the Site if you engage in copyright or other intellectual property infringement. The law provides for civil and criminal penalties for copyright and other intellectual property law infringements.
Displaying, performing, storing, copying, distributing, or otherwise making available or using any content from the Site or Applications is prohibited, unless specifically authorized by KEI. Accordingly no such content or Applications may be used on another web site without express written permission from KEI.
Please see KEI's Copyright Policy for more detailed copyright and trademark information, use limitations and for notice and procedures for making a claim of copyright infringement to KEI.

7. PRIVACY POLICY
KEI respects your right to privacy and understands that visitors to KEI need to be in control of their personal information. To that end KEI has developed a Privacy Policy, and you should review it carefully. Your use of the Site is your consent to the Privacy Policy.

8. SUBMITTING MATERIALS AND FEEDBACK
Unless you enter into a separate written agreement with KEI, KEI does not claim ownership in Materials you submit, however, by submitting Materials in any form to KEI, in addition to other provisions of the Terms, you automatically grant KEI a royalty-free, world-wide, non-exclusive, and assignable right and license to use, copy, reproduce, modify, adapt, publish, edit, translate, create derivative works from, transmit, distribute, publicly display and publicly perform such Materials for the purpose of displaying and promoting the Materials on any website operated by, and in any related marketing materials produced by, KEI and its affiliates. 

9. INDEMNITY
You agree to indemnify and hold KEI, its, affiliates, officers, agents, partners and employees harmless from any claim or demand, including reasonable attorneys fees, arising out of your content and materials, your use of the Site, your violation of these Terms or your violation of any third party's rights including such party's copyrights and trademarks.

10. DISCLAIMER OF WARRANTIES 
EXCEPT IF EXPRESSLY PROVIDED OTHERWISE IN A WRITTEN AGREEMENT (SUCH AS A EULA) BETWEEN YOU AND KEI, THE SITE AND APPLICATIONS OFFERED AT OR THROUGH THE SITE, AND APPLICATIONS ARE PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND AND WITH ALL RISKS. KEI HEREBY DISCLAIMS TO THE MAXIMUM EXTENT PERMITTED BY LAW (A) ALL WARRANTIES EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE; AVAILABILITY OF THE SITE; LACK OF VIRUSES, WORMS, TROJAN HORSES, OR OTHER CODE THAT MANIFESTS CONTAMINATING OR DESTRUCTIVE PROPERTIES; ACCURACY, COMPLETENESS, RELIABILITY, TIMELINESS, CURRENCY, OR USEFULNESS OF ANY INFORMATION ON THE SITE; AND (B) ANY DUTIES OF REASONABLE CARE, WORKMANLIKE EFFORT OR LACK OF NEGLIGENCE IN CONNECTION WITH THE SITE, APPLICATIONS, OR RELATED INFORMATION. THE ENTIRE RISK AS TO SATISFACTORY QUALITY, PERFORMANCE, ACCURACY AND EFFORT IN CONNECTION WITH THE SITE, APPLICATIONS, AND RELATED INFORMATION IS BORNE BY YOU. SOME STATES DO NOT ALLOW THE EXCLUSION OR LIMITATION OF IMPLIED WARRANTIES, SO THE ABOVE LIMITATION MAY NOT APPLY TO YOU.

IN ADDITION, KEI DISCLAIMS ANY WARRANTIES OF NON-INFRINGEMENT, TITLE OR QUIET ENJOYMENT IN CONNECTION WITH THE SITE, APPLICATIONS, AND RELATED INFORMATION. 

11. ASSUMPTION OF RISKS
YOU ASSUME ALL RISKS THAT THE SITE, APPLICATIONS, AND RELATED INFORMATION ARE SUITABLE OR ACCURATE FOR YOUR NEEDS AND WILL BE UNINTERRUPTED, TIMELY, SECURE OR ERROR-FREE. ANY APPLICATIONS DOWNLOADED OR OTHERWISE OBTAINED THROUGH THE SITE ARE AT YOUR OWN DISCRETION AND RISK AND YOU ARE SOLELY RESPONSIBLE FOR ANY DAMAGE TO YOUR COMPUTER OR LOSS OF DATA. BY PARTICIPATING IN MULTI-PLAYER GAMES OR VISITING CHAT ROOMS YOU MAY BE EXPOSED TO RUDE, CRUDE, INDECENT, OR OTHER OFFENSIVE LANGUAGE OR REFERENCES. YOU AGREE THAT KEI SHALL NOT BE RESPONSIBLE FOR ANY LOSS OR DAMAGE OF ANY SORT RELATING TO YOUR DEALINGS WITH ANY THIRD PARTY ADVERTISER OR CONTENT PROVIDER ON THE SITE. 

12. NO INCIDENTAL, CONSEQUENTIAL OR CERTAIN OTHER DAMAGES
TO THE MAXIMUM EXTENT ALLOWED BY LAW, YOU AGREE THAT NEITHER  KEI NOR ANY OF KEI'S AFFILIATES OR AGENTS WILL BE LIABLE TO YOU AND/OR ANY OTHER PERSON FOR ANY CONSEQUENTIAL OR INCIDENTAL DAMAGES (INCLUDING BUT NOT LIMITED TO LOST PROFITS, LOSS OF PRIVACY OR FOR FAILURE TO MEET ANY DUTY INCLUDING BUT NOT LIMITED TO ANY DUTY OF GOOD FAITH, LACK OF NEGLIGENCE OR OF WORKMANLIKE EFFORT) OR ANY OTHER INDIRECT, SPECIAL, OR PUNITIVE DAMAGES WHATSOEVER THAT ARISE OUT OF OR ARE RELATED TO THE SITE, APPLICATIONS OR RELATED INFORMATION, OR TO ANY BREACH OF THESE TERMS, EVEN IF KEI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES AND EVEN IN THE EVENT OF FAULT, TORT (INCLUDING NEGLIGENCE) OR STRICT OR PRODUCT LIABILITY. SOME STATES DO NOT ALLOW THE EXCLUSION OR LIMITATION OF LIABILITY FOR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION MAY NOT APPLY TO YOU.

13. LIMITATION OF LIABILITY AND EXCLUSIVE REMEDY
YOU AGREE THAT YOUR SOLE REMEDY FOR ANY BREACH OF THESE TERMS BY KEI OR ANY KEI AGENTS SHALL BE, AT KEI'S OPTION, (1) SUBSTITUTION OR REPLACEMENT OF ALL OR PART OF THE APPLICATION OR PRODUCT THAT GIVES RISE TO DAMAGES INCURRED BY YOU IN REASONABLE RELIANCE ON KEI; OR (2) REFUND OF THE AMOUNT THAT YOU PAID TO KEI. YOU AGREE THAT THE DAMAGE EXCLUSIONS IN THESE TERMS SHALL APPLY EVEN IF ANY REMEDY FAILS OF ITS ESSENTIAL PURPOSE.

14. YOUR REPRESENTATIONS AND WARRANTIES 
You represent and warrant for the benefit of KEI, KEI�s suppliers, and any third parties mentioned on the Site, in addition to other representations and obligations contained in these Terms, that: (a) you possess the legal right and ability to enter into and make the representations and warranties contained in these Terms; (b) all information submitted by you to the Site is true and accurate; (c) you will keep your registration information current; (d) you will be responsible for all use of your password even if such use was conducted without your authority or permission; (e) you will not use the Site for any purpose that is unlawful or prohibited by these Terms; (f) you are the owner of the Materials and they are original to you; (g) the Materials do not infringe any third party right, such as copyright, trademark, and publicity/privacy right; (h) the Materials do not constitute defamation or libel or otherwise violate the law, and (i) you agree to defend, indemnify, and hold KEI (and its employees, representative, agents, and assigns) harmless from breaches of (a) through (h). 

15. LINKS; ADVERTISERS
The Site may contain links to third party sites that are not under the control of KEI and KEI is not responsible for any content on any linked site. If you access a third party site from the Site, then you do so at your own risk. KEI provides links only as a convenience and the inclusion of the link does not imply that KEI endorses or accepts any responsibility for the content on those third party sites. Additionally, your dealings with or participation in promotions of advertisers found on the Site, including payment and delivery of goods, and any other terms (such as warranties) are solely between you and such advertisers. You agree that KEI shall not be responsible for any loss or damage of any sort relating to your dealings with such advertisers.

16. INTERNATIONAL USAGE
This Site is controlled and operated by KEI from its offices within Georgia, United States of America. KEI make no representation that the Site, Applications, or related information offered by KEI are appropriate or available in other locations. Those who choose to access the Site from other locations do so on their own initiative and are responsible for compliance with local laws, if and to the extent local laws are applicable.

17. OWNERSHIP
The Site, Applications, and related information are the exclusive property of KEI or its suppliers. All rights not licensed herein are hereby reserved to KEI or its suppliers.

18. TERMINATION OR CANCELLATION OF SITE ACCESS; MODIFICATIONS TO SITE AND APPLICATIONS
If you violate these Terms, KEI may terminate or cancel your access rights to the Site and/or Applications immediately without notice. KEI may also block your use of the Site and/or Applications or direct you to cease using it. KEI reserves the right at any time to modify or discontinue the Site, Applications, or any part thereof and you agree that KEI shall not be liable to you or to any third party for any modification, suspension, or discontinuance of the Site, Applications, or any part thereof, except you may receive a pro-rata refund in the event such modification, suspension, or discontinuance materially affects your access to those parts of the Site or Applications that you have paid for.

19. EXCLUSIVE JURISDICTION 
These Terms shall be governed by the laws of the State of Georgia without regard to its conflict of law provisions. Any disputes arising under or related in any way to these Terms, the Site or any Application shall be litigated or otherwise heard in the appropriate forum in Fulton county Georgia. The parties hereto hereby consent to the exclusive jurisdiction of the state and federal courts sitting in Fulton County Georgia, and hereby waive any claim or defense that such forum is not convenient or proper, and consent to service of process by any means authorized by Georgia law.

20. ENTIRE AGREEMENT
These Terms, as amended, your registration forms, and the disclosures provided by KEI and the consents provided by you, constitute the entire agreement between you and KEI. If any provision of these Terms shall be unlawful, void, or for any reason unenforceable, then that provision shall be deemed severable from these Terms and shall not affect the validity and enforceability of any remaining provisions. KEI's failure to act with respect to a breach by you or others does not waive KEI's right to act with respect to subsequent or similar breaches.

21. AMENDMENTS
You agree that KEI may amend or modify these Terms or impose new conditions at any time by updating these Terms on the Site or upon notice from KEI to you as published through the Site or by email. Any use of the Site or order by you after such updating shall be deemed to constitute acceptance of such amendments, modifications, or new conditions. If you do not want to be bound by an amendment, you will need to terminate your registration, if any, and refrain from using the Site or using or ordering Applications after that date. No other amendments will be valid unless they are in a paper writing signed by KEI and by you.

22. NOTICES 
Except as expressly stated otherwise, any notices required or allowed under these Terms shall be given to KEI by postal mail to: Klaus Entertainment, Inc., 1050 Crown Pointe Parkway, Suite 230, Atlanta, GA 30338, or as to a successor address that KEI makes available on the Site or through other reasonable manner. If applicable law requires that KEI accepts email notices (but not otherwise), then you may send KEI email notice using KANEVA's Contact KEI Forum. With respect to KANEVA�s notices to you, KEI may provide notice of amendments by posting them in the Site and you agree to check for changes. Instead or in addition, KEI may give notice by sending email to the email address you provide during registration. Notice shall be deemed given 24 hours after it is posted or an email is sent, unless (as to email) the sending party is notified that the email address is invalid. 

Last Updated on December 15, 2004.
</textarea>
	</td>
	<td></td>
</tr>
<tr>
	<td colspan="3"><BR/></td>
</tr>		
<tr>
	<td><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
	<td style="background-color: #ededed;" width="620">
						<center>
						<span class="blogHeadline2">KANEVA COPYRIGHT INFORMATION LICENSE AGREEMENT</span>
						</center>
	
	</td>
	<td><img runat="server" src="~/images/spacer.gif" width="20" height="20"/></td>
</tr>
<tr>
<td></td>
<td>
<textarea name="txtCopyRight" rows="15" cols="110" class="Filter2" runat="server" EnableViewState="False">
KANEVA adheres to the Digital Millennium Copyright Act (DMCA). We respect the intellectual property rights of others and we require our members to do the same. KANEVAmembers remain the original copyright owner in all material (software and content) provided on the community. Use of the material in a manner which is inconsistent with the terms and conditions set forth herein is strictly prohibited. Do not post, give away or sell any item that is not 100% your own unless you have a license to re-distribute. Copyright infringement shall not be permitted.

Do not use items found on the Internet. 
Many items may state free to use, but that does not give permission to redistribute in any form--even just small pieces. Also, there is no good way to verify the person making the Internet file available is even the copyright holder. So don't risk it.

KANEVA requires Proof of Trademark
registration or application in order to have a product offered as such (that includes, but is not limited to, the advertising images, banners, articles, sales descriptions, title, read me, and license). 
Proof of right to use is also required for famous people, actors, movies, and TV shows from the person�s estate, copyright or TM registered owner, or the person.

KANEVA�s Policy Concerning Claims of
Copyright Infringement

If you believe that someone has infringed upon a copyright, we request that the parties involved attempt  to solve the problem themselves. In the event that you are not able to resolve copyright issues, KANEVA will resolve the issue through the following procedures.

Both parties
must attempt in good faith to resolve the problem themselves.

Please select the Copyright notification link below to formally communicate a claim of alleged copyright infringement. Please set forth in detail your claim to the exclusive ownership of the intellectual property in question. Also, please set forth in detail the specific reasons why you believe the product in question infringes your intellectual property rights. Prior to making a claim, please attempt to resolve the problem directly with the artist in question. 

Copyright
Notification - Official
Notification of Claim of Infringement.

Upon receiving a Copyright notification of an alleged copyright infringement claim, KANEVA will, within 1 business day of receipt of the Copyright Notification, suspend the availability of the challenged product in The KANEVA Stores and suspend any payment otherwise due the merchant of the challenged product.

 KANEVA will send an email communicating the claim to the complaining party and to the merchant of the challenged product. The merchant of the challenged product shall have ten (10) days thereafter to provide KANEVA and the complaining party with their Response to Claim. The Response to Claim should set forth in detail every reason why the challenged product does not infringe the intellectual property rights of the complaining party.


Response to Claim - Official Communication of Response to Claim of Infringement.

5.The Response to Claim should be communicated utilizing the Response to Claim link above. If no Response to Claim is communicated by the merchant of the challenged product within the ten (10) day period set forth above, KANEVA will have the right to delete the product from The KANEVA Stores and to withhold any payment otherwise due the merchant of the challenged product. KANEVA will review all information to determine if the merchant of the challenged product is to be warned or merchant status removed.


If a Response to Claim is made, the Response to Claim will be forwarded to the complaining party, and a designated agent for KANEVA will examine the challenged product, the Copyright Notification and the Response to Claim. The KANEVA agent has no legal authority to determine infringement or non-infringement. Rather, the KANEVA agent will examine the challenged product in a good faith effort to determine whether or not there are reasonable grounds to believe that the complaining party owns a valid copyright and that the challenged product may infringe the intellectual property rights of the complaining party.

If KANEVA�s agent does not find reasonable grounds to believe that a copyright infringement may have occurred, the parties will be notified by email, and the product may be resubmitted to The KANEVA Stores at the sole discretion of KANEVA.

If  KANEVA�s agent finds that there are reasonable grounds to believe that the challenged product may infringe the copyright of the complaining party, both parties will be notified by email, and the challenged product will be deleted from The KANEVA Stores. KANEVA shall have the right to withhold any payment otherwise due the merchant of the challenged product and shall also have the right to deposit any amount due the merchant of the challenged product into an escrow account until a court discussion is made. KANEVA will review all information to determine if the merchant of the challenged product is to be warned or merchant status removed.

It is in the best interest of both parties to try to resolve the dispute among yourselves. If at any time during this procedure the parties reach an agreement (such as: item being modified to satisfy the copyright holder, a percentage slip, or monetary payment agreement), KANEVA will work with the parties to facilitate a faster resolve.

KANEVA's Process &amp; Repeat Infringers

1st Offense: Each copyright claim is handled individually. Many times merchants are given a second chance when the infringement appears to be minor and believed to be accidental. Depending on the circumstances, the merchant may be allowed to update the item in question and may be put on a 30 day probation period. The probation could include withholding payment for a month (to cover refunds of a questionable item), signed upload agreement to be filed in merchant�s file at the KEI  office, longer testing time for new products so the company can review them for copyright concerns. KANEVA may choose to remove the merchant�s store if infringement appears severe and / or intentional. Buyers will be notified by email from a KANEVA Admin that an item is �questionable� for copyright violations and be offered an update and/or a refund by gift certificate.


2nd Offense: KANEVA will remove the merchants store and notify buyers of the item that appears to violation copyrights and offered a refund by gift certificate. Any payment due the merchant will be forfeited and used for refunds and KEI�s administrative cost for the item�s sale and return. 

Inconclusive: Items that have been questioned for copyright violations and which there is not clear evidence to support the claim will be deemed inconclusive. KANEVA will work with the copyright holder and accused infringer to try to resolve. If there is a resolve to satisfy all parties involved, KANEVA may agree to release the item back into The KANEVA Stores. Otherwise, the questionable item will not return to the KANEVA Stores. Buyers may or may not be notified or refunded, depending on the circumstances.


During testing, items believed to have copyright violations will be removed and the merchant notified and warned of their first copyright offense. This merchant may then go through a probation period, and other items currently in The KANEVA Stores may be removed and reviewed for possible copyright infringements. Any additional copyright concerns will be considered second offense.

KANEVA reserves the right to remove a merchant�s store in The KANEVA Stores or disable member account at its sole discretion. Allegations of infringement may form the basis for such removal and/or disabling. KANEVA welcomes communication from all parties concerning the products available in The KANEVA Stores and the procedures by which KANEVA seeks to protect the intellectual property rights of all parties.

KANEVA's Policy Concerning False Claims

Repeated copyright notification claims submitted by the same member that are determined to have no reasonable grounds to believe that copyright infringements may have occurred will form the basis for TOS violation warning and/or disabling of members account.

KANEVA's Policy Concerning Trademark Claims of Infringement

1. Please select the Copyright Notification link above to officiallycommunicate a potential trademark infringement claim. Proof of Trademark has to be added to the official notice.

2. Upon receiving an official Notification communication via mail or fax, KANEVAwill remove the product from the KANEVA Stores in question within 1 businessday. KANEVA's business hours are Mon - Fri 9am to 5pm.

3. Emails will be sent to both parties confirming the product's removal fromthe KANEVA Stores.

Proof of Trademark
Proof of Trademark or Patent is any one of the following:

1. Receipt for payment of application: Online application payment receipt, credit card receipt, or cancelled check
2. Letter from attorney with copy of application
3. Searchable serial number 
4. Searchable word mark. 

Documentation has to be printable to place in the Contributor�s file.

Response to Copyright Claim
Copyrights and Designated Agent For Notification of Claims of Infringement 
The Response to claim has to be received within 10 days of copyright notification. 

If no Response to Claim is made by the owner of the product in question, the product will be deleted from The KANEVA Stores. KANEVA will have the right to withhold any payment otherwise due and review the merchant status. 
�  If a Response to Claim is made, the Response to Claim will be forwarded on to the complaining party and KANEVA's agent will begin examination. 
�  Our designated agent will respond within 10 business days once receiving an official Response to Claim. 
In addition to completing the form below, you must also provide your signature by email to copyrights@ KANEVA.com, fax, or mail. KANEVA may request original source files that are dated. 
KANEVA 's Designated Agent for Notification of Claims of Copyright Infringement: 
Michael Jacobson, CFO
1050 Crown Pointe Parkway, Suite 230
Atlanta, GA 30338
Voice: 770. 551. 2760 

 
Copyright Notification
Copyrights and designated Agent for Notification of Claims of Infringement

You must submit the following information by mail:


�	Name, address, phone and email contact information
�	The image (s) used to determine possible infringement showing the parts or areas in question.
�	The signature of the copyright holder or a person authorized to act on behalf of the owner of the copyright interest that is allegedly infringed.
�	Please provide as much information as possible about the author and product that you believe contains copyright violations.
�	Provide a detailed description of exactly what is being allegedly infringed. What specific parts of the product are in question?
�	Provide a statement by you, under penalty of perjury, that you have a good faith belief that the disputed use is not authorized by the copyright owner, its agent, or the law.

KANEVA 's  Designated Agent for Notification of Claims of Copyright Infringement: 
Michael Jacobson, CFO
1050 Crown Pointe Parkway, Suite 230
Atlanta, GA 30338
Voice: 770. 551. 2760 

Last Updated on December 15, 2004.
</textarea>

<td></td>
</td></tr>
</table>