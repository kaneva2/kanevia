<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HomeContent.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.usercontrols.HomeContent" %>

<div id="homeWrapper">

<h1 class="homepageHead">Make Friends and Play in 3D</h1>

<img src="http://streaming.kaneva.com/media/home/bannerImages/Lead_image_party_612x137.jpg" class="homepageHeadImg" />

<p>You can play social games with your friends in a 3D Virtual World.  Card games, trivia contests, puzzles, adventures, and more!</p>
<hr />
<ol>
	<li>		
		<img src="http://streaming.kaneva.com/media/home/contentImages/side_image_friends_114x80.jpg" id=img2 class="sideImage" />
		<label for="friends">Make Friends & Hangout</label>
		<p>Make new friends or hang with your current friends. You can chat and interact in 3D with your friends no matter where they are!</p>
	</li>
	<li>
		<img src="http://streaming.kaneva.com/media/home/contentImages/side_image_collect_114x80.jpg" class="sideImage" />
		<label for="collect">Collect & Trade Stuff with Others</label>
		<p>You can get clothing, furniture, & all sorts stuff you can collect and trade with friends! Every player even gets their own virtual space!</p>
	</li>
	<li>
		<img src="http://streaming.kaneva.com/media/home/contentImages/side_image_build_114x80.jpg" class="sideImage" />
		<label for="build">Build Your Own Experience</label>
		<p>If you can imagine it, you can build it in Kaneva. Design clothing, decorate your home, even make and sell your own items!</p>
	</li>
</ol>

</div>
