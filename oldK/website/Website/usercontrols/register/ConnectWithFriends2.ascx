<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ConnectWithFriends2.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.usercontrols.ConnectWithFriends2" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>

<script src="../../jscript/register/connectWithFriends.js" type="text/javascript" language="javascript"></script>
<link href="../../css/registration_flow/connectWithFriends.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">

function SetDisplayArea (isLoginVisible)
{
	 if (isLoginVisible)
	{		
		SetDisplay ('<%= divStep1_ClientId %>',true);
		SetDisplay ('<%= divStep2_ClientId %>',false);
		
		try
		{			 
		$('<%= txtEmail_ClientId %>').focus ();
		}
		catch(err)
		{}
	}
	else
	{
		SetDisplay ('<%= divStep1_ClientId %>',false);	 
		SetDisplay ('<%= divStep2_ClientId %>',true);			 
	}
}

function valid_capture()
{
	var user="<%=Request.Form["user_id"]%>";
	
	var re = /^\d+$/;
	if(re.test(user)==true)
	{
		if(!frmvalid())
			return false;
	}
	
	var paw = document.forms['captcha']['pass'].value;
	var ts = (new Date()).valueOf( );
	var pk = "<%=primKey%>";
	var val=encription(paw,ts,pk);
	
	document.forms['captcha']['p'].value=val;
	document.forms['captcha']['starttime'].value = (new Date()).valueOf( );
	document.forms['captcha']['ts'].value = (new Date()).valueOf( );
}

</script>


<h1>Kaneva is more fun with friends!</h1>
<p>For every five friends that join, you'll earn 500 Rewards!<br />
You'll also get an additional 1,000 bonus Rewards after they sign in to the 3D world.<br />
You can use Rewards in Kanvea to purchase clothing, furniture, and more.</p>
<hr />
<div id="burst"></div>
<!-- Step 1 Login -->			
<div id="divStep1" runat="server">
<div class="stepHead"><h2>Sign In to Your Email</h2></div>
<p>Enter your email address and password, and we'll find your friends.</p>

<div style="width:85%;">
<asp:validationsummary cssclass="errBox black" id="valSum" runat="server" showmessagebox="False" showsummary="True"
displaymode="BulletList" style="margin-top:10px;" headertext="Please correct the following error(s):" forecolor="black"></asp:validationsummary>
<asp:customvalidator id="cvBlank" runat="server" display="None" enableclientscript="False"></asp:customvalidator>
</div>
<br />

<!-- Add me to this page! -->
<div style="text-align:left" runat="server" id="divEnterEmails">
<div class="flag" style="text-align:right; padding-right:0px; padding-top:10px; width:248px"><asp:linkbutton CausesValidation="false" text="Enter email addresses" id="lbEnterEmail" oncommand="lbEnterEmail_Click" tooltip="Enter email addresses" runat="server" style="margin-right:20px;"/></div>
</div>
<!-- end add -->
<fieldset>					  
<ol>
<li>
	<label id="lblEmail" runat="server">Email:</label>
	<asp:TextBox id="txtEmail" name="textfield" runat="server" style="width:300px"></asp:TextBox>&nbsp;
	<asp:RequiredFieldValidator id="rfvEmail" display="None" runat="server" text=""
	ErrorMessage="Email is required" Font-Bold="True" ControlToValidate="txtEmail"></asp:RequiredFieldValidator>
	<asp:regularexpressionvalidator id="revEmail" runat="server" controltovalidate="txtEmail" text="" errormessage="Invalid email address"
	display="none" enableclientscript="True"></asp:regularexpressionvalidator>
</li>
<li>
	<label id="lblPassword" runat="server">Password:</label>
	<asp:TextBox id="txtPW" name="textfield" runat="server" style="width:300px" TextMode="Password"></asp:TextBox>&nbsp;
	<asp:RequiredFieldValidator id="rfvPassword" display="None"  
	runat="server"  text=""
	ErrorMessage="Password is required" Font-Bold="True" ControlToValidate="txtPW"></asp:RequiredFieldValidator>
</li>
<li>
	<label>&nbsp;</label>
	<div class="btn_sign"><asp:imagebutton id="btnGetAddressBook" runat="server" causesvalidation="false" name="signIn" imageurl="~/images/connectwithfriends/btn_findfriends_188x41.gif" OnClientClick="ShowLoading(true,'Please wait while we complete your request...');" onclick="btnGetAddressBook_Click" alternatetext="Sign In" />&nbsp;</div>	
</li>
<li>
	<label>&nbsp;</label>
	<asp:checkbox id="chkInviteEveryone" name="checkbox" runat="server" checked />
	Invite all of my friends.
</li>
</ol>
</fieldset>	

<h5>Kaneva will NOT store or share your private information!</h5>
<p class="disclaimer">Any information you import (including your address book) is for your own private use.</p>
For more info, see our <a runat="server" href="~/overview/privacy.aspx" target="_blank">Privacy Policy</a>.</p>

</div>


<!-- Step 2 More Emails -->
<div id="divStep2" runat="server" style="display:none;width:100%;">

	<table border="0" cellspacing="0" cellpadding="0" width="99%">
		<tr>
			<td class="title"><h1>Find Your Friends</h1>
				<div class="indicator">
					<!--
						<div class="lEnd"></div>
						<div class="step1_on">Step 1: Create Your User ID</div>
						<div class="step2">Step 2: Invite Friends</div>
						<div class="step3">Step 3: Download</div>
						<div class="rEnd"></div>
						<div class="clear"></div>
					-->
				</div>
				<div class="clear"><!-- clear the floats --></div>
			</td>
		</tr>
		<tr>
			<td align="center">
				<br />
			</td>
		</tr>
		<tr>
			<td>
				<div class="col1">
					<p class="exMTop flag">Choose your address book</p>
					<h3>Kaneva is more fun with friends!</h3>
					<p>We&#8217;ll reward you for inviting your friends to join Kaneva! Simply import your address book, and we&#8217;ll do the rest.</p>
					<p>For every five friends that join, you&#8217;ll earn 500 Rewards! You&#8217;ll also get an additional 1,000 bonus Rewards after they sign in to the 3D world!</p>
				</div>
				<div class="col2">
					<ul>
						<li><a href="javascript:void(0);" id="yahoo_more" class="" onclick="SetSelected ($('yahoo'));">Yahoo</a></li>
						<li><a href="javascript:void(0);" id="gmail_more" class="" onclick="SetSelected ($('gmail'))">GMail</a></li>
						<li><a href="javascript:void(0);" id="aol_more" class="" onclick="SetSelected ($('aol'))">America Online</a></li>
						<li><a href="javascript:void(0);" id="hotmail_more" class="" onclick="SetSelected ($('hotmail'))">Windows Live Hotmail</a></li>
						<li><a href="javascript:void(0);" id="more" class="select">More</a></li>
					</ul>
					<h3 class="clear">More Email Providers</h3>
					<table class="moreTbl">
						<tr>
							<td><a href="javascript:SetMoreEmail('live.com');">live.com</a></td>
							<td><a href="javascript:SetMoreEmail('live.nl');">live.nl</a></td>
							<td><a href="javascript:SetMoreEmail('rediff.com');">rediff.com</a></td>
							<td><a href="javascript:SetMoreEmail('myspace');">myspace</a></td>
							<td><a href="javascript:SetMoreEmail('web.de');">web.de</a></td>
						</tr>
						<tr>
							<td><a href="javascript:SetMoreEmail('plaxo.com');">plaxo.com</a></td>
							<td><a href="javascript:SetMoreEmail('linkedin');">linkedin</a></td>
							<td><a href="javascript:SetMoreEmail('163.com');">163.com</a></td>
							<td><a href="javascript:SetMoreEmail('sina.com');">sina.com</a></td>
							<td><a href="javascript:SetMoreEmail('bol.com.br');">bol.com.br</a></td>
						</tr>
						<tr>
							<td><a href="javascript:SetMoreEmail('indiatimes');">indiatimes</a></td>
							<td><a href="javascript:SetMoreEmail('126.com');">126.com</a></td>
							<td><a href="javascript:SetMoreEmail('abv.bg');">abv.bg</a></td>
							<td><a href="javascript:SetMoreEmail('wp.pl');">wp.pl</a></td>
							<td><a href="javascript:SetMoreEmail('rambler.ru');">rambler.ru</a></td>
						</tr>
						<tr>
							<td><a href="javascript:SetMoreEmail('lycos.com');">lycos.com</a></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
					</table>
					 <div class="skipBtn"><asp:linkbutton id="lbSkipAllMore" onclick="Exit_Click" runat="server" causesvalidation="false" text="Skip this step >>"></asp:linkbutton></div>
					<h5 class="clear">Kaneva will NOT store or share your private information!</h5>
					<p class="disclaimer">Any information you import (including your address book)  is for your own private use. <br>
						For more info, see our <a runat="server" href="~/overview/privacy.aspx" target="_blank">Privacy Policy</a>.</p>
				</div>
				<div class="clear"><!-- clearthe floats --></div>
				
			</td>
		</tr>
	</table>

</div>



<!-- Step 3 Display Contacte -->
<div id="divStep3" runat="server" style="display:none;width:100%;">

<div class="stepHead"><h2>Find Your Friends</h2></div>
<h3>Hangout with Your Friends in 3D.</h3>
Select which contacts to invite to Kaneva from the list below.

<div style="width:85%;"> 
<span id="valSum$ajaxdest" name="__ajaxmark">
<div id="Div1" class="errBox black" style="color:Black;margin-top:10px;display:none;"> </div>
</span> <span id="cvBlank$ajaxdest" name="__ajaxmark"></span> 
</div>		


<div class="col2" id="divImportedContacts" runat="server">
	<div class="friendHead">
	<asp:checkbox id="cbSelectAllMembers" onclick="CheckAll(this);" runat="server" checked />Select All
	</div>
	<div class="friendArea">
		<asp:repeater id="rptContacts" runat="server">
			<itemtemplate>
				<!-- start contact -->
				<div id="divChk" runat="server" class="chckBox"><asp:checkbox id="chkEdit" name="friend" runat="server" checked /></div>
				<div id="divContactName" runat="server" class="name"><%#DataBinder.Eval(Container.DataItem, "Name")%></div>
				<div id="divContactEmail" runat="server" class="email"><%#DataBinder.Eval(Container.DataItem, "Email")%></div>
				<div class="clear"><!-- clear the floats --></div>
				<!-- end contact -->
			</itemtemplate>
		</asp:repeater>
	</div>

	<div>
		<div id="dvProgress" style="display:none;">
			<div id="dvProgress">
				<img src="../../images/progress_bar.gif" alt="Progress" style="float:left;"/>
				<span style="line-height:38px;">One Moment Please...</span>				
			</div>
		</div>
	
		<div id="dvAddButton" class="addButton">			
			<asp:imagebutton id="btnSendInvites" causesvalidation="false" runat="server" tabindex="15" 
				OnClientClick="SetDisplay('dvAddButton', false);SetDisplay('dvProgress', true);SetDisplay('skipButton', false);" 
				onclick="btnSendInvites_Click" imageurl="~/images/connectwithfriends/btn_next_141x41.gif" alternatetext="Next Step" />
		</div>
	</div>
</div>

<div class="col2" id="divNoContacts" runat="server" style="display:none;text-align:left;padding:20px;">
<h3 style="padding:15px 0 20px 0;">No Address Book Contacts</h3>
There are no contacts in your address book.<br /><br />
You can invite friends later by entering email addresses, importing another address book, or importing a .csv file.<br /><br />
<span style="width:100%;text-align:right;padding-right:8px;"><asp:linkbutton id="lbContinue" causesvalidation="false" runat="server" text="Continue" onclick="lbSkipInvites_Click"></asp:linkbutton></span>
</div>

<div class="col2" id="divContactsErr" runat="server" style="display:none;">
<div style="padding:20px;">
<h3 style="padding:15px 0 20px 0;">Unable to Import Address Book</h3>
Unfortunately, we cannot import address books from <span id="spnEmailProvider" runat="server"></span>.<br /><br />
You can invite friends later by entering email addresses, importing another address book, or importing a .csv file.<br /><br />
<span style="width:100%;text-align:right;padding-right:30px;"><asp:linkbutton id="lbContinueErr" causesvalidation="false" runat="server" text="Continue" onclick="lbSkipInvites_Click"></asp:linkbutton></span>
</div>
</div>
<div class="clear"><!-- clear the floats --></div>                
<br />	
</div>

<div id="skipButton" class="skipFriends">
	<div id="dialog">
		<asp:linkbutton id="lbSkipAll" onclick="Exit_Click" runat="server" causesvalidation="false"><span>Skip this step >></span></asp:linkbutton>
	</div>
</div>	
</div>
