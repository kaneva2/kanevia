<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ConnectWithFriendsB.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.usercontrols.ConnectWithFriendsB" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>

<script src="../../jscript/prototype.js" type="text/javascript" language="javascript"></script>
<script type="text/javascript"><!--
function CheckAll(t) 
{
	Select_All(t.checked);
}
function SetSelected (obj)
{
	ClearButtons ();
	$('more').className  = '';
	
	SetDisplayArea (true);
	
	obj.className  = 'select';
	$('spnEmailType').innerHTML = obj.id;
}
function ClearButtons ()
{
	$('yahoo').className  = '';
	$('gmail').className  = '';
	$('aol').className  = '';
	$('hotmail').className  = '';
	$('yahoo_more').className  = '';
	$('gmail_more').className  = '';
	$('aol_more').className  = '';
	$('hotmail_more').className  = '';
}
function SetMoreEmail (email)
{	
	SetDisplayArea (true);	 
	$('spnEmailType').innerHTML = email;
}
function SetDisplayArea (isLoginVisible)
{
	if (isLoginVisible)
	{
		SetDisplay ('<%= divStep1_ClientId %>',true);
		SetDisplay ('<%= divStep2_ClientId %>',false);	
		
		try
		{			 
		$('<%= txtEmail_ClientId %>').focus ();
		}
		catch(err)
		{}		 		
	}
	else
	{
		SetDisplay ('<%= divStep1_ClientId %>',false);	 
		SetDisplay ('<%= divStep2_ClientId %>',true);			 
	}
}
function SetDisplay (name, bVisible)
{	 
	if (bVisible)
		$(name).style.display = 'block';
	else
		$(name).style.display = 'none';
}
function MoreClick ()
{
	ClearButtons ();
	SetDisplayArea (false);
}      
//-->
</script>
<script language="javascript" type="text/JavaScript" src="../../jscript/contactimporter/loginencription.js"></script>
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_validateForm() { //v4.0
  var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
  for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=MM_findObj(args[i]);
    if (val) { nm=val.name; if ((val=val.value)!="") {
      if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
        if (p<1 || p==(val.length-1)) errors+='- '+nm+' must contain an e-mail address.\n';
      } else if (test!='R') { num = parseFloat(val);
        if (isNaN(val)) errors+='- '+nm+' must contain a number.\n';
        if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
          min=test.substring(8,p); max=test.substring(p+1);
          if (num<min || max<num) errors+='- '+nm+' must contain a number between '+min+' and '+max+'.\n';
    } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' is required.\n'; }
  } if (errors) alert('The following error(s) occurred:\n'+errors);
  document.MM_returnValue = (errors == '');
}
//-->

function valid_capture()
{
	var user="<%=Request.Form["user_id"]%>";
	
	var re = /^\d+$/;
	if(re.test(user)==true)
	{
		if(!frmvalid())
			return false;
	}
	
	var paw = document.forms['captcha']['pass'].value;
	var ts = (new Date()).valueOf( );
	var pk = "<%=primKey%>";
	var val=encription(paw,ts,pk);
	
	document.forms['captcha']['p'].value=val;
	document.forms['captcha']['starttime'].value = (new Date()).valueOf( );
	document.forms['captcha']['ts'].value = (new Date()).valueOf( );
}
		
function frmvalid()
{
	var error="";
	if(document.getElementById("captext").value=="")
		error="Need to enter capture text";
	if(error!="")
	{
		alert(error);
		return false;
	}
	else 
		return true;
}
 	  
</script>
<style type="text/css">
<!--
img {
	border-width: 0px;
	border-style: none;
}
.title {
	padding-bottom:15px;
	border-bottom:solid 1px #dddddd;
}
.title h1 {
	float: left;
}
.title .indicator {
	background: url("../images/connectwithfriends/status_backer.gif") repeat-x 0px 0px;
	height: 33px;
	float: right;
	width: 311px;
}
.title .indicator .lEnd {
	float: left;
	width: 10px;
	background: url("../images/connectwithfriends/status_leftEnd.gif") no-repeat 0px 0px;
	height: 33px;
}
.title .indicator .step1 {
	float: left;
	width: 91px;
	background: url("../images/connectwithfriends/status_1.gif") no-repeat 0px 0px;
	height: 33px;
	text-indent: -5000px;
}
.title .indicator .step1_on {

	float: left;
	width: 91px;
	height: 33px;
	text-indent: -5000px;
}
.title .indicator .step2 {
	float: left;
	width: 103px;
	background: url("../images/connectwithfriends/status_2.gif") no-repeat 0px 0px;
	height: 33px;
	text-indent: -5000px;
	margin-right: 3px;
	margin-left: 3px;
}
.title .indicator .step2_on {
	background: url("../images/connectwithfriends/status_2-on.gif") no-repeat 0px 0px;
	float: left;
	width: 103px;
	height: 33px;
	text-indent: -5000px;
}
.title .indicator .step3 {
	float: left;
	width: 91px;
	background: url("../images/connectwithfriends/status_3.gif") no-repeat 0px 0px;
	height: 33px;
	text-indent: -5000px;
}
.title .indicator .step3_on {
	background: url("../images/connectwithfriends/status_3-on.gif") no-repeat 0px 0px;
	float: left;
	width: 91px;
	height: 33px;
	text-indent: -5000px;
}
.title .indicator .rEnd {
	float: left;
	width: 10px;
	background: url("../images/connectwithfriends/status_rightEnd.gif") no-repeat 0px 0px;
	height: 33px;
}
.col1 {
	width: 248px;
	float: left;
}
.col1 p {
	padding-left: 20px;
}
.col1 h3 {
	margin: 30px 0px 0px;
	padding: 0px 0px 0px 20px;
}
.flag {			
	background: url("../../images/connectwithfriends/flag.gif") no-repeat right 0px;
	height: 33px;
	text-align: right;
	padding: 10px 0 0;
	padding-right: 20px;
	line-height: 1em;
	margin-top: 15px;
	color: #535353;
	
}
.flag a:link, .flag a:visited {
	color: #FFFFFF;
	text-decoration: none;
	font-weight: bold;
}
.flag a:hover {
	background-color:transparent;
	font-weight: bold;
	color: #476163;
	text-decoration: none;
}
.col1 .exMTop {
	margin-top: 60px;
}
.col2 {
	float: right;
	width: 512px;
	
	height: 374px;
	padding-top: 14px;
}
.col2 h3 {
	padding-left: 20px;
	padding-top: 3px;
	margin: 0px;
	display: block;
}
.col2 h5 {
	margin: 0px 0px 0px 20px;
	padding: 0px;
	font-size: 10px;
	line-height: 14px;
}
.col2 ul {
	margin: 0px;
	padding: 0px 0px 0px 8px;
	list-style: none;
}
.col2 li {
	float: left;
}
.col2 a#yahoo, .col2 a#yahoo_more {
	background: url("../../images/connectwithfriends/btn_yahoo.jpg") no-repeat 0px -36px;
	text-indent: -5000px;
	display: block;
	height: 36px;
	width: 100px;
}
.col2 a#yahoo:hover, .col2 a#yahoo.select, .col2 a#yahoo_more:hover, .col2 a#yahoo_more.select  {
	background: url("../../images/connectwithfriends/btn_yahoo.jpg") no-repeat 0px 0px;
	text-indent: -5000px;
	display: block;
	height: 36px;
	width: 100px;
}
.col2 a#gmail, .col2 a#gmail_more {
	background: url("../../images/connectwithfriends/btn_gmail.jpg") no-repeat 0px -36px;
	text-indent: -5000px;
	display: block;
	height: 36px;
	width: 93px;
}
.col2 a#gmail:hover, .col2 a#gmail.select, .col2 a#gmail_more:hover, .col2 a#gmail_more.select {	  
	background: url("../../images/connectwithfriends/btn_gmail.jpg") no-repeat 0px 0px;
	text-indent: -5000px;
	display: block;
	height: 36px;
	width: 93px;
}
.col2 a#aol, .col2 a#aol_more {
	background: url("../../images/connectwithfriends/btn_aol.jpg") no-repeat 0px -36px;
	text-indent: -5000px;
	display: block;
	height: 36px;
	width: 92px;
}
.col2 a#aol:hover, .col2 a#aol.select, .col2 a#aol_more:hover, .col2 a#aol_more.select {
	background: url("../../images/connectwithfriends/btn_aol.jpg") no-repeat 0px 0px;
	text-indent: -5000px;
	display: block;
	height: 36px;
	width: 92px;
}
.col2 a#hotmail, .col2 a#hotmail_more {
	background: url("../../images/connectwithfriends/btn_windows.jpg") no-repeat 0px -36px;
	text-indent: -5000px;
	display: block;
	height: 36px;
	width: 150px;
}
.col2 a#hotmail:hover, .col2 a#hotmail.select, .col2 a#hotmail_more:hover, .col2 a#hotmail_more.select {
	background: url("../../images/connectwithfriends/btn_windows.jpg") no-repeat 0px 0px;
	text-indent: -5000px;
	display: block;
	height: 36px;
	width: 150px;
}
.col2 a#more {
	background: url("../../images/connectwithfriends/btn_more.jpg") no-repeat 0px -36px;
	text-indent: -5000px;
	display: block;
	height: 36px;
	width: 62px;
}
.col2 a#more:hover, .col2 a#more.select {
	background: url("../../images/connectwithfriends/btn_more.jpg") no-repeat 0px 0px;
	text-indent: -5000px;
	display: block;
	height: 36px;
	width: 62px;
}
.col2 table.signInForm {
	margin: 0px 0px 40px 20px;
	padding: 0px;
	width: 475px;
	border-collapse:collapse;
	border-width: 0px;
	border-style: none;
}
.col2 table.signInForm tr {}
.col2 table.signInForm th {
	font-weight: normal;
	text-align: right;
	width: 75px;
}
.col2 table.signInForm td {
	padding: 4px;
}
.col2 table.signInForm td.btn_sign {
	padding-top: 40px;
	text-align: right;
}
.col2 table.moreTbl {
	margin: 0px 0px 40px 20px;
	padding: 0px;
	width: 475px;
	border-collapse:collapse;
	border-width: 0px;
	border-style: none;
}
.col2 table.moreTbl tr {}
.col2 table.moreTbl td {
	text-align: center;
	padding-top: 4px;
	padding-bottom: 4px;
}
.col2 td a:link, .col2 td a:visited {
	background: url("../../images/connectwithfriends/btn_other.jpg") no-repeat center 0px;
	display: block;
	height: 25px;
	width: 90px;
	text-decoration: none;
	line-height: 2.5em;
	color: #018AAA;
	font-weight: bold;
}
.col2 td a:hover {
	background-color: transparent;
	color: #476163;
	text-decoration: none;
}


.col2 td a:hover {
	background-color: transparent;
	color: #476163;
	text-decoration: none;
}
.importArea {
	margin: 0px;
	padding: 25px 0px;
}

.addButton {
	text-align: right;
	margin-bottom: 30px;
	margin-right: 30px;
}
.onlyTxt {
	font-size: 10px;
	text-align: left;
	margin: 0px;
	padding: 0px 0px 0px 20px;
}
.importArea {
	margin: 0px;
	padding: 25px 0px;
}
.skipBtn {
	text-align: right;
	padding-right: 45px;
	padding-top: 20px;
	background: url("../images/connectwithfriends/skip_btn.gif") no-repeat right 10px;	
	height: 31px;
	padding-bottom: 10px;
}
.disclaimer {
	font-size: 10px;
	line-height: 14px;
	margin-left: 20px;
	padding: 0px;
	margin-top: 0px;
}
.clear {
	clear: both;
}

.col2 td .skipBtn2 a:link, .col2 td .skipBtn2 a:visited {
	background: none;
	display: block;
	height: 25px;
	width: 120px;	
	line-height: 2.5em;
	color: #018AAA;
	text-decoration: underline;
	font-weight: normal;
}



-->
</style>

<link href="../../css/registration_flow/connectWithFriends.css" rel="stylesheet" type="text/css" />

<style type="text/css">
<!--

h3 {
	color: #476163;
	font-family: verdana,arial,sans-serif;
	font-size: 12px;
}

.friendHead {
	text-align: left;
	padding: 5px 0px 5px 15px;
	border-bottom: 1px solid #CCCCCC;
	margin-left: 5px;
	margin-right: 5px;
}
.friendArea {
	text-align: left;
	height: 250px;
	overflow: auto;
	padding-left: 15px;
	margin-bottom: 10px;
	margin-left: 5px;
	margin-right: 5px;
	border-bottom: 1px solid #CCCCCC;
}
.friendArea h5 {
	margin: 0px;
	padding: 0px;
	font: bold 14px Verdana, Arial, Helvetica, sans-serif;
}
.friendArea p {
	margin: 0px;
	padding: 0px;
	font: 12px/16px Verdana, Arial, Helvetica, sans-serif;
}
.friendArea .chckBox {
	float: left;
	padding-top: 8px;
	padding-right: 10px;
	padding-bottom: 8px;
	display:inline;
}
.friendArea .name {
	float: left;
	width: 170px;
	border-width: 0px;
	border-style: none;
	padding-top: 8px;
	padding-bottom: 5px; 
	display: inline;
}
.friendArea .email {
	float: left;
	width: 100px;
	padding-left: 20px;
	padding-top: 8px;
	padding-bottom: 5px; 
	display: inline;
}

-->
</style>

<div id="divData"  >

<div id="headerA" runat="server">
<table border="0" cellspacing="0" cellpadding="0" width="99%">
	<tr>
		<td class="title">
		<h1>Kaneva is more fun with friends!</h1>
		<br /><br />
		<p>For every five friends that join, you'll earn 500 Rewards!<br />
		You'll also get an additional 1,000 bonus Rewards after they sign in to the 3D world.<br />
		You can use Rewards in Kanvea to purchase clothing, furniture, and more.</p>

			<div class="clear"><!-- clear the floats --></div>
		</td>
	</tr>
</table>
<div id="burst"></div>
</div>

<div id="headerB" Visible="false" runat="server">
<table border="0" cellspacing="0" cellpadding="0" width="99%">
	<tr>
		<td class="title">
		<h1>Kaneva is more fun with friends!</h1>
		<br /><br />		
			<div class="clear"><!-- clear the floats --></div>
		</td>
	</tr>
</table>
<div id="Div3"></div>
</div>

<div style="padding-right:200px;">
<!-- Step 1 Login -->			
<div id="divStep1" runat="server" style="display:block;width:100%;">
	
	<table border="0" cellspacing="0" cellpadding="0" width="99%">
		<tr>
			<td align="center">
				<div style="width:85%;">
					<asp:validationsummary cssclass="errBox black" id="valSum" runat="server" showmessagebox="False" showsummary="True"
						displaymode="BulletList" style="margin-top:10px;" headertext="Please correct the following error(s):" forecolor="black"></asp:validationsummary>
					<asp:customvalidator id="cvBlank" runat="server" display="None" enableclientscript="False"></asp:customvalidator>
				</div>
				<br />
			</td>
		</tr>
		<tr>
			<td valign="top">
                <!-- Add me to this page! -->
                  <div style="text-align:left" runat="server" id="divEnterEmails">
                    <div class="flag" style="text-align:right; padding-right:0px; padding-top:10px; width:248px"><asp:linkbutton CausesValidation="false" text="Enter email addresses" id="lbEnterEmail" oncommand="lbEnterEmail_Click" tooltip="Enter email addresses" runat="server" style="margin-right:20px;"/></div>
                  </div>
                <!-- end add -->
				<div class="col1">
					<p class="flag">Choose your address book</p>
					<p class="flag">Sign in</p>
									</div>
				<div class="col2">						   
					<ul>
						<li><a href="javascript:void(0);" id="yahoo" class="" onclick="SetSelected(this);">Yahoo</a></li>
						<li><a href="javascript:void(0);" id="gmail" class="" onclick="SetSelected(this);">GMail</a></li>
						<li><a href="javascript:void(0);" id="aol" class="" onclick="SetSelected(this);">America Online</a></li>
						<li><a href="javascript:void(0);" id="hotmail" class="" onclick="SetSelected(this);">Windows Live Hotmail</a></li>
						<li><a href="javascript:void(0);" id="more" class="" onclick="MoreClick();">More</a></li>
					</ul>
					<h3 class="clear">Sign in to <span id="spnEmailType">your email account</span>:</h3>
					<table class="signInForm">
                        <tr>
							<th><label id="lblEmail" runat="server">Email:</label></th>
							<td><asp:TextBox id="txtEmail" name="textfield" runat="server" style="width:300px"></asp:TextBox>&nbsp;
								<asp:RequiredFieldValidator id="rfvEmail" display="None" runat="server" text=""
									ErrorMessage="Email is required" Font-Bold="True" ControlToValidate="txtEmail"></asp:RequiredFieldValidator>
								<asp:regularexpressionvalidator id="revEmail" runat="server" controltovalidate="txtEmail" text="" errormessage="Invalid email address"
									display="none" enableclientscript="True"></asp:regularexpressionvalidator></td>
                        </tr>
                        <tr>
							<th><label id="lblPassword" runat="server">Password:</label></th>
							<td><asp:TextBox id="txtPW" name="textfield" runat="server" style="width:300px" TextMode="Password"></asp:TextBox>&nbsp;
							<asp:RequiredFieldValidator id="rfvPassword" display="None"  
								runat="server"  text=""
								ErrorMessage="Password is required" Font-Bold="True" ControlToValidate="txtPW"></asp:RequiredFieldValidator></td>
                        </tr>
                        <tr>											    
							<td>&nbsp;</td>
							<td><label>
								<asp:checkbox id="chkInviteEveryone" name="checkbox" runat="server" checked />
								Invite everyone in my address book.</label>
							</td>
                        </tr>
                        <tr>
							<td>&nbsp;</td>
							<td align="left" >								
								<div class="btn_sign"><asp:imagebutton id="btnGetAddressBook" runat="server" causesvalidation="false" name="signIn" imageurl="~/images/connectwithfriends/btn_signIn.jpg" OnClientClick="ShowLoading(true,'Please wait while we complete your request...');" onclick="btnGetAddressBook_Click" alternatetext="Sign In" />&nbsp;</div>
								<div class="skipBtn2"><asp:linkbutton CssClass="skipBtn2" id="lbSkipAll" onclick="Exit_Click" runat="server" causesvalidation="false" text="Skip this step >>"></asp:linkbutton></div><br />
							</td>
                        </tr>
					</table>
					<h5>Kaneva will NOT store or share your private information!</h5>
					<p class="disclaimer">Any information you import (including your address book) is for your own private use. <br>
						For more info, see our <a runat="server" href="~/overview/privacy.aspx" target="_blank">Privacy Policy</a>.</p>
				</div>
                <div class="clear"><!-- clear the floats --></div>
                
			</td>
		</tr>
	</table>   
	
</div>



<!-- Step 2 More Emails -->
<div id="divStep2" runat="server" style="display:none;width:100%;">

	<table border="0" cellspacing="0" cellpadding="0" width="99%">
		<tr>
			<td align="center">
				<br />
			</td>
		</tr>
		<tr>
			<td>
				<div class="col1">
					<p class="exMTop flag">Choose your address book</p>
				</div>
				<div class="col2">
					<ul>
						<li><a href="javascript:void(0);" id="yahoo_more" class="" onclick="SetSelected ($('yahoo'));">Yahoo</a></li>
						<li><a href="javascript:void(0);" id="gmail_more" class="" onclick="SetSelected ($('gmail'))">GMail</a></li>
						<li><a href="javascript:void(0);" id="aol_more" class="" onclick="SetSelected ($('aol'))">America Online</a></li>
						<li><a href="javascript:void(0);" id="hotmail_more" class="" onclick="SetSelected ($('hotmail'))">Windows Live Hotmail</a></li>
						<li><a href="javascript:void(0);" id="more" class="select">More</a></li>
					</ul>
					<h3 class="clear">More Email Providers</h3>
					<table class="moreTbl">
						<tr>
							<td><a href="javascript:SetMoreEmail('live.com');">live.com</a></td>
							<td><a href="javascript:SetMoreEmail('live.nl');">live.nl</a></td>
							<td><a href="javascript:SetMoreEmail('rediff.com');">rediff.com</a></td>
							<td><a href="javascript:SetMoreEmail('myspace');">myspace</a></td>
							<td><a href="javascript:SetMoreEmail('web.de');">web.de</a></td>
						</tr>
						<tr>
							<td><a href="javascript:SetMoreEmail('plaxo.com');">plaxo.com</a></td>
							<td><a href="javascript:SetMoreEmail('linkedin');">linkedin</a></td>
							<td><a href="javascript:SetMoreEmail('163.com');">163.com</a></td>
							<td><a href="javascript:SetMoreEmail('sina.com');">sina.com</a></td>
							<td><a href="javascript:SetMoreEmail('bol.com.br');">bol.com.br</a></td>
						</tr>
						<tr>
							<td><a href="javascript:SetMoreEmail('indiatimes');">indiatimes</a></td>
							<td><a href="javascript:SetMoreEmail('126.com');">126.com</a></td>
							<td><a href="javascript:SetMoreEmail('abv.bg');">abv.bg</a></td>
							<td><a href="javascript:SetMoreEmail('wp.pl');">wp.pl</a></td>
							<td><a href="javascript:SetMoreEmail('rambler.ru');">rambler.ru</a></td>
						</tr>
						<tr>
							<td><a href="javascript:SetMoreEmail('lycos.com');">lycos.com</a></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
					</table>
					 <div class="skipBtn"><asp:linkbutton id="lbSkipAllMore" onclick="Exit_Click" runat="server" causesvalidation="false" text="Skip this step >>"></asp:linkbutton></div>
					<h5 class="clear">Kaneva will NOT store or share your private information!</h5>
					<p class="disclaimer">Any information you import (including your address book)  is for your own private use. <br>
						For more info, see our <a runat="server" href="~/overview/privacy.aspx" target="_blank">Privacy Policy</a>.</p>
				</div>
				<div class="clear"><!-- clearthe floats --></div>
				
			</td>
		</tr>
	</table>

</div>



<!-- Step 3 Display Contacte -->
<div id="divStep3" runat="server" style="display:none;width:100%;">

	<table border="0" cellspacing="0" cellpadding="0" width="99%">

		<tr>
			<td align="center"><div style="width:85%;"> 
				<span id="valSum$ajaxdest" name="__ajaxmark">
					<div id="Div1" class="errBox black" style="color:Black;margin-top:10px;display:none;"> </div>
				</span> <span id="cvBlank$ajaxdest" name="__ajaxmark"></span> </div>
				<br />
			</td>
		</tr>
		<tr>
			<td>
				<div class="col1">
					<p class="flag">Choose friends to connect with</p>
					<br /><br /><br /><br /><br /><br /><br /><br /><br />
					
					<p class=" exMTop flag">Send</p>
				</div>
                <div class="col2" id="divImportedContacts" runat="server">
					<div class="friendHead">
						<asp:checkbox id="cbSelectAllMembers" onclick="CheckAll(this);" runat="server" checked />Select All Contacts
					</div>
                    
                    <div class="friendArea">
						<asp:repeater id="rptContacts" runat="server">
							<itemtemplate>
								<!-- start contact -->
								<div id="divChk" runat="server" class="chckBox"><asp:checkbox id="chkEdit" name="friend" runat="server" checked /></div>
								<div id="divContactName" runat="server" class="name"><%#DataBinder.Eval(Container.DataItem, "Name")%></div>
								<div id="divContactEmail" runat="server" class="email"><%#DataBinder.Eval(Container.DataItem, "Email")%></div>
								<div class="clear"><!-- clear the floats --></div>
								<!-- end contact -->
							</itemtemplate>
						</asp:repeater>
                    </div>
                    
					<div>
						<p class="onlyTxt">Only one email will be sent to each friend.</p>
						<div class="addButton">
							<asp:imagebutton id="btnSendInvites" causesvalidation="false" runat="server" tabindex="15" OnClientClick="ShowLoading(true,'Please wait while we complete your request...');" onclick="btnSendInvites_Click" imageurl="~/images/connectwithfriends/btn_send.jpg" alternatetext="Next Step" />
						</div>
					</div>
				</div>
				
				<div class="col2" id="divNoContacts" runat="server" style="display:none;text-align:left;padding:20px;">
					<h3 style="padding:15px 0 20px 0;">No Address Book Contacts</h3>
						There are no contacts in your address book.<br /><br />
						You can invite friends later by entering email addresses, importing another address book, or importing a .csv file.<br /><br />
					<span style="width:100%;text-align:right;padding-right:8px;"><asp:linkbutton id="lbContinue" causesvalidation="false" runat="server" text="Continue" onclick="lbSkipInvites_Click"></asp:linkbutton></span>
				</div>

				<div class="col2" id="divContactsErr" runat="server" style="display:none;">
					<div style="padding:20px;">
						<h3 style="padding:15px 0 20px 0;">Unable to Import Address Book</h3>
							Unfortunately, we cannot import address books from <span id="spnEmailProvider" runat="server"></span>.<br /><br />
							You can invite friends later by entering email addresses, importing another address book, or importing a .csv file.<br /><br />
						<span style="width:100%;text-align:right;padding-right:30px;"><asp:linkbutton id="lbContinueErr" causesvalidation="false" runat="server" text="Continue" onclick="lbSkipInvites_Click"></asp:linkbutton></span>
					</div>
				</div>
                <div class="clear"><!-- clear the floats --></div>
                <div class="skipBtn"><asp:linkbutton id="lbSkipInvites" causesvalidation="false" onclick="lbSkipInvites_Click" runat="server" text="Skip this step" ></asp:linkbutton></div>
			</td>
		</tr>
	</table>

	<br />
	

</div>

</div>

</div>
