///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

namespace KlausEnt.KEP.Kaneva
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

	/// <summary>
	///		Summary description for NavMediaUploads.
	/// </summary>
	public class NavMediaUploads : System.Web.UI.UserControl
	{

		private void Page_Load(object sender, System.EventArgs e)
		{
			if (!IsPostBack)
			{
				BindTabs();
			}
		
			aLibrary.HRef = ResolveUrl ("~/asset/publishedItemsNew.aspx?communityId=" + CommunityId);
			aUpload.HRef = ResolveUrl ("~/mykaneva/upload.aspx?communityId=" + CommunityId + "&userId=" + UserId);
		}
		
		public void BindTabs()
		{
			string css_selected = "selected";

			// Set selected tab
			if ( ActiveTab.Equals(TAB.LIBRARY) )
			{
				navLibrary.Attributes.Add("class",css_selected);
			}
			else if ( ActiveTab.Equals(TAB.UPLOAD) )
			{
				navUpload.Attributes.Add("class",css_selected);
			}
		}

		/// <summary>
		/// The SubTab to display
		/// </summary>
		public TAB ActiveTab
		{
			get 
			{
				return m_ActiveTab;
			} 
			set
			{
				m_ActiveTab = value;
			}
		}

		/// <summary>
		/// this really should be stored in channelNav
		/// </summary>
		public enum TAB
		{
			LIBRARY,
			UPLOAD,
			NONE
		}

		public int CommunityId
		{
			get 
			{
				if (ViewState ["UploadNavCommunityId"] != null)
				{
					return Convert.ToInt32 (ViewState ["UploadNavCommunityId"]);
				}
				else
				{
					return 0;
				}
			} 
			set
			{
				ViewState ["UploadNavCommunityId"] = value;
			}
		}
		
		public int UserId
		{
			get 
			{
				if (ViewState ["UploadNavUserId"] != null)
				{
					return Convert.ToInt32 (ViewState ["UploadNavUserId"]);
				}
				else
				{
					return 0;
				}
			} 
			set
			{
				ViewState ["UploadNavUserId"] = value;
			}
		}
		
		protected TAB m_ActiveTab = TAB.LIBRARY;

		protected HtmlAnchor			aLibrary, aUpload;
		protected HtmlGenericControl	navLibrary, navUpload;

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
