///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva.mykaneva.widgets;
using MagicAjax;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.usercontrols
{
    public partial class CommunityProfilePanel : ModuleViewBaseControl
    {
        #region Declarations

       /* protected LinkButton lbEdit, lbDelete;

        protected HtmlTable tblEdit;
        protected Label lblTitle;

        protected HtmlImage imgAvatar;
        protected HyperLink hlOwner;
        protected Panel pnlModerators;
        protected Repeater rpModerators;
        protected Label lblCreateDate;
        protected Label lblNumMembers;*/
        private int _InstanceID = 0;
        private int _PageID = 0;

        #endregion

        private void Page_Load(object sender, System.EventArgs e)
        {
            if (!IsPostBack)
            {
                BindData();
            }
        }

        private void BindData()
        {
            try
            {
                //get the layout moudules for this page
                DataTable dtModules = KlausEnt.KEP.Kaneva.framework.widgets.PageUtility.GetPageLayoutModules(PageID);

                //get the channel description widget for this page
                DataRow[] rows = dtModules.Select("module_id = " + (int)Constants.eMODULE_TYPE.CHANNEL_DESCRIPTION);
                DataRow row = rows[0];

                //get the instance id of the channel description
                _InstanceID = Convert.ToInt32(row["id"].ToString());

            }
            catch (Exception) { }

            //gt the channel description data
            DataRow drChannelDescription = KlausEnt.KEP.Kaneva.framework.widgets.WidgetUtility.GetModuleChannelDescription(_InstanceID);
            if (drChannelDescription != null)
            {
                string userName = drChannelDescription["creator_username"].ToString();
                string profileUrl = KanevaGlobals.GetPersonalChannelUrl(drChannelDescription["creator_username"].ToString());

                hlOwner.NavigateUrl = profileUrl;
                hlOwner.Text = userName;

                int channelId = Convert.ToInt32(drChannelDescription["channel_id"]);
                imgAvatar.Src = GetBroadcastChannelImageURL(drChannelDescription["thumbnail_large_path"].ToString(), "la");

                bool showMod = drChannelDescription["show_moderators"].ToString() == "1";
                pnlModerators.Visible = showMod;
                if (showMod)
                {
                    int numModerators = Convert.ToInt32(drChannelDescription["num_moderators"]);
                    PagedDataTable pds = CommunityUtility.GetCommunityUsers(channelId, 0,
                        false, false, false, false,
                        " account_type_id = " + (int)CommunityMember.CommunityMemberAccountType.MODERATOR,
                        "", 1, numModerators);
                    if (pds.TotalCount > 0)
                    {
                        rpModerators.DataSource = pds;
                        rpModerators.DataBind();
                    }
                    else
                    {
                        pnlModerators.Visible = false;
                    }
                }

                lblCreateDate.Text = Convert.ToDateTime(drChannelDescription["created_date"]).ToShortDateString();
                lblNumMembers.Text = drChannelDescription["number_of_members"].ToString();
            }

        }

        private void rpModerators_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                int userId = Int32.Parse(DataBinder.Eval(e.Item.DataItem, "user_id").ToString());
                string userName = DataBinder.Eval(e.Item.DataItem, "username").ToString();
                string profileUrl = KanevaGlobals.GetPersonalChannelUrl(DataBinder.Eval(e.Item.DataItem, "username").ToString());
                HyperLink hlModerator = (HyperLink)e.Item.FindControl("hlModerator");

                hlModerator.NavigateUrl = profileUrl;
                hlModerator.Text = userName;
            }
        }

        #region Properties

        public string Username
        {
            get
            {
                if (ViewState["Username"] != null)
                {
                    return (string)ViewState["Username"];
                }
                else
                {
                    ViewState["Username"] = UsersUtility.GetUserNameFromId(ProfileOwnerId).ToUpper();

                    return (string)ViewState["Username"];
                }
            }
        }

        public int PageID
        {
            get
            {
                return _PageID;
            }
            set
            {
                _PageID = value;
            }
        }

        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            this.rpModerators.ItemDataBound += new RepeaterItemEventHandler(rpModerators_ItemDataBound);
        }

        #endregion
    }
}