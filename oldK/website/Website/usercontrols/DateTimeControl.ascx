<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="DateTimeControl.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.usercontrols.DateTimeControl" %>

<script type="text/javascript" language="javascript" src="../jscript/prototype.js"></script>
<link href="../css/new.css" rel="stylesheet" type="text/css" />
<link href="../css/editWidgets.css" rel="stylesheet" type="text/css" />

<style > 
 div.dateCalendar{ display:none; position:absolute; z-index:100; } 
 </style> 
 	
<table border="0" cellpadding="2" cellspacing="0" >
       <tr>
            <td style="width:70px" align="left"><label id="lbl_date" runat="server">*Start Date:</label></td>
            <td align="left" style="vertical-align:text-top;width:240px" valign="middle">
			    <asp:TextBox enabled="true" ID="txtDate" class="formKanevaText" style="width:100px" visible="true" MaxLength="10" runat="server" OnTextChanged="txtDate_TextChanged" AutoPostBack="True" CausesValidation="True"/>
			    <img id="imgSelectDate" name="imgSelectDate" runat="server" src="../images/blast/cal_16.gif" />
                <asp:Dropdownlist runat="server" ID="drpTime" style="width: 80px; vertical-align: top;" OnSelectedIndexChanged="drpTime_SelectedIndexChanged" AutoPostBack="True">
                    <asp:ListItem value="00:00:00">12:00 AM</asp:ListItem >
                    <asp:ListItem value="00:30:00">12:30 AM</asp:ListItem >
                    <asp:ListItem value="01:00:00">1:00 AM</asp:ListItem >
                    <asp:ListItem value="01:30:00">1:30 AM</asp:ListItem >
                    <asp:ListItem value="02:00:00">2:00 AM</asp:ListItem >
                    <asp:ListItem value="02:30:00">2:30 AM</asp:ListItem >
                    <asp:ListItem value="03:00:00">3:00 AM</asp:ListItem >
                    <asp:ListItem value="03:30:00">3:30 AM</asp:ListItem >
                    <asp:ListItem value="04:00:00">4:00 AM</asp:ListItem >
                    <asp:ListItem value="04:30:00">4:30 AM</asp:ListItem >
                    <asp:ListItem value="05:00:00">5:00 AM</asp:ListItem >
                    <asp:ListItem value="05:30:00">5:30 AM</asp:ListItem >
                    <asp:ListItem value="06:00:00">6:00 AM</asp:ListItem >
                    <asp:ListItem value="06:30:00">6:30 AM</asp:ListItem >
                    <asp:ListItem value="07:00:00">7:00 AM</asp:ListItem >
                    <asp:ListItem value="07:30:00">7:30 AM</asp:ListItem >
                    <asp:ListItem value="08:00:00">8:00 AM</asp:ListItem >
                    <asp:ListItem value="08:30:00">8:30 AM</asp:ListItem >
                    <asp:ListItem value="09:00:00">9:00 AM</asp:ListItem >
                    <asp:ListItem value="09:30:00">9:30 AM</asp:ListItem >
                    <asp:ListItem value="10:00:00">10:00 AM</asp:ListItem >
                    <asp:ListItem value="10:30:00">10:30 AM</asp:ListItem >
                    <asp:ListItem value="11:00:00">11:00 AM</asp:ListItem >
                    <asp:ListItem value="11:30:00">11:30 AM</asp:ListItem >
                    <asp:ListItem value="12:00:00">12:00 PM</asp:ListItem >
                    <asp:ListItem value="12:30:00">12:30 PM</asp:ListItem >
                    <asp:ListItem value="13:00:00">1:00 PM</asp:ListItem >
                    <asp:ListItem value="13:30:00">1:30 PM</asp:ListItem >
                    <asp:ListItem value="14:00:00">2:00 PM</asp:ListItem >
                    <asp:ListItem value="14:30:00">2:30 PM</asp:ListItem >
                    <asp:ListItem value="15:00:00">3:00 PM</asp:ListItem >
                    <asp:ListItem value="15:30:00">3:30 PM</asp:ListItem >
                    <asp:ListItem value="16:00:00">4:00 PM</asp:ListItem >
                    <asp:ListItem value="16:30:00">4:30 PM</asp:ListItem >
                    <asp:ListItem value="17:00:00">5:00 PM</asp:ListItem >
                    <asp:ListItem value="17:30:00">5:30 PM</asp:ListItem >
                    <asp:ListItem value="18:00:00">6:00 PM</asp:ListItem >
                    <asp:ListItem value="18:30:00">6:30 PM</asp:ListItem >
                    <asp:ListItem value="19:00:00">7:00 PM</asp:ListItem >
                    <asp:ListItem value="19:30:00">7:30 PM</asp:ListItem >
                    <asp:ListItem value="20:00:00" Selected="True">8:00 PM</asp:ListItem >
                    <asp:ListItem value="20:30:00">8:30 PM</asp:ListItem >
                    <asp:ListItem value="21:00:00">9:00 PM</asp:ListItem >
                    <asp:ListItem value="21:30:00">9:30 PM</asp:ListItem >
                    <asp:ListItem value="22:00:00">10:00 PM</asp:ListItem >
                    <asp:ListItem value="22:30:00">10:30 PM</asp:ListItem >
                    <asp:ListItem value="23:00:00">11:00 PM</asp:ListItem >
                    <asp:ListItem value="23:30:00">11:30 PM</asp:ListItem >
                </asp:Dropdownlist>&nbsp;EST						
                <div id="cal1Container" runat="server" class="dateCalendar"></div>
                <asp:RequiredFieldValidator runat="server" id="rfvStartDate" controltovalidate="txtDate" text="* Please enter a a value." errormessage="The promotion start date is required." display="Dynamic"></asp:RequiredFieldValidator>
           </td>
        </tr>

</table>
