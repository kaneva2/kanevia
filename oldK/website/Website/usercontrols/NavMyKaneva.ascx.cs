///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

namespace KlausEnt.KEP.Kaneva
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

	/// <summary>
	///		Summary description for NavMyKaneva.
	/// </summary>
	public class NavMyKaneva : System.Web.UI.UserControl
	{
		private void Page_Load(object sender, System.EventArgs e)
		{
            //aProfile.HRef = ResolveUrl("~/community/ProfilePage.aspx");
            aProfile.HRef = ResolveUrl("~/community/ProfilePage.aspx");
            aFriends.HRef = ResolveUrl("~/mykaneva/friendsGroups.aspx");
			aUploads.HRef = ResolveUrl ("~/asset/publishedItemsNew.aspx");
			aMessages.HRef = ResolveUrl ("~/mykaneva/mailbox.aspx");
			aChannels.HRef = ResolveUrl ("~/bookmarks/channelTags.aspx");
			a3dApps.HRef = ResolveUrl("~/myKaneva/my3dApps.aspx");
			aAccount.HRef = ResolveUrl ("~/myKaneva/general.aspx");

			BindTabs();
		}
		
		public void BindTabs()
		{
            ResetTabs ();
            string css_selected = "selected";

			// Set selected tab 
			if ( ActiveTab.Equals(TAB.PROFILE) )
			{
				navProfile.Attributes.Add("class",css_selected);
			}
			else if ( ActiveTab.Equals(TAB.FRIENDS) )
			{
				navFriends.Attributes.Add("class",css_selected);
			}
			else if ( ActiveTab.Equals(TAB.MEDIA_UPLOADS) )
			{
				navUploads.Attributes.Add("class",css_selected);
			}
			else if ( ActiveTab.Equals(TAB.MESSAGES) )
			{
				navMessages.Attributes.Add("class",css_selected);
			}
			else if ( ActiveTab.Equals(TAB.CHANNELS) )
			{
				navChannels.Attributes.Add("class",css_selected);
			}
			else if (ActiveTab.Equals(TAB.MY3DAPPS))
			{
				nav3dApps.Attributes.Add("class", css_selected);
			}
			else if ( ActiveTab.Equals(TAB.ACCOUNT) )
			{
				navAccount.Attributes.Add("class",css_selected);
			}
		}

        private void ResetTabs ()
        {
            navProfile.Attributes.Remove ("class");
            navFriends.Attributes.Remove ("class");
            navUploads.Attributes.Remove ("class");
            navMessages.Attributes.Remove ("class");
            navChannels.Attributes.Remove ("class");
            nav3dApps.Attributes.Remove ("class");
            navAccount.Attributes.Remove ("class");
        }

		/// <summary>
		/// The SubTab to display
		/// </summary>
		public TAB ActiveTab
		{
			get 
			{
				return m_ActiveTab;
			} 
			set
			{
				m_ActiveTab = value;
			}
		}

		/// <summary>
		/// this really should be stored in channelNav
		/// </summary>
		public enum TAB
		{
			PROFILE,
			FRIENDS,
			MEDIA_UPLOADS,
			MESSAGES,
			CHANNELS,
			MY3DAPPS,
			ACCOUNT,
			NONE
		}

		protected TAB m_ActiveTab = TAB.PROFILE;

		protected HtmlAnchor		 aProfile, aFriends, aUploads, aMessages, aChannels, aAccount, a3dApps;
		protected HtmlGenericControl navProfile, navFriends, navUploads, navMessages, navChannels, navAccount, nav3dApps;

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
