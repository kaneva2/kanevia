///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

namespace KlausEnt.KEP.Kaneva
{
	using System;
	using System.Data;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

	/// <summary>
	///		Summary description for PublishedItemsFilter.
	/// </summary>
	public class PublishedItemsFilter : System.Web.UI.UserControl
	{

		private void Page_Load(object sender, System.EventArgs e)
		{
			if (!IsPostBack)
			{
				SetupFilter ();
			}
		}

		private void SetupFilter ()
		{	
			// View
			drpView.Items.Add (new ListItem ("view...", ""));
			drpView.Items.Add (new ListItem ("----------", " "));
			drpView.Items.Add (new ListItem ("list", "lst"));
			drpView.Items.Add (new ListItem ("detail list", "li"));

			if (DefaultView.Trim ().Length > 0)
			{
				try
				{
					drpView.SelectedValue = DefaultView.Trim ();
				}
				catch (Exception) {}
			}


			// Pages
			// *********** NOTE *************************************************************************
			// IF YOU CHANGE THESE, MAKE SURE YOU HAVE ONE EQUAL TO THE CURRENT SETTING OF AssetsPerPage 
			// ****************************************************************************************** 
			drpPages.Items.Add (new ListItem ("5", "5"));
			drpPages.Items.Add (new ListItem ("10", "10"));
			drpPages.Items.Add (new ListItem ("20", "20"));
			drpPages.Items.Add (new ListItem ("30", "30"));
			drpPages.Items.Add (new ListItem ("50", "50"));
			drpPages.Items.Add (new ListItem ("100", "100"));
			try
			{
				drpPages.SelectedValue = NumberOfPages.ToString ();
			}
			catch (Exception){}
		}

		private string GetSearchDate (DateTime dtDate,int monthDifference)
		{
			System.Globalization.GregorianCalendar cal = new System.Globalization.GregorianCalendar ();
			return cal.AddMonths (dtDate, -monthDifference).ToString ("MM/yy");
		}

		/// <summary>
		/// Change Pages
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void drpPages_Change (Object sender, EventArgs e)
		{
			FilterChanged (this, new FilterChangedEventArgs (CurrentFilter, CurrentSort));
		}

		/// <summary>
		/// The current filter
		/// </summary>
		public string CurrentFilter
		{
			get 
			{
				if (ViewState ["filter"] != null)
				{
					return ViewState ["filter"].ToString ();
				}
				else
				{
					return "";
				}
			} 
			set
			{
				ViewState ["filter"] = value;
			}
		}

		/// <summary>
		/// The current Sort
		/// </summary>
		public string CurrentSort
		{
			get 
			{
				if (ViewState ["sort"] != null)
				{
					return ViewState ["sort"].ToString ();
				}
				else
				{
					return DEFAULT_STORE_SORT;
				}
			} 
			set
			{
				ViewState ["sort"] = value;
			}
		}

		/// <summary>
		/// The current Number of Pages to display
		/// </summary>
		public int NumberOfPages
		{
			get 
			{
				if (drpPages.SelectedValue.Length > 0 && IsPostBack)
				{
					Response.Cookies ["sipp"].Value = drpPages.SelectedValue;
					Response.Cookies ["sipp"].Expires = DateTime.Now.AddYears (1);
					return Convert.ToInt32 (drpPages.SelectedValue);
				}
				else
				{
					// Read it from the cookie?
					if (Request.Cookies ["sipp"] == null)
					{
						return KanevaGlobals.AssetsPerPage;
					}
					else
					{
						return Convert.ToInt32 (Request.Cookies ["sipp"].Value);
					}
				}
			} 
		}

		/// <summary>
		/// The default view
		/// </summary>
		public string DefaultView
		{
			get 
			{
				if (ViewState ["DefaultView"] != null && IsPostBack)
				{
					return (string) ViewState ["DefaultView"];
				}
				else
				{
					if (Request.Cookies ["sview"] == null)
					{
						return "li";
					}
					else
					{
						return Request.Cookies ["sview"].Value;
					}
				}
			} 
			set
			{
				ViewState ["DefaultView"] = value;
			}

		}

		/// <summary>
		/// The Current View
		/// </summary>
		public string CurrentView
		{
			get 
			{
				if (!IsPostBack && DefaultView.Trim ().Length > 0)
				{
					return DefaultView.Trim ();
				}
				// Save the current view
				if (IsPostBack && drpView.SelectedValue.Length > 0)
				{
					Response.Cookies ["sview"].Value = drpView.SelectedValue;
					Response.Cookies ["sview"].Expires = DateTime.Now.AddYears (1);
				}
				return drpView.SelectedValue;
			} 
		}

		/// <summary>
		/// Change View
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void drpView_Change (Object sender, EventArgs e)
		{
			FilterChangedEventArgs fcea = new FilterChangedEventArgs (CurrentFilter, CurrentSort);
			fcea.View = Server.HtmlEncode (drpView.SelectedValue.Trim ());

			FilterChanged (this, fcea);
		}


		private const string DEFAULT_STORE_SORT = "a.name";

		public event FilterChangedEventHandler FilterChanged;

		protected DropDownList drpPages, drpView;

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
