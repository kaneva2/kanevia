///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

namespace KlausEnt.KEP.Kaneva.usercontrols
{
	using System;
	using System.Collections;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

    public partial class WOKRelatedItemsView : BaseUserControl
    {
        private void Page_Load(object sender, System.EventArgs e)
        {
            if (!IsPostBack)
            {
                
                try
                {
                    //bind the data
                    BindData();
                }
                catch(Exception)
                {
                }
            }
        }

        #region Primary Functions

        private void BindData()
        {
            //populate attributes
            WOKItemId = Convert.ToInt32(WOKItem["global_id"]);
            WOKItemOwnerId = Convert.ToInt32(WOKItem["item_creator_id"]);


            if (WOKItemId > 0)
            {
                lbMore.CssClass = "";
                if (WOKItemOwnerId < 1)
                {
                    lbMore.Text = "More Kaneva Items...";
                }
                lbRelated.CssClass = "";

                dlRelated.Visible = false;
                dlMore.Visible = false;

                int count = 0;

                PagedDataTable pdtRelated = StoreUtility.GetRelatedWOKItemsByItemId(WOKItemId, 1, this.ItemsPerPage);
                PagedDataTable pdtMore = StoreUtility.GetWOKCatalogItemsByOwnerIdExclusive(WOKItemOwnerId, WOKItemId, 1, this.ItemsPerPage);

                // display the related items 
                if (this.IsRelatedItemsSearch && pdtRelated.TotalCount > 0)
                {
                    lbRelated.CssClass = "selected";

                    dlRelated.DataSource = pdtRelated;
                    dlRelated.DataBind();
                    dlRelated.Visible = true;

                    // Show the results
                    if (pdtRelated.TotalCount > ItemsPerPage)
                    {
                        //pull all categories the item is in
                        WOKItemsCategories = StoreUtility.GenerateCategoryIdListByItemId(WOKItemId);
                        WOKItemCategoryNames = StoreUtility.GenerateCategoryNameListByItemId(WOKItemId);

                        count = ItemsPerPage;
                        DataRow drWOKItem = StoreUtility.GetWOKItem(WOKItemId);
                        aAllResults.InnerText = "See More " + TruncateWithEllipsis(Server.HtmlDecode(WOKItemCategoryNames), 20);
                        aAllResults.HRef = GetRelatedItemsMoreLink();
                        aAllResults.Visible = true;
                    }
                    else 
                    {
                        count = pdtRelated.TotalCount;
                        aAllResults.Visible = false;
                    }

                    spnShowing.InnerHtml = "Showing 1 - " + count.ToString() + " of " + pdtRelated.TotalCount.ToString();

                    if (pdtMore.TotalCount == 0)
                    {
                        liMore.Visible = false;
                        lbRelated.Attributes["onclick"] = "javascript:return false;";
                        lbRelated.Attributes["onmouseover"] = "javascript:this.style.cursor='default';";
                    }
                }
                else if (pdtMore.TotalCount > 0)	// display the more items
                {
                    lbMore.CssClass = "selected";

                    dlMore.DataSource = pdtMore;
                    dlMore.DataBind();
                    dlMore.Visible = true;

                    // Show the results										  
                    if (pdtMore.TotalCount > ItemsPerPage)
                    {
                        //FUTURE -- Add back when we get new search browse working so can view media for a specific user

                        count = ItemsPerPage;

                        aAllResults.InnerText = "See More Items By " + WOKItem["ownername"].ToString();

                        aAllResults.HRef = GetMoreUserItemsLink();

                        aAllResults.Visible = true;
                    }
                    else
                    {
                        count = pdtMore.TotalCount;
                        aAllResults.Visible = false;
                    }

                    spnShowing.InnerHtml = "Showing 1 - " + count.ToString() + " of " + pdtMore.TotalCount.ToString();

                    if (pdtRelated.TotalCount == 0)
                    {
                        liRelated.Visible = false;
                        lbMore.Attributes["onclick"] = "javascript:return false;";
                        lbMore.Attributes["onmouseover"] = "javascript:this.style.cursor='default';";
                    }
                }
                else
                {
                    divContainer.Visible = false;
                }
            }
        }

        #endregion


        #region Helper Methods

        /// <summary>
        /// generate a link for the virtual world page that will auto display any WOK items in the same category 
        /// </summary>
        protected string GetRelatedItemsMoreLink()
        {
            return ResolveUrl("~/3d-virtual-world/virtual-life.kaneva?catId=" + WOKItemsCategories);
        }

        /// <summary>
        /// generate a link for the virtual world page that will auto display any WOK items in the same category 
        /// </summary>
        protected string GetMoreUserItemsLink()
        {
            return ResolveUrl("~/3d-virtual-world/virtual-life.kaneva?typeId=" + WOKItem["item_creator_id"].ToString());
        }

        ///// <summary>
        ///// Get users personal channel id
        ///// </summary>
        //private int GetUserPersonalCommunityId(int userId)
        //{
        //    return CommunityUtility.GetPersonalChannelId(userId);
        //}

        #endregion

        #region Event Handlers
        /// <summary>
        /// Event fired when one of the tabs is clicked
        /// </summary>
        protected void RelatedTab_Changed(object sender, CommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "related":
                    IsRelatedItemsSearch = true;
                    break;
                case "more":
                    IsRelatedItemsSearch = false;
                    break;
            }

            BindData();
        }
        #endregion

        #region Properties

        private string WOKItemsCategories
        {
            get { return _WOKItems_categories; }
            set { _WOKItems_categories = value; }
        }

        private string WOKItemCategoryNames
        {
            get { return _WOKItem_categoryNames; }
            set { _WOKItem_categoryNames = value; }
        }

        private bool IsRelatedItemsSearch
        {
            get { return _is_related_items; }
            set { _is_related_items = value; }
        }

        public int ItemsPerPage
        {
            get { return _items_per_page; }
            set { _items_per_page = value; }
        }

        public int WOKItemId
        {
            get { return _WOKItemId; }
            set { _WOKItemId = value; }
        }

        public DataRow WOKItem
        {
            get { return _WOKItem; }
            set { _WOKItem = value; }
        }

        public int WOKItemOwnerId
        {
            get { return _WOKItem_owner_id; }
            set { _WOKItem_owner_id = value; }
        }

        #endregion

        #region Declerations

        private int _items_per_page = 5;
        private int _WOKItemId;
        private DataRow _WOKItem = null;
        private int _WOKItem_owner_id = 0;
        private bool _is_related_items = true;
        private string _WOKItems_categories = "";
        private string _WOKItem_categoryNames = "";

        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion


    }
}
