///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

namespace KlausEnt.KEP.Kaneva
{
    using System;
    using System.Data;
    using System.Drawing;
    using System.Web;
    using System.Web.UI.WebControls;
    using System.Web.UI.HtmlControls;

    /// <summary>
    ///		Summary description for StartHtml.
    /// </summary>
    public class StartHtml : System.Web.UI.UserControl
    {

        #region Delcarations
        private bool _standardsMode = false;
        private string m_styles = "";
        private string m_Title = "Kaneva - Imagine What You Can Do";
        private string m_metaTitle = "";
        //private string m_metaTitle = "<meta name=\"title\" content=\" Kaneva. The Online Community and Social Network.\">";
        private string m_metaDescription = "<meta name=\"description\" content=\"Where your Profile, Friends, Media and favorite Communities are teleported into a modern-day 3D world where you can explore, socialize and experience entertainment in an entirely new way.\">";
        private string m_metaKeywords = "<meta name=\"keywords\" content=\" online community, social network, meet people, friends, networking, games, mmo, movies, 3D, virtual world, photos, video, blog, bands, music, digital, rpg, entertainment, join groups, forums, online social networking, caneva, kaneeva, kanneva\">";

        //		private string m_metaKeywords = "<meta name=\"keywords\" content=\"meet people, friends, networking, games, mmo, movies, 3D, virtual world, sharing photos, trailers, caneva, kaneva, kaneeva, kaneve, kanneva, kineva, kanava\">";
        //		private string m_metaDescription = "<meta name=\"description\" content=\"KANEVA - Your Digital Canvas - Explore, Express, Create\">";

        private string m_webAnalyticsExperimentCode = "";

        protected System.Web.UI.WebControls.Literal litCustomVars, litCustomEvents, litOmnitureJSFile, litPageType, litPageURL,
            litStyleSheet, litJavascripts, litJavascriptDetect, litTitle, litMetaDataKeywords, litMetaDataDescription,
            litMetaDataTitle, litOmnitureReportId, litHtmlHeadTag, litWebAnalyticsExperimentCode;
        #endregion

        private void Page_Load(object sender, System.EventArgs e)
        {
            // DocType and Html            
            string strQuirksMode = "<HTML>";
            string strStandardsMode = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n" +
                        "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n";
            litHtmlHeadTag.Text = (StandardsMode) ? strStandardsMode : strQuirksMode;

            string strBaseStyles = "<link id=\"styleSheet\" rel=\"stylesheet\" href=\"" + ResolveUrl("~/css/Kaneva.css") + "\" type=\"text/css\"/>" +
                "<link id=\"styleSheet\" rel=\"stylesheet\" href=\"" + ResolveUrl ("~/css/base/footer.css") + "\" type=\"text/css\"/>";

            // Style sheets
            litStyleSheet.Text = (StyleSheets != "") ? StyleSheets : strBaseStyles;


            // Global Javascripts
            litJavascripts.Text = "<script language=\"JavaScript\" type=\"text/javascript\" src=\"" + ResolveUrl("~/jscript/kaneva.js") + "\"></script>" +
                "<script language=\"JavaScript\" type=\"text/javascript\" src=\"" + ResolveUrl("~/jscript/SWFObject/swfobject.js") + "\"></script>";

            litJavascriptDetect.Text = "<NOSCRIPT><META HTTP-EQUIV=\"refresh\" CONTENT=\"1; URL=" + ResolveUrl("~/noJavascript.aspx") + "\"></NOSCRIPT>";

            // populates title bar
            if (m_Title.Length > 0)
            {
                litTitle.Text = m_Title;
            }

            // MetadataTitle
            //litMetaDataTitle.Text	= MetaDataTitle;

            // MetadataKeywords	
            litMetaDataKeywords.Text = MetaDataKeywords;

            // MetadataDescription
            litMetaDataDescription.Text = MetaDataDescription;

            // Analytics Experiment Code
            litWebAnalyticsExperimentCode.Text = WebAnalyticsExperimentCode;
        }

        /// <summary>
        /// The page title property
        /// </summary>
        public string Title
        {
            set
            {
                if (value != string.Empty)
                {
                    m_Title = value;
                }
            }
            get
            {
                return m_Title;
            }
        }

        public string MetaDataTitle
        {
            set
            {
                if (value != string.Empty)
                {
                    m_metaTitle = value;
                }
            }
            get
            {
                return m_metaTitle;
            }
        }

        /// <summary>
        /// sets or gets meta data keyword html
        /// </summary>
        public string MetaDataKeywords
        {
            set
            {
                if (value != string.Empty)
                {
                    m_metaKeywords = value;
                }
            }
            get
            {
                return m_metaKeywords;
            }
        }

        /// <summary>
        /// sets or gets meta data description html
        /// </summary>
        public string MetaDataDescription
        {
            set
            {
                if (value != string.Empty)
                {
                    m_metaDescription = value;
                }
            }
            get
            {
                return m_metaDescription;
            }
        }

        public bool StandardsMode
        {
            set { _standardsMode = value; }
            get { return _standardsMode; }
        }

        /// <summary>
        /// sets or gets stylesheets
        /// </summary>
        public string StyleSheets
        {
            set
            {
                if (value != string.Empty)
                {
                    m_styles = value;
                }
            }
            get
            {
                return m_styles;
            }
        }

        /// <summary>
        /// sets or gets web analytics experiment code
        /// </summary>
        public string WebAnalyticsExperimentCode
        {
            set
            {
                if (value != string.Empty)
                {
                    m_webAnalyticsExperimentCode = value;
                }
            }
            get
            {
                return m_webAnalyticsExperimentCode;
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}
