<%@ Control Language="c#" AutoEventWireup="false" Codebehind="NavMyChannels.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.NavMyChannels" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>


<div id="thirdaryNav">
	<ul>
		<li id="liChanName" runat="server" class="channelname"></li>
		<li id="liEdit" runat="server"><a id="aEdit" runat="server">Edit</a></li>
		<li id="liLayout" runat="server"><a id="aLayout" runat="server">Layout</a></li>
		<li id="liDesign" runat="server"><a id="aDesign" runat="server">Design</a></li>
		<li id="liPhoto" runat="server"><a id="aPhoto" runat="server">Profile Photo</a></li>
		<li id="liInterests" runat="server"><a id="aInterests" runat="server">My Interests</a></li>
		<li id="liPersonal" runat="server"><a id="aPersonal" runat="server">Personal</a></li>
		<li id="liUploads" runat="server"><a id="aUploads" runat="server">Community Media</a></li>
		<li id="liMembers" runat="server"><a id="aMembers" runat="server">Members</a></li>
		<li id="liAdmin" runat="server"><a id="aAdmin" runat="server">Admin</a></li>
		<li id="liBlogs" runat="server"><a id="aBlogs" runat="server">Blogs</a></li>
	</ul>				
</div>

