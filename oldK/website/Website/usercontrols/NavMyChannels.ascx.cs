///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

namespace KlausEnt.KEP.Kaneva
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

	/// <summary>
	///		Summary description for NavProfile.
	/// </summary>
	public class NavMyChannels : System.Web.UI.UserControl
	{

		private void Page_Load(object sender, System.EventArgs e)
		{
			bool isPersonal = CommunityUtility.IsChannelPersonal(ChannelId);
			
			liChanName.InnerText = KanevaGlobals.TruncateWithEllipsis (ChannelName, 15);
			
			// Edit
            aEdit.HRef = ResolveUrl("~/community/ProfilePage.aspx?communityId=" + ChannelId.ToString() + "&pageId=" + PageId.ToString());

			// Layout
			aLayout.HRef = ResolveUrl ("~/mykaneva/editPage.aspx?communityId=" + ChannelId.ToString() + "&pageId=" + PageId.ToString());

			// Design
			aDesign.HRef = ResolveUrl ("~/mykaneva/pageSettings.aspx?communityId=" + ChannelId.ToString() + "&pageId=" + PageId.ToString());
				
			// Blogs
			aBlogs.HRef = ResolveUrl ("~/blog/blog.aspx?communityId=" + ChannelId );

			
			if (isPersonal)
			{
				// Photo
				aPhoto.HRef = ResolveUrl ("~/mykaneva/photo.aspx");

				// Interests
				aInterests.HRef = ResolveUrl ("~/mykaneva/profile.aspx");

				// Personal
				aPersonal.HRef = ResolveUrl ("~/mykaneva/personal.aspx");

				// Remove channel tabs
				liUploads.Visible = false;
				liMembers.Visible = false;
				liAdmin.Visible = false;
			}
			else
			{
				aUploads.HRef = ResolveUrl ("~/asset/publishedItemsNew.aspx?communityId=" + ChannelId );

				aMembers.HRef = ResolveUrl ("~/community/members.aspx?communityId=" + ChannelId );

                //configure to match the widgets text
                aBlogs.InnerText = "News";
                aBlogs.HRef = ResolveUrl("~/blog/blog.aspx?communityId=" + ChannelId);


				if (CommunityUtility.IsCommunityOwner (ChannelId, KanevaWebGlobals.GetUserId (), true) )
				{
					liAdmin.Visible = true;
					// Admin Link
					string pageURL = "";
					if (PageId > -1)
						pageURL += "&pageid=" + PageId;
	
					aAdmin.HRef = ResolveUrl ("~/community/commEdit2.aspx?communityId=" + ChannelId + pageURL);
				}
				else
				{
					liAdmin.Visible = false;
				}

				// Remove personal channel tabs
				liPhoto.Visible = false;
				liPersonal.Visible = false;
				liInterests.Visible = false;
			}
			
			BindTabs();
		}
		
		public void BindTabs()
		{
			string css_selected = "selected";

			// Set selected tab
			switch (ActiveTab)
			{
				case TAB.EDIT:
					liEdit.Attributes.Add("class",css_selected);
					break;
				case TAB.LAYOUT:
					liLayout.Attributes.Add("class",css_selected);
					break;
                case TAB.DESIGN:
					liDesign.Attributes.Add("class",css_selected);
					break;
				case TAB.PROFILE_PHOTO:
					liPhoto.Attributes.Add("class",css_selected);
					break;
				case TAB.INTERESTS:
					liInterests.Attributes.Add("class",css_selected);
					break;
				case TAB.PERSONAL:
					liPersonal.Attributes.Add("class",css_selected);
					break;
				case TAB.MEDIA_UPLOADS:
					liUploads.Attributes.Add("class",css_selected);
					break;
				case TAB.MEMBERS:
					liMembers.Attributes.Add("class",css_selected);
					break;
				case TAB.ADMIN:
					liAdmin.Attributes.Add("class",css_selected);
					break;
				case TAB.BLOGS:
					liBlogs.Attributes.Add("class",css_selected);
					break;
			}
			
		}

		private void SetChannelProperties()
		{
			DataRow drChannel = CommunityUtility.GetCommunity(ChannelId);
			if(drChannel != null)
			{
				ChannelName = drChannel["name"].ToString();
				ChannelOwnerId = Convert.ToInt32(drChannel["creator_id"]);
			}
		}

		/// <summary>
		/// The SubTab to display
		/// </summary>
		public TAB ActiveTab
		{
			get 
			{
				return m_ActiveTab;
			} 
			set
			{
				m_ActiveTab = value;
			}
		}

		/// <summary>
		/// this really should be stored in channelNav
		/// </summary>
		public enum TAB
		{
			EDIT,
			LAYOUT,
			DESIGN,
			PROFILE_PHOTO,
			INTERESTS,
			PERSONAL,
			MEDIA_UPLOADS,
			MEMBERS,
			ADMIN,
			BLOGS,
			NONE
		}

		public int ChannelId
		{
			get 
			{
				if (ViewState ["ChannelNavChannelId"] != null)
				{
					return Convert.ToInt32(ViewState ["ChannelNavChannelId"]);
				}
				else
				{
					return 0;
				}
			} 
			set
			{
				ViewState ["ChannelNavChannelId"] = value;
				SetChannelProperties();
			}
		}
		
		public string ChannelName
		{
			get 
			{
				if (ViewState ["ChannelNavChannelName"] != null)
				{
					return ViewState ["ChannelNavChannelName"].ToString();
				}
				else
				{
					return "";
				}
			} 
			set
			{
				ViewState ["ChannelNavChannelName"] = value;
			}
		}
		public int ChannelOwnerId
		{
			get 
			{
				if (ViewState ["ChannelNavChannelOwnerId"] != null)
				{
					return Convert.ToInt32(ViewState ["ChannelNavChannelOwnerId"]);
				}
				else
				{
					return 0;
				}
			} 
			set
			{
				ViewState ["ChannelNavChannelOwnerId"] = value;
			}
		}

		public int PageId
		{
			get 
			{
				if (ViewState ["ChannelNavPageId"] != null)
				{
					return Convert.ToInt32(ViewState ["ChannelNavPageId"]);
				}
				else
				{
					return 0;
				}
			} 
			set
			{
				ViewState ["ChannelNavPageId"] = value;
			}
		}



		protected TAB m_ActiveTab = TAB.EDIT;

		protected HtmlAnchor aEdit, aLayout, aDesign, aPhoto, aInterests, aPersonal, aBlogs, aUploads, aMembers, aAdmin;
		protected HtmlGenericControl liEdit, liLayout, liDesign, liPhoto, liInterests, liPersonal, liBlogs;
		protected HtmlGenericControl liChanName, liUploads, liMembers, liAdmin;


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
