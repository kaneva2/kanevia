<%@ Control Language="c#" AutoEventWireup="false" Codebehind="NavMyKaneva.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.NavMyKaneva" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<div id="secondary">
	<ul>
		<li id="navProfile" runat="server"><a id="aProfile" runat="server">My Profile</a></li>
		<li id="navMessages" runat="server"><a id="aMessages" runat="server">My Messages</a></li>
		<li id="navFriends" runat="server"><a id="aFriends" runat="server">My Friends</a></li>	
		<li id="navUploads" runat="server"><a id="aUploads" runat="server">My Media Library</a></li>
		<li id="navChannels" runat="server"><a id="aChannels" runat="server">My Communities</a></li>
		<li id="nav3dApps" runat="server"><a id="a3dApps" runat="server">My Worlds</a></li>
		<li id="navAccount" runat="server"><a id="aAccount" runat="server">My Account</a></li>
	</ul>				
</div>
