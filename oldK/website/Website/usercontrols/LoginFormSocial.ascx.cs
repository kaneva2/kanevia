///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KlausEnt.KEP.Kaneva.usercontrols
{
    public partial class LoginFormSocial : System.Web.UI.UserControl
    {
        protected void Page_Load (object sender, EventArgs e)
        {
            string loginUrl = KanevaGlobals.SSLLogin ? "https://" : "http://";
            loginUrl += KanevaGlobals.SiteName + "/loginSecure.aspx";

            loginSubmit.PostBackUrl = loginUrl;

            if (!IsPostBack)
            {
                aLostPassword.HRef = ResolveUrl ("~/lostPassword.aspx");

                // Set the focus
                Page.SetFocus (txtUserName);
            }

            // This script catches the enter key in case there is a button higher in priority.
            string strJavascript = "<script language=\"JavaScript\">\n<!-- \n" +
              "function checkEnter(event)\n{\n" +
              "if ((event.which && event.which == 13) || (event.keyCode && event.keyCode == 13))\n" +
                      "{\n event.returnValue=false; \n event.cancel=true; \n" + Page.ClientScript.GetPostBackEventReference (this.loginSubmit, "", false) + "; \n return false; \n }\n else \n{ \n return true; \n}" +
              "}\n//--></script>\n";

            if (!Page.ClientScript.IsClientScriptBlockRegistered (GetType (), "checkEnter"))
            {
                Page.ClientScript.RegisterClientScriptBlock (GetType (), "checkEnter", strJavascript);
            }
        }

        #region Properties

        public string Username
        {
            get
            {
                return txtUserName.Value;
            }
        }
        public string PassWord
        {
            get
            {
                return txtPassword.Value;
            }
        }
        public bool RememberMe
        {
            get
            {
                return chkRememberLogin.Checked;
            }
        }
        
        #endregion
    }
}