///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using log4net;

namespace KlausEnt.KEP.Kaneva.usercontrols
{
    public class PassesData : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.ContentType = "text/plain";
            Response.CacheControl = "no-cache";
            Response.AddHeader("Pragma", "no-cache");
            Response.Expires = -1;

            if (KanevaWebGlobals.CurrentUser != null)
            {
                if (!IsPostBack)
                {
                    ShowAvailablePasses();
                }

            }
        }

        /// <summary>
        /// ShowPasses
        /// </summary>
        private void ShowAvailablePasses ()
        {
            try
            {
                string filter = "";

                //check to see if user is minor
                if (!KanevaWebGlobals.CurrentUser.IsAdult)
                {
                    filter += KanevaGlobals.AccessPassGroupID;
                }

                //get all the users currently owned passes
                DataTable usersSubscriptions = new SubscriptionFacade ().GetUnpurchasedSubscriptions (KanevaWebGlobals.CurrentUser.UserId, filter);

                // A/B Test VIP Tiers
                string vipExperimentId = "7c5f1ad8-a12d-11e6-84f2-a3cf223dc93b";
                //string vipGroupA = "8b67bc97-a12d-11e6-84f2-a3cf223dc93b";
                string vipGroupB = "9b6d0499-a12d-11e6-84f2-a3cf223dc93b";

                ExperimentFacade experimentFacade = new ExperimentFacade();
                UserExperimentGroup userExperimentGroup = experimentFacade.GetUserExperimentGroupCommon(KanevaWebGlobals.CurrentUser.UserId, vipExperimentId);

                // If user is not in Group B, then remove the 4.99 special
                if (userExperimentGroup != null && userExperimentGroup.GroupId != vipGroupB)
                {
                    DataRow[] rows = usersSubscriptions.Select("promotion_id = 91");
                    if (rows.Length > 0)
                    {
                        usersSubscriptions.Rows.Remove(rows[0]);
                    }
                }
                // -- End A/B test code



                if (usersSubscriptions.Rows.Count > 0)
                {
                    divAvailablePasses.Visible = true;
                    rptPasses.DataSource = usersSubscriptions;
                    rptPasses.DataBind ();
                }
            }
            catch (Exception ex)
            {
                m_logger.Error ("Error processing pass promotions data", ex);
                divAvailablePasses.Visible = false;
            }
        }
        
        
        #region Declerations

        protected System.Web.UI.HtmlControls.HtmlGenericControl divSubscibedPasses;
        protected System.Web.UI.HtmlControls.HtmlGenericControl pAP;
        protected System.Web.UI.HtmlControls.HtmlGenericControl pVIP;
        protected System.Web.UI.HtmlControls.HtmlGenericControl divAvailablePasses;
        protected Repeater rptPasses;

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

        #endregion Declerations

    }
}
