<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BlastComments.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.usercontrols.BlastComments" %>

<asp:Literal runat="server" ID="litLoadComments"></asp:Literal>

<div class="commentBox" id="divCommentBox" runat="server">
	<div class="allComments" id="divViewAllComments" runat="server"><a href="javascript:void(0);" id="aViewAll" runat="server" visible="false"></a></div><!-- allComments-->
	<asp:TextBox ID="txtBlastComment" onKeyPress="return enter(event);" TextMode="MultiLine" wrap="true" rows="1" class="textInput txtComment" text="Add a Comment..." maxlength="140" runat="server"/>
	<div class="buttonsComment" id="divButtonsComment" runat="server">
	<a id="btnAddComment" class="button_add" style="display:none" runat="server">Add Comment</a>
	</div><!-- button-->
</div><!-- commentBox-->

<asp:Literal runat="server" ID="litInitComments"></asp:Literal>
