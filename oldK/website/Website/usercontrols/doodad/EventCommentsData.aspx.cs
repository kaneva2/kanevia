///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using log4net;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.usercontrols
{
    public partial class EventCommentsData : BasePage
    {
        #region Declerations

        private int commentsPerPage = 4;
        private int pageNumber = 1;
        private int eventId = 0;
        private string eventBlastId = string.Empty;
        private string eventBlastCommentId = string.Empty;
        string commentText = "";

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

        #endregion Declerations
        
        protected void Page_Load (object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (KanevaWebGlobals.CurrentUser != null && KanevaWebGlobals.CurrentUser.UserId > 0)
                {
                    GetRequestValues ();
                    
                    //  Delete an entry comment
                    if (Request["del"] != null)
                    {   // Delete a contest or contest entry comment
                        if (EventId > 0 && EventBlastCommentId != string.Empty)
                        {
                            if (CanDelete ())
                            {
                                GetEventFacade.DeleteEventComment (EventId, EventBlastId, EventBlastCommentId);
                            }
                        }
                    }  // Add a comment
                    else if (Request["add"] != null)
                    {
                        // check for blank message
                        if (CommentText.Length == 0)
                        {
                            litJS.Text = "<script type=\"text/javascript\">alert('You can not submit a blank comment.');</script>";
                            return;
                        }

                        // check for injection scripts
                        if (KanevaWebGlobals.ContainsInjectScripts (CommentText))
                        {
                            m_logger.Warn ("User " + KanevaWebGlobals.CurrentUser.UserId.ToString () + " tried to script from IP " + Common.GetVisitorIPAddress());
                            litJS.Text = "<script type=\"text/javascript\">alert('Your input contains invalid scripting, please remove script code and try again.');</script>";
                            return;
                        }

                        // check for restricted words
                        if (KanevaWebGlobals.isTextRestricted (CommentText, Constants.eRESTRICTION_TYPE.POTTY_MOUTH))
                        {
                            litJS.Text = "<script type=\"text/javascript\">alert('" + Constants.VALIDATION_REGEX_RESTRICTED_WORD_MESSAGE + "');</script>";
                            return;
                        }

                        // Make sure comment does not exceed max length
                        if (Server.HtmlDecode (CommentText).Length > KanevaGlobals.MaxContestLength)         
                        {
                            litJS.Text = "<script type=\"text/javascript\">alert('Your comment is too long.  Comments must be " + KanevaGlobals.MaxContestLength.ToString () + " characters or less.');</script>";
                            return;
                        }

                        if (EventId > 0)
                        {
                            GetEventFacade.InsertComment (EventId, EventBlastId, KanevaWebGlobals.CurrentUser.UserId, CommentText);

                            try
                            {
                                // Get the comments so we can send notifications to blast owner and others who have commented
                                Event evt = GetEventFacade.GetEvent (EventId);
                                PagedList<EventComment> commentUsers = GetEventFacade.GetEventComments (EventId, EventBlastId, PageNum, 1000, "created_date DESC", "user_id");
                                SendCommentNotifications (commentUsers, CommentText, evt);
                            }
                            catch {}
                        }
                    }

                    Response.ContentType = "text/plain";
                    Response.CacheControl = "no-cache";
                    Response.AddHeader ("Pragma", "no-cache");
                    Response.Expires = -1;

                    // Get the comments
                    rptComments.DataSource = GetEventFacade.GetEventComments (EventId, EventBlastId, PageNum, commentsPerPage, "created_date DESC", ""); 
                    rptComments.DataBind ();
                }
            }
        }

        private void SendCommentNotifications (PagedList<EventComment> comments, string commentText, Event currentEvent)
        {
            try
            {
                Community community = GetCommunityFacade.GetCommunity(currentEvent.CommunityId);
                int evtGameId = community.WOK3App.GameId;

                Message message = new Message ();
               
                foreach (EventComment ec in comments)
                {
                    // We need one requestId per comment.
                    string requestId = string.Empty;

                    if (ec.Creator.EmailNotifications && ec.Creator.EmailIsValidated)
                    {
                        // Don't send email to user that just created the comment
                        if (KanevaWebGlobals.CurrentUser.UserId != ec.Creator.UserId)
                        {
                            requestId = GetUserFacade.GetTrackingRequestId(requestId, Constants.cREQUEST_TRACKING_REQUEST_TYPE_ID_EVENT_BLAST_COMMENT, KanevaWebGlobals.CurrentUser.UserId, evtGameId, currentEvent.CommunityId);
                            string requestTypeSentIdEmail = GetUserFacade.InsertTrackingTypeSent(requestId, Constants.cREQUEST_TRACKING_MESSAGE_TYPE_ID_EMAIL, ec.Creator.UserId);
                            
                            MailUtilityWeb.SendEventEmail(currentEvent, Event.eMESSAGE_TYPE.EventBlastCommentNotification.ToString(), ec.Creator.Email, ec.Creator.UserId, KanevaWebGlobals.CurrentUser.UserId, ec.Comment, requestTypeSentIdEmail);
                        }
                    }

                    if (KanevaWebGlobals.CurrentUser.UserId != ec.Creator.UserId && KanevaGlobals.EnableEventKMails)
                    {
                        requestId = GetUserFacade.GetTrackingRequestId(requestId, Constants.cREQUEST_TRACKING_REQUEST_TYPE_ID_EVENT_BLAST_COMMENT, KanevaWebGlobals.CurrentUser.UserId, evtGameId, currentEvent.CommunityId);
                        string requestTypeSentIdPM = GetUserFacade.InsertTrackingTypeSent(requestId, Constants.cREQUEST_TRACKING_MESSAGE_TYPE_ID_PM, ec.Creator.UserId);

                        Event.EventMessage em = GetEventFacade.GetEventMessageDetails(Event.eMESSAGE_TYPE.EventBlastCommentNotification, currentEvent, KanevaWebGlobals.CurrentUser.Username, ResolveUrl("~/mykaneva/events/eventDetail.aspx"), commentText, requestTypeSentIdPM);

                        // Send them a Kmail
                        message = new Message (0, KanevaWebGlobals.CurrentUser.UserId, ec.Creator.UserId, em.Subject, em.Message,
                            new DateTime (), 0, (int) Constants.eMESSAGE_TYPE.PRIVATE_MESSAGE, 0, "U", "S");

                        // Send KMail
                        GetUserFacade.InsertMessage (message);
                    }
                }
            }
            catch { }
        }

        private void GetRequestValues ()
        {
            if (Request["eId"] != null)
            {
                EventId = Convert.ToInt32 (Request["eId"]);
            }

            // We had to replace the - with _ so it would be valid for JS variable and function names
            // Now we need to replace it back to original so it will match id in db
            if (Request["ebId"] != null)
            {
                EventBlastId = Request["ebId"].ToString().Replace('_', '-');
            }
 
            if (Request["cmtId"] != null)
            {
                EventBlastCommentId = Request["cmtId"].ToString ().Replace ('_', '-');
            }

            if (Request["p"] != null)
            {
                PageNum = Convert.ToInt32 (Request["p"]);
            }

            if (Request["cpp"] != null)
            {
                CommentsPerPage = Convert.ToInt32 (Request["cpp"]);
            }

            if (Request["cmt"] != null)
            {
                CommentText = Request["cmt"].ToString ();
            }
       }

        protected void rptComments_ItemDataBound (Object Sender, RepeaterItemEventArgs e)
        {
            if ((e.Item.ItemType == ListItemType.Item) || (e.Item.ItemType == ListItemType.AlternatingItem))
            {
                HtmlContainerControl divComment = ((HtmlContainerControl) e.Item.FindControl ("divComment"));
                HtmlContainerControl spnOwner = ((HtmlContainerControl) e.Item.FindControl ("spnOwner"));
                HtmlAnchor aDelete = ((HtmlAnchor) e.Item.FindControl ("aDelete"));

                if (divComment != null)
                {
                    // If user has already voted on this contest, they can't vote again
                    EventComment comment = (EventComment) e.Item.DataItem;
                    if (comment.EventOwnerUserId == comment.Creator.UserId)
                    {   // comment belongs to contest owner, apply special styling
                        divComment.Attributes.Add ("class", "feedbackBox ownercomment");
                    }

                    // If user is admin, then display delete option
                    if (CanDelete ())
                    {
                        aDelete.Visible = true;
                        // since this entire method call is a string you have to pass the type enum (type.EVENT) as a string and it will get
                        // evaluated in the js. If you don't pass as a string it gets passed as undefined
                        aDelete.Attributes.Add("onclick", "DeleteComment({type:'type.EVENT',eventId: '" + comment.EventId + "',eventBlastId: '" + comment.EventBlastId.Replace('-', '_') + "',eventBlastCommentId: '" + comment.EventBlastCommentId.Replace('-', '_') + "'});");
                    }                                      
                }                                          
            }
        }

        protected bool CanDelete ()
        {
            bool allowEditing = false;

            //check security level
            switch (KanevaWebGlobals.CheckUserAccess ((int) SitePrivilege.ePRIVILEGE.USER_DATA_ADMIN))
            {
                case (int) SitePrivilege.eACCESS_LEVEL.ACCESS_FULL:
                    allowEditing = true;
                    break;
                case (int) SitePrivilege.eACCESS_LEVEL.ACCESS_READ:
                    break;
                case (int) SitePrivilege.eACCESS_LEVEL.ACCESS_NONE:
                    break;
                default:
                    break;
            }
            return allowEditing;
        }

        #region Properties

        private int PageNum
        {
            set { this.pageNumber = value; }
            get { return this.pageNumber; }
        }
        private int CommentsPerPage
        {
            set { this.commentsPerPage = value; }
            get { return this.commentsPerPage; }
        }
        private int EventId
        {
            set { this.eventId = value; }
            get { return this.eventId; }
        }
        private string EventBlastId
        {
            set { this.eventBlastId = value; }
            get { return this.eventBlastId; }
        }
        private string EventBlastCommentId
        {
            set { this.eventBlastCommentId = value; }
            get { return this.eventBlastCommentId; }
        }
        private string CommentText
        {
            set { this.commentText = value; }
            get { return this.commentText; }
        }

        #endregion Properties
    }
}