///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.usercontrols.doodad
{
    public partial class UserProfileData : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.ContentType = "text/plain";
            Response.CacheControl = "no-cache";
            Response.AddHeader("Pragma", "no-cache");
            Response.Expires = -1;


            // Set the current user for this control

            // Get the user info
            ShowUserInfo();

            // Get the user raves
            ShowRaves();

            // Get the fame
            ShowWorldFame();

            // Show user pass subscriptions
            ShowUserActivePasses();
        }

        /// <summary>
        /// ShowUserInfo
        /// </summary>
        private void ShowUserInfo()
        {
            imgAvatar.Src = GetProfileImageURL(Current_User.ThumbnailSquarePath, "sq", Current_User.Gender, Current_User.FacebookSettings.UseFacebookProfilePicture, Current_User.FacebookSettings.FacebookUserId);
            imgAvatar.Alt = Current_User.Username;

            lblUserName.Text = Current_User.Username;
            lnkAvatarImg.HRef = lnkUsername.HRef = ResolveUrl("~/community/ProfilePage.aspx");
        }

        private void ShowRaves()
        {
            lblRaves.Text = Current_User.NumberOfDiggs == 0 ? "0" : Current_User.NumberOfDiggs.ToString("#,###");
        }

        private void ShowWorldFame()
        {
            try
            {
                // Fame Game Info (may be other types fame in the future)
                FameFacade fameFacade = new FameFacade();
                UserFame userFame = fameFacade.GetUserFame(Current_User.UserId, (int)FameTypes.World);

                if (userFame != null)
                {
                    lblFameLevel.Text = userFame.CurrentLevel.LevelNumber.ToString();
                }
                else
                {
                    lblFameLevel.Text = "0";
                }
            }
            catch (Exception)
            {
                lblFameLevel.Text = "0";
            }
        }

        private void ShowUserActivePasses()
        {
            try
            {
                imgAP.Visible = KanevaWebGlobals.CurrentUser.UsersSubscriptions.HasAccessPass ? true : false;
                imgVIP.Visible = KanevaWebGlobals.CurrentUser.UsersSubscriptions.HasVIPPass ? true : false;

                //            imgAP.Visible = imgVIP.Visible = true;

            }
            catch { }
        }


        // private 
        private User Current_User = KanevaWebGlobals.CurrentUser;
    }
}