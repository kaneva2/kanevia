﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TopWorldsData.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.usercontrols.TopWorldsData" %>

<asp:panel id="pnlTopWorlds" clientidmode="static" runat="server" visible="false">
<script>
    $j(document).ready(function () {
        $j('.worldcontainer').mouseover(function () {
            $j(this).addClass('hilite');
        })
        .mouseout(function () {
            $j(this).removeClass('hilite');
        });
    });
</script>

<div class="topworldscontainer" id="divTopWorlds" runat="server">

    <div>
        <h2>Popular Worlds</h2>
        <a id="lbViewAll" runat="server" class="btnxsmall grey viewallbtn" href="~/community/channel.kaneva"><span>View All</span></a>
    <div style="clear:both;"></div>
    </div>
    <div style="margin:0 auto;">
        <asp:repeater id="rptTopWorlds" runat="server" OnItemDataBound="rptTopWorlds_ItemDataBound">
            <itemtemplate>
                <a id="aMeet3D" runat="server">
                <div class="worldcontainer">
                    <div class="world" id="divWorld" runat="server">
                        <div class="framesize-small">
                            <img src='<%# GetBroadcastChannelImageURL (DataBinder.Eval(Container.DataItem, "image_path").ToString ().Replace("_me.jpg", "_sm.jpg"), "sm") %>'></img>
                        </div>
                        <div class="worldname"><%# DataBinder.Eval(Container.DataItem, "location") %></div>
                        <div class="population"><div>Population:<br /><%# GetPopulationText(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "population"))) %></div></div>
                        <div class="worldplaynow"><a class="btnxsmall blue" runat="server" id="btnMeet3D" href="javascript:void(0);"><span>Play Now</span></a></div>
                    </div>
                </div></a>
            </itemtemplate>
        </asp:repeater>
    
        <div style="clear:both;"></div>
    </div>
</div>

</asp:panel>