<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NotificationsData.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.usercontrols.NotificationsData" %>

<asp:Literal id="litJS" runat="server"></asp:Literal>


<asp:repeater EnableViewState="True" id="rptNotifications" runat="server" onitemdatabound="rptNotifications_ItemDataBound">
	<itemtemplate>
		<p id="pContainer" class="notificationDDkaneva" runat="server"></p>											  
	</itemtemplate>  
</asp:Repeater>  

<div id="divNoNotifications" runat="server" visible="false" class="noData">You have 0 notifications.</div>