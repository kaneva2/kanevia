///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

namespace KlausEnt.KEP.Kaneva.usercontrols
{
    public partial class ContestComments : Kaneva.mykaneva.widgets.ModuleViewBaseControl
    {
        protected void Page_Load (object sender, EventArgs e)
        {
            StringBuilder clientScript = new StringBuilder ();

            // Load JS files
            RegisterJavaScript ("jscript/jquery/jquery-1.8.2.min.js");
            RegisterJavaScript("jscript/doodad/comment.js?v3");

            // This div is where the comments are loaded into
            litLoadComments.Text = "<div class=\"commentText\" id=\"pComments" + ContestId.ToString() + ContestEntryId.ToString () + "\">" +
                "<div class=\"loadingText\">Loading Comments...</div></div>";

            // If there are comments init them
            if (NumberOfComments > 0)
            {
                litInitComments.Text = "<script type=\"text/javascript\">BlastCommInit({type: type.CONTEST, contestId: \"" + ContestId.ToString() + "\", contestEntryId: \"" + ContestEntryId.ToString () + "\"});</script>";
            }
            else
            {
                litLoadComments.Text = "<div id=\"pComments" + ContestId.ToString () + ContestEntryId.ToString () + "\"></div>";
            }

            // Show view all?
            divViewAllComments.Visible = false;
            if (NumberOfComments > CINITIAL_COMMENTS)
            {
                string hideBox = "";
                if (!ShowCommentsBox) { hideBox = "$j('#" + divCommentBox.ClientID + "').hide();"; }
                
                divViewAllComments.Visible = true;
                // since this entire method call is a string you have to pass the type enum (type.CONTEST) as a string and it will get
                // evaluated in the js. If you don't pass as a string it gets passed as undefined
                litViewAllComments.Text = "<a href=\"javascript:void(0);\" onclick=\"ViewAllComments({type: 'type.CONTEST',contestId: '" + ContestId.ToString() + "',contestEntryId: '" + ContestEntryId.ToString() + "',viewAllContainer: $j('#" + divViewAllComments.ClientID + "')});" + hideBox + "\">View All " + NumberOfComments.ToString() + " Comments</a>";
            }

            if (ShowCommentsBox)
            {
                // Set up add comments
//                txtBlastComment.Attributes.Add("onfocus", "ShowAddComment($j(this),$j('#" + btnAddComment.ClientID + "'),defText);");
//                txtBlastComment.Attributes.Add("onblur", "ResetAddComment($j(this),defText);");
//                txtBlastComment.Attributes.Add("onkeyup", "ResizeTextArea($j(this));");

                // since this entire method call is a string you have to pass the type enum (type.CONTEST) as a string and it will get
                // evaluated in the js. If you don't pass as a string it gets passed as undefined
                btnAddComment.Attributes.Add ("onclick", "javascript:callGAEvent('Category','Comment');AddComment({type: 'type.CONTEST',contestId: '" + ContestId.ToString () + "',contestEntryId: '" + ContestEntryId.ToString () + "',txt: $j('#" + txtBlastComment.ClientID + "'),btn: $j('#" + btnAddComment.ClientID + "'),div: $j('#" + divViewAllComments.ClientID + "')});return false;");

                divCommentBox.Visible = true;
                txtBlastComment.Visible = true;
                divButtonsComment.Visible = true;
            }
            else
            {
                txtBlastComment.Visible = false;
                divButtonsComment.Visible = false;
                if (NumberOfComments <= CINITIAL_COMMENTS)
                {
                    divCommentBox.Visible = false;
                }
            }
        }

        # region Properties

        public Int64 ContestEntryId
        {
            get { return this._contestEntryId; }
            set { this._contestEntryId = value; }
        }

        public Int64 ContestId
        {
            get { return this._contestId; }
            set { this._contestId = value; }
        }

        public int NumberOfComments
        {
            get { return this._numberOfComments; }
            set { this._numberOfComments = value; }
        }

        public bool ShowCommentsBox
        {
            get { return this._showCommentsBox; }
            set { this._showCommentsBox = value; }
        }

        #endregion Properties


        #region Declerations

        private Int64 _contestEntryId = 0;
        private Int64 _contestId = 0;
        private int _numberOfComments = 0;
        private const int CINITIAL_COMMENTS = 2;
        private bool _showCommentsBox = true;

        #endregion Declerations
    }
}