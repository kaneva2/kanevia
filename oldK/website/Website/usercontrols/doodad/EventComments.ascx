﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EventComments.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.usercontrols.EventComments" %>
	
<asp:Literal runat="server" ID="litLoadComments"></asp:Literal>

<div class="commentBox" id="divCommentBox" runat="server">
	<div class="allComments" id="divViewAllComments" runat="server"><asp:Literal runat="server" ID="litViewAllComments"></asp:Literal></div><!-- allComments-->
	<asp:TextBox ID="txtBlastComment" onKeyPress="return enter(event);" TextMode="MultiLine" wrap="true" rows="1" class="textInput txtComment" text="Add a Comment..." maxlength="140" runat="server"/>
	<div class="buttonsComment" id="divButtonsComment" runat="server">
	<a id="btnAddComment" class="button_add" style="display:none" runat="server">Add Comment</a>
	</div><!-- button-->
</div><!-- commentBox-->

<asp:Literal runat="server" ID="litInitComments"></asp:Literal>
