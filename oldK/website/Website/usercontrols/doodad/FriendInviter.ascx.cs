///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Kaneva.usercontrols
{
    public partial class FriendInviter : Kaneva.mykaneva.widgets.ModuleViewBaseControl
    {
        protected void Page_Load (object sender, EventArgs e)
        {
            sitename.Value = "http://" + KanevaGlobals.SiteName + "/";

            fbConnected.Visible = true;
            fbNotConnected.Visible = false;
            if (KanevaWebGlobals.CurrentUser.FacebookSettings.FacebookUserId > 0)
            {
 //               fbConnected.Visible = true;
            }
            else
            {
  //              fbNotConnected.Visible = true;
            }
        }
    }
}