﻿<%@ Page Language="C#" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="EventsListData.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.usercontrols.doodad.EventsListData" %>



<div class="noData" id="divNoEvents" runat="server" Visible="False">You have 0 upcoming events.</div> 

	<!-- Birthdays -->
	<asp:repeater id="rptBirthdays" runat="server" onitemdatabound="rptBirthdays_ItemDataBound">
		<headertemplate>
			<div class="birthdays">
				<div class="header">
					<div class="title">Birthdays</div>
					<ul id="ulSeeAllBdays" class="showAll" runat="server" visible="false"><li><a href="#" onclick="Effect.toggle('bd','slide'); return false;">View All</a></li></ul>
				</div>
		</headertemplate>
		<itemtemplate>
				<asp:literal id="litBirthdays" runat="server"></asp:literal>
				<div class="birthday"><asp:HyperLink id="hlUsername" runat="server"></asp:HyperLink> <span class="time" id="spnBdayTime" runat="server"></span></div>
		</itemtemplate>
		<footertemplate>
				<asp:literal id="litBirthdaysClosingTag" runat="server"></asp:literal>
			</div>
		</footertemplate>
	</asp:repeater>

	<!-- Anniversaries -->
	<asp:repeater id="rptAnniversaries" runat="server" onitemdatabound="rptAnniversaries_ItemDataBound">
		<headertemplate>
			<div class="anniversaries">
				<div class="header">
					<div class="title">Anniversaries</div>
					<ul id="ulSeeAllAnniversaries" class="showAll" runat="server" visible="false"><li><a href="#" onclick="Effect.toggle('an','slide'); return false;">View All</a></li></ul>
				</div>
		</headertemplate>
		<itemtemplate>
				<asp:literal id="litAnniversaries" runat="server"></asp:literal>
				<div class="anniversary"><asp:HyperLink id="hlUsername" runat="server"></asp:HyperLink> <span class="time" id="spnAnniversaryTime" runat="server"></span></div>
		</itemtemplate>
		<footertemplate>
				<asp:literal id="litAnniversariesClosingTag" runat="server"></asp:literal>
			</div>
		</footertemplate>
	</asp:repeater>

	<!-- Your Events -->
	<asp:repeater id="rptEvents" runat="server" onitemdatabound="rptEvents_ItemDataBound" >
		<headertemplate>
			<div class="events">
				<div class="header">
					<div class="title">Your Events</div>
					<ul id="ulSeeAllYourEvents" class="showAll" runat="server" visible="false"><li><a href="#" onclick="Effect.toggle('ye','slide'); return false;">View All</a></li></ul>
				</div>
		</headertemplate>
		<itemtemplate>
				<asp:literal id="litYourEvents" runat="server"></asp:literal>
				<div class="event">
					<asp:HyperLink ID="hlEventName" runat="server" class="eventtitle"></asp:HyperLink> <span class="eventEdit" id="spnEdit" runat="server" visible="false">(<asp:HyperLink ID="hlEventEdit" runat="server"></asp:HyperLink>)</span>
					<p><asp:Literal ID="litEventWhen" runat="server"></asp:Literal> at
					<a id="lbGotoWorld" runat="server">GO 3D</a>
					<div class="eventAccept" id="divAttend" runat="server" visible="false"><a ID="lbAttend" runat="server">Attend &#187;</a></div></p>
				</div>
		</itemtemplate>	
		<footertemplate>
				<asp:literal id="litYourEventsClosingTag" runat="server"></asp:literal>
			</div>
		</footertemplate>
	</asp:repeater>	

	<!-- Premium Events -->
	<asp:repeater id="rptEventsPremium" runat="server" onitemdatabound="rptEventsPremium_ItemDataBound">
		<headertemplate>
			<div class="events prem">
				<div class="header">
					<div class="title">Premium Events</div>
					<ul id="ulSeeAllYourPremiumEvents" class="showAll" runat="server" visible="false"><li><a href="#" onclick="Effect.toggle('pe','slide'); return false;">View All</a></li></ul>
				</div>
		</headertemplate>
		<itemtemplate>
				<asp:literal id="litYourPremiumEvents" runat="server"></asp:literal>
				<div class="event">
					<asp:HyperLink ID="hlEventName" runat="server"></asp:HyperLink> <span class="eventEdit" id="spnEdit" runat="server" visible="false">(<asp:HyperLink ID="hlEventEdit" runat="server"></asp:HyperLink>)</span>
					<p><asp:Literal ID="litEventWhen" runat="server"></asp:Literal> at
					<a id="lbGotoWorld" runat="server">GO 3D</a>
				</div>
		</itemtemplate>	
		<footertemplate>
				<asp:literal id="litYourPremiumEventsClosingTag" runat="server"></asp:literal>
			</div>
		</footertemplate>
	</asp:repeater>		
    
