<%@ Page Language="C#" EnableViewState="False" AutoEventWireup="true" CodeBehind="BlastCommentsData.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.usercontrols.BlastCommentData" %>

<asp:Literal id="litJS" runat="server"></asp:Literal>

<asp:repeater id="rptComments" runat="server">
	<itemtemplate>
		<div class="feedbackBox">
			<div class="feedbackImg">
				<a href="<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "NameNoSpaces").ToString ())%>">
					<img src='<%#GetProfileImageURL (DataBinder.Eval(Container.DataItem, "ThumbnailSmallPath").ToString (), "sq", DataBinder.Eval(Container.DataItem, "Gender").ToString (), Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "OwnerFacebookSettings.UseFacebookProfilePicture")), Convert.ToUInt64(DataBinder.Eval(Container.DataItem, "OwnerFacebookSettings.FacebookUserId")) )%>' border="0" />
				</a>
			</div>
			<div class="feedbackText">
				<p><a href="<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "NameNoSpaces").ToString ())%>"><%# DataBinder.Eval(Container.DataItem, "NameNoSpaces")%>:</a>
				<%# DataBinder.Eval(Container.DataItem, "Message")%></p>
				<div class="time">(<%# FormatDateTimeSpan (DataBinder.Eval(Container.DataItem, "DateCreated"), DataBinder.Eval(Container.DataItem, "CurrentTime")) %>)</div>
			</div>
		</div>
	</itemtemplate>
</asp:repeater>

