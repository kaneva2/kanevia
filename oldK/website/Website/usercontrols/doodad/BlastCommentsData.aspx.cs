///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.usercontrols
{
    public partial class BlastCommentData : BasePage
    {
        protected void Page_Load (object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (KanevaWebGlobals.CurrentUser != null && KanevaWebGlobals.CurrentUser.UserId > 0)
                {
                    ulong entryId = 0;
                    BlastFacade blastFacade = new BlastFacade ();

                    if (Request["del"] != null)
                    {
                        if (Request["eId"] != null)
                        {
                            entryId = Convert.ToUInt64(Request["eId"]);

                            // Only allow admin or owner
                            Blast blast = blastFacade.GetBlast(entryId);

                            if (Convert.ToInt32 (blast.SenderId).Equals(KanevaWebGlobals.CurrentUser.UserId) || IsAdministrator() ||
								//the recipient of a request blast can delete the blast
								blast.RecipientId.Equals(KanevaWebGlobals.CurrentUser.UserId)  
								)
                            {
                                blastFacade.DeleteBlast (entryId);
                            }
                        }

                    }
                    if (Request["add"] != null)
                    {
                        string commentText = "";

                        if (Request["cmt"] != null)
                        {
                            commentText = Request["cmt"].ToString ();
                        }

                        // check for blank message
                        if (commentText.Length == 0)
                        {
                            litJS.Text = "<script type=\"text/javascript\">alert('You can not submit a blank comment.');</script>";
                            return;
                        }

                        // check for injection scripts
                        if (KanevaWebGlobals.ContainsInjectScripts (commentText))
                        {
                            //                 m_logger.Warn ("User " + KanevaWebGlobals.CurrentUser.UserId.ToString () + " tried to script from IP " + Common.GetVisitorIPAddress());
                            litJS.Text = "<script type=\"text/javascript\">alert('Your input contains invalid scripting, please remove script code and try again.');</script>";
                            return;
                        }

                        // check for restricted words
                        if (KanevaWebGlobals.isTextRestricted (commentText, Constants.eRESTRICTION_TYPE.POTTY_MOUTH))
                        {
                            litJS.Text = "<script type=\"text/javascript\">alert('" + Constants.VALIDATION_REGEX_RESTRICTED_WORD_MESSAGE + "');</script>";
                            return;
                        }

                        // Make sure comment does not exceed max length
                        if (Server.HtmlDecode(commentText).Length > KanevaGlobals.MaxBlastLength)         //** what is limit??
                        {
                            litJS.Text = "<script type=\"text/javascript\">alert('Your comment is too long.  Comments must be " + KanevaGlobals.MaxBlastLength.ToString () + " characters or less.');</script>";
                            return;
                        }

                        if (Request["eId"] != null)
                        {
                            entryId = Convert.ToUInt64 (Request["eId"]);

                            blastFacade.InsertBlastReply (entryId, KanevaWebGlobals.CurrentUser.UserId, commentText, KanevaWebGlobals.CurrentUser.IpAddress);

							#region send email notifications


							UserFacade userFacade = new UserFacade();
							Blast blast2 = blastFacade.GetBlast (entryId);

							int currentId = 0;		
					
							// current location
							string blastLocation = "";
							// profile, mykaneva, commnunity, 3dapp														
							if (Request.Url.ToString().EndsWith("ProfilePage.aspx"))
							{
								blastLocation = KanevaGlobals.GetPersonalChannelUrl(KanevaWebGlobals.CurrentUser.NameNoSpaces);
							}
							else 
							{
								blastLocation = Request.UrlReferrer.ToString();
							}
							
							// send to original person							
							// Don't send email if user is commenting on their own blast
							if (Convert.ToInt32(blast2.SenderId) != KanevaWebGlobals.CurrentUser.UserId)
							{								
								User blastOwner = userFacade.GetUser(Convert.ToInt32(blast2.SenderId));
								if (blastOwner.NotificationPreference.NotifyBlastComments)
								{
									MailUtilityWeb.SendBlastCommentNotificationEmail(blastOwner.Email, commentText, blastLocation, blastOwner.UserId, KanevaWebGlobals.CurrentUser.UserId);
								}
							}

							// send to other repliers
							foreach (BlastReply blastReply in blastFacade.GetBlastReplies(entryId, 1, 1000, "user_id"))
							{

								if (currentId != blastReply.UserId)
								{
									//send email to everyone but the commenter
									if ((blastReply.UserId != KanevaWebGlobals.CurrentUser.UserId) && (blastReply.UserId != blast2.SenderId))
									{
										User bpUser = userFacade.GetUser(blastReply.UserId);

										// Send Email
										if (bpUser.NotificationPreference.NotifyBlastComments)
										{
											MailUtilityWeb.SendBlastCommentNotificationEmail(bpUser.Email, commentText, blastLocation, bpUser.UserId, KanevaWebGlobals.CurrentUser.UserId);
										}
									}
									currentId = blastReply.UserId;
								}

							}
							
							#endregion

						}
                    }

                    Response.ContentType = "text/plain";
                    Response.CacheControl = "no-cache";
                    Response.AddHeader ("Pragma", "no-cache");
                    Response.Expires = -1;

                    int commentsPerPage = 4;
                    //    ulong entryId = 0;
                    int pageNumber = 1;

                    if (Request["eId"] != null)
                    {
                        entryId = Convert.ToUInt32 (Request["eId"]);
                    }

                    if (Request["p"] != null)
                    {
                        pageNumber = Convert.ToInt32 (Request["p"]);
                    }

                    if (Request["cpp"] != null)
                    {
                        commentsPerPage = Convert.ToInt32 (Request["cpp"]);
                    }

                    // If commenting on an asset
					rptComments.DataSource = blastFacade.GetBlastReplies(entryId, pageNumber, commentsPerPage, "_date_created");
                    rptComments.DataBind ();
                }
            }
        }

    }
}
