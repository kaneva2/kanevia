///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

namespace KlausEnt.KEP.Kaneva.usercontrols
{
    public partial class EventComments : Kaneva.mykaneva.widgets.ModuleViewBaseControl
    {
        protected void Page_Load (object sender, EventArgs e)
        {
            // This div is where the comments are loaded into
            litLoadComments.Text = "<div class=\"commentText\" id=\"pComments" + EventId.ToString () + EventBlastIdValidJS.ToString () + "\">" +
                "<div class=\"loadingText\">Loading Comments...</div></div>";

            // If there are comments init them
            if (NumberOfComments > 0)
            {
                litInitComments.Text = "<script type=\"text/javascript\">BlastCommInit({type: type.EVENT, eventId: \"" + EventId.ToString () + "\", eventBlastId: \"" + EventBlastIdValidJS.ToString () + "\"});</script>";
                
                // This is a temporary fix.  When the user posts a blast, the BlastCommInit function never gets fired
                // on the ajax postback.  The "Loading Comments..." label gets displayed and never cleared.  So for now
                // we will not display that text when a blast is posted.  TODO is figure out how to fire the BlastCommInit
                // Javascript function on Ajax postback.  This is issue with all blasts on the site, not just in events
                if (IsPostBack)
                {
                    litLoadComments.Text = "<div id=\"pComments" + EventId.ToString () + EventBlastIdValidJS.ToString () + "\"></div>";
                }
            }
            else
            {
                litLoadComments.Text = "<div id=\"pComments" + EventId.ToString () + EventBlastIdValidJS.ToString () + "\"></div>";
            }

            // Show view all?
            divViewAllComments.Style.Add ("display", "none");
            if (NumberOfComments > CINITIAL_COMMENTS)
            {
                string hideBox = "";
                if (!ShowCommentsBox) { hideBox = "$j('#" + divCommentBox.ClientID + "').hide();"; }

                divViewAllComments.Style.Add ("display", "block");
                // since this entire method call is a string you have to pass the type enum (type.EVENT) as a string and it will get
                // evaluated in the js. If you don't pass as a string it gets passed as undefined
                litViewAllComments.Text = "<a href=\"javascript:void(0);\" onclick=\"ViewAllComments({type: 'type.EVENT',eventId: '" + EventId.ToString() + "',eventBlastId: '" + EventBlastIdValidJS.ToString() + "',viewAllContainer: $('" + divViewAllComments.ClientID + "')});" + hideBox + "\">View All " + NumberOfComments.ToString() + " Comments</a>";
            }

            if (ShowCommentsBox)
            {
                // Set up add comments
//                txtBlastComment.Attributes.Add("onfocus", "ShowAddComment($j(this),$j('#" + btnAddComment.ClientID + "'),defText);");
//                txtBlastComment.Attributes.Add("onblur", "ResetAddComment($j(this),defText);");
//                txtBlastComment.Attributes.Add("onkeyup", "ResizeTextArea($j(this));");

                // since this entire method call is a string you have to pass the type enum (type.EVENT) as a string and it will get
                // evaluated in the js. If you don't pass as a string it gets passed as undefined
                btnAddComment.Attributes.Add("onclick", "javascript:callGAEvent('Category','EventComment');AddComment({type: 'type.EVENT',eventId: '" + EventId.ToString() + "',eventBlastId: '" + EventBlastIdValidJS.ToString() + "',txt: $j('#" + txtBlastComment.ClientID + "'),btn: $j('#" + btnAddComment.ClientID + "'),div: $j('#" + divViewAllComments.ClientID + "')});return false;");

                divCommentBox.Visible = true;                                                               
                txtBlastComment.Visible = true;
                divButtonsComment.Visible = true;
            }
            else
            {
                txtBlastComment.Visible = false;
                divButtonsComment.Visible = false;
                if (NumberOfComments <= CINITIAL_COMMENTS)
                {
                    divCommentBox.Visible = false;
                }
            }
        }

        # region Properties

        public int EventId
        {
            get { return this._eventId; }
            set { this._eventId = value; }
        }

        public string EventBlastId
        {
            get { return this._eventBlastId; }
            set { this._eventBlastId = value; }
        }

        public string EventBlastIdValidJS
        {
            get { return this._eventBlastId.Replace ('-', '_'); }
        }

        public int NumberOfComments
        {
            get { return this._numberOfComments; }
            set { this._numberOfComments = value; }
        }

        public bool ShowCommentsBox
        {
            get { return this._showCommentsBox; }
            set { this._showCommentsBox = value; }
        }

        #endregion Properties


        #region Declerations

        private int _eventId = 0;
        private string _eventBlastId = string.Empty;
        private int _numberOfComments = 0;
        private const int CINITIAL_COMMENTS = 2;
        private bool _showCommentsBox = true;

        #endregion Declerations
    }
}