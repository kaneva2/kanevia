///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Kaneva.usercontrols
{
    public partial class Raves : Kaneva.mykaneva.widgets.ModuleViewBaseControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            litCommunityId.Text = CommunityId.ToString();
        }

        public new int CommunityId
        {
            get { return this.communityId; }
            set { this.communityId = value; }
        }

        private int communityId = 0;
    }
}