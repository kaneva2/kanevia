///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva.usercontrols
{
    public partial class EventsList : Kaneva.mykaneva.widgets.ModuleViewBaseControl
    {

        #region Page Load

        protected void Page_Load(object sender, EventArgs e)
        {
            lnkAddEvent.Attributes.Add("href", ResolveUrl("~/mykaneva/events/eventForm.aspx"));
        }

        #endregion Page Load


    }
}