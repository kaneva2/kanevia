///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.usercontrols
{
    public class RaveData : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.ContentType = "text/plain";
            Response.CacheControl = "no-cache";
            Response.AddHeader("Pragma", "no-cache");
            Response.Expires = -1;

            int communityId = 0;
            if (Request["communityId"] != null && Request["communityId"].ToString ().Length > 0)
            {
                communityId = Convert.ToInt32 (Request["communityId"]);
            }

            if (communityId > 0)
            {
                PagedList<RaveTransaction> plRT = new TransactionFacade ().GetCommunityRaveTransactions (communityId, "cd.created_date > ADDDATE(NOW(),INTERVAL -1 DAY)", "mrh.create_datetime > ADDDATE(NOW(),INTERVAL -1 DAY)", "", 1, 1);
                Response.Write (plRT.TotalCount.ToString ());
            }
            else if (KanevaWebGlobals.CurrentUser != null)
            {
                PagedList<RaveTransaction> plRT = new TransactionFacade().GetCommunityRaveTransactionsByUser(KanevaWebGlobals.CurrentUser.UserId, false, "cd.created_date > ADDDATE(NOW(),INTERVAL -1 DAY)", "mrh.create_datetime > ADDDATE(NOW(),INTERVAL -1 DAY)","", 1, 1);
                Response.Write(plRT.TotalCount.ToString ());
            }

        }
    }
}
