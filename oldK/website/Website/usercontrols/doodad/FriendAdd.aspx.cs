///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.usercontrols.doodad
{
    public partial class FriendAdd : System.Web.UI.Page
    {
        protected void Page_Load (object sender, EventArgs e)
        {
            // Must be logged in
            if (!Request.IsAuthenticated)
            {
                return;
            }

            if (Request["uId"] != null)
            {
                string outTxt = "";

                int friendUserId = Convert.ToInt32 (Request["uId"]);

                if (friendUserId > 0)
                {
                    if (KanevaWebGlobals.CurrentUser.UserId > 0)
                    {
                        // Can't friend yourself
                        if (KanevaWebGlobals.CurrentUser.UserId.Equals (friendUserId))
                        {
                            return;
                        }

                        // Add them as a friend
                        UserFacade userFacade = new UserFacade();
                        int ret = userFacade.InsertFriendRequest(KanevaWebGlobals.CurrentUser.UserId, friendUserId, 
                            Page.Request.CurrentExecutionFilePath, Global.RequestRabbitMQChannel());

                        if (ret.Equals (1))	// already a friend
                        {
                            outTxt = "Already your friend";
                        }
                        else if (ret.Equals (2))  // pending friend request
                        {
                            outTxt = "Request Sent";
                        }
                        else
                        {
                            // send request email
                            try
                            {
                                MailUtilityWeb.SendFriendRequestEmail (KanevaWebGlobals.CurrentUser.UserId, friendUserId);
                            }
                            catch { }

                            outTxt = "Request Sent";
                        }
                    }
                }
                
                Response.ContentType = "text/plain";
                Response.CacheControl = "no-cache";
                Response.AddHeader ("Pragma", "no-cache");
                Response.Expires = -1;

                Response.Clear ();
                Response.Write (outTxt);
                Response.End ();
            }
        }
    }
}
