<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ActionItems.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.usercontrols.ActionItems" %>
 	

<div class="actionDD" id="divActionItems" runat="server">
    <h2>Action Items</h2>
    <div class="progress-container">
		<div id="divActionItemsProgressBar" runat="server" style="width:65%;"></div>
    </div>
	<div class="progress-containerText"><asp:literal id="litPercentComplete" runat="server"></asp:literal>% Complete</div>
    
	<asp:repeater id="rptActionItemList" runat="server">
		<itemtemplate>
			<%# DisplayActionItem (DataBinder.Eval (Container.DataItem, "isComplete"), DataBinder.Eval (Container.DataItem, "ChecklistText"), DataBinder.Eval (Container.DataItem, "url"))%>
		</itemtemplate>
		<footertemplate></div></footertemplate>
	</asp:repeater>		
    
    <p class="viewAll"><a href="javascript:void(0)" onclick="$('divHidItems').style.display='block';this.style.display='none';">View All Action Items &raquo;</a></p>
</div><!-- actionDD-->
