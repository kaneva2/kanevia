<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FriendInviter.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.usercontrols.FriendInviter" %>
<%@ Register TagPrefix="UserControl" TagName="ColorBox" Src="~/usercontrols/ColorBox.ascx" %>

<link href="/jscript/colorbox/colorbox.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/jscript/colorbox/jquery.colorbox.js"></script>


<div class="inviteDD">
	
	<div id="fbConnected" runat="server"> 
		<a class="invite_friends" href="~/mykaneva/invitefriend.aspx" runat="server">Invite Friends</a>
	</div>

	<div id="fbNotConnected" runat="server">
		<img id="Img1" onclick="FBLogin(FBAction.CONNECT);" src="~/images/facebook/FB_ConnectwithFriends.png" runat="server" />
		<p style="margin-top:6px;">Earn 1000 Rewards or more!</p>
	</div>

	<span style="display:none;">
	<img id="fb" src="~/images/connectwithfriends/facebook.png" runat="server" clientidmode="static" style="width:32px;" />
	<img id="yahoo" src="~/images/connectwithfriends/yahoo.png" runat="server" clientidmode="static" class="email-provider" style="width:32px;" />
	<img id="gmail" src="~/images/connectwithfriends/gmail.png" runat="server" clientidmode="static" class="email-provider" style="width:32px;" />
	<img id="windowslive" src="~/images/connectwithfriends/outlook.com.png" runat="server" clientidmode="static" class="email-provider" style="width:32px;" />
	<img id="aol" src="~/images/connectwithfriends/aol.png" runat="server" clientidmode="static" class="email-provider" style="width:32px;" />
	</span>

</div><!-- inviteDD -->
<input type="hidden" runat="server" clientidmode="static" id="sitename" value="" />

<script type="text/javascript">
	$j('#fb').click(function () {
		FBLogin(FBAction.CONNECT);
	});

	var popup = null,
	closedTimer = null
	statusTimer = null,
	import_id = null,
	importer = null,
	abort = false,
	state = null,
	processing = false,
	service = "";

	$j('.email-provider').click(function (ev) {
		if (processing) { return false; }
		ev.preventDefault();
		cleanUp();
		processing = true;

		$j.colorbox({
			html: '<p>Importing Contacts...<br/>You may need to disable your pop-up blocker.<br/><br/><img src="'
						+ $j('#site_url').val() + 'images/ajax-loader.gif'
						+ '" alt="" class="invite-loader"/></p>',
			scrolling: false,
			initialWidth: 100,
			initialHeight: 100,
			width: 400
		});
 
		service = $j(this).attr('id');

		if (service == 'gmail' || service == 'yahoo' || service == 'windowslive' || service == 'aol') {
			popup = window.open('', "login", 'height=420,width=600,location=no,toolbar=no,resizable=no,status=no,menubar=no');
			popup.document.body.innerHTML = 'Loading, please wait ...';
		}
			 
		$j.ajax({
			type: "POST",
			url: $j('#sitename').val() + "services/csimporter.asmx/Consent",
			data: '{"service":"' + service + '"}',
			contentType: "application/json; charset=utf-8",
			dataType: "json"
		}).done(function (data) {
			var obj = $j.parseJSON(data.d);
			if (!obj){
				$j.colorbox({html:'<p>We encountered an error trying to connect to ' + service + '</p>'});
				alert(1);
				cleanUp();
                return;
            }
            import_id = obj.ImportId;	 
		    popup.location = obj.Url;
		    popup.focus();
		    closedTimer = setInterval(checkPopup, 1000);
		    abort = false;
		    setTimeout(getContacts, 1000);
		}).fail(function (jqXHR, textStatus) {
            $j.colorbox({html:'<p>We encountered an error connecting to ' + service + '</p>'});
	//		alert(2);
			cleanUp();
            return;
		});
    });

	var objContacts;
	function getContacts() {
		if (!abort) {	  	 
			$j.ajax({
				type: "POST",
				url: $j('#sitename').val() + "services/csimporter.asmx/ImportStatus",
				data: '{"importId":"' + import_id + '"}',
				contentType: "application/json; charset=utf-8",
				dataType: "json"
            }).done(function (data) {  
				var obj = $j.parseJSON(data.d);
			    if (!obj){
               		$j.colorbox({html:'<p>We encountered an error retrieving your contacts.</p>'});
	//				alert(3);
					cleanUp();
                    return;
                }
                state = obj.State;	 
				if (obj.IsComplete === true && obj.IsError === false && import_id != null) {
					document.location = $j('#sitename').val() + "mykaneva/inviteFriend.aspx?id=" + import_id + "&service=" + service;
				} else {
					setTimeout(getContacts, 1000);
				}
			}).fail(function (jqXHR, textStatus) {
               	$j.colorbox({html:'<p>We encountered an error retrieving your contacts.</p>'});
//				alert(4);
				cleanUp();
                return;
			});
		}
	}

	function cleanUp() {	 
		if ($j.colorbox) { $j.colorbox.close(); }
		if (popup && !popup.closed) {
			popup.close();
		}
		clearInterval(closedTimer);
		clearInterval(statusTimer);
		popup = null;
		closedTimer = null;
		statusTimer = null;
		import_id = null;
		state = null;
		importer = null;
		processing = false;
	}

	function checkStatus() {
		if (state === null || state === 'inprogress') {
			abortImport();
		}
	}

	function abortImport() {
		abort = true;
		if (importer) {
			importer.abort();
		}
		cleanUp();
	}

	function checkPopup() {
		if (!popup || popup.closed) {
			popup = null;
			clearInterval(closedTimer);
			closedTimer = null;
			statusTimer = setInterval(checkStatus, 1000);
		}
	}

</script>