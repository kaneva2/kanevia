﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MessageSummary.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.usercontrols.doodad.MessageSummary" %>

<div class="navSummary" id="divMessageSummary" runat="server">

	<div class="shop" id="divShopContainer" runat="server"><a id="a3" href="http://shop.kaneva.com" runat="server" title="">Shop</a></div>

	<div class="messages"><a id="aMessages" href="~/mykaneva/messagecenter.aspx?mc=msg" runat="server" title="View personal messages in your Inbox.">Messages</a> <span class="new" id="spnNewMessages" runat="server"></span></div>

	<div class="friends"><a id="aFriendInvites" href="~/mykaneva/messagecenter.aspx?mc=fr" runat="server" title="View and approve people who want to be your friend.">Friends</a> <span class="new" id="spnNewFriendRequests" runat="server"></span></div>
		
	<div class="media"><a id="a4" href="~/asset/publishedItemsNew.aspx" runat="server" title="View your Media Library">Media</a></div>

	<div class="contests"><a id="aContests" href="~/mykaneva/contests.aspx" runat="server" title="View or create a contest.">Contests</a> </div>
	
	<div class="activity"><a id="a2" href="~/mykaneva/messagecenter.aspx?mc=act" runat="server" title="View recent activity for you and your friends.">Activities</a>  <span id="spnNewActivities" runat="server"></span></div>
 
</div>

<div class="navGo3D">
	<hr />
	<asp:linkbutton id="btnGo3D" runat="server" CssClass="btnmedium blue" text="Play Now" />
</div><!-- button-->

<div class="navSummary navWorlds" id="navWorlds">
<h2>Worlds</h2>
	<div><a id="A6" href="~/mykaneva/my3dapps.aspx" runat="server">My Worlds</a></div>
	<div id="divRequestContainer" runat="server"><a href="~/mykaneva/messagecenter.aspx?mc=ar" id="btnNav3DAppRequests" runat="server"  tooltip="">Requests</a>  <span class="new" id="spnNew3DAppRequests" runat="server"></span></div>
	<div id="Div5"><a href="~/mykaneva/messagecenter.aspx?mc=ci" id="btnNavCommunityInvites" runat="server">Invites</a>  <span class="worldreq new" id="spnNewCommunityInvites" runat="server"></span></div>
	<div id="divCreateWorldContainer" runat="server"><a id="A5" href="~/community/StartWorld.aspx" runat="server">Create a World</a></div>
	<div id="approvals" runat="server"><a href="~/mykaneva/messagecenter.aspx?mc=cr" id="btnNavCommunityRequests" runat="server" tooltip="View and approve people who want to join your members-only World.">World Approvals</a><span class="commreq new" id="spnNewCommunityRequests" runat="server"></span></div>
</div>


