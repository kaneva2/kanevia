///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using System.Collections.Generic;
using log4net;


namespace KlausEnt.KEP.Kaneva.usercontrols
{
    public partial class ActionItems : Kaneva.mykaneva.widgets.ModuleViewBaseControl
    {
        protected void Page_Load (object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ShowActionItemList ();

                decimal percent = Math.Round ((Convert.ToDecimal (itemsCompleted) / itemsTotal) * 100, 0);
                litPercentComplete.Text = percent.ToString ();
                divActionItemsProgressBar.Style.Add ("width", percent.ToString () + "%");

                if (percent == 100)
                {
                    divActionItems.Visible = false;
                }
            }
        }

        /// <summary>
        /// ShowActionItemList
        /// </summary>
        private void ShowActionItemList ()
        {
            try
            {
                // Get the check list items
                PagedList<Nudge> nudges = new FameFacade().GetChecklist (Current_User.UserId, 1, "", "sort_order ASC", 1, Int32.MaxValue);

                if (nudges != null && nudges.Count > 0)
                {
                    rptActionItemList.DataSource = nudges;
                    rptActionItemList.DataBind ();

                    itemsTotal = nudges.Count;
                }
            }
            catch (Exception ex)
            {
                m_logger.Error ("Error retrieving beginner checklist, userid =" + Current_User.UserId, ex);
            }
        }

        public string DisplayActionItem (object isCompleted, object text, object url)
        {
            try
            {
                string itemText = text.ToString ();
                string hidDiv = "";
                string itemDesc = "";
                string cssClass = "unchecked";
                itemsCurrCount++;

                if (Convert.ToBoolean (isCompleted))
                {
                    itemDesc = itemText;
                    cssClass = "checked";
                    itemsCompleted++;
                }
                else
                {
                    string itemURL = url.ToString ();
                    itemDesc = "<a href=\"" + itemURL + "\">" + itemText + "</a>";
                }

                if (itemsCurrCount.Equals (6))
                {
                    hidDiv = "<div id=\"divHidItems\" style=\"display:none\">";   
                }

                return hidDiv + "<p class=\"" + cssClass + "\">" + itemDesc + "</p>";
            }
            catch
            {
                return text.ToString (); ;
            }
        }

        /// <summary>
        /// Repeater rptActionItemList ItemDataBound Event Handler
        /// </summary>
        protected void rptActionItemList_ItemDataBound (Object Sender, RepeaterItemEventArgs e)
        {
            string x = e.Item.ToString ();

            // Execute the following logic for Items and Alternating Items.
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                string eventTitle = DataBinder.Eval (e.Item.DataItem, "Name").ToString ();
                int eventId = Convert.ToInt32 (DataBinder.Eval (e.Item.DataItem, "event_id"));
                int userId = Convert.ToInt32 (DataBinder.Eval (e.Item.DataItem, "user_id"));
                string eventStart = Convert.ToDateTime (DataBinder.Eval (e.Item.DataItem, "start_time")).ToString ("MM/dd/yy h:mm tt");
                string location = DataBinder.Eval (e.Item.DataItem, "location").ToString ();
                int communityId = Convert.ToInt32 (DataBinder.Eval (e.Item.DataItem, "user_id"));
                string username = DataBinder.Eval (e.Item.DataItem, "Host").ToString ();
                int premium = Convert.ToInt32 (DataBinder.Eval (e.Item.DataItem, "premium"));


                ((HyperLink) e.Item.FindControl ("hlEventName")).Text = eventTitle;
                ((HyperLink) e.Item.FindControl ("hlEventName")).NavigateUrl = "~/mykaneva/events/eventDetail.aspx?event=" + eventId;

                ((HyperLink) e.Item.FindControl ("hlEventEdit")).Text = "Edit";
                ((HyperLink) e.Item.FindControl ("hlEventEdit")).NavigateUrl = "~/mykaneva/events/eventForm.aspx?event=" + eventId;
                ((HyperLink) e.Item.FindControl ("hlEventEdit")).Visible = userId == KanevaWebGlobals.CurrentUser.UserId ? true : false;

                ((Literal) e.Item.FindControl ("litEventWhen")).Text = eventStart;
                // Convert.ToDateTime(DataBinder.Eval (Container.DataItem, "start_time")).ToString("MM/dd/yy h:mm tt")

                ((HyperLink) e.Item.FindControl ("hlEventWhere")).Text = location;
                ((HyperLink) e.Item.FindControl ("hlEventWhere")).NavigateUrl = "~/kgp/playwok.aspx?goto=C" + communityId + "&ILC=HO3D&link=hangoutnow";

                ((HyperLink) e.Item.FindControl ("hlEventWho")).Text = username;
                ((HyperLink) e.Item.FindControl ("hlEventWho")).NavigateUrl = GetPersonalChannelUrl (username).ToString (); ;
            }
        }

        #region Declerations

        private int itemsTotal = 0;
        private int itemsCompleted = 0;
        private int itemsCurrCount = 0;

        // private 
        private User Current_User = KanevaWebGlobals.CurrentUser;
        
        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

        #endregion Declerations
    }
}