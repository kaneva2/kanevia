<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FriendFinder.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.usercontrols.FriendFinder" %>

<script type="text/javascript"><!--

document.observe("dom:loaded", FriendRecInit);

function FriendRecInit (){
    var ajaxRequest = new Ajax.Request('usercontrols/doodad/FriendFinderData.aspx', {
        method:       'get', 
        parameters:   '', 
        asynchronous: true,
        evalJS:true,
        evalScripts:true,
        onComplete:   showFriendRecs,
        onFailure: errorFriendRecs
    });
}

function showFriendRecs(xmlHttpRequest, responseHeader) {
    xmlHttpRequest.responseText.evalScripts();
    $('pFriendRecommends').innerHTML = xmlHttpRequest.responseText;
}

function errorFriendRecs(error)   
{   
   var val = error.responseText;   
}   

// Send Friend Request
function FriendSendReq (uId, oDiv){		
    divAdd = oDiv;
    divAdd.innerHTML = '<img src="images/ajax-loader_sm.gif" alt="Loading..." border="0" width="16" height="16"/>';	
    var ajaxRequest = new Ajax.Request('usercontrols/doodad/FriendAdd.aspx', {
        method:       'post', 
        parameters:   'uId=' + uId, 
        asynchronous: true,
        evalJS:		  true,
        evalScripts:  true,
        onComplete:   showReqSent,
        onFailure:    errorFriendReq
    });
}
var divAdd = null;
function showReqSent(xmlHttpRequest, responseHeader) {
    xmlHttpRequest.responseText.evalScripts();
    divAdd.style.fontSize = '10px';
    divAdd.style.paddingTop = '4px';
    divAdd.innerHTML = xmlHttpRequest.responseText;
}
function errorFriendReq(error)   
{   
   var val = error.responseText;   
}   
--> </script> 

<div class="friendsDD">
	<h2>Friends</h2>
	<p>Based on your profile, you are likely to know these people.</p>
	
	<div id="pFriendRecommends"><img class="imgLoading" runat="server" src="~/images/ajax-loader.gif" height="24" width="24" alt="Loading..." border="0"/><div class="loadingText">Loading...</div></div> 
        
	<a runat="server" href="~/people/people.kaneva"><p>View all Connections &raquo;</p></a>
</div><!-- friendsDD-->
