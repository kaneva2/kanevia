///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

namespace KlausEnt.KEP.Kaneva.usercontrols
{
    public class BlastComments : Kaneva.mykaneva.widgets.ModuleViewBaseControl
    {
        protected void Page_Load (object sender, EventArgs e)
        {
            // Load JS files
            RegisterJavaScript("jscript/jquery/jquery-1.8.2.min.js");
            RegisterJavaScript("jscript/doodad/Comment.js?v3");

            // This div is where the comments are loaded into
            litLoadComments.Text = "<div class=\"commentText\" id=\"pComments" + EntryId.ToString () + "\">" +
                "<div class=\"loadingText\">Loading Comments...</div></div>";

            // If there are comments init them
            if (NumberOfComments > 0)
            {
                litInitComments.Text = "<script type=\"text/javascript\">BlastCommInit({type: type.BLAST,entryId: \"" + EntryId.ToString() + "\"});</script>";
            }
            else
            {
                litLoadComments.Text = "<div id=\"pComments" + EntryId.ToString () + "\"></div>";
            }

            // Show view all?
            divViewAllComments.Visible = false;
            if (NumberOfComments > CINITIAL_COMMENTS)
            {
                divViewAllComments.Visible = true;
                aViewAll.InnerText = "View All " + NumberOfComments.ToString () + " Comments";
                // since this entire method call is a string you have to pass the type enum (type.BLAST) as a string and it will get
                // evaluated in the js. If you don't pass as a string it gets passed as undefined
                aViewAll.Attributes.Add("onclick", "ViewAllComments({type: 'type.BLAST\',entryId: '" + EntryId.ToString() + "',viewAllContainer: $j('#" + divViewAllComments.ClientID + "')});");
                aViewAll.Visible = true;
            }

            if (ShowCommentsBox)
            {
                // Set up add comments
//                txtBlastComment.Attributes.Add("onfocus", "ShowAddComment($j(this),$j('#" + btnAddComment.ClientID + "'),defText);");
//                txtBlastComment.Attributes.Add("onblur", "ResetAddComment($j(this),defText);");
//                txtBlastComment.Attributes.Add("onkeyup", "ResizeTextArea($j(this));");

                // since this entire method call is a string you have to pass the type enum (type.BLAST) as a string and it will get
                // evaluated in the js. If you don't pass as a string it gets passed as undefined
                btnAddComment.Attributes.Add("onclick", "javascript:callGAEvent('Category','Comment');AddComment({type: 'type.BLAST',entryId: '" + EntryId.ToString() + "',txt: $j('#" + txtBlastComment.ClientID + "'),btn: $j('#" + btnAddComment.ClientID + "'),div: $j('#" + divViewAllComments.ClientID + "')});return false;");
            }
            else
            {
                txtBlastComment.Visible = false;
                divButtonsComment.Visible = false;
                divCommentBox.Visible = false;
            }
        }

        # region Properties

        public Int64 EntryId
        {
            get { return this.entryId; }
            set { this.entryId = value; }
        }

        public int NumberOfComments
        {
            get { return this.numberOfComments; }
            set { this.numberOfComments = value; }
        }

        public bool ShowCommentsBox
        {
            get { return this.showCommentsBox; }
            set { this.showCommentsBox = value; }
        }


        #endregion Properties


        #region Declerations

        protected Literal litLoadComments, litInitComments;
        protected Literal litViewAllComments;
        protected HtmlAnchor btnAddComment, aViewAll;
        protected TextBox txtBlastComment;
        protected HtmlContainerControl divViewAllComments, divButtonsComment, divCommentBox;
        
        private Int64 entryId = 0;
        private int numberOfComments = 0;
        private const int CINITIAL_COMMENTS = 2;
        private bool showCommentsBox = true;

        #endregion Declerations
    }
}