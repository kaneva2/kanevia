﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MyTopWorldsData.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.usercontrols.MyTopWorldsData" %>

<asp:Literal id="litJS" runat="server"></asp:Literal>

<div id="divMore" class="more" runat="server" visible="false"><a href="" id="aViewAll" runat="server">View All</a></div>
<asp:repeater EnableViewState="True" id="rptTopPlaces" runat="server">
	<itemtemplate>
		<div class="topworldrow">
			<div class="framesize-small">
				<div class="frame">																										
					<a id="aImgPlay" runat="server" href='<%# GetBroadcastChannelUrl (DataBinder.Eval (Container.DataItem, "NameNoSpaces").ToString ())%>'>
					<img runat="server" src='<%# GetBroadcastChannelImageURL (DataBinder.Eval(Container.DataItem, "ThumbnailSmallPath").ToString (), "sm") %>' border="0" id="Img5"/></a>
				</div>
			</div>		
			<p><a id="aNamePlay" runat="server" href='<%# GetBroadcastChannelUrl (DataBinder.Eval (Container.DataItem, "NameNoSpaces").ToString ())%>'><%# DataBinder.Eval(Container.DataItem, "Name") %></a></p>
			<p>Raves: <%# DataBinder.Eval(Container.DataItem, "Stats.NumberOfDiggs") %></p>
		</div>											  
	</itemtemplate>
	<SeparatorTemplate>
		<hr class="seperator" />
	</SeparatorTemplate>  
</asp:Repeater>  

<div id="divNoTopPlaces" runat="server" visible="false" class="noData">Top Worlds Not Found.</div>