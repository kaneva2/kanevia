<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FriendFinderData.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.usercontrols.FriendFinderData" %>


<asp:repeater id="rptRecommendedFriends" runat="server">
	<itemtemplate>
	
	    <div class="friendList">
            <div class="friendListImg">
                <a href="<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "Value.NameNoSpaces").ToString ())%>"><img src='<%#GetProfileImageURL (DataBinder.Eval(Container.DataItem, "Value.ThumbnailSmallPath").ToString (), "sm", (DataBinder.Eval(Container.DataItem, "Value.Gender").ToString()=="M"?"M":"F") ) %>' border="0" /></a></p>
            </div><!-- friendListImg-->
            <div class="friendListInfo">
			    <p><a class="userName" href="<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "Value.NameNoSpaces").ToString ())%>"><%# DataBinder.Eval(Container.DataItem, "Value.Username")%></a></p>
			    <p class="friendName"><%# DataBinder.Eval (Container.DataItem, "Value.DisplayName") %></p>
				<div id="divAddFriend<%# DataBinder.Eval(Container.DataItem, "Value.UserId").ToString() %>" onclick="FriendSendReq(<%# DataBinder.Eval(Container.DataItem, "Value.UserId") %>,this);" >
					<a href="javascript:void(0);">Add Friend</a>
				</div><!-- button-->
			</div><!-- friendListInfo-->
	    </div><!-- friendList-->
	
	</itemtemplate>
</asp:repeater>

<div class="noData" id="divNoData" runat="server" visible="false">No Data Found</div>
