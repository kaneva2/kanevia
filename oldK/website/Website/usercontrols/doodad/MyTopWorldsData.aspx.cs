///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Kaneva.BusinessLayer.BusinessObjects;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Kaneva.usercontrols
{
    public partial class MyTopWorldsData : BasePage
    {
        protected void Page_Load (object sender, EventArgs e)
        {
            Response.ContentType = "text/plain";
            Response.CacheControl = "no-cache";
            Response.AddHeader ("Pragma", "no-cache");
            Response.Expires = -1;

            BindTopPlaces ();
        }

        private void BindTopPlaces ()
        {
            int[] communityTypeIds = { Convert.ToInt32 (CommunityType.APP_3D), Convert.ToInt32 (CommunityType.COMMUNITY), Convert.ToInt32 (CommunityType.HOME) };
            PagedList<Community> worlds = GetCommunityFacade.GetUserTopWorlds (KanevaWebGlobals.CurrentUser.UserId, communityTypeIds, "last_touched DESC", "", 1, 3);

            if (worlds.TotalCount > 0)
            {
                rptTopPlaces.DataSource = worlds;
                rptTopPlaces.DataBind ();

                if (worlds.TotalCount > 3)
                {
                    divMore.Visible = true;
                    aViewAll.Attributes.Add ("href", ResolveUrl("~/mykaneva/my3dapps.aspx"));
                }
            }
        }
    }
}