///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Collections.Generic;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.usercontrols
{
    public partial class FriendFinderData : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            UserFacade userFacade = new UserFacade();
            List<Friend> plFriends;
            List<Friend> plFriendsOfFriends;
            PagedList<Friend> plFriendsOfFriendsReced;

            Hashtable htRecommendedFriends = new Hashtable(cFriendsToReturn);
            Random random = new Random();

            Response.ContentType = "text/plain";
            Response.CacheControl = "no-cache";
            Response.AddHeader ("Pragma", "no-cache");
            Response.Expires = -1;

            if (KanevaWebGlobals.CurrentUser == null)
            {
                Response.Clear();
                Response.Write("No user");
                Response.End();
            }

            int userId = KanevaWebGlobals.CurrentUser.UserId;

            // First get Friends on friends
            if (KanevaWebGlobals.CurrentUser.Stats.NumberOfFriends > 0)
            {
                // Randomly get a friend, then get 3 of thier friends
                plFriends = userFacade.GetFriendsFast(userId, 0, "", "f.friend_id DESC", random.Next(1, 4), 2);

                foreach (Friend friend in plFriends)
                {
                    plFriendsOfFriends = userFacade.GetFriendsFast(friend.UserId, 0, "", "f.friend_id DESC", random.Next(1, 4), 3);

                    // Loop through the friends of friends
                    foreach (Friend friendOfFriend in plFriendsOfFriends)
                    {
                        // Not myself or a friend I already have
                        if ((friendOfFriend.UserId != userId) &&
                            (friendOfFriend.StatusId.Equals((int)Constants.eUSER_STATUS.REGVALIDATED) || friendOfFriend.StatusId.Equals((int)Constants.eUSER_STATUS.REGNOTVALIDATED)) &&
                            (!userFacade.AreFriends(friendOfFriend.UserId, userId)) && IsFriendOfValidAge(KanevaWebGlobals.CurrentUser.Age, friendOfFriend.Age))
                        {
                            if (!htRecommendedFriends.ContainsKey(friendOfFriend.UserId))
                            {
                                htRecommendedFriends.Add(friendOfFriend.UserId, friendOfFriend);
                            }
                        }

                        if (htRecommendedFriends.Count >= cFriendsToReturn)
                        {
                            break;
                        }
                    }

                    if (htRecommendedFriends.Count >= cFriendsToReturn)
                    {
                        break;
                    }
                }
            }        

            // Did friend way find any?
            if (htRecommendedFriends.Count < cFriendsToReturn)
            {
                // Find some by location
                plFriendsOfFriendsReced = userFacade.GetFriendsRecommended(KanevaWebGlobals.CurrentUser.Country, KanevaWebGlobals.CurrentUser.ZipCode, KanevaWebGlobals.CurrentUser.Age, "last_login", random.Next(1, 6), 5);

                // Loop through the friends of friends
                foreach (Friend possibleFriendRec in plFriendsOfFriendsReced)
                {
                    // Not myself or a friend I already have
                    if ((possibleFriendRec.UserId != userId) &&
                        (possibleFriendRec.StatusId.Equals((int)Constants.eUSER_STATUS.REGVALIDATED) || possibleFriendRec.StatusId.Equals((int)Constants.eUSER_STATUS.REGNOTVALIDATED)) && 
                        (!userFacade.AreFriends (possibleFriendRec.UserId, userId)))
                    {
                        if (!htRecommendedFriends.ContainsKey(possibleFriendRec.UserId))
                        {
                            htRecommendedFriends.Add(possibleFriendRec.UserId, possibleFriendRec);
                        }
                    }

                    if (htRecommendedFriends.Count >= cFriendsToReturn)
                    {
                        break;
                    }
                }
            }

            if (htRecommendedFriends.Count > 0)
            {
                rptRecommendedFriends.DataSource = htRecommendedFriends;
                rptRecommendedFriends.DataBind();
            }
            else
            {
                divNoData.Visible = true;
            }
        }

        private bool IsFriendOfValidAge (int currentUserAge, int friendAge)
        {
            // is user under 18
            if (currentUserAge < 18)
            {
                return friendAge < 18;
            }
            else   // user is 18 or older
            {
                return friendAge >= 18;
            }
        }

        private const int cFriendsToReturn = 3;
        protected Repeater rptRecommendedFriends;

    }
}
