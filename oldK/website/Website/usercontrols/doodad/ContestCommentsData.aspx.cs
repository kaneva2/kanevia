///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using log4net;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.usercontrols
{
    public partial class ContestCommentsData : BasePage
    {
        #region Declerations

        private int commentsPerPage = 4;
        private int pageNumber = 1;
        private ulong contestEntryId = 0;
        private ulong contestId = 0;
        private ulong commentId = 0;
        string commentText = "";

        // Logger
        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

        #endregion Declerations
        
        protected void Page_Load (object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                RegisterJavaScript("jscript/doodad/comment.js?v3");

                if (KanevaWebGlobals.CurrentUser != null && KanevaWebGlobals.CurrentUser.UserId > 0)
                {
                    //BlastFacade blastFacade = new BlastFacade ();

                    GetRequestValues ();

                    //  Delete an entry comment
                    if (Request["del"] != null)
                    {   // Delete a contest or contest entry comment
                        if (ContestId > 0 && CommentId > 0)
                        {
                            if (CanDelete ())
                            {
                                GetContestFacade.DeleteComment (ContestId, ContestEntryId, CommentId);
                            }
                        }
                    }  // Add a comment
                    else if (Request["add"] != null)
                    {
                        // check for blank message
                        if (CommentText.Length == 0)
                        {
                            litJS.Text = "<script type=\"text/javascript\">alert('You can not submit a blank comment.');</script>";
                            return;
                        }

                        // check for injection scripts
                        if (KanevaWebGlobals.ContainsInjectScripts (CommentText))
                        {
                            //                 m_logger.Warn ("User " + KanevaWebGlobals.CurrentUser.UserId.ToString () + " tried to script from IP " + Common.GetVisitorIPAddress());
                            litJS.Text = "<script type=\"text/javascript\">alert('Your input contains invalid scripting, please remove script code and try again.');</script>";
                            return;
                        }

                        // check for restricted words
                        if (KanevaWebGlobals.isTextRestricted (CommentText, Constants.eRESTRICTION_TYPE.POTTY_MOUTH))
                        {
                            litJS.Text = "<script type=\"text/javascript\">alert('" + Constants.VALIDATION_REGEX_RESTRICTED_WORD_MESSAGE + "');</script>";
                            return;
                        }

                        // Make sure comment does not exceed max length
                        if (Server.HtmlDecode (CommentText).Length > KanevaGlobals.MaxContestLength)         
                        {
                            litJS.Text = "<script type=\"text/javascript\">alert('Your comment is too long.  Comments must be " + KanevaGlobals.MaxContestLength.ToString () + " characters or less.');</script>";
                            return;
                        }

                        if (ContestId > 0)
                        {
                            GetContestFacade.InsertComment (ContestId, ContestEntryId, KanevaWebGlobals.CurrentUser.UserId, CommentText);
                        }
                    }
                    

                    Response.ContentType = "text/plain";
                    Response.CacheControl = "no-cache";
                    Response.AddHeader ("Pragma", "no-cache");
                    Response.Expires = -1;

                    // Get the comments
                    rptComments.DataSource = GetContestFacade.GetContestComments (ContestId, ContestEntryId, PageNum, commentsPerPage, "created_date DESC");
                    rptComments.DataBind ();
                }
            }
        }

        private void GetRequestValues ()
        {
            if (Request["cId"] != null)
            {
                ContestId = Convert.ToUInt32 (Request["cId"]);
            }

            if (Request["eId"] != null)
            {
                ContestEntryId = Convert.ToUInt32 (Request["eId"]);
            }

            if (Request["cmtId"] != null)
            {
                CommentId = Convert.ToUInt32 (Request["cmtId"]);
            }

            if (Request["p"] != null)
            {
                PageNum = Convert.ToInt32 (Request["p"]);
            }

            if (Request["cpp"] != null)
            {
                CommentsPerPage = Convert.ToInt32 (Request["cpp"]);
            }

            if (Request["cmt"] != null)
            {
                CommentText = Request["cmt"].ToString ();
            }
       }

        protected void rptComments_ItemDataBound (Object Sender, RepeaterItemEventArgs e)
        {
            if ((e.Item.ItemType == ListItemType.Item) || (e.Item.ItemType == ListItemType.AlternatingItem))
            {
                HtmlContainerControl divComment = ((HtmlContainerControl) e.Item.FindControl ("divComment"));
                HtmlContainerControl spnOwner = ((HtmlContainerControl) e.Item.FindControl ("spnOwner"));
                HtmlAnchor aDelete = ((HtmlAnchor) e.Item.FindControl ("aDelete"));

                if (divComment != null)
                {
                    // If user has already voted on this contest, they can't vote again
                    ContestComment comment = (ContestComment) e.Item.DataItem;
                    if (comment.ContestOwnerUserId == comment.Creator.UserId)
                    {   // comment belongs to contest owner, apply special styling
                        divComment.Attributes.Add ("class", "feedbackBox ownercomment");

                        if (spnOwner != null)
                        {
                            spnOwner.InnerHtml = "(<img src=\"" + ResolveUrl ("~/images/icons/icon_contests_ribbon_12x18.png") + "\" /> owner)";
                        }
                    }

                    // If user is admin, then display delete option
                    if (CanDelete ())
                    {
                        aDelete.Visible = true;
                        // since this entire method call is a string you have to pass the type enum (type.CONTEST) as a string and it will get
                        // evaluated in the js. If you don't pass as a string it gets passed as undefined
                        aDelete.Attributes.Add("onclick", "DeleteComment({type:'type.CONTEST',contestId:'" + comment.ContestId + "',contestEntryId: '" + comment.ContestEntryId + "',commentId: '" + comment.CommentId + "'});");
                    }                                       
                }                                           
            }
        }

        protected bool CanDelete ()
        {
            bool allowEditing = false;

            //check security level
            switch (KanevaWebGlobals.CheckUserAccess ((int) SitePrivilege.ePRIVILEGE.USER_DATA_ADMIN))
            {
                case (int) SitePrivilege.eACCESS_LEVEL.ACCESS_FULL:
                    allowEditing = true;
                    break;
                case (int) SitePrivilege.eACCESS_LEVEL.ACCESS_READ:
                    break;
                case (int) SitePrivilege.eACCESS_LEVEL.ACCESS_NONE:
                    break;
                default:
                    break;
            }
            return allowEditing;
        }

        #region Properties

        private int PageNum
        {
            set { this.pageNumber = value; }
            get { return this.pageNumber; }
        }
        private int CommentsPerPage
        {
            set { this.commentsPerPage = value; }
            get { return this.commentsPerPage; }
        }
        private ulong ContestId
        {
            set { this.contestId = value; }
            get { return this.contestId; }
        }
        private ulong ContestEntryId
        {
            set { this.contestEntryId = value; }
            get { return this.contestEntryId; }
        }
        private ulong CommentId
        {
            set { this.commentId = value; }
            get { return this.commentId; }
        }
        private string CommentText
        {
            set { this.commentText = value; }
            get { return this.commentText; }
        }

        #endregion Properties
    }
}