///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.usercontrols.doodad
{
    public partial class MessageSummary : Kaneva.mykaneva.widgets.ModuleViewBaseControl
    {
        protected void Page_Load (object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // Show Summary Counts
                GetMessageCount ();
                GetFriendRequestCount ();
                GetWorldsSummaryCount ();
                GetCommunityRequestsCount ();
                GetCommunityInviteCount();

                CheckUserExperimentParticipation();
            }

            // add redirect and ga call
            ConfigureUserMeetMe3D (btnGo3D, KanevaWebGlobals.CurrentUser.Username, "");
        }

        private void GetMessageCount ()
        {
            int cnt = 0;
            spnNewMessages.Visible = false;
            
            try
            {
                cnt = KanevaWebGlobals.CurrentUser.Stats.NumberOfNewMessages;
            }
            catch {}

            if (cnt > 0)
            {
                spnNewMessages.InnerText = "(" + cnt.ToString () + ")";
                spnNewMessages.Visible = true;
            }
        }
        private void GetFriendRequestCount ()
        {
            int cnt = 0;
            spnNewFriendRequests.Visible = false;

            try
            {
                cnt = KanevaWebGlobals.CurrentUser.Stats.NumberOfRequests;
            }
            catch { }

            if (cnt > 0)
            {
                spnNewFriendRequests.InnerText = "(" + cnt.ToString () + ")";
                spnNewFriendRequests.Visible = true;
            }
        }
        private void GetWorldsSummaryCount ()
        {
            double cnt = 0;
            spnNew3DAppRequests.Visible = false;

            try
            {
                cnt = KanevaWebGlobals.CurrentUser.Stats.Number3DAppRequests; 
            }
            catch { }

            if (cnt > 0)
            {
                spnNew3DAppRequests.InnerText = "(" + cnt.ToString () + ")";
                spnNew3DAppRequests.Visible = true;
            }
        }
        private void GetCommunityRequestsCount ()
        {
            double cnt = 0;
            approvals.Visible = false;

            try
            {
                cnt = CommunityUtility.GetPendingMembersCount (KanevaWebGlobals.CurrentUser.UserId);
            }
            catch { }

            if (cnt > 0)
            {
                spnNewCommunityRequests.InnerText = "  (" + cnt.ToString () + ")";
                spnNewCommunityRequests.Visible = true;
                approvals.Visible = true;
            }
        }
        private void GetCommunityInviteCount()
        {
            int cnt = 0;
            spnNewCommunityInvites.Visible = false;

            try
            {
                cnt = KanevaWebGlobals.CurrentUser.Stats.NumberCommunityInvites;
            }
            catch { }

            if (cnt > 0)
            {
                spnNewCommunityInvites.InnerText = "(" + cnt.ToString() + ")";
                spnNewCommunityInvites.Visible = true;
            }
        }
        private void GetActivityCount()
        { }

        private void CheckUserExperimentParticipation()
        {
            // Shop - Requests - Create a World
            const string _Shop_Requests_Create_ExperimentId = "5a721132-5656-11e5-8d32-0a74286ca6fb";
            const string _Shop_Requests_Create_OriginalId = "730a2bcf-5656-11e5-8d32-0a74286ca6fb";  // All visible
            const string _Shop_Requests_Create_VariantBId = "8e87b914-5656-11e5-8d32-0a74286ca6fb";  // Shop hidden
            const string _Shop_Requests_Create_VariantCId = "becfa67e-5656-11e5-8d32-0a74286ca6fb";  // Requests hidden
            const string _Shop_Requests_Create_VariantDId = "d4a5b9eb-5656-11e5-8d32-0a74286ca6fb";  // Create Worlds hidden

            UserExperimentGroup userExperimentGroup = GetExperimentFacade.GetUserExperimentGroupCommon(KanevaWebGlobals.CurrentUser.UserId, _Shop_Requests_Create_ExperimentId);

            if (userExperimentGroup != null)
            {
                switch (userExperimentGroup.GroupId)
                {
                    case _Shop_Requests_Create_VariantBId:
                        divShopContainer.Visible = false;
                        break;
                    case _Shop_Requests_Create_VariantCId:
                        divRequestContainer.Visible = false;
                        break;
                    case _Shop_Requests_Create_VariantDId:
                        divCreateWorldContainer.Visible = false;
                        break;
                    default:
                        break;
                }
            }
        }
    }
}