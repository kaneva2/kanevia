///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Collections.Generic;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.usercontrols
{
    public partial class TopWorldsData : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Request.IsAuthenticated)
            {
                Response.Redirect("/");
            }

            BindTopWorlds();
        }
        
        private void BindTopWorlds()
        {
            List<GameFacade.TopWorld> lstTopWorlds = GetGameFacade.GetTopWorlds(KanevaWebGlobals.CurrentUser.UserId, !KanevaWebGlobals.CurrentUser.HasAccessPass, " -5 MINUTE", KanevaGlobals.WokGameId);
            if (lstTopWorlds.Count > 0)
            {
                rptTopWorlds.DataSource = lstTopWorlds;
                rptTopWorlds.DataBind();
                pnlTopWorlds.Visible = true;
            }
        }

        public string GetPopulationText (int pop)
        {
            string txtPop = "";
            if (pop <= 2)
            {
                txtPop = "Low";
            }
            else if (pop > 2 && pop <= 6)
            {
                txtPop = "Medium";
            }
            else
            {
                txtPop = "High";
            }
            return txtPop;
        }

        public void rptTopWorlds_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                int communityId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "community_id"));

                // Set the links for go in world
                HtmlAnchor btnMeet3D = (HtmlAnchor)e.Item.FindControl("btnMeet3D");
                HtmlAnchor aMeet3D = (HtmlAnchor)e.Item.FindControl("aMeet3D");
                int gameId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "game_id"));

                ConfigureCommunityMeetMe3D(btnMeet3D, gameId, communityId, Convert.ToString(DataBinder.Eval(e.Item.DataItem, "location")), "");
                ConfigureCommunityMeetMe3D(aMeet3D, gameId, communityId, Convert.ToString(DataBinder.Eval(e.Item.DataItem, "location")), "");

                if(e.Item.ItemIndex == 0)
                {
                    HtmlContainerControl divWorld = (HtmlContainerControl)e.Item.FindControl("divWorld");
                    divWorld.Attributes.Add("class", "world first");
                }
                else if (e.Item.ItemIndex == 4)
                {
                    HtmlContainerControl divWorld = (HtmlContainerControl)e.Item.FindControl("divWorld");
                    divWorld.Attributes.Add("class", "world last");
                }
            }
        }
    }
}