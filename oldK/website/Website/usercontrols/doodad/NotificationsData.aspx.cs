///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Collections.Generic;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.usercontrols
{
    public partial class NotificationsData : BasePage
    {
        protected void Page_Load (object sender, EventArgs e)
        {
            Response.ContentType = "text/plain";
            Response.CacheControl = "no-cache";
            Response.AddHeader("Pragma", "no-cache");
            Response.Expires = -1;

            int pageNumberPassive = 1;

            if (Request["p"] != null)
            {
                pageNumberPassive = Convert.ToInt32(Request["p"]);
            }

            BindNotifications (pageNumberPassive);

            // Get count of new notifications since last login

        }

        /// <summary>
        /// BindPassiveBlasts
        /// </summary>
        /// <param name="pageNumber"></param>
        private void BindNotifications(int pageNumber)
        {
            try
            {
                User Current_User = KanevaWebGlobals.CurrentUser;

                //user imposed filters on passive blast
                string userVisibilityFilter = "";
                if (Current_User != null)
                {
                    if (Current_User.BlastShowBlogs)
                    {
                        userVisibilityFilter += (int)BlastTypes.BLOG + ",";
                    }
                    if (Current_User.BlastShowComments)
                    {
                        userVisibilityFilter += (int)BlastTypes.MEDIA_COMMENT + ",";
                    }
                    if (Current_User.BlastShowForums)
                    {
                        userVisibilityFilter += (int)BlastTypes.FORUM_POST + ",";
                    }
                    if (Current_User.BlastShowJoinCommunity)
                    {
                        userVisibilityFilter += (int)BlastTypes.JOIN_COMMUNITY + ",";
                    }
                    if (Current_User.BlastShowProfiles)
                    {
                        userVisibilityFilter += (int)BlastTypes.PROFILE_UPDATE + ",";
                    }
                    if (Current_User.BlastShowRave3dHangout)
                    {
                        userVisibilityFilter += (int)BlastTypes.RAVE_3D_HANGOUT + ",";
                    }
                    if (Current_User.BlastShowRave3dHome)
                    {
                        userVisibilityFilter += (int)BlastTypes.RAVE_3D_HOME + ",";
                    }
                    if (Current_User.BlastShowSendGift)
                    {
                        userVisibilityFilter += (int)BlastTypes.SEND_GIFT + ",";
                    }
                    if (Current_User.BlastShowRave3dApp)
                    {
                        userVisibilityFilter += (int)BlastTypes.RAVE_3D_APP + ",";
                    }
                    if (Current_User.BlastShowCameraScreenshot)
                    {
                        userVisibilityFilter += (int)BlastTypes.CAMERA_SCREENSHOT + ",";
                    }

                    userVisibilityFilter += (int)BlastTypes.FAME + ",";

                    if (userVisibilityFilter.Length > 0)
                    {
                        userVisibilityFilter = userVisibilityFilter.Substring(0, (userVisibilityFilter.Length - 1));
                    }
                }

                int MaxBlasts = 20;

                // Get blasts
                BlastFacade blastFacade = new BlastFacade();

                //if (KanevaWebGlobals.CurrentUser.Stats.NumberOfFriends > 150)
                //{
                //    // MaxValue is used to assume they have at least 100 active blasts, this is for users with large friends
                //    // groups and it is too expensive to do a count on the total number of blasts.
                //    uint MaxValue = 100;
                //    PagedList<Blast> lPassiveBlasts = null;
                //    lPassiveBlasts = blastFacade.GetPassiveBlastsManyFriends (KanevaWebGlobals.CurrentUser.UserId, MaxBlasts, userVisibilityFilter, pageNumber, MaxValue);

                //    if (lPassiveBlasts.Count > 0)
                //    {
                //        //Blasts
                //        rptNotifications.DataSource = lPassiveBlasts;
                //        rptNotifications.DataBind ();
                //    }
                //    else
                //    {
                //        divNoNotifications.Visible = true;
                //    }   

                //    // Set up pager
                //    //litJS.Text = "<script type=\"text/javascript\">setPager(" + MaxBlasts.ToString() + "," + MaxValue.ToString () + ");</script>";
                //}
                //else
                //{
                    IList<Blast> plPassiveBlasts = null;
                    plPassiveBlasts = blastFacade.GetPassiveBlasts(KanevaWebGlobals.CurrentUser.UserId, MaxBlasts, userVisibilityFilter, pageNumber);

                    if (plPassiveBlasts.Count > 0)
                    {
                        //Blasts
                        rptNotifications.DataSource = plPassiveBlasts;
                        rptNotifications.DataBind ();
                    }
                    else
                    {
                        divNoNotifications.Visible = true;
                    }

                    // Set up pager
                    //litJS.Text = "<script type=\"text/javascript\">setPager(" + MaxBlasts.ToString() + "," + plPassiveBlasts.TotalCount.ToString() + ");</script>";
               // }
            }
            catch { }
        }

        /// <summary>
        /// Repeater rptBlasts ItemDataBound Event Handler
        /// </summary>
        protected void rptNotifications_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            BlastFacade blastFacade = new BlastFacade();
            string strOut = "kaneva";
            string className = "";
            bool includeFrom = false;
            Blast blast = (Blast)e.Item.DataItem;

            switch (blast.BlastType)
            {
                case (int)BlastTypes.BLOG:
                    className = "blog";
                    break;
                
                case (int)BlastTypes.MEDIA_COMMENT:
                    className = "comment";
                    break;
                
                case (int)BlastTypes.FORUM_POST:
                    className = "forum";
                    break;
                
                case (int)BlastTypes.JOIN_COMMUNITY:
                    className = "community";
                    break;
                
                case (int)BlastTypes.PROFILE_UPDATE:
                    className = "profile";
                    break;
                
                case (int)BlastTypes.RAVE_3D_HANGOUT:
                    className = "rave";
                    break;
                
                case (int)BlastTypes.RAVE_3D_HOME:
                    className = "rave";
                    break;
                
                case (int)BlastTypes.SEND_GIFT:
                    className = "gift";
                    break;

                case (int)BlastTypes.RAVE_3D_APP:
                    className = "apps3d";
                    break;

                case (int)BlastTypes.CAMERA_SCREENSHOT:
                    className = "apps3d";
                    break;
                
                case (int)BlastTypes.FAME:
                    className = "fame";
                    break;
            }

            className = "notificationDD" + className;

            strOut = blast.Subj;
            if (includeFrom)
            {
                strOut += "<br />From: " +
                    "<a href=\"" + ResolveUrl ("~/channel/" + blast.SenderName + ".people") + "\">" + blast.SenderName + "</a>";
            }
            
            HtmlContainerControl container = (HtmlContainerControl) e.Item.FindControl ("pContainer");
            container.Attributes.Add ("class", className);
            container.InnerHtml += strOut;
        }


        #region Declerations

        protected Repeater rptNotifications;

        private Blast _prevBlast = new Blast();
        private string _rollupBlasts = string.Empty;
        protected Literal litJS;

        #endregion Declerations
    }
}
