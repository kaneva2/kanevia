﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserProfileData.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.usercontrols.doodad.UserProfileData" %>

<a class="avatar" id="lnkAvatarImg" href="~/community/profilepage.aspx" runat="server"><img id="imgAvatar" runat="server" src="~/images/KanevaIcon01.gif" border="0" /></a>
<div class="username"><a id="lnkUsername" href="~/community/profilepage.aspx" runat="server"><asp:label id="lblUserName" runat="server" /></a></div>
    
<div class="dialog" id="divSubscibedPasses" runat="server">
	<img runat="server" id="imgAP" src="~/images/mykaneva/accessPassLogo.jpg" visible="false" border="0" width="15" height="15" alt="Access Pass Subscriber"/>
	<img runat="server" id="imgVIP" src="~/images/mykaneva/VIPLogo.jpg" visible="false" border="0" width="15" height="15" alt="VIP Subscriber"/>
</div>

<p class="worldFame">World Fame: Level <asp:label id="lblFameLevel" runat="server"></asp:label></p>
<p class="raves">Raves: <asp:label id="lblRaves" runat="server" /></p>
<hr />
