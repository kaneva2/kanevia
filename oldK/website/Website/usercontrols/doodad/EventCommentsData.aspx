﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EventCommentsData.aspx.cs" Inherits="KlausEnt.KEP.Kaneva.usercontrols.EventCommentsData" %>

<asp:Literal id="litJS" runat="server"></asp:Literal>

<asp:repeater id="rptComments" runat="server" onitemdatabound="rptComments_ItemDataBound">
	<itemtemplate>
		<div class="feedbackBox" id="divComment" runat="server">
			<div class="feedbackImg">
				<a href="<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "Creator.Username").ToString ())%>">
					<img src='<%#GetProfileImageURL (DataBinder.Eval(Container.DataItem, "Creator.ThumbnailSquarePath").ToString (), "sq", DataBinder.Eval(Container.DataItem, "Creator.Gender").ToString (), Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "Creator.FacebookSettings.UseFacebookProfilePicture")), Convert.ToUInt64(DataBinder.Eval(Container.DataItem, "Creator.FacebookSettings.FacebookUserId"))   )%>' border="0" />
				</a>
			</div>
			<div class="feedbackText">
				<p><a href="<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "Creator.Username").ToString ())%>"><%# DataBinder.Eval(Container.DataItem, "Creator.Username")%> <span id="spnOwner" runat="server"></span>:</a>
				<%# DataBinder.Eval(Container.DataItem, "Comment")%></p>
				<div class="time">(<%# FormatDateTimeSpan (DataBinder.Eval(Container.DataItem, "CreatedDate"), DataBinder.Eval(Container.DataItem, "CurrentDate")) %>)  <a id="aDelete" runat="server" visible="false" href="javascript:void(0);">delete</a></div>
			</div>
		</div>
	</itemtemplate>
</asp:repeater>

