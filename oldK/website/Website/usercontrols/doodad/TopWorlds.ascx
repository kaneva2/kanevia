﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TopWorlds.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.usercontrols.TopWorlds" %>

<script type="text/javascript"><!--

    $j(document).ready(function () {
        $j.ajax({
            type: "GET",
            url: "usercontrols/doodad/TopWorldsData.aspx",
            data: '',
            contentType: "application/text; charset=utf-8",
            dataType: "text"
        }).done(function (data) {
            if (data.length > 0) {
                $j('#pTopWorlds').html(data);
                $j('#topworldscontainer').animate({ height: '200px' }, 500);
                $j('.topworldscontainer').slideDown('slow');
            }
            else {
                $j('#topworldscontainer').slideUp('slow');
            }
        });
    });

//--> </script> 

<div id="topworldscontainer" class="topWorldsDD" style="margin-bottom:20px;height:60px;">
	
    <div id="pTopWorlds"><img id="Img1" class="imgLoading" runat="server" src="~/images/ajax-loader.gif" alt="Loading..." border="0"/><div class="loadingText">Loading...</div></div> 
	
</div>