///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva.usercontrols.doodad
{
    public partial class EventsListData : BasePage
    {
        #region Declerations

        private int bDayCount = 0;
        private int annivCount = 0;
        private int basicEventCount = 0;
        private int premiumEventCount = 0;
        private bool showAllBirthdayBtn = false;
        private bool showAllAnniversaryBtn = false;
        private bool showAllYourEventsBtn = false;
        private bool showAllPremiumEventsBtn = false;

        #endregion Declerations

        #region Page Load

        protected void Page_Load(object sender, EventArgs e)
        {
            Response.ContentType = "text/plain";
            Response.CacheControl = "no-cache";
            Response.AddHeader("Pragma", "no-cache");
            Response.Expires = -1;

            ShowBirthdays();
            ShowAnniversaries();
            ShowEvents();
        }

        #endregion Page Load

        #region Methods

        private void ShowBirthdays()
        {
            PagedList<FriendDates> fd = GetUserFacade.GetFriendBirthdays(KanevaWebGlobals.CurrentUser.UserId, "", "birthday ASC", 20, 1);

            if (fd.Count > 0)
            {
                rptBirthdays.DataSource = fd;
                if (fd.Count > 3) { showAllBirthdayBtn = true; }
                rptBirthdays.DataBind();
            }
        }

        private void ShowAnniversaries()
        {
            PagedList<FriendDates> fd = GetUserFacade.GetFriendAnniversaries(KanevaWebGlobals.CurrentUser.UserId, "", "anniversary ASC", 20, 1);

            if (fd.Count > 0)
            {
                rptAnniversaries.DataSource = fd;
                if (fd.Count > 3) { showAllAnniversaryBtn = true; }
                rptAnniversaries.DataBind();
            }
        }

        /// <summary>
        /// ShowEvents
        /// </summary>
        private void ShowEvents()
        {
            int nextHours = -1;

            // Premium events                                                                                             // -1
            PagedDataTable dtEventsPremium = EventsUtility.GetPremuimEventsMyKaneva(KanevaWebGlobals.CurrentUser.UserId, nextHours, false, "start_time ASC", 1, 50);

            // Regular events
            PagedDataTable dtEvents = EventsUtility.GetUsersEventsMyKaneva(KanevaWebGlobals.CurrentUser.UserId, -1, false, "start_time ASC", 1, 50);

            if (dtEvents.Rows.Count.Equals(0) && dtEventsPremium.Rows.Count.Equals(0))
            {
                divNoEvents.Visible = true;
            }
            else
            {
                if (dtEventsPremium.Rows.Count > 0)
                {
                    rptEventsPremium.DataSource = dtEventsPremium;
                    if (dtEventsPremium.Rows.Count > 3) { showAllPremiumEventsBtn = true; }
                    rptEventsPremium.DataBind();
                }

                if (dtEvents.Rows.Count > 0)
                {
                    rptEvents.DataSource = dtEvents;
                    if (dtEvents.Rows.Count > 3) { showAllYourEventsBtn = true; }
                    rptEvents.DataBind();
                }
            }
        }

        private string FormatFutureTimeSpan(DateTime dtDate, bool showAnnivYears, bool formatForEvents)
        {
            string timespan = string.Empty;

            if (dtDate.Equals(DateTime.MinValue))
                return "";

            bool bDayIsLeapYear = System.DateTime.IsLeapYear(dtDate.Year);
            bool currentYearIsLeapYear = System.DateTime.IsLeapYear(DateTime.Today.Year);
            int span = 0;

            if (bDayIsLeapYear && !currentYearIsLeapYear && DateTime.Today.Month > 2)
            {
                span = (dtDate.DayOfYear - 1) - DateTime.Today.DayOfYear;
            }
            else if (!bDayIsLeapYear && currentYearIsLeapYear && DateTime.Today.Month > 2)
            {
                span = (dtDate.DayOfYear + 1) - DateTime.Today.DayOfYear;
            }
            else // both or neither are leap year or date is prior to Feb 29
            {
                span = dtDate.DayOfYear - DateTime.Today.DayOfYear;
            }

            if (span == 0)
            {
                timespan = " today";
                if (formatForEvents)
                {
                    timespan = "";
                    if (dtDate.TimeOfDay < DateTime.Now.TimeOfDay)
                    {
                        timespan = "Now";
                    }
                    else
                    {
                        int hrs = dtDate.TimeOfDay.Hours - DateTime.Now.TimeOfDay.Hours;
                        if (hrs == 0)
                        {
                            int mins = dtDate.TimeOfDay.Minutes - DateTime.Now.TimeOfDay.Minutes;
                            timespan = "In " + mins.ToString() + (mins > 1 ? " minutes" : " minute");
                        }
                        else
                        {
                            timespan = "In " + hrs.ToString() + (hrs > 1 ? " hours" : " hour");
                        }
                    }
                }
            }
            else if (span == 1)
            {
                timespan = " tomorrow";
                if (formatForEvents) timespan = "In 1 day";
            }
            else if (span > 1)
            {
                timespan = " in " + span + " days";
                if (formatForEvents) timespan = "In " + span + " days";
            }

            if (showAnnivYears)
            {
                try
                {
                    int numYrs = DateTime.Now.Year - dtDate.Year;
                    timespan += " (" + numYrs.ToString() + " year" + (numYrs > 1 ? "s)" : ")");
                }
                catch { }
            }

            return timespan;
        }

        #endregion Methods

        #region Event Handlers

        /// <summary>
        /// Repeater rptBirthdays ItemDataBound Event Handler
        /// </summary>
        protected void rptBirthdays_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (bDayCount == 3)
                {
                    ((Literal)e.Item.FindControl("litBirthdays")).Text = "<div id=\"bd\" style=\"display:none;\" class=\"bdayContainer\">";
                }

                DateTime birthDate = Convert.ToDateTime(DataBinder.Eval(e.Item.DataItem, "BirthDate"));
                string username = DataBinder.Eval(e.Item.DataItem, "Username").ToString();

                ((HtmlContainerControl)e.Item.FindControl("spnBdayTime")).InnerText = FormatFutureTimeSpan(birthDate, false, false);

                ((HyperLink)e.Item.FindControl("hlUsername")).Text = username;
                ((HyperLink)e.Item.FindControl("hlUsername")).NavigateUrl = GetPersonalChannelUrl(username).ToString(); ;

                bDayCount++;
            }
            else if (e.Item.ItemType == ListItemType.Header)
            {
                if (showAllBirthdayBtn)
                {
                    ((HtmlGenericControl)e.Item.FindControl("ulSeeAllBdays")).Visible = true;
                }
            }
            else if (e.Item.ItemType == ListItemType.Footer)
            {
                if (showAllBirthdayBtn)
                {
                    ((Literal)e.Item.FindControl("litBirthdaysClosingTag")).Text = "</div>";
                }
            }
        }

        /// <summary>
        /// Repeater rptAnniversaries_ItemDataBound ItemDataBound Event Handler
        /// </summary>
        protected void rptAnniversaries_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (annivCount == 3)
                {
                    ((Literal)e.Item.FindControl("litAnniversaries")).Text = "<div id=\"an\" style=\"display:none;\" class=\"anniversaryContainer\">";
                }

                DateTime annivDate = Convert.ToDateTime(DataBinder.Eval(e.Item.DataItem, "SignupDate"));
                string username = DataBinder.Eval(e.Item.DataItem, "Username").ToString();

                ((HtmlContainerControl)e.Item.FindControl("spnAnniversaryTime")).InnerText = FormatFutureTimeSpan(annivDate, true, false);

                ((HyperLink)e.Item.FindControl("hlUsername")).Text = username;
                ((HyperLink)e.Item.FindControl("hlUsername")).NavigateUrl = GetPersonalChannelUrl(username).ToString(); ;

                annivCount++;
            }
            else if (e.Item.ItemType == ListItemType.Header)
            {
                if (showAllAnniversaryBtn)
                {
                    ((HtmlGenericControl)e.Item.FindControl("ulSeeAllAnniversaries")).Visible = true;
                }
            }
            else if (e.Item.ItemType == ListItemType.Footer)
            {
                if (showAllAnniversaryBtn)
                {
                    ((Literal)e.Item.FindControl("litAnniversariesClosingTag")).Text = "</div>";
                }
            }
        }

        /// <summary>
        /// Repeater rptEvents ItemDataBound Event Handler
        /// </summary>
        protected void rptEvents_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            string x = e.Item.ToString();

            // Execute the following logic for Items and Alternating Items.
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                string eventTitle = DataBinder.Eval(e.Item.DataItem, "title").ToString();
                int eventId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "event_id"));
                int userId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "user_id"));
                string location = DataBinder.Eval(e.Item.DataItem, "location").ToString();
                int communityId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "community_id"));
                string tracking_request_GUID = DataBinder.Eval(e.Item.DataItem, "tracking_request_GUID").ToString();

                ((HyperLink)e.Item.FindControl("hlEventName")).Text = eventTitle;
                ((HyperLink)e.Item.FindControl("hlEventName")).NavigateUrl = ResolveUrl("~/mykaneva/events/eventDetail.aspx?event=" + eventId);

                ((HyperLink)e.Item.FindControl("hlEventEdit")).Text = "edit";
                ((HyperLink)e.Item.FindControl("hlEventEdit")).NavigateUrl = ResolveUrl("~/mykaneva/events/eventForm.aspx?event=" + eventId);
                ((HtmlContainerControl)e.Item.FindControl("spnEdit")).Visible = userId == KanevaWebGlobals.CurrentUser.UserId ? true : false;

                Community community = GetCommunityFacade.GetCommunity(communityId);
                ConfigureCommunityMeetMe3D((HtmlAnchor)e.Item.FindControl("lbGotoWorld"), community.WOK3App.GameId, community.CommunityId, community.Name, tracking_request_GUID);
                ((HtmlAnchor)e.Item.FindControl("lbGotoWorld")).InnerText = location;

                if (basicEventCount == 3)
                {
                    ((Literal)e.Item.FindControl("litYourEvents")).Text = "<div id=\"ye\" style=\"display:none;\" class=\"basicEventsContainer\">";
                }

                DateTime startDate = Convert.ToDateTime(DataBinder.Eval(e.Item.DataItem, "start_time"));

                string startTimespan = FormatFutureTimeSpan(startDate, false, true);
                ((Literal)e.Item.FindControl("litEventWhen")).Text = startTimespan;

                // If user has not accepted/declined invite, show attend link
                if (Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "invite_status_id")) == (int)EventInvitee.Invite_Status.INVITED)
                {
                    HtmlAnchor lbAttend = (HtmlAnchor) e.Item.FindControl ("lbAttend");
                    if (startTimespan == "Now")
                    {
                        ConfigureCommunityMeetMe3D(lbAttend, community.WOK3App.GameId, community.CommunityId, community.Name, tracking_request_GUID);
                        lbAttend.InnerHtml = "Attend Now &#187;";
                        lbAttend.Attributes.Add ("href", "javascript:void(0);");
                    }
                    else
                    {
                        lbAttend.Attributes.Add("href", ResolveUrl("~/mykaneva/events/eventDetail.aspx?event=" + eventId + "&action=attend&" + Constants.cREQUEST_TRACKING_URL_PARAM + "=" + tracking_request_GUID));
                    }

                    ((HtmlContainerControl)e.Item.FindControl("divAttend")).Visible = true;
                }

                basicEventCount++;
            }
            else if (e.Item.ItemType == ListItemType.Header)
            {
                if (showAllYourEventsBtn)
                {
                    ((HtmlGenericControl)e.Item.FindControl("ulSeeAllYourEvents")).Visible = true;
                }
            }
            else if (e.Item.ItemType == ListItemType.Footer)
            {
                if (showAllYourEventsBtn)
                {
                    ((Literal)e.Item.FindControl("litYourEventsClosingTag")).Text = "</div>";
                }
            }
        }

        /// <summary>
        /// Repeater rptEvents ItemDataBound Event Handler
        /// </summary>
        protected void rptEventsPremium_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            string x = e.Item.ToString();

            // Execute the following logic for Items and Alternating Items.
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                string eventTitle = DataBinder.Eval(e.Item.DataItem, "title").ToString();
                int eventId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "event_id"));
                int userId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "user_id"));
                string location = DataBinder.Eval(e.Item.DataItem, "location").ToString();
                int communityId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "community_id"));
                string tracking_request_GUID = DataBinder.Eval(e.Item.DataItem, "tracking_request_GUID").ToString();

                ((HyperLink)e.Item.FindControl("hlEventName")).Text = eventTitle;
                ((HyperLink)e.Item.FindControl("hlEventName")).NavigateUrl = "~/mykaneva/events/eventDetail.aspx?event=" + eventId;

                ((HyperLink)e.Item.FindControl("hlEventEdit")).Text = "edit";
                ((HyperLink)e.Item.FindControl("hlEventEdit")).NavigateUrl = "~/mykaneva/events/eventForm.aspx?event=" + eventId;
                ((HtmlContainerControl)e.Item.FindControl("spnEdit")).Visible = userId == KanevaWebGlobals.CurrentUser.UserId ? true : false;

                Community community = GetCommunityFacade.GetCommunity(communityId);
                ConfigureCommunityMeetMe3D((HtmlAnchor)e.Item.FindControl("lbGotoWorld"), community.WOK3App.GameId, community.CommunityId, community.Name, tracking_request_GUID);
                ((HtmlAnchor)e.Item.FindControl("lbGotoWorld")).InnerText = location;

                if (premiumEventCount == 3)
                {
                    ((Literal)e.Item.FindControl("litYourPremiumEvents")).Text = "<div id=\"pe\" style=\"display:none;\" class=\"basicEventsContainer\">";
                }

                DateTime startDate = Convert.ToDateTime(DataBinder.Eval(e.Item.DataItem, "start_time"));

                string startTimespan = FormatFutureTimeSpan(startDate, false, true);
                ((Literal)e.Item.FindControl("litEventWhen")).Text = startTimespan;

                // If user has not accepted/declined invite, show attend link
                /*
                if (DataBinder.Eval (e.Item.DataItem, "invite_status_id") != null &&
                    Convert.ToInt32 (DataBinder.Eval (e.Item.DataItem, "invite_status_id")) == (int) EventInvitee.Invite_Status.INVITED)
                {
                    LinkButton lbAttend = (LinkButton) e.Item.FindControl ("lbAttend");
                    if (startTimespan == "Now")
                    {
                        ConfigureCommunityMeetMe3D (lbAttend, community.WOK3App.GameId, community.CommunityId, community.Name, tracking_request_GUID);
                        lbAttend.Text = "Attend Now &#187;";
                    }
                    else
                    {
                        lbAttend.Attributes.Add ("href", ResolveUrl ("~/mykaneva/events/eventDetail.aspx?event=" + eventId + "&action=attend"));
                    }

                    ((HtmlContainerControl) e.Item.FindControl ("divAttend")).Visible = true;
                }
                */
                premiumEventCount++;
            }
            else if (e.Item.ItemType == ListItemType.Header)
            {
                if (showAllPremiumEventsBtn)
                {
                    ((HtmlGenericControl)e.Item.FindControl("ulSeeAllYourPremiumEvents")).Visible = true;
                }
            }
            else if (e.Item.ItemType == ListItemType.Footer)
            {
                if (showAllPremiumEventsBtn)
                {
                    ((Literal)e.Item.FindControl("litYourPremiumEventsClosingTag")).Text = "</div>";
                }
            }
        }

        #endregion Event Handlers

        #region Declerations

        // private 
        private User Current_User = KanevaWebGlobals.CurrentUser;

        #endregion Declarations
    }
}