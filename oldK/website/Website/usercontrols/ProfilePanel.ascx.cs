///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using MagicAjax;
using Kaneva.BusinessLayer.Facade;
using KlausEnt.KEP.Kaneva.mykaneva.widgets;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva.usercontrols
{
    public partial class ProfilePanel : ModuleViewBaseControl
    {
        #region Declerations

        private int _ProfileOwnersID = 0;
        private int _ChannelID = 0;

        protected LinkButton lbEdit, lbDelete;
        protected ImageButton lblOnline;
        protected HtmlImage voteImg;


        protected HtmlTable tblEdit;
        protected Label lblTitle;

        protected Label lblUserName, lblDisplayName;
        protected HtmlImage imgAvatar, imgAdd, imgMsg;
        protected LinkButton btnAddFriend, btnAddFriend2;
        protected LinkButton btnSendMessage, btnSendMessage2;
        protected LinkButton btnBlockUser, btnBlockUser2;
        protected LinkButton btnReportAbuse, btnReportAbuse2;
        protected LinkButton btnTellOthers, btnTellOthers2;
        protected Label lblAge;
        protected Label lblGender;
        protected Label lblLocation;
        protected Label lblMemberSince;
        protected Label lblLastLogin;
        protected Label lblLastUpdate;
        protected HyperLink hlPerm;
        protected LinkButton btnVote;
        protected Literal litOnline, litDisplayText, litHotLinkLocation;
        protected Literal litWokUrl;

        protected HtmlContainerControl controlSection1, controlSection2, controlSection3;
        protected HtmlContainerControl divUrl;
        protected HtmlAnchor aPictures;

        protected HtmlAnchor aAvatar;

        protected Literal litIndexTools;

        // Digg someone
        protected Label lblVotes;

        #endregion

        private void Page_Load(object sender, System.EventArgs e)
        {
            // Put user code to initialize the page here
            if (!IsPostBack)
            {
                BindData();
                
                // Which rave graphic do we show?	 
                RaveFacade raveFacade = new RaveFacade ();
                if (!raveFacade.IsCommunityRaveFree (KanevaWebGlobals.CurrentUser.UserId, ChannelID, RaveType.eRAVE_TYPE.SINGLE))
                {
                    btnVote.CssClass = "RavePlus";
                }
                else
                {
                    btnVote.CssClass = "raveBtn";
                }
                litCommunityId.Text = ChannelID.ToString ();
            }

            if (GetUserId().Equals(ProfileOwnersID))
            {
                btnAddFriend.Attributes["onclick"] = "javascript:" +
                    "alert('You cannot add yourself as a friend.');return (false);";
               
            }
            else
            {
                btnAddFriend.Attributes["onclick"] = "javascript:return " +
                    "confirm('Send a friend request to this user?')";
               
            }

            SetBlockedText();
        }

        #region Helper Methods

        private void BindData()
        {
            //// If user has at least 1 public photo, then display Show More Photos link
            //DataRow drMedia = StoreUtility.GetUserMediaCounts (ProfileOwnersID);
            //aPictures.Visible = false;

            //try
            //{
            //    if (Convert.ToUInt32(drMedia["sum_picture"]) > 0)
            //    {
            //        aPictures.Visible = true;
            //        aPictures.HRef = ResolveUrl("~/mykaneva/pictures.aspx?userId=" + ProfileOwnersID);
            //    }
            //}
            //catch { }

            //gather USER data 
            UserFacade userFacade = new UserFacade();
            User userProfileOwner = userFacade.GetUser(ProfileOwnersID);

            //if profile owner is the same as the viewing user, hide the meet me in three-D fly out
            //else continue checks.
            if (GetUserId() != ProfileOwnersID)
            {
                //check to see if browsing user is logged in/a member of kaneva
                if (Request.IsAuthenticated)
                {
                    //browsing user is a member of WOK
                    if (KanevaWebGlobals.CurrentUser.HasWOKAccount)
                    {
                        //check to see if profile owner is a wok member
                        //profile owner and browsing user are WOK members
                        if (userProfileOwner.WokPlayerId > 0)
                        {
                            //if profile owner is logged in to WOK set string to go to their location - for now always set to go to their location
                            if (KanevaWebGlobals.IsInWorld(userProfileOwner.Ustate))
                            {
                                btnMeetMe3D.Attributes.Add("href", ResolveUrl("~/kgp/playwok.aspx") + "?goto=P" + ProfileOwnerId.ToString() + "&ILC=MM3D&link=mmnowin");
                            }
                            else
                            {
                                
                                btnMeetMe3D.Attributes.Add("href", ResolveUrl("~/myKaneva/newMessage.aspx") + "?userId=" + this.ProfileOwnerId.ToString() + "&meetMe3D=A");
                            }
                        }
                        //profile owner is not a wok member (browsing user is)
                        else
                        {
                            //give user option of going in world anyway
                            
                            btnMeetMe3D.Attributes.Add("href", ResolveUrl("~/kgp/playwok.aspx") + "?goto=C0&ILC=MM3D&link=private");
                        }
                    }
                    //browsing user is not a memeber of WOK (profile owner status unknown) send user to download page
                    else
                    {
                        
                        btnMeetMe3D.Attributes.Add("href", ResolveUrl("~/kgp/playwok.aspx") + "?ILC=MM3D&link=getinto");
                    }
                }
                // browsingUser is not logged in show only login/join kaneva (no one's WOK status know)
                else
                {
                    btnMeetMe3D.Attributes.Add("href", ResolveUrl("~/loginSecure.aspx") + "?ILC=MM3D&link=joinsignin");
                }
            }
            else
            {
                //show link buttons to WOK (profile owner is member of WOK)
                if (userProfileOwner.WokPlayerId > 0)
                {
                    btnMeetMe3D.Attributes.Add("href", ResolveUrl("~/kgp/playwok.aspx") + "?goto=A" + ProfileOwnerId.ToString() + "&ILC=MM3D&link=gotomyhome");
                }
                //show link to download WOK (profile owner is NOT member of WOK)
                else
                {
                    btnMeetMe3D.Attributes.Add("href", ResolveUrl("~/kgp/playwok.aspx") + "?ILC=MM3D&link=getinto");
                }
            }

            imgAvatar.Src = UsersUtility.GetProfileImageURL(userProfileOwner.ThumbnailMediumPath, "me", userProfileOwner.Gender);
            imgAvatar.Alt = userProfileOwner.Username;

            // set up the image to display using highslide
            aAvatar.HRef = ResolveUrl("~/mykaneva/PictureProfileDetail.aspx?userId=" + ProfileOwnersID);
            aAvatar.Attributes.Add("onclick", "return hs.expand(this, { easing: 'easeOutBack', easingClose: 'easeInBack', src: '" +
                UsersUtility.GetProfileImageURL(userProfileOwner.ThumbnailXlargePath, "xl", userProfileOwner.Gender) + "' })");


            lblUserName.Text = userProfileOwner.Username;
            lblDisplayName.Text = userProfileOwner.DisplayName;

            //set users online/inworld status
            litOnline.Text = GetOnlineText(userProfileOwner.Ustate);

            int age = -1;
            string gender = "";
            string location = "";
            // get birth date
            age = userProfileOwner.Age;

            // get gender
            gender = userProfileOwner.Gender == "M" ? "Male" : "Female";

            // get 'location'
            location = userProfileOwner.Location;

            DataRow drControlPanel = KlausEnt.KEP.Kaneva.framework.widgets.WidgetUtility.GetModuleControlPanel(InstanceId);

            if (drControlPanel != null)
            {
                if (drControlPanel["title"] != DBNull.Value && drControlPanel["title"].ToString() != "")
                {
                    lblTitle.Text = drControlPanel["title"].ToString();
                    lblTitle.Visible = true;
                }
                else
                {
                    lblTitle.Visible = false;
                }

                // if age should be shown
                if (drControlPanel["show_age"].ToString() == "1")
                {
                    lblAge.Visible = true;
                    lblAge.Text = "" + age + " Years Old";
                }
                else
                {
                    lblAge.Visible = false;
                }

                // if gender should be shown
                if (drControlPanel["show_gender"].ToString() == "1")
                {
                    lblGender.Visible = true;
                    lblGender.Text = "" + gender;
                }
                else
                {
                    lblGender.Visible = false;
                }

                // if location should be shown
                if (drControlPanel["show_location"].ToString() == "1")
                {
                    lblLocation.Visible = true;
                    lblLocation.Text = location;
                }
                else
                {
                    lblLocation.Visible = false;
                }
            }

            //digg
            lblVotes.Text = userProfileOwner.NumberOfDiggs.ToString();

            // The permalink
            string permLink = "";

            if (userProfileOwner.URL.Length.Equals(0))
            {
                permLink = KanevaGlobals.GetPersonalChannelUrl(userProfileOwner.NameNoSpaces);
            }
            else
            {
                permLink = "http://" + KanevaGlobals.SiteName + "/" + userProfileOwner.URL;
            }

            hlPerm.Text = permLink;
            hlPerm.Attributes.Add("alt", permLink);
            hlPerm.NavigateUrl = permLink;

            DataRow drSystemStats = CommunityUtility.GetCommunityStats(ChannelID);
            if (drSystemStats != null)
            {
                lblMemberSince.Text = Convert.ToDateTime(drSystemStats["signup_date"].ToString()).ToShortDateString();
                lblLastLogin.Text = Convert.ToDateTime(drSystemStats["last_login"].ToString()).ToShortDateString();
                lblLastUpdate.Text = Convert.ToDateTime(drSystemStats["last_update"].ToString()).ToShortDateString();
            }
        }

        /// <summary>
        /// Set the blocked text
        /// </summary>
        private void SetBlockedText()
        {
            UserFacade userFacade = new UserFacade();

            if (userFacade.IsUserBlocked(GetUserId(), ProfileOwnersID))
            {
                btnBlockUser.CssClass = "UnblockUser";
            }
            else
            {
                btnBlockUser.CssClass = "BlockUser";
            }
        }

        private void AddFriend()
        {
            if (Request.IsAuthenticated)
            {
                // Add them as a friend
                int ret = GetUserFacade.InsertFriendRequest(GetUserId(), ProfileOwnersID);

                if (ret.Equals(1))	// already a friend
                {
                    AjaxCallHelper.WriteAlert("This member is already your friend.");
                }
                else if (ret.Equals(2))  // pending friend request
                {
                    AjaxCallHelper.WriteAlert("You have already sent this member a friend request.");
                }
                else
                {
                    // send request email
                    MailUtilityWeb.SendFriendRequestEmail(GetUserId(), ProfileOwnersID);
                }

            }
        }

        #endregion


        #region Event Handlers

        /// <summary>
        /// Handle a profile vote
        /// </summary>
        protected void btnVote_Click(object sender, CommandEventArgs e)
        {
            if (!Request.IsAuthenticated)
            {
                Response.Redirect (GetLoginURL ());
            }

            RaveFacade raveFacade = new RaveFacade ();
            RaveType.eRAVE_TYPE eRaveType = e.CommandArgument.ToString ().Equals ("megarave") ? RaveType.eRAVE_TYPE.MEGA : RaveType.eRAVE_TYPE.SINGLE;

            if (!raveFacade.IsCommunityRaveFree (KanevaWebGlobals.CurrentUser.UserId, ChannelID,
                eRaveType) && !this.IsUserAdministrator ())
            {
                // Call popFacebox
                if (eRaveType == RaveType.eRAVE_TYPE.MEGA)
                {
                    AjaxCallHelper.Write ("popFaceboxMega();");
                }
                else
                {
                    AjaxCallHelper.Write ("popFacebox();");
                }
            }
            else  // first time rave
            {
                // First rave is free.  Insert a normal rave.
                raveFacade.RaveCommunity (KanevaWebGlobals.CurrentUser.UserId, ChannelID, WebCache.GetRaveType (eRaveType), "");

                btnVote.CssClass = "RavePlus";
            }
        }

        /// <summary>
        /// Handle a report abuse
        /// </summary>
        protected void btnReportAbuse_Click(object sender, System.EventArgs e)
        {
            Response.Redirect(ResolveUrl("~/suggestions.aspx?mode=WB&category=KANEVA%20Web%20Site&rurl=" + Server.UrlEncode(Request.Url.ToString())));
        }

        private void btnAddFriend_Click(object sender, EventArgs e)
        {
            // Must be logged in
            if (!Request.IsAuthenticated)
            {
                AjaxCallHelper.WriteAlert("Please sign in to add this user as a friend");
                return;
            }

            // Can't friend yourself
            if (GetUserId().Equals(ProfileOwnersID))
            {
                AjaxCallHelper.WriteAlert("You cannot add yourself as a friend.");
                return;
            }

            AddFriend();
            //Response.Redirect (Request.CurrentExecutionFilePath + "?communityId=" + ChannelID;
        }

        /// <summary>
        /// Handle a profile vote
        /// </summary>
        protected void btnTellOthers_Click(object sender, System.EventArgs e)
        {
            Response.Redirect(ResolveUrl("~/myKaneva/newMessage.aspx?aboutId=" + this.ProfileOwnersID));
        }

        /// <summary>
        /// Handle a block user
        /// </summary>
        protected void btnBlockUser_Click(object sender, System.EventArgs e)
        {
            // Must be logged in
            if (!Request.IsAuthenticated)
            {
                AjaxCallHelper.WriteAlert("Please sign in to block/unblock this user");
                return;
            }

            // Can't block yourself
            if (GetUserId().Equals(ProfileOwnersID))
            {
                AjaxCallHelper.WriteAlert("You cannot block yourself.");
                return;
            }

            UserFacade userFacade = new UserFacade();

            if (userFacade.IsUserBlocked(GetUserId(), ProfileOwnersID))
            {
                UsersUtility.UnBlockUser(GetUserId(), ProfileOwnersID);
            }
            else
            {
                UsersUtility.BlockUser(GetUserId(), ProfileOwnersID);
            }

            SetBlockedText();
        }

        private void btnSendMessage_Click(object sender, EventArgs e)
        {
            Response.Redirect(ResolveUrl("~/myKaneva/newMessage.aspx?userId=" + this.ProfileOwnersID));
        }

        #endregion

        #region Accessor Functions

        public int ChannelID
        {
            get
            {
                return _ChannelID;
            }
            set
            {
                _ChannelID = value;
            }
        }

        public int ProfileOwnersID
        {
            get
            {
                return _ProfileOwnersID;
            }
            set
            {
                _ProfileOwnersID = value;
            }
        }

        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            btnAddFriend.Click += new EventHandler(btnAddFriend_Click);
           
            btnSendMessage.Click += new EventHandler(btnSendMessage_Click);
        }
        #endregion
    }
}