<%@ Control Language="c#" AutoEventWireup="false" Codebehind="NavMessages.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.NavMessages" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<div id="thirdaryNav">
	<ul>
		<li id="navInbox" runat="server"><a id="aInbox" runat="server">Private Messages</a></li>
		<!--<li id="navBulletins" runat="server"><a id="aBulletins" runat="server">Group Bulletins</a></li>-->
		<li id="navChannelReq" runat="server"><a id="aChannelReq" runat="server">Community Requests</a></li>
		<li id="navTrash" runat="server"><a id="aTrash" runat="server">Trashcan</a></li>
	</ul>        
</div>