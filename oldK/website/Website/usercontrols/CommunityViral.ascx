<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CommunityViral.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.usercontrols.CommunityViral" %>
    
    <link href="../../css/community_invite/temp_cifStyles.css" type="text/css" rel="stylesheet">
    
    <script src="../../../jscript/prototype.js" type="text/javascript" language="javascript"></script>
    <script type="text/javascript"><!--
    function CheckAll(t) 
    {
	    Select_All(t.checked);
    }   
    //-->
    </script>

<div class="cif_wrapper" id="divLoading">
	<table height="300" cellspacing="0" cellpadding="0" width="70%" border="0">
		<tr>
			<th class="loadingText" id="divLoadingText">
				Loading...
			</th>
		</tr>
		<tr>
		    <td valign="top" align="center">
		        <div id="divProgress"><img src="../../images/progress_bar.gif" alt="Progress"/></div>
		    </td>
		</tr>
	</table>
</div>
<div id="divData" style="DISPLAY: none">

     <div class="cif_wrapper" id="divCommunityViral" runat="server">
          <div class="cif_top"><!-- top border --></div>
          <div class="cif_backer">
            <div class="cif_title" style="height:64px;text-align:center;">
                <h1 style="width:100%;text-align:center;"><br />Invite Friends to Join Your New World</h1>
            </div>
            <table class="cif_Search">
              <tr>
                <td style="text-align:right">Invite Friends:&nbsp;</td>
                <td style="width:300px"><asp:TextBox ID="txtSearch" style="width:300px" MaxLength="40" runat="server"/></td>
                <td style="width:115px"><asp:linkbutton text="Search" id="lbSearch" oncommand="lbSearch_Click" tooltip="Search" runat="server" /></td>
              </tr>
            </table>
            <div class="cif_insetBox">
              <div class="cif_selectAll">
                <asp:checkbox id="cbSelectAllMembers" onclick="CheckAll(this);" runat="server"/>
                &nbsp;Select All</div>
              <div class="cif_whtBox" id="divFriends">
                  <ul>
                  <asp:repeater id="rptFriends" runat="server">
                    <ItemTemplate>   
                       <li class="select">
                          <div class="cif_thumb"><a title="asfd" href='<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "NameNoSpaces").ToString ())%>'><img src='<%#GetProfileImageURL (DataBinder.Eval(Container.DataItem, "ThumbnailMediumPath").ToString (), "me", DataBinder.Eval(Container.DataItem, "Gender").ToString ())%>'/></a></div>
                          <div style="text-align:center"><asp:checkbox id="chkEdit" name="select_join" runat="server"/></div>
                          <p><a href='<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "NameNoSpaces").ToString ())%>'><%# TruncateWithEllipsis(DataBinder.Eval(Container.DataItem, "DisplayName").ToString(), 16)%></a></p>
                          <p><%# TruncateWithEllipsis(DataBinder.Eval(Container.DataItem, "Username").ToString(), 16)%></p>
                          <p class="small" style="overflow:hidden;"><%# DataBinder.Eval(Container.DataItem, "Location")%></p>   
                        </li>
                        <input type="hidden" runat="server" id="hidFriendId" value='<%#DataBinder.Eval(Container.DataItem, "UserId")%>' name="hidFriendId">
                        <%# (Convert.ToInt32(Container.ItemIndex) + 1) % 5 == 0 && Convert.ToInt32(Container.ItemIndex) > 0 ? "</ul><ul> " : ""%>
                    </ItemTemplate>
                  </asp:repeater>
                   </ul>
               </div>
               <div class="cif_buttons">
                 <div class="cif_btnSend"><asp:linkbutton text="Send Invitations" id="lbSend" OnClientClick="ShowLoading(true,'Please wait while we complete your request...');" oncommand="lbSend_Click" tooltip="Send Invitations" runat="server" /></div>
                 <div class="cif_btnSkip"><asp:linkbutton text="Skip" id="lbSkip" oncommand="lbSkip_Click" tooltip="Skip" runat="server" /></div>
                 <div class="cif_btnMore"><asp:linkbutton text="Send &amp; Invite People Not on Kaneva" id="lbSendInviteMore" OnClientClick="ShowLoading(true,'Please wait while we complete your request...');" oncommand="lbSendInviteMore_Click" tooltip="Send &amp; Invite People Not on Kaneva" runat="server" /></a></div>
                 <div class="clearboth"><!-- clear the floats --></div>
              </div>
            </div>
          </div>
          <div class="cif_btm"><!-- btm border --></div>
        </div>
</div>