﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FacebookInvite.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.usercontrols.FacebookInvite" %>
<%@ Register TagPrefix="UserControl" TagName="FacebookJS" Src="~/usercontrols/FacebookJavascriptInclude.ascx" %>
<link href="../css/base/buttons.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="../jscript/colorbox/jquery-1.7.1.min.js"></script>	
<usercontrol:FacebookJS runat="server"></usercontrol:FacebookJS>

<script>
var $j = jQuery.noConflict();
</script>
<div id="fb-root"></div> 
<style>
div {text-align:left;}
#footer {text-align:center;}
</style>   

<asp:panel class="panel" runat="server" id="pnlLogin" visible="false">
	<img id="Img2" onclick="FBLogin(FBAction.CONNECT);" src="~/images/facebook/FB_signin.png" runat="server" />
</asp:panel> 

<asp:panel class="panel" runat="server" id="pnlConnect" visible="false">
	<img id="Img1" onclick="FBLogin(FBAction.CONNECT);" src="~/images/facebook/FB_FindFriends.png" runat="server" />
	<p>Earn 1000 Rewards or more!</p>
</asp:panel> 

<asp:panel class="listpanel" runat="server" id="pnlList" visible="false">
<div class="fbFriends">
	<h1>Invite Friends to Join</h1>
	<div class="listframe">
		<asp:repeater id="rptFBFriends" runat="server">
			<itemtemplate>
				<div class="fbfriend">
					<img class="thumb" src="<%# DataBinder.Eval(Container.DataItem, "Picture") %>" />
					<div class="name"><%# Server.HtmlDecode (DataBinder.Eval(Container.DataItem, "Name").ToString()) %></div>
					<div><%# GetUserLocale(DataBinder.Eval(Container.DataItem, "Locale").ToString()) %></div>
					<a class="button" id="btnSendInvite" onclick='SendMsg(<%# Server.HtmlDecode (DataBinder.Eval(Container.DataItem, "Id").ToString())%>, <%# CurrentUser_UserId() %>)'><span>Send Invite</span></a>
				</div>
			</itemtemplate>
		</asp:repeater><span id="noFBFriends" runat="server" visible="false">
		<p class="notfoundmsg" id="noFBFriendsMsg" runat="server">We could not find any of your Facebook friends.</p> 
		</span> 
	</div>
</div>

<div class="fbOnKaneva">
	<h1>Friends on Kaneva</h1><a class="button" onserverclick="btnSendAll_Click" id="btnSendAll" runat="server" visible="false"><span>Request All</span></a>
	<div class="listframe">
		<asp:repeater id="rptFBFriendsOnKaneva" runat="server" onitemdatabound="rptFBFriendsOnKaneva_ItemDataBound">
			<itemtemplate>
				<div class="kanfriend">
					<a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "NameNoSpaces").ToString ())%>>
					<img class="thumb" runat="server" src='<%#GetProfileImageURL (DataBinder.Eval(Container.DataItem, "ThumbnailMediumPath").ToString (), "sq", DataBinder.Eval(Container.DataItem, "Gender").ToString (), Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "FacebookSettings.UseFacebookProfilePicture")), Convert.ToUInt64(DataBinder.Eval(Container.DataItem, "FacebookSettings.FacebookUserId"))  )%>' border="0" id="Img1"/></a>
					<div class="name"><a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "NameNoSpaces").ToString ())%>><%# Server.HtmlDecode (DataBinder.Eval(Container.DataItem, "DisplayName").ToString()) %></a></div>
					<div class="loc"><%# DataBinder.Eval(Container.DataItem, "Location").ToString() %></div>
					<div class="btncontainer">
						<a class="button" id="btnAddFriend" runat="server"><span>Send Friend Request</span></a>
						<img id="imgloading<%# DataBinder.Eval (Container.DataItem, "UserId").ToString() %>" alt="" src="../images/ajax-loader_sm.gif" />
					</div>
				</div>
			</itemtemplate>
		</asp:repeater>
		<span id="noFBOnKaneva" runat="server" visible="false">
		<p class="notfoundmsg bold">No Friends?</p><p class="notfoundmsg2">The World of Kaneva is more fun with friends.</p> 
		</span>
	</div>
</div>
</asp:panel>