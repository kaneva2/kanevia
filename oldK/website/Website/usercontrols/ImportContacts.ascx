﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ImportContacts.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.usercontrols.ImportContacts" %>
<%@ Register TagPrefix="UserControl" TagName="ColorBox" Src="~/usercontrols/ColorBox.ascx" %>
<%@ Register TagPrefix="UserControl" TagName="FacebookJS" Src="~/usercontrols/FacebookJavascriptInclude.ascx" %>

<link href="../css/base/buttons_new.css" type="text/css" rel="stylesheet">
<link href="../css/importContacts.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="../jscript/colorbox/jquery-1.7.1.min.js"></script>	
<usercontrol:FacebookJS runat="server"></usercontrol:FacebookJS>

<link href="../jscript/colorbox/colorbox.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../jscript/colorbox/jquery.colorbox.js"></script>

<script>
	var $j = jQuery.noConflict();
</script>
<div id="fb-root"></div> 
 

<div class="step-data-container">

<asp:updatepanel id="upContent" runat="server" childrenastriggers="true" updatemode="conditional" visible="true">
	<contenttemplate>	

		<!-- STEP 1 -->
		<span id="spnStep1" runat="server" visible="true">	

			<div class="box">
				<p class="calltoaction">Choose where to find your friends</p>

				<ul class="mailicons">
					<li><img id="gmail" src="~/images/connectwithfriends/gmail_logo_70.jpg" runat="server" clientidmode="static" class="email-provider" /><br />Gmail</li>
					<li><img id="yahoo" src="~/images/connectwithfriends/yahoo_logo_70.jpg" runat="server" clientidmode="static" class="email-provider" /><br />Yahoo</li>
					<li><img id="aol" src="~/images/connectwithfriends/aol_logo_70.jpg" runat="server" clientidmode="static" class="email-provider" /><br />AOL</li>
					<li><img id="windowslive" src="~/images/connectwithfriends/outlook_logo_70.jpg" runat="server" clientidmode="static" class="email-provider" /><br />Outlook.com</li>
					<li><img id="facebook" onclick="FBLogin(FBAction.CONNECT);" src="~/images/connectwithfriends/faceBook_logo_70.jpg" runat="server" clientidmode="static" /><br />Facebook</li>
				</ul>
			</div>

			<div class="clearfix"></div>

			<div id="emailForm" class="box manual">
				<div class="manualcontainer">
					<p class="calltoaction">Send an individual email invite</p>
					
					<div class="errholder">
					<span id="spnAlertMsg" runat="server" class="errBox black"></span>
					</div>

					<p class="label"><span>To:</span><span class="muted text-left text-small">(seperate Email addresses by spaces, commas or semicolons)</span></p>
					<asp:textbox id="txtTo" TextMode="multiline" Rows="3" runat="server" />

					<span class="hidden">
					<label>Optional Personal Message:</label>
					<asp:textbox id="txtMessage" textmode="multiline" rows="5" maxlength="100" runat="server"/>
					</span>

					<div class="btncontainer">
						<asp:LinkButton cssclass="blue btnmedium" id="btnSendEmail" onclick="btnSendEmail_Click" runat="server" causesvalidation="false"><span>Send Invite</span></asp:LinkButton>
					</div>
				</div>
			</div>


		</span>


		<!-- STEP 2 -->
		<span id="spnStep2" runat="server" visible="false">
			
			<p class="text-left"><asp:literal id="litFriendsAsMembersMessage" runat="server"></asp:literal></p>
			<p class="text-left">Select which <%= ServiceType %> <%= ContactsOrFriends %>s you want to be friends with in Kaneva. They will be sent a 
				Kaneva friend request from you.</p>

			<div class="checkboxcontainer"><input class="checkall" type="checkbox" id="friendCheckAll" checked/><label>Select All</label></div>
			<div class="contactcontainer">
				<asp:repeater id="rptFriendsOnKaneva" runat="server">
					<itemtemplate>
						<div class=" row kanfriend">
							<asp:checkbox id="chkAddFriend" name="friend" runat="server" checked />
							<img class="thumb" runat="server" src='<%#GetProfileImageURL (DataBinder.Eval(Container.DataItem, "ThumbnailPath").ToString (), "sq", DataBinder.Eval(Container.DataItem, "Gender").ToString (), Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "UseFacebookProfilePicture")), Convert.ToUInt64(DataBinder.Eval(Container.DataItem, "FacebookUserId"))  )%>' border="0" id="Img1"/>
							<div class="name"><%# Server.HtmlDecode (DataBinder.Eval(Container.DataItem, "FirstName") + " " + DataBinder.Eval(Container.DataItem, "LastName")) %></div>
							<div class="displayname"><a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "Username").ToString ())%>><%# DataBinder.Eval(Container.DataItem, "Username") %></a></div>
							<div class="email"><%# DataBinder.Eval (Container.DataItem, "EmailAddresses[0]") %></div>
							<asp:hiddenfield id="hidUserId" value='<%# DataBinder.Eval(Container.DataItem, "UserId").ToString() %>' runat="server" />
						</div>
					</itemtemplate>
					<separatortemplate><hr /></separatortemplate>
				</asp:repeater>

				<asp:repeater id="rptFBFriendsOnKaneva" runat="server">
					<itemtemplate>
						<div class="row kanfriend">
							<asp:checkbox id="chkAddFriend" name="friend" runat="server" checked />
							<a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "NameNoSpaces").ToString ())%>>
							<img class="thumb" runat="server" src='<%#GetProfileImageURL (DataBinder.Eval(Container.DataItem, "ThumbnailMediumPath").ToString (), "sq", DataBinder.Eval(Container.DataItem, "Gender").ToString (), Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "FacebookSettings.UseFacebookProfilePicture")), Convert.ToUInt64(DataBinder.Eval(Container.DataItem, "FacebookSettings.FacebookUserId"))  )%>' border="0" id="Img1"/></a>
							<div class="name"><%# Server.HtmlDecode (DataBinder.Eval(Container.DataItem, "FirstName") + " " + DataBinder.Eval(Container.DataItem, "LastName")) %></div>
							<div class="displayname"><a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "NameNoSpaces").ToString ())%>><%# Server.HtmlDecode (DataBinder.Eval(Container.DataItem, "DisplayName").ToString()) %></a></div>
							<div class="email"><%# Server.HtmlDecode (DataBinder.Eval(Container.DataItem, "Email").ToString()) %></div>
							<asp:hiddenfield id="hidUserId" value='<%# DataBinder.Eval(Container.DataItem, "UserId").ToString() %>' runat="server" />
						</div>
					</itemtemplate>
					<separatortemplate><hr /></separatortemplate>
				</asp:repeater>
		
			</div>
			<div class="btncontainer">
				<asp:LinkButton cssclass="grey btnxsmall" id="btnSkip" runat="server" onclick="btnSkip_Click" causesvalidation="false"><span>Skip</span></asp:LinkButton>
				<asp:LinkButton cssclass="grey btnmedium" id="btnAddFriends" runat="server" onclick="btnAddFriends_Click" causesvalidation="false"><span>Add Friends</span></asp:LinkButton>
			</div>
			<p class="changemsg">Or you can choose a different service</p> 
			<p class="service-icons"><a href="~/mykaneva/invitefriend.aspx?service=none" runat="server">
					<img id="Img2" src="~/images/connectwithfriends/facebook.png" runat="server" />
					<img id="Img3" src="~/images/connectwithfriends/aol.png" runat="server" />
					<img id="Img4" src="~/images/connectwithfriends/yahoo.png" runat="server" />
					<img id="Img5" src="~/images/connectwithfriends/gmail.png" runat="server" />
					<img id="Img6" src="~/images/connectwithfriends/outlook.com.png" runat="server" />
				</a>
			</p>
		</span>

		<!-- STEP 3 -->
		<span id="spnStep3" runat="server" visible="false">
			
			<p class="text-left">Select which <%= ServiceType %> <%= ContactsOrFriends %>s to invite to Kaneva.<span id="msgSendInvitesTop" runat="server">They will be sent an invite email.</span></p>

			<div class="checkboxcontainer"><span id="inviteCheckAll" runat="server"><input class="checkall" type="checkbox" checked/><label>Select All</label></span></div>
			<div class="contactcontainer">
				<asp:repeater id="rptEmailContacts" runat="server">
					<itemtemplate>
						<div class="row invite">
							<div class="rowcheckbox"><asp:checkbox id="chkInvite" name="invite" runat="server" checked /></div> 
							<div class="name"><%# Server.HtmlDecode (DataBinder.Eval(Container.DataItem, "FirstName") + " " + DataBinder.Eval(Container.DataItem, "LastName")) %></div>
							<div class="email"><%# DataBinder.Eval (Container.DataItem, "EmailAddresses[0]") %></div>
							<asp:hiddenfield id="hidContactName" runat="server" value='<%# Server.HtmlDecode (DataBinder.Eval(Container.DataItem, "FirstName") + " " + DataBinder.Eval(Container.DataItem, "LastName")) %>' />
							<asp:hiddenfield id="hidContactEmail" runat="server" value='<%# DataBinder.Eval (Container.DataItem, "EmailAddresses[0]") %>' />
						</div>
					</itemtemplate>
					<separatortemplate><hr /></separatortemplate>
				</asp:repeater>
			
				<div class="listframe">
					<asp:repeater id="rptFBFriends" runat="server">
						<itemtemplate>
							<div class="row invite facebook">
								<div class="rowcheckbox"></div>
								<img class="thumb" src="<%# DataBinder.Eval(Container.DataItem, "Picture") %>" />
								<div class="namecontainer">
									<div class="name"><%# Server.HtmlDecode (DataBinder.Eval(Container.DataItem, "Name").ToString()) %></div>
									<div class="locale"><%# GetUserLocale(DataBinder.Eval(Container.DataItem, "Locale").ToString()) %></div>
								</div>
								<a class="grey btnxsmall button" id="btnSendInvite" href="javascript:void(0);" onclick='SendMsg(<%# Server.HtmlDecode (DataBinder.Eval(Container.DataItem, "Id").ToString())%>, <%# CurrentUser_UserId() %>)'><span>Send Invite</span></a>
							</div>
						</itemtemplate>
						<separatortemplate><hr /></separatortemplate>
					</asp:repeater>
				</div>

			</div>
			<div class="btncontainer">
				<asp:LinkButton cssclass="grey btnxsmall" id="btnSkipInvites" runat="server" onclick="btnSkip_Click" causesvalidation="false"><span>Skip</span></asp:LinkButton>
				<asp:LinkButton cssclass="grey btnmedium" id="btnSendInvites" runat="server" onclick="btnSendInvites_Click" causesvalidation="false" clientidmode="static"><span>Send Invites</span></asp:LinkButton>
				<p id="sendinginvites" class="sendingmsg" clientidmode="static" runat="server">Sending invites...</p>
			</div>
			<p class="muted text-left text-small" id="msgSendInvitesBottom" runat="server">Please send invites only to people you know who would be excited to join Kaneva.<br />
				We will send 2 reminders after the initial invite.</p>

			<p class="changemsg step3">Or you can choose a different service</p> 
			<p class="service-icons"><a id="A1" href="~/mykaneva/invitefriend.aspx?service=none" runat="server">
					<img id="Img7" src="~/images/connectwithfriends/facebook.png" runat="server" />
					<img id="Img8" src="~/images/connectwithfriends/aol.png" runat="server" />
					<img id="Img9" src="~/images/connectwithfriends/yahoo.png" runat="server" />
					<img id="Img10" src="~/images/connectwithfriends/gmail.png" runat="server" />
					<img id="Img11" src="~/images/connectwithfriends/outlook.com.png" runat="server" />
				</a>
			</p>

		</span>

	</contenttemplate>
</asp:updatepanel>

<input type="hidden" runat="server" clientidmode="static" id="sitename" value="" />    

</div>
