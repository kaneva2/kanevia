///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

namespace KlausEnt.KEP.Kaneva.usercontrols
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

	/// <summary>
	///		Summary description for Profile.
	/// </summary>
	public class Profile : System.Web.UI.UserControl
	{
		protected DropDownList drpRelationship, drpOrientation, drpChildren, drpEducation, drpIncome, drpHeightFeet, drpHeightInches, drpSmoking, drpDrinking;
		protected DropDownList drpReligion, drpEthnicity;

		protected HtmlTableRow trSexOrientation, trDrinking, trSmoking;

		protected PlaceHolder phStyleCustomizes;

		private int		_user_id;

		private string	_evenRowStyle			= "";
		private string	_oddRowStyle			= "";
		private string	_dataColumnAlignment	= "right";

		private void Page_Load(object sender, System.EventArgs e)
		{
			if (!IsPostBack)
			{
				// Add freaky to adults
				if (KanevaWebGlobals.CurrentUser.IsAdult)
				{
					drpRelationship.Items.Add (new ListItem ("Swinger", "Swinger"));
					drpRelationship.Items.Add (new ListItem ("Freaky", "Freaky"));

					drpOrientation.Items.Add (new ListItem ("Freak", "Freak"));

					trSexOrientation.Visible = true;
					trSmoking.Visible = true;
					trDrinking.Visible = true;
				}

				BindData( _user_id );
			}
		}

		private void BindData( int userId )
		{
			DataRow drUserProfile = UsersUtility.GetUserProfile (userId);
			if ( drUserProfile != null )
			{
				SetDropDownIndex (drpRelationship, Server.HtmlDecode (drUserProfile ["relationship"].ToString ()));
				SetDropDownIndex (drpOrientation, Server.HtmlDecode (drUserProfile ["orientation"].ToString ()));
				SetDropDownIndex (drpChildren, Server.HtmlDecode (drUserProfile ["children"].ToString ()));
				SetDropDownIndex (drpEducation, Server.HtmlDecode (drUserProfile ["education"].ToString ()));
				SetDropDownIndex (drpIncome, Server.HtmlDecode (drUserProfile ["income"].ToString ()));
				SetDropDownIndex (drpHeightFeet, Server.HtmlDecode (drUserProfile ["height_feet"].ToString ()));
				SetDropDownIndex (drpHeightInches, Server.HtmlDecode (drUserProfile ["height_inches"].ToString ()));
				SetDropDownIndex (drpSmoking, Server.HtmlDecode (drUserProfile ["smoke"].ToString ()));
				SetDropDownIndex (drpDrinking, Server.HtmlDecode (drUserProfile ["drink"].ToString ()));

				SetDropDownIndex (drpReligion, Server.HtmlDecode (drUserProfile ["religion"].ToString ()));
				SetDropDownIndex (drpEthnicity, Server.HtmlDecode (drUserProfile ["ethnicity"].ToString ()));
			}

			LoadCustomStyles();
		}

		#region Methods

		/// <summary>
		/// SetDropDownIndex
		/// </summary>
		protected void SetDropDownIndex (DropDownList drp, string theValue)
		{
			try
			{
				drp.SelectedValue = theValue;
			}
			catch (Exception)
			{}
		}

		public void Update()
		{
			// Update user profile
			UsersUtility.UpdateUserProfile (_user_id, Server.HtmlEncode (drpRelationship.SelectedValue), Server.HtmlEncode (drpOrientation.SelectedValue), Server.HtmlEncode (drpReligion.SelectedValue),
				Server.HtmlEncode (drpEthnicity.SelectedValue), Server.HtmlEncode (drpChildren.SelectedValue), Server.HtmlEncode (drpEducation.SelectedValue), Server.HtmlEncode (drpIncome.SelectedValue), 
				Convert.ToInt32 (Server.HtmlEncode (drpHeightFeet.SelectedValue)), Convert.ToInt32 (Server.HtmlEncode (drpHeightInches.SelectedValue)), Server.HtmlEncode (drpSmoking.SelectedValue), Server.HtmlEncode (drpDrinking.SelectedValue));
		}

		public void LoadCustomStyles()
		{
			Literal litCustomStyle = new Literal ();
			litCustomStyle.Text = "<style type=\"text/css\"><!--\n";

			if ( _evenRowStyle != "" )
			{
				litCustomStyle.Text += ".lineItemEven {" + _evenRowStyle + "}\n";
			}

			if ( _oddRowStyle != "" )
			{
				litCustomStyle.Text += ".lineItemOdd {" + _oddRowStyle + "}\n";
			}


			litCustomStyle.Text += "--></style>";
			phStyleCustomizes.Controls.Add (litCustomStyle);
		}
		

		#endregion

		#region Properties

		public int UserId
		{
			get { return _user_id; }
			set { _user_id = value; }
		}

		public string EvenRowStyle
		{
			get { return _evenRowStyle; }
			set { _evenRowStyle = value; }
		}

		public string OddRowStyle
		{
			get { return _oddRowStyle; }
			set { _oddRowStyle = value; }
		}

		public string DataColumnAlignment
		{
			get { return _dataColumnAlignment; }
			set { _dataColumnAlignment = value; }
		}

		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
