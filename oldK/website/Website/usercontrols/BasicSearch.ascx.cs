///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using KlausEnt.KEP.Kaneva;

namespace KlausEnt.KEP.Kaneva.usercontrols
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

	/// <summary>
	///		Summary description for BasicSearch.
	/// </summary>
	public class BasicSearch : BaseUserControl
	{

		private void Page_Load(object sender, System.EventArgs e)
		{
			lbChannels.Attributes["onclick"] = "setHilite(this)";
			lbPeople.Attributes["onclick"] = "setHilite(this)";
			lbVideos.Attributes["onclick"] = "setHilite(this)";
			lbPhotos.Attributes["onclick"] = "setHilite(this)";
			lbMusic.Attributes["onclick"] = "setHilite(this)";
			lbGames.Attributes["onclick"] = "setHilite(this)";

			if (!IsPostBack)
			{
				SetDefaultSearchType();
			}
	 
			// Is user logged in?
			if (Request.IsAuthenticated)
			{
				if (KanevaWebGlobals.CurrentUser.IsAdult)
				{
					hlChangeProfile.NavigateUrl = ResolveUrl("~/mykaneva/settings.aspx");
                    lblShowRestricted.Text = KanevaWebGlobals.CurrentUser.HasAccessPass ? "Yes" : "No";
				}
				else
				{
					divRestricted.Visible = false;
					//hlChangeProfile.Visible = false;
					//lblShowRestricted.Visible = false;
				}
			}
			else
			{
				hlChangeProfile.NavigateUrl = GetLoginURL();	
			}

		}

		private void SetDefaultSearchType()
		{
			int type;

			switch (SearchType)
			{
				case "People":
					lbPeople.CssClass = "selected";
					type = (int) Constants.eCHANNEL_SEARCH_TYPE.PERSONAL;
					break;
				case "Videos":
					lbVideos.CssClass = "selected";
					type = (int) Constants.eASSET_TYPE.VIDEO;
					break;
				case "Photos":
					lbPhotos.CssClass = "selected";
					type = (int) Constants.eASSET_TYPE.PICTURE;
					break;
				case "Music":
					lbMusic.CssClass = "selected";
					type = (int) Constants.eASSET_TYPE.MUSIC;
					break;
				case "Games":
					lbGames.CssClass = "selected";
					type = (int) Constants.eASSET_TYPE.GAME;
					break;
				default:
					lbChannels.CssClass = "selected";
					type = (int) Constants.eCHANNEL_SEARCH_TYPE.NON_PERSONAL;
					break;
			}

			hidSearchType.Value = type.ToString();
		}

		/// <summary>
		/// SetSearchType
		/// </summary>
		public string SearchType 
		{
			get { return m_def_search_type; }
			set { m_def_search_type = value; }
		}
		public string OrderBy
		{
			get { return rblOrderBy.SelectedValue; }	
		}
		public string NewWithin 
		{
			get { return rblNewWithin.SelectedValue; }	
		}
		public string Keywords 
		{
			get { return txtKeywords.Value.Trim(); }	
		}

		public bool PhotoRequired
		{
			set { m_photo_req = value; }
			get { return m_photo_req; }
		}

		protected void imgSearch_Click (object sender, System.Web.UI.ImageClickEventArgs e)			  
		{
			Response.Redirect(ResolveUrl("~/searchresults.aspx?" +				
				"type=" + hidSearchType.Value +
				"&kwd=" + Server.UrlEncode(Keywords) +
				"&ob=" + OrderBy + 
				"&wi=" + NewWithin +
				"&ph=" + chkPhotoRequired.Checked.ToString()));	
		}

		protected void AdvSearch_Click (object sender, EventArgs e)
		{
			Response.Redirect(ResolveUrl("~/searchadvanced.aspx?" +
				Constants.QUERY_STRING_SEARCH_TYPE + "=" + hidSearchType.Value));
		}

		
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	
		protected RadioButtonList	rblOrderBy, rblNewWithin;
		protected HtmlInputHidden	hidSearchType;
		protected HtmlInputText		txtKeywords;
		protected ImageButton		imgSearch;
		protected LinkButton		lbChannels, lbPeople, lbVideos, lbPhotos, lbMusic, lbGames, lbAdvSearch;
		protected Label				lblShowRestricted;
		protected HyperLink			hlChangeProfile;
		protected CheckBox			chkPhotoRequired;
		protected HtmlContainerControl	divRestricted;


		private string m_def_search_type = "Channels";
		private bool m_photo_req = true;
	}
}
