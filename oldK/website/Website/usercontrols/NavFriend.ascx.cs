///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

namespace KlausEnt.KEP.Kaneva
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

	/// <summary>
	///		Summary description for NavFriend.
	/// </summary>
	public class NavFriend : System.Web.UI.UserControl
	{

		private void Page_Load(object sender, System.EventArgs e)
		{
			if (!IsPostBack)
			{
				BindTabs();
			}
			
			aGroups.HRef = ResolveUrl ("~/mykaneva/friendsGroups.aspx");
			aRequests.HRef = ResolveUrl ("~/mykaneva/mailboxFriendRequests.aspx");
			aReqSent.HRef = ResolveUrl ("~/mykaneva/friendsPending.aspx");
			aInvite.HRef = ResolveUrl ("~/myKaneva/inviteFriend.aspx");
            aManageInvites.HRef = ResolveUrl ("~/myKaneva/inviteHistory.aspx");
		}
		
		public void BindTabs()
		{
			string css_selected = "selected";

			// Set selected tab
            if ( ActiveTab.Equals(TAB.GROUPS) )
			{
				navGroups.Attributes.Add("class",css_selected);
			}
			else if ( ActiveTab.Equals(TAB.REQUESTS) )
			{
				navRequests.Attributes.Add("class",css_selected);
			}
			else if ( ActiveTab.Equals(TAB.REQUESTS_SENT) )
			{
				navReqSent.Attributes.Add("class",css_selected);
			}
			else if ( ActiveTab.Equals(TAB.INVITE) )
			{
				navInvite.Attributes.Add("class",css_selected);
			}
            else if (ActiveTab.Equals (TAB.MANAGE_INVITES))
            {
                navManageInvites.Attributes.Add ("class", css_selected);
            }
		}

		/// <summary>
		/// The SubTab to display
		/// </summary>
		public TAB ActiveTab
		{
			get 
			{
				return m_ActiveTab;
			} 
			set
			{
				m_ActiveTab = value;
			}
		}

		/// <summary>
		/// this really should be stored in channelNav
		/// </summary>
		public enum TAB
		{
			GROUPS,
			REQUESTS,
			REQUESTS_SENT,
			INVITE,
            MANAGE_INVITES,
			NONE
		}

		
		protected TAB m_ActiveTab = TAB.GROUPS;

		protected HtmlAnchor			aGroups, aRequests, aReqSent, aInvite, aManageInvites;
		protected HtmlGenericControl	navList, navGroups, navRequests, navReqSent, navInvite, navManageInvites;

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
