<%@ Control Language="c#" AutoEventWireup="false" Codebehind="Uploads.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.usercontrols.Uploads" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ OutputCache Duration="120" VaryByParam="none" %>

<table cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td class="headertout" align="right"><strong>Kaneva Uploads:</strong> <asp:label id="lblVideoCount" runat="server"/> Videos / <asp:label id="lblMusicCount" runat="server"/> Music / <asp:label id="lblPhotoCount" runat="server"/> Photos / <asp:label id="lblGamesCount" runat="server"/> Games&nbsp;&nbsp;</td>
		<td><a href="~/mykaneva/upload.aspx" runat="server"><img runat="server" src="~/images/btn_uploadmedia.gif" alt="Upload Your Media" width="115" height="29" border="0"></a></td>
	</tr>
</table>