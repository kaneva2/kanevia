///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

namespace KlausEnt.KEP.Kaneva.usercontrols
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

	/// <summary>
	///		Summary description for KanevaToday.
	/// </summary>
	public class KanevaToday : BaseUserControl
	{

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Today at Kaneva
			lblNumPersonalChannels.Text = WebCache.GetTotalPersonalChannels().ToString ();
			lblNumBroadcastChannels.Text = WebCache.GetTotalBroadcastChannels().ToString ();
			lblNumMedia.Text = WebCache.GetTotalAssets().ToString ();
		}

		// Today at Kaneva
		protected Label	lblNumPersonalChannels;
		protected Label	lblNumBroadcastChannels;
		protected Label	lblNumMedia;

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
