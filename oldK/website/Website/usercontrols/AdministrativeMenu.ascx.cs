///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Kaneva.usercontrols
{
    public partial class AdministrativeMenu : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SetAdministrativeMenu(Request.IsAuthenticated);
        }

        private void SetAdministrativeMenu(bool bLoggedIn)
        {
            bool displayAdminMenu = false;

            if (bLoggedIn)
            {
                // Are they an admin?
                if (UsersUtility.IsUserAdministrator())
                {
                    navReports.InnerHtml = "<a href=\"" + ResolveUrl("~/Administration/reports/KPI.aspx") + "\"> Reports </a>";
                    navReports.Visible = true;

                    navCatalog.InnerHtml = "<a href=\"" + ResolveUrl("~/Administration/Catalog/CatalogAdministration.aspx") + "\"> Catalog </a>";
                    navCatalog.Visible = true;

                    navFame.InnerHtml = "<a href=\"" + ResolveUrl ("~/Administration/Fame/FameAdministration.aspx") + "\"> Fame </a>";
                    navFame.Visible = true;

                    navEmail.InnerHtml = "<a href=\"" + ResolveUrl("~/Administration/Tools/Email/Mailer/Mailer.aspx") + "\"> Email </a>";
                    navEmail.Visible = true;
                    
                    navSettings.InnerHtml = "<a href=\"" + ResolveUrl("~/Administration/settings/webOptionsManager.aspx") + "\"> Settings </a>";
                    navSettings.Visible = true;

                    displayAdminMenu = true;
                }

                // Are they a CSR?
                if (UsersUtility.IsUserCSR())
                {
                    navCSR.InnerHtml = "<a href=\"" + ResolveUrl("~/Administration/csr/userSearch.aspx") + "\"> Customer Service </a>";
                    navCSR.Visible = true;
                    displayAdminMenu = true;
                }
            }

            if (displayAdminMenu)
            {
                //lblUserName.Text = KanevaWebGlobals.CurrentUser.Username;
                liUserName.InnerHtml = "<a href=\"" + ResolveUrl("~/default.aspx") + "\" style=\"padding-right:20px; color:Black; font-size:16px; font-weight:bold\">" + KanevaWebGlobals.CurrentUser.Username + "</a>";
                tblAdminMenu.Visible = true;
            }
        }

    }
}