<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="TabbedControlLoader.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.usercontrols.TabbedControlLoader" %>
<script language="javascript" type="text/javascript" >
<!-- 

var orderBy=''; 
var filter='';
var previousControl = '';

var infoPage = '';
var pageParams = '';
var useFilter = 'false';
var useAlphaSort = 'false';
var useDateSort = 'false';

function HidePreviousControl()
{
   //first hide the previously viewed control
   if(previousControl != '')
   {
        $(previousControl).style.display = "none";
   }
}

function HideAllFilterControls()
{
   $('sortControl').style.display = "none";
   $('alphaSort').style.display = "none";
   $('dateSort').style.display = "none";
   $('divAjaxPager').style.display = "none";
}

function LoadTabDataAJAX(dataPage, parameters, useSorter, useAlphaSorter, useDateSorter)
{
    //reset the AJAX data return area
    $('divAjaxData').innerHTML = "<div class='loadingTabData'><img src='../images/ajax-loader.gif' /><br/><p>Loading...</p></div>";
   
    //set the page variables for the sorting controls to reuse later
    infoPage = dataPage;
    pageParams = parameters;
    useFilter = useSorter;
    useAlphaSort = useAlphaSorter;
    useDateSort = useDateSorter;				 

   //toggle div visibility for controls vs AJAX
   $('widget_body').style.display = "none";
   $('divAjaxData').style.display = "block";
   
   //configure sorting options
   if(useSorter.toLowerCase() == 'true')
   {
      $('sortControl').style.display = "block";
   }
   else
   {
      $('sortControl').style.display = "none";
   }
   if(useAlphaSorter.toLowerCase() == 'true')
   {
      $('alphaSort').style.display = "block";
   }
   else
   {
      $('alphaSort').style.display = "none";
   }
   if(useDateSorter.toLowerCase() == 'true')
   {
      $('dateSort').style.display = "block";
   }
   else
   {
      $('dateSort').style.display = "none";
   }
									  
   //get the data					  
    AjaxReqInit (parameters + '&orderby=' + orderBy + '&filter=' + filter, dataPage); 
}

function DisplayHiddenControl(controlName)
{
    //toggle div visibility for controls vs AJAX
   $('widget_body').style.display = "block";
   $('divAjaxData').style.display = "none";

   //first hide the previously viewed control
   HidePreviousControl();
   
   //show the selected control
   $(controlName).style.display = "block";
   $(controlName).style.height = "auto";		
   
   //set this as next control previous control
   previousControl = controlName;	  
   
   //hide any filters
   HideAllFilterControls();
}

function ToggleVisibility(me)
{
    if(me.style.display == "none")
    {
        me.style.display == "block";
    }
    else
    {
        me.style.display == "none";
    }
}

function SetDisplayOrder (order) 
{
	orderBy = order;
    filter=''; 
    LoadTabDataAJAX(infoPage,pageParams,useFilter,useAlphaSort,useDateSort); 
}

function SetFilter (val)
{						
    filter=val; 
    orderBy=''; 
    $('selSortBy').value='alpha'; 
    LoadTabDataAJAX(infoPage,pageParams,useFilter,useAlphaSort,useDateSort); 
}
var Tabs = new Array();	  //array of Tab objects
function Tab (name, parentNode, isSelected)
{	 
	this.name = name;
	this.parentNode = parentNode;
	this.parentNode.className = name;
	
	if (typeof(_tab_prototype_called) == 'undefined')
	{
		_tab_prototype_called = true;
		Tab.prototype.reset = reset;
		Tab.prototype.select = select;
	}
 
	function reset() {this.parentNode.className = this.name;}
	function select() {this.parentNode.className = 'selected ' + this.name;}	   
}

function SetSelectedTab(tabClicked)
{									
	if (Tabs.length == 0) {
		var tabs = $('tabContainer').getElementsByTagName('a');
		for(i=0; i < tabs.length; i++)
		{										
			Tabs[i] = new Tab (tabs[i].innerHTML.replace(' ','_'), tabs[i].parentNode, false);
		}   
	}
	
	for (i=0; i < Tabs.length; i++) {
		Tabs[i].reset();	 
		if (tabClicked.parentNode == Tabs[i].parentNode)
		{					  
			Tabs[i].select();
		}					   
	}
							 
    filter=orderBy='';
    $('selSortBy').value="alpha";
   }

//-->
</script>
    <div id="tabContainer">
        <asp:DataList ID="tabList" runat="server" RepeatColumns="10" RepeatDirection="Horizontal" RepeatLayout="Flow">
            <ItemTemplate>
                <div id="tab" runat="server">
                    <a class="" href="javascript:void(0);" id="tabButton" runat="server"></a>
                </div>
            </ItemTemplate>
        </asp:DataList>		
    </div>
    
    <div class="dateSort" id="dateSort" style="display:none">
		<a href="javascript:void(0);" id="all" onclick="SetFilter(this.id);">All</a>|<a href="javascript:void(0);" id="upcoming" onclick="SetFilter(this.id);">Upcoming</a>|<a href="javascript:void(0);" id="past" onclick="SetFilter(this.id);">Past</a>
    </div>
    
	<div id="sortControl" style="display:none">
	    Sort by: 
	    <select id="selSortBy" onchange="SetDisplayOrder(this.value);">
		    <option value="alpha">Alphabetical</option>
		    <option value="raves">Most Popular</option>
		    <!--option value="views">Most Views</option-->
		    <option value="date">Newest</option>
	    </select>
	</div>	
	
	<div id="alphaSort" style="display:none">
		<a href="javascript:void(0);" id="all_results" onclick="SetFilter('');" style="width:20px">All</a>|<a href="javascript:void(0);" id="A" onclick="SetFilter(this.id);">A</a>|<a href="javascript:void(0);" id="B" onclick="SetFilter(this.id);">B</a>|<a href="javascript:void(0);" id="C" onclick="SetFilter(this.id);">C</a>|<a href="javascript:void(0);" id="D" onclick="SetFilter(this.id);">D</a>|<a href="javascript:void(0);" id="E" onclick="SetFilter(this.id);">E</a>|<a href="javascript:void(0);" id="F" onclick="SetFilter(this.id);">F</a>|<a href="javascript:void(0);" id="G" onclick="SetFilter(this.id);">G</a>|<a href="javascript:void(0);" id="H" onclick="SetFilter(this.id);">H</a>|<a href="javascript:void(0);" id="I" onclick="SetFilter(this.id);">I</a>|<a href="javascript:void(0);" id="J" onclick="SetFilter(this.id);">J</a>|<a href="javascript:void(0);" id="K" onclick="SetFilter(this.id);">K</a>|<a href="javascript:void(0);" id="L" onclick="SetFilter(this.id);">L</a>|<a href="javascript:void(0);" id="M" onclick="SetFilter(this.id);">M</a>|<a href="javascript:void(0);" id="N" onclick="SetFilter(this.id);">N</a>|<a href="javascript:void(0);" id="O" onclick="SetFilter(this.id);">O</a>|<a href="javascript:void(0);" id="P" onclick="SetFilter(this.id);">P</a>|<a href="javascript:void(0);" id="Q" onclick="SetFilter(this.id);">Q</a>|<a href="javascript:void(0);" id="R" onclick="SetFilter(this.id);">R</a>|<a href="javascript:void(0);" id="S" onclick="SetFilter(this.id);">S</a>|<a href="javascript:void(0);" id="T" onclick="SetFilter(this.id);">T</a>|<a href="javascript:void(0);" id="U" onclick="SetFilter(this.id);">U</a>|<a href="javascript:void(0);" id="V" onclick="SetFilter(this.id);">V</a>|<a href="javascript:void(0);" id="W" onclick="SetFilter(this.id);">W</a>|<a href="javascript:void(0);" id="X" onclick="SetFilter(this.id);">X</a>|<a href="javascript:void(0);" id="Y" onclick="SetFilter(this.id);">Y</a>|<a href="javascript:void(0);" id="Z" onclick="SetFilter(this.id);">Z</a>
	</div>
	
	<div class="clear"><!-- clear the floats --></div>
	
    <div id="widget_body" style="display:none">
        <asp:PlaceHolder runat="server" id="phWidgetShell"></asp:PlaceHolder>
    </div>

    <div id="divAjaxData" style="display:none"><div class="loadingTabData"><br /><p>Loading...</p></div></div> 

    <div class="modulePaging">
		<div id="divAjaxPager"><!-- Passive blast pager --></div>  
	</div>

 
<script type="text/javascript">
	if ($('wrapper_about')) {
		$('wrapper_about').style.display = "block";
		$('wrapper_about').style.overflow = "hidden";
		$('wrapper_about').style.height = "0";
	}
</script>
