///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System.Web.UI;
using KlausEnt.KEP.Kaneva.channel;

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	using System.Web.Security;

	/// <summary>
	///	Summary description for Header.
	/// </summary>
	public class Header : Kaneva.usercontrols.BaseUserControl
	{
		/// <summary>
		/// Page_Load
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Page_Load (object sender, System.EventArgs e)
		{
            litJS.Text = "<script type=\"text/javascript\" src=\"" + ResolveUrl ("~/jscript/jquery/jquery-1.8.2.min.js") + "\"></script>" +
                         "<script type=\"text/javascript\" src=\"" + ResolveUrl ("~/jscript/jquery/jquery.watermark.min.js") + "\"></script>" +
                         "<script type=\"text/javascript\" src=\"" + ResolveUrl("~/jscript/notifications/jquery.notifyBar.min.js") + "\"></script>";

            if (Request.IsAuthenticated)
            {
                spnAuthenticated.Visible = true;

                SetTertiaryButtons (Request.IsAuthenticated);

                litJS.Text += "<script type=\"text/javascript\">var nopluginUrl = 'http://" + KanevaGlobals.SiteName + "/community/install3d.kaneva';</script>";

                if (Request.IsAuthenticated)
                {
                    GetUserFortune ();
                }

                // Grab single sign-on session variables used to display the sso Flash content.
                string singleSignOnToken = GetUserFacade.GetSingleSignOnToken(KanevaWebGlobals.CurrentUser.UserId);
                if (!string.IsNullOrWhiteSpace(singleSignOnToken))
                {
                    string fSSOVars = "<param name=\"FlashVars\" value=\"" + singleSignOnToken + "\" />";

                    divFlashSSO.Visible = true;
                    litFlashSSOVars.Text = fSSOVars;
                    litFlashSSOVarsIE.Text = fSSOVars;

                    litJS.Text += "<script type=\"text/javascript\">var flashVarsToken='" + singleSignOnToken + "';</script>";
                }

                litJS.Text += "<script type=\"text/javascript\" src=\"" + ResolveUrl("~/jscript/PatcherDetect.js?v5") + "\"></script>";
            }
            else  // User not logged in
            {
                spnNotAuthenticated.Visible = true;

                SetPagePadding (false, false);
		
                litJS.Text += "<script type=\"text/javascript\" src=\"" + ResolveUrl ("~/jscript/landing_page/base.js") + "\"></script>";
            }

            litCSS.Text += "<link href=\"" + ResolveUrl ("~/css/topnav.css") + "?v4\" rel=\"stylesheet\" type=\"text/css\">" +
                           "<link href=\"" + ResolveUrl("~/jscript/notifications/css/jquery.notifyBar.min.css") + "\" rel=\"stylesheet\" type=\"text/css\">";
        }

		
		#region Helper Methods

		private void SetTertiaryButtons(bool bLoggedIn)
		{
			if (bLoggedIn)
			{
                // Display new message count
                int numberNewMessages = KanevaWebGlobals.CurrentUser.Stats.NumberOfNewMessages;
                divMsgCount.InnerText = numberNewMessages > 99 ? "99" : numberNewMessages.ToString();
                if (numberNewMessages == 0) { divMsgCount.Visible = false; }

                // Display new world requests count
                int numberNewWorldReq = KanevaWebGlobals.CurrentUser.Stats.Number3DAppRequests;
                divWorldReqCount.InnerText = numberNewWorldReq > 99 ? "99" : numberNewWorldReq.ToString ();
                if (numberNewWorldReq == 0) { divWorldReqCount.Visible = false; }


                spnUsername.InnerText = KanevaWebGlobals.CurrentUser.Username;
                string validateMsg = "";
                validateContainer.Visible = false;

                if (System.IO.Path.GetFileNameWithoutExtension (Request.Url.AbsolutePath).ToLower () == "default")
                {
                    bool isEmailValidated = true;

                    if (KanevaWebGlobals.CurrentUser.StatusId.Equals ((int) Constants.eUSER_STATUS.REGNOTVALIDATED) &&
                        KanevaWebGlobals.CurrentUser.FacebookSettings.FacebookUserId == 0)
                    {
                        // Checking fame to see if they've been rewarded for validating their email.
                        // Users will only be rewarded the first time they validate.
                        FameFacade fameFacade = new FameFacade ();
                        if (fameFacade.IsOneTimePacketRedeemed (KanevaWebGlobals.CurrentUser.UserId, (int) PacketId.WEB_OT_EMAIL_VALIATION))
                        {
                            validateMsg = "Click here to verify your contact email address.";
                        }
                        else
                        {
                            validateMsg = "Confirm your email address and earn 1000 Rewards. An email was sent to <span>" + KanevaWebGlobals.CurrentUser.Email + "</span>.";
                        }

                        isEmailValidated = false;
                    }

                    SetPagePadding (true, !isEmailValidated);

                    // If we've marked them as a bounce show an alert.
                    if (KanevaWebGlobals.CurrentUser.EmailStatus > 0 && !KanevaWebGlobals.CurrentUser.StatusId.Equals ((int) Constants.eUSER_STATUS.REGNOTVALIDATED))
                    {
                        validateMsg = "We're unable to reach you! Please update your contact email address.";
                    }

                    if (validateMsg != "")
                    {
                        msg.InnerHtml = validateMsg;
                        validateContainer.Visible = true;
                    }
                }
                else
                {
                    SetPagePadding (true, false);
                }
			}
			else
			{
                lnkLogout.Visible = false;
	        }

            // Shop and Buy Buttons
            aShop.HRef = "http://" + KanevaGlobals.ShoppingSiteName;
            aMake3DApp.HRef = ResolveUrl("~/community/StartWorld.aspx");

            aShopDD.HRef = "http://" + KanevaGlobals.ShoppingSiteName;
        }

        private void SetPagePadding (bool isLoggedIn, bool emailNotValidated)
        {
            int pxCount = 75;

            if (isLoggedIn)
            {
                string pageName = GetCurrentPageName ().ToLower ();

                if (pageName == "general.aspx" || pageName == "settings.aspx" || pageName == "dashboard;aspx" ||
                    pageName == "billinginfosummary.aspx" || pageName == "transactions.aspx" || pageName == "inventory.aspx")
                {
                    pxCount = 90;
                }

                if (emailNotValidated)
                {
                    pxCount += 78;
                }
            }

            litStyle.Text = ".PageContainer {padding-top:" + pxCount + "px;} ";
            litStyle.Text += ".newcontainerborder {padding-top:" + pxCount + "px;}";
        }

        public string GetCurrentPageName ()
        {
            string spath = System.Web.HttpContext.Current.Request.Url.AbsolutePath;
            System.IO.FileInfo oInfo = new System.IO.FileInfo (spath);
            return oInfo.Name;
        }
 
        public void SetClass(string currTab)
		{
			string strTab = ActiveTab.ToString() ;
            if (strTab == currTab)
            {
                switch (ActiveTab)
                {
                    default:
                        return;
                }
            }
		}

		public void SetNavVisible(UserControl nav)
		{
			SetNavVisible(nav, 2);
		}
		public void SetNavVisible(UserControl nav, int lvl)				
		{
            if (nav != null)
            {
                if (nav.AppRelativeVirtualPath == "~/usercontrols/NavAccount.ascx")
                {
                    if (lvl != 3)
                    {
                        phNavLvl3.Visible = false;
                    }
                    else
                    {
                        phNavLvl3.Controls.Add (nav);
                        phNavLvl3.Visible = true;
                    }
                }
            }
        }

        public void GetUserFortune ()
        {
            // Get user point balances
            UserBalances ub = new UserFacade ().GetUserBalancesCached (KanevaWebGlobals.CurrentUser.UserId);

            spnCreditsCount_Header.InnerHtml = ub.Credits == 0 ? "0" : ub.Credits.ToString ("#,###");
            spnRewardsCount_Header.InnerHtml = ub.Rewards == 0 ? "0" : ub.Rewards.ToString ("#,###");
        }

		#endregion Helper Methods


		#region Event Handlers

        /// <summary>
		/// Log out of the site
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void lnkLogout_Click (object sender, EventArgs e) 
		{
			System.Web.HttpCookie aCookie;
            string bannerCookieName = KanevaWebGlobals.GetBannerCookieName (); 
            int limit = Request.Cookies.Count - 1;
            for (int i = limit; i != -1; i--)
			{
				aCookie = Request.Cookies [i];
                if (aCookie.Name != bannerCookieName)
                {
                    aCookie.Expires = DateTime.Now.AddDays (-1);
                    Response.Cookies.Add (aCookie);
                }
			}
			
			SetTertiaryButtons (false);

            // Logout of single sign-on
            GetUserFacade.CleanupSingleSignOn(KanevaWebGlobals.CurrentUser.UserId);

			FormsAuthentication.SignOut ();
			Session.Abandon ();

            Response.Redirect ("~/default.aspx");

			// Get rid of warning message via this script code
	/*		string strJavascript = "<script language=\"JavaScript\">" +
				"window.location = (\"http://" + KanevaGlobals.SiteName + "/loginHelper.aspx?action=logout\");" +
				"</script>";

			if (!Page.ClientScript.IsClientScriptBlockRegistered (GetType (), "logout"))
			{
                Page.ClientScript.RegisterClientScriptBlock(GetType(), "logout", strJavascript);	
			}	
     */
		}

        protected void btnResend_Click (object sender, EventArgs e)
        {
            // If the user has already received the one time registration award send them the email change template.        
            FameFacade fameFacade = new FameFacade ();
            if (fameFacade.IsOneTimePacketRedeemed (KanevaWebGlobals.CurrentUser.UserId, (int) PacketId.WEB_OT_EMAIL_VALIATION))
            {
                MailUtilityWeb.SendEmailChange (KanevaWebGlobals.CurrentUser.Username, KanevaWebGlobals.CurrentUser.Email, KanevaWebGlobals.CurrentUser.KeyValue, "general");
            }
            else
            {
                MailUtilityWeb.SendVerificationEmail (KanevaWebGlobals.CurrentUser.Username, KanevaWebGlobals.CurrentUser.Email, KanevaWebGlobals.CurrentUser.KeyValue, "general");
            }

            spnResend.InnerHtml = "Sent!";
            spnResend.Attributes.Add ("class", "sent");
        }

        #endregion Event Handlers


        #region Properties

        public enum TAB
        {
            _UPDATE_,
            HOME,
            CONNECT,
            PLAY,
            CREATE,
            SIGN_IN,
            JOIN,
            SEARCH,
            HELP,
            NONE
        }

        /// <summary>
        /// The SubTab to display
        /// </summary>
        public TAB ActiveTab
        {
            get
            {
                return ViewState["HeaderActiveTab"] == null ?
                    TAB.HOME : (TAB) ViewState["HeaderActiveTab"];
            }
            set
            {
                ViewState["HeaderActiveTab"] = value;
            }
        }

        public NavAccount MyAccountNav
        {
            get
            {
                if (m_nav_account == null)
                {
                    m_nav_account = (NavAccount) LoadControl ("~/usercontrols/NavAccount.ascx");
                }

                return m_nav_account;
            }
        }
        public NavFriend MyFriendNav
        {
            get
            {
                if (m_nav_friend == null)
                {
                    m_nav_friend = (NavFriend) LoadControl ("~/usercontrols/NavFriend.ascx");
                }

                return m_nav_friend;
            }
        }
        public NavMediaUploads MyMediaUploadsNav
        {
            get
            {
                if (m_nav_media_uploads == null)
                {
                    m_nav_media_uploads = (NavMediaUploads) LoadControl ("~/usercontrols/NavMediaUploads.ascx");
                }

                return m_nav_media_uploads;
            }
        }
        public NavMessages MyMessagesNav
        {
            get
            {
                if (m_nav_messages == null)
                {
                    m_nav_messages = (NavMessages) LoadControl ("~/usercontrols/NavMessages.ascx");
                }

                return m_nav_messages;
            }
        }
        public NavMyChannels MyChannelsNav
        {
            get
            {
                if (m_nav_mychannels == null)
                {
                    m_nav_mychannels = (NavMyChannels) LoadControl ("~/usercontrols/NavMyChannels.ascx");
                }

                return m_nav_mychannels;
            }
        }
        public NavMyKaneva MyKanevaNav
        {
            get
            {
                if (m_nav_mykaneva == null)
                {
                    m_nav_mykaneva = (NavMyKaneva) LoadControl ("~/usercontrols/NavMyKaneva.ascx");
                }

                return m_nav_mykaneva;
            }
        }
        public string FlashSSOPath
        {
            get { return ResolveUrl("~/flash/kaneva_sinfo.swf"); }
        }

        #endregion Properties
		

		#region Declarations

		protected KlausEnt.KEP.Kaneva.NavAccount		m_nav_account;
		protected KlausEnt.KEP.Kaneva.NavFriend			m_nav_friend;
		protected KlausEnt.KEP.Kaneva.NavMediaUploads	m_nav_media_uploads;
		protected KlausEnt.KEP.Kaneva.NavMessages		m_nav_messages;
		protected KlausEnt.KEP.Kaneva.NavMyChannels		m_nav_mychannels;
		protected KlausEnt.KEP.Kaneva.NavMyKaneva		m_nav_mykaneva;

        protected Label lblUserName;

        protected LinkButton lnkLogout;
		protected PlaceHolder phNavLvl3;

        protected HtmlContainerControl spnMail, spnCreditsCount_Header, spnRewardsCount_Header, spnAuthenticated, spnNotAuthenticated, msg, profiledd;
        protected HtmlContainerControl divMsgCount, divWorldReqCount, divFlashSSO, validateContainer, spnUsername, spnResend;
        protected HtmlContainerControl liPeople;

        protected HtmlAnchor aMake3DApp, aShop, aShopDD;

        protected Literal litCSS, litJS, litXtraJS, litRaveVars, litStyle, litFlashSSOVars, litFlashSSOVarsIE, litAdminLinks;

		#endregion Declerations


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
            InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}

}
