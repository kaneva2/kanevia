///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

namespace KlausEnt.KEP.Kaneva.usercontrols
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

	/// <summary>
	///		Summary description for AccountWizardNavBar.
	/// </summary>
	public class AccountWizardNavBar : BaseUserControl
	{
		private void Page_Load(object sender, System.EventArgs e)
		{
			if (!IsPostBack)
			{
				BindSteps();
			}
		}

		#region Helper Methods
		public void BindSteps()
		{
			spnOne.Attributes.Add("class", ClassDisable);
			spnTwo.Attributes.Add("class", ClassDisable);
			spnThree.Attributes.Add("class", ClassDisable);
			spnFour.Attributes.Add("class", ClassDisable);
			spnFive.Attributes.Add("class", ClassDisable);

			// Set selected tab
			switch (ActiveStep)
			{
				case STEP.STEP_1:
					spnOne.Attributes.Add("class", ClassActive);
					break;

				case STEP.STEP_2:
					spnOne.Attributes.Add("class", ClassComplete);
					spnTwo.Attributes.Add("class", ClassActive);
					break;

				case STEP.STEP_3:
					spnOne.Attributes.Add("class", ClassComplete);
					spnTwo.Attributes.Add("class", ClassComplete);
					spnThree.Attributes.Add("class", ClassActive);
					break;

				case STEP.STEP_4:
					spnOne.Attributes.Add("class", ClassComplete);
					spnTwo.Attributes.Add("class", ClassComplete);
					spnThree.Attributes.Add("class", ClassComplete);
					spnFour.Attributes.Add("class", ClassActive);
					break;

				case STEP.STEP_5:
					spnOne.Attributes.Add("class", ClassComplete);
					spnTwo.Attributes.Add("class", ClassComplete);
					spnThree.Attributes.Add("class", ClassComplete);
					spnFour.Attributes.Add("class", ClassComplete);
					spnFive.Attributes.Add("class", ClassActive);
					break;
			}
		}
		#endregion

		#region Properties

		/// <summary>
		/// The current step 
		/// </summary>
		public static STEP ActiveStep
		{
			get 
			{
				return m_ActiveStep;
			} 
			set
			{
				m_ActiveStep = value;
			}
		}
			
		public string ClassDisable
		{
			get 
			{
				return m_class_disable;
			} 
			set
			{
				m_class_disable = value;
			}
		}
			
		public string ClassActive
		{
			get 
			{
				return m_class_active;
			} 
			set
			{
				m_class_active = value;
			}
		}
			
		public string ClassComplete
		{
			get 
			{
				return m_class_complete;
			} 
			set
			{
				m_class_complete = value;
			}
		}
			

		#endregion
		
		#region Declerations

		private string m_class_disable	= "incomplete";
		private string m_class_active	= "active";
		private string m_class_complete = "completed";

		protected static STEP m_ActiveStep = STEP.NONE;

		protected HtmlContainerControl	spnOne, spnTwo, spnThree, spnFour, spnFive;

		public enum STEP
		{
			STEP_1,
			STEP_2,
			STEP_3,
			STEP_4,
			STEP_5,
			NONE
		}
		
		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
