///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace KlausEnt.KEP.Kaneva.usercontrols
{
    public partial class DateTimeControl : System.Web.UI.UserControl
    {

        protected void Page_Load(object sender, EventArgs e)
        {
             WireJavaScript();
           if (!Page.IsPostBack)
            {
                ConfigurePage();
            }
        }

        #region Setup Functions
        private void WireJavaScript()
        {
            string clientId = this.ClientID;
            imgSelectDate.Attributes.Add("onload", "initCalendar" + clientId + "();");

            //wire javascript for the promotional start date calendar

            string str_handleDate = "<script type=\"text/javascript\" src=\"../jscript/yahoo/yahoo-min.js\"></script> \n"; 
            str_handleDate += "<script type=\"text/javascript\" src=\"../jscript/yahoo/event-min.js\" ></script>  \n";   
            str_handleDate += "<script type=\"text/javascript\" src=\"../jscript/yahoo/dom-min.js\" ></script>  \n";   
            str_handleDate += "<script type=\"text/javascript\" src=\"../jscript/yahoo/calendar-min.js\"></script> \n"; 
            str_handleDate += "<link type=\"text/css\" rel=\"stylesheet\" href=\"../css/yahoo/calendar.css\"> \n\n\n";
            str_handleDate += "<script type=\"text/javascript\" > \n <!-- \n YAHOO.namespace(\"example.calendar\"); \n";
            str_handleDate += "function handleDate" + clientId + "(type,args,obj)\n{ \n";
            str_handleDate += "var dates = args[0]; \n var date = dates[0]; \n var year = date[0], month = date[1], day = date[2]; \n ";
            str_handleDate += "var txtDate1 = document.getElementById(\"" + txtDate.ClientID + "\"); \n ";
            str_handleDate += "txtDate1.value = month + \"-\" + day + \"-\" + year; \n ";
            str_handleDate += "YAHOO.example.calendar.cal" + clientId + ".hide(); \n } \n \n";
            str_handleDate += "function initCalendar" + clientId + "() \n { \n // Admin Calendar \n ";
            str_handleDate += "YAHOO.example.calendar.cal" + clientId + " = new YAHOO.widget.Calendar (\"cal" + clientId + "\", \"" + this.cal1Container.ClientID + "\", { iframe:true, zIndex:200, mindate:\"1/1/2007\", title:\"Choose a date:\", close:true } ); \n ";
            str_handleDate += "YAHOO.example.calendar.cal" + clientId + ".render(); \n ";
            str_handleDate += "// Listener to show Admin Calendar when the button is clicked \n ";
            str_handleDate += "YAHOO.util.Event.addListener(\"" + this.imgSelectDate.ClientID + "\", \"click\", YAHOO.example.calendar.cal" + clientId + ".show, YAHOO.example.calendar.cal" + clientId + ", true); \n ";
            str_handleDate += "YAHOO.example.calendar.cal" + clientId + ".selectEvent.subscribe(handleDate" + clientId + ", YAHOO.example.calendar.cal" + clientId + ", true); \n } \n ";
            str_handleDate += "//--> \n </script> \n\n";

            if (!Page.ClientScript.IsClientScriptBlockRegistered(GetType(), "dateHandler" + clientId))
            {
                Page.ClientScript.RegisterClientScriptBlock(GetType(), "dateHandler" + clientId, str_handleDate);
            }

        }

        private void ConfigurePage()
        {
            drpTime.Visible = TimeFieldVisible;
            drpTime.SelectedValue = TimeFieldValue;
            if (DateFieldValue != null)
            {
                txtDate.Text = DateFieldValue ;
            }
            txtDate.Visible = imgSelectDate.Visible = DateFieldVisible;
            lbl_date.InnerText = SetDateLabel;
        }

        #endregion

        #region Event Handlers 

        protected void txtDate_TextChanged(object sender, EventArgs e)
        {
            DateFieldValue = txtDate.Text;
        }

        protected void drpTime_SelectedIndexChanged(object sender, EventArgs e)
        {
            TimeFieldValue = drpTime.SelectedValue;
        }

        #endregion

        #region Accessor Functions

        public bool TimeFieldVisible
        {
            get
            {
                if (ViewState["_timeVisible"] == null)
                {
                    ViewState["_timeVisible"] = true;
                }
                return (bool)ViewState["_timeVisible"];
            }
            set
            {
                ViewState["_timeVisible"] = value;
            }
        }

        public string TimeFieldValue
        {
            get
            {
                if (ViewState["_timeValue"] == null)
                {
                    ViewState["_timeValue"] = "08:00 AM"; ;
                }
                return ViewState["_timeValue"].ToString();
            }
            set
            {
                ViewState["_timeValue"] = value;
            }
        }

        public string DateFieldValue
        {
            get
            {
                if (ViewState["_dateValue"] == null)
                {
                    ViewState["_dateValue"] = "";
                }
                return ViewState["_dateValue"].ToString();
            }
            set
            {
                ViewState["_dateValue"] = value;
            }
        }

        public bool DateFieldVisible
        {
            get
            {
                if (ViewState["_dateVisible"] == null)
                {
                    ViewState["_dateVisible"] = true;
                }
                return (bool)ViewState["_dateVisible"];
            }
            set
            {
                ViewState["_dateVisible"] = value;
            }
        }

        public string SetDateLabel
        {
            get
            {
                if (ViewState["_dateLabelText"] == null)
                {
                    ViewState["_dateLabelText"] = "Start Date";
                }
                return ViewState["_dateLabelText"].ToString();
            }
            set
            {
                ViewState["_dateLabelText"] = value;
            }
        }

        #endregion

        #region Web Form Designer generated code

        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            this.drpTime.SelectedIndexChanged += new System.EventHandler(this.drpTime_SelectedIndexChanged);
            this.txtDate.TextChanged += new System.EventHandler(this.txtDate_TextChanged);
        }
        #endregion
    }
}