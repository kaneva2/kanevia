///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Specialized;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KlausEnt.KEP.Kaneva.mykaneva.widgets;
using Kaneva.BusinessLayer.BusinessObjects;
using System.Collections.Generic;

namespace KlausEnt.KEP.Kaneva.usercontrols
{
    public partial class TabbedControlLoader : BaseUserControl
    {
        #region Declarations
        private bool hideEmptyTabs = false;
        private TabDisplay defaultTab = new TabDisplay();
        private IList<TabDisplay> tabValues = new List<TabDisplay>();
        #endregion

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
//            if (Request.IsAuthenticated)
//            {
                //populate the tabs
                PopulateTabs();
//            }
//            else
//            {
//            }
        }
        #endregion

        #region Functions

        /// <summary>
        /// PopulateTabs - gathers and binds tabs to control
        /// 
        /// </summary>
        private void PopulateTabs()
        {
            //pull the tab values from the attributes and set the values
            tabList.DataSource = TabValues;

            //Bind tabs to control
            tabList.DataBind();
        }

        /// <summary>
        /// PopulateTabs - gathers and binds tabs to control
        /// 
        /// </summary>
        private void SetDefaultTab(TabDisplay currentTab, string javaScript, HtmlGenericControl tab)
        {
            //pull the tab values from the attributes and set the values
            if(currentTab.TabName == DefaultTab.TabName)
            {
                //sets the tab class to indicate it is selected
                tab.Attributes["class"] = "selected " + currentTab.TabName;
                Page.ClientScript.RegisterStartupScript(GetType(), "defaultTabData", "<script type=\"text/javascript\" >" + javaScript + "</script>");
            }
        }

        /// <summary>
        /// AddControl - loads the indicated control in to the pages control hierarchy and returns the client id
        /// </summary>
        public string AddControl(string controlToLoad, string name)
        {
            ModuleViewBaseControl userControl = (ModuleViewBaseControl)Page.LoadControl(controlToLoad);

            // Place web user control to place holder control
            HtmlGenericControl wrapper = new HtmlGenericControl();
            wrapper.Controls.Add(userControl);
            wrapper.Style.Add("display", "none");
            phWidgetShell.Controls.Add(wrapper);

            wrapper.ID = "wrapper_" + name.ToLower();
            wrapper.ClientIDMode = System.Web.UI.ClientIDMode.Static;

            return wrapper.ClientID;
        }
        
        #endregion

        #region Attributes

        /// <summary>
        /// Hide Empty Tabs - attribute for getting and setting if tabs should be hidden for counts less than 1
        /// </summary>
        public bool HideEmptyTabs
        {
            get
            {
                return this.hideEmptyTabs;
            }
            set
            {
                this.hideEmptyTabs = value;
            }
        }

        /// <summary>
        /// DefaultTab - sets the tab that you want as the default
        /// </summary>
        public TabDisplay DefaultTab
        {
            get
            {
                return this.defaultTab;
            }
            set
            {
                this.defaultTab = value;
            }
        }

        /// <summary>
        /// Tabe Values - a pairing of display values for the tabs and the control to load
        /// </summary>
        public IList<TabDisplay> TabValues
        {
            get
            {
                return this.tabValues;
            }
            set
            {
                this.tabValues = value;
            }
        }

        #endregion

        #region Event Handlers

        private void tabList_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //find all the controls in the data List
                HtmlAnchor tabLink = (HtmlAnchor)e.Item.FindControl("tabButton");
                HtmlGenericControl tab = (HtmlGenericControl)e.Item.FindControl("tab");

                //set the tab text                                         
                tabLink.InnerText = ((TabDisplay)e.Item.DataItem).TabName;
                tabLink.ID = "tab" + ((TabDisplay) e.Item.DataItem).TabName;
                tabLink.ClientIDMode = System.Web.UI.ClientIDMode.Static;
                
                tab.Attributes.Add ("class", ((TabDisplay) e.Item.DataItem).TabName.Replace(" ","_"));
 
                //get the values
                string ajaxDataPage = ResolveUrl(((TabDisplay)e.Item.DataItem).AjaxDataPage);
                string ajaxDataParams = ((TabDisplay)e.Item.DataItem).AjaxDataPageParams;
                string controlToLoad = ResolveUrl(((TabDisplay)e.Item.DataItem).ControlToLoad);
                bool useSorter = ((TabDisplay)e.Item.DataItem).AllowSorting;
                bool useAlphaSorter = ((TabDisplay)e.Item.DataItem).AllowAlphaSorting;
				bool useDateSorter = ((TabDisplay)e.Item.DataItem).AllowDateSorting;

                string function = "";

                //set the onclick function according to whether or not this is an AJAX control or a regular one
                if (((ajaxDataPage == null) || (ajaxDataPage == "")) && (controlToLoad != null) && (controlToLoad != ""))
                {
                    function = "DisplayHiddenControl('" + AddControl(controlToLoad, ((TabDisplay) e.Item.DataItem).TabName) + "');";
                }
                else if (((ajaxDataPage != null) || (ajaxDataPage != "")))
                {
					function = "try{LoadTabDataAJAX('" + ajaxDataPage + "','" + ajaxDataParams + "','" + useSorter + "','" + useAlphaSorter + "','" + useDateSorter + "');}catch (err){}";
                }

                //set the onclick for the tab
                tabLink.Attributes.Add("onclick", "SetSelectedTab(this);" + function);

                //set the default tab
                SetDefaultTab(((TabDisplay)e.Item.DataItem), function, tab);

                if (((TabDisplay) e.Item.DataItem).TabId == (int)Constants.eTab.About &&
                    ((TabDisplay) e.Item.DataItem).TabName == DefaultTab.TabName)
                {

                }
            }
        }

        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            this.tabList.ItemDataBound += new System.Web.UI.WebControls.DataListItemEventHandler(this.tabList_ItemDataBound);
        }
        #endregion

    }
}