<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Footer2.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.Footer2" %>


<div class="footer" id="footerNotAuth" runat="server">
	<ul>
		<li><a href="http://blog.kaneva.com/?page_id=2">About</a></li>
		<li>|</li>
		<li><a id="A1" href="~/community/install3d.kaneva" runat="server">Download</a></li>
		<li>|</li>
		<li><a id="A14" href="~/community/safety.kaneva" runat="server">Safety</a></li>
		<li>|</li>
		<li><a id="A27" href="~/community/Careers.kaneva" runat="server">Careers</a></li>
		<li>|</li>
		<li><a href="http://developer.kaneva.com" >Developers</a></li>
		<li>|</li>
		<li><a href="~/overview/TermsAndConditions.aspx" runat="server">Terms</a></li>
		<li>|</li>
		<li><a href="~/overview/privacy.aspx" runat="server">Policy</a></li>
		<li>|</li>
		<li><a href="http://support.kaneva.com/ics/support/default.asp?deptID=5433" id="aHelp" runat="server" target="_blank">Help</a></li>
	</ul>
	� <%= DateTime.Now.ToString("yyyy") %> Kaneva<br />
	<span style="visibility:hidden;"><asp:Label id="lblVersion" runat="server"/><asp:Label id="lblLoadTime" runat="server"/></span>
</div>  

<div class="footer" id="footerNotAuth_Original" runat="server">
  <ul>
    <li><a href="./">Home</a></li>
    <li>|</li>
    <li><a id="A15" href="~/community/install3d.kaneva" runat="server">Install 3D Kaneva</a></li>
    <li>|</li>
    <li><a id="A28" href="~/overview/guidelines.aspx" runat="server">Member Guidelines</a></li>
    <li>|</li>
    <li><a id="A29" href="~/community/safety.kaneva" runat="server">Parental Resources</a></li>
    <li>|</li>
    <li><a id="A30" href="~/community/Careers.kaneva" runat="server">Careers</a></li>
    <li>|</li>
    <li><a id="A31" href="~/sitemap.aspx" runat="server">Site Map</a></li>
  </ul>
  <ul>
    <li><a href="http://support.kaneva.com/ics/support/default.asp?deptID=5433" id="aHelpNonAuth" runat="server" target="_blank">Support Center</a></li>
    <li>|</li>
    <li><a href="http://forums.kaneva.com/">Member Forums</a></li>
    <li>|</li>
    <li><a href="http://ideas.kaneva.com"">Kaneva Ideas</a></li>
    <li>|</li>
    <li><a id="A33" href="~/share/default.aspx" runat="server">Share Kaneva</a></li>
    <li>|</li>
    <li><a id="A34" href="~/community/news.kaneva" runat="server">Press Center</a></li>
    <li>|</li>
    <li><a href="http://developer.kaneva.com" >Developers</a></li>
  </ul>
  � 2012 Kaneva
</div>  


<div class="footerAuth" id="footerAuth" runat="server">    
	<hr />
	<div class="footer_container">
	
	<div class="footerSection">
		<div class="footerSecTitle">You</div>
		<div><a id="A2" runat="server" href="~/mykaneva/buycredits.aspx">Buy Credits</a></div>
		<div><a id="A3" runat="server" href="http://shop.kaneva.com/MySales.aspx">My Store</a></div>
		<div><a id="A4" runat="server" href="~/myKaneva/transactions.aspx">My Transactions</a></div>
		<div><a id="A5" runat="server" href="~/myKaneva/inventory.aspx">My Inventory</a></div>
		<div><a id="A6" runat="server" href="~/mykaneva/upload.aspx">Upload Media</a></div>
		<div><a id="A7" runat="server" href="~/3d-virtual-world/world-of-kaneva-fame.kaneva">Earn Fame</a></div>
	</div>

	<div class="footerSection">
		<div class="footerSecTitle">Explore</div>
		<div><a id="A8" runat="server" href="http://shop.kaneva.com/">Shop</a></div>
		<div><a id="A9" runat="server" href="~/watch/watch.kaneva?type=2">Videos</a></div>
		<div><a id="A10" runat="server" href="~/watch/watch.kaneva?type=5">Photos</a></div>
		<div><a id="A11" runat="server" href="~/watch/watch.kaneva?type=1">Games</a></div>
		<div><a id="A35" runat="server" href="~/community/channel.kaneva?3dapps=">Worlds</a></div>
	</div>

	<div class="footerSection">
		<div class="footerSecTitle">Help</div>
		<div><a id="A13" runat="server" href="http://forums.kaneva.com/">Forums</a></div>
		<div><a id="aHelpAuth" runat="server" href="http://support.kaneva.com/ics/support/default.asp?deptID=5433">Support</a></div>
		<div><a id="A16" runat="server" href="~/share/default.aspx">Spread the Word</a></div>
		<div><a id="A17" runat="server" href="~/sitemap.aspx">Site Map</a></div>
		<div><a id="A18" runat="server" href="http://blog.kaneva.com/?page_id=10">Contact Us</a></div>
	</div>

	<div class="footerSection last">
		<div class="footerSecTitle">Do More</div>
		<div><a id="A19" runat="server" href="~/community/install3d.kaneva">Download 3D Kaneva</a></div>
		<div><a id="A20" runat="server" href="http://developer.kaneva.com">Developers</a></div>
	</div>

	<div class="footerSectionHorizontal">
		<span class="footerSecTitle">Learn About Us:</span>
		<div><a id="A21" runat="server" href="http://blog.kaneva.com/?page_id=2">About Kaneva</a><span>|</span><a id="A22" runat="server" href="http://blog.kaneva.com">Blog</a><span>|</span><a id="A23" runat="server" href="~/community/safety.kaneva">Parental Resources</a><span>|</span><a id="A24" runat="server" href="~/overview/privacy.aspx">Privacy</a><span>|</span><a id="A25" runat="server" href="~/overview/copyright.aspx">Copyright</a><span>|</span><a id="A26" runat="server" href="~/community/Careers.kaneva">Jobs</a></div>
	</div>

	<div class="footerVersion">� <%= DateTime.Now.ToString("yyyy") %> Kaneva<br />
		<span style="visibility:hidden;"><asp:Label id="lblVersionAuth" runat="server"/>&nbsp;<asp:Label id="lblLoadTimeAuth" runat="server"/></span></div>
	
	</div>
	
</div>