///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.usercontrols
{
    public partial class VirtualWorld_Nav : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        { 
            aSupport.HRef = Common.GetHelpURL(KanevaWebGlobals.CurrentUser.FirstName, KanevaWebGlobals.CurrentUser.LastName, KanevaWebGlobals.CurrentUser.Email);
        }

        public string ShoppingSiteName ()
        {
            string shoppingSite = KanevaGlobals.ShoppingSiteName;

            if (shoppingSite.IndexOf ("http") < 0)
            {
                shoppingSite = "http://" + shoppingSite;
            }

            return shoppingSite;
        }

        protected HtmlAnchor aSupport;
    }
}