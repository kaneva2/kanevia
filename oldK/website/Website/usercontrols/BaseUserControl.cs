///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using System;
using KlausEnt.KEP.Kaneva;
using System.Data;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using System.Collections;
using System.Collections.Specialized;

namespace KlausEnt.KEP.Kaneva.usercontrols
{
	/// <summary>
	/// Summary description for BaseUserControl.
	/// </summary>
	public class BaseUserControl : System.Web.UI.UserControl
	{
        #region Declarations
        private UserFacade userFacade;
        private ShoppingFacade shoppingFacade;
        private FameFacade fameFacade;
        private GameDeveloperFacade gameDeveloperFacade;
        private DevelopmentCompanyFacade developmentCompanyFacade;
        private SiteSecurityFacade siteSecurityFacade;
        private GameFacade gameFacade;
        private PromotionsFacade promotionFacade;
        private TransactionFacade transactionFacade;
        private CommunityFacade communityFacade;
        private ForumFacade forumFacade;
        private MediaFacade mediaFacade;
        private BlogFacade blogFacade;
        private BlastFacade blastFacade;
        private InterestsFacade interestsFacade;
        private MarketingFacade marketingFacade;
        private RaveFacade raveFacade;
        private ContestFacade contestFacade;
        private SocialFacade socialFacade;
        private EventFacade eventFacade;
        private SiteMgmtFacade siteMgmtFacade;
        private ExperimentFacade experimentFacade;

        #endregion
        
        public BaseUserControl ()
            : base ()
		{
		}

        /// <summary>
        /// GetLoginURL
        /// </summary>
        /// <returns></returns>
        public string GetLoginURL()
        {
            if (Configuration.SSLLogin)
            {
                return "https://" + Configuration.SiteName + "/loginSecure.aspx?logretURL=" + Server.UrlEncode(GetCurrentURL());
            }
            else
            {
                return ResolveUrl("~/loginSecure.aspx?logretURL=" + Server.UrlEncode(GetCurrentURL()));
            }
        }

        /// <summary>
        /// GetCurrentURL
        /// </summary>
        /// <returns></returns>
        public string GetCurrentURL()
        {
            if (Request.ApplicationPath != "/")
            {
                return Request.FilePath.Replace(Request.ApplicationPath, "") + "?" + Request.QueryString.ToString();
            }
            else
            {
                return Request.FilePath + "?" + Request.QueryString.ToString();
            }

        }

		/// <summary>
		/// Return the personal channel link
		/// </summary>
		/// <param name="channelId"></param>
		/// <returns></returns>
		public static string GetPersonalChannelUrl (string nameNoSpaces)
		{
			return KanevaGlobals.GetPersonalChannelUrl (nameNoSpaces);
		}

		/// <summary>
		/// Return the broadcast channel link
		/// </summary>
		/// <param name="channelId"></param>
		/// <returns></returns>
		public static string GetBroadcastChannelUrl (string nameNoSpaces)
		{
			return KanevaGlobals.GetBroadcastChannelUrl (nameNoSpaces);
		}

        /// <summary>
        /// Return the personal channel id of current logged in user
        /// </summary>
        /// <returns></returns>
        protected int GetPersonalChannelId()
        {
            return KanevaWebGlobals.CurrentUser.CommunityId;
        }

        /// <summary>
        /// GetAssetTypeName
        /// </summary>
        public string GetAssetTypeName(int assetTypeId)
        {
            try
            {
                DataTable dtAssetTypes = WebCache.GetAssetTypes();
                DataRow[] drAssetType = dtAssetTypes.Select("asset_type_id = " + assetTypeId, "");

                if (drAssetType != null && drAssetType.Length > 0)
                {
                    return drAssetType[0]["name"].ToString();
                }
            }
            catch (Exception) { };

            return "Unknown";
        }

        /// <summary>
        /// GetAssetTypeIcon
        /// </summary>
        public string GetAssetTypeIcon(int assetTypeId)
        {
            try
            {
                DataTable dtAssetTypes = WebCache.GetAssetTypes();
                DataRow[] drAssetType = dtAssetTypes.Select("asset_type_id = " + assetTypeId, "");

                if (drAssetType != null && drAssetType.Length > 0)
                {
                    return drAssetType[0]["icon_path"].ToString();
                }
            }
            catch (Exception) { };

            return "";
        }

        protected bool IsMature(int assetRatingId)
        {
            return StoreUtility.IsMatureRating(assetRatingId);
        }
		
		/// <summary>
		/// Return the appropriate channel link based on supplied  arguement
		/// specialized function for cases where control may need to be able to 
		/// display both - Global for reuse
		///
		/// </summary>
		/// <param name="nameNoSpaces"></param>
		/// <param name="choice"></param>
		/// <returns></returns>
		public static string GetUrlPrsnlBroadAsset (string id, int choice)
		{
			string URL = "";
			switch(choice)
			{
				case (int) Constants.eURL_TYPE.BROADBAND:
					URL = KanevaGlobals.GetBroadcastChannelUrl (id);
					break;
				case (int) Constants.eURL_TYPE.MEDIA:
					//handles exception at this level since this is most often used in .ascx page
					//default URL on exception still to be determined
					try 
					{
						//to be completed lated due to time constraint
						//URL = StoreUtility.GetAssetDetailsLink (Convert.ToInt32(id), Page);
					}
					catch(Exception)
					{
					}
					break;
				case (int) Constants.eURL_TYPE.PERSONAL:
				default:
					URL = KanevaGlobals.GetPersonalChannelUrl (id);
					break;
			}
			return URL;
		}
		/// <summary>
		/// TruncateWithEllipsis
		/// </summary>
		public string TruncateWithEllipsis (string text, int length)
		{
			return KanevaGlobals.TruncateWithEllipsis (text, length);
		}

		/// <summary>
		/// (Deprecated) - Get the the access level of the current session user
		/// access function from base page
		/// </summary>
		public bool IsUserAdministrator()
		{
			return UsersUtility.IsUserAdministrator ();
		}

		/// <summary>
		/// determines if the user is a minor? 
		/// throws it exceptions
		/// </summary>
		/// <param name="age">age</param>
		/// <returns>bool</returns>
		public bool IsUserAMinor(int age)
		{
			//check age by comparing to cut off
			if (age >= KanevaGlobals.MinorCutOffAge)
			{
				return false;
			}
			//this return is for minor found
			return true;
		}

		/// <summary>
		/// determines if the user is a minor? 
		/// throws its exceptions
		/// </summary>
		/// <param name="birthday">birthday</param>
		/// <returns>bool</returns>
		public bool IsUserAMinor(DateTime birthday)
		{
			return IsUserAMinor(KanevaGlobals.GetAgeInYears(birthday));
		}

        public bool HasWritePrivileges(int privilegeToCheck)
        {
            //check security level for the provided privilege
            int accessLevel = KanevaWebGlobals.CheckUserAccess(privilegeToCheck);

            //check can they change data (write)
            return accessLevel.Equals((int)SitePrivilege.eACCESS_LEVEL.ACCESS_FULL);
        }

        public bool HasReadPrivileges(int privilegeToCheck)
        {
            //check security level for the provided privilege
            int accessLevel = KanevaWebGlobals.CheckUserAccess(privilegeToCheck);

            //check can they change data (write)
            return accessLevel.Equals((int)SitePrivilege.eACCESS_LEVEL.ACCESS_READ);
        }

        public bool HasNoPrivileges(int privilegeToCheck)
        {
            //check security level for the provided privilege
            int accessLevel = KanevaWebGlobals.CheckUserAccess(privilegeToCheck);

            //check can they change data (write)
            return accessLevel.Equals((int)SitePrivilege.eACCESS_LEVEL.ACCESS_NONE);
        }


		/// <summary>
		/// Return the user image URL
		/// </summary>
		public string GetProfileImageURL (string imagePath)
		{
			return UsersUtility.GetProfileImageURL (imagePath);
		}

		/// <summary>
		/// Return the user image URL
		/// </summary>
		public string GetProfileImageURL (string imagePath, string size, string gender)
		{
			return UsersUtility.GetProfileImageURL (imagePath, size, gender);
		}

        /// <summary>
        /// Return the user image URL
        /// </summary>
        public string GetProfileImageURL (string imagePath, string size, string gender, bool checkForFacebookImg)
        {
            return UsersUtility.GetProfileImageURL (imagePath, size, gender, checkForFacebookImg);
        }

        /// <summary>
        /// Return the user image URL. Note: Duplicated in Facade Common. This will eventually be deprecated following resolution of
        /// namespace issue with Facade Configuration on some files.
        /// </summary>
        public string GetProfileImageURL (string imagePath, string defaultSize, string gender, bool useFBProfile, ulong fbUserId)
        {
            if (useFBProfile && fbUserId > 0)
            {
                return KanevaGlobals.FacebookGraphUrl + fbUserId.ToString () + KanevaGlobals.GetFacebookProfileImageQueryStringBySize (defaultSize);
            }

            return UsersUtility.GetProfileImageURL (imagePath, defaultSize, gender);
        }

        /// <summary>
        /// Note: Duplicated in Facade Common. This will eventually be deprecated following resolution of
        /// namespace issue with Facade Configuration on some files.
        /// </summary>
        public string GetFacebookProfileImageUrl (UInt64 fbUserId, string defaultSize)
        {
            return KanevaGlobals.FacebookGraphUrl + fbUserId.ToString () + KanevaGlobals.GetFacebookProfileImageQueryStringBySize (defaultSize);
        }

		/// <summary>
		/// Get the correct Media Type
		/// </summary>
		protected string GetMediaImageURL (string imagePath, string size, int assetId, int assetTypeId)
		{
			return GetMediaImageURL (imagePath, size, assetId, assetTypeId, 0);
		}

		/// <summary>
		/// Get the correct Media Type
		/// </summary>
		protected string GetMediaImageURL (string imagePath, string size, int assetId, int assetTypeId, int thumbGenerated)
		{
			return StoreUtility.GetMediaImageURL (imagePath, size, assetId, assetTypeId, thumbGenerated, Page);
		}

		/// <summary>
		/// Return the video image URL
		/// </summary>
		public string GetVideoImageURL (string imagePath, string size)
		{
			return StoreUtility.GetVideoImageURL (imagePath, size);
		}

		/// <summary>
		/// Return the photo image URL
		/// </summary>
		public string GetPhotoImageURL (string imagePath, string size)
		{
			return StoreUtility.GetPhotoImageURL (imagePath, size);
		}

		/// <summary>
		/// Return the music image URL
		/// </summary>
		public string GetMusicImageURL (string imagePath, string size)
		{
			return StoreUtility.GetMusicImageURL (imagePath, size);
		}

		/// <summary>
		/// Return the game image URL
		/// </summary>
		public string GetGameImageURL (string imagePath, string size)
		{
			return StoreUtility.GetGameImageURL (imagePath, size);
		}

		/// <summary>
		/// Return the photo image URL
		/// </summary>
		public string GetBroadcastChannelImageURL (string imagePath, string defaultSize)
		{
            string img = CommunityUtility.GetBroadcastChannelImageURL (imagePath, defaultSize);

            if (defaultSize == "sm" && img.Contains ("_default_sm.jpg"))
            {
                img = img.Replace ("_default_sm.jpg", "_default_sm_fix.jpg");
            }
            return img;
        }

		/// <summary>
		/// Show an error message on startup
		/// </summary>
		/// <param name="errorMessage"></param>
		protected void ShowErrorOnStartup (string errorMessage)
		{
			ShowErrorOnStartup (errorMessage, true);
		}

		/// <summary>
		/// GetOnlineText gets the user's online status and returns it in a string for display
		/// </summary>
		protected string GetOnlineText (string ustate)
		{
			//implement catch here to prevent error page from being displayed for a simple online stauts check
			//default to no status is error occurs
			try
			{
				string strUserState = KanevaWebGlobals.GetUserState (ustate);
				if(strUserState.Length > 0)
				{
					return "<p class=\"online\">" + strUserState + "</p>";
				}
				else
				{
					return "<p >&nbsp;</p>";
				}
			}
			catch(InvalidCastException)
			{
			}

			return "";
		}

		/// <summary>
		/// Get Asset Details Link
		/// </summary>
		/// <param name="assetId"></param>
		/// <returns></returns>
		public string GetAssetDetailsLink (int assetId)
		{
			return StoreUtility.GetAssetDetailsLink (assetId, Page);
		}

		/// <summary>
		/// Show an error message on startup
		/// </summary>
		/// <param name="errorMessage"></param>
		protected void ShowErrorOnStartup (string errorMessage, bool bShowPleaseMessage)
		{
			string scriptString = "<script language=JavaScript>";
			if (bShowPleaseMessage)
			{
				scriptString += "alert ('Please correct the following errors:\\n\\n" + errorMessage + "');";
			}
			else
			{
				scriptString += "alert ('" + errorMessage + "');";
			}
			scriptString += "</script>";

			if (!Page.ClientScript.IsClientScriptBlockRegistered (GetType (),"ShowError"))
			{
                Page.ClientScript.RegisterStartupScript(GetType(), "ShowError", scriptString);
			}
		}

        protected void RegisterJavaScript (string fileAndPath)
        {
            string fileName = System.IO.Path.GetFileName (fileAndPath);

            if (!Page.ClientScript.IsClientScriptIncludeRegistered (fileName))
            {
                Page.ClientScript.RegisterClientScriptInclude (fileName, "http://" + KanevaGlobals.SiteName + 
                    (fileAndPath.StartsWith("/") ? "" : "/") + fileAndPath);
            }
        }

        /// <summary>
        /// GetCommunityTags
        /// </summary>
        /// <param name="keywords"></param>
        /// <returns></returns>
        public string GetCommunityTags (string keywords)
        {
            Hashtable htKeywords = new Hashtable ();
            Common.AddToHash (htKeywords, keywords);

            string strKeywords = "";

            IDictionaryEnumerator en = htKeywords.GetEnumerator ();
            while (en.MoveNext ())
            {
                strKeywords += "<a href=\"" + ResolveUrl ("~/community/commDrillDown.aspx?kwd=" + en.Value) + "\">" + en.Value + "</a> ";
            }

            if (strKeywords.Length == 0)
            {
                strKeywords = "<span>Currently not tagged.</span>";
            }

            return strKeywords;
        }

        // ***********************************************
        // Get Facade Functions
        // ***********************************************
        #region Get Facades

        public RaveFacade GetRaveFacade
        {
            get
            {
                if (raveFacade == null)
                {
                    raveFacade = new RaveFacade();
                }
                return raveFacade;
            }
        }

        public ShoppingFacade GetShoppingFacade
        {
            get
            {
                if (shoppingFacade == null)
                {
                    shoppingFacade = new ShoppingFacade ();
                }
                return shoppingFacade;
            }
        }

        public BlastFacade GetBlastFacade
        {
            get
            {
                if (blastFacade == null)
                {
                    blastFacade = new BlastFacade ();
                }
                return blastFacade;
            }
        }

        public MarketingFacade GetMarketingFacade
        {
            get
            {
                if (marketingFacade == null)
                {
                    marketingFacade = new MarketingFacade();
                }
                return marketingFacade;
            }
        }

        public InterestsFacade GetInterestsFacade
        {
            get
            {
                if (interestsFacade == null)
                {
                    interestsFacade = new InterestsFacade();
                }
                return interestsFacade;
            }
        }

        public BlogFacade GetBlogFacade
        {
            get
            {
                if (blogFacade == null)
                {
                    blogFacade = new BlogFacade ();
                }
                return blogFacade;
            }
        }

        public UserFacade GetUserFacade
        {
            get
            {
                if (userFacade == null)
                {
                    userFacade = new UserFacade ();
                }
                return userFacade;
            }
        }

        public FameFacade GetFameFacade
        {
            get
            {
                if (fameFacade == null)
                {
                    fameFacade = new FameFacade ();
                }
                return fameFacade;
            }
        }

        public ForumFacade GetForumFacade
        {
            get
            {
                if (forumFacade == null)
                {
                    forumFacade = new ForumFacade ();
                }
                return forumFacade;
            }
        }

        public GameFacade GetGameFacade
        {
            get
            {
                if (gameFacade == null)
                {
                    gameFacade = new GameFacade ();
                }
                return gameFacade;
            }
        }

        public GameDeveloperFacade GetGameDeveloperFacade
        {
            get
            {
                if (gameDeveloperFacade == null)
                {
                    gameDeveloperFacade = new GameDeveloperFacade ();
                }
                return gameDeveloperFacade;
            }
        }

        public DevelopmentCompanyFacade GetDevelopmentCompanyFacade
        {
            get
            {
                if (developmentCompanyFacade == null)
                {
                    developmentCompanyFacade = new DevelopmentCompanyFacade ();
                }
                return developmentCompanyFacade;
            }
        }

        public SiteSecurityFacade GetSiteSecurityFacade
        {
            get
            {
                if (siteSecurityFacade == null)
                {
                    siteSecurityFacade = new SiteSecurityFacade ();
                }
                return siteSecurityFacade;
            }
        }

        protected PromotionsFacade GetPromotionsFacade
        {
            get
            {
                if (promotionFacade == null)
                {
                    promotionFacade = new PromotionsFacade ();
                }
                return promotionFacade;
            }
        }

        protected TransactionFacade GetTransactionFacade
        {
            get
            {
                if (transactionFacade == null)
                {
                    transactionFacade = new TransactionFacade ();
                }
                return transactionFacade;
            }
        }

        protected CommunityFacade GetCommunityFacade
        {
            get
            {
                if (communityFacade == null)
                {
                    communityFacade = new CommunityFacade ();
                }
                return communityFacade;
            }
        }

        protected ContestFacade GetContestFacade
        {
            get
            {
                if (contestFacade == null)
                {
                    contestFacade = new ContestFacade ();
                }
                return contestFacade;
            }
        }

        protected MediaFacade GetMediaFacade
        {
            get
            {
                if (mediaFacade == null)
                {
                    mediaFacade = new MediaFacade ();
                }
                return mediaFacade;
            }
        }

        protected SocialFacade GetSocialFacade
        {
            get
            {
                if (socialFacade == null)
                {
                    socialFacade = new SocialFacade ();
                }
                return socialFacade;
            }
        }

        protected EventFacade GetEventFacade
        {
            get
            {
                if (eventFacade == null)
                {
                    eventFacade = new EventFacade ();
                }
                return eventFacade;
            }
        }

        protected SiteMgmtFacade GetSiteManagementFacade
        {
            get
            {
                if (siteMgmtFacade == null)
                {
                    siteMgmtFacade = new SiteMgmtFacade ();
                }
                return siteMgmtFacade;
            }
        }

        protected ExperimentFacade GetExperimentFacade
        {
            get
            {
                if (experimentFacade == null)
                {
                    experimentFacade = new ExperimentFacade();
                }
                return experimentFacade;
            }
        }

        #endregion

        /// <summary>
        /// Format the date into a readable formate
        /// </summary>
        /// <param name="dtDate"></param>
        /// <returns></returns>
        public string FormatDateTimeSpan (Object dtDate, Object dtDateNow)
        {
            if (dtDate.Equals (DBNull.Value))
                return "";
            return KanevaGlobals.FormatDateTimeSpan (dtDate, dtDateNow);
        }

        public bool AddFriend (int userId, int friendId, ref string msg)
        {
            // Must be logged in
            if (!Request.IsAuthenticated)
            {
                msg = "Please sign in to add this user as a friend";
                return false;
            }
            // Can't friend yourself
            if (userId.Equals (friendId))
            {
                msg = "You cannot add yourself as a friend.";
                return false;
            }
            // Add them as a friend
            int ret = GetUserFacade.InsertFriendRequest (userId, friendId,
                Page.Request.CurrentExecutionFilePath, Global.RequestRabbitMQChannel());
            if (ret.Equals (0))	// already a friend
            {
                msg = "This member is already your friend.";
            }
            else if (ret.Equals (2))  // pending friend request
            {
                msg = "You have already sent this member a friend request.";
            }
            else if (ret.Equals (1))  // success
            {
                // send request email
                MailUtilityWeb.SendFriendRequestEmail (userId, friendId);
                return true;
            }
            else
            {
                msg = "Friend request could not be sent at this time.";
            }
            return false;
        }

        public void ConfigureCommunityMeetMe3D(global::System.Web.UI.WebControls.LinkButton lnkButton, int gameId, int communityId, string communityName, string requestTrackingGUID)
        {
            lnkButton.Attributes.Add("onclick", StpUrl.GetPluginJS(Request.IsAuthenticated, KanevaWebGlobals.CurrentUser.HasWOKAccount, gameId, communityId, communityName, requestTrackingGUID, KanevaWebGlobals.CurrentUser.UserId));
        }

        public void ConfigureCommunityMeetMe3D(global::System.Web.UI.HtmlControls.HtmlAnchor aMeetMeLink, int gameId, int communityId, string communityName, string requestTrackingGUID)
        {
            aMeetMeLink.Attributes.Add ("onclick", StpUrl.GetPluginJS (Request.IsAuthenticated, KanevaWebGlobals.CurrentUser.HasWOKAccount, gameId, communityId, communityName, requestTrackingGUID, KanevaWebGlobals.CurrentUser.UserId));
        }

        public void ConfigureUserMeetMe3D(global::System.Web.UI.WebControls.LinkButton lnkButton, string userName, string requestTrackingGUID)
        {
            lnkButton.Attributes.Add ("onclick", StpUrl.GetPluginJSForUser (Request.IsAuthenticated, KanevaWebGlobals.CurrentUser.HasWOKAccount, userName, requestTrackingGUID, KanevaWebGlobals.CurrentUser.UserId));
        }

        public void ConfigureUserMeetMe3D(global::System.Web.UI.WebControls.ImageButton lnkButton, string userName, string requestTrackingGUID)
        {
            lnkButton.Attributes.Add ("onclick", StpUrl.GetPluginJSForUser (Request.IsAuthenticated, KanevaWebGlobals.CurrentUser.HasWOKAccount, userName, requestTrackingGUID, KanevaWebGlobals.CurrentUser.UserId));
        }

        public string CommunityMeetMe3DLink (int gameId, int communityId, string communityName, string requestTrackingGUID)
        {
            return StpUrl.GetPluginJS (Request.IsAuthenticated, KanevaWebGlobals.CurrentUser.HasWOKAccount, gameId, communityId, communityName, requestTrackingGUID, KanevaWebGlobals.CurrentUser.UserId);
        }
        
        public string UserMeetMe3DLink (string userName, string requestTrackingGUID)
        {
            return StpUrl.GetPluginJSForUser (Request.IsAuthenticated, KanevaWebGlobals.CurrentUser.HasWOKAccount, userName, requestTrackingGUID, KanevaWebGlobals.CurrentUser.UserId);
        }


        /// <summary>
        /// Get the results text
        /// </summary>
        /// <returns></returns>
        public string GetResultsText (int TotalCount, int pageNumber, int resultsPerPage, int currentPageCount)
        {
            return KanevaGlobals.GetResultsText (TotalCount, pageNumber, resultsPerPage, currentPageCount, true);
        }

        /// <summary>
        /// Get the results text
        /// </summary>
        /// <returns></returns>
        public string GetResultsText (UInt32 TotalCount, int pageNumber, int resultsPerPage, int currentPageCount)
        {
            return KanevaGlobals.GetResultsText (TotalCount, pageNumber, resultsPerPage, currentPageCount, true);
        }


        /// <summary>
        /// Get the results text
        /// </summary>
        /// <returns></returns>
        public string GetResultsText (int TotalCount, int pageNumber, int resultsPerPage, int currentPageCount, bool bGetCounts)
        {
            return KanevaGlobals.GetResultsText (TotalCount, pageNumber, resultsPerPage, currentPageCount, bGetCounts);
        }

        /// <summary>
        /// Get the results text
        /// </summary>
        /// <returns></returns>
        public string GetResultsText (UInt32 TotalCount, int pageNumber, int resultsPerPage, int currentPageCount, bool bGetCounts)
        {
            return KanevaGlobals.GetResultsText (TotalCount, pageNumber, resultsPerPage, currentPageCount, bGetCounts);
        }
    }
}
