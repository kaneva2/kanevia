<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WOKItemComments.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.usercontrols.WOKItemComments" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="Pager.ascx" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>

<script language="javascript" src="../jscript/prototype.js"></script>
<script language="JavaScript">
<!--
function reload() 
{
	window.location = window.location.href;
}

var ctrl_id = '<asp:literal id="litControlId" runat="server" />';
var textboxtextSave = '';
var maxChars = <asp:literal id="litMaxLen" runat="server"/>;

var prev_replyto_id = null;
var prev_lnk_btn = null;
var prev_loc = null;

var reply_btn_txt = '(reply)';
var reply_btn_close_txt = '(close)';
var add_btn_txt = 'Post a Comment';
var add_btn_close_txt = 'Close Comment Box';

function CharsRemain(t, span_id)
{		
	var val = t.value.length;
	
	if (val > maxChars)
		t.value = textboxtextSave;
	else
	{		 
		$(span_id).innerHTML = maxChars-val+' ';
		textboxtextSave = t.value;
	}   
}
function ValidateLen (txtarea)
{	  
	txtarea.value = txtarea.value.stripScripts().strip();

	var len = txtarea.value.length;
	
	if (len == 0)
	{
		alert('You can not enter a blank comment.');
		return false;
	}
	else if (len > maxChars)
	{
		alert('Your comment is too long.  It must be ' + maxChars + ' characters or less.');
		return false;
	}
	
	prev_lnk_btn = null;
	prev_replyto_id = null;
	return true;
}

function ShowReply(comment_id, t, parentId, replyToId)
{	 
	var div_comment = $('divComment'+replyToId);
	var div_container = $('divRespond');
	var reply = replyToId > 0 ? true : false;
	
	div_container.style.top = div_comment.offsetTop;
	div_container.style.left = div_comment.offsetLeft;
	
	if (t.innerHTML == reply_btn_close_txt || t.innerHTML == add_btn_close_txt || prev_lnk_btn == null)
	{	
		Element.toggle('divRespond');
	}
	else
	{	  
		if (prev_lnk_btn.innerHTML == reply_btn_txt	|| prev_lnk_btn.innerHTML == add_btn_txt)
		{
			Element.toggle('divRespond');
		}
		else
		{
			if (prev_lnk_btn.innerHTML == reply_btn_close_txt) 
				prev_lnk_btn.innerHTML = reply_btn_txt;
			else
				prev_lnk_btn.innerHTML = add_btn_txt;
			
			if (prev_replyto_id != null)
			{
				$('divComment'+prev_replyto_id).style.paddingBottom='0px';;
			}
		}
	}	
	
	if (Element.visible('divRespond'))
	{
		$(ctrl_id+'_tblRespond').style.display="block";
		
		div_comment.style.paddingBottom = 145 + 'px';
		div_container.style.height = '170px';
		
		if (reply)
		{
			t.innerHTML = reply_btn_close_txt;
		}
		else
		{
			t.innerHTML = add_btn_close_txt;
			div_comment.style.paddingBottom = 165 + 'px';
			div_container.style.top = div_comment.offsetTop + 20;
		}

		div_container.style.display='block';

		var txt_box = $(ctrl_id+'_txtResponse');
		if (txt_box != null)
		{
			txt_box.focus();
		}
	}
	else
	{
		div_comment.style.paddingBottom='0px';
		
		if (reply)
			t.innerHTML = reply_btn_txt;
		else
			t.innerHTML = add_btn_txt;
	}
	
	$(ctrl_id+'_hidParentCommentID').value = parentId;
	$(ctrl_id+'_hidReplyToCommentID').value = replyToId;
	
	prev_lnk_btn = t;
	prev_replyto_id = replyToId;
	prev_loc = div_container.style.top;
}
function ShowCommentBox (comment_id, parent_id, t)
{													  
	if (parent_id == 0)
	{
		parent_id = comment_id;
	}
	
	var div_comment = $('divComment'+comment_id);
	
	var htmlstring = '\
		<table id="ucComments_tblRespond" align="center" name="tblRespond" cellpadding="0" cellspacing="0" border="0" width="100%"> \
			<tr> \
				<td align="center" class="bodyText"> \
					<br/> \
					<div style="text-align:right;width:382px;" class="note"><span id="spnRemain'+comment_id+'">'+maxChars+'</span> characters remaining</div> \
					<textarea id="txtCommentBox'+comment_id+'" onkeyup="CharsRemain(this,\'spnRemain'+comment_id+'\');" rows="5" cols="45" name="txtCommentBox'+comment_id+'" style="margin:0 0 5 0"></textarea><br/> \
				</td> \
			</tr> \
			<tr> \
				<td align="center"> \
					<input class="biginput" onclick="SaveComment('+comment_id+','+parent_id+','+comment_id+');" style="width:100px;margin:5px;" type="button" name="=btnPostComment" value="Post it!" id="btnPostComment" class="Filter2" />&nbsp;';
	
	if (comment_id > 0)
	{				
		htmlstring += '<span class="floatright"><a href="javascript:void(0);" onclick="ShowCommentBox('+comment_id+','+parent_id+',$(\'aReply'+comment_id+'\'));return false;" style="position:absolute;bottom:8px;right:10px;display:block;">(close)</a></span>';
	}
	
	htmlstring += '\
				</td> \
			</tr> \
		</table>';
	
	div_comment.innerHTML = htmlstring;
	
	Element.toggle('divComment'+comment_id);
	
	var btn_text = '';
	if (Element.visible('divComment'+comment_id))
	{
		if (comment_id > 0)
			btn_text = reply_btn_close_txt;
		else
			btn_text = add_btn_close_txt;
			
		var txt_area = $('txtCommentBox'+comment_id);
		if (txt_area)
			txt_area.focus();
	}
	else
	{
		if (comment_id > 0)
			btn_text = reply_btn_txt;
		else
			btn_text = add_btn_txt;
	}
	t.innerHTML = btn_text;
}
function SaveComment (comment_id, parent_id, replyto_id)
{															  
	if ( ValidateLen($('txtCommentBox'+comment_id)) )
	{
		$(ctrl_id+'_hidCommentText').value = $('txtCommentBox'+comment_id).value.stripScripts().escapeHTML();
		
		$(ctrl_id+'_hidCommentID').value = comment_id;
		$(ctrl_id+'_hidParentCommentID').value = parent_id;	   
		$(ctrl_id+'_hidReplyToCommentID').value = replyto_id;   

		AJAXCbo.DoAjaxCall(ctrl_id+':SubmitReply','','async'); 
	}
	return false;
}										
function ShowEditBox (comment_id)
{
	var comment_txt;
	var div_edit = $('divEdit'+comment_id);
	
	if ($('txtEditComment'+comment_id))
	{	 
		comment_txt = $('hidCommentTxt'+comment_id).value.unescapeHTML();
	}	
	else
	{	   
		comment_txt = div_edit.innerText;
	}
		
	Element.toggle('aEdit'+comment_id);
	Element.toggle('aDelete'+comment_id);
	
	Element.toggle('aEditSave'+comment_id);
	Element.toggle('aEditCancel'+comment_id);

	if (Element.visible('aEditSave'+comment_id))
	{
		var htmlstring = ' \
			<div style="text-align:right;width:316px;" class="note"><span id="spnEditRemain'+comment_id+'"></span> characters remaining</div> \
			<textarea onfocus="CharsRemain(this,\'spnEditRemain'+comment_id+'\');" name="txtEditComment'+comment_id+'" id="txtEditComment'+comment_id+'" \
			rows="5" cols="37" value="1234" style="margin:0 0 5 0" onkeyup="CharsRemain(this,\'spnEditRemain'+comment_id+'\');">'+div_edit.innerHTML+'</textarea>';
		div_edit.innerHTML = htmlstring;
		
		var txt_area = $('txtEditComment'+comment_id);
		if (txt_area)
		{
			//txt_area.value = div_edit.innerText;
			txt_area.focus();
		}
	}
	else
	{			 
		div_edit.innerHTML = comment_txt;
	}

}
function SaveEdit (comment_id, user_id)
{
	var txt_box = $('txtEditComment'+comment_id);
	
	if ( ValidateLen (txt_box) )
	{
		$(ctrl_id+'_hidCommentText').value = txt_box.value.stripScripts().escapeHTML();
		$(ctrl_id+'_hidCommentID').value = comment_id;
		$(ctrl_id+'_hidUserID').value = user_id;
		
		AJAXCbo.DoAjaxCall(ctrl_id+':SaveEdit','','async');
	}
}
function Toggle(id)
{
	Element.toggle('divComment'+id);
}

//-->
</script>

<ajax:ajaxpanel id="ajpComments" runat="server">

<div id="divView" runat="server">

	<h2>Reviews <span class="hlink"><a id="lnkAddCommentImg" runat="server" href="javascript:void(0);"><img id="Img1" src="~/images/icon_comment.gif" runat="server" style="vertical-align:middle;" border="0" width="16" height="16" align="top" /></a> <a id="lnkAddComment" runat="server" href="javascript:void(0);">Post a Comment</a></span></h2>
	<table width="97%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td width="100%" align="center">

				<table cellpadding="0" cellspacing="0" border="0" width="100%">
					<tr>
						<td valign="top">
							<div id="commentsContainer">
					
								<div class="replyform" style="display:none;" id="divComment0"></div>

						<asp:repeater id="rptComments" runat="server">
							<itemtemplate>
			
								<div id="divContainer<%# (DataBinder.Eval(Container.DataItem, "CommentId")) %>" style="width=100%;">
								<div class="comment<%# Convert.ToInt32(DataBinder.Eval(Container.DataItem, "ParentCommentId")) == 0 ? "" : " reply" %>">
									<div class="commentpoint"></div>
									<div class="framesize-xsmall commentpic">
										<div class="frame">
											<span class="ct"><span class="cl"></span></span>							
												<div class="imgconstrain">
													<a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "Owner.NameNoSpaces").ToString ())%>>
														<img id="Img2" runat="server" src='<%#GetProfileImageURL (DataBinder.Eval(Container.DataItem, "Owner.ThumbnailSmallPath").ToString (), "sm", DataBinder.Eval(Container.DataItem, "Owner.Gender").ToString ())%>' border="0"/>
													</a>
												</div>																																				
											<span class="cb"><span class="cl"></span></span>
										</div>
									</div>
									<div class="commenttext">
										<h3><a href=<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "Owner.NameNoSpaces").ToString ())%>><%# DataBinder.Eval(Container.DataItem, "Owner.Username")%></a>
										<span class="hlink note"><%# FormatDateTimeSpan (DataBinder.Eval(Container.DataItem, "CreatedDate"), DataBinder.Eval(Container.DataItem, "CurrentTime")) %> &nbsp;
										
										<span style="width:36px;text-align:right;"><a <%# GetReplyLinkScript(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "CommentId")), Convert.ToInt32(DataBinder.Eval(Container.DataItem, "ParentCommentId"))) %> id="aReply<%# (DataBinder.Eval(Container.DataItem, "CommentId")) %>">(reply)</a></span></span></h3>
										
										<%# GetOnlineText(DataBinder.Eval(Container.DataItem, "Owner.UState").ToString())%>
										<div id="divEdit<%# (DataBinder.Eval(Container.DataItem, "CommentId")) %>"><%# Server.HtmlDecode(DataBinder.Eval(Container.DataItem, "CommentText").ToString()) %></div>
										<div class="note" style="margin:5 0 0 0;display:<%# GetThreadEditedText (DataBinder.Eval(Container.DataItem, "UpdatedUsername").ToString (), DataBinder.Eval(Container.DataItem, "LastUpdatedDate"), DataBinder.Eval(Container.DataItem, "CurrentTime")) == "" ? "none" : "block" %>;">
											<asp:label runat="server" id="lblEdited" text='<%# GetThreadEditedText (DataBinder.Eval(Container.DataItem, "UpdatedUsername").ToString (), DataBinder.Eval(Container.DataItem, "LastUpdatedDate"), DataBinder.Eval(Container.DataItem, "CurrentTime")) %>'/>
										</div>
										<div style="text-align:right;display:<%# IsUserAdministrator() || (int)DataBinder.Eval(Container.DataItem, "UserId") == GetUserId() || CanUserDeleteComment() ? "block" : "none" %>;">
											<!--a visible=<%# IsUserAdministrator() || (int)DataBinder.Eval(Container.DataItem, "UserId") == GetUserId() %> href='<%#GetEditCommentScript ( (int)DataBinder.Eval(Container.DataItem, "CommentId"), (int)DataBinder.Eval(Container.DataItem, "UserId"), Server.HtmlDecode(DataBinder.Eval(Container.DataItem, "CommentText").ToString()))  %>'>Edit</a>&nbsp;&nbsp;&nbsp;-->
											
											<a id="aEdit<%# (DataBinder.Eval(Container.DataItem, "CommentId")) %>" onclick="ShowEditBox(<%# (DataBinder.Eval(Container.DataItem, "CommentId")) %>);" visible="<%# IsUserAdministrator() || (int)DataBinder.Eval(Container.DataItem, "UserId") == GetUserId() %>" href="javascript:void(0);" style="margin-right:5px;">Edit</a>
											<a id="aDelete<%# (DataBinder.Eval(Container.DataItem, "CommentId")) %>" visible='<%# CanUserDeleteComment() || (int)DataBinder.Eval(Container.DataItem, "UserId") == GetUserId() %>' href="<%#GetDeleteCommentScript ((int) DataBinder.Eval(Container.DataItem, "CommentId"), (int)DataBinder.Eval(Container.DataItem, "UserId")) %>">Delete</a>
										
											<a id="aEditSave<%# (DataBinder.Eval(Container.DataItem, "CommentId")) %>" onclick="SaveEdit(<%# (DataBinder.Eval(Container.DataItem, "CommentId")) %>,<%# DataBinder.Eval(Container.DataItem, "UserId") %>);" href="javascript:void(0);" style="display:none;margin-right:5px;">Save</a>
											<a id="aEditCancel<%# (DataBinder.Eval(Container.DataItem, "CommentId")) %>" onclick="ShowEditBox(<%# (DataBinder.Eval(Container.DataItem, "CommentId")) %>);" style="display:none;" href="javascript:void(0);">Cancel</a>
											
										</div>
									</div><input type="hidden" id="hidCommentTxt<%# (DataBinder.Eval(Container.DataItem, "CommentId")) %>" value="<%# DataBinder.Eval(Container.DataItem, "CommentText") %>"/>
								</div></div>
								<div class="replyform" style="display:none;" id="divComment<%# (DataBinder.Eval(Container.DataItem, "CommentId")) %>"></div>

							</itemtemplate>
						</asp:repeater>
						
																
								<div id="divCallOut" runat="server" class="comment nocomment" visible="false">
									<div class="commenttext"><asp:Label id="nocomments" Runat="server">Be the first to leave a comment!</asp:Label></div>
								</div>
						
							</div>		
						</td>
					</tr>
					
				</table>

				
				<br/>
												
			</td>
		</tr>
	</table>
	
	<table cellpadding="0" cellspacing="0" border="0" width="99%" style="padding:0 10 0 5;" id="tblFooter" runat="server">
		<tr>
			<td width="100%" align="center">
				<table cellpadding="0" cellspacing="0" width="100%" border="0" align="left">
					<tr>
						<td valign="bottom" align="right">
							<table cellpadding="0" cellspacing="0" align="right" border="0">
								<tr>
									<td align="right" class="widgetTextBold10">
										<asp:label runat="server" id="lblSearch" cssclass="widgetTextBold10"/><br>
										<kaneva:pager isajaxmode="True" runat="server" id="pgTop"/>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	
							
	<input type="hidden" runat="server" id="hidCommentID" value="0" name="hidCommentID">
	<input type="hidden" runat="server" id="hidCommentOwnerID" value="0" name="hidCommentOwnerID"> 
	<input type="hidden" runat="server" id="hidParentCommentID" value="0" name="hidParentCommentID">
	<input type="hidden" runat="server" id="hidReplyToCommentID" value="0" name="hidReplyToCommentID"> 
	<input type="hidden" runat="server" id="hidCommentText" value="" name="hidCommentText"> 
	<input type="hidden" runat="server" id="hidUserID" value="0" name="hidUserID"> 
	<asp:button id="DeleteThread" onclick="Delete_Reviews" runat="server" visible="false"></asp:button>
	<asp:button id="SubmitReply" onclick="ReplyComment_Click" runat="server" visible="False"></asp:button>
	<asp:button id="SaveEdit" onclick="EditComments_Click" runat="server" visible="false"></asp:button>
</div>								

</ajax:ajaxpanel>

