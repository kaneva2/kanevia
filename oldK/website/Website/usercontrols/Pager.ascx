<%@ Control Language="c#" AutoEventWireup="false" Codebehind="Pager.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.Pager" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>

<asp:linkbutton ID="PrevPage" runat="server" CommandName="PrevPage" OnCommand="Page_Changed"><<</asp:linkbutton> <asp:label id="sep1" runat="server">|</asp:label>
<asp:label ID="PagesDisplay" runat="server"></asp:label> <asp:label id="sep2" runat="server">|</asp:label>
<asp:linkbutton ID="NextPage" runat="server" CommandName="NextPage" OnCommand="Page_Changed">>></asp:linkbutton>
			
<input type="hidden" runat="server" id="hidPageNumber" value="1"> 
<!-- Hidden button to handle the click event when user clicks on a page link-->
<asp:button ID="PageClick" OnClick="Page_Click" runat="server" Visible="false"></asp:button>