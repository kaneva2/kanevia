///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;

namespace KlausEnt.KEP.Kaneva.usercontrols
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	using log4net;

	/// <summary>
	///		Summary description for ProfileInterests.
	/// </summary>
	public class ProfileInterests : BaseUserControl
	{
		public CuteEditor.Editor txtTitle;
		protected CuteEditor.Editor txtAboutMe, txtLikeToMeet, txtMusic, txtMovies, txtTV, txtBooks, txtGames;
		protected CuteEditor.Editor txtHobbies, txtSports, txtGadgets, txtFashion, txtCars, txtArtists;
		protected CuteEditor.Editor txtClubs, txtBrands, txtRoleModel, txtQuotes;

		private int _user_id;

		// Logger
		private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

		private void Page_Load(object sender, System.EventArgs e)
		{
			if (!IsPostBack)
				LoadData( _user_id );
		}

		public void LoadData( int userId )
		{
            UserFacade userFacade = new UserFacade();
            User user = userFacade.GetUser(userId);

			DataRow drUserProfile = UsersUtility.GetUserProfile (userId);

			txtTitle.Text = drUserProfile ["title_blast"].ToString ();
			txtAboutMe.Text = user.Description;
			txtLikeToMeet.Text = drUserProfile ["like_to_meet"].ToString ();
			txtMusic.Text = drUserProfile ["favorite_music"].ToString ();
			txtMovies.Text = drUserProfile ["favorite_movies"].ToString ();
			txtTV.Text = drUserProfile ["favorite_tv"].ToString ();
			txtBooks.Text = drUserProfile ["favorite_books"].ToString ();
			txtGames.Text = drUserProfile ["favorite_games"].ToString ();

			txtHobbies.Text = drUserProfile ["hobbies"].ToString ();
			txtSports.Text = drUserProfile ["favorite_sports"].ToString ();
			txtGadgets.Text = drUserProfile ["favorite_gadgets"].ToString ();
			txtFashion.Text = drUserProfile ["favorite_fashion"].ToString ();
			txtCars.Text = drUserProfile ["favorite_cars"].ToString ();
			txtArtists.Text = drUserProfile ["favorite_artists"].ToString ();
			txtClubs.Text = drUserProfile ["clubs"].ToString ();
			txtBrands.Text = drUserProfile ["favorite_brands"].ToString ();
			txtRoleModel.Text = drUserProfile ["role_models"].ToString ();
			txtQuotes.Text = drUserProfile ["favorite_quotes"].ToString ();
		}

		#region Methods

		public bool Update ()
		{			
			// Check for any inject scripts
			if (KanevaWebGlobals.ContainsInjectScripts (txtTitle.Text) || KanevaWebGlobals.ContainsInjectScripts (txtLikeToMeet.Text)
				|| KanevaWebGlobals.ContainsInjectScripts (txtGames.Text) || KanevaWebGlobals.ContainsInjectScripts (txtMovies.Text)
				|| KanevaWebGlobals.ContainsInjectScripts (txtMusic.Text) || KanevaWebGlobals.ContainsInjectScripts (txtBooks.Text)
				|| KanevaWebGlobals.ContainsInjectScripts (txtTV.Text) || KanevaWebGlobals.ContainsInjectScripts (txtHobbies.Text)
				|| KanevaWebGlobals.ContainsInjectScripts (txtSports.Text) || KanevaWebGlobals.ContainsInjectScripts (txtGadgets.Text)
				|| KanevaWebGlobals.ContainsInjectScripts (txtFashion.Text) || KanevaWebGlobals.ContainsInjectScripts (txtCars.Text)
				|| KanevaWebGlobals.ContainsInjectScripts (txtArtists.Text) || KanevaWebGlobals.ContainsInjectScripts (txtClubs.Text)
				|| KanevaWebGlobals.ContainsInjectScripts (txtBrands.Text) || KanevaWebGlobals.ContainsInjectScripts (txtRoleModel.Text)
				|| KanevaWebGlobals.ContainsInjectScripts (txtQuotes.Text) || KanevaWebGlobals.ContainsInjectScripts (txtAboutMe.Text)
				)
			{
				m_logger.Warn ("User " + KanevaWebGlobals.GetUserId () + " tried to script from IP " + Common.GetVisitorIPAddress());
				return false;
			}

            UserFacade userFacade = new UserFacade();
		
			// Update user profile
			//UsersUtility.UpdateUserProfile (_user_id, txtTitle.Text, txtLikeToMeet.Text, txtGames.Text, txtMovies.Text, txtMusic.Text, txtBooks.Text, txtTV.Text);

			//*** NEW ***
		    UsersUtility.UpdateUserProfile ( _user_id, txtTitle.Text.Trim(), txtLikeToMeet.Text.Trim(), txtGames.Text.Trim(), 
					txtMovies.Text.Trim(), txtMusic.Text.Trim(), txtBooks.Text.Trim(), txtTV.Text.Trim(), txtHobbies.Text.Trim(), 
					txtSports.Text.Trim(), txtGadgets.Text.Trim(), txtFashion.Text.Trim(), txtCars.Text.Trim(), txtArtists.Text.Trim(),
					txtClubs.Text.Trim(), txtBrands.Text.Trim(), txtRoleModel.Text.Trim(), txtQuotes.Text.Trim() );


            userFacade.UpdateUser(_user_id, txtAboutMe.Text);
			LoadData (_user_id);

			return true;
		}

		#endregion

		#region Properties

		public int UserId
		{
			get { return _user_id; }
			set { _user_id = value; }
		}
		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
