///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva.usercontrols
{
    public partial class AccountCompleteness : BaseUserControl
    {
        protected void Page_Load (object sender, EventArgs e)
        {
            // Checklist is only applicable for users after a specified date
            if (KanevaWebGlobals.CurrentUser.SignupDate < KanevaGlobals.CompletionChecklistStartDate)
            {
                this.Visible = false;
                return;
            }

            if (!IsPostBack)
            {
                Checklist checklist = GetFameFacade.GetChecklist (KanevaWebGlobals.CurrentUser.UserId, 6);

                decimal pct = checklist.PercentComplete + 10;
                pctcomplete.Style.Add ("width", pct + "%");
                pctmsg.InnerText = pct + "%";

                if (checklist.Completed)
                {
                    this.Visible = false;
                }
                else
                {
                    string itemsList = "";
                    string item = "";
                    int count = 0;
                    foreach (ChecklistPacket cp in checklist.ChecklistPackets)
                    {
                        if (!cp.Completed)
                        {
                            item = ShowChecklistItem (cp);

                            // Set initial message 
                            if (count == 0)
                            {
                                itemText.InnerHtml = item;
                            }

                            item = "items.push(new Item('" + item + "', " + count + "));";
                            itemsList += item;

                            count++;
                        }
                    }

                    if (count > 0)
                    {
                        litScript.Text = itemsList;
                        if (count == 1)
                        {   // If only 1 item remaining, hide skip button
                            btnSkip.Attributes.Add ("display", "none");
                        }
                    }
                    else // All packets are completed, complete checklist and show you're done message
                    {    // This message will only show 1 time to user then will be hidden going forward
                        CompleteChecklist ();
                    }
                }
            }
        }

        private string ShowChecklistItem (ChecklistPacket cp)
        {
            string msg = "";
            ChecklistItem item = GetCurrentChecklistItem (cp.Name);
            switch (item)
            {
                case ChecklistItem.INSTALL_WOK:
                    msg = "<a href=\"" + ResolveUrl ("~/community/install3d.kaneva") + "\">Install Kaneva</a> on your computer";
                    break;
                case ChecklistItem.EXPLORE_WORLDS:
                    msg = "<a href=\"" + ResolveUrl ("~/community/channel.kaneva?3dapps=") + "\">Explore 3D Worlds</a> for fun and engagement";
                    break;
                case ChecklistItem.ADD_PROFILE_PIC:
                    msg = "<a href=\"" + ResolveUrl ("~/community/ProfileEdit.aspx?communityId=" + KanevaWebGlobals.CurrentUser.CommunityId) + "\">Add Profile Picture</a>";
                    break;
                case ChecklistItem.ADD_3_FRIENDS:
                    msg = "Add 3 Friends by <a href=\"" + ResolveUrl ("~/community/channel.kaneva?3dapps=") + "\">Meeting Others in 3D</a>";
                    break;
                case ChecklistItem.FACEBOOK:
                    msg = "Find your <a href=\"javascript:void(0);\" onclick=\"FBLogin(FBAction.CONNECT);\">Facebook Friends on Kaneva</a>";
                    break;
                case ChecklistItem.CREATE_WORLD:
                    msg = "<a href=\"" + ResolveUrl ("~/community/StartWorld.aspx") + "\">Create Your Own World</a>";
                    break;
                default:
                    msg = "Great Job!";
                    expertTitle.InnerHtml = "You're All Set!";
                    btnSkip.Attributes.Add ("class", "btnClose");
                    btnSkip.InnerText = "Close";
                    GetFameFacade.CompleteAccountCompleteChecklist (KanevaWebGlobals.CurrentUser.UserId, 6);
                    break;
            }

            return msg;
        }

        private void CompleteChecklist ()
        {
            itemText.InnerHtml = "Great Job!";
            expertTitle.InnerHtml = "You're All Set!";
            btnSkip.Attributes.Add ("class", "btnClose");
            btnSkip.InnerText = "Close";
            btnSkip.Attributes.Add ("onclick", "$j('#chklistContainer').css('display', 'none');");
            GetFameFacade.CompleteAccountCompleteChecklist (KanevaWebGlobals.CurrentUser.UserId, 6);
        }

        private ChecklistItem GetCurrentChecklistItem (string name)
        {
            ChecklistItem ci;

            switch (name.ToLower ())
            {
                case "install kaneva on your computer":
                case "install_wok":
                    ci = ChecklistItem.INSTALL_WOK;
                    break;
                case "explore 3d worlds for fun and engagement":
                case "explore_worlds":
                    ci = ChecklistItem.EXPLORE_WORLDS;
                    break;
                case "add profile picture":
                case "add_profile_pic":
                    ci = ChecklistItem.ADD_PROFILE_PIC;
                    break;
                case "add 3 friends by meeting others in 3d":
                case "add_3_friends":
                    ci = ChecklistItem.ADD_3_FRIENDS;
                    break;
                case "find you facebook friends on kaneva":
                case "facebook":
                    ci = ChecklistItem.FACEBOOK;
                    break;
                case "create your own world":
                case "create_world":
                    ci = ChecklistItem.CREATE_WORLD;
                    break;
                default:
                    ci = ChecklistItem.COMPLETED;
                    break;
            }

            return ci;
        }

        private enum ChecklistItem
        {
            INSTALL_WOK = 1,
            EXPLORE_WORLDS = 2,
            ADD_PROFILE_PIC = 3,
            ADD_3_FRIENDS = 4,
            FACEBOOK = 5,
            CREATE_WORLD = 6,
            COMPLETED = 7
        }
    }
}