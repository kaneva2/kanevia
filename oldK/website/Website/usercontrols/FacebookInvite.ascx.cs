///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Kaneva.BusinessLayer.BusinessObjects;
using Kaneva.BusinessLayer.Facade;
using log4net;

namespace KlausEnt.KEP.Kaneva.usercontrols
{
    public partial class FacebookInvite : BaseUserControl
    {
        protected void Page_Load (object sender, EventArgs e)
        {
        }

        #region Helper Methods
        
        public void LoadFriends ()
        {
            // If user is not connected, show Find Friends button
            if (KanevaWebGlobals.CurrentUser.FacebookSettings.FacebookUserId == 0)
            {
                ShowSection (FBSection.Connect);          
            }
            else // If user is connected but not logged in, show facebook login button
            {
                if (rptFBFriends.Items == null || rptFBFriends.Items.Count == 0)
                {
                    bool isError = false;
                    string errMsg = string.Empty;
                    string ids = string.Empty;
                    noFBFriends.Visible = false;
                    noFBOnKaneva.Visible = false;

                    FacebookFriendList friends = GetSocialFacade.GetCurrentUsersFriends (KanevaWebGlobals.CurrentUser.FacebookSettings.AccessToken, out isError, out errMsg);
                    
                    if (isError)
                    {
                        if (errMsg.ToLower().Contains("oauthexception"))
                        {
                            ShowSection (FBSection.Login);
                            return;
                        }
                        
                        noFBFriendsMsg.InnerText = "We could not retrieve your Facebook friends list.  Please try again later.";
                        noFBFriends.Visible = true;
                        m_logger.Error ("Error retreiving users friends list from Facebook. --> " + errMsg);
                    }

                    // If user is connected, show friends lists
                    // Check to see if any of these Facebook Id's are connected to Kaneva accounts
                    if (friends.data.Count > 0)
                    {
                        foreach (FacebookUser fbuser in friends.data)
                        {
                            ids += fbuser.Id.ToString () + ",";
                        }
                    }

                    if (ids.Length > 0)
                    {
                        // Remove the last comma
                        ids = ids.Remove (ids.Length - 1, 1);

                        // Get any Facebook friends that have accounts connected to their Facebook account 
                        // and the current user is not already friends with on Kaneva
                        PagedList<User> fbFriends = GetUserFacade.GetUsersByFacebookUserId (KanevaWebGlobals.CurrentUser.UserId, ids, "", 1, 5000);

                        if (fbFriends.Count > 0)
                        {
                            // Show the list of friends that are already on Kaneva and invite the user to add them as friends
                            rptFBFriendsOnKaneva.DataSource = fbFriends;
                            rptFBFriendsOnKaneva.DataBind ();
                            btnSendAll.Visible = true;

                            // Store list of user ids in ViewState so we can use if they want to send to all
                            IList<Int32> uids = new List<Int32> ();
                            foreach (User user in fbFriends)
                            {
                                uids.Add (user.UserId);
                            }
                            this.UIds = uids;
                        }
                        else
                        {
                            // Show no Kaneva friends msg
                            noFBOnKaneva.Visible = true;
                        }

                        if (friends.data.Count > 0)
                        {
                            // Get the FB userids from friends list that exist on Kaneva
                            IList<UInt64> fbFriendsOnKanevaUserIds = GetUserFacade.GetFacebookUserIdForConnectedUsers (ids);
                            foreach (UInt64 userid in fbFriendsOnKanevaUserIds)
                            {
                                // Since user is already on Kaneva, do not show them in the invite to Kaneva list
                                friends.data.RemoveAll (delegate (FacebookUser fbu) { return fbu.Id == userid; });
                            }

                            // Recheck count to make sure we did not remove all the friends because
                            // they were already on Kaneva
                            if (friends.data.Count > 0)
                            {
                                rptFBFriends.DataSource = friends.data;
                                rptFBFriends.DataBind ();
                            }
                            else
                            {
                                // show no friends msg
                                noFBFriends.Visible = true;
                            }
                        }
                        else
                        {
                            // show no friends msg
                            noFBFriends.Visible = true;
                        }

                        ShowSection (FBSection.List);
                    }
                }
            }
        }

        public void ShowSection (FBSection section)
        {
            pnlConnect.Visible = false;
            pnlList.Visible = false;
            pnlLogin.Visible = false;
            
            if (section == FBSection.Connect)
            {
                pnlConnect.Visible = true;
            }
            else if (section == FBSection.List)
            {
                pnlList.Visible = true;
            }
            else
            {
                pnlLogin.Visible = true;
            }
        }

        protected string GetUserLocale (string isoCC)
        {
            try
            {
                if (isoCC.Length > 0)
                {
                    Country country = WebCache.GetCountry (isoCC.Substring (isoCC.IndexOf ("_") + 1));

                    if (country != null && country.Name.Length > 0)
                    {
                        return country.Name;
                    }
                }
                return "";
            }
            catch { return ""; }
        }

        public bool AddFriend (int userId, int friendId)
        {
            // Can't friend yourself
            if (userId.Equals (friendId))
            {
                return false;
            }
            // Add them as a friend
            int ret = GetUserFacade.InsertFriendRequest (userId, friendId,
                Page.Request.CurrentExecutionFilePath, Global.RequestRabbitMQChannel());
            if (ret.Equals (0))	// already a friend
            { }
            else if (ret.Equals (2))  // pending friend request
            { }
            else if (ret.Equals (1))  // success
            {
                // send request email
                MailUtilityWeb.SendFriendRequestEmail (userId, friendId);
                return true;
            }
            return false;
        }
        
        #endregion Helper Methods

        #region Event Handlers

        protected void btnSendAll_Click (Object sender, EventArgs e)
        {
            foreach (int id in UIds)
            {
                AddFriend (KanevaWebGlobals.CurrentUser.UserId, id);
            }

            foreach (RepeaterItem item in rptFBFriendsOnKaneva.Items)
            {
                HtmlAnchor btnAddFriend = (HtmlAnchor) item.FindControl ("btnAddFriend");
                if (btnAddFriend != null)
                {
                    btnAddFriend.Disabled = true;
                }
            }
            //** What to do here??  Where to go??
        }

        protected void rptFBFriendsOnKaneva_ItemDataBound (Object Sender, RepeaterItemEventArgs e)
        {
            if ((e.Item.ItemType == ListItemType.Item) || (e.Item.ItemType == ListItemType.AlternatingItem))
            {
                HtmlAnchor btnAddFriend = ((HtmlAnchor) e.Item.FindControl ("btnAddFriend"));

                User user = (User) e.Item.DataItem;

                btnAddFriend.Attributes.Add ("onclick", "AddFriend(" + user.UserId.ToString () + ",this.id);return false;");
            }
        }

        #endregion Event Handlers

        #region Properties

        private IList<Int32> UIds
        {
            get
            {
                try
                {
                    if (ViewState["UIds"] != null)
                    {
                        return (IList<Int32>) ViewState["UIds"];
                    }
                }
                catch { }
                return new List<Int32> ();
            }
            set { ViewState["UIds"] = value; }
        }

        public enum FBSection
        {
            Login,
            Connect,
            List
        }

        public string CurrentUser_UserId ()
        {
            return KanevaWebGlobals.CurrentUser.UserId.ToString ();
        }

        #endregion Properties

        #region Declearations

        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

        #endregion Declerations
    }
}