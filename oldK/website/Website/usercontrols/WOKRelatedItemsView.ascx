<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="WOKRelatedItemsView.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.usercontrols.WOKRelatedItemsView" %>
<%@ Register TagPrefix="Kaneva" TagName="Pager" Src="../usercontrols/Pager.ascx" %>
<%@ Register TagPrefix="ajax" Namespace="MagicAjax.UI.Controls" Assembly="MagicAjax" %>

<div id="divContainer" runat="server">

<ajax:ajaxpanel id="Ajaxpanel1" runat="server">

<div id="related_header">
	<ul class="related_tabs">
		<li style="width:86px;" id="liRelated" runat="server"><asp:linkbutton id="lbRelated" runat="server" commandname="related" oncommand="RelatedTab_Changed">Related</asp:linkbutton></li> 
		<li style="width:170px;" id="liMore" runat="server"><asp:linkbutton id="lbMore" runat="server" commandname="more" oncommand="RelatedTab_Changed">More from this member</asp:linkbutton></li> 
	</ul>
</div>

<div id="related_content">

<table width="96%"  border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
													
		<asp:datalist runat="server" enableviewstate="True" showfooter="False" width="100%" id="dlRelated" cellpadding="0" cellspacing="0" border="0" repeatcolumns="1" repeatdirection="Horizontal">
			<itemstyle horizontalalign="left" verticalalign="top" />
			
			<itemtemplate>

			<table width="100%" border="0" cellspacing="0" cellpadding="0">	
				<tr>
					<td align="left">
						
						<div class="framesize-small">
							<div class="passrequired" visible='<%# PassRequired (Convert.ToInt32 ( DataBinder.Eval(Container.DataItem, "access_type_id")))%>' runat="server" id="divPassRequired"></div>
							<div class="frame">
								<span class="ct"><span class="cl"></span></span>							
									<div class="imgconstrain">
										<a title='<%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "display_name").ToString ())%>' href='<%# GetWOKDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "global_id")))%>'>
											<img border="0" src='<%#GetWOKItemImageURL (DataBinder.Eval(Container.DataItem, "item_image_path").ToString () ,"me")%>' border="0" />
										</a>
									</div>																																				
								<span class="cb"><span class="cl"></span></span>
							</div>
						</div>
								
					</td>
					<td width="1"><img runat="server" src="~/images/spacer.gif" border="0" width="10" height="8" id="Img1"/></td>
					<td align="left" width="100%">
						
						<a title='<%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "display_name").ToString ())%>' href='<%# GetWOKDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "global_id")))%>'><%# TruncateWithEllipsis (Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "display_name").ToString ()), 100) %></a>
						<br>
						Shared by: <a href='<%# GetPersonalChannelUrl (DataBinder.Eval(Container.DataItem, "ownername_nospaces").ToString ())%>'><%# DataBinder.Eval(Container.DataItem, "ownername") %></a>
						<br>
						Views: <%# DataBinder.Eval(Container.DataItem, "views_count") %> | Raves: <%# DataBinder.Eval(Container.DataItem, "raves_count") %>
								
					</td>
				</tr>
			</table>
					
			<hr />			
				
			</itemtemplate>
		</asp:datalist>
		
		
		<asp:datalist runat="server" enableviewstate="True" showfooter="False" width="100%" id="dlMore" cellpadding="0" cellspacing="0" border="0" repeatcolumns="1" repeatdirection="Horizontal">
			<itemstyle horizontalalign="left" verticalalign="top" />
			
			<itemtemplate>

			<table width="100%" border="0" cellspacing="0" cellpadding="0">	
				<tr>
					<td align="left">
						
						<div class="framesize-small">
							<div class="passrequired" visible='<%# PassRequired (Convert.ToInt32 ( DataBinder.Eval(Container.DataItem, "access_type_id")))%>' runat="server" id="divPassRequired"></div>
							<div class="frame">
								<span class="ct"><span class="cl"></span></span>							
									<div class="imgconstrain">
										<a title='<%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "display_name").ToString ())%>' href='<%# GetWOKDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "global_id")))%>'>
											<img border="0" src='<%#GetWOKItemImageURL (DataBinder.Eval(Container.DataItem, "item_image_path").ToString () ,"me")%>' border="0" />
										</a>
									</div>																																				
								<span class="cb"><span class="cl"></span></span>
							</div>
						</div>
								
					</td>
					<td width="1"><img runat="server" src="~/images/spacer.gif" border="0" width="10" height="8" id="Img2"/></td>
					<td align="left" width="100%">
						
						<a title='<%# Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "display_name").ToString ())%>' href='<%# GetWOKDetailsLink (Convert.ToInt32 (DataBinder.Eval (Container.DataItem, "global_id")))%>'><%# TruncateWithEllipsis (Server.HtmlDecode (DataBinder.Eval (Container.DataItem, "display_name").ToString ()), 100) %></a>
						<br/>
						Views: <%# DataBinder.Eval(Container.DataItem, "views_count")%> | Raves: <%# DataBinder.Eval(Container.DataItem, "raves_count") %>
								
					</td>
				</tr>
			</table>
					
			<hr />			
				
			</itemtemplate>
		</asp:datalist>


		</td>
	</tr>
	<tr id="Tr1" runat="server" visible="false">
		<td>
			<table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0">
				<tr>
					<td width="7" class="frTopLeft"><img id="Img3" runat="server" src="~/images/spacer.gif" width="6" height="6" /></td>
					<td width="50%" class="frTop"><img id="Img4" runat="server" src="~/images/spacer.gif" width="6" height="6" /></td>
					<td width="6" class="frTopRight"><img id="Img5" runat="server" src="~/images/spacer.gif" width="6" height="6" /></td>
				</tr>
				<tr align="left" class="intColor1">
					<td class="intBorderC1BorderLeft1">&nbsp;</td>
					<td width="100%" class="intColor1">
						<table width="100%"  border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="3" height="3"><img id="Img6" runat="server" src="~/images/spacer.gif" width="3" height="3" /></td>
								<td width="3" height="3"><img id="Img7" runat="server" src="~/images/spacer.gif" width="3" height="3" /></td>
								<td width="3" height="3"><img id="Img8" runat="server" src="~/images/spacer.gif" width="3" height="3" /></td>
							</tr>
							<tr>
								<td width="3" height="3"><img id="Img9" runat="server" src="~/images/spacer.gif" width="3" height="3" /></td>
								<td width="100%" align="right"><span class="textGeneralGray03">Please encourage the owner to categorize this media.</span></td>
								<td width="3" height="3"><img id="Img10" runat="server" src="~/images/spacer.gif" width="3" height="3" /></td>
							</tr>
							<tr>
								<td width="3" height="3"><img id="Img11" runat="server" src="~/images/spacer.gif" width="3" height="3" /></td>
								<td width="3" height="3"><img id="Img12" runat="server" src="~/images/spacer.gif" width="3" height="3" /></td>
								<td width="3" height="3"><img id="Img13" runat="server" src="~/images/spacer.gif" width="3" height="3" /></td>
							</tr>
						</table>
					</td>
					<td class="intBorderC1BorderRight1">&nbsp;</td>
				</tr>
				<tr align="left" >
					<td class="intBorderC1BottomLeft"><img id="Img14" runat="server" src="~/images/spacer.gif" width="7" height="7" /></td>
					<td class="intBorderC1Bottom"><img id="Img15" runat="server" src="~/images/spacer.gif" width="7" height="7" /></td>
					<td class="intBorderC1BottomRight"><img id="Img16" runat="server" src="~/images/spacer.gif" width="7" height="7" /></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
				
					
</div>
<div id="related_footer"><span id="spnShowing" runat="server"/><a id="aAllResults" runat="server">See More Media</a></div>
																													
</ajax:ajaxpanel>

</div>