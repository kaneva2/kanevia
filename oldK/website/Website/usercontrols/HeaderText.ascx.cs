///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

namespace KlausEnt.KEP.Kaneva
{
	using System;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

	/// <summary>
	///		Summary description for HeaderText.
	/// </summary>
	public class HeaderText : System.Web.UI.UserControl
	{

		private void Page_Load(object sender, System.EventArgs e)
		{
			tdHeaderText.InnerText = Text;
		}

		/// <summary>
		/// The current Header Text
		/// </summary>
		public string Text
		{
			get 
			{
				return m_HeaderText;
			} 
			set
			{
				m_HeaderText = value;
			}
		}

		protected string m_HeaderText = "";
		protected HtmlTableCell tdHeaderText;

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
