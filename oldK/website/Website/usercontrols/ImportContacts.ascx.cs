///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CloudSponge;
using Newtonsoft.Json;
using Kaneva.BusinessLayer.Facade;
using Kaneva.BusinessLayer.BusinessObjects;
using log4net;

namespace KlausEnt.KEP.Kaneva.usercontrols
{
    public partial class ImportContacts : BaseUserControl
    {
        protected void Page_Load (object sender, EventArgs e)
        {
            if (!Request.IsAuthenticated)
            {
                Response.Redirect ("~/");
            }

            sitename.Value = "http://" + KanevaGlobals.SiteName + "/";

            ScriptManager.RegisterStartupScript (Page, Page.GetType (), "importscript", "<script type='text/javascript' src='" + ResolveUrl("~/jscript/import-contacts.js") + "'></script>", false);

            if (!IsPostBack)
            {
                if (Request["service"] != null && Request["service"].Length > 0)
                {
                    SetServiceType (Request["service"]);
                }

                if (ImportId > 0)
                {
                    LoadContacts (ImportId);
                }
                else
                {
                    LoadFriends ();
                }
            }
        }

        #region Helper Methods
        
        public void LoadContacts (int importId)
        {
            if (importId > 0)
            {
                ClearRepeaters ();
                List<Contact> memberContacts = new List<Contact> ();
                var emailContacts = Contacts (importId, out memberContacts);

                rptFriendsOnKaneva.Visible = false;
                if (memberContacts.Count > 0)
                {
                    rptFriendsOnKaneva.DataSource = memberContacts;
                    rptFriendsOnKaneva.DataBind ();
                    rptFriendsOnKaneva.Visible = true;

                    ShowFriendsAsMembersMessage(counter.KanevaFriends);

                    ShowStep (Step.Two);

                    ViewState.Add ("emailContacts", emailContacts);
                }
                else  // No contacts that are currently on Kaneva so skip step 2
                {
                    ShowContacts (emailContacts);
                }
            }
        }
        
        private void ShowContacts ()
        {
            ShowContacts (null);
        }
        
        private void ShowContacts (List<Contact> emailContacts)
        {
            if (emailContacts == null)
            {
                emailContacts = (List<Contact>) ViewState["emailContacts"];
            }

            if (emailContacts != null && emailContacts.Count > 0)
            {
                rptEmailContacts.DataSource = emailContacts;
                rptEmailContacts.DataBind ();
                rptEmailContacts.Visible = true;

                ShowStep (Step.Three);
            }
            else
            {
                ShowFeedbackMessage ();
                ShowStep (Step.One);
            }
        }
        
        protected void ShowFriendsAsMembersMessage (int count)
        {
            litFriendsAsMembersMessage.Text = "You have " + count + " " + ServiceType + " " +
                ContactsOrFriends + (count > 1 || count == 0 ? "s" : "") + 
                " already as " +
                (count > 1 ? "friends" : "a friend") + " in Kaneva.";
        }
        
        private void SendInvite (int currentUserId, string toEmailAddress, string toName)
        {
            // Generate the unique key code
            string keyCode = KanevaGlobals.GenerateUniqueString (20);

            // Are there to be any K-Points awarded?
            Double awardAmount = 0.0;
            string keiPointIdAward = "";

            System.Data.DataRow drInvitePointAward = UsersUtility.GetInvitePointAwards ();
            if (drInvitePointAward != null)
            {
                awardAmount = Convert.ToDouble (drInvitePointAward["point_award_amount"]);
                keiPointIdAward = drInvitePointAward["kei_point_id"].ToString ();
            }

            // Save the invites to the invites table
            int inviteID = UsersUtility.InsertInvite (currentUserId, toEmailAddress, toName, keyCode, awardAmount, keiPointIdAward, CommunityId(), GetInviteSource());
            if (inviteID == 0)
            {
                m_logger.Error ("Error inserting invite. UserId=" + currentUserId + ", ToEmail=" + toEmailAddress);
                return;
            }

            // Is it an invite from a community?
            if (CommunityId () > 0)
            {
                System.Data.DataRow drChannel = CommunityUtility.GetCommunity (CommunityId ());
                MailUtilityWeb.SendCommunityInvitation (currentUserId, CommunityId (), drChannel["name"].ToString (), toEmailAddress, toName, "", inviteID, keyCode);
            }
            else
            {
                MailUtilityWeb.SendInvitation (currentUserId, toEmailAddress, toName, "", inviteID, keyCode, Page);
            }
        }
        
        private void SendEmail (string emailAddress, string fromUsername)
        {
            // Generate the unique key code
            string keyCode = KanevaGlobals.GenerateUniqueString (20);

            // Are there to be any K-Points awarded?
            Double awardAmount = 0.0;
            string keiPointIdAward = "";

            DataRow drInvitePointAward = GetUserFacade.GetInvitePointAwards ((int) Constants.eINVITE_POINT_AWARD_TYPE.REGISTRATION);
            if (drInvitePointAward != null)
            {
                awardAmount = Convert.ToDouble (drInvitePointAward["point_award_amount"]);
                keiPointIdAward = drInvitePointAward["kei_point_id"].ToString ();
            }
            
            // Save the invites to the invites table
            int inviteID = UsersUtility.InsertInvite (KanevaWebGlobals.CurrentUser.UserId, emailAddress, keyCode, awardAmount, keiPointIdAward, GetInviteSource());
            if (inviteID == 0)
            {
                m_logger.Error ("Error inserting invite. UserId=" + KanevaWebGlobals.CurrentUser.UserId + ", ToEmail=" + emailAddress);
                return;
            }

            // Updated here for the new viral friends stuff.  Passing in the email address as the name also since we do not
            // know the name of the person we are sending the email to
            MailUtilityWeb.SendInvitation (KanevaWebGlobals.CurrentUser.UserId, emailAddress, emailAddress, txtMessage.Text, inviteID, keyCode, Page);
        }
        
        private void ShowFeedbackMessage ()
        {
            ShowFeedbackMessage (string.Empty, false);
        }
        private void ShowFeedbackMessage (string msg, bool isErr)
        {
            if (msg == string.Empty)
            {
                isErr = false;
                if (counter.FriendRequests > 0)
                {
                    msg = counter.FriendRequests.ToString () + " Kaneva Friend invite" + (counter.FriendRequests > 1 ? "s have" : " has") + " been sent";
                }
                else if (counter.Invites > 0)
                {
                    msg = counter.Invites.ToString () + " Kaneva invite" + (counter.Invites > 1 ? "s have" : " has") + " been sent";
                }
                else
                {
                    msg = "You do not have any contacts. Try a different service.";
                    isErr = true;
                }
            }

            if (msg != string.Empty)
            {
                ScriptManager.RegisterStartupScript (Page, Page.GetType (), "showmsg", "showMessage('" + msg + "'," + isErr.ToString().ToLower() + ");", true);
            }
        }
        
        private void SendFriendRequests ()  // Step 2
        {
            CheckBox chkAddFriend;
            Repeater currRepeater;
            if (IsFacebook)
            {
                currRepeater = rptFBFriendsOnKaneva;
            }
            else
            {
                currRepeater = rptFriendsOnKaneva;
            }

            foreach (RepeaterItem contact in currRepeater.Items)
            {
                try
                {
                    chkAddFriend = (CheckBox) contact.FindControl ("chkAddFriend");

                    if (chkAddFriend.Checked)
                    {
                        HiddenField hidUserId = (HiddenField) contact.FindControl ("hidUserId");
                        if (hidUserId != null)
                        {
                            int friendId = Convert.ToInt32 (hidUserId.Value);
                            if (AddFriend (KanevaWebGlobals.CurrentUser.UserId, friendId))
                            {
                                counter.FriendRequests++;
                            }
                        }
                    }
                }
                catch { }
            }
            if (counter.FriendRequests == 0)
            {
                counter.FriendRequests--;
            }
        }
        
        public bool AddFriend (int userId, int friendId)
        {
            // Can't friend yourself
            if (userId.Equals (friendId))
            {
                return false;
            }
            // Add them as a friend
            int ret = GetUserFacade.InsertFriendRequest (userId, friendId,
                Page.Request.CurrentExecutionFilePath, Global.RequestRabbitMQChannel());
            if (ret.Equals (0))	// already a friend
            { }
            else if (ret.Equals (2))  // pending friend request
            { }
            else if (ret.Equals (1))  // success
            {
                // send request email
                MailUtilityWeb.SendFriendRequestEmail (userId, friendId);
                return true;
            }
            return false;
        }
        
        private void ShowStep (Step step)
        {
            spnStep1.Visible = false;
            spnStep2.Visible = false;
            spnStep3.Visible = false;
            int stepNum = 1;

            if (step == Step.Two)
            {
                spnStep2.Visible = true;
                stepNum = 2;
            }
            else if (step == Step.Three)
            {
                spnStep3.Visible = true;
                stepNum = 3;
            }
            else
            {
                spnStep1.Visible = true;
                stepNum = 1;
            }

            ScriptManager.RegisterStartupScript (Page, Page.GetType (), "setstep" + stepNum, "step(" + stepNum + ");ScrollToTop();", true);
        }
        
        public void SetServiceType (string service)
        {
            switch (service.ToLower ())
            {
                case "aol":
                    ServiceType = ServiceTypes.AOL.Value;
                    break;
                case "facebook":
                    ServiceType = ServiceTypes.Facebook.Value;
                    break;
                case "gmail":
                    ServiceType = ServiceTypes.Gmail.Value;
                    break;
                case "windowslive":
                    ServiceType = ServiceTypes.Outlook.Value;
                    break;
                case "yahoo":
                    ServiceType = ServiceTypes.Yahoo.Value;
                    break;
                case "manual":
                    ServiceType = ServiceTypes.Manual.Value;
                    break;
                default:
                    ServiceType = ServiceTypes.None.Value;
                    break;
            }
        }
        
        private List<CloudSponge.Contact> Contacts (int importId, out List<Contact> memberContacts)
        {
            memberContacts = new List<Contact> ();
            try
            {
                if (KanevaWebGlobals.CurrentUser.UserId > 0)
                {
                    if (importId > 0)
                    {
                        var api = new Api (KanevaGlobals.CloudSpongeDomainKey, KanevaGlobals.CloudSpongePassword);
                        var contacts = api.Contacts (importId);

                        // Pull out all the email address properties
                        var emails = from items in contacts.Contacts select items.EmailAddresses;

                        // Get all emails and put into a string list
                        List<string> listEmails = new List<string> ();
                        foreach (var email in emails)
                        {
                            foreach (var e in email)
                            {
                                listEmails.Add (email.FirstOrDefault ());
                            }
                        }

                        // Get any users that are already members
                        PagedList<User> users = new UserFacade ().GetUsersByEmail (listEmails);

                        var emailContacts = contacts.Contacts.ToList ();


                        // For each member, set the userid value and put them at top of list
                        foreach (User u in users)
                        {
                            // Find the user in contact list
                            var c = emailContacts.First (a => a.EmailAddresses.FirstOrDefault () == u.Email);
                            c.UserId = u.UserId;
                            c.Username = u.Username;
                            c.ThumbnailPath = u.ThumbnailSquarePath;
                            c.Gender = u.Gender;
                            c.UseFacebookProfilePicture = u.FacebookSettings.UseFacebookProfilePicture;
                            c.FacebookUserId = u.FacebookSettings.FacebookUserId;
                            emailContacts.Remove (c);

                            if (GetUserFacade.AreFriends (Kaneva.KanevaWebGlobals.CurrentUser.UserId, u.UserId))
                            {
                                // incrememnt counter of contacts user is already friends with on Kaneva
                                counter.KanevaFriends++;
                            }
                            else
                            {
                                memberContacts.Insert (0, c);
                            }
                        }

                        // return JsonConvert.SerializeObject (emailContacts);
                        return emailContacts;
                    }
                }
            }
            catch { }
            return null;
        }

        private int GetInviteSource ()
        {
            switch (ServiceType)
            {
                case "AOL":
                    return (int) eINVITE_SOURCE_TYPE.AOL;
                case "Facebook":
                    return (int) eINVITE_SOURCE_TYPE.Facebook;
                case "Gmail":
                    return (int) eINVITE_SOURCE_TYPE.Gmail;
                case "Outlook.com":
                    return (int) eINVITE_SOURCE_TYPE.Outlook;
                case "Yahoo":
                    return (int) eINVITE_SOURCE_TYPE.Yahoo;
                default:
                    return (int) eINVITE_SOURCE_TYPE.Manual;
            }
        }

        private void ClearRepeaters ()
        {
            rptEmailContacts.Visible = false;
            rptFBFriends.Visible = false;
            rptFBFriendsOnKaneva.Visible = false;
            rptFriendsOnKaneva.Visible = false;
        }

        #endregion Helper Methods


        #region Event Handlers
        
        protected void btnAddFriends_Click (object sender, EventArgs e)
        {
            SendFriendRequests ();

            if (IsFacebook)
            {
                ShowFacebookUsersToInvite ();
            }
            else
            {
                ShowContacts ();
            }

            // This check is if the user selected the Add Friends button
            // but none of the users in the list were selected
            if (counter.FriendRequests > 0)
            {
                ShowFeedbackMessage ();
            }
        }
        
        protected void btnSkip_Click (object sender, EventArgs e)
        {
            ScriptManager.RegisterClientScriptBlock (Page, Page.GetType (), "showmsg", "showMessage('')", true);
            if (spnStep3.Visible)
            {
                if (KanevaWebGlobals.CurrentUser.FacebookSettings.FacebookUserId > 0)
                {
                    if (ServiceType == ServiceTypes.Facebook.Value)
                    {
                        ShowStep (Step.One);
                    }
                    else
                    {
                        LoadFriends ();
                    }
                }
                else
                {
                    ShowStep (Step.One);
                }
            }
            else
            {
                if (ServiceType == ServiceTypes.Facebook.Value)
                {
                    ShowFacebookUsersToInvite ();
                }
                else
                {
                    ShowContacts ();
                }
            }
        }
        
        protected void btnSendEmail_Click (object sender, EventArgs e)
        {
            spnAlertMsg.InnerText = string.Empty;
            spnAlertMsg.Style.Add ("display", "none");

            if (txtTo.Text.Trim () == string.Empty)
            {
                spnAlertMsg.InnerText = "Entering an email address is required to continue.";
                spnAlertMsg.Style.Add ("display", "block");
                return;
            }

            // Check to make sure user did not enter any "potty mouth" words
            if (KanevaWebGlobals.isTextRestricted (txtMessage.Text, Constants.eRESTRICTION_TYPE.POTTY_MOUTH))
            {
                spnAlertMsg.InnerText = Constants.VALIDATION_REGEX_RESTRICTED_WORD_MESSAGE;
                spnAlertMsg.Style.Add ("display", "block");
                return;
            }

            System.Text.RegularExpressions.Regex regexEmail = new System.Text.RegularExpressions.Regex (Constants.VALIDATION_REGEX_EMAIL);

            // Validate the email addresses
            //parse the email addresses using spaces, comman and semicolons
            string[] emailAddresses = txtTo.Text.Trim ().Split (';');
            ArrayList emailList = new ArrayList ();
            foreach (string s in emailAddresses)
            {
                string[] list = s.Trim ().Split (' ');
                foreach (string s2 in list)
                {
                    string[] list2 = s2.Trim ().Split (',');
                    foreach (string s3 in list2)
                    {
                        if (s3.Trim () != string.Empty)
                            emailList.Add (s3);
                    }
                }
            }

            ArrayList emailListMember = new ArrayList ();

            //foreach (string toEmail in emailList)
            for (int i = emailList.Count - 1; i >= 0; i--)
            {
                string sToEmail = (string) emailList[i];

                // Make sure it is a valid type
                if (!regexEmail.IsMatch (sToEmail))
                {
                    spnAlertMsg.InnerText = "Invalid email address '" + sToEmail + "'.";
                    spnAlertMsg.Style.Add ("display", "block");
                    return;
                }

                // Make sure the email does not already exist as a registered user
                if (GetUserFacade.EmailExists (sToEmail))
                {
                    // 0002538: from CK - Friend center - Allow to skip already e-mails in system and send to everyone ... 
                    //errMessage += "The email address \\'" + toEmail + "\\' is already associated with a user, please select another address.\\n";
                    emailListMember.Add (emailList[i]);
                    emailList.RemoveAt (i);
                }
            }

            int countNotSent = 0;

            if (emailList.Count > 0)
            {
               // Send out the emails
                for (int i = emailList.Count - 1; i >= 0; i--)
                {
                    string toEmail = emailList[i].ToString ();

                    // check to see if user can send another invite to this person
                    DataRow drUser = UsersUtility.GetInvite (toEmail, KanevaWebGlobals.CurrentUser.UserId);

                    if ((drUser != null) && (Convert.ToDateTime (drUser["reinvite_date"]).AddDays (14) > DateTime.Now))
                    {
                        emailList.RemoveAt (i);
                        countNotSent++;
                    }
                    else
                    {
                        ServiceType = ServiceTypes.Manual.Value;
                        SendEmail (toEmail, KanevaWebGlobals.CurrentUser.Username);
                        counter.Invites++;
                    }
                }

                if (counter.Invites > 0)
                {
                    ShowFeedbackMessage ();
                    // Clear out emails and show default text
                    txtTo.Text = "";
                }
            }
            else
            {    
                spnAlertMsg.InnerText = "No emails sent because all email addresses belong to current Kaneva members.";
                spnAlertMsg.Style.Add ("display", "block");
            }
        }

        #endregion Event Handlers


        #region SendInvites Async Code Section
        /// <summary>
        /// Click event fired for btnSendInvites
        /// </summary>
        /// <returns></returns>
        protected void btnSendInvites_Click (object sender, EventArgs e)
        {
            try
            {
                foreach (RepeaterItem contact in rptEmailContacts.Items)
                {
                    CheckBox chkInvite = (CheckBox) contact.FindControl ("chkInvite");
                    if (chkInvite != null)
                    {
                        if (chkInvite.Checked)
                        {
                            counter.Invites++;
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                m_logger.Error ("Error Sending Invites. " + exc);
            }

            // Only start process if items have been selected in list
            if (counter.Invites > 0)
            {
                ViewState["tempUserID"] = KanevaWebGlobals.CurrentUser.UserId;
                PageAsyncTask patSendInvite = new PageAsyncTask (BeginAsyncSendInvites, EndAsyncSendInvites, TimeoutSendInvites, "tskSendInvite", false);
                Page.RegisterAsyncTask (patSendInvite);
                Page.ExecuteRegisteredAsyncTasks ();
                ShowFeedbackMessage ();
            }

            ShowStep (Step.One);
        }

        IAsyncResult BeginAsyncSendInvites (Object src, EventArgs args, AsyncCallback cb, Object state)
        {
            _dlgtSendInvites = new AsyncTaskDelegate (SendInvites);
            IAsyncResult result = _dlgtSendInvites.BeginInvoke (cb, state);
            return result;
        }

        void TimeoutSendInvites (IAsyncResult ar)
        {
            m_logger.Error ("Timeout sending email");
        }

        void EndAsyncSendInvites (IAsyncResult ar)
        {
            _dlgtSendInvites.EndInvoke (ar);
            m_logger.Debug ("Done Sending");
        }

        public void SendInvites ()
        {
            CheckBox chkInvite;

            int currentUserId = 0;
            if (ViewState["tempUserID"] != null)
            {
                currentUserId = (int) ViewState["tempUserID"];
            }

            // Record that they've sent invites
            Response.Cookies["invsent"].Value = "true";
            Response.Cookies["invsent"].Expires = DateTime.Now.AddHours (4);

            // send invite or friend request to each contact that is checked
            // if email does not belong to Kaneva account, send invite
            // if email does belong to Kaneva account, send friend request
            if (currentUserId > 0)
            {
                foreach (RepeaterItem contact in rptEmailContacts.Items)
                {
                    try
                    {
                        chkInvite = (CheckBox) contact.FindControl ("chkInvite");

                        if (chkInvite.Checked)
                        {
                            HiddenField hidContactName = (HiddenField) contact.FindControl ("hidContactName");
                            HiddenField hidContactEmail = (HiddenField) contact.FindControl ("hidContactEmail");

                            if (hidContactEmail != null)
                            {
                                string contactEmail = hidContactEmail.Value.Trim ();

                                if (contactEmail.Length > 0)
                                {
                                    System.Data.DataRow drUser = null;

                                    try
                                    {
                                        // check if there is an invite for this user already 
                                        drUser = UsersUtility.GetInvite (contactEmail, currentUserId);
                                    }
                                    catch (Exception exc)
                                    {
                                        m_logger.Error ("Error Getting Invite. User=" + currentUserId + " - ContactEmail=" + contactEmail, exc);
                                    }

                                    if ((drUser != null) && (Convert.ToDateTime (drUser["reinvite_date"]).AddDays (14) > DateTime.Now))
                                    {
                                        // Do nothing
                                        m_logger.Info ("Already invited by this user");
                                    }
                                    else
                                    {
                                        if (hidContactName != null)
                                        {
                                            // send invite
                                            SendInvite (currentUserId, contactEmail, hidContactName.Value.Trim ());
                                        }
                                    }

                                }
                            }
                        }
                    }
                    catch (Exception exc2)
                    {
                        m_logger.Error ("Error occured processing repeater information.", exc2);
                    }
                }
            }
            else
            {
                m_logger.Error ("Error getting user id from ViewState. UserId could not be retreived.");    
            }
        }
        #endregion Send Invites Async Code Section


        #region FACEBOOK

        #region Helper Methods

        public void LoadFriends ()
        {
            // If user is not connected, show Find Friends button
            if (KanevaWebGlobals.CurrentUser.FacebookSettings.FacebookUserId == 0 || ServiceType == ServiceTypes.None.Value)
            {
                // Don't show any Facebook info, show Step 1
            }
            else // If user is connected but not logged in, show facebook login button
            {
                ClearRepeaters ();

                ServiceType = ServiceTypes.Facebook.Value;

                if (rptFBFriends.Items == null || rptFBFriends.Items.Count == 0)
                {
                    bool isError = false;
                    string errMsg = string.Empty;
                    string ids = string.Empty;

                    FacebookFriendList friends = GetSocialFacade.GetCurrentUsersFriends (KanevaWebGlobals.CurrentUser.FacebookSettings.AccessToken, out isError, out errMsg);

                    if (isError)
                    {
                        if (errMsg.ToLower ().Contains ("oauthexception"))
                        {
                            // Auth error so show Step 1 and user can select Facebook icon again to reauthorize
                            return;
                        }

                        // Show error to user 
                        ShowFeedbackMessage ("We could not retrieve your Facebook friends list.  Please try again later.", true);
                        m_logger.Error ("Error retreiving users friends list from Facebook. --> " + errMsg);
                        return;
                    }

                    // If user is connected, show friends lists
                    // Check to see if any of these Facebook Id's are connected to Kaneva accounts
                    if (friends.data.Count > 0)
                    {
                        foreach (FacebookUser fbuser in friends.data)
                        {
                            ids += fbuser.Id.ToString () + ",";
                        }
                    }

                    // If user has facebook friends that were retrieved
                    if (ids.Length > 0)
                    {
                        // Remove the last comma
                        ids = ids.Remove (ids.Length - 1, 1);

                        // Get any Facebook friends that have accounts connected to their Facebook account 
                        // and the current user is not already friends with on Kaneva
                        PagedList<User> fbFriends = GetUserFacade.GetUsersByFacebookUserId (KanevaWebGlobals.CurrentUser.UserId, ids, "", 1, 5000);

                        // If Facebook friends exist on Kaneva, show that list
                        if (fbFriends.Count > 0)
                        {
                            // Show the list of friends that are already on Kaneva and invite the user to add them as friends
                            rptFBFriendsOnKaneva.DataSource = fbFriends;
                            rptFBFriendsOnKaneva.DataBind ();
                            rptFBFriendsOnKaneva.Visible = true;

                            ShowFriendsAsMembersMessage(fbFriends.Count);
                            
                            ShowStep (Step.Two);

                            // Store list of user ids in ViewState so we can use if they want to send to all
                            IList<Int32> uids = new List<Int32> ();
                            foreach (User user in fbFriends)
                            {
                                uids.Add (user.UserId);
                            }
                            this.KanevaUserIds = uids;

                            // Get the FB userids from friends list that exist on Kaneva
                            IList<UInt64> fbFriendsOnKanevaUserIds = GetUserFacade.GetFacebookUserIdForConnectedUsers (ids);
                            foreach (UInt64 userid in fbFriendsOnKanevaUserIds)
                            {
                                // Since user is already on Kaneva, do not show them in the invite to Kaneva list
                                friends.data.RemoveAll (delegate (FacebookUser fbu) { return fbu.Id == userid; });
                            }

                            // Store the list of Facebook friends not on Kaneva so we will have it on the next step
                            this.FacebookUserIds = ids;
                        }
                        else
                        {
                            if (friends.data.Count > 0)
                            {
                                rptFBFriends.DataSource = friends.data;
                                rptFBFriends.DataBind ();
                                rptFBFriends.Visible = true;

                                // Prep UI for Facebook list
                                inviteCheckAll.Visible = false;
                                btnSendInvites.Visible = false;

                                ShowStep (Step.Three);
                            }
                            else
                            {
                                // show no friends msg
                                ShowFeedbackMessage ("None of your Facebook friends are users on Kaneva.", true);
                            }
                        }
                    }
                    else
                    {   // NO Facebook Friends at all
                        ShowFeedbackMessage ("We could not retrieve any of your Facebook friends.", true);
                    }
                }
            }
        }
        
        private void ShowFacebookUsersToInvite ()
        {
            bool isError = false;
            string errMsg = string.Empty;
            ClearRepeaters ();

            FacebookFriendList friends = GetSocialFacade.GetCurrentUsersFriends (KanevaWebGlobals.CurrentUser.FacebookSettings.AccessToken, out isError, out errMsg);

            if (isError)
            {
                if (errMsg.ToLower ().Contains ("oauthexception"))
                {
                    // Auth error so show Step 1 and user can select Facebook icon again to reauthorize
                    return;
                }

                ShowFeedbackMessage ("We could not retrieve your Facebook friends list.  Please try again later.", true);
                m_logger.Error ("Error retreiving users friends list from Facebook. --> " + errMsg);
            }

            if (friends.data.Count > 0)
            {
                // Get the FB userids from friends list that exist on Kaneva
                IList<UInt64> fbFriendsOnKanevaUserIds = GetUserFacade.GetFacebookUserIdForConnectedUsers (this.FacebookUserIds);
                foreach (UInt64 userid in fbFriendsOnKanevaUserIds)
                {
                    // Since user is already on Kaneva, do not show them in the invite to Kaneva list
                    friends.data.RemoveAll (delegate (FacebookUser fbu) { return fbu.Id == userid; });
                }

                // Check again to make sure all of users facebook friends are not on Kaneva
                if (friends.data.Count > 0)
                {
                    rptFBFriends.DataSource = friends.data;
                    rptFBFriends.DataBind ();
                    rptFBFriends.Visible = true;

                    // Prep UI for Facebook list
                    inviteCheckAll.Visible = false;
                    msgSendInvitesTop.Visible = false;
                    msgSendInvitesBottom.Visible = false;
                    btnSendInvites.Visible = false;

                    ShowStep (Step.Three);
                }
                else
                {
                    // show no friends msg
                    ShowFeedbackMessage ("We could not find retrieve any of your Facebook friends.", true);
                }
            }
            else
            {
                // show no friends msg
                ShowFeedbackMessage ("We could not find retrieve any of your Facebook friends.", true);
            }
        }
        
        protected string GetUserLocale (string isoCC)
        {
            try
            {
                if (isoCC.Length > 0)
                {
                    Country country = WebCache.GetCountry (isoCC.Substring (isoCC.IndexOf ("_") + 1));

                    if (country != null && country.Name.Length > 0)
                    {
                        return country.Name;
                    }
                }
                return "";
            }
            catch { return ""; }
        }


        #endregion Helper Methods

        #region Properties

        private IList<Int32> KanevaUserIds
        {
            get
            {
                try
                {
                    if (ViewState["KanUIds"] != null)
                    {
                        return (IList<Int32>) ViewState["KanUIds"];
                    }
                }
                catch { }
                return new List<Int32> ();
            }
            set { ViewState["KanUIds"] = value; }
        }

        private string FacebookUserIds
        {
            get
            {
                try
                {
                    if (_fbUserIds == "")
                    {
                        if (ViewState["FBUIds"] != null)
                        {
                            return ViewState["FBUIds"].ToString ();
                        }
                    }
                    return _fbUserIds;
                }
                catch { }
                return "";
            }
            set { ViewState["FBUIds"] = value; }
        }

        public enum FBSection
        {
            Login,
            Connect,
            List
        }

        public string CurrentUser_UserId ()
        {
            return KanevaWebGlobals.CurrentUser.UserId.ToString ();
        }

        #endregion Properties

        #region Declearations

        private static readonly ILog m_logger = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);

        #endregion Declerations

        #endregion FACEBOOK


        #region Properties

        public enum Step
        {
            One,
            Two,
            Three
        }
        private class Counts
        {
            private int _friendRequests = 0;
            private int _invites = 0;
            private int _kanevaFriends = 0;

            public int FriendRequests
            {
                get { return _friendRequests; }
                set { _friendRequests = value; }
            }

            public int Invites
            {
                get { return _invites; }
                set { _invites = value; }
            }

            public int KanevaFriends
            {
                get { return _kanevaFriends; }
                set { _kanevaFriends = value; }
            }

        }

        public class ServiceTypes
        {
            private ServiceTypes (string value) { Value = value; }

            public string Value { get; set; }

            public static ServiceTypes AOL { get { return new ServiceTypes ("AOL"); } }
            public static ServiceTypes Facebook { get { return new ServiceTypes ("Facebook"); } }
            public static ServiceTypes Gmail { get { return new ServiceTypes ("Gmail"); } }
            public static ServiceTypes Outlook { get { return new ServiceTypes ("Outlook.com"); } }
            public static ServiceTypes Yahoo { get { return new ServiceTypes ("Yahoo"); } }
            public static ServiceTypes None { get { return new ServiceTypes ("none"); } }
            public static ServiceTypes Manual { get { return new ServiceTypes ("manual"); } }
        }
        
        public string ServiceType
        {
            get
            {
                if (_serviceType == string.Empty)
                {
                    if (ViewState["ServiceType"] != null)
                    {
                        _serviceType = ViewState["ServiceType"].ToString();   
                    }
                }
                return _serviceType;
            }
            set
            {
                _serviceType = value;
                ViewState["ServiceType"] = _serviceType;
            }
        }

        protected string ContactsOrFriends
        {
            get
            {
                if (ServiceType == "Facebook")
                {
                    return "friend";
                }
                return "contact";
            }
        }

        /// <summary>
        /// GetCommunityId
        /// </summary>
        /// <returns></returns>
        private int CommunityId ()
        {
            if (Request["communityId"] != null)
            {
                return Convert.ToInt32 (Request["communityId"]);
            }

            return 0;
        }
        private int ImportId
        {
            get
            {
                if (Request["id"] != null && Request["id"].Length > 0)
                {
                    _importId = Convert.ToInt32 (Request["id"]);
                }
                return _importId;
            }
        }

        private bool IsFacebook
        {
            get { return ServiceType == ServiceTypes.Facebook.Value; }
        }

        #endregion Properties


        #region Declerations

        private string _serviceType = "";
        private string _fbUserIds = "";
        private AsyncTaskDelegate _dlgtSendInvites;
        protected delegate void AsyncTaskDelegate ();
        private int _importId = 0;
        private Counts counter = new Counts ();

        #endregion Declerations
    }
}