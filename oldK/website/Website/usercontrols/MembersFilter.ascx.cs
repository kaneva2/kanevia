///////////////////////////////////////////////////////////////////////////////
// Copyright Kaneva 2001-2020
// This file is licensed under the PolyForm Noncommercial License 1.0.0
// https://polyformproject.org/licenses/noncommercial/1.0.0/ 
///////////////////////////////////////////////////////////////////////////////

using Kaneva.BusinessLayer.BusinessObjects;

namespace KlausEnt.KEP.Kaneva
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

	/// <summary>
	///		Summary description for MembersFilter.
	/// </summary>
	public class MembersFilter : System.Web.UI.UserControl
	{

		private void Page_Load(object sender, System.EventArgs e)
		{
			if (!IsPostBack)
			{
				SetupFilter ();
				drpStatus.Enabled = StatusEnabled;
				drpRole.Enabled = RolesEnabled;
			}
		}

		private void SetupFilter ()
		{
			// Role
			drpRole.Items.Add (new ListItem ("role...", ""));
			drpRole.Items.Add (new ListItem ("----------", " "));
			drpRole.Items.Add (new ListItem ("owner", ((int) CommunityMember.CommunityMemberAccountType.OWNER).ToString ()));
			drpRole.Items.Add (new ListItem ("members", ((int) CommunityMember.CommunityMemberAccountType.SUBSCRIBER).ToString ()));
			drpRole.Items.Add (new ListItem ("moderators", ((int) CommunityMember.CommunityMemberAccountType.MODERATOR).ToString ()));

			// Status
			drpStatus.Items.Add (new ListItem ("status...", ""));
			drpStatus.Items.Add (new ListItem ("----------", " "));
			drpStatus.Items.Add (new ListItem ("active", ((UInt32) CommunityMember.CommunityMemberStatus.ACTIVE).ToString ()));
			drpStatus.Items.Add (new ListItem ("suspended", ((UInt32) CommunityMember.CommunityMemberStatus.LOCKED).ToString ()));
			//drpStatus.Items.Add (new ListItem ("pending", ((UInt32) CommunityMember.CommunityMemberStatus.PENDING).ToString ()));
			
			// Pages
			drpPages.Items.Add (new ListItem ("4", "4"));
			drpPages.Items.Add (new ListItem ("8", "8"));
			drpPages.Items.Add (new ListItem ("20", "20"));
			drpPages.Items.Add (new ListItem ("40", "40"));
			drpPages.Items.Add (new ListItem ("80", "80"));
			drpPages.Items.Add (new ListItem ("100", "100"));

			try
			{
				drpPages.SelectedValue = InitialItemsPerPage.ToString ();
			}
			catch (Exception)
			{}
		}

		/// <summary>
		/// Change Members filter
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void drpMembers_Change (Object sender, EventArgs e)
		{
			drpRole.SelectedIndex = 0;
			drpStatus.SelectedIndex = 0;
			if (drpMembers.SelectedValue.Trim ().Length > 0)
			{
				// Make sure it is one of the valid letter to protect against SQL injection
				string filter = drpMembers.SelectedValue.Substring (0,1);
				System.Text.RegularExpressions.Regex regexSingleLetter = new System.Text.RegularExpressions.Regex ("[a-zA-Z]");
				if (regexSingleLetter.IsMatch (filter))
				{
					CurrentFilter = " u.username like '" + Server.HtmlEncode (filter) + "%'";
				}
			}
			else
			{
				CurrentFilter = "";
			}

			FilterChanged (this, new FilterChangedEventArgs (CurrentFilter, CurrentSort));
		}

		/// <summary>
		/// Change Role
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void drpRole_Change (Object sender, EventArgs e)
		{
			drpMembers.SelectedIndex = 0;
			drpStatus.SelectedIndex = 0;
			if (drpRole.SelectedValue.Trim ().Length > 0)
			{
				CurrentFilter = " cm.account_type_id = " + Convert.ToInt32 (Server.HtmlEncode (drpRole.SelectedValue));
			}
			else
			{
				CurrentFilter = "";
			}
			

			FilterChanged (this, new FilterChangedEventArgs (CurrentFilter, CurrentSort));
		}

		/// <summary>
		/// Change Status
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void drpStatus_Change (Object sender, EventArgs e)
		{
			drpMembers.SelectedIndex = 0;
			drpRole.SelectedIndex = 0;
			if (drpStatus.SelectedValue.Trim ().Length > 0)
			{
			CurrentFilter = " cm.status_id = " + Convert.ToInt32 (Server.HtmlEncode (drpStatus.SelectedValue));
			}
			else
			{
				CurrentFilter = "";
			}

			FilterChanged (this, new FilterChangedEventArgs (CurrentFilter, CurrentSort));

		}


		/// <summary>
		/// Change Pages
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void drpPages_Change (Object sender, EventArgs e)
		{
			FilterChanged (this, new FilterChangedEventArgs (CurrentFilter, CurrentSort));
		}

		/// <summary>
		/// The current filter
		/// </summary>
		public string CurrentFilter
		{
			get 
			{
				if (ViewState ["filter"] != null)
				{
					return ViewState ["filter"].ToString ();
				}
				else
				{
					return "";
				}
			} 
			set
			{
				ViewState ["filter"] = value;
			}
		}

		/// <summary>
		/// The current Sort
		/// </summary>
		public string CurrentSort
		{
			get 
			{
				if (ViewState ["sort"] != null)
				{
					return ViewState ["sort"].ToString ();
				}
				else
				{
					return DEFAULT_MEMBER_SORT;
				}
			} 
			set
			{
				ViewState ["sort"] = value;
			}
		}

		/// <summary>
		/// The ItemsPerPages to display
		/// </summary>
		public int ItemsPerPages
		{
			get 
			{
				if (drpPages.SelectedValue.Length > 0)
				{
					return Convert.ToInt32 (drpPages.SelectedValue);
				}
				else
				{
					return InitialItemsPerPage;
				}
			} 
		}

		/// <summary>
		/// The Initial items to display per page
		/// </summary>
		public int InitialItemsPerPage
		{
			get 
			{
				if (ViewState ["IIPP"] != null)
				{
					return (int) ViewState ["IIPP"];
				}
				else
				{
					return KanevaGlobals.MembersPerPage;
				}
			} 
			set
			{
				ViewState ["IIPP"] = value;
			}
		}

		/// <summary>
		/// The RolesEnabled to display
		/// </summary>
		public bool RolesEnabled
		{
			get 
			{
				if (ViewState ["RolesEnabled"] != null)
				{
					return (bool) ViewState ["RolesEnabled"];
				}
				else
				{
					return true;
				}
			} 
			set
			{
				ViewState ["RolesEnabled"] = value;
			}
		}

		/// <summary>
		/// The StatusEnabled to display
		/// </summary>
		public bool StatusEnabled
		{
			get 
			{
				if (ViewState ["StatusEnabled"] != null)
				{
					return (bool) ViewState ["StatusEnabled"];
				}
				else
				{
					return true;
				}
			} 
			set
			{
				ViewState ["StatusEnabled"] = value;
			}
		}

		private const string DEFAULT_MEMBER_SORT = "c.name";

		public event FilterChangedEventHandler FilterChanged;

		protected DropDownList drpMembers, drpRole, drpStatus, drpPages;

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
