﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContestWidget.ascx.cs" Inherits="KlausEnt.KEP.Kaneva.usercontrols.ContestWidget" %>

<div id="contest_widget" runat="server" clientidmode="Static">
	
	<div class="widget" runat="server">
		<h2 id="title" runat="server">My Contests</h2>
		<asp:repeater id="rptWidgetContests" runat="server" onitemdatabound="rptWidgetContests_ItemDataBound">
			<itemtemplate>
				<div class="row">
					<div class="pts">
						<div class="amt"><%# DataBinder.Eval (Container.DataItem, "PrizeAmount") %><br /><span>CREDITS</span></div>
						<img id="imgFollow" src="~/images/icons/icon_follow_18x15.png" runat="server" visible="false" title="You are Following this contest." />
					</div>
					<div class="info">
						<p class="title"><a href="javascript:void(0);" onclick="ShowDetails(<%# DataBinder.Eval (Container.DataItem, "ContestId") %>);" title='<%# DataBinder.Eval (Container.DataItem, "Title") %>'><%# TruncateWithEllipsis (Server.HtmlDecode(DataBinder.Eval (Container.DataItem, "Title").ToString()), 33) %></a></p>
						<div class="entries"><%# DataBinder.Eval (Container.DataItem, "NumberOfEntries") %> entries</div>
						<div class="votes"><%# DataBinder.Eval (Container.DataItem, "NumberOfVotes") %> votes</div>
						<div id="divTimeRemaining" runat="server" class="remaining"><%# DataBinder.Eval (Container.DataItem, "TimeRemaining") %> left</div>
					</div>
				</div>
			</itemtemplate>
			<separatortemplate>
				<div class="divide">
					<p class="top"></p>
					<p class="btm"></p>
				</div>
			</separatortemplate>
		</asp:repeater>
	</div>
	
	<div class="nodata" id="divNoData" runat="server" visible="false">No Data Found</div>	

</div>
