@ECHO OFF

SETLOCAL EnableDelayedExpansion

SET ThisDir=%~dp0
SET BaseDir=%ThisDir%..\..
SET ScriptsDir=%BaseDir%\BuildScripts


SET ProjectName=TinyXML
SET SolutionToBuild="%ThisDir%tinyxml_vs2015.sln"
rem SET DebugConfiguration="Debug"
rem SET ReleaseConfiguration="Release"

call %ScriptsDir%\BuildSolution.cmd %1

IF %CopyFiles% == 1 (
	call %ScriptsDir%\xcopy.cmd %ThisDir%tinystr.h %IncludeDir%\TinyXML\tinystr.h* /I /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%tinyxml.h %IncludeDir%\TinyXML\tinyxml.h* /I /Y

	call %ScriptsDir%\xcopy.cmd %ThisDir%debug_STL\tinyxmld_STL.lib %LibDir%\tinyxmld_STL.lib* /I /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%debug_STL\tinyxmld_stl.pdb %LibDir%\tinyxmld_stl.pdb* /I /Y
	
	call %ScriptsDir%\xcopy.cmd %ThisDir%release_STL\tinyxml_STL.lib %LibDir%\tinyxml_STL.lib* /I /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%release_STL\tinyxml_stl.pdb %LibDir%\tinyxml_stl.pdb* /I /Y
)
