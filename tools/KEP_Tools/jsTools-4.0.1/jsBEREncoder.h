#ifndef __JSBERENCODER_H__
#define __JSBERENCODER_H__
/******************************************************************************
 Name: jsBEREncoder.h

 Description: encoder class 

 Copyright (C) 1996, 2004 Jim Wallace

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 History:
 2/23/99		jmw created

 Notes: 

******************************************************************************/
#ifndef _js_h_
	#error Include js.h before jsBEREncoder.h
#endif

#include <vector>

#include "jsBERDecoder.h"

/**********************************************************************
 Description:  PatchLength object used internally by jsBEREncoder
	for keeping track of nested constructed objects
**********************************************************************/
class JSEXPORT PatchLength
{
public:
	PatchLength( jsBERTag tag = DONT_CARE_TAG, ULONG offset = 0, BYTE lenType = INDEFINITE_LENGTH ) : 
					m_offset( offset ), m_lenType( lenType ), m_tag(tag) {}

	PatchLength( const PatchLength &src ) { operator=( src ); }

	PatchLength &operator=( const PatchLength &src ) 
		{ m_offset = src.offset(); m_lenType = src.lenType(); m_tag = src.m_tag; return *this; }

	ULONG				offset() const { return m_offset; }
	BYTE				lenType() const { return m_lenType; }
	jsBERTag			tag() const { return m_tag; }

	int lenLength() const 
	{
		// return the number of bytes for a length, not an integer
		if ( lenType() == ONE_BYTE_LENGTH )
			return 1;
		else
			return ( (LENGTH_MASK & lenType()) + 1 );
	}

	// need for vector when building DLL
	inline bool operator<( const PatchLength &src ) const { return m_offset < src.m_offset; }
	inline bool operator==( const PatchLength &src ) const { return m_offset == src.m_offset && m_lenType == src.m_lenType && m_tag == src.m_tag; }

private:
	ULONG				m_offset;	// pointer to length
	BYTE				m_lenType;	// what type of length it is
	jsBERTag			m_tag;		// tag where encoding -- used for checking
};

typedef BYTE *(*jsReallocFn)(BYTE* ptr,ULONG extraParm, ULONG size);

// for warning about extern in below
#ifdef _MSC_VER
#pragma warning (disable : 4231 ) 

// Instantiate classes vector<PatchLength> 
// This doesn't create an object. It only forces the generation of all
// of the members of classes vector<PatchLength>. It exports
// them from the DLL and imports them into the EXE.
JSEXPIMP_TEMPLATE template class JSEXPORT std::vector<PatchLength>;

#pragma warning (default : 4231 ) 
#endif



/******************************************************************************
CLASS     
    jsBEREncoder

    Class used for encoding and decoding BER data

DESCRIPTION
    This class is used for encode data BER format into a buffer.  You may 
	make explicit calls or shift simple types into them.  It supports using
	some of the more complicated BER constructed such as SETs, and constructed types.
	
SIMPLE EXAMPLE
	This first sample shows the encoding of a typical encoding of a request 
	that has some nested constructions.  Note the indenting to indicate the
	nesting of BER constructs

    EXAMPLE

        m_coder.beginConstructed( SEQUENCE );

            m_coder.encodeInteger( msgId );

            m_coder.beginConstructed( m_requestTag ); 

                // call virt fn to encode the main stuff
                rc = encodeRequest( msgId );

            m_coder.endConstructed( m_requestTag ); 

        m_coder.endConstructed( SEQUENCE );
    END

MORE COMPLICATED EXAMPLE
	This is taken from the search filter encoding.
  
    EXAMPLE
    coder->beginConstructed( SUBSTRING_CONTEXT_TAG, jsBEREncoder::calcLengthType( totlen ) );

        coder->encodeOctetString( (PCBYTE)attrType, ::strlen( attrType ) );

        coder->beginConstructed( CONSTRUCTED_TAG | SEQUENCE, jsBEREncoder::calcLengthType( subtotlen ) );
        
            // figure out what to put in ANY
            int start = *substring != STAR ? 1: 0, 
                end   = substring[::strlen(substring)-1] != STAR ? subs.size()-1 : subs.size();

            if ( *substring != STAR ) // if doesn't start with STAR, then has initial
                coder->encodeOctetString( (PCBYTE)subs[0].notPSZBuffer(), subs[0].length(), INITIAL_CONTEXT_TAG );

            for ( int i = start; i < end; i++ )
                coder->encodeOctetString( (PCBYTE)subs[i].notPSZBuffer(), subs[i].length(), ANY_CONTEXT_TAG );

            if ( substring[::strlen(substring)-1] != STAR  )
                coder->encodeOctetString( (PCBYTE)subs[i].notPSZBuffer(), subs[i].length(), FINAL_CONTEXT_TAG );

        coder->endConstructed( CONSTRUCTED_TAG | SEQUENCE );

    coder->endConstructed( SUBSTRING_CONTEXT_TAG );

	END
	
******************************************************************************/
class JSEXPORT jsBEREncoder
{
public:
	///---------------------------------------------------
	// enum for helping in shifting items.
	//
    enum action { 
        beginWrite, // set this Coder to write mode
        endWrite    // set this Coder to read mode
    };

	//===================================================================
	// GROUP: Constructor and initiailizers
	//===================================================================

	///---------------------------------------------------
	// Constructor from buffer. <b>Note</b> if you use this constructor
	// the buffer can not expand past max. 
	//
	// 
	// [in] buffer the buffer to encode or decode into
	// [in] max maximum size of this buffer
	// [in] reallocFn optional function to reallocate the buffer passed in.
	// If null, the buffer is not reallocated
	// [in] reallocULONG optional parameter to the realloc function
	//
	inline jsBEREncoder( PBYTE Buffer, ULONG max, jsReallocFn reallocFn = 0, ULONG reallocULONG = 0  ) 
		:  m_freeBuffer( false ), m_bufferSize( max ), m_buffer(Buffer), m_putOffset(0), m_inc(1024), m_incBig(1024), m_reallocFn(reallocFn), m_reallocULONG(reallocULONG) {}

	///---------------------------------------------------
	// Constructor from empty and allocate buffer.  If you try to add 
	// past the end of the buffer, it will re-allocate the buffer, using new.
	// 
	// [in] max maximum size of this buffer
	// [in] inc incrementaly size for reallocation
	//
	jsBEREncoder( ULONG maxBuffer = 4096, ULONG inc = 1024 );

	///---------------------------------------------------
	// Copy constructor from another jsBEREncoder.  It will
	// allocate and make a copy of the src.
	// 
	// [in] src the coder to copy from
	// [in] posInSrc where to start copying from in the source. Use 0
	//		for start
	// [in] len number of bytes to copy
	//
	// [throws] jsBEREncodeException
	//
	jsBEREncoder( const jsBEREncoder &src, ULONG posInSrc, ULONG len );

	///---------------------------------------------------
	// Copy operator.  Copies the data from the other
	//	coder.  Note that this must have enough room or be 
	//	able to re-allocate itself.
	//
	// [in] src jsBEREncoder to copy.  
	// 
	// [throws] jsBEREncodeException
	//
	// [Returns] 
	//		reference to this
	jsBEREncoder &operator=( const jsBEREncoder &src );

	///---------------------------------------------------
	// free the buffer is allocated it
	inline ~jsBEREncoder() { if ( m_freeBuffer ) delete m_buffer; }

	//===================================================================
	// GROUP: Buffer manipulation functions
	//===================================================================

	///---------------------------------------------------
	// Make sure the buffer is of the correct size, and rewind
	// it for new data to encode.  This function is used when a
	// jsBEREncoder is re-used and new data is being added to it.
	//
	// [in] len the length we need 
	// [in] somedata pointer to data to copy to the beginning of the rewound
	//		buffer
	// [in] somedatalen length of the somedata buffer to copy.  This length
	//		must be <= len
	//
	// [throws] jsBEREncodeException
	//
	// [Returns]
	//		writable pointer to the buffer after somedata.
	PBYTE assureLengthRewind( ULONG len, PCBYTE somedata, ULONG somedatalen );

	///---------------------------------------------------
	// Add a prefix and, or suffix buffer to the current internal buffer
	//
	// [in] prefix pointer to prefix to add.  Null if no prefix to add
	// [in] prefixLen length of prefix.  May be zero.
	// [in] suffix pointer to suffix to add.  Null if no suffix to add
	// [in] suffixLen length of suffix.  May be zero.
	//
	// [throws] jsBEREncodeException
	//
	// [Returns]
	//		TRUE if copied ok
	bool prefixSuffix( PCBYTE prefix, ULONG prefixLen, PCBYTE suffix, ULONG suffixLen );

	///---------------------------------------------------
	// Add a prefix and suffix coders to the current internal buffer
	//
	// [in] prefix jsBEREncoder to add as a prefix
	// [in] suffix jsBEREncoder to add as a suffix
	//
	// [throws] jsBEREncodeException
	//
	// [Returns]
	//		TRUE if copied ok
	bool prefixSuffix( const jsBEREncoder &prefix, const jsBEREncoder &suffix ) 
			{ return prefixSuffix( prefix.buffer(), prefix.curSize(), suffix.buffer(), suffix.curSize() ); }

	///---------------------------------------------------
	// Rewind the current pointer to start of buffer.  Use
	// with caution if using a non-zero value.  If you rewind
	// past an un-ended beginConstructed, it will remove it
	// from the patch array, so you can't end it (which you
	// shouldn't since you removed it by rewinding). 
	//
	// [in] putOffset where to rewind to.  
	//
	// [throws] jsBEREncodeException if new offset > than current
	void rewind( ULONG putOffset = 0 );

	///---------------------------------------------------
    // Call this after you write to the buffer returned by 
    // assureLengthRewind().  This will move the internal pointer
    // up this many bytes so it know more data was encoded into it.
    // This is used when getting data from another source such as
    // the network.
    //
    // [in] len length of data written
    //
	// [throws] jsBEREncodeException
	//
    void wroteToAssureBuffer( ULONG len );

	//===================================================================
	// GROUP: Encode functions
	//===================================================================

	///---------------------------------------------------
	// Append a jsBEREncoder to the end of this one.  This simply copies
	// the buffer of one to the end of this one.
	//
	// [in] coder the coder to append
	//
	// [throws] jsBEREncodeException
	//
	void appendCoder( const jsBEREncoder &coder );

	///---------------------------------------------------
	// Begin the encoding of a constructed type.  This function encodes
	// the tag of the constructed type and sets aside space for the length.
	// When endConstructed() is called the length is updated.  You may 
	// nest beginConstructed() calls. Make sure that each begin has 
	// a matching end.
	//
	// [in] tag tag of this constructed item
	// [in] lenType the type of length anticipated for this item.  It defaults
	//		to ONE_BYTE_LENGTH.  You will avoid a memcopy if you pass in the
	//		correct value.
	//
	// [throws] jsBEREncodeException
	//
	void beginConstructed( jsBERTag tag, BYTE lenType = ONE_BYTE_LENGTH );

	///---------------------------------------------------
	// Encode a boolean value
	//
	// [in] val the value to encode
	// [in] tag tag that this encoded type should have the 
	//		default value is the vanilla version, if this is
	//		a choice, context or application tag, you must pass in the
	//		tag you want to use
	//
	// [throws] jsBEREncodeException
	//
	// [Returns]
	//		number of bytes the pointer moved
	ULONG encodeBoolean ( bool val, jsBERTag tag = BOOLEAN_TAG );

	///---------------------------------------------------
	// Encode an enumerated value
	//
	// [in] val the value to encode
	// [in] tag tag that this encoded type should have the 
	//		default value is the vanilla version, if this is
	//		a choice, context or application tag, you must pass in the
	//		tag you want to use
	//
	// [throws] jsBEREncodeException
	//
	// [Returns]
	//		number of bytes the pointer moved
	ULONG	encodeEnumerated ( int val, jsBERTag tag = ENUMERATED_TAG );

	///---------------------------------------------------
	// Encode a four-byte long value
	//
	// [in] val the value to encode 
	// [in] tag tag that this encoded type should have the 
	//		default value is the vanilla version, if this is
	//		a choice, context or application tag, you must pass in the
	//		tag you want to use
	//
	// [throws] jsBEREncodeException
	//
	// [Returns]
	//		Number of bytes encoded
	ULONG encodeInteger( LONG val, jsBERTag tag = INTEGER_TAG );

	///---------------------------------------------------
	// Encode an eight-byte long value
	//
	// [in] val the value to encode 
	// [in] tag tag that this encoded type should have the 
	//		default value is the vanilla version, if this is
	//		a choice, context or application tag, you must pass in the
	//		tag you want to use
	//
	// [throws] jsBEREncodeException
	//
	// [Returns]
	//		Number of bytes encoded
	ULONG encodeLongInteger( LONGLONG val, jsBERTag tag = INTEGER_TAG );

	///---------------------------------------------------------
	// Encode a double real value
	// 
	// [in] val the value to encode 
	// [in] tag tag that this encoded type should have the 
	//		default value is the vanilla version, if this is
	//		a choice, context or application tag, you must pass in the
	//		tag you want to use
	//
	// [throws] jsBEREncodeException
	//
	// [Returns]
	//		Number of bytes encoded
	ULONG encodeReal(	DOUBLE val, jsBERTag tag = REAL_TAG );

	///---------------------------------------------------
	// Encode just a length into the coder, no tag.  This is useful
	// if you are encoding an object that you've already encoded the
	// tag for, and now need to encode the length
	//
	// [in] length the length to encode
	//
	// [throws] jsBEREncodeException
	//
	// [Returns]
	//		number of bytes encoded
	ULONG encodeLength( ULONG length );

	///---------------------------------------------------
	// Encode just a length into the coder, no tag.  This is useful
	// if you are encoding an object that you've already encoded the
	// tag for, and now need to encode the length
	//
	// [in] length the length to encode
	//
	// [throws] jsBEREncodeException
	//
	// [Returns]
	//		number of bytes encoded
	ULONG encodeTag( jsBERTag tag );

	///---------------------------------------------------
	// Static function to encode a length-type number into a buffer.  It does
	// no checking for length of the buffer.  An encoded 32 bit integer can
	// take no more than five bytes, one for the length indication, and max of
	// four for length.  This does <b>not</b> encode a tag.
	//
	// [in] length the number to encode
	// [in] lenType the type of this length (i.e. ONE_BYTE_LENGTH)
	// [in] ptr pointer where to encode the number
	//
	static void encodeNumber( ULONG length, BYTE lenType, PBYTE &ptr );

	///---------------------------------------------------
	// Encode a character string
	//
	// [in] val the value to encode
	// [in] maxLen max length for this string
	// [out] len length of this Encodeed string
	// [in] tag tag that this encoded type should have the 
	//		default value is the vanilla version, if this is
	//		a choice, context or application tag, you must pass in the
	//		tag you want to use
	//
	// [throws] jsBEREncodeException
	//
	// [Returns]
	//		len of string + length of length + length of tag
	ULONG encodeOctetString( PCBYTE s, ULONG len, jsBERTag tag = OCTET_STRING_TAG );

#ifdef JS_OCTETSTRING
	///---------------------------------------------------
	// Encode a string. This moves the pointer!
	//
	// [out] s destination string 
	// [in] tag tag that this encoded type should have the 
	//		default value is the vanilla version, if this is
	//		a choice, context or application tag, you must pass in the
	//		tag you want to use
	//
	// [throws] jsBEREncodeException
	//
	// [Returns]
	//		len of string + length of length + length of tag
	ULONG encodeOctetString( const jsOctetString &s, jsBERTag tag = OCTET_STRING_TAG )
		{ return encodeOctetString( (PCBYTE)s, s.length(), tag ); }
#endif

	///---------------------------------------------------
	// Encode a string
	//
	// [in] val the value to encode
	// [in] tag tag that this encoded type should have the 
	//		default value is the vanilla version, if this is
	//		a choice, context or application tag, you must pass in the
	//		tag you want to use
	//
	// [throws] jsBEREncodeException
	//
	// [Returns]
	//		len of string + length of length + length of tag
	ULONG encodeOctetString( const std::string &s, jsBERTag tag = OCTET_STRING_TAG)
		{ return encodeOctetString( (PCBYTE)s.c_str(), s.length(), tag ); }

	///---------------------------------------------------
	// Encode an ASCII string
	//
	// [in] val the value to encode
	// [in] tag tag that this encoded type should have the 
	//		default value is the vanilla version, if this is
	//		a choice, context or application tag, you must pass in the
	//		tag you want to use
	//
	// [throws] jsBEREncodeException
	//
	// [Returns]
	//		len of string + length of length + length of tag
	ULONG encodeString( const std::string &s, jsBERTag tag = IA5_STRING_TAG)
		{ return encodeOctetString( (PCBYTE)s.c_str(), s.length(), tag ); }

	ULONG encodeString( const char* s, jsBERTag tag = IA5_STRING_TAG)
		{ return encodeOctetString( (PCBYTE)s, s ? ::strlen(s) : 0, tag ); }

	///---------------------------------------------------------
	// encode a null item
	// 
	// [in] tag tag that this encoded type should have the 
	//		default value is the vanilla version, if this is
	//		a choice, context or application tag, you must pass in the
	//		tag you want to use
	//
	// [throws] jsBEREncodeException
	//
	// [Returns]
	//    length of tag + 1
	ULONG encodeNull( jsBERTag tag = NULL_TAG );

	///---------------------------------------------------
	// Ends the encoding of a constructed type.  This function encodes
	// the length and if need be makes room for the length.  
	//
	// [in] tag tag you passed to corresponding beginConstructed() call.
	//		this is only used in debugging as a weak sanity check to make
	//		sure you're ending the tag you began.  It is weak since it does
	//		not distinguish from two identical tags that are nested.  But 
	//		better than nothing.
	//
	// [throws] jsBEREncodeException
	//
	void endConstructed( jsBERTag tag );

	//===================================================================
	// GROUP: Encode operators
	//===================================================================

	///---------------------------------------------------
	// Set the next Encode type.  Pass in nextBOOL, or nextEnum
	// to be able to make the next >>(int) unambiguous
	//
	// [in] act action you wish to take, either begin or end write mode
	//
	// [Returns]
	//		reference to this for chaining operators
    jsBEREncoder& operator<<( action act );

	///---------------------------------------------------
	// Encode an int type.  This <i>must</i> abe preceded with the 
	// nextBOOL or nextEnum Nextencode enum or it will throw an exception.
	// This reset the Nextencode value so if you're doing multiple
	// bools or enums, you must shift a nextencode for each one
	//
	// [in] val an enum or BOOL the value to encode
	//
	// [throws] jsBEREncodeException
	//
	// [Returns]
	//		reference to this for chaining operators
    jsBEREncoder& operator<<( int i);

#ifdef JS_OCTETSTRING
	///---------------------------------------------------
	// Encode a jsOctetString.
	//
	// [out] s destination string 
	//
	// [throws] jsBEREncodeException
	//
	// [Returns]
	//		len of string + length of length
	inline jsBEREncoder& operator<<( const jsOctetString &s) { encodeOctetString( s ); return *this; }
#endif

	///---------------------------------------------------
	// Encode a jsString
	//
	// [in] val the value to encode
	//
	// [throws] jsBEREncodeException
	//
	// [Returns]
	//		reference to this for chaining operators
	inline jsBEREncoder& operator<<( const std::string &s) { encodeOctetString( s ); return *this; }

	///---------------------------------------------------
	// Encode a four-byte long value
	//
	// [in] val the value to encode
	//
	// [throws] jsBEREncodeException
	//
	// [Returns]
	//		reference to this for chaining operators
	inline jsBEREncoder& operator<<( LONG val ) { encodeInteger( (LONG)val ); return *this; }

	///---------------------------------------------------
	// Set the next Encode type.  Pass in nextBOOL, or nextEnum
	// to be able to make the next >>(int) unambiguous
	//
	// [in] nd the value of the next Integer to encode
	//
	// [Returns]
	//		reference to this for chaining operators
    jsBEREncoder& operator<<( jsBERDecoder::nextCode nd );

	///---------------------------------------------------
	// Encode a string
	//
	// [in] val the value to encode
	//
	// [throws] jsBEREncodeException
	//
	// [Returns]
	//		reference to this for chaining operators
	inline jsBEREncoder& operator<<( const char* s) { encodeOctetString( (PCBYTE)s, s ? strlen(s) : 0 ); return *this; }

	///---------------------------------------------------
	// Encode a two-byte short value
	//
	// [in] val the value to encode
	//
	// [throws] jsBEREncodeException
	//
	// [Returns]
	//		reference to this for chaining operators
	inline jsBEREncoder& operator<<( SHORT  val ) { encodeInteger( (LONG)val ); return *this; }

	///---------------------------------------------------
	// Encode a four-byte unsigned long value
	//
	// [in] val the value to encode
	//
	// [throws] jsBEREncodeException
	//
	// [Returns]
	//		reference to this for chaining operators
	inline jsBEREncoder& operator<<( ULONG val ) { encodeInteger( (LONG)val ); return *this; }

	///---------------------------------------------------
	// Encode a eight-byte unsigned long value
	//
	// [in] val the value to encode
	//
	// [throws] jsBEREncodeException
	//
	// [Returns]
	//		reference to this for chaining operators
	inline jsBEREncoder& operator<<( LONGLONG val ) { encodeLongInteger( val ); return *this; }

	///---------------------------------------------------
	// Encode a eight-byte unsigned long value
	//
	// [in] val the value to encode
	//
	// [throws] jsBEREncodeException
	//
	// [Returns]
	//		reference to this for chaining operators
	inline jsBEREncoder& operator<<( ULONGLONG val ) { encodeLongInteger( (LONGLONG)val ); return *this; }

	///---------------------------------------------------
	// Encode a double real value
	//
	// [in] val the value to encode
	//
	// [throws] jsBEREncodeException
	//
	// [Returns]
	//		reference to this for chaining operators
	inline jsBEREncoder& operator<<( double val ) { encodeReal( val ); return *this; }

	///---------------------------------------------------
	// Encode a two-byte unsigned short value
	//
	// [in] val the value to encode
	//
	// [throws] jsBEREncodeException
	//
	// [Returns]
	//		reference to this for chaining operators
	inline jsBEREncoder& operator<<( USHORT  val ) { encodeInteger( (LONG)val ); return *this; }

	///---------------------------------------------------
	// Encode a boolean 
	//
	// [in] val the value to encode
	//
	// [throws] jsBEREncodeException
	//
	// [Returns]
	//		reference to this for chaining operators
	inline jsBEREncoder& operator<<( bool val ) { encodeBoolean( val ); return *this; }

	//===================================================================
	// GROUP: Static Helper functions
	//===================================================================

	///---------------------------------------------------
	// Determine how many bytes a BER INTEGER will take
	//
	// [in] len length to check
	//
	// [Returns]
	//		length in bytes that number will take up
	static int calcIntegerLength( LONG len ); 

	///---------------------------------------------------
	// Determine how many bytes will this length take
	//
	// [in] len length to check
	//
	// [Returns]
	//		length in bytes that length will take encoded
	static int calcLengthLength( ULONG len ); 

	///---------------------------------------------------
	// Get the BYTE to be used in the encoding of this length
	//
	// [in] len length to check
	//
	// [Returns]
	//		the BYTE for the encoded length type.  ONE_BYTE_LENGTH, etc.
	static BYTE calcLengthType( ULONG len ); 

	///---------------------------------------------------
	// Get the length in bytes of a tag
	//
	// [in] tag length to check
	//
	// [Returns]
	//		the length in bytes of the tag when encoded
	static int calcTagLength( jsBERTag tag );

	//===================================================================
	// GROUP: Accessor functions
	//===================================================================

	///---------------------------------------------------
	// Get a pointer to the base buffer.
	//
	// [Returns]
	//		pointer to the buffer.
	PCBYTE buffer() const { jsAssert( m_patchArray.size() == 0 ); return m_buffer; }
	
	///---------------------------------------------------
	// Get the current size of the buffer
	//
	// [Returns]
	//		total length in bytes of that have been encoded into
	//		this buffer
	ULONG   bufferSize() const { return m_bufferSize; }

	///---------------------------------------------------
	// Get a pointer the current position we're putting at
	//
	// [Returns]
	//		pointer in buffer.
	PCBYTE curPtr() const { return m_buffer + m_putOffset; }
	
	///---------------------------------------------------
	// Get a the offset to the current position we're putting at
	// this is only useful for passing back into rewind(), or
	// calcluating the number of bytes encoded.
	//
	// [Returns]
	//		the offset
	ULONG curOffset() const { return m_putOffset; }
	
	///---------------------------------------------------
	// Get the current size of the buffer
	//
	// [Returns]
	//		total length in bytes of that have been encoded into
	//		this buffer
	ULONG   curSize() const { return m_putOffset; }

	///---------------------------------------------------
	// Get the size left in the buffer.  If you passed in your own
	// pointer in construction, this is all you've got.  If you 
	// just passed in a size, this is how much is left before it
	// will reallocate
	//
	// [Returns]
	//		length in bytes left in this buffer
	ULONG sizeLeft() const { return m_bufferSize - m_putOffset; }

	void copyBuffer( PCBYTE input_buffer, ULONG length );

private:

	// helpers to check validity of request and throw exceptions
	// is errors
	inline void throwIfNoRoom( ULONG extraSpaceNeeded );

	jsBERDecoder::nextCode		m_nextCode;		// what the next int encode value should be
												// set to nextAny most of the time

	void assureLength( ULONG newLength,	ULONG moveOffset = 0 );

	static ULONG encodeLength( ULONG length, PBYTE ptr );

	ULONG					m_bufferSize;	// the buffer size
	PBYTE					m_buffer;		// start of the buffer
	ULONG					m_putOffset;	// where next put will occur
	bool					m_freeBuffer;	// should we fee on exit

	std::vector<PatchLength>m_patchArray;	// array of items to be patched
	ULONG					m_inc;			// memory allocation increment
	ULONG					m_incBig;		// same, for bigger buffers.

	jsReallocFn				m_reallocFn;	// method for reallocating
	ULONG					m_reallocULONG; // extra parmeter for realloc
};

/******************************************************************************
CLASS     
    jsBEREncodeException

    Class used for throwing decoding exceptions

DESCRIPTION
    Catch this class when using functions in the jsBEREncoder class.  The reason
	holds the numeric reason why the exeption was thrown and the text will contain
	some human-readable reason
    
******************************************************************************/
class JSEXPORT jsBEREncodeException : public jsBERException
{
public:
    ///---------------------------------------------------
    // Reasons why this exception can be thrown
    enum Reason { 
        nullPointer,        // Null pointer passed in
        badParam,           // Some other parm was bad
        pointerPastEnd,     // the current pointer has moved past the end 
                            // of the buffer.  This object is now corrupted.
        cantReallocUserBuffer, // can't reallocate the buffer since this object was
                            // constructed with the user's buffer.
        noMemory,           // got errro reallocating memory
        invalidInputString, // string sent in is > SHORT_MAX in length
        incorrectOperation  // attempted to shift in an int without shifting in a nextcode
                            // first
    };

	///---------------------------------------------------
	// construct the exception
	//
	// [in] text text about the exception
	// [in] why reason why the exception was thrown
	jsBEREncodeException( const char* text, Reason why ) : jsBERException( text ), m_why( why ) {}

	///---------------------------------------------------
	// construct the exception
	//
	// [in] text text string id about the exception
	// [in] why reason why the exception was thrown
//	jsBEREncodeException( UINT text, Reason why ) : jsBERException( text ), m_why( why ) {}

	///---------------------------------------------------
	// get the reason
	//
	// [Returns] 
	//		reason why it was thrown
	Reason why() const { return m_why; }

private:
	Reason			m_why;
};

inline void jsBEREncoder::throwIfNoRoom( ULONG extraSpaceNeeded )
{
	assureLength( curSize()+extraSpaceNeeded  );
}

inline  ULONG jsBEREncoder::encodeNull( jsBERTag tag /*= NULL_TAG*/ )
{
	ULONG ret = encodeTag( tag );
	ret += encodeLength(0);
	return ret;
}

#endif //__JSBERENCODER_H__
