@ECHO OFF

SETLOCAL EnableDelayedExpansion

SET ThisDir=%~dp0
SET BaseDir=%ThisDir%..\..
SET ScriptsDir=%BaseDir%\BuildScripts


SET ProjectName=jsTools-4.0.1
SET SolutionToBuild="%ThisDir%jsTools_vs2015.sln"
rem SET DebugConfiguration="debug"
rem SET ReleaseConfiguration="release"

call %ScriptsDir%\BuildSolution.cmd %1

IF %CopyFiles% == 1 (
	call %ScriptsDir%\xcopy.cmd %ThisDir%js.h %IncludeDir%\jsTools\js.h* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%jsBERDecoder.h %IncludeDir%\jsTools\jsBERDecoder.h* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%jsBEREncoder.h %IncludeDir%\jsTools\jsBEREncoder.h* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%jsConnection.h %IncludeDir%\jsTools\jsConnection.h* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%jsDerivedRefCountPtr.h %IncludeDir%\jsTools\jsDerivedRefCountPtr.h* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%jsEnumFiles.h %IncludeDir%\jsTools\jsEnumFiles.h* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%jsEvent.h %IncludeDir%\jsTools\jsEvent.h* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%jsInetInfo.h %IncludeDir%\jsTools\jsInetInfo.h* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%jsLibraryFunction.h %IncludeDir%\jsTools\jsLibraryFunction.h* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%jsLock.h %IncludeDir%\jsTools\jsLock.h* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%jsNet.h %IncludeDir%\jsTools\jsNet.h* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%jsRefCountPtr.h %IncludeDir%\jsTools\jsRefCountPtr.h* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%jsSemaphore.h %IncludeDir%\jsTools\jsSemaphore.h* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%jsSocket.h %IncludeDir%\jsTools\jsSocket.h* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%jsSync.h %IncludeDir%\jsTools\jsSync.h* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%jsThread.h %IncludeDir%\jsTools\jsThread.h* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%jsThreadTypes.h %IncludeDir%\jsTools\jsThreadTypes.h* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%jsTime.h %IncludeDir%\jsTools\jsTime.h* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%jsTypes.h %IncludeDir%\jsTools\jsTypes.h* /Y

	call %ScriptsDir%\xcopy.cmd %ThisDir%lib\jsTools.lib %LibDir%\jsTools.lib* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%lib\jsTools.pdb %LibDir%\jsTools.pdb* /Y
	
	call %ScriptsDir%\xcopy.cmd %ThisDir%lib\jsToolsd.lib %LibDir%\jsToolsd.lib* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%lib\jsToolsd.pdb %LibDir%\jsToolsd.pdb* /Y
)
