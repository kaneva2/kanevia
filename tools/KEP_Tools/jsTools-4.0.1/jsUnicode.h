#pragma once

// The Unicode conversion functions are implemented in ..../Source/Platform/Common/include.
// We can't reference that file directly because its relative path depends on which branch we're using (the file doesn't even exist on Mainline yet).
// Long-term, jsTools should be replaced with a cleaner cross-platform library that natively supports UTF-8, or
// the parts that we need should simply be migrated over to mainline, then we can reference the file directly.
#include "Unicode_CopiedFromCommonInclude.h"
