/******************************************************************************
 Unicode.h

 Copyright (c) 2015 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include <string>
#include <vector>

namespace KEP {

// Converts a UTF-8 character sequence to a UTF-32 character.
// Returns: true if UTF-8 sequence is valid.
// (size_t* version) Updates pnConsumed with the number of UTF-8 characters used to construct the UTF-32 character.
// (const char** version) Updates *ppUtf8End with a pointer to one past the last byte of the character converted.
// For invalid sequences, *pnConsumed(or *ppUtf8End) will point to the first invalid byte
// and *pUtf32 will be set to zero.
// For unvalidated input, ensure at least 6 characters can be read from pUtf8, or that it
// is null-terminated.
inline bool Utf8ToUtf32(const char* pUtf8, char32_t* pUtf32, size_t* pnConsumed=nullptr)
{
	bool bSucceeded = true;
	size_t nConsumed = 0;
	char32_t chOut = 0;
	char chFirst = pUtf8[nConsumed];
	if( !(chFirst & 0x80) ) {
		chOut = chFirst;
		++nConsumed;
	} else if( (chFirst & 0xc0) != 0xc0 ) {
		// Error. Invalid initial byte.
		bSucceeded = false;
	} else {
		chOut = chFirst;
		++nConsumed;
		do {
			char chNext = pUtf8[nConsumed];
			if( (chNext & 0xc0) != 0x80 ) {
				bSucceeded = false;
				chOut = 0;
				break;
			}
			++nConsumed;
			chOut <<= 6;
			chOut |= (chNext & 0x3f);
		} while( chFirst & (1 << (7-nConsumed)) );

		// Clear control bits of first char.
		if( bSucceeded )
			chOut &= (char32_t(1) << (1 + 5 * nConsumed)) - 1;
	}
	*pUtf32 = chOut;
	if( pnConsumed )
		*pnConsumed = nConsumed;
	return bSucceeded;
}

inline bool Utf8ToUtf32(const char* pUtf8, char32_t* pUtf32, const char** ppUtf8End)
{
	if( ppUtf8End )
	{
		size_t nConsumed = 0;
		bool bRet = Utf8ToUtf32(pUtf8, pUtf32, &nConsumed);
		*ppUtf8End = pUtf8 + nConsumed;
		return bRet;
	}
	else
	{
		return Utf8ToUtf32(pUtf8, pUtf32);
	}
}

// Converts a UTF-16 character sequence to a UTF-32 character.
// Returns: true if UTF-16 sequence is valid.
// Updates *ppUtf16End with a pointer to one past the last character converted.
// For invalid sequences, *ppUtf16End will point to the first invalid byte
// and *pUtf32 will be set to zero.
// Up to two 16 bit characters will be read.
template<typename Char16>
inline bool Utf16ToUtf32(const Char16* pUtf16, char32_t* pUtf32, const Char16** ppUtf16End=nullptr)
{
	static_assert(sizeof(Char16)==sizeof(char16_t), "UTF-16 character size mismatch");
	typedef std::make_unsigned_t<Char16> UChar16; // So we can do unsigned comparisons.

	bool bSucceeded = false;
	size_t nConsumed = 0;
	char32_t chOut = 0;
	UChar16 chFirst = pUtf16[nConsumed];
	if( chFirst < 0xd800 || chFirst >= 0xe000 ) {
		nConsumed = 1;
		bSucceeded = true;
		chOut = chFirst;
	} else {
		if( chFirst < 0xdc00 ) {
			UChar16 chSecond = pUtf16[1];
			if( chSecond >= 0xdc00 && chSecond < 0xe000 ) {
				nConsumed = 2;
				bSucceeded = true;
				chOut = 0x10000 + ((chFirst & 0x3ff) << 10) + (chSecond & 0x3ff);
			}
		}
	}

	*pUtf32 = chOut;
	if( ppUtf16End )
		*ppUtf16End = pUtf16 + nConsumed;
	return bSucceeded;
}

template<typename Char16>
inline bool Utf16ToUtf32(const Char16* pUtf16, char32_t* pUtf32, size_t* pNumChar16Processed)
{
	const Char16* pEndUtf16;
	bool bRet = Utf16ToUtf32(pUtf16, pUtf32, &pEndUtf16);
	if( pNumChar16Processed )
		*pNumChar16Processed = pEndUtf16 - pUtf16;
	return bRet;
}

template<typename Char16=wchar_t>
inline bool Utf32ToUtf16(char32_t chUtf32, Char16* pUtf16, Char16** ppUtf16End=nullptr)
{
	static_assert(sizeof(Char16)==sizeof(char16_t), "UTF-16 character size mismatch");

	Char16* pUtf16End = pUtf16;
	bool bSucceeded = false;
	if( chUtf32 <= 0xffff )
	{
		if( chUtf32 <= 0xd7ff || chUtf32 >= 0xe000 )
		{
			// One character sequence.
			bSucceeded = true;
			*pUtf16End++ = Char16(chUtf32);
		}
		else
		{
			// Invalid code point: between 0xd800 and 0xdfff
		}
	}
	else if( chUtf32 <= 0x10ffff )
	{
		// Two character sequence.
		bSucceeded = true;
		char32_t chOffset = chUtf32  - 0x10000;
		*pUtf16End++ = Char16(0xd800 + (chOffset >> 10));
		*pUtf16End++ = Char16(0xdc00 + (chOffset & ((1 << 10)-1)));
	}
	else
	{
		// Code point to large.
	}
	if( ppUtf16End )
		*ppUtf16End = pUtf16End;
	return bSucceeded;
}

template<typename Char16=wchar_t>
inline bool Utf32ToUtf16(char32_t chUtf32, Char16* pUtf16, size_t* pnWritten)
{
	static_assert(sizeof(Char16)==sizeof(char16_t), "UTF-16 character size mismatch");
	Char16* pUtf16End;
	bool b = Utf32ToUtf16(chUtf32, pUtf16, &pUtf16End);
	if( pnWritten )
		*pnWritten = pUtf16End - pUtf16;
	return b;
}

inline bool Utf32ToUtf8(char32_t chUtf32, char* pUtf8, char** ppUtf8End=nullptr)
{
	// Unicode codepoints are supposed to be <= 0x10ffff, so most of the 4 byte encodings,
	// as well as the 5 and 6 byte encodings, are technically invalid.

	bool bSucceeded = true;
	size_t nWritten = 0;
	if( chUtf32 < 0x80 ) {
		pUtf8[0] = char(chUtf32);
		nWritten = 1;
	} else if( chUtf32 < 0x800 ) {
		pUtf8[0] = char((chUtf32 >> 6) | 0xc0);
		pUtf8[1] = char((chUtf32 & 0x3f) | 0x80);
		nWritten = 2;
	} else if( chUtf32 < 0x10000 ) {
#if defined(DONT_VALIDATE_UNICODE_VALUE)
		if( 1 ) {
#else
		if( chUtf32 < 0xd800 || chUtf32 >= 0xe000 ) {
#endif
			pUtf8[0] = char((chUtf32 >> 12) | 0xe0);
			pUtf8[1] = char(((chUtf32 >> 6) & 0x3f) | 0x80);
			pUtf8[2] = char(((chUtf32 >> 0) & 0x3f) | 0x80);
			nWritten = 3;
		} else {
			// Invalid code point: 0xd800 - 0xdfff
			bSucceeded = false;
		}
#if 1 // Check for overflow (code points beyond 0x10ffff
	} else if( chUtf32 <= 0x10ffff ) {
		pUtf8[0] = char((chUtf32 >> 18) | 0xf0);
		pUtf8[1] = char(((chUtf32 >> 12) & 0x3f) | 0x80);
		pUtf8[2] = char(((chUtf32 >> 6) & 0x3f) | 0x80);
		pUtf8[3] = char(((chUtf32 >> 0) & 0x3f) | 0x80);
		nWritten = 4;
#else
	} else if( chUtf32 < 0x200000 ) {
		pUtf8[0] = char((chUtf32 >> 18) | 0xf0);
		pUtf8[1] = char(((chUtf32 >> 12) & 0x3f) | 0x80);
		pUtf8[2] = char(((chUtf32 >> 6) & 0x3f) | 0x80);
		pUtf8[3] = char(((chUtf32 >> 0) & 0x3f) | 0x80);
		nWritten = 4;
	} else if( chUtf32 < 0x4000000 ) {
		pUtf8[0] = char((chUtf32 >> 24) | 0xf8);
		pUtf8[1] = char(((chUtf32 >> 18) & 0x3f) | 0x80);
		pUtf8[2] = char(((chUtf32 >> 12) & 0x3f) | 0x80);
		pUtf8[3] = char(((chUtf32 >> 6) & 0x3f) | 0x80);
		pUtf8[4] = char(((chUtf32 >> 0) & 0x3f) | 0x80);
		nWritten = 5;
	} else if( chUtf32 < char32_t(0x80000000) ) {
		pUtf8[0] = char((chUtf32 >> 30) | 0xfc);
		pUtf8[1] = char(((chUtf32 >> 24) & 0x3f) | 0x80);
		pUtf8[2] = char(((chUtf32 >> 18) & 0x3f) | 0x80);
		pUtf8[3] = char(((chUtf32 >> 12) & 0x3f) | 0x80);
		pUtf8[4] = char(((chUtf32 >> 6) & 0x3f) | 0x80);
		pUtf8[4] = char(((chUtf32 >> 0) & 0x3f) | 0x80);
		nWritten = 6;
#if defined(DONT_VALIDATE_UNICODE_VALUE)
	} else if( 1 ) {
		pUtf8[0] = char(0xfe);
		pUtf8[1] = char(((chUtf32 >> 30) & 0x3f) | 0x80);
		pUtf8[2] = char(((chUtf32 >> 24) & 0x3f) | 0x80);
		pUtf8[3] = char(((chUtf32 >> 18) & 0x3f) | 0x80);
		pUtf8[4] = char(((chUtf32 >> 12) & 0x3f) | 0x80);
		pUtf8[5] = char(((chUtf32 >> 6) & 0x3f) | 0x80);
		pUtf8[6] = char(((chUtf32 >> 0) & 0x3f) | 0x80);
		nWritten = 7;
#endif
#endif
	} else {
		bSucceeded = false;
	}
	if( ppUtf8End )
		*ppUtf8End = pUtf8 + nWritten;

	return bSucceeded;
}

inline bool Utf32ToUtf8(char32_t chUtf32, char* pUtf8, size_t* pnWritten)
{
	char* pUtf8End;
	bool b = Utf32ToUtf8(chUtf32, pUtf8, &pUtf8End);
	if( pnWritten )
		*pnWritten = pUtf8End - pUtf8;
	return b;
}

template<typename Char16>
inline size_t CharLengthUtf16(const Char16* pChar)
{
	Char16 ch = *pChar;
	if( ch < 0xd800 || ch >= 0xe000 )
		return 1;
	else if( ch < 0xdc00 )
		return 2;
	else
		return 0; // Invalid initial character.
}

template<typename Char8>
inline size_t CharLengthUtf8(const Char8* pChar)
{
	Char8 ch = *pChar;
	if( (ch & 0x80) == 0)
		return 1;
	if( (ch & 0x40) != 0 ) {
		if( (ch & 0x20) == 0 )
			return 2;
		if( (ch & 0x10) == 0 )
			return 3;
		if( (ch & 0x08) == 0 )
			return 4;
		// Overlong character encoding.
	}
	return 0;
}

template<typename Char16=wchar_t>
inline std::basic_string<Char16> Utf8ToUtf16(const char* pszUtf8)
{
	static_assert(sizeof(Char16)==sizeof(char16_t), "UTF16 character size mismatch");

	std::basic_string<Char16> str16;
	if( pszUtf8 )
	{
		const char* pszNextUtf8 = pszUtf8;
		while(true) {
			char32_t ch32;
			Utf8ToUtf32(pszNextUtf8, &ch32, &pszNextUtf8);
			if( ch32 == 0 )
				break; // Last character encountered, or error occurred.

			Char16 ach16[2];
			size_t nWritten;
			// No error handling needed for Utf32ToUtf16:
			// nWritten will be zero on error so this character will be ignored.
			Utf32ToUtf16(ch32, ach16, &nWritten);
			for( size_t i = 0; i < nWritten; ++i )
				str16 += ach16[i];
		}
	}
	return str16;
}

template<typename Char16=wchar_t>
inline std::basic_string<Char16> Utf8ToUtf16(const std::basic_string<char>& strUtf8)
{
	return Utf8ToUtf16(strUtf8.c_str());
}

template<typename Char16, size_t N>
inline std::basic_string<Char16> Utf8ToUtf16(const char achUtf8[N])
{
	return Utf8ToUtf16(&achUtf16[0]);
}

inline size_t AppendUtf32AsUtf8(char32_t ch32, std::basic_string<char>* pstr8)
{
	char ach8[8];
	size_t nWritten;
	// No error handling needed for Utf32ToUtf8:
	// nWritten will be zero on error so this character will be ignored.
	Utf32ToUtf8(ch32, ach8, &nWritten);
	pstr8->append(ach8, nWritten);
	return nWritten;
}

template<typename Char16>
inline std::basic_string<char> Utf16ToUtf8(const Char16* pszUtf16)
{
	static_assert(sizeof(Char16)==sizeof(char16_t), "UTF-16 character size mismatch");

	std::basic_string<char> str8;
	if( pszUtf16 )
	{
		const Char16* pszNextUtf16 = pszUtf16;
		while(true) {
			char32_t ch32;
			Utf16ToUtf32(pszNextUtf16, &ch32, &pszNextUtf16);
			if( ch32 == 0 )
				break; // Last character encountered, or error occurred.

			AppendUtf32AsUtf8(ch32, &str8);
		}
	}
	return str8;
}

inline std::basic_string<char> Utf16ToUtf8(const wchar_t* pszUtf16)
{
	return Utf16ToUtf8<wchar_t>(pszUtf16);
}

inline std::basic_string<char> Utf16ToUtf8(const char16_t* pszUtf16)
{
	return Utf16ToUtf8<char16_t>(pszUtf16);
}

template<typename Char16>
inline std::basic_string<char> Utf16ToUtf8(const Char16* pszUtf16, size_t nChar16)
{
	static_assert(sizeof(Char16)==sizeof(char16_t), "UTF-16 character size mismatch");

	std::basic_string<char> str8;
	if( pszUtf16 )
	{
		size_t iNextChar = 0;
		const Char16* pszNextUtf16 = pszUtf16;
		const Char16* pszEndUtf16 = pszNextUtf16 + nChar16;
		while(true) {
			char32_t ch32;
			if( pszNextUtf16 + CharLengthUtf16(pszUtf16 + iNextChar) >= pszEndUtf16 )
				break; // Don't try reading partial character.
			if( !Utf16ToUtf32(pszNextUtf16, &ch32, &pszNextUtf16) )
				break;

			AppendUtf32AsUtf8(ch32, &str8);
		}
	}
	return str8;
}

// Ignores (rather than truncates) characters that can't be encoded as 7 bit ASCII
template<typename Char16>
inline std::basic_string<char> Utf16ToAsciiLossy(const Char16* pszUtf16)
{
	static_assert(sizeof(Char16)==sizeof(char16_t), "UTF-16 character size mismatch");

	std::basic_string<char> str8;
	if( pszUtf16 )
	{
		const Char16* pszNextUtf16 = pszUtf16;
		while(true) {
			char32_t ch32;
			Utf16ToUtf32(pszNextUtf16, &ch32, &pszNextUtf16);
			if( ch32 == 0 )
				break; // Last character encountered, or error occurred.

			// Use this character if it is a valid 7 bit Ascii character.
			if( (ch32 & ~(0x7f)) == 0x0 )
				str8 += ch32;
		}
	}
	return str8;
}

template<typename Char16, size_t N>
inline std::basic_string<char> Utf16ToUtf8(const Char16 achUtf16[N])
{
	return Utf16ToUtf8(&achUtf16[0]);
}

template<typename Char16, size_t N>
inline std::basic_string<char> Utf16ToUtf8(Char16 achUtf16[N])
{
	return Utf16ToUtf8(&achUtf16[0]);
}

template<typename Char16>
inline std::basic_string<char> Utf16ToUtf8(const std::basic_string<Char16>& strUtf16)
{
	return Utf16ToUtf8(strUtf16.c_str());
}

#if 0
static inline bool TestUnicodeConversion()
{
	bool bSucceeded = true;

	// Test some known edge-case characters.
	struct StringTypes {
		char32_t chUtf32;
		char16_t achUtf16[3];
		char achUtf8[7];
	};
	StringTypes aTestStrings[] = {
		{ {0}, {0}, {0} },
		{ {0x7f}, {0x7f, 0}, {0x7f, 0} },
		{ {0x80}, {0x80, 0}, {0xc2u, 0x80u, 0} },
		{ {0x100}, {0x100, 0}, {0xc4u, 0x80u, 0} },
		{ {0x7ff}, {0x7ff, 0}, {0xdfu, 0xbfu, 0} },
		{ {0x800}, {0x800, 0}, {0xe0u, 0xa0u, 0x80u, 0} },
		{ {0xffff}, {0xffffu, 0}, {0xefu, 0xbfu, 0xbfu, 0} },
		{ {0x10000}, {0xd800u, 0xdc00u, 0}, {0xf0u, 0x90u, 0x80u, 0x80u, 0} },
		{ {0x10ffff}, {0xdbffu, 0xdfffu, 0}, {0xf4u, 0x8fu, 0xbfu, 0xbfu, 0} },
	};
	char ach8[7];
	char16_t ach16[2];
	char32_t ch32;
	for( size_t i = 0; i < std::extent<decltype(aTestStrings)>::value; ++i )
	{
		memset( ach8, 0, sizeof(ach8) );
		memset( ach16, 0, sizeof(ach16) );
		ch32 = 0;
		if( !Utf32ToUtf16(aTestStrings[i].chUtf32, ach16) )
			bSucceeded = false;
		else if( memcmp(ach16, aTestStrings[i].achUtf16, sizeof(ach16)) != 0 )
			bSucceeded = false;

		if( !Utf32ToUtf8(aTestStrings[i].chUtf32, ach8) )
			bSucceeded = false;
		else if( memcmp(ach8, aTestStrings[i].achUtf8, sizeof(ach8)) != 0 )
			bSucceeded = false;

		if( !Utf8ToUtf32(aTestStrings[i].achUtf8, &ch32) )
			bSucceeded = false;
		else if( ch32 != aTestStrings[i].chUtf32 )
			bSucceeded = false;

		if( !Utf16ToUtf32(aTestStrings[i].achUtf16, &ch32) )
			bSucceeded = false;
		else if( ch32 != aTestStrings[i].chUtf32 )
			bSucceeded = false;
	}

	// Test round-trip conversion for all valid characters
	for( uint32_t i = 0; i < 0x110000; ++i )
	{
		char ach8[7] = { 0 };
		wchar_t ach16[3] = { 0 };
		size_t n;
		if( !KEP::Utf32ToUtf8(i, ach8, &n) )
		{
			// Function is supposed to fail for characters in this range.
			if( !(i >= 0xd800 && i <= 0xdfff) )
				bSucceeded = false;
		}
		else
		{
			size_t n2;
			char32_t ch32;
			if( !KEP::Utf8ToUtf32(ach8, &ch32, &n2) )
				bSucceeded = false;
			else if( i != ch32 )
				bSucceeded = false;
			else if( n != n2 )
				bSucceeded = false;
		}

		if( !KEP::Utf32ToUtf16(i, ach16, &n) )
		{
			// Function is supposed to fail for characters in this range.
			if( !(i >= 0xd800 && i <= 0xdfff) )
				bSucceeded = false;
		}
		else
		{
			size_t n2;
			char32_t ch32;
			if( !KEP::Utf16ToUtf32(ach16, &ch32, &n2) )
				bSucceeded = false;
			else if( i != ch32 )
				bSucceeded = false;
			else if( n != n2 )
				bSucceeded = false;
		}
	}

	return bSucceeded;
}
#endif

inline std::wstring CharToWcharString(const char* input)
{
	size_t n = strlen(input);
	return std::wstring(input, input + n);
}

inline std::string WcharToCharString(const wchar_t* input)
{
	size_t n = wcslen(input);
	return std::string(input, input + n);
}

// Converts UTF-16 argv to UTF-8 argv.
// The contents of (*pargv8Utf8) will give argv as a char** in UTF-8 encoding.
// Don't let either output argument go out of scope until you are done processing.
//
// @param argc  Number of arguments.
// @param argv16  UTF-16 argv list.
// @param pachUtf8  Output buffer to hold converted characters.
// @param pargv8  Output buffer to hold UTF-8 argv. Pointers point to items in pachUtf8
inline void ConvertArgvUtf16ToUtf8(int argc, wchar_t*const* argvUtf16, std::vector<char>* pachUtf8, std::vector<char*>* pargvUtf8)
{
	std::vector<size_t> aiOffsets;
	for( int i = 0; i < argc; ++i ) {
		aiOffsets.push_back(pachUtf8->size());
		std::string strArg = Utf16ToUtf8(argvUtf16[i]);
		pachUtf8->insert(pachUtf8->end(), strArg.begin(), strArg.end());
		pachUtf8->push_back(0);
	}

	std::vector<char*> apszArgsUtf8;
	for( size_t i = 0; i < aiOffsets.size(); ++i ) {
		pargvUtf8->push_back(pachUtf8->data() + aiOffsets[i]);
	}

	// Add final nullptr
	pargvUtf8->push_back(nullptr);
}

class CommandLineUtf8
{
public:
	CommandLineUtf8(int argc, wchar_t*const* argv) { Init(argc, argv); }
	void Init(int argc, wchar_t*const* argv)
	{
		m_achUtf8.clear();
		m_argvUtf8.clear();
		ConvertArgvUtf16ToUtf8(argc, argv, &m_achUtf8, &m_argvUtf8);
	}

	char** GetArgV() { return m_argvUtf8.data(); }
	int GetArgC() const { return m_argvUtf8.size()-1; }

private:
	std::vector<char> m_achUtf8;
	std::vector<char*> m_argvUtf8;
};


} //namespace KEP
