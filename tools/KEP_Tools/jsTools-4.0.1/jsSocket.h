/******************************************************************************
 Name: jsSocket.h

 Description: socket abstract class definition

 (c) Copyright 1999 Iterated Systems, Inc.
 ALL RIGHTS RESERVED

 The software and information contained herein are proprietary to, and
 comprise valuable trade secrets of Iterated Systems, Inc., which
 intends to preserve as trade secrets such software and information.

 History:
 9/18/99	Derived from jsSocketImpl.h

 Notes: 

******************************************************************************/
#ifndef _jsSocket_h_
#define _jsSocket_h_

#include "jsNet.h"
#include <vector>
using namespace std;

//
// jsSocket - Abstract definition of a sockets-like interface
//
class JSEXPORT jsSocket : public jsBase
{
public:
	
	jsSocket() {}
	virtual  ~jsSocket() {}
	
	virtual void duplicate( const jsSocket & from ) = 0;

	//define basic functions for the tcp class. 
	virtual bool socket() = 0;

	//timeout = 0, means infinite access time. Specify timeout in seconds
	virtual bool  connect( ULONG addr, UINT port, bool block = true, ULONG timeout = 0 ) = 0;

	virtual SOCKFD accept() = 0;
	
	virtual bool send( const BYTE * data, ULONG len, ULONG& written, ULONG timeout = 0 ) = 0;
  
	virtual bool receive( BYTE *data, ULONG maxlen, ULONG& read, ULONG timeout = 0  ) = 0;

	virtual bool listen ( ULONG backlog =  255 ) = 0;

	virtual bool bind( UINT port = 0 ) = 0;

	virtual bool bindUntilSuccess( UINT startPortNo, UINT endPortNo ) = 0;	
	
	virtual bool close( bool shutdown = true ) = 0;

	//common accessor funtions
	virtual SOCKFD getSock()  const = 0; 

	virtual LONG getMaxFd()  const = 0;

	virtual std::string domainName() = 0;
	
	virtual std::string hostName()  const = 0;

	virtual std::string dnsName( const std::string &name="" )   = 0;

	virtual bool loadInetInfo ( const std::string &hname, std::string& dnsName, jsStringArray& hostAlias, vector<ULONG>& hostAddr ) = 0;

	virtual LONG localport() = 0;

	virtual LONG peerport() = 0;

	virtual std::string address() = 0;

	virtual ULONG peeraddr() = 0;

	virtual LONG lastError() const = 0;

	virtual bool isBlocking() const = 0;

	//operations on socket mode
	virtual bool setNonBlocking( bool set = false ) = 0;

	virtual bool setLinger( bool set = true ) = 0;

	virtual bool setReuse( bool set = true ) = 0;

	virtual bool setRecvBufSize( ULONG size ) = 0;

	virtual bool setSendBufSize( ULONG size ) = 0;

	virtual bool setOOB( bool set = true ) = 0;

	virtual bool setKeepAlive( bool set = true ) = 0;

	//reuse a socket.
	virtual bool setSocket( SOCKFD sock ) = 0;

	virtual bool eof() = 0;

	virtual bool nbcode() const = 0;	 

	virtual bool timeout()  const = 0;

	
	//static helper fns

	static ULONG inet_addr( BYTE _first, BYTE dotnext2, BYTE dotnext3, BYTE dotnext4 );
	static ULONG inet_addr( const char* addr );

	/*
	**NOTE** Solaris 4.1 CC complains about function names like htonl,ntohl
	So, I got rid of them. 
	*/
	static ULONG nethtonl( ULONG hostaddr );
	static ULONG netntohl( ULONG netaddr );

	static bool is_inet_addr( const char* addr );

	// parm in network format
	static std::string inet_ntoa( ULONG addr );

private:
	// Disallow copy constructor and assignment
	jsSocket( const jsSocket &  );
	jsSocket & operator =( const jsSocket & from );
};

#include "jsRefCountPtr.h"
typedef jsRefCountPtr<jsSocket>	jsSocketPtr;

#endif



 
 
 
 
 
