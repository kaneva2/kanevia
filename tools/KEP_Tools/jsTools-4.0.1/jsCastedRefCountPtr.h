#ifndef __JSCASTEDREFCOUNTPTR_H__
#define __JSCASTEDREFCOUNTPTR_H__
/******************************************************************************
 Name: jsCastedRefCountPtr.h

 Description: allows clean casting of jsRefCountObjects

 Copyright (C) 1996, 2004 Jim Wallace

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 History:
 2/25/99		jmw created 

 Notes: 

******************************************************************************/
#ifndef _js_h_
	#error Include js.h before jsCastedRefCountPtr.h
#endif

#include "jsRefCountPtr.h"


template <class TOCLASS, class FROMCLASS, class InternalImpl = jsInternalRefCntPtr<FROMCLASS> > 
/***********************************************************
CLASS
	jsCastedRefCountPtr
	
	allows clean casting of jsRefCountObjects

DESCRIPTION
	Use this class if you know that an object is actually 
	a derived class.  This class cast it to the other object
	and allows for access to it like a pointer.

	If you are compiling with RTTI, this will only work if
	the base class has a virtual function.  This class will use
	RTTI to check the type in the constructor.

SAMPLE
	This sample shows how to use the class.

	EXAMPLE
    class xx // base class
    {
    public:
        virtual t() { cout << "xxx" << endl; }
        virtual ~xx() { cout << "xx gone" << endl; }
    };

    class yy : public xx // derived
    {
    public:
        virtual t() { cout << "yyyy" << endl; }
        virtual tt() { cout << "tt yyyy" << endl; }
        virtual ~yy() { cout << "yyyy gone" << endl; }
    };

    class zz
    {
    };
		
		{
		// create a base ref counted pointer with 
		// the derived class
        jsRefCountPtr<xx> x = jsRefCountPtr<xx>(new yy );

        jsCastedRefCountPtr<zz,xx> z(x);


        if ( z ) // false if using rtti, otherwise DANGER!!!
				 // at this point xx only has ref count of 1
        {

        }

        jsCastedRefCountPtr<yy,xx> y(x);

        if ( y ) // true, ref count is now 2
        {
            y->t();
            (*y).t();
            y->tt();
            (*y).tt();
            y->ttt();
            (*y).ttt();
        }

		} // end of scope, when y and x die, pointer deleted

	END

***********************************************************/
class jsCastedRefCountPtr
{
public:

	///---------------------------------------------------------
	// constructor.  If using RTTI, this will do a dynamic_cast
	// in the constuctor.  Always check the operator bool()
	// after constructing it.  See the SAMPLE
	// 
	// [in] ptr reference counted object that you want to cast
	//
	jsCastedRefCountPtr( jsRefCountPtr<FROMCLASS,InternalImpl> &ptr ) : m_RefPtr(ptr) 
	{
#		ifdef _CPPRTTI
			m_ptr = dynamic_cast<TOCLASS*>( ptr.operator->() );
			if ( m_ptr == 0 )
				m_RefPtr = jsRefCountPtr<FROMCLASS,InternalImpl>((FROMCLASS*)0); // release it
#		else
			m_ptr = (TOCLASS*)ptr.operator->();
#		endif
	}


	///---------------------------------------------------------
	// copy constructor
	// 
	// [in] src object to copy
	//
	jsCastedRefCountPtr( jsCastedRefCountPtr &src )  
	{ 
		assign( src );
	}

	///---------------------------------------------------------
	// assignment operator
	// 
	// [in] src object to copy
	//
	// [Returns]
	//    this
	jsCastedRefCountPtr operator=( jsCastedRefCountPtr &src ) 
	{
		assign( src );
		return *this;
	}

	// this is a separate method since get infinite recursion
	// on operator= since "return *this;" calls copy constructor
	// which calls operator= which calls the copy...
	void assign( jsCastedRefCountPtr &src )
	{ 
		if ( this != &src ) 
		{ 
			m_RefPtr = src.m_RefPtr; 
			m_ptr = src.m_ptr; 
		} 
	}


	///---------------------------------------------------------
	// [Returns]
	//		a pointer to the contained object.  <b>Do not save 
	// off this pointer!</b>
	inline TOCLASS* operator->() { return m_ptr; }

	///---------------------------------------------------------
	// [Returns]
	//		a const pointer to the contained object.  <b>Do not save 
	// off this pointer!</b>
	inline const TOCLASS* operator->() const { return m_ptr; }

	///---------------------------------------------------------
	// [Returns]
	//		a reference to the contained object.  <b>Do not save 
	// off this reference!</b>
	inline TOCLASS &operator*() { return *(m_ptr); }

	///---------------------------------------------------------
	// [Returns]
	//		a const reference to the contained object.  <b>Do not save 
	// off this reference!</b>
	inline const TOCLASS &operator*() const { return *(m_ptr); }

	///---------------------------------------------------------
	// [Returns]
	//		true if the contained pointer is not null
	inline operator bool() const { return m_ptr != 0 ; }

	///---------------------------------------------------------
	// [Returns]
	//		true if the underlying pointers are equal
	inline bool operator==( const jsCastedRefCountPtr &p ) const 
			{ return m_RefPtr == p.m_RefPtr; }

	///---------------------------------------------------------
	// [Returns]
	//		true if the underlying pointers are not equal
	inline bool operator!=( const jsCastedRefCountPtr &p ) const 
			{ return !operator==(p); }

	///---------------------------------------------------------
	// [Returns]
	//		true if the underlying pointers are equal
	inline bool operator==( const jsRefCountPtr<FROMCLASS,InternalImpl> &p ) const 
			{ return m_RefPtr == p; }

	///---------------------------------------------------------
	// [Returns]
	//		true if the underlying pointers are not equal
	inline bool operator!=( const jsRefCountPtr<FROMCLASS,InternalImpl>	&p ) const 
			{ return !operator==(p); }


	///---------------------------------------------------------
	// [Returns]
	//    a reference counted pointer to the class this was casted from
	inline jsRefCountPtr<FROMCLASS,InternalImpl> CastedPtr() { return m_RefPtr; }

private:

	jsRefCountPtr<FROMCLASS,InternalImpl>	m_RefPtr;
	TOCLASS									*m_ptr;
};


#endif //__JSCASTEDREFCOUNTPTR_H__
