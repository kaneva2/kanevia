/******************************************************************************
 Name: jsRefCountPtr.h

  Description: 	reference counting point template

 Copyright (C) 1996, 2004 Jim Wallace

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 History:
 11/26/96		jmw created

 Notes: 

******************************************************************************/
#ifndef _jsRefCountPtr_H_
#define _jsRefCountPtr_H_

#ifndef _js_h_
	#error Include js.h before jsRefCountPtr.h
#endif

#ifdef _MSC_VER
#pragma warning( disable:4786 ) // template name too long
#endif

#include "jsLock.h"

template <class T,class InternalImpl> class jsRefCountPtr;

/***********************************************************
CLASS
	jsInternalRefCntPtrBase
	
	common implementation of counter object for jsRefCountPtr

DESCRIPTION
	See jsRefCountPtr for details.  
	
***********************************************************/
template <class T> class jsInternalRefCntPtrBase
{
public:
	inline jsInternalRefCntPtrBase( T * ptr, bool uselock ) : m_refCnt(1), m_ptr(ptr), m_lockingType(uselock) {}

	// someone else is using it
	inline virtual void		incCnt();

	// when count goes to 0, the item gets deleted
	inline virtual void		decCnt() = 0;

	T				*ptr() { return m_ptr; }
	int				refCnt() { return m_refCnt; }	// reference count for this ptr

protected:
	T				*m_ptr;
	int				m_refCnt;	// reference count for this ptr

	inline virtual ~jsInternalRefCntPtrBase() { jsAssert( m_ptr == 0 ); /* delete m_ptr; should never do this*/ }

	bool m_lockingType;

};

/***********************************************************
CLASS
	jsInternalRefCntPtr
	
	default implementation of counter object for jsRefCountPtr

DESCRIPTION
	See jsRefCountPtr for details.  By using this as the 
	internal implementation delete is called on the object
	with the ref count goes to zero.
	
***********************************************************/
template <class T> class jsInternalRefCntPtr : public jsInternalRefCntPtrBase<T>
{
public:
	inline jsInternalRefCntPtr( T * ptr, bool uselock ) : jsInternalRefCntPtrBase<T>( ptr, uselock ) {}

	// when count goes to 0, the item gets deleted
	inline virtual void		decCnt();
};

/***********************************************************
CLASS
	jsDeleteFnInternalRefCntPtr
	
	implementation of counter object for jsRefCountPtr that calls Delete()

DESCRIPTION
	See jsRefCountPtr for details.  By using this as the 
	internal implementation object->Delete() is called on the object
	with the ref count goes to zero.
	
***********************************************************/
template <class T> class jsDeleteFnInternalRefCntPtr : public jsInternalRefCntPtrBase<T>
{
public:
	inline jsDeleteFnInternalRefCntPtr( T * ptr, bool uselock ) : jsInternalRefCntPtrBase<T>( ptr, uselock ) {}

	// when count goes to 0, the item gets deleted
	inline virtual void		decCnt();
};


template <class T, class InternalImpl = jsInternalRefCntPtr<T> > 
/***********************************************************
CLASS
	jsRefCountPtr
	
	reference counted pointer

DESCRIPTION
	reference counting pointer.  Allows mutiple users of pointer
	to release it without worrying about deleting it from under 
	someone else

USAGE
	This class should be treated just like a regular pointer, only 
	you never delete it.  Once created, use -> or * to access the
	internal object, just like dereferencing a pointer.  Create a jsRefCounterPtr 
	on the stack, or as a member variable, never use new to allocate
	a jsRefCountPtr object or use a pointer to a jsRefCounterPtr
	object.  Follow the steps below to use it.  

	Usage:<ol>
	<li>Create a pointer you need a reference count on.
	<li>Create a jsRefCountPtr<myclassptr> object for the first object, 
		this creates an object with a refcount of 1
	<li>For each copy you need construct another jsRefCountPtr from the 
		first one. This increments the count.
	<li>As each jsRefCountPtr is destroyed, the refcount goes down.  When it
		reaches zero, the pointer you passed into step 2 is deleted
	</ol>

	You may use assignment (operator=) to copy jsRefCountPtrs and it 
	correctly sets the counts.  Same is true using the copy constructor.  Pass
	jsRefCountPtrs by reference into functions.  Never store a pointer or 
	reference to a jsRefCountPtr since that won't increment the count.

	Use the * and -> operators on jsRefCountPtr objects to get to the 
	internal pointer members.

TIPS
	-- The only time you can use a C++ reference to a jsRefCountPtr is when 
	passing it in as a function parameter.  Since a parameter is a temporary
	object, you really don't need to increment the count.  This avoids creating
	an extra object.
	
	If the function saves it off, it should always use a new object, not a
	reference or pointer to a jsCastedRefCountPtr.

	-- If you want to decrement the count on a jsCastedRefCountPtr, you can do
	this by assigning it to another jsCastedRefCountPtr created with the
	default constructor.  The default constructor creates an object with a 
	null pointer, and assigning it to your object will decrement the count,
	and assign the internal pointer to null.  You should not use -> or * after
	assigning null, or it will dereference null.  See below for an example.

	EXAMPLE

		jsRefCountPtr<MyObject> A( new MyObject );

		...

		A = jsRefCountPtr<MyObject>(); // assign default (null) to A.
									   // if A is last reference, internal object
									   // is deleted

	END

	-- There are two templates provided in jsTools for keeping the 
	count and deleting the object.  This is the second template parameter to 
	jsRefCountPtr and
	defaults to jsInternalRefCntPtr that uses <i>delete</i> to 
	delete the object.  jsDeleteFnInternalRefCntPtr calls a Delete()
	method on the object to delete it.  You may also supply your own, or 
	derive it from jsDeleteFnInternalRefCntPtr.

CASTING
	If you need to cast the internal pointer, use the jsCastedRefCountPtr 
	class to do this.

CAVEATS AND NOTES
	Although operator-> returns the actual internal pointer, saving that 
	will defeat the purpose of using the class.

	There is no direct manipulation of the reference count or
	a cast operator for that same reason.



***********************************************************/
class jsRefCountPtr
{
public:
	///---------------------------------------------------------
	// constructor from a pointer to an object.  
	// <b>*IMPORTANT*</b> do not delete the pointer or allow someone else to 
	// delete this pointer otherwise you defeat the purpose of this class
	// 
	// [in] ptr a pointer to an object.  May be null
	// [in] useLock true to synchonize increment and decrement of count
	//
	inline jsRefCountPtr( T *ptr = 0, bool useLock = true ) : m_ref( new InternalImpl(ptr, useLock) ) 
			{ /* we need default for use in array */ }

	// use this if you need access to the counts yourself, be careful!
	inline jsRefCountPtr( InternalImpl *ptr ) : m_ref( ptr ) 
			{ jsAssert( ptr ); m_ref->incCnt(); }

	///---------------------------------------------------------
	// copy constructor
	//
	// [in] s object to copy 
	inline jsRefCountPtr( const jsRefCountPtr &s ) : m_ref( s.m_ref ) { m_ref->incCnt(); }

	
	///---------------------------------------------------------
	// assignment operator.  allow use of p = q;  This decrements this
	// one and increments the new one
	// 
	// [in] s ref count pointer to assign to this one.
	inline jsRefCountPtr &operator=( const jsRefCountPtr &s );

	
	// GROUP: accessor operators

	///---------------------------------------------------------
	// [Returns]
	//		a pointer to the contained object.  <b>Do not save 
	// off this pointer!</b>
	inline T* operator->() { return m_ref->ptr(); }

	///---------------------------------------------------------
	// [Returns]
	//		a const pointer to the contained object.  <b>Do not save 
	// off this pointer!</b>
	inline const T* operator->() const { return m_ref->ptr(); }

	///---------------------------------------------------------
	// [Returns]
	//		a reference to the contained object.  <b>Do not save 
	// off this reference!</b>
	inline T &operator*() { return *(m_ref->ptr()); }

	///---------------------------------------------------------
	// [Returns]
	//		a const reference to the contained object.  <b>Do not save 
	// off this reference!</b>
	inline const T &operator*() const { return *(m_ref->ptr()); }

	///---------------------------------------------------------
	// [Returns]
	//		true if the contained pointer is not null
	inline operator bool() const { return m_ref->ptr() != 0 ; }

	///---------------------------------------------------------
	// [Returns]
	//		the current ref count on this object
	inline int	count() const { return m_ref->refCnt(); }

	///---------------------------------------------------------
	// [Returns]
	//		true if the underlying pointers are equal
	inline bool operator==( const jsRefCountPtr &p ) const { return m_ref->ptr() == p.m_ref->ptr(); }

	///---------------------------------------------------------
	// [Returns]
	//		true if the underlying pointers are not equal
	inline bool operator!=( const jsRefCountPtr &p ) const { return !operator==(p); }

	///---------------------------------------------------------
	// [Returns]
	//		true if the underlying pointers are equal
	inline bool operator==( const T *p ) const { return m_ref->ptr() == p; }

	///---------------------------------------------------------
	// [Returns]
	//		true if the underlying pointers are not equal
	inline bool operator!=( const T *p ) const { return !operator==(p); }

	///---------------------------------------------------------
	// destructor decrements the reference count
	~jsRefCountPtr() { m_ref->decCnt(); }

private:
	InternalImpl		 	*m_ref;		// this is *the* object that all these separate guys
										// point to.  It holds the count

	// this is not allowed, will assert on you.  
	inline jsRefCountPtr &operator=( T * /*ptr*/ ) { jsAssert(false); return *this; }

};


///---------------------------------------------------------
// macro to make a pointer.  The resulting typedef it
// <className>Ptr
// 
// [in] className the name of the class
#define MAKE_JS_REFCNT_PTR( className ) typedef jsRefCountPtr< className > className##Ptr;

template <class T,class InternalImpl> 
inline 	jsRefCountPtr<T,InternalImpl> &jsRefCountPtr<T,InternalImpl>::operator=( const jsRefCountPtr &s ) 
{ 
	if ( this != &s )
	{
		m_ref->decCnt(); 
		m_ref = s.m_ref; 
		m_ref->incCnt(); 
	}

	return *this; 
}

template <class T>
inline void	jsInternalRefCntPtr<T>::decCnt()
{ 
	long prevCount;
	if ( m_lockingType )
		prevCount = ::jsSyncDecrement(m_refCnt) - 1;
	else
		prevCount = --m_refCnt; 

	if ( prevCount == 0 ) 
	{ 
		delete m_ptr; 
		m_ptr = 0;
		delete this; 
	} 
}

template <class T>
inline void	jsInternalRefCntPtrBase<T>::incCnt() 
{ 
	if ( m_lockingType ) 
		::jsSyncIncrement(m_refCnt); 
	else 
		m_refCnt++; 
}

template <class T>
inline void	jsDeleteFnInternalRefCntPtr<T>::decCnt()
{ 
	long prevCount;
	if ( m_lockingType )
		prevCount = ::jsSyncDecrement(m_refCnt) - 1;
	else
		prevCount = --m_refCnt; 

	if ( prevCount == 0 ) 
	{ 
		if ( m_ptr )
		{
			m_ptr->Delete();
		}
		m_ptr = 0;
		delete this; 
	} 
}


#endif
 
 
 
 
 
