/******************************************************************************
LIBRARY
	jsTools

INTRODUCTION
	This is about the fourth time I've written a set of Operating System 
	wrappers.  The main idea is to avoid any API calls.  Any OS construct you 
	want to call should be wrapped in a portable class.  The reference 
	platforms are NT and Solaris right now.  Linux, and SCO Unixware is the next possible 
	platforms.  This technique has proved 
	successful on previous projects where portable code bases were written in
	Win32 (NT), Solaris, Unixware, and OS/2.  All of the program logic was 
	written in a portable manner without any ugly #ifdefs cluttering the code.  

COPYRIGHT DETAILS
	<BLOCKQUOTE>
	(c) Copyright 1996, 1999, 2000, 2004 Jim Wallace.

	This library is free software; you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public
	License as published by the Free Software Foundation; either
	version 2.1 of the License, or (at your option) any later version.

	This library is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public
	License along with this library; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
	</BLOCKQUOTE>

DOCUMENTATION
    The documentation for using jsTools is available in HTML by using Cocoon to
    generate it from the comments in these headers.  Note that code internal
    to jsTools is not documented in HTML, but standard comments are used.

BUILDING 
	<b>Defines used in all builds</b>
	_JSDBG turns on debugging calls such as jsAssert and jsTrace (jsOutputToDebugger)
	_NT must be defined for NT.  Other Windows platforms have not been tested.
	_UNIX must be defined if _NT is not.

	<b>Win32 static library</b>
	Build the library with _JSBUILD_LIB defined in the jsTools project.  Then
	in your project define _USE_LIB.  These defines prevent the classes from
	being exported.

	<b>Win32 DLL</b>
	Build the library with _JSBUILD_DLL defined in the jsTools project.  Then
	in your project do not use _USE_LIB (you can define _JSDLL if you want
	to be explicit, but the absence of _USE_LIB assumes you are using the DLL).  
	These export all the classes being exported.

	<b>Win32 Warning 4786</b>
	You will often get a 4786 Warning about the template name being too long.  
	jsTools suppresses that in some of its headers.  To suppress it, use the 
	following lines<br><code>

	#ifdef _MSC_VER
	#pragma warning (disable:4786)
	#endif
	</code>

	<b>Win32 Resource string loading</b>
	You should call std::string::RegisterResource in your main, WinMain, or DLLMain on
	process attach.
	This allows you to use std::string::loadString properly.  Call std::string::UnregisterResource 
	when you exit or in DLLMain when the process detaches.

	Note that when using the DLL version of jsTools, it links all the registered
	resources together, thus you must make sure all the string ids are unique across all
	the DLLs and the exe that use jsTools.  The debug build will write a message to the
	debugger if it finds the same string id in multiple resources.  This is important since
	you may be sprintf'ing into the loaded string, which may not be the one you expected, 
	which could crash.
	

VERSION HISTORY
	
	<b>New things in 9/00</b>
	<ul>
	<li> Added jsFolderWatcher to monitor directories for new files (Win32 only)
	<li> Added jsLogger, a COM object for doing jsLog-type logging.  This is 
	used when you may have multiple exes trying to log to the same file since it
	handles the serialization.
	<li> Added jsLogViewer tool.  This is an MFC-Win32 GUI application for monitoring
	files.
	<li> Added jsSafeArray and jssa* classes to wrap the Win32 SafeArray APIs.
	<li> Made std::string derive from basic_string and support char-type functionality.  
	For Unicode builds, std::string is wide, otherwise, MBCS.  Added the jsTCHAR.h header.
	</ul>

	<b>New things in 3/99</b>
	<ul>
	<li> Added jsLogOStream class for using the ostreams with the jsLog class 
	<li> Added jsSecurable class for wrapping NT's Security Descriptor
	<li> Added new jsEncodable and jsMetaClass classes as start to dynamic
	object instantiation from BER buffers.
	<li> Added new jsVerifyDo and jsVerifyThrow macros for asserting in debug builds
	<li> Added jsLibraryFunction class to wrap explicit DLL and shared library loading.
	<li> Changed std::string loadString to support use in library or DLL on Win32
	<li> Added jsConvertCommandLine to convert a Win32 (one string) command line to 
	argv/argc commands
	<li> Added jsEnumFiles
	<li> Added jsCastedRefCountPtr to downcast a jsRefCountPtr safely.
	</ul>
	
	<b>New things in 1/99</b>
	<ul>
	<li> Changed std::string to use STL.  This affected other areas that used the string
	<li> Added jsStopwatch class
	<li> Added memory mapped file class
	</ul>


******************************************************************************/
 
 
 
 
 
