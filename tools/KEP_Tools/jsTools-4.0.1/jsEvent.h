/******************************************************************************
 Name: jsEvent.h

 Description: definition of the portable event class

 Copyright (C) 1996, 2004 Jim Wallace

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 History:
 11/20/96		jmw created

 Notes: 

******************************************************************************/
#ifndef _jsEvent_h_
#define _jsEvent_h_

#ifndef _js_h_
	#error Include js.h before jsEvent.h
#endif

// forward decl of Implementation classes
class jsEventImpl;

/******************************************************************************
CLASS     
    jsEvent

	OS event class

DESCRIPTION
    one or more threads can wait on it
	Depending on constructor, one or all threads can be released
    
******************************************************************************/
class JSEXPORT jsEvent : public jsBase
{
public:
    ///
    // constructor
    //
    // [in] InitiallySetToHalt true if waiters are blocked after construction
    // [in] releaseAllOnRelease true if <b>all</b> waiters are released when
    //  the event triggers, and to halt them again, you must call
	// haltWaiters().  If false, only one thread is releases on each 
	// releaseWaiters()
	jsEvent( bool InitiallySetToHalt = true, bool releaseAllOnRelease = true );

    ///
    // wait for the event to be triggered
    //
    // [in] timoutMS how long to wait for the event in ms
    //
    // [Returns]
    //    WR_OK if event was triggered<br>
    //    WR_TIMEOUT if the timeout was hit<br>
    //    WR_ERROR if an error occurred
	jsWaitRet wait( ULONG timoutMS = INFINITE_WAIT ) const;

    ///
    // wait for the event to be triggered, but allow application
	// messages to be processed while waiting.
    //
    // [in] timoutMS how long to wait for the event in ms
    //
    // [Returns]
    //    WR_OK if event was triggered<br>
    //    WR_TIMEOUT if the timeout was hit<br>
    //    WR_ERROR if an error occurred
	jsWaitRet msgWait( ULONG timoutMS = INFINITE_WAIT ) const;

    ///
    // release one or all waiting threads, depending on constructor
    //
    // [Returns]
    //    true if no error
	bool releaseWaiters();

    ///
    // block all waiters.  
    //
    // [Returns]
    //    true if no error
	bool haltWaiters();

	~jsEvent();

private:
	jsEventImpl		*m_impl;
};


/******************************************************************************/
///
// little global fn to allow anyone to sleep, not just a thread
//
// [in] ms wait time in ms
inline void jsSleep( UINT ms ) // wait time in MS
{
	jsEvent e;
	e.wait( ms );
}

#endif 
 
 
 
 
 
