/******************************************************************************
 Name: jsLock.h

 Description: definition of the portable locking classes that are used
 with the synchronization classes in jsSync.h

 Copyright (C) 1996, 2004 Jim Wallace

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 History:
 11/20/96		jmw created

 Notes: 

******************************************************************************/
#ifndef _jsLock_h_
#define _jsLock_h_

#ifndef _js_h_
	#error Include js.h before jsLock.h
#endif

#include "jsSync.h"

// handy macros for using inside objects that have a lockable object
// inside them.  Put this where you need to lock and when it goes
// out of scope, it unlocks 

// read, write locks
#define RLOCK jsRWLock _READ_LOCK_( getSync(), true, LR_READONLY )
#define WLOCK jsRWLock _WRITE_LOCK_( getSync(), true, LR_READWRITE )

// Fast locks
#define FLOCK jsFastLock _Fast_LOCK_( getSync() )

// regular mutex-type lock
// There's a conflict LOCK on the Mac with MSL's mcompile.h
#define JSLOCK  jsLock _LOCK_( getSync(), true )

class jsFastLock;

/******************************************************************************
CLASS
	jsLock

	automatic unlocking mechanism for jsSync.

DESCRIPTION
	This object will automatically lock and unlock a jsSync.  This is 
	very useful since a return from anywhere will unlock a lock, thus 
	you can avoid potentail abandoned locks.
******************************************************************************/
class jsLock : public jsBase
{
public:
	///
	// constructor.  
	//
	// [in] sync the sync this will lock and unlock
	// [in] lockNow if true will lock the sync in the constructor
	jsLock( jsSync &sync, bool lockNow = false );

	///
	// unlock the sync
	void	unlock();

	///
	// lock the sync
	//
	// [Returns]
	//	the return value from the jsSync lock()
	jsWaitRet lock();

	///
	// try to lock the sync
	//
	// [Returns]
	//	the return value from the jsSync tryLock()
	jsWaitRet tryLock();

	///
	// see if the sync is locked
	//
	// [Returns]
	//	the return value from the jsSync peekLock()
	jsWaitRet peekLock();

	bool	locked() const { return m_locked > 0; }

	~jsLock();

private:
	jsSync		&m_sync;	
	ULONG		m_locked;

	void operator=( const jsLock & ); // avoids warning 
};

/******************************************************************************
CLASS
	jsFastLock

	automatic unlocking mechanism for jsFastSync.

DESCRIPTION
	This object will automatically lock and unlock a jsFastSync.  This is 
	very useful since a return from anywhere will unlock a lock, thus 
	you can avoid potentail abandoned locks.
******************************************************************************/
class jsFastLock : public jsBase
{
public:
	///
	// constructor.  
	//
	// [in] sync the sync this will lock and unlock
	// [in] lockNow if true will lock the sync in the constructor
	jsFastLock( jsFastSync &sync, bool lockNow = true ) : m_sync(sync), m_locked(0) 
	{ 
		if( lockNow) 
			lock(); 
	}

	///
	// lock the sync
	inline void	lock();

	///
	// unlock the sync
	inline void	unlock();

	bool	locked() const { return m_locked > 0; }

	~jsFastLock() { while ( locked() ) unlock(); }

private:
	ULONG			m_locked;  // sync itself protected this
	jsFastSync		&m_sync;

	void operator=( const jsFastLock & ); // avoids warning 
};
				
/******************************************************************************
CLASS
	jsLock

	automatic unlocking mechanism for jsRWSync.

DESCRIPTION
	This object will automatically lock and unlock a jsRWSync.  This is 
	very useful since a return from anywhere will unlock a lock, thus 
	you can avoid potentail abandoned locks.
******************************************************************************/
class jsRWLock : protected jsBase
{
public:
	///---------------------------------------------------------
	// constructor
	// 
	// [in] level the level of the lock for the lock manager's use
	// [in] name name for debugging
	// [in] lockNow if true then locks during the constructor
	// [in] lr how to lock it
	//
	jsRWLock( jsRWSync &sync, bool lockNow = false, jsLockReason lr = LR_READONLY );

	///---------------------------------------------------------
	// lock the sync object
	// 
	// [in] lr how to lock it
	//
	// [Returns]
	//    WR_OK if locked
	inline jsWaitRet lock( jsLockReason lr );

	///---------------------------------------------------------
	// try to lock the sync object
	// 
	// [in] lr how to lock it
	//
	// [Returns]
	//    WR_OK if locked<br>
	//	  WR_TIMEOUT if couldn't get the lock
	jsWaitRet tryLock( jsLockReason lr );

	///---------------------------------------------------------
	// see if the item is locked
	//
	// [in] lr how to lock it
	//
	// [Returns]
	//    WR_OK if locked<br>
	//	  WR_TIMEOUT if not locked
	jsWaitRet peekLock( jsLockReason lr );

	///---------------------------------------------------------
	// unlock the item.  You must hold the lock
	inline void	unlock();

	///---------------------------------------------------------
	// [Returns]
	//    true if locked
	bool	locked() const { return m_locked > 0; }
	

	///---------------------------------------------------------
	//	unlocks the lock
	~jsRWLock() { while ( locked() ) unlock(); }

private:
	jsRWSync		&m_sync;	// readWrite version of sync
	ULONG			m_locked;	// are we locked?
	jsLockReason	m_jsLockReason;// why we got the lock

	void operator=( const jsRWLock & ); // avoids warning
};


//==============================================================
// inline template functions for atomic operations
//==============================================================

///
// locking exchange of two objects.  The class must have operator=, 
// and copy operator for return.
// 
// [out] variable destination object 
// [in] newValue the object to set variable to
// [in] sync optional sync to use as a lock.  If 0 (default) it will
// use the jsFastSync::Global()
// 
// [Returns]
//	  the original value
template <class T>
inline T jsSyncExchange( T &variable,  const T &newValue, jsFastSync *sync = 0 )
{
	// add valid checking since having problems with initializiation
	// of global data on Unix
	jsFastSync *qs = (sync==0?&jsFastSync::Global():sync);
	if ( qs && qs->ok() )
		qs->lock();

	T oldValue = variable; 
	variable = newValue;

	if ( qs && qs->ok() )
		qs->unlock();
	return oldValue;
}

///
// locking increment.  The class must be able to 
// have int added to it, operator+=(int), and copy for return.
// 
// [out] variable object to increment
// [in] inc amount to increment
// [in] sync optional sync to use as a lock.  If 0 (default) it will
// use the jsFastSync::Global()
// 
// [Returns]
//	  the original value
template <class T>
inline T jsSyncIncrement( T &variable, int inc, jsFastSync *sync )
{
	// add valid checking since having problems with initializiation
	// of global data on Unix
	jsFastSync *qs = (sync==0?&jsFastSync::Global():sync);
	if ( qs && qs->ok() )
		qs->lock();

	T oldValue = variable; 
	variable += inc;

	if ( qs && qs->ok() )
		qs->unlock();
	return oldValue;
}

///
// locking decrement.  The class must be able to 
// have int added to it, operator+=(int), and copy for return.
// 
// [out] variable object to decrement
// [in] inc amount to decrement
// [in] sync optional sync to use as a lock.  If 0 (default) it will
// use the jsFastSync::Global()
// 
// [Returns]
//	  the original value
template <class T>
inline T jsSyncDecrement( T &variable, int dec, jsFastSync *sync )
	{ return jsSyncIncrement( variable, -dec, sync ); }

//template <class T> T jsSyncExchange( T &variable, const T &newValue, jsFastSync *sync );
// 
// [Returns]
//	  the original value
#if defined _MSC_VER && _MSC_VER < 1100
	template <class T>
	inline T jsSyncExchange( T &variable, const T &newValue ) { return jsSyncExchange( variable, newValue, (jsFastSync*)0); }
#endif

// class must be able to have int added to it
// 	operator+=(int), and copy for return
//template <class T> T jsSyncIncrement( T &variable, int inc, jsFastSync *sync  );
// 
// [Returns]
//	  the original value
template <class T>
inline T jsSyncIncrement( T &variable, int inc = 1 ) { return jsSyncIncrement( variable, inc, (jsFastSync*)0 ); }

// class must be able to have int added to it
// 	operator+=(int), and copy for return
// template <class T> T jsSyncDecrement( T &variable, int dec, jsFastSync *sync );
// 
// [Returns]
//	  the original value
template <class T> T
inline jsSyncDecrement( T &variable, int dec = 1 ) { return jsSyncIncrement( variable, -dec, (jsFastSync*)0 ); }

//==============================================================
// inline functions 
//==============================================================
inline jsWaitRet jsLock::lock()
{
	jsSyncIncrement(m_locked);
	return m_sync.lock();
}

inline jsLock::jsLock( jsSync &sync, bool lockNow )
	: m_sync(sync), m_locked(0)
{
	if ( lockNow )
		lock();
}

inline jsRWLock::jsRWLock(
	jsRWSync &sync,
	bool lockNow,	// = false
	jsLockReason lr	//= LR_READONLY
	) :
		m_sync(sync), m_locked(0), m_jsLockReason(LR_NOLOCK)
{
	if ( lockNow )
		lock( lr );
}

inline jsWaitRet jsRWLock::lock( jsLockReason lr )
{
	jsWaitRet wr = m_sync.lock(lr);
	if ( wr == WR_OK )
	{
		jsFastSync::Global().lock();
		m_locked++;
		m_jsLockReason = lr;
		jsFastSync::Global().unlock();
	}

	return wr;
}

inline jsWaitRet jsRWLock::tryLock( jsLockReason lr )
{
	jsWaitRet wr = m_sync.tryLock(lr);
	if ( wr == WR_OK )
	{
		jsFastSync::Global().lock();
		m_locked++;
		m_jsLockReason = lr;
		jsFastSync::Global().unlock();
	}

	return wr;
}

inline jsWaitRet jsRWLock::peekLock( jsLockReason lr )
{
	return m_sync.peekLock( lr );
}

inline void jsRWLock::unlock()
{
	// check both to avoid race
	if ( m_locked && jsSyncDecrement(m_locked) >= 1 )
		m_sync.unlock(m_jsLockReason);
}

inline	void jsLock::unlock()
{
	// check both to avoid race
	if ( m_locked && jsSyncDecrement(m_locked) >= 1 )
		m_sync.unlock();
}

inline jsLock::~jsLock() 
{ 
	while ( locked() ) 
		unlock(); 
}


inline jsWaitRet jsLock::tryLock()
{
	jsWaitRet wr = m_sync.tryLock();
	if ( wr == WR_OK )
		jsSyncIncrement(m_locked);
	return wr;
}

inline jsWaitRet jsLock::peekLock()
{
	return m_sync.peekLock();
}

inline void jsFastLock::lock() 
{ 
	m_sync.lock();		// void, too
	m_locked++;

#if defined( _JSDBG ) && !defined( _NT )
	if ( m_locked > 1 )
		jsTrace( "jsFastLock locked recursively!!" );
#endif
}

inline void jsFastLock::unlock() 
{ 
	if ( m_locked > 0 )
	{
		m_locked--;
		m_sync.unlock(); // assume always works, since a second unlock will probably fail, too
	}
}

#endif

 
 
 
 
 
