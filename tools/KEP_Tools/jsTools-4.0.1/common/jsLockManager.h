/******************************************************************************

 Name: jsLockManager.h
 
 Description: the lock manager is used to help avoid deadlocks using 
 the jsTools

 Copyright (C) 1996, 2004 Jim Wallace

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 History:
 11/26/96		jmw created

 Notes: 

******************************************************************************/
#ifndef _LockManager_h_
#define _LockManager_h_

#ifndef _js_h_
	#error Include js.h before jsLockManager.h
#endif

#include "jsSync.h"

// shorthand macro
#define jsLM (jsLockManager::theLockManager)

/******************************************************************************
 Description: Warns when a deadlock is near in debug version ONLY

******************************************************************************/
class jsLockManager
{
public:
		
	static ULONG timeoutWarningMS;			// since public anyone may set, defaults to 2000 MS
	static jsLockManager *theLockManager;	// one and only lock manager
	
	#if defined(_JSUSELM) 
	// only the debug version does the work

			jsLockManager();
			~jsLockManager();
			bool notifyLock(BYTE level);
			bool notifyUnlock(BYTE level);

			static bool checkLevel( const char* name, BYTE level );

	private:	
			jsFastSync m_LockMgrSync; // protect the member data

			BYTE *allocList();

			void freeList(BYTE * list);
			vector<BYTE>	m_levels;
			int				m_listIndex; // for thread local data

			vector<BYTE *> freeLists;
			BYTE *lockMgrList();

	#else // non debug version does nothing
			jsLockManager() {};
			~jsLockManager() {};
			bool notifyLock(BYTE level) {return true;}
			bool notifyUnlock(BYTE level) {return true;}

			static bool checkLevel( const char* name, BYTE level ) {return true;}
	#endif
};


#endif
 
 
 
 
 
