/******************************************************************************
 Name: jsEvent.cpp

 Description: implementation the event class, mostly passthrus

 Copyright (C) 1996, 2004 Jim Wallace

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 History:
 11/26/96		jmw created

 Notes: 

******************************************************************************/
#include "js.h"

#include <jsSyncImpl.h>
#include "jsLock.h"


/******************************************************************************
	event passthrus
******************************************************************************/
jsEvent::jsEvent( 
	bool InitiallySetToHalt,	// = true
	bool releaseAllOnRelease	// = true 
	) : m_impl( new jsEventImpl( InitiallySetToHalt, releaseAllOnRelease ) )
{
	setOk( m_impl && m_impl->ok() );
}

jsWaitRet jsEvent::wait( 
	ULONG timeoutMS	// = WAIT_INFINITE 
	) const
{
	if ( ok() )
		return m_impl->wait( timeoutMS );
	else
		return WR_ERROR;
}

jsWaitRet jsEvent::msgWait( 
	ULONG timeoutMS	// = WAIT_INFINITE 
	) const
{
	if ( ok() )
		return m_impl->msgWait( timeoutMS );
	else
		return WR_ERROR;
}

bool jsEvent::releaseWaiters()
{
	if ( ok() )
		return m_impl->releaseWaiters();
	else
		return false;
}


bool jsEvent::haltWaiters()
{
	if ( ok() )
		return m_impl->haltWaiters();
	else
		return false;
}

jsEvent::~jsEvent()
{
	delete m_impl;
}
#ifdef _MSC_VER
// for template sign/unsign mismatch for some reason this removes them
#pragma warning (disable : 4018 ) 
#endif
 
