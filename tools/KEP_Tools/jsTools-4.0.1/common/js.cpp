/******************************************************************************
 Name: js.cpp

 Description: some common functions.  Here's where the #ifdef rule is broken.

 Copyright (C) 1996 Jim Wallace

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 History:
 11/26/96		jmw created

 Notes: 

******************************************************************************/
#include "js.h"

#ifdef _JSDBG
	// for ... parm on print to debugger
	#include "stdio.h"
	#include "stdarg.h"
#ifdef _JSCFGASSERT	
	#include "jsConfiguration.h"
#endif
#endif

#ifdef _NT
	#pragma warning ( disable : 4201 4214 4115 4514 )

	#include "windows.h" 

	#pragma warning ( default : 4201 4214 4115 )

	void jsBeep() { ::MessageBeep( -1 ); }

#else
	void jsBeep() { _tprintf( "%c", '\a' ); }
#endif

#ifndef _MSC_VER
	#include <assert.h> // use VC++ calls if have 'em
#endif

#include "ustring.h"

/******************************************************************************

 Description: this is were all the asserts and verifies end up

 make sure can't recursively call this, right now
 can't since only one fn called

******************************************************************************/
bool jsAssertFunction( 
	bool expr,		// if false, then error
	const char *file,	// file name for diag, always single byte 
	int line		// line # for diag
	)
{
	if ( !expr )
	{
		jsOutputToDebugger( "Assert at %d in %s", line, (const char*)_U(file) );

		#if defined(_JSDBG) & defined(_JSCFGASSERT)
		jsConfiguration cfg;
		if ( cfg.ok() )
		{
			int beeps = cfg.get("Debug", "AssertBeeps", 0 );
			for ( int i = 0; i < beeps; i++ )
				jsBeep();

			if ( cfg.get("Debug", "AssertBreak", 1 ) == 0 )
				return expr;  // don't break

			// get line number
			char p[12];
			_stprintf( p, "%d", line );

			// get file name (single byte)
			const char* q = MAX( strrchr( file, '\\' ), strrchr( file, '/' ) );
			if ( !q || !(*(q+1)) )
				q = file;
			else
				++q;
			
			std::string key( "Debug" );
			key += "\\";
			key += _U(q);

			if ( cfg.get( key, p, 0 ) == 1 ) // ignore list is key of file, value name line
				return expr;
		}
		#endif

		jsBreakIntoDebugger();
	}

	return expr;
}

/******************************************************************************

 Description:  thread-safe print to debugger
	for now just used stack for buffer, but could lock
	DO NOT ASSERT here since assert code calls this

******************************************************************************/
void jsOutputToDebugger( const char* s, ... )
{
	#ifdef _JSDBG
		char p[255];
		va_list marker;

		va_start( marker, s );

		_vsnprintf( p, 255, s, marker );

		#ifdef _NT
			::OutputDebugStringA( p );
			::OutputDebugStringA( "\r\n" );
		#else
			fprintf( stderr, "%s\n", p );
		#endif

		va_end( marker );
	#endif
}

/******************************************************************************

 Description: interupt the program into the debugger
 
 i'm not sure what to do here for non-intel platforms

******************************************************************************/
void jsBreakIntoDebugger()
{
	#ifdef _JSDBG
		#ifdef _MSC_VER
 			_asm { int 3 }
		#else
			assert( false );
		#endif
	#endif
}

#ifdef _NT

ULONG jsTickCount()	{ return ::GetTickCount(); }

#elif defined(_UNIX)

	#include <sys/time.h> // for gettimeofday
	ULONG jsTickCount() 
	{
		struct timeval  tp;

		::gettimeofday( &tp, 0 ); // gets seconds and mircosecs since 6/1/90?

		// strip off some of msb to avoid always overflowing
		return (tp.tv_sec & 0x000fffff) * 1000 + tp.tv_usec / 1000;
	}

#elif defined(_MAC)

	ULONG jsTickCount()
	{
		// Use MacOS Microseconds call to get system time
		unsigned long long	time;
		::Microseconds( (UnsignedWide *) &time );
		
		// Divide by 1000 to get milliseconds
		return (ULONG) ((time + 500LL) / 1000LL);
	}

#else
	#error Neither _MAC, _UNIX or _NT is defined!
#endif

#ifdef _MSC_VER
// for template sign/unsign mismatch for some reason this removes them
#pragma warning (disable : 4018 ) 
#endif
 
