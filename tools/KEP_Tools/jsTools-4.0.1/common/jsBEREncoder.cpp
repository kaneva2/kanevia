/******************************************************************************
 Name: jsBEREncoder.cpp

 Description: encoder class for 

 Copyright (C) 1996, 2004 Jim Wallace

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 History:
 2/23/99		jmw created

 Notes: 

******************************************************************************/
#include "js.h"
#include "jsBEREncoder.h"

#include <float.h> // for finite

/**********************************************************************
 Description:  copy part of a coder into this one
**********************************************************************/
jsBEREncoder::jsBEREncoder( 
		const jsBEREncoder &coder, 
		ULONG posInSrc,			// where to copy from 0 for start
		ULONG len ) :			// number of bytes to copy, we'll check
								// to see if overrun maxlen
		m_freeBuffer(true), m_bufferSize(0), 
		m_buffer(0), m_putOffset(0), m_inc(1024), m_incBig(1024),
		m_reallocFn(0), m_reallocULONG(0),
		m_nextCode(jsBERDecoder::nextAny)
{
	// check to make sure the values passed in are valid 
	jsVerifyReturnVoid( len != 0 );

	jsVerifyReturnVoid( coder.bufferSize() >= posInSrc + len );

	m_bufferSize = len;
	m_buffer = new BYTE[m_bufferSize];

	if ( m_buffer == 0 )
		throw new jsBEREncodeException( "assureLength", jsBEREncodeException::noMemory );


	::memcpy( m_buffer, coder.buffer() + posInSrc, len );
}

jsBEREncoder::jsBEREncoder( ULONG maxBuffer /*= 4096*/, ULONG inc /*= 1024*/ ) : 
		m_freeBuffer(true), 
		m_bufferSize(maxBuffer), 
		m_buffer(new BYTE[maxBuffer]), 
		m_putOffset(0), 
		m_inc(inc), 
		m_incBig(inc), 
		m_reallocFn(0), 
		m_reallocULONG(0), 		
		m_nextCode(jsBERDecoder::nextAny)
{ 
	if ( m_buffer == 0 ) 
	{
		m_bufferSize = 0; 
	}
	if ( m_inc == 0 )
		m_inc = 1024; 

	if ( m_incBig == 0 )
		m_incBig = 1024; 
}


/**********************************************************************
 Description:  static fn that puts a number of a given length 
 at a given pointer
**********************************************************************/
void jsBEREncoder::encodeNumber( ULONG length, BYTE lenType, PBYTE &ptr ) 
{

	if ( lenType == FOUR_BYTE_LENGTH )
	{
		*(ptr++) = (BYTE)(length >> 24);
	}

	if ( lenType == THREE_BYTE_LENGTH ||
		 lenType == FOUR_BYTE_LENGTH )
	{
		*(ptr++) = (BYTE)(length >> 16);
	}

	if ( lenType == TWO_BYTE_LENGTH ||
		 lenType == THREE_BYTE_LENGTH ||
		 lenType == FOUR_BYTE_LENGTH )
	{
		*(ptr++) = (BYTE)(length >> 8);
	}

	*(ptr++) = (BYTE)length;
}


/**********************************************************************
 Description:  encodes a length into the buffer

 Returns: number of bytes moved
**********************************************************************/
ULONG jsBEREncoder::encodeLength( ULONG length ) 
{
	throwIfNoRoom( calcLengthLength( length ) );

	ULONG bytesMoved = encodeLength( length, m_buffer+m_putOffset );

	m_putOffset += bytesMoved;

	return bytesMoved;
}

/**********************************************************************
 Description:  static fn to encode a length at a given ptr

 Returns: number of bytes moved
**********************************************************************/
ULONG jsBEREncoder::encodeLength( ULONG length, PBYTE ptr )
{
	/*
	 * The long variant of length must be used if the octet string is longer
	 * than 127 bytes.  The upper bit of the length byte is set and the lower
	 * bits indicate the number of length bytes which will follow.
	 */
	if (length > 127)
	{
		BYTE lenType;
		if ( length > 0xffffff )
		{
			lenType = FOUR_BYTE_LENGTH;
		}
		else if ( length > 0xffff )
		{
			lenType = THREE_BYTE_LENGTH;
		}
		else 
		{
			lenType = TWO_BYTE_LENGTH;
		}

		*(ptr++) = lenType;

		encodeNumber( length, lenType, ptr );

		return  1+( LENGTH_MASK & lenType );
	}
	else // one byte length
	{
		encodeNumber( length, ONE_BYTE_LENGTH, ptr );
		return 1*sizeof(BYTE);
	}
}

/**********************************************************************
 Description:  start a constructed object where we don't know the length yet
**********************************************************************/
void jsBEREncoder::beginConstructed( jsBERTag tag, BYTE lenType )
{
	throwIfNoRoom( calcTagLength( tag ) + (lenType & LENGTH_MASK ) );

	encodeTag( tag );

	switch ( lenType )
	{
		case INDEFINITE_LENGTH:
			// not supported
			throw new jsBEREncodeException( "beginConstructed", jsBEREncodeException::badParam );

/*			m_buffer[m_putOffset++] = lenType;
			m_patchArray.push_back( PatchLength( tag, 0, lenType ) ); 
			m_putOffset += 1; // skip over it
*/			break;

		case ONE_BYTE_LENGTH:
			m_patchArray.push_back( PatchLength( tag, m_putOffset, lenType ) );
			m_putOffset++;
			break;

		default:
			m_buffer[m_putOffset++] = lenType;
			m_patchArray.push_back( PatchLength( tag, m_putOffset, lenType ) );

			m_putOffset += ( LENGTH_MASK & lenType ); // holding area

			break;
	};
}

/**********************************************************************
 Description:  end the constructed object 
**********************************************************************/
void jsBEREncoder::endConstructed( jsBERTag tag )
{
	// must have called startConstructed and called it with this
	// lentype last
	jsVerifyReturnVoid( m_patchArray.size() != 0 );

	switch ( m_patchArray[m_patchArray.size()-1].lenType() )
	{
		case INDEFINITE_LENGTH: // add end of contents
			// not supported
			throw new jsBEREncodeException( "endConstructed", jsBEREncodeException::badParam );
/*			
			jsVerifyReturnVoid(*(m_buffer+(m_patchArray[m_patchArray.size()-1].offset()-2)) == tag );
			m_buffer[m_putOffset++] = END_OF_CONTENTS;
			m_buffer[m_putOffset++] = END_OF_CONTENTS;
*/			break;

		default: // all other lengths just put the length back 
#			ifdef _DEBUG
				ULONG tagLen = calcTagLength(tag);
				const BYTE *tagPtr = 0;
				if ( m_patchArray[m_patchArray.size()-1].lenType()  == ONE_BYTE_LENGTH )
				{
					tagPtr = m_buffer+(m_patchArray[m_patchArray.size()-1].offset()-tagLen);
				}
				else
				{
					tagPtr = m_buffer+(m_patchArray[m_patchArray.size()-1].offset()-(tagLen+1));
				}
				for ( UINT i = 0; i < tagLen; i++ )
				{
					jsAssert( tagPtr[i] == (BYTE)(tag >> ((3-i)*8)));
				}
#			endif

			PatchLength patch( m_patchArray[m_patchArray.size()-1] );
			m_patchArray.pop_back();

			PBYTE ptr = m_buffer + patch.offset();

			// see if length fits where they thought it might

			// subtract the length of the 'length' from the total length 
			ULONG length = curPtr() - (ptr + patch.lenLength()); 
			if ( patch.lenLength() > 1 )
				length++; // take into account the length of length byte if > 1

			if ( jsBEREncoder::calcLengthLength( length ) == patch.lenLength() )
			{
				// guessed right
				encodeNumber( length, patch.lenType(), ptr );
			}
			else if ( jsBEREncoder::calcLengthLength( length ) < patch.lenLength()  )
			{
				// guessed too big, this should be ok, just waste up to 3 bytes
				encodeNumber( length, patch.lenType(), ptr );
			}
			else  // ( jsBEREncoder::calcLengthLength( length ) > patch.lenLength()  )
			{
				// guessed too small, we need to move the buffer out so we can
				// get enough room for the length
				ULONG diff = jsBEREncoder::calcLengthLength( length ) - patch.lenLength();

				// make sure we have enough room
				assureLength( curSize()+diff );

				ptr = m_buffer + patch.offset(); // in case it moved

				if ( patch.lenLength() > 1 )
					ptr--;  // encode the whole new length, over the previous one

				::memmove( ptr+patch.lenLength()+diff, ptr+patch.lenLength(), length );

				encodeLength( length, ptr );

				// add to the putoffset, too
				m_putOffset += diff;
			}

			break;
	}
		
}

/**********************************************************************
 Description:  static fn to calculate how long this number will be 
 when encodes as a length

 Returns: the length
**********************************************************************/
int jsBEREncoder::calcLengthLength( ULONG len )
{
	if ( len < 0x80 )			// 0-127 take 1 byte
		return 1;
	else if ( len <= 0xffff )
		return 3;
	else if ( len <= 0xffffff )
		return 4;
	else 
		return 5;
		
}

/**********************************************************************
 Description:  static fn to calculate how long this number will be 
 when encodes as a length

 Returns: the length
**********************************************************************/
int jsBEREncoder::calcIntegerLength( LONG value )
{
	if ((value >= -128) && (value < 128))
	{
		return 1;
	}
	else if ((value >= -32768L) && (value < 32768L))
	{
		return 2;
	}
	else if ((value >= -8388608L) && (value < 8388608L))
	{
		return 3;
	}
	else
	{
		return 4;
	}
}

/**********************************************************************
 Description:  static fn to calculate the length tag of a number
 that is a length

 Returns: the length tag
**********************************************************************/
BYTE jsBEREncoder::calcLengthType( ULONG len )
{
	if ( len < 0x80 )
		return ONE_BYTE_LENGTH;
	else if ( len <= 0xffff )
		return TWO_BYTE_LENGTH;
	else if ( len <= 0xffffff )
		return THREE_BYTE_LENGTH;
	else 
		return FOUR_BYTE_LENGTH;
		
}

/**********************************************************************
 Description:  assure we have this much memory allocated, and copy
 some data, too

 Returns: a pointer to the area after what we copied
**********************************************************************/
PBYTE jsBEREncoder::assureLengthRewind( ULONG len, PCBYTE somedata, ULONG somedatalen )
{
	jsAssert( m_patchArray.size() == 0 ); 

	if ( m_bufferSize < len ) // must reallocate
	{
		if ( m_reallocFn )
		{
			m_buffer = m_reallocFn( m_buffer, m_reallocULONG, len );
		}
		else
		{
			if ( !m_freeBuffer )
			{
				jsAssert( false );
				throw new jsBEREncodeException( "assureLengthRewind", jsBEREncodeException::cantReallocUserBuffer );
			}

			delete m_buffer;
			m_bufferSize = len+(1024-(len%1024));
			m_buffer = new BYTE[m_bufferSize ]; // round up to 1K
		}

		if ( m_buffer == 0 )
			throw new jsBEREncodeException( "assureLength", jsBEREncodeException::noMemory );
	}

	rewind();

	if ( somedatalen )
	{
		memcpy( m_buffer, somedata, somedatalen );
		m_putOffset = somedatalen;
	}

	return m_buffer + somedatalen;
}

/**********************************************************************
 Description:  append the buffer from the coder passed in to the 
 end of this one

**********************************************************************/
void jsBEREncoder::appendCoder( const jsBEREncoder &coder )
{
	// need to assure length
	assureLength( curSize() + coder.curSize() );

	::memcpy( m_buffer + m_putOffset, coder.buffer(), coder.curSize() );

	m_putOffset += coder.curSize();
}

/**********************************************************************
 Description:  assure the length of the buffer and optionally grow
 the buffer out by offset bytes, moving the buffer down

  throws expections if errors
 *********************************************************************/
void jsBEREncoder::assureLength( 
	ULONG newLength,	// new length of buffer
	ULONG offset		// where in the new buffer old buffer starts, usu 0
						// but if caller plans on prefixing the buffer
						// this can avoid double copy
	)
{
	if ( newLength > bufferSize() ||
		 curSize() + offset > bufferSize() ) 
	{
		// Increase the allocation chuck size, if the requested buffer
		// increase size is large. This allows the buffer size to grow
		// in larger chunks, if the items being added are of a large size.
		// Results in fewer allocs and memcpy calls. (faster)
		ULONG this_incr = 8 * (newLength - curSize() );
		if(this_incr > m_incBig)
		{
			m_incBig = this_incr;
			m_incBig = MIN( m_incBig, 0x4000 );
		}

		ULONG maxLen = MAX( newLength, curSize() + offset );
		maxLen += m_incBig;
		ULONG newSize = maxLen -(maxLen%m_inc); // round dn to increment

		BYTE *temp = 0;

		if ( m_reallocFn )
		{
			temp = m_reallocFn( m_buffer, m_reallocULONG, newSize );
			if ( temp == 0 )
				throw new jsBEREncodeException( "assureLength", jsBEREncodeException::noMemory );

			if ( offset > 0 )
				::memmove( temp+offset, temp, curSize() );

			m_bufferSize = newSize;
		}
		else
		{
			if ( !m_freeBuffer )
			{
				jsAssert( false );
				throw new jsBEREncodeException( "assureLength", jsBEREncodeException::cantReallocUserBuffer );
			}

			// must reallocate
			temp = new BYTE[ newSize ]; 

			if ( temp == 0 )
				throw new jsBEREncodeException( "assureLength", jsBEREncodeException::noMemory );

			m_bufferSize = newSize;

			::memcpy( temp+offset, m_buffer, curSize() );

			delete m_buffer;
		}


		m_buffer = temp;
	}
	else if ( offset > 0 )
	{
		// just move it
		::memmove( m_buffer+offset, m_buffer, curSize() );
	}
	m_putOffset += offset;
}

/**********************************************************************
 Description:  add something to either end or both ends to the coder
 Note that any parm may be 0.  
**********************************************************************/
bool jsBEREncoder::prefixSuffix( PCBYTE prefix, ULONG prefixLen, PCBYTE suffix, ULONG suffixLen )
{
	jsVerifyReturn( prefixLen && prefix || suffixLen && suffix || 
					prefixLen == 0 && suffixLen == 0, false );

	bool bRet = false;
	if ( prefixLen || suffixLen ) // something to do
	{
		// see if we have enough space, move buffer out 
		// for prefix
		assureLength( curSize()+prefixLen+suffixLen, prefixLen );

		if ( prefixLen )
		{
			// copy into empty space allocated by assure length
			::memcpy( m_buffer, prefix, prefixLen );
		}
		if ( suffixLen )
		{
			::memcpy( m_buffer+curSize(), suffix, suffixLen );
			m_putOffset += suffixLen;
		}
		bRet = true;
	}

	return bRet;
}

/**********************************************************************
 Description:  enocde a tag 

  throws expections if errors

 Returns: number of bytes used to encode this item

 *********************************************************************/
ULONG jsBEREncoder::encodeTag( jsBERTag tag )
{
	int tagLen = calcTagLength( tag );

	throwIfNoRoom( tagLen ); 

	for ( int i = 0; i < tagLen; i++ )
		m_buffer[m_putOffset++] = (BYTE)(tag >> (3-i)*8); 

	return tagLen;
}

int jsBEREncoder::calcTagLength( jsBERTag tag )
{
	// calculate the length
	if ( ((tag >> 24 ) & HIGH_TAG_TYPE) == HIGH_TAG_TYPE )
	{
		ULONG ret = 2; // at least two bytes;

		if ( tag & 0x00800000 )
			ret++;

		if ( tag & 0x00008000 )
			ret++;

		return ret;
	}
	else
		return 1;
}

/**********************************************************************
 Description:  enocde a boolean value

  throws expections if errors

 Returns: number of bytes used to encode this item

 *********************************************************************/
ULONG jsBEREncoder::encodeBoolean ( bool value, jsBERTag tag )
{
	ULONG startOffset = m_putOffset;

	/*
	 * The boolean value is encoded with a boolean tag and a length byte 
	 * of one followed by the boolean value.  Any value other than zero passed
	 * in will result in 0xff being encoded as the boolean true value.
	 */
	encodeTag( tag );

	throwIfNoRoom( 2 );	// bool takes two bytes

	m_buffer[m_putOffset++] = 0x01; // length

	if (value != 0)
		m_buffer[m_putOffset++]  = 0xff;
	else
		m_buffer[m_putOffset++]  = 0x00;

	return m_putOffset - startOffset;
}

/**********************************************************************
 Description:  enocde a enumerated value

  throws expections if errors

 Returns: number of bytes used to encode this item

 *********************************************************************/
ULONG	jsBEREncoder::encodeEnumerated (	int	value, jsBERTag tag )
{
	ULONG startOffset = m_putOffset;

	/*
	 * The enumerated value is encoded with an enumerated tag and a length byte 
	 * followed by the necessary number of data bytes.
	 */
	encodeTag( tag );

	/*
	 * Encodings for enumerated types are the same as the integer value
	 * which represents it, except that the tag is different.
	 */
	if ((value >= -128) && (value < 128))
	{
		throwIfNoRoom( 2 ); 

		m_buffer[m_putOffset++] = 0x01; // length
		m_buffer[m_putOffset++] = (BYTE)value;
	}
	else
	{
		throwIfNoRoom( 3 ); // three more than tag

		m_buffer[m_putOffset++]  = 0x02;
		m_buffer[m_putOffset++]  = (BYTE)(value >> 8);
		m_buffer[m_putOffset++]  = (BYTE)value;
	}

	return m_putOffset - startOffset;
}

/**********************************************************************
 Description:  enocde an integer value

  throws expections if errors

 Returns: number of bytes used to encode this item

 *********************************************************************/
ULONG jsBEREncoder::encodeInteger (	LONG	value, jsBERTag tag /*= INTEGER_TAG */)
{
	ULONG startOffset = m_putOffset;

	encodeTag( tag );

	/*
	 * The integer value is encoded with an integer tag and a length byte 
	 * followed by the necessary number of data bytes.
	 */
	if ((value >= -128) && (value < 128))
	{
		throwIfNoRoom( 2 ); 

		m_buffer[m_putOffset++] = 0x01;
		m_buffer[m_putOffset++] = (BYTE)value;
	}
	else if ((value >= -32768L) && (value < 32768L))
	{
		throwIfNoRoom( 3 ); // three more than tag

		m_buffer[m_putOffset++] = 0x02;
		m_buffer[m_putOffset++] = (BYTE)(value >> 8);
		m_buffer[m_putOffset++] = (BYTE)value;
	}
	else if ((value >= -8388608L) && (value < 8388608L))
	{
		throwIfNoRoom( 4 ); // four more than tag

		m_buffer[m_putOffset++] = 0x03;
		m_buffer[m_putOffset++] = (BYTE)(value >> 16);
		m_buffer[m_putOffset++] = (BYTE)(value >> 8);
		m_buffer[m_putOffset++] = (BYTE)value;
	}
	else
	{
		throwIfNoRoom( 5 ); // five more than tag

		m_buffer[m_putOffset++] = 0x04;
		m_buffer[m_putOffset++] = (BYTE)(value >> 24);
		m_buffer[m_putOffset++] = (BYTE)(value >> 16);
		m_buffer[m_putOffset++] = (BYTE)(value >> 8);
		m_buffer[m_putOffset++] = (BYTE)value;
	}

	return m_putOffset - startOffset;
}


/**********************************************************************
 Description:  enocde an integer value

  throws expections if errors

 Returns: number of bytes used to encode this item

 *********************************************************************/
ULONG jsBEREncoder::encodeLongInteger ( LONGLONG	value, jsBERTag tag /*= INTEGER_TAG */)
{
	if ( value >= -0x80000000i64 && value < 0x80000000i64 )
		return encodeInteger( (LONG)value, tag );
	
	ULONG startOffset = m_putOffset;

	encodeTag( tag );

	/*
	 * The integer value is encoded with an integer tag and a length byte 
	 * followed by the necessary number of data bytes.
	 */
	if ( value >= -0x800000000i64 && value < 0x800000000i64 )
	{
		throwIfNoRoom( 6 ); // siz more than tag

		m_buffer[m_putOffset++] = 0x05;
		m_buffer[m_putOffset++] = (BYTE)(value >> 32);
		m_buffer[m_putOffset++] = (BYTE)(value >> 24);
		m_buffer[m_putOffset++] = (BYTE)(value >> 16);
		m_buffer[m_putOffset++] = (BYTE)(value >> 8);
		m_buffer[m_putOffset++] = (BYTE)value;
	}
	else if ( value >= -0x8000000000i64 && value < 0x8000000000i64 )
	{
		throwIfNoRoom( 7 ); // siz more than tag

		m_buffer[m_putOffset++] = 0x06;
		m_buffer[m_putOffset++] = (BYTE)(value >> 40);
		m_buffer[m_putOffset++] = (BYTE)(value >> 32);
		m_buffer[m_putOffset++] = (BYTE)(value >> 24);
		m_buffer[m_putOffset++] = (BYTE)(value >> 16);
		m_buffer[m_putOffset++] = (BYTE)(value >> 8);
		m_buffer[m_putOffset++] = (BYTE)value;
	}
	else if ( value >= -0x80000000000i64 && value < 0x80000000000i64 )
	{
		throwIfNoRoom( 8 ); // siz more than tag

		m_buffer[m_putOffset++] = 0x07;
		m_buffer[m_putOffset++] = (BYTE)(value >> 48);
		m_buffer[m_putOffset++] = (BYTE)(value >> 40);
		m_buffer[m_putOffset++] = (BYTE)(value >> 32);
		m_buffer[m_putOffset++] = (BYTE)(value >> 24);
		m_buffer[m_putOffset++] = (BYTE)(value >> 16);
		m_buffer[m_putOffset++] = (BYTE)(value >> 8);
		m_buffer[m_putOffset++] = (BYTE)value;
	}
	else 
	{
		throwIfNoRoom( 9 ); // siz more than tag

		m_buffer[m_putOffset++] = 0x08;
		m_buffer[m_putOffset++] = (BYTE)(value >> 56);
		m_buffer[m_putOffset++] = (BYTE)(value >> 48);
		m_buffer[m_putOffset++] = (BYTE)(value >> 40);
		m_buffer[m_putOffset++] = (BYTE)(value >> 32);
		m_buffer[m_putOffset++] = (BYTE)(value >> 24);
		m_buffer[m_putOffset++] = (BYTE)(value >> 16);
		m_buffer[m_putOffset++] = (BYTE)(value >> 8);
		m_buffer[m_putOffset++] = (BYTE)value;
	}
		
	return m_putOffset - startOffset;
}

/**********************************************************************
 Description:  enocde an integer value

  throws expections if errors

 Returns: number of bytes used to encode this item

 *********************************************************************/
ULONG jsBEREncoder::encodeReal(	DOUBLE value, jsBERTag tag /*= REAL_TAG */)
{
	jsAssert(sizeof (LONGLONG) == sizeof (double) && sizeof (double) == 8);  // should be...

	ULONG startOffset = m_putOffset;

	encodeTag( tag );

	throwIfNoRoom( 1 ); // at least a length byte
	ULONG lenOffset = m_putOffset++;

	/*
	 * The real value is encoded with a real tag and a length byte 
	 * followed by the necessary number of data bytes.
	 */

    /* no contents for 0.0 reals */
    if (value == 0.0) 
	{
		m_buffer[lenOffset] = 0;
		return m_putOffset - startOffset;
	}


	LONGLONG val, *p;
    p = (LONGLONG*) &value;
    val = *p;

    if ( isfinite(value) )
    {
		throwIfNoRoom(  sizeof (double) + 2 ); 

	    // encode the value 
		int	exponent = (int)((val >> 52) & 0x7ff);
		LONGLONG mantissa = (val & _LL(0xfffffffffffff)) | _LL(0x10000000000000);

		
        // write format byte, we're doing base 2 and no other shifting

		if ((val >> 63) & 1 ) // negative ?
			m_buffer[m_putOffset++] = (REAL_BINARY | REAL_EXPLEN_2 | REAL_SIGN);
		else
			m_buffer[m_putOffset++] = (REAL_BINARY | REAL_EXPLEN_2);


		exponent -= (1023 + 52);

		//  write the exponent 
		m_buffer[m_putOffset++] = (exponent >> 8);
		m_buffer[m_putOffset++] = (exponent & 0xff);

		// write the mantissa
		for ( int i = 6; i >= 0; i--)
		{
			m_buffer[m_putOffset++] = (int)((mantissa >> i*8) & 0xff);
		}


		m_buffer[lenOffset] = sizeof(double) + 2;
    }
	else
    {
	    // special real values for +/- infinity
		throwIfNoRoom( 1 ); 

        if ((val >> 63) & 1 ) // negative ?
            m_buffer[m_putOffset++] = REAL_MINUS_INFINITY;
        else
            m_buffer[m_putOffset++] = REAL_PLUS_INFINITY;

		m_buffer[lenOffset] = 1; // just one byte
    }

	return m_putOffset - startOffset;
}

/**********************************************************************
 Description:  enocde an octet string value

  throws expections if errors

 Returns: number of bytes used to encode this item

 *********************************************************************/
ULONG jsBEREncoder::encodeOctetString (	PCBYTE		input_buffer,
										ULONG		length,
										jsBERTag	tag)
{
	jsAssert( input_buffer != 0 || length == 0 );

	ULONG startOffset = m_putOffset;

	/*
	 * An octet string is encoded with an octet string tag and a length 
	 * identifier followed by the bytes of the octet string.
	 */
	encodeTag( tag );

	encodeLength( length );

	throwIfNoRoom( length );  

	jsVerifyThrow( length == 0 || input_buffer != 0, new jsBEREncodeException( "encodeOctetString", jsBEREncodeException::nullPointer ) );	

	if ( length && input_buffer )
		::memcpy (m_buffer + m_putOffset, input_buffer, length);

	m_putOffset += length;

	return m_putOffset - startOffset;
}

void jsBEREncoder::copyBuffer(PCBYTE input_buffer, ULONG length)
{
	jsAssert( input_buffer != 0 || length == 0 );

	jsVerifyThrow( length == 0 || input_buffer != 0, new jsBEREncodeException( "encodeOctetString", jsBEREncodeException::nullPointer ) );	

	if ( length && input_buffer )
		::memcpy (m_buffer, input_buffer, length);

	m_putOffset = length;
}

jsBEREncoder& jsBEREncoder::operator<<( int i)
{
	switch ( m_nextCode ) 
	{
		case jsBERDecoder::nextBOOL:
			encodeBoolean( i != 0 );
			break;

		case jsBERDecoder::nextEnum:
			encodeEnumerated( i );
			break;

		default:
			throw new jsBEREncodeException( "", jsBEREncodeException::incorrectOperation );
			break;
	}
	m_nextCode = jsBERDecoder::nextAny;

	return *this;
}

///---------------------------------------------------
// Set the next Encode type.  Pass in nextBOOL, or nextEnum
// to be able to make the next >>(int) unambiguous
//
// [in] nd the value of the next Integer to encode
//
// [Returns]
//		reference to this for chaining operators
jsBEREncoder& jsBEREncoder::operator<<( jsBERDecoder::nextCode nd )
{
	m_nextCode = nd;
	return *this;
}

///---------------------------------------------------
// Copy operator.
//
// [in] src jsBEREncoder to copy.  Copies the data from the other
//	coder
// 
// [Returns] 
//		reference to this
jsBEREncoder &jsBEREncoder::operator=( const jsBEREncoder &src )
{
	if ( &src != this )
		assureLengthRewind( src.curSize(), src.buffer(), src.curSize() );

	return *this;
}

///---------------------------------------------------
// Call this after you write to the buffer returned by 
// assureLengthRewind().  This will move the internal pointer
// up this many bytes so it know more data was encoded into it.
// This is used when getting data from another source such as
// the network.
//
// [in] len length of data written
//
// [throws] jsBEREncodeException
//
void jsBEREncoder::wroteToAssureBuffer( ULONG len )
{
    throwIfNoRoom( len );

    m_putOffset += len;
}


void jsBEREncoder::rewind( ULONG putOffset /*= 0 */)
{ 
	if ( putOffset > m_putOffset )
		throw new jsBEREncodeException( "jsBEREncoder::rewind", jsBEREncodeException::badParam );

	m_putOffset = putOffset;  
	m_nextCode = jsBERDecoder::nextAny; 

	// check patch array!
	if ( m_putOffset == 0 )
		m_patchArray.clear();
	else
	{
		std::vector<PatchLength>::iterator i;
		for ( i = m_patchArray.begin(); i < m_patchArray.end(); )
		{
			if ( i->offset() > m_putOffset )
				i = m_patchArray.erase(i);
			else
				i++;
		}
	}

}


#ifdef _MSC_VER
// for template sign/unsign mismatch for some reason this removes them
#pragma warning (disable : 4018 ) 
#endif
 
