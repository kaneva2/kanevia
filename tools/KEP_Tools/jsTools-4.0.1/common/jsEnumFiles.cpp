/******************************************************************************
 Name: jsEnumFiles.cpp

 Description: enumerate files in a directory

 Copyright (C) 1996, 2004 Jim Wallace

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 History:
 1/28/98		jmw created

 Notes: 

******************************************************************************/
#include "js.h"

#include "jsEnumFiles.h"

#include <jsEnumFilesImpl.h>

const ULONG jsEnumFiles::FA_readOnly = 0x01;		

const ULONG jsEnumFiles::FA_directory = 0x02;

const ULONG jsEnumFiles::FA_hidden = 0x04;
	
const ULONG jsEnumFiles::FA_system = 0x08;	

const ULONG jsEnumFiles::FA_normal = 0x00;

///---------------------------------------------------------
// constructor
// 
// [in] fileMask the mask of files to look for
//
jsEnumFiles::jsEnumFiles( const wchar_t* fileMask, bool includeDirs )
{
	m_impl = new jsEnumFilesImpl( fileMask, includeDirs );
	setOk( m_impl != 0 );
}

///---------------------------------------------------------
// get the next file.  Only if this returns true will
// the current file name, etc. be valid
//
// [Returns]
//    true if got another file.  
bool jsEnumFiles::next()
{
	jsVerifyReturn( m_impl && m_impl->ok(), false );

	return m_impl->next();
}

///---------------------------------------------------------
// [Returns]
//    the current file name without its path
const wchar_t* jsEnumFiles::currentFileName() const
{
	jsVerifyReturn( m_impl && m_impl->ok(), false );

	return m_impl->currentFileName();
}

///---------------------------------------------------------
// [Returns]
//    the current directory name without the file name, will
// not have trailing slash
const wchar_t* jsEnumFiles::currentDirName() const
{
	jsVerifyReturn( m_impl && m_impl->ok(), false );

	return m_impl->currentDirName();
}

jsTime jsEnumFiles::currentLastWriteTime() const
{
	jsVerifyReturn( m_impl && m_impl->ok(), false );

	return m_impl->currentLastWriteTime();
}

///---------------------------------------------------------
// [Returns]
//    the current path name
const wchar_t* jsEnumFiles::currentPath() const
{
	jsVerifyReturn( m_impl && m_impl->ok(), false );

	return m_impl->currentPath();
}

ULONG jsEnumFiles::currentAttrs() const
{
	jsVerifyReturn( m_impl && m_impl->ok(), false );

	return m_impl->currentAttrs();
}

jsEnumFiles::~jsEnumFiles() { delete m_impl; }
