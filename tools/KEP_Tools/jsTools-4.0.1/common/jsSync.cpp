/******************************************************************************

 Name: jsSync.cpp
 Abbreviation: 

 Copyright (C) 1996, 2004 Jim Wallace

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 History:
 11/26/96		jmw created

 Notes: 

******************************************************************************/
#include "js.h"

#include "jsThread.h"
#include "jsSync.h"
#include "jsLockManager.h"
#include <jsSyncImpl.h>

// the global sync used in SyncExchange, etc
jsFastSync jsFastSync::m_global;
jsFastSync &jsFastSync::Global() { return m_global; }

/****************************************************************************
	jsFastSync passthrus
****************************************************************************/
jsSync::jsSync( 
	BYTE level,
	const char* name,
	bool lockNow,
	bool useLockMgr
	) : 
	jsSyncBase( level, name, useLockMgr ),
	m_impl( new jsSyncImpl )
{
	setOk( m_impl && m_impl->ok() );
	if ( ok() && lockNow )
		setOk( lock() == WR_OK );
}

jsSync::jsSync( 
	BYTE level,
	const char* name,
	bool lockNow	// = false 
	) : 
	jsSyncBase( level, name, true ),
	m_impl( new jsSyncImpl )
{
	setOk( m_impl && m_impl->ok() );
	if ( ok() && lockNow )
		setOk( lock() == WR_OK );
}

jsWaitRet jsSync::lock()
{
	jsWaitRet wr = WR_ERROR;
	if ( ok () )
	{
		if ( jsLM )
			jsLM->notifyLock( level() );

		#ifdef _JSDBG
			// this doesn't work for Unixware since it 
			// doesn't support a lock with timeout
			ULONG timeout = 2000, tot = 0;
			do 
			{
				wr = m_impl->lock( timeout );  
				tot += timeout/1000;
				if ( wr != WR_OK )
				{
					jsTrace( "Thread %lx waiting %ld sec for lock %ld held by %lx",
						jsThread::currentThreadID(),
						tot,
						level(),
						threadID() );
				}
			}
			while ( wr != WR_OK );
		#else
			wr = m_impl->lock( INFINITE_WAIT );
		#endif
	}
	if ( wr == WR_OK )
		m_TID = jsThread::currentThreadID();

	return wr;
}


jsWaitRet jsSync::tryLock()
{
	if ( ok () )
	{
		if ( jsLM )
			jsLM->notifyLock( level() );

		jsWaitRet wr = m_impl->tryLock();

		if ( wr != WR_OK && jsLM )
			jsLM->notifyUnlock( level() );
		return wr;
	}
	else
		return WR_ERROR;
}

jsWaitRet jsSync::peekLock()
{
	if ( ok () )
	{
		// no lock mgr since just peeking
		jsWaitRet wr = m_impl->tryLock();

		if ( wr == WR_OK )
			m_impl->unlock();

		return wr;
	}
	else
		return WR_ERROR;
}

void jsSync::unlock()
{
	if ( ok () )
	{
		if ( jsLM )
			jsLM->notifyUnlock( level() );

		m_impl->unlock();
	}
}

jsSync::~jsSync()
{
	if ( ok () )
		delete m_impl;
}

/****************************************************************************
	jsFastSync passthrus
****************************************************************************/
jsFastSync::jsFastSync( 
	bool lockNow //= false 
	) : 
	jsSyncBase( FASTSYNC_LEVEL, "jsFastSync" ), 
	m_impl( new jsFastSyncImpl( lockNow ) )
{
	setOk( m_impl && m_impl->ok() );
}

void jsFastSync::lock()
{
	jsAssert( ok() );
	m_impl->lock();
}

void jsFastSync::unlock()
{
	jsAssert( ok() );
	m_impl->unlock();
}

jsFastSync::~jsFastSync()
{
	delete m_impl;
}

/******************************************************************************

 Description: constructor for read/write sync class

******************************************************************************/
jsRWSync::jsRWSync( 
	BYTE level,
	const char* name,
	bool lockNow,	// = false
	jsLockReason lr	// = LR_READONLY 
	) :
	jsSyncBase( level, name ),
	m_readerCnt(0), 
	m_readDoneEvent(false,true), // initially halt, release all on set
	m_reader( level/*FastSYNC_LEVEL*/, name, false, false ), 
	m_exclusive( level/*FastSYNC_LEVEL*/, name, false, false )
{
	jsAssert( m_readDoneEvent.ok() &&
		   m_exclusive.ok() &&
		   m_reader.ok() );

	setOk( m_readDoneEvent.ok() &&
		   m_exclusive.ok() &&
		   m_reader.ok() );

	if ( ok() && lockNow )
		setOk( lock( lr ) == WR_OK );
}

/******************************************************************************

 Description: try to get the lock

 Returns: WR_OK if locked, WR_TIMEOUT if failed

******************************************************************************/
jsWaitRet jsRWSync::tryLock(jsLockReason lr)
{
	if ( jsLM )
		jsLM->notifyLock( level() );

	jsWaitRet wr = _lock( lr, true );

	if ( wr != WR_OK && jsLM )
		jsLM->notifyUnlock( level() );

	return wr;
}

/******************************************************************************

 Description: see if it is locked without getting it

 Returns: WR_OK if not locked, WR_TIMEOUT if locked

******************************************************************************/
jsWaitRet jsRWSync::peekLock(jsLockReason lr)
{
	jsWaitRet wr = _lock( lr, true );
	if ( wr == WR_OK )
		unlock( lr );
	return wr;
}

/******************************************************************************

 Description: lock it for the reason given

 Returns: WR_OK if not locked, WR_TIMEOUT if locked

******************************************************************************/
jsWaitRet jsRWSync::lock( 
	jsLockReason lr )
{
	if ( jsLM )
		jsLM->notifyLock( level() );

	return _lock( lr, false );
}

/******************************************************************************

 Description: private method to lock at given access level.  This fn
 supports the trying flag which the other fns use

 Returns: WR_OK if not locked, WR_TIMEOUT if locked, WR_ERROR if not ok

******************************************************************************/
jsWaitRet jsRWSync::_lock( 
	jsLockReason lr,
	bool	   bTrying 
	)
{
	jsAssert( ok() );
	if ( !ok() )
		return WR_ERROR;

	// Claim the read lock or write lock as specified
	if (lr == LR_READONLY)
	{
		// Claim the exclusive lock.  This call blocks if there's
		// an active writer or if there's a writer waiting for active readers to
		// cojslete.
		if ( bTrying )
		{
			jsWaitRet wr = m_exclusive.tryLock();
			if ( wr != WR_OK )
				return wr;
		}
		else
			m_exclusive.lock();

		// Claim access to the reader count.  If this blocks, it's only for the
		// briefest moment, while other threads go through to increment or
		// decrement the reader count.
		if ( bTrying )
		{
			jsWaitRet wr = m_reader.tryLock();
			if ( wr != WR_OK )
				return wr;
		}
		else
			m_reader.lock();

		// Increment the reader count.  If this is the first reader, we reset the
		// m_readDoneEvent event so that the next writer blocks.
		if (m_readerCnt++ == 0)
			m_readDoneEvent.haltWaiters();

		// Release access to the reader count
		m_reader.unlock();

		// Release access to the exclusive lock.  This enables
		// other readers to come through and the next writer to wait for active
		// readers to cojslete (which in turn prevents new readers from entering).
		m_exclusive.unlock();
	}
	else
	{
		// Verify that since this isn't the read lock, that it's the write lock
		jsAssert(lr == LR_READWRITE);

		// Claim the exclusive lock.  This not only prevents other
		// threads from claiming the write lock, but also prevents any new threads
		// from claiming the read lock.
		if ( bTrying )
		{
			jsWaitRet wr = m_exclusive.tryLock();
			if ( wr != WR_OK )
				return wr;
		}
		else
			m_exclusive.lock();

		// Wait for the active readers to release their read locks.
		return m_readDoneEvent.wait();
	}

	return WR_OK;
}

/******************************************************************************

 Description: unlocks the rw sync object

******************************************************************************/
void jsRWSync::unlock( jsLockReason lr )
{
	if ( jsLM )
		jsLM->notifyUnlock( level() );

	jsAssert( ok() );
	if ( !ok() || lr == LR_NOLOCK )
		return;

	// Release the read lock or write lock as specified
	if (lr == LR_READONLY)
	{
		// Claim access to the reader count.  If this blocks, it's only for the
		// briefest moment, while other threads go through to increment or
		// decrement the reader count.
		m_reader.lock();

		// Decrement the reader count.  If this is the last reader, set 
		// m_readDoneEvent, which allows the first waiting writer to proceed.
		if (--m_readerCnt == 0)
			m_readDoneEvent.releaseWaiters();

		// Release access to the reader count
		m_reader.unlock();
	}
	else 
	{
		// Verify that since this isn't the read lock, that it's the write lock
		jsAssert(lr == LR_READWRITE);

		// Make exclusive available to one other writer or to the first reader
		m_exclusive.unlock();
	}
}

jsRWSync::~jsRWSync()
{
	if (ok())
	{
		// all locks automatically clean up
	}
}

jsSyncBase::jsSyncBase( BYTE level, const char* name, bool useLockMgr ) : m_level(level), m_TID(0) 
{ 
	if ( useLockMgr && jsLM ) 
		jsLM->checkLevel( name, level ); 
}
#ifdef _MSC_VER
// for template sign/unsign mismatch for some reason this removes them
#pragma warning (disable : 4018 ) 
#endif
 
