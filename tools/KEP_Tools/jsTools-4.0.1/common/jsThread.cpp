/******************************************************************************
 Name: jsThread.cpp

 Description: implementation of the portable thread class

 this class is a thin wrapper around the OS-dependant version.  The reason
 for doing this indirection is to prevent the header included in your code
 being cluttered with os-specific headers and code

 Copyright (C) 1996, 2004 Jim Wallace

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 History:
 11/20/96		jmw created

 Notes: 

******************************************************************************/
#include "js.h"

#include "jsThread.h"
#include <jsThreadImpl.h>

jsThread::jsThread( 
	const char* name, 
	jsThdPriority prio, // = PR_NORMAL 
	jsThreadType  type	// = TT_NORMAL
	) : 
	m_name( name ), 
	m_priority(prio)
{
	m_thd = new jsThreadImpl( this, name, prio, type );
	setOk( m_thd != 0);
}

bool jsThread::start()
{
	jsAssert( ok() );
	return m_thd->start();
}

void jsThread::wakeUp()
{
	jsAssert( ok() );
	m_thd->wakeUp();
}

bool jsThread::reset()
{
	jsAssert( ok() );
	m_thd->reset();
	return true;
}

bool jsThread::isSleeping() const
{
	jsAssert( ok() );
	return m_thd->isSleeping();
}


bool jsThread::isRunning() const
{
	jsAssert( ok() );
	return m_thd->isRunning();
}

bool jsThread::isTerminated() const
{
	jsAssert( ok() );
	return m_thd->isTerminated();
}

jsThdPriority jsThread::priority() const
{
	jsAssert( ok() );
	return m_thd->priority();
}

void jsThread::priority( jsThdPriority mp )
{
	jsAssert( ok() );
	m_thd->priority( mp );
}

ULONG jsThread::exitCode()
{
	jsAssert( ok() );
	return m_thd->exitCode();
}

void jsThread::kill()
{
	jsAssert( ok() );
	m_thd->kill();
}

jsThread::StopRet jsThread::stop( 
	ULONG waitTimeMS,	// = 60000
	bool killIfTimeout	// = false 
	)
{
	jsAssert( ok() );
	return m_thd->stop( waitTimeMS, killIfTimeout );
}

bool jsThread::join( 
	ULONG WaitMS	//= INFINITE_WAIT 
	) const
{
	jsAssert( ok() );
	return m_thd->join( WaitMS );
}

jsThreadID jsThread::currentThreadID()
{
	return jsThreadImpl::currentThreadID();

}

jsThreadID jsThread::threadID() const
{
	jsAssert( ok() );
	return m_thd->threadID();
}

bool jsThread::operator==( 
	jsThreadID thdID 
	)
{
	jsAssert( ok() );
	return m_thd->operator==( thdID );
}

int	jsThread::createThreadData()
{
	return jsThreadImpl::createThreadData();
}

ULONG jsThread::getThreadData( 
	int id 
	)
{
	return jsThreadImpl::getThreadData( id );
}

bool jsThread::setThreadData( 
	int id, 
	ULONG value 
	)
{
	return jsThreadImpl::setThreadData( id, value );
}

void jsThread::freeThreadData( 
	int id 
	)
{
	jsThreadImpl::freeThreadData( id );
}

jsThread::~jsThread()
{
	delete m_thd;
}

bool jsThread::_sleep( 
	ULONG ms 
	)
{
	jsAssert( ok() );
	return m_thd->_sleep( ms );
}

void jsThread::_yield()
{
	jsAssert( ok() );
	m_thd->_yield();
}

void jsThread::_setTerminated()
{
	jsAssert( ok() );
	m_thd->_setTerminated();
}

bool jsThread::isCreated() const 
{
	jsAssert( ok() );
	return m_thd->isCreated();
}

#ifdef _MSC_VER
// for template sign/unsign mismatch for some reason this removes them
#pragma warning (disable : 4018 4786 ) 
#endif
 
