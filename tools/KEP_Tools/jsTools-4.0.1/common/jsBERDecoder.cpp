/******************************************************************************
 Name: jsBEREncoder.cpp

 Description: encoder class for 

 Copyright (C) 1996, 2004 Jim Wallace

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 History:
 2/23/99		jmw created

 Notes: 

******************************************************************************/
#include "js.h"
#ifdef JS_OCTETSTRING
#include "jsOctetString.h"
#endif

#include <math.h> // for pow

#include "jsBERDecoder.h"

const ULONG jsBERDecoder::WRITE_MODE = ULONG_MAX;

static const BYTE REAL_EXPLEN_MASK	= 0x03;
static const BYTE REAL_FACTOR_MASK	= 0x0c;
static const BYTE REAL_BASE_MASK = 0x30;
static const BYTE REAL_BASE_2 = 0x00;
static const BYTE REAL_BASE_8 = 0x10;
static const BYTE REAL_BASE_16 = 0x20;

///---------------------------------------------------
// Copy operator.
//
// [in] src jsBERDecoder to copy.  Sets this object's pointers to same values
// as the others, including the current pointer.
// 
// [throws] jsBERDecodeException
//
// [Returns] 
//		reference to this
jsBERDecoder &jsBERDecoder::operator=( const jsBERDecoder &f )
{
	if ( this != &f )
	{
		setPtr( f.size(), f.buffer() );

		if ( !f.canRead() )
			m_getOffset = WRITE_MODE; // both in WRITE_MODE
		else
			m_getOffset = f.curOffset();
	}
	return *this;
}


///---------------------------------------------------
// Set the internal pointer to a new buffer
//
// [in] size size if BYTEs of buffer
// [in] buffer the buffer where to start decoding
//
// [throws] jsBERDecodeException
//
void jsBERDecoder::setPtr( ULONG size, PCBYTE buffer )
{
	
/*	there were times I wanted to do this
	if fact I wanted to do both null
	if ( size == 0 )
	{
		throw new jsBERDecodeException( "size", jsBERDecodeException::badParam );
	}

	if ( buffer == 0 )
	{
		throw new jsBERDecodeException( "", jsBERDecodeException::nullPointer );
	}
*/

	m_lastTagOffset = ULONG_MAX;
	m_size = size;
	m_buffer = buffer;

	rewind();
}

//===================================================================
// GROUP: Pointer positioning functions
//===================================================================

///---------------------------------------------------
// move current pointer past this tagged item.
//
// [throws] jsBERDecodeException
//
// [Returns]
//		the number of bytes it moved
ULONG	jsBERDecoder::skipTaggedItem()
{
	ULONG	oldOffset = m_getOffset;

	throwIfCantRead("skipTaggedItem");

	ULONG		length;

	/*
	 * Increment the data pointer past the identifier.  Retrieve the length
	 * and then increment the pointer by that amount.  Return the number of
	 * bytes skipped.
	 */
	jsBERTag tag;
	decodeTag( tag ); // skip tag

	int lenLength = decodeLength( length );
	m_getOffset += length; // skip rest

	jsAssert( length + 1 + lenLength == m_getOffset - oldOffset ); // sanity
	return m_getOffset - oldOffset;
}

///---------------------------------------------------
// move current pointer past the tagged item we just got out.  Use 
// this function is you come to an item you don't recognize and 
// want to skip over it.
//
// [in] tag tag that just extracted which we want to skip.
//
// [throws] jsBERDecodeException
//
// [Returns]
//		the number of bytes it moved
ULONG	jsBERDecoder::skipLastTaggedItem( jsBERTag tag )
{
	throwIfCantRead("skipLastTaggedItem");

	// Save the bytes already decoded for this tagged item.
	ULONG bytesAlreadyDecoded = m_getOffset - m_lastTagOffset;

	if ( m_lastTagOffset >= m_size-1 ) // tag is at least one byte
		throw new jsBERDecodeException( "", jsBERDecodeException::incorrectOperation );

	m_getOffset = m_lastTagOffset;

	if ( peekTag() != tag )
	{
		throw new jsBERDecodeException( "skipLastTaggedItem", jsBERDecodeException::tagNotFound );
	}

	// Return the number of bytes skipped minus the number of bytes already decoded.
	return skipTaggedItem() - bytesAlreadyDecoded;
}

//===================================================================
// GROUP: Extraction functions
//===================================================================

///---------------------------------------------------
// Extract a two-byte short value
//
// [out] val destination for value from buffer
//
// [throws] jsBERDecodeException
//
ULONG jsBERDecoder::decodeShortInteger( SHORT &val, jsBERTag expectedTag /*=DONT_CARE_TAG*/ )
{
	ULONG	oldOffset = m_getOffset;
	BYTE	length;

	throwIfCantRead("decodeShortInteger");

	throwIfLessThan("decodeShortInteger", 3 ); // at least 3 bytes tag, len, data

	decodeAndCheckTag("decodeShortInteger", expectedTag );

	/*
	 * Increment the data pointer past the identifier and retrieve the length
	 * byte for the integer.  Retrieve the proper number of bytes necessary to
	 * reconstruct the integer.
	 */

	length = m_buffer[m_getOffset++];	// length of number

	switch (length)
	{
		case 1:
			val = (SHORT)m_buffer[m_getOffset++];
			if ( val & 0x80 ) // negative?
				val |= 0xff00; // extend sign
			break;

		case 2:
			throwIfLessThan("decodeShortInteger", 1 ); // 1 more than min

			val = ((SHORT) m_buffer[m_getOffset++] << 8);
			val |= ((SHORT) m_buffer[m_getOffset++] & 0xff);
			break;

		default:
			throw new jsBERDecodeException( "decodeShortInteger", jsBERDecodeException::badLength );
			break;
	}

	return m_getOffset - oldOffset;
}

///---------------------------------------------------
// Extract a four-byte long value
//
// [out] val destination for value from buffer
//
// [throws] jsBERDecodeException
//
ULONG jsBERDecoder::decodeInteger( LONG &val, jsBERTag expectedTag /*=DONT_CARE_TAG*/ )
{
	BYTE	length;
	ULONG	oldOffset = m_getOffset;

	throwIfCantRead("decodeInteger");

	throwIfLessThan("decodeInteger", 3 ); // at least 3 bytes tag, len, data

	decodeAndCheckTag("decodeInteger", expectedTag );

	/*
	 * Increment the data pointer past the identifier and retrieve the length
	 * byte for the integer.  Retrieve the proper number of bytes necessary to
	 * reconstruct the integer.
	 */
	

	length = m_buffer[m_getOffset++];	// length of number

	switch (length)
	{
		case 1:
			val = (LONG)m_buffer[m_getOffset++];
			if ( val & 0x80 ) // negative?
				val |= 0xffffff00; // extend sign
			break;

		case 2:
			throwIfLessThan("decodeInteger", 1 ); // 1 more than min

			val =  ((LONG) m_buffer[m_getOffset++] << 8);
			val |= ((LONG) m_buffer[m_getOffset++] & 0xff);
			if ( val & 0x8000 ) // negative?
				val |= 0xffff0000; // extend sign
			break;

		case 3:
			throwIfLessThan("decodeInteger", 2 ); // 2 more than min

			val =  ((LONG)  m_buffer[m_getOffset++] << 16);
			val |= (((LONG) m_buffer[m_getOffset++] & 0xff) << 8);
			val |= ((LONG)  m_buffer[m_getOffset++] & 0xff);
			if ( val & 0x800000 ) // negative?
				val |= 0xff000000; // extend sign
			break;

		case 4:
			throwIfLessThan("decodeInteger", 3 ); // 3 more than min

			val =  ((LONG)  m_buffer[m_getOffset++] << 24);
			val |= (((LONG) m_buffer[m_getOffset++] & 0xff) << 16);
			val |= (((LONG) m_buffer[m_getOffset++] & 0xff) << 8);
			val |= ((LONG)  m_buffer[m_getOffset++] & 0xff);
			break;
		
		default:
			throw new jsBERDecodeException( "decodeInteger", jsBERDecodeException::badLength );
			break;
	}

	return m_getOffset - oldOffset;
}

///---------------------------------------------------
// Extract an eight-byte long value
//
// [out] val destination for value from buffer
//
// [throws] jsBERDecodeException
//
ULONG jsBERDecoder::decodeLongInteger( LONGLONG &val, jsBERTag expectedTag /*=DONT_CARE_TAG*/ )
{
	BYTE	length;
	ULONG	oldOffset = m_getOffset;

	throwIfCantRead("decodeInteger");

	throwIfLessThan("decodeInteger", 3 ); // at least 3 bytes tag, len, data

	decodeAndCheckTag("decodeInteger", expectedTag );

	/*
	 * Increment the data pointer past the identifier and retrieve the length
	 * byte for the integer.  Retrieve the proper number of bytes necessary to
	 * reconstruct the integer.
	 */
	

	length = m_buffer[m_getOffset++];	// length of number

	if ( length <= 4 )
	{
		m_getOffset = oldOffset;
		LONG l = 0;
		ULONG ret = decodeInteger(l, expectedTag);
		val = l;
		return ret;
	}
	
	switch (length)
	{
		case 5:
			throwIfLessThan("decodeInteger", 4 ); 

			val =  ((LONGLONG)  m_buffer[m_getOffset++] << 32);
			val |= (((LONGLONG) m_buffer[m_getOffset++] & 0xff) << 24);
			val |= (((LONGLONG) m_buffer[m_getOffset++] & 0xff) << 16);
			val |= (((LONGLONG) m_buffer[m_getOffset++] & 0xff) << 8);
			val |= ((LONGLONG)  m_buffer[m_getOffset++] & 0xff);
			if ( val &       0x8000000000 ) // negative?
				val |= 0xffffff0000000000; // extend sign
			break;

		case 6:
			throwIfLessThan("decodeInteger", 5 ); 

			val =  ((LONGLONG)  m_buffer[m_getOffset++] << 40);
			val |= (((LONGLONG) m_buffer[m_getOffset++] & 0xff) << 32);
			val |= (((LONGLONG) m_buffer[m_getOffset++] & 0xff) << 24);
			val |= (((LONGLONG) m_buffer[m_getOffset++] & 0xff) << 16);
			val |= (((LONGLONG) m_buffer[m_getOffset++] & 0xff) << 8);
			val |= ((LONGLONG)  m_buffer[m_getOffset++] & 0xff);
			if ( val &     0x800000000000 ) // negative?
				val |= 0xffff000000000000; // extend sign
			break;

		case 7:
			throwIfLessThan("decodeInteger", 6 ); 

			val =  ((LONGLONG)  m_buffer[m_getOffset++] << 48);
			val |= (((LONGLONG) m_buffer[m_getOffset++] & 0xff) << 40);
			val |= (((LONGLONG) m_buffer[m_getOffset++] & 0xff) << 32);
			val |= (((LONGLONG) m_buffer[m_getOffset++] & 0xff) << 24);
			val |= (((LONGLONG) m_buffer[m_getOffset++] & 0xff) << 16);
			val |= (((LONGLONG) m_buffer[m_getOffset++] & 0xff) << 8);
			val |= ((LONGLONG)  m_buffer[m_getOffset++] & 0xff);
			if ( val &   0x80000000000000 ) // negative?
				val |= 0xff00000000000000; // extend sign
			break;

		case 8:
			throwIfLessThan("decodeInteger", 7 ); // 3 more than min

			val =  ((LONGLONG)  m_buffer[m_getOffset++] << 56);
			val |= (((LONGLONG) m_buffer[m_getOffset++] & 0xff) << 48);
			val |= (((LONGLONG) m_buffer[m_getOffset++] & 0xff) << 40);
			val |= (((LONGLONG) m_buffer[m_getOffset++] & 0xff) << 32);
			val |= (((LONGLONG) m_buffer[m_getOffset++] & 0xff) << 24);
			val |= (((LONGLONG) m_buffer[m_getOffset++] & 0xff) << 16);
			val |= (((LONGLONG) m_buffer[m_getOffset++] & 0xff) << 8);
			val |= ((LONGLONG)  m_buffer[m_getOffset++] & 0xff);
			break;
		
		default:
			throw new jsBERDecodeException( "decodeLongInteger", jsBERDecodeException::badLength );
			break;
	}

	return m_getOffset - oldOffset;
}


///---------------------------------------------------
// Extract a double real value
//
// [out] val destination for value from buffer
//
// [throws] jsBERDecodeException
//
// [Returns] 
//		bytes read
ULONG jsBERDecoder::decodeReal( double &val, jsBERTag expectedTag /*=DONT_CARE_TAG*/ )
{
	BYTE	length;
	ULONG	startOffset = m_getOffset;

	throwIfCantRead("decodeReal");

	throwIfLessThan("decodeReal", 2 ); // at least 2 bytes tag, len. if len == 0, then 0.0

	decodeAndCheckTag("decodeReal", expectedTag );

	/*
	 * Increment the data pointer past the identifier and retrieve the length
	 * byte for the integer.  Retrieve the proper number of bytes necessary to
	 * reconstruct the integer.
	 */

	length = m_buffer[m_getOffset++];	// length of number

    if (length == 0)
    {
        val = 0.0;
    }
	else
	{
		throwIfLessThan("decodeReal", length ); 

		BYTE firstOctet = m_buffer[m_getOffset++];
		if ( 1 == length )
		{
			if ( REAL_PLUS_INFINITY == firstOctet )
				val = DOUBLE_MAX;
			else if ( REAL_MINUS_INFINITY == firstOctet )
				val = -DOUBLE_MAX;
			else
				throw new jsBERDecodeException( "decodeReal", jsBERDecodeException::badLength );
		}
		else
		{
			UINT  expLen = 0;
			USHORT base;
			LONG exponent = 0;

			if ( (firstOctet & REAL_BINARY) != 0 )
			{
				BYTE firstExpOctet = m_buffer[m_getOffset++];

				if ( firstExpOctet & 0x80 )
					exponent = -1;

				switch (firstOctet & REAL_EXPLEN_MASK)
				{
					case REAL_EXPLEN_1:
						expLen = 1;
						exponent =  (exponent << 8) | firstExpOctet;
						break;

					case REAL_EXPLEN_2:
						expLen = 2;
						exponent =  (exponent << 16) | (((ULONG) firstExpOctet) << 8) | m_buffer[m_getOffset++];
						break;

					case REAL_EXPLEN_3:
						expLen = 3;
						exponent =  (exponent << 16) | (((ULONG) firstExpOctet) << 8) | m_buffer[m_getOffset++];
						exponent =  (exponent << 8) | m_buffer[m_getOffset++];
						break;

					default:  // long form
						{
						expLen = firstExpOctet + 1;

						int i = firstExpOctet-1;

						firstExpOctet =  m_buffer[m_getOffset++];

						if (firstExpOctet & 0x80)
							exponent = (-1 <<8) | firstExpOctet;
						else
							exponent = firstExpOctet;

						for ( ; i > 0 ; i-- )
							exponent = (exponent << 8) | m_buffer[m_getOffset++];
						}
						break;
				}

				DOUBLE  mantissa = 0.0;

				for ( int i = 1 + expLen; i < length; i++)
				{
					mantissa *= (1<<8);
					mantissa +=  m_buffer[m_getOffset++];
				}

				//  adjust N by scaling factor 
				mantissa *= (1<<((firstOctet & REAL_FACTOR_MASK) >> 2));

				switch (firstOctet & REAL_BASE_MASK)
				{
					case REAL_BASE_2:
						base = 2;
						break;

					case REAL_BASE_8:
						base = 8;
						break;

					case REAL_BASE_16:
						base = 16;
						break;

					default:
						throw new jsBERDecodeException( "decodeReal", jsBERDecodeException::unexpectedData );
						break;

				}

				DOUBLE tmpBase = base;
				DOUBLE tmpExp = exponent;

				val =  mantissa * ::pow((double)base, (double)exponent);

				if (firstOctet & REAL_SIGN)
					val = -val;

			}
			else // not the binary 
			{
				throw new jsBERDecodeException( "decodeReal", jsBERDecodeException::incorrectOperation );
			}
		}
	}

	return m_getOffset - startOffset;
}

///---------------------------------------------------
// Extract a length only.  This moves the pointer!
//
// [out] length referenc to ULONG for length
//
// [throws] jsBERDecodeException
//
// [Returns]
//		number of bytes taken up by the length
ULONG  jsBERDecoder::decodeLength( ULONG &length )
{
	ULONG	oldOffset = m_getOffset;

	throwIfLessThan("decodeLength", 1 ); 

	BYTE b = m_buffer[m_getOffset++];;

	if ( b & LONG_FORM_MASK )
	{
		if ( b == ONE_BYTE_LENGTH )
		{
			throwIfLessThan("decodeLength", 1 ); 

			length = m_buffer[m_getOffset++];
		}
		else if ( b == TWO_BYTE_LENGTH )
		{
			throwIfLessThan("decodeLength", 2 ); 

			length = (m_buffer[m_getOffset] << 8) + m_buffer[m_getOffset + 1];
			m_getOffset += 2;
		}
		else if ( b == THREE_BYTE_LENGTH )
		{
			throwIfLessThan("decodeLength", 3 ); 

			length = (m_buffer[m_getOffset] << 16) + (m_buffer[m_getOffset + 1] << 8) + 
				m_buffer[m_getOffset + 2];
			m_getOffset += 3;
		}
		else if ( b == FOUR_BYTE_LENGTH )
		{
			throwIfLessThan("decodeLength", 3 ); 

			length = (m_buffer[m_getOffset] << 24) + (m_buffer[m_getOffset + 1] << 16) + 
				(m_buffer[m_getOffset + 2] << 8) + m_buffer[m_getOffset + 3];
			m_getOffset += 4;
		}
		else
		{
			throw new jsBERDecodeException( "", jsBERDecodeException::unexpectedData );
		}
	}
	else // short form
	{
		length = b;
	}

	return 	m_getOffset - oldOffset;
}

///---------------------------------------------------
// Extract a character string
//
// [out] val destination for value from buffer
// [in] maxLen max length for this string
// [out] len length of this extracted string
//
// [throws] jsBERDecodeException
//
// [Returns]
//		len of string + length of length + length of tag
ULONG jsBERDecoder::decodeOctetString( PBYTE s, ULONG maxLen, ULONG &length, jsBERTag expectedTag /*=DONT_CARE_TAG*/ )
{
	ULONG	oldOffset = m_getOffset;

	throwIfLessThan("decodeOctetString", 1 ); // tag

	decodeAndCheckTag("decodeOctetString", expectedTag );

	/*
	 * Check to see which variant of the length is being used and then
	 * retrieve the length.
	 */
	decodeLength( length ); // will throw can't read, or past end

	throwIfLessThan("decodeOctetString", length ); 

	/*?? is this in a spec? (jw)
			length <= MAXIMUM_OCTET_STRING (32K)&&*/
	if ( length <= maxLen ) 
	{
		::memcpy (s, curPtr(), length);
		m_getOffset += length;
	}
	else
	{
		char s[33];
		_ultoa( length, s, 10 );
		m_getOffset = oldOffset; // let 'em try again, after throwing

		throw new jsBERDecodeException( s, jsBERDecodeException::bufferTooSmall );
	}

	if ( length < maxLen ) 
		s[length] = 0;	// be nice and null terminate, if we can

	return m_getOffset-oldOffset;
}

///---------------------------------------------------
// Extract a string
//
// [out] val destination for value from buffer
//
// [throws] jsBERDecodeException
//
// [Returns]
//		len of string + length of length + length of tag
ULONG jsBERDecoder::decodeOctetString( std::string &s, jsBERTag expectedTag /*=DONT_CARE_TAG*/)
{
	ULONG	oldOffset = m_getOffset;

	throwIfLessThan("decodeOctetString", 1 ); // tag

	decodeAndCheckTag("decodeOctetString", expectedTag );

	ULONG stringLen;

	decodeLength( stringLen );

	throwIfLessThan("decodeOctetString", stringLen ); 

	s.assign( (const char*)curPtr(), stringLen );

	m_getOffset += stringLen;

	return m_getOffset - oldOffset;
}


#ifdef _jsOctetString_h_
///---------------------------------------------------
// Extract a string. This moves the pointer!
//
// [out] s destination string 
//
// [throws] jsBERDecodeException
//
// [Returns]
//		len of string + length of length + length of tag
ULONG jsBERDecoder::decodeOctetString( jsOctetString &s, jsBERTag expectedTag /*=DONT_CARE_TAG*/ )
{
	ULONG	oldOffset = m_getOffset;

	throwIfLessThan("decodeOctetString", 1 ); // tag

	decodeAndCheckTag("decodeOctetString", expectedTag );

	ULONG stringLen;

	decodeLength( stringLen );

	throwIfLessThan("decodeOctetString", stringLen ); 

	s.set( (PCBYTE)curPtr(), stringLen );

	m_getOffset += stringLen;

	return m_getOffset - oldOffset;
}
#	endif

///---------------------------------------------------
// Extract a boolean value
//
// [out] val destination for value from buffer
//
// [throws] jsBERDecodeException
//
// [Returns]
//		number of bytes the pointer moved always 3
ULONG jsBERDecoder::decodeBoolean ( bool &val, jsBERTag expectedTag /*=DONT_CARE_TAG*/ )
{
	ULONG	oldOffset = m_getOffset;

	BYTE	boolean_byte;

	throwIfCantRead("decodeBoolean");

	throwIfLessThan("decodeBoolean", 3 ); // always 3 bytes long (tag, id, value )

	decodeAndCheckTag("decodeBoolean", expectedTag );

	/*
	 * Increment the data pointer past the identifier and length.  Retrieve the
	 * boolean byte and set the output to one for all non-zero boolean values.
	 */
	m_getOffset += 1; // skip len

	boolean_byte = m_buffer[m_getOffset++];

	if (boolean_byte != 0)
		val = true;
	else
		val = false;

	return m_getOffset - oldOffset;
}

///---------------------------------------------------
// Extract an enumerated value
//
// [out] val destination for value from buffer
//
// [throws] jsBERDecodeException
//
// [Returns]
//		number of bytes the pointer moved
ULONG jsBERDecoder::decodeEnumerated ( int &val, jsBERTag expectedTag /*=DONT_CARE_TAG*/ )
{
	ULONG	oldOffset = m_getOffset;

	BYTE	length;

	throwIfCantRead("decodeEnumerated");

	throwIfLessThan("decodeEnumerated", 3 ); // min of 3

	decodeAndCheckTag("decodeEnumerated", expectedTag );

	/*
	 * Encodings for enumerated types are the same as the integer value
	 * which represents it, except that the tag is different.  Increment the
	 * data pointer past the identifier, retrieve the length byte for the
	 * enumeration, then retrieve the proper number of bytes necessary to 
	 * reconstruct the enumerated value.
	 */
	length = m_buffer[m_getOffset++];

	switch (length)
	{
		case 1:
			val = (int)m_buffer[m_getOffset++];
			break;

		case 2:
			throwIfLessThan("decodeEnumerated ", 1 ); // need one more

			val = ((int) m_buffer[m_getOffset++] << 8);
			val |= ((int) m_buffer[m_getOffset++] & 0xff);
			break;
		
		default:
			throw new jsBERDecodeException( "decodeEnumerated", jsBERDecodeException::badLength );
			break;
	}

	return m_getOffset - oldOffset;
}

//===================================================================
// GROUP: Extraction operators
//===================================================================

///---------------------------------------------------
// Extract an int type.  This <i>must</i> abe preceded with the 
// nextBOOL or nextEnum Action enum or it will throw an exception
//
// [out] val an enum or bool destination for value from buffer
//
// [throws] jsBERDecodeException
//
// [Returns]
//		reference to this for chaining operators
jsBERDecoder& jsBERDecoder::operator>>( int &i)
{
	switch ( m_nextCode ) 
	{
		case nextBOOL:
			{
			bool b;
			decodeBoolean( b );
			i = b ? 1 : 0;
			}
			break;

		case nextEnum:
			decodeEnumerated( i );
			break;

		default:
			throw new jsBERDecodeException( "", jsBERDecodeException::incorrectOperation );
			break;
	}
	m_nextCode = nextAny;

	return *this;
}

///---------------------------------------------------------
// decode a constructed tag and the following length.
// 
// [out] tag the tag found.  Will be expectedTag, unless DONT_CARE_TAG
// passed in (the default)
// [out] len length of the consturcted item
// [in] expectedTag optional expected tag 
//
// [throws] jsBERDecodeException if wrong tag found, or other error
//
// [Returns]
//    number of bytes decoded to get tag and length
ULONG jsBERDecoder::decodeConstructedTagAndLength( OUT jsBERTag &tag, OUT ULONG &len, IN jsBERTag expectedTag /*= DONT_CARE_TAG*/, bool movePointer /*= true*/ )
{
	jsAssert( tagIsConstructed( expectedTag ) || expectedTag == DONT_CARE_TAG );

	ULONG bytesDecoded = decodeTag( tag );

	if ( !tagIsConstructed( tag ) || (expectedTag != DONT_CARE_TAG && tag != expectedTag ) )
		throw new jsBERDecodeException( "decodeConstructedTagAndLength", jsBERDecodeException::unexpectedData );

	bytesDecoded += decodeLength( len );

	if ( !movePointer )
	{
		m_getOffset -= bytesDecoded;
	}

	return bytesDecoded;
}

///---------------------------------------------------
// Set the next extraction type.  Pass in nextBOOL, or nextEnum
// to be able to make the next >>(int) unambiguous
//
// [in] nd the value of the next Integer to decode
//
// [Returns]
//		reference to this for chaining operators
jsBERDecoder& jsBERDecoder::operator>>( const nextCode &nd )
{
	m_nextCode = nd;
	return *this;
}

///---------------------------------------------------
// Decoder a tag
//
// [out] val destination for tag from buffer
//
// [throws] jsBERDecodeException
//
// [Returns]
//		Number of it takes
ULONG jsBERDecoder::decodeTagNoMove( jsBERTag &val ) const
{
	const BYTE *p = curPtr();
	ULONG bytesDecoded = 1;

	val = (*p << 24 );

	if ( (*p & HIGH_TAG_TYPE) == HIGH_TAG_TYPE )
	{
		ULONG temp = 0;
		int shiftAmt = 16;

		// get out the extra bytes
        do
        {
			p++;
            temp = (*p);
            val |= (temp << shiftAmt);
            bytesDecoded++;
            shiftAmt -= 8;
        }
        while ( (temp & 0x80) != 0 && bytesDecoded <= sizeof( jsBERTag ) );

        if ( (temp & 0x80) != 0 ) // had more
        {
            throw new jsBERDecodeException( "", jsBERDecodeException::tagTooLong );
        }
	}
	// else we already got it

	return bytesDecoded;
}


#ifdef _MSC_VER
// for template sign/unsign mismatch for some reason this removes them
#pragma warning (disable : 4018 ) 
#endif
 
