/******************************************************************************
 Name: jsconnect.cpp

 Description: socket implementation class for TCP/IP implementation

 Copyright (C) 1996, 2004 Jim Wallace

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 History:
 1/26/97	jmw created via AS


 Notes:  Socket CONNECTION object used for keeping information on the connection.
 		 The jsConnection object is basically a encapsulation of the underlying TCP
		 Object which may be implementation dependant.All interations with the tcp sub-system
		 must be done via the jsConnection object.

	**CAUTION** We have chosen, not to make this object multi-threaded. Rather, instantiate
	a copy of the object on each thread. That way, we can be guaranteed, of
	parallel execution on multiple platforms.


******************************************************************************/
#include "js.h"
#include "jsSync.h"
#include "jsConnection.h"
#include "jsTcpImpl.h"


jsConnection::jsConnection() :
	m_sock( new jsTCPImpl() ),
	m_listener( false ),
	m_state( jsConnection::NEW ),
	m_checkCon ( false ),
	m_status ( jsConnection::Ok )
{ setOk( true ); } 
	
jsConnection::jsConnection ( SOCKFD s ) : 
	m_sock( new jsTCPImpl( s ) ),
	m_listener( false ),
	m_state( jsConnection::NEW ),
	m_checkCon ( false ),
	m_status ( jsConnection::Ok )
{ setOk( true ); }


jsConnection::jsConnection( const jsConnection & from ) :
	m_sock( new jsTCPImpl() ),
	m_lastAccess( from.m_lastAccess ),
	m_listener( from.m_listener ),
	m_checkCon( from.m_checkCon ),
	m_state( from.m_state ),
	m_status( from.m_status ),
	m_conttl( from.m_conttl )
{
	m_sock->duplicate( *(from.m_sock) );
	setOk( from.ok() );
} 

jsConnection & jsConnection::operator =( const jsConnection & from )
{
	if ( this != &from )
	{
		m_sock = new jsTCPImpl();
		m_sock->duplicate( *(from.m_sock) );
		m_lastAccess = from.m_lastAccess;
		m_listener = from.m_listener;
		m_checkCon = from.m_checkCon;
		m_state = from.m_state;
		m_status = from.m_status;
		m_conttl = from.m_conttl;
		setOk( from.ok() );
	}
		
	return *this;
}


//when we kill a connection object, we close it's connection
jsConnection::~jsConnection()
{
	delete m_sock;
}


jsConnection::Status jsConnection::listen ( USHORT backlog )
{  
	m_state = jsConnection::LISTEN;
	bool st = m_sock->listen( backlog ) ;
	m_lastAccess = jsTime::now();
	return _status( st );
}


jsConnection::Status jsConnection::bindUntilSuccess( UINT startPortNo, UINT endPortNo )
{
	bool st =  m_sock->bindUntilSuccess( startPortNo, endPortNo );
	m_state = jsConnection::BIND;
	m_lastAccess = jsTime::now();
	return jsConnection::Ok;
}


//don't really care about status	 
jsConnection::Status jsConnection::close() 
{
   	m_state = jsConnection::CLOSE;
	m_sock->close(); 
	m_lastAccess = jsTime::now();
	setOk( false );
	return jsConnection::Ok; 
}


jsConnection::Status jsConnection::abort()  
{  
   	m_state = jsConnection::ABORT;
	m_sock->close();
	m_lastAccess = jsTime::now();
 	setOk ( false );
	return jsConnection::Ok; 
}

bool jsConnection::setNonBlocking( bool set ) 
{ 
	bool st = m_sock->setNonBlocking( set );
 	m_lastAccess = jsTime::now();
	return st;
}


//given a Url, connect to it
jsConnection::Status jsConnection::connect( 
		const char* host, 
		UINT port, 
		ULONG timeout,		// = 0, 
		bool block			// = true 
		)
{
	#ifdef _SOCKET_DBG
	jsTrace("Entering jsconnection::connect");
	#endif

   	m_state = jsConnection::CONNECT;

	//get the internet info on the host
	jsInetInfo ii( host );
	if ( !ii.ok() )
		return jsConnection::Error;
	ULONG addr = ii.numAddress();

	m_state = jsConnection::CONNECT;
	bool st  = m_sock->connect( addr, port, block, timeout ) ;

	std::string ermsg;

	m_lastAccess = jsTime::now();
	if ( !st )
	{
		if ( m_sock->eof() )
		{
			m_status = jsConnection::Eof;
			ermsg = "eof";
		}
		else if ( m_sock->nbcode() )
		{
			m_status = jsConnection::ConnectWouldBlock;
			ermsg = "connectwouldblock";
		}
		else if ( m_sock->timeout() )
		{
			m_status = jsConnection::Timeout;
			ermsg = "timeout";
		}
		else
		{
			m_status = jsConnection::Error;
			ermsg = "generic error";
		}

		#ifdef _SOCKET_DBG
		jsTrace( "jsConnection::connect error msg = %s\n", (const char*)ermsg );
		#endif
		
	}
	else
		m_status = jsConnection::Ok;

	#ifdef _SOCKET_DBG
	jsTrace("jsConnection::connect returned %d", m_status );
	#endif

	return m_status;		
}

jsConnection::Status jsConnection::bind( UINT port )
{
   	m_state = jsConnection::BIND;
	bool st = m_sock->bind( port ) ; 
	m_lastAccess = jsTime::now();
	return _status( st );
}

#ifdef _MSC_VER
// for template sign/unsign mismatch for some reason this removes them
#pragma warning (disable : 4018 ) 
#endif
 
