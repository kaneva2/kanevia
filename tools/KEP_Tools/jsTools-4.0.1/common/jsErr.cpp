/******************************************************************************
 Name: jsErr.cpp

 Description: error functions

 Copyright (C) 1999, 2004 Jim Wallace

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 History:
 6/7/99		jmw created

 Notes: 

******************************************************************************/
#include "js.h"
#include "jsErr.h"

std::vector<jsErrString> jsErr::jsErrStrings;

void jsErr::addStringRes( jsStringRes resource, ULONG minErr, ULONG maxErr,
		UINT stringBase ) 
{
	jsErrStrings.push_back( jsErrString( resource, minErr, maxErr, stringBase ) );
}

LPCTSTR jsErr::asString( OUT jsString &s, OUT bool *loaded /*= 0*/ )
{
	//BOOL bErr = FAILED(hr);

	bool found = false;

#ifdef _NT
	TCHAR *p;
	ULONG dwSize = ::FormatMessage(
		FORMAT_MESSAGE_IGNORE_INSERTS | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_ALLOCATE_BUFFER,
		NULL,
		m_err,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_NEUTRAL),
		(TCHAR*)&p,
		1,
		NULL);

    if (dwSize>2)
    {
		// Take out the trailing CRLF.
		p[--dwSize] = 0;
		p[--dwSize] = 0;

		s = p;

		::LocalFree( p );

		found = true;

    }
	else 
#endif // _NT
	{

		// search the array
		for ( UINT i = 0; i < jsErrStrings.size(); i++ )
		{
			if ( m_err >= jsErrStrings[i].m_minErrNo && 
				 m_err <= jsErrStrings[i].m_maxErrNo )
			{
				found = s.loadString( (m_err - jsErrStrings[i].m_minErrNo)+jsErrStrings[i].m_stringIdBase );
				break;
			}
		}
		if ( !found )
			s.strPrintf( _T("0x%x"), m_err );
	}

	if ( loaded )
		*loaded = found;

	return s;
}
