/******************************************************************************
 Name: jsLibraryFunction.cpp

 Description: gets a function from a library

 Copyright (C) 1999, 2004 Jim Wallace

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 History:
 1/28/99		jmw created

 Notes: 

******************************************************************************/
#include "js.h"
#include "jsLibraryFunction.h"
#include <jsLibraryFunctionImpl.h>

///---------------------------------------------------------
// constructor
// 
// [in] lib name of lib including extension, without dir
// [in] freeOnDelete set to false to not free/close the lib on 
//      delete of the object.  Sometimes this can be useful in debugging
//		etc.
//
jsLibraryFunction::jsLibraryFunction( const char* lib, bool freeOnDelete /*= true*/)
{
	m_impl = new jsLibraryFunctionImpl( lib, freeOnDelete );
	setOk( m_impl != 0 );
}

///---------------------------------------------------------
// getFunction
// 
// [in] procName name of procedure in lib. always single byte
//
// [Returns]
//    the fn ptr or null if error.  In that case lastError() is valid
JSLIBFUNCTION jsLibraryFunction::getFunction( const char* procName )
{
	jsVerifyReturn( ok(), 0 );
	return m_impl->getFunction( procName ); 
}

ULONG jsLibraryFunction::lastError() const
{
	jsVerifyReturn( ok(), jsErr( E_INVALIDARG ) );
	return m_impl->lastError(); 
}

const char* jsLibraryFunction::libraryName() const
{
	jsVerifyReturn( ok(), 0 );
	return m_impl->libraryName(); 
}

jsLibraryFunction::~jsLibraryFunction() 
{ 
	if ( m_impl ) 
		delete m_impl; 
}
