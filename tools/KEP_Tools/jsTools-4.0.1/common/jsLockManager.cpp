/******************************************************************************
 Name: LockMangager.cpp

 Description: these functions help prevent deadlock in the debug (_JDBG)
 builds

 Copyright (C) 1996, 2004 Jim Wallace

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 History:
 11/20/96	jmw created

 Notes: 

******************************************************************************/
#include "js.h"

#include "jsLock.h"
#include "jsThread.h"
#include "jsLockManager.h"

ULONG jsLockManager::timeoutWarningMS = 2000;		// default timeout for warning
jsLockManager *jsLockManager::theLockManager = 0;	// one and only lock manager


#if defined (_JSUSELM )
#include "jsConfiguration.h"

static const int cMaxLevel = FASTSYNC_LEVEL;

BYTE *jsLockManager::lockMgrList()
{
	return (BYTE*)jsThread::getThreadData( m_listIndex );
}

/******************************************************************************

 Description: starts up the lock mgr.

******************************************************************************/
jsLockManager::jsLockManager()
{
	m_listIndex = jsThread::createThreadData();
	jsLM = this;
}

/******************************************************************************

 Name: jsLockManager::~jsLockManager

 Description: cleans up the list

******************************************************************************/
jsLockManager::~jsLockManager()
{
	for( UINT i = 0; i < freeLists.size(); i++ )
	{
		delete freeLists[i];
	}
	
}

/******************************************************************************

 Description: allocate a list and init it for this thread

 Returns: list ptr

******************************************************************************/
BYTE * jsLockManager::allocList()
{
	BYTE * k;
	int i = freeLists.size();
	if (i == 0)
	{
		k = new BYTE[sizeof(BYTE)*(cMaxLevel+1)];
	}
	else
	{
		k = (BYTE*)freeLists[i-1];  // remove last element
		freeLists.erase( freeLists.begin()+(i-1) );
	}
	::memset(k,0,(cMaxLevel+1)*sizeof(BYTE));

	jsVerify ( jsThread::setThreadData( m_listIndex, (ULONG)k ) );

	return(k);
}

/******************************************************************************

 Description: add item to list for lock mgr

******************************************************************************/
void jsLockManager::freeList(BYTE * l)
{
	freeLists.push_back(l);
}

/******************************************************************************

 Description: checks to see if this level is being used and prints to the 
 debugger the results so you can see who is using what levels

 Implementation:

 Returns: true if ok to lock at that level
 	      false if may cause a deadlock

******************************************************************************/
bool jsLockManager::checkLevel( const char* name, BYTE level )
{
	if ( level >= cMaxLevel )
		return true; 

	if ( jsLM == 0 )
		return true; // if not init'ed just allow it

	jsFastLock lock( jsLM->m_LockMgrSync );

	for ( UINT i = 0; i < jsLM->m_levels.size(); i++ )
		if ( jsLM->m_levels[i] == level )
		{
			jsOutputToDebugger( "LockMgr WARNING %s trying to get level %d but already used", 
				name, (int)level );
			return false;
			
		}

	jsLM->m_levels.push_back(level);

	jsOutputToDebugger( "LockMgr Registering %s at level %d", name, (int)level );

	return true;
}


/******************************************************************************

 Description: checks to see if you can get the lock of the given level for
 this thread.  If it is out of order, returns false

 Implementation:

 Returns: true if ok to lock at that level
 	      false if may cause a deadlock

******************************************************************************/
bool jsLockManager::notifyLock(
	BYTE level)
{
	jsAssert(level > 0 && level <= cMaxLevel);
	m_LockMgrSync.lock();

	if (lockMgrList() == 0)
	{
		allocList();
	}
	// lockMgrList()[0] is the number of the highest lock this thread
	// currently has.  The new lock must be larger than this, or
	// this level must already be locked by this thread.
	if ( level < lockMgrList()[0] && lockMgrList()[level] == 0 )
	{

		char p[1024];
		_stprintf( p, "Illegal locking order.  Thread %x has %d and wants %d\r\n",
    	          		jsThread::currentThreadID(),lockMgrList()[0],level);
		jsOutputToDebugger( p );

		jsConfiguration cfg;

		if ( cfg.get( "Debug", "jsLockManagerAssert", 1 ) )
			jsAssert(false);
	}
	lockMgrList()[0] = MAX(lockMgrList()[0],level);
	lockMgrList()[level]++;

	m_LockMgrSync.unlock();			
	return(true);
}

/******************************************************************************

 Description: releases the given level lock for the current thread.

 Returns: true if released ok
 	      false if didn't have lock

******************************************************************************/
bool jsLockManager::notifyUnlock(
	BYTE level)		// level of lock to release
{
	if (lockMgrList() == 0)
	{
		jsAssert(false);
		return(false);
	}
	jsAssert(level > 0 && level <= cMaxLevel);
	m_LockMgrSync.lock();
	jsAssert(lockMgrList()[level] > 0);
	lockMgrList()[level]--;
	
	BYTE i;
	for(i = lockMgrList()[0]; i > 0 && lockMgrList()[i] == 0; i--)
	{
			// do nothing in the for();
	}

	if (i == 0)
	{
		freeList(lockMgrList());
		jsVerify ( jsThread::setThreadData( m_listIndex, 0 ) );
	}
	else
	{
		lockMgrList()[0] = i;
	}
	m_LockMgrSync.unlock();			
	return(true);
}
#endif // _JSDBG
#ifdef _MSC_VER
// for template sign/unsign mismatch for some reason this removes them
#pragma warning (disable : 4018 ) 
#endif
 
