/******************************************************************************

 Name: jsInetInfo.cpp
 Abbreviation: 

 Description: implementation of the internet infor class

 Copyright (C) 1996, 2004 Jim Wallace

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 History:
 1/26/97		jmw created via AS

 Notes:		All information related to gethostbyname and gethostbyaddress is encapsulated
			in this class. Again, this class is not **thread safe***. Instantiate a
			new object for each thread or instance you need the info.


******************************************************************************/
#include "js.h"
#include "jsInetinfo.h"
#include "jsTcpImpl.h"
#include "jsNet.h"

jsInetInfo::jsInetInfo( const char* hname ) : m_sock( new jsTCPImpl( 0 ) )
{
	setOk ( false );
	m_loaded = false;

	if ( m_sock )
	{
		if ( hname )
			m_hostName = hname;
		else
			m_hostName = m_sock->hostName();

		if ( getAllHostInfo( m_hostName, m_dnsName, m_hostAlias, m_hostAddr ) )
		{
			m_loaded = true;
			setOk ( true );
		}
	}
}


jsInetInfo::~jsInetInfo()
{
}

#ifdef _MSC_VER
// for template sign/unsign mismatch for some reason this removes them
#pragma warning (disable : 4018 ) 
#endif
 
