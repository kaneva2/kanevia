/******************************************************************************

 Name: jsSempahore.cpp
 Abbreviation: 

 Description: portable portion of the synchronization interface classes 
 Most of this is simply passthrough calls to the implementation.  The 
 ReadWrite sync does have its implementatin here since it uses the other classes

 Copyright (C) 1996 Jim Wallace

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 History:
 11/26/96		jmw created

 Notes: 

******************************************************************************/
#include "js.h"

#include "jsSemaphore.h"
#include <jsSemaphoreImpl.h>	// implmentation of semaphore

jsSemaphore::jsSemaphore( 
	ULONG initCount, // = 0 
	ULONG maxCount   // = ULONG_MAX
	) : m_impl( new jsSemaphoreImpl( initCount, maxCount ) )
{
}

jsSemaphore::~jsSemaphore()
{
	delete m_impl;
}

bool jsSemaphore::increment(
	UINT count 	// = 1
	)
{
	jsAssert( m_impl && m_impl->ok() );
	return m_impl->increment( count );
}


jsWaitRet jsSemaphore::waitAndDecrement( ULONG timeoutMS /*= INFINITE*/, ULONG unixPollMS /*= 100*/ )
{
	jsAssert( m_impl && m_impl->ok() );
	return m_impl->waitAndDecrement( timeoutMS, unixPollMS );
}

jsWaitRet jsSemaphore::tryWaitAndDecrement()
{
	jsAssert( m_impl && m_impl->ok() );
	return m_impl->tryWaitAndDecrement();
}
#ifdef _MSC_VER
// for template sign/unsign mismatch for some reason this removes them
#pragma warning (disable : 4018 ) 
#endif
 
