/******************************************************************************
 Name: js.h

 Description: error object that is similar to an HRESULT 

 Copyright (C) 1996, 2004 Jim Wallace

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 History:
 1/99		jmw created

 Notes: 

******************************************************************************/
#ifndef __JSERR_H__
#define __JSERR_H__

#ifndef _js_h_
	#error Include js.h before jsErr.h
#endif

#include "string.h"

#ifdef _WIN32
#include <wtypes.h> // for HRESULT 
#include <winerror.h>
#else
	// Declare this stuff for non-Windows platforms
	typedef	LONG	HRESULT;
	
	#define NO_ERROR	0L
	
	#define HRESULT_CODE(hr)	((hr) & 0xFFFF)
	#define HRESULT_FACILITY(hr)	(((hr) >> 16) & 0x1FFF)
	#define HRESULT_SEVERITY(hr)	(((hr) >> 31) & 0x1)
	
	#define MAKE_HRESULT(sev,fac,code) \
			((HRESULT) (((ULONG) (sev) << 31) | ((ULONG) fac << 16) | ((ULONG) code) ) )
		
	#ifdef _MAC
		#define E_INVALIDARG	paramErr
	#endif
#endif

/*==========================================================
CLASS
	jsErr
	
	an error class like HRESULT

DESCRIPTION

==========================================================*/
class JSEXPORT jsErr
{
public:

	/// types of errors (Win32 Facility codes)
	enum errType
	{
		null=0,
		rpc=1,
		dispatch=2,
		storage=3,
		itf=4,			// user defined
		win32=7,		// regular win32 error
		windows=8,
		sspi=9,
		control=10,
		cert=11,
		internet=12,
		mediaserver=13,
		msmq=14,
		setupapi=15
	};

	///---------------------------------------------------------
	// constructor from a ulong 
	// 
	// [in] ul the number to set this to.  This is when you 
	// already have an HRESULT.  If you pass in a number less
	// than 0xffff (not an HRESULT) it makes a win32 HRESULT from
	// ul
	//
	jsErr( LONG ul = NO_ERROR ) : m_err(ul) { set(ul); }


	///---------------------------------------------------------
	// constructor from an error number
	// 
	// [in] err the error number
	// [in] et the type of error this is 
	//
	jsErr( LONG err, errType et  ) { set( err, et ); }

	///---------------------------------------------------------
	// copy constructor 
	// 
	// [in] src the item to copy 
	//
	jsErr( const jsErr &err ) { operator=(err ); }


	///---------------------------------------------------------
	// cast to ULONG operator
	//
	// [Returns]
	//    ULONG value
	operator ULONG() const { return m_err; }


	///---------------------------------------------------------
	// set the value.
	// 
	// [in] errno the error number
	// [in] et the type of error this is 
	//
	inline void set( LONG err, errType et ) 
		{
			if ( err != NO_ERROR )
				m_err = MAKE_HRESULT(err != 0,et,err);
			else
				m_err = NO_ERROR;
		}


	///---------------------------------------------------------
	// set the value.  If no facilty set, then it makes a win32 error
	// from it
	// 
	// [in] errno the error number
	//
	inline void set( LONG err ) 
		{

			if ( HRESULT_FACILITY( err ) == 0 && HRESULT_SEVERITY( err ) == 0 ) 
				set( err, win32 );
			else
				m_err = err;
		}

	///---------------------------------------------------------
	// [Returns]
	//    true if this is an error code
	bool isErr() const { return HRESULT_SEVERITY( m_err ) != 0; }

	///---------------------------------------------------------
	// [Returns]
	//    true if this is a not an error code.  OK, or a warning
	bool ok() const { return HRESULT_SEVERITY( m_err ) == 0; }

	///---------------------------------------------------------
	// [Returns]
	//    the error number 
	LONG errorNo() const { return HRESULT_CODE(m_err); }

	///---------------------------------------------------------
	// [Returns]
	//    the error type
	errType errorType() const { return (errType)HRESULT_FACILITY( m_err ); }

	///---------------------------------------------------------
	// copy operator
	// 
	// [in] src the item to copy 
	//
	// [Returns]
	//	  this
	jsErr &operator=( const jsErr &err ) { m_err = err.m_err; return *this; }

private:
	ULONG	m_err;
};
#endif //__JSERR_H__
