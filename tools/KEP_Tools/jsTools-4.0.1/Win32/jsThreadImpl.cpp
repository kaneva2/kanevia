/******************************************************************************
 Name: jsThreadImpl.cpp

 Description: Win32 implemenation of the thread class

 Copyright (C) 1996, 2004 Jim Wallace

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 History:
 11/20/96	jmw created

 Notes: 

******************************************************************************/
#include "js.h"

// for warining level 4 disable a bunc of crap
#pragma warning ( disable : 4201 4214 4115 4514 )
#include <windows.h> 
#include <winbase.h>
#pragma warning ( default : 4201 4214 4115 )

#include <process.h>

#include "jsThreadImpl.h"
#include "jsThread.h"

/*****************************************************************************

 Description:  get the current NT thread id

******************************************************************************/
jsThreadID jsThreadImpl::currentThreadID() 
{ 
	return ::GetCurrentThreadId(); 
}

/*****************************************************************************

 Description:  initialize private memeber.  the thread is not really
				created until CreateThread is called.

******************************************************************************/
jsThreadImpl::jsThreadImpl( 
	jsThread *thd, 
	const char* name, 
	jsThdPriority prio, // = PR_NORMAL 
	jsThreadType  //type = TT_NORMAL
	) : 
	m_thd(thd),
	m_name(name),
	m_wake( false, false ),
	m_nPriority(jsThreadImpl::mapPriority(prio)),
	m_handle(0),
	m_threadID(0),
	m_bTerminated(false),
	m_bSuspendOnCreate(false),
	m_bStarted(false)

{
	#if _MSC_VER >= 900 & defined(_WINDOWS) & defined( _MFC_VER )
		m_WinThd = 0;
	#endif
}

/*****************************************************************************

 Description:  If the thread is running, terminate it.  If the handle
				is open, close it.
				
 !not thread safe
******************************************************************************/
jsThreadImpl::~jsThreadImpl()
{
	 // was -> kill(); // cleans up thread if still running
	if ( isRunning() )
		jsOutputToDebugger( "Warning thread %x destroyed when still running!", m_threadID );
}

/*****************************************************************************

 Description:  kill a thread.  This is not the recommended method of 
		ending a thread since it may leave locks locked, and it will
		leak resources and memory

******************************************************************************/
void jsThreadImpl::kill()
{
	// don't kill a thread twice.  And don't terminate if it is
	// deleting this object, or there is a memory leak
	if (m_handle != 0 && m_threadID != ::GetCurrentThreadId() && isRunning() )
	{
		::TerminateThread(m_handle,KILLED_EXIT_CODE);
	}
	if (m_handle != 0)
	{
		::CloseHandle(m_handle);
		m_handle = 0; // do this in case delete ourselves before create returns
	}

	#if (_MSC_VER >= 900) & defined(_WINDOWS) & defined( _MFC_VER )
		delete m_WinThd;
	#endif
}

typedef struct tagTHREADNAME_INFO
{
   DWORD dwType; // must be 0x1000
   const char* szName; // pointer to name (in user addr space)
   DWORD dwThreadID; // thread ID (-1=caller thread)
   DWORD dwFlags; // reserved for future use, must be zero
} THREADNAME_INFO;

static void SetThreadName( DWORD dwThreadID, LPCSTR szThreadName)
{
#ifdef _JSDBG
   THREADNAME_INFO info;
   info.dwType = 0x1000;
   info.szName = szThreadName;
   info.dwThreadID = dwThreadID;
   info.dwFlags = 0;

   __try
   {
      RaiseException( 0x406D1388, 0, sizeof(info)/sizeof(DWORD), (DWORD*)&info );
   }
   __except(EXCEPTION_CONTINUE_EXECUTION)
   {
   }
#endif
}

/*****************************************************************************

 Description: Newly created threads always start in this function.
				the only parameter is a void pointer to the C++ thread
				object.

******************************************************************************/
ULONG _stdcall jsThreadImpl::ThreadEntryPoint(LPVOID lpvThis)
{
	jsThreadImpl * pThread = (jsThreadImpl *)lpvThis;

	SetThreadName( -1, pThread->m_name.c_str() );
	pThread->m_bStarted = true;
	ULONG result = pThread->m_thd->_doWork();

//do we need this?	pThread->ExitThread(result);

	_endthreadex(result); // not really needed since called on return

	return(result);
}

/*****************************************************************************

 Description: create the thread and start it if suspend not on in consts

 Implementation: 

 Returns: true if created it ok and handle is valid
 		  false if error creating thread

******************************************************************************/
bool  jsThreadImpl::start()
{
	jsAssert(m_handle == 0 && m_threadID != 0xdddddddd );

	m_bStarted = false;

	jsOutputToDebugger( "About to create thread" );

	#if (_MSC_VER >= 900 ) & defined(_WINDOWS) & defined( _MFC_VER )
		m_WinThd = ::AfxBeginThread( (AFX_THREADPROC)EntryPoint, (LPVOID)this,
				 m_nPriority, 0, CREATE_SUSPENDED, NULL );
		m_handle = m_WinThd->m_hThread;
		m_threadID = m_WinThd->m_nThreadID;

		wsprintf( s, "Started thread '%s'.  ID == %lx\n",
					(LPCSTR)m_name,m_threadID);
		OutputDebugString( s );

		if (!m_bSuspendOnCreate && m_handle != 0)
		{
			m_WinThd->ResumeThread();
		}
	#else
		m_handle = (HANDLE)::_beginthreadex( NULL, 0, (UINT (__stdcall*)(void*))ThreadEntryPoint,(LPVOID)this,
								CREATE_SUSPENDED,(UINT*)&m_threadID);
/*		m_handle = ::CreateThread(NULL, 0, ThreadEntryPoint,(LPVOID)this,
								CREATE_SUSPENDED,&m_threadID);
		if (m_handle != 0)
*/		if ( m_handle != (HANDLE)-1 )
		{
			jsOutputToDebugger( "Started thread '%s'.  ID == %lx\n",
						m_name.c_str(),m_threadID);

			::SetThreadPriority(m_handle,m_nPriority);
		}
		::ResumeThread( m_handle );
	#endif

	return(m_handle != 0);
}

/*****************************************************************************

 Description:  This can only be called after a thread has terminated.
				It clears out state values to allow CreateThread to
				be called again.

******************************************************************************/
bool jsThreadImpl::reset()
{
	if (m_handle != 0)
	{
		::CloseHandle(m_handle);
	}

	#if (_MSC_VER >= 900) & defined(_WINDOWS) & defined( _MFC_VER )
		delete m_WinThd;
		m_WinThd = 0;
	#endif

	m_nPriority = THREAD_PRIORITY_NORMAL;
	m_handle = 0;
	m_threadID = 0;
	m_bTerminated = false;
	m_bSuspendOnCreate = false;
	m_bStarted = false;

    return true;
}

/*****************************************************************************

 Description: Terminates a thread.  If you want to be nice, you can
				pass a non-0 value.  This will cause the member
				m_bTerminated to be set to false then wait the given
				amount of time for the thread to exit gracefully.
				After that time, the thread is killed if it hasn't
				exited.

******************************************************************************/
jsThread::StopRet jsThreadImpl::stop(
	ULONG niceWait,		// = WAIT_FOREVER, ms wait
	bool killIfTimeout	// = true
	)
{
	jsThread::StopRet result = jsThread::srTimeout;

	m_bTerminated = true;
	m_wake.releaseWaiters(); // in case it's asleep
	
	if ( m_handle != 0 && m_bStarted)
	{
		// keep resuming until the suspend count is 0
		//while (Resume() > 0);

		DWORD dwRet = ::WaitForSingleObject(m_handle,niceWait);

		if ( dwRet == WAIT_OBJECT_0 )
		{
			result = jsThread::srStoppedOk;
		}
		else if ( dwRet == WAIT_TIMEOUT && killIfTimeout )
		{
			result = ::TerminateThread(m_handle,KILLED_EXIT_CODE) ? jsThread::srKilled : jsThread::srKillFailed;
		}
		else if ( dwRet != WAIT_TIMEOUT ) // error 
		{
			result = jsThread::srKillFailed; // error!
		}
	}
	else 
		result = jsThread::srStoppedOk; // not started

	return(result);
}

/*****************************************************************************

 Description: If the thread has been created, and hasn't exited,
				and is not suspended, this function returns true.
				To see if the thread is suspended, we suspend it
				and resume it.  The count returned by Suspend will be
				0 if and only if it was not previously suspended. In
				any case, the suspend state will be the same when this
				thread exists as when it is entered.

******************************************************************************/
bool jsThreadImpl::isRunning() const
{
	if (m_handle == 0 || exitCode() != STILL_ACTIVE)
	{
		return(false);
	}

	if (::GetCurrentThread() == m_handle)
	{
		// this is the thread in question, so it must be running.
		return(true);
	}
	
/*	int count = Suspend();
	if (count == -1)
	{	
		return(false);
	}
	else
	{
		Resume();
		return(count == 0);
	}
*/
	return true;
}


/*****************************************************************************

 Description: put the thread to a wake-able sleep by waiting on event

 Returns: true if timed out
 		  false if ended wait for any other reason

******************************************************************************/
bool jsThreadImpl::_sleep(ULONG ms)
{
	m_wake.haltWaiters();
	if (m_wake.wait(ms) == WR_OK)
	{
		return (true);
	}
	else
	{
		return (false);
	}
}

/*****************************************************************************

 Description: just set the event so wakes up

******************************************************************************/
void jsThreadImpl::wakeUp()
{
	m_wake.releaseWaiters();
}

/*****************************************************************************

 Description: private fn for mapping to NT

******************************************************************************/
int	jsThreadImpl::mapPriority( 
	jsThdPriority mpp 
	)
{
	int i = THREAD_PRIORITY_NORMAL; 

	switch ( mpp )
	{
		case PR_NORMAL:
			i = THREAD_PRIORITY_NORMAL;
			break;
		case PR_LOW:
			i = THREAD_PRIORITY_BELOW_NORMAL;
			break;
		case PR_HIGH:
			i = THREAD_PRIORITY_ABOVE_NORMAL;
			break;
		case PR_LOWEST:
			i = THREAD_PRIORITY_LOWEST;
			break;
		case PR_HIGHEST:
			i = THREAD_PRIORITY_HIGHEST;
			break;
		default:
			jsAssert(false);
			break;
	};

	return i;
}

/*****************************************************************************

 Description: private fn for mapping to NT

******************************************************************************/
jsThdPriority jsThreadImpl::unmapPriority( 
	int	mpp 
	)
{
	jsThdPriority i = PR_NORMAL; 

	switch ( mpp )
	{
		case THREAD_PRIORITY_NORMAL:
			i = PR_NORMAL;
			break;
		case THREAD_PRIORITY_BELOW_NORMAL:
			i = PR_LOW;
			break;
		case THREAD_PRIORITY_ABOVE_NORMAL:
			i = PR_HIGH;
			break;
		case THREAD_PRIORITY_LOWEST:
			i = PR_LOWEST;
			break;
		case THREAD_PRIORITY_HIGHEST:
			i = PR_HIGHEST;
			break;
		default:
			jsAssert(false);
			break;
	};

	return i;
}

/*****************************************************************************

 Description: 

******************************************************************************/
void jsThreadImpl::_yield()
{
	_sleep(1);
}

/*****************************************************************************

 Description: TLS functions

******************************************************************************/
int		jsThreadImpl::createThreadData()
{
	return ::TlsAlloc();
}

ULONG	jsThreadImpl::getThreadData( int id )
{
	return (ULONG)::TlsGetValue( id );
}

bool	jsThreadImpl::setThreadData( int id, ULONG value )
{
	return ::TlsSetValue( id, (PVOID)value ) == TRUE;
}

void 	jsThreadImpl::freeThreadData( int id )
{
	::TlsFree( id ); // nt returns bool
}


/*****************************************************************************

 Description: for NT to join the thread is to wait for its handle

******************************************************************************/
bool jsThreadImpl::join(ULONG timeMS) const
{
	return ::WaitForSingleObject( m_handle, timeMS ) == WAIT_OBJECT_0;
}

ULONG  jsThreadImpl::exitCode() const 
{
	 ULONG result;
	::GetExitCodeThread(m_handle,&result);
	return(result);
}

jsThdPriority jsThreadImpl::priority() const 
{
	int i = ::GetThreadPriority(m_handle);
	return unmapPriority( i );
}


void jsThreadImpl::priority(jsThdPriority priority) 
{
	m_nPriority = mapPriority(priority);

	if ( m_handle )
		::SetThreadPriority(m_handle,m_nPriority);

}

bool 	jsThreadImpl::isSleeping() const
{ 
	return m_wake.wait(0) == WR_TIMEOUT; 
}

bool jsThreadImpl::isCreated() const 
{
	return exitCode() == STILL_ACTIVE;
}
#ifdef _MSC_VER
// for template sign/unsign mismatch for some reason this removes them
#pragma warning (disable : 4018 ) 
#endif
