/******************************************************************************
 Name: jsSocket.cpp

 Description: socket class definition - static method implementation.

 Copyright (C) 1996, 2004 Jim Wallace

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 History:
 1/20/97	jmw created

 Notes: 

******************************************************************************/

#include "js.h"
#include "jsSocket.h"

#include "ustring.h"


/*****************************************************************************
 Description:  convert four bytes to network long ip address
******************************************************************************/
inline ULONG jsSocket::inet_addr( BYTE a, BYTE b, BYTE c, BYTE d )
{
	return  htonl(((a) << 24) | ((b) << 16) | ((c) << 8) | (d)) ;
}


/*****************************************************************************
 Description:  convert string to long ip address
******************************************************************************/
ULONG jsSocket::inet_addr( const char* addr )
{
	ULONG u = ::inet_addr( _U(addr) );
	if ( u == INADDR_NONE )
		return (ULONG)bad_addr;
	else
		return u;
}


/*****************************************************************************
 Description:  convert host long to network long
******************************************************************************/
/*
**NOTE** Again solaris CC 4.1 doesn't like the "::" in front of htonl or ntohl
So please don't put them back!
*/
ULONG jsSocket::nethtonl( ULONG hostaddr )
{
	return  htonl( hostaddr );
}


/*****************************************************************************
 Description:  convert network long to host long
******************************************************************************/
ULONG jsSocket::netntohl( ULONG netaddr )
{
	return ntohl( netaddr );
}


/*****************************************************************************
 Description:  returns true if addr is an internet address
******************************************************************************/
bool jsSocket::is_inet_addr( const char* addr ) 
{
	return inet_addr( addr ) != bad_addr;
}


/*****************************************************************************
 Description:  convert address in network long to a string
******************************************************************************/
std::string jsSocket::inet_ntoa( ULONG addr )
{
	std::string s;
	struct in_addr *inaddr = (in_addr *)&addr; //AS does this and it works ok

	char *p = ::inet_ntoa( *inaddr );
	if ( p )
		s = _U(p);

	return s;
}



 
 
 
 
 
