/******************************************************************************
 Name: jsSyncImpl.cpp

 Description: Win32 implementationof the synchronization classes
 Not alot of comments here since pretty thin wrapper over the Win32 API

 Copyright (C) 1996, 2004 Jim Wallace

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 History:
 11/20/96	jmw created

 Notes: 

******************************************************************************/
#include "js.h"

#include "./jsSyncimpl.h"

/*jsFastSyncImpl**************************************************************/
jsFastSyncImpl::jsFastSyncImpl( 
	bool lockNow // = false
	)
{ 
	::InitializeCriticalSection(&m_cs);
	if ( lockNow )
		lock();
				
	setOk(true);
}

jsFastSyncImpl::~jsFastSyncImpl() 
{ 
	::DeleteCriticalSection(&m_cs);
}

void jsFastSyncImpl::lock() 
{ 
	::EnterCriticalSection(&m_cs);
}

void jsFastSyncImpl::unlock() 
{ 
	::LeaveCriticalSection(&m_cs);
}

/*jsEventImpl******************************************************************/
jsEventImpl::jsEventImpl( bool InitiallySetToHalt, bool releaseAllOnRelease )
{
	// second parm is manual reset
	// third is initial signaled state
	m_handle = ::CreateEvent(NULL,releaseAllOnRelease,!InitiallySetToHalt,NULL);
	setOk( m_handle != 0 );
}

jsEventImpl::~jsEventImpl() 
{ 
	::CloseHandle(m_handle);
}

bool jsEventImpl::haltWaiters()		
{ 
	return ::ResetEvent(m_handle) == TRUE;
}

bool jsEventImpl::releaseWaiters()	
{ 
	return ::SetEvent(m_handle) == TRUE;
}

jsWaitRet jsEventImpl::wait(
	DWORD timeOut 	//= INFINITE
	) const
{ 
	return(jsMapWaitRet(::WaitForSingleObject(m_handle,timeOut)));
}

jsWaitRet jsEventImpl::msgWait(
	DWORD timeOut 	//= INFINITE
	) const
{ 
	// setup the handles for waiting
	unsigned short uWAIT_OBJECTS = 1 ;
	HANDLE ahWaitObjects[ 1 ] ; // max wait objects
	ahWaitObjects[0] = m_handle;
	DWORD start = GetTickCount();
	DWORD ret = 0;

	while ( true )
	{
		ret = ::MsgWaitForMultipleObjects(
					uWAIT_OBJECTS,	// number of handles in the handle array
					ahWaitObjects,	// pointer to the object-handle array
					FALSE,			// wait for all or wait for one
					timeOut,		// time-out interval in milliseconds
					QS_ALLINPUT		// type of input events to wait for
					);
		
		if ( ret == WAIT_OBJECT_0+uWAIT_OBJECTS )
		{
			// We have been signalled for an event other than the one for which we are waiting.
			// Attempt to process this event then wait again for our event.

			// Decrement the timeout for the time we have been waiting.
			if ( timeOut != INFINITE_WAIT )
			{
				DWORD elapsed = ::GetTickCount()-start;
				if ( elapsed > timeOut )
					timeOut = 0;
				else
					timeOut -= elapsed;
			}

			MSG msg;
			while (PeekMessage(&msg, 0, 0, 0, TRUE))
					DispatchMessage(&msg);
		}
		else
		{
			break;
		}
	}

	return jsMapWaitRet( ret );
}

/*jsSyncImpl******************************************************************/
jsSyncImpl::jsSyncImpl()
{
	m_handle = ::CreateMutex(NULL,false,0);
	
	jsAssert( m_handle != 0 );

	setOk(m_handle != 0);
}

jsSyncImpl::~jsSyncImpl()
{
	::CloseHandle(m_handle); 
}

void jsSyncImpl::unlock()	
{
	::ReleaseMutex(m_handle);
}

jsWaitRet jsSyncImpl::lock( ULONG timeout )
{
	DWORD ret = ::WaitForSingleObject(m_handle,timeout);
	
	return jsMapWaitRet( ret );
}

jsWaitRet jsSyncImpl::tryLock()
{
	DWORD timeout = 0;
	DWORD ret = ::WaitForSingleObject(m_handle,timeout);
	
	return jsMapWaitRet( ret );
}


#ifdef _MSC_VER
// for template sign/unsign mismatch for some reason this removes them
#pragma warning (disable : 4018 ) 
#endif
