/******************************************************************************
 Name: jsSyncImpl.h

 Description: Win32 definition of the synchronization implementation classes

 Copyright (C) 1996, 2004 Jim Wallace

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 History:
 11/20/96	jmw created

 Notes: 

******************************************************************************/
#ifndef _syncImpl_h_
#define _syncImpl_h_

#include "jsSync.h"

#pragma warning ( disable : 4201 4214 4115 4514 )
#include <windows.h>
#pragma warning ( default : 4201 4214 4115 )

//---------------------------------------------------------
// map a Win32 return code to the jsWaitRet
//
// [in] ret the win32 return code
//
// [Returns]
//		the mapped return code
jsWaitRet jsMapWaitRet( IN DWORD ret );


/******************************************************************************
 
 Description: Event implementation.

******************************************************************************/
class jsEventImpl : public jsBase
{
public:
	jsEventImpl( bool InitiallySetToHalt, bool releaseAllOnRelease );

	jsWaitRet wait( DWORD timoutMS = INFINITE_WAIT ) const;

	jsWaitRet msgWait( DWORD timoutMS = INFINITE_WAIT ) const;

	bool releaseWaiters();
	bool haltWaiters();

	~jsEventImpl();

private:
	HANDLE	m_handle;
};

/******************************************************************************
 
 Description: implementation of the Fastsync object. This is for very 
 fast locking, if such an implementation can exist.  For NT it is a cs. 
 You cannot call any other locks during this one

******************************************************************************/
class jsFastSyncImpl : public jsBase
{
public:
	jsFastSyncImpl( bool lockNow = false );

	void lock();
	void unlock();

	~jsFastSyncImpl();

private:
	_RTL_CRITICAL_SECTION	m_cs;
};

/******************************************************************************
 
 Description: sync object.  Can be called recursively from the thread
 that holds the lock.

******************************************************************************/
class jsSyncImpl : public jsBase
{
public:
	jsSyncImpl();

	jsWaitRet lock( ULONG timeout );
	jsWaitRet tryLock();
	void unlock();

	~jsSyncImpl();

private:
	HANDLE	m_handle;	// mutex handle
};

#endif
