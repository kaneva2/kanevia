/******************************************************************************
 Name: jsEnumFilesImpl.h

 Description: enumerate files in a directory

 Copyright (C) 1996, 2004 Jim Wallace

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 History:
 1/28/99		jmw created

 Notes: 

******************************************************************************/
#ifndef _jsEnumFilesImpl_h_
#define _jsEnumFilesImpl_h_

#ifndef _js_h_
	#error Include js.h before jsEnumFilesImpl.h
#endif

#include <windows.h>
#include <stdlib.h> // for max path

/***********************************************************
-CLASS
	jsEnumFilesImpl
	
	class to enumerate files in a directory

DESCRIPTION
	This is the Win32 version.

***********************************************************/	
class JSEXPORT jsEnumFilesImpl : public jsBase
{
public:
	//---------------------------------------------------------
	// constructor
	// 
	// [in] fileMask the mask of files to look for
	// [in] includeDirs include dirs
	//
	jsEnumFilesImpl( const wchar_t* fileMask, bool includeDirs );

	//---------------------------------------------------------
	// get the next file.  Only if this returns true will
	// the current file name, etc. be valid
	//
	// [Returns]
	//    true if got another file.  
	bool next();

	//---------------------------------------------------------
	// [Returns]
	//    the current file name without its path
	const wchar_t* currentFileName() const { return m_findData.cFileName; }

	//---------------------------------------------------------
	// [Returns]
	//    the current directory name without the file name, will
	// not have trailing slash
	const wchar_t* currentDirName() const { return m_driveDir; }

	//---------------------------------------------------------
	// [Returns]
	//    the current path name
	const wchar_t* currentPath() { return m_curPath; }


	jsTime currentLastWriteTime() const;

	//---------------------------------------------------------
	// [Returns]
	//    the current attribute
	ULONG currentAttrs() { return mapAttrToJS( m_attrs ); }

	~jsEnumFilesImpl() { if ( m_hFind != INVALID_HANDLE_VALUE ) ::FindClose( m_hFind ); }

private:
	bool checkFile();
//	ULONG mapAttrFromJS( ULONG attributes );
	ULONG mapAttrToJS( ULONG attributes );

	bool		m_recurse;
	int			m_attrs;

	HANDLE			m_hFind;
	WIN32_FIND_DATAW m_findData;

	wchar_t		m_driveDir[_MAX_PATH+1];
	wchar_t		m_curPath[_MAX_PATH+1];
	wchar_t		m_fileMask[_MAX_PATH+1];

	bool		m_hasFile,
				m_includeDirs;
};
#endif
