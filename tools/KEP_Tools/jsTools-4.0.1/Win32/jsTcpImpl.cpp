/******************************************************************************
 Name: jsTCPImpl.cpp

 Description: socket implementation class for TCP/IP implementation

 Copyright (C) 1996, 2004 Jim Wallace

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 History:
 1/22/97	jmw created via AS

   Notes: 	An implementation class for TCP/IP. 
			For convenience, this includes, all code
			needed maybe by most of the derived classes like Connection, ServerConnection, 
			InetInfo.... 
			This makes it easier to port to any other platform. ie. we do it only here.
			This class, should never be accessed directly.
			Always come in thru the derived object, if you should need to do anything
			at all with TCP!

  CAUTION:	This class is ***NOT ThreadSafe*******.
			The caller is responsible for thread safeness.	


******************************************************************************/
#include "js.h"
#include "jsSync.h"
#include "jsLock.h"
#include "jsTcpImpl.h"
#include "ustring.h"

static const char * MODNAME = "jsTCPImpl";

// member data that is static 
bool jsTCPImpl::m_loaded = false;	// have we loaded TCP/IP
jsFastSync jsTCPImpl::m_cs;			// serialize 


/*****************************************************************************

 Description: constructs the TCP/IP implementation

******************************************************************************/
jsTCPImpl::jsTCPImpl( SOCKFD s ) :
	m_loadedHosts(false),
	m_blocking(true),
	m_eof(false),
	m_lastErr(0)
{
	setOk( false );

	if ( !m_loaded )
	{
		if ( !loadTcp() )
			return;
	}

	if ( s )
		m_sockFD = s;
	else
		m_sockFD = INVSOC;

	setOk ( true );
}

/*****************************************************************************

 Description: default constructor for the TCP/IP implementation

******************************************************************************/
jsTCPImpl::jsTCPImpl() :
	m_loadedHosts(false),
	m_blocking(true),
	m_eof(false),
	m_lastErr(0),
	m_sockFD(INVSOC)
{
	setOk( false );

	if ( !m_loaded )
	{
		if ( !loadTcp() )
			return;
	}

	setOk ( true );
}


/*****************************************************************************

 Description: destrutor closes the socket

******************************************************************************/
jsTCPImpl::~jsTCPImpl()
{
	#ifdef SOCKET_DEBUG
	jsTrace( "%s, Deleting jsTCPImpl object", MODNAME );
	#endif

	close(true);
}


/*****************************************************************************

 Description: duplicate from existing socket

******************************************************************************/
void jsTCPImpl::duplicate( const jsSocket & from )
{
	// Convert to the "real" implementation
	const jsTCPImpl & reallyFrom = dynamic_cast<const jsTCPImpl &>( from );

	// Do assignment
	m_sockFD = reallyFrom.m_sockFD;
	m_blocking = reallyFrom.m_blocking;
	m_loadedHosts = reallyFrom.m_loadedHosts;
   	m_eof = reallyFrom.m_eof;
	m_lastErr = reallyFrom.m_lastErr;
	m_inetInfo = reallyFrom.m_inetInfo;

	setOk( from.ok() );
}


/*****************************************************************************

 Description: initialize the tcp system, specially on NT

 Returns: true if TCP loaded ok
 		  false if got error, check lasterr if no_error, version mismatch

******************************************************************************/
bool jsTCPImpl::loadTcp()
{
	resetErr();

	jsVerifyReturn( !m_loaded, false );

	#ifdef _NT
	{
		jsFastLock mylock( jsTCPImpl::m_cs );

	    WSADATA            wsadata;
		if ( ::WSAStartup( DESIRED_WINSOCK_VERSION, &wsadata) ) 
		{
			m_lastErr = ::WSAGetLastError();
		    jsTrace("Can't initialize WinSock dll \n");
	        ::WSACleanup();
	        return false;
	    }

	    if ( wsadata.wVersion < MINIMUM_WINSOCK_VERSION ) 
	    {
			jsTrace("Winsock.. Bad version of WinSoc\n");
	        ::WSACleanup();
	        return false;
	    }
		m_loaded = true;
		return true;
	}
	#else
	{
		m_loaded = true;
		return true;
	} 
	#endif

}

/*****************************************************************************

 Description: release TCP by cleaning up any thing global

******************************************************************************/
void jsTCPImpl::releaseTcp()
{
	#ifdef _NT
		::WSACleanup();
	#endif

	m_loaded = false;

	return;
}

/*****************************************************************************

 Description: create a socket

 Returns: true if created 
 		  false if error, or socket already created

******************************************************************************/
bool jsTCPImpl::socket()
{
	resetErr();

	jsVerifyReturn( m_sockFD == INVSOC, false  );

	//always assume internet. Who cares about others anyway?
	m_sockFD = ::socket( AF_INET, SOCK_STREAM, 0 );

	if ( m_sockFD == INVSOC )
		m_lastErr = lastError();

	if ( m_sockFD == INVSOC )
	{
		jsTrace( "Socket(%d) socket call. Error '%d'", m_sockFD, lastError() );
		return false;
	}

	return true;
}


/*****************************************************************************

 Description: connect to address

 if we already have the internet address, directly connect

 Returns: true if connected
 		  false if error 

******************************************************************************/
bool  jsTCPImpl::connect( 
		ULONG addr,			// address in network format
		UINT port,			// port
		bool block,			// blocking mode?
		ULONG timeout )		// timeout in seconds
{
	#ifdef _SOCKET_DEBUG
	jsTrace("Entering jsTcpImpl::connect");
	#endif

	resetErr();

	if ( m_sockFD != INVSOC || !m_loaded || m_eof )
		return false;

	if ( !socket() )
		return false;

	if ( m_blocking != block )
	{
		if ( !setNonBlocking( !block ) )
		{
			#ifdef _SOCKET_DEBUG
			jsTrace("Error in seting socket back to nonBlocking");
			#endif
			return false;
		}
		m_blocking = block;
	}

	//Convert, the internet Info to an address. Note, we only take the first
	//entry for now. We may want to optimize this in future?
	in_addr *iaddr = (in_addr *)&addr;
	sockaddr_in saddr;

	fillSockAddr( iaddr, port, saddr );

	setOk ( _doConnect( (sockaddr*) &saddr, timeout ) );

	#ifdef _SOCKET_DEBUG
	jsTrace("returning from jsTcpImpl::connect with status %d", ok() );
	#endif

    	return ok();
}

/*****************************************************************************

 Description: This code tries to close the socket. If not, we just ignore it

 Returns: true if created it ok and handle is valid
 		  false if error creating thread

******************************************************************************/
bool jsTCPImpl::close( 
		bool //sd		// shutdown, too? not used any more
		)
{
	resetErr();

	if ( m_sockFD == INVSOC )	//allow multiple calls
		return true;

	//don't really care about return code
	if ( ::closesocket( m_sockFD ) != NO_ERROR )
		m_lastErr = lastError();

	/*
	if (sd)
	{
	   ::shutdown(m_sockFD,2);
	  	m_lastErr = lastError();
	}
	*/

	jsTrace( "closing down socket '%d'", m_sockFD);

	m_sockFD = INVSOC;
	m_eof = false;

	setOk( false );

	return true;
}

/*****************************************************************************

 Description: accept a new socket on m_sockFD.

 Returns: true if ok
 		  false if error 

******************************************************************************/
SOCKFD jsTCPImpl::accept()
{
	resetErr();

	if ( !ok() )
		return INVSOC;

	//****NOTE****On Solaris - accept does not work, when multiple threads
	//are blocking on it at the same time. This is a bug. In the meantime, until
	//it is fixed, we get around it by serializing access to the accept call.
	SOCKFD fd;
	{
		#if defined(_UNIX) && defined(_SOLARIS)
			jsFastLock mylock( m_cs );
		#endif

		//On Unix systems. accept may be interrupted by a signal.If that
		//is the case, resubmit the call until we get a success or a failure
		do 
		{
			fd = ::accept( m_sockFD, 0, 0 );
			if ( fd == INVSOC )
				m_lastErr = lastError();

		} while ( m_lastErr == EINTR ); 
	}

	if ( fd == INVSOC )
		jsTrace( "Accept failed on socket '%ld'",  m_sockFD );
	else
		jsTrace( "Accept succeeded with new socket '%ld'", fd );

	return fd;
}

/*****************************************************************************

 Description: Bind on a port for any address

 Returns: true if bound
 		  false if error

******************************************************************************/
bool jsTCPImpl::bind( UINT port )
{
	resetErr();

	if ( m_sockFD != INVSOC || !m_loaded || m_eof  )
		return false;

	if ( !socket() )
	{
		return false;
	}

	//Note:To make this a multiple domain thing, use a specific address instead of 
	//INADDR_ANY
	sockaddr_in saddr;
	saddr.sin_family = AF_INET;
	saddr.sin_addr.s_addr = htonl (INADDR_ANY);
	
	USHORT p = (USHORT)port;
	p = htons( (USHORT)port );
	saddr.sin_port = p;
		
	int st = ::bind( m_sockFD, ( sockaddr*) &saddr, sizeof(saddr));
	if ( st != 0 )
		m_lastErr = lastError();

	if ( st < 0 )
	{
		jsTrace( "bind error = '%d'", lastError() );
		return false;
	}
	else
	{
		//fill in some details like, what is our address
		dnsName();
		return true;
	}
}

/*****************************************************************************

 Description: listen with a backlog of 255 by default

 Returns: true if ok
 		  false if error

******************************************************************************/
bool jsTCPImpl::listen( 
		ULONG backlog		// = 255
		)
{
	resetErr();

	if( !ok() ) 
		return false;

	int st = ::listen ( m_sockFD, backlog );
	if ( st != 0 )
		m_lastErr = lastError();

	return  st == 0;
}

/*****************************************************************************

 Description: read data from port
 by default we use blocking io for reads

 Returns: true if ok
 		  false if error

******************************************************************************/
bool jsTCPImpl::receive( 
	BYTE *data,		// where to put it
	ULONG nbytes,	// len of data
	ULONG& len,		// len read
	ULONG timeout	// in secs
	)
{
	resetErr();

	if( !ok() ) 
	{
		len = 0;
		return false;
	}
	
	#ifdef _SOCKET_DEBUG
	jsTrace("jsTcpImpl::receive with timeout = %d secs", timeout );
	#endif

	//if timeout is specified, then call select with the read file descriptor
	//until we are done
	if ( timeout )
	{
		bool st = _sockwait ( m_sockFD, timeout, SEL_READ );  

		#ifdef _SOCKET_DEBUG
	        jsTrace( "jsTcpImpl::_sockwait in receive returned a status of %d", st );
		#endif
	
		if ( !st )
			return st;
	}

	//On Unix systems. recv may be interrupted by a signal.If that
	//is the case, resubmit the call until we get a success or a failure
	int nread = 0;

	do 
	{
		nread = 0;
		nread = ::recv( m_sockFD,  (char*)data, nbytes, 0);

		if ( nread < 0 )
			m_lastErr = lastError();

	} while ( m_lastErr == EINTR  );

	if( nread < 0 ) 
	{
		if ( m_blocking || ( !m_blocking && !isProgress( m_lastErr ) ) )
			jsTrace( "Socket(%d) recv error '%d'", m_sockFD, lastError());
		return false;
	}
	else if ( nread == 0 )	 //end of file
	{
		m_eof = true;
		return false;
	}

	#ifdef _SOCKET_DEBUG
	jsTrace( "jsTcpImpl::receive Socket(%d) Asked to read '%d' bytes. Read '%d' bytes", m_sockFD, nbytes, nread);
	#endif

	len = nread;

	return true;
}


/*****************************************************************************

 Description: send data out on the port
 by default we use blocking io for writes

 Returns: true if ok
 		  false if error

******************************************************************************/
bool  jsTCPImpl::send( 
		const BYTE * data,		// to send
		ULONG nbytes,		// len to set
		ULONG& len,			// len sent
		ULONG timeout		// sec
		)
{
	resetErr();

	if( !ok() ) 
	{
		len = 0;
		return false;
	}

	#ifdef _SOCKET_DEBUG
	jsTrace("jsTcpImpl::write with timeout = %d secs", timeout );
	#endif

	//if timeout is specified, then call select with the read file descriptor
	//until we are done
	if ( timeout )
	{
		bool st = _sockwait ( m_sockFD, timeout, SEL_WRITE );  
		#ifdef _SOCKET_DEBUG
		jsTrace( "jsTcpImpl::_sockwait in send returned %d", st );
		#endif

		if ( !st )
			return st;
	}


	int nwritten;
	len = 0;

	//On Unix systems. accept may be interrupted by a signal.If that
	//is the case, resubmit the call until we get a success or a failure
	do 
	{
		nwritten = ::send( m_sockFD, (char*)data, nbytes, 0 ); 

		if ( nwritten < 0 )
			m_lastErr = lastError();

	} while ( m_lastErr == EINTR );

	if( nwritten < 0 ) 
	{
		len = 0;
		if ( m_blocking || ( !m_blocking && !isProgress( m_lastErr ) ) )
			jsTrace( "Socket(%d) send error = '%d'", m_sockFD, lastError());
		return false;
	}

	len = nwritten;
	#ifdef _SOCKET_DEBUG
	jsTrace( "jsTcpImpl::send Socket(%d) Asked to write '%d' bytes, actually wrote '%d' bytes", m_sockFD, nbytes, nwritten);
	#endif

	return (ULONG)nwritten == nbytes;   //This should be a error if blocking
}

/*****************************************************************************

 Description: try binding over serveral ports
 supply a start and end port and we will give you a first bind port

 Returns: true if bound to a port
 		  false if error

******************************************************************************/
bool jsTCPImpl::bindUntilSuccess( UINT sportno, UINT eportno )
{
	for ( UINT i = sportno; i <= eportno; i++)
	{
		if ( bind ( i )  )
			return true;		
	}
	return false;
}

/*****************************************************************************

 Description: set socket to non blocking

 Returns: true if ok
 		  false if error

******************************************************************************/
bool jsTCPImpl::setNonBlocking( bool set )
{
	resetErr();

	if ( m_blocking == !set )	//if already set, don't do anything
		return true;

	unsigned long y = set != 0;

	int s = ::ioctlsocket( m_sockFD, FIONBIO, &y);

	if ( s < 0 )
	{
		m_lastErr = lastError();

		jsTrace( "Socket(%d) IOCTL error '%d'", m_sockFD, lastError());
		return false;
	}
	else
	{
		m_blocking = !set;
		return true;
	}
}

/*****************************************************************************

 Description: set the receive buffer size in bytes

 Returns: true if ok
 		  false if error

******************************************************************************/
bool jsTCPImpl::setRecvBufSize( ULONG size )
{
	resetErr();

	int s = ::setsockopt( m_sockFD, SOL_SOCKET, SO_RCVBUF, (char *) &size, sizeof(size));

	if ( s < 0 )
	{
		m_lastErr = lastError();
		jsTrace( "Socket(%d) setsockopt SO_RCVBUF error '%d'", m_sockFD, lastError());
		return false;
	}
	else
		return true;		
}

/*****************************************************************************

 Description: sets the send buffer size

 Returns: true if ok
 		  false if error

******************************************************************************/
bool jsTCPImpl::setSendBufSize( ULONG size )
{
	resetErr();

	int s = ::setsockopt( m_sockFD, SOL_SOCKET, SO_SNDBUF, (char *) &size, sizeof(size));

	if ( s < 0 )
	{
		m_lastErr = lastError();
		jsTrace( "Socket(%d) setsockopt SO_SNDBUF error '%d'", m_sockFD, lastError());
		return false;
	}
	else
		return true;		
}


/*****************************************************************************

 Description: sets the linger option on or off

 Returns: true if ok
 		  false if error

******************************************************************************/
bool jsTCPImpl::setLinger( bool set )
{
	resetErr();

	int s;
	USHORT on = (USHORT)( set ? 1 : 0 );
	struct linger l = { on, 1 }; 

	s = ::setsockopt( m_sockFD, SOL_SOCKET, SO_LINGER, (char *) &l, sizeof(struct linger));

	if ( s < 0 )
	{
		m_lastErr = lastError();
		jsTrace( "Socket(%d) setsockopt SO_LINGER error '%d'", m_sockFD, lastError());
		return false;
	}
	else
		return true;	
}

/*****************************************************************************

 Description: conststruct the TCP/IP implementation

 Implementation: 

 Returns: true if ok
 		  false if error

******************************************************************************/
bool jsTCPImpl::setKeepAlive( bool set )
{
	resetErr();

	int s;
	int off = ( set ? 1 : 0 );
	s = ::setsockopt( m_sockFD, SOL_SOCKET, SO_KEEPALIVE, (char *) &off, sizeof(off));

	if ( s < 0 )
	{
		m_lastErr = lastError();
		jsTrace( "Socket(%d) setsockopt SO_KEEPALIVE error '%d'", m_sockFD, lastError());
		return false;
	}
	else
		return true;		
}


/*****************************************************************************

 Description: sets the out of band option on or off

 Implementation: 

 Returns: true if ok
 		  false if error

******************************************************************************/
bool jsTCPImpl::setOOB( bool set )
{
	resetErr();

	int off = (	set ? 1 : 0 );
	int s = ::setsockopt( m_sockFD, SOL_SOCKET, SO_OOBINLINE, (char *) &off, sizeof(off));

	if ( s < 0 )
	{
		m_lastErr = lastError();
		jsTrace( "Socket(%d) setsockopt SO_OOB error '%d'", m_sockFD, lastError());
		return false;
	}
	else
		return true;
}


/*****************************************************************************

 Description: conststruct the TCP/IP implementation

 Implementation: 

 Returns: true if ok
 		  false if error

******************************************************************************/
bool jsTCPImpl::setReuse( bool set )
{
	resetErr();

	int s;
	int on = ( set ? 1 : 0 );
	s = ::setsockopt( m_sockFD, SOL_SOCKET, SO_REUSEADDR, (char *) &on, sizeof(on));

	if ( s < 0 )
	{
		m_lastErr = lastError();
		jsTrace( "Socket(%d) setsockopt SO_REUSEADDR error", m_sockFD, lastError());
		return false;
	}
	else
		return true;		
}


/*****************************************************************************

 Description: call this function, to load all the host specific info. 
	By this, we mean the dns part

 Returns: true if ok
 		  false if error

******************************************************************************/
bool jsTCPImpl::_hostDetails( const char* name, _InetInfo& ii )
{
	resetErr();

	jsVerifyReturn( name != 0, false );

	hostent* he = 0;

	//if a ascii name, do gethostbyname
	if ( !is_inet_addr( name ) )
	{
		#if	defined(_NT) || defined (_UNIXWARE)

			he = ::gethostbyname( _U( name ) );

		#elif defined(_SOLARIS)

			hostent mhe;
			char buf[MAXGETHOSTSTRUCT+1];
			int errstat;
			he = ::gethostbyname_r( _U(name), &mhe, buf, MAXGETHOSTSTRUCT, &errstat);
			if ( he )
				he = &mhe;
		#endif

		if  (!he )
		{
			m_lastErr = lastError();
			jsTrace( "gethostbyname failed");
			return false;
		}
		return _copyHostent( he, ii );
	}
	else  //do gethostbyaddr
		return _copyHostent1( name, ii); 
}

/*****************************************************************************

 Description: get the local address from the socket

 Implementation: caller responsible for memory deallocation

 Returns: true if ok
 		  false if error

******************************************************************************/
sockaddr_in* jsTCPImpl::localaddr( SOCKFD sock )  
{
	resetErr();

	sockaddr_in *sin = new sockaddr_in;
	if ( !sin )
		return 0;

	int len = sizeof (*sin);
	sin->sin_addr.s_addr = htonl(INADDR_ANY);

	int st = ::getsockname( sock, ( sockaddr*) sin, &len);

	if ( st < 0 )
	{
		m_lastErr = lastError();
		delete sin;
		sin = 0;
	}

	return sin;
}

/*****************************************************************************

 Description: get the peer name

 Implementation: caller responsible for memory deallocation

 Returns: true if ok
 		  false if error

******************************************************************************/
sockaddr_in* jsTCPImpl::peeraddr( SOCKFD sock )
{
	resetErr();

	sockaddr_in *sin = new sockaddr_in;
	jsVerifyReturn(sin != 0, 0);

	int len = sizeof (*sin);
	int st = ::getpeername( sock, ( sockaddr*) sin, &len);

	if ( st < 0)
	{
		m_lastErr = lastError();
		delete sin;
		sin = 0;
	}

	return sin;
}

/*****************************************************************************

 Description: do the actual connection here

 Implementation: ****CAUTION*** On connection errors, 
 the socket is left in "non-blocking" mode
	Since there is not much you can do with it anyway.

 Returns: true if ok
 		  false if error

******************************************************************************/
bool jsTCPImpl::_doConnect(  sockaddr* saddr, ULONG timeout  )
{  
	/****NOTE****
	Implementing alarms on Threads on Unix is not portable nor advisable, since
	that is going to be changed by Posix in future. On Nt it is difficult, with
	call backs to keep track on which thread. 
	To get around that, I have used select in thread specific context, 
	to implement timeout functionality.
	*************/

	ULONG a = INFINITE_WAIT; 
	resetErr();

	#ifdef _SOCKET_DEBUG
	jsTrace("connect timeout of %ld secs\n", timeout );
	#endif

	//if no timeout, just leave the socket alone. The caller decides on mode
	if (  timeout && timeout != INFINITE_WAIT )
	{
		if ( !setNonBlocking ( true ) )
				return false;
	}						

	//On Unix systems. connect may be interrupted by a signal.If that
	//is the case, resubmit the call until we get a success or a failure
	int s;
	do 
	{
		s = ::connect( m_sockFD, saddr, sizeof(  sockaddr ) );
		
		if ( s != 0 )
			m_lastErr = lastError();
		#ifdef _SOCKET_DEBUG
		jsTrace("::connect returned %ld lasterr %d\n", s, m_lastErr);
		#endif

	} while ( m_lastErr == EINTR );

	//if we have a legitimate non-blocking error, wait for prescribed 
	//time interval, until we are done or timed out
	bool st = false;
	if ( timeout  )
	{
		if ( s < 0 )
		{
			if ( isProgress( m_lastErr ) )
			{
				#ifdef _SOCKET_DEBUG
				jsTrace( "connect socket in progress\n");
				#endif

				st = _sockwait( m_sockFD, timeout, SEL_CONNECT );
				#ifdef _SOCKET_DEBUG
				jsTrace( "_sockwait for connect returned %d\n", st );
				#endif
			}
			else
				return false;	 //don't bother to reset the socket, it's a goner!
		}

		//**NOTE** Since m_lastErr is used to keep track of last error, to keep track
		//of timeouts, we reset the last error to ETIMEDOUT if the resetting back to
		//non blocking goes okay.
		int laste = m_lastErr;

		//reset to blocking
		if ( !setNonBlocking( false ) )
			return false;

		if ( laste == ETIMEDOUT )  //reset the last error number back
			m_lastErr = ETIMEDOUT;
	}
	else
		st = ( (s == 0) ?  true : false );

	#ifdef _SOCKET_DEBUG
	jsTrace("Socket connect returned %d \n", st );
	#endif

	return st;
}

/*****************************************************************************

 Description: wait on a socket, via _select

 Returns: true if ok
 		  false if error

******************************************************************************/
bool jsTCPImpl::_sockwait( SOCKFD fd, ULONG timeoutSec, SelType op )
{
	resetErr();

	if ( fd == INVSOC )
		return false;

	struct timeval mytime = { timeoutSec , 0 };
	fd_set myfd;
	FD_ZERO( &myfd );
	FD_SET( fd, &myfd );

	bool st = false;

	if ( op == SEL_CONNECT )	//we are waiting on a connect call, wait for a write ready
		st = _select( fd, 0, &myfd, 0, &mytime );
	else if ( op == SEL_READ )
		st = _select( fd, &myfd, 0, 0, &mytime );
	else if ( op == SEL_WRITE )
		st = _select ( fd, 0, &myfd, 0, &mytime );
	else
		jsTrace(  "Bad select operation type passed in '%d'", (int)op );

	return st;
}

/*****************************************************************************

 Description: copy hostent from inetinfo

 Implementation: need to copy over the hostent structure, since it gets modified
 on ::gethostbyname call on a per thread basis.


 Returns: true if ok
 		  false if error

******************************************************************************/
bool jsTCPImpl::_copyHostent(  
		hostent* he,	// copy from 
		_InetInfo& ii,	// copy to
		bool expand  
		)
{
	if ( !he )
		return false;

	//copy the dns host name first
	ii.m_dnsName = _U(he->h_name);
	jsTrace( "Fully qualified dns name for host '%s'", ii.m_dnsName.c_str() );
	
	if ( !expand )  //we don't need to expand other stuff
		return true;
		 
	//next do the aliases
	char *ptr;
	std::string temp;

	if ( *(he->h_aliases) )
		jsTrace("Aliases for the host name");

	while ( ( ptr = *(he->h_aliases) )  != 0 )
	{
		temp = _U(ptr);
		jsTrace( "%s", temp.c_str() );
		ii.m_hostAlias.push_back( temp );
		he->h_aliases++;			
	}

	//next copy the addresses ( the value not the address of struct in_addr )
	in_addr *ptr1;
	ULONG addr;
	char **aptr = he->h_addr_list;
	jsTrace("Dotted Internet address" );
	while ( (ptr1= ( in_addr*) *aptr) != 0 )
	{
		addr = (*ptr1).s_addr;
		ii.m_hostAddr.push_back ( addr );
		temp = _U(::inet_ntoa( *ptr1 ));
		jsTrace( "%s", temp.c_str() );
		aptr++;
	}

	return true;
}


/*****************************************************************************

 Description: copy hostent info givve addr

 Implementation: 

 Returns: true if ok
 		  false if error

******************************************************************************/
bool jsTCPImpl::_copyHostent1( 
		const char* numericAddr,	// from
		_InetInfo& ii,			// to
		bool expand )
{
	resetErr();

	//record the address 
	ULONG connectAddr = inet_addr(_U(numericAddr) );
	in_addr* iaddr = ( in_addr*) &connectAddr;	 

	//get the dns name. Try gethostbyaddr. If that fails, record the internet
	//address as the dns name. This is needed to avoid sites that don't have a 
	//reverse dns lookup.
	hostent* he =0;

	#if defined(_NT) || defined(_UNIXWARE)

	he = ::gethostbyaddr( (const char*)iaddr, sizeof(in_addr), AF_INET );

	#elif defined(_SOLARIS)

		hostent mhe;
		char buf[MAXGETHOSTSTRUCT+1];
		int estat;
		he = ::gethostbyaddr_r( (const char*)iaddr, sizeof(in_addr), AF_INET, &mhe, buf, MAXGETHOSTSTRUCT, &estat);
		if ( !he )
			he = &mhe;
	#endif 
		
	if ( !he )  //just record address as dns name
	{
		m_lastErr = lastError();
		ii.m_hostAddr.push_back ( (ULONG) connectAddr );
		ii.m_dnsName = numericAddr;
		jsTrace( "Numeric name for host '%s'", ii.m_dnsName.c_str() ); 
		return true;
	}
	else
		return _copyHostent( he, ii, expand );
}


/*****************************************************************************

 Description: get the host name

 Returns: the host name

******************************************************************************/
std::string jsTCPImpl::hostName() const
{
	std::string name;
	char n[MAXHOSTNAMELEN+1];
	if ( ::gethostname( n, MAXHOSTNAMELEN-1 ) == 0)
			name = _U(n);
	return name;
}



/*****************************************************************************

 Description: get the domain name

 Returns: doamin name

******************************************************************************/
std::string jsTCPImpl::domainName()
{

	std::string name;
    std::string host = dnsName();

	const char* h = host.c_str();
    const char* domain;

    if ( h && *h) 
    {
		if ((domain = strchr(host.c_str(), '.')) !=  0)
	    	name = _U(++domain);
    } 
	return name;	
}

/*****************************************************************************

 Description: get the local port from our fd

 Returns: port, -1 if error

******************************************************************************/
LONG jsTCPImpl::localport()
{

	int p = -1;
	sockaddr_in *sin = localaddr( m_sockFD );

	if ( sin == 0 )
		return -1;

	if (sin->sin_family == AF_INET) 
		p = ntohs (sin->sin_port);

	delete sin; 
	sin = 0;

	return p;
}

/*****************************************************************************

 Description: get the port from the peer address

 Returns: peer port, -1 if error

******************************************************************************/
LONG jsTCPImpl::peerport() 
{

	int p = -1;
	 sockaddr_in *sin = peeraddr( m_sockFD );

	if (!sin )
	{
		DELETE_AND_ZERO(sin);
		return -1;
	} 

	if (sin->sin_family == AF_INET) 
		p = ntohs (sin->sin_port);

	delete sin; 
	sin = 0;

	return p;
}

/*****************************************************************************

 Description: load the info into parms from the hostname supplied

 Implementation: 

 Returns: true if ok
 		  false if error

******************************************************************************/
bool jsTCPImpl::loadInetInfo ( 
		const std::string &hname, 
		std::string& dnsName, 
		jsStringArray& hostAlias, 
		vector<ULONG>& hostAddr 
		) 
{
	_InetInfo ii;
	if ( !m_loadedHosts )
	{
		std::string s(hname.length() ? hname : hostName());

		if ( !_hostDetails( s.c_str(), ii )  )
			return false;

		m_loadedHosts = true;
	}

	//copy over the inetInfo structure
	dnsName = ii.m_dnsName;
	for (size_t i=0; i< ii.m_hostAlias.size(); i++ )
	{
		hostAlias.push_back( ii.m_hostAlias[i] );
	}
	for (size_t i1=0; i1< ii.m_hostAddr.size(); i1++ )
	{
		hostAddr.push_back( ii.m_hostAddr[i1] );
	}
	return true;
}

/*****************************************************************************

 Description: get highest fd

 Returns: highest fd

******************************************************************************/
LONG jsTCPImpl::getMaxFd() const
{
	#ifdef FD_SETSIZE
		return FD_SETSIZE;
	#else
		#ifdef _UNIX
			return ::getdtablesize();
		#else
			return 2048;  //think, this will be enough?
		#endif
	#endif
}

#ifdef _MSC_VER
// for template sign/unsign mismatch for some reason this removes them
#pragma warning (disable : 4018 4786 ) 
#endif
 
