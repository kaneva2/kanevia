/******************************************************************************
 Name: jsTcpImpl.h

 Description: socket implementation class for TCP/IP definition

 Copyright (C) 1996, 2004 Jim Wallace

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 Notes: 	An implementation class for TCP/IP. 
			For convenience, this includes, all code
			needed maybe by most of the derived classes like Connection, ServerConnection, 
			InetInfo.... 
			This makes it easier to port to any other platform. ie. we do it only here.
			This class, should never be accessed directly.
			Always come in thru the derived object, if you should need to do anything
			at all with TCP!

  CAUTION:	This class is ***NOT ThreadSafe*******.
			The caller is responsible for thread safeness.
			All Non-blocking calls must have a timeout of 0.

******************************************************************************/
#ifndef _jsTCPImpl_h_
#define _jsTCPImpl_h_

#include "jsNet.h"
#include "jsSocket.h"
	
#ifdef _MSC_VER
#pragma warning (disable:4251) // not exporting private data
#endif

class JSEXPORT jsTCPImpl : public jsSocket
{
public:
	
	jsTCPImpl();
	jsTCPImpl ( SOCKFD s );			

	virtual ~jsTCPImpl();

	virtual void duplicate( const jsSocket & from );

	//define basic functions for the tcp class. 
	virtual bool socket();

	//timeoutSec = 0, means infinite access time. Specify timeoutSec in seconds
	virtual bool  connect( ULONG addr, UINT port, bool block = true, ULONG timeoutSec = 0 );

	virtual SOCKFD accept();
	
	virtual bool send( const BYTE * data, ULONG len, ULONG& written, ULONG timeoutSec = 0 );
  
	virtual bool receive( BYTE * data, ULONG maxlen, ULONG& read, ULONG timeoutSec = 0  );

	virtual bool listen ( ULONG backlog =  255 );

	virtual bool bind( UINT port = 0 );

	virtual bool bindUntilSuccess( UINT startPortNo, UINT endPortNo );	
	
	virtual bool close( bool shutdown = true );

	//common accessor funtions
	virtual SOCKFD getSock() const; 

	virtual LONG getMaxFd() const;

	virtual std::string domainName();
	
	virtual std::string hostName() const;

	virtual std::string dnsName( const std::string &name = "" );

	virtual bool loadInetInfo ( const std::string &hname, std::string& dnsName, jsStringArray& hostAlias, vector<ULONG>& hostAddr );

	virtual LONG localport();

	virtual LONG peerport();

	virtual std::string address();

	virtual ULONG peeraddr();

	virtual LONG lastError() const;

	virtual bool isBlocking() const { return m_blocking; }

	//operations on socket mode
	virtual bool setNonBlocking( bool set = true );

	virtual bool setLinger( bool set = true );

	virtual bool setReuse( bool set = true );

	virtual bool setRecvBufSize( ULONG size );

	virtual bool setSendBufSize( ULONG size );

	virtual bool setOOB( bool set = true );

	virtual bool setKeepAlive( bool set = true );

	virtual bool setSocket( SOCKFD sock );

	virtual bool eof()  { return m_eof; }

	virtual bool nbcode() const	 { return isProgress( m_lastErr ); }

	virtual bool timeout() const { return (m_lastErr == ETIMEDOUT) ? true : false; }

protected:

	inline bool isIntr() const;
	inline bool isProgress( ULONG e ) const;


private:
	SOCKFD	m_sockFD;		// the socket file descriptor for this object
	bool	m_blocking;		// are we blocking ?
	bool 	m_loadedHosts;	// loaded dns names
   	bool    m_eof;			// have we reached eof
	ULONG     m_lastErr;		// last error

	//a small class to be used for storing the results of parsing all internet 
	//related info 
	class _InetInfo
	{
	public:
		std::string		m_dnsName;
		jsStringArray	m_hostAlias;
		vector<ULONG>	m_hostAddr;	
	};
	_InetInfo m_inetInfo;

	static inline void fillSockAddr( const in_addr* userAddr, UINT port, sockaddr_in& saddr );

	bool _doConnect( sockaddr* saddr, ULONG timeoutSec = 0 );
	bool _getdnsName( std::string hname, _InetInfo& ii );
	bool _hostDetails( const char* name, _InetInfo& ii ); // single byte host name

	bool _copyHostent( hostent* he, _InetInfo& ii, bool expand = true );
	bool _copyHostent1( const char* numericAddr, _InetInfo& ii, bool expand = true );
    sockaddr_in* localaddr( SOCKFD sock );
    sockaddr_in* peeraddr( SOCKFD sock );

	//op = true means wait for write notification. Op = false means wait
	//for read notification.
	enum  SelType
	{
		SEL_CONNECT =0,
		SEL_READ = 1,
		SEL_WRITE = 2
	};

	bool _sockwait( SOCKFD fd, ULONG timeoutSec, SelType op );

	inline bool _select( SOCKFD fd, fd_set* rfd, fd_set* wfd, fd_set* efd, struct timeval* timeoutSec);

	inline void resetErr() { m_lastErr = NO_ERROR; }

	static jsFastSync m_cs;   //used for loading the dll and serialization

protected:
	bool loadTcp();
	static void releaseTcp();

private:
	static bool m_loaded;    	//loaded winsock DLL for NT
};

#ifdef _MSC_VER
#pragma warning (default:4251) // not exporting private data
#endif







/*********************************************************************
				Definition of inlines
**********************************************************************/

//be extremely careful while using this function
inline bool jsTCPImpl::setSocket( SOCKFD sock )
{
	m_sockFD = sock;
	m_loadedHosts = false;
	m_blocking = true;
	m_eof = false;
	m_lastErr = 0;
	setOk ( true );
	return true;
}

inline SOCKFD jsTCPImpl::getSock() const
{
	return m_sockFD;
}

inline bool jsTCPImpl::isProgress( ULONG e ) const
{
	switch( e )
	{
		case EINPROGRESS:
		case EWOULDBLOCK:
			return true;

		default:
			return false;
	} 

}

inline LONG jsTCPImpl::lastError() const
{
	int t = socketerrno;
	return t;
}

inline bool jsTCPImpl::_select( SOCKFD fd, fd_set* rfd, fd_set* wfd, fd_set* efd, struct timeval* timeoutSec)
{
	int s;
	do 
	{
		s = ::select ( fd+1, rfd, wfd, efd, timeoutSec );
		m_lastErr = lastError();
	} while ( s < 0 && m_lastErr == EINTR );

	if ( s < 0 )
	{
		#ifdef _SOCKET_DBG2
		jsTrace("jsTcpImpl::_select returned error");
		#endif	
		return false;
	}
	else if ( s == 1 )
	{
		if ( 
			 ( rfd && FD_ISSET( fd, rfd ) ) ||
			 ( wfd && FD_ISSET( fd, wfd ) ) ||
			 ( efd && FD_ISSET( fd, efd ) )
			)
		{
			#ifdef _SOCKET_DBG2
			jsTrace("jsTcpImpl::_select returned success ");
			#endif	
			return true;
		}
		else
		{
		#ifdef _SOCKET_DBG2
		jsTrace("jsTcpImpl::_select returned success but not on our file descriptors");
		#endif	
		return false;
		}
	}
	else if ( s == 0 )
	{
		#ifdef _SOCKET_DBG2
		jsTrace("jsTcpImpl::_select returned Timeout");
		#endif	
		m_lastErr = ETIMEDOUT;
		return false;
	}
	else
	{	jsAssert(false);
		return false;
	}
}

inline bool jsTCPImpl::isIntr()	const
{
	if ( socketerrno == EINTR )
		return true;
	else
		return false;
}

inline std::string jsTCPImpl::dnsName( const std::string &name /*=""*/ )  
{
	std::string n = name;
	if ( !m_loadedHosts )
	{
		if ( n.length() == 0 )
			n = hostName();

		_hostDetails( n.c_str(), m_inetInfo );
		m_loadedHosts = true;
	}
	return m_inetInfo.m_dnsName;
}


// static fn to fill address
inline void jsTCPImpl::fillSockAddr( const in_addr* userAddr, UINT port, sockaddr_in& saddr )
{
	if ( userAddr )
	{
		::memset(&saddr, 0, sizeof(saddr));
		saddr.sin_family = AF_INET;
		saddr.sin_port = htons((USHORT)port);
		::memcpy( &saddr.sin_addr, userAddr, sizeof( in_addr) );
	}
	else
	{
		jsTrace("[fillSockAddr] in_addr is passed in as null");
		jsAssert( false );
	}
}


inline ULONG jsTCPImpl::peeraddr() 
{
	sockaddr_in * saddr = peeraddr( m_sockFD );
	if ( !saddr )
		return 0;
	else
	{
		ULONG addr = (saddr->sin_addr).s_addr;
		DELETE_AND_ZERO(saddr);
		return addr;
	}
} 

inline std::string jsTCPImpl::address() 
{
	sockaddr_in *sin = localaddr( m_sockFD );

	if ( sin == 0 )
		return "";

	std::string ret;

	if (sin->sin_family == AF_INET) 
		ret = jsSocket::inet_ntoa(sin->sin_addr.S_un.S_addr);

	delete sin; 
	sin = 0;

	return ret;
/*
	if ( !m_loadedHosts )
	{
		_hostDetails( hostName(), m_inetInfo );
		m_loadedHosts = true;
	}

 	if ( m_inetInfo.m_hostAddr.size() ) 
		return jsSocket::inet_ntoa( (ULONG) m_inetInfo.m_hostAddr[0] ); 
 	else 
 		return _T(""); 
*/
}


#endif



 
 
 
 
 
