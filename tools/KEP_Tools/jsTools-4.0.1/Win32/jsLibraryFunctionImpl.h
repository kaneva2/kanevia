/******************************************************************************
 Name: jsLibraryFunctionImpl.h

 Description: gets a function from a library

 Copyright (C) 1999, 2004 Jim Wallace

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 History:
 1/28/99		jmw created

 Notes: 

******************************************************************************/
#ifndef __JSLIBRARYFUNCTIONIMPL_H__
#define __JSLIBRARYFUNCTIONIMPL_H__

#include <string>
using namespace std;

#include "jsErr.h"
#include "jsLibraryFunction.h"

/**********************************************************
-CLASS
	jsLibraryFunctionImpl
	
	get a function from a lib

DESCRIPTION

***********************************************************/
class jsLibraryFunctionImpl 
{
public:
	//---------------------------------------------------------
	// constructor
	// 
	// [in] lib name of lib including extension, without dir
	//
	jsLibraryFunctionImpl( const char* lib, bool freeOnDelete ) : m_lib( 0 ), m_dllName( lib ), m_lastError(NO_ERROR), m_freeOnDelete( freeOnDelete ) {}

	//---------------------------------------------------------
	// getFnProc
	// 
	// [in] procName name of procedure in lib.  Always single byte
	//
	// [Returns]
	//    the fn ptr or null if error.  In that case lastError() is valid
	JSLIBFUNCTION getFunction( const char* procName ); // always single byte


	//---------------------------------------------------------
	// [Returns]
	//    error if getFunction returns 0
	const jsErr &lastError() const { return m_lastError; }

	//---------------------------------------------------------
	// [Returns]
	//    the name of the library
	const char* libraryName() const { return m_dllName.c_str(); }

	//---------------------------------------------------------
	// frees library
	~jsLibraryFunctionImpl() { if ( m_lib && m_freeOnDelete )  ::FreeLibrary( m_lib ); }

private:
	HINSTANCE				m_lib;
	string					m_dllName;
	jsErr					m_lastError;
	bool					m_freeOnDelete;
};

#endif //__JSLIBRARYFUNCTIONIMPL_H__
