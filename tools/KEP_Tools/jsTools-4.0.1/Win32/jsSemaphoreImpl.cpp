/******************************************************************************
 Name: jsSemphoreImpl.cpp

 Description: Win32 implementationof the synchronization classes
 Not alot of comments here since pretty thin wrapper over the Win32 API

 Copyright (C) 1996, 2004 Jim Wallace

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 History:
 11/20/96	jmw created

 Notes: 

******************************************************************************/
#include "js.h"
#include "jsSemaphoreImpl.h"

jsSemaphoreImpl::jsSemaphoreImpl( 
	ULONG initCount, //= 0 
	ULONG max       //=256
	)
{
	// max of 256 enough?
	m_handle = ::CreateSemaphore(NULL,initCount,max,NULL);
	setOk( m_handle != 0 );
}

jsSemaphoreImpl::~jsSemaphoreImpl()
{
	::CloseHandle(m_handle);
}

bool jsSemaphoreImpl::increment(
	UINT count 	// = 1
	)
{
	LONG c;
	return ::ReleaseSemaphore(m_handle,count,&c) == TRUE;
}

jsWaitRet jsSemaphoreImpl::waitAndDecrement( ULONG timeoutMS /*= INFINITE*/, ULONG /*unixPollMS = 100*/ )
{
	DWORD timeout = timeoutMS;
	DWORD ret = ::WaitForSingleObject(m_handle,timeout);
	
	return jsMapWaitRet( ret );
}

jsWaitRet jsSemaphoreImpl::tryWaitAndDecrement()
{
	DWORD timeout = 0;
	DWORD ret = ::WaitForSingleObject(m_handle,timeout);
	
	return jsMapWaitRet( ret );
}

jsWaitRet jsMapWaitRet( DWORD ret ) 
{
	jsWaitRet wr;
	switch (ret)
	{
		case WAIT_TIMEOUT:
			wr = WR_TIMEOUT;
			break;
		case WAIT_OBJECT_0:
			wr = WR_OK;
			break;
		default:
			wr = WR_ERROR;
			break;
	};		
	return wr;
}


#ifdef _MSC_VER
// for template sign/unsign mismatch for some reason this removes them
#pragma warning (disable : 4018 ) 
#endif
