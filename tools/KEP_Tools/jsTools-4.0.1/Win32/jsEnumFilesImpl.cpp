/******************************************************************************
 Name: jsEnumFilesImpl.cpp

 Description: enumerate files in a directory

 Copyright (C) 1996, 2004 Jim Wallace

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 History:
 1/28/98		jmw created

 Notes: 

******************************************************************************/
#include "js.h"

#include <stdlib.h> // for splitpath

#include "../jsEnumFiles.h"
#include "jsEnumFilesImpl.h"
#include "jsUnicode.h"

///---------------------------------------------------------
// does a file exists
// 
// [in] fname the file or directory name to check
// [out] isDir true if it is a directory.  If null, not set.
//
// [Returns]
//    true if the file or directory exists
bool jsFileExists( const char* fname, bool *isDir )
{
	ULONG attrs = ::GetFileAttributesW( KEP::Utf8ToUtf16(fname).c_str() ); 

	if ( isDir && attrs != 0xffffffff )
	{
		*isDir = (attrs & FILE_ATTRIBUTE_DIRECTORY) == FILE_ATTRIBUTE_DIRECTORY;
	}

	return attrs != 0xffffffff ;
}

///---------------------------------------------------------
// constructor
// 
// [in] m_fileMask the mask of files to look for
//
jsEnumFilesImpl::jsEnumFilesImpl( const wchar_t* _fileMask, bool includeDirs ) : 
	m_recurse( false ), m_hFind( INVALID_HANDLE_VALUE ), 
	m_hasFile(false), 
	m_includeDirs( includeDirs )
{
	m_findData.cFileName[0] = '\0';
	m_curPath[0] = '\0';
	setOk( false );

	jsVerifyReturnVoid( _fileMask != 0 );
	wcsncpy( m_fileMask, _fileMask, _MAX_PATH );

	m_attrs = 0; // mapAttrFromJS( attributes );

	wchar_t dir[_MAX_PATH], name[_MAX_PATH], ext[_MAX_PATH];

	_wsplitpath( _fileMask, m_driveDir, dir, name, ext );
	wcsncat( m_driveDir, dir, wcslen( dir ) - 1  ); // don't append slash

	setOk( true );
}
/*
ULONG jsEnumFilesImpl::mapAttrFromJS( ULONG a )
{
	ULONG ret = 0;

	if ( ((LONG)a & jsEnumFiles::FA_directory) == jsEnumFiles::FA_directory )
		ret |= FILE_ATTRIBUTE_DIRECTORY;

	if ( ((LONG)a & jsEnumFiles::FA_hidden) == jsEnumFiles::FA_hidden)
		ret |= FILE_ATTRIBUTE_HIDDEN;

	if ( ((LONG)a & jsEnumFiles::FA_system) == jsEnumFiles::FA_system)
		ret |= FILE_ATTRIBUTE_SYSTEM;

	if ( ((LONG)a & jsEnumFiles::FA_normal) == jsEnumFiles::FA_normal)
		ret |= FILE_ATTRIBUTE_NORMAL;

	if ( ((LONG)a & jsEnumFiles::FA_readOnly) == jsEnumFiles::FA_readOnly)
		ret |= FILE_ATTRIBUTE_READONLY;

	return ret;
}
*/
ULONG jsEnumFilesImpl::mapAttrToJS( ULONG a)
{
	ULONG ret = 0;

	if ( (a & FILE_ATTRIBUTE_DIRECTORY) == FILE_ATTRIBUTE_DIRECTORY )
		ret |= jsEnumFiles::FA_directory;

	if ( (a & FILE_ATTRIBUTE_HIDDEN) == FILE_ATTRIBUTE_HIDDEN )
		ret |= jsEnumFiles::FA_hidden;

	if ( (a & FILE_ATTRIBUTE_SYSTEM) == FILE_ATTRIBUTE_SYSTEM)
		ret |= jsEnumFiles::FA_system;

	if ( (a & FILE_ATTRIBUTE_NORMAL) == FILE_ATTRIBUTE_NORMAL)
		ret |= jsEnumFiles::FA_normal;

	if ( (a & FILE_ATTRIBUTE_READONLY) == FILE_ATTRIBUTE_READONLY)
		ret |= jsEnumFiles::FA_readOnly;

	return ret;
}


///---------------------------------------------------------
// get the next file.  Only if this returns true will
// the current file name, etc. be valid
//
// [Returns]
//    true if got another file.  
bool jsEnumFilesImpl::next()
{
	m_findData.cFileName[0] = '\0';
	m_curPath[0] = '\0';
	m_hasFile = false;

	if ( m_hFind == INVALID_HANDLE_VALUE )
	{
		m_hFind = ::FindFirstFileW( m_fileMask, &m_findData);

		if( m_hFind == INVALID_HANDLE_VALUE )
		{
			/*no matching files here...*/
			return false;
		}
		else 
		{	
			if ( checkFile() )
			{
				return true; // match!
			}
		}
	}

	/* keep looking */
	while( ::FindNextFileW(m_hFind, &m_findData) )
	{
		if ( checkFile() )
		{
			return true; // match!
		}
	}

	return false;
}

wchar_t * getNonWildCardPostfix(wchar_t * fileMask)
{
	if (fileMask==NULL)
	{
		return NULL;
	}

	wchar_t * p = fileMask+wcslen(fileMask);
	for( ; p>fileMask; p-- )
	{
		wchar_t ch = *(p-1);
		if (ch=='*' || ch=='?')
			break;
	}

	return *p==0 ? NULL : p;
}

bool jsEnumFilesImpl::checkFile()
{
	//
	// FindFirstFile/FindNextFile matches both long file name and 8.3 DOS-style file names. There is
	// a chance that a long file name not match the pattern but the short version does. For example, 
	// test.luac has a 8.3 file name of "TEST~1.LUA" which matches "*.lua". Same situation applies to 
	// file names like "ServerEngine.xml-old" or "ZMenu.dll-deleted". This unexpected behavior have 
	// caused all sorts of confusions.
	//
	// ==Partial workaround==
	// One additional filtering by the postfix can solve most issues we have. It does not handle all 
	// cases but it does take care of the extension pattern "*.ext" well. By comparing all returned
	// file names to a postfix (e.g. ".lua", ".dll" or ".xml") extracted from the wild card pattern, 
	// we can easily rule out all the false positives.
	//
	wchar_t *postfix = getNonWildCardPostfix(m_fileMask);
	if( postfix )
	{
		ptrdiff_t offset = wcslen(m_findData.cFileName) - wcslen(postfix);
		if (offset<0 || _wcsicmp(m_findData.cFileName+offset, postfix)!=0)
		{
			return false;		// file name too short or postfix does not match
		}
	}

	if ( (m_findData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) && !m_includeDirs )
		return false; 
	else
	{
		if ( wcscmp(m_findData.cFileName, L".") == 0 ||
			 wcscmp(m_findData.cFileName, L"..") == 0 ) 
		{
			; // skip these
		}
		else 
		{
			wcscpy( m_curPath, m_driveDir ); 
			wcscat( m_curPath, L"\\" );
			wcscat( m_curPath, m_findData.cFileName );
			m_hasFile = true;
			m_attrs = m_findData.dwFileAttributes;
			return true; // do this one
		}
	}
	return false;
}

jsTime jsEnumFilesImpl::currentLastWriteTime() const
{
	FILETIME ft;

	::FileTimeToLocalFileTime( &m_findData.ftLastWriteTime, &ft );

	SYSTEMTIME st;

	::FileTimeToSystemTime( &ft, &st );

	struct tm tms;

	tms.tm_year = st.wYear - 1900;
	tms.tm_mon = st.wMonth-1;	// 0 - 11 -> 1-12
	tms.tm_mday = st.wDay;  // 1 - 31
	tms.tm_hour = st.wHour; // 0-23
	tms.tm_min = st.wMinute;
	tms.tm_sec = st.wSecond;
	tms.tm_isdst = -1;

	return jsTime( mktime( &tms ) );
}

