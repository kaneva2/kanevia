/******************************************************************************
 Name: jsSemaphoreImpl.h

 Description: Win32 definition of the synchronization implementation classes

 Copyright (C) 1996, 2004 Jim Wallace

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 History:
 11/20/96	jmw created

 Notes: 

******************************************************************************/
#ifndef _jsSemaphoreImpl_h_
#define _jsSemaphoreImpl_h_

#include "jsSyncImpl.h"

#pragma warning ( disable : 4201 4214 4115 4514 )
#include <windows.h>
#pragma warning ( default : 4201 4214 4115 )


/******************************************************************************
 
 Description: counting semaphore

******************************************************************************/
class jsSemaphoreImpl : public jsBase
{
public:
	jsSemaphoreImpl( ULONG initCount = 0, ULONG maxCount = 256 );
	~jsSemaphoreImpl();

	bool increment(UINT count = 1);

	//---------------------------------------------------------
	// wait for the item and decrement the count.  
	//
	// [in] timeoutMS number of milleseconds to wait before timing out
	// [in] unixPollMS for implementations that don't support timeout
	// on a semaphore, how often should this poll, in milliseconds.
	// 
	// [Returns]
	//    WR_OK if got item<br>
	//	  WR_ERROR if error,
	//	  WR_TIMEROUT if a timeout
	jsWaitRet waitAndDecrement( ULONG timeoutMS = INFINITE, ULONG unixPollMS = 100 );

	jsWaitRet tryWaitAndDecrement();

private:
	HANDLE	m_handle;	// semaphore handle
};



#endif
