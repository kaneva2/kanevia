/******************************************************************************
 Name: jsSafeArray.h

 Description: non-MFC copy of the MFC safe array with extensions

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 History:
 11/20/96		jmw created

 Notes: 

******************************************************************************/


#ifndef _SAFEARRAY_H_
#define _SAFEARRAY_H_

class jsSafeArray;

#ifndef _js_h_
#include <assert.h>
#ifdef _DEBUG
#define jsAssert( x ) assert( x )
#define jsVerify( x ) assert( x )
#else
#define jsAssert( x )
#define jsVerify( x ) (x)
#endif
#endif

// Helper for initializing jsSafeArray
void jsSafeArrayInit(jsSafeArray* psa);

#include <oaidl.h> // for SAFEARRAY, etc.

typedef const SAFEARRAY* LPCSAFEARRAY;
typedef const VARIANT * LPCVARIANT;

class jsSafeArray;

class jsSAData
{
public:
	///---------------------------------------------------------
	// [Returns]
	//	void pointer to data
	operator void* () { return m_buffer; }

	///---------------------------------------------------------
	// [Returns]
	//	char pointer to data
	operator char* () { return (char*)m_buffer; }

	///---------------------------------------------------------
	// [Returns]
	//	byte pointer to data
	operator BYTE* () { return (BYTE*)m_buffer; }

	///---------------------------------------------------------
	// [Returns]
	//	void pointer to data
	void *data() { return m_buffer; }

	///---------------------------------------------------------
	// 	unaccessed the data
	~jsSAData();

private:
	void			*m_buffer;
	jsSafeArray		&m_array;

	jsSAData( jsSafeArray &a );

friend class jsSafeArray; // so it can construct this
};


/***********************************************************
CLASS
	jsSafeArray 
	
	wrapper for the SafeArray api

DESCRIPTION
	non-MFC copy of the MFC safe array with extensions

***********************************************************/
class jsSafeArray : public tagVARIANT
{
//Constructors
public:
	//---------------------------------------------------------
	// GROUP: constructors
	//---------------------------------------------------------

	///---------------------------------------------------------
	// default constructor
	// 
	jsSafeArray();

	jsSafeArray(const SAFEARRAY& saSrc, VARTYPE vtSrc);
	jsSafeArray(LPCSAFEARRAY pSrc, VARTYPE vtSrc);
	jsSafeArray(const jsSafeArray& saSrc);
	jsSafeArray(const VARIANT& varSrc);
	jsSafeArray( LPCVARIANT pSrc);


	//---------------------------------------------------------
	// GROUP: operations
	//---------------------------------------------------------
	void clear();
	void attach(VARIANT& varSrc);
	VARIANT detach();

	jsSafeArray& operator=(const jsSafeArray& saSrc);
	jsSafeArray& operator=(const VARIANT& varSrc);
	jsSafeArray& operator=(LPCVARIANT pSrc);

	//---------------------------------------------------------
	// GROUP: comparison operators
	//---------------------------------------------------------
	bool operator==(const SAFEARRAY& saSrc) const;
	bool operator==(LPCSAFEARRAY pSrc) const;
	bool operator==(const jsSafeArray& saSrc) const;
	bool operator==(const VARIANT& varSrc) const;
	bool operator==(LPCVARIANT pSrc) const;

	bool operator!=(const SAFEARRAY& saSrc) const;
	bool operator!=(LPCSAFEARRAY pSrc) const;
	bool operator!=(const jsSafeArray& saSrc) const;
	bool operator!=(const VARIANT& varSrc) const;
	bool operator!=(LPCVARIANT pSrc) const;

	operator LPVARIANT();
	operator LPCVARIANT() const;

	//---------------------------------------------------------
	// GROUP:  One dim array helpers
	//---------------------------------------------------------
	void createOneDim(VARTYPE vtSrc, DWORD dwElements,
		void* pvSrcData = NULL, long nLBound = 0);

	DWORD getOneDimSize();

	void resizeOneDim(DWORD dwElements);

	// Multi dim array helpers
	HRESULT create(VARTYPE vtSrc, DWORD dwDims, DWORD* rgElements);

	// SafeArray wrapper methods
	HRESULT create(VARTYPE vtSrc, DWORD dwDims, SAFEARRAYBOUND* rgsabounds);

	// safer way to access data.  jsSAData does unaccess data when done
	jsSAData data() { return jsSAData(*this); }

	void accessData(void** ppvData);
	void unaccessData();
	void allocData();
	void allocDescriptor(DWORD dwDims);
	void copy(LPSAFEARRAY* ppsa);
	long getLBound(DWORD dwDim = 1);
	long getUBound(DWORD dwDim = 1);
	void getElement(long* rgIndices, void* pvData);
	void ptrOfIndex(long* rgIndices, void** ppvData);
	void putElement(long* rgIndices, void* pvData);
	void redim(SAFEARRAYBOUND* psaboundNew);
	void lock();
	void unlock();
	DWORD getDim();
	DWORD getElemSize();
	void destroy();
	void destroyData();
	void destroyDescriptor();

	void createGuidArray( const GUID &guid, unsigned int additionalItems = 0 );
	bool jsSafeArray::extractGuid( GUID &guid, long startIndex = 0 );

//Implementation
public:
	~jsSafeArray();
};

/**********************************************************
CLASS
	jsSAArrayWrapper
	
	helper class used by classes that want to be a safeArray
	but have their own member data

DESCRIPTION
	This class added VARIANT operators for classes that
	add member data that prevents casting.  By using this class
	you can treat the derived class just about like a safearray,
	and have member data that would prevent casting if you inherited.
***********************************************************/
class jsSAArrayWrapper
{
public:
	operator VARIANT *() { return m_array; }
	operator VARIANT &() { jsAssert( m_array != 0 ); return *m_array; }

	~jsSAArrayWrapper() { if ( m_freeArray ) delete m_array; }

protected:
	jsSAArrayWrapper() : m_array( new jsSafeArray ), m_freeArray(true) {}
	jsSAArrayWrapper( VARIANT *v ) : m_array( static_cast<jsSafeArray *>(v) ), m_freeArray(false) {}

	jsSafeArray *m_array;
	bool		m_freeArray;

};

inline bool jsSafeArray::operator!=(const SAFEARRAY& saSrc) const { return !operator==(saSrc); }
inline bool jsSafeArray::operator!=(LPCSAFEARRAY pSrc) const { return !operator==(pSrc); }
inline bool jsSafeArray::operator!=(const jsSafeArray& saSrc) const { return !operator==(saSrc); }
inline bool jsSafeArray::operator!=(const VARIANT& varSrc) const { return !operator==(varSrc); }
inline bool jsSafeArray::operator!=(LPCVARIANT pSrc) const { return !operator==(pSrc); }

inline jsSafeArray::~jsSafeArray()
	{ clear(); }

inline jsSafeArray::operator LPVARIANT()
	{ return this; }

inline jsSafeArray::operator LPCVARIANT() const
	{ return this; }

inline DWORD jsSafeArray::getDim()
	{ return ::SafeArrayGetDim(parray); }

inline DWORD jsSafeArray::getElemSize()
	{ return ::SafeArrayGetElemsize(parray); }

inline jsSAData::~jsSAData() 
{ 
	m_array.unaccessData(); 
}


inline 	jsSAData::jsSAData( jsSafeArray &a ) : m_array(a) 
{
	m_array.accessData( &m_buffer );
}

#endif