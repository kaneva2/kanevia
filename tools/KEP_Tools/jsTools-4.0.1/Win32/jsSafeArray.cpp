/******************************************************************************
 Name: jsSafeArray.cpp

 Description: non-MFC copy of the MFC safe array with extensions

 Copyright (C) 1996, 2004 Jim Wallace

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 History:
 11/20/96		jmw created

 Notes: 

******************************************************************************/
#include <atlbase.h>

#include <comdef.h>

#include <oleauto.h>
#include "jsSafeArray.h"

static void PASCAL checkError( HRESULT sc)
{
	if (FAILED(sc))
	{
		_com_raise_error( sc );
	}
}

static HRESULT PASCAL CompareSafeArrays(SAFEARRAY* parray1, SAFEARRAY* parray2)
{
	HRESULT compare = S_FALSE;

	// If one is NULL they must both be NULL to compare
	if (parray1 == NULL || parray2 == NULL)
	{
		return (parray1 == parray2) ? S_OK : S_FALSE;
	}

	// Dimension must match and if 0, then arrays compare
	DWORD dwDim1 = ::SafeArrayGetDim(parray1);
	DWORD dwDim2 = ::SafeArrayGetDim(parray2);
	if (dwDim1 != dwDim2)
		return S_FALSE;
	else if (dwDim1 == 0)
		return S_OK;

	// Element size must match
	DWORD dwSize1 = ::SafeArrayGetElemsize(parray1);
	DWORD dwSize2 = ::SafeArrayGetElemsize(parray2);
	if (dwSize1 != dwSize2)
		return S_FALSE;

	long* pLBound1 = NULL;
	long* pLBound2 = NULL;
	long* pUBound1 = NULL;
	long* pUBound2 = NULL;

	void* pData1 = NULL;
	void* pData2 = NULL;

	try
	{
		// Bounds must match
		pLBound1 = new long[dwDim1];
		pLBound2 = new long[dwDim2];
		pUBound1 = new long[dwDim1];
		pUBound2 = new long[dwDim2];

		size_t nTotalElements = 1;

		// Get and compare bounds
		for (DWORD dwIndex = 0; dwIndex < dwDim1; dwIndex++)
		{
			checkError(::SafeArrayGetLBound(
				parray1, dwIndex+1, &pLBound1[dwIndex]));
			checkError(::SafeArrayGetLBound(
				parray2, dwIndex+1, &pLBound2[dwIndex]));
			checkError(::SafeArrayGetUBound(
				parray1, dwIndex+1, &pUBound1[dwIndex]));
			checkError(::SafeArrayGetUBound(
				parray2, dwIndex+1, &pUBound2[dwIndex]));

			// Check the magnitude of each bound
			if (pUBound1[dwIndex] - pLBound1[dwIndex] !=
				pUBound2[dwIndex] - pLBound2[dwIndex])
			{
				delete[] pLBound1;
				delete[] pLBound2;
				delete[] pUBound1;
				delete[] pUBound2;

				return S_FALSE;
			}

			// Increment the element count
			nTotalElements *= pUBound1[dwIndex] - pLBound1[dwIndex] + 1;
		}

		// Access the data
		checkError(::SafeArrayAccessData(parray1, &pData1));
		checkError(::SafeArrayAccessData(parray2, &pData2));

		// Calculate the number of bytes of data and compare
		size_t nSize = nTotalElements * dwSize1;
		int nOffset = memcmp(pData1, pData2, nSize);

		compare = nOffset == 0 ? S_OK : S_FALSE;

		// Release the array locks
		checkError(::SafeArrayUnaccessData(parray1));
		checkError(::SafeArrayUnaccessData(parray2));
	}
	catch ( HRESULT hr )
	{
		// Clean up bounds arrays
		delete[] pLBound1;
		delete[] pLBound2;
		delete[] pUBound1;
		delete[] pUBound2;

		// Release the array locks
		if (pData1 != NULL)
			checkError(::SafeArrayUnaccessData(parray1));
		if (pData2 != NULL)
			checkError(::SafeArrayUnaccessData(parray2));

		return hr;
	}

	// Clean up bounds arrays
	delete[] pLBound1;
	delete[] pLBound2;
	delete[] pUBound1;
	delete[] pUBound2;

	return compare;
}

static HRESULT CreateOneDimArray(VARIANT& varSrc, DWORD dwSize)
{
	UINT nDim;

	try 
	{

		// Clear VARIANT and re-create SafeArray if necessary
		if (varSrc.vt != (VT_UI1 | VT_ARRAY) ||
			(nDim = ::SafeArrayGetDim(varSrc.parray)) != 1)
		{
			if ( !::VariantClear(&varSrc) == NOERROR)
			{
				jsAssert( false );
			}

			varSrc.vt = VT_UI1 | VT_ARRAY;

			SAFEARRAYBOUND bound;
			bound.cElements = dwSize;
			bound.lLbound = 0;
			varSrc.parray = ::SafeArrayCreate(VT_UI1, 1, &bound);
			if (varSrc.parray == NULL)
				return E_OUTOFMEMORY;
		}
		else
		{
			// Must redimension array if necessary
			long lLower, lUpper;
			checkError(::SafeArrayGetLBound(varSrc.parray, 1, &lLower));
			checkError(::SafeArrayGetUBound(varSrc.parray, 1, &lUpper));

			// Upper bound should always be greater than lower bound
			long lSize = lUpper - lLower;
			if (lSize < 0)
			{
				jsAssert(false);
				lSize = 0;

			}

			if ((DWORD)lSize != dwSize)
			{
				SAFEARRAYBOUND bound;
				bound.cElements = dwSize;
				bound.lLbound = lLower;
				checkError(::SafeArrayRedim(varSrc.parray, &bound));
			}
		}
	}
	catch ( HRESULT hr )
	{
		return hr;
	}

	return S_OK;
}

static void PASCAL CopyBinaryData(SAFEARRAY* parray, const void* pvSrc, DWORD dwSize)
{
	// Access the data, copy it and unaccess it.
	void* pDest;
	checkError(::SafeArrayAccessData(parray, &pDest));
	memcpy(pDest, pvSrc, dwSize);
	checkError(::SafeArrayUnaccessData(parray));
}

/////////////////////////////////////////////////////////////////////////////
// jsSafeArray class
jsSafeArray::jsSafeArray(const SAFEARRAY& saSrc, VARTYPE vtSrc)
{
	jsSafeArrayInit(this);
	vt = (VARTYPE)(vtSrc | VT_ARRAY);
	checkError(::SafeArrayCopy((LPSAFEARRAY)&saSrc, &parray));
//	m_dwDims = GetDim();
//	m_dwElementSize = GetElemSize();
}

jsSafeArray::jsSafeArray(LPCSAFEARRAY pSrc, VARTYPE vtSrc)
{
	jsSafeArrayInit(this);
	vt = (VARTYPE)(vtSrc | VT_ARRAY);
	checkError(::SafeArrayCopy((LPSAFEARRAY)pSrc, &parray));
//	m_dwDims = GetDim();
//	m_dwElementSize = GetElemSize();
}

jsSafeArray::jsSafeArray(const jsSafeArray& saSrc)
{
	jsSafeArrayInit(this);
	*this = saSrc;
//	m_dwDims = GetDim();
//	m_dwElementSize = GetElemSize();
}

jsSafeArray::jsSafeArray(const VARIANT& varSrc)
{
	jsSafeArrayInit(this);
	*this = varSrc;
//	m_dwDims = GetDim();
//	m_dwElementSize = GetElemSize();
}

jsSafeArray::jsSafeArray(LPCVARIANT pSrc)
{
	jsSafeArrayInit(this);
	*this = pSrc;
//	m_dwDims = GetDim();
//	m_dwElementSize = GetElemSize();
}

// Operations
void jsSafeArray::attach(VARIANT& varSrc)
{
	jsAssert((varSrc.vt & VT_ARRAY) != 0);

	// Free up previous safe array if necessary
	clear();

	// give control of data to jsSafeArray
	::memcpy(this, &varSrc, sizeof(varSrc));
	varSrc.vt = VT_EMPTY;
}

VARIANT jsSafeArray::detach()
{
	VARIANT varResult = *this;
	vt = VT_EMPTY;
	return varResult;
}

// Assignment operators
jsSafeArray& jsSafeArray::operator=(const jsSafeArray& saSrc)
{
	jsAssert((saSrc.vt & VT_ARRAY) != 0);

	checkError(::VariantCopy(this, (LPVARIANT)&saSrc));
	return *this;
}

jsSafeArray& jsSafeArray::operator=(const VARIANT& varSrc)
{
	jsAssert((varSrc.vt & VT_ARRAY) != 0);

	checkError(::VariantCopy(this, (LPVARIANT)&varSrc));
	return *this;
}

jsSafeArray& jsSafeArray::operator=(LPCVARIANT pSrc)
{
	jsAssert((pSrc->vt & VT_ARRAY) != 0);

	checkError(::VariantCopy(this, (LPVARIANT)pSrc));
	return *this;
}

// Comparison operators
bool jsSafeArray::operator==(const SAFEARRAY& saSrc) const
{
	HRESULT hr = CompareSafeArrays(parray, (LPSAFEARRAY)&saSrc);
	checkError( hr );
	return hr == S_OK; // S_FALSE if not equal
}

bool jsSafeArray::operator==(LPCSAFEARRAY pSrc) const
{
	HRESULT hr = CompareSafeArrays(parray, (LPSAFEARRAY)pSrc);
	checkError( hr );
	return hr == S_OK; // S_FALSE if not equal
}

bool jsSafeArray::operator==(const jsSafeArray& saSrc) const
{
	if (vt != saSrc.vt)
		return false;

	HRESULT hr = CompareSafeArrays(parray, saSrc.parray);
	checkError( hr );
	return hr == S_OK; // S_FALSE if not equal
}

bool jsSafeArray::operator==(const VARIANT& varSrc) const
{
	if (vt != varSrc.vt)
		return false;

	HRESULT hr = CompareSafeArrays(parray, varSrc.parray);
	checkError( hr );
	return hr == S_OK; // S_FALSE if not equal

}

bool jsSafeArray::operator==(LPCVARIANT pSrc) const
{
	if (vt != pSrc->vt)
		return false;

	HRESULT hr = CompareSafeArrays(parray, pSrc->parray) == S_OK; 
	checkError( hr );
	return hr == S_OK; // S_FALSE if not equal

}

void jsSafeArray::createOneDim(VARTYPE vtSrc, DWORD dwElements,
	void* pvSrcData, long nLBound)
{
	jsAssert(dwElements >= 0);

	// Setup the bounds and create the array
	SAFEARRAYBOUND rgsabound;
	rgsabound.cElements = dwElements;
	rgsabound.lLbound = nLBound;
	create(vtSrc, 1, &rgsabound);

	// Copy over the data if neccessary
	if (pvSrcData != NULL)
	{
		void* pvDestData;
		accessData(&pvDestData);
		::memcpy(pvDestData, pvSrcData, getElemSize() * dwElements);
		unaccessData();
	}
}

DWORD jsSafeArray::getOneDimSize()
{
	jsAssert(getDim() == 1);
	return getUBound(1) + 1 - getLBound(1);
}

void jsSafeArray::resizeOneDim(DWORD dwElements)
{
	jsAssert(getDim() == 1);

	SAFEARRAYBOUND rgsabound;

	rgsabound.cElements = dwElements;
	rgsabound.lLbound = 0;

	redim(&rgsabound);
}

HRESULT jsSafeArray::create(VARTYPE vtSrc, DWORD dwDims, DWORD* rgElements)
{
	jsAssert(rgElements != NULL);

	// Allocate and fill proxy array of bounds (with lower bound of zero)
	SAFEARRAYBOUND* rgsaBounds = new SAFEARRAYBOUND[dwDims];

	for (DWORD dwIndex = 0; dwIndex < dwDims; dwIndex++)
	{
		// Assume lower bound is 0 and fill in element count
		rgsaBounds[dwIndex].lLbound = 0;
		rgsaBounds[dwIndex].cElements = rgElements[dwIndex];
	}

	HRESULT hr = create(vtSrc, dwDims, rgsaBounds);

	if ( FAILED( hr ) )
	{
		// Must free up memory
		delete [] rgsaBounds;
		return hr; 
	}

	delete [] rgsaBounds;

	return S_OK;
}

HRESULT jsSafeArray::create(VARTYPE vtSrc, DWORD dwDims, SAFEARRAYBOUND* rgsabound)
{
	HRESULT  hr = S_OK;

	jsAssert(dwDims > 0);
	jsAssert(rgsabound != NULL);

	// Validate the VARTYPE for SafeArrayCreate call
	jsAssert(!(vtSrc & VT_ARRAY));
	jsAssert(!(vtSrc & VT_BYREF));
	jsAssert(!(vtSrc & VT_VECTOR));
	jsAssert(vtSrc != VT_EMPTY);
	jsAssert(vtSrc != VT_NULL);

	// Free up old safe array if necessary
	clear();

	parray = ::SafeArrayCreate(vtSrc, dwDims, rgsabound);

	if (parray == NULL)
		hr = E_OUTOFMEMORY; 

	vt = unsigned short(vtSrc | VT_ARRAY);
//	m_dwDims = dwDims;
//	m_dwElementSize = GetElemSize();

	return hr;
}

void jsSafeArray::accessData(void** ppvData)
{
	checkError(::SafeArrayAccessData(parray, ppvData));
}

void jsSafeArray::unaccessData()
{
	checkError(::SafeArrayUnaccessData(parray));
}

void jsSafeArray::allocData()
{
	checkError(::SafeArrayAllocData(parray));
}

void jsSafeArray::allocDescriptor(DWORD dwDims)
{
	checkError(::SafeArrayAllocDescriptor(dwDims, &parray));
}

void jsSafeArray::copy(LPSAFEARRAY* ppsa)
{
	checkError(::SafeArrayCopy(parray, ppsa));
}

long jsSafeArray::getLBound(DWORD dwDim )
{
	long Lbound;
	checkError(::SafeArrayGetLBound(parray, dwDim, &Lbound));
	return Lbound;
}

long jsSafeArray::getUBound(DWORD dwDim)
{
	long Ubound;
	checkError(::SafeArrayGetUBound(parray, dwDim, &Ubound));
	return Ubound;
}

void jsSafeArray::getElement(long* rgIndices, void* pvData)
{
	checkError(::SafeArrayGetElement(parray, rgIndices, pvData));
}

void jsSafeArray::ptrOfIndex(long* rgIndices, void** ppvData)
{
	checkError(::SafeArrayPtrOfIndex(parray, rgIndices, ppvData));
}

void jsSafeArray::putElement(long* rgIndices, void* pvData)
{
	checkError(::SafeArrayPutElement(parray, rgIndices, pvData));
}

void jsSafeArray::redim(SAFEARRAYBOUND* psaboundNew)
{
	checkError(::SafeArrayRedim(parray, psaboundNew));
}

void jsSafeArray::lock()
{
	checkError(::SafeArrayLock(parray));
}

void jsSafeArray::unlock()
{
	checkError(::SafeArrayUnlock(parray));
}

void jsSafeArray::destroy()
{
	checkError(::SafeArrayDestroy(parray));
	jsSafeArrayInit(this);
	vt = VT_EMPTY;
}

void jsSafeArray::destroyData()
{
	checkError(::SafeArrayDestroyData(parray));
}

void jsSafeArray::destroyDescriptor()
{
	checkError(::SafeArrayDestroyDescriptor(parray));
	jsSafeArrayInit(this);
	vt = VT_EMPTY;
}

static const int GUID_ITEMS = 11;

void jsSafeArray::createGuidArray( const GUID &guid, unsigned int additionalItems )
{
    createOneDim( VT_VARIANT, GUID_ITEMS+additionalItems );

	// GUID format is 
	// DWORD Data1;    WORD   Data2;    WORD   Data3;    BYTE  Data4[8];

	LONG index = 0;
	_variant_t v = (long)guid.Data1;
	putElement( &index, (void*)&v );
	index++;

	v = (short)guid.Data2;
	putElement( &index, (void*)&v );
	index++;

	v = (short)guid.Data3;
	putElement( &index, (void*)&v );
	index++;

	for ( int j = 0; j < 8; j++ )
	{
		v = guid.Data4[j];
		putElement( &index, (void*)&v );
		index++;
	}
}

bool jsSafeArray::extractGuid( GUID &guid, long startIndex /*= 0*/ )
{
	long end = vt == VT_EMPTY ? 0 : getOneDimSize();

	_variant_t v;

	long index = startIndex ;

	if ( end < startIndex + GUID_ITEMS )
		return false;

	// GUID format is 
	// DWORD Data1;    WORD   Data2;    WORD   Data3;    BYTE  Data4[8];

	// get the GUID
	getElement( &index, (void*)&v);
	if ( v.vt != VT_I4 )
		return false;
	guid.Data1 = (DWORD)(long)v;
	index++;

	getElement( &index, (void*)&v);
	if ( v.vt != VT_I2 )
		return false;
	guid.Data2 = (WORD)(short)v;
	index++;

	getElement( &index, (void*)&v);
	if ( v.vt != VT_I2 )
		return false;
	guid.Data3 = (WORD)(short)v;
	index++;

	for ( int i = 0; i < 8; i++ )
	{
		getElement( &index, (void*)&v);
		if ( v.vt != VT_UI1 )
			return false;
		guid.Data4[i] = v;
		index++;
	}

	return true;
}

///////////////////////////////////////////////////////////////////////////////
// jsSafeArray Helpers
static void jsSafeArrayInit(jsSafeArray* psa)
{
	memset(psa, 0, sizeof(*psa));
}


jsSafeArray::jsSafeArray()
	{ jsSafeArrayInit(this);
		vt = VT_EMPTY; }

void jsSafeArray::clear()
	{ jsVerify(::VariantClear(this) == NOERROR); }
