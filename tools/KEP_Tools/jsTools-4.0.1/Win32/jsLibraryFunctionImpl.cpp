/******************************************************************************
 Name: jsLibraryFunctionImpl.cpp

 Description: impl file for loading a function

 Copyright (C) 1999, 2004 Jim Wallace

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 History:
 1/28/99		jmw created

 Notes: 

******************************************************************************/
#include <js.h>
#include <windows.h>
#include "jsLibraryFunctionImpl.h"

#include "jsUnicode.h"

///---------------------------------------------------------
// getFunction
// 
// [in] procName name of procedure in lib. Always single byte
//
// [Returns]
//    the fn ptr or null if error.  In that case lastError() is valid
JSLIBFUNCTION jsLibraryFunctionImpl::getFunction( const char* procName)
{
	m_lastError.set( NO_ERROR );

	if ( m_lib == 0 ) 
	{
		m_lib = ::LoadLibrary( KEP::Utf8ToUtf16(m_dllName).c_str() );
		if ( m_lib == NULL )
		{
			m_lastError.set( ::GetLastError() );
			return 0;
		}
	}

	FARPROC p = ::GetProcAddress( m_lib, procName );
	if ( p == NULL )
	{
		m_lastError.set( ::GetLastError() );
		return 0;
	}
	
	return (JSLIBFUNCTION)p;
}

