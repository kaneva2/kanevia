/******************************************************************************
 Name: jsThreadImpl.h

 Description: implementation of the portable thread class

 Copyright (C) 1996, 2004 Jim Wallace

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 History:
 11/20/96		jmw created

 Notes: 

******************************************************************************/
#ifndef _ThreadImpl_h_
#define _ThreadImpl_h_

#include "jsThread.h"
#include "jsLock.h"

#ifdef _NT
	#pragma warning ( disable : 4201 4214 4115 4514 )

	#include "windows.h" 

	#pragma warning ( default : 4201 4214 4115 )
#endif

// Win32 implementation of the thread class
class jsThreadImpl : public jsBase
{
public:
	enum {KILLED_EXIT_CODE = 0xdead};

	jsThreadImpl( jsThread *thd, const char* name, jsThdPriority prio = PR_NORMAL, 
						jsThreadType = TT_NORMAL );

	bool start();
	void wakeUp();
	bool reset();

	// status
	bool isSleeping() const;
	bool isRunning() const;
	bool isTerminated() const { return m_bTerminated; }
	bool isCreated() const;

	jsThdPriority priority() const;
	void priority( jsThdPriority mp );

	ULONG exitCode() const;

	// ending the thread
	void kill();

	jsThread::StopRet stop( ULONG NiceWaitMS = 60000, 
			 bool killIfTimeout = false );

	bool join( ULONG WaitMS = INFINITE_WAIT ) const;
 
	// thread id functions
	static jsThreadID currentThreadID();
	jsThreadID threadID() const { return m_threadID; }
	bool operator==( jsThreadID id ) { return m_threadID == id; }

	// thread-specific data, all work in context of calling thread
	static int		createThreadData();
	static ULONG	getThreadData( int id );
	static bool		setThreadData( int id, ULONG value );
	static void 	freeThreadData( int id );

	virtual ~jsThreadImpl();

	static int			mapPriority( jsThdPriority mpp );
	static jsThdPriority	unmapPriority( int ospri );

protected:
	// prevent copy
	jsThreadImpl( const jsThreadImpl & ) { jsAssert(false); }
	jsThreadImpl &operator=( const jsThreadImpl &) { jsAssert(false); return *this; }

	// these fns are only to be used in derived class
	bool _sleep( ULONG ms ); 
	void _yield();
	void _setTerminated() { m_bTerminated = true; }

private:
	jsThread	*m_thd;		// owner of this thread object
	jsEvent		m_wake;		// for sleeping
	std::string	m_name;		// name of this thread

	bool		m_bTerminated; // keep track of if we're done

	#ifdef _NT

	HANDLE	m_handle;		// thread handle

	ULONG 	m_threadID;
	bool	m_bExited;
	int		m_nPriority;
	bool	m_bStarted; 
	bool	m_bSuspendOnCreate;

	// static fn that is the entry point of the thread
	// we call this which calls this objects _doWork virtual function
	static ULONG _stdcall ThreadEntryPoint(LPVOID lpvThis);

	#if  (_MFC_VER > 0 && _MSC_VER >= 900 && defined(_WINDOWS))
		CWinThread *m_WinThd;
	#endif

	#endif // NT

friend class jsThread;
};

#endif
