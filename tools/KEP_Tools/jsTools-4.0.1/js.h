/******************************************************************************
 Name: js.h

 Description: header of common defines, etc used by all other js*.h and js*.cpp
 files

 Copyright (C) 1996, 2004 Jim Wallace

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 History:
 11/20/96		jmw created

 Notes: 

******************************************************************************/
#ifndef _js_h_
#define _js_h_

#define	NOMINMAX		//undefs min and max from mfc

// typedefs for commonly used types
// common typedefs in another file so CORBA can inlucde them
typedef unsigned char   octet;
#undef JSDEFINEDOCTET

// VC++ /J option will define this, others can either
//	JS_UNSIGNED_CHAR manually if the compiler uses char as unsigned char
#ifdef _CHAR_UNSIGNED
#	define JS_UNSIGNED_CHAR
#endif

#include <string>
#include "jsTypes.h"	// for use with corba headers, too

///
// double type
typedef double DOUBLE;

///
// max DOUBLE value
#ifndef DOUBLE_MAX 
const DOUBLE DOUBLE_MAX = 1.7976931348623158e+308;
#endif

///
// double type
typedef float FLOAT;

///
// max FLOAT value
#ifndef FLOAT_MAX 
const FLOAT FLOAT_MAX = 3.4E+38f;
#endif


///
// max ULONG value
#ifndef ULONG_MAX
const ULONG ULONG_MAX 	= 0xffffffff;
#endif

///
// max LONG value
#ifndef LONG_MAX
const LONG LONG_MAX 	= 0x7fffffff;
#endif

///
// max USHORT value
#ifndef USHORT_MAX
const USHORT USHORT_MAX = 0xffff;
#endif

///
// max SHORT value
#ifndef SHORT_MAX
const SHORT SHORT_MAX 	= 0x7fff;
#endif

///
// max BYTE value
#ifndef BYTE_MAX
const BYTE BYTE_MAX		= 0xff;
#endif

///
// pointer a LONG 
typedef long			*PLONG;

///
// pointer a ULONG 
typedef unsigned long	*PULONG;

///
// pointer a SHORT 
typedef short			*PSHORT;

///
// pointer a USHORT 
typedef unsigned short	*PUSHORT;

///
// pointer a BYTE 
typedef unsigned char	*PBYTE;

///
// const pointer a BYTE
typedef const unsigned char	*PCBYTE;

///
// unsigned integer, platform dependent size
typedef unsigned int	UINT;

///
// max UINT value
#ifndef UINT_MAX 
const UINT UINT_MAX 	= (UINT)-1;
#endif

///
// pointer to UINT
typedef unsigned int	*PUINT;

///
// usigned integer, platform dependent size
typedef int 			INT;

///
// pointer to INT
typedef int 			*PINT;

///
// pointer to void
typedef void			*PVOID;

///
// const pointer to void
typedef const void		*PCVOID;

///
// NT redefinitions
#ifdef _NT
#define isfinite _finite
#endif

///
// for functions that take timeouts, this
// indicated to wait forever
const ULONG INFINITE_WAIT = 0xffffffff;

// defines to allow you to use IN, OUT, INOUT
// in function definitions
#ifndef IN
#define IN 
#endif 

#ifndef OUT
#define OUT
#endif 

#ifndef INOUT
#define INOUT
#endif 

// enums used throughout

///
// enum for use with many of the synchronization classes
enum jsWaitRet  
{ 
    WR_OK,      // item returned in time limit
    WR_TIMEOUT, // item failed to return in time limit
    WR_ERROR    // an error occurred while waiting
};

#ifdef _NT
/*
	/// 
	// The export option helper macro.
	// Use this for exporting or importing funcitons and
	// classes from a DLL or shared libarary.  Currently the 
	// macro only works for Win32.  Simply put the macro
	// as a qualifier to the class or funciton
	//
	// e.g. class JSEXPORT myClass
	// int JSEXPORT myFunction
	//
	// Use #defines to control what the macro does.
	// [] _JSBUILD_DLL exports the item
	// [] _JSBUILD_LIB make the macro do nothing
	// [] _JSLIB make the macro do nothing, same as _JSBUILD_LIB
	// [] <nothing> or _JSDLL imports the item
	JSEXPORT
*/
	#ifdef _JSBUILD_DLL

		#define JSEXPORT __declspec( dllexport ) 
		#define JSEXPIMP_TEMPLATE

		#if defined(_JSBUILD_LIB) || defined(_JSLIB) || defined(_JSDLL)
			#error Too many build options defined!
		#endif

	#elif defined(_JSBUILD_LIB) || defined (_JSLIB)

		#define JSEXPORT 
		#define JSEXPIMP_TEMPLATE

		#if defined(_JSBUILD_DLL) || defined(_JSDLL)
			#error Too many build options defined!
		#endif

	#else // using the DLL

		#define JSEXPORT __declspec( dllimport ) 
		#define JSEXPIMP_TEMPLATE extern

		#if defined(_JSBUILD_LIB) || defined(_JSLIB) || defined(_JSBUILD_DLL)
			#error Too many build options defined!
		#endif

	#endif

#else

	#define JSEXPORT
	#define JSEXPIMP_TEMPLATE

#endif

/*****************************************************************************
CLASS
    jsBase

    Base class for many jsTools classes

DESCRIPTION
    encapsulation of some basic functionality used mostly internally
******************************************************************************/
class JSEXPORT jsBase
{
public:
    ///-----------------------------
    // constructor
	jsBase() : m_ok(true) {}

    ///-----------------------------
    // copy constructor
	jsBase( const jsBase &src ) { operator=(src); }

    ///-----------------------------
    // assignment operator
	jsBase &operator=(const jsBase &src ) { if ( &src != this ) m_ok = src.m_ok; return *this; }

    ///-----------------------------
    // [Returns]
    //  true if the object has its ok flag set to true
	virtual bool ok() const { return m_ok; }

	virtual ~jsBase() {}

protected:
    ///-----------------------------
    // in a derived class use this to set the ok flag
	void setOk( bool b = true ) { m_ok = b; }

private:
	bool	m_ok;
};


/*****************************************************************************
GROUP: general helper functions
******************************************************************************/

#ifndef MIN
///
// min macro.  This does evaluate the parameter twice
#define MIN(a,b) \
        (a < b ? a : b)
#endif

#ifndef MAX
///
// max macro.  This does evaluate the parameter twice
#define MAX(a,b) \
        (a > b ? a : b)
#endif

///
// min template.  This does not evaluate the parameter twice
template<class T> inline const T& minObject( const T& a, const T& b ) { return a < b ? a : b; }

///
// max template.  This does not evaluate the parameter twice
template<class T> inline const T& maxObject( const T& a, const T& b ) { return a > b ? a : b; }

///
// jsTickCount gets the current time in ms 
ULONG JSEXPORT jsTickCount(); 

/*****************************************************************************
GROUP: Debugging macros
******************************************************************************/

///
// print a string to the debugger output window
// 
// [in] s the string to print out
// [in] ... optional printf-type parameters 
void JSEXPORT jsOutputToDebugger( const char* s, ... );

///
// cause a break into the debugger
void JSEXPORT jsBreakIntoDebugger();

///
// the function that actually does the assert handling and possible
// logging, write your own if you want something different
//
// [in] expression if false this will break into the debugger
// [in] file file name where assert occurred -- always single byte since __FILE__ is char *
// [in] line line in file where assert occurred
bool JSEXPORT jsAssertFunction( bool expression, const char * file, int line );

///
// macro for printing to the debugger
#define jsTrace jsOutputToDebugger

#ifdef _JSDBG

#ifndef INLINE 
	#define INLINE
#endif

    ///
	// debug memory allocation
    //  for now just do new
	#define jsDebugNew new

    ///
	// assert.  This only is evaluated if _JSDBG is defined
    //
    // [in] b boolean value, if false, will assert
	#define jsAssert(b) jsAssertFunction((b),__FILE__,__LINE__)

    /// 
    // assert if expression is false if _JSDBG is defined.  Otherwise
    // will evaluate expression and not assert.
    //
    // [in] b boolean value, if false, will assert
	#define jsVerify(b) jsAssert(b)

    ///
	// verifies that return from the function if the expression is false
	// the tempbool is used to evaluate the expression only once in case it is 
	// something like jsVerifyReturnVoid(i++);<br>
    // If _JSDBG is not defined, it does everything but the assert.
    //
    // [in] b boolean value, if false, will assert
	#define jsVerifyReturnVoid( b )\
		{bool _jsTempbool_ = (b); jsAssert( _jsTempbool_); if ( !_jsTempbool_ ) return;}

    ///
	// verifies that return from the function if the expression is false
	// the tempbool is used to evaluate the expression only once in case it is 
	// something like jsVerifyReturn(i++);<br>
    // If _JSDBG is not defined, it does everything but the assert.
    //
    // [in] b boolean value, if false, will assert
    // [in] retVal the value to return if b is false.
	#define jsVerifyReturn( b, retVal )\
		{bool _jsTempbool_ = (b); jsAssert( _jsTempbool_); if ( !_jsTempbool_ ) return retVal;}

    ///
	// verifies that return from the function if the expression is false
	// the tempbool is used to evaluate the expression only once in case it is 
	// something like jsVerifyThrow(i++);<br>
    // If _JSDBG is not defined, it does everything but the assert.
    //
    // [in] b boolean value, if false, will assert
    // [in] throwIt the thing to be thrown if b is false.
	#define jsVerifyThrow( b, throwIt )\
		{bool _jsTempbool_ = (b); jsAssert( _jsTempbool_); if ( !_jsTempbool_ ) throw (throwIt);}

    ///
	// Same as jsVerityReturn(), only this sends a message to jsTrace also.
    //
    // [in] b boolean value, if false, will assert
    // [in] retVal the value to return if b is false.
    // [in] msg msg to send to jsTrace
	#define jsVerifyReturnMsg( b, retVal, msg )\
		{bool _jsTempbool_ = (b); jsAssert( _jsTempbool_ ); if ( !_jsTempbool_ ) { jsTrace(msg); return retVal; } }

    ///
	// Same as jsVerityReturnVoid(), only this sends a message to jsTrace also.
    //
    // [in] b boolean value, if false, will assert
    // [in] msg msg to send to jsTrace
	#define jsVerifyReturnMsgVoid( b, msg )\
		{bool _jsTempbool_ = (b); jsAssert( _jsTempbool_); if ( !_jsTempbool_ ) { jsTrace(msg); return; } }

    ///
	// this does a verify then some arbitray action.
    //
    // [in] b boolean value, if false, will assert
    // [in] action the msg msg to send to jsTrace
	#define jsVerifyDo( b, action ) \
		{bool _jsTempbool_ = (b); jsAssert( _jsTempbool_ ); if ( !_jsTempbool_ ) { action; } }

#else
	// non debug versions of the macros.  
	// most do nothing, except Verifies which still to the expressions and possible return
#ifndef INLINE
	#define INLINE inline 
#endif

	#define jsVerify(b) (b)
	#define jsDebugNew new
	#define jsAssert(b)	
	#define jsVerifyReturn( b, retVal ) { if ( !(b) ) return retVal;}
	#define jsVerifyThrow( b, throwIt ) { if ( !(b) ) throw (throwIt);}
	#define jsVerifyReturnVoid( b ) { if ( !(b) ) return; }

	// no messages printed out in non-debug builds
	#define jsVerifyReturnMsg( b, retVal, msg ) { if ( !(b) ) return retVal; }
	#define jsVerifyReturnMsgVoid( b, msg ) { if ( !(b) ) return; }

	#define jsVerifyDo( b, action ) \
		{ if ( !(b) ) { action; } }

#endif

#ifndef DELETE_DEF
    ///
    // little helper macro to delete and zero a variable
    //
    // [in] x varible to delete, then set to 0
	#define DELETE_AND_ZERO(x) { if (x) { delete x; x = 0; } }
	#define DELETE_DEF
#endif

#include <vector>
#define jsStringArray std::vector<std::string> 

#endif
 
 
 
 
 
