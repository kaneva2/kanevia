// EncodeTest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <jsBEREncoder.h>
#include <jsBERDecoder.h>

int _tmain(int argc, _TCHAR* argv[])
{
	jsBEREncoder encoder;
	LONGLONG ll[10] = { 0x12i64, 0x1234i64 , 0x12356i64, 0x12345678i64, 0 - 0x12345678i64,
						0x1234567890 , 0x123456789012, 0x12345678901234, 0x1234567890123456, 0 - 0x12345678901234  };
	ULONGLONG ull[10] = { 0x98i64, 0x9876i64 , 0x987654i64 , 0x98765432i64 , 0x9876543210 , 
						0x987654321098 , 0x98765432109876, 0x9876543210987654, 0xfff12345678901 };
	LONGLONG l;
	ULONGLONG ul;

	for ( int i = 0; i < 10; i++ )
	{
		encoder << ll[i] << ull[i];
	}

	jsBERDecoder decoder( encoder.buffer(), encoder.bufferSize() );

	for ( int i = 0; i < 10; i++ )
	{
		decoder >> l >> ul;
		printf( "%d %I64d == %I64d\n", ll[i] == l , ll[i],  l );
		printf( "%d %I64u == %I64u\n", ull[i] == ul , ull[i],  ul );
	}
	return 0;
}

