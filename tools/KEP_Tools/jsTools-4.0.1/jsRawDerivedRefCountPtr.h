#ifndef __jsRawDerivedRefCountPtr_H__
#define __jsRawDerivedRefCountPtr_H__
/******************************************************************************
 Name: jsRawDerivedRefCountPtr.h

 Description: allows clean casting of jsRefCountObjects

 Copyright (C) 1996, 2004 Jim Wallace

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 History:
 5/12/99		jmw created 

 Notes: 

******************************************************************************/
#ifndef _js_h_
	#error Include js.h before jsRawDerivedRefCountPtr.h
#endif

#include "jsRefCountPtr.h"


template <class DERIVEDCLASS, class PARENTCLASS > 
/***********************************************************
CLASS
	jsRawDerivedRefCountPtr
	
	allows use of inheritance for jsRefCount objects

DESCRIPTION
	Use this class if you are using derived classes of 
	an object that uses the jsRefCountPtr.  This allows
	you to use a derived class ref count pointer and still make calls to 
	methods that take the base class ref count pointer.

	If you are compiling with RTTI, this will only work if
	the base class has a virtual function.  This class will use
	RTTI to check the type when you assign the base class pointers to 
	this object in the constructor or with the assignement operator.
	If it is not of the derived class, it will have a null pointer.

SAMPLE
	This sample shows how to use the class.

	EXAMPLE
	class A
	{
	public:
		virtual int x() const { jsOutputToDebugger( "A called" ); return 1; }
		virtual ~A() { jsOutputToDebugger( "A dies" );}
	};

	class B : public A
	{
	public:
		virtual int x() const { jsOutputToDebugger( "B called" ); return 2; }
		virtual ~B() { jsOutputToDebugger( "B dies" );}
	};

	
	typedef jsRefCountPtr<A> APtr;

	// define the derived refcount template
	typedef jsRawDerivedRefCountPtr<B, APtr> BPtr;

	// old class D doesn't know about B.  It only
	// uses jsRefCountPtr's to A
	class D
	{
	public:
		void set( APtr &a ) { mA = a; }
		APtr get() { return mA; }

		std::vector<APtr> array;

		int fn( APtr &a )
		{
			jsAssert( a );

			(*a).x();
			return a->x();
		}

		int fn2( const APtr &a )
		{
			jsAssert( a );

			(*a).x();
			return a->x();
		}

	};

	void test()
	{
		D d;
		APtr a( new A );
		BPtr b( new B );

		d.fn( a );
		d.fn( b );
		d.fn2( a );
		d.fn2( b );

		...
	}



	END

***********************************************************/
class jsRawDerivedRefCountPtr : public PARENTCLASS
{
public:
	typedef PARENTCLASS parentClassPtr;

	///---------------------------------------------------------
	// constructor from ref counted parent class.  
	// 
	// [in] ptr reference counted object 
	//
	jsRawDerivedRefCountPtr( parentClassPtr &ptr ) : 
		parentClassPtr( (DERIVEDCLASS*)0 )
	{
		if ( ptr && checkPointer( ptr ) )
			(*this) = ptr;
	}

	///---------------------------------------------------------
	// constructor from ref counted class.  
	// 
	// [in] ptr reference counted object 
	//
	jsRawDerivedRefCountPtr( const jsRawDerivedRefCountPtr &ptr ) : 
		parentClassPtr( ptr )
	{}

	///---------------------------------------------------------
	// constructor from derived class.  
	// 
	// [in] ptr object that you want
	//
	jsRawDerivedRefCountPtr( DERIVEDCLASS *ptr = NULL) : 
		parentClassPtr( ptr )
	{}

	///---------------------------------------------------------
	// constructor from parent class.  If the ptr is not 
	// of the derived type (if using RTTI) if will be null.
	// If not using RTTI, you're on your own
	// 
	// [in] ptr object that you want
	//
/*	jsRawDerivedRefCountPtr( PARENTCLASS *ptr ) : 
		parentClassPtr( (void*)0 )
	{
		if ( ptr && checkPointer( ptr ) )
			operator=( parentClassPtr( ptr ) );
	}
*/
	///---------------------------------------------------------
	// assignment operator
	// 
	// [in] src object to copy
	//
	// [Returns]
	//    this
	jsRawDerivedRefCountPtr &operator=( const jsRawDerivedRefCountPtr &src ) 
	{ 
		if ( this != &src ) 
		{
			parentClassPtr::operator=(src); 
		} 
		return *this; 
	}

	///---------------------------------------------------------
	// assignment operator
	// 
	// [in] src object to copy
	//
	// [Returns]
	//    this
	jsRawDerivedRefCountPtr &operator=( parentClassPtr &src ) 	
	{ 
		if ( this != &src ) 
		{
			if ( checkPointer( src ) )
				parentClassPtr::operator=(src); 
			else
				(*this) = jsRawDerivedRefCountPtr((DERIVEDCLASS*)0);
		} 
		return *this; 
	}



	///---------------------------------------------------------
	// [Returns]
	//		a pointer to the contained object.  <b>Do not save 
	// off this pointer!</b>
	inline DERIVEDCLASS* operator->() { return (DERIVEDCLASS*)parentClassPtr::operator->(); }

	///---------------------------------------------------------
	// [Returns]
	//		a const pointer to the contained object.  <b>Do not save 
	// off this pointer!</b>
	inline const DERIVEDCLASS* operator->() const { return (DERIVEDCLASS*)parentClassPtr::operator->(); }

	///---------------------------------------------------------
	// [Returns]
	//		a reference to the contained object.  <b>Do not save 
	// off this reference!</b>
	inline DERIVEDCLASS &operator*() { return *operator->(); }

	///---------------------------------------------------------
	// [Returns]
	//		a const reference to the contained object.  <b>Do not save 
	// off this reference!</b>
	inline const DERIVEDCLASS &operator*() const { return *operator->(); }

	///---------------------------------------------------------
	// [Returns]
	//		true if the contained pointer is not null
	inline operator bool() const { return parentClassPtr::operator bool(); }

private:
	bool checkPointer( PARENTCLASS &ptr )
	{
		bool ret = true;
#		ifdef _CPPRTTI
			if ( ptr.operator->() )
			{
				DERIVEDCLASS *p = dynamic_cast<DERIVEDCLASS*>( ptr.operator->() );
				ret = p != 0;
			}
#		endif
		return ret;
	}
};


#endif //__jsRawDerivedRefCountPtr_H__
