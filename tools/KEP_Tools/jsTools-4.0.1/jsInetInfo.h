/******************************************************************************
 Name: jsInetInfo.h

 Description: socket jsConnection class definition

 Copyright (C) 1996, 2004 Jim Wallace

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 History:
 1/26/97	jmw created via AS

 Notes:		All information related to gethostbyname and gethostbyaddress is encapsulated
			in this class. Again, this class is not **thread safe***. Instantiate a
			new objcet for each thread or instance you need the info.

******************************************************************************/
#ifndef _jsInetInfo_h_
#define _jsInetInfo_h_

#ifndef _js_h_
	#error Include js.h before jsInetInfo.h
#endif

#include "jsSync.h"

#include "jsSocket.h"


class jsInetInfo : public jsBase
{
public:
	jsInetInfo( const char* hname = 0 );
	virtual ~jsInetInfo();

	//get local hostname
	inline std::string hostName();

	//get complete hostname with DNS expansion if applicable
	inline std::string fqHostName(); 

	//get the numeric Ip address, in host byte order
	inline ULONG numAddress();

	//get the ip address of the socket as a string
	inline std::string address();

	inline void allNumAddress( vector<ULONG>& addr );

	inline void allAddress( jsStringArray& addr );

	inline void allAlias( jsStringArray& alias );

	//copy constructors
	inline jsInetInfo( const jsInetInfo& ii );

	inline const jsInetInfo&	operator = (const jsInetInfo& ii ); 

private:

	std::string m_hostName;
	std::string m_dnsName;
	jsStringArray m_hostAlias;
	vector<ULONG> m_hostAddr;	

	jsSocketPtr m_sock;
	bool m_loaded;

	//given a host name, return the dnsname, host address array  and the host aliases as strings
	inline bool getAllHostInfo( std::string hname, std::string& dnsname, jsStringArray& halias, vector<ULONG>& haddr );

	inline void _copy( const jsInetInfo& ii );

};


/***************************************************************************************************
					Define all inlines below
 ***************************************************************************************************/

//NOTE:we may give in a numeric address, in that case hostname = numeric address, whereas
//domain name may be a real hostname...
inline jsInetInfo::jsInetInfo( const jsInetInfo& ii) : m_sock( ii.m_sock )
{
	operator=(ii);
}

inline const jsInetInfo& jsInetInfo::operator= ( const jsInetInfo& ii )
{
	if ( &ii != this )
		_copy( ii );
	return *this;
}

inline void jsInetInfo::_copy( const jsInetInfo& ii )
{
	m_dnsName = ii.m_dnsName;
	m_hostName = ii.m_hostName;
	m_hostAlias.clear();
	m_hostAddr.clear();
	size_t i;
	for (i=0; i< ii.m_hostAlias.size(); i++)
		m_hostAlias.push_back( ii.m_hostAlias[i] );
	for (i=0; i< ii.m_hostAddr.size(); i++)
		m_hostAddr.push_back( ii.m_hostAddr[i] );

}


inline std::string jsInetInfo::hostName()
{
	if ( !m_loaded )
		jsVerify( false ); // , "[jsInetInfo] - Unable to load host name info");
	return m_hostName;
}

inline std::string jsInetInfo::fqHostName()
{
	if ( !m_loaded )
		jsVerify( false ); //, "[jsInetInfo] - Unable to load host name info");
	return m_dnsName;
}

//give the first numeric address
inline ULONG jsInetInfo::numAddress()
{
	if ( !m_loaded )
		jsVerify( false ); //, "[jsInetInfo] - Unable to load host name info");
	return m_hostAddr[0];
}

//give the first numeric address
inline std::string jsInetInfo::address()
{
	if ( !m_loaded || !m_sock  )
		jsVerify( false ); //, "[jsInetInfo] - Unable to load host name info");
	return m_sock->inet_ntoa( m_hostAddr[0] );
}


inline void jsInetInfo::allNumAddress( vector<ULONG>& addr )
{
	if ( !m_loaded )
		jsVerify( false ); //, "[jsInetInfo] - Unable to load host name info");

	for ( size_t i=0; i< m_hostAddr.size(); i++)
		addr.push_back( m_hostAddr[i] );
}

inline void jsInetInfo::allAddress( jsStringArray& addr )
{
	if ( !m_loaded || !m_sock )
		jsVerify( false ); //, "[jsInetInfo] - Unable to load host name info");

	for ( size_t i=0; i< m_hostAddr.size(); i++)
		addr.push_back( m_sock->inet_ntoa( m_hostAddr[i] ) );
}


inline void jsInetInfo::allAlias( jsStringArray& alias )
{
	if ( !m_loaded )
		jsVerify( false ); //, "[jsInetInfo] - Unable to load host name info");

	for ( size_t i=0; i< m_hostAlias.size(); i++)
		alias.push_back( m_hostAlias[i] );
}

inline bool jsInetInfo::getAllHostInfo( std::string hname, std::string& dnsname, jsStringArray& halias, vector<ULONG>& haddr )
{
	if ( m_loaded )
		jsVerify( false ); //, "[jsInetInfo] - host name info already loaded");
	bool st = m_sock->loadInetInfo( hname, dnsname, halias, haddr );
	return st;
}


#endif



 
 
 
 
 
