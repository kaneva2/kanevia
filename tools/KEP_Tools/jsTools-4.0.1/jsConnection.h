/******************************************************************************
 Name: jsConnection.h

 Description: socket jsConnection class definition

 Copyright (C) 1996, 2004 Jim Wallace

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 History:
 1/26/97	jmw created

 Notes: 

******************************************************************************/
#ifndef _jsConnection_h_
#define _jsConnection_h_

#ifndef _js_h_
	#error Include js.h before jsConnection.h
#endif

#include "jsSync.h"
#include "jsTime.h"
#include "jsInetInfo.h"

/******************************************************************************
 Description: jsConnection class 

 Notes:  Socket jsConnection object used for keeping information on the jsConnection.
 		 The jsConnection object is basically a encapsulation of the underlying TCP
		 Object which may be implementation dependant.All interations with the tcp sub-system
		 must be done via the jsConnection object.

	**CAUTION** I have chosen, not to make this object multi-threaded. Rather, instantiate
	a copy of the object on each thread. That way, we can be guaranteed, of
	parallel execution on multiple processors.
******************************************************************************/
class JSEXPORT jsConnection : public jsBase
{
public:
	
	enum Status 
	{ 
		Unknown =-1, 
		Ok, 
		Error, 
		ReadWouldBlock, 
		WriteWouldBlock ,
		ConnectWouldBlock, 
		Eof, 
		Close, 
		Abort,
		Timeout 
	};
	
	enum State
	{
		NEW,
		CONNECT,
		READ,
		WRITE,
		BIND,
		ABORT,
		LISTEN,
		CLOSE
	};
	

	jsConnection(); 
	jsConnection ( SOCKFD s ); 	
	~jsConnection();
	
	// Copy and assignment
	jsConnection( const jsConnection & from );
	
	jsConnection & operator =( const jsConnection & from );
	

	//Basic Tcp functions.
	inline SOCKFD socket(); 
	bool setSocket( SOCKFD sock );

	Status connect( const char* Url, UINT port, ULONG timeout = 0, bool block = true );

	inline Status read( BYTE * buffer, ULONG len, ULONG& read, ULONG timeout = 0 ); 
  
	inline Status write( const BYTE * buffer, ULONG len, ULONG& written, ULONG timeout = 0 ); 

	Status listen( USHORT backlog = 255 ); 

	inline SOCKFD accept();

	Status bind( UINT port = 0 );
	 
	Status bindUntilSuccess( UINT startPortNo = 0, UINT endPortNo = 20000  );

	Status close(); 

	Status abort();  

	//get status functions
	bool isBlocking();

	int localport();

	int peerport();

	std::string address();

	std::string peeraddress();
	
	//Set socket parameters
	inline bool setRecvBufSize( ULONG size );

	inline bool setSendBufSize( ULONG size );

	bool setNonBlocking( bool set = false ); 

	inline bool setSockParam ( bool block = true, bool linger = true, bool keepAlive = true, bool reuse = true, bool oob = true );  

	inline bool setLinger(  bool linger = true );  

	//the next five functions, can be used for watch dog jsConnection checks
	bool isCheck()	  { return m_checkCon; }

	void setCheck( bool set = true ) { m_checkCon = set; }

	jsTime lastAccess() { return m_lastAccess; }

	int getTTL() { return m_conttl; }

	void setTTL( int t ) { m_conttl = t; }

	//inline jsInetInfo inetInfo();

protected:

	// if we use jsSocketPtr here, we have to export it in the DLL
	jsSocket *			m_sock;	//actual tcp object
	
	jsTime				m_lastAccess;
	bool				m_listener;	//am i a listener?

	bool				m_checkCon;
	State				m_state;
	Status				m_status;
	int					m_conttl;

	inline Status _status( bool st );
};

/****************************************************************
		Defination of all inlines follow
*****************************************************************/

inline SOCKFD jsConnection::accept()
{
	return m_sock->accept();
}

 
inline jsConnection::Status jsConnection::read( BYTE *buffer, ULONG len, ULONG& read, ULONG timeoutSec ) 
{
	std::string ermsg;
   	m_state = jsConnection::READ;
	bool st = m_sock->receive( buffer, len, read, timeoutSec ) ;
	if ( !st )
	{
		if ( m_sock->eof() )
		{
			m_status = jsConnection::Eof;
			ermsg = "eof";
		}
		else if ( m_sock->nbcode() )
		{
			m_status = jsConnection::ReadWouldBlock;
			ermsg = "readwouldblock";
		}
		else if ( m_sock->timeout() )
		{
			m_status = jsConnection::Timeout;
			ermsg = "timeout";
		}
		else
		{
			m_status = jsConnection::Error;
			ermsg = "generic error";
		}

		#ifdef _SOCKET_DBG
		jsTrace( "jsConnection::read error msg = %s", (const char*)ermsg);
		#endif
	}
	else
		m_status = jsConnection::Ok;
	m_lastAccess = jsTime::now(); 
	return m_status; 
}

/*inline jsInetInfo jsConnection::inetInfo()
{
	jsInetInfo ii;
	return ii;
}
*/


inline SOCKFD jsConnection::socket()
{
	int t =  m_sock->socket(); 
	m_lastAccess = jsTime::now();
	return t; 
}


inline bool jsConnection::setSocket( SOCKFD sock )
{
	return m_sock->setSocket( sock );
}


inline jsConnection::Status jsConnection::write( const BYTE *buffer, ULONG len, ULONG& written, ULONG timeout  ) 
{ 
	std::string ermsg;
   	m_state = jsConnection::WRITE;
	written = 0; 
	bool st = m_sock->send( buffer, len, written, timeout ) ;
	if ( !st )
	{
		if ( m_sock->eof() )
		{
			m_status = jsConnection::Eof;
			ermsg = "eof";
		}
		else if ( m_sock->nbcode() )
		{
			m_status = jsConnection::WriteWouldBlock;
			ermsg = "writewouldblock";
		}
		else if ( m_sock->timeout() )
		{
			m_status = jsConnection::Timeout;
			ermsg = "timeout";
		}
		else
		{
			m_status = jsConnection::Error;
			ermsg = "generic error";
		}

		#ifdef _SOCKET_DBG
		jsTrace("jsConnection::write errormsg = %s", (const char*)ermsg);
		#endif
	}
	else
		m_status = jsConnection::Ok;

	m_lastAccess = jsTime::now(); 
	return m_status; 
}



inline bool jsConnection::setRecvBufSize( ULONG size )
{
	m_state = jsConnection::LISTEN;
	bool st = m_sock->setRecvBufSize( size ) ;
	m_lastAccess = jsTime::now(); 
	return st; 		
}

inline bool jsConnection::setSendBufSize( ULONG size )
{ 
	m_state = jsConnection::LISTEN;
	bool st = m_sock->setSendBufSize( size ) ;
	m_lastAccess = jsTime::now(); 
	return st; 		
}

inline std::string jsConnection::address()
{
	return m_sock->address();
}

inline std::string jsConnection::peeraddress()
{
	return jsSocket::inet_ntoa( m_sock->peeraddr() );
}

inline bool jsConnection::isBlocking() 
{
	return m_sock->isBlocking(); 
}

inline int jsConnection::localport()
{
	return m_sock->localport();
}

inline int jsConnection::peerport()
{
	return m_sock->peerport();
}


inline bool jsConnection::setLinger(
	bool linger
	)  
{
    if 	( !m_sock->setLinger( linger ) ) 
	{
		close();
		return false;
	}
	else
	{
		return true;
	}
}


inline bool jsConnection::setSockParam (
	bool block, 
	bool linger, 
	bool keepAlive, 
	bool reuse,
	bool oob
	)  
{
    if 	( 
    		!m_sock->setNonBlocking( !block ) ||  
		 ( linger &&  !m_sock->setLinger() ) ||
		 ( keepAlive && !m_sock->setKeepAlive() )  ||
		 ( reuse && !m_sock->setReuse() ) ||
		 ( oob && !m_sock->setOOB() )  
		)
	{
		close();
		return false;
	}
	else
	{
		return true;
	}
}


inline jsConnection::Status jsConnection::_status ( bool st )
{
	if ( st )
		return jsConnection::Ok;
	else
		return jsConnection::Error;

}

#endif



 
 
 
 
 
