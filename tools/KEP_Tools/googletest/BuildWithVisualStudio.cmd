@ECHO OFF

SETLOCAL EnableDelayedExpansion

SET ThisDir=%~dp0
SET BaseDir=%ThisDir%..\..
SET ScriptsDir=%BaseDir%\BuildScripts


SET ProjectName=googletest
SET SolutionToBuild="%ThisDir%msvc\gtest-md.sln"
rem SET DebugConfiguration="Debug"
rem SET ReleaseConfiguration="Release"

call %ScriptsDir%\BuildSolution.cmd %1

IF %CopyFiles% == 1 (

	call %ScriptsDir%\xcopy.cmd %ThisDir%include\*.h %IncludeDir%\ /E /Y

	call %ScriptsDir%\xcopy.cmd %ThisDir%msvc\gtest-md\Debug\gtestd.lib %LibDir%\ /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%msvc\gtest-md\Debug\gtestd.pdb %LibDir%\ /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%msvc\gtest-md\Release\gtest.lib %LibDir%\ /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%msvc\gtest-md\Release\gtest.pdb %LibDir%\ /Y
)
