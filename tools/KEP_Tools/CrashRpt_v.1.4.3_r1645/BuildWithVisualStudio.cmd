@ECHO OFF

SETLOCAL EnableDelayedExpansion

SET ThisDir=%~dp0
SET BaseDir=%ThisDir%..\..
SET ScriptsDir=%BaseDir%\BuildScripts


SET ProjectName=CrashRpt_v.1.4.3_r1645
SET SolutionToBuild="%ThisDir%CrashRpt_vs2015.sln"
rem SET DebugConfiguration="debug"
rem SET ReleaseConfiguration="release"

call %ScriptsDir%\BuildSolution.cmd %1

IF %CopyFiles% == 1 (
	call %ScriptsDir%\xcopy.cmd %ThisDir%include\CrashRpt.h %IncludeDir%\CrashRpt\ /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%lang_files\crashrpt_lang_en.ini %BinDir%\crashrpt_lang.ini*  /Y

	call %ScriptsDir%\xcopy.cmd %ThisDir%lib\CrashRpt1403.lib %LibDir%\ /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%bin\CrashRpt1403.dll %BinDir%\ /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%bin\CrashRpt1403.pdb %BinDir%\ /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%bin\CrashSender1403.exe %BinDir%\ /Y

	call %ScriptsDir%\xcopy.cmd %ThisDir%lib\CrashRpt1403d.lib %LibDir%\ /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%bin\CrashRpt1403d.dll %BinDir%\ /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%bin\CrashRpt1403d.pdb %BinDir%\ /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%bin\CrashSender1403d.exe %BinDir%\ /Y
)
