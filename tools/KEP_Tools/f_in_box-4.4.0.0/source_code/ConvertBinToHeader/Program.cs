using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Diagnostics;

namespace ConsoleApplication1
{
    class Program
    {
        const string SWITCH_BIN2H = "/bin2h";
        const string SWITCH_DEF2CPP = "/def2cpp";
        const string SWITCH_BIN2PAS = "/bin2pas";

        const string DEF_MACRO = "DEF_PROCESS_THIS";
        const string DEF_EXPORTS = "EXPORTS";

        static int Main(string[] args)
        {
            try
            {
                string strSwitch = args[0];

                switch (strSwitch)
                {
                    case SWITCH_BIN2H:
                    {
                        string strBinPath = args[1];

                        using (FileStream f = new FileStream(strBinPath, FileMode.Open, FileAccess.Read, FileShare.Read))
                        {
                            byte[] buf = new byte[64 * 1024];
                            int count;

                            char[] output = new char[f.Length * 5]; // 0xff, <-- 5 bytes
                            int pos = 0;

                            while ((count = f.Read(buf, 0, buf.Length)) > 0)
                            {
                                for (int i = 0; i < count; i++)
                                {
                                    output[pos++] = '0';
                                    output[pos++] = 'x';
                                    string b = buf[i].ToString("x");

                                    for (int n = 0; n < b.Length; n++)
                                        output[pos++] = b[n];

                                    output[pos++] = ',';
                                }
                            }

                            Console.Write(output, 0, pos);
                        }

                        break;
                    }

                    case SWITCH_DEF2CPP:
                    {
                        string strDEFPath = args[1];

                        using (FileStream f = new FileStream(strDEFPath, FileMode.Open, FileAccess.Read, FileShare.Read))
                        {
                            using (StreamReader reader = new StreamReader(f))
                            {
                                while (!reader.EndOfStream)
                                { 
                                    string line = reader.ReadLine();

                                    if (DEF_EXPORTS == line || 0 == line.Length || line.Contains(" "))
                                        continue;
                                    else
                                        Console.WriteLine(DEF_MACRO + "(" + line + ")");
                                }
                            }
                        }

                        break;
                    }
                    case SWITCH_BIN2PAS:
                    {
                        const int packetSize = 100;
                        string strBinPath = args[1];

                        using (FileStream f = new FileStream(strBinPath, FileMode.Open, FileAccess.Read, FileShare.Read))
                        {
                            byte[] buffer = new byte[4096];
                            int bytesRead = 0, n = 0,pos = 0;
                            Console.WriteLine("dll: array[0..{0}] of Byte = (", f.Length - 1);
                            char[] output = new char[f.Length * 5];                            
                            while ((bytesRead = f.Read(buffer, 0, 4096)) > 0)
                            {
                                for(int i=0;i<bytesRead;i++)
                                {
                                    if ((n+i) % packetSize == 0)
                                    {
                                        output[pos++] = '\r';
                                        output[pos++] = '\n';                                        
                                    }
                                    output[pos++] = ((n + i) == 0) ? ' ' : ',';
                                    output[pos++] = '$';
                                    string b = buffer[i].ToString("X");

                                    for (int k = 0; k < b.Length; k++)
                                        output[pos++] = b[k];                   
                                }
                                n += bytesRead;
                            }
                            Console.WriteLine(output,0,pos);
                            Console.WriteLine(");");
                        }
                        break;
                    }
                }

                return 0;
            }
            catch (Exception e)
            {
                Console.Write(e.ToString());

                return 1;
            }
        }
    }
}
