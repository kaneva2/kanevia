#ifndef __AudioClientProxy_H_
#define __AudioClientProxy_H_

namespace f_in_box
{
	HRESULT CreateAudioClientProxy(CFlashOCXLoader* pLoader, IAudioClient* pOriginalAudioClient, IAudioClient** ppProxyAudioClient);
}

#endif // !__AudioClientProxy_H_
