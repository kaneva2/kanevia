#include "stdafx.h"

ULONG WINAPI FPC_IStream_AddRef(IStream* pStream)
{
    return NULL != pStream ? pStream->AddRef() : -1;
}

ULONG WINAPI FPC_IStream_Release(IStream* pStream)
{
    return NULL != pStream ? pStream->Release() : -1;
}

HRESULT WINAPI FPC_IStream_Write(IStream* pStream, const void *pv, ULONG cb, ULONG *pcbWritten)
{
    return NULL != pStream ? pStream->Write(pv, cb, pcbWritten) : E_POINTER;
}

HRESULT WINAPI FPC_IStream_SetSize(IStream* pStream, ULONG nSize)
{
	if (NULL == pStream)
		return E_POINTER;

	ULARGE_INTEGER size;
	DEF_ZERO_IT(size);
	size.LowPart = nSize;

	return pStream->SetSize(size);
}
