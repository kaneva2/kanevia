#pragma once

namespace f_in_box
{
    typedef void (WINAPI *P_RtlZeroMemory)(LPCVOID What, SIZE_T Count);
    typedef DWORD (WINAPI *P_RtlCopyMemory)(PVOID Destination, LPCVOID Source, SIZE_T Length);
    typedef BOOL (WINAPI *P_TlsSetValue)(DWORD dwTlsIndex, LPVOID lpTlsValue);

    namespace Globals
    {
        /// Initializes global stuff
        void Init();
        /// Frees global stuff
        void Free();

        extern HANDLE g_hHeap;
        
        extern DWORD g_nTlsSlotParam1;
        extern DWORD g_nTlsSlotParam2;

        extern P_RtlCopyMemory g_pRtlCopyMemory;
        extern P_RtlZeroMemory g_pRtlZeroMemory;

        extern P_TlsSetValue g_pTlsSetValue;
    }
}
