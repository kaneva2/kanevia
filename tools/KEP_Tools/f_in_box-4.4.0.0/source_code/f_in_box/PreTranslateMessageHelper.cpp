#include "stdafx.h"
#include "PreTranslateMessageHelper.h"
#include "MemManager.h"
#include "thunks.h"
#include "Globals.h"

using namespace f_in_box;

CPreTranslateMessageHelper::CPreTranslateMessageHelper() : 
    m_hHook(NULL), 
    m_lpAdapterCode(NULL)
{
}

void CPreTranslateMessageHelper::Start()
{
    // Create adapter
    m_lpAdapterCode = AllocThunkWithOneParam(&StaticGetMsgProc, this);

    // Set hook
    m_hHook = SetWindowsHookEx(WH_GETMESSAGE, (HOOKPROC)m_lpAdapterCode, NULL, GetCurrentThreadId());
}

void CPreTranslateMessageHelper::Stop()
{
    if (NULL != m_hHook)
	{
        UnhookWindowsHookEx(m_hHook);
		m_hHook = NULL;
	}

    if (NULL != m_lpAdapterCode)
	{
        VirtualFree(m_lpAdapterCode, 0, MEM_RELEASE);
		m_lpAdapterCode = NULL;
	}
}

CPreTranslateMessageHelper::~CPreTranslateMessageHelper()
{
	Stop();
}

LRESULT
CALLBACK
CPreTranslateMessageHelper::StaticGetMsgProc(
    int code,       // hook code
    WPARAM wParam,  // removal option
    LPARAM lParam   // message
)
{
    CPreTranslateMessageHelper* pThis = (CPreTranslateMessageHelper*)TlsGetValue(Globals::g_nTlsSlotParam1);
    ASSERT(NULL != pThis);

    if (HC_ACTION == code && PM_REMOVE == wParam)
        // If code is HC_ACTION, the hook procedure must process the message
    {
        MSG* pMsg = reinterpret_cast<MSG*>(lParam);

        pThis->PreTranslateMessageHelper_PreTranslateMessage(pMsg);
    }

    return CallNextHookEx(pThis->m_hHook, code, wParam, lParam);
}

void CPreTranslateMessageHelper::PreTranslateMessageHelper_PreTranslateMessage(MSG* pMsg)
{
}
