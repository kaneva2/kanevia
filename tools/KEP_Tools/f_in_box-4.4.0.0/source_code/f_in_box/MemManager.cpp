#include "stdafx.h"
#include "MemManager.h"
#include "Globals.h"

using namespace f_in_box;

#ifndef _LIB

#ifdef _DEBUG
//#define DEF_LOG_ALLOCATIONS 1
//#define DEF_USE_VIRTUAL_MEM 1
//#define DEF_USE_LOCAL 1
#endif // _DEBUG

void __cdecl operator delete(void* p)
{
    if (NULL != p)
    {
#ifdef DEF_USE_VIRTUAL_MEM
        VirtualFree(p, 0, MEM_RELEASE);
#elif DEF_USE_LOCAL
		LocalFree((HLOCAL)p);
#else
        HeapFree(Globals::g_hHeap /* GetProcessHeap() */, 0, p);
#endif // DEF_USE_VIRTUAL_MEM

#ifdef DEF_LOG_ALLOCATIONS

        TCHAR szBuffer[1024];
        wsprintf(szBuffer, _T("Freed: 0x%.8x\n"), p);
        OutputDebugString(szBuffer);

#endif // DEF_LOG_ALLOCATIONS
	}
}

void __cdecl operator delete[](void* p)
{
    if (NULL != p)
    {
#ifdef DEF_USE_VIRTUAL_MEM
        VirtualFree(p, 0, MEM_RELEASE);
#elif DEF_USE_LOCAL
		LocalFree((HLOCAL)p);
#else
        HeapFree(Globals::g_hHeap /* GetProcessHeap() */, 0, p);
#endif // DEF_USE_VIRTUAL_MEM

#ifdef DEF_LOG_ALLOCATIONS

        TCHAR szBuffer[1024];
        wsprintf(szBuffer, _T("Freed: 0x%.8x\n"), p);
        OutputDebugString(szBuffer);

#endif // DEF_LOG_ALLOCATIONS
	}
}

void* __cdecl operator new[](size_t nCount)
{
	LPVOID p = 

#ifdef DEF_USE_VIRTUAL_MEM
        VirtualAlloc(NULL, nCount, MEM_COMMIT, PAGE_READWRITE);
#elif DEF_USE_LOCAL
		(LPVOID)LocalAlloc(LPTR, nCount);
#else
        HeapAlloc(Globals::g_hHeap /* GetProcessHeap() */, 0, nCount);
#endif // DEF_USE_VIRTUAL_MEM

#ifdef DEF_LOG_ALLOCATIONS

    TCHAR szBuffer[1024];
    wsprintf(szBuffer, _T("Allocated %d bytes at 0x%.8x\n"), nCount, p);
    OutputDebugString(szBuffer);

#endif // DEF_LOG_ALLOCATIONS

    return p;
}

void* __cdecl operator new(size_t nCount)
{
	LPVOID p = 

#ifdef DEF_USE_VIRTUAL_MEM
        VirtualAlloc(NULL, nCount, MEM_COMMIT, PAGE_READWRITE);
#elif DEF_USE_LOCAL
		(LPVOID)LocalAlloc(LPTR, nCount);
#else
        HeapAlloc(Globals::g_hHeap /* GetProcessHeap() */, 0, nCount);
#endif // DEF_USE_VIRTUAL_MEM

#ifdef DEF_LOG_ALLOCATIONS

    TCHAR szBuffer[1024];
    wsprintf(szBuffer, _T("Allocated %d bytes at 0x%.8x\n"), nCount, p);
    OutputDebugString(szBuffer);

#endif // DEF_LOG_ALLOCATIONS

    return p;
}

#endif // !_LIB

namespace f_in_box
{
    void MemoryZero(LPVOID p, SIZE_T nSize)
    {
        return Globals::g_pRtlZeroMemory(p, nSize);
    }

    void MemoryCopy(LPVOID pTo, LPCVOID pFrom, SIZE_T nSize)
    {
        Globals::g_pRtlCopyMemory(pTo, pFrom, nSize);
    }

    int MemoryCompare(LPCVOID p1, LPCVOID p2, SIZE_T nSize)
    {
        LPBYTE lpBuf1 = (LPBYTE)p1;
        LPBYTE lpBuf2 = (LPBYTE)p2;

        for (SIZE_T i = 0; i < nSize; i++, lpBuf1++, lpBuf2++)
            if (*lpBuf1 < *lpBuf2)
                return -1;
            else if (*lpBuf1 > *lpBuf2)
                return +1;

        return 0;
    }
}

#ifndef _LIB

extern "C" int _fltused = 0;

extern "C" int __cdecl _purecall(void)
{
    ASSERT(FALSE);
    return 0;
}

#if (_MSC_VER == 1400)
#ifdef _DEBUG

void* __cdecl memset( void *dest, int c, size_t count )
{
   LPBYTE lp = (LPBYTE)dest;

   for (DWORD i = 0; i < count; i++, lp++)
      *lp = c;

   return dest;
}

#endif // _DEBUG
#endif // (_MSC_VER == 1400)

#endif // !_LIB
