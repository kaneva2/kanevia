// Copyright (c) Softanics

#include <windows.h>

namespace f_in_box
{
    BYTE g_f_in_box_content[] = 
    {
#include "f_in_box_content.h"
    };

    DWORD g_f_in_box_content_size = sizeof(g_f_in_box_content);
}
