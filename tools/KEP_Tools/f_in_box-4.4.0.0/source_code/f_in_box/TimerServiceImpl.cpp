#include "stdafx.h"
#include "TimerService.h"
#include "TimerServiceImpl.h"
#include "ObjectWithCounter.h"
#include "ComPtr.h"
#include "CriticalSection.h"
#include "Map.h"
#include "thunks.h"
#include "Globals.h"

using namespace f_in_box::std;
using namespace f_in_box::com_helpers;

namespace f_in_box
{
    HRESULT CreateTimerImpl(REFIID iid, void** ppObject);

    /// Contains information about timer settings
    class CTimerSinkData : public CObjectWithCounter
    {
        CComPtr<ITimerSink> m_pTimerSink;

        DWORD m_dwMinTime;
        DWORD m_dwMaxTime;
        DWORD m_dwInterval;
        DWORD m_dwStartGetTickCountTime;

        PVOID m_pTimerProcThunk;

        UINT_PTR m_nTimerEventId;

    private:
        CTimerSinkData(ITimerSink* pTimerSink, DWORD dwMinTime, DWORD dwMaxTime, DWORD dwInterval) :
            m_pTimerSink(pTimerSink),
            m_dwMinTime(dwMinTime),
            m_dwMaxTime(dwMaxTime),
            m_dwInterval(dwInterval),
            m_dwStartGetTickCountTime(0),
            m_pTimerProcThunk(NULL),
            m_nTimerEventId(0)
        {
        }

        ~CTimerSinkData()
        {
            if (NULL != m_pTimerProcThunk)
                VirtualFree(m_pTimerProcThunk, 0, MEM_RELEASE);
        }

    public:
        static CComPtr<CTimerSinkData> Create(ITimerSink* pTimerSink, DWORD dwMinTime, DWORD dwMaxTime, DWORD dwInterval)
        {
            CComPtr<CTimerSinkData> pTimerSinkData = new CTimerSinkData(pTimerSink, dwMinTime, dwMaxTime, dwInterval);
            return pTimerSinkData;
        }

        void Start()
        {
            m_dwStartGetTickCountTime = GetTickCount();
            m_pTimerProcThunk = AllocThunkWithOneParam(&StaticTimerProc, this);
            m_nTimerEventId = SetTimer(NULL, 0, m_dwInterval, (TIMERPROC)m_pTimerProcThunk);
        }

        void Stop()
        {
            // TODO: check if really started?
            KillTimer(NULL, m_nTimerEventId);
        }

    private:
        static VOID CALLBACK StaticTimerProc(HWND hWnd, UINT uMsg, UINT_PTR idEvent, DWORD dwTime)
        {
            CTimerSinkData* pThis = (CTimerSinkData*)TlsGetValue(Globals::g_nTlsSlotParam1);
            ASSERT(NULL != pThis);

            pThis->TimerProc(hWnd, uMsg, idEvent, dwTime);
        }

        VOID CALLBACK TimerProc(HWND hWnd, UINT uMsg, UINT_PTR idEvent, DWORD dwTime)
        {
            DWORD dwElapsedTime = GetTickCount() - m_dwStartGetTickCountTime;

            VARIANT time;
            time.vt = VT_UI4;
            time.uintVal = dwElapsedTime;

            if (0 == m_dwInterval)
                // One time timer
            {
                if (dwElapsedTime >= m_dwMinTime)
                {
                    m_pTimerSink->OnTimer(time);
                    KillTimer(NULL, idEvent);
                }
            }
            else if (0 == m_dwMaxTime)
                // The timer never elapsed
            {
                if (dwElapsedTime >= m_dwMinTime)
                    m_pTimerSink->OnTimer(time);
            }
            else
                // Timer from min time to max time
            {
                if (dwElapsedTime >= m_dwMinTime)
                    m_pTimerSink->OnTimer(time);
                else if (dwElapsedTime <= m_dwMaxTime)
                    KillTimer(NULL, idEvent);
            }
        }
    };

    /// ITimer implementation
    class CTimerImpl :
        public ITimer,
		public CObjectWithCounter
    {
    private:
        /// To lock collections
        CCriticalSection m_cs;

        /// A number of adised ITimerSinks: to generate a cookie in Advise()
        DWORD m_nTimerSinkCount;

        /// A map to find sink data by cookie (see Advise())
        CMap<DWORD, CComPtr<CTimerSinkData> > m_TimerSinksByCookie;

	public:
        CTimerImpl() :
            m_nTimerSinkCount(0)
		{
		}

        virtual ULONG __stdcall AddRef()
        {
			return CObjectWithCounter::AddRef();
        }

        virtual ULONG __stdcall Release()
        {
			return CObjectWithCounter::Release();
        }

		HRESULT STDMETHODCALLTYPE QueryInterface(REFIID riid, void** ppvObject)
		{
            DEF_BEGIN_INTERFACE_MAP()
                DEF_DECLARE_INTERFACE(ITimer)
            DEF_END_INTERFACE_MAP()
		}

        // ITimer

        HRESULT STDMETHODCALLTYPE Advise(
            VARIANT vtimeMin,
            VARIANT vtimeMax,
            VARIANT vtimeInterval,
            DWORD dwFlags,
            ITimerSink* pTimerSink,
            DWORD* pdwCookie)
        {
            CCriticalSectionOwner lock(m_cs);

            m_nTimerSinkCount++;

            CComPtr<CTimerSinkData> pTimerSinkData = CTimerSinkData::Create(pTimerSink, vtimeMin.uintVal, vtimeMax.uintVal, vtimeInterval.uintVal);

            DWORD dwCookie = m_nTimerSinkCount++;
            m_TimerSinksByCookie.Add(dwCookie, pTimerSinkData);

            pTimerSinkData->Start();

            *pdwCookie = dwCookie;

            return S_OK;
        }

        HRESULT STDMETHODCALLTYPE Unadvise(DWORD dwCookie)
        {
            CCriticalSectionOwner lock(m_cs);

            CComPtr<CTimerSinkData> pTimerSinkData;
            if (!m_TimerSinksByCookie.Find(dwCookie, pTimerSinkData))
            {
                ASSERT(FALSE);
                return E_INVALIDARG;
            }
            else
            {
                pTimerSinkData->Stop();
                return S_OK;
            }
        }

        HRESULT STDMETHODCALLTYPE Freeze(BOOL fFreeze)
        {
            return S_OK;
        }

        HRESULT STDMETHODCALLTYPE GetTime(VARIANT *pvtime)
        {
            pvtime->vt = VT_UI4;
            pvtime->uintVal = 0;

            return S_OK;
        }
    };

    class CTimerSinkImpl :
        public ITimerSink,
		public CObjectWithCounter
    {
        CComPtr<ITimerSink> m_pOldSink;

	public:
        CTimerSinkImpl(ITimerSink* pOldSink) : m_pOldSink(pOldSink)
		{
		}

        virtual ULONG __stdcall AddRef()
        {
			return CObjectWithCounter::AddRef();
        }

        virtual ULONG __stdcall Release()
        {
			return CObjectWithCounter::Release();
        }

		HRESULT STDMETHODCALLTYPE QueryInterface(REFIID riid, void** ppvObject)
		{
            DEF_BEGIN_INTERFACE_MAP()
                DEF_DECLARE_INTERFACE(ITimerSink)
            DEF_END_INTERFACE_MAP()
		}

        // ITimerSink

        HRESULT STDMETHODCALLTYPE OnTimer(VARIANT vtimeAdvise)
        {
            return m_pOldSink->OnTimer(vtimeAdvise);
        }

        HRESULT STDMETHODCALLTYPE GetNamedTimer(REFGUID rguidName, ITimer** ppTimer)
        {
            ASSERT(FALSE);
            return E_NOTIMPL;
        }

        HRESULT STDMETHODCALLTYPE SetNamedTimerReference(REFGUID rguidName, ITimer* pReferenceTimer)
        {
            ASSERT(FALSE);
            return E_NOTIMPL;
        }
    };

    class CTimerServiceImpl :
        public ITimerService,
		public CObjectWithCounter
    {
	public:
		CTimerServiceImpl()
		{
		}

        virtual ULONG __stdcall AddRef()
        {
			return CObjectWithCounter::AddRef();
        }

        virtual ULONG __stdcall Release()
        {
			return CObjectWithCounter::Release();
        }

		HRESULT STDMETHODCALLTYPE QueryInterface(REFIID riid, void** ppvObject)
		{
            DEF_BEGIN_INTERFACE_MAP()
                DEF_DECLARE_INTERFACE(ITimerService)
            DEF_END_INTERFACE_MAP()
		}

        // ITimerService

        HRESULT STDMETHODCALLTYPE CreateTimer(ITimer* pReferenceTimer, ITimer** ppNewTimer)
        {
            // Actually I don't know what to do with pReferenceTimer, but I expect that it is NULL,
            // so we don't need it; if it is not NULL, let's try to understand what does Flash want
            ASSERT(NULL == pReferenceTimer);

            return CreateTimerImpl(IID_ITimer, (void**)ppNewTimer);
        }

        HRESULT STDMETHODCALLTYPE GetNamedTimer(REFGUID rguidName, ITimer** ppTimer)
        {
            ASSERT(FALSE);
            return E_NOTIMPL;
        }

        HRESULT STDMETHODCALLTYPE SetNamedTimerReference(REFGUID rguidName, ITimer* pReferenceTimer)
        {
            ASSERT(FALSE);
            return E_NOTIMPL;
        }
    };

    HRESULT CreateTimerServiceImpl(REFIID iid, void** ppObject)
    {
        CComPtr<CTimerServiceImpl> pTimerServiceImpl = new CTimerServiceImpl();
        return pTimerServiceImpl->QueryInterface(iid, ppObject);
    }

    HRESULT CreateTimerSinkImpl(ITimerSink* pOldSink, REFIID iid, void** ppObject)
    {
        CComPtr<CTimerSinkImpl> pTimerSinkImpl = new CTimerSinkImpl(pOldSink);
        return pTimerSinkImpl->QueryInterface(iid, ppObject);
    }

    HRESULT CreateTimerImpl(REFIID iid, void** ppObject)
    {
        CComPtr<CTimerImpl> pTimerImpl = new CTimerImpl();
        return pTimerImpl->QueryInterface(iid, ppObject);
    }
}
