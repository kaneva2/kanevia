#ifndef __MEM_MANAGER_H__F5B21EFD_BBB7_481b_9F35_815DB28AA8DD__
#define __MEM_MANAGER_H__F5B21EFD_BBB7_481b_9F35_815DB28AA8DD__

#ifndef _LIB

void* __cdecl operator new(size_t nCount);
void* __cdecl operator new[](size_t nCount);
void __cdecl operator delete(void* p);
void __cdecl operator delete[](void* p);

#endif // !_LIB

namespace f_in_box
{
    void MemoryZero(LPVOID p, SIZE_T nSize);
    void MemoryCopy(LPVOID pTo, LPCVOID pFrom, SIZE_T nSize);
    int MemoryCompare(LPCVOID p1, LPCVOID p2, SIZE_T nSize);
}

#endif // !__MEM_MANAGER_H__F5B21EFD_BBB7_481b_9F35_815DB28AA8DD__
