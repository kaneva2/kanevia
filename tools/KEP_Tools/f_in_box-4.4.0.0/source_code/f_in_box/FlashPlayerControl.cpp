#include "stdafx.h"
#include "CriticalSection.h"
#include "f_in_box.h"
#include "FlashOCXLoader.h"
#include "FlashInterfaces.h"
#include "ComPtr.h"
#include "BSTR.h"
#include "User32Wrapper.h"
#include "FPCWnd.h"
#include "Globals.h"

using namespace f_in_box;
using namespace f_in_box::com_helpers;

//=====================================================================================================
// Global variables

HINSTANCE g_hInstance = NULL;

HFPC g_FPC = NULL;

#ifdef F_IN_BOX_EXPORTS

//=====================================================================================================
// DLL entry point
BOOL APIENTRY DllMain(HINSTANCE hInstance,
                      DWORD  ul_reason_for_call,
                      LPVOID lpReserved)
{
    g_hInstance = hInstance;

#ifndef _LIB
	switch (ul_reason_for_call)
	{
		case DLL_PROCESS_ATTACH: 
		{
            Globals::Init();
			break;
		}
		case DLL_PROCESS_DETACH: 
		{
			FPC_SetGlobalOption(DEF_FINBOX_GLOBAL_OPTION__MINIMIZE_MEMORY, FALSE);

            Globals::Free();

			break;
		}
	}
#endif // !_LIB

    return TRUE;
}
//=====================================================================================================

#endif // F_IN_BOX_EXPORTS

BOOL WINAPI InternalRegisterFlashWindowClass(LPVOID lpFlashOCXCodeData = NULL, DWORD dwSizeOfFlashOCXCode = 0)
{
    if (NULL != g_FPC)
        // Already registered
        return FALSE;

    CComPtr<CFlashOCXLoader> pFlashOCXLoader = CFlashOCXLoader::Create();
    if (pFlashOCXLoader.IsNull())
    {
        ASSERT(FALSE);
        return FALSE;
    }

    HRESULT hr = pFlashOCXLoader->Load(lpFlashOCXCodeData, dwSizeOfFlashOCXCode, WC_FLASHA, WC_FLASHW);
    if (S_OK != hr)
    {
        ASSERT(FALSE);
        return FALSE;
    }

    g_FPC = (HFPC)pFlashOCXLoader.Detach();

    return TRUE;
}

BOOL WINAPI RegisterFlashWindowClass()
{
    return InternalRegisterFlashWindowClass();
}

BOOL WINAPI RegisterFlashWindowClassEx(LPVOID lpFlashOCXCodeData, DWORD dwSizeOfFlashOCXCode)
{
    return InternalRegisterFlashWindowClass(lpFlashOCXCodeData, dwSizeOfFlashOCXCode);
}

void WINAPI UnregisterFlashWindowClass()
{
    if (NULL != g_FPC)
    {
        FPC_UnloadCode(g_FPC);

        g_FPC = NULL;
    }
}
//=====================================================================================================

//=====================================================================================================
// Get installed flash version
BOOL WINAPI GetInstalledFlashVersionEx(SFPCVersion* pVersion)
{
	BOOL bRes = FALSE;

    HKEY hKey;

    if (ERROR_SUCCESS == 
        RegOpenKeyExA(HKEY_CLASSES_ROOT, 
                        "CLSID\\{D27CDB6E-AE6D-11cf-96B8-444553540000}\\InprocServer32", 
                        0, 
                        KEY_READ, 
                        &hKey))
        // OK
    {
        DWORD dwSizeOfPath;

        if (ERROR_SUCCESS == RegQueryValueExA(hKey, "", NULL, NULL, NULL, &dwSizeOfPath))
        {
            LPSTR lpszFlashOCXPath = new CHAR[dwSizeOfPath];

            if (ERROR_SUCCESS == RegQueryValueExA(hKey, "", NULL, NULL, (LPBYTE)lpszFlashOCXPath, &dwSizeOfPath))
            {
                HMODULE hModule = LoadLibraryExA(lpszFlashOCXPath, NULL, LOAD_LIBRARY_AS_DATAFILE);

                if (NULL != hModule)
                {
                    HRSRC hRsrc = FindResource(hModule, MAKEINTRESOURCE(1), RT_VERSION);
                    HGLOBAL hGlobal = LoadResource(hModule, hRsrc);
                    LPVOID lpResDataOfVersion = LockResource(hGlobal);

                    if (NULL != lpResDataOfVersion)
                    {
						// Please see about VS_VERSIONINFO in MSDN
						VS_FIXEDFILEINFO* pFixedFileInfo = (VS_FIXEDFILEINFO*)((DWORD)lpResDataOfVersion + sizeof(WORD) * 19);

						while (0 == *(WORD*)pFixedFileInfo)
							pFixedFileInfo = (VS_FIXEDFILEINFO*)((DWORD)pFixedFileInfo + sizeof(WORD));

						DWORD dwMinorPart = pFixedFileInfo->dwProductVersionLS;
						DWORD dwMajorPart = pFixedFileInfo->dwProductVersionMS;

						pVersion->v[3] = (WORD)(dwMajorPart >> 16);
						pVersion->v[2] = (WORD)(dwMajorPart & 0xffff);
						pVersion->v[1] = (WORD)(dwMinorPart >> 16);
						pVersion->v[0] = (WORD)(dwMinorPart & 0xffff);

						bRes = TRUE;
                    }
                }

                FreeLibrary(hModule);
            }

            delete[] lpszFlashOCXPath;
        }

        RegCloseKey(hKey);
    }

    return bRes;
}
//=====================================================================================================

BOOL WINAPI GetUsingFlashVersionEx(SFPCVersion* pVersion)
{
    return NULL != g_FPC ? FPC_GetVersionEx(g_FPC, pVersion) : 0;
}

HMODULE WINAPI FPC_Internal_GetFlashOCXHandle()
{
    return NULL != g_FPC ? ((CFlashOCXLoader*)g_FPC)->GetHINSTANCE() : 0;
}

HRESULT WINAPI FPCCallFunctionBSTR(HWND hwndFlashPlayerControl, BSTR bstrRequest, BSTR* bstrResponse)
{
    SFPCCallFunction info; DEF_ZERO_IT(info);

    info.bstrRequest = bstrRequest;

    ::SendMessage(hwndFlashPlayerControl, FPCM_CALL_FUNCTION, 0, (LPARAM)&info);

    *bstrResponse = info.bstrResponse;

    return info.hr;
}

template <class CHAR_TYPE>
HRESULT WINAPI FPCCallFunctionAW(HWND hwndFlashPlayerControl, const CHAR_TYPE* lpszRequest, CHAR_TYPE* lpszResponse, DWORD* pdwResponseLength)
{
    f_in_box::com_helpers::CBSTR bstrResponse;

    HRESULT hr;
    
    hr = FPCCallFunctionBSTR(hwndFlashPlayerControl, f_in_box::com_helpers::CBSTR(lpszRequest), bstrResponse.GetAddress());

    if (S_OK != hr)
        return hr;

	if (NULL == lpszResponse)
	{
		if (NULL != pdwResponseLength)
			*pdwResponseLength = bstrResponse.length() + 1;
	}
	else
	{
		if (NULL == pdwResponseLength)
			f_in_box::MemoryCopy(lpszResponse, 
					(CHAR_TYPE*)bstrResponse, 
					(bstrResponse.length() + 1) * sizeof(CHAR_TYPE));
		else
		{
			*pdwResponseLength = 
				min(bstrResponse.length() + 1, *pdwResponseLength);

			f_in_box::MemoryCopy(lpszResponse, 
					(CHAR_TYPE*)bstrResponse, 
					(*pdwResponseLength) * sizeof(CHAR_TYPE));
		}
	}

	return hr;
}

HRESULT WINAPI FPCCallFunctionA(HWND hwndFlashPlayerControl, LPCSTR lpszRequest, LPSTR lpszResponse, DWORD* pdwResponseLength)
{
	return FPCCallFunctionAW(hwndFlashPlayerControl, lpszRequest, lpszResponse, pdwResponseLength);
}

HRESULT WINAPI FPCCallFunctionW(HWND hwndFlashPlayerControl, LPCWSTR lpszRequest, LPWSTR lpszResponse, DWORD* pdwResponseLength)
{
	return FPCCallFunctionAW(hwndFlashPlayerControl, lpszRequest, lpszResponse, pdwResponseLength);
}

HRESULT WINAPI FPCSetReturnValueA(HWND hwndFlashPlayerControl, const CHAR* lpszReturnValue)
{
    SFPCSetReturnValueA info; DEF_ZERO_IT(info);

    info.lpszReturnValue = lpszReturnValue;

    ::SendMessage(hwndFlashPlayerControl, FPCM_SET_RETURN_VALUEA, 0, (LPARAM)&info);

    return info.hr;
}

HRESULT WINAPI FPCSetReturnValueW(HWND hwndFlashPlayerControl, const WCHAR* lpszReturnValue)
{
    SFPCSetReturnValueW info; DEF_ZERO_IT(info);

    info.lpszReturnValue = lpszReturnValue;

    ::SendMessage(hwndFlashPlayerControl, FPCM_SET_RETURN_VALUEW, 0, (LPARAM)&info);

    return info.hr;
}

HRESULT WINAPI FPC_PutStandardMenu(HWND hwndFlashPlayerControl, BOOL bEnable)
{
	SFPCPutStandardMenu FPCPutStandardMenu;

	FPCPutStandardMenu.StandardMenu = bEnable ? VARIANT_TRUE : VARIANT_FALSE;

	::SendMessage(hwndFlashPlayerControl, FPCM_PUT_STANDARD_MENU, 0, (LPARAM)&FPCPutStandardMenu);

	return S_OK;
}

HRESULT WINAPI FPC_GetStandardMenu(HWND hwndFlashPlayerControl, BOOL* pbEnable)
{
	if (NULL == pbEnable)
		return E_POINTER;

	SFPCGetStandardMenu FPCGetStandardMenu;

	::SendMessage(hwndFlashPlayerControl, FPCM_GET_STANDARD_MENU, 0, (LPARAM)&FPCGetStandardMenu);

	*pbEnable = VARIANT_FALSE != FPCGetStandardMenu.StandardMenu;

	return S_OK;
}

HRESULT WINAPI FPC_PutAudioVolume(DWORD dwVolume)
{
    if (NULL == g_FPC)
        return E_FAIL;

    return FPC_SetSoundVolume(g_FPC, dwVolume);
}

HRESULT WINAPI FPC_GetAudioVolume(DWORD* pdwVolume)
{
    if (NULL == g_FPC)
        return E_FAIL;

    *pdwVolume = FPC_GetSoundVolume(g_FPC);

    return S_OK;
}

void WINAPI FPCSetAudioEnabled(BOOL bEnable)
{
    if (NULL == g_FPC)
        return;

    FPC_EnableSound(g_FPC, bEnable);
}

BOOL WINAPI FPCGetAudioEnabled()
{
    if (NULL == g_FPC)
        return FALSE;

    return FPC_IsSoundEnabled(g_FPC);
}

static void GetResourceDataAndSizeA(HINSTANCE hInstance, 
								    LPCSTR lpResName, 
									LPCSTR lpResType, 
									LPVOID& lpData, 
									DWORD& dwSize)
{
	lpData = NULL;
	dwSize = 0;

	HRSRC hRSRC = FindResourceA(hInstance, lpResName, lpResType);

	if (NULL == hRSRC)
		return;

	HGLOBAL hGlobal = LoadResource(hInstance, hRSRC);

	if (NULL == hGlobal)
		return;

    lpData = LockResource(hGlobal);
    dwSize = SizeofResource(hInstance, hRSRC);
}

static void GetResourceDataAndSizeW(HINSTANCE hInstance, 
								    LPCWSTR lpResName, 
									LPCWSTR lpResType, 
									LPVOID& lpData, 
									DWORD& dwSize)
{
	lpData = NULL;
	dwSize = 0;

	HMODULE hModule__Kernel32 = GetModuleHandle(_T("kernel32.dll"));

	if (NULL == hModule__Kernel32)
		return;

	typedef HRSRC (WINAPI *PFINDRESOURCEW)(HMODULE hModule, LPCWSTR lpName, LPCWSTR lpType);
	PFINDRESOURCEW pFindResourceW = NULL;
	(FARPROC&)pFindResourceW = GetProcAddress(hModule__Kernel32, "FindResourceW");

	HRSRC hRSRC;

	if (NULL != pFindResourceW)
		hRSRC = pFindResourceW(hInstance, lpResName, lpResType);
	else
        hRSRC = FindResourceA(hInstance, 
                              f_in_box::std::WToCA(lpResName).c_str(), 
                              f_in_box::std::WToCA(lpResType).c_str());

	if (NULL == hRSRC)
		return;

	HGLOBAL hGlobal = LoadResource(hInstance, hRSRC);

	if (NULL == hGlobal)
		return;

    lpData = LockResource(hGlobal);
    dwSize = SizeofResource(hInstance, hRSRC);
}

BOOL WINAPI FPCLoadMovieFromMemory(HWND hwndFlashPlayerControl, int layer, LPVOID lpData, DWORD dwSize)
{
	if (NULL == hwndFlashPlayerControl)
		return FALSE;

	SFPCLoadMovieFromMemory info; DEF_ZERO_IT(info);

	info.layer = layer;
	info.lpData = lpData;
	info.dwSize = dwSize;

	::SendMessage(hwndFlashPlayerControl, FPCM_LOADMOVIEFROMMEMORY, 0, (LPARAM)&info);

	return TRUE;
}

BOOL WINAPI FPCPutMovieFromMemory(HWND hwndFlashPlayerControl, LPVOID lpData, DWORD dwSize)
{
	if (NULL == hwndFlashPlayerControl)
		return FALSE;

	SFPCPutMovieFromMemory info; DEF_ZERO_IT(info);

	info.lpData = lpData;
	info.dwSize = dwSize;

	::SendMessage(hwndFlashPlayerControl, FPCM_PUTMOVIEFROMMEMORY, 0, (LPARAM)&info);

	return TRUE;
}

BOOL WINAPI FPCLoadMovieFromResourceA(HWND hwndFlashPlayerControl, int layer, HINSTANCE hInstance, LPCSTR lpName, LPCSTR lpType)
{
	if (NULL == hwndFlashPlayerControl)
		return FALSE;

	LPVOID lpData = NULL;
	DWORD dwSize = 0;

	GetResourceDataAndSizeA(hInstance, lpName, lpType, lpData, dwSize);

	if (NULL == lpData)
		return FALSE;

	return FPCLoadMovieFromMemory(hwndFlashPlayerControl, layer, lpData, dwSize);
}

BOOL WINAPI FPCLoadMovieFromResourceW(HWND hwndFlashPlayerControl, int layer, HINSTANCE hInstance, LPCWSTR lpName, LPCWSTR lpType)
{
	if (NULL == hwndFlashPlayerControl)
		return FALSE;

	LPVOID lpData = NULL;
	DWORD dwSize = 0;

	GetResourceDataAndSizeW(hInstance, lpName, lpType, lpData, dwSize);

	if (NULL == lpData)
		return FALSE;

	return FPCLoadMovieFromMemory(hwndFlashPlayerControl, layer, lpData, dwSize);
}

BOOL WINAPI FPCPutMovieFromResourceA(HWND hwndFlashPlayerControl, HINSTANCE hInstance, LPCSTR lpName, LPCSTR lpType)
{
	if (NULL == hwndFlashPlayerControl)
		return FALSE;

	LPVOID lpData = NULL;
	DWORD dwSize = 0;

	GetResourceDataAndSizeA(hInstance, lpName, lpType, lpData, dwSize);

	if (NULL == lpData)
		return FALSE;

	return FPCPutMovieFromMemory(hwndFlashPlayerControl, lpData, dwSize);
}

BOOL WINAPI FPCPutMovieFromResourceW(HWND hwndFlashPlayerControl, HINSTANCE hInstance, LPCWSTR lpName, LPCWSTR lpType)
{
	if (NULL == hwndFlashPlayerControl)
		return FALSE;

	LPVOID lpData = NULL;
	DWORD dwSize = 0;

	GetResourceDataAndSizeW(hInstance, lpName, lpType, lpData, dwSize);

	if (NULL == lpData)
		return FALSE;

	return FPCPutMovieFromMemory(hwndFlashPlayerControl, lpData, dwSize);
}

BOOL WINAPI FPCIsTransparentAvailable()
{
	BOOL bRes;

	HDC hdcScreen = CreateDC(_T("DISPLAY"), NULL, NULL, NULL);

	bRes = FALSE;

    SUser32Wrapper User32Wrapper;

	if (GetDeviceCaps(hdcScreen, BITSPIXEL) >= 16)
		if (NULL != User32Wrapper.UpdateLayeredWindow)
			bRes = TRUE;

	DeleteDC(hdcScreen);

	return bRes;
}

BOOL WINAPI FPCIsFlashInstalled()
{
	BOOL bRes;

	CoInitialize(NULL);
	{
        f_in_box::com_helpers::CComPtr<IUnknown> pUnknown;
        pUnknown.CoCreateInstance(f_in_box::flash::GetFlashObjectGUID(), IID_IUnknown);
        bRes = pUnknown.IsNotNull();
	}
	CoUninitialize();

	return bRes;
}

BOOL WINAPI FPCSetEventListener(HWND hwndFlashPlayerControl, PFLASHPLAYERCONTROLEVENTLISTENER pListener, LPARAM lParam)
{
	DEF_PRIVATE_MESSAGE__SET_EVENT_LISTENER_Data info;

	info.lParam = lParam;
	info.pListener = pListener;

	::SendMessage(hwndFlashPlayerControl, 
				  DEF_PRIVATE_MESSAGE__SET_EVENT_LISTENER, 
				  0, 
				  (LPARAM)&info);

	return TRUE;
}

BOOL WINAPI FPC_Internal_HookFunc(LPCSTR lpszDLLName, LPCSTR lpszFuncName, LPVOID* pOldFunc, LPVOID lpNewFunc)
{
    // TODO: implement it :)

    return FALSE;
}

HRESULT WINAPI HandlerThunk(LPCSTR lpszURL, IStream** ppStream, HFPC hFPC, LPARAM lParam)
{
    return ((PLOADEXTERNALRESOURCEHANDLER)lParam)(lpszURL, ppStream);
}

void WINAPI FPCSetGlobalOnLoadExternalResourceHandler(PLOADEXTERNALRESOURCEHANDLER pHandler)
{
    if (NULL != g_FPC)
    {
        CContentManager& ContentManager = ((CFlashOCXLoader*)g_FPC)->GetContentManager();
        ContentManager.AddOnLoadExternalResourceHandlerA(&HandlerThunk, (LPARAM)pHandler);
    }
}

void WINAPI FPC_SetContext(HWND hWnd, LPCSTR lpszContext)
{


    SendMessage(hWnd, RegisterWindowMessageA(DEF_PRIVATE_MESSAGE__SET_CONTEXT), 0, (LPARAM)lpszContext);

    RedrawWindow(hWnd, NULL, NULL, RDW_INVALIDATE | RDW_UPDATENOW | RDW_ALLCHILDREN);


}

HWND WINAPI FPC_CreateWindowA(HFPC hFPC, 
                       DWORD dwExStyle,
                       LPCSTR lpWindowName,
                       DWORD dwStyle,
                       int x,
                       int y,
                       int nWidth,
                       int nHeight,
                       HWND hWndParent,
                       HMENU hMenu,
                       HINSTANCE hInstance,
                       LPVOID lpParam)
{
    return CreateWindowExA(dwExStyle, 
                           (LPCSTR)FPC_GetClassAtomA(hFPC), 
                           lpWindowName,
                           dwStyle,
                           x,
                           y,
                           nWidth,
                           nHeight,
                           hWndParent,
                           hMenu,
                           hInstance,
                           lpParam);
}

HWND WINAPI FPC_CreateWindowW(HFPC hFPC, 
                       DWORD dwExStyle,
                       LPCWSTR lpWindowName,
                       DWORD dwStyle,
                       int x,
                       int y,
                       int nWidth,
                       int nHeight,
                       HWND hWndParent,
                       HMENU hMenu,
                       HINSTANCE hInstance,
                       LPVOID lpParam)
{
    typedef HWND (WINAPI *P_CreateWindowEx)(DWORD dwExStyle,
        LPCWSTR lpClassName,
        LPCWSTR lpWindowName,
        DWORD dwStyle,
        int x,
        int y,
        int nWidth,
        int nHeight,
        HWND hWndParent,
        HMENU hMenu,
        HINSTANCE hInstance,
        LPVOID lpParam
    );

    HMODULE hUser32 = GetModuleHandle(_T("user32.dll"));

    P_CreateWindowEx pCreateWindowEx;
    (FARPROC&)pCreateWindowEx = GetProcAddress(hUser32, "CreateWindowExW");

    if (pCreateWindowEx)
        return pCreateWindowEx(dwExStyle, 
                               (LPCWSTR)FPC_GetClassAtomW(hFPC), 
                               lpWindowName,
                               dwStyle,
                               x,
                               y,
                               nWidth,
                               nHeight,
                               hWndParent,
                               hMenu,
                               hInstance,
                               lpParam);
    else
        return NULL;
}
