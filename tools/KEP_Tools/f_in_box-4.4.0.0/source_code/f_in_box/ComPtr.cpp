#include "stdafx.h"
#include "ComPtr.h"
#include "MemManager.h"

namespace f_in_box
{
    namespace com_helpers
    {
        BOOL IsEqualGUID(const GUID& guid1, const GUID& guid2)
        {
            ASSERT(sizeof(guid1) == sizeof(guid2));

            return 0 == MemoryCompare(&guid1, &guid2, sizeof(guid1));
        }
    }
}
