#ifndef __MAP_H_68334F7D_15DA_4c02_97ED_0BE57D4D5675__
#define __MAP_H_68334F7D_15DA_4c02_97ED_0BE57D4D5675__

#include "List.h"

namespace f_in_box
{
    namespace std
    {
		template <class key_type>
		struct Less
		{
			bool operator() (const key_type& key1, const key_type& key2) const
			{
				return key1 < key2;
			}
		};

        template<class key_type, class value_type, class key_comparer = Less<key_type> >
        class CMap
        {
            struct SElement
            {
                key_type m_key;
                value_type m_value;

                SElement(const key_type& key, const value_type& value) : 
                    m_key(key), 
                    m_value(value)
                {
                }
            };

            CList<SElement*> m_Elements;

			key_comparer comparer;

		private:

			bool IsEqualKeys(const key_type& key1, const key_type& key2)
			{
				return (!(comparer(key1, key2)) && (!comparer(key2, key1)));
			}

        public:
            CMap()
            {
            }

            ~CMap()
            {
                RemoveAll();
            }

            void Add(const key_type& key, const value_type& value)
            {
                POSITION pos = m_Elements.GetFirstPosition();
                SElement* pElement;

                while (NULL != pos)
                {
                    m_Elements.GetNext(pos, pElement);

                    if (IsEqualKeys(pElement->m_key, key))
                    {
						// Set new value
                        pElement->m_value = value;
                        return;
                    }
                }

                m_Elements.Add(new SElement(key, value));
            }

			BOOL IsKeyPresent(const key_type& key)
			{
				value_type value;
				return Find(key, value);
			}

            BOOL Find(const key_type& key, value_type& value)
            {
                POSITION pos = m_Elements.GetFirstPosition();
                SElement* pElement;

                while (NULL != pos)
                {
                    m_Elements.GetNext(pos, pElement);

                    if (pElement->m_key == key)
                    {
                        value = pElement->m_value;
                        return TRUE;
                    }
                }

                return FALSE;
            }

            POSITION GetFirstPosition() const
            {
                return m_Elements.GetFirstPosition();
            }

            POSITION GetLastPosition() const
            {
                return m_Elements.GetLastPosition();
            }

            void GetAt(POSITION pos, key_type& key, value_type& value)
            {
                SElement* pElement = m_Elements.GetAt(pos);

                key = pElement->m_key;
                value = pElement->m_value;
            }

            void GetNext(POSITION& pos, key_type& key, value_type& value)
            {
                SElement* pElement = m_Elements.GetAt(pos);

                key = pElement->m_key;
                value = pElement->m_value;

                m_Elements.GetNext(pos, pElement);
            }

            value_type& GetNextValue(POSITION& pos)
            {
                SElement* pElement = m_Elements.GetAt(pos);

                // key = pElement->m_key;
                value_type& value = pElement->m_value;

                m_Elements.GetNext(pos, pElement);

				return value;
            }

            void RemoveAll()
            {
                POSITION pos = m_Elements.GetFirstPosition();
                SElement* pElement;

                while (NULL != pos)
                {
                    m_Elements.GetNext(pos, pElement);

                    delete pElement;
                }

                m_Elements.RemoveAll();
            }

            BOOL RemoveByKey(const key_type& key)
            {
                POSITION pos = m_Elements.GetFirstPosition();
                SElement* pElement;

                while (NULL != pos)
                {
                    pElement = m_Elements.GetAt(pos);

                    if (pElement->m_key == key)
                    {
                        m_Elements.RemoveAt(pos);
                        delete pElement;

                        return TRUE;
                    }

					m_Elements.GetNext(pos);
                }
                
                return FALSE;
            }
        };
    }
}

#endif // !__MAP_H_68334F7D_15DA_4c02_97ED_0BE57D4D5675__
