#include "stdafx.h"
#include "FlashOCXLoader.h"
#include "AudioClient.h"
#include "mmdeviceapi.h"
#include "MMDeviceProxy.h"
#include "ObjectWithCounter.h"
#include "ComPtr.h"
#include "AudioClientProxy.h"

using namespace f_in_box::com_helpers;
using namespace f_in_box::std;

namespace f_in_box
{
	/// This is a proxy of IMMDevice that passes all calls to underlying
	/// object, but additionally get IDirectSound8 returned by IMMDevice::Activate() - so we could
	/// control audio output.
	class CMMDeviceProxy : 
		public CObjectWithCounter, 
		public IMMDevice
	{
	private:
		/// Object that should receive all calls
		CComPtr<IMMDevice> m_pUnderlyingObject;

		/// Main object
		com_helpers::CComPtr<CFlashOCXLoader> m_pLoader;

	public:
		explicit CMMDeviceProxy(CFlashOCXLoader* pLoader, IMMDevice* pUnderlyingObject) : 
			m_pLoader(pLoader), 
			m_pUnderlyingObject(pUnderlyingObject)
		{
		}

        virtual ULONG __stdcall AddRef()
        {
			return CObjectWithCounter::AddRef();
        }

        virtual ULONG __stdcall Release()
        {
			return CObjectWithCounter::Release();
        }

		HRESULT STDMETHODCALLTYPE QueryInterface(REFIID riid, void** ppvObject)
		{
			if (f_in_box::com_helpers::IsEqualGUID(riid, IID_IUnknown))
				*ppvObject = this;
			else if (f_in_box::com_helpers::IsEqualGUID(riid, IID_IMMDevice))
				*ppvObject = static_cast<IMMDevice*>(this);
			else
				return m_pUnderlyingObject->QueryInterface(riid, ppvObject);

			AddRef();

			return S_OK;
		}

		virtual HRESULT STDMETHODCALLTYPE Activate(REFIID iid, DWORD dwClsCtx, PROPVARIANT* pActivationParams, void** ppInterface)
		{
			if (f_in_box::com_helpers::IsEqualGUID(iid, IID_IAudioClient))
			{
				CComPtr<IAudioClient> pOriginalAudioClient;
				HRESULT hr = m_pUnderlyingObject->Activate(IID_IAudioClient, dwClsCtx, pActivationParams, (void**)&pOriginalAudioClient);
				if (S_OK != hr)
				{
					// Set *ppInterface to NULL on error as stated in the documentation
					*ppInterface = NULL;

					return hr;
				}

				return CreateAudioClientProxy(m_pLoader, pOriginalAudioClient, (IAudioClient**)ppInterface);
			}
			else
			{
				return m_pUnderlyingObject->Activate(iid, dwClsCtx, pActivationParams, ppInterface);
			}
		}

		virtual HRESULT STDMETHODCALLTYPE OpenPropertyStore(DWORD stgmAccess, PVOID /* IPropertyStore* */ * ppProperties)
		{
			return m_pUnderlyingObject->OpenPropertyStore(stgmAccess, ppProperties);
		}

		virtual HRESULT STDMETHODCALLTYPE GetId(LPWSTR* ppstrId)
		{
			return m_pUnderlyingObject->GetId(ppstrId);
		}

		virtual HRESULT STDMETHODCALLTYPE GetState(DWORD* pdwState)
		{
			return m_pUnderlyingObject->GetState(pdwState);
		}
	};

	HRESULT CreateMMDeviceProxy(CFlashOCXLoader* pLoader, IMMDevice* pOriginalMMDevice, IMMDevice** ppProxyMMDevice)
	{
		CComPtr<IMMDevice> pProxy = new CMMDeviceProxy(pLoader, pOriginalMMDevice);

		return pProxy->QueryInterface(IID_IMMDevice, (void**)ppProxyMMDevice);
	}
}
