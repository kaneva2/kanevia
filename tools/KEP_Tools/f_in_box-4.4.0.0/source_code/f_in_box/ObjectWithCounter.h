#ifndef _ObjectWithCounter_H
#define _ObjectWithCounter_H

#define DEF_BEGIN_INTERFACE_MAP()   \
    if (NULL == ppvObject) \
        return E_POINTER; \
        \
    *ppvObject = NULL; \
        \
    if (f_in_box::com_helpers::IsEqualGUID(riid, IID_IUnknown))   \
        *ppvObject = this;  \
    else

#define DEF_DECLARE_INTERFACE(name) \
    if (f_in_box::com_helpers::IsEqualGUID(riid, IID_##name)) \
        *ppvObject = static_cast<name*>(this); \
    else

#define DEF_DECLARE_INTERFACE_WITH_IID(name, iid) \
    if (f_in_box::com_helpers::IsEqualGUID(riid, iid)) \
        *ppvObject = static_cast<name*>(this); \
    else

#define DEF_END_INTERFACE_MAP() \
    {   \
        return E_NOINTERFACE;   \
    }   \
    \
    AddRef();   \
    \
    return S_OK;

namespace f_in_box
{
    namespace std
    {
        /// Base class of classes which lifetime is based on reference count
        class CObjectWithCounter
        {
        private:
            volatile LONG m_nRefCount;

        protected:
            CObjectWithCounter() : 
                m_nRefCount(0)
            {
            }

            virtual ~CObjectWithCounter()
            {
                ASSERT(0 == m_nRefCount);
            }

            virtual LONG GetRefCount()
            {
                return m_nRefCount;
            }
        public:
            virtual ULONG __stdcall AddRef()
            {
#if (_MSC_VER == 1200) // VC++ 6.0
                return (ULONG)InterlockedIncrement((LONG*)&m_nRefCount);
#else
                return (ULONG)InterlockedIncrement(&m_nRefCount);
#endif
            }

            virtual ULONG __stdcall Release()
            {
#if (_MSC_VER == 1200) // VC++ 6.0
                LONG lRes = InterlockedDecrement((LONG*)&m_nRefCount);
#else
                LONG lRes = InterlockedDecrement(&m_nRefCount);
#endif

                if (0 == m_nRefCount)
                    delete this;

                return (ULONG)lRes;
            }
        };
    }
}

#endif // !_ObjectWithCounter_H
