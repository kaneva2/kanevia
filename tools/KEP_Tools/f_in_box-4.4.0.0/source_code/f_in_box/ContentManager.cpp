// Includes
#include "stdafx.h"
#include "ContentManager.h"
#include "f_in_box.h"
#include "MyMoniker.h"
#include "FPCWnd.h"
#include "MemManager.h"

// Defines
#define DEF_LOAD_FROM_MEMORY_MAGIC_STRING			L"D7BAED74924D435D84DB8A1E89E7AE3A"
#define DEF_LOAD_FROM_MEMORY_MOVIE_FILE_NAME		L"4BC37A1F293B47169DBC8019C50075F5.swf"
#define DEF_LOAD_FROM_MEMORY_NOTHING				L"file://AGKJGSAKLHASKLASKLSAKLKLSAKLAKLL.swf"

static f_in_box::std::CString<WCHAR> GetBase(HWND hwndFlashPlayerControl);

using namespace f_in_box;

CContentManager::CContentManager() : 
	m_nNextId(100), 
    m_dwHandlerCookie__OnLoadExternalResource(1), 
	m_dwHandlerCookie__GetBindInfo(1), 
    m_pFlashOCXLoader(NULL)
{
}

CContentManager::~CContentManager()
{
}

f_in_box::std::CString<WCHAR> CContentManager::GenerateNewMainMovieURL()
{
	CCriticalSectionOwner lock(m_cs);

	TCHAR lpszBuffer[1024]; DEF_ZERO_IT(lpszBuffer);

	char Buffer[256];
	wsprintfA(Buffer, "%d", m_nNextId);

	m_nNextId++;

	f_in_box::std::CString<WCHAR> strResult = L"Z:\\FromMemory\\";
	strResult += DEF_LOAD_FROM_MEMORY_MAGIC_STRING;
	strResult += L"\\";
    strResult += f_in_box::std::AToCW(Buffer);
	strResult += L"\\";
	strResult += DEF_LOAD_FROM_MEMORY_MOVIE_FILE_NAME;

	return strResult;
}

BOOL CContentManager::ParseURL(LPCWSTR lpszURL, int& nId, f_in_box::std::CString<WCHAR>& strRelativePart)
{
	BOOL bRes = FALSE;
	f_in_box::std::CString<WCHAR> strURL = lpszURL;

	for (UINT nIndex = 0; nIndex < strURL.GetLength(); nIndex++)
		if (L'\\' == strURL[nIndex])
			strURL[nIndex] = L'/';

	f_in_box::std::CString<WCHAR> wstring_FROM_MEMORY_MAGIC_STRING(DEF_LOAD_FROM_MEMORY_MAGIC_STRING);
	
	SIZE_T nPos1 = strURL.Find(DEF_LOAD_FROM_MEMORY_MAGIC_STRING);

	if ((SIZE_T)-1 != nPos1)
	{
		SIZE_T nPos2 = strURL.Find(L"/", nPos1 + wstring_FROM_MEMORY_MAGIC_STRING.GetLength() + 1);

		if ((SIZE_T)-1 != nPos2)
		{
			SIZE_T nStartId = nPos1 + wstring_FROM_MEMORY_MAGIC_STRING.GetLength() + 1;
			SIZE_T nCountId = nPos2 - nStartId;

			f_in_box::std::CString<WCHAR> strId = strURL.SubStr(nStartId, nCountId);

            nId = strId.ToInt();

			SIZE_T nStartRelativePart = nPos2 + 1;
			SIZE_T nCountRelativePart = strURL.GetLength() - nStartRelativePart;

			strRelativePart = strURL.SubStr(nStartRelativePart, nCountRelativePart);

			bRes = TRUE;
		}
	}

	return bRes;
}

BOOL CContentManager::IsURLsEqual(LPCWSTR lpszURL1, LPCWSTR lpszURL2)
{
	int nId1, nId2;
	f_in_box::std::CString<WCHAR> strRelativePart1, strRelativePart2;
	BOOL bIsFromMemoryURL1 = ParseURL(lpszURL1, nId1, strRelativePart1);
	BOOL bIsFromMemoryURL2 = ParseURL(lpszURL2, nId2, strRelativePart2);

	if (bIsFromMemoryURL1 & !bIsFromMemoryURL2)
		return FALSE;
	else if (!bIsFromMemoryURL2 & bIsFromMemoryURL1)
		return FALSE;
	else if (bIsFromMemoryURL1 & bIsFromMemoryURL2)
		return nId1 == nId2 && strRelativePart1 == strRelativePart2;
	else
	{
		f_in_box::std::CString<WCHAR> strURL1(lpszURL1);
		f_in_box::std::CString<WCHAR> strURL2(lpszURL2);

		return strURL1 == strURL2;
	}
}

void CContentManager::AddHWND(int nId, HWND hwndFlashPlayerControl)
{
	m_FlashPlayerControlIdPairs.Add(nId, hwndFlashPlayerControl);
}

HWND CContentManager::FindHWND(int nId)
{
	return m_FlashPlayerControlIdPairs.Find(nId);
}

// bUseLayer == TRUE - LoadMovie
// bUseLayer == FALSE - PutMovie
void CContentManager::LoadMovieFromMemory(HWND hwndFlashPlayerControl, 
										  LPVOID lpData, 
										  DWORD dwSize, 
										  BOOL bUseLayer /* = FALSE */ , 
										  int nLayer /* = 0 */ )
{
    f_in_box::com_helpers::CComPtr<IStream> pStream;

	LoadMovieUsingStream(hwndFlashPlayerControl, &pStream, bUseLayer, nLayer);

	ASSERT(pStream);

	if (pStream)
	{
		ULONG nWritten;
		pStream->Write(lpData, dwSize, &nWritten);
		pStream = NULL;
	}
}

void CContentManager::RemoveFlashPlayerControlHWND(HWND hwndFlashPlayerControl)
{
	m_FlashPlayerControlIdPairs.Remove(hwndFlashPlayerControl);
}

HRESULT CContentManager::BindToStorage(IBindCtx *pbc, 
									   IMoniker *pmkToLeft, 
									   REFIID riid, 
									   void **ppvObj, 
									   IMoniker* pmkContext, 
									   IBindStatusCallback* pBindStatusCallback, 
									   LPCWSTR lpszURL)
{
	*ppvObj = NULL;

	if (pBindStatusCallback)
	{
		BINDINFO bind_info;
		DEF_ZERO_IT(bind_info);
		DWORD dwBindf = 0;

		bind_info.cbSize = sizeof(bind_info);

		pBindStatusCallback->GetBindInfo(&dwBindf, &bind_info);
	}

	///
//	pmkContext = NULL;

	if (f_in_box::std::CString<WCHAR>(lpszURL) == f_in_box::std::CString<WCHAR>(DEF_LOAD_FROM_MEMORY_NOTHING))
		// ��� URL, ������������ ��� ���������� ����
		return MK_E_NOSTORAGE;

	HRESULT hr = MK_E_NOSTORAGE;
	BOOL bHandled = FALSE;

	// ��� �� ��� ���������������� � ������ ����� �������-����������
    f_in_box::com_helpers::CComPtr<IContentProvider> pContentProvider = FindContentProviderAndRemove(lpszURL);

	if (pContentProvider)
		// ContentProvider ������, �.�. ���� ���������, ��� ��� ������ ��� ������ LoadMovieUsingStream
	{
		bHandled = TRUE;

		pContentProvider->SetBindStatusCallback(pBindStatusCallback);
		hr = MK_S_ASYNCHRONOUS;
	}
	else
	{
		// ������� �������-���������
		f_in_box::com_helpers::CComPtr<IStream> pStream;
		CreateContentProvider(&pContentProvider, &pStream);
		pContentProvider->SetBindStatusCallback(pBindStatusCallback);
		// ������ ��� ������ � pStream ����� ���������� pBindStatusCallback::Write � ������
		// ����� ������������ �����

		// ������
		int nId;
		f_in_box::std::CString<WCHAR> strRelativePart;

		if (ParseURL(lpszURL, nId, strRelativePart))
			// ������������� ������� �� �������������� ����
		{
			// ���� ������, ��������������� � ���������� Id
			HWND hwndFlashPlayerControl = FindHWND(nId);

			// ������ �������
			ASSERT(NULL != hwndFlashPlayerControl);

			if (NULL == hwndFlashPlayerControl)
				return E_FAIL;

			// ���������: bHandled ����� ���� ����� FALSE, � ������ ��� ���� ��������
			bHandled = CallLoadExternalResource(hwndFlashPlayerControl, strRelativePart.c_str(), pStream);

			// ���������: bHandled ����� ���� ����� FALSE, � ������ ��� ���� ��������
			bHandled = bHandled || S_OK == pContentProvider->IsDataSaved();
			// �� ��� ������ bHandled �������� ����������������

			if (!bHandled)
				// ��������� ��������� ��������� ����
			{
				// Get Base
				f_in_box::std::CString<WCHAR> strBase = GetBase(hwndFlashPlayerControl);

				f_in_box::std::CString<WCHAR> strNewURL = strBase;
				strNewURL += L"/";
				strNewURL += strRelativePart;

				f_in_box::com_helpers::CComPtr<IMoniker> pMoniker;

                if (S_OK == m_UrlMonHelper.CreateURLMoniker(pmkContext, strNewURL.c_str(), &pMoniker))
					if (pMoniker)
					{
						bHandled = TRUE;
						hr = pMoniker->BindToStorage(pbc, pmkToLeft, riid, ppvObj);
					}
			}
		}
		else
		{
			if (!bHandled)
				// ��������� ���������� �����������
			{
                CCriticalSectionOwner cso(m_cs);

                f_in_box::std::POSITION pos = m_OnLoadExternalResourceHandlerInfo.GetFirstPosition();

                while (NULL != pos)
                {
                    SOnLoadExternalResourceHandlerInfo handler_info;
                    DWORD dwCookie;
                    m_OnLoadExternalResourceHandlerInfo.GetNext(pos, dwCookie, handler_info);

 					// a handler may change passed pointer
                    // it means that the handler gives us the stream from which we should load content
                    // the handler may use passed pointer and either write data to it immediately or later
					// if later, the handler should return S_ASYNCHRONOUS
					IStream* pStreamForHandler = pStream;

                    HRESULT hr1 = handler_info.Call(lpszURL, &pStreamForHandler, (HFPC)(CFlashOCXLoader*)m_pFlashOCXLoader);
 
					if (pStreamForHandler != pStream)
						// The pointer has changed
					{
						LARGE_INTEGER liZero; DEF_ZERO_IT(liZero);
						ULARGE_INTEGER uliNewPos;
						pStreamForHandler->Seek(liZero, STREAM_SEEK_SET, &uliNewPos);

                        const int nBufferSize = 1024 * 1024;
	                    BYTE* pBuffer = new BYTE[nBufferSize];

						while (true)
						{
							ULONG nRead = 0;
							HRESULT hr = pStreamForHandler->Read(pBuffer, nBufferSize, &nRead);

							if (S_OK != hr || 0 == nRead)
								break;

							ULONG nWritten;
							pStream->Write(pBuffer, nRead, &nWritten);
						}

                        delete[] pBuffer;

						pStreamForHandler->Release();

						bHandled = TRUE;
						hr = S_ASYNCHRONOUS;
					}
					else
					{
						bHandled = ( S_ASYNCHRONOUS == hr1 ) || ( S_OK == hr1 );
						hr = S_ASYNCHRONOUS;
					}

                    if (bHandled)
                        // Break from here
                        break;
				}
			}

			if (!bHandled)
			{
				f_in_box::com_helpers::CComPtr<IMoniker> pURLMoniker;

                m_UrlMonHelper.CreateURLMoniker(pmkContext, lpszURL, &pURLMoniker);

				if (pURLMoniker)
				{
					bHandled = TRUE;
					hr = pURLMoniker->BindToStorage(pbc, pmkToLeft, riid, ppvObj);

					return hr;
				}
			}
		}
	}

	return bHandled ? MK_S_ASYNCHRONOUS : MK_E_NOSTORAGE;
}

HRESULT CContentManager::MyCreateURLMoniker(IMoniker* pmkContext, LPCWSTR szURL, IMoniker** ppmk)
{
	return CMyMoniker::Create(szURL, this, pmkContext, IID_IMoniker, (LPVOID*)ppmk);
}

HANDLE CContentManager::MyCreateFakeFile(LPCWSTR lpFileName, 
										 DWORD dwDesiredAccess, 
										 DWORD dwShareMode, 
										 LPSECURITY_ATTRIBUTES lpSecurityAttributes, 
										 DWORD dwCreationDisposition, 
										 DWORD dwFlagsAndAttributes, 
										 HANDLE hTemplateFile)
{
    CCriticalSectionOwner cso(m_cs);

	HANDLE hFile = INVALID_HANDLE_VALUE;

    f_in_box::std::POSITION pos = m_OnLoadExternalResourceHandlerInfo.GetFirstPosition();

    while (NULL != pos)
    {
        SOnLoadExternalResourceHandlerInfo handler_info;
        DWORD dwCookie;
        m_OnLoadExternalResourceHandlerInfo.GetNext(pos, dwCookie, handler_info);

		f_in_box::com_helpers::CComPtr<IStream> pStream;
		CreateStreamOnHGlobal(NULL, TRUE, &pStream);

 		// a handler may change passed pointer
        // it means that the handler gives us the stream from which we should load content
        // the handler may use passed pointer and either write data to it immediately or later
		// if later, the handler should return S_ASYNCHRONOUS
		IStream* pStreamForHandler = pStream;

		HRESULT hr = handler_info.Call(lpFileName, &pStreamForHandler, (HFPC)(CFlashOCXLoader*)m_pFlashOCXLoader);

		if (S_OK == hr || S_ASYNCHRONOUS == hr)
		{
			LARGE_INTEGER liZero; DEF_ZERO_IT(liZero);
			ULARGE_INTEGER uliNewPos;
			pStreamForHandler->Seek(liZero, STREAM_SEEK_SET, &uliNewPos);

			// Create a fake handle
			hFile = CreateEvent(NULL, FALSE, FALSE, NULL);
			// Save it
			m_FakeHandleAndIStreamPairs.Add(hFile, pStreamForHandler);

			break;
		}
	}

	return hFile;
}

HANDLE CContentManager::MyCreateFileA(LPCSTR lpFileName, 
									  DWORD dwDesiredAccess, 
									  DWORD dwShareMode, 
									  LPSECURITY_ATTRIBUTES lpSecurityAttributes, 
									  DWORD dwCreationDisposition, 
									  DWORD dwFlagsAndAttributes, 
									  HANDLE hTemplateFile)
{
	return MyCreateFileW(
		f_in_box::std::AToCW(lpFileName).c_str(), 
			dwDesiredAccess, 
			dwShareMode, 
			lpSecurityAttributes, 
			dwCreationDisposition, 
			dwFlagsAndAttributes, 
			hTemplateFile
		);
}

HANDLE CContentManager::MyCreateFileW(LPCWSTR lpFileName, 
									  DWORD dwDesiredAccess, 
									  DWORD dwShareMode, 
									  LPSECURITY_ATTRIBUTES lpSecurityAttributes, 
									  DWORD dwCreationDisposition, 
									  DWORD dwFlagsAndAttributes, 
									  HANDLE hTemplateFile)
{
	HANDLE hFile = INVALID_HANDLE_VALUE;

	typedef HANDLE (WINAPI *PCreateFileW)(LPCWSTR lpFileName, 
										DWORD dwDesiredAccess, 
										DWORD dwShareMode, 
										LPSECURITY_ATTRIBUTES lpSecurityAttributes, 
										DWORD dwCreationDisposition, 
										DWORD dwFlagsAndAttributes, 
										HANDLE hTemplateFile);
	PCreateFileW pCreateFileW = NULL;
	(FARPROC&)pCreateFileW = GetProcAddress(GetModuleHandle(_T("kernel32.dll")), "CreateFileW");

	int nId;
	f_in_box::std::CString<WCHAR> strRelativePart;

    if (ParseURL(lpFileName, nId, strRelativePart))
	{
		HWND hwndFlashPlayerControl = FindHWND(nId);

		ASSERT(NULL != hwndFlashPlayerControl);

		// Get Base
		SFPCGetBaseW FPCGetBaseW;

		WCHAR lpszBase[_MAX_PATH + 1]; DEF_ZERO_IT(lpszBase);
		FPCGetBaseW.lpszBuffer = lpszBase;
		FPCGetBaseW.dwBufferSize = DEF_ARRAY_SIZE(lpszBase) - 1;

		::SendMessage(hwndFlashPlayerControl, FPCM_GET_BASEW, 0, (LPARAM)&FPCGetBaseW);

		f_in_box::std::CString<WCHAR> strNewURL = lpszBase;

		if (0 == lstrlenW(lpszBase))
		{
			typedef DWORD (WINAPI *P_GetCurrentDirectoryW)(IN DWORD nBufferLength, OUT LPWSTR lpBuffer);
			P_GetCurrentDirectoryW pGetCurrentDirectoryW = 
				(P_GetCurrentDirectoryW)GetProcAddress(GetModuleHandle(_T("kernel32.dll")), "GetCurrentDirectoryW");

			if (NULL != pGetCurrentDirectoryW)
			{
				WCHAR temp[2];
				DWORD nSize = pGetCurrentDirectoryW(1, temp);

				if (nSize > 0)
				{
					WCHAR* base = new WCHAR[nSize + 2];
					f_in_box::MemoryZero(base, sizeof(WCHAR) * (nSize + 2));
					pGetCurrentDirectoryW(nSize + 1, base);
					delete[] base;

					strNewURL = base;
				}
			}
			else
			{
				CHAR lpszBaseA[_MAX_PATH + 1]; DEF_ZERO_IT(lpszBaseA);
				GetCurrentDirectoryA(DEF_ARRAY_SIZE(lpszBaseA) - 1, lpszBaseA);
				strNewURL = f_in_box::std::AToCW(lpszBaseA);
			}
		}
		else
			strNewURL = lpszBase;

		strNewURL += L"/";
		strNewURL += strRelativePart;

        for (SIZE_T nIndex = 0; nIndex < strNewURL.GetLength(); nIndex++)
			if (L'/' == strNewURL[nIndex])
				strNewURL[nIndex] = L'\\';

		if (NULL == pCreateFileW)
			hFile = pCreateFileW(strNewURL.c_str(), 
							dwDesiredAccess, 
							dwShareMode, 
							lpSecurityAttributes, 
							dwCreationDisposition, 
							dwFlagsAndAttributes, 
							hTemplateFile);
		else
			hFile = CreateFileA(f_in_box::std::WToCA(strNewURL.c_str()).c_str(), 
							dwDesiredAccess, 
							dwShareMode, 
							lpSecurityAttributes, 
							dwCreationDisposition, 
							dwFlagsAndAttributes, 
							hTemplateFile);
	}

	if (INVALID_HANDLE_VALUE == hFile)
		hFile = 
			MyCreateFakeFile(lpFileName, 
							 dwDesiredAccess, 
							 dwShareMode, 
							 lpSecurityAttributes, 
							 dwCreationDisposition, 
							 dwFlagsAndAttributes, 
							 hTemplateFile);

	if (INVALID_HANDLE_VALUE == hFile)
	{
		if (NULL != pCreateFileW)
			hFile = pCreateFileW(lpFileName, 
	 							 dwDesiredAccess, 
								 dwShareMode, 
								 lpSecurityAttributes, 
								 dwCreationDisposition, 
								 dwFlagsAndAttributes, 
								 hTemplateFile);
		else
			hFile = CreateFileA(f_in_box::std::WToCA(lpFileName).c_str(), 
	 							 dwDesiredAccess, 
								 dwShareMode, 
								 lpSecurityAttributes, 
								 dwCreationDisposition, 
								 dwFlagsAndAttributes, 
								 hTemplateFile);
	}

	return hFile;
}

f_in_box::com_helpers::CComPtr<IStream> CContentManager::FindStreamByHandle(HANDLE hFakeFileHandle)
{
	return m_FakeHandleAndIStreamPairs.Find(hFakeFileHandle);
}

BOOL CContentManager::CloseFakeHandle(HANDLE hFakeFileHandle)
{
	return m_FakeHandleAndIStreamPairs.Remove(hFakeFileHandle);
}

BOOL CContentManager::CallLoadExternalResource(HWND hwndFlashPlayerControl, LPCWSTR lpszRelativePath, IStream* pStream)
{
	DEF_PRIVATE_MESSAGE__CALL_LOAD_EXTERNAL_RESOURCE_Data info;

	info.pStream = pStream;
	info.bstrPath = lpszRelativePath;
	info.bHandled = FALSE;

	::SendMessage(hwndFlashPlayerControl, DEF_PRIVATE_MESSAGE__CALL_LOAD_EXTERNAL_RESOURCE, 0, (LPARAM)&info);

	return info.bHandled;
}

//
f_in_box::com_helpers::CComPtr<IContentProvider> CContentManager::FindContentProviderAndRemove(LPCWSTR lpszURL)
{
	return m_URLContentProviderPairs.FindContentProvider(lpszURL, TRUE);
}

////
//void CContentManager::SetLoadExternalResourceHandler(PLOADEXTERNALRESOURCEHANDLER pLoadExternalResourceHandler)
//{
//	CCriticalSectionOwner lock(m_cs);
//
//	m_pLoadExternalResourceHandler = pLoadExternalResourceHandler;
//}

//=====================================================================================================
BOOL WINAPI FPCLoadMovieFromStream(HWND hwndFlashPlayerControl, int layer, IStream* pStream)
{
	if (NULL == hwndFlashPlayerControl || NULL == pStream)
		return FALSE;

    SFPCLoadMovieUsingStream FPCLoadMovieUsingStream; DEF_ZERO_IT(FPCLoadMovieUsingStream);
    FPCLoadMovieUsingStream.layer = layer;

    ::SendMessage(hwndFlashPlayerControl, FPCM_LOAD_MOVIE_USING_STREAM, 0, (LPARAM)&FPCLoadMovieUsingStream);

    if (NULL == FPCLoadMovieUsingStream.pStream)
        return FALSE;

    f_in_box::com_helpers::CComPtr<IStream> pOutStream;
    pOutStream.Attach(FPCLoadMovieUsingStream.pStream);

    const int nBufferSize = 1024 * 1024;
	BYTE* pBuffer = new BYTE[nBufferSize];

	while (true)
	{
		ULONG nRead;

		pStream->Read(pBuffer, nBufferSize, &nRead);

		if (0 == nRead)
			break;
		else
		{
			ULONG nWritten;
			pOutStream->Write(pBuffer, nRead, &nWritten);
			pOutStream = NULL;
		}
	}

    delete[] pBuffer;

	return TRUE;
}
//=====================================================================================================

//=====================================================================================================
BOOL WINAPI FPCPutMovieFromStream(HWND hwndFlashPlayerControl, IStream* pStream)
{
	if (NULL == hwndFlashPlayerControl || NULL == pStream)
		return FALSE;

    SFPCPutMovieUsingStream FPCPutMovieUsingStream; DEF_ZERO_IT(FPCPutMovieUsingStream);

    ::SendMessage(hwndFlashPlayerControl, FPCM_PUT_MOVIE_USING_STREAM, 0, (LPARAM)&FPCPutMovieUsingStream);

    if (NULL == FPCPutMovieUsingStream.pStream)
        return FALSE;

    f_in_box::com_helpers::CComPtr<IStream> pOutStream;
    pOutStream.Attach(FPCPutMovieUsingStream.pStream);

    const int nBufferSize = 1024 * 1024;
    LPBYTE pBuffer = new BYTE[nBufferSize];

	while (true)
	{
		ULONG nRead;

		pStream->Read(pBuffer, nBufferSize, &nRead);

		if (0 == nRead)
			break;
		else
		{
			ULONG nWritten;
			pOutStream->Write(pBuffer, nRead, &nWritten);
			pOutStream = NULL;
		}
	}

    delete[] pBuffer;

	return TRUE;
}
//=====================================================================================================

//=====================================================================================================
BOOL WINAPI FPCLoadMovieUsingStream(HWND hwndFlashPlayerControl, int layer, IStream** ppStream)
{
    if (NULL == hwndFlashPlayerControl || NULL == ppStream)
		return FALSE;

    SFPCLoadMovieUsingStream FPCLoadMovieUsingStream; DEF_ZERO_IT(FPCLoadMovieUsingStream);
    FPCLoadMovieUsingStream.layer = layer;

    ::SendMessage(hwndFlashPlayerControl, FPCM_LOAD_MOVIE_USING_STREAM, 0, (LPARAM)&FPCLoadMovieUsingStream);

    *ppStream = FPCLoadMovieUsingStream.pStream;

	return TRUE;
}
//=====================================================================================================

//=====================================================================================================
BOOL WINAPI FPCPutMovieUsingStream(HWND hwndFlashPlayerControl, IStream** ppStream)
{
	if (NULL == hwndFlashPlayerControl || NULL == ppStream)
		return FALSE;

    SFPCPutMovieUsingStream FPCPutMovieUsingStream; DEF_ZERO_IT(FPCPutMovieUsingStream);

    ::SendMessage(hwndFlashPlayerControl, FPCM_PUT_MOVIE_USING_STREAM, 0, (LPARAM)&FPCPutMovieUsingStream);

    *ppStream = FPCPutMovieUsingStream.pStream;

	return TRUE;
}
//=====================================================================================================

//=====================================================================================================
// bUseLayer == TRUE - LoadMovie
// bUseLayer == FALSE - PutMovie
void CContentManager::LoadMovieUsingStream(HWND hwndFlashPlayerControl, 
										   IStream** ppStream, 
										   BOOL bUseLayer /* = FALSE */ , 
										   int nLayer /* = -1 */ )
{
    f_in_box::com_helpers::CComPtr<IContentProvider> pContentProvider;
	f_in_box::com_helpers::CComPtr<IStream> pStream;
	CreateContentProvider(&pContentProvider, &pStream);

	*ppStream = pStream;
	(*ppStream)->AddRef();

	f_in_box::std::CString<WCHAR> strURL = GenerateNewMainMovieURL();

	m_URLContentProviderPairs.Add(strURL.c_str(), pContentProvider);

	int nId;
	f_in_box::std::CString<WCHAR> strRelativePath;
	ParseURL(strURL.c_str(), nId, strRelativePath);
	AddHWND(nId, hwndFlashPlayerControl);

	if (bUseLayer)
	{
		SFPCLoadMovieW FPCLoadMovieW;

		FPCLoadMovieW.layer = nLayer;
		FPCLoadMovieW.url.lpszBuffer = DEF_LOAD_FROM_MEMORY_NOTHING;
		::SendMessage(hwndFlashPlayerControl, FPCM_LOADMOVIEW, 0, (LPARAM)&FPCLoadMovieW);

		FPCLoadMovieW.layer = nLayer;
		FPCLoadMovieW.url.lpszBuffer = strURL.c_str();
		::SendMessage(hwndFlashPlayerControl, FPCM_LOADMOVIEW, 0, (LPARAM)&FPCLoadMovieW);
	}
	else
	{
		SFPCPutMovieW FPCPutMovieW;

		FPCPutMovieW.lpszBuffer = DEF_LOAD_FROM_MEMORY_NOTHING;
		::SendMessage(hwndFlashPlayerControl, FPCM_PUT_MOVIEW, 0, (LPARAM)&FPCPutMovieW);

		FPCPutMovieW.lpszBuffer = strURL.c_str();
		::SendMessage(hwndFlashPlayerControl, FPCM_PUT_MOVIEW, 0, (LPARAM)&FPCPutMovieW);
	}
}
//=====================================================================================================

//=====================================================================================================
f_in_box::std::CString<WCHAR> GetBase(HWND hwndFlashPlayerControl)
{
	f_in_box::std::CString<WCHAR> strBase;

	if (NULL != hwndFlashPlayerControl)
	{
		SFPCGetBaseW FPCGetBaseW;

		FPCGetBaseW.lpszBuffer = NULL;

		::SendMessage(hwndFlashPlayerControl, FPCM_GET_BASEW, 0, (LPARAM)&FPCGetBaseW);

		if (FPCGetBaseW.dwBufferSize > 0)
		{
			FPCGetBaseW.lpszBuffer = new WCHAR[FPCGetBaseW.dwBufferSize];
			f_in_box::MemoryZero(FPCGetBaseW.lpszBuffer, sizeof(WCHAR) * FPCGetBaseW.dwBufferSize);

			::SendMessage(hwndFlashPlayerControl, FPCM_GET_BASEW, 0, (LPARAM)&FPCGetBaseW);

			strBase = FPCGetBaseW.lpszBuffer;
		}
	}

	if (0 == strBase.GetLength())
	{
		DWORD dwBufferSize = 16;

		typedef DWORD (WINAPI *PGetCurrentDirectoryW)(DWORD nBufferLength, LPWSTR lpBuffer);
		PGetCurrentDirectoryW pGetCurrentDirectoryW = NULL;
		(FARPROC&)pGetCurrentDirectoryW = GetProcAddress(GetModuleHandle(_T("kernel32.dll")), "GetCurrentDirectoryW");

		if (pGetCurrentDirectoryW)
		{
			WCHAR* lpszCurDir = new WCHAR[1];
			DWORD dwBufferSize = pGetCurrentDirectoryW(1, lpszCurDir);

			delete[] lpszCurDir;

			lpszCurDir = new WCHAR[dwBufferSize + 1];
			pGetCurrentDirectoryW(dwBufferSize + 1, lpszCurDir);

			strBase = lpszCurDir;

			delete[] lpszCurDir;
		}
		else
		{
			CHAR* lpszCurDir = new CHAR[1];
			DWORD dwBufferSize = GetCurrentDirectoryA(1, lpszCurDir);

			delete[] lpszCurDir;

			lpszCurDir = new CHAR[dwBufferSize + 1];
			GetCurrentDirectoryA(dwBufferSize + 1, lpszCurDir);

            strBase = f_in_box::std::AToCW(lpszCurDir);

			delete[] lpszCurDir;
		}
	}

	for (size_t i = 0; i < strBase.GetLength(); i++)
		if (L'\\' == strBase[i])
			strBase[i] = L'/';

	return strBase;
}
//=====================================================================================================

DWORD CContentManager::AddOnLoadExternalResourceHandlerA(PLOAD_EXTERNAL_RESOURCE_HANDLERA pHandler, LPARAM lParam)
{
    CCriticalSectionOwner cso(m_cs);

    DWORD dwCookie = m_dwHandlerCookie__OnLoadExternalResource;

    m_OnLoadExternalResourceHandlerInfo.Add(dwCookie, SOnLoadExternalResourceHandlerInfo(pHandler, lParam));

    m_dwHandlerCookie__OnLoadExternalResource++;

    return dwCookie;
}

DWORD CContentManager::AddOnLoadExternalResourceHandlerW(PLOAD_EXTERNAL_RESOURCE_HANDLERW pHandler, LPARAM lParam)
{
    CCriticalSectionOwner cso(m_cs);

    DWORD dwCookie = m_dwHandlerCookie__OnLoadExternalResource;

    m_OnLoadExternalResourceHandlerInfo.Add(dwCookie, SOnLoadExternalResourceHandlerInfo(pHandler, lParam));

    m_dwHandlerCookie__OnLoadExternalResource++;

    return dwCookie;
}

BOOL CContentManager::RemoveOnLoadExternalResourceHandler(DWORD dwCookie)
{
    CCriticalSectionOwner cso(m_cs);

    return m_OnLoadExternalResourceHandlerInfo.RemoveByKey(dwCookie);
}


DWORD CContentManager::AddGetBindInfoHandler(PGET_BIND_INFO_HANDLER pHandler, LPARAM lParam)
{
    CCriticalSectionOwner cso(m_cs);

    DWORD dwCookie = m_dwHandlerCookie__GetBindInfo;

    m_GetBindInfoHandlerInfo.Add(dwCookie, SGetBindInfoHandlerInfo(pHandler, lParam));

    m_dwHandlerCookie__GetBindInfo++;

    return dwCookie;
}

BOOL CContentManager::RemoveGetBindInfoHandler(DWORD dwCookie)
{
    CCriticalSectionOwner cso(m_cs);

    return m_GetBindInfoHandlerInfo.RemoveByKey(dwCookie);
}

void CContentManager::CallBindInfoHandlers(DWORD* grfBINDF, BINDINFO* pbindinfo)
{
    CCriticalSectionOwner cso(m_cs);

	f_in_box::std::POSITION pos = m_GetBindInfoHandlerInfo.GetFirstPosition();

	while (NULL != pos)
	{
		SGetBindInfoHandlerInfo handler_info;
		DWORD dwCookie;
		m_GetBindInfoHandlerInfo.GetNext(pos, dwCookie, handler_info);

		handler_info.Call((HFPC)(CFlashOCXLoader*)m_pFlashOCXLoader, grfBINDF, pbindinfo);
	}
}
