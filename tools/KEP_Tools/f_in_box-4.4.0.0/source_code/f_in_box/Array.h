#ifndef __ARRAY_H_6CE7F2D3_5A17_4036_B84D_C2CD459E3B33__
#define __ARRAY_H_6CE7F2D3_5A17_4036_B84D_C2CD459E3B33__

namespace f_in_box
{
    namespace std
    {
        template <class element_type>
        class CArray
        {
        private:
            element_type* m_p;
            SIZE_T m_nCount;

        public:
            CArray(UINT nCount = 0) : 
                m_p(NULL), 
                m_nCount(0)
            {
                if (nCount > 0)
                {
                    m_p = new element_type[nCount];
                    m_nCount = nCount;
                }
            }

            CArray(const element_type* pCopyFrom, UINT nCount = 0) : 
                m_p(NULL), 
                m_nCount(0)
            {
                if (nCount > 0)
                {
                    m_p = new element_type[nCount];
                    m_nCount = nCount;

					for (SIZE_T i = 0; i < m_nCount; i++)
						m_p[i] = pCopyFrom[i];
				}
            }

			CArray(const CArray& other) : 
                m_p(NULL), 
                m_nCount(0)
			{
				Free();

				if (other.m_nCount > 0)
				{
					Alloc(other.m_nCount);

					for (UINT i = 0; i < m_nCount; i++)
						m_p[i] = other[i];
				}
			}

			const CArray& operator=(const CArray& other)
			{
				Free();

				if (other.m_nCount > 0)
				{
					Alloc(other.m_nCount);

					for (UINT i = 0; i < m_nCount; i++)
						m_p[i] = other[i];
				}

				return *this;
			}

            ~CArray()
            {
                Free();
            }

            void Alloc(SIZE_T nCount)
            {
                Free();

                m_p = new element_type[nCount];
                m_nCount = nCount;
            }

            void Free()
            {
                if (NULL != m_p)
                {
                    delete[] m_p;
					m_p = NULL;
                    m_nCount = 0;
                }
            }

            operator element_type* ()
            {
                return m_p;
            }

			const element_type& operator[] (SIZE_T nIndex) const
			{
				return m_p[nIndex];
			}

			SIZE_T GetCount() const
			{
				return m_nCount;
			}
        };
    }
}

#endif // !__ARRAY_H_6CE7F2D3_5A17_4036_B84D_C2CD459E3B33__
