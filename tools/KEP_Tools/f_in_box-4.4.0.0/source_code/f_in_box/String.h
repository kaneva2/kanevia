#ifndef __STRING_H_731E6AFD_879C_44da_ADF0_5DC35E891A25__
#define __STRING_H_731E6AFD_879C_44da_ADF0_5DC35E891A25__

#include "MemManager.h"

namespace f_in_box
{
    namespace std
    {
        template <class char_type>
        class CString
        {
        private:
            char_type* m_pStr;

        public:
            static SIZE_T GetLength(const char_type* p)
            {
                SIZE_T nLength = 0;

                for (const char_type* p1 = p; NULL != *p1; p1++)
                    nLength++;

                return nLength;
            }

        private:

            void Init(const char_type* pSource = NULL)
            {
                delete[] m_pStr;
                m_pStr = NULL;

                if (NULL == pSource)
                {
                    m_pStr = new char_type[1];
                    m_pStr[0] = 0;
                }
                else
                {
                    SIZE_T nLength = GetLength(pSource);
                    m_pStr = new char_type[nLength + 1];
					f_in_box::MemoryCopy(m_pStr, pSource, sizeof(char_type) * (nLength + 1));
                }
            }

        public:
            CString(const char_type* pSource = NULL) : 
                m_pStr(NULL)
            {
                Init(pSource);
            }

            CString(const CString<char_type>& str) : 
                m_pStr(NULL)
            {
                Init(str.c_str());
            }

            CString<char_type>& operator= (const CString<char_type>& str)
            {
                Init(str.c_str());

                return *this;
            }

            ~CString()
            {
                delete[] m_pStr;
                m_pStr = NULL;
            }

            const char_type* c_str() const
            {
                return m_pStr;
            }

            SIZE_T GetLength() const
            {
                return GetLength(m_pStr);
            }

            char_type& operator[] (const SIZE_T i) const
            {
                return m_pStr[i];
            }

			friend bool operator== (const char_type* p, const CString<char_type>& strOther)
			{
				return CString<char_type>(p) == strOther;
			}

            bool operator== (const CString<char_type>& strOther)
            {
                if (GetLength() != strOther.GetLength())
                    return false;

                for (UINT i = 0; i < GetLength(); i++)
                    if (m_pStr[i] != strOther[i])
                        return false;

                return true;
            }

			friend CString<char_type> operator + (const char_type* p, const CString<char_type>& other)
			{
				return CString<char_type>(p) + other;
			}

            CString<char_type> operator + (const CString<char_type>& other)
            {
                SIZE_T nNewLength = GetLength() + other.GetLength() + 1;

                CString<char_type> strResult;

                if (nNewLength > 0)
                {
                    char_type* pResult = new char_type[nNewLength];

                    pResult[nNewLength - 1] = 0;

                    f_in_box::MemoryCopy(pResult, m_pStr, GetLength() * sizeof(char_type));
                    f_in_box::MemoryCopy(pResult + GetLength(), other.m_pStr, other.GetLength() * sizeof(char_type));

                    strResult = pResult;

                    delete[] pResult;
                }

                return strResult;
            }

            CString<char_type>& operator += (const CString<char_type>& other)
            {
                *this = *this + other;

                return *this;
            }

            SIZE_T Find(const CString<char_type>& substr, SIZE_T nStartPos = 0)
            {
                char_type* pSubstrStart = m_pStr + nStartPos;
                char_type* pSubstrEnd = pSubstrStart + substr.GetLength();
                char_type* pStrEnd = m_pStr + GetLength();

                for (; pSubstrEnd <= pStrEnd; pSubstrStart++, pSubstrEnd++)
                {
                    BOOL bEqual = TRUE;

                    char_type* p1 = pSubstrStart;
                    char_type* p2 = substr.m_pStr;

                    for (; p1 < pSubstrEnd; p1++, p2++)
                        if (*p1 != *p2)
                        {
                            bEqual = FALSE;
                            break;
                        }

                    if (bEqual)
                        return pSubstrStart - m_pStr;
                }

                return (SIZE_T)-1;
            }

            CString<char_type> SubStr(SIZE_T nFrom, SIZE_T nCount)
            {
                char_type* pBuffer = new char_type[nCount + 1];
                pBuffer[nCount] = 0;

                f_in_box::MemoryCopy(pBuffer, m_pStr + nFrom, sizeof(char_type) * nCount);

                CString<char_type> strSubStr(pBuffer);

                delete[] pBuffer;

                return strSubStr;
            }

            int ToInt() const
            {
                int n = 0;

                int nBase = 1;

                for (char_type* p = m_pStr + GetLength() - 1; p >= m_pStr; p--, nBase *= 10)
                    n += (*p - '0') * nBase;

                return n;
            }

			bool IsEmpty() const
			{
				return 0 == GetLength();
			}
        };

		static bool operator < (const CString<CHAR>& str1, const CString<CHAR>& str2)
		{
			return 0 < lstrcmpiA(str1.c_str(), str2.c_str());
		}

		static bool operator < (const CString<WCHAR>& str1, const CString<WCHAR>& str2)
		{
			return 0 < lstrcmpiW(str1.c_str(), str2.c_str());
		}

		typedef CString<WCHAR> CWideString;
		typedef CString<CHAR> CAnsiString;

		CString<CHAR> WToCA(LPCWSTR p);
        CString<WCHAR> AToCW(LPCSTR p);
	}
}

#endif // !__STRING_H_731E6AFD_879C_44da_ADF0_5DC35E891A25__
