#ifndef __LIST_H__27AE9216_674D_498f_8D35_5EBE09E2D9D0__
#define __LIST_H__27AE9216_674D_498f_8D35_5EBE09E2D9D0__

namespace f_in_box
{
    namespace std
    {
        typedef void* POSITION;

        template<class value_type>
        class CList
        {
        private:

            struct SElement
            {
                SElement* m_pPrev;
                SElement* m_pNext;

                value_type m_value;
            };

            SElement* m_pFirst;
            SElement* m_pLast;

        public:

            CList() : 
                m_pFirst(NULL), 
                m_pLast(NULL)
            {
            }

            ~CList()
            {
                RemoveAll();
            }

            POSITION Add(const value_type& value)
            {
                SElement* pNewElement = new SElement;

                pNewElement->m_value = value;

                if (NULL == m_pFirst)
                {
                    ASSERT(NULL == m_pLast);

                    pNewElement->m_pPrev = NULL;
                    pNewElement->m_pNext = NULL;

                    m_pFirst = m_pLast = pNewElement;
                }
                else
                {
                    pNewElement->m_pNext = NULL;
                    pNewElement->m_pPrev = m_pLast;
                    
                    m_pLast->m_pNext = pNewElement;

                    m_pLast = pNewElement;
                }

                return pNewElement;
            }

            POSITION GetFirstPosition() const
            {
                return m_pFirst;
            }

            POSITION GetLastPosition() const
            {
                return m_pLast;
            }

            value_type& GetAt(POSITION pos)
            {
                return ((SElement*)pos)->m_value;
            }

            const value_type& GetAt(POSITION pos) const
            {
                return ((SElement*)pos)->m_value;
            }

            void GetNext(POSITION& pos, value_type& value)
            {
                SElement* pElement = (SElement*)pos;

                value = pElement->m_value;
                pos = pElement->m_pNext;
            }

            const value_type& GetNext(POSITION& pos)
            {
                SElement* pElement = (SElement*)pos;

                pos = pElement->m_pNext;

                return pElement->m_value;
            }

            void GetPrev(POSITION& pos, value_type& value)
            {
                SElement* pElement = (SElement*)pos;

                value = pElement->m_value;
                pos = pElement->m_pPrev;
            }

            void RemoveAt(POSITION pos)
            {
                SElement* pElement = (SElement*)pos;

                if (NULL != pElement->m_pPrev)
                    pElement->m_pPrev->m_pNext = pElement->m_pNext;

                if (NULL != pElement->m_pNext)
                    pElement->m_pNext->m_pPrev = pElement->m_pPrev;

                if (m_pFirst == pElement)
                    m_pFirst = pElement->m_pNext;

                if (m_pLast == pElement)
                    m_pLast = pElement->m_pPrev;

                delete pElement;
            }

            void RemoveAll()
            {
                for (SElement* pElement = m_pFirst; NULL != pElement; )
                {
                    SElement* pNextElement = pElement->m_pNext;
                    delete pElement;
                    pElement = pNextElement;
                }

                m_pFirst = m_pLast = NULL;
            }

			//POSITION Find(const value_type& val, POSITION StartFrom = NULL)
			//{
			//	if (NULL == StartFrom)
			//		StartFrom = GetFirstPosition();

			//	for (POSITION pos = StartFrom; NULL != pos; GetNext(pos);)
			//	{
			//		if (GetAt(pos) == val)
			//			return pos;
			//	}
			//}
        };
    }
}

#endif // !__LIST_H__27AE9216_674D_498f_8D35_5EBE09E2D9D0__
