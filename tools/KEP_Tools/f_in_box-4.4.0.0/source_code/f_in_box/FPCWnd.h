#ifndef __FPC_WND_H__171FB242_B8B1_4122_9AA9_99C36EBFCD90__
#define __FPC_WND_H__171FB242_B8B1_4122_9AA9_99C36EBFCD90__

#include "PreTranslateMessageHelper.h"
#include "FlashInterfaces.h"
#include "FlashOCXLoader.h"
#include "ComPtr.h"
#include "CriticalSection.h"
#include "User32Wrapper.h"
#include "BSTR.h"
#include "Wnd.h"
#include "FlashAXWnd.h"
#include "TimerService.h"

namespace f_in_box
{

#define GET_VERSION_INTERFACE_POINTER(version)    m_pShockwaveFlash##version

#define DEF_PRIVATE_MESSAGE__SET_EVENT_LISTENER				RegisterWindowMessageA("DEF_PRIVATE_MESSAGE__SET_EVENT_LISTENER__cfde44ac-A1B5-422b-AC7E-AFCD35AB3671")
struct DEF_PRIVATE_MESSAGE__SET_EVENT_LISTENER_Data
{
	PFLASHPLAYERCONTROLEVENTLISTENER pListener;
	LPARAM lParam;
};

#define DEF_PRIVATE_MESSAGE__CALL_LOAD_EXTERNAL_RESOURCE	RegisterWindowMessageA("DEF_PRIVATE_MESSAGE__CALL_LOAD_EXTERNAL_RESOURCE__4006D732-0800-4a8e-BD45-8D7A153fad53")
struct DEF_PRIVATE_MESSAGE__CALL_LOAD_EXTERNAL_RESOURCE_Data
{
    f_in_box::com_helpers::CBSTR bstrPath;
    f_in_box::com_helpers::CComPtr<IStream> pStream;
	BOOL bHandled;
};

class CFPCWnd;

#define DEF_PRIVATE_MESSAGE__GET_OBJECT_PTR	RegisterWindowMessageA("DEF_PRIVATE_MESSAGE__GET_OBJECT_PTR__A4679BFB-0972-46ae-A032-1A46711970F9")
struct DEF_PRIVATE_MESSAGE__GET_OBJECT_PTR_Data
{
	CFPCWnd* pObject;
};

class CFPCWnd : 
    public IOleClientSite, 
    public IOleInPlaceSiteWindowless, 
    public IOleControlSite, 
    public IOleContainer, 
    public IOleInPlaceFrame, 
    public IObjectWithSite, 
	public IServiceProvider, 
    public f_in_box::flash::IShockwaveFlashEvents, 
    public CPreTranslateMessageHelper, 
	public f_in_box::CWnd
{
private:





    DWORD m_dwStartTime;
    f_in_box::std::CString<CHAR> m_strContext;
    UINT m_uMsg__SetContext;



    // Reference count
    ULONG m_nRefCount;

    //
    com_helpers::CComPtr<CFlashOCXLoader> m_pFlashOCXLoader;

    //
    RECT m_rcPos;

    //
    BOOL m_bInPlaceActive;

    //
    BOOL m_bWindowless;

    //
    BOOL m_bUIActive;

    //
    HACCEL m_hAccel;

    //
    BOOL m_bMDIApp;

    //
    BOOL m_bCanWindowlessActivate;

    //
    BOOL m_bCapture;

    //
    BOOL m_bHaveFocus;

    //
    BOOL m_bDCReleased;

    //
    HDC m_hDCScreen;

    //
    BOOL m_bLocked;

    //
    DWORD m_dwCookie;
    BOOL m_bSubcribedToEvents;

    //
    BOOL m_bTransparent;

    //
    DWORD m_dwMiscStatus;

    //
    DWORD m_dwViewObjectType;

    //
    SIZE m_pxSize;
    SIZE m_hmSize;

    //
	LPARAM m_LPARAM_EventListenerParam;
	PFLASHPLAYERCONTROLEVENTLISTENER m_pEventListener;

    CCriticalSection m_csEventListener;

    //
    SUser32Wrapper m_User32Wrapper;

    //
    COLORREF m_clrBackground;

    //
    SIZE m_size;
    HBITMAP m_hBmpWhite;
    HBITMAP m_hBmpBlack;
    LPVOID m_pBitsWhite;
    LPVOID m_pBitsBlack;
    HBITMAP m_hBmp;
    LPVOID m_pBits;

    // 0 - 255
    BYTE m_nOverallOpaque;

    //
    BOOL m_bReleaseAll;

    //
    BOOL m_bStandardMenu;

    //
    f_in_box::com_helpers::CComPtr<IOleObject> m_pOleObject;
    f_in_box::com_helpers::CComPtr<IOleInPlaceObjectWindowless> m_pOleInPlaceObjectWindowless;
    f_in_box::com_helpers::CComPtr<IUnknown> m_pUnknown;
    f_in_box::com_helpers::CComPtr<IViewObject> m_pViewObject;
    f_in_box::com_helpers::CComPtr<IViewObjectEx> m_pViewObjectEx;

    //
    f_in_box::com_helpers::CComPtr<f_in_box::flash::v3::IShockwaveFlash> m_pShockwaveFlash3;
    f_in_box::com_helpers::CComPtr<f_in_box::flash::v4::IShockwaveFlash> m_pShockwaveFlash4;
    f_in_box::com_helpers::CComPtr<f_in_box::flash::v5::IShockwaveFlash> m_pShockwaveFlash5;
    f_in_box::com_helpers::CComPtr<f_in_box::flash::v6::IShockwaveFlash> m_pShockwaveFlash6;
    f_in_box::com_helpers::CComPtr<f_in_box::flash::v7::IShockwaveFlash> m_pShockwaveFlash7;
    f_in_box::com_helpers::CComPtr<f_in_box::flash::v8::IShockwaveFlash> m_pShockwaveFlash8;

    //
    f_in_box::com_helpers::CComPtr<IUnknown> m_pUnkSite;

    //
    UINT m_uMsg__PRIVATE_MESSAGE__SET_EVENT_LISTENER;
    UINT m_uMsg__PRIVATE_MESSAGE__CALL_LOAD_EXTERNAL_RESOURCE;
	UINT m_uMsg__DEF_PRIVATE_MESSAGE__GET_OBJECT_PTR;

    //
    f_in_box::CFlashAXWnd m_FlashAXWnd;

	//
	BOOL m_bAttachToParent;

	//
	UINT_PTR m_TimerId__NtHitTest;

	f_in_box::com_helpers::CComPtr<IDirectDraw2> m_pDirectDraw;
	f_in_box::com_helpers::CComPtr<IDirectDrawSurface> m_pDirectDrawSurface;
	SIZE m_SurfaceSize;
	HDC m_hdcDirectDrawSurface;

public:
    CFPCWnd(HWND hWnd, CFlashOCXLoader* pFlashOCXLoader, LPCREATESTRUCT pCreateStruct);
    virtual ~CFPCWnd();

    LRESULT
    CALLBACK
    WindowProc(
        HWND hWnd, 
        UINT uMsg, 
        WPARAM wParam, 
        LPARAM lParam 
    );

	LRESULT
	CALLBACK
	InternalWindowProc(
		HWND hWnd, 
		UINT uMsg, 
		WPARAM wParam, 
		LPARAM lParam, 
		BOOL& bHandled
	);

	void PaintTo(HDC hDC);
	BOOL CallWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam, LRESULT* plResult, BOOL* pbHandled);

	com_helpers::CComPtr<CFlashOCXLoader> GetOCXLoader() const;

	static CFPCWnd* FromHWND(HWND hwndFlashPlayerControl);

private:

	//
	HWND GetControlOwner() const
	{
		return m_bAttachToParent ? ::GetParent(m_hWnd) : m_hWnd;
	}

	f_in_box::com_helpers::CComPtr<IViewObject> GetViewObject()
	{
		return m_pViewObject;
	}

    // Send notification FPCN_UPDATE_RECT
    void SendUpdateRectNotification(RECT rc);

    // Send notification FPCN_PAINT
    void SendPaintNotification(LPVOID lpPixels);

    // Event handlers
    void OnReadyStateChange(long newState);
    void OnProgress(long percentDone);
    void OnFSCommand(BSTR command, BSTR args);
    void OnFlashCall(BSTR request);

    void PreTranslateMessageHelper_PreTranslateMessage(MSG* pMsg);

    // Returns flash version if version <= 8, and 8 if version > 8
    DWORD GetFlashVersion();

	void ReleaseAll();

	//
	void CallPaintHandler(HDC hdc, DWORD dwStage);

	/// Uses DirectDraw surface to paint if it's available, if not, calls IViewObject::Draw only
	void Draw(HDC hDC, LPRECT pRect);

public:
    // IUnknown
    virtual HRESULT STDMETHODCALLTYPE QueryInterface(REFIID riid, void** ppvObject);
    virtual ULONG STDMETHODCALLTYPE AddRef();
    virtual ULONG STDMETHODCALLTYPE Release();

    // IOleClientSite
    virtual HRESULT STDMETHODCALLTYPE SaveObject();
    virtual HRESULT STDMETHODCALLTYPE GetMoniker(DWORD dwAssign, DWORD dwWhichMoniker, IMoniker** ppmk);
    virtual HRESULT STDMETHODCALLTYPE GetContainer(IOleContainer** ppContainer);
    virtual HRESULT STDMETHODCALLTYPE ShowObject();
    virtual HRESULT STDMETHODCALLTYPE OnShowWindow(BOOL fShow);
    virtual HRESULT STDMETHODCALLTYPE RequestNewObjectLayout();

    // IOleWindow
    virtual HRESULT STDMETHODCALLTYPE GetWindow(HWND* phwnd);
    virtual HRESULT STDMETHODCALLTYPE ContextSensitiveHelp(BOOL fEnterMode);

    // IOleInPlaceSite
    virtual HRESULT STDMETHODCALLTYPE CanInPlaceActivate();
    virtual HRESULT STDMETHODCALLTYPE OnInPlaceActivate();
    virtual HRESULT STDMETHODCALLTYPE OnUIActivate();
        
    virtual HRESULT STDMETHODCALLTYPE GetWindowContext( 
                        IOleInPlaceFrame** ppFrame, 
                        IOleInPlaceUIWindow** ppDoc, 
                        LPRECT lprcPosRect, 
                        LPRECT lprcClipRect, 
                        LPOLEINPLACEFRAMEINFO lpFrameInfo);
        
    virtual HRESULT STDMETHODCALLTYPE Scroll(SIZE scrollExtant);
    virtual HRESULT STDMETHODCALLTYPE OnUIDeactivate(BOOL fUndoable);
    virtual HRESULT STDMETHODCALLTYPE OnInPlaceDeactivate();
    virtual HRESULT STDMETHODCALLTYPE DiscardUndoState();
    virtual HRESULT STDMETHODCALLTYPE DeactivateAndUndo();
    virtual HRESULT STDMETHODCALLTYPE OnPosRectChange(LPCRECT lprcPosRect);

    // IOleInPlaceSiteEx
    virtual HRESULT STDMETHODCALLTYPE OnInPlaceActivateEx(BOOL* pfNoRedraw, DWORD dwFlags);
    virtual HRESULT STDMETHODCALLTYPE OnInPlaceDeactivateEx(BOOL fNoRedraw);
    virtual HRESULT STDMETHODCALLTYPE RequestUIActivate();

    // IOleInPlaceSiteWindowless
    virtual HRESULT STDMETHODCALLTYPE CanWindowlessActivate();
    virtual HRESULT STDMETHODCALLTYPE GetCapture();
    virtual HRESULT STDMETHODCALLTYPE SetCapture(BOOL fCapture);
    virtual HRESULT STDMETHODCALLTYPE GetFocus();
    virtual HRESULT STDMETHODCALLTYPE SetFocus(BOOL fFocus);
    virtual HRESULT STDMETHODCALLTYPE GetDC(LPCRECT pRect, DWORD grfFlags, HDC* phDC);
    virtual HRESULT STDMETHODCALLTYPE ReleaseDC(HDC hDC);
    virtual HRESULT STDMETHODCALLTYPE InvalidateRect(LPCRECT pRect, BOOL fErase);
    virtual HRESULT STDMETHODCALLTYPE InvalidateRgn(HRGN hRGN, BOOL fErase);
    virtual HRESULT STDMETHODCALLTYPE ScrollRect(INT dx, INT dy, LPCRECT pRectScroll, LPCRECT pRectClip);
    virtual HRESULT STDMETHODCALLTYPE AdjustRect(LPRECT prc);
    virtual HRESULT STDMETHODCALLTYPE OnDefWindowMessage(UINT msg, WPARAM wParam, LPARAM lParam, LRESULT *plResult);

    // IOleControlSite
    virtual HRESULT STDMETHODCALLTYPE OnControlInfoChanged();
    virtual HRESULT STDMETHODCALLTYPE LockInPlaceActive(BOOL fLock);
    virtual HRESULT STDMETHODCALLTYPE GetExtendedControl(IDispatch** ppDisp);
    virtual HRESULT STDMETHODCALLTYPE TransformCoords(POINTL *pPtlHimetric, POINTF *pPtfContainer, DWORD dwFlags);
    virtual HRESULT STDMETHODCALLTYPE TranslateAccelerator(MSG* pMsg, DWORD grfModifiers);
    virtual HRESULT STDMETHODCALLTYPE OnFocus(BOOL fGotFocus);
    virtual HRESULT STDMETHODCALLTYPE ShowPropertyFrame();

    // IParseDisplayName
    virtual HRESULT STDMETHODCALLTYPE ParseDisplayName(
                        IBindCtx *pbc,
                        LPOLESTR pszDisplayName,
                        ULONG* pchEaten,
                        IMoniker** ppmkOut);

    // IOleContainer
    virtual HRESULT STDMETHODCALLTYPE EnumObjects(DWORD grfFlags, IEnumUnknown** ppenum);
    virtual HRESULT STDMETHODCALLTYPE LockContainer(BOOL fLock);

    // IDispatch
    virtual HRESULT STDMETHODCALLTYPE GetTypeInfoCount(UINT* pctinfo);
    virtual HRESULT STDMETHODCALLTYPE GetTypeInfo( 
                        UINT iTInfo, 
                        LCID lcid, 
                        ITypeInfo** ppTInfo);
    virtual HRESULT STDMETHODCALLTYPE GetIDsOfNames( 
                        REFIID riid, 
                        LPOLESTR* rgszNames, 
                        UINT cNames, 
                        LCID lcid, 
                        DISPID* rgDispId);
    virtual HRESULT STDMETHODCALLTYPE Invoke( 
                        DISPID dispIdMember, 
                        REFIID riid, 
                        LCID lcid, 
                        WORD wFlags, 
                        DISPPARAMS* pDispParams, 
                        VARIANT* pVarResult, 
                        EXCEPINFO* pExcepInfo, 
                        UINT* puArgErr);

    // IOleInPlaceUIWindow
    virtual HRESULT STDMETHODCALLTYPE GetBorder(LPRECT lprectBorder);
    virtual HRESULT STDMETHODCALLTYPE RequestBorderSpace(LPCBORDERWIDTHS pborderwidths);
    virtual HRESULT STDMETHODCALLTYPE SetBorderSpace(LPCBORDERWIDTHS pborderwidths);
    virtual HRESULT STDMETHODCALLTYPE SetActiveObject(IOleInPlaceActiveObject *pActiveObject, LPCOLESTR pszObjName);

    // IOleInPlaceFrame
    virtual HRESULT STDMETHODCALLTYPE InsertMenus(HMENU hmenuShared, LPOLEMENUGROUPWIDTHS lpMenuWidths);
    virtual HRESULT STDMETHODCALLTYPE SetMenu(HMENU hmenuShared, HOLEMENU holemenu, HWND hwndActiveObject);
    virtual HRESULT STDMETHODCALLTYPE RemoveMenus(HMENU hmenuShared);
    virtual HRESULT STDMETHODCALLTYPE SetStatusText(LPCOLESTR pszStatusText);
    virtual HRESULT STDMETHODCALLTYPE EnableModeless(BOOL fEnable);
    virtual HRESULT STDMETHODCALLTYPE TranslateAccelerator(LPMSG lpmsg, WORD wID);

    // IObjectWithSite
    virtual HRESULT STDMETHODCALLTYPE SetSite(IUnknown* pUnkSite);
    virtual HRESULT STDMETHODCALLTYPE GetSite(REFIID riid, void** ppvSite);

	// IServiceProvider
	virtual HRESULT STDMETHODCALLTYPE QueryService(REFGUID guidService, REFIID riid, void** ppvObject);

    // Message handlers
    LRESULT OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
    LRESULT OnEraseBackground(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
    LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
    LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
    LRESULT OnMouseActivate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
    LRESULT OnSetFocus(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
    LRESULT OnKillFocus(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
    LRESULT OnNcHitTest(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
    LRESULT OnContextMenu(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
    LRESULT OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

    LRESULT OnWindowlessMouseMessage(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
    LRESULT OnWindowMessage(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	LRESULT OnTimer(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

    LRESULT OnQueryInterface(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
    LRESULT OnPutMovieFromMemory(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
    LRESULT OnLoadMovieFromMemory(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
    LRESULT OnPutMovieUsingStream(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
    LRESULT OnLoadMovieUsingStream(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
    LRESULT OnGetFrameBitmap(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
    LRESULT OnPutStandardMenu(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
    LRESULT OnGetStandardMenu(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
    LRESULT OnPutOverallOpaque(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
    LRESULT OnGetOverallOpaque(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
    LRESULT OnSetEventListener(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
    LRESULT OnCallLoadExternalResource(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
    LRESULT OnCallFunction(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
    LRESULT OnSetReturnValueA(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
    LRESULT OnSetReturnValueW(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
    LRESULT OnGetObjectPtr(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

#define CFlashPlayerControlWindow CFPCWnd
#define _bstr_t f_in_box::com_helpers::CBSTR
#include "message_handlers.inl"

    friend class CFlashOCXLoader;
};

}

#endif // !__FPC_WND_H__171FB242_B8B1_4122_9AA9_99C36EBFCD90__
