#include "stdafx.h"
#include "FlashOCXLoader.h"
#include "AudioClient.h"
#include "AudioRenderClientProxy.h"
#include "ObjectWithCounter.h"
#include "ComPtr.h"

using namespace f_in_box::com_helpers;
using namespace f_in_box::std;

namespace f_in_box
{
	/// This is a proxy of IAudioRenderClient that passes all calls to underlying
	/// object, but additionally get IDirectSound8 returned by IAudioRenderClient::Activate() - so we could
	/// control audio output.
	class CAudioRenderClientProxy : 
		public CObjectWithCounter, 
		public IAudioRenderClient
	{
	private:
		/// Object that should receive all calls
		CComPtr<IAudioRenderClient> m_pUnderlyingObject;

		/// Main object
		com_helpers::CComPtr<CFlashOCXLoader> m_pLoader;

		/// The buffer for audio samples data returned by GetBuffer
		BYTE* m_pData;

		/// Audio format (WAVEFORMATEX)
		CArray<BYTE> m_DeviceFormat;

	public:
		explicit CAudioRenderClientProxy(const CArray<BYTE>& DeviceFormat, CFlashOCXLoader* pLoader, IAudioRenderClient* pUnderlyingObject) : 
			m_DeviceFormat(DeviceFormat), 
			m_pLoader(pLoader), 
			m_pUnderlyingObject(pUnderlyingObject)
		{
		}

        virtual ULONG __stdcall AddRef()
        {
			return CObjectWithCounter::AddRef();
        }

        virtual ULONG __stdcall Release()
        {
			return CObjectWithCounter::Release();
        }

		HRESULT STDMETHODCALLTYPE QueryInterface(REFIID riid, void** ppvObject)
		{
			if (f_in_box::com_helpers::IsEqualGUID(riid, IID_IUnknown))
				*ppvObject = this;
			else if (f_in_box::com_helpers::IsEqualGUID(riid, IID_IAudioRenderClient))
				*ppvObject = static_cast<IAudioRenderClient*>(this);
			else
				return m_pUnderlyingObject->QueryInterface(riid, ppvObject);

			AddRef();

			return S_OK;
		}

        virtual HRESULT STDMETHODCALLTYPE GetBuffer(UINT32 NumFramesRequested, BYTE** ppData)
		{
			HRESULT hr = m_pUnderlyingObject->GetBuffer(NumFramesRequested, ppData);
			if (S_OK != hr)
			{
				return hr;
			}

			if (0 != NumFramesRequested)
			{
				m_pData = *ppData;
			}
			else
			{
				m_pData = NULL;
			}

			return hr;
		}

		virtual HRESULT STDMETHODCALLTYPE ReleaseBuffer(UINT32 NumFramesWritten, DWORD dwFlags)
		{
			if (NULL != m_pData && 0 != NumFramesWritten)
			{
				WAVEFORMATEX* pFormat = (WAVEFORMATEX*)(LPBYTE)m_DeviceFormat;

				m_pLoader->OnProcessAudio(pFormat, NumFramesWritten, m_pData);
			}

			return m_pUnderlyingObject->ReleaseBuffer(NumFramesWritten, dwFlags);
		}
	};

	HRESULT CreateAudioRenderClientProxy(const CArray<BYTE>& DeviceFormat, CFlashOCXLoader* pLoader, IAudioRenderClient* pOriginalAudioRenderClient, IAudioRenderClient** ppProxyAudioRenderClient)
	{
		CComPtr<IAudioRenderClient> pProxy = new CAudioRenderClientProxy(DeviceFormat, pLoader, pOriginalAudioRenderClient);

		return pProxy->QueryInterface(IID_IAudioRenderClient, (void**)ppProxyAudioRenderClient);
	}
}
