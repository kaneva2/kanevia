#ifndef __CONTENT_PROVIDER_H__6D767BC0_70BC_459b_8804_0AD21EE54B51__
#define __CONTENT_PROVIDER_H__6D767BC0_70BC_459b_8804_0AD21EE54B51__

namespace f_in_box
{

class IContentProvider : public IUnknown
{
public:
	virtual HRESULT __stdcall SetBindStatusCallback(IBindStatusCallback* pBindStatusCallback) = 0;

	// S_OK - saved, S_FALSE - not saved
	virtual HRESULT __stdcall IsDataSaved() = 0;

	virtual HRESULT __stdcall EndOfData() = 0;

    virtual HRESULT __stdcall Write(const void *pv, ULONG cb, ULONG *pcbWritten) = 0;

	virtual HRESULT __stdcall SetSize(ULARGE_INTEGER libNewSize) = 0;
};

// Create content provider and stream which can be used for writing data
// ppStream �������� ������
// ppContentProvider ��������� ������ content manager'�
HRESULT CreateContentProvider(IContentProvider** ppContentProvider, IStream** ppStream);

}

#endif // !__CONTENT_PROVIDER_H__6D767BC0_70BC_459b_8804_0AD21EE54B51__
