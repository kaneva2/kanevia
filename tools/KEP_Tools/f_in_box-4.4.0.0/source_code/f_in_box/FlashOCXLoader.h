#ifndef __FLASH_OCX_LOADER_H__EFF85F23_0755_44fb_A74D_69F1AF95C3FA__
#define __FLASH_OCX_LOADER_H__EFF85F23_0755_44fb_A74D_69F1AF95C3FA__

#include "String.h"
#include "Map.h"
#include "MemDLL.h"
#include "ContentManager.h"
#include "Array.h"
#include "CodeReplacer.h"
#include "AudioClient.h"
#include "ObjectWithCounter.h"
#include "ntdefs.h"

#define DEF_NO_FOCUS_HWND (HWND(-1))

namespace f_in_box
{
    class CFlashOCXLoader :
        public std::CObjectWithCounter,
        public CMemDLL
    {
    private:
        //
        CContentManager m_ContentManager;

        // As we change window class names, we should store old and new names
        f_in_box::std::CMap<f_in_box::std::CString<CHAR>, f_in_box::std::CString<CHAR> > m_map_OldClassName2NewClassName;

        //
        f_in_box::std::CMap<WORD, HCURSOR> m_mapCursorId2Handler__IsIntresource;
        f_in_box::std::CMap<f_in_box::std::CString<CHAR>, HCURSOR> m_mapCursorId2Handler__StringResource;

        //
        CCriticalSection m_CriticalSection__AudioFunctions;
        BOOL m_bAudioEnabled;
        DWORD m_dwAudioVolume;
        f_in_box::std::CMap<HWAVEOUT, WORD> m_DeviceBytesPerSample;
	    f_in_box::std::CMap<HWAVEOUT, f_in_box::std::CArray<BYTE> > m_DeviceFormat;

        //
        ATOM m_atomWindowClassA;
        ATOM m_atomWindowClassW;

        //
        f_in_box::std::CString<CHAR> m_strWindowClassNameA;
        f_in_box::std::CString<WCHAR> m_strWindowClassNameW;

        //
        PVOID m_lpAdapterCode;

	    //
	    UINT_PTR m_nTimerId;

	    //
	    PSOUNDLISTENER m_pSoundListener;
	    LPARAM m_SoundListenerlParam;

	    //
	    PPREPROCESSURLHANDLER m_pPreProcessURLHandler;
	    LPARAM m_PreProcessURLHandlerlParam;

	    // Time of sound capturing start
	    DWORD m_dwStartTimeForSoundCapturing;

    

	    f_in_box::std::CString<CHAR> m_strContext;

    

	    BOOL m_bShouldFree;

	    DWORD m_dwTlsIndex__FocusedHWND;

	    CCriticalSection m_cs;

	    f_in_box::std::CMap<DWORD, DWORD> m_Options;

	    /// Can Flash use GPU accelaration?
	    BOOL m_bCanFlashUseGPUAcceleration;

	    /// Is m_bCanFlashUseGPUAcceleration inited?
	    BOOL m_bIsCanFlashUseGPUAccelerationInited;

	    /// We are within a test of using GPU acceleration?
	    BOOL m_bWithinGPUAccelerationTest;

	    /// Set of HDC that were allocated by CFPCWnd::GetDC, we need them
	    /// to properly handle GetClipBox
        f_in_box::std::CMap<HDC, HDC> m_FPCWndHDC;

	    /// m_FPCWndHDC lock
	    CCriticalSection m_csFPCWndHDC;

	    /// Set of IAudioClient created by Flash; f-in-box uses it to adjust volume
        f_in_box::std::CMap<PVOID, f_in_box::com_helpers::CComPtr<IAudioClient> > m_AudioClientCollection;

	    /// m_AudioClientCollection lock
	    CCriticalSection m_csAudioClientCollection;

    private:
        CFlashOCXLoader();
        virtual ~CFlashOCXLoader();

    public:
        static com_helpers::CComPtr<CFlashOCXLoader> Create()
        {
            return new CFlashOCXLoader();
        }

        HRESULT Load(LPVOID lpCode = NULL, 
                     DWORD dwSizeOfCode = 0, 
                     LPCSTR szClassNameA = NULL, 
                     LPCWSTR szClassNameW = NULL);

        ATOM GetClassAtomA();
        ATOM GetClassAtomW();

        LPCSTR GetClassNameA();
        LPCWSTR GetClassNameW();

        CContentManager& GetContentManager();

	    BOOL GetModuleVersionEx(SFPCVersion* pVersion);

        DWORD GetAudioVolume();
        void SetAudioVolume(DWORD dwVolume);

        BOOL GetAudioEnabled();
        void SetAudioEnabled(BOOL bEnable);

	    void StopMinimizeMemoryTimer();

	    DWORD SetSoundListener(PSOUNDLISTENER pSoundListener, LPARAM lParam);

	    DWORD SetPreProcessURLHandler(PPREPROCESSURLHANDLER pHandler, LPARAM lParam);

	    void SetContext(LPCSTR szContext);

	    BOOL CanUnloadNow();
	    BOOL UnloadCode();
	    void UnloadIfShould();

	    void SaveFocus(HWND hwnd)
	    {
		    TlsSetValue(m_dwTlsIndex__FocusedHWND, (LPVOID)hwnd);
	    }
	    HWND GetSavedFocus()
	    {
		    return (HWND)TlsGetValue(m_dwTlsIndex__FocusedHWND);
	    }

	    DWORD GetOptionValue(DWORD dwOption)
	    {
		    CCriticalSectionOwner lock(m_cs);

		    DWORD dwValue;
		    if (m_Options.Find(dwOption, dwValue))
			    return dwValue;
		    else
			    return -1;
	    }

	    void SetOptionValue(DWORD dwOption, DWORD dwValue)
	    {
		    CCriticalSectionOwner lock(m_cs);

		    m_Options.Add(dwOption, dwValue);
	    }

	    /// Tests whether Flash can use DirectDraw's hardware (GPU) acceleration or not
	    /// Flash 11 has issue with some videocards: https://bugbase.adobe.com/index.cfm?event=bug&id=3019125
	    /// So we load a semitransparent movie with fully transparent top level pixel
	    /// Then we check whether it's transparent or not: if not, we have to use software emulation
	    /// when working with DirectDraw
	    void TestCanFlashUseGPUAcceleration();

	    BOOL CanFlashUseGPUAcceleration() const
	    {
		    return m_bCanFlashUseGPUAcceleration;
	    }

	    BOOL WithinGPUAccelerationTest() const
	    {
		    return m_bWithinGPUAccelerationTest;
	    }

	    void OnProcessAudio(PWAVEFORMATEX pFormat, UINT nNumFramesWritten, PVOID pData);

	    void FPCWndHDC_Add(HDC hDC)
	    {
		    CCriticalSectionOwner lock(m_csFPCWndHDC);

		    m_FPCWndHDC.Add(hDC, hDC);
	    }

	    void FPCWndHDC_Remove(HDC hDC)
	    {
		    CCriticalSectionOwner lock(m_csFPCWndHDC);

		    m_FPCWndHDC.RemoveByKey(hDC);
	    }

	    void AddAudioClient(IAudioClient* pAudioClient)
        {
		    CCriticalSectionOwner lock(m_csAudioClientCollection);

            m_AudioClientCollection.Add((PVOID)pAudioClient, pAudioClient);

            UpdateAudioClientVolumeAndMuteStatus(pAudioClient);
        }

	    void RemoveAudioClient(IAudioClient* pAudioClient)
        {
		    CCriticalSectionOwner lock(m_csAudioClientCollection);

            m_AudioClientCollection.RemoveByKey(pAudioClient);
        }

    private:

        BOOL OnProcessImport(LPCSTR lpszLibName, 
                             LPCSTR lpszFuncName, 
                             LPVOID& pNewAddress, 
                             LPVOID& lpParam);

        void SaveLoadedCursor(LPCSTR lpszCursorName, HCURSOR hCursor);
        void FreeLoadedCursors();
        HCURSOR FindLoadedCursor(LPCSTR lpszCursorName);

        /// Adjusts volume considering passed buffer as a set of nCount samples of TSampleType (can be BYTE, WORD, DWORD etc.)
        template <class TSampleType>
        void AdjustVolume(WORD wBytesPerSample, PVOID pData, int nCount);

        /// Sets current audio volume for each of an audio client
        void UpdateAudioClientVolumeAndMuteStatus(IAudioClient* pAudioClient);

        /// Sets current audio volume for each of current audio clients
        void UpdateAudioClientsVolumeAndMuteStatus();

        //
        static
        LRESULT
        CALLBACK
        StaticWindowProc(
            HWND hWnd, 
            UINT uMsg, 
            WPARAM wParam, 
            LPARAM lParam 
        );

        //
        // Functions that we should intercept
        //

        static
        HRESULT
        WINAPI
        StaticHook_CreateURLMoniker(
            IMoniker* pmkContext, 
            LPWSTR szURL, 
            IMoniker** ppmk
        );
        HRESULT
        WINAPI
        Hook_CreateURLMoniker(
            LPVOID lpOriginalFunc, 

            IMoniker* pmkContext, 
            LPWSTR szURL, 
            IMoniker** ppmk
        );

        static
        HWND
        WINAPI
        StaticHook_CreateWindowExA(
            DWORD dwExStyle,      // extended window style
            LPCSTR lpClassName,  // registered class name
            LPCSTR lpWindowName, // window name
            DWORD dwStyle,        // window style
            int x,                // horizontal position of window
            int y,                // vertical position of window
            int nWidth,           // window width
            int nHeight,          // window height
            HWND hWndParent,      // handle to parent or owner window
            HMENU hMenu,          // menu handle or child identifier
            HINSTANCE hInstance,  // handle to application instance
            LPVOID lpParam        // window-creation data
        );
        HWND
        WINAPI
        Hook_CreateWindowExA(
            LPVOID lpOriginalFunc, 

            DWORD dwExStyle,      // extended window style
            LPCSTR lpClassName,  // registered class name
            LPCSTR lpWindowName, // window name
            DWORD dwStyle,        // window style
            int x,                // horizontal position of window
            int y,                // vertical position of window
            int nWidth,           // window width
            int nHeight,          // window height
            HWND hWndParent,      // handle to parent or owner window
            HMENU hMenu,          // menu handle or child identifier
            HINSTANCE hInstance,  // handle to application instance
            LPVOID lpParam        // window-creation data
        );

        static
        ATOM
        WINAPI
        StaticHook_RegisterClassExA(
            CONST WNDCLASSEXA *lpwcx  // class data
        );
        ATOM
        WINAPI
        Hook_RegisterClassExA(
            LPVOID lpOriginalFunc, 

            CONST WNDCLASSEXA *lpwcx  // class data
        );

        static
        ATOM
        WINAPI
        StaticHook_RegisterClassA(
            CONST WNDCLASSA *lpwcx  // class data
        );
        ATOM
        WINAPI
        Hook_RegisterClassA(
            LPVOID lpOriginalFunc, 

            CONST WNDCLASSA *lpwcx  // class data
        );

        static
        HCURSOR
        WINAPI
        StaticHook_LoadCursorA(
            HINSTANCE hInstance,  // handle to application instance
            LPCSTR lpCursorName  // name or resource identifier
        );
        HCURSOR
        WINAPI
        Hook_LoadCursorA(
            LPVOID lpOriginalFunc, 

            HINSTANCE hInstance,  // handle to application instance
            LPCSTR lpCursorName  // name or resource identifier
        );

        static
        BOOL
        WINAPI
        StaticHook_UnregisterClassA(
            LPCSTR lpClassName,  // class name
            HINSTANCE hInstance   // handle to application instance
        );
        BOOL
        WINAPI
        Hook_UnregisterClassA(
            LPVOID lpOriginalFunc, 

            LPCSTR lpClassName,  // class name
            HINSTANCE hInstance   // handle to application instance
        );

        static
        HMENU
        WINAPI
        StaticHook_LoadMenuA(
            HINSTANCE hInstance,  // handle to module
            LPCSTR lpMenuName    // menu name or resource identifier
        );
        HMENU
        WINAPI
        Hook_LoadMenuA(
            LPVOID lpOriginalFunc, 

            HINSTANCE hInstance,  // handle to module
            LPCSTR lpMenuName    // menu name or resource identifier
        );

        static
        MMRESULT
        WINAPI
        StaticHook_waveOutWrite(
            HWAVEOUT hwo,  
            LPWAVEHDR pwh, 
            UINT cbwh      
        );
        MMRESULT
        WINAPI
        Hook_waveOutWrite(
            LPVOID lpOriginalFunc, 

            HWAVEOUT hwo,  
            LPWAVEHDR pwh, 
            UINT cbwh      
        );

        static
        MMRESULT
        WINAPI
        StaticHook_waveOutOpen(
            LPHWAVEOUT     phwo,      
            UINT_PTR       uDeviceID, 
            LPWAVEFORMATEX pwfx,      
            DWORD          dwCallback,
            DWORD          dwCallbackInstance,
            DWORD          fdwOpen    
        );
        MMRESULT
        WINAPI
        Hook_waveOutOpen(
            LPVOID lpOriginalFunc, 

            LPHWAVEOUT     phwo,      
            UINT_PTR       uDeviceID, 
            LPWAVEFORMATEX pwfx,      
            DWORD          dwCallback,
            DWORD          dwCallbackInstance,
            DWORD          fdwOpen    
        );

        static
        MMRESULT
        WINAPI
        StaticHook_waveOutClose(
            HWAVEOUT hwo  
        );
        MMRESULT
        WINAPI
        Hook_waveOutClose(
            LPVOID lpOriginalFunc, 

            HWAVEOUT hwo  
        );

        static
        HANDLE
        WINAPI
        StaticHook_CreateFileA(
            LPCSTR lpFileName,
            DWORD dwDesiredAccess,
            DWORD dwShareMode,
            LPSECURITY_ATTRIBUTES lpSecurityAttributes,
            DWORD dwCreationDisposition,
            DWORD dwFlagsAndAttributes,
            HANDLE hTemplateFile
        );
        HANDLE
        WINAPI
        Hook_CreateFileA(
            LPVOID lpOriginalFunc, 

            LPCSTR lpFileName,
            DWORD dwDesiredAccess,
            DWORD dwShareMode,
            LPSECURITY_ATTRIBUTES lpSecurityAttributes,
            DWORD dwCreationDisposition,
            DWORD dwFlagsAndAttributes,
            HANDLE hTemplateFile
        );

        static
        HANDLE
        WINAPI
        StaticHook_CreateFileW(
            LPCWSTR lpFileName,
            DWORD dwDesiredAccess,
            DWORD dwShareMode,
            LPSECURITY_ATTRIBUTES lpSecurityAttributes,
            DWORD dwCreationDisposition,
            DWORD dwFlagsAndAttributes,
            HANDLE hTemplateFile
        );
        HANDLE
        WINAPI
        Hook_CreateFileW(
            LPVOID lpOriginalFunc, 

            LPCWSTR lpFileName,
            DWORD dwDesiredAccess,
            DWORD dwShareMode,
            LPSECURITY_ATTRIBUTES lpSecurityAttributes,
            DWORD dwCreationDisposition,
            DWORD dwFlagsAndAttributes,
            HANDLE hTemplateFile
        );

        static
        BOOL
        WINAPI
        StaticHook_FlushInstructionCache(
            HANDLE hProcess,
            LPCVOID lpBaseAddress,
            SIZE_T dwSize
        );
        BOOL
        WINAPI
        Hook_FlushInstructionCache(
            LPVOID lpOriginalFunc, 

            HANDLE hProcess,
            LPCVOID lpBaseAddress,
            SIZE_T dwSize
        );

        static
        HMODULE
        WINAPI
        StaticHook_LoadLibraryA(
            LPCSTR lpFileName
        );
        HMODULE
        WINAPI
        Hook_LoadLibraryA(
            LPVOID lpOriginalFunc, 

            LPCSTR lpFileName
        );

        static
        BOOL
        WINAPI
	    StaticHook_GetClassInfoExA(
		    HINSTANCE hinst,
		    LPCSTR lpszClass,
		    LPWNDCLASSEXA lpwcx
	    );
	    BOOL
	    WINAPI
	    Hook_GetClassInfoExA(
            LPVOID lpOriginalFunc, 

		    HINSTANCE hinst,
		    LPCSTR lpszClass,
		    LPWNDCLASSEXA lpwcx
	    );

        static
        int
        WINAPI
	    StaticHook_GetClassNameA(
		    HWND hWnd,
		    LPSTR lpClassName,
		    int nMaxCount
	    );
	    int
	    WINAPI
	    Hook_GetClassNameA(
            LPVOID lpOriginalFunc, 

		    HWND hWnd,
		    LPSTR lpClassName,
		    int nMaxCount
	    );

	    static
	    HMODULE
	    WINAPI
	    StaticHook_GetModuleHandleA(
		    LPCSTR lpModuleName
	    );
	    HMODULE
	    WINAPI
	    Hook_GetModuleHandleA(
		    LPVOID lpOriginalFunc, 

		    LPCSTR lpModuleName
	    );

        static
        HMODULE
        WINAPI
        StaticHook_LoadLibraryExA(
            LPCSTR lpFileName, 
		    HANDLE hFile,
		    DWORD dwFlags
        );
        HMODULE
        WINAPI
        Hook_LoadLibraryExA(
            LPVOID lpOriginalFunc, 

            LPCSTR lpFileName, 
		    HANDLE hFile,
		    DWORD dwFlags
        );

	    static
	    HRSRC
	    WINAPI
	    StaticHook_FindResourceA(
		    HMODULE hModule,
		    LPCSTR lpName,
		    LPCSTR lpType
	    );
	    HRSRC
	    WINAPI
	    Hook_FindResourceA(
            LPVOID lpOriginalFunc, 

		    HMODULE hModule,
		    LPCSTR lpName, 
		    LPCSTR lpType
	    );

	    static
	    HRSRC
	    WINAPI
	    StaticHook_FindResourceExA(
		    HMODULE hModule,
		    LPCSTR lpType, 
		    LPCSTR lpName, 
		    WORD wLanguage
	    );
	    HRSRC
	    WINAPI
	    Hook_FindResourceExA(
            LPVOID lpOriginalFunc, 

		    HMODULE hModule,
		    LPCSTR lpType, 
		    LPCSTR lpName, 
		    WORD wLanguage
	    );

	    static
	    HRSRC
	    WINAPI
	    StaticHook_FindResourceExW(
		    HMODULE hModule,
		    LPCWSTR lpType, 
		    LPCWSTR lpName, 
		    WORD wLanguage
	    );
	    HRSRC
	    WINAPI
	    Hook_FindResourceExW(
            LPVOID lpOriginalFunc, 

		    HMODULE hModule,
		    LPCWSTR lpType, 
		    LPCWSTR lpName, 
		    WORD wLanguage
	    );

	    static
	    UINT
	    WINAPI
	    StaticHook_SetTimer(
		    HWND hWnd,
		    UINT_PTR nIDEvent,
		    UINT uElapse,
		    TIMERPROC lpTimerFunc
	    );
	    UINT
	    WINAPI
	    Hook_SetTimer(
            LPVOID lpOriginalFunc, 

		    HWND hWnd,
		    UINT_PTR nIDEvent,
		    UINT uElapse,
		    TIMERPROC lpTimerFunc
	    );

	    static
	    LPVOID
	    WINAPI
	    StaticHook_VirtualAlloc(
		    IN LPVOID lpAddress,
		    IN SIZE_T dwSize,
		    IN DWORD flAllocationType,
		    IN DWORD flProtect
	    );

	    LPVOID
	    WINAPI
	    Hook_VirtualAlloc(
            LPVOID lpOriginalFunc, 

		    IN LPVOID lpAddress,
		    IN SIZE_T dwSize,
		    IN DWORD flAllocationType,
		    IN DWORD flProtect
	    );

	    static
	    BOOL
	    WINAPI
	    StaticHook_VirtualProtect(
		    LPVOID lpAddress,
            SIZE_T dwSize,
            DWORD flNewProtect,
            PDWORD lpflOldProtect
	    );

	    BOOL
	    WINAPI
	    Hook_VirtualProtect(
            LPVOID lpOriginalFunc, 

		    LPVOID lpAddress,
            SIZE_T dwSize,
            DWORD flNewProtect,
            PDWORD lpflOldProtect
	    );

        static
        BOOL
        WINAPI
        StaticHook_ReadFile(
		    HANDLE hFile,
		    LPVOID lpBuffer,
		    DWORD nNumberOfBytesToRead,
		    LPDWORD lpNumberOfBytesRead,
		    LPOVERLAPPED lpOverlapped
	    );
        BOOL
        WINAPI
        Hook_ReadFile(
            LPVOID lpOriginalFunc, 

		    HANDLE hFile,
		    LPVOID lpBuffer,
		    DWORD nNumberOfBytesToRead,
		    LPDWORD lpNumberOfBytesRead,
		    LPOVERLAPPED lpOverlapped
        );

        static
        BOOL
        WINAPI
        StaticHook_CloseHandle(
		    HANDLE hObject
	    );
        BOOL
        WINAPI
        Hook_CloseHandle(
            LPVOID lpOriginalFunc, 

		    HANDLE hObject
        );

        static
        DWORD
        WINAPI
        StaticHook_GetFileSize(
		    HANDLE hFile,
		    LPDWORD lpFileSizeHigh
	    );
        DWORD
        WINAPI
        Hook_GetFileSize(
            LPVOID lpOriginalFunc, 

		    HANDLE hFile,
		    LPDWORD lpFileSizeHigh
        );

        static
        BOOL
        WINAPI
        StaticHook_GetFileSizeEx(
		    HANDLE hFile, 
		    PLARGE_INTEGER lpFileSize
	    );
        BOOL
        WINAPI
        Hook_GetFileSizeEx(
            LPVOID lpOriginalFunc, 

		    HANDLE hFile, 
		    PLARGE_INTEGER lpFileSize
        );

        static
        DWORD
        WINAPI
        StaticHook_SetFilePointer(
		    HANDLE hFile,
		    LONG lDistanceToMove,
		    PLONG lpDistanceToMoveHigh,
		    DWORD dwMoveMethod
	    );
        DWORD
        WINAPI
        Hook_SetFilePointer(
            LPVOID lpOriginalFunc, 

		    HANDLE hFile,
		    LONG lDistanceToMove,
		    PLONG lpDistanceToMoveHigh,
		    DWORD dwMoveMethod
        );

        static
        BOOL
        WINAPI
        StaticHook_SetFilePointerEx(
		    HANDLE hFile,
		    LARGE_INTEGER liDistanceToMove,
		    PLARGE_INTEGER lpNewFilePointer,
		    DWORD dwMoveMethod
	    );
        BOOL
        WINAPI
        Hook_SetFilePointerEx(
            LPVOID lpOriginalFunc, 

		    HANDLE hFile,
		    LARGE_INTEGER liDistanceToMove,
		    PLARGE_INTEGER lpNewFilePointer,
		    DWORD dwMoveMethod
        );

        static
        HWND
        WINAPI
        StaticHook_GetFocus(
	    );
        HWND
        WINAPI
        Hook_GetFocus(
            LPVOID lpOriginalFunc
        );

        static
	    HRESULT
	    WINAPI 
	    StaticHook_CoCreateInstance(
		    REFCLSID rclsid, 
		    LPUNKNOWN pUnkOuter, 
		    DWORD dwClsContext, 
		    REFIID riid, 
		    LPVOID *ppv
	    );
	    HRESULT
        WINAPI
        Hook_CoCreateInstance(
            LPVOID lpOriginalFunc, 

		    REFCLSID rclsid, 
		    LPUNKNOWN pUnkOuter, 
		    DWORD dwClsContext, 
		    REFIID riid, 
		    LPVOID *ppv
        );

        static
	    int
	    WINAPI 
	    StaticHook_GetClipBox(
		    HDC hDC,
		    LPRECT lpRect
	    );
	    int
        WINAPI
        Hook_GetClipBox(
            LPVOID lpOriginalFunc, 

		    HDC hDC,
		    LPRECT lpRect
        );

        static
	    LPVOID
	    WINAPI 
	    StaticHook_RtlPcToFileHeader(
		    PVOID PcValue,
		    PVOID* BaseOfImage
	    );
	    LPVOID
        WINAPI
        Hook_RtlPcToFileHeader(
            LPVOID lpOriginalFunc, 

		    PVOID PcValue,
		    PVOID* BaseOfImage
        );

        static
	    BOOL
	    WINAPI 
	    StaticHook_SetProcessValidCallTargets(
            HANDLE hProcess,
            PVOID VirtualAddress,
            SIZE_T RegionSize,
            ULONG NumberOfOffsets,
            PCFG_CALL_TARGET_INFO OffsetInformation);
    };
}

#endif // !__FLASH_OCX_LOADER_H__EFF85F23_0755_44fb_A74D_69F1AF95C3FA__
