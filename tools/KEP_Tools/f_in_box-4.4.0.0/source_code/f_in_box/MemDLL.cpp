#include "stdafx.h"
#include "MemDLL.h"
#include "String.h"
#include "Array.h"
#include "MemManager.h"
#include "thunks.h"
#include "Globals.h"
#include "ntdefs.h"

using namespace f_in_box;

CMemDLL::CMemDLL() : 
    m_pImageBase(NULL),
    m_pFlashTlsPointerData(NULL),
    m_pFlashTlsData(NULL),
    m_pCodeThatReturnsSavedTlsPtr(NULL)
{
}

CMemDLL::~CMemDLL()
{
    if (NULL != m_pImageBase)
        Unload();

    //f_in_box::std::POSITION pos = m_list_Adapters.GetFirstPosition();

    //while (NULL != pos)
    //    VirtualFree(m_list_Adapters.GetNext(pos), 0, MEM_RELEASE);

    f_in_box::std::POSITION pos = m_Adapters.GetFirstPosition();
	SAdapterInfo AdapterInfo;
	LPVOID pThunk;

    while (NULL != pos)
	{
		m_Adapters.GetNext(pos, pThunk, AdapterInfo);
		VirtualFree(AdapterInfo.m_pThunk, 0, MEM_RELEASE);
	}

    if (NULL != m_pFlashTlsPointerData)
        VirtualFree(m_pFlashTlsPointerData, 0, MEM_RELEASE);

    if (NULL != m_pFlashTlsData)
        HeapFree(GetProcessHeap(), 0, m_pFlashTlsData);

    if (NULL != m_pCodeThatReturnsSavedTlsPtr)
        VirtualFree(m_pCodeThatReturnsSavedTlsPtr, 0, MEM_RELEASE);
}

HRESULT CMemDLL::Load(LPCVOID pData, DWORD dwSize)
{
    if (NULL != m_pImageBase)
        Unload();

    // Current pointer
    char* pPtr = (char*)pData;

    pPtr += ((PIMAGE_DOS_HEADER)pPtr)->e_lfanew;

    PIMAGE_NT_HEADERS pImageNTHeaders = (PIMAGE_NT_HEADERS)pPtr;

    m_dwImageSize = pImageNTHeaders->OptionalHeader.SizeOfImage;

    // Alloc memory for library
    PVOID pImageBase = VirtualAlloc(NULL, pImageNTHeaders->OptionalHeader.SizeOfImage, MEM_COMMIT, PAGE_EXECUTE_READWRITE);

    // Copy headers
    f_in_box::MemoryCopy(pImageBase, pData, pImageNTHeaders->OptionalHeader.SizeOfHeaders);

// Sections

    // Set pPtr to section table
    pPtr += sizeof(pImageNTHeaders->Signature) + 
            sizeof(pImageNTHeaders->FileHeader) + 
            pImageNTHeaders->FileHeader.SizeOfOptionalHeader;

    for (int intSectionIndex = 0; intSectionIndex < pImageNTHeaders->FileHeader.NumberOfSections; intSectionIndex++)
    {
        PIMAGE_SECTION_HEADER pImageSectionHeader = (PIMAGE_SECTION_HEADER)pPtr + intSectionIndex;

        //
        int intVirtualSectionSize = pImageSectionHeader->Misc.VirtualSize;
        //
        int intRawSectionSize = pImageSectionHeader->SizeOfRawData;

        PVOID pSectionBase = (char*)pImageBase + pImageSectionHeader->VirtualAddress;

        f_in_box::MemoryZero(pSectionBase, intVirtualSectionSize);

        f_in_box::MemoryCopy(pSectionBase, 
                   (char*)pData + pImageSectionHeader->PointerToRawData, 
                        // Sometimes 0 == intRawSectionSize, for example, when the section has uninitialized data
                   min(intVirtualSectionSize, intRawSectionSize));
    }

// Relocations

    // Difference between bases
    DWORD_PTR nImageBaseDelta = (LPBYTE)pImageBase - (LPBYTE)pImageNTHeaders->OptionalHeader.ImageBase;

    int intRelocationInfoSize = 
        pImageNTHeaders->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_BASERELOC].Size;

    PIMAGE_BASE_RELOCATION pImageBaseRelocations = 
        (PIMAGE_BASE_RELOCATION)((char*)pImageBase + 
                                pImageNTHeaders->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_BASERELOC].VirtualAddress);

    PIMAGE_BASE_RELOCATION pReloc = pImageBaseRelocations;

    while ((char*)pReloc - (char*)pImageBaseRelocations < intRelocationInfoSize)
    {
        // Number of relocs into current block
        int intRelocCount = (pReloc->SizeOfBlock - sizeof(*pReloc)) / sizeof(WORD);

        PWORD pwRelocInfo = (PWORD)((char*)pReloc + sizeof(*pReloc));

        for (int intRelocIndex = 0; intRelocIndex < intRelocCount; intRelocIndex++)
        {
            WORD wRelocInfo = *pwRelocInfo;

            switch (wRelocInfo >> 12)
            {
                case IMAGE_REL_BASED_DIR64: 
                case IMAGE_REL_BASED_HIGHLOW: 
                {
                    PDWORD_PTR p = (PDWORD_PTR)((char*)pImageBase + pReloc->VirtualAddress + (wRelocInfo & 0x0fff));

                    DWORD_PTR value = *(DWORD_PTR*)p;

                    value += nImageBaseDelta;

                    *p = value;

                    break;
                }
                default: 
                {
                    break;
                }
            }

            pwRelocInfo++;
        }

        pReloc = (PIMAGE_BASE_RELOCATION)pwRelocInfo;
    }

// Import table

    PIMAGE_IMPORT_DESCRIPTOR pImports = 
        (PIMAGE_IMPORT_DESCRIPTOR)((char*)pImageBase + 
                                pImageNTHeaders->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].VirtualAddress);

    PIMAGE_IMPORT_DESCRIPTOR pImport = pImports;

    while (NULL != pImport->Name)
    {
        // DLL Name
        LPSTR lpszLibName = (LPSTR)((PCHAR)pImageBase + pImport->Name);

        // Load library
	    HMODULE hLib = LoadLibraryA(lpszLibName);

		m_LoadedModules.Add(hLib);

        DWORD_PTR* lpPRVA_Import = 
            0 == pImport->TimeDateStamp ?
                (DWORD_PTR*)(pImport->FirstThunk + (PCHAR)pImageBase) :
                (DWORD_PTR*)(pImport->OriginalFirstThunk + (PCHAR)pImageBase);

        while (*lpPRVA_Import)
        {
            // Get function name
            LPSTR lpszFunctionName;

            if (*lpPRVA_Import & IMAGE_ORDINAL_FLAG)
                // Importing by number (ordinal)
                // Ordinal is in the loword
            {
                lpszFunctionName = (LPSTR)(DWORD_PTR)(*lpPRVA_Import & 0xffff);

                // Get function address
                *(LPVOID*)lpPRVA_Import = ::GetProcAddress(hLib, lpszFunctionName);
            }
            else
                // Importing by name
            {
                lpszFunctionName = (LPSTR)((PIMAGE_IMPORT_BY_NAME)((PCHAR)pImageBase + *lpPRVA_Import))->Name;

                LPVOID lpOriginalAddress = ::GetProcAddress(hLib, lpszFunctionName);

                LPVOID lpNewAddress;
                LPVOID lpParam;

                if (OnProcessImport(lpszLibName, lpszFunctionName, lpNewAddress, lpParam))
                    // We are ready to intercept
                    *(LPVOID*)lpPRVA_Import = CreateOrFindThunk(lpOriginalAddress, lpNewAddress, lpParam);
                else
                    *(LPVOID*)lpPRVA_Import = lpOriginalAddress;

				// save where what functions are resides in import table; it's for HookFunc method
				SImportTableEntry ImportTableEntry(lpszLibName, lpszFunctionName, (LPVOID*)lpPRVA_Import);
				m_list_ImportTableEntries.Add(ImportTableEntry);
            }

            ASSERT(NULL != *(LPVOID*)lpPRVA_Import);

            lpPRVA_Import++;
        }

        pImport++;
    }

    FlushInstructionCache(GetCurrentProcess(), pImageBase, pImageNTHeaders->OptionalHeader.SizeOfImage);

    if (IMAGE_FILE_MACHINE_AMD64 == pImageNTHeaders->FileHeader.Machine)
    {
        IMAGE_DATA_DIRECTORY FlashTlsDataDirectory = pImageNTHeaders->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_TLS];
        if (FlashTlsDataDirectory.Size > 0)
        {
            PIMAGE_TLS_DIRECTORY pFlashTlsDirectory = (PIMAGE_TLS_DIRECTORY)((PBYTE)pImageBase + FlashTlsDataDirectory.VirtualAddress);
            DWORD nFlashTlsDataSize = pFlashTlsDirectory->EndAddressOfRawData - pFlashTlsDirectory->StartAddressOfRawData + pFlashTlsDirectory->SizeOfZeroFill;

            PTEB pTEB = (PTEB)NtCurrentTeb();

            // Let's allocate it nearby the ThreadLocalStoragePointer to make sure that offset below is 32 bit
            m_pFlashTlsPointerData = VirtualAllocWithin32BitOffset(pTEB->ThreadLocalStoragePointer, sizeof(PVOID), PAGE_READWRITE);
            m_pFlashTlsData = HeapAlloc(GetProcessHeap(), 0, nFlashTlsDataSize);
            MemoryCopy(m_pFlashTlsPointerData, &m_pFlashTlsData, sizeof(m_pFlashTlsData));
            MemoryCopy(m_pFlashTlsData, (PVOID)(DWORD_PTR)pFlashTlsDirectory->StartAddressOfRawData, nFlashTlsDataSize);

            DWORD nOffset = (PBYTE)m_pFlashTlsPointerData - (PBYTE)pTEB->ThreadLocalStoragePointer;

            // AddressOfIndex will have the value that being used as:
            // pTEB->ThreadLocalStoragePointer[nOffset] will point exactly to pTlsPointerData, that in its turn contains TLS data
            // from the flash ocx dll
            *(DWORD*)pFlashTlsDirectory->AddressOfIndex = nOffset / sizeof(PVOID);

            // But the problem is that pTEB->ThreadLocalStoragePointer then might be changed (see ticket #8)
            // So let's patch the flash code that gets pTEB->ThreadLocalStoragePointer

            PVOID pCurrentThreadLocalStoragePointer = pTEB->ThreadLocalStoragePointer;

            m_pCodeThatReturnsSavedTlsPtr = VirtualAllocWithin32BitOffset(m_pImageBase, 0x10000, PAGE_EXECUTE_READWRITE);

            // 48 B8 34 12 34 12 34 12 34 12 mov rax,1234123412341234h 
            PBYTE p = (PBYTE)m_pCodeThatReturnsSavedTlsPtr;
            p[0] = 0x48;
            p[1] = 0xb8;
            MemoryCopy(&p[2], &pCurrentThreadLocalStoragePointer, sizeof(pCurrentThreadLocalStoragePointer));
            // ret
            p[10] = 0xc3;

            // Let's find all mov eax, gs:[58h] and replace it with call to m_pCodeThatReturnsSavedTlsPtr
            const BYTE sample[] = { 0x65, 0x48, 0x8B, 0x04, 0x25, 0x58, 0x00, 0x00, 0x00 };
            p = (PBYTE)pImageBase;

            while (p < (PBYTE)pImageBase + m_dwImageSize)
            {
                int i = 0;
                for (; i < DEF_ARRAY_SIZE(sample) && p < (PBYTE)pImageBase + m_dwImageSize; )
                {
                    if (*p != sample[i])
                    {
                        if (0 == i)
                            p++;
                        else
                            i = 0;
                    }
                    else
                    {
                        i++;
                        p++;
                    }
                }
                
                if (i == DEF_ARRAY_SIZE(sample))
                    // Found
                {
                    PBYTE instruction = (PBYTE)p - DEF_ARRAY_SIZE(sample);
                    DWORD diff = (PBYTE)m_pCodeThatReturnsSavedTlsPtr - ((PBYTE)instruction + 5 /* size of call instruction */ );

                    instruction[0] = 0xe8;
                    MemoryCopy(&instruction[1], &diff, sizeof(diff));

                    instruction[5] = 0x90;
                    instruction[6] = 0x90;
                    instruction[7] = 0x90;
                    instruction[8] = 0x90;
                }
            }
        }
    }
    else
    {
        IMAGE_DATA_DIRECTORY FlashTlsDataDirectory = pImageNTHeaders->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_TLS];
        if (FlashTlsDataDirectory.Size > 0)
        {
            PIMAGE_TLS_DIRECTORY pFlashTlsDirectory = (PIMAGE_TLS_DIRECTORY)((PBYTE)pImageBase + FlashTlsDataDirectory.VirtualAddress);
            DWORD nFlashTlsDataSize = pFlashTlsDirectory->EndAddressOfRawData - pFlashTlsDirectory->StartAddressOfRawData + pFlashTlsDirectory->SizeOfZeroFill;

            PTEB pTEB = (PTEB)NtCurrentTeb();

            // Let's allocate it nearby the ThreadLocalStoragePointer to make sure that offset below is 32 bit
            m_pFlashTlsPointerData = VirtualAllocWithin32BitOffset(pTEB->ThreadLocalStoragePointer, sizeof(PVOID), PAGE_READWRITE);
            m_pFlashTlsData = HeapAlloc(GetProcessHeap(), 0, nFlashTlsDataSize);
            MemoryCopy(m_pFlashTlsPointerData, &m_pFlashTlsData, sizeof(m_pFlashTlsData));
            MemoryCopy(m_pFlashTlsData, (PVOID)(DWORD_PTR)pFlashTlsDirectory->StartAddressOfRawData, nFlashTlsDataSize);

            DWORD nOffset = (PBYTE)m_pFlashTlsPointerData - (PBYTE)pTEB->ThreadLocalStoragePointer;

            // AddressOfIndex will have the value that being used as:
            // pTEB->ThreadLocalStoragePointer[nOffset] will point exactly to pTlsPointerData, that in its turn contains TLS data
            // from the flash ocx dll
            *(DWORD*)pFlashTlsDirectory->AddressOfIndex = nOffset / sizeof(PVOID);

            // But the problem is that pTEB->ThreadLocalStoragePointer then might be changed (see ticket #8)
            // So let's patch the flash code that gets pTEB->ThreadLocalStoragePointer

            PVOID pCurrentThreadLocalStoragePointer = pTEB->ThreadLocalStoragePointer;

            m_pCodeThatReturnsSavedTlsPtr = VirtualAllocWithin32BitOffset(m_pImageBase, 0x10000, PAGE_EXECUTE_READWRITE);

            // B8 44 33 22 11 mov eax,11223344h 
            PBYTE p = (PBYTE)m_pCodeThatReturnsSavedTlsPtr;
            p[0] = 0xb8;
            MemoryCopy(&p[1], &pCurrentThreadLocalStoragePointer, sizeof(pCurrentThreadLocalStoragePointer));
            // ret
            p[5] = 0xc3;

            // Let's find all mov eax,fs:[0000002Ch] (64A12C000000) and replace it with call to m_pCodeThatReturnsSavedTlsPtr
            const BYTE sample[] = { 0x64, 0xa1, 0x2c, 0x00, 0x00, 0x00 };
            p = (PBYTE)pImageBase;

            while (p < (PBYTE)pImageBase + m_dwImageSize)
            {
                int i = 0;
                for (; i < DEF_ARRAY_SIZE(sample) && p < (PBYTE)pImageBase + m_dwImageSize; )
                {
                    if (*p != sample[i])
                    {
                        if (0 == i)
                            p++;
                        else
                            i = 0;
                    }
                    else
                    {
                        i++;
                        p++;
                    }
                }
                
                if (i == DEF_ARRAY_SIZE(sample))
                    // Found
                {
                    PBYTE instruction = (PBYTE)p - DEF_ARRAY_SIZE(sample);
                    DWORD diff = (PBYTE)m_pCodeThatReturnsSavedTlsPtr - ((PBYTE)instruction + 5 /* size of call instruction */ );

                    instruction[0] = 0xe8; // call
                    MemoryCopy(&instruction[1], &diff, sizeof(diff));

                    instruction[5] = 0x90;
                }
            }
        }
    }

    // Entry point
    m_pDllMain = (PDLLMAIN)((PCHAR)pImageBase + pImageNTHeaders->OptionalHeader.AddressOfEntryPoint);

    if (NULL != m_pDllMain)
    {
        m_pDllMain((HMODULE)pImageBase, DLL_PROCESS_ATTACH, NULL);
        m_pDllMain((HMODULE)pImageBase, DLL_THREAD_ATTACH, NULL);
    }

    m_pImageBase = pImageBase;

#ifdef _DEBUG
	{
		char szBuffer[1024];
		wsprintfA(szBuffer,
				  "CMemDLL, image base: 0x%p, size: %d\n",
				  m_pImageBase,
				  pImageNTHeaders->OptionalHeader.SizeOfImage);
		OutputDebugStringA(szBuffer);
	}
#endif // _DEBUG

    return S_OK;
}

HRESULT CMemDLL::Unload()
{
    if (NULL != m_pImageBase)
    {
        if (NULL != m_pDllMain)
        {
			m_pDllMain((HMODULE)m_pImageBase, DLL_THREAD_DETACH, NULL);
			m_pDllMain((HMODULE)m_pImageBase, DLL_PROCESS_DETACH, NULL);
        }

        VirtualFree(m_pImageBase, 0, MEM_RELEASE);
        m_pImageBase = NULL;
    }

	m_list_ImportTableEntries.RemoveAll();

	// Unload all libraries
	for (f_in_box::std::POSITION pos = m_LoadedModules.GetFirstPosition(); 
		 NULL != pos; 
		 FreeLibrary(m_LoadedModules.GetNext(pos)));

    return S_OK;
}

LPVOID* CMemDLL::FindImportTableEntry(LPCSTR lpszLibName, LPCSTR lpszFuncName)
{
	LPVOID* pResult = NULL;

	f_in_box::std::POSITION pos = m_list_ImportTableEntries.GetFirstPosition();

	while (NULL != pos)
	{
		SImportTableEntry ImportTableEntry = m_list_ImportTableEntries.GetNext(pos);

		if (0 == lstrcmpiA(ImportTableEntry.m_strDLLName.c_str(), lpszLibName) && 
			0 == lstrcmpiA(ImportTableEntry.m_strFuncName.c_str(), lpszFuncName))
		{
			pResult = ImportTableEntry.m_ppFunc;
			break;
		}
	}

    return pResult;
}

BOOL CMemDLL::GetResourceData(LPCSTR lpResType, 
                              LPCSTR lpResName, 
                              LPVOID& lpResData, 
                              DWORD& dwResSize)
{
    if (!GetResourceDataLang(lpResType, lpResName, lpResData, dwResSize, GetThreadLocale()))
        if (!GetResourceDataLang(lpResType, lpResName, lpResData, dwResSize, GetSystemDefaultLCID()))
            if (!GetResourceDataLang(lpResType, lpResName, lpResData, dwResSize, GetUserDefaultLCID()))
                if (!GetResourceDataLang(lpResType, lpResName, lpResData, dwResSize, 0))
                    if (!GetResourceDataLang(lpResType, lpResName, lpResData, dwResSize, MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US)))
                        return FALSE;

    return TRUE;
}

BOOL CMemDLL::GetResourceDataLang(LPCSTR lpResType, 
                                  LPCSTR lpResName, 
                                  LPVOID& lpResData, 
                                  DWORD& dwResSize, 
                                  LCID LangId)
{
    // ���������
    BOOL bRes = FALSE;

    // ������� ���������
    char* pPtr = (char*)m_pImageBase;

    // ������������� pPtr �� _IMAGE_NT_HEADERS
    pPtr += ((PIMAGE_DOS_HEADER)pPtr)->e_lfanew;

    // �������� ��������� �� _IMAGE_NT_HEADERS
    PIMAGE_NT_HEADERS pImageNTHeaders = (PIMAGE_NT_HEADERS)pPtr;

// 
    PIMAGE_RESOURCE_DIRECTORY pImageResourceDirectoryRoot = 
        (PIMAGE_RESOURCE_DIRECTORY)
            ((char*)m_pImageBase + 
                pImageNTHeaders->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_RESOURCE].VirtualAddress);

    PIMAGE_RESOURCE_DIRECTORY_ENTRY pImageResourceDirectoryEntries =
        (PIMAGE_RESOURCE_DIRECTORY_ENTRY)
            ((LPBYTE)pImageResourceDirectoryRoot + 
                sizeof(IMAGE_RESOURCE_DIRECTORY));

    PIMAGE_RESOURCE_DIRECTORY_ENTRY pImageResourceDirectoryEntryForResType = NULL;

    // ���� �� ��������� �������� ������, ����� ����� �������, ��������������� ���������� ���� ��������
    for (int intEntryIndex = 0; 
            intEntryIndex < pImageResourceDirectoryRoot->NumberOfIdEntries + 
                            pImageResourceDirectoryRoot->NumberOfNamedEntries; 
            intEntryIndex++)
    {
        PIMAGE_RESOURCE_DIRECTORY_ENTRY pImageResourceDirectoryEntry = 
            pImageResourceDirectoryEntries + intEntryIndex;

        if (DEF_IS_INTRESOURCE(lpResType))
        {
            if (!pImageResourceDirectoryEntry->NameIsString)
            if (pImageResourceDirectoryEntry->Id == (WORD)lpResType)
            {
                pImageResourceDirectoryEntryForResType = pImageResourceDirectoryEntry;
                break;
            }
        }
        else
        {
            if (pImageResourceDirectoryEntry->NameIsString)
            {
                PIMAGE_RESOURCE_DIRECTORY_STRING pImageResourceDirectoryString = 
                    (PIMAGE_RESOURCE_DIRECTORY_STRING)
                        ((PBYTE)pImageResourceDirectoryRoot + 
                        pImageResourceDirectoryEntry->NameOffset);
                LPBYTE lpResTypeName = (LPBYTE)(&pImageResourceDirectoryString->NameString);
                WORD wResTypeNameCharCount = pImageResourceDirectoryString->Length;

                // LPWSTR lpwszResTypeName = (LPWSTR)_alloca(sizeof(WCHAR) * (wResTypeNameCharCount + 1));
                f_in_box::std::CArray<WCHAR> lpwszResTypeName(wResTypeNameCharCount + 1);

                f_in_box::MemoryZero(lpwszResTypeName, sizeof(WCHAR) * (wResTypeNameCharCount + 1));
                f_in_box::MemoryCopy(lpwszResTypeName, lpResTypeName, sizeof(WCHAR) * wResTypeNameCharCount);

                // LPSTR lpszResTypeName = (LPSTR)_alloca(wResTypeNameCharCount + 1);
                f_in_box::std::CArray<CHAR> lpszResTypeName(wResTypeNameCharCount + 1);

                f_in_box::MemoryZero(lpszResTypeName, wResTypeNameCharCount + 1);
                wsprintfA(lpszResTypeName, "%ls", lpwszResTypeName);

                if (!lstrcmpA(lpszResTypeName, lpResType))
                {
                    pImageResourceDirectoryEntryForResType = pImageResourceDirectoryEntry;
                    break;
                }
            }
        }
    }

    if (NULL != pImageResourceDirectoryEntryForResType)
    {
        PIMAGE_RESOURCE_DIRECTORY pImageResourceDirectoryOfResData = 
            (PIMAGE_RESOURCE_DIRECTORY)
                ((LPBYTE)pImageResourceDirectoryRoot + 
                    pImageResourceDirectoryEntryForResType->OffsetToDirectory);

        PIMAGE_RESOURCE_DIRECTORY_ENTRY pImageResourceDirectoryEntries =
            (PIMAGE_RESOURCE_DIRECTORY_ENTRY)
                ((LPBYTE)pImageResourceDirectoryOfResData + 
                sizeof(IMAGE_RESOURCE_DIRECTORY));

        // ���� ������...
        for (int intEntryIndex = 0; 
                intEntryIndex < pImageResourceDirectoryOfResData->NumberOfIdEntries + 
                                pImageResourceDirectoryOfResData->NumberOfNamedEntries; 
                intEntryIndex++)
        {
            PIMAGE_RESOURCE_DIRECTORY_ENTRY pImageResourceDirectoryEntry = 
                pImageResourceDirectoryEntries + intEntryIndex;

            bool bFound = false;

            if (DEF_IS_INTRESOURCE(lpResName))
            {
                if (!pImageResourceDirectoryEntry->NameIsString)
                if (pImageResourceDirectoryEntry->Id == (WORD)lpResName)
                {
                    bFound = true;
                }
            }
            else
            {
                if (pImageResourceDirectoryEntry->NameIsString)
                {
                    PIMAGE_RESOURCE_DIRECTORY_STRING pImageResourceDirectoryString = 
                        (PIMAGE_RESOURCE_DIRECTORY_STRING)
                            ((PBYTE)pImageResourceDirectoryRoot + 
                            pImageResourceDirectoryEntry->NameOffset);
                    LPBYTE lpResNameName = (LPBYTE)(&pImageResourceDirectoryString->NameString);
                    WORD wResNameNameCharCount = pImageResourceDirectoryString->Length;

                    // LPWSTR lpwszResNameName = (LPWSTR)_alloca(sizeof(WCHAR) * (wResNameNameCharCount + 1));
                    f_in_box::std::CArray<WCHAR> lpwszResNameName(wResNameNameCharCount+ 1);

                    f_in_box::MemoryZero(lpwszResNameName, sizeof(WCHAR) * (wResNameNameCharCount + 1));
                    f_in_box::MemoryCopy(lpwszResNameName, lpResNameName, sizeof(WCHAR) * wResNameNameCharCount);

                    // LPSTR lpszResNameName = (LPSTR)_alloca(wResNameNameCharCount + 1);
                    f_in_box::std::CArray<CHAR> lpszResNameName(wResNameNameCharCount + 1);

                    f_in_box::MemoryZero(lpszResNameName, wResNameNameCharCount + 1);
                    wsprintfA(lpszResNameName, "%ls", lpwszResNameName);

                    if (!lstrcmpA(lpszResNameName, lpResName))
                        bFound = true;
                }
            }

            if (bFound)
            {
                PIMAGE_RESOURCE_DIRECTORY pImageResourceDirectory1 = 
                    (PIMAGE_RESOURCE_DIRECTORY)
                        ((LPBYTE)pImageResourceDirectoryRoot + 
                        pImageResourceDirectoryEntry->OffsetToDirectory);

                PIMAGE_RESOURCE_DIRECTORY_ENTRY pEntry = 
                    (PIMAGE_RESOURCE_DIRECTORY_ENTRY)
                        ((LPBYTE)pImageResourceDirectory1 + 
                        sizeof(IMAGE_RESOURCE_DIRECTORY));

                for (int i = 0; 
                        i < pImageResourceDirectory1->NumberOfIdEntries + 
                            pImageResourceDirectory1->NumberOfNamedEntries; 
                        i++)
                    //if (MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US) == pEntry[i].Id || 
                    //    0 == pEntry[i].Id)
                    if ((WORD)LangId == pEntry[i].Id)
                    {
                        PIMAGE_RESOURCE_DATA_ENTRY pResourceDataEntry = 
                            (PIMAGE_RESOURCE_DATA_ENTRY)
                                ((LPBYTE)pImageResourceDirectoryRoot + 
                                    pEntry[i].OffsetToData);

                        lpResData = 
                            (LPBYTE)m_pImageBase + pResourceDataEntry->OffsetToData;

                        dwResSize = pResourceDataEntry->Size;

                        bRes = TRUE;

                        break;
                    }

                break;
            }
        }
    }
    
    return bRes;
}

//
FARPROC CMemDLL::GetProcAddress(LPCSTR lpProcName)
{
    // ���������
    FARPROC lpAddressOfProc = NULL;

    if (NULL != m_pImageBase)
    {
        // ������� ���������
        char* pPtr = (char*)m_pImageBase;

        // ������������� pPtr �� _IMAGE_NT_HEADERS
        pPtr += ((PIMAGE_DOS_HEADER)pPtr)->e_lfanew;

        // �������� ��������� �� _IMAGE_NT_HEADERS
        PIMAGE_NT_HEADERS pImageNTHeaders = (PIMAGE_NT_HEADERS)pPtr;

        // ������� ��������
        PIMAGE_EXPORT_DIRECTORY pExports = 
            (PIMAGE_EXPORT_DIRECTORY)
                ((char*)m_pImageBase + 
                pImageNTHeaders->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT].VirtualAddress);

        for (DWORD dwExportedSymbolIndex = 0; dwExportedSymbolIndex < pExports->NumberOfNames; dwExportedSymbolIndex++)
            // ���� �� ���� ������������� �� ����� ��������
        {
            DWORD dwVirtualAddressOfName = ((PDWORD)((PCHAR)m_pImageBase + pExports->AddressOfNames))[dwExportedSymbolIndex];
            LPSTR lpszName = (LPSTR)((PCHAR)m_pImageBase + dwVirtualAddressOfName);

            if (!lstrcmpA(lpszName, lpProcName))
                // ����� �������!
            {
                // ������
                WORD wIndex = 
                    ((PWORD)((PCHAR)m_pImageBase + pExports->AddressOfNameOrdinals))[dwExportedSymbolIndex];

                DWORD dwVirtualAddressOfAddressOfProc = 
                    ((PDWORD)((PCHAR)m_pImageBase + pExports->AddressOfFunctions))[wIndex];

                lpAddressOfProc = 
                    (FARPROC)((PCHAR)m_pImageBase + dwVirtualAddressOfAddressOfProc);
            }
        }
    }

    return lpAddressOfProc;
}

BOOL CMemDLL::OnProcessImport(LPCSTR lpszLibName, LPCSTR lpszFuncName, LPVOID& pNewAddress, LPVOID& lpParam)
{
    pNewAddress = NULL;
    lpParam = this;

    if (0 != HIWORD(lpszFuncName))
        // This is a name, not an ordinal
    {
        if (0 == lstrcmpiA(lpszFuncName, "GetProcAddress"))
            pNewAddress = &StaticHook_GetProcAddress;
    }

    return NULL != pNewAddress;
}

FARPROC
WINAPI
CMemDLL::StaticHook_GetProcAddress(
	HMODULE hModule,
	LPCSTR lpProcName
)
{
    LPVOID lpOriginalFunc = TlsGetValue(Globals::g_nTlsSlotParam1);
    ASSERT(NULL != lpOriginalFunc);

    CMemDLL* pThis = (CMemDLL*)TlsGetValue(Globals::g_nTlsSlotParam2);
    ASSERT(NULL != pThis);

    return pThis->Hook_GetProcAddress(
			lpOriginalFunc, 

			hModule, 
			lpProcName
		);
}

FARPROC
WINAPI
CMemDLL::Hook_GetProcAddress(
    LPVOID lpOriginalFunc, 

	HMODULE hModule,
	LPCSTR lpProcName
)
{
	typedef FARPROC (WINAPI *P_GetProcAddress)(
			HMODULE hModule,
			LPCSTR lpProcName
		);
	P_GetProcAddress pGetProcAddress = (P_GetProcAddress)lpOriginalFunc;

	LPVOID lpOriginalAddress = (LPVOID)pGetProcAddress(hModule, lpProcName);

	LPVOID lpNewAddress;
	LPVOID lpParam;

	if (OnProcessImport(NULL, lpProcName, lpNewAddress, lpParam))
	{
        // We are ready to intercept

		// Let's check -- may be we already have a thunk

        return (FARPROC)CreateOrFindThunk(lpOriginalAddress, lpNewAddress, lpParam);
	}
    else
		return (FARPROC)lpOriginalAddress;
}

LPVOID CMemDLL::CreateOrFindThunk(LPVOID lpOriginalAddress, LPVOID lpNewAddress, LPVOID lpParam)
{
	SAdapterInfo AdapterInfo;
	if (m_Adapters.Find(lpOriginalAddress, AdapterInfo))
		return AdapterInfo.m_pThunk;

    // Create thunk
    LPVOID lpAdapterCode = AllocThunkWithTwoParams(lpNewAddress, lpOriginalAddress, lpParam);

    m_Adapters.Add(lpOriginalAddress, SAdapterInfo(lpOriginalAddress, lpNewAddress, lpAdapterCode));

	return lpAdapterCode;
}

PVOID CMemDLL::VirtualAllocWithin32BitOffset(PVOID p, DWORD dwSize, DWORD dwProtectionFlags)
{
#ifdef _WIN64
	SYSTEM_INFO SystemInfo;
	GetSystemInfo(&SystemInfo);

	PBYTE pAddressToTryAlloc = (PBYTE)p;

	while (true)
	{
		PVOID pAllocation = VirtualAlloc(pAddressToTryAlloc, dwSize, MEM_RESERVE, dwProtectionFlags);

		if (NULL == pAllocation)
		{
			pAddressToTryAlloc += SystemInfo.dwPageSize;
			continue;
		}
		else
		{
			pAllocation = VirtualAlloc(pAddressToTryAlloc, dwSize, MEM_COMMIT, dwProtectionFlags);

			if (NULL == pAllocation)
				continue;

			return pAllocation;
		}
	}
#else
    return VirtualAlloc(NULL, dwSize, MEM_COMMIT, dwProtectionFlags);
#endif // _WIN64
}
