#ifndef __CONTENT_MANAGER_H__69449111_2994_4caa_AC90_B2E9358C19FD__
#define __CONTENT_MANAGER_H__69449111_2994_4caa_AC90_B2E9358C19FD__

#include "CriticalSection.h"
#include "f_in_box.h"
#include "ContentProvider.h"
#include "ComPtr.h"
#include "String.h"
#include "List.h"
#include "Map.h"
#include "UrlMonHelper.h"

namespace f_in_box
{

class CFlashOCXLoader;

class CContentManager
{
private:
	//
	static BOOL IsURLsEqual(LPCWSTR lpszURL1, LPCWSTR lpszURL2);
    static BOOL ParseURL(LPCWSTR lpszURL, int& nId, f_in_box::std::CString<WCHAR>& strRelativePart);

	// Variables
	// Private Variables
private:

    f_in_box::UrlMonHelper::SUrlMonHelper m_UrlMonHelper;

//	PLOADEXTERNALRESOURCEHANDLER m_pLoadExternalResourceHandler;

	// Next unique id
	ULONG m_nNextId;

    com_helpers::CComPtr<CFlashOCXLoader> m_pFlashOCXLoader;

	//
	CCriticalSection m_cs;

	struct SURLContentProviderPair
	{
		f_in_box::std::CString<WCHAR> m_strURL;
		f_in_box::com_helpers::CComPtr<IContentProvider> m_pContentProvider;

		SURLContentProviderPair(LPCWSTR lpszURL = NULL, IContentProvider* pContentProvider = NULL) : 
			m_strURL(lpszURL), 
			m_pContentProvider(pContentProvider)
		{
		}
	};

	class CURLContentProviderPairs
	{
	private:
        f_in_box::std::CList<SURLContentProviderPair> m_ListOfURLContentProviderPair;

		CCriticalSection m_cs;

	public:
		CURLContentProviderPairs()
		{
		}

		void Add(LPCWSTR lpszURL, IContentProvider* pContentProvider)
		{
			CCriticalSectionOwner lock(m_cs);

			m_ListOfURLContentProviderPair.Add(SURLContentProviderPair(lpszURL, pContentProvider));
		}

		f_in_box::com_helpers::CComPtr<IContentProvider> FindContentProvider(LPCWSTR lpszURL, BOOL bRemove)
		{
			CCriticalSectionOwner lock(m_cs);

			f_in_box::com_helpers::CComPtr<IContentProvider> pContentProvider;

            f_in_box::std::POSITION pos = m_ListOfURLContentProviderPair.GetFirstPosition();

            while (NULL != pos)
            {
                SURLContentProviderPair& item = m_ListOfURLContentProviderPair.GetAt(pos);

    			if (CContentManager::IsURLsEqual(item.m_strURL.c_str(), lpszURL))
				{
					pContentProvider = item.m_pContentProvider;

					if (bRemove)
						m_ListOfURLContentProviderPair.RemoveAt(pos);

					break;
				}

                m_ListOfURLContentProviderPair.GetNext(pos);
            }

			return pContentProvider;
		}

	} m_URLContentProviderPairs;

    friend class CURLContentProviderPairs;

	struct SFlashPlayerControlIdPair
	{
		int m_nId;
		HWND m_hwndFlashPlayerControl;

		SFlashPlayerControlIdPair(int nId = 0, HWND hwndFlashPlayerControl = NULL) : 
			m_nId(nId), 
			m_hwndFlashPlayerControl(hwndFlashPlayerControl)
		{
		}
	};

	class CFlashPlayerControlIdPairs
	{
	private:
        f_in_box::std::CList<SFlashPlayerControlIdPair> m_ListOfFlashPlayerControlIdPairList;

		CCriticalSection m_cs;

	public:
		CFlashPlayerControlIdPairs()
		{
		}

		void Add(int nId, HWND hwndFlashPlayerControl)
		{
			CCriticalSectionOwner lock(m_cs);

			Remove(hwndFlashPlayerControl);
			m_ListOfFlashPlayerControlIdPairList.Add(SFlashPlayerControlIdPair(nId, hwndFlashPlayerControl));
		}

		HWND Find(int nId)
		{
			CCriticalSectionOwner lock(m_cs);

            f_in_box::std::POSITION pos = m_ListOfFlashPlayerControlIdPairList.GetFirstPosition();

            while (NULL != pos)
            {
                SFlashPlayerControlIdPair& item = m_ListOfFlashPlayerControlIdPairList.GetAt(pos);

                if (item.m_nId == nId)
                    return item.m_hwndFlashPlayerControl;

                m_ListOfFlashPlayerControlIdPairList.GetNext(pos);
            }

			return NULL;
		}

		void Remove(HWND hwndFlashPlayerControl)
		{
			CCriticalSectionOwner lock(m_cs);

            f_in_box::std::POSITION pos = m_ListOfFlashPlayerControlIdPairList.GetFirstPosition();

            while (NULL != pos)
            {
                SFlashPlayerControlIdPair& item = m_ListOfFlashPlayerControlIdPairList.GetAt(pos);

                if (item.m_hwndFlashPlayerControl == hwndFlashPlayerControl)
                {
                    f_in_box::std::POSITION posToRemove = pos;

                    m_ListOfFlashPlayerControlIdPairList.GetNext(pos);

                    m_ListOfFlashPlayerControlIdPairList.RemoveAt(posToRemove);
                }
                else
                    m_ListOfFlashPlayerControlIdPairList.GetNext(pos);
            }
		}

	} m_FlashPlayerControlIdPairs;

    friend class CFlashPlayerControlIdPairs;
 
	struct SFakeHandleAndIStreamPair
	{
		HANDLE m_Handle;
		f_in_box::com_helpers::CComPtr<IStream> m_pStream;

		SFakeHandleAndIStreamPair(HANDLE Handle = INVALID_HANDLE_VALUE, IStream* pStream = NULL) : 
			m_Handle(Handle), 
			m_pStream(pStream)
		{
		}
	};

	class CFakeHandleAndIStreamPairs
	{
	private:
        f_in_box::std::CList<SFakeHandleAndIStreamPair> m_ListOfFakeHandleAndIStreamPairList;

		CCriticalSection m_cs;

	public:
		CFakeHandleAndIStreamPairs()
		{
		}

		void Add(HANDLE Handle, IStream* pStream)
		{
			CCriticalSectionOwner lock(m_cs);

			m_ListOfFakeHandleAndIStreamPairList.Add(SFakeHandleAndIStreamPair(Handle, pStream));
		}

		f_in_box::com_helpers::CComPtr<IStream> Find(HANDLE Handle)
		{
			CCriticalSectionOwner lock(m_cs);

            f_in_box::std::POSITION pos = m_ListOfFakeHandleAndIStreamPairList.GetFirstPosition();

            while (NULL != pos)
            {
                SFakeHandleAndIStreamPair& item = m_ListOfFakeHandleAndIStreamPairList.GetAt(pos);

                if (item.m_Handle == Handle)
                    return item.m_pStream;

                m_ListOfFakeHandleAndIStreamPairList.GetNext(pos);
            }

			return NULL;
		}

		BOOL Remove(HANDLE Handle)
		{
			BOOL bRes = FALSE;

			CCriticalSectionOwner lock(m_cs);

            f_in_box::std::POSITION pos = m_ListOfFakeHandleAndIStreamPairList.GetFirstPosition();

            while (NULL != pos)
            {
                SFakeHandleAndIStreamPair& item = m_ListOfFakeHandleAndIStreamPairList.GetAt(pos);

                if (item.m_Handle == Handle)
                {
                    f_in_box::std::POSITION posToRemove = pos;

                    m_ListOfFakeHandleAndIStreamPairList.GetNext(pos);

                    m_ListOfFakeHandleAndIStreamPairList.RemoveAt(posToRemove);

					bRes = TRUE;

					CloseHandle(Handle);

					break;
                }
                else
                    m_ListOfFakeHandleAndIStreamPairList.GetNext(pos);
            }

			return bRes;
		}

	} m_FakeHandleAndIStreamPairs;

    friend class CFakeHandleAndIStreamPairs;
 
    struct SOnLoadExternalResourceHandlerInfo
    {
        PLOAD_EXTERNAL_RESOURCE_HANDLERA m_pHandlerA;
        PLOAD_EXTERNAL_RESOURCE_HANDLERW m_pHandlerW;
        LPARAM m_lParam;

        SOnLoadExternalResourceHandlerInfo(PLOAD_EXTERNAL_RESOURCE_HANDLERA pHandler = NULL, LPARAM lParam = 0) : 
            m_pHandlerA(pHandler), 
            m_pHandlerW(NULL), 
            m_lParam(lParam)
        {
        }

        SOnLoadExternalResourceHandlerInfo(PLOAD_EXTERNAL_RESOURCE_HANDLERW pHandler, LPARAM lParam) : 
            m_pHandlerW(pHandler), 
            m_pHandlerA(NULL), 
            m_lParam(lParam)
        {
        }

        HRESULT Call(LPCWSTR lpszURL, IStream** ppStream, HFPC hFPC)
        {
            if (m_pHandlerW)
                return m_pHandlerW(lpszURL, ppStream, hFPC, m_lParam);
            else if (m_pHandlerA)
                return m_pHandlerA(f_in_box::std::WToCA(lpszURL).c_str(), ppStream, hFPC, m_lParam);
            else
                return E_FAIL;
        }
    };
    DWORD m_dwHandlerCookie__OnLoadExternalResource;
    f_in_box::std::CMap<DWORD, SOnLoadExternalResourceHandlerInfo> m_OnLoadExternalResourceHandlerInfo;


    struct SGetBindInfoHandlerInfo
    {
		PGET_BIND_INFO_HANDLER m_pHandler;
        LPARAM m_lParam;

        SGetBindInfoHandlerInfo(PGET_BIND_INFO_HANDLER pHandler = NULL, LPARAM lParam = 0) : 
            m_pHandler(pHandler), 
            m_lParam(lParam)
        {
        }

        HRESULT Call(HFPC hFPC, DWORD* grfBINDF, BINDINFO* pbindinfo)
        {
            if (m_pHandler)
				return m_pHandler(hFPC, grfBINDF, pbindinfo, m_lParam);
            else
                return E_FAIL;
        }
    };
    DWORD m_dwHandlerCookie__GetBindInfo;
    f_in_box::std::CMap<DWORD, SGetBindInfoHandlerInfo> m_GetBindInfoHandlerInfo;


	// Functions
	// Private Functions
private:
    f_in_box::std::CString<WCHAR> GenerateNewMainMovieURL();
	void AddHWND(int nId, HWND hwndFlashPlayerControl);
	HWND FindHWND(int nId);
	BOOL CallLoadExternalResource(HWND hwndFlashPlayerControl, LPCWSTR lpszRelativePath, IStream* pStream);

	// Public Functions
public:
	CContentManager();
	~CContentManager();

	// bUseLayer == TRUE - LoadMovie
	// bUseLayer == FALSE - PutMovie
	void LoadMovieFromMemory(HWND hwndFlashPlayerControl, 
							 LPVOID lpData, 
							 DWORD dwSize, 
							 BOOL bUseLayer = FALSE, 
							 int nLayer = -1);

	// bUseLayer == TRUE - LoadMovie
	// bUseLayer == FALSE - PutMovie
	void LoadMovieUsingStream(HWND hwndFlashPlayerControl, 
							  IStream** ppStream, 
							  BOOL bUseLayer = FALSE, 
							  int nLayer = -1);

	//
	f_in_box::com_helpers::CComPtr<IContentProvider> FindContentProviderAndRemove(LPCWSTR lpszURL);

	HRESULT BindToStorage(IBindCtx *pbc, IMoniker *pmkToLeft, REFIID riid, void **ppvObj, IMoniker* pmkContext, IBindStatusCallback* pBindStatusCallback, LPCWSTR lpszURL);

	// 
	void RemoveFlashPlayerControlHWND(HWND hwndFlashPlayerControl);

	//
	HRESULT MyCreateURLMoniker(IMoniker* pmkContext, LPCWSTR szURL, IMoniker** ppmk);
	HANDLE MyCreateFileA(LPCSTR lpFileName, 
					     DWORD dwDesiredAccess, 
					     DWORD dwShareMode, 
						 LPSECURITY_ATTRIBUTES lpSecurityAttributes, 
						 DWORD dwCreationDisposition, 
						 DWORD dwFlagsAndAttributes, 
						 HANDLE hTemplateFile);
	HANDLE MyCreateFileW(LPCWSTR lpFileName, 
					     DWORD dwDesiredAccess, 
					     DWORD dwShareMode, 
						 LPSECURITY_ATTRIBUTES lpSecurityAttributes, 
						 DWORD dwCreationDisposition, 
						 DWORD dwFlagsAndAttributes, 
						 HANDLE hTemplateFile);
	HANDLE MyCreateFakeFile(LPCWSTR lpFileName, 
					        DWORD dwDesiredAccess, 
					        DWORD dwShareMode, 
							LPSECURITY_ATTRIBUTES lpSecurityAttributes, 
							DWORD dwCreationDisposition, 
							DWORD dwFlagsAndAttributes, 
							HANDLE hTemplateFile);

    //
    void SetFlashOCXLoader(CFlashOCXLoader* pFlashOCXLoader)
    {
        m_pFlashOCXLoader = pFlashOCXLoader;
    }

    // Returns cookie
    DWORD AddOnLoadExternalResourceHandlerA(PLOAD_EXTERNAL_RESOURCE_HANDLERA pHandler, LPARAM lParam);
    DWORD AddOnLoadExternalResourceHandlerW(PLOAD_EXTERNAL_RESOURCE_HANDLERW pHandler, LPARAM lParam);
    BOOL RemoveOnLoadExternalResourceHandler(DWORD dwCookie);

	DWORD AddGetBindInfoHandler(PGET_BIND_INFO_HANDLER pHandler, LPARAM lParam);
	BOOL RemoveGetBindInfoHandler(DWORD dwCookie);
	void CallBindInfoHandlers(DWORD* grfBINDF, BINDINFO* pbindinfo);

    f_in_box::com_helpers::CComPtr<IContentProvider> GetContentProvider(LPCWSTR lpszURL);

	com_helpers::CComPtr<CFlashOCXLoader> GetFlashOCXLoader() const
	{
		return m_pFlashOCXLoader;
	}

	f_in_box::com_helpers::CComPtr<IStream> FindStreamByHandle(HANDLE hFakeFileHandle);
	BOOL CloseFakeHandle(HANDLE hFakeFileHandle);
};

}

#endif // !__CONTENT_MANAGER_H__69449111_2994_4caa_AC90_B2E9358C19FD__
