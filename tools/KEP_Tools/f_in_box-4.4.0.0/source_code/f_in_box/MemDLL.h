#ifndef __MEMDLL_H_FC75F6CA861A43c09BA76D1C2B135A06__
#define __MEMDLL_H_FC75F6CA861A43c09BA76D1C2B135A06__

#include "Map.h"
#include "String.h"

namespace f_in_box
{

//
// Class represents DLL loaded into memory
//
class CMemDLL
{
private:
    // Image base
    LPVOID m_pImageBase;

    // Image size
    DWORD m_dwImageSize;

    // List of pointers to adapters
//    f_in_box::std::CList<LPVOID> m_list_Adapters;

	struct SAdapterInfo
	{
		LPVOID m_pOriginalAddress;
		LPVOID m_pNewAddress;
		LPVOID m_pThunk;

		SAdapterInfo(LPVOID pOriginalAddress = NULL, LPVOID pNewAddress = NULL, LPVOID pThunk = NULL) : 
			m_pOriginalAddress(pOriginalAddress), 
			m_pNewAddress(pNewAddress), 
			m_pThunk(pThunk)
		{
		}
	};

	// Original address -> SAdapterInfo
    f_in_box::std::CMap<LPVOID, SAdapterInfo> m_Adapters;

    //
    typedef BOOL (WINAPI *PDLLMAIN)(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved);
    PDLLMAIN m_pDllMain;

	//
	struct SImportTableEntry
	{
		f_in_box::std::CString<char> m_strDLLName;
		f_in_box::std::CString<char> m_strFuncName;
		LPVOID* m_ppFunc;

		SImportTableEntry(LPCSTR szDLLName = NULL, LPCSTR szFuncName = NULL, LPVOID* ppFunc = NULL) : 
			m_strDLLName(szDLLName), 
			m_strFuncName(szFuncName), 
			m_ppFunc(ppFunc)
		{
		}
	};
    f_in_box::std::CList<SImportTableEntry> m_list_ImportTableEntries;

	// List of loaded modules
	f_in_box::std::CList<HMODULE> m_LoadedModules;

    /// Points to tls data pointer (m_pFlashTlsData)
    PVOID m_pFlashTlsPointerData;
    /// Tls data (TODO: now the same for all threads, but it would be correct to have a pointer for each thread)
    PVOID m_pFlashTlsData;

    /// A code that just sets rax to correct tls pointer
    PVOID m_pCodeThatReturnsSavedTlsPtr;

public:
    CMemDLL();
    virtual ~CMemDLL();

    HRESULT Load(LPCVOID pData, DWORD dwSize);
    HRESULT Unload();

    // If you want to hook some functions after loading, you can use this function
    LPVOID* FindImportTableEntry(LPCSTR lpszLibName, LPCSTR lpszFuncName);

    BOOL GetResourceDataLang(LPCSTR lpResType, 
                             LPCSTR lpResName, 
                             LPVOID& lpResData, 
                             DWORD& dwResSize, 
                             LCID wLangId);

    BOOL GetResourceData(LPCSTR lpResType, 
                         LPCSTR lpResName, 
                         LPVOID& lpResData, 
                         DWORD& dwResSize);

    //
    FARPROC GetProcAddress(LPCSTR lpProcName);

    HINSTANCE GetHINSTANCE() const
    {
        return (HINSTANCE)m_pImageBase;
    }

    PVOID GetImageBase() const
    {
        return m_pImageBase;
    }

    DWORD GetImageSize() const
    {
        return m_dwImageSize;
    }

protected:
    //
    // If returns TRUE, pNewAddress contain new address of imported function
    // When this function will be called, two additional arguments will be 
    // passed to it: old address and lParam
    //
    // Example:
    //
    // int WINAPI MessageBoxA(
    //		HWND hWnd,
    //		LPCTSTR lpText,
    //		LPCTSTR lpCaption,
    //		UINT uType
    //	);
    //
    // New function should be:
    // 
    // int WINAPI New_MessageBoxA(
    //		LPVOID lpOldAddress // old address
    //		LPVOID lpParam		// additional data
    //
    //		HWND hWnd,
    //		LPCTSTR lpText,
    //		LPCTSTR lpCaption,
    //		UINT uType
    //	);
    //
    virtual BOOL OnProcessImport(LPCSTR lpszLibName, 
                                 LPCSTR lpszFuncName, 
                                 LPVOID& pNewAddress, 
                                 LPVOID& lpParam);

private:

	LPVOID CreateOrFindThunk(LPVOID lpOriginalAddress, LPVOID lpNewAddress, LPVOID lpParam);

	static
	FARPROC 
	WINAPI 
	StaticHook_GetProcAddress(
		HMODULE hModule,
		LPCSTR lpProcName
	);

	FARPROC 
	WINAPI 
	Hook_GetProcAddress(
        LPVOID lpOriginalFunc, 

		HMODULE hModule,
		LPCSTR lpProcName
	);

    static PVOID VirtualAllocWithin32BitOffset(PVOID p, DWORD dwSize, DWORD dwProtectionFlags);
};

}

#endif // !__MEMDLL_H_FC75F6CA861A43c09BA76D1C2B135A06__
