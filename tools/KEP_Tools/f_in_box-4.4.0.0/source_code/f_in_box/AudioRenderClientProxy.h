#ifndef __AudioRenderClientProxy_H_
#define __AudioRenderClientProxy_H_

namespace f_in_box
{
	HRESULT CreateAudioRenderClientProxy(
		const std::CArray<BYTE>& DeviceFormat, 
		CFlashOCXLoader* pLoader, 
		IAudioRenderClient* pOriginalAudioRenderClient, 
		IAudioRenderClient** ppProxyAudioRenderClient);
}

#endif // !__AudioRenderClientProxy_H_
