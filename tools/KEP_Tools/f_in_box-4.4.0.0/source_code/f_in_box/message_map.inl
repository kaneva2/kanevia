// Methods

// Method: SetZoomRect
// Return type: void
MESSAGE_HANDLER(FPCM_SETZOOMRECT, OnSetZoomRect)

// Method: Zoom
// Return type: void
MESSAGE_HANDLER(FPCM_ZOOM, OnZoom)

// Method: Pan
// Return type: void
MESSAGE_HANDLER(FPCM_PAN, OnPan)

// Method: Play
// Return type: void
MESSAGE_HANDLER(FPCM_PLAY, OnPlay)

// Method: Stop
// Return type: void
MESSAGE_HANDLER(FPCM_STOP, OnStop)

// Method: Back
// Return type: void
MESSAGE_HANDLER(FPCM_BACK, OnBack)

// Method: Forward
// Return type: void
MESSAGE_HANDLER(FPCM_FORWARD, OnForward)

// Method: Rewind
// Return type: void
MESSAGE_HANDLER(FPCM_REWIND, OnRewind)

// Method: StopPlay
// Return type: void
MESSAGE_HANDLER(FPCM_STOPPLAY, OnStopPlay)

// Method: GotoFrame
// Return type: void
MESSAGE_HANDLER(FPCM_GOTOFRAME, OnGotoFrame)

// Method: CurrentFrame
// Return type: long
MESSAGE_HANDLER(FPCM_CURRENTFRAME, OnCurrentFrame)

// Method: IsPlaying
// Return type: VARIANT_BOOL
MESSAGE_HANDLER(FPCM_ISPLAYING, OnIsPlaying)

// Method: PercentLoaded
// Return type: long
MESSAGE_HANDLER(FPCM_PERCENTLOADED, OnPercentLoaded)

// Method: FrameLoaded
// Return type: VARIANT_BOOL
MESSAGE_HANDLER(FPCM_FRAMELOADED, OnFrameLoaded)

// Method: FlashVersion
// Return type: long
MESSAGE_HANDLER(FPCM_FLASHVERSION, OnFlashVersion)

// Method: LoadMovie
// Return type: void
MESSAGE_HANDLER(FPCM_LOADMOVIEA, OnLoadMovieA)
MESSAGE_HANDLER(FPCM_LOADMOVIEW, OnLoadMovieW)

// Method: TGotoFrame
// Return type: void
MESSAGE_HANDLER(FPCM_TGOTOFRAMEA, OnTGotoFrameA)
MESSAGE_HANDLER(FPCM_TGOTOFRAMEW, OnTGotoFrameW)

// Method: TGotoLabel
// Return type: void
MESSAGE_HANDLER(FPCM_TGOTOLABELA, OnTGotoLabelA)
MESSAGE_HANDLER(FPCM_TGOTOLABELW, OnTGotoLabelW)

// Method: TCurrentFrame
// Return type: long
MESSAGE_HANDLER(FPCM_TCURRENTFRAMEA, OnTCurrentFrameA)
MESSAGE_HANDLER(FPCM_TCURRENTFRAMEW, OnTCurrentFrameW)

// Method: TCurrentLabel
// Return type: BSTR
MESSAGE_HANDLER(FPCM_TCURRENTLABELA, OnTCurrentLabelA)
MESSAGE_HANDLER(FPCM_TCURRENTLABELW, OnTCurrentLabelW)

// Method: TPlay
// Return type: void
MESSAGE_HANDLER(FPCM_TPLAYA, OnTPlayA)
MESSAGE_HANDLER(FPCM_TPLAYW, OnTPlayW)

// Method: TStopPlay
// Return type: void
MESSAGE_HANDLER(FPCM_TSTOPPLAYA, OnTStopPlayA)
MESSAGE_HANDLER(FPCM_TSTOPPLAYW, OnTStopPlayW)

// Method: SetVariable
// Return type: void
MESSAGE_HANDLER(FPCM_SETVARIABLEA, OnSetVariableA)
MESSAGE_HANDLER(FPCM_SETVARIABLEW, OnSetVariableW)

// Method: GetVariable
// Return type: BSTR
MESSAGE_HANDLER(FPCM_GETVARIABLEA, OnGetVariableA)
MESSAGE_HANDLER(FPCM_GETVARIABLEW, OnGetVariableW)

// Method: TSetProperty
// Return type: void
MESSAGE_HANDLER(FPCM_TSETPROPERTYA, OnTSetPropertyA)
MESSAGE_HANDLER(FPCM_TSETPROPERTYW, OnTSetPropertyW)

// Method: TGetProperty
// Return type: BSTR
MESSAGE_HANDLER(FPCM_TGETPROPERTYA, OnTGetPropertyA)
MESSAGE_HANDLER(FPCM_TGETPROPERTYW, OnTGetPropertyW)

// Method: TCallFrame
// Return type: void
MESSAGE_HANDLER(FPCM_TCALLFRAMEA, OnTCallFrameA)
MESSAGE_HANDLER(FPCM_TCALLFRAMEW, OnTCallFrameW)

// Method: TCallLabel
// Return type: void
MESSAGE_HANDLER(FPCM_TCALLLABELA, OnTCallLabelA)
MESSAGE_HANDLER(FPCM_TCALLLABELW, OnTCallLabelW)

// Method: TSetPropertyNum
// Return type: void
MESSAGE_HANDLER(FPCM_TSETPROPERTYNUMA, OnTSetPropertyNumA)
MESSAGE_HANDLER(FPCM_TSETPROPERTYNUMW, OnTSetPropertyNumW)

// Method: TGetPropertyNum
// Return type: double
MESSAGE_HANDLER(FPCM_TGETPROPERTYNUMA, OnTGetPropertyNumA)
MESSAGE_HANDLER(FPCM_TGETPROPERTYNUMW, OnTGetPropertyNumW)

// Method: TGetPropertyAsNumber
// Return type: double
MESSAGE_HANDLER(FPCM_TGETPROPERTYASNUMBERA, OnTGetPropertyAsNumberA)
MESSAGE_HANDLER(FPCM_TGETPROPERTYASNUMBERW, OnTGetPropertyAsNumberW)

// Properties

// Property: ReadyState
// Type: long
MESSAGE_HANDLER(FPCM_GET_READYSTATE, OnGetReadyState)

// Property: TotalFrames
// Type: long
MESSAGE_HANDLER(FPCM_GET_TOTALFRAMES, OnGetTotalFrames)

// Property: Playing
// Type: VARIANT_BOOL
MESSAGE_HANDLER(FPCM_GET_PLAYING, OnGetPlaying)
MESSAGE_HANDLER(FPCM_PUT_PLAYING, OnPutPlaying)

// Property: Quality
// Type: int
MESSAGE_HANDLER(FPCM_GET_QUALITY, OnGetQuality)
MESSAGE_HANDLER(FPCM_PUT_QUALITY, OnPutQuality)

// Property: ScaleMode
// Type: int
MESSAGE_HANDLER(FPCM_GET_SCALEMODE, OnGetScaleMode)
MESSAGE_HANDLER(FPCM_PUT_SCALEMODE, OnPutScaleMode)

// Property: AlignMode
// Type: int
MESSAGE_HANDLER(FPCM_GET_ALIGNMODE, OnGetAlignMode)
MESSAGE_HANDLER(FPCM_PUT_ALIGNMODE, OnPutAlignMode)

// Property: BackgroundColor
// Type: long
MESSAGE_HANDLER(FPCM_GET_BACKGROUNDCOLOR, OnGetBackgroundColor)
MESSAGE_HANDLER(FPCM_PUT_BACKGROUNDCOLOR, OnPutBackgroundColor)

// Property: Loop
// Type: VARIANT_BOOL
MESSAGE_HANDLER(FPCM_GET_LOOP, OnGetLoop)
MESSAGE_HANDLER(FPCM_PUT_LOOP, OnPutLoop)

// Property: Movie
// Type: BSTR
MESSAGE_HANDLER(FPCM_GET_MOVIEA, OnGetMovieA)
MESSAGE_HANDLER(FPCM_GET_MOVIEW, OnGetMovieW)
MESSAGE_HANDLER(FPCM_PUT_MOVIEA, OnPutMovieA)
MESSAGE_HANDLER(FPCM_PUT_MOVIEW, OnPutMovieW)

// Property: FrameNum
// Type: long
MESSAGE_HANDLER(FPCM_GET_FRAMENUM, OnGetFrameNum)
MESSAGE_HANDLER(FPCM_PUT_FRAMENUM, OnPutFrameNum)

// Property: WMode
// Type: BSTR
MESSAGE_HANDLER(FPCM_GET_WMODEA, OnGetWModeA)
MESSAGE_HANDLER(FPCM_GET_WMODEW, OnGetWModeW)
MESSAGE_HANDLER(FPCM_PUT_WMODEA, OnPutWModeA)
MESSAGE_HANDLER(FPCM_PUT_WMODEW, OnPutWModeW)

// Property: SAlign
// Type: BSTR
MESSAGE_HANDLER(FPCM_GET_SALIGNA, OnGetSAlignA)
MESSAGE_HANDLER(FPCM_GET_SALIGNW, OnGetSAlignW)
MESSAGE_HANDLER(FPCM_PUT_SALIGNA, OnPutSAlignA)
MESSAGE_HANDLER(FPCM_PUT_SALIGNW, OnPutSAlignW)

// Property: Menu
// Type: VARIANT_BOOL
MESSAGE_HANDLER(FPCM_GET_MENU, OnGetMenu)
MESSAGE_HANDLER(FPCM_PUT_MENU, OnPutMenu)

// Property: Base
// Type: BSTR
MESSAGE_HANDLER(FPCM_GET_BASEA, OnGetBaseA)
MESSAGE_HANDLER(FPCM_GET_BASEW, OnGetBaseW)
MESSAGE_HANDLER(FPCM_PUT_BASEA, OnPutBaseA)
MESSAGE_HANDLER(FPCM_PUT_BASEW, OnPutBaseW)

// Property: Scale
// Type: BSTR
MESSAGE_HANDLER(FPCM_GET_SCALEA, OnGetScaleA)
MESSAGE_HANDLER(FPCM_GET_SCALEW, OnGetScaleW)
MESSAGE_HANDLER(FPCM_PUT_SCALEA, OnPutScaleA)
MESSAGE_HANDLER(FPCM_PUT_SCALEW, OnPutScaleW)

// Property: DeviceFont
// Type: VARIANT_BOOL
MESSAGE_HANDLER(FPCM_GET_DEVICEFONT, OnGetDeviceFont)
MESSAGE_HANDLER(FPCM_PUT_DEVICEFONT, OnPutDeviceFont)

// Property: EmbedMovie
// Type: VARIANT_BOOL
MESSAGE_HANDLER(FPCM_GET_EMBEDMOVIE, OnGetEmbedMovie)
MESSAGE_HANDLER(FPCM_PUT_EMBEDMOVIE, OnPutEmbedMovie)

// Property: BGColor
// Type: BSTR
MESSAGE_HANDLER(FPCM_GET_BGCOLORA, OnGetBGColorA)
MESSAGE_HANDLER(FPCM_GET_BGCOLORW, OnGetBGColorW)
MESSAGE_HANDLER(FPCM_PUT_BGCOLORA, OnPutBGColorA)
MESSAGE_HANDLER(FPCM_PUT_BGCOLORW, OnPutBGColorW)

// Property: Quality2
// Type: BSTR
MESSAGE_HANDLER(FPCM_GET_QUALITY2A, OnGetQuality2A)
MESSAGE_HANDLER(FPCM_GET_QUALITY2W, OnGetQuality2W)
MESSAGE_HANDLER(FPCM_PUT_QUALITY2A, OnPutQuality2A)
MESSAGE_HANDLER(FPCM_PUT_QUALITY2W, OnPutQuality2W)

// Property: SWRemote
// Type: BSTR
MESSAGE_HANDLER(FPCM_GET_SWREMOTEA, OnGetSWRemoteA)
MESSAGE_HANDLER(FPCM_GET_SWREMOTEW, OnGetSWRemoteW)
MESSAGE_HANDLER(FPCM_PUT_SWREMOTEA, OnPutSWRemoteA)
MESSAGE_HANDLER(FPCM_PUT_SWREMOTEW, OnPutSWRemoteW)

// Property: Stacking
// Type: BSTR
MESSAGE_HANDLER(FPCM_GET_STACKINGA, OnGetStackingA)
MESSAGE_HANDLER(FPCM_GET_STACKINGW, OnGetStackingW)
MESSAGE_HANDLER(FPCM_PUT_STACKINGA, OnPutStackingA)
MESSAGE_HANDLER(FPCM_PUT_STACKINGW, OnPutStackingW)

// Property: FlashVars
// Type: BSTR
MESSAGE_HANDLER(FPCM_GET_FLASHVARSA, OnGetFlashVarsA)
MESSAGE_HANDLER(FPCM_GET_FLASHVARSW, OnGetFlashVarsW)
MESSAGE_HANDLER(FPCM_PUT_FLASHVARSA, OnPutFlashVarsA)
MESSAGE_HANDLER(FPCM_PUT_FLASHVARSW, OnPutFlashVarsW)

// Property: AllowScriptAccess
// Type: BSTR
MESSAGE_HANDLER(FPCM_GET_ALLOWSCRIPTACCESSA, OnGetAllowScriptAccessA)
MESSAGE_HANDLER(FPCM_GET_ALLOWSCRIPTACCESSW, OnGetAllowScriptAccessW)
MESSAGE_HANDLER(FPCM_PUT_ALLOWSCRIPTACCESSA, OnPutAllowScriptAccessA)
MESSAGE_HANDLER(FPCM_PUT_ALLOWSCRIPTACCESSW, OnPutAllowScriptAccessW)

// Property: MovieData
// Type: BSTR
MESSAGE_HANDLER(FPCM_GET_MOVIEDATAA, OnGetMovieDataA)
MESSAGE_HANDLER(FPCM_GET_MOVIEDATAW, OnGetMovieDataW)
MESSAGE_HANDLER(FPCM_PUT_MOVIEDATAA, OnPutMovieDataA)
MESSAGE_HANDLER(FPCM_PUT_MOVIEDATAW, OnPutMovieDataW)

