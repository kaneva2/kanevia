#include "stdafx.h"
#include "f_in_box.h"
#include "MemDLL.h"
#include "ContentManager.h"
#include "FlashOCXLoader.h"
#include "FPCWnd.h"

using namespace f_in_box;
using namespace f_in_box::com_helpers;

HFPC WINAPI FPC_LoadInternal(LPVOID lpFlashOCXCodeData = NULL, DWORD dwSizeOfFlashOCXCode = 0)
{
    CComPtr<CFlashOCXLoader> pFlashOCXLoader = CFlashOCXLoader::Create();

    HRESULT hr = pFlashOCXLoader->Load(lpFlashOCXCodeData, dwSizeOfFlashOCXCode);

    if (S_OK != hr)
    {
        ASSERT(FALSE);
        return NULL;
    }

    return (HFPC)pFlashOCXLoader.Detach();
}

HFPC WINAPI FPC_LoadOCXCodeFromMemory(LPVOID lpFlashOCXCodeData, DWORD dwSizeOfFlashOCXCode)
{
    return FPC_LoadInternal(lpFlashOCXCodeData, dwSizeOfFlashOCXCode);
}

HFPC WINAPI FPC_LoadRegisteredOCX()
{
    return FPC_LoadInternal();
}

BOOL WINAPI FPC_CanUnloadNow(HFPC hFPC)
{
    BOOL bRes;

    if (NULL != hFPC)
    {
		CFlashOCXLoader* pFlashOCXLoader = (CFlashOCXLoader*)hFPC;

        bRes = pFlashOCXLoader->CanUnloadNow();
    }
    else
        bRes = FALSE;

    return bRes;
}

BOOL WINAPI FPC_UnloadCode(HFPC hFPC)
{
    BOOL bRes;

    if (NULL != hFPC)
    {
		CFlashOCXLoader* pFlashOCXLoader = (CFlashOCXLoader*)hFPC;
		pFlashOCXLoader->UnloadCode();

        bRes = TRUE;
    }
    else
        bRes = FALSE;

    return bRes;
}

ATOM WINAPI FPC_GetClassAtomA(HFPC hFPC)
{
    return ((CFlashOCXLoader*)hFPC)->GetClassAtomA();
}

ATOM WINAPI FPC_GetClassAtomW(HFPC hFPC)
{
    return ((CFlashOCXLoader*)hFPC)->GetClassAtomW();
}

BOOL WINAPI FPC_GetVersionEx(HFPC hFPC, SFPCVersion* pVersion)
{
    return ((CFlashOCXLoader*)hFPC)->GetModuleVersionEx(pVersion);
}

void WINAPI FPC_EnableSound(HFPC hFPC, BOOL bEnable)
{
    ((CFlashOCXLoader*)hFPC)->SetAudioEnabled(bEnable);
}

BOOL WINAPI FPC_IsSoundEnabled(HFPC hFPC)
{
    return ((CFlashOCXLoader*)hFPC)->GetAudioEnabled();
}

DWORD WINAPI FPC_AddOnLoadExternalResourceHandlerA(HFPC hFPC, PLOAD_EXTERNAL_RESOURCE_HANDLERA pHandler, LPARAM lParam)
{
    return ((CFlashOCXLoader*)hFPC)->GetContentManager().AddOnLoadExternalResourceHandlerA(pHandler, lParam);
}

DWORD WINAPI FPC_AddOnLoadExternalResourceHandlerW(HFPC hFPC, PLOAD_EXTERNAL_RESOURCE_HANDLERW pHandler, LPARAM lParam)
{
    return ((CFlashOCXLoader*)hFPC)->GetContentManager().AddOnLoadExternalResourceHandlerW(pHandler, lParam);
}

HRESULT WINAPI FPC_RemoveOnLoadExternalResourceHandler(HFPC hFPC, DWORD dwCookie)
{
    return ((CFlashOCXLoader*)hFPC)->GetContentManager().RemoveOnLoadExternalResourceHandler(dwCookie);
}

// 0 <= nVolume <= DEF_MAX_FLASH_AUDIO_VOLUME
HRESULT WINAPI FPC_SetSoundVolume(HFPC hFPC, DWORD dwVolume)
{
    ((CFlashOCXLoader*)hFPC)->SetAudioVolume(dwVolume);

    return S_OK;
}

DWORD WINAPI FPC_GetSoundVolume(HFPC hFPC)
{
    return ((CFlashOCXLoader*)hFPC)->GetAudioVolume();
}

LPCSTR WINAPI FPC_GetClassNameA(HFPC hFPC)
{
    return ((CFlashOCXLoader*)hFPC)->GetClassNameA();
}

LPCWSTR WINAPI FPC_GetClassNameW(HFPC hFPC)
{
    return ((CFlashOCXLoader*)hFPC)->GetClassNameW();
}

HWND WINAPI FPC_GetAxHWND(HWND hWndFPC)
{
	HWND hWnd = NULL;

	SFPCQueryInterface FPCQueryInterface;
	
	FPCQueryInterface.hr = E_FAIL;
	FPCQueryInterface.iid = IID_IOleWindow;
	FPCQueryInterface.pvObject = NULL;

	::SendMessage(hWndFPC, FPCM_QUERYINTERFACE, 0, (LPARAM)&FPCQueryInterface);

	if (S_OK == FPCQueryInterface.hr)
	{
		IOleWindow* pOleWindow = (IOleWindow*)FPCQueryInterface.pvObject;

		pOleWindow->GetWindow(&hWnd);

		pOleWindow->Release();
	}

	return hWnd;
}

DWORD WINAPI FPC_AddGetBindInfoHandler(HFPC hFPC, PGET_BIND_INFO_HANDLER pHandler, LPARAM lParam)
{
    return ((CFlashOCXLoader*)hFPC)->GetContentManager().AddGetBindInfoHandler(pHandler, lParam);
}

HRESULT WINAPI FPC_RemoveGetBindInfoHandler(HFPC hFPC, DWORD dwCookie)
{
    return ((CFlashOCXLoader*)hFPC)->GetContentManager().RemoveGetBindInfoHandler(dwCookie);
}

LPVOID* WINAPI FPC_GetImportTableEntry(HFPC hFPC, LPCSTR lpszDLLName, LPCSTR lpszFuncName)
{
    return ((CFlashOCXLoader*)hFPC)->FindImportTableEntry(lpszDLLName, lpszFuncName);
}

DWORD WINAPI FPC_SetSoundListener(HFPC hFPC, PSOUNDLISTENER pSoundListener, LPARAM lParam)
{
	return ((CFlashOCXLoader*)hFPC)->SetSoundListener(pSoundListener, lParam);
}

DWORD WINAPI FPC_SetPreProcessURLHandler(HFPC hFPC, PPREPROCESSURLHANDLER pHandler, LPARAM lParam)
{
	return ((CFlashOCXLoader*)hFPC)->SetPreProcessURLHandler(pHandler, lParam);
}

void WINAPI FPC_SetContextEx(HFPC hFPC, LPCSTR lpszContext)
{
	((CFlashOCXLoader*)hFPC)->SetContext(lpszContext);
}

HRESULT WINAPI FPC_QueryInterface(HWND hwndFlashPlayerControl, REFIID iid, void** ppObject)
{
	SFPCQueryInterface FPCQueryInterface;
	
	FPCQueryInterface.hr = E_FAIL;
	FPCQueryInterface.iid = iid;
	FPCQueryInterface.pvObject = NULL;

	::SendMessage(hwndFlashPlayerControl, FPCM_QUERYINTERFACE, 0, (LPARAM)&FPCQueryInterface);

	*ppObject = FPCQueryInterface.pvObject;

	return FPCQueryInterface.hr;
}

BOOL WINAPI FPC_EnableFullScreen(HWND hwndFlashPlayerControl, BOOL bEnable)
{
	SFPCVersion MinVersionThatAllowsFullScreen;
	FPC_BuildVersion(9, 0, 28, 0, &MinVersionThatAllowsFullScreen);

	SFPCVersion CurrentFlashVersion;
	if (!FPC_GetVersionEx(FPC_GetHFPC(hwndFlashPlayerControl), &CurrentFlashVersion))
	{
		return FALSE;
	}

	if (FPC_CompareVersions(&CurrentFlashVersion, &MinVersionThatAllowsFullScreen) < 0)
	{
		return FALSE;
	}

	f_in_box::com_helpers::CComPtr<IUnknown> pUnknown;
	
	if (S_OK == FPC_QueryInterface(hwndFlashPlayerControl, IID_IUnknown, (void**)&pUnknown))
	{
		f_in_box::com_helpers::CComPtr<f_in_box::flash::v9_0_28_0::IShockwaveFlash> pFlash;
		
		if (S_OK == 
			pUnknown->QueryInterface(f_in_box::flash::v9_0_28_0::IShockwaveFlash::GetGUID(), (void**)&pFlash))
		{
			HRESULT hr = pFlash->put_AllowFullScreen(f_in_box::com_helpers::CBSTR(bEnable ? _T("true") : _T("false")));
			return S_OK == hr;
		}
	}

	return TRUE;
}

BOOL WINAPI FPC_IsFullScreenEnabled(HWND hwndFlashPlayerControl)
{
	SFPCVersion MinVersionThatAllowsFullScreen;
	FPC_BuildVersion(9, 0, 28, 0, &MinVersionThatAllowsFullScreen);

	SFPCVersion CurrentFlashVersion;
	if (!FPC_GetVersionEx(FPC_GetHFPC(hwndFlashPlayerControl), &CurrentFlashVersion))
	{
		return FALSE;
	}

	if (FPC_CompareVersions(&CurrentFlashVersion, &MinVersionThatAllowsFullScreen) < 0)
		return FALSE;

	f_in_box::com_helpers::CComPtr<IUnknown> pUnknown;
	
	if (S_OK == FPC_QueryInterface(hwndFlashPlayerControl, IID_IUnknown, (void**)&pUnknown))
	{
		f_in_box::com_helpers::CComPtr<f_in_box::flash::v9_0_28_0::IShockwaveFlash> pFlash;
		
		if (S_OK == 
			pUnknown->QueryInterface(f_in_box::flash::v9_0_28_0::IShockwaveFlash::GetGUID(), (void**)&pFlash))
		{
			f_in_box::com_helpers::CBSTR bstrAllowFullScreen;
			pFlash->get_AllowFullScreen(bstrAllowFullScreen.GetAddress());

			if (f_in_box::std::CString<char>((LPCSTR)bstrAllowFullScreen) == "true")
				return TRUE;
			else
				return FALSE;
		}
	}

	return TRUE;
}

HFPC WINAPI FPC_GetHFPC(HWND hwndFlashPlayerControl)
{
	CFPCWnd* pWnd = CFPCWnd::FromHWND(hwndFlashPlayerControl);
	return NULL != pWnd ? (HFPC)(CFlashOCXLoader*)(CFPCWnd::FromHWND(hwndFlashPlayerControl)->GetOCXLoader()) : NULL;
}

void WINAPI FPC_PaintTo(HWND hwndFlashPlayerControl, HDC hDC)
{
	CFPCWnd* pWnd = CFPCWnd::FromHWND(hwndFlashPlayerControl);
	
	if (NULL != pWnd)
		pWnd->PaintTo(hDC);
}

BOOL WINAPI FPC_CallWndProc(HWND hwndFlashPlayerControl, UINT message, WPARAM wParam, LPARAM lParam, LRESULT* plResult, BOOL* pbHandled)
{
	CFPCWnd* pWnd = CFPCWnd::FromHWND(hwndFlashPlayerControl);
	
	if (NULL != pWnd)
		return pWnd->CallWndProc(hwndFlashPlayerControl, message, wParam, lParam, plResult, pbHandled);
	else
		return FALSE;
}

BOOL WINAPI FPC_SetFocus(HWND hwndFlashPlayerControl, BOOL bFocus)
{
	CFPCWnd* pWnd = CFPCWnd::FromHWND(hwndFlashPlayerControl);
	
	if (NULL != pWnd)
	{
		pWnd->SetFocus(bFocus);
		return TRUE;
	}
	else
		return FALSE;
}

BOOL WINAPI FPC_GetFocus(HWND hwndFlashPlayerControl)
{
	CFPCWnd* pWnd = CFPCWnd::FromHWND(hwndFlashPlayerControl);
	
	if (NULL != pWnd)
		return S_OK == pWnd->GetFocus();
	else
		return FALSE;
}

BOOL WINAPI FPC_SetCapture(HWND hwndFlashPlayerControl, BOOL bCapture)
{
	CFPCWnd* pWnd = CFPCWnd::FromHWND(hwndFlashPlayerControl);
	
	if (NULL != pWnd)
	{
		pWnd->SetCapture(bCapture);
		return TRUE;
	}
	else
		return FALSE;
}

BOOL WINAPI FPC_GetCapture(HWND hwndFlashPlayerControl)
{
	CFPCWnd* pWnd = CFPCWnd::FromHWND(hwndFlashPlayerControl);
	
	if (NULL != pWnd)
		return S_OK == pWnd->GetCapture();
	else
		return FALSE;
}

BOOL WINAPI FPC_SetOption(HFPC hFPC, DWORD dwOption, DWORD dwValue)
{
    BOOL bRes;

    if (NULL != hFPC)
    {
		CFlashOCXLoader* pFlashOCXLoader = (CFlashOCXLoader*)hFPC;

		pFlashOCXLoader->SetOptionValue(dwOption, dwValue);

        bRes = TRUE;
    }
    else
        bRes = FALSE;

    return bRes;
}

DWORD WINAPI FPC_GetOption(HFPC hFPC, DWORD dwOption)
{
    DWORD dwRes = -1;

    if (NULL != hFPC)
    {
		CFlashOCXLoader* pFlashOCXLoader = (CFlashOCXLoader*)hFPC;

		dwRes = pFlashOCXLoader->GetOptionValue(dwOption);
    }

    return dwRes;
}

namespace f_in_box
{
	BOOL g_bMinimizeMemory;
	CCodeReplacerWithTrampoline* g_pCodeReplacer_NtQueryInformationProcess;

	typedef enum _PROCESSINFOCLASS {
		ProcessExecuteFlags = 0x22
	} PROCESSINFOCLASS;

	typedef DWORD NTSTATUS;

	typedef NTSTATUS (WINAPI *P_NtQueryInformationProcess)(
		HANDLE ProcessHandle, 
		PROCESSINFOCLASS ProcessInformationClass, 
		PVOID ProcessInformation, 
		ULONG ProcessInformationLength, 
		PULONG ReturnLength);
	static NTSTATUS WINAPI Static_NtQueryInformationProcess(
		HANDLE ProcessHandle, 
		PROCESSINFOCLASS ProcessInformationClass, 
		PVOID ProcessInformation, 
		ULONG ProcessInformationLength, 
		PULONG ReturnLength);

	NTSTATUS WINAPI Static_NtQueryInformationProcess(
		HANDLE ProcessHandle, 
		PROCESSINFOCLASS ProcessInformationClass, 
		PVOID ProcessInformation, 
		ULONG ProcessInformationLength, 
		PULONG ReturnLength)
	{
		CCodeReplacerWithTrampolineOwner owner(*g_pCodeReplacer_NtQueryInformationProcess);
		P_NtQueryInformationProcess pNtQueryInformationProcess = (P_NtQueryInformationProcess)owner.GetFunctionPointer();

		if (ProcessExecuteFlags == ProcessInformationClass)
		{
#define MEM_EXECUTE_OPTION_ENABLE (-1)
#define STATUS_INFO_LENGTH_MISMATCH ((NTSTATUS)0xC0000004L)
#define STATUS_SUCCESS (0)

			DWORD dwResult = MEM_EXECUTE_OPTION_ENABLE;

			if (ProcessInformationLength < sizeof(dwResult))
				return STATUS_INFO_LENGTH_MISMATCH;
			else
			{
				MemoryCopy(ProcessInformation, &dwResult, sizeof(dwResult));

				if (NULL != ReturnLength)
					*ReturnLength = sizeof(dwResult);

				return STATUS_SUCCESS;
			}
		}
		else
			return 
				pNtQueryInformationProcess(
					ProcessHandle, 
					ProcessInformationClass, 
					ProcessInformation, 
					ProcessInformationLength, 
					ReturnLength);
	}
};

void WINAPI FPC_SetGlobalOption(DWORD dwOption, DWORD dwValue)
{
	if (DEF_FINBOX_GLOBAL_OPTION__MINIMIZE_MEMORY == dwOption)
	{
		if (dwValue)
		{
			if (NULL == g_pCodeReplacer_NtQueryInformationProcess)
			{
				PVOID p = ::GetProcAddress(GetModuleHandle(_T("ntdll.dll")), "NtQueryInformationProcess");

				if (NULL != p)
				{
					g_pCodeReplacer_NtQueryInformationProcess = 
						new CCodeReplacerWithTrampoline(p, &f_in_box::Static_NtQueryInformationProcess 
                        // TODO: are you sure regarding _WIN64?
#ifndef _WIN64
                        , FALSE, NULL
#endif // !_WIN64
                        );

					if (g_pCodeReplacer_NtQueryInformationProcess->Start())
						f_in_box::g_bMinimizeMemory = TRUE;
					else
					{
						delete g_pCodeReplacer_NtQueryInformationProcess;
						g_pCodeReplacer_NtQueryInformationProcess = NULL;

						f_in_box::g_bMinimizeMemory = FALSE;
					}
				}
			}
		}
		else
		{
			if (NULL != g_pCodeReplacer_NtQueryInformationProcess)
			{
				delete g_pCodeReplacer_NtQueryInformationProcess;
				g_pCodeReplacer_NtQueryInformationProcess = NULL;
			}

			f_in_box::g_bMinimizeMemory = FALSE;
		}
	}
}

DWORD WINAPI FPC_GetGlobalOption(DWORD dwOption)
{
	if (DEF_FINBOX_GLOBAL_OPTION__MINIMIZE_MEMORY == dwOption)
		return f_in_box::g_bMinimizeMemory;
	else
		return FALSE;
}

/// Returns -1, if pVersion1 is less than pVersion2
/// Returns +1, if pVersion2 is less than pVersion1
int WINAPI FPC_CompareVersions(const SFPCVersion* pVersion1, const SFPCVersion* pVersion2)
{
	for (int i = 3; i >= 0; i--)
	{
		if (pVersion1->v[i] < pVersion2->v[i])
			return -1;
		if (pVersion1->v[i] > pVersion2->v[i])
			return +1;
	}

	return 0;
}

void WINAPI FPC_BuildVersion(WORD v3, WORD v2, WORD v1, WORD v0, SFPCVersion* pVersion)
{
	pVersion->v[0] = v0;
	pVersion->v[1] = v1;
	pVersion->v[2] = v2;
	pVersion->v[3] = v3;
}
