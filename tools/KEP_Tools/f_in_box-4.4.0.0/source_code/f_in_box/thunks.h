#pragma once

namespace f_in_box
{
    PVOID AllocThunkWithOneParam(PVOID pTargetFunction, PVOID param);
    PVOID AllocThunkWithTwoParams(PVOID pTargetFunction, PVOID param1, PVOID param2);
}
