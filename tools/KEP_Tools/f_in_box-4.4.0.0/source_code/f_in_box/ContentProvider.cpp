#include "stdafx.h"
#include "ContentProvider.h"
#include "ComPtr.h"
#include "CriticalSection.h"
#include "String.h"

namespace f_in_box
{

class CContentProvider : 
	public IContentProvider, 
	public IBinding
{
public:
    static GUID GetInterfaceIID()
    {
        // {B51AFD6B-34FB-457e-A7D4-DE317625F3A0}
        GUID IID_IContentProvider = 
            { 0xb51afd6b, 0x34fb, 0x457e, { 0xa7, 0xd4, 0xde, 0x31, 0x76, 0x25, 0xf3, 0xa0 } };

        return IID_IContentProvider;
    }

private:

	ULONG m_nRefCount;
    CCriticalSection m_cs;

	//
    f_in_box::com_helpers::CComPtr<IStream> m_pPreSavedData;
	DWORD m_dwSizeOfPreSavedData;

	// ������� m_pBindStatusCallback->OnStartBinding(0, NULL);
	BOOL m_bStartBindingCalled;

	// 
	f_in_box::com_helpers::CComPtr<IBindStatusCallback> m_pBindStatusCallback;

	//
	BOOL m_bDataSaved;

	//
	BOOL m_bEndOfData;

	//
	BOOL m_bSizeKnown;
	ULARGE_INTEGER m_nSize;

public:

	CContentProvider() : 
		m_nRefCount(1), 
		m_bStartBindingCalled(FALSE), 
		m_bDataSaved(FALSE), 
		m_dwSizeOfPreSavedData(0), 
		m_bEndOfData(FALSE), 
		m_bSizeKnown(FALSE)
	{
		CreateStreamOnHGlobal(NULL, TRUE, &m_pPreSavedData);

#ifdef _DEBUG

        TCHAR szBuffer[1024];
        wsprintf(szBuffer, _T("CContentProvider::CContentProvider(), this = 0x%.8x\n"), this);
        OutputDebugString(szBuffer);

#endif // !_DEBUG
    }

	~CContentProvider()
	{
#ifdef _DEBUG

        TCHAR szBuffer[1024];
        wsprintf(szBuffer, _T("CContentProvider::~CContentProvider(), this = 0x%.8x\n"), this);
        OutputDebugString(szBuffer);

#endif // !_DEBUG
	}
	
// IUnknown

    virtual HRESULT STDMETHODCALLTYPE QueryInterface(REFIID riid, void** ppvObject)
	{
		*ppvObject = NULL;

		if (f_in_box::com_helpers::IsEqualGUID(IID_IStream, riid))
			*ppvObject = (IStream*)this;
		else if (f_in_box::com_helpers::IsEqualGUID(IID_IUnknown, riid))
			*ppvObject = this;
		else if (f_in_box::com_helpers::IsEqualGUID(GetInterfaceIID(), riid))
			*ppvObject = this;
		else if (f_in_box::com_helpers::IsEqualGUID(IID_IBinding, riid))
			*ppvObject = (IBinding*)this;

		HRESULT hr;

		if (NULL != *ppvObject)
		{
			hr = S_OK;
			AddRef();
		}
		else
			hr = E_NOINTERFACE;

		return hr;
	}
    
    virtual ULONG STDMETHODCALLTYPE AddRef()
	{
        CCriticalSectionOwner sco(m_cs);

		return ++m_nRefCount;
	}
    
    virtual ULONG STDMETHODCALLTYPE Release()
	{
        ULONG nNewRefCount;

        {
            CCriticalSectionOwner sco(m_cs);

            nNewRefCount = --m_nRefCount;
        }

		if (0 == nNewRefCount)
			delete this;

		return nNewRefCount;
	}

// IContentProvider
	virtual HRESULT __stdcall SetBindStatusCallback(IBindStatusCallback* pBindStatusCallback)
	{
        CCriticalSectionOwner sco(m_cs);

		ASSERT(!m_pBindStatusCallback);
		ASSERT(NULL != pBindStatusCallback);
		ASSERT(!m_bStartBindingCalled);

		// ���������� BindStatusCallback
		m_pBindStatusCallback = pBindStatusCallback;

		if (m_bDataSaved)
			// ���-�� ��� ���� ��������
		{
			OnStartBinding();

			// Seek to the stream beginning
			LARGE_INTEGER liZero; DEF_ZERO_IT(liZero);
			ULARGE_INTEGER uliNewPosition;
			m_pPreSavedData->Seek(liZero, STREAM_SEEK_SET, &uliNewPosition);

			FORMATETC formatec; DEF_ZERO_IT(formatec);

			formatec.cfFormat = 0;
			formatec.ptd = NULL;
			formatec.dwAspect = DVASPECT_CONTENT;
			formatec.lindex = -1;
			formatec.tymed = TYMED_ISTREAM;

			STGMEDIUM stgmedium; DEF_ZERO_IT(stgmedium);

			stgmedium.tymed = TYMED_ISTREAM;
			stgmedium.pstm = m_pPreSavedData;
			stgmedium.pUnkForRelease = NULL;

			HRESULT hr;

			hr = m_pBindStatusCallback->OnDataAvailable(BSCF_FIRSTDATANOTIFICATION | BSCF_LASTDATANOTIFICATION, 
														m_dwSizeOfPreSavedData, 
														&formatec, 
														&stgmedium);

			m_pPreSavedData = NULL;

			if (m_bEndOfData)
				m_pBindStatusCallback->OnStopBinding(0, NULL);
		}

		return S_OK;
	}

	// S_OK - saved, S_FALSE - not saved
	virtual HRESULT __stdcall IsDataSaved()
	{
        CCriticalSectionOwner sco(m_cs);

		return m_bDataSaved ? S_OK : S_FALSE;
	}

	virtual HRESULT __stdcall EndOfData()
	{
        CCriticalSectionOwner sco(m_cs);

		if (!m_bEndOfData)
		{
			m_bEndOfData = TRUE;

			if (m_bDataSaved)
				if (m_pBindStatusCallback)
				{
					OnStartBinding();

					// ����� ������
					m_pBindStatusCallback->OnStopBinding(S_OK, L"");
				}
		}

		return S_OK;
	}

    virtual HRESULT __stdcall Write(const void *pv, ULONG cb, ULONG *pcbWritten)
	{
        CCriticalSectionOwner sco(m_cs);

		if (0 == cb)
		{
			EndOfData();

			return S_OK;
		}

        HRESULT hr = S_OK;

		m_bDataSaved = TRUE;

		if (m_pBindStatusCallback)
			// ���� ���� ���������� ������
		{
			OnStartBinding();

			f_in_box::com_helpers::CComPtr<IStream> pMemStream;
			CreateStreamOnHGlobal(NULL, TRUE, &pMemStream);

			pMemStream->Write(pv, cb, pcbWritten);

			LARGE_INTEGER liZero; DEF_ZERO_IT(liZero);
			ULARGE_INTEGER uliNewPosition;
			pMemStream->Seek(liZero, STREAM_SEEK_SET, &uliNewPosition);

			FORMATETC formatec; DEF_ZERO_IT(formatec);
				
			formatec.cfFormat = 0;
			formatec.ptd = NULL;
			formatec.dwAspect = DVASPECT_CONTENT;
			formatec.lindex = -1;
			formatec.tymed = TYMED_ISTREAM;

			STGMEDIUM stgmedium; DEF_ZERO_IT(stgmedium);

			stgmedium.tymed = TYMED_ISTREAM;
			stgmedium.pstm = pMemStream;
			stgmedium.pUnkForRelease = NULL;

			hr = m_pBindStatusCallback->OnDataAvailable(BSCF_FIRSTDATANOTIFICATION | BSCF_LASTDATANOTIFICATION, 
														cb, 
														&formatec, 
														&stgmedium);
		}
		else
			// ��������� ������ �������
		{
			m_pPreSavedData->Write(pv, cb, pcbWritten);
			m_dwSizeOfPreSavedData += cb;
		}

		return hr;
	}

	void OnStartBinding()
	{
		if (!m_bStartBindingCalled)
		{
			ProvideSize();

			// ��� ������ �� ���������� => �������� OnStartBinding
			m_pBindStatusCallback->OnStartBinding(0, this);
			m_bStartBindingCalled = TRUE;
		}
	}

	void ProvideSize()
	{
		f_in_box::com_helpers::CComPtr<IHttpNegotiate> pHttpNegotiate;

        if (S_OK == m_pBindStatusCallback->QueryInterface(IID_IHttpNegotiate, (void**)&pHttpNegotiate))
		{
			LPWSTR p = NULL;

			CHAR buf[1024]; DEF_ZERO_IT(buf);
			wsprintfA(buf, "HTTP/1.1 200 OK\r\nContent-Length: %d\r\n", m_bSizeKnown ? m_nSize.LowPart : 0);
			pHttpNegotiate->OnResponse(200, f_in_box::std::AToCW(buf).c_str(), NULL, &p);
		}
	}

	HRESULT __stdcall SetSize(ULARGE_INTEGER libNewSize)
	{
		m_bSizeKnown = TRUE;
		m_nSize = libNewSize;

		ProvideSize();

		return S_OK;
	}

	// IBinding

    HRESULT STDMETHODCALLTYPE Abort()
	{
		return E_NOTIMPL;
	}
    
    HRESULT STDMETHODCALLTYPE Suspend()
	{
		return E_NOTIMPL;
	}
    
    HRESULT STDMETHODCALLTYPE Resume()
	{
		return E_NOTIMPL;
	}
    
    HRESULT STDMETHODCALLTYPE SetPriority(LONG nPriority)
	{
		return E_NOTIMPL;
	}
    
    HRESULT STDMETHODCALLTYPE GetPriority(LONG* pnPriority)
	{
		return E_NOTIMPL;
	}
    
    HRESULT STDMETHODCALLTYPE GetBindResult(
		CLSID* pclsidProtocol, 
		DWORD* pdwResult, 
		LPOLESTR* pszResult, 
		DWORD* pdwReserved)
	{
		return E_NOTIMPL;
	}
};

class CContentProviderStream : public IStream
{
private:

	ULONG m_nRefCount;
    CCriticalSection m_cs;

	// 
	f_in_box::com_helpers::CComPtr<IContentProvider> m_pContentProvider;

public:

	CContentProviderStream(IContentProvider* pContentProvider) : 
		m_nRefCount(1), 
        m_pContentProvider(pContentProvider)
	{
#ifdef _DEBUG

        TCHAR szBuffer[1024];
        wsprintf(szBuffer, _T("CContentProviderStream::CContentProviderStream(), this = 0x%.8x\n"), this);
        OutputDebugString(szBuffer);

#endif // !_DEBUG
	}

	~CContentProviderStream()
	{
#ifdef _DEBUG

        TCHAR szBuffer[1024];
        wsprintf(szBuffer, _T("CContentProviderStream::~CContentProviderStream(), this = 0x%.8x\n"), this);
        OutputDebugString(szBuffer);

#endif // !_DEBUG
	}
	
// IUnknown

    virtual HRESULT STDMETHODCALLTYPE QueryInterface(REFIID riid, void** ppvObject)
	{
		*ppvObject = NULL;

		if (f_in_box::com_helpers::IsEqualGUID(IID_IStream, riid))
			*ppvObject = this;
		else if (f_in_box::com_helpers::IsEqualGUID(IID_IUnknown, riid))
			*ppvObject = this;

		HRESULT hr;

		if (NULL != *ppvObject)
		{
			hr = S_OK;
			AddRef();
		}
		else
			hr = E_NOINTERFACE;

		return hr;
	}
    
    virtual ULONG STDMETHODCALLTYPE AddRef()
	{
        CCriticalSectionOwner sco(m_cs);

		return ++m_nRefCount;
	}
    
    virtual ULONG STDMETHODCALLTYPE Release()
	{
		ULONG nNewRefCount;

        {
            CCriticalSectionOwner sco(m_cs);

            nNewRefCount = --m_nRefCount;
        }

		if (0 == nNewRefCount)
		{
			// ��������� ������
			ASSERT(m_pContentProvider);

			m_pContentProvider->EndOfData();

			delete this;
		}

		return nNewRefCount;
	}

// ISequentialStream

    virtual HRESULT STDMETHODCALLTYPE Read(void *pv, ULONG cb, ULONG *pcbRead)
	{
		return E_NOTIMPL;
	}
    
    virtual HRESULT STDMETHODCALLTYPE Write(const void *pv, ULONG cb, ULONG *pcbWritten)
	{
		ASSERT(m_pContentProvider);

		return m_pContentProvider->Write(pv, cb, pcbWritten);
	}

// IStream

    virtual /* [local] */ HRESULT STDMETHODCALLTYPE Seek( 
        /* [in] */ LARGE_INTEGER dlibMove,
        /* [in] */ DWORD dwOrigin,
        /* [out] */ ULARGE_INTEGER *plibNewPosition){ return E_NOTIMPL; }
    
    virtual HRESULT STDMETHODCALLTYPE SetSize( 
        /* [in] */ ULARGE_INTEGER libNewSize)
	{
		return m_pContentProvider->SetSize(libNewSize);
	}
    
    virtual /* [local] */ HRESULT STDMETHODCALLTYPE CopyTo( 
        /* [unique][in] */ IStream *pstm,
        /* [in] */ ULARGE_INTEGER cb,
        /* [out] */ ULARGE_INTEGER *pcbRead,
        /* [out] */ ULARGE_INTEGER *pcbWritten){ return E_NOTIMPL; }
    
    virtual HRESULT STDMETHODCALLTYPE Commit( 
        /* [in] */ DWORD grfCommitFlags){ return E_NOTIMPL; }
    
    virtual HRESULT STDMETHODCALLTYPE Revert( void){ return E_NOTIMPL; }
    
    virtual HRESULT STDMETHODCALLTYPE LockRegion( 
        /* [in] */ ULARGE_INTEGER libOffset,
        /* [in] */ ULARGE_INTEGER cb,
        /* [in] */ DWORD dwLockType){ return E_NOTIMPL; }
    
    virtual HRESULT STDMETHODCALLTYPE UnlockRegion( 
        /* [in] */ ULARGE_INTEGER libOffset,
        /* [in] */ ULARGE_INTEGER cb,
        /* [in] */ DWORD dwLockType){ return E_NOTIMPL; }
    
    virtual HRESULT STDMETHODCALLTYPE Stat( 
        /* [out] */ STATSTG *pstatstg,
        /* [in] */ DWORD grfStatFlag){ return E_NOTIMPL; }
    
    virtual HRESULT STDMETHODCALLTYPE Clone( 
        /* [out] */ IStream **ppstm){ return E_NOTIMPL; }
};

// Create content provider and stream which can be used for writing data
// ppStream �������� ������
// ppContentProvider ��������� ������ content manager'�
HRESULT CreateContentProvider(IContentProvider** ppContentProvider, IStream** ppStream)
{
	*ppContentProvider = new CContentProvider;
	*ppStream = new CContentProviderStream(*ppContentProvider);

	return S_OK;
}

}
