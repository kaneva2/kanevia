#ifndef __AUDIOCLIENT_H__
#define __AUDIOCLIENT_H__

namespace f_in_box
{
	extern const IID IID_IAudioClient;
	extern const IID IID_IAudioRenderClient;

	typedef int AUDCLNT_SHAREMODE;
	typedef LONGLONG REFERENCE_TIME;
	typedef REFERENCE_TIME *LPREFERENCE_TIME;

	/// Declaration of standard IAudioClient interface
    class IAudioClient : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE Initialize( 
            AUDCLNT_SHAREMODE ShareMode,
            DWORD StreamFlags,
            REFERENCE_TIME hnsBufferDuration,
            REFERENCE_TIME hnsPeriodicity,
            const WAVEFORMATEX* pFormat,
            const GUID* AudioSessionGuid) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetBufferSize(UINT32* pNumBufferFrames) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetStreamLatency(REFERENCE_TIME* phnsLatency) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetCurrentPadding(UINT32* pNumPaddingFrames) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE IsFormatSupported( 
            AUDCLNT_SHAREMODE ShareMode,
            const WAVEFORMATEX* pFormat,
            WAVEFORMATEX** ppClosestMatch) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetMixFormat( 
            WAVEFORMATEX** ppDeviceFormat) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetDevicePeriod( 
            REFERENCE_TIME* phnsDefaultDevicePeriod, 
            REFERENCE_TIME* phnsMinimumDevicePeriod) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE Start() = 0;
        virtual HRESULT STDMETHODCALLTYPE Stop() = 0;
        virtual HRESULT STDMETHODCALLTYPE Reset() = 0;
        virtual HRESULT STDMETHODCALLTYPE SetEventHandle(HANDLE eventHandle) = 0;
        virtual HRESULT STDMETHODCALLTYPE GetService(REFIID riid, void** ppv) = 0;
    };

	/// Declaration of standard IAudioRenderClient interface
    class IAudioRenderClient : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE GetBuffer(UINT32 NumFramesRequested, BYTE** ppData) = 0;
        virtual HRESULT STDMETHODCALLTYPE ReleaseBuffer(UINT32 NumFramesWritten, DWORD dwFlags) = 0;
    };
}

#endif // !__AUDIOCLIENT_H__
