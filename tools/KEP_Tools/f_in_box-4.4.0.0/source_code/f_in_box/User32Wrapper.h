#ifndef __USER32_WRAPPER_H__D866A8A7_995D_4371_BE35_ED716BB59B7C__
#define __USER32_WRAPPER_H__D866A8A7_995D_4371_BE35_ED716BB59B7C__

#define DEF_USER32_DLL_NAME                             _T("user32.dll")
#define DEF_UPDATE_LAYERED_WINDOW_FUNCTION_NAME         "UpdateLayeredWindow"

namespace f_in_box
{

struct SUser32Wrapper
{
    HMODULE m_HMODULE__User32DLL;

    BOOL (WINAPI *UpdateLayeredWindow)(
        HWND hwnd,             // handle to layered window
        HDC hdcDst,            // handle to screen DC
        POINT *pptDst,         // new screen position
        SIZE *psize,           // new size of the layered window
        HDC hdcSrc,            // handle to surface DC
        POINT *pptSrc,         // layer position
        COLORREF crKey,        // color key
        BLENDFUNCTION *pblend, // blend function
        DWORD dwFlags          // options
    );

    SUser32Wrapper()
    {
        m_HMODULE__User32DLL = LoadLibrary(DEF_USER32_DLL_NAME);
        (FARPROC&)UpdateLayeredWindow = 
            GetProcAddress(m_HMODULE__User32DLL, 
                           DEF_UPDATE_LAYERED_WINDOW_FUNCTION_NAME);
    }

    ~SUser32Wrapper()
    {
        FreeLibrary(m_HMODULE__User32DLL);
    }

    //
    BOOL IsLayeredWindowSupported()
    {
        return NULL != UpdateLayeredWindow;
    }
};

}

#endif // __USER32_WRAPPER_H__D866A8A7_995D_4371_BE35_ED716BB59B7C__
