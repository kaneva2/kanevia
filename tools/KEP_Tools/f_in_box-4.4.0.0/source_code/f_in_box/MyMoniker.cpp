#include "stdafx.h"
#include "MyMoniker.h"
#include "MyBindStatusCallback.h"
#include "ContentManager.h"
#include "FlashOCXLoader.h"

using namespace f_in_box;

CMyMoniker::CMyMoniker() : 
    m_nRefCount(0)
{
}

CMyMoniker::~CMyMoniker()
{
    ASSERT(0 == m_nRefCount);
}

HRESULT CMyMoniker::Init(CContentManager* pContentManager, LPCWSTR lpszURL, IMoniker* pmkContext)
{
	if (NULL == lpszURL)
		return E_POINTER;

	m_strURL = lpszURL;
	m_pmkContext = pmkContext;
    m_pContentManager = pContentManager;

	m_UrlMonHelper.CreateURLMoniker(pmkContext, lpszURL, &m_pStandardURLMoniker);

	return S_OK;
}

STDMETHODIMP CMyMoniker::GetClassID(CLSID *pClassID)
{
	return m_pStandardURLMoniker ? m_pStandardURLMoniker->GetClassID(pClassID) : E_FAIL;
}

STDMETHODIMP CMyMoniker::IsDirty(void)
{
	return m_pStandardURLMoniker ? m_pStandardURLMoniker->IsDirty() : E_FAIL;
}

STDMETHODIMP CMyMoniker::Load(IStream *pStm)
{
	return m_pStandardURLMoniker ? m_pStandardURLMoniker->Load(pStm) : E_FAIL;
}

STDMETHODIMP CMyMoniker::Save(IStream *pStm, BOOL fClearDirty)
{
	return m_pStandardURLMoniker ? m_pStandardURLMoniker->Save(pStm, fClearDirty) : E_FAIL;
}

STDMETHODIMP CMyMoniker::GetSizeMax(ULARGE_INTEGER *pcbSize)
{
	return m_pStandardURLMoniker ? m_pStandardURLMoniker->GetSizeMax(pcbSize) : E_FAIL;
}

STDMETHODIMP CMyMoniker::BindToObject(IBindCtx *pbc, IMoniker *pmkToLeft, REFIID riidResult, void **ppvResult)
{
	return m_pStandardURLMoniker ? m_pStandardURLMoniker->BindToObject(pbc, pmkToLeft, riidResult, ppvResult) : E_FAIL;
}

STDMETHODIMP CMyMoniker::BindToStorage(IBindCtx *pbc, IMoniker *pmkToLeft, REFIID riid, void **ppvObj)
{
    HRESULT hr = E_FAIL;

    f_in_box::com_helpers::CComPtr<IMyBindStatusCallback> pMyBindStatusCallback;
	CMyBindStatusCallback::CreateInstance(IMyBindStatusCallback::GetIID(), (void**)&pMyBindStatusCallback, m_pContentManager);

    f_in_box::com_helpers::CComPtr<IBindStatusCallback> pBindStatusCallback;
    m_UrlMonHelper.RegisterBindStatusCallback(pbc, pMyBindStatusCallback, &pBindStatusCallback, 0);

    pMyBindStatusCallback->SetLastBindStatusCallback(pBindStatusCallback);

	return m_pContentManager->BindToStorage(pbc, 
                                            pmkToLeft, 
                                            riid, 
                                            ppvObj, 
                                            m_pmkContext, 
                                            pMyBindStatusCallback, 
                                            m_strURL.c_str());
}

STDMETHODIMP CMyMoniker::Reduce(IBindCtx *pbc, DWORD dwReduceHowFar, IMoniker **ppmkToLeft, IMoniker **ppmkReduced)
{
	return m_pStandardURLMoniker ? m_pStandardURLMoniker->Reduce(pbc, dwReduceHowFar, ppmkToLeft, ppmkReduced) : E_FAIL;
}

STDMETHODIMP CMyMoniker::ComposeWith(IMoniker *pmkRight, BOOL fOnlyIfNotGeneric, IMoniker **ppmkComposite)
{
	return m_pStandardURLMoniker ? m_pStandardURLMoniker->ComposeWith(pmkRight, fOnlyIfNotGeneric, ppmkComposite) : E_FAIL;
}

STDMETHODIMP CMyMoniker::Enum(BOOL fForward, IEnumMoniker **ppenumMoniker)
{
	return m_pStandardURLMoniker ? m_pStandardURLMoniker->Enum(fForward, ppenumMoniker) : E_FAIL;
}

STDMETHODIMP CMyMoniker::IsEqual(IMoniker *pmkOtherMoniker)
{
	return m_pStandardURLMoniker ? m_pStandardURLMoniker->IsEqual(pmkOtherMoniker) : E_FAIL;
}

STDMETHODIMP CMyMoniker::Hash(DWORD *pdwHash)
{
	return m_pStandardURLMoniker ? m_pStandardURLMoniker->Hash(pdwHash) : E_FAIL;
}

STDMETHODIMP CMyMoniker::IsRunning(IBindCtx *pbc, IMoniker *pmkToLeft, IMoniker *pmkNewlyRunning)
{
	return m_pStandardURLMoniker ? m_pStandardURLMoniker->IsRunning(pbc, pmkToLeft, pmkNewlyRunning) : E_FAIL;
}

STDMETHODIMP CMyMoniker::GetTimeOfLastChange(IBindCtx *pbc, IMoniker *pmkToLeft, FILETIME *pFileTime)
{
	return m_pStandardURLMoniker ? m_pStandardURLMoniker->GetTimeOfLastChange(pbc, pmkToLeft, pFileTime) : E_FAIL;
}

STDMETHODIMP CMyMoniker::Inverse(IMoniker **ppmk)
{
	return m_pStandardURLMoniker ? m_pStandardURLMoniker->Inverse(ppmk) : E_FAIL;
}

STDMETHODIMP CMyMoniker::CommonPrefixWith(IMoniker *pmkOther, IMoniker **ppmkPrefix)
{
	return m_pStandardURLMoniker ? m_pStandardURLMoniker->CommonPrefixWith(pmkOther, ppmkPrefix) : E_FAIL;
}

STDMETHODIMP CMyMoniker::RelativePathTo(IMoniker *pmkOther, IMoniker **ppmkRelPath)
{
	return m_pStandardURLMoniker ? m_pStandardURLMoniker->RelativePathTo(pmkOther, ppmkRelPath) : E_FAIL;
}

STDMETHODIMP CMyMoniker::GetDisplayName(IBindCtx *pbc, IMoniker *pmkToLeft, LPOLESTR *ppszDisplayName)
{
	return m_pStandardURLMoniker ? m_pStandardURLMoniker->GetDisplayName(pbc, pmkToLeft, ppszDisplayName) : E_FAIL;

	//// ������� ���������� ��� E_NOTIMPL, �� ����� ������ ���-�� ������ urlmon.dll
	//// ��� ������� ������ �� ������� ���� �� ������

	//CComPtr<IMalloc> pMalloc;
	//CoGetMalloc(1, &pMalloc);

	//DWORD dwLength = (m_strURL.length() + 1) * sizeof(WCHAR);
	//*ppszDisplayName = (WCHAR*)pMalloc->Alloc(dwLength);
	//f_in_box::MemoryCopy(*ppszDisplayName, m_strURL.c_str(), dwLength);

 //   return S_OK;
}

STDMETHODIMP CMyMoniker::ParseDisplayName(IBindCtx *pbc, IMoniker *pmkToLeft, LPOLESTR pszDisplayName, ULONG *pchEaten, IMoniker **ppmkOut)
{
	return m_pStandardURLMoniker ? m_pStandardURLMoniker->ParseDisplayName(pbc, pmkToLeft, pszDisplayName, pchEaten, ppmkOut): E_FAIL;
}

STDMETHODIMP CMyMoniker::IsSystemMoniker(DWORD *pdwMksys)
{
	return m_pStandardURLMoniker ? m_pStandardURLMoniker->IsSystemMoniker(pdwMksys): E_FAIL;

	//*pdwMksys = MKSYS_NONE;
 //   return S_FALSE;
}

//
HRESULT CMyMoniker::Create(LPCWSTR lpszURL, 
                           CContentManager* pContentManager, 
                           IMoniker* pmkContext, 
                           REFIID refiid, 
                           LPVOID* ppObject)
{
	CMyMoniker* pMyMoniker = new CMyMoniker;

    HRESULT hr = pMyMoniker->QueryInterface(refiid, ppObject);

    if (S_OK != hr)
        delete pMyMoniker;
    else
		pMyMoniker->Init(pContentManager, lpszURL, pmkContext);

	return hr;
}

HRESULT STDMETHODCALLTYPE CMyMoniker::QueryInterface(REFIID riid, void** ppvObject)
{
    void* pObject = NULL;

    if (f_in_box::com_helpers::IsEqualGUID(IID_IUnknown, riid))
        pObject = (IUnknown*)this;
    else if (f_in_box::com_helpers::IsEqualGUID(IID_IMoniker, riid))
        pObject = (IMoniker*)this;

    *ppvObject = pObject;

    HRESULT hr;

    if (NULL != pObject)
    {
        AddRef();

        hr = S_OK;
    }
    else
	{
		if (m_pStandardURLMoniker)
			hr = m_pStandardURLMoniker->QueryInterface(riid, ppvObject);
		else
			hr = E_NOINTERFACE;
	}

    return hr;
}

ULONG STDMETHODCALLTYPE CMyMoniker::AddRef()
{
    return ++m_nRefCount;
}

ULONG STDMETHODCALLTYPE CMyMoniker::Release()
{
    ULONG nNewRefCount = --m_nRefCount;

    if (0 == nNewRefCount)
        delete this;

    return nNewRefCount;
}
