#include "stdafx.h"
#include "Wnd.h"
#include "MemManager.h"
#include "thunks.h"
#include "Globals.h"

using namespace f_in_box;

CWnd::CWnd() : 
    m_lpAdapterCode(NULL), 
    m_hWnd(NULL), 
	m_OldWNDPROC(NULL)
{
}

CWnd::~CWnd()
{
    if (NULL != m_lpAdapterCode)
    {
        VirtualFree(m_lpAdapterCode, 0, MEM_RELEASE);
        m_lpAdapterCode = NULL;
    }
}

void CWnd::Subclass(HWND hWnd, BOOL bAutoDelete)
{
    ASSERT(NULL != hWnd);

    m_hWnd = hWnd;

	m_bAutoDelete = bAutoDelete;

    // Create adapter
    m_lpAdapterCode = AllocThunkWithOneParam(&StaticWindowProc, this);

	if (IsWindowUnicode(hWnd))
	{
		HMODULE hUser32DLL = LoadLibrary(_T("user32.dll"));

		typedef LONG_PTR (WINAPI *P_GetWindowLongPtrW)(HWND hWnd, LONG_PTR nIndex);
		P_GetWindowLongPtrW pGetWindowLongPtrW;
        (FARPROC&)pGetWindowLongPtrW = GetProcAddress(hUser32DLL, "GetWindowLongPtrW");

        if (NULL != pGetWindowLongPtrW)
        {
            m_OldWNDPROC = (WNDPROC)pGetWindowLongPtrW(hWnd, GWLP_WNDPROC);
        }
        else
        {
		    typedef LONG (WINAPI *P_GetWindowLongW)(HWND hWnd, int nIndex);
		    P_GetWindowLongW pGetWindowLongW;
            (FARPROC&)pGetWindowLongW = GetProcAddress(hUser32DLL, "GetWindowLongW");

		    if (NULL != pGetWindowLongW)
			    m_OldWNDPROC = (WNDPROC)pGetWindowLongW(hWnd, GWL_WNDPROC);
		    else
			    m_OldWNDPROC = (WNDPROC)GetWindowLongA(hWnd, GWL_WNDPROC);
        }

		FreeLibrary(hUser32DLL);
	}
	else
    {
#ifdef _WIN64
		m_OldWNDPROC = (WNDPROC)GetWindowLongPtrA(hWnd, GWLP_WNDPROC);
#else
		m_OldWNDPROC = (WNDPROC)GetWindowLongA(hWnd, GWL_WNDPROC);
#endif // _WIN64
    }

	if (IsWindowUnicode(hWnd))
	{
		HMODULE hUser32DLL = LoadLibrary(_T("user32.dll"));

        typedef LONG_PTR (WINAPI *P_SetWindowLongPtrW)(HWND hWnd, int nIndex, LONG_PTR dwNewLong);
		P_SetWindowLongPtrW pSetWindowLongPtrW;
        (FARPROC&)pSetWindowLongPtrW = GetProcAddress(hUser32DLL, "SetWindowLongPtrW");

        if (NULL != pSetWindowLongPtrW)
        {
            m_OldWNDPROC = (WNDPROC)pSetWindowLongPtrW(hWnd, GWLP_WNDPROC, (LONG_PTR)m_lpAdapterCode);
        }
        else
        {
            typedef LONG (WINAPI *P_SetWindowLongW)(HWND hWnd, int nIndex, LONG dwNewLong);
		    P_SetWindowLongW pSetWindowLongW;
		    HMODULE hUser32DLL = LoadLibrary(_T("user32.dll"));
		    (FARPROC&)pSetWindowLongW = GetProcAddress(hUser32DLL, "SetWindowLongW");

		    if (NULL != pSetWindowLongW)
			    pSetWindowLongW(hWnd, GWL_WNDPROC, (LONG)m_lpAdapterCode);
		    else
			    SetWindowLongA(hWnd, GWL_WNDPROC, (LONG)m_lpAdapterCode);
        }

		FreeLibrary(hUser32DLL);
	}
	else
    {
#ifdef _WIN64
		SetWindowLongPtrA(hWnd, GWLP_WNDPROC, (LONG_PTR)m_lpAdapterCode);
#else
		SetWindowLongA(hWnd, GWL_WNDPROC, (LONG)m_lpAdapterCode);
#endif // _WIN64
    }
}

LRESULT
CALLBACK
CWnd::WindowProc(
    HWND hWnd,
    UINT uMsg,
    WPARAM wParam,
    LPARAM lParam
)
{
	LRESULT lResult = CallOldWindowProc(hWnd, uMsg, wParam, lParam);

    if (m_bAutoDelete && WM_NCDESTROY == uMsg)
        delete this;

    return lResult;
}

LRESULT
CALLBACK
CWnd::CallOldWindowProc(
    HWND hWnd,
    UINT uMsg,
    WPARAM wParam,
    LPARAM lParam
)
{
    if (NULL != m_OldWNDPROC)
	{
		if (IsWindowUnicode(hWnd))
		{
			typedef LRESULT (WINAPI *P_CallWindowProcW)(WNDPROC lpPrevWndFunc, HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam);
			P_CallWindowProcW pCallWindowProcW;
			HMODULE hUser32DLL = LoadLibrary(_T("user32.dll"));
			(FARPROC&)pCallWindowProcW = GetProcAddress(hUser32DLL, "CallWindowProcW");
			LRESULT lResult;
			if (NULL != pCallWindowProcW)
				lResult = pCallWindowProcW(m_OldWNDPROC, hWnd, uMsg, wParam, lParam);
			else
				lResult = CallWindowProcA(m_OldWNDPROC, hWnd, uMsg, wParam, lParam);
			FreeLibrary(hUser32DLL);
			return lResult;
		}
		else
	        return CallWindowProcA(m_OldWNDPROC, hWnd, uMsg, wParam, lParam);
	}
    else
    {
//        ASSERT(FALSE);

        return -1;
    }
}

LRESULT
CALLBACK
CWnd::StaticWindowProc(
    HWND hWnd, 
    UINT uMsg, 
    WPARAM wParam, 
    LPARAM lParam 
)
{
    CWnd* pWnd = (CWnd*)TlsGetValue(Globals::g_nTlsSlotParam1);
    ASSERT(NULL != pWnd);

    return pWnd->WindowProc(hWnd, uMsg, wParam, lParam);
}
