#ifndef __STDAFX_H__5DFDB980_0C1C_4b6b_AB4C_74F27822044D__
#define __STDAFX_H__5DFDB980_0C1C_4b6b_AB4C_74F27822044D__

#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers

#ifndef WINVER				// Allow use of features specific to Windows 95 and Windows NT 4 or later.
#define WINVER 0x0400		// Change this to the appropriate value to target Windows 98 and Windows 2000 or later.
#endif

#ifndef _WIN32_WINNT		// Allow use of features specific to Windows NT 4 or later.
#define _WIN32_WINNT 0x0500		// Change this to the appropriate value to target Windows 98 and Windows 2000 or later.
#endif						

#ifndef _WIN32_WINDOWS		// Allow use of features specific to Windows 98 or later.
#define _WIN32_WINDOWS 0x0410 // Change this to the appropriate value to target Windows Me or later.
#endif

#ifndef _WIN32_IE			// Allow use of features specific to IE 4.0 or later.
#define _WIN32_IE 0x0400	// Change this to the appropriate value to target IE 5.0 or later.
#endif

#include <urlmon.h>

#define DEF_ARRAY_SIZE(array)		(sizeof(array) / sizeof(array[0]))
#define DEF_ZERO_IT(var)			(f_in_box::MemoryZero(&(var), sizeof(var)))

#include <tchar.h>

#ifdef _DEBUG
#define ASSERT(expr) \
	if (!(expr)) \
	{ \
		char* buf = (char*)VirtualAlloc(NULL, 64 * 1024, MEM_COMMIT, PAGE_READWRITE); \
		wsprintfA(buf, "File: %s, line: %d", __FILE__, __LINE__); \
		\
		if (IDYES == \
			::MessageBoxA(NULL, \
						  buf, \
						  "[F-IN-BOX] Debug Assertion Failed", \
						  MB_YESNO | MB_SYSTEMMODAL)) \
			DebugBreak(); \
	}
#else
#define ASSERT(expr)
#endif // _DEBUG

#include <oleidl.h>
#include <ocidl.h>

#include <windows.h>
#include <windowsx.h>

#include "MemManager.h"

#define DEF_IS_INTRESOURCE(_r) (((ULONG)(_r) >> 16) == 0)

#define DEF_WS_EX_LAYERED 0x80000

#define DEF_AC_SRC_ALPHA  0x01

#define DEF_ULW_ALPHA     0x00000002

#ifndef DWORD_PTR
#ifdef _WIN64
#define DWORD_PTR DWORD64
#else
#define DWORD_PTR DWORD
#endif // _WIN64
#endif// !DWORD_PTR



// char* szContext = (char*)lParam
#define DEF_PRIVATE_MESSAGE__SET_CONTEXT         "DEF_SET_CONTEXT__44DC0B36_ACE9_4861_BB29_7951E0BB2066"
#define DEF_RIGHT_CONTEXT                        "bf5376db-eb4e-4155-a373-ca5ec74baea8"



#ifdef _WIN64

// In x64 stack must be 16-bit aligned
#define STACK_ALIGN (16)

#endif // _WIN64

#ifndef GWL_WNDPROC
#define GWL_WNDPROC (-4)
#endif // !GWL_WNDPROC

#endif // !__STDAFX_H__5DFDB980_0C1C_4b6b_AB4C_74F27822044D__

// no_sal2.h is unavailable in VS2015 update 2.
// I don't see where it's actually being used.
// Removing it doesn't seem to have any effect.
//#include <no_sal2.h>
#include <ddraw.h>
#include <mmsystem.h>
