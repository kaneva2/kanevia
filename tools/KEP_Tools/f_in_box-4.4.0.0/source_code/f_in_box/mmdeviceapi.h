#ifndef __MMDEVICEAPI_H__
#define __MMDEVICEAPI_H__

namespace f_in_box
{
	extern const IID IID_IMMDeviceEnumerator;
	extern const IID IID_IMMDevice;
	extern const CLSID CLSID_MMDeviceEnumerator;
    extern const IID IID_ISimpleAudioVolume;
	extern const IID IID_IAudioStreamVolume;

    enum EDataFlow
    {
		eRender	= 0, 
		eCapture = ( eRender + 1 ), 
		eAll = ( eCapture + 1 ), 
		EDataFlow_enum_count = ( eAll + 1 )
    };

	enum ERole
	{
		eConsole = 0, 
		eMultimedia	= ( eConsole + 1 ) ,
		eCommunications	= ( eMultimedia + 1 ) ,
		ERole_enum_count	= ( eCommunications + 1 ) 
	};

	/// Declaration of standard IMMDevice interface
	class IMMDevice : public IUnknown
	{
	public:
		virtual HRESULT STDMETHODCALLTYPE Activate(REFIID iid, DWORD dwClsCtx, PROPVARIANT* pActivationParams, void** ppInterface) = 0;
		virtual HRESULT STDMETHODCALLTYPE OpenPropertyStore(DWORD stgmAccess, PVOID /* IPropertyStore* */ * ppProperties) = 0;
		virtual HRESULT STDMETHODCALLTYPE GetId(LPWSTR* ppstrId) = 0;
		virtual HRESULT STDMETHODCALLTYPE GetState(DWORD* pdwState) = 0;
	};

	/// Declaration of standard IMMDeviceCollection interface
    class IMMDeviceCollection : public IUnknown
    {
	public:
		virtual HRESULT STDMETHODCALLTYPE GetCount(UINT* pcDevices) = 0;
		virtual HRESULT STDMETHODCALLTYPE Item(UINT nDevice, IMMDevice** ppDevice) = 0;
    };

	/// Declaration of standard IMMDeviceEnumerator interface
	class IMMDeviceEnumerator : public IUnknown
	{
	public:
		virtual HRESULT STDMETHODCALLTYPE EnumAudioEndpoints(EDataFlow dataFlow, DWORD dwStateMask, IMMDeviceCollection** ppDevices) = 0;
		virtual HRESULT STDMETHODCALLTYPE GetDefaultAudioEndpoint(EDataFlow dataFlow, ERole role, IMMDevice** ppEndpoint) = 0;
	    virtual HRESULT STDMETHODCALLTYPE GetDevice(LPCWSTR pwstrId, IMMDevice** ppDevice) = 0;
		virtual HRESULT STDMETHODCALLTYPE RegisterEndpointNotificationCallback(PVOID /*IMMNotificationClient*/ * pClient) = 0;
		virtual HRESULT STDMETHODCALLTYPE UnregisterEndpointNotificationCallback(PVOID /*IMMNotificationClient*/ * pClient) = 0;
	};

	/// Declaration of standard ISimpleAudioVolume  interface
	class ISimpleAudioVolume : public IUnknown
	{
	public:
        virtual HRESULT STDMETHODCALLTYPE SetMasterVolume(float fLevel, LPCGUID EventContext) = 0;
        virtual HRESULT STDMETHODCALLTYPE GetMasterVolume(float* pfLevel) = 0;
        virtual HRESULT STDMETHODCALLTYPE SetMute(const BOOL bMute, LPCGUID EventContext) = 0;
        virtual HRESULT STDMETHODCALLTYPE GetMute(BOOL* pbMute) = 0;
	};

	/// Declaration of standard IAudioStreamVolume  interface
	class IAudioStreamVolume : public IUnknown
	{
	public:
		virtual HRESULT STDMETHODCALLTYPE GetChannelCount(UINT32 *pdwCount) = 0;
		virtual HRESULT STDMETHODCALLTYPE SetChannelVolume(UINT32 dwIndex, const float fLevel) = 0;
		virtual HRESULT STDMETHODCALLTYPE GetChannelVolume(UINT32 dwIndex, float *pfLevel) = 0;
		virtual HRESULT STDMETHODCALLTYPE SetAllVolumes(UINT32 dwCount, const float *pfVolumes) = 0;
		virtual HRESULT STDMETHODCALLTYPE GetAllVolumes(UINT32 dwCount, float *pfVolumes) = 0;
	};

}

#endif // !__MMDEVICEAPI_H__
