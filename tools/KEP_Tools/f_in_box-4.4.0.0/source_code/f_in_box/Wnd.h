#ifndef __WND_H__ADA8019F_7FEC_4898_8514_247A467603F7__
#define __WND_H__ADA8019F_7FEC_4898_8514_247A467603F7__

namespace f_in_box
{
    class CWnd
    {
    private:
        LPVOID m_lpAdapterCode;
        BOOL m_bAutoDelete;

        // TODO: make this member private
    protected:
        WNDPROC m_OldWNDPROC;

    public:
        HWND m_hWnd;

    public:
        CWnd();
        virtual ~CWnd();

        void Subclass(HWND hWnd, BOOL bAutoDelete);

    protected:
        virtual
        LRESULT
        CALLBACK
        WindowProc(
            HWND hWnd,
            UINT uMsg,
            WPARAM wParam,
            LPARAM lParam
        );

        LRESULT
        CALLBACK
        CallOldWindowProc(
            HWND hWnd,
            UINT uMsg,
            WPARAM wParam,
            LPARAM lParam
        );

    private:
        static
        LRESULT
        CALLBACK
        StaticWindowProc(
            HWND hWnd, 
            UINT uMsg, 
            WPARAM wParam, 
            LPARAM lParam 
        );
    };
}

#endif // !__WND_H__ADA8019F_7FEC_4898_8514_247A467603F7__
