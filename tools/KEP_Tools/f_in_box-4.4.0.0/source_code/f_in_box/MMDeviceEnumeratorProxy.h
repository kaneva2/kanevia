#ifndef __MMDeviceEnumeratorProxy_H_
#define __MMDeviceEnumeratorProxy_H_

namespace f_in_box
{
	HRESULT CreateMMDeviceEnumeratorProxy(CFlashOCXLoader* pLoader, IMMDeviceEnumerator* pOriginalObject, REFIID refiid, PVOID* ppProxyObject);
}

#endif // !__MMDeviceEnumeratorProxy_H_
