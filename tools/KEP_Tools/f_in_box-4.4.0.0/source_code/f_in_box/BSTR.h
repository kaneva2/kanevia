#ifndef __BSTR_H__ECD6A1E6_3F50_448a_8AAE_9ACD396D69E9__
#define __BSTR_H__ECD6A1E6_3F50_448a_8AAE_9ACD396D69E9__

#include "string.h"

namespace f_in_box
{
    namespace com_helpers
    {
        class CBSTR
        {
        private:

            BSTR m_bstr;
            std::CString<CHAR> m_strAnsiString;

        public:
            CBSTR(BSTR bstr = NULL) : 
                m_bstr(NULL)
            {
                if (NULL != bstr)
                {
                    m_bstr = SysAllocString(bstr);
                }
            }

            CBSTR(LPCSTR sz)
            {
                std::CString<CHAR> str(sz);
                m_bstr = SysAllocString(std::AToCW(str.c_str()).c_str());
            }

            CBSTR(LPCWSTR sz)
            {
                std::CString<WCHAR> str(sz);
                m_bstr = SysAllocString(str.c_str());
            }

            ~CBSTR()
            {
                if (NULL != m_bstr)
                {
                    SysFreeString(m_bstr);
                }
            }

            BSTR GetBSTR()
            {
                return m_bstr;
            }

            BSTR* GetAddress()
            {
                ASSERT(NULL == m_bstr);

                return &m_bstr;
            }

            UINT length() const
            {
                return SysStringLen(m_bstr);
            }

            operator LPCSTR()
            {
				if (m_bstr)
				{
					m_strAnsiString = std::CString<CHAR>(std::WToCA(m_bstr).c_str());
					return m_strAnsiString.c_str();
				}
				else
					return "";
            }

            operator LPSTR()
            {
				if (m_bstr)
				{
					m_strAnsiString = std::CString<CHAR>(std::WToCA(m_bstr).c_str());
					return (LPSTR)m_strAnsiString.c_str();
				}
				else
					return "";
            }

            operator LPCWSTR()
            {
				return m_bstr ? m_bstr : L"";
            }

            operator LPWSTR()
            {
				return m_bstr ? (LPWSTR)m_bstr : L"";
            }
        };
    }
}

#endif // __BSTR_H__ECD6A1E6_3F50_448a_8AAE_9ACD396D69E9__
