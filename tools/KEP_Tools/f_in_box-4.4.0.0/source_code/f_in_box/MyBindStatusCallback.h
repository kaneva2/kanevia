#ifndef __MY_BIND_STATUS_CALLBACK_H__9986DE53_8648_4ad9_9A75_77CE737010FC__
#define __MY_BIND_STATUS_CALLBACK_H__9986DE53_8648_4ad9_9A75_77CE737010FC__

#include "ComPtr.h"

namespace f_in_box
{

class IMyBindStatusCallback : public IBindStatusCallback
{
public:
    static IID GetIID()
    {
        // {E56BFD27-F33C-41e9-A0C2-BEBD2D86CD8A}
        GUID guid = 
            { 0xe56bfd27, 0xf33c, 0x41e9, { 0xa0, 0xc2, 0xbe, 0xbd, 0x2d, 0x86, 0xcd, 0x8a } };

        return guid;
    }

    virtual HRESULT __stdcall SetLastBindStatusCallback(IBindStatusCallback* pBindStatusCallback) = 0;
};

class CMyBindStatusCallback : 
	public IMyBindStatusCallback, 
	public IHttpNegotiate
{
private:
    ULONG m_nRefCount;

	CContentManager* m_pContentManager;

public:
    f_in_box::com_helpers::CComPtr<IBindStatusCallback> m_pLastBindStatusCallback;

private:
    CMyBindStatusCallback(CContentManager* pContentManager);
    virtual ~CMyBindStatusCallback();

public:

    static HRESULT CreateInstance(REFIID refiid, void** ppObject, CContentManager* pContentManager);

private:
    // IUnknown
    virtual HRESULT STDMETHODCALLTYPE QueryInterface(REFIID riid, void** ppvObject);
    virtual ULONG STDMETHODCALLTYPE AddRef();
    virtual ULONG STDMETHODCALLTYPE Release();

    // IMyBindStatusCallback
    virtual HRESULT __stdcall SetLastBindStatusCallback(IBindStatusCallback* pBindStatusCallback);

    // IBindStatusCallback
        virtual HRESULT STDMETHODCALLTYPE OnStartBinding( 
            /* [in] */ DWORD dwReserved,
            /* [in] */ IBinding *pib) ;
        
        virtual HRESULT STDMETHODCALLTYPE GetPriority( 
            /* [out] */ LONG *pnPriority) ;
        
        virtual HRESULT STDMETHODCALLTYPE OnLowResource( 
            /* [in] */ DWORD reserved) ;
        
        virtual HRESULT STDMETHODCALLTYPE OnProgress( 
            /* [in] */ ULONG ulProgress,
            /* [in] */ ULONG ulProgressMax,
            /* [in] */ ULONG ulStatusCode,
            /* [in] */ LPCWSTR szStatusText) ;
        
        virtual HRESULT STDMETHODCALLTYPE OnStopBinding( 
            /* [in] */ HRESULT hresult,
            /* [unique][in] */ LPCWSTR szError) ;
        
        virtual /* [local] */ HRESULT STDMETHODCALLTYPE GetBindInfo( 
            /* [out] */ DWORD *grfBINDF,
            /* [unique][out][in] */ BINDINFO *pbindinfo) ;
        
        virtual /* [local] */ HRESULT STDMETHODCALLTYPE OnDataAvailable( 
            /* [in] */ DWORD grfBSCF,
            /* [in] */ DWORD dwSize,
            /* [in] */ FORMATETC *pformatetc,
            /* [in] */ STGMEDIUM *pstgmed) ;
        
        virtual HRESULT STDMETHODCALLTYPE OnObjectAvailable( 
            /* [in] */ REFIID riid,
            /* [iid_is][in] */ IUnknown *punk) ;

	// IHttpNegotiate
        virtual HRESULT STDMETHODCALLTYPE BeginningTransaction( 
            /* [in] */ LPCWSTR szURL,
            /* [unique][in] */ LPCWSTR szHeaders,
            /* [in] */ DWORD dwReserved,
            /* [out] */ LPWSTR *pszAdditionalHeaders)
		{
			HRESULT hr = S_OK;

			if (m_pLastBindStatusCallback.IsNotNull())
			{
				f_in_box::com_helpers::CComPtr<IHttpNegotiate> pHttpNegotiate;

                if (S_OK == m_pLastBindStatusCallback->QueryInterface(IID_IHttpNegotiate, (void**)&pHttpNegotiate))
					hr = pHttpNegotiate->BeginningTransaction(szURL, szHeaders, dwReserved, pszAdditionalHeaders);
			}

			return hr;
		}
        
        virtual HRESULT STDMETHODCALLTYPE OnResponse( 
            /* [in] */ DWORD dwResponseCode,
            /* [unique][in] */ LPCWSTR szResponseHeaders,
            /* [unique][in] */ LPCWSTR szRequestHeaders,
            /* [out] */ LPWSTR *pszAdditionalRequestHeaders)
		{
			HRESULT hr = S_OK;

			if (m_pLastBindStatusCallback.IsNotNull())
			{
				f_in_box::com_helpers::CComPtr<IHttpNegotiate> pHttpNegotiate;

                if (S_OK == m_pLastBindStatusCallback->QueryInterface(IID_IHttpNegotiate, (void**)&pHttpNegotiate))
					hr = pHttpNegotiate->OnResponse(dwResponseCode, szResponseHeaders, szRequestHeaders, pszAdditionalRequestHeaders);
			}

			return hr;
		}
};

}

#endif // !__MY_BIND_STATUS_CALLBACK_H__9986DE53_8648_4ad9_9A75_77CE737010FC__
