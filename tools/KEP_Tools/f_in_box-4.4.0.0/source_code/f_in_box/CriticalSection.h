//=====================================================================================================

#ifndef __CRITICAL_SECTION_H__71C44736_1C35_4622_A53B_54DE5714D281__
#define __CRITICAL_SECTION_H__71C44736_1C35_4622_A53B_54DE5714D281__

//=====================================================================================================

namespace f_in_box
{

//=====================================================================================================
//
class CCriticalSection
{
    // Variables
    // Private Variables
private:
    //
    CRITICAL_SECTION m_cs;

    // Functions
    // Public Functions
public:
    // Конструктор
    CCriticalSection();
    // Деструктор
    ~CCriticalSection();

    //
    void Enter();
    //
    void Leave();

    operator LPCRITICAL_SECTION();
};
//=====================================================================================================

//=====================================================================================================
//
class CCriticalSectionOwner
{
    // Variables
    // Private Variables
private:
    //
    LPCRITICAL_SECTION m_lpcs;

    // Functions
    // Public Functions
public:
    // Конструктор
    CCriticalSectionOwner(LPCRITICAL_SECTION lpcs);
    // Деструктор
    ~CCriticalSectionOwner();
};
//=====================================================================================================

}

#endif // !__CRITICAL_SECTION_H__71C44736_1C35_4622_A53B_54DE5714D281__
