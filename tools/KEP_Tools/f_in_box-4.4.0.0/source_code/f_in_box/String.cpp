#include "stdafx.h"
#include "String.h"
#include "MemManager.h"

namespace f_in_box
{
    namespace std
    {
        CString<CHAR> WToCA(LPCWSTR p)
        {
            SIZE_T nLength = CString<WCHAR>::GetLength(p);

            nLength = WideCharToMultiByte(CP_ACP, 0, p, (int)nLength, NULL, 0, NULL, NULL);

            CHAR* pChar = new CHAR[nLength + 1];
	        f_in_box::MemoryZero(pChar, sizeof(CHAR) * (nLength + 1) );

            WideCharToMultiByte(CP_ACP, 0, p, (int)nLength, pChar, (int)nLength, NULL, NULL);

            CString<CHAR> strRes = pChar;

            delete[] pChar;

            return strRes;
        }

        CString<WCHAR> AToCW(LPCSTR p)
        {
            SIZE_T nLength = CString<CHAR>::GetLength(p);

            nLength = MultiByteToWideChar(CP_ACP, 0, p, -1, NULL, 0);

            WCHAR* pWChar = new WCHAR[nLength + 1];
	        f_in_box::MemoryZero(pWChar, sizeof(WCHAR) * (nLength + 1) );

            MultiByteToWideChar(CP_ACP, 0, p, -1, pWChar, (int)nLength + 1);

            CString<WCHAR> strRes = pWChar;

            delete[] pWChar;

            return strRes;
        }
    }
}
