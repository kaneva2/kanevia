//
// Softanics, Artem A. Razin, All rights reserved, Copyright 2004 - 2009
//
// Description: contains two types of code replacer, that are used to intercept Windows API functions
//

#ifndef __CODEREPLACER_H__
#define __CODEREPLACER_H__

#include "CriticalSection.h"
#include "String.h"
#include "MoveInstruction.h"

namespace f_in_box
{
	class CCodeReplacerWithTrampolineOwner;

	class CCodeReplacerWithTrampoline
	{
		friend class CCodeReplacerWithTrampolineOwner;

	private:
		// Replacer initialized correctly
		BOOL m_bReady;

		HMODULE m_hModule;

		// Pointer to the function
		LPVOID m_pFunction;

		// Pointer to trampoline
		LPVOID m_pTrampoline;

		//
		PVOID m_pTrampolineFromOriginalFunction;

		// New bytes that jump to new function
//#ifdef _WIN64
//		BYTE m_OldBytes[12];
//		BYTE m_NewBytes[12];
//#else
		BYTE m_OldBytes[5];
		BYTE m_NewBytes[5];
//#endif // _WIN64

		// Address of new function; new bytes (see above) contains a jump (JMP) to m_lpNewFunctionAddress
		LPVOID m_lpNewFunctionAddress;

		//
		DWORD m_dwTag;

	private:

		static void CopyMemoryEx(PVOID Destination, const VOID* Source, SIZE_T Length)
		{
			DWORD dwOldProtect;

			// INFO: if we call VirtualProtect with PAGE_READWRITE, it raises an exception (DEP?? multiprocessor??)
			VirtualProtect(Destination, Length, PAGE_EXECUTE_READWRITE, &dwOldProtect);
			f_in_box::MemoryCopy(Destination, Source, Length);
			VirtualProtect(Destination, Length, dwOldProtect, &dwOldProtect);

			FlushInstructionCache(GetCurrentProcess(), Destination, Length);
		}

	public:

		void Init(LPVOID pFunction, LPVOID lpNewFunctionAddress

#ifndef _WIN64

			, BOOL bWithParam, PVOID pParam

#endif // !_WIN64
			
			)
		{
			m_pFunction = pFunction;

			// Save old bytes
			f_in_box::MemoryCopy(m_OldBytes, m_pFunction, sizeof(m_OldBytes));

			SYSTEM_INFO SystemInfo;
			GetSystemInfo(&SystemInfo);

			MEMORY_BASIC_INFORMATION mem_info;
			DEF_ZERO_IT(mem_info);
			VirtualQuery(m_pFunction, &mem_info, sizeof(mem_info));

			PBYTE pAddressToTryAlloc = (PBYTE)mem_info.BaseAddress + mem_info.RegionSize;

			while (true)
			{
				m_pTrampolineFromOriginalFunction = 
					VirtualAlloc(pAddressToTryAlloc, sizeof(m_NewBytes), MEM_RESERVE, PAGE_EXECUTE_READWRITE);

				if (NULL == m_pTrampolineFromOriginalFunction)
				{
					pAddressToTryAlloc += SystemInfo.dwPageSize;
					continue;
				}
				else
				{
					m_pTrampolineFromOriginalFunction = 
						VirtualAlloc(pAddressToTryAlloc, sizeof(m_NewBytes), MEM_COMMIT, PAGE_EXECUTE_READWRITE);

					if (NULL == m_pTrampolineFromOriginalFunction)
						continue;

					break;
				}
			}

#ifdef _WIN64
			ASSERT( (DWORD_PTR)m_pTrampolineFromOriginalFunction - (DWORD_PTR)m_pFunction < (DWORD)-1 );
#endif // _WIN64

			// �������� ������ �� ������� �� m_pTrampolineFromOriginalFunction
			m_NewBytes[0] = 0xe9; // jmp
			DWORD_PTR nDifference = (DWORD_PTR)m_pTrampolineFromOriginalFunction - ((DWORD_PTR)m_pFunction + 1 + sizeof(DWORD));
			f_in_box::MemoryCopy(&m_NewBytes[1], &nDifference, sizeof(nDifference)); // relative

			// ������ �������� �������� m_pTrampolineFromOriginalFunction
			PBYTE pTrampolineFromOriginalFunction = (PBYTE)m_pTrampolineFromOriginalFunction;

#ifdef _WIN64

//000007FFFFE2000E 48 81 EC 08 00 00 00 sub         rsp, 8 
//000007FFFFE20015 C7 44 24 00 AE 5D 58 77 mov         dword ptr [rsp], 77585DAEh 
//000007FFFFE2001D C7 44 24 04 00 00 00 00 mov         dword ptr [rsp+4], 0 
//000007FFFFE20025 C3               ret  

			const BYTE instr_sub[] = { 0x48, 0x81, 0xEC, 0x08, 0x00, 0x00, 0x00 };
			BYTE instr_first_mov[] = { 0xC7, 0x44, 0x24, 0x00 };
			BYTE instr_second_mov[] = { 0xC7, 0x44, 0x24, 0x04 };
			BYTE instr_ret[] = { 0xC3 };

			size_t nOffset = 0;

			f_in_box::MemoryCopy(&pTrampolineFromOriginalFunction[nOffset], instr_sub, sizeof(instr_sub));
			nOffset += sizeof(instr_sub);

			f_in_box::MemoryCopy(&pTrampolineFromOriginalFunction[nOffset], instr_first_mov, sizeof(instr_first_mov));
			nOffset += sizeof(instr_first_mov);
			DWORD dwLowReturnAddress = (DWORD)((DWORD_PTR)lpNewFunctionAddress & 0xffffffff);
			f_in_box::MemoryCopy(&pTrampolineFromOriginalFunction[nOffset], &dwLowReturnAddress, sizeof(dwLowReturnAddress));
			nOffset += sizeof(dwLowReturnAddress);

			f_in_box::MemoryCopy(&pTrampolineFromOriginalFunction[nOffset], instr_second_mov, sizeof(instr_second_mov));
			nOffset += sizeof(instr_second_mov);
			DWORD dwHighReturnAddress = (DWORD)((DWORD_PTR)lpNewFunctionAddress >> 32);
			f_in_box::MemoryCopy(&pTrampolineFromOriginalFunction[nOffset], &dwHighReturnAddress, sizeof(dwHighReturnAddress));
			nOffset += sizeof(dwHighReturnAddress);

			f_in_box::MemoryCopy(&pTrampolineFromOriginalFunction[nOffset], instr_ret, sizeof(instr_ret));
			nOffset += sizeof(instr_ret);

			//pTrampolineFromOriginalFunction[0] = 0x48;
			//pTrampolineFromOriginalFunction[1] = 0xb8;
			//f_in_box::MemoryCopy(&pTrampolineFromOriginalFunction[2], &lpNewFunctionAddress, sizeof(lpNewFunctionAddress));
			//pTrampolineFromOriginalFunction[2 + sizeof(lpNewFunctionAddress)] = 0x50;
			//pTrampolineFromOriginalFunction[3 + sizeof(lpNewFunctionAddress)] = 0xc3;
#else

			int nOffset = 0;

			if (bWithParam)
			{
/*
	//
	// ret addr <-- esp
	// arg[n]
	// arg[n-1]
	// ...
	//

	__asm
	{
		push eax
00411460 50               push        eax  
		push eax
00411461 50               push        eax  
		mov eax, [esp + 8]
00411462 8B 44 24 08      mov         eax,dword ptr [esp+8] 

	// eax: ret addr
	//
	// old eax
	// old eax
	// ret addr
	// arg[n]
	// arg[n-1]
	// ...
	//

		mov [esp + 4], eax
00411466 89 44 24 04      mov         dword ptr [esp+4],eax 
		mov dword ptr [esp + 8], 0x123
0041146A C7 44 24 08 23 01 00 00 mov         dword ptr [esp+8],123h 

	// eax: ret addr
	//
	// old eax
	// ret addr
	// 0x123
	// arg[n]
	// arg[n-1]
	// ...
	//

		pop eax
00411472 58               pop         eax  
*/

				const BYTE add_param_1[] = { 0x50, 0x50, 0x8b, 0x44, 0x24, 0x08, 0x89, 0x44, 0x24, 0x04, 0xc7, 0x44, 0x24, 0x08 };
				const BYTE add_param_2[] = { 0x58 };

				f_in_box::MemoryCopy(&pTrampolineFromOriginalFunction[nOffset], &add_param_1[0], sizeof(add_param_1));
				nOffset += sizeof(add_param_1);

				f_in_box::MemoryCopy(&pTrampolineFromOriginalFunction[nOffset], &pParam, sizeof(pParam));
				nOffset += sizeof(pParam);

				f_in_box::MemoryCopy(&pTrampolineFromOriginalFunction[nOffset], &add_param_2[0], sizeof(add_param_2));
				nOffset += sizeof(add_param_2);
			}

			pTrampolineFromOriginalFunction[nOffset] = 0xe9; // jmp
			nOffset++;

			nDifference = (DWORD_PTR)m_lpNewFunctionAddress - ((DWORD_PTR)pTrampolineFromOriginalFunction + nOffset + sizeof(DWORD_PTR));
			f_in_box::MemoryCopy(&pTrampolineFromOriginalFunction[nOffset], &nDifference, sizeof(nDifference)); // relative
#endif // _WIN64

//#ifdef _WIN64
//			m_NewBytes[0] = 0x48;
//			m_NewBytes[1] = 0xb8;
//			f_in_box::MemoryCopy(&m_NewBytes[2], &lpNewFunctionAddress, sizeof(lpNewFunctionAddress));
//			m_NewBytes[2 + sizeof(lpNewFunctionAddress)] = 0x50;
//			m_NewBytes[3 + sizeof(lpNewFunctionAddress)] = 0xc3;
//#else
			//m_NewBytes[0] = 0xe9; // jmp
			//DWORD_PTR nDifference = (DWORD_PTR)m_lpNewFunctionAddress - ((DWORD_PTR)m_pFunction + 1 + sizeof(DWORD_PTR));
			//f_in_box::MemoryCopy(&m_NewBytes[1], &nDifference, sizeof(nDifference)); // relative
//#endif // _WIN64

			int nTotalSize = 0;
			PBYTE pInstruction = (PBYTE)m_pFunction;

			while (nTotalSize < sizeof(m_NewBytes))
			{
				f_in_box::Utils::MoveInstruction::instruction_t InstructionInfo;
				BYTE nInstructionSize = f_in_box::Utils::MoveInstruction::GetInstruction(pInstruction, &InstructionInfo);
				pInstruction += nInstructionSize;
				nTotalSize += nInstructionSize;
			}

			PBYTE pWhereWeShouldReturnFromTrampoline = pInstruction;

#ifdef _WIN64
			BYTE m_TrampolineJump[12];
#else
			BYTE m_TrampolineJump[5];
#endif // _WIN64

			// TODO: ��� ����� ������ ����� ������ ������� ����, � �� �������� ����� �������� �������; ���� ������� ���������� ���������
			DWORD dwTrampolineSize = nTotalSize + sizeof(m_TrampolineJump);
			m_pTrampoline = VirtualAlloc(NULL, dwTrampolineSize, MEM_COMMIT | MEM_TOP_DOWN, PAGE_EXECUTE_READWRITE);

			PBYTE pFrom = (PBYTE)m_pFunction;
			PBYTE pTo = (PBYTE)m_pTrampoline;

			while (nTotalSize > 0)
			{
				f_in_box::Utils::MoveInstruction::instruction_t InstructionInfo;
				int nInstructionSize = f_in_box::Utils::MoveInstruction::GetInstruction(pFrom, &InstructionInfo);
				ASSERT(nInstructionSize > 0);
				nTotalSize -= nInstructionSize;

				f_in_box::Utils::MoveInstruction::MoveInstruction(pFrom, pTo, pTo - pFrom);
				pFrom += nInstructionSize;
				pTo += nInstructionSize;
			}

#ifdef _WIN64

			nOffset = 0;

			f_in_box::MemoryCopy(&pTo[nOffset], instr_sub, sizeof(instr_sub));
			nOffset += sizeof(instr_sub);

			f_in_box::MemoryCopy(&pTo[nOffset], instr_first_mov, sizeof(instr_first_mov));
			nOffset += sizeof(instr_first_mov);
			dwLowReturnAddress = (DWORD)((DWORD_PTR)pWhereWeShouldReturnFromTrampoline & 0xffffffff);
			f_in_box::MemoryCopy(&pTo[nOffset], &dwLowReturnAddress, sizeof(dwLowReturnAddress));
			nOffset += sizeof(dwLowReturnAddress);

			f_in_box::MemoryCopy(&pTo[nOffset], instr_second_mov, sizeof(instr_second_mov));
			nOffset += sizeof(instr_second_mov);
			dwHighReturnAddress = (DWORD)((DWORD_PTR)pWhereWeShouldReturnFromTrampoline >> 32);
			f_in_box::MemoryCopy(&pTo[nOffset], &dwHighReturnAddress, sizeof(dwHighReturnAddress));
			nOffset += sizeof(dwHighReturnAddress);

			f_in_box::MemoryCopy(&pTo[nOffset], instr_ret, sizeof(instr_ret));
			nOffset += sizeof(instr_ret);

			//pTo[0] = 0x48;
			//pTo[1] = 0xb8;
			//f_in_box::MemoryCopy(&pTo[2], &pWhereWeShouldReturnFromTrampoline, sizeof(pWhereWeShouldReturnFromTrampoline));
			//pTo[2 + sizeof(lpNewFunctionAddress)] = 0x50;
			//pTo[3 + sizeof(lpNewFunctionAddress)] = 0xc3;
#else
			pTo[0] = 0xe9; // jmp
			nDifference = (DWORD_PTR)pWhereWeShouldReturnFromTrampoline - ((DWORD_PTR)&pTo[1] + sizeof(DWORD_PTR));
			f_in_box::MemoryCopy(&pTo[1], &nDifference, sizeof(nDifference)); // relative
#endif // _WIN64

			FlushInstructionCache(GetCurrentProcess(), m_pTrampoline, dwTrampolineSize);

			m_bReady = TRUE;
		}

		BOOL IsReadyToUse() const
		{
			return m_bReady;
		}

		CCodeReplacerWithTrampoline(LPCSTR szDLLName, LPCSTR szFunctionName, LPVOID lpNewFunctionAddress
			
#ifndef _WIN64

			, BOOL bWithParam, PVOID pParam

#endif // !_WIN64

				, DWORD dwTag = 0

			) : 
			m_bReady(FALSE), 
			m_hModule(NULL), 
			m_pTrampoline(NULL), 
			m_pTrampolineFromOriginalFunction(NULL), 
			m_lpNewFunctionAddress(lpNewFunctionAddress), 
			m_dwTag(dwTag)
		{
			m_hModule = LoadLibraryA(szDLLName);

			if (NULL == m_hModule)
				return;

			PVOID pFunction = GetProcAddress(m_hModule, szFunctionName);

			if (NULL == pFunction)
				return;

			Init(pFunction, lpNewFunctionAddress

#ifndef _WIN64

			, bWithParam, pParam

#endif // !_WIN64

				);
		}

		CCodeReplacerWithTrampoline(LPVOID pFunction, LPVOID lpNewFunctionAddress
			
#ifndef _WIN64

			, BOOL bWithParam, PVOID pParam

#endif // !_WIN64

			, DWORD dwTag = 0

			) : 
			m_bReady(FALSE), 
			m_hModule(NULL), 
			m_pTrampoline(NULL), 
			m_pTrampolineFromOriginalFunction(NULL), 
			m_lpNewFunctionAddress(lpNewFunctionAddress), 
			m_dwTag(dwTag)
		{
			if (NULL == pFunction)
				return;

			Init(pFunction, lpNewFunctionAddress
				
#ifndef _WIN64

			, bWithParam, pParam

#endif // !_WIN64

				);
		}

		~CCodeReplacerWithTrampoline()
		{
			Stop();

			if (NULL != m_hModule)
			{
				FreeLibrary(m_hModule);
				m_hModule = NULL;
			}

			if (NULL != m_pTrampoline)
			{
				VirtualFree(m_pTrampoline, 0, MEM_RELEASE);
				m_pTrampoline = NULL;
			}

			if (NULL != m_pTrampolineFromOriginalFunction)
			{
				VirtualFree(m_pTrampolineFromOriginalFunction, 0, MEM_RELEASE);
				m_pTrampolineFromOriginalFunction = NULL;
			}

			m_bReady = FALSE;
		}

		// Start intercepting
		BOOL Start()
		{
			if (!m_bReady)
				return FALSE;

			CopyMemoryEx(m_pFunction, m_NewBytes, sizeof(m_NewBytes));

			return TRUE;
		}

		// Stop intercepting
		BOOL Stop()
		{
			if (!m_bReady)
				return FALSE;

			CopyMemoryEx(m_pFunction, m_OldBytes, sizeof(m_OldBytes));

			return TRUE;
		}

		DWORD GetTag() const { return m_dwTag; }

		LPVOID GetFunctionPointer() const
		{
			return m_pTrampoline;
		}
	};
	class CCodeReplacerWithTrampolineOwner
	{
	private:
		CCodeReplacerWithTrampoline& m_CodeReplacer;

	public:
		CCodeReplacerWithTrampolineOwner(CCodeReplacerWithTrampoline& CodeReplacer) : 
			m_CodeReplacer(CodeReplacer)
		{
		}

		~CCodeReplacerWithTrampolineOwner()
		{
		}

		LPVOID GetFunctionPointer() const
		{
			return m_CodeReplacer.GetFunctionPointer();
		}
	};

	typedef CCodeReplacerWithTrampolineOwner CCodeReplacerOwner;
	typedef CCodeReplacerWithTrampoline CCodeReplacer;

	typedef CCodeReplacerWithTrampolineOwner CCodeReplacerOwnerJMP;
	typedef CCodeReplacerWithTrampoline CCodeReplacerJMP;
}

#endif // !__CODEREPLACER_H__
