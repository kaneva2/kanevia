HRESULT WINAPI FPC_SetZoomRect(/* in */ HWND hwndFlashPlayerControl, /* in */ long left, /* in */ long top, /* in */ long right, /* in */ long bottom)
{
    SFPCSetZoomRect FPCSetZoomRect; DEF_ZERO_IT(FPCSetZoomRect);

    FPCSetZoomRect.hr = E_FAIL;

    FPCSetZoomRect.left = left;
    FPCSetZoomRect.top = top;
    FPCSetZoomRect.right = right;
    FPCSetZoomRect.bottom = bottom;

    ::SendMessage(hwndFlashPlayerControl, FPCM_SETZOOMRECT, NULL, (LPARAM)&FPCSetZoomRect);

    return FPCSetZoomRect.hr;
}

HRESULT WINAPI FPC_Zoom(/* in */ HWND hwndFlashPlayerControl, /* in */ int factor)
{
    SFPCZoom FPCZoom; DEF_ZERO_IT(FPCZoom);

    FPCZoom.hr = E_FAIL;

    FPCZoom.factor = factor;

    ::SendMessage(hwndFlashPlayerControl, FPCM_ZOOM, NULL, (LPARAM)&FPCZoom);

    return FPCZoom.hr;
}

HRESULT WINAPI FPC_Pan(/* in */ HWND hwndFlashPlayerControl, /* in */ long x, /* in */ long y, /* in */ int mode)
{
    SFPCPan FPCPan; DEF_ZERO_IT(FPCPan);

    FPCPan.hr = E_FAIL;

    FPCPan.x = x;
    FPCPan.y = y;
    FPCPan.mode = mode;

    ::SendMessage(hwndFlashPlayerControl, FPCM_PAN, NULL, (LPARAM)&FPCPan);

    return FPCPan.hr;
}

HRESULT WINAPI FPC_Play(/* in */ HWND hwndFlashPlayerControl)
{
    SFPCPlay FPCPlay; DEF_ZERO_IT(FPCPlay);

    FPCPlay.hr = E_FAIL;


    ::SendMessage(hwndFlashPlayerControl, FPCM_PLAY, NULL, (LPARAM)&FPCPlay);

    return FPCPlay.hr;
}

HRESULT WINAPI FPC_Stop(/* in */ HWND hwndFlashPlayerControl)
{
    SFPCStop FPCStop; DEF_ZERO_IT(FPCStop);

    FPCStop.hr = E_FAIL;


    ::SendMessage(hwndFlashPlayerControl, FPCM_STOP, NULL, (LPARAM)&FPCStop);

    return FPCStop.hr;
}

HRESULT WINAPI FPC_Back(/* in */ HWND hwndFlashPlayerControl)
{
    SFPCBack FPCBack; DEF_ZERO_IT(FPCBack);

    FPCBack.hr = E_FAIL;


    ::SendMessage(hwndFlashPlayerControl, FPCM_BACK, NULL, (LPARAM)&FPCBack);

    return FPCBack.hr;
}

HRESULT WINAPI FPC_Forward(/* in */ HWND hwndFlashPlayerControl)
{
    SFPCForward FPCForward; DEF_ZERO_IT(FPCForward);

    FPCForward.hr = E_FAIL;


    ::SendMessage(hwndFlashPlayerControl, FPCM_FORWARD, NULL, (LPARAM)&FPCForward);

    return FPCForward.hr;
}

HRESULT WINAPI FPC_Rewind(/* in */ HWND hwndFlashPlayerControl)
{
    SFPCRewind FPCRewind; DEF_ZERO_IT(FPCRewind);

    FPCRewind.hr = E_FAIL;


    ::SendMessage(hwndFlashPlayerControl, FPCM_REWIND, NULL, (LPARAM)&FPCRewind);

    return FPCRewind.hr;
}

HRESULT WINAPI FPC_StopPlay(/* in */ HWND hwndFlashPlayerControl)
{
    SFPCStopPlay FPCStopPlay; DEF_ZERO_IT(FPCStopPlay);

    FPCStopPlay.hr = E_FAIL;


    ::SendMessage(hwndFlashPlayerControl, FPCM_STOPPLAY, NULL, (LPARAM)&FPCStopPlay);

    return FPCStopPlay.hr;
}

HRESULT WINAPI FPC_GotoFrame(/* in */ HWND hwndFlashPlayerControl, /* in */ long FrameNum)
{
    SFPCGotoFrame FPCGotoFrame; DEF_ZERO_IT(FPCGotoFrame);

    FPCGotoFrame.hr = E_FAIL;

    FPCGotoFrame.FrameNum = FrameNum;

    ::SendMessage(hwndFlashPlayerControl, FPCM_GOTOFRAME, NULL, (LPARAM)&FPCGotoFrame);

    return FPCGotoFrame.hr;
}

HRESULT WINAPI FPC_CurrentFrame(/* in */ HWND hwndFlashPlayerControl, /* out */ long* Result)
{
    SFPCCurrentFrame FPCCurrentFrame; DEF_ZERO_IT(FPCCurrentFrame);

    FPCCurrentFrame.hr = E_FAIL;

    if (NULL == Result)
        return E_POINTER;

    ::SendMessage(hwndFlashPlayerControl, FPCM_CURRENTFRAME, NULL, (LPARAM)&FPCCurrentFrame);

    if (S_OK == FPCCurrentFrame.hr)
        *Result = FPCCurrentFrame.Result;

    return FPCCurrentFrame.hr;
}

HRESULT WINAPI FPC_IsPlaying(/* in */ HWND hwndFlashPlayerControl, /* out */ VARIANT_BOOL* Result)
{
    SFPCIsPlaying FPCIsPlaying; DEF_ZERO_IT(FPCIsPlaying);

    FPCIsPlaying.hr = E_FAIL;

    if (NULL == Result)
        return E_POINTER;

    ::SendMessage(hwndFlashPlayerControl, FPCM_ISPLAYING, NULL, (LPARAM)&FPCIsPlaying);

    if (S_OK == FPCIsPlaying.hr)
        *Result = FPCIsPlaying.Result;

    return FPCIsPlaying.hr;
}

HRESULT WINAPI FPC_PercentLoaded(/* in */ HWND hwndFlashPlayerControl, /* out */ long* Result)
{
    SFPCPercentLoaded FPCPercentLoaded; DEF_ZERO_IT(FPCPercentLoaded);

    FPCPercentLoaded.hr = E_FAIL;

    if (NULL == Result)
        return E_POINTER;

    ::SendMessage(hwndFlashPlayerControl, FPCM_PERCENTLOADED, NULL, (LPARAM)&FPCPercentLoaded);

    if (S_OK == FPCPercentLoaded.hr)
        *Result = FPCPercentLoaded.Result;

    return FPCPercentLoaded.hr;
}

HRESULT WINAPI FPC_FrameLoaded(/* in */ HWND hwndFlashPlayerControl, /* in */ long FrameNum, /* out */ VARIANT_BOOL* Result)
{
    SFPCFrameLoaded FPCFrameLoaded; DEF_ZERO_IT(FPCFrameLoaded);

    FPCFrameLoaded.hr = E_FAIL;

    if (NULL == Result)
        return E_POINTER;
    FPCFrameLoaded.FrameNum = FrameNum;

    ::SendMessage(hwndFlashPlayerControl, FPCM_FRAMELOADED, NULL, (LPARAM)&FPCFrameLoaded);

    if (S_OK == FPCFrameLoaded.hr)
        *Result = FPCFrameLoaded.Result;

    return FPCFrameLoaded.hr;
}

HRESULT WINAPI FPC_FlashVersion(/* in */ HWND hwndFlashPlayerControl, /* out */ long* Result)
{
    SFPCFlashVersion FPCFlashVersion; DEF_ZERO_IT(FPCFlashVersion);

    FPCFlashVersion.hr = E_FAIL;

    if (NULL == Result)
        return E_POINTER;

    ::SendMessage(hwndFlashPlayerControl, FPCM_FLASHVERSION, NULL, (LPARAM)&FPCFlashVersion);

    if (S_OK == FPCFlashVersion.hr)
        *Result = FPCFlashVersion.Result;

    return FPCFlashVersion.hr;
}

HRESULT WINAPI FPC_LoadMovieA(/* in */ HWND hwndFlashPlayerControl, /* in */ int layer, /* in */ LPCSTR url)
{
    SFPCLoadMovieA FPCLoadMovieA; DEF_ZERO_IT(FPCLoadMovieA); 

    FPCLoadMovieA.hr = E_FAIL;

    FPCLoadMovieA.layer = layer;
    FPCLoadMovieA.url.lpszBuffer = url;

    ::SendMessage(hwndFlashPlayerControl, FPCM_LOADMOVIEA, NULL, (LPARAM)&FPCLoadMovieA);

    return FPCLoadMovieA.hr;
}

HRESULT WINAPI FPC_LoadMovieW(/* in */ HWND hwndFlashPlayerControl, /* in */ int layer, /* in */ LPCWSTR url)
{
    SFPCLoadMovieW FPCLoadMovieW; DEF_ZERO_IT(FPCLoadMovieW);

    FPCLoadMovieW.hr = E_FAIL;

    FPCLoadMovieW.layer = layer;
    FPCLoadMovieW.url.lpszBuffer = url;

    ::SendMessage(hwndFlashPlayerControl, FPCM_LOADMOVIEW, NULL, (LPARAM)&FPCLoadMovieW);

    return FPCLoadMovieW.hr;
}

HRESULT WINAPI FPC_TGotoFrameA(/* in */ HWND hwndFlashPlayerControl, /* in */ LPCSTR target, /* in */ long FrameNum)
{
    SFPCTGotoFrameA FPCTGotoFrameA; DEF_ZERO_IT(FPCTGotoFrameA); 

    FPCTGotoFrameA.hr = E_FAIL;

    FPCTGotoFrameA.target.lpszBuffer = target;
    FPCTGotoFrameA.FrameNum = FrameNum;

    ::SendMessage(hwndFlashPlayerControl, FPCM_TGOTOFRAMEA, NULL, (LPARAM)&FPCTGotoFrameA);

    return FPCTGotoFrameA.hr;
}

HRESULT WINAPI FPC_TGotoFrameW(/* in */ HWND hwndFlashPlayerControl, /* in */ LPCWSTR target, /* in */ long FrameNum)
{
    SFPCTGotoFrameW FPCTGotoFrameW; DEF_ZERO_IT(FPCTGotoFrameW);

    FPCTGotoFrameW.hr = E_FAIL;

    FPCTGotoFrameW.target.lpszBuffer = target;
    FPCTGotoFrameW.FrameNum = FrameNum;

    ::SendMessage(hwndFlashPlayerControl, FPCM_TGOTOFRAMEW, NULL, (LPARAM)&FPCTGotoFrameW);

    return FPCTGotoFrameW.hr;
}

HRESULT WINAPI FPC_TGotoLabelA(/* in */ HWND hwndFlashPlayerControl, /* in */ LPCSTR target, /* in */ LPCSTR label)
{
    SFPCTGotoLabelA FPCTGotoLabelA; DEF_ZERO_IT(FPCTGotoLabelA); 

    FPCTGotoLabelA.hr = E_FAIL;

    FPCTGotoLabelA.target.lpszBuffer = target;
    FPCTGotoLabelA.label.lpszBuffer = label;

    ::SendMessage(hwndFlashPlayerControl, FPCM_TGOTOLABELA, NULL, (LPARAM)&FPCTGotoLabelA);

    return FPCTGotoLabelA.hr;
}

HRESULT WINAPI FPC_TGotoLabelW(/* in */ HWND hwndFlashPlayerControl, /* in */ LPCWSTR target, /* in */ LPCWSTR label)
{
    SFPCTGotoLabelW FPCTGotoLabelW; DEF_ZERO_IT(FPCTGotoLabelW);

    FPCTGotoLabelW.hr = E_FAIL;

    FPCTGotoLabelW.target.lpszBuffer = target;
    FPCTGotoLabelW.label.lpszBuffer = label;

    ::SendMessage(hwndFlashPlayerControl, FPCM_TGOTOLABELW, NULL, (LPARAM)&FPCTGotoLabelW);

    return FPCTGotoLabelW.hr;
}

HRESULT WINAPI FPC_TCurrentFrameA(/* in */ HWND hwndFlashPlayerControl, /* in */ LPCSTR target, /* out */ long* Result)
{
    SFPCTCurrentFrameA FPCTCurrentFrameA; DEF_ZERO_IT(FPCTCurrentFrameA); 

    FPCTCurrentFrameA.hr = E_FAIL;

    if (NULL == Result)
        return E_POINTER;
    FPCTCurrentFrameA.target.lpszBuffer = target;

    ::SendMessage(hwndFlashPlayerControl, FPCM_TCURRENTFRAMEA, NULL, (LPARAM)&FPCTCurrentFrameA);

    if (S_OK == FPCTCurrentFrameA.hr)
        *Result = FPCTCurrentFrameA.Result;

    return FPCTCurrentFrameA.hr;
}

HRESULT WINAPI FPC_TCurrentFrameW(/* in */ HWND hwndFlashPlayerControl, /* in */ LPCWSTR target, /* out */ long* Result)
{
    SFPCTCurrentFrameW FPCTCurrentFrameW; DEF_ZERO_IT(FPCTCurrentFrameW);

    FPCTCurrentFrameW.hr = E_FAIL;

    if (NULL == Result)
        return E_POINTER;
    FPCTCurrentFrameW.target.lpszBuffer = target;

    ::SendMessage(hwndFlashPlayerControl, FPCM_TCURRENTFRAMEW, NULL, (LPARAM)&FPCTCurrentFrameW);

    if (S_OK == FPCTCurrentFrameW.hr)
        *Result = FPCTCurrentFrameW.Result;

    return FPCTCurrentFrameW.hr;
}

HRESULT WINAPI FPC_TCurrentLabelA(/* in */ HWND hwndFlashPlayerControl, /* in */ LPCSTR target, /* out */ LPSTR lpszBuffer, /* in, out */ DWORD* pdwBufferSize)
{
    SFPCTCurrentLabelA FPCTCurrentLabelA; DEF_ZERO_IT(FPCTCurrentLabelA); 

    FPCTCurrentLabelA.hr = E_FAIL;

    if (NULL == pdwBufferSize)
        return E_POINTER;

    FPCTCurrentLabelA.target.lpszBuffer = target;

    FPCTCurrentLabelA.Result.lpszBuffer = lpszBuffer;
    FPCTCurrentLabelA.Result.dwBufferSize = *pdwBufferSize;

    ::SendMessage(hwndFlashPlayerControl, FPCM_TCURRENTLABELA, NULL, (LPARAM)&FPCTCurrentLabelA);

    *pdwBufferSize = FPCTCurrentLabelA.Result.dwBufferSize;
    return FPCTCurrentLabelA.hr;
}

HRESULT WINAPI FPC_TCurrentLabelW(/* in */ HWND hwndFlashPlayerControl, /* in */ LPCWSTR target, /* out */ LPWSTR lpszBuffer, /* in, out */ DWORD* pdwBufferSize)
{
    SFPCTCurrentLabelW FPCTCurrentLabelW; DEF_ZERO_IT(FPCTCurrentLabelW);

    FPCTCurrentLabelW.hr = E_FAIL;

    if (NULL == pdwBufferSize)
        return E_POINTER;

    FPCTCurrentLabelW.target.lpszBuffer = target;

    FPCTCurrentLabelW.Result.lpszBuffer = lpszBuffer;
    FPCTCurrentLabelW.Result.dwBufferSize = *pdwBufferSize;

    ::SendMessage(hwndFlashPlayerControl, FPCM_TCURRENTLABELW, NULL, (LPARAM)&FPCTCurrentLabelW);

    *pdwBufferSize = FPCTCurrentLabelW.Result.dwBufferSize;
    return FPCTCurrentLabelW.hr;
}

HRESULT WINAPI FPC_TPlayA(/* in */ HWND hwndFlashPlayerControl, /* in */ LPCSTR target)
{
    SFPCTPlayA FPCTPlayA; DEF_ZERO_IT(FPCTPlayA); 

    FPCTPlayA.hr = E_FAIL;

    FPCTPlayA.target.lpszBuffer = target;

    ::SendMessage(hwndFlashPlayerControl, FPCM_TPLAYA, NULL, (LPARAM)&FPCTPlayA);

    return FPCTPlayA.hr;
}

HRESULT WINAPI FPC_TPlayW(/* in */ HWND hwndFlashPlayerControl, /* in */ LPCWSTR target)
{
    SFPCTPlayW FPCTPlayW; DEF_ZERO_IT(FPCTPlayW);

    FPCTPlayW.hr = E_FAIL;

    FPCTPlayW.target.lpszBuffer = target;

    ::SendMessage(hwndFlashPlayerControl, FPCM_TPLAYW, NULL, (LPARAM)&FPCTPlayW);

    return FPCTPlayW.hr;
}

HRESULT WINAPI FPC_TStopPlayA(/* in */ HWND hwndFlashPlayerControl, /* in */ LPCSTR target)
{
    SFPCTStopPlayA FPCTStopPlayA; DEF_ZERO_IT(FPCTStopPlayA); 

    FPCTStopPlayA.hr = E_FAIL;

    FPCTStopPlayA.target.lpszBuffer = target;

    ::SendMessage(hwndFlashPlayerControl, FPCM_TSTOPPLAYA, NULL, (LPARAM)&FPCTStopPlayA);

    return FPCTStopPlayA.hr;
}

HRESULT WINAPI FPC_TStopPlayW(/* in */ HWND hwndFlashPlayerControl, /* in */ LPCWSTR target)
{
    SFPCTStopPlayW FPCTStopPlayW; DEF_ZERO_IT(FPCTStopPlayW);

    FPCTStopPlayW.hr = E_FAIL;

    FPCTStopPlayW.target.lpszBuffer = target;

    ::SendMessage(hwndFlashPlayerControl, FPCM_TSTOPPLAYW, NULL, (LPARAM)&FPCTStopPlayW);

    return FPCTStopPlayW.hr;
}

HRESULT WINAPI FPC_SetVariableA(/* in */ HWND hwndFlashPlayerControl, /* in */ LPCSTR name, /* in */ LPCSTR value)
{
    SFPCSetVariableA FPCSetVariableA; DEF_ZERO_IT(FPCSetVariableA); 

    FPCSetVariableA.hr = E_FAIL;

    FPCSetVariableA.name.lpszBuffer = name;
    FPCSetVariableA.value.lpszBuffer = value;

    ::SendMessage(hwndFlashPlayerControl, FPCM_SETVARIABLEA, NULL, (LPARAM)&FPCSetVariableA);

    return FPCSetVariableA.hr;
}

HRESULT WINAPI FPC_SetVariableW(/* in */ HWND hwndFlashPlayerControl, /* in */ LPCWSTR name, /* in */ LPCWSTR value)
{
    SFPCSetVariableW FPCSetVariableW; DEF_ZERO_IT(FPCSetVariableW);

    FPCSetVariableW.hr = E_FAIL;

    FPCSetVariableW.name.lpszBuffer = name;
    FPCSetVariableW.value.lpszBuffer = value;

    ::SendMessage(hwndFlashPlayerControl, FPCM_SETVARIABLEW, NULL, (LPARAM)&FPCSetVariableW);

    return FPCSetVariableW.hr;
}

HRESULT WINAPI FPC_GetVariableA(/* in */ HWND hwndFlashPlayerControl, /* in */ LPCSTR name, /* out */ LPSTR lpszBuffer, /* in, out */ DWORD* pdwBufferSize)
{
    SFPCGetVariableA FPCGetVariableA; DEF_ZERO_IT(FPCGetVariableA); 

    FPCGetVariableA.hr = E_FAIL;

    if (NULL == pdwBufferSize)
        return E_POINTER;

    FPCGetVariableA.name.lpszBuffer = name;

    FPCGetVariableA.Result.lpszBuffer = lpszBuffer;
    FPCGetVariableA.Result.dwBufferSize = *pdwBufferSize;

    ::SendMessage(hwndFlashPlayerControl, FPCM_GETVARIABLEA, NULL, (LPARAM)&FPCGetVariableA);

    *pdwBufferSize = FPCGetVariableA.Result.dwBufferSize;
    return FPCGetVariableA.hr;
}

HRESULT WINAPI FPC_GetVariableW(/* in */ HWND hwndFlashPlayerControl, /* in */ LPCWSTR name, /* out */ LPWSTR lpszBuffer, /* in, out */ DWORD* pdwBufferSize)
{
    SFPCGetVariableW FPCGetVariableW; DEF_ZERO_IT(FPCGetVariableW);

    FPCGetVariableW.hr = E_FAIL;

    if (NULL == pdwBufferSize)
        return E_POINTER;

    FPCGetVariableW.name.lpszBuffer = name;

    FPCGetVariableW.Result.lpszBuffer = lpszBuffer;
    FPCGetVariableW.Result.dwBufferSize = *pdwBufferSize;

    ::SendMessage(hwndFlashPlayerControl, FPCM_GETVARIABLEW, NULL, (LPARAM)&FPCGetVariableW);

    *pdwBufferSize = FPCGetVariableW.Result.dwBufferSize;
    return FPCGetVariableW.hr;
}

HRESULT WINAPI FPC_TSetPropertyA(/* in */ HWND hwndFlashPlayerControl, /* in */ LPCSTR target, /* in */ int property, /* in */ LPCSTR value)
{
    SFPCTSetPropertyA FPCTSetPropertyA; DEF_ZERO_IT(FPCTSetPropertyA); 

    FPCTSetPropertyA.hr = E_FAIL;

    FPCTSetPropertyA.target.lpszBuffer = target;
    FPCTSetPropertyA.property = property;
    FPCTSetPropertyA.value.lpszBuffer = value;

    ::SendMessage(hwndFlashPlayerControl, FPCM_TSETPROPERTYA, NULL, (LPARAM)&FPCTSetPropertyA);

    return FPCTSetPropertyA.hr;
}

HRESULT WINAPI FPC_TSetPropertyW(/* in */ HWND hwndFlashPlayerControl, /* in */ LPCWSTR target, /* in */ int property, /* in */ LPCWSTR value)
{
    SFPCTSetPropertyW FPCTSetPropertyW; DEF_ZERO_IT(FPCTSetPropertyW);

    FPCTSetPropertyW.hr = E_FAIL;

    FPCTSetPropertyW.target.lpszBuffer = target;
    FPCTSetPropertyW.property = property;
    FPCTSetPropertyW.value.lpszBuffer = value;

    ::SendMessage(hwndFlashPlayerControl, FPCM_TSETPROPERTYW, NULL, (LPARAM)&FPCTSetPropertyW);

    return FPCTSetPropertyW.hr;
}

HRESULT WINAPI FPC_TGetPropertyA(/* in */ HWND hwndFlashPlayerControl, /* in */ LPCSTR target, /* in */ int property, /* out */ LPSTR lpszBuffer, /* in, out */ DWORD* pdwBufferSize)
{
    SFPCTGetPropertyA FPCTGetPropertyA; DEF_ZERO_IT(FPCTGetPropertyA); 

    FPCTGetPropertyA.hr = E_FAIL;

    if (NULL == pdwBufferSize)
        return E_POINTER;

    FPCTGetPropertyA.target.lpszBuffer = target;
    FPCTGetPropertyA.property = property;

    FPCTGetPropertyA.Result.lpszBuffer = lpszBuffer;
    FPCTGetPropertyA.Result.dwBufferSize = *pdwBufferSize;

    ::SendMessage(hwndFlashPlayerControl, FPCM_TGETPROPERTYA, NULL, (LPARAM)&FPCTGetPropertyA);

    *pdwBufferSize = FPCTGetPropertyA.Result.dwBufferSize;
    return FPCTGetPropertyA.hr;
}

HRESULT WINAPI FPC_TGetPropertyW(/* in */ HWND hwndFlashPlayerControl, /* in */ LPCWSTR target, /* in */ int property, /* out */ LPWSTR lpszBuffer, /* in, out */ DWORD* pdwBufferSize)
{
    SFPCTGetPropertyW FPCTGetPropertyW; DEF_ZERO_IT(FPCTGetPropertyW);

    FPCTGetPropertyW.hr = E_FAIL;

    if (NULL == pdwBufferSize)
        return E_POINTER;

    FPCTGetPropertyW.target.lpszBuffer = target;
    FPCTGetPropertyW.property = property;

    FPCTGetPropertyW.Result.lpszBuffer = lpszBuffer;
    FPCTGetPropertyW.Result.dwBufferSize = *pdwBufferSize;

    ::SendMessage(hwndFlashPlayerControl, FPCM_TGETPROPERTYW, NULL, (LPARAM)&FPCTGetPropertyW);

    *pdwBufferSize = FPCTGetPropertyW.Result.dwBufferSize;
    return FPCTGetPropertyW.hr;
}

HRESULT WINAPI FPC_TCallFrameA(/* in */ HWND hwndFlashPlayerControl, /* in */ LPCSTR target, /* in */ int FrameNum)
{
    SFPCTCallFrameA FPCTCallFrameA; DEF_ZERO_IT(FPCTCallFrameA); 

    FPCTCallFrameA.hr = E_FAIL;

    FPCTCallFrameA.target.lpszBuffer = target;
    FPCTCallFrameA.FrameNum = FrameNum;

    ::SendMessage(hwndFlashPlayerControl, FPCM_TCALLFRAMEA, NULL, (LPARAM)&FPCTCallFrameA);

    return FPCTCallFrameA.hr;
}

HRESULT WINAPI FPC_TCallFrameW(/* in */ HWND hwndFlashPlayerControl, /* in */ LPCWSTR target, /* in */ int FrameNum)
{
    SFPCTCallFrameW FPCTCallFrameW; DEF_ZERO_IT(FPCTCallFrameW);

    FPCTCallFrameW.hr = E_FAIL;

    FPCTCallFrameW.target.lpszBuffer = target;
    FPCTCallFrameW.FrameNum = FrameNum;

    ::SendMessage(hwndFlashPlayerControl, FPCM_TCALLFRAMEW, NULL, (LPARAM)&FPCTCallFrameW);

    return FPCTCallFrameW.hr;
}

HRESULT WINAPI FPC_TCallLabelA(/* in */ HWND hwndFlashPlayerControl, /* in */ LPCSTR target, /* in */ LPCSTR label)
{
    SFPCTCallLabelA FPCTCallLabelA; DEF_ZERO_IT(FPCTCallLabelA); 

    FPCTCallLabelA.hr = E_FAIL;

    FPCTCallLabelA.target.lpszBuffer = target;
    FPCTCallLabelA.label.lpszBuffer = label;

    ::SendMessage(hwndFlashPlayerControl, FPCM_TCALLLABELA, NULL, (LPARAM)&FPCTCallLabelA);

    return FPCTCallLabelA.hr;
}

HRESULT WINAPI FPC_TCallLabelW(/* in */ HWND hwndFlashPlayerControl, /* in */ LPCWSTR target, /* in */ LPCWSTR label)
{
    SFPCTCallLabelW FPCTCallLabelW; DEF_ZERO_IT(FPCTCallLabelW);

    FPCTCallLabelW.hr = E_FAIL;

    FPCTCallLabelW.target.lpszBuffer = target;
    FPCTCallLabelW.label.lpszBuffer = label;

    ::SendMessage(hwndFlashPlayerControl, FPCM_TCALLLABELW, NULL, (LPARAM)&FPCTCallLabelW);

    return FPCTCallLabelW.hr;
}

HRESULT WINAPI FPC_TSetPropertyNumA(/* in */ HWND hwndFlashPlayerControl, /* in */ LPCSTR target, /* in */ int property, /* in */ double value)
{
    SFPCTSetPropertyNumA FPCTSetPropertyNumA; DEF_ZERO_IT(FPCTSetPropertyNumA); 

    FPCTSetPropertyNumA.hr = E_FAIL;

    FPCTSetPropertyNumA.target.lpszBuffer = target;
    FPCTSetPropertyNumA.property = property;
    FPCTSetPropertyNumA.value = value;

    ::SendMessage(hwndFlashPlayerControl, FPCM_TSETPROPERTYNUMA, NULL, (LPARAM)&FPCTSetPropertyNumA);

    return FPCTSetPropertyNumA.hr;
}

HRESULT WINAPI FPC_TSetPropertyNumW(/* in */ HWND hwndFlashPlayerControl, /* in */ LPCWSTR target, /* in */ int property, /* in */ double value)
{
    SFPCTSetPropertyNumW FPCTSetPropertyNumW; DEF_ZERO_IT(FPCTSetPropertyNumW);

    FPCTSetPropertyNumW.hr = E_FAIL;

    FPCTSetPropertyNumW.target.lpszBuffer = target;
    FPCTSetPropertyNumW.property = property;
    FPCTSetPropertyNumW.value = value;

    ::SendMessage(hwndFlashPlayerControl, FPCM_TSETPROPERTYNUMW, NULL, (LPARAM)&FPCTSetPropertyNumW);

    return FPCTSetPropertyNumW.hr;
}

HRESULT WINAPI FPC_TGetPropertyNumA(/* in */ HWND hwndFlashPlayerControl, /* in */ LPCSTR target, /* in */ int property, /* out */ double* Result)
{
    SFPCTGetPropertyNumA FPCTGetPropertyNumA; DEF_ZERO_IT(FPCTGetPropertyNumA); 

    FPCTGetPropertyNumA.hr = E_FAIL;

    if (NULL == Result)
        return E_POINTER;
    FPCTGetPropertyNumA.target.lpszBuffer = target;
    FPCTGetPropertyNumA.property = property;

    ::SendMessage(hwndFlashPlayerControl, FPCM_TGETPROPERTYNUMA, NULL, (LPARAM)&FPCTGetPropertyNumA);

    if (S_OK == FPCTGetPropertyNumA.hr)
        *Result = FPCTGetPropertyNumA.Result;

    return FPCTGetPropertyNumA.hr;
}

HRESULT WINAPI FPC_TGetPropertyNumW(/* in */ HWND hwndFlashPlayerControl, /* in */ LPCWSTR target, /* in */ int property, /* out */ double* Result)
{
    SFPCTGetPropertyNumW FPCTGetPropertyNumW; DEF_ZERO_IT(FPCTGetPropertyNumW);

    FPCTGetPropertyNumW.hr = E_FAIL;

    if (NULL == Result)
        return E_POINTER;
    FPCTGetPropertyNumW.target.lpszBuffer = target;
    FPCTGetPropertyNumW.property = property;

    ::SendMessage(hwndFlashPlayerControl, FPCM_TGETPROPERTYNUMW, NULL, (LPARAM)&FPCTGetPropertyNumW);

    if (S_OK == FPCTGetPropertyNumW.hr)
        *Result = FPCTGetPropertyNumW.Result;

    return FPCTGetPropertyNumW.hr;
}

HRESULT WINAPI FPC_TGetPropertyAsNumberA(/* in */ HWND hwndFlashPlayerControl, /* in */ LPCSTR target, /* in */ int property, /* out */ double* Result)
{
    SFPCTGetPropertyAsNumberA FPCTGetPropertyAsNumberA; DEF_ZERO_IT(FPCTGetPropertyAsNumberA); 

    FPCTGetPropertyAsNumberA.hr = E_FAIL;

    if (NULL == Result)
        return E_POINTER;
    FPCTGetPropertyAsNumberA.target.lpszBuffer = target;
    FPCTGetPropertyAsNumberA.property = property;

    ::SendMessage(hwndFlashPlayerControl, FPCM_TGETPROPERTYASNUMBERA, NULL, (LPARAM)&FPCTGetPropertyAsNumberA);

    if (S_OK == FPCTGetPropertyAsNumberA.hr)
        *Result = FPCTGetPropertyAsNumberA.Result;

    return FPCTGetPropertyAsNumberA.hr;
}

HRESULT WINAPI FPC_TGetPropertyAsNumberW(/* in */ HWND hwndFlashPlayerControl, /* in */ LPCWSTR target, /* in */ int property, /* out */ double* Result)
{
    SFPCTGetPropertyAsNumberW FPCTGetPropertyAsNumberW; DEF_ZERO_IT(FPCTGetPropertyAsNumberW);

    FPCTGetPropertyAsNumberW.hr = E_FAIL;

    if (NULL == Result)
        return E_POINTER;
    FPCTGetPropertyAsNumberW.target.lpszBuffer = target;
    FPCTGetPropertyAsNumberW.property = property;

    ::SendMessage(hwndFlashPlayerControl, FPCM_TGETPROPERTYASNUMBERW, NULL, (LPARAM)&FPCTGetPropertyAsNumberW);

    if (S_OK == FPCTGetPropertyAsNumberW.hr)
        *Result = FPCTGetPropertyAsNumberW.Result;

    return FPCTGetPropertyAsNumberW.hr;
}

HRESULT WINAPI FPC_GetReadyState(/* in */ HWND hwndFlashPlayerControl, long* pReadyState)
{
    if (NULL == pReadyState)
        return E_POINTER;

    SFPCGetReadyState FPCGetReadyState;

    FPCGetReadyState.hr = E_FAIL;

    ::SendMessage(hwndFlashPlayerControl, FPCM_GET_READYSTATE, 0, (LPARAM)&FPCGetReadyState);

    if (S_OK == FPCGetReadyState.hr)
        *pReadyState = FPCGetReadyState.ReadyState;

    return FPCGetReadyState.hr;
}

HRESULT WINAPI FPC_GetTotalFrames(/* in */ HWND hwndFlashPlayerControl, long* pTotalFrames)
{
    if (NULL == pTotalFrames)
        return E_POINTER;

    SFPCGetTotalFrames FPCGetTotalFrames;

    FPCGetTotalFrames.hr = E_FAIL;

    ::SendMessage(hwndFlashPlayerControl, FPCM_GET_TOTALFRAMES, 0, (LPARAM)&FPCGetTotalFrames);

    if (S_OK == FPCGetTotalFrames.hr)
        *pTotalFrames = FPCGetTotalFrames.TotalFrames;

    return FPCGetTotalFrames.hr;
}

HRESULT WINAPI FPC_PutPlaying(/* in */ HWND hwndFlashPlayerControl, VARIANT_BOOL Playing)
{
    SFPCPutPlaying FPCPutPlaying = { Playing, E_FAIL };

    ::SendMessage(hwndFlashPlayerControl, FPCM_PUT_PLAYING, 0, (LPARAM)&FPCPutPlaying);

    return FPCPutPlaying.hr;
}

HRESULT WINAPI FPC_GetPlaying(/* in */ HWND hwndFlashPlayerControl, VARIANT_BOOL* pPlaying)
{
    if (NULL == pPlaying)
        return E_POINTER;

    SFPCGetPlaying FPCGetPlaying;

    FPCGetPlaying.hr = E_FAIL;

    ::SendMessage(hwndFlashPlayerControl, FPCM_GET_PLAYING, 0, (LPARAM)&FPCGetPlaying);

    if (S_OK == FPCGetPlaying.hr)
        *pPlaying = FPCGetPlaying.Playing;

    return FPCGetPlaying.hr;
}

HRESULT WINAPI FPC_PutQuality(/* in */ HWND hwndFlashPlayerControl, int Quality)
{
    SFPCPutQuality FPCPutQuality = { Quality, E_FAIL };

    ::SendMessage(hwndFlashPlayerControl, FPCM_PUT_QUALITY, 0, (LPARAM)&FPCPutQuality);

    return FPCPutQuality.hr;
}

HRESULT WINAPI FPC_GetQuality(/* in */ HWND hwndFlashPlayerControl, int* pQuality)
{
    if (NULL == pQuality)
        return E_POINTER;

    SFPCGetQuality FPCGetQuality;

    FPCGetQuality.hr = E_FAIL;

    ::SendMessage(hwndFlashPlayerControl, FPCM_GET_QUALITY, 0, (LPARAM)&FPCGetQuality);

    if (S_OK == FPCGetQuality.hr)
        *pQuality = FPCGetQuality.Quality;

    return FPCGetQuality.hr;
}

HRESULT WINAPI FPC_PutScaleMode(/* in */ HWND hwndFlashPlayerControl, int ScaleMode)
{
    SFPCPutScaleMode FPCPutScaleMode = { ScaleMode, E_FAIL };

    ::SendMessage(hwndFlashPlayerControl, FPCM_PUT_SCALEMODE, 0, (LPARAM)&FPCPutScaleMode);

    return FPCPutScaleMode.hr;
}

HRESULT WINAPI FPC_GetScaleMode(/* in */ HWND hwndFlashPlayerControl, int* pScaleMode)
{
    if (NULL == pScaleMode)
        return E_POINTER;

    SFPCGetScaleMode FPCGetScaleMode;

    FPCGetScaleMode.hr = E_FAIL;

    ::SendMessage(hwndFlashPlayerControl, FPCM_GET_SCALEMODE, 0, (LPARAM)&FPCGetScaleMode);

    if (S_OK == FPCGetScaleMode.hr)
        *pScaleMode = FPCGetScaleMode.ScaleMode;

    return FPCGetScaleMode.hr;
}

HRESULT WINAPI FPC_PutAlignMode(/* in */ HWND hwndFlashPlayerControl, int AlignMode)
{
    SFPCPutAlignMode FPCPutAlignMode = { AlignMode, E_FAIL };

    ::SendMessage(hwndFlashPlayerControl, FPCM_PUT_ALIGNMODE, 0, (LPARAM)&FPCPutAlignMode);

    return FPCPutAlignMode.hr;
}

HRESULT WINAPI FPC_GetAlignMode(/* in */ HWND hwndFlashPlayerControl, int* pAlignMode)
{
    if (NULL == pAlignMode)
        return E_POINTER;

    SFPCGetAlignMode FPCGetAlignMode;

    FPCGetAlignMode.hr = E_FAIL;

    ::SendMessage(hwndFlashPlayerControl, FPCM_GET_ALIGNMODE, 0, (LPARAM)&FPCGetAlignMode);

    if (S_OK == FPCGetAlignMode.hr)
        *pAlignMode = FPCGetAlignMode.AlignMode;

    return FPCGetAlignMode.hr;
}

HRESULT WINAPI FPC_PutBackgroundColor(/* in */ HWND hwndFlashPlayerControl, long BackgroundColor)
{
    SFPCPutBackgroundColor FPCPutBackgroundColor = { BackgroundColor, E_FAIL };

    ::SendMessage(hwndFlashPlayerControl, FPCM_PUT_BACKGROUNDCOLOR, 0, (LPARAM)&FPCPutBackgroundColor);

    return FPCPutBackgroundColor.hr;
}

HRESULT WINAPI FPC_GetBackgroundColor(/* in */ HWND hwndFlashPlayerControl, long* pBackgroundColor)
{
    if (NULL == pBackgroundColor)
        return E_POINTER;

    SFPCGetBackgroundColor FPCGetBackgroundColor;

    FPCGetBackgroundColor.hr = E_FAIL;

    ::SendMessage(hwndFlashPlayerControl, FPCM_GET_BACKGROUNDCOLOR, 0, (LPARAM)&FPCGetBackgroundColor);

    if (S_OK == FPCGetBackgroundColor.hr)
        *pBackgroundColor = FPCGetBackgroundColor.BackgroundColor;

    return FPCGetBackgroundColor.hr;
}

HRESULT WINAPI FPC_PutLoop(/* in */ HWND hwndFlashPlayerControl, VARIANT_BOOL Loop)
{
    SFPCPutLoop FPCPutLoop = { Loop, E_FAIL };

    ::SendMessage(hwndFlashPlayerControl, FPCM_PUT_LOOP, 0, (LPARAM)&FPCPutLoop);

    return FPCPutLoop.hr;
}

HRESULT WINAPI FPC_GetLoop(/* in */ HWND hwndFlashPlayerControl, VARIANT_BOOL* pLoop)
{
    if (NULL == pLoop)
        return E_POINTER;

    SFPCGetLoop FPCGetLoop;

    FPCGetLoop.hr = E_FAIL;

    ::SendMessage(hwndFlashPlayerControl, FPCM_GET_LOOP, 0, (LPARAM)&FPCGetLoop);

    if (S_OK == FPCGetLoop.hr)
        *pLoop = FPCGetLoop.Loop;

    return FPCGetLoop.hr;
}

HRESULT WINAPI FPC_PutMovieA(/* in */ HWND hwndFlashPlayerControl, LPCSTR Movie)
{
    SFPCPutMovieA FPCPutMovieA = { Movie, E_FAIL };

    ::SendMessage(hwndFlashPlayerControl, FPCM_PUT_MOVIEA, 0, (LPARAM)&FPCPutMovieA);

    return FPCPutMovieA.hr;
}

HRESULT WINAPI FPC_PutMovieW(/* in */ HWND hwndFlashPlayerControl, LPCWSTR Movie)
{
    SFPCPutMovieW FPCPutMovieW = { Movie, E_FAIL };

    ::SendMessage(hwndFlashPlayerControl, FPCM_PUT_MOVIEW, 0, (LPARAM)&FPCPutMovieW);

    return FPCPutMovieW.hr;
}

HRESULT WINAPI FPC_GetMovieA(/* in */ HWND hwndFlashPlayerControl, LPSTR lpszBuffer, DWORD* pdwBufferSize)
{
    if (NULL == pdwBufferSize)
        return E_POINTER;

    SFPCGetMovieA FPCGetMovieA = { lpszBuffer, *pdwBufferSize, E_FAIL };

    ::SendMessage(hwndFlashPlayerControl, FPCM_GET_MOVIEA, 0, (LPARAM)&FPCGetMovieA);

    *pdwBufferSize = FPCGetMovieA.dwBufferSize;

    return FPCGetMovieA.hr;
}

HRESULT WINAPI FPC_GetMovieW(/* in */ HWND hwndFlashPlayerControl, LPWSTR lpszBuffer, DWORD* pdwBufferSize)
{
    if (NULL == pdwBufferSize)
        return E_POINTER;

    SFPCGetMovieW FPCGetMovieW = { lpszBuffer, *pdwBufferSize, E_FAIL };

    ::SendMessage(hwndFlashPlayerControl, FPCM_GET_MOVIEW, 0, (LPARAM)&FPCGetMovieW);

    *pdwBufferSize = FPCGetMovieW.dwBufferSize;

    return FPCGetMovieW.hr;
}

HRESULT WINAPI FPC_PutFrameNum(/* in */ HWND hwndFlashPlayerControl, long FrameNum)
{
    SFPCPutFrameNum FPCPutFrameNum = { FrameNum, E_FAIL };

    ::SendMessage(hwndFlashPlayerControl, FPCM_PUT_FRAMENUM, 0, (LPARAM)&FPCPutFrameNum);

    return FPCPutFrameNum.hr;
}

HRESULT WINAPI FPC_GetFrameNum(/* in */ HWND hwndFlashPlayerControl, long* pFrameNum)
{
    if (NULL == pFrameNum)
        return E_POINTER;

    SFPCGetFrameNum FPCGetFrameNum;

    FPCGetFrameNum.hr = E_FAIL;

    ::SendMessage(hwndFlashPlayerControl, FPCM_GET_FRAMENUM, 0, (LPARAM)&FPCGetFrameNum);

    if (S_OK == FPCGetFrameNum.hr)
        *pFrameNum = FPCGetFrameNum.FrameNum;

    return FPCGetFrameNum.hr;
}

HRESULT WINAPI FPC_PutWModeA(/* in */ HWND hwndFlashPlayerControl, LPCSTR WMode)
{
    SFPCPutWModeA FPCPutWModeA = { WMode, E_FAIL };

    ::SendMessage(hwndFlashPlayerControl, FPCM_PUT_WMODEA, 0, (LPARAM)&FPCPutWModeA);

    return FPCPutWModeA.hr;
}

HRESULT WINAPI FPC_PutWModeW(/* in */ HWND hwndFlashPlayerControl, LPCWSTR WMode)
{
    SFPCPutWModeW FPCPutWModeW = { WMode, E_FAIL };

    ::SendMessage(hwndFlashPlayerControl, FPCM_PUT_WMODEW, 0, (LPARAM)&FPCPutWModeW);

    return FPCPutWModeW.hr;
}

HRESULT WINAPI FPC_GetWModeA(/* in */ HWND hwndFlashPlayerControl, LPSTR lpszBuffer, DWORD* pdwBufferSize)
{
    if (NULL == pdwBufferSize)
        return E_POINTER;

    SFPCGetWModeA FPCGetWModeA = { lpszBuffer, *pdwBufferSize, E_FAIL };

    ::SendMessage(hwndFlashPlayerControl, FPCM_GET_WMODEA, 0, (LPARAM)&FPCGetWModeA);

    *pdwBufferSize = FPCGetWModeA.dwBufferSize;

    return FPCGetWModeA.hr;
}

HRESULT WINAPI FPC_GetWModeW(/* in */ HWND hwndFlashPlayerControl, LPWSTR lpszBuffer, DWORD* pdwBufferSize)
{
    if (NULL == pdwBufferSize)
        return E_POINTER;

    SFPCGetWModeW FPCGetWModeW = { lpszBuffer, *pdwBufferSize, E_FAIL };

    ::SendMessage(hwndFlashPlayerControl, FPCM_GET_WMODEW, 0, (LPARAM)&FPCGetWModeW);

    *pdwBufferSize = FPCGetWModeW.dwBufferSize;

    return FPCGetWModeW.hr;
}

HRESULT WINAPI FPC_PutSAlignA(/* in */ HWND hwndFlashPlayerControl, LPCSTR SAlign)
{
    SFPCPutSAlignA FPCPutSAlignA = { SAlign, E_FAIL };

    ::SendMessage(hwndFlashPlayerControl, FPCM_PUT_SALIGNA, 0, (LPARAM)&FPCPutSAlignA);

    return FPCPutSAlignA.hr;
}

HRESULT WINAPI FPC_PutSAlignW(/* in */ HWND hwndFlashPlayerControl, LPCWSTR SAlign)
{
    SFPCPutSAlignW FPCPutSAlignW = { SAlign, E_FAIL };

    ::SendMessage(hwndFlashPlayerControl, FPCM_PUT_SALIGNW, 0, (LPARAM)&FPCPutSAlignW);

    return FPCPutSAlignW.hr;
}

HRESULT WINAPI FPC_GetSAlignA(/* in */ HWND hwndFlashPlayerControl, LPSTR lpszBuffer, DWORD* pdwBufferSize)
{
    if (NULL == pdwBufferSize)
        return E_POINTER;

    SFPCGetSAlignA FPCGetSAlignA = { lpszBuffer, *pdwBufferSize, E_FAIL };

    ::SendMessage(hwndFlashPlayerControl, FPCM_GET_SALIGNA, 0, (LPARAM)&FPCGetSAlignA);

    *pdwBufferSize = FPCGetSAlignA.dwBufferSize;

    return FPCGetSAlignA.hr;
}

HRESULT WINAPI FPC_GetSAlignW(/* in */ HWND hwndFlashPlayerControl, LPWSTR lpszBuffer, DWORD* pdwBufferSize)
{
    if (NULL == pdwBufferSize)
        return E_POINTER;

    SFPCGetSAlignW FPCGetSAlignW = { lpszBuffer, *pdwBufferSize, E_FAIL };

    ::SendMessage(hwndFlashPlayerControl, FPCM_GET_SALIGNW, 0, (LPARAM)&FPCGetSAlignW);

    *pdwBufferSize = FPCGetSAlignW.dwBufferSize;

    return FPCGetSAlignW.hr;
}

HRESULT WINAPI FPC_PutMenu(/* in */ HWND hwndFlashPlayerControl, VARIANT_BOOL Menu)
{
    SFPCPutMenu FPCPutMenu = { Menu, E_FAIL };

    ::SendMessage(hwndFlashPlayerControl, FPCM_PUT_MENU, 0, (LPARAM)&FPCPutMenu);

    return FPCPutMenu.hr;
}

HRESULT WINAPI FPC_GetMenu(/* in */ HWND hwndFlashPlayerControl, VARIANT_BOOL* pMenu)
{
    if (NULL == pMenu)
        return E_POINTER;

    SFPCGetMenu FPCGetMenu;

    FPCGetMenu.hr = E_FAIL;

    ::SendMessage(hwndFlashPlayerControl, FPCM_GET_MENU, 0, (LPARAM)&FPCGetMenu);

    if (S_OK == FPCGetMenu.hr)
        *pMenu = FPCGetMenu.Menu;

    return FPCGetMenu.hr;
}

HRESULT WINAPI FPC_PutBaseA(/* in */ HWND hwndFlashPlayerControl, LPCSTR Base)
{
    SFPCPutBaseA FPCPutBaseA = { Base, E_FAIL };

    ::SendMessage(hwndFlashPlayerControl, FPCM_PUT_BASEA, 0, (LPARAM)&FPCPutBaseA);

    return FPCPutBaseA.hr;
}

HRESULT WINAPI FPC_PutBaseW(/* in */ HWND hwndFlashPlayerControl, LPCWSTR Base)
{
    SFPCPutBaseW FPCPutBaseW = { Base, E_FAIL };

    ::SendMessage(hwndFlashPlayerControl, FPCM_PUT_BASEW, 0, (LPARAM)&FPCPutBaseW);

    return FPCPutBaseW.hr;
}

HRESULT WINAPI FPC_GetBaseA(/* in */ HWND hwndFlashPlayerControl, LPSTR lpszBuffer, DWORD* pdwBufferSize)
{
    if (NULL == pdwBufferSize)
        return E_POINTER;

    SFPCGetBaseA FPCGetBaseA = { lpszBuffer, *pdwBufferSize, E_FAIL };

    ::SendMessage(hwndFlashPlayerControl, FPCM_GET_BASEA, 0, (LPARAM)&FPCGetBaseA);

    *pdwBufferSize = FPCGetBaseA.dwBufferSize;

    return FPCGetBaseA.hr;
}

HRESULT WINAPI FPC_GetBaseW(/* in */ HWND hwndFlashPlayerControl, LPWSTR lpszBuffer, DWORD* pdwBufferSize)
{
    if (NULL == pdwBufferSize)
        return E_POINTER;

    SFPCGetBaseW FPCGetBaseW = { lpszBuffer, *pdwBufferSize, E_FAIL };

    ::SendMessage(hwndFlashPlayerControl, FPCM_GET_BASEW, 0, (LPARAM)&FPCGetBaseW);

    *pdwBufferSize = FPCGetBaseW.dwBufferSize;

    return FPCGetBaseW.hr;
}

HRESULT WINAPI FPC_PutScaleA(/* in */ HWND hwndFlashPlayerControl, LPCSTR Scale)
{
    SFPCPutScaleA FPCPutScaleA = { Scale, E_FAIL };

    ::SendMessage(hwndFlashPlayerControl, FPCM_PUT_SCALEA, 0, (LPARAM)&FPCPutScaleA);

    return FPCPutScaleA.hr;
}

HRESULT WINAPI FPC_PutScaleW(/* in */ HWND hwndFlashPlayerControl, LPCWSTR Scale)
{
    SFPCPutScaleW FPCPutScaleW = { Scale, E_FAIL };

    ::SendMessage(hwndFlashPlayerControl, FPCM_PUT_SCALEW, 0, (LPARAM)&FPCPutScaleW);

    return FPCPutScaleW.hr;
}

HRESULT WINAPI FPC_GetScaleA(/* in */ HWND hwndFlashPlayerControl, LPSTR lpszBuffer, DWORD* pdwBufferSize)
{
    if (NULL == pdwBufferSize)
        return E_POINTER;

    SFPCGetScaleA FPCGetScaleA = { lpszBuffer, *pdwBufferSize, E_FAIL };

    ::SendMessage(hwndFlashPlayerControl, FPCM_GET_SCALEA, 0, (LPARAM)&FPCGetScaleA);

    *pdwBufferSize = FPCGetScaleA.dwBufferSize;

    return FPCGetScaleA.hr;
}

HRESULT WINAPI FPC_GetScaleW(/* in */ HWND hwndFlashPlayerControl, LPWSTR lpszBuffer, DWORD* pdwBufferSize)
{
    if (NULL == pdwBufferSize)
        return E_POINTER;

    SFPCGetScaleW FPCGetScaleW = { lpszBuffer, *pdwBufferSize, E_FAIL };

    ::SendMessage(hwndFlashPlayerControl, FPCM_GET_SCALEW, 0, (LPARAM)&FPCGetScaleW);

    *pdwBufferSize = FPCGetScaleW.dwBufferSize;

    return FPCGetScaleW.hr;
}

HRESULT WINAPI FPC_PutDeviceFont(/* in */ HWND hwndFlashPlayerControl, VARIANT_BOOL DeviceFont)
{
    SFPCPutDeviceFont FPCPutDeviceFont = { DeviceFont, E_FAIL };

    ::SendMessage(hwndFlashPlayerControl, FPCM_PUT_DEVICEFONT, 0, (LPARAM)&FPCPutDeviceFont);

    return FPCPutDeviceFont.hr;
}

HRESULT WINAPI FPC_GetDeviceFont(/* in */ HWND hwndFlashPlayerControl, VARIANT_BOOL* pDeviceFont)
{
    if (NULL == pDeviceFont)
        return E_POINTER;

    SFPCGetDeviceFont FPCGetDeviceFont;

    FPCGetDeviceFont.hr = E_FAIL;

    ::SendMessage(hwndFlashPlayerControl, FPCM_GET_DEVICEFONT, 0, (LPARAM)&FPCGetDeviceFont);

    if (S_OK == FPCGetDeviceFont.hr)
        *pDeviceFont = FPCGetDeviceFont.DeviceFont;

    return FPCGetDeviceFont.hr;
}

HRESULT WINAPI FPC_PutEmbedMovie(/* in */ HWND hwndFlashPlayerControl, VARIANT_BOOL EmbedMovie)
{
    SFPCPutEmbedMovie FPCPutEmbedMovie = { EmbedMovie, E_FAIL };

    ::SendMessage(hwndFlashPlayerControl, FPCM_PUT_EMBEDMOVIE, 0, (LPARAM)&FPCPutEmbedMovie);

    return FPCPutEmbedMovie.hr;
}

HRESULT WINAPI FPC_GetEmbedMovie(/* in */ HWND hwndFlashPlayerControl, VARIANT_BOOL* pEmbedMovie)
{
    if (NULL == pEmbedMovie)
        return E_POINTER;

    SFPCGetEmbedMovie FPCGetEmbedMovie;

    FPCGetEmbedMovie.hr = E_FAIL;

    ::SendMessage(hwndFlashPlayerControl, FPCM_GET_EMBEDMOVIE, 0, (LPARAM)&FPCGetEmbedMovie);

    if (S_OK == FPCGetEmbedMovie.hr)
        *pEmbedMovie = FPCGetEmbedMovie.EmbedMovie;

    return FPCGetEmbedMovie.hr;
}

HRESULT WINAPI FPC_PutBGColorA(/* in */ HWND hwndFlashPlayerControl, LPCSTR BGColor)
{
    SFPCPutBGColorA FPCPutBGColorA = { BGColor, E_FAIL };

    ::SendMessage(hwndFlashPlayerControl, FPCM_PUT_BGCOLORA, 0, (LPARAM)&FPCPutBGColorA);

    return FPCPutBGColorA.hr;
}

HRESULT WINAPI FPC_PutBGColorW(/* in */ HWND hwndFlashPlayerControl, LPCWSTR BGColor)
{
    SFPCPutBGColorW FPCPutBGColorW = { BGColor, E_FAIL };

    ::SendMessage(hwndFlashPlayerControl, FPCM_PUT_BGCOLORW, 0, (LPARAM)&FPCPutBGColorW);

    return FPCPutBGColorW.hr;
}

HRESULT WINAPI FPC_GetBGColorA(/* in */ HWND hwndFlashPlayerControl, LPSTR lpszBuffer, DWORD* pdwBufferSize)
{
    if (NULL == pdwBufferSize)
        return E_POINTER;

    SFPCGetBGColorA FPCGetBGColorA = { lpszBuffer, *pdwBufferSize, E_FAIL };

    ::SendMessage(hwndFlashPlayerControl, FPCM_GET_BGCOLORA, 0, (LPARAM)&FPCGetBGColorA);

    *pdwBufferSize = FPCGetBGColorA.dwBufferSize;

    return FPCGetBGColorA.hr;
}

HRESULT WINAPI FPC_GetBGColorW(/* in */ HWND hwndFlashPlayerControl, LPWSTR lpszBuffer, DWORD* pdwBufferSize)
{
    if (NULL == pdwBufferSize)
        return E_POINTER;

    SFPCGetBGColorW FPCGetBGColorW = { lpszBuffer, *pdwBufferSize, E_FAIL };

    ::SendMessage(hwndFlashPlayerControl, FPCM_GET_BGCOLORW, 0, (LPARAM)&FPCGetBGColorW);

    *pdwBufferSize = FPCGetBGColorW.dwBufferSize;

    return FPCGetBGColorW.hr;
}

HRESULT WINAPI FPC_PutQuality2A(/* in */ HWND hwndFlashPlayerControl, LPCSTR Quality2)
{
    SFPCPutQuality2A FPCPutQuality2A = { Quality2, E_FAIL };

    ::SendMessage(hwndFlashPlayerControl, FPCM_PUT_QUALITY2A, 0, (LPARAM)&FPCPutQuality2A);

    return FPCPutQuality2A.hr;
}

HRESULT WINAPI FPC_PutQuality2W(/* in */ HWND hwndFlashPlayerControl, LPCWSTR Quality2)
{
    SFPCPutQuality2W FPCPutQuality2W = { Quality2, E_FAIL };

    ::SendMessage(hwndFlashPlayerControl, FPCM_PUT_QUALITY2W, 0, (LPARAM)&FPCPutQuality2W);

    return FPCPutQuality2W.hr;
}

HRESULT WINAPI FPC_GetQuality2A(/* in */ HWND hwndFlashPlayerControl, LPSTR lpszBuffer, DWORD* pdwBufferSize)
{
    if (NULL == pdwBufferSize)
        return E_POINTER;

    SFPCGetQuality2A FPCGetQuality2A = { lpszBuffer, *pdwBufferSize, E_FAIL };

    ::SendMessage(hwndFlashPlayerControl, FPCM_GET_QUALITY2A, 0, (LPARAM)&FPCGetQuality2A);

    *pdwBufferSize = FPCGetQuality2A.dwBufferSize;

    return FPCGetQuality2A.hr;
}

HRESULT WINAPI FPC_GetQuality2W(/* in */ HWND hwndFlashPlayerControl, LPWSTR lpszBuffer, DWORD* pdwBufferSize)
{
    if (NULL == pdwBufferSize)
        return E_POINTER;

    SFPCGetQuality2W FPCGetQuality2W = { lpszBuffer, *pdwBufferSize, E_FAIL };

    ::SendMessage(hwndFlashPlayerControl, FPCM_GET_QUALITY2W, 0, (LPARAM)&FPCGetQuality2W);

    *pdwBufferSize = FPCGetQuality2W.dwBufferSize;

    return FPCGetQuality2W.hr;
}

HRESULT WINAPI FPC_PutSWRemoteA(/* in */ HWND hwndFlashPlayerControl, LPCSTR SWRemote)
{
    SFPCPutSWRemoteA FPCPutSWRemoteA = { SWRemote, E_FAIL };

    ::SendMessage(hwndFlashPlayerControl, FPCM_PUT_SWREMOTEA, 0, (LPARAM)&FPCPutSWRemoteA);

    return FPCPutSWRemoteA.hr;
}

HRESULT WINAPI FPC_PutSWRemoteW(/* in */ HWND hwndFlashPlayerControl, LPCWSTR SWRemote)
{
    SFPCPutSWRemoteW FPCPutSWRemoteW = { SWRemote, E_FAIL };

    ::SendMessage(hwndFlashPlayerControl, FPCM_PUT_SWREMOTEW, 0, (LPARAM)&FPCPutSWRemoteW);

    return FPCPutSWRemoteW.hr;
}

HRESULT WINAPI FPC_GetSWRemoteA(/* in */ HWND hwndFlashPlayerControl, LPSTR lpszBuffer, DWORD* pdwBufferSize)
{
    if (NULL == pdwBufferSize)
        return E_POINTER;

    SFPCGetSWRemoteA FPCGetSWRemoteA = { lpszBuffer, *pdwBufferSize, E_FAIL };

    ::SendMessage(hwndFlashPlayerControl, FPCM_GET_SWREMOTEA, 0, (LPARAM)&FPCGetSWRemoteA);

    *pdwBufferSize = FPCGetSWRemoteA.dwBufferSize;

    return FPCGetSWRemoteA.hr;
}

HRESULT WINAPI FPC_GetSWRemoteW(/* in */ HWND hwndFlashPlayerControl, LPWSTR lpszBuffer, DWORD* pdwBufferSize)
{
    if (NULL == pdwBufferSize)
        return E_POINTER;

    SFPCGetSWRemoteW FPCGetSWRemoteW = { lpszBuffer, *pdwBufferSize, E_FAIL };

    ::SendMessage(hwndFlashPlayerControl, FPCM_GET_SWREMOTEW, 0, (LPARAM)&FPCGetSWRemoteW);

    *pdwBufferSize = FPCGetSWRemoteW.dwBufferSize;

    return FPCGetSWRemoteW.hr;
}

HRESULT WINAPI FPC_PutStackingA(/* in */ HWND hwndFlashPlayerControl, LPCSTR Stacking)
{
    SFPCPutStackingA FPCPutStackingA = { Stacking, E_FAIL };

    ::SendMessage(hwndFlashPlayerControl, FPCM_PUT_STACKINGA, 0, (LPARAM)&FPCPutStackingA);

    return FPCPutStackingA.hr;
}

HRESULT WINAPI FPC_PutStackingW(/* in */ HWND hwndFlashPlayerControl, LPCWSTR Stacking)
{
    SFPCPutStackingW FPCPutStackingW = { Stacking, E_FAIL };

    ::SendMessage(hwndFlashPlayerControl, FPCM_PUT_STACKINGW, 0, (LPARAM)&FPCPutStackingW);

    return FPCPutStackingW.hr;
}

HRESULT WINAPI FPC_GetStackingA(/* in */ HWND hwndFlashPlayerControl, LPSTR lpszBuffer, DWORD* pdwBufferSize)
{
    if (NULL == pdwBufferSize)
        return E_POINTER;

    SFPCGetStackingA FPCGetStackingA = { lpszBuffer, *pdwBufferSize, E_FAIL };

    ::SendMessage(hwndFlashPlayerControl, FPCM_GET_STACKINGA, 0, (LPARAM)&FPCGetStackingA);

    *pdwBufferSize = FPCGetStackingA.dwBufferSize;

    return FPCGetStackingA.hr;
}

HRESULT WINAPI FPC_GetStackingW(/* in */ HWND hwndFlashPlayerControl, LPWSTR lpszBuffer, DWORD* pdwBufferSize)
{
    if (NULL == pdwBufferSize)
        return E_POINTER;

    SFPCGetStackingW FPCGetStackingW = { lpszBuffer, *pdwBufferSize, E_FAIL };

    ::SendMessage(hwndFlashPlayerControl, FPCM_GET_STACKINGW, 0, (LPARAM)&FPCGetStackingW);

    *pdwBufferSize = FPCGetStackingW.dwBufferSize;

    return FPCGetStackingW.hr;
}

HRESULT WINAPI FPC_PutFlashVarsA(/* in */ HWND hwndFlashPlayerControl, LPCSTR FlashVars)
{
    SFPCPutFlashVarsA FPCPutFlashVarsA = { FlashVars, E_FAIL };

    ::SendMessage(hwndFlashPlayerControl, FPCM_PUT_FLASHVARSA, 0, (LPARAM)&FPCPutFlashVarsA);

    return FPCPutFlashVarsA.hr;
}

HRESULT WINAPI FPC_PutFlashVarsW(/* in */ HWND hwndFlashPlayerControl, LPCWSTR FlashVars)
{
    SFPCPutFlashVarsW FPCPutFlashVarsW = { FlashVars, E_FAIL };

    ::SendMessage(hwndFlashPlayerControl, FPCM_PUT_FLASHVARSW, 0, (LPARAM)&FPCPutFlashVarsW);

    return FPCPutFlashVarsW.hr;
}

HRESULT WINAPI FPC_GetFlashVarsA(/* in */ HWND hwndFlashPlayerControl, LPSTR lpszBuffer, DWORD* pdwBufferSize)
{
    if (NULL == pdwBufferSize)
        return E_POINTER;

    SFPCGetFlashVarsA FPCGetFlashVarsA = { lpszBuffer, *pdwBufferSize, E_FAIL };

    ::SendMessage(hwndFlashPlayerControl, FPCM_GET_FLASHVARSA, 0, (LPARAM)&FPCGetFlashVarsA);

    *pdwBufferSize = FPCGetFlashVarsA.dwBufferSize;

    return FPCGetFlashVarsA.hr;
}

HRESULT WINAPI FPC_GetFlashVarsW(/* in */ HWND hwndFlashPlayerControl, LPWSTR lpszBuffer, DWORD* pdwBufferSize)
{
    if (NULL == pdwBufferSize)
        return E_POINTER;

    SFPCGetFlashVarsW FPCGetFlashVarsW = { lpszBuffer, *pdwBufferSize, E_FAIL };

    ::SendMessage(hwndFlashPlayerControl, FPCM_GET_FLASHVARSW, 0, (LPARAM)&FPCGetFlashVarsW);

    *pdwBufferSize = FPCGetFlashVarsW.dwBufferSize;

    return FPCGetFlashVarsW.hr;
}

HRESULT WINAPI FPC_PutAllowScriptAccessA(/* in */ HWND hwndFlashPlayerControl, LPCSTR AllowScriptAccess)
{
    SFPCPutAllowScriptAccessA FPCPutAllowScriptAccessA = { AllowScriptAccess, E_FAIL };

    ::SendMessage(hwndFlashPlayerControl, FPCM_PUT_ALLOWSCRIPTACCESSA, 0, (LPARAM)&FPCPutAllowScriptAccessA);

    return FPCPutAllowScriptAccessA.hr;
}

HRESULT WINAPI FPC_PutAllowScriptAccessW(/* in */ HWND hwndFlashPlayerControl, LPCWSTR AllowScriptAccess)
{
    SFPCPutAllowScriptAccessW FPCPutAllowScriptAccessW = { AllowScriptAccess, E_FAIL };

    ::SendMessage(hwndFlashPlayerControl, FPCM_PUT_ALLOWSCRIPTACCESSW, 0, (LPARAM)&FPCPutAllowScriptAccessW);

    return FPCPutAllowScriptAccessW.hr;
}

HRESULT WINAPI FPC_GetAllowScriptAccessA(/* in */ HWND hwndFlashPlayerControl, LPSTR lpszBuffer, DWORD* pdwBufferSize)
{
    if (NULL == pdwBufferSize)
        return E_POINTER;

    SFPCGetAllowScriptAccessA FPCGetAllowScriptAccessA = { lpszBuffer, *pdwBufferSize, E_FAIL };

    ::SendMessage(hwndFlashPlayerControl, FPCM_GET_ALLOWSCRIPTACCESSA, 0, (LPARAM)&FPCGetAllowScriptAccessA);

    *pdwBufferSize = FPCGetAllowScriptAccessA.dwBufferSize;

    return FPCGetAllowScriptAccessA.hr;
}

HRESULT WINAPI FPC_GetAllowScriptAccessW(/* in */ HWND hwndFlashPlayerControl, LPWSTR lpszBuffer, DWORD* pdwBufferSize)
{
    if (NULL == pdwBufferSize)
        return E_POINTER;

    SFPCGetAllowScriptAccessW FPCGetAllowScriptAccessW = { lpszBuffer, *pdwBufferSize, E_FAIL };

    ::SendMessage(hwndFlashPlayerControl, FPCM_GET_ALLOWSCRIPTACCESSW, 0, (LPARAM)&FPCGetAllowScriptAccessW);

    *pdwBufferSize = FPCGetAllowScriptAccessW.dwBufferSize;

    return FPCGetAllowScriptAccessW.hr;
}

HRESULT WINAPI FPC_PutMovieDataA(/* in */ HWND hwndFlashPlayerControl, LPCSTR MovieData)
{
    SFPCPutMovieDataA FPCPutMovieDataA = { MovieData, E_FAIL };

    ::SendMessage(hwndFlashPlayerControl, FPCM_PUT_MOVIEDATAA, 0, (LPARAM)&FPCPutMovieDataA);

    return FPCPutMovieDataA.hr;
}

HRESULT WINAPI FPC_PutMovieDataW(/* in */ HWND hwndFlashPlayerControl, LPCWSTR MovieData)
{
    SFPCPutMovieDataW FPCPutMovieDataW = { MovieData, E_FAIL };

    ::SendMessage(hwndFlashPlayerControl, FPCM_PUT_MOVIEDATAW, 0, (LPARAM)&FPCPutMovieDataW);

    return FPCPutMovieDataW.hr;
}

HRESULT WINAPI FPC_GetMovieDataA(/* in */ HWND hwndFlashPlayerControl, LPSTR lpszBuffer, DWORD* pdwBufferSize)
{
    if (NULL == pdwBufferSize)
        return E_POINTER;

    SFPCGetMovieDataA FPCGetMovieDataA = { lpszBuffer, *pdwBufferSize, E_FAIL };

    ::SendMessage(hwndFlashPlayerControl, FPCM_GET_MOVIEDATAA, 0, (LPARAM)&FPCGetMovieDataA);

    *pdwBufferSize = FPCGetMovieDataA.dwBufferSize;

    return FPCGetMovieDataA.hr;
}

HRESULT WINAPI FPC_GetMovieDataW(/* in */ HWND hwndFlashPlayerControl, LPWSTR lpszBuffer, DWORD* pdwBufferSize)
{
    if (NULL == pdwBufferSize)
        return E_POINTER;

    SFPCGetMovieDataW FPCGetMovieDataW = { lpszBuffer, *pdwBufferSize, E_FAIL };

    ::SendMessage(hwndFlashPlayerControl, FPCM_GET_MOVIEDATAW, 0, (LPARAM)&FPCGetMovieDataW);

    *pdwBufferSize = FPCGetMovieDataW.dwBufferSize;

    return FPCGetMovieDataW.hr;
}

