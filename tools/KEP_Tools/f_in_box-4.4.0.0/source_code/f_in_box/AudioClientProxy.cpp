#include "stdafx.h"
#include "FlashOCXLoader.h"
#include "AudioClient.h"
#include "AudioClientProxy.h"
#include "ObjectWithCounter.h"
#include "ComPtr.h"
#include "AudioRenderClientProxy.h"

using namespace f_in_box::com_helpers;
using namespace f_in_box::std;

namespace f_in_box
{
	/// This is a proxy of IAudioClient that passes all calls to underlying
	/// object, but additionally get IDirectSound8 returned by IAudioClient::Activate() - so we could
	/// control audio output.
	class CAudioClientProxy : 
		public CObjectWithCounter, 
		public IAudioClient
	{
	private:
		/// Object that should receive all calls
		CComPtr<IAudioClient> m_pUnderlyingObject;

		/// Main object
		com_helpers::CComPtr<CFlashOCXLoader> m_pLoader;

		/// The audio format, this is a WAVEFORMATEX actually
		CArray<BYTE> m_DeviceFormat;

	public:
		explicit CAudioClientProxy(CFlashOCXLoader* pLoader, IAudioClient* pUnderlyingObject) : 
			m_pLoader(pLoader), 
			m_pUnderlyingObject(pUnderlyingObject)
		{
		}

        virtual ~CAudioClientProxy()
        {
            m_pLoader->RemoveAudioClient(this);
        }

        virtual ULONG __stdcall AddRef()
        {
			return CObjectWithCounter::AddRef();
        }

        virtual ULONG __stdcall Release()
        {
			return CObjectWithCounter::Release();
        }

		HRESULT STDMETHODCALLTYPE QueryInterface(REFIID riid, void** ppvObject)
		{
			if (f_in_box::com_helpers::IsEqualGUID(riid, IID_IUnknown))
				*ppvObject = this;
			else if (f_in_box::com_helpers::IsEqualGUID(riid, IID_IAudioClient))
				*ppvObject = static_cast<IAudioClient*>(this);
			else
				return m_pUnderlyingObject->QueryInterface(riid, ppvObject);

			AddRef();

			return S_OK;
		}

        virtual HRESULT STDMETHODCALLTYPE Initialize( 
            AUDCLNT_SHAREMODE ShareMode,
            DWORD StreamFlags,
            REFERENCE_TIME hnsBufferDuration,
            REFERENCE_TIME hnsPeriodicity,
            const WAVEFORMATEX* pFormat,
            const GUID* AudioSessionGuid)
		{
			HRESULT hr = m_pUnderlyingObject->Initialize(
				ShareMode,
				StreamFlags,
				hnsBufferDuration,
				hnsPeriodicity,
				pFormat,
				AudioSessionGuid);

			if (S_OK == hr)
			{
                m_pLoader->AddAudioClient(this);

				// Save audio format
				m_DeviceFormat = CArray<BYTE>((const BYTE*)pFormat, sizeof(WAVEFORMATEX) + (WAVE_FORMAT_PCM == pFormat->wFormatTag ? 0 : pFormat->cbSize));
			}

			return hr;
		}
        
        virtual HRESULT STDMETHODCALLTYPE GetBufferSize(UINT32* pNumBufferFrames)
		{
			return m_pUnderlyingObject->GetBufferSize(pNumBufferFrames);
		}
        
        virtual HRESULT STDMETHODCALLTYPE GetStreamLatency(REFERENCE_TIME* phnsLatency)
		{
			return m_pUnderlyingObject->GetStreamLatency(phnsLatency);
		}
        
        virtual HRESULT STDMETHODCALLTYPE GetCurrentPadding(UINT32* pNumPaddingFrames)
		{
			return m_pUnderlyingObject->GetCurrentPadding(pNumPaddingFrames);
		}
        
        virtual HRESULT STDMETHODCALLTYPE IsFormatSupported( 
            AUDCLNT_SHAREMODE ShareMode,
            const WAVEFORMATEX* pFormat,
            WAVEFORMATEX** ppClosestMatch)
		{
			return m_pUnderlyingObject->IsFormatSupported(
				ShareMode, 
				pFormat, 
				ppClosestMatch);
		}
        
        virtual HRESULT STDMETHODCALLTYPE GetMixFormat(
            WAVEFORMATEX** ppDeviceFormat)
		{
			return m_pUnderlyingObject->GetMixFormat(ppDeviceFormat);
		}
        
        virtual HRESULT STDMETHODCALLTYPE GetDevicePeriod( 
            REFERENCE_TIME* phnsDefaultDevicePeriod, 
            REFERENCE_TIME* phnsMinimumDevicePeriod)
		{
			return m_pUnderlyingObject->GetDevicePeriod( 
				phnsDefaultDevicePeriod, 
				phnsMinimumDevicePeriod);
		}
        
        virtual HRESULT STDMETHODCALLTYPE Start()
		{
			return m_pUnderlyingObject->Start();
		}

		virtual HRESULT STDMETHODCALLTYPE Stop()
		{
			return m_pUnderlyingObject->Stop();
		}

		virtual HRESULT STDMETHODCALLTYPE Reset()
		{
			return m_pUnderlyingObject->Reset();
		}

		virtual HRESULT STDMETHODCALLTYPE SetEventHandle(HANDLE eventHandle)
		{
			return m_pUnderlyingObject->SetEventHandle(eventHandle);
		}

		virtual HRESULT STDMETHODCALLTYPE GetService(REFIID riid, void** ppv)
		{
			if (f_in_box::com_helpers::IsEqualGUID(riid, IID_IAudioRenderClient))
			{
				CComPtr<IAudioRenderClient> pOriginalAudioRenderClient;
				HRESULT hr = m_pUnderlyingObject->GetService(riid, (void**)&pOriginalAudioRenderClient);
				if (S_OK != hr)
				{
					// Set *ppv to NULL on error as stated in the documentation
					*ppv = NULL;

					return hr;
				}

				return CreateAudioRenderClientProxy(m_DeviceFormat, m_pLoader, pOriginalAudioRenderClient, (IAudioRenderClient**)ppv);
			}

			return m_pUnderlyingObject->GetService(riid, ppv);
		}
   	};

	HRESULT CreateAudioClientProxy(CFlashOCXLoader* pLoader, IAudioClient* pOriginalAudioClient, IAudioClient** ppProxyAudioClient)
	{
		CComPtr<IAudioClient> pProxy = new CAudioClientProxy(pLoader, pOriginalAudioClient);

		return pProxy->QueryInterface(IID_IAudioClient, (void**)ppProxyAudioClient);
    }
}
