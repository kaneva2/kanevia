LRESULT CFlashPlayerControlWindow::OnSetZoomRect(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCSetZoomRect* pMessageInfo = reinterpret_cast<SFPCSetZoomRect*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->SetZoomRect(pMessageInfo->left, pMessageInfo->top, pMessageInfo->right, pMessageInfo->bottom);
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->SetZoomRect(pMessageInfo->left, pMessageInfo->top, pMessageInfo->right, pMessageInfo->bottom);
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->SetZoomRect(pMessageInfo->left, pMessageInfo->top, pMessageInfo->right, pMessageInfo->bottom);
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->SetZoomRect(pMessageInfo->left, pMessageInfo->top, pMessageInfo->right, pMessageInfo->bottom);
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->SetZoomRect(pMessageInfo->left, pMessageInfo->top, pMessageInfo->right, pMessageInfo->bottom);
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->SetZoomRect(pMessageInfo->left, pMessageInfo->top, pMessageInfo->right, pMessageInfo->bottom);
	}

	return 1;
}

LRESULT CFlashPlayerControlWindow::OnZoom(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCZoom* pMessageInfo = reinterpret_cast<SFPCZoom*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->Zoom(pMessageInfo->factor);
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->Zoom(pMessageInfo->factor);
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->Zoom(pMessageInfo->factor);
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->Zoom(pMessageInfo->factor);
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->Zoom(pMessageInfo->factor);
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->Zoom(pMessageInfo->factor);
	}

	return 1;
}

LRESULT CFlashPlayerControlWindow::OnPan(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCPan* pMessageInfo = reinterpret_cast<SFPCPan*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->Pan(pMessageInfo->x, pMessageInfo->y, pMessageInfo->mode);
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->Pan(pMessageInfo->x, pMessageInfo->y, pMessageInfo->mode);
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->Pan(pMessageInfo->x, pMessageInfo->y, pMessageInfo->mode);
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->Pan(pMessageInfo->x, pMessageInfo->y, pMessageInfo->mode);
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->Pan(pMessageInfo->x, pMessageInfo->y, pMessageInfo->mode);
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->Pan(pMessageInfo->x, pMessageInfo->y, pMessageInfo->mode);
	}

	return 1;
}

LRESULT CFlashPlayerControlWindow::OnPlay(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCPlay* pMessageInfo = reinterpret_cast<SFPCPlay*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->Play();
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->Play();
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->Play();
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->Play();
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->Play();
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->Play();
	}

	return 1;
}

LRESULT CFlashPlayerControlWindow::OnStop(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCStop* pMessageInfo = reinterpret_cast<SFPCStop*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->Stop();
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->Stop();
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->Stop();
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->Stop();
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->Stop();
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->Stop();
	}

	return 1;
}

LRESULT CFlashPlayerControlWindow::OnBack(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCBack* pMessageInfo = reinterpret_cast<SFPCBack*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->Back();
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->Back();
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->Back();
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->Back();
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->Back();
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->Back();
	}

	return 1;
}

LRESULT CFlashPlayerControlWindow::OnForward(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCForward* pMessageInfo = reinterpret_cast<SFPCForward*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->Forward();
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->Forward();
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->Forward();
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->Forward();
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->Forward();
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->Forward();
	}

	return 1;
}

LRESULT CFlashPlayerControlWindow::OnRewind(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCRewind* pMessageInfo = reinterpret_cast<SFPCRewind*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->Rewind();
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->Rewind();
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->Rewind();
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->Rewind();
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->Rewind();
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->Rewind();
	}

	return 1;
}

LRESULT CFlashPlayerControlWindow::OnStopPlay(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCStopPlay* pMessageInfo = reinterpret_cast<SFPCStopPlay*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->StopPlay();
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->StopPlay();
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->StopPlay();
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->StopPlay();
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->StopPlay();
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->StopPlay();
	}

	return 1;
}

LRESULT CFlashPlayerControlWindow::OnGotoFrame(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCGotoFrame* pMessageInfo = reinterpret_cast<SFPCGotoFrame*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->GotoFrame(pMessageInfo->FrameNum);
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->GotoFrame(pMessageInfo->FrameNum);
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->GotoFrame(pMessageInfo->FrameNum);
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->GotoFrame(pMessageInfo->FrameNum);
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->GotoFrame(pMessageInfo->FrameNum);
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->GotoFrame(pMessageInfo->FrameNum);
	}

	return 1;
}

LRESULT CFlashPlayerControlWindow::OnCurrentFrame(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCCurrentFrame* pMessageInfo = reinterpret_cast<SFPCCurrentFrame*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->CurrentFrame(&pMessageInfo->Result);
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->CurrentFrame(&pMessageInfo->Result);
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->CurrentFrame(&pMessageInfo->Result);
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->CurrentFrame(&pMessageInfo->Result);
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->CurrentFrame(&pMessageInfo->Result);
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->CurrentFrame(&pMessageInfo->Result);
	}

	return 1;
}

LRESULT CFlashPlayerControlWindow::OnIsPlaying(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCIsPlaying* pMessageInfo = reinterpret_cast<SFPCIsPlaying*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->IsPlaying(&pMessageInfo->Result);
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->IsPlaying(&pMessageInfo->Result);
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->IsPlaying(&pMessageInfo->Result);
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->IsPlaying(&pMessageInfo->Result);
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->IsPlaying(&pMessageInfo->Result);
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->IsPlaying(&pMessageInfo->Result);
	}

	return 1;
}

LRESULT CFlashPlayerControlWindow::OnPercentLoaded(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCPercentLoaded* pMessageInfo = reinterpret_cast<SFPCPercentLoaded*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->PercentLoaded(&pMessageInfo->Result);
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->PercentLoaded(&pMessageInfo->Result);
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->PercentLoaded(&pMessageInfo->Result);
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->PercentLoaded(&pMessageInfo->Result);
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->PercentLoaded(&pMessageInfo->Result);
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->PercentLoaded(&pMessageInfo->Result);
	}

	return 1;
}

LRESULT CFlashPlayerControlWindow::OnFrameLoaded(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCFrameLoaded* pMessageInfo = reinterpret_cast<SFPCFrameLoaded*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->FrameLoaded(pMessageInfo->FrameNum, &pMessageInfo->Result);
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->FrameLoaded(pMessageInfo->FrameNum, &pMessageInfo->Result);
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->FrameLoaded(pMessageInfo->FrameNum, &pMessageInfo->Result);
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->FrameLoaded(pMessageInfo->FrameNum, &pMessageInfo->Result);
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->FrameLoaded(pMessageInfo->FrameNum, &pMessageInfo->Result);
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->FrameLoaded(pMessageInfo->FrameNum, &pMessageInfo->Result);
	}

	return 1;
}

LRESULT CFlashPlayerControlWindow::OnFlashVersion(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCFlashVersion* pMessageInfo = reinterpret_cast<SFPCFlashVersion*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->FlashVersion(&pMessageInfo->Result);
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->FlashVersion(&pMessageInfo->Result);
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->FlashVersion(&pMessageInfo->Result);
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->FlashVersion(&pMessageInfo->Result);
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->FlashVersion(&pMessageInfo->Result);
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->FlashVersion(&pMessageInfo->Result);
	}

	return 1;
}

LRESULT CFlashPlayerControlWindow::OnLoadMovieA(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCLoadMovieA* pMessageInfo = reinterpret_cast<SFPCLoadMovieA*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->LoadMovie(pMessageInfo->layer, _bstr_t(pMessageInfo->url.lpszBuffer).GetBSTR());
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->LoadMovie(pMessageInfo->layer, _bstr_t(pMessageInfo->url.lpszBuffer).GetBSTR());
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->LoadMovie(pMessageInfo->layer, _bstr_t(pMessageInfo->url.lpszBuffer).GetBSTR());
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->LoadMovie(pMessageInfo->layer, _bstr_t(pMessageInfo->url.lpszBuffer).GetBSTR());
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->LoadMovie(pMessageInfo->layer, _bstr_t(pMessageInfo->url.lpszBuffer).GetBSTR());
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->LoadMovie(pMessageInfo->layer, _bstr_t(pMessageInfo->url.lpszBuffer).GetBSTR());
	}

	return 1;
}

LRESULT CFlashPlayerControlWindow::OnLoadMovieW(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCLoadMovieW* pMessageInfo = reinterpret_cast<SFPCLoadMovieW*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->LoadMovie(pMessageInfo->layer, _bstr_t(pMessageInfo->url.lpszBuffer).GetBSTR());
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->LoadMovie(pMessageInfo->layer, _bstr_t(pMessageInfo->url.lpszBuffer).GetBSTR());
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->LoadMovie(pMessageInfo->layer, _bstr_t(pMessageInfo->url.lpszBuffer).GetBSTR());
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->LoadMovie(pMessageInfo->layer, _bstr_t(pMessageInfo->url.lpszBuffer).GetBSTR());
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->LoadMovie(pMessageInfo->layer, _bstr_t(pMessageInfo->url.lpszBuffer).GetBSTR());
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->LoadMovie(pMessageInfo->layer, _bstr_t(pMessageInfo->url.lpszBuffer).GetBSTR());
	}

	return 1;
}

LRESULT CFlashPlayerControlWindow::OnTGotoFrameA(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCTGotoFrameA* pMessageInfo = reinterpret_cast<SFPCTGotoFrameA*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->TGotoFrame(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->FrameNum);
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->TGotoFrame(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->FrameNum);
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->TGotoFrame(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->FrameNum);
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->TGotoFrame(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->FrameNum);
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->TGotoFrame(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->FrameNum);
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->TGotoFrame(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->FrameNum);
	}

	return 1;
}

LRESULT CFlashPlayerControlWindow::OnTGotoFrameW(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCTGotoFrameW* pMessageInfo = reinterpret_cast<SFPCTGotoFrameW*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->TGotoFrame(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->FrameNum);
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->TGotoFrame(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->FrameNum);
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->TGotoFrame(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->FrameNum);
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->TGotoFrame(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->FrameNum);
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->TGotoFrame(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->FrameNum);
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->TGotoFrame(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->FrameNum);
	}

	return 1;
}

LRESULT CFlashPlayerControlWindow::OnTGotoLabelA(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCTGotoLabelA* pMessageInfo = reinterpret_cast<SFPCTGotoLabelA*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->TGotoLabel(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), _bstr_t(pMessageInfo->label.lpszBuffer).GetBSTR());
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->TGotoLabel(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), _bstr_t(pMessageInfo->label.lpszBuffer).GetBSTR());
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->TGotoLabel(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), _bstr_t(pMessageInfo->label.lpszBuffer).GetBSTR());
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->TGotoLabel(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), _bstr_t(pMessageInfo->label.lpszBuffer).GetBSTR());
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->TGotoLabel(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), _bstr_t(pMessageInfo->label.lpszBuffer).GetBSTR());
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->TGotoLabel(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), _bstr_t(pMessageInfo->label.lpszBuffer).GetBSTR());
	}

	return 1;
}

LRESULT CFlashPlayerControlWindow::OnTGotoLabelW(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCTGotoLabelW* pMessageInfo = reinterpret_cast<SFPCTGotoLabelW*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->TGotoLabel(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), _bstr_t(pMessageInfo->label.lpszBuffer).GetBSTR());
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->TGotoLabel(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), _bstr_t(pMessageInfo->label.lpszBuffer).GetBSTR());
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->TGotoLabel(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), _bstr_t(pMessageInfo->label.lpszBuffer).GetBSTR());
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->TGotoLabel(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), _bstr_t(pMessageInfo->label.lpszBuffer).GetBSTR());
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->TGotoLabel(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), _bstr_t(pMessageInfo->label.lpszBuffer).GetBSTR());
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->TGotoLabel(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), _bstr_t(pMessageInfo->label.lpszBuffer).GetBSTR());
	}

	return 1;
}

LRESULT CFlashPlayerControlWindow::OnTCurrentFrameA(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCTCurrentFrameA* pMessageInfo = reinterpret_cast<SFPCTCurrentFrameA*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->TCurrentFrame(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), &pMessageInfo->Result);
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->TCurrentFrame(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), &pMessageInfo->Result);
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->TCurrentFrame(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), &pMessageInfo->Result);
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->TCurrentFrame(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), &pMessageInfo->Result);
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->TCurrentFrame(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), &pMessageInfo->Result);
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->TCurrentFrame(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), &pMessageInfo->Result);
	}

	return 1;
}

LRESULT CFlashPlayerControlWindow::OnTCurrentFrameW(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCTCurrentFrameW* pMessageInfo = reinterpret_cast<SFPCTCurrentFrameW*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->TCurrentFrame(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), &pMessageInfo->Result);
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->TCurrentFrame(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), &pMessageInfo->Result);
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->TCurrentFrame(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), &pMessageInfo->Result);
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->TCurrentFrame(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), &pMessageInfo->Result);
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->TCurrentFrame(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), &pMessageInfo->Result);
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->TCurrentFrame(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), &pMessageInfo->Result);
	}

	return 1;
}

LRESULT CFlashPlayerControlWindow::OnTCurrentLabelA(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCTCurrentLabelA* pMessageInfo = reinterpret_cast<SFPCTCurrentLabelA*>(lParam);

	pMessageInfo->hr = E_FAIL;

	_bstr_t bstrResult;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->TCurrentLabel(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), bstrResult.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->Result.dwBufferSize = lstrlenA((LPCSTR)bstrResult) + 1;

			if (NULL != pMessageInfo->Result.lpszBuffer)
				lstrcpyA(pMessageInfo->Result.lpszBuffer, (LPCSTR)bstrResult);
		}
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->TCurrentLabel(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), bstrResult.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->Result.dwBufferSize = lstrlenA((LPCSTR)bstrResult) + 1;

			if (NULL != pMessageInfo->Result.lpszBuffer)
				lstrcpyA(pMessageInfo->Result.lpszBuffer, (LPCSTR)bstrResult);
		}
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->TCurrentLabel(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), bstrResult.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->Result.dwBufferSize = lstrlenA((LPCSTR)bstrResult) + 1;

			if (NULL != pMessageInfo->Result.lpszBuffer)
				lstrcpyA(pMessageInfo->Result.lpszBuffer, (LPCSTR)bstrResult);
		}
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->TCurrentLabel(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), bstrResult.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->Result.dwBufferSize = lstrlenA((LPCSTR)bstrResult) + 1;

			if (NULL != pMessageInfo->Result.lpszBuffer)
				lstrcpyA(pMessageInfo->Result.lpszBuffer, (LPCSTR)bstrResult);
		}
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->TCurrentLabel(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), bstrResult.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->Result.dwBufferSize = lstrlenA((LPCSTR)bstrResult) + 1;

			if (NULL != pMessageInfo->Result.lpszBuffer)
				lstrcpyA(pMessageInfo->Result.lpszBuffer, (LPCSTR)bstrResult);
		}
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->TCurrentLabel(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), bstrResult.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->Result.dwBufferSize = lstrlenA((LPCSTR)bstrResult) + 1;

			if (NULL != pMessageInfo->Result.lpszBuffer)
				lstrcpyA(pMessageInfo->Result.lpszBuffer, (LPCSTR)bstrResult);
		}
	}

	return 1;
}

LRESULT CFlashPlayerControlWindow::OnTCurrentLabelW(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCTCurrentLabelW* pMessageInfo = reinterpret_cast<SFPCTCurrentLabelW*>(lParam);

	pMessageInfo->hr = E_FAIL;

	_bstr_t bstrResult;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->TCurrentLabel(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), bstrResult.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->Result.dwBufferSize = lstrlenW((LPCWSTR)bstrResult) + 1;

			if (NULL != pMessageInfo->Result.lpszBuffer)
				lstrcpyW(pMessageInfo->Result.lpszBuffer, (LPCWSTR)bstrResult);
		}
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->TCurrentLabel(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), bstrResult.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->Result.dwBufferSize = lstrlenW((LPCWSTR)bstrResult) + 1;

			if (NULL != pMessageInfo->Result.lpszBuffer)
				lstrcpyW(pMessageInfo->Result.lpszBuffer, (LPCWSTR)bstrResult);
		}
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->TCurrentLabel(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), bstrResult.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->Result.dwBufferSize = lstrlenW((LPCWSTR)bstrResult) + 1;

			if (NULL != pMessageInfo->Result.lpszBuffer)
				lstrcpyW(pMessageInfo->Result.lpszBuffer, (LPCWSTR)bstrResult);
		}
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->TCurrentLabel(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), bstrResult.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->Result.dwBufferSize = lstrlenW((LPCWSTR)bstrResult) + 1;

			if (NULL != pMessageInfo->Result.lpszBuffer)
				lstrcpyW(pMessageInfo->Result.lpszBuffer, (LPCWSTR)bstrResult);
		}
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->TCurrentLabel(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), bstrResult.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->Result.dwBufferSize = lstrlenW((LPCWSTR)bstrResult) + 1;

			if (NULL != pMessageInfo->Result.lpszBuffer)
				lstrcpyW(pMessageInfo->Result.lpszBuffer, (LPCWSTR)bstrResult);
		}
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->TCurrentLabel(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), bstrResult.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->Result.dwBufferSize = lstrlenW((LPCWSTR)bstrResult) + 1;

			if (NULL != pMessageInfo->Result.lpszBuffer)
				lstrcpyW(pMessageInfo->Result.lpszBuffer, (LPCWSTR)bstrResult);
		}
	}

	return 1;
}

LRESULT CFlashPlayerControlWindow::OnTPlayA(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCTPlayA* pMessageInfo = reinterpret_cast<SFPCTPlayA*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->TPlay(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR());
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->TPlay(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR());
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->TPlay(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR());
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->TPlay(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR());
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->TPlay(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR());
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->TPlay(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR());
	}

	return 1;
}

LRESULT CFlashPlayerControlWindow::OnTPlayW(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCTPlayW* pMessageInfo = reinterpret_cast<SFPCTPlayW*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->TPlay(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR());
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->TPlay(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR());
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->TPlay(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR());
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->TPlay(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR());
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->TPlay(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR());
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->TPlay(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR());
	}

	return 1;
}

LRESULT CFlashPlayerControlWindow::OnTStopPlayA(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCTStopPlayA* pMessageInfo = reinterpret_cast<SFPCTStopPlayA*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->TStopPlay(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR());
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->TStopPlay(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR());
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->TStopPlay(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR());
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->TStopPlay(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR());
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->TStopPlay(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR());
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->TStopPlay(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR());
	}

	return 1;
}

LRESULT CFlashPlayerControlWindow::OnTStopPlayW(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCTStopPlayW* pMessageInfo = reinterpret_cast<SFPCTStopPlayW*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->TStopPlay(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR());
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->TStopPlay(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR());
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->TStopPlay(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR());
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->TStopPlay(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR());
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->TStopPlay(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR());
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->TStopPlay(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR());
	}

	return 1;
}

LRESULT CFlashPlayerControlWindow::OnSetVariableA(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCSetVariableA* pMessageInfo = reinterpret_cast<SFPCSetVariableA*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->SetVariable(_bstr_t(pMessageInfo->name.lpszBuffer).GetBSTR(), _bstr_t(pMessageInfo->value.lpszBuffer).GetBSTR());
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->SetVariable(_bstr_t(pMessageInfo->name.lpszBuffer).GetBSTR(), _bstr_t(pMessageInfo->value.lpszBuffer).GetBSTR());
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->SetVariable(_bstr_t(pMessageInfo->name.lpszBuffer).GetBSTR(), _bstr_t(pMessageInfo->value.lpszBuffer).GetBSTR());
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->SetVariable(_bstr_t(pMessageInfo->name.lpszBuffer).GetBSTR(), _bstr_t(pMessageInfo->value.lpszBuffer).GetBSTR());
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->SetVariable(_bstr_t(pMessageInfo->name.lpszBuffer).GetBSTR(), _bstr_t(pMessageInfo->value.lpszBuffer).GetBSTR());
	}

	return 1;
}

LRESULT CFlashPlayerControlWindow::OnSetVariableW(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCSetVariableW* pMessageInfo = reinterpret_cast<SFPCSetVariableW*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->SetVariable(_bstr_t(pMessageInfo->name.lpszBuffer).GetBSTR(), _bstr_t(pMessageInfo->value.lpszBuffer).GetBSTR());
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->SetVariable(_bstr_t(pMessageInfo->name.lpszBuffer).GetBSTR(), _bstr_t(pMessageInfo->value.lpszBuffer).GetBSTR());
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->SetVariable(_bstr_t(pMessageInfo->name.lpszBuffer).GetBSTR(), _bstr_t(pMessageInfo->value.lpszBuffer).GetBSTR());
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->SetVariable(_bstr_t(pMessageInfo->name.lpszBuffer).GetBSTR(), _bstr_t(pMessageInfo->value.lpszBuffer).GetBSTR());
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->SetVariable(_bstr_t(pMessageInfo->name.lpszBuffer).GetBSTR(), _bstr_t(pMessageInfo->value.lpszBuffer).GetBSTR());
	}

	return 1;
}

LRESULT CFlashPlayerControlWindow::OnGetVariableA(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCGetVariableA* pMessageInfo = reinterpret_cast<SFPCGetVariableA*>(lParam);

	pMessageInfo->hr = E_FAIL;

	_bstr_t bstrResult;

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->GetVariable(_bstr_t(pMessageInfo->name.lpszBuffer).GetBSTR(), bstrResult.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->Result.dwBufferSize = lstrlenA((LPCSTR)bstrResult) + 1;

			if (NULL != pMessageInfo->Result.lpszBuffer)
				lstrcpyA(pMessageInfo->Result.lpszBuffer, (LPCSTR)bstrResult);
		}
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->GetVariable(_bstr_t(pMessageInfo->name.lpszBuffer).GetBSTR(), bstrResult.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->Result.dwBufferSize = lstrlenA((LPCSTR)bstrResult) + 1;

			if (NULL != pMessageInfo->Result.lpszBuffer)
				lstrcpyA(pMessageInfo->Result.lpszBuffer, (LPCSTR)bstrResult);
		}
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->GetVariable(_bstr_t(pMessageInfo->name.lpszBuffer).GetBSTR(), bstrResult.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->Result.dwBufferSize = lstrlenA((LPCSTR)bstrResult) + 1;

			if (NULL != pMessageInfo->Result.lpszBuffer)
				lstrcpyA(pMessageInfo->Result.lpszBuffer, (LPCSTR)bstrResult);
		}
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->GetVariable(_bstr_t(pMessageInfo->name.lpszBuffer).GetBSTR(), bstrResult.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->Result.dwBufferSize = lstrlenA((LPCSTR)bstrResult) + 1;

			if (NULL != pMessageInfo->Result.lpszBuffer)
				lstrcpyA(pMessageInfo->Result.lpszBuffer, (LPCSTR)bstrResult);
		}
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->GetVariable(_bstr_t(pMessageInfo->name.lpszBuffer).GetBSTR(), bstrResult.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->Result.dwBufferSize = lstrlenA((LPCSTR)bstrResult) + 1;

			if (NULL != pMessageInfo->Result.lpszBuffer)
				lstrcpyA(pMessageInfo->Result.lpszBuffer, (LPCSTR)bstrResult);
		}
	}

	return 1;
}

LRESULT CFlashPlayerControlWindow::OnGetVariableW(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCGetVariableW* pMessageInfo = reinterpret_cast<SFPCGetVariableW*>(lParam);

	pMessageInfo->hr = E_FAIL;

	_bstr_t bstrResult;

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->GetVariable(_bstr_t(pMessageInfo->name.lpszBuffer).GetBSTR(), bstrResult.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->Result.dwBufferSize = lstrlenW((LPCWSTR)bstrResult) + 1;

			if (NULL != pMessageInfo->Result.lpszBuffer)
				lstrcpyW(pMessageInfo->Result.lpszBuffer, (LPCWSTR)bstrResult);
		}
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->GetVariable(_bstr_t(pMessageInfo->name.lpszBuffer).GetBSTR(), bstrResult.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->Result.dwBufferSize = lstrlenW((LPCWSTR)bstrResult) + 1;

			if (NULL != pMessageInfo->Result.lpszBuffer)
				lstrcpyW(pMessageInfo->Result.lpszBuffer, (LPCWSTR)bstrResult);
		}
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->GetVariable(_bstr_t(pMessageInfo->name.lpszBuffer).GetBSTR(), bstrResult.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->Result.dwBufferSize = lstrlenW((LPCWSTR)bstrResult) + 1;

			if (NULL != pMessageInfo->Result.lpszBuffer)
				lstrcpyW(pMessageInfo->Result.lpszBuffer, (LPCWSTR)bstrResult);
		}
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->GetVariable(_bstr_t(pMessageInfo->name.lpszBuffer).GetBSTR(), bstrResult.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->Result.dwBufferSize = lstrlenW((LPCWSTR)bstrResult) + 1;

			if (NULL != pMessageInfo->Result.lpszBuffer)
				lstrcpyW(pMessageInfo->Result.lpszBuffer, (LPCWSTR)bstrResult);
		}
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->GetVariable(_bstr_t(pMessageInfo->name.lpszBuffer).GetBSTR(), bstrResult.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->Result.dwBufferSize = lstrlenW((LPCWSTR)bstrResult) + 1;

			if (NULL != pMessageInfo->Result.lpszBuffer)
				lstrcpyW(pMessageInfo->Result.lpszBuffer, (LPCWSTR)bstrResult);
		}
	}

	return 1;
}

LRESULT CFlashPlayerControlWindow::OnTSetPropertyA(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCTSetPropertyA* pMessageInfo = reinterpret_cast<SFPCTSetPropertyA*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->TSetProperty(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->property, _bstr_t(pMessageInfo->value.lpszBuffer).GetBSTR());
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->TSetProperty(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->property, _bstr_t(pMessageInfo->value.lpszBuffer).GetBSTR());
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->TSetProperty(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->property, _bstr_t(pMessageInfo->value.lpszBuffer).GetBSTR());
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->TSetProperty(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->property, _bstr_t(pMessageInfo->value.lpszBuffer).GetBSTR());
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->TSetProperty(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->property, _bstr_t(pMessageInfo->value.lpszBuffer).GetBSTR());
	}

	return 1;
}

LRESULT CFlashPlayerControlWindow::OnTSetPropertyW(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCTSetPropertyW* pMessageInfo = reinterpret_cast<SFPCTSetPropertyW*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->TSetProperty(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->property, _bstr_t(pMessageInfo->value.lpszBuffer).GetBSTR());
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->TSetProperty(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->property, _bstr_t(pMessageInfo->value.lpszBuffer).GetBSTR());
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->TSetProperty(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->property, _bstr_t(pMessageInfo->value.lpszBuffer).GetBSTR());
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->TSetProperty(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->property, _bstr_t(pMessageInfo->value.lpszBuffer).GetBSTR());
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->TSetProperty(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->property, _bstr_t(pMessageInfo->value.lpszBuffer).GetBSTR());
	}

	return 1;
}

LRESULT CFlashPlayerControlWindow::OnTGetPropertyA(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCTGetPropertyA* pMessageInfo = reinterpret_cast<SFPCTGetPropertyA*>(lParam);

	pMessageInfo->hr = E_FAIL;

	_bstr_t bstrResult;

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->TGetProperty(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->property, bstrResult.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->Result.dwBufferSize = lstrlenA((LPCSTR)bstrResult) + 1;

			if (NULL != pMessageInfo->Result.lpszBuffer)
				lstrcpyA(pMessageInfo->Result.lpszBuffer, (LPCSTR)bstrResult);
		}
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->TGetProperty(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->property, bstrResult.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->Result.dwBufferSize = lstrlenA((LPCSTR)bstrResult) + 1;

			if (NULL != pMessageInfo->Result.lpszBuffer)
				lstrcpyA(pMessageInfo->Result.lpszBuffer, (LPCSTR)bstrResult);
		}
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->TGetProperty(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->property, bstrResult.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->Result.dwBufferSize = lstrlenA((LPCSTR)bstrResult) + 1;

			if (NULL != pMessageInfo->Result.lpszBuffer)
				lstrcpyA(pMessageInfo->Result.lpszBuffer, (LPCSTR)bstrResult);
		}
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->TGetProperty(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->property, bstrResult.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->Result.dwBufferSize = lstrlenA((LPCSTR)bstrResult) + 1;

			if (NULL != pMessageInfo->Result.lpszBuffer)
				lstrcpyA(pMessageInfo->Result.lpszBuffer, (LPCSTR)bstrResult);
		}
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->TGetProperty(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->property, bstrResult.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->Result.dwBufferSize = lstrlenA((LPCSTR)bstrResult) + 1;

			if (NULL != pMessageInfo->Result.lpszBuffer)
				lstrcpyA(pMessageInfo->Result.lpszBuffer, (LPCSTR)bstrResult);
		}
	}

	return 1;
}

LRESULT CFlashPlayerControlWindow::OnTGetPropertyW(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCTGetPropertyW* pMessageInfo = reinterpret_cast<SFPCTGetPropertyW*>(lParam);

	pMessageInfo->hr = E_FAIL;

	_bstr_t bstrResult;

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->TGetProperty(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->property, bstrResult.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->Result.dwBufferSize = lstrlenW((LPCWSTR)bstrResult) + 1;

			if (NULL != pMessageInfo->Result.lpszBuffer)
				lstrcpyW(pMessageInfo->Result.lpszBuffer, (LPCWSTR)bstrResult);
		}
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->TGetProperty(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->property, bstrResult.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->Result.dwBufferSize = lstrlenW((LPCWSTR)bstrResult) + 1;

			if (NULL != pMessageInfo->Result.lpszBuffer)
				lstrcpyW(pMessageInfo->Result.lpszBuffer, (LPCWSTR)bstrResult);
		}
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->TGetProperty(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->property, bstrResult.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->Result.dwBufferSize = lstrlenW((LPCWSTR)bstrResult) + 1;

			if (NULL != pMessageInfo->Result.lpszBuffer)
				lstrcpyW(pMessageInfo->Result.lpszBuffer, (LPCWSTR)bstrResult);
		}
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->TGetProperty(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->property, bstrResult.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->Result.dwBufferSize = lstrlenW((LPCWSTR)bstrResult) + 1;

			if (NULL != pMessageInfo->Result.lpszBuffer)
				lstrcpyW(pMessageInfo->Result.lpszBuffer, (LPCWSTR)bstrResult);
		}
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->TGetProperty(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->property, bstrResult.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->Result.dwBufferSize = lstrlenW((LPCWSTR)bstrResult) + 1;

			if (NULL != pMessageInfo->Result.lpszBuffer)
				lstrcpyW(pMessageInfo->Result.lpszBuffer, (LPCWSTR)bstrResult);
		}
	}

	return 1;
}

LRESULT CFlashPlayerControlWindow::OnTCallFrameA(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCTCallFrameA* pMessageInfo = reinterpret_cast<SFPCTCallFrameA*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->TCallFrame(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->FrameNum);
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->TCallFrame(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->FrameNum);
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->TCallFrame(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->FrameNum);
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->TCallFrame(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->FrameNum);
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->TCallFrame(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->FrameNum);
	}

	return 1;
}

LRESULT CFlashPlayerControlWindow::OnTCallFrameW(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCTCallFrameW* pMessageInfo = reinterpret_cast<SFPCTCallFrameW*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->TCallFrame(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->FrameNum);
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->TCallFrame(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->FrameNum);
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->TCallFrame(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->FrameNum);
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->TCallFrame(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->FrameNum);
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->TCallFrame(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->FrameNum);
	}

	return 1;
}

LRESULT CFlashPlayerControlWindow::OnTCallLabelA(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCTCallLabelA* pMessageInfo = reinterpret_cast<SFPCTCallLabelA*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->TCallLabel(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), _bstr_t(pMessageInfo->label.lpszBuffer).GetBSTR());
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->TCallLabel(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), _bstr_t(pMessageInfo->label.lpszBuffer).GetBSTR());
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->TCallLabel(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), _bstr_t(pMessageInfo->label.lpszBuffer).GetBSTR());
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->TCallLabel(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), _bstr_t(pMessageInfo->label.lpszBuffer).GetBSTR());
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->TCallLabel(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), _bstr_t(pMessageInfo->label.lpszBuffer).GetBSTR());
	}

	return 1;
}

LRESULT CFlashPlayerControlWindow::OnTCallLabelW(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCTCallLabelW* pMessageInfo = reinterpret_cast<SFPCTCallLabelW*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->TCallLabel(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), _bstr_t(pMessageInfo->label.lpszBuffer).GetBSTR());
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->TCallLabel(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), _bstr_t(pMessageInfo->label.lpszBuffer).GetBSTR());
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->TCallLabel(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), _bstr_t(pMessageInfo->label.lpszBuffer).GetBSTR());
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->TCallLabel(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), _bstr_t(pMessageInfo->label.lpszBuffer).GetBSTR());
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->TCallLabel(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), _bstr_t(pMessageInfo->label.lpszBuffer).GetBSTR());
	}

	return 1;
}

LRESULT CFlashPlayerControlWindow::OnTSetPropertyNumA(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCTSetPropertyNumA* pMessageInfo = reinterpret_cast<SFPCTSetPropertyNumA*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->TSetPropertyNum(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->property, pMessageInfo->value);
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->TSetPropertyNum(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->property, pMessageInfo->value);
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->TSetPropertyNum(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->property, pMessageInfo->value);
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->TSetPropertyNum(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->property, pMessageInfo->value);
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->TSetPropertyNum(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->property, pMessageInfo->value);
	}

	return 1;
}

LRESULT CFlashPlayerControlWindow::OnTSetPropertyNumW(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCTSetPropertyNumW* pMessageInfo = reinterpret_cast<SFPCTSetPropertyNumW*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->TSetPropertyNum(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->property, pMessageInfo->value);
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->TSetPropertyNum(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->property, pMessageInfo->value);
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->TSetPropertyNum(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->property, pMessageInfo->value);
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->TSetPropertyNum(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->property, pMessageInfo->value);
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->TSetPropertyNum(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->property, pMessageInfo->value);
	}

	return 1;
}

LRESULT CFlashPlayerControlWindow::OnTGetPropertyNumA(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCTGetPropertyNumA* pMessageInfo = reinterpret_cast<SFPCTGetPropertyNumA*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->TGetPropertyNum(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->property, &pMessageInfo->Result);
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->TGetPropertyNum(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->property, &pMessageInfo->Result);
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->TGetPropertyNum(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->property, &pMessageInfo->Result);
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->TGetPropertyNum(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->property, &pMessageInfo->Result);
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->TGetPropertyNum(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->property, &pMessageInfo->Result);
	}

	return 1;
}

LRESULT CFlashPlayerControlWindow::OnTGetPropertyNumW(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCTGetPropertyNumW* pMessageInfo = reinterpret_cast<SFPCTGetPropertyNumW*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->TGetPropertyNum(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->property, &pMessageInfo->Result);
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->TGetPropertyNum(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->property, &pMessageInfo->Result);
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->TGetPropertyNum(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->property, &pMessageInfo->Result);
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->TGetPropertyNum(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->property, &pMessageInfo->Result);
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->TGetPropertyNum(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->property, &pMessageInfo->Result);
	}

	return 1;
}

LRESULT CFlashPlayerControlWindow::OnTGetPropertyAsNumberA(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCTGetPropertyAsNumberA* pMessageInfo = reinterpret_cast<SFPCTGetPropertyAsNumberA*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->TGetPropertyAsNumber(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->property, &pMessageInfo->Result);
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->TGetPropertyAsNumber(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->property, &pMessageInfo->Result);
	}

	return 1;
}

LRESULT CFlashPlayerControlWindow::OnTGetPropertyAsNumberW(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCTGetPropertyAsNumberW* pMessageInfo = reinterpret_cast<SFPCTGetPropertyAsNumberW*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->TGetPropertyAsNumber(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->property, &pMessageInfo->Result);
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->TGetPropertyAsNumber(_bstr_t(pMessageInfo->target.lpszBuffer).GetBSTR(), pMessageInfo->property, &pMessageInfo->Result);
	}

	return 1;
}

LRESULT CFlashPlayerControlWindow::OnGetReadyState(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCGetReadyState* pMessageInfo = reinterpret_cast<SFPCGetReadyState*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->get_ReadyState(&pMessageInfo->ReadyState);
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->get_ReadyState(&pMessageInfo->ReadyState);
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->get_ReadyState(&pMessageInfo->ReadyState);
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->get_ReadyState(&pMessageInfo->ReadyState);
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->get_ReadyState(&pMessageInfo->ReadyState);
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->get_ReadyState(&pMessageInfo->ReadyState);
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnGetTotalFrames(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCGetTotalFrames* pMessageInfo = reinterpret_cast<SFPCGetTotalFrames*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->get_TotalFrames(&pMessageInfo->TotalFrames);
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->get_TotalFrames(&pMessageInfo->TotalFrames);
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->get_TotalFrames(&pMessageInfo->TotalFrames);
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->get_TotalFrames(&pMessageInfo->TotalFrames);
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->get_TotalFrames(&pMessageInfo->TotalFrames);
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->get_TotalFrames(&pMessageInfo->TotalFrames);
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnPutPlaying(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCPutPlaying* pMessageInfo = reinterpret_cast<SFPCPutPlaying*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->put_Playing(pMessageInfo->Playing);
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->put_Playing(pMessageInfo->Playing);
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->put_Playing(pMessageInfo->Playing);
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->put_Playing(pMessageInfo->Playing);
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->put_Playing(pMessageInfo->Playing);
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->put_Playing(pMessageInfo->Playing);
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnGetPlaying(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCGetPlaying* pMessageInfo = reinterpret_cast<SFPCGetPlaying*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->get_Playing(&pMessageInfo->Playing);
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->get_Playing(&pMessageInfo->Playing);
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->get_Playing(&pMessageInfo->Playing);
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->get_Playing(&pMessageInfo->Playing);
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->get_Playing(&pMessageInfo->Playing);
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->get_Playing(&pMessageInfo->Playing);
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnPutQuality(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCPutQuality* pMessageInfo = reinterpret_cast<SFPCPutQuality*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->put_Quality(pMessageInfo->Quality);
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->put_Quality(pMessageInfo->Quality);
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->put_Quality(pMessageInfo->Quality);
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->put_Quality(pMessageInfo->Quality);
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->put_Quality(pMessageInfo->Quality);
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->put_Quality(pMessageInfo->Quality);
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnGetQuality(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCGetQuality* pMessageInfo = reinterpret_cast<SFPCGetQuality*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->get_Quality(&pMessageInfo->Quality);
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->get_Quality(&pMessageInfo->Quality);
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->get_Quality(&pMessageInfo->Quality);
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->get_Quality(&pMessageInfo->Quality);
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->get_Quality(&pMessageInfo->Quality);
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->get_Quality(&pMessageInfo->Quality);
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnPutScaleMode(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCPutScaleMode* pMessageInfo = reinterpret_cast<SFPCPutScaleMode*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->put_ScaleMode(pMessageInfo->ScaleMode);
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->put_ScaleMode(pMessageInfo->ScaleMode);
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->put_ScaleMode(pMessageInfo->ScaleMode);
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->put_ScaleMode(pMessageInfo->ScaleMode);
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->put_ScaleMode(pMessageInfo->ScaleMode);
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->put_ScaleMode(pMessageInfo->ScaleMode);
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnGetScaleMode(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCGetScaleMode* pMessageInfo = reinterpret_cast<SFPCGetScaleMode*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->get_ScaleMode(&pMessageInfo->ScaleMode);
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->get_ScaleMode(&pMessageInfo->ScaleMode);
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->get_ScaleMode(&pMessageInfo->ScaleMode);
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->get_ScaleMode(&pMessageInfo->ScaleMode);
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->get_ScaleMode(&pMessageInfo->ScaleMode);
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->get_ScaleMode(&pMessageInfo->ScaleMode);
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnPutAlignMode(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCPutAlignMode* pMessageInfo = reinterpret_cast<SFPCPutAlignMode*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->put_AlignMode(pMessageInfo->AlignMode);
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->put_AlignMode(pMessageInfo->AlignMode);
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->put_AlignMode(pMessageInfo->AlignMode);
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->put_AlignMode(pMessageInfo->AlignMode);
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->put_AlignMode(pMessageInfo->AlignMode);
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->put_AlignMode(pMessageInfo->AlignMode);
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnGetAlignMode(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCGetAlignMode* pMessageInfo = reinterpret_cast<SFPCGetAlignMode*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->get_AlignMode(&pMessageInfo->AlignMode);
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->get_AlignMode(&pMessageInfo->AlignMode);
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->get_AlignMode(&pMessageInfo->AlignMode);
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->get_AlignMode(&pMessageInfo->AlignMode);
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->get_AlignMode(&pMessageInfo->AlignMode);
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->get_AlignMode(&pMessageInfo->AlignMode);
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnPutBackgroundColor(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCPutBackgroundColor* pMessageInfo = reinterpret_cast<SFPCPutBackgroundColor*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->put_BackgroundColor(pMessageInfo->BackgroundColor);
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->put_BackgroundColor(pMessageInfo->BackgroundColor);
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->put_BackgroundColor(pMessageInfo->BackgroundColor);
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->put_BackgroundColor(pMessageInfo->BackgroundColor);
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->put_BackgroundColor(pMessageInfo->BackgroundColor);
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->put_BackgroundColor(pMessageInfo->BackgroundColor);
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnGetBackgroundColor(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCGetBackgroundColor* pMessageInfo = reinterpret_cast<SFPCGetBackgroundColor*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->get_BackgroundColor(&pMessageInfo->BackgroundColor);
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->get_BackgroundColor(&pMessageInfo->BackgroundColor);
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->get_BackgroundColor(&pMessageInfo->BackgroundColor);
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->get_BackgroundColor(&pMessageInfo->BackgroundColor);
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->get_BackgroundColor(&pMessageInfo->BackgroundColor);
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->get_BackgroundColor(&pMessageInfo->BackgroundColor);
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnPutLoop(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCPutLoop* pMessageInfo = reinterpret_cast<SFPCPutLoop*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->put_Loop(pMessageInfo->Loop);
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->put_Loop(pMessageInfo->Loop);
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->put_Loop(pMessageInfo->Loop);
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->put_Loop(pMessageInfo->Loop);
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->put_Loop(pMessageInfo->Loop);
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->put_Loop(pMessageInfo->Loop);
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnGetLoop(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCGetLoop* pMessageInfo = reinterpret_cast<SFPCGetLoop*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->get_Loop(&pMessageInfo->Loop);
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->get_Loop(&pMessageInfo->Loop);
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->get_Loop(&pMessageInfo->Loop);
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->get_Loop(&pMessageInfo->Loop);
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->get_Loop(&pMessageInfo->Loop);
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->get_Loop(&pMessageInfo->Loop);
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnPutMovieA(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCPutMovieA* pMessageInfo = reinterpret_cast<SFPCPutMovieA*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->put_Movie(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->put_Movie(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->put_Movie(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->put_Movie(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->put_Movie(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->put_Movie(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnPutMovieW(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCPutMovieW* pMessageInfo = reinterpret_cast<SFPCPutMovieW*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->put_Movie(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->put_Movie(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->put_Movie(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->put_Movie(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->put_Movie(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->put_Movie(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnGetMovieA(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCGetMovieA* pMessageInfo = reinterpret_cast<SFPCGetMovieA*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->get_Movie(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenA((LPSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyA(pMessageInfo->lpszBuffer, (LPCSTR)bstrPropertyValue);
		}
	}

	if (4 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->get_Movie(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenA((LPSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyA(pMessageInfo->lpszBuffer, (LPCSTR)bstrPropertyValue);
		}
	}

	if (5 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->get_Movie(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenA((LPSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyA(pMessageInfo->lpszBuffer, (LPCSTR)bstrPropertyValue);
		}
	}

	if (6 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->get_Movie(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenA((LPSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyA(pMessageInfo->lpszBuffer, (LPCSTR)bstrPropertyValue);
		}
	}

	if (7 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->get_Movie(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenA((LPSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyA(pMessageInfo->lpszBuffer, (LPCSTR)bstrPropertyValue);
		}
	}

	if (8 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->get_Movie(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenA((LPSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyA(pMessageInfo->lpszBuffer, (LPCSTR)bstrPropertyValue);
		}
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnGetMovieW(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCGetMovieW* pMessageInfo = reinterpret_cast<SFPCGetMovieW*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->get_Movie(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenW((LPCWSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyW(pMessageInfo->lpszBuffer, (LPCWSTR)bstrPropertyValue);
		}
	}

	if (4 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->get_Movie(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenW((LPCWSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyW(pMessageInfo->lpszBuffer, (LPCWSTR)bstrPropertyValue);
		}
	}

	if (5 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->get_Movie(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenW((LPCWSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyW(pMessageInfo->lpszBuffer, (LPCWSTR)bstrPropertyValue);
		}
	}

	if (6 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->get_Movie(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenW((LPCWSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyW(pMessageInfo->lpszBuffer, (LPCWSTR)bstrPropertyValue);
		}
	}

	if (7 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->get_Movie(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenW((LPCWSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyW(pMessageInfo->lpszBuffer, (LPCWSTR)bstrPropertyValue);
		}
	}

	if (8 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->get_Movie(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenW((LPCWSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyW(pMessageInfo->lpszBuffer, (LPCWSTR)bstrPropertyValue);
		}
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnPutFrameNum(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCPutFrameNum* pMessageInfo = reinterpret_cast<SFPCPutFrameNum*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->put_FrameNum(pMessageInfo->FrameNum);
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->put_FrameNum(pMessageInfo->FrameNum);
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->put_FrameNum(pMessageInfo->FrameNum);
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->put_FrameNum(pMessageInfo->FrameNum);
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->put_FrameNum(pMessageInfo->FrameNum);
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->put_FrameNum(pMessageInfo->FrameNum);
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnGetFrameNum(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCGetFrameNum* pMessageInfo = reinterpret_cast<SFPCGetFrameNum*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->get_FrameNum(&pMessageInfo->FrameNum);
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->get_FrameNum(&pMessageInfo->FrameNum);
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->get_FrameNum(&pMessageInfo->FrameNum);
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->get_FrameNum(&pMessageInfo->FrameNum);
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->get_FrameNum(&pMessageInfo->FrameNum);
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->get_FrameNum(&pMessageInfo->FrameNum);
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnPutWModeA(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCPutWModeA* pMessageInfo = reinterpret_cast<SFPCPutWModeA*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->put_WMode(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->put_WMode(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->put_WMode(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->put_WMode(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->put_WMode(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->put_WMode(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnPutWModeW(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCPutWModeW* pMessageInfo = reinterpret_cast<SFPCPutWModeW*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->put_WMode(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->put_WMode(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->put_WMode(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->put_WMode(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->put_WMode(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->put_WMode(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnGetWModeA(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCGetWModeA* pMessageInfo = reinterpret_cast<SFPCGetWModeA*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->get_WMode(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenA((LPSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyA(pMessageInfo->lpszBuffer, (LPCSTR)bstrPropertyValue);
		}
	}

	if (4 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->get_WMode(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenA((LPSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyA(pMessageInfo->lpszBuffer, (LPCSTR)bstrPropertyValue);
		}
	}

	if (5 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->get_WMode(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenA((LPSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyA(pMessageInfo->lpszBuffer, (LPCSTR)bstrPropertyValue);
		}
	}

	if (6 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->get_WMode(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenA((LPSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyA(pMessageInfo->lpszBuffer, (LPCSTR)bstrPropertyValue);
		}
	}

	if (7 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->get_WMode(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenA((LPSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyA(pMessageInfo->lpszBuffer, (LPCSTR)bstrPropertyValue);
		}
	}

	if (8 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->get_WMode(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenA((LPSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyA(pMessageInfo->lpszBuffer, (LPCSTR)bstrPropertyValue);
		}
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnGetWModeW(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCGetWModeW* pMessageInfo = reinterpret_cast<SFPCGetWModeW*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->get_WMode(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenW((LPCWSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyW(pMessageInfo->lpszBuffer, (LPCWSTR)bstrPropertyValue);
		}
	}

	if (4 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->get_WMode(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenW((LPCWSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyW(pMessageInfo->lpszBuffer, (LPCWSTR)bstrPropertyValue);
		}
	}

	if (5 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->get_WMode(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenW((LPCWSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyW(pMessageInfo->lpszBuffer, (LPCWSTR)bstrPropertyValue);
		}
	}

	if (6 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->get_WMode(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenW((LPCWSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyW(pMessageInfo->lpszBuffer, (LPCWSTR)bstrPropertyValue);
		}
	}

	if (7 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->get_WMode(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenW((LPCWSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyW(pMessageInfo->lpszBuffer, (LPCWSTR)bstrPropertyValue);
		}
	}

	if (8 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->get_WMode(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenW((LPCWSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyW(pMessageInfo->lpszBuffer, (LPCWSTR)bstrPropertyValue);
		}
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnPutSAlignA(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCPutSAlignA* pMessageInfo = reinterpret_cast<SFPCPutSAlignA*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->put_SAlign(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->put_SAlign(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->put_SAlign(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->put_SAlign(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->put_SAlign(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->put_SAlign(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnPutSAlignW(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCPutSAlignW* pMessageInfo = reinterpret_cast<SFPCPutSAlignW*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->put_SAlign(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->put_SAlign(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->put_SAlign(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->put_SAlign(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->put_SAlign(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->put_SAlign(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnGetSAlignA(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCGetSAlignA* pMessageInfo = reinterpret_cast<SFPCGetSAlignA*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->get_SAlign(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenA((LPSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyA(pMessageInfo->lpszBuffer, (LPCSTR)bstrPropertyValue);
		}
	}

	if (4 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->get_SAlign(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenA((LPSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyA(pMessageInfo->lpszBuffer, (LPCSTR)bstrPropertyValue);
		}
	}

	if (5 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->get_SAlign(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenA((LPSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyA(pMessageInfo->lpszBuffer, (LPCSTR)bstrPropertyValue);
		}
	}

	if (6 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->get_SAlign(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenA((LPSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyA(pMessageInfo->lpszBuffer, (LPCSTR)bstrPropertyValue);
		}
	}

	if (7 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->get_SAlign(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenA((LPSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyA(pMessageInfo->lpszBuffer, (LPCSTR)bstrPropertyValue);
		}
	}

	if (8 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->get_SAlign(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenA((LPSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyA(pMessageInfo->lpszBuffer, (LPCSTR)bstrPropertyValue);
		}
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnGetSAlignW(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCGetSAlignW* pMessageInfo = reinterpret_cast<SFPCGetSAlignW*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->get_SAlign(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenW((LPCWSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyW(pMessageInfo->lpszBuffer, (LPCWSTR)bstrPropertyValue);
		}
	}

	if (4 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->get_SAlign(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenW((LPCWSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyW(pMessageInfo->lpszBuffer, (LPCWSTR)bstrPropertyValue);
		}
	}

	if (5 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->get_SAlign(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenW((LPCWSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyW(pMessageInfo->lpszBuffer, (LPCWSTR)bstrPropertyValue);
		}
	}

	if (6 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->get_SAlign(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenW((LPCWSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyW(pMessageInfo->lpszBuffer, (LPCWSTR)bstrPropertyValue);
		}
	}

	if (7 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->get_SAlign(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenW((LPCWSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyW(pMessageInfo->lpszBuffer, (LPCWSTR)bstrPropertyValue);
		}
	}

	if (8 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->get_SAlign(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenW((LPCWSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyW(pMessageInfo->lpszBuffer, (LPCWSTR)bstrPropertyValue);
		}
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnPutMenu(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCPutMenu* pMessageInfo = reinterpret_cast<SFPCPutMenu*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->put_Menu(pMessageInfo->Menu);
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->put_Menu(pMessageInfo->Menu);
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->put_Menu(pMessageInfo->Menu);
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->put_Menu(pMessageInfo->Menu);
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->put_Menu(pMessageInfo->Menu);
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->put_Menu(pMessageInfo->Menu);
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnGetMenu(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCGetMenu* pMessageInfo = reinterpret_cast<SFPCGetMenu*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->get_Menu(&pMessageInfo->Menu);
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->get_Menu(&pMessageInfo->Menu);
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->get_Menu(&pMessageInfo->Menu);
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->get_Menu(&pMessageInfo->Menu);
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->get_Menu(&pMessageInfo->Menu);
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->get_Menu(&pMessageInfo->Menu);
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnPutBaseA(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCPutBaseA* pMessageInfo = reinterpret_cast<SFPCPutBaseA*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->put_Base(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->put_Base(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->put_Base(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->put_Base(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->put_Base(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->put_Base(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnPutBaseW(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCPutBaseW* pMessageInfo = reinterpret_cast<SFPCPutBaseW*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->put_Base(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->put_Base(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->put_Base(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->put_Base(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->put_Base(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->put_Base(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnGetBaseA(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCGetBaseA* pMessageInfo = reinterpret_cast<SFPCGetBaseA*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->get_Base(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenA((LPSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyA(pMessageInfo->lpszBuffer, (LPCSTR)bstrPropertyValue);
		}
	}

	if (4 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->get_Base(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenA((LPSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyA(pMessageInfo->lpszBuffer, (LPCSTR)bstrPropertyValue);
		}
	}

	if (5 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->get_Base(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenA((LPSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyA(pMessageInfo->lpszBuffer, (LPCSTR)bstrPropertyValue);
		}
	}

	if (6 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->get_Base(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenA((LPSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyA(pMessageInfo->lpszBuffer, (LPCSTR)bstrPropertyValue);
		}
	}

	if (7 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->get_Base(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenA((LPSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyA(pMessageInfo->lpszBuffer, (LPCSTR)bstrPropertyValue);
		}
	}

	if (8 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->get_Base(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenA((LPSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyA(pMessageInfo->lpszBuffer, (LPCSTR)bstrPropertyValue);
		}
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnGetBaseW(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCGetBaseW* pMessageInfo = reinterpret_cast<SFPCGetBaseW*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->get_Base(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenW((LPCWSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyW(pMessageInfo->lpszBuffer, (LPCWSTR)bstrPropertyValue);
		}
	}

	if (4 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->get_Base(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenW((LPCWSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyW(pMessageInfo->lpszBuffer, (LPCWSTR)bstrPropertyValue);
		}
	}

	if (5 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->get_Base(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenW((LPCWSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyW(pMessageInfo->lpszBuffer, (LPCWSTR)bstrPropertyValue);
		}
	}

	if (6 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->get_Base(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenW((LPCWSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyW(pMessageInfo->lpszBuffer, (LPCWSTR)bstrPropertyValue);
		}
	}

	if (7 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->get_Base(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenW((LPCWSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyW(pMessageInfo->lpszBuffer, (LPCWSTR)bstrPropertyValue);
		}
	}

	if (8 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->get_Base(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenW((LPCWSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyW(pMessageInfo->lpszBuffer, (LPCWSTR)bstrPropertyValue);
		}
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnPutScaleA(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCPutScaleA* pMessageInfo = reinterpret_cast<SFPCPutScaleA*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->put_Scale(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->put_Scale(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->put_Scale(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->put_Scale(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->put_Scale(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->put_Scale(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnPutScaleW(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCPutScaleW* pMessageInfo = reinterpret_cast<SFPCPutScaleW*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->put_Scale(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->put_Scale(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->put_Scale(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->put_Scale(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->put_Scale(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->put_Scale(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnGetScaleA(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCGetScaleA* pMessageInfo = reinterpret_cast<SFPCGetScaleA*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->get_Scale(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenA((LPSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyA(pMessageInfo->lpszBuffer, (LPCSTR)bstrPropertyValue);
		}
	}

	if (4 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->get_Scale(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenA((LPSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyA(pMessageInfo->lpszBuffer, (LPCSTR)bstrPropertyValue);
		}
	}

	if (5 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->get_Scale(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenA((LPSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyA(pMessageInfo->lpszBuffer, (LPCSTR)bstrPropertyValue);
		}
	}

	if (6 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->get_Scale(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenA((LPSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyA(pMessageInfo->lpszBuffer, (LPCSTR)bstrPropertyValue);
		}
	}

	if (7 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->get_Scale(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenA((LPSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyA(pMessageInfo->lpszBuffer, (LPCSTR)bstrPropertyValue);
		}
	}

	if (8 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->get_Scale(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenA((LPSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyA(pMessageInfo->lpszBuffer, (LPCSTR)bstrPropertyValue);
		}
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnGetScaleW(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCGetScaleW* pMessageInfo = reinterpret_cast<SFPCGetScaleW*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->get_Scale(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenW((LPCWSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyW(pMessageInfo->lpszBuffer, (LPCWSTR)bstrPropertyValue);
		}
	}

	if (4 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->get_Scale(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenW((LPCWSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyW(pMessageInfo->lpszBuffer, (LPCWSTR)bstrPropertyValue);
		}
	}

	if (5 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->get_Scale(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenW((LPCWSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyW(pMessageInfo->lpszBuffer, (LPCWSTR)bstrPropertyValue);
		}
	}

	if (6 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->get_Scale(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenW((LPCWSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyW(pMessageInfo->lpszBuffer, (LPCWSTR)bstrPropertyValue);
		}
	}

	if (7 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->get_Scale(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenW((LPCWSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyW(pMessageInfo->lpszBuffer, (LPCWSTR)bstrPropertyValue);
		}
	}

	if (8 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->get_Scale(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenW((LPCWSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyW(pMessageInfo->lpszBuffer, (LPCWSTR)bstrPropertyValue);
		}
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnPutDeviceFont(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCPutDeviceFont* pMessageInfo = reinterpret_cast<SFPCPutDeviceFont*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->put_DeviceFont(pMessageInfo->DeviceFont);
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->put_DeviceFont(pMessageInfo->DeviceFont);
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->put_DeviceFont(pMessageInfo->DeviceFont);
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->put_DeviceFont(pMessageInfo->DeviceFont);
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->put_DeviceFont(pMessageInfo->DeviceFont);
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->put_DeviceFont(pMessageInfo->DeviceFont);
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnGetDeviceFont(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCGetDeviceFont* pMessageInfo = reinterpret_cast<SFPCGetDeviceFont*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->get_DeviceFont(&pMessageInfo->DeviceFont);
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->get_DeviceFont(&pMessageInfo->DeviceFont);
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->get_DeviceFont(&pMessageInfo->DeviceFont);
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->get_DeviceFont(&pMessageInfo->DeviceFont);
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->get_DeviceFont(&pMessageInfo->DeviceFont);
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->get_DeviceFont(&pMessageInfo->DeviceFont);
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnPutEmbedMovie(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCPutEmbedMovie* pMessageInfo = reinterpret_cast<SFPCPutEmbedMovie*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->put_EmbedMovie(pMessageInfo->EmbedMovie);
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->put_EmbedMovie(pMessageInfo->EmbedMovie);
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->put_EmbedMovie(pMessageInfo->EmbedMovie);
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->put_EmbedMovie(pMessageInfo->EmbedMovie);
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->put_EmbedMovie(pMessageInfo->EmbedMovie);
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->put_EmbedMovie(pMessageInfo->EmbedMovie);
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnGetEmbedMovie(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCGetEmbedMovie* pMessageInfo = reinterpret_cast<SFPCGetEmbedMovie*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->get_EmbedMovie(&pMessageInfo->EmbedMovie);
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->get_EmbedMovie(&pMessageInfo->EmbedMovie);
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->get_EmbedMovie(&pMessageInfo->EmbedMovie);
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->get_EmbedMovie(&pMessageInfo->EmbedMovie);
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->get_EmbedMovie(&pMessageInfo->EmbedMovie);
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->get_EmbedMovie(&pMessageInfo->EmbedMovie);
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnPutBGColorA(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCPutBGColorA* pMessageInfo = reinterpret_cast<SFPCPutBGColorA*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->put_BGColor(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->put_BGColor(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->put_BGColor(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->put_BGColor(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->put_BGColor(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->put_BGColor(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnPutBGColorW(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCPutBGColorW* pMessageInfo = reinterpret_cast<SFPCPutBGColorW*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->put_BGColor(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->put_BGColor(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->put_BGColor(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->put_BGColor(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->put_BGColor(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->put_BGColor(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnGetBGColorA(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCGetBGColorA* pMessageInfo = reinterpret_cast<SFPCGetBGColorA*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->get_BGColor(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenA((LPSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyA(pMessageInfo->lpszBuffer, (LPCSTR)bstrPropertyValue);
		}
	}

	if (4 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->get_BGColor(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenA((LPSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyA(pMessageInfo->lpszBuffer, (LPCSTR)bstrPropertyValue);
		}
	}

	if (5 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->get_BGColor(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenA((LPSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyA(pMessageInfo->lpszBuffer, (LPCSTR)bstrPropertyValue);
		}
	}

	if (6 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->get_BGColor(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenA((LPSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyA(pMessageInfo->lpszBuffer, (LPCSTR)bstrPropertyValue);
		}
	}

	if (7 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->get_BGColor(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenA((LPSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyA(pMessageInfo->lpszBuffer, (LPCSTR)bstrPropertyValue);
		}
	}

	if (8 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->get_BGColor(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenA((LPSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyA(pMessageInfo->lpszBuffer, (LPCSTR)bstrPropertyValue);
		}
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnGetBGColorW(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCGetBGColorW* pMessageInfo = reinterpret_cast<SFPCGetBGColorW*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->get_BGColor(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenW((LPCWSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyW(pMessageInfo->lpszBuffer, (LPCWSTR)bstrPropertyValue);
		}
	}

	if (4 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->get_BGColor(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenW((LPCWSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyW(pMessageInfo->lpszBuffer, (LPCWSTR)bstrPropertyValue);
		}
	}

	if (5 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->get_BGColor(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenW((LPCWSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyW(pMessageInfo->lpszBuffer, (LPCWSTR)bstrPropertyValue);
		}
	}

	if (6 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->get_BGColor(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenW((LPCWSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyW(pMessageInfo->lpszBuffer, (LPCWSTR)bstrPropertyValue);
		}
	}

	if (7 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->get_BGColor(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenW((LPCWSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyW(pMessageInfo->lpszBuffer, (LPCWSTR)bstrPropertyValue);
		}
	}

	if (8 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->get_BGColor(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenW((LPCWSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyW(pMessageInfo->lpszBuffer, (LPCWSTR)bstrPropertyValue);
		}
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnPutQuality2A(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCPutQuality2A* pMessageInfo = reinterpret_cast<SFPCPutQuality2A*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->put_Quality2(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->put_Quality2(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->put_Quality2(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->put_Quality2(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->put_Quality2(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->put_Quality2(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnPutQuality2W(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCPutQuality2W* pMessageInfo = reinterpret_cast<SFPCPutQuality2W*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->put_Quality2(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->put_Quality2(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->put_Quality2(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (6 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->put_Quality2(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->put_Quality2(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->put_Quality2(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnGetQuality2A(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCGetQuality2A* pMessageInfo = reinterpret_cast<SFPCGetQuality2A*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->get_Quality2(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenA((LPSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyA(pMessageInfo->lpszBuffer, (LPCSTR)bstrPropertyValue);
		}
	}

	if (4 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->get_Quality2(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenA((LPSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyA(pMessageInfo->lpszBuffer, (LPCSTR)bstrPropertyValue);
		}
	}

	if (5 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->get_Quality2(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenA((LPSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyA(pMessageInfo->lpszBuffer, (LPCSTR)bstrPropertyValue);
		}
	}

	if (6 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->get_Quality2(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenA((LPSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyA(pMessageInfo->lpszBuffer, (LPCSTR)bstrPropertyValue);
		}
	}

	if (7 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->get_Quality2(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenA((LPSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyA(pMessageInfo->lpszBuffer, (LPCSTR)bstrPropertyValue);
		}
	}

	if (8 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->get_Quality2(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenA((LPSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyA(pMessageInfo->lpszBuffer, (LPCSTR)bstrPropertyValue);
		}
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnGetQuality2W(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCGetQuality2W* pMessageInfo = reinterpret_cast<SFPCGetQuality2W*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (3 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(3)->get_Quality2(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenW((LPCWSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyW(pMessageInfo->lpszBuffer, (LPCWSTR)bstrPropertyValue);
		}
	}

	if (4 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->get_Quality2(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenW((LPCWSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyW(pMessageInfo->lpszBuffer, (LPCWSTR)bstrPropertyValue);
		}
	}

	if (5 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->get_Quality2(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenW((LPCWSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyW(pMessageInfo->lpszBuffer, (LPCWSTR)bstrPropertyValue);
		}
	}

	if (6 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(6)->get_Quality2(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenW((LPCWSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyW(pMessageInfo->lpszBuffer, (LPCWSTR)bstrPropertyValue);
		}
	}

	if (7 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->get_Quality2(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenW((LPCWSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyW(pMessageInfo->lpszBuffer, (LPCWSTR)bstrPropertyValue);
		}
	}

	if (8 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->get_Quality2(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenW((LPCWSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyW(pMessageInfo->lpszBuffer, (LPCWSTR)bstrPropertyValue);
		}
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnPutSWRemoteA(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCPutSWRemoteA* pMessageInfo = reinterpret_cast<SFPCPutSWRemoteA*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->put_SWRemote(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->put_SWRemote(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->put_SWRemote(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->put_SWRemote(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnPutSWRemoteW(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCPutSWRemoteW* pMessageInfo = reinterpret_cast<SFPCPutSWRemoteW*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (4 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->put_SWRemote(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->put_SWRemote(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->put_SWRemote(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->put_SWRemote(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnGetSWRemoteA(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCGetSWRemoteA* pMessageInfo = reinterpret_cast<SFPCGetSWRemoteA*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (4 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->get_SWRemote(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenA((LPSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyA(pMessageInfo->lpszBuffer, (LPCSTR)bstrPropertyValue);
		}
	}

	if (5 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->get_SWRemote(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenA((LPSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyA(pMessageInfo->lpszBuffer, (LPCSTR)bstrPropertyValue);
		}
	}

	if (7 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->get_SWRemote(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenA((LPSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyA(pMessageInfo->lpszBuffer, (LPCSTR)bstrPropertyValue);
		}
	}

	if (8 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->get_SWRemote(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenA((LPSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyA(pMessageInfo->lpszBuffer, (LPCSTR)bstrPropertyValue);
		}
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnGetSWRemoteW(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCGetSWRemoteW* pMessageInfo = reinterpret_cast<SFPCGetSWRemoteW*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (4 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(4)->get_SWRemote(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenW((LPCWSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyW(pMessageInfo->lpszBuffer, (LPCWSTR)bstrPropertyValue);
		}
	}

	if (5 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->get_SWRemote(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenW((LPCWSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyW(pMessageInfo->lpszBuffer, (LPCWSTR)bstrPropertyValue);
		}
	}

	if (7 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->get_SWRemote(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenW((LPCWSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyW(pMessageInfo->lpszBuffer, (LPCWSTR)bstrPropertyValue);
		}
	}

	if (8 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->get_SWRemote(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenW((LPCWSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyW(pMessageInfo->lpszBuffer, (LPCWSTR)bstrPropertyValue);
		}
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnPutStackingA(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCPutStackingA* pMessageInfo = reinterpret_cast<SFPCPutStackingA*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->put_Stacking(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnPutStackingW(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCPutStackingW* pMessageInfo = reinterpret_cast<SFPCPutStackingW*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (5 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->put_Stacking(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnGetStackingA(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCGetStackingA* pMessageInfo = reinterpret_cast<SFPCGetStackingA*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (5 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->get_Stacking(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenA((LPSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyA(pMessageInfo->lpszBuffer, (LPCSTR)bstrPropertyValue);
		}
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnGetStackingW(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCGetStackingW* pMessageInfo = reinterpret_cast<SFPCGetStackingW*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (5 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(5)->get_Stacking(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenW((LPCWSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyW(pMessageInfo->lpszBuffer, (LPCWSTR)bstrPropertyValue);
		}
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnPutFlashVarsA(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCPutFlashVarsA* pMessageInfo = reinterpret_cast<SFPCPutFlashVarsA*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->put_FlashVars(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->put_FlashVars(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnPutFlashVarsW(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCPutFlashVarsW* pMessageInfo = reinterpret_cast<SFPCPutFlashVarsW*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->put_FlashVars(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->put_FlashVars(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnGetFlashVarsA(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCGetFlashVarsA* pMessageInfo = reinterpret_cast<SFPCGetFlashVarsA*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (7 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->get_FlashVars(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenA((LPSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyA(pMessageInfo->lpszBuffer, (LPCSTR)bstrPropertyValue);
		}
	}

	if (8 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->get_FlashVars(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenA((LPSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyA(pMessageInfo->lpszBuffer, (LPCSTR)bstrPropertyValue);
		}
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnGetFlashVarsW(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCGetFlashVarsW* pMessageInfo = reinterpret_cast<SFPCGetFlashVarsW*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (7 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->get_FlashVars(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenW((LPCWSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyW(pMessageInfo->lpszBuffer, (LPCWSTR)bstrPropertyValue);
		}
	}

	if (8 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->get_FlashVars(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenW((LPCWSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyW(pMessageInfo->lpszBuffer, (LPCWSTR)bstrPropertyValue);
		}
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnPutAllowScriptAccessA(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCPutAllowScriptAccessA* pMessageInfo = reinterpret_cast<SFPCPutAllowScriptAccessA*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->put_AllowScriptAccess(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->put_AllowScriptAccess(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnPutAllowScriptAccessW(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCPutAllowScriptAccessW* pMessageInfo = reinterpret_cast<SFPCPutAllowScriptAccessW*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->put_AllowScriptAccess(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->put_AllowScriptAccess(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnGetAllowScriptAccessA(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCGetAllowScriptAccessA* pMessageInfo = reinterpret_cast<SFPCGetAllowScriptAccessA*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (7 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->get_AllowScriptAccess(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenA((LPSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyA(pMessageInfo->lpszBuffer, (LPCSTR)bstrPropertyValue);
		}
	}

	if (8 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->get_AllowScriptAccess(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenA((LPSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyA(pMessageInfo->lpszBuffer, (LPCSTR)bstrPropertyValue);
		}
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnGetAllowScriptAccessW(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCGetAllowScriptAccessW* pMessageInfo = reinterpret_cast<SFPCGetAllowScriptAccessW*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (7 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->get_AllowScriptAccess(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenW((LPCWSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyW(pMessageInfo->lpszBuffer, (LPCWSTR)bstrPropertyValue);
		}
	}

	if (8 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->get_AllowScriptAccess(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenW((LPCWSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyW(pMessageInfo->lpszBuffer, (LPCWSTR)bstrPropertyValue);
		}
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnPutMovieDataA(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCPutMovieDataA* pMessageInfo = reinterpret_cast<SFPCPutMovieDataA*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->put_MovieData(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->put_MovieData(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnPutMovieDataW(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCPutMovieDataW* pMessageInfo = reinterpret_cast<SFPCPutMovieDataW*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (7 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->put_MovieData(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}

	if (8 == GetFlashVersion())
	{
		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->put_MovieData(_bstr_t(pMessageInfo->lpszBuffer).GetBSTR());
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnGetMovieDataA(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCGetMovieDataA* pMessageInfo = reinterpret_cast<SFPCGetMovieDataA*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (7 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->get_MovieData(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenA((LPSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyA(pMessageInfo->lpszBuffer, (LPCSTR)bstrPropertyValue);
		}
	}

	if (8 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->get_MovieData(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenA((LPSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyA(pMessageInfo->lpszBuffer, (LPCSTR)bstrPropertyValue);
		}
	}


	return 1;
}

LRESULT CFlashPlayerControlWindow::OnGetMovieDataW(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SFPCGetMovieDataW* pMessageInfo = reinterpret_cast<SFPCGetMovieDataW*>(lParam);

	pMessageInfo->hr = E_FAIL;

	if (7 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(7)->get_MovieData(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenW((LPCWSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyW(pMessageInfo->lpszBuffer, (LPCWSTR)bstrPropertyValue);
		}
	}

	if (8 == GetFlashVersion())
	{
		_bstr_t bstrPropertyValue;

		pMessageInfo->hr = GET_VERSION_INTERFACE_POINTER(8)->get_MovieData(bstrPropertyValue.GetAddress());

		if (SUCCEEDED(pMessageInfo->hr))
		{
			pMessageInfo->dwBufferSize = lstrlenW((LPCWSTR)bstrPropertyValue) + 1;

			if (NULL != pMessageInfo->lpszBuffer)
				lstrcpyW(pMessageInfo->lpszBuffer, (LPCWSTR)bstrPropertyValue);
		}
	}


	return 1;
}

