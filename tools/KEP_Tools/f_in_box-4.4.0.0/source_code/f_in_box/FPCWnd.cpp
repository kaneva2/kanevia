#include "stdafx.h"
#include "FPCWnd.h"
#include "EnumUnknown.h"
#include "MemManager.h"
#include "UrlMonHelper.h"
#include "MyMoniker.h"
#include "TimerServiceImpl.h"

using namespace f_in_box;

#define MSG_HANDLER(message, handler)     \
    if (message == uMsg)    \
    {   \
        BOOL bIsHandled = TRUE;   \
        \
        lResult = handler(uMsg, wParam, lParam, bIsHandled); \
        \
        if (bIsHandled) \
		{	\
			bHandled = TRUE;	\
            return lResult; \
		}	\
    }

#define MSG_RANGE_HANDLER(from_message, to_message, handler)    \
    if (from_message <= uMsg && uMsg <= to_message)    \
    {   \
        BOOL bIsHandled = TRUE;   \
        \
        lResult = handler(uMsg, wParam, lParam, bIsHandled); \
        \
        if (bIsHandled) \
		{	\
			bHandled = TRUE;	\
            return lResult; \
		}	\
    }

#define DEF_FLASH_COLOR_TO_RGB(flash_color)		( ((flash_color) & 0xff000000) | RGB( GetBValue((flash_color)), GetGValue((flash_color)), GetRValue((flash_color)) ) )

#define DEF_TIMER_ID_NC_HITTEST (0x123456)

namespace f_in_box
{
	void PixelToHiMetric(const SIZEL* lpSizeInPix, LPSIZEL lpSizeInHiMetric);
	void HiMetricToPixel(const SIZEL * lpSizeInHiMetric, LPSIZEL lpSizeInPix);

	static const IID IID_IDirectDraw3 = 
		{ 0x618f8ad4, 0x8b7a, 0x11d0, 0x8f, 0xcc, 0x0, 0xc0, 0x4f, 0xd9, 0x18, 0x9d };

	static const IID IID_IDirectDraw4 = 
		{ 0x9c59509a, 0x39bd, 0x11d1, 0x8c, 0x4a, 0x00, 0xc0, 0x4f, 0xd9, 0x30, 0xc5 };

	static const IID IID_IDirectDraw2 = 
		{ 0xB3A6F3E0, 0x2B43, 0x11CF, 0xA2, 0xDE, 0x00, 0xAA, 0x00, 0xB9, 0x33, 0x56 };

	static const IID IID_IDirectDraw7 = 
		{ 0x15e65ec0, 0x3b9c, 0x11d2, 0xb9, 0x2f, 0x00, 0x60, 0x97, 0x97, 0xea, 0x5b };

	static GUID CLSID_DirectDrawFactory = 
		{ 0x4fd2a832, 0x86c8, 0x11d0, 0x8f, 0xca, 0x0, 0xc0, 0x4f, 0xd9, 0x18, 0x9d };

	static IID IID_IDirectDrawFactory = 
		{ 0x4fd2a833, 0x86c8, 0x11d0, 0x8f, 0xca, 0x0, 0xc0, 0x4f, 0xd9, 0x18, 0x9d };

	DECLARE_INTERFACE_(IDirectDrawFactory, IUnknown)
	{
		/*** IUnknown methods ***/
		STDMETHOD(QueryInterface) (THIS_ REFIID riid, LPVOID FAR * ppvObj) PURE;
		STDMETHOD_(ULONG,AddRef) (THIS)  PURE;
		STDMETHOD_(ULONG,Release) (THIS) PURE;
		/*** IDirectDrawFactory methods ***/
		STDMETHOD(CreateDirectDraw) (THIS_ GUID * pGUID, HWND hWnd, DWORD dwCoopLevelFlags, DWORD dwReserved, IUnknown *pUnkOuter, IDirectDraw **ppDirectDraw) PURE;
		STDMETHOD(DirectDrawEnumerate) (THIS_ LPDDENUMCALLBACK lpCallback, LPVOID lpContext) PURE;
	};
}

CFPCWnd::CFPCWnd(HWND hWnd, CFlashOCXLoader* pFlashOCXLoader, LPCREATESTRUCT pCreateStruct) : 





    m_dwStartTime(GetTickCount()), 
    m_uMsg__SetContext(RegisterWindowMessageA(DEF_PRIVATE_MESSAGE__SET_CONTEXT)), 



    m_nRefCount(1), 
    m_pFlashOCXLoader(pFlashOCXLoader), 
    m_bMDIApp(FALSE), 
    m_bCanWindowlessActivate(TRUE), 
    m_bCapture(FALSE), 
    m_bHaveFocus(FALSE), 
    m_bDCReleased(TRUE), 
    m_hDCScreen(NULL), 
    m_bLocked(FALSE), 
    m_bSubcribedToEvents(FALSE), 
    m_bTransparent(FALSE), 
    m_bInPlaceActive(FALSE), 
    m_pEventListener(NULL), 
    m_hAccel(NULL), 
    m_clrBackground(RGB(0xff, 0x00, 0x00)), 
    m_hBmpWhite(NULL), 
    m_hBmpBlack(NULL), 
    m_pBitsWhite(NULL), 
    m_pBitsBlack(NULL), 
    m_hBmp(NULL), 
    m_pBits(NULL), 
    m_nOverallOpaque(255), 
    m_bReleaseAll(FALSE), 
    m_bStandardMenu(FALSE), 
	m_uMsg__PRIVATE_MESSAGE__SET_EVENT_LISTENER(DEF_PRIVATE_MESSAGE__SET_EVENT_LISTENER), 
	m_uMsg__PRIVATE_MESSAGE__CALL_LOAD_EXTERNAL_RESOURCE(DEF_PRIVATE_MESSAGE__CALL_LOAD_EXTERNAL_RESOURCE), 
	m_uMsg__DEF_PRIVATE_MESSAGE__GET_OBJECT_PTR(DEF_PRIVATE_MESSAGE__GET_OBJECT_PTR), 
	m_bAttachToParent(0 != (pCreateStruct->style & FPCS_ATTACH_TO_PARENT_INTERNAL)), 
	m_TimerId__NtHitTest(0), 
	m_hdcDirectDrawSurface(NULL)
{
    m_size.cx = m_size.cy = 0;

	Subclass(hWnd, TRUE);

    m_FlashAXWnd.SetStandardMenu(m_bStandardMenu);
}

CFPCWnd::~CFPCWnd()
{
    ReleaseAll();

    if (NULL != m_hBmpWhite)
    {
        DeleteObject(m_hBmpWhite);
        m_hBmpWhite = NULL;
    }

    if (NULL != m_hBmpBlack)
    {
        DeleteObject(m_hBmpBlack);
        m_hBmpBlack = NULL;
    }

    if (NULL != m_hBmp)
    {
        DeleteObject(m_hBmp);
        m_hBmp = NULL;
    }
}

LRESULT CFPCWnd::OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	// Tests for Flash 11 black blackground issue
	m_pFlashOCXLoader->TestCanFlashUseGPUAcceleration();

	LPCREATESTRUCT lpCreateStruct = (LPCREATESTRUCT)lParam;

    if ((lpCreateStruct->dwExStyle & DEF_WS_EX_LAYERED) || 
        ((lpCreateStruct->style & FPCS_TRANSPARENT) || (lpCreateStruct->style & FPCS_OPAQUE)))
        // Transparent mode
    {
        m_bTransparent = TRUE;
    }
    else
    {
        m_bTransparent = FALSE;
    }

    HRESULT (STDAPICALLTYPE *pDllGetClassObject)(REFCLSID rclsid, REFIID riid, LPVOID * ppv);
    (FARPROC&)pDllGetClassObject = m_pFlashOCXLoader->GetProcAddress("DllGetClassObject");

	if (NULL == pDllGetClassObject)
		return 0;

    f_in_box::com_helpers::CComPtr<IClassFactory> pClassFactory;

    HRESULT hr = pDllGetClassObject(f_in_box::flash::GetFlashObjectGUID(), 
                                    IID_IClassFactory, 
							        (void**)&pClassFactory);

	if (S_OK != hr)
        return 0;

    hr = pClassFactory->CreateInstance(NULL, IID_IUnknown, (void**)&m_pUnknown);

	if (S_OK != hr)
        return 0;

    m_pUnknown->QueryInterface(f_in_box::flash::v3::IShockwaveFlash::GetGUID(), 
                               (void**)&m_pShockwaveFlash3);

    m_pUnknown->QueryInterface(f_in_box::flash::v4::IShockwaveFlash::GetGUID(), 
                               (void**)&m_pShockwaveFlash4);

    m_pUnknown->QueryInterface(f_in_box::flash::v5::IShockwaveFlash::GetGUID(), 
                               (void**)&m_pShockwaveFlash5);

    m_pUnknown->QueryInterface(f_in_box::flash::v6::IShockwaveFlash::GetGUID(), 
                               (void**)&m_pShockwaveFlash6);

    m_pUnknown->QueryInterface(f_in_box::flash::v7::IShockwaveFlash::GetGUID(), 
                               (void**)&m_pShockwaveFlash7);

    m_pUnknown->QueryInterface(f_in_box::flash::v8::IShockwaveFlash::GetGUID(), 
                               (void**)&m_pShockwaveFlash8);

	if (lpCreateStruct->style & FPCS_OPAQUE)
        m_pShockwaveFlash3->put_WMode(L"Opaque");
	else if (m_bTransparent)
        m_pShockwaveFlash3->put_WMode(L"Transparent");
    else
        m_pShockwaveFlash3->put_WMode(L"Window");

    // Events
    {
        f_in_box::com_helpers::CComPtr<IConnectionPointContainer> pFlashConnectionPointContainer;
        m_pUnknown->QueryInterface(IID_IConnectionPointContainer, (void**)&pFlashConnectionPointContainer);

        f_in_box::com_helpers::CComPtr<IConnectionPoint> pFlashConnectionPoint;

        hr = pFlashConnectionPointContainer->FindConnectionPoint(f_in_box::flash::IShockwaveFlashEvents::GetGUID(), 
                                                                 &pFlashConnectionPoint);

        f_in_box::com_helpers::CComPtr<IUnknown> pUnknown;
        hr = QueryInterface(IID_IUnknown, (void**)&pUnknown);

        hr = pFlashConnectionPoint->Advise(pUnknown, &m_dwCookie);

        if (S_OK == hr)
            m_bSubcribedToEvents = TRUE;
    }

    m_pUnknown->QueryInterface(IID_IOleObject, (void**)&m_pOleObject);

    if (m_pOleObject)
    {
        m_pOleObject->GetMiscStatus(DVASPECT_CONTENT, &m_dwMiscStatus);

        hr = m_pOleObject->SetClientSite(this);

        m_dwViewObjectType = 0;

        hr = m_pOleObject->QueryInterface(IID_IViewObject, (void**)&m_pViewObject);

        hr = m_pOleObject->QueryInterface(IID_IViewObjectEx, (void**)&m_pViewObjectEx);

        if (FAILED(hr))
        {
            hr = m_pOleObject->QueryInterface(IID_IViewObject2, (void**)&m_pViewObject);

            if (SUCCEEDED(hr))
                m_dwViewObjectType = 3;
        }
        else
            m_dwViewObjectType = 7;

        if (FAILED(hr))
        {
            hr = m_pOleObject->QueryInterface(IID_IViewObject, (void**)&m_pViewObject);
    
            if (SUCCEEDED(hr))
                m_dwViewObjectType = 1;
        }

        if (0 == (m_dwMiscStatus & OLEMISC_INVISIBLEATRUNTIME))
        {
			if (m_bAttachToParent)
			{
				m_rcPos.left = lpCreateStruct->x;
				m_rcPos.top = lpCreateStruct->y;
				m_rcPos.right = m_rcPos.left + lpCreateStruct->cx;
				m_rcPos.bottom = m_rcPos.top + lpCreateStruct->cy;
			}
			else
				GetClientRect(m_hWnd, &m_rcPos);

            m_pxSize.cx = m_rcPos.right - m_rcPos.left;
            m_pxSize.cy = m_rcPos.bottom - m_rcPos.top;
            PixelToHiMetric(&m_pxSize, &m_hmSize);
            m_pOleObject->SetExtent(DVASPECT_CONTENT, &m_hmSize);
            m_pOleObject->GetExtent(DVASPECT_CONTENT, &m_hmSize);
            HiMetricToPixel(&m_hmSize, &m_pxSize);
            m_rcPos.right = m_rcPos.left + m_pxSize.cx;
            m_rcPos.bottom = m_rcPos.top + m_pxSize.cy;

            hr = m_pOleObject->DoVerb(OLEIVERB_INPLACEACTIVATE, 
                                      NULL, 
                                      this, 
                                      0, 
                                      GetControlOwner(), 
                                      &m_rcPos);

            RedrawWindow(GetControlOwner(), 
                         NULL, 
                         NULL, 
                         RDW_INVALIDATE | RDW_UPDATENOW | RDW_ERASE | RDW_INTERNALPAINT | RDW_FRAME);

			// Try to create IDirectDrawFactory
			f_in_box::com_helpers::CComPtr<IDirectDrawFactory> pDirectDrawFactory;
			hr = CoCreateInstance(CLSID_DirectDrawFactory, NULL, CLSCTX_INPROC_SERVER, IID_IDirectDrawFactory, (void**)&pDirectDrawFactory);

			// [Kaneva] DirectDraw usage is not thread safe. Behave as though there is no DirectDraw available so f_in_box can safely be used in multiple threads simultaneously.
			if (false && S_OK == hr)
			{
				// Flash 11 has a lot problems with hardware acceleration currently on some video cards, so we
				// check for this above (see TestCanFlashUseGPUAcceleration)
				f_in_box::com_helpers::CComPtr<IDirectDraw> pDirectDraw;

				// If we are inside the test of GPU acceleration, then we have to test GPU acceleration
				hr = pDirectDrawFactory->CreateDirectDraw(
					m_pFlashOCXLoader->WithinGPUAccelerationTest() || m_pFlashOCXLoader->CanFlashUseGPUAcceleration() ? (GUID*)DDCREATE_HARDWAREONLY : (GUID*)DDCREATE_EMULATIONONLY, 
					::GetDesktopWindow(), 
					DDSCL_NORMAL, 
					0, 
					NULL, 
					&pDirectDraw);

				if (S_OK != hr)
					// May be hardware acceleration is not available at all
				{
					hr = pDirectDrawFactory->CreateDirectDraw(
						(GUID*)DDCREATE_EMULATIONONLY, 
						::GetDesktopWindow(), 
						DDSCL_NORMAL, 
						0, 
						NULL, 
						&pDirectDraw);
				}

				if (S_OK == hr)
				{
					hr = pDirectDraw->QueryInterface(IID_IDirectDraw2, (void**)&m_pDirectDraw);
				}
			}
		}
    }

    f_in_box::com_helpers::CComPtr<IObjectWithSite> pSite;

    m_pUnknown->QueryInterface(IID_IObjectWithSite, (void**)&pSite);

    if (pSite)
        pSite->SetSite((IOleClientSite*)this);

    if (!m_bTransparent)
    {
        f_in_box::com_helpers::CComPtr<IOleWindow> pOleWindow;
        m_pUnknown->QueryInterface(IID_IOleWindow, (void**)&pOleWindow);
        HWND hWnd__AX;
        pOleWindow->GetWindow(&hWnd__AX);

        m_FlashAXWnd.Subclass(hWnd__AX);
    }

	if (m_bTransparent && m_bAttachToParent)
		m_TimerId__NtHitTest = SetTimer(m_hWnd, DEF_TIMER_ID_NC_HITTEST, 1, NULL);

	Start();

    return 1;
}

HRESULT STDMETHODCALLTYPE CFPCWnd::QueryInterface(REFIID riid, void** ppvObject)
{
    void* pObject = NULL;

#define DEF_BEGIN_INTERFACE_TABLE if (f_in_box::com_helpers::IsEqualGUID(IID_IUnknown, riid)) { pObject = (IUnknown*)(IOleClientSite*)this; } else
#define DEF_SUPPORT_INTERFACE(interface_class) if (f_in_box::com_helpers::IsEqualGUID(IID_##interface_class, riid)) { (interface_class*&)pObject = this; } else
#define DEF_SUPPORT_INTERFACE_(interface_class, interface_base_class) if (f_in_box::com_helpers::IsEqualGUID(IID_##interface_class, riid)) { pObject = (interface_class*)(interface_base_class*)this; } else
#define DEF_SUPPORT_INTERFACE2(supported_iid, interface_class) if (f_in_box::com_helpers::IsEqualGUID(supported_iid, riid)) { pObject = (interface_class*)this; } else
#define DEF_END_INTERFACE_TABLE              { }

    DEF_BEGIN_INTERFACE_TABLE
        DEF_SUPPORT_INTERFACE(IOleClientSite)
        DEF_SUPPORT_INTERFACE(IOleInPlaceSiteWindowless)
        DEF_SUPPORT_INTERFACE(IOleInPlaceSiteEx)
        DEF_SUPPORT_INTERFACE(IOleInPlaceSite)
        DEF_SUPPORT_INTERFACE_(IOleWindow, IOleInPlaceSite)
        DEF_SUPPORT_INTERFACE(IOleControlSite)
        DEF_SUPPORT_INTERFACE(IOleContainer)
        DEF_SUPPORT_INTERFACE(IObjectWithSite)
        DEF_SUPPORT_INTERFACE(IOleInPlaceFrame)
        DEF_SUPPORT_INTERFACE(IDispatch)
		DEF_SUPPORT_INTERFACE(IOleInPlaceUIWindow)
		DEF_SUPPORT_INTERFACE(IServiceProvider)
        DEF_SUPPORT_INTERFACE2(f_in_box::flash::IShockwaveFlashEvents::GetGUID(), f_in_box::flash::IShockwaveFlashEvents)
    DEF_END_INTERFACE_TABLE

#undef DEF_BEGIN_INTERFACE_TABLE
#undef DEF_SUPPORT_INTERFACE
#undef DEF_END_INTERFACE_TABLE

    HRESULT hr;

    *ppvObject = pObject;

    if (NULL != pObject)
    {
        AddRef();
        hr = S_OK;
    }
    else
        hr = E_NOINTERFACE;

    return hr;
}

ULONG STDMETHODCALLTYPE CFPCWnd::AddRef()
{
    return ++m_nRefCount;
}

ULONG STDMETHODCALLTYPE CFPCWnd::Release()
{
    return --m_nRefCount;
}

HRESULT STDMETHODCALLTYPE CFPCWnd::SaveObject()
{
    return E_NOTIMPL;
}

HRESULT STDMETHODCALLTYPE CFPCWnd::GetMoniker(DWORD dwAssign, DWORD dwWhichMoniker, IMoniker** ppmk)
{
    return E_NOTIMPL;
}

HRESULT STDMETHODCALLTYPE CFPCWnd::GetContainer(IOleContainer** ppContainer)
{
    return QueryInterface(IID_IOleContainer, (void**)ppContainer);
}

HRESULT STDMETHODCALLTYPE CFPCWnd::ShowObject()
{
    HDC hDC = ::GetDC(GetControlOwner());

    if (NULL == hDC)
        return E_FAIL;

    if (m_pViewObject)
	{
		Draw(hDC, &m_rcPos);
	}
    
    ::ReleaseDC(GetControlOwner(), hDC);

    return S_OK;
}

HRESULT STDMETHODCALLTYPE CFPCWnd::OnShowWindow(BOOL fShow)
{
    return E_NOTIMPL;
}

HRESULT STDMETHODCALLTYPE CFPCWnd::RequestNewObjectLayout()
{
    return E_NOTIMPL;
}

HRESULT STDMETHODCALLTYPE CFPCWnd::GetWindow(HWND* phwnd)
{
    *phwnd = GetControlOwner();

    return S_OK;
}

HRESULT STDMETHODCALLTYPE CFPCWnd::ContextSensitiveHelp(BOOL fEnterMode)
{
    return E_NOTIMPL;
}

HRESULT STDMETHODCALLTYPE CFPCWnd::CanInPlaceActivate()
{
    return S_OK;
}

HRESULT STDMETHODCALLTYPE CFPCWnd::OnInPlaceActivate()
{
    // should only be called once the first time control is inplace-activated
    ASSERT(m_bInPlaceActive == FALSE);
    ASSERT(m_pOleInPlaceObjectWindowless == NULL);

    m_bInPlaceActive = TRUE;
    OleLockRunning(m_pOleObject, TRUE, FALSE);
    m_bWindowless = FALSE;
    m_pOleObject->QueryInterface(IID_IOleInPlaceObject, (void**)&m_pOleInPlaceObjectWindowless);

    return S_OK;
}

HRESULT STDMETHODCALLTYPE CFPCWnd::OnUIActivate()
{
    m_bUIActive = TRUE;

    return S_OK;
}

HRESULT STDMETHODCALLTYPE CFPCWnd::GetWindowContext( 
                IOleInPlaceFrame** ppFrame, 
                IOleInPlaceUIWindow** ppDoc, 
                LPRECT lprcPosRect, 
                LPRECT lprcClipRect, 
                LPOLEINPLACEFRAMEINFO lpFrameInfo)
{
    if (NULL != ppFrame)
        *ppFrame = NULL;

    if (NULL != ppDoc)
        *ppDoc = NULL;

    if (NULL == ppFrame || NULL == ppDoc || NULL == lprcPosRect || NULL == lprcClipRect)
    {
        ASSERT(FALSE);
        return E_POINTER;
    }

    QueryInterface(IID_IOleInPlaceFrame, (void**)ppFrame);
    QueryInterface(IID_IOleInPlaceUIWindow, (void**)ppDoc);

    ::GetClientRect(m_hWnd, lprcPosRect);
    ::GetClientRect(m_hWnd, lprcClipRect);

    if (NULL == m_hAccel)
    {
        ACCEL ac; DEF_ZERO_IT(ac);
        m_hAccel = CreateAcceleratorTable(&ac, 1);
    }

    lpFrameInfo->cb = sizeof(OLEINPLACEFRAMEINFO);
    lpFrameInfo->fMDIApp = m_bMDIApp;
	lpFrameInfo->hwndFrame = ::GetParent(m_hWnd);
    lpFrameInfo->haccel = m_hAccel;
    lpFrameInfo->cAccelEntries = (NULL != m_hAccel) ? 1 : 0;

    return S_OK;
}

HRESULT STDMETHODCALLTYPE CFPCWnd::Scroll(SIZE scrollExtant)
{
    return E_NOTIMPL;
}

HRESULT STDMETHODCALLTYPE CFPCWnd::OnUIDeactivate(BOOL fUndoable)
{
    m_bUIActive = FALSE;

    return S_OK;
}

HRESULT STDMETHODCALLTYPE CFPCWnd::OnInPlaceDeactivate()
{
    m_bInPlaceActive = FALSE;

    m_pOleInPlaceObjectWindowless.Release();

    return S_OK;
}

HRESULT STDMETHODCALLTYPE CFPCWnd::DiscardUndoState()
{
    return E_NOTIMPL;
}

HRESULT STDMETHODCALLTYPE CFPCWnd::DeactivateAndUndo()
{
    return E_NOTIMPL;
}

HRESULT STDMETHODCALLTYPE CFPCWnd::OnPosRectChange(LPCRECT lprcPosRect)
{
    return E_NOTIMPL;
}

HRESULT STDMETHODCALLTYPE CFPCWnd::OnInPlaceActivateEx(BOOL* pfNoRedraw, DWORD dwFlags)
{
    // should only be called once the first time control is inplace-activated
    ASSERT(m_bInPlaceActive == FALSE);
    ASSERT(m_pOleInPlaceObjectWindowless == NULL);

    m_bInPlaceActive = TRUE;

    OleLockRunning(m_pOleObject, TRUE, FALSE);

    HRESULT hr = E_FAIL;

    if (dwFlags & ACTIVATE_WINDOWLESS)
    {
        m_bWindowless = TRUE;
        hr = m_pOleObject->QueryInterface(IID_IOleInPlaceObjectWindowless, (void**)&m_pOleInPlaceObjectWindowless);
    }

    if (FAILED(hr))
    {
        m_bWindowless = FALSE;
        hr = m_pOleObject->QueryInterface(IID_IOleInPlaceObject, (void**)&m_pOleInPlaceObjectWindowless);
    }

    if (m_pOleInPlaceObjectWindowless)
        m_pOleInPlaceObjectWindowless->SetObjectRects(&m_rcPos, &m_rcPos);

    return S_OK;
}

HRESULT STDMETHODCALLTYPE CFPCWnd::OnInPlaceDeactivateEx(BOOL fNoRedraw)
{
    m_bInPlaceActive = FALSE;

    m_pOleInPlaceObjectWindowless.Release();

    return S_OK;
}

HRESULT STDMETHODCALLTYPE CFPCWnd::RequestUIActivate()
{
    return S_OK;
}

HRESULT STDMETHODCALLTYPE CFPCWnd::CanWindowlessActivate()
{
    return m_bCanWindowlessActivate ? S_OK : S_FALSE;
}

HRESULT STDMETHODCALLTYPE CFPCWnd::GetCapture()
{
    return m_bCapture ? S_OK : S_FALSE;
}

HRESULT STDMETHODCALLTYPE CFPCWnd::SetCapture(BOOL fCapture)
{
    if (fCapture)
    {
        ::SetCapture(GetControlOwner());
        m_bCapture = TRUE;
    }
    else
    {
        if (m_bCapture)
        {
            ::ReleaseCapture();
            m_bCapture = FALSE;
        }
    }

    return S_OK;
}

HRESULT STDMETHODCALLTYPE CFPCWnd::GetFocus()
{
    return m_bHaveFocus ? S_OK : S_FALSE;
}

HRESULT STDMETHODCALLTYPE CFPCWnd::SetFocus(BOOL fFocus)
{
    m_bHaveFocus = fFocus;

	if (m_bAttachToParent)
		m_pFlashOCXLoader->SaveFocus(fFocus ? m_hWnd : DEF_NO_FOCUS_HWND);

    return S_OK;
}

HRESULT STDMETHODCALLTYPE CFPCWnd::GetDC(LPCRECT pRect, DWORD grfFlags, HDC* phDC)
{
    if (NULL == phDC)
        return E_POINTER;

    if (!m_bDCReleased)
        return E_FAIL;

    *phDC = ::GetDC(m_hWnd);

    if (NULL == *phDC)
        return E_FAIL;

	// Save the HDC to the list of allocated HDCs
	m_pFlashOCXLoader->FPCWndHDC_Add(*phDC);

    m_bDCReleased = FALSE;

    if (grfFlags & OLEDC_NODRAW)
        return S_OK;

    RECT rect;
    GetClientRect(m_hWnd, &rect);
    
    if (grfFlags & OLEDC_OFFSCREEN)
    {
        HDC hDCOffscreen = CreateCompatibleDC(*phDC);
    
        if (NULL != hDCOffscreen)
        {
            HBITMAP hBitmap = CreateCompatibleBitmap(*phDC, rect.right - rect.left, rect.bottom - rect.top);
    
            if (NULL == hBitmap)
                DeleteDC(hDCOffscreen);
            else
            {
                HGDIOBJ hOldBitmap = SelectObject(hDCOffscreen, hBitmap);

                if (NULL == hOldBitmap)
                {
                    DeleteObject(hBitmap);
                    DeleteDC(hDCOffscreen);
                }
                else
                {
                    DeleteObject(hOldBitmap);
                    m_hDCScreen = *phDC;
                    *phDC = hDCOffscreen;
                }
            }
        }
    }

    if (grfFlags & OLEDC_PAINTBKGND)
        FillRect(*phDC, &rect, (HBRUSH)(COLOR_WINDOW + 1));

    return S_OK;
}

HRESULT STDMETHODCALLTYPE CFPCWnd::ReleaseDC(HDC hDC)
{
    m_bDCReleased = TRUE;
    
    if (NULL != m_hDCScreen)
    {
        RECT rect;
        GetClientRect(m_hWnd, &rect);

        // Offscreen DC has to be copied to screen DC before releasing the screen dc;
        BitBlt(m_hDCScreen, 
               rect.left, 
               rect.top, 
               rect.right - rect.left, 
               rect.bottom - rect.top, 
               hDC, 
               0, 
               0, 
               SRCCOPY);
        
        DeleteDC(hDC);
        
        hDC = m_hDCScreen;
    }

	// Remove the HDC to the list of allocated HDCs
	// We do it before ReleaseDC, because another thread may allocate the same value of HDC
	// between calls of ReleaseDC and FPCWndHDC_Remove
	m_pFlashOCXLoader->FPCWndHDC_Remove(hDC);

    ::ReleaseDC(m_hWnd, hDC);

    return S_OK;
}

HRESULT STDMETHODCALLTYPE CFPCWnd::InvalidateRect(LPCRECT pRect, BOOL fErase)
{
    ::InvalidateRect(GetControlOwner(), pRect, fErase);

    // We should notify about update
    RECT rcUpdate;

    if (NULL != pRect)
        rcUpdate = *pRect;
    else
        GetClientRect(GetControlOwner(), &rcUpdate);

    SendUpdateRectNotification(rcUpdate);

    return S_OK;
}

HRESULT STDMETHODCALLTYPE CFPCWnd::InvalidateRgn(HRGN hRGN, BOOL fErase)
{
    ::InvalidateRgn(GetControlOwner(), hRGN, fErase);

    // We should notify about update
    RECT rcUpdate;

    GetRgnBox(hRGN, &rcUpdate);

    SendUpdateRectNotification(rcUpdate);

    return S_OK;
}

HRESULT STDMETHODCALLTYPE CFPCWnd::ScrollRect(INT dx, INT dy, LPCRECT pRectScroll, LPCRECT pRectClip)
{
    return S_OK;
}

HRESULT STDMETHODCALLTYPE CFPCWnd::AdjustRect(LPRECT prc)
{
    return S_OK;
}

HRESULT STDMETHODCALLTYPE CFPCWnd::OnDefWindowMessage(UINT msg, WPARAM wParam, LPARAM lParam, LRESULT *plResult)
{
	// TODO: m_bAttachToParent -- ?
    *plResult = WindowProc(m_hWnd, msg, wParam, lParam);

    return S_OK;
}

HRESULT STDMETHODCALLTYPE CFPCWnd::OnControlInfoChanged()
{
    return S_OK;
}

HRESULT STDMETHODCALLTYPE CFPCWnd::LockInPlaceActive(BOOL fLock)
{
    return S_OK;
}

HRESULT STDMETHODCALLTYPE CFPCWnd::GetExtendedControl(IDispatch** ppDisp)
{
    if (NULL == ppDisp)
        return E_POINTER;

    return m_pOleObject->QueryInterface(IID_IDispatch, (void**)ppDisp);
}

HRESULT STDMETHODCALLTYPE CFPCWnd::TransformCoords(POINTL *pPtlHimetric, POINTF *pPtfContainer, DWORD dwFlags)
{
    return E_NOTIMPL;
}

HRESULT STDMETHODCALLTYPE CFPCWnd::TranslateAccelerator(MSG* pMsg, DWORD grfModifiers)
{
    return S_FALSE;
}

HRESULT STDMETHODCALLTYPE CFPCWnd::OnFocus(BOOL fGotFocus)
{
    m_bHaveFocus = fGotFocus;

    return S_OK;
}

HRESULT STDMETHODCALLTYPE CFPCWnd::ShowPropertyFrame()
{
    return E_NOTIMPL;
}

HRESULT STDMETHODCALLTYPE CFPCWnd::ParseDisplayName(
                IBindCtx *pbc,
                LPOLESTR pszDisplayName,
                ULONG* pchEaten,
                IMoniker** ppmkOut)
{
    return E_NOTIMPL;
}

HRESULT STDMETHODCALLTYPE CFPCWnd::EnumObjects(DWORD grfFlags, IEnumUnknown** ppenum)
{
    if (NULL == ppenum)
        return E_POINTER;

    *ppenum = NULL;

    IUnknown* pUnknown = m_pUnknown;
    
    return f_in_box::com_helpers::CEnumUnknownImpl::CreateInstance(IID_IEnumUnknown, (void**)ppenum, &pUnknown, 1);
}

HRESULT STDMETHODCALLTYPE CFPCWnd::LockContainer(BOOL fLock)
{
    m_bLocked = fLock;

    return S_OK;
}

HRESULT STDMETHODCALLTYPE CFPCWnd::GetTypeInfoCount(UINT* pctinfo)
{
    return E_FAIL;
}

HRESULT STDMETHODCALLTYPE CFPCWnd::GetTypeInfo(
                UINT iTInfo, 
                LCID lcid, 
                ITypeInfo** ppTInfo)
{
    return E_FAIL;
}

HRESULT STDMETHODCALLTYPE CFPCWnd::GetIDsOfNames(
                REFIID riid, 
                LPOLESTR* rgszNames, 
                UINT cNames, 
                LCID lcid, 
                DISPID* rgDispId)
{
    struct SMapName2DISPID
    {
        LPCWSTR m_lpszName;
        DISPID m_dispid;
    };

    const SMapName2DISPID MapName2DISPID[] = 
    {
        { L"OnReadyStateChange", 0xfffffd9f }, 
        { L"OnProgress", 0x000007a6 }, 
        { L"FSCommand", 0x00000096 }, 
        { L"FlashCall", 0x000000c5 }
    };

    HRESULT hr = S_OK;

    for (unsigned int intIndex = 0; intIndex < cNames; intIndex++)
    {
        f_in_box::std::CString<WCHAR> strName = rgszNames[intIndex];

        BOOL bFound = FALSE;

        for (int intExistingNameIndex = 0; 
             intExistingNameIndex < DEF_ARRAY_SIZE(MapName2DISPID); 
             intExistingNameIndex++)
            if (strName == MapName2DISPID[intExistingNameIndex].m_lpszName)
            {
                bFound = TRUE;

                rgDispId[intIndex] = MapName2DISPID[intExistingNameIndex].m_dispid;

                break;
            }

        if (!bFound)
        {
            hr = DISP_E_UNKNOWNNAME;
            break;
        }
    }

    return hr;
}

HRESULT STDMETHODCALLTYPE CFPCWnd::Invoke(
                DISPID dispIdMember, 
                REFIID riid, 
                LCID lcid, 
                WORD wFlags, 
                DISPPARAMS* pDispParams, 
                VARIANT* pVarResult, 
                EXCEPINFO* pExcepInfo, 
                UINT* puArgErr)
{
    HRESULT hr;

    switch (dispIdMember)
    {
        //        [id(0xfffffd9f)]
        //        void OnReadyStateChange(long newState);
        case 0xfffffd9f:
        {
            long newState = pDispParams->rgvarg[0].lVal;

            OnReadyStateChange(newState);

            hr = S_OK;

            break;
        }

        //        [id(0x000007a6)]
        //        void OnProgress(long percentDone);
        case 0x000007a6:
        {
            long percentDone = pDispParams->rgvarg[0].lVal;

            OnProgress(percentDone);

            hr = S_OK;

            break;
        }

        //        [id(0x00000096)]
        //        void FSCommand(
        //                        [in] BSTR command, 
        //                        [in] BSTR args);
        case 0x00000096:
        {
            _bstr_t bstrArgs = pDispParams->rgvarg[0].bstrVal;
            _bstr_t bstrCommand = pDispParams->rgvarg[1].bstrVal;

            OnFSCommand(pDispParams->rgvarg[1].bstrVal, pDispParams->rgvarg[0].bstrVal);

            hr = S_OK;

            break;
        }

        // [id(0x000000c5)]
        // void FlashCall([in] BSTR request);
        case 0x000000c5:
        {
            _bstr_t bstrRequest = pDispParams->rgvarg[0].bstrVal;

			// if (ExternalInterface.call("{8533542B-B75B-417a-A972-3AD945F14852}") != "{242DF42C-A9ED-4209-B1E2-FD3633BAEEE7}")
			LPCWSTR szMark = L"{8533542B-B75B-417a-A972-3AD945F14852}";
			LPCWSTR szAnswer = L"<string>{242DF42C-A9ED-4209-B1E2-FD3633BAEEE7}</string>";
			LPCWSTR szRequest = (LPCWSTR)bstrRequest;
//			f_in_box::std::CArray<WCHAR> buf( lstrlenW(szMark) + 1 );
			bool bMarkFound = false;

			//for (int i = 0; i + lstrlenW(szMark) <= lstrlenW(szRequest); i++)
			//{
			//	f_in_box::MemoryZero(buf, sizeof(*buf) * buf.GetCount());
			//	f_in_box::MemoryCopy(buf, szRequest + i, sizeof(szMark[0]) * lstrlenW(szMark));

			//	if (0 == lstrcmpiW(buf, szMark))
			//	{
			//		bMarkFound = true;
			//		break;
			//	}
			//}

			LPCWSTR request_cur = szRequest;
			LPCWSTR mark_cur = szMark;

			while (true)
			{
				if (0 == *mark_cur)
				{
					bMarkFound = true;
					break;
				}
				else if (0 == *request_cur)
				{
					break;
				}
				else if (*request_cur == *mark_cur)
					// ����������
				{
					request_cur++;
					mark_cur++;
				}
				else
				{
					if (mark_cur != szMark)
						// �.�. �� ����� ���� ���������� => �� ��������� �������� ���� ���������� *szMark � ������� *request_cur
						mark_cur = szMark;
					else
						request_cur++;
				}
			}

			if (bMarkFound)
				FPCSetReturnValueW(m_hWnd, szAnswer);
			else
	            OnFlashCall(bstrRequest);

            hr = S_OK;

            break;
        }

        default:
        {
            hr = DISP_E_MEMBERNOTFOUND;
        }
    }

    return hr;
}

// Send notification FPCN_UPDATE_RECT
void CFPCWnd::SendUpdateRectNotification(RECT rc)
{
    HWND hwndParent = ::GetParent(m_hWnd);

    //
    UINT uId = (UINT)::GetWindowLong(m_hWnd, GWL_ID);

    SFPCNUpdateRect info;

    info.hdr.code = FPCN_UPDATE_RECT;
    info.hdr.hwndFrom = m_hWnd;
    info.hdr.idFrom = uId;

	info.rc = rc;

	if (NULL != hwndParent)
	    ::SendMessage(hwndParent, WM_NOTIFY, uId, (LPARAM)&info);

    {
    	CCriticalSectionOwner cso(m_csEventListener);

	    if (NULL != m_pEventListener)
		    m_pEventListener(m_hWnd, m_LPARAM_EventListenerParam, (NMHDR*)&info);
    }
}

void CFPCWnd::OnReadyStateChange(long newState)
{
    HWND hwndParent = ::GetParent(m_hWnd);

    //
    UINT uId = (UINT)GetWindowLong(m_hWnd, GWL_ID);

    //
    SFPCOnReadyStateChangeInfoStruct info;

    info.hdr.code = FPCN_ONREADYSTATECHANGE;
    info.hdr.hwndFrom = m_hWnd;
    info.hdr.idFrom = uId;

    info.newState = newState;

    if (NULL != hwndParent)
	    ::SendMessage(hwndParent, WM_NOTIFY, uId, (LPARAM)&info);

	{
    	CCriticalSectionOwner cso(m_csEventListener);

		if (NULL != m_pEventListener)
			m_pEventListener(m_hWnd, m_LPARAM_EventListenerParam, (NMHDR*)&info);
	}
}

void CFPCWnd::OnProgress(long percentDone)
{
    HWND hwndParent = ::GetParent(m_hWnd);

    //
    UINT uId = (UINT)GetWindowLong(m_hWnd, GWL_ID);

    //
    SFPCOnProgressInfoStruct info;

    info.hdr.code = FPCN_ONPROGRESS;
    info.hdr.hwndFrom = m_hWnd;
    info.hdr.idFrom = uId;

    info.percentDone = percentDone;

    if (NULL != hwndParent)
        ::SendMessage(hwndParent, WM_NOTIFY, uId, (LPARAM)&info);

    {
        CCriticalSectionOwner cso(m_csEventListener);
        
        if (NULL != m_pEventListener)
            m_pEventListener(m_hWnd, m_LPARAM_EventListenerParam, (NMHDR*)&info);
    }
}

void CFPCWnd::OnFSCommand(BSTR command, BSTR args)
{
    HWND hwndParent = ::GetParent(m_hWnd);

    //
    UINT uId = (UINT)GetWindowLong(m_hWnd, GWL_ID);

    _bstr_t bstrCommand = command;
    _bstr_t bstrArgs = args;

    //
    {
        SFPCFSCommandInfoStructW info;

        info.hdr.code = FPCN_FSCOMMANDW;
        info.hdr.hwndFrom = m_hWnd;
        info.hdr.idFrom = uId;

        info.command = bstrCommand;
        info.args = bstrArgs;

        if (NULL != hwndParent)
            ::SendMessage(hwndParent, WM_NOTIFY, uId, (LPARAM)&info);

        {
            CCriticalSectionOwner cso(m_csEventListener);

            if (NULL != m_pEventListener)
                m_pEventListener(m_hWnd, m_LPARAM_EventListenerParam, (NMHDR*)&info);
        }
    }

    //
    {
        SFPCFSCommandInfoStructA info;

        info.hdr.code = FPCN_FSCOMMANDA;
        info.hdr.hwndFrom = m_hWnd;
        info.hdr.idFrom = uId;

        info.command = bstrCommand;
        info.args = bstrArgs;

        if (NULL != hwndParent)
            ::SendMessage(hwndParent, WM_NOTIFY, uId, (LPARAM)&info);

        CCriticalSectionOwner cso(m_csEventListener);
        {
            if (NULL != m_pEventListener)
                m_pEventListener(m_hWnd, m_LPARAM_EventListenerParam, (NMHDR*)&info);
        }
    }
}

void CFPCWnd::OnFlashCall(BSTR request)
{
    HWND hwndParent = ::GetParent(m_hWnd);

    //
    UINT uId = (UINT)GetWindowLong(m_hWnd, GWL_ID);

    _bstr_t bstrRequest = request;

    //
    {
        SFPCFlashCallInfoStructW info;

        info.hdr.code = FPCN_FLASHCALLW;
        info.hdr.hwndFrom = m_hWnd;
        info.hdr.idFrom = uId;

        info.request = bstrRequest;

        if (NULL != hwndParent)
            ::SendMessage(hwndParent, WM_NOTIFY, uId, (LPARAM)&info);

        CCriticalSectionOwner cso(m_csEventListener);
        {
            if (NULL != m_pEventListener)
                m_pEventListener(m_hWnd, m_LPARAM_EventListenerParam, (NMHDR*)&info);
        }
    }

    //
    {
        SFPCFlashCallInfoStructA info;

        info.hdr.code = FPCN_FLASHCALLA;
        info.hdr.hwndFrom = m_hWnd;
        info.hdr.idFrom = uId;

        info.request = bstrRequest;

        if (NULL != hwndParent)
            ::SendMessage(hwndParent, WM_NOTIFY, uId, (LPARAM)&info);

        CCriticalSectionOwner cso(m_csEventListener);
        {
            if (NULL != m_pEventListener)
                m_pEventListener(m_hWnd, m_LPARAM_EventListenerParam, (NMHDR*)&info);
        }
    }
}

void CFPCWnd::PreTranslateMessageHelper_PreTranslateMessage(MSG* pMsg)
{
    if (WM_KEYFIRST <= pMsg->message && pMsg->message <= WM_KEYLAST)
    {
        if (m_bHaveFocus)
        {
            f_in_box::com_helpers::CComPtr<IOleInPlaceActiveObject> 
                pOleInPlaceActiveObject(IID_IOleInPlaceActiveObject, m_pUnknown);

            if (pOleInPlaceActiveObject)
                if (S_OK == pOleInPlaceActiveObject->TranslateAccelerator(pMsg))
                    pMsg->message = WM_NULL;
        }
    }
}

static int MapPixToLogHim(int x, int ppli)
{
    return MulDiv(2540, x, ppli);
}

static int MapLogHimToPix(int x, int ppli)
{
    return MulDiv(ppli, x, 2540);
}

namespace f_in_box
{
	void PixelToHiMetric(const SIZEL* lpSizeInPix, LPSIZEL lpSizeInHiMetric)
	{
		int nPixelsPerInchX;    // Pixels per logical inch along width
		int nPixelsPerInchY;    // Pixels per logical inch along height

		HDC hDCScreen = GetDC(NULL);
		ASSERT(hDCScreen != NULL);
		nPixelsPerInchX = GetDeviceCaps(hDCScreen, LOGPIXELSX);
		nPixelsPerInchY = GetDeviceCaps(hDCScreen, LOGPIXELSY);
		ReleaseDC(NULL, hDCScreen);

		lpSizeInHiMetric->cx = MapPixToLogHim(lpSizeInPix->cx, nPixelsPerInchX);
		lpSizeInHiMetric->cy = MapPixToLogHim(lpSizeInPix->cy, nPixelsPerInchY);
	}

	void HiMetricToPixel(const SIZEL * lpSizeInHiMetric, LPSIZEL lpSizeInPix)
	{
		int nPixelsPerInchX;    // Pixels per logical inch along width
		int nPixelsPerInchY;    // Pixels per logical inch along height

		HDC hDCScreen = GetDC(NULL);
		ASSERT(hDCScreen != NULL);
		nPixelsPerInchX = GetDeviceCaps(hDCScreen, LOGPIXELSX);
		nPixelsPerInchY = GetDeviceCaps(hDCScreen, LOGPIXELSY);
		ReleaseDC(NULL, hDCScreen);

		lpSizeInPix->cx = MapLogHimToPix(lpSizeInHiMetric->cx, nPixelsPerInchX);
		lpSizeInPix->cy = MapLogHimToPix(lpSizeInHiMetric->cy, nPixelsPerInchY);
	}
}

HRESULT STDMETHODCALLTYPE CFPCWnd::GetBorder(LPRECT lprectBorder)
{
    return INPLACE_E_NOTOOLSPACE;
}

HRESULT STDMETHODCALLTYPE CFPCWnd::RequestBorderSpace(LPCBORDERWIDTHS pborderwidths)
{
    return INPLACE_E_NOTOOLSPACE;
}

HRESULT STDMETHODCALLTYPE CFPCWnd::SetBorderSpace(LPCBORDERWIDTHS pborderwidths)
{
    return E_NOTIMPL;
}

HRESULT STDMETHODCALLTYPE CFPCWnd::SetActiveObject(IOleInPlaceActiveObject *pActiveObject, LPCOLESTR pszObjName)
{
    return S_OK;
}

HRESULT STDMETHODCALLTYPE CFPCWnd::InsertMenus(HMENU hmenuShared, LPOLEMENUGROUPWIDTHS lpMenuWidths)
{
    return S_OK;
}

HRESULT STDMETHODCALLTYPE CFPCWnd::SetMenu(HMENU hmenuShared, HOLEMENU holemenu, HWND hwndActiveObject)
{
    return S_OK;
}

HRESULT STDMETHODCALLTYPE CFPCWnd::RemoveMenus(HMENU hmenuShared)
{
    return S_OK;
}

HRESULT STDMETHODCALLTYPE CFPCWnd::SetStatusText(LPCOLESTR pszStatusText)
{
    return S_OK;
}

HRESULT STDMETHODCALLTYPE CFPCWnd::EnableModeless(BOOL fEnable)
{
    return S_OK;
}

HRESULT STDMETHODCALLTYPE CFPCWnd::TranslateAccelerator(LPMSG lpmsg, WORD wID)
{
    return S_FALSE;
}

LRESULT
CALLBACK
CFPCWnd::WindowProc(
    HWND hWnd, 
    UINT uMsg, 
    WPARAM wParam, 
    LPARAM lParam
)
{
	BOOL bHandled = FALSE;
	LRESULT lResult = InternalWindowProc(hWnd, uMsg, wParam, lParam, bHandled);

	if (!bHandled)
		lResult = CWnd::WindowProc(hWnd, uMsg, wParam, lParam);

	return lResult;
}

LRESULT
CALLBACK
CFPCWnd::InternalWindowProc(
    HWND hWnd, 
    UINT uMsg, 
    WPARAM wParam, 
    LPARAM lParam, 
	BOOL& bHandled
)
{
    if (WM_GETDLGCODE == uMsg)
    {
		if (0 != (GetWindowLong(m_hWnd, GWL_STYLE) & FPCS_NEED_ALL_KEYS))
			return DLGC_WANTALLKEYS;
    }



    if (m_uMsg__SetContext == uMsg)
    {
        m_strContext = (char*)lParam;

        if (NULL != m_FlashAXWnd.m_hWnd)
            SendMessage(m_FlashAXWnd.m_hWnd, uMsg, wParam, lParam);

		m_pFlashOCXLoader->SetContext(m_strContext.c_str());

        return 0;
    }



    LRESULT lResult = 0;

    MSG_HANDLER(WM_CREATE, OnCreate)
	MSG_HANDLER(WM_ERASEBKGND, OnEraseBackground)
	MSG_HANDLER(WM_PAINT, OnPaint)
	MSG_HANDLER(WM_SIZE, OnSize)
	MSG_HANDLER(WM_MOUSEACTIVATE, OnMouseActivate)
	MSG_HANDLER(WM_SETFOCUS, OnSetFocus)
	MSG_HANDLER(WM_KILLFOCUS, OnKillFocus)
   	MSG_HANDLER(WM_NCHITTEST, OnNcHitTest)

    MSG_HANDLER(WM_RBUTTONUP, OnContextMenu)

    MSG_HANDLER(WM_TIMER, OnTimer)

	MSG_HANDLER(FPCM_QUERYINTERFACE, OnQueryInterface)
	MSG_HANDLER(FPCM_PUTMOVIEFROMMEMORY, OnPutMovieFromMemory)
	MSG_HANDLER(FPCM_LOADMOVIEFROMMEMORY, OnLoadMovieFromMemory)
	MSG_HANDLER(FPCM_PUT_MOVIE_USING_STREAM, OnPutMovieUsingStream)
	MSG_HANDLER(FPCM_LOAD_MOVIE_USING_STREAM, OnLoadMovieUsingStream)
	MSG_HANDLER(FPCM_GET_FRAME_BITMAP, OnGetFrameBitmap)
	MSG_HANDLER(FPCM_PUT_STANDARD_MENU, OnPutStandardMenu)
	MSG_HANDLER(FPCM_GET_STANDARD_MENU, OnGetStandardMenu)
	MSG_HANDLER(FPCM_PUT_OVERALL_OPAQUE, OnPutOverallOpaque)
	MSG_HANDLER(FPCM_GET_OVERALL_OPAQUE, OnGetOverallOpaque)
    MSG_HANDLER(FPCM_CALL_FUNCTION, OnCallFunction)
    MSG_HANDLER(FPCM_SET_RETURN_VALUEA, OnSetReturnValueA)
    MSG_HANDLER(FPCM_SET_RETURN_VALUEW, OnSetReturnValueW)

	MSG_HANDLER(m_uMsg__PRIVATE_MESSAGE__SET_EVENT_LISTENER, OnSetEventListener)
	MSG_HANDLER(m_uMsg__PRIVATE_MESSAGE__CALL_LOAD_EXTERNAL_RESOURCE, OnCallLoadExternalResource)
	MSG_HANDLER(m_uMsg__DEF_PRIVATE_MESSAGE__GET_OBJECT_PTR, OnGetObjectPtr)

    #undef MESSAGE_HANDLER
    #define MESSAGE_HANDLER MSG_HANDLER
#include "message_map.inl"

    if (m_bWindowless)
	{
		MSG_HANDLER(WM_SETCURSOR, OnWindowlessMouseMessage)

		// Mouse messages handled when a windowless control has captured the cursor
		// or if the cursor is over the control
   		DWORD dwHitResult = m_bCapture ? HITRESULT_HIT : HITRESULT_OUTSIDE;
		if (dwHitResult == HITRESULT_OUTSIDE && m_pViewObject.IsNotNull())
		{
			if (WM_MOUSEWHEEL == uMsg)
			{
				RECT rcWindow;
				GetWindowRect(GetControlOwner(), &rcWindow);

				POINT ptMouse = { GET_X_LPARAM(lParam) - rcWindow.left, GET_Y_LPARAM(lParam) - rcWindow.top };
				m_pViewObjectEx->QueryHitPoint(DVASPECT_CONTENT, &m_rcPos, ptMouse, 0, &dwHitResult);
			}
			else
			{
				POINT ptMouse = { GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam) };
				m_pViewObjectEx->QueryHitPoint(DVASPECT_CONTENT, &m_rcPos, ptMouse, 0, &dwHitResult);
			}
		}
		if (dwHitResult == HITRESULT_HIT)
		{
            if (WM_RBUTTONDOWN == uMsg || 
                WM_RBUTTONUP == uMsg || 
                WM_CONTEXTMENU == uMsg)
                if (!m_bStandardMenu)
                    return 0;

			MSG_HANDLER(WM_MOUSEMOVE, OnWindowlessMouseMessage)
			MSG_HANDLER(WM_LBUTTONUP, OnWindowlessMouseMessage)
			MSG_HANDLER(WM_RBUTTONUP, OnWindowlessMouseMessage)
			MSG_HANDLER(WM_MBUTTONUP, OnWindowlessMouseMessage)
			MSG_HANDLER(WM_LBUTTONDOWN, OnWindowlessMouseMessage)
			MSG_HANDLER(WM_RBUTTONDOWN, OnWindowlessMouseMessage)
			MSG_HANDLER(WM_MBUTTONDOWN, OnWindowlessMouseMessage)
			MSG_HANDLER(WM_LBUTTONDBLCLK, OnWindowlessMouseMessage)
			MSG_HANDLER(WM_RBUTTONDBLCLK, OnWindowlessMouseMessage)
			MSG_HANDLER(WM_MBUTTONDBLCLK, OnWindowlessMouseMessage)
			MSG_HANDLER(WM_MOUSEWHEEL, OnWindowlessMouseMessage)
		}
	}
	if (m_bWindowless & m_bHaveFocus)
	{
		// Keyboard messages handled only when a windowless control has the focus
		MSG_HANDLER(WM_KEYDOWN, OnWindowMessage)
		MSG_HANDLER(WM_KEYUP, OnWindowMessage)
		MSG_HANDLER(WM_CHAR, OnWindowMessage)

#ifndef WM_UNICHAR
#define WM_UNICHAR 0x0109
#endif // !WM_UNICHAR

		MSG_HANDLER(WM_UNICHAR, OnWindowMessage)

		MSG_HANDLER(WM_DEADCHAR, OnWindowMessage)
		MSG_HANDLER(WM_SYSKEYDOWN, OnWindowMessage)
		MSG_HANDLER(WM_SYSKEYUP, OnWindowMessage)
		MSG_HANDLER(WM_SYSDEADCHAR, OnWindowMessage)
		MSG_HANDLER(WM_HELP, OnWindowMessage)
		MSG_HANDLER(WM_CANCELMODE, OnWindowMessage)
		MSG_HANDLER(WM_MBUTTONDBLCLK, OnWindowMessage)
		MSG_HANDLER(WM_IME_KEYDOWN, OnWindowMessage)
		MSG_HANDLER(WM_IME_KEYUP, OnWindowMessage)
		MSG_RANGE_HANDLER(WM_IME_SETCONTEXT, WM_IME_CHAR, OnWindowMessage)
		MSG_RANGE_HANDLER(WM_IME_STARTCOMPOSITION, WM_IME_KEYLAST, OnWindowMessage)

#ifndef WM_IME_REQUEST
#define WM_IME_REQUEST 0x0288
#endif // !WM_IME_REQUEST

		MSG_HANDLER(WM_IME_REQUEST, OnWindowMessage)
	}
	MSG_HANDLER(WM_DESTROY, OnDestroy)

	return lResult;

//    return DefWindowProc(hWnd, uMsg, wParam, lParam);
//    return CWnd::WindowProc(hWnd, uMsg, wParam, lParam);

	//if (IsWindowUnicode(hWnd))
	//{
	//	typedef LONG (WINAPI *P_DefWindowProcW)(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
	//	P_DefWindowProcW pDefWindowProcW;
	//	HMODULE hUser32DLL = LoadLibrary(_T("user32.dll"));
	//	(FARPROC&)pDefWindowProcW = ::GetProcAddress(hUser32DLL, "DefWindowProcW");

	//	if (NULL != pDefWindowProcW)
	//		lResult = pDefWindowProcW(hWnd, uMsg, wParam, lParam);
	//	else
	//		lResult = DefWindowProcA(hWnd, uMsg, wParam, lParam);

	//	FreeLibrary(hUser32DLL);
	//}
	//else
	//    lResult = DefWindowProcA(hWnd, uMsg, wParam, lParam);

	//return lResult;
}

LRESULT CFPCWnd::OnEraseBackground(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
    //if (m_pViewObject.IsNotNull())
    //    bHandled = FALSE;

    return 1;
}

LRESULT CFPCWnd::OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
    if (m_pViewObject.IsNull())
    {
        PAINTSTRUCT ps;
    
        HDC hdc = ::BeginPaint(m_hWnd, &ps);
    
        if (hdc == NULL)
            return 0;

        RECT rcClient;
        GetClientRect(m_hWnd, &rcClient);
    
        HBRUSH hbrBack = CreateSolidBrush(m_clrBackground);
    
        if (hbrBack != NULL)
        {
            FillRect(hdc, &rcClient, hbrBack);
            DeleteObject(hbrBack);
        }

        ::EndPaint(m_hWnd, &ps);

        return 1;
    }
    else if (m_pViewObject.IsNotNull() && m_bWindowless)
    {
        // Is child window?
        if (::GetWindowLong(m_hWnd, GWL_STYLE) & WS_CHILD)
            // Child
        {
			PAINTSTRUCT ps;
			HDC hdc = ::BeginPaint(m_hWnd, &ps);

            if (NULL == hdc)
                return 0;

            RECT rcClient;
            GetClientRect(m_hWnd, &rcClient);

            int nClientWidth = rcClient.right - rcClient.left;
            int nClientHeight = rcClient.bottom - rcClient.top;

            BITMAPINFO bitmap_info; DEF_ZERO_IT(bitmap_info);
            f_in_box::MemoryZero(&bitmap_info, sizeof(bitmap_info));
            bitmap_info.bmiHeader.biSize = sizeof(bitmap_info.bmiHeader);
            bitmap_info.bmiHeader.biWidth = nClientWidth;
            bitmap_info.bmiHeader.biHeight = nClientHeight;
            bitmap_info.bmiHeader.biPlanes = 1;
            bitmap_info.bmiHeader.biBitCount = 32;

            HDC hdcMem;

			hdcMem = ::CreateCompatibleDC(hdc);

			////
			//RECT rcClip;
			//rcClip.left = ps.rcPaint.left;
			//rcClip.top = ps.rcPaint.top;
			//rcClip.right = ps.rcPaint.right;
			//rcClip.bottom = ps.rcPaint.bottom;

//			IntersectClipRect(hdcMem, rcClip.left, rcClip.top, rcClip.right, rcClip.bottom);

            if (nClientWidth != m_size.cx || nClientHeight != m_size.cy)
            {
                if (NULL != m_hBmp)
                {
                    DeleteObject(m_hBmp);
                    m_hBmp = NULL;
                }

                m_hBmp = CreateDIBSection(hdcMem, &bitmap_info, DIB_RGB_COLORS, &m_pBits, 0, 0);

                m_size.cx = nClientWidth;
                m_size.cy = nClientHeight;
            }

            HBITMAP hBitmapOld = (HBITMAP)SelectObject(hdcMem, m_hBmp); 
            {
				{
					long lFlashColor;
					FPC_GetBackgroundColor(m_hWnd, &lFlashColor);
					COLORREF clColor = DEF_FLASH_COLOR_TO_RGB(lFlashColor);

					HBRUSH hbrBack = CreateSolidBrush(clColor);

					FillRect(hdcMem, &rcClient, hbrBack);
					//FillRect(hdcMem, &rcClip, hbrBack);
					DeleteObject(hbrBack);
				}

				CallPaintHandler(hdcMem, DEF_F_IN_BOX__PREPAINT_STAGE);

				Draw(hdcMem, &m_rcPos);

                // Notify about new update
                SendPaintNotification(m_pBits);

				CallPaintHandler(hdcMem, DEF_F_IN_BOX__AFTERPAINT_STAGE);

//                ::BitBlt(hdc, rcClip.left, rcClip.top, rcClip.right - rcClip.left, rcClip.bottom - rcClip.top, hdcMem, rcClip.left, rcClip.top, SRCCOPY);
                ::BitBlt(hdc, 0, 0, rcClient.right, rcClient.bottom, hdcMem, 0, 0, SRCCOPY);
            }
            ::SelectObject(hdcMem, hBitmapOld); 

            ::DeleteDC(hdcMem);

            ::EndPaint(m_hWnd, &ps);
        }
        else
            // Popup window
        {
            RECT rcUpdateRect; DEF_ZERO_IT(rcUpdateRect);
            ::GetUpdateRect(m_hWnd, &rcUpdateRect, FALSE);

            RECT rcClient;
            GetClientRect(m_hWnd, &rcClient);

            if (rcUpdateRect.left == rcUpdateRect.right || 
                rcUpdateRect.top == rcUpdateRect.bottom)
                rcUpdateRect = rcClient;

            PAINTSTRUCT ps;
            HDC hdc = ::BeginPaint(m_hWnd, &ps);

            if (NULL == hdc)
                return 0;

            if (!m_User32Wrapper.IsLayeredWindowSupported())
            {
                HBITMAP hBitmap = 
                    CreateCompatibleBitmap(hdc, 
                                        rcClient.right - rcClient.left, 
                                        rcClient.bottom - rcClient.top);

                if (NULL != hBitmap)
                {
                    HDC hdcCompatible = ::CreateCompatibleDC(hdc);

                    if (NULL != hdcCompatible)
                    {
                        HBITMAP hBitmapOld = (HBITMAP)SelectObject(hdcCompatible, hBitmap); 

                        if (NULL != hBitmapOld)
                        {
                            long lFlashColor;
                            FPC_GetBackgroundColor(m_hWnd, &lFlashColor);
                            COLORREF clColor = DEF_FLASH_COLOR_TO_RGB(lFlashColor);

                            HBRUSH hbrBack = CreateSolidBrush(clColor);

                            if (NULL != hbrBack)
                            {
                                FillRect(hdcCompatible, &rcClient, hbrBack);
                                DeleteObject(hbrBack);

								CallPaintHandler(hdcCompatible, DEF_F_IN_BOX__PREPAINT_STAGE);

                                Draw(hdcCompatible, &m_rcPos); 

								CallPaintHandler(hdcCompatible, DEF_F_IN_BOX__AFTERPAINT_STAGE);

                                ::BitBlt(hdc, 0, 0, rcClient.right, rcClient.bottom, hdcCompatible, 0, 0, SRCCOPY);
                            }
                        }

                        ::SelectObject(hdcCompatible, hBitmapOld); 
                    }

                    ::DeleteDC(hdcCompatible);
                }

                ::DeleteObject(hBitmap);
            }
            else
            {
                HDC hdcScreen = CreateDC(_T("DISPLAY"), NULL, NULL, NULL);
                HDC hdcMem = CreateCompatibleDC(hdcScreen);

                int nClientWidth = rcClient.right - rcClient.left;
                int nClientHeight = rcClient.bottom - rcClient.top;

                int nUpdateWidth = rcUpdateRect.right - rcUpdateRect.left;
                int nUpdateHeight = rcUpdateRect.bottom - rcUpdateRect.top;

				// Clipping
				HRGN hRgnClipping = CreateRectRgnIndirect(&rcUpdateRect);
				SelectClipRgn(hdcMem, hRgnClipping);

                BITMAPINFO bitmap_info; DEF_ZERO_IT(bitmap_info);
                f_in_box::MemoryZero(&bitmap_info, sizeof(bitmap_info));
                bitmap_info.bmiHeader.biSize = sizeof(bitmap_info.bmiHeader);
                bitmap_info.bmiHeader.biWidth = nClientWidth;
                bitmap_info.bmiHeader.biHeight = nClientHeight;
                bitmap_info.bmiHeader.biPlanes = 1;
                bitmap_info.bmiHeader.biBitCount = 32;

				if (nClientWidth != m_size.cx || nClientHeight != m_size.cy)
                {
                    if (NULL != m_hBmp)
                    {
                        DeleteObject(m_hBmp);
                        m_hBmp = NULL;
                    }
                    m_hBmp = CreateDIBSection(hdcMem, &bitmap_info, DIB_RGB_COLORS, &m_pBits, 0, 0);

                    if (NULL != m_hBmpWhite)
                    {
                        DeleteObject(m_hBmpWhite);
                        m_hBmpWhite = NULL;
                    }
                    m_hBmpWhite = CreateDIBSection(hdcMem, &bitmap_info, DIB_RGB_COLORS, &m_pBitsWhite, 0, 0);

                    if (NULL != m_hBmpBlack)
                    {
                        DeleteObject(m_hBmpBlack);
                        m_hBmpBlack = NULL;
                    }
                    m_hBmpBlack = CreateDIBSection(hdcMem, &bitmap_info, DIB_RGB_COLORS, &m_pBitsBlack, 0, 0);

                    m_size.cx = nClientWidth;
                    m_size.cy = nClientHeight;
                }

				// Write to white picture
				HGDIOBJ hOldBitmap = SelectObject(hdcMem, m_hBmpWhite);
				HBRUSH hBrushWhite = CreateSolidBrush(RGB(0xff, 0xff, 0xff));
				FillRect(hdcMem, &rcClient, hBrushWhite);
				DeleteObject(hBrushWhite);

				Draw(hdcMem, &rcClient);

				SelectObject(hdcMem, hOldBitmap);

				// Write to black picture
				hOldBitmap = SelectObject(hdcMem, m_hBmpBlack);
				HBRUSH hBrushBlack = CreateSolidBrush(RGB(0x00, 0x00, 0x00));
				FillRect(hdcMem, &rcClient, hBrushBlack);
				DeleteObject(hBrushBlack);

				Draw(hdcMem, &rcClient);

				SelectObject(hdcMem, hOldBitmap);

                int X = rcUpdateRect.left;
                int Y = rcUpdateRect.top;
                int i = nClientWidth * (rcClient.bottom - 1 - Y) + X;
                int nDelta = 
                    (nClientWidth * (rcClient.bottom - 1) + rcUpdateRect.left) - 
                    (nClientWidth * (rcClient.bottom - 0) + (rcUpdateRect.right - 1) ) - 1;

                PDWORD pCurBitsWhite = PDWORD(m_pBitsWhite) + i;
                PDWORD pCurBitsBlack = PDWORD(m_pBitsBlack) + i;
                PDWORD pCurBits = PDWORD(m_pBits) + i;

				for (; 
					Y < rcUpdateRect.bottom; 
					Y++, i += nDelta, pCurBitsWhite += nDelta, pCurBitsBlack += nDelta, pCurBits += nDelta)
					for (X = rcUpdateRect.left; 
						X < rcUpdateRect.right; 
						X++, i++, pCurBitsWhite++, pCurBitsBlack++, pCurBits++)
					{
						DWORD ColorWhite = *pCurBitsWhite;
						DWORD ColorBlack = *pCurBitsBlack;

						DWORD Alpha = (ColorWhite & 0x000000ff) - (ColorBlack & 0x000000ff);
						Alpha = ~Alpha;

						*pCurBits = (Alpha << 24) + (ColorBlack & 0x00ffffff);
					}

                // Notify about new update
                SendPaintNotification(m_pBits);

                hOldBitmap = SelectObject(hdcMem, m_hBmp);

                BLENDFUNCTION blend_function; DEF_ZERO_IT(blend_function);
                blend_function.BlendOp = AC_SRC_OVER;
                blend_function.BlendFlags = 0;
                blend_function.SourceConstantAlpha = m_nOverallOpaque;
                blend_function.AlphaFormat = DEF_AC_SRC_ALPHA;

                POINT ptSrc; DEF_ZERO_IT(ptSrc);
                POINT ptDest; DEF_ZERO_IT(ptDest);

                {
                    RECT rcWindow;
                    GetWindowRect(m_hWnd, &rcWindow);
                    ptDest.x = rcWindow.left;
                    ptDest.y = rcWindow.top;
                }

                SIZE size; DEF_ZERO_IT(size);

                size.cx = nClientWidth;
                size.cy = nClientHeight;

                m_User32Wrapper.UpdateLayeredWindow(
                    m_hWnd, 
                    hdcScreen, 
                    &ptDest, 
                    &size, 
                    hdcMem, 
                    &ptSrc, 
                    0, 
                    &blend_function, 
                    DEF_ULW_ALPHA);

				DeleteObject(hRgnClipping);

                SelectObject(hdcMem, hOldBitmap);

                ::DeleteDC(hdcMem);
                ::DeleteDC(hdcScreen);
            }

            ::EndPaint(m_hWnd, &ps);
        }
    }
    else
    {
        bHandled = FALSE;
        return 0;
    }

    return 1;
}

LRESULT CFPCWnd::OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
    int nWidth = GET_X_LPARAM(lParam);  // width of client area
    int nHeight = GET_Y_LPARAM(lParam); // height of client area

    m_rcPos.right = m_rcPos.left + nWidth;
    m_rcPos.bottom = m_rcPos.top + nHeight;
    m_pxSize.cx = m_rcPos.right - m_rcPos.left;
    m_pxSize.cy = m_rcPos.bottom - m_rcPos.top;

    PixelToHiMetric(&m_pxSize, &m_hmSize);

    if (m_pOleObject)
        m_pOleObject->SetExtent(DVASPECT_CONTENT, &m_hmSize);
    
    if (m_pOleInPlaceObjectWindowless)
        m_pOleInPlaceObjectWindowless->SetObjectRects(&m_rcPos, &m_rcPos);

    if (m_bWindowless)
        InvalidateRect(NULL, TRUE);
    
    bHandled = FALSE;
    
    return 0;
}

LRESULT CFPCWnd::OnMouseActivate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
    bHandled = FALSE;

    if (m_dwMiscStatus & OLEMISC_NOUIACTIVATE)
    {
        if (m_pOleObject.IsNotNull() && !m_bInPlaceActive)
        {
            f_in_box::com_helpers::CComPtr<IOleClientSite> pClientSite;
    
            QueryInterface(IID_IOleClientSite, (void**)&pClientSite);
    
            if (pClientSite.IsNotNull())
                m_pOleObject->DoVerb(OLEIVERB_INPLACEACTIVATE, NULL, pClientSite, 0, GetControlOwner(), &m_rcPos);
        }
    }
    else
    {
        BOOL b;
        OnSetFocus(0, 0, 0, b);
    }

    return 0;
}

LRESULT CFPCWnd::OnSetFocus(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
    m_bHaveFocus = TRUE;

    if (!m_bReleaseAll)
    {
        if (m_pOleObject.IsNotNull() && !m_bUIActive)
        {
            f_in_box::com_helpers::CComPtr<IOleClientSite> pClientSite;
    
            QueryInterface(IID_IOleClientSite, (void**)&pClientSite);
    
            if (pClientSite.IsNotNull())
                m_pOleObject->DoVerb(OLEIVERB_UIACTIVATE, NULL, pClientSite, 0, GetControlOwner(), &m_rcPos);
        }
    
        if (m_bWindowless)
            ::SetFocus(GetControlOwner());
        else
            if (!IsChild(GetControlOwner(), ::GetFocus()))
                ::SetFocus(::GetWindow(GetControlOwner(), GW_CHILD));
    }

    bHandled = FALSE;

    return 0;
}

LRESULT CFPCWnd::OnKillFocus(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
    m_bHaveFocus = FALSE;

    bHandled = FALSE;

    return 0;
}

LRESULT CFPCWnd::OnNcHitTest(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
    bHandled = FALSE;

    return 0;
}

LRESULT CFPCWnd::OnContextMenu(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
    bHandled = !m_bStandardMenu;

    return 0;
}

LRESULT CFPCWnd::OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	Stop();

	if (0 != m_TimerId__NtHitTest)
	{
		KillTimer(m_hWnd, m_TimerId__NtHitTest);
		m_TimerId__NtHitTest = 0;
	}

    ReleaseAll();

    bHandled = FALSE;

    return 0;
}

LRESULT CFPCWnd::OnWindowlessMouseMessage(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
    LRESULT lRes = 0;

    bHandled = FALSE;

    if (m_bInPlaceActive && m_bWindowless && m_pOleInPlaceObjectWindowless.IsNotNull())
    {
        /* HRESULT hr = */ m_pOleInPlaceObjectWindowless->OnWindowMessage(uMsg, wParam, lParam, &lRes);
	}

    return lRes;
}

LRESULT CFPCWnd::OnWindowMessage(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
    if (!m_bStandardMenu && 
        WM_KEYDOWN == uMsg && 
        VK_APPS == wParam)
    {
        bHandled = TRUE;
        return 0;
    }

    LRESULT lRes = 0;
 
    HRESULT hr = S_FALSE;

    if (m_bInPlaceActive && m_bWindowless && m_pOleInPlaceObjectWindowless.IsNotNull())
        hr = m_pOleInPlaceObjectWindowless->OnWindowMessage(uMsg, wParam, lParam, &lRes);

    bHandled = S_OK == hr;

    return lRes;
}

LRESULT CFPCWnd::OnQueryInterface(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
    SFPCQueryInterface* pFCQueryInterface = (SFPCQueryInterface*)lParam;

    if (m_pUnknown.IsNotNull())
        pFCQueryInterface->hr = 
            m_pUnknown->QueryInterface(pFCQueryInterface->iid, &pFCQueryInterface->pvObject);
    else
        pFCQueryInterface->hr = E_POINTER;

    return 0;
}

LRESULT CFPCWnd::OnPutMovieFromMemory(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
    if (m_pUnknown.IsNull())
        return 0;

    SFPCPutMovieFromMemory* pFCPutMovieFromMemory = (SFPCPutMovieFromMemory*)lParam;

    m_pFlashOCXLoader->GetContentManager().LoadMovieFromMemory(m_hWnd, pFCPutMovieFromMemory->lpData, pFCPutMovieFromMemory->dwSize);

    return 1;
}

LRESULT CFPCWnd::OnLoadMovieFromMemory(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
    if (m_pUnknown.IsNull())
        return 0;

    SFPCLoadMovieFromMemory* pFCLoadMovieFromMemory = (SFPCLoadMovieFromMemory*)lParam;

    m_pFlashOCXLoader->GetContentManager().LoadMovieFromMemory(
        m_hWnd, 
        pFCLoadMovieFromMemory->lpData, 
        pFCLoadMovieFromMemory->dwSize, 
        TRUE, 
        pFCLoadMovieFromMemory->layer);

    return 1;
}

LRESULT CFPCWnd::OnGetFrameBitmap(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
    // DRF - Kaneva - Added
	auto pGFB = (SFPCGetFrameBitmap*)lParam;
	if (!pGFB)
		return 0;

	// It seems that this 3rd party app:
	// http://www.rm.com/Support/GeneralDownload.asp?cref=DWN605616&nav=0&referrer=KLSearch&Search=Easiteach%20Reader&recordno=4&cluster=All
	// EasiTeach is sending FPCM_GET_FRAME_BITMAP with lParam == NULL
	if (IsBadWritePtr(&(pGFB->hBitmap), sizeof(pGFB->hBitmap )))
		return 0;

	// DRF - Kaneva - Added
	pGFB->hBitmap = NULL;

	if (m_bTransparent)
    {
	    HDC hdcScreen = CreateDC(_T("DISPLAY"), NULL, NULL, NULL);
		
		// DRF - Kaneva - Added
		if (!hdcScreen)
			return 0;

	    HDC hdcMem = CreateCompatibleDC(hdcScreen);

		// DRF - Kaneva - Added
		if (!hdcMem) {
		    ::DeleteDC(hdcScreen);
			return 0;
		}

	    RECT rcClient;
	    GetClientRect(m_hWnd, &rcClient);

	    RECT rcUpdateRect = rcClient;

	    int nClientWidth = rcClient.right - rcClient.left;
	    int nClientHeight = rcClient.bottom - rcClient.top;

	    int nUpdateWidth = rcUpdateRect.right - rcUpdateRect.left;
	    int nUpdateHeight = rcUpdateRect.bottom - rcUpdateRect.top;

	    BITMAPINFO bitmap_info; DEF_ZERO_IT(bitmap_info);
	    f_in_box::MemoryZero(&bitmap_info, sizeof(bitmap_info));
	    bitmap_info.bmiHeader.biSize = sizeof(bitmap_info.bmiHeader);
	    bitmap_info.bmiHeader.biWidth = nClientWidth;
	    bitmap_info.bmiHeader.biHeight = nClientHeight;
	    bitmap_info.bmiHeader.biPlanes = 1;
	    bitmap_info.bmiHeader.biBitCount = 32;

	    LPVOID pBits;
	    HBITMAP hBmp = CreateDIBSection(hdcMem, &bitmap_info, DIB_RGB_COLORS, &pBits, 0, 0);

		// DRF - Kaneva - Added
		if (!hBmp || !pBits) {
		    ::DeleteDC(hdcMem);
		    ::DeleteDC(hdcScreen);
			return 0;
		}

	    LPVOID pBitsWhite;
	    HBITMAP hBmpWhite = CreateDIBSection(hdcMem, &bitmap_info, DIB_RGB_COLORS, &pBitsWhite, 0, 0);

		// DRF - Kaneva - Added
		if (!hBmpWhite || !pBitsWhite) {
		    ::DeleteDC(hdcMem);
		    ::DeleteDC(hdcScreen);
		    ::DeleteObject(hBmp);
			return 0;
		}

	    LPVOID pBitsBlack;
	    HBITMAP hBmpBlack = CreateDIBSection(hdcMem, &bitmap_info, DIB_RGB_COLORS, &pBitsBlack, 0, 0);

		// DRF - Kaneva - Added
		if (!hBmpBlack || !pBitsBlack) {
		    ::DeleteDC(hdcMem);
		    ::DeleteDC(hdcScreen);
		    ::DeleteObject(hBmp);
		    ::DeleteObject(hBmpWhite);
			return 0;
		}

		HGDIOBJ hOldBitmap = SelectObject(hdcMem, hBmpWhite);
	    HBRUSH hBrushWhite = CreateSolidBrush(RGB(0xff, 0xff, 0xff));
		FillRect(hdcMem, &rcClient, hBrushWhite);
	    DeleteObject(hBrushWhite);

	    // Draw to entire white picture
	    Draw(hdcMem, &rcClient);

	    SelectObject(hdcMem, hBmpBlack);
	    HBRUSH hBrushBlack = CreateSolidBrush(RGB(0x00, 0x00, 0x00));
	    FillRect(hdcMem, &rcClient, hBrushBlack);
	    DeleteObject(hBrushBlack);

	    // Draw to entire black picture
	    Draw(hdcMem, &rcClient);

	    int X = rcUpdateRect.left;
	    int Y = rcUpdateRect.top;
	    int i = nClientWidth * (rcClient.bottom - 1 - Y) + X;
	    int nDelta = 
		    (nClientWidth * (rcClient.bottom - 1) + rcUpdateRect.left) - 
		    (nClientWidth * (rcClient.bottom - 0) + (rcUpdateRect.right - 1) ) - 1;

	    PDWORD pCurBitsWhite = PDWORD(pBitsWhite) + i;
	    PDWORD pCurBitsBlack = PDWORD(pBitsBlack) + i;
	    PDWORD pCurBits = PDWORD(pBits) + i;

	    for (; Y < rcUpdateRect.bottom; Y++, i += nDelta, pCurBitsWhite += nDelta, pCurBitsBlack += nDelta, pCurBits += nDelta)
		    for (X = rcUpdateRect.left; X < rcUpdateRect.right; X++, i++, pCurBitsWhite++, pCurBitsBlack++, pCurBits++)
		    {
			    DWORD ColorWhite = *pCurBitsWhite;
			    DWORD ColorBlack = *pCurBitsBlack;
			    DWORD Alpha = (ColorWhite & 0x000000ff) - (ColorBlack & 0x000000ff);
			    Alpha = ~Alpha;

			    *pCurBits = (Alpha << 24) + (ColorBlack & 0x00ffffff);
		    }

	    SelectObject(hdcMem, hOldBitmap);

	    ::DeleteDC(hdcMem);
	    ::DeleteDC(hdcScreen);

	    ::DeleteObject(hBmpWhite);
	    ::DeleteObject(hBmpBlack);

	    pGFB->hBitmap = hBmp;

	    return NULL != hBmp;
    }
    else
    {
        HWND hwndFlashActiveX = ::GetWindow(m_hWnd, GW_CHILD);

        RECT rc;
        ::GetClientRect(hwndFlashActiveX, &rc);

        HDC hDC__Screen = CreateDC(_T("DISPLAY"), NULL, NULL, NULL);

		// DRF - Kaneva - Added
		if (!hDC__Screen)
			return 0;

		HDC hDC__Mem = CreateCompatibleDC(hDC__Screen);
        
		// DRF - Kaneva - Added
		if (!hDC__Mem) {
		    ::DeleteDC(hDC__Screen);
			return 0;
		}

		HBITMAP hBitmap = CreateCompatibleBitmap(hDC__Screen, rc.right, rc.bottom);

		// DRF - Kaneva - Added
		if (!hBitmap) {
		    ::DeleteDC(hDC__Mem);
		    ::DeleteDC(hDC__Screen);
			return 0;
		}

		HBITMAP hOldBitmap = (HBITMAP)SelectObject(hDC__Mem, hBitmap);

		// DRF - Kaneva - Added
		if (!hOldBitmap) {
		    ::DeleteDC(hDC__Mem);
		    ::DeleteDC(hDC__Screen);
		    ::DeleteObject(hBitmap);
			return 0;
		}

        ::SendMessage(hwndFlashActiveX, WM_PAINT, (WPARAM)hDC__Mem, 0);
		
        if (0 != lstrcmpiA(m_strContext.c_str(), DEF_RIGHT_CONTEXT))
	    {
		    char szSomeText[] = "CDLN?uDqrHNM";

		    for (char* p = szSomeText; NULL != *p; p++)
			    *p = (*p ^ 16) - 15;

		    DrawTextA(hDC__Mem, szSomeText, -1, &rc, DT_LEFT | DT_TOP | DT_SINGLELINE);
	    }
		
        SelectObject(hDC__Mem, hOldBitmap);

        DeleteDC(hDC__Mem);
        DeleteDC(hDC__Screen);

        pGFB->hBitmap = hBitmap;

        return NULL != hBitmap;
    }
}

LRESULT CFPCWnd::OnPutStandardMenu(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
    SFPCPutStandardMenu* pFPCPutStandardMenu = (SFPCPutStandardMenu*)lParam;

    m_bStandardMenu = pFPCPutStandardMenu->StandardMenu;

    m_FlashAXWnd.SetStandardMenu(m_bStandardMenu);

    return 1;
}

LRESULT CFPCWnd::OnGetStandardMenu(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
    SFPCGetStandardMenu* pFPCGetStandardMenu = (SFPCGetStandardMenu*)lParam;

    pFPCGetStandardMenu->StandardMenu = m_bStandardMenu;

    return 1;
}

LRESULT CFPCWnd::OnPutOverallOpaque(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
    SFPCPutOverallOpaque* pFPCPutOverallOpaque = (SFPCPutOverallOpaque*)lParam;

    m_nOverallOpaque = (BYTE)pFPCPutOverallOpaque->Value;

    RedrawWindow(m_hWnd, NULL, NULL, RDW_INVALIDATE);

    return 1;
}

LRESULT CFPCWnd::OnGetOverallOpaque(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
    SFPCGetOverallOpaque* pFPCGetOverallOpaque = (SFPCGetOverallOpaque*)lParam;

    pFPCGetOverallOpaque->Value = m_nOverallOpaque;

    return 1;
}

LRESULT CFPCWnd::OnSetEventListener(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
    DEF_PRIVATE_MESSAGE__SET_EVENT_LISTENER_Data* pInfo = 
        (DEF_PRIVATE_MESSAGE__SET_EVENT_LISTENER_Data*)lParam;

    CCriticalSectionOwner cso(m_csEventListener);
    {
        m_LPARAM_EventListenerParam = pInfo->lParam;
        m_pEventListener = pInfo->pListener;
    }

    return 1;
}

LRESULT CFPCWnd::OnCallLoadExternalResource(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
    DEF_PRIVATE_MESSAGE__CALL_LOAD_EXTERNAL_RESOURCE_Data* pInfo = 
        (DEF_PRIVATE_MESSAGE__CALL_LOAD_EXTERNAL_RESOURCE_Data*)lParam;

    HWND hwndParent = ::GetParent(m_hWnd);
    UINT uId = (UINT)::GetWindowLong(m_hWnd, GWL_ID);

    {
        SFPCLoadExternalResourceA info;

        info.hdr.code = FPCN_LOADEXTERNALRESOURCEA;
        info.hdr.hwndFrom = m_hWnd;
        info.hdr.idFrom = uId;

        info.lpszRelativePath = pInfo->bstrPath;
        info.lpStream = pInfo->pStream;

        if (NULL != hwndParent)
            ::SendMessage(hwndParent, WM_NOTIFY, uId, (LPARAM)&info);

        CCriticalSectionOwner cso(m_csEventListener);
        {
            if (NULL != m_pEventListener)
                m_pEventListener(m_hWnd, m_LPARAM_EventListenerParam, (NMHDR*)&info);
        }
    }
    {
        SFPCLoadExternalResourceExA info;

        info.hdr.code = FPCN_LOADEXTERNALRESOURCEEXA;
        info.hdr.hwndFrom = m_hWnd;
        info.hdr.idFrom = uId;

        info.lpszRelativePath = pInfo->bstrPath;
        info.lpStream = pInfo->pStream;
        info.bHandled = pInfo->bHandled;

        if (NULL != hwndParent)
            ::SendMessage(hwndParent, WM_NOTIFY, uId, (LPARAM)&info);

        CCriticalSectionOwner cso(m_csEventListener);
        {
            if (NULL != m_pEventListener)
                m_pEventListener(m_hWnd, m_LPARAM_EventListenerParam, (NMHDR*)&info);
        }

        pInfo->bHandled = info.bHandled;
    }

    {
        SFPCLoadExternalResourceW info;

        info.hdr.code = FPCN_LOADEXTERNALRESOURCEW;
        info.hdr.hwndFrom = m_hWnd;
        info.hdr.idFrom = uId;

        info.lpszRelativePath = pInfo->bstrPath;
        info.lpStream = pInfo->pStream;

        if (NULL != hwndParent)
            ::SendMessage(hwndParent, WM_NOTIFY, uId, (LPARAM)&info);

        CCriticalSectionOwner cso(m_csEventListener);
        {
            if (NULL != m_pEventListener)
                m_pEventListener(m_hWnd, m_LPARAM_EventListenerParam, (NMHDR*)&info);
        }
    }
    {
        SFPCLoadExternalResourceExW info;

        info.hdr.code = FPCN_LOADEXTERNALRESOURCEEXW;
        info.hdr.hwndFrom = m_hWnd;
        info.hdr.idFrom = uId;

        info.lpszRelativePath = pInfo->bstrPath;
        info.lpStream = pInfo->pStream;
        info.bHandled = pInfo->bHandled;

        if (NULL != hwndParent)
            ::SendMessage(hwndParent, WM_NOTIFY, uId, (LPARAM)&info);

        CCriticalSectionOwner cso(m_csEventListener);
        {
            if (NULL != m_pEventListener)
                m_pEventListener(m_hWnd, m_LPARAM_EventListenerParam, (NMHDR*)&info);
        }

        pInfo->bHandled = info.bHandled;
    }

    return 1;
}

//
DWORD CFPCWnd::GetFlashVersion()
{
    long lVersion = 0;

    if (m_pShockwaveFlash3.IsNotNull())
        m_pShockwaveFlash3->FlashVersion(&lVersion);

    DWORD dwMajorVersion = LOBYTE(HIWORD(lVersion));

    if (dwMajorVersion > 8)
	    dwMajorVersion = 8;

    return dwMajorVersion;
}

// Send notification FPCN_PAINT
void CFPCWnd::SendPaintNotification(LPVOID lpPixels)
{
    HWND hwndParent = ::GetParent(m_hWnd);

    //
    UINT uId = (UINT)GetWindowLong(m_hWnd, GWL_ID);

    SFPCNPaint info;

    info.hdr.code = FPCN_PAINT;
    info.hdr.hwndFrom = m_hWnd;
    info.hdr.idFrom = uId;

    info.lpPixels = (LPDWORD)lpPixels;

    if (NULL != hwndParent)
        ::SendMessage(hwndParent, WM_NOTIFY, uId, (LPARAM)&info);

    CCriticalSectionOwner cso(m_csEventListener);
    {
        if (NULL != m_pEventListener)
            m_pEventListener(m_hWnd, m_LPARAM_EventListenerParam, (NMHDR*)&info);
    }
}

void CFPCWnd::ReleaseAll()
{
    if (m_bReleaseAll)
        return;

    m_bReleaseAll = TRUE;

    if (m_pViewObject.IsNotNull())
        m_pViewObject->SetAdvise(DVASPECT_CONTENT, 0, NULL);

    if (m_pOleObject.IsNotNull())
    {
        m_pOleObject->Close(OLECLOSE_NOSAVE);
        m_pOleObject->SetClientSite(NULL);
    }

    if (m_pUnknown.IsNotNull())
    {
        f_in_box::com_helpers::CComPtr<IObjectWithSite> pSite;
    
        m_pUnknown->QueryInterface(IID_IObjectWithSite, (void**)&pSite);
    
        if (pSite.IsNotNull())
            pSite->SetSite(NULL);
    }

    m_pUnkSite.Release();

    m_dwViewObjectType = 0;
    m_bInPlaceActive = FALSE;
    m_bWindowless = FALSE;
    m_bInPlaceActive = FALSE;
    m_bUIActive = FALSE;
    m_bCapture = FALSE;
    m_bReleaseAll = FALSE;

    if (NULL != m_hAccel)
    {
        DestroyAcceleratorTable(m_hAccel);
        m_hAccel = NULL;
    }

    // Unadvise
    if (m_bSubcribedToEvents)
    {
        HRESULT hr;

        f_in_box::com_helpers::CComPtr<IConnectionPointContainer> pFlashConnectionPointContainer;
    
        m_pUnknown->QueryInterface(IID_IConnectionPointContainer, (void**)&pFlashConnectionPointContainer);

        f_in_box::com_helpers::CComPtr<IConnectionPoint> pFlashConnectionPoint;

        hr = 
            pFlashConnectionPointContainer->FindConnectionPoint(f_in_box::flash::IShockwaveFlashEvents::GetGUID(), 
                                                                &pFlashConnectionPoint);

        hr = pFlashConnectionPoint->Unadvise(m_dwCookie);

        m_bSubcribedToEvents = FALSE;
    }

    m_pOleObject.Release();
    m_pOleInPlaceObjectWindowless.Release();
    m_pUnknown.Release();
    m_pViewObject.Release();
    m_pViewObjectEx.Release();

    m_pShockwaveFlash3.Release();
    m_pShockwaveFlash4.Release();
    m_pShockwaveFlash5.Release();
    m_pShockwaveFlash6.Release();
    m_pShockwaveFlash7.Release();
    m_pShockwaveFlash8.Release();

    //m_pHTMLDocument.Release();
}

LRESULT CFPCWnd::OnPutMovieUsingStream(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
    if (m_pUnknown.IsNull())
        return 0;

    SFPCPutMovieUsingStream* pInfo = (SFPCPutMovieUsingStream*)lParam;

    m_pFlashOCXLoader->GetContentManager().LoadMovieUsingStream(m_hWnd, &pInfo->pStream);

    return 1;
}

LRESULT CFPCWnd::OnLoadMovieUsingStream(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
    if (m_pUnknown.IsNull())
        return 0;

    SFPCLoadMovieUsingStream* pInfo = (SFPCLoadMovieUsingStream*)lParam;

    m_pFlashOCXLoader->GetContentManager().LoadMovieUsingStream(
        m_hWnd, 
        &pInfo->pStream, 
        TRUE, 
        pInfo->layer);

    return 1;
}

HRESULT STDMETHODCALLTYPE CFPCWnd::SetSite(IUnknown* pUnkSite)
{
    m_pUnkSite = pUnkSite;

    return S_OK;
}

HRESULT STDMETHODCALLTYPE CFPCWnd::GetSite(REFIID riid, void** ppvSite)
{
    return m_pUnkSite->QueryInterface(riid, ppvSite);
}

LRESULT CFPCWnd::OnCallFunction(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
    SFPCCallFunction* pInfo = (SFPCCallFunction*)lParam;

    if (m_pShockwaveFlash8.IsNull())
    {
        pInfo->hr = E_FAIL;

        return 0;
    }

	// Check the version
	long lVersion = 0;
	m_pShockwaveFlash8->FlashVersion(&lVersion);
	DWORD dwMajorVersion = LOBYTE(HIWORD(lVersion));

	if (dwMajorVersion < 8)
		// Unsupported
    {
        pInfo->hr = E_NOTIMPL;

        return 0;
    }

	// Call the method
    pInfo->hr = m_pShockwaveFlash8->CallFunction(pInfo->bstrRequest, &pInfo->bstrResponse);

    return 1;
}

LRESULT CFPCWnd::OnSetReturnValueA(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
    SFPCSetReturnValueA* pInfo = (SFPCSetReturnValueA*)lParam;

    if (m_pShockwaveFlash8.IsNull())
    {
        pInfo->hr = E_FAIL;

        return 0;
    }

	// Check the version
	long lVersion = 0;
	m_pShockwaveFlash8->FlashVersion(&lVersion);
	DWORD dwMajorVersion = LOBYTE(HIWORD(lVersion));

	if (dwMajorVersion < 8)
		// Unsupported
    {
        pInfo->hr = E_NOTIMPL;

        return 0;
    }

	// Call the method
    pInfo->hr = m_pShockwaveFlash8->SetReturnValue(f_in_box::com_helpers::CBSTR(pInfo->lpszReturnValue));

    return 1;
}

LRESULT CFPCWnd::OnSetReturnValueW(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
    SFPCSetReturnValueW* pInfo = (SFPCSetReturnValueW*)lParam;

    if (m_pShockwaveFlash8.IsNull())
    {
        pInfo->hr = E_FAIL;

        return 0;
    }

	// Check the version
	long lVersion = 0;
	m_pShockwaveFlash8->FlashVersion(&lVersion);
	DWORD dwMajorVersion = LOBYTE(HIWORD(lVersion));

	if (dwMajorVersion < 8)
		// Unsupported
    {
        pInfo->hr = E_NOTIMPL;

        return 0;
    }

	// Call the method
    pInfo->hr = m_pShockwaveFlash8->SetReturnValue(f_in_box::com_helpers::CBSTR(pInfo->lpszReturnValue));

    return 1;
}

void CFPCWnd::PaintTo(HDC hDC)
{
	Draw(hDC, &m_rcPos);
}

void CFPCWnd::CallPaintHandler(HDC hdc, DWORD dwStage)
{
	SFPCNPaintStage info;

	info.hdc = hdc;
	info.dwStage = dwStage;

    HWND hwndParent = ::GetParent(m_hWnd);
    UINT uId = (UINT)::GetWindowLong(m_hWnd, GWL_ID);

	info.hdr.code = FPCN_PAINT_STAGE;
	info.hdr.hwndFrom = m_hWnd;
	info.hdr.idFrom = uId;

	if (NULL != hwndParent)
		::SendMessage(hwndParent, WM_NOTIFY, uId, (LPARAM)&info);

	CCriticalSectionOwner cso(m_csEventListener);
	{
		if (NULL != m_pEventListener)
			m_pEventListener(m_hWnd, m_LPARAM_EventListenerParam, (NMHDR*)&info);
	}
}

LRESULT CFPCWnd::OnGetObjectPtr(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	DEF_PRIVATE_MESSAGE__GET_OBJECT_PTR_Data* pInfo = 
        (DEF_PRIVATE_MESSAGE__GET_OBJECT_PTR_Data*)lParam;

	pInfo->pObject = this;

	return 1;
}

CFPCWnd* CFPCWnd::FromHWND(HWND hwndFlashPlayerControl)
{
	DEF_PRIVATE_MESSAGE__GET_OBJECT_PTR_Data info; DEF_ZERO_IT(info);

	::SendMessage(hwndFlashPlayerControl, DEF_PRIVATE_MESSAGE__GET_OBJECT_PTR, 0, (LPARAM)&info);

	return info.pObject;
}

com_helpers::CComPtr<CFlashOCXLoader> CFPCWnd::GetOCXLoader() const
{
	return m_pFlashOCXLoader;
}

BOOL CFPCWnd::CallWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam, LRESULT* plResult, BOOL* pbHandled)
{
	*plResult = InternalWindowProc(hWnd, message, wParam, lParam, *pbHandled);

	return TRUE;
}

LRESULT CFPCWnd::OnTimer(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	if (wParam == m_TimerId__NtHitTest)
	{
		POINT pt;
		GetCursorPos(&pt);

		::ScreenToClient(::GetParent(m_hWnd), &pt);
		::PostMessage(::GetParent(m_hWnd), WM_MOUSEMOVE, 0, MAKELPARAM(pt.x, pt.y));

		bHandled = TRUE;

		return 0;
	}

	bHandled = FALSE;

	return 1;
}

HRESULT STDMETHODCALLTYPE CFPCWnd::QueryService(REFGUID guidService, REFIID riid, void** ppvObject)
{
	HRESULT hr = E_NOTIMPL;

	if (f_in_box::com_helpers::IsEqualGUID(IID_IDirectDraw3, guidService))
	{
		if (m_pDirectDraw.IsNotNull())
		{
			hr = m_pDirectDraw->QueryInterface(riid, ppvObject);
		}
	}
	else if (f_in_box::com_helpers::IsEqualGUID(IID_ITimerService, guidService))
	{
        hr = CreateTimerServiceImpl(riid, ppvObject);
	}

	return hr;
}

void CFPCWnd::Draw(HDC hDC, LPRECT pRect)
{
	if (m_pDirectDraw.IsNotNull())
	{
		// Create / recreate surface and get its HDC

		if (m_pDirectDrawSurface.IsNull() || m_SurfaceSize.cx != pRect->right || m_SurfaceSize.cy != pRect->bottom)
		{
			DDSURFACEDESC ddsd;
			f_in_box::MemoryZero(&ddsd, sizeof(ddsd));
			ddsd.dwSize = sizeof(ddsd);
			ddsd.dwFlags = DDSD_PIXELFORMAT | DDSD_CAPS | DDSD_WIDTH | DDSD_HEIGHT;
			ddsd.dwHeight = pRect->bottom;
			ddsd.dwWidth = pRect->right;
			ddsd.ddpfPixelFormat.dwSize = sizeof(ddsd.ddpfPixelFormat);
			ddsd.ddpfPixelFormat.dwFlags = DDPF_RGB;
			ddsd.ddpfPixelFormat.dwFourCC = 0;
			ddsd.ddpfPixelFormat.dwRGBBitCount = 0x00000020;
			ddsd.ddpfPixelFormat.dwRBitMask = 0x00ff0000;
			ddsd.ddpfPixelFormat.dwGBitMask = 0x0000ff00;
			ddsd.ddpfPixelFormat.dwBBitMask = 0xff;
			ddsd.ddsCaps.dwCaps = DDSCAPS_SYSTEMMEMORY | DDSCAPS_VIDEOMEMORY | DDSCAPS_OWNDC;

			HRESULT hr = m_pDirectDraw->CreateSurface(&ddsd, &m_pDirectDrawSurface, NULL);

			if (S_OK == hr)
			{
				hr = m_pDirectDrawSurface->GetDC(&m_hdcDirectDrawSurface);

				if (S_OK != hr)
				{
					m_hdcDirectDrawSurface = NULL;
				}
			}

			m_SurfaceSize.cx = pRect->right;
			m_SurfaceSize.cy = pRect->bottom;
		}
	}

	int nWidth = pRect->right - pRect->left;
	int nHeight = pRect->bottom - pRect->top;

	if (NULL != m_hdcDirectDrawSurface)
	{
		BitBlt(m_hdcDirectDrawSurface, pRect->left, pRect->top, nWidth, nHeight, hDC, pRect->left, pRect->top, SRCCOPY);
		m_pViewObject->Draw(DVASPECT_CONTENT, -1, NULL, NULL, NULL, m_hdcDirectDrawSurface, (RECTL*)pRect, (RECTL*)pRect, NULL, NULL); 
		BitBlt(hDC, pRect->left, pRect->top, nWidth, nHeight, m_hdcDirectDrawSurface, pRect->left, pRect->top, SRCCOPY);
	}
	else
	{
		m_pViewObject->Draw(DVASPECT_CONTENT, -1, NULL, NULL, NULL, hDC, (RECTL*)pRect, (RECTL*)pRect, NULL, NULL); 
	}




    if (0 != lstrcmpiA(m_strContext.c_str(), DEF_RIGHT_CONTEXT))
    {
        if ( 0 == ( ( GetTickCount() - m_dwStartTime ) / 1000 / 5 ) % 2 )
        {
            char szSomeText[] = "CDLN?uDqrHNM";

            for (char* p = szSomeText; NULL != *p; p++)
                *p = (*p ^ 16) - 15;

			int nOldBkMode = SetBkMode(hDC, TRANSPARENT);
            DrawTextA(hDC, szSomeText, -1, &m_rcPos, DT_LEFT | DT_TOP | DT_SINGLELINE);
			SetBkMode(hDC, nOldBkMode);
        }
    }

}
