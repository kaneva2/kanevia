#include "stdafx.h"
#include "Wnd.h"

namespace f_in_box
{
    class CFlashAXWnd: public CWnd
    {
    private:
        BOOL m_bStandardMenu;





        DWORD m_dwStartTime;
        f_in_box::std::CString<CHAR> m_strContext;
        UINT m_uMsg__SetContext;
        UINT_PTR m_nTimerId;



    public:
        CFlashAXWnd()

            :





    m_dwStartTime(GetTickCount()), 
    m_uMsg__SetContext(RegisterWindowMessageA(DEF_PRIVATE_MESSAGE__SET_CONTEXT)), 
    m_nTimerId(0), 



	m_bStandardMenu(FALSE)

        {
        }

        void SetStandardMenu(BOOL bStandardMenu)
        {
            m_bStandardMenu = bStandardMenu;
        }

        void Subclass(HWND hWnd)
        {
            CWnd::Subclass(hWnd, FALSE);





            m_nTimerId = SetTimer(m_hWnd, 120, 5000, NULL);



        }

    private:
        LRESULT
        CALLBACK
        WindowProc(
            HWND hWnd,
            UINT uMsg,
            WPARAM wParam,
            LPARAM lParam
        )
        {
            if (WM_RBUTTONDOWN == uMsg || 
                WM_RBUTTONUP == uMsg || 
                WM_CONTEXTMENU == uMsg)
                if (!m_bStandardMenu)
                    return 0;

            if (WM_KEYDOWN == uMsg && VK_APPS == wParam)
                if (!m_bStandardMenu)
                    return 0;





            if (WM_TIMER == uMsg)
                if (0 != lstrcmpiA(m_strContext.c_str(), DEF_RIGHT_CONTEXT))
	                if (wParam == m_nTimerId)
	                {
	                    InvalidateRect(m_hWnd, NULL, TRUE);
	                    UpdateWindow(m_hWnd);
	                }

            if (WM_PAINT == uMsg)
                if (0 != lstrcmpiA(m_strContext.c_str(), DEF_RIGHT_CONTEXT))
                {
                    LRESULT lResult = CallOldWindowProc(hWnd, uMsg, wParam, lParam);

                    if ( 0 == ( ( GetTickCount() - m_dwStartTime ) / 1000 / 5 ) % 2 )
                    {
						HDC hDC = GetDC(m_hWnd);
						RECT rcClient;

						GetClientRect(m_hWnd, &rcClient);

						char szSomeText[] = "CDLN?uDqrHNM";

						for (char* p = szSomeText; NULL != *p; p++)
							*p = (*p ^ 16) - 15;

						DrawText(hDC, szSomeText, -1, &rcClient, DT_LEFT | DT_TOP | DT_SINGLELINE);

						ReleaseDC(m_hWnd, hDC);
                    }

                    return lResult;
                }

            if (m_uMsg__SetContext == uMsg)
            {
                m_strContext = (char*)lParam;

                return 0;
            }



			if (WM_GETDLGCODE == uMsg)
			{
				if (0 != (GetWindowLong(GetParent(m_hWnd), GWL_STYLE) & FPCS_NEED_ALL_KEYS))
					return DLGC_WANTALLKEYS;
			}

            return CallOldWindowProc(hWnd, uMsg, wParam, lParam);
        }
    };
}
