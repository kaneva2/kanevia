#ifndef	COMMON32_H
#define	COMMON32_H

namespace f_in_box
{
	namespace Utils
	{
		namespace MoveInstruction
		{

#define	EXPORTS	__declspec(dllexport)
#define PRE_66   0x01
#define PRE_67   0x02

#define F_IMM8          0x00000001
#define F_IMM16         0x00000002
#define F_IMM32         0x00000008
#define	F_IMM64			0x00000010
#define F_RELATIVE		0x00000020

			//typedef char* pchar;
			//typedef unsigned __int8		BYTE;
			//typedef unsigned __int16	WORD;
			//typedef unsigned __int32	DWORD;
			//typedef unsigned __int64	LONGLONG;

			enum regs {
			//---------------+--------------------------+---------------+-----------+---------------+
			//               |         mod <= 1         |               |           |               |
			//  mod = 11b	 | mod = 00b|	mod = 01b	|	mod = 10b	|	SIBR2	|	SIBR1		|
			//---------------+----------+---------------+---------------+-----------+---------------+
				EAX = 0,//000|	[EAX]	| [EAX]+offset8	| [EAX]+offset32|	EAX		|	EAX			|
				ECX = 1,//001|	[ECX]	| [ECX]+offset8	| [ECX]+offset32|	ECX		|	ECX			|
				EDX = 2,//010|	[EDX]	| [EDX]+offset8	| [EDX]+offset32|	EDX		|	EDX			|
				EBX = 3,//011|	[EBX]	| [EBX]+offset8	| [EBX]+offset32|	EBX		|	EBX			|
				ESP = 4,//100|	 +SIB	| +SIB+offset8	| +SIB+offset32	| DONTUSER2	|	ESP			|
				EBP = 5,//101| +offset32| [EBP]+offset8	| [EBP]+offset32|	EBP		|offset32/EBP*	| ���� mod=00b, �� offset32, ����� - EBP
				ESI = 6,//110|	[ESI]	| [ESI]+offset8	| [ESI]+offset32|	ESI		|	ESI			|
				EDI = 7,//111|	[EDI]	| [EDI]+offset8	| [EDI]+offset32|	EDI		|	EDI			|
			//---------------+----------+---------------+---------------+-----------+---------------+
			};

			typedef	struct	sib_s {
				BYTE	base:3;
				BYTE	index:3;
				BYTE	scale:2;	//1,2,4,8
			} sib_t;

			typedef struct	rex_s {
				BYTE	b:1;
				BYTE	x:1;
				BYTE	r:1;
				BYTE	w:1;
				BYTE	reserved:4;
			} rex_t;

			typedef struct	mod_rm_s {
				BYTE	rm:3;
				BYTE	reg:3;
				BYTE	mod:2;
			} mod_rm_t;

#pragma pack(push,1)
			typedef struct instruction_s {
				BYTE		length;
				//
				BYTE		opcode[2];
				mod_rm_t	modrm;
				sib_t		sib;
				rex_t		rex;
				//
				union {
					BYTE	imm8;
					WORD	imm16;
					DWORD	imm32;
					LONGLONG	imm64;
				} imm;
				BYTE* pLocalPointerToImm;		// ��������� �� ��������� � pBuffer
				DWORD		flags;
			} instruction_t;
#pragma pack(pop)

			// Returns size of instruction
			BYTE extract_instruction32bits(BYTE *pBuffer, instruction_t *hs);
			BYTE extract_instruction64bits(BYTE *pBuffer, instruction_t *hs);

			// Copy instruction to new address
			bool MoveInstruction32( void* pFrom, void* pTo, size_t Offset ); // true ���� ���� ��������
			bool MoveInstruction64( void* pFrom, void* pTo, size_t Offset ); // true ���� ���� ��������

			BYTE GetInstruction(PVOID pBuffer, instruction_t* p);
			bool MoveInstruction(void* pFrom, void* pTo, size_t Offset);
		}
	}
}

#endif