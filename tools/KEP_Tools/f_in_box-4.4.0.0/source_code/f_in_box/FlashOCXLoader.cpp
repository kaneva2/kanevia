#include "stdafx.h"
#include "ntdefs.h"
#include "MemDLL.h"
#include "FlashOCXLoader.h"
#include "ContentManager.h"
#include "FPCWnd.h"
#include "MemManager.h"
#include "mmdeviceapi.h"
#include "MMDeviceEnumeratorProxy.h"
#include "ComPtr.h"
#include "Globals.h"
#include "thunks.h"

extern HINSTANCE g_hInstance;

static LRESULT CALLBACK FlashPlayerControlWindowProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

using namespace f_in_box;
using namespace f_in_box::std;
using namespace f_in_box::com_helpers;

#define DEF_FILE_TO_STREAM_SEEK(file_seek) ( FILE_BEGIN == file_seek ? STREAM_SEEK_SET : ( FILE_CURRENT == file_seek ? STREAM_SEEK_CUR : STREAM_SEEK_END ) )

static f_in_box::std::CString<CHAR> GetUniqueString()
{
    f_in_box::std::CString<CHAR> strResult;

    GUID guid;

    if (S_OK != CoCreateGuid(&guid))
    {
        CHAR lpszStr[1024]; DEF_ZERO_IT(lpszStr);
        wsprintfA(lpszStr, "f_in_box__%d", GetTickCount());

        strResult = lpszStr;
    }
    else
    {
        CHAR szBuffer[1024];
        wsprintfA(szBuffer, "%.8x_%.8x_%.8x_%.8x", guid.Data1, guid.Data2, guid.Data3, guid.Data4);

        strResult = szBuffer;
    }

    return strResult;
}

static
VOID
CALLBACK
TimerProcMinimizeMemory(
	HWND hwnd, 
	UINT uMsg, 
	UINT_PTR idEvent, 
	DWORD dwTime
)
{
	typedef BOOL (WINAPI *P_SetProcessWorkingSetSize)(
		HANDLE hProcess,
		SIZE_T dwMinimumWorkingSetSize,
		SIZE_T dwMaximumWorkingSetSize
	);

	P_SetProcessWorkingSetSize pSetProcessWorkingSetSize;

	(FARPROC&)pSetProcessWorkingSetSize = 
		GetProcAddress(GetModuleHandle(_T("kernel32.dll")), "SetProcessWorkingSetSize");

	if (NULL != pSetProcessWorkingSetSize)
		pSetProcessWorkingSetSize(GetCurrentProcess(), -1, -1);
}

UINT_PTR WINAPI FPC_StartMinimizeMemoryTimer(int nInterval)
{
	return SetTimer(NULL, 0, nInterval, &TimerProcMinimizeMemory);
}

void WINAPI FPC_StopMinimizeMemoryTimer(UINT_PTR nTimerId)
{
	KillTimer(NULL, nTimerId);
}

void WINAPI FPC_StopFPCMinimizeMemoryTimer(HFPC hFPC)
{
	((CFlashOCXLoader*)hFPC)->StopMinimizeMemoryTimer();
}


CFlashOCXLoader::CFlashOCXLoader() : 
    m_bAudioEnabled(TRUE), 
    m_dwAudioVolume(DEF_MAX_FLASH_AUDIO_VOLUME), 
    m_atomWindowClassA(0), 
    m_atomWindowClassW(0), 
    m_lpAdapterCode(NULL), 
	m_nTimerId(0), 
	m_pSoundListener(NULL), 
	m_pPreProcessURLHandler(NULL), 
    m_dwStartTimeForSoundCapturing(-1), 
	m_bShouldFree(FALSE), 
	m_bIsCanFlashUseGPUAccelerationInited(FALSE), 
	m_bWithinGPUAccelerationTest(FALSE), 
	m_bCanFlashUseGPUAcceleration(FALSE)
{
    CoInitialize(NULL);

	m_dwTlsIndex__FocusedHWND = TlsAlloc();
	SaveFocus(DEF_NO_FOCUS_HWND);

    m_ContentManager.SetFlashOCXLoader(this);

	m_nTimerId = FPC_StartMinimizeMemoryTimer(10000);

#ifdef _DEBUG

        TCHAR szBuffer[1024];
        wsprintf(szBuffer, _T("CFlashOCXLoader::CFlashOCXLoader(), this = 0x%.8x\n"), this);
        OutputDebugString(szBuffer);

#endif // !_DEBUG

}

void CFlashOCXLoader::StopMinimizeMemoryTimer()
{
	if (0 != m_nTimerId)
	{
		KillTimer(NULL, m_nTimerId);
		m_nTimerId = 0;
	}
}

HRESULT
CFlashOCXLoader::Load(LPVOID lpCode /* = NULL */ , DWORD dwSizeOfCode /* = 0 */ , LPCSTR szClassNameA /* = NULL */ , LPCWSTR szClassNameW /* = NULL */ )
{
    HRESULT hr = E_FAIL;

    if (NULL == lpCode)
    {
        HKEY hKey;

        if (ERROR_SUCCESS == 
            RegOpenKeyExA(HKEY_CLASSES_ROOT, 
                          "CLSID\\{D27CDB6E-AE6D-11cf-96B8-444553540000}\\InprocServer32", 
                          0, 
                          KEY_READ, 
                          &hKey))
            // OK
        {
            DWORD dwSizeOfPath;

            if (ERROR_SUCCESS == RegQueryValueExA(hKey, "", NULL, NULL, NULL, &dwSizeOfPath))
            {
                LPSTR lpszFlashOCXPath = new CHAR[dwSizeOfPath];

                if (ERROR_SUCCESS == RegQueryValueExA(hKey, "", NULL, NULL, (LPBYTE)lpszFlashOCXPath, &dwSizeOfPath))
                {
                    HANDLE hFile = CreateFileA(lpszFlashOCXPath, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

                    if (INVALID_HANDLE_VALUE != hFile)
                    {
                        DWORD dwSize = GetFileSize(hFile, NULL);
                        HANDLE hFileMapping = CreateFileMapping(hFile, NULL, PAGE_READONLY, 0, dwSize, NULL);

                        if (NULL != hFileMapping)
                        {
                            LPVOID lpData = MapViewOfFile(hFileMapping, FILE_MAP_READ, 0, 0, 0);

                            if (NULL != lpData)
                            {
                                hr = CMemDLL::Load(lpData, dwSize);

                                UnmapViewOfFile(lpData);
                            }

                            CloseHandle(hFileMapping);
                        }

                        CloseHandle(hFile);
                    }
                }

                delete[] lpszFlashOCXPath;
            }

            RegCloseKey(hKey);
        }
    }
    else
        hr = CMemDLL::Load(lpCode, dwSizeOfCode);

    if (S_OK != hr)
        return hr;
    
    // Create adapter that sets this pointer into a tls slot
    m_lpAdapterCode = AllocThunkWithOneParam(&StaticWindowProc, this);

    // Class registration
    f_in_box::std::CString<CHAR> strClassNameA = GetUniqueString();

    m_strWindowClassNameA = NULL == szClassNameA ? strClassNameA.c_str() : szClassNameA;

    WNDCLASSEXA wcex;

    wcex.cbSize = sizeof(wcex);

    wcex.style = CS_DBLCLKS | CS_GLOBALCLASS;
    wcex.lpfnWndProc = (WNDPROC)m_lpAdapterCode;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
    wcex.hInstance = g_hInstance;
	wcex.hIcon = NULL;
	wcex.hCursor = NULL;
    wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = NULL;
    wcex.lpszClassName = m_strWindowClassNameA.c_str();
	wcex.hIconSm = NULL;

    m_atomWindowClassA = RegisterClassExA(&wcex);

    typedef ATOM (WINAPI *P_RegisterClassExW)(CONST WNDCLASSEXW *lpwcx);
    HMODULE hModule__User32 = LoadLibrary(_T("user32.dll"));
    P_RegisterClassExW pRegisterClassExW = 
        (P_RegisterClassExW)::GetProcAddress(hModule__User32, "RegisterClassExW");

    if (NULL != pRegisterClassExW)
    {
        f_in_box::std::CString<WCHAR> strClassNameW = f_in_box::std::AToCW(GetUniqueString().c_str());

        m_strWindowClassNameW = NULL == szClassNameW ? strClassNameW.c_str() : szClassNameW;

        WNDCLASSEXW wcex;

        wcex.cbSize = sizeof(wcex);

        wcex.style = CS_DBLCLKS | CS_GLOBALCLASS;
        wcex.lpfnWndProc = (WNDPROC)m_lpAdapterCode;
	    wcex.cbClsExtra = 0;
	    wcex.cbWndExtra = 0;
        wcex.hInstance = g_hInstance;
	    wcex.hIcon = NULL;
	    wcex.hCursor = NULL; // LoadCursor(NULL, IDC_ARROW);
        wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	    wcex.lpszMenuName = NULL;
        wcex.lpszClassName = m_strWindowClassNameW.c_str();
	    wcex.hIconSm = NULL;

        m_atomWindowClassW = pRegisterClassExW(&wcex);
    }

    FreeLibrary(hModule__User32);

    return hr;
}

CFlashOCXLoader::~CFlashOCXLoader()
{
    if (0 != m_atomWindowClassA)
    {
        UnregisterClass((LPCTSTR)m_atomWindowClassA, g_hInstance);
        m_atomWindowClassA = 0;
    }

    if (0 != m_atomWindowClassW)
    {
        UnregisterClass((LPCTSTR)m_atomWindowClassW, g_hInstance);
        m_atomWindowClassW = 0;
    }

    if (NULL != m_lpAdapterCode)
    {
        VirtualFree(m_lpAdapterCode, 0, MEM_RELEASE);
        m_lpAdapterCode = NULL;
    }

	StopMinimizeMemoryTimer();

	// We call it right here to make sure that no futher winapi calls will be made
	Unload();

	TlsFree(m_dwTlsIndex__FocusedHWND);

    CoUninitialize();

#ifdef _DEBUG

        TCHAR szBuffer[1024];
        wsprintf(szBuffer, _T("CFlashOCXLoader::~CFlashOCXLoader(), this = 0x%.8x\n"), this);
        OutputDebugString(szBuffer);

#endif // !_DEBUG

}

BOOL CFlashOCXLoader::OnProcessImport(LPCSTR lpszLibName, 
                                      LPCSTR lpszFuncName, 
                                      LPVOID& pNewAddress, 
                                      LPVOID& lpParam)
{
    pNewAddress = NULL;

    lpParam = this;

    if (0 == HIWORD(lpszFuncName))
        // This is an ordinal, not name
    {
        CMemDLL::OnProcessImport(lpszLibName, lpszFuncName, pNewAddress, lpParam);
    }
    else if (0 == lstrcmpiA(lpszFuncName, "CreateURLMoniker"))
        pNewAddress = &StaticHook_CreateURLMoniker;
    else if (0 == lstrcmpiA(lpszFuncName, "CreateWindowExA"))
        pNewAddress = &StaticHook_CreateWindowExA;
    else if (0 == lstrcmpiA(lpszFuncName, "RegisterClassExA"))
        pNewAddress = &StaticHook_RegisterClassExA;
    else if (0 == lstrcmpiA(lpszFuncName, "RegisterClassA"))
        pNewAddress = &StaticHook_RegisterClassA;
    else if (0 == lstrcmpiA(lpszFuncName, "LoadCursorA"))
        pNewAddress = &StaticHook_LoadCursorA;
    else if (0 == lstrcmpiA(lpszFuncName, "UnregisterClassA"))
        pNewAddress = &StaticHook_UnregisterClassA;
    else if (0 == lstrcmpiA(lpszFuncName, "LoadMenuA"))
        pNewAddress = &StaticHook_LoadMenuA;
    else if (0 == lstrcmpiA(lpszFuncName, "waveOutWrite"))
        pNewAddress = &StaticHook_waveOutWrite;
    else if (0 == lstrcmpiA(lpszFuncName, "waveOutOpen"))
        pNewAddress = &StaticHook_waveOutOpen;
    else if (0 == lstrcmpiA(lpszFuncName, "waveOutClose"))
        pNewAddress = &StaticHook_waveOutClose;
    else if (0 == lstrcmpiA(lpszFuncName, "CreateFileA"))
        pNewAddress = &StaticHook_CreateFileA;
    else if (0 == lstrcmpiA(lpszFuncName, "CreateFileW"))
        pNewAddress = &StaticHook_CreateFileW;
    else if (0 == lstrcmpiA(lpszFuncName, "FlushInstructionCache"))
        pNewAddress = &StaticHook_FlushInstructionCache;
    else if (0 == lstrcmpiA(lpszFuncName, "LoadLibraryA"))
        pNewAddress = &StaticHook_LoadLibraryA;
    else if (0 == lstrcmpiA(lpszFuncName, "GetClassInfoExA"))
        pNewAddress = &StaticHook_GetClassInfoExA;
    else if (0 == lstrcmpiA(lpszFuncName, "GetClassNameA"))
        pNewAddress = &StaticHook_GetClassNameA;
    else if (0 == lstrcmpiA(lpszFuncName, "GetModuleHandleA"))
        pNewAddress = &StaticHook_GetModuleHandleA;
    else if (0 == lstrcmpiA(lpszFuncName, "LoadLibraryExA"))
        pNewAddress = &StaticHook_LoadLibraryExA;
    else if (0 == lstrcmpiA(lpszFuncName, "FindResourceA"))
        pNewAddress = &StaticHook_FindResourceA;
    else if (0 == lstrcmpiA(lpszFuncName, "FindResourceExA"))
        pNewAddress = &StaticHook_FindResourceExA;
    else if (0 == lstrcmpiA(lpszFuncName, "FindResourceExW"))
        pNewAddress = &StaticHook_FindResourceExW;
    else if (0 == lstrcmpiA(lpszFuncName, "SetTimer"))
        pNewAddress = &StaticHook_SetTimer;
    else if (0 == lstrcmpiA(lpszFuncName, "VirtualAlloc"))
        pNewAddress = &StaticHook_VirtualAlloc;
    else if (0 == lstrcmpiA(lpszFuncName, "VirtualProtect"))
        pNewAddress = &StaticHook_VirtualProtect;
    else if (0 == lstrcmpiA(lpszFuncName, "ReadFile"))
        pNewAddress = &StaticHook_ReadFile;
    else if (0 == lstrcmpiA(lpszFuncName, "CloseHandle"))
        pNewAddress = &StaticHook_CloseHandle;
    else if (0 == lstrcmpiA(lpszFuncName, "GetFileSize"))
        pNewAddress = &StaticHook_GetFileSize;
    else if (0 == lstrcmpiA(lpszFuncName, "GetFileSizeEx"))
        pNewAddress = &StaticHook_GetFileSizeEx;
    else if (0 == lstrcmpiA(lpszFuncName, "SetFilePointer"))
        pNewAddress = &StaticHook_SetFilePointer;
    else if (0 == lstrcmpiA(lpszFuncName, "SetFilePointerEx"))
        pNewAddress = &StaticHook_SetFilePointerEx;
    else if (0 == lstrcmpiA(lpszFuncName, "GetFocus"))
        pNewAddress = &StaticHook_GetFocus;
    else if (0 == lstrcmpiA(lpszFuncName, "CoCreateInstance"))
        pNewAddress = &StaticHook_CoCreateInstance;
    else if (0 == lstrcmpiA(lpszFuncName, "GetClipBox"))
        pNewAddress = &StaticHook_GetClipBox;
    else if (0 == lstrcmpiA(lpszFuncName, "RtlPcToFileHeader"))
        pNewAddress = &StaticHook_RtlPcToFileHeader;
    else if (0 == lstrcmpiA(lpszFuncName, "SetProcessValidCallTargets"))
        pNewAddress = &StaticHook_SetProcessValidCallTargets;
	else
		CMemDLL::OnProcessImport(lpszLibName, lpszFuncName, pNewAddress, lpParam);

    return NULL != pNewAddress;
}

HRESULT
WINAPI
CFlashOCXLoader::StaticHook_CreateURLMoniker(
    IMoniker* pmkContext, 
    LPWSTR szURL, 
    IMoniker** ppmk
)
{
    LPVOID lpOriginalFunc = TlsGetValue(Globals::g_nTlsSlotParam1);
    ASSERT(NULL != lpOriginalFunc);

    CFlashOCXLoader* pThis = (CFlashOCXLoader*)TlsGetValue(Globals::g_nTlsSlotParam2);
    ASSERT(NULL != pThis);

    return
    pThis->Hook_CreateURLMoniker(
            lpOriginalFunc, 

            pmkContext, 
            szURL, 
            ppmk
        );
}

HRESULT
WINAPI
CFlashOCXLoader::Hook_CreateURLMoniker(
    LPVOID lpOriginalFunc, 

    IMoniker* pmkContext, 
    LPWSTR szURL, 
    IMoniker** ppmk
)
{
	BOOL bContinue = TRUE;

	if (NULL != m_pPreProcessURLHandler)
		m_pPreProcessURLHandler((HFPC)this, m_PreProcessURLHandlerlParam, &szURL, &bContinue);

	if (!bContinue)
		return E_FAIL;

	return m_ContentManager.MyCreateURLMoniker(pmkContext, szURL, ppmk);

    //typedef HRESULT (WINAPI *P_CreateURLMoniker)(
    //    IMoniker* pmkContext, 
    //    LPWSTR szURL, 
    //    IMoniker** ppmk
    //);
    //P_CreateURLMoniker pCreateURLMoniker = (P_CreateURLMoniker)lpOriginalFunc;

    //return
    //pCreateURLMoniker(
    //        pmkContext, 
    //        szURL, 
    //        ppmk
    //    );
}

HWND
WINAPI
CFlashOCXLoader::StaticHook_CreateWindowExA(
    DWORD dwExStyle,      // extended window style
    LPCSTR lpClassName,  // registered class name
    LPCSTR lpWindowName, // window name
    DWORD dwStyle,        // window style
    int x,                // horizontal position of window
    int y,                // vertical position of window
    int nWidth,           // window width
    int nHeight,          // window height
    HWND hWndParent,      // handle to parent or owner window
    HMENU hMenu,          // menu handle or child identifier
    HINSTANCE hInstance,  // handle to application instance
    LPVOID lpParam        // window-creation data
)
{
    LPVOID lpOriginalFunc = TlsGetValue(Globals::g_nTlsSlotParam1);
    ASSERT(NULL != lpOriginalFunc);

    CFlashOCXLoader* pThis = (CFlashOCXLoader*)TlsGetValue(Globals::g_nTlsSlotParam2);
    ASSERT(NULL != pThis);

    return
    pThis->Hook_CreateWindowExA(
            lpOriginalFunc, 

            dwExStyle,      // extended window style
            lpClassName,  // registered class name
            lpWindowName, // window name
            dwStyle,        // window style
            x,                // horizontal position of window
            y,                // vertical position of window
            nWidth,           // window width
            nHeight,          // window height
            hWndParent,      // handle to parent or owner window
            hMenu,          // menu handle or child identifier
            hInstance,  // handle to application instance
            lpParam        // window-creation data
        );
}

HWND
WINAPI
CFlashOCXLoader::Hook_CreateWindowExA(
    LPVOID lpOriginalFunc, 

    DWORD dwExStyle,      // extended window style
    LPCSTR lpClassName,  // registered class name
    LPCSTR lpWindowName, // window name
    DWORD dwStyle,        // window style
    int x,                // horizontal position of window
    int y,                // vertical position of window
    int nWidth,           // window width
    int nHeight,          // window height
    HWND hWndParent,      // handle to parent or owner window
    HMENU hMenu,          // menu handle or child identifier
    HINSTANCE hInstance,  // handle to application instance
    LPVOID lpParam        // window-creation data
)
{
    typedef HWND (WINAPI *P_CreateWindowExA)(
        DWORD dwExStyle,      // extended window style
        LPCSTR lpClassName,  // registered class name
        LPCSTR lpWindowName, // window name
        DWORD dwStyle,        // window style
        int x,                // horizontal position of window
        int y,                // vertical position of window
        int nWidth,           // window width
        int nHeight,          // window height
        HWND hWndParent,      // handle to parent or owner window
        HMENU hMenu,          // menu handle or child identifier
        HINSTANCE hInstance,  // handle to application instance
        LPVOID lpParam        // window-creation data
    );
    P_CreateWindowExA pCreateWindowExA = (P_CreateWindowExA)lpOriginalFunc;

    f_in_box::std::CString<CHAR> strClassName;

    if (0 != HIWORD((int)lpClassName))
        // lpClassName is a name, not an atom
    {
        if (m_map_OldClassName2NewClassName.Find(lpClassName, strClassName))
            // We found it!
            lpClassName = strClassName.c_str();
    }

	if (NULL != hInstance)
		hInstance = g_hInstance;

    HWND hWnd = 
    pCreateWindowExA(
            dwExStyle,      // extended window style
            lpClassName,  // registered class name
            lpWindowName, // window name
            dwStyle,        // window style
            x,                // horizontal position of window
            y,                // vertical position of window
            nWidth,           // window width
            nHeight,          // window height
            hWndParent,      // handle to parent or owner window
            hMenu,          // menu handle or child identifier
            hInstance,  // handle to application instance
            lpParam        // window-creation data
        );

    return hWnd;
}

ATOM
WINAPI
CFlashOCXLoader::StaticHook_RegisterClassExA(
    CONST WNDCLASSEXA *lpwcx  // class data
)
{
    LPVOID lpOriginalFunc = TlsGetValue(Globals::g_nTlsSlotParam1);
    ASSERT(NULL != lpOriginalFunc);

    CFlashOCXLoader* pThis = (CFlashOCXLoader*)TlsGetValue(Globals::g_nTlsSlotParam2);
    ASSERT(NULL != pThis);

    return
    pThis->Hook_RegisterClassExA(
            lpOriginalFunc, 

            lpwcx  // class data
        );
}

ATOM
WINAPI
CFlashOCXLoader::Hook_RegisterClassExA(
    LPVOID lpOriginalFunc, 

    CONST WNDCLASSEXA *lpwcx  // class data
)
{
    WNDCLASSEXA NewWndClassEx;
    f_in_box::MemoryCopy(&NewWndClassEx, lpwcx, sizeof(NewWndClassEx));

    f_in_box::std::CString<CHAR> strNewWndClassName = GetUniqueString();

    if (0 != (HIWORD(lpwcx->lpszClassName)))
        // It's name, not an atom
    {
        NewWndClassEx.lpszClassName = strNewWndClassName.c_str();

        m_map_OldClassName2NewClassName.Add(lpwcx->lpszClassName, strNewWndClassName.c_str());

        NewWndClassEx.hInstance = g_hInstance;
    }

    typedef ATOM (WINAPI *P_RegisterClassExA)(
        CONST WNDCLASSEXA *lpwcx  // class data
    );
    P_RegisterClassExA pRegisterClassExA = (P_RegisterClassExA)lpOriginalFunc;

    return
    pRegisterClassExA(&NewWndClassEx);
}

ATOM
WINAPI
CFlashOCXLoader::StaticHook_RegisterClassA(
    CONST WNDCLASSA *lpwcx  // class data
)
{
    LPVOID lpOriginalFunc = TlsGetValue(Globals::g_nTlsSlotParam1);
    ASSERT(NULL != lpOriginalFunc);

    CFlashOCXLoader* pThis = (CFlashOCXLoader*)TlsGetValue(Globals::g_nTlsSlotParam2);
    ASSERT(NULL != pThis);

    return
    pThis->Hook_RegisterClassA(
            lpOriginalFunc, 

            lpwcx  // class data
        );
}

ATOM
WINAPI
CFlashOCXLoader::Hook_RegisterClassA(
    LPVOID lpOriginalFunc, 

    CONST WNDCLASSA *lpwcx  // class data
)
{
    WNDCLASSA NewWndClass;
    f_in_box::MemoryCopy(&NewWndClass, lpwcx, sizeof(NewWndClass));

    f_in_box::std::CString<CHAR> strNewWndClassName = GetUniqueString();

    if (0 != (HIWORD(lpwcx->lpszClassName)))
        // It's name, not an atom
    {
        NewWndClass.lpszClassName = strNewWndClassName.c_str();

        m_map_OldClassName2NewClassName.Add(lpwcx->lpszClassName, strNewWndClassName.c_str());

        NewWndClass.hInstance = g_hInstance;
    }

    typedef ATOM (WINAPI *P_RegisterClassA)(
        CONST WNDCLASSA *lpwcx  // class data
    );
    P_RegisterClassA pRegisterClassA = (P_RegisterClassA)lpOriginalFunc;

    return
    pRegisterClassA(&NewWndClass);
}

HCURSOR
WINAPI
CFlashOCXLoader::StaticHook_LoadCursorA(
    HINSTANCE hInstance,  // handle to application instance
    LPCSTR lpCursorName  // name or resource identifier
)
{
    LPVOID lpOriginalFunc = TlsGetValue(Globals::g_nTlsSlotParam1);
    ASSERT(NULL != lpOriginalFunc);

    CFlashOCXLoader* pThis = (CFlashOCXLoader*)TlsGetValue(Globals::g_nTlsSlotParam2);
    ASSERT(NULL != pThis);

    return 
    pThis->Hook_LoadCursorA(
            lpOriginalFunc, 

            hInstance, 
            lpCursorName
        );
}

HCURSOR
WINAPI
CFlashOCXLoader::Hook_LoadCursorA(
    LPVOID lpOriginalFunc, 

    HINSTANCE hInstance,  // handle to application instance
    LPCSTR lpCursorName  // name or resource identifier
)
{
    typedef HCURSOR (WINAPI *P_LoadCursorA)(
        HINSTANCE hInstance,  // handle to application instance
        LPCSTR lpCursorName  // name or resource identifier
    );
    P_LoadCursorA pLoadCursorA = (P_LoadCursorA)lpOriginalFunc;

    HCURSOR hCursor = NULL;

    if (GetHINSTANCE() == hInstance)
    {
        hCursor = FindLoadedCursor(lpCursorName);

        if (NULL != hCursor)
            return hCursor;

        LPVOID lpResDataOfGroupCursor = NULL;
        DWORD dwResSizeOfGroupCursor = 0;

        if (GetResourceData((LPCSTR)RT_GROUP_CURSOR, 
                            lpCursorName, 
                            lpResDataOfGroupCursor, 
                            dwResSizeOfGroupCursor))
        {
            int intCursorIndex = LookupIconIdFromDirectory((LPBYTE)lpResDataOfGroupCursor, FALSE);

            if (0 != intCursorIndex)
            {
                LPVOID lpResDataOfCursor = NULL;
                DWORD dwResSizeOfCursor = 0;

                if (GetResourceData((LPCSTR)RT_CURSOR, 
                                    (LPCSTR)MAKEINTRESOURCE(intCursorIndex), 
                                    lpResDataOfCursor, 
                                    dwResSizeOfCursor))
                {
                    hCursor = (HCURSOR)CreateIconFromResource((LPBYTE)lpResDataOfCursor, dwResSizeOfCursor, FALSE, 0x00030000);

                    if (NULL != hCursor)
                        SaveLoadedCursor(lpCursorName, hCursor);
                }
            }
        }
    }
    else
        hCursor = pLoadCursorA(hInstance, lpCursorName);

    return hCursor;
}

BOOL
WINAPI
CFlashOCXLoader::StaticHook_UnregisterClassA(
    LPCSTR lpClassName,  // class name
    HINSTANCE hInstance   // handle to application instance
)
{
    LPVOID lpOriginalFunc = TlsGetValue(Globals::g_nTlsSlotParam1);
    ASSERT(NULL != lpOriginalFunc);

    CFlashOCXLoader* pThis = (CFlashOCXLoader*)TlsGetValue(Globals::g_nTlsSlotParam2);
    ASSERT(NULL != pThis);

    return
    pThis->Hook_UnregisterClassA(
            lpOriginalFunc, 

            lpClassName, 
            hInstance
        );
}

BOOL
WINAPI
CFlashOCXLoader::Hook_UnregisterClassA(
    LPVOID lpOriginalFunc, 

    LPCSTR lpClassName,  // class name
    HINSTANCE hInstance   // handle to application instance
)
{
    typedef BOOL (WINAPI *P_UnregisterClassA)(
        LPCSTR lpClassName,  // class name
        HINSTANCE hInstance   // handle to application instance
    );
    P_UnregisterClassA pUnregisterClassA = (P_UnregisterClassA)lpOriginalFunc;

    if (0 == HIWORD(lpClassName))
        // Atom is passed
        return
        pUnregisterClassA(
                lpClassName, 
                hInstance
            );
    else
        // Class name is passed
    {
        f_in_box::std::CString<CHAR> strNewClassName;
        
        if (m_map_OldClassName2NewClassName.Find(lpClassName, strNewClassName))
            return
            pUnregisterClassA(
                    strNewClassName.c_str(), 
                    g_hInstance
                );
        else
        {
            ASSERT(FALSE);

            return
            pUnregisterClassA(
                    lpClassName, 
                    hInstance
                );
        }
    }
}

HMENU
WINAPI
CFlashOCXLoader::StaticHook_LoadMenuA(
    HINSTANCE hInstance,  // handle to module
    LPCSTR lpMenuName    // menu name or resource identifier
)
{
    LPVOID lpOriginalFunc = TlsGetValue(Globals::g_nTlsSlotParam1);
    ASSERT(NULL != lpOriginalFunc);

    CFlashOCXLoader* pThis = (CFlashOCXLoader*)TlsGetValue(Globals::g_nTlsSlotParam2);
    ASSERT(NULL != pThis);

    return
    pThis->Hook_LoadMenuA(
            lpOriginalFunc, 

            hInstance, 
            lpMenuName
        );
}

HMENU
WINAPI
CFlashOCXLoader::Hook_LoadMenuA(
    LPVOID lpOriginalFunc, 

    HINSTANCE hInstance,  // handle to module
    LPCSTR lpMenuName    // menu name or resource identifier
)
{
    typedef HMENU (WINAPI *P_LoadMenuA)(
        HINSTANCE hInstance,  // handle to module
        LPCSTR lpMenuName    // menu name or resource identifier
    );
    P_LoadMenuA pLoadMenuA = (P_LoadMenuA)lpOriginalFunc;

    HMENU hMenu = NULL;

    LPVOID lpResData = NULL;
    DWORD dwResSize = 0;

    if (GetHINSTANCE() == hInstance)
    {
        if (GetResourceData((LPCSTR)RT_MENU, 
                            lpMenuName, 
                            lpResData, 
                            dwResSize))
            hMenu = LoadMenuIndirect((CONST MENUTEMPLATE*)lpResData);
        else
        {
            ASSERT(FALSE);
        }
    }
    else
        hMenu = 
            pLoadMenuA(
                    hInstance, 
                    lpMenuName
                );

    return hMenu;
}

MMRESULT
WINAPI
CFlashOCXLoader::StaticHook_waveOutWrite(
    HWAVEOUT hwo,  
    LPWAVEHDR pwh, 
    UINT cbwh      
)
{
    LPVOID lpOriginalFunc = TlsGetValue(Globals::g_nTlsSlotParam1);
    ASSERT(NULL != lpOriginalFunc);

    CFlashOCXLoader* pThis = (CFlashOCXLoader*)TlsGetValue(Globals::g_nTlsSlotParam2);
    ASSERT(NULL != pThis);

    return
    pThis->Hook_waveOutWrite(
            lpOriginalFunc, 

            hwo,  
            pwh, 
            cbwh      
        );
}

MMRESULT
WINAPI
CFlashOCXLoader::Hook_waveOutWrite(
    LPVOID lpOriginalFunc, 

    HWAVEOUT hwo,  
    LPWAVEHDR pwh, 
    UINT cbwh      
)
{
	CCriticalSectionOwner cso(m_CriticalSection__AudioFunctions);

    typedef MMRESULT (WINAPI *P_waveOutWrite)(
        HWAVEOUT hwo,  
        LPWAVEHDR pwh, 
        UINT cbwh
    );
    P_waveOutWrite pwaveOutWrite = (P_waveOutWrite)lpOriginalFunc;

	MMRESULT mmresult;

	if (!m_bAudioEnabled)
		f_in_box::MemoryZero(pwh->lpData, pwh->dwBufferLength);
	else
	{
		if (NULL != m_pSoundListener)
		{
			f_in_box::std::CArray<BYTE> WaveFormat;
			m_DeviceFormat.Find(hwo, WaveFormat);

			m_pSoundListener((HFPC)this, m_SoundListenerlParam, (PWAVEFORMATEX)(LPBYTE)WaveFormat, pwh, cbwh);

			{
				if (-1 == m_dwStartTimeForSoundCapturing)
					m_dwStartTimeForSoundCapturing = GetTickCount();




                if (0 != lstrcmpiA(m_strContext.c_str(), DEF_RIGHT_CONTEXT))
                {
					if ((GetTickCount() - m_dwStartTimeForSoundCapturing) >= 10 * 1000)
						// We capture more than 10 seconds -- it's a limitation!
						m_pSoundListener = NULL;
				}


			}
		}

        WORD wBytesPerSample;

        if (m_DeviceBytesPerSample.Find(hwo, wBytesPerSample))
		{
			if (1 == wBytesPerSample)
			{
				char* lpData = (char*)pwh->lpData;

				int nCount = pwh->dwBufferLength;

				for (int i = 0; i < nCount; i++)
				{
					long nValue = lpData[i];
					nValue *= m_dwAudioVolume;
                    nValue /= DEF_MAX_FLASH_AUDIO_VOLUME;

					lpData[i] = (char)nValue;
				}
			}
			else if (2 == wBytesPerSample)
			{
                ASSERT(2 == sizeof(short));

				short* lpData = (short*)pwh->lpData;

				int nCount;

				if (0 == (pwh->dwBufferLength % 2))
					nCount = pwh->dwBufferLength / 2;
				else
					// Strange the size of the buffer
				{
					ASSERT(FALSE);

                    nCount = pwh->dwBufferLength >= 1 ? (pwh->dwBufferLength - 1) / 2 : 0;
				}

				for (int i = 0; i < nCount; i++)
				{
                    long nValue = lpData[i];
					nValue *= (long)m_dwAudioVolume;
                    nValue /= DEF_MAX_FLASH_AUDIO_VOLUME;

					lpData[i] = (short)nValue;
				}
			}
			else
				// Very strange value
			{
				ASSERT(FALSE);
			}
		}
		else
		{
			ASSERT(FALSE);
		}
	}

    mmresult = 
    pwaveOutWrite(
            hwo,  
            pwh, 
            cbwh
        );

	return mmresult;
}

MMRESULT
WINAPI
CFlashOCXLoader::StaticHook_waveOutOpen(
    LPHWAVEOUT     phwo,      
    UINT_PTR       uDeviceID, 
    LPWAVEFORMATEX pwfx,      
    DWORD          dwCallback,
    DWORD          dwCallbackInstance,
    DWORD          fdwOpen    
)
{
    LPVOID lpOriginalFunc = TlsGetValue(Globals::g_nTlsSlotParam1);
    ASSERT(NULL != lpOriginalFunc);

    CFlashOCXLoader* pThis = (CFlashOCXLoader*)TlsGetValue(Globals::g_nTlsSlotParam2);
    ASSERT(NULL != pThis);

    return
    pThis->Hook_waveOutOpen(
            lpOriginalFunc, 

            phwo,      
            uDeviceID, 
            pwfx,      
            dwCallback,
            dwCallbackInstance,
            fdwOpen    
        );
}

MMRESULT
WINAPI
CFlashOCXLoader::Hook_waveOutOpen(
    LPVOID lpOriginalFunc, 

    LPHWAVEOUT     phwo,      
    UINT_PTR       uDeviceID, 
    LPWAVEFORMATEX pwfx,      
    DWORD          dwCallback,
    DWORD          dwCallbackInstance,
    DWORD          fdwOpen    
)
{
	CCriticalSectionOwner cso(m_CriticalSection__AudioFunctions);

    typedef MMRESULT (WINAPI *P_waveOutOpen)(
        LPHWAVEOUT     phwo,      
        UINT_PTR       uDeviceID, 
        LPWAVEFORMATEX pwfx,      
        DWORD          dwCallback,
        DWORD          dwCallbackInstance,
        DWORD          fdwOpen    
    );
    P_waveOutOpen pwaveOutOpen = (P_waveOutOpen)lpOriginalFunc;

    MMRESULT mmresult = 
        pwaveOutOpen(
                phwo,      
                uDeviceID, 
                pwfx,      
                dwCallback,
                dwCallbackInstance,
                fdwOpen    
            );

	if (MMSYSERR_NOERROR == mmresult)
	{
        m_DeviceBytesPerSample.Add(*phwo, pwfx->wBitsPerSample / 8);

		m_DeviceFormat.Add(
			*phwo, 
			f_in_box::std::CArray<BYTE>(
				(const BYTE*)pwfx, 
				sizeof(WAVEFORMATEX) + (WAVE_FORMAT_PCM == pwfx->wFormatTag ? 0 : pwfx->cbSize)));
	}

	return mmresult;
}

MMRESULT
WINAPI
CFlashOCXLoader::StaticHook_waveOutClose(
    HWAVEOUT hwo  
)
{
    LPVOID lpOriginalFunc = TlsGetValue(Globals::g_nTlsSlotParam1);
    ASSERT(NULL != lpOriginalFunc);

    CFlashOCXLoader* pThis = (CFlashOCXLoader*)TlsGetValue(Globals::g_nTlsSlotParam2);
    ASSERT(NULL != pThis);

    return
    pThis->Hook_waveOutClose(
            lpOriginalFunc, 
            hwo
        );
}

MMRESULT
WINAPI
CFlashOCXLoader::Hook_waveOutClose(
    LPVOID lpOriginalFunc, 

    HWAVEOUT hwo  
)
{
	CCriticalSectionOwner cso(m_CriticalSection__AudioFunctions);

    typedef MMRESULT (WINAPI *P_waveOutClose)(
        HWAVEOUT hwo
    );
    P_waveOutClose pwaveOutClose = (P_waveOutClose)lpOriginalFunc;

	MMRESULT mmresult = pwaveOutClose(hwo);

	if (MMSYSERR_NOERROR == mmresult)
	{
#ifdef _DEBUG
		{
			WORD temp;
    		ASSERT(m_DeviceBytesPerSample.Find(hwo, temp));
		}
#endif // _DEBUG

        m_DeviceBytesPerSample.RemoveByKey(hwo);

#ifdef _DEBUG
		{
			f_in_box::std::CArray<BYTE> temp;
    		ASSERT(m_DeviceFormat.Find(hwo, temp));
		}
#endif // _DEBUG

		m_DeviceFormat.RemoveByKey(hwo);
	}

	return mmresult;
}

HANDLE
WINAPI
CFlashOCXLoader::StaticHook_CreateFileA(
    LPCSTR lpFileName,
    DWORD dwDesiredAccess,
    DWORD dwShareMode,
    LPSECURITY_ATTRIBUTES lpSecurityAttributes,
    DWORD dwCreationDisposition,
    DWORD dwFlagsAndAttributes,
    HANDLE hTemplateFile
)
{
    LPVOID lpOriginalFunc = TlsGetValue(Globals::g_nTlsSlotParam1);
    ASSERT(NULL != lpOriginalFunc);

    CFlashOCXLoader* pThis = (CFlashOCXLoader*)TlsGetValue(Globals::g_nTlsSlotParam2);
    ASSERT(NULL != pThis);

    return
    pThis->Hook_CreateFileA(
            lpOriginalFunc, 

            lpFileName,
            dwDesiredAccess,
            dwShareMode,
            lpSecurityAttributes,
            dwCreationDisposition,
            dwFlagsAndAttributes,
            hTemplateFile
        );
}

HANDLE
WINAPI
CFlashOCXLoader::Hook_CreateFileA(
    LPVOID lpOriginalFunc, 

    LPCSTR lpFileName,
    DWORD dwDesiredAccess,
    DWORD dwShareMode,
    LPSECURITY_ATTRIBUTES lpSecurityAttributes,
    DWORD dwCreationDisposition,
    DWORD dwFlagsAndAttributes,
    HANDLE hTemplateFile
)
{
	return m_ContentManager.MyCreateFileA(lpFileName, 
                                          dwDesiredAccess, 
                                          dwShareMode, 
                                          lpSecurityAttributes, 
                                          dwCreationDisposition, 
                                          dwFlagsAndAttributes, 
                                          hTemplateFile);

    //typedef HANDLE (WINAPI *P_CreateFileA)(
    //    LPCSTR lpFileName,
    //    DWORD dwDesiredAccess,
    //    DWORD dwShareMode,
    //    LPSECURITY_ATTRIBUTES lpSecurityAttributes,
    //    DWORD dwCreationDisposition,
    //    DWORD dwFlagsAndAttributes,
    //    HANDLE hTemplateFile
    //);
    //P_CreateFileA pCreateFileA = (P_CreateFileA)lpOriginalFunc;

    //return
    //pCreateFileA(
    //        lpFileName,
    //        dwDesiredAccess,
    //        dwShareMode,
    //        lpSecurityAttributes,
    //        dwCreationDisposition,
    //        dwFlagsAndAttributes,
    //        hTemplateFile
    //    );
}

HANDLE
WINAPI
CFlashOCXLoader::StaticHook_CreateFileW(
    LPCWSTR lpFileName,
    DWORD dwDesiredAccess,
    DWORD dwShareMode,
    LPSECURITY_ATTRIBUTES lpSecurityAttributes,
    DWORD dwCreationDisposition,
    DWORD dwFlagsAndAttributes,
    HANDLE hTemplateFile
)
{
    LPVOID lpOriginalFunc = TlsGetValue(Globals::g_nTlsSlotParam1);
    ASSERT(NULL != lpOriginalFunc);

    CFlashOCXLoader* pThis = (CFlashOCXLoader*)TlsGetValue(Globals::g_nTlsSlotParam2);
    ASSERT(NULL != pThis);

    return
    pThis->Hook_CreateFileW(
            lpOriginalFunc, 

            lpFileName,
            dwDesiredAccess,
            dwShareMode,
            lpSecurityAttributes,
            dwCreationDisposition,
            dwFlagsAndAttributes,
            hTemplateFile
        );
}

HANDLE
WINAPI
CFlashOCXLoader::Hook_CreateFileW(
    LPVOID lpOriginalFunc, 

    LPCWSTR lpFileName,
    DWORD dwDesiredAccess,
    DWORD dwShareMode,
    LPSECURITY_ATTRIBUTES lpSecurityAttributes,
    DWORD dwCreationDisposition,
    DWORD dwFlagsAndAttributes,
    HANDLE hTemplateFile
)
{
	return m_ContentManager.MyCreateFileW(lpFileName, 
                                          dwDesiredAccess, 
                                          dwShareMode, 
                                          lpSecurityAttributes, 
                                          dwCreationDisposition, 
                                          dwFlagsAndAttributes, 
                                          hTemplateFile);

    //typedef HANDLE (WINAPI *P_CreateFileW)(
    //    LPCWSTR lpFileName,
    //    DWORD dwDesiredAccess,
    //    DWORD dwShareMode,
    //    LPSECURITY_ATTRIBUTES lpSecurityAttributes,
    //    DWORD dwCreationDisposition,
    //    DWORD dwFlagsAndAttributes,
    //    HANDLE hTemplateFile
    //);
    //P_CreateFileW pCreateFileW = (P_CreateFileW)lpOriginalFunc;

    //if (NULL != pCreateFileW)
    //    return
    //    pCreateFileW(
    //            lpFileName,
    //            dwDesiredAccess,
    //            dwShareMode,
    //            lpSecurityAttributes,
    //            dwCreationDisposition,
    //            dwFlagsAndAttributes,
    //            hTemplateFile
    //        );
    //else
    //    return
    //    CreateFileA(
    //            f_in_box::std::WToCA(lpFileName).c_str(),
    //            dwDesiredAccess,
    //            dwShareMode,
    //            lpSecurityAttributes,
    //            dwCreationDisposition,
    //            dwFlagsAndAttributes,
    //            hTemplateFile
    //        );
}

BOOL
WINAPI
CFlashOCXLoader::StaticHook_FlushInstructionCache(
    HANDLE hProcess,
    LPCVOID lpBaseAddress,
    SIZE_T dwSize
)
{
    LPVOID lpOriginalFunc = TlsGetValue(Globals::g_nTlsSlotParam1);
    ASSERT(NULL != lpOriginalFunc);

    CFlashOCXLoader* pThis = (CFlashOCXLoader*)TlsGetValue(Globals::g_nTlsSlotParam2);
    ASSERT(NULL != pThis);

    return
    pThis->Hook_FlushInstructionCache(
            lpOriginalFunc, 

            hProcess,
            lpBaseAddress,
            dwSize
        );
}

BOOL
WINAPI
CFlashOCXLoader::Hook_FlushInstructionCache(
    LPVOID lpOriginalFunc, 

    HANDLE hProcess,
    LPCVOID lpBaseAddress,
    SIZE_T dwSize
)
{
    typedef BOOL (WINAPI *P_FlushInstructionCache)(
        HANDLE hProcess,
        LPCVOID lpBaseAddress,
        SIZE_T dwSize
    );
    P_FlushInstructionCache pFlushInstructionCache = (P_FlushInstructionCache)lpOriginalFunc;

    DWORD dwOldProtect;

    /* BOOL bRes = */ VirtualProtect((LPVOID)lpBaseAddress, dwSize, PAGE_EXECUTE_READWRITE, &dwOldProtect);

    return
    pFlushInstructionCache(
        hProcess,
        lpBaseAddress,
        dwSize
    );
}

void CFlashOCXLoader::FreeLoadedCursors()
{
    {
        f_in_box::std::POSITION pos = m_mapCursorId2Handler__IsIntresource.GetFirstPosition();

        WORD id;
        HCURSOR hCursor;

        for (m_mapCursorId2Handler__IsIntresource.GetNext(pos, id, hCursor); 
            NULL != pos; 
            m_mapCursorId2Handler__IsIntresource.GetNext(pos, id, hCursor))
            DestroyCursor(hCursor);
    }

    {
        f_in_box::std::POSITION pos = m_mapCursorId2Handler__StringResource.GetFirstPosition();

        f_in_box::std::CString<CHAR> strId;
        HCURSOR hCursor;

        for (m_mapCursorId2Handler__StringResource.GetNext(pos, strId, hCursor); 
            NULL != pos; 
            m_mapCursorId2Handler__StringResource.GetNext(pos, strId, hCursor))
            DestroyCursor(hCursor);
    }

	m_mapCursorId2Handler__IsIntresource.RemoveAll();
	m_mapCursorId2Handler__StringResource.RemoveAll();
}

void CFlashOCXLoader::SaveLoadedCursor(LPCSTR lpszCursorName, HCURSOR hCursor)
{
	if (DEF_IS_INTRESOURCE(lpszCursorName))
		m_mapCursorId2Handler__IsIntresource.Add((WORD)lpszCursorName, hCursor);
	else
        m_mapCursorId2Handler__StringResource.Add(lpszCursorName, hCursor);
}

HCURSOR CFlashOCXLoader::FindLoadedCursor(LPCSTR lpszCursorName)
{
    HCURSOR hCursor;

    if (DEF_IS_INTRESOURCE(lpszCursorName))
    {
        if (m_mapCursorId2Handler__IsIntresource.Find((WORD)lpszCursorName, hCursor))
            return hCursor;
        else
            return NULL;
    }
	else
	{
        if (m_mapCursorId2Handler__StringResource.Find(lpszCursorName, hCursor))
            return hCursor;
        else
            return NULL;
	}
}

LRESULT
CALLBACK
CFlashOCXLoader::StaticWindowProc(
    HWND hWnd, 
    UINT uMsg, 
    WPARAM wParam, 
    LPARAM lParam 
)
{
    CFlashOCXLoader* pThis = (CFlashOCXLoader*)TlsGetValue(Globals::g_nTlsSlotParam1);
    ASSERT(NULL != pThis);

    if (WM_CREATE == uMsg)
    {
        CFPCWnd* pWnd = new CFPCWnd(hWnd, pThis, (LPCREATESTRUCT)lParam);

		BOOL bHandled;
        return pWnd->OnCreate(uMsg, wParam, lParam, bHandled);
    }
    else
	{
		if (IsWindowUnicode(hWnd))
		{
			typedef LONG (WINAPI *P_DefWindowProcW)(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
			P_DefWindowProcW pDefWindowProcW;
			HMODULE hUser32DLL = LoadLibrary(_T("user32.dll"));
			(FARPROC&)pDefWindowProcW = ::GetProcAddress(hUser32DLL, "DefWindowProcW");
			LRESULT lResult;
			if (NULL != pDefWindowProcW)
				lResult = pDefWindowProcW(hWnd, uMsg, wParam, lParam);
			else
				lResult = DefWindowProcA(hWnd, uMsg, wParam, lParam);
			FreeLibrary(hUser32DLL);
			return lResult;
		}
		else
	        return DefWindowProcA(hWnd, uMsg, wParam, lParam);
	}
}

ATOM CFlashOCXLoader::GetClassAtomA()
{
    return m_atomWindowClassA;
}

ATOM CFlashOCXLoader::GetClassAtomW()
{
    return m_atomWindowClassW;
}

HMODULE
WINAPI
CFlashOCXLoader::StaticHook_LoadLibraryA(
    LPCSTR lpFileName
)
{
    LPVOID lpOriginalFunc = TlsGetValue(Globals::g_nTlsSlotParam1);
    ASSERT(NULL != lpOriginalFunc);

    CFlashOCXLoader* pThis = (CFlashOCXLoader*)TlsGetValue(Globals::g_nTlsSlotParam2);
    ASSERT(NULL != pThis);

    return
    pThis->Hook_LoadLibraryA(lpOriginalFunc, lpFileName);
}

HMODULE
WINAPI
CFlashOCXLoader::Hook_LoadLibraryA(
    LPVOID lpOriginalFunc, 

    LPCSTR lpFileName
)
{
    typedef HMODULE (WINAPI *P_LoadLibraryA)(
        LPCSTR lpFileName
    );
    P_LoadLibraryA pLoadLibraryA = (P_LoadLibraryA)lpOriginalFunc;

    BOOL bSWSupport = FALSE;

	//TEMP
    for (int i = lstrlenA(lpFileName) - 1; i >= 0; i--)
        if (lpFileName[i] == '\\')
            if ( 
                 (0 == lstrcmpiA(lpFileName + i, "\\swsupport.dll")) || 
                 (0 == lstrcmpiA(lpFileName + i, "\\swsupport"))
               )
            {
                bSWSupport = TRUE;
                break;
            }

    //HMODULE hModule = NULL;

    // Load swsupport.dll from memory
    //if (bSWSupport)
    //{
    //    CMemDLL* pMemDLL = new CMemDLL;

    //    HANDLE hFile = CreateFile(lpFileName, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);

    //    if (INVALID_HANDLE_VALUE != hFile)
    //    {
    //        DWORD dwSize = GetFileSize(hFile, NULL);
    //        LPBYTE lpData = new BYTE[dwSize];
    //        DWORD temp;
    //        ReadFile(hFile, lpData, dwSize, &temp, NULL);

    //        pMemDLL->Load(lpData, dwSize);
    //        hModule = pMemDLL->GetHINSTANCE();

    //        CloseHandle(hFile);
    //        delete[] lpData;
    //    }
    //}
    //else
    //    hModule = pLoadLibraryA(lpFileName);

    //return hModule;

    // Don't load swsupport.dll
    return bSWSupport ? NULL : pLoadLibraryA(lpFileName);
}

CContentManager& CFlashOCXLoader::GetContentManager()
{
    return m_ContentManager;
}

BOOL CFlashOCXLoader::GetModuleVersionEx(SFPCVersion* pVersion)
{
	BOOL bRes = FALSE;

	LPVOID lpResDataOfVersion;
    DWORD dwResSizeOfVersion;

    if (GetResourceData((LPCSTR)RT_VERSION, 
                        (LPCSTR)MAKEINTRESOURCE(1), 
                        lpResDataOfVersion, 
                        dwResSizeOfVersion))
    {
        // Please see about VS_VERSIONINFO in MSDN
        VS_FIXEDFILEINFO* pFixedFileInfo = (VS_FIXEDFILEINFO*)((DWORD)lpResDataOfVersion + sizeof(WORD) * 19);

        while (0 == *(WORD*)pFixedFileInfo)
            pFixedFileInfo = (VS_FIXEDFILEINFO*)((DWORD)pFixedFileInfo + sizeof(WORD));

        DWORD dwMinorPart = pFixedFileInfo->dwProductVersionLS;
        DWORD dwMajorPart = pFixedFileInfo->dwProductVersionMS;

		pVersion->v[3] = (WORD)(dwMajorPart >> 16);
		pVersion->v[2] = (WORD)(dwMajorPart & 0xffff);
		pVersion->v[1] = (WORD)(dwMinorPart >> 16);
		pVersion->v[0] = (WORD)(dwMinorPart & 0xffff);

		bRes = TRUE;
    }

    return bRes;
}

DWORD CFlashOCXLoader::GetAudioVolume()
{
    CCriticalSectionOwner cso(m_CriticalSection__AudioFunctions);

    return m_dwAudioVolume;
}

void CFlashOCXLoader::SetAudioVolume(DWORD dwVolume)
{
	CCriticalSectionOwner cso(m_CriticalSection__AudioFunctions);

    m_dwAudioVolume = dwVolume;

    UpdateAudioClientsVolumeAndMuteStatus();
}

void CFlashOCXLoader::UpdateAudioClientVolumeAndMuteStatus(IAudioClient* pAudioClient)
{
    CComPtr<IAudioStreamVolume> pAudioStreamVolume;
    HRESULT hr = pAudioClient->GetService(IID_IAudioStreamVolume, (void**)&pAudioStreamVolume);
    if (S_OK == hr)
    {
        if (DEF_MAX_FLASH_AUDIO_VOLUME != m_dwAudioVolume)
        {
			#define MAX_CHANNEL_COUNT 100	// Just in case

			UINT channelCount = 0;
			if (FAILED(pAudioStreamVolume->GetChannelCount(&channelCount))) {
				ASSERT(FALSE);
			}
			else if (channelCount > MAX_CHANNEL_COUNT) {
				ASSERT(FALSE);
			}
			else {
				// New volume (with mute flag considered)
				float newVol = m_bAudioEnabled ? (float)m_dwAudioVolume / DEF_MAX_FLASH_AUDIO_VOLUME : 0.0f;

				float volArray[MAX_CHANNEL_COUNT];
				for (UINT i=0; i<channelCount; i++) { volArray[i] = newVol; }

				if (FAILED(pAudioStreamVolume->SetAllVolumes(channelCount, volArray))) {
					ASSERT(FALSE);
				}
			}
        }
    }
    else
        // Quite unexpectedly
    {
        ASSERT(FALSE);
    }
}

void CFlashOCXLoader::UpdateAudioClientsVolumeAndMuteStatus()
{
	CCriticalSectionOwner cso(m_csAudioClientCollection);

    for (POSITION pos = m_AudioClientCollection.GetFirstPosition(); NULL != pos; )
    {
        UpdateAudioClientVolumeAndMuteStatus(m_AudioClientCollection.GetNextValue(pos));
    }
}

BOOL CFlashOCXLoader::GetAudioEnabled()
{
	CCriticalSectionOwner cso(m_CriticalSection__AudioFunctions);

    return m_bAudioEnabled;
}

void CFlashOCXLoader::SetAudioEnabled(BOOL bEnable)
{
	CCriticalSectionOwner cso(m_CriticalSection__AudioFunctions);

    m_bAudioEnabled = bEnable;

    UpdateAudioClientsVolumeAndMuteStatus();
}

LPCSTR CFlashOCXLoader::GetClassNameA()
{
    return m_strWindowClassNameA.c_str();
}

LPCWSTR CFlashOCXLoader::GetClassNameW()
{
    return m_strWindowClassNameW.c_str();
}

DWORD CFlashOCXLoader::SetSoundListener(PSOUNDLISTENER pSoundListener, LPARAM lParam)
{
	CCriticalSectionOwner cso(m_CriticalSection__AudioFunctions);

	m_pSoundListener = pSoundListener;
	m_SoundListenerlParam = lParam;

	return 1;
}

DWORD CFlashOCXLoader::SetPreProcessURLHandler(PPREPROCESSURLHANDLER pHandler, LPARAM lParam)
{
	m_pPreProcessURLHandler = pHandler;
	m_PreProcessURLHandlerlParam = lParam;

	return 1;
}

void CFlashOCXLoader::SetContext(LPCSTR szContext)
{


	m_strContext = szContext;


}

BOOL
WINAPI
CFlashOCXLoader::StaticHook_GetClassInfoExA(
	HINSTANCE hinst,
	LPCSTR lpszClass,
	LPWNDCLASSEXA lpwcx
)
{
    LPVOID lpOriginalFunc = TlsGetValue(Globals::g_nTlsSlotParam1);
    ASSERT(NULL != lpOriginalFunc);

    CFlashOCXLoader* pThis = (CFlashOCXLoader*)TlsGetValue(Globals::g_nTlsSlotParam2);
    ASSERT(NULL != pThis);

	return pThis->Hook_GetClassInfoExA(lpOriginalFunc, hinst, lpszClass, lpwcx);
}

BOOL
WINAPI
CFlashOCXLoader::Hook_GetClassInfoExA(
    LPVOID lpOriginalFunc, 

	HINSTANCE hinst,
	LPCSTR lpszClass,
	LPWNDCLASSEXA lpwcx
)
{
	typedef BOOL (WINAPI *P_GetClassInfoExA)(
		HINSTANCE hinst,
		LPCSTR lpszClass,
		LPWNDCLASSEXA lpwcx
	);
	P_GetClassInfoExA pGetClassInfoExA;

    f_in_box::std::CString<CHAR> strClassName;

	if (0 != HIWORD((int)lpszClass))
        // lpClassName is a name, not an atom
    {
        if (m_map_OldClassName2NewClassName.Find(lpszClass, strClassName))
            // We found it!
		{
			lpszClass = strClassName.c_str();
		}
    }

	if (NULL != hinst)
		hinst = g_hInstance;

	pGetClassInfoExA = (P_GetClassInfoExA)lpOriginalFunc;

	return pGetClassInfoExA(hinst, lpszClass, lpwcx);
}

int
WINAPI
CFlashOCXLoader::StaticHook_GetClassNameA(
	HWND hWnd,
	LPSTR lpClassName,
	int nMaxCount
)
{
    LPVOID lpOriginalFunc = TlsGetValue(Globals::g_nTlsSlotParam1);
    ASSERT(NULL != lpOriginalFunc);

    CFlashOCXLoader* pThis = (CFlashOCXLoader*)TlsGetValue(Globals::g_nTlsSlotParam2);
    ASSERT(NULL != pThis);

	return pThis->Hook_GetClassNameA(lpOriginalFunc, hWnd, lpClassName, nMaxCount);
}

int
WINAPI
CFlashOCXLoader::Hook_GetClassNameA(
    LPVOID lpOriginalFunc, 

	HWND hWnd,
	LPSTR lpClassName,
	int nMaxCount
)
{
	typedef int (WINAPI *P_GetClassNameA)(
		HWND hWnd,
		LPSTR lpClassName,
		int nMaxCount
	);
	P_GetClassNameA pGetClassNameA = (P_GetClassNameA)lpOriginalFunc;

	return pGetClassNameA(hWnd, lpClassName, nMaxCount);
}

HMODULE
WINAPI
CFlashOCXLoader::StaticHook_GetModuleHandleA(
	LPCSTR lpModuleName
)
{
    LPVOID lpOriginalFunc = TlsGetValue(Globals::g_nTlsSlotParam1);
    ASSERT(NULL != lpOriginalFunc);

    CFlashOCXLoader* pThis = (CFlashOCXLoader*)TlsGetValue(Globals::g_nTlsSlotParam2);
    ASSERT(NULL != pThis);

	return pThis->Hook_GetModuleHandleA(lpOriginalFunc, lpModuleName);
}

HMODULE
WINAPI
CFlashOCXLoader::Hook_GetModuleHandleA(
	LPVOID lpOriginalFunc, 

	LPCSTR lpModuleName
)
{
	typedef HMODULE (WINAPI *P_GetModuleHandleA)(
		LPCSTR lpModuleName
	);
	P_GetModuleHandleA pGetModuleHandleA = (P_GetModuleHandleA)lpOriginalFunc;

	return pGetModuleHandleA(lpModuleName);
}

HMODULE
WINAPI
CFlashOCXLoader::StaticHook_LoadLibraryExA(
    LPCSTR lpFileName, 
	HANDLE hFile,
	DWORD dwFlags
)
{
    LPVOID lpOriginalFunc = TlsGetValue(Globals::g_nTlsSlotParam1);
    ASSERT(NULL != lpOriginalFunc);

    CFlashOCXLoader* pThis = (CFlashOCXLoader*)TlsGetValue(Globals::g_nTlsSlotParam2);
    ASSERT(NULL != pThis);

    return
    pThis->Hook_LoadLibraryExA(lpOriginalFunc, lpFileName, hFile, dwFlags);
}

HMODULE
WINAPI
CFlashOCXLoader::Hook_LoadLibraryExA(
    LPVOID lpOriginalFunc, 

    LPCSTR lpFileName, 
	HANDLE hFile,
	DWORD dwFlags
)
{
    typedef HMODULE (WINAPI *P_LoadLibraryExA)(
		LPCSTR lpFileName, 
		HANDLE hFile,
		DWORD dwFlags
    );
    P_LoadLibraryExA pLoadLibraryExA = (P_LoadLibraryExA)lpOriginalFunc;

    return pLoadLibraryExA(lpFileName, hFile, dwFlags);
}

HRSRC
WINAPI
CFlashOCXLoader::StaticHook_FindResourceA(
	HMODULE hModule,
	LPCSTR lpName,
	LPCSTR lpType
)
{
    LPVOID lpOriginalFunc = TlsGetValue(Globals::g_nTlsSlotParam1);
    ASSERT(NULL != lpOriginalFunc);

    CFlashOCXLoader* pThis = (CFlashOCXLoader*)TlsGetValue(Globals::g_nTlsSlotParam2);
    ASSERT(NULL != pThis);

	return pThis->Hook_FindResourceA(
			lpOriginalFunc, 

			hModule,
			lpName,
			lpType
		);
}

HRSRC
WINAPI
CFlashOCXLoader::Hook_FindResourceA(
    LPVOID lpOriginalFunc, 

	HMODULE hModule,
	LPCSTR lpName,
	LPCSTR lpType
)
{
	typedef HRSRC (WINAPI *P_FindResourceA)(
		HMODULE hModule,
		LPCSTR lpName,
		LPCSTR lpType);
	P_FindResourceA pFindResourceA = (P_FindResourceA)lpOriginalFunc;

	return pFindResourceA(
			hModule,
			lpName,
			lpType
		);
}

HRSRC
WINAPI
CFlashOCXLoader::StaticHook_FindResourceExA(
	HMODULE hModule,
	LPCSTR lpType, 
	LPCSTR lpName, 
	WORD wLanguage
)
{
    LPVOID lpOriginalFunc = TlsGetValue(Globals::g_nTlsSlotParam1);
    ASSERT(NULL != lpOriginalFunc);

    CFlashOCXLoader* pThis = (CFlashOCXLoader*)TlsGetValue(Globals::g_nTlsSlotParam2);
    ASSERT(NULL != pThis);

	return pThis->Hook_FindResourceExA(
			lpOriginalFunc, 

			hModule,
			lpType, 
			lpName, 
			wLanguage
		);
}

HRSRC
WINAPI
CFlashOCXLoader::Hook_FindResourceExA(
    LPVOID lpOriginalFunc, 

	HMODULE hModule,
	LPCSTR lpType, 
	LPCSTR lpName, 
	WORD wLanguage
)
{
	typedef HRSRC (WINAPI *P_FindResourceExA)(
		HMODULE hModule,
		LPCSTR lpType, 
		LPCSTR lpName, 
		WORD wLanguage);
	P_FindResourceExA pFindResourceExA = (P_FindResourceExA)lpOriginalFunc;

	return pFindResourceExA(
			hModule,
			lpType,
			lpName, 
			wLanguage
		);
}

HRSRC
WINAPI
CFlashOCXLoader::StaticHook_FindResourceExW(
	HMODULE hModule,
	LPCWSTR lpType, 
	LPCWSTR lpName, 
	WORD wLanguage
)
{
    LPVOID lpOriginalFunc = TlsGetValue(Globals::g_nTlsSlotParam1);
    ASSERT(NULL != lpOriginalFunc);

    CFlashOCXLoader* pThis = (CFlashOCXLoader*)TlsGetValue(Globals::g_nTlsSlotParam2);
    ASSERT(NULL != pThis);

	return pThis->Hook_FindResourceExW(
			lpOriginalFunc, 

			hModule,
			lpType, 
			lpName, 
			wLanguage
		);
}

HRSRC
WINAPI
CFlashOCXLoader::Hook_FindResourceExW(
    LPVOID lpOriginalFunc, 

	HMODULE hModule,
	LPCWSTR lpType, 
	LPCWSTR lpName, 
	WORD wLanguage
)
{
	typedef HRSRC (WINAPI *P_FindResourceExW)(
		HMODULE hModule,
		LPCWSTR lpType, 
		LPCWSTR lpName, 
		WORD wLanguage);
	P_FindResourceExW pFindResourceExW = (P_FindResourceExW)lpOriginalFunc;

	return pFindResourceExW(
			hModule,
			lpType,
			lpName, 
			wLanguage
		);
}

UINT
WINAPI
CFlashOCXLoader::StaticHook_SetTimer(
	HWND hWnd,
	UINT_PTR nIDEvent,
	UINT uElapse,
	TIMERPROC lpTimerFunc
)
{
    LPVOID lpOriginalFunc = TlsGetValue(Globals::g_nTlsSlotParam1);
    ASSERT(NULL != lpOriginalFunc);

    CFlashOCXLoader* pThis = (CFlashOCXLoader*)TlsGetValue(Globals::g_nTlsSlotParam2);
    ASSERT(NULL != pThis);

	return pThis->Hook_SetTimer(
			lpOriginalFunc, 

			hWnd,
			nIDEvent,
			uElapse,
			lpTimerFunc
		);
}

UINT
WINAPI
CFlashOCXLoader::Hook_SetTimer(
    LPVOID lpOriginalFunc, 

	HWND hWnd,
	UINT_PTR nIDEvent,
	UINT uElapse,
	TIMERPROC lpTimerFunc
)
{
	typedef UINT (WINAPI *P_SetTimer)(
			HWND hWnd,
			UINT_PTR nIDEvent,
			UINT uElapse,
			TIMERPROC lpTimerFunc
		);
	P_SetTimer pSetTimer = (P_SetTimer)lpOriginalFunc;

	return pSetTimer(
			hWnd,
			nIDEvent,
			uElapse,
			lpTimerFunc
		);
}

LPVOID
WINAPI
CFlashOCXLoader::StaticHook_VirtualAlloc(
	IN LPVOID lpAddress,
	IN SIZE_T dwSize,
	IN DWORD flAllocationType,
	IN DWORD flProtect
)
{
    LPVOID lpOriginalFunc = TlsGetValue(Globals::g_nTlsSlotParam1);
    ASSERT(NULL != lpOriginalFunc);

    CFlashOCXLoader* pThis = (CFlashOCXLoader*)TlsGetValue(Globals::g_nTlsSlotParam2);
    ASSERT(NULL != pThis);

	return pThis->Hook_VirtualAlloc(
			lpOriginalFunc, 

			lpAddress,
			dwSize,
			flAllocationType,
			flProtect
		);
}

namespace f_in_box
{
	extern BOOL g_bMinimizeMemory;
}

LPVOID
WINAPI
CFlashOCXLoader::Hook_VirtualAlloc(
    LPVOID lpOriginalFunc, 

	IN LPVOID lpAddress,
	IN SIZE_T dwSize,
	IN DWORD flAllocationType,
	IN DWORD flProtect
)
{
	typedef LPVOID (WINAPI *P_VirtualAlloc)(
			IN LPVOID lpAddress,
			IN SIZE_T dwSize,
			IN DWORD flAllocationType,
			IN DWORD flProtect
		);
	P_VirtualAlloc pVirtualAlloc = (P_VirtualAlloc)lpOriginalFunc;

	if (NULL != lpAddress && ( 0 != ( PAGE_GUARD & flProtect ) ) )
	{
		BOOL bProcessed = FALSE;

		if (!f_in_box::g_bMinimizeMemory)
		{
			// We used the following approach to avoid crash when DEP enabled and SEH is checking
			// But it causes high memory usage
			// That's why currently we use (by default) patching ntdll.dll!NtQueryProcessInformation that is called from ntdll.dll!RtlIsValidHandler

			flProtect &= ~PAGE_GUARD;

			MEMORY_BASIC_INFORMATION mem_info;
			DEF_ZERO_IT(mem_info);
			VirtualQuery(lpAddress, &mem_info, sizeof(mem_info));

			dwSize = mem_info.RegionSize;
		}
	}

    // Clear PAGE_TARGETS_INVALID flag because VirtualAlloc fails on Win 8.1 with this flag (I suspect it's because
    // f-in-box itself allocates memory for flash.ocx code), see ticket #10
	return pVirtualAlloc(lpAddress, dwSize, flAllocationType, flProtect & ~PAGE_TARGETS_INVALID);
}

BOOL
WINAPI
CFlashOCXLoader::StaticHook_VirtualProtect(
    LPVOID lpAddress,
    SIZE_T dwSize,
    DWORD flNewProtect,
    PDWORD lpflOldProtect
)
{
    LPVOID lpOriginalFunc = TlsGetValue(Globals::g_nTlsSlotParam1);
    ASSERT(NULL != lpOriginalFunc);

    CFlashOCXLoader* pThis = (CFlashOCXLoader*)TlsGetValue(Globals::g_nTlsSlotParam2);
    ASSERT(NULL != pThis);

	return pThis->Hook_VirtualProtect(
			lpOriginalFunc, 

            lpAddress,
            dwSize,
            flNewProtect,
            lpflOldProtect
		);
}

BOOL
WINAPI
CFlashOCXLoader::Hook_VirtualProtect(
    LPVOID lpOriginalFunc,

    LPVOID lpAddress,
    SIZE_T dwSize,
    DWORD flNewProtect,
    PDWORD lpflOldProtect
)
{
	typedef BOOL (WINAPI *P_VirtualProtect)(
            LPVOID lpAddress,
            SIZE_T dwSize,
            DWORD flNewProtect,
            PDWORD lpflOldProtect
		);
	P_VirtualProtect pVirtualProtect = (P_VirtualProtect)lpOriginalFunc;

    // Clear PAGE_TARGETS_INVALID flag because VirtualProtect fails on Win 8.1 with this flag (I suspect it's because
    // f-in-box itself Protectates memory for flash.ocx code), see ticket #10
	return pVirtualProtect(lpAddress, dwSize, flNewProtect & ~PAGE_TARGETS_INVALID, lpflOldProtect);
}

BOOL
WINAPI
CFlashOCXLoader::StaticHook_ReadFile(
	HANDLE hFile,
	LPVOID lpBuffer,
	DWORD nNumberOfBytesToRead,
	LPDWORD lpNumberOfBytesRead,
	LPOVERLAPPED lpOverlapped
)
{
    LPVOID lpOriginalFunc = TlsGetValue(Globals::g_nTlsSlotParam1);
    ASSERT(NULL != lpOriginalFunc);

    CFlashOCXLoader* pThis = (CFlashOCXLoader*)TlsGetValue(Globals::g_nTlsSlotParam2);
    ASSERT(NULL != pThis);

	return pThis->Hook_ReadFile(
			lpOriginalFunc, 

			hFile,
			lpBuffer,
			nNumberOfBytesToRead,
			lpNumberOfBytesRead,
			lpOverlapped
		);
}

BOOL
WINAPI
CFlashOCXLoader::Hook_ReadFile(
    LPVOID lpOriginalFunc, 

	HANDLE hFile,
	LPVOID lpBuffer,
	DWORD nNumberOfBytesToRead,
	LPDWORD lpNumberOfBytesRead,
	LPOVERLAPPED lpOverlapped
)
{
	f_in_box::com_helpers::CComPtr<IStream> pStream = m_ContentManager.FindStreamByHandle(hFile);

	if (pStream.IsNull())
	{
		typedef BOOL (WINAPI *P_ReadFile)(
				HANDLE hFile,
				LPVOID lpBuffer,
				DWORD nNumberOfBytesToRead,
				LPDWORD lpNumberOfBytesRead,
				LPOVERLAPPED lpOverlapped
			);
		P_ReadFile pReadFile = (P_ReadFile)lpOriginalFunc;

		return
			pReadFile(
				hFile,
				lpBuffer,
				nNumberOfBytesToRead,
				lpNumberOfBytesRead,
				lpOverlapped
			);
	}
	else
	{
		ULONG nNumberOfBytesRead;
		
		BOOL bRes = SUCCEEDED(pStream->Read(lpBuffer, nNumberOfBytesToRead, &nNumberOfBytesRead));

		if (bRes && lpNumberOfBytesRead)
			*lpNumberOfBytesRead = nNumberOfBytesRead;

		return bRes;
	}
}

BOOL
WINAPI
CFlashOCXLoader::StaticHook_CloseHandle(
	HANDLE hObject
)
{
    LPVOID lpOriginalFunc = TlsGetValue(Globals::g_nTlsSlotParam1);
    ASSERT(NULL != lpOriginalFunc);

    CFlashOCXLoader* pThis = (CFlashOCXLoader*)TlsGetValue(Globals::g_nTlsSlotParam2);
    ASSERT(NULL != pThis);

	return pThis->Hook_CloseHandle(
			lpOriginalFunc, 

			hObject
		);
}

BOOL
WINAPI
CFlashOCXLoader::Hook_CloseHandle(
    LPVOID lpOriginalFunc, 

	HANDLE hObject
)
{
	if (m_ContentManager.CloseFakeHandle(hObject))
		return TRUE;
	else
	{
		typedef BOOL (WINAPI *P_CloseHandle)(
				HANDLE hObject
			);
		P_CloseHandle pCloseHandle = (P_CloseHandle)lpOriginalFunc;

		return
			pCloseHandle(
				hObject
			);
	}
}

DWORD
WINAPI
CFlashOCXLoader::StaticHook_GetFileSize(
	HANDLE hFile,
	LPDWORD lpFileSizeHigh
)
{
    LPVOID lpOriginalFunc = TlsGetValue(Globals::g_nTlsSlotParam1);
    ASSERT(NULL != lpOriginalFunc);

    CFlashOCXLoader* pThis = (CFlashOCXLoader*)TlsGetValue(Globals::g_nTlsSlotParam2);
    ASSERT(NULL != pThis);

	return pThis->Hook_GetFileSize(
			lpOriginalFunc, 

			hFile,
			lpFileSizeHigh
		);
}

DWORD
WINAPI
CFlashOCXLoader::Hook_GetFileSize(
    LPVOID lpOriginalFunc, 

	HANDLE hFile,
	LPDWORD lpFileSizeHigh
)
{
	f_in_box::com_helpers::CComPtr<IStream> pStream = m_ContentManager.FindStreamByHandle(hFile);

	if (pStream.IsNull())
	{
		typedef DWORD (WINAPI *P_GetFileSize)(
				HANDLE hFile,
				LPDWORD lpFileSizeHigh
			);
		P_GetFileSize pGetFileSize = (P_GetFileSize)lpOriginalFunc;

		DWORD dwSize = 
			pGetFileSize(
				hFile,
				lpFileSizeHigh
			);

		return dwSize;
	}
	else
	{
		LARGE_INTEGER zero; DEF_ZERO_IT(zero);
		ULARGE_INTEGER size; DEF_ZERO_IT(size);
		pStream->Seek(zero, STREAM_SEEK_CUR, &size);

		ASSERT(0 == size.HighPart);

		return size.LowPart;
	}
}

BOOL
WINAPI
CFlashOCXLoader::StaticHook_GetFileSizeEx(
	HANDLE hFile, 
	PLARGE_INTEGER lpFileSize
)
{
    LPVOID lpOriginalFunc = TlsGetValue(Globals::g_nTlsSlotParam1);
    ASSERT(NULL != lpOriginalFunc);

    CFlashOCXLoader* pThis = (CFlashOCXLoader*)TlsGetValue(Globals::g_nTlsSlotParam2);
    ASSERT(NULL != pThis);

	return pThis->Hook_GetFileSizeEx(
			lpOriginalFunc, 

			hFile, 
			lpFileSize
		);
}

BOOL
WINAPI
CFlashOCXLoader::Hook_GetFileSizeEx(
    LPVOID lpOriginalFunc, 

	HANDLE hFile, 
	PLARGE_INTEGER lpFileSize
)
{
	f_in_box::com_helpers::CComPtr<IStream> pStream = m_ContentManager.FindStreamByHandle(hFile);

	if (pStream.IsNull())
	{
		typedef BOOL (WINAPI *P_GetFileSizeEx)(
				HANDLE hFile,
				PLARGE_INTEGER lpFileSize
			);
		P_GetFileSizeEx pGetFileSizeEx = (P_GetFileSizeEx)lpOriginalFunc;

		return
			pGetFileSizeEx(
				hFile,
				lpFileSize
			);
	}
	else
	{
		LARGE_INTEGER zero; DEF_ZERO_IT(zero);
		ULARGE_INTEGER size; DEF_ZERO_IT(size);
		ULARGE_INTEGER cur; DEF_ZERO_IT(cur);
		pStream->Seek(zero, STREAM_SEEK_SET, &cur);
		pStream->Seek(zero, STREAM_SEEK_END, &size);
		LARGE_INTEGER cur1;
		cur1.QuadPart = cur.QuadPart;
		pStream->Seek(cur1, STREAM_SEEK_SET, &cur);

		lpFileSize->QuadPart = size.QuadPart;

		return TRUE;
	}
}

DWORD
WINAPI
CFlashOCXLoader::StaticHook_SetFilePointer(
	HANDLE hFile,
	LONG lDistanceToMove,
	PLONG lpDistanceToMoveHigh,
	DWORD dwMoveMethod
)
{
    LPVOID lpOriginalFunc = TlsGetValue(Globals::g_nTlsSlotParam1);
    ASSERT(NULL != lpOriginalFunc);

    CFlashOCXLoader* pThis = (CFlashOCXLoader*)TlsGetValue(Globals::g_nTlsSlotParam2);
    ASSERT(NULL != pThis);

	return pThis->Hook_SetFilePointer(
			lpOriginalFunc, 

			hFile,
			lDistanceToMove,
			lpDistanceToMoveHigh,
			dwMoveMethod
		);
}

DWORD
WINAPI
CFlashOCXLoader::Hook_SetFilePointer(
    LPVOID lpOriginalFunc, 

	HANDLE hFile,
	LONG lDistanceToMove,
	PLONG lpDistanceToMoveHigh,
	DWORD dwMoveMethod
)
{
	f_in_box::com_helpers::CComPtr<IStream> pStream = m_ContentManager.FindStreamByHandle(hFile);

	if (pStream.IsNull())
	{
		typedef DWORD (WINAPI *P_SetFilePointer)(
				HANDLE hFile,
				LONG lDistanceToMove,
				PLONG lpDistanceToMoveHigh,
				DWORD dwMoveMethod
			);
		P_SetFilePointer pSetFilePointer = (P_SetFilePointer)lpOriginalFunc;

		DWORD dwRes = 
			pSetFilePointer(
				hFile,
				lDistanceToMove,
				lpDistanceToMoveHigh,
				dwMoveMethod
			);

		return dwRes;
	}
	else
	{
		LARGE_INTEGER move_to; DEF_ZERO_IT(move_to);
		move_to.LowPart = lDistanceToMove;
		if (lpDistanceToMoveHigh) move_to.HighPart = *lpDistanceToMoveHigh;

		ULARGE_INTEGER new_pos;
		
		return SUCCEEDED(pStream->Seek(move_to, DEF_FILE_TO_STREAM_SEEK(dwMoveMethod), &new_pos));
	}
}

BOOL
WINAPI
CFlashOCXLoader::StaticHook_SetFilePointerEx(
	HANDLE hFile,
	LARGE_INTEGER liDistanceToMove,
	PLARGE_INTEGER lpNewFilePointer,
	DWORD dwMoveMethod
)
{
    LPVOID lpOriginalFunc = TlsGetValue(Globals::g_nTlsSlotParam1);
    ASSERT(NULL != lpOriginalFunc);

    CFlashOCXLoader* pThis = (CFlashOCXLoader*)TlsGetValue(Globals::g_nTlsSlotParam2);
    ASSERT(NULL != pThis);

	return pThis->Hook_SetFilePointerEx(
			lpOriginalFunc, 

			hFile,
			liDistanceToMove,
			lpNewFilePointer,
			dwMoveMethod
		);
}

BOOL
WINAPI
CFlashOCXLoader::Hook_SetFilePointerEx(
    LPVOID lpOriginalFunc, 

	HANDLE hFile,
	LARGE_INTEGER liDistanceToMove,
	PLARGE_INTEGER lpNewFilePointer,
	DWORD dwMoveMethod
)
{
	f_in_box::com_helpers::CComPtr<IStream> pStream = m_ContentManager.FindStreamByHandle(hFile);

	if (pStream.IsNull())
	{
		typedef BOOL (WINAPI *P_SetFilePointerEx)(
				HANDLE hFile,
				LARGE_INTEGER liDistanceToMove,
				PLARGE_INTEGER lpNewFilePointer,
				DWORD dwMoveMethod
			);
		P_SetFilePointerEx pSetFilePointerEx = (P_SetFilePointerEx)lpOriginalFunc;

		return 
			pSetFilePointerEx(
				hFile,
				liDistanceToMove,
				lpNewFilePointer,
				dwMoveMethod
			);
	}
	else
	{
		ULARGE_INTEGER new_pos;
		
		BOOL bRes = SUCCEEDED(pStream->Seek(liDistanceToMove, DEF_FILE_TO_STREAM_SEEK(dwMoveMethod), &new_pos));

		if (bRes && lpNewFilePointer)
			lpNewFilePointer->QuadPart = new_pos.QuadPart;

		return bRes;
	}
}

BOOL CFlashOCXLoader::CanUnloadNow()
{
	typedef HRESULT (__stdcall *P_DllCanUnloadNow)();
	P_DllCanUnloadNow pDllCanUnloadNow = (P_DllCanUnloadNow)GetProcAddress("DllCanUnloadNow");

	if (pDllCanUnloadNow)
		return S_OK == pDllCanUnloadNow();
	else
		return TRUE;
}

BOOL CFlashOCXLoader::UnloadCode()
{
	if (CanUnloadNow())
	{
		Release();
		return TRUE;
	}
	else
	{
		m_bShouldFree = TRUE;
		return FALSE;
	}
}

void CFlashOCXLoader::UnloadIfShould()
{
	if (m_bShouldFree && CanUnloadNow())
		Release();
}

HWND
WINAPI
CFlashOCXLoader::StaticHook_GetFocus()
{
    LPVOID lpOriginalFunc = TlsGetValue(Globals::g_nTlsSlotParam1);
    ASSERT(NULL != lpOriginalFunc);

    CFlashOCXLoader* pThis = (CFlashOCXLoader*)TlsGetValue(Globals::g_nTlsSlotParam2);
    ASSERT(NULL != pThis);

	return pThis->Hook_GetFocus(lpOriginalFunc);
}

HWND
WINAPI
CFlashOCXLoader::Hook_GetFocus(
    LPVOID lpOriginalFunc
)
{
	typedef HWND (__stdcall *P_GetFocus)();
	P_GetFocus pGetFocus = (P_GetFocus)lpOriginalFunc;

	HWND hwndFocus = GetSavedFocus();
	if (DEF_NO_FOCUS_HWND != hwndFocus)
	{
		CFPCWnd* pWnd = CFPCWnd::FromHWND(hwndFocus);

		if (NULL != pWnd)
			return pWnd->GetControlOwner();
	}

	return pGetFocus();
}

namespace f_in_box
{
	const BYTE g_SemitransparentSampleMovieToDetectFlashBehavior[] = {
#include "movie.swf.h"
	};
}

void CFlashOCXLoader::TestCanFlashUseGPUAcceleration()
{
	if (m_bIsCanFlashUseGPUAccelerationInited)
		return;

	m_bIsCanFlashUseGPUAccelerationInited = TRUE;

	struct SFlagManager
	{
		BOOL* m_pbFlag;

		SFlagManager(BOOL* pbFlag) : m_pbFlag(pbFlag)
		{
			*pbFlag = TRUE;
		}

		~SFlagManager()
		{
			*m_pbFlag = FALSE;
		}
	} flag_manager(&m_bWithinGPUAccelerationTest);

	const int nWidth = 16;
	const int nHeight = 16;

	HWND hTestFlashWnd = ::CreateWindowW(MAKEINTRESOURCEW(m_atomWindowClassW), NULL, FPCS_TRANSPARENT | WS_POPUP, 0, 0, nWidth, nHeight, NULL, NULL, NULL, NULL);

	if (NULL != hTestFlashWnd)
	{
		MoveWindow(hTestFlashWnd, 0, 0, nWidth, nHeight, TRUE);

		// Loads test semitransparent movie
		FPCLoadMovieFromMemory(hTestFlashWnd, 0, (PVOID)f_in_box::g_SemitransparentSampleMovieToDetectFlashBehavior, sizeof(f_in_box::g_SemitransparentSampleMovieToDetectFlashBehavior));

        HDC hdcScreen = CreateDC(_T("DISPLAY"), NULL, NULL, NULL);
        HDC hdcMem = CreateCompatibleDC(hdcScreen);

        BITMAPINFO bitmap_info;
		DEF_ZERO_IT(bitmap_info);
        bitmap_info.bmiHeader.biSize = sizeof(bitmap_info.bmiHeader);
        bitmap_info.bmiHeader.biWidth = nWidth;
        bitmap_info.bmiHeader.biHeight = nHeight;
        bitmap_info.bmiHeader.biPlanes = 1;
        bitmap_info.bmiHeader.biBitCount = 32;

		PVOID pBitsWhite;
		HBITMAP hBmpWhite = CreateDIBSection(hdcMem, &bitmap_info, DIB_RGB_COLORS, &pBitsWhite, 0, 0);

        HGDIOBJ hOldBitmap = SelectObject(hdcMem, hBmpWhite);
        HBRUSH hBrushWhite = CreateSolidBrush(RGB(0xff, 0xff, 0xff));
		RECT rcClient;
		rcClient.left = rcClient.top = 0;
		rcClient.right = nWidth; 
		rcClient.bottom = nHeight;
        FillRect(hdcMem, &rcClient, hBrushWhite);
        DeleteObject(hBrushWhite);

		// 
		CFPCWnd* pWnd = CFPCWnd::FromHWND(hTestFlashWnd);

		if (NULL != pWnd)
		{
			// Write to whole white picture
			pWnd->Draw(hdcMem, &rcClient);

			if (0x00ffffff != *(PDWORD)pBitsWhite)
				// The top level pixel changed => this is a problem of Flash 11 (see more e.g. here: https://bugbase.adobe.com/index.cfm?event=bug&id=3019125)
				// Or, may be IDirectDraw::CreateSurface failed (I got DDERR_NOTEXTUREHW in tests)
				// Anyway, we have to use software emulation in such cases
			{
				m_bCanFlashUseGPUAcceleration = FALSE;
			}
			else
				// The pixel has not changed during painting, so we can use GPU acceleration!
			{
				m_bCanFlashUseGPUAcceleration = TRUE;
			}
		}

		SelectObject(hdcMem, hOldBitmap);

		DeleteObject(hBmpWhite);
		DeleteObject(hdcMem);
		DeleteDC(hdcScreen);

		DestroyWindow(hTestFlashWnd);
	}
}

HRESULT
WINAPI 
CFlashOCXLoader::StaticHook_CoCreateInstance(
	REFCLSID rclsid, 
	LPUNKNOWN pUnkOuter, 
	DWORD dwClsContext, 
	REFIID riid, 
	LPVOID *ppv)
{
    LPVOID lpOriginalFunc = TlsGetValue(Globals::g_nTlsSlotParam1);
    ASSERT(NULL != lpOriginalFunc);

    CFlashOCXLoader* pThis = (CFlashOCXLoader*)TlsGetValue(Globals::g_nTlsSlotParam2);
    ASSERT(NULL != pThis);

	return pThis->Hook_CoCreateInstance(
		lpOriginalFunc, 
		rclsid, 
		pUnkOuter, 
		dwClsContext, 
		riid, 
		ppv);
}

HRESULT
WINAPI
CFlashOCXLoader::Hook_CoCreateInstance(
    LPVOID lpOriginalFunc, 

	REFCLSID rclsid, 
	LPUNKNOWN pUnkOuter, 
	DWORD dwClsContext, 
	REFIID riid, 
	LPVOID *ppv
)
{
	typedef HRESULT (WINAPI *P_CoCreateInstance)(
		REFCLSID rclsid, 
		LPUNKNOWN pUnkOuter, 
		DWORD dwClsContext, 
		REFIID riid, 
		LPVOID *ppv);

	P_CoCreateInstance pCoCreateInstance = (P_CoCreateInstance)lpOriginalFunc;

	HRESULT hr = pCoCreateInstance(
		rclsid, 
		pUnkOuter, 
		dwClsContext, 
		riid, 
		ppv);

    if (S_OK == hr && f_in_box::com_helpers::IsEqualGUID(rclsid, f_in_box::CLSID_MMDeviceEnumerator))
		// If CLSID_MMDeviceEnumerator has been created, we hook it to get access to output audio buffer
		// In new Flash versions (e.g. 11.4.402.265), Flash uses MMDeviceEnumerator to produce audio
		// instead of old-fashioned waveOutWrite
	{
		CComPtr<f_in_box::IMMDeviceEnumerator> pMMDeviceEnumerator;
		
		if (S_OK == ((IUnknown*)*ppv)->QueryInterface(f_in_box::IID_IMMDeviceEnumerator, (void**)&pMMDeviceEnumerator))
		{
			PVOID pProxy;
			if (S_OK == CreateMMDeviceEnumeratorProxy(this, pMMDeviceEnumerator, riid, &pProxy))
			{
				// Release old interface
				((IUnknown*)(*ppv))->Release();

				// Returns proxy's pointer instead of just created
				*ppv = pProxy;

				return S_OK;
			}
		}
	}

	return hr;
}

void CFlashOCXLoader::OnProcessAudio(PWAVEFORMATEX pFormat, UINT nNumFramesWritten, PVOID pData)
{
	if (!m_bAudioEnabled)
	{
		f_in_box::MemoryZero(pData, pFormat->nChannels * pFormat->wBitsPerSample / 8 * nNumFramesWritten);
	}
	else
	{
		if (NULL != m_pSoundListener)
		{
			WAVEHDR wavehdr;
			DEF_ZERO_IT(wavehdr);

			wavehdr.dwBufferLength = pFormat->nChannels * pFormat->wBitsPerSample / 8 * nNumFramesWritten;
			wavehdr.lpData = (char*)pData;

			m_pSoundListener((HFPC)this, m_SoundListenerlParam, pFormat, &wavehdr, sizeof(wavehdr));

			{
				if (-1 == m_dwStartTimeForSoundCapturing)
					m_dwStartTimeForSoundCapturing = GetTickCount();




                if (0 != lstrcmpiA(m_strContext.c_str(), DEF_RIGHT_CONTEXT))
                {
					if ((GetTickCount() - m_dwStartTimeForSoundCapturing) >= 10 * 1000)
						// We capture more than 10 seconds -- it's a limitation!
						m_pSoundListener = NULL;
				}


			}
		}

        if (WAVE_FORMAT_PCM == pFormat->wFormatTag)
        {
            WORD wBytesPerSample = pFormat->wBitsPerSample / 8;
		    int nCount = nNumFramesWritten * pFormat->nChannels;

		    if (1 == wBytesPerSample)
		    {
                AdjustVolume<BYTE>(wBytesPerSample, pData, nCount);
		    }
		    else if (2 == wBytesPerSample)
		    {
                AdjustVolume<WORD>(wBytesPerSample, pData, nCount);
		    }
		    else if (4 == wBytesPerSample)
		    {
                AdjustVolume<DWORD>(wBytesPerSample, pData, nCount);
		    }
		    else
			    // Very strange value
		    {
			    ASSERT(FALSE);
		    }
        }
	}
}

template <class TSampleType>
void CFlashOCXLoader::AdjustVolume(WORD wBytesPerSample, PVOID pData, int nCount)
{
    ASSERT(wBytesPerSample == sizeof(TSampleType));

    TSampleType* lpData = (TSampleType*)pData;

    for (int i = 0; i < nCount; i++)
    {
        long nValue = lpData[i];
	    nValue *= (long)m_dwAudioVolume;
        nValue /= DEF_MAX_FLASH_AUDIO_VOLUME;

	    lpData[i] = (TSampleType)nValue;
    }
}

int
WINAPI 
CFlashOCXLoader::StaticHook_GetClipBox(
	HDC hDC,
	LPRECT lpRect
)
{
    LPVOID lpOriginalFunc = TlsGetValue(Globals::g_nTlsSlotParam1);
    ASSERT(NULL != lpOriginalFunc);

    CFlashOCXLoader* pThis = (CFlashOCXLoader*)TlsGetValue(Globals::g_nTlsSlotParam2);
    ASSERT(NULL != pThis);

	return pThis->Hook_GetClipBox(lpOriginalFunc, hDC, lpRect);
}

int
WINAPI
CFlashOCXLoader::Hook_GetClipBox(
    LPVOID lpOriginalFunc, 

	HDC hDC,
	LPRECT lpRect
)
{
	// Flash 11.9 calls GetClipBox and gets NULLREGION in case of semitransparent window
	// Flash considers this result as it's not required to redraw the window
	// That's why we hook this function to return full window rectangle region

	BOOL bFound;
	{
		CCriticalSectionOwner lock(m_csFPCWndHDC);
		bFound = m_FPCWndHDC.IsKeyPresent(hDC);
	}

	if (!bFound)
	{
		typedef HRESULT (WINAPI *P_GetClipBox)(
			HDC hDC,
			LPRECT lpRect);
		P_GetClipBox pGetClipBox = (P_GetClipBox)lpOriginalFunc;
		return pGetClipBox(hDC, lpRect);
	}

	BITMAP structBitmapHeader;
	MemoryZero(&structBitmapHeader, sizeof(BITMAP));
	HGDIOBJ hBitmap = GetCurrentObject(hDC, OBJ_BITMAP);
	GetObject(hBitmap, sizeof(BITMAP), &structBitmapHeader);

	lpRect->left = lpRect->top = 0;
	lpRect->right = structBitmapHeader.bmWidth;
	lpRect->bottom = structBitmapHeader.bmHeight;

	return COMPLEXREGION;
}

LPVOID
WINAPI 
CFlashOCXLoader::StaticHook_RtlPcToFileHeader(
	PVOID PcValue,
	PVOID* BaseOfImage
)
{
    LPVOID lpOriginalFunc = TlsGetValue(Globals::g_nTlsSlotParam1);
    ASSERT(NULL != lpOriginalFunc);

    CFlashOCXLoader* pThis = (CFlashOCXLoader*)TlsGetValue(Globals::g_nTlsSlotParam2);
    ASSERT(NULL != pThis);

	if ((PBYTE)PcValue >= (PBYTE)pThis->GetImageBase() && (PBYTE)PcValue < (PBYTE)pThis->GetImageBase() + pThis->GetImageSize())
	{
		if (NULL != BaseOfImage)
			*BaseOfImage = pThis->GetImageBase();

		return pThis->GetImageBase();
	}

	typedef LPVOID (WINAPI *P_RtlPcToFileHeader)(
		PVOID PcValue,
		PVOID* BaseOfImage);
	P_RtlPcToFileHeader pRtlPcToFileHeader = (P_RtlPcToFileHeader)lpOriginalFunc;
	return pRtlPcToFileHeader(PcValue, BaseOfImage);
}

BOOL
WINAPI 
CFlashOCXLoader::StaticHook_SetProcessValidCallTargets(
    HANDLE hProcess,
    PVOID VirtualAddress,
    SIZE_T RegionSize,
    ULONG NumberOfOffsets,
    PCFG_CALL_TARGET_INFO OffsetInformation)
{
    // Original call returns FALSE and thus Flash 20.0.0.267 calls DebugBreak() on Win 10 (see ticket #9)
    return TRUE;
}
