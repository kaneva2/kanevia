// Copyright (c) Softanics

#include <windows.h>
#include "f_in_box.h"

#ifdef _DEBUG
#define ASSERT(expr) \
    if (!(expr)) \
    { \
        char* buf = (char*)VirtualAlloc(NULL, 64 * 1024, MEM_COMMIT, PAGE_READWRITE); \
        wsprintfA(buf, "File: %s, line: %d\r\nExpression: %s", __FILE__, __LINE__, #expr); \
        \
        if (IDYES == \
            ::MessageBoxA(\
                NULL, \
                buf, \
                "[F-IN-BOX] Debug Assertion Failed", \
                MB_YESNO | MB_SYSTEMMODAL)) \
            DebugBreak(); \
    }
#else
#define ASSERT(expr) {  }
#endif // _DEBUG

namespace f_in_box
{
    extern BYTE g_f_in_box_content[];
    extern DWORD g_f_in_box_content_size;
}

namespace f_in_box
{
    // Image base of manually loaded f_in_box dll
    HMODULE g_hImageBase;

    // TRUE, if static lib functions are redirected to functions of loaded bx sdk dll
    BOOL g_bFunctionsRedirected;

    typedef BOOL (WINAPI *PDLLMAIN)(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved);

    HMODULE Internal_Load(LPCVOID pData);
    FARPROC Internal_GetProcAddress(HMODULE hModule, LPCSTR lpProcName);
    void Internal_CopyMemory(LPVOID Destination, LPCVOID Source, SIZE_T Count);
    void Internal_ZeroMemory(LPCVOID What, SIZE_T Count);

    void MapDll();

    HMODULE Internal_Load(LPCVOID pData)
    {
        // Current pointer
        LPBYTE pPtr = (LPBYTE)pData;
        int intSectionIndex;
        int intRelocationInfoSize;
        PIMAGE_BASE_RELOCATION pImageBaseRelocations, pReloc;
        PIMAGE_IMPORT_DESCRIPTOR pImports, pImport;
        PDLLMAIN pDllMain;

        pPtr += ((PIMAGE_DOS_HEADER)pPtr)->e_lfanew;
        PIMAGE_NT_HEADERS pImageNTHeaders = (PIMAGE_NT_HEADERS)pPtr;

        // Alloc memory for library
        PVOID pImageBase = 
            VirtualAlloc(NULL, pImageNTHeaders->OptionalHeader.SizeOfImage, MEM_COMMIT, PAGE_EXECUTE_READWRITE);

        // Copy headers
        Internal_CopyMemory(pImageBase, pData, pImageNTHeaders->OptionalHeader.SizeOfHeaders);

        // Sections

        // Set pPtr to section table
        pPtr += sizeof(pImageNTHeaders->Signature) + sizeof(pImageNTHeaders->FileHeader) + pImageNTHeaders->FileHeader.SizeOfOptionalHeader;

        for (intSectionIndex = 0; intSectionIndex < pImageNTHeaders->FileHeader.NumberOfSections; intSectionIndex++)
        {
            PIMAGE_SECTION_HEADER pImageSectionHeader = (PIMAGE_SECTION_HEADER)pPtr + intSectionIndex;

            //
            int intVirtualSectionSize = pImageSectionHeader->Misc.VirtualSize;
            //
            int intRawSectionSize = pImageSectionHeader->SizeOfRawData;

            PVOID pSectionBase = (char*)pImageBase + pImageSectionHeader->VirtualAddress;

            Internal_ZeroMemory(pSectionBase, intVirtualSectionSize);

            Internal_CopyMemory(
                pSectionBase, 
                (LPBYTE)pData + pImageSectionHeader->PointerToRawData, 
                    // Sometimes 0 == intRawSectionSize, for example, when the section has uninitialized data
                min(intVirtualSectionSize, intRawSectionSize));
        }

        // Relocations

        // Difference between bases
        ptrdiff_t nImageBaseDelta = (LPBYTE)(DWORD_PTR)pImageBase - (LPBYTE)(DWORD_PTR)pImageNTHeaders->OptionalHeader.ImageBase;

        intRelocationInfoSize = 
            pImageNTHeaders->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_BASERELOC].Size;

        pImageBaseRelocations = 
            (PIMAGE_BASE_RELOCATION)((char*)pImageBase + 
                                    pImageNTHeaders->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_BASERELOC].VirtualAddress);

        pReloc = pImageBaseRelocations;

        while ((char*)pReloc - (char*)pImageBaseRelocations < intRelocationInfoSize)
        {
            // Number of relocs into current block
            int intRelocCount = (pReloc->SizeOfBlock - sizeof(*pReloc)) / sizeof(WORD);

            PWORD pwRelocInfo = (PWORD)((char*)pReloc + sizeof(*pReloc));

            int intRelocIndex;

            for (intRelocIndex = 0; intRelocIndex < intRelocCount; intRelocIndex++)
            {
                if (*pwRelocInfo & 0xf000)
                    *(PDWORD_PTR)((char*)pImageBase + pReloc->VirtualAddress + (*pwRelocInfo & 0x0fff)) += nImageBaseDelta;

                pwRelocInfo++;
            }

            pReloc = (PIMAGE_BASE_RELOCATION)pwRelocInfo;
        }

        // Import table

        pImports = 
            (PIMAGE_IMPORT_DESCRIPTOR)((char*)pImageBase + 
                                    pImageNTHeaders->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].VirtualAddress);

        pImport = pImports;

        while (0 != pImport->Name)
        {
            // DLL Name
            LPSTR lpszLibName = (LPSTR)((PCHAR)pImageBase + pImport->Name);

            // Load library
            HMODULE hLib = LoadLibraryA(lpszLibName);

            DWORD_PTR* lpPRVA_Import = 
                0 == pImport->TimeDateStamp ?
                    (DWORD_PTR*)(pImport->FirstThunk + (PCHAR)pImageBase) :
                    (DWORD_PTR*)(pImport->OriginalFirstThunk + (PCHAR)pImageBase);

            while (*lpPRVA_Import)
            {
                // Get function name
                LPSTR lpszFunctionName;

                if (*lpPRVA_Import & IMAGE_ORDINAL_FLAG)
                    // Importing by number (ordinal)
                    // Ordinal is in the loword
                    lpszFunctionName = (LPSTR)(DWORD_PTR)(*lpPRVA_Import & 0xffff);
                else
                    // Importing by name
                    lpszFunctionName = (LPSTR)((PIMAGE_IMPORT_BY_NAME)((PCHAR)pImageBase + *lpPRVA_Import))->Name;

                // Get function address
                *(LPVOID*)lpPRVA_Import = GetProcAddress(hLib, lpszFunctionName);

                lpPRVA_Import++;
            }

            pImport++;
        }

        FlushInstructionCache(GetCurrentProcess(), pImageBase, pImageNTHeaders->OptionalHeader.SizeOfImage);

        if (0 != pImageNTHeaders->OptionalHeader.AddressOfEntryPoint)
        {
            // Entry point
            pDllMain = (PDLLMAIN)((PCHAR)pImageBase + pImageNTHeaders->OptionalHeader.AddressOfEntryPoint);

            if (NULL != pDllMain)
            {
                pDllMain((HMODULE)pImageBase, DLL_PROCESS_ATTACH, NULL);
                pDllMain((HMODULE)pImageBase, DLL_THREAD_ATTACH, NULL);
            }
        }

        return (HMODULE)pImageBase;
    }

    FARPROC Internal_GetProcAddress(HMODULE hModule, LPCSTR lpProcName)
    {
        FARPROC lpAddressOfProc = NULL;

        if (NULL != hModule)
        {
            LPBYTE pPtr = (LPBYTE)hModule;
            PIMAGE_NT_HEADERS pImageNTHeaders;
            PIMAGE_EXPORT_DIRECTORY pExports;
            DWORD dwExportedSymbolIndex;

            pPtr += ((PIMAGE_DOS_HEADER)pPtr)->e_lfanew;

            pImageNTHeaders = (PIMAGE_NT_HEADERS)pPtr;

            pExports = (PIMAGE_EXPORT_DIRECTORY)((char*)hModule + pImageNTHeaders->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT].VirtualAddress);

            for (dwExportedSymbolIndex = 0; dwExportedSymbolIndex < pExports->NumberOfNames; dwExportedSymbolIndex++)
            {
                DWORD dwVirtualAddressOfName = ((PDWORD)((PCHAR)hModule + pExports->AddressOfNames))[dwExportedSymbolIndex];
                LPSTR lpszName = (LPSTR)((PCHAR)hModule + dwVirtualAddressOfName);

                if (!lstrcmpA(lpszName, lpProcName))
                {
                    WORD wIndex = ((PWORD)((PCHAR)hModule + pExports->AddressOfNameOrdinals))[dwExportedSymbolIndex];

                    DWORD dwVirtualAddressOfAddressOfProc = ((PDWORD)((PCHAR)hModule + pExports->AddressOfFunctions))[wIndex];

                    lpAddressOfProc = (FARPROC)((PCHAR)hModule + dwVirtualAddressOfAddressOfProc);
                }
            }
        }

        return lpAddressOfProc;
    }

    void Internal_CopyMemory(LPVOID Destination, LPCVOID Source, SIZE_T Count)
    {
        typedef void (__cdecl *P_memcpy)(LPVOID Destination, LPCVOID Source, SIZE_T Count);
        static P_memcpy pmemcpy = NULL;

        if (NULL == pmemcpy)
            pmemcpy = (P_memcpy)GetProcAddress(GetModuleHandleA("ntdll.dll"), "memcpy");

        pmemcpy(Destination, Source, Count);
    }

    void Internal_ZeroMemory(LPCVOID What, SIZE_T Count)
    {
        typedef void (__stdcall *P_RtlZeroMemory)(LPCVOID What, SIZE_T Count);
        static P_RtlZeroMemory pRtlZeroMemory = NULL;

        if (NULL == pRtlZeroMemory)
            pRtlZeroMemory = (P_RtlZeroMemory)GetProcAddress(GetModuleHandleA("kernel32.dll"), "RtlZeroMemory");

        pRtlZeroMemory(What, Count);
    }

    void MapDll()
    {
        if (NULL != g_hImageBase)
            // Already initialized
        {
            return;
        }

        g_hImageBase = Internal_Load(g_f_in_box_content);

        if (NULL == g_hImageBase)
            // Fatal error
        {
            ASSERT(FALSE);
            return;
        }
    }
}

// Each function should have minimal (because it should be implemented, we just hook it to redirect to actual implementation
// that is located in the mapped bx sdk dll) and different (to prevent compiler from generation one body for several functions -
// it just will break hooking: each functions should have its own code) implementation
// Notice: we use a lot of DebugBreak() calls to have enough space for new code that will be made by PatchByCallAndJump()
#define DEF_INIT_IMPL_RETURN(type) \
    DebugBreak(); \
    DebugBreak(); \
    DebugBreak(); \
    DebugBreak(); \
    DebugBreak(); \
    DebugBreak(); \
    DebugBreak(); \
    DebugBreak(); \
    DebugBreak(); \
    DebugBreak(); \
    DebugBreak(); \
    DebugBreak(); \
    DebugBreak(); \
    DebugBreak(); \
    DebugBreak(); \
    DebugBreak(); \
    return (type)(DWORD_PTR)((int)(DWORD_PTR)__FILE__ % __LINE__);

#define DEF_FUNCTION_BODY(function_name) \
DEF_RETURN_VALUE WINAPI function_name DEF_ARGUMENTS \
{ \
    f_in_box::MapDll(); \
 \
    typedef DEF_RETURN_VALUE (WINAPI *P_Func) DEF_ARGUMENTS; \
    P_Func pFunc = (P_Func)f_in_box::Internal_GetProcAddress(f_in_box::g_hImageBase, #function_name); \
 \
    return pFunc DEF_ARGUMENT_NAMES; \
}

// BOOL WINAPI RegisterFlashWindowClass()
#define DEF_RETURN_VALUE BOOL
#define DEF_ARGUMENTS ()
#define DEF_ARGUMENT_NAMES ()
DEF_FUNCTION_BODY(RegisterFlashWindowClass)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// BOOL WINAPI RegisterFlashWindowClassEx(LPVOID lpFlashOCXCodeData, DWORD dwSizeOfFlashOCXCode)
#define DEF_RETURN_VALUE BOOL
#define DEF_ARGUMENTS (LPVOID lpFlashOCXCodeData, DWORD dwSizeOfFlashOCXCode)
#define DEF_ARGUMENT_NAMES (lpFlashOCXCodeData, dwSizeOfFlashOCXCode)
DEF_FUNCTION_BODY(RegisterFlashWindowClassEx)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// void WINAPI UnregisterFlashWindowClass()
#define DEF_RETURN_VALUE VOID
#define DEF_ARGUMENTS ()
#define DEF_ARGUMENT_NAMES ()
DEF_FUNCTION_BODY(UnregisterFlashWindowClass)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// int WINAPI FPC_CompareVersions(const SFPCVersion* pVersion1, const SFPCVersion* pVersion2)
#define DEF_RETURN_VALUE int
#define DEF_ARGUMENTS (const SFPCVersion* pVersion1, const SFPCVersion* pVersion2)
#define DEF_ARGUMENT_NAMES (pVersion1, pVersion2)
DEF_FUNCTION_BODY(FPC_CompareVersions)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// void WINAPI FPC_BuildVersion(WORD v3, WORD v2, WORD v1, WORD v0, SFPCVersion* pVersion)
#define DEF_RETURN_VALUE VOID
#define DEF_ARGUMENTS (WORD v3, WORD v2, WORD v1, WORD v0, SFPCVersion* pVersion)
#define DEF_ARGUMENT_NAMES (v3, v2, v1, v0, pVersion)
DEF_FUNCTION_BODY(FPC_BuildVersion)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// BOOL WINAPI GetInstalledFlashVersionEx(SFPCVersion* pVersion)
#define DEF_RETURN_VALUE BOOL
#define DEF_ARGUMENTS (SFPCVersion* pVersion)
#define DEF_ARGUMENT_NAMES (pVersion)
DEF_FUNCTION_BODY(GetInstalledFlashVersionEx)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// BOOL WINAPI GetUsingFlashVersionEx(SFPCVersion* pVersion)
#define DEF_RETURN_VALUE BOOL
#define DEF_ARGUMENTS (SFPCVersion* pVersion)
#define DEF_ARGUMENT_NAMES (pVersion)
DEF_FUNCTION_BODY(GetUsingFlashVersionEx)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// void WINAPI FPCSetAudioEnabled(BOOL bEnable)
#define DEF_RETURN_VALUE VOID
#define DEF_ARGUMENTS (BOOL bEnable)
#define DEF_ARGUMENT_NAMES (bEnable)
DEF_FUNCTION_BODY(FPCSetAudioEnabled)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// BOOL WINAPI FPCGetAudioEnabled()
#define DEF_RETURN_VALUE BOOL
#define DEF_ARGUMENTS ()
#define DEF_ARGUMENT_NAMES ()
DEF_FUNCTION_BODY(FPCGetAudioEnabled)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// BOOL WINAPI FPCIsTransparentAvailable()
#define DEF_RETURN_VALUE BOOL
#define DEF_ARGUMENTS ()
#define DEF_ARGUMENT_NAMES ()
DEF_FUNCTION_BODY(FPCIsTransparentAvailable)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// BOOL WINAPI FPCIsFlashInstalled()
#define DEF_RETURN_VALUE BOOL
#define DEF_ARGUMENTS ()
#define DEF_ARGUMENT_NAMES ()
DEF_FUNCTION_BODY(FPCIsFlashInstalled)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// void WINAPI FPCSetGlobalOnLoadExternalResourceHandler(PLOADEXTERNALRESOURCEHANDLER pHandler)
#define DEF_RETURN_VALUE VOID
#define DEF_ARGUMENTS (PLOADEXTERNALRESOURCEHANDLER pHandler)
#define DEF_ARGUMENT_NAMES (pHandler)
DEF_FUNCTION_BODY(FPCSetGlobalOnLoadExternalResourceHandler)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// BOOL WINAPI FPCLoadMovieUsingStream(HWND hwndFlashPlayerControl, int layer, IStream** ppStream)
#define DEF_RETURN_VALUE BOOL
#define DEF_ARGUMENTS (HWND hwndFlashPlayerControl, int layer, IStream** ppStream)
#define DEF_ARGUMENT_NAMES (hwndFlashPlayerControl, layer, ppStream)
DEF_FUNCTION_BODY(FPCLoadMovieUsingStream)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// BOOL WINAPI FPCPutMovieUsingStream(HWND hwndFlashPlayerControl, IStream** ppStream)
#define DEF_RETURN_VALUE BOOL
#define DEF_ARGUMENTS (HWND hwndFlashPlayerControl, IStream** ppStream)
#define DEF_ARGUMENT_NAMES (hwndFlashPlayerControl, ppStream)
DEF_FUNCTION_BODY(FPCPutMovieUsingStream)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// BOOL WINAPI FPCLoadMovieFromStream(HWND hwndFlashPlayerControl, int layer, IStream* pStream)
#define DEF_RETURN_VALUE BOOL
#define DEF_ARGUMENTS (HWND hwndFlashPlayerControl, int layer, IStream* pStream)
#define DEF_ARGUMENT_NAMES (hwndFlashPlayerControl, layer, pStream)
DEF_FUNCTION_BODY(FPCLoadMovieFromStream)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// BOOL WINAPI FPCPutMovieFromStream(HWND hwndFlashPlayerControl, IStream* pStream)
#define DEF_RETURN_VALUE BOOL
#define DEF_ARGUMENTS (HWND hwndFlashPlayerControl, IStream* pStream)
#define DEF_ARGUMENT_NAMES (hwndFlashPlayerControl, pStream)
DEF_FUNCTION_BODY(FPCPutMovieFromStream)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// BOOL WINAPI FPCLoadMovieFromMemory(HWND hwndFlashPlayerControl, int layer, LPVOID lpData, DWORD dwSize)
#define DEF_RETURN_VALUE BOOL
#define DEF_ARGUMENTS (HWND hwndFlashPlayerControl, int layer, LPVOID lpData, DWORD dwSize)
#define DEF_ARGUMENT_NAMES (hwndFlashPlayerControl, layer, lpData, dwSize)
DEF_FUNCTION_BODY(FPCLoadMovieFromMemory)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// BOOL WINAPI FPCPutMovieFromMemory(HWND hwndFlashPlayerControl, LPVOID lpData, DWORD dwSize)
#define DEF_RETURN_VALUE BOOL
#define DEF_ARGUMENTS (HWND hwndFlashPlayerControl, LPVOID lpData, DWORD dwSize)
#define DEF_ARGUMENT_NAMES (hwndFlashPlayerControl, lpData, dwSize)
DEF_FUNCTION_BODY(FPCPutMovieFromMemory)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// BOOL WINAPI FPCLoadMovieFromResourceA(HWND hwndFlashPlayerControl, int layer, HINSTANCE hInstance, LPCSTR lpName, LPCSTR lpType)
#define DEF_RETURN_VALUE BOOL
#define DEF_ARGUMENTS (HWND hwndFlashPlayerControl, int layer, HINSTANCE hInstance, LPCSTR lpName, LPCSTR lpType)
#define DEF_ARGUMENT_NAMES (hwndFlashPlayerControl, layer, hInstance, lpName, lpType)
DEF_FUNCTION_BODY(FPCLoadMovieFromResourceA)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// BOOL WINAPI FPCLoadMovieFromResourceW(HWND hwndFlashPlayerControl, int layer, HINSTANCE hInstance, LPCWSTR lpName, LPCWSTR lpType)
#define DEF_RETURN_VALUE BOOL
#define DEF_ARGUMENTS (HWND hwndFlashPlayerControl, int layer, HINSTANCE hInstance, LPCWSTR lpName, LPCWSTR lpType)
#define DEF_ARGUMENT_NAMES (hwndFlashPlayerControl, layer, hInstance, lpName, lpType)
DEF_FUNCTION_BODY(FPCLoadMovieFromResourceW)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// BOOL WINAPI FPCPutMovieFromResourceA(HWND hwndFlashPlayerControl, HINSTANCE hInstance, LPCSTR lpName, LPCSTR lpType)
#define DEF_RETURN_VALUE BOOL
#define DEF_ARGUMENTS (HWND hwndFlashPlayerControl, HINSTANCE hInstance, LPCSTR lpName, LPCSTR lpType)
#define DEF_ARGUMENT_NAMES (hwndFlashPlayerControl, hInstance, lpName, lpType)
DEF_FUNCTION_BODY(FPCPutMovieFromResourceA)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// BOOL WINAPI FPCPutMovieFromResourceW(HWND hwndFlashPlayerControl, HINSTANCE hInstance, LPCWSTR lpName, LPCWSTR lpType)
#define DEF_RETURN_VALUE BOOL
#define DEF_ARGUMENTS (HWND hwndFlashPlayerControl, HINSTANCE hInstance, LPCWSTR lpName, LPCWSTR lpType)
#define DEF_ARGUMENT_NAMES (hwndFlashPlayerControl, hInstance, lpName, lpType)
DEF_FUNCTION_BODY(FPCPutMovieFromResourceW)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// BOOL WINAPI FPCSetEventListener(HWND hwndFlashPlayerControl, PFLASHPLAYERCONTROLEVENTLISTENER pListener, LPARAM lParam)
#define DEF_RETURN_VALUE BOOL
#define DEF_ARGUMENTS (HWND hwndFlashPlayerControl, PFLASHPLAYERCONTROLEVENTLISTENER pListener, LPARAM lParam)
#define DEF_ARGUMENT_NAMES (hwndFlashPlayerControl, pListener, lParam)
DEF_FUNCTION_BODY(FPCSetEventListener)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPCCallFunctionA(HWND hwndFlashPlayerControl, LPCSTR lpszRequest, LPSTR lpszResponse, DWORD* pdwResponseLength)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (HWND hwndFlashPlayerControl, LPCSTR lpszRequest, LPSTR lpszResponse, DWORD* pdwResponseLength)
#define DEF_ARGUMENT_NAMES (hwndFlashPlayerControl, lpszRequest, lpszResponse, pdwResponseLength)
DEF_FUNCTION_BODY(FPCCallFunctionA)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPCCallFunctionW(HWND hwndFlashPlayerControl, LPCWSTR lpszRequest, LPWSTR lpszResponse, DWORD* pdwResponseLength)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (HWND hwndFlashPlayerControl, LPCWSTR lpszRequest, LPWSTR lpszResponse, DWORD* pdwResponseLength)
#define DEF_ARGUMENT_NAMES (hwndFlashPlayerControl, lpszRequest, lpszResponse, pdwResponseLength)
DEF_FUNCTION_BODY(FPCCallFunctionW)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPCCallFunctionBSTR(HWND hwndFlashPlayerControl, BSTR bstrRequest, BSTR* bstrResponse)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (HWND hwndFlashPlayerControl, BSTR bstrRequest, BSTR* bstrResponse)
#define DEF_ARGUMENT_NAMES (hwndFlashPlayerControl, bstrRequest, bstrResponse)
DEF_FUNCTION_BODY(FPCCallFunctionBSTR)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPCSetReturnValueA(HWND hwndFlashPlayerControl, LPCSTR lpszReturnValue)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (HWND hwndFlashPlayerControl, LPCSTR lpszReturnValue)
#define DEF_ARGUMENT_NAMES (hwndFlashPlayerControl, lpszReturnValue)
DEF_FUNCTION_BODY(FPCSetReturnValueA)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPCSetReturnValueW(HWND hwndFlashPlayerControl, LPCWSTR lpszReturnValue)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (HWND hwndFlashPlayerControl, LPCWSTR lpszReturnValue)
#define DEF_ARGUMENT_NAMES (hwndFlashPlayerControl, lpszReturnValue)
DEF_FUNCTION_BODY(FPCSetReturnValueW)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_PutStandardMenu(HWND hwndFlashPlayerControl, BOOL bEnable)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (HWND hwndFlashPlayerControl, BOOL bEnable)
#define DEF_ARGUMENT_NAMES (hwndFlashPlayerControl, bEnable)
DEF_FUNCTION_BODY(FPC_PutStandardMenu)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_GetStandardMenu(HWND hwndFlashPlayerControl, BOOL* pbEnable)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (HWND hwndFlashPlayerControl, BOOL* pbEnable)
#define DEF_ARGUMENT_NAMES (hwndFlashPlayerControl, pbEnable)
DEF_FUNCTION_BODY(FPC_GetStandardMenu)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_PutAudioVolume(DWORD dwVolume)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (DWORD dwVolume)
#define DEF_ARGUMENT_NAMES (dwVolume)
DEF_FUNCTION_BODY(FPC_PutAudioVolume)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_GetAudioVolume(DWORD* pdwVolume)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (DWORD* pdwVolume)
#define DEF_ARGUMENT_NAMES (pdwVolume)
DEF_FUNCTION_BODY(FPC_GetAudioVolume)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HMODULE WINAPI FPC_Internal_GetFlashOCXHandle()
#define DEF_RETURN_VALUE HMODULE
#define DEF_ARGUMENTS ()
#define DEF_ARGUMENT_NAMES ()
DEF_FUNCTION_BODY(FPC_Internal_GetFlashOCXHandle)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// BOOL WINAPI FPC_Internal_HookFunc(LPCSTR lpszDLLName, LPCSTR lpszFuncName, LPVOID* pOldFunc, LPVOID lpNewFunc)
#define DEF_RETURN_VALUE BOOL
#define DEF_ARGUMENTS (LPCSTR lpszDLLName, LPCSTR lpszFuncName, LPVOID* pOldFunc, LPVOID lpNewFunc)
#define DEF_ARGUMENT_NAMES (lpszDLLName, lpszFuncName, pOldFunc, lpNewFunc)
DEF_FUNCTION_BODY(FPC_Internal_HookFunc)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HFPC WINAPI FPC_LoadOCXCodeFromMemory(LPVOID lpFlashOCXCodeData, DWORD dwSizeOfFlashOCXCode)
#define DEF_RETURN_VALUE HFPC
#define DEF_ARGUMENTS (LPVOID lpFlashOCXCodeData, DWORD dwSizeOfFlashOCXCode)
#define DEF_ARGUMENT_NAMES (lpFlashOCXCodeData, dwSizeOfFlashOCXCode)
DEF_FUNCTION_BODY(FPC_LoadOCXCodeFromMemory)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HFPC WINAPI FPC_LoadRegisteredOCX()
#define DEF_RETURN_VALUE HFPC
#define DEF_ARGUMENTS ()
#define DEF_ARGUMENT_NAMES ()
DEF_FUNCTION_BODY(FPC_LoadRegisteredOCX)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// BOOL WINAPI FPC_UnloadCode(HFPC hFPC)
#define DEF_RETURN_VALUE BOOL
#define DEF_ARGUMENTS (HFPC hFPC)
#define DEF_ARGUMENT_NAMES (hFPC)
DEF_FUNCTION_BODY(FPC_UnloadCode)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// BOOL WINAPI FPC_CanUnloadNow(HFPC hFPC)
#define DEF_RETURN_VALUE BOOL
#define DEF_ARGUMENTS (HFPC hFPC)
#define DEF_ARGUMENT_NAMES (hFPC)
DEF_FUNCTION_BODY(FPC_CanUnloadNow)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// BOOL WINAPI FPC_SetOption(HFPC hFPC, DWORD dwOption, DWORD dwValue)
#define DEF_RETURN_VALUE BOOL
#define DEF_ARGUMENTS (HFPC hFPC, DWORD dwOption, DWORD dwValue)
#define DEF_ARGUMENT_NAMES (hFPC, dwOption, dwValue)
DEF_FUNCTION_BODY(FPC_SetOption)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// DWORD WINAPI FPC_GetOption(HFPC hFPC, DWORD dwOption)
#define DEF_RETURN_VALUE DWORD
#define DEF_ARGUMENTS (HFPC hFPC, DWORD dwOption)
#define DEF_ARGUMENT_NAMES (hFPC, dwOption)
DEF_FUNCTION_BODY(FPC_GetOption)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// ATOM WINAPI FPC_GetClassAtomA(HFPC hFPC)
#define DEF_RETURN_VALUE ATOM
#define DEF_ARGUMENTS (HFPC hFPC)
#define DEF_ARGUMENT_NAMES (hFPC)
DEF_FUNCTION_BODY(FPC_GetClassAtomA)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// ATOM WINAPI FPC_GetClassAtomW(HFPC hFPC)
#define DEF_RETURN_VALUE ATOM
#define DEF_ARGUMENTS (HFPC hFPC)
#define DEF_ARGUMENT_NAMES (hFPC)
DEF_FUNCTION_BODY(FPC_GetClassAtomW)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// LPCSTR WINAPI FPC_GetClassNameA(HFPC hFPC)
#define DEF_RETURN_VALUE LPCSTR
#define DEF_ARGUMENTS (HFPC hFPC)
#define DEF_ARGUMENT_NAMES (hFPC)
DEF_FUNCTION_BODY(FPC_GetClassNameA)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// LPCWSTR WINAPI FPC_GetClassNameW(HFPC hFPC)
#define DEF_RETURN_VALUE LPCWSTR
#define DEF_ARGUMENTS (HFPC hFPC)
#define DEF_ARGUMENT_NAMES (hFPC)
DEF_FUNCTION_BODY(FPC_GetClassNameW)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HWND WINAPI FPC_CreateWindowA(HFPC hFPC, DWORD dwExStyle, LPCSTR lpWindowName, DWORD dwStyle, int x, int y, int nWidth, int nHeight, HWND hWndParent, HMENU hMenu, HINSTANCE hInstance, LPVOID lpParam)
#define DEF_RETURN_VALUE HWND
#define DEF_ARGUMENTS (HFPC hFPC, DWORD dwExStyle, LPCSTR lpWindowName, DWORD dwStyle, int x, int y, int nWidth, int nHeight, HWND hWndParent, HMENU hMenu, HINSTANCE hInstance, LPVOID lpParam)
#define DEF_ARGUMENT_NAMES (hFPC, dwExStyle, lpWindowName, dwStyle, x, y, nWidth, nHeight, hWndParent, hMenu, hInstance, lpParam)
DEF_FUNCTION_BODY(FPC_CreateWindowA)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HWND WINAPI FPC_CreateWindowW(HFPC hFPC, DWORD dwExStyle, LPCWSTR lpWindowName, DWORD dwStyle, int x, int y, int nWidth, int nHeight, HWND hWndParent, HMENU hMenu, HINSTANCE hInstance,LPVOID lpParam)
#define DEF_RETURN_VALUE HWND
#define DEF_ARGUMENTS (HFPC hFPC, DWORD dwExStyle, LPCWSTR lpWindowName, DWORD dwStyle, int x, int y, int nWidth, int nHeight, HWND hWndParent, HMENU hMenu, HINSTANCE hInstance,LPVOID lpParam)
#define DEF_ARGUMENT_NAMES (hFPC, dwExStyle, lpWindowName, dwStyle, x, y, nWidth, nHeight, hWndParent, hMenu, hInstance, lpParam)
DEF_FUNCTION_BODY(FPC_CreateWindowW)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// void WINAPI FPC_AttachToWindow(HFPC hFPC, HWND hWnd)
#define DEF_RETURN_VALUE VOID
#define DEF_ARGUMENTS (HFPC hFPC, HWND hWnd)
#define DEF_ARGUMENT_NAMES (hFPC, hWnd)
DEF_FUNCTION_BODY(FPC_AttachToWindow)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// BOOL WINAPI FPC_GetVersionEx(HFPC hFPC, SFPCVersion* pVersion)
#define DEF_RETURN_VALUE BOOL
#define DEF_ARGUMENTS (HFPC hFPC, SFPCVersion* pVersion)
#define DEF_ARGUMENT_NAMES (hFPC, pVersion)
DEF_FUNCTION_BODY(FPC_GetVersionEx)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// void WINAPI FPC_EnableSound(HFPC hFPC, BOOL bEnable)
#define DEF_RETURN_VALUE VOID
#define DEF_ARGUMENTS (HFPC hFPC, BOOL bEnable)
#define DEF_ARGUMENT_NAMES (hFPC, bEnable)
DEF_FUNCTION_BODY(FPC_EnableSound)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// BOOL WINAPI FPC_IsSoundEnabled(HFPC hFPC)
#define DEF_RETURN_VALUE BOOL
#define DEF_ARGUMENTS (HFPC hFPC)
#define DEF_ARGUMENT_NAMES (hFPC)
DEF_FUNCTION_BODY(FPC_IsSoundEnabled)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// DWORD WINAPI FPC_AddOnLoadExternalResourceHandlerA(HFPC hFPC, PLOAD_EXTERNAL_RESOURCE_HANDLERA pHandler, LPARAM lParam)
#define DEF_RETURN_VALUE DWORD
#define DEF_ARGUMENTS (HFPC hFPC, PLOAD_EXTERNAL_RESOURCE_HANDLERA pHandler, LPARAM lParam)
#define DEF_ARGUMENT_NAMES (hFPC, pHandler, lParam)
DEF_FUNCTION_BODY(FPC_AddOnLoadExternalResourceHandlerA)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// DWORD WINAPI FPC_AddOnLoadExternalResourceHandlerW(HFPC hFPC, PLOAD_EXTERNAL_RESOURCE_HANDLERW pHandler, LPARAM lParam)
#define DEF_RETURN_VALUE DWORD
#define DEF_ARGUMENTS (HFPC hFPC, PLOAD_EXTERNAL_RESOURCE_HANDLERW pHandler, LPARAM lParam)
#define DEF_ARGUMENT_NAMES (hFPC, pHandler, lParam)
DEF_FUNCTION_BODY(FPC_AddOnLoadExternalResourceHandlerW)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_RemoveOnLoadExternalResourceHandler(HFPC hFPC, DWORD dwCookie)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (HFPC hFPC, DWORD dwCookie)
#define DEF_ARGUMENT_NAMES (hFPC, dwCookie)
DEF_FUNCTION_BODY(FPC_RemoveOnLoadExternalResourceHandler)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_SetSoundVolume(HFPC hFPC, DWORD dwVolume)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (HFPC hFPC, DWORD dwVolume)
#define DEF_ARGUMENT_NAMES (hFPC, dwVolume)
DEF_FUNCTION_BODY(FPC_SetSoundVolume)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// DWORD WINAPI FPC_GetSoundVolume(HFPC hFPC)
#define DEF_RETURN_VALUE DWORD
#define DEF_ARGUMENTS (HFPC hFPC)
#define DEF_ARGUMENT_NAMES (hFPC)
DEF_FUNCTION_BODY(FPC_GetSoundVolume)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HWND WINAPI FPC_GetAxHWND(HWND hWndFPC)
#define DEF_RETURN_VALUE HWND
#define DEF_ARGUMENTS (HWND hWndFPC)
#define DEF_ARGUMENT_NAMES (hWndFPC)
DEF_FUNCTION_BODY(FPC_GetAxHWND)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// void WINAPI FPC_SetContext(HWND hWnd, LPCSTR lpszContext)
#define DEF_RETURN_VALUE VOID
#define DEF_ARGUMENTS (HWND hWnd, LPCSTR lpszContext)
#define DEF_ARGUMENT_NAMES (hWnd, lpszContext)
DEF_FUNCTION_BODY(FPC_SetContext)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// ULONG WINAPI FPC_IStream_AddRef(IStream* pStream)
#define DEF_RETURN_VALUE ULONG
#define DEF_ARGUMENTS (IStream* pStream)
#define DEF_ARGUMENT_NAMES (pStream)
DEF_FUNCTION_BODY(FPC_IStream_AddRef)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// ULONG WINAPI FPC_IStream_Release(IStream* pStream)
#define DEF_RETURN_VALUE ULONG
#define DEF_ARGUMENTS (IStream* pStream)
#define DEF_ARGUMENT_NAMES (pStream)
DEF_FUNCTION_BODY(FPC_IStream_Release)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_IStream_Write(IStream* pStream, const void *pv, ULONG cb, ULONG *pcbWritten)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (IStream* pStream, const void *pv, ULONG cb, ULONG *pcbWritten)
#define DEF_ARGUMENT_NAMES (pStream, pv, cb, pcbWritten)
DEF_FUNCTION_BODY(FPC_IStream_Write)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_IStream_SetSize(IStream* pStream, ULONG nSize)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (IStream* pStream, ULONG nSize)
#define DEF_ARGUMENT_NAMES (pStream, nSize)
DEF_FUNCTION_BODY(FPC_IStream_SetSize)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// DWORD WINAPI FPC_AddGetBindInfoHandler(HFPC hFPC, PGET_BIND_INFO_HANDLER pHandler, LPARAM lParam)
#define DEF_RETURN_VALUE DWORD
#define DEF_ARGUMENTS (HFPC hFPC, PGET_BIND_INFO_HANDLER pHandler, LPARAM lParam)
#define DEF_ARGUMENT_NAMES (hFPC, pHandler, lParam)
DEF_FUNCTION_BODY(FPC_AddGetBindInfoHandler)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_RemoveGetBindInfoHandler(HFPC hFPC, DWORD dwCookie)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (HFPC hFPC, DWORD dwCookie)
#define DEF_ARGUMENT_NAMES (hFPC, dwCookie)
DEF_FUNCTION_BODY(FPC_RemoveGetBindInfoHandler)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// LPVOID* WINAPI FPC_GetImportTableEntry(HFPC hFPC, LPCSTR lpszDLLName, LPCSTR lpszFuncName)
#define DEF_RETURN_VALUE LPVOID*
#define DEF_ARGUMENTS (HFPC hFPC, LPCSTR lpszDLLName, LPCSTR lpszFuncName)
#define DEF_ARGUMENT_NAMES (hFPC, lpszDLLName, lpszFuncName)
DEF_FUNCTION_BODY(FPC_GetImportTableEntry)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// UINT_PTR WINAPI FPC_StartMinimizeMemoryTimer(int nInterval)
#define DEF_RETURN_VALUE UINT_PTR
#define DEF_ARGUMENTS (int nInterval)
#define DEF_ARGUMENT_NAMES (nInterval)
DEF_FUNCTION_BODY(FPC_StartMinimizeMemoryTimer)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// void WINAPI FPC_StopMinimizeMemoryTimer(UINT_PTR nTimerId)
#define DEF_RETURN_VALUE VOID
#define DEF_ARGUMENTS (UINT_PTR nTimerId)
#define DEF_ARGUMENT_NAMES (nTimerId)
DEF_FUNCTION_BODY(FPC_StopMinimizeMemoryTimer)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// void WINAPI FPC_StopFPCMinimizeMemoryTimer(HFPC hFPC)
#define DEF_RETURN_VALUE VOID
#define DEF_ARGUMENTS (HFPC hFPC)
#define DEF_ARGUMENT_NAMES (hFPC)
DEF_FUNCTION_BODY(FPC_StopFPCMinimizeMemoryTimer)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// DWORD WINAPI FPC_SetSoundListener(HFPC hFPC, PSOUNDLISTENER pSoundListener, LPARAM lParam)
#define DEF_RETURN_VALUE DWORD
#define DEF_ARGUMENTS (HFPC hFPC, PSOUNDLISTENER pSoundListener, LPARAM lParam)
#define DEF_ARGUMENT_NAMES (hFPC, pSoundListener, lParam)
DEF_FUNCTION_BODY(FPC_SetSoundListener)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// DWORD WINAPI FPC_SetPreProcessURLHandler(HFPC hFPC, PPREPROCESSURLHANDLER pHandler, LPARAM lParam)
#define DEF_RETURN_VALUE DWORD
#define DEF_ARGUMENTS (HFPC hFPC, PPREPROCESSURLHANDLER pHandler, LPARAM lParam)
#define DEF_ARGUMENT_NAMES (hFPC, pHandler, lParam)
DEF_FUNCTION_BODY(FPC_SetPreProcessURLHandler)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// BOOL WINAPI FPC_EnableFullScreen(HWND hwndFlashPlayerControl, BOOL bEnable)
#define DEF_RETURN_VALUE BOOL
#define DEF_ARGUMENTS (HWND hwndFlashPlayerControl, BOOL bEnable)
#define DEF_ARGUMENT_NAMES (hwndFlashPlayerControl, bEnable)
DEF_FUNCTION_BODY(FPC_EnableFullScreen)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// BOOL WINAPI FPC_IsFullScreenEnabled(HWND hwndFlashPlayerControl)
#define DEF_RETURN_VALUE BOOL
#define DEF_ARGUMENTS (HWND hwndFlashPlayerControl)
#define DEF_ARGUMENT_NAMES (hwndFlashPlayerControl)
DEF_FUNCTION_BODY(FPC_IsFullScreenEnabled)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_QueryInterface(HWND hwndFlashPlayerControl, REFIID iid, void** ppObject)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (HWND hwndFlashPlayerControl, REFIID iid, void** ppObject)
#define DEF_ARGUMENT_NAMES (hwndFlashPlayerControl, iid, ppObject)
DEF_FUNCTION_BODY(FPC_QueryInterface)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HFPC WINAPI FPC_GetHFPC(HWND hwndFlashPlayerControl)
#define DEF_RETURN_VALUE HFPC
#define DEF_ARGUMENTS (HWND hwndFlashPlayerControl)
#define DEF_ARGUMENT_NAMES (hwndFlashPlayerControl)
DEF_FUNCTION_BODY(FPC_GetHFPC)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// void WINAPI FPC_PaintTo(HWND hwndFlashPlayerControl, HDC hDC)
#define DEF_RETURN_VALUE VOID
#define DEF_ARGUMENTS (HWND hwndFlashPlayerControl, HDC hDC)
#define DEF_ARGUMENT_NAMES (hwndFlashPlayerControl, hDC)
DEF_FUNCTION_BODY(FPC_PaintTo)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// BOOL WINAPI FPC_CallWndProc(HWND hwndFlashPlayerControl, UINT message, WPARAM wParam, LPARAM lParam, LRESULT* plResult, BOOL* pbHandled)
#define DEF_RETURN_VALUE BOOL
#define DEF_ARGUMENTS (HWND hwndFlashPlayerControl, UINT message, WPARAM wParam, LPARAM lParam, LRESULT* plResult, BOOL* pbHandled)
#define DEF_ARGUMENT_NAMES (hwndFlashPlayerControl, message, wParam, lParam, plResult, pbHandled)
DEF_FUNCTION_BODY(FPC_CallWndProc)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// BOOL WINAPI FPC_SetFocus(HWND hwndFlashPlayerControl, BOOL bFocus)
#define DEF_RETURN_VALUE BOOL
#define DEF_ARGUMENTS (HWND hwndFlashPlayerControl, BOOL bFocus)
#define DEF_ARGUMENT_NAMES (hwndFlashPlayerControl, bFocus)
DEF_FUNCTION_BODY(FPC_SetFocus)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// BOOL WINAPI FPC_GetFocus(HWND hwndFlashPlayerControl)
#define DEF_RETURN_VALUE BOOL
#define DEF_ARGUMENTS (HWND hwndFlashPlayerControl)
#define DEF_ARGUMENT_NAMES (hwndFlashPlayerControl)
DEF_FUNCTION_BODY(FPC_GetFocus)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// BOOL WINAPI FPC_SetCapture(HWND hwndFlashPlayerControl, BOOL bCapture)
#define DEF_RETURN_VALUE BOOL
#define DEF_ARGUMENTS (HWND hwndFlashPlayerControl, BOOL bCapture)
#define DEF_ARGUMENT_NAMES (hwndFlashPlayerControl, bCapture)
DEF_FUNCTION_BODY(FPC_SetCapture)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// BOOL WINAPI FPC_GetCapture(HWND hwndFlashPlayerControl)
#define DEF_RETURN_VALUE BOOL
#define DEF_ARGUMENTS (HWND hwndFlashPlayerControl)
#define DEF_ARGUMENT_NAMES (hwndFlashPlayerControl)
DEF_FUNCTION_BODY(FPC_GetCapture)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// void WINAPI FPC_SetGlobalOption(DWORD dwOption, DWORD dwValue)
#define DEF_RETURN_VALUE VOID
#define DEF_ARGUMENTS (DWORD dwOption, DWORD dwValue)
#define DEF_ARGUMENT_NAMES (dwOption, dwValue)
DEF_FUNCTION_BODY(FPC_SetGlobalOption)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// DWORD WINAPI FPC_GetGlobalOption(DWORD dwOption)
#define DEF_RETURN_VALUE DWORD
#define DEF_ARGUMENTS (DWORD dwOption)
#define DEF_ARGUMENT_NAMES (dwOption)
DEF_FUNCTION_BODY(FPC_GetGlobalOption)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_SetZoomRect(/* in */ HWND hwndFlashPlayerControl, /* in */ long left, /* in */ long top, /* in */ long right, /* in */ long bottom)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, /* in */ long left, /* in */ long top, /* in */ long right, /* in */ long bottom)
#define DEF_ARGUMENT_NAMES (hwndFlashPlayerControl, left, top, right, bottom)
DEF_FUNCTION_BODY(FPC_SetZoomRect)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_Zoom(/* in */ HWND hwndFlashPlayerControl, /* in */ int factor)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, /* in */ int factor)
#define DEF_ARGUMENT_NAMES (hwndFlashPlayerControl, factor)
DEF_FUNCTION_BODY(FPC_Zoom)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_Pan(/* in */ HWND hwndFlashPlayerControl, /* in */ long x, /* in */ long y, /* in */ int mode)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, /* in */ long x, /* in */ long y, /* in */ int mode)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, /* in */ x, /* in */ y, /* in */ mode)
DEF_FUNCTION_BODY(FPC_Pan)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_Play(/* in */ HWND hwndFlashPlayerControl)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl)
DEF_FUNCTION_BODY(FPC_Play)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_Stop(/* in */ HWND hwndFlashPlayerControl)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl)
DEF_FUNCTION_BODY(FPC_Stop)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_Back(/* in */ HWND hwndFlashPlayerControl)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl)
DEF_FUNCTION_BODY(FPC_Back)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_Forward(/* in */ HWND hwndFlashPlayerControl)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl)
DEF_FUNCTION_BODY(FPC_Forward)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_Rewind(/* in */ HWND hwndFlashPlayerControl)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl)
DEF_FUNCTION_BODY(FPC_Rewind)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_StopPlay(/* in */ HWND hwndFlashPlayerControl)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl)
DEF_FUNCTION_BODY(FPC_StopPlay)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_GotoFrame(/* in */ HWND hwndFlashPlayerControl, /* in */ long FrameNum)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, /* in */ long FrameNum)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, /* in */ FrameNum)
DEF_FUNCTION_BODY(FPC_GotoFrame)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_CurrentFrame(/* in */ HWND hwndFlashPlayerControl, /* out */ long* Result)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, /* out */ long* Result)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, /* out */ Result)
DEF_FUNCTION_BODY(FPC_CurrentFrame)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_IsPlaying(/* in */ HWND hwndFlashPlayerControl, /* out */ VARIANT_BOOL* Result)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, /* out */ VARIANT_BOOL* Result)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, /* out */ Result)
DEF_FUNCTION_BODY(FPC_IsPlaying)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_PercentLoaded(/* in */ HWND hwndFlashPlayerControl, /* out */ long* Result)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, /* out */ long* Result)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, /* out */ Result)
DEF_FUNCTION_BODY(FPC_PercentLoaded)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_FrameLoaded(/* in */ HWND hwndFlashPlayerControl, /* in */ long FrameNum, /* out */ VARIANT_BOOL* Result)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, /* in */ long FrameNum, /* out */ VARIANT_BOOL* Result)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, /* in */ FrameNum, /* out */ Result)
DEF_FUNCTION_BODY(FPC_FrameLoaded)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_FlashVersion(/* in */ HWND hwndFlashPlayerControl, /* out */ long* Result)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, /* out */ long* Result)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, /* out */ Result)
DEF_FUNCTION_BODY(FPC_FlashVersion)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_LoadMovieA(/* in */ HWND hwndFlashPlayerControl, /* in */ int layer, /* in */ LPCSTR url)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, /* in */ int layer, /* in */ LPCSTR url)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, /* in */ layer, /* in */ url)
DEF_FUNCTION_BODY(FPC_LoadMovieA)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_LoadMovieW(/* in */ HWND hwndFlashPlayerControl, /* in */ int layer, /* in */ LPCWSTR url)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, /* in */ int layer, /* in */ LPCWSTR url)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, /* in */ layer, /* in */ url)
DEF_FUNCTION_BODY(FPC_LoadMovieW)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_TGotoFrameA(/* in */ HWND hwndFlashPlayerControl, /* in */ LPCSTR target, /* in */ long FrameNum)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, /* in */ LPCSTR target, /* in */ long FrameNum)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, /* in */ target, /* in */ FrameNum)
DEF_FUNCTION_BODY(FPC_TGotoFrameA)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_TGotoFrameW(/* in */ HWND hwndFlashPlayerControl, /* in */ LPCWSTR target, /* in */ long FrameNum)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, /* in */ LPCWSTR target, /* in */ long FrameNum)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, /* in */ target, /* in */ FrameNum)
DEF_FUNCTION_BODY(FPC_TGotoFrameW)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_TGotoLabelA(/* in */ HWND hwndFlashPlayerControl, /* in */ LPCSTR target, /* in */ LPCSTR label)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, /* in */ LPCSTR target, /* in */ LPCSTR label)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, /* in */ target, /* in */ label)
DEF_FUNCTION_BODY(FPC_TGotoLabelA)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_TGotoLabelW(/* in */ HWND hwndFlashPlayerControl, /* in */ LPCWSTR target, /* in */ LPCWSTR label)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, /* in */ LPCWSTR target, /* in */ LPCWSTR label)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, /* in */ target, /* in */ label)
DEF_FUNCTION_BODY(FPC_TGotoLabelW)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_TCurrentFrameA(/* in */ HWND hwndFlashPlayerControl, /* in */ LPCSTR target, /* out */ long* Result)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, /* in */ LPCSTR target, /* out */ long* Result)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, /* in */ target, /* out */ Result)
DEF_FUNCTION_BODY(FPC_TCurrentFrameA)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_TCurrentFrameW(/* in */ HWND hwndFlashPlayerControl, /* in */ LPCWSTR target, /* out */ long* Result)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, /* in */ LPCWSTR target, /* out */ long* Result)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, /* in */ target, /* out */ Result)
DEF_FUNCTION_BODY(FPC_TCurrentFrameW)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_TCurrentLabelA(/* in */ HWND hwndFlashPlayerControl, /* in */ LPCSTR target, /* out */ LPSTR lpszBuffer, /* in, out */ DWORD* pdwBufferSize)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, /* in */ LPCSTR target, /* out */ LPSTR lpszBuffer, /* in, out */ DWORD* pdwBufferSize)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, /* in */ target, /* out */ lpszBuffer, /* in, out */ pdwBufferSize)
DEF_FUNCTION_BODY(FPC_TCurrentLabelA)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_TCurrentLabelW(/* in */ HWND hwndFlashPlayerControl, /* in */ LPCWSTR target, /* out */ LPWSTR lpszBuffer, /* in, out */ DWORD* pdwBufferSize)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, /* in */ LPCWSTR target, /* out */ LPWSTR lpszBuffer, /* in, out */ DWORD* pdwBufferSize)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, /* in */ target, /* out */ lpszBuffer, /* in, out */ pdwBufferSize)
DEF_FUNCTION_BODY(FPC_TCurrentLabelW)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_TPlayA(/* in */ HWND hwndFlashPlayerControl, /* in */ LPCSTR target)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, /* in */ LPCSTR target)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, /* in */ target)
DEF_FUNCTION_BODY(FPC_TPlayA)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_TPlayW(/* in */ HWND hwndFlashPlayerControl, /* in */ LPCWSTR target)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, /* in */ LPCWSTR target)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, /* in */ target)
DEF_FUNCTION_BODY(FPC_TPlayW)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_TStopPlayA(/* in */ HWND hwndFlashPlayerControl, /* in */ LPCSTR target)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, /* in */ LPCSTR target)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, /* in */ target)
DEF_FUNCTION_BODY(FPC_TStopPlayA)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_TStopPlayW(/* in */ HWND hwndFlashPlayerControl, /* in */ LPCWSTR target)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, /* in */ LPCWSTR target)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, /* in */ target)
DEF_FUNCTION_BODY(FPC_TStopPlayW)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_SetVariableA(/* in */ HWND hwndFlashPlayerControl, /* in */ LPCSTR name, /* in */ LPCSTR value)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, /* in */ LPCSTR name, /* in */ LPCSTR value)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, /* in */ name, /* in */ value)
DEF_FUNCTION_BODY(FPC_SetVariableA)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_SetVariableW(/* in */ HWND hwndFlashPlayerControl, /* in */ LPCWSTR name, /* in */ LPCWSTR value)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, /* in */ LPCWSTR name, /* in */ LPCWSTR value)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, /* in */ name, /* in */ value)
DEF_FUNCTION_BODY(FPC_SetVariableW)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_GetVariableA(/* in */ HWND hwndFlashPlayerControl, /* in */ LPCSTR name, /* out */ LPSTR lpszBuffer, /* in, out */ DWORD* pdwBufferSize)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, /* in */ LPCSTR name, /* out */ LPSTR lpszBuffer, /* in, out */ DWORD* pdwBufferSize)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, /* in */ name, /* out */ lpszBuffer, /* in, out */ pdwBufferSize)
DEF_FUNCTION_BODY(FPC_GetVariableA)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_GetVariableW(/* in */ HWND hwndFlashPlayerControl, /* in */ LPCWSTR name, /* out */ LPWSTR lpszBuffer, /* in, out */ DWORD* pdwBufferSize)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, /* in */ LPCWSTR name, /* out */ LPWSTR lpszBuffer, /* in, out */ DWORD* pdwBufferSize)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, /* in */ name, /* out */ lpszBuffer, /* in, out */ pdwBufferSize)
DEF_FUNCTION_BODY(FPC_GetVariableW)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_TSetPropertyA(/* in */ HWND hwndFlashPlayerControl, /* in */ LPCSTR target, /* in */ int property, /* in */ LPCSTR value)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, /* in */ LPCSTR target, /* in */ int property, /* in */ LPCSTR value)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, /* in */ target, /* in */ property, /* in */ value)
DEF_FUNCTION_BODY(FPC_TSetPropertyA)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_TSetPropertyW(/* in */ HWND hwndFlashPlayerControl, /* in */ LPCWSTR target, /* in */ int property, /* in */ LPCWSTR value)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, /* in */ LPCWSTR target, /* in */ int property, /* in */ LPCWSTR value)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, /* in */ target, /* in */ property, /* in */ value)
DEF_FUNCTION_BODY(FPC_TSetPropertyW)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_TGetPropertyA(/* in */ HWND hwndFlashPlayerControl, /* in */ LPCSTR target, /* in */ int property, /* out */ LPSTR lpszBuffer, /* in, out */ DWORD* pdwBufferSize)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, /* in */ LPCSTR target, /* in */ int property, /* out */ LPSTR lpszBuffer, /* in, out */ DWORD* pdwBufferSize)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, /* in */ target, /* in */ property, /* out */ lpszBuffer, /* in, out */ pdwBufferSize)
DEF_FUNCTION_BODY(FPC_TGetPropertyA)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_TGetPropertyW(/* in */ HWND hwndFlashPlayerControl, /* in */ LPCWSTR target, /* in */ int property, /* out */ LPWSTR lpszBuffer, /* in, out */ DWORD* pdwBufferSize)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, /* in */ LPCWSTR target, /* in */ int property, /* out */ LPWSTR lpszBuffer, /* in, out */ DWORD* pdwBufferSize)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, /* in */ target, /* in */ property, /* out */ lpszBuffer, /* in, out */ pdwBufferSize)
DEF_FUNCTION_BODY(FPC_TGetPropertyW)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_TCallFrameA(/* in */ HWND hwndFlashPlayerControl, /* in */ LPCSTR target, /* in */ int FrameNum)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, /* in */ LPCSTR target, /* in */ int FrameNum)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, /* in */ target, /* in */ FrameNum)
DEF_FUNCTION_BODY(FPC_TCallFrameA)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_TCallFrameW(/* in */ HWND hwndFlashPlayerControl, /* in */ LPCWSTR target, /* in */ int FrameNum)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, /* in */ LPCWSTR target, /* in */ int FrameNum)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, /* in */ target, /* in */ FrameNum)
DEF_FUNCTION_BODY(FPC_TCallFrameW)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_TCallLabelA(/* in */ HWND hwndFlashPlayerControl, /* in */ LPCSTR target, /* in */ LPCSTR label)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, /* in */ LPCSTR target, /* in */ LPCSTR label)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, /* in */ target, /* in */ label)
DEF_FUNCTION_BODY(FPC_TCallLabelA)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_TCallLabelW(/* in */ HWND hwndFlashPlayerControl, /* in */ LPCWSTR target, /* in */ LPCWSTR label)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, /* in */ LPCWSTR target, /* in */ LPCWSTR label)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, /* in */ target, /* in */ label)
DEF_FUNCTION_BODY(FPC_TCallLabelW)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_TSetPropertyNumA(/* in */ HWND hwndFlashPlayerControl, /* in */ LPCSTR target, /* in */ int property, /* in */ double value)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, /* in */ LPCSTR target, /* in */ int property, /* in */ double value)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, /* in */ target, /* in */ property, /* in */ value)
DEF_FUNCTION_BODY(FPC_TSetPropertyNumA)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_TSetPropertyNumW(/* in */ HWND hwndFlashPlayerControl, /* in */ LPCWSTR target, /* in */ int property, /* in */ double value)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, /* in */ LPCWSTR target, /* in */ int property, /* in */ double value)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, /* in */ target, /* in */ property, /* in */ value)
DEF_FUNCTION_BODY(FPC_TSetPropertyNumW)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_TGetPropertyNumA(/* in */ HWND hwndFlashPlayerControl, /* in */ LPCSTR target, /* in */ int property, /* out */ double* Result)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, /* in */ LPCSTR target, /* in */ int property, /* out */ double* Result)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, /* in */ target, /* in */ property, /* out */ Result)
DEF_FUNCTION_BODY(FPC_TGetPropertyNumA)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_TGetPropertyNumW(/* in */ HWND hwndFlashPlayerControl, /* in */ LPCWSTR target, /* in */ int property, /* out */ double* Result)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, /* in */ LPCWSTR target, /* in */ int property, /* out */ double* Result)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, /* in */ target, /* in */ property, /* out */ Result)
DEF_FUNCTION_BODY(FPC_TGetPropertyNumW)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_TGetPropertyAsNumberA(/* in */ HWND hwndFlashPlayerControl, /* in */ LPCSTR target, /* in */ int property, /* out */ double* Result)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, /* in */ LPCSTR target, /* in */ int property, /* out */ double* Result)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, /* in */ target, /* in */ property, /* out */ Result)
DEF_FUNCTION_BODY(FPC_TGetPropertyAsNumberA)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_TGetPropertyAsNumberW(/* in */ HWND hwndFlashPlayerControl, /* in */ LPCWSTR target, /* in */ int property, /* out */ double* Result)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, /* in */ LPCWSTR target, /* in */ int property, /* out */ double* Result)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, /* in */ target, /* in */ property, /* out */ Result)
DEF_FUNCTION_BODY(FPC_TGetPropertyAsNumberW)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_GetReadyState(/* in */ HWND hwndFlashPlayerControl, long* pReadyState)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, long* pReadyState)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, pReadyState)
DEF_FUNCTION_BODY(FPC_GetReadyState)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_GetTotalFrames(/* in */ HWND hwndFlashPlayerControl, long* pTotalFrames)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, long* pTotalFrames)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, pTotalFrames)
DEF_FUNCTION_BODY(FPC_GetTotalFrames)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_PutPlaying(/* in */ HWND hwndFlashPlayerControl, VARIANT_BOOL Playing)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, VARIANT_BOOL Playing)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, Playing)
DEF_FUNCTION_BODY(FPC_PutPlaying)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_GetPlaying(/* in */ HWND hwndFlashPlayerControl, VARIANT_BOOL* pPlaying)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, VARIANT_BOOL* pPlaying)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, pPlaying)
DEF_FUNCTION_BODY(FPC_GetPlaying)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_PutQuality(/* in */ HWND hwndFlashPlayerControl, int Quality)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, int Quality)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, Quality)
DEF_FUNCTION_BODY(FPC_PutQuality)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_GetQuality(/* in */ HWND hwndFlashPlayerControl, int* pQuality)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, int* pQuality)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, pQuality)
DEF_FUNCTION_BODY(FPC_GetQuality)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_PutScaleMode(/* in */ HWND hwndFlashPlayerControl, int ScaleMode)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, int ScaleMode)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, ScaleMode)
DEF_FUNCTION_BODY(FPC_PutScaleMode)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_GetScaleMode(/* in */ HWND hwndFlashPlayerControl, int* pScaleMode)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, int* pScaleMode)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, pScaleMode)
DEF_FUNCTION_BODY(FPC_GetScaleMode)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_PutAlignMode(/* in */ HWND hwndFlashPlayerControl, int AlignMode)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, int AlignMode)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, AlignMode)
DEF_FUNCTION_BODY(FPC_PutAlignMode)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_GetAlignMode(/* in */ HWND hwndFlashPlayerControl, int* pAlignMode)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, int* pAlignMode)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, pAlignMode)
DEF_FUNCTION_BODY(FPC_GetAlignMode)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_PutBackgroundColor(/* in */ HWND hwndFlashPlayerControl, long BackgroundColor)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, long BackgroundColor)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, BackgroundColor)
DEF_FUNCTION_BODY(FPC_PutBackgroundColor)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_GetBackgroundColor(/* in */ HWND hwndFlashPlayerControl, long* pBackgroundColor)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, long* pBackgroundColor)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, pBackgroundColor)
DEF_FUNCTION_BODY(FPC_GetBackgroundColor)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_PutLoop(/* in */ HWND hwndFlashPlayerControl, VARIANT_BOOL Loop)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, VARIANT_BOOL Loop)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, Loop)
DEF_FUNCTION_BODY(FPC_PutLoop)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_GetLoop(/* in */ HWND hwndFlashPlayerControl, VARIANT_BOOL* pLoop)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, VARIANT_BOOL* pLoop)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, pLoop)
DEF_FUNCTION_BODY(FPC_GetLoop)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_PutMovieA(/* in */ HWND hwndFlashPlayerControl, LPCSTR Movie)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, LPCSTR Movie)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, Movie)
DEF_FUNCTION_BODY(FPC_PutMovieA)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_PutMovieW(/* in */ HWND hwndFlashPlayerControl, LPCWSTR Movie)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, LPCWSTR Movie)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, Movie)
DEF_FUNCTION_BODY(FPC_PutMovieW)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_GetMovieA(/* in */ HWND hwndFlashPlayerControl, LPSTR lpszBuffer, DWORD* pdwBufferSize)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, LPSTR lpszBuffer, DWORD* pdwBufferSize)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, lpszBuffer, pdwBufferSize)
DEF_FUNCTION_BODY(FPC_GetMovieA)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_GetMovieW(/* in */ HWND hwndFlashPlayerControl, LPWSTR lpszBuffer, DWORD* pdwBufferSize)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, LPWSTR lpszBuffer, DWORD* pdwBufferSize)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, lpszBuffer, pdwBufferSize)
DEF_FUNCTION_BODY(FPC_GetMovieA)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_PutFrameNum(/* in */ HWND hwndFlashPlayerControl, long FrameNum)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, long FrameNum)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, FrameNum)
DEF_FUNCTION_BODY(FPC_PutFrameNum)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_GetFrameNum(/* in */ HWND hwndFlashPlayerControl, long* pFrameNum)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, long* pFrameNum)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, pFrameNum)
DEF_FUNCTION_BODY(FPC_GetFrameNum)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_PutWModeA(/* in */ HWND hwndFlashPlayerControl, LPCSTR WMode)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, LPCSTR WMode)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, WMode)
DEF_FUNCTION_BODY(FPC_PutWModeA)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_PutWModeW(/* in */ HWND hwndFlashPlayerControl, LPCWSTR WMode)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, LPCWSTR WMode)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, WMode)
DEF_FUNCTION_BODY(FPC_PutWModeW)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_GetWModeA(/* in */ HWND hwndFlashPlayerControl, LPSTR lpszBuffer, DWORD* pdwBufferSize)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, LPSTR lpszBuffer, DWORD* pdwBufferSize)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, lpszBuffer, pdwBufferSize)
DEF_FUNCTION_BODY(FPC_GetWModeA)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_GetWModeW(/* in */ HWND hwndFlashPlayerControl, LPWSTR lpszBuffer, DWORD* pdwBufferSize)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, LPWSTR lpszBuffer, DWORD* pdwBufferSize)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, lpszBuffer, pdwBufferSize)
DEF_FUNCTION_BODY(FPC_GetWModeW)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_PutSAlignA(/* in */ HWND hwndFlashPlayerControl, LPCSTR SAlign)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, LPCSTR SAlign)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, SAlign)
DEF_FUNCTION_BODY(FPC_PutSAlignA)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_PutSAlignW(/* in */ HWND hwndFlashPlayerControl, LPCWSTR SAlign)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, LPCWSTR SAlign)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, SAlign)
DEF_FUNCTION_BODY(FPC_PutSAlignW)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_GetSAlignA(/* in */ HWND hwndFlashPlayerControl, LPSTR lpszBuffer, DWORD* pdwBufferSize)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, LPSTR lpszBuffer, DWORD* pdwBufferSize)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, lpszBuffer, pdwBufferSize)
DEF_FUNCTION_BODY(FPC_GetSAlignA)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_GetSAlignW(/* in */ HWND hwndFlashPlayerControl, LPWSTR lpszBuffer, DWORD* pdwBufferSize)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, LPWSTR lpszBuffer, DWORD* pdwBufferSize)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, lpszBuffer, pdwBufferSize)
DEF_FUNCTION_BODY(FPC_GetSAlignW)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_PutMenu(/* in */ HWND hwndFlashPlayerControl, VARIANT_BOOL Menu)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, VARIANT_BOOL Menu)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, Menu)
DEF_FUNCTION_BODY(FPC_PutMenu)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_GetMenu(/* in */ HWND hwndFlashPlayerControl, VARIANT_BOOL* pMenu)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, VARIANT_BOOL* pMenu)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, pMenu)
DEF_FUNCTION_BODY(FPC_GetMenu)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_PutBaseA(/* in */ HWND hwndFlashPlayerControl, LPCSTR Base)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, LPCSTR Base)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, Base)
DEF_FUNCTION_BODY(FPC_PutBaseA)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_PutBaseW(/* in */ HWND hwndFlashPlayerControl, LPCWSTR Base)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, LPCWSTR Base)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, Base)
DEF_FUNCTION_BODY(FPC_PutBaseW)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_GetBaseA(/* in */ HWND hwndFlashPlayerControl, LPSTR lpszBuffer, DWORD* pdwBufferSize)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, LPSTR lpszBuffer, DWORD* pdwBufferSize)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, lpszBuffer, pdwBufferSize)
DEF_FUNCTION_BODY(FPC_GetBaseA)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_GetBaseW(/* in */ HWND hwndFlashPlayerControl, LPWSTR lpszBuffer, DWORD* pdwBufferSize)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, LPWSTR lpszBuffer, DWORD* pdwBufferSize)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, lpszBuffer, pdwBufferSize)
DEF_FUNCTION_BODY(FPC_GetBaseW)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_PutScaleA(/* in */ HWND hwndFlashPlayerControl, LPCSTR Scale)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, LPCSTR Scale)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, Scale)
DEF_FUNCTION_BODY(FPC_PutScaleA)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_PutScaleW(/* in */ HWND hwndFlashPlayerControl, LPCWSTR Scale)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, LPCWSTR Scale)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, Scale)
DEF_FUNCTION_BODY(FPC_PutScaleW)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_GetScaleA(/* in */ HWND hwndFlashPlayerControl, LPSTR lpszBuffer, DWORD* pdwBufferSize)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, LPSTR lpszBuffer, DWORD* pdwBufferSize)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, lpszBuffer, pdwBufferSize)
DEF_FUNCTION_BODY(FPC_GetScaleA)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_GetScaleW(/* in */ HWND hwndFlashPlayerControl, LPWSTR lpszBuffer, DWORD* pdwBufferSize)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, LPWSTR lpszBuffer, DWORD* pdwBufferSize)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, lpszBuffer, pdwBufferSize)
DEF_FUNCTION_BODY(FPC_GetScaleW)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_PutDeviceFont(/* in */ HWND hwndFlashPlayerControl, VARIANT_BOOL DeviceFont)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, VARIANT_BOOL DeviceFont)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, DeviceFont)
DEF_FUNCTION_BODY(FPC_PutDeviceFont)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_GetDeviceFont(/* in */ HWND hwndFlashPlayerControl, VARIANT_BOOL* pDeviceFont)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, VARIANT_BOOL* pDeviceFont)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, pDeviceFont)
DEF_FUNCTION_BODY(FPC_GetDeviceFont)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_PutEmbedMovie(/* in */ HWND hwndFlashPlayerControl, VARIANT_BOOL EmbedMovie)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, VARIANT_BOOL EmbedMovie)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, EmbedMovie)
DEF_FUNCTION_BODY(FPC_PutEmbedMovie)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_GetEmbedMovie(/* in */ HWND hwndFlashPlayerControl, VARIANT_BOOL* pEmbedMovie)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, VARIANT_BOOL* pEmbedMovie)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, pEmbedMovie)
DEF_FUNCTION_BODY(FPC_GetEmbedMovie)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_PutBGColorA(/* in */ HWND hwndFlashPlayerControl, LPCSTR BGColor)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, LPCSTR BGColor)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, BGColor)
DEF_FUNCTION_BODY(FPC_PutBGColorA)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_PutBGColorW(/* in */ HWND hwndFlashPlayerControl, LPCWSTR BGColor)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, LPCWSTR BGColor)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, BGColor)
DEF_FUNCTION_BODY(FPC_PutBGColorW)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_GetBGColorA(/* in */ HWND hwndFlashPlayerControl, LPSTR lpszBuffer, DWORD* pdwBufferSize)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, LPSTR lpszBuffer, DWORD* pdwBufferSize)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, lpszBuffer, pdwBufferSize)
DEF_FUNCTION_BODY(FPC_GetBGColorA)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_GetBGColorW(/* in */ HWND hwndFlashPlayerControl, LPWSTR lpszBuffer, DWORD* pdwBufferSize)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, LPWSTR lpszBuffer, DWORD* pdwBufferSize)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, lpszBuffer, pdwBufferSize)
DEF_FUNCTION_BODY(FPC_GetBGColorW)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_PutQuality2A(/* in */ HWND hwndFlashPlayerControl, LPCSTR Quality2)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, LPCSTR Quality2)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, Quality2)
DEF_FUNCTION_BODY(FPC_PutQuality2A)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_PutQuality2W(/* in */ HWND hwndFlashPlayerControl, LPCWSTR Quality2)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, LPCWSTR Quality2)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, Quality2)
DEF_FUNCTION_BODY(FPC_PutQuality2W)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_GetQuality2A(/* in */ HWND hwndFlashPlayerControl, LPSTR lpszBuffer, DWORD* pdwBufferSize)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, LPSTR lpszBuffer, DWORD* pdwBufferSize)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, lpszBuffer, pdwBufferSize)
DEF_FUNCTION_BODY(FPC_GetQuality2A)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_GetQuality2W(/* in */ HWND hwndFlashPlayerControl, LPWSTR lpszBuffer, DWORD* pdwBufferSize)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, LPWSTR lpszBuffer, DWORD* pdwBufferSize)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, lpszBuffer, pdwBufferSize)
DEF_FUNCTION_BODY(FPC_GetQuality2W)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_PutSWRemoteA(/* in */ HWND hwndFlashPlayerControl, LPCSTR SWRemote)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, LPCSTR SWRemote)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, SWRemote)
DEF_FUNCTION_BODY(FPC_PutSWRemoteA)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_PutSWRemoteW(/* in */ HWND hwndFlashPlayerControl, LPCWSTR SWRemote)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, LPCWSTR SWRemote)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, SWRemote)
DEF_FUNCTION_BODY(FPC_PutSWRemoteW)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_GetSWRemoteA(/* in */ HWND hwndFlashPlayerControl, LPSTR lpszBuffer, DWORD* pdwBufferSize)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, LPSTR lpszBuffer, DWORD* pdwBufferSize)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, lpszBuffer, pdwBufferSize)
DEF_FUNCTION_BODY(FPC_GetSWRemoteA)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_GetSWRemoteW(/* in */ HWND hwndFlashPlayerControl, LPWSTR lpszBuffer, DWORD* pdwBufferSize)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, LPWSTR lpszBuffer, DWORD* pdwBufferSize)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, lpszBuffer, pdwBufferSize)
DEF_FUNCTION_BODY(FPC_GetSWRemoteW)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_PutStackingA(/* in */ HWND hwndFlashPlayerControl, LPCSTR Stacking)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, LPCSTR Stacking)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, Stacking)
DEF_FUNCTION_BODY(FPC_PutStackingA)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_PutStackingW(/* in */ HWND hwndFlashPlayerControl, LPCWSTR Stacking)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, LPCWSTR Stacking)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, Stacking)
DEF_FUNCTION_BODY(FPC_PutStackingW)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_GetStackingA(/* in */ HWND hwndFlashPlayerControl, LPSTR lpszBuffer, DWORD* pdwBufferSize)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, LPSTR lpszBuffer, DWORD* pdwBufferSize)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, lpszBuffer, pdwBufferSize)
DEF_FUNCTION_BODY(FPC_GetStackingA)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_GetStackingW(/* in */ HWND hwndFlashPlayerControl, LPWSTR lpszBuffer, DWORD* pdwBufferSize)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, LPWSTR lpszBuffer, DWORD* pdwBufferSize)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, lpszBuffer, pdwBufferSize)
DEF_FUNCTION_BODY(FPC_GetStackingW)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_PutFlashVarsA(/* in */ HWND hwndFlashPlayerControl, LPCSTR FlashVars)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, LPCSTR FlashVars)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, FlashVars)
DEF_FUNCTION_BODY(FPC_PutFlashVarsA)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_PutFlashVarsW(/* in */ HWND hwndFlashPlayerControl, LPCWSTR FlashVars)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, LPCWSTR FlashVars)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, FlashVars)
DEF_FUNCTION_BODY(FPC_PutFlashVarsW)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_GetFlashVarsA(/* in */ HWND hwndFlashPlayerControl, LPSTR lpszBuffer, DWORD* pdwBufferSize)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, LPSTR lpszBuffer, DWORD* pdwBufferSize)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, lpszBuffer, pdwBufferSize)
DEF_FUNCTION_BODY(FPC_GetFlashVarsA)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_GetFlashVarsW(/* in */ HWND hwndFlashPlayerControl, LPWSTR lpszBuffer, DWORD* pdwBufferSize)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, LPWSTR lpszBuffer, DWORD* pdwBufferSize)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, lpszBuffer, pdwBufferSize)
DEF_FUNCTION_BODY(FPC_GetFlashVarsW)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_PutAllowScriptAccessA(/* in */ HWND hwndFlashPlayerControl, LPCSTR AllowScriptAccess)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, LPCSTR AllowScriptAccess)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, AllowScriptAccess)
DEF_FUNCTION_BODY(FPC_PutAllowScriptAccessA)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_PutAllowScriptAccessW(/* in */ HWND hwndFlashPlayerControl, LPCWSTR AllowScriptAccess)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, LPCWSTR AllowScriptAccess)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, AllowScriptAccess)
DEF_FUNCTION_BODY(FPC_PutAllowScriptAccessW)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_GetAllowScriptAccessA(/* in */ HWND hwndFlashPlayerControl, LPSTR lpszBuffer, DWORD* pdwBufferSize)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, LPSTR lpszBuffer, DWORD* pdwBufferSize)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, lpszBuffer, pdwBufferSize)
DEF_FUNCTION_BODY(FPC_GetAllowScriptAccessA)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_GetAllowScriptAccessW(/* in */ HWND hwndFlashPlayerControl, LPWSTR lpszBuffer, DWORD* pdwBufferSize)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, LPWSTR lpszBuffer, DWORD* pdwBufferSize)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, lpszBuffer, pdwBufferSize)
DEF_FUNCTION_BODY(FPC_GetAllowScriptAccessW)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_PutMovieDataA(/* in */ HWND hwndFlashPlayerControl, LPCSTR MovieData)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, LPCSTR MovieData)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, MovieData)
DEF_FUNCTION_BODY(FPC_PutMovieDataA)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_PutMovieDataW(/* in */ HWND hwndFlashPlayerControl, LPCWSTR MovieData)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, LPCWSTR MovieData)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, MovieData)
DEF_FUNCTION_BODY(FPC_PutMovieDataW)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_GetMovieDataA(/* in */ HWND hwndFlashPlayerControl, LPSTR lpszBuffer, DWORD* pdwBufferSize)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, LPSTR lpszBuffer, DWORD* pdwBufferSize)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, lpszBuffer, pdwBufferSize)
DEF_FUNCTION_BODY(FPC_GetMovieDataA)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES

// HRESULT WINAPI FPC_GetMovieDataW(/* in */ HWND hwndFlashPlayerControl, LPWSTR lpszBuffer, DWORD* pdwBufferSize)
#define DEF_RETURN_VALUE HRESULT
#define DEF_ARGUMENTS (/* in */ HWND hwndFlashPlayerControl, LPWSTR lpszBuffer, DWORD* pdwBufferSize)
#define DEF_ARGUMENT_NAMES (/* in */ hwndFlashPlayerControl, lpszBuffer, pdwBufferSize)
DEF_FUNCTION_BODY(FPC_GetMovieDataW)
#undef DEF_RETURN_VALUE
#undef DEF_ARGUMENTS
#undef DEF_ARGUMENT_NAMES
