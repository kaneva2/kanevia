#ifndef __MY_MONIKER_H__845567B9_B9B7_46d3_93E3_18DE42D1A21E__
#define __MY_MONIKER_H__845567B9_B9B7_46d3_93E3_18DE42D1A21E__

#include "ContentProvider.h"
#include "ContentManager.h"
#include "String.h"
#include "ComPtr.h"
#include "UrlMonHelper.h"

namespace f_in_box
{

class CMyMoniker : public IMoniker
{
    // Variables
    // Private Variables
private:

    ULONG m_nRefCount;

    f_in_box::UrlMonHelper::SUrlMonHelper m_UrlMonHelper;

    // URL
    f_in_box::std::CString<WCHAR> m_strURL;

    f_in_box::com_helpers::CComPtr<IMoniker> m_pmkContext;

	f_in_box::com_helpers::CComPtr<IMoniker> m_pStandardURLMoniker;

    //
    CContentManager* m_pContentManager;

public:
	static HRESULT Create(LPCWSTR lpszURL, CContentManager* pContentManager, IMoniker* pmkContext, REFIID refiid, LPVOID* ppObject);

private:
    CMyMoniker();
    ~CMyMoniker();

	HRESULT Init(CContentManager* pContentManager, LPCWSTR lpszURL, IMoniker* pmkContext);

private:
    // IUnknown
    virtual HRESULT STDMETHODCALLTYPE QueryInterface(REFIID riid, void** ppvObject);
    virtual ULONG STDMETHODCALLTYPE AddRef();
    virtual ULONG STDMETHODCALLTYPE Release();

    // IPersist
    STDMETHOD(GetClassID)(CLSID *pClassID); 

    // IPersistStream
    STDMETHOD(IsDirty)(void);
    STDMETHOD(Load)(IStream *pStm);
    STDMETHOD(Save)(IStream *pStm, BOOL fClearDirty); 
    STDMETHOD(GetSizeMax)(ULARGE_INTEGER *pcbSize); 

    // IMoniker
    STDMETHOD(BindToObject)(IBindCtx *pbc, IMoniker *pmkToLeft, REFIID riidResult, void **ppvResult); 
    STDMETHOD(BindToStorage)(IBindCtx *pbc, IMoniker *pmkToLeft, REFIID riid, void **ppvObj);
    STDMETHOD(Reduce)(IBindCtx *pbc, DWORD dwReduceHowFar, IMoniker **ppmkToLeft, IMoniker **ppmkReduced); 
    STDMETHOD(ComposeWith)(IMoniker *pmkRight, BOOL fOnlyIfNotGeneric, IMoniker **ppmkComposite);
    STDMETHOD(Enum)(BOOL fForward, IEnumMoniker **ppenumMoniker);
    STDMETHOD(IsEqual)(IMoniker *pmkOtherMoniker);
    STDMETHOD(Hash)(DWORD *pdwHash);
    STDMETHOD(IsRunning)(IBindCtx *pbc, IMoniker *pmkToLeft, IMoniker *pmkNewlyRunning);
    STDMETHOD(GetTimeOfLastChange)(IBindCtx *pbc, IMoniker *pmkToLeft, FILETIME *pFileTime);
    STDMETHOD(Inverse)(IMoniker **ppmk);
    STDMETHOD(CommonPrefixWith)(IMoniker *pmkOther, IMoniker **ppmkPrefix);
    STDMETHOD(RelativePathTo)(IMoniker *pmkOther, IMoniker **ppmkRelPath);
    STDMETHOD(GetDisplayName)(IBindCtx *pbc, IMoniker *pmkToLeft, LPOLESTR *ppszDisplayName);
    STDMETHOD(ParseDisplayName)(IBindCtx *pbc, IMoniker *pmkToLeft, LPOLESTR pszDisplayName, ULONG *pchEaten, IMoniker **ppmkOut);
    STDMETHOD(IsSystemMoniker)(DWORD *pdwMksys);
};

}

#endif // !__MY_MONIKER_H__845567B9_B9B7_46d3_93E3_18DE42D1A21E__
