#include "stdafx.h"
#include "thunks.h"
#include "Globals.h"

namespace f_in_box
{
    PVOID AllocThunkWithOneParam(PVOID pTargetFunction, PVOID param)
    {
        return AllocThunkWithTwoParams(pTargetFunction, param, NULL);
    }

    PVOID AllocThunkWithTwoParams(PVOID pTargetFunction, PVOID param1, PVOID param2)
    {
        // TODO: hard coded size?
        // TODO: take memory from a pool?
        PBYTE pThunk = (PBYTE)VirtualAlloc(NULL, 1024, MEM_COMMIT, PAGE_EXECUTE_READWRITE);

        // Current instruction offset
        int nInstructionPointer = 0;

        const DWORD slots[] = { Globals::g_nTlsSlotParam1, Globals::g_nTlsSlotParam2 };
        const PVOID params[] = { param1, param2 };

#ifdef _WIN64

        // 4 - space that should be provided by a caller function to allow called function stores argument there
        // 2 - space for saved registers rcx and rdx
        BYTE nReservedStackSpace = (4 + 2) * sizeof(DWORD_PTR);
        nReservedStackSpace = (nReservedStackSpace + STACK_ALIGN - 1) & ~(STACK_ALIGN - 1);
        nReservedStackSpace += sizeof(DWORD_PTR); // As return address is pushed to the stack it's required to add 8 to make stack aligned well

        // sub rsp, XX (48 83 EC XX)
        pThunk[nInstructionPointer++] = 0x48;
        pThunk[nInstructionPointer++] = 0x83;
        pThunk[nInstructionPointer++] = 0xec;
        pThunk[nInstructionPointer++] = nReservedStackSpace;

        // mov qword ptr [rsp+XX], rcx (48 89 4C 24 XX)
        pThunk[nInstructionPointer++] = 0x48;
        pThunk[nInstructionPointer++] = 0x89;
        pThunk[nInstructionPointer++] = 0x4c;
        pThunk[nInstructionPointer++] = 0x24;
        // First four entries are for argument space allocated due to x64 calling convention
        pThunk[nInstructionPointer++] = 4 * sizeof(DWORD_PTR);

        // mov qword ptr [rsp+XX], rdx (48 89 54 24 XX)
        pThunk[nInstructionPointer++] = 0x48;
        pThunk[nInstructionPointer++] = 0x89;
        pThunk[nInstructionPointer++] = 0x54;
        pThunk[nInstructionPointer++] = 0x24;
        pThunk[nInstructionPointer++] = 5 * sizeof(DWORD_PTR);

        for (int nSlotIndex = 0; nSlotIndex < DEF_ARRAY_SIZE(slots); nSlotIndex++)
        {
            // First argument for TlsSetValue
            // mov rcx, XXXXXXXXXXXXXX (48 B9 XX XX XX XX XX XX XX)
            pThunk[nInstructionPointer++] = 0x48;
            pThunk[nInstructionPointer++] = 0xb9;
            *(DWORD_PTR*)&pThunk[nInstructionPointer] = slots[nSlotIndex];
            nInstructionPointer += sizeof(DWORD_PTR);

            // Second argument for TlsSetValue
            // mov rdx, XXXXXXXXXXXXXX (48 BA XX XX XX XX XX XX XX)
            pThunk[nInstructionPointer++] = 0x48;
            pThunk[nInstructionPointer++] = 0xba;
            *(DWORD_PTR*)&pThunk[nInstructionPointer] = (DWORD_PTR)params[nSlotIndex];
            nInstructionPointer += sizeof(DWORD_PTR);

            // Call TlsSetValue
            // mov rax, XXXXXXXXXXXXXX (48 B8 XX XX XX XX XX XX XX)
            // call rax (FF D0)
            pThunk[nInstructionPointer++] = 0x48;
            pThunk[nInstructionPointer++] = 0xb8;
            *(DWORD_PTR*)&pThunk[nInstructionPointer] = (DWORD_PTR)Globals::g_pTlsSetValue;
            nInstructionPointer += sizeof(DWORD_PTR);
            pThunk[nInstructionPointer++] = 0xff;
            pThunk[nInstructionPointer++] = 0xd0;
        }

        // mov rcx, qword ptr [rsp+XX] (48 8B 4C 24 XX)
        pThunk[nInstructionPointer++] = 0x48;
        pThunk[nInstructionPointer++] = 0x8b;
        pThunk[nInstructionPointer++] = 0x4c;
        pThunk[nInstructionPointer++] = 0x24;
        pThunk[nInstructionPointer++] = 4 * sizeof(DWORD_PTR);

        // mov rdx, qword ptr [rsp+XX] (48 8B 54 24 XX)
        pThunk[nInstructionPointer++] = 0x48;
        pThunk[nInstructionPointer++] = 0x8b;
        pThunk[nInstructionPointer++] = 0x54;
        pThunk[nInstructionPointer++] = 0x24;
        pThunk[nInstructionPointer++] = 5 * sizeof(DWORD_PTR);

        // add rsp, XX (48 83 C4 XX)
        pThunk[nInstructionPointer++] = 0x48;
        pThunk[nInstructionPointer++] = 0x83;
        pThunk[nInstructionPointer++] = 0xc4;
        pThunk[nInstructionPointer++] = nReservedStackSpace;

        // push XXXXXXXX (68 XX XX XX XX) 
        // mov dword ptr [rsp+4], XXXXXXXX (C7 44 24 04 XX XX XX XX)
        // ret (C3)
        pThunk[nInstructionPointer++] = 0x68;
        *(DWORD*)&pThunk[nInstructionPointer] = (DWORD)(DWORD_PTR)pTargetFunction;
        nInstructionPointer += sizeof(DWORD);
        pThunk[nInstructionPointer++] = 0xc7;
        pThunk[nInstructionPointer++] = 0x44;
        pThunk[nInstructionPointer++] = 0x24;
        pThunk[nInstructionPointer++] = 0x04;
        *(DWORD*)&pThunk[nInstructionPointer] = (DWORD)((DWORD_PTR)pTargetFunction >> 32);
        nInstructionPointer += sizeof(DWORD);
        pThunk[nInstructionPointer++] = 0xc3;

#else

        for (int nSlotIndex = 0; nSlotIndex < DEF_ARRAY_SIZE(slots); nSlotIndex++)
        {
            // push XXXXXXXX (68 XX XX XX XX)
            pThunk[nInstructionPointer++] = 0x68;
            *(DWORD*)&pThunk[nInstructionPointer] = (DWORD)params[nSlotIndex];
            nInstructionPointer += sizeof(DWORD);

            // push XXXXXXXX (68 XX XX XX XX)
            pThunk[nInstructionPointer++] = 0x68;
            *(DWORD*)&pThunk[nInstructionPointer] = slots[nSlotIndex];
            nInstructionPointer += sizeof(DWORD);

            // mov eax, XXXXXXXX (B8 XX XX XX XX)
            // call eax (FF D0)
            pThunk[nInstructionPointer++] = 0xb8;
            *(DWORD*)&pThunk[nInstructionPointer] = (DWORD)(DWORD_PTR)Globals::g_pTlsSetValue;
            nInstructionPointer += sizeof(DWORD);
            pThunk[nInstructionPointer++] = 0xff;
            pThunk[nInstructionPointer++] = 0xd0;
        }

        // push XXXXXXXX (68 XX XX XX XX)
        // ret (C3)
        pThunk[nInstructionPointer++] = 0x68;
        *(DWORD*)&pThunk[nInstructionPointer] = (DWORD)(DWORD_PTR)pTargetFunction;
        nInstructionPointer += sizeof(DWORD);
        pThunk[nInstructionPointer++] = 0xc3;

#endif // _WIN64

        // TODO: hard coded size?
        FlushInstructionCache(GetCurrentProcess(), pThunk, 1024);

        return pThunk;
    }
}
