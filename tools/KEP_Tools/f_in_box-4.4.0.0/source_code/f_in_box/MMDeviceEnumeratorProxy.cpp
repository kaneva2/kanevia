#include "stdafx.h"
#include "FlashOCXLoader.h"
#include "mmdeviceapi.h"
#include "MMDeviceEnumeratorProxy.h"
#include "ObjectWithCounter.h"
#include "ComPtr.h"
#include "MMDeviceProxy.h"

using namespace f_in_box::com_helpers;
using namespace f_in_box::std;

namespace f_in_box
{
	/// This is a proxy of IMMDeviceEnumerator that passes all calls to underlying
	/// object, but additionally returns IMMDevice proxy from GetDevice to allow then
	/// get access to IMMDevice::Activate() that returns IDirectSound8. So we could
	/// control audio output.
	class CMMDeviceEnumeratorProxy : 
		public CObjectWithCounter, 
		public IMMDeviceEnumerator
	{
	private:
		/// Object that should receive all calls
		CComPtr<IMMDeviceEnumerator> m_pUnderlyingObject;

		/// Main object
		com_helpers::CComPtr<CFlashOCXLoader> m_pLoader;

	public:
		explicit CMMDeviceEnumeratorProxy(CFlashOCXLoader* pLoader, IMMDeviceEnumerator* pUnderlyingObject) : 
			m_pLoader(pLoader), 
			m_pUnderlyingObject(pUnderlyingObject)
		{
		}

        virtual ULONG __stdcall AddRef()
        {
			return CObjectWithCounter::AddRef();
        }

        virtual ULONG __stdcall Release()
        {
			return CObjectWithCounter::Release();
        }

		HRESULT STDMETHODCALLTYPE QueryInterface(REFIID riid, void** ppvObject)
		{
			if (f_in_box::com_helpers::IsEqualGUID(riid, IID_IUnknown))
				*ppvObject = this;
			else if (f_in_box::com_helpers::IsEqualGUID(riid, IID_IMMDeviceEnumerator))
				*ppvObject = static_cast<IMMDeviceEnumerator*>(this);
			else
				return m_pUnderlyingObject->QueryInterface(riid, ppvObject);

			AddRef();

			return S_OK;
		}

		HRESULT STDMETHODCALLTYPE EnumAudioEndpoints(EDataFlow dataFlow, DWORD dwStateMask, IMMDeviceCollection** ppDevices)
		{
			return m_pUnderlyingObject->EnumAudioEndpoints(dataFlow, dwStateMask, ppDevices);
		}

		HRESULT STDMETHODCALLTYPE GetDefaultAudioEndpoint(EDataFlow dataFlow, ERole role, IMMDevice** ppEndpoint)
		{
			if (eRender == dataFlow && eMultimedia == role)
				// We need a render device (because probably Flash may request a microphone device, we don't 
				// need to handle such one)
			{
				CComPtr<IMMDevice> pOriginalDevice;
				HRESULT hr = m_pUnderlyingObject->GetDefaultAudioEndpoint(dataFlow, role, &pOriginalDevice);
				if (S_OK != hr)
				{
					// Set *ppEndpoint to NULL on error as stated in the documentation
					*ppEndpoint = NULL;

					return hr;
				}

				return CreateMMDeviceProxy(m_pLoader, pOriginalDevice, ppEndpoint);
			}
			else
			{
				return m_pUnderlyingObject->GetDefaultAudioEndpoint(dataFlow, role, ppEndpoint);
			}
		}

	    HRESULT STDMETHODCALLTYPE GetDevice(LPCWSTR pwstrId, IMMDevice** ppDevice)
		{
			return m_pUnderlyingObject->GetDevice(pwstrId, ppDevice);
		}

		HRESULT STDMETHODCALLTYPE RegisterEndpointNotificationCallback(PVOID /*IMMNotificationClient*/ * pClient)
		{
			return m_pUnderlyingObject->RegisterEndpointNotificationCallback(pClient);
		}

		HRESULT STDMETHODCALLTYPE UnregisterEndpointNotificationCallback(PVOID /*IMMNotificationClient*/ * pClient)
		{
			return m_pUnderlyingObject->UnregisterEndpointNotificationCallback(pClient);
		}
	};

	HRESULT CreateMMDeviceEnumeratorProxy(CFlashOCXLoader* pLoader, IMMDeviceEnumerator* pOriginalObject, REFIID refiid, PVOID* ppProxyObject)
	{
		CComPtr<IUnknown> pProxy = new CMMDeviceEnumeratorProxy(pLoader, pOriginalObject);

		return pProxy->QueryInterface(refiid, ppProxyObject);
	}
}
