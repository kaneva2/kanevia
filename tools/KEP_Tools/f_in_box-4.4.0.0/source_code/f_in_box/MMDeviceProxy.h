#ifndef __MMDeviceProxy_H_
#define __MMDeviceProxy_H_

namespace f_in_box
{
	HRESULT CreateMMDeviceProxy(CFlashOCXLoader* pLoader, IMMDevice* pOriginalMMDevice, IMMDevice** ppProxyMMDevice);
}

#endif // !__MMDeviceProxy_H_
