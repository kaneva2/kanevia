#ifndef __ENUM_UNKNOWN_H__43D0A6ED_4A08_41e3_BF43_FA91075BFC62__
#define __ENUM_UNKNOWN_H__43D0A6ED_4A08_41e3_BF43_FA91075BFC62__

namespace f_in_box
{
    namespace com_helpers
    {
        class CEnumUnknownImpl : public IEnumUnknown
        {
        private:
            ULONG m_nRefCount;
            IUnknown** m_pUnknowns;
            ULONG m_nCount;
            ULONG m_nPos;

        public:
            static
            HRESULT
            CreateInstance(REFIID refiid, void** ppObject, IUnknown** ppUnknownArray, ULONG nCount)
            {
                CEnumUnknownImpl* pEnumUnknownImpl = new CEnumUnknownImpl;

                HRESULT hr = pEnumUnknownImpl->QueryInterface(refiid, ppObject);

                if (S_OK != hr)
                    delete pEnumUnknownImpl;
                else
                {
                    pEnumUnknownImpl->m_nCount = nCount;
                    pEnumUnknownImpl->m_pUnknowns = new IUnknown* [nCount];

                    for (ULONG i = 0; i < nCount; i++)
                    {
                        pEnumUnknownImpl->m_pUnknowns[i] = ppUnknownArray[i];

                        if (pEnumUnknownImpl->m_pUnknowns[i])
                            pEnumUnknownImpl->m_pUnknowns[i]->AddRef();
                    }

                    pEnumUnknownImpl->m_nPos = 0;
                }

                return hr;
            }

        private:
            CEnumUnknownImpl() : 
                m_nRefCount(0), 
                m_nPos(0), 
                m_nCount(0)
            {
            }

            ~CEnumUnknownImpl()
            {
                ASSERT(0 == m_nRefCount);

                for (ULONG i = 0; i < m_nCount; i++)
                    m_pUnknowns[i]->Release();

                delete[] m_pUnknowns;
            }

            // IUnknown
            virtual HRESULT STDMETHODCALLTYPE QueryInterface(REFIID riid, void** ppvObject)
            {
                void* pObject = NULL;

#define DEF_BEGIN_INTERFACE_TABLE
#define DEF_SUPPORT_INTERFACE(supported_iid, interface_class) if (f_in_box::com_helpers::IsEqualGUID(supported_iid, riid)) { pObject = (interface_class*)this; } else
#define DEF_END_INTERFACE_TABLE              { }

                DEF_BEGIN_INTERFACE_TABLE
                    DEF_SUPPORT_INTERFACE(IID_IUnknown, IUnknown)
                    DEF_SUPPORT_INTERFACE(IID_IEnumUnknown, IEnumUnknown)
                DEF_END_INTERFACE_TABLE

#undef DEF_BEGIN_INTERFACE_TABLE
#undef DEF_SUPPORT_INTERFACE
#undef DEF_END_INTERFACE_TABLE

                HRESULT hr;

                *ppvObject = pObject;

                if (NULL != pObject)
                {
                    AddRef();
                    hr = S_OK;
                }
                else
                    hr = E_NOINTERFACE;

                return hr;
            }

            virtual ULONG STDMETHODCALLTYPE AddRef()
            {
                return ++m_nRefCount;
            }

            virtual ULONG STDMETHODCALLTYPE Release()
            {
                ULONG nNewRefCount = --m_nRefCount;

                if (0 == nNewRefCount)
                    delete this;

                return nNewRefCount;
            }

            // IEnumUnknown
            virtual HRESULT STDMETHODCALLTYPE Next(ULONG celt, IUnknown** rgelt, ULONG* pceltFetched)
            {
                ULONG n = 0;
                
                for (; 
                     m_nPos < m_nCount && n < celt; 
                     m_nPos++, n++)
                {
                    rgelt[n] = m_pUnknowns[m_nPos];

                    if (rgelt[n])
                        rgelt[n]->AddRef();
                }

                return n == celt ? S_OK : S_FALSE;
            }
        
            virtual HRESULT STDMETHODCALLTYPE Skip(ULONG celt)
            {
                if (0 == m_nCount)
                    return S_FALSE;

                ULONG nNewPos = m_nPos + celt;

                if (nNewPos > m_nCount)
                {
                    m_nPos = m_nCount - 1;

                    return S_FALSE;
                }
                else
                {
                    m_nPos = nNewPos;

                    return S_OK;
                }
            }
        
            virtual HRESULT STDMETHODCALLTYPE Reset()
            {
                m_nPos = 0;

                return S_OK;
            }
        
            virtual HRESULT STDMETHODCALLTYPE Clone(IEnumUnknown** ppenum)
            {
                return CreateInstance(IID_IEnumUnknown, (void**)ppenum, m_pUnknowns, m_nCount);
            }
        };
    }
}

#endif // __ENUM_UNKNOWN_H__43D0A6ED_4A08_41e3_BF43_FA91075BFC62__
