#pragma once

namespace f_in_box
{
    class ITimerService;
    class ITimer;
    class ITimerSink;

    extern const IID IID_ITimerService;
    extern const IID IID_ITimer;
    extern const IID IID_ITimerSink;

    class ITimerService : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE CreateTimer(ITimer* pReferenceTimer, ITimer** ppNewTimer) = 0;
        virtual HRESULT STDMETHODCALLTYPE GetNamedTimer(REFGUID rguidName, ITimer** ppTimer) = 0;
        virtual HRESULT STDMETHODCALLTYPE SetNamedTimerReference(REFGUID rguidName, ITimer* pReferenceTimer) = 0;
    };

    class ITimer : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE Advise(
            VARIANT vtimeMin,
            VARIANT vtimeMax,
            VARIANT vtimeInterval,
            DWORD dwFlags,
            ITimerSink* pTimerSink,
            DWORD* pdwCookie) = 0;
        virtual HRESULT STDMETHODCALLTYPE Unadvise(DWORD dwCookie) = 0;
        virtual HRESULT STDMETHODCALLTYPE Freeze(BOOL fFreeze) = 0;
        virtual HRESULT STDMETHODCALLTYPE GetTime(VARIANT *pvtime) = 0;
    };

    class ITimerSink : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE OnTimer(VARIANT vtimeAdvise) = 0;
    };
}
