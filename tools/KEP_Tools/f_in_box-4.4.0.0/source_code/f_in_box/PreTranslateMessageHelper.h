#ifndef __PRE_TRANSLATE_MESSAGE_HELPER__2F2F0198_70FD_4f0d_B817_EB907A08623F__
#define __PRE_TRANSLATE_MESSAGE_HELPER__2F2F0198_70FD_4f0d_B817_EB907A08623F__

namespace f_in_box
{

class CPreTranslateMessageHelper
{
private:
    HHOOK m_hHook;
    LPVOID m_lpAdapterCode;

public:
    CPreTranslateMessageHelper();
    virtual ~CPreTranslateMessageHelper();

protected:
    virtual void PreTranslateMessageHelper_PreTranslateMessage(MSG* pMsg);

	void Start();
	void Stop();

private:
    static
    LRESULT
    CALLBACK
    StaticGetMsgProc(
        int code,       // hook code
        WPARAM wParam,  // removal option
        LPARAM lParam   // message
    );
};

}

#endif // !__PRE_TRANSLATE_MESSAGE_HELPER__2F2F0198_70FD_4f0d_B817_EB907A08623F__
