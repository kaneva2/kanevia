#ifndef __URL_MON_HELPER_H__63F876AE_638D_4784_B8E8_71D95F6B2A91__
#define __URL_MON_HELPER_H__63F876AE_638D_4784_B8E8_71D95F6B2A91__

#include <urlmon.h>

namespace f_in_box
{
    namespace UrlMonHelper
    {
        struct SUrlMonHelper
        {
        private:
            HMODULE m_hURLMON;

            typedef HRESULT (WINAPI *P_CreateURLMoniker)(IMoniker *pmkContext, 
                                                         LPCWSTR szURL, 
                                                         IMoniker** ppmk);
            P_CreateURLMoniker m_pCreateURLMoniker;

            typedef HRESULT (WINAPI *P_RegisterBindStatusCallback)(IBindCtx *pbc, 
                                                                   IBindStatusCallback *pbsc, 
                                                                   IBindStatusCallback **ppbscPrevious, 
                                                                   DWORD dwReserved);
            P_RegisterBindStatusCallback m_pRegisterBindStatusCallback;

        public:
            SUrlMonHelper() : 
                m_hURLMON(NULL), 
                m_pCreateURLMoniker(NULL)
            {
                m_hURLMON = LoadLibrary(_T("urlmon.dll"));

                if (NULL != m_hURLMON)
                {
                    (FARPROC&)m_pCreateURLMoniker = GetProcAddress(m_hURLMON, "CreateURLMoniker");
                    ASSERT(NULL != m_pCreateURLMoniker);

                    (FARPROC&)m_pRegisterBindStatusCallback = GetProcAddress(m_hURLMON, "RegisterBindStatusCallback");
                    ASSERT(NULL != m_pRegisterBindStatusCallback);
                }
                else
                {
                    ASSERT(FALSE);
                }
            }

            ~SUrlMonHelper()
            {
                if (NULL != m_hURLMON)
                    FreeLibrary(m_hURLMON);
            }

            HRESULT CreateURLMoniker(IMoniker *pmkContext, 
                                     LPCWSTR szURL, 
                                     IMoniker** ppmk)
            {
                if (NULL != m_pCreateURLMoniker)
                    return m_pCreateURLMoniker(pmkContext, szURL, ppmk);
                else
                    return E_FAIL;
            }

            HRESULT RegisterBindStatusCallback(IBindCtx *pbc, 
                                               IBindStatusCallback *pbsc, 
                                               IBindStatusCallback **ppbscPrevious, 
                                               DWORD dwReserved)
            {
                if (NULL != m_pRegisterBindStatusCallback)
                    return m_pRegisterBindStatusCallback(pbc, pbsc, ppbscPrevious, dwReserved);
                else
                    return E_FAIL;
            }
        };
    }
}

#endif // __URL_MON_HELPER_H__63F876AE_638D_4784_B8E8_71D95F6B2A91__
