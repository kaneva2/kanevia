#ifndef __COM_PTR_H__C135BB49_2AAA_451a_A298_28AE3290E2A5__
#define __COM_PTR_H__C135BB49_2AAA_451a_A298_28AE3290E2A5__

namespace f_in_box
{
    namespace com_helpers
    {
        // Returns TRUE if equal, FALSE if not
        BOOL IsEqualGUID(const GUID& guid1, const GUID& guid2);

        template <class interface_type>
        class CComPtr
        {
        private:
            interface_type* m_p;

        public:
            CComPtr(interface_type* p = NULL)
            {
                m_p = p;

                if (m_p)
                    m_p->AddRef();
            }

            CComPtr(const CComPtr<interface_type>& other)
            {
                m_p = other.m_p;

                if (m_p)
                    m_p->AddRef();
            }

            const CComPtr<interface_type>& operator= (const CComPtr<interface_type>& other)
            {
                Release();

                m_p = other.m_p;

                if (m_p)
                    m_p->AddRef();

                return *this;
            }

            CComPtr(REFIID refiid, IUnknown* pUnknown)
            {
                if (pUnknown)
                    pUnknown->QueryInterface(refiid, (void**)&m_p);
            }

            ~CComPtr()
            {
                Release();
            }

            HRESULT CoCreateInstance(REFCLSID clsid, REFIID refiid, DWORD dwClsContext = CLSCTX_ALL)
            {
                Release();

                return ::CoCreateInstance(clsid, NULL, dwClsContext, refiid, (void**)&m_p);
            }

            void Release()
            {
                if (m_p)
                {
                    m_p->Release();
                    m_p = NULL;
                }
            }

            void Attach(interface_type* p)
            {
                Release();

                m_p = p;
            }

            interface_type* Detach()
            {
                interface_type* p = m_p;

                m_p = NULL;

                return p;
            }

            operator interface_type* ()
            {
                return m_p;
            }

            interface_type** operator& ()
            {
                ASSERT(NULL == m_p);

                return &m_p;
            }

            interface_type* operator-> ()
            {
                return m_p;
            }

            BOOL IsNull() const
            {
                return NULL == m_p;
            }

            BOOL IsNotNull() const
            {
                return NULL != m_p;
            }
        };
    }
}

#endif // !__COM_PTR_H__C135BB49_2AAA_451a_A298_28AE3290E2A5__
