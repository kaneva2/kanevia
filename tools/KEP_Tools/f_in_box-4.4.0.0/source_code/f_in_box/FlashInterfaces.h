#ifndef __FLASH_H__916F0C58_0AD7_4dbf_861C_7FD4796D25BF__
#define __FLASH_H__916F0C58_0AD7_4dbf_861C_7FD4796D25BF__

namespace f_in_box
{
    namespace flash
    {
        static GUID GetFlashObjectGUID()
        {
            // {d27cdb6e-ae6d-11cf-96b8-444553540000}
            GUID guid = 
                { 0xd27cdb6e, 0xae6d, 0x11cf, { 0x96, 0xb8, 0x44, 0x45, 0x53, 0x54, 0x00, 0x00 } };

            return guid;
        }

        static GUID GetFlashInterfaceGUID()
        {
            // {d27cdb6c-ae6d-11cf-96b8-444553540000}
            GUID guid = 
                { 0xd27cdb6c, 0xae6d, 0x11cf, { 0x96, 0xb8, 0x44, 0x45, 0x53, 0x54, 0x00, 0x00 } };

            return guid;
        }

        class IShockwaveFlashEvents : public IDispatch
        {
        public:
            static GUID GetGUID()
            {
                // {d27cdb6d-ae6d-11cf-96b8-444553540000}
                GUID guidShockwaveFlashEvents = 
                    { 0xd27cdb6d, 0xae6d, 0x11cf, { 0x96, 0xb8, 0x44, 0x45, 0x53, 0x54, 0x00, 0x00 } };

                return guidShockwaveFlashEvents;
            }
        };

        namespace v3
        {
            class IShockwaveFlash : public IDispatch
            {
            public:
                static GUID GetGUID()
                {
                    return GetFlashInterfaceGUID();
                }

                virtual HRESULT __stdcall get_ReadyState(/*[out,retval]*/ long * thestate) = 0;
                virtual HRESULT __stdcall get_TotalFrames(/*[out,retval]*/ long * numframes) = 0;
                virtual HRESULT __stdcall get_Playing(/*[out,retval]*/ VARIANT_BOOL * Playing) = 0;
                virtual HRESULT __stdcall put_Playing(/*[in]*/ VARIANT_BOOL Playing) = 0;
                virtual HRESULT __stdcall get_Quality(/*[out,retval]*/ int * Quality) = 0;
                virtual HRESULT __stdcall put_Quality(/*[in]*/ int Quality) = 0;
                virtual HRESULT __stdcall get_ScaleMode(/*[out,retval]*/ int * scale) = 0;
                virtual HRESULT __stdcall put_ScaleMode(/*[in]*/ int scale) = 0;
                virtual HRESULT __stdcall get_AlignMode(/*[out,retval]*/ int * align) = 0;
                virtual HRESULT __stdcall put_AlignMode(/*[in]*/ int align) = 0;
                virtual HRESULT __stdcall get_BackgroundColor(/*[out,retval]*/ long * color) = 0;
                virtual HRESULT __stdcall put_BackgroundColor(/*[in]*/ long color) = 0;
                virtual HRESULT __stdcall get_Loop(/*[out,retval]*/ VARIANT_BOOL * Loop) = 0;
                virtual HRESULT __stdcall put_Loop(/*[in]*/ VARIANT_BOOL Loop) = 0;
                virtual HRESULT __stdcall get_Movie(/*[out,retval]*/ BSTR * path) = 0;
                virtual HRESULT __stdcall put_Movie(/*[in]*/ BSTR path) = 0;
                virtual HRESULT __stdcall get_FrameNum(/*[out,retval]*/ long * FrameNum) = 0;
                virtual HRESULT __stdcall put_FrameNum(/*[in]*/ long FrameNum) = 0;
                virtual HRESULT __stdcall SetZoomRect(/*[in]*/ long left,
                        /*[in]*/ long top,
                        /*[in]*/ long right,
                        /*[in]*/ long bottom) = 0;
                virtual HRESULT __stdcall Zoom(/*[in]*/ int factor) = 0;
                virtual HRESULT __stdcall Pan(/*[in]*/ long x,
                        /*[in]*/ long y,
                        /*[in]*/ int mode) = 0;
                virtual HRESULT __stdcall Play() = 0;
                virtual HRESULT __stdcall Stop() = 0;
                virtual HRESULT __stdcall Back() = 0;
                virtual HRESULT __stdcall Forward() = 0;
                virtual HRESULT __stdcall Rewind() = 0;
                virtual HRESULT __stdcall StopPlay() = 0;
                virtual HRESULT __stdcall GotoFrame(/*[in]*/ long FrameNum) = 0;
                virtual HRESULT __stdcall CurrentFrame(/*[out,retval]*/ long * FrameNum) = 0;
                virtual HRESULT __stdcall IsPlaying(/*[out,retval]*/ VARIANT_BOOL * Playing) = 0;
                virtual HRESULT __stdcall PercentLoaded(/*[out,retval]*/ long * __MIDL_0015) = 0;
                virtual HRESULT __stdcall FrameLoaded(/*[in]*/ long FrameNum,
                        /*[out,retval]*/ VARIANT_BOOL * loaded) = 0;
                virtual HRESULT __stdcall FlashVersion(/*[out,retval]*/ long * version) = 0;
                virtual HRESULT __stdcall get_WMode(/*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall put_WMode(/*[in]*/ BSTR pVal) = 0;
                virtual HRESULT __stdcall get_SAlign(/*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall put_SAlign(/*[in]*/ BSTR pVal) = 0;
                virtual HRESULT __stdcall get_Menu(/*[out,retval]*/ VARIANT_BOOL * pVal) = 0;
                virtual HRESULT __stdcall put_Menu(/*[in]*/ VARIANT_BOOL pVal) = 0;
                virtual HRESULT __stdcall get_Base(/*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall put_Base(/*[in]*/ BSTR pVal) = 0;
                virtual HRESULT __stdcall get_Scale(/*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall put_Scale(/*[in]*/ BSTR pVal) = 0;
                virtual HRESULT __stdcall get_DeviceFont(/*[out,retval]*/ VARIANT_BOOL * pVal) = 0;
                virtual HRESULT __stdcall put_DeviceFont(/*[in]*/ VARIANT_BOOL pVal) = 0;
                virtual HRESULT __stdcall get_EmbedMovie(/*[out,retval]*/ VARIANT_BOOL * pVal) = 0;
                virtual HRESULT __stdcall put_EmbedMovie(/*[in]*/ VARIANT_BOOL pVal) = 0;
                virtual HRESULT __stdcall get_BGColor(/*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall put_BGColor(/*[in]*/ BSTR pVal) = 0;
                virtual HRESULT __stdcall get_Quality2(/*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall put_Quality2(/*[in]*/ BSTR pVal) = 0;
                virtual HRESULT __stdcall LoadMovie(/*[in]*/ int layer,
                        /*[in]*/ BSTR url) = 0;
                virtual HRESULT __stdcall TGotoFrame(/*[in]*/ BSTR target,
                        /*[in]*/ long FrameNum) = 0;
                virtual HRESULT __stdcall TGotoLabel(/*[in]*/ BSTR target,
                        /*[in]*/ BSTR label) = 0;
                virtual HRESULT __stdcall TCurrentFrame(/*[in]*/ BSTR target,
                        /*[out,retval]*/ long * FrameNum) = 0;
                virtual HRESULT __stdcall TCurrentLabel(/*[in]*/ BSTR target,
                        /*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall TPlay(/*[in]*/ BSTR target) = 0;
                virtual HRESULT __stdcall TStopPlay(/*[in]*/ BSTR target) = 0;
            };
        }

        namespace v4
        {
            class IShockwaveFlash : public IDispatch
            {
            public:
                static GUID GetGUID()
                {
                    return GetFlashInterfaceGUID();
                }

                virtual HRESULT __stdcall get_ReadyState(/*[out,retval]*/ long * thestate) = 0;
                virtual HRESULT __stdcall get_TotalFrames(/*[out,retval]*/ long * numframes) = 0;
                virtual HRESULT __stdcall get_Playing(/*[out,retval]*/ VARIANT_BOOL * Playing) = 0;
                virtual HRESULT __stdcall put_Playing(/*[in]*/ VARIANT_BOOL Playing) = 0;
                virtual HRESULT __stdcall get_Quality(/*[out,retval]*/ int * Quality) = 0;
                virtual HRESULT __stdcall put_Quality(/*[in]*/ int Quality) = 0;
                virtual HRESULT __stdcall get_ScaleMode(/*[out,retval]*/ int * scale) = 0;
                virtual HRESULT __stdcall put_ScaleMode(/*[in]*/ int scale) = 0;
                virtual HRESULT __stdcall get_AlignMode(/*[out,retval]*/ int * align) = 0;
                virtual HRESULT __stdcall put_AlignMode(/*[in]*/ int align) = 0;
                virtual HRESULT __stdcall get_BackgroundColor(/*[out,retval]*/ long * color) = 0;
                virtual HRESULT __stdcall put_BackgroundColor(/*[in]*/ long color) = 0;
                virtual HRESULT __stdcall get_Loop(/*[out,retval]*/ VARIANT_BOOL * Loop) = 0;
                virtual HRESULT __stdcall put_Loop(/*[in]*/ VARIANT_BOOL Loop) = 0;
                virtual HRESULT __stdcall get_Movie(/*[out,retval]*/ BSTR * path) = 0;
                virtual HRESULT __stdcall put_Movie(/*[in]*/ BSTR path) = 0;
                virtual HRESULT __stdcall get_FrameNum(/*[out,retval]*/ long * FrameNum) = 0;
                virtual HRESULT __stdcall put_FrameNum(/*[in]*/ long FrameNum) = 0;
                virtual HRESULT __stdcall SetZoomRect(/*[in]*/ long left,
                        /*[in]*/ long top,
                        /*[in]*/ long right,
                        /*[in]*/ long bottom) = 0;
                virtual HRESULT __stdcall Zoom(/*[in]*/ int factor) = 0;
                virtual HRESULT __stdcall Pan(/*[in]*/ long x,
                        /*[in]*/ long y,
                        /*[in]*/ int mode) = 0;
                virtual HRESULT __stdcall Play() = 0;
                virtual HRESULT __stdcall Stop() = 0;
                virtual HRESULT __stdcall Back() = 0;
                virtual HRESULT __stdcall Forward() = 0;
                virtual HRESULT __stdcall Rewind() = 0;
                virtual HRESULT __stdcall StopPlay() = 0;
                virtual HRESULT __stdcall GotoFrame(/*[in]*/ long FrameNum) = 0;
                virtual HRESULT __stdcall CurrentFrame(/*[out,retval]*/ long * FrameNum) = 0;
                virtual HRESULT __stdcall IsPlaying(/*[out,retval]*/ VARIANT_BOOL * Playing) = 0;
                virtual HRESULT __stdcall PercentLoaded(/*[out,retval]*/ long * __MIDL_0015) = 0;
                virtual HRESULT __stdcall FrameLoaded(/*[in]*/ long FrameNum,
                        /*[out,retval]*/ VARIANT_BOOL * loaded) = 0;
                virtual HRESULT __stdcall FlashVersion(/*[out,retval]*/ long * version) = 0;
                virtual HRESULT __stdcall get_WMode(/*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall put_WMode(/*[in]*/ BSTR pVal) = 0;
                virtual HRESULT __stdcall get_SAlign(/*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall put_SAlign(/*[in]*/ BSTR pVal) = 0;
                virtual HRESULT __stdcall get_Menu(/*[out,retval]*/ VARIANT_BOOL * pVal) = 0;
                virtual HRESULT __stdcall put_Menu(/*[in]*/ VARIANT_BOOL pVal) = 0;
                virtual HRESULT __stdcall get_Base(/*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall put_Base(/*[in]*/ BSTR pVal) = 0;
                virtual HRESULT __stdcall get_Scale(/*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall put_Scale(/*[in]*/ BSTR pVal) = 0;
                virtual HRESULT __stdcall get_DeviceFont(/*[out,retval]*/ VARIANT_BOOL * pVal) = 0;
                virtual HRESULT __stdcall put_DeviceFont(/*[in]*/ VARIANT_BOOL pVal) = 0;
                virtual HRESULT __stdcall get_EmbedMovie(/*[out,retval]*/ VARIANT_BOOL * pVal) = 0;
                virtual HRESULT __stdcall put_EmbedMovie(/*[in]*/ VARIANT_BOOL pVal) = 0;
                virtual HRESULT __stdcall get_BGColor(/*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall put_BGColor(/*[in]*/ BSTR pVal) = 0;
                virtual HRESULT __stdcall get_Quality2(/*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall put_Quality2(/*[in]*/ BSTR pVal) = 0;
                virtual HRESULT __stdcall LoadMovie(/*[in]*/ int layer,
                        /*[in]*/ BSTR url) = 0;
                virtual HRESULT __stdcall TGotoFrame(/*[in]*/ BSTR target,
                        /*[in]*/ long FrameNum) = 0;
                virtual HRESULT __stdcall TGotoLabel(/*[in]*/ BSTR target,
                        /*[in]*/ BSTR label) = 0;
                virtual HRESULT __stdcall TCurrentFrame(/*[in]*/ BSTR target,
                        /*[out,retval]*/ long * FrameNum) = 0;
                virtual HRESULT __stdcall TCurrentLabel(/*[in]*/ BSTR target,
                        /*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall TPlay(/*[in]*/ BSTR target) = 0;
                virtual HRESULT __stdcall TStopPlay(/*[in]*/ BSTR target) = 0;
                virtual HRESULT __stdcall SetVariable(/*[in]*/ BSTR name,
                        /*[in]*/ BSTR value) = 0;
                virtual HRESULT __stdcall GetVariable(/*[in]*/ BSTR name,
                        /*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall TSetProperty(/*[in]*/ BSTR target,
                        /*[in]*/ int property,
                        /*[in]*/ BSTR value) = 0;
                virtual HRESULT __stdcall TGetProperty(/*[in]*/ BSTR target,
                        /*[in]*/ int property,
                        /*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall TCallFrame(/*[in]*/ BSTR target,
                        /*[in]*/ int FrameNum) = 0;
                virtual HRESULT __stdcall TCallLabel(/*[in]*/ BSTR target,
                        /*[in]*/ BSTR label) = 0;
                virtual HRESULT __stdcall TSetPropertyNum(/*[in]*/ BSTR target,
                        /*[in]*/ int property,
                        /*[in]*/ double value) = 0;
                virtual HRESULT __stdcall TGetPropertyNum(/*[in]*/ BSTR target,
                        /*[in]*/ int property,
                        /*[out,retval]*/ double * pVal) = 0;
                virtual HRESULT __stdcall get_SWRemote(/*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall put_SWRemote(/*[in]*/ BSTR pVal) = 0;
            };
        }

        namespace v5
        {
            class IShockwaveFlash : public IDispatch
            {
            public:
                static GUID GetGUID()
                {
                    return GetFlashInterfaceGUID();
                }

                virtual HRESULT __stdcall get_ReadyState(/*[out,retval]*/ long * thestate) = 0;
                virtual HRESULT __stdcall get_TotalFrames(/*[out,retval]*/ long * numframes) = 0;
                virtual HRESULT __stdcall get_Playing(/*[out,retval]*/ VARIANT_BOOL * Playing) = 0;
                virtual HRESULT __stdcall put_Playing(/*[in]*/ VARIANT_BOOL Playing) = 0;
                virtual HRESULT __stdcall get_Quality(/*[out,retval]*/ int * Quality) = 0;
                virtual HRESULT __stdcall put_Quality(/*[in]*/ int Quality) = 0;
                virtual HRESULT __stdcall get_ScaleMode(/*[out,retval]*/ int * scale) = 0;
                virtual HRESULT __stdcall put_ScaleMode(/*[in]*/ int scale) = 0;
                virtual HRESULT __stdcall get_AlignMode(/*[out,retval]*/ int * align) = 0;
                virtual HRESULT __stdcall put_AlignMode(/*[in]*/ int align) = 0;
                virtual HRESULT __stdcall get_BackgroundColor(/*[out,retval]*/ long * color) = 0;
                virtual HRESULT __stdcall put_BackgroundColor(/*[in]*/ long color) = 0;
                virtual HRESULT __stdcall get_Loop(/*[out,retval]*/ VARIANT_BOOL * Loop) = 0;
                virtual HRESULT __stdcall put_Loop(/*[in]*/ VARIANT_BOOL Loop) = 0;
                virtual HRESULT __stdcall get_Movie(/*[out,retval]*/ BSTR * path) = 0;
                virtual HRESULT __stdcall put_Movie(/*[in]*/ BSTR path) = 0;
                virtual HRESULT __stdcall get_FrameNum(/*[out,retval]*/ long * FrameNum) = 0;
                virtual HRESULT __stdcall put_FrameNum(/*[in]*/ long FrameNum) = 0;
                virtual HRESULT __stdcall SetZoomRect(/*[in]*/ long left,
                        /*[in]*/ long top,
                        /*[in]*/ long right,
                        /*[in]*/ long bottom) = 0;
                virtual HRESULT __stdcall Zoom(/*[in]*/ int factor) = 0;
                virtual HRESULT __stdcall Pan(/*[in]*/ long x,
                        /*[in]*/ long y,
                        /*[in]*/ int mode) = 0;
                virtual HRESULT __stdcall Play() = 0;
                virtual HRESULT __stdcall Stop() = 0;
                virtual HRESULT __stdcall Back() = 0;
                virtual HRESULT __stdcall Forward() = 0;
                virtual HRESULT __stdcall Rewind() = 0;
                virtual HRESULT __stdcall StopPlay() = 0;
                virtual HRESULT __stdcall GotoFrame(/*[in]*/ long FrameNum) = 0;
                virtual HRESULT __stdcall CurrentFrame(/*[out,retval]*/ long * FrameNum) = 0;
                virtual HRESULT __stdcall IsPlaying(/*[out,retval]*/ VARIANT_BOOL * Playing) = 0;
                virtual HRESULT __stdcall PercentLoaded(/*[out,retval]*/ long * __MIDL_0011) = 0;
                virtual HRESULT __stdcall FrameLoaded(/*[in]*/ long FrameNum,
                        /*[out,retval]*/ VARIANT_BOOL * loaded) = 0;
                virtual HRESULT __stdcall FlashVersion(/*[out,retval]*/ long * version) = 0;
                virtual HRESULT __stdcall get_WMode(/*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall put_WMode(/*[in]*/ BSTR pVal) = 0;
                virtual HRESULT __stdcall get_SAlign(/*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall put_SAlign(/*[in]*/ BSTR pVal) = 0;
                virtual HRESULT __stdcall get_Menu(/*[out,retval]*/ VARIANT_BOOL * pVal) = 0;
                virtual HRESULT __stdcall put_Menu(/*[in]*/ VARIANT_BOOL pVal) = 0;
                virtual HRESULT __stdcall get_Base(/*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall put_Base(/*[in]*/ BSTR pVal) = 0;
                virtual HRESULT __stdcall get_Scale(/*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall put_Scale(/*[in]*/ BSTR pVal) = 0;
                virtual HRESULT __stdcall get_DeviceFont(/*[out,retval]*/ VARIANT_BOOL * pVal) = 0;
                virtual HRESULT __stdcall put_DeviceFont(/*[in]*/ VARIANT_BOOL pVal) = 0;
                virtual HRESULT __stdcall get_EmbedMovie(/*[out,retval]*/ VARIANT_BOOL * pVal) = 0;
                virtual HRESULT __stdcall put_EmbedMovie(/*[in]*/ VARIANT_BOOL pVal) = 0;
                virtual HRESULT __stdcall get_BGColor(/*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall put_BGColor(/*[in]*/ BSTR pVal) = 0;
                virtual HRESULT __stdcall get_Quality2(/*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall put_Quality2(/*[in]*/ BSTR pVal) = 0;
                virtual HRESULT __stdcall LoadMovie(/*[in]*/ int layer,
                        /*[in]*/ BSTR url) = 0;
                virtual HRESULT __stdcall TGotoFrame(/*[in]*/ BSTR target,
                        /*[in]*/ long FrameNum) = 0;
                virtual HRESULT __stdcall TGotoLabel(/*[in]*/ BSTR target,
                        /*[in]*/ BSTR label) = 0;
                virtual HRESULT __stdcall TCurrentFrame(/*[in]*/ BSTR target,
                        /*[out,retval]*/ long * FrameNum) = 0;
                virtual HRESULT __stdcall TCurrentLabel(/*[in]*/ BSTR target,
                        /*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall TPlay(/*[in]*/ BSTR target) = 0;
                virtual HRESULT __stdcall TStopPlay(/*[in]*/ BSTR target) = 0;
                virtual HRESULT __stdcall SetVariable(/*[in]*/ BSTR name,
                        /*[in]*/ BSTR value) = 0;
                virtual HRESULT __stdcall GetVariable(/*[in]*/ BSTR name,
                        /*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall TSetProperty(/*[in]*/ BSTR target,
                        /*[in]*/ int property,
                        /*[in]*/ BSTR value) = 0;
                virtual HRESULT __stdcall TGetProperty(/*[in]*/ BSTR target,
                        /*[in]*/ int property,
                        /*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall TCallFrame(/*[in]*/ BSTR target,
                        /*[in]*/ int FrameNum) = 0;
                virtual HRESULT __stdcall TCallLabel(/*[in]*/ BSTR target,
                        /*[in]*/ BSTR label) = 0;
                virtual HRESULT __stdcall TSetPropertyNum(/*[in]*/ BSTR target,
                        /*[in]*/ int property,
                        /*[in]*/ double value) = 0;
                virtual HRESULT __stdcall TGetPropertyNum(/*[in]*/ BSTR target,
                        /*[in]*/ int property,
                        /*[out,retval]*/ double * pVal) = 0;
                virtual HRESULT __stdcall get_SWRemote(/*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall put_SWRemote(/*[in]*/ BSTR pVal) = 0;
                virtual HRESULT __stdcall get_Stacking(/*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall put_Stacking(/*[in]*/ BSTR pVal) = 0;
            };
        }

        namespace v6
        {
            class IShockwaveFlash : public IDispatch
            {
            public:
                static GUID GetGUID()
                {
                    return GetFlashInterfaceGUID();
                }

                virtual HRESULT __stdcall get_ReadyState(/*[out,retval]*/ long * pVal) = 0;
                virtual HRESULT __stdcall get_TotalFrames(/*[out,retval]*/ long * pVal) = 0;
                virtual HRESULT __stdcall get_Playing(/*[out,retval]*/ VARIANT_BOOL * pVal) = 0;
                virtual HRESULT __stdcall put_Playing(/*[in]*/ VARIANT_BOOL pVal) = 0;
                virtual HRESULT __stdcall get_Quality(/*[out,retval]*/ int * pVal) = 0;
                virtual HRESULT __stdcall put_Quality(/*[in]*/ int pVal) = 0;
                virtual HRESULT __stdcall get_ScaleMode(/*[out,retval]*/ int * pVal) = 0;
                virtual HRESULT __stdcall put_ScaleMode(/*[in]*/ int pVal) = 0;
                virtual HRESULT __stdcall get_AlignMode(/*[out,retval]*/ int * pVal) = 0;
                virtual HRESULT __stdcall put_AlignMode(/*[in]*/ int pVal) = 0;
                virtual HRESULT __stdcall get_BackgroundColor(/*[out,retval]*/ long * pVal) = 0;
                virtual HRESULT __stdcall put_BackgroundColor(/*[in]*/ long pVal) = 0;
                virtual HRESULT __stdcall get_Loop(/*[out,retval]*/ VARIANT_BOOL * pVal) = 0;
                virtual HRESULT __stdcall put_Loop(/*[in]*/ VARIANT_BOOL pVal) = 0;
                virtual HRESULT __stdcall get_Movie(/*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall put_Movie(/*[in]*/ BSTR pVal) = 0;
                virtual HRESULT __stdcall get_FrameNum(/*[out,retval]*/ long * pVal) = 0;
                virtual HRESULT __stdcall put_FrameNum(/*[in]*/ long pVal) = 0;
                virtual HRESULT __stdcall SetZoomRect(/*[in]*/ long left,
                        /*[in]*/ long top,
                        /*[in]*/ long right,
                        /*[in]*/ long bottom) = 0;
                virtual HRESULT __stdcall Zoom(/*[in]*/ int factor) = 0;
                virtual HRESULT __stdcall Pan(/*[in]*/ long x,
                        /*[in]*/ long y,
                        /*[in]*/ int mode) = 0;
                virtual HRESULT __stdcall Play() = 0;
                virtual HRESULT __stdcall Stop() = 0;
                virtual HRESULT __stdcall Back() = 0;
                virtual HRESULT __stdcall Forward() = 0;
                virtual HRESULT __stdcall Rewind() = 0;
                virtual HRESULT __stdcall StopPlay() = 0;
                virtual HRESULT __stdcall GotoFrame(/*[in]*/ long FrameNum) = 0;
                virtual HRESULT __stdcall CurrentFrame(/*[out,retval]*/ long * FrameNum) = 0;
                virtual HRESULT __stdcall IsPlaying(/*[out,retval]*/ VARIANT_BOOL * Playing) = 0;
                virtual HRESULT __stdcall PercentLoaded(/*[out,retval]*/ long * percent) = 0;
                virtual HRESULT __stdcall FrameLoaded(/*[in]*/ long FrameNum,
                        /*[out,retval]*/ VARIANT_BOOL * loaded) = 0;
                virtual HRESULT __stdcall FlashVersion(/*[out,retval]*/ long * version) = 0;
                virtual HRESULT __stdcall get_WMode(/*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall put_WMode(/*[in]*/ BSTR pVal) = 0;
                virtual HRESULT __stdcall get_SAlign(/*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall put_SAlign(/*[in]*/ BSTR pVal) = 0;
                virtual HRESULT __stdcall get_Menu(/*[out,retval]*/ VARIANT_BOOL * pVal) = 0;
                virtual HRESULT __stdcall put_Menu(/*[in]*/ VARIANT_BOOL pVal) = 0;
                virtual HRESULT __stdcall get_Base(/*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall put_Base(/*[in]*/ BSTR pVal) = 0;
                virtual HRESULT __stdcall get_Scale(/*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall put_Scale(/*[in]*/ BSTR pVal) = 0;
                virtual HRESULT __stdcall get_DeviceFont(/*[out,retval]*/ VARIANT_BOOL * pVal) = 0;
                virtual HRESULT __stdcall put_DeviceFont(/*[in]*/ VARIANT_BOOL pVal) = 0;
                virtual HRESULT __stdcall get_EmbedMovie(/*[out,retval]*/ VARIANT_BOOL * pVal) = 0;
                virtual HRESULT __stdcall put_EmbedMovie(/*[in]*/ VARIANT_BOOL pVal) = 0;
                virtual HRESULT __stdcall get_BGColor(/*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall put_BGColor(/*[in]*/ BSTR pVal) = 0;
                virtual HRESULT __stdcall get_Quality2(/*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall put_Quality2(/*[in]*/ BSTR pVal) = 0;
                virtual HRESULT __stdcall LoadMovie(/*[in]*/ int layer,
                        /*[in]*/ BSTR url) = 0;
                virtual HRESULT __stdcall TGotoFrame(/*[in]*/ BSTR target,
                        /*[in]*/ long FrameNum) = 0;
                virtual HRESULT __stdcall TGotoLabel(/*[in]*/ BSTR target,
                        /*[in]*/ BSTR label) = 0;
                virtual HRESULT __stdcall TCurrentFrame(/*[in]*/ BSTR target,
                        /*[out,retval]*/ long * FrameNum) = 0;
                virtual HRESULT __stdcall TCurrentLabel(/*[in]*/ BSTR target,
                        /*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall TPlay(/*[in]*/ BSTR target) = 0;
                virtual HRESULT __stdcall TStopPlay(/*[in]*/ BSTR target) = 0;
                virtual HRESULT __stdcall SetVariable(/*[in]*/ BSTR name,
                        /*[in]*/ BSTR value) = 0;
                virtual HRESULT __stdcall GetVariable(/*[in]*/ BSTR name,
                        /*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall TSetProperty(/*[in]*/ BSTR target,
                        /*[in]*/ int property,
                        /*[in]*/ BSTR value) = 0;
                virtual HRESULT __stdcall TGetProperty(/*[in]*/ BSTR target,
                        /*[in]*/ int property,
                        /*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall TCallFrame(/*[in]*/ BSTR target,
                        /*[in]*/ int FrameNum) = 0;
                virtual HRESULT __stdcall TCallLabel(/*[in]*/ BSTR target,
                        /*[in]*/ BSTR label) = 0;
                virtual HRESULT __stdcall TSetPropertyNum(/*[in]*/ BSTR target,
                        /*[in]*/ int property,
                        /*[in]*/ double value) = 0;
                virtual HRESULT __stdcall TGetPropertyNum(/*[in]*/ BSTR target,
                        /*[in]*/ int property,
                        /*[out,retval]*/ double * pVal) = 0;
            };
        }

        namespace v7
        {
            class IShockwaveFlash : public IDispatch
            {
            public:
                static GUID GetGUID()
                {
                    return GetFlashInterfaceGUID();
                }

                virtual HRESULT __stdcall get_ReadyState(/*[out,retval]*/ long * pVal) = 0;
                virtual HRESULT __stdcall get_TotalFrames(/*[out,retval]*/ long * pVal) = 0;
                virtual HRESULT __stdcall get_Playing(/*[out,retval]*/ VARIANT_BOOL * pVal) = 0;
                virtual HRESULT __stdcall put_Playing(/*[in]*/ VARIANT_BOOL pVal) = 0;
                virtual HRESULT __stdcall get_Quality(/*[out,retval]*/ int * pVal) = 0;
                virtual HRESULT __stdcall put_Quality(/*[in]*/ int pVal) = 0;
                virtual HRESULT __stdcall get_ScaleMode(/*[out,retval]*/ int * pVal) = 0;
                virtual HRESULT __stdcall put_ScaleMode(/*[in]*/ int pVal) = 0;
                virtual HRESULT __stdcall get_AlignMode(/*[out,retval]*/ int * pVal) = 0;
                virtual HRESULT __stdcall put_AlignMode(/*[in]*/ int pVal) = 0;
                virtual HRESULT __stdcall get_BackgroundColor(/*[out,retval]*/ long * pVal) = 0;
                virtual HRESULT __stdcall put_BackgroundColor(/*[in]*/ long pVal) = 0;
                virtual HRESULT __stdcall get_Loop(/*[out,retval]*/ VARIANT_BOOL * pVal) = 0;
                virtual HRESULT __stdcall put_Loop(/*[in]*/ VARIANT_BOOL pVal) = 0;
                virtual HRESULT __stdcall get_Movie(/*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall put_Movie(/*[in]*/ BSTR pVal) = 0;
                virtual HRESULT __stdcall get_FrameNum(/*[out,retval]*/ long * pVal) = 0;
                virtual HRESULT __stdcall put_FrameNum(/*[in]*/ long pVal) = 0;
                virtual HRESULT __stdcall SetZoomRect(/*[in]*/ long left,
                        /*[in]*/ long top,
                        /*[in]*/ long right,
                        /*[in]*/ long bottom) = 0;
                virtual HRESULT __stdcall Zoom(/*[in]*/ int factor) = 0;
                virtual HRESULT __stdcall Pan(/*[in]*/ long x,
                        /*[in]*/ long y,
                        /*[in]*/ int mode) = 0;
                virtual HRESULT __stdcall Play() = 0;
                virtual HRESULT __stdcall Stop() = 0;
                virtual HRESULT __stdcall Back() = 0;
                virtual HRESULT __stdcall Forward() = 0;
                virtual HRESULT __stdcall Rewind() = 0;
                virtual HRESULT __stdcall StopPlay() = 0;
                virtual HRESULT __stdcall GotoFrame(/*[in]*/ long FrameNum) = 0;
                virtual HRESULT __stdcall CurrentFrame(/*[out,retval]*/ long * FrameNum) = 0;
                virtual HRESULT __stdcall IsPlaying(/*[out,retval]*/ VARIANT_BOOL * Playing) = 0;
                virtual HRESULT __stdcall PercentLoaded(/*[out,retval]*/ long * percent) = 0;
                virtual HRESULT __stdcall FrameLoaded(/*[in]*/ long FrameNum,
                        /*[out,retval]*/ VARIANT_BOOL * loaded) = 0;
                virtual HRESULT __stdcall FlashVersion(/*[out,retval]*/ long * version) = 0;
                virtual HRESULT __stdcall get_WMode(/*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall put_WMode(/*[in]*/ BSTR pVal) = 0;
                virtual HRESULT __stdcall get_SAlign(/*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall put_SAlign(/*[in]*/ BSTR pVal) = 0;
                virtual HRESULT __stdcall get_Menu(/*[out,retval]*/ VARIANT_BOOL * pVal) = 0;
                virtual HRESULT __stdcall put_Menu(/*[in]*/ VARIANT_BOOL pVal) = 0;
                virtual HRESULT __stdcall get_Base(/*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall put_Base(/*[in]*/ BSTR pVal) = 0;
                virtual HRESULT __stdcall get_Scale(/*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall put_Scale(/*[in]*/ BSTR pVal) = 0;
                virtual HRESULT __stdcall get_DeviceFont(/*[out,retval]*/ VARIANT_BOOL * pVal) = 0;
                virtual HRESULT __stdcall put_DeviceFont(/*[in]*/ VARIANT_BOOL pVal) = 0;
                virtual HRESULT __stdcall get_EmbedMovie(/*[out,retval]*/ VARIANT_BOOL * pVal) = 0;
                virtual HRESULT __stdcall put_EmbedMovie(/*[in]*/ VARIANT_BOOL pVal) = 0;
                virtual HRESULT __stdcall get_BGColor(/*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall put_BGColor(/*[in]*/ BSTR pVal) = 0;
                virtual HRESULT __stdcall get_Quality2(/*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall put_Quality2(/*[in]*/ BSTR pVal) = 0;
                virtual HRESULT __stdcall LoadMovie(/*[in]*/ int layer,
                        /*[in]*/ BSTR url) = 0;
                virtual HRESULT __stdcall TGotoFrame(/*[in]*/ BSTR target,
                        /*[in]*/ long FrameNum) = 0;
                virtual HRESULT __stdcall TGotoLabel(/*[in]*/ BSTR target,
                        /*[in]*/ BSTR label) = 0;
                virtual HRESULT __stdcall TCurrentFrame(/*[in]*/ BSTR target,
                        /*[out,retval]*/ long * FrameNum) = 0;
                virtual HRESULT __stdcall TCurrentLabel(/*[in]*/ BSTR target,
                        /*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall TPlay(/*[in]*/ BSTR target) = 0;
                virtual HRESULT __stdcall TStopPlay(/*[in]*/ BSTR target) = 0;
                virtual HRESULT __stdcall SetVariable(/*[in]*/ BSTR name,
                        /*[in]*/ BSTR value) = 0;
                virtual HRESULT __stdcall GetVariable(/*[in]*/ BSTR name,
                        /*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall TSetProperty(/*[in]*/ BSTR target,
                        /*[in]*/ int property,
                        /*[in]*/ BSTR value) = 0;
                virtual HRESULT __stdcall TGetProperty(/*[in]*/ BSTR target,
                        /*[in]*/ int property,
                        /*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall TCallFrame(/*[in]*/ BSTR target,
                        /*[in]*/ int FrameNum) = 0;
                virtual HRESULT __stdcall TCallLabel(/*[in]*/ BSTR target,
                        /*[in]*/ BSTR label) = 0;
                virtual HRESULT __stdcall TSetPropertyNum(/*[in]*/ BSTR target,
                        /*[in]*/ int property,
                        /*[in]*/ double value) = 0;
                virtual HRESULT __stdcall TGetPropertyNum(/*[in]*/ BSTR target,
                        /*[in]*/ int property,
                        /*[out,retval]*/ double * pVal) = 0;
                virtual HRESULT __stdcall TGetPropertyAsNumber(/*[in]*/ BSTR target,
                        /*[in]*/ int property,
                        /*[out,retval]*/ double * pVal) = 0;
                virtual HRESULT __stdcall get_SWRemote(/*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall put_SWRemote(/*[in]*/ BSTR pVal) = 0;
                virtual HRESULT __stdcall get_FlashVars(/*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall put_FlashVars(/*[in]*/ BSTR pVal) = 0;
                virtual HRESULT __stdcall get_AllowScriptAccess(/*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall put_AllowScriptAccess(/*[in]*/ BSTR pVal) = 0;
                virtual HRESULT __stdcall get_MovieData(/*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall put_MovieData(/*[in]*/ BSTR pVal) = 0;
            };
        }

        namespace v8
        {
            class IShockwaveFlash : public IDispatch
            {
            public:
                static GUID GetGUID()
                {
                    return GetFlashInterfaceGUID();
                }

                virtual HRESULT __stdcall get_ReadyState(/*[out,retval]*/ long * pVal) = 0;
                virtual HRESULT __stdcall get_TotalFrames(/*[out,retval]*/ long * pVal) = 0;
                virtual HRESULT __stdcall get_Playing(/*[out,retval]*/ VARIANT_BOOL * pVal) = 0;
                virtual HRESULT __stdcall put_Playing(/*[in]*/ VARIANT_BOOL pVal) = 0;
                virtual HRESULT __stdcall get_Quality(/*[out,retval]*/ int * pVal) = 0;
                virtual HRESULT __stdcall put_Quality(/*[in]*/ int pVal) = 0;
                virtual HRESULT __stdcall get_ScaleMode(/*[out,retval]*/ int * pVal) = 0;
                virtual HRESULT __stdcall put_ScaleMode(/*[in]*/ int pVal) = 0;
                virtual HRESULT __stdcall get_AlignMode(/*[out,retval]*/ int * pVal) = 0;
                virtual HRESULT __stdcall put_AlignMode(/*[in]*/ int pVal) = 0;
                virtual HRESULT __stdcall get_BackgroundColor(/*[out,retval]*/ long * pVal) = 0;
                virtual HRESULT __stdcall put_BackgroundColor(/*[in]*/ long pVal) = 0;
                virtual HRESULT __stdcall get_Loop(/*[out,retval]*/ VARIANT_BOOL * pVal) = 0;
                virtual HRESULT __stdcall put_Loop(/*[in]*/ VARIANT_BOOL pVal) = 0;
                virtual HRESULT __stdcall get_Movie(/*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall put_Movie(/*[in]*/ BSTR pVal) = 0;
                virtual HRESULT __stdcall get_FrameNum(/*[out,retval]*/ long * pVal) = 0;
                virtual HRESULT __stdcall put_FrameNum(/*[in]*/ long pVal) = 0;
                virtual HRESULT __stdcall SetZoomRect(/*[in]*/ long left,
                        /*[in]*/ long top,
                        /*[in]*/ long right,
                        /*[in]*/ long bottom) = 0;
                virtual HRESULT __stdcall Zoom(/*[in]*/ int factor) = 0;
                virtual HRESULT __stdcall Pan(/*[in]*/ long x,
                        /*[in]*/ long y,
                        /*[in]*/ int mode) = 0;
                virtual HRESULT __stdcall Play() = 0;
                virtual HRESULT __stdcall Stop() = 0;
                virtual HRESULT __stdcall Back() = 0;
                virtual HRESULT __stdcall Forward() = 0;
                virtual HRESULT __stdcall Rewind() = 0;
                virtual HRESULT __stdcall StopPlay() = 0;
                virtual HRESULT __stdcall GotoFrame(/*[in]*/ long FrameNum) = 0;
                virtual HRESULT __stdcall CurrentFrame(/*[out,retval]*/ long * FrameNum) = 0;
                virtual HRESULT __stdcall IsPlaying(/*[out,retval]*/ VARIANT_BOOL * Playing) = 0;
                virtual HRESULT __stdcall PercentLoaded(/*[out,retval]*/ long * percent) = 0;
                virtual HRESULT __stdcall FrameLoaded(/*[in]*/ long FrameNum,
                        /*[out,retval]*/ VARIANT_BOOL * loaded) = 0;
                virtual HRESULT __stdcall FlashVersion(/*[out,retval]*/ long * version) = 0;
                virtual HRESULT __stdcall get_WMode(/*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall put_WMode(/*[in]*/ BSTR pVal) = 0;
                virtual HRESULT __stdcall get_SAlign(/*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall put_SAlign(/*[in]*/ BSTR pVal) = 0;
                virtual HRESULT __stdcall get_Menu(/*[out,retval]*/ VARIANT_BOOL * pVal) = 0;
                virtual HRESULT __stdcall put_Menu(/*[in]*/ VARIANT_BOOL pVal) = 0;
                virtual HRESULT __stdcall get_Base(/*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall put_Base(/*[in]*/ BSTR pVal) = 0;
                virtual HRESULT __stdcall get_Scale(/*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall put_Scale(/*[in]*/ BSTR pVal) = 0;
                virtual HRESULT __stdcall get_DeviceFont(/*[out,retval]*/ VARIANT_BOOL * pVal) = 0;
                virtual HRESULT __stdcall put_DeviceFont(/*[in]*/ VARIANT_BOOL pVal) = 0;
                virtual HRESULT __stdcall get_EmbedMovie(/*[out,retval]*/ VARIANT_BOOL * pVal) = 0;
                virtual HRESULT __stdcall put_EmbedMovie(/*[in]*/ VARIANT_BOOL pVal) = 0;
                virtual HRESULT __stdcall get_BGColor(/*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall put_BGColor(/*[in]*/ BSTR pVal) = 0;
                virtual HRESULT __stdcall get_Quality2(/*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall put_Quality2(/*[in]*/ BSTR pVal) = 0;
                virtual HRESULT __stdcall LoadMovie(/*[in]*/ int layer,
                        /*[in]*/ BSTR url) = 0;
                virtual HRESULT __stdcall TGotoFrame(/*[in]*/ BSTR target,
                        /*[in]*/ long FrameNum) = 0;
                virtual HRESULT __stdcall TGotoLabel(/*[in]*/ BSTR target,
                        /*[in]*/ BSTR label) = 0;
                virtual HRESULT __stdcall TCurrentFrame(/*[in]*/ BSTR target,
                        /*[out,retval]*/ long * FrameNum) = 0;
                virtual HRESULT __stdcall TCurrentLabel(/*[in]*/ BSTR target,
                        /*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall TPlay(/*[in]*/ BSTR target) = 0;
                virtual HRESULT __stdcall TStopPlay(/*[in]*/ BSTR target) = 0;
                virtual HRESULT __stdcall SetVariable(/*[in]*/ BSTR name,
                        /*[in]*/ BSTR value) = 0;
                virtual HRESULT __stdcall GetVariable(/*[in]*/ BSTR name,
                        /*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall TSetProperty(/*[in]*/ BSTR target,
                        /*[in]*/ int property,
                        /*[in]*/ BSTR value) = 0;
                virtual HRESULT __stdcall TGetProperty(/*[in]*/ BSTR target,
                        /*[in]*/ int property,
                        /*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall TCallFrame(/*[in]*/ BSTR target,
                        /*[in]*/ int FrameNum) = 0;
                virtual HRESULT __stdcall TCallLabel(/*[in]*/ BSTR target,
                        /*[in]*/ BSTR label) = 0;
                virtual HRESULT __stdcall TSetPropertyNum(/*[in]*/ BSTR target,
                        /*[in]*/ int property,
                        /*[in]*/ double value) = 0;
                virtual HRESULT __stdcall TGetPropertyNum(/*[in]*/ BSTR target,
                        /*[in]*/ int property,
                        /*[out,retval]*/ double * pVal) = 0;
                virtual HRESULT __stdcall TGetPropertyAsNumber(/*[in]*/ BSTR target,
                        /*[in]*/ int property,
                        /*[out,retval]*/ double * pVal) = 0;
                virtual HRESULT __stdcall get_SWRemote(/*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall put_SWRemote(/*[in]*/ BSTR pVal) = 0;
                virtual HRESULT __stdcall get_FlashVars(/*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall put_FlashVars(/*[in]*/ BSTR pVal) = 0;
                virtual HRESULT __stdcall get_AllowScriptAccess(/*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall put_AllowScriptAccess(/*[in]*/ BSTR pVal) = 0;
                virtual HRESULT __stdcall get_MovieData(/*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall put_MovieData(/*[in]*/ BSTR pVal) = 0;
                virtual HRESULT __stdcall get_InlineData(/*[out,retval]*/ IUnknown * * ppIUnknown) = 0;
                virtual HRESULT __stdcall put_InlineData(/*[in]*/ IUnknown * ppIUnknown) = 0;
                virtual HRESULT __stdcall get_SeamlessTabbing(/*[out,retval]*/ VARIANT_BOOL * pVal) = 0;
                virtual HRESULT __stdcall put_SeamlessTabbing(/*[in]*/ VARIANT_BOOL pVal) = 0;
                virtual HRESULT __stdcall EnforceLocalSecurity() = 0;
                virtual HRESULT __stdcall get_Profile(/*[out,retval]*/ VARIANT_BOOL * pVal) = 0;
                virtual HRESULT __stdcall put_Profile(/*[in]*/ VARIANT_BOOL pVal) = 0;
                virtual HRESULT __stdcall get_ProfileAddress(/*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall put_ProfileAddress(/*[in]*/ BSTR pVal) = 0;
                virtual HRESULT __stdcall get_ProfilePort(/*[out,retval]*/ long * pVal) = 0;
                virtual HRESULT __stdcall put_ProfilePort(/*[in]*/ long pVal) = 0;
                virtual HRESULT __stdcall CallFunction(/*[in]*/ BSTR request,
                        /*[out,retval]*/ BSTR * response) = 0;
                virtual HRESULT __stdcall SetReturnValue(/*[in]*/ BSTR returnValue) = 0;
                virtual HRESULT __stdcall DisableLocalSecurity() = 0;
            };
        }


	// Version 9.0.28.0 or higher
        namespace v9_0_28_0
        {
            class IShockwaveFlash : public IDispatch
            {
            public:
                static GUID GetGUID()
                {
                    return GetFlashInterfaceGUID();
                }

                virtual HRESULT __stdcall get_ReadyState(/*[out,retval]*/ long * pVal) = 0;
                virtual HRESULT __stdcall get_TotalFrames(/*[out,retval]*/ long * pVal) = 0;
                virtual HRESULT __stdcall get_Playing(/*[out,retval]*/ VARIANT_BOOL * pVal) = 0;
                virtual HRESULT __stdcall put_Playing(/*[in]*/ VARIANT_BOOL pVal) = 0;
                virtual HRESULT __stdcall get_Quality(/*[out,retval]*/ int * pVal) = 0;
                virtual HRESULT __stdcall put_Quality(/*[in]*/ int pVal) = 0;
                virtual HRESULT __stdcall get_ScaleMode(/*[out,retval]*/ int * pVal) = 0;
                virtual HRESULT __stdcall put_ScaleMode(/*[in]*/ int pVal) = 0;
                virtual HRESULT __stdcall get_AlignMode(/*[out,retval]*/ int * pVal) = 0;
                virtual HRESULT __stdcall put_AlignMode(/*[in]*/ int pVal) = 0;
                virtual HRESULT __stdcall get_BackgroundColor(/*[out,retval]*/ long * pVal) = 0;
                virtual HRESULT __stdcall put_BackgroundColor(/*[in]*/ long pVal) = 0;
                virtual HRESULT __stdcall get_Loop(/*[out,retval]*/ VARIANT_BOOL * pVal) = 0;
                virtual HRESULT __stdcall put_Loop(/*[in]*/ VARIANT_BOOL pVal) = 0;
                virtual HRESULT __stdcall get_Movie(/*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall put_Movie(/*[in]*/ BSTR pVal) = 0;
                virtual HRESULT __stdcall get_FrameNum(/*[out,retval]*/ long * pVal) = 0;
                virtual HRESULT __stdcall put_FrameNum(/*[in]*/ long pVal) = 0;
                virtual HRESULT __stdcall SetZoomRect(/*[in]*/ long left,
                        /*[in]*/ long top,
                        /*[in]*/ long right,
                        /*[in]*/ long bottom) = 0;
                virtual HRESULT __stdcall Zoom(/*[in]*/ int factor) = 0;
                virtual HRESULT __stdcall Pan(/*[in]*/ long x,
                        /*[in]*/ long y,
                        /*[in]*/ int mode) = 0;
                virtual HRESULT __stdcall Play() = 0;
                virtual HRESULT __stdcall Stop() = 0;
                virtual HRESULT __stdcall Back() = 0;
                virtual HRESULT __stdcall Forward() = 0;
                virtual HRESULT __stdcall Rewind() = 0;
                virtual HRESULT __stdcall StopPlay() = 0;
                virtual HRESULT __stdcall GotoFrame(/*[in]*/ long FrameNum) = 0;
                virtual HRESULT __stdcall CurrentFrame(/*[out,retval]*/ long * FrameNum) = 0;
                virtual HRESULT __stdcall IsPlaying(/*[out,retval]*/ VARIANT_BOOL * Playing) = 0;
                virtual HRESULT __stdcall PercentLoaded(/*[out,retval]*/ long * percent) = 0;
                virtual HRESULT __stdcall FrameLoaded(/*[in]*/ long FrameNum,
                        /*[out,retval]*/ VARIANT_BOOL * loaded) = 0;
                virtual HRESULT __stdcall FlashVersion(/*[out,retval]*/ long * version) = 0;
                virtual HRESULT __stdcall get_WMode(/*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall put_WMode(/*[in]*/ BSTR pVal) = 0;
                virtual HRESULT __stdcall get_SAlign(/*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall put_SAlign(/*[in]*/ BSTR pVal) = 0;
                virtual HRESULT __stdcall get_Menu(/*[out,retval]*/ VARIANT_BOOL * pVal) = 0;
                virtual HRESULT __stdcall put_Menu(/*[in]*/ VARIANT_BOOL pVal) = 0;
                virtual HRESULT __stdcall get_Base(/*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall put_Base(/*[in]*/ BSTR pVal) = 0;
                virtual HRESULT __stdcall get_Scale(/*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall put_Scale(/*[in]*/ BSTR pVal) = 0;
                virtual HRESULT __stdcall get_DeviceFont(/*[out,retval]*/ VARIANT_BOOL * pVal) = 0;
                virtual HRESULT __stdcall put_DeviceFont(/*[in]*/ VARIANT_BOOL pVal) = 0;
                virtual HRESULT __stdcall get_EmbedMovie(/*[out,retval]*/ VARIANT_BOOL * pVal) = 0;
                virtual HRESULT __stdcall put_EmbedMovie(/*[in]*/ VARIANT_BOOL pVal) = 0;
                virtual HRESULT __stdcall get_BGColor(/*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall put_BGColor(/*[in]*/ BSTR pVal) = 0;
                virtual HRESULT __stdcall get_Quality2(/*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall put_Quality2(/*[in]*/ BSTR pVal) = 0;
                virtual HRESULT __stdcall LoadMovie(/*[in]*/ int layer,
                        /*[in]*/ BSTR url) = 0;
                virtual HRESULT __stdcall TGotoFrame(/*[in]*/ BSTR target,
                        /*[in]*/ long FrameNum) = 0;
                virtual HRESULT __stdcall TGotoLabel(/*[in]*/ BSTR target,
                        /*[in]*/ BSTR label) = 0;
                virtual HRESULT __stdcall TCurrentFrame(/*[in]*/ BSTR target,
                        /*[out,retval]*/ long * FrameNum) = 0;
                virtual HRESULT __stdcall TCurrentLabel(/*[in]*/ BSTR target,
                        /*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall TPlay(/*[in]*/ BSTR target) = 0;
                virtual HRESULT __stdcall TStopPlay(/*[in]*/ BSTR target) = 0;
                virtual HRESULT __stdcall SetVariable(/*[in]*/ BSTR name,
                        /*[in]*/ BSTR value) = 0;
                virtual HRESULT __stdcall GetVariable(/*[in]*/ BSTR name,
                        /*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall TSetProperty(/*[in]*/ BSTR target,
                        /*[in]*/ int property,
                        /*[in]*/ BSTR value) = 0;
                virtual HRESULT __stdcall TGetProperty(/*[in]*/ BSTR target,
                        /*[in]*/ int property,
                        /*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall TCallFrame(/*[in]*/ BSTR target,
                        /*[in]*/ int FrameNum) = 0;
                virtual HRESULT __stdcall TCallLabel(/*[in]*/ BSTR target,
                        /*[in]*/ BSTR label) = 0;
                virtual HRESULT __stdcall TSetPropertyNum(/*[in]*/ BSTR target,
                        /*[in]*/ int property,
                        /*[in]*/ double value) = 0;
                virtual HRESULT __stdcall TGetPropertyNum(/*[in]*/ BSTR target,
                        /*[in]*/ int property,
                        /*[out,retval]*/ double * pVal) = 0;
                virtual HRESULT __stdcall TGetPropertyAsNumber(/*[in]*/ BSTR target,
                        /*[in]*/ int property,
                        /*[out,retval]*/ double * pVal) = 0;
                virtual HRESULT __stdcall get_SWRemote(/*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall put_SWRemote(/*[in]*/ BSTR pVal) = 0;
                virtual HRESULT __stdcall get_FlashVars(/*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall put_FlashVars(/*[in]*/ BSTR pVal) = 0;
                virtual HRESULT __stdcall get_AllowScriptAccess(/*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall put_AllowScriptAccess(/*[in]*/ BSTR pVal) = 0;
                virtual HRESULT __stdcall get_MovieData(/*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall put_MovieData(/*[in]*/ BSTR pVal) = 0;
                virtual HRESULT __stdcall get_InlineData(/*[out,retval]*/ IUnknown * * ppIUnknown) = 0;
                virtual HRESULT __stdcall put_InlineData(/*[in]*/ IUnknown * ppIUnknown) = 0;
                virtual HRESULT __stdcall get_SeamlessTabbing(/*[out,retval]*/ VARIANT_BOOL * pVal) = 0;
                virtual HRESULT __stdcall put_SeamlessTabbing(/*[in]*/ VARIANT_BOOL pVal) = 0;
                virtual HRESULT __stdcall EnforceLocalSecurity() = 0;
                virtual HRESULT __stdcall get_Profile(/*[out,retval]*/ VARIANT_BOOL * pVal) = 0;
                virtual HRESULT __stdcall put_Profile(/*[in]*/ VARIANT_BOOL pVal) = 0;
                virtual HRESULT __stdcall get_ProfileAddress(/*[out,retval]*/ BSTR * pVal) = 0;
                virtual HRESULT __stdcall put_ProfileAddress(/*[in]*/ BSTR pVal) = 0;
                virtual HRESULT __stdcall get_ProfilePort(/*[out,retval]*/ long * pVal) = 0;
                virtual HRESULT __stdcall put_ProfilePort(/*[in]*/ long pVal) = 0;
                virtual HRESULT __stdcall CallFunction(/*[in]*/ BSTR request,
                        /*[out,retval]*/ BSTR * response) = 0;
                virtual HRESULT __stdcall SetReturnValue(/*[in]*/ BSTR returnValue) = 0;
                virtual HRESULT __stdcall DisableLocalSecurity() = 0;

				virtual HRESULT __stdcall get_AllowNetworking(/*[out, retval]*/ BSTR* pVal);
				virtual HRESULT __stdcall put_AllowNetworking(/*[in]*/ BSTR pVal);
				virtual HRESULT __stdcall get_AllowFullScreen(/*[out, retval]*/ BSTR* pVal);
				virtual HRESULT __stdcall put_AllowFullScreen(/*[in]*/ BSTR pVal);
            };
        }
    }
}

#endif // !__FLASH_H__916F0C58_0AD7_4dbf_861C_7FD4796D25BF__
