#include "stdafx.h"
#include "ContentManager.h"
#include "MyBindStatusCallback.h"
#include "FlashOCXLoader.h"

using namespace f_in_box;

CMyBindStatusCallback::CMyBindStatusCallback(CContentManager* pContentManager) : 
	m_pContentManager(pContentManager), 
    m_nRefCount(0)
{
}

CMyBindStatusCallback::~CMyBindStatusCallback()
{
}

STDMETHODIMP CMyBindStatusCallback::OnStartBinding(
    /* [in] */ DWORD dwReserved,
    /* [in] */ IBinding *pib)
{
    return m_pLastBindStatusCallback->OnStartBinding(dwReserved, pib);
}

STDMETHODIMP CMyBindStatusCallback::GetPriority( 
    /* [out] */ LONG *pnPriority)
{
    return m_pLastBindStatusCallback->GetPriority(pnPriority);
}

STDMETHODIMP CMyBindStatusCallback::OnLowResource( 
    /* [in] */ DWORD reserved)
{
    return m_pLastBindStatusCallback->OnLowResource(reserved);
}

STDMETHODIMP CMyBindStatusCallback::OnProgress( 
    /* [in] */ ULONG ulProgress,
    /* [in] */ ULONG ulProgressMax,
    /* [in] */ ULONG ulStatusCode,
    /* [in] */ LPCWSTR szStatusText)
{
    return m_pLastBindStatusCallback->OnProgress(ulProgress, ulProgressMax, ulStatusCode, szStatusText);
}

STDMETHODIMP CMyBindStatusCallback::OnStopBinding( 
    /* [in] */ HRESULT hresult,
    /* [unique][in] */ LPCWSTR szError)
{
    return m_pLastBindStatusCallback->OnStopBinding(hresult, szError);
}

 /* [local] */ STDMETHODIMP CMyBindStatusCallback::GetBindInfo( 
    /* [out] */ DWORD *grfBINDF,
    /* [unique][out][in] */ BINDINFO *pbindinfo)
{
    HRESULT hr = m_pLastBindStatusCallback->GetBindInfo(grfBINDF, pbindinfo);

    m_pContentManager->CallBindInfoHandlers(grfBINDF, pbindinfo);

    return hr;
}

 /* [local] */ STDMETHODIMP CMyBindStatusCallback::OnDataAvailable( 
    /* [in] */ DWORD grfBSCF,
    /* [in] */ DWORD dwSize,
    /* [in] */ FORMATETC *pformatetc,
    /* [in] */ STGMEDIUM *pstgmed)
{
    return m_pLastBindStatusCallback->OnDataAvailable(grfBSCF, dwSize, pformatetc, pstgmed);
}

STDMETHODIMP CMyBindStatusCallback::OnObjectAvailable( 
    /* [in] */ REFIID riid,
    /* [iid_is][in] */ IUnknown *punk)
{
    return m_pLastBindStatusCallback->OnObjectAvailable(riid, punk);
}

HRESULT STDMETHODCALLTYPE CMyBindStatusCallback::QueryInterface(REFIID riid, void** ppvObject)
{
    void* pObject = NULL;

    if (f_in_box::com_helpers::IsEqualGUID(IID_IUnknown, riid))
        pObject = this;
    else if (f_in_box::com_helpers::IsEqualGUID(IID_IBindStatusCallback, riid))
        pObject = (IBindStatusCallback*)this;
    else if (f_in_box::com_helpers::IsEqualGUID(IMyBindStatusCallback::GetIID(), riid))
        pObject = (IMyBindStatusCallback*)this;
    else if (f_in_box::com_helpers::IsEqualGUID(IID_IHttpNegotiate, riid))
        pObject = (IHttpNegotiate*)this;

    *ppvObject = pObject;

    HRESULT hr;

    if (NULL != pObject)
    {
        AddRef();

        hr = S_OK;
    }
    else
	{
        if (m_pLastBindStatusCallback.IsNotNull())
            hr = m_pLastBindStatusCallback->QueryInterface(riid, ppvObject);
        else
            hr = E_NOINTERFACE;
    }

    return hr;
}

ULONG STDMETHODCALLTYPE CMyBindStatusCallback::AddRef()
{
    return ++m_nRefCount;
}

ULONG STDMETHODCALLTYPE CMyBindStatusCallback::Release()
{
    ULONG nNewRefCount = --m_nRefCount;

    if (0 == nNewRefCount)
	{
		m_pLastBindStatusCallback = NULL;

		com_helpers::CComPtr<CFlashOCXLoader> pFlashOCXLoader = m_pContentManager->GetFlashOCXLoader();

        delete this;

		pFlashOCXLoader->UnloadIfShould();
	}

    return nNewRefCount;
}

HRESULT CMyBindStatusCallback::CreateInstance(REFIID refiid, void** ppObject, CContentManager* pContentManager)
{
    CMyBindStatusCallback* pObject = new CMyBindStatusCallback(pContentManager);

    HRESULT hr = pObject->QueryInterface(refiid, ppObject);

    if (S_OK != hr)
        delete pObject;

    return hr;
}

HRESULT __stdcall CMyBindStatusCallback::SetLastBindStatusCallback(IBindStatusCallback* pBindStatusCallback)
{
    m_pLastBindStatusCallback = pBindStatusCallback;

    return S_OK;
}
