#include "stdafx.h"
#include "Globals.h"

namespace f_in_box
{
    namespace Globals
    {
        void Init()
        {
            // TODO: check for already initialized?

            g_hHeap = HeapCreate(0, 0, 0);

            // ntdll.dll functions

#define DEF_INIT_NTDLL_FUNCTION_POINTER(name) \
            g_p##name = (P_##name)GetProcAddress(hNtDllModuleHandle, #name); \
            ASSERT(NULL != g_p##name);

#define DEF_INIT_NTDLL_FUNCTION_POINTER_WITH_ANOTHER_REAL_NAME(name, real_name) \
            g_p##name = (P_##name)GetProcAddress(hNtDllModuleHandle, #real_name); \
            ASSERT(NULL != g_p##name);

            HMODULE hNtDllModuleHandle = LoadLibraryW(L"ntdll.dll");
            
            DEF_INIT_NTDLL_FUNCTION_POINTER_WITH_ANOTHER_REAL_NAME(RtlCopyMemory, RtlMoveMemory);
            DEF_INIT_NTDLL_FUNCTION_POINTER(RtlZeroMemory);

#undef DEF_INIT_NTDLL_FUNCTION_POINTER_WITH_ANOTHER_REAL_NAME
#undef DEF_INIT_NTDLL_FUNCTION_POINTER

            // kernel32.dll functions

#define DEF_INIT_KERNEL32_FUNCTION_POINTER(name) \
            g_p##name = (P_##name)GetProcAddress(hKernel32ModuleHandle, #name); \
            ASSERT(NULL != g_p##name);

            HMODULE hKernel32ModuleHandle = LoadLibraryW(L"kernel32.dll");

            DEF_INIT_KERNEL32_FUNCTION_POINTER(TlsSetValue);

#undef DEF_INIT_KERNEL32_FUNCTION_POINTER_WITH_ANOTHER_REAL_NAME
#undef DEF_INIT_KERNEL32_FUNCTION_POINTER

            g_nTlsSlotParam1 = TlsAlloc();
            g_nTlsSlotParam2 = TlsAlloc();
        }

        void Free()
        {
            HeapDestroy(g_hHeap);
            TlsFree(g_nTlsSlotParam1);
            TlsFree(g_nTlsSlotParam2);
        }

        HANDLE g_hHeap;
        DWORD g_nTlsSlotParam1;
        DWORD g_nTlsSlotParam2;
        P_RtlCopyMemory g_pRtlCopyMemory;
        P_RtlZeroMemory g_pRtlZeroMemory;
        P_TlsSetValue g_pTlsSetValue;
    }
}
