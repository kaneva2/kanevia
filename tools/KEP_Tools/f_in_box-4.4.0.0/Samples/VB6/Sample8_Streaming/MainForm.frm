VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form MainForm 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Sample 8 - Secure flash content loading"
   ClientHeight    =   6165
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8700
   Icon            =   "MainForm.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   411
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   580
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton PlayBtn 
      Caption         =   "Play crypted SWF / FLV..."
      Height          =   375
      Left            =   6360
      TabIndex        =   6
      Top             =   1680
      Width           =   2175
   End
   Begin VB.Timer Timer 
      Interval        =   100
      Left            =   7200
      Top             =   4080
   End
   Begin MSComDlg.CommonDialog CommonDialog 
      Left            =   7200
      Top             =   4560
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton CryptBtn 
      Caption         =   "Crypt SWF / FLV..."
      Height          =   375
      Left            =   6360
      TabIndex        =   5
      Top             =   1200
      Width           =   2175
   End
   Begin VB.CommandButton OrderBtn 
      Caption         =   "Buy license"
      Height          =   375
      Left            =   6360
      TabIndex        =   2
      Top             =   5640
      Width           =   2175
   End
   Begin VB.CommandButton MoreAboutBtn 
      Caption         =   "More about F-IN-BOX"
      Height          =   375
      Left            =   6360
      TabIndex        =   1
      Top             =   5160
      Width           =   2175
   End
   Begin VB.Label Label2 
      Caption         =   $"MainForm.frx":548A
      Height          =   375
      Left            =   120
      TabIndex        =   4
      Top             =   600
      Width           =   8535
   End
   Begin VB.Label Label1 
      Caption         =   $"MainForm.frx":552E
      Height          =   495
      Left            =   120
      TabIndex        =   3
      Top             =   120
      Width           =   8535
   End
   Begin VB.Label PlaceHolder 
      Height          =   4860
      Left            =   120
      TabIndex        =   0
      Top             =   1095
      Visible         =   0   'False
      Width           =   6060
   End
End
Attribute VB_Name = "MainForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' IStream helpers

Private Declare Function FPC_IStream_AddRef Lib "f_in_box.dll" _
    (ByVal pStream As Long) As Long
    
Private Declare Function FPC_IStream_Release Lib "f_in_box.dll" _
    (ByVal pStream As Long) As Long
    
Private Declare Function FPC_IStream_Write Lib "f_in_box.dll" _
    (ByVal pStream As Long, _
    pData As Any, _
    ByVal nSize As Long, _
    ByRef nWritten As Long) As Long

'Win32 API
Private Declare Function ShellExecute Lib "shell32" _
   Alias "ShellExecuteA" _
  (ByVal hwnd As Long, _
   ByVal lpOperation As String, _
   ByVal lpFile As String, _
   ByVal lpParameters As String, _
   ByVal lpDirectory As String, _
   ByVal nShowCmd As Long) As Long
   
Private Declare Function FPC_CreateWindow Lib "f_in_box.dll" _
   Alias "FPC_CreateWindowA" _
  (ByVal hFPC As Long, _
   ByVal dwExStyle As Long, _
   ByVal lpWindowName As String, _
   ByVal dwStyle As Long, _
   ByVal x As Long, ByVal y As Long, _
   ByVal nWidth As Long, _
   ByVal nHeight As Long, _
   ByVal hWndParent As Long, _
   ByVal hMenu As Long, _
   ByVal hInstance As Long, _
   lpParam As Any) As Long
   
'Win32 constants
Private Const WS_VISIBLE As Long = &H10000000
Private Const WS_CHILD As Long = &H40000000
Private Const WS_CLIPSIBLINGS As Long = &h4000000
Private Const WM_USER As Long = &H400
Private Const SW_SHOW = 5

'FlashPlayerControl API
Private Declare Function FPCIsFlashInstalled Lib "f_in_box.dll" _
    () As Long

Private Declare Function FPC_LoadRegisteredOCX Lib "f_in_box.dll" _
    () As Long

Private Declare Function FPC_UnloadCode Lib "f_in_box.dll" _
    (ByVal hFPC As Long) As Long

Private Declare Function FPC_AddOnLoadExternalResourceHandler Lib "f_in_box.dll" _
    Alias "FPC_AddOnLoadExternalResourceHandlerA" _
    (ByVal hFPC As Long, _
     ByVal dwPtr As Long, _
     ByVal lParam As Long) As Long

Private Declare Function FPC_RemoveOnLoadExternalResourceHandler Lib "f_in_box.dll" _
    (ByVal hFPC As Long, _
     ByVal dwCookie As Long) As Long
    
Private Declare Function FPCPutMovieUsingStream Lib "f_in_box.dll" _
   (ByVal hwndFlashPlayerControl As Long, _
    ByRef ppStream As Long) As Boolean
    
Private Declare Function FPCPutMovieFromResource Lib "f_in_box.dll" _
    Alias "FPCPutMovieFromResourceA" _
    (ByVal hwndFlashPlayerControl As Long, _
    ByVal hInstance As Long, _
    ByVal lpName As String, _
    ByVal lpType As String) As Boolean
    
'FlashPlayerControl constants

Dim hFPC As Long
Dim nHandlerCookie As Long
Private FlashPlayerControlWnd As Long
Private StrFLVPath As String
Private Unpacker As CUnpacker
Private MediaDir As String

Public Property Get FlashPlayerControlVal() As Long
    FlashPlayerControlVal = FlashPlayerControlWnd
End Property

Public Property Get UnpackerRef() As CUnpacker
    Set UnpackerRef = Unpacker
End Property

Public Property Get FLVPath() As String
    FLVPath = StrFLVPath
End Property

Private Sub Form_Load()
    Initialize
End Sub

Private Sub Initialize()
    
    'Call the FindMediaDir procedure
    FindMediaDir
    
    'Prepare FlashPlayerControl
    
    If FPCIsFlashInstalled = 0 Then
        MsgBox ("The application needs Flash" + Chr$(13) + Chr$(10) + "Flash is not installed")
        End
    End If
    
    hFPC = FPC_LoadRegisteredOCX

    If hFPC = 0 Then
        MsgBox ("FPC_LoadRegisteredOCX() failed")
        End
    End If
   
    'Create flash player window
    FlashPlayerControlWnd = FPC_CreateWindow( _
                                        hFPC, _
                                        0, _
                                        vbNullString, _
                                        WS_CHILD Or WS_VISIBLE Or WS_CLIPSIBLINGS, _
                                        PlaceHolder.Left, PlaceHolder.Top, _
                                        PlaceHolder.Width, _
                                        PlaceHolder.Height, _
                                        hwnd, 0, _
                                        App.hInstance, _
                                        ByVal 0)
    
    'Set callback to FlashPlayerControl
    nHandlerCookie = FPC_AddOnLoadExternalResourceHandler(hFPC, AddressOf GlobalOnLoadExternalResourceHandler, 0)
    
    'Create unpacker object
    Set Unpacker = New CUnpacker
End Sub

Private Sub FindMediaDir()
    MediaDir = CurDir
End Sub

Private Sub CryptBtn_Click()
    Dim strInputFile As String
    Dim strOutputFile As String

    MsgBox "Select flash movie or flash video file for crypting"
    
    CommonDialog.CancelError = True
    On Error GoTo ErrHandler
    
    CommonDialog.DefaultExt = "swf"
    CommonDialog.FileName = ""
    CommonDialog.Filter = "Flash files (*.swf;*.flv)|*.swf;*.flv|All Files (*.*)|*.*||"
    CommonDialog.Flags = 0
    CommonDialog.ShowOpen
    If (Len(CommonDialog.FileName) > 0) Then
        strInputFile = CommonDialog.FileName
        
        MsgBox "Select where to save a crypted file"

        CommonDialog.DefaultExt = "crypted"
        CommonDialog.FileName = strInputFile & ".crypted"
        CommonDialog.Filter = "Crypted Flash files (*.crypted)|*.crypted;*.crypted|All Files (*.*)|*.*||"
        CommonDialog.Flags = cdlOFNHideReadOnly Or cdlOFNOverwritePrompt
        CommonDialog.ShowSave
        
        If (Len(CommonDialog.FileName) > 0) Then
            strOutputFile = CommonDialog.FileName
            
            Dim mbResult As VbMsgBoxResult
            mbResult = MsgBox("This is a demo application. That's why the crypting is very simple (XOR)" & Chr$(13) + Chr$(10) & "Press OK to start", vbOKCancel)
            If mbResult = vbOK Then
                Dim hFileInput As Long
                Dim hFileOutput As Long
                
                hFileInput = CreateFile(strInputFile, GENERIC_READ, FILE_SHARE_READ, 0, OPEN_EXISTING, 0, 0)
                hFileOutput = CreateFile(strOutputFile, GENERIC_WRITE, FILE_SHARE_WRITE, 0, CREATE_ALWAYS, 0, 0)
                
                Const nBufferSize As Long = 1024
                Dim pBuffer(nBufferSize) As Byte
                Dim dwNumberOfBytesWritten As Long
                Dim dwNumberOfBytesRead As Long
                Dim bRes As Boolean
            
                Do While True
                    bRes = ReadFile(hFileInput, pBuffer(0), nBufferSize, dwNumberOfBytesRead, 0)
                    If Not bRes Or dwNumberOfBytesRead = 0 Then
                        GoTo HCloseHandle
                    End If
                    ' Very-very simple crypting
                    Dim I As Long
                    For I = 0 To dwNumberOfBytesRead Step 1
                        pBuffer(I) = pBuffer(I) Xor &H6
                    Next
                    'Write data to file
                    WriteFile hFileOutput, pBuffer(0), dwNumberOfBytesRead, dwNumberOfBytesWritten, 0
                Loop
           
HCloseHandle:
                CloseHandle hFileInput
                CloseHandle hFileOutput
                
                mbResult = MsgBox("The crypting finished." & Chr$(13) + Chr$(10) & "Would you like to open the crypted file and play it?", vbYesNo Or vbDefaultButton1)
                If mbResult = vbYes Then
                    PlayCryptedFlashFile strOutputFile
                End If
            End If
        End If
    End If
    
ErrHandler:
    CommonDialog.CancelError = False
    'User pressed the Cancel button
End Sub

Private Sub Form_Terminate()
    If hFPC <> 0 Then
        FPC_RemoveOnLoadExternalResourceHandler hFPC, nHandlerCookie
        FPC_UnloadCode hFPC
    End If
End Sub

Private Sub PlayBtn_Click()
    CommonDialog.DefaultExt = "crypted"
    CommonDialog.FileName = ""
    CommonDialog.Filter = "Crypted Flash files (*.crypted)|*.crypted|All Files (*.*)|*.*||"
    CommonDialog.Flags = 0
    CommonDialog.ShowOpen
    If (Len(CommonDialog.FileName) > 0) Then
        PlayCryptedFlashFile CommonDialog.FileName
    End If
End Sub

Private Sub MoreAboutBtn_Click()
    ShellExecute hwnd, vbNullString, "http://www.f-in-box.com/dll/", vbNullString, vbNullString, SW_SHOW
End Sub

Private Sub OrderBtn_Click()
    ShellExecute hwnd, vbNullString, "http://www.f-in-box.com/dll/order.html", vbNullString, vbNullString, SW_SHOW
End Sub

Private Sub PlayCryptedFlashFile(lpszPath As String)
    'Read and decrypt first three bytes to determinate the type of this file (flash movie or flash video)
    Dim hFile As Long
    hFile = CreateFile(lpszPath, GENERIC_READ, FILE_SHARE_READ, 0, OPEN_EXISTING, 0, 0)
    
    Const nBufferSize As Long = 1024
    Dim pBuffer(nBufferSize) As Byte
    Dim dwNumberOfBytesRead As Long
    
    ReadFile hFile, pBuffer(0), nBufferSize, dwNumberOfBytesRead, 0
    CloseHandle hFile
    
    'Decrypt
    Dim I As Long
    For I = 0 To dwNumberOfBytesRead Step 1
        pBuffer(I) = pBuffer(I) Xor &H6
    Next
    
    Const cF As Byte = &H46
    Const cL As Byte = &H4C
    Const cV As Byte = &H56
     
    If pBuffer(0) = cF And pBuffer(1) = cL And pBuffer(2) = cV Then
        'Flash video
        StrFLVPath = lpszPath
        Call FPCPutMovieFromResource(FlashPlayerControlWnd, 0, "FLVPLAYER", "SWF")
    Else
        'Flash movie
        Dim pStream As Long ' IStream
        Call FPCPutMovieUsingStream(FlashPlayerControlWnd, pStream)
        Unpacker.Run pStream, lpszPath
    End If
End Sub

Private Sub Timer_Timer()
    If Unpacker.IsRunning Then
        Unpacker.Tick
    End If
End Sub
