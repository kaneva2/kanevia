VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form MainForm 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Sample 6 - external resources embedding"
   ClientHeight    =   6615
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8700
   Icon            =   "MainForm.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   441
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   580
   StartUpPosition =   3  'Windows Default
   Begin MSComDlg.CommonDialog CommonDialog 
      Left            =   240
      Top             =   6000
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton OrderBtn 
      Caption         =   "Buy license"
      Height          =   375
      Left            =   6600
      TabIndex        =   2
      Top             =   6120
      Width           =   1935
   End
   Begin VB.CommandButton MoreAboutBtn 
      Caption         =   "More about F-IN-BOX"
      Height          =   375
      Left            =   4080
      TabIndex        =   1
      Top             =   6120
      Width           =   2415
   End
   Begin VB.Label Label2 
      Caption         =   $"MainForm.frx":548A
      Height          =   615
      Left            =   120
      TabIndex        =   4
      Top             =   720
      Width           =   8535
   End
   Begin VB.Label Label1 
      Caption         =   $"MainForm.frx":557D
      Height          =   615
      Left            =   120
      TabIndex        =   3
      Top             =   120
      Width           =   8535
   End
   Begin VB.Label PlaceHolder 
      Height          =   4500
      Left            =   120
      TabIndex        =   0
      Top             =   1440
      Visible         =   0   'False
      Width           =   8460
   End
End
Attribute VB_Name = "MainForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Win32 API
Private Declare Function ShellExecute Lib "shell32" _
   Alias "ShellExecuteA" _
  (ByVal hwnd As Long, _
   ByVal lpOperation As String, _
   ByVal lpFile As String, _
   ByVal lpParameters As String, _
   ByVal lpDirectory As String, _
   ByVal nShowCmd As Long) As Long
   
Private Declare Function FPC_CreateWindow Lib "f_in_box.dll" _
   Alias "FPC_CreateWindowA" _
  (ByVal hFPC As Long, _
   ByVal dwExStyle As Long, _
   ByVal lpWindowName As String, _
   ByVal dwStyle As Long, _
   ByVal x As Long, ByVal y As Long, _
   ByVal nWidth As Long, _
   ByVal nHeight As Long, _
   ByVal hWndParent As Long, _
   ByVal hMenu As Long, _
   ByVal hInstance As Long, _
   lpParam As Any) As Long
   
Private Declare Function SetWindowLong Lib "user32" _
    Alias "SetWindowLongA" _
   (ByVal hwnd As Long, _
    ByVal nIndex As Long, _
    ByVal wNewLong As Long) As Long
   
Private Declare Function SendMessage Lib "user32" _
   Alias "SendMessageA" _
  (ByVal hwnd As Long, _
   ByVal wMsg As Long, _
   ByVal wParam As Long, _
   lParam As Any) As Long

'Win32 constants
Private Const WS_VISIBLE As Long = &H10000000
Private Const WS_CHILD As Long = &H40000000
Private Const WS_CLIPSIBLINGS As Long = &h4000000
Private Const WM_USER As Long = &H400
Private Const SW_SHOW = 5

Private Const GWL_WNDPROC = (-4)
Private Const S_OK As Long = &H0

'FlashPlayerControl API
Private Declare Function FPCIsFlashInstalled Lib "f_in_box.dll" _
    () As Long

Private Declare Function FPC_LoadRegisteredOCX Lib "f_in_box.dll" _
    () As Long

Private Declare Function FPC_UnloadCode Lib "f_in_box.dll" _
    (ByVal hFPC As Long) As Long

Private Declare Function FPCPutMovieFromResource Lib "f_in_box.dll" _
    Alias "FPCPutMovieFromResourceA" _
    (ByVal hwndFlashPlayerControl As Long, _
    ByVal hInstance As Long, _
    ByVal lpName As String, _
    ByVal lpType As String) As Boolean

'FlashPlayerControl constants
Private Const FPCM_FIRST As Long = WM_USER + &H1000
Private Const FPCM_PUT_MOVIE As Long = FPCM_FIRST + 317

'Struct SFPCPutMovieA
Private Type SFPCPutMovieA
    lpszBuffer As String
    hr As Long
End Type

Private hFPC As Long
Private FlashPlayerControlWnd As Long
Private PrevWindowProc As Long
Private MediaDir As String

Public Property Get fpc() As Long
   fpc = hFPC
End Property

Public Property Get PrevWindowProcVal() As Long
    PrevWindowProcVal = PrevWindowProc
End Property

Public Property Get FlashPlayerControlVal() As Long
    FlashPlayerControlVal = FlashPlayerControlWnd
End Property

Private Sub Form_Load()
    Initialize
End Sub

Private Sub Initialize()
    
    'Call the FindMediaDir procedure
    FindMediaDir
    
    'Prepare FlashPlayerControl
    If FPCIsFlashInstalled = 0 Then
        MsgBox ("The application needs Flash" + Chr$(13) + Chr$(10) + "Flash is not installed")
    End If
    
    hFPC = FPC_LoadRegisteredOCX

    If hFPC = 0 Then
        MsgBox ("FPC_LoadRegisteredOCX() failed")
        End
    End If
   
    'Create flash player window
    FlashPlayerControlWnd = FPC_CreateWindow( _
                                        hFPC, _
                                        0, _
                                        vbNullString, _
                                        WS_CHILD Or WS_VISIBLE Or WS_CLIPSIBLINGS, _
                                        PlaceHolder.Left, PlaceHolder.Top, _
                                        PlaceHolder.Width, _
                                        PlaceHolder.Height, _
                                        hwnd, 0, _
                                        App.hInstance, _
                                        ByVal 0)
                                        
    'Subclass window
    PrevWindowProc = SetWindowLong(hwnd, GWL_WNDPROC, AddressOf MainFormWindowProc)
    'Play movie from resource
    Call FPCPutMovieFromResource(FlashPlayerControlWnd, 0, "MOVIE", "SWF")
End Sub

Private Sub FindMediaDir()
    MediaDir = App.Path
End Sub

Private Sub Form_Terminate()
   If hFPC <> 0 Then
    FPC_UnloadCode hFPC
   End If
End Sub

Private Sub MoreAboutBtn_Click()
    ShellExecute hwnd, vbNullString, "http://www.f-in-box.com/dll/", vbNullString, vbNullString, SW_SHOW
End Sub

Private Sub OrderBtn_Click()
    ShellExecute hwnd, vbNullString, "http://www.f-in-box.com/dll/order.html", vbNullString, vbNullString, SW_SHOW
End Sub


