VERSION 5.00
Begin VB.Form MainForm 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Sample4 - Translucency"
   ClientHeight    =   3240
   ClientLeft      =   150
   ClientTop       =   720
   ClientWidth     =   6150
   Icon            =   "MainForm.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   216
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   410
   StartUpPosition =   3  'Windows Default
   Visible         =   0   'False
   Begin VB.Menu ContextMenu 
      Caption         =   "ContextMenu"
      Begin VB.Menu Minimize 
         Caption         =   "Minimize"
      End
      Begin VB.Menu Close 
         Caption         =   "Close"
      End
      Begin VB.Menu Separator1 
         Caption         =   "-"
      End
      Begin VB.Menu MoreAboutFlashPlayerControl 
         Caption         =   "More about F-IN-BOX"
      End
      Begin VB.Menu OrderFlashPlayerControl 
         Caption         =   "Buy license"
      End
      Begin VB.Menu VisitOurForum 
         Caption         =   "Visit our forum!"
      End
   End
End
Attribute VB_Name = "MainForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'FlashPlayerControl API
Private Declare Function FPCIsFlashInstalled Lib "f_in_box.dll" _
    () As Long

Private Declare Function FPC_LoadRegisteredOCX Lib "f_in_box.dll" _
    () As Long
    
Private Declare Function FPC_UnloadCode Lib "f_in_box.dll" _
    (ByVal hFPC As Long) As Long
    
Private Declare Function FPCIsTransparentAvailable Lib "f_in_box.dll" _
    () As Long

Private hFPC As Long
Private MediaDir As String
Private FlashPlayerControl As TransparentFPC

Public Property Get FlashPlayerControlRef() As TransparentFPC
    Set FlashPlayerControlRef = FlashPlayerControl
End Property

Private Sub Form_Load()
    Initialize
End Sub

Private Sub Initialize()
    'Call the FindMediaDir procedure
    FindMediaDir
    
    'Prepare FlashPlayerControl
    If FPCIsFlashInstalled = 0 Then
        MsgBox ("The application needs Flash" + Chr$(13) + Chr$(10) + "Flash is not installed")
    End If
        
    If FPCIsTransparentAvailable = 0 Then
        MsgBox ("Flash Transparent is not available")
    End If
    
    hFPC = FPC_LoadRegisteredOCX

    If hFPC = 0 Then
        MsgBox ("FPC_LoadRegisteredOCX() failed")
        End
    End If
    
    'FlashPlayerControl creating
    Set FlashPlayerControl = New TransparentFPC
    FlashPlayerControl.hFPC = hFPC
    FlashPlayerControl.Create
    FlashPlayerControl.SubclassWindow
End Sub

Private Sub FindMediaDir()
    MediaDir = App.Path
End Sub

Private Sub Close_Click()
    Set FlashPlayerControl = Nothing
    Unload Me
End Sub

Private Sub Minimize_Click()
    FlashPlayerControl.Minimize
End Sub

Private Sub MoreAboutFlashPlayerControl_Click()
    FlashPlayerControl.MoreAboutFlashPlayerControl
End Sub

Private Sub OrderFlashPlayerControl_Click()
    FlashPlayerControl.OrderFlashPlayerControl
End Sub

Private Sub VisitOurForum_Click()
    FlashPlayerControl.VisitOurForum
End Sub
