VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form MainForm 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Sample 3 - Standalone Player (how to embed Flash.ocx)"
   ClientHeight    =   6030
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8700
   Icon            =   "MainForm.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   402
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   580
   StartUpPosition =   3  'Windows Default
   Begin MSComDlg.CommonDialog CommonDialog 
      Left            =   8040
      Top             =   4920
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton OrderBtn 
      Caption         =   "Buy license"
      Height          =   375
      Left            =   6600
      TabIndex        =   5
      Top             =   5520
      Width           =   1935
   End
   Begin VB.CommandButton MoreAboutBtn 
      Caption         =   "More about F-IN-BOX"
      Height          =   375
      Left            =   4080
      TabIndex        =   4
      Top             =   5520
      Width           =   2415
   End
   Begin VB.CheckBox EnableSoundsCheck 
      Caption         =   "Enable sounds"
      Height          =   375
      Left            =   3240
      TabIndex        =   3
      Top             =   5040
      Value           =   1  'Checked
      Width           =   3015
   End
   Begin VB.CheckBox StandardMenuCheck 
      Caption         =   "Enable standard context flash menu"
      Height          =   375
      Left            =   120
      TabIndex        =   2
      Top             =   5040
      Width           =   3015
   End
   Begin VB.CommandButton PlaySWFBtn 
      Caption         =   "Play SWF from file..."
      Height          =   375
      Left            =   120
      TabIndex        =   1
      Top             =   5520
      Width           =   1815
   End
   Begin VB.Label PlaceHolder 
      Height          =   4680
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Visible         =   0   'False
      Width           =   8340
   End
End
Attribute VB_Name = "MainForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Win32 API
Private Declare Function ShellExecute Lib "shell32" _
   Alias "ShellExecuteA" _
  (ByVal hWnd As Long, _
   ByVal lpOperation As String, _
   ByVal lpFile As String, _
   ByVal lpParameters As String, _
   ByVal lpDirectory As String, _
   ByVal nShowCmd As Long) As Long
   
Private Declare Function FPC_CreateWindow Lib "f_in_box.dll" _
   Alias "FPC_CreateWindowA" _
  (ByVal hFPC As Long, _
   ByVal dwExStyle As Long, _
   ByVal lpWindowName As String, _
   ByVal dwStyle As Long, _
   ByVal x As Long, ByVal y As Long, _
   ByVal nWidth As Long, _
   ByVal nHeight As Long, _
   ByVal hWndParent As Long, _
   ByVal hMenu As Long, _
   ByVal hInstance As Long, _
   lpParam As Any) As Long
   
Private Declare Function SendMessage Lib "user32" _
   Alias "SendMessageA" _
  (ByVal hWnd As Long, _
   ByVal wMsg As Long, _
   ByVal wParam As Long, _
   lParam As Any) As Long
   
Private Declare Function GetModuleHandle Lib "kernel32" _
    Alias "GetModuleHandleA" _
   (ByVal ApplicationFileName As String) As Integer
   
Private Declare Function FindResource Lib "kernel32" _
    Alias "FindResourceA" _
   (ByVal hInstance As Long, _
    ByVal lpName As String, _
    ByVal lpType As String) As Long
   
Private Declare Function LoadResource Lib "kernel32" _
   (ByVal hInstance As Long, _
    ByVal hResInfo As Long) As Long
   
Private Declare Function LockResource Lib "kernel32" _
   (ByVal hResData As Long) As Long
   
Private Declare Function SizeofResource Lib "kernel32" _
   (ByVal hInstance As Long, _
    ByVal hResInfo As Long) As Long

'Win32 constants
Private Const WS_VISIBLE As Long = &H10000000
Private Const WS_CHILD As Long = &H40000000
Private Const WS_CLIPSIBLINGS As Long = &h4000000
Private Const WM_USER As Long = &H400
Private Const SW_SHOW = 5

'FlashPlayerControl API
Private Declare Function FPCIsFlashInstalled Lib "f_in_box.dll" _
    () As Long

Private Declare Function FPC_LoadOCXCodeFromMemory Lib "f_in_box.dll" _
    (ByVal lpData As Long, _
     ByVal dwSize As Long) As Long

Private Declare Function FPC_UnloadCode Lib "f_in_box.dll" _
    (ByVal hFPC As Long) As Long

Private Declare Function FPCPutMovieFromResource Lib "f_in_box.dll" _
    Alias "FPCPutMovieFromResourceA" _
    (ByVal hwndFlashPlayerControl As Long, _
    ByVal hInstance As Long, _
    ByVal lpName As String, _
    ByVal lpType As String) As Boolean

Private Declare Sub FPC_EnableSound Lib "f_in_box.dll" _
    (ByVal hFPC As Long, _
     ByVal bEnable As Boolean)

'FlashPlayerControl constants
Private Const FPCM_FIRST As Long = WM_USER + &H1000
Private Const FPCM_PUT_MOVIE As Long = FPCM_FIRST + 317
Private Const FPCM_PUT_STANDARD_MENU As Long = FPCM_FIRST + 5

'Struct SFPCPutMovieA
Private Type SFPCPutMovieA
    lpszBuffer As String
    hr As Long
End Type

'Struct SFPCPutStandardMenu
Private Type SFPCPutStandardMenu
    StandardMenu As Boolean
End Type

Dim hFPC As Long
Dim FlashPlayerControlWnd As Long
Dim MediaDir As String

Private Sub Form_Load()
    Initialize
End Sub

Private Sub Initialize()
    'Call the FindMediaDir procedure
    FindMediaDir
    
    'Prepare FlashPlayerControl
    Dim hModule As Long
    Dim hResInfo As Long
    Dim hResData As Long
    Dim lpFlashOCXCode As Long
    Dim dwFlashOCXCodeSize As Long
    
    hModule = GetModuleHandle(vbNullString)
    hResInfo = FindResource(hModule, "FLASH_OCX_CODE", "BIN")
    hResData = LoadResource(hModule, hResInfo)
    lpFlashOCXCode = LockResource(hResData)
    dwFlashOCXCodeSize = SizeofResource(hModule, hResInfo)
    
    If hResData = 0 Then
        MsgBox ("flash ocx code not found")
        End
    End If
    
    hFPC = FPC_LoadOCXCodeFromMemory(lpFlashOCXCode, dwFlashOCXCodeSize)
    
    If hFPC = 0 Then
        MsgBox ("FPC_LoadOCXCodeFromMemory() failed")
        End
    End If
    
    'Create flash player window
    FlashPlayerControlWnd = FPC_CreateWindow( _
                                        hFPC, _
                                        0, _
                                        vbNullString, _
                                        WS_CHILD Or WS_VISIBLE Or WS_CLIPSIBLINGS, _
                                        PlaceHolder.Left, PlaceHolder.Top, _
                                        PlaceHolder.Width, _
                                        PlaceHolder.Height, _
                                        hWnd, 0, _
                                        App.hInstance, _
                                        ByVal 0)
                                        
    'Loading movie from resource
    'Also you can use the message FPCM_PUTMOVIEFROMMEMORY
    Call FPCPutMovieFromResource(FlashPlayerControlWnd, 0&, "MOVIE", "SWF")
End Sub

Private Sub FindMediaDir()
    MediaDir = App.Path
End Sub

Private Sub Form_Terminate()
   If hFPC <> 0 Then
    FPC_UnloadCode hFPC
   End If
End Sub

Private Sub MoreAboutBtn_Click()
    ShellExecute hWnd, vbNullString, "http://www.f-in-box.com/dll/", vbNullString, vbNullString, SW_SHOW
End Sub

Private Sub OrderBtn_Click()
    ShellExecute hWnd, vbNullString, "http://www.f-in-box.com/dll/order.html", vbNullString, vbNullString, SW_SHOW
End Sub

Private Sub PlaySWFBtn_Click()
    CommonDialog.DefaultExt = "swf"
    CommonDialog.Filter = "Flash movie files (*.swf)|*.swf|All Files (*.*)|*.*||"
    CommonDialog.ShowOpen
    If (Len(CommonDialog.FileName) > 0) Then
        Dim FPCPutMovie As SFPCPutMovieA
        FPCPutMovie.lpszBuffer = CommonDialog.FileName
        Call SendMessage(FlashPlayerControlWnd, FPCM_PUT_MOVIE, 0&, FPCPutMovie)
    End If
End Sub

Private Sub StandardMenuCheck_Click()
    Dim FPCPutStandardMenu As SFPCPutStandardMenu
    FPCPutStandardMenu.StandardMenu = StandardMenuCheck.Value = 1
    Call SendMessage(FlashPlayerControlWnd, FPCM_PUT_STANDARD_MENU, 0&, FPCPutStandardMenu)
End Sub

Private Sub EnableSoundsCheck_Click()
    FPC_EnableSound hFPC, EnableSoundsCheck.Value = 1
End Sub
