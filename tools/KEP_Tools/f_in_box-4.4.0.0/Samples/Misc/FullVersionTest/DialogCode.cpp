// DialogCode.cpp : implementation file
//

#include "stdafx.h"
#include "fullversiontest.h"
#include "DialogCode.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDialogCode dialog


CDialogCode::CDialogCode(CWnd* pParent /*=NULL*/)
	: CDialog(CDialogCode::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDialogCode)
	m_strUnlockKey = _T("");
	//}}AFX_DATA_INIT
}


void CDialogCode::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDialogCode)
	DDX_Text(pDX, IDC_EDIT1, m_strUnlockKey);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDialogCode, CDialog)
	//{{AFX_MSG_MAP(CDialogCode)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDialogCode message handlers
