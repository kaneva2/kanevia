// FullVersionTest.h : main header file for the FullVersionTest application
//

#if !defined(AFX_FullVersionTest_H__4825FEC0_BC0D_44A8_A5EE_3E40623A4E7E__INCLUDED_)
#define AFX_FullVersionTest_H__4825FEC0_BC0D_44A8_A5EE_3E40623A4E7E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CFullVersionTestApp:
// See FullVersionTest.cpp for the implementation of this class
//

class CFullVersionTestApp : public CWinApp
{
public:

	HFPC m_hFPC;

public:
	CFullVersionTestApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFullVersionTestApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CFullVersionTestApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

extern CFullVersionTestApp theApp;

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FullVersionTest_H__4825FEC0_BC0D_44A8_A5EE_3E40623A4E7E__INCLUDED_)
