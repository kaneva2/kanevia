#if !defined(AFX_DIALOGCODE_H__1572DA2B_3F74_480F_9CB6_5181C446D2DE__INCLUDED_)
#define AFX_DIALOGCODE_H__1572DA2B_3F74_480F_9CB6_5181C446D2DE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DialogCode.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDialogCode dialog

class CDialogCode : public CDialog
{
// Construction
public:
	CDialogCode(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDialogCode)
	enum { IDD = IDD_DIALOG_CODE };
	CString	m_strUnlockKey;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDialogCode)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDialogCode)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DIALOGCODE_H__1572DA2B_3F74_480F_9CB6_5181C446D2DE__INCLUDED_)
