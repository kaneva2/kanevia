// Sample02_SWF_FLV_EmbeddingDlg.h : header file
//

#if !defined(AFX_Sample02_SWF_FLV_EmbeddingDLG_H__B27E4747_654A_4EFC_BF67_E8245BE2F540__INCLUDED_)
#define AFX_Sample02_SWF_FLV_EmbeddingDLG_H__B27E4747_654A_4EFC_BF67_E8245BE2F540__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CSample02_SWF_FLV_EmbeddingDlg dialog

class CSample02_SWF_FLV_EmbeddingDlg : public CDialog
{
private:

	HWND m_hwndFlashPlayerControl;

	DWORD m_dwHandlerCookie;

private:

	static HRESULT WINAPI StaticGlobalOnLoadExternalResourceHandler(LPCTSTR lpszURL, IStream** ppStream, HFPC hFPC, LPARAM lParam)
	{
		CSample02_SWF_FLV_EmbeddingDlg* pThis = (CSample02_SWF_FLV_EmbeddingDlg*)lParam;

		return pThis->GlobalOnLoadExternalResourceHandler(lpszURL, ppStream, hFPC);
	}

	HRESULT WINAPI GlobalOnLoadExternalResourceHandler(LPCTSTR lpszURL, IStream** ppStream, HFPC hFPC);

// Construction
public:
	CSample02_SWF_FLV_EmbeddingDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CSample02_SWF_FLV_EmbeddingDlg)
	enum { IDD = IDD_Sample02_SWF_FLV_Embedding_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSample02_SWF_FLV_EmbeddingDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	virtual void PostNcDestroy();
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CSample02_SWF_FLV_EmbeddingDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnPlayEmbeddedSwf();
	afx_msg void OnPlayEmbeddedFlv();
	afx_msg void OnSite();
	afx_msg void OnOrder();
	afx_msg void OnCheckStandardMenu();
	afx_msg void OnCheckSounds();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_Sample02_SWF_FLV_EmbeddingDLG_H__B27E4747_654A_4EFC_BF67_E8245BE2F540__INCLUDED_)
