// Sample02_SWF_FLV_EmbeddingDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Sample02_SWF_FLV_Embedding.h"
#include "Sample02_SWF_FLV_EmbeddingDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSample02_SWF_FLV_EmbeddingDlg dialog

CSample02_SWF_FLV_EmbeddingDlg::CSample02_SWF_FLV_EmbeddingDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSample02_SWF_FLV_EmbeddingDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSample02_SWF_FLV_EmbeddingDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CSample02_SWF_FLV_EmbeddingDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSample02_SWF_FLV_EmbeddingDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CSample02_SWF_FLV_EmbeddingDlg, CDialog)
	//{{AFX_MSG_MAP(CSample02_SWF_FLV_EmbeddingDlg)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(ID_PLAY_EMBEDDED_SWF, OnPlayEmbeddedSwf)
	ON_BN_CLICKED(ID_PLAY_EMBEDDED_FLV, OnPlayEmbeddedFlv)
	ON_BN_CLICKED(ID_SITE, OnSite)
	ON_BN_CLICKED(ID_ORDER, OnOrder)
	ON_BN_CLICKED(IDC_CHECK_STANDARD_MENU, OnCheckStandardMenu)
	ON_BN_CLICKED(IDC_CHECK_SOUNDS, OnCheckSounds)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSample02_SWF_FLV_EmbeddingDlg message handlers

BOOL CSample02_SWF_FLV_EmbeddingDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
	
	// FlashPlayerControl creating
	// NOTE: CALL FPC_LoadRegisteredOCX() or FPC_LoadOCXCodeFromMemory() before creating!
	// See InitInstance() implementation
	{
		RECT rc;
		GetDlgItem(IDC_STATIC_FPC_PLACE)->GetWindowRect(&rc);
		ScreenToClient(&rc);

		m_hwndFlashPlayerControl = 
			CreateWindow((LPCTSTR)FPC_GetClassAtom(theApp.m_hFPC), 
						 NULL, 
						 WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS, 
						 rc.left, 
						 rc.top, 
						 rc.right - rc.left, 
						 rc.bottom - rc.top, 
						 m_hWnd, 
						 NULL, 
						 NULL, 
						 NULL);
	}

	// Loading movie from resource
	OnPlayEmbeddedSwf();

	CheckDlgButton(IDC_CHECK_SOUNDS, BST_CHECKED);

	FPC_AddOnLoadExternalResourceHandler(theApp.m_hFPC, &StaticGlobalOnLoadExternalResourceHandler, (LPARAM)this);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CSample02_SWF_FLV_EmbeddingDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CSample02_SWF_FLV_EmbeddingDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CSample02_SWF_FLV_EmbeddingDlg::OnPlayEmbeddedSwf() 
{
	HMODULE hModule = GetModuleHandle(NULL);
	HRSRC hResInfo = FindResource(hModule, _T("MOVIE"), _T("SWF"));
	HGLOBAL hResData = LoadResource(hModule, hResInfo);
	LPVOID lpData = LockResource(hResData);
	DWORD dwSize = SizeofResource(hModule, hResInfo);

	SFPCPutMovieFromMemory FPCPutMovieFromMemory;
	FPCPutMovieFromMemory.lpData = lpData;
	FPCPutMovieFromMemory.dwSize = dwSize;

	::SendMessage(m_hwndFlashPlayerControl, FPCM_PUTMOVIEFROMMEMORY, 0, (LPARAM)&FPCPutMovieFromMemory);
}

void CSample02_SWF_FLV_EmbeddingDlg::OnPlayEmbeddedFlv() 
{
	FPC_PutFlashVars(m_hwndFlashPlayerControl, _T("FLVPath=http://FLV/FlashVideo.flv"));

	HMODULE hModule = GetModuleHandle(NULL);
	HRSRC hResInfo = FindResource(hModule, _T("FLVPlayer"), _T("SWF"));
	HGLOBAL hResData = LoadResource(hModule, hResInfo);
	LPVOID lpData = LockResource(hResData);
	DWORD dwSize = SizeofResource(hModule, hResInfo);

	SFPCPutMovieFromMemory FPCPutMovieFromMemory;
	FPCPutMovieFromMemory.lpData = lpData;
	FPCPutMovieFromMemory.dwSize = dwSize;

	::SendMessage(m_hwndFlashPlayerControl, FPCM_PUTMOVIEFROMMEMORY, 0, (LPARAM)&FPCPutMovieFromMemory);
}

void CSample02_SWF_FLV_EmbeddingDlg::OnCheckStandardMenu() 
{
	SFPCPutStandardMenu FPCPutStandardMenu;
	FPCPutStandardMenu.StandardMenu = IsDlgButtonChecked(IDC_CHECK_STANDARD_MENU);
	::SendMessage(m_hwndFlashPlayerControl, FPCM_PUT_STANDARD_MENU, 0, (LPARAM)&FPCPutStandardMenu);
}

void CSample02_SWF_FLV_EmbeddingDlg::OnCheckSounds() 
{
	if (IsDlgButtonChecked(IDC_CHECK_SOUNDS))
		FPC_EnableSound(theApp.m_hFPC, TRUE);
	else
		FPC_EnableSound(theApp.m_hFPC, FALSE);
}

HRESULT WINAPI CSample02_SWF_FLV_EmbeddingDlg::GlobalOnLoadExternalResourceHandler(LPCTSTR lpszURL, IStream** ppStream, HFPC hFPC)
{
	HRESULT hr = E_FAIL;

	if (0 == lstrcmpi(lpszURL, _T("http://FLV/FlashVideo.flv")))
	{
		// Save flash video to the stream from the resource
		HMODULE hModule = GetModuleHandle(NULL);
		HRSRC hResInfo = FindResource(hModule, _T("EMBEDDED_FLV"), _T("FLV"));
		HGLOBAL hResData = LoadResource(hModule, hResInfo);
		LPCVOID lpData = LockResource(hResData);
		DWORD dwSize = SizeofResource(hModule, hResInfo);

		ULONG nWritten;
		(*ppStream)->Write(lpData, dwSize, &nWritten);

		hr = S_OK;
	}

	return hr;
}

void CSample02_SWF_FLV_EmbeddingDlg::PostNcDestroy() 
{
	CDialog::PostNcDestroy();

	FPC_RemoveOnLoadExternalResourceHandler(theApp.m_hFPC, m_dwHandlerCookie);
}

void CSample02_SWF_FLV_EmbeddingDlg::OnSite() 
{
	ShellExecute(m_hWnd, NULL, _T("http://www.f-in-box.com/dll/"), NULL, NULL, SW_SHOW);
}

void CSample02_SWF_FLV_EmbeddingDlg::OnOrder() 
{
	ShellExecute(m_hWnd, NULL, _T("http://www.f-in-box.com/dll/order.html"), NULL, NULL, SW_SHOW);
}
