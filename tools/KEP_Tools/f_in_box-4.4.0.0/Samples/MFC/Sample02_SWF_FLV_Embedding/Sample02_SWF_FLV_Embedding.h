// Sample02_SWF_FLV_Embedding.h : main header file for the Sample02_SWF_FLV_Embedding application
//

#if !defined(AFX_Sample02_SWF_FLV_Embedding_H__7534C57E_26DA_4545_8888_9F2FF01185C2__INCLUDED_)
#define AFX_Sample02_SWF_FLV_Embedding_H__7534C57E_26DA_4545_8888_9F2FF01185C2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CSample02_SWF_FLV_EmbeddingApp:
// See Sample02_SWF_FLV_Embedding.cpp for the implementation of this class
//

class CSample02_SWF_FLV_EmbeddingApp : public CWinApp
{
public:

	HFPC m_hFPC;

public:
	CSample02_SWF_FLV_EmbeddingApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSample02_SWF_FLV_EmbeddingApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CSample02_SWF_FLV_EmbeddingApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

extern CSample02_SWF_FLV_EmbeddingApp theApp;

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_Sample02_SWF_FLV_Embedding_H__7534C57E_26DA_4545_8888_9F2FF01185C2__INCLUDED_)
