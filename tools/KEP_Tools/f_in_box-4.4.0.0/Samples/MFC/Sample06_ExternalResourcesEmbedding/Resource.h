//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Sample06_ExternalResourcesEmbedding.rc
//
#define ID_ORDER                        6
#define ID_SITE                         7
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_Sample06_ExternalResourcesEmbedding_DIALOG 102
#define IDR_MAINFRAME                   128
#define IDR_IMAGE1                      129
#define IDC_STATIC_FPC_PLACE            1000

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
