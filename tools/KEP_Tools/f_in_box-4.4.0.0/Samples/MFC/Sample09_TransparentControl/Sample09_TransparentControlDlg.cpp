// Sample09_TransparentControlDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Sample09_TransparentControl.h"
#include "Sample09_TransparentControlDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

void WINAPI EventListener(HWND hwndFlashPlayerControl, LPARAM lParam, NMHDR* lpNMHDR);

/////////////////////////////////////////////////////////////////////////////
// CSample09_TransparentControlDlg dialog

CSample09_TransparentControlDlg::CSample09_TransparentControlDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSample09_TransparentControlDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSample09_TransparentControlDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CSample09_TransparentControlDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSample09_TransparentControlDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CSample09_TransparentControlDlg, CDialog)
	//{{AFX_MSG_MAP(CSample09_TransparentControlDlg)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(ID_PLAY_EMBEDDED_SWF, OnPlayEmbeddedSwf)
	ON_BN_CLICKED(ID_PLAY_EMBEDDED_FLV, OnPlayEmbeddedFlv)
	ON_BN_CLICKED(ID_PLAY_SWF_FLV_FROM_FILE, OnPlaySwfFlvFromFile)
	ON_BN_CLICKED(ID_SITE, OnSite)
	ON_BN_CLICKED(ID_ORDER, OnOrder)
	ON_BN_CLICKED(IDC_CHECK_STANDARD_MENU, OnCheckStandardMenu)
	ON_BN_CLICKED(IDC_CHECK_SOUNDS, OnCheckSounds)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSample09_TransparentControlDlg message handlers

BOOL CSample09_TransparentControlDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
	
	// FlashPlayerControl creating
	// NOTE: CALL FPC_LoadRegisteredOCX() or FPC_LoadOCXCodeFromMemory() before creating!
	// See InitInstance() implementation
	{
		RECT rc;
		GetDlgItem(IDC_STATIC_FPC_PLACE)->GetWindowRect(&rc);
		ScreenToClient(&rc);

		m_hwndFlashPlayerControl = 
			CreateWindow((LPCTSTR)FPC_GetClassAtom(theApp.m_hFPC), 
						 NULL, 
						 WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | FPCS_TRANSPARENT | FPCS_NEED_ALL_KEYS, 
						 rc.left, 
						 rc.top, 
						 rc.right - rc.left, 
						 rc.bottom - rc.top, 
						 m_hWnd, 
						 NULL, 
						 NULL, 
						 NULL);
	}

	// Loading movie from resource
	OnPlayEmbeddedSwf();

	CheckDlgButton(IDC_CHECK_SOUNDS, BST_CHECKED);

	FPC_AddOnLoadExternalResourceHandler(theApp.m_hFPC, &StaticGlobalOnLoadExternalResourceHandler, (LPARAM)this);

	FPCSetEventListener(m_hwndFlashPlayerControl, &EventListener, 0);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void WINAPI EventListener(HWND hwndFlashPlayerControl, LPARAM lParam, NMHDR* lpNMHDR)
{
	if (FPCN_PAINT_STAGE == lpNMHDR->code)
	{
		SFPCNPaintStage* info = (SFPCNPaintStage*)lpNMHDR;

		if (DEF_F_IN_BOX__PREPAINT_STAGE == info->dwStage)
		{
			RECT rc;
			::GetClientRect(lpNMHDR->hwndFrom, &rc);

			FillRect(info->hdc, &rc, GetSysColorBrush(COLOR_BTNFACE));
		}
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CSample09_TransparentControlDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CSample09_TransparentControlDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CSample09_TransparentControlDlg::OnPlayEmbeddedSwf() 
{
	HMODULE hModule = GetModuleHandle(NULL);
	HRSRC hResInfo = FindResource(hModule, _T("MOVIE"), _T("SWF"));
	HGLOBAL hResData = LoadResource(hModule, hResInfo);
	LPVOID lpData = LockResource(hResData);
	DWORD dwSize = SizeofResource(hModule, hResInfo);

	SFPCPutMovieFromMemory FPCPutMovieFromMemory;
	FPCPutMovieFromMemory.lpData = lpData;
	FPCPutMovieFromMemory.dwSize = dwSize;

	::SendMessage(m_hwndFlashPlayerControl, FPCM_PUTMOVIEFROMMEMORY, 0, (LPARAM)&FPCPutMovieFromMemory);
}

void CSample09_TransparentControlDlg::OnPlayEmbeddedFlv() 
{
	FPC_PutFlashVars(m_hwndFlashPlayerControl, _T("FLVPath=http://FLV/FlashVideo.flv"));

	HMODULE hModule = GetModuleHandle(NULL);
	HRSRC hResInfo = FindResource(hModule, _T("FLVPlayer"), _T("SWF"));
	HGLOBAL hResData = LoadResource(hModule, hResInfo);
	LPVOID lpData = LockResource(hResData);
	DWORD dwSize = SizeofResource(hModule, hResInfo);

	SFPCPutMovieFromMemory FPCPutMovieFromMemory;
	FPCPutMovieFromMemory.lpData = lpData;
	FPCPutMovieFromMemory.dwSize = dwSize;

	::SendMessage(m_hwndFlashPlayerControl, FPCM_PUTMOVIEFROMMEMORY, 0, (LPARAM)&FPCPutMovieFromMemory);
}

void CSample09_TransparentControlDlg::OnCheckStandardMenu() 
{
	SFPCPutStandardMenu FPCPutStandardMenu;
	FPCPutStandardMenu.StandardMenu = IsDlgButtonChecked(IDC_CHECK_STANDARD_MENU);
	::SendMessage(m_hwndFlashPlayerControl, FPCM_PUT_STANDARD_MENU, 0, (LPARAM)&FPCPutStandardMenu);
}

void CSample09_TransparentControlDlg::OnCheckSounds() 
{
	if (IsDlgButtonChecked(IDC_CHECK_SOUNDS))
		FPC_EnableSound(theApp.m_hFPC, TRUE);
	else
		FPC_EnableSound(theApp.m_hFPC, FALSE);
}

HRESULT WINAPI CSample09_TransparentControlDlg::GlobalOnLoadExternalResourceHandler(LPCTSTR lpszURL, IStream** ppStream, HFPC hFPC)
{
	HRESULT hr = E_FAIL;

	if (0 == lstrcmpi(lpszURL, _T("http://FLV/FlashVideo.flv")))
	{
		// Save flash video to the stream from the resource
		HMODULE hModule = GetModuleHandle(NULL);
		HRSRC hResInfo = FindResource(hModule, _T("EMBEDDED_FLV"), _T("FLV"));
		HGLOBAL hResData = LoadResource(hModule, hResInfo);
		LPCVOID lpData = LockResource(hResData);
		DWORD dwSize = SizeofResource(hModule, hResInfo);

		ULONG nWritten;
		(*ppStream)->Write(lpData, dwSize, &nWritten);

		hr = S_OK;
	}

	return hr;
}

void CSample09_TransparentControlDlg::PostNcDestroy() 
{
	CDialog::PostNcDestroy();

	FPC_RemoveOnLoadExternalResourceHandler(theApp.m_hFPC, m_dwHandlerCookie);
}

void CSample09_TransparentControlDlg::OnSite() 
{
	ShellExecute(m_hWnd, NULL, _T("http://www.f-in-box.com/dll/"), NULL, NULL, SW_SHOW);
}

void CSample09_TransparentControlDlg::OnOrder() 
{
	ShellExecute(m_hWnd, NULL, _T("http://www.f-in-box.com/dll/order.html"), NULL, NULL, SW_SHOW);
}

void CSample09_TransparentControlDlg::OnPlaySwfFlvFromFile() 
{
    CFileDialog dlg(TRUE, 
                    _T("swf"), 
                    _T(""), 
                    0, 
                    _T("All Flash Files (*.swf;*.flv)|*.swf;*.flv|Flash Movies (*.swf)|*.swf|Flash Video Files (*.flv)|*.flv|All Files (*.*)|*.*||"), 
                    this);

    if (IDOK == dlg.DoModal())
    {
        //
        CString strFlashMoviePath = dlg.GetPathName();

		if (0 == dlg.GetFileExt().CompareNoCase(_T("swf")))
		{
			//
			TCHAR lpszFlashMoviePath[MAX_PATH + 1] = { 0 };
			lstrcpy(lpszFlashMoviePath, strFlashMoviePath);

			//
			SFPCPutMovie SFPCPutMovie;
			SFPCPutMovie.lpszBuffer = lpszFlashMoviePath;
			::SendMessage(m_hwndFlashPlayerControl, FPCM_PUT_MOVIE, 0, (LPARAM)&SFPCPutMovie);
		}
		else
		{
			// Just provide URL for Flash
			CString strFlashVars = _T("FLVPath=");
			strFlashVars += strFlashMoviePath;
			FPC_PutFlashVars(m_hwndFlashPlayerControl, strFlashVars);
			FPCPutMovieFromResource(m_hwndFlashPlayerControl, 0, _T("FLVPlayer"), _T("SWF"));
		}
    }
}
