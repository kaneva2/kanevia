// Sample09_TransparentControl.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "Sample09_TransparentControl.h"
#include "Sample09_TransparentControlDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSample09_TransparentControlApp

BEGIN_MESSAGE_MAP(CSample09_TransparentControlApp, CWinApp)
	//{{AFX_MSG_MAP(CSample09_TransparentControlApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSample09_TransparentControlApp construction

CSample09_TransparentControlApp::CSample09_TransparentControlApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CSample09_TransparentControlApp object

CSample09_TransparentControlApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CSample09_TransparentControlApp initialization

BOOL CSample09_TransparentControlApp::InitInstance()
{
	AfxEnableControlContainer();

	// Check if flash is installed on the machine
	if (!FPCIsFlashInstalled())
	{
		AfxMessageBox(_T("The application needs Flash\nFlash is not installed"));
		return FALSE;
	}

	// Load registered flash activex
	// Don't forget to unload it; see ExitInstance()
	m_hFPC = FPC_LoadRegisteredOCX();

	if (NULL == m_hFPC)
	{
		AfxMessageBox(_T("FPC_LoadRegisteredOCX() failed"));
		return FALSE;
	}

	CSample09_TransparentControlDlg dlg;
	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}

int CSample09_TransparentControlApp::ExitInstance() 
{
	// Unload the flash ocx code
	if (NULL != m_hFPC)
	{
		FPC_UnloadCode(m_hFPC);
		m_hFPC = NULL;
	}
	
	return CWinApp::ExitInstance();
}
