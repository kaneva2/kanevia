//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by Sample10_SoundRecorder.rc
//
#define ID_PLAY_SWF_FROM_FILE           3
#define ID_PLAY_FLV_FROM_FILE           4
#define ID_ORDER                        6
#define ID_SITE                         7
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_Sample10_SoundRecorder_DIALOG 102
#define IDR_MAINFRAME                   128
#define IDR_SWF1                        129
#define IDC_STATIC_FPC_PLACE            1000
#define IDC_CHECK_STANDARD_MENU         1001
#define IDC_CHECK_SOUNDS                1002
#define IDC_SLIDER_SOUND_VOLUME         1003
#define IDC_STATIC_VOLUME               1004
#define IDC_EDIT_WAV_PATH               1005
#define IDC_BUTTON_SELECT_WAV           1006
#define IDC_CHECK_AUTOSTART             1007
#define IDC_BUTTON_START                1008
#define IDC_BUTTON_STOP                 1009

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1009
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
