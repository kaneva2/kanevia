// Sample10_SoundRecorderDlg.h : header file
//

#if !defined(AFX_Sample10_SoundRecorderDLG_H__A2A885BE_E70D_4124_8A3F_D0475D2F1D8B__INCLUDED_)
#define AFX_Sample10_SoundRecorderDLG_H__A2A885BE_E70D_4124_8A3F_D0475D2F1D8B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CSample10_SoundRecorderDlg dialog

class CSample10_SoundRecorderDlg : public CDialog
{
private:

	HWND m_hwndFlashPlayerControl;

	DWORD m_dwHandlerCookie;

	CString m_strFLVPath;

private:

	static HRESULT WINAPI StaticGlobalOnLoadExternalResourceHandler(LPCTSTR lpszURL, IStream** ppStream, HFPC hFPC, LPARAM lParam)
	{
		CSample10_SoundRecorderDlg* pThis = (CSample10_SoundRecorderDlg*)lParam;

		return pThis->GlobalOnLoadExternalResourceHandler(lpszURL, ppStream, hFPC);
	}

	HRESULT WINAPI GlobalOnLoadExternalResourceHandler(LPCTSTR lpszURL, IStream** ppStream, HFPC hFPC);

// Construction
public:
	CSample10_SoundRecorderDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CSample10_SoundRecorderDlg)
	enum { IDD = IDD_Sample10_SoundRecorder_DIALOG };
	CButton	m_wndBrowseButton;
	CButton	m_wndStopButton;
	CButton	m_wndStartButton;
	CEdit	m_wndWAVPath;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSample10_SoundRecorderDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	virtual void PostNcDestroy();
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CSample10_SoundRecorderDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnPlaySwfFromFile();
	afx_msg void OnPlayFlvFromFile();
	afx_msg void OnSite();
	afx_msg void OnOrder();
	afx_msg void OnButtonStart();
	afx_msg void OnButtonStop();
	afx_msg void OnButtonSelectWav();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_Sample10_SoundRecorderDLG_H__A2A885BE_E70D_4124_8A3F_D0475D2F1D8B__INCLUDED_)
