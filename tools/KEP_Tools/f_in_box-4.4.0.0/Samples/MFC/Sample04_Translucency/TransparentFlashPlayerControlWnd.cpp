// TransparentFlashPlayerControlWnd.cpp : implementation file
//

#include "stdafx.h"
#include "Sample04_Translucency.h"
#include "TransparentFlashPlayerControlWnd.h"
#include ".\transparentflashplayercontrolwnd.h"


// CTransparentFlashPlayerControlWnd

IMPLEMENT_DYNAMIC(CTransparentFlashPlayerControlWnd, CWnd)
CTransparentFlashPlayerControlWnd::CTransparentFlashPlayerControlWnd() : 
	m_bCaptured(FALSE)
{
}

CTransparentFlashPlayerControlWnd::~CTransparentFlashPlayerControlWnd()
{
}

BEGIN_MESSAGE_MAP(CTransparentFlashPlayerControlWnd, CWnd)
	ON_COMMAND(ID_CLOSE, OnClose)
	ON_COMMAND(ID_MINIMIZE, OnMinimize)
	ON_COMMAND(ID_FORUM, OnForum)
	ON_COMMAND(ID_ORDER, OnOrder)
	ON_COMMAND(ID_SITE, OnSite)
END_MESSAGE_MAP()

// CTransparentFlashPlayerControlWnd message handlers

void CTransparentFlashPlayerControlWnd::PostNcDestroy()
{
	CWnd::PostNcDestroy();

	delete this;
}

LRESULT CTransparentFlashPlayerControlWnd::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
		case WM_LBUTTONDOWN:
		{
			POINT pt = { (int)(short)LOWORD(lParam), (int)(short)HIWORD(lParam) };

			RECT rc;
			::GetWindowRect(m_hWnd, &rc);

			m_nX = rc.left + pt.x;
			m_nY = rc.top + pt.y;

			m_nLeft = rc.left;
			m_nTop = rc.top;

			m_bCaptured = TRUE;

			::SetCapture(m_hWnd);

			return 0;

			break;
		}

		case WM_LBUTTONUP:
		{
			m_bCaptured = FALSE;
			::ReleaseCapture();

			break;
		}

		case WM_MOUSEMOVE:
		{
			if (m_bCaptured)
			{
				POINT pt = { (int)(short)LOWORD(lParam), (int)(short)HIWORD(lParam) };

				RECT rc;
				::GetWindowRect(m_hWnd, &rc);

				int nWidth = rc.right - rc.left;
				int nHeight = rc.bottom - rc.top;

				rc.left = m_nLeft + (pt.x + rc.left - m_nX);
				rc.top = m_nTop + (pt.y + rc.top - m_nY);

				::MoveWindow(m_hWnd, rc.left, rc.top, nWidth, nHeight, TRUE);

				return 0;
			}

			break;
		}

		case WM_RBUTTONUP:
		{
			HMENU hMenu = LoadMenu(0, MAKEINTRESOURCE(IDR_CONTEXT_MENU));
			HMENU hContextMenu = GetSubMenu(hMenu, 0);

			POINT pt;
			GetCursorPos(&pt);
			TrackPopupMenu(hContextMenu, TPM_LEFTALIGN | TPM_TOPALIGN, pt.x, pt.y, 0, m_hWnd, NULL);

			DestroyMenu(hMenu);

			break;
		}

		default:
		{
		}
	}

	return CWnd::WindowProc(message, wParam, lParam);
}

void CTransparentFlashPlayerControlWnd::OnClose()
{
	DestroyWindow();
}

void CTransparentFlashPlayerControlWnd::OnMinimize()
{
	CloseWindow();
}

void CTransparentFlashPlayerControlWnd::OnForum()
{
	ShellExecute(m_hWnd, NULL, _T("http://www.f-in-box.com/forum/"), NULL, NULL, SW_SHOW);
}

void CTransparentFlashPlayerControlWnd::OnOrder()
{
	ShellExecute(m_hWnd, NULL, _T("http://www.f-in-box.com/dll/order.html"), NULL, NULL, SW_SHOW);
}

void CTransparentFlashPlayerControlWnd::OnSite()
{
	ShellExecute(m_hWnd, NULL, _T("http://www.f-in-box.com/dll/"), NULL, NULL, SW_SHOW);
}
