#ifndef __CTransparentFlashPlayerControlWnd__6C05E817_5FC6_4a52_BBBB_D97568B63862__
#define __CTransparentFlashPlayerControlWnd__6C05E817_5FC6_4a52_BBBB_D97568B63862__


// CTransparentFlashPlayerControlWnd

class CTransparentFlashPlayerControlWnd : public CWnd
{
	DECLARE_DYNAMIC(CTransparentFlashPlayerControlWnd)

private:
	int m_nX, m_nY;
	int m_nLeft, m_nTop;
	BOOL m_bCaptured;

public:
	CTransparentFlashPlayerControlWnd();
	virtual ~CTransparentFlashPlayerControlWnd();

protected:
	DECLARE_MESSAGE_MAP()
	virtual void PostNcDestroy();
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
public:
	afx_msg void OnClose();
	afx_msg void OnForum();
	afx_msg void OnMinimize();
	afx_msg void OnOrder();
	afx_msg void OnSite();
};

#endif // __CTransparentFlashPlayerControlWnd__6C05E817_5FC6_4a52_BBBB_D97568B63862__
