// Sample04_Translucency.h : main header file for the Sample04_Translucency application
//

#if !defined(AFX_Sample04_Translucency_H__069B96D6_DBD3_4BEF_8B4E_F2E9F9A3E382__INCLUDED_)
#define AFX_Sample04_Translucency_H__069B96D6_DBD3_4BEF_8B4E_F2E9F9A3E382__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CSample04_TranslucencyApp:
// See Sample04_Translucency.cpp for the implementation of this class
//

class CSample04_TranslucencyApp : public CWinApp
{
public:

	HFPC m_hFPC;

public:
	CSample04_TranslucencyApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSample04_TranslucencyApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CSample04_TranslucencyApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_Sample04_Translucency_H__069B96D6_DBD3_4BEF_8B4E_F2E9F9A3E382__INCLUDED_)
