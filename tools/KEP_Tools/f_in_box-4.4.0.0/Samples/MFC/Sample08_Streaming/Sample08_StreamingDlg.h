// Sample08_StreamingDlg.h : header file
//

#if !defined(AFX_Sample08_StreamingDLG_H__051EA702_B855_4AEF_B07B_0B87316BBE2F__INCLUDED_)
#define AFX_Sample08_StreamingDLG_H__051EA702_B855_4AEF_B07B_0B87316BBE2F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CSample08_StreamingDlg dialog

class CSample08_StreamingDlg : public CDialog
{
private:

	HWND m_hwndFlashPlayerControl;

	DWORD m_dwHandlerCookie;

	CString m_strCryptedFlashFilePath;

private:

	static HRESULT WINAPI StaticGlobalOnLoadExternalResourceHandler(LPCTSTR lpszURL, IStream** ppStream, HFPC hFPC, LPARAM lParam)
	{
		CSample08_StreamingDlg* pThis = (CSample08_StreamingDlg*)lParam;

		return pThis->GlobalOnLoadExternalResourceHandler(lpszURL, ppStream, hFPC);
	}

	HRESULT WINAPI GlobalOnLoadExternalResourceHandler(LPCTSTR lpszURL, IStream** ppStream, HFPC hFPC);

	void PlayCryptedFlashFile(LPCTSTR lpszPath);

// Construction
public:
	CSample08_StreamingDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CSample08_StreamingDlg)
	enum { IDD = IDD_Sample08_Streaming_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSample08_StreamingDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	virtual void PostNcDestroy();
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CSample08_StreamingDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnCrypt();
	afx_msg void OnPlay();
	afx_msg void OnSite();
	afx_msg void OnOrder();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_Sample08_StreamingDLG_H__051EA702_B855_4AEF_B07B_0B87316BBE2F__INCLUDED_)
