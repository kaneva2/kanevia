// Sample08_Streaming.h : main header file for the Sample08_Streaming application
//

#if !defined(AFX_Sample08_Streaming_H__B776FA94_0F4A_4FAB_9D39_C49DE9F68CC8__INCLUDED_)
#define AFX_Sample08_Streaming_H__B776FA94_0F4A_4FAB_9D39_C49DE9F68CC8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CSample08_StreamingApp:
// See Sample08_Streaming.cpp for the implementation of this class
//

class CSample08_StreamingApp : public CWinApp
{
public:

	HFPC m_hFPC;

public:
	CSample08_StreamingApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSample08_StreamingApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CSample08_StreamingApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

extern CSample08_StreamingApp theApp;

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_Sample08_Streaming_H__B776FA94_0F4A_4FAB_9D39_C49DE9F68CC8__INCLUDED_)
