// Sample11_FLVPlayer.h : main header file for the Sample11_FLVPlayer application
//

#if !defined(AFX_Sample11_FLVPlayer_H__7534C57E_26DA_4545_8888_9F2FF01185C2__INCLUDED_)
#define AFX_Sample11_FLVPlayer_H__7534C57E_26DA_4545_8888_9F2FF01185C2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CSample11_FLVPlayerApp:
// See Sample11_FLVPlayer.cpp for the implementation of this class
//

class CSample11_FLVPlayerApp : public CWinApp
{
public:

	HFPC m_hFPC;

public:
	CSample11_FLVPlayerApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSample11_FLVPlayerApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CSample11_FLVPlayerApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

extern CSample11_FLVPlayerApp theApp;

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_Sample11_FLVPlayer_H__7534C57E_26DA_4545_8888_9F2FF01185C2__INCLUDED_)
