// URLDialog.cpp : implementation file
//

#include "stdafx.h"
#include "sample11_flvplayer.h"
#include "URLDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CURLDialog dialog


CURLDialog::CURLDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CURLDialog::IDD, pParent)
{
	//{{AFX_DATA_INIT(CURLDialog)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CURLDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CURLDialog)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CURLDialog, CDialog)
	//{{AFX_MSG_MAP(CURLDialog)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CURLDialog message handlers

void CURLDialog::OnOK() 
{
	GetDlgItemText(IDC_EDIT_URL, m_strURL);
	
	CDialog::OnOK();
}
