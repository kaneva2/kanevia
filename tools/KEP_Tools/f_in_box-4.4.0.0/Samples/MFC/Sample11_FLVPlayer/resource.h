//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by Sample11_FLVPlayer.rc
//
#define ID_PLAY_EMBEDDED_SWF            3
#define ID_PLAY_EMBEDDED_FLV            4
#define ID_ORDER                        6
#define ID_SITE                         7
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_Sample11_FLVPlayer_DIALOG   102
#define IDR_MAINFRAME                   128
#define IDR_MENU_FLV                    131
#define IDD_URL_DIALOG                  132
#define IDC_STATIC_FPC_PLACE            1000
#define IDC_CHECK_STANDARD_MENU         1001
#define IDC_SLIDER_POSITION             1001
#define IDC_CHECK_SOUNDS                1002
#define ID_PLAY_FLV                     1002
#define ID_PLAY                         1003
#define ID_PAUSE                        1004
#define IDC_CHECK_PLAY_AUTOMATICALLY    1005
#define IDC_CHECK_SHOW_CONTROLS         1006
#define IDC_CHECK_ENABLE_SOUNDS         1007
#define IDC_SLIDER_SOUND_VOLUME         1008
#define IDC_EDIT_URL                    1009
#define IDC_STATIC_POS                  1010
#define IDC_STATIC_INFO                 1011
#define ID_PLAY_FROM_FILE               32771
#define ID_PLAY_FROM_URL                32772
#define ID_PLAY_FROM_RTMP               32773

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        133
#define _APS_NEXT_COMMAND_VALUE         32774
#define _APS_NEXT_CONTROL_VALUE         1011
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
