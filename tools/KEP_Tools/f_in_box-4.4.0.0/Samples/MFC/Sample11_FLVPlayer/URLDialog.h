#if !defined(AFX_URLDIALOG_H__821D157D_D301_49C3_952F_D4F94C6C5AFC__INCLUDED_)
#define AFX_URLDIALOG_H__821D157D_D301_49C3_952F_D4F94C6C5AFC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// URLDialog.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CURLDialog dialog

class CURLDialog : public CDialog
{
private:
	CString m_strURL;

// Construction
public:
	CURLDialog(CWnd* pParent = NULL);   // standard constructor

	CString GetURL() const
	{
		return m_strURL;
	}

// Dialog Data
	//{{AFX_DATA(CURLDialog)
	enum { IDD = IDD_URL_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CURLDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CURLDialog)
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_URLDIALOG_H__821D157D_D301_49C3_952F_D4F94C6C5AFC__INCLUDED_)
