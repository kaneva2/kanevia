// Sample03_StandalonePlayer.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "Sample03_StandalonePlayer.h"
#include "Sample03_StandalonePlayerDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSample03_StandalonePlayerApp

BEGIN_MESSAGE_MAP(CSample03_StandalonePlayerApp, CWinApp)
	//{{AFX_MSG_MAP(CSample03_StandalonePlayerApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSample03_StandalonePlayerApp construction

CSample03_StandalonePlayerApp::CSample03_StandalonePlayerApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CSample03_StandalonePlayerApp object

CSample03_StandalonePlayerApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CSample03_StandalonePlayerApp initialization

BOOL CSample03_StandalonePlayerApp::InitInstance()
{
	AfxEnableControlContainer();

	// Loading flash ocx code from the memory
	HMODULE hModule = GetModuleHandle(NULL);
	HRSRC hResInfo = FindResource(hModule, _T("FLASH_OCX_CODE"), _T("BIN"));
	HGLOBAL hResData = LoadResource(hModule, hResInfo);
	LPVOID lpFlashOCXCode = LockResource(hResData);
	DWORD dwFlashOCXCodeSize = SizeofResource(hModule, hResInfo);

	m_hFPC = FPC_LoadOCXCodeFromMemory(lpFlashOCXCode, dwFlashOCXCodeSize);

	if (NULL == m_hFPC)
	{
		AfxMessageBox(_T("FPC_LoadOCXCodeFromMemory() failed"));
		return FALSE;
	}

	CSample03_StandalonePlayerDlg dlg;
	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}

int CSample03_StandalonePlayerApp::ExitInstance() 
{
	// Unload the flash ocx code
	if (NULL != m_hFPC)
	{
		FPC_UnloadCode(m_hFPC);
		m_hFPC = NULL;
	}
	
	return CWinApp::ExitInstance();
}
