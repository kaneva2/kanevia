// Sample03_StandalonePlayerDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Sample03_StandalonePlayer.h"
#include "Sample03_StandalonePlayerDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSample03_StandalonePlayerDlg dialog

CSample03_StandalonePlayerDlg::CSample03_StandalonePlayerDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSample03_StandalonePlayerDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSample03_StandalonePlayerDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CSample03_StandalonePlayerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSample03_StandalonePlayerDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CSample03_StandalonePlayerDlg, CDialog)
	//{{AFX_MSG_MAP(CSample03_StandalonePlayerDlg)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(ID_PLAY_SWF_FROM_FILE, OnPlaySwfFromFile)
	ON_BN_CLICKED(ID_SITE, OnSite)
	ON_BN_CLICKED(ID_ORDER, OnOrder)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSample03_StandalonePlayerDlg message handlers

BOOL CSample03_StandalonePlayerDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
	
	// FlashPlayerControl creating
	// NOTE: CALL FPC_LoadRegisteredOCX() or FPC_LoadOCXCodeFromMemory() before creating!
	// See InitInstance() implementation
	{
		RECT rc;
		GetDlgItem(IDC_STATIC_FPC_PLACE)->GetWindowRect(&rc);
		ScreenToClient(&rc);

		m_hwndFlashPlayerControl = 
			CreateWindow((LPCTSTR)FPC_GetClassAtom(theApp.m_hFPC), 
						 NULL, 
						 WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS, 
						 rc.left, 
						 rc.top, 
						 rc.right - rc.left, 
						 rc.bottom - rc.top, 
						 m_hWnd, 
						 NULL, 
						 NULL, 
						 NULL);
	}

	// Loading movie from resource
	// Also you can use the message FPCM_PUTMOVIEFROMMEMORY
	FPCPutMovieFromResource(m_hwndFlashPlayerControl, 0, _T("MOVIE"), _T("SWF"));

	// 
	FPC_PutStandardMenu(m_hwndFlashPlayerControl, TRUE);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CSample03_StandalonePlayerDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CSample03_StandalonePlayerDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CSample03_StandalonePlayerDlg::OnPlaySwfFromFile() 
{
    CFileDialog dlg(TRUE, 
                    _T("swf"), 
                    _T(""), 
                    0, 
                    _T("Flash movie files (*.swf)|*.swf|All Files (*.*)|*.*||"), 
                    this);

    if (IDOK == dlg.DoModal())
    {
        //
        CString strFlashMoviePath = dlg.GetPathName();

        //
        TCHAR lpszFlashMoviePath[MAX_PATH + 1] = { 0 };
        lstrcpy(lpszFlashMoviePath, strFlashMoviePath);

        //
        SFPCPutMovie SFPCPutMovie;
        SFPCPutMovie.lpszBuffer = lpszFlashMoviePath;
        ::SendMessage(m_hwndFlashPlayerControl, FPCM_PUT_MOVIE, 0, (LPARAM)&SFPCPutMovie);
    }
}

void CSample03_StandalonePlayerDlg::OnSite() 
{
	ShellExecute(m_hWnd, NULL, _T("http://www.f-in-box.com/dll/"), NULL, NULL, SW_SHOW);
}

void CSample03_StandalonePlayerDlg::OnOrder() 
{
	ShellExecute(m_hWnd, NULL, _T("http://www.f-in-box.com/dll/order.html"), NULL, NULL, SW_SHOW);
}
