// Sample03_StandalonePlayerDlg.h : header file
//

#if !defined(AFX_Sample03_StandalonePlayerDLG_H__47E09CF6_9654_48B2_9E05_BA23E409463D__INCLUDED_)
#define AFX_Sample03_StandalonePlayerDLG_H__47E09CF6_9654_48B2_9E05_BA23E409463D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CSample03_StandalonePlayerDlg dialog

class CSample03_StandalonePlayerDlg : public CDialog
{
private:

	HWND m_hwndFlashPlayerControl;

// Construction
public:
	CSample03_StandalonePlayerDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CSample03_StandalonePlayerDlg)
	enum { IDD = IDD_Sample03_StandalonePlayer_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSample03_StandalonePlayerDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CSample03_StandalonePlayerDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnPlaySwfFromFile();
	afx_msg void OnSite();
	afx_msg void OnOrder();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_Sample03_StandalonePlayerDLG_H__47E09CF6_9654_48B2_9E05_BA23E409463D__INCLUDED_)
