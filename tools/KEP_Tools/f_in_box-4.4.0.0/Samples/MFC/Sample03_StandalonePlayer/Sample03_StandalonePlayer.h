// Sample03_StandalonePlayer.h : main header file for the Sample03_StandalonePlayer application
//

#if !defined(AFX_Sample03_StandalonePlayer_H__F8B3C50B_1A07_4087_9F51_0428102F9E88__INCLUDED_)
#define AFX_Sample03_StandalonePlayer_H__F8B3C50B_1A07_4087_9F51_0428102F9E88__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CSample03_StandalonePlayerApp:
// See Sample03_StandalonePlayer.cpp for the implementation of this class
//

class CSample03_StandalonePlayerApp : public CWinApp
{
public:

	HFPC m_hFPC;

public:
	CSample03_StandalonePlayerApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSample03_StandalonePlayerApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CSample03_StandalonePlayerApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

extern CSample03_StandalonePlayerApp theApp;

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_Sample03_StandalonePlayer_H__F8B3C50B_1A07_4087_9F51_0428102F9E88__INCLUDED_)
