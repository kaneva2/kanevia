// Sample05_SWF2BitmapDlg.h : header file
//

#if !defined(AFX_Sample05_SWF2BitmapDLG_H__6190FD03_77C2_4FD2_A3B7_5E0360C35249__INCLUDED_)
#define AFX_Sample05_SWF2BitmapDLG_H__6190FD03_77C2_4FD2_A3B7_5E0360C35249__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CSample05_SWF2BitmapDlg dialog

class CSample05_SWF2BitmapDlg : public CDialog
{
private:

	HWND m_hwndFlashPlayerControl;

// Construction
public:
	CSample05_SWF2BitmapDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CSample05_SWF2BitmapDlg)
	enum { IDD = IDD_Sample05_SWF2Bitmap_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSample05_SWF2BitmapDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CSample05_SWF2BitmapDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnPlaySwfFromFile();
	afx_msg void OnPlayEmbeddedSwf();
	afx_msg void OnSaveAsBitmap();
	afx_msg void OnSite();
	afx_msg void OnOrder();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_Sample05_SWF2BitmapDLG_H__6190FD03_77C2_4FD2_A3B7_5E0360C35249__INCLUDED_)
