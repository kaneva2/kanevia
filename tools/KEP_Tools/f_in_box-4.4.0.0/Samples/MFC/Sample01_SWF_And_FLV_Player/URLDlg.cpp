// URLDlg.cpp : implementation file
//

#include "stdafx.h"
#include "sample01_swf_and_flv_player.h"
#include "URLDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CURLDlg dialog


CURLDlg::CURLDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CURLDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CURLDlg)
	m_strURL = _T("");
	//}}AFX_DATA_INIT
}


void CURLDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CURLDlg)
	DDX_Text(pDX, IDC_EDIT_URL, m_strURL);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CURLDlg, CDialog)
	//{{AFX_MSG_MAP(CURLDlg)
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CURLDlg message handlers

BOOL CURLDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();	

	CheckDlgButton(IDC_RADIO_SWF, TRUE);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CURLDlg::OnDestroy() 
{
	m_bIsSWF = 0 != IsDlgButtonChecked(IDC_RADIO_SWF);

	CDialog::OnDestroy();
}
