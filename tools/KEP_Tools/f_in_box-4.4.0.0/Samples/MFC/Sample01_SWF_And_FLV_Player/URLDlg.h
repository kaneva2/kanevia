#if !defined(AFX_URLDLG_H__B87743A5_18C8_42FC_BC42_086853FD1800__INCLUDED_)
#define AFX_URLDLG_H__B87743A5_18C8_42FC_BC42_086853FD1800__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// URLDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CURLDlg dialog

class CURLDlg : public CDialog
{
public:

	BOOL m_bIsSWF;

// Construction
public:
	CURLDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CURLDlg)
	enum { IDD = IDD_DIALOG_URL };
	CString	m_strURL;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CURLDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CURLDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_URLDLG_H__B87743A5_18C8_42FC_BC42_086853FD1800__INCLUDED_)
