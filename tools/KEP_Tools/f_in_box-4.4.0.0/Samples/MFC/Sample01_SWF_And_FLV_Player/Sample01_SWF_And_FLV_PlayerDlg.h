// Sample01_SWF_And_FLV_PlayerDlg.h : header file
//

#if !defined(AFX_Sample01_SWF_And_FLV_PlayerDLG_H__A2A885BE_E70D_4124_8A3F_D0475D2F1D8B__INCLUDED_)
#define AFX_Sample01_SWF_And_FLV_PlayerDLG_H__A2A885BE_E70D_4124_8A3F_D0475D2F1D8B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CSample01_SWF_And_FLV_PlayerDlg dialog

class CSample01_SWF_And_FLV_PlayerDlg : public CDialog
{
private:

	HWND m_hwndFlashPlayerControl;

	DWORD m_dwHandlerCookie;

	CString m_strFLVPath;

private:

	static HRESULT WINAPI StaticGlobalOnLoadExternalResourceHandler(LPCTSTR lpszURL, IStream** ppStream, HFPC hFPC, LPARAM lParam)
	{
		CSample01_SWF_And_FLV_PlayerDlg* pThis = (CSample01_SWF_And_FLV_PlayerDlg*)lParam;

		return pThis->GlobalOnLoadExternalResourceHandler(lpszURL, ppStream, hFPC);
	}

	HRESULT WINAPI GlobalOnLoadExternalResourceHandler(LPCTSTR lpszURL, IStream** ppStream, HFPC hFPC);

// Construction
public:
	CSample01_SWF_And_FLV_PlayerDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CSample01_SWF_And_FLV_PlayerDlg)
	enum { IDD = IDD_Sample01_SWF_And_FLV_Player_DIALOG };
	CSliderCtrl	m_wndSliderCtrl__SoundVolume;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSample01_SWF_And_FLV_PlayerDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	virtual void PostNcDestroy();
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CSample01_SWF_And_FLV_PlayerDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnCheckStandardMenu();
	afx_msg void OnPlaySwfFlvFromFile();
	afx_msg void OnCheckSounds();
	afx_msg void OnSite();
	afx_msg void OnOrder();
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnPlaySwfFlvFromUrl();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_Sample01_SWF_And_FLV_PlayerDLG_H__A2A885BE_E70D_4124_8A3F_D0475D2F1D8B__INCLUDED_)
