@ECHO OFF

SETLOCAL EnableDelayedExpansion

SET ThisDir=%~dp0
SET BaseDir=%ThisDir%..\..
SET ScriptsDir=%BaseDir%\BuildScripts


SET ProjectName=f_in_box-4.4.0.0
SET SolutionToBuild="%ThisDir%source_code\f_in_box_vs2015.sln"
rem SET DebugConfiguration="debug"
rem SET ReleaseConfiguration="release"

call %ScriptsDir%\BuildSolution.cmd %1

IF %CopyFiles% == 1 (
	call %ScriptsDir%\xcopy.cmd %ThisDir%f_in_box\include %IncludeDir%\f_in_box\ /E /Y
	
	call %ScriptsDir%\xcopy.cmd %ThisDir%f_in_box\lib\f_in_box.lib %LibDir%\f_in_box.lib* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%f_in_box\bin\f_in_box.dll %BinDir%\f_in_box.dll* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%f_in_box\bin\f_in_box.pdb %BinDir%\f_in_box.pdb* /Y

	call %ScriptsDir%\xcopy.cmd %ThisDir%f_in_box\lib\f_in_boxd.lib %LibDir%\f_in_boxd.lib* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%f_in_box\bin\f_in_boxd.dll %BinDir%\f_in_boxd.dll* /Y
	call %ScriptsDir%\xcopy.cmd %ThisDir%f_in_box\bin\f_in_boxd.pdb %BinDir%\f_in_boxd.pdb* /Y
)
