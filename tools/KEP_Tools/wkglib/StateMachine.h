/******************************************************************************
 StateMachine.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include <string>
#include <vector>

namespace wkg
{

class XMLDocument;
class XMLNode;

namespace state_machine
{

class State;
class Transition;
class Trigger;

class StateMachine
{

public:
	StateMachine();
	virtual ~StateMachine();
	
	virtual bool Load(const char *filename);

	State *CreateState(std::string name);
	State *GetState();
	void AddState(State *newState);

	bool SetState(std::string stateName);
	void SendTrigger(Trigger* trig);
	void OnUpdate(uint32 ms);

protected:
	virtual bool Load(XMLDocument *doc);

	// returns State pointer to given state in m_states, 0 if state not found
	State *FindState(std::string stateName);
	void GetStates(XMLNode *curNode);
	void CreateTransitions(State* theState, XMLNode* curNode);
	void AssignPointers();

protected:
	typedef std::vector<State *> SMVector;
	typedef SMVector::iterator SMIterator;
	typedef std::map<std::string, State *> StateStringMap;

	SMVector					m_states;
	StateStringMap				m_map;
	std::string					m_name;
	State						*m_curr_state;

};

}

}



