/******************************************************************************
 pixelshader.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#ifndef _PIXEL_SHADER_H_
#define _PIXEL_SHADER_H_

#include <string>

namespace wkg
{

namespace engine
{

class PixelShader
{
	public:
		PixelShader();
		virtual ~PixelShader();

		bool Init(const char* psoFilename);

		virtual void LoadConstants();

		virtual bool CompileShader(const std::string function);
		virtual bool CreateShader();
		bool Load(const char* psoFilename);

		IDirect3DPixelShader9 *GetHandle() const { return m_handle; }

	protected:
		IDirect3DPixelShader9 *m_handle;
		uint32 *m_function;
};

}

}

#endif

