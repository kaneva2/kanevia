/******************************************************************************
 odesolver.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

// a generic ODE solver
// we need to provide the following things for the ode solver:
//  a function f(x, t) that can be evaluated given x and t
//  a function dfdt(x, t) that returns the time derivative at time t

namespace wkg {

class ODESolver
{
	public:
		enum IntegrationMethod { EULER, MIDPOINT, RUNGEKUTTA4 };

		ODESolver() : m_method(RUNGEKUTTA4) {};
		~ODESolver() {};

		void SetMethod(IntegrationMethod m) { m_method = m; }
		IntegrationMethod GetMethod() { return m_method; }

		typedef void (*dxdt_func)(real t, real x[], real xdot[]);

		void Integrate(real x0[], real xend[], int32 len, real t0, real h, dxdt_func dxdt)
		{
			switch(m_method)
			{
			case EULER:
				EulerIntegrate(x0, xend, len, t0, h, dxdt);
				break;

			case MIDPOINT:
				MidpointIntegrate(x0, xend, len, t0, h, dxdt);
				break;

			case RUNGEKUTTA4:
				RungeKutta4(x0, xend, len, t0, h, dxdt);
				break;

			default:
				ASSERT(0);
				break;
			};
		}

		void EulerIntegrate(real x0[], real xend[], int32 len, real t0, real h, dxdt_func dxdt)
		{
			real *xdot = new real[len];
			dxdt(t0, x0, xdot);
		
			for(int32 i = 0; i < len; i++)
				xend[i] = x0[i] + xdot[i] * h;

			delete [] xdot;
		}

		void MidpointIntegrate(real x0[], real xend[], int32 len, real t0, real h, dxdt_func dxdt)
		{
			real *xdot = new real[len];
			real *temp = new real[len];

			dxdt(t0, x0, xdot);

			for(int32 i = 0; i < len; i++)
				temp[i] = x0[i] + xdot[i] * (h / 2.0f);

			dxdt(0.0f, temp, xdot);

			for(i = 0; i < len; i++)
				xend[i] = x0[i] + xdot[i] * h;

			delete [] temp;
			delete [] xdot;
		}

		void RungeKutta4(real x0[], real xend[], int32 len, real t0, real h, dxdt_func dxdt)
		{
			real *f1 = new real[len];
			real *f2 = new real[len];
			real *f3 = new real[len];
			real *f4 = new real[len];

			real *xdot = new real[len];
			real *tempx = new real[len];
			real *tempt = new real[len];


			// f1 = hf(t, x)
			dxdt(t0, x0, xdot);

			int32 i;
			for(i = 0; i < len; i++)
				f1[i] = h * xdot[i];

			// f2 = hf(t + h/2, x + f1/2)
			for(i = 0; i < len; i++)
				tempx[i] = x0[i] + f1[i] / 2.0f;

			dxdt(0.0f, tempx, xdot);

			for(i = 0; i < len; i++)
				f2[i] = h * xdot[i];
		
			// f3 = hf(t + h/2, x + f2/2)
			for(i = 0; i < len; i++)
				tempx[i] = x0[i] + f2[i] / 2.0f;

			dxdt(0.0f, tempx, xdot);

			for(i = 0; i < len; i++)
				f3[i] = h * xdot[i];

			// f4 = hf(t + h, x + f3)
			for(i = 0; i < len; i++)
				tempx[i] = x0[i] + f3[i];

			dxdt(0.0f, tempx, xdot);

			for(i = 0; i < len; i++)
				f4[i] = h * xdot[i];

			for(i = 0; i < len; i++)
				xend[i] = x0[i] + (f1[i] + 2 * f2[i] + 2 * f3[i] + f4[i]) / 6.0f;

			delete [] f1;
			delete [] f2;
			delete [] f3;
			delete [] f4;
			delete [] tempx;
			delete [] tempt;
			delete [] xdot;
		}

	protected:
		IntegrationMethod m_method;

};

}
