/******************************************************************************
 renderstate.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "wkgtypes.h"
#include "RenderState.h"
#include "Device.h"

namespace wkg {
namespace engine {

RenderState RenderState::def(0, 0);
RenderState::RSList RenderState::rslist;
RenderState::TSSList RenderState::tsslist;
RenderState::SSSList RenderState::ssslist;

RenderState::RenderState(int32, int32)
{
	memzero(rsValue, RS_MAX * sizeof(uint32));
	memzero(tssValue, TSS_MAXSTAGE * TSS_MAX * sizeof(uint32));

	// All of these values come straight from the directx documentation.
	// Modify them only to keep in sync with directx changes.
	//
	Set(D3DRS_ZENABLE, WKG_ZENABLE);

	Set(D3DRS_FILLMODE, D3DFILL_SOLID);
//	Set(D3DRS_FILLMODE, D3DFILL_WIREFRAME);

	Set(D3DRS_SHADEMODE, D3DSHADE_GOURAUD);

	Set(D3DRS_ZWRITEENABLE, TRUE);

	Set(D3DRS_ALPHATESTENABLE, FALSE);

	Set(D3DRS_LASTPIXEL, TRUE);

	Set(D3DRS_SRCBLEND, D3DBLEND_ONE);
	Set(D3DRS_DESTBLEND, D3DBLEND_ZERO);

	Set(D3DRS_CULLMODE, D3DCULL_CCW);

	Set(D3DRS_ZFUNC, D3DCMP_LESSEQUAL);

	Set(D3DRS_ALPHAREF, 0x000000FF);
	Set(D3DRS_ALPHAFUNC, D3DCMP_ALWAYS);

	Set(D3DRS_DITHERENABLE, FALSE);
	Set(D3DRS_ALPHABLENDENABLE, FALSE);
	Set(D3DRS_FOGENABLE, FALSE);

	Set(D3DRS_SPECULARENABLE, FALSE);

	Set(D3DRS_FOGCOLOR, 0);
	Set(D3DRS_FOGTABLEMODE, D3DFOG_NONE);
	SetReal(D3DRS_FOGSTART, 0.0f);
	SetReal(D3DRS_FOGEND, 1.0f);

	SetReal(D3DRS_FOGDENSITY, 1.0f);
	Set(D3DRS_RANGEFOGENABLE, FALSE);

	Set(D3DRS_STENCILENABLE, FALSE);
	Set(D3DRS_STENCILFAIL, D3DSTENCILOP_KEEP);
	Set(D3DRS_STENCILZFAIL, D3DSTENCILOP_KEEP);
	Set(D3DRS_STENCILPASS, D3DSTENCILOP_KEEP);
	Set(D3DRS_STENCILFUNC, D3DCMP_ALWAYS);
	Set(D3DRS_STENCILREF, 0);
	Set(D3DRS_STENCILMASK, 0xFFFFFFFFL);
	Set(D3DRS_STENCILWRITEMASK, 0xFFFFFFFFL);

	Set(D3DRS_TEXTUREFACTOR, 0xFFFFFFFFL);

	Set(D3DRS_WRAP0, 0);
	Set(D3DRS_WRAP1, 0);
	Set(D3DRS_WRAP2, 0);
	Set(D3DRS_WRAP3, 0);
	Set(D3DRS_WRAP4, 0);
	Set(D3DRS_WRAP5, 0);
	Set(D3DRS_WRAP6, 0);
	Set(D3DRS_WRAP7, 0);

	Set(D3DRS_CLIPPING, TRUE);
	Set(D3DRS_LIGHTING, TRUE);
	Set(D3DRS_AMBIENT, 0);

	Set(D3DRS_FOGVERTEXMODE, D3DFOG_NONE);

	Set(D3DRS_COLORVERTEX, TRUE);
	Set(D3DRS_LOCALVIEWER, TRUE);
	Set(D3DRS_NORMALIZENORMALS, FALSE);

	Set(D3DRS_DIFFUSEMATERIALSOURCE, D3DMCS_COLOR1);
	Set(D3DRS_SPECULARMATERIALSOURCE, D3DMCS_COLOR2);
	Set(D3DRS_AMBIENTMATERIALSOURCE, D3DMCS_MATERIAL);
	Set(D3DRS_EMISSIVEMATERIALSOURCE, D3DMCS_MATERIAL);

	Set(D3DRS_VERTEXBLEND, D3DVBF_DISABLE);

	Set(D3DRS_CLIPPLANEENABLE, 0);

	SetReal(D3DRS_POINTSIZE, 1.0f);
	SetReal(D3DRS_POINTSIZE_MIN, 0.0f);

	Set(D3DRS_POINTSPRITEENABLE, FALSE);
	Set(D3DRS_POINTSCALEENABLE, FALSE);

	SetReal(D3DRS_POINTSCALE_A, 1.0f);
	SetReal(D3DRS_POINTSCALE_B, 0.0f);
	SetReal(D3DRS_POINTSCALE_C, 0.0f);

	Set(D3DRS_MULTISAMPLEANTIALIAS, TRUE);
	Set(D3DRS_MULTISAMPLEMASK, 0xFFFFFFFFL);

	Set(D3DRS_PATCHEDGESTYLE, D3DPATCHEDGE_DISCRETE);

	Set(D3DRS_DEBUGMONITORTOKEN, D3DDMT_ENABLE);

	SetReal(D3DRS_POINTSIZE_MAX, 64.0f);

	Set(D3DRS_INDEXEDVERTEXBLENDENABLE, FALSE);

	Set(D3DRS_COLORWRITEENABLE, 0x0000000FL);
	
	SetReal(D3DRS_TWEENFACTOR, 0.0f);

	Set(D3DRS_BLENDOP, D3DBLENDOP_ADD);

	Set(D3DRS_POSITIONDEGREE, D3DDEGREE_CUBIC);
	Set(D3DRS_NORMALDEGREE, D3DDEGREE_LINEAR);

	Set(D3DRS_SCISSORTESTENABLE, FALSE);

	SetReal(D3DRS_SLOPESCALEDEPTHBIAS, 0.0f);
	Set(D3DRS_ANTIALIASEDLINEENABLE, FALSE);
	SetReal(D3DRS_MINTESSELLATIONLEVEL, 1.0f);

	SetReal(D3DRS_MAXTESSELLATIONLEVEL, 1.0f);
	SetReal(D3DRS_ADAPTIVETESS_X, 0.0f);
	SetReal(D3DRS_ADAPTIVETESS_Y, 0.0f);
	SetReal(D3DRS_ADAPTIVETESS_Z, 1.0f);
	SetReal(D3DRS_ADAPTIVETESS_Z, 0.0f);

	Set(D3DRS_ENABLEADAPTIVETESSELLATION, FALSE);
	Set(D3DRS_TWOSIDEDSTENCILMODE, FALSE);
	Set(D3DRS_CCW_STENCILFAIL, 0x00000001);

	Set(D3DRS_CCW_STENCILZFAIL, 1);
	Set(D3DRS_CCW_STENCILPASS, 1);
	Set(D3DRS_CCW_STENCILFUNC, 1);
	Set(D3DRS_COLORWRITEENABLE1, 0xf);
	Set(D3DRS_COLORWRITEENABLE2, 0xf);
	Set(D3DRS_COLORWRITEENABLE3, 0xf);

	Set(D3DRS_BLENDFACTOR, 0xffffffff);

	Set(D3DRS_SRGBWRITEENABLE, 0);
	
	SetReal(D3DRS_DEPTHBIAS, 0.0f);

	Set(D3DRS_WRAP8, 0);
	Set(D3DRS_WRAP9, 0);
	Set(D3DRS_WRAP10, 0);
	Set(D3DRS_WRAP11, 0);
	Set(D3DRS_WRAP12, 0);
	Set(D3DRS_WRAP13, 0);
	Set(D3DRS_WRAP14, 0);
	Set(D3DRS_WRAP15, 0);

	Set(D3DRS_SEPARATEALPHABLENDENABLE, FALSE);
	Set(D3DRS_SRCBLENDALPHA, D3DBLEND_ONE);
	Set(D3DRS_DESTBLENDALPHA, D3DBLEND_ZERO);

	Set(D3DRS_BLENDOPALPHA, D3DBLENDOP_ADD);


	for (int32 i = 0; i < TSS_MAXSTAGE; i++)
	{
		Set(i, D3DTSS_COLOROP, i == 0 ? D3DTOP_MODULATE : D3DTOP_DISABLE);
		Set(i, D3DTSS_COLORARG1, D3DTA_TEXTURE);
		Set(i, D3DTSS_COLORARG2, D3DTA_CURRENT);

		Set(i, D3DTSS_ALPHAOP, i == 0 ? D3DTOP_SELECTARG1 : D3DTOP_DISABLE);
		Set(i, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
		Set(i, D3DTSS_ALPHAARG2, D3DTA_CURRENT);

		SetReal(i, D3DTSS_BUMPENVMAT00, 0.0f);
		SetReal(i, D3DTSS_BUMPENVMAT01, 0.0f);
		SetReal(i, D3DTSS_BUMPENVMAT10, 0.0f);
		SetReal(i, D3DTSS_BUMPENVMAT11, 0.0f);

		Set(i, D3DTSS_TEXCOORDINDEX, i | D3DTSS_TCI_PASSTHRU);

		SetReal(i, D3DTSS_BUMPENVLSCALE, 0.0f);
		SetReal(i, D3DTSS_BUMPENVLOFFSET, 0.0f);

		Set(i, D3DTSS_TEXTURETRANSFORMFLAGS, D3DTTFF_DISABLE);

		Set(i, D3DTSS_COLORARG0, D3DTA_CURRENT);
		Set(i, D3DTSS_ALPHAARG0, D3DTA_CURRENT);
		Set(i, D3DTSS_RESULTARG, D3DTA_CURRENT);

		Set(i, D3DTSS_CONSTANT, 0);
	}

	for(int32 i = 0; i < SSS_MAXSTAGE; i++)
	{
		SSet(i, D3DSAMP_ADDRESSU, D3DTADDRESS_WRAP);
		SSet(i, D3DSAMP_ADDRESSV, D3DTADDRESS_WRAP);
		SSet(i, D3DSAMP_ADDRESSW, D3DTADDRESS_WRAP);
		SSet(i, D3DSAMP_BORDERCOLOR, 0x00);
		SSet(i, D3DSAMP_MAGFILTER,  D3DTEXF_POINT);
		SSet(i, D3DSAMP_MINFILTER,  D3DTEXF_POINT);
		SSet(i, D3DSAMP_MIPFILTER,  D3DTEXF_NONE);
		SSetReal(i, D3DSAMP_MIPMAPLODBIAS, 0.0f);
		SSet(i, D3DSAMP_MAXMIPLEVEL, 0);
		SSet(i, D3DSAMP_MAXANISOTROPY, 1);
		SSet(i, D3DSAMP_SRGBTEXTURE, 0);
		SSet(i, D3DSAMP_ELEMENTINDEX, 0);
	}
}

RenderState::RenderState(Basis basis)
{
	if (basis == NEW)
	{
		memcpy(rsValue, def.rsValue, RS_MAX * sizeof(uint32));
		memcpy(tssValue, def.tssValue, TSS_MAXSTAGE * TSS_MAX * sizeof(uint32));
		memcpy(sssValue, def.sssValue, SSS_MAXSTAGE * SSS_MAX * sizeof(uint32));
	}
	else
	{
#if 1
		// this is pretty wasteful if you only need to change a small number of states
		// (i.e. the typical case)....if it turns out to be a big time-consumer, we
		// can split this class out into a change-only class
		//
		memcpy(rsValue, Device::Instance()->GetRenderState().rsValue, RS_MAX * sizeof(uint32));
		memcpy(tssValue, Device::Instance()->GetRenderState().tssValue, TSS_MAXSTAGE * TSS_MAX * sizeof(uint32));
		memcpy(sssValue, Device::Instance()->GetRenderState().sssValue, SSS_MAXSTAGE * SSS_MAX * sizeof(uint32));
#else
		ASSERT(0);
#endif
	}
}

void 
RenderState::Set(D3DRENDERSTATETYPE state, uint32 value)
{
	ASSERT(state >= 0 && state < RS_MAX);

	rsValue[state] = value;
}

void 
RenderState::SetReal(D3DRENDERSTATETYPE state, real value)
{
	ASSERT(state >= 0 && state < RS_MAX);

	rsValue[state] = *((uint32*)&((float)value));
}

void 
RenderState::Set(int32 stage, D3DTEXTURESTAGESTATETYPE state, uint32 value)
{
	ASSERT(stage >= 0 && stage < TSS_MAXSTAGE);
	ASSERT(state >= 0 && state < TSS_MAX);

	tssValue[stage][state] = value;
}

void 
RenderState::SetReal(int32 stage, D3DTEXTURESTAGESTATETYPE state, real value)
{
	ASSERT(stage >= 0 && stage < TSS_MAXSTAGE);
	ASSERT(state >= 0 && state < TSS_MAX);

	tssValue[stage][state] = *((uint32*)&((float)value));
}

void 
RenderState::SSet(int32 stage, D3DSAMPLERSTATETYPE state, uint32 value)
{
	ASSERT(stage >= 0 && stage < SSS_MAXSTAGE);
	ASSERT(state >= 0 && state < SSS_MAX);

	sssValue[stage][state] = value;
}

void 
RenderState::SSetReal(int32 stage, D3DSAMPLERSTATETYPE state, real value)
{
	ASSERT(stage >= 0 && stage < SSS_MAXSTAGE);
	ASSERT(state >= 0 && state < SSS_MAX);

	sssValue[stage][state] = *((uint32*)&((float)value));
}

uint32 
RenderState::Get(D3DRENDERSTATETYPE state) const
{
	ASSERT(state >= 0 && state < RS_MAX);

	return rsValue[state];
}

real 
RenderState::GetReal(D3DRENDERSTATETYPE state) const
{
	ASSERT(state >= 0 && state < RS_MAX);

	return *(real*)&rsValue[state];
}

uint32 
RenderState::Get(int32 stage, D3DTEXTURESTAGESTATETYPE state) const
{
	ASSERT(stage >= 0 && stage < TSS_MAXSTAGE);
	ASSERT(state >= 0 && state < TSS_MAX);

	return tssValue[stage][state];
}

real 
RenderState::GetReal(int32 stage, D3DTEXTURESTAGESTATETYPE state) const
{
	ASSERT(stage >= 0 && stage < TSS_MAXSTAGE);
	ASSERT(state >= 0 && state < TSS_MAX);

	return *(real*)&tssValue[stage][state];
}

uint32 
RenderState::SGet(int32 stage, D3DSAMPLERSTATETYPE state) const
{
	ASSERT(stage >= 0 && stage < SSS_MAXSTAGE);
	ASSERT(state >= 0 && state < SSS_MAX);

	return sssValue[stage][state];
}

real 
RenderState::SGetReal(int32 stage, D3DSAMPLERSTATETYPE state) const
{
	ASSERT(stage >= 0 && stage < SSS_MAXSTAGE);
	ASSERT(state >= 0 && state < SSS_MAX);

	return *(real*)&sssValue[stage][state];
}

//
// ----------------------------------------------------------------------------------------------------
//

#if 0
SaveRenderStateValue::SaveRenderStateValue()
{
}

SaveRenderStateValue::~SaveRenderStateValue()
{
	Restore();
}

void 
SaveRenderStateValue::Save(D3DRENDERSTATETYPE state)
{
	saved.insert(SavedStateMap::value_type(state, Device::Instance()->GetRenderState().Get(state)));
}

void 
SaveRenderStateValue::SaveDefault(D3DRENDERSTATETYPE state)
{
	savedDef.insert(SavedStateMap::value_type(state, RenderState::GetDefault().Get(state)));
}

void 
SaveRenderStateValue::Restore()
{
	if (savedDef.size() > 0)
	{
		SavedStateMap::iterator& iter = savedDef.begin();
		SavedStateMap::iterator& defEnd = savedDef.end();

		while (iter != defEnd)
		{
			RenderState::GetDefault().Set(D3DRENDERSTATETYPE(iter->first), iter->second);
			++iter;
		}

		savedDef.clear();
	}

	if (saved.size() > 0)
	{
		RenderState rs(RenderState::DELTA);

		SavedStateMap::iterator& iter = saved.begin();
		SavedStateMap::iterator& defEnd = saved.end();

		while (iter != defEnd)
		{
			rs.Set(D3DRENDERSTATETYPE(iter->first), iter->second);
			++iter;
		}

		saved.clear();

		Device::Instance()->SetRenderState(rs);
	}
}
#endif
//
// ----------------------------------------------------------------------------------------------------
//

RenderState::RSList::RSList()
{
	count = 0;

	list[count++] = D3DRS_ZENABLE;
	list[count++] = D3DRS_FILLMODE;
	list[count++] = D3DRS_SHADEMODE;
	list[count++] = D3DRS_ZWRITEENABLE;
	list[count++] = D3DRS_ALPHATESTENABLE;
	list[count++] = D3DRS_LASTPIXEL;
	list[count++] = D3DRS_SRCBLEND;
	list[count++] = D3DRS_DESTBLEND;
	list[count++] = D3DRS_CULLMODE;
	list[count++] = D3DRS_ZFUNC;
	list[count++] = D3DRS_ALPHAREF;
	list[count++] = D3DRS_ALPHAFUNC;
	list[count++] = D3DRS_DITHERENABLE;
	list[count++] = D3DRS_ALPHABLENDENABLE;
	list[count++] = D3DRS_FOGENABLE;
	list[count++] = D3DRS_SPECULARENABLE;
	list[count++] = D3DRS_FOGCOLOR;
	list[count++] = D3DRS_FOGTABLEMODE;
	list[count++] = D3DRS_FOGSTART;
	list[count++] = D3DRS_FOGEND;
	list[count++] = D3DRS_FOGDENSITY;
	list[count++] = D3DRS_RANGEFOGENABLE;
	list[count++] = D3DRS_STENCILENABLE;
	list[count++] = D3DRS_STENCILFAIL;
	list[count++] = D3DRS_STENCILZFAIL;
	list[count++] = D3DRS_STENCILPASS;
	list[count++] = D3DRS_STENCILFUNC;
	list[count++] = D3DRS_STENCILREF;
	list[count++] = D3DRS_STENCILMASK;
	list[count++] = D3DRS_STENCILWRITEMASK;
	list[count++] = D3DRS_TEXTUREFACTOR;
	list[count++] = D3DRS_WRAP0;
	list[count++] = D3DRS_WRAP1;
	list[count++] = D3DRS_WRAP2;
	list[count++] = D3DRS_WRAP3;
	list[count++] = D3DRS_WRAP4;
	list[count++] = D3DRS_WRAP5;
	list[count++] = D3DRS_WRAP6;
	list[count++] = D3DRS_WRAP7;
	list[count++] = D3DRS_CLIPPING;
	list[count++] = D3DRS_LIGHTING;
	list[count++] = D3DRS_AMBIENT;
	list[count++] = D3DRS_FOGVERTEXMODE;
	list[count++] = D3DRS_COLORVERTEX;
	list[count++] = D3DRS_LOCALVIEWER;
	list[count++] = D3DRS_NORMALIZENORMALS;
	list[count++] = D3DRS_DIFFUSEMATERIALSOURCE;
	list[count++] = D3DRS_SPECULARMATERIALSOURCE;
	list[count++] = D3DRS_AMBIENTMATERIALSOURCE;
	list[count++] = D3DRS_EMISSIVEMATERIALSOURCE;
	list[count++] = D3DRS_VERTEXBLEND;
	list[count++] = D3DRS_CLIPPLANEENABLE;
	list[count++] = D3DRS_POINTSIZE;
	list[count++] = D3DRS_POINTSIZE_MIN;
	list[count++] = D3DRS_POINTSPRITEENABLE;
	list[count++] = D3DRS_POINTSCALEENABLE;
	list[count++] = D3DRS_POINTSCALE_A;
	list[count++] = D3DRS_POINTSCALE_B;
	list[count++] = D3DRS_POINTSCALE_C;
	list[count++] = D3DRS_MULTISAMPLEANTIALIAS;
	list[count++] = D3DRS_MULTISAMPLEMASK;
	list[count++] = D3DRS_PATCHEDGESTYLE;
	list[count++] = D3DRS_DEBUGMONITORTOKEN;
	list[count++] = D3DRS_POINTSIZE_MAX;
	list[count++] = D3DRS_INDEXEDVERTEXBLENDENABLE;
	list[count++] = D3DRS_COLORWRITEENABLE;
	list[count++] = D3DRS_TWEENFACTOR;
	list[count++] = D3DRS_BLENDOP;
	list[count++] = D3DRS_POSITIONDEGREE;
    list[count++] = D3DRS_NORMALDEGREE;
    list[count++] = D3DRS_SCISSORTESTENABLE;
    list[count++] = D3DRS_SLOPESCALEDEPTHBIAS;
    list[count++] = D3DRS_ANTIALIASEDLINEENABLE;
    list[count++] = D3DRS_MINTESSELLATIONLEVEL;
    list[count++] = D3DRS_MAXTESSELLATIONLEVEL;
    list[count++] = D3DRS_ADAPTIVETESS_X;
    list[count++] = D3DRS_ADAPTIVETESS_Y;
    list[count++] = D3DRS_ADAPTIVETESS_Z;
    list[count++] = D3DRS_ADAPTIVETESS_W;
    list[count++] = D3DRS_ENABLEADAPTIVETESSELLATION;
    list[count++] = D3DRS_TWOSIDEDSTENCILMODE;
    list[count++] = D3DRS_CCW_STENCILFAIL;
    list[count++] = D3DRS_CCW_STENCILZFAIL;
    list[count++] = D3DRS_CCW_STENCILPASS;
    list[count++] = D3DRS_CCW_STENCILFUNC;
    list[count++] = D3DRS_COLORWRITEENABLE1;
    list[count++] = D3DRS_COLORWRITEENABLE2;
    list[count++] = D3DRS_COLORWRITEENABLE3;
    list[count++] = D3DRS_BLENDFACTOR;
    list[count++] = D3DRS_SRGBWRITEENABLE;
    list[count++] = D3DRS_DEPTHBIAS;
    list[count++] = D3DRS_WRAP8;
    list[count++] = D3DRS_WRAP9;
    list[count++] = D3DRS_WRAP10;
    list[count++] = D3DRS_WRAP11;
    list[count++] = D3DRS_WRAP12;
    list[count++] = D3DRS_WRAP13;
    list[count++] = D3DRS_WRAP14;
    list[count++] = D3DRS_WRAP15;
    list[count++] = D3DRS_SEPARATEALPHABLENDENABLE;
    list[count++] = D3DRS_SRCBLENDALPHA;
    list[count++] = D3DRS_DESTBLENDALPHA;
    list[count++] = D3DRS_BLENDOPALPHA;
}

RenderState::TSSList::TSSList()
{
	count = 0;

	list[count++] = D3DTSS_COLOROP;
	list[count++] = D3DTSS_COLORARG1;
	list[count++] = D3DTSS_COLORARG2;
	list[count++] = D3DTSS_ALPHAOP;
	list[count++] = D3DTSS_ALPHAARG1;
	list[count++] = D3DTSS_ALPHAARG2;
	list[count++] = D3DTSS_BUMPENVMAT00;
	list[count++] = D3DTSS_BUMPENVMAT01;
	list[count++] = D3DTSS_BUMPENVMAT10;
	list[count++] = D3DTSS_BUMPENVMAT11;
	list[count++] = D3DTSS_TEXCOORDINDEX;
	list[count++] = D3DTSS_BUMPENVLSCALE;
	list[count++] = D3DTSS_BUMPENVLOFFSET;
	list[count++] = D3DTSS_TEXTURETRANSFORMFLAGS;
	list[count++] = D3DTSS_COLORARG0;
	list[count++] = D3DTSS_ALPHAARG0;
	list[count++] = D3DTSS_RESULTARG;
	list[count++] = D3DTSS_CONSTANT;
}

RenderState::SSSList::SSSList()
{
	count = 0;

	list[count++] = D3DSAMP_ADDRESSU;
    list[count++] = D3DSAMP_ADDRESSV;
    list[count++] = D3DSAMP_ADDRESSW;
    list[count++] = D3DSAMP_BORDERCOLOR;
    list[count++] = D3DSAMP_MAGFILTER;
    list[count++] = D3DSAMP_MINFILTER;
    list[count++] = D3DSAMP_MIPFILTER;
    list[count++] = D3DSAMP_MIPMAPLODBIAS;
    list[count++] = D3DSAMP_MAXMIPLEVEL;
    list[count++] = D3DSAMP_MAXANISOTROPY;
    list[count++] = D3DSAMP_SRGBTEXTURE;
    list[count++] = D3DSAMP_ELEMENTINDEX;
}

}

}

