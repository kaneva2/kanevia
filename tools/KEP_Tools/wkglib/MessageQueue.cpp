/******************************************************************************
 MessageQueue.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "singleton.h"
#include <map>
#include <queue>
#include "Message.h"
#include "Messenger.h"
#include "MessageSubscription.h"
#include "MessageQueue.h"

namespace wkg
{

MessageQueue::MessageQueue()
{
}

MessageQueue::~MessageQueue()
{

}

void
MessageQueue::RegisterMessenger(int32 id, Messenger* new_messenger)
{
	m_id_map[id] = new_messenger;
}

void
MessageQueue::QueueMessage(Message* new_msg)
{
	m_queue.push(new_msg);
}

void
MessageQueue::ProcessMessages()
{
	while(!m_queue.empty())
	{
		Message* first_msg = m_queue.front();
		m_queue.pop();
		int32 id_to = first_msg->GetIDTo();

		// Process Subscription messages
		if(-1 == id_to)
		{
			std::vector<MessageSubscription*>::iterator itor = m_subscriptions.begin();
			std::vector<MessageSubscription*>::iterator iend = m_subscriptions.end();
			for(; itor != iend; ++itor)
			{
				if( (*itor)->GetIDFrom() == first_msg->GetIDFrom())
				{
					Messenger* recipient = m_id_map[(*itor)->GetIDTo()];
					if(recipient)
					{
						recipient->OnMessage(first_msg);
					}
				}
			}
		}

		// Process Non-Subscription messages
		else
		{
			Messenger* recipient = m_id_map[id_to];
			if(recipient)
			{
				recipient->OnMessage(first_msg);
			}
		}
	}
}

void
MessageQueue::AddSubscription(int32 to, int32 from)
{
	MessageSubscription* subscription = new MessageSubscription(to, from);
	m_subscriptions.push_back(subscription);
}


}