/******************************************************************************
 Messenger.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

// derive game objects from this to send and receive messages
#pragma once

#include "UniqueIDObj.h"

namespace wkg
{

class Message;

class Messenger : public UniqueIDObj
{
public:
	Messenger();
	virtual ~Messenger();

	int32 GetUniqueID()			{	return m_id;	}

	virtual void OnMessage(Message* received);

protected:

};

}

