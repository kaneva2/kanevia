/******************************************************************************
 device.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "device.h"
#include "matrix44.h"
#include "renderstate.h"

namespace wkg 
{

namespace engine 
{

RenderState *Device::m_rs = 0;
D3DCAPS9 Device::m_caps;
LPDIRECT3D9 Device::m_d3d = 0;

bool 
Device::Init(Device **dev, HWND hwnd, bool windowed, int32 w, int32 h, IDirect3DDevice9 *d3ddevice)
{
	RECT rc;
	int32 width, height;

	GetClientRect(hwnd, &rc);
	if(0 == w)
		width = rc.right;
	else
		width = w;

	if(0 == h)
		height = rc.bottom;
	else 
		height =  h;

	HRESULT hr = d3ddevice->GetDirect3D(&m_d3d);
	if(0 == m_d3d)
	{
		ASSERT(0);
		return false;
	}

	D3DDISPLAYMODE displaymode;
	if(FAILED(m_d3d->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &displaymode)))
		return false;

	(*dev) = (wkg::engine::Device *)d3ddevice;

	(*dev)->GetDeviceCaps(&m_caps);

	ms_instance = (*dev);

	m_rs = new RenderState();

	return true;
}

bool 
Device::Init(Device **dev, HWND hwnd, bool windowed, int32 w, int32 h)
{
	RECT rc;
	int32 width, height;

	GetClientRect(hwnd, &rc);
	if(0 == w)
		width = rc.right;
	else
		width = w;

	if(0 == h)
		height = rc.bottom;
	else 
		height =  h;

	m_d3d = Direct3DCreate9(D3D_SDK_VERSION);
	if(0 == m_d3d)
	{
		ASSERT(0);
		return false;
	}

	D3DDISPLAYMODE displaymode;
	if(FAILED(m_d3d->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &displaymode)))
		return false;

	D3DPRESENT_PARAMETERS d3dpp; 
	memzero(&d3dpp, sizeof(D3DPRESENT_PARAMETERS));

	if(windowed)
	{
//#ifdef _DEBUG
		// remove the "topmost" style to allow easier debugging
		//
		DWORD exStyle = GetWindowLong(hwnd, GWL_EXSTYLE);
		if (exStyle & WS_EX_TOPMOST)
		{
			SetWindowLong(hwnd, GWL_EXSTYLE, exStyle & ~WS_EX_TOPMOST);
			// update the cached info (the rest of these flags are necessary 
			//  to turn off the rest of this heavily overloaded function)
			SetWindowPos(hwnd, HWND_NOTOPMOST, 0, 0, 0, 0, SWP_FRAMECHANGED |	
				SWP_NOACTIVATE | SWP_NOMOVE | SWP_NOREDRAW | SWP_NOREPOSITION | SWP_NOSIZE);
		}
//#endif
		d3dpp.Windowed = TRUE;
		d3dpp.hDeviceWindow = hwnd;
		d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
//		d3dpp.BackBufferFormat = D3DFMT_A8R8G8B8;
		d3dpp.BackBufferFormat = displaymode.Format;
		d3dpp.EnableAutoDepthStencil = TRUE;
		d3dpp.AutoDepthStencilFormat = D3DFMT_D24X8;
//		d3dpp.AutoDepthStencilFormat = D3DFMT_D16;
		d3dpp.PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE;

		if (width > 0)
		{
			displaymode.Width = width;
			d3dpp.BackBufferWidth = width;
		}

		if (height > 0)
		{
			displaymode.Height = height;
			d3dpp.BackBufferHeight = height;
		}
	}
	else
	{
		d3dpp.Windowed = FALSE;
		d3dpp.hDeviceWindow = hwnd;
		d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
		d3dpp.BackBufferFormat = D3DFMT_X8R8G8B8;
		d3dpp.EnableAutoDepthStencil = TRUE;
		d3dpp.AutoDepthStencilFormat = D3DFMT_D24X8;
//		d3dpp.AutoDepthStencilFormat = D3DFMT_D16;
		d3dpp.FullScreen_RefreshRateInHz = 75;
//		d3dpp.FullScreen_PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE;
		d3dpp.BackBufferWidth = width;
		d3dpp.BackBufferHeight = height;
	}

	d3dpp.Flags |= D3DPRESENTFLAG_LOCKABLE_BACKBUFFER;

//#define SOFTWARE_VERTEX_PROCESSING

#if (defined(REF_DEVICE))
	if(FAILED(m_d3d->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_REF, hwnd,
		D3DCREATE_SOFTWARE_VERTEXPROCESSING, 
		&d3dpp, dev)))
	{
		return false;
	}
#elif (defined(SOFTWARE_VERTEX_PROCESSING))
	if(FAILED(m_d3d->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hwnd,
		D3DCREATE_SOFTWARE_VERTEXPROCESSING, 
		&d3dpp, dev)))
	{
		return false;
	}
#else
	HRESULT hres = m_d3d->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hwnd,
		D3DCREATE_HARDWARE_VERTEXPROCESSING, 
		&d3dpp, (IDirect3DDevice9 **)dev);
	if(FAILED(hres))
	{
		DEBUGMSG("Warning: defaulting to software vertex processing!");
		if(FAILED(m_d3d->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hwnd,
			D3DCREATE_SOFTWARE_VERTEXPROCESSING, 
			&d3dpp, (IDirect3DDevice9 **)dev)))
		{
			return false;
		}
	}
#endif

	(*dev)->GetDeviceCaps(&m_caps);

	ms_instance = (*dev);

	m_rs = new RenderState();

	return true;
}


void 
Device::ForceDefaultRenderState()
{
	int32 i;
	uint32 j;
	const RenderState::RSList& state = RenderState::GetRSList();
	const RenderState::TSSList& tstate = RenderState::GetTSSList();

	*m_rs = RenderState::GetDefault();

	for (i = 0; i < state.GetCount(); i++)
		((IDirect3DDevice9 *)this)->SetRenderState(state[i], m_rs->Get(state[i]));

	for (j = 0; j < m_caps.MaxTextureBlendStages; j++)
		for (i = 0; i < tstate.GetCount(); i++)
			SetTextureStageState(j, tstate[i], m_rs->Get(j, tstate[i]));
}

void 
Device::CaptureRenderState(RenderState &rs)
{
	int32 i;
	uint32 j;
	const RenderState::RSList &state = RenderState::GetRSList();
	const RenderState::TSSList &tstate = RenderState::GetTSSList();
	const RenderState::SSSList &sstate = RenderState::GetSSSList();

	for(i = 0; i < state.GetCount(); i++)
	{
		DWORD devrs;
		((IDirect3DDevice9 *)this)->GetRenderState(state[i], &devrs);
		rs.Set(state[i], devrs);
	}

	for(j = 0; j < m_caps.MaxTextureBlendStages; j++)
	{
		for(i = 0; i < tstate.GetCount(); i++)
		{
			DWORD devts;
			GetTextureStageState(j, tstate[i], &devts);	
			rs.Set(j, tstate[i], devts);
    	}
	}

	for(j = 0; j < m_caps.MaxTextureBlendStages; j++)
	{
		for(i = 0; i < sstate.GetCount(); i++)
		{
			DWORD devss;
			GetSamplerState(j, sstate[i], &devss);	
			rs.SSet(j, sstate[i], devss);
    	}
	}
}

void 
Device::RestoreRenderState(const RenderState& rs)
{
	int32 i;
	uint32 j;
	const RenderState::RSList& state = RenderState::GetRSList();
	const RenderState::TSSList& tstate = RenderState::GetTSSList();
	const RenderState::SSSList& sstate = RenderState::GetSSSList();

	for (i = 0; i < state.GetCount(); i++)
	{
		m_rs->Set(state[i], rs.Get(state[i]));
		((IDirect3DDevice9 *)this)->SetRenderState(state[i], rs.Get(state[i]));
	}

	for (j = 0; j < m_caps.MaxTextureBlendStages; j++)
	{
		for (i = 0; i < tstate.GetCount(); i++)
		{
			m_rs->Set(j, tstate[i], rs.Get(j, tstate[i]));
			((IDirect3DDevice9 *)this)->SetTextureStageState(j, tstate[i], rs.Get(j, tstate[i]));
    	}
	}

	for(j = 0; j < m_caps.MaxTextureBlendStages; j++)
	{
		for(i = 0; i < sstate.GetCount(); i++)
		{
			m_rs->SSet(j, sstate[i], rs.SGet(j, sstate[i]));
			((IDirect3DDevice9 *)this)->SetSamplerState(j, sstate[i], rs.SGet(j, sstate[i]));
    	}
	}

}

void 
Device::SetRenderState(const RenderState& rs)
{
	int32 i;
	uint32 j;
	const RenderState::RSList& state = RenderState::GetRSList();
	const RenderState::TSSList& tstate = RenderState::GetTSSList();

	for (i = 0; i < state.GetCount(); i++)
	{
		if (m_rs->Get(state[i]) != rs.Get(state[i]))
		{
			m_rs->Set(state[i], rs.Get(state[i]));
			((IDirect3DDevice9 *)this)->SetRenderState(state[i], rs.Get(state[i]));
		}
	}

	for (j = 0; j < m_caps.MaxTextureBlendStages; j++)
	{
		for (i = 0; i < tstate.GetCount(); i++)
		{
			if (m_rs->Get(j, tstate[i]) != rs.Get(j, tstate[i]))
			{
				m_rs->Set(j, tstate[i], rs.Get(j, tstate[i]));
				((IDirect3DDevice9 *)this)->SetTextureStageState(j, tstate[i], rs.Get(j, tstate[i]));

    		}
    	}
	}
}

HRESULT 
Device::SetTexture(int32 stage, const Texture *t)
{
	if(t)
		return ((IDirect3DDevice9 *)this)->SetTexture(stage, t->Interface());
	else
		return ((IDirect3DDevice9 *)this)->SetTexture(stage, 0);
}

HRESULT 
Device::SetTexture(int32 stage, IDirect3DTexture9 *t)
{
	return ((IDirect3DDevice9 *)this)->SetTexture(stage, t);
}

HRESULT 
Device::SetTransform(D3DTRANSFORMSTATETYPE state, const D3DXMATRIX *m)
{
	return ((IDirect3DDevice9 *)this)->SetTransform(state, m);
}

HRESULT 
Device::SetTransform(D3DTRANSFORMSTATETYPE state, const Matrix44 *m)
{
	D3DMATRIX d3dm;

	d3dm._11 = m->_11; d3dm._12 = m->_12; d3dm._13 = m->_13; d3dm._14 = m->_14;
	d3dm._21 = m->_21; d3dm._22 = m->_22; d3dm._23 = m->_23; d3dm._24 = m->_24;
	d3dm._31 = m->_31; d3dm._32 = m->_32; d3dm._33 = m->_33; d3dm._34 = m->_34;
	d3dm._41 = m->_41; d3dm._42 = m->_42; d3dm._43 = m->_43; d3dm._44 = m->_44;

	return ((IDirect3DDevice9 *)this)->SetTransform(state, (D3DMATRIX *)m);
}

HRESULT 
Device::GetTransform(D3DTRANSFORMSTATETYPE state, Matrix44 *m)
{
	D3DMATRIX d3dm;
	HRESULT hres = ((IDirect3DDevice9 *)this)->GetTransform(state, &d3dm);

	m->_11 = d3dm._11; m->_12 = d3dm._12; m->_13 = d3dm._13; m->_14 = d3dm._14;
	m->_21 = d3dm._21; m->_22 = d3dm._22; m->_23 = d3dm._23; m->_24 = d3dm._24;
	m->_31 = d3dm._31; m->_32 = d3dm._32; m->_33 = d3dm._33; m->_34 = d3dm._34;
	m->_41 = d3dm._41; m->_42 = d3dm._42; m->_43 = d3dm._43; m->_44 = d3dm._44;

	return hres;

}

}

}

