/******************************************************************************
 Vector3.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "wkgtypes.h"
#include "Vector3.h"
#include "Matrix44.h"

namespace wkg {

const Vector3 Vector3::ZERO(0.0f, 0.0f, 0.0f);
const Vector3 Vector3::ONE(1.0f, 1.0f, 1.0f);
const Vector3 Vector3::AXIS[] =
{
	Vector3(1.0f, 0.0f, 0.0f),
	Vector3(0.0f, 1.0f, 0.0f),
	Vector3(0.0f, 0.0f, 1.0f)
};

Vector3 
Vector3::PtMult(const Matrix44& mat) const
{
	return Vector3(
		x * mat._11 + y * mat._21 + z * mat._31 + mat._41,
		x * mat._12 + y * mat._22 + z * mat._32 + mat._42,
		x * mat._13 + y * mat._23 + z * mat._33 + mat._43);
}

Vector3 
Vector3::PtMultProject(const Matrix44& mat) const
{
	real w = (x * mat._14 + y * mat._24 + z * mat._34 + mat._44);

	return Vector3(
		x * mat._11 + y * mat._21 + z * mat._31 + mat._41,
		x * mat._12 + y * mat._22 + z * mat._32 + mat._42,
		x * mat._13 + y * mat._23 + z * mat._33 + mat._43) / w;
}

Vector3 
Vector3::PtMultTransposeOf(const Matrix44& mat) const
{
	return Vector3(
		x * mat._11 + y * mat._12 + z * mat._13 + mat._14,
		x * mat._21 + y * mat._22 + z * mat._23 + mat._24,
		x * mat._31 + y * mat._32 + z * mat._33 + mat._34);
}

Vector3 
Vector3::PtMultTransposeOfProject(const Matrix44& mat) const
{
	real w = (x * mat._41 + y * mat._42 + z * mat._43 + mat._44);

	return Vector3(
		x * mat._11 + y * mat._12 + z * mat._13 + mat._14,
		x * mat._21 + y * mat._22 + z * mat._23 + mat._24,
		x * mat._31 + y * mat._32 + z * mat._33 + mat._34) / w;
}

Vector3& 
Vector3::PtMultEq(const Matrix44& mat)
{
	*this = PtMult(mat);
	return *this;
}

Vector3& 
Vector3::PtMultEqProject(const Matrix44& mat)
{
	*this = PtMultProject(mat);
	return *this;
}

Vector3& 
Vector3::PtMultEqTransposeOf(const Matrix44& mat)
{
	*this = PtMultTransposeOf(mat);
	return *this;
}

Vector3& 
Vector3::PtMultEqTransposeOfProject(const Matrix44& mat)
{
	*this = PtMultTransposeOfProject(mat);
	return *this;
}

Vector3 
Vector3::VecMult(const Matrix44& mat) const
{
	return Vector3(
		x * mat._11 + y * mat._21 + z * mat._31,
		x * mat._12 + y * mat._22 + z * mat._32,
		x * mat._13 + y * mat._23 + z * mat._33);
}

Vector3 
Vector3::VecMultTransposeOf(const Matrix44& mat) const
{
	return Vector3(
		x * mat._11 + y * mat._12 + z * mat._13,
		x * mat._21 + y * mat._22 + z * mat._23,
		x * mat._31 + y * mat._32 + z * mat._33);
}

Vector3& 
Vector3::VecMultEq(const Matrix44& mat)
{
	*this = VecMult(mat);
	return *this;
}

Vector3& 
Vector3::VecMultEqTransposeOf(const Matrix44& mat)
{
	*this = VecMultTransposeOf(mat);
	return *this;
}

}
