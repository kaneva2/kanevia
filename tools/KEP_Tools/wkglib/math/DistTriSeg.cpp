#include "wkgtypes.h"
#include "Distance.h"
#include "Triangle.h"
#include "LineSeg.h"

namespace wkg {

//--------- This code is *entirely* based on the following (they deserve credit :)

// Magic Software, Inc.
// http://www.magic-software.com
// Copyright (c) 2000-2002.  All Rights Reserved
//
// Source code from Magic Software is supplied under the terms of a license
// agreement and may not be copied or disclosed except in accordance with the
// terms of that agreement.  The various license agreements may be found at
// the Magic Software web site.  This file is subject to the license
//
// FREE SOURCE CODE
// http://www.magic-software.com/License/free.pdf

real 
Distance::SqrTriSeg(const Triangle& tri, const LineSegment& seg, real* pfTriP0, real *pfTriP1, real *pfSegP)
{
	Vector3 diff = tri.GetOrigin() - seg.GetOrigin();
	real fA00 = seg.GetDirection().LengthSqr();
	real fA01 = -seg.GetDirection().Dot(tri.GetEdge0());
	real fA02 = -seg.GetDirection().Dot(tri.GetEdge1());
	real fA11 = tri.GetEdge0().LengthSqr();
	real fA12 = tri.GetEdge0().Dot(tri.GetEdge1());
	real fA22 = tri.GetEdge1().Dot(tri.GetEdge1());
	real fB0  = -diff.Dot(seg.GetDirection());
	real fB1  = diff.Dot(tri.GetEdge0());
	real fB2  = diff.Dot(tri.GetEdge1());
	real fCof00 = fA11*fA22-fA12*fA12;
	real fCof01 = fA02*fA12-fA01*fA22;
	real fCof02 = fA01*fA12-fA02*fA11;
	real fDet = fA00*fCof00+fA01*fCof01+fA02*fCof02;

	LineSegment triSeg;
	real fSqrDist, fSqrDist0, fR, fS, fT, fR0, fS0, fT0;

	if (ABS(fDet) >= tolerance)
	{
		real fCof11 = fA00*fA22-fA02*fA02;
		real fCof12 = fA02*fA01-fA00*fA12;
		real fCof22 = fA00*fA11-fA01*fA01;
		real fInvDet = 1.0f/fDet;
		real fRhs0 = -fB0*fInvDet;
		real fRhs1 = -fB1*fInvDet;
		real fRhs2 = -fB2*fInvDet;

		fR = fCof00*fRhs0+fCof01*fRhs1+fCof02*fRhs2;
		fS = fCof01*fRhs0+fCof11*fRhs1+fCof12*fRhs2;
		fT = fCof02*fRhs0+fCof12*fRhs1+fCof22*fRhs2;

		if (fR < 0.0f)
		{
			if (fS + fT <= 1.0f)
			{
				if (fS < 0.0f)
				{
					if (fT < 0.0f)  // region 4m
					{
						// min on face s=0 or t=0 or r=0
						//
						triSeg.Set(tri.GetOrigin(), tri.GetEdge1());
						fSqrDist = Distance::SqrSegSeg(seg,triSeg,&fR,&fT);
						fS = 0.0f;
						triSeg.Set(tri.GetOrigin(), tri.GetEdge0());
						fSqrDist0 = Distance::SqrSegSeg(seg,triSeg,&fR0,&fS0);
						fT0 = 0.0f;

						if (fSqrDist0 < fSqrDist)
						{
							fSqrDist = fSqrDist0;
							fR = fR0;
							fS = fS0;
							fT = fT0;
						}

						fSqrDist0 = Distance::SqrTriPoint(tri,seg.GetOrigin(),&fS0,&fT0);
						fR0 = 0.0f;

						if (fSqrDist0 < fSqrDist)
						{
							fSqrDist = fSqrDist0;
							fR = fR0;
							fS = fS0;
							fT = fT0;
						}
					}
					else  // region 3m
					{
						// min on face s=0 or r=0
						//
						triSeg.Set(tri.GetOrigin(), tri.GetEdge1());
						fSqrDist = Distance::SqrSegSeg(seg,triSeg,&fR,&fT);
						fS = 0.0f;
						fSqrDist0 = Distance::SqrTriPoint(tri,seg.GetOrigin(),&fS0,&fT0);
						fR0 = 0.0f;

						if (fSqrDist0 < fSqrDist)
						{
							fSqrDist = fSqrDist0;
							fR = fR0;
							fS = fS0;
							fT = fT0;
						}
					}
				}
				else if ( fT < 0.0f )  // region 5m
				{
					// min on face t=0 or r=0
					//
					triSeg.Set(tri.GetOrigin(), tri.GetEdge0());
					fSqrDist = Distance::SqrSegSeg(seg,triSeg,&fR,&fS);
					fT = 0.0f;
					fSqrDist0 = Distance::SqrTriPoint(tri,seg.GetOrigin(),&fS0,&fT0);
					fR0 = 0.0f;

					if (fSqrDist0 < fSqrDist)
					{
						fSqrDist = fSqrDist0;
						fR = fR0;
						fS = fS0;
						fT = fT0;
					}
				}
				else  // region 0m
				{
					// min on face r=0
					//
					fSqrDist = Distance::SqrTriPoint(tri,seg.GetOrigin(),&fS,&fT);
					fR = 0.0f;
				}
			}
			else
			{
				if (fS < 0.0f)  // region 2m
				{
					// min on face s=0 or s+t=1 or r=0
					//
					triSeg.Set(tri.GetOrigin(), tri.GetEdge1());
					fSqrDist = Distance::SqrSegSeg(seg,triSeg,&fR,&fT);
					fS = 0.0f;
					triSeg.Set(tri.GetOrigin()+tri.GetEdge0(), tri.GetEdge2());
					fSqrDist0 = Distance::SqrSegSeg(seg,triSeg,&fR0,&fT0);
					fS0 = 1.0f-fT0;

					if (fSqrDist0 < fSqrDist)
					{
						fSqrDist = fSqrDist0;
						fR = fR0;
						fS = fS0;
						fT = fT0;
					}

					fSqrDist0 = Distance::SqrTriPoint(tri,seg.GetOrigin(),&fS0,&fT0);
					fR0 = 0.0f;

					if (fSqrDist0 < fSqrDist)
					{
						fSqrDist = fSqrDist0;
						fR = fR0;
						fS = fS0;
						fT = fT0;
					}
				}
				else if (fT < 0.0f)  // region 6m
				{
					// min on face t=0 or s+t=1 or r=0
					//
					triSeg.Set(tri.GetOrigin(), tri.GetEdge0());
					fSqrDist = Distance::SqrSegSeg(seg,triSeg,&fR,&fS);
					fT = 0.0f;
					triSeg.Set(tri.GetOrigin()+tri.GetEdge0(), tri.GetEdge2());
					fSqrDist0 = Distance::SqrSegSeg(seg,triSeg,&fR0,&fT0);
					fS0 = 1.0f-fT0;

					if (fSqrDist0 < fSqrDist)
					{
						fSqrDist = fSqrDist0;
						fR = fR0;
						fS = fS0;
						fT = fT0;
					}

					fSqrDist0 = Distance::SqrTriPoint(tri,seg.GetOrigin(),&fS0,&fT0);
					fR0 = 0.0f;

					if (fSqrDist0 < fSqrDist)
					{
						fSqrDist = fSqrDist0;
						fR = fR0;
						fS = fS0;
						fT = fT0;
					}
				}
				else  // region 1m
				{
					// min on face s+t=1 or r=0
					//
					triSeg.Set(tri.GetOrigin()+tri.GetEdge0(), tri.GetEdge2());
					fSqrDist = Distance::SqrSegSeg(seg,triSeg,&fR,&fT);
					fS = 1.0f-fT;
					fSqrDist0 = Distance::SqrTriPoint(tri,seg.GetOrigin(),&fS0,&fT0);
					fR0 = 0.0f;

					if (fSqrDist0 < fSqrDist)
					{
						fSqrDist = fSqrDist0;
						fR = fR0;
						fS = fS0;
						fT = fT0;
					}
				}
			}
		}
		else if (fR <= 1.0f)
		{
			if (fS + fT <= 1.0f)
			{
				if (fS < 0.0f)
				{
					if (fT < 0.0f)  // region 4
					{
						// min on face s=0 or t=0
						//
						triSeg.Set(tri.GetOrigin(), tri.GetEdge1());
						fSqrDist = Distance::SqrSegSeg(seg,triSeg,&fR,&fT);
						fS = 0.0f;
						triSeg.Set(tri.GetOrigin(), tri.GetEdge0());
						fSqrDist0 = Distance::SqrSegSeg(seg,triSeg,&fR0,&fS0);
						fT0 = 0.0f;

						if (fSqrDist0 < fSqrDist)
						{
							fSqrDist = fSqrDist0;
							fR = fR0;
							fS = fS0;
							fT = fT0;
						}
					}
					else  // region 3
					{
						// min on face s=0
						//
						triSeg.Set(tri.GetOrigin(), tri.GetEdge1());
						fSqrDist = Distance::SqrSegSeg(seg,triSeg,&fR,&fT);
						fS = 0.0f;
					}
				}
				else if (fT < 0.0f)  // region 5
				{
					// min on face t=0
					//
					triSeg.Set(tri.GetOrigin(), tri.GetEdge0());
					fSqrDist = Distance::SqrSegSeg(seg,triSeg,&fR,&fS);
					fT = 0.0f;
				}
				else  // region 0
				{
					// global minimum is interior, done
					fSqrDist = 0.0f;
				}
			}
			else
			{
				if (fS < 0.0f)  // region 2
				{
					// min on face s=0 or s+t=1
					//
					triSeg.Set(tri.GetOrigin(), tri.GetEdge1());
					fSqrDist = Distance::SqrSegSeg(seg,triSeg,&fR,&fT);
					fS = 0.0f;
					triSeg.Set(tri.GetOrigin()+tri.GetEdge0(), tri.GetEdge2());
					fSqrDist0 = Distance::SqrSegSeg(seg,triSeg,&fR0,&fT0);
					fS0 = 1.0f-fT0;

					if (fSqrDist0 < fSqrDist)
					{
						fSqrDist = fSqrDist0;
						fR = fR0;
						fS = fS0;
						fT = fT0;
					}
				}
				else if ( fT < 0.0f )  // region 6
				{
					// min on face t=0 or s+t=1
					//
					triSeg.Set(tri.GetOrigin(), tri.GetEdge0());
					fSqrDist = Distance::SqrSegSeg(seg,triSeg,&fR,&fS);
					fT = 0.0f;
					triSeg.Set(tri.GetOrigin()+tri.GetEdge0(), tri.GetEdge2());
					fSqrDist0 = Distance::SqrSegSeg(seg,triSeg,&fR0,&fT0);
					fS0 = 1.0f-fT0;

					if (fSqrDist0 < fSqrDist)
					{
						fSqrDist = fSqrDist0;
						fR = fR0;
						fS = fS0;
						fT = fT0;
					}
				}
				else  // region 1
				{
					// min on face s+t=1
					//
					triSeg.Set(tri.GetOrigin()+tri.GetEdge0(), tri.GetEdge2());
					fSqrDist = Distance::SqrSegSeg(seg,triSeg,&fR,&fT);
					fS = 1.0f-fT;
				}
			}
		}
		else  // fR > 1
		{
			if (fS+fT <= 1.0f)
			{
				if (fS < 0.0f)
				{
					if (fT < 0.0f)  // region 4p
					{
						// min on face s=0 or t=0 or r=1
						//
						triSeg.Set(tri.GetOrigin(), tri.GetEdge1());
						fSqrDist = Distance::SqrSegSeg(seg,triSeg,&fR,&fT);
						fS = 0.0f;
						triSeg.Set(tri.GetOrigin(), tri.GetEdge0());
						fSqrDist0 = Distance::SqrSegSeg(seg,triSeg,&fR0,&fS0);
						fT0 = 0.0f;

						if (fSqrDist0 < fSqrDist)
						{
							fSqrDist = fSqrDist0;
							fR = fR0;
							fS = fS0;
							fT = fT0;
						}
						fSqrDist0 = Distance::SqrTriPoint(tri,seg.GetDestination(),&fS0,&fT0);
						fR0 = 1.0f;

						if (fSqrDist0 < fSqrDist)
						{
							fSqrDist = fSqrDist0;
							fR = fR0;
							fS = fS0;
							fT = fT0;
						}
					}
					else  // region 3p
					{
						// min on face s=0 or r=1
						//
						triSeg.Set(tri.GetOrigin(), tri.GetEdge1());
						fSqrDist = Distance::SqrSegSeg(seg,triSeg,&fR,&fT);
						fS = 0.0f;
						fSqrDist0 = Distance::SqrTriPoint(tri,seg.GetDestination(),&fS0,&fT0);
						fR0 = 1.0f;

						if (fSqrDist0 < fSqrDist)
						{
							fSqrDist = fSqrDist0;
							fR = fR0;
							fS = fS0;
							fT = fT0;
						}
					}
				}
				else if (fT < 0.0f)  // region 5p
				{
					// min on face t=0 or r=1
					//
					triSeg.Set(tri.GetOrigin(), tri.GetEdge0());
					fSqrDist = Distance::SqrSegSeg(seg,triSeg,&fR,&fS);
					fT = 0.0f;
					fSqrDist0 = Distance::SqrTriPoint(tri,seg.GetDestination(),&fS0,&fT0);
					fR0 = 1.0f;

					if (fSqrDist0 < fSqrDist)
					{
						fSqrDist = fSqrDist0;
						fR = fR0;
						fS = fS0;
						fT = fT0;
					}
				}
				else  // region 0p
				{
					// min face on r=1
					//
					fSqrDist = Distance::SqrTriPoint(tri,seg.GetDestination(),&fS,&fT);
					fR = 1.0f;
				}
			}
			else
			{
				if (fS < 0.0f)  // region 2p
				{
					// min on face s=0 or s+t=1 or r=1
					//
					triSeg.Set(tri.GetOrigin(), tri.GetEdge1());
					fSqrDist = Distance::SqrSegSeg(seg,triSeg,&fR,&fT);
					fS = 0.0f;
					triSeg.Set(tri.GetOrigin()+tri.GetEdge0(), tri.GetEdge2());
					fSqrDist0 = Distance::SqrSegSeg(seg,triSeg,&fR0,&fT0);
					fS0 = 1.0f-fT0;

					if (fSqrDist0 < fSqrDist)
					{
						fSqrDist = fSqrDist0;
						fR = fR0;
						fS = fS0;
						fT = fT0;
					}
					fSqrDist0 = Distance::SqrTriPoint(tri,seg.GetDestination(),&fS0,&fT0);
					fR0 = 1.0f;

					if (fSqrDist0 < fSqrDist)
					{
						fSqrDist = fSqrDist0;
						fR = fR0;
						fS = fS0;
						fT = fT0;
					}
				}
				else if (fT < 0.0f)  // region 6p
				{
					// min on face t=0 or s+t=1 or r=1
					//
					triSeg.Set(tri.GetOrigin(), tri.GetEdge0());
					fSqrDist = Distance::SqrSegSeg(seg,triSeg,&fR,&fS);
					fT = 0.0f;
					triSeg.Set(tri.GetOrigin()+tri.GetEdge0(), tri.GetEdge2());
					fSqrDist0 = Distance::SqrSegSeg(seg,triSeg,&fR0,&fT0);
					fS0 = 1.0f-fT0;

					if (fSqrDist0 < fSqrDist)
					{
						fSqrDist = fSqrDist0;
						fR = fR0;
						fS = fS0;
						fT = fT0;
					}
					fSqrDist0 = Distance::SqrTriPoint(tri,seg.GetDestination(),&fS0,&fT0);
					fR0 = 1.0f;

					if (fSqrDist0 < fSqrDist)
					{
						fSqrDist = fSqrDist0;
						fR = fR0;
						fS = fS0;
						fT = fT0;
					}
				}
				else  // region 1p
				{
					// min on face s+t=1 or r=1
					//
					triSeg.Set(tri.GetOrigin()+tri.GetEdge0(), tri.GetEdge2());
					fSqrDist = Distance::SqrSegSeg(seg,triSeg,&fR,&fT);
					fS = 1.0f-fT;
					fSqrDist0 = Distance::SqrTriPoint(tri,seg.GetDestination(),&fS0,&fT0);
					fR0 = 1.0f;

					if (fSqrDist0 < fSqrDist)
					{
						fSqrDist = fSqrDist0;
						fR = fR0;
						fS = fS0;
						fT = fT0;
					}
				}
			}
		}
	}
	else
	{
		// segment and triangle are parallel
		//
		triSeg.Set(tri.GetOrigin(), tri.GetEdge0());
		fSqrDist = Distance::SqrSegSeg(seg,triSeg,&fR,&fS);
		fT = 0.0f;

		triSeg.SetDirection(tri.GetEdge1());
		fSqrDist0 = Distance::SqrSegSeg(seg,triSeg,&fR0,&fT0);
		fS0 = 0.0f;

		if (fSqrDist0 < fSqrDist)
		{
			fSqrDist = fSqrDist0;
			fR = fR0;
			fS = fS0;
			fT = fT0;
		}

		triSeg.Set(tri.GetOrigin()+tri.GetEdge0(), tri.GetEdge2());
		fSqrDist0 = Distance::SqrSegSeg(seg,triSeg,&fR0,&fT0);
		fS0 = 1.0f-fT0;

		if (fSqrDist0 < fSqrDist)
		{
			fSqrDist = fSqrDist0;
			fR = fR0;
			fS = fS0;
			fT = fT0;
		}

		fSqrDist0 = Distance::SqrTriPoint(tri,seg.GetOrigin(),&fS0,&fT0);
		fR0 = 0.0f;

		if (fSqrDist0 < fSqrDist)
		{
			fSqrDist = fSqrDist0;
			fR = fR0;
			fS = fS0;
			fT = fT0;
		}

		fSqrDist0 = Distance::SqrTriPoint(tri,seg.GetDestination(),&fS0,&fT0);
		fR0 = 1.0f;

		if (fSqrDist0 < fSqrDist)
		{
			fSqrDist = fSqrDist0;
			fR = fR0;
			fS = fS0;
			fT = fT0;
		}
	}

	if (pfSegP)
		*pfSegP = fR;

	if (pfTriP0)
		*pfTriP0 = fS;

	if (pfTriP1)
		*pfTriP1 = fT;

	return fSqrDist;
}

}
