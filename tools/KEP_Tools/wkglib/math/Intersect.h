/******************************************************************************
 Intersect.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "AABB.h"
#include "plane.h"
#include "sphere.h"
#include "distance.h"
#include "capsule.h"

namespace wkg 
{

class Ellipsoid;
class Capsule;
class Line;
class LineSegment;
class OBB;
class Ray;
class Sphere;
class Triangle;
class Cylinder;
class LineContactSet;
class Box;

class Intersect
{
	public:
		static real tolerance;		// defaults to TOLERANCEf

		static bool RayTri(const Ray&, const Triangle&, real* t = 0);
		static bool RayCylinder(const Ray&, const Cylinder&, real* t = 0);

		static bool LineAABB(const Line&, const AABB&, real* tmin = 0, real* tmax = 0);

		static bool SegAABB(const LineSegment&, const AABB&, real* t = 0);

		static bool SegOBB(const LineSegment&, const OBB&);

		static bool  FindIntersection(const LineSegment& rkSegment,
			const OBB &rkBox, int32 &riQuantity, Vector3 akPoint[2]);

		static bool  FindIntersection(const LineSegment& rkSegment,
			const Ellipsoid &rkSphere, int32 &riQuantity, Vector3 akPoint[2]);

		static bool PlaneAABB(const Plane&, const AABB&);

		static bool AABBAABB(const AABB&, const AABB&);
		static bool AABBLine(const AABB&, const Line&, real* tmin = 0, real* tmax = 0);
		static bool AABBSeg(const AABB&, const LineSegment&, real* t = 0);
		static bool AABBPlane(const AABB&, const Plane&);
		static bool AABBSphere(const AABB&, const Sphere&);
		static bool AABBTri(const AABB&, const Triangle&);
		static bool AABBCapsule(const AABB&, Capsule&, real* t = 0);

		static bool TriTri(const Triangle&, const Triangle&);
		static bool TriAABB(const Triangle&, const AABB&);
		static bool TriRay(const Triangle&, const Ray&, real* t = 0);
		static bool TriSphere(const Triangle&, const Sphere&);
		static bool TriCapsule(const Triangle&, const Capsule&, real* t = 0);

		static bool OBBOBB(const OBB&, const OBB&);

		static bool SphereSphere(const Sphere&, const Sphere&, Vector3 *poc = 0, Vector3 *n = 0);
		static bool SphereAABB(const Sphere&, const AABB&);
		static bool SphereTri(const Sphere&, const Triangle&);
		static bool SphereOBB(const Sphere&, const OBB&);

		static bool CapsuleTri(const Capsule&, const Triangle&, real* t = 0);
		static bool CapsuleAABB(const Capsule&, const AABB&, real* t = 0);
		static bool SphereCapsule(const Sphere &sphere, const Capsule &cap, Vector3 *poc = 0, Vector3 *n = 0);

		static bool CylinderRay(const Cylinder&, const Ray&, real* t = 0);
};



inline bool
Intersect::AABBAABB(const AABB& aabb1, const AABB& aabb2)
{
	return 
		 !(aabb1.min.x > aabb2.max.x || aabb1.max.x < aabb2.min.x
		|| aabb1.min.y > aabb2.max.y || aabb1.max.y < aabb2.min.y
		|| aabb1.min.z > aabb2.max.z || aabb1.max.z < aabb2.min.z);
}

inline bool 
Intersect::AABBPlane(const AABB& aabb, const Plane& plane)
{
	return plane.Classify(aabb, tolerance) == Plane::ZERO;
}

inline bool
Intersect::SphereSphere(const Sphere& sphere0, 
						const Sphere& sphere1,
						Vector3 *poc,
						Vector3 *n)
{
	Vector3 diff;
	real radSum;

	diff = sphere0.GetCenter() - sphere1.GetCenter();
	radSum = sphere0.GetRadius() + sphere1.GetRadius();

	bool b = diff.Dot(diff) <= SQR(radSum);
	if(b)
	{
		Vector3 our_n = (sphere1.GetCenter() - sphere0.GetCenter());
		our_n.Normalize();

		if(n)
		{
			(*n) = our_n;
		}

		if(poc)
		{
			(*poc) = sphere0.GetCenter() + (our_n * sphere0.GetRadius());
		}
	}
	return b;
}

inline bool 
Intersect::RayTri(const Ray& ray, const Triangle& tri, real* t)
{
	return TriRay(tri, ray, t);
}

inline bool 
Intersect::LineAABB(const Line& line, const AABB& aabb, real* tmin, real* tmax)
{
	return AABBLine(aabb, line, tmin, tmax);
}

inline bool 
Intersect::SegAABB(const LineSegment& seg, const AABB& aabb, real* t)
{
	return AABBSeg(aabb, seg, t);
}

inline bool 
Intersect::PlaneAABB(const Plane& plane, const AABB& aabb)
{
	return AABBPlane(aabb, plane);
}

inline bool 
Intersect::AABBTri(const AABB& aabb, const Triangle& tri)
{
	return TriAABB(tri, aabb);
}

inline bool
Intersect::AABBCapsule(const AABB& aabb, Capsule& capsule, real *t)
{
	return CapsuleAABB(capsule, aabb, t);
}

inline bool 
Intersect::SphereAABB(const Sphere& sphere, const AABB& aabb)
{
	return AABBSphere(aabb, sphere);
}

inline bool  
Intersect::SphereTri(const Sphere& sphere, const Triangle& tri)
{
	return TriSphere(tri, sphere);
}

inline bool  
Intersect::CapsuleTri(const Capsule& capsule, const Triangle& tri, real* t)
{
	return TriCapsule(tri, capsule, t);
}

inline bool 
Intersect::RayCylinder(const Ray& ray, const Cylinder& cyl, real* t)
{
	return CylinderRay(cyl, ray, t);
}

inline bool 
Intersect::SphereCapsule(const Sphere &sphere, const Capsule &cap,
						Vector3 *poc, Vector3 *n)
{
	// find out how far the center of the sphere is from the line segment
	real dsq = Distance::SqrSegPoint(cap.GetSpan(), sphere.GetCenter());
	real sphere_r = sphere.GetRadius();
	real cap_r = cap.GetRadius();

	if(dsq < (sphere_r * sphere_r + cap_r * cap_r))
	{
		if(poc || n)
		{
			// there was an intersection: find out where
			// project the point onto the line
			real t0 = cap.GetSpan().GetDirection().Dot((sphere.GetCenter() - cap.GetSpan().GetOrigin()));
			t0 /= cap.GetSpan().GetDirection().Dot(cap.GetSpan().GetDirection());
			Vector3 p = cap.GetSpan().GetOrigin() + (cap.GetSpan().GetDirection() * t0);

			// find the normal
			Vector3 nn = p - sphere.GetCenter();
			nn.Normalize();

			// and now the point of contact is from the center of the sphere out to that point
			//  by a distance of the radius of the sphere
			if(poc)
				(*poc) = sphere.GetCenter() + nn * sphere.GetRadius();

			if(n)
				(*n) = nn;

			return true;
		}
	}

	return false;
}

}