/******************************************************************************
 IntersectOBBOBB.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "wkgtypes.h"
#include "Intersect.h"
#include "OBB.h"
#include "Vector3.h"

namespace wkg {

bool 
Intersect::OBBOBB(const OBB& A, const OBB& B)
{
	Vector3 D, c0, c1, c2, absC0, absC1, absC2;
	real R, R0, R1;
	real A0dotD, A1dotD, A2dotD;

	D = B.GetCenter() - A.GetCenter();

	// A.Axis[0]
	//
	R0 = A.GetExtent(0);
	c0.x = A.GetAxis(0).Dot(B.GetAxis(0));
	c0.y = A.GetAxis(0).Dot(B.GetAxis(1));
	c0.z = A.GetAxis(0).Dot(B.GetAxis(2));
	absC0.Set(ABS(c0.x), ABS(c0.y), ABS(c0.z));
	R1 = 
		B.GetExtent(0) * absC0.x +
		B.GetExtent(1) * absC0.y +
		B.GetExtent(2) * absC0.z;
	A0dotD = A.GetAxis(0).Dot(D);
	R = ABS(A0dotD);

	if (R > R0 + R1)
		return false;

	// A.Axis[1]
	//
	R0 = A.GetExtent(1);
	c1.x = A.GetAxis(1).Dot(B.GetAxis(0));
	c1.y = A.GetAxis(1).Dot(B.GetAxis(1));
	c1.z = A.GetAxis(1).Dot(B.GetAxis(2));
	absC1.Set(ABS(c1.x), ABS(c1.y), ABS(c1.z));
	R1 = 
		B.GetExtent(0) * absC1.x +
		B.GetExtent(1) * absC1.y +
		B.GetExtent(2) * absC1.z;
	A1dotD = A.GetAxis(1).Dot(D);
	R = ABS(A1dotD);

	if (R > R0 + R1)
		return false;

	// A.Axis[2]
	//
	R0 = A.GetExtent(2);
	c2.x = A.GetAxis(2).Dot(B.GetAxis(0));
	c2.y = A.GetAxis(2).Dot(B.GetAxis(1));
	c2.z = A.GetAxis(2).Dot(B.GetAxis(2));
	absC2.Set(ABS(c2.x), ABS(c2.y), ABS(c2.z));
	R1 = 
		B.GetExtent(0) * absC2.x +
		B.GetExtent(1) * absC2.y +
		B.GetExtent(2) * absC2.z;
	A2dotD = A.GetAxis(2).Dot(D);
	R = ABS(A2dotD);

	if (R > R0 + R1)
		return false;

	// B.Axis[0]
	//
	R0 = 
		A.GetExtent(0) * absC0.x +
		A.GetExtent(1) * absC1.x +
		A.GetExtent(2) * absC2.x;
	R1 = B.GetExtent(0);
	R = ABS(B.GetAxis(0).Dot(D));

	if (R > R0 + R1)
		return false;

	// B.Axis[1]
	//
	R0 = 
		A.GetExtent(0) * absC0.y +
		A.GetExtent(1) * absC1.y +
		A.GetExtent(2) * absC2.y;
	R1 = B.GetExtent(1);
	R = ABS(B.GetAxis(1).Dot(D));

	if (R > R0 + R1)
		return false;

	// B.Axis[2]
	//
	R0 = 
		A.GetExtent(0) * absC0.z +
		A.GetExtent(1) * absC1.z +
		A.GetExtent(2) * absC2.z;
	R1 = B.GetExtent(2);
	R = ABS(B.GetAxis(2).Dot(D));

	if (R > R0 + R1)
		return false;

	// A.Axis[0].Cross(B.Axis[0])
	//
	R0 = A.GetExtent(1) * absC2.x + A.GetExtent(2) * absC1.x;
	R1 = B.GetExtent(1) * absC0.z + B.GetExtent(2) * absC0.y;
	R = ABS(A2dotD * c1.x - A1dotD * c2.x);

	if (R > R0 + R1)
		return false;

	// A.Axis[0].Cross(B.Axis[1])
	//
	R0 = A.GetExtent(1) * absC2.y + A.GetExtent(2) * absC1.y;
	R1 = B.GetExtent(0) * absC0.z + B.GetExtent(2) * absC0.x;
	R = ABS(A2dotD * c1.y - A1dotD * c2.y);

	if (R > R0 + R1)
		return false;

	// A.Axis[0].Cross(B.Axis[2])
	//
	R0 = A.GetExtent(1) * absC2.z + A.GetExtent(2) * absC1.z;
	R1 = B.GetExtent(0) * absC0.y + B.GetExtent(1) * absC0.x;
	R = ABS(A2dotD * c1.z - A1dotD * c2.z);

	if (R > R0 + R1)
		return false;

	// A.Axis[1].Cross(B.Axis[0])
	//
	R0 = A.GetExtent(0) * absC2.x + A.GetExtent(2) * absC0.x;
	R1 = B.GetExtent(1) * absC1.z + B.GetExtent(2) * absC1.y;
	R = ABS(A0dotD * c2.x - A2dotD * c0.x);

	if (R > R0 + R1)
		return false;

	// A.Axis[1].Cross(B.Axis[1])
	//
	R0 = A.GetExtent(0) * absC2.y + A.GetExtent(2) * absC0.y;
	R1 = B.GetExtent(0) * absC1.z + B.GetExtent(2) * absC1.x;
	R = ABS(A0dotD * c2.y - A2dotD * c0.y);

	if (R > R0 + R1)
		return false;

	// A.Axis[1].Cross(B.Axis[2])
	//
	R0 = A.GetExtent(0) * absC2.z + A.GetExtent(2) * absC0.z;
	R1 = B.GetExtent(0) * absC1.y + B.GetExtent(1) * absC1.x;
	R = ABS(A0dotD * c2.z - A2dotD * c0.z);

	if (R > R0 + R1)
		return false;

	// A.Axis[2].Cross(B.Axis[0])
	//
	R0 = A.GetExtent(0) * absC1.x + A.GetExtent(1) * absC0.x;
	R1 = B.GetExtent(1) * absC2.z + B.GetExtent(2) * absC2.y;
	R = ABS(A1dotD * c0.x - A0dotD * c1.x);

	if (R > R0 + R1)
		return false;

	// A.Axis[2].Cross(B.Axis[1])
	//
	R0 = A.GetExtent(0) * absC1.y + A.GetExtent(1) * absC0.y;
	R1 = B.GetExtent(0) * absC2.z + B.GetExtent(2) * absC2.x;
	R = ABS(A1dotD * c0.y - A0dotD * c1.y);

	if (R > R0 + R1)
		return false;

	// A.Axis[2].Cross(B.Axis[2])
	//
	R0 = A.GetExtent(0) * absC1.z + A.GetExtent(1) * absC0.z;
	R1 = B.GetExtent(0) * absC2.y + B.GetExtent(1) * absC2.x;
	R = ABS(A1dotD * c0.z - A0dotD * c1.z);

	if (R > R0 + R1)
		return false;

	return true;
}

}
