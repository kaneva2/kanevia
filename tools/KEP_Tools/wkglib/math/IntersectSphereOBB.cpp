/******************************************************************************
 IntersectSphereOBB.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "matrix44.h"
#include "vector3.h"
#include "intersect.h"
#include "obb.h"
#include "sphere.h"

namespace wkg 
{

bool 
Intersect::SphereOBB(const Sphere &rkSphere, const OBB &rkBox)
{
   // Test for intersection in the coordinate system of the box by
    // transforming the sphere into that coordinate system.
    Vector3 kCDiff = rkSphere.GetCenter() - rkBox.GetCenter();


    real fAx = ABS(kCDiff.Dot(rkBox.GetAxis(0)));
    real fAy = ABS(kCDiff.Dot(rkBox.GetAxis(1)));
    real fAz = ABS(kCDiff.Dot(rkBox.GetAxis(2)));
    real fDx = fAx - rkBox.GetExtent(0);
    real fDy = fAy - rkBox.GetExtent(1);
    real fDz = fAz - rkBox.GetExtent(2);

    if ( fAx <= rkBox.GetExtent(0) )
    {
        if ( fAy <= rkBox.GetExtent(1) )
        {
            if ( fAz <= rkBox.GetExtent(2) )
            {
                // sphere center inside box
                return true;
            }
            else
            {
                // potential sphere-face intersection with face z
                return fDz <= rkSphere.GetRadius();
            }
        }
        else
        {
            if ( fAz <= rkBox.GetExtent(2) )
            {
                // potential sphere-face intersection with face y
                return fDy <= rkSphere.GetRadius();
            }
            else
            {
                // potential sphere-edge intersection with edge formed
                // by faces y and z
                real fRSqr = rkSphere.GetRadius() * rkSphere.GetRadius();
                return fDy * fDy + fDz * fDz <= fRSqr;
            }
        }
    }
    else
    {
        if ( fAy <= rkBox.GetExtent(1) )
        {
            if ( fAz <= rkBox.GetExtent(2) )
            {
                // potential sphere-face intersection with face x
                return fDx <= rkSphere.GetRadius();
            }
            else
            {
                // potential sphere-edge intersection with edge formed
                // by faces x and z
                real fRSqr = rkSphere.GetRadius()*rkSphere.GetRadius();
                return fDx*fDx + fDz*fDz <= fRSqr;
            }
        }
        else
        {
            if ( fAz <= rkBox.GetExtent(2) )
            {
                // potential sphere-edge intersection with edge formed
                // by faces x and y
                real fRSqr = rkSphere.GetRadius()*rkSphere.GetRadius();
                return fDx*fDx + fDy*fDy <= fRSqr;
            }
            else
            {
                // potential sphere-vertex intersection at corner formed
                // by faces x,y,z
                real fRSqr = rkSphere.GetRadius()*rkSphere.GetRadius();
                return fDx*fDx + fDy*fDy + fDz*fDz <= fRSqr;
            }
        }
    }

	return false;

}

}
