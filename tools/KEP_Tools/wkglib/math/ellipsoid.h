
#pragma once

// Compliments of:
// Magic Software, Inc.
// http://www.magic-software.com
// http://www.wild-magic.com
// Copyright (c) 2004.  All Rights Reserved
//
// The Wild Magic Library (WML) source code is supplied under the terms of
// the license agreement http://www.magic-software.com/License/WildMagic.pdf
// and may not be copied or disclosed except in accordance with the terms of
// that agreement.

#include "vector3.h"
#include "matrix44.h"

namespace wkg
{

class Ellipsoid
{
public:
    // center-matrix form, (X-C)^T A (X-C) = 1, where A is a positive
    // definite matrix

    Ellipsoid();

    void SetCenter(Vector3 &v);
    const Vector3 &GetCenter() const;

    void SetA(Matrix44 &m);
    const Matrix44 &GetA() const;

    void SetInverseA(Matrix44 &m);
    const Matrix44 &GetInverseA() const;

protected:
    Vector3 m_kCenter;
    Matrix44 m_kA;

	// for intersection and culling support
    Matrix44 m_kInverseA;
};

}


