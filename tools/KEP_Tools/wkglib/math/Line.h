/******************************************************************************
 Line.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "Vector3.h"

namespace wkg {

class Line
{
	public:
		Line();									// uninitialized
		Line(const Vector3& pt, const Vector3& dir);

		void Set(const Vector3& pt, const Vector3& dir);
		void SetPoint(const Vector3&);			// on the line
		void SetDirection(const Vector3&);		// not necessarily unit length

		const Vector3& GetPoint() const;		// on the line
		const Vector3& GetDirection() const;	// not necessarily unit length

	public:
		Vector3 pt;								// on the line
		Vector3 dir;							// not necessarily unit length
};

inline
Line::Line()
{
	// uninitialized
}

inline void 
Line::SetPoint(const Vector3& pt)
{
	this->pt = pt;
}

inline void 
Line::SetDirection(const Vector3& dir)
{
	this->dir = dir;
}

inline void 
Line::Set(const Vector3& pt, const Vector3& dir)
{
	SetPoint(pt);
	SetDirection(dir);
}

inline
Line::Line(const Vector3& pt, const Vector3& dir)
{
	Set(pt, dir);
}

inline const Vector3& 
Line::GetPoint() const
{
	return pt;
}

inline const Vector3& 
Line::GetDirection() const
{
	return dir;
}

}
