/******************************************************************************
 Sphere.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "Vector3.h"

namespace wkg {

class Sphere
{
	public:
		Sphere();	// uninit
		Sphere(const Vector3& center, real radius);

		void Set(const Vector3& center, real radius);
		void SetCenter(const Vector3&);
		void SetRadius(real);

		const Vector3& GetCenter() const;
		const real GetRadius() const;
		const real GetRadiusSqr() const;

		SERIALIZABLE

	protected:
		Vector3 center;
		real radius;
		real radiusSqr;
};

inline 
Sphere::Sphere()
{
	// uninit
}

inline void 
Sphere::SetCenter(const Vector3& ctr)
{
	center = ctr;
}

inline void 
Sphere::SetRadius(real rad)
{
	radius = rad;
	radiusSqr = rad*rad;
}

inline void 
Sphere::Set(const Vector3& ctr, real rad)
{
	SetCenter(ctr);
	SetRadius(rad);
}

inline 
Sphere::Sphere(const Vector3& center, real radius)
{
	Set(center, radius);
}

inline const Vector3& 
Sphere::GetCenter() const
{
	return center;
}

inline const real 
Sphere::GetRadius() const
{
	return radius;
}

inline const real 
Sphere::GetRadiusSqr() const
{
	return radiusSqr;
}

}
