/******************************************************************************
 Plane.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "wkgtypes.h"
#include "Plane.h"
#include "AABB.h"

namespace wkg {

void 
Plane::Calc(const Vector3& v0, const Vector3& v1, const Vector3& v2)
{
	Vector3 v01 = v1 - v0;
	Vector3 v02 = v2 - v0;
	
	ASSERT(v01.Length() > TOLERANCEf &&
		v02.Length() > TOLERANCEf);

	normal = v01.Cross(v02);
	normal.Normalize();
	constant = - normal.Dot(v0);
}

Plane::Classification 
Plane::Classify(const AABB& box, real epsilon) const
{
	Vector3 vmin, vmax;
	Classification side;

	// find diagonal of box with end point closest to (and furthest from) the plane
	//
	if (normal.x >= 0.0f)
	{
		vmin.x = box.min.x;
		vmax.x = box.max.x;
	}
	else
	{
		vmin.x = box.max.x;
		vmax.x = box.min.x;
	}

	if (normal.y >= 0.0f)
	{
		vmin.y = box.min.y;
		vmax.y = box.max.y;
	}
	else
	{
		vmin.y = box.max.y;
		vmax.y = box.min.y;
	}

	if (normal.z >= 0.0f)
	{
		vmin.z = box.min.z;
		vmax.z = box.max.z;
	}
	else
	{
		vmin.z = box.max.z;
		vmax.z = box.min.z;
	}

	side = Classify(vmin, epsilon);
	if (side == POS)
		return POS;

	side = Classify(vmax, epsilon);
	if (side != Plane::NEG)
		return ZERO;

	return NEG;
}

}
