/******************************************************************************
 SparseMatrixNN.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

namespace wkg {

class VectorN;

class SparseMatrixNN
{
	public:
		SparseMatrixNN();
		SparseMatrixNN(uint32 N, uint32 numNonDiagonalElements);
		~SparseMatrixNN() {};

		real Get(uint32 i, uint32 j) const;        
		bool SetSequentialRow(uint32 row, real diagVal,
							  uint32 numCols, uint32* colIndices, real* values);

		uint32 GetDim() const;

		bool Multiply(const VectorN& x, VectorN& b) const; 
		bool TransposeMultiply(const VectorN &x, VectorN& b) const;

	public:
		uint32  m_N;        // number of rows (== number of cols as well)
		uint32  m_size;

		real*   m_sa;		// data values
		uint32* m_ija;		// row indicies
};

inline uint32 
SparseMatrixNN::GetDim() const  
{
	return m_N; 
}

}
