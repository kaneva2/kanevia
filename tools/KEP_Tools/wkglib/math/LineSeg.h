/******************************************************************************
 LineSeg.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "Vector3.h"

namespace wkg {

class LineSegment			// lineseg == from + dir*t   0 <= t <= 1   (note: dir is not necessarily unit length)
{
	public:
		LineSegment();		// uninitialized

		void Set(const Vector3& from, const Vector3& dir);			// remember the "far" endpoint is from + dir
		void SetEndPoints(const Vector3& from, const Vector3& to);	// *NOT* from, dir!
		void SetOrigin(const Vector3& from);
		void SetDirection(const Vector3& dir);						// remember the "far" endpoint is from + dir
		void SetDestination(const Vector3& dest);

		const Vector3& GetOrigin() const;
		const Vector3& GetDirection() const;
		Vector3 GetDestination() const;

		SERIALIZABLE

	public:
		Vector3 from;
		Vector3 dir;		// not necessarily unit length
};

inline
LineSegment::LineSegment() 
{
	/* uninitialized */ 
}

inline void 
LineSegment::SetOrigin(const Vector3& org)
{
	from = org;
}

inline void 
LineSegment::SetDirection(const Vector3& direct)
{
	dir = direct;
}

inline void 
LineSegment::SetDestination(const Vector3& dest)
{
	SetDirection(dest - from);
}

inline void 
LineSegment::SetEndPoints(const Vector3& org, const Vector3& dest)
{
	SetOrigin(org);
	SetDestination(dest);
}

inline void 
LineSegment::Set(const Vector3& org, const Vector3& direct)
{
	SetOrigin(org);
	SetDirection(direct);
}

inline const Vector3& 
LineSegment::GetOrigin() const
{
	return from;
}

inline const Vector3& 
LineSegment::GetDirection() const
{
	return dir;
}

inline Vector3 
LineSegment::GetDestination() const
{
	return from + dir;
}

}
