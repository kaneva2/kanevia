/******************************************************************************
 OBB.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "wkgtypes.h"
#include "OBB.h"
#include "Matrix44.h"
#include "Vector4.h"

namespace wkg {

void 
OBB::ComputeVerts(Vector3* verts) const
{
	Vector3 axisExtent[3];

	axisExtent[0] = axis[0] * extent[0];
	axisExtent[1] = axis[1] * extent[1];
	axisExtent[2] = axis[2] * extent[2];

	verts[0] = center + axisExtent[0] + axisExtent[1] + axisExtent[2];
	verts[1] = center - axisExtent[0] - axisExtent[1] - axisExtent[2];

	verts[2] = center + axisExtent[0] + axisExtent[1] - axisExtent[2];
	verts[3] = center - axisExtent[0] - axisExtent[1] + axisExtent[2];

	verts[4] = center + axisExtent[0] - axisExtent[1] + axisExtent[2];
	verts[5] = center - axisExtent[0] + axisExtent[1] - axisExtent[2];

	verts[6] = center + axisExtent[0] - axisExtent[1] - axisExtent[2];
	verts[7] = center - axisExtent[0] + axisExtent[1] + axisExtent[2];
}

void 
OBB::Transform(const Matrix44& mat, OBB* result) const
{
	Vector4 v4;

	result->extent[0] = extent[0];
	result->extent[1] = extent[1];
	result->extent[2] = extent[2];

	v4.Set(center.x, center.y, center.z, 1.0f);
	v4 *= mat;
	result->center.Set(v4.x, v4.y, v4.z);

	v4.Set(axis[0].x, axis[0].y, axis[0].z, 0.0f);
	v4 *= mat;
	result->axis[0].Set(v4.x, v4.y, v4.z);

	v4.Set(axis[1].x, axis[1].y, axis[1].z, 0.0f);
	v4 *= mat;
	result->axis[1].Set(v4.x, v4.y, v4.z);

	v4.Set(axis[2].x, axis[2].y, axis[2].z, 0.0f);
	v4 *= mat;
	result->axis[2].Set(v4.x, v4.y, v4.z);
}

#if 0
#ifdef USE_SERIALIZATION
}
#include "Stream.h"
namespace wkg {

bool 
OBB::Load(Stream* stream)
{
	return center.Load(stream)
		&& axis[0].Load(stream)
		&& axis[1].Load(stream)
		&& axis[2].Load(stream)
		&& stream->Read(&extent[0])
		&& stream->Read(&extent[1])
		&& stream->Read(&extent[2]);
}

bool 
OBB::Save(Stream* stream) const
{
	return center.Save(stream)
		&& axis[0].Save(stream)
		&& axis[1].Save(stream)
		&& axis[2].Save(stream)
		&& stream->Write(extent[0])
		&& stream->Write(extent[1])
		&& stream->Write(extent[2]);
}

#endif
#endif

}

