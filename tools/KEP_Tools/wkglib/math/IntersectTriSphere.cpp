/******************************************************************************
 IntersectTriSphere.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "wkgtypes.h"
#include "Intersect.h"
#include "Triangle.h"
#include "Sphere.h"
#include "Distance.h"
#include "lineseg.h"
#include "linecontactset.h"

namespace wkg 
{

bool 
Intersect::TriSphere(const Triangle& tri, const Sphere& sphere)
{
    return Distance::SqrTriPoint(tri, sphere.GetCenter()) < sphere.GetRadiusSqr();
}

}
