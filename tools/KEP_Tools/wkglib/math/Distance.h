/******************************************************************************
 Distance.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

namespace wkg {

class LineSegment;
class Triangle;
class Vector3;
class AABB;
class Line;

class Distance
{
	public:
		static real tolerance;		// defaults to TOLERANCEf

		static real SqrTriPoint(const Triangle&,	const Vector3&,		real* sTri = 0, real* tTri = 0);
		static real SqrTriSeg(const Triangle&,		const LineSegment&,	real* sTri = 0, real* tTri = 0, real* rSeg = 0);

		static real SqrSegPoint(const LineSegment&,	const Vector3&);
		static real SqrSegSeg(const LineSegment&,	const LineSegment&, real* tSeg0 = 0, real* tSeg1 = 0);
		
		static real SqrSegAABB(const LineSegment&,	const AABB&,		real* tSeg = 0, real* tBoxAxis0 = 0, real* tBoxAxis1 = 0, real* tBoxAxis2 = 0);
		static real SqrLineAABB(const Line&,		const AABB&,		real* tSeg = 0, real* tBoxAxis0 = 0, real* tBoxAxis1 = 0, real* tBoxAxis2 = 0);
		static real SqrPointAABB(const Vector3&,	const AABB&,		real* tSeg = 0, real* tBoxAxis0 = 0, real* tBoxAxis1 = 0, real* tBoxAxis2 = 0);
};

}
