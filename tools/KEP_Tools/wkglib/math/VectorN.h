/******************************************************************************
 VectorN.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/*                                                                           */
/*                                                                           */
/*                   MV++ Numerical Matrix/Vector C++ Library                */
/*                             MV++ Version 1.5                              */
/*                                                                           */
/*                                  R. Pozo                                  */
/*               National Institute of Standards and Technology              */
/*                                                                           */
/*                                  NOTICE                                   */
/*                                                                           */
/* Permission to use, copy, modify, and distribute this software and         */
/* its documentation for any purpose and without fee is hereby granted       */
/* provided that this permission notice appear in all copies and             */
/* supporting documentation.                                                 */
/*                                                                           */
/* Neither the Institution (National Institute of Standards and Technology)  */
/* nor the author makes any representations about the suitability of this    */
/* software for any purpose.  This software is provided ``as is''without     */
/* expressed or implied warranty.                                            */
/*                                                                           */
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

//
//      mv_vector_double.h       Basic vector class (double precision)
//

#pragma once

namespace wkg {

class SparseMatrixNN;

class VectorN
{                                                                      
    public:                                                            
		VectorN();                             
		VectorN(uint32);                             
		VectorN(const real*, uint32);      
		VectorN(const VectorN &); 
		~VectorN();                              
                                                                       
		real& operator[](uint32 i)				{  ASSERT(m_p); return m_p[i];  }
		const real& operator[](uint32 i) const	{  ASSERT(m_p); return m_p[i];  }

		void operator+=(const VectorN&);
		void operator-=(const VectorN&);
		void operator*=(real);

		real Dot(const VectorN&) const;
		real Length() const;
		real LengthSqr() const;

		inline uint32 Size() const { return m_dim;}
		inline uint32 Dim()  const { return m_dim;}
		inline bool IsNull() const { return m_dim == 0 || !m_p;}

		// Create a new *uninitalized* vector of size N
		void NewSize(uint32);

		void AddScaledVector(real scalar, const VectorN& vec);
                                                                       
		VectorN& operator=(const VectorN&);

    protected:                                                           
		real	*m_p;
		uint32	m_dim;


		friend class SparseMatrixNN;  // so matrix multiply doesn't have to use accessor funcs
};                                                                     

}	// end namespace
