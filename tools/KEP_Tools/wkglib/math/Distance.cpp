/******************************************************************************
 Distance.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "wkgtypes.h"
#include "Distance.h"

namespace wkg {

real Distance::tolerance = TOLERANCEf;

}