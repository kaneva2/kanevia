
#include "wkgtypes.h"
#include "ellipsoid.h"

// compliments of:
// Magic Software, Inc.
// http://www.magic-software.com
// http://www.wild-magic.com
// Copyright (c) 2004.  All Rights Reserved
//
// The Wild Magic Library (WML) source code is supplied under the terms of
// the license agreement http://www.magic-software.com/License/WildMagic.pdf
// and may not be copied or disclosed except in accordance with the terms of
// that agreement.

namespace wkg
{

Ellipsoid::Ellipsoid()
{
}

void
Ellipsoid::SetCenter(Vector3 &c)
{
    m_kCenter = c;
}

const Vector3 &
Ellipsoid::GetCenter() const
{
    return m_kCenter;
}

void
Ellipsoid::SetA(Matrix44 &m)
{
    m_kA = m;
}

const Matrix44 &
Ellipsoid::GetA() const
{
    return m_kA;
}

void
Ellipsoid::SetInverseA(Matrix44 &m)
{
    m_kInverseA = m;
}

const Matrix44 &
Ellipsoid::GetInverseA() const
{
    return m_kInverseA;
}

}

