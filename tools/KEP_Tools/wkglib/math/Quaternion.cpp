/******************************************************************************
 Quaternion.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "wkgtypes.h"
#include "Quaternion.h"
#include "Matrix44.h"

namespace wkg {

const Quaternion Quaternion::IDENTITY(0.0f, 0.0f, 0.0f, 1.0f);

Quaternion::operator Matrix44() const
{
	return ToMat();
}

void
Quaternion::FromMat(const Matrix44& m)
{
#if 1
	D3DXQUATERNION xq;
	D3DXMATRIX xm;
	xm._11 = m._11;	xm._12 = m._12;	xm._13 = m._13;	xm._14 = m._14;
	xm._21 = m._21;	xm._22 = m._22;	xm._23 = m._23;	xm._24 = m._24;
	xm._31 = m._31;	xm._32 = m._32;	xm._33 = m._33;	xm._34 = m._34;
	xm._41 = m._41;	xm._42 = m._42;	xm._43 = m._43;	xm._44 = m._44;

	D3DXQuaternionRotationMatrix(&xq, &xm);
	this->x = xq.x;	this->y = xq.y;	this->z = xq.z;	this->w = xq.w;
#else
	real tr, s;

	tr = m._11 + m._22 + m._33;

	if (tr > 0.0f)
	{
		s = sqrt(tr + 1.0f);
		w = s * 0.5f;
		s = 0.5f / s;

		v.Set((m._23 - m._32) * s, (m._31 - m._13) * s, (m._12 - m._21) * s);
	}
	else
	{
		int32 i, j, k;

		i = 0;
		if (m._22 > m._11)
			i = 1;
		if (m._33 > m.m[i][i])
			i = 2;

		j = i < 2 ? i + 1 : 0;
		k = j < 2 ? j + 1 : 0;

		s = sqrt((m.m[i][i] - (m.m[j][j] + m.m[k][k])) + 1.0f);

		q[i] = s * 0.5f;
		s = 0.5f / s;

		q[3] = (m.m[j][k] - m.m[k][j]) * s;
		q[j] = (m.m[i][j] - m.m[j][i]) * s;
		q[k] = (m.m[i][k] - m.m[k][i]) * s;
	}
#endif
}

Matrix44
Quaternion::ToMat() const
{
#if 1
	D3DXQUATERNION xq;
	xq.x = this->x;	xq.y = this->y;	xq.z = this->z; xq.w = this->w;

	D3DXMATRIX xm;
	D3DXMatrixRotationQuaternion(&xm, &xq);


	Matrix44 m;
	m._11 = xm._11;	m._12 = xm._12;	m._13 = xm._13;	m._14 = xm._14;
	m._21 = xm._21;	m._22 = xm._22;	m._23 = xm._23;	m._24 = xm._24;
	m._31 = xm._31;	m._32 = xm._32;	m._33 = xm._33;	m._34 = xm._34;
	m._41 = xm._41;	m._42 = xm._42;	m._43 = xm._43;	m._44 = xm._44;

	return m;

#else
	Matrix44 m;
	Vector3 vs;
	real wx, wy, wz, xx, xy, xz, yy, yz, zz;

	vs = v * 2.0f;
	wx = w * vs.x;
	wy = w * vs.y;
	wz = w * vs.z;
	xx = x * vs.x;
	xy = x * vs.y;
	xz = x * vs.z;
	yy = y * vs.y;
	yz = y * vs.z;
	zz = z * vs.z;

	m._11 = 1.0f - (yy + zz);	m._12 = xy + wz;			m._13 = xz - wy;
	m._21 = xy - wz;			m._22 = 1.0f - (xx + zz);	m._23 = yz + wx;
	m._31 = xz + wy;			m._32 = yz - wx;			m._33 = 1.0f - (xx + yy);

	m._14 = m._24 = m._34 = m._41 = m._42 = m._43 = 0.0f;
	m._44 = 1.0f;

#endif
}


Quaternion 
Quaternion::Slerp(const Quaternion& q, const real t) const
{
#if 1
	D3DXQUATERNION q1;
	q1.x = this->x;
	q1.y = this->y;
	q1.z = this->z;
	q1.w = this->w;

	D3DXQUATERNION q2;
	q2.x = q.x;
	q2.y = q.y;
	q2.z = q.z;
	q2.w = q.w;

	D3DXQUATERNION out_q;

	D3DXQuaternionSlerp(&out_q, &q1, &q2, t); 

	Quaternion our_out;
	our_out.x = out_q.x;
	our_out.y = out_q.y;
	our_out.z = out_q.z;
	our_out.w = out_q.w;

	return our_out;


#else
	Quaternion out;
	real omega, cosom, sinom, sclp, sclq;

	cosom = x*q.x + y*q.y + z*q.z + w*q.w;

	if (1.0f + cosom > TOLERANCEf)
	{
		if (1.0f - cosom > TOLERANCEf)
		{
			omega = acos(cosom);
			sinom = sin(omega);
			sclp = sin((1.0f - t) * omega) / sinom;
			sclq = sin(t * omega) / sinom;
		}
		else
		{
			sclp = 1.0f - t;
			sclq = t;
		}

		out.Set(
			x * sclp + q.x * sclq,
			y * sclp + q.y * sclq,
			z * sclp + q.z * sclq,
			w * sclp + q.w * sclq);
	}
	else
	{
		out.x = -y;
		out.y = x;
		out.z = -w;
		out.w = z;
		sclp = sin((1.0f - t) * WKG_PIOVER2);
		sclq = sin(t * WKG_PIOVER2);

		out.Set(
			x * sclp + out.x * sclq,
			y * sclp + out.y * sclq,
			z * sclp + out.z * sclq,
			out.w);
	}

	return out;
#endif
}


const Quaternion& Quaternion::SetToAngleBetweenVectors(const Vector3& v0,
													   const Vector3& v1)
{
	Vector3 a(v0);
	Vector3 b(v1);
	a.Normalize();
	b.Normalize();
	Vector3 cross(a.Cross(b));
	real dot = a.Dot(b);
	real s = sqrt((1+dot)*2);
	if(EQUAL(0.0f, s))
	{
		x = 0.0f;
		y = 0.0f;
		z = 0.0f;
		w = 1.0f;
	}
	else
	{
		x = cross.x / s;
		y = cross.y / s;
		z = cross.z / s;
		w = s / 2.0f;
	}

	return *this;
}


#ifdef USE_SERIALIZATION
}
#include "Stream.h"
namespace wkg {

bool 
Quaternion::Load(Stream* stream)
{
	return stream->Read(&x)
		&& stream->Read(&y)
		&& stream->Read(&z)
		&& stream->Read(&w);
}

bool 
Quaternion::Save(Stream* stream) const
{
	return stream->Write(x)
		&& stream->Write(y)
		&& stream->Write(z)
		&& stream->Write(w);
}

#endif

}
