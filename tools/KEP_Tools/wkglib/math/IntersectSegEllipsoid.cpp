/******************************************************************************
 IntersectSegEllipsoid.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "wkgtypes.h"
#include "Intersect.h"
#include "Ellipsoid.h"
#include "LineSeg.h"
#include "Vector3.h"
#include "vector4.h"

namespace wkg 
{

bool 
Intersect::FindIntersection(
	const LineSegment &rkSegment,
    const Ellipsoid &rkEllipsoid3, 
	int32 &riQuantity, 
	Vector3 akPoint[2])
{
	// set up quadratic Q(t) = a*t^2 + 2*b*t + c
    Vector3 kDiff = rkSegment.GetOrigin() - rkEllipsoid3.GetCenter();

	// assume identity matrix for now
//    Vector3 kMatDir = rkSegment.GetDirection();
//    Vector3 kMatDiff = kDiff;

	Vector3 rKSegmentDir(rkSegment.GetDirection());

	Vector4 rkSegmentDir4(rKSegmentDir.x, rKSegmentDir.y, rKSegmentDir.z, 1.0);
	Vector4 kDiff4(kDiff.x, kDiff.y, kDiff.z, 1.0f);

    Vector4 kMatDir4 = rkSegmentDir4 * rkEllipsoid3.GetA();
    Vector4 kMatDiff4 = kDiff4 * rkEllipsoid3.GetA();

	Vector3 kMatDir(kMatDir4.x, kMatDir4.y, kMatDir4.z);
	Vector3 kMatDiff(kMatDiff4.x, kMatDiff4.y, kMatDiff4.z);

	real fA = kMatDir.Dot(kMatDir);
	real fB = kMatDir.Dot(kMatDiff);
	real fC = kMatDiff.Dot(kMatDiff) - 1.0f;

//    real fA = rkSegment.GetDirection().Dot(kMatDir);
//    real fB = rkSegment.GetDirection().Dot(kMatDiff);
//    real fC = kDiff.Dot(kMatDiff) - (real)1.0f;

    // no intersection if Q(t) has no real roots
    real afT[2];
    real fDiscr = (fB * fB - fA * fC);

    if(fDiscr < (real)0.0f)
    {
        riQuantity = 0;
        return false;
    }
    else if(fDiscr > (real)0.0f)
    {
        real fRoot = sqrt(fDiscr);
        real fInvA = ((real)1.0f) / fA;
        afT[0] = (-fB - fRoot) * fInvA;
        afT[1] = (-fB + fRoot) * fInvA;

        // assert: t0 < t1 since A > 0
        if(afT[0] > (real)1.0f || afT[1] < (real)0.0f)
        {
            riQuantity = 0;
            return false;
        }
        else if(afT[0] >= (real)0.0)
        {
            if(afT[1] > (real)1.0)
            {
                riQuantity = 1;
                akPoint[0] = rkSegment.GetOrigin() + rkSegment.GetDirection() *  afT[0];

                return true;
            }
            else
            {
                riQuantity = 2;

                akPoint[0] = rkSegment.GetOrigin() + rkSegment.GetDirection() * afT[0];
                akPoint[1] = rkSegment.GetOrigin() + rkSegment.GetDirection() * afT[1];

                return true;
            }
        }
        else  // afT[1] >= 0
        {
            riQuantity = 1;
            akPoint[0] = rkSegment.GetOrigin() + rkSegment.GetDirection() * afT[1];

            return true;
        }
    }
    else
    {
        afT[0] = -fB/fA;
        if ((real)0.0 <= afT[0] && afT[0] <= (real)1.0)
        {
            riQuantity = 1;
            akPoint[0] = rkSegment.GetOrigin() + rkSegment.GetDirection() * afT[0];

            return true;
        }
        else
        {
            riQuantity = 0;

            return false;
        }
    }
}

}
