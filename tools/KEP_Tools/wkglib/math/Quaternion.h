/******************************************************************************
 Quaternion.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "Vector3.h"

namespace wkg {

class Matrix44;

class Quaternion
{
	public:
		Quaternion();	// uninitiailized
		Quaternion(const Matrix44&);

		Quaternion(const real x, const real y, const real z, const real w);	// *NOT* euler angles...quat xyzw (xyz vector, w scalar)
		Quaternion(const Vector3&, const real w);							// *NOT* euler angles...quat xyzw (xyz vector, w scalar)

		const Quaternion& Identity();

		const Quaternion& Set(const real x, const real y, const real z, const real w);	// *NOT* euler angles...quat xyzw (xyz vector, w scalar)
		const Quaternion& Set(const Vector3& v, real w);									// *NOT* euler angles
			
		const Quaternion& SetToAngleBetweenVectors(const Vector3& v0, const Vector3& v1);

		operator Matrix44() const;

		Quaternion operator+(const real) const;
		Quaternion operator-(const real) const;
		Quaternion operator*(const real) const;
		Quaternion operator/(const real) const;

		Quaternion operator+(const Quaternion&) const;
		Quaternion operator-(const Quaternion&) const;
		Quaternion operator*(const Quaternion&) const;

		const Quaternion& operator+=(const Quaternion&);
		const Quaternion& operator-=(const Quaternion&);
		const Quaternion& operator*=(const Quaternion&);

		const Quaternion& operator+=(const real);
		const Quaternion& operator-=(const real);
		const Quaternion& operator*=(const real);
		const Quaternion& operator/=(const real);

		const Quaternion& Normalize();

		void RotationScale(real scale);

		real Length() const;
		real LengthSqr() const;

		real Norm() const;
		const Quaternion& Conjugate();
		const Quaternion& Inverse();

		Quaternion Slerp(const Quaternion&, const real t) const;

		operator real*();
		operator const real*() const;

		void Dump();

		SERIALIZABLE

	protected:
		Matrix44 ToMat() const;
		void FromMat(const Matrix44&);

	public:
		static const Quaternion IDENTITY;	// 0.0f, 0.0f, 0.0f, 1.0f

	public:
		union
		{
			struct
			{
				real x, y, z, w;
			};

			real q[4];

			struct
			{
				Vector3 v;
				real s;
			};
		};
};

inline
Quaternion::Quaternion()
{
	// uninit
}

inline
Quaternion::Quaternion(const real qx, const real qy, const real qz, const real qw)
	: x(qx), y(qy), z(qz), w(qw)
{
}

inline
Quaternion::Quaternion(const Vector3& v, const real qw)
	: x(v.x), y(v.y), z(v.z), w(qw)
{
}

inline
Quaternion::Quaternion(const Matrix44& mat)
{
	FromMat(mat);
}

inline const Quaternion&
Quaternion::Set(const real qx, const real qy, const real qz, const real qw)
{
	x = qx;
	y = qy;
	z = qz;
	w = qw;

	return *this;
}

inline const Quaternion&
Quaternion::Set(const Vector3& v, real s)
{
	Set(v.x, v.y, v.z, s);
	return *this;
}

inline const Quaternion& 
Quaternion::Identity()
{
	return Set(0.0f, 0.0f, 0.0f, 1.0f);
}

inline Quaternion 
Quaternion::operator+(const real scalar) const
{
	return Quaternion(x + scalar, y + scalar, z + scalar, w + scalar);
}

inline Quaternion
Quaternion::operator-(const real scalar) const
{
	return Quaternion(x - scalar, y - scalar, z - scalar, w - scalar);
}

inline Quaternion 
Quaternion::operator*(const real scalar) const
{
	return Quaternion(x * scalar, y * scalar, z * scalar, w * scalar);
}

inline Quaternion
Quaternion::operator/(const real scalar) const
{
	ASSERT(scalar != 0.0f);
	return operator*(1.0f / scalar);
}

inline Quaternion 
Quaternion::operator+(const Quaternion& q) const
{
	return Quaternion(x + q.x, y + q.y, z + q.z, w + q.w);
}

inline Quaternion 
Quaternion::operator-(const Quaternion& q) const
{
	return Quaternion(x - q.x, y - q.y, z - q.z, w - q.w);
}

inline Quaternion 
Quaternion::operator*(const Quaternion& q) const
{
	return Quaternion(Vector3(q.v * s + v * q.s + v.Cross(q.v)), s * q.s - v.Dot(q.v));
}

inline const Quaternion&
Quaternion::operator+=(const Quaternion& q)
{
	x += q.x;
	y += q.y;
	z += q.z;
	w += q.w;

	return *this;
}

inline const Quaternion&
Quaternion::operator-=(const Quaternion& q)
{
	x -= q.x;
	y -= q.y;
	z -= q.z;
	w -= q.w;

	return *this;
}

inline const Quaternion&
Quaternion::operator*=(const Quaternion& q)
{
	*this = operator*(q);

	return *this;
}

inline const Quaternion& 
Quaternion::operator+=(const real scalar)
{
	x += scalar;
	y += scalar;
	z += scalar;
	w += scalar;

	return *this;
}

inline const Quaternion& 
Quaternion::operator-=(const real scalar)
{
	x -= scalar;
	y -= scalar;
	z -= scalar;
	w -= scalar;

	return *this;
}

inline const Quaternion& 
Quaternion::operator*=(const real scalar)
{
	x *= scalar;
	y *= scalar;
	z *= scalar;
	w *= scalar;

	return *this;
}

inline const Quaternion& 
Quaternion::operator/=(const real scalar)
{
	ASSERT(scalar != 0.0f);

	return *this *= (1.0f / scalar);
}

inline real
Quaternion::LengthSqr() const
{
	return v.LengthSqr() + s*s;
}

inline real
Quaternion::Length() const
{
	return sqrt(LengthSqr());
}

inline const Quaternion&
Quaternion::Normalize()
{
	return *this /= Length();
}

inline real
Quaternion::Norm() const
{
	return LengthSqr();
}

inline const Quaternion&
Quaternion::Conjugate()
{
	x = -x;
	y = -y;
	z = -z;

	return *this;
}

inline const Quaternion&
Quaternion::Inverse()
{
	return Conjugate();
}

inline void 
Quaternion::RotationScale(real scale)
{
	*this = IDENTITY.Slerp(*this, scale);
}

inline 
Quaternion::operator real*()
{
	return q;
}

inline 
Quaternion::operator const real*() const
{
	return q;
}

inline void 
Quaternion::Dump()
{
	DBPRINTLN(("{%.2f, %.2f, %.2f, %.2f}", x, y, z, w));
}

}
