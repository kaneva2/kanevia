#pragma once

namespace wkg {

// Based upon the following (v 1.4):
//
// Magic Software, Inc.
// http://www.magic-software.com
// Copyright (c) 2000-2002.  All Rights Reserved
//
// Source code from Magic Software is supplied under the terms of a license
// agreement and may not be copied or disclosed except in accordance with the
// terms of that agreement.  The various license agreements may be found at
// the Magic Software web site.  This file is subject to the license
//
// FREE SOURCE CODE
// http://www.magic-software.com/License/free.pdf
//
class Eigen
{
	public:
		Eigen (int32 size);
		~Eigen ();

		// set the matrix for eigensolving
		//
		real& Matrix (int32 row, int32 col);
		void SetMatrix (real** mat33);

		// get the results of eigensolving (eigenvectors are columns of matrix)
		//
		real GetEigenvalue (int32 i) const;
		real GetEigenvector (int32 row, int32 col) const;
		real* GetEigenvalue ();
		real** GetEigenvector ();

		// solve eigensystem
		//
		void EigenStuff2();
		void EigenStuff3();
		void EigenStuff4();
		void EigenStuffN();
		void EigenStuff();

		// solve eigensystem, use decreasing sort on eigenvalues
		//
		void DecrSortEigenStuff2();
		void DecrSortEigenStuff3();
		void DecrSortEigenStuff4();
		void DecrSortEigenStuffN();
		void DecrSortEigenStuff();

		// solve eigensystem, use increasing sort on eigenvalues
		//
		void IncrSortEigenStuff2();
		void IncrSortEigenStuff3();
		void IncrSortEigenStuff4();
		void IncrSortEigenStuffN();
		void IncrSortEigenStuff();

	protected:
		int32 m_iSize;
		real** m_aafMat;
		real* m_afDiag;
		real* m_afSubd;

		// Householder reduction to tridiagonal form
		//
		static void Tridiagonal2(real** mat, real* diag, real* subd);
		static void Tridiagonal3(real** mat, real* diag, real* subd);
		static void Tridiagonal4(real** mat, real* diag, real* subd);
		static void TridiagonalN(int32 size, real** mat, real* diag, real* subd);

		// QL algorithm with implicit shifting, applies to tridiagonal matrices
		//
		static bool QLAlgorithm(int32 size, real* diag, real* subd, real** mat);

		// sort eigenvalues from largest to smallest
		//
		static void DecreasingSort(int32 size, real* eigval, real** eigvec);

		// sort eigenvalues from smallest to largest
		//
		static void IncreasingSort(int32 size, real* eigval, real** eigvec);
};

inline real& 
Eigen::Matrix(int32 row, int32 col)
{
    return m_aafMat[row][col];
}

inline real 
Eigen::GetEigenvalue(int32 i) const
{
    return m_afDiag[i];
}

inline real 
Eigen::GetEigenvector(int32 row, int32 col) const
{
    return m_aafMat[row][col];
}

inline real* 
Eigen::GetEigenvalue()
{
    return m_afDiag;
}

inline real** 
Eigen::GetEigenvector()
{
    return m_aafMat;
}

}
