/******************************************************************************
 Plane.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "Vector3.h"

namespace wkg {

class AABB;

class Plane
{
	public:
		Plane();	// uninitialized

		void Set(real A, real B, real C, real D);

		void Calc(const Vector3& v0, const Vector3& v1, const Vector3& v2);	// clockwise, left-handed system; doesn't check if any of the points are collinear

		void Flip();	// face in opposite direction

		enum Classification { ZERO, POS, NEG };
		Classification Classify(const Vector3& point, real epsilon = TOLERANCEf) const;
		Classification Classify(const class AABB& box, real epsilon = TOLERANCEf) const;	// which half-space does the box lie within (ZERO means it straddles the plane)
		Classification Classify(real dist, real epsilon = TOLERANCEf) const;

  		Vector3 ClosestPointTo(const Vector3& point) const;
		Vector3 UnitClosestPointTo(const Vector3& point) const;

		SERIALIZABLE

	public:
		Vector3 normal;
		real	constant;
};

inline
Plane::Plane()
{
	/* uninitialized */ 
}

inline void 
Plane::Set(real A, real B, real C, real D)
{
	normal.x = A;
	normal.y = B;
	normal.z = C;
	constant = D;
	ASSERT(EQUAL(normal.Length(), 1.0f));
}

inline void 
Plane::Flip()
{
	Set(-normal.x, -normal.y, -normal.z, -constant);
}

inline Plane::Classification 
Plane::Classify(real dist, real epsilon) const
{
	if (dist < -epsilon)
		return NEG;
	else if (dist > epsilon)
		return POS;
	else
	{
		ASSERT(EQUAL(dist, 0.0f, epsilon));
		return ZERO;
	}
}

inline Plane::Classification 
Plane::Classify(const Vector3& point, real epsilon) const
{
	real dist = normal.Dot(point) + constant;

	return Classify(dist, epsilon);
}

inline Vector3 
Plane::UnitClosestPointTo(const Vector3& point) const
{
	real k;

	k = normal.Dot(point) + constant;

	return point - normal * k;
}

inline Vector3 
Plane::ClosestPointTo(const Vector3& point) const
{
	real k;

	// NOTE:  in general the plane's normal has not been normalized (k's denominator != 1)
	//
	k = (normal.Dot(point) + constant) / normal.Dot(normal);

	return point - normal * k;
}

}
