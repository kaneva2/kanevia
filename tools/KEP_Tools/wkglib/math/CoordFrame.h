/******************************************************************************
 CoordFrame.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "Vector3.h"

namespace wkg {

class Matrix44;
class Quaternion;
class Vector4;

class Axis3D
{
	public:
		Axis3D();		// uninitialized
		virtual ~Axis3D();

		void SetLH(const Vector3& dirVec, const Vector3& upHint);
		void SetLH(const Vector4& dirVec, const Vector4& upHint);
		void SetLH(const Quaternion& orientation);
		void SetLH(real dirX, real dirY, real dirZ, real upHintX, real upHintY, real upHintZ);

		void SetRH(const Vector3& dirVec, const Vector3& upHint);
		void SetRH(const Vector4& dirVec, const Vector4& upHint);
		void SetRH(const Quaternion& orientation);
		void SetRH(real dirX, real dirY, real dirZ, real upHintX, real upHintY, real upHintZ);

		const Vector3& GetXAxis() const;
		const Vector3& GetYAxis() const;
		const Vector3& GetZAxis() const;

		Matrix44& GetXYZToAxis3D(Matrix44& result) const;	// rotate standard basis (XYZ axis) into this set of axis...mult by this matrix to get Axis3D coords for vectors relative to the XYZ coord sys
		Matrix44& GetAxis3DToXYZ(Matrix44& result) const;	// rotate axis coord sys into standard basis (XYZ axis).....mult by this matrix to get XYZ coords for vectors relative to the Axis3D coord sys

	protected:
		Vector3 right, at, up;
};

class CoordFrame
{
	public:
		virtual ~CoordFrame();

		virtual void SetLookAt(const Vector3& origin, const Vector3& lookAt, const Vector3& upHint) = 0;
		virtual void SetLookDir(const Vector3& origin, const Vector3& lookDir, const Vector3& upHint) = 0;
		virtual void SetLookDir(const Vector3& origin, const Quaternion& orientation) = 0;

		void SetLookAt(const Vector4& origin, const Vector4& lookAt, const Vector4& upHint);
		void SetLookDir(const Vector4& origin, const Vector4& lookDir, const Vector4& upHint);
		void SetLookDir(const Vector4& origin, const Quaternion& orientation);

		void SetLookAt(real originX, real originY, real originZ, real lookAtX, real lookAtY, real lookAtZ, real upHintX, real upHintY, real upHintZ);
		void SetLookDir9(real originX, real originY, real originZ, real lookDirX, real lookDirY, real lookDirZ, real upHintX, real upHintY, real upHintZ);	// the '9' is there to get around an *annoying* compiler bug (take out the 9 and the compiler will be unable to find this method)
		void SetLookDir(real originX, real originY, real originZ, const Quaternion& orientation);

		const Vector3& GetOrigin() const;
		const Axis3D& GetAxis() const;
		const Vector3& GetXAxis() const;
		const Vector3& GetYAxis() const;
		const Vector3& GetZAxis() const;

		Matrix44& GetXYZToCFrame(Matrix44& result) const;	// rotate points from standard basis (XYZ axis) into this CoordFrame...mult by this matrix to get CoordFrame coords for points relative to the XYZ coord sys
		Matrix44& GetCFrameToXYZ(Matrix44& result) const;	// rotate points from CoordFrame into standard basis (XYZ axis)........mult by this matrix to get XYZ coords for points relative to the CoordFrame coord sys...(view matrix)

		Matrix44& GetViewMatrix(Matrix44& result) const { return GetCFrameToXYZ(result); }	// ("CFrameToXYZ" matrix)

	protected:
		Vector3 origin;
		Axis3D	axis;
};

class CoordFrameLH : public CoordFrame	// left-handed coord frame
{
	public:
		virtual void SetLookAt(const Vector3& origin, const Vector3& lookAt, const Vector3& upHint);
		virtual void SetLookDir(const Vector3& origin, const Vector3& lookDir, const Vector3& upHint);
		virtual void SetLookDir(const Vector3& origin, const Quaternion& orientation);
};

class CoordFrameRH : public CoordFrame	// right-handed coord frame
{
	public:
		virtual void SetLookAt(const Vector3& origin, const Vector3& lookAt, const Vector3& upHint);
		virtual void SetLookDir(const Vector3& origin, const Vector3& lookDir, const Vector3& upHint);
		virtual void SetLookDir(const Vector3& origin, const Quaternion& orientation);
};

//
// --------------------------------------- inlines ---------------------------------------
//

inline 
Axis3D::Axis3D()
{
	// uninitialized
}

inline 
Axis3D::~Axis3D()
{
}

inline const Vector3& 
Axis3D::GetXAxis() const
{
	return right;
}

inline const Vector3& 
Axis3D::GetYAxis() const
{
	return up;
}

inline const Vector3& 
Axis3D::GetZAxis() const
{
	return at;
}

inline void 
Axis3D::SetLH(real dirX, real dirY, real dirZ, real upHintX, real upHintY, real upHintZ)
{
	SetLH(
		Vector3(dirX, dirY, dirZ), 
		Vector3(upHintX, upHintY, upHintZ));
}

inline void 
Axis3D::SetRH(real dirX, real dirY, real dirZ, real upHintX, real upHintY, real upHintZ)
{
	SetRH(
		Vector3(dirX, dirY, dirZ), 
		Vector3(upHintX, upHintY, upHintZ));
}

inline 
CoordFrame::~CoordFrame()
{
}

inline const Vector3& 
CoordFrame::GetOrigin() const
{
	return origin;
}

inline const Axis3D& 
CoordFrame::GetAxis() const
{
	return axis;
}

inline const Vector3& 
CoordFrame::GetXAxis() const
{
	return axis.GetXAxis();
}

inline const Vector3& 
CoordFrame::GetYAxis() const
{
	return axis.GetYAxis();
}

inline const Vector3& 
CoordFrame::GetZAxis() const
{
	return axis.GetZAxis();
}

inline void 
CoordFrame::SetLookAt(real originX, real originY, real originZ, real lookAtX, real lookAtY, real lookAtZ, real upHintX, real upHintY, real upHintZ)
{
	SetLookAt(
		Vector3(originX, originY, originZ),
		Vector3(lookAtX, lookAtY, lookAtZ),
		Vector3(upHintX, upHintY, upHintZ));
}

inline void 
CoordFrame::SetLookDir9(real originX, real originY, real originZ, real lookDirX, real lookDirY, real lookDirZ, real upHintX, real upHintY, real upHintZ)
{
	SetLookDir(
		Vector3(originX, originY, originZ),
		Vector3(lookDirX, lookDirY, lookDirZ),
		Vector3(upHintX, upHintY, upHintZ));
}

inline void 
CoordFrame::SetLookDir(real originX, real originY, real originZ, const Quaternion& orientation)
{
	SetLookDir(
		Vector3(originX, originY, originZ),
		orientation);
}

}