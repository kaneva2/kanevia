/******************************************************************************
 Matrix44.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

namespace wkg 
{

class Vector3;
class Vector4;

class Matrix44
{
	public:
		Matrix44();	// uninitialized
		Matrix44(
			real _11, real _12, real _13, real _14,
			real _21, real _22, real _23, real _24,
			real _31, real _32, real _33, real _34,
			real _41, real _42, real _43, real _44);

		Matrix44(D3DXMATRIX &d3dm)
		{
			_11 = d3dm._11; _12 = d3dm._12; _13 = d3dm._13; _14 = d3dm._14;
			_21 = d3dm._21; _22 = d3dm._22; _23 = d3dm._23; _24 = d3dm._24;
			_31 = d3dm._31; _32 = d3dm._32; _33 = d3dm._33; _34 = d3dm._34;
			_41 = d3dm._41; _42 = d3dm._42; _43 = d3dm._43; _44 = d3dm._44;
		}

		const Matrix44& Identity();													// affects the entire matrix

		const Matrix44& NoRotation();												// sets the upper left 3x3 to the identity
	    const Matrix44& NoTranslation();											// sets the fourth row to zeros (_44 remains untouched)

		const Matrix44& SetTranslation(real x, real y, real z);						// affects only the fourth row (only _41, _42, _43)
		const Matrix44& SetTranslation(const Vector3&);								// affects only the fourth row (only _41, _42, _43)
		const Matrix44& SetTranslation(const Vector4&);								// affects only the fourth row (only _41, _42, _43)
		const Matrix44& SetRotationYawPitchRoll(real yaw, real pitch, real roll);	// affects only the upperleft 3x3 rows and cols
		const Matrix44& SetScale(real x, real y, real z);							// affects only the diagonal (only _11, _22, _33)
		const Matrix44& SetScale(const Vector3&);									// affects only the diagonal (only _11, _22, _33)
		const Matrix44& SetScale(const Vector4&);									// affects only the diagonal (only _11, _22, _33)

		const Matrix44& Translate(real x, real y, real z);							// affects the entire matrix
		const Matrix44& Translate(const Vector3&);									// affects the entire matrix
		const Matrix44& Translate(const Vector4&);									// affects the entire matrix

		const Matrix44& RotateX(real rad);											// affects the entire matrix
		const Matrix44& RotateY(real rad);											// affects the entire matrix
		const Matrix44& RotateZ(real rad);											// affects the entire matrix
		const Matrix44& RotateYawPitchRoll(real yaw, real pitch, real roll);		// in radians...affects the entire matrix
		const Matrix44& RotateAxisAngle(real x, real y, real z, real rad);			// affects the entire matrix
		const Matrix44& RotateAxisAngle(const Vector3&, real rad);					// affects the entire matrix
		const Matrix44& RotateAxisAngle(const Vector4&, real rad);					// affects the entire matrix

		const Matrix44& Scale(real x, real y, real z);								// affects the entire matrix
		const Matrix44& Scale(const Vector3&);										// affects the entire matrix
		const Matrix44& Scale(const Vector4&);										// affects the entire matrix

		const Matrix44& Star(real x, real y, real z);								// affects the entire matrix
		const Matrix44& Star(const Vector3&);										// affects the entire matrix
		const Matrix44& Star(const Vector4&);										// affects the entire matrix

		const Matrix44& Transpose();												// affects the entire matrix

		const Matrix44& Inverse();													// affects the entire matrix

		const Matrix44 operator*(const Matrix44&) const;
		const Matrix44& operator*=(const Matrix44&);

		const Matrix44 &operator = (const D3DXMATRIX &m)
		{
			_11 = m._11; _12 = m._12; _13 = m._13; _14 = m._14;
			_21 = m._21; _22 = m._22; _23 = m._23; _24 = m._24;
			_31 = m._31; _32 = m._32; _33 = m._33; _34 = m._34;
			_41 = m._41; _42 = m._42; _43 = m._43; _44 = m._44;

			return *this;
		}

		const Matrix44 MultTransposeOf(const Matrix44&) const;
		const Matrix44& MultEqTransposeOf(const Matrix44&);

		real Trace() const;

#ifdef _DEBUG
		void Dump()
		{
			DEBUGMSG("%.2f %.2f %.2f %.2f", _11, _12, _13, _14);
			DEBUGMSG("%.2f %.2f %.2f %.2f", _21, _22, _23, _24);
			DEBUGMSG("%.2f %.2f %.2f %.2f", _31, _32, _33, _34);
			DEBUGMSG("%.2f %.2f %.2f %.2f", _41, _42, _43, _44);
		}
#endif

		operator real*();
		operator const real*() const;

		Matrix44 CopyTranspose() const;

	public:
		static const Matrix44 IDENTITY;

	public:
		union
		{
			real m[4][4];

			struct
			{
				real _11, _12, _13, _14;
				real _21, _22, _23, _24;
				real _31, _32, _33, _34;
				real _41, _42, _43, _44;
			};
		};
};

#define SPECIALIZE(Name, Args, Method)	\
	class Name : public Matrix44		\
	{									\
		public:							\
			Name##Args { Method; }		\
	}

SPECIALIZE(Translate44,				(real x, real y, real z),	Translate(x,y,z));
SPECIALIZE(RotateX44,				(real rad),					RotateX(rad));
SPECIALIZE(RotateY44,				(real rad),					RotateY(rad));
SPECIALIZE(RotateZ44,				(real rad),					RotateZ(rad));
SPECIALIZE(RotateYawPitchRoll44,	(real y, real p, real r),	RotateYawPitchRoll(y, p, r));
SPECIALIZE(Scale44,					(real x, real y, real z),	Scale(x,y,z));

#undef SPECIALIZE

inline
Matrix44::Matrix44() 
{
	/* uninitialized */ 
}

inline
Matrix44::Matrix44(
	real m11, real m12, real m13, real m14,
	real m21, real m22, real m23, real m24,
	real m31, real m32, real m33, real m34,
	real m41, real m42, real m43, real m44)
	:	_11(m11), _12(m12), _13(m13), _14(m14),
		_21(m21), _22(m22), _23(m23), _24(m24),
		_31(m31), _32(m32), _33(m33), _34(m34),
		_41(m41), _42(m42), _43(m43), _44(m44)
{
}

inline const Matrix44& 
Matrix44::Identity()
{
	_11 = _22 = _33 = _44 = 1.0f;
	_12 = _13 = _14 =
	_21 = _23 = _24 =
	_31 = _32 = _34 =
	_41 = _42 = _43 = 0.0f;

	return *this;
}

inline const Matrix44& 
Matrix44::NoRotation()
{
	_11 = _22 = _33 = 1.0f;
	_12 = _13 =
	_21 = _23 =
	_31 = _32 = 0.0f;

	return *this;
}

inline const Matrix44& 
Matrix44::NoTranslation()
{
	_41 = _42 = _43 = 0.0f;

	return *this;
}

inline const Matrix44& 
Matrix44::SetTranslation(real x, real y, real z)
{
	_41 = x;
	_42 = y;
	_43 = z;

	return *this;
}

inline const Matrix44& 
Matrix44::SetScale(real x, real y, real z)
{
	_11 = x;
	_22 = y;
	_33 = z;

	return *this;
}

inline real 
Matrix44::Trace() const
{
	return _11 * _22 * _33 * _44;
}

inline 
Matrix44::operator real*()
{
	return &_11;
}

inline 
Matrix44::operator const real*() const
{
	return &_11;
}

}
