/******************************************************************************
 IntersectTriCapsule.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "wkgtypes.h"
#include "Intersect.h"
#include "Triangle.h"
#include "Capsule.h"
#include "Distance.h"

namespace wkg {

bool 
Intersect::TriCapsule(const Triangle& tri, const Capsule& capsule, real* t)
{
	return Distance::SqrTriSeg(tri, capsule.seg, 0, 0, t) <= SQR(capsule.radius);
}

}
