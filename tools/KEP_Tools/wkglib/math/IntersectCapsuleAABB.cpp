/******************************************************************************
 IntersectCapsuleAABB.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "wkgtypes.h"
#include "Intersect.h"
#include "AABB.h"
#include "Capsule.h"
#include "Distance.h"

namespace wkg {

bool 
Intersect::CapsuleAABB(const Capsule& capsule, const AABB& aabb, real* t)
{
	return Distance::SqrSegAABB(capsule.seg, aabb, t) <= SQR(capsule.radius);
}

}
