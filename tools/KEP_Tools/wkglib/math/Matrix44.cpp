/******************************************************************************
 Matrix44.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "wkgtypes.h"
#include "Matrix44.h"

namespace wkg {

const Matrix44 Matrix44::IDENTITY(
	1.0f, 0.0f, 0.0f, 0.0f,
	0.0f, 1.0f, 0.0f, 0.0f,
	0.0f, 0.0f, 1.0f, 0.0f,
	0.0f, 0.0f, 0.0f, 1.0f);

const Matrix44& 
Matrix44::SetRotationYawPitchRoll(real yaw, real pitch, real roll)
{
	real cy = cos(yaw);		// around y axis
	real sy = sin(yaw);
	real cp = cos(pitch);	// around x axis
	real sp = sin(pitch);
	real cr = cos(roll);	// around z axis
	real sr = sin(roll);

	// Euler angle order: ZXY (to match d3dx and 3dsmax)
	//
	_11 = cr*cy + sr*sp*sy;
	_12 = sr*cp;
	_13 = -cr*sy + sr*sp*cy;
	_21 = -sr*cy + cr*sp*sy;
	_22 = cr*cp;
	_23 = sr*sy + cr*sp*cy;
	_31 = cp*sy;
	_32 = -sp;
	_33 = cp*cy;

	return *this;
}

const Matrix44& 
Matrix44::Translate(real x, real y, real z)
{
	_11 = _22 = _33 = _44 = 1.0f;	//  1  0  0  0
									//  0  1  0  0
	_12 = _13 = _14 =				//  0  0  1  0
	_21 = _23 = _24 =				//  x  y  z  1
	_31 = _32 = _34 = 0.0f;

	return SetTranslation(x, y, z);
}

const Matrix44& 
Matrix44::RotateX(real rad)
{
	real c = cos(rad);
	real s = sin(rad);

	_11 = _44 = 1.0f;				//  1  0  0  0
									//  0  c  s  0
	_12 = _13 = _14 =				//  0 -s  c  0
	_21 = _24 =						//  0  0  0  1
	_31 = _34 =
	_41 = _42 = _43 = 0.0f;

	_22 = _33 = c;
	_23 = s;
	_32 = -s;

	return *this;
}

const Matrix44& 
Matrix44::RotateY(real rad)
{
	real c = cos(rad);
	real s = sin(rad);

	_22 = _44 = 1.0f;				//  c  0 -s  0
									//  0  1  0  0
	_12 = _14 =						//  s  0  c  0
	_21 = _23 = _24 =				//  0  0  0  1
	_32 = _34 =
	_41 = _42 = _43 = 0.0f;

	_11 = _33 = c;
	_13 = -s;
	_31 = s;

	return *this;
}

const Matrix44& 
Matrix44::RotateZ(real rad)
{
	real c = cos(rad);
	real s = sin(rad);

	_33 = _44 = 1.0f;				//  c  s  0  0
									// -s  c  0  0
	_13 = _14 =						//  0  0  1  0
	_23 = _24 =						//  0  0  0  1
	_31 = _32 = _34 = 
	_41 = _42 = _43 = 0.0f;

	_11 = _22 = c;
	_12 = s;
	_21 = -s;

	return *this;
}

const Matrix44& 
Matrix44::RotateYawPitchRoll(real yaw, real pitch, real roll)
{
	_44 = 1.0f;
	_14 = _24 = _34 =
	_41 = _42 = _43 = 0.0f;

	return SetRotationYawPitchRoll(yaw, pitch, roll);
}

const Matrix44& 
Matrix44::RotateAxisAngle(real x, real y, real z, real rad)
{
#ifdef DIRECT3D_VERSION	//todo
	D3DXMatrixRotationAxis((D3DXMATRIX*)this, &D3DXVECTOR3(x, y, z), rad); 
#else
//#	error implement this
	ASSERT(0);
#endif

	return *this;
}

const Matrix44& 
Matrix44::Scale(real x, real y, real z)
{
	_44 = 1.0f;						//  x  0  0  0
									//  0  y  0  0
	_12 = _13 = _14 =				//  0  0  z  0
	_21 = _23 = _24 =				//  0  0  0  1
	_31 = _32 = _34 =
	_41 = _42 = _43 = 0.0f;

	return SetScale(x, y, z);
}

const Matrix44& 
Matrix44::Star(real x, real y, real z)
{
	_11 = _14 =						//  0  z -y  0
	_22 = _24 =						// -z  0  x  0
	_33 = _34 =						//  y -x  0  0
	_41 = _42 = _43 = _44 = 0.0f;	//  0  0  0  0

	_12 = z;
	_13 = -y;
	_21 = -z;
	_23 = x;
	_31 = y;
	_32 = -x;

	return *this;
}

const Matrix44& 
Matrix44::Transpose()
{
#	define SWAP(a,b)	{ tmp = m[a][b]; m[a][b] = m[b][a]; m[b][a] = tmp; }

	real tmp;

	SWAP(0, 1);
	SWAP(0, 2);
	SWAP(0, 3);
	SWAP(1, 2);
	SWAP(1, 3);
	SWAP(2, 3);

#	undef SWAP

	return *this;
}

const Matrix44 
Matrix44::operator*(const Matrix44& other) const
{
	Matrix44 result;

//#	define M1(r,c)	m[r][c]
//#	define M2(r,c)	other.m[r][c]
//#	define R(r,c)	result.m[r][c]
//
//	for (int32 i = 0; i < 4; i++)
//		for (int32 j = 0; j < 4; j++)
//			R(i,j) = M1(i,0)*M2(0,j) + M1(i,1)*M2(1,j) + M1(i,2)*M2(2,j) + M1(i,3)*M2(3,j);

#	define M1(r,c)	_##r##c
#	define M2(r,c)	other._##r##c
#	define R(r,c)	result._##r##c

	R(1,1) = M1(1,1)*M2(1,1) + M1(1,2)*M2(2,1) + M1(1,3)*M2(3,1) + M1(1,4)*M2(4,1);
	R(1,2) = M1(1,1)*M2(1,2) + M1(1,2)*M2(2,2) + M1(1,3)*M2(3,2) + M1(1,4)*M2(4,2);
	R(1,3) = M1(1,1)*M2(1,3) + M1(1,2)*M2(2,3) + M1(1,3)*M2(3,3) + M1(1,4)*M2(4,3);
	R(1,4) = M1(1,1)*M2(1,4) + M1(1,2)*M2(2,4) + M1(1,3)*M2(3,4) + M1(1,4)*M2(4,4);

	R(2,1) = M1(2,1)*M2(1,1) + M1(2,2)*M2(2,1) + M1(2,3)*M2(3,1) + M1(2,4)*M2(4,1);
	R(2,2) = M1(2,1)*M2(1,2) + M1(2,2)*M2(2,2) + M1(2,3)*M2(3,2) + M1(2,4)*M2(4,2);
	R(2,3) = M1(2,1)*M2(1,3) + M1(2,2)*M2(2,3) + M1(2,3)*M2(3,3) + M1(2,4)*M2(4,3);
	R(2,4) = M1(2,1)*M2(1,4) + M1(2,2)*M2(2,4) + M1(2,3)*M2(3,4) + M1(2,4)*M2(4,4);

	R(3,1) = M1(3,1)*M2(1,1) + M1(3,2)*M2(2,1) + M1(3,3)*M2(3,1) + M1(3,4)*M2(4,1);
	R(3,2) = M1(3,1)*M2(1,2) + M1(3,2)*M2(2,2) + M1(3,3)*M2(3,2) + M1(3,4)*M2(4,2);
	R(3,3) = M1(3,1)*M2(1,3) + M1(3,2)*M2(2,3) + M1(3,3)*M2(3,3) + M1(3,4)*M2(4,3);
	R(3,4) = M1(3,1)*M2(1,4) + M1(3,2)*M2(2,4) + M1(3,3)*M2(3,4) + M1(3,4)*M2(4,4);

	R(4,1) = M1(4,1)*M2(1,1) + M1(4,2)*M2(2,1) + M1(4,3)*M2(3,1) + M1(4,4)*M2(4,1);
	R(4,2) = M1(4,1)*M2(1,2) + M1(4,2)*M2(2,2) + M1(4,3)*M2(3,2) + M1(4,4)*M2(4,2);
	R(4,3) = M1(4,1)*M2(1,3) + M1(4,2)*M2(2,3) + M1(4,3)*M2(3,3) + M1(4,4)*M2(4,3);
	R(4,4) = M1(4,1)*M2(1,4) + M1(4,2)*M2(2,4) + M1(4,3)*M2(3,4) + M1(4,4)*M2(4,4);

#	undef M1
#	undef M2
#	undef R

	return result;
}

const Matrix44& 
Matrix44::operator*=(const Matrix44& other)
{
	*this = *this * other;

	return *this;
}

const Matrix44 
Matrix44::MultTransposeOf(const Matrix44& other) const
{
	Matrix44 result;

//#	define M1(r,c)	m[r][c]
//#	define M2(r,c)	other.m[r][c]
//#	define R(r,c)	result.m[r][c]
//
//	for (int32 i = 0; i < 4; i++)
//		for (int32 j = 0; j < 4; j++)
//			R(i,j) = M1(i,0)*M2(j,0) + M1(i,1)*M2(j,1) + M1(i,2)*M2(j,2) + M1(i,3)*M2(j,3);

#	define M1(r,c)	_##r##c
#	define M2(r,c)	other._##r##c
#	define R(r,c)	result._##r##c

	R(1,1) = M1(1,1)*M2(1,1) + M1(1,2)*M2(1,2) + M1(1,3)*M2(1,3) + M1(1,4)*M2(1,4);
	R(1,2) = M1(1,1)*M2(2,1) + M1(1,2)*M2(2,2) + M1(1,3)*M2(2,3) + M1(1,4)*M2(2,4);
	R(1,3) = M1(1,1)*M2(3,1) + M1(1,2)*M2(3,2) + M1(1,3)*M2(3,3) + M1(1,4)*M2(3,4);
	R(1,4) = M1(1,1)*M2(4,1) + M1(1,2)*M2(4,2) + M1(1,3)*M2(4,3) + M1(1,4)*M2(4,4);

	R(2,1) = M1(2,1)*M2(1,1) + M1(2,2)*M2(1,2) + M1(2,3)*M2(1,3) + M1(2,4)*M2(1,4);
	R(2,2) = M1(2,1)*M2(2,1) + M1(2,2)*M2(2,2) + M1(2,3)*M2(2,3) + M1(2,4)*M2(2,4);
	R(2,3) = M1(2,1)*M2(3,1) + M1(2,2)*M2(3,2) + M1(2,3)*M2(3,3) + M1(2,4)*M2(3,4);
	R(2,4) = M1(2,1)*M2(4,1) + M1(2,2)*M2(4,2) + M1(2,3)*M2(4,3) + M1(2,4)*M2(4,4);

	R(3,1) = M1(3,1)*M2(1,1) + M1(3,2)*M2(1,2) + M1(3,3)*M2(1,3) + M1(3,4)*M2(1,4);
	R(3,2) = M1(3,1)*M2(2,1) + M1(3,2)*M2(2,2) + M1(3,3)*M2(2,3) + M1(3,4)*M2(2,4);
	R(3,3) = M1(3,1)*M2(3,1) + M1(3,2)*M2(3,2) + M1(3,3)*M2(3,3) + M1(3,4)*M2(3,4);
	R(3,4) = M1(3,1)*M2(4,1) + M1(3,2)*M2(4,2) + M1(3,3)*M2(4,3) + M1(3,4)*M2(4,4);

	R(4,1) = M1(4,1)*M2(1,1) + M1(4,2)*M2(1,2) + M1(4,3)*M2(1,3) + M1(4,4)*M2(1,4);
	R(4,2) = M1(4,1)*M2(2,1) + M1(4,2)*M2(2,2) + M1(4,3)*M2(2,3) + M1(4,4)*M2(2,4);
	R(4,3) = M1(4,1)*M2(3,1) + M1(4,2)*M2(3,2) + M1(4,3)*M2(3,3) + M1(4,4)*M2(3,4);
	R(4,4) = M1(4,1)*M2(4,1) + M1(4,2)*M2(4,2) + M1(4,3)*M2(4,3) + M1(4,4)*M2(4,4);

#	undef M1
#	undef M2
#	undef R

	return result;
}

const Matrix44& 
Matrix44::MultEqTransposeOf(const Matrix44& other)
{
	*this = MultTransposeOf(other);

	return *this;
}

const Matrix44& 
Matrix44::Inverse()
{
#ifdef DIRECT3D_VERSION	//todo
	real det;
	D3DXMatrixInverse((D3DXMATRIX*)this, &det, (const D3DXMATRIX*)this);
#else
	Matrix44 A = *this;
	Matrix44& B = *this;
    real det, oodet;

	ASSERT(0);	//todo: double check this...may not be correct

	// a1 a2	a1 a2 a3
	// b1 b2	b1 b2 b3
	//			c1 c2 c3
	//
#	define det2x2(a1,a2, b1,b2)	\
		((a1) * (b2) - (b1) * (a2))
#	define det3x3(a1,a2,a3, b1,b2,b3, c1,c2,c3)	\
		((a1) * det2x2(b2,b3, c2, c3) - (a2) * det2x2(b1,b3, c1,c3) + (a3) * det2x2(b1,b2, c1,c2))

    B._11 =  det3x3(A._22,A._23,A._24, A._32,A._33,A._34, A._42,A._43,A._44);
    B._12 = -det3x3(A._12,A._13,A._14, A._32,A._33,A._34, A._42,A._43,A._44);
    B._13 =  det3x3(A._12,A._13,A._14, A._22,A._23,A._24, A._42,A._43,A._44);
    B._14 = -det3x3(A._12,A._13,A._14, A._22,A._23,A._24, A._32,A._33,A._34);

    B._21 = -det3x3(A._21,A._23,A._24, A._31,A._33,A._34, A._41,A._43,A._44);
    B._22 =  det3x3(A._11,A._13,A._14, A._31,A._33,A._34, A._41,A._43,A._44);
    B._23 = -det3x3(A._11,A._13,A._14, A._21,A._23,A._24, A._41,A._43,A._44);
    B._24 =  det3x3(A._11,A._13,A._14, A._21,A._23,A._24, A._31,A._33,A._34);

    B._31 =  det3x3(A._21,A._22,A._24, A._31,A._32,A._34, A._41,A._42,A._44);
    B._32 = -det3x3(A._11,A._12,A._14, A._31,A._32,A._34, A._41,A._42,A._44);
    B._33 =  det3x3(A._11,A._12,A._14, A._21,A._22,A._24, A._41,A._42,A._44);
    B._34 = -det3x3(A._11,A._12,A._14, A._21,A._22,A._24, A._31,A._32,A._34);

    B._41 = -det3x3(A._21,A._22,A._23, A._31,A._32,A._33, A._41,A._42,A._43);
    B._42 =  det3x3(A._11,A._12,A._13, A._31,A._32,A._33, A._41,A._42,A._43);
    B._43 = -det3x3(A._11,A._12,A._13, A._21,A._22,A._23, A._41,A._42,A._43);
    B._44 =  det3x3(A._11,A._12,A._13, A._21,A._22,A._23, A._31,A._32,A._33);

#	undef det2x2
#	undef det3x3

	det = (A._11 * B._11) + (A._21 * B._12) + (A._31 * B._13) + (A._41 * B._14);

	oodet = 1.0f / det;

	B._11 *= oodet;
	B._12 *= oodet;
	B._13 *= oodet;
	B._14 *= oodet;
	B._21 *= oodet;
	B._22 *= oodet;
	B._23 *= oodet;
	B._24 *= oodet;
	B._31 *= oodet;
	B._32 *= oodet;
	B._33 *= oodet;
	B._34 *= oodet;
	B._41 *= oodet;
	B._42 *= oodet;
	B._43 *= oodet;
	B._44 *= oodet;
#endif

	return *this;
}


Matrix44 
Matrix44::CopyTranspose() const
{
	Matrix44 copy = *this;

	copy.Transpose();
	return copy;
}

}

#include "Vector3.h"
#include "Vector4.h"

#define OVERLOAD(func, args, expandedArgs)	const Matrix44& Matrix44::##func##args { return func##expandedArgs; }

namespace wkg {

OVERLOAD(SetTranslation,	(const Vector3& v),					(v.x, v.y, v.z));
OVERLOAD(SetScale,			(const Vector3& v),					(v.x, v.y, v.z));
OVERLOAD(Translate,			(const Vector3& v),					(v.x, v.y, v.z));
OVERLOAD(Star,				(const Vector3& v),					(v.x, v.y, v.z));
OVERLOAD(RotateAxisAngle,	(const Vector3& axis, real rad),	(axis.x, axis.y, axis.z, rad));

OVERLOAD(SetTranslation,	(const Vector4& v),					(v.x, v.y, v.z));
OVERLOAD(SetScale,			(const Vector4& v),					(v.x, v.y, v.z));
OVERLOAD(Translate,			(const Vector4& v),					(v.x, v.y, v.z));
OVERLOAD(Star,				(const Vector4& v),					(v.x, v.y, v.z));
OVERLOAD(RotateAxisAngle,	(const Vector4& axis, real rad),	(axis.x, axis.y, axis.z, rad));

}
