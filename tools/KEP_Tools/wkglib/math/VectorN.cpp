/******************************************************************************
 VectorN.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "wkgtypes.h"           
#include "VectorN.h"


namespace wkg {

VectorN::VectorN()
:	m_p(0),
	m_dim(0)
{
}

VectorN::VectorN(uint32 n)
{
	if (n > 0)
	{
		m_p = new real[n];
		if (m_p)
			m_dim = n;
		else
		{
			m_dim = 0;
			ASSERT(m_p);
		}
	}
	else
	{
		m_p = 0;
		m_dim = 0;
	}
}


VectorN::VectorN(const VectorN & m)
{
	if (m.m_p && m.m_dim > 0)
	{
		m_p = new real[m.m_dim];
		if (m_p)
		{
			memcpy(m_p, m.m_p, sizeof(real) * m.m_dim);
			m_dim = m.m_dim;
		}
		else
		{
			m_dim = 0;
			ASSERT(0);
		}
	}
	else
	{
		m_p = 0;
		m_dim = 0;
	}
}

VectorN::VectorN(const real* d, uint32 n)
{
	if (d && n > 0)
	{
		m_p = new real[n];
		if (m_p)
		{
			memcpy(m_p, d, sizeof(real) * n);
			m_dim = n;
		}
		else
		{
			m_dim = 0;
			ASSERT(0);
		}
	}
	else
	{
		m_p = 0;
		m_dim = 0;
	}
}


VectorN::~VectorN()
{
	delete [] m_p;
	m_p = 0;
	m_dim = 0;
}


void VectorN::NewSize(uint32 n)
{
    if (m_dim != n)                     // only delete and new if
    {                                   // the size of memory is really
        if (m_p)
		{
			delete [] m_p;				// changing, otherwise just
		}

		if (n > 0)
		{
			m_p = new real[n];
			if (m_p)
				m_dim = n;
			else
			{
				m_dim = 0;
				ASSERT(0);
			}
		}
		else
		{
			m_p = 0;
			m_dim = 0;
		}
    }
}


VectorN& VectorN::operator=(const VectorN & m) 
{
    NewSize(m.m_dim);

    if (m_p && m_dim)
    {
		memcpy(m_p, m.m_p, sizeof(real)*m_dim);
	}

    return *this;   
}


void VectorN::operator+=(const VectorN& vec)
{
	ASSERT(vec.m_dim == m_dim);
	ASSERT(m_p && vec.m_p);

	for (uint32 i=0; i<m_dim; i++)
	{
		m_p[i] += vec.m_p[i];
	}
}


void VectorN::operator-=(const VectorN& vec)
{
	ASSERT(vec.m_dim == m_dim);
	ASSERT(m_p && vec.m_p);

	for (uint32 i=0; i<m_dim; i++)
	{
		m_p[i] -= vec.m_p[i];
	}
}


void VectorN::operator*=(real scalar)
{
	ASSERT(m_p);

	for (uint32 i=0; i<m_dim; i++)
	{
		m_p[i] *= scalar;
	}
}


real VectorN::Dot(const VectorN& vec) const
{
	real sum = 0.0f;

	ASSERT(vec.m_dim == m_dim);
	ASSERT(m_p && vec.m_p);

	for (uint32 i=0; i<m_dim; i++)
	{
		sum += m_p[i] * vec.m_p[i];
	}

	return sum;
}

real VectorN::LengthSqr() const
{
	real sum = 0.0f;

	ASSERT(m_p);

	for (uint32 i=0; i<m_dim; i++)
	{
		sum += m_p[i] * m_p[i];
	}

	return sum;
}

real VectorN::Length() const
{
	return (real)sqrt(LengthSqr());
}

void VectorN::AddScaledVector(real scalar, const VectorN& vec)
{
	ASSERT(vec.m_dim == m_dim);
	ASSERT(m_p && vec.m_p);

	for (uint32 i=0; i<m_dim; i++)
	{
		m_p[i] += scalar * vec.m_p[i];
	}
}


}