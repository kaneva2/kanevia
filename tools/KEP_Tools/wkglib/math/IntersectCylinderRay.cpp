/******************************************************************************
 IntersectCylinderRay.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "wkgtypes.h"
#include "Intersect.h"
#include "Cylinder.h"
#include "Ray.h"
#include "Vector3.h"

namespace wkg {

// This code is based upon the Ray-Triangle intersection test from Tomas Moller and Ben Trumbore.
//
// http://www.ce.chalmers.se/staff/tomasm/raytri/raytri.c
// or 
// http://www.acm.org/jgt/papers/MollerTrumbore97/
//

bool 
Intersect::CylinderRay(const Cylinder& cyl, const Ray& ray, real* t)
{
//bool TestCylinderRayIntersection(const Vector3& orig, const Vector3& dir,
//								 const Vector3& base, const Vector3& axis,
//								 real radius, real *pDist)

	Vector3		RC;		// Ray base to cylinder base
	real		d;		// Shortest distance between the ray and the cylinder
	Vector3		n, D;
	real		ln;

	RC.x = ray.from.x - cyl.GetBase().x;
	RC.y = ray.from.y - cyl.GetBase().y;
	RC.z = ray.from.z - cyl.GetBase().z;
	n = ray.dir.Cross(cyl.GetAxis());

	ln = n.Length();
	if (wkg::EQUAL(ln, 0.0f))
	{	/* ray parallel to cyl	*/
	    d	 = RC.Dot(cyl.GetAxis());
	    D.x	 = RC.x - d*cyl.GetAxis().x;
	    D.y	 = RC.y - d*cyl.GetAxis().y;
	    D.z	 = RC.z - d*cyl.GetAxis().z;
	    d	 = D.Length();
	}
	else
	{
		n.Normalize();
		d = ABS(RC.Dot(n));		/* shortest distance	*/
	}

	if (d <= cyl.GetRadius() && t)
	{
	    Vector3 O(RC.Cross(cyl.GetAxis()));
	    real q = - O.Dot(n) / ln;
	    O = n.Cross(cyl.GetAxis());
		O.Normalize();
		real p = ABS(wkg::sqrt(cyl.GetRadiusSqr() - d*d) / (ray.dir.Dot(O)));
	    *t = q - p;
	}

	return d <= cyl.GetRadius();
}

}
