/******************************************************************************
 Triangle.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "Vector3.h"

namespace wkg {

// Triangles are stored as an absolute point and two relative line segments (as per Eberly).
// This keeps slightly more info around than just a collection of three points and helps
// speed geometric algorithms that require vector opts.
//
// This organization defines a "triangle coordinate system" with an origin and two axis (s and t) that
// lie along the edges.
//		0 <= s <= 1		
//		0 <= t <= 1		only these ranges belong to the triangle (all others are outside)
//		s + t <= 1
//
// A specific point within the triangle (on an edge or interior) is addressed as (s,t)
//
class Triangle
{
	public:
		Triangle();		// uninit
		Triangle(const Vector3*);												// clockwise is front facing

		void Set(const Vector3&, const Vector3&, const Vector3&);				// clockwise is front facing
		void Set(const Vector3 verts[], const uint16 idx[], int32 startIndex);	// clockwise is front facing
		void Set(const Vector3 verts[], const uint32 idx[], uint32 startIndex);	// clockwise is front facing

		void SetOrigin(const Vector3&);

		const Vector3& GetOrigin() const;
		const Vector3& GetEdge0() const;
		const Vector3& GetEdge1() const;

		// the following are calculated
		//
		Vector3 GetEdge2() const;
		Vector3 GetNormal() const;	// not necessarily a unit vector
		Vector3 GetCentroid() const;
		void GetVerts(Vector3 verts[]) const;

	protected:
		Vector3 org;
		Vector3 edge0;
		Vector3 edge1;
};

inline  
Triangle::Triangle()
{
	// uninit
}

inline void 
Triangle::SetOrigin(const Vector3& newOrg)
{
	org = newOrg;
}

inline void 
Triangle::Set(const Vector3& a, const Vector3& b, const Vector3& c)
{
	SetOrigin(a);
	edge0 = b - a;
	edge1 = c - a;
}

inline
Triangle::Triangle(const Vector3* v)
{
	Set(v[0], v[1], v[2]);
}

inline void 
Triangle::Set(const Vector3 verts[], const uint16 idx[], int32 startIndex)
{
	Set(verts[idx[startIndex]], verts[idx[startIndex+1]], verts[idx[startIndex+2]]);
}

inline void 
Triangle::Set(const Vector3 verts[], const uint32 idx[], uint32 startIndex)
{
	Set(verts[idx[startIndex]], verts[idx[startIndex+1]], verts[idx[startIndex+2]]);
}

inline const Vector3& 
Triangle::GetOrigin() const
{
	return org;
}

inline const Vector3& 
Triangle::GetEdge0() const
{
	return edge0;
}

inline const Vector3& 
Triangle::GetEdge1() const
{
	return edge1;
}

inline Vector3
Triangle::GetNormal() const
{
	return edge0.Cross(edge1);
}

inline Vector3 
Triangle::GetCentroid() const
{
	return org + ((edge0 + edge1) / 3.0f);		// simplified from (org + (org + edge0) + (org + edge1)) / 3.0f
}

inline Vector3
Triangle::GetEdge2() const
{
	return edge1 - edge0;
}

inline void 
Triangle::GetVerts(Vector3 verts[]) const
{
	verts[0] = org;
	verts[1] = org + edge0;
	verts[2] = org + edge1;
}

}
