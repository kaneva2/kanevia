/******************************************************************************
 Frustum.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "ViewVolume.h"

namespace wkg {

class CoordFrame;

class Frustum : public ViewVolume
{
	public:
		Frustum();		// uninitialized

		void Set(const CoordFrame&,
			real hither, real yon,
			real fovVertical,
			real aspect);		// fov (*vertical* fov...radians); aspect=wid/hei
};

inline
Frustum::Frustum() 
{ 
	/* uninitialized */ 
}

}
