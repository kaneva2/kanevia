/******************************************************************************
 ViewVolume.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "Plane.h"
#include "Vector3.h"

namespace wkg {

class AABB;

class ViewVolume
{
	public:
		ViewVolume();	// uninitialized

		const Vector3& GetCOP() const;

		bool Includes(const AABB& box, uint8* activePlanes, real tolerance = TOLERANCEf) const;

	public:
		enum Constants { ALLPLANES = 0x3f };
		enum Side { HITHER, YON, LEFT, RIGHT, TOP, BOTTOM, NUMPLANES };
		Plane plane[6];
		Vector3 cop;	// center of projection
};

inline
ViewVolume::ViewVolume() 
{ 
	/* uninitialized */ 
}

inline const Vector3& 
ViewVolume::GetCOP() const
{
	return cop;
}

}
