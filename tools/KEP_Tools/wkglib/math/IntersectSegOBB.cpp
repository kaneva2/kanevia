/******************************************************************************
 IntersectSegOBB.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "Intersect.h"
#include "OBB.h"
#include "LineSeg.h"
#include "Vector3.h"

namespace wkg 
{

bool 
Intersect::SegOBB(const LineSegment& rkSegment, const OBB& rkBox)
{
    real fAWdU[3], fADdU[3], fAWxDdU[3], fRhs;
    Vector3 kSDir = rkSegment.GetDirection() * 0.5f;
    Vector3 kSCen = rkSegment.GetOrigin() + kSDir;

    Vector3 kDiff = kSCen - rkBox.GetCenter();

    fAWdU[0] = ABS(kSDir.Dot(rkBox.GetAxis(0)));
    fADdU[0] = ABS(kDiff.Dot(rkBox.GetAxis(0)));
    fRhs = rkBox.GetExtent(0) + fAWdU[0];
    if ( fADdU[0] > fRhs )
        return false;

    fAWdU[1] = ABS(kSDir.Dot(rkBox.GetAxis(1)));
    fADdU[1] = ABS(kDiff.Dot(rkBox.GetAxis(1)));
    fRhs = rkBox.GetExtent(1) + fAWdU[1];
    if ( fADdU[1] > fRhs )
        return false;

    fAWdU[2] = ABS(kSDir.Dot(rkBox.GetAxis(2)));
    fADdU[2] = ABS(kDiff.Dot(rkBox.GetAxis(2)));
    fRhs = rkBox.GetExtent(2) + fAWdU[2];
    if ( fADdU[2] > fRhs )
        return false;

    Vector3 kWxD = kSDir.Cross(kDiff);

    fAWxDdU[0] = ABS(kWxD.Dot(rkBox.GetAxis(0)));
    fRhs = rkBox.GetExtent(1) * fAWdU[2] + rkBox.GetExtent(2) * fAWdU[1];
    if ( fAWxDdU[0] > fRhs )
        return false;

    fAWxDdU[1] = ABS(kWxD.Dot(rkBox.GetAxis(1)));
    fRhs = rkBox.GetExtent(0) * fAWdU[2] + rkBox.GetExtent(2) * fAWdU[0];
    if ( fAWxDdU[1] > fRhs )
        return false;

    fAWxDdU[2] = ABS(kWxD.Dot(rkBox.GetAxis(2)));
    fRhs = rkBox.GetExtent(0) * fAWdU[1] + rkBox.GetExtent(1) * fAWdU[0];
    if ( fAWxDdU[2] > fRhs )
        return false;

    return true;
}

static bool 
Clip(real fDenom, real fNumer, real &rfT0, real &rfT1)
{
    // Return value is 'true' if line segment intersects the current test
    // plane.  Otherwise 'false' is returned in which case the line segment
    // is entirely clipped.

    if ( fDenom > (real)0.0f )
    {
        if ( fNumer > fDenom*rfT1 )
            return false;
        if ( fNumer > fDenom*rfT0 )
            rfT0 = fNumer/fDenom;
        return true;
    }
    else if ( fDenom < (real)0.0f )
    {
        if ( fNumer > fDenom*rfT0 )
            return false;
        if ( fNumer > fDenom*rfT1 )
            rfT1 = fNumer/fDenom;
        return true;
    }
    else
    {
        return fNumer <= (real)0.0;
    }
}


static bool 
FindIntersectionPrivate(
	const Vector3& rkOrigin,
    const Vector3& rkDirection, 
	const real afExtent[3], 
	real& rfT0,
    real& rfT1)
{
    real fSaveT0 = rfT0, fSaveT1 = rfT1;

    bool bNotEntirelyClipped =
        Clip(+rkDirection.x,-rkOrigin.x-afExtent[0],rfT0,rfT1) &&
        Clip(-rkDirection.x,+rkOrigin.x-afExtent[0],rfT0,rfT1) &&
        Clip(+rkDirection.y,-rkOrigin.y-afExtent[1],rfT0,rfT1) &&
        Clip(-rkDirection.y,+rkOrigin.y-afExtent[1],rfT0,rfT1) &&
        Clip(+rkDirection.z,-rkOrigin.z-afExtent[2],rfT0,rfT1) &&
        Clip(-rkDirection.z,+rkOrigin.z-afExtent[2],rfT0,rfT1);

    return bNotEntirelyClipped && ( rfT0 != fSaveT0 || rfT1 != fSaveT1 );
}

bool 
Intersect::FindIntersection(
	const LineSegment& rkSegment,
    const OBB& rkBox, 
	int32 &riQuantity, 
	Vector3 akPoint[2])
{
    // convert segment to box coordinates
    Vector3 kDiff = rkSegment.GetOrigin() - rkBox.GetCenter();
    Vector3 kOrigin(
        kDiff.Dot(rkBox.GetAxis(0)),
        kDiff.Dot(rkBox.GetAxis(1)),
        kDiff.Dot(rkBox.GetAxis(2))
    );
    Vector3 kDirection(
        rkSegment.GetDirection().Dot(rkBox.GetAxis(0)),
        rkSegment.GetDirection().Dot(rkBox.GetAxis(1)),
        rkSegment.GetDirection().Dot(rkBox.GetAxis(2))
    );

    real fT0 = (real)0.0;
	real fT1 = (real)1.0;
    bool bIntersects = FindIntersectionPrivate(kOrigin, kDirection, rkBox.GetExtents(), fT0, fT1);
    if ( bIntersects )
    {
        if ( fT0 > (real)0.0 )
        {
            if ( fT1 < (real)1.0 )
            {
                riQuantity = 2;

				if(fT0 > fT1)
				{
					real temp = fT1;
					fT1 = fT0;
					fT0 = temp;
				}

                akPoint[0] = rkSegment.GetOrigin() + rkSegment.GetDirection() * fT0;
                akPoint[1] = rkSegment.GetOrigin() + rkSegment.GetDirection() * fT1;
            }
            else
            {
                riQuantity = 1;
                akPoint[0] = rkSegment.GetOrigin() + rkSegment.GetDirection() * fT0;
            }
        }
        else  // fT0 == 0
        {
            if ( fT1 < (real)1.0 )
            {
                riQuantity = 1;
                akPoint[0] = rkSegment.GetOrigin() + rkSegment.GetDirection() * fT1;
            }
            else  // fT1 == 1
            {
                // segment entirely in box
                riQuantity = 0;
            }
        }
    }
    else
    {
        riQuantity = 0;
    }

    return bIntersects;
}

}
