/******************************************************************************
 Frustum.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "wkgtypes.h"
#include "Frustum.h"
#include "CoordFrame.h"

namespace wkg {

void 
Frustum::Set(const CoordFrame& frame,
	real hither, real yon,
	real fov,
	real aspect)		// fov is *vertical fov* in radians; aspect=wid/hei
{
	Vector3 Nfovw, Nfovh;

	ASSERT(fov > 0.0f && fov < WKG_PI);	// tan undef when cos(fov/2)==0; frustum is pretty useless when fov==0...a line :)

	Nfovh = frame.GetZAxis() * tan(fov * 0.5f);
	Nfovw = frame.GetZAxis() * (tan(fov * 0.5f) * aspect);

	cop = frame.GetOrigin();

	plane[HITHER].normal	= -frame.GetZAxis();
	plane[HITHER].constant	= -(plane[HITHER].normal.Dot(cop + frame.GetZAxis() * hither));

	plane[YON].normal		= frame.GetZAxis();
	plane[YON].constant		= -(plane[YON].normal.Dot(cop + frame.GetZAxis() * yon));

	plane[LEFT].normal		= -(frame.GetXAxis() + Nfovw);
	plane[LEFT].normal.Normalize();
	plane[LEFT].constant	= -(plane[LEFT].normal.Dot(cop));

	plane[RIGHT].normal		= frame.GetXAxis() - Nfovw;
	plane[RIGHT].normal.Normalize();
	plane[RIGHT].constant	= -(plane[RIGHT].normal.Dot(cop));

	plane[TOP].normal		= frame.GetYAxis() - Nfovh;
	plane[TOP].normal.Normalize();
	plane[TOP].constant		= -(plane[TOP].normal.Dot(cop));

	plane[BOTTOM].normal	= -(frame.GetYAxis() + Nfovh);
	plane[BOTTOM].normal.Normalize();
	plane[BOTTOM].constant	= -(plane[BOTTOM].normal.Dot(cop));
}

}
