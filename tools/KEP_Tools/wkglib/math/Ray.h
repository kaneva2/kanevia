/******************************************************************************
 Ray.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "Vector3.h"

namespace wkg {

class Ray
{
	public:
		Ray();												// uninitialized
		Ray(const Vector3& org, const Vector3& dir);		// dir must be unit length

		void Set(const Vector3& org, const Vector3& dir);	// dir must be unit length
		void SetOrigin(const Vector3&);
		void SetDirection(const Vector3&);					// must be unit length

		const Vector3& GetOrigin() const;
		const Vector3& GetDirection() const;

	public:
		Vector3 from;		// origin
		Vector3 dir;		// unit length
};

inline
Ray::Ray()
{
	// uninitialized
}

inline void 
Ray::SetOrigin(const Vector3& org)
{
	from = org;
}

inline void 
Ray::SetDirection(const Vector3& dir)
{
	this->dir = dir;
}

inline void 
Ray::Set(const Vector3& org, const Vector3& dir)
{
	SetOrigin(org);
	SetDirection(dir);
}

inline
Ray::Ray(const Vector3& org, const Vector3& dir)
{
	Set(org, dir);
}

inline const Vector3& 
Ray::GetOrigin() const
{
	return from;
}

inline const Vector3& 
Ray::GetDirection() const
{
	return dir;
}

}
