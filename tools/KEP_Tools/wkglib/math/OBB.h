/******************************************************************************
 OBB.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include "vector3.h"

namespace wkg 
{

class Matrix44;

class OBB
{
	public:
		// uninit
		OBB();

		void SetCenter(const Vector3&);
		void SetCenter(real x, real y, real z);

		void SetAxis(int32, const Vector3&);

		void SetExtent(int32, real);

		const Vector3& GetCenter() const;

		const Vector3& GetAxis(int32) const;

		real GetExtent(int32) const;
		const real *GetExtents() const { return extent; }

		// 8 points
		void ComputeVerts(Vector3* verts) const;
		real ComputeVolume() const;

		// only translation/rotation supported
		void Transform(const Matrix44&, OBB* result) const;

//		SERIALIZABLE

	protected:
		Vector3	center;
		Vector3	axis[3];
		real	extent[3];
};

inline 
OBB::OBB()
{
	// uninit
}

inline void 
OBB::SetCenter(const Vector3& val)
{
	center = val;
}

inline void 
OBB::SetCenter(real x, real y, real z)
{
	center.Set(x, y, z);
}

inline void 
OBB::SetAxis(int32 which, const Vector3& val)
{
	ASSERT(which >= 0 && which < 3);
	axis[which] = val;
}

inline void 
OBB::SetExtent(int32 which, real val)
{
	ASSERT(which >= 0 && which < 3);
	extent[which] = val;
}

inline const Vector3& 
OBB::GetCenter() const
{
	return center;
}

inline const Vector3& 
OBB::GetAxis(int32 which) const
{
	ASSERT(which >= 0 && which < 3);
	return axis[which];
}

inline real 
OBB::GetExtent(int32 which) const
{
	ASSERT(which >= 0 && which < 3);
	return extent[which];
}

inline real 
OBB::ComputeVolume() const
{
	// * 8 since each extent is *half* length along it's axis
	return 8.0f * (extent[0] * extent[1] * extent[2]);
}

}
