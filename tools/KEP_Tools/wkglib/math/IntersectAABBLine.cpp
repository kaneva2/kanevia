/******************************************************************************
 IntersectAABBLine.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "wkgtypes.h"
#include "Intersect.h"
#include "AABB.h"
#include "Line.h"

namespace wkg {

bool 
Intersect::AABBLine(const AABB& aabb, const Line& line, real* tminResult, real* tmaxResult)
{
	real tmin, tmax, t1, t2, invf;

	tmin = -MAXREAL;
	tmax = MAXREAL;

	// x axis
	//
	if (ABS(line.dir.x) > tolerance)
	{
		invf = 1.0f / line.dir.x;
		t1 = (aabb.min.x - line.pt.x) * invf;
		t2 = (aabb.max.x - line.pt.x) * invf;

		if (t1 > t2)
		{
			if (t2 > tmin)	tmin = t2;
			if (t1 < tmax)	tmax = t1;
		}
		else
		{
			if (t1 > tmin)	tmin = t1;
			if (t2 < tmax)	tmax = t2;
		}
		if (tmin > tmax)	return false;
	}
	else if (line.pt.x < aabb.min.x || line.pt.x > aabb.max.x) 
		return false;

	// y axis
	//
	if (ABS(line.dir.y) > tolerance)
	{
		invf = 1.0f / line.dir.y;
		t1 = (aabb.min.y - line.pt.y) * invf;
		t2 = (aabb.max.y - line.pt.y) * invf;

		if (t1 > t2)
		{
			if (t2 > tmin)	tmin = t2;
			if (t1 < tmax)	tmax = t1;
		}
		else
		{
			if (t1 > tmin)	tmin = t1;
			if (t2 < tmax)	tmax = t2;
		}
		if (tmin > tmax)	return false;
	}
	else if (line.pt.y < aabb.min.y || line.pt.y > aabb.max.y) 
		return false;

	// z axis
	//
	if (ABS(line.dir.z) > tolerance)
	{
		invf = 1.0f / line.dir.z;
		t1 = (aabb.min.z - line.pt.z) * invf;
		t2 = (aabb.max.z - line.pt.z) * invf;

		if (t1 > t2)
		{
			if (t2 > tmin)	tmin = t2;
			if (t1 < tmax)	tmax = t1;
		}
		else
		{
			if (t1 > tmin)	tmin = t1;
			if (t2 < tmax)	tmax = t2;
		}
		if (tmin > tmax)	return false;
	}
	else if (line.pt.z < aabb.min.z || line.pt.z > aabb.max.z) 
		return false;

	if (tminResult)
		*tminResult = tmin;

	if (tmaxResult)
		*tmaxResult = tmax;

	return true;
}

}
