// Magic Software, Inc.
// http://www.magic-software.com
// Copyright (c) 2000-2003.  All Rights Reserved
//
// Source code from Magic Software is supplied under the terms of a license
// agreement and may not be copied or disclosed except in accordance with the
// terms of that agreement.  The various license agreements may be found at
// the Magic Software web site.  This file is subject to the license
//
// FREE SOURCE CODE
// http://www.magic-software.com/License/free.pdf
//
// The original code base was provided by Nolan Walker
// http://www.cs.unc.edu/~walkern

#pragma once

// A line contact set is a polyline surrounding the contact area
// (or a single point for n=1, or a line segment for n=2)

// This could probably be changed to intialize to some max num
// of points so that the space isn't wasted.

namespace wkg
{

class LineContactSet
{
public:
    // The number of points (1-8)
    int32 m_iNumPoints;

    // Index of these points (only NumPoints are valid)
    Vector3 m_akPoints[8];

    // Projections
    real m_fMin, m_fMax;
};

} 



