/******************************************************************************
 IntersectAABBSeg.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "wkgtypes.h"
#include "Intersect.h"
#include "AABB.h"
#include "LineSeg.h"

namespace wkg {

bool 
Intersect::AABBSeg(const AABB& aabb, const LineSegment& seg, real* tResult)
{
	real tmin, tmax, t1, t2, invf;

	tmin = -MAXREAL;
	tmax = MAXREAL;

	// x axis
	//
	if (ABS(seg.dir.x) > tolerance)
	{
		invf = 1.0f / seg.dir.x;
		t1 = (aabb.min.x - seg.from.x) * invf;
		t2 = (aabb.max.x - seg.from.x) * invf;

		if (t1 > t2)
		{
			if (t2 > tmin)	tmin = t2;
			if (t1 < tmax)	tmax = t1;
		}
		else
		{
			if (t1 > tmin)	tmin = t1;
			if (t2 < tmax)	tmax = t2;
		}
		if (tmin > tmax)	return false;
		if (tmax < 0.0f)	return false;
		if (SQR(tmin) > seg.dir.LengthSqr())	return false;
	}
	else if (seg.from.x < aabb.min.x || seg.from.x > aabb.max.x)
		return false;

	// y axis
	//
	if (ABS(seg.dir.y) > tolerance)
	{
		invf = 1.0f / seg.dir.y;
		t1 = (aabb.min.y - seg.from.y) * invf;
		t2 = (aabb.max.y - seg.from.y) * invf;

		if (t1 > t2)
		{
			if (t2 > tmin)	tmin = t2;
			if (t1 < tmax)	tmax = t1;
		}
		else
		{
			if (t1 > tmin)	tmin = t1;
			if (t2 < tmax)	tmax = t2;
		}
		if (tmin > tmax)	return false;
		if (tmax < 0.0f)	return false;
		if (SQR(tmin) > seg.dir.LengthSqr())	return false;
	}
	else if (seg.from.y < aabb.min.y || seg.from.y > aabb.max.y)
		return false;

	// z axis
	//
	if (ABS(seg.dir.z) > tolerance)
	{
		invf = 1.0f / seg.dir.z;
		t1 = (aabb.min.z - seg.from.z) * invf;
		t2 = (aabb.max.z - seg.from.z) * invf;

		if (t1 > t2)
		{
			if (t2 > tmin)	tmin = t2;
			if (t1 < tmax)	tmax = t1;
		}
		else
		{
			if (t1 > tmin)	tmin = t1;
			if (t2 < tmax)	tmax = t2;
		}
		if (tmin > tmax)	return false;
		if (tmax < 0.0f)	return false;
		if (SQR(tmin) > seg.dir.LengthSqr())	return false;
	}
	else if (seg.from.z < aabb.min.z || seg.from.z > aabb.max.z)
		return false;

	if (tResult)
	{
		if (tmin > 0.0f)
			*tResult = tmin;
		else
			*tResult = tmax;
	}

	return true;
}

}