/******************************************************************************
 SparseMatrixNN.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "wkgtypes.h"
#include "SparseMatrixNN.h"
#include "VectorN.h"

namespace wkg {

SparseMatrixNN::SparseMatrixNN()
:	m_N(0),
	m_size(0),
	m_sa(NULL),
	m_ija(NULL)
{
}


SparseMatrixNN::SparseMatrixNN(uint32 N, uint32 numNonDiagonalElements)
:	m_N(N),
	m_size(N+numNonDiagonalElements),
	m_sa(new real[m_size]),
	m_ija(new uint32[m_size])
{
	ASSERT(m_sa && m_ija);

	m_ija[0] = m_N+2;
}


real SparseMatrixNN::Get(uint32 i, uint32 j) const
{ 
	if (i >= m_N || j >= m_N)
		return 0.0f;

	if (i == j)
		return m_sa[i];

//	if (j < m_ija[i])
//		return 0.0f;

	for (uint32 k=m_ija[i]; k<(m_ija[i+1]-1); k++)
	{
		if (m_ija[k] == j)
			return m_sa[k];
	}
	
	return 0.0f;
}


bool SparseMatrixNN::Multiply(const VectorN& x, VectorN& b) const
{
	uint32 i,k;

	if (!m_ija || x.m_dim != m_N || b.m_dim != m_N)
		return false;
	
	for (i=0; i<m_N; i++)
	{
		b.m_p[i] = m_sa[i] * x.m_p[i];   // Start with diagonal term.

		for (k=m_ija[i]; k<=m_ija[i+1]-1; k++)  // Loop over off-diagonal terms.
		{
			b.m_p[i] += m_sa[k-1] * x.m_p[m_ija[k-1]];
		}
	}

	return true;
}


bool SparseMatrixNN::TransposeMultiply(const VectorN &x, VectorN& b) const
{
	uint32 i,j,k;

	if (!m_ija || x.m_dim != m_N || b.m_dim != m_N)
		return false;

	for (i=0; i < m_N; i++)
	{
		b.m_p[i] = m_sa[i] * x.m_p[i];	// Start with diagonal terms.
	}

	for (i=0; i < m_N; i++)		// Loop over off-diagonal terms.
	{
		for (k=m_ija[i]; k <= m_ija[i+1]-1; k++)
		{
			j = m_ija[k-1];
			b.m_p[j] += m_sa[k-1] * x.m_p[i];
		}
	}

	return true;
}


bool SparseMatrixNN::SetSequentialRow(uint32 row, real diagVal, uint32 numCols, uint32* colIndices, real* values)
{
	if (row >= m_N)
		return false;

	m_sa[row] = diagVal;	// set diagonal coefficient

	for (uint32 k=0; k<numCols; k++)	// set off-diagonal coefficients
	{
		m_sa[k+m_ija[row]-1] = values[k];
		m_ija[k+m_ija[row]-1] = colIndices[k];
	}
	
	m_ija[row+1] = m_ija[row]+numCols;

	return true;
}



}
