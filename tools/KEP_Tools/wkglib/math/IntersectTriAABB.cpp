#include "wkgtypes.h"
#include "Intersect.h"
#include "Triangle.h"
#include "AABB.h"

namespace wkg {

//--------- This code is *entirely* based on the following (they deserve credit :)

// Magic Software, Inc.
// http://www.magic-software.com
// Copyright (c) 2000-2002.  All Rights Reserved
//
// Source code from Magic Software is supplied under the terms of a license
// agreement and may not be copied or disclosed except in accordance with the
// terms of that agreement.  The various license agreements may be found at
// the Magic Software web site.  This file is subject to the license
//
// FREE SOURCE CODE
// http://www.magic-software.com/License/free.pdf

static inline void 
ProjectTriangle(const Vector3& dir, const Vector3 tri[], real* min, real* max)
{
	real dot;

    *min = *max = dir.Dot(tri[0]);

    dot = dir.Dot(tri[1]);
    if (dot < *min)
        *min = dot;
    else if (dot > *max)
        *max = dot;

    dot = dir.Dot(tri[2]);
    if (dot < *min)
        *min = dot;
    else if (dot > *max)
        *max = dot;
}

static inline void 
ProjectBox(const Vector3& dir, const Vector3& center, const Vector3& extent, real* min, real* max)
{
    real dirDotC = dir.Dot(center);
    real r =
        extent.x * ABS(dir.Dot(Vector3::AXIS[0])) +
        extent.y * ABS(dir.Dot(Vector3::AXIS[1])) +
        extent.z * ABS(dir.Dot(Vector3::AXIS[2]));
    *min = dirDotC - r;
    *max = dirDotC + r;
}

bool 
Intersect::TriAABB(const Triangle& tri, const AABB& aabb)
{
    real min0, max0, min1, max1, DirdotC;
    Vector3 dir, triVerts[3];
	Vector3 center, extent;

	center = aabb.GetCenter();
	extent = aabb.GetExtent();

    // test direction of triangle normal
	//
	dir = tri.GetNormal();
	min0 = max0 = dir.Dot(tri.GetOrigin());
	ProjectBox(dir, center, extent, &min1, &max1);
	if (max1 < min0 || max0 < min1)
		return false;

	tri.GetVerts(triVerts);

    // test direction of box faces
	//
    ProjectTriangle(Vector3::AXIS[0], triVerts, &min0, &max0);
	DirdotC = Vector3::AXIS[0].Dot(center);
    min1 = DirdotC - extent.x;
    max1 = DirdotC + extent.x;
	if (max1 < min0 || max0 < min1)
        return false;

    ProjectTriangle(Vector3::AXIS[1], triVerts, &min0, &max0);
	DirdotC = Vector3::AXIS[1].Dot(center);
    min1 = DirdotC - extent.y;
    max1 = DirdotC + extent.y;
	if (max1 < min0 || max0 < min1)
        return false;

    ProjectTriangle(Vector3::AXIS[2], triVerts, &min0, &max0);
	DirdotC = Vector3::AXIS[2].Dot(center);
    min1 = DirdotC - extent.z;
    max1 = DirdotC + extent.z;
	if (max1 < min0 || max0 < min1)
        return false;

    // test direction of triangle/box edge cross products
	//
	dir = tri.GetEdge0().Cross(Vector3::AXIS[0]);
	ProjectTriangle(dir, triVerts, &min0, &max0);
	ProjectBox(dir, center, extent, &min1, &max1);
	if (max1 < min0 || max0 < min1)
		return false;

	dir = tri.GetEdge0().Cross(Vector3::AXIS[1]);
	ProjectTriangle(dir, triVerts, &min0, &max0);
	ProjectBox(dir, center, extent, &min1, &max1);
	if (max1 < min0 || max0 < min1)
		return false;

	dir = tri.GetEdge0().Cross(Vector3::AXIS[2]);
	ProjectTriangle(dir, triVerts, &min0, &max0);
	ProjectBox(dir, center, extent, &min1, &max1);
	if (max1 < min0 || max0 < min1)
		return false;

	dir = tri.GetEdge1().Cross(Vector3::AXIS[0]);
	ProjectTriangle(dir, triVerts, &min0, &max0);
	ProjectBox(dir, center, extent, &min1, &max1);
	if (max1 < min0 || max0 < min1)
		return false;

	dir = tri.GetEdge1().Cross(Vector3::AXIS[1]);
	ProjectTriangle(dir, triVerts, &min0, &max0);
	ProjectBox(dir, center, extent, &min1, &max1);
	if (max1 < min0 || max0 < min1)
		return false;

	dir = tri.GetEdge1().Cross(Vector3::AXIS[2]);
	ProjectTriangle(dir, triVerts, &min0, &max0);
	ProjectBox(dir, center, extent, &min1, &max1);
	if (max1 < min0 || max0 < min1)
		return false;

	Vector3 edge2 = tri.GetEdge2();

	dir = edge2.Cross(Vector3::AXIS[0]);
	ProjectTriangle(dir, triVerts, &min0, &max0);
	ProjectBox(dir, center, extent, &min1, &max1);
	if (max1 < min0 || max0 < min1)
		return false;

	dir = edge2.Cross(Vector3::AXIS[1]);
	ProjectTriangle(dir, triVerts, &min0, &max0);
	ProjectBox(dir, center, extent, &min1, &max1);
	if (max1 < min0 || max0 < min1)
		return false;

	dir = edge2.Cross(Vector3::AXIS[2]);
	ProjectTriangle(dir, triVerts, &min0, &max0);
	ProjectBox(dir, center, extent, &min1, &max1);
	if (max1 < min0 || max0 < min1)
		return false;

    return true;
}

}
