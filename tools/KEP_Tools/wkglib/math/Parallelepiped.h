/******************************************************************************
 Parallelepiped.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "ViewVolume.h"

namespace wkg {

class CoordFrame;

class Parallelepiped : public ViewVolume
{
	public:
		Parallelepiped();		// uninitialized

		void Set(const CoordFrame&, 
			real hither, real yon, 
			real width, real height);
};

inline
Parallelepiped::Parallelepiped()
{
	/* uninitialized */
}

}
