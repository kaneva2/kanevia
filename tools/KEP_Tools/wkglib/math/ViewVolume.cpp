/******************************************************************************
 ViewVolume.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "wkgtypes.h"
#include "ViewVolume.h"
#include "AABB.h"

namespace wkg {

bool 
ViewVolume::Includes(const AABB& box, uint8* active, real tolerance) const
{
	// todo: use the speed-up techniques discussed in RTR
	//
	if (*active)
	{
		uint8 bit = 1;

		for (int32 i = 0; i < NUMPLANES; i++)
		{
			if (*active & bit)
			{
				switch (plane[i].Classify(box, tolerance))
				{
					case Plane::POS:
						return false;

					case Plane::NEG:
						*active &= ~bit;
						break;
				}
			}
			bit <<= 1;
		}
	}
	return true;
}

}