/******************************************************************************
 IntersectTriRay.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "wkgtypes.h"
#include "Intersect.h"
#include "Triangle.h"
#include "Ray.h"
#include "Vector3.h"

namespace wkg {

// This code is based upon the Ray-Triangle intersection test from Tomas Moller and Ben Trumbore.
//
// http://www.ce.chalmers.se/staff/tomasm/raytri/raytri.c
// or 
// http://www.acm.org/jgt/papers/MollerTrumbore97/
//

bool 
Intersect::TriRay(const Triangle& tri, const Ray& ray, real* t)
{
	Vector3 tvec, pvec, qvec;
	real det, u, v;

	pvec = ray.dir.Cross(tri.GetEdge1());	// begin calculating determinant -- also used to calculate U parameter

	det = tri.GetEdge0().Dot(pvec);			// if determinant is near zero, ray lies in plane of triangle

	if (det < tolerance)					// back-face triangles aren't considered
		return false;

	tvec = ray.from - tri.GetOrigin();		// calculate distance from vert0 to ray origin

	u = tvec.Dot(pvec);						// calculate U parameter and test bounds
	if (u < 0.0f || u > det)
		return false;

	qvec = tvec.Cross(tri.GetEdge0());		// prepare to test V parameter

	v = ray.dir.Dot(qvec);					// calculate V parameter and test bounds
	if (v < 0.0f || u + v > det)
		return false;

	if (t)									// calculate where ray hits triangle (along the ray)
		*t = tri.GetEdge1().Dot(qvec) / det;

	return true;
}

}
