#include "wkgtypes.h"
#include "Distance.h"
#include "Vector3.h"
#include "AABB.h"

namespace wkg {

//--------- This code is *entirely* based on the following (they deserve credit :)

// Magic Software, Inc.
// http://www.magic-software.com
// Copyright (c) 2000-2002.  All Rights Reserved
//
// Source code from Magic Software is supplied under the terms of a license
// agreement and may not be copied or disclosed except in accordance with the
// terms of that agreement.  The various license agreements may be found at
// the Magic Software web site.  This file is subject to the license
//
// FREE SOURCE CODE
// http://www.magic-software.com/License/free.pdf

real 
Distance::SqrPointAABB(const Vector3& rkPoint, const AABB& rkBox, real* pfLParam, real* pfBParam0, real* pfBParam1, real* pfBParam2)
{
#ifdef _DEBUG
    // The three parameters pointers are either all non-null or all null.
    if ( pfBParam0 )
    {
        ASSERT( pfBParam1 && pfBParam2 );
    }
    else
    {
        ASSERT( !pfBParam1 && !pfBParam2 );
    }
#endif

    // compute coordinates of point in box coordinate system
    Vector3 kDiff = rkPoint - rkBox.GetCenter();

	// next statement used to be this, but since box is axis aligned,
	// use the x and y axes
//	Vector3 kClosest(kDiff.Dot(rkBox.Axis(0)),kDiff.Dot(rkBox.Axis(1)),
//		kDiff.Dot(rkBox.Axis(2)));

    Vector3 kClosest(kDiff.Dot(Vector3::AXIS[0]),kDiff.Dot(Vector3::AXIS[1]),
        kDiff.Dot(Vector3::AXIS[2]));

    // project test point onto box
    real fSqrDistance = 0.0f;
    real fDelta;
	Vector3 extent = rkBox.GetExtent();

    if ( kClosest.x < -extent.x )
    {
        fDelta = kClosest.x + extent.x;
        fSqrDistance += fDelta*fDelta;
        kClosest.x = -extent.x;
    }
    else if ( kClosest.x > extent.x )
    {
        fDelta = kClosest.x - extent.x;
        fSqrDistance += fDelta*fDelta;
        kClosest.x = extent.x;
    }

    if ( kClosest.y < -extent.y )
    {
        fDelta = kClosest.y + extent.y;
        fSqrDistance += fDelta*fDelta;
        kClosest.y = -extent.y;
    }
    else if ( kClosest.y > extent.y )
    {
        fDelta = kClosest.y - extent.y;
        fSqrDistance += fDelta*fDelta;
        kClosest.y = extent.y;
    }

    if ( kClosest.z < -extent.z )
    {
        fDelta = kClosest.z + extent.z;
        fSqrDistance += fDelta*fDelta;
        kClosest.z = -extent.z;
    }
    else if ( kClosest.z > extent.z )
    {
        fDelta = kClosest.z - extent.z;
        fSqrDistance += fDelta*fDelta;
        kClosest.z = extent.z;
    }

    if ( pfBParam0 )
    {
        *pfBParam0 = kClosest.x;
        *pfBParam1 = kClosest.y;
        *pfBParam2 = kClosest.z;
    }

    return fSqrDistance;
}


}