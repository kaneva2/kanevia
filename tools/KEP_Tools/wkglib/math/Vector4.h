/******************************************************************************
 Vector4.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

namespace wkg {

class Matrix44;

class Vector4
{
    public:
	    Vector4();												// uninit
	    Vector4(real x, real y, real z, real w = 1.0f);			// w = 1.0f by default (i.e. a point)

		bool operator==(const Vector4&) const;
		bool operator!=(const Vector4&) const;

	    Vector4& Set(real x, real y, real z, real w = 1.0f);	// w = 1.0f by default (i.e. a point)
		Vector4& Zero();										// set to all zeros

	    Vector4 operator-() const;

	    Vector4 operator+(const Vector4&) const;
	    Vector4 operator-(const Vector4&) const;
	    Vector4 operator*(real) const;
	    Vector4 operator/(real) const;

	    Vector4& operator+=(const Vector4&);
	    Vector4& operator-=(const Vector4&);

		Vector4& operator+=(real);
		Vector4& operator-=(real);
	    Vector4& operator*=(real);
	    Vector4& operator/=(real);

	    real Dot(const Vector4&) const;
	    Vector4 Cross(const Vector4&) const;

	    real Length() const;
	    real LengthSqr() const;

	    Vector4& Normalize();
	    Vector4& Homogenize();

	    Vector4 operator*(const Matrix44&) const;
	    Vector4& operator*=(const Matrix44&);
	    
		Vector4 MultTransposeOf(const Matrix44&) const;
		Vector4& MultEqTransposeOf(const Matrix44&);

		operator real*();
		operator const real*() const;

		void Dump() const;

		SERIALIZABLE

	public:
		union
		{
			struct
			{
				real x, y, z, w;
			};

			real v[4];
		};

	public:
		static const Vector4 ZERO;		// 0.0, 0.0, 0.0, 1.0  (i.e. a point)
		static const Vector4 ONE;		// 1.0, 1.0, 1.0, 1.0  (i.e. a point)
		static const Vector4 AXIS[3];	// order: x, y, z (all vectors; w = 0)
};

inline
Vector4::Vector4()
{
	// uninit
}

inline
Vector4::Vector4(const real nx, const real ny, const real nz, const real nw)
	: x(nx), y(ny), z(nz), w(nw)
{
}
 
inline bool 
Vector4::operator==(const Vector4& v) const
{
	return EQUAL(x, v.x) && EQUAL(y, v.y) && EQUAL(z, v.z) && EQUAL(w, v.w);
}

inline bool 
Vector4::operator!=(const Vector4& v) const
{
	return !operator==(v);
}

inline Vector4&
Vector4::Set(const real nx, const real ny, const real nz, const real nw)
{
	x = nx; 
	y = ny; 
	z = nz; 
	w = nw;

	return *this;
}

inline Vector4&
Vector4::Zero()
{
	return Set(0.0f, 0.0f, 0.0f, 0.0f);
}

inline Vector4 
Vector4::operator-() const
{
	return Vector4(-x, -y, -z, -w);
}

inline Vector4 
Vector4::operator+(const Vector4& v) const
{
	return Vector4(x + v.x, y + v.y, z + v.z, w + v.w);
}

inline Vector4 
Vector4::operator-(const Vector4& v) const
{
	return Vector4(x - v.x, y - v.y, z - v.z, w - v.w);
}

inline Vector4 
Vector4::operator*(real scalar) const
{
	return Vector4(x * scalar, y * scalar, z * scalar, w * scalar);
}

inline Vector4 
Vector4::operator/(real scalar) const
{
	ASSERT(scalar != 0.0f);
	return operator*(1.0f / scalar);
}

inline Vector4& 
Vector4::operator+=(real scalar)
{
	x += scalar;
	y += scalar;
	z += scalar;
	w += scalar;

	return *this;
}

inline Vector4& 
Vector4::operator-=(real scalar)
{
	x -= scalar;
	y -= scalar;
	z -= scalar;
	w -= scalar;

	return *this;
}

inline Vector4& 
Vector4::operator*=(real scalar)
{
	x *= scalar;
	y *= scalar;
	z *= scalar;
	w *= scalar;

	return *this;
}

inline Vector4& 
Vector4::operator/=(real scalar)
{
	ASSERT(scalar != 0.0f);
	return operator*=(1.0f / scalar);
}

inline Vector4& 
Vector4::operator+=(const Vector4& v)
{
	x += v.x;
	y += v.y;
	z += v.z;
	w += v.w;

	return *this;
}

inline Vector4& 
Vector4::operator-=(const Vector4& v)
{
	x -= v.x;
	y -= v.y;
	z -= v.z;
	w -= v.w;

	return *this;
}

inline real 
Vector4::Dot(const Vector4& v) const
{
	return x * v.x + y * v.y + z * v.z + w * v.w;
}

inline Vector4 
Vector4::Cross(const Vector4& v) const
{
	return Vector4(
		y * v.z - z * v.y,
		z * v.x - x * v.z,
		x * v.y - y * v.x,
		0.0f);			// cross product is defined for *vectors* (not points)
}

inline real 
Vector4::LengthSqr() const
{
	return Dot(*this);
}

inline real 
Vector4::Length() const
{
	return sqrt(LengthSqr());
}

inline Vector4&
Vector4::Normalize()
{
	real len = Length();

	if (len > 0.0f)
		return operator/=(len);

	ASSERT(0);
	return *this;
}

inline Vector4&
Vector4::Homogenize()
{
	if (w != 0.0 && w != 1.0)
		return operator/=(w);
	ASSERT(w == 1.0f);
	return *this;
}

inline void 
Vector4::Dump() const
{
	DBPRINTLN(("<%.2f, %.2f, %.2f, %.2f>", x, y, z, w));
}

inline 
Vector4::operator real*()
{
	return v;
}

inline 
Vector4::operator const real*() const
{
	return v;
}

}
