/******************************************************************************
 IntersectAABBSphere.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "wkgtypes.h"
#include "Intersect.h"
#include "AABB.h"
#include "Sphere.h"

namespace wkg {

bool 
Intersect::AABBSphere(const AABB &aabb, const Sphere &sphere)
{
	real s, d = 0; 

	if (sphere.GetCenter().x < aabb.GetMin().x)
	{
		s = sphere.GetCenter().x - aabb.GetMin().x;
		d += s * s; 
	}
	else if (sphere.GetCenter().x > aabb.GetMax().x)
	{ 
		s = sphere.GetCenter().x - aabb.GetMax().x;
		d += s * s; 
	}

	if (sphere.GetCenter().y < aabb.GetMin().y)
	{
		s = sphere.GetCenter().y - aabb.GetMin().y;
		d += s * s; 
	}
	else if (sphere.GetCenter().y > aabb.GetMax().y)
	{ 
		s = sphere.GetCenter().y - aabb.GetMax().y;
		d += s * s; 
	}

	if (sphere.GetCenter().z < aabb.GetMin().z)
	{
		s = sphere.GetCenter().z - aabb.GetMin().z;
		d += s * s; 
	}
	else if (sphere.GetCenter().z > aabb.GetMax().z)
	{ 
		s = sphere.GetCenter().z - aabb.GetMax().z;
		d += s * s; 
	}

	return d <= sphere.GetRadiusSqr();
}

}