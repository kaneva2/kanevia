/******************************************************************************
 CoordFrame.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "wkgtypes.h"
#include "CoordFrame.h"
#include "Matrix44.h"
#include "Vector4.h"
#include "Quaternion.h"

namespace wkg {

void 
Axis3D::SetLH(const Vector3& dirVec, const Vector3& upHint)
{
	real angle;

	at = dirVec;
	at.Normalize();

	up = upHint;
	up.Normalize();

	// find real up axis orthogonal to at axis
	//		project upHint onto axis in 
	//		its direction which is orthogonal to at
	// check if upHint and dirVec are (close to) parallel first
	// (if parallel, pick another axis arbitrarily...project onto plane of two smallest axis)
	//
	angle = at.Dot(up);
	if (EQUAL(ABS(angle), 1.0f))
	{
#		define ONE_OVER_ROOT2	((real)0.70710678118654752440084436210485)

		if (ABS(up.x) > ABS(up.y))
			if (ABS(up.x) > ABS(up.z))
				up.Set(0.0f, ONE_OVER_ROOT2, ONE_OVER_ROOT2);
			else
				up.Set(ONE_OVER_ROOT2, ONE_OVER_ROOT2, 0.0f);
		else if (ABS(up.y) > ABS(up.z))
			up.Set(ONE_OVER_ROOT2, 0.0f, ONE_OVER_ROOT2);
		else
			up.Set(ONE_OVER_ROOT2, ONE_OVER_ROOT2, 0.0f);

#		undef ONE_OVER_ROOT2
	}

	up = up - (at * at.Dot(up));
	up.Normalize();

	right = up.Cross(at);
}

void 
Axis3D::SetLH(const Vector4& dirVec, const Vector4& upHint)
{
	SetLH(*((const Vector3*)&dirVec), *((const Vector3*)&upHint));
}

void 
Axis3D::SetLH(const Quaternion& orientation)
{
	Matrix44 m = orientation;

	right.Set(m._11, m._12, m._13);	// xaxis is 1st row of matrix
	up.Set(m._21, m._22, m._23);	// yaxis is 2nd row of matrix
	at.Set(m._31, m._32, m._33);	// zaxis is 3rd row of matrix
}

void 
Axis3D::SetRH(const Vector3& dirVec, const Vector3& upHint)
{
	SetLH(-dirVec, upHint);
}

void 
Axis3D::SetRH(const Vector4& dirVec, const Vector4& upHint)
{
	SetRH(*((const Vector3*)&dirVec), *((const Vector3*)&upHint));
}

void 
Axis3D::SetRH(const Quaternion& orientation)
{
	Matrix44 m = orientation;

	right.Set(m._11, m._12, m._13);	// xaxis is 1st row of matrix
	up.Set(m._21, m._22, m._23);	// yaxis is 2nd row of matrix
	at.Set(-m._31, -m._32, -m._33);	// zaxis is 3rd row of matrix ('-' makes it right handed)
}

Matrix44&
Axis3D::GetXYZToAxis3D(Matrix44& result) const
{
	result._11 = right.x;	result._12 = right.y;	result._13 = right.z;	result._14 = 0.0f;
	result._21 = up.x;		result._22 = up.y;		result._23 = up.z;		result._24 = 0.0f;
	result._31 = at.x;		result._32 = at.y;		result._33 = at.z;		result._34 = 0.0f;
	result._41 = 0.0f;		result._42 = 0.0f;		result._43 = 0.0f;		result._44 = 1.0f;
	return result;
}

Matrix44&
Axis3D::GetAxis3DToXYZ(Matrix44& result) const
{
	result._11 = right.x;	result._12 = up.x;	result._13 = at.x;	result._14 = 0.0f;
	result._21 = right.y;	result._22 = up.y;	result._23 = at.y;	result._24 = 0.0f;
	result._31 = right.z;	result._32 = up.z;	result._33 = at.z;	result._34 = 0.0f;
	result._41 = 0.0f;		result._42 = 0.0f;	result._43 = 0.0f;	result._44 = 1.0f;
	return result;
}

void 
CoordFrame::SetLookAt(const Vector4& from, const Vector4& lookAt, const Vector4& upHint)
{
	SetLookAt(
		*((const Vector3*)&from),
		*((const Vector3*)&lookAt),
		*((const Vector3*)&upHint));
}

void 
CoordFrame::SetLookDir(const Vector4& from, const Vector4& lookDir, const Vector4& upHint)
{
	SetLookDir(
		*((const Vector3*)&from),
		*((const Vector3*)&lookDir),
		*((const Vector3*)&upHint));
}

void 
CoordFrame::SetLookDir(const Vector4& from, const Quaternion& orientation)
{
	SetLookDir(
		*((const Vector3*)&from),
		orientation);
}

Matrix44&
CoordFrame::GetXYZToCFrame(Matrix44& result) const
{
	// R(chg of basis) * T(origin)
	//
	axis.GetXYZToAxis3D(result);
	result._41 = origin.x;				
	result._42 = origin.y;				
	result._43 = origin.z;				
	return result;
}

Matrix44&
CoordFrame::GetCFrameToXYZ(Matrix44& result) const
{
	// T(-origin) * R(chg of basis)
	//
	axis.GetAxis3DToXYZ(result);
	result._41 = -axis.GetXAxis().Dot(origin);	
	result._42 = -axis.GetYAxis().Dot(origin);	
	result._43 = -axis.GetZAxis().Dot(origin);
	return result;
}

void 
CoordFrameLH::SetLookAt(const Vector3& from, const Vector3& lookAt, const Vector3& upHint)
{
	origin = from;
 	axis.SetLH(lookAt - from, upHint);
}

void 
CoordFrameLH::SetLookDir(const Vector3& from, const Vector3& lookDir, const Vector3& upHint)
{
	origin = from;
	axis.SetLH(lookDir, upHint);
}

void 
CoordFrameLH::SetLookDir(const Vector3& from, const Quaternion& orientation)
{
	origin = from;
	axis.SetLH(orientation);
}

void 
CoordFrameRH::SetLookAt(const Vector3& from, const Vector3& lookAt, const Vector3& upHint)
{
	origin = from;
	axis.SetRH(lookAt - from, upHint);
}

void 
CoordFrameRH::SetLookDir(const Vector3& from, const Vector3& lookDir, const Vector3& upHint)
{
	origin = from;
	axis.SetRH(-lookDir, upHint);
}

void 
CoordFrameRH::SetLookDir(const Vector3& from, const Quaternion& orientation)
{
	origin = from;
	axis.SetRH(orientation);
}

}