#include "wkgtypes.h"
#include "Distance.h"
#include "LineSeg.h"

namespace wkg {
	
//--------- This code is *entirely* based on the following (they deserve credit :)

// Magic Software, Inc.
// http://www.magic-software.com
// Copyright (c) 2000-2002.  All Rights Reserved
//
// Source code from Magic Software is supplied under the terms of a license
// agreement and may not be copied or disclosed except in accordance with the
// terms of that agreement.  The various license agreements may be found at
// the Magic Software web site.  This file is subject to the license
//
// FREE SOURCE CODE
// http://www.magic-software.com/License/free.pdf

real 
Distance::SqrSegPoint(const LineSegment& seg, const Vector3& point)
{
    Vector3 kDiff = point - seg.from;
    real fT = kDiff.Dot(seg.dir);

    if ( fT <= 0.0f )
    {
        fT = 0.0f;
    }
    else
    {
        real fSqrLen = seg.dir.LengthSqr();
        if ( fT >= fSqrLen )
        {
            fT = 1.0f;
            kDiff -= seg.dir;
        }
        else
        {
            fT /= fSqrLen;
            kDiff -= seg.dir*fT;
        }
    }

    return kDiff.LengthSqr();
}

}
