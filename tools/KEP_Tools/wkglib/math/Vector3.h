/******************************************************************************
 Vector3.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#ifndef _VECTOR3_H_
#define _VECTOR3_H_

namespace wkg {

class Matrix44;

class Vector3
{
	public:
		Vector3();	// uninitialized
		Vector3(real x, real y, real z);

		bool operator==(const Vector3& v) const;
		bool operator!=(const Vector3& v) const;

		Vector3& Set(real x, real y, real z);
		Vector3& Zero();						// set to all zeros

		Vector3 operator-(void) const;

		Vector3 operator+(const Vector3& v) const;
		Vector3 operator-(const Vector3& v) const;
		Vector3 operator*(real scalar) const;
		Vector3 operator/(real scalar) const;
		
		Vector3& operator+=(const Vector3& v);
		Vector3& operator-=(const Vector3& v);

		Vector3& operator+=(real scalar);
		Vector3& operator-=(real scalar);
		Vector3& operator*=(real scalar);
		Vector3& operator/=(real scalar);

		real Dot(const Vector3& v) const;
		Vector3 Cross(const Vector3& v) const;

		real Length() const;
		real LengthSqr() const;

		Vector3& Normalize();

		Vector3 PtMult(const Matrix44&) const;						// treat Vector3 as a point (w = 1.0f)
		Vector3 PtMultProject(const Matrix44&) const;				// treat Vector3 as a point (w = 1.0f), after transform, homogenize (so w == 1.0 again)
		Vector3 PtMultTransposeOf(const Matrix44&) const;			// treat Vector3 as a point (w = 1.0f)
		Vector3 PtMultTransposeOfProject(const Matrix44&) const;	// treat Vector3 as a point (w = 1.0f), after transform, homogenize (so w == 1.0 again)

		Vector3& PtMultEq(const Matrix44&);							// treat Vector3 as a point (w = 1.0f)
		Vector3& PtMultEqProject(const Matrix44&);					// treat Vector3 as a point (w = 1.0f), after transform, homogenize (so w == 1.0 again)
		Vector3& PtMultEqTransposeOf(const Matrix44&);				// treat Vector3 as a point (w = 1.0f)
		Vector3& PtMultEqTransposeOfProject(const Matrix44&);		// treat Vector3 as a point (w = 1.0f), after transform, homogenize (so w == 1.0 again)

		Vector3 VecMult(const Matrix44&) const;						// treat Vector3 as a vector (w = 0.0f)
		Vector3 VecMultTransposeOf(const Matrix44&) const;			// treat Vector3 as a vector (w = 0.0f)

		Vector3& VecMultEq(const Matrix44&);						// treat Vector3 as a vector (w = 0.0f)
		Vector3& VecMultEqTransposeOf(const Matrix44&);				// treat Vector3 as a vector (w = 0.0f)

		operator real*();
		operator const real*() const;

		void Dump() const;

//		SERIALIZABLE

	public:
		union
		{
			struct
			{
				real x, y, z;
			};

			real v[3];
		};

	public:
		static const Vector3 ZERO;		// 0.0f, 0.0f, 0.0f
		static const Vector3 ONE;		// 1.0f, 1.0f, 1.0f
		static const Vector3 AXIS[3];	// order: x, y, z
};

inline
Vector3::Vector3() 
{
	/* uninitialized */ 
}

inline
Vector3::Vector3(real newx, real newy, real newz) 
	: x(newx), y(newy), z(newz)
{
}

inline bool 
Vector3::operator==(const Vector3& v) const
{
	return EQUAL(x, v.x) && EQUAL(y, v.y) && EQUAL(z, v.z);
}

inline bool
Vector3::operator!=(const Vector3& v) const
{
	return !operator==(v);
}

inline Vector3& 
Vector3::Set(real nx, real ny, real nz)
{
	x = nx;
	y = ny;
	z = nz;

	return *this;
}

inline Vector3&
Vector3::Zero()
{
	return Set(0.0f, 0.0f, 0.0f);
}

inline Vector3 
Vector3::operator-(void) const
{
	return Vector3(-x, -y, -z);
}

inline Vector3 
Vector3::operator+(const Vector3& v) const
{
	return Vector3(x + v.x, y + v.y, z + v.z);
}

inline Vector3 
Vector3::operator-(const Vector3& v) const
{
	return Vector3(x - v.x, y - v.y, z - v.z);
}

inline Vector3 
Vector3::operator*(real scalar) const
{
	return Vector3(x * scalar, y * scalar, z * scalar);
}

inline Vector3 
Vector3::operator/(real scalar) const
{
	ASSERT(scalar != 0.0f);
	return operator*(1.0f / scalar);
}

inline Vector3&
Vector3::operator+=(real scalar)
{
	x += scalar;
	y += scalar;
	z += scalar;

	return *this;
}

inline Vector3&
Vector3::operator-=(real scalar)
{
	x -= scalar;
	y -= scalar;
	z -= scalar;

	return *this;
}

inline Vector3&
Vector3::operator*=(real scalar)
{
	x *= scalar;
	y *= scalar;
	z *= scalar;

	return *this;
}

inline Vector3&
Vector3::operator/=(real scalar)
{
	return operator*=(1.0f / scalar);
}

inline Vector3& 
Vector3::operator+=(const Vector3& v)
{
	x += v.x;
	y += v.y;
	z += v.z;

	return *this;
}

inline Vector3&
Vector3::operator-=(const Vector3& v)
{
	x -= v.x;
	y -= v.y;
	z -= v.z;

	return *this;
}

inline real 
Vector3::Dot(const Vector3& v) const
{
	return x * v.x + y * v.y + z * v.z;
}

inline Vector3 
Vector3::Cross(const Vector3& v) const
{
	return Vector3(
		y * v.z - z * v.y,
		z * v.x - x * v.z,
		x * v.y - y * v.x);
}

inline real 
Vector3::LengthSqr() const
{
	return Dot(*this);
}

inline void 
Vector3::Dump() const
{
	DBPRINTLN(("<%.4f, %.4f, %.4f>", x, y, z));
}

inline 
Vector3::operator real*()
{
	return v;
}

inline 
Vector3::operator const real*() const
{
	return v;
}

inline real
Vector3::Length() const
{
	return ::sqrtf(LengthSqr());
}

inline Vector3&
Vector3::Normalize()
{
	real len = Length();

	if (len > 0.0f)
		return operator/=(len);

//	ASSERT(0);

	return *this;
}


}

#endif
