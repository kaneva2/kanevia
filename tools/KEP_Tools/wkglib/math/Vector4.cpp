/******************************************************************************
 Vector4.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "wkgtypes.h"
#include "Vector4.h"	
#include "Matrix44.h"
		
namespace wkg {

const Vector4 Vector4::ZERO(0.0f, 0.0f, 0.0f, 1.0f);
const Vector4 Vector4::ONE(1.0f, 1.0f, 1.0f, 1.0f);
const Vector4 Vector4::AXIS[] =
{
	Vector4(1.0f, 0.0f, 0.0f, 0.0f),
	Vector4(0.0f, 1.0f, 0.0f, 0.0f),
	Vector4(0.0f, 0.0f, 1.0f, 0.0f)
};

Vector4 
Vector4::operator*(const Matrix44& mat) const
{
	return Vector4(
		x * mat._11 + y * mat._21 + z * mat._31 + w * mat._41,
		x * mat._12 + y * mat._22 + z * mat._32 + w * mat._42,
		x * mat._13 + y * mat._23 + z * mat._33 + w * mat._43,
		x * mat._14 + y * mat._24 + z * mat._34 + w * mat._44);
}

Vector4&
Vector4::operator*=(const Matrix44 &mat)
{
	*this = operator*(mat);
	return *this;
}

Vector4 
Vector4::MultTransposeOf(const Matrix44& mat) const
{
	return Vector4(
		x * mat._11 + y * mat._12 + z * mat._13 + w * mat._14,
		x * mat._21 + y * mat._22 + z * mat._23 + w * mat._24,
		x * mat._31 + y * mat._32 + z * mat._33 + w * mat._34,
		x * mat._41 + y * mat._42 + z * mat._43 + w * mat._44);
}

Vector4&
Vector4::MultEqTransposeOf(const Matrix44& mat)
{
	*this = MultTransposeOf(mat);
	return *this;	
}

#ifdef USE_SERIALIZATION
}
#include "Stream.h"
namespace wkg {

bool 
Vector4::Load(Stream* stream)
{
	return stream->Read(&x)
		&& stream->Read(&y)
		&& stream->Read(&z)
		&& stream->Read(&w);
}

bool 
Vector4::Save(Stream* stream) const
{
	return stream->Write(x)
		&& stream->Write(y)
		&& stream->Write(z)
		&& stream->Write(w);
}

#endif

}
