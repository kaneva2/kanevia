/******************************************************************************
 Parallelepiped.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "wkgtypes.h"
#include "Parallelepiped.h"
#include "CoordFrame.h"

namespace wkg {

void 
Parallelepiped::Set(const CoordFrame& frame,
	real hither, real yon,
	real width,
	real height)
{
	cop = frame.GetOrigin();

	plane[HITHER].normal	= -frame.GetZAxis();
	plane[HITHER].constant	= -(plane[HITHER].normal.Dot(cop + frame.GetZAxis() * hither));

	plane[YON].normal		= frame.GetZAxis();
	plane[YON].constant		= -(plane[YON].normal.Dot(cop + frame.GetZAxis() * yon));

	plane[LEFT].normal		= -frame.GetXAxis();
	plane[LEFT].constant	= -(plane[LEFT].normal.Dot(cop - frame.GetXAxis() * (width * 0.5f)));

	plane[RIGHT].normal		= frame.GetXAxis();
	plane[RIGHT].constant	= -(plane[RIGHT].normal.Dot(cop + frame.GetXAxis() * (width * 0.5f)));

	plane[TOP].normal		= frame.GetYAxis();
	plane[TOP].constant		= -(plane[TOP].normal.Dot(cop + frame.GetYAxis() * (height * 0.5f)));

	plane[BOTTOM].normal	= -frame.GetYAxis();
	plane[BOTTOM].constant	= -(plane[BOTTOM].normal.Dot(cop - frame.GetYAxis() * (height * 0.5f)));
}

}