#include "wkgtypes.h"
#include "Intersect.h"
#include "Triangle.h"

namespace wkg {

//--------- This code is *entirely* based on the following (they deserve credit :)

// Magic Software, Inc.
// http://www.magic-software.com
// Copyright (c) 2000-2002.  All Rights Reserved
//
// Source code from Magic Software is supplied under the terms of a license
// agreement and may not be copied or disclosed except in accordance with the
// terms of that agreement.  The various license agreements may be found at
// the Magic Software web site.  This file is subject to the license
//
// FREE SOURCE CODE
// http://www.magic-software.com/License/free.pdf

static inline bool 
IsDisjoint(real umin, real umax, real vmin, real vmax)
{
	return umax < vmin || umin > vmax;
}

static inline void
ProjectionInterval(const Vector3& axis, const Vector3 vert[3], real* min, real* max)
{
	real dot0, dot1, dot2;

	dot0 = axis.Dot(vert[0]);
	dot1 = axis.Dot(vert[1]);
	dot2 = axis.Dot(vert[2]);

	*min = *max = dot0;
	if (dot1 < *min)
		*min = dot1;
	else if (dot1 > *max)
		*max = dot1;

	if (dot2 < *min)
		*min = dot2;
	else if (dot2 > *max)
		*max = dot2;
}

#define CheckSeparatingAxis(Axis)				\
	ProjectionInterval(Axis, U, &umin, &umax);	\
	ProjectionInterval(Axis, V, &vmin, &vmax);	\
	if (IsDisjoint(umin, umax, vmin, vmax))		\
		return false

bool 
Intersect::TriTri(const Triangle& tri0, const Triangle& tri1)
{
	Vector3 N, M, U[3], V[3], E2, F2, axis;
	real umin, umax, vmin, vmax;

	// check tri0 normal as sep axis
	//
	N = tri0.GetNormal();
	umin = umax = N.Dot(tri0.GetOrigin());
	tri1.GetVerts(V);
	ProjectionInterval(N, V, &vmin, &vmax);
	if (IsDisjoint(umin, umax, vmin, vmax))
		return false;

	// check if triangles are parallel
	//
	M = tri1.GetNormal();
	tri0.GetVerts(U);
	
	axis = N.Cross(M);
	if (axis.Dot(axis) >= tolerance * N.Dot(N) * M.Dot(M))
	{
		// not parallel
		//
		// check tri1 normal as sep axis
		//
		vmin = vmax = M.Dot(tri1.GetOrigin());
		ProjectionInterval(M, U, &umin, &umax);
		if (IsDisjoint(umin, umax, vmin, vmax))
			return false;

		// --------------------------- E0 x F[012] ----------------------------
		//
		axis = tri0.GetEdge0().Cross(tri1.GetEdge0());
		CheckSeparatingAxis(axis);

		axis = tri0.GetEdge0().Cross(tri1.GetEdge1());
		CheckSeparatingAxis(axis);

		F2 = tri1.GetEdge2();
		axis = tri0.GetEdge0().Cross(F2);
		CheckSeparatingAxis(axis);

		// --------------------------- E1 x F[012] ----------------------------
		//
		axis = tri0.GetEdge1().Cross(tri1.GetEdge0());
		CheckSeparatingAxis(axis);

		axis = tri0.GetEdge1().Cross(tri1.GetEdge1());
		CheckSeparatingAxis(axis);

		axis = tri0.GetEdge1().Cross(F2);
		CheckSeparatingAxis(axis);

		// --------------------------- E2 x F[012] ----------------------------
		//
		E2 = tri0.GetEdge2();
		axis = E2.Cross(tri1.GetEdge0());
		CheckSeparatingAxis(axis);

		axis = E2.Cross(tri1.GetEdge1());
		CheckSeparatingAxis(axis);

		axis = E2.Cross(F2);
		CheckSeparatingAxis(axis);
	}
	else
	{
		// parallel (and coplanar since N is not a sep axis)
		//
		// --------------------------- N x E[012] ----------------------------
		//
		axis = N.Cross(tri0.GetEdge0());
		CheckSeparatingAxis(axis);
		
		axis = N.Cross(tri0.GetEdge1());
		CheckSeparatingAxis(axis);
		
		axis = N.Cross(tri0.GetEdge2());
		CheckSeparatingAxis(axis);
		
		// --------------------------- M x F[012] ----------------------------
		//
		axis = M.Cross(tri1.GetEdge0());
		CheckSeparatingAxis(axis);
		
		axis = M.Cross(tri1.GetEdge1());
		CheckSeparatingAxis(axis);
		
		axis = M.Cross(tri1.GetEdge2());
		CheckSeparatingAxis(axis);
	}

	return true;
}

}
