#include "wkgtypes.h"
#include "Distance.h"
#include "LineSeg.h"
#include "AABB.h"
#include "line.h"

namespace wkg {

//--------- This code is *entirely* based on the following (they deserve credit :)

// Magic Software, Inc.
// http://www.magic-software.com
// Copyright (c) 2000-2002.  All Rights Reserved
//
// Source code from Magic Software is supplied under the terms of a license
// agreement and may not be copied or disclosed except in accordance with the
// terms of that agreement.  The various license agreements may be found at
// the Magic Software web site.  This file is subject to the license
//
// FREE SOURCE CODE
// http://www.magic-software.com/License/free.pdf

real 
Distance::SqrSegAABB(const LineSegment& rkSeg, const AABB& rkBox, real* pfLParam, real* pfBParam0, real* pfBParam1, real* pfBParam2)
{
    Line kLine;
    kLine.pt = rkSeg.from;
    kLine.dir = rkSeg.dir;

    real fLP, fBP0, fBP1, fBP2;
    real fSqrDistance = Distance::SqrLineAABB(kLine,rkBox,&fLP,&fBP0,&fBP1,&fBP2);
    if ( fLP >= 0.0f )
    {
        if ( fLP <= 1.0f )
        {
            if (pfLParam)
                *pfLParam = fLP;
			if (pfBParam0)
                *pfBParam0 = fBP0;
			if (pfBParam1)
                *pfBParam1 = fBP1;
			if (pfBParam1)
                *pfBParam2 = fBP2;

            return fSqrDistance;
        }
        else
        {
            fSqrDistance = Distance::SqrPointAABB(rkSeg.from+rkSeg.dir,
                rkBox,pfBParam0,pfBParam1,pfBParam2);

            if ( pfLParam )
                *pfLParam = 1.0f;

            return fSqrDistance;
        }
    }
    else
    {
        fSqrDistance = Distance::SqrPointAABB(rkSeg.from,rkBox,pfBParam0,pfBParam1,pfBParam2);

        if ( pfLParam )
            *pfLParam = 0.0f;

        return fSqrDistance;
    }

}


}