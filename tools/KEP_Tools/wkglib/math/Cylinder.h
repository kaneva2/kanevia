/******************************************************************************
 Cylinder.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "Vector3.h"

namespace wkg {

class Cylinder
{
	public:
		Cylinder();	// uninit
		Cylinder(const Vector3& base, const Vector3& axis, real radius);

		void Set(const Vector3& base, const Vector3& axis, real radius);
		void SetBase(const Vector3&);
		void SetAxis(const Vector3&);
		void SetRadius(real);

		const Vector3& GetBase() const;
		const Vector3& GetAxis() const;
		const real GetRadius() const;
		const real GetRadiusSqr() const;

		SERIALIZABLE

	protected:
		Vector3 base;
		Vector3 axis;
		real radius;
		real radiusSqr;
};

inline 
Cylinder::Cylinder()
{
	// uninit
}

inline void 
Cylinder::SetBase(const Vector3& pos)
{
	base = pos;
}

inline void 
Cylinder::SetAxis(const Vector3& dir)
{
	axis = dir;;
}

inline void 
Cylinder::SetRadius(real rad)
{
	radius = rad;
	radiusSqr = rad*rad;
}

inline void 
Cylinder::Set(const Vector3& pos, const Vector3& dir, real rad)
{
	SetBase(pos);
	SetAxis(dir);
	SetRadius(rad);
}

inline 
Cylinder::Cylinder(const Vector3& pos, const Vector3& dir, real rad)
{
	Set(pos, dir, rad);
}

inline const Vector3& 
Cylinder::GetBase() const
{
	return base;
}

inline const Vector3& 
Cylinder::GetAxis() const
{
	return axis;
}

inline const real 
Cylinder::GetRadius() const
{
	return radius;
}

inline const real 
Cylinder::GetRadiusSqr() const
{
	return radiusSqr;
}

}
