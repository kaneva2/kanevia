/******************************************************************************
 AABB.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "Vector3.h"

namespace wkg {

class AABB
{
	public:
		AABB();						// init
		AABB(const Vector3& min, const Vector3& max);

		void Add(const Vector3& point);
		void Add(Vector3 *points, int32 numPoints);

		void SetEmpty();			// set so that box contains nothing
		void Set(const Vector3&, const Vector3&);
		void SetMin(const Vector3&);
		void SetMax(const Vector3&);

		const Vector3& GetMin() const;
		const Vector3& GetMax() const;

		Vector3 GetCenter() const;
		Vector3 GetDiag() const;	// distance from center to corner (in each dimension)
		Vector3 GetExtent() const;	// distance from center to center of each face (in each dimension)

		bool Contains(const Vector3& point) const;
		bool Contains(const AABB&) const;

		SERIALIZABLE

	public:
		Vector3 min, max;
};

inline void
AABB::SetEmpty()
{
	min.x = min.y = min.z = MAXREAL;
	max.x = max.y = max.z = -MAXREAL;
}

inline 
AABB::AABB()
{
	SetEmpty();
}

inline
AABB::AABB(const Vector3& min, const Vector3& max)
{
	this->min = min;
	this->max = max;
}

inline void 
AABB::Add(const Vector3& point)
{
	min.x = MIN(min.x, point.x);
	min.y = MIN(min.y, point.y);
	min.z = MIN(min.z, point.z);
	max.x = MAX(max.x, point.x);
	max.y = MAX(max.y, point.y);
	max.z = MAX(max.z, point.z);
}

inline void 
AABB::Add(Vector3 *points, int32 numPoints)
{
	while (numPoints-- > 0)
		Add(points[numPoints]);
}

inline const Vector3& 
AABB::GetMin() const
{
	return min;
}

inline const Vector3& 
AABB::GetMax() const
{
	return max;
}

inline Vector3 
AABB::GetCenter() const
{
	return (min + max) * 0.5;
}

inline Vector3 
AABB::GetDiag() const
{
	return max - min;
}

inline Vector3 
AABB::GetExtent() const
{
	return GetDiag() * 0.5f;
}

inline void 
AABB::SetMin(const Vector3& min)
{
	this->min = min;
}

inline void 
AABB::SetMax(const Vector3& max)
{
	this->max = max;
}

inline void 
AABB::Set(const Vector3& min, const Vector3& max)
{
	SetMin(min);
	SetMax(max);
}

inline bool 
AABB::Contains(const Vector3& point) const
{
	return point.x >= min.x && point.x <= max.x 
		&& point.y >= min.y && point.y <= max.y
		&& point.z >= min.z && point.z <= max.z;
}

inline bool 
AABB::Contains(const AABB& other) const
{
	return Contains(other.min) && Contains(other.max);
}

}
