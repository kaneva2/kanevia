/******************************************************************************
 precompiled.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#pragma warning (disable : 4786)
#pragma warning (disable : 4503)

#define WIN32_LEAN_AND_MEAN
#define NOSERVICE
#define NOMCX
#define NOIME
#define NOSOUND
#define NOCOMM
#define NOKANJI
#define NORPC
#define NOPROXYSTUB
#define NOIMAGE
#define NOTAPE
#define ANSI_ONLY

#include <windows.h>
#include <mmsystem.h>

#include <d3d9.h>
#include <d3dx9.h>
#include <dxerr9.h>
#include <dsound.h>

#define DIRECTINPUT_VERSION 0x0800
#include <dinput.h>
#include <tchar.h>


// stl
#include <list>
#include <vector>
#include <string>
#include <map>

