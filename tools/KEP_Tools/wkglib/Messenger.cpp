/******************************************************************************
 Messenger.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "UniqueIDObj.h"
#include "Message.h"
#include "MessageQueue.h"
#include "Messenger.h"

namespace wkg
{

Messenger::Messenger() : 
	UniqueIDObj()
{
	MessageQueue::Instance()->RegisterMessenger(m_id, this);
}

Messenger::~Messenger()
{
	MessageQueue::Instance()->RegisterMessenger(m_id, 0);
}

void
Messenger::OnMessage(Message* received)
{
}



}

