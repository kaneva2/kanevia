/******************************************************************************
 Stream.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

namespace wkg 
{

class Stream
{
	public:
		Stream() { }
		virtual ~Stream() { }

		virtual void Close() = 0;

		virtual uint32 Read(uint8* dest, uint32 destMax) = 0;		// returns num Read
		virtual uint32 Write(uint8* src, uint32 srcAmt) = 0;		// returns num written

		virtual bool Read(real*) = 0;		// always single precision in the stream
		virtual bool Read(int32*) = 0;
		virtual bool Read(uint32*) = 0;
		virtual bool Read(int16*) = 0;
		virtual bool Read(uint16*) = 0;
		virtual bool Read(uint8*) = 0;
		virtual bool Read(int8*) = 0;

		virtual bool Write(real) = 0;		// always single precision in the stream
		virtual bool Write(int32) = 0;
		virtual bool Write(uint32) = 0;
		virtual bool Write(int16) = 0;
		virtual bool Write(uint16) = 0;
		virtual bool Write(uint8) = 0;
		virtual bool Write(int8) = 0;
};

}
