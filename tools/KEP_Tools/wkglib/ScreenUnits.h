/******************************************************************************
 ScreenUnits.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

namespace wkg 
{

class ScreenUnits
{
	public:
		enum Resolution
		{
			RES640x480,
			RES800x600,
			RES1024x768
		};

		static int32 GetCurrentScreenUnit(int32, Resolution res);
		static void SetResolution(Resolution res);
		
		static Resolution GetResolution() { return s_res; }
		static real GetResolutionMultiplier(Resolution res);
		static int32 GetCurrentResolutionWidth();
		static int32 GetCurrentResolutionHeight();

	protected:
		static Resolution s_res;
};

}

