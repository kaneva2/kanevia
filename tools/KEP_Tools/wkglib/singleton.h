/******************************************************************************
 singleton.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

// Very useful singleton pattern.
//  Nothing fancy - just keeps a static pointer to the last of this
//  type that was created with the new() operator.

// use it like this:
//  Device::Instance()->DoSomethingImportant();

#pragma once

namespace wkg 
{

template <typename T> class Singleton
{
	public:
		Singleton();	// initialized
		~Singleton();

		static T *Instance() { /*ASSERT(ms_instance); */return ms_instance; }

		static bool IsCreated() { return ms_instance != 0; }

	protected:
		static T *ms_instance;
};

template <typename T> T*
Singleton<T>::ms_instance = (T *)0;

template <typename T> 
Singleton<T>::Singleton()
{
	ASSERT(!ms_instance);
    ms_instance = static_cast<T *>(this);
}

template <typename T> 
Singleton<T>::~Singleton()
{
	ASSERT(ms_instance);
	ms_instance = static_cast<T *>(0);
}

}
