/******************************************************************************
 wkgtypes.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "compileOpts.h"
#include "precompiled.h"

// who cares:  identifier was truncated to '255' characters in the debug information (probably due to template expansion)
#pragma warning (disable : 4786)
#pragma warning (disable : 4503)

#include "wkgdebug.h"

#ifdef SINGLEPRECISION
	typedef float			real;
#else
	typedef double			real;
#endif

typedef unsigned __int64	uint64;
typedef unsigned long		uint32;
typedef unsigned short		uint16;
typedef __int64				int64;
typedef signed long			int32;
typedef signed short		int16;
typedef unsigned char		uint8;
typedef signed char			int8;

#include "wkgconst.h"
#include "wkgfunc.h"

namespace wkg {

#ifdef USE_SERIALIZATION
	class Stream;
#	define SERIALIZABLE				\
		bool Load(Stream*);			\
		bool Save(Stream*) const;
#else
#	define SERIALIZABLE
#endif

}

#ifdef DIRECT3D_VERSION
#	include "wkgd3dwrap.h"
#endif

#define WKG_ZENABLE D3DZB_TRUE