/******************************************************************************
 wkgdebug.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#undef ASSERT

#ifdef _DEBUG
#	define ASSERT(t)		if (!(t)) __asm { int 3 }
#	define DBUG(line)		line
#	define DBPRINT(args)	dbprint##args
#	define DBPRINTLN(args)	dbprintln##args
#	define DEBUGMSG dbprintln
	extern void dbprint(const char*, ...);
	extern void dbprintln(const char*, ...);
#else
#	define ASSERT(t)
#	define DBUG(line)
#	define DBPRINT(args)
#	define DBPRINTLN(args)
#	define DEBUGMSG
#endif
