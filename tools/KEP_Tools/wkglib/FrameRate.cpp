/******************************************************************************
 FrameRate.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "wkgtypes.h"
#include "FrameRate.h"
#include "Realtime.h"

namespace wkg {

FrameRate::FrameRate()
{
	mFrameCount = 0;
	mElapsed = 0;
	mFPS = 0.0;
}

void 
FrameRate::Update()
{
	++mFrameCount;

	mElapsed += Realtime::Instance()->GetElapsed();

	if (mElapsed >= 500)
	{
		mFPS = real(mFrameCount) * 1000.0f / real(mElapsed);
		mFrameCount = 0;
		mElapsed = 0;
	}
}

}