/******************************************************************************
 InstanceFactory.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include <vector>
#include "factoryrelease.h"

#ifdef _DEBUG
#   include "typeinfo.h"
#endif

namespace wkg
{

// each piece of data in the factory is an instance, as
// opposed to the Factory class, where each item represents a
// type of instance (like the MeshFactory or TextureFactory
// The InstanceFactory mainly serves as a place to keep track
// of memory allocated for its instances

template <typename T> class InstanceFactory : public FactoryRelease<T>
{
    public:
        InstanceFactory();
        virtual ~InstanceFactory();

		void Add(T* pT);

        virtual void Release(const T *data);

    protected:
        typedef std::vector<T*> NodeVector;
        typedef NodeVector::iterator NodeIterator;

        T *Get(NodeIterator itor);
        void Release(NodeIterator itor);

        NodeIterator FindNode(const T *data) const;

    protected:
        mutable NodeVector m_nodes;

};

template <typename T> 
InstanceFactory<T>::InstanceFactory()
{
    // our node vector is already initialized with no elements in it
}

template <typename T> 
InstanceFactory<T>::~InstanceFactory()
{
	// release all our references - since the order of deletion
	// between instances may be important, we just Release each
	// obj, and if any extra references are kept around,
	// they have to be released themselves, so everything
	// is deleted in the proper order
    NodeIterator itor;
    for(itor = m_nodes.begin(); itor < m_nodes.end(); itor++)
    {
        (*itor)->Release();
    }
}


template <typename T>
void InstanceFactory<T>::Release(InstanceFactory<T>::NodeIterator itor)
{
	// the actual deletion of the object is the resposibility of the
	//  user or the object
    if(itor != m_nodes.end())
    {
		// do not delete because deletion is handled in Release method 
		// of RefCounter object
        m_nodes.erase(itor);
    }
    else
    {
#ifdef _DEBUG
        const type_info &t = typeid(T);

        DEBUGMSG("class Factory<%s>: Attempt to release object already released!", t.name());
#endif
    }
}


template <typename T> 
void InstanceFactory<T>::Release(const T *data)
{
	ASSERT(data);
    Release(FindNode(data));
}


template <typename T>
InstanceFactory<T>::NodeIterator InstanceFactory<T>::FindNode(const T * const data) const
{
    NodeIterator itor;
    for(itor = m_nodes.begin(); itor != m_nodes.end(); itor++)
    {
        if((*itor) == data) return itor;
    }

    return itor;
}


template <typename T>
void InstanceFactory<T>::Add(T *pT)
{
	// assumes pointer passed in is properly incremented in ref count
	pT->SetFactoryRelease(this);

#ifdef _DEBUG
    NodeIterator itor;
    for(itor = m_nodes.begin(); itor != m_nodes.end(); itor++)
    {
        if((*itor) == pT) ASSERT(0);
    }
#endif

    m_nodes.push_back(pT);
}

}
