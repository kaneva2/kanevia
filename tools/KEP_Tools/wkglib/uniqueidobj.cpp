/******************************************************************************
 uniqueidobj.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "uniqueidobj.h"

namespace wkg
{

uint32 UniqueIDObj::m_curr_id = 1;

UniqueIDObj::UniqueIDObj()
{
	m_id = m_curr_id;
	m_curr_id++;
}

}

