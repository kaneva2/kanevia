/******************************************************************************
 Units.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "Vector3.h"

namespace wkg 
{

class Units
{
	protected:
		static const real UNITS_TO_FOOT;	// length
		static const real UNITS_TO_GRAM;	// mass
		static const real UNITS_TO_NEWTON;	// force
	
	public:
//
// --------------------------------- Length ------------------------------------
//
		static real ToMeters(real Units);
		static real ToFeet(real Units);

		static real FromMeters(real Meters);	// to Units
		static real FromFeet(real Feet);		// to Units

		static real MetersToFeet(real Meters);
		static real FeetToMeters(real Feet);

		static Vector3 ToMeters(const Vector3& Units);
		static Vector3 ToFeet(const Vector3& Units);
		static Vector3 FromMeters(const Vector3& Meters);
		static Vector3 FromFeet(const Vector3& Feet);
//
// ---------------------------------- Mass -------------------------------------
//
		static real ToGrams(real Units);
		static real ToKGrams(real Units);
		static real ToSlugs(real Units);

		static real FromGrams(real Grams);		// to Units
		static real FromKGrams(real Kilograms);	// to Units
		static real FromSlugs(real Slugs);		// to Units

		static real GramsToSlugs(real Grams);
		static real SlugsToGrams(real Slugs);
//
// ---------------------------------- Force -------------------------------------
//
		static real ToNewtons(real Units);
		static real ToPounds(real Units);

		static real FromNewtons(real Newtons);	// to Units
		static real FromPounds(real Pounds);	// to Units

		static real NewtonsToPounds(real Newtons);
		static real PoundsToNewtons(real Pounds);
};

//
// --------------------------------- Length ------------------------------------
//
inline real 
Units::FeetToMeters(real Feet)
{
	return Feet * 0.3048f;
}

inline real
Units::MetersToFeet(real Meters)
{
	return Meters / 0.3048f;
}

inline real
Units::FromFeet(real Feet)
{
	return Feet * UNITS_TO_FOOT;
}

inline real
Units::ToFeet(real Units)
{
	return Units / UNITS_TO_FOOT;
}

inline real
Units::FromMeters(real Meters)
{
	return FromFeet(MetersToFeet(Meters));
}

inline real
Units::ToMeters(real Units)
{
	return FeetToMeters(ToFeet(Units)); 
}

inline Vector3 
Units::ToMeters(const Vector3& Units)
{
	return Vector3(ToMeters(Units.x), ToMeters(Units.y), ToMeters(Units.z));
}

inline Vector3 
Units::ToFeet(const Vector3& Units)
{
	return Vector3(ToFeet(Units.x), ToFeet(Units.y), ToFeet(Units.z));
}

inline Vector3 
Units::FromMeters(const Vector3& Meters)
{
	return Vector3(FromMeters(Meters.x), FromMeters(Meters.y), FromMeters(Meters.z));
}

inline Vector3 
Units::FromFeet(const Vector3& Feet)
{
	return Vector3(FromFeet(Feet.x), FromFeet(Feet.y), FromFeet(Feet.z));
}
//
// ---------------------------------- Mass -------------------------------------
//
inline real 
Units::GramsToSlugs(real Grams)
{
	return Grams / 14590.0f;
}

inline real 
Units::SlugsToGrams(real Slugs)
{
	return Slugs * 14590.0f;
}


inline real 
Units::ToGrams(real Units)
{
	return Units / UNITS_TO_GRAM;
}

inline real 
Units::FromGrams(real Grams)
{
	return Grams * UNITS_TO_GRAM;
}

inline real 
Units::ToKGrams(real Units)
{
	return ToGrams(Units) * 0.001f;
}

inline real 
Units::FromKGrams(real Kilograms)
{
	return FromGrams(Kilograms * 1000.0f);
}

inline real 
Units::ToSlugs(real Units)
{
	return GramsToSlugs(ToGrams(Units));
}

inline real 
Units::FromSlugs(real Slugs)
{
	return FromGrams(SlugsToGrams(Slugs));
}
//
// ---------------------------------- Force -------------------------------------
//
inline real 
Units::NewtonsToPounds(real Newtons)
{
	return Newtons * 0.2248f;
}

inline real 
Units::PoundsToNewtons(real Pounds)
{
	return Pounds * 4.448f;
}

inline real 
Units::ToNewtons(real Units)
{
	return Units / UNITS_TO_NEWTON;
}

inline real 
Units::FromNewtons(real Newtons)
{
	return Newtons * UNITS_TO_NEWTON;
}

inline real 
Units::ToPounds(real Units)
{
	return NewtonsToPounds(ToNewtons(Units));
}

inline real 
Units::FromPounds(real Pounds)
{
	return FromNewtons(PoundsToNewtons(Pounds));
}

}
