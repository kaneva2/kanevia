/******************************************************************************
 NameValueSet.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include <vector>

namespace wkg 
{

class NameValueSet
{
	public:
		NameValueSet();
		~NameValueSet();

		// parses a given buffer of name value sets of the form:
		//  name=value\n
		bool Parse(const char* nameValSrc);

		// number of name/value pairs
		int32 GetCount() const;	
		bool Get(int32 which, const char** name, const char** value) const;

	protected:
		void ParsePairs();

	protected:
		struct nv { char* name; char* value; };
		typedef std::vector<nv> NVList;

		char*	src;
		NVList	nvList;
};

inline int32
NameValueSet::GetCount() const
{
	return (uint32)nvList.size();
}

inline bool 
NameValueSet::Get(int32 which, const char** name, const char** value) const
{
	if (which <= GetCount())
	{
		*name = nvList[which].name;
		*value = nvList[which].value;
		return true;
	}
	else
		return false;
}

}