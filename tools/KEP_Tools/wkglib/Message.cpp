/******************************************************************************
 Message.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include <string>
#include "Message.h"

namespace wkg
{

Message::Message(std::string name, int32 id_from, int32 id_to) : 
	m_name(name),
	m_id_from(id_from),
	m_id_to(id_to)
{
}

Message::~Message()
{

}

}