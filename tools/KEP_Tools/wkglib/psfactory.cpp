/******************************************************************************
 psfactory.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"

#include "psfactory.h"
#include "pixelshader.h"

namespace wkg
{

namespace engine
{

PSFactory::PSFactory()
{
}

PSFactory::~PSFactory()
{
	ShaderIterator itor = m_shaders.begin();
	ShaderIterator end = m_shaders.end();
	for(; itor < end; itor++)
	{
		delete itor->m_shader;
	}

	m_shaders.clear();
}

void 
PSFactory::RegisterShader(PixelShader *vs, std::string name)
{
	ShaderEntry entry;
	entry.m_name = name;
	entry.m_shader = vs;
	m_shaders.push_back(entry);
}

PixelShader *
PSFactory::GetShader(std::string name)
{
	PixelShader *shader = (PixelShader *)0;

	ShaderIterator itor = m_shaders.begin();
	ShaderIterator end = m_shaders.end();
	for(; itor < end; itor++)
	{
		if(itor->m_name == name)
		{
			shader = itor->m_shader; 
			break;
		}
	}

	ASSERT(shader);
	return shader;
}

}

}

