/******************************************************************************
 wkgconst.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#define WKG_PI				(static_cast<real>(3.14159265359))
#define WKG_PIOVER2			(static_cast<real>(1.570796326794895))

#ifdef SINGLEPRECISION
#	define MAXREAL			1.0e38f
#	define TOLERANCEf		0.00001f
#else
#	define MAXREAL			1.0e308
#	define TOLERANCEf		0.000001
#endif
