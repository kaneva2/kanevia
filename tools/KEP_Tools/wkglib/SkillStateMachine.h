/******************************************************************************
 SkillStateMachine.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include "StateMachine.h"

namespace wkg
{

using namespace state_machine;

namespace game
{

class Actor;

class SkillStateMachine : public state_machine::StateMachine
{
public:
	SkillStateMachine(game::Actor *owner);
	virtual ~SkillStateMachine();

	// why does this have to be here?
	virtual bool Load(const char *filename) { return StateMachine::Load(filename); }

protected:
	virtual bool Load(XMLDocument *doc);
	void GetStates(XMLNode *curNode);

protected:
	game::Actor *m_actor;

};

}

}

