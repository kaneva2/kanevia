/******************************************************************************
 MessageQueue.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

// A message queue.
//  There, generally, should be only one of these in the world used to 
//  send and receive messages between game objects.  Messengers automatically
//  register themselves with this queue, so the queue has to be constructed
//  before the messengers.  

#pragma once

#include "singleton.h"
#include <map>
#include <queue>

namespace wkg
{

class Messenger;
class Message;
class MessageSubscription;

class MessageQueue : public Singleton<MessageQueue>
{
public:
	MessageQueue();
	virtual ~MessageQueue();

	// automatically called if you inherit from messenger
	void RegisterMessenger(int32 id, Messenger* new_messenger);

	// put a message in the queue to be processed.  it's the
	//  eventual recipient's responsibility to free the message
	void QueueMessage(Message* new_msg);

	// called once per frame from the main game loop to dispatch the queued
	//  messages
	void ProcessMessages();

	// subscribes a messenger with the given id to all generic messages from
	//  the given from id.  if a messenger sends a message to id -1, then
	//  all those that subscribed to that messenger 
	void AddSubscription(int32 to, int32 from);

protected:
	std::map<int32, Messenger *>			m_id_map;
	std::queue<Message *>					m_queue;
	std::vector<MessageSubscription*>		m_subscriptions;

};

}