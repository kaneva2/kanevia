/******************************************************************************
 DynIVB.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "wkgtypes.h"
#include "DynIVB.h"

namespace wkg {
namespace engine {

DynIVB::DynIVB()
{
	pIB = 0;
	istartByte = ibNext = 0;
	piBatch = 0;
}

DynIVB::~DynIVB()
{
	ASSERT(0 == piBatch);	// make sure the index buf isn't still locked

	// do nothing... we don't own the index buf
}

bool 
DynIVB::Prepare(LPDIRECT3DVERTEXBUFFER9 pVB, uint8 vertSize, LPDIRECT3DINDEXBUFFER9 pIB, int32 numBatches)
{
	D3DVERTEXBUFFER_DESC vbdesc;
	D3DINDEXBUFFER_DESC ibdesc;

	ASSERT(!this->pVB && !pBatch && !this->vertSize && !maxVerts && !maxBatches && !vertsPerBatch && !startByte && !vbNext);
	ASSERT(!this->pIB && !piBatch);

	if (pVB && pIB && 
		D3D_OK == pVB->GetDesc(&vbdesc) && vbdesc.Usage & D3DUSAGE_DYNAMIC && vbdesc.Usage & D3DUSAGE_WRITEONLY &&
		D3D_OK == pIB->GetDesc(&ibdesc) && ibdesc.Usage & D3DUSAGE_DYNAMIC && ibdesc.Usage & D3DUSAGE_WRITEONLY && ibdesc.Format == D3DFMT_INDEX16)
	{
		this->pVB = pVB;
		this->pIB = pIB;
		this->vertSize = vertSize;
		maxVerts = MIN(vbdesc.Size / vertSize, ibdesc.Size / sizeof(uint16));
		maxBatches = numBatches;
		vertsPerBatch = maxVerts / maxBatches;
		ASSERT(vertsPerBatch >= 3);
		
		if (vertsPerBatch > 0xffff)		// uint16 can't address more than 64k
			vertsPerBatch = 0xffff;

		startByte = maxVerts;	// force lock at beginning of vb
		vbNext = vertsPerBatch;	// ... no room for more
		pBatch = 0;				// ... none queued
		istartByte = ibdesc.Size / sizeof(uint16);
		ibNext = (uint16)vertsPerBatch;
		piBatch = 0;

		return true;
	}
	else
	{
		pVB = 0;
		pIB = 0;
		vertSize = 0;
		maxVerts = maxBatches = vertsPerBatch = startByte = vbNext = 0;
		pBatch = 0;
		istartByte = ibNext = 0;
		piBatch = 0;

		return false;
	}
}

bool
DynIVB::CanAppend(int32 numVerts, uint16 numIndices, uint8** pVert, uint16** pIVert, uint16* pINext)
{
	ASSERT(numIndices >= numVerts);

	if (ibNext + numIndices >= vertsPerBatch)
	{
		uint32 lockFlags;

		if (pBatch)
		{
			ASSERT(piBatch);
			*pVert = 0;
			*pIVert = 0;
			*pINext = 0;

			return false;
		}

		startByte += vertsPerBatch;		// move past the entire batch (regardless of whether it was fully used) to simplify locking
		vbNext = 0;						// (i.e. lock only on batch boundaries)
		istartByte += (uint16)vertsPerBatch;
		ibNext = 0;

		if (istartByte + vertsPerBatch <= maxVerts)
			lockFlags = D3DLOCK_NOOVERWRITE;			// next batch fits within this vert buf
		else
		{
			startByte = 0;								// entire vert buf is full...get a new one and lock at the beginning
			istartByte = 0;
			lockFlags = D3DLOCK_DISCARD;
		}

		if (D3D_OK == pVB->Lock(startByte * vertSize, vertsPerBatch * vertSize, (void**)&pBatch, lockFlags))
		{
			if (D3D_OK == pIB->Lock(istartByte * sizeof(uint16), vertsPerBatch * sizeof(uint16), (void**)&piBatch, lockFlags))
			{
				*pVert = pBatch;
				*pIVert = piBatch;
				*pINext = (uint16)vbNext;
				vbNext += numVerts;
				ibNext += numIndices;

				return true;
			}
			else
				pVB->Unlock();
		}

		// d3d problem...probably lost device; try to start new vert buf/index buf on next attempt
		//
		startByte = maxVerts;	// force lock at beginning of vb
		vbNext = vertsPerBatch;	// ... no room for more
		pBatch = 0;				// ... none queued
		istartByte = (uint16)vertsPerBatch;
		ibNext = (uint16)vertsPerBatch;
		piBatch = 0;
		*pVert = 0;
		*pIVert = 0;
		*pINext = 0;

		return false;
	}
	else
	{
		*pVert = pBatch + vbNext * vertSize;
		*pIVert = piBatch + ibNext;
		*pINext = (uint16)vbNext;
		vbNext += numVerts;
		ibNext += numIndices;

		return true;
	}
}

bool 
DynIVB::BatchReady(int32* pStartVert, int32* pNumVerts, uint16* pStartIdx, uint16* pNumIdx)
{
	if (pBatch && piBatch)
	{
		pVB->Unlock();
		pIB->Unlock();
		*pStartVert = startByte;
		*pNumVerts = vbNext;
		*pStartIdx = istartByte;
		*pNumIdx = ibNext;

		vbNext = vertsPerBatch;		// move to next batch
		pBatch = 0;					// signal that we've already sent the last batch
		ibNext = (uint16)vertsPerBatch;
		piBatch = 0;

		return true;
	}

	*pStartVert = 0;
	*pNumVerts = 0;
	*pStartIdx = 0;
	*pNumIdx = 0;

	return false;
}


}}