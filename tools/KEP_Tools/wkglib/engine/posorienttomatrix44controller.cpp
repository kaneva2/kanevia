/******************************************************************************
 posorienttomatrix44controller.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "matrix44.h"
#include "posorienttomatrix44controller.h"

namespace wkg
{

namespace engine
{

PosOrientToMatrix44Controller::PosOrientToMatrix44Controller() :
	Controllable(), Controller(),
	m_position(0.0f, 0.0f, 0.0f),
	m_orientation(0.0f, 0.0f, 0.0f, 1.0f),
	m_matrix(0)
{
}

PosOrientToMatrix44Controller::~PosOrientToMatrix44Controller()
{
}

void 
PosOrientToMatrix44Controller::ControllableUpdate()
{
	if(!m_matrix) return;
	// update our matrix based on our new position and orientation

	Matrix44 translation(Matrix44::IDENTITY);
	translation.SetTranslation(m_position);

	Matrix44 orientation = (Matrix44)m_orientation;

	(*m_matrix) = orientation * translation;

	ControllerUpdate();
}

}

}

