/******************************************************************************
 DynIVB.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "DynVB.h"

namespace wkg 
{

namespace engine 
{

class DynIVB : public DynVB
{
	public:
		DynIVB();
		~DynIVB();

		LPDIRECT3DINDEXBUFFER9 GetIB();

		bool Prepare(LPDIRECT3DVERTEXBUFFER9 pVB, uint8 vertSize, LPDIRECT3DINDEXBUFFER9 pIB, int32 numBatches = 4);

		bool CanAppend(int32 numVerts, uint16 numIndices, uint8** ppVert, uint16** ppIVert, uint16* pINext);		// returns false if no room currently available (i.e. needs BatchReady())
		bool BatchReady(int32* pStartVert, int32* pNumVerts, uint16* pStartIdx, uint16* pNumIdx);

	protected:
		LPDIRECT3DINDEXBUFFER9 pIB;
		uint16 istartByte, ibNext;
		uint16* piBatch;
};

inline LPDIRECT3DINDEXBUFFER9
DynIVB::GetIB()
{
	return pIB;
}

}

}
