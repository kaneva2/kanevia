/******************************************************************************
 AreaTriggerBox2D.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include "AreaTrigger.h"

namespace wkg 
{
class Vector3;

namespace engine
{

class AreaTriggerBox2D : public AreaTrigger
{
public:
	AreaTriggerBox2D(std::string name, int32 waitingFor);
	AreaTriggerBox2D(std::string name, int32 waitingFor, real size, Vector3& v);
	virtual ~AreaTriggerBox2D();

	real GetSize()					{	return m_size;	}
	void SetSize(real size)			{	m_size = size;	}
	Vector3 GetPosition()				{	return m_position;	}
	void SetPosition(Vector3& v)		{	m_position = v;		}

	void OnCollide(Collidable *other, Collision *c)	{}
	virtual void OnUpdate(uint32 ms);

	virtual WorldObject* CollisionDetected();

protected:
	real		m_size;
	Vector3		m_position;

};

}

}