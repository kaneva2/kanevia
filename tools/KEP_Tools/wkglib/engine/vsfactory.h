/******************************************************************************
 vsfactory.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#ifndef _VS_FACTORY_H_
#define _VS_FACTORY_H_

#include <string>
#include <vector>
#include "singleton.h"

namespace wkg
{

namespace engine
{

class VertexShader;

class VSFactory : public Singleton<VSFactory>
{
public:
	VSFactory();
	virtual ~VSFactory();

	void RegisterShader(VertexShader *vs, std::string name);
	VertexShader *GetShader(std::string name);

protected:
	class ShaderEntry
	{
		public:
			std::string m_name;
			VertexShader *m_shader;
	};

	typedef std::vector<ShaderEntry> ShaderVector;
	typedef ShaderVector::iterator ShaderIterator;

	ShaderVector m_shaders;	
};

}

}

#endif

