/******************************************************************************
 wkgindex.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

namespace wkg
{

typedef uint16 WKGIndex16;
typedef uint32 WKGIndex32;

#ifdef USE_32BIT_INDICES
#define WKGIndex WKGIndex32
#else
#define WKGIndex WKGIndex16
#endif


}