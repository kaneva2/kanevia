/******************************************************************************
 wkbfile.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

// White Knuckle Binary file wrapper

#define WKB_OLD_VERSION (131)
#define WKB_VERSION (132)

#include "meshopts.h"

namespace wkg
{

class WKGWeightedVertex;
class WKGFace;
class WKGMaterial;
class WKGMaterial131;
class WKGBone;
class WKGKeyframe;
class WKGMeshSection;
class WKGColor;
class WKGVertexColor;

class WKBHeaderLayout
{
public:
	uint32 m_type;
	uint32 m_version;

	uint8  m_flags;

	uint32 m_num_verts;
	uint32 m_vert_offset;
	uint32 m_vert_color_offset;

	uint32 m_num_faces;
	uint32 m_face_offset;

	uint32 m_num_sections;			// total number of sections (solid followed by transparent)
	uint32 m_num_solid_sections;	// #transparent = m_num_sections - m_num_solid_sections
	uint32 m_section_offset;

	uint32 m_num_materials;
	uint32 m_material_offset;

	uint32 m_num_bones;
	uint32 m_bone_offset;
};

class WKBFileLayout
{
public:
	WKBHeaderLayout m_header;
	uint8 *m_data;
};

class WKBFile
{
public:
	WKBFile();
	~WKBFile();

	void SetSizes(
		uint32 num_verts,
		uint32 num_faces, 
		uint32 num_sections,
		uint32 num_solid_sections,
		uint32 num_mats, 
		uint32 num_bones = 0, 
		bool reset = true);

	// utility functions
	void FlipHands();
	void CalcNormals();

	void PartitionSections(real max_size);

	void SetVerts(WKGWeightedVertex *verts);
	void SetVertColors(WKGVertexColor *colors);
	void SetFaces(WKGFace *face);
	void SetSections(WKGMeshSection *sections);
	void SetMaterials(WKGMaterial *mats);
	void SetBones(WKGBone *bones);

	uint32 GetNumVerts() { return m_file_layout->m_header.m_num_verts; }
	uint32 GetNumFaces() { return m_file_layout->m_header.m_num_faces; }
	uint32 GetNumSections() { return m_file_layout->m_header.m_num_sections; }
	uint32 GetNumSolidSections() { return m_file_layout->m_header.m_num_solid_sections; }
	uint32 GetNumTransparentSections() { return GetNumSections() - GetNumSolidSections(); }
	uint32 GetNumBones() { return m_file_layout->m_header.m_num_bones; }
	uint32 GetNumMaterials() { return m_file_layout->m_header.m_num_materials; }

	void FindMinSphere(Vector3 &center, real &radius);
	void FindAABB(Vector3 &min, Vector3 &max);

	WKGWeightedVertex *GetVerts()
	{
		if(GetNumVerts())
			return (WKGWeightedVertex *)(((uint8 *)m_file_layout) + m_file_layout->m_header.m_vert_offset);
		else
			return 0;
	}

	WKGVertexColor *GetVertColors()
	{
		if(GetNumVerts())
			return (WKGVertexColor *)(((uint8 *)m_file_layout) + m_file_layout->m_header.m_vert_color_offset);
		else
			return 0;
	}

	WKGFace *GetFaces()
	{
		if(GetNumFaces())
			return (WKGFace *)(((uint8 *)m_file_layout) + m_file_layout->m_header.m_face_offset);
		else
			return 0;
	}

	WKGMeshSection *GetSections()
	{
		if(GetNumSections())
			return (WKGMeshSection *)(((uint8 *)m_file_layout) + m_file_layout->m_header.m_section_offset);
		else
			return 0;
	}

	WKGBone *GetBones()
	{
		if(GetNumBones())
			return (WKGBone *)(((uint8 *)m_file_layout) + m_file_layout->m_header.m_bone_offset);
		else
			return 0;
	}

	WKGMaterial *GetMaterials();

	bool Load(const char *filename);
	bool Save(const char *filename);

	void FindAABBForSection(int32 section, Vector3 &min, Vector3 &max);

protected:
	bool Reset();
	void PartitionSection(real max_size, int32 i, std::vector<WKGMeshSection> &resultant);

protected:
	int32 m_file_size;
	WKBFileLayout *m_file_layout;

	int32 m_file_version;

	WKGMaterial *m_new_mats;

};

}
