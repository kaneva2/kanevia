/******************************************************************************
 wkafile.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/


#pragma once

// White Knuckle Animation file wrapper

#define WKA_VERSION (120)

namespace wkg
{

class WKGKeyframe;
class WKGBoneWeight;

class WKAHeaderLayout
{
public:
	uint32 m_type;
	uint32 m_version;

	uint8  m_flags;

	uint32 m_num_keyframes;
	uint32 m_keyframe_offset;

	uint32 m_num_bone_weights;
	uint32 m_bone_weight_offset;
};

class WKAFileLayout
{
public:
	WKAHeaderLayout m_header;
	uint8 *m_data;
};

class WKAFile
{
public:
	WKAFile();
	~WKAFile();

	void SetSizes(uint32 num_keyframes, uint32 num_bone_weights);
	void SetKeyframes(WKGKeyframe *keyframes);
	void SetBoneWeights(WKGBoneWeight *bone_weights);

	uint32 GetNumKeyframes() { return m_file_layout->m_header.m_num_keyframes; }
	uint32 GetNumBoneWeights() { return m_file_layout->m_header.m_num_bone_weights; }

	void FlipHands();

	WKGKeyframe *GetKeyframes()
	{
		if(GetNumKeyframes())
			return (WKGKeyframe *)(((uint8 *)m_file_layout) + m_file_layout->m_header.m_keyframe_offset);
		else 
			return 0;
	}

	WKGBoneWeight *GetBoneWeights()
	{
		if(GetNumBoneWeights())
			return (WKGBoneWeight *)(((uint8 *)m_file_layout) + m_file_layout->m_header.m_bone_weight_offset);
		else
			return 0;
	}

	bool Load(const char *filename);
	bool Save(const char *filename);

protected:
	bool Reset();

protected:
	int32 m_file_size;
	WKAFileLayout *m_file_layout;

};

}
