/******************************************************************************
 boundingvolume.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

namespace wkg
{

namespace engine
{

class WorldObject;

class Collision;

class BoundingVolume
{
public:
	BoundingVolume() : m_world_object(0) {}
	BoundingVolume(WorldObject *wo) : m_world_object(wo) {}
	virtual ~BoundingVolume() {} 

	virtual Collision *Collide(BoundingVolume *other, uint32 ms) = 0;
	WorldObject *GetWorldObject() { return m_world_object; }

	virtual real GetBVSphereRadius() { return 0.0f; }

protected:
	WorldObject *m_world_object;

};

}

}

