/******************************************************************************
 rendertotexture.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "device.h"
#include "rendertotexture.h"

namespace wkg
{

namespace engine
{

RenderToTexture::RenderToTexture(int32 width, int32 height) :
	m_old_rt(0), m_old_zbuf(0)
{
	HRESULT hr;
	if(0 == width || 0 == height)
	{
		IDirect3DSurface9 *rt;
		HRESULT hr = Device::Instance()->GetRenderTarget(0, &rt);
		ASSERT(SUCCEEDED(hr) && rt);

		D3DSURFACE_DESC desc;

		rt->GetDesc(&desc);
		width = desc.Width;
		height = desc.Height;

		rt->Release();
	}

	// create our texture
	hr = Device::Instance()->CreateTexture(
		width, height, 1, D3DUSAGE_RENDERTARGET, D3DFMT_A8R8G8B8, D3DPOOL_DEFAULT, &m_texture, 0);

	ASSERT(SUCCEEDED(hr));
}

RenderToTexture::~RenderToTexture()
{
	if(m_texture)
		m_texture->Release();
	m_texture = 0;
}

void 
RenderToTexture::Prepare()
{
	HRESULT hr;

	hr = Device::Instance()->GetDepthStencilSurface(&m_old_zbuf);
	ASSERT(SUCCEEDED(hr) && m_old_zbuf);

	hr = Device::Instance()->GetRenderTarget(0, &m_old_rt);
	ASSERT(SUCCEEDED(hr) && m_old_rt);

	IDirect3DSurface9 *new_rt;
	hr = m_texture->GetSurfaceLevel(0, &new_rt);
	ASSERT(SUCCEEDED(hr) && new_rt);

	hr = Device::Instance()->SetRenderTarget(0, new_rt);
	hr = Device::Instance()->SetDepthStencilSurface(m_old_zbuf);
	ASSERT(SUCCEEDED(hr));
}

void
RenderToTexture::Restore()
{
	HRESULT hr;

	ASSERT(m_old_zbuf && m_old_rt);

	hr = Device::Instance()->SetRenderTarget(0, m_old_rt);
	hr = Device::Instance()->SetDepthStencilSurface(m_old_zbuf);

	m_old_rt->Release();
	m_old_zbuf->Release();
}

}

}

