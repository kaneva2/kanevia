/******************************************************************************
 orientationkeyframecontroller.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include "vector3.h"
#include "controllable.h"
#include "controller.h"
#include "wkgkeyframe.h"

namespace wkg
{

namespace engine
{

class OrientationKeyframeController : public Controllable, public Controller
{
public:
	OrientationKeyframeController();
	virtual ~OrientationKeyframeController();

	virtual void ControllableUpdate();
	void SetTarget(Quaternion *q)
	{
		m_orientation = q;
	}

	void BeginTransition() 
	{ 
		if(m_orientation)
			m_begin_transition = *m_orientation; 
	}

	void AddOrientationKeyframe(WKGOrientationKeyframe *rotkf)
	{
		m_orientation_keyframes.push_back(rotkf);
	}

	// public to be controlled by the keyframe controller
	int32 m_frame;
	int32 m_next_frame;
	real m_partial_frame;

protected:
	WKGOrientationKeyframeVector m_orientation_keyframes;

	Quaternion *m_orientation;
	Quaternion m_begin_transition;
};

}

}