/******************************************************************************
 skinnedeffectmeshrenderer.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "device.h"
#include "meshtypes.h"
#include "meshsection.h"
#include "skinnedmesh.h"
#include "skinnedmesheffect.h"
#include "skinnedeffectmeshrenderer.h"

namespace wkg 
{

namespace engine 
{

SkinnedEffectMeshRenderer::SkinnedEffectMeshRenderer(const char *fxfile) : EffectMeshRenderer()
{
	m_shader = new SkinnedMeshEffect();
	if(!fxfile)
		m_shader->Create("effects\\skinned.fx");
	else
		m_shader->Create(fxfile);

    D3DVERTEXELEMENT9 decl[] =
    {
        { 0,  0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION,		0 },
        { 0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL,		0 },
        { 0, 24, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD,		0 },
		{ 1,  0, D3DDECLTYPE_SHORT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_BLENDINDICES,	0 },
		{ 1,  4, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_BLENDWEIGHT,	0 },
        D3DDECL_END()
    };

	HRESULT hres = Device::Instance()->CreateVertexDeclaration(decl, &m_decl);
}

void 
SkinnedEffectMeshRenderer::RenderAllSections(Mesh *mesh)
{
	MeshSection* section;
	uint32 i;

	Device::Instance()->SetVertexDeclaration(m_decl);

	Device::Instance()->SetStreamSource(0, mesh->GetVB(), 0, sizeof(WKGVertex));
	SkinnedMesh *sm = dynamic_cast<SkinnedMesh *>(mesh);
	if(sm)
		Device::Instance()->SetStreamSource(1, sm->GetWeightVB(), 0, sizeof(WKGVertexWeights));
	else
		Device::Instance()->SetStreamSource(1, 0, 0, 0);

	WKGMaterial *meshMat;
	D3DMATERIAL9 mat;

	for (i = 0; i < mesh->GetNumSections(); i++)
	{
		section = mesh->GetSections()[i];
		if (section)
		{
			if(mesh->GetBumpTextures()[section->m_material]) continue;

			m_shader->PrepareSection(section);

			// set the texture
			ASSERT(section->m_material >= 0 && section->m_material < mesh->GetNumMaterials());
			meshMat = mesh->GetMaterials() + section->m_material;
			memcpy(&(mat.Diffuse), &(meshMat->diffuse), sizeof(WKGColor));
			memcpy(&(mat.Ambient), &(meshMat->ambient), sizeof(WKGColor));
			memcpy(&(mat.Specular), &(meshMat->specular), sizeof(WKGColor));
			memset(&(mat.Emissive), 0, sizeof(WKGColor));
			mat.Power = meshMat->power;
			Device::Instance()->SetMaterial(&mat);

			ASSERT(section->m_material < mesh->GetNumTextures());
			Device::Instance()->SetTexture(0, mesh->GetTextures()[section->m_material]);
			Device::Instance()->SetTexture(1, mesh->GetBumpTextures()[section->m_material]);

			section->Render();
		}
	}
}

}

}
