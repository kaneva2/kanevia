/******************************************************************************
 KeyFrameList.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#ifndef _KEYFRAMELIST_H_
#define _KEYFRAMELIST_H_

#include "keyframe.h"

namespace wkg {


class KeyFrameList
{
	public:
		KeyFrameList(int32 size);
		virtual ~KeyFrameList();

		int32 Size() const { return m_size; }
		KeyFrame*& operator[](int32 i) { return m_keys[i]; }

	protected:
		KeyFrame**		m_keys;
		int32			m_size;
};


// To use the KeyFrameList class, you specify the type of data stored
// in the keyframe class, not the keyframe type (ie, Vector4 and not
// KeyFrameTemplate<Vector4>
template <typename T> class KeyFrameListTemplate : public KeyFrameList
{
	public:
		KeyFrameListTemplate(int32 size);
		virtual ~KeyFrameListTemplate() {}

		void SetKeyFrame(int32 index, const T& data, int32 ms);
};


// KeyFrameList functions
inline KeyFrameList::KeyFrameList(int32 size)
:	m_size(size)
{
	ASSERT(m_size > 0);

	m_keys = new KeyFrame*[size];

	memzero(m_keys, m_size*sizeof(KeyFrame*));
}

inline KeyFrameList::~KeyFrameList()
{
	for (int32 i = 0; i < m_size; i++)
	{
		delete m_keys[i];
	}

	delete [] m_keys;
}



// KeyFrameListTemplate functions
template <typename T>
KeyFrameListTemplate<T>::KeyFrameListTemplate(int32 size)
:	KeyFrameList(size)
{
}


template <typename T>
void KeyFrameListTemplate<T>::SetKeyFrame(int32 index, const T& data, int32 ms)
{
	if (index < m_size)
	{
		delete m_keys[index];
		m_keys[index] = new KeyFrameTemplate<T>(data, ms);
	}
	else
	{
		ASSERT(!"KeyFrameListTemplate::SetKeyFrame - bad key frame index");
	}
}

}

#endif
