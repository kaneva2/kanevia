/******************************************************************************
 cameramanager.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include "singleton.h"

namespace wkg
{

class Camera;

namespace engine
{

class CameraManager : public Singleton<CameraManager>
{
public:
	CameraManager() : Singleton<CameraManager>()
	{
	}

	virtual ~CameraManager() {}

	void SetCamera(Camera *cam) { m_cam = cam; }
	Camera *GetCamera() { return m_cam; }

protected:
	Camera *m_cam;
};

}

}

