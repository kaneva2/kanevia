/******************************************************************************
 wkgcolor.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

namespace wkg
{

class WKGColor
{
public:
	WKGColor() {}

	WKGColor(float ir, float ig, float ib, float ia)
	{
		r = ir;
		g = ig;
		b = ib;
		a = ia;
	}

	const WKGColor &operator = (const WKGColor &other)
	{
		r = other.r;
		g = other.g;
		b = other.b;
		a = other.a;
	}

	float r, g, b, a;
};

class WKGVertexColor
{
public:
	WKGVertexColor() {}

	WKGVertexColor(float ir, float ig, float ib, float ia)
	{
		SetARGB(ia, ir, ig, ib);
	}

	void SetARGB(float a, float r, float g, float b)
	{
		argb = 
			(uint32(a * 255.0f + 0.5f) << 24) |
			(uint32(r * 255.0f + 0.5f) << 16) |
			(uint32(g * 255.0f + 0.5f) <<  8) |
			(uint32(b * 255.0f + 0.5f) <<  0);
	}

	uint32 argb;
};

}

