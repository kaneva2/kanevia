/******************************************************************************
 wkgboneweight.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

namespace wkg
{

class WKGBoneWeight
{
public:
	char m_name[255];
	int32 m_weight;
};

}
