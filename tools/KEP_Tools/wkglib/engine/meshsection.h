/******************************************************************************
 meshsection.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include <vector>

namespace wkg
{

namespace engine
{

class MeshSection
{
public:
	MeshSection();
	~MeshSection();

	void SetIndices(uint32 num_idxs, WKGIndex *i);

	void Render();

	LPDIRECT3DINDEXBUFFER9 GetIB() { return m_ib; }

	// currently, only tri *lists* supported
	uint32 GetNumTris() { return m_num_idxs / 3; }

	uint32 m_num_idxs;
	LPDIRECT3DINDEXBUFFER9 m_ib;
	int32 m_material;
	std::vector<int32> m_bones;

	uint32 m_min_vert;
	uint32 m_num_verts;

	void SetBounds(const Vector3 &min, const Vector3 &max)
	{
		m_min = min;
		m_max = max;
	}

	const Vector3 &GetMin() { return m_min; }
	const Vector3 &GetMax() { return m_max; }

protected:
	Vector3 m_min;
	Vector3 m_max;
};

}

}


