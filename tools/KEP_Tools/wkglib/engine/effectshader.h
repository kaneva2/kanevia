/******************************************************************************
 effectshader.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include <string>

namespace wkg
{

class WKGColor;
class Vector3;
class Vector4;

namespace engine
{

class Mesh;
class MeshSection;
class Texture;

class EffectShader
{
public:
	EffectShader();
	virtual ~EffectShader();

	virtual bool Create(const char *filename);
	virtual void Prepare(Mesh *mesh);
	virtual void PrepareSection(MeshSection *section);

	virtual int32 Begin();
	virtual void BeginPass(int32 pass);
	virtual void EndPass();
	virtual void End();
	virtual void CommitChanges();

	bool SetMatrix(const char *parameter, Matrix44 &m);
	bool SetVector(const char *parameter, Vector4 &v);
	bool SetVector(const char *parameter, Vector3 &v);
	bool SetFloat(const char *parameter,  real f);
	bool SetInt(const char *parameter,  uint32 dw);
	bool SetTexture(const char *parameter,  Texture *t);
	bool SetTexture(const char *parameter,  LPDIRECT3DTEXTURE9 tex);
	bool SetColor(const char *parameter, WKGColor &color);

	void OnLostDevice();
	void OnResetDevice();

protected:
	std::string m_filename;

	ID3DXEffect *m_effect;
	D3DXHANDLE m_technique;

};

}

}
