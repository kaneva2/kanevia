/******************************************************************************
 AreaTrigger.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "vector3.h"
#include "Message.h"
#include "MessageQueue.h"
#include "BVEllipsoid.h"
#include "AreaTrigger.h"

namespace wkg 
{

namespace engine
{

AreaTrigger::AreaTrigger(std::string name, int32 waitingFor) : 
	WorldObject(),
	m_name(name),
	m_waitingFor(waitingFor)
{
}

AreaTrigger::~AreaTrigger()
{

}

void
AreaTrigger::OnUpdate(uint32 ms)
{
	WorldObject* wo = CollisionDetected();
	if(wo)
	{
//		DEBUGMSG("Area Trigger tripped");
		Message* msg = new Message(m_name, this->GetUniqueID(), -1);
		MessageQueue::Instance()->QueueMessage(msg);
	}
}

}
}