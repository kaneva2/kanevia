/******************************************************************************
 skinnedmeshblureffect.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include "skinnedmesheffect.h"

namespace wkg
{

namespace engine
{

class SkinnedMeshBlurEffect : public SkinnedMeshEffect
{
public:
	SkinnedMeshBlurEffect();
	virtual ~SkinnedMeshBlurEffect();

	virtual void Prepare(Mesh *mesh);
	virtual void PrepareSection(MeshSection *section);
};

}

}

