/******************************************************************************
 vertex.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#ifndef _VERTEX_H_
#define _VERTEX_H_

#define FVF_CUSTOM_VERTEX (FVF_SIMPLE_VERTEX | D3DFVF_NORMAL)

#include "simplevertex.h"

namespace wkg
{

namespace engine
{

class Vertex : public SimpleVertex
{
	public:
		real nx, ny, nz;
};

}

}

#endif