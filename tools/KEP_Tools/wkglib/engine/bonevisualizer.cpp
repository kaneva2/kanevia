/******************************************************************************
 bonevisualizer.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "vector3.h"
#include "vector4.h"
#include "matrix44.h"
#include "meshfactory.h"
#include "mesh.h"
#include "meshrenderer.h"
#include "bonevisualizer.h"

namespace wkg
{

namespace engine
{

BoneVisualizer::BoneVisualizer() : MeshNode(), m_length(1.0f), m_mesh(0)
{
	MeshDescriptor msd;
	msd.type = MeshDescriptor::STANDARD_MESH;
	msd.filename = "bone.wbm";
	m_mesh = MeshFactory::Instance()->Get(msd);
	if(m_mesh)
	{
		SetMesh(m_mesh);
		SetRenderer(new MeshRenderer());
	}
	m_name = "bone visualizer";
}

BoneVisualizer::~BoneVisualizer()
{
	if(m_mesh)
		m_mesh->Release();
	m_mesh = 0;
}

void
BoneVisualizer::OnUpdate()
{
	Vector4 pos(0.0f, 0.0f, 0.0f, 1.0f);
	pos *= m_matrix;

	Matrix44 scale(Matrix44::IDENTITY);
	scale.SetScale(m_length, 0.5, 0.5f);

	SetMatrix(scale);
}

}

}

