/******************************************************************************
 vertexshader.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "device.h"
#include "vertexshader.h"
#include "filestream.h"

namespace wkg
{

namespace engine
{

VertexShader::VertexShader() : 
	m_handle(0), 
	m_function(0), 
	m_fvf(0),
	m_declaration(0), 
	m_is_fvf(false)
{
}

bool
VertexShader::Init(uint32 fvf)
{
	m_fvf = fvf;
	m_is_fvf = true;

	return true;
}

bool
VertexShader::Init(const char *filename, D3DVERTEXELEMENT9 *e)
{
	if(!e)
	{
		std::vector<D3DVERTEXELEMENT9> elements;
		
		D3DVERTEXELEMENT9 e;

		e.Stream = 0;
		e.Offset = 0;
		e.Type = D3DDECLTYPE_FLOAT3;
		e.Method = D3DDECLMETHOD_DEFAULT;
		e.Usage =  D3DDECLUSAGE_POSITION;
		e.UsageIndex = 0;
		elements.push_back(e);

		e.Stream = 0;
		e.Offset = 12;
		e.Type = D3DDECLTYPE_FLOAT3;
		e.Method = D3DDECLMETHOD_DEFAULT;
		e.Usage =  D3DDECLUSAGE_NORMAL;
		e.UsageIndex = 0;
		elements.push_back(e);

		e.Stream = 0;
		e.Offset = 24;
		e.Type = D3DDECLTYPE_FLOAT2;
		e.Method = D3DDECLMETHOD_DEFAULT;
		e.Usage =  D3DDECLUSAGE_TEXCOORD;
		e.UsageIndex = 0;
		elements.push_back(e);

		e.Stream = 0xff;
		e.Offset = 0;
		e.Type = 0;
		e.Method = 0;
		e.Usage =  0;
		e.UsageIndex = 0;
		elements.push_back(e);


		if (!CreateDeclaration(&(elements[0])))
			return false;
	}
	else
	{
		if (!CreateDeclaration(e))
			return false;
	}

	return LoadFunction(filename) && CreateShader();
}

VertexShader::~VertexShader()
{
	if(m_declaration)
	{
		m_declaration->Release();
		m_declaration = 0;
	}

	if(m_handle)
	{
		m_handle->Release();
		m_handle = 0;
	}

	delete [] m_function;
}

bool
VertexShader::CreateShader()
{
    HRESULT hres = Device::Instance()->CreateVertexShader(
        m_function, &m_handle);

	if(FAILED(hres)) 
	{
		ASSERT(0);
		return false;
	}
	else
		return true;
}

bool
VertexShader::LoadFunction(const char *filename)
{
	FileStream in;

	if(in.Open(filename, FileStream::ReadOnly))
	{
		uint32 len = in.GetFileSize();

		if(len > 0)
		{
			m_function = new uint32[(len + 3) / 4];
			if(m_function)
			{
				if(len != in.Read((uint8*)m_function, len))
				{
					delete m_function;
					m_function = 0;
				}
			}
		}

		in.Close();
	}

	return (m_function != 0);
}

bool
VertexShader::CreateDeclaration(D3DVERTEXELEMENT9 *e)
{
	HRESULT hres = Device::Instance()->CreateVertexDeclaration(e, &m_declaration);

	if(FAILED(hres))
	{
		ASSERT(0);
		return false;
	}

	return true;
}

bool 
VertexShader::Activate()
{
	if(m_is_fvf)
	{
		Device::Instance()->SetVertexShader(0);
		Device::Instance()->SetFVF(m_fvf);
	}
	else
	{
		Device::Instance()->SetVertexShader(m_handle);
		Device::Instance()->SetVertexDeclaration(m_declaration);
	}
	return true;
}

}

}
