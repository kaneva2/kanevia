/******************************************************************************
 TangentBasis.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#pragma once

#include "Vector3.h"

namespace wkg 
{

namespace engine 
{

class TangentBasis
{
	public:
		TangentBasis();
		~TangentBasis();

		bool BeginBuild(LPDIRECT3DVERTEXBUFFER9, uint32 nVerts, uint32 SrcStride, D3DPOOL = D3DPOOL_MANAGED);
		bool ComputeMeshSection(LPDIRECT3DINDEXBUFFER9, uint32 nTris);
		LPDIRECT3DVERTEXBUFFER9 EndBuild();

		struct Vert
		{
			Vector3 T;
			Vector3 B;
			Vector3 N;
		};

		struct SrcVert		// TangentBasis operates upon this type of vert (in the vert buffer passed to BeginBuild())
		{					//
			Vector3 pos;	// Note that the verts may actually be larger as long as this info
			Vector3 norm;	// is at the beginning of each vert and as long as the correct stride
			real tu, tv;	// is passed in to BeginBuild().
		};					//

	protected:
		void Reset();

	protected:
		LPDIRECT3DVERTEXBUFFER9 srcvb;
		LPDIRECT3DVERTEXBUFFER9 tbvb;
		uint32 numVerts;
		uint32 srcStride;
		uint32* numIncident;
};

}

}

