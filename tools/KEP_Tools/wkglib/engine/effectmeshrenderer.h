/******************************************************************************
 effectmeshrenderer.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include "meshrenderer.h"

namespace wkg 
{

namespace engine 
{

class EffectShader;

class EffectMeshRenderer : public MeshRenderer
{
public:
	EffectMeshRenderer(EffectShader *es = 0);
	virtual void Render(Mesh *mesh);

	virtual void OnLostDevice();
	virtual void OnResetDevice();

protected:
	virtual void RenderAllSections(Mesh *mesh);

protected:
	EffectShader *m_shader;
	IDirect3DVertexDeclaration9 *m_decl;

	Texture *m_phong_map;
	Texture *m_cube_map;

	Texture *m_half_angle_map;
	Texture *m_normal_angle_map;

	Texture *m_fresnel_map;
};

}

}
