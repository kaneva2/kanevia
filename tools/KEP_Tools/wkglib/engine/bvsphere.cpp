/******************************************************************************
 bvsphere.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "worldobject.h"
#include "intersect.h"
#include "collision.h"
#include "bvoctree.h"
#include "bvellipsoid.h"
#include "bvbox.h"
#include "bvsphere.h"

namespace wkg
{

namespace engine
{

BVSphere::BVSphere(WorldObject *wo, real radius) : 
	BoundingVolume(wo),
	m_radius(radius)
{
}

BVSphere::~BVSphere()
{
}

Collision *
BVSphere::Collide(BoundingVolume *other, uint32 ms)
{
	BVSphere *bvsphere;
	bvsphere = dynamic_cast<BVSphere *>(other);
	if(bvsphere)
	{
		Sphere our_sphere(
			m_world_object->GetPosition() + 
			m_world_object->GetBVOffset(), 
			m_radius);

		Sphere other_sphere(
			bvsphere->GetWorldObject()->GetPosition() + 
			bvsphere->GetWorldObject()->GetBVOffset(), 
			bvsphere->GetRadius());

		LineSegment seg;
		real radius;
		Vector3 poc, n;
		bool b;

		Vector3 src = m_world_object->GetPosition();
		Vector3 dst = src;

		seg.SetEndPoints(src, dst);

		radius = m_radius;

		Capsule c(seg, radius);

		b = Intersect::SphereCapsule(other_sphere, c, &poc, &n);
		
		if(b)
		{
			Collision *c = new Collision();
			c->m_dst = poc;
			// DWDTODO:
			c->m_poc = Vector3(0.0f, 0.0f, 0.0f);
			c->m_n = n;
			c->m_t = 1.0f;

			return c;
		}
		else
			return 0;
	}

	BVEllipsoid *bve = dynamic_cast<BVEllipsoid *>(other);
	if(bve)
	{
		Collision *c = other->Collide(this, ms);
		if(c) c->m_n = -c->m_n;
		return c;
	}

	BVBox *bvb = dynamic_cast<BVBox *>(other);
	if(bvb)
	{
		Collision *c = other->Collide(this, ms);
		if(c) c->m_n = -c->m_n;
		return c;
	}

	BVOctree *bvot = dynamic_cast<BVOctree *>(other);
	if(other)
	{
		Collision *c = other->Collide(this, ms);
		if(c) c->m_n = -c->m_n;
		return c;
	}

	DEBUGMSG("don't know how to collide provided bv types!");

	ASSERT(0);

	return false;
}

}

}

