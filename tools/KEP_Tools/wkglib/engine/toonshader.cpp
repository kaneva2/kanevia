/******************************************************************************
 toonshader.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "matrix44.h"
#include "vector4.h"
#include "device.h"
#include "toonshader.h"

namespace wkg
{

namespace engine
{

bool 
ToonShader::Create(const char *filename)
{
	bool res = EffectShader::Create(filename);

	HRESULT hr;

	hr = D3DXCreateTextureFromFile(
		Device::Instance()->GetD3DDevicePointer(), 
		"textures\\toon\\sil.tga", 
		&m_tex);

	if (FAILED(hr))
		return false;

	return res;
}

void 
ToonShader::Prepare(Mesh *mesh)
{
 	Device::Instance()->GetD3DDevicePointer()->SetTexture(1, m_tex);

	Matrix44 world, view, proj, wvp;

	Device::Instance()->GetTransform(D3DTS_WORLD, &world);
	Device::Instance()->GetTransform(D3DTS_VIEW, &view);
	Device::Instance()->GetTransform(D3DTS_PROJECTION, &proj);

	wvp = world * view * proj;
	wvp.Transpose();
	Device::Instance()->GetD3DDevicePointer()->SetVertexShaderConstant(0, &wvp,  4);

	Matrix44 world_inverse = world;
	world_inverse.Inverse();
	world_inverse.Transpose();
	Device::Instance()->GetD3DDevicePointer()->SetVertexShaderConstant(4, &world_inverse,  4);

	world.Transpose();
	Device::Instance()->GetD3DDevicePointer()->SetVertexShaderConstant(9, &world,  4);

	// Get the camera position
	Matrix44 view2world = view;
	view2world.Inverse();
	Vector4 cam(view2world._41, view2world._42, view2world._43, 1.0f);
//	Vector4 cam(0.0f, 0.0f, 0.0f, 1.0f);
	Device::Instance()->GetD3DDevicePointer()->SetVertexShaderConstant(8, &cam,  1);

}

}

}