/******************************************************************************
 weightedvs.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#include "wkgtypes.h"
#include "weightedvs.h"

namespace wkg
{

namespace engine
{

WeightedVS::WeightedVS() : VertexShader()
{
}

WeightedVS::~WeightedVS()
{
}

bool
WeightedVS::CreateDeclaration()
{
	m_declaration = new uint32[8];
	if (!m_declaration)
		return false;

	int32 i = 0;
	m_declaration[i++] = D3DVSD_STREAM(0);
	m_declaration[i++] = D3DVSD_REG(0, D3DVSDT_FLOAT3); // position
	m_declaration[i++] = D3DVSD_REG(1, D3DVSDT_FLOAT3); // normal
	m_declaration[i++] = D3DVSD_REG(2, D3DVSDT_FLOAT2); // texture coords
	m_declaration[i++] = D3DVSD_STREAM(1);
	m_declaration[i++] = D3DVSD_REG(3, D3DVSDT_SHORT2); // bone idxs
	m_declaration[i++] = D3DVSD_REG(4, D3DVSDT_FLOAT2); // weights
	m_declaration[i++] = D3DVSD_END();

	return true;
}

}

}