/******************************************************************************
 collision.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

namespace wkg
{

namespace engine
{

class Collision
{
public:
	Vector3 m_dst;
	Vector3 m_poc;
	Vector3 m_n;
	real m_t;
};

}

}

