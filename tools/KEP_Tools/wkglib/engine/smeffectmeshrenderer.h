/******************************************************************************
 smeffectmeshrenderer.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include "meshrenderer.h"

namespace wkg 
{

namespace engine 
{

class EffectShader;

class SMEffectMeshRenderer : public MeshRenderer
{
public:
	SMEffectMeshRenderer(EffectShader *es = 0);
	virtual void Render(Mesh *mesh);

protected:
	virtual void RenderAllSections(Mesh *mesh);

protected:
	EffectShader *m_shader;
	IDirect3DVertexDeclaration9 *m_decl;

	Texture *m_spot;
};

}

}
