/******************************************************************************
 Copy of octnode.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include "node.h"
#include "aabb.h"

namespace wkg
{

class Capsule;

namespace engine
{

class Mesh;

class OctNode : public Node, public AABB
{
public:
	OctNode();
	~OctNode();

	bool AddMesh(Mesh *mesh, const Matrix44 &xform);
	bool AddGeometry(std::vector<Vector3> &verts);

	virtual void DFSRender();
	virtual void DFSUpdate(Matrix44 &m);

	bool CollideSphere(Vector3 &dst, real r, Vector3 *n = 0, Vector3 *poc = 0);
	bool CollideSphereExhaustive(Vector3 &dst, real r, Vector3 *n = 0);
	bool CollideCapsule(Capsule &c, Vector3 *n = 0);
	bool CollideDynamicSphere(Vector3 &src, Vector3 &dst, real r, Vector3 *n = 0);
	bool CollideDynamicEllipsoid(Vector3 &src, Vector3 &out_dst, real r, real yscale, Vector3 *out_n);

	bool HasChildren();

	SERIALIZABLE

protected:

	enum CollisionType { ONC_NONE, ONC_TOUCHING, ONC_COLLIDING, ONC_EMBEDDED };
	class OctNodeCollision
	{
	public:

		OctNodeCollision(Vector3 &v1, Vector3 &v2, Vector3 &v3, CollisionType type)
		{
			m_points[0] = v1;
			m_points[1] = v2;
			m_points[2] = v3;
			m_type = type;
		}

		Vector3 m_points[3];
		CollisionType m_type;

		OctNodeCollision &operator = (const OctNodeCollision &other)
		{
			m_points[0] = other.m_points[0];
			m_points[1] = other.m_points[1];
			m_points[2] = other.m_points[2];
			m_type = other.m_type;
			return *this;
		}
	};

	bool CollideDynamicSphereHelper(Vector3 &src, Vector3 &dst, real r, Vector3 *n = 0);

	CollisionType CollideDynamicEllipsoidHelper(
		Vector3 &src, 
		Vector3 &out_dst, 
		real r, 
		real yscale, 
		Vector3 *out_n,
		Vector3 *pofc = 0,
		std::vector<OctNodeCollision> *collisions = 0);


	bool PolygonContains(Vector3 &p, Vector3 &v1, Vector3 &v2, Vector3 &v3);
	bool PlaneSweptSphereIntersection(
		Vector3 &n, Vector3 &p,
		Vector3 begin, Vector3 &delta, real radius, 
		float &t1, float &t2,
		real &dist_to_plane);

	bool PolygonCircleIntersect(
		Vector3 &v1, Vector3 &v2, Vector3 &v3, 
		Vector3 &center, real radius, Vector3 &out);

	bool SweptCircleEdgeVertexIntersect(
			Vector3 &v1, Vector3 &v2, Vector3 &v3,
			Vector3 &begin, Vector3 &delta, 
			real a, real b, real c, 
			Vector3 &out_point, float &out_fraction);

	bool FindLowestRootInInterval(real a, real b, real c, real upper_bound, real &outx);

	bool PolygonSweptSphereIntersect(
		Vector3 &v1, Vector3 &v2, Vector3 &v3,
		Vector3 &begin, Vector3 &delta, 
		real radius, 
		Vector3 &out_point,
		real &out_fraction,
		real &dist_to_plane);

	void GetChildBounds(int32 child, Vector3 &min, Vector3 &max);

protected:
	OctNode *m_children[8];

	std::vector<Vector3> m_verts;

	enum { LEFT = 1, RIGHT = 2, LOWER = 4, UPPER = 8, BACK = 16, FRONT = 32 };
	static uint32 ms_child_flags[8];

};

}

}
