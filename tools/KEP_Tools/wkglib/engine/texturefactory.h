/******************************************************************************
 texturefactory.h

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/

#pragma once

#include <string>
#include "singleton.h"
#include "factory.h"

namespace wkg 
{

namespace engine 
{

class Texture;

class TextureFactory : 
	public Singleton <TextureFactory>, 
	public Factory<Texture, std::string>
{
    public:
	    TextureFactory();
	    virtual ~TextureFactory();

		Texture* GetCubeTexture(const std::string& filename);

    protected:
        virtual Texture* Create(const std::string& filename) const;

	protected:
		mutable bool createCube;
};

}

}

