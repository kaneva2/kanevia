/******************************************************************************
 orientationkeyframecontroller.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/


#include "wkgtypes.h"
#include "OrientationKeyframeController.h"

namespace wkg
{

namespace engine
{

OrientationKeyframeController::OrientationKeyframeController() :
	Controllable(),
	Controller(),
	m_frame(0), 
	m_next_frame(0),
	m_partial_frame(0.0f),
	m_orientation(0)
{
}

OrientationKeyframeController::~OrientationKeyframeController()
{
}

void 
OrientationKeyframeController::ControllableUpdate()
{
	if(!m_orientation) return;
	// update the position based on the keyframe list we have

	Quaternion curr_orient(0.0f, 0.0f, 0.0f, 1.0f);
	Quaternion next_orient(0.0f, 0.0f, 0.0f, 1.0f);
	
	bool curr_found = false;
	bool next_found = false;

	if(m_frame == -1)
	{
		// special case for transitions
		curr_found = true;
		curr_orient = m_begin_transition;
	}

	WKGOrientationKeyframeIterator itor = m_orientation_keyframes.begin();
	WKGOrientationKeyframeIterator iend = m_orientation_keyframes.end();

	for(; itor < iend; itor++)
	{
		if((*itor)->m_frame == m_frame)
		{
			curr_found = true;
			curr_orient = (*itor)->m_orientation;

			if(next_found)
				break;
		}
		if((*itor)->m_frame == m_next_frame)
		{
			next_found = true;
			next_orient = (*itor)->m_orientation;

			if(curr_found)
				break;
		}
	}

	(*m_orientation) = curr_orient.Slerp(next_orient, m_partial_frame);
	ControllerUpdate();
}

}

}