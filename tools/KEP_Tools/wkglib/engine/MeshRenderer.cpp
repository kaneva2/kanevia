/******************************************************************************
 MeshRenderer.cpp

 Copyright (c) 2004-2006 Kaneva, Inc. All Rights Reserved Worldwide
 Kaneva Proprietary and Confidential
******************************************************************************/
#include "wkgtypes.h"
#include "MeshRenderer.h"
#include "MeshTypes.h"
#include "Mesh.h"
#include "MeshSection.h"
#include "Device.h"
#include "vertexshader.h"
#include "vsfactory.h"

namespace wkg 
{

namespace engine 
{

void MeshRenderer::Render(Mesh* mesh)
{
	Device::Instance()->SetVertexShader(0);
	Device::Instance()->SetFVF(D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1 | D3DFVF_TEXCOORDSIZE2(0));

	D3DMATERIAL9 mat, fullMat;
	MeshSection *section;
	WKGMaterial *meshMat;
	Texture *meshTex;
	uint32 i;

	wkg::memset(&fullMat, 0, sizeof(D3DMATERIAL9));
	fullMat.Diffuse.r = fullMat.Diffuse.g = fullMat.Diffuse.b = fullMat.Diffuse.a = 1.0f;
	fullMat.Ambient.r = fullMat.Ambient.g = fullMat.Ambient.b = fullMat.Ambient.a = 1.0f;

	Device::Instance()->SetStreamSource(0, mesh->GetVB(), 0, sizeof(WKGVertex));

//	if (mesh->GetColorVB())
//		Device::Instance()->SetStreamSource(1, mesh->GetColorVB(), sizeof(WKGVertexColor));

	for (i = 0; i < mesh->GetNumSections(); i++)
	{
		section = mesh->GetSections()[i];
		if (section)
		{
			meshTex = mesh->GetTextures()[section->m_material];
			if (meshTex)
			{
				Device::Instance()->SetMaterial(&fullMat);
			}
			else
			{
				meshMat = mesh->GetMaterials() + section->m_material;
				wkg::memcpy(&(mat.Diffuse), &(meshMat->diffuse), sizeof(WKGColor));
				wkg::memcpy(&(mat.Ambient), &(meshMat->ambient), sizeof(WKGColor));
				wkg::memcpy(&(mat.Specular), &(meshMat->specular), sizeof(WKGColor));
				wkg::memset(&(mat.Emissive), 0, sizeof(WKGColor));
				mat.Power = meshMat->power;
				Device::Instance()->SetMaterial(&mat);
			}

			Device::Instance()->SetTexture(0, meshTex);
			Device::Instance()->SetStreamSource(1, 0, 0, 0);
			Device::Instance()->SetStreamSource(2, 0, 0, 0);
			section->Render();
		}
	}

//	if (mesh->GetColorVB())
//	   Device::Instance()->SetStreamSource(1, 0, 0);
}

}

}
